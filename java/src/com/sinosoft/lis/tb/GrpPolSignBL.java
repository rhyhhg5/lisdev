package com.sinosoft.lis.tb;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保签单业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class GrpPolSignBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 数据操作字符串 */
  private String mOperate;
  private FDate fDate = new FDate();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  /** 业务处理相关变量 */
  private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

  /** 实收总表 */
  private LJAPaySchema mLJAPaySchema = new LJAPaySchema();

  private String mSerialNo = "";
  private String mPolType = "2"; // 2-集体单  3-合同下的集体单
  private double mLeft = 0; // 多交的金额
  private String mRiskFlag = ""; // 主险或附险
  private double sumMoney = 0; // 总的交费额
  private Date maxPayDate;
  private Date maxEnterAccDate;
  private String mPartFlag = "0"; // 部分签单标记 "0"--非部分签单 "1"--部分签单

  private String mEdorType = ""; //保全项目
  private boolean mBQFlag = false; //保全标记
  private TransferData mBQData = null; //保全标记
  private String mEdorNo = "";

  private int CycNum = 50; //集体下一次循环的个人数目
  public GrpPolSignBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (this.getInputData(cInputData) == false)
      return false;

    // 数据操作业务处理
    if (this.dealData() == false)
      return false;

    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData() {
    String tGrpPolNo = mLCGrpPolSchema.getGrpPolNo();
    mInputData.clear();

    // 生成流水号
    if (mSerialNo.trim().equals("")) {
      String tLimit = "";
      tLimit = PubFun.getNoLimit(mGlobalInput.ComCode);
      mSerialNo = PubFun1.CreateMaxNo("SerialNo", tLimit);
    }

    //外部保全可以调用--加入BQData-包含流水号，GlobalInput,
    if (dealOneGrp("", tGrpPolNo, null, null) == false)
      return false;

    return true;
  }

  /**
   * 处理一张集体投保单的数据
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  public boolean dealOneGrp(String tContNo, String tGrpPolNo,
                            LJAPaySchema tLJAPaySchema, TransferData tBQData) {
    if (StrTool.cTrim(tGrpPolNo).equals(""))
      return false;

    //如果保全处理
    if (tBQData != null) {
      mBQData = tBQData;
      mGlobalInput = (GlobalInput) tBQData.getValueByName("GlobalInput");
      mSerialNo = (String) tBQData.getValueByName("SerialNo");
      mEdorType = (String) tBQData.getValueByName("EdorType");
      mEdorNo = (String) tBQData.getValueByName("EdorNo");
      mBQFlag = true;
    }
    else {
      mBQData = null;
      mEdorType = "";
      mBQFlag = false;
    }
    mLJAPaySchema = tLJAPaySchema;

    // 准备集体投保单的数据
    LCGrpPolSchema tLCGrpPolSchema = this.prepareOneGrp(tContNo, tGrpPolNo);
    if (tLCGrpPolSchema == null)
      return false;

    // 签单校验
    if (this.checkOneGrp(tLCGrpPolSchema) == false)
      return false;

    // 准备公用信息
    String tNewGrpPolNo = this.preparePubInfo(tLCGrpPolSchema);
    if (tNewGrpPolNo == null)
      return false;

    //处理集体的数据
    VData VDataForGrp = getOneGrp(tLCGrpPolSchema, tNewGrpPolNo);
    if (VDataForGrp == null)
      return false;

    //处理集体下所有个人的数据
    if (getPolForGrp(tLCGrpPolSchema, tNewGrpPolNo, tBQData) == false)
      return false;

    //个人处理完毕后处理集体的状态
    if (updateOneGrp(VDataForGrp) == false)
      return false;

    //如果有磁盘导入信息，删除磁盘导入信息
    delDiskInfo(tGrpPolNo);

    return true;
  }

  /**
   * 处理一张集体投保单的数据
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private VData getOneGrp(LCGrpPolSchema tLCGrpPolSchema, String tNewGrpPolNo) {
    VData tGrpPol = new VData();
    String tOldGrpPolNo = tLCGrpPolSchema.getGrpProposalNo();
    String tPolNo = "";
    String tGrpPolNo = "";
    /*Lis5.3 upgrade get
       String tContNo = tLCGrpPolSchema.getContNo();
     */
    String tContNo = "";

    VData tAPay = new VData();

    //如果非保全签单-走正常流程-得到实收总表
    if (mBQFlag == false) {
      // 准备实收总表的信息（实收总表必须独立出来，考虑到主附险使用同一个收据号的问题）
      tAPay = this.prepareAPay(tLCGrpPolSchema, tNewGrpPolNo);
      if (tAPay == null)
        return null;
    }

    // 非部分签单才处理集体单的部分
    if (mPartFlag.equals("0")) {
      // 处理集体本身的信息（注意：顺序不能改变!）
      tGrpPol = this.dealGrpPol(tLCGrpPolSchema, tNewGrpPolNo);
      if (tGrpPol == null)
        return null;

      // 把实收总表信息加入集体本身的信息中
      tGrpPol.add(tAPay);
    }

    // 部分签单取出签过的集体单的部分
    if (mPartFlag.equals("1"))
      tGrpPol.add(tLCGrpPolSchema);

    if (mBQFlag == true) { //保全数据添加--不分保全项目--为BLS的保全项目判断而传入
      tGrpPol.add(mBQData);
    }

    //数据提交
    GrpPolSignBLS tGrpPolSignBLS = new GrpPolSignBLS();
    if (tGrpPolSignBLS.saveGrpPol(tGrpPol) == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tGrpPolSignBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "saveGrpPol";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return null;
    }

    return tGrpPol;
  }

  /**
   * 处理一张集体投保单下所有个人的数据
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getPolForGrp(LCGrpPolSchema tLCGrpPolSchema,
                               String tNewGrpPolNo, TransferData tBQData) {
    VData tReturn = new VData();
    VData tGrpPol = new VData();
    String tOldGrpPolNo = tLCGrpPolSchema.getGrpProposalNo();
    String tPolNo = "";
    String tGrpPolNo = "";
    /*Lis5.3 upgrade get
                 String tContNo = tLCGrpPolSchema.getContNo();
     */
    String tContNo = "";

    ProposalSignBLS tSignBLS = new ProposalSignBLS();
    ProposalSignBL tPolSign = new ProposalSignBL();

    VData tPolPub = new VData();
    tPolPub.add(mGlobalInput);
    tPolPub.add(mLJAPaySchema);
    tPolPub.add(mSerialNo);

    //查询集体单险种的险种承保描述表，传入个单
    LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
    tLMRiskAppDB.setRiskCode(tLCGrpPolSchema.getRiskCode());
    if (tLMRiskAppDB.getInfo() == false) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "prepareOnePol";
      tError.errorMessage = "险种承保描述取出失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    tPolPub.add(tLMRiskAppDB.getSchema());

    //查询集体单险种的险种描述表，传入个单
    LMRiskDB tLMRiskDB = new LMRiskDB();
    tLMRiskDB.setRiskCode(tLCGrpPolSchema.getRiskCode());
    if (tLMRiskDB.getInfo() == false) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "prepareOnePol";
      tError.errorMessage = "险种描述取出失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    tPolPub.add(tLMRiskDB.getSchema());

    //查询系统变量表,传入个单
    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    tLDSysVarDB.setSysVar("ChangeSubValiDate");
    if (tLDSysVarDB.getInfo() == false) {
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "dealData";
      tError.errorMessage = "系统变量表没有ChangeSubValiDate变量！";
      this.mErrors.addOneError(tError);
      return false;
    }
    tPolPub.add(tLDSysVarDB.getSchema());

    //查询集体单下的个人数目
    String strSql = "select count(*) from lcpol where grppolno='" +
        tOldGrpPolNo + "'";
    ExeSQL tExeSQL = new ExeSQL();
    SSRS tSSRS = tExeSQL.execSQL(strSql);
    String strCount = tSSRS.GetText(1, 1);
    int iCount = Integer.parseInt(strCount);
    int NUM = 1;
    strSql = "select PolNo from lcpol where grppolno='" + tOldGrpPolNo + "'";

    //如果基数大与个人保单纪录数，跳出循环
    while (NUM <= iCount) {
      NUM = NUM + CycNum;

      tExeSQL = new ExeSQL();
      //每次取出查询结果集的前CycNum条签单。（每次查询出来的结果会比上一次结果少CycNum条，因为已经签单）
      tSSRS = tExeSQL.execSQL(strSql, 1, CycNum);
      if (tSSRS == null) {
        CError tError = new CError();
        tError.moduleName = "GrpPolSignBL";
        tError.functionName = "getPolForGrp";
        tError.errorMessage = "集体下个人投保单读取失败！";
        this.mErrors.addOneError(tError);
        return false;
      }

      for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        tPolNo = tSSRS.GetText(i, 1);
        tGrpPolNo = tNewGrpPolNo;
        tContNo = "";

         VData tPol = null;

 // 接口发生改变
//        VData tPol = tPolSign.dealOnePol(tContNo, tGrpPolNo, tPolNo, tPolPub,
//                                         tBQData);
        if (tPol == null) {
          // @@错误处理
          this.mErrors.copyAllErrors(tPolSign.mErrors);
          return false;
        }
        //个人签单
        if (tSignBLS.saveOnePol(tPol) == false)
          return false;
      }

    }

    return true;
  }

  /**
   * 更新集体保单的状态
   * @param GrpDetail
   * @return
   */
  private boolean updateOneGrp(VData GrpDetail) {

    LCGrpPolSchema tLCGrpPolSchema = (LCGrpPolSchema) GrpDetail.
        getObjectByObjectName("LCGrpPolSchema", 0);
    int index = GrpDetail.indexOf(tLCGrpPolSchema);
    tLCGrpPolSchema.setAppFlag("1");
    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
    LCPolSet tLCPolSet = tLCPolDB.query();
    String PayToDate = "1900-1-1";
    String PayEndDate = "1900-1-1";
    PubFun tPubFun = new PubFun();
    for (int n = 1; n <= tLCPolSet.size(); n++) {
      if (tPubFun.calInterval(PayToDate, tLCPolSet.get(n).getPaytoDate(), "D") >
          0) {
        PayToDate = tLCPolSet.get(n).getPaytoDate();
      }
      if (tPubFun.calInterval(PayEndDate, tLCPolSet.get(n).getPayEndDate(), "D") >
          0) {
        PayEndDate = tLCPolSet.get(n).getPayEndDate();
      }
    }
    tLCGrpPolSchema.setPaytoDate(PayToDate);
    tLCGrpPolSchema.setPayEndDate(PayEndDate);
    GrpDetail.setElementAt(tLCGrpPolSchema, index);
    //数据提交
    GrpPolSignBLS tGrpPolSignBLS = new GrpPolSignBLS();
    if (tGrpPolSignBLS.updateGrpAppFlag(GrpDetail) == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tGrpPolSignBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "updateOneGrp";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备一张投保单的数据
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private LCGrpPolSchema prepareOneGrp(String tContNo, String tGrpPolNo) {
    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();

    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    tLCGrpPolDB.setGrpProposalNo(tGrpPolNo);
    tLCGrpPolSet = tLCGrpPolDB.query();
    if (tLCGrpPolDB.mErrors.needDealError() == true) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "prepareOneGrp";
      tError.errorMessage = "投保单取数失败!";
      this.mErrors.addOneError(tError);
      return null;
    }
    tLCGrpPolSchema = (LCGrpPolSchema) tLCGrpPolSet.get(1);

    // 判断是否为部分签单
    if (!tLCGrpPolSchema.getGrpPolNo().equals(tLCGrpPolSchema.getGrpProposalNo()))
      mPartFlag = "1";

      // 如果有新值则进行修改赋值
    if (!StrTool.cTrim(tContNo).equals("")) {
      /*Lis5.3 upgrade set
          tLCGrpPolSchema.setContNo( tContNo );
       */
      mPolType = "3";
    }

    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setGrpPolNo(tGrpPolNo);
    int i = tLCPolDB.getCount();
    if (i <= 0 && mPartFlag.equals("0")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "prepareOneGrp";
      tError.errorMessage = "此集体投保单下没有个人投保单信息，不能签单!";
      this.mErrors.addOneError(tError);
      return null;
    }

    // 判断是主险还是附险
    mRiskFlag = "";
    String tRiskCode = tLCGrpPolSchema.getRiskCode();
    LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
    tLMRiskAppDB.setRiskCode(tRiskCode);
    if (tLMRiskAppDB.getInfo() == false) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "preparePolGroup";
      tError.errorMessage = "险种描述取出失败!";
      this.mErrors.addOneError(tError);
      return null;
    }

    // 是主险的情况
    if (StrTool.cTrim(tLMRiskAppDB.getSubRiskFlag()).equals("M")) {
      mRiskFlag = "M";
      //判断附加险是否能签单
      if (checkSRiskPolState(tLCGrpPolSchema) == false)
        return null;

    }
    // 是附险的情况
    if (StrTool.cTrim(tLMRiskAppDB.getSubRiskFlag()).equals("S"))
      mRiskFlag = "S";

    String needgetpoldate = tLMRiskAppDB.getNeedGetPolDate();
    if (needgetpoldate == null || needgetpoldate.equals("0")) {
      //回执日期为空
    }
    else if (needgetpoldate.equals("1")) {
      //令回执日期为投保单录入日期
      /*Lis5.3 upgrade set
       tLCGrpPolSchema.setGetPolDate(tLCGrpPolSchema.getMakeDate());
       */
    }
    else if (needgetpoldate.equals("2")) {
      //令回执日期为签单日期
      /*Lis5.3 upgrade set
       tLCGrpPolSchema.setGetPolDate(PubFun.getCurrentDate());
       */
    }
    return tLCGrpPolSchema;
  }

  /**
   * 校验数据是否合法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkOneGrp(LCGrpPolSchema tLCGrpPolSchema) {
    // 校验是否复核
    if (this.checkApprove(tLCGrpPolSchema) == false)
      return false;

    // 校验是否核保
    if (this.checkUW(tLCGrpPolSchema) == false)
      return false;

    //　校验财务信息--如果是保全类型跳过检验-BQFlag
    if (mPartFlag.equals("0") && mBQFlag == false) {
      if (this.checkFinance(tLCGrpPolSchema) == false)
        return false;
    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    //集体投保单
    mLCGrpPolSchema.setSchema( (LCGrpPolSchema) cInputData.
                              getObjectByObjectName("LCGrpPolSchema", 0));

    if (StrTool.cTrim(mLCGrpPolSchema.getGrpPolNo()).equals("")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "集体投保单号传入失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 校验投保单是否复核
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApprove(LCGrpPolSchema tLCGrpPolSchema) {
    if (StrTool.cTrim(tLCGrpPolSchema.getApproveFlag()).equals("0")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "此集体投保单尚未进行复核操作，不能签单!（集体投保单号：" +
          tLCGrpPolSchema.getGrpPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (!StrTool.cTrim(tLCGrpPolSchema.getApproveFlag()).equals("9")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "此集体投保单没有复核通过，不能签单!（集体投保单号：" +
          tLCGrpPolSchema.getGrpPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 校验投保单是否核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkUW(LCGrpPolSchema tLCGrpPolSchema) {
    if (StrTool.cTrim(tLCGrpPolSchema.getUWFlag()).equals("0")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "此集体投保单尚未进行核保操作，不能签单!（集体投保单号：" +
          tLCGrpPolSchema.getGrpPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (! (StrTool.cTrim(tLCGrpPolSchema.getUWFlag()).equals("3") //条件承保
           || StrTool.cTrim(tLCGrpPolSchema.getUWFlag()).equals("4") //通融承保
           || StrTool.cTrim(tLCGrpPolSchema.getUWFlag()).equals("9"))) { //正常通过
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "此集体投保单核保没有通过，不能签单!（集体投保单号：" +
          tLCGrpPolSchema.getGrpPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 校验投保单财务信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkFinance(LCGrpPolSchema tLCGrpPolSchema) {
    double sumPrem = 0;
    sumMoney = 0;
    maxPayDate = fDate.getDate("1900-01-01");
    maxEnterAccDate = fDate.getDate("1900-01-01");

    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    tLDSysVarDB.setSysVar("SignPayCheckFlag");
    tLDSysVarDB.getInfo();
    // financeFlag: "1"--暂交费必须等于应交保费　"2"--暂交费可以大于应交保费
    String financeFlag = tLDSysVarDB.getSysVarValue();

    sumPrem = tLCGrpPolSchema.getPrem();

    LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
    tLJTempFeeDB.setOtherNo(tLCGrpPolSchema.getGrpPolNo());
    tLJTempFeeDB.setOtherNoType("1"); // 集体单号
    tLJTempFeeDB.setTempFeeType("1");
    tLJTempFeeDB.setConfFlag("0");

    LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
    if (tLJTempFeeDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "checkFinance";
      tError.errorMessage = "财务信息取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    //houzm增加的部分：将暂加费纪录类型为银行扣款的也查询出来
    LJTempFeeDB tempLJTempFeeDB = new LJTempFeeDB();
    tempLJTempFeeDB.setOtherNo(tLCGrpPolSchema.getPrtNo()); //印刷号
    tempLJTempFeeDB.setOtherNoType("4"); //其它号码类型为印刷号
    //tempLJTempFeeDB.setTempFeeType( "5" ); //暂交费类型为银行扣款
    tempLJTempFeeDB.setRiskCode(tLCGrpPolSchema.getRiskCode());
    tempLJTempFeeDB.setConfFlag("0"); //核销标记为假
    LJTempFeeSet tempLJTempFeeSet = tempLJTempFeeDB.query();
    if (tempLJTempFeeDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tempLJTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "checkFinance";
      tError.errorMessage = "财务信息取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    //将暂交费纪录类型为银行扣款的其它号码类型置为1,用于保存
    /*
                     for(int m=1;m<=tempLJTempFeeSet.size();m++)
                     {
        tempLJTempFeeSet.get(m).setOtherNoType("1");
                     }
     */
    tLJTempFeeSet.add(tempLJTempFeeSet); //将后续查询到的纪录存放到set
    //以上为houzm添加

    if (tLJTempFeeSet.size() == 0) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "checkFinance";
      tError.errorMessage = "此集体投保单尚未交费,不能签单!（集体投保单号：" +
          tLCGrpPolSchema.getGrpPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    int m = tLJTempFeeSet.size();
    for (int j = 1; j <= m; j++) {
      LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(j);
      if (tLJTempFeeSchema.getEnterAccDate() == null) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "GrpPolSignBL";
        tError.functionName = "checkFinance";
        tError.errorMessage = "财务缴费还没有到帐，不能签单!（集体投保单号：" +
            tLCGrpPolSchema.getGrpPolNo().trim() + "）";
        this.mErrors.addOneError(tError);
        return false;
      }

      // LJAPay中使用
      Date payDate = fDate.getDate(tLJTempFeeSchema.getPayDate());
      if (maxPayDate.before(payDate))
        maxPayDate = payDate;
      Date enterAccDate = fDate.getDate(tLJTempFeeSchema.getEnterAccDate());
      if (maxEnterAccDate.before(enterAccDate))
        maxEnterAccDate = enterAccDate;

      sumMoney += tLJTempFeeSchema.getPayMoney();
    }
    mLeft = sumMoney - sumPrem;

    System.out.println("---sumPrem:" + sumPrem);
    System.out.println("---sumMoney:" + sumMoney);
    if (sumMoney < sumPrem) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "checkFinance";
      tError.errorMessage = "财务缴费金额少于投保单应缴保费，不能签单!（集体投保单号：" +
          tLCGrpPolSchema.getGrpPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (StrTool.cTrim(financeFlag).equals("1") && sumMoney < sumPrem) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolSignBL";
      tError.functionName = "checkFinance";
      tError.errorMessage = "财务缴费金额大于投保单应缴保费，不能签单!（集体投保单号：" +
          tLCGrpPolSchema.getGrpPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 处理集体投保单的数据
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private VData dealGrpPol(LCGrpPolSchema tLCGrpPolSchema, String tNewGrpPolNo) {
    VData tReturn = new VData();

    // 准备集体保单的数据
    tLCGrpPolSchema = this.prepareGrpPol(tLCGrpPolSchema, tNewGrpPolNo);
    if (tLCGrpPolSchema == null)
      return null;

    tReturn.add(tLCGrpPolSchema);

    // 核保信息
    VData tUW = this.prepareUW(tLCGrpPolSchema, tNewGrpPolNo);
    if (tUW == null)
      return null;

    tReturn.add( (LCGUWMasterSet) tUW.get(0));
    tReturn.add( (LCGUWSubSet) tUW.get(1));
    tReturn.add( (LCGUWErrorSet) tUW.get(2));

    // 准备集体的交费信息--如果是保全，则不走正常财务数据准备
    if (mBQFlag) {
      VData tVData = new VData();
      //团体新增附加险
      if (mEdorType.equals("NS")) {
        mBQData.setNameAndValue("NewPolNo", tNewGrpPolNo);
        tVData.add(mBQData);
        tVData.add(mGlobalInput);
        tVData.add(tLCGrpPolSchema);

        EdorSignBL tEdorSignBL = new EdorSignBL();
        if (tEdorSignBL.submitData(tVData, "QUERY||GRPNS") == false) {
          CError tError = new CError();
          tError.moduleName = "GrpPolSignBL";
          tError.functionName = "dealGrpPol";
          tError.errorMessage = "保全财务处理失败";
          this.mErrors.addOneError(tError);
          return null;
        }
        VData temp = tEdorSignBL.getResult();
        LJAPayGrpSet tLJAPayGrpSet = (LJAPayGrpSet) temp.getObjectByObjectName(
            "LJAPayGrpSet", 0);

        //返回得到集体实收总表数据--不要和已经存在的数据重复
        tReturn.add(tLJAPayGrpSet);
      }
    }
    else {
      VData tFinance = this.prepareGrpFinance(tLCGrpPolSchema, tNewGrpPolNo);
      if (tFinance == null)
        return null;
      tReturn.add( (LJTempFeeSet) tFinance.get(0));
      tReturn.add( (LJTempFeeClassSet) tFinance.get(1));
      tReturn.add( (LJAPayGrpSet) tFinance.get(2));
    }

    return tReturn;
  }

  /**
   * 准备公用信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private String preparePubInfo(LCGrpPolSchema tLCGrpPolSchema) {
    String tGrpPolNo = "";
    String tLimit = PubFun.getNoLimit(tLCGrpPolSchema.getManageCom());

    if (mPartFlag.equals("0")) {
      // 生成保单号
      tGrpPolNo = PubFun1.CreateMaxNo("GRPPOLNO", tLimit);
      System.out.println("---NewGrpPolNo:" + tGrpPolNo);
      if (tGrpPolNo.trim().equals("")) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "GrpPolSignBL";
        tError.functionName = "preparePubInfo";
        tError.errorMessage = "集体保单号生成失败!";
        this.mErrors.addOneError(tError);
        return null;
      }
    }
    if (mPartFlag.equals("1"))
      tGrpPolNo = tLCGrpPolSchema.getGrpPolNo();

    return tGrpPolNo;
  }

  /**
   * 准备集体保单信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private LCGrpPolSchema prepareGrpPol(LCGrpPolSchema tLCGrpPolSchema,
                                       String tNewGrpPolNo) {
    tLCGrpPolSchema.setGrpPolNo(tNewGrpPolNo);
    tLCGrpPolSchema.setSumPrem(tLCGrpPolSchema.getPrem());
    tLCGrpPolSchema.setSumPay(tLCGrpPolSchema.getSumPrem());
    tLCGrpPolSchema.setDif(mLeft);
    /*Lis5.3 upgrade set
       tLCGrpPolSchema.setSignCom( mGlobalInput.ManageCom );
       tLCGrpPolSchema.setSignDate( PubFun.getCurrentDate() );
     */
    //为了支持部分签单，此标记在全部签单成功时做update操作
    //tLCGrpPolSchema.setAppFlag( "1" );
    tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
    tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());

    return tLCGrpPolSchema;
  }

  /**
   * 准备核保信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private VData prepareUW(LCGrpPolSchema tLCGrpPolSchema, String tNewGrpPolNo) {
    VData tReturn = new VData();
    String tOldGrpPolNo = tLCGrpPolSchema.getGrpPolNo();

    LCGUWMasterSet tLCGUWMasterSet = new LCGUWMasterSet();
    LCGUWSubSet tLCGUWSubSet = new LCGUWSubSet();
    LCGUWErrorSet tLCGUWErrorSet = new LCGUWErrorSet();

    LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB();
    tLCGUWMasterDB.setGrpPolNo(tOldGrpPolNo);
    tLCGUWMasterSet = tLCGUWMasterDB.query();
    if (tLCGUWMasterDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCGUWMaster表取数失败!";
      this.mErrors.addOneError(tError);
      return null;
    }

    int n = tLCGUWMasterSet.size();
    for (int i = 1; i <= n; i++) {
      LCGUWMasterSchema tLCGUWMasterSchema = tLCGUWMasterSet.get(i);

      tLCGUWMasterSchema.setGrpPolNo(tNewGrpPolNo);
      tLCGUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
      tLCGUWMasterSchema.setModifyTime(PubFun.getCurrentTime());

      tLCGUWMasterSet.set(i, tLCGUWMasterSchema);
    }

    // 核保轨迹表
    LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB();
    tLCGUWSubDB.setGrpPolNo(tOldGrpPolNo);
    tLCGUWSubSet = tLCGUWSubDB.query();
    if (tLCGUWSubDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCGUWSub表取数失败!";
      this.mErrors.addOneError(tError);
      return null;
    }

    int m = tLCGUWSubSet.size();
    for (int i = 1; i <= m; i++) {
      LCGUWSubSchema tLCGUWSubSchema = tLCGUWSubSet.get(i);

      tLCGUWSubSchema.setGrpPolNo(tNewGrpPolNo);
      tLCGUWSubSchema.setModifyDate(PubFun.getCurrentDate());
      tLCGUWSubSchema.setModifyTime(PubFun.getCurrentTime());

      tLCGUWSubSet.set(i, tLCGUWSubSchema);
    }

    // 核保错误信息表
    LCGUWErrorDB tLCGUWErrorDB = new LCGUWErrorDB();
    tLCGUWErrorDB.setGrpPolNo(tOldGrpPolNo);
    tLCGUWErrorSet = tLCGUWErrorDB.query();
    if (tLCGUWErrorDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGUWErrorDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ProposalSignBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCGUWError表取数失败!";
      this.mErrors.addOneError(tError);
      return null;
    }

    int k = tLCGUWErrorSet.size();
    for (int i = 1; i <= k; i++) {
      LCGUWErrorSchema tLCGUWErrorSchema = tLCGUWErrorSet.get(i);

      tLCGUWErrorSchema.setGrpPolNo(tNewGrpPolNo);
      tLCGUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
      tLCGUWErrorSchema.setModifyTime(PubFun.getCurrentTime());

      tLCGUWErrorSet.set(i, tLCGUWErrorSchema);
    }

    // 顺序不能修改
    tReturn.add(tLCGUWMasterSet);
    tReturn.add(tLCGUWSubSet);
    tReturn.add(tLCGUWErrorSet);

    return tReturn;
  }

  /**
   * 准备财务信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private VData prepareGrpFinance(LCGrpPolSchema tLCGrpPolSchema,
                                  String tNewGrpPolNo) {
    VData tReturn = new VData();
    String tOldGrpPolNo = tLCGrpPolSchema.getGrpProposalNo();

    String confDate = PubFun.getCurrentDate();
    String confTime = PubFun.getCurrentTime();

    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();

    if (mPolType.equals("3")) { // 合同下的集体的投保单
      tReturn.add(tLJTempFeeSet);
      tReturn.add(tLJTempFeeClassSet);
    }
    if (mPolType.equals("2")) {
      // 暂交费信息
      LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
      tLJTempFeeDB.setOtherNo(tOldGrpPolNo);
      tLJTempFeeDB.setOtherNoType("1");
      tLJTempFeeDB.setTempFeeType("1");
      tLJTempFeeDB.setConfFlag("0");

      tLJTempFeeSet = tLJTempFeeDB.query();
      if (tLJTempFeeDB.mErrors.needDealError()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpPolSignBL";
        tError.functionName = "prepareFinance";
        tError.errorMessage = "LJTempFee表取数失败!";
        this.mErrors.addOneError(tError);
        return null;
      }
      //houzm增加的部分：将暂加费纪录类型为银行扣款的也查询出来
      LJTempFeeDB tempLJTempFeeDB = new LJTempFeeDB();
      tempLJTempFeeDB.setOtherNo(tLCGrpPolSchema.getPrtNo()); //印刷号
      tempLJTempFeeDB.setOtherNoType("4"); //其它号码类型为印刷号
      //tempLJTempFeeDB.setTempFeeType( "5" ); //暂交费类型为银行扣款
      tempLJTempFeeDB.setRiskCode(tLCGrpPolSchema.getRiskCode());
      tempLJTempFeeDB.setConfFlag("0"); //核销标记为假
      LJTempFeeSet tempLJTempFeeSet = tempLJTempFeeDB.query();
      if (tempLJTempFeeDB.mErrors.needDealError()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tempLJTempFeeDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "ProposalSignBL";
        tError.functionName = "checkFinance";
        tError.errorMessage = "财务信息取数失败!";
        this.mErrors.addOneError(tError);
        return null;
      }
      tLJTempFeeSet.add(tempLJTempFeeSet); //将后续查询到的纪录存放到set

      //以上为houzm添加
      int n = tLJTempFeeSet.size();
      for (int i = 1; i <= n; i++) {
        LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i);

        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        tLJTempFeeClassDB.setTempFeeNo(tLJTempFeeSchema.getTempFeeNo());
        tLJTempFeeClassSet = tLJTempFeeClassDB.query();
        if (tLJTempFeeClassDB.mErrors.needDealError()) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "GrpPolSignBL";
          tError.functionName = "prepareFinance";
          tError.errorMessage = "LJTempFeeClass表取数失败!";
          this.mErrors.addOneError(tError);
          return null;
        } // end of if

        int m = tLJTempFeeClassSet.size();
        for (int j = 1; j <= m; j++) {
          LJTempFeeClassSchema tLJTempFeeClassSchema = tLJTempFeeClassSet.get(j);
          tLJTempFeeClassSchema.setConfDate(confDate);
          tLJTempFeeClassSchema.setConfFlag("1");
          tLJTempFeeClassSchema.setModifyDate(confDate);
          tLJTempFeeClassSchema.setModifyTime(confTime);

          tLJTempFeeClassSet.set(j, tLJTempFeeClassSchema);
        } // end of for

        tLJTempFeeSchema.setOtherNo(tNewGrpPolNo);
        tLJTempFeeSchema.setOtherNoType("1");
        tLJTempFeeSchema.setConfDate(confDate);
        tLJTempFeeSchema.setConfFlag("1");
        tLJTempFeeSchema.setModifyDate(confDate);
        tLJTempFeeSchema.setModifyTime(confTime);

        tLJTempFeeSet.set(i, tLJTempFeeSchema);
      } // end of for
    } // end of if

    // 实交集体表
    LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
    tLJAPayGrpSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
    tLJAPayGrpSchema.setPayCount(1);
    /*Lis5.3 upgrade get
       tLJAPayGrpSchema.setContNo( tLCGrpPolSchema.getContNo() );
       tLJAPayGrpSchema.setAppntNo( tLCGrpPolSchema.getGrpNo() );
     */
    tLJAPayGrpSchema.setPayNo(mLJAPaySchema.getPayNo());
    tLJAPayGrpSchema.setSumDuePayMoney(tLCGrpPolSchema.getPrem());
    tLJAPayGrpSchema.setSumActuPayMoney(tLCGrpPolSchema.getPrem());
    tLJAPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
    tLJAPayGrpSchema.setPayDate(mLJAPaySchema.getPayDate());
    tLJAPayGrpSchema.setPayType("ZC");
    tLJAPayGrpSchema.setEnterAccDate(mLJAPaySchema.getEnterAccDate());
    tLJAPayGrpSchema.setConfDate(confDate);
    tLJAPayGrpSchema.setLastPayToDate("1899-12-31");
    tLJAPayGrpSchema.setCurPayToDate(tLCGrpPolSchema.getPaytoDate());
    tLJAPayGrpSchema.setSerialNo(mSerialNo);
    tLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
    tLJAPayGrpSchema.setMakeDate(confDate);
    tLJAPayGrpSchema.setMakeTime(confTime);
    tLJAPayGrpSchema.setModifyDate(confDate);
    tLJAPayGrpSchema.setModifyTime(confTime);
    tLJAPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
    tLJAPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
    tLJAPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
    tLJAPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
    tLJAPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
    tLJAPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());

    tLJAPayGrpSet.add(tLJAPayGrpSchema);

    if (mPolType.equals("3")) {
      tReturn.add(tLJAPayGrpSet);

      return tReturn;
    }

    if (mLeft > 0) {
      LJAPayGrpSchema tLJAPayGrpSchema1 = new LJAPayGrpSchema();
      tLJAPayGrpSchema1.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
      tLJAPayGrpSchema1.setPayCount(1);
      /*Lis5.3 upgrade get
          tLJAPayGrpSchema1.setContNo( tLCGrpPolSchema.getContNo() );
          tLJAPayGrpSchema1.setAppntNo( tLCGrpPolSchema.getGrpNo() );
       */
      tLJAPayGrpSchema1.setPayNo(mLJAPaySchema.getPayNo());
      tLJAPayGrpSchema1.setSumDuePayMoney(0);
      tLJAPayGrpSchema1.setSumActuPayMoney(mLeft);
      tLJAPayGrpSchema1.setPayIntv(tLCGrpPolSchema.getPayIntv());
      tLJAPayGrpSchema1.setPayDate(mLJAPaySchema.getPayDate());
      tLJAPayGrpSchema1.setPayType("YET");
      tLJAPayGrpSchema1.setEnterAccDate(mLJAPaySchema.getEnterAccDate());
      tLJAPayGrpSchema1.setConfDate(confDate);
      tLJAPayGrpSchema1.setLastPayToDate("1899-12-31");
      tLJAPayGrpSchema1.setCurPayToDate(tLCGrpPolSchema.getPaytoDate());
      tLJAPayGrpSchema1.setSerialNo(mSerialNo);
      tLJAPayGrpSchema1.setOperator(mGlobalInput.Operator);
      tLJAPayGrpSchema1.setMakeDate(confDate);
      tLJAPayGrpSchema1.setMakeTime(confTime);
      tLJAPayGrpSchema1.setModifyDate(confDate);
      tLJAPayGrpSchema1.setModifyTime(confTime);
      tLJAPayGrpSchema1.setManageCom(tLCGrpPolSchema.getManageCom());
      tLJAPayGrpSchema1.setAgentCom(tLCGrpPolSchema.getAgentCom());
      tLJAPayGrpSchema1.setAgentType(tLCGrpPolSchema.getAgentType());
      tLJAPayGrpSchema1.setRiskCode(tLCGrpPolSchema.getRiskCode());
      tLJAPayGrpSchema1.setAgentCode(tLCGrpPolSchema.getAgentCode());
      tLJAPayGrpSchema1.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
      tLJAPayGrpSet.add(tLJAPayGrpSchema1);
    } // end of if

    // 顺序不能改变
    tReturn.add(tLJTempFeeSet);
    tReturn.add(tLJTempFeeClassSet);
    tReturn.add(tLJAPayGrpSet);

    return tReturn;
  }

  /**
   * 准备实收总表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private VData prepareAPay(LCGrpPolSchema tLCGrpPolSchema, String tNewGrpPolNo) {
    String tAction = "";
    LJAPaySet tLJAPaySet = new LJAPaySet();
    LJAPaySchema tLJAPaySchema = new LJAPaySchema();
    VData tReturn = new VData();

    if (tLCGrpPolSchema.getPrem() == 0.0 // 保费为零时不处理实收总表信息
        || !mPolType.equals("2")) { // 非集体投保单不处理实收总表信息
      tAction = "INSERT";

      tReturn.add(tAction);
      tReturn.add(tLJAPaySet);
      return tReturn;
    }

    if (mPolType.equals("2")) { // 集体单的处理
      if (mPartFlag.equals("1")) { // 处理部分签单（此处不处理LJAPay，只是取出原来的LJAPay的信息）
        LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
        tLJAPayGrpDB.setGrpPolNo(tNewGrpPolNo);
        tLJAPayGrpDB.setPayType("ZC");
        tLJAPayGrpDB.setPayCount(1);
        LJAPayGrpSet tLJAPayGrpSet1 = tLJAPayGrpDB.query();
        if (tLJAPayGrpSet1.size() <= 0) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ProposalSignBL";
          tError.functionName = "prepareAPay";
          tError.errorMessage = "LJAPayGrp表取数失败!";
          this.mErrors.addOneError(tError);
          return null;
        }

        LJAPayGrpSchema tLJAPayGrpSchema = (LJAPayGrpSchema) tLJAPayGrpSet1.get(
            1);

        LJAPaySchema tLJAPaySchema1 = new LJAPaySchema();
        tLJAPaySchema1.setPayNo(tLJAPayGrpSchema.getPayNo());
        tLJAPaySchema1.setPayDate(tLJAPayGrpSchema.getPayDate());
        tLJAPaySchema1.setEnterAccDate(tLJAPayGrpSchema.getEnterAccDate());

        mLJAPaySchema = tLJAPaySchema1;

        tAction = "INSERT";

        tReturn.add(tAction);
        tReturn.add(tLJAPaySet);
        return tReturn;
      } // end of if

      boolean flag = false;
      String mainGrpPolNo = "";

      // 判断本投保单的险种类型和投保情况
      if (mRiskFlag.equals("S")) { // 附险：使用主险的LJAPay，增加总金额
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        String sql = "select * from LCGrpPol "
            + "where PrtNo = '" + tLCGrpPolSchema.getPrtNo() + "' "
            + "and GrpPolNo <> '" + tLCGrpPolSchema.getGrpPolNo() + "' "
            + "and RiskCode in ( "
            + "select RiskCode from LMRiskApp "
            + "where SubRiskFlag = 'M' )";

        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery(sql);
        if (tLCGrpPolDB.mErrors.needDealError() == true) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ProposalSignBL";
          tError.functionName = "prepareAPay";
          tError.errorMessage = "主险投保单取数失败!";
          this.mErrors.addOneError(tError);
          return null;
        }
        if (tLCGrpPolSet.size() == 1) {
          flag = true; // 附险与主险是一批投保的
          LCGrpPolSchema tLCGrpPolSchema1 = tLCGrpPolSet.get(1);
          mainGrpPolNo = tLCGrpPolSchema1.getGrpPolNo();
        }
      } // end of if

      if (mRiskFlag.equals("S") && flag == true) { // 本投保单是附加险，而且与主险投保单同时承保
        tAction = "UPDATE";

        // 取出主险的LJAPay
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setIncomeNo(mainGrpPolNo);
        tLJAPayDB.setIncomeType("1");
        tLJAPaySet = tLJAPayDB.query();
        if (tLJAPaySet.size() != 1) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLJAPayDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ProposalSignBL";
          tError.functionName = "prepareAPay";
          tError.errorMessage = "LJAPay表取数失败!";
          this.mErrors.addOneError(tError);
          return null;
        }

        tLJAPaySchema = (LJAPaySchema) tLJAPaySet.get(1);
        tLJAPaySchema.setSumActuPayMoney(tLJAPaySchema.getSumActuPayMoney() +
                                         sumMoney);
        tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());

        tLJAPaySet.set(1, tLJAPaySchema);

        mLJAPaySchema = tLJAPaySchema;
      }
      else {
        tAction = "INSERT";

        // 生成流水号
        String tLimit = "";
        tLimit = PubFun.getNoLimit(tLCGrpPolSchema.getManageCom());
        String tPayNo = PubFun1.CreateMaxNo("PayNo", tLimit);

        tLJAPaySchema.setPayNo(tPayNo);
        tLJAPaySchema.setIncomeNo(tNewGrpPolNo);
        tLJAPaySchema.setIncomeType("1");
        /*Lis5.3 upgrade get
        tLJAPaySchema.setAppntNo(tLCGrpPolSchema.getGrpNo());
        */
        tLJAPaySchema.setSumActuPayMoney(sumMoney);
        tLJAPaySchema.setPayDate(fDate.getString(maxPayDate));
        tLJAPaySchema.setEnterAccDate(fDate.getString(maxEnterAccDate));
        tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
        tLJAPaySchema.setSerialNo(mSerialNo);
        tLJAPaySchema.setOperator(mGlobalInput.Operator);
        tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
        tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
        tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
        tLJAPaySchema.setManageCom(tLCGrpPolSchema.getManageCom());
        tLJAPaySchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
        tLJAPaySchema.setAgentType(tLCGrpPolSchema.getAgentType());
//        tLJAPaySchema.setBankCode(tLCGrpPolSchema.getBankCode());
//        tLJAPaySchema.setBankAccNo(tLCGrpPolSchema.getBankAccNo());

        tLJAPaySchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
        tLJAPaySchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
        tLJAPaySchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
 //       tLJAPaySchema.setAccName(tLCGrpPolSchema.getAccName());

        tLJAPaySet.add(tLJAPaySchema);

        mLJAPaySchema = tLJAPaySchema;
      } // end of if
    } // end of if

    tReturn.add(tAction);
    tReturn.add(tLJAPaySet);
    return tReturn;
  }

  /**
   * 检验附险是否可以签单
   * @param tLCGrpPolSchema
   * @return
   */
  private boolean checkSRiskPolState(LCGrpPolSchema tLCGrpPolSchema) {
    String sql = "select * from LCGrPPol where prtno='" +
        tLCGrpPolSchema.getPrtNo() + "' ";
    sql = sql + " and RiskCode<>'" + tLCGrpPolSchema.getRiskCode() + "'";
    sql = sql + " and Appflag='0'";
    LCGrpPolDB tempLCGrpPolDB = new LCGrpPolDB();
    LCGrpPolSet tLCGrpPolSet = tempLCGrpPolDB.executeQuery(sql);
    if (tLCGrpPolSet != null) {
      for (int n = 1; n <= tLCGrpPolSet.size(); n++) {
        if (checkOneGrp(tLCGrpPolSet.get(n)) == false) {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ProposalSignBL";
          tError.functionName = "preparePolGroup";
          tError.errorMessage = tLCGrpPolSchema.getGrpPolNo() +
              "号投保单的附加险不能签单,原因：（" + mErrors.getFirstError() + "）";
          this.mErrors.addOneError(tError);
          return false;
        }
      }
    }
    return true;
  }

  /**
   * 根据集体投保单号删除磁盘投保日志
   * @param GrpNo
   * @return
   */
  private boolean delDiskInfo(String GrpNo) {
    //数据提交
    GrpPolSignBLS tGrpPolSignBLS = new GrpPolSignBLS();
    if (tGrpPolSignBLS.delDiskInfo(GrpNo) == false) {
      return false;
    }
    return true;
  }
}
