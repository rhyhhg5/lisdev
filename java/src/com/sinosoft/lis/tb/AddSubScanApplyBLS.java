package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 扫描件申请</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class AddSubScanApplyBLS {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 数据操作字符串 */
  private String mOperate;

  public AddSubScanApplyBLS() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT||MAIN"和"INSERT||DETAIL"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    boolean tReturn = false;
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;

    //信息保存
    if(this.mOperate.equals("INSERT||MAIN")) {
      tReturn = save(cInputData);
    }

    if (tReturn)
      System.out.println("Save sucessful");
    else
      System.out.println("Save failed");

    return tReturn;
  }

  //保存操作
  private boolean save(VData mInputData) {
    boolean tReturn =true;
    System.out.println("Start Save...");

    Connection conn=DBConnPool.getConnection();

    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "AddSubScanApplyBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try {
      //开始事务，锁表
      conn.setAutoCommit(false);

      //LCAddPol表
      System.out.println("Start LCAddPol表...");
      LCAddPolDB tLCAddPolDB = new LCAddPolDB(conn);
      tLCAddPolDB.setSchema((LCAddPolSchema)mInputData.getObjectByObjectName("LCAddPolSchema",0));

      //提交前确认该印刷号没有被其他操作员申请
      LCAddPolSchema tLCAddPolSchema = new LCAddPolSchema();
      tLCAddPolSchema.setMainPolNo(tLCAddPolDB.getMainPolNo());
      tLCAddPolSchema.setSerialNo(tLCAddPolDB.getSerialNo());
      String strInputOperator = tLCAddPolSchema.getDB().query().get(1).getInputOperator();
      System.out.println("数据库存的操作员:" + strInputOperator + "提交申请的操作员:" + tLCAddPolDB.getInputOperator());

      if ((strInputOperator != null && !strInputOperator.equals(tLCAddPolDB.getInputOperator())) || !tLCAddPolDB.update()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCAddPolDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AddSubScanApplyBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "LCAddPol表数据保存失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback();
        conn.close();
        return false;
      }

      conn.commit();
      conn.close();
      System.out.println("End Committed");
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "AddSubScanApplyBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try{ conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }

  public static void main(String[] args) {
    AddSubScanApplyBLS AddSubScanApplyBLS1 = new AddSubScanApplyBLS();
  }
}