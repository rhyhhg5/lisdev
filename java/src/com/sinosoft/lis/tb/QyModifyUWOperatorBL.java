package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class QyModifyUWOperatorBL
{
    public CErrors mErrors = new CErrors();
    private String mPrtNo = "";
    private String mAction = "";
    private String mContType = "";
    private String mModifyUWOperator = "";
    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    
    public QyModifyUWOperatorBL() {}
    
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "QyModifyUWOperatorBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("UPDATE")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        
        System.out.println("Begin getInputData");
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("End getInputData");
        
        //修改保单核保师
        if("ModifyUWOperator".equals(mAction))
        {
            if(!ModifyUWOperator(mPrtNo, mModifyUWOperator, mContType))
            {
                buildError("ModifyUWOperator", "修改保单核保师失败！");
                return false;
            }
        }
        
        //将保单放回核保池
        if("ReturnUWPol".equals(mAction))
        {
            if(!ReturnUWPol(mPrtNo, mContType))
            {
                buildError("ReturnUWPol", "将保单放回核保池失败！");
                return false;
            }
        }
        
        VData vData = new VData();
        vData.add(map);
        
        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(vData, "")) {
            mErrors.copyAllErrors(pubsubmit.mErrors);
            return false;
        }
        System.out.println("---End pubsubmit---");
        System.out.println("操作成功！");
        return true;
    }
    
    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));
        mPrtNo = (String)cInputData.getObjectByObjectName("String", 0);
        mAction = (String)cInputData.getObjectByObjectName("String", 2);
        mModifyUWOperator = (String)cInputData.getObjectByObjectName("String", 1);
        mContType = (String)cInputData.getObjectByObjectName("String", 3);
        
        if(mPrtNo == null || "".equals(mPrtNo))
        {
            buildError("getInputData", "获取保单印刷号信息失败！");
            return false;
        }
        
        if(mAction == null || "".equals(mAction))
        {
            buildError("getInputData", "获取操作信息失败！");
            return false;
        }
        
        if(mContType == null || "".equals(mContType))
        {
            buildError("getInputData", "获取保单类型信息失败！");
            return false;
        }
        
        if("ModifyUWOperator".equals(mAction) 
                && (mModifyUWOperator == null || "".equals(mModifyUWOperator)))
        {
            buildError("getInputData", "获取核保师信息失败！");
            return false;
        }
        
        System.out.println(" PrtNo = " + mPrtNo 
                + "\n Action = " + mAction 
                + "\n ModifyUWOperator = " + mModifyUWOperator
                + "\n ContType = " + mContType);
        return true;
    }
    
    //修改保单的核保师
    private boolean ModifyUWOperator(String tPrtNo, String tModifyUWOperator, String tContType)
    {
        String strSQL = "";
        String updateContSQL = "";
        
        if("1".equals(tContType))
        {
            strSQL = "update lwmission set DefaultOperator = '" + tModifyUWOperator + "' "
                + ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
                + ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + " where MissionProp1 = '" + tPrtNo + "' and Activityid = '0000001100' ";
            
            updateContSQL = "update lccont set UwOperator = '" + tModifyUWOperator + "' "
            + ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
            + ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
            + " where PrtNo = '" + tPrtNo + "' ";
        }else if("2".equals(tContType))
        {
            strSQL = "update lwmission set DefaultOperator = '" + tModifyUWOperator + "' "
            + ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
            + ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
            + " where MissionProp2 = '" + tPrtNo + "' and Activityid = '0000002004' ";
            
            updateContSQL = "update lcgrpcont set UwOperator = '" + tModifyUWOperator + "' "
            + ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
            + ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
            + " where PrtNo = '" + tPrtNo + "' ";
        }
        
        if("".equals(strSQL))
        {
            buildError("ModifyUWOperator", "生成修改核保师维护语句失败！");
            return false;
        }
        
        System.out.println("UpdateSQL : " + strSQL);
        System.out.println("ContUpdateSQL : " + updateContSQL);
        map.put(strSQL, "UPDATE");
        map.put(updateContSQL, "UPDATE");
        return true;
    }
    
    //将保单放回核保池
    private boolean ReturnUWPol(String tPrtNo, String tContType)
    {
        String strSQL = "";
        String updateContSQL = "";
        if("1".equals(tContType))
        {
            strSQL = "update lwmission set DefaultOperator = null "
            + ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
            + ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
            + " where MissionProp1 = '" + tPrtNo + "' and Activityid = '0000001100' ";
            
            updateContSQL = "update lccont set UwOperator = null "
            + ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
            + ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
            + " where PrtNo = '" + tPrtNo + "' ";
        }else if("2".equals(tContType))
        {
            strSQL = "update lwmission set DefaultOperator = null "
            + ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
            + ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
            + " where MissionProp2 = '" + tPrtNo + "' and Activityid = '0000002004' ";
            
            updateContSQL = "update lcgrpcont set UwOperator = null "
            + ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
            + ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
            + " where PrtNo = '" + tPrtNo + "' ";
        }
        
        if("".equals(strSQL))
        {
            buildError("ModifyUWOperator", "生成将保单放回核保池的维护语句失败！");
            return false;
        }
        
        System.out.println("UpdateSQL : " + strSQL);
        map.put(strSQL, "UPDATE");
        map.put(updateContSQL, "UPDATE");
        return true;
    }

}
