package com.sinosoft.lis.tb;

import java.util.HashMap;
import java.util.HashSet;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertifyImportLogSchema;
import com.sinosoft.lis.schema.LSInsuredListSchema;
import com.sinosoft.lis.vschema.LSInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class AddTaxList {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	/** 全局变量 */
	private GlobalInput mGlobalInput;
	private VData mResult = new VData();
	/** 批次号 */
	private String mBatchNo = null;
	/** 团体编号 */
	private String mGrpNo = null;
	/** 当前日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	/** 节点名*/
	private String[] sheetName = {"InsuredInfo"};
	/** 配置文件名 */
	private String configName = "TaxDiskImport.xml";
	/** 被保人错误导入时的错误类型 */
	private String errType = "00";
	/** 被保人错误导入时的错误信息 */
	private String errInfo = "正常导入";
	
	private HashMap mKeyIndex = new HashMap();
	private HashSet mSet = new HashSet();
	
	public AddTaxList(){}
	
	public AddTaxList(String BatchNo, GlobalInput gi){
		this.mBatchNo = BatchNo;
		this.mGlobalInput = gi;
	}
	
	/**
	 * 添加传入的多个Sheet数据
	 * @param path，String
	 * @param filename String
	 */
	public boolean doAdd(String path,String fileName){
		//处理批次号，批次号码为文件名（不含扩展名）
		mBatchNo = fileName.substring(0,fileName.lastIndexOf("."));
		String tSQL = "select grpno from lsbatchinfo where batchno='"+mBatchNo+"'";
		mGrpNo = new ExeSQL().getOneValue(tSQL);
		if(mGrpNo==null || "".equals(mGrpNo)){
			this.mErrors.addOneError("获取团体编号失败！");
			return false;
		}
		
		//从磁盘导入数据
		TaxDiskImport importFile = new TaxDiskImport(path+fileName,path+configName,sheetName);
		if(!importFile.doImport()){
			this.mErrors.copyAllErrors(importFile.mErrors);
			return false;
		}
		
		LSInsuredListSet tLSInsuredListSet = (LSInsuredListSet)importFile.getSchemaSet();
		this.mResult.add(tLSInsuredListSet);
		
		//存放Insert Into语句的容器
		MMap map = new MMap();
		if(tLSInsuredListSet.size()>0){
			for(int i=1; i<=tLSInsuredListSet.size(); i++){
				//校验导入数据（依据身份证号判断性别和生日）正确性
				if(checkimportData(tLSInsuredListSet.get(i)) == true){
					addOneInsured(map,tLSInsuredListSet.get(i),i);
				}
				if(errType.equals("12")){
					LICertifyImportLogSchema tLICertifyImportLogSchema = new LICertifyImportLogSchema();
					tLICertifyImportLogSchema.setBatchNo(mBatchNo);
					tLICertifyImportLogSchema.setCardNo(tLSInsuredListSet.get(i).getInsuredId());
					tLICertifyImportLogSchema.setErrorType(errType);
					tLICertifyImportLogSchema.setErrorInfo(errInfo);
					tLICertifyImportLogSchema.setOperator(mGlobalInput.Operator);
					tLICertifyImportLogSchema.setMakeDate(mCurrentDate);
					tLICertifyImportLogSchema.setMakeTime(mCurrentTime);
					tLICertifyImportLogSchema.setModifyDate(mCurrentDate);
					tLICertifyImportLogSchema.setModifyTime(mCurrentTime);
					
					map.put(tLICertifyImportLogSchema, "DELETE&INSERT");
				}else{
					addOneImportLog(map,tLSInsuredListSet.get(i),i);
				}
			}
		}else{
			LICertifyImportLogSchema tLICertifyImportLogSchema = new LICertifyImportLogSchema();
			tLICertifyImportLogSchema.setBatchNo(mBatchNo);
			tLICertifyImportLogSchema.setCardNo("无");
			tLICertifyImportLogSchema.setErrorType("11");
			tLICertifyImportLogSchema.setErrorInfo("没有导入被保人信息，请检查模板！");
			tLICertifyImportLogSchema.setOperator(mGlobalInput.Operator);
			tLICertifyImportLogSchema.setMakeDate(mCurrentDate);
			tLICertifyImportLogSchema.setMakeTime(mCurrentTime);
			tLICertifyImportLogSchema.setModifyDate(mCurrentDate);
			tLICertifyImportLogSchema.setModifyTime(mCurrentTime);
			
			map.put(tLICertifyImportLogSchema, "DELETE&INSERT");
		}
		//提交数据到数据库
		if(!submitData(map)){
			return false;
		}
		//查看lsinsuredlist表中，该批次是否已有导入被保人（可以不是全部被保人）
		//如有，则将lsbatchinfo表中,该团体客户的该批次的记录的batchstate置为1：待内检
		//如无，则提示[导入失败，详见批次错误日志]
		//为防止在数据提交导数据库前就走以下的代码，挂起1秒
		try{
			Thread.sleep(1000);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
		String checkSQL = "select distinct 1 from lsinsuredlist where batchno='"+mBatchNo+"'";
		String isHave = new ExeSQL().getOneValue(checkSQL);
		if("1".equals(isHave)){
			MMap themap = new MMap();
			themap.put("update LSBatchInfo set batchstate='1' where batchno='"+mBatchNo+"' and grpno='"+mGrpNo+"'","UPDATE");
			VData upData = new VData();
			upData.add(themap);
			PubSubmit ps = new PubSubmit();
			if(!ps.submitData(upData, "")){
				mErrors.copyAllErrors(ps.mErrors);
				return false;
			}
		}else{
			buildError("doADD","导入失败，详见批次错误日志");
			return false;
		}
		
		return true;
	}
	
	/**
	 * 校验导入数据的正确性
	 * @return boolean
	 */
	private boolean checkimportData(LSInsuredListSchema aLSInsuredListSchema){
		LSInsuredListSchema tLSInsuredListSchema = aLSInsuredListSchema;
		String tSerilNo = tLSInsuredListSchema.getInsuredId();
		String tName = tLSInsuredListSchema.getName();
		String tSex = tLSInsuredListSchema.getSex();
		String tBirthday = tLSInsuredListSchema.getBirthday();
		String tIdType = tLSInsuredListSchema.getIdType();
		String tIdNo = tLSInsuredListSchema.getIdNo();
		//一、首先是非空校验
		if(tSerilNo==null || "".equals(tSerilNo)){
			errType = "01";
			errInfo = "序号未填写";
			buildError("checkimportData","序号不能为空！");
			return false;
		}
		if(tName==null || "".equals(tName)){
			errType = "02";
			errInfo = "姓名未填写";
			buildError("checkimportData","姓名不能为空！");
			return false;
		}
		if(tSex==null || "".equals(tSex)){
			errType = "03";
			errInfo = "性别未填写";
			buildError("checkimportData","性别不能为空！");
			return false;
		}
		if(tBirthday==null || "".equals(tBirthday)){
			errType = "04";
			errInfo = "生日未填写";
			buildError("checkimportData","生日不能为空！");
			return false;
		}
		if(tIdType==null || "".equals(tIdType)){
			errType = "05";
			errInfo = "证件类型未填写";
			buildError("checkimportData","证件类型不能为空！");
			return false;
		}
		if(tIdNo==null || "".equals(tIdNo)){
			errType = "06";
			errInfo = "证件号码未填写";
			buildError("checkimportData","证件号码不能为空！");
			return false;
		}
		//二、其次是数据匹配校验,当证件类型为0:身份证时，进入以下校验
		if("0".equals(tIdType)){
			//1、身份证号应该是18位
			if(tIdNo.length()!=18){
				errType = "07";
				errInfo = "身份证长度不是18位";
				buildError("checkimportData","身份证号不是18位，请检查!");
				return false;
			}
			//2、身份证号与生日是否匹配
			if( !(tIdNo.substring(6,14).equals(tBirthday.replaceAll("-",""))) ){
				errType = "08";
				errInfo = "身份证号与生日不匹配";
				buildError("checkimportData","身份证号与生日不匹配，请检查!");
				return false;
			}
			//3、身份证号与性别是否匹配
			int the17Bit = Integer.parseInt(String.valueOf(tIdNo.charAt(16)));
			if( (the17Bit%2 == 0 && "0".equals(tSex)) || (the17Bit%2 == 1 && "1".equals(tSex)) ){
				errType = "09";
				errInfo = "身份证号与性别不匹配";
				buildError("checkimportData","身份证号与性别不匹配，请检查!");
				return false;
			}
		}
		//三、对已经导入进系统的数据校验，提示[本批次已导入该被保人，无需重复导入!]。
		String tSQL = "select 1 from lsinsuredlist "
					+ "where batchno='"+mBatchNo+"' "
					+ "and name='"+tName+"' "
					+ "and idno='"+tIdNo+"'";
		String repeatImportFlag = new ExeSQL().getOneValue(tSQL);
		if("1".equals(repeatImportFlag)){
			errType = "10";
			errInfo = "本批次已导入该被保人，无需重复导入！";
			buildError("checkimportData","本批次已导入该被保人，无需重复导入!");
			return false;
		}
		
		//再次增加身份证号信息的校验20170823
		if(!PubFun.CheckIDNo(tIdType, tIdNo, tBirthday, tSex).equals("")){
			errType = "10";
			errInfo = "身份证号信息输入有误！";
			buildError("checkimportData","身份证号信息输入有误!");
			return false;
		}
		
		//重新将errType及errInfo置为正常导入状态
		errType = "00";
		errInfo = "正常导入";
		return true;
	}
	
	/**
	 * 添加一条导入记录
	 * @param map MMap
	 * @param aLSInsredListSchema LSInsredListSchema
	 */
	private void addOneImportLog(MMap map,LSInsuredListSchema aLSInsuredListSchema,int i){
		
		
		
		LSInsuredListSchema tLSInsuredListSchema = aLSInsuredListSchema;
		LICertifyImportLogSchema tLICertifyImportLogSchema = new LICertifyImportLogSchema();
		tLICertifyImportLogSchema.setBatchNo(mBatchNo);
		tLICertifyImportLogSchema.setCardNo(tLSInsuredListSchema.getInsuredId());
		tLICertifyImportLogSchema.setErrorType(errType);
		tLICertifyImportLogSchema.setErrorInfo(errInfo);
		tLICertifyImportLogSchema.setOperator(mGlobalInput.Operator);
		tLICertifyImportLogSchema.setMakeDate(mCurrentDate);
		tLICertifyImportLogSchema.setMakeTime(mCurrentTime);
		tLICertifyImportLogSchema.setModifyDate(mCurrentDate);
		tLICertifyImportLogSchema.setModifyTime(mCurrentTime);
		
		map.put(tLICertifyImportLogSchema, "DELETE&INSERT");
	}
	
	/**
	 * 添加一个被保人
	 * @param map MMap
	 * @param aLSInsredListSchema LSInsredListSchema
	 */
	private void addOneInsured(MMap map,LSInsuredListSchema aLSInsuredListSchema,int i){
		
		LSInsuredListSchema tLSInsuredListSchema = aLSInsuredListSchema;
		
		
		if(mKeyIndex.containsKey(aLSInsuredListSchema.getName())){
			if( (mKeyIndex.get(aLSInsuredListSchema.getName())).equals(aLSInsuredListSchema.getIdNo()) ){
				errType = "12";
				errInfo = "本批次已导入该被保人，无需重复导入！";
			}
		}
		if(!(errType.equals("12"))){
			tLSInsuredListSchema.setBatchNo(mBatchNo);
			tLSInsuredListSchema.setManageCom(mGlobalInput.ComCode);
			tLSInsuredListSchema.setOperator(mGlobalInput.Operator);
			tLSInsuredListSchema.setMakeDate(mCurrentDate);
			tLSInsuredListSchema.setMakeTime(mCurrentTime);
			tLSInsuredListSchema.setModifyDate(mCurrentDate);
			tLSInsuredListSchema.setModifyTime(mCurrentTime);
			
			map.put(tLSInsuredListSchema, "DELETE&INSERT");
		}
		
		mKeyIndex.put(tLSInsuredListSchema.getName(),tLSInsuredListSchema.getIdNo());
	}
	
	/**
	 * 提交数据到数据库
	 * @param map MMap
	 * @return boolean
	 */
	private boolean submitData(MMap map){
		VData tVData = new VData();
		tVData.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if(!tPubSubmit.submitData(tVData, "")){
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
	
	/**
	 * 返回执行结果
	 * @return VData
	 */
	public VData getResult(){
		return this.mResult;
	}
	
	/**
	 * 出错处理
	 * @param szFunc String
	 * @param szErrMsg String
	 */
	private void buildError(String szFunc,String szErrMsg){
		CError cError = new CError();
		cError.moduleName = "AddTaxList";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	/**
	 * main 测试用
	 */
	public static void main(String[] args){
		/* 导入磁盘文件测试  Part1*/
		GlobalInput tGI = new GlobalInput();
		tGI.ComCode = "86110000";
		tGI.Operator = "zc";
		String path = "E:\\CZ\\";
		String fileName = "TAX2015092112234201.xls";
		String batchno = "TAX2015092112234201";
		AddTaxList aaaa = new AddTaxList(batchno,tGI);
		if(!aaaa.doAdd(path,fileName)){
			System.out.println(aaaa.mErrors.getFirstError());
			System.out.println("批次导入失败！");
		}//Part1 End
		
		/* 身份证与生日，性别匹配测试  Part2*/
//		String idno = "522501198407021683";
//		String birthday = "1984-07-02";
//		String sex = "1";
//		String subIdno = idno.substring(6,14);
//		birthday = birthday.replaceAll("-", "");
//		System.out.println("subIdno:"+subIdno+",birthday:"+birthday);
		
//		System.out.println("身份证的长度为："+idno.length());
		
//		int the17Bit = Integer.parseInt(String.valueOf(idno.charAt(16)));
//		System.out.println(the17Bit%2);
//		System.out.println(sex);
//		if( (the17Bit%2 == 0 && "0".equals(sex)) || (the17Bit%2 == 1 && "1".equals(sex)) ){
//			System.out.println("证件号码与性别不匹配，请检查");
//		}else{
//			System.out.println("OK!");
//		}// Part2 End
		
	}
}
