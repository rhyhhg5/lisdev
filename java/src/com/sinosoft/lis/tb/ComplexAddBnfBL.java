package com.sinosoft.lis.tb;

import com.sinosoft.lis.bl.LCAppntBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.Vector;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.cbcheck.RecalculationPremBL;
import com.sinosoft.lis.config.OLDBankRateBL;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: BL层业务逻辑处理类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class ComplexAddBnfBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局数据 */

	private GlobalInput mGlobalInput = new GlobalInput();

	private MMap map = new MMap();

	// 统一更新日期，时间
	private String theCurrentDate = PubFun.getCurrentDate();

	private String theCurrentTime = PubFun.getCurrentTime();

	/** 业务处理相关变量 */
	private LCBnfSchema mLCBnfSchema = new LCBnfSchema();

	// @Constructor
	public ComplexAddBnfBL() {
	}

	/**
	 * 数据提交的公共方法
	 * 
	 * @param: cInputData 传入的数据 cOperate 数据操作字符串
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将传入的数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 将外部传入的数据分解到本类的属性中，准备处理
		if (this.getInputData() == false) {
			return false;
		}

		// 根据业务逻辑对数据进行处理

		if (this.dealData() == false) {
			return false;
		}

		// 装配处理好的数据，准备给后台进行保存
		this.prepareOutputData();
		System.out.println("---prepareOutputData---");

		// 数据提交、保存

		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);

			CError tError = new CError();
			tError.moduleName = "ComplexAddBnfBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		System.out.println("---commitData---");

		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		try {
			// 全局变量
			mGlobalInput.setSchema((GlobalInput) mInputData
					.getObjectByObjectName("GlobalInput", 0));
			// 合同表
			mLCBnfSchema.setSchema((LCBnfSchema) mInputData
					.getObjectByObjectName("LCBnfSchema", 0));
			if ("".equals(mLCBnfSchema.getContNo())
					|| "".equals(mLCBnfSchema.getPolNo())) {
				CError tError = new CError();
				tError.moduleName = "getInputData";
				tError.functionName = "getInputData";
				tError.errorMessage = "未获取到补录受益人信息!";
				this.mErrors.addOneError(tError);
			}
		} catch (Exception ex) {
			CError tError = new CError();
			tError.moduleName = "ProposalBL";
			tError.functionName = "checkData";
			tError.errorMessage = ex.toString();
			this.mErrors.addOneError(tError);
			return false;

		}
		return true;

	}

	/**
	 * 根据业务逻辑对数据进行处理
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		LCBnfDB tLCBnfDB = new LCBnfDB();
		LCBnfSet tLCBnfSet = tLCBnfDB
				.executeQuery("select * from lcbnf where contno='"
						+ mLCBnfSchema.getContNo() + "' and polno='"
						+ mLCBnfSchema.getPolNo() + "' and bnftype='"
						+ mLCBnfSchema.getBnfType() + "' and bnfno='"
						+ mLCBnfSchema.getBnfNo() + "'");
		if(tLCBnfSet.size()!=1){
			CError tError = new CError();
			tError.moduleName = "getInputData";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取补录受益人基本信息失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		LCBnfSchema tLCBnfSchema = new LCBnfSchema();
		tLCBnfSchema=tLCBnfSet.get(1);
		if("UPDATE||MAIN".equals(mOperate)){
			tLCBnfSchema.setSex(mLCBnfSchema.getSex());
			tLCBnfSchema.setNativePlace(mLCBnfSchema.getNativePlace());
			tLCBnfSchema.setOccupationCode(mLCBnfSchema.getOccupationCode());
			tLCBnfSchema.setPhone(mLCBnfSchema.getPhone());
			tLCBnfSchema.setPostalAddress(mLCBnfSchema.getPostalAddress());
			tLCBnfSchema.setIDStartDate(mLCBnfSchema.getIDStartDate());
			tLCBnfSchema.setIDEndDate(mLCBnfSchema.getIDEndDate());
		}else{
			tLCBnfSchema.setSex("");
			tLCBnfSchema.setNativePlace("");
			tLCBnfSchema.setOccupationCode("");
			tLCBnfSchema.setPhone("");
			tLCBnfSchema.setPostalAddress("");
			tLCBnfSchema.setIDStartDate("");
			tLCBnfSchema.setIDEndDate("");			
		}
		tLCBnfSchema.setModifyDate(theCurrentDate);
		tLCBnfSchema.setModifyTime(theCurrentTime);
		map.put(tLCBnfSchema, "UPDATE");
		
		return true;
	}

	/**
	 * 根据业务逻辑对数据进行处理
	 * 
	 */
	private void prepareOutputData() {
		mInputData.clear();
		mInputData.add(map);
	}

	/**
	 * 得到处理后的结果集
	 * 
	 * @return 结果集
	 */

	public VData getResult() {
		return mResult;
	}

}
