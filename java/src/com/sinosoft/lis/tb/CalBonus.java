package com.sinosoft.lis.tb;

import java.util.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.EdorCalZT;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import java.text.*;
import java.sql.Connection;
/**
 * <p>Title: </p>
 * <p>Description:会计年度末计算红利 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class CalBonus
{

    public CErrors mErrors = new CErrors();
    private int COUNT=100; // 提交纪录数
    private GlobalInput mGlobalInput =new GlobalInput() ;
    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();

    /*转换精确位数的对象   */
    private String FORMATMODOL="0.00";//保费保额计算出来后的精确位数
    private DecimalFormat mDecimalFormat=new DecimalFormat(FORMATMODOL);//数字转换对象

    public CalBonus()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput=new GlobalInput();
        tGlobalInput.ManageCom="86110000";
        tGlobalInput.Operator="001";
        tGlobalInput.ComCode="86110000";
        CalBonus calBonus1 = new CalBonus();
        //1-设置全局变量
        calBonus1.setGlobalInput(tGlobalInput);
        //2-校验红利分配的状态
        //calBonus1.verifyBonusStatus("");
        //3-红利计算
        calBonus1.realCalBonus("2003");



    }

    /**
     * 接收传入的数据
     * @param tGI
     * @return
     */
    public boolean setGlobalInput(GlobalInput tGI)
    {
        if(tGI==null)
        {
            CError tCError = new CError();
            tCError.functionName="getGlobalInput";
            tCError.moduleName="CalBonus";
            tCError.errorMessage="请传入GlobalInput数据！";
            mErrors.addOneError(tCError);
        }
        mGlobalInput=tGI;
        return true;
    }

    /**
     * 校验红利分配的状态，为计算做准备
     * @param AccountYear :接收传入的会计年度值，如果为空，取系统值
     * @return
     */
    public boolean verifyBonusStatus(String AccountYear)
    {
        String calYear=AccountYear;
        if(calYear==null||calYear.equals(""))
        {
            calYear=StrTool.getYear();
        }

        LOBonusMainSet needUpdateLOBonusMainSet=new LOBonusMainSet();

        //1-查询LMBonusGroup
        LMBonusGroupDB tLMBonusGroupDB=new LMBonusGroupDB();
        String strSQL="select * from LMBonusGroup";
        LMBonusGroupSet tLMBonusGroupSet=tLMBonusGroupDB.executeQuery(strSQL);
        if(tLMBonusGroupSet.size()==0)
            return true;


        LCPolSet tLCPolSet=new LCPolSet();
        LOBonusMainSchema tLOBonusMainSchema=new LOBonusMainSchema();
        for(int n=1;n<=tLMBonusGroupSet.size();n++)
        {
            //2-根据红利分配组别描述表的条件简单查询：不包括失效保单的校验
            strSQL="select count(*) from lcpol where appflag='1' and "+tLMBonusGroupSet.get(n).getBonusCondition();
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS=tExeSQL.execSQL(strSQL);
            String strCount=tSSRS.GetText(1,1);
            int iCount = Integer.parseInt(strCount);
            if(iCount==0)
            {
               continue;
            }

            //3-查询该组号的红利分配主表
            LOBonusMainDB tLOBonusMainDB=new LOBonusMainDB();
            tLOBonusMainDB.setGroupID(tLMBonusGroupSet.get(n).getGroupID());
            tLOBonusMainDB.setFiscalYear(calYear);
            if(tLOBonusMainDB.getInfo()==false)
            {
                continue;
            }
            tLOBonusMainSchema=tLOBonusMainDB.getSchema(); //???校验
            //如果红利分配主表的状态为初始状态，那么检查该组的可分配盈余和该组的红利分配比例是否存在.否则下一循环
            if(tLOBonusMainSchema.getComputeState()==null||tLOBonusMainSchema.getComputeState().equals("0"))
            {
                if(tLOBonusMainSchema.getDistributeRate()>0&&tLOBonusMainSchema.getDistributeValue()>0)
                {
                    //将校验条件中的单引号替换为两个单引号，为了做数据库的操作
                    String strCondition=StrTool.replace(tLMBonusGroupSet.get(n).getBonusCondition(),"'","''");
                    tLOBonusMainSchema.setComputeState("1");//校验结束
                    tLOBonusMainSchema.setBonusGroupType(tLMBonusGroupSet.get(n).getBonusGroupType());
                    tLOBonusMainSchema.setBonusCondition(strCondition);
                    tLOBonusMainSchema.setModifyDate(PubFun.getCurrentDate());
                    tLOBonusMainSchema.setModifyTime(PubFun.getCurrentTime());
                    //将校验结束的单条纪录加入集合，后面更新
                    needUpdateLOBonusMainSet.add(tLOBonusMainSchema);
                }
                else
                {
                    continue;
                }
            }
            else
            {
                continue;
            }
        }

        //4-update LOBonusMain
        Connection conn = DBConnPool.getConnection();
        LOBonusMainDBSet tLOBonusMainDBSet = new LOBonusMainDBSet();
        tLOBonusMainDBSet.set(needUpdateLOBonusMainSet);
        if(tLOBonusMainDBSet.update()==false)
        {
            mErrors.copyAllErrors(tLOBonusMainDBSet.mErrors);
            CError tCError = new CError();
            tCError.functionName="verifyBonusStatus";
            tCError.moduleName="CalBonus";
            tCError.errorMessage="更新红利分配主表：LOBonusMain失败！";
            mErrors.addOneError(tCError);
            return false;
        }
        return true;
    }

    /**
     * 计算各个组的红利系数和个人保单的红利金额
     * @param AccountYear:接收传入的会计年度值，如果为空，取系统值
     * @return
     */
    public boolean realCalBonus(String AccountYear)
    {

        String calYear=AccountYear;
        if(calYear==null||calYear.equals(""))
        {
            calYear=StrTool.getYear();
        }
        String tPolNo = "";
        String tContNo = "";
        String tGrpPolNo = "";
        String tRiskCode = "";
        String tPayToDate = "";
        String tCValiDate = "";
        String tPayIntv = "";
        String tEndDate = "";
        EdorCalZT tEdorCalZT=new EdorCalZT();//用于校验保单是否失效的类

        //查询红利分配主表,找出状态为 1-校验结束 或 2-计算开始 的数据
        LOBonusMainDB tLOBonusMainDB = new LOBonusMainDB();
        String strSQL="select * from LOBonusMain where computeState='1' or computeState='2' ";
        LOBonusMainSet tLOBonusMainSet=tLOBonusMainDB.executeQuery(strSQL);
//        if(tLOBonusMainSet.size()==0)
//            return true;
        LOBonusMainSchema tLOBonusMainSchema=new LOBonusMainSchema();
        for(int n=1;n<=tLOBonusMainSet.size();n++)
        {
            tLOBonusMainSchema=tLOBonusMainSet.get(n);
            if(tLOBonusMainSchema.getDistributeRate()<=0||tLOBonusMainSchema.getDistributeValue()<=0||tLOBonusMainSchema.getBonusCondition()==null)
            {
                CError tCError = new CError();
                tCError.functionName="realCalBonus";
                tCError.moduleName="CalBonus";
                tCError.errorMessage="组号为"+tLOBonusMainSchema.getGroupID()+" 红利分配主表的关键数据有误!";
                mErrors.addOneError(tCError);
                continue;
            }

            String StartDate=tLOBonusMainSchema.getFiscalYear()+"-1-1";
            String EndDate=tLOBonusMainSchema.getFiscalYear()+"-12-31";
            //根据红利分配组别描述表的条件简单查询且签单并且不在保单红利表中，注意不包括失效保单的校验
            strSQL="select count(*) from lcpol where appflag='1' and "+tLOBonusMainSchema.getBonusCondition()+" and cvalidate>='"+StartDate+"' and cvalidate<='"+EndDate+"'" ;
            strSQL=strSQL+" and polno not in (select polno from LOBonusPol where FiscalYear="+tLOBonusMainSchema.getFiscalYear()+" and GroupID="+tLOBonusMainSchema.getGroupID()+")";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS=tExeSQL.execSQL(strSQL);
            String strCount=tSSRS.GetText(1,1);
            int iCount = Integer.parseInt(strCount);
            if(iCount==0)
            {
                continue;
            }
            //如果状态是1-校验结束，则更新该红利分配主表的状态为2-计算开始:便于下次使用
            if(tLOBonusMainSchema.getComputeState().equals("1"))
            {
                strSQL="update LOBonusMain set ComputeState='2',ModifyDate='"+PubFun.getCurrentDate()+"',ModifyTime='"+PubFun.getCurrentTime()+"'";
                strSQL=strSQL+" where GroupID="+tLOBonusMainSchema.getGroupID()+" and FiscalYear="+tLOBonusMainSchema.getFiscalYear();
                tExeSQL = new ExeSQL();
                if(tExeSQL.execUpdateSQL(strSQL)==false)
                {
                    mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tCError = new CError();
                    tCError.functionName="realCalBonus";
                    tCError.moduleName="CalBonus";
                    tCError.errorMessage="更新LOBonusMain表数据失败!";
                    mErrors.addOneError(tCError);
                    return false;
                }
            }

            int NUM=1;
            strSQL="select PolNo,ContNo,GrpPolNo,RiskCode,PayToDate,CValiDate,PayIntv,EndDate from lcpol where appflag='1' and "+tLOBonusMainSchema.getBonusCondition()+" and cvalidate>='"+StartDate+"' and cvalidate<='"+EndDate+"'" ;;
            strSQL=strSQL+" and polno not in (select polno from LOBonusPol where FiscalYear="+tLOBonusMainSchema.getFiscalYear()+" and GroupID="+tLOBonusMainSchema.getGroupID()+") order by CValiDate";
            //用于保存LOBonusPol数据
            LOBonusPolSet saveLOBonusPolSet=new LOBonusPolSet();
            LOBonusPolSchema saveLOBonusPolSchema=new LOBonusPolSchema();
            //如果基数大与个人保单纪录数，跳出循环-需要测试
            LOBonusPolDBSet tLOBonusPolDBSet=new LOBonusPolDBSet();
            int iStart=1;
            int iAccount=0;
            while(NUM<=iCount)
            {
                NUM=NUM+COUNT;

                tExeSQL = new ExeSQL();
                //1-每次取出查询结果集 1 到 COUNT 条,因为查询结果总是变换和减少的。因为LOBonusPol中的个人保单数据越来越多
                //2-可能当前查询的数据有iAccount条没有成功，这样需要向后跳iAccount条
                iStart=iStart+iAccount;
                System.out.println("1-- "+iAccount);
                System.out.println("2-- "+iStart);

                tSSRS=tExeSQL.execSQL(strSQL,iStart,COUNT);
                iAccount=tSSRS.getMaxRow();
                for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
                {
                    tPolNo = tSSRS.GetText(i,1);
                    tContNo = tSSRS.GetText(i,2);
                    tGrpPolNo = tSSRS.GetText(i,3);
                    tRiskCode = tSSRS.GetText(i,4);
                    tPayToDate = tSSRS.GetText(i,5);
                    tCValiDate = tSSRS.GetText(i,6);
                    tPayIntv = tSSRS.GetText(i,7);
                    tEndDate = tSSRS.GetText(i,8);

                    //如果当前保单失效，下一循环
                    if(tEdorCalZT.JudgeLapse(tRiskCode,tPayToDate,PubFun.getCurrentDate(),tPayIntv,tEndDate)==true)
                    {
                        continue;
                    }
                    //计算红利函数--计算单个保单的红利系数--??是否可以为0
                    double PolBonusQuotiety=calPolBonusQuotiety(tPolNo) ;
                    if(PolBonusQuotiety==0)
                        continue;

                    //计算保单年度值--:不满一年的会加 1 --润年问题
                    int PolYear=PubFun.calPolYear(tCValiDate,PubFun.getCurrentDate());
                    //计算实际红利应该分配日期--校验
                    FDate tD=new FDate();
                    Date baseDate =tD.getDate(tCValiDate);
                    Date SGetDate=PubFun.calDate(baseDate,PolYear,"Y",null);
                    String strSGetDate=String.valueOf(SGetDate);//调试用
                    //准备数据
                    saveLOBonusPolSchema=new LOBonusPolSchema();
                    saveLOBonusPolSchema.setPolNo(tPolNo);
                    saveLOBonusPolSchema.setContNo(tContNo);
                    saveLOBonusPolSchema.setGrpPolNo(tGrpPolNo);
                    saveLOBonusPolSchema.setFiscalYear(calYear);
                    saveLOBonusPolSchema.setBonusCoef(PolBonusQuotiety);
                    saveLOBonusPolSchema.setBonusFlag("0");    //红利领取标记
                    saveLOBonusPolSchema.setSGetDate(SGetDate);//红利应该分配日期
                    saveLOBonusPolSchema.setOperator(mGlobalInput.Operator);
                    saveLOBonusPolSchema.setMakeDate(PubFun.getCurrentDate());
                    saveLOBonusPolSchema.setMakeTime(PubFun.getCurrentTime());
                    saveLOBonusPolSchema.setModifyDate(PubFun.getCurrentDate());
                    saveLOBonusPolSchema.setModifyTime(PubFun.getCurrentTime());
                    saveLOBonusPolSchema.setGroupID(tLOBonusMainSchema.getGroupID());
                    saveLOBonusPolSet.add(saveLOBonusPolSchema);
                    iAccount=iAccount-1;
                }
                //数据批量提交
                tLOBonusPolDBSet.set(saveLOBonusPolSet);
                System.out.println("红利保单插入");
                if(tLOBonusPolDBSet.insert()==false)
                {
                    mErrors.copyAllErrors(tLOBonusPolDBSet.mErrors);
                    CError tCError = new CError();
                    tCError.functionName="realCalBonus";
                    tCError.moduleName="CalBonus";
                    tCError.errorMessage="更新LOBonusPol表数据失败!";
                    mErrors.addOneError(tCError);
                    return false;
                }
                tLOBonusPolDBSet.clear();
                saveLOBonusPolSet.clear();
            }
            //计算总的红利系数--根据组别查询保单红利表，求和
            double sumBonusQuotiety=calSumPolBonusQuotiety(tLOBonusMainSchema.getGroupID());
            //更新红利分配主表--更新计算状态 3 C 值计算结束和红利系数和
            strSQL="update LOBonusMain set ComputeState='3',ModifyDate='"+PubFun.getCurrentDate()+"',";
            strSQL=strSQL+"ModifyTime='"+PubFun.getCurrentTime()+"',BonusCoefSum="+sumBonusQuotiety;
            strSQL=strSQL+" where GroupID="+tLOBonusMainSchema.getGroupID()+" and FiscalYear="+tLOBonusMainSchema.getFiscalYear();
            tExeSQL = new ExeSQL();
            if(tExeSQL.execUpdateSQL(strSQL)==false)
            {
                mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tCError = new CError();
                tCError.functionName="realCalBonus";
                tCError.moduleName="CalBonus";
                tCError.errorMessage="更新LOBonusMain表数据失败!";
                mErrors.addOneError(tCError);
                return false;
            }
        }

        //根据组别状态为3--C 值计算结束的分红主表，计算单个保单下的红利金额--函数-传入组别红利系数总和
        tLOBonusMainDB = new LOBonusMainDB();
        strSQL="select * from LOBonusMain where computeState='3' ";
        tLOBonusMainSet=tLOBonusMainDB.executeQuery(strSQL);
        if(tLOBonusMainSet.size()==0)
            return true;
        for(int n=1;n<=tLOBonusMainSet.size();n++)
        {
            tLOBonusMainSchema=tLOBonusMainSet.get(n);
            if(calPolBonusMoney(tLOBonusMainSchema.getGroupID(),calYear)==false)
            {
                CError tCError = new CError();
                tCError.functionName="realCalBonus";
                tCError.moduleName="CalBonus";
                tCError.errorMessage="计算个人保单红利失败!";
                mErrors.addOneError(tCError);
                return false;
            }
            else
            {
                //红利分配主表更新计算状态
                strSQL="update LOBonusMain set ComputeState='5',ModifyDate='"+PubFun.getCurrentDate()+"',";
                strSQL=strSQL+"ModifyTime='"+PubFun.getCurrentTime()+"'";
                strSQL=strSQL+" where GroupID="+tLOBonusMainSchema.getGroupID()+" and FiscalYear="+tLOBonusMainSchema.getFiscalYear();
                ExeSQL tExeSQL = new ExeSQL();
                if(tExeSQL.execUpdateSQL(strSQL)==false)
                {
                    mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tCError = new CError();
                    tCError.functionName="realCalBonus";
                    tCError.moduleName="CalBonus";
                    tCError.errorMessage="更新LOBonusMain表数据失败!";
                    mErrors.addOneError(tCError);
                    return false;
                }
            }

        }
        return true;
    }

    /**
     * 计算个人保单下的红利系数--增额交清的保额不记入
     * @param PolNo
     * @return
     */
    public double calPolBonusQuotiety(String PolNo)
    {
        LCPolDB tLCPolDB=new LCPolDB();
        tLCPolDB.setPolNo(PolNo);
        if(tLCPolDB.getInfo()==false)
        {
            CError tCError = new CError();
            tCError.functionName="realCalBonus";
            tCError.moduleName="CalBonus";
            tCError.errorMessage="查询个人保单表失败!";
            mErrors.addOneError(tCError);
            return 0;
        }
        LCPolSchema tLCPolSchema=tLCPolDB.getSchema();
        LMRiskBonusSchema tLMRiskBonusSchema=mCRI.findRiskBonusByRiskCode(tLCPolSchema.getRiskCode());
        if(tLMRiskBonusSchema==null||tLMRiskBonusSchema.getBonusCoefCode()==null)
        {
            return 0;
        }
        //计算保单年度值--需要校验:即不满一年的需要加 1 --润年问题
        int PolYear=PubFun.calPolYear(tLCPolSchema.getCValiDate(),PubFun.getCurrentDate());
        //求出正常分红的保额，即剔除增额交清部分的保额,保费
        String strSQL="select NVL(SUM(Amnt),0),NVL(SUM(StandPrem),0) from lcduty where polno='"+tLCPolSchema.getPolNo()+"' and length(trim(dutycode))=6 ";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS=tExeSQL.execSQL(strSQL);
        String actuAmnt=tSSRS.GetText(1,1);
        String actuPrem=tSSRS.GetText(1,2);

        CalBase mCalBase = new CalBase();
        mCalBase.setAmnt(Double.parseDouble(actuAmnt));
        mCalBase.setPrem(Double.parseDouble(actuPrem));

        mCalBase.setAppAge( tLCPolSchema.getInsuredAppAge() );
        mCalBase.setSex(tLCPolSchema.getInsuredSex());
        mCalBase.setPayEndYear(tLCPolSchema.getPayEndYear());
        mCalBase.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag());
        mCalBase.setPayIntv(tLCPolSchema.getPayIntv());

        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode( tLMRiskBonusSchema.getBonusCoefCode());
        //增加基本要素
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem() );
        mCalculator.addBasicFactor("Amnt", mCalBase.getAmnt() );
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge() );
        mCalculator.addBasicFactor("Sex", mCalBase.getSex() );
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear() );
        mCalculator.addBasicFactor("PayEndYearFlag", mCalBase.getPayEndYearFlag() );
        mCalculator.addBasicFactor("PolYear",String.valueOf(PolYear));//添加保单年度值
        mCalculator.addBasicFactor("PayIntv",mCalBase.getPayIntv());

        String tStr = "";
        double mValue;
        tStr = mCalculator.calculate() ;
        if (tStr.trim().equals(""))
            mValue = 0;
        else
            mValue = Double.parseDouble( tStr );

        //格式化，取两位精度-校验
        String strCalPrem=mDecimalFormat.format(mValue);//转换计算后的保费(规定的精度)
        mValue=Double.parseDouble(strCalPrem);

        return mValue;
    }

    /**
     * 计算某组别下所有个人保单的系数和
     * @param GroupID 组别
     * @return 系数和
     */
    public double calSumPolBonusQuotiety(int GroupID)
    {
        String strSQL="select sum(BonusCoef) from LOBonusPol where GroupID="+GroupID+"";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS=tExeSQL.execSQL(strSQL);
        String strSumValue=tSSRS.GetText(1,1);
        double SumValue=Double.parseDouble(strSumValue);
        //格式化，取两位精度-校验
        String strCalPrem=mDecimalFormat.format(SumValue);//转换计算后的保费(规定的精度)
        double sumValue=Double.parseDouble(strCalPrem);
        return sumValue;
    }

    /**
     * 计算某组别下每个个人保单的红利金额
     * @param GroupID 组别
     * @param AccountYear 会计年度值
     * @return
     */
    public boolean calPolBonusMoney(int GroupID,String AccountYear)
    {
        //1-根据组别查询红利分配主表LOBonusMain，得到红利结算时刻的红利系数和BonusCoefSum
        LOBonusMainDB tLOBonusMainDB=new LOBonusMainDB();
        tLOBonusMainDB.setGroupID(GroupID);
        tLOBonusMainDB.setFiscalYear(AccountYear);
        if(tLOBonusMainDB.getInfo()==false)
        {
            mErrors.copyAllErrors(tLOBonusMainDB.mErrors);
            CError tCError = new CError();
            tCError.functionName="calPolBonusMoney";
            tCError.moduleName="CalBonus";
            tCError.errorMessage="查询红利分配主表失败!";
            mErrors.addOneError(tCError);
            return false;
        }
        //如果计算状态不是3，那么不计算跳出--??是否应该返回错误
        if(!tLOBonusMainDB.getComputeState().endsWith("3"))
        {
            return true;
        }
        double sumBonusQuotiety=tLOBonusMainDB.getBonusCoefSum();//该组的红利分配系数和
        double distributeValue =tLOBonusMainDB.getDistributeValue();//该组的可分配盈余
        double distributeRate=tLOBonusMainDB.getDistributeRate();//该组的红利分配比例
        //2 查询该组别下的所有个人单，从保单红利表LOBonusPol查询--从效率考虑
        String strSQL="select count(*) from LOBonusPol where GroupID="+GroupID;
        strSQL=strSQL+" and FiscalYear="+AccountYear;
        strSQL=strSQL+" and (BonusMakeDate is null or BonusMakeDate='')"; //即该个人保单尚未计算红利金额
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS=tExeSQL.execSQL(strSQL);
        String strCount=tSSRS.GetText(1,1);
        int iCount = Integer.parseInt(strCount);
        int NUM=1;
        if(iCount==0)
        {
            return true;
        }
        String strSQLSel="select PolNo,BonusCoef from LOBonusPol where GroupID="+GroupID;
        strSQLSel=strSQLSel+" and FiscalYear="+AccountYear;
        strSQLSel=strSQLSel+" and (BonusMakeDate is null or BonusMakeDate='')"; //即该个人保单尚未计算红利金额
        //3 根据该数计算个单的红利金额
        String tPolNo="";        //保单号
        String strBonusCoef="";  //个人保单的红利系数-字符串
        double tBonusCoef=0.0;   //个人保单的红利系数-
        String strBonusMoney=""; //个人保单的红利金额
        double tBonusMoney=0.0;

        while(NUM<=iCount)
        {
            NUM=NUM+COUNT;
            tExeSQL = new ExeSQL();
            System.out.println("NUM:"+NUM);
            //纪录集合的数据在减少，因为保单红利表的更新的数据BonusMakeDate不再为空
            tSSRS=tExeSQL.execSQL(strSQLSel,1,COUNT);

            if(tSSRS==null)
            {
                CError tError = new CError();
                tError.moduleName = "CalBonus";
                tError.functionName = "calPolBonusMoney";
                tError.errorMessage = "保单红利表读取失败！";
                this.mErrors.addOneError( tError ) ;
                return false;
            }
            for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
            {
                tPolNo=tSSRS.GetText(i,1);
                strBonusCoef=tSSRS.GetText(i,2);
                tBonusCoef=Double.parseDouble(strBonusCoef);
                //红利金额计算公式
                tBonusMoney=tBonusCoef/sumBonusQuotiety*distributeValue*distributeRate;
                //格式化，取两位精度-校验
                strBonusMoney=mDecimalFormat.format(tBonusMoney);//转换计算后的保费(规定的精度)
                tBonusMoney=Double.parseDouble(strBonusMoney);

                strSQL="update LOBonusPol set BonusMoney="+tBonusMoney+",BonusMakeDate='"+PubFun.getCurrentDate()+"',";
                strSQL=strSQL+ " ModifyDate='"+PubFun.getCurrentDate()+"',ModifyTime='"+PubFun.getCurrentTime()+"'";
                strSQL=strSQL+ "where PolNo='"+tPolNo+"' and FiscalYear="+AccountYear+" and GroupID="+GroupID;
                tExeSQL = new ExeSQL();
                if(tExeSQL.execUpdateSQL(strSQL)==false)
                {
                    mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalBonus";
                    tError.functionName = "calPolBonusMoney";
                    tError.errorMessage = "更新保单红利表读取失败！";
                    this.mErrors.addOneError( tError ) ;
                    return false;
                }
            }

        }
        return true;
    }

}