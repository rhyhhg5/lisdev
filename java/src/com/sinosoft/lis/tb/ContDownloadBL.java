package com.sinosoft.lis.tb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ContDownloadBL {

	public CErrors mErrors = new CErrors();
	
	private String mManageCom = null;
	private String mStartDate = null;
	private String mEndDate = null;
	
	private String mFilePath = null;
	
	public String downloadFile(VData cInputData){

		if(!getInputData(cInputData)){
			return null;
		}
		
		if(!dealData()){
			return null;
		}
		
		return mFilePath;
	}
	
	private boolean dealData() {

		ExeSQL tExeSQL = new ExeSQL();
		
		String tUIRoot = tExeSQL.getOneValue("select SysVarValue from LDSysVar where SysVar = 'UIRoot' ");
		if(tUIRoot == null || tUIRoot.equals("")){
			mErrors.addOneError("没配置应用路径，请联系IT相关人员处理");
			return false;
		}
		mFilePath = tUIRoot + "temp/" + "XXNH02" 
			+ PubFun.getCurrentDate2() + PubFun.getCurrentTime2() 
            + "_" + System.currentTimeMillis() + ".TXT";
		
		String sql = "select a.CustomerNo,  "
			+ "  case c.RiskType when 'A' then '1' when 'H' then '2' else '3' end, "
			+ "	 a.RiskCode,  "
			+ "	 c.RiskName,  "
			+ "	 a.GrpContNo,  "
			+ "	 a.ManageCom, (select Name from LDCom c where c.ComCode = a.ManageCom), "
			+ "	 a.AgentCode, (select Name from LAAgent c where c.AgentCode = a.AgentCode), "
			+ "	 b.Mobile,  "
			+ "	 a.CValidate,  "
            + "  lgc.CInValidate, "
			+ "	 a.Prem,  "
			+ "	 a.Peoples2 "
			+ "from LCGrpPol a, LCGrpCont lgc, LAAgent b, LMRiskApp c "
			+ "where a.AgentCode = b.AgentCode "
            + "  and lgc.grpcontno = a.grpcontno "
            + "  and a.appflag not in ('0','9')"
			+ "	 and a.RiskCode = c.RiskCode "
			+ "	 and a.ManageCom like '" + mManageCom + "%' "
			+ "	 and exists(select 1 from LCGrpCont where GrpContNo = a.GrpContNo and SignDate between '" + mStartDate + "' and '" + mEndDate + "') "
			+ "with ur ";
		
		//File tContFile = new File("D:\\XXNH02" + PubFun.getCurrentDate2() + PubFun.getCurrentTime2() + ".TXT");
		File tContFile = new File(mFilePath);
		
		int tStart = 1;
		int length = 5000;
		SSRS tSSRS = tExeSQL.execSQL(sql, tStart, length);
		
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(tContFile));
			while(tSSRS.getMaxRow() > 0){
				for(int row = 1; row <= tSSRS.getMaxRow(); row++){
					for(int col = 1; col <= tSSRS.getMaxCol(); col++){
						writer.write(tSSRS.GetText(row, col) + "|!");
					}
					//writer.newLine();
                    writer.write("\r\n");//写入换行   
				}
				
				tStart += length;
				tSSRS = tExeSQL.execSQL(sql, tStart, length);
			}

			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			mErrors.addOneError("生产等文件错误");
			return false;
		}
		
		return true;
	}

	private boolean getInputData(VData cInputData) {

		TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
		
		mManageCom = (String)tTransferData.getValueByName("ManageCom");
		mStartDate = (String)tTransferData.getValueByName("StartDate");
		mEndDate = (String)tTransferData.getValueByName("EndDate");
		
		if(mManageCom == null
				|| mStartDate == null
				|| mEndDate == null){
			mErrors.addOneError("传入的数据不完整");
			return false;
		}
		
		return true;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
