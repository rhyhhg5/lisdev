package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class LCAssistSalechnlBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();


	//录入
	private LCExtendSchema mLCExtendSchema = new LCExtendSchema();


	private MMap mMap = new MMap();

	public LCAssistSalechnlBL() {
	}

	public MMap submitData(VData cInputData, String cOperate) {
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return null;
		}

		//进行业务处理
		if (!dealData(cOperate)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return null;
		}

		return mMap;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mLCExtendSchema = (LCExtendSchema) cInputData.getObjectByObjectName("LCExtendSchema", 0);
		this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		
		if(this.mLCExtendSchema == null){
			return false;
		}

		return true;

	}

	private boolean dealData(String cOperate) {
		if("DELETE".equals(cOperate)){
			
			String tPrtNo = mLCExtendSchema.getPrtNo();
			String tSQL = "select serno from LCExtend where prtno = '"+tPrtNo+"' ";
			String tSerNo = new ExeSQL().getOneValue(tSQL);
			if(tSerNo != null && !"".equals(tSerNo)){
				String tDelSQL = "delete from LCExtend where serno = '"+tSerNo+"'";
				mMap.put(tDelSQL, SysConst.DELETE);
			}
			
		} else {
			
			String tPrtNo = mLCExtendSchema.getPrtNo();
			String tSQL = "select serno from LCExtend where prtno = '"+tPrtNo+"' ";
			String tSerNo = new ExeSQL().getOneValue(tSQL);
			if(tSerNo != null && !"".equals(tSerNo)){
				String tDelSQL = "delete from LCExtend where serno = '"+tSerNo+"'";
				mMap.put(tDelSQL, SysConst.DELETE);
			}
			if(mLCExtendSchema.getAssistSalechnl() != null && !"".equals(mLCExtendSchema.getAssistSalechnl())){
				String tNewSerNo = PubFun1.CreateMaxNo("ExtendSerNo", 20);
				mLCExtendSchema.setSerNo(tNewSerNo);
				mLCExtendSchema.setContFlag("2");
				mLCExtendSchema.setManageCom(mGlobalInput.ManageCom);
				mLCExtendSchema.setOperator(mGlobalInput.Operator);
				mLCExtendSchema.setMakeDate(CurrentDate);
				mLCExtendSchema.setMakeTime(CurrentTime);
				mLCExtendSchema.setModifyDate(CurrentDate);
				mLCExtendSchema.setModifyTime(CurrentTime);
				mMap.put(mLCExtendSchema, SysConst.INSERT);
			}
		}
		return true;

	}
}
