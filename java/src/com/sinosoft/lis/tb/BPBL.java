package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class BPBL {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();
    private VData mInputData2 = new VData();
    private VData updateData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 提交数据的容器 */
    private MMap map = new MMap();
    private MMap map2 = new MMap();
    private MMap updateMap = new MMap();

    /** 公共输入信息 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();
    
    //是否要存储大项目信息标记："Y",是；"N",不是。
    private String isNewBPInfo = "";
    //是否要存储大项目年度标记:"Y",是；"N",不是。
    private String isNewBPYear = "";
    //是否存在修改已有大项目名称的标记:
    private String toUpdate = "";
    
    private LCBigProjectInfoSchema tLCBigProjectInfoSchema = new LCBigProjectInfoSchema();
    private LCBigProjectContSchema tLCBigProjectContSchema = new LCBigProjectContSchema();
    private LCBigProjectContTrackSchema tLCBigProjectContTrackSchema = new LCBigProjectContTrackSchema();
    private LCBigProjectYearSchema tLCBigProjectYearSchema = new LCBigProjectYearSchema();
    
    public BPBL(){}
    
    public boolean submitData(VData cInputData, String cOperate) {
    	
    	mOperate = cOperate;
    	mInputData = (VData)cInputData.clone();
    	
    	if(!getInputData(mInputData)){
            return false;
        }

        if(!checkData()){
            return false;
        }

        if(!dealData()){
            return false;
        }
        
        if(!prepareDate()){
        	return false;
        }
        
        PubSubmit p = new PubSubmit();
        PubSubmit p2 = new PubSubmit();
        PubSubmit updateP = new PubSubmit();
        if("INSERT".equals(mOperate)){
        	if(!p.submitData(mInputData, mOperate))
        	{
        		CError tError = new CError();
        		tError.moduleName = "BPBL";
        		tError.functionName = "submitData";
        		tError.errorMessage = "提交数据失败";
        		mErrors.addOneError(tError);
        		System.out.println(tError.errorMessage);
        		return false;
        	}
        	if("Y".equals(toUpdate)){
        		if(!updateP.submitData(updateData, "UPDATE")){
        			CError tError = new CError();
            		tError.moduleName = "BPBL";
            		tError.functionName = "submitData";
            		tError.errorMessage = "修改已有大项目名称时出错";
            		mErrors.addOneError(tError);
            		System.out.println(tError.errorMessage);
        			return false;
        		}
        	}
        }else if("DELETE".equals(mOperate)){
        	if(!p.submitData(mInputData, mOperate))
        	{
        		CError tError = new CError();
        		tError.moduleName = "BPBL";
        		tError.functionName = "submitData";
        		tError.errorMessage = "提交数据失败";
        		mErrors.addOneError(tError);
        		System.out.println(tError.errorMessage);
        		return false;
        	}
        	if(!p2.submitData(mInputData2, "INSERT"))
        	{
        		CError tError = new CError();
        		tError.moduleName = "BPBL";
        		tError.functionName = "submitData";
        		tError.errorMessage = "提交数据失败,删除数据同时向轨迹表插入数据未受理";
        		mErrors.addOneError(tError);
        		System.out.println(tError.errorMessage);
        		return false;
        	}
        }
        
    	return true;
    }
    
    private boolean getInputData(VData tVData){
    	tLCBigProjectInfoSchema = (LCBigProjectInfoSchema)tVData.getObjectByObjectName("LCBigProjectInfoSchema", 0);
    	tLCBigProjectContSchema = (LCBigProjectContSchema)tVData.getObjectByObjectName("LCBigProjectContSchema", 0);
    	tLCBigProjectContTrackSchema = (LCBigProjectContTrackSchema)tVData.getObjectByObjectName("LCBigProjectContTrackSchema", 0);
    	tLCBigProjectYearSchema = (LCBigProjectYearSchema)tVData.getObjectByObjectName("LCBigProjectYearSchema", 0);
    	return true;
    }
    
    private boolean checkData(){
    	if(tLCBigProjectInfoSchema.getBigProjectNo() == null || "".equals(tLCBigProjectInfoSchema.getBigProjectNo())){
    		isNewBPInfo = "N";
    	}else{
    		isNewBPInfo = "Y";
    	}
    	if(tLCBigProjectContSchema == null){
    		CError tError = new CError();
            tError.moduleName = "BPBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有传入LCBigProjectContSchema信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
    	}
    	if(tLCBigProjectContTrackSchema == null){
    		CError tError = new CError();
            tError.moduleName = "BPBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有传入LCBigProjectContTrackSchema信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
    	}
    	if(tLCBigProjectYearSchema.getBigProjectNo() == null || "".equals(tLCBigProjectYearSchema.getBigProjectNo())){
    		isNewBPYear = "N";
    	}else{
    		isNewBPYear = "Y";
    	}
    	return true;
    }
    
    private boolean dealData(){
    	if("INSERT".equals(mOperate)){
    		String tInputBPNo = tLCBigProjectInfoSchema.getBigProjectNo();
    		String haveRecord = "";
    		haveRecord = new ExeSQL().getOneValue("select 1 from lcbigprojectinfo where bigprojectno='"+tInputBPNo+"'");
    		if("Y".equals(isNewBPInfo)){
    			if(haveRecord==null || "".equals(haveRecord)){
    				map.put(this.tLCBigProjectInfoSchema, SysConst.INSERT);
    			}else{
    				updateMap.put(this.tLCBigProjectInfoSchema, SysConst.UPDATE);
    				toUpdate = "Y";
    			}
    		}
    		map.put(this.tLCBigProjectContSchema, SysConst.INSERT);
    		map.put(this.tLCBigProjectContTrackSchema, SysConst.INSERT);
    		if("Y".equals(isNewBPYear)){
    			map.put(this.tLCBigProjectYearSchema, SysConst.INSERT);
    		}
    	}else if("DELETE".equals(mOperate)){
    		map.put(this.tLCBigProjectContSchema, SysConst.DELETE);
    		map2.put(this.tLCBigProjectContTrackSchema, SysConst.INSERT);
    	}
    	return true;
    }
    
    private boolean prepareDate(){
    	if("INSERT".equals(mOperate)){
    		mInputData.add(map);
    		if("Y".equals(toUpdate)){
    			updateData.add(updateMap);
    		}
    	}else if("DELETE".equals(mOperate)){
    		mInputData.add(map);
    		mInputData2.add(map2);
    	}
    	return true;
    }
    
    public VData getResult() {
        return mResult;
    }
	
}
