package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class MaintainArchiveNo {
    private ExeSQL mExeSQL = new ExeSQL();

    public MaintainArchiveNo() {
    }

    //团单处理
    private ES_DOC_MAINSchema dealOneG(ES_DOC_MAINSchema cES_DOC_MAINSchema) {
        String tDate = cES_DOC_MAINSchema.getMakeDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        String tManageCom = cES_DOC_MAINSchema.getManageCom();
        String tDoccode = cES_DOC_MAINSchema.getDocCode();
        String comPart = "";
        int length = tManageCom.length();
        //准备归档号
        if (length == 2) {
            String sql = "select ManageCom from LCGrpCont where PrtNo = '" + tDoccode + "' "
             + " union select ManageCom from LBGrpCont where PrtNo = '" + tDoccode + "' "
             + " union select ManageCom from LOBGrpCont where PrtNo = '" + tDoccode + "' ";
            tManageCom = mExeSQL.getOneValue(sql);
            if (tManageCom.length() >= 6) {
                comPart = tManageCom.substring(2, 6);
            }
            else {
                //System.out.println("机构代码不能两位");
                comPart = "XXXX";
            }
        } else if (length == 4) {
            comPart = tManageCom.substring(2, 4) + "00";
        } else {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "T" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);

        cES_DOC_MAINSchema.setArchiveNo(tArchiveNo);

        return cES_DOC_MAINSchema;
    }

    //个单处理
    private ES_DOC_MAINSchema dealOneP(ES_DOC_MAINSchema cES_DOC_MAINSchema) {
        String tDate = cES_DOC_MAINSchema.getMakeDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        String tManageCom = cES_DOC_MAINSchema.getManageCom();
        String tDoccode = cES_DOC_MAINSchema.getDocCode();
        String comPart = "";
        int length = tManageCom.length();
        //准备归档号
        if (length == 2) {
            String sql = "select ManageCom from LCCont where PrtNo = '" + tDoccode + "' "
             + " union select ManageCom from LBCont where PrtNo = '" + tDoccode + "' "
             + " union select ManageCom from LOBCont where PrtNo = '" + tDoccode + "' ";
            tManageCom = mExeSQL.getOneValue(sql);
            if (tManageCom.length() >= 6) {
                comPart = tManageCom.substring(2, 6);
            }
            else {
                //System.out.println("机构代码不能两位");
                comPart = "XXXX";
            }
        } else if (length == 4) {
            comPart = tManageCom.substring(2, 4) + "00";
        } else {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "G" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);

        cES_DOC_MAINSchema.setArchiveNo(tArchiveNo);

        return cES_DOC_MAINSchema;
    }

    public boolean maintain() {
        MMap map = new MMap();
        VData d = new VData();
        PubSubmit p = new PubSubmit();
        ES_DOC_MAINDB tEsdocmainDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tEsdocmainSet = new ES_DOC_MAINSet();
        ES_DOC_MAINSchema tEsdocmainSchema = new ES_DOC_MAINSchema();
        ES_DOC_MAINSchema uES_DOC_MAINSchema = new ES_DOC_MAINSchema();
        String tsql = "";
        int m = 0;
        int k = 0;
        int nc = 0;
        int ns = 0;
        int ne = 0;
        int iGSize = 0;
        ExeSQL tExeSQL = new ExeSQL();

        //准备个单
        tsql = "select count(*) from ES_DOC_MAIN where SubType='TB01' and ArchiveNO is null with ur";
        String result = tExeSQL.getOneValue(tsql);
        if (Integer.parseInt(result) > 0) {
            ne = Integer.parseInt(result);
            tsql = "";
            tsql = "select * from ES_DOC_MAIN where SubType='TB01' and ArchiveNO is null with ur";
            m = ne / 5000;
            if (m*5000 < ne)
            {
                m = m + 1;
            }
            k = 0;
            do {
                ns = k * 5000 + 1;
                if ((ne - ns) > 5000)
                {
                    nc = 5000;
                }
                else
                {
                    nc = ne - ns + 1;    //不足5000条的
                }
                tEsdocmainSet = null;
                uES_DOC_MAINSchema = null;
                d = new VData();
                p = new PubSubmit();
                map = new MMap();
                //每次处理5000条
                tEsdocmainSet = tEsdocmainDB.executeQuery(tsql,1,nc);
                iGSize = tEsdocmainSet.size(); //gongqun 提高效率
                for (int i = 1; i <= iGSize; i++) {
                    tEsdocmainSchema = (ES_DOC_MAINSchema) tEsdocmainSet.get(i);
                    uES_DOC_MAINSchema = dealOneP(tEsdocmainSchema).getSchema();
                    map.put(uES_DOC_MAINSchema, SysConst.UPDATE);
                }
                d.add(map);
                if (!p.submitData(d, "")) {
                    System.out.println("个单归档号维护保存失败！");
                }
                k++;
            }
            while (k < m);
    }


//清空集合
        result = null;
        tsql = "";
        int iTSize = 0;
        //准备团单
        tsql = "select count(*) from ES_DOC_MAIN where SubType='TB02' and ArchiveNO is null with ur";
        result = tExeSQL.getOneValue(tsql);
        if (Integer.parseInt(result) > 0) {
            ne = Integer.parseInt(result);
            tsql = "";
            tsql = "select * from ES_DOC_MAIN where SubType='TB02' and ArchiveNO is null with ur";
            m = ne / 5000;
            if (m*5000 < ne)
            {
                m = m + 1;
            }
            k = 0;
            do {
                ns = k * 5000 + 1;
                if ((ne - ns) > 5000)
                {
                    nc = 5000;
                }
                else
                {
                    nc = ne - ns + 1;    //不足5000条的
                }
                tEsdocmainSet = null;
                uES_DOC_MAINSchema = null;
                d = new VData();
                p = new PubSubmit();
                map = new MMap();
                //每次处理5000条
                tEsdocmainSet = tEsdocmainDB.executeQuery(tsql,1,nc);
                iTSize = tEsdocmainSet.size(); //gongqun 提高效率
                for (int j = 1; j <= iTSize; j++) {
                    tEsdocmainSchema = (ES_DOC_MAINSchema) tEsdocmainSet.get(j);
                    uES_DOC_MAINSchema = dealOneG(tEsdocmainSchema).getSchema();
                    map.put(uES_DOC_MAINSchema, SysConst.UPDATE);
                }
                d.add(map);
                if (!p.submitData(d, "")) {
                    System.out.println("团单归档号维护保存失败！");
                }
                k++;
            }
            while (k < m);
        }
        return true;
    }

    public static void main(String[] args) {
        MaintainArchiveNo bl = new MaintainArchiveNo();
        if (!bl.maintain()) {
            System.out.println("可能有错误！");
        }
    }
}
