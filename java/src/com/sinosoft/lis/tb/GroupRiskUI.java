package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class GroupRiskUI
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();


    /** 往前面传输数据的容器 */
    private VData mResult = new VData();


    /** 数据操作字符串 */
    private String mOperate;


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();


    // @Constructor
    public GroupRiskUI()
    {}


    // @Main
    public static void main(String[] args)
    {
        LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
        LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData tTransferData = new TransferData();
        mLCGrpContSchema.setGrpContNo("1400000690");
        mGlobalInput.ManageCom = "86";
        mGlobalInput.Operator = "001";
        //第一个险种
        mLCGrpPolSchema.setGrpContNo("1400000690");
        mLCGrpPolSchema.setRiskCode("1601");
        mLCGrpPolSchema.setPayIntv("12");
        mLCGrpPolSchema.setCValiDate("2005-3-30");
        mLCGrpPolSet.add(mLCGrpPolSchema);
        //第二个险种
        mLCGrpPolSchema = new LCGrpPolSchema();
        mLCGrpPolSchema.setGrpContNo("1400000690");
        mLCGrpPolSchema.setRiskCode("1602");
        mLCGrpPolSchema.setPayIntv("12");
        mLCGrpPolSchema.setCValiDate("2005-3-30");
        mLCGrpPolSet.add(mLCGrpPolSchema);
        //第三个险种
        mLCGrpPolSchema = new LCGrpPolSchema();
        mLCGrpPolSchema.setGrpContNo("1400000690");
        mLCGrpPolSchema.setRiskCode("1603");
        mLCGrpPolSchema.setPayIntv("12");
        mLCGrpPolSchema.setCValiDate("2005-3-30");
        mLCGrpPolSet.add(mLCGrpPolSchema);


        VData tVData = new VData();
        tVData.add(mLCGrpContSchema);
        tVData.add(mLCGrpPolSet);
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);
        GroupRiskBL tGroupRiskBL = new GroupRiskBL();
        if (tGroupRiskBL.submitData(tVData, "DELETE||GROUPRISK") == false)
        {
            System.out.println(tGroupRiskBL.mErrors.getFirstError().toString());

        }

    }


    // @Method
    /**
     * 数据提交方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return: boolean
     **/
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 数据操作字符串拷贝到本类中
        this.mOperate = cOperate;

        GroupRiskBL tGroupRiskBL = new GroupRiskBL();

        System.out.println("---UI BEGIN---");
        if (tGroupRiskBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGroupRiskBL.mErrors);
            return false;
        }
        else
        {
            mResult = tGroupRiskBL.getResult();
        }
        System.out.print(mErrors.toString());
        return true;
    }


    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

}
