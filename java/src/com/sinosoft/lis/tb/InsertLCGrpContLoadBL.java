package com.sinosoft.lis.tb;

import java.io.UnsupportedEncodingException;

import com.sinosoft.lis.db.LCGrpContRoadDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCGrpContRoadSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class InsertLCGrpContLoadBL {

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */
	private VData mInputData = new VData();

	/** 输出数据的容器 */
	private VData mResult = new VData();

	/** 提交数据的容器 */
	private MMap map = new MMap();

	/** 数据操作字符串 */
	private String mOperate;

	// 统一更新日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 统一更新时间
	private String mCurrentTime = PubFun.getCurrentTime();

	private LCGrpContRoadSchema tLCGrpContRoadSchema = new LCGrpContRoadSchema();

	private LCGrpContRoadSet mLCGrpContRoadSet = new LCGrpContRoadSet();

	public InsertLCGrpContLoadBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) throws UnsupportedEncodingException {

		mOperate = cOperate;
		mInputData = (VData) cInputData.clone();

		if (!getInputData(mInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!prepareDate()) {
			return false;
		}

		PubSubmit p = new PubSubmit();

		if (!p.submitData(mInputData, mOperate)) {
			CError tError = new CError();
			tError.moduleName = "BPBL";
			tError.functionName = "submitData";
			tError.errorMessage = "提交数据失败";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		return true;
	}

	private boolean getInputData(VData tVData) {
		tLCGrpContRoadSchema = (LCGrpContRoadSchema) tVData
				.getObjectByObjectName("LCGrpContRoadSchema", 0);
		return true;
	}

	private boolean checkData() {
		if (tLCGrpContRoadSchema == null) {
			CError tError = new CError();
			tError.moduleName = "InsertLCGrpContLoadBL";
			tError.functionName = "checkData";
			tError.errorMessage = "没有传入LCGrpContRoadSchema信息";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		return true;
	}

	private boolean dealData() throws UnsupportedEncodingException {
		LCGrpContRoadSchema mLCGrpContRoadSchema = new LCGrpContRoadSchema();
		String tEngineeringFactor=null;
		String tprtno = tLCGrpContRoadSchema.getPrtNo();
		String tCountry = tLCGrpContRoadSchema.getCountry();
		String tCountryCategory = tLCGrpContRoadSchema.getCountryCategory();
		String tEngineeringFlag = tLCGrpContRoadSchema.getEngineeringFlag();
		String tEngineeringCategory = tLCGrpContRoadSchema
				.getEngineeringCategory();
		int day = 0;
		String tinsureyear="";
		// 根据国家查询算费因子
//		tCountry=new String(tCountry.getBytes("ISO-8859-1"),"GB2312");
//		tEngineeringCategory=new String(tEngineeringCategory.getBytes("ISO-8859-1"),"GB2312");
		System.out.println("a:"+tCountry);
		System.out.println("b:"+tEngineeringCategory);
//		String sqlCountry = "select code,codealias from ldcode where codetype='country' and codename='"
//				+ tCountryCategory + "'";
		//根据国家类型查询算费因子
		String sqlCountry = "select code,codealias from ldcode where codetype='country' and code='"
				+ tCountryCategory + "'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sqlCountry);
		if (tSSRS.getMaxRow() <= 0) {
			CError tError = new CError();
			tError.moduleName = "InsertLCGrpContLoadBL";
			tError.functionName = "dealData";
			tError.errorMessage = "录入国家查询失败！";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		// 国家类别及国家系数
		//String tCountryCategory = tSSRS.GetText(1, 1);
		String tCountryFactor = tSSRS.GetText(1, 2);

		if(tEngineeringCategory==null||"".equals(tEngineeringCategory)){
			tEngineeringFactor="1";
			tEngineeringCategory=null;
		}else{
			String sqlengineering = "select codealias from ldcode where codetype='engineering' and code='"
					+ tEngineeringCategory + "'";
			tSSRS = tExeSQL.execSQL(sqlengineering);
			if (tSSRS.getMaxRow() <= 0) {
				CError tError = new CError();
				tError.moduleName = "InsertLCGrpContLoadBL";
				tError.functionName = "dealData";
				tError.errorMessage = "录入工程查询失败！";
				mErrors.addOneError(tError);
				System.out.println(tError.errorMessage);
				return false;
			}
			// 工程类别及工程系数
			tEngineeringFactor = tSSRS.GetText(1, 1);
		}
		
		String tsqlinsu = "select cvalidate,cinvalidate from lcgrpcont where PrtNo='"
				+ tprtno + "'";
		tSSRS = tExeSQL.execSQL(tsqlinsu);
		if (tSSRS.getMaxRow() <= 0) {
			CError tError = new CError();
			tError.moduleName = "InsertLCGrpContLoadBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取保单生效日失效日失败！";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		} else {
			String tcvalidate = tSSRS.GetText(1, 1);
			String tcinvalidate = tSSRS.GetText(1, 2);
			day = PubFun.calInterval(tcvalidate, tcinvalidate, "D")+1;
			String sql = "select case when "+day+" <= 15 then "
				       + "case when "+day+" <= 7 then 0.10 when "+day+" >= 8 and "+day+" <= 15 then 0.15 end "
				       + "when "+day+" > 15 then "
				       + "case (select monthdiff('"+tcinvalidate+"', '"+tcvalidate+"') from dual) "
				       + "when 1 then 0.25 "
				       + "when 2 then 0.35 "
				       + "when 3 then 0.45 "
				       + "when 4 then 0.55 "
				       + "when 5 then 0.65 "
				       + "when 6 then 0.70 "
				       + "when 7 then 0.75 "
				       + "when 8 then 0.80 "
				       + "when 9 then 0.85 "
				       + "when 10 then 0.90 "
				       + "when 11 then 0.95 "
				       + "when 12 then 1.00 end end "
				       + "from dual where 1=1 ";
			tSSRS = tExeSQL.execSQL(sql);
			if(tSSRS.getMaxRow()> 0){
				tinsureyear=tSSRS.GetText(1, 1);
			}
		}

		String sql = "select * from lcgrpcontroad where prtno='" + tprtno
				+ "' with ur";
		LCGrpContRoadDB mLCGrpContRoadDB = new LCGrpContRoadDB();
		LCGrpContRoadSet tLCGrpContRoadSet = new LCGrpContRoadSet();
		tLCGrpContRoadSet = mLCGrpContRoadDB.executeQuery(sql);
		if (tLCGrpContRoadSet.size() > 0) {
			mLCGrpContRoadSchema = tLCGrpContRoadSet.get(1);
			mLCGrpContRoadSchema.setCountry(tCountry);
			mLCGrpContRoadSchema.setManageCom(tLCGrpContRoadSchema.getManageCom());
			mLCGrpContRoadSchema.setCountryCategory(tCountryCategory);
			mLCGrpContRoadSchema.setCountryFactor(tCountryFactor);
			mLCGrpContRoadSchema.setEngineeringFlag(tEngineeringFlag);
			mLCGrpContRoadSchema.setEngineeringCategory(tEngineeringCategory);
			mLCGrpContRoadSchema.setEngineeringFactor(tEngineeringFactor);
			mLCGrpContRoadSchema.setInsureyear(tinsureyear);
			mLCGrpContRoadSchema.setModifyDate(mCurrentDate);
			mLCGrpContRoadSchema.setModifyTime(mCurrentTime);

			mLCGrpContRoadSet.add(mLCGrpContRoadSchema);
		} else {
			tLCGrpContRoadSchema.setCountryCategory(tCountryCategory);
			tLCGrpContRoadSchema.setCountryFactor(tCountryFactor);
			tLCGrpContRoadSchema.setEngineeringFactor(tEngineeringFactor);
			tLCGrpContRoadSchema.setInsureyear(tinsureyear);
			tLCGrpContRoadSchema.setMakeDate(mCurrentDate);
			tLCGrpContRoadSchema.setMakeTime(mCurrentTime);
			tLCGrpContRoadSchema.setModifyDate(mCurrentDate);
			tLCGrpContRoadSchema.setModifyTime(mCurrentTime);
			mLCGrpContRoadSet.add(tLCGrpContRoadSchema);
		}

		map.put(mLCGrpContRoadSet, SysConst.DELETE_AND_INSERT);

		return true;
	}

	private boolean prepareDate() {
		mInputData.add(map);
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args){
		LCGrpContRoadSchema tLCGrpContRoadSchema=new LCGrpContRoadSchema();
		tLCGrpContRoadSchema.setPrtNo("18171117021");
		tLCGrpContRoadSchema.setCountry("马来西亚");
		tLCGrpContRoadSchema.setEngineeringFlag("1");
		tLCGrpContRoadSchema.setEngineeringCategory("");
		tLCGrpContRoadSchema.setManageCom("86110000");
		VData tVData = new VData();
		tVData.add(tLCGrpContRoadSchema);

		InsertLCGrpContLoadBL tInsertLCGrpContLoadBL = new InsertLCGrpContLoadBL();
		try {
			tInsertLCGrpContLoadBL.submitData(tVData, "");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
