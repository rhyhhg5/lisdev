package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LDBusinessRuleSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDBusinessRuleSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 契约规则设置，如契约保单发送规则、保单下载规则设置
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BusinessRuleConfigBL
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private LDBusinessRuleSchema mLDBusinessRuleSchema = null;  //用来传入规则类型和业务类型
    private LDBusinessRuleSet mLDBusinessRuleSet = null;

    private MMap map = new MMap();

    public BusinessRuleConfigBL()
    {
    }

    /**
     * 外部提交的方法，本方法不直接处理业务
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        cInputData = (VData) cInputData.clone();
        if (getSubmitMap(cInputData) == null)
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        VData data = new VData();
        data.add(map);
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.addOneError("提交数据失败");
            System.out.println("提交失败:" + tPubSubmit.mErrors.getErrContent());
            return false;
        }

        return true;
    }

    /**
     * 外部提交的方法，返回处理后的MMap集合
     * @param cInputData VData
     * @return boolean
     */
    public MMap getSubmitMap(VData cInputData)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }
        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 将传入的外包数据分解到本类的全局变量
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLDBusinessRuleSet = (LDBusinessRuleSet) cInputData
                             .getObjectByObjectName("LDBusinessRuleSet", 0);

        mLDBusinessRuleSchema = (LDBusinessRuleSchema) cInputData
                                .getObjectByObjectName("LDBusinessRuleSchema", 0);

        if(mGI == null)
        {
            mErrors.addOneError("页面超时，请重登录");
            return false;
        }
        if(mLDBusinessRuleSchema == null
           || mLDBusinessRuleSchema.getRuleType() == null
            || mLDBusinessRuleSchema.getBussType() == null)
        {
            mErrors.addOneError("请选择规则类型和业务类型");
            return false;
        }

        System.out.println(mGI.Operator + ": " + PubFun.getCurrentTime() + this);

        return true;
    }

    /**
     * 进行数据合法性的校验
     * @return boolean
     */
    private boolean checkData()
    {
        if(mLDBusinessRuleSet != null)
        {
            for(int i = 1; i <= mLDBusinessRuleSet.size(); i++)
            {
                if(mLDBusinessRuleSet.get(i).getRuleType() == null
                   || mLDBusinessRuleSet.get(i).getRuleType().equals("")
                   || mLDBusinessRuleSet.get(i).getBussType() == null
                   || mLDBusinessRuleSet.get(i).getBussType().equals("")
                    || mLDBusinessRuleSet.get(i).getComCode() == null
                     || mLDBusinessRuleSet.get(i).getComCode().equals(""))
                {
                    mErrors.addOneError("请录入规则类型、业务类型和机构");
                    return false;
                }

                if(mLDBusinessRuleSet.get(i).getComCode().length() != 4)
                {
                    mErrors.addOneError("只能分配分公司的权限");
                    return false;
                }
            }
        }

        return true;
    }

    private boolean dealData()
    {
        String sql = "delete from LDBusinessRule "
                     + "where RuleType = '"
                     + mLDBusinessRuleSchema.getRuleType() + "' "
                     + "   and BussType = '"
                     + mLDBusinessRuleSchema.getBussType() + "' ";
        map.put(sql, SysConst.DELETE);

        if(mLDBusinessRuleSet != null)
        {
            for(int i = 1; i <= mLDBusinessRuleSet.size(); i++)
            {
                mLDBusinessRuleSet.get(i).setRuleName(codeName("ruletype",
                    mLDBusinessRuleSchema.getRuleType()));
                mLDBusinessRuleSet.get(i).setRuleName(codeName("busstype",
                    mLDBusinessRuleSchema.getBussType()));
                mLDBusinessRuleSet.get(i).setState("1");
                mLDBusinessRuleSet.get(i).setOperator(mGI.Operator);
            }
            PubFun.fillDefaultField(mLDBusinessRuleSet);
            map.put(mLDBusinessRuleSet, SysConst.INSERT);
        }
        return true;
    }

    //得到代码对应的汉字
    private String codeName(String cCodeType, String cCode)
    {
        return com.sinosoft.lis.bq.CommonBL.getCodeName(cCodeType, cCode);
    }

    public static void main(String[] args)
    {
        BusinessRuleConfigBL businessruleconfigbl = new BusinessRuleConfigBL();
    }
}
