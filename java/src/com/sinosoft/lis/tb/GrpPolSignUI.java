package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;

/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-07-01
 */

public class GrpPolSignUI
{
	/** 传入数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	// @Constructor
	public GrpPolSignUI() {}

	// @Method
	public static void main(String[] args)
	{
        LCGrpPolSchema p = new LCGrpPolSchema();
        p.setGrpPolNo( "86110020040120000144" );
        GlobalInput g = new GlobalInput();
        g.ComCode = "86";
        g.Operator = "001";
        VData v = new VData();
        v.add( p );
        v.add( g );
        GrpPolSignUI ui = new GrpPolSignUI();
        if( ui.submitData( v, "" ) == true )
            System.out.println("-----ok-----");
        else
            System.out.println("------NO------");
    }

	/**
	* 数据提交方法
	* @param: 传入数据、数据操作字符串
	* @return: boolean
	**/
	public boolean submitData( VData cInputData, String cOperate )
	{
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;

		GrpPolSignBL tGrpPolSignBL = new GrpPolSignBL();
		if( tGrpPolSignBL.submitData( cInputData, mOperate ) == false )
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tGrpPolSignBL.mErrors );
			return false;
		}

		return true;
	}

}