/**
 * 2013-3-25
 */
package com.sinosoft.lis.tb;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sinosoft.lis.db.ES_DOC_DEFDB;
import com.sinosoft.lis.db.ES_SERVER_INFODB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_DEFSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.schema.ES_SERVER_INFOSchema;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_SERVER_INFOSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.ActivityOperator;

/**
 * EasyScan图像数据接口
 * 目前主要用于万能投保书数据
 * 
 * 
 * @author LY
 *
 */
public class ESPolicyPicsBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mScanManageCom = null;

    private String mScanOperator = "PADSYS";

    private String mPrtNo = null;

    /** 
     * List:[Map:[[String<PicType>, InputStream], [String<PicType>, InputStream]], Map:[...], ...] 
     **/
    private List mPicFiles = null;

    public ESPolicyPicsBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        if (mPrtNo == null || mPrtNo.equals(""))
        {
            buildError("getInputData", "参数[印刷号]不存在。");
            return false;
        }

        mScanManageCom = (String) mTransferData.getValueByName("ScanManageCom");
        if (mScanManageCom == null || mScanManageCom.equals(""))
        {
            buildError("getInputData", "参数[扫描机构]不存在。");
            return false;
        }

        mPicFiles = (List) mTransferData.getValueByName("PicFiles");
        if (mPicFiles == null)
        {
            buildError("getInputData", "参数[影像件图片数据]不存在。");
            return false;
        }

        // 生成全局登录对象，防止超时
        if (mGlobalInput == null)
        {
            mGlobalInput = new GlobalInput();
            mGlobalInput.ManageCom = mScanManageCom;
            mGlobalInput.ComCode = mScanManageCom;
            mGlobalInput.Operator = mScanOperator;
        }
        // --------------------

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("PAD-WNTB".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = createESPolicyPics();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap createESPolicyPics()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 校验保单信息是否存在
        if (!chkPolicyInfo(mPrtNo))
        {
            return null;
        }
        // --------------------

        // 创建ES影像件数据信息
        tTmpMap = createESPicsInfo(mPrtNo, mScanManageCom, mScanOperator);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    /**
     * 校验印刷号在系统中是否已经被使用
     * @param cPrtNo 印刷号
     * @return 如果返回值为“true”，则该印刷号在系统中未曾使用；否则，该印刷号在系统中已使用
     */
    private boolean chkPolicyInfo(String cPrtNo)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tStrSql = null;
        String tResult = null;

        tStrSql = "select 1 from ES_Doc_Main where BussType = 'TB' and SubType = 'TB04' and DocCode = '" + cPrtNo
                + "' ";
        tStrSql += " union all ";
        tStrSql += " select 1 from LCCont where ContType = '1' and PrtNo = '" + cPrtNo + "' ";
        tStrSql += " union all ";
        tStrSql += " select 1 from LBCont where ContType = '1' and PrtNo = '" + cPrtNo + "' ";

        tResult = tExeSQL.getOneValue(tStrSql);
        if (!"".equals(tResult))
        {
            String tErrInfo = "该投保书印刷号码在系统中已经使用。";
            buildError("chkPolicyInfo", tErrInfo);

            return false;
        }

        return true;
    }

    /**
     * 创建ES影像件数据信息，生成原则仿照EasyScan生成数据
     * @param cPrtNo 印刷号
     * @return 如果返回值不为空，返回为待提交ES影像件数据的MMap集合；如果返回值为空，则为处理中出现报错。
     */
    private MMap createESPicsInfo(String cPrtNo, String cManageCom, String cScanOperator)
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 获取影像件基础信息
        ES_SERVER_INFOSchema tEs_ServerInfo = getServerInfo(cManageCom);
        if (tEs_ServerInfo == null)
        {
            return null;
        }
        // --------------------

        // 获取影像件类型信息
        ES_DOC_DEFSchema tEs_PicTypeInfo = getPicTypeInfo();
        if (tEs_PicTypeInfo == null)
        {
            return null;
        }
        // --------------------

        // 创建ES，Main表数据信息
        ES_DOC_MAINSchema tEsMainInfo = createEsMainInfo(cPrtNo, cManageCom, cScanOperator, tEs_ServerInfo,
                tEs_PicTypeInfo);
        if (tEsMainInfo == null)
        {
            return null;
        }
        // --------------------

        // 创建ES，Page表数据信息
        ES_DOC_PAGESSet tEsPagesInfo = createEsPagesInfo(mPicFiles, tEsMainInfo, tEs_ServerInfo, tEs_PicTypeInfo);
        if (tEsPagesInfo == null)
        {
            return null;
        }
        // --------------------

        // 创建单证关系表数据信息
        ES_DOC_RELATIONSchema tEsRelaInfo = createEsRelaInfo(tEsMainInfo);
        if (tEsRelaInfo == null)
        {
            return null;
        }
        // --------------------

        // 创建工作流
        tTmpMap = createMissionFirstNode(tEsMainInfo);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 准备提交数据
        tMMap.put(tEsMainInfo, SysConst.INSERT);
        tMMap.put(tEsPagesInfo, SysConst.INSERT);
        tMMap.put(tEsRelaInfo, SysConst.INSERT);
        // --------------------

        // 加防并发锁
        tTmpMap = lockTable(tEsMainInfo.getDocCode(), mGlobalInput);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    /**
     * 获取影像件基础信息
     * @param cManageCom
     * @return
     */
    private ES_SERVER_INFOSchema getServerInfo(String cManageCom)
    {
        String tStrSql = "select esi.* from Es_Com_Server ecs "
                + " inner join Es_Server_Info esi on esi.HostName = ecs.HostName " + " where 1 = 1 "
                + " and ecs.ManageCom = '" + cManageCom + "' ";

        ES_SERVER_INFODB tES_SERVER_INFODB = new ES_SERVER_INFODB();
        ES_SERVER_INFOSet tEs_ServerInfoSet = tES_SERVER_INFODB.executeQuery(tStrSql);

        if (tEs_ServerInfoSet == null || tEs_ServerInfoSet.size() != 1)
        {
            String tErrInfo = "机构[" + cManageCom + "]不存在上传影像件权限";
            buildError("createESPicsInfo", tErrInfo);

            return null;
        }

        return tEs_ServerInfoSet.get(1);
    }

    /**
     * 获取影像件类型信息
     * @return
     */
    private ES_DOC_DEFSchema getPicTypeInfo()
    {
        ES_DOC_DEFDB tES_DOC_DEFDB = new ES_DOC_DEFDB();
        tES_DOC_DEFDB.setBussType("TB");
        tES_DOC_DEFDB.setSubType("TB04");

        if (!tES_DOC_DEFDB.getInfo())
        {
            String tErrInfo = "单证类型[TB04-银保万能投保书]信息获取失败";
            buildError("createESPicsInfo", tErrInfo);

            return null;
        }

        return tES_DOC_DEFDB.getSchema();
    }

    private ES_DOC_MAINSchema createEsMainInfo(String cPrtNo, String cScanManageCom, String cScanOperator,
            ES_SERVER_INFOSchema cEsServerInfo, ES_DOC_DEFSchema cEsPicTypeInfo)
    {
        ES_DOC_MAINSchema tEsMainInfo = new ES_DOC_MAINSchema();

        // 获取ES影像件流水号 DocID
        String tDocID = getMaxNo("DocID");
        if (tDocID == null)
        {
            String tErrInfo = "影像件流水号DocID获取失败";
            buildError("getMaxNo", tErrInfo);
            return null;
        }
        // --------------------

        tEsMainInfo.setDocID(tDocID);
        tEsMainInfo.setDocCode(cPrtNo);

        tEsMainInfo.setBussType(cEsPicTypeInfo.getBussType());
        tEsMainInfo.setSubType(cEsPicTypeInfo.getSubType());

        tEsMainInfo.setNumPages(0); // 后续根据页数进行更新，此处初始值设置为0

        tEsMainInfo.setDocFlag("1"); // 仿照ES数据写死“1”

        tEsMainInfo.setScanOperator(cScanOperator);
        tEsMainInfo.setManageCom(cScanManageCom);

        tEsMainInfo.setMakeDate(PubFun.getCurrentDate());
        tEsMainInfo.setMakeTime(PubFun.getCurrentTime());
        tEsMainInfo.setModifyDate(PubFun.getCurrentDate());
        tEsMainInfo.setModifyTime(PubFun.getCurrentTime());

        tEsMainInfo.setVersion(cEsPicTypeInfo.getVersion());
        tEsMainInfo.setScanNo("0"); //  仿照ES数据写死“0”

        tEsMainInfo.setState("01"); // ES状态

        // 生成影像件归档号码
        String tArchiveNo = createArchiveNo(cScanManageCom);
        if (tDocID == null)
        {
            return null;
        }
        tEsMainInfo.setArchiveNo(tArchiveNo);
        // --------------------

        return tEsMainInfo;
    }

    private ES_DOC_PAGESSet createEsPagesInfo(List cPicFiles, ES_DOC_MAINSchema cEsMainInfo,
            ES_SERVER_INFOSchema cEsServerInfo, ES_DOC_DEFSchema cEsPicTypeInfo)
    {
        if (cPicFiles == null || cPicFiles.size() == 0)
        {
            String tErrInfo = "未找到影像件图片数据流信息";
            buildError("createEsPagesInfo", tErrInfo);

            return null;
        }

        ES_DOC_PAGESSet tEsPagesSet = new ES_DOC_PAGESSet();

        for (int i = 0; i < cPicFiles.size(); i++)
        {
            ES_DOC_PAGESSchema tEsPageInfo = new ES_DOC_PAGESSchema();

            tEsPageInfo.setDocID(cEsMainInfo.getDocID());
            tEsPageInfo.setPageCode(i + 1);

            tEsPageInfo.setManageCom(cEsMainInfo.getManageCom());
            tEsPageInfo.setOperator(cEsMainInfo.getScanOperator()); // 此处记录的是扫描员
            tEsPageInfo.setScanNo(cEsMainInfo.getScanNo());

            tEsPageInfo.setMakeDate(PubFun.getCurrentDate());
            tEsPageInfo.setMakeTime(PubFun.getCurrentTime());
            tEsPageInfo.setModifyDate(PubFun.getCurrentDate());
            tEsPageInfo.setModifyTime(PubFun.getCurrentTime());

            tEsPageInfo.setHostName(cEsServerInfo.getHostName());
            tEsPageInfo.setPageFlag("1");

            // 获取影像件页流水号 PageID
            String tPageId = getMaxNo("PageID");
            if (tPageId == null)
            {
                String tErrInfo = "影像件页流水号PageID获取失败";
                buildError("createEsPagesInfo", tErrInfo);
                return null;
            }

            tEsPageInfo.setPageID(tPageId);
            // --------------------

            tEsPageInfo.setPageName("F" + tPageId);
            tEsPageInfo.setPageSuffix(".gif");

            // 获取影像件归档路径
            String tImgArchiveDir = getImgArchiveDir(cEsMainInfo);
            tEsPageInfo.setPicPathFTP(cEsServerInfo.getServerPort() + "/" + tImgArchiveDir);
            tEsPageInfo.setPicPath(cEsServerInfo.getPicPath() + tImgArchiveDir);
            // --------------------

            tEsPageInfo.setPageType("0");

            tEsPagesSet.add(tEsPageInfo);

            // 生成文件
            Map tPicFilesMap = (Map) cPicFiles.get(i);
            if (!uploadPicsFiles(tEsPageInfo, cEsServerInfo, tPicFilesMap))
            {
                return null;
            }
            // --------------------
        }

        // 更新ES主表总页数信息
        cEsMainInfo.setNumPages(tEsPagesSet.size());
        // --------------------

        return tEsPagesSet;
    }

    /**
     * 创建单证关系表数据信息
     * @param cEsMainInfo
     * @return
     */
    private ES_DOC_RELATIONSchema createEsRelaInfo(ES_DOC_MAINSchema cEsMainInfo)
    {
        ES_DOC_RELATIONSchema tEsRelaInfo = new ES_DOC_RELATIONSchema();

        tEsRelaInfo.setDocID(cEsMainInfo.getDocID());
        tEsRelaInfo.setDocCode(cEsMainInfo.getDocCode());

        tEsRelaInfo.setBussNoType("11"); // 仿照ES数据，写死“11”

        tEsRelaInfo.setBussNo(cEsMainInfo.getDocCode());
        tEsRelaInfo.setBussType(cEsMainInfo.getBussType());

        tEsRelaInfo.setSubType(cEsMainInfo.getSubType());

        tEsRelaInfo.setRelaFlag("0"); // 仿照ES写死“0”

        return tEsRelaInfo;
    }

    private MMap createMissionFirstNode(ES_DOC_MAINSchema cEsMainInfo)
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        VData tVData = new VData();

        // 参数要素
        TransferData tTransferData = new TransferData();

        tTransferData.setNameAndValue("PrtNo", cEsMainInfo.getDocCode());
        tTransferData.setNameAndValue("InputDate", cEsMainInfo.getMakeDate());
        tTransferData.setNameAndValue("ScanOperator", cEsMainInfo.getScanOperator());
        tTransferData.setNameAndValue("ManageCom", cEsMainInfo.getManageCom());

        tVData.add(mGlobalInput);
        tVData.add(tTransferData);
        // --------------------

        try
        {
            ActivityOperator tActivityOpertor = new ActivityOperator();
            if (tActivityOpertor.CreateStartMission("0000000009", "0000009001", tVData))
            {
                VData tTmpVData = tActivityOpertor.getResult();
                tTmpMap = (MMap) tTmpVData.getObjectByObjectName("MMap", 0);
                if (tTmpMap == null)
                {
                    String tErrInfo = "创建起始工作流节点失败 - 未获取到有效数据信息";
                    buildError("createMissionFirstNode", tErrInfo);
                    return null;
                }
                tMMap.add(tTmpMap);
                tTmpMap = null;
            }
            else
            {
                String tErrInfo = "创建起始工作流节点失败";
                buildError("createMissionFirstNode", tErrInfo);
                return null;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

            String tErrInfo = "创建起始工作流节点出现异常错误";
            buildError("createMissionFirstNode", tErrInfo);
            return null;
        }

        return tMMap;
    }

    private boolean uploadPicsFiles(ES_DOC_PAGESSchema cEsPicInfo, ES_SERVER_INFOSchema cEsServerInfo, Map cFileMap)
    {
        String tPageName = cEsPicInfo.getPageName();
        String tPageSuffix = null;

        String tFileDir = cEsServerInfo.getServerBasePath() + cEsPicInfo.getPicPath();

        // 本地测试使用
        // tFileDir = "E:/hmyc/Projects/PICCH/ui/" + cEsPicInfo.getPicPath();
        // --------------------

        Iterator tPicSuffixSet = cFileMap.keySet().iterator();
        while (tPicSuffixSet.hasNext())
        {
            BufferedOutputStream tBufOutSPicFile = null;
            BufferedInputStream tBufInSPicFile = null;
            try
            {
                tPageSuffix = (String) tPicSuffixSet.next();
                String tFileFullName = tPageName + "." + tPageSuffix.toLowerCase();

                byte[] tDataCache = new byte[2048];
                tBufInSPicFile = new BufferedInputStream((InputStream) cFileMap.get(tPageSuffix));

                File tFFileDir = new File(tFileDir);
                if (!tFFileDir.exists() || !tFFileDir.isDirectory())
                {
                    tFFileDir.mkdirs();
                }

                File tFPicFile = new File(tFileDir + File.separator + tFileFullName);
                if (tFPicFile.exists() && tFPicFile.isFile())
                {
                    String tErrInfo = "生成影像文件失败-存在同名文件，请尝试重新上传";
                    buildError("uploadPicsFiles", tErrInfo);
                    return false;
                }

                tFPicFile.createNewFile();
                tBufOutSPicFile = new BufferedOutputStream(new FileOutputStream(tFPicFile));

                while (tBufInSPicFile.read(tDataCache) != -1)
                {
                    tBufOutSPicFile.write(tDataCache);
                }

                tBufOutSPicFile.flush();
            }
            catch (Exception e)
            {
                e.printStackTrace();

                String tErrInfo = "生成影像文件失败";
                buildError("uploadPicsFiles", tErrInfo);
                return false;
            }
            finally
            {
                try
                {
                    if (tBufOutSPicFile != null)
                    {
                        tBufOutSPicFile.close();
                    }

                    if (tBufInSPicFile != null)
                    {
                        tBufInSPicFile.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();

                    String tErrInfo = "关闭文件流时出现异常";
                    buildError("uploadPicsFiles", tErrInfo);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 生成Es数据DocID流水号
     * @param cNoType
     * @return
     */
    private String getMaxNo(String cNoType)
    {
        String strNo = PubFun1.CreateMaxNo(cNoType, 1);

        if (strNo.equals("") || strNo.equals("0"))
        {
            strNo = null;
        }

        return strNo;
    }

    /**
     * 获取影像件存储的相对路径
     * @param cEsMainInfo
     * @return
     */
    private String getImgArchiveDir(ES_DOC_MAINSchema cEsMainInfo)
    {
        String tManageCom = cEsMainInfo.getManageCom();
        String tScanDate = cEsMainInfo.getMakeDate();

        StringBuffer tImgArchiveDir = new StringBuffer();
        tImgArchiveDir.append(tManageCom);
        tImgArchiveDir.append("/");
        tImgArchiveDir.append(tScanDate.substring(0, 4));
        tImgArchiveDir.append("/");
        tImgArchiveDir.append(tScanDate.substring(5, 7));
        tImgArchiveDir.append("/");
        tImgArchiveDir.append(tScanDate.substring(8, 10));
        tImgArchiveDir.append("/");

        return tImgArchiveDir.toString();
    }

    /**
     * 生成归档号
     * @return String
     */
    private String createArchiveNo(String cManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7) + tDate.substring(8, 10);

        String comPart = "";
        int length = cManageCom.length();
        if (length == 2)
        {
            String tErrInfo = "两位机构代码不能进行投保单扫描";
            buildError("getMaxNo", tErrInfo);
            return null;
        }
        else if (length == 4)
        {
            comPart = cManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = cManageCom.substring(2, 6);
        }

        String tComtop = "Y" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    }

    /**
     * 锁定扫描上载
     * @param LockNoKey
     * @param tGlobalInput
     * @return
     */
    private MMap lockTable(String lockNoKey, GlobalInput globalInput)
    {
        MMap tTmpMap = null;

        //扫描上载类型：“SI”
        String tLockNoType = "SI";

        //锁定有效时间
        String tAIS = "60";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", lockNoKey);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(globalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tTmpMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tTmpMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return tTmpMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println("[" + szFunc + "]: " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
}
