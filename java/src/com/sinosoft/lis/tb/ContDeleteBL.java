package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:个单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class ContDeleteBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    private String mOperator;

    private String mManageCom;

    private String mDeleteReason;

    private String mRelatePremDelete;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LCContSet mLCContSet = new LCContSet();

    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    private int ContCount = 0;

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();

    private String theCurrentTime = PubFun.getCurrentTime();

    /**判断删除扫描件 */
    private boolean deleteScan = false;

    public ContDeleteBL()
    {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //将VData数据还原成业务需要的类
        if (this.getInputData() == false)
        {
            return false;
        }

        System.out.println("---getInputData successful---");

        if (!checkData())
        {
            return false;
        }

        if (this.dealData() == false)
        {
            return false;
        }

        System.out.println("---dealdata successful---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        if (!StrTool.cTrim(cOperate).equals("GETMAP"))
        {
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mResult, cOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                return false;
            }
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        String sql = "select 1 from Es_Doc_Main esd " + "where State is not null "
                + "   and State = '03' " + "   and DocCode = '"
                + mLCContSchema.getPrtNo() + "' "
                + "   and exists(select 1 from bpomissionstate where bussno = esd.doccode and state<>'05' ) ";
        String temp = new ExeSQL().getOneValue(sql);
        if (!"".equals(temp))
        {
            CError tError = new CError();
            tError.moduleName = "ContDeleteBL";
            tError.functionName = "checkData";
            tError.errorMessage = "扫描件已发送至外包录入前置机，不能删除";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        
        //新单删除增加校验 zhangchengxuan 2012-5-14 
        //1、该保单进入过人工核保不能删除。2、该保单有撤单申请不能删除。
        String cheSql = "select uwflag from lccont where prtno='"
                + mLCContSchema.getPrtNo() + "'";
        String uwflag = new ExeSQL().getOneValue(cheSql);
        if(uwflag == null || uwflag.equals("a")){
        	CError tError = new CError();
            tError.moduleName = "ContDeleteBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保单已撤单，无法进行新单删除。";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        cheSql = "select 1 from lwmission where missionprop1='" + mLCContSchema.getPrtNo() + "' and activityid='0000001100' " +
        		"union all " +
        		"select 1 from lbmission where missionprop1='" + mLCContSchema.getPrtNo() + "' and activityid='0000001100'";
        SSRS tSSRS = new ExeSQL().execSQL(cheSql);
        if(tSSRS.MaxRow > 0){
        	CError tError = new CError();
            tError.moduleName = "ContDeleteBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保单进行过人工核保，无法进行新单删除。";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
    }

    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("contdeletebl---getInputData()");
        //全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }
        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            mDeleteReason = "";
            mRelatePremDelete = "";
        }
        else
        {
            mDeleteReason = (String) mTransferData
                    .getValueByName("DeleteReason");
            mRelatePremDelete = (String) mTransferData
                    .getValueByName("RelatePremDelete");
        }
               
        /**
         * 在团单录单界面调用该程序，如果mRelatePremDelete为"1",则更新LCInsuredList中的state为"0"
         */
//        mLCContSet.set((LCContSet) mInputData.getObjectByObjectName(
//                "LCContSet", 0));
//        if (mLCContSet != null)
//        {
//            ContCount = mLCContSet.size();
//            System.out.println("ContCount" + ContCount);
//        }

        //团体保单实例
        mLCContSchema.setSchema((LCContSchema) mInputData
                .getObjectByObjectName("LCContSchema", 0));

        if (mLCContSchema == null)
        {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));

            return false;
        }
//      add by yingxl 2008-04-18 个单处理
        if ("1".equals(mLCContSchema.getContType())){
            
            String strSql = "select STATE from LJTEMPFEE WHERE otherno ='"
                                      + mLCContSchema.getPrtNo()+"' and STATE='0'" ;
       
        //String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
        ExeSQL tExeSQL = new ExeSQL();  
        SSRS tSSRS = tExeSQL.execSQL(strSql);
        if (tSSRS.getMaxRow() > 0) {
            mErrors.addOneError(new CError("目前状态不允许进行新单删除")) ;
            return false ;
        }
            boolean abool= new ExeSQL().execUpdateSQL("UPDATE LJTEMPFEE SET STATE='0' WHERE otherno ='"
                                      + mLCContSchema.getPrtNo()+"'") ;
            if(!abool){
                //LJTEMPFEE中state不需要进行修改
                mErrors.addOneError(new CError("目前状态不允许进行新单删除")) ;
                return false ;
            }
            boolean bbool= new ExeSQL().execUpdateSQL("UPDATE ljspay set cansendbank ='1' where otherno='"
                                      + mLCContSchema.getPrtNo()+"'") ;
                      
        } 
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        if (mLCContSchema.getContNo() != null)
        {
            tLCContDB.setContNo(mLCContSchema.getContNo());
            tLCContSet = tLCContDB.query();
        }
        else if (mLCContSchema.getPrtNo() != null
                && !"".equals(mLCContSchema.getPrtNo()))
        {
            tLCContSet = new LCContSet();
            mLCContSchema.setContType("1");
            deleteScan = true;
            tLCContSet.add(mLCContSchema);
        }
        else
        {
            this.mErrors.addOneError(new CError("未查到合同单！"));
            return false;
        }
        //             System.out.println("woshiyigeyao!");

        if ((tLCContSet == null) || (tLCContSet.size() <= 0))
        {
            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            this.mErrors.addOneError(new CError("未查到集体合同单！"));

            return false;
        }

        mLCContSchema.setSchema(tLCContSet.get(1));

        String sscontno = mLCContSchema.getContNo();
        System.out.println(sscontno);
        System.out.println(mOperator);
        return true;
    }

    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * @return boolean
     */
    private boolean dealData()
    {
        String tContType = mLCContSchema.getContType();
        String tContNo = mLCContSchema.getContNo();

        // 对于只生成了起始工作流节点，而尚未录入保单的情况，tContNo 为 null。
        //        if (tContNo == null || tContNo.equals(""))
        //        {
        //            mErrors.addOneError("请传入个人合同号，否则无法删除");
        //            return false;
        //        }

        String tProposalContNo = mLCContSchema.getProposalContNo();
        String tGrpContNo = mLCContSchema.getGrpContNo();
        String tPrtNo = mLCContSchema.getPrtNo();
        int tPeoples = mLCContSchema.getPeoples();
        double tPrem = mLCContSchema.getPrem();
        double tAmnt = mLCContSchema.getAmnt();

        mLCDelPolLog.setOtherNo(tContNo);
        mLCDelPolLog.setOtherNoType("12");
        mLCDelPolLog.setPrtNo(mLCContSchema.getPrtNo());

        if ("1".equals(mLCContSchema.getAppFlag()))
        {
            mLCDelPolLog.setIsPolFlag("1");
        }
        else
        {
            mLCDelPolLog.setIsPolFlag("0");
        }

        mLCDelPolLog.setOperator(mOperator);
        mLCDelPolLog.setManageCom(mManageCom);
        mLCDelPolLog.setMakeDate(theCurrentDate);
        mLCDelPolLog.setMakeTime(theCurrentTime);
        mLCDelPolLog.setModifyDate(theCurrentDate);
        mLCDelPolLog.setModifyTime(theCurrentTime);
        mLCDelPolLog.setDeleteReason(mDeleteReason);

        if ("2".equals(tContType))
        {
            System.out.println("进入团单个单删除");

            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = new LCPolSet();

            tLCPolDB.setContNo(tContNo);
            tLCPolSet = tLCPolDB.query();

            LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
            LCGrpContSet tLCGrpContSet = new LCGrpContSet();
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(tGrpContNo);
            tLCGrpContSet = tLCGrpContDB.query();
            tLCGrpContSchema.setSchema(tLCGrpContSet.get(1));
            double premnew = tLCGrpContSchema.getPrem();
            double Amntnew = tLCGrpContSchema.getAmnt();
            int Peoplesnew = tLCGrpContSchema.getPeoples2();
            premnew = premnew - tPrem;
            Amntnew = Amntnew - tAmnt;
            Peoplesnew = Peoplesnew - tPeoples;

            //mMap.put("insert into LOBGrpCont (select * from LCGrpCont where GrpContNo  = '" + tGrpContNo + "')", "INSERT");
            mMap
                    .put(
                            "update LCGrpCont set  prem = (select case when sum(prem) is null  then 0 else sum(prem) end from LCCont where GrpContNo='"
                                    + tGrpContNo
                                    + "' and ContNo!='"
                                    + tContNo
                                    + "'), "
                                    + "Amnt = (select case when sum(Amnt) is null  then 0 else sum(Amnt) end from LCCont where GrpContNo='"
                                    + tGrpContNo
                                    + "' and ContNo!='"
                                    + tContNo
                                    + "'), "
                                    + "Peoples2 = "
                                    + Peoplesnew
                                    + " where GrpContNo  =  '"
                                    + tGrpContNo
                                    + "'", "UPDATE");
            int count = tLCPolSet.size();
            System.out.println(count);
            for (int i = 1; i <= count; i++)
            {
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema.setSchema(tLCPolSet.get(i));
                String tGrpPolNo = tLCPolSchema.getGrpPolNo();
                String tPolNo = tLCPolSchema.getPolNo();
                tPrem = tLCPolSchema.getPrem();
                tAmnt = tLCPolSchema.getAmnt();

                LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
                LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
                LCGrpPolDB tLCGrpPoltDB = new LCGrpPolDB();
                tLCGrpPoltDB.setGrpPolNo(tGrpPolNo);
                tLCGrpPolSet = tLCGrpPoltDB.query();
                tLCGrpPolSchema.setSchema(tLCGrpPolSet.get(1));
                premnew = tLCGrpPolSchema.getPrem();
                Amntnew = tLCGrpPolSchema.getAmnt();
                Peoplesnew = tLCGrpPolSchema.getPeoples2();
                premnew = premnew - tPrem;
                Amntnew = Amntnew - tAmnt;
                Peoplesnew = Peoplesnew - tPeoples;

                //mMap.put("insert into LOBGrpPol (select * from LCGrpPol where GrpPolNo  = '" + tGrpPolNo + "')", "INSERT");
                mMap.put("update LCGrpPol set prem =(select case when sum(prem) is null  then 0 else sum(prem) end from LCPol where GrpPolNo='"
                        + tGrpPolNo + "' and PolNo!='" + tPolNo + "'), "
                        + "Amnt = (select case when sum(Amnt) is null  then 0 else sum(Amnt) end from LCPol where GrpPolNo='"
                        + tGrpPolNo + "' and PolNo!='" + tPolNo + "'), "
                        + "Peoples2 = " + Peoplesnew + " where GrpPolNo  =  '" + tGrpPolNo
                        + "'", "UPDATE");
            }
        }
        String polNoWherePart = getWherePart(tContNo);
        if (!deleteScan)
        {
            mMap.put("insert into LOBInsuredRelated (select * from LCInsuredRelated where PolNo in "
                            + polNoWherePart + ")", "INSERT");
            mMap.put("delete from LCInsuredRelated where PolNo in "
                    + polNoWherePart, "DELETE");

            mMap.put("insert into LOBDuty (select * from LCDuty where PolNo in "
                            + polNoWherePart + ")", "INSERT");
            mMap.put("delete from LCDuty where PolNo in " + polNoWherePart,
                    "DELETE");
            //增加没有换号的保单删除操作
            mMap.put("insert into LOBDuty (select * from LCDuty where contno  ='"
                            + mLCContSchema.getProposalContNo() + "')",
                    "INSERT");
            mMap.put("delete from LCDuty where contno= '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBPrem_1 (select * from LCPrem_1 where PolNo in "
                            + polNoWherePart + ")", "INSERT");
            mMap.put("delete from LCPrem_1 where PolNo in " + polNoWherePart,
                    "DELETE");

            mMap.put("insert into LOBPremToAcc (select * from LCPremToAcc where PolNo in "
                            + polNoWherePart + ")", "INSERT");
            mMap.put("delete from LCPremToAcc where PolNo in " + polNoWherePart,
                    "DELETE");

            mMap.put("insert into LOBGetToAcc (select * from LCGetToAcc where PolNo in "
                            + polNoWherePart + ")", "INSERT");
            mMap.put("delete from LCGetToAcc where PolNo in " + polNoWherePart,
                    "DELETE");

            mMap.put("insert into LOBPol (select * from LCPol where ProposalContNo = '"
                            + mLCContSchema.getProposalContNo() + "')",
                    "INSERT");
            mMap.put("delete from LCPol where ProposalContNo = '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBBnf (select * from LCBnf where PolNo in "
                    + polNoWherePart + ")", "INSERT");
            mMap.put("delete from LCBnf where ContNo ='" + tContNo + "'",
                    "DELETE");
            //增加没有换号的保单删除操作
            mMap.put("insert into LOBBnf (select * from LCBnf where ContNo = '"
                    + mLCContSchema.getProposalContNo() + "')", "INSERT");
            mMap.put("delete from LCBnf where ContNo ='"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBCont (select * from LCCont where ProposalContNo = '"
                            + mLCContSchema.getProposalContNo() + "')",
                    "INSERT");
            mMap.put("delete from LCCont where ProposalContNo = '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");
            mMap.put("delete from LCInsuredlist where ContNo = '"
                    + mLCContSchema.getContNo() + "'", "DELETE");

            mMap.put("insert into LOBPrem (select * from LCPrem where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCPrem where ContNo = '" + tContNo + "'",
                    "DELETE");
            //增加没有换号的保单删除操作
            mMap.put("insert into LOBPrem (select * from LCPrem where ContNo = '"
                            + mLCContSchema.getProposalContNo() + "')",
                    "INSERT");
            mMap.put("delete from LCPrem where ContNo = '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBGet (select * from LCGet where ContNo = '"
                    + tContNo + "')", "INSERT");
            mMap.put("delete from LCGet where ContNo = '" + tContNo + "'",
                    "DELETE");
            //增加没有换号的保单删除操作
            mMap.put("insert into LOBGet (select * from LCGet where ContNo = '"
                    + mLCContSchema.getProposalContNo() + "')", "INSERT");
            mMap.put("delete from LCGet where ContNo = '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBInsureAccFee (select * from LCInsureAccFee where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccFee where ContNo = '" + tContNo
                    + "'", "DELETE");

            mMap.put("insert into LOBInsureAccClassFee (select * from LCInsureAccClassFee where PolNo in "
                                    + polNoWherePart + ")", "INSERT");
            mMap.put("delete from LCInsureAccClassFee where ContNo = '"
                    + tContNo + "'", "DELETE");

            mMap.put("insert into LOBInsureAccClass (select * from LCInsureAccClass where PolNo in "
                            + polNoWherePart + ")", "INSERT");
            mMap.put("delete from LCInsureAccClass where PolNo in "
                    + polNoWherePart, "DELETE");

            mMap.put("insert into LOBInsureAcc (select * from LCInsureAcc where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAcc where ContNo = '" + tContNo + "'",
                    "DELETE");

            mMap.put("insert into LOBInsureAccTrace (select * from LCInsureAccTrace where ContNo = '"
                                    + tContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccTrace where ContNo = '" + tContNo
                    + "'", "DELETE");

            mMap.put("delete from LOBInsured where contno = '" + tContNo + "'",
                    "INSERT");
            mMap.put("insert into LOBInsured (select * from LCInsured where contno = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCInsured where  contno = '" + tContNo + "'",
                    "DELETE");

            mMap.put("delete from LOBCustomerImpart where ContNo = '" + tContNo
                    + "'", "INSERT");
            mMap.put("insert into LOBCustomerImpart (select * from LCCustomerImpart where ContNo = '"
                                    + tContNo + "')", "INSERT");
            mMap.put("delete from LCCustomerImpart where ContNo = '" + tContNo
                    + "'", "DELETE");

            mMap.put("delete from LOBCustomerImpartParams where ContNo = '"
                    + tContNo + "'", "INSERT");
            mMap.put("insert into LOBCustomerImpartParams (select * from LCCustomerImpartParams where ContNo = '"
                                    + tContNo + "')", "INSERT");
            mMap.put("delete from LCCustomerImpartParams where ContNo = '"
                    + tContNo + "'", "DELETE");

            mMap.put("delete from LOBAppnt where ContNo = '"
                    + mLCContSchema.getContNo() + "'", "DELETE");
            mMap.put("insert into LOBAppnt (select * from LCAppnt where ContNo = '"
                            + mLCContSchema.getContNo() + "')", "INSERT");
            mMap.put("delete from LCAppnt where ContNo = '"
                    + mLCContSchema.getContNo() + "'", "DELETE");

            mMap.put("insert into LOBUWError (select * from LCUWError where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCUWError where ContNo = '" + tContNo + "'",
                    "DELETE");

            mMap.put("insert into LOBUWSub (select * from LCUWSub where ProposalContNo = '"
                            + mLCContSchema.getProposalContNo() + "')",
                    "INSERT");
            mMap.put("delete from LCUWSub where ProposalContNo = '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBUWMaster (select * from LCUWMaster where ProposalContNo = '"
                            + mLCContSchema.getProposalContNo() + "')",
                    "INSERT");
            mMap.put("delete from LCUWMaster where ProposalContNo = '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBCUWError (select * from LCCUWError where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCCUWError where ContNo = '" + tContNo + "'",
                    "DELETE");

            mMap.put("insert into LOBCUWSub (select * from LCCUWSub where  ProposalContNo = '"
                            + mLCContSchema.getProposalContNo() + "')",
                    "INSERT");
            mMap.put("delete from LCCUWSub where ProposalContNo = '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBCUWMaster (select * from LCCUWMaster where  ProposalContNo = '"
                            + mLCContSchema.getProposalContNo() + "')",
                    "INSERT");
            mMap.put("delete from LCCUWMaster where  ProposalContNo = '"
                    + mLCContSchema.getProposalContNo() + "'", "DELETE");

            mMap.put("insert into LOBUWReport (select * from LCUWReport where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCUWReport where ContNo = '" + tContNo + "'",
                    "DELETE");

            mMap.put("insert into LOBNotePad (select * from LCNotePad where ProposalContNo = '"
                            + tProposalContNo + "')", "INSERT");
            mMap.put("delete from LCNotePad where ContNo = '" + tContNo + "'",
                    "DELETE");

            mMap.put("insert into LOBRReport (select * from LCRReport where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCRReport where ContNo = '" + tContNo + "'",
                    "DELETE");

            mMap.put("insert into LOBIssuePol (select * from LCIssuePol where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCIssuePol where ContNo = '" + tContNo + "'",
                    "DELETE");

            mMap.put("insert into LOBPENoticeItem (select * from LCPENoticeItem where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCPENoticeItem where ContNo = '" + tContNo
                    + "'", "DELETE");

            mMap.put("insert into LOBPENotice (select * from LCPENotice where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCPENotice where ContNo = '" + tContNo + "'",
                    "DELETE");

//            mMap.put("insert into LOBCSpec (select * from LCCSpec where ContNo = '"
//                            + tContNo + "')", "INSERT");
//            mMap.put("delete from LCCSpec where ContNo = '" + tContNo + "'",
//                    "DELETE");

            mMap.put("insert into LOBSpec (select * from LCSpec where ContNo = '"
                            + tContNo + "')", "INSERT");
            mMap.put("delete from LCSpec where ContNo = '" + tContNo + "'",
                    "DELETE");
            //删除保单锁定问题
            mMap.put("delete from ldsystrace where polno = '"
                    + mLCContSchema.getPrtNo() + "'", "DELETE");
            mMap.put("delete from loprtmanager where otherno = '"
                    + tProposalContNo + "'", "DELETE");
            mMap.put("delete from LZSysCertify where certifyno like '"
                    + mLCContSchema.getPrtNo() + "%'", "DELETE");
            //删除套餐信息
            mMap.put("delete from LCRiskDutyWrap where ContNo = '" + tContNo + "'", "DELETE");
            
            //删除退保及部分领取比例
            mMap.put("delete from LCRiskFeeRate where Prtno = '" + mLCContSchema.getPrtNo() + "'", "DELETE");
            
        }
        
        // 删除综合开拓
        mMap.put("delete from LCExtend where prtno = '" + mLCContSchema.getPrtNo() + "'", "DELETE");

        //如果是个单，则删除个单扫描件
        if ("1".equals(tContType))
        {
            //删除投保书扫描件 标准个险（TB01）和万能险（TBO4） 
            mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='"
                    + tPrtNo + "' and subtype in ('TB01', 'TB04','TB28','TB29'))", "DELETE");
            mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='"
                    + tPrtNo + "'and subtype in ('TB01', 'TB04','TB28','TB29'))", "DELETE");
            mMap.put("delete from es_doc_main where doccode='" + tPrtNo
                    + "'and subtype in ('TB01', 'TB04','TB28','TB29')", "DELETE");
            //删除体检扫描件TB15
            mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode like '"
                    + tPrtNo + "2%' and subtype='TB15')", "DELETE");
            mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode like '"
                    + tPrtNo + "2%'and subtype='TB15')", "DELETE");
            mMap.put("delete from es_doc_main where doccode like '" + tPrtNo
                    + "2%'and subtype='TB15'", "DELETE");
            //删除契调扫描件TB16
            mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode like '"
                    + tPrtNo + "%' and subtype='TB16')", "DELETE");
            mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode like '"
                    + tPrtNo + "%'and subtype='TB16')", "DELETE");
            mMap.put("delete from es_doc_main where doccode like '" + tPrtNo
                    + "%'and subtype='TB16'", "DELETE");
            //删除核保通知书扫描件TB21
            mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode like '"
                    + tPrtNo + "9%' and subtype='TB21')", "DELETE");
            mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode like '"
                    + tPrtNo + "9%'and subtype='TB21')", "DELETE");
            mMap.put("delete from es_doc_main where doccode like '" + tPrtNo
                    + "9%'and subtype='TB21'", "DELETE");
            //删除问题件通知书扫描件TB22
            mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode like '"
                    + tPrtNo + "1%' and subtype='TB22')", "DELETE");
            mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode like '"
                    + tPrtNo + "1%'and subtype='TB22')", "DELETE");
            mMap.put("delete from es_doc_main where doccode like '" + tPrtNo
                    + "1%'and subtype='TB22'", "DELETE");
            
            //2014-8-6添加
            //删除家庭单(TB27)
            mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='"
                    + tPrtNo + "' and subtype='TB27')", "DELETE");
            mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='"
                    + tPrtNo + "'and subtype='TB27')", "DELETE");
            mMap.put("delete from es_doc_main where doccode='" + tPrtNo
                    + "'and subtype='TB27'", "DELETE");
            
            //删除新增表信息
            mMap.put("delete from lccontsub where prtno='" + tPrtNo + "' ", "DELETE");
            mMap.put("delete from lstransinfo where prtno='" + tPrtNo + "' ", "DELETE");
            
            //税优团体投保时，需把导入被保人中的prtno置空
            mMap.put("update lsinsuredlist set prtno=null where prtno='" + tPrtNo + "' ", "UPDATE");
            
            //删除保单操作轨迹表
            mMap.put("delete from LDSysTrace where PolNo = '" + tPrtNo + "'", "DELETE");
            
            //校验是否银行在途
        	String bankOnTheWay = new ExeSQL().getOneValue("select db2inst1.nvl(count(1), 0) from LJSPay where OtherNo = '" 
        			+ tPrtNo + "' and OtherNoType in ('9','16') and BankOnTheWayFlag = '1'");
        	if(!bankOnTheWay.equals("0.0"))
        	{
        		CError tError = new CError();
                tError.moduleName = "UWSendPrintBL";
                tError.functionName = "prepareBankData";
                tError.errorMessage = "保单银行在途，请待回盘之后进行相同操作！";
                this.mErrors.addOneError(tError);
                boolean abool= new ExeSQL().execUpdateSQL("UPDATE LJTEMPFEE SET STATE=null WHERE otherno ='" + mLCContSchema.getPrtNo()+"'") ;
				if(!abool){
				  //LJTEMPFEE中state不需要进行修改
				  mErrors.addOneError(new CError("回执删除状态出错")) ;
				  return false ;
				}

                
                return false;
        	}
            
        	MMap mmap = new MMap();
        	mmap.put("update ljspay set CanSendBank = '1' where otherno = '"
                     + tPrtNo + "' and (BankOnTheWayFlag is null or BankOnTheWayFlag = '0')", "UPDATE");
        	VData mVData = new VData();
            mVData.add(mmap);
            PubSubmit mPubSubmit = new PubSubmit();
            if (!mPubSubmit.submitData(mVData, "UPDATE"))
            {
                this.mErrors.copyAllErrors(mPubSubmit.mErrors);
                return false;
            }
        	//删除非银行在途，且锁定的财务应收记录
        	MMap map = new MMap();
            SSRS getNoticeNo = new ExeSQL().execSQL("select distinct a.tempfeeno from ljtempfee "
                                    + " a left join ljspay b "
                                    + " on a.tempfeeno = b.getnoticeno "
                                    + " and (b.BankOnTheWayFlag is null or b.BankOnTheWayFlag = '0') "
                                    + " and b.OtherNoType in ('9','16') "
                                    + " and b.CanSendBank = '1' "
                                    + " where a.otherno='"
                                    + tPrtNo +"'"
                                    + " and a.enteraccdate is null "
                                    );
             for (int i = 1; i <= getNoticeNo.MaxRow; i++) {
                 String getNoticeNoString = getNoticeNo.GetText(i, 1);
                 map.put("delete from LJSPayB where GetNoticeNo = '"
                          + getNoticeNoString + "'", "DELETE");
                 //备份到B表
                 map.put("insert into LJSPayB (select GetNoticeNo, OtherNo, OtherNoType, AppntNo, SumDuePayMoney, PayDate, "
                          + "BankOnTheWayFlag, BankSuccFlag, SendBankCount, ApproveCode, ApproveDate, SerialNo, Operator, MakeDate, "
                          + "MakeTime, ModifyDate, ModifyTime, ManageCom, AgentCom, AgentType, BankCode, BankAccNo, RiskCode, "
                          + "AgentCode, AgentGroup, AccName, StartPayDate, PayTypeFlag, '7', '', '1',markettype,salechnl "
                          + "from LJSPay where GetNoticeNo = '"
                          + getNoticeNoString + "')","INSERT");
                 //删除未到帐的财务暂收记录
                 map.put("delete from LJSPay where GetNoticeNo = '"
                          + getNoticeNoString + "'", "DELETE");
                 map.put("delete from ljtempfeeclass where tempfeeno = '"
                          + getNoticeNoString+"'"+" and EnterAccDate is null ", "DELETE");
                 map.put("delete from ljtempfee where tempfeeno = '"
                          + getNoticeNoString + "'"+" and OtherNoType = '4' and EnterAccDate is null ", "DELETE");
             }//end modify
             
             VData tVData = new VData();
             tVData.add(map);
             PubSubmit tPubSubmit = new PubSubmit();
             if (!tPubSubmit.submitData(tVData, "DELETE&INSERT"))
             {
                 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                 return false;
             }
            
            //缴费方式是银行投保，需删除应收记录
            if(mLCContSchema.getPayMode() != null && mLCContSchema.getPayMode().equals("4"))
            {
            	//校验是否银行在途
//            	String bankOnTheWay = new ExeSQL().getOneValue("select db2inst1.nvl(count(1), 0) from LJSPay where OtherNo = '" 
//            			+ tPrtNo + "' and OtherNoType = '9' and BankOnTheWayFlag = '1'");
//            	if(bankOnTheWay.equals("0.0"))
//            	{
//            		//删除非银行在途，且锁定的财务应收记录
//                    SSRS getNoticeNo = new ExeSQL().execSQL("select GetNoticeNo from LJSPay where OtherNo = '" + tPrtNo
//                    		+ "' and OtherNoType = '9' and (BankOnTheWayFlag is null or BankOnTheWayFlag = '0') and CanSendBank = '1'");
//                    for(int i = 1; i <= getNoticeNo.MaxRow; i++)
//                    {
//                    	String getNoticeNoString = getNoticeNo.GetText(i, 1);
//                        System.out.println("******  " + getNoticeNoString + "  ******");
//                    	mMap.put("delete from LJSPayB where GetNoticeNo = '" + getNoticeNoString + "'", "DELETE");
//                    	mMap.put("insert into LJSPayB (select GetNoticeNo, OtherNo, OtherNoType, AppntNo, SumDuePayMoney, PayDate, "
//                    			+ "BankOnTheWayFlag, BankSuccFlag, SendBankCount, ApproveCode, ApproveDate, SerialNo, Operator, MakeDate, "
//                    			+ "MakeTime, ModifyDate, ModifyTime, ManageCom, AgentCom, AgentType, BankCode, BankAccNo, RiskCode, "
//                    			+ "AgentCode, AgentGroup, AccName, StartPayDate, PayTypeFlag, '7', '', '1' "
//                    			+ "from LJSPay where GetNoticeNo = '" + getNoticeNoString + "')", "INSERT");
//                    	mMap.put("delete from LJSPay where GetNoticeNo = '" + getNoticeNoString + "'", "DELETE");
//                    }
//                   删除未到帐的财务暂收记录
//                	mMap.put("delete from LJTempFee where OtherNo = '" + tPrtNo + "' and EnterAccDate is null", "DELETE");
//                	mMap.put("delete from LJTempFeeClass where TempFeeNo in (select TempFeeNo from LJTempFee where OtherNo = '"
//                			+ tPrtNo + "' and EnterAccDate is null)", "DELETE");
                    
                    //modify by yingxl 2008-04-18 
                    //删除非银行在途，且锁定的财务应收记录
//                    SSRS getNoticeNo = new ExeSQL().execSQL("select distinct a.tempfeeno from ljtempfee "
//                                            + " a left join ljspay b "
//                                            + " on a.tempfeeno = b.getnoticeno "
//                                            + " and (b.BankOnTheWayFlag is null or b.BankOnTheWayFlag = '0') "
//                                            + " and b.OtherNoType = '9' "
//                                            + " and b.CanSendBank = '1' "
//                                            + " where a.otherno='"
//                                            + mLCContSchema.getPrtNo()+"'"
//                                            + " and a.enteraccdate is null "
//                                            );
//                     for (int i = 1; i <= getNoticeNo.MaxRow; i++) {
//                         String getNoticeNoString = getNoticeNo.GetText(i, 1);
//                         System.out.println("******  " + getNoticeNoString + "  ******");
//                         mMap.put("delete from LJSPayB where GetNoticeNo = '"
//                                  + getNoticeNoString + "'", "DELETE");
//                         //备份到B表
//                         mMap.put("insert into LJSPayB (select GetNoticeNo, OtherNo, OtherNoType, AppntNo, SumDuePayMoney, PayDate, "
//                                  + "BankOnTheWayFlag, BankSuccFlag, SendBankCount, ApproveCode, ApproveDate, SerialNo, Operator, MakeDate, "
//                                  + "MakeTime, ModifyDate, ModifyTime, ManageCom, AgentCom, AgentType, BankCode, BankAccNo, RiskCode, "
//                                  + "AgentCode, AgentGroup, AccName, StartPayDate, PayTypeFlag, '7', '', '1' "
//                                  + "from LJSPay where GetNoticeNo = '"
//                                  + getNoticeNoString + "')","INSERT");
//                         //删除未到帐的财务暂收记录
//                         mMap.put("delete from LJSPay where GetNoticeNo = '"
//                                  + getNoticeNoString + "'", "DELETE");
//                         mMap.put("delete from ljtempfeeclass where tempfeeno = '"
//                                  + getNoticeNoString+"'"+" and EnterAccDate is null ", "DELETE");
//                         mMap.put("delete from ljtempfee where tempfeeno = '"
//                                  + getNoticeNoString + "'"+" and OtherNoType = '4' and EnterAccDate is null ", "DELETE");
//                     }//end modify
//            	}
//            	else
//            	{
//                    this.mErrors.addOneError(new CError("保单银行在途，不能进行新单删除！"));
//            		return false;
//            	}
            }
            //查询个人保单的工作流节点ID
            //2014-8-6添加家庭单(ProcessID='0000000013')工作流中数据删除
            String strSql = "select distinct MissionId from ("
                + "select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
                + "and MissionProp1 = '" + tPrtNo + "' and ActivityId in "
                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp1') "
                + "union select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
                + "and MissionProp2 = '" + tPrtNo + "' and ActivityId in "
                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp2') "
                + "union select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
                + "and MissionProp5 = '" + tPrtNo + "' and ActivityId in "
                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp5') "
                + ") as X";
            System.out.println(strSql);
            SSRS tSSRS = new ExeSQL().execSQL(strSql);
            if (tSSRS.getMaxRow() > 1) {
                this.mErrors.addOneError(new CError("保单工作流任务ID查询失败！印刷号：" + tPrtNo));
                return false;
            }
            else if(tSSRS.getMaxRow() == 1)
            {
                //删除保单工作流节点
                LWMissionDB tLWMissionDB = new LWMissionDB();
                tLWMissionDB.setMissionID(tSSRS.GetText(1, 1));
                LWMissionSet tLWMissionSet = tLWMissionDB.query();
                //备份工作流节点
                LBMissionSet tLBMissionSet = new LBMissionSet();
                LBMissionSchema tLBMissionSchema;
                
                Reflections tReflections = new Reflections();
                for (int i = 1; i <= tLWMissionSet.size(); i++) {
                    tLBMissionSchema = new LBMissionSchema();
                    tReflections.transFields(tLBMissionSchema, tLWMissionSet.get(i));

                    String serialNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
                    tLBMissionSchema.setSerialNo(serialNo);
                    tLBMissionSchema.setActivityStatus("3"); //节点任务执行完毕
                    tLBMissionSchema.setLastOperator(mGlobalInput.Operator);
                    tLBMissionSchema.setMakeDate(theCurrentDate);
                    tLBMissionSchema.setMakeTime(theCurrentTime);
                    tLBMissionSchema.setModifyDate(theCurrentDate);
                    tLBMissionSchema.setModifyTime(theCurrentTime);
                    tLBMissionSet.add(tLBMissionSchema);
                }
                mMap.put(tLBMissionSet, SysConst.DELETE_AND_INSERT);
                mMap.put(tLWMissionSet, SysConst.DELETE);
            }
        }
        
        mMap.put("delete from LCGrpImportLog where ContNo = '" + tContNo + "'",
                "DELETE");
        if (!deleteScan)
        {
            mMap.put(mLCDelPolLog, "INSERT");
        }
        if (mRelatePremDelete != null && mRelatePremDelete.equals("1"))
        {
            mMap.put("update  lcinsuredlist set state = '0' where grpcontno='"
                    + tGrpContNo + "'", "UPDATE");
        }

        if (!dealBPOData(tContType, tPrtNo))
        {
            return false;
        }

        return true;
    }

    /**
     * dealBPOData
     * 删除外包数据
     * @param cPrtNo String
     * @return boolean
     */
    private boolean dealBPOData(String tContType, String cPrtNo)
    {
        if (!"1".equals(tContType) || cPrtNo == null)
        {
            return true;
        }
        String sql = "select distinct a.BPOBatchNo, a.ContID from BPOLCPol a "
                + "where PrtNo = '" + cPrtNo + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);

        Reflections ref = new Reflections();

        String[] tBPOContTables = { "BPOLCAppnt", "BPOLCInsured", "BPOLCPol",
                "BPOLCBnf", "BPOLCImpart", "BPOIssue" };
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            for (int index = 0; index < tBPOContTables.length; index++)
            {
                sql = "insert into " + tBPOContTables[index] + "B "
                        + "   (select (select count(1) + 1 from "
                        + tBPOContTables[index] + "B"
                        + "           where BPOBatchNo = a.BPOBatchNo "
                        + "              and ContID = a.ContID),  "
                        + "   a.* from " + tBPOContTables[index] + " a "
                        + "    where BPOBatchNo = '" + tSSRS.GetText(i, 1)
                        + "' " + "       and ContID = '" + tSSRS.GetText(i, 2)
                        + "' " + "   ) ";
                mMap.put(sql, SysConst.INSERT);

                sql = "delete from " + tBPOContTables[index] + " "
                        + "where BPOBatchNo = '" + tSSRS.GetText(i, 1) + "' "
                        + "   and ContID = '" + tSSRS.GetText(i, 2) + "' ";
                mMap.put(sql, SysConst.INSERT);
            }
        }

        //将对外包任务状态的处理移出 for 循环 -- 处理在 BPOLBPol 中没有记录的保单的新单删除处理
        //修改外包任务状态，生成轨迹信息
        BPOMissionStateDB tBPOMissionStateDB = new BPOMissionStateDB();
        if(tSSRS.getMaxRow() > 0)
        {
        	tBPOMissionStateDB.setBPOBatchNo(tSSRS.GetText(1, 1));
        }
        tBPOMissionStateDB.setBussNoType("TB");
        tBPOMissionStateDB.setBussNo(cPrtNo);
        BPOMissionStateSet set = tBPOMissionStateDB.query();
        for (int j = 1; j <= set.size(); j++)
        {
            set.get(j).setState("06");
            set.get(j).setOperator(this.mOperate);
            set.get(j).setModifyDate(PubFun.getCurrentDate());
            set.get(j).setModifyTime(PubFun.getCurrentTime());
            mMap.put(set.get(j), SysConst.UPDATE);

            BPOMissionDetailStateSchema schema = new BPOMissionDetailStateSchema();
            ref.transFields(schema, set.get(j));
            schema.setOperateType(BPO.OPERATE_TYPE_DELETE);
            schema.setOperateResult("1");

            sql = "select max(SequenceNo) + 1 "
                    + "from BPOMissionDetailState " + "where BPOBatchNo='"
                    + set.get(j).getBPOBatchNo() + "'"
                    + "   and MissionID = '" + schema.getMissionID() + "' "
                    + "   and BussNoType = 'TB' " + "   and BussNo = '"
                    + schema.getBussNo() + "' ";
            String nextNo = new ExeSQL().getOneValue(sql);
            if ("".equals(nextNo) || "null".equals(nextNo))
            {
                nextNo = "1";
            }
            schema.setSequenceNo(nextNo);
            schema.setRemark("新单删除");
            schema.setOperator(this.mOperate);
            schema.setModifyDate(PubFun.getCurrentDate());
            schema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(schema, SysConst.INSERT);
        }

        return true;
    }

    /**
     * getWherePart
     *
     * @param tContNo String
     * @return String
     */
    private String getWherePart(String tContNo)
    {
        String strSql = "select polno from lcpol where contno='" + tContNo
                + "'";
        SSRS ssrs = (new ExeSQL()).execSQL(strSql);
        StringBuffer wherePart = new StringBuffer();
        wherePart.append(" (");
        if (ssrs != null && ssrs.getMaxRow() > 0)
        {
            for (int i = 1; i <= ssrs.getMaxRow(); i++)
            {
                wherePart.append("'");
                wherePart.append(ssrs.GetText(i, 1));
                wherePart.append("'");
                if (i != ssrs.getMaxRow())
                {
                    wherePart.append(",");
                }
                else
                {
                    wherePart.append(")");
                }
            }
            return wherePart.toString();
        }
        else
        {
            return "('')";
        }
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {
        //记录当前操作员
        mResult.clear();
        //add by yingxl 2008-04-19     
        // 将锁定的字段重新赋值，恢复到以前
//        mMap.put("UPDATE LJTEMPFEE SET STATE='1' WHERE otherno ='"
//                + mLCContSchema.getPrtNo(), "update") ;
//        mMap.put("udpate ljspay set cansendbank ='0' where otherno='"
//                + mLCContSchema.getPrtNo()+"'", "update") ;
        mResult.add(mMap);
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }
}
