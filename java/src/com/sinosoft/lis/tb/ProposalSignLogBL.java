package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class ProposalSignLogBL
{
    /**错误的容器，错程序报错，可通过该变量获取出错信息*/
    public CErrors mErrors = new CErrors();

    private String mTransact = null;
    private GlobalInput mGI = null;
    private LCSignLogSchema mLCSignLogSchema = null;

    private MMap map = new MMap();  //存储处理后的数据，提交操作时使用

    public ProposalSignLogBL()
    {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData：存储外部数据集合
     * @param cOperate String：数据的操作方式
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        VData data = new VData(); //操作提交接口的数据容器
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();  //公共数据库提交类
        if(!tPubSubmit.submitData(data, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("操作提交失败");
            return false;
        }

        return true;
    }

    /**
     * 方法参数同submitData
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        mTransact = cOperate;

        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * checkData
     * 校验传入的数据是否符合业务规则要求
     * @return boolean：符合：true，否则false
     */
    private boolean checkData()
    {
        return true;
    }

    /**
    * 进行业务处理
    * @return boolean
    */
   public boolean dealData()
   {
       //保存
       if(SysConst.INSERT.equals(mTransact))
       {
           mLCSignLogSchema.setSeriNo(PubFun1.CreateMaxNo("SignLog", 10));
           mLCSignLogSchema.setMakeDate(PubFun.getCurrentDate());
           mLCSignLogSchema.setMakeTime(PubFun.getCurrentTime());
           map.put(mLCSignLogSchema, mTransact);
       }
       //修改
       else if(SysConst.UPDATE.equals(mTransact))
       {
           LCSignLogDB db = mLCSignLogSchema.getDB();
           if(!db.getInfo())
           {
               mErrors.addOneError("请传入流水号进行变更");
               return false;
           }

           db.setContNo(mLCSignLogSchema.getContNo());
           db.setContType(mLCSignLogSchema.getContType());
           db.setErrInfo(mLCSignLogSchema.getErrInfo());
           db.setMakeDate(PubFun.getCurrentDate());
           db.setMakeTime(PubFun.getCurrentTime());
           map.put(db.getSchema(), mTransact);
       }
       //删除
       else if(SysConst.DELETE.equals(mTransact))
       {
           if(mLCSignLogSchema.getSeriNo() == null
               || mLCSignLogSchema.getSeriNo().equals(""))
           {
               mErrors.addOneError("请传入流水号");
               return false;
           }
           //目前不需要增加其他处理

           map.put(mLCSignLogSchema, mTransact);
       }

       return true;
   }

   /**
        * 接收传递的参数
        * @param cInputData VData
        * @return boolean
        */
       public boolean getInputData(VData cInputData)
       {
           mGI = (GlobalInput) cInputData
                          .getObjectByObjectName("GlobalInput", 0);
           mLCSignLogSchema = (LCSignLogSchema) cInputData
                                .getObjectByObjectName("LCSignLogSchema", 0);
           if(mGI == null)
           {
               mErrors.addOneError("请传入操作员信息");
               return false;
           }
           if(mLCSignLogSchema == null)
           {
               mErrors.addOneError("请传入账户信息");
               return false;
           }

           return true;
    }

    public static void main(String[] args)
    {
        ProposalSignLogBL proposalsignlogbl = new ProposalSignLogBL();
    }
}
