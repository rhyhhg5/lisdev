package com.sinosoft.lis.tb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.diskimport.MultiSheetImporter;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.VData;

/**
 * 可以复用将excel文件存入数据库 这层不需要修改 内含XML和ST.JAVA生成方法，
 * 按需自己修改 只需按需生成****ST.java
 * 
 * @author lw
 *
 */

public class OnlyDiskImport {
	public OnlyDiskImport() {

	}

	/** 从xls文件读入Sheet的开始行 */
	private static final int STARTROW = 2;
	/** 使用默认的导入方式 */
	private MultiSheetImporter importer;
	/** 错误处理 */
	public CErrors mErrors = new CErrors();
	/** Sheet Name */
	private String[] mSheetName;
	/** Schema 的名字 */
	private String schemaName[];
	private String setName[];
	/** Sheet对应table的名字 */
	private String[] mTableNames;

	public OnlyDiskImport(String fileName, String configFileName,
			String[] sheetName, String[] TableNames, String schemaName[],
			String setName[]) {
		mSheetName = sheetName;
		mTableNames = TableNames;
		this.schemaName = schemaName;
		this.setName = setName;
		importer = new MultiSheetImporter(fileName, configFileName, mSheetName);
	}

	/**
	 * 执行导入
	 * 
	 * @return boolean
	 */
	public boolean doImport() {
		System.out.println(" Into GrpDiskImport doImport...");
		System.out.println(mTableNames[0]);
		importer.setTableName(mTableNames);
		importer.setMStartRows(STARTROW);
		if (!importer.doImport()) {
			mErrors.copyAllErrors(importer.mErrors);
			return false;
		}

		System.out.println("版本号效验通过，GrpDiskImport完成。");
		return true;
	}

	/**
	 * 得到导入结果
	 * 
	 * @return SchemaSet
	 */
	public HashMap getResult() {
		return importer.getResult();
	}

	/**
	 * getSchemaSet 获取从MutiSheetImporter获得的SchemaSet
	 *
	 * @return LCInsuredListSet
	 * @throws Exception
	 */
	public List<SchemaSet> getSchemaList() throws Exception {
		List<SchemaSet> setList = new ArrayList();
		HashMap tHashMap = importer.getResult();
		for (int i = 0; i < mSheetName.length; i++) {			
//			System.out.println(tHashMap.size());
//			System.out.println(tHashMap.toString());
			SchemaSet s1 = (SchemaSet) tHashMap.get(mSheetName[i]);
			System.out.println(mSheetName[i]);
			System.out.println(s1.size());
			setList.add(s1);

		}

		return setList;
	}

	// 输出文件(后缀自己定)  会覆盖之前文件，会续写
	public static void writeTxt2(String bl, String filepath) {
		FileOutputStream tFileOutputStream = null;
		OutputStreamWriter tOutputStreamWriter = null;
		BufferedWriter mBufferedWriter = null;
		try {
			createFile(filepath);
			tFileOutputStream = new FileOutputStream(filepath);
			// tFileOutputStream = new FileOutputStream(url + filename);
			tOutputStreamWriter = new OutputStreamWriter(tFileOutputStream,
					"utf-8");

			mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
			mBufferedWriter.write(bl);
			mBufferedWriter.newLine();
			mBufferedWriter.flush();

		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			try {
				mBufferedWriter.close();
				tOutputStreamWriter.close();
				tFileOutputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/*
	 * 创建目录 如果存在则不创建
	 */
	public static boolean createDir(String destDirName) {
		File dir = new File(destDirName);
		if (dir.exists()) {// 判断目录是否存在

			return true;
		}

		if (dir.mkdirs()) {// 创建目标目录
			System.out.println("创建目录成功！" + destDirName);
			return true;
		} else {
			System.out.println("创建目录失败！");
			return false;
		}
	}

	/*
	 * 创建文档，存在则不创建
	 */

	public static boolean createFile(String FileName) throws IOException {
		File dir = new File(FileName);
		if (dir.exists()) {// 判断目录是否存在

			return true;
		}

		if (dir.createNewFile()) {// 创建目标目录
			System.out.println("创建文件成功！" + FileName);
			return true;
		} else {
			System.out.println("创建文件失败！");
			return false;
		}

	}

	// 用于生成导入EXCEL时需要的配置文件
	public static void createxml(String[] tables, String[] confignames) {
		StringBuffer sb = new StringBuffer();
		sb.append("<CONFIG>\n");
		for (String table : tables) {
			int a = 0;
			sb.append("<" + confignames[a] + ">\n");
			sb.append("<COL0></COL0>\n");
			System.out.println("START CREATEXML!");
			Connection conn = null;
			try {
				// 为了查找顺序列名的sql，好像必须有数据，未测试空表
				String sql = "select * from db2inst1." + table
						+ " fetch first 1 rows only with ur";
				String columnName = "";

				conn = DBConnPool.getConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				ResultSetMetaData data = rs.getMetaData();
				rs.next();

				int j = 1;
				// 由于我从第二个列开始需要导入所以i=2，如果表格全部导入为i=1
				for (int i = 2; i <= data.getColumnCount(); i++) {
					// 获得指定列的列名
					columnName = data.getColumnName(i);
					sb.append("<COL" + j + ">" + columnName + "</COL" + j
							+ ">\n");
					j += 1;
				}
				sb.append("</" + confignames[a] + ">\n");
				rs.close();
				a += 1;
			} catch (Exception e) {

			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		sb.append("</CONFIG>\n");
		String sqlString = sb.toString();

		writeTxt2(sqlString, "F:/XFYJ.xml");
		System.out.println("END");
	}

	// 该方法于导出数据库信息无关
	// 用于生成导入EXCEL时需要的配置文件,只输出到所需列数,最后一列不会取到
	// 多个表格可能范围不一致，可以分别调用，自己拼凑
	public static void createxml(String[] tables, String[] confignames,
			int startcol, int endcol) {
		StringBuffer sb = new StringBuffer();
		sb.append("<CONFIG>\n");
		for (String table : tables) {
			int a = 0;
			sb.append("<" + confignames[a] + ">\n");
			sb.append("<COL0></COL0>\n");
			System.out.println("START CREATEXML!");
			Connection conn = null;
			try {
				// 为了查找顺序列名的sql，好像必须有数据，未测试空表
				String sql = "select * from db2inst1." + table
						+ " fetch first 1 rows only with ur";
				String columnName = "";

				conn = DBConnPool.getConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				ResultSetMetaData data = rs.getMetaData();
				rs.next();
				if (endcol > data.getColumnCount()) {
					endcol = data.getColumnCount();
				}

				int j = 1;
				// 由于我从第二个列开始需要导入所以i=2，如果表格全部导入为i=1
				for (int i = startcol; i <= endcol; i++) {
					// 获得指定列的列名
					columnName = data.getColumnName(i);
					sb.append("<COL" + j + ">" + columnName + "</COL" + j
							+ ">\n");
					j += 1;
				}
				sb.append("</" + confignames[a] + ">\n");
				a += 1;
				rs.close();
				
			} catch (Exception e) {

			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		sb.append("</CONFIG>\n");
		String sqlString = sb.toString();

		writeTxt2(sqlString, "F:/XFYJ.xml");
		System.out.println("END");
	}

	// 创建上层java类，目前只是使用与XFYJ 需要生成别的自己修改
	public static void createxceljava(String[] tables, String sheetnames) {
		StringBuffer sb = new StringBuffer();
		StringBuffer schema = new StringBuffer();
		StringBuffer set = new StringBuffer();
		sb.append("package com.sinosoft.lis.bq;\n");
		sb.append("import com.sinosoft.utility.*;\n");
		sb.append("import com.sinosoft.lis.pubfun.*;\n");
		sb.append("import com.sinosoft.lis.schema.*;\n");
		sb.append("import com.sinosoft.lis.vschema.*;\n");
		sb.append("import com.sinosoft.lis.db.*;\n");
		sb.append("import com.sinosoft.lis.tb.OnlyDiskImport;\n");
		sb.append("import java.util.*;\n");
		sb.append("public class " + tables[0] + "ST{\n");
		sb.append("private GlobalInput mGlobalInput;\n");
		sb.append("private VData mResult = new VData();\n");
		sb.append("private String mEdorNo = null;\n");

		for (int i = 0; i < tables.length; i++) {
			schema.append("\"" + tables[i] + "Schema" + "\",");
			set.append("\"" + tables[i] + "Set" + "\",");
		}
		schema.deleteCharAt(schema.lastIndexOf(","));
		set.deleteCharAt(set.lastIndexOf(","));
		String[] schemaname = schema.toString().replaceAll("\"", "").split(",");
		String[] setname = set.toString().replaceAll("\"", "").split(",");
		sb.append("private  final String [] schemaName={" + schema + "};\n");
		sb.append("private  final String [] setName={" + set + "};\n");
		sb.append("private String[] sheetName = " + sheetnames + "\n");
		sb.append("private String configName = \"XFYJ.xml\";\n");
		sb.append("private  String[] mTableNames={"
				+ schema.toString().replaceAll("Schema", "") + "};\n");
		sb.append("public boolean doAdd(String path, String fileName,GlobalInput mGlobalInput,String Edorno) throws Exception {\n");
		sb.append("this.mGlobalInput=mGlobalInput;\n");
		sb.append("this.mEdorNo=Edorno;\n");
		sb.append("OnlyDiskImport importFile = new OnlyDiskImport(path + fileName,"
				+ "path + configName,sheetName,mTableNames,schemaName,setName);\n");
		sb.append("System.out.println(path + configName);\n");
		sb.append("if (!importFile.doImport()) {\n");
		sb.append("return false;\n");
		sb.append("}\n");

		// 如果很多表单可写成循环
		sb.append("List <SchemaSet> list= new ArrayList();\n");
		sb.append("list=importFile.getSchemaList();\n");
		sb.append(setname[0] + " m" + setname[0] + "=(" + setname[0]
				+ ")list.get(0);" + "\n");
		sb.append("MMap map = new MMap();\n");
		sb.append("  for ( int i = 1; i <= m" + setname[0]
				+ ".size(); i++) {\n");
		sb.append("addone" + schemaname[0] + "(map, m" + setname[0]
				+ ".get(i));\n");
		sb.append("}\n");

		sb.append(" if (!submitData(map)) {\n");
		sb.append("return false;\n");
		sb.append("}\n");
		sb.append("return true;\n");

		sb.append(setname[1] + " m" + setname[1] + "=(" + setname[1]
				+ ")list.get(1);" + "\n");
		sb.append("MMap map = new MMap();\n");
		sb.append("  for ( int i = 1; i <= m" + setname[1]
				+ ".size(); i++) {\n");
		sb.append("addone" + schemaname[1] + "(map, m" + setname[1]
				+ ".get(i));\n");
		sb.append("}\n");

		sb.append(" if (!submitData(map)) {\n");
		sb.append("return false;\n");
		sb.append("}\n");
		sb.append("return true;\n");

		sb.append("}\n");

		sb.append("private void addone" + schemaname[0] + "(MMap map,\n");
		sb.append(schemaname[0] + " m" + schemaname[0] + ") {\n");
		sb.append(schemaname[0] + " t" + schemaname[0] + "= m" + schemaname[0]
				+ ";\n");
		// 保存表格中从页面传过来的值
		sb.append("t" + schemaname[0] + ".setEdorNo(mEdorNo);" + "\n");
		// 保存表单必须保存的额外信息
		sb.append("t" + schemaname[0]
				+ ".setMakeDate(PubFun.getCurrentDate());" + "\n");
		sb.append("t" + schemaname[0]
				+ ".setModifyDate(PubFun.getCurrentDate());" + "\n");
		sb.append("t" + schemaname[0]
				+ ".setMakeTime(PubFun.getCurrentTime());" + "\n");
		sb.append("t" + schemaname[0]
				+ ".setModifyTime( PubFun.getCurrentTime());" + "\n");
		sb.append("t" + schemaname[0]
				+ ".setOperator( mGlobalInput.Operator;);" + "\n");
		sb.append("t" + schemaname[0]
				+ ".setOperator( mGlobalInput.ManageCom;);" + "\n");
		sb.append("map.put(t" + schemaname[0] + ", \"INSERT\");");
		sb.append("}\n");

		sb.append("private void addone" + schemaname[1] + "(MMap map,\n");
		sb.append(schemaname[1] + " m" + schemaname[1] + ") {\n");
		sb.append(schemaname[1] + " t" + schemaname[1] + "= m" + schemaname[1]
				+ ";\n");

		sb.append("t" + schemaname[1] + ".setEdorNo(mEdorNo);" + "\n");

		sb.append("map.put(t" + schemaname[1] + ", \"INSERT\");");
		sb.append("}\n");

		sb.append("private boolean submitData(MMap map) {\n");
		sb.append("VData data = new VData();\n");
		sb.append("data.add(map);\n");
		sb.append(" PubSubmit tPubSubmit = new PubSubmit();\n");
		sb.append("if (!tPubSubmit.submitData(data, \"\")) {\n");
		sb.append("return false;}\n");
		sb.append("return true;");
		sb.append("}\n");
		sb.append("}\n");

		String sqlString = sb.toString();

		writeTxt2(sqlString, "F:/XFYJST.java");
		System.out.println("END");

	}

	public static void main(String[] args) throws Exception {
		String[] tables = { "XFYJAPPNT", "XFYJINSURED" };
		String[] conf = { "AppntInfo", "InsuredInfo" };

		String[] tablenames = { "XFYJAppnt", "XFYJInsured" };
		// String [] tablenames={"LCCont","LCPol"};
		String sheetnames = "{\"AppntInfo\", \"InsuredInfo\"};";
		createxceljava(tablenames, sheetnames);
		 //createxml(tables, conf);

	}

}
