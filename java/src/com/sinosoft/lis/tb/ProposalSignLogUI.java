package com.sinosoft.lis.tb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class ProposalSignLogUI
{
    public CErrors mErrors = new CErrors();

    public ProposalSignLogUI()
    {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData：存储外部数据集合
     * @param cOperate String：数据的操作方式
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        ProposalSignLogBL bl = new ProposalSignLogBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        ProposalSignLogUI bl = new ProposalSignLogUI();
    }
}
