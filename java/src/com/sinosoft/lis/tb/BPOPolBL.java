package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOPolBL
{
    public final static String ONE_INSURED_POL = "1";

    public final static String ALL_INSURED_POL = "2";

    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private BPOLCPolSet mBPOLCPolSet = null;

    private String mDealType = null;

    private Reflections ref = new Reflections();

    private MMap map = new MMap();

    public BPOPolBL()
    {

    }

    /**
     * 数据提交的公共方法，调用getSubmitMap进行业务处理，
     * 处理成功后将返回结果保存入内部VData对象中
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if (getSubmitMap(cInputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据库失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getSubmitMMap
     * 外包调用的接口方法。本方法调用checkData进行数据校验，调用dealData进行业务逻辑处
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return MMap：处理成功，返回处理后的带提交数据库集合, 处理失败，返回null
     */
    public MMap getSubmitMap(VData cInputData, String operate)
    {
        if (!getInputData(cInputData))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法，是否可进行业务逻辑处理
     * @return boolean：true合法，false不合法
     */
    private boolean checkData()
    {
        if (mGI == null)
        {
            CError tError = new CError();
            tError.moduleName = "BPOInsuredBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有接收到操作员信息，可能已超时";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (mBPOLCPolSet == null || mBPOLCPolSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOInsuredBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有传入险种信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        for (int i = 1; i <= mBPOLCPolSet.size(); i++)
        {
            if (mBPOLCPolSet.get(i).getContID() == null
                    || mBPOLCPolSet.get(i).getContID().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "BPOPolBL";
                tError.functionName = "checkData";
                tError.errorMessage = "请传入合同ID号";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            if (ONE_INSURED_POL.equals(this.mDealType))
            {
                if (mBPOLCPolSet.get(i).getInsuredID() == null
                        || mBPOLCPolSet.get(i).getInsuredID().equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "BPOPolBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "请传入被保人ID号";
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
                }
            }

            if ("".equals(mBPOLCPolSet.get(i).getBPOBatchNo())
                    || "".equals(mBPOLCPolSet.get(i).getContID())
                    || "".equals(mBPOLCPolSet.get(i).getRiskCode())
                    || ("".equals(mBPOLCPolSet.get(i).getMult()) && ""
                            .equals(mBPOLCPolSet.get(i).getMult()))
                    || "".equals(mBPOLCPolSet.get(i).getInsuYear())
                    || "".equals(mBPOLCPolSet.get(i).getInsuYearFlag())
                    || "".equals(mBPOLCPolSet.get(i).getPayEndYear())
                    || "".equals(mBPOLCPolSet.get(i).getPayEndYearFlag())
                    || "".equals(mBPOLCPolSet.get(i).getPayIntv()))
            {
                CError tError = new CError();
                tError.moduleName = "BPOPolBL";
                tError.functionName = "checkData";
                tError.errorMessage = "险种信息录入不完整";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        }

        return true;
    }

    /**
     * dealData
     * 本类的核心方法，用来按业务逻辑处理传入的数据
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        int tMaxPolID = getMaxPolID();

        //删除以前的险种信息
        String sql = "delete from BPOLCPol " + "where BPOBatchNo = '"
                + mBPOLCPolSet.get(1).getBPOBatchNo() + "' "
                + "   and ContID = '" + mBPOLCPolSet.get(1).getContID() + "' ";
        if (this.ONE_INSURED_POL.equals(mDealType))
        {
            sql += "   and InsuredID = '" + mBPOLCPolSet.get(1).getInsuredID()
                    + "' ";
        }
        map.put(sql, SysConst.UPDATE);

        int newPolCount = 0;
        for (int i = 1; i <= mBPOLCPolSet.size(); i++)
        {
            BPOLCPolSchema tBPOLCPolSchema = new BPOLCPolSchema();

            //系统中险种
            if (mBPOLCPolSet.get(i).getPolID() != null
                    && !mBPOLCPolSet.get(i).getPolID().equals(""))
            {
                BPOLCPolDB db = new BPOLCPolDB();
                db.setBPOBatchNo(mBPOLCPolSet.get(i).getBPOBatchNo());
                db.setPolID(mBPOLCPolSet.get(i).getPolID());
                if (!db.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "BPOPolBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "没有查询到险种ID为" + db.getPolID() + "的险种 ";
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
                }
                ref.transFields(tBPOLCPolSchema, db.getSchema());
                //                tBPOLCPolSchema.setBPOBatchNo(tBPOBatchNo);
                tBPOLCPolSchema.setContID(mBPOLCPolSet.get(i).getContID());
                tBPOLCPolSchema
                        .setInsuredID(mBPOLCPolSet.get(i).getInsuredID());
                tBPOLCPolSchema
                        .setMainPolID(mBPOLCPolSet.get(i).getMainPolID());
                //tBPOLCPolSchema.setMainPolID(tMainPolIDs[i]);
                //tBPOLCPolSchema.setPolID(mBPOLCPolSet.get(i));
                tBPOLCPolSchema.setAppntID(mBPOLCPolSet.get(i).getAppntID());
                tBPOLCPolSchema.setRiskCode(mBPOLCPolSet.get(i).getRiskCode());
                tBPOLCPolSchema.setInsuYear(mBPOLCPolSet.get(i).getInsuYear());
                tBPOLCPolSchema.setInsuYearFlag(mBPOLCPolSet.get(i)
                        .getInsuYearFlag());
                tBPOLCPolSchema.setPayEndYear(mBPOLCPolSet.get(i)
                        .getPayEndYear());
                tBPOLCPolSchema.setPayEndYearFlag(mBPOLCPolSet.get(i)
                        .getPayEndYearFlag());
                tBPOLCPolSchema.setPayIntv(mBPOLCPolSet.get(i).getPayIntv());
                tBPOLCPolSchema.setPrem(mBPOLCPolSet.get(i).getPrem());
                
                // 处理契约追加保费
                tBPOLCPolSchema.setSupplementaryPrem(mBPOLCPolSet.get(i).getSupplementaryPrem());
                // --------------------------------------
                
                if (mBPOLCPolSet.get(i).getMult() != null)
                {
                    tBPOLCPolSchema.setMult(mBPOLCPolSet.get(i).getMult());
                }
                else if (mBPOLCPolSet.get(i).getAmnt() != null)
                {
                    tBPOLCPolSchema.setAmnt(mBPOLCPolSet.get(i).getAmnt());
                }
                tBPOLCPolSchema.setRelationToAppnt(mBPOLCPolSet.get(i)
                        .getRelationToAppnt());
                tBPOLCPolSchema.setRelationToMainInsured(mBPOLCPolSet.get(i)
                        .getRelationToMainInsured());
                tBPOLCPolSchema.setPrtNo(mBPOLCPolSet.get(i).getPrtNo());
                tBPOLCPolSchema.setPolApplyDate(mBPOLCPolSet.get(i)
                        .getPolApplyDate());
                tBPOLCPolSchema
                        .setCValiDate(mBPOLCPolSet.get(i).getCValiDate());
                tBPOLCPolSchema.setFirstTrialOperator(mBPOLCPolSet.get(i)
                        .getFirstTrialOperator());
                tBPOLCPolSchema.setReceiveDate(mBPOLCPolSet.get(i)
                        .getReceiveDate());
                tBPOLCPolSchema.setPayMode(mBPOLCPolSet.get(i).getPayMode());
                tBPOLCPolSchema
                        .setManageCom(mBPOLCPolSet.get(i).getManageCom());
                tBPOLCPolSchema.setSaleChnl(mBPOLCPolSet.get(i).getSaleChnl());
                tBPOLCPolSchema
                        .setAgentCode(mBPOLCPolSet.get(i).getAgentCode());
                tBPOLCPolSchema
                        .setAgentName(mBPOLCPolSet.get(i).getAgentName());
                tBPOLCPolSchema.setOperator(mGI.Operator);
                tBPOLCPolSchema.setModifyDate(PubFun.getCurrentDate());
                tBPOLCPolSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(tBPOLCPolSchema, SysConst.INSERT);
            }
            //新增加的险种
            else
            {
                newPolCount++;
                mBPOLCPolSet.get(i).setPolID("" + (tMaxPolID + newPolCount));
                mBPOLCPolSet.get(i).setOperator(mGI.Operator);
                PubFun.fillDefaultField(mBPOLCPolSet.get(i));
                map.put(mBPOLCPolSet.get(i), SysConst.INSERT);
            }
        }

        return true;
    }

    /**
     * getMaxPolID
     *
     * @return String
     */
    private int getMaxPolID()
    {
        String sql = "select int(max(PolID)) + 1 from BPOLCPol "
                + "where BPOBatchNo = '" + mBPOLCPolSet.get(1).getBPOBatchNo()
                + "' ";
        String maxNo = new ExeSQL().getOneValue(sql);
        if (maxNo.equals("") || maxNo.equals("null"))
        {
            maxNo = "1";
        }

        return Integer.parseInt(maxNo);
    }

    /**
     * getInputData
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mBPOLCPolSet = (BPOLCPolSet) data.getObjectByObjectName("BPOLCPolSet",
                0);

        return true;
    }

    public void setmDealType(String cDealType)
    {
        mDealType = cDealType;
    }
}
