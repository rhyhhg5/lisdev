package com.sinosoft.lis.tb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.db.LMUWDB;
import com.sinosoft.lis.vschema.LMUWSet;
import com.sinosoft.lis.schema.LMUWSchema;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.schema.BPOLCAppntSchema;
import com.sinosoft.lis.schema.BPOLCInsuredSchema;
import com.sinosoft.lis.schema.BPOLCPolSchema;
import com.sinosoft.lis.schema.BPOLCBnfSchema;
import com.sinosoft.lis.schema.BPOLCImpartSchema;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BPOCheckContData {

    /** 往后面传输数据的容器 */
    private VData inputData;

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private String operator;

    private BPOLCAppntSchema tBPOLCAppntSchema;

    private BPOLCInsuredSchema tBPOLCInsuredSchema;

    private BPOLCPolSchema tBPOLCPolSchema;

    private BPOLCBnfSchema tBPOLCBnfSchema;

    private BPOLCImpartSchema tBPOLCImpartSchema;

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         operate 数据操作
     * @return:
     */
    public boolean submitData(VData inputData, String operate) {
        this.inputData = (VData) inputData.clone();

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        return true;
    }

    /**
     * 用于获得传如的数据
     * @return boolean
     */
    private boolean getInputData() {
        GlobalInput globalInput = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
        operator = globalInput.Operator;

        tBPOLCAppntSchema = (BPOLCAppntSchema) inputData.getObjectByObjectName("BPOLCAppntSchema", 0);
        tBPOLCInsuredSchema = (BPOLCInsuredSchema) inputData.getObjectByObjectName("BPOLCInsuredSchema", 0);
        tBPOLCPolSchema = (BPOLCPolSchema) inputData.getObjectByObjectName("BPOLCPolSchema", 0);
        tBPOLCBnfSchema = (BPOLCBnfSchema) inputData.getObjectByObjectName("BPOLCBnfSchema", 0);
        tBPOLCImpartSchema = (BPOLCImpartSchema) inputData.getObjectByObjectName("BPOLCImpartSchema", 0);

        if (tBPOLCAppntSchema == null || tBPOLCInsuredSchema == null ||
            tBPOLCPolSchema == null || tBPOLCBnfSchema == null ||
            tBPOLCImpartSchema == null) {
            CError error = new CError();
            error.moduleName = "BPOCheckContData";
            error.functionName = "getInputData";
            error.errorMessage = "获得的数据不够！";
            this.mErrors.addOneError(error);
        }

        return true;
    }

    /**
     * 用于校验外包录入的字段是否符合要求
     * @return boolean
     */
    private boolean checkData() {

        return calculator();
    }

    /**
     * 该方法未用到
     * @return boolean
     */
    private boolean dealData() {

        return true;
    }

    /**
     * 该方法未用到
     * @return boolean
     */
    private boolean prepareOutputData() {

        return true;
    }

//*************************************************************************
//                              业务处理
//*************************************************************************

      /**
       * 用于校验需要校验的字段。如某字段需要校验，则在 LMCalMode 中添加一条校验 SQL
       * 校验通过返回 '1' ，未通过返回 '0'
       * @return String
       */
      private boolean calculator() {
        Calculator calculator = getCalculator();

        LMCalModeSet tLMCalModeSet = getLMCalModeSet();

        if(tLMCalModeSet == null || tLMCalModeSet.size() < 1)
        {
            return false;
        }

        for (int i = 1; i <= tLMCalModeSet.size(); i++) {
            calculator.setCalCode(tLMCalModeSet.get(i).getCalCode());
            String flag = calculator.calculate();

            if(flag.equals("0"))
            {
                // @@错误处理
                //this.mErrors.copyAllErrors();
                CError tError = new CError();
                tError.moduleName = "BPOCheckContData";
                tError.functionName = "calculator";
                tError.errorMessage = tLMCalModeSet.get(i).getRemark() + "未通过校验，请检查！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        //校验投保申请日期
        if(!checkDate(tBPOLCPolSchema.getPolApplyDate()))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors();
            CError tError = new CError();
            tError.moduleName = "BPOCheckContData";
            tError.functionName = "calculator";
            tError.errorMessage = "投保申请日期未通过校验，请检查！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //校验投保人生日
        if(!checkDate(tBPOLCAppntSchema.getBirthday()))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors();
            CError tError = new CError();
            tError.moduleName = "BPOCheckContData";
            tError.functionName = "calculator";
            tError.errorMessage = "投保人生日未通过校验，请检查！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //校验被保人生日
        if(!checkDate(tBPOLCInsuredSchema.getBirthday()))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors();
            CError tError = new CError();
            tError.moduleName = "BPOCheckContData";
            tError.functionName = "calculator";
            tError.errorMessage = "被保人生日未通过校验，请检查！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //校验保单生效日期
        if(!checkDate(tBPOLCPolSchema.getCValiDate()))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors();
            CError tError = new CError();
            tError.moduleName = "BPOCheckContData";
            tError.functionName = "calculator";
            tError.errorMessage = "保单生效日期未通过校验，请检查！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 该方法用于获得校验对应的 LMCalModeSet
     * @return LMCalModeSet
     */
    private LMCalModeSet getLMCalModeSet()
    {
        LMCalModeSchema mLMCalMode = new LMCalModeSchema();
        mLMCalMode.setType("B");//设置类型为校验类型
        LMCalModeDB mLMCalModeDB = new LMCalModeDB();
        mLMCalModeDB.setSchema(mLMCalMode);
        LMCalModeSet set = mLMCalModeDB.query();

        return set;
    }

    /**
     * 该方法用于设置参数，参数用于对换 SQL 中的对应字段
     * @return Calculator
     */
    private Calculator getCalculator()
    {
        Calculator calculator = new Calculator();

        //---------------------------------------------------------------------
        //                     外包险种信息表  BPOLCPol
        //---------------------------------------------------------------------
        calculator.addBasicFactor("PolID", "" + tBPOLCPolSchema.getPolID());//险种ID
        calculator.addBasicFactor("PolContID", "" + tBPOLCPolSchema.getContID());//合同ID
        calculator.addBasicFactor("PrtNo", tBPOLCPolSchema.getPrtNo());//印刷号
        calculator.addBasicFactor("RiskCode", tBPOLCPolSchema.getRiskCode());//险种代码
        calculator.addBasicFactor("FamilyType", tBPOLCPolSchema.getFamilyType());//保单类型
        calculator.addBasicFactor("MainPolID", "" + tBPOLCPolSchema.getMainPolID());//主险ID
        calculator.addBasicFactor("PolInsuredID", "" + tBPOLCPolSchema.getInsuredID());//被保人ID
        calculator.addBasicFactor("RelationToAppnt", tBPOLCPolSchema.getRelationToAppnt());//与投保人关系
        calculator.addBasicFactor("RelationToMainInsured", tBPOLCPolSchema.getRelationToMainInsured());//与主被保人关系
        calculator.addBasicFactor("PolAppntID", "" + tBPOLCPolSchema.getAppntID());//投保人ID
        calculator.addBasicFactor("RelaId", "" + tBPOLCPolSchema.getRelaId());//连身被保险人ID
        calculator.addBasicFactor("PolApplyDate", tBPOLCPolSchema.getPolApplyDate());//投保申请日期
        calculator.addBasicFactor("CValiDate", tBPOLCPolSchema.getCValiDate());//生效日期
        calculator.addBasicFactor("FirstTrialOperator", tBPOLCPolSchema.getFirstTrialOperator());//初审人
        calculator.addBasicFactor("FirstTrialTime", tBPOLCPolSchema.getFirstTrialTime());//初审时间
        calculator.addBasicFactor("ReceiveOperator", tBPOLCPolSchema.getReceiveOperator());//收单人
        calculator.addBasicFactor("ReceiveDate", tBPOLCPolSchema.getReceiveDate());//收单日期
        calculator.addBasicFactor("ReceiveTime", tBPOLCPolSchema.getReceiveTime());//收单时间
        calculator.addBasicFactor("TempFeeNo", tBPOLCPolSchema.getTempFeeNo());//暂收据号
        calculator.addBasicFactor("PayMode", tBPOLCPolSchema.getPayMode());//交费方式
        calculator.addBasicFactor("LiveGetMode", tBPOLCPolSchema.getLiveGetMode());//生存保险金领取方式
        calculator.addBasicFactor("DeadGetMode", tBPOLCPolSchema.getDeadGetMode());//身故金领取方式
        calculator.addBasicFactor("PayIntv", "" + tBPOLCPolSchema.getPayIntv());//交费间隔
        calculator.addBasicFactor("InsuYear", "" + tBPOLCPolSchema.getInsuYear());//保险期间
        calculator.addBasicFactor("InsuYearFlag", tBPOLCPolSchema.getInsuYearFlag());//保险期间标志
        calculator.addBasicFactor("PayEndYear", "" + tBPOLCPolSchema.getPayEndYear());//交费年期
        calculator.addBasicFactor("PayEndYearFlag", tBPOLCPolSchema.getPayEndYearFlag());//交费年期标志
        calculator.addBasicFactor("GetYear", "" + tBPOLCPolSchema.getGetYear());//年金开始领取年龄/年期
        calculator.addBasicFactor("GetYearFlag", tBPOLCPolSchema.getGetYearFlag());//年金开始领取标志
        calculator.addBasicFactor("GetStartType", tBPOLCPolSchema.getGetStartType());//起领日期计算类型
        calculator.addBasicFactor("GetDutyKind", tBPOLCPolSchema.getGetDutyKind());//年金领取类型
        calculator.addBasicFactor("BonusGetMode", tBPOLCPolSchema.getBonusGetMode());//红利领取方式
        calculator.addBasicFactor("PremToAmnt", tBPOLCPolSchema.getPremToAmnt());//计算方向
        calculator.addBasicFactor("Mult", "" + tBPOLCPolSchema.getMult());//档次
        calculator.addBasicFactor("Prem", "" + tBPOLCPolSchema.getPrem());//保费
        calculator.addBasicFactor("Amnt", "" + tBPOLCPolSchema.getAmnt());//保额
        calculator.addBasicFactor("CalRule", tBPOLCPolSchema.getCalRule());//计算规则
        calculator.addBasicFactor("FloatRate", "" + tBPOLCPolSchema.getFloatRate());//费率
        calculator.addBasicFactor("GetLimit", "" + tBPOLCPolSchema.getGetLimit());//免赔额
        calculator.addBasicFactor("GetRate", "" + tBPOLCPolSchema.getGetRate());//赔付比例
        calculator.addBasicFactor("HealthCheckFlag", tBPOLCPolSchema.getHealthCheckFlag());//是否体检
        calculator.addBasicFactor("OutPayFlag", tBPOLCPolSchema.getOutPayFlag());//溢交保费方式
        calculator.addBasicFactor("ManageCom", tBPOLCPolSchema.getManageCom());//管理机构
        calculator.addBasicFactor("SaleChnl", tBPOLCPolSchema.getSaleChnl());//销售渠道
        calculator.addBasicFactor("AgentCode", tBPOLCPolSchema.getAgentCode());//代理人编码
        calculator.addBasicFactor("AgentName", tBPOLCPolSchema.getAgentName());//代理人姓名
        calculator.addBasicFactor("AgentCom", tBPOLCPolSchema.getAgentCom());//代理机构
        calculator.addBasicFactor("BankWorkSite", tBPOLCPolSchema.getBankWorkSite());//银行营业网点

        //---------------------------------------------------------------------
        //                    外包投保人信息表  BPOLCAppnt
        //---------------------------------------------------------------------
        calculator.addBasicFactor("AppntID", "" + tBPOLCAppntSchema.getAppntID());//投保人ID
        calculator.addBasicFactor("AppntContID", "" + tBPOLCAppntSchema.getContID());//合同ID
        calculator.addBasicFactor("AppntCustomerNo", tBPOLCAppntSchema.getCustomerNo());//客户号
        calculator.addBasicFactor("AppntName", tBPOLCAppntSchema.getName());//姓名
        calculator.addBasicFactor("AppntSex", tBPOLCAppntSchema.getSex());//性别
        calculator.addBasicFactor("AppntBirthday", tBPOLCAppntSchema.getBirthday());//出生日期
        calculator.addBasicFactor("AppntIDType", tBPOLCAppntSchema.getIDType());//证件类型
        calculator.addBasicFactor("AppntIDNo", tBPOLCAppntSchema.getIDNo());//证件号码
        calculator.addBasicFactor("AppntNativePlace", tBPOLCAppntSchema.getNativePlace());//国籍
        calculator.addBasicFactor("AppntRgtAddress", tBPOLCAppntSchema.getRgtAddress());//户口所在地
        calculator.addBasicFactor("AppntMarriage", tBPOLCAppntSchema.getMarriage());//婚姻状况
        calculator.addBasicFactor("AppntNationality", tBPOLCAppntSchema.getNationality());//民族
        calculator.addBasicFactor("AppntOccupationCode", tBPOLCAppntSchema.getOccupationCode());//职业代码
        calculator.addBasicFactor("AppntOccupationType", tBPOLCAppntSchema.getOccupationType());//职业类别
        calculator.addBasicFactor("AppntWorkType", tBPOLCAppntSchema.getWorkType());//职业(工种)
        calculator.addBasicFactor("AppntPluralityType", tBPOLCAppntSchema.getPluralityType());//兼职
        calculator.addBasicFactor("BankCode", tBPOLCAppntSchema.getBankCode());//开户行
        calculator.addBasicFactor("BankAccNo", tBPOLCAppntSchema.getBankAccNo());//银行帐号
        calculator.addBasicFactor("AccName", tBPOLCAppntSchema.getAccName());//户名
        calculator.addBasicFactor("AppntSmokeFlag", tBPOLCAppntSchema.getSmokeFlag());//是否吸烟
        calculator.addBasicFactor("AppntPostalAddress", tBPOLCAppntSchema.getPostalAddress());//联系地址
        calculator.addBasicFactor("AppntZipCode", tBPOLCAppntSchema.getZipCode());//邮编
        calculator.addBasicFactor("AppntPhone", tBPOLCAppntSchema.getPhone());//联系电话
        calculator.addBasicFactor("AppntFax", tBPOLCAppntSchema.getFax());//传真
        calculator.addBasicFactor("AppntMobile", tBPOLCAppntSchema.getMobile());//手机
        calculator.addBasicFactor("AppntEMail", tBPOLCAppntSchema.getEMail());//电子邮箱
        calculator.addBasicFactor("AppntHomeZipCode", tBPOLCAppntSchema.getHomeZipCode());//家庭邮编
        calculator.addBasicFactor("AppntHomeFax", tBPOLCAppntSchema.getHomeFax());//家庭传真
        calculator.addBasicFactor("AppntGrpName", tBPOLCAppntSchema.getGrpName());//工作单位
        calculator.addBasicFactor("AppntCompanyZipCode", tBPOLCAppntSchema.getCompanyZipCode());//单位邮编
        calculator.addBasicFactor("AppntCompanyFax", tBPOLCAppntSchema.getCompanyFax());//单位传真
        calculator.addBasicFactor("AppntGrpNo", tBPOLCAppntSchema.getGrpNo());//单位编码
        calculator.addBasicFactor("AppntJoinCompanyDate", tBPOLCAppntSchema.getJoinCompanyDate());//入司日期
        calculator.addBasicFactor("AppntDegree", tBPOLCAppntSchema.getDegree());//学历
        calculator.addBasicFactor("AppntPosition", tBPOLCAppntSchema.getPosition());//岗位职务
        calculator.addBasicFactor("AppntSalary", "" + tBPOLCAppntSchema.getSalary());//年收入
        calculator.addBasicFactor("Remark", "" + tBPOLCAppntSchema.getRemark());//特别约定
        calculator.addBasicFactor("PremScope", "" + tBPOLCAppntSchema.getPremScope());//整单保费

        //---------------------------------------------------------------------
        //                    外包被保人信息表  BPOLCInsured
        //---------------------------------------------------------------------
        calculator.addBasicFactor("InsuredID", "" + tBPOLCInsuredSchema.getInsuredID());//被保人ID
        calculator.addBasicFactor("InsuredContID", "" + tBPOLCInsuredSchema.getContID());//合同ID
        calculator.addBasicFactor("InsuredCustomerNo", tBPOLCInsuredSchema.getCustomerNo());//客户号
        calculator.addBasicFactor("InsuredName", tBPOLCInsuredSchema.getName());//姓名
        calculator.addBasicFactor("InsuredSex", tBPOLCInsuredSchema.getSex());//性别
        calculator.addBasicFactor("InsuredBirthday", tBPOLCInsuredSchema.getBirthday());//出生日期
        calculator.addBasicFactor("InsuredIDType", tBPOLCInsuredSchema.getIDType());//证件类型
        calculator.addBasicFactor("InsuredIDNo", tBPOLCInsuredSchema.getIDNo());//证件号码
        calculator.addBasicFactor("InsuredNativePlace", tBPOLCInsuredSchema.getNativePlace());//国籍
        calculator.addBasicFactor("InsuredRgtAddress", tBPOLCInsuredSchema.getRgtAddress());//户口所在地
        calculator.addBasicFactor("InsuredMarriage", tBPOLCInsuredSchema.getMarriage());//婚姻状况
        calculator.addBasicFactor("InsuredNationality", tBPOLCInsuredSchema.getNationality());//民族
        calculator.addBasicFactor("InsuredOccupationCode", tBPOLCInsuredSchema.getOccupationCode());//职业代码
        calculator.addBasicFactor("InsuredOccupationType", tBPOLCInsuredSchema.getOccupationType());//职业类别
        calculator.addBasicFactor("InsuredWorkType", tBPOLCInsuredSchema.getWorkType());//职业(工种)
        calculator.addBasicFactor("InsuredPluralityType", tBPOLCInsuredSchema.getPluralityType());//兼职
        calculator.addBasicFactor("InsuredSmokeFlag", tBPOLCInsuredSchema.getSmokeFlag());//是否吸烟
        calculator.addBasicFactor("InsuredPostalAddress", tBPOLCInsuredSchema.getPostalAddress());//联系地址
        calculator.addBasicFactor("InsuredZipCode", tBPOLCInsuredSchema.getZipCode());//邮编
        calculator.addBasicFactor("InsuredPhone", tBPOLCInsuredSchema.getPhone());//联系电话
        calculator.addBasicFactor("InsuredFax", tBPOLCInsuredSchema.getFax());//传真
        calculator.addBasicFactor("InsuredMobile", tBPOLCInsuredSchema.getMobile());//手机
        calculator.addBasicFactor("InsuredEMail", tBPOLCInsuredSchema.getEMail());//电子邮箱
        calculator.addBasicFactor("InsuredHomeZipCode", tBPOLCInsuredSchema.getHomeZipCode());//家庭邮编
        calculator.addBasicFactor("InsuredHomeFax", tBPOLCInsuredSchema.getHomeFax());//家庭传真
        calculator.addBasicFactor("InsuredGrpName", tBPOLCInsuredSchema.getGrpName());//工作单位
        calculator.addBasicFactor("InsuredCompanyZipCode", tBPOLCInsuredSchema.getCompanyZipCode());//单位邮编
        calculator.addBasicFactor("InsuredCompanyFax", tBPOLCInsuredSchema.getCompanyFax());//单位传真
        calculator.addBasicFactor("InsuredGrpNo", tBPOLCInsuredSchema.getGrpNo());//单位编码
        calculator.addBasicFactor("InsuredJoinCompanyDate", tBPOLCInsuredSchema.getJoinCompanyDate());//入司日期
        calculator.addBasicFactor("InsuredDegree", tBPOLCInsuredSchema.getDegree());//学历
        calculator.addBasicFactor("InsuredPosition", tBPOLCInsuredSchema.getPosition());//岗位职务
        calculator.addBasicFactor("InsuredSalary", "" + tBPOLCInsuredSchema.getSalary());//年收入

        //---------------------------------------------------------------------
        //                    外包客户告知信息表  BPOLCImpart
        //---------------------------------------------------------------------
        calculator.addBasicFactor("ImpartContID", "" + tBPOLCImpartSchema.getContID());//合同ID
        calculator.addBasicFactor("CustomerNoType", "" + tBPOLCImpartSchema.getCustomerNoType());//客户ID类型
        calculator.addBasicFactor("ImpartCustomerNo", "" + tBPOLCImpartSchema.getCustomerNo());//客户ID
        calculator.addBasicFactor("ImpartVer", tBPOLCImpartSchema.getImpartVer());//告知版别
        calculator.addBasicFactor("ImpartCode", tBPOLCImpartSchema.getImpartCode());//告知编码
        calculator.addBasicFactor("ImpartParamModle", tBPOLCImpartSchema.getImpartParamModle());//填写内容
        calculator.addBasicFactor("DiseaseContent", tBPOLCImpartSchema.getDiseaseContent());//说明内容

        //---------------------------------------------------------------------
        //                    外包受益人信息表  BPOLCBnf
        //---------------------------------------------------------------------
        calculator.addBasicFactor("BnfContID", "" + tBPOLCBnfSchema.getContID());//合同ID
        calculator.addBasicFactor("BnfInsuredID", "" + tBPOLCBnfSchema.getInsuredID());//被保人ID
        calculator.addBasicFactor("BnfType", tBPOLCBnfSchema.getBnfType());//受益人类型
        calculator.addBasicFactor("BnfName", tBPOLCBnfSchema.getName());//受益人姓名
        calculator.addBasicFactor("BnfSex", tBPOLCBnfSchema.getSex());//性别
        calculator.addBasicFactor("BnfBirthday", tBPOLCBnfSchema.getBirthday());//出生日期
        calculator.addBasicFactor("BnfIDType", tBPOLCBnfSchema.getIDType());//证件类型
        calculator.addBasicFactor("BnfIDNo", tBPOLCBnfSchema.getIDNo());//证件号码
        calculator.addBasicFactor("RelationToInsured", tBPOLCBnfSchema.getRelationToInsured());//与被保人关系
        calculator.addBasicFactor("BnfGrade", tBPOLCBnfSchema.getBnfGrade());//受益级别
        calculator.addBasicFactor("BnfLot", "" + tBPOLCBnfSchema.getBnfLot());//受益份额

        return calculator;
    }

    /**
     * 用于校验日期的格式是否合法
     * @param strDate String
     * @return boolean
     */
    private boolean checkDate(String strDate)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd");
            formatter.setLenient(false);
            Date date = formatter.parse(strDate);
            System.out.println(date.toString());
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public static void main(String[] args)
    {
        BPOCheckContData c = new BPOCheckContData();
        //System.out.println(c.checkDate("2005-1-12"));
        //System.out.println(c.checkDate("2005-01-12"));
        //System.out.println(c.checkDate("2005-41-12"));
        //System.out.println(c.checkDate("2005-441-12"));
        //System.out.println(c.checkDate("2005-41-a"));
        //System.out.println(c.checkDate("2005-41-"));
        VData inputData = new VData();
        String operate = "";
        BPOLCAppntSchema mBPOLCAppntSchema = new BPOLCAppntSchema();
        mBPOLCAppntSchema.setAccName("");

        BPOLCInsuredSchema mBPOLCInsuredSchema = new BPOLCInsuredSchema();

        BPOLCPolSchema mBPOLCPolSchema = new BPOLCPolSchema();

        BPOLCBnfSchema mBPOLCBnfSchema = new BPOLCBnfSchema();

        BPOLCImpartSchema mBPOLCImpartSchema = new BPOLCImpartSchema();


        System.out.println(c.submitData(inputData, operate));
    }
}
