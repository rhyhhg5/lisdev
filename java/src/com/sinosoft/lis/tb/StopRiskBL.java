package com.sinosoft.lis.tb;


import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class StopRiskBL {
    /** 数据操作字符串 */
    private String mOperate="";
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();//返回结果
    private String mcodetype="";
    private String mcode="";
    private LDCodeSchema mLDCodeSchema = new LDCodeSchema();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LDCodeDB tLDCodeDB = new LDCodeDB(); 
    private MMap mMap = new MMap();

    public StopRiskBL() {
	}
    public boolean submitData(VData cInputData, String cOperate ){
//    	将操作数据拷贝到本类中
    	
        this.mOperate = cOperate;
        mInputData = (VData)cInputData.clone();
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkData()){
        	return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        
    	PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mResult, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mResult = null;
        return true;
    }
    private boolean getInputData(VData cInputData){
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
    	TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		
    	mcode=(String)tTransferData.getValueByName("code");
    	mcodetype=(String)tTransferData.getValueByName("codetype");
    	
    	return true;
    }
    private boolean checkData() {
        
        if(("").equals(mcodetype) || mcodetype==null){
        	System.out.println("mcodetype为空");
        	return  false;
        }
		return true;
    }
    private boolean dealData() {
    	String sql="";
    	
        	sql	= "update ldcode set codetype='" + mcodetype +"' "
        		+ " where codetype in ('stoprisk','startrisk') and code = '" + mcode +"'";
    
    	mMap.put(sql,SysConst.UPDATE);
    	return true;
    }
    private boolean prepareOutputData(){
    
    	mResult = new VData();
    	mResult.add(mMap);
    	return true;
    }
}
