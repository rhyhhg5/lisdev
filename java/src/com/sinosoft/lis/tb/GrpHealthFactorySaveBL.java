package com.sinosoft.lis.tb;

import com.sinosoft.workflowengine.*;
import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.f1print.*;

/**
 * <p>Title: </p>
 * <p>Description:团单要约信息录入 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author sunxy
 * @version 1.0
 */

public class GrpHealthFactorySaveBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  private GlobalInput mGlobalInput = new GlobalInput();
  private TransferData mTransferData = new TransferData();

  /** 数据操作字符串 */
  private String mOperater;
  private String mManageCom;
  private String mOperate;

  /** 业务数据操作字符串 */
  private String mGrpPolNo;
  private String mPolNo;
  private String mContNo;
  private String mRiskCode;

  /** 要素资料项目表 */
  private LCFactorySet mLCFactorySet = new LCFactorySet();
  private LCFactorySet mOldLCFactorySet = new LCFactorySet();
  private LCFactorySet mNewLCFactorySet = new LCFactorySet();
  private LCParamSet mOldLCParamSet = new LCParamSet();
  private LCParamSet mNewLCParamSet = new LCParamSet();

  /** 时间信息*/
  String mCurrentDate = PubFun.getCurrentDate(); //当前值
  String mCurrentTime = PubFun.getCurrentTime();

  public GrpHealthFactorySaveBL() {
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData, cOperate))
      return false;

    //校验处理
    if (!checkData())
      return false;

    //进行业务处理
    if (!dealData())
      return false;

    //准备往后台的数据
    if (!prepareOutputData())
      return false;

    System.out.println("Start SysUWNoticeBL Submit...");
    //公共提交
    PubSubmit tPubSubmit = new PubSubmit();
    if (tPubSubmit.submitData(mResult, "") == false)
      return false;

    return true;
  }

  /**
   * 校验业务数据
   * @return
   */
  private boolean checkData() {

    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    if (mOperate.equals("save")) {
      // 准备计算要素信息
      for (int i = 1; i <= mLCFactorySet.size(); i++) {
        LCFactorySchema tLCFactorySchema = new LCFactorySchema();
        tLCFactorySchema = mLCFactorySet.get(i);
        if (prepareHealth(tLCFactorySchema, i) == false)
          return false;
      }
    }
    if (mOperate.equals("delete")) {
      // 准备计算要素信息
      for (int i = 1; i <= mLCFactorySet.size(); i++) {
        LCFactorySchema tLCFactorySchema = new LCFactorySchema();
        tLCFactorySchema = mLCFactorySet.get(i);
        if (prepareHealthDelete(tLCFactorySchema, i) == false)
          return false;
      }
    }

    return true;

  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData, String cOperate) {
    //从输入数据中得到所有对象
    //获得全局公共数据
    mOperate = cOperate;
    mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
        "TransferData", 0);
    mInputData = cInputData;

    if (mOperate == null ||
        (!mOperate.equals("save") && !mOperate.equals("delete"))) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据操作类型Operate失败(必须为save 或delete)!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (mGlobalInput == null) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //获得操作员编码
    mOperater = mGlobalInput.Operator;
    if (mOperater == null || mOperater.trim().equals("")) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据Operate失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //获得登陆机构编码
    mManageCom = mGlobalInput.ManageCom;
    if (mManageCom == null || mManageCom.trim().equals("")) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    mOperate = cOperate;

    //获得业务数据
    if (mTransferData == null) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输业务数据失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    mRiskCode = (String) mTransferData.getValueByName("RiskCode");
    if (mRiskCode == null || mRiskCode.trim().equals("")) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输业务数据中RiskCode失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //获得要约信息项目
    mLCFactorySet = (LCFactorySet) mTransferData.getValueByName("LCFactorySet");
//	if ( mLCFactorySet == null || mLCFactorySet.size()==0)
//	{
//	  // @@错误处理
//	  //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
//	  CError tError = new CError();
//	  tError.moduleName = "PEdorUWAutoHealthAfterInitService";
//	  tError.functionName = "getInputData";
//	  tError.errorMessage = "前台传输获得要约信息项目数据失败!";
//	  this.mErrors .addOneError(tError) ;
//	  return false;
//	}
    //考虑到会提出删除已录入的要素信息的需求，所以暂时注释掉
    return true;
  }

  /**
   * 逐条准备计算要素信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareHealth(LCFactorySchema tLCFactorySchema, int tIndex) {
    //基本录入信息校验
    mGrpPolNo = tLCFactorySchema.getGrpPolNo();
    mPolNo = tLCFactorySchema.getPolNo();
    mContNo = tLCFactorySchema.getContNo();
    if (mGrpPolNo == null || mPolNo == null || mContNo == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "prepareHealth";
      tError.errorMessage = "第" + tIndex + "条的要素基本信息（如团单号，保单号等）录入出错";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (mGrpPolNo.trim().equals("") || mPolNo.trim().equals("") ||
        mContNo.trim().equals("")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "prepareHealth";
      tError.errorMessage = "第" + tIndex + "条的要素基本信息（如团单号，保单号等）录入出错";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (tLCFactorySchema.getFactoryCode() == null ||
        tLCFactorySchema.getFactoryCode().trim().length() < 7) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "prepareHealth";
      tError.errorMessage = "第" + tIndex + "条的要素计算编码录入出错";
      this.mErrors.addOneError(tError);
      return false;
    }

    String tFactoryCode = tLCFactorySchema.getFactoryCode().trim().substring(0,
        6);
    String tFactorySubCode = tLCFactorySchema.getFactoryCode().trim().substring(
        6);

    //取要素描述信息
    LMFactoryModeDB tLMFactoryModeDB = new LMFactoryModeDB();
    LMFactoryModeSet tLMFactoryModeSet = new LMFactoryModeSet();

    tLMFactoryModeDB.setRiskCode(mRiskCode);
    tLMFactoryModeDB.setFactoryType(tLCFactorySchema.getFactoryType());
    tLMFactoryModeDB.setFactoryCode(tFactoryCode);
    tLMFactoryModeDB.setFactorySubCode(tFactorySubCode);

    tLMFactoryModeSet = tLMFactoryModeDB.query();
    if (tLMFactoryModeSet == null || tLMFactoryModeSet.size() != 1) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "prepareHealth";
      tError.errorMessage = "取计算编码为：" + tLCFactorySchema.getFactoryCode() +
          "的计算类型为：" + tLCFactorySchema.getFactoryType() + "的计算Sql失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    LMFactoryModeSchema tLMFactoryModeSchema = new LMFactoryModeSchema();
    tLMFactoryModeSchema = tLMFactoryModeSet.get(1);

    String tCalSql = tLMFactoryModeSchema.getCalSql();
    int indexModeParams = tLMFactoryModeSchema.getParams().indexOf(",");//模板，位置
    int indexRelaParams = tLCFactorySchema.getParams().indexOf(",");//实际数据，位置
    String[] tParams = null;
    String[] tRelParams = null;

    String tNewCalSql = tLMFactoryModeSchema.getCalSql().trim();
    //表示参数为一个的情况下
    if (indexModeParams == -1 && indexRelaParams == -1) {
      tNewCalSql = StrTool.replace(tNewCalSql,
                                   "?" + tLMFactoryModeSchema.getParams() + "?",
                                   tLCFactorySchema.getParams());
    }
    else
    //参数为多个的情况下
    if (indexModeParams != -1 && indexRelaParams != -1) {
      //根据，拆分字符串，返回数组
      tParams = PubFun.split(tLMFactoryModeSchema.getParams(), ",");
      tRelParams = PubFun.split(tLCFactorySchema.getParams(), ",");

      if (tParams.length == tRelParams.length) {
        for (int i = 0; i < tParams.length; i++) {
          tNewCalSql = StrTool.replace(tNewCalSql, "?" + tParams[i] + "?",
                                       tRelParams[i]);
        }
      }
      else {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "GrpHealthFactorySaveBL";
        tError.functionName = "prepareHealth";
        tError.errorMessage = "录入参数与计算编码为：" + tLCFactorySchema.getFactoryCode() +
            "的计算类型为：" + tLCFactorySchema.getFactoryType() + "的计算Sql描述中的参数个数不同!";
        this.mErrors.addOneError(tError);
        return false;

      }

    }
    else {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "prepareHealth";
      tError.errorMessage = "录入参数与计算编码为：" + tLCFactorySchema.getFactoryCode() +
          "的计算类型为：" + tLCFactorySchema.getFactoryType() + "的计算Sql描述中的参数个数不同!";
      this.mErrors.addOneError(tError);
      return false;

    }

    //准备计算要素信息数据
//	  tLCFactorySchema.setGrpPolNo( mGrpPolNo );
//	  tLCFactorySchema.setPolNo( mPolNo );
//	  tLCFactorySchema.setContNo(mContNo);
    tLCFactorySchema.setFactoryCode(tFactoryCode);
    tLCFactorySchema.setFactoryName(tLMFactoryModeSchema.getFactoryName());
    tLCFactorySchema.setFactorySubCode(tFactorySubCode);
    tLCFactorySchema.setCalSql(tNewCalSql);
    tLCFactorySchema.setCalRemark(tLMFactoryModeSchema.getCalRemark());
    tLCFactorySchema.setInerSerialNo(String.valueOf(tIndex));
    tLCFactorySchema.setMakeDate(mCurrentDate); //当前值
    tLCFactorySchema.setMakeTime(mCurrentTime);
    tLCFactorySchema.setModifyDate(mCurrentDate); //当前值
    tLCFactorySchema.setModifyTime(mCurrentTime);
    mNewLCFactorySet.add(tLCFactorySchema);

    if (tRelParams != null && tRelParams != null) {
      for (int i = 0; i < tRelParams.length; i++) {
        LCParamSchema tLCParamSchema = new LCParamSchema();

        //准备计算要素参数信息数据
        tLCParamSchema.setGrpPolNo(tLCFactorySchema.getGrpPolNo());
        tLCParamSchema.setPolNo(tLCFactorySchema.getPolNo());
        tLCParamSchema.setContNo(tLCFactorySchema.getContNo());
        tLCParamSchema.setFactoryType(tLCFactorySchema.getFactoryType());
        tLCParamSchema.setFactoryCode(tLCFactorySchema.getFactoryCode());
        tLCParamSchema.setOtherNo(tLCFactorySchema.getOtherNo());
        tLCParamSchema.setFactoryName(tLCFactorySchema.getFactoryName());
        tLCParamSchema.setInerSerialNo(tLCFactorySchema.getInerSerialNo());
        tLCParamSchema.setFactorySubCode(tLCFactorySchema.getFactorySubCode());
        tLCParamSchema.setParamName(tParams[i]);
        tLCParamSchema.setParam(tRelParams[i]);
        tLCParamSchema.setMakeDate(mCurrentDate); //当前值
        tLCParamSchema.setMakeTime(mCurrentTime);
        tLCParamSchema.setModifyDate(mCurrentDate); //当前值
        tLCParamSchema.setModifyTime(mCurrentTime);

        mNewLCParamSet.add(tLCParamSchema);
      }
    }
    else {
      LCParamSchema tLCParamSchema = new LCParamSchema();

      //准备计算要素参数信息数据
      tLCParamSchema.setGrpPolNo(tLCFactorySchema.getGrpPolNo());
      tLCParamSchema.setPolNo(tLCFactorySchema.getPolNo());
      tLCParamSchema.setContNo(tLCFactorySchema.getContNo());
      tLCParamSchema.setFactoryType(tLCFactorySchema.getFactoryType());
      tLCParamSchema.setFactoryCode(tLCFactorySchema.getFactoryCode());
      tLCParamSchema.setOtherNo(tLCFactorySchema.getOtherNo());
      tLCParamSchema.setFactoryName(tLCFactorySchema.getFactoryName());
      tLCParamSchema.setInerSerialNo(tLCFactorySchema.getInerSerialNo());
      tLCParamSchema.setFactorySubCode(tLCFactorySchema.getFactorySubCode());
      tLCParamSchema.setParamName(tLMFactoryModeSchema.getParams());
      tLCParamSchema.setParam(tLCFactorySchema.getParams());
      tLCParamSchema.setMakeDate(mCurrentDate); //当前值
      tLCParamSchema.setMakeTime(mCurrentTime);
      tLCParamSchema.setModifyDate(mCurrentDate); //当前值
      tLCParamSchema.setModifyTime(mCurrentTime);

      mNewLCParamSet.add(tLCParamSchema);
    }

    //删除旧的要素信息
    LCFactoryDB tLCFactoryDB = new LCFactoryDB();
    LCFactorySet tLCFactorySet = new LCFactorySet();
    tLCFactoryDB.setGrpPolNo(mGrpPolNo);
    tLCFactoryDB.setPolNo(mPolNo);
    tLCFactoryDB.setContNo(mContNo);
    tLCFactorySet = tLCFactoryDB.query();
    if (tLCFactorySet == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "dealData";
      tError.errorMessage = "查询旧的要素项目信息出错!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (tLCFactorySet.size() > 0)
      mOldLCFactorySet.add(tLCFactorySet);

      //删除旧的计算要素参数信息
    LCParamDB tLCParamDB = new LCParamDB();
    LCParamSet tLCParamSet = new LCParamSet();
    tLCParamDB.setGrpPolNo(mGrpPolNo);
    tLCParamDB.setPolNo(mPolNo);
    tLCParamDB.setContNo(mContNo);
    tLCParamSet = tLCParamDB.query();
    if (tLCParamSet == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "dealData";
      tError.errorMessage = "查询旧的计算要素参数信息出错!";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (tLCParamSet.size() > 0)
      mOldLCParamSet.add(tLCParamSet);

    return true;
  }

  /**
   * 逐条准备计算要素信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareHealthDelete(LCFactorySchema tLCFactorySchema,
                                      int tIndex) {
    //基本录入信息校验
    mGrpPolNo = tLCFactorySchema.getGrpPolNo();
    mPolNo = tLCFactorySchema.getPolNo();
    mContNo = tLCFactorySchema.getContNo();
    if (mGrpPolNo == null || mPolNo == null || mContNo == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "prepareHealth";
      tError.errorMessage = "第" + tIndex + "条的要素基本信息（如团单号，保单号等）录入出错";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (mGrpPolNo.trim().equals("") || mPolNo.trim().equals("") ||
        mContNo.trim().equals("")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "prepareHealth";
      tError.errorMessage = "第" + tIndex + "条的要素基本信息（如团单号，保单号等）录入出错";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (tLCFactorySchema.getFactoryCode() == null ||
        tLCFactorySchema.getFactoryCode().trim().length() < 7) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "prepareHealth";
      tError.errorMessage = "第" + tIndex + "条的要素计算编码录入出错";
      this.mErrors.addOneError(tError);
      return false;
    }

    String tFactoryCode = tLCFactorySchema.getFactoryCode().trim().substring(0,
        6);
    String tFactorySubCode = tLCFactorySchema.getFactoryCode().trim().substring(
        6);

    //删除旧的要素信息
    LCFactoryDB tLCFactoryDB = new LCFactoryDB();
    LCFactorySet tLCFactorySet = new LCFactorySet();
    tLCFactoryDB.setGrpPolNo(mGrpPolNo);
    tLCFactoryDB.setPolNo(mPolNo);
    tLCFactoryDB.setContNo(mContNo);
    tLCFactoryDB.setOtherNo(tLCFactorySchema.getOtherNo());
    tLCFactoryDB.setFactoryCode(tFactoryCode);
    tLCFactoryDB.setFactorySubCode(tFactorySubCode);
    tLCFactoryDB.setFactoryType(tLCFactorySchema.getFactoryType());
    tLCFactorySet = tLCFactoryDB.query();
    if (tLCFactorySet == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "dealData";
      tError.errorMessage = "查询旧的要素项目信息出错!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (tLCFactorySet.size() > 0)
      mOldLCFactorySet.add(tLCFactorySet);

      //删除旧的计算要素参数信息
    LCParamDB tLCParamDB = new LCParamDB();
    LCParamSet tLCParamSet = new LCParamSet();
    tLCParamDB.setGrpPolNo(mGrpPolNo);
    tLCParamDB.setPolNo(mPolNo);
    tLCParamDB.setContNo(mContNo);
    tLCParamDB.setOtherNo(tLCFactorySchema.getOtherNo());
    tLCParamDB.setFactoryCode(tFactoryCode);
    tLCParamDB.setFactorySubCode(tFactorySubCode);
    tLCParamDB.setFactoryType(tLCFactorySchema.getFactoryType());

    tLCParamSet = tLCParamDB.query();
    if (tLCParamSet == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpHealthFactorySaveBL";
      tError.functionName = "dealData";
      tError.errorMessage = "查询旧的计算要素参数信息出错!";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (tLCParamSet.size() > 0)
      mOldLCParamSet.add(tLCParamSet);

    return true;
  }

  private boolean prepareOutputData() {
    mResult.clear();
    MMap map = new MMap();

    //删除旧计算要素参数信息数据
    if (mOldLCParamSet != null && mOldLCParamSet.size() > 0) {
      map.put(mOldLCParamSet, "DELETE");
    }

    //删除旧计算要素信息数据
    if (mOldLCFactorySet != null && mOldLCFactorySet.size() > 0) {
      map.put(mOldLCFactorySet, "DELETE");
    }

    //添加新的计算要素信息数据
    if (mNewLCFactorySet != null && mNewLCFactorySet.size() > 0) {
      map.put(mNewLCFactorySet, "INSERT");
    }

    //添加新的计算要素参数信息数据
    if (mNewLCParamSet != null && mNewLCParamSet.size() > 0) {
      map.put(mNewLCParamSet, "INSERT");
    }

    mResult.add(map);
    return true;
  }

  public VData getResult() {
    return mResult;
  }

  public TransferData getReturnTransferData() {
    return mTransferData;
  }

  public CErrors getErrors() {
    return mErrors;
  }

  public static void main(String[] args) {
  }
}
