package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author zxs
 *
 */
public class DownLoadGifUI
{
    public CErrors mErrors = new CErrors();

    public DownLoadGifUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	DownLoadGifBL tDownLoadGifBL = new DownLoadGifBL();
            if (!tDownLoadGifBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tDownLoadGifBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
