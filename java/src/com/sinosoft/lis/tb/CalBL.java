package com.sinosoft.lis.tb;

import java.text.*;
import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/*
 * <p>Title: 保费计算类 </p>
 * <p>Description: 通过传入的保单信息和责任信息构建出保费信息和领取信息 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author HST
 * @version 1.0
 * @date 2002-07-01
 */

public class CalBL {
    // @Field
    private FDate fDate = new FDate();
    public CErrors mErrors = new CErrors(); // 错误信息
    private boolean mFlag = false; // 记录Duty的传入方式，true传入没有DutyCode的DutySet
    private boolean mGetFlag = false; // 记录Get的传入方式，true传入没有GetDutyCode的GetSet
    private boolean noCalFlag = false; //不需要计算保费保额的标记

    private LCPolBL mLCPolBL = new LCPolBL(); // 保单信息
    private LCDutySchema mLCDutyBLIn; // 录入的与责任相关的信息
    private LCDutyBLSet mLCDutyBLSet; // 责任信息
    private LCGetSchema mLCGetBLIn; // 录入的与领取相关的信息
    private LCGetBLSet mLCGetSetIn; // 传入的领取信息
    private LCPremBLSet mLCPremBLSet = new LCPremBLSet(); // 保费项信息
    private LCGetBLSet mLCGetBLSet = new LCGetBLSet(); // 领取项信息
    private String mRiskWrapCode = null;  //套餐编码

    private LMDutyCtrlSet mLMDutyCtrlSet; // 录入控制信息

    private CalBase mCalBase; // 计算基础数据
    private String mOperator = "";
    private String calType;
    private String currCalMode = "";

    private double OriginPrem = 0; //更新时保存原先保单的实际保费
    private double OriginStandPrem = 0; //更新时保存原先保单的标准保费

    /*转换精确位数的对象   */
    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象
    private String FRateFORMATMODOL = "0.0000000000000000000"; //浮动费率计算出来后的精确位数
    private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); //数字转换对象

    /* 处理浮动费率的变量 */
    private double FloatRate = 0; //浮动费率
    private double InputPrem = 0; //保存界面录入的保费
    private double InputAmnt = 0; //保存界面录入的保额
    private boolean autoCalFloatRateFlag = false; //是否自动计算浮动费率,如果界面传入浮动费率=ConstRate，自动计算
    private final double ConstRate = -1; //如果标明需要从输入的保费保额计算浮动费率，那么界面传入的浮动费率值为常量

    /** 添加要素 */
    private String mRetireRate;
    private String mAvgAge;
    private String mInsuredNum;
    private String mIndustry;
    private String mInsuredState;
    //private String mGrpGetIntv;


    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();
    private LCGrpContSchema mLCGrpContSchemma = null;

    public static void main(String[] args) {
        /**     001险种测试
                 LCPolBL tLCPolBL = new LCPolBL();
                 tLCPolBL.setPolNo("001");
                 tLCPolBL.setRiskCode("001");
                 tLCPolBL.setCValiDate("2002-07-20");
                 tLCPolBL.setRiskVersion("2002");
                 tLCPolBL.setOccupationType("3");
                 tLCPolBL.setMult(2);

                 CalBL tC = new CalBL(tLCPolBL,"");
         **/

        ///**     002险种测试
         LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo("001");
        tLCPolBL.setRiskCode("330306");
        tLCPolBL.setCValiDate("2006-11-08");
        tLCPolBL.setRiskVersion("2002");
        tLCPolBL.setAmnt(10000);
        tLCPolBL.setInsuredSex("1");
        tLCPolBL.setInsuredBirthday("1977-05-03");
        tLCPolBL.setInsuredAppAge(22);

        LCDutyBL tLCDutyBL = new LCDutyBL();
        tLCDutyBL.setInsuYear(1000);
        tLCDutyBL.setInsuYearFlag("A");
        tLCDutyBL.setPremToAmnt("G");
        //       tLCDutyBL.setPayEndYear(10);
        //       tLCDutyBL.setGetYear(50);
        LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
        tLCDutyBLSet.add(tLCDutyBL);

//        CalBL tC = new CalBL(tLCPolBL, tLCDutyBLSet, null);
        //**/

        /**     003险种测试
                 LCPolBL tLCPolBL = new LCPolBL();
                 tLCPolBL.setPolNo("001");
                 tLCPolBL.setRiskCode("003");
                 tLCPolBL.setCValiDate("2002-07-20");
                 tLCPolBL.setRiskVersion("2002");
                 tLCPolBL.setPrem("3000");
                 tLCPolBL.setAmnt("1000000");

                 CalBL tC = new CalBL(tLCPolBL,"");
         **/

        /**     004险种测试
         LCPolBL tLCPolBL = new LCPolBL();
         tLCPolBL.setPolNo("001");
         tLCPolBL.setRiskCode("004");
         tLCPolBL.setCValiDate("2002-07-20");
         tLCPolBL.setRiskVersion("2002");
         tLCPolBL.setInsuredSex("1");
         tLCPolBL.setMult("6");
         tLCPolBL.setOccupationType("3");
         tLCPolBL.setInsuredBirthday("1977-05-03");
         tLCPolBL.setInsuredAppAge(22);

         LCDutyBL tLCDutyBL1 = new LCDutyBL();
         tLCDutyBL1.setDutyCode("001001");

         LCDutyBL tLCDutyBL2 = new LCDutyBL();
         tLCDutyBL2.setDutyCode("002001");
         tLCDutyBL2.setPayEndYear(10);
         tLCDutyBL2.setGetYear(50);
         tLCDutyBL2.setPrem("2000");

         LCDutyBL tLCDutyBL3 = new LCDutyBL();
         tLCDutyBL3.setDutyCode("003001");
         tLCDutyBL3.setPrem("3000");
         tLCDutyBL3.setAmnt("100000");

         LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
         tLCDutyBLSet.add(tLCDutyBL1);
         tLCDutyBLSet.add(tLCDutyBL2);
         tLCDutyBLSet.add(tLCDutyBL3);
         **/
         CalBL tC = new CalBL(tLCPolBL, tLCDutyBLSet, "");
         System.out.print(tC.getComCode("86110000"));

//        boolean flag = tC.calPol();
//        tLCPolBL = tC.getLCPol();
    }

    // @Constructor
    public CalBL(LCPolBL aLCPolBL, String calFlag) {
        mLCPolBL.setSchema(aLCPolBL);
        mLCDutyBLIn = new LCDutySchema();
        mLCGetBLIn = new LCGetSchema();
        calType = calFlag;
    }

    public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet, String calFlag) {

        mLCPolBL.setSchema(tLCPolBL); //保存界面录入的投保单信息
        mLCGetBLIn = new LCGetSchema();

        if (tLCDutyBLSet.size() == 1 &&
            (tLCDutyBLSet.get(1).getDutyCode() == null ||
             tLCDutyBLSet.get(1).getDutyCode().equals(""))) {
            mLCDutyBLIn = tLCDutyBLSet.get(1); //保存界面录入的责任信息-单条
        } else {
            mLCDutyBLIn = new LCDutySchema(); //无数据
            mLCDutyBLSet = new LCDutyBLSet();
            mLCDutyBLSet.set(tLCDutyBLSet); //保存界面录入的责任信息-多条
            mFlag = true;
        }
        calType = calFlag;
    }


    public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet, String calFlag,
                 TransferData tTransferData) {
        mLCPolBL.setSchema(tLCPolBL); //保存界面录入的投保单信息
        mLCGetBLIn = new LCGetSchema();

        if (tLCDutyBLSet.size() == 1 &&
            (tLCDutyBLSet.get(1).getDutyCode() == null ||
             tLCDutyBLSet.get(1).getDutyCode().equals(""))) {
            mLCDutyBLIn = tLCDutyBLSet.get(1); //保存界面录入的责任信息-单条
        } else {
            mLCDutyBLIn = new LCDutySchema(); //无数据
            mLCDutyBLSet = new LCDutyBLSet();
            mLCDutyBLSet.set(tLCDutyBLSet); //保存界面录入的责任信息-多条
            mFlag = true;
        }
        calType = calFlag;
        if (tTransferData != null) {
            this.mRetireRate = (String) tTransferData.getValueByName(
                    "RetireRate");
            this.mIndustry = (String) tTransferData.getValueByName("Industry");
            this.mAvgAge = (String) tTransferData.getValueByName("AvgAge");
            this.mInsuredNum = (String) tTransferData.getValueByName(
                    "InsuredNum");
            this.mInsuredState = (String) tTransferData.getValueByName(
                    "InsuredState");
            //this.mGrpGetIntv = (String) tTransferData.getValueByName("GrpGetIntv");
            this.mRiskWrapCode = (String) tTransferData.getValueByName("RiskWrapCode");
        }
    }

    public CalBL(LCPolSchema tLCPolBL, LCDutySet tLCDutyBLSet, String calFlag,
                 TransferData tTransferData) {
        mLCPolBL.setSchema(tLCPolBL); //保存界面录入的投保单信息
        mLCGetBLIn = new LCGetSchema();

        if (tLCDutyBLSet.size() == 1 &&
            (tLCDutyBLSet.get(1).getDutyCode() == null ||
             tLCDutyBLSet.get(1).getDutyCode().equals(""))) {
            mLCDutyBLIn = tLCDutyBLSet.get(1); //保存界面录入的责任信息-单条
        } else {
            mLCDutyBLIn = new LCDutySchema(); //无数据
            mLCDutyBLSet = new LCDutyBLSet();
            mLCDutyBLSet.set(tLCDutyBLSet); //保存界面录入的责任信息-多条
            mFlag = true;
        }
        calType = calFlag;
        if (tTransferData != null) {
            this.mRetireRate = (String) tTransferData.getValueByName(
                    "RetireRate");
            this.mIndustry = (String) tTransferData.getValueByName("Industry");
            this.mAvgAge = (String) tTransferData.getValueByName("AvgAge");
            this.mInsuredNum = (String) tTransferData.getValueByName(
                    "InsuredNum");
            this.mInsuredState = (String) tTransferData.getValueByName(
                    "InsuredState");
            //this.mGrpGetIntv = (String) tTransferData.getValueByName("GrpGetIntv");
            this.mRiskWrapCode = (String) tTransferData.getValueByName("RiskWrapCode");
        }

    }


    public CalBL(LCPolBL tLCPolBL, LCGetBLSet tLCGetBLSet, String calFlag) {
        mLCPolBL.setSchema(tLCPolBL);
        mLCDutyBLIn = new LCDutySchema();

        if (tLCGetBLSet.size() == 1 &&
            StrTool.cTrim(tLCGetBLSet.get(1).getGetDutyCode()).equals("")) {
            mLCGetBLIn = tLCGetBLSet.get(1);
        } else {
            mLCGetBLIn = new LCGetSchema();
            mLCGetSetIn = new LCGetBLSet();
            mLCGetSetIn.set(tLCGetBLSet);
            mGetFlag = true;
        }
        calType = calFlag;
    }

    /**
     *
     * @param tLCPolBL LCPolBL
     * @param tLCDutyBLSet LCDutyBLSet
     * @param tLCGetBLSet LCGetBLSet
     * @param calFlag String
     * @param tTransferData TransferData
     */
    public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet,
                 LCGetBLSet tLCGetBLSet, String calFlag,
                 TransferData tTransferData) {
        mLCPolBL.setSchema(tLCPolBL); //保存界面录入的投保单信息

        //责任
        if (tLCDutyBLSet.size() == 1 &&
            (tLCDutyBLSet.get(1).getDutyCode() == null ||
             tLCDutyBLSet.get(1).getDutyCode().equals(""))) {
            mLCDutyBLIn = tLCDutyBLSet.get(1); //保存界面录入的责任信息-单条
        } else {
            mLCDutyBLIn = new LCDutySchema(); //无数据
            mLCDutyBLSet = new LCDutyBLSet();
            mLCDutyBLSet.set(tLCDutyBLSet); //保存界面录入的责任信息-多条
            mFlag = true; //可选责任为真
        }

        //领取项
        if (tLCGetBLSet.size() == 1 &&
            StrTool.cTrim(tLCGetBLSet.get(1).getGetDutyCode()).equals("")) {
            mLCGetBLIn = tLCGetBLSet.get(1); //保存界面录入的领取项信息-其它字段为空，GetDutyKind字段有值
        } else {
            mLCGetBLIn = new LCGetSchema();
            mLCGetSetIn = new LCGetBLSet();
            mLCGetSetIn.set(tLCGetBLSet);
            mGetFlag = true; //领取项为真
        }

        calType = calFlag;
        if (tTransferData != null) {
            this.mRetireRate = (String) tTransferData.getValueByName(
                    "RetireRate");
            this.mIndustry = (String) tTransferData.getValueByName("Industry");
            this.mAvgAge = (String) tTransferData.getValueByName("AvgAge");
            this.mInsuredNum = (String) tTransferData.getValueByName(
                    "InsuredNum");
            this.mInsuredState = (String) tTransferData.getValueByName(
                    "InsuredState");
            //this.mGrpGetIntv = (String) tTransferData.getValueByName("GrpGetIntv");
            this.mRiskWrapCode = (String) tTransferData.getValueByName("RiskWrapCode");
        }
    }

    /**
     *
     * @param tLCPolBL LCPolBL
     * @param tLCDutyBLSet LCDutyBLSet
     * @param tLCGetBLSet LCGetBLSet
     * @param calFlag String
     */
    public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet,
                 LCGetBLSet tLCGetBLSet, String calFlag) {
        mLCPolBL.setSchema(tLCPolBL); //保存界面录入的投保单信息

        //责任
        if (tLCDutyBLSet.size() == 1 &&
            (tLCDutyBLSet.get(1).getDutyCode() == null ||
             tLCDutyBLSet.get(1).getDutyCode().equals(""))) {
            mLCDutyBLIn = tLCDutyBLSet.get(1); //保存界面录入的责任信息-单条
        } else {
            mLCDutyBLIn = new LCDutySchema(); //无数据
            mLCDutyBLSet = new LCDutyBLSet();
            mLCDutyBLSet.set(tLCDutyBLSet); //保存界面录入的责任信息-多条
            mFlag = true; //可选责任为真
        }

        //领取项
        if (tLCGetBLSet.size() == 1 &&
            StrTool.cTrim(tLCGetBLSet.get(1).getGetDutyCode()).equals("")) {
            mLCGetBLIn = tLCGetBLSet.get(1); //保存界面录入的领取项信息-其它字段为空，GetDutyKind字段有值
        } else {
            mLCGetBLIn = new LCGetSchema();
            mLCGetSetIn = new LCGetBLSet();
            mLCGetSetIn.set(tLCGetBLSet);
            mGetFlag = true; //领取项为真
        }
        calType = calFlag;
    }


    // @Method
    /**
     * 计算函数
     * @param: 无
     * @return: boolean
     **/
    public boolean calPol() {
        // 准备描述的相关信息(查询责任录入控制信息-mLMDutyCtrlSet)
        if (this.preDefineInfo() == false) {
            return false;
        }

        System.out.println("after preDefineInfo()");

        if (mLCDutyBLSet == null) {
            //取得必选的责任-通过描述取得责任信息的结构,并将界面录入的责任信息存入-mLCDutyBLSet
            if (this.getDutyStructure() == false) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calPol";
                tError.errorMessage = "getDutyStructure方法返回失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        getLCGrpCont();

        //从传入的保单信息中获取责任表中需要计算的以外的信息
        this.getDutyOtherData();

        System.out.println("after getDutyOtherData()");
        System.out.println(" LCDuty : " + mLCDutyBLSet.size());
        int n = mLCDutyBLSet.size();
        for (int i = 1; i <= n; i++) {
            LCDutySchema tLCDutySchema = mLCDutyBLSet.get(i);

            // 准备录入部分的信息
            //System.out.println("测试 : " + mLCDutyBLSet.encode());
            if (mFlag == true) {
                mLCDutyBLIn.setSchema(tLCDutySchema);
            }
            // 计算责任表中的部分信息(保险期间，交费期间，领取期间等)
            tLCDutySchema = this.calDutyOther(tLCDutySchema);
            if (tLCDutySchema == null) {
                return false;
            }
            System.out.println("计算责任表下的保费项和领取项信息");
            // 计算责任表下的保费项和领取项信息
            tLCDutySchema = this.calOneDuty(tLCDutySchema);
            if (this.mErrors.needDealError()) {
                return false;
            }

            //增加浮动费率处理
            mLCDutyBLSet.set(i, tLCDutySchema);
        }

        // 通过责任信息计算保单信息
        this.calPolFromDuty();

        return true;
    }

    /**
     * getLCGrpCont
     * 查询团单信息
     */
    private void getLCGrpCont(){
        String GrpContNo = mLCPolBL.getGrpContNo();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(GrpContNo);
        if(!tLCGrpContDB.getInfo()){
            mLCGrpContSchemma = null;
        }

        mLCGrpContSchemma = tLCGrpContDB.getSchema();
    }

    /**
     * 当不需要计算保费保额时调用该函数，增加了对保费保额的处理
     * @param pLCPremSet
     * @return
     */
    public boolean calPol2(LCPremSet pLCPremSet) {
        if (calPol() == false) {
            return false;
        }

        double sumPrem = 0;
        for (int i = 1; i <= pLCPremSet.size(); i++) {
            sumPrem = sumPrem + pLCPremSet.get(i).getStandPrem();
            for (int n = 1; n <= mLCPremBLSet.size(); n++) { //找到对应的保费项修改金额
                if (pLCPremSet.get(i).getDutyCode().equals(mLCPremBLSet.get(n).
                        getDutyCode()) &&
                    pLCPremSet.get(i).getPayPlanCode().equals(mLCPremBLSet.get(
                            n).getPayPlanCode())) {

                    mLCPremBLSet.get(n).setPrem(pLCPremSet.get(i).getPrem());
                    mLCPremBLSet.get(n).setSumPrem(mLCPremBLSet.get(n).
                            getSumPrem() +
                            mLCPremBLSet.get(n).getPrem());
                    mLCPremBLSet.get(n).setRate(pLCPremSet.get(i).getRate());
                    /*Lis5.3 upgrade get
                     mLCPremBLSet.get(n).setManageFeeRate(pLCPremSet.get(i).getManageFeeRate());
                     */
                    //对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
                    //但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
                    mLCPremBLSet.get(n).setStandPrem(mLCPremBLSet.get(n).
                            getPrem());

                    //保全人工核保处数据的需要add by sxy 2004-03-16
                    //mLCPremBLSet.get(n).setState("0") ;

                    mLCPremBLSet.get(n).setOperator(pLCPremSet.get(n).
                            getOperator());
                    mLCPremBLSet.get(n).setMakeDate(pLCPremSet.get(n).
                            getMakeDate());
                    mLCPremBLSet.get(n).setMakeTime(pLCPremSet.get(n).
                            getMakeTime());

                    break;
                }

            }
            for (int m = 1; m <= mLCDutyBLSet.size(); m++) { //找到对应的责任项累积金额
                mLCDutyBLSet.get(m).setAmnt(0); //保额置0
                mLCDutyBLSet.get(m).setRiskAmnt(0); //风险保额
                if (pLCPremSet.get(i).getDutyCode().equals(mLCDutyBLSet.get(m).
                        getDutyCode())) {
                    mLCDutyBLSet.get(m).setPrem(mLCDutyBLSet.get(m).getPrem() +
                                                pLCPremSet.get(i).getPrem());
                    mLCDutyBLSet.get(m).setSumPrem(mLCDutyBLSet.get(m).getPrem());
                    //对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
                    //但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
                    mLCDutyBLSet.get(m).setStandPrem(mLCDutyBLSet.get(m).
                            getPrem());

                    break;
                }
            }
        }
        mLCPolBL.setPrem(sumPrem);
        //对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
        //但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
        mLCPolBL.setStandPrem(sumPrem);
        mLCPolBL.setSumPrem(sumPrem);
        mLCPolBL.setAmnt(0); //保额置0
        mLCPolBL.setRiskAmnt(0); //风险保额

        for (int x = 1; x <= mLCGetBLSet.size(); x++) {
            mLCGetBLSet.get(x).setStandMoney(0); //保额置0
            mLCGetBLSet.get(x).setActuGet(0); //保额置0
        }

        return true;
    }

    /**
     * 准备描述数据--准备mLMDutyCtrlSet数据
     * @param: 无
     * @return: boolean
     **/
    private boolean preDefineInfo() {
        // 责任录入控制信息
        mLMDutyCtrlSet = new LMDutyCtrlSet();

//        LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
//        tLMRiskDutyDB.setRiskCode( mLCPolBL.getRiskCode() );
//        LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
//
//        if (tLMRiskDutyDB.mErrors.needDealError())
        LMRiskDutySet tLMRiskDutySet =
                mCRI.findRiskDutyByRiskCodeClone(mLCPolBL.getRiskCode());

        if (tLMRiskDutySet == null) {
            // @@错误处理
//            this.mErrors.copyAllErrors(tLMRiskDutyDB.mErrors);
            this.mErrors.copyAllErrors(mCRI.mErrors);
            mCRI.mErrors.clearErrors();

            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "preDefineInfo";
            tError.errorMessage = "LMRiskDuty表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("in preDefineInfo()");

        int n = tLMRiskDutySet.size();
        for (int i = 1; i <= n; i++) {
            LMRiskDutySchema tLMRiskDutySchema = tLMRiskDutySet.get(i);

//            LMDutyCtrlDB tLMDutyCtrlDB = new LMDutyCtrlDB();
//            tLMDutyCtrlDB.setDutyCode( tLMRiskDutySchema.getDutyCode() );
//            LMDutyCtrlSet tLMDutyCtrlSet = tLMDutyCtrlDB.query();
//            if (tLMDutyCtrlDB.mErrors.needDealError())
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLMDutyCtrlDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "CalBL";
//                tError.functionName = "preDefineInfo";
//                tError.errorMessage = "LMDutyCtrl表查询失败!" ;
//                this.mErrors.addOneError(tError) ;
//                return false;
//            }

            LMDutyCtrlSet tLMDutyCtrlSet =
                    mCRI.findDutyCtrlByDutyCodeClone(tLMRiskDutySchema.
                    getDutyCode());

            if (tLMDutyCtrlSet == null) {
                // @@错误处理
                this.mErrors.copyAllErrors(mCRI.mErrors);
                mCRI.mErrors.clearErrors();

                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "preDefineInfo";
                tError.errorMessage = "LMDutyCtrl表查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLMDutyCtrlSet.add(tLMDutyCtrlSet);
        }

        return true;
    }

    /**
     * 判断该字段的内容的取值位置是否为录入值
     * @param: 录入项代码、录入位置（责任、领取项、保费项）、字段名称
     * @return: boolean
     **/
    private boolean chkInput(String code, String ctrl, String fieldName) {
        int n = mLMDutyCtrlSet.size();
        for (int i = 1; i <= n; i++) {
            LMDutyCtrlSchema tLMDutyCtrlSchema = mLMDutyCtrlSet.get(i);
            if (StrTool.cTrim(ctrl).equals("D")) {
                if (StrTool.cTrim(code).equals(StrTool.cTrim(tLMDutyCtrlSchema.
                        getDutyCode()))
                    &&
                    StrTool.cTrim(fieldName).toLowerCase().equals(StrTool.cTrim(
                            tLMDutyCtrlSchema.
                            getFieldName()).toLowerCase())
                    &&
                    StrTool.cTrim(tLMDutyCtrlSchema.getCtrlType()).equals("D")) {
                    if (StrTool.cTrim(tLMDutyCtrlSchema.getInpFlag()).equals(
                            "Y")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                if (StrTool.cTrim(code).equals(StrTool.cTrim(tLMDutyCtrlSchema.
                        getOtherCode()))
                    &&
                    StrTool.cTrim(fieldName).toLowerCase().equals(StrTool.cTrim(
                            tLMDutyCtrlSchema.
                            getFieldName()).toLowerCase())
                    &&
                    StrTool.cTrim(ctrl).equals(StrTool.cTrim(tLMDutyCtrlSchema.
                        getCtrlType()))) {
                    if (StrTool.cTrim(tLMDutyCtrlSchema.getInpFlag()).equals(
                            "Y")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } // end of if
        } // end of for
        return false;
    }

    public LCPolBL getLCPol() {
        return mLCPolBL;
    }

    public LCDutyBLSet getLCDuty() {
        return mLCDutyBLSet;
    }

    public LCPremBLSet getLCPrem() {
        return mLCPremBLSet;
    }

    public LCGetBLSet getLCGet() {
        return mLCGetBLSet;
    }

    /**
     * 通过描述取得保费项信息的结构
     * @param: 无
     * @return: boolean
     **/
    private boolean getPremStructure(LCDutySchema mLCDutySchema) {
        LMDutyPayRelaSet tLMDutyPayRelaSet =
                mCRI.findDutyPayRelaByDutyCode(mLCDutySchema.getDutyCode());

        if (tLMDutyPayRelaSet == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "getPremStructure";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        // 把数据装入mLCPremBLSet中
        int n = tLMDutyPayRelaSet.size();
        for (int i = 1; i <= n; i++) {
            LCPremBL tLCPremBL = new LCPremBL();
            tLCPremBL.setDutyCode(mLCDutySchema.getDutyCode());
            tLCPremBL.setPayPlanCode(tLMDutyPayRelaSet.get(i).getPayPlanCode());

            mLCPremBLSet.add(tLCPremBL);
        }
        return true;
    }

    /**
     * 从保单信息或责任信息中获取保费项表中需要计算的以外的信息
     * @param: 无
     * @return: 无
     **/
    private void getPremOtherData(LCDutySchema mLCDutySchema) {
        int n = mLCPremBLSet.size();
        for (int i = 1; i <= n; i++) {
            LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);
            //PQ1 设置从LCCont中获得的值
            // 从保单获得的信息
            if (tLCPremSchema.getPolNo() == null) {
                tLCPremSchema.setPolNo(mLCPolBL.getPolNo());
            }
            if (tLCPremSchema.getContNo() == null) {
                tLCPremSchema.setContNo(mLCPolBL.getContNo());
            }
            if (tLCPremSchema.getGrpContNo() == null) {
                tLCPremSchema.setGrpContNo(mLCPolBL.getGrpContNo());
            }

            if (tLCPremSchema.getAppntNo() == null) {
                tLCPremSchema.setAppntNo(mLCPolBL.getAppntNo());
            }
            /*Lis5.3 upgrade set
             if (tLCPremSchema.getAppntType() == null) tLCPremSchema.setAppntType( mLCPolBL.getAppntType() );
             */
            if (tLCPremSchema.getAppntType() == null) {
                tLCPremSchema.setAppntType(mLCPolBL.getContType());
            }

            if (tLCPremSchema.getPayStartDate() == null) {
                tLCPremSchema.setPayStartDate(mLCPolBL.getCValiDate());
            }
            if (tLCPremSchema.getManageCom() == null) {
                tLCPremSchema.setManageCom(mLCPolBL.getManageCom());
            }
            //////////////////////////////////////////////////////////////////////////////////////////////
            // 保费项表暂时处理管理费比例的方法是：从保单表的管理费比例（暂时存储）中取出
            // 1。民生常青基业的处理方式是保单的管理费比例和保费项上的管理费比例是一样的，并且保单表和保费表是一一对应的
            // 2。众悦团体年金的处理方式是个单的管理费比例取于团单的管理费比例，故每一张个单的管理费比例相同
            //         add by guoxiang at 2004-9-7
            /////////////////////////////////////////////////////////////////////////////////////////////
            /*Lis5.3 upgrade get          此处逻辑要恢复
                      if (tLCPremSchema.getManageFeeRate() == 0.0) tLCPremSchema.setManageFeeRate( mLCPolBL.getManageFeeRate() );
             */
            // 从责任表获得的信息

            /*Lis5.3 upgrade get  此处逻辑要恢复
             if (tLCPremSchema.getMult() == 0) tLCPremSchema.setMult( mLCDutySchema.getMult() );
             */
//            if (tLCPremSchema.getStandPrem() == 0.0) {
//                tLCPremSchema.setStandPrem(mLCDutySchema.getStandPrem());
//            }

            //保全保全人工核保的需要add by sxy 2004-03-16
            tLCPremSchema.setState("0");
            mLCPremBLSet.set(i, tLCPremSchema);
        }
    }

    /**
     * 计算保费项表中保费的值
     * @param: LCPremSchema  LMDutyPayDB
     * @return: premium value
     **/
    private double calPremValue(LCPremSchema mLCPremSchema,
                                LCDutySchema tLCDutySchema,
                                LMDutyPaySchema tLMDutyPaySchema,
                                String calFlag) {
        double mValue = -1;
        currCalMode = calFlag;
        String mCalCode = null;
        if (StrTool.cTrim(calFlag).equals("P")) {
            mCalCode = tLMDutyPaySchema.getCnterCalCode(); // 保费算保额
        }
        if (StrTool.cTrim(calFlag).equals("G")) {
            mCalCode = tLMDutyPaySchema.getCalCode(); // 保额算保费
        }
        if (StrTool.cTrim(calFlag).equals("O")) {
            mCalCode = tLMDutyPaySchema.getOthCalCode(); // 其他算保费
        }
        //根据实际保费算保额情况记录保费保额计算方向，在保单重算时使用
        if (StrTool.cTrim(calFlag).equals("A") &&
            tLCDutySchema.getStandPrem() != 0.0) {
            mCalCode = tLMDutyPaySchema.getCnterCalCode(); // 保费、保额互算{
            currCalMode = "P";
        }
        if (StrTool.cTrim(calFlag).equals("A") &&
            tLCDutySchema.getAmnt() != 0.0) {
            mCalCode = tLMDutyPaySchema.getCalCode(); // 保费、保额互算{
            currCalMode = "G";
        }
        if (StrTool.cTrim(calFlag).equals("I")) {
            mCalCode = tLMDutyPaySchema.getCnterCalCode(); // 录入保费保额
        }
        /**
         * 改造保全保费计算方式
         * 如果是保全计算则传入的calType则是保全类型
         * 需要从险种责任保全计算表中(LMPolDutyEdorCal)根据责任代码和保全类型
         * 查询出保全的calcode
         * @author : yangming
         */
        /**
         * calType是保全批改类型,由于程序的遗留问题,因此仅NI(增人)时从保全描述表中
         * 取出计算代码进行计算.
         */
        /*
                if (StrTool.cTrim(this.calType).equals("NI")) {
         LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
         tLMPolDutyEdorCalDB.setDutyCode(tLCDutySchema.getDutyCode());
                    tLMPolDutyEdorCalDB.setEdorType(this.calType);
         LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB.query();
         if (tLMPolDutyEdorCalSet != null && tLMPolDutyEdorCalSet.size() > 0) {
         mCalCode = tLMPolDutyEdorCalSet.get(1).getChgPremCalCode();
                    }
                }
         */

        //取默认值
        if (StrTool.cTrim(mCalCode).equals("")) {
            mValue = tLMDutyPaySchema.getDefaultVal();
            return mValue;
        }

        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear());
        mCalculator.addBasicFactor("GetYearFlag", mCalBase.getGetYearFlag());
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("InsuYear", mCalBase.getInsuYear());
        mCalculator.addBasicFactor("InsuYearFlag", mCalBase.getInsuYearFlag());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("RnewFlag", mCalBase.getRnewFlag());
        mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("FRate", mCalBase.getFloatRate());
        mCalculator.addBasicFactor("GetDutyKind", mCalBase.getGetDutyKind());
        mCalculator.addBasicFactor("StandbyFlag1", mCalBase.getStandbyFlag1());
        mCalculator.addBasicFactor("StandbyFlag2", mCalBase.getStandbyFlag2());
        mCalculator.addBasicFactor("StandbyFlag3", mCalBase.getStandbyFlag3());
        mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
        mCalculator.addBasicFactor("GetLimit", mCalBase.getGetLimit());
        mCalculator.addBasicFactor("GetRate", mCalBase.getGetRate());
        mCalculator.addBasicFactor("SSFlag", mCalBase.getSSFlag());
        mCalculator.addBasicFactor("PeakLine", mCalBase.getPeakLine());
        mCalculator.addBasicFactor("CalType", mCalBase.getCalType());
        mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
        mCalculator.addBasicFactor("EdorValiDate", mCalBase.getEdorValiDate());
        mCalculator.addBasicFactor("CInValiDate", mCalBase.getCInValiDate());
        mCalculator.addBasicFactor("CalRule", mCalBase.getCalRule());
        mCalculator.addBasicFactor("LastPayToDate", mCalBase.getLastPayToDate());
        mCalculator.addBasicFactor("PayToDate", mCalBase.getPayToDate());
        mCalculator.addBasicFactor("RevertType", mCalBase.getRevertType());

        mCalculator.addBasicFactor("ComCode", mCalBase.getComCode());
        mCalculator.addBasicFactor("RetireRate", mCalBase.getRetireRate());
        mCalculator.addBasicFactor("InsuredState", mCalBase.getInsuredState());
        mCalculator.addBasicFactor("Industry", mCalBase.getIndustry());
        mCalculator.addBasicFactor("InsuredNum", mCalBase.getInsuredNum());
        mCalculator.addBasicFactor("AvgAge", mCalBase.getAvgAge());
        mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());
        mCalculator.addBasicFactor("Copys", mCalBase.getCopys());
        mCalculator.addBasicFactor("RiskWrapCode", mRiskWrapCode);
        //zqt一带一路
        mCalculator.addBasicFactor("CountryFactor", mCalBase.getCountryFactor());
        mCalculator.addBasicFactor("EngineeringFactor", mCalBase.getEngineeringFactor());
        mCalculator.addBasicFactor("ScaleFactor", mCalBase.getScaleFactor());
        mCalculator.addBasicFactor("Avage", mCalBase.getAvage());
        mCalculator.addBasicFactor("Insureyear", mCalBase.getInsureyear());
        //add by zjd
        mCalculator.addBasicFactor("ContPlanCode", mLCPolBL.getContPlanCode());
        //add by lxs
        mCalculator.addBasicFactor("Totaldiscountfactor", mCalBase.getTotaldiscountfactor());
        //mCalculator.addBasicFactor("GrpGetIntv", mGrpGetIntv);
        //zxs 20190910
        mCalculator.addBasicFactor("Occupationtype", mLCPolBL.getOccupationType());
        if(tLCDutySchema.getFloatRate()>=0.001 && tLCDutySchema.getFloatRate()<=1)
        {
        	mCalculator.addBasicFactor("FeeRate", String.valueOf(tLCDutySchema.getFloatRate()));
        }
        this.prepareDutyParam(mCalculator,tLCDutySchema);
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals("")) {
            mValue = 0;
        } else {
            mValue = Double.parseDouble(tStr);
        }

        //System.out.println(mValue);
        return PubFun.setPrecision(mValue, "0.00");
    }

    /**
     * 计算保费项表中的其他值
     * @param: 无
     * @return: boolean
     **/
    private LCPremSchema calPremOther(LCPremSchema mLCPremSchema,
                                      LMDutyPaySchema tLMDutyPaySchema,
                                      LCDutySchema tLCDutySchema) {
        mLCPremSchema.setPayPlanType(tLMDutyPaySchema.getType());
        mLCPremSchema.setUrgePayFlag(tLMDutyPaySchema.getUrgePayFlag());
        mLCPremSchema.setNeedAcc(tLMDutyPaySchema.getNeedAcc());
        mLCPremSchema.setState("0");

        // 判断 PayIntv 的取值位置
        if (chkInput(mLCPremSchema.getPayPlanCode(), "P", "PayIntv")) {
            mLCPremSchema.setPayIntv(mLCDutyBLIn.getPayIntv());
        } else {
            mLCPremSchema.setPayIntv(tLMDutyPaySchema.getPayIntv());
        }

        // 计算缴费终止日期
        Date baseDate = null;
        Date compDate = null;
        int interval = 0;
        String unit = null;
        Date payEndDate = null;
        int payEndYear = 0;
        String payEndYearFlag = null;

        // 判断 payEndYear 的取值位置
        if (chkInput(mLCPremSchema.getPayPlanCode(), "P", "PayEndYear")) {
            if (mLCDutyBLIn.getPayEndYear() == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calPremOther";
                tError.errorMessage = "必须录入缴费终止年期!";
                this.mErrors.addOneError(tError);
            }
            payEndYear = mLCDutyBLIn.getPayEndYear();
        } else {
            if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
                    "3")) { // 取保险终止日期
                payEndYear = tLCDutySchema.getInsuYear();
            }

            if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
                    "4")) { // 取领取开始日期
                payEndYear = tLCDutySchema.getGetYear();
            }

            if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
                    "0")) { // 以计算为准
                payEndYear = tLMDutyPaySchema.getPayEndYear();
            }
        }

        // 判断 payEndYearFlag 的取值位置
        if (chkInput(mLCPremSchema.getPayPlanCode(), "P", "PayEndYearFlag")) {
            if (mLCDutyBLIn.getPayEndYearFlag() == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calPremOther";
                tError.errorMessage = "必须录入缴费终止年期标志!";
                System.out.println("必须录入缴费终止年期标志:请检查责任项的payendyearflag标记");
                this.mErrors.addOneError(tError);
            }
            payEndYearFlag = mLCDutyBLIn.getPayEndYearFlag().toUpperCase();
        } else {
            if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
                    "3")) { // 取保险终止日期
                payEndYearFlag = tLCDutySchema.getInsuYearFlag().toUpperCase();
            }

            if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
                    "4")) { // 取领取开始日期
                payEndYearFlag = tLCDutySchema.getGetYearFlag().toUpperCase();
            }

            if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
                    "0")) { // 以计算为准
                payEndYearFlag = tLMDutyPaySchema.getPayEndYearFlag().
                                 toUpperCase();
            }
        }

        // 计算
        if (StrTool.cTrim(payEndYearFlag).equals("A")) {
            baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
            unit = "Y";

            if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalRef()).equals(
                    "S")) { // 起保日期对应日
                compDate = fDate.getDate(mLCPolBL.getCValiDate());
            }
            if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalRef()).equals(
                    "B")) { // 生日对应日
                compDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
            }
        } else {
            baseDate = fDate.getDate(mLCPolBL.getCValiDate());
            unit = payEndYearFlag;
        }
        interval = payEndYear;

        if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals("4")) { // 取领取开始日期
            if (StrTool.cTrim(tLCDutySchema.getGetStartType()).equals("B")) {
                compDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
            }

            if (StrTool.cTrim(tLCDutySchema.getGetStartType()).equals("S")) {
                compDate = fDate.getDate(mLCPolBL.getCValiDate());
            }
        }

        payEndDate = PubFun.calDate(baseDate, interval, unit, compDate);
        //        if(0 == mLCPolBL.getPayIntv() && "2".equals(mLCPolBL.getContType())){
        //            payEndDate = new FDate().getDate(mLCGrpContSchemma.getCInValiDate());
        //            payEndDate = PubFun.calDate(payEndDate, 1, "D", null);
        //        }

        mLCPremSchema.setPayEndDate(fDate.getString(payEndDate));
        //System.out.println("payEndDate: " + mLCPremSchema.getPayEndDate() );

        // 准备计算基础数据部分
        mCalBase.setPayEndYear(payEndYear);
        mCalBase.setPayEndYearFlag(payEndYearFlag);

        return mLCPremSchema;
    }

    /**
     * 通过描述取得保费项信息的结构
     * @param: 无
     * @return: boolean
     **/
    private boolean getGetStructure(LCDutySchema mLCDutySchema) {
        LMDutyGetRelaSet tLMDutyGetRelaSet =
                mCRI.findDutyGetRelaByDutyCode(mLCDutySchema.getDutyCode());

        if (tLMDutyGetRelaSet == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "getGetStructure";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        // 把数据装入mLCGetBLSet中
        int n = tLMDutyGetRelaSet.size();
        for (int i = 1; i <= n; i++) {
            LCGetBL tLCGetBL = new LCGetBL();
            tLCGetBL.setDutyCode(mLCDutySchema.getDutyCode());
            tLCGetBL.setGetDutyCode(tLMDutyGetRelaSet.get(i).getGetDutyCode());

            mLCGetBLSet.add(tLCGetBL);
        }
        return true;
    }

    /**
     * 从保单信息或责任信息中获取领取项表中需要计算的以外的信息
     * @param: 无
     * @return: 无
     **/
    private void getGetOtherData() {
        int n = mLCGetBLSet.size();
        for (int i = 1; i <= n; i++) {
            LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);
            //PQ1 需要设一些从LCCont获得的值
            // 从保单获得的信息
            tLCGetSchema.setPolNo(mLCPolBL.getPolNo());
            tLCGetSchema.setContNo(mLCPolBL.getContNo());
            tLCGetSchema.setGrpContNo(mLCPolBL.getGrpContNo());
            //tLCGetSchema.setAppntNo( mLCPolBL.getAppntNo() );
            // tLCGetSchema.setAppntType( mLCPolBL.getContType() );

            tLCGetSchema.setInsuredNo(mLCPolBL.getInsuredNo());

            tLCGetSchema.setGetMode(mLCPolBL.getLiveGetMode());
            tLCGetSchema.setManageCom(mLCPolBL.getManageCom());

            mLCGetBLSet.set(i, tLCGetSchema);
        }
    }

    /**
     * 计算领取项表中实际领取的值
     * @param: 无
     * @return: 实际领取值
     **/
    private double calGetValue(LCGetSchema mLCGetSchema,
                               LCDutySchema tLCDutySchema,
                               LMDutyGetSchema tLMDutyGetSchema, String calFlag) {
        double mValue = -1;

        String mCalCode = null;
        if (StrTool.cTrim(calFlag).equals("P")) {
            mCalCode = tLMDutyGetSchema.getCalCode(); // 保费算保额
        }
        if (StrTool.cTrim(calFlag).equals("G")) {
            mCalCode = tLMDutyGetSchema.getCnterCalCode(); // 保额算保费
        }
        if (StrTool.cTrim(calFlag).equals("O")) {
            mCalCode = tLMDutyGetSchema.getOthCalCode(); // 其他算保费
        }
        if (StrTool.cTrim(calFlag).equals("A") &&
            tLCDutySchema.getStandPrem() != 0.0) {
            mCalCode = tLMDutyGetSchema.getCalCode(); // 保费、保额互算
        }
        if (StrTool.cTrim(calFlag).equals("A") &&
            tLCDutySchema.getAmnt() != 0.0) {
            mCalCode = tLMDutyGetSchema.getCnterCalCode(); // 保费、保额互算
        }
        if (StrTool.cTrim(calFlag).equals("I")) {
            mCalCode = tLMDutyGetSchema.getCnterCalCode(); // 录入保费保额
        }

        //取默认值
        if (StrTool.cTrim(mCalCode).equals("")) {
            mValue = tLMDutyGetSchema.getDefaultVal();
            return mValue;
        }

        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear());
        mCalculator.addBasicFactor("GetYearFlag", mCalBase.getGetYearFlag());
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("InsuYear", mCalBase.getInsuYear());
        mCalculator.addBasicFactor("InsuYearFlag", mCalBase.getInsuYearFlag());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("RnewFlag", mCalBase.getRnewFlag());
        mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("FRate", mCalBase.getFloatRate());
        mCalculator.addBasicFactor("GetDutyKind", mCalBase.getGetDutyKind());
        mCalculator.addBasicFactor("StandbyFlag1", mCalBase.getStandbyFlag1());
        mCalculator.addBasicFactor("StandbyFlag2", mCalBase.getStandbyFlag2());
        mCalculator.addBasicFactor("StandbyFlag3", mCalBase.getStandbyFlag3());
        mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
        mCalculator.addBasicFactor("GetLimit", mCalBase.getGetLimit());
        mCalculator.addBasicFactor("GetRate", mCalBase.getGetRate());
        mCalculator.addBasicFactor("SSFlag", mCalBase.getSSFlag());
        mCalculator.addBasicFactor("PeakLine", mCalBase.getPeakLine());
        mCalculator.addBasicFactor("CalType", mCalBase.getCalType());
        mCalculator.addBasicFactor("EdorValiDate", mCalBase.getEdorValiDate());
        mCalculator.addBasicFactor("CInValiDate", mCalBase.getCInValiDate());
        mCalculator.addBasicFactor("CalRule", mCalBase.getCalRule());
        mCalculator.addBasicFactor("LastPayToDate", mCalBase.getLastPayToDate());
        mCalculator.addBasicFactor("PayToDate", mCalBase.getPayToDate());
        mCalculator.addBasicFactor("RevertType", mCalBase.getRevertType());
        mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());
        mCalculator.addBasicFactor("Copys", mCalBase.getCopys());
        mCalculator.addBasicFactor("RiskWrapCode", mRiskWrapCode);
        //add by zjd
        mCalculator.addBasicFactor("ContPlanCode", mLCPolBL.getContPlanCode());
        //mCalculator.addBasicFactor("GrpGetIntv", mGrpGetIntv);
        this.prepareDutyParam(mCalculator,tLCDutySchema);
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr.trim().equals("")) {
            mValue = 0;
        } else {
            mValue = Double.parseDouble(tStr);
        }

        //System.out.println(mValue);
        return mValue;
    }

    /**
     * 计算领取项表中的其他值
     * @param: 无
     * @return: boolean
     **/
    private LCGetSchema calGetOther(LCGetSchema mLCGetSchema,
                                    LMDutyGetSchema tLMDutyGetSchema,
                                    LCDutySchema tLCDutySchema) {
        mLCGetSchema.setLiveGetType(tLMDutyGetSchema.getType());
        mLCGetSchema.setNeedAcc(tLMDutyGetSchema.getNeedAcc());
        mLCGetSchema.setCanGet(tLMDutyGetSchema.getCanGet());
        mLCGetSchema.setNeedCancelAcc(tLMDutyGetSchema.getNeedCancelAcc());

        //DiscntFlag  1-只能现金领取, 0-可以选择。
        if (tLMDutyGetSchema.getDiscntFlag() == null) { //如果是空，默认是0
            tLMDutyGetSchema.setDiscntFlag("0");
        }
        if (tLMDutyGetSchema.getDiscntFlag().equals("1")) { //如果是1，则领取项必须是现金
            //可以查找该行，前面已使用,如果用于接收界面输入的领取方式，若描述表中是1，则将该值覆盖。
            mLCGetSchema.setGetMode("1");
        } else {
            mLCGetSchema.setGetMode(mLCPolBL.getLiveGetMode());
        }

        LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
        if (StrTool.cTrim(tLMDutyGetSchema.getType()).equals("0")) { // 生存领取类型
            // 给付责任类型GetDutyKind
            if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "GetDutyKind")) {
                mLCGetSchema.setGetDutyKind(mLCGetBLIn.getGetDutyKind());
                LMDutyGetAliveSchema tempLMDutyGetAliveSchema =
                        mCRI.findDutyGetAlive(
                                mLCGetSchema.getGetDutyCode(),
                                mLCGetSchema.getGetDutyKind());

                if (tempLMDutyGetAliveSchema == null) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(mCRI.mErrors);
                    mCRI.mErrors.clearErrors();

                    CError tError = new CError();
                    tError.moduleName = "CalBL";
                    tError.functionName = "calGetOther";
                    tError.errorMessage = "LMDutyGetAlive表查询失败!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                tLMDutyGetAliveSchema.setSchema(tempLMDutyGetAliveSchema);
            } else {
                LMDutyGetAliveSet tLMDutyGetAliveSet =
                        mCRI.findDutyGetAliveByGetDutyCodeClone(mLCGetSchema.
                        getGetDutyCode());

                if (tLMDutyGetAliveSet == null) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(mCRI.mErrors);
                    mCRI.mErrors.clearErrors();

                    CError tError = new CError();
                    tError.moduleName = "CalBL";
                    tError.functionName = "calGetOther";
                    tError.errorMessage = "LMDutyGetAlive表查询失败!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                tLMDutyGetAliveSchema = tLMDutyGetAliveSet.get(1);
                mLCGetSchema.setGetDutyKind(tLMDutyGetAliveSchema.
                                            getGetDutyKind());
            } // end of if

            // 复制描述数据
            tLMDutyGetSchema.setGetIntv(tLMDutyGetAliveSchema.getGetIntv());
            tLMDutyGetSchema.setDefaultVal(tLMDutyGetAliveSchema.getDefaultVal());
            tLMDutyGetSchema.setCalCode(tLMDutyGetAliveSchema.getCalCode());
            tLMDutyGetSchema.setCnterCalCode(tLMDutyGetAliveSchema.
                                             getCnterCalCode());
            tLMDutyGetSchema.setOthCalCode(tLMDutyGetAliveSchema.getOthCalCode());
            tLMDutyGetSchema.setGetYear(tLMDutyGetAliveSchema.getGetStartPeriod());
            tLMDutyGetSchema.setGetYearFlag(tLMDutyGetAliveSchema.
                                            getGetStartUnit());
            tLMDutyGetSchema.setStartDateCalRef(tLMDutyGetAliveSchema.
                                                getStartDateCalRef());
            tLMDutyGetSchema.setStartDateCalMode(tLMDutyGetAliveSchema.
                                                 getStartDateCalMode());
            tLMDutyGetSchema.setMinGetStartPeriod(tLMDutyGetAliveSchema.
                                                  getMinGetStartPeriod());
            tLMDutyGetSchema.setGetEndPeriod(tLMDutyGetAliveSchema.
                                             getGetEndPeriod());
            tLMDutyGetSchema.setGetEndUnit(tLMDutyGetAliveSchema.getGetEndUnit());
            tLMDutyGetSchema.setEndDateCalRef(tLMDutyGetAliveSchema.
                                              getEndDateCalRef());
            tLMDutyGetSchema.setEndDateCalMode(tLMDutyGetAliveSchema.
                                               getEndDateCalMode());
            tLMDutyGetSchema.setMaxGetEndPeriod(tLMDutyGetAliveSchema.
                                                getMaxGetEndPeriod());
            tLMDutyGetSchema.setAddFlag(tLMDutyGetAliveSchema.getAddFlag());
            tLMDutyGetSchema.setUrgeGetFlag(tLMDutyGetAliveSchema.
                                            getUrgeGetFlag());
            //tLMDutyGetSchema.setDiscntFlag( tLMDutyGetAliveSchema.getDiscntFlag() );
        } // end of if

        mLCGetSchema.setUrgeGetFlag(tLMDutyGetSchema.getUrgeGetFlag());
        if (mLCGetSchema.getGetIntv() == 0) {
            mLCGetSchema.setGetIntv(tLMDutyGetSchema.getGetIntv());
        }
        if (mLCGetSchema.getGetLimit() == 0.0) {
            mLCGetSchema.setGetLimit(tLMDutyGetSchema.getGetLimit());
        }
        if (mLCGetSchema.getGetRate() == 0.0) {
            mLCGetSchema.setGetRate(tLMDutyGetSchema.getGetRate());
        }
        String s = mLCGetSchema.getState();
        if (s != null) {
            s = tLMDutyGetSchema.getAddAmntFlag() + s.substring(1, s.length());
        } else {
            s = tLMDutyGetSchema.getAddAmntFlag();
        }

        mLCGetSchema.setState(s);

        // 递增率
        if (StrTool.cTrim(tLMDutyGetSchema.getAddFlag()).equals("Y")) {
            if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "AddRate")) {
                mLCGetSchema.setAddRate(mLCGetBLIn.getAddRate());
            } else {
                mLCGetSchema.setAddRate(tLMDutyGetAliveSchema.getAddValue());
            }
        }

        // 准备计算基础数据部分
        mCalBase.setGetDutyKind(mLCGetSchema.getGetDutyKind());
        mCalBase.setAddRate(mLCGetSchema.getAddRate());

        // 计算日期
        Date baseDate = null;
        Date compDate = null;
        int interval = 0;
        String unit = null;
        Date getStartDate = null;
        Date getEndDate = null;

        // 计算领取开始日期
        String getStartType = null;
        String getYearFlag = null;
        int getYear = 0;

        // 判断 getYear 的取值位置
        if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "GetYear")) {
            if (mLCDutyBLIn.getGetYear() == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calGetOther";
                tError.errorMessage = "必须录入开始领取年期!";
                this.mErrors.addOneError(tError);
                return null;
            }
            getYear = mLCDutyBLIn.getGetYear();
        } else {
            getYear = tLMDutyGetSchema.getGetYear();
        }

        // 判断 getYearFlag 的取值位置
        if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "GetYearFlag")) {
            if (mLCDutyBLIn.getGetYearFlag() == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calGetOther";
                tError.errorMessage = "必须录入开始领取年期标志!";
                this.mErrors.addOneError(tError);
                return null;
            }
            getYearFlag = mLCDutyBLIn.getGetYearFlag();
        } else {
            getYearFlag = tLMDutyGetSchema.getGetYearFlag();
        }

        // 判断 getStartType 的取值位置
        if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "GetStartType")) {
            if (mLCDutyBLIn.getGetStartType() == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calGetOther";
                tError.errorMessage = "必须录入领取日期计算类型!";
                this.mErrors.addOneError(tError);
                return null;
            }
            getStartType = mLCDutyBLIn.getGetStartType();
        } else {
            getStartType = tLMDutyGetSchema.getStartDateCalRef();
        }

        // 计算
        if (StrTool.cTrim(getYearFlag).equals("A")) {
            baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
            unit = "Y";

            if (StrTool.cTrim(getStartType).equals("S")) { // 起保日期对应日
                compDate = fDate.getDate(mLCPolBL.getCValiDate());
            }
            if (StrTool.cTrim(getStartType).equals("B")) { // 生日对应日
                compDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
            }
        } else {
            baseDate = fDate.getDate(mLCPolBL.getCValiDate());
            unit = getYearFlag;
        }
        interval = getYear;

        if (StrTool.cTrim(tLMDutyGetSchema.getStartDateCalMode()).equals("3")) { // 取保险终止日期
            mLCGetSchema.setGetStartDate(tLCDutySchema.getEndDate());
        }
        if (StrTool.cTrim(tLMDutyGetSchema.getStartDateCalMode()).equals("0")) { // 取计算结果
            getStartDate = PubFun.calDate(baseDate, interval, unit, compDate);
            mLCGetSchema.setGetStartDate(fDate.getString(getStartDate));
        }

        mLCGetSchema.setGettoDate(mLCGetSchema.getGetStartDate());
        //System.out.println("getStartDate: " + mLCGetSchema.getGetStartDate() );

        // 准备计算基础数据部分
        mCalBase.setGetYear(getYear);
        mCalBase.setGetYearFlag(getYearFlag);

        // 计算领取终止日期
        if (StrTool.cTrim(tLMDutyGetSchema.getEndDateCalMode()).equals("3")) { // 取保险终止日期
            mLCGetSchema.setGetEndDate(tLCDutySchema.getEndDate());
        }

        if (StrTool.cTrim(tLMDutyGetSchema.getEndDateCalMode()).equals("0")) { // 取计算的值
            if (StrTool.cTrim(tLMDutyGetSchema.getGetEndUnit()).equals("A")) {
                baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
                unit = "Y";
                if (StrTool.cTrim(tLMDutyGetSchema.getEndDateCalRef()).equals(
                        "B")) {
                    compDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
                }
                if (StrTool.cTrim(tLMDutyGetSchema.getEndDateCalRef()).equals(
                        "S")) {
                    compDate = fDate.getDate(mLCPolBL.getCValiDate());
                }
            } else {
                baseDate = fDate.getDate(mLCPolBL.getCValiDate());
                unit = tLMDutyGetSchema.getGetEndUnit();
            }
            interval = tLMDutyGetSchema.getGetEndPeriod();
            getEndDate = PubFun.calDate(baseDate, interval, unit, compDate);
            mLCGetSchema.setGetEndDate(fDate.getString(getEndDate));
        }
        //System.out.println("getEndDate: " + mLCGetSchema.getGetEndDate() );

        return mLCGetSchema;
    }

    /**
     * 通过描述取得责任信息的结构
     * @param: 无
     * @return: boolean
     **/
    private boolean getDutyStructure() {
        // 从险种责任表中取出责任的结构
//        LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
//
//        tLMRiskDutyDB.setRiskCode( mLCPolBL.getRiskCode() );
//        tLMRiskDutyDB.setChoFlag("M");
//
//        LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
//        if (tLMRiskDutyDB.mErrors.needDealError())
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tLMRiskDutyDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "CalBL";
//            tError.functionName = "getDutyStructure";
//            tError.errorMessage = "LMRiskDuty表查询失败!" ;
//            this.mErrors.addOneError(tError) ;
//            return false;
//        }

        LMRiskDutySet tLMRiskDutySet =
                mCRI.findRiskDutyByRiskCode(mLCPolBL.getRiskCode());

        if (tLMRiskDutySet == null) {
            // @@错误处理
            this.mErrors.copyAllErrors(mCRI.mErrors);
            mCRI.mErrors.clearErrors();

            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "getDutyStructure";
            tError.errorMessage = "LMRiskDuty表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        // 把数据装入mLCDutyBLSet中
        mLCDutyBLSet = new LCDutyBLSet();
        int n = tLMRiskDutySet.size();
        for (int i = 1; i <= n; i++) {
            if (!StrTool.cTrim(tLMRiskDutySet.get(i).getChoFlag()).equals("M")) {
                continue;
            }

            LCDutyBL tLCDutyBL = new LCDutyBL();
            tLCDutyBL.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());

            if (mLCDutyBLIn != null) { // 录入优先
                tLCDutyBL.setPayIntv(mLCDutyBLIn.getPayIntv());
                tLCDutyBL.setInsuYearFlag(mLCDutyBLIn.getInsuYearFlag());
                tLCDutyBL.setInsuYear(mLCDutyBLIn.getInsuYear());
                tLCDutyBL.setAcciYearFlag(mLCDutyBLIn.getAcciYearFlag());
                tLCDutyBL.setAcciYear(mLCDutyBLIn.getAcciYear());
                tLCDutyBL.setPayEndYear(mLCDutyBLIn.getPayEndYear());
                tLCDutyBL.setPayEndYearFlag(mLCDutyBLIn.getPayEndYearFlag());
                tLCDutyBL.setGetYear(mLCDutyBLIn.getGetYear());
                tLCDutyBL.setGetYearFlag(mLCDutyBLIn.getGetYearFlag());
                tLCDutyBL.setStandbyFlag1(mLCDutyBLIn.getStandbyFlag1());
                tLCDutyBL.setStandbyFlag2(mLCDutyBLIn.getStandbyFlag2());
                tLCDutyBL.setStandbyFlag3(mLCDutyBLIn.getStandbyFlag3());
                tLCDutyBL.setMult(mLCDutyBLIn.getMult());
                tLCDutyBL.setCalRule(mLCDutyBLIn.getCalRule());
                tLCDutyBL.setFloatRate(mLCDutyBLIn.getFloatRate());
                tLCDutyBL.setPremToAmnt(mLCDutyBLIn.getPremToAmnt());
                tLCDutyBL.setGetLimit(mLCDutyBLIn.getGetLimit());
                tLCDutyBL.setGetRate(mLCDutyBLIn.getGetRate());
                tLCDutyBL.setSSFlag(mLCDutyBLIn.getSSFlag());
                tLCDutyBL.setPeakLine(mLCDutyBLIn.getPeakLine());
                tLCDutyBL.setCopys(mLCDutyBLIn.getCopys());
            }

            mLCDutyBLSet.add(tLCDutyBL);
        }

        return true;
    }

    /**
     * 从保单信息中获取责任表中需要计算的以外的信息
     * @param: 无
     * @return: 无
     **/
    private void getDutyOtherData() {
        int n = mLCDutyBLSet.size();
        for (int i = 1; i <= n; i++) {
            LCDutySchema tLCDutySchema = mLCDutyBLSet.get(i);

            // 从保单带过来的信息
            //PQ1 需要设一些从LCCont带过来的值
            if (tLCDutySchema.getPolNo() == null) {
                tLCDutySchema.setPolNo(mLCPolBL.getPolNo());
            }
            if (tLCDutySchema.getContNo() == null) {
                tLCDutySchema.setContNo(mLCPolBL.getContNo());
            }
            if (tLCDutySchema.getMult() == 0.0) {
                tLCDutySchema.setMult(mLCPolBL.getMult());
            }
            if (tLCDutySchema.getFirstPayDate() == null) {
                tLCDutySchema.setFirstPayDate(mLCPolBL.getCValiDate());
            }
            String tsql = "select 1 from ldcode "
 			   		+ "where codetype='DutyCode' and code='"+tLCDutySchema.getDutyCode() + "' ";
        SSRS tSSRS = new ExeSQL().execSQL(tsql);
        if(tSSRS.getMaxRow()<=0){
            if (tLCDutySchema.getStandPrem() == 0.0) {
                if (tLCDutySchema.getPrem() == 0.0) {
                    tLCDutySchema.setPrem(mLCPolBL.getPrem());
                    tLCDutySchema.setStandPrem(mLCPolBL.getStandPrem());
                } else {
                    tLCDutySchema.setStandPrem(tLCDutySchema.getPrem());
                }
            }

            if (tLCDutySchema.getAmnt() == 0.0) {
                tLCDutySchema.setAmnt(mLCPolBL.getAmnt());
            }
            if (tLCDutySchema.getRiskAmnt() == 0.0) {
                tLCDutySchema.setRiskAmnt(mLCPolBL.getRiskAmnt());
            }
        }
            if (tLCDutySchema.getLiveGetMode() == null) {
                tLCDutySchema.setLiveGetMode(mLCPolBL.getLiveGetMode());
            }
            if (tLCDutySchema.getDeadGetMode() == null) {
                tLCDutySchema.setDeadGetMode(mLCPolBL.getDeadGetMode());
            }
            if (tLCDutySchema.getBonusGetMode() == null) {
                tLCDutySchema.setBonusGetMode(mLCPolBL.getBonusGetMode());
            }
            tLCDutySchema.setContNo(mLCPolBL.getContNo());
            tLCDutySchema.setOperator(mLCPolBL.getOperator());
            tLCDutySchema.setMakeDate(PubFun.getCurrentDate());
            tLCDutySchema.setMakeTime(PubFun.getCurrentTime());
            mLCDutyBLSet.set(i, tLCDutySchema);
        }
    }

    /**
     * 计算责任表中的其他相关信息
     * @param: 无
     * @return: boolean
     **/
    private LCDutySchema calDutyOther(LCDutySchema mLCDutySchema) {
//        LMDutyDB tLMDutyDB = new LMDutyDB();
//
//        tLMDutyDB.setDutyCode( mLCDutySchema.getDutyCode() );
//
//        if (tLMDutyDB.getInfo() == false)
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tLMDutyDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "CalBL";
//            tError.functionName = "calDutyOther";
//            tError.errorMessage = "LMDuty表查询失败!" ;
//            this.mErrors.addOneError(tError);
//        }
        LMDutySchema tLMDutySchema =
                mCRI.findDutyByDutyCodeClone(mLCDutySchema.getDutyCode());

        if (tLMDutySchema == null) {
            // @@错误处理
            this.mErrors.copyAllErrors(mCRI.mErrors);
            mCRI.mErrors.clearErrors();

            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "calDutyOther";
            tError.errorMessage = "LMDuty表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (mLCDutyBLIn.getGetStartType() != null) {
            mLCDutySchema.setGetStartType(mLCDutyBLIn.getGetStartType());
        }

        Date baseDate = null;
        Date compDate = null;
        int interval = 0;
        String unit = null;

        int insuYearIn = mLCDutyBLIn.getInsuYear();
        String insuYearFlagIn = mLCDutyBLIn.getInsuYearFlag();
        int payEndYearIn = mLCDutyBLIn.getPayEndYear();
        String payEndYearFlagIn = mLCDutyBLIn.getPayEndYearFlag();
        int getYearIn = mLCDutyBLIn.getGetYear();
        String getYearFlagIn = mLCDutyBLIn.getGetYearFlag();

        // 保险期限
        Date endDate = null;
        int insuYear = 0;
        int years = 0;
        String insuYearFlag = null;

        // 判断 InsuYear 的取值位置
        if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("0")) { // 取录入值
            if (mLCDutyBLIn.getInsuYear() == 0 || mLCDutyBLIn.getInsuYearFlag() == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calDutyOther";
                tError.errorMessage = "必须录入保险终止年期及年期单位!";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (mLCDutyBLIn.getInsuYearFlag().trim().equals("")) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calDutyOther";
                tError.errorMessage = "必须录入保险终止年期及年期单位!";
                this.mErrors.addOneError(tError);
                return null;
            }
            insuYear = insuYearIn;
            insuYearFlag = insuYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("1")) { // 和缴费终止期间的值相同
            insuYear = payEndYearIn;
            insuYearFlag = payEndYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("2")) { // 和起领期间的值相同
            insuYear = getYearIn;
            insuYearFlag = getYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("4")) { // 从描述表取其默认值
            insuYear = tLMDutySchema.getInsuYear();
            insuYearFlag = tLMDutySchema.getInsuYearFlag();
        }
        if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("5")) { // 更据算法，将缴费终止期间和默认值相加
            insuYear = tLMDutySchema.getInsuYear() + payEndYearIn;
            insuYearFlag = payEndYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("6")) { // 更据算法，将起领期间和默认值相加
            insuYear = tLMDutySchema.getInsuYear() + getYearIn;
            insuYearFlag = getYearFlagIn;
        }

        // 计算
        if (mLCDutySchema.getEndDate() == null ||
            mLCDutySchema.getEndDate() == "") {
            baseDate = fDate.getDate(mLCPolBL.getCValiDate());
            unit = insuYearFlag;
            interval = insuYear;
            compDate = null;
            if (unit == null && interval == 0) {
                endDate = fDate.getDate("3000-01-01");
            } else {
                if (StrTool.cTrim(unit).equals("A")) {
                    baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
                    unit = "Y";
                    compDate = fDate.getDate(mLCPolBL.getCValiDate());
                } else {
                    baseDate = fDate.getDate(mLCPolBL.getCValiDate());
                }

                endDate = PubFun.calDate(baseDate, interval, unit, compDate);
                //                if(0 == mLCPolBL.getPayIntv()
                //                   && "2".equals(mLCPolBL.getContType())){
                //                    endDate = new FDate().getDate(
                //                        mLCGrpContSchemma.getCInValiDate());
                //                    endDate = PubFun.calDate(endDate, 1, "D", null);
                //                }

                if (StrTool.cTrim(insuYearFlag).equals("A")) {
                    if(mLCPolBL.getCValiDate() == null)
                    {
                        CError tError = new CError();
                        tError.moduleName = "CalBL";
                        tError.functionName = "calDutyOther";
                        tError.errorMessage = "没有传入生效日期";
                        mErrors.addOneError(tError);
                        System.out.println(tError.errorMessage);
                        return null;
                    }
                    years = PubFun.calInterval(fDate.getDate(mLCPolBL.
                            getCValiDate()), endDate, "Y");
                } else {
                    years = insuYear;
                }
                mLCDutySchema.setYears(years);
            }

            mLCDutySchema.setEndDate(fDate.getString(endDate));

        }
        mLCDutySchema.setInsuYearFlag(insuYearFlag.trim());
        mLCDutySchema.setInsuYear(insuYear);

        // 意外责任期限
        Date acciEndDate = null;
        int acciYear = 0;
        String acciYearFlag = null;

        // 判断 acciYear 的取值位置
        if (chkInput(mLCDutySchema.getDutyCode(), "D", "AcciYear")) {
            if (mLCDutyBLIn.getAcciYear() == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calDutyOther";
                tError.errorMessage = "必须录入意外责任终止年期及单位!";
                this.mErrors.addOneError(tError);
                return null;
            }
            acciYear = mLCDutyBLIn.getAcciYear();
        } else {
            acciYear = tLMDutySchema.getAcciYear();
        }

        // 判断 AcciYearFlag 的取值位置
        if (chkInput(mLCDutySchema.getDutyCode(), "D", "AcciYearFlag")) {
            if (mLCDutyBLIn.getAcciYearFlag() == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calDutyOther";
                tError.errorMessage = "必须录入意外责任终止年期标志!";
                this.mErrors.addOneError(tError);
                return null;
            }
//            if (mLCDutyBLIn.getAcciYearFlag().trim().equals(""))
//            {
//                // @@错误处理
//                CError tError = new CError();
//                tError.moduleName = "CalBL";
//                tError.functionName = "calDutyOther";
//                tError.errorMessage = "必须录入意外责任终止年期标志!" ;
//                this.mErrors.addOneError(tError) ;
//                return null;
//            }
            acciYearFlag = mLCDutyBLIn.getAcciYearFlag();
        } else {
            acciYearFlag = tLMDutySchema.getAcciYearFlag();
        }

        // 计算
        if (StrTool.cTrim(mLCDutySchema.getAcciEndDate()).equals("") &&
            acciYear != 0) {
            baseDate = fDate.getDate(mLCPolBL.getCValiDate());
            unit = acciYearFlag;
            interval = acciYear;
            compDate = null;
            if (!(unit == null && interval == 0)) {
                if (StrTool.cTrim(unit).equals("A")) {
                    baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
                    unit = "Y";
                    compDate = fDate.getDate(mLCPolBL.getCValiDate());
                } else {
                    baseDate = fDate.getDate(mLCPolBL.getCValiDate());
                }

                acciEndDate = PubFun.calDate(baseDate, interval, unit, compDate);

                mLCDutySchema.setAcciEndDate(fDate.getString(acciEndDate));
            }
        }
        mLCDutySchema.setAcciYearFlag(acciYearFlag);
        mLCDutySchema.setAcciYear(acciYear);

        // 判断 PayEndYear 的取值位置
        int payEndYear = 0;
        String payEndYearFlag = null;
        if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("0")) { // 取录入值
            System.out.println("mLCDutyBLIn.getPayIntv() : " +
                               mLCDutyBLIn.getPayIntv());
            if (mLCDutyBLIn.getPayIntv() != 0 &&
                (mLCDutyBLIn.getPayEndYear() == 0 ||
                 mLCDutyBLIn.getPayEndYearFlag() == null)) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calDutyOther";
                tError.errorMessage = "必须录入缴费终止年期和缴费终止年期单位!";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (mLCDutyBLIn.getPayIntv() != 0 &&
                mLCDutyBLIn.getPayEndYearFlag().trim().equals("")) {
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calDutyOther";
                tError.errorMessage = "必须录入缴费终止年期和缴费终止年期单位!";
                this.mErrors.addOneError(tError);
                return null;
            }

            payEndYear = payEndYearIn;
            payEndYearFlag = payEndYearFlagIn;
            /** 如果缴费频次为系统默认值 */
            if (mLCDutyBLIn.getPayIntv() == 0) {
                payEndYear = insuYearIn;
                mLCDutyBLIn.setPayEndYear(payEndYear);
                mLCPolBL.setPayEndYear(payEndYear);
                payEndYearFlag = insuYearFlagIn;
                mLCDutyBLIn.setPayEndYearFlag(insuYearFlagIn);
                mLCPolBL.setPayEndYearFlag(payEndYearFlag);
            }
        }
        if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("3")) { // 和保险期间的值相同
            payEndYear = insuYearIn;
            payEndYearFlag = insuYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("2")) { // 和起领期间的值相同
            payEndYear = getYearIn;
            payEndYearFlag = getYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("4")) { // 从描述表取其默认值
            payEndYear = tLMDutySchema.getPayEndYear();
            payEndYearFlag = tLMDutySchema.getPayEndYearFlag();
        }
        if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("7")) { // 更据算法，将保险期间和默认值相加
            payEndYear = tLMDutySchema.getPayEndYear() + insuYearIn;
            payEndYearFlag = insuYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("6")) { // 更据算法，将起领期间和默认值相加
            payEndYear = tLMDutySchema.getPayEndYear() + getYearIn;
            payEndYearFlag = getYearFlagIn;
        }
        mLCDutySchema.setPayEndYearFlag(payEndYearFlag);
        mLCDutySchema.setPayEndYear(payEndYear);

        // 判断 GetYear 的取值位置
        int getYear = 0;
        String getYearFlag = null;
        if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("0")) { // 取录入值

            if (mLCDutyBLIn.getGetYear() == 0 || mLCDutyBLIn.getGetYearFlag() == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calDutyOther";
                tError.errorMessage = "必须录入起领年期及单位!";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (mLCDutyBLIn.getGetYearFlag().trim().equals("")) {
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calDutyOther";
                tError.errorMessage = "必须录入起领年期及单位!";
                this.mErrors.addOneError(tError);
                return null;
            }

            getYear = getYearIn;
            getYearFlag = getYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("3")) { // 和保险期间的值相同
            getYear = insuYearIn;
            getYearFlag = insuYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("1")) { // 和交费终止期间的值相同
            getYear = payEndYearIn;
            getYearFlag = payEndYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("4")) { // 从描述表取其默认值
            getYear = tLMDutySchema.getGetYear();
            getYearFlag = tLMDutySchema.getGetYearFlag();
        }
        if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("7")) { // 更据算法，将保险期间和默认值相加
            getYear = tLMDutySchema.getGetYear() + insuYearIn;
            getYearFlag = insuYearFlagIn;
        }
        if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("5")) { // 更据算法，将交费终止期间和默认值相加
            getYear = tLMDutySchema.getGetYear() + payEndYearIn;
            getYearFlag = payEndYearFlagIn;
        }
        mLCDutySchema.setGetYearFlag(getYearFlag);
        mLCDutySchema.setGetYear(getYear);

        return mLCDutySchema;
    }

    /**
     * 从保费项表中取得责任表中部分信息
     * @param: 无
     * @return: boolean
     **/
    private LCDutySchema calDutyFromPrem(LCDutySchema mLCDutySchema,
                                         LMDutySchema tLMDutySchema) {
        String dutyCode = mLCDutySchema.getDutyCode();

        double sumPrem = 0;
        Date maxPayEndDate = fDate.getDate("1900-01-01");
        int payIntv = -8;
        int n = mLCPremBLSet.size();
        for (int i = 1; i <= n; i++) {
            LCPremBL mLCPremBL = (LCPremBL) mLCPremBLSet.get(i);

            if (StrTool.cTrim(mLCPremBL.getDutyCode()).equals(dutyCode)) {
                payIntv = mLCPremBL.getPayIntv();
                sumPrem += mLCPremBL.getStandPrem();

                Date payEndDate = fDate.getDate(mLCPremBL.getPayEndDate());
                if (maxPayEndDate.before(payEndDate)) {
                    maxPayEndDate = payEndDate;
                }
            }
        }

        // 计算缴费年期
        Date birthday = fDate.getDate(mLCPolBL.getInsuredBirthday());
        Date startDate = fDate.getDate(mLCPolBL.getCValiDate());
        int payYears = PubFun.calInterval(startDate, maxPayEndDate, "Y");
        mLCDutySchema.setPayYears(payYears);

        mLCDutySchema.setPayIntv(payIntv);
        mLCDutySchema.setStandPrem(sumPrem);
        mLCDutySchema.setPrem(sumPrem);
        mLCDutySchema.setPayEndDate(fDate.getString(maxPayEndDate));

        System.out.println("CalBL:calDutyFromPrem:PayEndDate:" +
                           mLCDutySchema.getPayEndDate());
        return mLCDutySchema;
    }

    /**
     * 从领取项表中取得责任表中部分信息
     * @param: 无
     * @return: boolean
     **/
    private LCDutySchema calDutyFromGet(LCDutySchema mLCDutySchema,
                                        LMDutySchema tLMDutySchema) {
        String dutyCode = mLCDutySchema.getDutyCode();

        double sumAmnt = 0;
        Date minGetStartDate = fDate.getDate("3000-01-01");
        int n = mLCGetBLSet.size();
        for (int i = 1; i <= n; i++) {
            if (mLCGetBLSet.get(i).getDutyCode().equals(dutyCode)) {
                LCGetBL mLCGetBL = (LCGetBL) mLCGetBLSet.get(i);

                if (mLCGetBL.getState().substring(0, 1).equals("Y")) {
                    sumAmnt += mLCGetBL.getStandMoney();
                }

                Date getStartDate = fDate.getDate(mLCGetBL.getGetStartDate());
                if (minGetStartDate.after(getStartDate)) {
                    minGetStartDate = getStartDate;
                }
            }
        }
        mCalBase.setAmnt(sumAmnt);

        // 计算基本保额和风险保额
        mLCDutySchema = this.calAmnt(mLCDutySchema, tLMDutySchema);

        mLCDutySchema.setGetStartDate(fDate.getString(minGetStartDate));

        return mLCDutySchema;
    }

    /**
     * 计算一个责任下的所有信息
     * @param: LCDutySchema
     * @return: boolean
     **/
    private LCDutySchema calOneDuty(LCDutySchema mLCDutySchema) {
        String dutyCode = mLCDutySchema.getDutyCode();
        String calMode = "";

        //如果界面没有录入浮动费率，此时mLCDutySchema的FloatRate=0，那么在后面的程序中，会将mLCPolBL的FloatRate字段置为1
        //保存浮动费率,录入的保费，保额:用于后面的浮动费率的计算
        FloatRate = mLCDutySchema.getFloatRate();

        InputPrem = mLCDutySchema.getPrem();
        InputAmnt = mLCDutySchema.getAmnt();

//        String strCalPrem = mDecimalFormat.format(InputPrem); //转换计算后的保费(规定的精度)
//        String strCalAmnt = mDecimalFormat.format(InputAmnt); //转换计算后的保额
//        InputPrem = Double.parseDouble(strCalPrem);
//        InputAmnt = Double.parseDouble(strCalAmnt);

        //保存计算方向
        String calRule = mLCDutySchema.getCalRule();
        if (calRule == null) {
            //默认为表定费率
            mLCDutySchema.setCalRule("0");
        }
        //如果是统一费率，需要去默认值里取出统一费率的值,算出保费保额
        else if (calRule.equals("1")) {
            autoCalFloatRateFlag = true;
        } else if (calRule.equals("3") &&
                   !StrTool.cTrim(this.calType).equals("NI")) {
            autoCalFloatRateFlag = true;
            FloatRate = -1;
        }
        //保全增人时需要无论是约定保费计算方式,还是表定计算方式都需要走描述算法所以处
        //理浮动费率的方式与标准费率计算方式相同
        else if (calRule.equals("3") && StrTool.cTrim(this.calType).equals("NI")) {
            autoCalFloatRateFlag = false;
            FloatRate = -1;
        }
        //计算方向为4的表示在磁盘投保文件中录入每一个人的约定保费,处理起来同约定保费相同
        //当时业务上需要区分保费计算方向是同一约定还是人员约定
        else if (calRule.equals("4") && StrTool.cTrim(this.calType).equals("NI")) {
            autoCalFloatRateFlag = true;
            FloatRate = -1;
        } else if (calRule.equals("4")&&!StrTool.cTrim(this.calType).equals("NI")) {
            autoCalFloatRateFlag = true;
            FloatRate = -1;
        }

        //如果界面录入浮动费率=-1，则计算浮动费率的标记为真
        if (FloatRate == ConstRate) {
            autoCalFloatRateFlag = true;

            //将保单中的该子段设为1，后面描述算法中计算
            mLCDutySchema.setFloatRate(1);
        }

        //浮动费率处理END

        // 准备计算基础数据部分
        mCalBase = new CalBase();
        mCalBase.setCalType(calType);
        mCalBase.setPrem(mLCDutySchema.getStandPrem());
//        mCalBase.setPrem( mLCDutySchema.getPrem() );
        mCalBase.setGet(mLCDutySchema.getAmnt());
        mCalBase.setMult(mLCDutySchema.getMult());
        //份数：按份卖的险种保存份数，其他险种存0。为了保证000301、000302算法的正确性，当份数为0时，该字段赋值为1。
//        if (mLCDutySchema.getMult() == 0) {
//            mCalBase.setMult(1);
//        }
        //modify by lxs 增加税优折扣因子字段
        LCContSubDB tLCContSubDB=new LCContSubDB();
        tLCContSubDB.setPrtNo(mLCPolBL.getPrtNo());
		if (tLCContSubDB.getInfo()) {
			String Totaldiscountfactor=tLCContSubDB.getTotaldiscountfactor();
		if (Totaldiscountfactor != ""&&Totaldiscountfactor!= "null"&&Totaldiscountfactor!=null) {
		mCalBase.setTotaldiscountfactor(Totaldiscountfactor);
		} 
		else {
				mCalBase.setTotaldiscountfactor("1");
			 }
		} 
		else {
			mCalBase.setTotaldiscountfactor("1");
		}
		mCalBase.setYears(mLCDutySchema.getYears());
		mCalBase.setInsuYear(mLCDutySchema.getInsuYear());
		mCalBase.setInsuYearFlag(mLCDutySchema.getInsuYearFlag());
		mCalBase.setAppAge(mLCPolBL.getInsuredAppAge());
		mCalBase.setSex(mLCPolBL.getInsuredSex());
		mCalBase.setJob(mLCPolBL.getOccupationType());
		mCalBase.setCount(mLCPolBL.getInsuredPeoples());
		mCalBase.setPolNo(mLCPolBL.getPolNo());
		mCalBase.setContNo(mLCPolBL.getContNo());
        //add by zjd contplancode
        mCalBase.setContPlanCode(mLCPolBL.getContPlanCode());
        mCalBase.setStandbyFlag1(mLCDutySchema.getStandbyFlag1());
        mCalBase.setStandbyFlag2(mLCDutySchema.getStandbyFlag2());
        mCalBase.setStandbyFlag3(mLCDutySchema.getStandbyFlag3());
        mCalBase.setGrpPolNo(mLCPolBL.getGrpPolNo());
        mCalBase.setGetLimit(mLCDutySchema.getGetLimit());
        mCalBase.setGetRate(mLCDutySchema.getGetRate());
        mCalBase.setSSFlag(mLCDutySchema.getSSFlag());
        mCalBase.setPeakLine(mLCDutySchema.getPeakLine());
        mCalBase.setCValiDate(mLCPolBL.getCValiDate());

        mCalBase.setComCode(getComCode(mLCPolBL.getManageCom()));
        mCalBase.setIndustry(this.mIndustry);
        mCalBase.setRetireRate(this.mRetireRate);
        mCalBase.setAvgAge(this.mAvgAge);
        mCalBase.setInsuredNum(this.mInsuredNum);
        mCalBase.setInsuredState(this.mInsuredState);
        mCalBase.setGrpContNo(mLCPolBL.getGrpContNo());
        mCalBase.setCopys(mLCDutySchema.getCopys());
        
        //一带一路算费要素
        String tprtno = mLCPolBL.getPrtNo();
        String sql="select 1 from lcgrppol where prtno='"+tprtno+"' and riskcode in ('163001','163002')";
    	ExeSQL exeSql = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = exeSql.execSQL(sql);
        if(ssrs.getMaxRow()!=0){
        	String tsql="select * from lcgrpcontroad where prtno='"+tprtno+"'";
        	LCGrpContRoadDB tLCGrpContRoadDB=new LCGrpContRoadDB();
        	LCGrpContRoadSet tLCGrpContRoadSet = new LCGrpContRoadSet();
        	tLCGrpContRoadSet = tLCGrpContRoadDB.executeQuery(tsql);
        	LCGrpContRoadSchema tLCGrpContRoadSchemma = new LCGrpContRoadSchema();
        	if(tLCGrpContRoadSet.size()!=1){
        		 CError tError = new CError();
                 tError.moduleName = "CalBL";
                 tError.functionName = "calOneDuty";
                 tError.errorMessage = "计算保费失败：未查询到一带一路产品要素!";
                 this.mErrors.addOneError(tError);
                 return mLCDutySchema;
        	}else{
        		tLCGrpContRoadSchemma=tLCGrpContRoadSet.get(1);
        		
        		mCalBase.setEngineeringFactor(tLCGrpContRoadSchemma.getEngineeringFactor());
        		mCalBase.setCountryFactor(tLCGrpContRoadSchemma.getCountryFactor());
        		mCalBase.setScaleFactor(tLCGrpContRoadSchemma.getScaleFactor());
        		mCalBase.setAvage(tLCGrpContRoadSchemma.getAvage());
        		mCalBase.setInsureyear(tLCGrpContRoadSchemma.getInsureyear());
        		
        	}
        }
        
        /**
         * 为了兼容保全的增人的磁盘投保,增加保全的各个要素
         * @autthor : Yangming
         */

        String GrpContNo = mLCPolBL.getGrpContNo();
        if (mLCPolBL.getContType().equals("2")) {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(GrpContNo);
            if (!tLCGrpContDB.getInfo()) {
                System.out.println(
                        "CalBL.calOneDuty(mLCDutySchema)  \n--Line:1784  --Author:YangMing");
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "calOneDuty";
                tError.errorMessage = "处理保全增人时报错！<br>在查找团体合同表时为查询到该团体保单信息!";
                this.mErrors.addOneError(tError);
                return mLCDutySchema;
            }
            
            mCalBase.setCInValiDate(mLCGrpContSchemma.getCInValiDate());
        }

        System.out.println("---------getlimit:" + mLCDutySchema.getGetLimit() +
                           "  getrate:" +
                           mLCDutySchema.getGetRate());

        if (mLCDutySchema.getFloatRate() == 0.0) {
            mLCDutySchema.setFloatRate(1);
        }
        mCalBase.setFloatRate(mLCDutySchema.getFloatRate());

        int tRnewFlag = -8;
        if (mLCPolBL.getRenewCount() == 0) {
            tRnewFlag = 0;
        }
        if (mLCPolBL.getRenewCount() > 0) {
            tRnewFlag = 1;
        }
        mCalBase.setRnewFlag(tRnewFlag);

        LMDutySchema tLMDutySchema =
                mCRI.findDutyByDutyCode(dutyCode);

        if (tLMDutySchema == null) {
            // @@错误处理
            this.mErrors.copyAllErrors(mCRI.mErrors);
            mCRI.mErrors.clearErrors();

            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "calOneDuty";
            tError.errorMessage = "LMDuty表查询失败!";
            this.mErrors.addOneError(tError);
            return mLCDutySchema;
        }

        if (mLCDutySchema.getPayEndYearFlag() == null) {
            mLCDutySchema.setPayEndYearFlag(tLMDutySchema.getPayEndYearFlag());
        }
        if (mLCDutySchema.getGetYearFlag() == null) {
            mLCDutySchema.setGetYearFlag(tLMDutySchema.getGetYearFlag());
        }
        if (mLCDutySchema.getInsuYearFlag() == null) {
            mLCDutySchema.setInsuYearFlag(tLMDutySchema.getInsuYearFlag());
        }
        if (mLCDutySchema.getAcciYearFlag() == null) {
            mLCDutySchema.setAcciYearFlag(tLMDutySchema.getAcciYearFlag());
        }

        if (calType == null || calType.equals("")) {
            calMode = tLMDutySchema.getCalMode();
        } else {
            /** 改用缓存查询方式 */
//            LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
//            tLMPolDutyEdorCalDB.setDutyCode(tLMDutySchema.getDutyCode());
//            tLMPolDutyEdorCalDB.setEdorType(calType);
            LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = mCRI.
                    findEdorCalByDutyCode(tLMDutySchema.getDutyCode(), calType);
//            if (tLMPolDutyEdorCalDB.mErrors.needDealError()) {
//                mErrors.copyAllErrors(tLMPolDutyEdorCalDB.mErrors);
//                mErrors.addOneError(new CError("查询险种责任保全计算信息失败！"));
//                return null;
//            }
            if (tLMPolDutyEdorCalSet.size() > 0) {
                calMode = tLMPolDutyEdorCalSet.get(1).getCalMode(); //获取保全重算的计算方式
            } else {
                calMode = tLMDutySchema.getCalMode(); //若不指定保全重算的计算方式，则默认承保的计算方式
            }
        }

        // 保费项结构部分
        if (getPremStructure(mLCDutySchema) == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "calOneDuty";
            tError.errorMessage = "getPremStructure方法返回失败!";
            this.mErrors.addOneError(tError);
            return mLCDutySchema;
        }
        //从保单信息或责任信息中获取保费项表中需要计算的以外的信息
        this.getPremOtherData(mLCDutySchema);

        int n = mLCPremBLSet.size();
        for (int i = 1; i <= n; i++) {
            LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);

            if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                LMDutyPaySchema tLMDutyPaySchema =
                        mCRI.findDutyPayByPayPlanCode(tLCPremSchema.
                        getPayPlanCode());

                if (tLMDutyPaySchema == null) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "CalBL";
                    tError.functionName = "calOneDuty";
                    tError.errorMessage = "LMDutyPay表查询失败!";
                    this.mErrors.addOneError(tError);
                    return mLCDutySchema;
                }

                // 计算保费项中除保费外的其他信息
                tLCPremSchema = this.calPremOther(tLCPremSchema,
                                                  tLMDutyPaySchema,
                                                  mLCDutySchema);
                if (mOperator != null && !mOperator.equals("")) {
                    tLCPremSchema.setOperator(mOperator);
                    tLCPremSchema.setMakeDate(PubFun.getCurrentDate());
                    tLCPremSchema.setMakeTime(PubFun.getCurrentTime());
                }
                mLCPremBLSet.set(i, tLCPremSchema);
            } // end of if
        } // end of for

        // 领取项结构部分
        if (getGetStructure(mLCDutySchema) == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "calOneDuty";
            tError.errorMessage = "getGetStructure方法返回失败!";
            this.mErrors.addOneError(tError);
            return mLCDutySchema;
        }
        this.getGetOtherData();

        int m = mLCGetBLSet.size();
        for (int i = 1; i <= m; i++) {
            LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);

            if (tLCGetSchema.getDutyCode().equals(dutyCode)) {
                LMDutyGetSchema tLMDutyGetSchema =
                        mCRI.findDutyGetByGetDutyCode(tLCGetSchema.
                        getGetDutyCode()).getSchema();

                if (tLMDutyGetSchema == null) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "CalBL";
                    tError.functionName = "calOneDuty";
                    tError.errorMessage = "LMDutyGet表查询失败!";
                    this.mErrors.addOneError(tError);
                    return mLCDutySchema;
                }

                // 取得相对应的传入的领取项的信息
                if (mGetFlag == true) { // 传入的是多条的GetSet
                    int inpGetCount = mLCGetSetIn.size();
                    for (int j = 1; j <= inpGetCount; j++) {
                        LCGetSchema tLCGetSchema1 = mLCGetSetIn.get(j);
                        if (tLCGetSchema1.getGetDutyCode().equals(tLCGetSchema.
                                getGetDutyCode())) {
                            mLCGetBLIn = tLCGetSchema1.getSchema();
                            break;
                        } // end of if
                    } // end of for
                } // end of if

                // 计算领取项中除保费外的其他信息
                tLCGetSchema = this.calGetOther(tLCGetSchema, tLMDutyGetSchema,
                                                mLCDutySchema);
                if (this.mErrors.needDealError()) {
                    return null;
                }
                tLCGetSchema.setOperator(mLCGetBLIn.getOperator());
                tLCGetSchema.setMakeDate(mLCGetBLIn.getMakeDate());
                tLCGetSchema.setMakeTime(mLCGetBLIn.getMakeTime());
                mLCGetBLSet.set(i, tLCGetSchema);
            } // end of if
        } // end of for

        if (!noCalFlag) { //如果需要计算保费,该标记由承保程序传入
            // 计算保费
            int j = mLCPremBLSet.size();
            for (int i = 1; i <= j; i++) {
                LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);

                if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                    LMDutyPaySchema tLMDutyPaySchema =
                            mCRI.findDutyPayByPayPlanCode(tLCPremSchema.
                            getPayPlanCode());

                    if (tLMDutyPaySchema == null) {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "CalBL";
                        tError.functionName = "calOneDuty";
                        tError.errorMessage = "LMDutyPay表查询失败!";
                        this.mErrors.addOneError(tError);
                        return mLCDutySchema;
                    }

                    // 准备计算基础数据部分
                    mCalBase.setPayIntv(tLCPremSchema.getPayIntv());

                    if (mLCGetBLIn.getGetDutyKind() == null) {
                        //此时保全会从后台查询出领取项，可能是多条，循环找出GetDutyKind不为空的一条赋值
                        for (int t = 1; t <= mLCGetBLSet.size(); t++) {
                            if (mLCGetBLSet.get(t).getGetDutyKind() != null) {
                                mCalBase.setGetDutyKind(mLCGetBLSet.get(t).
                                        getGetDutyKind());
                            }
                        }

                    } else {
                        //houzm add(将界面录入的GetDutyKind保存)
                        mCalBase.setGetDutyKind(mLCGetBLIn.getGetDutyKind());
                    }
                    //可以添加从界面传入的要素，该要素只是用于计算


                    // 计算保费值
                    double calResult = 0;
                    calResult = calPremValue(tLCPremSchema, mLCDutySchema,
                                             tLMDutyPaySchema,
                                             calMode);
                    mLCDutySchema.setPremToAmnt(currCalMode);
                    if (StrTool.cTrim(tLMDutyPaySchema.getZeroFlag()).equals(
                            "N")) {
                        if (calResult <= 0.0) {
                            // @@错误处理
                            String strError = getCalError(tLCPremSchema,
                                    tLMDutyPaySchema);
                            CError tError = new CError();
                            tError.moduleName = "CalBL";
                            tError.functionName = "calOneDuty";
                            tError.errorMessage = "保费计算失败" + strError;
                            this.mErrors.addOneError(tError);
                            return mLCDutySchema;
                        } else {
                            tLCPremSchema.setStandPrem(calResult);
                        }
                    }
                    if (StrTool.cTrim(tLMDutyPaySchema.getZeroFlag()).equals(
                            "Y")) {
                        if (calResult < 0.0) {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "CalBL";
                            tError.functionName = "calOneDuty";
                            tError.errorMessage = "保费计算失败";
                            this.mErrors.addOneError(tError);
                            return mLCDutySchema;
                        } else {
                            tLCPremSchema.setStandPrem(calResult);
                        }
                    }
                    tLCPremSchema.setPrem(tLCPremSchema.getStandPrem());
                    mLCPremBLSet.set(i, tLCPremSchema);
                } // end of if
            } // end of for

            // 计算保额
            int k = mLCGetBLSet.size();
            for (int i = 1; i <= k; i++) {
                LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);

                if (tLCGetSchema.getDutyCode().equals(dutyCode)) {
                    LMDutyGetSchema tLMDutyGetSchema = this.mCRI.
                            findDutyGetByGetDutyCode(tLCGetSchema.
                            getGetDutyCode()).getSchema();
                    LMDutyGetDB tLMDutyGetDB = tLMDutyGetSchema.getDB();
//                    tLMDutyGetDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
//                    if (tLMDutyGetDB.getInfo() == false) {
//                        // @@错误处理
//                        CError tError = new CError();
//                        tError.moduleName = "CalBL";
//                        tError.functionName = "calOneDuty";
//                        tError.errorMessage = "LMDutyGet表查询失败!";
//                        this.mErrors.addOneError(tError);
//                        return mLCDutySchema;
//                    }

                    // 准备计算基础数据部分
                    mCalBase.setGetIntv(tLCGetSchema.getGetIntv());
                    mCalBase.setGDuty(tLCGetSchema.getGetDutyCode());
                    mCalBase.setAddRate(tLCGetSchema.getAddRate());
                    mCalBase.setGrpContNo(tLCGetSchema.getGrpContNo());

                    // 计算保额值
                    double calResult = 0;
                    calResult = calGetValue(tLCGetSchema, mLCDutySchema,
                                            tLMDutyGetDB, calMode);
                    //null认为是N
                    if (tLMDutyGetDB.getZeroFlag() == null ||
                        tLMDutyGetDB.getZeroFlag().equals("")) {
                        tLMDutyGetDB.setZeroFlag("N");
                    }
                    if (StrTool.cTrim(tLMDutyGetDB.getZeroFlag()).equals("N")) {
                        if (calResult <= 0.0) {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "CalBL";
                            tError.functionName = "calOneDuty";
                            tError.errorMessage = "保额计算失败!";
                            this.mErrors.addOneError(tError);
                            return mLCDutySchema;
                        } else {
                            tLCGetSchema.setStandMoney(calResult);
                            tLCGetSchema.setActuGet(calResult);
                        }
                    }
                    if (StrTool.cTrim(tLMDutyGetDB.getZeroFlag()).equals("Y")) {
                        if (calResult < 0.0) {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "CalBL";
                            tError.functionName = "calOneDuty";
                            tError.errorMessage = "保额计算失败!";
                            this.mErrors.addOneError(tError);
                            return mLCDutySchema;
                        } else {
                            tLCGetSchema.setStandMoney(calResult);
                            tLCGetSchema.setActuGet(calResult);
                        }
                    }
                    //mLCGetBLSet.set(i, tLCGetSchema);
                } // end of if
            } // end of for
        }

        // 责任总体计算
        mLCDutySchema = this.calDutyFromPrem(mLCDutySchema, tLMDutySchema);
        mLCDutySchema = this.calDutyFromGet(mLCDutySchema, tLMDutySchema);
        if (this.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "calOneDuty";
            tError.errorMessage = "责任总体计算失败!";
            this.mErrors.addOneError(tError);
            return mLCDutySchema;
        }
        if (!dealFloatRate(mLCPremBLSet, mLCGetBLSet, mLCDutySchema)) {
            this.mErrors.copyAllErrors(mCRI.mErrors);
            mCRI.mErrors.clearErrors();

            CError tError = new CError();
            tError.moduleName = "CalBL";
            tError.functionName = "calOneDuty";
            tError.errorMessage = "处理浮动费率失败!";
            this.mErrors.addOneError(tError);
            return mLCDutySchema;

        }

        return mLCDutySchema;
    }

    /**
     * getCalError
     *
     * @param tLCPremSchema LCPremSchema
     * @return String
     */
    private String getCalError(LCPremSchema tLCPremSchema,
                               LMDutyPaySchema tLMDutyPaySchema) {
        System.out.println("开始保费校验报错.......");
        if (tLMDutyPaySchema == null ||
            tLMDutyPaySchema.getPayPlanCode() == null ||
            tLMDutyPaySchema.getOthCalCode() == null) {
            return "";
        }

        String payPlanCode = tLMDutyPaySchema.getPayPlanCode();
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode(payPlanCode);
        LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();
        CErrors tCErrors = new CErrors();

        // 计算
        for (int i = 1; i <= tLMCalModeSet.size(); i++) {
            CError tCError = getError(tLMCalModeSet.get(i));
            if (tCError != null) {
                tCErrors.addOneError(tCError);
            }
        }

        String strError = "";
        if (tCErrors.needDealError()) {
            for (int i = 1; i <= tCErrors.getErrorCount(); i++) {
                strError += "<br>（" + i + "）、";
                strError += tCErrors.getError(i - 1).errorMessage;
                strError += ";";
            }
        }

        return strError;
    }

    /**
     * getError
     *
     * @param tCalCode String
     * @return CError
     */
    private CError getError(LMCalModeSchema tLMCalModeSchema) {

        Calculator mCalculator = new Calculator();

        mCalculator.setCalCode(tLMCalModeSchema.getCalCode());
        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear());
        mCalculator.addBasicFactor("GetYearFlag", mCalBase.getGetYearFlag());
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("InsuYear", mCalBase.getInsuYear());
        mCalculator.addBasicFactor("InsuYearFlag", mCalBase.getInsuYearFlag());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("RnewFlag", mCalBase.getRnewFlag());
        mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("FRate", mCalBase.getFloatRate());
        mCalculator.addBasicFactor("GetDutyKind", mCalBase.getGetDutyKind());
        mCalculator.addBasicFactor("StandbyFlag1", mCalBase.getStandbyFlag1());
        mCalculator.addBasicFactor("StandbyFlag2", mCalBase.getStandbyFlag2());
        mCalculator.addBasicFactor("StandbyFlag3", mCalBase.getStandbyFlag3());
        mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
        mCalculator.addBasicFactor("GetLimit", mCalBase.getGetLimit());
        mCalculator.addBasicFactor("GetRate", mCalBase.getGetRate());
        mCalculator.addBasicFactor("SSFlag", mCalBase.getSSFlag());
        mCalculator.addBasicFactor("PeakLine", mCalBase.getPeakLine());
        mCalculator.addBasicFactor("CalType", mCalBase.getCalType());
        mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
        mCalculator.addBasicFactor("EdorValiDate", mCalBase.getEdorValiDate());
        mCalculator.addBasicFactor("CInValiDate", mCalBase.getCInValiDate());
        mCalculator.addBasicFactor("CalRule", mCalBase.getCalRule());
        mCalculator.addBasicFactor("LastPayToDate", mCalBase.getLastPayToDate());
        mCalculator.addBasicFactor("PayToDate", mCalBase.getPayToDate());
        mCalculator.addBasicFactor("RevertType", mCalBase.getRevertType());
        mCalculator.addBasicFactor("ComCode", mCalBase.getComCode());
        mCalculator.addBasicFactor("RetireRate", mCalBase.getRetireRate());
        mCalculator.addBasicFactor("InsuredState", mCalBase.getInsuredState());
        mCalculator.addBasicFactor("Industry", mCalBase.getIndustry());
        mCalculator.addBasicFactor("InsuredNum", mCalBase.getInsuredNum());
        mCalculator.addBasicFactor("AvgAge", mCalBase.getAvgAge());
        mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());
        mCalculator.addBasicFactor("Copys", mCalBase.getCopys());
        //add by zjd
        mCalculator.addBasicFactor("ContPlanCode", mLCPolBL.getContPlanCode());
        String tStr = "";
        tStr = mCalculator.calculate();

        double mValue = 0;
        if (tStr == null || tStr.trim().equals("")) {
            mValue = 0;
        } else {
            mValue = Double.parseDouble(tStr);
        }
        /** 存在错误 */
        if (mValue > 0) {
            /** 报错信息 */
            CError tCError = new CError();
            tCError.errorMessage = tLMCalModeSchema.getRemark();
            return tCError;
        } else {
            return null;
        }
    }

    /**
     * getComCode
     *
     * @param tComCode String
     * @return String
     */
    private String getComCode(String tComCode) {

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tComCode);
        if(!tLDComDB.getInfo())
        {
            return null;
        }

        int length = 4;

        String tComCitySize = tLDComDB.getComCitySize();
        if(tComCitySize != null && !tComCitySize.equals(""))
        {
            length = Integer.parseInt(tComCitySize);
        }

        if (StrTool.cTrim(tComCode).length() <= 8 &&
            StrTool.cTrim(tComCode).length() >= length) {
            return tComCode.substring(0, length);
        } else if (StrTool.cTrim(tComCode).length() <= length) {
            return PubFun.RCh(tComCode, "0", length);
        }
        return "";
    }

    /**
     * 从责任表中取得保单表中部分信息
     * @param: 无
     * @return: boolean
     **/
    private boolean calPolFromDuty() {

        double sumAmnt = 0;
        double sumRiskAmnt = 0;
        double sumPrem = 0;
        double sumStandPrem = 0;
        int payYears = 0;
        int years = 0;
        Date minPayEndDate = fDate.getDate("3000-01-01");
        Date minGetStartDate = fDate.getDate("3000-01-01");
        Date maxEndDate = fDate.getDate("1900-01-01");
        Date maxAcciEndDate = fDate.getDate("1900-01-01");

        int n = mLCDutyBLSet.size();
        for (int i = 1; i <= n; i++) {
            LCDutySchema mLCDutySchema = mLCDutyBLSet.get(i);

            sumAmnt += mLCDutySchema.getAmnt();
            sumRiskAmnt += mLCDutySchema.getRiskAmnt();
            sumPrem += mLCDutySchema.getPrem();
            sumStandPrem += mLCDutySchema.getStandPrem();

            Date payEndDate = fDate.getDate(mLCDutySchema.getPayEndDate());
            if (payEndDate != null) {
                if (minPayEndDate.after(payEndDate)) {
                    minPayEndDate = payEndDate;
                }
            }

            Date getStartDate = fDate.getDate(mLCDutySchema.getGetStartDate());
            if (getStartDate != null) {
                if (minGetStartDate.after(getStartDate)) {
                    minGetStartDate = getStartDate;
                }
            }

            Date endDate = fDate.getDate(mLCDutySchema.getEndDate());
            if (endDate != null) {
                if (maxEndDate.before(endDate)) {
                    maxEndDate = endDate;
                }
            }

            Date acciEndDate = fDate.getDate(mLCDutySchema.getAcciEndDate());
            if (acciEndDate != null) {
                if (maxAcciEndDate.before(acciEndDate)) {
                    maxAcciEndDate = acciEndDate;
                }
            } else {
                maxAcciEndDate = null;
            }

            payYears = mLCDutySchema.getPayYears();
            //System.out.println("CalBL:calPolFromDuty:payYears:"+payYears);
            years = mLCDutySchema.getYears();
        }

        mLCPolBL.setStandPrem(sumStandPrem);
        mLCPolBL.setPrem(sumPrem);
        mLCPolBL.setAmnt(sumAmnt);
        mLCPolBL.setRiskAmnt(sumRiskAmnt);
        mLCPolBL.setPayEndDate(fDate.getString(minPayEndDate));
        mLCPolBL.setGetStartDate(fDate.getString(minGetStartDate));
        mLCPolBL.setEndDate(fDate.getString(maxEndDate));
        mLCPolBL.setAcciEndDate(fDate.getString(maxAcciEndDate));
        mLCPolBL.setPayYears(payYears);
        mLCPolBL.setYears(years);

        return true;
    }

    /**
     * 计算风险保额和基本保额
     * @param:LCDutySchema LMDutyDB
     * @return: LCDutySchema
     **/
    private LCDutySchema calAmnt(LCDutySchema tLCDutySchema,
                                 LMDutySchema tLMDutySchema) {
        double tAmnt = -1;
        double tRiskAmnt = -1;
        String tBasicCalCode = tLMDutySchema.getBasicCalCode();
        String tRiskCalCode = tLMDutySchema.getRiskCalCode();
        String tStr = "";

        // 计算
        Calculator mCalculator = new Calculator();
        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear());
        mCalculator.addBasicFactor("GetYearFlag", mCalBase.getGetYearFlag());
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("InsuYear", mCalBase.getInsuYear());
        mCalculator.addBasicFactor("InsuYearFlag", mCalBase.getInsuYearFlag());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("RnewFlag", mCalBase.getRnewFlag());
        mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("Amnt", mCalBase.getAmnt()); // 计算出来的保额
        mCalculator.addBasicFactor("FRate", mCalBase.getFloatRate());
        mCalculator.addBasicFactor("StandbyFlag1", mCalBase.getStandbyFlag1());
        mCalculator.addBasicFactor("StandbyFlag2", mCalBase.getStandbyFlag2());
        mCalculator.addBasicFactor("StandbyFlag3", mCalBase.getStandbyFlag3());
        mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
        mCalculator.addBasicFactor("GetLimit", mCalBase.getGetLimit());
        mCalculator.addBasicFactor("GetRate", mCalBase.getGetRate());
        mCalculator.addBasicFactor("SSFlag", mCalBase.getSSFlag());
        mCalculator.addBasicFactor("PeakLine", mCalBase.getPeakLine());
        mCalculator.addBasicFactor("CalType", mCalBase.getCalType());
        mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
        mCalculator.addBasicFactor("EdorNo", mCalBase.getEdorNo());
        mCalculator.addBasicFactor("EdorValiDate", mCalBase.getEdorValiDate());
        mCalculator.addBasicFactor("CInValiDate", mCalBase.getCInValiDate());
        mCalculator.addBasicFactor("LastPayToDate", mCalBase.getLastPayToDate());
        mCalculator.addBasicFactor("PayToDate", mCalBase.getPayToDate());
        mCalculator.addBasicFactor("RevertType", mCalBase.getRevertType());
        mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());
        mCalculator.addBasicFactor("PremAfterCal", String.valueOf(tLCDutySchema.getPrem()));
        //add by zjd
        mCalculator.addBasicFactor("ContPlanCode",mLCPolBL.getContPlanCode());
        this.prepareDutyParam(mCalculator,tLCDutySchema);

        // 计算基本保额
        mCalculator.setCalCode(tBasicCalCode);

        tStr = mCalculator.calculate();
        if (tStr.trim().equals("")) {
            tAmnt = 0;
        } else {
            tAmnt = Double.parseDouble(tStr);
        }

        // 计算风险保额
        mCalculator.setCalCode(tRiskCalCode);

        tStr = mCalculator.calculate();
        if (tStr.trim().equals("")) {
            tRiskAmnt = 0;
        } else {
            tRiskAmnt = Double.parseDouble(tStr);
        }

        tLCDutySchema.setAmnt(tAmnt);
        tLCDutySchema.setRiskAmnt(tRiskAmnt);

        //System.out.println( tAmnt );
        //System.out.println( tRiskAmnt );
        return tLCDutySchema;
    }

    /**
     * 从外部给标志赋值
     * @param parmFlag
     */
    public void setNoCalFalg(boolean parmFlag) {
        noCalFlag = parmFlag;
    }


    /**
     * 判断是否需要处理浮动附率：如果需要，将相关数据变更
     * 只处理页面录入保费，保额而没有录入浮动费率的情况：录入浮动费率的情况已经在险种计算公式描述中完成
     * 在此之前，如果传入浮动费率，计算公式中已经计算过浮动费率
     * @param pLCPolBL         个人保单纪录
     * @param pLCPremBLSet      保费项
     * @param pLCGetBLSet       领取项
     * @param pLCDutyBLSet      责任项
     * @return
     */
    private boolean dealFloatRate(LCPremBLSet pLCPremBLSet,
                                  LCGetBLSet pLCGetBLSet,
                                  LCDutySchema pLCDutyShema) {
        String dutyCode = pLCDutyShema.getDutyCode();
        //如果界面上录入保费和保额：且浮动费率==0(不计算浮动费率,校验录入的保费保额是否和计算得到的数据匹配
        if ((FloatRate == 0) && (InputPrem > 0) && (InputAmnt > 0)) {
            dealFormatModol(pLCPremBLSet, pLCGetBLSet, pLCDutyShema); //转换精度
            if ((InputPrem != pLCDutyShema.getPrem())
                || (InputAmnt != pLCDutyShema.getAmnt())) {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealFloatRate";
                tError.errorMessage = "您录入的保费保额（" + InputPrem + "," + InputAmnt
                                      + "）与计算得到的保费保额（" + pLCDutyShema.getPrem()
                                      + "," + pLCDutyShema.getAmnt() + "）不符合！";
                this.mErrors.addOneError(tError);

                return false;
            }
            return true;
        }

        //如果界面上录入保费和保额：且浮动费率==规定的常量,那么认为该浮动费率需要自动计算得到
        //如果界面上录入保费和保额：且浮动费率<>规定的常量,那么认为浮动费率已经给出，且在前面的保费保额计算中用到。此处不用处理
        //该种险种的计算公式需要描述
        if ((autoCalFloatRateFlag == true) && (InputPrem > 0)
            && (InputAmnt > 0) && pLCDutyShema.getPrem() > 0) {
            String strCalPrem = mDecimalFormat.format(pLCDutyShema.getPrem()); //转换计算后的保费(规定的精度)
            String strCalAmnt = mDecimalFormat.format(pLCDutyShema.getAmnt()); //转换计算后的保额
            double calPrem = Double.parseDouble(strCalPrem);
            double calAmnt = Double.parseDouble(strCalAmnt);

            //如果界面录入的保费保额与计算出来的保费保额相等，则认为浮动费率=1,不做任何处理返回
            if ((calPrem == InputPrem) && (calAmnt == InputAmnt)) {
                return true;
            }

            if (pLCDutyShema.getPrem() == 0) {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealFloatRate";
                tError.errorMessage = "计算得到的保费为0,不能做浮动费率计算!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //浮动费率的计算公式为：P(现)*A(原)/(A(现)*P(原))
            double formatFloatRate = 0;
            FloatRate = (InputPrem * pLCDutyShema.getAmnt()) /
                        (InputAmnt * pLCDutyShema
                         .getPrem());
            formatFloatRate = Double.parseDouble(mFRDecimalFormat.format(
                    FloatRate));

            //更新责任项
            pLCDutyShema.setPrem(pLCDutyShema.getPrem() * FloatRate);
            /**
             * Yamging注掉：因为实在是不明白为什么标准保费再乘以浮动费率
             * 这样使标保成为录入保费。
             * 隐患：如果表定保额与约定保额不同的话，会造成标准保费与浮动费率
             * 严重错误
             */
            System.out.println(
                    "CalBL.dealFloatRate(pLCPremBLSet, pLCGetBLSet, pLCDutyShema)");
            System.out.println("Author: YangMing:\n解决浮动费率问题，将标保存入！");
//            pLCDutyShema.setStandPrem(pLCDutyShema.getStandPrem() * FloatRate);
            pLCDutyShema.setFloatRate(formatFloatRate); //存储转换后的浮动费率精度

            //更新保费项
            for (int j = 1; j <= pLCPremBLSet.size(); j++) {

                LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
                if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                    if (!tLCPremSchema.getPayPlanCode().substring(0,
                            6).equals("000000")) {
//                        tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem() * FloatRate);
                        tLCPremSchema.setPrem(tLCPremSchema.getStandPrem() *
                                              FloatRate);
                        tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem() *
                                1);
                    }
                    pLCPremBLSet.set(j, tLCPremSchema);
                }
            }

//        //更新责任项
//        for (int j = 1; j <= pLCDutyBLSet.size(); j++)
//        {
//            LCDutySchema tLCDutySchema = (LCDutySchema) pLCDutyBLSet.get(j);
//            tLCDutySchema.setStandPrem(tLCDutySchema.getStandPrem() * FloatRate);
//            pLCDutyBLSet.set(j, tLCDutySchema);
//        }

            dealFormatModol(pLCPremBLSet, pLCGetBLSet, pLCDutyShema); //转换精度

            //因为累计和可能存在进位误差（可能会有0.01元的），所以需要调整
            double sumDutyPrem = 0.0;
            for (int j = 1; j <= pLCPremBLSet.size(); j++) {
                LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
                if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                    sumDutyPrem = sumDutyPrem + tLCPremSchema.getStandPrem();
                }
            }

            if (sumDutyPrem != InputPrem) {
                double diffMoney = sumDutyPrem - InputPrem;

//            //调整责任表
//            for (int j = 1; j <= pLCDutyBLSet.size(); j++)
//            {
//                if (pLCDutyBLSet.get(j).getStandPrem() > 0.0)
//                {
//                    pLCDutyBLSet.get(j).setStandPrem(pLCDutyBLSet.get(j)
//                                                                 .getStandPrem()
//                                                     - diffMoney);
//
//                    break;
//                }
//            }

                //调整保费表
                for (int j = 1; j <= pLCPremBLSet.size(); j++) {
                    if (pLCPremBLSet.get(j).getDutyCode().equals(dutyCode)) {
                        if (pLCPremBLSet.get(j).getStandPrem() > 0) {
                            //Yangming:不明白下面这个减法是有什么作用，标准保费在之前已经处理好
                            pLCPremBLSet.get(j).setStandPrem(pLCPremBLSet.get(j)
                                    .getStandPrem()
                                    - 0);
                            break;
                        }
                    }
                }
            }

            return true;
        }

        //如果界面上录入保费.而保额为0：且浮动费率==规定的常量,那么认为录入的保费即为经过计算后得到的保费，
        //此时，需要自动计算得到该浮动费率
        if ((autoCalFloatRateFlag == true) && (InputPrem > 0)
            && (InputAmnt == 0) && pLCDutyShema.getPrem() > 0) {
            String strCalPrem = mDecimalFormat.format(pLCDutyShema.getPrem()); //转换计算后的保费(规定的精度)
            double calPrem = Double.parseDouble(strCalPrem);

            //如果界面录入的保费与计算出来的保费相等，则认为浮动费率=1,不做任何处理返回
            if (calPrem == InputPrem) {
                return true;
            }

            if (pLCDutyShema.getPrem() == 0) {
                CError tError = new CError();
                tError.moduleName = "CalBL";
                tError.functionName = "dealFloatRate";
                tError.errorMessage = "计算得到的保费为0,不能做浮动费率计算!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //浮动费率的计算公式为：P(现)/P(原)
            double formatFloatRate = 0;
            FloatRate = InputPrem / (pLCDutyShema.getPrem());
            formatFloatRate = Double.parseDouble(mFRDecimalFormat.format(
                    FloatRate));

            //更新责任项
            pLCDutyShema.setPrem(InputPrem);
            pLCDutyShema.setStandPrem(calPrem);
            pLCDutyShema.setFloatRate(formatFloatRate); //存储转换后的浮动费率精度

            //更新保费项
            for (int j = 1; j <= pLCPremBLSet.size(); j++) {
                LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
                if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                    if (!tLCPremSchema.getPayPlanCode().substring(0,
                            6).equals("000000")) {
                        tLCPremSchema.setPrem(PubFun.setPrecision(tLCPremSchema.
                                getStandPrem() * FloatRate, "0.00"));
                    }
                    pLCPremBLSet.set(j, tLCPremSchema);
                }
            }

//        //更新责任项
//        for (int j = 1; j <= pLCDutyBLSet.size(); j++)
//        {
//            LCDutySchema tLCDutySchema = (LCDutySchema) pLCDutyBLSet.get(j);
//            tLCDutySchema.setStandPrem(tLCDutySchema.getStandPrem() * FloatRate);
//            pLCDutyBLSet.set(j, tLCDutySchema);
//
//            if ((pLCDutyBLSet.get(j).getStandPrem() == 0)
//                    && (pLCPremBLSet.get(j).getStandPrem() != 0))
//            {
//                pLCPremBLSet.get(j).setStandPrem(0);
//            }
//        }

            dealFormatModol(pLCPremBLSet, pLCGetBLSet, pLCDutyShema); //转换精度

            //因为累计和可能存在进位误差（可能会有0.01元的），所以需要调整
            double sumDutyPrem = 0.0;
            for (int j = 1; j <= pLCPremBLSet.size(); j++) {
                LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
                if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                    sumDutyPrem = sumDutyPrem + tLCPremSchema.getStandPrem();
                }
            }

            if (sumDutyPrem != InputPrem) {
                double diffMoney = sumDutyPrem - InputPrem;

//            //调整责任表
//            for (int j = 1; j <= pLCDutyBLSet.size(); j++)
//            {
//                if (pLCDutyBLSet.get(j).getStandPrem() > 0.0)
//                {
//                    pLCDutyBLSet.get(j).setStandPrem(pLCDutyBLSet.get(j)
//                                                                 .getStandPrem()
//                                                     - diffMoney);
//
//                    break;
//                }
//            }

                //调整保费表
                for (int j = 1; j <= pLCPremBLSet.size(); j++) {
                    if (pLCPremBLSet.get(j).getDutyCode().equals(dutyCode)) {
                        if (pLCPremBLSet.get(j).getStandPrem() > 0) {
                            pLCPremBLSet.get(j).setStandPrem(pLCPremBLSet.get(j)
                                    .getStandPrem()
                                    - 0);

                            break;
                        }
                    }
                }
            }

            return true;
        }
        /**
         * 处理表定费率折扣,
         * 计算方向:2
         * @param <any> pLCPremBLSet
         */
        if (pLCDutyShema.getCalRule().equals("2")) {
            for (int j = 1; j <= pLCPremBLSet.size(); j++) {
                pLCPremBLSet.get(j).setPrem(Math.round(pLCPremBLSet.get(j).getStandPrem()*this.FloatRate));
            System.out.println("pLCPremBLSet.get(j).getStandPrem()"+pLCPremBLSet.get(j).getStandPrem());
            System.out.println("this.FloatRate"+this.FloatRate);
        }
            pLCDutyShema.setPrem(Math.round(pLCDutyShema.getStandPrem()*this.FloatRate));
        }
        /**
         * 处理计算保费为0的情况
         */
        if ((autoCalFloatRateFlag == true) && (InputPrem > 0)
            && (InputAmnt > 0) && pLCDutyShema.getPrem() == 0) {
            for (int j = 1; j <= pLCPremBLSet.size(); j++) {
                LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
                if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                    if (!tLCPremSchema.getPayPlanCode().substring(0,
                            6).equals("000000")) {
                        //                        tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem() * FloatRate);
                        tLCPremSchema.setPrem(InputPrem);
                        tLCPremSchema.setStandPrem(0);
                    }
                    pLCPremBLSet.set(j, tLCPremSchema);
                }
            }
            //
            pLCDutyShema.setPrem(InputPrem);
            pLCDutyShema.setStandPrem(0);
        }
        if ((autoCalFloatRateFlag == true) && (InputPrem > 0)
            && (InputAmnt == 0) && pLCDutyShema.getPrem() == 0) {
            for (int j = 1; j <= pLCPremBLSet.size(); j++) {
                LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
                if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                    if (!tLCPremSchema.getPayPlanCode().substring(0,
                            6).equals("000000")) {
                        //                        tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem() * FloatRate);
                        tLCPremSchema.setPrem(InputPrem);
                        tLCPremSchema.setStandPrem(0);
                    }
                    pLCPremBLSet.set(j, tLCPremSchema);
                }
            }
            //
            pLCDutyShema.setPrem(InputPrem);
            pLCDutyShema.setStandPrem(0);
        }

        //否则，直接转换精度
        dealFormatModol(pLCPremBLSet, pLCGetBLSet, pLCDutyShema);

        return true;
    }


    /**
     * 处理传入的数据，转换合适的精度
     * @param pLCPolBL
     * @param pLCPremBLSet
     * @param pLCGetBLSet
     * @param pLCDutyBLSet
     * @return
     */
    private boolean dealFormatModol(LCPremBLSet pLCPremBLSet,
                                    LCGetBLSet pLCGetBLSet,
                                    LCDutySchema pLCDutyShema) {
        String dutyCode = pLCDutyShema.getDutyCode();
        String strCalPrem = mDecimalFormat.format(pLCDutyShema.getPrem()); //转换计算后的保费(规定的精度)
        String strCalAmnt = mDecimalFormat.format(pLCDutyShema.getAmnt()); //转换计算后的保额
        String strStandPrem = mDecimalFormat.format(pLCDutyShema.getStandPrem());
        double calPrem = Double.parseDouble(strCalPrem);
        double calAmnt = Double.parseDouble(strCalAmnt);
        double calStandPrem = Double.parseDouble(strStandPrem);

        /**
         * author:Yangming
         * 实在不明白上任程序员为什么要把标准保费字段存约定保费
         * 目前修改为存储实际计算的保费。
         */
        //更新个人保单
        pLCDutyShema.setPrem(calPrem);
        pLCDutyShema.setStandPrem(calStandPrem);
        pLCDutyShema.setAmnt(calAmnt);

        //更新保费项
        double tempPrem = 0;
        double tempInputPrem = 0;
        /**
         * 原来的处理保费项时存在隐患,从如果一个责任对应多个保费项
         * 而每个保费项的保费不同,在这个情况下不应把责任的保费存入
         * 保费项.因此增加:
         * tempInputPrem
         * @author : YangMing
         */

        //double tempAmnt=0;
        for (int j = 1; j <= pLCPremBLSet.size(); j++) {
            LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
            if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
                tempPrem = Double.parseDouble(mDecimalFormat.format(
                        tLCPremSchema
                        .getStandPrem()));
                tempInputPrem = Double.parseDouble(mDecimalFormat.format(
                        tLCPremSchema
                        .getPrem()));
                tLCPremSchema.setPrem(tempInputPrem);
                tLCPremSchema.setStandPrem(tempPrem);
                pLCPremBLSet.set(j, tLCPremSchema);
            }
        }

        //更新责任项

        tempPrem = Double.parseDouble(mDecimalFormat.format(pLCDutyShema
                .getStandPrem()));
        pLCDutyShema.setStandPrem(tempPrem);

        return true;
    }

    public void setOperator(String tOperator) {
        mOperator = tOperator;
    }

    public void setRiskWrapCode(String tRiskWrapCode) {
        mRiskWrapCode = tRiskWrapCode;
    }
    
    /**
     * 支持健享全球团体医疗保险，增加算费要素
     * 处理LCContPlanDutyParam中保障计划的要素值放到Calculator对象中，如果Calculator中已有这个要素，则不放
     * @param aCalculator
     * @return boolean
     */
    private boolean prepareDutyParam(Calculator aCalculator, LCDutySchema aLCDutySchema)
    {
    	String tGrpContNo = mLCPolBL.getGrpContNo();
    	String tContPlanCode = mLCPolBL.getContPlanCode();
    	String tDutyCode = aLCDutySchema.getDutyCode();
    	String tRiskCode = mLCPolBL.getRiskCode();
    	if(isNull(tGrpContNo) || isNull(tContPlanCode) || isNull(tDutyCode) || isNull(tRiskCode))
    	{
    		return true;
    	}
    	LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
    	tLCContPlanDutyParamDB.setGrpContNo(tGrpContNo);
    	tLCContPlanDutyParamDB.setContPlanCode(tContPlanCode);
    	tLCContPlanDutyParamDB.setDutyCode(tDutyCode);
    	tLCContPlanDutyParamDB.setRiskCode(tRiskCode);
    	LCContPlanDutyParamSet tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.query();
    	if(tLCContPlanDutyParamSet==null || tLCContPlanDutyParamSet.size()==0)
    	{
    		return true;
    	}
    	
    	LMCalFactorSet tLMCalFactorSet = new LMCalFactorSet();
    	tLMCalFactorSet = aCalculator.getCalFactors1();
    	
    	for(int i=1;i<=tLCContPlanDutyParamSet.size();i++)
    	{
    		boolean flag = false; //aCalculator 不存在 LCContPlanDutyParam
    		LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
    		tLCContPlanDutyParamSchema.setSchema(tLCContPlanDutyParamSet.get(i).getSchema());
    		for(int j=1; j<=tLMCalFactorSet.size(); j++)
    		{
    			LMCalFactorSchema tLMCalFactorSchema = new LMCalFactorSchema();
    			tLMCalFactorSchema.setSchema(tLMCalFactorSet.get(j).getSchema());
    			String tFactorCode = tLMCalFactorSchema.getFactorCode();
    			if(tFactorCode.equals(tLCContPlanDutyParamSchema.getCalFactor()))
    			{
    				flag = true;
    			}
    		}
    		if(!flag)
			{
    			aCalculator.addBasicFactor(tLCContPlanDutyParamSchema.getCalFactor(), tLCContPlanDutyParamSchema.getCalFactorValue());
			}
    	}
    	return true;
    }
    
    private boolean isNull(String s)
    {
    	if(s==null || s.equals("") || s.equals("null"))
    	{
    		return true;
    	}
    	return false;
    }
}
