package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 无名单录入 </p>
 * <p>Description:磁盘投保导入无名单 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: SinoSoft</p>
 * @author 张星
 * @version 1.0
 */

public class LGrpNoNameContBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    /** 业务数据操作字符串 */
    private String mGrpContNo;

    private String CurrentData;

    private String mInsuredID = "";
    
    //by gzh 增加印刷号
    private String prtno = "";

    int m = 0;

    /** 被保险人清单列表 */
    private LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();

    /** 公共帐户 */
    private String polType = "";

    ExeSQL tExeSQL = new ExeSQL();

    public LGrpNoNameContBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        //校验是否有未打印的体检通知书
        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start NoNameContBL Submit...");

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "NoNameContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        //对无名单进行算费
        TransferData tTransferData = new TransferData();
		ParseGuideInUI tParseGuideInUI = new ParseGuideInUI();
		String tSQL = "select  BatchNo from LCInsuredList where GrpContNo='"+mGrpContNo+"'  and insuredid not in ('C','G') order by BatchNo";
		String mBatchNo = new ExeSQL().getOneValue(tSQL);
		tTransferData.setNameAndValue("FileName", mBatchNo);
		tTransferData.setNameAndValue("GrpContNo", mGrpContNo);
		// tTransferData.setNameAndValue("FilePath", ImportPath);
		VData tVData = new VData();

		tVData.add(tTransferData);
		tVData.add(mGlobalInput);

		try {
			if(!tParseGuideInUI.submitData(tVData, "INSERT||DATABASE")){
//				 @@错误处理
	            this.mErrors.copyAllErrors(tParseGuideInUI.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "LGrpNoNameContBL";
	            tError.functionName = "submitData";
	            tError.errorMessage = "数据提交失败!";
	            this.mErrors.addOneError(tError);
	            return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

        return true;
    }

    /**
	 * 校验业务数据
	 * 
	 * @return
	 */
    private boolean checkData()
    {
        // 校验保单信息
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LGrpNoNameContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询团体保单表失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        prtno = tLCGrpContDB.getPrtNo();
        // 无名单控制
        if (!chkNoName(tLCGrpContDB.getPrtNo()))
        {
            buildError("checkData", "该单不符合无名单录入规范，请核实。");
            return false;
        }
        // --------------------

        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setGrpContNo(mGrpContNo);
        //        LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
        mLCInsuredListSet = tLCInsuredListDB.query();
        //        if (tLCInsuredListSet.size() > 0) {
        //            CError tError = new CError();
        //            tError.moduleName = "LGrpNoNameContBL";
        //            tError.functionName = "getInputData";
        //            tError.errorMessage = "该保单已经录入过被保险人或者无名单,不允许再次录入!";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
            if (!mLCInsuredListSet.get(i).getInsuredID().equals("C")
                    && !mLCInsuredListSet.get(i).getInsuredID().equals("D")
                    && !mLCInsuredListSet.get(i).getInsuredID().equals("G"))
            {
                buildError("checkData", "该保单已经录入过被保险人或者无名单,不允许再次录入!");
                return false;
            }
            else
            {
                this.polType = "2";
            }
        }
        /**
         * 校验无名单的保费计算方式必须是否为约定费率
         */

        String tContPlan = "select calfactorvalue from lccontplandutyparam where calfactor = 'CalRule' and grpcontno = '"
                + mGrpContNo + "' and contplancode not in ('11','00')  ";
        SSRS aSSRS = new SSRS();
        aSSRS = tExeSQL.execSQL(tContPlan);
        if (aSSRS.MaxRow <= 0)
        {
            CError tError = new CError();
            tError.moduleName = "LGrpNoNameContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询团体保障计划失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                //                if (aSSRS.equals(null) || !aSSRS.GetText(a, 1).equals("3")) {
                //                    CError tError = new CError();
                //                    tError.moduleName = "LGrpNoNameContBL";
                //                    tError.functionName = "getInputData";
                //                    tError.errorMessage = "无名单的保费计算方式必须为约定费率!";
                //                    this.mErrors.addOneError(tError);
                //                    return false;
                //                }
            }
        }
        /**
         *  校验在保障计划录入时是否录入了总保费
         */

        String tExist = (new ExeSQL())
                .getOneValue("select distinct 1 from lccontplanrisk where grpcontno='"
                        + mGrpContNo
                        + "' and contplancode not in ('11','00') and riskprem = 0 and RiskWrapCode != '900101'");
        if (tExist.equals("1") && !StrTool.cTrim(this.polType).equals("2"))
        {
            CError tError = new CError();
            tError.moduleName = "LGrpNoNameContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "无名单在保障计划定制时必须录入总保费!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        String tPlanSql = " select contplancode,peoples3 from lccontplan where grpcontno = '"
                + mGrpContNo + "' " + " and contplancode not in ('11','00')";
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(tPlanSql);
        if (tSSRS.MaxRow <= 0)
        {
            CError tError = new CError();
            tError.moduleName = "LGrpNoNameContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询团体保障计划失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {

            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {

                //保障计划编码
                LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
                tLCInsuredListSchema.setContPlanCode(tSSRS.GetText(i, 1));
                tLCInsuredListSchema.setGrpContNo(mGrpContNo);
                //mInsuredID 值
                String tSql = " select max(Integer(InsuredID))+1 from lcinsuredlist where grpcontno = '"
                        + mGrpContNo + "'" + " and insuredid not in ('C','D')";
                mInsuredID = tExeSQL.getOneValue(tSql);

                if (mInsuredID.equals(null) || mInsuredID.equals(""))
                {
                    m++;
                    mInsuredID = Integer.toString(m);
                    tLCInsuredListSchema.setInsuredID(mInsuredID);
                }
                else
                {
                    tLCInsuredListSchema.setInsuredID(mInsuredID);
                }

                tLCInsuredListSchema.setInsuredID(mInsuredID);
                tLCInsuredListSchema.setState("0");
                tLCInsuredListSchema.setContNo(String.valueOf(i));
                tLCInsuredListSchema.setEmployeeName("无名单");
                tLCInsuredListSchema.setInsuredName("无名单");
                //增加list表中batchno为印刷号 by gzh
                tLCInsuredListSchema.setBatchNo(prtno);
                //---by gzh end
                tLCInsuredListSchema.setRelation("00");
                tLCInsuredListSchema.setIDType("4");
                //无名单职业类别统一为一类
                tLCInsuredListSchema.setOccupationType("0");
                //无名单统一认为被保险人年龄为35
                CurrentData = PubFun.getCurrentDate();
                tLCInsuredListSchema.setBirthday(PubFun.calDate(CurrentData,
                        -35, "Y", null));
                tLCInsuredListSchema.setPublicAccType("1");
                //无名单人数
                tLCInsuredListSchema.setNoNamePeoples(tSSRS.GetText(i, 2));
                //无名单性别（统一认为"0"）
                tLCInsuredListSchema.setSex("0");
                tLCInsuredListSchema.setMakeDate(CurrentData);
                tLCInsuredListSchema.setMakeTime(PubFun.getCurrentTime());
                tLCInsuredListSchema.setModifyDate(CurrentData);
                tLCInsuredListSchema.setModifyTime(PubFun.getCurrentTime());
                tLCInsuredListSchema.setOperator(mGlobalInput.Operator);
                mLCInsuredListSet.add(tLCInsuredListSchema);
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LGrpNoNameContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        if (mGrpContNo == null)
        {
            CError tError = new CError();
            tError.moduleName = "LGrpNoNameContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输团单保单号码失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 工作流准备后台提交数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();

        //添加体检项目数据
        map.put(mLCInsuredListSet, "DELETE&INSERT");

        mResult.add(map);
        return true;
    }

    /**
     * 校验广东无名单规则
     * @param cPrtNo
     * @return
     */
    private boolean chkNoName(String cPrtNo)
    {
        String tStrSql = " select 1  "
                + " from LCGrpCont lgc "
                + " where 1 = 1 and lgc.CardFlag is null "
                + " and lgc.PrtNo = '"
                + cPrtNo
                + "' "
                + " and lgc.ManageCom like '8644%' "
                + " and (lgc.MarketType is null or lgc.MarketType in ('1', '99')) "
                + " and not exists " + " (" + " select 1 "
                + " from LCGrpPol lgp "
                + " inner join LMRiskApp lmrp on lmrp.RiskCode = lgp.RiskCode "
                + " where 1 = 1 " + " and lgp.PrtNo = '" + cPrtNo + "' "
                + " and lmrp.RiskType8 = '4' " + " )";
        String tResult = new ExeSQL().getOneValue(tStrSql);

        if ("1".equals(tResult))
        {
            System.out.println(cPrtNo + "：广东分公司无名单控制。");
            return false;
        }

        return true;
    }

    /**
     * 返回处理后的结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 返回工作流中的Lwfieldmap所描述的值
     * @return TransferData
     */
    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    /**
     * 返回错误对象
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {
        //SysUWNoticeBL sysUWNoticeBL1 = new SysUWNoticeBL();
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LGrpNoNameContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
