package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 无名单录入 </p>
 * <p>Description:磁盘投保导入无名单 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: SinoSoft</p>
 * @author 张星
 * @version 1.0
 */

public class LGrpCheckInsuredBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    /** 业务数据操作字符串 */
    private String mGrpContNo;

    int m = 0;

    ExeSQL tExeSQL = new ExeSQL();

    public LGrpCheckInsuredBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        //校验是否有未打印的体检通知书
        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
	 * 校验业务数据
	 * 
	 * @return
	 */
    private boolean checkData()
    {
        // 校验保单信息
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LGrpCheckInsuredBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询团体保单表失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	RSWrapper rswrapper = new RSWrapper();
        String tSql = "select * from LCGrpSubInsuredImport where grpcontno = '"+mGrpContNo+"' and DataChkFlag = '00' ";
    	LCGrpSubInsuredImportSet tLCGrpSubInsuredImportSet = new LCGrpSubInsuredImportSet();
        rswrapper.prepareData(tLCGrpSubInsuredImportSet, tSql);
        try
        {
            do
            {
                rswrapper.getData();
                for(int i=1;i<=tLCGrpSubInsuredImportSet.size();i++){
                	if("".equals(StrTool.cTrim(tLCGrpSubInsuredImportSet.get(i).getName()))){
                		tLCGrpSubInsuredImportSet.get(i).setDataChkFlag("02");
                		tLCGrpSubInsuredImportSet.get(i).setChkRemark("被保人姓名为空！");
                		continue;
                	}
                	if("".equals(StrTool.cTrim(tLCGrpSubInsuredImportSet.get(i).getSex()))){
                		tLCGrpSubInsuredImportSet.get(i).setDataChkFlag("02");
                		tLCGrpSubInsuredImportSet.get(i).setChkRemark("被保人性别为空！");
                		continue;
                	}
                	if("".equals(StrTool.cTrim(tLCGrpSubInsuredImportSet.get(i).getBirthday()))){
                		tLCGrpSubInsuredImportSet.get(i).setDataChkFlag("02");
                		tLCGrpSubInsuredImportSet.get(i).setChkRemark("被保人出生日期为空！");
                		continue;
                	}
                	if("".equals(StrTool.cTrim(tLCGrpSubInsuredImportSet.get(i).getIDType()))){
                		tLCGrpSubInsuredImportSet.get(i).setDataChkFlag("02");
                		tLCGrpSubInsuredImportSet.get(i).setChkRemark("被保人证件类型为空！");
                		continue;
                	}
                	if("".equals(StrTool.cTrim(tLCGrpSubInsuredImportSet.get(i).getIDNo()))){
                		tLCGrpSubInsuredImportSet.get(i).setDataChkFlag("02");
                		tLCGrpSubInsuredImportSet.get(i).setChkRemark("被保人证件号码为空！");
                		continue;
                	}
                	tLCGrpSubInsuredImportSet.get(i).setDataChkFlag("01");
            		tLCGrpSubInsuredImportSet.get(i).setChkRemark("校验通过！");
                	
        		}
//        		保存数据
                saveData(tLCGrpSubInsuredImportSet);
                
            }
	        while (tLCGrpSubInsuredImportSet.size() > 0);
	        rswrapper.close();
        }catch (Exception ex)
        {
            ex.printStackTrace();
            rswrapper.close();
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LGrpNoNameContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        if (mGrpContNo == null)
        {
            CError tError = new CError();
            tError.moduleName = "LGrpNoNameContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输团单保单号码失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 工作流准备后台提交数据
     * @return boolean
     */
    private boolean saveData(LCGrpSubInsuredImportSet aLCGrpSubInsuredImportSet)
    {
        mResult.clear();
        MMap map = new MMap();

        //添加体检项目数据
        map.put(aLCGrpSubInsuredImportSet, SysConst.UPDATE);

        mResult.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LGrpCheckInsuredBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回处理后的结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 返回工作流中的Lwfieldmap所描述的值
     * @return TransferData
     */
    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    /**
     * 返回错误对象
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {
        //SysUWNoticeBL sysUWNoticeBL1 = new SysUWNoticeBL();
    }

}
