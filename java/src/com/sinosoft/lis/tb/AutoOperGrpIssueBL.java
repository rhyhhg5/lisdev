package com.sinosoft.lis.tb;

import java.util.Vector;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 系统自动发放团单问题件 </p>
 * <p>Description: 新契约团单录入后相关模块使用的自动发放团单问题件业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft</p>
 * @author 闫少杰
 * @version 1.0
 * @CreateDate：2006-02-21
 * @Content:
 *          1 根据传入的团单合同实体进行系统自动发放团单问题件处理，然后返回团单问题件集合；
 */
public class AutoOperGrpIssueBL
{
    /** 从前面传输数据的容器—从调用者获取的业务数据—存放复杂数据 */
    private VData mInputData;
    /* 从前面传输数据的容器—从调用者获取的业务数据—存放简单数据 */
    private TransferData mTransferData = new TransferData();
    /** 全局变量—操作员的登陆信息 */
    private GlobalInput tGI = new GlobalInput();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    private CErrors mErrors = new CErrors();
    /** 往前面传输数据的容器—返回给调用者的处理结果 */
    private VData mResult = new VData();
    /* 团单合同实体 */
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    /* 团单问题件实体集合 */
    private LCGrpIssuePolSet mLCGrpIssuePolSet = new LCGrpIssuePolSet();
    /* 系统自动发放团单问题件算法计算处理类实例 */
    private Calculator mCalculator;

    /** 当前公用业务数据属性 */
    private String mGrpContNo;   //当前操作的团体投保单合同号码
    private String mManageCom;   //当前操作员的登陆机构
    private String mOperator;    //当前操作员的登陆用户

    public AutoOperGrpIssueBL()
    {
    }

    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //获取调用者传入数据
        if (!getInputData(cInputData))
        {
            return false;
        }

        //检验传入数据
        if(!CheckData())
        {
            return false;
        }

        //进行业务逻辑处理
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 业务处理
     * @return boolean
     */
    public boolean dealData()
    {
        //初始化Calculator对象
        initCalculator();

        //系统自动发放团单问题件
        if( !dealSysAutoIssue() )
        {
            return false;
        }

        return true;
    }

    /**
     * 传递参数
     * @param cInputData VData
     * @return boolean
     */
    public boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData START ----------");
        mInputData = (VData) cInputData.clone();
        tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        //mLCGrpContSchema = (LCGrpContSchema) mInputData.getObjectByObjectName("LCGrpContSchema",0);

        //获取当前操作员信息
        if (tGI == null)
        {
            buildError("getInputData", "获取当前操作员登陆信息失败！");
            return false;
        }
        //获取当前操作员登陆机构
        mManageCom = tGI.ComCode;
        if(mManageCom==null || mManageCom.equals(""))
        {
            buildError("getInputData", "获取当前操作员登陆机构失败！");
            return false;
        }
        //获取当前操作员登陆用户名
        mOperator = tGI.Operator;
        if (mOperator == null || mOperator.equals(""))
        {
            buildError("getInputData", "获取当前操作员登陆用户名失败!");
            return false;
        }

        //获取团单投保单号码
        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        if (mGrpContNo == null || mGrpContNo.equals(""))
        {
            buildError("getInputData", "获得团体投保单号码失败！");
            return false;
        }

        System.out.println("getInputData END ----------");
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean CheckData()
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if(!tLCGrpContDB.getInfo())
        {
            CError.buildErr(this, "查询团体合同投保单信息失败！");
            return false;
        }
        else
        {
            mLCGrpContSchema = tLCGrpContDB.getSchema();
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean dealSysAutoIssue()
    {
        System.out.println("dealSysAutoIssue START ----------");
        //查询当前团单问题件规则定义内容
        String strQuerySQL = "select * from LDIssue where IssueType='2' order by IssueCode, IssueRuleCode";
        System.out.println("查询团单问题件定义内容的SQL = "+strQuerySQL);
        LDIssueDB tLDIssueDB = new LDIssueDB();
        LDIssueSet tLDIssueSet = tLDIssueDB.executeQuery(strQuerySQL);

        if (tLDIssueDB.mErrors.needDealError())
        {
            CError.buildErr(this, "团单问题件定义表查询失败！");
            return false;
        }
        if (tLDIssueSet == null || tLDIssueSet.size() < 1)
        {
            CError.buildErr(this, "没有查询到团单问题件定义内容数据！");
            return false;
        }

        //根据团单问题件编码和问题件规则编码去算法表中执行处理
        LDIssueSchema tLDIssueSchema;
        LCGrpIssuePolSchema tLCGrpIssuePolSchema;
        String tSerialNo = "";        //团单问题件业务流水号
        String tCalCode = "";         //团单问题件规则算法代码
        String strErrCount = "";      //算法执行后的结果，'0'为不需要处理，不为'0'且不是''则表示需要处理。
        for (int i = 1; i <= tLDIssueSet.size(); i++)
        {
            //根据循环出来的算法代码--calcode去对应算法
            tLDIssueSchema  = tLDIssueSet.get(i).getSchema();
            //系统自动发放问题件的算法编码规则为：问题件代码+问题件规则代码， 比如 '101A0'='101'+'A0'
            tCalCode = tLDIssueSchema.getIssueCode() + tLDIssueSchema.getIssueRuleCode();  //算法编码

            strErrCount = getCalValue(tCalCode);

            //算法描述中执行结果不为0表示具有需要系统自动发放团单问题件
            if(!strErrCount.equals("0") && !strErrCount.equals(""))
            {
                tLCGrpIssuePolSchema = new LCGrpIssuePolSchema();

                //开始设置团单问题件业务实体数据
                tSerialNo = PubFun1.CreateMaxNo("QustSerlNo", 20);
                tLCGrpIssuePolSchema.setGrpContNo(mGrpContNo);
                tLCGrpIssuePolSchema.setProposalGrpContNo(mLCGrpContSchema.getProposalGrpContNo());
                tLCGrpIssuePolSchema.setSerialNo(tSerialNo);
                tLCGrpIssuePolSchema.setFieldName(tLDIssueSchema.getIssueName());
                tLCGrpIssuePolSchema.setIssueType("2");
                tLCGrpIssuePolSchema.setOperatePos("5");         //操作位置设定为新单复核
                tLCGrpIssuePolSchema.setBackObjType(tLDIssueSchema.getBackObjType());
                tLCGrpIssuePolSchema.setBackObj(tLDIssueSchema.getBackObj());
                tLCGrpIssuePolSchema.setIsueManageCom(mLCGrpContSchema.getManageCom());
                tLCGrpIssuePolSchema.setIssueCont(tLDIssueSchema.getIssueCont());
                tLCGrpIssuePolSchema.setPrintCount(0);           //团单问题件不需要打印，所以打印次数为0
                tLCGrpIssuePolSchema.setNeedPrint("N");          //团单问题件不需要打印
                tLCGrpIssuePolSchema.setReplyMan("");
                tLCGrpIssuePolSchema.setReplyResult("");
                tLCGrpIssuePolSchema.setState("1");
                tLCGrpIssuePolSchema.setOperator(mOperator);
                tLCGrpIssuePolSchema.setManageCom(mManageCom);
                tLCGrpIssuePolSchema.setMakeDate(PubFun.getCurrentDate());
                tLCGrpIssuePolSchema.setMakeTime(PubFun.getCurrentTime());
                tLCGrpIssuePolSchema.setModifyDate(PubFun.getCurrentDate());
                tLCGrpIssuePolSchema.setModifyTime(PubFun.getCurrentTime());
                tLCGrpIssuePolSchema.setQuestionObj(tLDIssueSchema.getIssueRuleCode());  //目前该字段存的是错误类型
                //QuestionObj应该是对应问题域的名称：如投保单的团体基本资料、参保人员资料、缴费资料、保险责任、声明部分；被保险人清单；其它
                tLCGrpIssuePolSchema.setErrField(tLDIssueSchema.getIssueCode());    //错误字段
                //ErrField应该是对应的字段在数据库中
                tLCGrpIssuePolSchema.setErrFieldName(tLDIssueSchema.getIssueName());
                //tLCGrpIssuePolSchema.setErrContent();         //原填写内容

                mLCGrpIssuePolSet.add(tLCGrpIssuePolSchema);
            }
        }

        //如果系统自动发放了团单问题件，则将
        if(mLCGrpIssuePolSet.size() > 0)
        {
            mResult.add(mLCGrpIssuePolSet);
        }
        System.out.println("dealSysAutoIssue END ----------");
        return true;
    }

    /**
     * 返回算法代码对应的算法执行后的值
     * @param calCode String
     * @return String
     */
    private String getCalValue(String calCode)
    {
        String tCalValue = "";

        mCalculator.setCalCode(calCode);

        System.out.println("CalSQL=" + mCalculator.getCalSQL());

        //开始执行算法
        tCalValue = mCalculator.calculate().trim();
        //如果没有返回值，则设置为0
        if(tCalValue.equals(""))
        {
            tCalValue="0";
        }

        return tCalValue;
    }

    /**
     * initCalculator：初始化一个Calculator全局对象
     */
    private void initCalculator()
    {
        Calculator tCalculator = new Calculator();
        Vector tv = mTransferData.getValueNames();
        for (int i = 0; i < tv.size(); i++)
        {
            String tName = (String) tv.get(i);
            String tValue = (String) mTransferData.getValueByName(tName);
            System.out.println("传入参数名字" + i + ":" + tName + " *** 传入参数值" + i + ":"  + tValue);
            tCalculator.addBasicFactor(tName, tValue);
        }
        mCalculator = tCalculator;
    }

    /**
     * buildError：错误构建方法
     * @param strFunc String
     * @param strErrMsg String
     */
    private void buildError(String strFunc, String strErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "AutoOperGrpIssueBL";
        cError.functionName = strFunc;
        cError.errorMessage = strErrMsg;
        mErrors.addOneError(cError);
    }

    /**
     * getErrors：获取错误信息
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * getResult：获取处理结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 程序测试入口
     * @param arg String[]
     */
    public static void main(String arg[])
    {
        AutoOperGrpIssueBL objAutoOperGrpIssueBL = new AutoOperGrpIssueBL();
        VData objInputData = new VData();
        TransferData objTransferData = new TransferData();

        GlobalInput objGI = new GlobalInput();

        objGI.Operator = "group";
        objGI.ComCode = "86";

        //计算要素
        objTransferData.setNameAndValue("GrpContNo","1400002422");

        objInputData.add(objGI);
        objInputData.add(objTransferData);
        try
        {
            objAutoOperGrpIssueBL.submitData(objInputData,"");

            VData objResult = new VData();
            CErrors colErrors = new CErrors();
            CError objError = new CError();
            colErrors = objAutoOperGrpIssueBL.getErrors();
            if(colErrors.needDealError())
            {
                for(int indexOfErrors=1; indexOfErrors<=colErrors.getErrorCount(); indexOfErrors++)
                {
                    objError = colErrors.getError(indexOfErrors-1);
                    System.out.println("错误信息"+indexOfErrors+"："+objError.errorMessage);
                }
            }
            else
            {
                objResult = objAutoOperGrpIssueBL.getResult();
                LCGrpIssuePolSet objLCGrpIssuePolSet = new LCGrpIssuePolSet();
                objLCGrpIssuePolSet = (LCGrpIssuePolSet)objResult.getObjectByObjectName("LCGrpIssuePolSet",0);
                if(objLCGrpIssuePolSet.size()>0)
                {
                    System.out.println("系统自动发放了团单问题件，具体内容是:");
                    for(int indexOfGrpIssuePolSet=1; indexOfGrpIssuePolSet<=objLCGrpIssuePolSet.size(); indexOfGrpIssuePolSet++)
                    {
                        System.out.println(indexOfGrpIssuePolSet+"：团体投保单号码"+objLCGrpIssuePolSet.get(indexOfGrpIssuePolSet).getGrpContNo()+"存在"+objLCGrpIssuePolSet.get(indexOfGrpIssuePolSet).getIssueCont());
                    }
                }
                else
                {
                    System.out.println("系统没有自动发放团单问题件！");
                }
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
