package com.sinosoft.lis.tb;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title:个单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class PadContDeleteUI
{
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	public PadContDeleteUI()
	{
	}

	/**
	 * 不执行任何操作，只传递数据给下一层
	 * @param cInputData VData
	 * @param cOperate String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;

		PadContDeleteBL tContDeleteBL = new PadContDeleteBL();

		if (tContDeleteBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tContDeleteBL.mErrors);

			return false;
		}
		else
		{
			mResult = tContDeleteBL.getResult();
		}

		return true;
	}

	/**
	 * 获取从BL层取得的结果
	 * @return VData
	 */
	public VData getResult()
	{
		return mResult;
	}

	public static void main(String[] agrs)
	{
		LCContSchema tLCContSchema = new LCContSchema();    //集体保单
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.ManageCom = "86";
		mGlobalInput.Operator = "001";

		tLCContSchema.setContNo("13000122016");
		tLCContSchema.setManageCom("86110000");
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("DeleteReason","数据维护");

		VData tVData = new VData();
		tVData.add(tLCContSchema);
		tVData.add(mGlobalInput);
                tVData.add(tTransferData);

		PadContDeleteUI tgrlbl = new PadContDeleteUI();
		tgrlbl.submitData(tVData, "DELETE");

		if (tgrlbl.mErrors.needDealError())
		{
			System.out.println(tgrlbl.mErrors.getFirstError());
		}
	}
}
