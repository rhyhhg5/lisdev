package com.sinosoft.lis.tb;
//程序名称：ProposalConfirmNoUI.java
//程序功能：投保审核号录入
//创建日期：2007-11-16
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class ProposalConfirmNoUI 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	public ProposalConfirmNoUI()
	{
	}

    public boolean submitData(VData cInputData, String cOperate) 
    {
    	ProposalConfirmNoBL tProposalConfirmNoBL = new ProposalConfirmNoBL();
        if (!tProposalConfirmNoBL.submitData(cInputData, cOperate)) 
        {
            this.mErrors.copyAllErrors(tProposalConfirmNoBL.mErrors);
            return false;
        }
        return true;
    }
}
