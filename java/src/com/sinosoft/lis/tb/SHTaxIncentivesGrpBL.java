package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSGrpSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class SHTaxIncentivesGrpBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();

	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	/** 数据操作字符串 */
	private String mOperate;
	
	private LSGrpSchema mLSGrpSchema = new LSGrpSchema();
	
	public boolean submitData(VData cInputData, String cOperate){
		mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        
		if(!getInputData()){
			return false;
		}
		if(!checkData()){
			return false;
		}
		if(!dealData()){
			CError tError = new CError();
			tError.moduleName = "TaxIncentivesGrpBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理失败TaxIncentivesGrpBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(!prepareOutputData()){
			return false;
		}
		PubSubmit ps = new PubSubmit();
		if(!ps.submitData(mInputData, null)){
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}
	
	private boolean getInputData(){
		try{
			tGI.setSchema((GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0));
			mLSGrpSchema = (LSGrpSchema)mInputData.getObjectByObjectName("LSGrpSchema", 0);
			return true;
		}catch(Exception ex){
			CError tError = new CError();
            tError.moduleName = "TaxIncentivesGrpBL";
            tError.functionName = "getInputData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
		}
	}
	
	private boolean checkData(){
		if(mLSGrpSchema.getGrpNo()==null || "".equals(mLSGrpSchema.getGrpNo())){
			CError tError = new CError();
            tError.moduleName = "SHTaxIncentivesGrpBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未获得团体编号!";
            this.mErrors.addOneError(tError);
            return false;
		}
		if(mLSGrpSchema.getGrpName()==null || "".equals(mLSGrpSchema.getGrpName())){
			CError tError = new CError();
            tError.moduleName = "SHTaxIncentivesGrpBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未获得单位名称!";
            this.mErrors.addOneError(tError);
            return false;
		}
		if((mLSGrpSchema.getTaxRegistration()==null || "".equals(mLSGrpSchema.getTaxRegistration())) 
				&& (mLSGrpSchema.getOrgancomCode()==null || "".equals(mLSGrpSchema.getOrgancomCode())) ){
			CError tError = new CError();
            tError.moduleName = "SHTaxIncentivesGrpBL";
            tError.functionName = "checkData";
            tError.errorMessage = "税务登记证号或社会信用代码可能都没有录入!";
            this.mErrors.addOneError(tError);
            return false;
		}
		return true;
	}
	
	private boolean dealData(){
		if("INSERT".equals(mOperate)){
			map.put(mLSGrpSchema, SysConst.INSERT);
		}
		if("UPDATE".equals(mOperate)){
			
			map.put(mLSGrpSchema, SysConst.UPDATE);
		}
		return true;
	}
	
	private boolean prepareOutputData(){
		try {
			mInputData.add(map);
		}catch (Exception ex){
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "SHTaxIncentivesGrpBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public VData getResult(){
        return mResult;
    }
}
