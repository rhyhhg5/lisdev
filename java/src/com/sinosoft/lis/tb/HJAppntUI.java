package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2014-08-25
 */
public class HJAppntUI {
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    // @Constructor
    public HJAppntUI() {}

    // @Method
    /**
     * 数据提交方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return: boolean
     **/
    public boolean submitData(VData cInputData, String cOperate) {
        // 数据操作字符串拷贝到本类中
        this.mOperate = cOperate;

        HJAppntBL tHJAppntBL = new HJAppntBL();

        System.out.println("---UI BEGIN---");
        if (tHJAppntBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tHJAppntBL.mErrors);
            return false;
        } else {
            mResult = tHJAppntBL.getResult();
        }
        System.out.print(mErrors.toString());
        return true;
    }

    public VData getResult() {
        return mResult;
    }

}
