package com.sinosoft.lis.tb;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 扫描件申请</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class AddSubScanApplyBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();

  /** 业务处理相关变量 */
  private LCAddPolSchema  inLCAddPolSchema = new LCAddPolSchema();
  private LCAddPolSchema  outLCAddPolSchema = new LCAddPolSchema();

  public AddSubScanApplyBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT||MAIN"和"INSERT||DETAIL"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    System.out.println("cOperate:" + cOperate);

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //申请投保单处理
    if (mOperate.equals("INSERT||MAIN")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start AddSubScanApply BLS Submit...");
      AddSubScanApplyBLS tAddSubScanApplyBLS = new AddSubScanApplyBLS();
      if(tAddSubScanApplyBLS.submitData(mInputData, cOperate) == false)	{
        // @@错误处理
        this.mErrors.copyAllErrors(tAddSubScanApplyBLS.mErrors);
        mResult.clear();
        return false;
      }
      System.out.println("End AddSubScanApply BLS Submit...");

      //如果有需要处理的错误，则返回
      if (tAddSubScanApplyBLS.mErrors .needDealError())  {
        this.mErrors.copyAllErrors(tAddSubScanApplyBLS.mErrors ) ;
      }
    }
    //查询
    else if (mOperate.equals("QUERY||MAIN")) {
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      inLCAddPolSchema = (LCAddPolSchema)mInputData.getObjectByObjectName("LCAddPolSchema", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "AddSubScanApplyBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    int i;

    try {
      //申请投保单处理
      if (mOperate.equals("INSERT||MAIN")) {
        LCAddPolSchema tLCAddPolSchema = new LCAddPolSchema();

        //根据主险保单号码和流水号码获取记录的详细信息
        tLCAddPolSchema.setMainPolNo(inLCAddPolSchema.getMainPolNo());
        tLCAddPolSchema.setSerialNo(inLCAddPolSchema.getSerialNo());
        outLCAddPolSchema.setSchema(tLCAddPolSchema.getDB().query().get(1));

        outLCAddPolSchema.setInputOperator(inLCAddPolSchema.getInputOperator());     //操作员编码
        outLCAddPolSchema.setInputDate(PubFun.getCurrentDate());                     //当前日期
        outLCAddPolSchema.setInputTime(PubFun.getCurrentTime());                     //当前时间
        outLCAddPolSchema.setInputState(inLCAddPolSchema.getInputState());           //状态
      }
      //完成录入，查询附加险处理
      else if (mOperate.equals("QUERY||MAIN")) {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolSet tLCPolSet = new LCPolSet();

        //根据印刷号获取记录的详细信息
        tLCPolSchema.setPrtNo(inLCAddPolSchema.getPrtNo());
        tLCPolSet.set(tLCPolSchema.getDB().query());

        LMRiskSet tLMRiskSet = new LMRiskSet();
        for (i=0; i<tLCPolSet.size(); i++) {
          tLCPolSchema.setSchema(tLCPolSet.get(i + 1));

          LMRiskSchema tLMRiskSchema = new LMRiskSchema();
          tLMRiskSchema.setRiskCode(tLCPolSchema.getRiskCode());
          tLMRiskSet.add(tLMRiskSchema.getDB().query().get(1));
        }
        mResult.add(tLMRiskSet.encode());
      }
    }
    catch(Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeWithdrawBL";
      tError.functionName = "dealData";
      tError.errorMessage = "数据处理错误!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    mInputData = new VData();
    try {
      mInputData.add(outLCAddPolSchema);
    }
    catch(Exception ex) {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="TempFeeWithdrawBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
    AddSubScanApplyBL AddSubScanApplyBL1 = new AddSubScanApplyBL();
  }
}