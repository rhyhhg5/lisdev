/*
 * <p>ClassName: ContInsuredBL </p>
 * <p>Description: ContInsuredBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2004-11-18 10:09:44
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.brieftb.BriefGroupContDeleteBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.Vector;

public class ContInsuredBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private SSRS tSSRS = new SSRS(); //备用
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private Reflections ref = new Reflections();
    private TransferData mTransferData = new TransferData();
    private MMap map = new MMap();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LCGrpContDB mLCGrpContDB = new LCGrpContDB();
    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
    private LCInsuredDB mOLDLCInsuredDB = new LCInsuredDB(); //记录需要删除或修改的客户
    private LDPersonSchema mLDPersonSchema = new LDPersonSchema(); //从界面传入的客户数据
    private LDPersonSchema tLDPersonSchema; //tLDPersonSet包含需要新建的客户
    private ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new
            ES_DOC_RELATIONSchema(); //添加到关联表被保险人的数据
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
    private LCAddressSchema tLCAddressSchema;
    private LCAccountSchema mLCAccountSchema = new LCAccountSchema();
    private LCCustomerImpartSet mLCCustomerImpartSet;
    private LCCustomerImpartParamsSet mLCCustomerImpartParamsSet;
    private LCCustomerImpartDetailSet mLCCustomerImpartDetailSet;
    private LCPolSet preLCPolSet = new LCPolSet();
    private String ContNo;
    private String FamilyType;
    private String ContType;
    private String tPolTypeFlag;
    private String tInsuredPeoples;
    private String tInsuredAppAge;
    private String tSequenceNo; //客户顺序号
    private String tSaleChnlDetail;
    //yangming:为无名单添加
    private LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
    private VData noNameData = new VData();
    private String OldContNo = "";
    private LCAppntSchema tLCAppntSchema = new LCAppntSchema();
    private String LoadFlag;
    private LCInsuredListSchema mLCInsuredListSchema;
	  //modify by zxs 
    private String InsuredAuth;
    
    private String  InsuredPassIDNo;//原通行证号码
    
    private String YBTFlag = "";//银保通标识

    //private LCInsuredSet mLCInsuredSet=new LCInsuredSet();
    public ContInsuredBL() {
    }

    /**
     * 处理数据，不提交后台执行
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean preparesubmitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!mOperate.equals("DELETE||CONTINSURED")) {
            if (!this.checkData()) {
                return false;
            }
            System.out.println("---checkData---");
            //进行业务处理
            if (!dealData()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ContInsuredBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        } else {
            if (!deleteData()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "deleteData";
                tError.errorMessage = "删除数据时失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * 处理业务逻辑，并返回处理后的MMap
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (mOperate.equals("DELETE||INSUREDRISK")) {
            System.out.println("asfdsfsfafasf");
            if (!delrisk(cInputData)) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ContInsuredBL-->delrisk!";
                this.mErrors.addOneError(tError);
                return null;

            }
        } else {
            if (!getInputData(cInputData)) {
                return null;
            }
            if (!mOperate.equals("DELETE||CONTINSURED")) {
                if (!this.checkData()) {
                    return null;
                }
                System.out.println("---checkData---");
                //进行业务处理
                if (!dealData()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "submitData";
                    tError.errorMessage = "数据处理失败ContInsuredBL-->dealData!";
                    this.mErrors.addOneError(tError);
                    return null;
                }

                /** 投保人和被保人是本人 */
                /** @todo 被保人和投保人是本人需要更新投保客户 */
                if(StrTool.cTrim(mLCInsuredSchema.getRelationToAppnt())
                   .equals("00"))
                {
                    if(!dealInsuredWhenAppnt())
                    {
                        return null;
                    }
                }

            } else {
                if (!deleteData()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "删除数据时失败!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
            }

            //在进行被保险人修改的之后,如果有个人险种要进行重算
            if (mOperate.equals("UPDATE||CONTINSURED") &&
                preLCPolSet.size() > 0 && ContType.equals("1")) {
                LCContSchema aftLCContSchema = new LCContSchema();
                LCPolSet aftLCPolSet = new LCPolSet();
                VData tInputData = new VData();
                ReCalInsuredBL tReCalInsuredBl = new ReCalInsuredBL(
                        mLCInsuredSchema,
                        preLCPolSet, mLCContSchema, mGlobalInput);
                if (!tReCalInsuredBl.reCalInsured()) {
                    this.mErrors.copyAllErrors(tReCalInsuredBl.mErrors);
                    return null;
                }
//                System.out.println("##测试LCContSchema : "+mLCContSchema.getContNo());
                /** 处理被保人客户号码 */

                tInputData = tReCalInsuredBl.getResult();
                MMap tmepMap = (MMap) tInputData.getObjectByObjectName("MMap",
                        0);
                LCGetSet tempLCGetSet = (LCGetSet) tmepMap.
                                        getObjectByObjectName("LCGetSet", 0);
                LCPolSet tempLCPolSet = (LCPolSet) tmepMap.
                                        getObjectByObjectName("LCPolSet", 0);
                for (int i = 1; i <= tempLCGetSet.size(); i++) {
                    tempLCGetSet.get(i).setInsuredNo(mLCInsuredSchema.
                            getInsuredNo());
                }
                for (int i = 1; i <= tempLCPolSet.size(); i++) {
                    tempLCPolSet.get(i).setInsuredNo(mLCInsuredSchema.
                            getInsuredNo());
                    tempLCPolSet.get(i).setInsuredName(mLCInsuredSchema.getName());
                    tempLCPolSet.get(i).setInsuredBirthday(mLCInsuredSchema.
                            getBirthday());
                    tempLCPolSet.get(i).setInsuredSex(mLCInsuredSchema.getSex());

                    if (mLCInsuredSchema.getInsuredNo().equals(StrTool.cTrim(this.
                            tLCAppntSchema.getAppntNo()))) {
                        tempLCPolSet.get(i).setAppntNo(tLCAppntSchema.
                                getAppntNo());
                        tempLCPolSet.get(i).setAppntName(tLCAppntSchema.
                                getAppntName());
                    }
                }
                map.add(tmepMap);
                //团单下，单更新了个人合同时，也需要更新团体合同和集体险种表
                if (ContType.equals("2")) {
                    if ((LCContSchema) tInputData.getObjectByObjectName(
                            "LCContSchema", 0) != null) {
                        aftLCContSchema = (LCContSchema) tInputData.
                                          getObjectByObjectName(
                                                  "LCContSchema", 0);
                        aftLCPolSet = (LCPolSet) tInputData.
                                      getObjectByObjectName(
                                              "LCPolSet", 0);
                        mLCGrpContDB.setGrpContNo(aftLCContSchema.getGrpContNo());
                        //集体险种信息
                        String wherePart = "from LCPol where GrpContNo='" +
                                           mLCGrpContDB.getGrpContNo()
                                           + "')";
                        String updateLCGrpContStr = "update LCGrpCont set "
                                + "Prem=(select SUM(Prem) " +
                                wherePart
                                + ",Amnt=(select SUM(Amnt) " +
                                wherePart
                                + ",SumPrem=(select SUM(SumPrem) " +
                                wherePart
                                + ",Mult=(select SUM(Mult) " +
                                wherePart
                                + ",Peoples2=(select SUM(Peoples) "
                                + "from lccont where grpcontno='"
                                + mLCGrpContDB.getGrpContNo() +
                                "')"
                                + " where grpcontno='"
                                + mLCGrpContDB.getGrpContNo() +
                                "'";

                        map.put(updateLCGrpContStr, "UPDATE");
                        //更新集体险种表
                        for (int i = 1; i <= preLCPolSet.size(); i++) {
                            String fromPart = "from LCPol where GrpContNo='" +
                                              aftLCContSchema.getGrpContNo()
                                              + "' and riskcode ='" +
                                              preLCPolSet.get(i).getRiskCode() +
                                              "')";
                            map.put("update LCGrpPol set "
                                    + "Prem=(select SUM(Prem) " + fromPart
                                    + ", Amnt=(select SUM(Amnt) " + fromPart
                                    + ", SumPrem=(select SUM(SumPrem) " +
                                    fromPart
                                    + ", Mult=(select SUM(Mult) " + fromPart
                                    + ", Peoples2=(select SUM(InsuredPeoples) " +
                                    fromPart + " where grppolno='" +
                                    preLCPolSet.get(i).getGrpPolNo()
                                    + "'", "UPDATE");
                        }
                    }

                }
            }
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return null;
        }

        return map;
    }

    /**
     * 若被保人是投保人，则修改投保人信息
     * @return boolean
     */
    private boolean dealInsuredWhenAppnt()
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLCContSchema.getContNo());
        if(!tLCAppntDB.getInfo())
        {
            System.out.println(
                "程序第201行出错，请检查ContInsuredBL.java中的submitData方法！");
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL.java";
            tError.functionName = "submitData";
            tError.errorMessage = "投保人和被保人是本人,但查询投保人信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLCAppntSchema = tLCAppntDB.getSchema();
        Reflections ref = new Reflections();
        
        //将客户身份证号码中的x转换成大写（投保人） 2009-02-13 liuyp
        if (tLCAppntSchema.getIDType() != null && tLCAppntSchema.getIDNo() != null) {
        	if (tLCAppntSchema.getIDType().equals("0")) {
                String tLCAppntIdNo = tLCAppntSchema.getIDNo().toUpperCase();
                tLCAppntSchema.setIDNo(tLCAppntIdNo);
            }
        }
        
        String AccName = tLCAppntSchema.getAccName();
        String BankCode = tLCAppntSchema.getBankCode();
        String BankAccCode = tLCAppntSchema.getBankAccNo();
        String addressNo = tLCAppntSchema.getAddressNo();
        ref.transFields(tLCAppntSchema, mLCInsuredSchema);
        tLCAppntSchema.setAccName(AccName);
        tLCAppntSchema.setBankCode(BankCode);
        tLCAppntSchema.setBankAccNo(BankAccCode);
        tLCAppntSchema.setAppntBirthday(this.mLCInsuredSchema.getBirthday());
        tLCAppntSchema.setAppntSex(this.mLCInsuredSchema.getSex());
        tLCAppntSchema.setAppntNo(this.mLCInsuredSchema.getInsuredNo());
        tLCAppntSchema.setAppntName(this.mLCInsuredSchema.getName());
        if(tLCAppntSchema.getAddressNo() == null ||
            tLCAppntSchema.getAddressNo().equals(""))
        {
            tLCAppntSchema.setAddressNo(addressNo);
        }
        tLCAppntSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLCAppntSchema);
        tLCAppntSchema.setMakeDate(tLCAppntDB.getMakeDate());
        tLCAppntSchema.setMakeTime(tLCAppntDB.getMakeTime());
		 //modify by zxs 
        tLCAppntSchema.setAuthorization(tLCAppntDB.getAuthorization());

        /** 判断客户号是否发生改变 */
        if(!tLCAppntSchema.getAppntNo()
           .equals(mLCInsuredSchema.getInsuredNo()))
        {
            System.out.println("客户号发生改变，开始维护相关表");
            if(!changeNo(mLCInsuredSchema.getInsuredNo(),
                         tLCAppntSchema.getAppntNo()))
            {
                return false;
            }
        }
        map.put(tLCAppntSchema, SysConst.UPDATE);

        mLCContSchema.setAppntNo(tLCAppntSchema.getAppntNo());
        mLCContSchema.setAppntName(tLCAppntSchema.getAppntName());
        mLCContSchema.setAppntSex(tLCAppntSchema.getAppntSex());
        mLCContSchema.setAppntBirthday(tLCAppntSchema.getAppntBirthday());
        mLCContSchema.setAppntIDNo(tLCAppntSchema.getIDNo());
        mLCContSchema.setAppntIDType(tLCAppntSchema.getIDType());

        String sql = "update LCInsured "
                     + "set AppntNo = '" + mLCContSchema.getAppntNo() + "', "
                     + "   Operator = '" + mGlobalInput.Operator + "', "
                     + "   ModifyDate = Current Date, "
                     + "   ModifyTime = Current Time "
                     + "where ContNo = '" + mLCContSchema.getContNo() + "' ";
        map.put(sql, SysConst.UPDATE);

        sql = "update LCPol "
              + "set AppntNo = '" + mLCContSchema.getAppntNo() + "', "
              + "   AppntName = '" + mLCContSchema.getAppntName() + "', "
              + "   Operator = '" + mGlobalInput.Operator + "', "
              + "   ModifyDate = Current Date, "
              + "   ModifyTime = Current Time "
              + "where ContNo = '" + mLCContSchema.getContNo() + "' ";
        map.put(sql, SysConst.UPDATE);

        sql = "update LCPrem "
              + "set AppntNo = '" + mLCContSchema.getAppntNo() + "', "
              + "   Operator = '" + mGlobalInput.Operator + "', "
              + "   ModifyDate = Current Date, "
              + "   ModifyTime = Current Time "
              + "where ContNo = '" + mLCContSchema.getContNo() + "' ";
        map.put(sql, SysConst.UPDATE);

        return true;
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        if(getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        //　数据提交、保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start tPRnewManualDunBLS Submit...");
        // this.conn = DBConnPool.getConnection();
//        tPubSubmit.setCommitFlag(false);
//        tPubSubmit.setConnection(this.conn);
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * changeNo
     *
     * @return boolean
     */
    private boolean changeNo(String new_AppntNo, String old_AppntNo) {
        String[] tableName_1 = {"LCUWSub", "LCUWMaster", "LCUWError",
                               "LCRReport",
                               "LCPrem", "LCInsured", "LCIndUWSub",
                               "LCIndUWMaster", "LCIndUWError", "LCCUWSub",
                               "LCCUWMaster", "LCCUWError", "LCCont", "LCAppnt"};
        String condition_1 = "AppntNo='" + new_AppntNo + "'";
        String wherepart = " Contno='" + this.mLCContSchema.getContNo() +
                           "' and AppntNo='" + old_AppntNo + "'";
        Vector VecContPol_1 = PubFun.formUpdateSql(tableName_1, condition_1,
                wherepart);
        String[] tableName_2 = {"LCPol", "LCCont", };
        String condition_2 = "AppntNo='" + new_AppntNo + "',AppntName='" +
                             tLCAppntSchema.getAppntName() +
                             "'";
        Vector VecContPol_2 = PubFun.formUpdateSql(tableName_2, condition_2,
                wherepart);
        for (int i = 0; i < VecContPol_1.size(); i++) {
            this.map.put((String) VecContPol_1.get(i), "UPDATE");
        }
        for (int i = 0; i < VecContPol_2.size(); i++) {
            this.map.put((String) VecContPol_2.get(i), "UPDATE");
        }

        return true;
    }

    /**
     * 校验传入的数据
     * @return boolean
     */
    private boolean checkData() {
        if (!this.checkLDPerson()) {
            return false;
        }
        if (!this.checkLCAddress()) {
            return false;
        }
        if (ContType.equals("2") && mOperate.equals("INSERT||CONTINSURED")) {
            if (mLDPersonSchema.getCustomerNo() != null &&
                !("").equals(mLDPersonSchema.getCustomerNo())) {
                LCInsuredDB tLCInsuredDB = new LCInsuredDB();
                tLCInsuredDB.setGrpContNo(mLCContSchema.getGrpContNo());
                tLCInsuredDB.setInsuredNo(mLDPersonSchema.getCustomerNo());
                int insuredCount = tLCInsuredDB.getCount();
                if (tLCInsuredDB.mErrors.needDealError()) {
                    CError tError = new CError();
                    tError.moduleName = "ContBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "查询团体合同下被保险人失败!";

                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (insuredCount > 0) {
                    CError tError = new CError();
                    tError.moduleName = "ContBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "该团体合同下已经存在该被保险人，不能重复添加!";
                    this.mErrors.addOneError(tError);
                    return false;

                }
            }
        }
//        if (mOperate.equals("UPDATE||CONTINSURED"))
//        {
//            LCPolDB tLCPolDB = new LCPolDB();
//            tLCPolDB.setContNo(mLCContSchema.getContNo());
//            tLCPolDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
//            int polCOunt = tLCPolDB.getCount();
//            if (tLCPolDB.mErrors.needDealError())
//            {
//                CError.buildErr(this, "查询被保险人险种保单时失败！");
//                return false;
//            }
//            if (polCOunt > 0)
//            {
//                if (balance(mLDPersonSchema, mOLDLCInsuredDB))
//                {
//                    CError.buildErr(this,
//                                    "该被保险人已经录入险种不能更新性别，生日，职业类别等关键信息,请先删除险种！");
//                }
//                return false;
//            }
//        }

//    //删除时要判断如果时主被保险人，要确认没有其他被保险人??放到录入完成时统一校验有没有主被保险人
//    if (mOperate.equals("DELETE||CONTINSURED")){
//      if (("00").equals(mOLDLCInsuredDB.getRelationToMainInsured())){
//        LCInsuredDB tempLCInsuredDB = new LCInsuredDB();
//        LCInsuredSet tempLCInsuredSet = new LCInsuredSet();
//        tempLCInsuredDB.setContNo(mOLDLCInsuredDB.getContNo());
//        tempLCInsuredSet = tempLCInsuredDB.query();
//        if (tempLCInsuredDB.mErrors.needDealError()) {
//          this.mErrors.copyAllErrors(mOLDLCInsuredDB.mErrors);
//          CError tError = new CError();
//          tError.moduleName = "ContInsuredBL";
//          tError.functionName = "deleteData";
//          tError.errorMessage = "获取合同下被保险人信息时失败!";
//          this.mErrors.addOneError(tError);
//          return false;
//        }
//        if (tempLCInsuredSet.size() > 1) {
//          this.mErrors.copyAllErrors(mOLDLCInsuredDB.mErrors);
//          CError tError = new CError();
//          tError.moduleName = "ContInsuredBL";
//          tError.functionName = "deleteData";
//          tError.errorMessage = "请先删除其他被保险人再删除主被保险人!";
//          this.mErrors.addOneError(tError);
//          return false;
//        }
//      }

//    }
        return true;

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData() {
        String tNo = "";
        String tLimit;
        String mflag = "";
        //当是个人时，由界面上取得合同号，当是团体下个人的时候，需要在保存个人合同下第一个被保险人的时候，生成LCCont
        if (ContType.equals("2")) {
            if (mLCContSchema.getContNo() == null ||
                mLCContSchema.getContNo().equals("")) {
                mLCGrpContDB.setGrpContNo(mLCContSchema.getGrpContNo());
                if (!mLCGrpContDB.getInfo()) {
                    this.mErrors.copyAllErrors(mLCGrpContDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "获取团体合同数据时失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLCContSchema.setManageCom(mLCGrpContDB.getManageCom());
                mLCContSchema.setAgentCode(mLCGrpContDB.getAgentCode());
                mLCContSchema.setAgentGroup(mLCGrpContDB.getAgentGroup());
                mLCContSchema.setAgentCom(mLCGrpContDB.getAgentCom());
                mLCContSchema.setAgentType(mLCGrpContDB.getAgentType());
                mLCContSchema.setSaleChnl(mLCGrpContDB.getSaleChnl());
                mLCContSchema.setSignCom(mGlobalInput.ManageCom);
                mLCContSchema.setAppntNo(mLCGrpContDB.getAppntNo());
                mLCContSchema.setAppntName(mLCGrpContDB.getGrpName());
                mLCContSchema.setCValiDate(mLCGrpContDB.getCValiDate());
                if (mLCInsuredListSchema != null && mLCInsuredListSchema.getEdorNo() == null &&
                    mLCInsuredListSchema.getEdorValiDate() != null) {
                    mLCContSchema.setCValiDate(mLCInsuredListSchema.
                                               getEdorValiDate());
                }
                mLCContSchema.setCInValiDate(mLCGrpContDB.getCInValiDate());
                /** yangming 目的是 团单下个单添加后关系是本人 */
                mLCInsuredSchema.setRelationToMainInsured("00");
                
                tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
                tNo = PubFun1.CreateMaxNo("ProposalContNo", tLimit);
                if (StrTool.cTrim(tNo).equals("")) {
                    System.out.println("程序生成合同号码错误, 检查ContInsuredBL 第511行");
                    this.mErrors.copyAllErrors(mLCGrpContDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "生成内部合同号码错误!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //合同上去掉处理机构
//        if (mLCInsuredSchema.getExecuteCom() == null ||
//            mLCInsuredSchema.getExecuteCom().equals("")) {
//          mLCContSchema.setExecuteCom(mGlobalInput.ManageCom);
//        }
//        else {
//          mLCContSchema.setExecuteCom(mLCInsuredSchema.getExecuteCom());
//        }

                mLCContSchema.setContNo(tNo);
                mLCContSchema.setProposalContNo(tNo);
                mLCContSchema.setContType(ContType);

                mLCContSchema.setInsuredNo("0"); //暂设
                mLCContSchema.setPolType(tPolTypeFlag);
                if (tInsuredPeoples == null || tInsuredPeoples.equals("")) {
                    mLCContSchema.setPeoples(1);
                } else {
                    mLCContSchema.setPeoples(tInsuredPeoples);
                }
                mLCContSchema.setCardFlag("0");
                String mSavePolType = (String) mTransferData.getValueByName(
                        "SavePolType");

                if (mLCContSchema.getAppFlag() == null ||
                    mLCContSchema.getAppFlag().equals("")) {
                    if (mSavePolType != null && !mSavePolType.equals("") &&
                        !mSavePolType.equals("null")) {
                        if (mSavePolType.trim().equals("")) {
                            mLCContSchema.setAppFlag("0");
                            mLCContSchema.setStateFlag("0");
                        } else {
                            mLCContSchema.setAppFlag(mSavePolType);
                        }
                    } else {
                        mLCContSchema.setAppFlag("0");
                        mLCContSchema.setStateFlag("0");
                    }
                }

                mLCContSchema.setApproveFlag("0");
                mLCContSchema.setUWFlag("0");

                mLCContSchema.setOperator(mGlobalInput.Operator);
                mLCContSchema.setMakeDate(theCurrentDate);
                mLCContSchema.setMakeTime(theCurrentTime);
            }
            if (StrTool.cTrim(this.LoadFlag).equals("2")) {
                //需要将当前合同保存到备份表中
                BriefGroupContDeleteBL tBriefGroupContDeleteBL = new
                        BriefGroupContDeleteBL();
                VData tempVData = new VData();
                LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
                tLCGrpContSchema.setGrpContNo(this.mLCContSchema.getGrpContNo());
                tempVData.add(tLCGrpContSchema);
                tBriefGroupContDeleteBL.submitData(tempVData, "DELETE||CONT");
                tempVData.clear();
                tempVData = tBriefGroupContDeleteBL.getResult();
                MMap tempMap = (MMap) tempVData.getObjectByObjectName("MMap", 0);
                if (tempMap.size() > 0) {
                    this.map.add(tempMap);
                }
            }
        } else { //针对个人合同单处理被保人数
            System.out.println("人数是----***：" + mLCContSchema.getPeoples());
            if (mLCContSchema.getPeoples() == 0) {
                mLCContSchema.setPeoples(1);
            } else {
                mLCContSchema.setPeoples(mLCContSchema.getPeoples() + 1);
            }
        }
        tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        
        //将客户身份证号码中的x转换成大写（被保险人） 2009-02-15 liuyp]
        if (mLDPersonSchema.getIDType() !=null && mLDPersonSchema.getIDNo() !=null) {
        	if (mLDPersonSchema.getIDType().equals("0")) {
                String tLDPersonIdNo = mLDPersonSchema.getIDNo().toUpperCase();
                mLDPersonSchema.setIDNo(tLDPersonIdNo);
            }
        }
        
        //判断是否需要新建客户
        if (mLDPersonSchema.getCustomerNo() == null ||
            ("").equals(mLDPersonSchema.getCustomerNo())) {
        	
        	
            tNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
            mLDPersonSchema.setCustomerNo(tNo);
                      
            if (this.tPolTypeFlag.equals("0")) { //个人客户
                mLDPersonSchema.setMakeDate(theCurrentDate);
                mLDPersonSchema.setMakeTime(theCurrentTime);
                mLDPersonSchema.setModifyDate(theCurrentDate);
                mLDPersonSchema.setModifyTime(theCurrentTime);
                mLDPersonSchema.setOperator(mGlobalInput.Operator);
                 //modify by zxs 
			if(InsuredAuth!=null&&!InsuredAuth.equals("")){
                mLDPersonSchema.setAuthorization(InsuredAuth);
			}
			if(InsuredPassIDNo!=null&&!InsuredPassIDNo.equals("")){
                mLDPersonSchema.setOriginalIDNo(InsuredPassIDNo);
			}
                tLDPersonSchema = new LDPersonSchema();
                tLDPersonSchema.setSchema(mLDPersonSchema); //tLDPersonSet包含需要新建的客户
            } else {
                if (this.tPolTypeFlag.equals("2")) {
                    mLCInsuredSchema.setName("公共账户");
                } else {
                    mLCInsuredSchema.setName("无名单");
                }
                
                if (mLCInsuredSchema.getSex() == null ||
                    mLCInsuredSchema.getSex().equals("")) {
                    mLCInsuredSchema.setSex("0");
                }
                if (mLCInsuredSchema.getBirthday() == null ||
                    mLCInsuredSchema.getBirthday().equals("")) { //如果是无名单，没有录入年龄，默认是30
                    int tInsuredAge = 0;
                    if (tInsuredAppAge == null || tInsuredAppAge.equals("")) { //如果投保年龄没有录入
                        tInsuredAge = 30;

                    } else {
                        tInsuredAge = Integer.parseInt(tInsuredAppAge); //前台录入年龄
                    }

                    //年龄所在年=系统年-年龄
                    String year = Integer.toString(Integer.parseInt(StrTool
                            .getYear())
                            - tInsuredAge);
                    String brithday = StrTool.getDate(year,
                            StrTool.getMonth(),
                            StrTool.getDay());
                    brithday = StrTool.replace(brithday, "/", "-");
                    mLCInsuredSchema.setBirthday(brithday);
                }
            }
        }else{
        	LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(mLDPersonSchema.getCustomerNo());
            if (tLDPersonDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "updateldperson";
                tError.errorMessage = "没有查到客户信息!";
                this.mErrors.addOneError(tError);
            }
            tLDPersonDB.setNativePlace(mLDPersonSchema.getNativePlace());
            tLDPersonDB.setNativeCity(mLDPersonSchema.getNativeCity());
        	//modify by zxs 
	          LDPersonSchema ldperson =   tLDPersonDB.getSchema();
			if(InsuredAuth!=null&&!InsuredAuth.equals("")){
				if(InsuredAuth.equals("0")&&(ldperson.getAuthorization()==null||ldperson.getAuthorization().equals(""))){
					ldperson.setAuthorization(InsuredAuth);
				}
				if(InsuredAuth.equals("1")){
					ldperson.setAuthorization(InsuredAuth);
				}
			}
			if(InsuredPassIDNo!=null&&!InsuredPassIDNo.equals("")){
				ldperson.setOriginalIDNo(InsuredPassIDNo);
			}
			if(!mLDPersonSchema.getIDType().equals("7")){
				ldperson.setIDType(mLDPersonSchema.getIDType());
				ldperson.setIDNo(mLDPersonSchema.getIDNo());
			}
        	map.put(ldperson, SysConst.UPDATE);
        }
        mLCInsuredSchema.setInsuredNo(mLDPersonSchema.getCustomerNo());
        System.out.println("mLCInsuredSchema.getInsuredNo:" +
                           mLCInsuredSchema.getInsuredNo());
        
        //zxs #4351 20190424
        
        if(!"ybt".equals(YBTFlag)){
        if("1".equals(mLCContSchema.getContType())&&!mLCContSchema.getPrtNo().startsWith("PD")){
        String insuredCount = new ExeSQL().getOneValue("select insuredno from lcinsured where prtno = '"+mLCContSchema.getPrtNo()+"' and sequenceno ='"+tSequenceNo+"'");
        MMap mMap = new MMap();
        if(insuredCount!=null&&!insuredCount.equals("")){
        	mMap.put("delete from LCPersonTrace where customerno in (select insuredno from lcinsured where prtno = '"+mLCContSchema.getPrtNo()+"' and sequenceno ='"+tSequenceNo+"') and ContractNo='"+mLCContSchema.getPrtNo()+"' ", "DELETE");
        }
        VData vData = new VData();
        vData.add(mMap);
        PubSubmit pubSubmit = new PubSubmit();
        if (!pubSubmit.submitData(vData, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(pubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        LCPersonTraceSchema lcPersonTraceSchema = new LCPersonTraceSchema();
        String customerno;
        if(tNo!=null&&!tNo.equals("")&&tNo.length()==9){
        	lcPersonTraceSchema.setCustomerNo(tNo);
        	customerno = tNo;
        }else{
        	lcPersonTraceSchema.setCustomerNo(mLCInsuredSchema.getInsuredNo());
        	customerno = mLCInsuredSchema.getInsuredNo();
        }
        SSRS tSSRS3 = new SSRS();
        tSSRS3 = new ExeSQL().execSQL("select * from LCPersonTrace where CustomerNo='"+customerno+"' and ContractNo='"+mLCContSchema.getPrtNo()+"'");
        if(tSSRS3.getMaxRow()<=0){
        SSRS tSSRS2 = new SSRS();
        String sql1 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
                + lcPersonTraceSchema.getCustomerNo() + "'";
        ExeSQL tExeSQL1 = new ExeSQL();
        tSSRS2 = tExeSQL1.execSQL(sql1);
        Integer firstinteger1 = Integer.valueOf(tSSRS2.GetText(1, 1));
        int tTraceNo = firstinteger1.intValue() + 1;
        Integer sTraceNo = new Integer(tTraceNo);
        String mTraceNo = sTraceNo.toString();
        lcPersonTraceSchema.setTraceNo(mTraceNo);
        if(InsuredAuth==null || InsuredAuth.equals("")||"0".equals(InsuredAuth)){
       	 lcPersonTraceSchema.setSharedMark("0");
            lcPersonTraceSchema.setAuthorization("0");
            lcPersonTraceSchema.setCustomerContact("6");
       }else{
       	 lcPersonTraceSchema.setSharedMark(InsuredAuth);
            lcPersonTraceSchema.setAuthorization(InsuredAuth);
            lcPersonTraceSchema.setCustomerContact("0");
       }
        lcPersonTraceSchema.setSpecialLimitMark("0");
        lcPersonTraceSchema.setCompanySource("2");//人保健康
        lcPersonTraceSchema.setInstitutionSource(mLCContSchema.getManageCom());
        lcPersonTraceSchema.setBusinessLink("1");
        lcPersonTraceSchema.setAuthType("1");//授权条款
        lcPersonTraceSchema.setAuthVersion("1.0");
        lcPersonTraceSchema.setContractNo(mLCContSchema.getPrtNo());
        lcPersonTraceSchema.setSendDate(theCurrentDate);
        lcPersonTraceSchema.setSendTime(theCurrentTime);
        lcPersonTraceSchema.setModifyDate(theCurrentDate);
        lcPersonTraceSchema.setModifyTime(theCurrentTime);
        map.put(lcPersonTraceSchema, "INSERT");
        }
        }
        }
        if(ContType.equals("2")){
        	LCGrpContDB tlgc = new LCGrpContDB();
        	tlgc.setGrpContNo(mLCContSchema.getGrpContNo());
            if (!tlgc.getInfo()) {
                this.mErrors.copyAllErrors(tlgc.mErrors);
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "deleteData";
                tError.errorMessage = "获取团体合同数据时失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        	String traceno = "select case when varchar(max(int(TraceNo))) is null then '0' else varchar(max(int(TraceNo))) end from lcpersontrace where customerno='"+mLCInsuredSchema.getInsuredNo()+"' with ur ";
        	LCPersonTraceSchema tlcpersontrace = new LCPersonTraceSchema();
        	String trace = "delete from lcpersontrace where customerno='"+mLCInsuredSchema.getInsuredNo()+"' and contractno='"+tlgc.getPrtNo()+"'";
        	boolean b = new ExeSQL().execUpdateSQL(trace);
        	if(!b){
        		CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "deletelcpersontrace";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	//标准、简易、补充工伤
        	if(tlgc.getCardFlag()==null||"".equals(tlgc.getCardFlag())||"0".equals(tlgc.getCardFlag())||"4".equals(tlgc.getCardFlag())){
        		tlcpersontrace.setCustomerNo(mLCInsuredSchema.getInsuredNo());
        		int index = Integer.parseInt(new ExeSQL().getOneValue(traceno));
        		tlcpersontrace.setTraceNo(String.valueOf(index+1));
        		if(tlgc.getMarketType()!=null&&!"".equals(tlgc.getMarketType())){
        			if("1".equals(tlgc.getMarketType())||"9".equals(tlgc.getMarketType())){
        				tlcpersontrace.setAuthorization("1");
        				tlcpersontrace.setSharedMark("1");
        			}else{
        				tlcpersontrace.setAuthorization("0");
        				tlcpersontrace.setSharedMark("0");
        			}
        		}
        		tlcpersontrace.setSpecialLimitMark("0");
        		tlcpersontrace.setBusinessLink("1");
        		tlcpersontrace.setCustomerContact("0");
        		tlcpersontrace.setAuthType("1");
        		tlcpersontrace.setAuthVersion("1.0");
        		tlcpersontrace.setCompanySource("2");
        		tlcpersontrace.setInstitutionSource(tlgc.getManageCom());
        		tlcpersontrace.setContractNo(tlgc.getPrtNo());
        		tlcpersontrace.setSendDate(theCurrentDate);
        		tlcpersontrace.setSendTime(theCurrentTime);
        		tlcpersontrace.setModifyDate(theCurrentDate);
        		tlcpersontrace.setModifyTime(theCurrentTime);
        		map.put(tlcpersontrace, SysConst.INSERT);
        	}
        }
        //产生客户地址
        if (mLCAddressSchema != null) {
//            if ((!StrTool.compareString(mLCAddressSchema.getPostalAddress(), "")
//                 || !StrTool.cTrim(mLCAddressSchema.getHomeAddress()).equals("")
//                 ||
//                 !StrTool.cTrim(mLCAddressSchema.getCompanyAddress()).equals("")) &&
//                (StrTool.compareString(mLCAddressSchema.getAddressNo(), ""))) {
                try {
                    tSSRS = new SSRS();
                    String sql = "Select Case When (max(integer(AddressNo))) Is Null Then 0 Else (max(integer(AddressNo))) End  from LCAddress where CustomerNo='"
                                 + mLDPersonSchema.getCustomerNo() + "'";
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(sql);
                    Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                    int ttNo = firstinteger.intValue() + 1;
                    Integer integer = new Integer(ttNo);
                    tNo = integer.toString();
                    System.out.println("得到的地址码是：" + tNo);
                    mLCAddressSchema.setAddressNo(tNo);

                } catch (Exception e) {
                    CError tError = new CError();
                    tError.moduleName = "ContIsuredBL";
                    tError.functionName = "createAddressNo";
                    tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
                    this.mErrors.addOneError(tError);
                    mLCAddressSchema.setAddressNo("");
                }
                mLCAddressSchema.setCustomerNo(mLDPersonSchema.getCustomerNo());
                mLCAddressSchema.setMakeDate(theCurrentDate);
                mLCAddressSchema.setMakeTime(theCurrentTime);
                mLCAddressSchema.setModifyDate(theCurrentDate);
                mLCAddressSchema.setModifyTime(theCurrentTime);
                mLCAddressSchema.setOperator(mGlobalInput.Operator);
                tLCAddressSchema = new LCAddressSchema();
                tLCAddressSchema.setSchema(mLCAddressSchema); //tLCAddressSchema包含需要新建的客户地址
//            }
        }

        //产生客户账户
//    if (mLCAccountSchema != null) {
//      if (mLCAccountSchema.getBankCode() != null &&
//          mLCAccountSchema.getBankAccNo() != null) {
//        mLCAccountSchema.setCustomerNo(mLDPersonSchema.getCustomerNo());
//        if (mLCAccountSchema.getAccName() == null &&
//            ("").equals(mLCAccountSchema.getAccName()))
//          mLCAccountSchema.setAccName(mLDPersonSchema.getName());
//        mLCAccountSchema.setMakeDate(theCurrentDate);
//        mLCAccountSchema.setMakeTime(theCurrentTime);
//        mLCAccountSchema.setModifyDate(theCurrentDate);
//        mLCAccountSchema.setModifyTime(theCurrentTime);
//        mLCAccountSchema.setOperator(mGlobalInput.Operator);
//
//        map.put(mLCAccountSchema, "INSERT");
//      }
//    }
        
        //将客户身份证号码中的x转换成大写（被保险人） 2009-02-15 liuyp
        if (mLCInsuredSchema.getIDType() != null && mLCInsuredSchema.getIDNo() != null) {
        	if (mLCInsuredSchema.getIDType().equals("0")) {
                String tLCInsuredIdNo = mLCInsuredSchema.getIDNo().toUpperCase();
                mLCInsuredSchema.setIDNo(tLCInsuredIdNo);
            }
        }
        
        //如果是主被保险人，设置合同表相关信息
        if (("00").equals(mLCInsuredSchema.getRelationToMainInsured())) {
            mLCContSchema.setInsuredBirthday(mLCInsuredSchema.getBirthday());
            mLCContSchema.setInsuredIDNo(mLCInsuredSchema.getIDNo());
            mLCContSchema.setInsuredIDType(mLCInsuredSchema.getIDType());
            mLCContSchema.setInsuredName(mLCInsuredSchema.getName());
            mLCContSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());
            mLCContSchema.setInsuredSex(mLCInsuredSchema.getSex());
            //如果是家庭单，在录入主被保险人时产生家庭保障号（在前台控制必须首先录入主被保险人）
            if (("1").equals(mLCContSchema.getFamilyType())) { //家庭单
                if (mLCContSchema.getFamilyID() == null ||
                    mLCContSchema.getFamilyID().equals("")) {
                    tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
                    tNo = PubFun1.CreateMaxNo("FamilyID", 10);
                    //mLCInsuredSchema.setFamilyID(tNo);
                    mLCContSchema.setFamilyID(tNo);
                }
            }
        }

        mLCInsuredSchema.setDiskImportNo(mOLDLCInsuredDB.getDiskImportNo());
        System.out.println("setDiskImportNo===" +
                           mOLDLCInsuredDB.getDiskImportNo());
        mLCInsuredSchema.setAddressNo(mLCAddressSchema.getAddressNo());
        mLCInsuredSchema.setContNo(mLCContSchema.getContNo());
        mLCInsuredSchema.setPrtNo(mLCContSchema.getPrtNo());
        mLCInsuredSchema.setAppntNo(mLCContSchema.getAppntNo());
        if(("00").equals(mLCInsuredSchema.getRelationToAppnt()))
        {
            mLCInsuredSchema.setAppntNo(mLCInsuredSchema.getInsuredNo());
        }
        mLCInsuredSchema.setGrpContNo(mLCContSchema.getGrpContNo());

        mLCInsuredSchema.setManageCom(mLCContSchema.getManageCom()); //管理理机构
        if (mLCInsuredSchema.getExecuteCom() == null ||
            mLCInsuredSchema.getExecuteCom().equals("")) {
            mLCInsuredSchema.setExecuteCom(mLCInsuredSchema.getManageCom()); //处理机构
        }
        mLCInsuredSchema.setSequenceNo(tSequenceNo);
        mLCInsuredSchema.setFamilyID(mLCContSchema.getFamilyID());
        mLCInsuredSchema.setModifyDate(theCurrentDate);
        mLCInsuredSchema.setModifyTime(theCurrentTime);
        mLCInsuredSchema.setOperator(mGlobalInput.Operator);
        if(mLCInsuredListSchema!=null)
        {
        	mLCInsuredSchema.setGrpInsuredPhone(mLCInsuredListSchema.getPhone());
        }

        //处理告知信息
        if (mLCCustomerImpartSet != null && mLCCustomerImpartSet.size() > 0) {
            //设置所有告知信息得客户号码
            for (int i = 1; i <= mLCCustomerImpartSet.size(); i++) {
                mLCCustomerImpartSet.get(i).setContNo(mLCContSchema.getContNo());
                mLCCustomerImpartSet.get(i).setGrpContNo(mLCContSchema.
                        getGrpContNo());
                mLCCustomerImpartSet.get(i).setPrtNo(mLCContSchema.getPrtNo());
                mLCCustomerImpartSet.get(i).setProposalContNo(mLCContSchema.
                        getProposalContNo());
                mLCCustomerImpartSet.get(i).setCustomerNo(mLCInsuredSchema.
                        getInsuredNo());
            }
            CustomerImpartBL mCustomerImpartBL = new CustomerImpartBL();
            VData tempVData = new VData();
            tempVData.add(mLCCustomerImpartSet);
            tempVData.add(mGlobalInput);
            mCustomerImpartBL.submitData(tempVData, "IMPART||DEAL");
            if (mCustomerImpartBL.mErrors.needDealError()) {

                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "dealData";
                tError.errorMessage = mCustomerImpartBL.mErrors.getFirstError().
                                      toString();
                this.mErrors.addOneError(tError);
                return false;
            }
            tempVData.clear();
            tempVData = mCustomerImpartBL.getResult();
            if (null != (LCCustomerImpartSet) tempVData.
                getObjectByObjectName("LCCustomerImpartSet", 0)) {
                mLCCustomerImpartSet = (LCCustomerImpartSet) tempVData.
                                       getObjectByObjectName(
                                               "LCCustomerImpartSet", 0);
                System.out.println("告知条数" + mLCCustomerImpartSet.size());
            } else {
                System.out.println("告知条数为空");
            }
            if (null != (LCCustomerImpartParamsSet) tempVData.
                getObjectByObjectName("LCCustomerImpartParamsSet", 0)) {
                mLCCustomerImpartParamsSet = (LCCustomerImpartParamsSet)
                                             tempVData.
                                             getObjectByObjectName(
                        "LCCustomerImpartParamsSet", 0);
            }
        }
        //处理告知明细信息
        if (mLCCustomerImpartDetailSet != null &&
            mLCCustomerImpartDetailSet.size() > 0) {
            //设置所有告知明细信息得客户号码
            for (int i = 1; i <= mLCCustomerImpartDetailSet.size(); i++) {
                if (mLCCustomerImpartDetailSet.get(i).getImpartVer().trim().
                    equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getImpartCode().trim().
                    equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getImpartDetailContent().
                    trim().equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getDiseaseContent().trim().
                    equals("")
                        ) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "dealData";
                    tError.errorMessage =
                            "健康状况告知中的告知版本，告知编码，告知内容，疾病内容录入不能为空，请检查！";
                    this.mErrors.addOneError(tError);
                    mflag = "CIDERROR";
                    break;
                }
                //增加对同一告知不同告知内容的存储
                for (int count = 1; count <= mLCCustomerImpartDetailSet.size();
                                 count++) {
                    if (StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartVer()).equals(
                                              mLCCustomerImpartDetailSet.get(
                            count).getImpartVer())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartCode()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getImpartCode())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getDiseaseContent()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getDiseaseContent())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getStartDate()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getStartDate())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getEndDate()).
                        equals(mLCCustomerImpartDetailSet.get(count).getEndDate())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getProver()).
                        equals(mLCCustomerImpartDetailSet.get(count).getProver())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getCurrCondition()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getCurrCondition())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getIsProved()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getIsProved())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartDetailContent()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getImpartDetailContent()
                        ) & count != i
                            ) {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ContInsuredBL";
                        tError.functionName = "dealData";
                        tError.errorMessage =
                                "健康状况告知中的告知版本，告知编码，告知内容，疾病内容录入重复，请检查！";
                        this.mErrors.addOneError(tError);
                        mflag = "CIDERROR";
                        break;
                    }
                }
                //tNo = PubFun1.CreateMaxNo("SubSerialNo", 7);
//                System.out.println("得到的告知码是：" + tNo);
                mLCCustomerImpartDetailSet.get(i).setSubSerialNo(String.valueOf(
                        i));
                mLCCustomerImpartDetailSet.get(i).setContNo(mLCContSchema.
                        getContNo());
                mLCCustomerImpartDetailSet.get(i).setGrpContNo(mLCContSchema.
                        getGrpContNo());
                mLCCustomerImpartDetailSet.get(i).setPrtNo(mLCContSchema.
                        getPrtNo());
                mLCCustomerImpartDetailSet.get(i).setProposalContNo(
                        mLCContSchema.
                        getProposalContNo());
                mLCCustomerImpartDetailSet.get(i).setCustomerNo(
                        mLCInsuredSchema.
                        getInsuredNo());
                mLCCustomerImpartDetailSet.get(i).setOperator(mGlobalInput.
                        Operator);
                mLCCustomerImpartDetailSet.get(i).setMakeDate(theCurrentDate);
                mLCCustomerImpartDetailSet.get(i).setMakeTime(theCurrentTime);
                mLCCustomerImpartDetailSet.get(i).setModifyDate(theCurrentDate);
                mLCCustomerImpartDetailSet.get(i).setModifyTime(theCurrentTime);
            }
        }

        if (mflag != "CIDERROR") {
            if (mOperate.equals("INSERT||CONTINSURED")) {
                insertData();
            }
            if (mOperate.equals("UPDATE||CONTINSURED")) {
                updateData();
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * 处理保存逻辑，保存处理过的数据
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean insertData() {
        //团单时需要更新团体合同表
        mLCContSchema.setModifyDate(theCurrentDate);
        mLCContSchema.setModifyTime(theCurrentTime);
        mLCInsuredSchema.setMakeDate(theCurrentDate);
        mLCInsuredSchema.setMakeTime(theCurrentTime);
        /*
                 //关联表数据的添加 add by tuqiang
                 ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
                 tES_DOC_MAINDB.setDocCode(mLCContSchema.getPrtNo());
                 ES_DOC_MAINSet tES_DOC_MAINDBSet = tES_DOC_MAINDB.query();
                 if(tES_DOC_MAINDBSet!=null&&tES_DOC_MAINDBSet.size()!=0)
                 {
          for(int i=0;i<tES_DOC_MAINDBSet.size();i++)
          {
         tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINDBSet.get(i+1).getDocID());
         tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINDBSet.get(i+1).getBussType());
         tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINDBSet.get(i+1).getSubType());
            tES_DOC_RELATIONSchema.setBussNo(mLCInsuredSchema.getInsuredNo());
            tES_DOC_RELATIONSchema.setBussNoType("41");
            tES_DOC_RELATIONSchema.setRelaFlag("0");
            map.put(tES_DOC_RELATIONSchema, "INSERT");
          }
                 }else
                 {
            System.out.println("写关联表出错！！");
                 }
         */
        map.put(mLCContSchema, "DELETE&INSERT");
        map.put(mLCInsuredSchema, "INSERT");
        if (tLDPersonSchema != null) {
            map.put(tLDPersonSchema, "INSERT");
        }
        if (tLCAddressSchema != null) {
            map.put(tLCAddressSchema, "INSERT");
        }
        if (mLCCustomerImpartParamsSet != null) {
            map.put(mLCCustomerImpartParamsSet, "INSERT");
        }
        if (mLCCustomerImpartSet != null) {
            map.put(mLCCustomerImpartSet, "INSERT");
        }
        if (mLCCustomerImpartDetailSet != null) {
            map.put(mLCCustomerImpartDetailSet, "INSERT");
        }
        if (ContType.equals("2")) {
            mLCGrpContDB.setGrpContNo(mLCContSchema.getGrpContNo());
            mLCGrpContDB.getInfo();
            if (mOperate.equals("INSERT||CONTINSURED")) {
                mLCGrpContDB.setPeoples2(mLCGrpContDB.getPeoples2() +
                                         mLCContSchema.getPeoples());
            }
            mLCGrpContDB.setModifyDate(theCurrentDate);
            mLCGrpContDB.setModifyTime(theCurrentTime);
            map.put(mLCGrpContDB.getSchema(), "UPDATE");
        }

        return true;
    }

    /**
     * 更新逻辑，更新处理过的数据
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean updateData() {
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCContSchema.getContNo());
        tLCPolDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
        tLCPolSet = tLCPolDB.query();
        if (tLCPolDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询该客户的险种保单失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLCPolSet.size() > 0 &&
            !StrTool.compareString(mOLDLCInsuredDB.getContPlanCode(),
                                   mLCInsuredSchema.getContPlanCode())) {
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该被保险人下还有险种保单，不能更新保险计划！";
            this.mErrors.addOneError(tError);
            return false;
        }
        deleteData();
        insertData();

        //当客户号码，姓名发生变更时，要更新险种表，领取项表
        if (!mOLDLCInsuredDB.getName().equals(mLCInsuredSchema.getName())
            ||
            !mOLDLCInsuredDB.getInsuredNo().equals(mLCInsuredSchema.
                getInsuredNo())) {
            LCGetSet tLCGetSet = new LCGetSet();
            LCGetDB tLCGetDB = new LCGetDB();

            tLCGetDB.setContNo(mLCContSchema.getContNo());
            tLCGetDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
            tLCGetSet = tLCGetDB.query();
            LCBnfSet tLCBnfSet = new LCBnfSet();
            LCBnfDB tLCBnfDB = new LCBnfDB();
            tLCBnfDB.setContNo(mLCContSchema.getContNo());
            tLCBnfDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
            tLCBnfSet = tLCBnfDB.query();
            if (tLCGetDB.mErrors.needDealError()) {
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询该客户的领取项失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            for (int i = 1; i <= tLCPolSet.size(); i++) {
                tLCPolSet.get(i).setInsuredNo(mLCInsuredSchema.getInsuredNo());
                tLCPolSet.get(i).setInsuredName(mLCInsuredSchema.getName());
            }
            for (int i = 1; i <= tLCGetSet.size(); i++) {
                tLCGetSet.get(i).setInsuredNo(mLCInsuredSchema.getInsuredNo());
            }
            if (tLCBnfSet.size() > 0) {
                map.put("update lcbnf set insuredno='" +
                        mLCInsuredSchema.getInsuredNo() + "'  where contno='" +
                        mLCContSchema.getContNo() + "' and insuredno='" +
                        mOLDLCInsuredDB.getInsuredNo() + "'"
                        , "UPDATE");
            }
            //更新体检，契调表中的客户信息
            map.put("update LCPENoticeResult set customerno='" +
                    mLCInsuredSchema.getInsuredNo() + "',name='" +
                    mLCInsuredSchema.getName() + "'  where contno='" +
                    mLCContSchema.getContNo() + "' and customerno='" +
                    mOLDLCInsuredDB.getInsuredNo() + "'"
                    , "UPDATE");
            map.put("update LCRReport set customerno='" +
                    mLCInsuredSchema.getInsuredNo() + "',name='" +
                    mLCInsuredSchema.getName() + "'  where contno='" +
                    mLCContSchema.getContNo() + "' and customerno='" +
                    mOLDLCInsuredDB.getInsuredNo() + "'"
                    , "UPDATE");
            map.put("update LCPENotice set customerno='" +
                    mLCInsuredSchema.getInsuredNo() + "',name='" +
                    mLCInsuredSchema.getName() + "'  where contno='" +
                    mLCContSchema.getContNo() + "' and customerno='" +
                    mOLDLCInsuredDB.getInsuredNo() + "'"
                    , "UPDATE");

            //更新查出来的保单集合，为重算作准备
            for (int i = 1; i <= preLCPolSet.size(); i++) {
                preLCPolSet.get(i).setInsuredNo(mLCInsuredSchema.getInsuredNo());
                preLCPolSet.get(i).setInsuredName(mLCInsuredSchema.getName());
            }
            if (tLCPolSet.size() > 0) {
                map.put(tLCPolSet, "UPDATE");
            }
            if (tLCGetSet.size() > 0) {
                map.put(tLCGetSet, "UPDATE");
            }

        }

        return true;
    }

    /**
     * 删除处理，删除处理后的数据，根据操作类型的不同采取不同的处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean deleteData() {
//        LCGrpContDB mLCGrpContDB = new LCGrpContDB();

        String updateContFlag = "INSERT";
        boolean updateGrpContFlag = false;
// 对于删除的情况，首先需要的是备份数据
        if (mOperate.equals("DELETE||CONTINSURED")) {
            map.put(
                    "insert into LOBInsured (select * from LCInsured where ContNo='"
                    + mLCContSchema.getContNo() + "' and InsuredNo = '"
                    + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");
            if (!StrTool.cTrim(mLCContSchema.getGrpContNo()).equals("") &&
                !this.ContNo.equals("")) {
                map.put("delete from lcinsuredlist where ContNo='" + ContNo +
                        "' and grpcontno='" + mLCContSchema.getGrpContNo() +
                        "'", "DELETE");
            }

            //备份告知参数表和告知表
            map.put("insert into LOBCustomerImpartParams"
                    + "(select *from LCCustomerImpartParams where ContNo='"
                    + mLCContSchema.getContNo() + "' and CustomerNo = '"
                    + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");
            map.put("insert into LOBCustomerImpart"
                    + "(select *from LCCustomerImpart where ContNo='"
                    + mLCContSchema.getContNo() + "' and CustomerNo = '"
                    + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");
            //备份完毕
            /*
                         map.put("insert into LobBnf"
                    + "(select * from LcBnf where ContNo='"
                    + mLCContSchema.getContNo() + "' and InsuredNo = '"
                    + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");
                         //受益人
             */
        }
        map.put(mOLDLCInsuredDB.getSchema(), "DELETE");

        //删除被保人套餐信息，在国际业务中有套餐信息，普通业务不会存储套餐
        map.put("delete from LCRiskDutyWrap where ContNo='"
                + mLCContSchema.getContNo() + "' and InsuredNo = '"
                + mOLDLCInsuredDB.getInsuredNo() + "'", "DELETE");

        //删除告知参数表
        map.put("delete from LCCustomerImpartParams where ContNo='"
                + mLCContSchema.getContNo() + "' and CustomerNo = '"
                + mOLDLCInsuredDB.getInsuredNo() + "'", "DELETE");
        //删除告知表
        map.put("delete from LCCustomerImpart where ContNo='"
                + mLCContSchema.getContNo() + "' and CustomerNo = '"
                + mOLDLCInsuredDB.getInsuredNo() + "'", "DELETE");
        //删除告知明细表
        map.put("delete from LCCustomerImpartDetail where ContNo='"
                + mLCContSchema.getContNo() + "' and CustomerNo = '"
                + mOLDLCInsuredDB.getInsuredNo() + "'", "DELETE");
        /*       //删除受益人
                map.put("delete from LcBnf where ContNo='"
                        + mLCContSchema.getContNo() + "' and InsuredNo = '"
                        + mOLDLCInsuredDB.getInsuredNo() + "'", "DELETE");
         */
        LCPolDB tLCPolDB = new LCPolDB();

        tLCPolDB.setContNo(mLCContSchema.getContNo());
        tLCPolDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
        preLCPolSet = tLCPolDB.query();
        if (tLCPolDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询该客户下险种保单时失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String updateLCGrpContStr = "";
        //如果删除被保险人时，需要删除被保险人下所有险种，在更新时不需要
        if (mOperate.equals("DELETE||CONTINSURED")) {
            if (preLCPolSet.size() > 0) {
                LCDelPolLogSet mLCDelPolLogSet = new LCDelPolLogSet();
                ///////////////////////////备份险种数据/////////////////////////////
                map.put(
                        "insert into LOBDuty (select * from LCDuty a where a.PolNo in "
                        + "(select b.polno from lcpol b where b.ContNo='" +
                        mLCContSchema.getContNo() + "'"
                        + " and b.InsuredNo = '" + mOLDLCInsuredDB.getInsuredNo()
                        + "'))", "INSERT");
                LCDutyDB tLCDutyDB = new LCDutyDB();
                LCDutySet tLCDutySet = new LCDutySet();
                tLCDutySet = tLCDutyDB.executeQuery(
                        "select * from LCDuty a where a.PolNo in "
                        + "(select b.polno from lcpol b where b.ContNo='"
                        + mLCContSchema.getContNo() + "'"
                        + " and b.InsuredNo = '" + mOLDLCInsuredDB.getInsuredNo()
                        + "')");
                if (tLCDutyDB.mErrors.needDealError()) {
                    CError.buildErr(this, "查询该被保险人责任信息时失败；");
                }
                if (tLCDutySet.size() > 0) {
                    map.put(tLCDutySet, "DELETE"); //被保人下的责任删除
                }

                map.put(
                        "insert into LOBPrem (select * from LCPrem a where a.PolNo in "
                        + "(select b.polno from lcpol b where b.ContNo='" +
                        mLCContSchema.getContNo() + "'"
                        + " and b.InsuredNo = '" + mOLDLCInsuredDB.getInsuredNo()
                        + "'))", "INSERT");
                LCPremDB tLCPremDB = new LCPremDB();
                LCPremSet tLCPremSet = new LCPremSet();
                tLCPremSet = tLCPremDB.executeQuery(
                        "select * from LCPrem a where a.PolNo in "
                        + "(select b.polno from lcpol b where b.ContNo='"
                        + mLCContSchema.getContNo() + "'"
                        + "and b.InsuredNo = '" + mOLDLCInsuredDB.getInsuredNo()
                        + "')");
                if (tLCPremDB.mErrors.needDealError()) {
                    CError.buildErr(this, "查询该被保险人保费项信息时失败；");
                }
                if (tLCPremSet.size() > 0) {
                    map.put(tLCPremSet, "DELETE"); //被保人下的险种删除
                }

                map.put(
                        "insert into LOBGet (select * from LCGet a where a.PolNo in "
                        + "(select b.polno from lcpol b where b.ContNo='" +
                        mLCContSchema.getContNo() + "'"
                        + " and b.InsuredNo = '" + mOLDLCInsuredDB.getInsuredNo()
                        + "'))", "INSERT");
                LCGetDB tLCGetDB = new LCGetDB();
                LCGetSet tLCGetSet = new LCGetSet();
                tLCGetSet = tLCGetDB.executeQuery(
                        "select * from LCGet a where a.PolNo in "
                        + "(select b.polno from lcpol b where b.ContNo='"
                        + mLCContSchema.getContNo() + "'"
                        + " and b.InsuredNo = '" + mOLDLCInsuredDB.getInsuredNo()
                        + "')");
                if (tLCGetDB.mErrors.needDealError()) {
                    CError.buildErr(this, "查询该被保险人给付项信息时失败；");
                }
                if (tLCGetSet.size() > 0) {
                    map.put(tLCGetSet, "DELETE"); //被保人下的给付删除
                }

                map.put(
                        "insert into LOBPol (select * from LCPol where ContNo='"
                        + mLCContSchema.getContNo() + "' and InsuredNo = '"
                        + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");

                map.put(preLCPolSet, "DELETE"); //被保人下的险种删除

                for (int polcount = 1; polcount <= preLCPolSet.size(); polcount++) {
//填写险种删除操作的日志////////////////////////////////////////////
                    LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();
                    LCPolSchema tLCPolSchema = new LCPolSchema();
                    tLCPolSchema.setSchema(preLCPolSet.get(polcount));
                    mLCDelPolLog.setOtherNo(tLCPolSchema.getPolNo());
                    if (ContType.equals("2")) {
                        mLCDelPolLog.setOtherNoType("4");
                    } else {
                        mLCDelPolLog.setOtherNoType("14");
                    }
                    mLCDelPolLog.setPrtNo(tLCPolSchema.getPrtNo());
                    if (tLCPolSchema.getAppFlag().equals("1")) {
                        mLCDelPolLog.setIsPolFlag("1");
                    } else {
                        mLCDelPolLog.setIsPolFlag("0");
                    }
                    mLCDelPolLog.setOperator(mGlobalInput.Operator);
                    mLCDelPolLog.setManageCom(mGlobalInput.ManageCom);
                    mLCDelPolLog.setMakeDate(PubFun.getCurrentDate());
                    mLCDelPolLog.setMakeTime(PubFun.getCurrentTime());
                    mLCDelPolLog.setModifyDate(PubFun.getCurrentDate());
                    mLCDelPolLog.setModifyTime(PubFun.getCurrentTime());
                    mLCDelPolLogSet.add(mLCDelPolLog);
                    if (polcount == preLCPolSet.size()) {
                        map.put(mLCDelPolLogSet, "INSERT");
                    }
/////////////////////////////////////////////////////////////////////////
                    //更新合同表
                    mLCContSchema.setPrem(mLCContSchema.getPrem() -
                                          preLCPolSet.get(polcount).getPrem());
                    mLCContSchema.setSumPrem(mLCContSchema.getSumPrem() -
                                             preLCPolSet.get(polcount).
                                             getSumPrem());
                    mLCContSchema.setAmnt(mLCContSchema.getAmnt() -
                                          preLCPolSet.get(polcount).getAmnt());
                    mLCContSchema.setMult(mLCContSchema.getMult() -
                                          preLCPolSet.get(polcount).getMult());
                    mLCContSchema.setPeoples(mLCContSchema.getPeoples() -
                                             preLCPolSet.get(polcount).
                                             getInsuredPeoples());
                    updateContFlag = "UPDATE";
                    //更新团体表
                    if (ContType.equals("2")) {

                        //集体险种信息
                        String fromPart = "from LCPol where GrpContNo='" +
                                          mLCContSchema.getGrpContNo()
                                          + "' and riskcode ='" +
                                          preLCPolSet.get(polcount).getRiskCode() +
                                          "')";
                        map.put("update LCGrpPol set "
                                + "Prem=(select SUM(Prem) " + fromPart
                                + ",Amnt=(select SUM(Amnt) " + fromPart
                                + ",SumPrem=(select SUM(SumPrem) " + fromPart
                                + ",Mult=(select SUM(Mult) " + fromPart
                                + ",Peoples2=(select SUM(InsuredPeoples) "
                                + fromPart
                                + " where grppolno='"
                                + preLCPolSet.get(polcount).getGrpPolNo() + "'"
                                , "UPDATE");

                    }
                }

            }
            if (("1").equals(mLCContSchema.getPolType())) { //无名单
                mLCContSchema.setPeoples(0);

            } else {
                mLCContSchema.setPeoples(mLCContSchema.getPeoples() - 1);

            }

            if (ContType.equals("2")) {
                //集体险种信息
                String fromPart = "from LCPol where GrpContNo='" +
                                  mLCContSchema.getGrpContNo()
                                  + "')";
                updateLCGrpContStr = "update LCGrpCont set "
                                     + "Prem=(select SUM(Prem) " +
                                     fromPart
                                     + ",Amnt=(select SUM(Amnt) " +
                                     fromPart
                                     + ",SumPrem=(select SUM(SumPrem) " +
                                     fromPart
                                     + ",Mult=(select SUM(Mult) " +
                                     fromPart
                                     + ",Peoples2=(select SUM(Peoples) "
                                     + "from lccont where grpcontno='"
                                     + mLCContSchema.getGrpContNo() +
                                     "')"
                                     + " where grpcontno='"
                                     + mLCContSchema.getGrpContNo() +
                                     "'";
                updateGrpContFlag = true;

            }

            //主被保险人时删除合同上主被保险人信息,在点录入完成，复核修改完成时，需要检查是否有主被保险人
            if (("00").equals(mOLDLCInsuredDB.getRelationToMainInsured())) {
                ////////////////////在此首先填写日志////////////////////////////////////////
                LCDelPolLogSchema InsuredLCDelPolLog = new LCDelPolLogSchema();
                InsuredLCDelPolLog.setOtherNo(mOLDLCInsuredDB.getSchema().
                                              getInsuredNo());
                InsuredLCDelPolLog.setOtherNoType("13");
                InsuredLCDelPolLog.setPrtNo(mOLDLCInsuredDB.getSchema().
                                            getPrtNo());
                if (mOLDLCInsuredDB.getSchema().equals("1")) { //no appflag,only give it
                    InsuredLCDelPolLog.setIsPolFlag("1");
                } else {
                    InsuredLCDelPolLog.setIsPolFlag("0");
                }
                InsuredLCDelPolLog.setOperator(mGlobalInput.Operator);
                InsuredLCDelPolLog.setManageCom(mGlobalInput.ManageCom);
                InsuredLCDelPolLog.setMakeDate(PubFun.getCurrentDate());
                InsuredLCDelPolLog.setMakeTime(PubFun.getCurrentTime());
                InsuredLCDelPolLog.setModifyDate(PubFun.getCurrentDate());
                InsuredLCDelPolLog.setModifyTime(PubFun.getCurrentTime());
/////////////////////////////日志填写完毕//////////////////////////////////

                mLCContSchema.setInsuredBirthday("");
                mLCContSchema.setInsuredIDNo("");
                mLCContSchema.setInsuredIDType("");
                mLCContSchema.setInsuredName("");
                mLCContSchema.setInsuredNo("0");
                mLCContSchema.setInsuredSex("");
                mLCContSchema.setModifyDate(theCurrentDate);
                mLCContSchema.setModifyTime(theCurrentTime);
                updateContFlag = "UPDATE";
                //团单时删除合同
                if (ContType.equals("2")) {
//首先将被保人的删除操作的日志填写,团单与个单的不同仅仅在于Type
                    InsuredLCDelPolLog.setOtherNoType("3");
///////////////填写日志完毕////////////////////////////////////
                    LCInsuredDB tLCInsuredDB = new LCInsuredDB();
                    tLCInsuredDB.setContNo(mOLDLCInsuredDB.getContNo());
                    int insuredCount = tLCInsuredDB.getCount();
                    if (tLCInsuredDB.mErrors.needDealError()) {
                        CError tError = new CError();
                        tError.moduleName = "ContInsuredBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "查询客户时失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    //最后一个客户时需要删除个人合同表
                    if (insuredCount == 1) {
                        updateContFlag = "DELETE";
//////////////////////////////在此首先填写个单合同日志////////////////////////////////////////
                        LCDelPolLogSchema LCContLCDelPolLog = new
                                LCDelPolLogSchema();
                        LCContLCDelPolLog.setOtherNo(mLCContSchema.getContNo());
                        LCContLCDelPolLog.setOtherNoType("2");
                        LCContLCDelPolLog.setPrtNo(mLCContSchema.getPrtNo());
                        if (mLCContSchema.getAppFlag().equals("1")) {
                            LCContLCDelPolLog.setIsPolFlag("1");
                        } else {
                            LCContLCDelPolLog.setIsPolFlag("0");
                        }
                        LCContLCDelPolLog.setOperator(mGlobalInput.Operator);
                        LCContLCDelPolLog.setManageCom(mGlobalInput.ManageCom);
                        LCContLCDelPolLog.setMakeDate(PubFun.getCurrentDate());
                        LCContLCDelPolLog.setMakeTime(PubFun.getCurrentTime());
                        LCContLCDelPolLog.setModifyDate(PubFun.getCurrentDate());
                        LCContLCDelPolLog.setModifyTime(PubFun.getCurrentTime());
                        map.put(LCContLCDelPolLog, "INSERT");
/////////////////////////////日志填写完毕//////////////////////////////////
/////////////////////////////数据的备份//////////////////////////////////
                        map.put(
                                "insert into LOBCont (select * from LCCont where ContNo='"
                                + mLCContSchema.getContNo() + "')", "INSERT");
/////////////////////////////数据备份完毕//////////////////////////////////
                    }
                }
                //map.put(mLCContSchema, "UPDATE");
                //删除主被保险人时填写日志
                map.put(InsuredLCDelPolLog, "INSERT");
            }

        }
        if (!updateContFlag.equals("INSERT")) {
            map.put(mLCContSchema, updateContFlag);
        }
        if (updateGrpContFlag) {
            map.put(updateLCGrpContStr, "UPDATE");
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));
            this.mLCContSchema.setSchema((LCContSchema) cInputData.
                                         getObjectByObjectName("LCContSchema",
                    0));
            this.mLCInsuredSchema.setSchema((LCInsuredSchema) cInputData.
                                            getObjectByObjectName(
                    "LCInsuredSchema",
                    0));
            this.mLCAddressSchema.setSchema((LCAddressSchema) cInputData.
                                            getObjectByObjectName(
                    "LCAddressSchema",
                    0));

            this.mOLDLCInsuredDB.setSchema((LCInsuredDB) cInputData.
                                           getObjectByObjectName("LCInsuredDB",
                    0));
            System.out.println("asdfasdf" + mOLDLCInsuredDB.getInsuredNo());

            this.mLDPersonSchema.setSchema((LDPersonSchema) cInputData.
                                           getObjectByObjectName(
                    "LDPersonSchema", 0));
            this.mLCCustomerImpartSet = (LCCustomerImpartSet) cInputData.
                                        getObjectByObjectName(
                                                "LCCustomerImpartSet", 0);
            this.mLCCustomerImpartDetailSet = (LCCustomerImpartDetailSet)
                                              cInputData.
                                              getObjectByObjectName(
                    "LCCustomerImpartDetailSet", 0);
            this.mTransferData = (TransferData) cInputData.
                                 getObjectByObjectName(
                                         "TransferData", 0);

            if (mTransferData == null) {
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "未得到传送数据!";
                this.mErrors.addOneError(tError);
                return false;

            }
            FamilyType = (String) mTransferData.getValueByName("FamilyType");
            tPolTypeFlag = (String) mTransferData.getValueByName("PolTypeFlag");
            tInsuredPeoples = (String) mTransferData.getValueByName(
                    "InsuredPeoples");
            tInsuredAppAge = (String) mTransferData.getValueByName(
                    "InsuredAppAge");
            FamilyType = (String) mTransferData.getValueByName("FamilyType");
            tSequenceNo = (String) mTransferData.getValueByName("SequenceNo");
            ContType = (String) mTransferData.getValueByName("ContType");
            OldContNo = (String) mTransferData.getValueByName("OldContNo");
            LoadFlag = (String) mTransferData.getValueByName("LoadFlag");
			 //modify by zxs 
            InsuredAuth  = (String) mTransferData.getValueByName("InsuredAuth");
            YBTFlag = (String)mTransferData.getValueByName("channl");
            InsuredPassIDNo = (String) mTransferData.getValueByName("InsuredPassIDNo");
            String DiskImportFlag = (String) mTransferData.getValueByName(
                    "DiskImportFlag");
            String OccupaytionType = mLCInsuredSchema.getOccupationType();
            ref.transFields(mLCInsuredSchema, mLDPersonSchema); //客户表
            mLCInsuredSchema.setOccupationType(OccupaytionType);
            mLCInsuredSchema.setInsuredNo(mLDPersonSchema.getCustomerNo());
            mLCContSchema.setContType(ContType);
            ContNo = mLCContSchema.getContNo();
            //当是个人时，由界面上取得合同号，当是团体下个人的时候，需要在保存个人合同下第一个的时候，生成LCCont
            //查询出销售渠道明细保存到lcpol
            if (ContNo != null && !("").equals(ContNo)) {
                LCContDB mLCContDB = new LCContDB();
                mLCContDB.setContNo(ContNo);
                LCContSet tLCContSet = mLCContDB.query();
                if (tLCContSet.size() > 1) {
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "查询合同信息不唯一!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tLCContSet.size() == 1) {
                    tSaleChnlDetail = tLCContSet.get(1).getSaleChnlDetail();
                    System.out.println("tSaleChnlDetail : " + tSaleChnlDetail);
                }
            }
            if (ContNo == null || ("").equals(ContNo)) {
                // @@错误处理
                if (ContType.equals("1")) {
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "未得到合同号!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            } else {
                if (DiskImportFlag != null && DiskImportFlag.equals("1")) {
                    //DoNothing
                } else {
                    LCContDB tLCContDB = new LCContDB();
                    tLCContDB.setContNo(ContNo);
                    tLCContDB.getInfo();
                    if (tLCContDB.mErrors.needDealError()) {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCContDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ContInsuredBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "未查到相关合同信息!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }

                    mLCContSchema.setSchema(tLCContDB);

                }
            }

            mLCContSchema.setFamilyType(FamilyType);
            if (!mOperate.equals("INSERT||CONTINSURED")) {
                System.out.println("InsuredNo:" + mOLDLCInsuredDB.getInsuredNo());
                mOLDLCInsuredDB.setContNo(mLCContSchema.getContNo());
                mOLDLCInsuredDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
                if (!mOLDLCInsuredDB.getInfo()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(mOLDLCInsuredDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "获取待删除被保险人信息时失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            mLCInsuredListSchema = (LCInsuredListSchema) cInputData.
                                   getObjectByObjectName("LCInsuredListSchema",
                    0);
        } catch (Exception ex) {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "getInputData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;

        }

        //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }

    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
            mResult.clear();
            LCInsuredSchema tempLCInsuredSchema = new LCInsuredSchema();
            tempLCInsuredSchema.setSchema(mLCInsuredSchema);
            mResult.add(tempLCInsuredSchema);
            mResult.add(this.tLCAddressSchema);
            mResult.add(this.mLDPersonSchema);
            mResult.add(this.mLCContSchema);
            mResult.add(this.mGlobalInput);
            mResult.add(this.mOLDLCInsuredDB);
            mResult.add(this.mLCCustomerImpartSet);
            mResult.add(this.mLCCustomerImpartDetailSet);
            mResult.add(this.mTransferData);
            mResult.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInsuredBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private boolean delrisk(VData cInputData) {
        System.out.println("asfdsfsfafasf0");
        this.mTransferData = (TransferData) cInputData.
                             getObjectByObjectName(
                                     "TransferData", 0);
        System.out.println("asfdsfsfafasf2");
        if (mTransferData == null) {
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "未得到传送数据!";
            this.mErrors.addOneError(tError);
            return false;

        }
        System.out.println("asfdsfsfafasf3");
        String InsuredNo = (String) mTransferData.getValueByName("InsuredNo");
        String PolNo = (String) mTransferData.getValueByName("PolNo");
        map.put("delete from lcpol where PolNo='" + PolNo + "'", "DELETE");
        map.put("delete from LCInsuredRelated where PolNo='" + PolNo + "'",
                "DELETE");
        map.put("delete from LCDuty where PolNo='" + PolNo + "'", "DELETE");
        map.put("delete from LCBnf where PolNo='" + PolNo + "'", "DELETE");
        map.put("delete from LCPrem where PolNo='" + PolNo + "'", "DELETE");
        map.put("delete from LCGet where PolNo='" + PolNo + "'", "DELETE");
        map.put("delete from LCSpec where PolNo='" + PolNo + "'", "DELETE");

        return true;
    }

    /**
     * 检查被保人录入的数据是否正确(适用于个人投保单和集体下的个单)
     * 如果在处理过程中出错或者数据有错误，则返回false,否则返回true
     * @return boolean
     */
    private boolean checkLDPerson() {
        //如果是无名单或者公共帐户的个人，不校验返回
        if (StrTool.compareString(tPolTypeFlag, "1")
            || StrTool.compareString(tPolTypeFlag, "2")) {
            return true;
        }

        if (mLDPersonSchema != null) {
            if (mLDPersonSchema.getName() != null) { //去空格
                mLDPersonSchema.setName(mLDPersonSchema.getName().trim());
                if (mLDPersonSchema.getSex() != null) {
                    mLDPersonSchema.setSex(mLDPersonSchema.getSex().trim());
                }
                if (mLDPersonSchema.getIDNo() != null) {
                    mLDPersonSchema.setIDNo(mLDPersonSchema.getIDNo().trim());
                }
                if (mLDPersonSchema.getIDType() != null) {
                    mLDPersonSchema.setIDType(mLDPersonSchema.getIDType().trim());
                }
                if (mLDPersonSchema.getBirthday() != null) {
                    mLDPersonSchema.setBirthday(mLDPersonSchema.getBirthday().
                                                trim());
                }

            }
            if (mLDPersonSchema.getCustomerNo() != null) {
                //如果有客户号
                if (!mLDPersonSchema.getCustomerNo().equals("")) {
                    LDPersonDB tLDPersonDB = new LDPersonDB();

                    tLDPersonDB.setCustomerNo(mLDPersonSchema.getCustomerNo());
                    if (!tLDPersonDB.getInfo()) {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkPerson";
                        tError.errorMessage = "查询客户失败!请确认您录入的客户号码";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if (mLDPersonSchema.getName() != null) {
                        String Name = StrTool.GBKToUnicode(tLDPersonDB.getName()
                                .trim());
                        String NewName = StrTool.GBKToUnicode(mLDPersonSchema.
                                getName()
                                .trim());

                        if (!Name.equals(NewName)) {
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户姓名("
                                                  + Name + ")与您录入的客户姓名("
                                                  + NewName + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                    if (mLDPersonSchema.getSex() != null) {
                        if (!StrTool.cTrim(tLDPersonDB.getSex()).equals(mLDPersonSchema
                                .getSex())) {
                            CError tError = new CError();
                            tError.moduleName = "ContBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户性别("
                                                  + tLDPersonDB.getSex()
                                                  + ")与您录入的客户性别("
                                                  + mLDPersonSchema.getSex()
                                                  + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                    if (mLDPersonSchema.getBirthday() != null) {
                        if (!StrTool.cTrim(tLDPersonDB.getBirthday()).equals(mLDPersonSchema
                                .getBirthday())) {
                            CError tError = new CError();
                            tError.moduleName = "ContBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户生日("
                                                  + tLDPersonDB.getBirthday()
                                                  + ")与您录入的客户生日("
                                                  + mLDPersonSchema.getBirthday()
                                                  + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                    //校验证件号码
                    if (mLDPersonSchema.getIDNo() != null) {
                        if (!StrTool.cTrim(tLDPersonDB.getIDNo()).equals(
                                mLDPersonSchema
                                .getIDNo())) {
                        	if(!StrTool.cTrim(tLDPersonDB.getOriginalIDNo()).equals(
                                    StrTool.cTrim(mLDPersonSchema.getIDNo()))){
                            CError tError = new CError();
                            tError.moduleName = "ContBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户证件号码("
                                                  + tLDPersonDB.getIDNo()
                                                  + ")与您录入的客户证件号码("
                                                  + mLDPersonSchema.getIDNo()
                                                  + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        	}
                        }
                    }
                    //校验证件类型
                    if (mLDPersonSchema.getIDType() != null) {
                        if (!StrTool.cTrim(tLDPersonDB.getIDType()).equals(mLDPersonSchema
                                .getIDType())) {
                        	if ((tLDPersonDB.getIDType().equals("a")||tLDPersonDB.getIDType().equals("b"))&&mLDPersonSchema.getIDType().equals("7")){
                        	}else{
                            CError tError = new CError();
                            tError.moduleName = "ContBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户证件类型("
                                                  + tLDPersonDB.getIDType()
                                                  + ")与您录入的客户证件类型("
                                                  + mLDPersonSchema.getIDType()
                                                  + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        	}
                        }
                    }

                } else {
                    //如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
                    
                        if ((mLDPersonSchema.getName() != null)
                            && (mLDPersonSchema.getSex() != null)
                            && (mLDPersonSchema.getIDNo() != null)) {
                            LDPersonDB tLDPersonDB = new LDPersonDB();
                            LDPersonSet tLDPersonSet=new LDPersonSet();
                        	tLDPersonDB.setName(mLDPersonSchema.getName());
                        	tLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
                        	tLDPersonDB.setIDType(mLDPersonSchema.getIDType());
                        	if(mLDPersonSchema.getIDType().equals("0")){
                        		tLDPersonSet = tLDPersonDB.query();
                        		//zxs
                        	}else if(mLDPersonSchema.getIDType().equals("a")||mLDPersonSchema.getIDType().equals("b")){
                        	LDPersonDB mLDPersonDB = new LDPersonDB();
                        	mLDPersonDB.setIDNo(mLDPersonSchema.getOriginalIDNo());
                        	mLDPersonDB.setIDType("7");
                        	mLDPersonDB.setName(mLDPersonSchema.getName());
                        	mLDPersonDB.setSex(mLDPersonSchema.getSex());
                        	mLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                             tLDPersonSet = mLDPersonDB.query();
                             if(tLDPersonSet.size()<=0){
                            	 LDPersonDB mtLDPersonDB = new LDPersonDB();
                            	 mtLDPersonDB.setName(mLDPersonSchema.getName());
                            	 mtLDPersonDB.setSex(mLDPersonSchema.getSex());
                            	 mtLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                            	 mtLDPersonDB.setIDType(mLDPersonSchema.getIDType());
                            	 mtLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
                                 tLDPersonSet = mtLDPersonDB.query();
                             }
                        }else if(mLDPersonSchema.getIDType().equals("7")){
                       	 tLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
                         tLDPersonDB.setIDType("7");
                         tLDPersonDB.setName(mLDPersonSchema.getName());
                         tLDPersonDB.setSex(mLDPersonSchema.getSex());
                         tLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                         tLDPersonSet = tLDPersonDB.query();
                         if(tLDPersonSet.size()<=0){
                        	 LDPersonDB mLDPersonDB = new LDPersonDB();
                        	 mLDPersonDB.setName(mLDPersonSchema.getName());
                        	 mLDPersonDB.setSex(mLDPersonSchema.getSex());
                        	 mLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                        	 mLDPersonDB.setOriginalIDNo(mLDPersonSchema.getIDNo());
                             tLDPersonSet = mLDPersonDB.query();
                         }
                    }else {
                        		tLDPersonDB.setSex(mLDPersonSchema.getSex());
                            	tLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                            	tLDPersonSet = tLDPersonDB.query();
                        	}
                        	
                        	if (tLDPersonSet != null) {
                        		if (tLDPersonSet.size() > 0)
                            	{
                                    mLDPersonSchema.setCustomerNo(tLDPersonSet.get(1).getCustomerNo());
                                }
                        	}
                        	
                            
                        }
                    
                }
            }

        }

        return true;
    }

    /**
     * 检查地址数据是否正确
     * 如果在处理过程中出错或者数据有错误，则返回false,否则返回true
     * @return boolean
     */
    private boolean checkLCAddress() {
        if (mLCAddressSchema != null) {
            if (mLCAddressSchema.getPostalAddress() != null) { //去空格
                mLCAddressSchema.setPostalAddress(mLCAddressSchema.
                                                  getPostalAddress().
                                                  trim());
                if (mLCAddressSchema.getZipCode() != null) {
                    mLCAddressSchema.setZipCode(mLCAddressSchema.getZipCode().
                                                trim());
                }
                if (mLCAddressSchema.getPhone() != null) {
                    mLCAddressSchema.setPhone(mLCAddressSchema.getPhone().trim());
                }

            }
            System.out.println("mLCAddressSchema.getCustomerNo()1" +
                               mLCAddressSchema.getCustomerNo());
            if (!StrTool.cTrim(mLCAddressSchema.getCustomerNo()).equals("") &
                mLCAddressSchema.getAddressNo() != null) {
                //如果有地址号
                if (!mLCAddressSchema.getAddressNo().equals("")) {
                    System.out.println("zhuzhu!!!!");
                    LCAddressDB tLCAddressDB = new LCAddressDB();

                    tLCAddressDB.setAddressNo(mLCAddressSchema.getAddressNo());
                    tLCAddressDB.setCustomerNo(mLCAddressSchema.getCustomerNo());
                    if (!tLCAddressDB.getInfo()) {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "数据库查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if (!StrTool.compareString(mLCAddressSchema.getCustomerNo(),
                                               tLCAddressDB.getCustomerNo())
                        || !StrTool.compareString(mLCAddressSchema.getAddressNo(),
                                                  tLCAddressDB.getAddressNo())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getPostalAddress(),
                                                  tLCAddressDB.getPostalAddress())
                        || !StrTool.compareString(mLCAddressSchema.getZipCode(),
                                                  tLCAddressDB.getZipCode())
                        || !StrTool.compareString(mLCAddressSchema.getPhone(),
                                                  tLCAddressDB.getPhone())
                        || !StrTool.compareString(mLCAddressSchema.getFax(),
                                                  tLCAddressDB.getFax())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getHomeAddress(),
                                                  tLCAddressDB.getHomeAddress())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getHomeZipCode(),
                                                  tLCAddressDB.getHomeZipCode())
                        || !StrTool.compareString(mLCAddressSchema.getHomePhone(),
                                                  tLCAddressDB.getHomePhone())
                        || !StrTool.compareString(mLCAddressSchema.getHomeFax(),
                                                  tLCAddressDB.getHomeFax())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getCompanyAddress(),
                                                  tLCAddressDB.
                                                  getCompanyAddress())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getCompanyZipCode(),
                                                  tLCAddressDB.
                                                  getCompanyZipCode())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getCompanyPhone(),
                                                  tLCAddressDB.getCompanyPhone())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getCompanyFax(),
                                                  tLCAddressDB.getCompanyFax())
                        || !StrTool.compareString(mLCAddressSchema.getMobile(),
                                                  tLCAddressDB.getMobile())
                        || !StrTool.compareString(mLCAddressSchema.getMobileChs(),
                                                  tLCAddressDB.getMobileChs())
                        || !StrTool.compareString(mLCAddressSchema.getEMail(),
                                                  tLCAddressDB.getEMail())
                        || !StrTool.compareString(mLCAddressSchema.getBP(),
                                                  tLCAddressDB.getBP())
                        || !StrTool.compareString(mLCAddressSchema.getMobile2(),
                                                  tLCAddressDB.getMobile2())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getMobileChs2(),
                                                  tLCAddressDB.getMobileChs2())
                        || !StrTool.compareString(mLCAddressSchema.getEMail2(),
                                                  tLCAddressDB.getEMail2())
                        || !StrTool.compareString(mLCAddressSchema.getBP2(),
                                                  tLCAddressDB.getBP2())
                            ) {
                        CError tError = new CError();
                        tError.moduleName = "ContInsuredBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage =
                                "您输入的地址信息与数据库里对应的信息不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * 对比险种中关系到保额保费计算的被保人信息是否有更改
     * @param vLDPersonSchema LDPersonSchema
     * @param vLCInsuredSchema LCInsuredSchema
     * @return boolean
     */
    private static boolean balance(LDPersonSchema vLDPersonSchema,
                                   LCInsuredSchema vLCInsuredSchema) {
        //性别
        if (!(vLDPersonSchema.getSex().trim()).equals(vLCInsuredSchema.getSex().
                trim())) {
            System.out.println("性别:" + vLDPersonSchema.getSex() + ":"
                               + vLCInsuredSchema.getSex());
            return true;
        }
        //生日
        if (!(vLDPersonSchema.getBirthday().trim()).equals(vLCInsuredSchema.
                getBirthday().trim())) {
            System.out.println("生日:" + vLDPersonSchema.getBirthday() + ":"
                               + vLCInsuredSchema.getBirthday());
            return true;
        }
        //职业类别
        if (vLDPersonSchema.getOccupationType() != null) {
            if (!StrTool.compareString(vLDPersonSchema.getOccupationType(),
                                       vLCInsuredSchema.getOccupationType())) {
                System.out.println("职业:" + vLDPersonSchema.getOccupationType()
                                   + ":" + vLCInsuredSchema.getOccupationType());
                return true;
            }
        }
        //职业类别
        if (!StrTool.compareString(vLDPersonSchema.getIDType(),
                                   vLCInsuredSchema.getIDType())) {
            System.out.println("证件类型:" + vLDPersonSchema.getIDType()
                               + ":" + vLCInsuredSchema.getIDType());
            return true;
        }

        if (!StrTool.compareString(vLDPersonSchema.getIDNo(),
                                   vLCInsuredSchema.getIDNo())) {
            System.out.println("证件号码:" + vLDPersonSchema.getIDNo()
                               + ":" + vLCInsuredSchema.getIDNo());
            return true;
        }

        else {
            if (vLCInsuredSchema.getOccupationType() != null) {
                return true;
            }
        }

        return false;
    }

    /**
     * YangMing：无名单处理
     * 添加被保险人时会判断和同类型是否是无
     * 名单类型就不需要重新计算保费，有后台
     * 处理。实际业务上也应该是不走险种信息
     * 直接按照团体保障计划的险种信息进行处
     * 理
     * @return boolean
     */
    public boolean dealProposal() {
        /**
         * 由于处理方式不完善，为减少因事务一致性而产生比较麻烦的问题
         * 暂时只在补名单时实现补人不进险种信息界面
         */
        String AppFlag = mLCGrpContDB.getAppFlag();
        if (AppFlag == null) {
            return true;
        }
        if (!AppFlag.equals("1")) {
            return true;
        }
        if (!getCongPlanInfo()) {
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "getCongPlanInfo";
            tError.errorMessage =
                    "在获取被保人保险计划信息时发生错误！请检查是否录入并选择保险计划。";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tRiskCode = "";
        //根据险种循环，分别计算每个险种的保费
        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
        LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema();
        LCBnfSet tLCBnfSet = new LCBnfSet();
        LCSpecSet tLCSpecSet = new LCSpecSet();
        TransferData tTransferData = new TransferData();
        LCInsuredRelatedSet mLCInsuredRelatedSet = new LCInsuredRelatedSet();
        LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
        tLCGrpAppntSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        tLCGrpAppntSchema.setCustomerNo(mLCContSchema.getAppntNo());
        tTransferData.setNameAndValue("getIntv", ""); //领取间隔（方式）
        tTransferData.setNameAndValue("GetDutyKind", "");
        tTransferData.setNameAndValue("samePersonFlag", "0");
        tTransferData.setNameAndValue("deleteAccNo", "0");
        tTransferData.setNameAndValue("ChangePlanFlag", "0");
        tTransferData.setNameAndValue("AddName", "1");

        for (int n = 1; n <= tLCContPlanRiskSet.size(); n++) {
            tRiskCode = tLCContPlanRiskSet.get(n).getRiskCode();
            //开始准备险种的责任数据。
            String strSql = "select PolNo from lcpol where contno='"
                            + this.OldContNo + "' and riskcode='"
                            + tRiskCode + "'";
            ExeSQL sql = new ExeSQL();
            String oldProposalNo = "";
            try {
                oldProposalNo = sql.getOneValue(strSql);
            } catch (Exception e) {
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "dealProposal";
                tError.errorMessage =
                        "查询险种号码出错，请确认是否在做补名单操作 ！";
                this.mErrors.addOneError(tError);
                return false;

            }
            System.out.println("开始准备" + tRiskCode + "的责任信息");
            LCContPlanDutyParamSet riskLCContPlanDutyParamSet = prepareDutyInfo(
                    tRiskCode);
            if (riskLCContPlanDutyParamSet == null) {
                return false;
            }
            LCContSchema planLCContSchema = new LCContSchema();
            planLCContSchema.setContNo(mLCInsuredSchema.getContNo());
            if (!planLCContSchema.getDB().getInfo()) {
                buildError("dealProposal", "报错！");
                return false;
            }
            LCPolSchema planLCPolSchema = preparePol(tRiskCode);
            planLCPolSchema.setMasterPolNo(oldProposalNo);
            System.out.println("处理不名单信息" + oldProposalNo);
            LCDutySet planLCDutySet = perpareDuty(tRiskCode);
            for (int i = 1; i <= planLCDutySet.size(); i++) {
                planLCDutySet.get(i).setContNo(mLCInsuredSchema.getContNo());
            }
            noNameData.clear();
            noNameData.add(planLCDutySet);
            noNameData.add(planLCContSchema);
            noNameData.add(planLCPolSchema);
            noNameData.add(tLCAppntSchema);
            noNameData.add(mLCInsuredSchema);
            noNameData.add(tLCGrpAppntSchema);
            noNameData.add(tLCBnfSet);
            noNameData.add(tLCSpecSet);
            noNameData.add(tTransferData);
            noNameData.add(this.mGlobalInput);
            noNameData.add(mLCInsuredRelatedSet);
            noNameData.add(tLCCustomerImpartSet);
            ProposalUI tProposalUI = new ProposalUI();

            if (this.mOperate.equals("DELETE||CONTINSURED")) {

            }
            if (!tProposalUI.submitData(noNameData, "INSERT||PROPOSAL")) {
                this.mErrors.copyAllErrors(tProposalUI.mErrors);
                return false;
            }
        }
        return true;
    }

    //yangming：获取保险计划
    private boolean getCongPlanInfo() {
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();

        tLCContPlanRiskDB.setGrpContNo(mLCInsuredSchema.getGrpContNo());
        tLCContPlanRiskDB.setContPlanCode(mLCInsuredSchema.getContPlanCode());
        tLCContPlanRiskDB.setPlanType("0");
        this.tLCContPlanRiskSet = tLCContPlanRiskDB.query();
        if (tLCContPlanRiskSet == null || tLCContPlanRiskSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "getCongPlanInfo";
            tError.errorMessage =
                    "查询保险计划时未找到该团体合同下存在" + mLCInsuredSchema.getContPlanCode()
                    + "保险计划。请检查！";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("团体合同"
                           + mLCInsuredSchema.getGrpContNo() + "下的"
                           + mLCInsuredSchema.getContPlanCode() + "计划,有"
                           + tLCContPlanRiskSet.size() + "个险种。");
        return true;
    }

    //准备险种责任
    private LCContPlanDutyParamSet prepareDutyInfo(String riskcode) {
        LCContPlanDutyParamDB tmpLCContPlanDutyParamDB = new
                LCContPlanDutyParamDB();
        tmpLCContPlanDutyParamDB.setGrpContNo(mLCInsuredSchema.getGrpContNo());
        tmpLCContPlanDutyParamDB.setRiskCode(riskcode);
        tmpLCContPlanDutyParamDB.setContPlanCode(mLCInsuredSchema.
                                                 getContPlanCode());
        tmpLCContPlanDutyParamDB.setPlanType("0");
        LCContPlanDutyParamSet reLCContPlanDutyParamSet =
                tmpLCContPlanDutyParamDB.query();
        if (reLCContPlanDutyParamSet != null &&
            reLCContPlanDutyParamSet.size() > 0) {
            return reLCContPlanDutyParamSet;
        } else {
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "iprepareDutyInfo";
            tError.errorMessage =
                    "查询险种责任时未找到该险种下的责任信息。请检查！";
            this.mErrors.addOneError(tError);
            return null;
        }
    }

    //处理lcpol
    private LCPolSchema preparePol(String RiskCode) {
        LCPolSchema tempLCPolSchema = new LCPolSchema();
        tempLCPolSchema.setPrtNo(mLCInsuredSchema.getPrtNo());
        tempLCPolSchema.setManageCom(mLCInsuredSchema.getManageCom());
        tempLCPolSchema.setSaleChnl(mLCContSchema.getSaleChnl());
        tempLCPolSchema.setSaleChnlDetail(mLCContSchema.getSaleChnlDetail());
        tempLCPolSchema.setAgentCom(mLCContSchema.getAgentCom());
        tempLCPolSchema.setAgentType(mLCContSchema.getAgentType());
        tempLCPolSchema.setAgentCode(mLCContSchema.getAgentCode());
        tempLCPolSchema.setAgentGroup(mLCContSchema.getAgentGroup());
        tempLCPolSchema.setHandler(mLCContSchema.getHandler());
        tempLCPolSchema.setAgentCode1(mLCContSchema.getAgentCode1());
        tempLCPolSchema.setPolTypeFlag(mLCContSchema.getPolType()); //保单类型标记
        tempLCPolSchema.setContNo(mLCContSchema.getContNo());
        tempLCPolSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        tempLCPolSchema.setFirstPayDate(mLCContSchema.getFirstPayDate());
        tempLCPolSchema.setRiskCode(RiskCode);
        tempLCPolSchema.setPayIntv(mLCContSchema.getPayIntv());
        tempLCPolSchema.setPayMode(mLCContSchema.getPayMode());
        tempLCPolSchema.setCValiDate(mLCContSchema.getCValiDate()); //保单生效日期
        tempLCPolSchema.setPayLocation(mLCContSchema.getPayLocation()); //收费方式
        return tempLCPolSchema;
    }

    //处理duty信息
    private LCDutySet perpareDuty(String RiskCode) {
        LCDutySet tLCDutySet = new LCDutySet();
        LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
        tLMRiskDutyDB.setRiskCode(RiskCode);
        LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
        for (int i = 1; i <= tLMRiskDutySet.size(); i++) {
            String DutyCode = tLMRiskDutySet.get(i).getDutyCode();
            LCContPlanDutyParamDB tLCContPlanDutyParamDB = new
                    LCContPlanDutyParamDB();
            tLCContPlanDutyParamDB.setGrpContNo(mLCInsuredSchema.getGrpContNo());
            tLCContPlanDutyParamDB.setRiskCode(RiskCode);
            tLCContPlanDutyParamDB.setContPlanCode(mLCInsuredSchema.
                    getContPlanCode());
            tLCContPlanDutyParamDB.setPlanType("0");
            tLCContPlanDutyParamDB.setDutyCode(DutyCode);
            LCContPlanDutyParamSet tLCContPlanDutyParamSet =
                    tLCContPlanDutyParamDB.query();
            LCDutySchema tempLCDutySchema = new LCDutySchema();
            if (tLCContPlanDutyParamSet.size() > 0) {
                tempLCDutySchema.setDutyCode(DutyCode);
                for (int n = 1; n <= tLCContPlanDutyParamSet.size(); n++) {
                    tempLCDutySchema.setV(tLCContPlanDutyParamSet.get(n).
                                          getCalFactor(),
                                          tLCContPlanDutyParamSet.get(n).
                                          getCalFactorValue());
                }
                tLCDutySet.add(tempLCDutySchema);
            }
        }
        return tLCDutySet;
    }

    public static void main(String[] args) {
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ContInsuredBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
