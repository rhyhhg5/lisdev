package com.sinosoft.lis.tb;

import com.sinosoft.lis.brieftb.BriefSingleContInputBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCDiseaseResultSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class TBCalculate {
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public double mPrem=0;//保费
	public double mAmnt=0;//保额
	
	public TBCalculateTable deal(VData tVData){
		System.out.println("开始试算业务");
		
		try{
			TBCalculateTable tTBCalculateTable = new TBCalculateTable();
			
			GlobalInput tGlobalInput = new GlobalInput();
			tGlobalInput.Operator = "001";
			tGlobalInput.ManageCom = "86110000";
			
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("ContType", "1");
			tTransferData.setNameAndValue("mMsgType", "WX0001");
			
			LCRiskDutyWrapSet tLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
			
			tVData.add(tGlobalInput);
			tVData.add(tTransferData);
		    tVData.add(tLCRiskDutyWrapSet);
			
			BriefSingleContInputBL bl = new BriefSingleContInputBL();
	        MMap tMMap3 = bl.getSubmitRiskMap(tVData, "INSERT||CONTINSURED");
	        CErrors tErrors = bl.mErrors;
	        
	        if(tErrors.getErrorCount() != 0){
	        	tTBCalculateTable.setErrorState("01");
	        	tTBCalculateTable.setErrorStateInfo(tErrors.getLastError());
	        }else{
	        	LCContSchema tLCContSchema = (LCContSchema)tMMap3.getObjectByObjectName("LCContSchema", 0);
		        LCPolSet cLCPolSet = (LCPolSet)tMMap3.getObjectByObjectName("LCPolSet", 0);
		        LCDutySet cLCDutySet = (LCDutySet)tMMap3.getObjectByObjectName("LCDutySet", 0);
		        mPrem =  tLCContSchema.getPrem();
		        mAmnt = tLCContSchema.getAmnt();
		        tTBCalculateTable.setErrorState("00");
	        	tTBCalculateTable.setErrorStateInfo("");
	        	tTBCalculateTable.setAmnt(mAmnt+"");
	        	tTBCalculateTable.setPrem(mPrem+"");
	        }
	        return tTBCalculateTable;
		}catch(Exception ex){
			return null;
		}
	}
	
	private static VData getVData() {
		
//		GlobalInput tGlobalInput = new GlobalInput();
//		tGlobalInput.Operator = "001";
//		tGlobalInput.ManageCom = "86110000";
		
		LCContSchema tLCContSchema = new LCContSchema();
		tLCContSchema.setPrtNo("SS000000001");
		tLCContSchema.setManageCom("86110000");
		tLCContSchema.setSaleChnl("01");
		tLCContSchema.setAgentCode("1101000001");
		tLCContSchema.setAgentCom("");
		tLCContSchema.setPolApplyDate("2014-08-28");
		tLCContSchema.setContType("1");
		tLCContSchema.setPolType("0");
		tLCContSchema.setCValiDate("2014-01-01");
		tLCContSchema.setCInValiDate("2015-01-01");
		
		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
		tLCInsuredSchema.setPrtNo("SS000000001");
		tLCInsuredSchema.setName("试算人");
		tLCInsuredSchema.setSex("0");
		tLCInsuredSchema.setBirthday("1985-05-05");
		tLCInsuredSchema.setIDType("4");
		tLCInsuredSchema.setIDNo("111111");
		tLCInsuredSchema.setOccupationCode("");
		tLCInsuredSchema.setOccupationType("1");
		
//		LCAppntSchema tLCAppntSchema = new LCAppntSchema();
		
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolSchema tLCPolSchema = new LCPolSchema();
		tLCPolSchema.setPrtNo("SS000000001");
		tLCPolSchema.setRiskCode("333501");
		tLCPolSchema.setAmnt("5000");
		tLCPolSchema.setPrem("100000");
		tLCPolSchema.setCopys("");
		tLCPolSchema.setMult("");
		tLCPolSchema.setInsuredAppAge("30");
		tLCPolSchema.setInsuYear("5");
		tLCPolSchema.setPayEndYear("5");
		tLCPolSchema.setInsuredSex("0");
		tLCPolSet.add(tLCPolSchema);
		
//		TransferData tTransferData = new TransferData();
//		tTransferData.setNameAndValue("ContType", "1");
//		tTransferData.setNameAndValue("mMsgType", "WX0001");
//		
//		LCRiskDutyWrapSet tLCRiskDutyWrapSet = new LCRiskDutyWrapSet();

		VData tVData = new VData();
//		tVData.add(tGlobalInput);
		tVData.add(tLCContSchema);
	    tVData.add(tLCInsuredSchema);
//	    tVData.add(tLCAppntSchema);
	    tVData.add(tLCPolSet);
//	    tVData.add(new LCDiseaseResultSet());	//告知信息
//	    tVData.add(new LCNationSet());	//抵达国家
//	    tVData.add(tTransferData);
//	    tVData.add(tLCRiskDutyWrapSet);

		return tVData;
	}
	public static void main(String args[]){
		TBCalculate tTBCalculate = new TBCalculate();
		VData tVData = getVData();
		TBCalculateTable tTBCalculateTable = tTBCalculate.deal(tVData);
	}
}
