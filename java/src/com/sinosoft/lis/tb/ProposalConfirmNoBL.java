package com.sinosoft.lis.tb;
//程序名称：ProposalConfirmNoBL.java
//程序功能：投保审核号录入
//创建日期：2007-11-16
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class ProposalConfirmNoBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	private VData mResult = new VData();
	
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput() ;
	
	/** 数据操作字符串 */
	private String mOperate ;
	
	/** 操作员 */
	private String mOperater ;
	
	/** 业务处理相关变量 */
	private LCUWConfirmInfoSchema mLCUWConfirmInfoSchema = new LCUWConfirmInfoSchema();

	public ProposalConfirmNoBL() 
	{
	}
	
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
    public boolean submitData(VData cInputData,String cOperate)
	{
        //将操作数据拷贝到本类中
	    this.mOperate = cOperate;
	    if (!getInputData(cInputData))
	    {
	         return false;
	    }
	    
        //数据校验
        if (!checkData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalConfirmNoBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败ProposalConfirmNoBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
	    
	    //进行业务处理
	    if (!dealData())
	    {
	          // @@错误处理
            CError tError = new CError();
	        tError.moduleName = "ProposalConfirmNoBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败ProposalConfirmNoBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	    }

	    if (!prepareOutputData())
	    {
	        return false;
	    }
	    
	    PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ProposalConfirmNoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
	    
	    mInputData = null;
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
		System.out.println("进行数据处理.....");
	    if (mOperate.equals("INSERT||MAIN"))
	    {
	    	LCUWConfirmInfoSchema tLCUWConfirmInfoSchema = new LCUWConfirmInfoSchema();
	    	tLCUWConfirmInfoSchema.setPrtNo(mLCUWConfirmInfoSchema.getPrtNo());
	    	tLCUWConfirmInfoSchema.setConfirmNo(mLCUWConfirmInfoSchema.getConfirmNo());
	        tLCUWConfirmInfoSchema.setConfirmOperator(mLCUWConfirmInfoSchema.getConfirmOperator());
	  	    tLCUWConfirmInfoSchema.setConfirmName(mLCUWConfirmInfoSchema.getConfirmName());
	    	tLCUWConfirmInfoSchema.setConfirmDate(mLCUWConfirmInfoSchema.getConfirmDate());
	    	tLCUWConfirmInfoSchema.setConfirmTime(mLCUWConfirmInfoSchema.getConfirmTime());
	    	tLCUWConfirmInfoSchema.setRemark(mLCUWConfirmInfoSchema.getRemark());
	    	tLCUWConfirmInfoSchema.setOperator(this.mOperater);
	    	tLCUWConfirmInfoSchema.setMakeDate(this.mCurrentDate);
	    	tLCUWConfirmInfoSchema.setMakeTime(this.mCurrentTime);
	    	tLCUWConfirmInfoSchema.setModifyDate(this.mCurrentDate);
	    	tLCUWConfirmInfoSchema.setModifyTime(this.mCurrentTime);
	        map.put(tLCUWConfirmInfoSchema, "INSERT"); //插入
	        return true;
	    }
	    if (mOperate.equals("UPDATE||MAIN"))
	    {
	    	LCUWConfirmInfoDB tLCUWConfirmInfoDB = new LCUWConfirmInfoDB();
	    	tLCUWConfirmInfoDB.setPrtNo(mLCUWConfirmInfoSchema.getPrtNo());
	    	LCUWConfirmInfoSchema tLCUWConfirmInfoSchema = new LCUWConfirmInfoSchema();
	    	if(tLCUWConfirmInfoDB.getInfo())//最好做个判断
	        {
	            tLCUWConfirmInfoSchema.setSchema(tLCUWConfirmInfoDB);
	            tLCUWConfirmInfoSchema.setPrtNo(mLCUWConfirmInfoSchema.getPrtNo());
		    	tLCUWConfirmInfoSchema.setConfirmNo(mLCUWConfirmInfoSchema.getConfirmNo());
		        tLCUWConfirmInfoSchema.setConfirmOperator(mLCUWConfirmInfoSchema.getConfirmOperator());
		  	    tLCUWConfirmInfoSchema.setConfirmName(mLCUWConfirmInfoSchema.getConfirmName());
		    	tLCUWConfirmInfoSchema.setConfirmDate(mLCUWConfirmInfoSchema.getConfirmDate());
		    	tLCUWConfirmInfoSchema.setConfirmTime(mLCUWConfirmInfoSchema.getConfirmTime());
		    	tLCUWConfirmInfoSchema.setRemark(mLCUWConfirmInfoSchema.getRemark());
		    	tLCUWConfirmInfoSchema.setOperator(this.mOperater);
		    	tLCUWConfirmInfoSchema.setModifyDate(mCurrentDate);
		    	tLCUWConfirmInfoSchema.setModifyTime(mCurrentTime);
	    	    map.put(tLCUWConfirmInfoSchema, "UPDATE");
	            return true;
	        }
	        else
	    	{
	            this.mErrors.copyAllErrors(tLCUWConfirmInfoDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "ProposalConfirmNoBL";
	            tError.functionName = "getInfo";
	            tError.errorMessage = "获取信息失败!";

	            this.mErrors.addOneError(tError);
	    		return false;
	    	}
	    }
	    if (this.mOperate.equals("DELETE||MAIN"))
	    {
	        map.put(mLCUWConfirmInfoSchema, "DELETE"); //删除
	    }

	    return true;
	}
	
	 /**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	     this.mLCUWConfirmInfoSchema = (LCUWConfirmInfoSchema)cInputData.getObjectByObjectName("LCUWConfirmInfoSchema",0);
	     this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
	     this.mOperater = this.mGlobalInput.Operator;
	     return true;
	}
	 
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLCUWConfirmInfoSchema);
            mInputData.add(this.map);
            mResult.clear();
            mResult.add(this.mLCUWConfirmInfoSchema);
        }
		catch(Exception ex)
		{
	 		// @@错误处理
			CError tError = new CError();
	 		tError.moduleName = "ProposalConfirmNoBL";
	 		tError.functionName = "prepareOutputData";
	 		tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	 		this.mErrors .addOneError(tError) ;
			return false;
		}
		return true;
    }
    
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() 
    {
        System.out.println("进行校验.....");
        if (mLCUWConfirmInfoSchema == null) {
        	System.out.println("mLCUWConfirmInfoSchema == null");
            return false;
        }
        if (mGlobalInput == null) {
        	System.out.println("mGlobalInput == null");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
	}
}
