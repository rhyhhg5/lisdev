/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;


import com.sinosoft.lis.db.LCGrpPayPlanDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpPayPlanDetailSchema;
import com.sinosoft.lis.schema.LCGrpPayPlanSchema;
import com.sinosoft.lis.vschema.LCGrpPayPlanDetailSet;
import com.sinosoft.lis.vschema.LCGrpPayPlanSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理回收操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */

public class GrpPayPlanDetailBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /* 业务相关的数据 */
    private MMap mMMap = null;
    private GlobalInput globalInput = new GlobalInput();
    private String mProposalGrpContNo = "";
    private String mPrtNo = "";
    private TransferData mTransferData = null;

    // 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。

    public GrpPayPlanDetailBL()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {

        if (!getInputData(cInputData))
        {
        	return false;
        }
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData(VData vData)
    {
    	
    	globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) vData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mProposalGrpContNo = (String) mTransferData.getValueByName("ProposalGrpContNo");
        if (mProposalGrpContNo == null || "".equals(mProposalGrpContNo))
        {
            buildError("getInputData", "获取投保单号码失败。");
            return false;
        }
        
        String tPrtNoSql = "select prtno from lcgrpcont where ProposalGrpContNo = '"+mProposalGrpContNo+"' "; 
  	  	mPrtNo = new ExeSQL().getOneValue(tPrtNoSql);
  	    if(mPrtNo == null || "".equals(mPrtNo)){
	  		buildError("getInputData", "获取保单印刷号码失败。");
	        return false;
  	    }
  	    
        return true;
    }
    
//  根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
    	mMMap = new MMap();
    	
    	String tContPlanCodeSql = "select contplancode from lccontplan where ProposalGrpContNo = '"+mProposalGrpContNo+"' and contplancode !='11' ";
    	SSRS tContPlanCodeSSRS = new ExeSQL().execSQL(tContPlanCodeSql);
    	if(tContPlanCodeSSRS == null || tContPlanCodeSSRS.MaxRow<=0){
    		buildError("dealData", "获取保单保障计划失败。");
	        return false;
    	}
    	LCGrpPayPlanDetailSet tLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet();
    	for(int m=1;m<=tContPlanCodeSSRS.MaxRow;m++){
    		LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();
        	LCGrpPayPlanDB tLCGrpPayPlanDB = new LCGrpPayPlanDB();
    		String tContPlanCode = tContPlanCodeSSRS.GetText(m, 1);
    		String tSQL = "select * from LCGrpPayPlan where ProposalGrpContNo = '"+mProposalGrpContNo+"' and contplancode = '"+tContPlanCode+"'  ";
        	tLCGrpPayPlanSet = tLCGrpPayPlanDB.executeQuery(tSQL);
        	if(tLCGrpPayPlanSet.size()<=0){
        		buildError("dealData", "获取保单约定缴费计划失败。");
    	        return false;
        	}
        	String tPolSQL = "select riskcode,contplancode,sum(prem) from lcpol where prtno = '"+mPrtNo+"' and contplancode = '"+tContPlanCode+"' group by contplancode,riskcode  ";
        	SSRS tPolSSRS = new ExeSQL().execSQL(tPolSQL);
        	if(tPolSSRS == null || tPolSSRS.MaxRow<=0){
        		buildError("dealData", "获取保单险种信息失败。");
    	        return false;
        	}
        	
        	for(int i=1;i<=tLCGrpPayPlanSet.size();i++){
        		LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
    			tLCGrpPayPlanSchema = tLCGrpPayPlanSet.get(i);
    			double tSumPrem = 0;
    			for(int j=1;j<=tPolSSRS.MaxRow;j++){
    				String tRiskCode = tPolSSRS.GetText(j, 1);
//        			if(!tLCGrpPayPlanSchema.getContPlanCode().equals(tPolSSRS.GetText(j, 2))){
//        				continue;
//        			}
        			LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema = new LCGrpPayPlanDetailSchema();
        			if("1".equals(tLCGrpPayPlanSchema.getPlanCode())){
                		tLCGrpPayPlanDetailSchema.setState("1");
        			}else{
                		tLCGrpPayPlanDetailSchema.setState("2");
        			}
        			double tNewPrem = getPrem(tLCGrpPayPlanSchema,tPolSSRS,tRiskCode);
        			if(j==tPolSSRS.MaxRow){
        				tNewPrem = Arith.sub(tLCGrpPayPlanSchema.getPrem(), tSumPrem);
        			}
        			tSumPrem = Arith.add(tSumPrem,tNewPrem);
            		tLCGrpPayPlanDetailSchema.setPrem(tNewPrem);
        			tLCGrpPayPlanDetailSchema.setRiskCode(tRiskCode);
            		tLCGrpPayPlanDetailSchema.setPrtNo(mPrtNo);
            		tLCGrpPayPlanDetailSchema.setProposalGrpContNo(mProposalGrpContNo);
            		tLCGrpPayPlanDetailSchema.setPlanCode(tLCGrpPayPlanSchema.getPlanCode());
            		tLCGrpPayPlanDetailSchema.setPaytoDate(tLCGrpPayPlanSchema.getPaytoDate());
            		tLCGrpPayPlanDetailSchema.setContPlanCode(tLCGrpPayPlanSchema.getContPlanCode());
            		tLCGrpPayPlanDetailSchema.setOperator(globalInput.Operator);
            		tLCGrpPayPlanDetailSchema.setMakeDate(PubFun.getCurrentDate());
            		tLCGrpPayPlanDetailSchema.setMakeTime(PubFun.getCurrentTime());
            		tLCGrpPayPlanDetailSchema.setModifyDate(PubFun.getCurrentDate());
            		tLCGrpPayPlanDetailSchema.setModifyTime(PubFun.getCurrentTime());
            		tLCGrpPayPlanDetailSet.add(tLCGrpPayPlanDetailSchema);
            	}
        	}
    	}
    	mMMap.put(tLCGrpPayPlanDetailSet, SysConst.DELETE_AND_INSERT);
    	//submit(mMMap);
    	return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertifyTakeBackUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    private double getPrem(LCGrpPayPlanSchema aLCGrpPayPlanSchema,SSRS aSSRS,String aRiskCode){
    	double aSumPrem = 0;//某一保障计划下的总保费
    	double tPrem = 0;//对应该险种的保费
    	for(int a=1;a<=aSSRS.MaxRow;a++){
    		if(aLCGrpPayPlanSchema.getContPlanCode().equals(aSSRS.GetText(a, 2))){
    			aSumPrem +=Double.parseDouble(aSSRS.GetText(a, 3));
    			if(aRiskCode.equals(aSSRS.GetText(a, 1))){
    				tPrem += Double.parseDouble(aSSRS.GetText(a, 3));
    			}
    		}
    	}
    	double aNewPrem = Arith.round(Arith.div(Arith.mul(aLCGrpPayPlanSchema.getPrem(),tPrem), aSumPrem), 2);
    	return aNewPrem;
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap aMMap)
    {
        VData data = new VData();
        data.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
    
    public static void main(String[] args){
    	GrpPayPlanDetailBL tGrpPayPlanDetailBL = new GrpPayPlanDetailBL();
    	
    	VData tVData = new VData();
    	
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.Operator = "001";
    	tGlobalInput.ManageCom = "86110000";
    	
    	TransferData mTransferData = new TransferData();
    	mTransferData.setNameAndValue("ProposalGrpContNo", "1400305381");
    	
    	tVData.add(tGlobalInput);
    	tVData.add(mTransferData);
    	
    	tGrpPayPlanDetailBL.submitData(tVData, "");
    	
    }

	public MMap getMMMap() {
		return mMMap;
	}
}
