/**
 * 2012-3-7
 */
package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ClaimBankParseGuideInUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public ClaimBankParseGuideInUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            ClaimBankParseGuideIn tClaimBankParseGuideIn = new ClaimBankParseGuideIn();
            if (!tClaimBankParseGuideIn.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tClaimBankParseGuideIn.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
