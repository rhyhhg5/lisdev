/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 保障计划UI层</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author hxl
 * @version 1.0
 */
public class AskLCContPlanUI {
     /** 往前面传输数据的容器 */
     private VData mResult = new VData();
     /** 数据操作字符串 */
     private String mOperate;
     /** 错误处理类 */
     public CErrors mErrors = new CErrors();

     public AskLCContPlanUI() {
     }
    /**
   * 不执行任何操作，只传递数据给下一层
   * @param cInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData( VData cInputData, String cOperate ){
    // 数据操作字符串拷贝到本类中
    this.mOperate = cOperate;
    AskLCContPlanBL tAskLCContPlanBL = new AskLCContPlanBL();
    if( !tAskLCContPlanBL.submitData( cInputData, mOperate )){
      // @@错误处理
      this.mErrors.copyAllErrors( tAskLCContPlanBL.mErrors );
      return false;
    }else{
      mResult = tAskLCContPlanBL.getResult();
    }
    return true;
  }
  /**
   * 获取从BL层取得的结果
   * @return VData
   */
  public VData getResult(){
        return mResult;
  }
}
