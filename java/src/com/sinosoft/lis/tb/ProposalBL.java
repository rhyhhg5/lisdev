/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import java.text.DecimalFormat;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.PubCertifyTakeBack;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YT
 * @version 1.0
 */
public class ProposalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 数据操作字符串 */
    private String mOperate;

    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();

    /** 投保单类型 */
    private String mAppntType = "1"; // 默认为个人投保人

    private String mPolType = "1"; // 默认为个人投保单类型

    private boolean TakeBackCertifyFalg = false; //是否回收单证标记

    private String OriginOperator = ""; //更新时保存原先保单的操作员

    private double OriginPrem = 0; //更新时保存原先保单的实际保费

    private double OriginStandPrem = 0; //更新时保存原先保单的标准保费

    /* 处理浮动费率的变量 */
    private double FloatRate = 0; //浮动费率

    private double InputPrem = 0; //保存界面录入的保费

    private double InputAmnt = 0; //保存界面录入的保额

    private boolean autoCalFloatRateFlag = false; //是否自动计算浮动费率,如果界面传入浮动费率=ConstRate，自动计算

    private final double ConstRate = -1; //如果标明需要从输入的保费保额计算浮动费率，那么界面传入的浮动费率值为常量

    /*转换精确位数的对象   */
    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数

    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

    private String FRateFORMATMODOL = "0.000000000000"; //浮动费率计算出来后的精确位数

    private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); //数字转换对象

    /* 不需要保费保额计算的标记，用于特殊的险种。譬如：众悦年金分红 */
    private boolean noCalFlag = false;

    private boolean deleteAccNo = false;

    //保存投保单对应的操作类型,默认是投保单标记(其他可能是保全类型)
    private String mSavePolType = "0";

    //是否文件投保标记
    boolean mGrpImportFlag = false;

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();

    private String theCurrentTime = PubFun.getCurrentTime();

    /** 业务处理相关变量 */
    private String tSaleChnlDetail = "";

    /** 接受前台传输数据的容器 */
    private TransferData mTransferData = new TransferData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /**合同表*/
    private LCContSchema mLCContSchema = new LCContSchema();

    /** 个人客户表 */
    private LDPersonSet mLDPersonSet = new LDPersonSet();

    /** 银行账户表 */
    private LCBankAccBL mLCBankAccBL = new LCBankAccBL();

    /** 银行授权书表 */
    private LCBankAuthBL mLCBankAuthBL = new LCBankAuthBL();

    /** 保单 */
    private LCPolBL mLCPolBL = new LCPolBL();

    //主险保单
    private LCPolBL mainLCPolBL = new LCPolBL();

    //private LCGrpPolBL mLCGrpPolBL = new LCGrpPolBL();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); //磁盘投保时，该集体信息从外部传入，否则内部查询

    private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema(); //磁盘投保时，该集体信息从外部传入，否则内部查询

    /**险种承保描述表 */
    private LMRiskAppSchema mLMRiskAppSchema = new LMRiskAppSchema();

    /**险种描述表 */
    private LMRiskSchema mLMRiskSchema = new LMRiskSchema();

    private LDGrpSchema mLDGrpSchema = new LDGrpSchema();

    /** 投保人 */
    private LCAppntBL mLCAppntBL = new LCAppntBL();

    private LCGrpAppntBL mLCGrpAppntBL = new LCGrpAppntBL();

    /** 被保人 */
    private LCInsuredBL mLCInsuredBL = new LCInsuredBL();

    private LCInsuredBLSet mLCInsuredBLSet = new LCInsuredBLSet();

    private LCInsuredBLSet mLCInsuredBLSetNew = new LCInsuredBLSet();

    /** 连带，连生被保人 */
    private LCInsuredRelatedSet mLCInsuredRelatedSet = new LCInsuredRelatedSet();

    private LCInsuredRelatedSet newLCInsuredRelatedSet = new LCInsuredRelatedSet();

    /** 受益人 */
    private LCBnfBLSet mLCBnfBLSet = new LCBnfBLSet();

    private LCBnfBLSet mLCBnfBLSetNew = new LCBnfBLSet();

    /** 告知信息 */
    private LCCustomerImpartBLSet mLCCustomerImpartBLSet = new LCCustomerImpartBLSet();

    private LCCustomerImpartBLSet mLCCustomerImpartBLSetNew = new LCCustomerImpartBLSet();

    /** 特别约定 */
    private LCSpecBLSet mLCSpecBLSet = new LCSpecBLSet();

    private LCSpecSet mLCSpecBLSetNew = new LCSpecBLSet();

    /** 保费项表*/
    private LCPremBLSet mLCPremBLSet = new LCPremBLSet();

    private LCPremBLSet mDiskLCPremBLSet = new LCPremBLSet(); //保存特殊的保费项数据(目前针对磁盘投保，不用计算保费保额类型)

    /** 给付项表*/
    private LCGetBLSet mLCGetBLSet = new LCGetBLSet();

    private LCGetSet mDiskLCGetSet = new LCGetSet();

    //一般的责任信息
    private LCDutyBL mLCDutyBL = new LCDutyBL();

    /** 责任表*/
    private LCDutyBLSet mLCDutyBLSet = new LCDutyBLSet();

    /** 可选责任信息 */
    private LCDutyBLSet mLCDutyBLSet1 = new LCDutyBLSet();

    private boolean mNeedDuty = false;

    /** 应收总表 **/
    LJSPaySet mLJSPaySet = new LJSPaySet();

    private String mChangePlanFlag = "1";

    private String EdorType;
    
    private String EdorNo ;

    /** 核保信息 */
    private LCUWMasterSchema tLCUWMasterSchema;

    private LCUWSubSchema tLCUWSubSchema;

    /** 添加要素 */
    private String mRetireRate;

    private String mAvgAge;

    private String mInsuredNum;

    private String mIndustry;

    public ProposalBL()
    {
    }

    public static void main(String[] args)
    {
        ProposalBL ProposalBL1 = new ProposalBL();
        String a = "13212";
        System.out.println(a.indexOf("null"));
        //        LCPolSchema mLCPolSchema =new LCPolSchema();
        //        LCSpecSchema mLCSpecSchema=new LCSpecSchema();
        GlobalInput mGlobalInput = new GlobalInput();

        //        TransferData mTransferData = new TransferData();
        //        /** 被保人 */
        //        LCInsuredSet mLCInsuredSet = new LCInsuredSet();
        //        LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
        //        mLCInsuredSchema.setPolNo("11");
        //        mLCInsuredSchema.setName("hzm");
        //        mLCInsuredSchema.setSex("0");
        //        mLCInsuredSchema.setBirthday("1978-1-1");
        //        mLCInsuredSchema.setModifyDate("2002-1-1");
        //        mLCInsuredSchema.setModifyTime("10:10:10");
        //        mLCInsuredSet.add(mLCInsuredSchema);
        //        /** 投保人 */
        //        LCAppntSchema mLCAppntSchema = new LCAppntSchema();
        //        LCAppntGrpSchema mLCAppntGrpSchema = new LCAppntGrpSchema();
        //        /** 保险责任表 */
        //        LCDutySchema mLCDutySchema = new LCDutySchema();
        //        mLCDutySchema.setPolNo("11");
        //        mLCDutySchema.setDutyCode("001");
        //        mLCDutySchema.setMult("1");
        //        mLCDutySchema.setStandPrem(100);
        //        mLCDutySchema.setPrem(100);
        //        mLCDutySchema.setAmnt(100);
        //        mLCDutySchema.setFirstPayDate("2002-1-1");
        //        mLCDutySchema.setPaytoDate("2002-2-1");
        //        mLCDutySchema.setPayIntv(12);
        //
        //        mLCDutySchema.setGetYearFlag("Y");
        //        mLCDutySchema.setGetYear(1);
        //        mLCDutySchema.setInsuYearFlag("M");
        //        mLCDutySchema.setInsuYear(1);
        //        mLCDutySchema.setGetStartDate("1");
        //        mLCDutySchema.setPayEndYearFlag("Y");
        //        mLCDutySchema.setPayEndDate("2008-1-1");
        //        mLCDutySchema.setGetStartDate("2003-1-1");
        //
        //        mLCDutySchema.setModifyDate("2002-1-1");
        //        mLCDutySchema.setModifyTime("10:10:10");

        /** 全局变量 */
        mGlobalInput.Operator = "endor0";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";

        //        /** 传递变量 */
        //        mTransferData.setNameAndValue("getIntv","1");
        //        mTransferData.setNameAndValue("samePersonFlag","1");
        //        /** 客户告知 */
        //        LCCustomerImpartSet mLCCustomerImpartSet=new LCCustomerImpartSet();
        //        LCCustomerImpartSchema mLCCustomerImpartSchema=new LCCustomerImpartSchema();
        //        mLCCustomerImpartSchema.setPolNo("11");
        //        mLCCustomerImpartSchema.setImpartCode("11");
        //        mLCCustomerImpartSchema.setImpartVer("001") ;
        //        mLCCustomerImpartSchema.setCustomerNo("11");
        //        mLCCustomerImpartSchema.setCustomerNoType("001") ;
        //        mLCCustomerImpartSchema.setUWClaimFlg("1") ;
        //        mLCCustomerImpartSchema.setPrtFlag("2") ;
        //        mLCCustomerImpartSchema.setModifyDate("01/01/01") ;
        //        mLCCustomerImpartSchema.setModifyTime("01:01:01") ;
        //        mLCCustomerImpartSet.add(mLCCustomerImpartSchema) ;
        //        /** 个人保单表 */
        //        mLCPolSchema.setPolNo("86110020030110023595");
        //        mLCPolSchema.setProposalNo("86110020030110023595");
        //        mLCPolSchema.setGrpPolNo("");
        //        mLCPolSchema.setPrtNo("");
        //        mLCPolSchema.setPolNo("1111") ;
        //        mLCPolSchema.setAgentCode("01-01-01");
        //        mLCPolSchema.setManageCom("1012");
        //        mLCPolSchema.setPayLocation("1");
        //        mLCPolSchema.setRiskCode("211601");
        //        mLCSpecSchema.setMakeDate("01-01-01") ;
        //
        //        VData tv=new VData();
        //        tv.add(mLCPolSchema);
        //        tv.add(mLCSpecSchema);
        //        tv.add(mLCCustomerImpartSet);
        //        tv.add(mGlobalInput);
        //        tv.add(mTransferData);
        //        tv.add(mLCDutySchema);
        //        tv.add(mLCAppntSchema);
        //        tv.add(mLCAppntGrpSchema);
        //        tv.add(mLCInsuredSet);
        //ProposalBL1.submitData(tv,"INSERT||PROPOSAL");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo("00002581101");
        tLCContDB.getInfo();

        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(tLCContDB.getContNo());
        tLCAppntDB.getInfo();

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo("11000095546");
        tLCPolDB.getInfo();
        tLCPolDB.setCValiDate("2006-06-16");

        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(tLCPolDB.getPolNo());

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(tLCContDB.getContNo());
        tLCInsuredDB.setInsuredNo("000025811");
        tLCInsuredDB.getInfo();

        TransferData tTransferData = new TransferData();
        //        tTransferData.setNameAndValue("getIntv","1"); //领取间隔（方式）
        //        tTransferData.setNameAndValue("GetDutyKind", ""); //年金开始领取年龄
        //        tTransferData.setNameAndValue("samePersonFlag", "0"); //投保人同被保人标志
        //        tTransferData.setNameAndValue("deleteAccNo", "0");
        //        tTransferData.setNameAndValue("ChangePlanFlag", "0");
        tTransferData.setNameAndValue("EdorType", "NS");
        tTransferData.setNameAndValue("SavePolType", "2");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.addElement(tLCPolDB.getSchema());
        tVData.add(tLCDutyDB.query());
        tVData.addElement(mGlobalInput);
        tVData.add(tTransferData);
        tVData.add(tLCContDB.getSchema());
        tVData.add(tLCAppntDB.getSchema());

        //        tVData.addElement(tLCGrpAppntSchema);
        tVData.addElement(tLCInsuredDB.getSchema());
        tVData.addElement(new LCInsuredRelatedSet());

        if (!ProposalBL1.submitData(tVData, "UPDATE||PROPOSAL"))
        {
            System.out.println(ProposalBL1.mErrors.getErrContent());
        }
    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (getInputData(cInputData) == false)
        {
            return false;
        }

        //数据操作校验
        if (checkData() == false)
        {
            return false;
        }

        //特殊校验处理针对险种－对民生用，可单独屏蔽不用,不影响后面
        if (preSpecCheck() == false)
        {
            return false;
        }

        //进行业务处理
        if (dealData() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "";
            this.mErrors.addOneError(tError);

            return false;
        }

        if (!this.mOperate.equals("DELETE||PROPOSAL"))
        {
            if (this.mOperate.equals("INSERT||PROPOSAL"))
            {
                if (CheckTBField("INSERT") == false)
                {
                    return false;
                }
            }
            else
            {
                if (CheckTBField("UPDATE") == false)
                {
                    return false;
                }
            }
        }

        //准备往后台的数据
        if (prepareOutputData() == false)
        {
            return false;
        }

        ProposalBLS tProposalBLS = new ProposalBLS();
        tProposalBLS.submitData(mInputData, cOperate);

        //如果有需要处理的错误，则返回
        if (tProposalBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tProposalBLS.mErrors);

            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //System.out.println("TakeBackCertifyFalg:"+TakeBackCertifyFalg);
        if (TakeBackCertifyFalg)
        { //如果回收单证标记为真(针对个人投保单)
            try
            {
                //单证回收
                PubCertifyTakeBack tPubCertifyTakeBack = new PubCertifyTakeBack();
                if (tPubCertifyTakeBack
                        .CheckNewType(tPubCertifyTakeBack.CERTIFY_CheckNo2))
                { //如果需要单证回收
                    String operator = mGlobalInput.Operator;
                    String CertifyType = "";

                    //System.out.println("getPrtNo:"+mLCPolBL.getPrtNo());
                    if (mLCPolBL.getPrtNo().substring(2, 4).equals("15"))
                    { //银行险的编码15，个险11，团险12
                        CertifyType = tPubCertifyTakeBack.CERTIFY_ProposalBank;
                    }
                    else
                    {
                        if (mLCPolBL.getPrtNo().substring(2, 4).equals("14"))
                        {
                            CertifyType = tPubCertifyTakeBack.CERTIFY_ProSpec;
                        }
                        else if (mLCPolBL.getPrtNo().substring(2, 4).equals(
                                "16"))
                        {
                            CertifyType = tPubCertifyTakeBack.CERTIFY_ProSimple;
                        }
                        else
                        {
                            CertifyType = tPubCertifyTakeBack.CERTIFY_Proposal;
                        }
                    }

                    //System.out.println("CertifyType:"+CertifyType);
                    if (!tPubCertifyTakeBack.CertifyTakeBack_A(mLCPolBL
                            .getPrtNo(), mLCPolBL.getPrtNo(), CertifyType,
                            operator))
                    {
                        System.out.println("单证回收错误（个人投保单）:"
                                + mLCPolBL.getPrtNo());
                        System.out.println("错误原因："
                                + tPubCertifyTakeBack.mErrors.getFirstError()
                                        .toString());

                        //保存错误信息
                    }
                }
            }
            catch (Exception e)
            {
                mErrors.addOneError(e.toString());
            }
        }
        mInputData = null;

        return true;
    }

    /**
     * 不包含校验的承保数据处理，不保存数据
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return VData
     */
    public VData appNoChk(VData cInputData, String cOperate)
    {
        VData tReturn = null;

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (getInputData(cInputData) == false)
        {
            return null;
        }

        //进行业务处理
        if (dealData() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "";
            this.mErrors.addOneError(tError);

            return null;
        }

        //准备往后台的数据
        if (prepareOutputData() == false)
        {
            return null;
        }

        // 检查是否出错
        if (this.mErrors.needDealError() == false)
        {
            tReturn = mInputData;
        }

        return tReturn;
    }

    /**
     * 数据处理校验 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean checkData()
    {

        // 判断投保单的类型 1--个人 2--集体 3--合同 //PQ 应该没有mPolType='3'的情况
        if (!mLCPolBL.getContType().equals("1"))
        {
            mPolType = "2";
            mAppntType = "2";
        }

        if (mOperate.equals("INSERT||PROPOSAL")
                || mOperate.equals("UPDATE||PROPOSAL"))
        {
            if (mLCPolBL.getPolTypeFlag() == null)
            { //保单类型标记0--个人单，1--无名单，2 ---公共帐户)
                mLCPolBL.setPolTypeFlag("0"); //缺省设为个人单
            }
            if (mLCPolBL.getAppFlag() == null
                    || mLCPolBL.getAppFlag().equals(""))
            { //签单标记为空时，补为0
                String tSavePolType = (String) mTransferData
                        .getValueByName("SavePolType");
                if (tSavePolType != null)
                {
                    if (tSavePolType.trim().equals(""))
                    {
                        mLCPolBL.setAppFlag("0");
                    }
                    else
                    {
                        mLCPolBL.setAppFlag(tSavePolType);
                    }
                }
                else
                {
                    mLCPolBL.setAppFlag("0");
                }
                mLCPolBL.setStateFlag("0");

            }
            if (mPolType.equals("2"))
            {
                if (mLCGrpContSchema == null)
                {
                    //如果是团单，从外界传入的险种承保描述信息和险种描述信息为空，那么从数据库查询
                    //LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
                    LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                    tLCGrpContDB.setGrpContNo(mLCPolBL.getGrpContNo());

                    if (tLCGrpContDB.getInfo() == false)
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "dealDataPerson";
                        tError.errorMessage = "集体(投)合同表取数失败";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    mLCGrpContSchema = tLCGrpContDB.getSchema();
                    // mLCGrpContSchema.setSchema(tLCGrpContDB);

                }
                if (mLCGrpPolSchema == null)
                {
                    //如果是团单，从外界传入的险种承保描述信息和险种描述信息为空，那么从数据库查询
                    //LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
                    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
//                  tLCGrpPolDB.setGrpContNo(mLCPolBL.getGrpContNo());
//                  tLCGrpPolDB.setRiskCode(mLCPolBL.getRiskCode());

                  LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery("select * from LCGrpPol where GrpContNo = '"+mLCPolBL.getGrpContNo()+"' "
                  																								+ " and RiskCode = '"+mLCPolBL.getRiskCode()+"' with ur");
 
                    if (tLCGrpPolDB.mErrors.needDealError()
                            || tLCGrpPolSet.size() == 0)
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "dealDataPerson";
                        tError.errorMessage = "集体险种表取数失败";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    mLCGrpPolSchema = tLCGrpPolSet.get(1).getSchema();
                }
                mLCPolBL.setGrpPolNo(mLCGrpPolSchema.getGrpPolNo());
                mLCPolBL.setAgentCode(mLCContSchema.getAgentCode());
                mLCPolBL.setAgentGroup(mLCContSchema.getAgentGroup());
                mLCPolBL.setAgentCom(mLCContSchema.getAgentCom());
            }

            //如果从外界传入的险种承保描述信息和险种描述信息为空，那么从数据库查询
            if (mLMRiskAppSchema == null)
            {
                LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
                tLMRiskAppDB.setRiskCode(mLCPolBL.getRiskCode());
                if (tLMRiskAppDB.getInfo() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLMRiskAppDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "LMRiskApp表查询失败!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                mLMRiskAppSchema = tLMRiskAppDB.getSchema();
            }
            if (mLMRiskSchema == null)
            {
                LMRiskDB tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(mLCPolBL.getRiskCode());
                if (tLMRiskDB.getInfo() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLMRiskDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "LMRiskApp表查询失败!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                mLMRiskSchema = tLMRiskDB.getSchema();
            }

            //如果是个单校验代理人编码并置上代理人组别
            if (mPolType.equals("1"))
            {
                if ((mLCPolBL.getSaleChnl() == null)
                        || mLCPolBL.getSaleChnl().equals(""))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "在投保数据接受时没有得到足够的数据，请您确认有：销售渠道编码!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                if ((mLCPolBL.getAgentCode() == null)
                        || mLCPolBL.getAgentCode().equals(""))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "在投保数据接受时没有得到足够的数据，请您确认有：代理人编码!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                else
                {
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(mLCPolBL.getAgentCode());
                    if (tLAAgentDB.getInfo() == false)
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "在投保数据接受时没有得到正确的数据，请您确认：代理人编码没有输入错误!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if ((tLAAgentDB.getManageCom() == null)
                            || !tLAAgentDB.getManageCom().equals(
                                    mLCPolBL.getManageCom()))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "代理人编码对应数据库中的管理机构为空或者和您录入的管理机构不符合！";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    mLCPolBL.setAgentGroup(tLAAgentDB.getAgentGroup());
                }
            }
        }

        if (mOperate.equals("INSERT||PROPOSAL"))
        {

            // 只校验主险
            if (mLMRiskAppSchema.getSubRiskFlag().equals("M"))
            {
                //如果是个人投保单且是主险，则将单证回收的标记为真，用于保存投保单信息后的单证回收模块
                TakeBackCertifyFalg = true;

                String sql = "";
                sql = "select * from LCPol" + " where PrtNo = '"
                        + mLCPolBL.getPrtNo() + "' " + " and RiskCode='"
                        + mLMRiskAppSchema.getRiskCode() + "'"
                        + " and InsuredNo='" + mLCInsuredBL.getInsuredNo()
                        + "' with ur";
                //PQ 印刷号可以有多个主险
                //一个合同下一个被保险人同一个险种只能录入一个
                System.out.println("adsfasdfasd" + sql);
                LCPolDB tLCPolDB = new LCPolDB();
                LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
                if (tLCPolDB.mErrors.needDealError() == true)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "LCPol表查询失败!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                if (tLCPolSet.size() > 0)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "该投保单下该被保险人已经录入过该险种！";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
            // end of if
            else
            {
                //如果是附加险，校验
                //  (1)如果当前要保存的附加险对应的主险下已经存在相同附加险的纪录，那么提示：已经存在不许录入。
                //  (2)如果要保存的附加险的被保人和主险不同，那么不许保存--暂时不做
                //因为保全要做特殊处理
                if (mSavePolType.equals("0"))
                {
                    LCPolDB tempLCpolDB = new LCPolDB();

                    if (mLCPolBL.getMainPolNo() == null
                            || mLCPolBL.getMainPolNo().equals(""))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "没有传入险种" + mLCPolBL.getRiskCode()
                                + "的主险信息";
                        mErrors.addOneError(tError);
                        System.out.println(tError.errorMessage);
                        return false;
                    }

//                  tempLCpolDB.setMainPolNo(mLCPolBL.getMainPolNo());
//                  tempLCpolDB.setRiskCode(mLCPolBL.getRiskCode());

                  LCPolSet tempLCPolSet = new LCPolSet();
                  tempLCPolSet = tempLCpolDB.executeQuery("select * from LCPol where MainPolNo ='"+mLCPolBL.getMainPolNo()+"' and RiskCode ='"+mLCPolBL.getRiskCode()+"' with ur");
 
                    if ((tempLCPolSet != null) && (tempLCPolSet.size() > 0))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "该投保单的主险下已经存在相同附加险的投保单！";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                }
            }

            //检验生效日期是否小于开办日期
            if (mLMRiskAppSchema.getStartDate() == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "该投保单的险种对应的开办日期在险种描述中不存在！";
                this.mErrors.addOneError(tError);

                return false;
            }
            else
            {
                if (PubFun.calInterval(mLMRiskAppSchema.getStartDate(),
                        mLCPolBL.getCValiDate(), "D") < 0)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "该投保单录入的生效日期小于该险种的开办日期，请确认！";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }

            //检验生效日期是否大于停办日期
            if ((EdorType == null) || EdorType.trim().equals(""))
            {
                if (mLMRiskAppSchema.getEndDate() != null)
                {
                    if (!mLMRiskAppSchema.getEndDate().trim().equals(""))
                    {
                        if (PubFun.calInterval(mLMRiskAppSchema.getEndDate(),
                                mLCPolBL.getCValiDate(), "D") > 0)
                        {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkData";
                            tError.errorMessage = "该投保单录入的生效日期超过该险种的停办日期，请确认！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                }
            }
            //检查投保人录入的数据是否正确
            //                if (checkLCAppnt() == false)
            //                {
            //                    return false;
            //                }
        }

        //检查被保人录入的数据是否正确
        if (checkLCInsured() == false)
        {
            return false;
        }
        // end of if

        if (mOperate.equals("UPDATE||PROPOSAL"))
        {
            String tProposalNo = mLCPolBL.getProposalNo();
            LCPolSchema tLCPolSchema = new LCPolSchema();
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tProposalNo);
            if (tLCPolDB.getInfo() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "LCPol表查询失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
            OriginOperator = tLCPolDB.getOperator();
            OriginPrem = tLCPolDB.getPrem();
            OriginStandPrem = tLCPolDB.getStandPrem(); //保存更新前的标准保费

            mLCPolBL.setMakeDate(tLCPolDB.getMakeDate());
            mLCPolBL.setMakeTime(tLCPolDB.getMakeTime());
            mLCPolBL.setModifyDate(theCurrentDate);
            mLCPolBL.setModifyTime(theCurrentTime);

            // 校验
            if (StrTool.cTrim(tLCPolDB.getAppFlag()).equals("1"))
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "此投保单已经签发保单，不能进行此项操作!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //如果是主险复核或核保成功，不能做修改
            if (tLCPolDB.getPolNo().equals(tLCPolDB.getMainPolNo()))
            {
                //                if (StrTool.cTrim(tLCPolDB.getApproveFlag()).equals("9"))
                //                {
                //                    CError tError = new CError();
                //                    tError.moduleName = "ProposalBL";
                //                    tError.functionName = "checkData";
                //                    tError.errorMessage = "此投保单已经复核成功，不能进行此项操作!";
                //                    this.mErrors.addOneError(tError);
                //
                //                    return false;
                //                }

                if (StrTool.cTrim(tLCPolDB.getUWFlag()).equals("4"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "此投保单核保结论为变更承保，不能进行此项操作!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
            else
            {
                //如果是附加险，不管复核或核保状态。容许修改,将其状态置为初始状态
                mLCPolBL.setApproveFlag("0");
                mLCPolBL.setUWFlag("0");
            }

            if (mChangePlanFlag.equals("1"))
            {
                //如果该投保单还有未回复的问题件,则不容许进行修改
                String tStr = "select count(*) from  lcissuepol where ProposalContNo='"
                        + tLCPolDB.getProposalNo()
                        + "'  and BackObjType in('1') and ReplyMan is null with ur";

                //LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
                ExeSQL tExeSQL = new ExeSQL();
                int tCount = Integer.parseInt(tExeSQL.getOneValue(tStr));
                //                System.out.println("======tCount=======" + tCount);
                if (tCount == 0)
                {
                    //                    System.out.println("======tCount=======" + tCount);
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "此投保单有操作员的问题件未回复，不能进行此项修改操作,请先回复!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
        }
        // end of if

        if (mOperate.equals("DELETE||PROPOSAL"))
        { // sun要求修改校验规则，只限制是否签单！
            String tProposalNo = mLCPolBL.getProposalNo();
            LCPolSchema tLCPolSchema = new LCPolSchema();
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tProposalNo);
            if (tLCPolDB.getInfo() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "LCPol表查询失败!";
                this.mErrors.addOneError(tError);

                return false;
            }

            // 校验
            if (StrTool.cTrim(tLCPolDB.getAppFlag()).equals("1"))
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "此投保单已经签发保单保单，不能进行此项操作!";
                this.mErrors.addOneError(tError);

                return false;
            }
            if (tLCPolDB.getPolNo().equals(tLCPolDB.getMainPolNo()))
            {
                LCPolDB tLCPolDB1 = new LCPolDB();
                tLCPolDB1.setMainPolNo(tLCPolDB.getMainPolNo());
                int tPolCount = tLCPolDB1.getCount();
                if (tLCPolDB1.mErrors.needDealError())
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "查询投保单失败";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tPolCount > 1)
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "该主险下还有附加险，请先删除附加险！";
                    this.mErrors.addOneError(tError);
                    return false;
                }

            }
        }

        return true;
    }

    /**
     * 检查被保人录入的数据是否正确(适用于个人投保单和集体下的个单)
     *
     * @return boolean
     */
    private boolean checkLCInsured()
    {
        //如果是无名单或者公共帐户的个人，不校验返回
        if (mLCPolBL.getPolTypeFlag().equals("1")
                || mLCPolBL.getPolTypeFlag().equals("2"))
        {
            return true;
        }

        // 被保人
        if (mLCInsuredBLSet != null)
        {
            /*Lis5.3 upgrade set
             for (int i = 1; i <= mLCInsuredBLSet.size(); i++)
             {
             LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
             tLCInsuredSchema = mLCInsuredBLSet.get(i);
             if (tLCInsuredSchema != null)
             {
             if (tLCInsuredSchema.getName() != null)
             { //去空格
             tLCInsuredSchema.setName(tLCInsuredSchema.getName()
             .trim());
             }
             if (tLCInsuredSchema.getSex() != null)
             {
             tLCInsuredSchema.setSex(tLCInsuredSchema.getSex().trim());
             }
             if (tLCInsuredSchema.getIDNo() != null)
             {
             tLCInsuredSchema.setIDNo(tLCInsuredSchema.getIDNo()
             .trim());
             }
             if (tLCInsuredSchema.getIDType() != null)
             {
             tLCInsuredSchema.setIDType(tLCInsuredSchema.getIDType()
             .trim());
             }
             if (tLCInsuredSchema.getBirthday() != null)
             {
             tLCInsuredSchema.setBirthday(tLCInsuredSchema.getBirthday()
             .trim());
             }

             //如果没有录入被保人标记是主被保人还是从被保人，则默认是主被保人
             if ((tLCInsuredSchema.getInsuredGrade() == null)
             || tLCInsuredSchema.getInsuredGrade().trim().equals(""))
             {
             tLCInsuredSchema.setInsuredGrade("M");
             }

             //有客户号
             if ((tLCInsuredSchema.getCustomerNo() != null)
             && !tLCInsuredSchema.getCustomerNo().trim().equals(""))
             {
             LDPersonDB tLDPersonDB = new LDPersonDB();
             tLDPersonDB.setCustomerNo(tLCInsuredSchema
             .getCustomerNo());
             if (tLDPersonDB.getInfo() == false)
             {
             CError tError = new CError();
             tError.moduleName = "ProposalBL";
             tError.functionName = "checkPerson";
             tError.errorMessage = "数据库查询失败!";
             this.mErrors.addOneError(tError);

             return false;
             }
             if (tLCInsuredSchema.getName() != null)
             {
             String Name = StrTool.GBKToUnicode(tLDPersonDB.getName()
             .trim());
             String NewName = StrTool.GBKToUnicode(tLCInsuredSchema.getName()
             .trim());

             if (!Name.equals(NewName))
             {
             CError tError = new CError();
             tError.moduleName = "ProposalBL";
             tError.functionName = "checkPerson";
             tError.errorMessage = "您输入的被保人客户号对应在数据库中的客户姓名("
             + Name + ")与您录入的客户姓名("
             + NewName + ")不匹配！";
             this.mErrors.addOneError(tError);

             return false;
             }
             }
             if (tLCInsuredSchema.getSex() != null)
             {
             if (!tLDPersonDB.getSex().equals(tLCInsuredSchema
             .getSex()))
             {
             CError tError = new CError();
             tError.moduleName = "ProposalBL";
             tError.functionName = "checkPerson";
             tError.errorMessage = "您输入的被保人客户号对应在数据库中的客户性别("
             + tLDPersonDB.getSex()
             + ")与您录入的客户性别("
             + tLCInsuredSchema.getSex()
             + ")不匹配！";
             this.mErrors.addOneError(tError);

             return false;
             }
             }
             }
             else //如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
             {
             if ((tLCInsuredSchema.getName() != null)
             && (tLCInsuredSchema.getIDNo() != null)
             && (tLCInsuredSchema.getSex() != null))
             {
             if (tLCInsuredSchema.getName().trim().equals("")
             && tLCInsuredSchema.getIDNo().trim().equals(""))
             {
             CError tError = new CError();
             tError.moduleName = "ProposalBL";
             tError.functionName = "checkPerson";
             tError.errorMessage = "请至少输入的被保人的姓名或ID号！";
             this.mErrors.addOneError(tError);

             return false;
             }

             LDPersonDB tLDPersonDB = new LDPersonDB();
             tLDPersonDB.setName(tLCInsuredSchema.getName());
             tLDPersonDB.setSex(tLCInsuredSchema.getSex());
             if (tLCInsuredSchema.getBirthday() != null)
             {
             tLDPersonDB.setBirthday(tLCInsuredSchema
             .getBirthday());
             }

             //tLDPersonDB.setIDType("0");
             tLDPersonDB.setIDNo(tLCInsuredSchema.getIDNo());

             LDPersonSet tLDPersonSet = tLDPersonDB.query();
             if (tLDPersonSet != null)
             {
             if (tLDPersonSet.size() > 0)
             {
             tLCInsuredSchema.setCustomerNo(tLDPersonSet.get(1)
             .getCustomerNo());
             mLCInsuredBLSet.set(i, tLCInsuredSchema);
             }
             }
             }
             }
             }
             }
             */
        }

        return true;
    }

    /**
     * 校验个人投保单的投保人
     * @return
     */
    //    private boolean checkLCAppnt()
    //    {
    //        //如果是无名单或者公共帐户的个人，不校验返回
    //        if (mLCPolBL.getPolTypeFlag().equals("1")
    //                || mLCPolBL.getPolTypeFlag().equals("2"))
    //        {
    //            return true;
    //        }
    //
    //        // 投保人-个人客户--如果是集体下个人，该投保人为空,所以个人时校验个人投保人
    //        if (mPolType.equals("1"))
    //        {
    //            if (mLCAppntBL != null)
    //            {
    //                if (mLCAppntBL.getAppntName() != null)
    //                { //去空格
    //                    mLCAppntBL.setAppntName(mLCAppntBL.getAppntName().trim());
    //                }
    //                if (mLCAppntBL.getAppntSex() != null)
    //                {
    //                    mLCAppntBL.setSex(mLCAppntBL.getAppntSex().trim());
    //                }
    //                if (mLCAppntBL.getAppntIDNo() != null)
    //                {
    //                    mLCAppntBL.setIDNo(mLCAppntBL.getidgetAppntIDNo().trim());
    //                }
    //                if (mLCAppntBL.getIDType() != null)
    //                {
    //                    mLCAppntBL.setIDType(mLCAppntBL.getIDType().trim());
    //                }
    //                if (mLCAppntBL.getBirthday() != null)
    //                {
    //                    mLCAppntBL.setBirthday(mLCAppntBL.getBirthday().trim());
    //                }
    //
    //                if (mLCAppntBL.getCustomerNo() != null)
    //                {
    //                    //如果有客户号
    //                    if (!mLCAppntBL.getCustomerNo().equals(""))
    //                    {
    //                        LDPersonDB tLDPersonDB = new LDPersonDB();
    //
    //                        tLDPersonDB.setCustomerNo(mLCAppntBL.getCustomerNo());
    //                        if (tLDPersonDB.getInfo() == false)
    //                        {
    //                            CError tError = new CError();
    //                            tError.moduleName = "ProposalBL";
    //                            tError.functionName = "checkPerson";
    //                            tError.errorMessage = "数据库查询失败!";
    //                            this.mErrors.addOneError(tError);
    //
    //                            return false;
    //                        }
    //                        if (mLCAppntBL.getName() != null)
    //                        {
    //                            String Name = StrTool.GBKToUnicode(tLDPersonDB.getName()
    //                                                                          .trim());
    //                            String NewName = StrTool.GBKToUnicode(mLCAppntBL.getName()
    //                                                                               .trim());
    //
    //                            if (!Name.equals(NewName))
    //                            {
    //                                CError tError = new CError();
    //                                tError.moduleName = "ProposalBL";
    //                                tError.functionName = "checkPerson";
    //                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户姓名("
    //                                                      + Name + ")与您录入的客户姓名("
    //                                                      + NewName + ")不匹配！";
    //                                this.mErrors.addOneError(tError);
    //
    //                                return false;
    //                            }
    //                        }
    //                        if (mLCAppntBL.getSex() != null)
    //                        {
    //                            if (!tLDPersonDB.getSex().equals(mLCAppntBL
    //                                                                 .getSex()))
    //                            {
    //                                CError tError = new CError();
    //                                tError.moduleName = "ProposalBL";
    //                                tError.functionName = "checkPerson";
    //                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户性别("
    //                                                      + tLDPersonDB.getSex()
    //                                                      + ")与您录入的客户性别("
    //                                                      + mLCAppntBL.getSex()
    //                                                      + ")不匹配！";
    //                                this.mErrors.addOneError(tError);
    //
    //                                return false;
    //                            }
    //                        }
    //                    }
    //                    else //如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
    //                    {
    //                        if ((mLCAppntBL.getName() != null)
    //                                && (mLCAppntBL.getSex() != null)
    //                                && (mLCAppntBL.getIDNo() != null))
    //                        {
    //                            LDPersonDB tLDPersonDB = new LDPersonDB();
    //                            tLDPersonDB.setName(mLCAppntBL.getName());
    //                            tLDPersonDB.setSex(mLCAppntBL.getSex());
    //                            tLDPersonDB.setBirthday(mLCAppntBL.getBirthday());
    //
    //                            //tLDPersonDB.setIDType("0");
    //                            tLDPersonDB.setIDNo(mLCAppntBL.getIDNo());
    //
    //                            LDPersonSet tLDPersonSet = tLDPersonDB.query();
    //                            if (tLDPersonSet != null)
    //                            {
    //                                if (tLDPersonSet.size() > 0)
    //                                {
    //                                    mLCAppntBL.setCustomerNo(tLDPersonSet.get(1)
    //                                                                            .getCustomerNo());
    //                                }
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //
    //        return true;
    //    }
    /**
     * 校验集体投保人信息（暂不用）
     *
     * @return boolean
     */
    private boolean checkLCAppntGrp()
    {
        //集体客户;
        if (mLCGrpAppntBL != null)
        {
            if (mLCGrpAppntBL.getCustomerNo() != null)
            { //如果集体客户号不为空
                if (!mLCGrpAppntBL.getCustomerNo().equals(""))
                {
                    LDGrpDB tLDGrpDB = new LDGrpDB();
                    //tLDGrpDB.setGrpNo(mLCGrpAppntBL.getGrpNo());
                    if (tLDGrpDB.getInfo() == false)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkPerson";
                        tError.errorMessage = "数据库查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if (mLCGrpAppntBL.getName() != null)
                    {
                        String Name = StrTool.GBKToUnicode(tLDGrpDB
                                .getGrpName().trim());
                        String NewName = StrTool.GBKToUnicode(mLCGrpAppntBL
                                .getName().trim());
                        if (!Name.equals(NewName))
                        {
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的集体客户号对应在数据库中的客户姓名("
                                    + Name + ")与您录入的集体客户姓名(" + NewName
                                    + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                }
            }
            else
            { //如果集体客户号为空。待补充
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
        //处理个人新增业务
        if (this.mOperate.equals("INSERT||PROPOSAL")
                || this.mOperate.equals("UPDATE||PROPOSAL"))
        {
            if (dealDataPerson() == false)
            {
                return false;
            }

            //银行实时出单的特殊处理--可以去掉
            if (dealDataForBank() == false)
            {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||PROPOSAL"))
        {
            if (dealDataDel() == false)
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行个人保单删除的逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealDataDel()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mLCPolBL.getProposalNo());
        if (tLCPolDB.getInfo() == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);

            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tLCPolDB.getContNo());
        if (tLCContDB.getInfo() == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContDB.mErrors);

            return false;
        }

        //由于精度问题注释该段代码 modify by sunxy 2005-01-16
        //        tLCContDB.setPrem(tLCContDB.getPrem()
        //                          - tLCPolDB.getPrem());
        //        tLCContDB.setAmnt(tLCContDB.getAmnt()
        //                          - tLCPolDB.getAmnt());

        //由于精度问题添加该段代码 modify by sunxy 2005-01-16
        String strCalContPrem = mDecimalFormat.format(tLCContDB.getPrem()
                - tLCPolDB.getPrem()); //转换计算后的保额
        String strCalContAmnt = mDecimalFormat.format(tLCContDB.getAmnt()
                - tLCPolDB.getAmnt()); //转换计算后的保额
        double calPolPrem = Double.parseDouble(strCalContPrem);
        double calPolAmnt = Double.parseDouble(strCalContAmnt);
        tLCContDB.setPrem(calPolPrem);
        tLCContDB.setAmnt(calPolAmnt);

        mLCContSchema.setSchema(tLCContDB.getSchema());
        if (tLCPolDB.getContType().equals("2"))
        {
            mPolType = "2";
        }

        if (mPolType.equals("2"))
        { // 集体下的个人
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpContNo(tLCPolDB.getGrpContNo());
            tLCGrpPolDB.setRiskCode(tLCPolDB.getRiskCode());

            LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery("select * from LCGrpPol where GrpContNo = '"+tLCPolDB.getGrpContNo()+"' "
					+ " and RiskCode = '"+tLCPolDB.getRiskCode()+"' with ur");
   
            if (tLCGrpPolSet.size() == 0)
            {

                this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);

                return false;

            }
            else
            {
                mLCGrpPolSchema = tLCGrpPolSet.get(1);
            }
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCGrpPolSchema.setModifyTime(theCurrentTime);

            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(tLCPolDB.getGrpContNo());
            if (tLCGrpContDB.getInfo() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                return false;
            }
            tLCGrpContDB.setModifyDate(theCurrentDate);
            tLCGrpContDB.setModifyTime(theCurrentTime);

            int NONAMENUM = 1;

            if ((tLCPolDB.getPolTypeFlag() == null)
                    || tLCPolDB.getPolTypeFlag().equals(""))
            {
                tLCPolDB.setPolTypeFlag("0");
            }

            //如果不是集体下的无名单加人--类型是4
            if (!mSavePolType.equals("4"))
            {
                //保单类型为无名单（投保人数>0）的或者为公共帐户（投保人数为0）的
                if (tLCPolDB.getPolTypeFlag().equals("1")
                        || tLCPolDB.getPolTypeFlag().equals("2"))
                {
                    mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema.getPeoples2()
                            - tLCPolDB.getInsuredPeoples());
                    //tLCGrpContDB.setPeoples2(tLCGrpContDB.getPeoples2()
                    //                            - tLCPolDB.getInsuredPeoples());
                    NONAMENUM = tLCPolDB.getInsuredPeoples();
                }
                else
                {
                    mLCGrpPolSchema
                            .setPeoples2(mLCGrpPolSchema.getPeoples2() - 1);
                    //tLCGrpContDB.setPeoples2(tLCGrpContDB.getPeoples2()
                    //                            - 1);
                }

                //由于精度问题添加该段代码 modify by sunxy 2005-01-16
                String strCalGrpPolPrem = mDecimalFormat.format(mLCGrpPolSchema
                        .getPrem()
                        - tLCPolDB.getPrem()); //转换计算后的保费(规定的精度)
                String strCalGrpPolAmnt = mDecimalFormat.format(mLCGrpPolSchema
                        .getAmnt()
                        - tLCPolDB.getAmnt()); //转换计算后的保额

                String strCalGrpContPrem = mDecimalFormat.format(tLCGrpContDB
                        .getPrem()
                        - tLCPolDB.getPrem()); //转换计算后的保费(规定的精度)
                String strCalGrpContAmnt = mDecimalFormat.format(tLCGrpContDB
                        .getAmnt()
                        - tLCPolDB.getAmnt()); //转换计算后的保额

                double calGrpPolPrem = Double.parseDouble(strCalGrpPolPrem);
                double calGrpPolAmnt = Double.parseDouble(strCalGrpPolAmnt);

                double calGrpContPrem = Double.parseDouble(strCalGrpContPrem);
                double calGrpContAmnt = Double.parseDouble(strCalGrpContAmnt);

                mLCGrpPolSchema.setPrem(calGrpPolPrem);
                mLCGrpPolSchema.setAmnt(calGrpPolAmnt);
                tLCGrpContDB.setPrem(calGrpContPrem);
                tLCGrpContDB.setAmnt(calGrpContAmnt);

            }
            mLCGrpContSchema = tLCGrpContDB.getSchema();
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行个人保单录入的逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealDataPerson()
    {
        int i;
        int iMax;
        String tStr = "";
        String tNo = "";
        String tLimit = "";
        Reflections tReflections = new Reflections();

        //产生保单号码
        if (mOperate.equals("INSERT||PROPOSAL"))
        {
            tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
            int m = 0;
            while (m < 10)
            {
                tNo = PubFun1.CreateMaxNo("ProposalNo", tLimit);
                if (tNo.indexOf("null") == -1)
                {
                    break;
                }
                System.out.println("生成险种号码发生第" + m + "次错误！");
                m++;
            }
            //tNo = PubFun1.CreateMaxNo("ProposalNo", tLimit);
            mLCPolBL.setProposalNo(tNo);
            mLCPolBL.setProposalContNo(mLCContSchema.getProposalContNo());
        }
        else
        {
            //投保申请日期处理:1 没有该投保单，属于新增，取录入值；
            //2 数据库中有该投保单，若数据库的指定生效日期值为N，取录入值；若数据库的指定生效日期值为Y，保持申请日期。
            if (mOperate.equals("UPDATE||PROPOSAL"))
            {
                System.out.println("猪猪" + mLCPolBL.getProposalNo());
                LCPolDB xLCPolDB = new LCPolDB();
                LCPolSet xLCPolSet = new LCPolSet();
                xLCPolDB.setProposalNo(mLCPolBL.getProposalNo());
                xLCPolSet = xLCPolDB.query();
                if (xLCPolSet.size() > 0)
                {
                    if (!StrTool.cTrim(xLCPolSet.get(1).getSpecifyValiDate())
                            .equals("Y"))
                    {
                        mLCPolBL.setPolApplyDate(xLCPolSet.get(1)
                                .getPolApplyDate());
                    }
                    mLCPolBL.setApproveFlag(xLCPolSet.get(1).getApproveFlag());
                    System.out.println("已存在的ApproveFlag"
                            + mLCPolBL.getApproveFlag());
                    mLCPolBL.setApproveCode(xLCPolSet.get(1).getApproveCode());
                    mLCPolBL.setApproveDate(xLCPolSet.get(1).getApproveDate());
                    mLCPolBL.setApproveTime(xLCPolSet.get(1).getApproveTime());
                    mLCPolBL.setUWFlag(xLCPolSet.get(1).getUWFlag());
                    System.out.println("已存在的UWFlag" + mLCPolBL.getUWFlag());
                    mLCPolBL.setUWCode(xLCPolSet.get(1).getUWCode());
                    mLCPolBL.setSaleChnlDetail(xLCPolSet.get(1)
                            .getSaleChnlDetail());
                    mLCPolBL.setRiskSeqNo(xLCPolSet.get(1).getRiskSeqNo());
                }
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(mLCContSchema.getContNo());
                if (!tLCContDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLCContDB.mErrors);
                    return false;
                }
                mLCPolBL.setProposalContNo(tLCContDB.getProposalContNo());
            }
        }
        mLCPolBL.setPolNo(mLCPolBL.getProposalNo());
        //查询出销售渠道明细保存到lcpol
        LCContDB mLCContDB = new LCContDB();
//      mLCContDB.setContNo(mLCContSchema.getContNo());
        LCContSet tLCContSet = mLCContDB.executeQuery("select * from lccont where contno = '"+mLCContSchema.getContNo()+"' with ur ");
        if (tLCContSet.size() > 1)
        {
            CError tError = new CError();
            tError.moduleName = "ContInsuredBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询合同信息不唯一!";
            this.mErrors.addOneError(tError);
            return false;
        }
        /**
         * 杨明主掉
         */
        //        tSaleChnlDetail = tLCContSet.get(1).getSaleChnlDetail();
        //        mLCPolBL.setSaleChnlDetail(tSaleChnlDetail);
        //        System.out.println("tSaleChnlDetail : " + tSaleChnlDetail);
        //如果是否指定生效日期在界面上没有传入，那么默认是否
        if ((mLCPolBL.getSpecifyValiDate() == null)
                || mLCPolBL.getSpecifyValiDate().equals("")
                || mLCPolBL.getSpecifyValiDate().equals("N"))
        {
            mLCPolBL.setSpecifyValiDate("N");
        }
        else if (mLCPolBL.getCValiDate() != null)
        {
            mLCPolBL.setSpecifyValiDate("1");
        }
        //删除应收总表数据
        //        if (deleteAccNo)
        //        {
        //            LJSPayDB tLJSPayDB = new LJSPayDB();
        //            tLJSPayDB.setOtherNo(mLCPolBL.getPolNo());
        //            mLJSPaySet = tLJSPayDB.query();
        //
        //            for (int k = 0; k < mLJSPaySet.size(); k++)
        //            {
        //                LJSPaySchema tLJSPaySchema = mLJSPaySet.get(k + 1);
        //                if (tLJSPaySchema.getBankOnTheWayFlag().equals("1"))
        //                {
        //                    // @@错误处理
        //                    CError tError = new CError();
        //                    tError.moduleName = "ProposalBL";
        //                    tError.functionName = "dealDataPerson";
        //                    tError.errorMessage = "应收表有数据在发往银行，不能删除应收总表数据!";
        //                    this.mErrors.addOneError(tError);
        //
        //                    return false;
        //                }
        //            }
        //        }

        if ((mLCPolBL.getMainPolNo() == null)
                || mLCPolBL.getMainPolNo().trim().equals(""))
        {
            mLCPolBL.setMainPolNo(mLCPolBL.getProposalNo());
            mainLCPolBL.setSchema(mLCPolBL);
        }
        else
        {
            if (mGrpImportFlag == false)
            {
                mainLCPolBL.setPolNo(mLCPolBL.getMainPolNo());
                mainLCPolBL.getInfo();
                if (mainLCPolBL.mErrors.needDealError())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "查询该附险的主险保单时失败！";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
        }

        //mLCPolBL.setMasterPolNo( SysConst.ZERONO );
        //为了中英打印需求，在保单不是按分数或档次卖的时候，mult存0
        //        if (mLCPolBL.getMult() == 0)
        //        {
        //            mLCPolBL.setMult(1);
        //        }

        //查询险种描述表
        //查询险种承保描述表
        // 投保人
        String tAppntCustomerNo = "";
        String tAppntName = "";
        if (mAppntType.equals("1"))
        { // 个人投保人
            //mLCAppntBL.setPolNo(mLCPolBL.getPolNo());
            //mLCAppntBL.setDefaultFields();
            //mLCAppntBL.setAppntGrade("M");
            /**
             * 前台页面已经传入LCAppnt信息,并且在getInputData中已经从数据库中查询出
             * 全部的投保信息,因此,不需要重新新建投保人信息.
             */
            //            Reflections Ref = new Reflections();
            //            Ref.transFields(mLCAppntBL, mLCPolBL);
            //            mLCAppntBL.setAppntNo(mLCPolBL.getAppntNo());
            //            mLCAppntBL.setContNo(mLCPolBL.getContNo());
            //            if (StrTool.cTrim(mLCAppntBL.getAppntNo()).equals("")) {
            //                tLimit = "SN";
            //
            //                String tAppntNo = PubFun1.CreateMaxNo("CustomerNo", tLimit);
            //                mLCAppntBL.setAppntNo(tAppntNo);
            //
            //                // 客户表同步数据
            //                LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            //                tReflections.transFields(tLDPersonSchema,
            //                                         mLCAppntBL.getSchema());
            //                tLDPersonSchema.setOperator(mGlobalInput.Operator);
            //                tLDPersonSchema.setMakeDate(theCurrentDate);
            //                tLDPersonSchema.setMakeTime(theCurrentTime);
            //                tLDPersonSchema.setModifyDate(theCurrentDate);
            //                tLDPersonSchema.setModifyTime(theCurrentTime);
            //                mLDPersonSet.add(tLDPersonSchema);
            //            } else {
            //                LCAppntDB mLCAppntDB = new LCAppntDB();
            //                mLCAppntDB.setSchema(mLCAppntBL);
            //                mLCAppntDB.getInfo();
            //                mLCAppntBL.setSchema(mLCAppntDB);
            //            }
            // 校验投保人投保年龄
            int tAppntAge = 0;
            tAppntAge = PubFun.calInterval(mLCAppntBL.getAppntBirthday(),
                    mLCPolBL.getCValiDate(), "Y");
            //            System.out.println("投保年龄:" + tAppntAge);
            //            System.out.println("投保年龄描述:" + mLMRiskAppSchema.getMinAppntAge());
            if (tAppntAge < mLMRiskAppSchema.getMinAppntAge())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson";
                tError.errorMessage = "投保人投保年龄不符合投保要求!";
                this.mErrors.addOneError(tError);

                return false;
            }
            tAppntCustomerNo = mLCAppntBL.getAppntNo();
            tAppntName = mLCAppntBL.getAppntName();
        }
        if (mAppntType.equals("2"))
        { // 集体投保人
            //            mLCGrpAppntBL.setPolNo(mLCPolBL.getPolNo());
            //            mLCAppntGrpBL.setAppntGrade("M");
            //            mLCAppntGrpBL.setOperator(mGlobalInput.Operator);
            //            mLCAppntGrpBL.setMakeDate(theCurrentDate);
            //            mLCAppntGrpBL.setMakeTime(theCurrentTime);
            //            mLCAppntGrpBL.setModifyDate(theCurrentDate);
            //            mLCAppntGrpBL.setModifyTime(theCurrentTime);

            //如果从外部接收的数据为空，那么从数据库查询
            //            if (mLDGrpSchema == null)
            //            {
            //                LDGrpDB tLDGrpDB = new LDGrpDB();
            //                //PQ 需要恢复
            //                tLDGrpDB.setCustomerNo(mLCGrpAppntBL.getCustomerNo());
            //                if (tLDGrpDB.getInfo() == false)
            //                {
            //                    // @@错误处理
            //                    this.mErrors.copyAllErrors(tLDGrpDB.mErrors);
            //
            //                    CError tError = new CError();
            //                    tError.moduleName = "ProposalBL";
            //                    tError.functionName = "dealDataPerson";
            //                    tError.errorMessage = "LDGrp表查询失败!";
            //                    this.mErrors.addOneError(tError);
            //
            //                    return false;
            //                }
            //                mLDGrpSchema = tLDGrpDB.getSchema();
            //            }

            tAppntCustomerNo = mLCGrpAppntBL.getCustomerNo();
            tAppntName = mLCGrpAppntBL.getName();
        }

        //添加投保人名称和客户号
        /*Lis5.3 upgrade set
         mLCPolBL.setAppntType(mAppntType); //1表示个人投保，2表示集体投保
         */

        mLCPolBL.setAppntNo(tAppntCustomerNo);
        mLCPolBL.setAppntName(tAppntName);

        //被保人-如果有多条，即有连带被保人，则将主被保人放在第一位
        //        LCInsuredBL tLCInsuredBL1 = new LCInsuredBL();
        //        if (mLCInsuredBLSet.size() > 1)
        //        {
        //            LCInsuredBL tempLCInsuredBL = new LCInsuredBL();
        //            for (int n = 1; n <= mLCInsuredBLSet.size(); n++)
        //            {
        //                /*Lis5.3 upgrade get
        //              String InsuredGrade = mLCInsuredBLSet.get(n).getInsuredGrade();
        //              */
        //             //PQ 被保险人处理，连带和主被保险人要分开
        //             String InsuredGrade ="";
        //                if ((InsuredGrade != null) && InsuredGrade.equals("M"))
        //                {
        //                    if (n == 1)
        //                    {
        //                        break;
        //                    }
        //
        //                    //得到排序第n位的数据和第1位互换
        //                    tempLCInsuredBL.setSchema(mLCInsuredBLSet.get(n).getSchema());
        //                    mLCInsuredBLSet.set(n, mLCInsuredBLSet.get(1));
        //                    mLCInsuredBLSet.set(1, tempLCInsuredBL);
        //
        //                    break;
        //                }
        //            }
        //        }
        //        tLCInsuredBL1.setSchema((LCInsuredSchema) mLCInsuredBLSet.get(1));

        //判断投保人是否和主被保人相同，集体投保人不判断
        // 0 表示投保人和主被保人不相同  1 表示投保人和主被保人相同

        //被保险人处理
        if (StrTool.cTrim(mLCInsuredBL.getInsuredNo()).equals(""))
        {
            tLimit = "SN";

            String tInsuredNo = PubFun1.CreateMaxNo("CustomerNo", tLimit);
            if (tInsuredNo.trim().equals(""))
            {
                mErrors.addOneError("客户号生成失败!");
                return false;
            }
            mLCInsuredBL.setInsuredNo(tInsuredNo);

            //如果投保单类型是无名单或者公共帐户，不添加客户信息，补充投保人信息
            if (mLCPolBL.getPolTypeFlag().equals("1")
                    || mLCPolBL.getPolTypeFlag().equals("2"))
            {
                //如果是附加险，查出主险的被保人号码赋给附加险
                if (mLMRiskAppSchema.getSubRiskFlag().equals("S"))
                {
                    LCPolDB tempLCpolDB = new LCPolDB();
                    tempLCpolDB.setPolNo(mLCPolBL.getMainPolNo());
                    if (tempLCpolDB.getInfo() == false)
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkLCInsured";
                        tError.errorMessage = "未查询到该投保单的主险！";
                        this.mErrors.addOneError(tError);

                        return false;
                    }

                    //如果主险也是无名单（因该是），那么取主险的被保人信息
                    if (tempLCpolDB.getPolTypeFlag().equals("1")
                            || tempLCpolDB.getPolTypeFlag().equals("2"))
                    {
                        mLCInsuredBL.setInsuredNo(tempLCpolDB.getInsuredNo());
                        mLCInsuredBL.setName(tempLCpolDB.getInsuredName());
                        mLCInsuredBL.setBirthday(tempLCpolDB
                                .getInsuredBirthday());
                        mLCInsuredBL.setSex(tempLCpolDB.getInsuredSex());
                    }
                }
                if (mLCPolBL.getPolTypeFlag().equals("2"))
                {
                    mLCInsuredBL.setName("公共账户");
                }
                else
                {
                    mLCInsuredBL.setName("无名单");
                }

                if (mLCInsuredBL.getSex() == null)
                {
                    mLCInsuredBL.setSex("0");
                }
                System.out
                        .println("被保人年龄 ！！！！ ： " + mLCInsuredBL.getBirthday());
                if (mLCInsuredBL.getBirthday() == null)
                { //如果是无名单，没有录入年龄，默认是30
                    int tInsuredAge = 0;
                    if (mLCPolBL.getInsuredAppAge() <= 0)
                    { //如果投保年龄没有录入
                        tInsuredAge = 30;
                        mLCPolBL.setInsuredAppAge(tInsuredAge);
                    }
                    else
                    {
                        tInsuredAge = mLCPolBL.getInsuredAppAge(); //前台录入年龄
                    }

                    //年龄所在年=系统年-年龄
                    String year = Integer.toString(Integer.parseInt(StrTool
                            .getYear())
                            - tInsuredAge);
                    String brithday = StrTool.getDate(year, StrTool.getMonth(),
                            StrTool.getDay());
                    brithday = StrTool.replace(brithday, "/", "-");
                    mLCInsuredBL.setBirthday(brithday);
                }
                else
                {
                    String birthday = mLCInsuredBL.getBirthday();
                    String year = birthday.substring(0, birthday.indexOf("-"));
                    String month = birthday.substring(
                            birthday.indexOf("-") + 1,
                            birthday.indexOf("-") + 3);
                    String day = birthday
                            .substring(birthday.lastIndexOf("-") + 1);

                    String today = PubFun.getCurrentDate();
                    String tyear = today.substring(0, today.indexOf("-"));
                    String tmonth = today.substring(today.indexOf("-") + 1,
                            today.indexOf("-") + 3);
                    String tday = today.substring(today.lastIndexOf("-") + 1);

                    int age = Integer.parseInt(tyear) - Integer.parseInt(year);
                    if (Integer.parseInt(tmonth) == Integer.parseInt(month))
                    {
                        if (Integer.parseInt(tday) < Integer.parseInt(day))
                        {
                            age = age - 1;
                        }
                    }
                    else if (Integer.parseInt(tmonth) < Integer.parseInt(month))
                    {
                        age = age - 1;
                    }
                    System.out.println("被保人年龄！ ： " + age);
                    mLCPolBL.setInsuredAppAge(age);
                }
            }
            else
            {
                // 客户表同步数据
                LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                tReflections.transFields(tLDPersonSchema, mLCInsuredBL
                        .getSchema());
                tLDPersonSchema.setOperator(mGlobalInput.Operator);
                tLDPersonSchema.setMakeDate(theCurrentDate);
                tLDPersonSchema.setMakeTime(theCurrentTime);
                tLDPersonSchema.setModifyDate(theCurrentDate);
                tLDPersonSchema.setModifyTime(theCurrentTime);
                mLDPersonSet.add(tLDPersonSchema);
            }

            mLCInsuredBLSet.set(1, mLCInsuredBL);
        }
        else
        {
            LCInsuredDB mLCInsuredDB = new LCInsuredDB();
            mLCInsuredDB.setSchema(mLCInsuredBL);
            mLCInsuredDB.getInfo();
            mLCInsuredBL.setSchema(mLCInsuredDB);
        }

        //mLCInsuredBL.setPolNo(mLCPolBL.getPolNo());

        mLCInsuredBL.setDefaultFields();
        //mLCInsuredBLSetNew.add(mLCInsuredBL);

        //PQ 连带或连生被保险人处理
        //校验连带被保人投保年龄
        if (mLCInsuredRelatedSet.size() >= 1)
        {
            if (checkSLCInsured(mLCInsuredRelatedSet) == false)
            {
                return false;
            }

            for (i = 1; i <= mLCInsuredRelatedSet.size(); i++)
            {
                LCInsuredRelatedSchema tLCInsuredRelatedSchema = new LCInsuredRelatedSchema();
                tLCInsuredRelatedSchema = mLCInsuredRelatedSet.get(i);

                tLCInsuredRelatedSchema.setPolNo(mLCPolBL.getPolNo());
                //                tLCInsuredRelatedBL.setPrtNo(mLCPolBL.getPrtNo());
                tLCInsuredRelatedSchema.setMainCustomerNo(mLCInsuredBL
                        .getInsuredNo());
                tLCInsuredRelatedSchema.setModifyDate(theCurrentDate);
                tLCInsuredRelatedSchema.setModifyTime(theCurrentTime);

                if (StrTool.cTrim(tLCInsuredRelatedSchema.getCustomerNo())
                        .equals(""))
                {
                    tLimit = "SN";

                    String tInsuredNo = PubFun1.CreateMaxNo("CustomerNo",
                            tLimit);
                    if (tInsuredNo.trim().equals(""))
                    {
                        mErrors.addOneError("客户号生成失败!");
                        return false;
                    }
                    tLCInsuredRelatedSchema.setCustomerNo(tInsuredNo);

                    // 客户表同步数据
                    LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                    tReflections.transFields(tLDPersonSchema,
                            tLCInsuredRelatedSchema);
                    tLDPersonSchema.setOperator(mGlobalInput.Operator);
                    tLDPersonSchema.setMakeDate(theCurrentDate);
                    tLDPersonSchema.setMakeTime(theCurrentTime);
                    tLDPersonSchema.setModifyDate(theCurrentDate);
                    tLDPersonSchema.setModifyTime(theCurrentTime);
                    mLDPersonSet.add(tLDPersonSchema);
                }
                //LCInsuredRelatedSet.set(i,tLCInsuredRelatedBL.getSchema());
                //newLCInsuredRelatedSet.add(tLCInsuredRelatedBL);
            }
        }

        // 校验被保人投保年龄
        int tInsuredAge = 0;
        String tInsuredAgeFlag = "";
        if (mLCPolBL.getPolTypeFlag().equals("1"))
        { //houzm
            tInsuredAge = PubFun.calInterval(mLCInsuredBL.getBirthday(),
                    mLCPolBL.getCValiDate(), "Y");
        }
        else
        {
            //通过前台录入出生日期及计算
            tInsuredAgeFlag = mLMRiskAppSchema.getInsuredAgeFlag();
            if ("D".equals(tInsuredAgeFlag))
                tInsuredAgeFlag = "D";
            else
                tInsuredAgeFlag = "Y";
            //add by fuxin 2010-07-27 保全增人被保人年龄取根据增人的生效日期计算。
            if((this.EdorNo == null || this.EdorNo.equals("")))
            {
            tInsuredAge = PubFun.calInterval(mLCInsuredBL.getBirthday(),
                    mLCPolBL.getCValiDate(), tInsuredAgeFlag);
            }else{
            	String SQL = " select (case "
            			   + " when edorvalidate is not null then "
            			   + " edorvalidate "
            			   +" else "
            			   +" (select edorvalidate "
            			   +" From lpgrpedormain "
            			   +" where a.edorno = edoracceptno) "
            			   +" end) From lcinsuredlist a "
            			   +" where edorno = '"+this.EdorNo+"' "
            			   +" and insuredname = '"+mLCInsuredBL.getName()+"' "
            			   +" and idno ='"+mLCInsuredBL.getIDNo()+"'"
            			   +"  with ur" ;
            	String tCValiDate = new ExeSQL().getOneValue(SQL);
            	System.out.println("保全增人被保人的生效日期："+tCValiDate);
            	tInsuredAge = PubFun.calInterval(mLCInsuredBL.getBirthday(),
            			tCValiDate, tInsuredAgeFlag);
            	System.out.println("保全增人被保人的投保年龄："+tInsuredAge);
            }

        }
        //        System.out.println("被保投保年龄:" + tInsuredAge);
        //            System.out.println("被保投保年龄描述:" + mLMRiskAppSchema.getMinInsuredAge()+"|"+mLMRiskAppSchema.getMaxInsuredAge());
        //      added by huxl @ 2006-12-07 暂时解决保全增人时，连带被保人年龄小于保单生效日期的问题
        if ((this.EdorType == null || this.EdorType.equals(""))
                && !mLCPolBL.getPolTypeFlag().equals("1"))
        {
            if ((tInsuredAge < mLMRiskAppSchema.getMinInsuredAge())
                    || ((tInsuredAge > mLMRiskAppSchema.getMaxInsuredAge()) && (mLMRiskAppSchema
                            .getMaxInsuredAge() > 0)))
            {
                if(tInsuredAge > mLMRiskAppSchema.getMaxInsuredAge()){
                	String tMaxAmntSQL = "select codename from ldcode where codetype = 'accidentageamnt' and code = '"+mLCPolBL.getRiskCode()+"' ";
                	String  tMaxAmnt = new ExeSQL().getOneValue(tMaxAmntSQL);
                	double tRiskAmnt = 0;
                	String tContPlanSQL = "select calfactorvalue from lccontplandutyparam where grpcontno = '"+mLCPolBL.getGrpContNo()+"' and riskcode = '"+mLCPolBL.getRiskCode()+"' and contplancode = '"+mLCPolBL.getContPlanCode()+"' and calfactor in ('Amnt','DutyAmnt')";
                	SSRS tContPlanSSRS = new ExeSQL().execSQL(tContPlanSQL);
                	if(tContPlanSSRS != null && tContPlanSSRS.MaxRow>0){
                		for(int tCIndex=1;tCIndex<=tContPlanSSRS.MaxRow;tCIndex++){
                			if(tContPlanSSRS.GetText(tCIndex, 1) !=null && !"".equals(tContPlanSSRS.GetText(tCIndex, 1))){
                				tRiskAmnt = tRiskAmnt+Double.parseDouble(tContPlanSSRS.GetText(tCIndex, 1));
                    		}
                		}
                	}
                	if(tMaxAmnt != null && !"".equals(tMaxAmnt)){
                		if(tRiskAmnt>Double.parseDouble(tMaxAmnt)){
                    		CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "dealDataPerson";
                            tError.errorMessage = "被保人投保年龄与保额不符合投保要求!";
                            this.mErrors.addOneError(tError);

                            return false;
                    	}
                	}else{
                		CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "dealDataPerson";
                        tError.errorMessage = "被保人投保年龄不符合投保要求!";
                        this.mErrors.addOneError(tError);

                        return false;
                	}
                	
                }else{
//                	 @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "被保人投保年龄不符合投保要求!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            	
            }
        }
        /** 生成险种序号，取当前被保险人的 最大险种序号 */
        String tRiskSeqNo = (new ExeSQL())
                .getOneValue("select integer(max(riskseqno))+1 from lcpol where ContNo='"
                        + mLCPolBL.getContNo()
                        + "' and InsuredNo='"
                        + mLCInsuredBL.getInsuredNo() + "'");
        if (tRiskSeqNo == null || tRiskSeqNo.equals("")
                || tRiskSeqNo.equals("null"))
        {
            tRiskSeqNo = "1";
        }
        if (mLCPolBL.getRiskSeqNo() == null)
        {
            mLCPolBL.setRiskSeqNo(tRiskSeqNo);
        }
        
        
        if(mSavePolType.equals("2")&& mLCPolBL.getRiskCode().equals("232701") ){  // 重疾被保人为投保人
        	
        	 LCAppntDB tLCAppntDB = new LCAppntDB();
             tLCAppntDB.setContNo(mLCPolBL.getContNo());
             tLCAppntDB.setAppntNo(mLCPolBL.getAppntNo());
          
             tLCAppntDB.getInfo();
             LCAppntSchema tLCAppntSchema = new LCAppntSchema();
             tLCAppntSchema = tLCAppntDB.getSchema();
        	
            mLCPolBL.setInsuredNo(tLCAppntSchema.getAppntNo());
            //        System.out.println("mLCInsuredBL.getInsuredNo()"+mLCInsuredBL.getInsuredNo());
            mLCPolBL.setInsuredName(tLCAppntSchema.getAppntName());
            mLCPolBL.setInsuredSex(tLCAppntSchema.getAppntSex());
            mLCPolBL.setInsuredBirthday(tLCAppntSchema.getAppntBirthday());
            //mLCPolBL.setInsuredHealth(mLCInsuredBL.getHealth());
           int tappntAge = PubFun.calInterval(tLCAppntSchema.getAppntBirthday(),
            		mLCPolBL.getCValiDate(), "Y");
            mLCPolBL.setInsuredAppAge(tappntAge);

            mLCPolBL.setOccupationType(mLCInsuredBL.getOccupationType());	
        	
        }else {
        mLCPolBL.setInsuredNo(mLCInsuredBL.getInsuredNo());
        //        System.out.println("mLCInsuredBL.getInsuredNo()"+mLCInsuredBL.getInsuredNo());
        mLCPolBL.setInsuredName(mLCInsuredBL.getName());
        mLCPolBL.setInsuredSex(mLCInsuredBL.getSex());
        mLCPolBL.setInsuredBirthday(mLCInsuredBL.getBirthday());
        //mLCPolBL.setInsuredHealth(mLCInsuredBL.getHealth());
        
        mLCPolBL.setInsuredAppAge(tInsuredAge);

        mLCPolBL.setOccupationType(mLCInsuredBL.getOccupationType());
        }
        mLCPolBL.setAutoPayFlag(mLMRiskAppSchema.getAutoPayFlag());
        /** 总公司费用率 */
        mLCPolBL.setComFeeRate(mLMRiskAppSchema.getAppInterest());
        /** 分公司费用率 */
        mLCPolBL.setBranchFeeRate(mLMRiskAppSchema.getAppPremRate());
        mLCPolBL.setStandPrem(mLCPolBL.getPrem()); //投保时总保费和每期保费相同
        if (StrTool.cTrim(mLCPolBL.getContType()).equals("2"))
        {
            mLCPolBL.setPayMode(mLCGrpContSchema.getPayMode());
        }
        if (StrTool.cTrim(mLCPolBL.getContType()).equals("1"))
        {
            mLCPolBL.setPayMode(mLCContSchema.getPayMode());
            mLCPolBL.setExPayMode(mLCContSchema.getExPayMode());
        }
        //如果不是无名单或者不是公共帐户，那么存放主被保人
        if (!(mLCPolBL.getPolTypeFlag().equals("1") || mLCPolBL
                .getPolTypeFlag().equals("2")))
        {
            mLCPolBL.setInsuredPeoples(1);
        }

        if (mOperate.equals("INSERT||PROPOSAL"))
        {
            //houzm添加：如果是集体下的个人录入保存，判断
            //如果姓名，性别，出生日期相同的，并且在同一印刷号，同一主附险中的个人信息，不能保存
            //if( !mLCPolBL.getGrpPolNo().equals( SysConst.ZERONO )&&mLCPolBL.getContNo().equals( SysConst.ZERONO ))
            /** 如果是大团单不需要交验是否重复 */
            if (mPolType.equals("2")
                    && !StrTool
                            .cTrim(this.mLCGrpContSchema.getBigProjectFlag())
                            .equals("1")
                    && mLCPolBL.getPolTypeFlag().equals("0"))
            {
                //                String strSql = "select * from LCPol where "
                //                        + " InsuredName = '" + mLCPolBL.getInsuredName() + "' "
                //                        + " and InsuredSex = '" + mLCPolBL.getInsuredSex()
                //                        + "' " + " and InsuredBirthday = '"
                //                        + mLCPolBL.getInsuredBirthday() + "' "
                //                        + " and RiskCode = '" + mLCPolBL.getRiskCode() + "' "
                //                        + " and PrtNo = '" + mLCPolBL.getPrtNo() + "' with ur";

                // 校验重复被保人修改为5要素：姓名、性别、出生日期、证件类别、证件号。
                //                if (mLCPolBL.getInsuredName() == null
                //                        || "".equals(mLCPolBL.getInsuredName())
                //                        || mLCPolBL.getInsuredSex() == null
                //                        || "".equals(mLCPolBL.getInsuredSex())
                //                        || mLCPolBL.getInsuredBirthday() == null
                //                        || "".equals(mLCPolBL.getInsuredBirthday())
                //                        || mLCInsuredBL.getIDNo() == null
                //                        || "".equals(mLCInsuredBL.getIDNo())
                //                        || mLCInsuredBL.getIDType() == null
                //                        || "".equals(mLCInsuredBL.getIDType()))
                //                {
                //                    // @@错误处理
                //                    CError tError = new CError();
                //                    tError.moduleName = "ProposalBL";
                //                    tError.functionName = "checkData";
                //                    tError.errorMessage = mLCPolBL.getInsuredName()
                //                            + "的姓名、性别、出生日期、证件类别、证件号为空！不能录入";
                //                    this.mErrors.addOneError(tError);
                //
                //                    return false;
                //                }

                String tIdNoCond = "";
                if (mLCInsuredBL.getIDNo() == null)
                {
                    tIdNoCond = " and lci.IdNo is null ";
                }
                else
                {
                    tIdNoCond = " and lci.IdNo = '" + mLCInsuredBL.getIDNo()
                            + "' ";
                }

                String tIdTypeCond = "";
                if (mLCInsuredBL.getIDNo() == null)
                {
                    tIdTypeCond = " and lci.IdType is null ";
                }
                else
                {
                    tIdTypeCond = " and lci.IdType = '"
                            + mLCInsuredBL.getIDType() + "' ";
                }

                String strSql = " select lcp.* "
                        + " from LCPol lcp "
                        + " inner join LCInsured lci on lcp.InsuredNo = lci.InsuredNo and lcp.ContNo = lci.ContNo "
                        + " where lci.Name = '" + mLCPolBL.getInsuredName()
                        + "' " + " and lci.Sex = '" + mLCPolBL.getInsuredSex()
                        + "' " + " and lci.Birthday = '"
                        + mLCPolBL.getInsuredBirthday() + "' "
                        + " and lcp.RiskCode = '" + mLCPolBL.getRiskCode()
                        + "' " + tIdNoCond + tIdTypeCond + " and lcp.PrtNo = '"
                        + mLCPolBL.getPrtNo() + "' " + " with ur ";
                // -------------------------------------------------------------

                LCPolDB tempLCPolDB = new LCPolDB();
                LCPolSet tempLCPolSet = tempLCPolDB.executeQuery(strSql);
                if (tempLCPolDB.mErrors.needDealError() == true)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tempLCPolDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "LCPol表查询失败!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                if (tempLCPolSet.size() > 0)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = mLCPolBL.getInsuredName()
                            + "的姓名，性别，出生日期及保单的印刷号和险种编码和已存在的保单数据重复！不能录入";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }

            //以上是houzm添加
        }
        if (mOperate.equals("INSERT||PROPOSAL"))
        {
            mLCPolBL.setOperator(mGlobalInput.Operator);
        }
        if (mOperate.equals("UPDATE||PROPOSAL"))
        {
            mLCPolBL.setOperator(OriginOperator);
        }

        //        System.out.println("保费保额计算调用部分");

        if (pubCal() == false)
        {
            return false;
        }

        // 如果年龄校验标志为天（tInsuredAgeFlag = "D"），对LCPol对象重置被保人年龄。
        if ("D".equals(tInsuredAgeFlag))
        {
            int tITmpInsuredAppAge = PubFun.calInterval(mLCPolBL
                    .getInsuredBirthday(), mLCPolBL.getCValiDate(), "Y");
            mLCPolBL.setInsuredAppAge(tITmpInsuredAppAge);
        }

        //System.out.println("保费保额计算结束");
        //保单表
        mLCPolBL.setRiskVersion(mLMRiskAppSchema.getRiskVer().trim());
        mLCPolBL.setKindCode(mLMRiskAppSchema.getKindCode().trim());

        if (mSavePolType.equals("0"))
        {
            mLCPolBL.setAppFlag("0"); //投保单/保单标志
        }
        else
        {
            mLCPolBL.setAppFlag(mSavePolType); //投保单/保单标志
        }
        mLCPolBL.setStateFlag("0"); //投保单/保单标志

        //处理在人工核保处修改险种的问题
        if (!StrTool.cTrim(mLCPolBL.getApproveFlag()).equals("0")
                && !StrTool.cTrim(mLCPolBL.getUWFlag()).equals("0")
                && mOperate.equals("UPDATE||PROPOSAL"))
        {
            mLCPolBL.setUWFlag("5");
            LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
            tLCUWMasterSchema = new LCUWMasterSchema();
            tLCUWMasterDB.setProposalNo(mLCPolBL.getProposalNo());
            tLCUWMasterSet = tLCUWMasterDB.query();
            if (tLCUWMasterSet.size() > 0)
            {
                System.out.println("来了来了!!!!!!!!!!!!!!!");
                tLCUWMasterSchema = tLCUWMasterSet.get(1);
                tLCUWMasterSchema.setPassFlag("5");
                tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
            }
        }
        else
        {
            mLCPolBL.setUWFlag("0"); //核保状态，0为未核保
            mLCPolBL.setApproveFlag("0"); //复核保状态，0为未复核
        }
        if (mLMRiskSchema.getRnewFlag().equals("N"))
        {
            mLCPolBL.setRnewFlag(-2); // 续保标记，－2非续保
        }

        if (this.mLCCustomerImpartBLSet.size() > 0)
        { //是否有告知信息
            mLCPolBL.setImpartFlag("1");
        }
        else
        {
            mLCPolBL.setImpartFlag("0");
        }
        if (this.mLCBnfBLSet.size() > 0)
        { //是否有受益人信息
            mLCPolBL.setBnfFlag("1");
        }
        else
        {
            mLCPolBL.setBnfFlag("0");
        }

        // System.out.println("承保描述表完成！");
        boolean bankFlag = true;
        /*Lis5.3 upgrade get
         if ((mLCPolBL.getBankCode() == null)
         || (mLCPolBL.getBankAccNo() == null))
         {
         bankFlag = false;
         }

         else
         {
         if (mLCPolBL.getBankCode().trim().equals("")
         || mLCPolBL.getBankAccNo().trim().equals(""))
         {
         bankFlag = false;
         }
         }
         */
        // 银行授权书表
        //        if (mLCPolBL.getPayLocation().trim().equals("0") // 银行自动转账
        //                || mLCPolBL.getPayLocation().trim().equals("8") // 首期交费：银行转账
        //                || (mLCPolBL.getPayLocation().trim().equals("9")
        //                && (bankFlag == true)) //首期交费：现金交纳并且填写了银行编码和银行账号
        //        )
        //        {
        //            if (bankFlag == false)
        //            {
        //                CError tError = new CError();
        //                tError.moduleName = "ProposalBL";
        //                tError.functionName = "dealDataPerson";
        //                tError.errorMessage = "开户银行和银行账号不能为空";
        //                this.mErrors.addOneError(tError);
        //
        //                return false;
        //            }
        //            mLCBankAuthBL.setPolNo(mLCPolBL.getPolNo());
        //            mLCBankAuthBL.setPayGetFlag("0");
        //
        //            mLCBankAuthBL.setBankCode(mLCContSchema.getBankCode());
        //            mLCBankAuthBL.setBankAccNo(mLCContSchema.getBankAccNo());
        //
        //            mLCBankAuthBL.setManageCom(mLCPolBL.getManageCom());
        //            mLCBankAuthBL.setPayValidFlag("1"); // 开通银行代收
        //            mLCBankAuthBL.setState("1"); // 正常
        //            mLCBankAuthBL.setOperator(mLCPolBL.getOperator());
        //            mLCBankAuthBL.setMakeDate(theCurrentDate);
        //            mLCBankAuthBL.setMakeTime(theCurrentTime);
        //            mLCBankAuthBL.setModifyDate(theCurrentDate);
        //            mLCBankAuthBL.setModifyTime(theCurrentTime);
        //
        //            // 银行账户表
        //            mLCBankAccBL = new LCBankAccBL();
        //
        //            mLCBankAccBL.setBankCode(mLCContSchema.getBankCode());
        //            mLCBankAccBL.setBankAccNo(mLCContSchema.getBankAccNo());
        //            mLCBankAccBL.setAccName(mLCContSchema.getAccName());
        //
        //            mLCBankAccBL.setModifyDate(theCurrentDate);
        //            mLCBankAccBL.setModifyTime(theCurrentTime);
        //        }
        //        else
        //        {
        //            mLCBankAuthBL = null; // 银行授权书表
        //            mLCBankAccBL = null; // 银行账户表
        //        }
        // 受益人
        iMax = mLCBnfBLSet.size();
        for (i = 1; i <= iMax; i++)
        {
            LCBnfBL tLCBnfBL = new LCBnfBL();
            tLCBnfBL.setSchema(mLCBnfBLSet.get(i));
            tLCBnfBL.setInsuredNo(mLCInsuredBL.getInsuredNo());
            tLCBnfBL.setContNo(mLCContSchema.getContNo());
            /** 判断受益人份额位数,如果录入格式为100 则转变成 1 格式 */
            double bnfLot = tLCBnfBL.getBnfLot();
            if (bnfLot > 1 && bnfLot < 100)
            {
                tLCBnfBL.setBnfLot(bnfLot / 100);
            }
            if (iMax == 1)
            {
                tLCBnfBL.setBnfGrade("1");
            }
            tLCBnfBL.setPolNo(mLCPolBL.getPolNo());
            tLCBnfBL.setOperator(mGlobalInput.Operator);
            tLCBnfBL.setMakeDate(theCurrentDate);
            tLCBnfBL.setMakeTime(theCurrentTime);
            tStr = tStr.valueOf(i);
            tLCBnfBL.setBnfNo(tStr);
            tLCBnfBL.setDefaultFields();
            mLCBnfBLSetNew.add(tLCBnfBL);
        }

        //处理告知信息190
        LCCustomerImpartBL tLCCustomerImpartBL;
        iMax = mLCCustomerImpartBLSet.size();
        for (i = 1; i <= iMax; i++)
        {
            tLCCustomerImpartBL = new LCCustomerImpartBL();
            tLCCustomerImpartBL.setSchema(mLCCustomerImpartBLSet.get(i)
                    .getSchema());
            /*Lis5.3 upgrade set
             tLCCustomerImpartBL.setPolNo(mLCPolBL.getPolNo());
             */
            if ((tLCCustomerImpartBL.getCustomerNoType() != null)
                    && tLCCustomerImpartBL.getCustomerNoType().equals("A"))
            { // 投保人告知
                tLCCustomerImpartBL.setCustomerNo(mLCPolBL.getInsuredNo());
            }
            if ((tLCCustomerImpartBL.getCustomerNoType() != null)
                    && tLCCustomerImpartBL.getCustomerNoType().equals("I"))
            { // 被保人告知
                tLCCustomerImpartBL.setCustomerNo(mLCPolBL.getAppntNo());
            }
            tLCCustomerImpartBL.setDefaultFields();

            mLCCustomerImpartBLSetNew.add(tLCCustomerImpartBL);
        }

        // 特别约定
        LCSpecBL tLCSpecBL;
        iMax = mLCSpecBLSet.size();
        for (i = 1; i <= iMax; i++)
        {
            tLCSpecBL = new LCSpecBL();
            tLCSpecBL.setSchema(mLCSpecBLSet.get(i).getSchema());
            tStr = tStr.valueOf(i);
            String tSpecNo = PubFun1.CreateMaxNo("SpecNo", PubFun
                    .getNoLimit(mGlobalInput.ComCode));
            if (tSpecNo.trim().equals(""))
            {
                mErrors.addOneError("特别约定号生成失败!");
                return false;
            }
            tLCSpecBL.setSerialNo(tSpecNo);
            tLCSpecBL.setContNo(mLCPolBL.getPolNo());
            tLCSpecBL.setGrpContNo(mLCPolBL.getGrpContNo());
            tLCSpecBL.setPolNo(mLCPolBL.getPolNo());
            tLCSpecBL.setProposalNo(mLCPolBL.getProposalNo());

            tLCSpecBL.setOperator(mGlobalInput.Operator);
            tLCSpecBL.setDefaultFields();
            tLCSpecBL.setMakeDate(tLCSpecBL.getModifyDate());
            tLCSpecBL.setMakeTime(tLCSpecBL.getModifyTime());
            mLCSpecBLSetNew.add(tLCSpecBL);
        }
        //更新合同表相关信息
        mLCContSchema.setModifyDate(theCurrentDate);
        mLCContSchema.setModifyDate(theCurrentDate);
        //mLCContSchema.PayIntv
        //mLCContSchema.setPayMode
        //mLCContSchema.setPayLocation
        //mLCContSchema.setDisputedFlag
        //mLCContSchema.setOutPayFlag
        //mLCContSchema.setGetPolMode
        //mLCContSchema.setPeoples(mLCContSchema.getPeoples()+mLCPolBL.getInsuredPeoples());

        //        System.out.println("sunxy:mLCContSchema.getPrem()"+mLCContSchema.getPrem()) ;
        //        System.out.println("sunxy:mLCPolBL.getPrem()"+mLCPolBL.getPrem()) ;
        LCPolDB mLCPolDB = new LCPolDB();
        mLCPolDB.setPolNo(mLCPolBL.getPolNo());
        double hisPrem = 0;
        double hisAmnt = 0;
        double hisSumPrem = 0;
        double hisMult = 0;
        if (mLCPolDB.getInfo())
        {
            hisPrem = mLCPolDB.getPrem();
            hisAmnt = mLCPolDB.getAmnt();
            hisSumPrem = mLCPolDB.getSumPrem();
            hisMult = mLCPolDB.getMult();
        }
        mLCContSchema.setMult(mLCContSchema.getMult() + mLCPolBL.getMult()
                - hisSumPrem);
        mLCContSchema.setPrem(mLCContSchema.getPrem() + mLCPolBL.getPrem()
                - hisPrem);
        mLCContSchema.setAmnt(mLCContSchema.getAmnt() + mLCPolBL.getAmnt()
                - hisAmnt);
        mLCContSchema.setSumPrem(mLCContSchema.getSumPrem()
                + mLCPolBL.getSumPrem() - hisSumPrem);
        this.mLCContSchema.setCInValiDate(PubFun.calDate(mLCPolBL.getEndDate(),
                1, "D", null));

        //         if (mLCPolBL.getPaytoDate().compareTo(mLCContSchema.getPaytoDate())>0)
        //         mLCContSchema.setPaytoDate (mLCPolBL.getPaytoDate()); //最大交至日期
        //         if (mLCPolBL.getFirstPayDate().compareTo(mLCContSchema.getFirstPayDate())<0)
        //         mLCContSchema.setFirstPayDate(mLCPolBL.getFirstPayDate()); //最小首期缴费日期
        //         if (mLCPolBL.getCValiDate().compareTo(mLCContSchema.getCValiDate())<0)
        //         mLCContSchema.setCValiDate(mLCPolBL.getCValiDate()); //最小生效日期

        /** 处理核保信息，如果在人工核保信息中，需要补核保数据 */
        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
        tLCCUWMasterDB.setContNo(this.mLCContSchema.getContNo());
        if (StrTool.cTrim(this.EdorType).equals("")
                && mOperate.equals("INSERT||PROPOSAL"))
        {
            if (tLCCUWMasterDB.getCount() > 0)
            {
                System.out.println("处理人工核保险种信息");
                if (!dealUWInfo())
                {
                    return false;
                }
                System.out.println("完成人工核保信息填充");
            }
        }
        // 集体下的个人投保单信息
        if (mPolType.equals("2"))
        {
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCPolBL.setGrpPolNo(mLCGrpPolSchema.getGrpPolNo());

            if (mOperate.equals("INSERT||PROPOSAL"))
            {
                //如果添加个人，则更新集体状态
                mLCGrpContSchema.setApproveFlag("0"); //复核状态
                mLCGrpContSchema.setUWFlag("0"); //核保状态

                mLCGrpPolSchema.setApproveFlag("0"); //复核状态
                mLCGrpPolSchema.setUWFlag("0"); //核保状态

                //普通的集体下个人被保人数为1
                int NONAMENUM = 1;

                //如果保单类型是无名单：被保人人数不定，根据录入决定;如果保单类型是公共帐户：被保人人数为0，因为只是为生成集体帐户而用
                if (mLCPolBL.getPolTypeFlag().equals("1")
                        || mLCPolBL.getPolTypeFlag().equals("2"))
                {
                    NONAMENUM = mLCPolBL.getInsuredPeoples();
                }

                //如果不是集体下无名单加人--如果是--是否也应该加人，加费
                if (!mSavePolType.equals("4"))
                {
                    //mLCGrpContSchema.setPeoples(mLCGrpContSchema.getPeoples2()
                    //                              + (1 * NONAMENUM));

                    //由于精度问题添加该段代码 modify by sunxy 2005-01-16
                    //                    mLCGrpContSchema.setPrem(mLCGrpContSchema.getPrem()
                    //                                              + mLCPolBL.getPrem());
                    //                    mLCGrpContSchema.setAmnt(mLCGrpContSchema.getAmnt()
                    //                                              + mLCPolBL.getAmnt());
                    //                    mLCGrpContSchema.setMult(mLCGrpContSchema.getMult()
                    //                                              + mLCPolBL.getMult());
                    //
                    //                    mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema.getPeoples2()
                    //                                                + (1 * NONAMENUM));
                    //                    mLCGrpPolSchema.setPrem(mLCGrpPolSchema.getPrem()
                    //                                            + mLCPolBL.getPrem());
                    //                    mLCGrpPolSchema.setAmnt(mLCGrpPolSchema.getAmnt()
                    //                                            + mLCPolBL.getAmnt());

                    //由于精度问题添加该段代码 add by sunxy 2005-01-16
                    String strCalGrpPolPrem = mDecimalFormat
                            .format(mLCGrpPolSchema.getPrem()
                                    + mLCPolBL.getPrem()); //转换计算后的保费(规定的精度)
                    String strCalGrpPolAmnt = mDecimalFormat
                            .format(mLCGrpPolSchema.getAmnt()
                                    + mLCPolBL.getAmnt()); //转换计算后的保额
                    String strCalGrpContPrem = mDecimalFormat
                            .format(mLCGrpContSchema.getPrem()
                                    + mLCPolBL.getPrem()); //转换计算后的保费(规定的精度)
                    String strCalGrpContAmnt = mDecimalFormat
                            .format(mLCGrpContSchema.getAmnt()
                                    + mLCPolBL.getAmnt()); //转换计算后的保额
                    double calGrpPolPrem = Double.parseDouble(strCalGrpPolPrem);
                    double calGrpPolAmnt = Double.parseDouble(strCalGrpPolAmnt);

                    double calGrpContPrem = Double
                            .parseDouble(strCalGrpContPrem);
                    double calGrpContAmnt = Double
                            .parseDouble(strCalGrpContAmnt);

                    mLCGrpPolSchema.setPrem(calGrpPolPrem);
                    mLCGrpPolSchema.setAmnt(calGrpPolAmnt);
                    mLCGrpContSchema.setPrem(calGrpContPrem);
                    mLCGrpContSchema.setAmnt(calGrpContAmnt);

                    mLCGrpContSchema.setMult(mLCGrpContSchema.getMult()
                            + mLCPolBL.getMult());

                    mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema.getPeoples2()
                            + (1 * NONAMENUM));

                }
            }
            if (mOperate.equals("UPDATE||PROPOSAL"))
            {
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(mLCPolBL.getPolNo());
                if (tLCPolDB.getInfo() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "个人投保单表取数失败";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                if ((tLCPolDB.getApproveFlag() == null)
                        || (tLCPolDB.getUWFlag() == null))
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "该个人投保单的复核状态或者核保状态存在问题！请告知信息技术部核实";
                    this.mErrors.addOneError(tError);

                    return false;
                }

                //判断如果更新的个人的复核状态或核保状态发生变化，更新集体状态
                if (!(tLCPolDB.getApproveFlag().equals(
                        mLCPolBL.getApproveFlag()) && tLCPolDB.getApproveFlag()
                        .equals(mLCPolBL.getApproveFlag())))
                {

                    mLCGrpContSchema.setApproveFlag("0"); //复核状态
                    mLCGrpContSchema.setUWFlag("0"); //核保状态

                    mLCGrpPolSchema.setApproveFlag("0"); //复核状态
                    mLCGrpPolSchema.setUWFlag("0"); //核保状态

                }

                int NONAMENUM = 1;

                //如果不是集体下无名单加人

                if (!mSavePolType.equals("4"))
                {
                    //上边已经加过mLCPolBL的prem,amnt,mult,sumprem,所以在这里不再加了
                    String strCalContPrem = mDecimalFormat.format(mLCContSchema
                            .getPrem()
                            - tLCPolDB.getPrem()); //转换计算后的保费(规定的精度)
                    String strCalContAmnt = mDecimalFormat.format(mLCContSchema
                            .getAmnt()
                            - tLCPolDB.getAmnt()); //转换计算后的保额
                    String strCalContSumPrem = mDecimalFormat
                            .format(mLCContSchema.getSumPrem()
                                    - tLCPolDB.getSumPrem());
                    double calContPrem = Double.parseDouble(strCalContPrem);
                    double calContAmnt = Double.parseDouble(strCalContAmnt);
                    double calContSumPrem = Double
                            .parseDouble(strCalContSumPrem);
                    System.out.println(" calContPrem " + calContPrem
                            + " calContAmnt " + calContAmnt
                            + " mLCContSchema.getMult() - tLCPolDB.getMult() "
                            + (mLCContSchema.getMult() - tLCPolDB.getMult()));
                    mLCContSchema.setPrem(calContPrem);
                    mLCContSchema.setAmnt(calContAmnt);
                    mLCContSchema.setMult(mLCContSchema.getMult()
                            - tLCPolDB.getMult());
                    mLCContSchema.setSumPrem(calContSumPrem);

                    //修改集体下个人时，只有无名单可能对被保人数变化
                    if (mLCPolBL.getPolTypeFlag().equals("1"))
                    {
                        //                      mLCGrpContSchema.setPeoples2((mLCGrpContSchema
                        //                                                     .getPeoples2()
                        //                                                    + mLCPolBL
                        //                                                      .getInsuredPeoples())
                        //                                                    - tLCPolDB
                        //                                                      .getInsuredPeoples()
                        //                                                      );
                        mLCGrpPolSchema.setPeoples2((mLCGrpPolSchema
                                .getPeoples2() + mLCPolBL.getInsuredPeoples())
                                - tLCPolDB.getInsuredPeoples());
                        NONAMENUM = mLCPolBL.getInsuredPeoples();
                    }
                    else
                    {
                        //                        mLCGrpContSchema.setPeoples2( mLCGrpContSchema.getPeoples2() + 1 );
                        mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema
                                .getPeoples2() + 1);
                    }

                    //由于精度问题注释调该段代码 add by sunxy 2005-01-16
                    //                    mLCGrpContSchema.setPrem((mLCGrpContSchema.getPrem()
                    //                                                               + mLCPolBL.getPrem())
                    //                                                               - tLCPolDB.getPrem());
                    //                                       mLCGrpContSchema.setAmnt((mLCGrpContSchema.getAmnt()
                    //                                                               + mLCPolBL.getAmnt())
                    //                                                               - tLCPolDB.getAmnt());
                    //                                      mLCGrpContSchema.setMult((mLCGrpContSchema.getMult()
                    //                                                               + mLCPolBL.getMult())
                    //                                                               - tLCPolDB.getMult());
                    //
                    //
                    //                                       mLCGrpPolSchema.setPrem((mLCGrpPolSchema.getPrem()
                    //                                                               + mLCPolBL.getPrem())
                    //                                                               - tLCPolDB.getPrem());
                    //                                       mLCGrpPolSchema.setAmnt((mLCGrpPolSchema.getAmnt()
                    //                                                               + mLCPolBL.getAmnt())
                    //                                            - tLCPolDB.getAmnt());

                    //由于精度问题添加该段代码 add by sunxy 2005-01-16
                    String strCalGrpPolPrem = mDecimalFormat
                            .format((mLCGrpPolSchema.getPrem() + mLCPolBL
                                    .getPrem())
                                    - tLCPolDB.getPrem()); //转换计算后的保费(规定的精度)
                    String strCalGrpPolAmnt = mDecimalFormat
                            .format((mLCGrpPolSchema.getAmnt() + mLCPolBL
                                    .getAmnt())
                                    - tLCPolDB.getAmnt()); //转换计算后的保额

                    String strCalGrpContPrem = mDecimalFormat
                            .format((mLCGrpContSchema.getPrem() + mLCPolBL
                                    .getPrem())
                                    - tLCPolDB.getPrem()); //转换计算后的保费(规定的精度)
                    String strCalGrpContAmnt = mDecimalFormat
                            .format((mLCGrpContSchema.getAmnt() + mLCPolBL
                                    .getAmnt())
                                    - tLCPolDB.getAmnt()); //转换计算后的保额

                    double calGrpPolPrem = Double.parseDouble(strCalGrpPolPrem);
                    double calGrpPolAmnt = Double.parseDouble(strCalGrpPolAmnt);

                    double calGrpContPrem = Double
                            .parseDouble(strCalGrpContPrem);
                    double calGrpContAmnt = Double
                            .parseDouble(strCalGrpContAmnt);

                    mLCGrpContSchema.setPrem(calGrpContPrem);
                    mLCGrpContSchema.setAmnt(calGrpContAmnt);
                    mLCGrpContSchema
                            .setMult((mLCGrpContSchema.getMult() + mLCPolBL
                                    .getMult())
                                    - tLCPolDB.getMult());
                    mLCGrpPolSchema.setPrem(calGrpPolPrem);
                    mLCGrpPolSchema.setAmnt(calGrpContAmnt);

                }
            }
        }

        return true;
    }

    /**
     * dealUWInfo
     *
     * @return Object
     */
    private boolean dealUWInfo()
    {
        tLCUWMasterSchema = new LCUWMasterSchema();
        tLCUWMasterSchema.setContNo(mLCPolBL.getContNo());
        tLCUWMasterSchema.setGrpContNo(mLCPolBL.getGrpContNo());
        tLCUWMasterSchema.setPolNo(mLCPolBL.getPolNo());
        tLCUWMasterSchema.setProposalContNo(mLCPolBL.getContNo());
        tLCUWMasterSchema.setProposalNo(mLCPolBL.getProposalNo());
        tLCUWMasterSchema.setUWNo(1);
        tLCUWMasterSchema.setInsuredNo(mLCPolBL.getInsuredNo());
        tLCUWMasterSchema.setInsuredName(mLCPolBL.getInsuredName());
        tLCUWMasterSchema.setAppntNo(mLCPolBL.getAppntNo());
        tLCUWMasterSchema.setAppntName(mLCPolBL.getAppntName());
        tLCUWMasterSchema.setAgentCode(mLCPolBL.getAgentCode());
        tLCUWMasterSchema.setAgentGroup(mLCPolBL.getAgentGroup());
        tLCUWMasterSchema.setUWGrade("0"); //核保级别
        tLCUWMasterSchema.setAppGrade("0"); //申报级别
        tLCUWMasterSchema.setPostponeDay("");
        tLCUWMasterSchema.setPostponeDate("");
        tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
        tLCUWMasterSchema.setState("5");
        tLCUWMasterSchema.setPassFlag("5");
        tLCUWMasterSchema.setHealthFlag("0");
        tLCUWMasterSchema.setSpecFlag("0");
        tLCUWMasterSchema.setQuesFlag("0");
        tLCUWMasterSchema.setReportFlag("0");
        tLCUWMasterSchema.setChangePolFlag("0");
        tLCUWMasterSchema.setPrintFlag("0");
        tLCUWMasterSchema.setManageCom(mLCPolBL.getManageCom());
        tLCUWMasterSchema.setUWIdea("");
        tLCUWMasterSchema.setUpReportContent("");
        tLCUWMasterSchema.setOperator(mLCPolBL.getOperator()); //操作员
        tLCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
        tLCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
        tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        tLCUWSubSchema = new LCUWSubSchema();
        tLCUWSubSchema.setUWNo(1); //第1次核保
        tLCUWSubSchema.setContNo(tLCUWMasterSchema.getContNo());
        tLCUWSubSchema.setPolNo(tLCUWMasterSchema.getPolNo());
        tLCUWSubSchema.setGrpContNo(tLCUWMasterSchema.getGrpContNo());
        tLCUWSubSchema.setProposalContNo(tLCUWMasterSchema.getProposalContNo());
        tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo());
        tLCUWSubSchema.setInsuredNo(tLCUWMasterSchema.getInsuredNo());
        tLCUWSubSchema.setInsuredName(tLCUWMasterSchema.getInsuredName());
        tLCUWSubSchema.setAppntNo(tLCUWMasterSchema.getAppntNo());
        tLCUWSubSchema.setAppntName(tLCUWMasterSchema.getAppntName());
        tLCUWSubSchema.setAgentCode(tLCUWMasterSchema.getAgentCode());
        tLCUWSubSchema.setAgentGroup(tLCUWMasterSchema.getAgentGroup());
        tLCUWSubSchema.setUWGrade(tLCUWMasterSchema.getUWGrade()); //核保级别
        tLCUWSubSchema.setAppGrade(tLCUWMasterSchema.getAppGrade()); //申请级别
        tLCUWSubSchema.setAutoUWFlag(tLCUWMasterSchema.getAutoUWFlag());
        tLCUWSubSchema.setState(tLCUWMasterSchema.getState());
        tLCUWSubSchema.setPassFlag(tLCUWMasterSchema.getState());
        tLCUWSubSchema.setPostponeDay(tLCUWMasterSchema.getPostponeDay());
        tLCUWSubSchema.setPostponeDate(tLCUWMasterSchema.getPostponeDate());
        tLCUWSubSchema.setUpReportContent(tLCUWMasterSchema
                .getUpReportContent());
        tLCUWSubSchema.setHealthFlag(tLCUWMasterSchema.getHealthFlag());
        tLCUWSubSchema.setSpecFlag(tLCUWMasterSchema.getSpecFlag());
        tLCUWSubSchema.setSpecReason(tLCUWMasterSchema.getSpecReason());
        tLCUWSubSchema.setQuesFlag(tLCUWMasterSchema.getQuesFlag());
        tLCUWSubSchema.setReportFlag(tLCUWMasterSchema.getReportFlag());
        tLCUWSubSchema.setChangePolFlag(tLCUWMasterSchema.getChangePolFlag());
        tLCUWSubSchema.setChangePolReason(tLCUWMasterSchema
                .getChangePolReason());
        tLCUWSubSchema.setAddPremReason(tLCUWMasterSchema.getAddPremReason());
        tLCUWSubSchema.setPrintFlag(tLCUWMasterSchema.getPrintFlag());
        tLCUWSubSchema.setPrintFlag2(tLCUWMasterSchema.getPrintFlag2());
        tLCUWSubSchema.setUWIdea(tLCUWMasterSchema.getUWIdea());
        tLCUWSubSchema.setOperator(tLCUWMasterSchema.getOperator()); //操作员
        tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom());
        tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        if (mOperate.equals("DELETE||PROPOSAL"))
        {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData
                    .getObjectByObjectName("GlobalInput", 0));

            // 保单
            mLCPolBL.setSchema((LCPolSchema) cInputData.getObjectByObjectName(
                    "LCPolSchema", 0));
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(mLCPolBL.getProposalNo());
            if (!tLCPolDB.getInfo())
            {
                CError.buildErr(this, "查询险种保单失败！");
                return false;
            }
            mLCPolBL.setSchema(tLCPolDB.getSchema());
        }
        else
        {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData
                    .getObjectByObjectName("GlobalInput", 0));

            // 前台向后台传递的数据容器：放置一些不能通过Schema传递的变量
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mTransferData == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
                this.mErrors.addOneError(tError);

                return false;
            }

            if (mTransferData.getValueByName("GrpImport") != null)
            {
                mGrpImportFlag = true;
                //传入主险保单,因为前面有LCPolSchema,为区别索引，特从第10个为止开始查找
                LCPolBL tMainPolBL = (LCPolBL) cInputData
                        .getObjectByObjectName("LCPolBL", 10);
                if (tMainPolBL == null)
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "传入主险保单不能为空!";
                    this.mErrors.addOneError(tError);

                    return false;

                }
                mainLCPolBL = tMainPolBL;
                //mainLCPolBL .setSchema( tMainPol  );
            }
            //合同
            mLCContSchema.setSchema((LCContSchema) cInputData
                    .getObjectByObjectName("LCContSchema", 0));
            if (mLCContSchema.getContNo() == null)
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在接受数据时没有得到合同的数据!";
                this.mErrors.addOneError(tError);

                return false;

            }
            else
            {
                if (!mGrpImportFlag)
                {
                    LCContDB mLCContDB = new LCContDB();
                    mLCContDB.setSchema(mLCContSchema);
                    mLCContDB.getInfo();
                    if (mLCContDB.mErrors.needDealError())
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "查询合同的数据失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    mLCContSchema.setSchema(mLCContDB);
                }
            }
            // 保单
            mLCPolBL.setSchema((LCPolSchema) cInputData.getObjectByObjectName(
                    "LCPolSchema", 0));

            if (mTransferData == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
                this.mErrors.addOneError(tError);

                return false;
            }
            //mLCPolBL.setCValiDate(mLCContSchema.getCValiDate());
            //PQ,yuanaq注掉
            //            if (mTransferData.getValueByName("samePersonFlag") == null)
            //            {
            //                // @@错误处理
            //                CError tError = new CError();
            //                tError.moduleName = "ProposalBL";
            //                tError.functionName = "prepareData";
            //                tError.errorMessage = "没有接收到samePersonFlag标志。";
            //                this.mErrors.addOneError(tError);
            //
            //                return false;
            //            }
            if (("1").equals(mLCContSchema.getContType()))
            {
                //投保人信息，被保险人信息从数据库里查询
                if (!mGrpImportFlag)
                {
                    mLCAppntBL.setSchema((LCAppntSchema) cInputData
                            .getObjectByObjectName("LCAppntSchema", 0));
                    LCAppntDB tLCAppntDB = new LCAppntDB();
                    tLCAppntDB.setContNo(mLCAppntBL.getContNo());
                    tLCAppntDB.setAppntNo(mLCAppntBL.getAppntNo());
                    if (tLCAppntDB.getInfo() == false)
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "没有查到传入的投保人客户信息!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    mLCAppntBL.setSchema(tLCAppntDB);
                }

            }
            else
            {

                mLCGrpAppntBL.setSchema((LCGrpAppntSchema) cInputData
                        .getObjectByObjectName("LCGrpAppntSchema", 0));
                LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
                tLCGrpAppntDB.setGrpContNo(mLCGrpAppntBL.getGrpContNo());
                tLCGrpAppntDB.setCustomerNo(mLCGrpAppntBL.getCustomerNo());
                if (tLCGrpAppntDB.getInfo() == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "没有查到传入的集体投保人客户信息!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                mLCGrpAppntBL.setSchema(tLCGrpAppntDB);
                // tLCGrpAppntDB.setSchema(tLCGrpAppntDB);

            }

            //被保险人
            //mLCInsuredBLSet.set((LCInsuredSet) cInputData.getObjectByObjectName("LCInsuredSet",
            //                                                                     0));
            mLCInsuredBL.setSchema((LCInsuredSchema) cInputData
                    .getObjectByObjectName("LCInsuredSchema", 0));
            if (!mGrpImportFlag)
            {
                LCInsuredDB tLCInsuredDB = new LCInsuredDB();
                tLCInsuredDB.setContNo(mLCInsuredBL.getContNo());
                tLCInsuredDB.setInsuredNo(mLCInsuredBL.getInsuredNo());
                if (tLCInsuredDB.getInfo() == false)
                {

                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "没有查到传入的被保险人客户信息!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                mLCInsuredBL.setSchema(tLCInsuredDB);
            }
            //连带被保险人
            mLCInsuredRelatedSet.set((LCInsuredRelatedSet) cInputData
                    .getObjectByObjectName("LCInsuredRelatedSet", 0));
            // 受益人
            mLCBnfBLSet.set((LCBnfSet) cInputData.getObjectByObjectName(
                    "LCBnfSet", 0));

            // 告知信息
            mLCCustomerImpartBLSet.set((LCCustomerImpartSet) cInputData
                    .getObjectByObjectName("LCCustomerImpartSet", 0));

            int impartCount = mLCCustomerImpartBLSet.size();
            for (int i = 1; i <= impartCount; i++)
            {
                LCCustomerImpartSchema tImpart = (LCCustomerImpartSchema) mLCCustomerImpartBLSet
                        .get(i);
                String impartCode = tImpart.getImpartCode();
                String impartVer = tImpart.getImpartVer();
                String customerType = tImpart.getCustomerNoType();
                for (int j = i + 1; j <= impartCount; j++)
                {
                    LCCustomerImpartSchema tImpart1 = (LCCustomerImpartSchema) mLCCustomerImpartBLSet
                            .get(j);
                    String impartCode1 = tImpart1.getImpartCode();
                    String impartVer1 = tImpart1.getImpartVer();
                    String customerType1 = tImpart1.getCustomerNoType();
                    if (impartCode.equals(impartCode1)
                            && impartVer.equals(impartVer1)
                            && customerType.equals(customerType1))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "prepareData";
                        tError.errorMessage = "客户告知录入重复。";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    // end of if
                }
                // end of for
            }
            // end of for

            // 特别约定
            mLCSpecBLSet.set((LCSpecSet) cInputData.getObjectByObjectName(
                    "LCSpecSet", 0));

            // 一般责任信息(接收传入的单个责任信息)
            if (cInputData.getObjectByObjectName("LCDutySchema", 0) != null)
            {
                mLCDutyBL.setSchema((LCDutySchema) cInputData
                        .getObjectByObjectName("LCDutySchema", 0));
                if (mLCDutyBL.getPayEndYear() > 0)
                {
                    if ((mLCDutyBL.getPayEndYearFlag() == null)
                            || mLCDutyBL.getPayEndYearFlag().equals(""))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "责任项的缴费期间单位PayEndYearFlag不能为空!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                }
            }

            // 可选责任(接收传入的多个责任信息)
            LCDutySet tLCDutySet = (LCDutySet) cInputData
                    .getObjectByObjectName("LCDutySet", 0);

            //初始化不需要责任的标记为假-该标记针对传入的单个责任做判断
            boolean isNullDuty = false;
            if ((mLCDutyBL.getPayEndDate() == null)
                    && (mLCDutyBL.getPayEndYear() == 0)
                    && (mLCDutyBL.getPayEndYearFlag() == null)
                    && (mLCDutyBL.getGetYearFlag() == null)
                    && (mLCDutyBL.getGetStartType() == null)
                    && (mLCDutyBL.getGetYear() == 0)
                    && (mLCDutyBL.getEndDate() == null)
                    && (mLCDutyBL.getInsuYear() == 0)
                    && (mLCDutyBL.getInsuYearFlag() == null)
                    && (mLCDutyBL.getPayIntv() < 0))
            {
                isNullDuty = true;
            }

            //如果传入的多个责任信息和传入的单个责任信息为空，则需要责任标记为假。
            if ((tLCDutySet == null) && isNullDuty)
            {
                mNeedDuty = false;
            }
            else
            {
                //如果传入的可选责任集合为空，保存录入的单个责任
                if (tLCDutySet == null)
                {
                    mLCDutyBLSet1.add(mLCDutyBL.getSchema());
                }
                else
                {
                    //如果有可选责任，保存可选责任
                    mLCDutyBLSet1.set(tLCDutySet);
                }
                mNeedDuty = true;
            }
            //            if (mTransferData.getValueByName("samePersonFlag").equals("0")
            //                    && (mLCAppntBL == null))
            //            {
            //                // @@错误处理
            //                CError tError = new CError();
            //                tError.moduleName = "ProposalBL";
            //                tError.functionName = "getInputData";
            //                tError.errorMessage = "投保人与被保人不是同一个人时，投保人数据不能为空!";
            //                this.mErrors.addOneError(tError);
            //
            //                return false;
            //            }
            if ((mLCPolBL == null) || (mLCInsuredBLSet == null)
                    || (mLCBnfBLSet == null)
                    || (mLCCustomerImpartBLSet == null)
                    || (mLCSpecBLSet == null) || (mLCContSchema == null))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在投保数据接受时没有得到足够的数据，请您确认有：合同，险种,投保人,被保人,受益人,告知信息,特别约定的信息!";
                this.mErrors.addOneError(tError);

                return false;
            }
            //PQ 现在根据LCCont里的账户信息判断
            //Lis5.3 upgrade get
            if (StrTool.compareString(mLCPolBL.getPayLocation(), "0")
                    && (StrTool.cTrim(mLCContSchema.getBankCode()).equals("") || StrTool
                            .cTrim(mLCContSchema.getBankAccNo()).equals("")))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "收费方式选择了银行自动转账，但是合同信息中没有录入开户行和银行账号信息!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //判断是否删除应收总表信息
            //            if (mTransferData.getValueByName("samePersonFlag").equals("1"))
            //            {
            //                deleteAccNo = true;
            //            }

            //判断保存的类型是
            if (mTransferData.getValueByName("SavePolType") != null)
            {
                mSavePolType = (String) mTransferData
                        .getValueByName("SavePolType");
                if ((mSavePolType == null) || mSavePolType.trim().equals(""))
                {
                    mSavePolType = "0";
                }
            }
            //
            if (mTransferData.getValueByName("EdorType") != null)
            {
                EdorType = (String) mTransferData.getValueByName("EdorType");
                if ((EdorType == null) || EdorType.trim().equals(""))
                {
                    EdorType = "";
                }
            }
            //add by fuxin 获得本次保全的工单号码
            if (mTransferData.getValueByName("EdorNo") != null)
            {
            	EdorNo = (String) mTransferData.getValueByName("EdorNo");
            	System.out.println("本次保全的工单号码："+EdorNo);
            }

            mLCPolBL.setContNo(mLCContSchema.getContNo());
            mLCPolBL.setPrtNo(mLCContSchema.getPrtNo());
            //            mLCPolBL.setproposal(mLCContSchema.getGrpPolNo());
            mLCPolBL.setAgentCode(mLCContSchema.getAgentCode());
            mLCPolBL.setAgentGroup(mLCContSchema.getAgentGroup());
            mLCPolBL.setGrpContNo(mLCContSchema.getGrpContNo());
            mLCPolBL.setContType(mLCContSchema.getContType());
            mLCPolBL.setPolTypeFlag(mLCContSchema.getPolType());
            //判断个单时，置团体险种保单号全0
            if (mLCPolBL.getContType().equals("1"))
            {
                mLCPolBL.setGrpPolNo(SysConst.ZERONO);
            }

        }

        //得到磁盘投保传来的集体投保单信息，注意，这里是引用，后面对该对象的修改会保留下来
        mLCGrpPolSchema = (LCGrpPolSchema) cInputData.getObjectByObjectName(
                "LCGrpPolSchema", 0);
        mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName(
                "LCGrpContSchema", 0);
        //得到需要特殊处理的保费项纪录，如果存在这样的纪录(譬如：众悦年金分红)
        mDiskLCPremBLSet.set((LCPremSet) cInputData.getObjectByObjectName(
                "LCPremSet", 0));
        if (mDiskLCPremBLSet != null)
        {
            if (mDiskLCPremBLSet.size() > 0)
            {
                noCalFlag = true;

                //保单传入的保费项置为0--否则在修改的时候会翻倍
                mLCPolBL.setPrem(0);
                mLCPolBL.setStandPrem(0);
            }
        }

        //当磁盘投保时，会从外部传入该值-后面会判断
        mLMRiskAppSchema = (LMRiskAppSchema) cInputData.getObjectByObjectName(
                "LMRiskAppSchema", 0);
        mLMRiskSchema = (LMRiskSchema) cInputData.getObjectByObjectName(
                "LMRiskSchema", 0);
        mLDGrpSchema = (LDGrpSchema) cInputData.getObjectByObjectName(
                "LDGrpSchema", 0);

        //        //如果界面没有录入浮动费率，此时mLCPolBL的FloatRate=0，那么在后面的程序中，会将mLCPolBL的FloatRate字段置为1
        //        //保存浮动费率,录入的保费，保额:用于后面的浮动费率的计算
        //        //PQ 浮动费率改变到责任级，需要改变
        //        FloatRate = mLCPolBL.getFloatRate();
        //
        //        //如果界面录入浮动费率=-1，则计算浮动费率的标记为真
        //        if (FloatRate == ConstRate)
        //        {
        //            autoCalFloatRateFlag = true;
        //
        //            //将保单中的该子段设为1，后面描述算法中计算
        //            mLCPolBL.setFloatRate(1);
        //        }
        //        InputPrem = mLCPolBL.getPrem();
        //        InputAmnt = mLCPolBL.getAmnt();
        //        //浮动费率处理END
        //获得是否为保单计划变更标志:0 表示是:1 表示否
        if (mTransferData.getValueByName("ChangePlanFlag") != null)
        {
            //            System.out.println("----ChangePlanFlag------"
            //                               + mTransferData.getValueByName("ChangePlanFlag"));
            mChangePlanFlag = (String) mTransferData
                    .getValueByName("ChangePlanFlag");
        }
        mDiskLCGetSet = (LCGetSet) cInputData.getObjectByObjectName("LCGetSet",
                0);
        this.mRetireRate = (String) mTransferData.getValueByName("RetireRate");
        this.mIndustry = (String) mTransferData.getValueByName("Industry");
        this.mAvgAge = (String) mTransferData.getValueByName("AvgAge");
        this.mInsuredNum = (String) mTransferData.getValueByName("InsuredNum");
        return true;
    }

    /**
     * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        int i;
        int iMax;
        mInputData = new VData();
        try
        {
            // 不能改变顺序!!

            mInputData.add(mAppntType);
            mInputData.add(mPolType);
            //若是保全调用，则不需要更新保单信息
            if (EdorType == null || EdorType.equals(""))
            {
                mInputData.add(mLCContSchema);
            }
            mInputData.add(mLCPolBL.getSchema());
            mInputData.add(mLCAppntBL.getSchema());
            mInputData.add(mLCGrpAppntBL.getSchema());
            if (tLCUWMasterSchema != null && tLCUWSubSchema != null)
            {
                mInputData.add(this.tLCUWMasterSchema);
                mInputData.add(this.tLCUWSubSchema);
            }
            if (tLCUWMasterSchema != null
                    && mOperate.equals("UPDATE||PROPOSAL"))
            {
                System.out.println("进入到这里了!!!!!!!!!!!!!!!!!!!!!");
                mInputData.add(this.tLCUWMasterSchema);
            }
            //            if (mLCBankAuthBL != null)
            //            {
            //                mInputData.add(mLCBankAuthBL.getSchema());
            //            }
            //
            //            if (mLCBankAccBL != null)
            //            {
            //                mInputData.add(mLCBankAccBL.getSchema());
            //            }

            //            LCInsuredSet tLCInsuredSet = new LCInsuredSet();
            //            tLCInsuredSet = (LCInsuredSet) mLCInsuredBLSetNew;
            //            mInputData.add(tLCInsuredSet);

            LCBnfSet tLCBnfSet = new LCBnfSet();
            tLCBnfSet = (LCBnfSet) mLCBnfBLSetNew;
            mInputData.add(tLCBnfSet);

            LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
            tLCCustomerImpartSet.set(mLCCustomerImpartBLSetNew);
            mInputData.add(mLCCustomerImpartBLSetNew);

            LCSpecSet tLCSpecSet = new LCSpecSet();
            tLCSpecSet = (LCSpecSet) mLCSpecBLSetNew;
            mInputData.add(tLCSpecSet);

            LCPremSet tLCPremSet = new LCPremSet();
            tLCPremSet = (LCPremSet) mLCPremBLSet;
            mInputData.add(tLCPremSet);

            LCGetSet tLCGetSet = new LCGetSet();
            tLCGetSet = (LCGetSet) mLCGetBLSet;
            mInputData.add(tLCGetSet);

            mInputData.add(mLCDutyBLSet);

            if (mPolType.equals("2"))
            {
                mInputData.add(mLCGrpPolSchema);
                mInputData.add(mLCGrpContSchema);
                if (!mOperate.equals("DELETE||PROPOSAL"))
                {
                    //如果个人标记不为0-即保全标记,则此时保证团单的状态不变
                    if (!mLCPolBL.getAppFlag().equals("0"))
                    {
                        LCGrpPolDB tempLCGrpPolDB = new LCGrpPolDB();
                        tempLCGrpPolDB.setGrpPolNo(mLCGrpPolSchema
                                .getGrpPolNo());
                        if (tempLCGrpPolDB.getInfo())
                        {
                            mLCGrpPolSchema.setApproveFlag(tempLCGrpPolDB
                                    .getApproveFlag());
                            mLCGrpPolSchema.setUWFlag(tempLCGrpPolDB
                                    .getUWFlag());
                        }
                        else
                        {
                            mLCGrpPolSchema.setApproveFlag("9");
                            mLCGrpPolSchema.setUWFlag("9");
                        }
                    }
                }
            }
            mInputData.add(mLCInsuredRelatedSet);
            mInputData.add(mLDPersonSet);
            mInputData.add(mLJSPaySet);
            mInputData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);

            return false;
        }

        // 准备往前面传输的数据
        mResult.clear();
        mResult.add(mLCPolBL.getSchema());
        mResult.add(mLCDutyBLSet);

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 判断是否需要处理浮动附率：如果需要，将相关数据变更
     * 只处理页面录入保费，保额而没有录入浮动费率的情况：录入浮动费率的情况已经在险种计算公式描述中完成
     * 在此之前，如果传入浮动费率，计算公式中已经计算过浮动费率
     *
     * @param pLCPolBL 个人保单纪录
     * @param pLCPremBLSet 保费项
     * @param pLCGetBLSet 领取项
     * @param pLCDutyBLSet 责任项
     * @return boolean
     */
    private boolean dealFloatRate(LCPolBL pLCPolBL, LCPremBLSet pLCPremBLSet,
            LCGetBLSet pLCGetBLSet, LCDutyBLSet pLCDutyBLSet)
    {
        //如果界面上录入保费和保额：且浮动费率==0(不计算浮动费率,校验录入的保费保额是否和计算得到的数据匹配
        if ((FloatRate == 0) && (InputPrem > 0) && (InputAmnt > 0))
        {
            dealFormatModol(pLCPolBL, pLCPremBLSet, pLCGetBLSet, pLCDutyBLSet); //转换精度
            if ((InputPrem != pLCPolBL.getPrem())
                    || (InputAmnt != pLCPolBL.getAmnt()))
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealFloatRate";
                tError.errorMessage = "您录入的保费保额（" + InputPrem + "," + InputAmnt
                        + "）与计算得到的保费保额（" + pLCPolBL.getPrem() + ","
                        + pLCPolBL.getAmnt() + "）不符合！";
                this.mErrors.addOneError(tError);

                return false;
            }

            return true;
        }

        //如果界面上录入保费和保额：且浮动费率==规定的常量,那么认为该浮动费率需要自动计算得到
        //如果界面上录入保费和保额：且浮动费率<>规定的常量,那么认为浮动费率已经给出，且在前面的保费保额计算中用到。此处不用处理
        //该种险种的计算公式需要描述
        if ((autoCalFloatRateFlag == true) && (InputPrem > 0)
                && (InputAmnt > 0))
        {
            String strCalPrem = mDecimalFormat.format(pLCPolBL.getPrem()); //转换计算后的保费(规定的精度)
            String strCalAmnt = mDecimalFormat.format(pLCPolBL.getAmnt()); //转换计算后的保额
            double calPrem = Double.parseDouble(strCalPrem);
            double calAmnt = Double.parseDouble(strCalAmnt);

            //如果界面录入的保费保额与计算出来的保费保额相等，则认为浮动费率=1,不做任何处理返回
            if ((calPrem == InputPrem) && (calAmnt == InputAmnt))
            {
                return true;
            }

            if (pLCPolBL.getPrem() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealFloatRate";
                tError.errorMessage = "计算得到的保费为0,不能做浮动费率计算!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //浮动费率的计算公式为：P(现)*A(原)/(A(现)*P(原))
            double formatFloatRate = 0;
            FloatRate = (InputPrem * pLCPolBL.getAmnt())
                    / (InputAmnt * pLCPolBL.getPrem());
            formatFloatRate = Double.parseDouble(mFRDecimalFormat
                    .format(FloatRate));

            //更新个人保单
            pLCPolBL.setPrem(pLCPolBL.getPrem() * FloatRate);
            pLCPolBL.setStandPrem(pLCPolBL.getStandPrem() * FloatRate);
            pLCPolBL.setFloatRate(formatFloatRate); //存储转换后的浮动费率精度

            //更新保费项
            for (int j = 1; j <= pLCPremBLSet.size(); j++)
            {
                LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
                if (!tLCPremSchema.getPayPlanCode().substring(0, 6).equals(
                        "000000"))
                {
                    tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem()
                            * FloatRate);
                }

                pLCPremBLSet.set(j, tLCPremSchema);
            }

            //更新责任项
            for (int j = 1; j <= pLCDutyBLSet.size(); j++)
            {
                LCDutySchema tLCDutySchema = (LCDutySchema) pLCDutyBLSet.get(j);
                tLCDutySchema.setStandPrem(tLCDutySchema.getStandPrem()
                        * FloatRate);
                pLCDutyBLSet.set(j, tLCDutySchema);
            }

            dealFormatModol(pLCPolBL, pLCPremBLSet, pLCGetBLSet, pLCDutyBLSet); //转换精度

            //因为累计和可能存在进位误差（可能会有0.01元的），所以需要调整
            double sumDutyPrem = 0.0;
            for (int j = 1; j <= pLCDutyBLSet.size(); j++)
            {
                LCDutySchema tLCDutySchema = (LCDutySchema) pLCDutyBLSet.get(j);
                sumDutyPrem = sumDutyPrem + tLCDutySchema.getStandPrem();
            }

            if (sumDutyPrem != InputPrem)
            {
                double diffMoney = sumDutyPrem - InputPrem;

                //调整责任表
                for (int j = 1; j <= pLCDutyBLSet.size(); j++)
                {
                    if (pLCDutyBLSet.get(j).getStandPrem() > 0.0)
                    {
                        pLCDutyBLSet.get(j).setStandPrem(
                                pLCDutyBLSet.get(j).getStandPrem() - diffMoney);

                        break;
                    }
                }

                //调整保费表
                for (int j = 1; j <= pLCPremBLSet.size(); j++)
                {
                    if (pLCPremBLSet.get(j).getStandPrem() > 0)
                    {
                        pLCPremBLSet.get(j).setStandPrem(
                                pLCPremBLSet.get(j).getStandPrem() - diffMoney);

                        break;
                    }
                }
            }

            return true;
        }

        //如果界面上录入保费.而保额为0：且浮动费率==规定的常量,那么认为录入的保费即为经过计算后得到的保费，
        //此时，需要自动计算得到该浮动费率
        if ((autoCalFloatRateFlag == true) && (InputPrem > 0)
                && (InputAmnt == 0))
        {
            String strCalPrem = mDecimalFormat.format(pLCPolBL.getPrem()); //转换计算后的保费(规定的精度)
            double calPrem = Double.parseDouble(strCalPrem);

            //如果界面录入的保费与计算出来的保费相等，则认为浮动费率=1,不做任何处理返回
            if (calPrem == InputPrem)
            {
                return true;
            }

            if (pLCPolBL.getPrem() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealFloatRate";
                tError.errorMessage = "计算得到的保费为0,不能做浮动费率计算!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //浮动费率的计算公式为：P(现)/P(原)
            double formatFloatRate = 0;
            FloatRate = InputPrem / (pLCPolBL.getPrem());
            formatFloatRate = Double.parseDouble(mFRDecimalFormat
                    .format(FloatRate));

            //更新个人保单
            pLCPolBL.setPrem(InputPrem);
            pLCPolBL.setStandPrem(InputPrem);
            pLCPolBL.setFloatRate(formatFloatRate); //存储转换后的浮动费率精度

            //更新保费项
            for (int j = 1; j <= pLCPremBLSet.size(); j++)
            {
                LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
                if (!tLCPremSchema.getPayPlanCode().substring(0, 6).equals(
                        "000000"))
                {
                    tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem()
                            * FloatRate);
                }

                pLCPremBLSet.set(j, tLCPremSchema);
            }

            //更新责任项
            for (int j = 1; j <= pLCDutyBLSet.size(); j++)
            {
                LCDutySchema tLCDutySchema = (LCDutySchema) pLCDutyBLSet.get(j);
                tLCDutySchema.setStandPrem(tLCDutySchema.getStandPrem()
                        * FloatRate);
                pLCDutyBLSet.set(j, tLCDutySchema);

                if ((pLCDutyBLSet.get(j).getStandPrem() == 0)
                        && (pLCPremBLSet.get(j).getStandPrem() != 0))
                {
                    pLCPremBLSet.get(j).setStandPrem(0);
                }
            }

            dealFormatModol(pLCPolBL, pLCPremBLSet, pLCGetBLSet, pLCDutyBLSet); //转换精度

            //因为累计和可能存在进位误差（可能会有0.01元的），所以需要调整
            double sumDutyPrem = 0.0;
            for (int j = 1; j <= pLCDutyBLSet.size(); j++)
            {
                LCDutySchema tLCDutySchema = (LCDutySchema) pLCDutyBLSet.get(j);
                sumDutyPrem = sumDutyPrem + tLCDutySchema.getStandPrem();
            }

            if (sumDutyPrem != InputPrem)
            {
                double diffMoney = sumDutyPrem - InputPrem;

                //调整责任表
                for (int j = 1; j <= pLCDutyBLSet.size(); j++)
                {
                    if (pLCDutyBLSet.get(j).getStandPrem() > 0.0)
                    {
                        pLCDutyBLSet.get(j).setStandPrem(
                                pLCDutyBLSet.get(j).getStandPrem() - diffMoney);

                        break;
                    }
                }

                //调整保费表
                for (int j = 1; j <= pLCPremBLSet.size(); j++)
                {
                    if (pLCPremBLSet.get(j).getStandPrem() > 0)
                    {
                        pLCPremBLSet.get(j).setStandPrem(
                                pLCPremBLSet.get(j).getStandPrem() - diffMoney);

                        break;
                    }
                }
            }

            return true;
        }

        //否则，直接转换精度
        dealFormatModol(pLCPolBL, pLCPremBLSet, pLCGetBLSet, pLCDutyBLSet);

        return true;
    }

    /**
     * 处理传入的数据，转换合适的精度
     *
     * @param pLCPolBL LCPolBL
     * @param pLCPremBLSet LCPremBLSet
     * @param pLCGetBLSet LCGetBLSet
     * @param pLCDutyBLSet LCDutyBLSet
     * @return boolean
     */
    private boolean dealFormatModol(LCPolBL pLCPolBL, LCPremBLSet pLCPremBLSet,
            LCGetBLSet pLCGetBLSet, LCDutyBLSet pLCDutyBLSet)
    {
        String strCalPrem = mDecimalFormat.format(pLCPolBL.getPrem()); //转换计算后的保费(规定的精度)
        String strCalAmnt = mDecimalFormat.format(pLCPolBL.getAmnt()); //转换计算后的保额
        double calPrem = Double.parseDouble(strCalPrem);
        double calAmnt = Double.parseDouble(strCalAmnt);

        //更新个人保单
        pLCPolBL.setPrem(calPrem);
        pLCPolBL.setStandPrem(calPrem);
        pLCPolBL.setAmnt(calAmnt);

        //更新保费项
        double tempPrem = 0;

        //double tempAmnt=0;
        for (int j = 1; j <= pLCPremBLSet.size(); j++)
        {
            LCPremSchema tLCPremSchema = (LCPremSchema) pLCPremBLSet.get(j);
            tempPrem = Double.parseDouble(mDecimalFormat.format(tLCPremSchema
                    .getStandPrem()));
            tLCPremSchema.setStandPrem(tempPrem);
            pLCPremBLSet.set(j, tLCPremSchema);
        }

        //更新责任项
        for (int j = 1; j <= mLCDutyBLSet.size(); j++)
        {
            LCDutySchema tLCDutySchema = (LCDutySchema) mLCDutyBLSet.get(j);
            tempPrem = Double.parseDouble(mDecimalFormat.format(tLCDutySchema
                    .getStandPrem()));
            tLCDutySchema.setStandPrem(tempPrem);
            mLCDutyBLSet.set(j, tLCDutySchema);
        }

        return true;
    }

    /**
     * 校验字段
     *
     * @return boolean
     * @param operType String
     */
    private boolean CheckTBField(String operType)
    {
        // 保单   mLCPolBL  mLCGrpPolBL
        // 投保人 mLCAppntBL  mLCAppntGrpBL
        // 被保人 mLCInsuredBLSet mLCInsuredBLSetNew
        // 受益人 mLCBnfBLSet mLCBnfBLSetNew
        // 告知信息 mLCCustomerImpartBLSet mLCCustomerImpartBLSetNew
        // 特别约定 mLCSpecBLSet mLCSpecBLSetNew
        // 保费项表 mLCPremBLSet  保存特殊的保费项数据(目前针对磁盘投保，不用计算保费保额类型)
        // 给付项表 mLCGetBLSet
        //一般的责任信息 mLCDutyBL
        // 责任表 mLCDutyBLSet
        String strMsg = "";
        boolean MsgFlag = false;
        LCInsuredSchema tLCInsuredSchema = mLCInsuredBLSetNew.get(1);
        LCDutySchema tLCDutySchema = mLCDutyBLSet.get(1);

        String RiskCode = mLCPolBL.getRiskCode();

        try
        {
            VData tVData = new VData();
            CheckFieldCom tCheckFieldCom = new CheckFieldCom();

            //计算要素
            FieldCarrier tFieldCarrier = new FieldCarrier();
            tFieldCarrier.setAppAge(mLCPolBL.getInsuredAppAge()); //被保人年龄
            tFieldCarrier.setInsuredName(mLCPolBL.getInsuredName()); //被保人姓名
            tFieldCarrier.setSex(mLCPolBL.getInsuredSex()); //被保人性别
            tFieldCarrier.setInsuredNo(mLCPolBL.getInsuredNo()); //被保人号
            tFieldCarrier.setMult(mLCPolBL.getMult()); //投保份数
            tFieldCarrier.setPolNo(mLCPolBL.getPolNo()); //投保单号码
            tFieldCarrier.setContNo(mLCPolBL.getContNo()); //投保单合同号码
            tFieldCarrier.setMainPolNo(mLCPolBL.getMainPolNo()); //主险号码
            tFieldCarrier.setRiskCode(mLCPolBL.getRiskCode()); //险种编码
            tFieldCarrier.setCValiDate(mLCPolBL.getCValiDate()); //生效日期
            tFieldCarrier.setAmnt(mLCPolBL.getAmnt()); //保额
            tFieldCarrier.setInsuredBirthday(mLCPolBL.getInsuredBirthday()); //被保人出生日期
            tFieldCarrier.setInsuYear(mLCPolBL.getInsuYear()); //保险期间
            tFieldCarrier.setInsuYearFlag(mLCPolBL.getInsuYearFlag()); //保险期间单位
            tFieldCarrier.setPayEndYear(mLCPolBL.getPayEndYear()); //交费期间
            tFieldCarrier.setPayEndYearFlag(mLCPolBL.getPayEndYearFlag()); //交费期间单位
            tFieldCarrier.setPayIntv(mLCPolBL.getPayIntv()); //交费方式
            tFieldCarrier.setPayYears(mLCPolBL.getPayYears()); //交费年期
            tFieldCarrier.setOccupationType(mLCPolBL.getOccupationType()); //被保人职业类别
            tFieldCarrier.setGrpPolNo(mLCPolBL.getGrpPolNo());
            tFieldCarrier.setEndDate(mLCPolBL.getEndDate());

            tFieldCarrier.setPrem(mLCPolBL.getPrem()); //保费 add by liwei 20080530

            //            System.out.println("保单类型为："+mLCPolBL.getPolTypeFlag());
            tFieldCarrier.setPolTypeFlag(mLCPolBL.getPolTypeFlag());
            LCPremSchema tLCPremSchema = null;
            for (int i = 1; i <= mLCPremBLSet.size(); i++)
            {

                tLCPremSchema = mLCPremBLSet.get(i);

            }
            /*Lis5.3 upgrade get
             System.out.println("进入算法表的管理费比例为"+tLCPremSchema.getManageFeeRate());
             tFieldCarrier.setManageFeeRate(tLCPremSchema.getManageFeeRate());
             */
            //            System.out.println("进入算法表的责任编码为"+tLCPremSchema.getDutyCode());
            tFieldCarrier.setDutyCode(tLCPremSchema.getDutyCode());

            if (mLCPolBL.getStandbyFlag1() != null)
            {
                tFieldCarrier.setStandbyFlag1(mLCPolBL.getStandbyFlag1());
            }
            if (mLCPolBL.getStandbyFlag2() != null)
            {
                tFieldCarrier.setStandbyFlag2(mLCPolBL.getStandbyFlag2());
            }
            if (mLCPolBL.getStandbyFlag3() != null)
            {
                tFieldCarrier.setStandbyFlag3(mLCPolBL.getStandbyFlag3());
            }
            /*Lis5.3 upgrade get
             if (mLCPolBL.getStandbyFlag4() != null)
             {
             tFieldCarrier.setStandbyFlag4(mLCPolBL.getStandbyFlag4());
             }
             if (tLCInsuredSchema != null)
             {
             tFieldCarrier.setIDNo(tLCInsuredSchema.getIDNo()); //被保人证件号码（身份证）
             tFieldCarrier.setWorkType(tLCInsuredSchema.getWorkType()); //职业工种
             tFieldCarrier.setGrpName(tLCInsuredSchema.getGrpName()); //单位名称
             }
             */
            //            System.out.println("Prepare Data");
            tVData.add(tFieldCarrier);

            LMCheckFieldSchema tLMCheckFieldSchema = new LMCheckFieldSchema();
            tLMCheckFieldSchema.setRiskCode(RiskCode);
            if (!mLCPolBL.getContType().equals("1"))
            {
                String SQl = "select distinct riskwrapcode from lcriskwrap where grpcontno='"
                        + mLCGrpContSchema.getGrpContNo() + "'  with ur";
                System.out.println(mLCPolBL.getGrpContNo());
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = new SSRS();
                tSSRS = tExeSQL.execSQL(SQl);
                if (tSSRS.MaxRow > 0)
                {
                    tLMCheckFieldSchema.setRiskCode(tSSRS.GetText(1, 1));
                }
            }

            tLMCheckFieldSchema.setFieldName("TB" + operType); //投保
            tVData.add(tLMCheckFieldSchema);
            if (tCheckFieldCom.CheckField(tVData) == false)
            {
                this.mErrors.copyAllErrors(tCheckFieldCom.mErrors);

                return false;
            }
            else
            {
                //                System.out.println("Check Data");

                LMCheckFieldSet mLMCheckFieldSet = tCheckFieldCom
                        .GetCheckFieldSet();
                for (int n = 1; n <= mLMCheckFieldSet.size(); n++)
                {
                    LMCheckFieldSchema tField = mLMCheckFieldSet.get(n);
                    if ((tField.getReturnValiFlag() != null)
                            && tField.getReturnValiFlag().equals("N"))
                    {
                        if ((tField.getMsgFlag() != null)
                                && tField.getMsgFlag().equals("Y"))
                        {
                            MsgFlag = true;
                            strMsg = strMsg + tField.getMsg() + " ; ";

                            break;
                        }
                    }
                }
                if (MsgFlag == true)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "CheckTBField";
                    tError.errorMessage = "数据有误：" + strMsg;
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "CheckTBField";
            tError.errorMessage = "发生错误，请检验CheckField模块:" + ex;
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 准备处理数据
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean PrepareSubmitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (getInputData(cInputData) == false)
        {
            return false;
        }

        //数据操作校验
        if (checkData() == false)
        {
            return false;
        }

        //进行业务处理
        if (dealData() == false)
        {
            return false;
        }

        if (!this.mOperate.equals("DELETE||PROPOSAL"))
        {
            if (this.mOperate.equals("INSERT||PROPOSAL"))
            {
                if (CheckTBField("INSERT") == false)
                {
                    return false;
                }
            }
            else
            {
                if (CheckTBField("UPDATE") == false)
                {
                    return false;
                }
            }
        }

        //准备往后台的数据
        if (prepareOutputData() == false)
        {
            return false;
        }

        return true;
    }

    /**
     * 和PrepareSubmitData配合使用，返回准备数据
     *
     * @return com.sinosoft.utility.VData
     */
    public VData getSubmitResult()
    {
        return mInputData;
    }

    /**
     * 保费保额计算调用部分
     *
     * @return boolean
     */
    public boolean pubCal()
    {
        //是否传入领取项的标记getDutykind
        String getDutykind = (String) mTransferData
                .getValueByName("GetDutyKind");
        LCGetBL tLCGetBL = new LCGetBL();
        CalBL tCalBL;
        if (mNeedDuty)
        {

            LMRiskDutySet tLMRiskDutySet = mCRI.findRiskDutyByRiskCode(mLCPolBL
                    .getRiskCode());

            if (tLMRiskDutySet == null)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mCRI.mErrors);
                mCRI.mErrors.clearErrors();

                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "pubCal";
                tError.errorMessage = "LMRiskDuty表查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            //统一费率处理
            int Month = 0;
            int Day = 0;
            int Year = 0;
            if (!(mLCPolBL.getContType()).equals("1"))
            {
                //                LCGrpContDB mLCGrpContDB = new LCGrpContDB();
                //                mLCGrpContDB.setGrpContNo(mLCPolBL.getGrpContNo());
                //                LCGrpContSet mLCGrpContSet = new LCGrpContSet();
                //                mLCGrpContSet = mLCGrpContDB.query();
                //                if (mLCGrpContSet.size() != 1) {
                //                    // @@错误处理
                //                    CError tError = new CError();
                //                    tError.moduleName = "ProposalBL";
                //                    tError.functionName = "pubCal";
                //                    tError.errorMessage = "查询合同信息出错!";
                //                    this.mErrors.addOneError(tError);
                //                    return false;
                //                }
                if (mLCGrpContSchema.getCValiDate() == null)
                {
                    buildError("pubCal", "发现团单生效日期错误！");
                    return false;
                }
                if (mLCGrpContSchema.getCInValiDate() == null)
                {
                    buildError("pubCal", "发现团单终止日期错误！");
                    return false;
                }
                String StartDate = mLCGrpContSchema.getCValiDate();
                String EndDate = mLCGrpContSchema.getCInValiDate();
                Month = PubFun.calInterval2(StartDate, EndDate, "M");
                Day = PubFun.calInterval3(StartDate, EndDate, "D");
                Year = PubFun.calInterval2(StartDate, EndDate, "Y");
            }
            for (int i = 1; i <= mLCDutyBLSet1.size(); i++)
            {

                LCDutySchema tLCDutySchema = mLCDutyBLSet1.get(i);
                String dutyCode = tLMRiskDutySet.get(i).getDutyCode();
                // String calMode = "";

                //如果界面没有录入浮动费率，此时mLCDutySchema的FloatRate=0，那么在后面的程序中，会将mLCPolBL的FloatRate字段置为1
                //保存浮动费率,录入的保费，保额:用于后面的浮动费率的计算
                //FloatRate = tLCDutySchema.getFloatRate();
                if (!(mLCPolBL.getContType()).equals("1"))
                {
                    /**String strSql = "select * from lmduty where dutycode in (select dutycode from lmriskduty where riskcode='"
                     + mLCPolBL.getRiskCode() + "')";
                     LMDutyDB tLMDutyDB = new LMDutyDB();
                     LMDutySet tLMDutySet = tLMDutyDB.executeQuery(strSql);*/
                    LMDutySchema tLMDutySchema = new LMDutySchema();
                    tLMDutySchema = mCRI.findDutyByDutyCode(dutyCode);
                    if (tLMDutySchema == null)
                    {
                        System.out
                                .println("程序第3326行出错，请检查ProposalBL.java中的pubCal方法！");
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL.java";
                        tError.functionName = "pubCal";
                        tError.errorMessage = "责任描述有问题！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (StrTool.cTrim(tLMDutySchema.getInsuYearFlag()).equals(
                            "M")
                            && tLCDutySchema.getInsuYear() == 0)
                    {
                        tLCDutySchema.setInsuYear(Month);
                    }
                    else if (StrTool.cTrim(tLMDutySchema.getInsuYearFlag())
                            .equals("Y")
                            && tLCDutySchema.getInsuYear() == 0)
                    {
                        tLCDutySchema.setInsuYear(Year);
                    }
                    else if (StrTool.cTrim(tLMDutySchema.getInsuYearFlag())
                            .equals("D")
                            && tLCDutySchema.getInsuYear() == 0)
                    {
                        tLCDutySchema.setInsuYear(Day);
                    }
                    if (StrTool.cTrim(tLMDutySchema.getInsuYearFlag()).equals(
                            ""))
                    {
                        // 获取保障期间单位 
                        String tSqlInsuYearFlag = "select calfactorvalue "
                                + " from lccontplandutyparam "
                                + " where grpcontno = '"
                                + mLCPolBL.getGrpContNo() + "' "
                                + " and contplancode = '"
                                + mLCPolBL.getContPlanCode() + "' "
                                + " and riskcode = '" + mLCPolBL.getRiskCode()
                                + "' " + " and dutycode = '" + dutyCode + "' "
                                + " and Calfactor = 'InsuYearFlag' ";

                        String tInsuYearFlag = new ExeSQL()
                                .getOneValue(tSqlInsuYearFlag);

                        if (!"".equals(tInsuYearFlag)
                                && !"null".equals(tInsuYearFlag))
                        {
                            String tSqlInsuYear = "select calfactorvalue "
                                    + " from lccontplandutyparam "
                                    + " where grpcontno = '"
                                    + mLCPolBL.getGrpContNo() + "' "
                                    + " and contplancode = '"
                                    + mLCPolBL.getContPlanCode() + "' "
                                    + " and riskcode = '"
                                    + mLCPolBL.getRiskCode() + "' "
                                    + " and dutycode = '" + dutyCode + "' "
                                    + " and Calfactor = 'InsuYear' ";

                            String tInsuYear = new ExeSQL()
                                    .getOneValue(tSqlInsuYear);
                            if (!"".equals(tInsuYear)
                                    && !"null".equals(tInsuYear))
                            {
                                try
                                {
                                    int tTmpInsuYear = Integer
                                            .parseInt(tInsuYear);
                                    tLCDutySchema.setInsuYear(tTmpInsuYear);
                                }
                                catch (Exception e)
                                {
                                    CError tError = new CError();
                                    tError.moduleName = "ProposalBL";
                                    tError.functionName = "pubCal";
                                    tError.errorMessage = "缴费期间转换失败，请核对要是否填写正确!";
                                    this.mErrors.addOneError(tError);
                                    return false;
                                }
                            }
                            else
                            {

                                if ("D".equals(tInsuYearFlag))
                                    tLCDutySchema.setInsuYear(Day);
                                else if ("M".equals(tInsuYearFlag))
                                    tLCDutySchema.setInsuYear(Month);
                                else if ("Y".equals(tInsuYearFlag))
                                    tLCDutySchema.setInsuYear(Year);
                            }
                            tLCDutySchema.setInsuYearFlag(tInsuYearFlag);
                        }
                        else
                        {
                            tLCDutySchema.setInsuYearFlag(tLMDutySchema
                                    .getInsuYearFlag());
                        }
                        // --------------------
                    }
                    else
                    {
                        tLCDutySchema.setInsuYearFlag(tLMDutySchema
                                .getInsuYearFlag());
                    }
                }
                InputPrem = tLCDutySchema.getPrem();
                InputAmnt = tLCDutySchema.getAmnt();
                if (mLCDutyBLSet1.size() == 1 && InputPrem == 0.0
                        && InputAmnt == 0.0)
                {
                    InputPrem = mLCPolBL.getPrem();
                    InputAmnt = mLCPolBL.getAmnt();
                }
                //保存计算方向
                String calRule = tLCDutySchema.getCalRule();
                if (calRule == null)
                {
                    //默认为表定费率
                    tLCDutySchema.setCalRule("0");
                }
                //如果是统一费率，需要去默认值或者保险计划里取出统一费率的值,算出保费保额
                else if (calRule.equals("1"))
                {
                    LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
                    LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
//                  tLCContPlanDutyParamDB
//                  .setGrpContNo(mLCPolBL.getGrpContNo());
//          tLCContPlanDutyParamDB.setDutyCode(dutyCode);
//          tLCContPlanDutyParamDB.setRiskCode(mLCPolBL.getRiskCode());
//          tLCContPlanDutyParamDB.setMainRiskCode(mainLCPolBL
//                  .getRiskCode());
//          tLCContPlanDutyParamDB.setCalFactor("FloatRate");
//          if (StrTool.compareString(mLCInsuredBL.getContPlanCode(),
//                  ""))
//          {
//              tLCContPlanDutyParamDB.setContPlanCode("00");
//          }
//          else
//          {
//              tLCContPlanDutyParamDB.setContPlanCode(mLCInsuredBL
//                      .getContPlanCode());
//          }
          String sql = "select * from LCContPlanDutyParam where GrpContNo = '"+mLCPolBL.getGrpContNo()+"' and DutyCode = '"+dutyCode+"' and "
          					+ " RiskCode = '"+mLCPolBL.getRiskCode()+"' and "
          					+ " CalFactor = 'FloatRate' and ";
          if (StrTool.compareString(mLCInsuredBL.getContPlanCode(),
                  ""))
          {
          	sql +=" ContPlanCode = '00' and ";
          }
          else
          {
          	sql +=" ContPlanCode = '"+mLCInsuredBL.getContPlanCode()+"' with ur ";

          }
          tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.executeQuery(sql);
         
                    if (tLCContPlanDutyParamDB.mErrors.needDealError())
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "pubCal";
                        tError.errorMessage = "LCContPlanDutyParam查询失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    //如果在计划中没有取到,需要去默认值里去
                    if (tLCContPlanDutyParamSet.size() <= 0
                            || (!StrTool.compareString(mLCInsuredBL
                                    .getContPlanCode(), "") && StrTool
                                    .compareString(tLCContPlanDutyParamSet.get(
                                            1).getCalFactorValue(), "")))
                    {
//                      tLCContPlanDutyParamDB.setContPlanCode("00");
                    	sql = "select * from LCContPlanDutyParam where GrpContNo = '"+mLCPolBL.getGrpContNo()+"' and DutyCode = '"+dutyCode+"' and "
            					+ " RiskCode = '"+mLCPolBL.getRiskCode()+"' and "
            					+ " CalFactor = 'FloatRate' and ContPlanCode = '00' with ur";
                        tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.executeQuery(sql);
   
                        if (tLCContPlanDutyParamDB.mErrors.needDealError())
                        {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "pubCal";
                            tError.errorMessage = "LCContPlanDutyParam查询失败!";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                    }
                    if (tLCContPlanDutyParamSet.size() <= 0
                            || StrTool.compareString(tLCContPlanDutyParamSet
                                    .get(1).getCalFactorValue(), ""))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "pubCal";
                        tError.errorMessage = "计算方向为统一费率，但是在保险计划于默认值中均未查到统一费率值!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }

                    //取出统一费率
                    double oneFloatRate = Double
                            .parseDouble(tLCContPlanDutyParamSet.get(1)
                                    .getCalFactorValue());
                    //如果界面上录入保费：
                    if ((InputPrem > 0) && (InputAmnt > 0))
                    {
                        if (InputPrem != Double.parseDouble(mDecimalFormat
                                .format(InputAmnt * oneFloatRate)))
                        {
                            CError tError = new CError();
                            tError.moduleName = "CalBL";
                            tError.functionName = "calOneDuty";
                            tError.errorMessage = "您录入的保费保额（" + InputPrem + ","
                                    + InputAmnt + "）与统一费率（" + oneFloatRate
                                    + "）不符合！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }
                    else if ((InputPrem > 0))
                    {
                        InputAmnt = Double.parseDouble(mDecimalFormat
                                .format(InputPrem / oneFloatRate));
                    }
                    else if ((InputAmnt > 0))
                    {
                        InputPrem = Double.parseDouble(mDecimalFormat
                                .format(InputAmnt * oneFloatRate));
                    }
                    //autoCalFloatRateFlag = true;

                    //将保单中的该子段设为1，后面描述算法中计算
                    tLCDutySchema.setFloatRate(1);
                    tLCDutySchema.setPrem(InputPrem);
                    tLCDutySchema.setStandPrem(InputPrem);
                    tLCDutySchema.setAmnt(InputAmnt);
                    if (mLCDutyBLSet1.size() == 1)
                    {
                        mLCPolBL.setPrem(InputPrem);
                        mLCPolBL.setStandPrem(InputPrem);
                        mLCPolBL.setAmnt(InputAmnt);
                    }

                }
            }

            //需要传入责任计算
            if (StrTool.cTrim(getDutykind).equals(""))
            {
                // 传入LCPol的和页面录入的责任信息（单个或可选责任）数据,保存在tCalBL对象的内部成员中，后续将用到
                tCalBL = new CalBL(mLCPolBL, mLCDutyBLSet1, EdorType,
                        mTransferData);
            }
            else
            {
                LCGetBLSet tLCGetBLSet = new LCGetBLSet();
                // 传入投保单和领取项和责任项的数据
                tLCGetBL.setGetDutyKind(getDutykind);
                tLCGetBLSet.add(tLCGetBL);
                //mLCPolBL-传入的投保单信息 ， mLCDutyBLSet1-传入的责任信息 ， tLCGetBLSet-保存传入的getDutykind字段，其它为空。
                tCalBL = new CalBL(mLCPolBL, mLCDutyBLSet1, tLCGetBLSet,
                        EdorType, mTransferData);
            }
        }
        else
        {
            //不传入责任计算
            if (StrTool.cTrim(getDutykind).equals(""))
            {
                // 只传入LCPol的数据
                tCalBL = new CalBL(mLCPolBL, EdorType);
            }
            else
            {
                // 传入LCPol和LCGet的数据
                tLCGetBL.setGetDutyKind(getDutykind);

                LCGetBLSet tLCGetBLSet = new LCGetBLSet();
                tLCGetBLSet.add(tLCGetBL);
                tCalBL = new CalBL(mLCPolBL, tLCGetBLSet, EdorType);
            }
        }

        tCalBL.setNoCalFalg(noCalFlag); //将是否需要计算的标记传入计算类中
        if (!noCalFlag)
        { //如果需要计算
            //            System.out.println("before tCalBL.calPol()");
            if (tCalBL.calPol() == false)
            { //前面tCalBL的成员变量保存了传入的数据后，这里计算将用到这些成员变量

                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson-cal1";
                tError.errorMessage = tCalBL.mErrors.getFirstError()
                        + "(如果没有具体错误原因，请依次检查：1 交费方式和交费期间（或保险期间）是否匹配 2 保费，保额是否输入 3 相关要素（譬如年龄）是否超过费率计算范围 4 职业代码是否和计算相关) ";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        else
        {
            if (tCalBL.calPol2(mDiskLCPremBLSet) == false)
            { //前面tCalBL的成员变量保存了传入的数据后，这里计算将用到这些成员变量
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson-cal2";
                tError.errorMessage = tCalBL.mErrors.getFirstError()
                        + "(如果没有具体错误原因，请依次检查：1 交费方式和交费期间（或保险期间）是否匹配 2 保费，保额是否输入 3 相关要素（譬如年龄）是否超过费率计算范围 4 职业代码是否和计算相关) ";
                this.mErrors.addOneError(tError);

                return false;
            }
        }

        //取出计算的结果
        mLCPolBL.setSchema(tCalBL.getLCPol().getSchema());

        mLCPremBLSet = tCalBL.getLCPrem(); //得到的保费项集合不包括加费的保费项，所以在后面处理
        mLCGetBLSet = tCalBL.getLCGet();
        mLCDutyBLSet = tCalBL.getLCDuty();
        mLCPremBLSet.setPolNo(mLCPolBL.getPolNo());
        mLCGetBLSet.setPolNo(mLCPolBL.getPolNo());
        if (mOperate.equals("INSERT||PROPOSAL"))
        {
            mLCPolBL.setDefaultFields();
        }

        for (int i = 1; i <= mLCPremBLSet.size(); i++)
        {
            mLCPremBLSet.get(i).setOperator(mGlobalInput.Operator);
            mLCPremBLSet.get(i).setMakeDate(theCurrentDate);
            mLCPremBLSet.get(i).setMakeTime(theCurrentTime);
            mLCPremBLSet.get(i).setModifyDate(theCurrentDate);
            mLCPremBLSet.get(i).setModifyTime(theCurrentTime);
        }
        for (int i = 1; i <= mLCGetBLSet.size(); i++)
        {
            mLCGetBLSet.get(i).setOperator(mGlobalInput.Operator);
            mLCGetBLSet.get(i).setMakeDate(theCurrentDate);
            mLCGetBLSet.get(i).setMakeTime(theCurrentTime);
            mLCGetBLSet.get(i).setModifyDate(theCurrentDate);
            mLCGetBLSet.get(i).setModifyTime(theCurrentTime);
        }
        //将磁盘投保中的LCGet保存在已经计算完毕的LCGet中
        if (mDiskLCGetSet != null)
        {
            if (this.mDiskLCGetSet.size() > 0)
            {
                for (int i = 1; i <= mLCGetBLSet.size(); i++)
                {
                    LCGetSchema tLCGetSchema = mLCGetBLSet.get(i).getSchema();
                    for (int m = 1; m <= mDiskLCGetSet.size(); m++)
                    {
                        LCGetSchema calLCGetSchema = mDiskLCGetSet.get(m)
                                .getSchema();
                        //同一责任
                        if (tLCGetSchema.getGetDutyCode().equals(
                                calLCGetSchema.getGetDutyCode()))
                        {
                            if (tLCGetSchema.getActuGet() == 0.0
                                    && calLCGetSchema.getActuGet() > 0)
                            {
                                tLCGetSchema.setActuGet(calLCGetSchema
                                        .getActuGet());
                            }
                            if (tLCGetSchema.getAddRate() == 0.0
                                    && calLCGetSchema.getAddRate() > 0)
                            {
                                tLCGetSchema.setAddRate(calLCGetSchema
                                        .getAddRate());
                            }
                            if (tLCGetSchema.getGetLimit() == 0.0
                                    && calLCGetSchema.getGetLimit() > 0)
                            {
                                tLCGetSchema.setGetLimit(calLCGetSchema
                                        .getGetLimit());
                            }
                            if (tLCGetSchema.getGetRate() == 0.0
                                    && calLCGetSchema.getGetRate() > 0)
                            {
                                tLCGetSchema.setGetRate(calLCGetSchema
                                        .getGetRate());
                            }
                            if (tLCGetSchema.getSumMoney() == 0.0
                                    && calLCGetSchema.getSumMoney() > 0)
                            {
                                tLCGetSchema.setSumMoney(calLCGetSchema
                                        .getSumMoney());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getAfterDie())
                                    .equals("")
                                    && !StrTool.cTrim(
                                            calLCGetSchema.getAfterDie())
                                            .equals(""))
                            {
                                tLCGetSchema.setAfterDie(calLCGetSchema
                                        .getAfterDie());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getBalaDate())
                                    .equals("")
                                    && !StrTool.cTrim(
                                            calLCGetSchema.getBalaDate())
                                            .equals(""))
                            {
                                tLCGetSchema.setBalaDate(calLCGetSchema
                                        .getBalaDate());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getCanGet()).equals(
                                    "")
                                    && !StrTool.cTrim(
                                            calLCGetSchema.getCanGet()).equals(
                                            ""))
                            {
                                tLCGetSchema.setCanGet(calLCGetSchema
                                        .getCanGet());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getGetEndDate())
                                    .equals("")
                                    && !StrTool.cTrim(
                                            calLCGetSchema.getGetEndDate())
                                            .equals(""))
                            {
                                tLCGetSchema.setGetEndDate(calLCGetSchema
                                        .getGetEndDate());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getGetMode())
                                    .equals("")
                                    && !StrTool.cTrim(
                                            calLCGetSchema.getGetMode())
                                            .equals(""))
                            {
                                tLCGetSchema.setGetMode(calLCGetSchema
                                        .getGetMode());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getNeedAcc())
                                    .equals("")
                                    && !StrTool.cTrim(
                                            calLCGetSchema.getNeedAcc())
                                            .equals(""))
                            {
                                tLCGetSchema.setNeedAcc(calLCGetSchema
                                        .getNeedAcc());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getNeedCancelAcc())
                                    .equals("")
                                    && !StrTool.cTrim(
                                            calLCGetSchema.getNeedCancelAcc())
                                            .equals(""))
                            {
                                tLCGetSchema.setNeedCancelAcc(calLCGetSchema
                                        .getNeedCancelAcc());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getState()).equals(
                                    "")
                                    && !StrTool
                                            .cTrim(calLCGetSchema.getState())
                                            .equals(""))
                            {
                                tLCGetSchema
                                        .setState(calLCGetSchema.getState());
                            }
                            if (StrTool.cTrim(tLCGetSchema.getUrgeGetFlag())
                                    .equals("")
                                    && !StrTool.cTrim(
                                            calLCGetSchema.getUrgeGetFlag())
                                            .equals(""))
                            {
                                tLCGetSchema.setUrgeGetFlag(calLCGetSchema
                                        .getUrgeGetFlag());
                            }
                        }
                    }
                    mLCGetBLSet.set(i, tLCGetSchema);
                }
            }
        }

        //将加费的保费项查询出来,并加入到保费项集合中
        String strSql = "select * from lcprem where polno='"
                + mLCPolBL.getPolNo()
                + "' and PayPlanCode like '000000%' with ur";
        LCPremDB tempLCPremDB = new LCPremDB();
        LCPremSet tempLCPremSet = new LCPremSet();
        tempLCPremSet = tempLCPremDB.executeQuery(strSql);
        mLCPremBLSet.add(tempLCPremSet);

        //        //处理浮动费率--houzm add
        //        if (!dealFloatRate(mLCPolBL, mLCPremBLSet, mLCGetBLSet, mLCDutyBLSet))
        //        {
        //            return false;
        //        }

        // 保费项
        for (int j = 1; j <= mLCPremBLSet.size(); j++)
        {
            LCPremSchema tLCPremSchema = (LCPremSchema) mLCPremBLSet.get(j);
            if (!tLCPremSchema.getPayPlanCode().substring(0, 6)
                    .equals("000000"))
            {
                if (tLCPremSchema.getPrem() <= 0)
                {
                    tLCPremSchema.setPrem(tLCPremSchema.getStandPrem());
                }
            }
            mLCPremBLSet.set(j, tLCPremSchema);
        }

        // 领取项
        for (int j = 1; j <= mLCGetBLSet.size(); j++)
        {
            LCGetSchema tLCGetSchema = (LCGetSchema) mLCGetBLSet.get(j);
            mLCGetBLSet.set(j, tLCGetSchema);
        }

        double sumPremLCPol = 0; //保单中的实际保费和

        // 责任项
        for (int j = 1; j <= mLCDutyBLSet.size(); j++)
        {
            LCDutySchema tLCDutySchema = (LCDutySchema) mLCDutyBLSet.get(j);

            double sumPremDuty = 0;
            for (int m = 1; m <= mLCPremBLSet.size(); m++)
            {
                if (mLCPremBLSet.get(m).getDutyCode().equals(
                        tLCDutySchema.getDutyCode()))
                {
                    //将该责任下的所有保费项累加
                    sumPremDuty = sumPremDuty + mLCPremBLSet.get(m).getPrem();
                }
            }
            tLCDutySchema.setPrem(sumPremDuty);
            tLCDutySchema.setFirstPayDate(mLCPolBL.getCValiDate());
            tLCDutySchema.setModifyDate(theCurrentDate);
            tLCDutySchema.setModifyTime(theCurrentTime);

            mLCDutyBLSet.set(j, tLCDutySchema);
            sumPremLCPol = sumPremLCPol + sumPremDuty;
        }
        mLCPolBL.setPrem(sumPremLCPol); //保存实际保费

        // 从责任表中取出第一条数据填充LCPol中的部分字段
        LCDutySchema tLCDutySchema1 = new LCDutySchema();
        //        System.out.println("mLCDutyBLSet"+mLCDutyBLSet.get(1));
        tLCDutySchema1.setSchema((LCDutySchema) mLCDutyBLSet.get(1));
        //        System.out.println("lcpol payintv:" + mLCPolBL.getPayIntv());
        //        System.out.println("lcduty payintv:" + tLCDutySchema1.getPayIntv());

        if (mLCPolBL.getPayIntv() == 0)
        {
            mLCPolBL.setPayIntv(tLCDutySchema1.getPayIntv());
        }
        if (mLCPolBL.getInsuYear() == 0)
        {
            mLCPolBL.setInsuYear(tLCDutySchema1.getInsuYear());
        }
        if (mLCPolBL.getPayEndYear() == 0)
        {
            mLCPolBL.setPayEndYear(tLCDutySchema1.getPayEndYear());
        }
        if (mLCPolBL.getGetYear() == 0)
        {
            mLCPolBL.setGetYear(tLCDutySchema1.getGetYear());
        }
        if (mLCPolBL.getAcciYear() == 0)
        {
            mLCPolBL.setAcciYear(tLCDutySchema1.getAcciYear());
        }
        if ((mLCPolBL.getInsuYearFlag() == null)
                && (tLCDutySchema1.getInsuYearFlag() != null))
        {
            mLCPolBL.setInsuYearFlag(tLCDutySchema1.getInsuYearFlag());
        }
        if ((mLCPolBL.getGetStartType() == null)
                && (tLCDutySchema1.getGetStartType() != null))
        {
            mLCPolBL.setGetStartType(tLCDutySchema1.getGetStartType());
        }
        if ((mLCPolBL.getGetYearFlag() == null)
                && (tLCDutySchema1.getGetYearFlag() != null))
        {
            mLCPolBL.setGetYearFlag(tLCDutySchema1.getGetYearFlag());
        }
        if ((StrTool.cTrim(mLCPolBL.getPayEndYearFlag()).equals(""))
                && (tLCDutySchema1.getPayEndYearFlag() != null))
        {
            mLCPolBL.setPayEndYearFlag(tLCDutySchema1.getPayEndYearFlag());
        }
        if ((mLCPolBL.getAcciYearFlag() == null)
                && (tLCDutySchema1.getAcciYearFlag() != null))
        {
            mLCPolBL.setAcciYearFlag(tLCDutySchema1.getAcciYearFlag());
        }

        return true;
    }

    /**
     * 校验连带被保险人年龄
     *
     * @param mLCInsuredRelatedSet LCInsuredRelatedSet
     * @return boolean
     */
    private boolean checkSLCInsured(LCInsuredRelatedSet mLCInsuredRelatedSet)
    {
        int tInsuredAge = 0;
        LCInsuredRelatedBL tLCInsuredRelatedBL = new LCInsuredRelatedBL();
        for (int i = 1; i <= mLCInsuredRelatedSet.size(); i++)
        {
            tLCInsuredRelatedBL.setSchema(mLCInsuredRelatedSet.get(i));

            String tInsuredAgeFlag = mLMRiskAppSchema.getInsuredAgeFlag();
            if ("D".equals(tInsuredAgeFlag))
                tInsuredAgeFlag = "D";
            else
                tInsuredAgeFlag = "Y";

            tInsuredAge = PubFun.calInterval(tLCInsuredRelatedBL.getBirthday(),
                    mLCPolBL.getCValiDate(), tInsuredAgeFlag);

            if ((tInsuredAge < mLMRiskAppSchema.getMinInsuredAge())
                    || ((tInsuredAge > mLMRiskAppSchema.getMaxInsuredAge()) && (mLMRiskAppSchema
                            .getMaxInsuredAge() > 0)))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson";
                tError.errorMessage = "连带被保人投保年龄不符合投保要求!";
                this.mErrors.addOneError(tError);

                return false;
            }

        }

        return true;
    }

    /**
     * 银行出单特殊处理
     *
     * @return boolean
     */
    private boolean dealDataForBank()
    {
        /*Lis5.3 upgrade get
         if ((mLCPolBL.getStandbyFlag3() != null)
         && (mLCPolBL.getStandbyFlag4() != null))
         {
         if (mLCPolBL.getStandbyFlag3().equals("Y"))
         {
         if (mLCPolBL.getStandbyFlag4().trim().length() != 20)
         {
         CError tError = new CError();
         tError.moduleName = "ProposalBL";
         tError.functionName = "dealDataForBank";
         tError.errorMessage = "银行出单标记为Y时必须录入长度为20位的银行出单保单号";
         this.mErrors.addOneError(tError);

         return false;
         }
         else
         {
         LCPolDB tLCPolDB = new LCPolDB();
         tLCPolDB.setStandbyFlag4(mLCPolBL.getStandbyFlag4());

         LCPolSet tLCPolSet = new LCPolSet();
         tLCPolSet = tLCPolDB.query();
         if (tLCPolSet.size() > 0)
         {
         CError tError = new CError();
         tError.moduleName = "ProposalBL";
         tError.functionName = "dealDataForBank";
         tError.errorMessage = "录入的银行出单保单号已经存在,不能保存!";
         this.mErrors.addOneError(tError);

         return false;
         }
         }
         }

         if (mLCPolBL.getStandbyFlag3().equals("N")
         && (mLCPolBL.getStandbyFlag4().trim().length() > 0))
         {
         CError tError = new CError();
         tError.moduleName = "ProposalBL";
         tError.functionName = "dealDataForBank";
         tError.errorMessage = "银行出单标记为N时不能录入银行出单的保单号!";
         this.mErrors.addOneError(tError);

         return false;
         }

         //如果银行出单标记为真且银行出单保单号录入，在不打印保单
         if (mLCPolBL.getStandbyFlag3().equals("Y")
         && (mLCPolBL.getStandbyFlag4().trim().length() > 0))
         {
         mLCPolBL.setPrintCount(1);
         mLCPolBL.setApproveFlag("9");
         mLCPolBL.setApproveCode("000");
         mLCPolBL.setApproveDate(theCurrentDate);
         }
         }
         */
        return true;
    }

    /**
     * 特殊处理，可屏蔽
     *
     * @return boolean
     */
    private boolean preSpecCheck()
    {
        if (mOperate.equals("INSERT||PROPOSAL")
                || mOperate.equals("UPDATE||PROPOSAL"))
        {
            if ((mLCPolBL.getRiskCode() != null)
                    && mLCPolBL.getRiskCode().equals("241801"))
            {
                if (mLCPolBL.getStandbyFlag2() == null)
                {
                    mLCPolBL.setStandbyFlag2("0");
                }

                int InsuNum = Integer.parseInt(mLCPolBL.getStandbyFlag2());
                if ((InsuNum + 1) != mLCInsuredBLSet.size())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "连带被保人数目和填写在保单中的数目不一致!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ProposalBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
