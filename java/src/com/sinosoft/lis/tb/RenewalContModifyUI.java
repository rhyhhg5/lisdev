package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title：电商WX0006产品 续保操作维护UI层 </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: sinosoft</p>
 * @author licaiyan
 * @version 1.0
 */
public class RenewalContModifyUI
{
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	public RenewalContModifyUI()
	{
	}

	/**
	 * 不执行任何操作，只传递数据给下一层
	 * @param cInputData VData
	 * @param cOperate String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;

		RenewalContModifyBL tRenewalContModifyBL = new RenewalContModifyBL();

		if (tRenewalContModifyBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tRenewalContModifyBL.mErrors);

			return false;
		}
		else
		{
			mResult = tRenewalContModifyBL.getResult();
		}

		return true;
	}

	/**
	 * 获取从BL层取得的结果
	 * @return VData
	 */
	public VData getResult()
	{
		return mResult;
	}

	public static void main(String[] agrs)
	{
		LCContSchema tLCContSchema = new LCContSchema();    //集体保单
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.ManageCom = "86";
		mGlobalInput.Operator = "001";

		tLCContSchema.setContNo("13000122016");
		tLCContSchema.setManageCom("86110000");
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("DeleteReason","数据维护");

		VData tVData = new VData();
		tVData.add(tLCContSchema);
		tVData.add(mGlobalInput);
                tVData.add(tTransferData);

		RenewalContModifyUI tgrlbl = new RenewalContModifyUI();
		tgrlbl.submitData(tVData, "DELETE");

		if (tgrlbl.mErrors.needDealError())
		{
			System.out.println(tgrlbl.mErrors.getFirstError());
		}
	}
}
