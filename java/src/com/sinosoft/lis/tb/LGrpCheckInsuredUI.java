package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author  zhangxing
 * @version 1.0
 */

public class LGrpCheckInsuredUI {

    private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    public LGrpCheckInsuredUI() {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        LGrpCheckInsuredBL tLGrpCheckInsuredBL = new LGrpCheckInsuredBL();
        System.out.println("Start LGrpCheckInsuredUI Submit...");
        tLGrpCheckInsuredBL.submitData(mInputData, cOperate);

        System.out.println("End LGrpCheckInsuredUI Submit...");

        //如果有需要处理的错误，则返回
        if (tLGrpCheckInsuredBL.mErrors.needDealError()) {
            mErrors.copyAllErrors(tLGrpCheckInsuredBL.mErrors);
            return false;
        }
        mResult.clear();
        mResult = tLGrpCheckInsuredBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {

    }

}
