package com.sinosoft.lis.tb;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

	public class CRSModifyInputUI {
	    /** 错误处理类，每个需要错误处理的类中都放置该类 */
	    public CErrors mErrors = new CErrors();
	    private VData mInputData = new VData();
	    private String mOperate = "";

	    //业务处理相关变量
	    /** 全局数据 */
//	    private GlobalInput mGlobalInput = new GlobalInput();
	  

	    public CRSModifyInputUI() {
	    }

	    public boolean submitData(VData cInputData, String cOperate) {
	    	
	    	this.mInputData = cInputData; 
	            this.mOperate = cOperate;
	       
	            CRSModifyInputBL tCRSModifyInputBL = new CRSModifyInputBL();
	            
	            try {
	    	   if (!tCRSModifyInputBL.submitData(mInputData, mOperate)) {
					this.mErrors.copyAllErrors(tCRSModifyInputBL.mErrors);
					CError tError = new CError();
					tError.moduleName = "LAContReceiveUI";
					tError.functionName = "submitDat";
					tError.errorMessage = "BL类处理失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
	       
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	    
	    return true;
	    }
	}