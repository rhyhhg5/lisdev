package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.tb.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCContReceiveUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private String mOperate = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContReceiveSchema mLCContReceiveSchema = new LCContReceiveSchema();

    public LCContReceiveUI() {
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContReceiveUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public boolean submitData(VData cInputData, String cOperate) {
        try {
            if (!cOperate.equals("RECEIVE") // 合同接收
                && !cOperate.equals("REJECT")) { //合同退回
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            this.mInputData = cInputData;
            this.mOperate = cOperate;
            LCContReceiveBL tLCContReceiveBL = new LCContReceiveBL();

            System.out.println("LCContReceiveUI BEGIN");
            if (tLCContReceiveBL.submitData(cInputData, mOperate) == false) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCContReceiveBL.mErrors);
                return false;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "group";
        String strOperation = "RECEIVE";
        LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
        LCContReceiveUI tLCContReceiveUI = new LCContReceiveUI();
        tLCContReceiveSchema.setReceiveID(1);
        tLCContReceiveSchema.setContNo("00001097901");
        tLCContReceiveSchema.setPrtNo("99000000021");
        tLCContReceiveSchema.setContType("1");
        tLCContReceiveSchema.setBackReasonCode("1");
        tLCContReceiveSchema.setBackReason("合同信息维护");

        VData vData = new VData();

        vData.addElement(tLCContReceiveSchema);
        vData.add(globalInput);

        try {
            if (!tLCContReceiveUI.submitData(vData, strOperation)) {
                if (tLCContReceiveUI.mErrors.needDealError()) {
                    System.out.println(tLCContReceiveUI.mErrors.getFirstError());

                } else {
                    System.out.println("保存失败，但是没有详细的原因");
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            ex.getMessage();
        }

    }

}
