package com.sinosoft.lis.tb;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class ContAnalysisBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public ContAnalysisBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "ContAnalysisBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 1][20];
     if(mtype.equals("1")){
        //标题
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单号";
        mToExcel[0][3] = "业务员代码";
        mToExcel[0][4] = "业务员姓名";
        mToExcel[0][5] = "营销机构编码";
        mToExcel[0][6] = "营销机构名称";
        mToExcel[0][7] = "销售渠道";
        mToExcel[0][8] = "投保人";
        mToExcel[0][9] = "签单保费";
        mToExcel[0][10] = "签单日期";
        mToExcel[0][11] = "状态";
    }
    else if (mtype.equals("2")){
            //标题
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单号";
        mToExcel[0][3] = "险种代码";
        mToExcel[0][4] = "险种名称";
        mToExcel[0][5] = "保障开始时间";
        mToExcel[0][6] = "保障终止时间";
        mToExcel[0][7] = "承保保费";
        mToExcel[0][8] = "保额";
        mToExcel[0][9] = "档次";
        mToExcel[0][10] = "签单日期";
        mToExcel[0][11] = "销售渠道";
        mToExcel[0][12] = "业务员编码";
        mToExcel[0][13] = "业务员姓名";
        mToExcel[0][14] = "营销机构编码";
        mToExcel[0][15] = "营销机构名称";
    }
    else if (mtype.equals("3")){
            //标题
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "险种代码";
        mToExcel[0][3] = "保障开始时间";
        mToExcel[0][4] = "保障终止时间";
        mToExcel[0][5] = "承保保费";
        mToExcel[0][6] = "保额";
        mToExcel[0][7] = "档次";
        mToExcel[0][8] = "核保结论";
        mToExcel[0][9] = "签单日期";
        mToExcel[0][10] = "销售渠道";
        mToExcel[0][11] = "业务员编码";
        mToExcel[0][12] = "业务员姓名";
        mToExcel[0][13] = "营销机构编码";
        mToExcel[0][14] = "营销机构名称";
    }
    else if (mtype.equals("4")){
            //标题
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "业务员编码";
        mToExcel[0][3] = "业务员姓名";
        mToExcel[0][4] = "营销机构编码";
        mToExcel[0][5] = "营销机构名称";
        mToExcel[0][6] = "销售渠道";
        mToExcel[0][7] = "自核提交时间";
        mToExcel[0][8] = "自核状态";
        mToExcel[0][9] = "核保完成时间";
        mToExcel[0][10] = "核保人员";
    }
    else if (mtype.equals("5")){
            //标题
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "体检号";
        mToExcel[0][2] = "保单号";
        mToExcel[0][3] = "体检下发日期";
        mToExcel[0][4] = "体检回复日期";
        mToExcel[0][5] = "业务员编码";
        mToExcel[0][6] = "业务员姓名";
        mToExcel[0][7] = "营销机构编码";
        mToExcel[0][8] = "营销机构名称";
    }
    else if (mtype.equals("6")){
            //标题
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "问题件号";
        mToExcel[0][2] = "问题件号下发日期";
        mToExcel[0][3] = "问题件回复日期";
        mToExcel[0][4] = "初审人员编码";
        mToExcel[0][5] = "业务员编码";
        mToExcel[0][6] = "业务员姓名";
        mToExcel[0][7] = "营销机构编码";
        mToExcel[0][8] = "营销机构名称";
    }
    else if (mtype.equals("7")){
            //标题
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单类型";
        mToExcel[0][3] = "保费";
        mToExcel[0][4] = "销售渠道";
        mToExcel[0][5] = "填单日期";
        mToExcel[0][6] = "交单日期";
        mToExcel[0][7] = "业务员编码";
        mToExcel[0][8] = "业务员姓名";
        mToExcel[0][9] = "营销机构编码";
        mToExcel[0][10] = "营销机构名称";
        mToExcel[0][11] = "交单时效（天）";
    }
    else if (mtype.equals("8")){
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单类型";
        mToExcel[0][3] = "扫描日期";
        mToExcel[0][4] = "收单日期";
        mToExcel[0][5] = "业务员编码";
        mToExcel[0][6] = "业务员姓名";
        mToExcel[0][7] = "营销机构编码";
        mToExcel[0][8] = "营销机构名称";
        mToExcel[0][9] = "受理时效（天）";
    }
    else if (mtype.equals("9")){
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单类型";
        mToExcel[0][3] = "保费";
        mToExcel[0][4] = "扫描日期";
        mToExcel[0][5] = "录入日期";
        mToExcel[0][6] = "业务员编码";
        mToExcel[0][7] = "业务员姓名";
        mToExcel[0][8] = "营销机构编码";
        mToExcel[0][9] = "营销机构名称";
        mToExcel[0][10] = "录入时效（天）";
    }
    else if (mtype.equals("10")){
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单类型";
        mToExcel[0][3] = "复核日期";
        mToExcel[0][4] = "录入日期";
        mToExcel[0][5] = "业务员编码";
        mToExcel[0][6] = "业务员姓名";
        mToExcel[0][7] = "营销机构编码";
        mToExcel[0][8] = "营销机构名称";
        mToExcel[0][9] = "复核时效（天）";
    }
    else if (mtype.equals("11")){
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "业务员编码";
        mToExcel[0][3] = "业务员姓名";
        mToExcel[0][4] = "营销机构编码";
        mToExcel[0][5] = "营销机构名称";
        mToExcel[0][6] = "销售渠道";
        mToExcel[0][7] = "自核提交日期";
        mToExcel[0][8] = "自核状态";
        mToExcel[0][9] = "核保完成日期";
        mToExcel[0][10] = "问题件下发日期";
        mToExcel[0][11] = "问题件回销日期";
        mToExcel[0][12] = "体检件下发日期";
        mToExcel[0][13] = "体检件回销日期";
        mToExcel[0][14] = "契调件下发日期";
        mToExcel[0][15] = "契调件回销日期";
        mToExcel[0][16] = "核保人员";
        mToExcel[0][17] = "核保时效（天）";
    }
    else if (mtype.equals("12")){
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单类型";
        mToExcel[0][3] = "保费";
        mToExcel[0][4] = "销售渠道";
        mToExcel[0][5] = "收费日期";
        mToExcel[0][6] = "收费方式";
        mToExcel[0][7] = "核保完成日期";
        mToExcel[0][8] = "业务员编码";
        mToExcel[0][9] = "业务员姓名";
        mToExcel[0][10] = "营销机构编码";
        mToExcel[0][11] = "营销机构名称";
        mToExcel[0][12] = "收费时效（天）";
    }
    else if (mtype.equals("13")){
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单类型";
        mToExcel[0][3] = "保单打印日期";
        mToExcel[0][4] = "客户签收日期";
        mToExcel[0][5] = "业务员编码";
        mToExcel[0][6] = "业务员姓名";
        mToExcel[0][7] = "营销机构编码";
        mToExcel[0][8] = "营销机构名称";
        mToExcel[0][9] = "送单时效（天）";
    }
    else {
        mToExcel[0][0] = "管理机构";
        mToExcel[0][1] = "印刷号";
        mToExcel[0][2] = "保单类型";
        mToExcel[0][3] = "填单日期";
        mToExcel[0][4] = "问题件下发日期";
        mToExcel[0][5] = "问题件回销日期";
        mToExcel[0][6] = "体检件下发日期";
        mToExcel[0][7] = "体检件回销日期";
        mToExcel[0][8] = "契调件下发日期";
        mToExcel[0][9] = "契调件回销日期";
        mToExcel[0][10] = "保单签发日期";
        mToExcel[0][11] = "保单签收日期";
        mToExcel[0][12] = "销售渠道";
        mToExcel[0][13] = "业务员编码";
        mToExcel[0][14] = "业务员姓名";
        mToExcel[0][15] = "营销机构编码";
        mToExcel[0][16] = "营销机构名称";
        mToExcel[0][17] = "承保时效（天）";
    }
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "ContAnalysisBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("AnalysisSql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "ContAnalysisBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        ContAnalysisBL ContAnalysisbl = new
            ContAnalysisBL();
    }
}
