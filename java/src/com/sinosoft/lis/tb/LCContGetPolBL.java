package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.LCContGetPolDB;
import com.sinosoft.lis.vschema.LCContGetPolSet;
import com.sinosoft.lis.certify.*;
import com.sinosoft.utility.StrTool;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCContGetPolBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private VData mResult = new VData();
    private VData cData = new VData();
    private String mOperate = "";
    MMap map = new MMap();

//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContGetPolSchema mLCContGetPolSchema = new LCContGetPolSchema();
    private LCContGetPolSchema m2LCContGetPolSchema = new LCContGetPolSchema();

    public LCContGetPolBL() {
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContReceiveUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("INSERT") && !cOperate.equals("UPDATE")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        //全局变量赋值
        mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("End getInputData");
        if (!checkdata()) {
            return false;
        }
        System.out.println("End checkdata");
        if (this.dealData() == false) {
            return false;
        }
        System.out.println("End dealData");
        //统一递交数据
        mResult.add(map);
        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(mResult, cOperate)) {
            mErrors.copyAllErrors(pubsubmit.mErrors);
            return false;
        }
        System.out.println("---End pubsubmit---");
        return true;
    }

    private boolean getInputData(VData cInputData) {
        //获取数据的时候不需要区分打印模式
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));
        mLCContGetPolSchema.setSchema((LCContGetPolSchema) cInputData.
                                      getObjectByObjectName(
                                              "LCContGetPolSchema", 0));

        return true;
    }

    private boolean checkdata() {
        LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
        LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
        LCContGetPolSet tLCContGetPolSet = new LCContGetPolSet();
        tLCContGetPolDB.setContNo(mLCContGetPolSchema.getContNo());
        tLCContGetPolSet = tLCContGetPolDB.query();
        System.out.println("tLCContGetPolSet.size is " + tLCContGetPolSet.size());
        if (tLCContGetPolSet.size() != 1) {
            buildError("checkdata", "根据主键查询失败");
            return false;
        }
        tLCContGetPolSchema = tLCContGetPolSet.get(1).getSchema();
        m2LCContGetPolSchema.setSchema(tLCContGetPolSchema);
        if (mOperate.equals("INSERT")) {
            if (StrTool.cTrim(tLCContGetPolSchema.getGetpolState()).equals("1")) {
                buildError("checkdata", "该单证已经回收过");
                return false;
            }
        }
        return true;
    }

    private boolean dealData() {
        String tContType = m2LCContGetPolSchema.getContType();
        if (mOperate.equals("INSERT")) {
            if (this.dealcertify() == false) {
                return false;
            }
            m2LCContGetPolSchema.setGetpolState("1");
            m2LCContGetPolSchema.setGetpolDate(mLCContGetPolSchema.
                                               getGetpolDate());
            m2LCContGetPolSchema.setGetpolMan(mLCContGetPolSchema.getGetpolMan());
            m2LCContGetPolSchema.setSendPolMan(mLCContGetPolSchema.
                                               getSendPolMan());
            m2LCContGetPolSchema.setGetPolManageCom(mGlobalInput.ManageCom);
            m2LCContGetPolSchema.setGetPolOperator(mGlobalInput.Operator);
            m2LCContGetPolSchema.setGetpolMakeDate(PubFun.getCurrentDate());
            m2LCContGetPolSchema.setGetpolMakeTime(PubFun.getCurrentTime());
            m2LCContGetPolSchema.setModifyDate(PubFun.getCurrentDate());
            m2LCContGetPolSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(m2LCContGetPolSchema, "UPDATE");
        }
        if (mOperate.equals("UPDATE")) {
//            if (m2LCContGetPolSchema.getContType().equals("1")) {
//                map.put(
//                        "delete from LZSysCertify where CertifyCode='9995' and CertifyNo='" +
//                        m2LCContGetPolSchema.getContNo()+"'", "DELETE");
//            }
//            if (m2LCContGetPolSchema.getContType().equals("2")) {
//                map.put(
//                        "delete from LZSysCertify where CertifyCode='9994' and CertifyNo='" +
//                        m2LCContGetPolSchema.getContNo()+"'", "DELETE");
//            }
            if (this.dealcertify() == false) {
                return false;
            }
            m2LCContGetPolSchema.setGetpolState("1");
            m2LCContGetPolSchema.setGetpolDate(mLCContGetPolSchema.
                                               getGetpolDate());
            m2LCContGetPolSchema.setGetpolMan(mLCContGetPolSchema.getGetpolMan());
            m2LCContGetPolSchema.setSendPolMan(mLCContGetPolSchema.
                                               getSendPolMan());
            m2LCContGetPolSchema.setGetPolManageCom(mGlobalInput.ManageCom);
            m2LCContGetPolSchema.setGetPolOperator(mGlobalInput.Operator);
            m2LCContGetPolSchema.setGetpolMakeDate(mLCContGetPolSchema.getGetpolMakeDate());
            m2LCContGetPolSchema.setGetpolMakeTime(PubFun.getCurrentTime());
            m2LCContGetPolSchema.setModifyDate(PubFun.getCurrentDate());
            m2LCContGetPolSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(m2LCContGetPolSchema, "UPDATE");
        }
        return true;
    }

    private boolean dealcertify() {
        String tContType = m2LCContGetPolSchema.getContType();
        LZSysCertifySchema schemaLZSysCertify = new LZSysCertifySchema();
        schemaLZSysCertify.setCertifyCode("");
        schemaLZSysCertify.setCertifyNo(m2LCContGetPolSchema.getContNo());
        schemaLZSysCertify.setTakeBackOperator(m2LCContGetPolSchema.
                                               getGetPolOperator());
        schemaLZSysCertify.setTakeBackDate(mLCContGetPolSchema.getGetpolDate());
        schemaLZSysCertify.setSendOutCom("D" +
                                         m2LCContGetPolSchema.getAgentCode());
        schemaLZSysCertify.setReceiveCom("A" +
                                         m2LCContGetPolSchema.getManageCom());

        if (tContType.equals("1")) {
            schemaLZSysCertify.setCertifyCode("9995");
        } else if (tContType.equals("2")) {
            schemaLZSysCertify.setCertifyCode("9994");
        }
        cData.addElement(mGlobalInput);
        cData.addElement(schemaLZSysCertify);
        if (!dealCertTakeBack()) {
            return false;
        }

        return true;
    }

    private boolean dealCertTakeBack() {
        SysCertTakeBackUI tSysCertTakeBackUI = new SysCertTakeBackUI();
        if (!tSysCertTakeBackUI.submitData(cData, "INSERT||MAIN")) {
            mErrors.copyAllErrors(tSysCertTakeBackUI.mErrors);
            return false;
        }
        if (m2LCContGetPolSchema.getContType().equals("1")) {
        	String getPolMakeDate="";
        	if(mLCContGetPolSchema.getGetpolMakeDate() !=null && !"".equals(mLCContGetPolSchema.getGetpolMakeDate())){
        		 getPolMakeDate = mLCContGetPolSchema.getGetpolMakeDate();
        	}else{
        		 getPolMakeDate = PubFun.getCurrentDate(); 
        	}
        	
            map.put("update lccont set GetPolDate='" + getPolMakeDate +
                    "',CustomGetPolDate='" + mLCContGetPolSchema.getGetpolDate() +
                    "',Modifydate='" + PubFun.getCurrentDate() +
                    "',Modifytime='" + PubFun.getCurrentTime() +
                    "' where prtno='" + m2LCContGetPolSchema.getPrtNo() + "'",
                    "UPDATE");
        }
        if (m2LCContGetPolSchema.getContType().equals("2")) {
            map.put("update lCCont set GetPolDate='" + PubFun.getCurrentDate() +
                    "',CustomGetPolDate='" + mLCContGetPolSchema.getGetpolDate() +
                    "',Modifydate='" + PubFun.getCurrentDate() +
                    "',Modifytime='" + PubFun.getCurrentTime() +
                    "' where prtno='" + m2LCContGetPolSchema.getPrtNo() + "'",
                    "UPDATE");
            map.put("update lCGrpcont set GetPolDate='" + PubFun.getCurrentDate() +
                    "',CustomGetPolDate='" + mLCContGetPolSchema.getGetpolDate() +
                    "',Modifydate='" + PubFun.getCurrentDate() +
                    "',Modifytime='" + PubFun.getCurrentTime() +
                    "' where prtno='" + m2LCContGetPolSchema.getPrtNo() + "'",
                    "UPDATE");
        }

        return true;
    }
}
