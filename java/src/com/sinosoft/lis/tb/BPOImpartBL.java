package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOImpartBL
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private BPOLCImpartSet mBPOLCImpartSet = null;
    private BPOLCImpartSet mBPOLCImpartSetDetail = null;  //客户告知说明内容

    private MMap map = new MMap();

    public BPOImpartBL()
    {
    }

    /**
     * 数据提交的公共方法，调用getSubmitMap进行业务处理，
     * 处理成功后将返回结果保存入内部VData对象中
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if(getSubmitMap(cInputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据库失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getSubmitMMap
     * 外包调用的接口方法。本方法调用checkData进行数据校验，调用dealData进行业务逻辑处
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return MMap：处理成功，返回处理后的带提交数据库集合, 处理失败，返回null
     */
    public MMap getSubmitMap(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法，是否可进行业务逻辑处理
     * @return boolean：true合法，false不合法
     */
    private boolean checkData()
    {
        if(mGI == null)
        {
            CError tError = new CError();
            tError.moduleName = "BPOInsuredBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有接收到操作员信息，可能已超时";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if(mBPOLCImpartSet == null || mBPOLCImpartSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOInsuredBL";
            tError.functionName = "checkData";
            tError.errorMessage = "请录入健康告知";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * dealData
     * 本类的核心方法，用来按业务逻辑处理传入的数据
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        String sql = "delete from BPOLCImpart "
                     + "where BPOBatchNo = '"
                     + mBPOLCImpartSet.get(1).getBPOBatchNo() + "' "
                     + "   and ContID = '"
                     + mBPOLCImpartSet.get(1).getContID() + "' "
                     + "   and CustomerNo = '"
                     + mBPOLCImpartSet.get(1).getCustomerNo() + "' ";
        map.put(sql, SysConst.DELETE);

        for(int i = 1; i <= mBPOLCImpartSet.size(); i++)
        {
            for(int j = 1;
                        mBPOLCImpartSetDetail != null
                        && j <= mBPOLCImpartSetDetail.size(); j++)
            {
                //查找该告知的告知说明内容
                if(mBPOLCImpartSet.get(i).getBPOBatchNo()
                   .equals(mBPOLCImpartSetDetail.get(j).getBPOBatchNo())
                   && mBPOLCImpartSet.get(i).getContID()
                   .equals(mBPOLCImpartSetDetail.get(j).getContID())
                   && mBPOLCImpartSet.get(i).getCustomerNo()
                   .equals(mBPOLCImpartSetDetail.get(j).getCustomerNo())
                   && mBPOLCImpartSet.get(i).getImpartVer()
                   .equals(mBPOLCImpartSetDetail.get(j).getImpartVer())
                   && mBPOLCImpartSet.get(i).getImpartCode()
                   .equals(mBPOLCImpartSetDetail.get(j).getImpartCode()))
                {
                    mBPOLCImpartSet.get(i).setDiseaseContent(
                        mBPOLCImpartSetDetail.get(j).getDiseaseContent());
                }
            }

            mBPOLCImpartSet.get(i).setOperator(mGI.Operator);
            PubFun.fillDefaultField(mBPOLCImpartSet.get(i));
        }
        map.put(mBPOLCImpartSet, SysConst.INSERT);

        return true;
    }

    /**
     * getInputData
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mBPOLCImpartSet = (BPOLCImpartSet) data
                          .getObjectByObjectName("BPOLCImpartSet", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);
        mBPOLCImpartSetDetail = (BPOLCImpartSet) tf
                                .getValueByName("BPOLCImpartSetDetail");
        return true;
    }

}
