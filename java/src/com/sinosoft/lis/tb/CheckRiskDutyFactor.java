package com.sinosoft.lis.tb;

import java.util.HashSet;
import java.util.Iterator;

import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author:YangMing
 * @version 1.0
 */

/**
 * 目的是希望采用算法的方式进行
 * 校验每个险种的各个要素信息是
 * 否录入完整从而保证后期的险种
 * 计算正确.
 */
public class CheckRiskDutyFactor
{

    private VData mInputData = new VData();

    public CErrors mErrors = new CErrors();

    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private double mValue = 0.0;

    private String mError = "";

    private VData mResult = new VData();

    public static void main(String[] args)
    {
        CheckRiskDutyFactor tCheckRiskDutyFactor = new CheckRiskDutyFactor();
        //        LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
        LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setDutyCode("626001");
        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        tLCContPlanDutyParamSchema.setCalFactorValue("88");
        tLCContPlanDutyParamSchema.setRiskCode("150406");
        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setDutyCode("626001");
        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
        tLCContPlanDutyParamSchema.setCalFactorValue("10000");
        tLCContPlanDutyParamSchema.setRiskCode("150406");
        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setDutyCode("626001");
        tLCContPlanDutyParamSchema.setCalFactor("GetLimit");
        tLCContPlanDutyParamSchema.setCalFactorValue("0");
        tLCContPlanDutyParamSchema.setRiskCode("150406");

        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //
        //        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setDutyCode("626001");
        tLCContPlanDutyParamSchema.setCalFactor("GetRate");
        tLCContPlanDutyParamSchema.setCalFactorValue("1");
        tLCContPlanDutyParamSchema.setRiskCode("150406");

        //
        //        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        //        tLCContPlanDutyParamSchema.setDutyCode("602001");
        //        tLCContPlanDutyParamSchema.setCalFactor("FloatRate");
        //        tLCContPlanDutyParamSchema.setCalFactorValue("0.8");
        //        tLCContPlanDutyParamSchema.setRiskCode("1602");
        //        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //
        //        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        //        tLCContPlanDutyParamSchema.setDutyCode("602001");
        //        tLCContPlanDutyParamSchema.setCalFactor("GetRate");
        //        tLCContPlanDutyParamSchema.setCalFactorValue("10");
        //        tLCContPlanDutyParamSchema.setRiskCode("1602");
        //        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //
        //        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        //        tLCContPlanDutyParamSchema.setDutyCode("601005");
        //        tLCContPlanDutyParamSchema.setCalFactor("Mult");
        //        tLCContPlanDutyParamSchema.setCalFactorValue("10");
        //        tLCContPlanDutyParamSchema.setRiskCode("1601");
        //        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //
        //        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        //        tLCContPlanDutyParamSchema.setDutyCode("601005");
        //        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        //        tLCContPlanDutyParamSchema.setCalFactorValue("1");
        //        tLCContPlanDutyParamSchema.setRiskCode("1601");
        //        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //
        //        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        //        tLCContPlanDutyParamSchema.setDutyCode("602001");
        //        tLCContPlanDutyParamSchema.setCalFactor("CalRule");
        //        tLCContPlanDutyParamSchema.setCalFactorValue("1");
        //        tLCContPlanDutyParamSchema.setRiskCode("1602");
        //        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //
        //        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        //        tLCContPlanDutyParamSchema.setDutyCode("601005");
        //        tLCContPlanDutyParamSchema.setCalFactor("CalRule");
        //        tLCContPlanDutyParamSchema.setCalFactorValue("1");
        //        tLCContPlanDutyParamSchema.setRiskCode("1601");
        //        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        //        LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
        //        tLCContPlanDutyParamDB.setGrpContNo("1400000110");
        //        mLCContPlanDutyParamSet = tLCContPlanDutyParamDB.query();
        VData tVData = new VData();
        tVData.add(mLCContPlanDutyParamSet);
        tCheckRiskDutyFactor.submitData(tVData, "");
    }

    public void CheckRiskDutyFactor()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = cInputData;
        if (!getInputData(mInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return true;
        }
        return true;
    }

    /**
     * 获取全部参数
     * @param cInputData VData
     * @return boolean
     * @author: Yangming
     */
    private boolean getInputData(VData cInputData)
    {
        mLCContPlanDutyParamSet.set((LCContPlanDutyParamSet) cInputData
                .getObjectByObjectName("LCContPlanDutyParamSet", 0));

        LCGrpContSet tLCGrpContSet = (LCGrpContSet) cInputData
                .getObjectByObjectName("LCGrpContSet", 0);
        if (tLCGrpContSet != null)
        {
            if (tLCGrpContSet.size() > 0)
                mLCGrpContSchema.setSchema(tLCGrpContSet.get(1));
        }

        /**@author:Yangming 保费添加方式改变，使添加的责任不是顺序的，现将保费要素按照责任排序 */
        LCContPlanDutyParamSet LCContPlanDutyParamSetAfterSave = new LCContPlanDutyParamSet();
        LCContPlanDutyParamSet tempLCContPlanDutyParamSet2 = new LCContPlanDutyParamSet();
        tempLCContPlanDutyParamSet2.set(mLCContPlanDutyParamSet);
        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++)
        {

            for (int m = 1; m <= tempLCContPlanDutyParamSet2.size(); m++)
            {
                if (mLCContPlanDutyParamSet.get(i).getDutyCode().equals(
                        tempLCContPlanDutyParamSet2.get(m).getDutyCode()))
                {
                    LCContPlanDutyParamSetAfterSave
                            .add(tempLCContPlanDutyParamSet2.get(m).getSchema());
                    tempLCContPlanDutyParamSet2.removeRange(m, m);
                }
            }
        }
        mLCContPlanDutyParamSet = null;
        mLCContPlanDutyParamSet = LCContPlanDutyParamSetAfterSave;
        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++)
        {
            System.out.println("Factor : " + "    "
                    + mLCContPlanDutyParamSet.get(i).getRiskCode() + "    "
                    + mLCContPlanDutyParamSet.get(i).getCalFactor() + "    "
                    + mLCContPlanDutyParamSet.get(i).getDutyCode());
        }

        return true;
    }

    private boolean dealData()
    {
        if (mLCContPlanDutyParamSet.size() <= 0)
        {
            System.out.println("程序第63行，author : Yangming");
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在校验险种要素信息时查询保险计划要素失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //拆分险种，每一个险种校验一遍
        VData RiskCode = new VData();
        LCContPlanDutyParamSet tempLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
        tempLCContPlanDutyParamSet.set(mLCContPlanDutyParamSet);
        HashSet risk = new HashSet();
        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++)
        {
            risk.add(mLCContPlanDutyParamSet.get(i).getRiskCode());
        }
        Iterator iterator = risk.iterator();
        while (iterator.hasNext())
        {
            RiskCode.add(iterator.next());
        }
        System.out.println("查看险种信息个数 : " + RiskCode.size());
        for (int i = 0; i < RiskCode.size(); i++)
        {
            System.out.println("查看险种信代码 : " + RiskCode.get(i));
        }
        //        String[] tempRiskKind = RiskCode.split("}");
        //        String Temp = "";
        //        if (tempRiskKind.length > 0)
        //            Temp = tempRiskKind[0] + "}";
        //        for (int i = 0; i < tempRiskKind.length; i++) {
        //            if (!tempRiskKind[0].equals(tempRiskKind[i])) {
        //                Temp += tempRiskKind[i] + "}";
        //            }
        //        }
        //区分保险计划中险种
        //        String[] RiskKind = null;
        //        if (RiskKind == null) {
        //            System.out.println("程序第85行，author : Yangming");
        //            CError tError = new CError();
        //            tError.moduleName = "LCContPlanBL";
        //            tError.functionName = "prepareData";
        //            tError.errorMessage = "拆分险种失败！";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        //        System.out.println("--------检测险种拆分是否正确-------");
        //        for (int i = 0; i < RiskKind.length; i++) {
        //            System.out.println("RiskKind " + i + " = " + RiskKind[i]);
        //        }
        for (int i = 0; i < RiskCode.size(); i++)
        {
            String SubRisk = (String) RiskCode.get(i);
            LCDutySchema tLCDutySchema = new LCDutySchema();
            String tDutyCode = null;
            LCDutySet tLCDutySet = new LCDutySet();
            for (int m = 1; m <= mLCContPlanDutyParamSet.size(); m++)
            {
                if (SubRisk
                        .equals(mLCContPlanDutyParamSet.get(m).getRiskCode()))
                {

                    //                System.out.println("CheckRiskDutyFactor.dealData()");
                    //                System.out.println("检测");
                    System.out
                            .println("mLCContPlanDutyParamSet.get(m).getCalFactor() :"
                                    + mLCContPlanDutyParamSet.get(m)
                                            .getCalFactor());
                    System.out
                            .println("mLCContPlanDutyParamSet.get(m).getCalFactorValue() :"
                                    + mLCContPlanDutyParamSet.get(m)
                                            .getCalFactorValue());
                    tLCDutySchema.setDutyCode(mLCContPlanDutyParamSet.get(m)
                            .getDutyCode());
                    tLCDutySchema.setV(mLCContPlanDutyParamSet.get(m)
                            .getCalFactor(), mLCContPlanDutyParamSet.get(m)
                            .getCalFactorValue());
                    if (m == mLCContPlanDutyParamSet.size()
                            || (m < mLCContPlanDutyParamSet.size() && !mLCContPlanDutyParamSet
                                    .get(m + 1).getDutyCode().equals(
                                            tLCDutySchema.getDutyCode())))
                    {
                        tLCDutySet.add(tLCDutySchema.getSchema());
                    }
                }
            }

            //            try {
            if (CalRiskDutyFactor(tLCDutySet, SubRisk))
            {
            }
            //           } catch (Exception ex) {
            //               ex.printStackTrace();
            //               return false;
            //           }

        }
        return true;
    }

    private boolean CalRiskDutyFactor(LCDutySet tLCDutySet, String rRiskCode)
    {
        String strSql = "select * from lmcalmode where calcode like 'CK%' and riskcode='"
                + rRiskCode + "'";

        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        LMCalModeSet tLMCalModeSet = tLMCalModeDB.executeQuery(strSql);
        if (tLMCalModeSet.size() <= 0)
        {
            System.out.println("程序第132行；没有查询到计算sql author : Yangming ");
            return true;
        }
        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            System.out.println("存在" + tLCDutySet.size() + "个责任编码,dutycode:"
                    + tLCDutySet.get(i).getDutyCode());
        }
        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            LCDutySchema mLCDutySchema = tLCDutySet.get(i);
            for (int m = 1; m <= tLMCalModeSet.size(); m++)
            {
                String oneCalCode = tLMCalModeSet.get(m).getCalCode();
                String oneError = tLMCalModeSet.get(m).getRemark();
                if (oneCalCode == null)
                {
                    System.out
                            .println("程序第138行；oneCalCode == null;\n author : Yangming ");
                    return true;
                }
                Calculator mCalculator = new Calculator();
                mCalculator.setCalCode(oneCalCode);
                //增加基本要素
                double Amnt = mLCDutySchema.getAmnt();
                double GetLimit = mLCDutySchema.getGetLimit();
                double GetRate = mLCDutySchema.getGetRate();
                double Mult = mLCDutySchema.getMult();
                double PayIntv = mLCDutySchema.getPayIntv();
                double Prem = mLCDutySchema.getPrem();

                mCalculator.addBasicFactor("Amnt", String.valueOf(Amnt));
                mCalculator
                        .addBasicFactor("GetLimit", String.valueOf(GetLimit));
                mCalculator.addBasicFactor("GetRate", String.valueOf(GetRate));
                mCalculator.addBasicFactor("Mult", String.valueOf(Mult));
                mCalculator.addBasicFactor("PayIntv", String.valueOf(PayIntv));
                mCalculator.addBasicFactor("Prem", String.valueOf(Prem));

                mCalculator.addBasicFactor("CValiDate", mLCGrpContSchema
                        .getCValiDate());
                mCalculator.addBasicFactor("EndDate", mLCGrpContSchema
                        .getCInValiDate());

                mCalculator.addBasicFactor("AcciEndDate", mLCDutySchema
                        .getAcciEndDate());
                mCalculator.addBasicFactor("AcciYearFlag", mLCDutySchema
                        .getAcciYearFlag());
                mCalculator.addBasicFactor("BonusGetMode", mLCDutySchema
                        .getBonusGetMode());
                mCalculator.addBasicFactor("CalRule", mLCDutySchema
                        .getCalRule());
                mCalculator.addBasicFactor("ContNo", mLCDutySchema.getContNo());
                mCalculator.addBasicFactor("DeadGetMode", mLCDutySchema
                        .getDeadGetMode());
                mCalculator.addBasicFactor("DutyCode", mLCDutySchema
                        .getDutyCode());
                mCalculator.addBasicFactor("EndDate", mLCDutySchema
                        .getEndDate());
                mCalculator.addBasicFactor("FirstPayDate", mLCDutySchema
                        .getFirstPayDate());
                mCalculator.addBasicFactor("FreeEndDate", mLCDutySchema
                        .getFreeEndDate());
                mCalculator.addBasicFactor("FreeFlag", mLCDutySchema
                        .getFreeFlag());
                mCalculator.addBasicFactor("FreeStartDate", mLCDutySchema
                        .getFreeStartDate());
                mCalculator.addBasicFactor("GetStartDate", mLCDutySchema
                        .getGetStartDate());
                mCalculator.addBasicFactor("GetStartType", mLCDutySchema
                        .getGetStartType());
                mCalculator.addBasicFactor("GetYearFlag", mLCDutySchema
                        .getGetYearFlag());
                mCalculator.addBasicFactor("InsuYearFlag", mLCDutySchema
                        .getInsuYearFlag()); //tLCPolSchema.getInsuredNo());;
                mCalculator.addBasicFactor("LiveGetMode", mLCDutySchema
                        .getLiveGetMode()); //tLCPolSchema.getRiskCode());;
                mCalculator.addBasicFactor("PayEndDate", mLCDutySchema
                        .getPayEndDate());
                mCalculator.addBasicFactor("PayEndYearFlag", mLCDutySchema
                        .getPayEndYearFlag()); //tLCPolSchema.getRiskCode());;
                mCalculator.addBasicFactor("PaytoDate", mLCDutySchema
                        .getPaytoDate());
                mCalculator.addBasicFactor("PolNo", mLCDutySchema.getPolNo()); //tLCPolSchema.getRiskCode());;
                mCalculator.addBasicFactor("PremToAmnt", mLCDutySchema
                        .getPremToAmnt());
                mCalculator.addBasicFactor("RiskCode", mLCDutySchema
                        .getAcciEndDate()); //tLCPolSchema.getRiskCode());;
                mCalculator.addBasicFactor("SSFlag", mLCDutySchema.getSSFlag());
                mCalculator.addBasicFactor("StandbyFlag1", mLCDutySchema
                        .getStandbyFlag1());
                mCalculator.addBasicFactor("StandbyFlag2", mLCDutySchema
                        .getStandbyFlag2());
                mCalculator.addBasicFactor("StandbyFlag3", mLCDutySchema
                        .getStandbyFlag3());
                mCalculator.addBasicFactor("FloatRate", String
                        .valueOf(mLCDutySchema.getFloatRate()));
                //增加管理机构
                String SQl = "select distinct managecom from lcgrpcont where grpcontno='"
                        + mLCContPlanDutyParamSet.get(1).getGrpContNo()
                        + "'  with ur";
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = new SSRS();
                tSSRS = tExeSQL.execSQL(SQl);
                System.out.println("SQl" + SQl);
                if (tSSRS.MaxRow > 0)
                {
                    System.out.println("tSSRS" + tSSRS.GetText(1, 1));
                    mCalculator.addBasicFactor("ManageCom", String
                            .valueOf(tSSRS.GetText(1, 1)));
                }
                addInsuYear(mCalculator);
                addRiskWrapCode(mCalculator, rRiskCode);
                String tStr = "";

                tStr = mCalculator.calculate();
                if (tStr == null || tStr.trim().equals(""))
                {
                    mValue = 0;
                }
                else
                {
                    mValue = Double.parseDouble(tStr);
                    mError += oneError + "<br>";
                    //                    CError tError = new CError();
                    //                    tError.moduleName = "LCContPlanBL";
                    //                    tError.functionName = "prepareData";
                    //                    tError.errorMessage = mError;
                    //                    this.mErrors.addOneError(tError);
                    //                   return false;
                    //                    this.mErrors.addOneError(mError);
                }
                System.out.println(mValue);
                System.out.println("校验出错误 ：" + mError);
            }
        }
        return true;
    }

    /**
     * addRiskWrapCode
     *
     * @param mCalculator Calculator
     */
    private void addRiskWrapCode(Calculator mCalculator, String tRiskCode)
    {
        String strSql = "select riskwrapcode from lcriskwrap where grpcontno='"
                + this.mLCContPlanDutyParamSet.get(1).getGrpContNo()
                + "' and riskcode='" + tRiskCode + "'";
        mCalculator.addBasicFactor("RiskWrapCode", (new ExeSQL())
                .getOneValue(strSql));
    }

    /**
     * addInsuYear
     *
     * @param mCalculator Calculator
     */
    private void addInsuYear(Calculator mCalculator)
    {
        String strSql = "select integer(to_date((select CInValiDate from lcgrpcont where grpcontno='"
                + this.mLCContPlanDutyParamSet.get(1).getGrpContNo()
                + "'))-to_date((select CValiDate from lcgrpcont where grpcontno='"
                + this.mLCContPlanDutyParamSet.get(1).getGrpContNo()
                + "')))+1 from dual";
        mCalculator.addBasicFactor("InsuYear", (new ExeSQL())
                .getOneValue(strSql));
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean prepareOutputData()
    {
        this.mResult.add(this.mError);
        return true;
    }

}
