package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class ParseGuideInUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /**显示批次号*/
  private String mBatchNo;

  public ParseGuideInUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    ParseGuideIn tParseGuideIn = new ParseGuideIn();
    System.out.println("---UI BEGIN---"+mOperate);
    if (tParseGuideIn.submitData(cInputData,cOperate) == false)
    {
      // @@错误处理
      mBatchNo = tParseGuideIn.getBatchNo();
      this.mErrors.copyAllErrors(tParseGuideIn.mErrors);
      return false;
    }
    else
    {
        System.out.println("我进入这里了");
        System.out.println(tParseGuideIn.mErrors.getErrorCount());
        this.mErrors.copyAllErrors(tParseGuideIn.mErrors);
        mResult = tParseGuideIn.getResult();
        mBatchNo = tParseGuideIn.getBatchNo();
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    System.out.println("-------test...");
  }
  public String getBatchNo()
  {
      return mBatchNo;
  }
}
