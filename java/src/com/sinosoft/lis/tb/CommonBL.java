package com.sinosoft.lis.tb;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.SchemaSet;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class CommonBL
{
    public CommonBL()
    {
    }

    /**
     * 校验险种是否绑定型附加险
     * LDCode中checkappendrisk描述了主附关系，Code主险、Code1附加险
     * @param cRiskCode String
     * @return boolean：是，返回true，否则false
     */
    public static boolean isAppendRisk(String cAppendRisk)
    {
        String sql = "select Code1 from LDCode1 "
                + "where CodeType = 'checkappendrisk' " + "   and Code = '"
                + cAppendRisk + "' " + "   and RiskWrapPlanName is not null ";
        String mainRisk = new ExeSQL().getOneValue(sql);
        if ("".equals(mainRisk) || "null".equals(mainRisk))
        {
            return false;
        }

        return true;
    }

    /**
     * 得到绑定型附加险套餐名
     * LDCode中checkappendrisk描述了主附关系，
     * Code主险、Code1附加险、RiskWrapPlanName套餐名
     * @param cRiskCode String
     * @return boolean：是，返回true，否则false
     */
    public static String getRiskWrapPlanName(String cMainRisk)
    {
        String sql = "select RiskWrapPlanName from LDCode1 "
                + "where CodeType = 'checkappendrisk' " + "   and Code1 = '"
                + cMainRisk + "' ";
        String riskWrapPlanName = new ExeSQL().getOneValue(sql);
        if ("".equals(riskWrapPlanName) || "null".equals(riskWrapPlanName))
        {
            return null;
        }

        return riskWrapPlanName;
    }

    /**
     * 得到系统UI目录路径
     * @return String
     */
    public static final String getUIRoot()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("UIRoot");
        if (!tLDSysVarDB.getInfo())
        {
            return null;
        }
        return tLDSysVarDB.getSysVarValue();
    }

    /**
     * 输出Schema的每个 字段名-值
     * @return String
     */
    public static final String getFieldNameAndValue(Schema cSchema)
    {
        if (cSchema == null)
        {
            return null;
        }

        StringBuffer rs = new StringBuffer();
        rs.append(cSchema.getClass().getName() + ":\n");
        for (int i = 0; i < cSchema.getFieldCount(); i++)
        {
            String tFieldName = cSchema.getFieldName(i);
            String tFieldValue = cSchema.getV(i);
            //            if(tFieldValue == null || tFieldValue.equals("null")
            //            || tFieldValue.equals("0") || tFieldValue.equals("0.0"))
            //            {
            //                continue;
            //            }

            rs.append(tFieldName + ": " + tFieldValue + "\n");
        }

        return rs.toString();
    }

    /**
     * 输出Set的每个 字段名-值，
     * @return String
     */
    public static final String getFieldNameAndValue(SchemaSet cSet)
    {
        if (cSet == null)
        {
            return null;
        }

        System.out.println(cSet.getClass().getName());

        StringBuffer rs = new StringBuffer();
        for (int i = 1; i <= cSet.size(); i++)
        {
            rs.append("Record " + i + ":\n"
                    + getFieldNameAndValue((Schema) cSet.getObj(i)) + "\n");
        }

        return rs.toString();
    }

    /**
     * 判断是否为先收费保单。
     * @param tContNo
     * @return
     */
    public static boolean getIsFristPayOfSingleCont(String tContNo)
    {
        String tStrSql = "select distinct 1 from lccont "
                + " where PayLocation = '0' and conttype = '1' "
                + " and tempfeeno != '' and paymode in ('1', '11', '12') "
                + " and ContNo = '" + tContNo + "' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);

        return !"".equals(tResult);
    }

    /**
     * 判断个单是否有到帐暂缴保费。
     * @param tContNo
     * @return
     */
    public static boolean getIsPayMoneyOfSingleCont(String tContNo)
    {
        String tStrSql = "select distinct 1 from LCCont lcc "
                + " inner join LJTempFee ljtf on ljtf.OtherNo = lcc.PrtNo and ljtf.OtherNoType = '4' "
                + " where lcc.ContType = '1' and ljtf.ConfMakeDate is not null and ljtf.ConfFlag = '0' "
                + " and lcc.ContNo = '" + tContNo + "' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);

        return !"".equals(tResult);
    }

    /**
     * 校验简易平台保单的印刷号是否复合业务规则
     * <br />规则描述保存在 LDCode1 表中，CodeType = "cardflag"
     * <br />如未找到相关类型（CardFlag）的描述信息，不进行校验。
     * @param tPrtNo    保单印刷号
     * @param tCardFlag 简易保单类型
     * @return 返回校验结果 true：符合；false：不符合
     */
    public static boolean isBriefContPrtNoOfRule(String tPrtNo, String tCardFlag)
    {
        System.out.println("tPrtNo:" + tPrtNo);
        System.out.println("tCardFlag:" + tCardFlag);
        
        /**
         * 印刷号不能为空。
         */
        if (tPrtNo == null)
        {
            return false;
        }
        else
        {
            int tNLength = tPrtNo.length();
            if (tNLength != 11 && tNLength != 12 && tNLength != 13)
                return false;
        }

        ExeSQL tExeSQL = new ExeSQL();
        String tResult = null;
        String tStrSql = null;

        tStrSql = "select count(1) from LDCode1 where CodeType = 'cardflag' "
                + " and Code = '" + tCardFlag + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);
        if ("0".equals(tResult))
        {
            System.out.println("未找到相关校验规则，不进行类型校验。");
            return true;
        }

        // 清空临时变量。
        tResult = null;
        tStrSql = null;

        tStrSql = "select count(1) from LDCode1 where CodeType = 'cardflag' "
                + " and Code = '" + tCardFlag + "' " + " and Code1 = '"
                + tPrtNo.substring(0, 2) + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);

        return !"0".equals(tResult);
    }

    public static void main(String[] args)
    {
        //        CommonBL commonbl = new CommonBL();
        //        System.out.println(CommonBL.getIsPayMoneyOfSingleCont("13000518805"));
        System.out.println(CommonBL.isBriefContPrtNoOfRule("16000518805", "2"));
    }
}
