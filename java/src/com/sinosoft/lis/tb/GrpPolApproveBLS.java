package com.sinosoft.lis.tb;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统复核功能部分 </p>
 * <p>Description: BL层业务逻辑保存类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft < /p>
 * @author HST
 * @version 1.0
 * @date 2002-09-17
 */
public class GrpPolApproveBLS
{
	/** 传入数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 错误处理类 */
	public  CErrors mErrors = new CErrors();

	// @Constructor
	public GrpPolApproveBLS() {}

	// @Method
	/**
	* 数据提交的公共方法
	* @param: cInputData 传入的数据
	*		  cOperate 数据操作字符串
	* @return:
	*/
	public boolean submitData( VData cInputData, String cOperate )
	{
	    // 首先将数据在本类中做一个备份
	    mInputData = ( VData )cInputData.clone() ;
		this.mOperate = cOperate;

		// 保存数据
		if( this.saveData() == false )
			return false;

		// 释放数据容器
	    mInputData=null;

	    return true;
	}

	/**
	* 保存数据
	* @param: 无
	* @return: boolean
	*/
	private boolean saveData()
	{
		// 分解传入数据容器中的数据
	  	LCGrpContSchema mLCGrpContSchema = ( LCGrpContSchema )mInputData.getObjectByObjectName( "LCGrpContSchema", 0 );
	  	LCGrpPolSchema mLCGrpPolSchema=new LCGrpPolSchema();
                LCContSchema mLCContSchema=new LCContSchema();
                // hst	-- 2003-7-18  解决大数据的问题
	  	//LCPolSet mLCPolSet = ( LCPolSet )mInputData.getObjectByObjectName( "LCPolSet", 0 );

		// 建立数据连接
		Connection conn = null;
		conn = DBConnPool.getConnection();
	    if( conn == null )
	    {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpPolApproveBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "数据库连接失败!";
			this.mErrors .addOneError(tError) ;
			return false;
	    }

		try
		{
			conn.setAutoCommit(false);
                    //团体保单复核
                    ExeSQL tExeSQL = new ExeSQL( conn );
                    String sql = "update LCGrpCont "
                                    + "set ApproveCode = '" + mLCGrpContSchema.getApproveCode() + "', "
                                    + "ApproveDate = '" + mLCGrpContSchema.getApproveDate() + "', "
                                    + "ApproveFlag = '" + mLCGrpContSchema.getApproveFlag() + "', "
                                    + "ModifyDate = '" + mLCGrpContSchema.getModifyDate() + "', "
                                    + "ModifyTime = '" + mLCGrpContSchema.getModifyTime() + "' "
                                    + "where ProposalGrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() + "'"
                                    + "and appflag = '0'"
                                    + "and approveflag <> '9'";
                     if( tExeSQL.execUpdateSQL( sql ) == false )
                        {
                                // @@错误处理
                            this.mErrors.copyAllErrors( tExeSQL.mErrors );
                                CError tError = new CError();
                                tError.moduleName = "GrpPolApproveBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "LCGrpCont修改失败!";
                                this.mErrors .addOneError(tError) ;
                                conn.rollback();
                                conn.close();
                                return false;
                        }

			// 修改团体单中险种单
			 tExeSQL = new ExeSQL( conn );
			 sql = "update LCGrpPol "
						+ "set ApproveCode = '" + mLCGrpContSchema.getApproveCode() + "', "
						+ "ApproveDate = '" + mLCGrpContSchema.getApproveDate() + "', "
						+ "ApproveFlag = '" + mLCGrpContSchema.getApproveFlag() + "', "
						+ "ModifyDate = '" + mLCGrpContSchema.getModifyDate() + "', "
						+ "ModifyTime = '" + mLCGrpContSchema.getModifyTime() + "' "
						+ "where GrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() + "'"
						//+ "and riskcode in (select riskcode from lmriskapp where subriskflag='S')"
						+ "and appflag = '0'"
			                        + "and approveflag <> '9'";
			if( tExeSQL.execUpdateSQL( sql ) == false )
			{
				// @@错误处理
				this.mErrors.copyAllErrors( tExeSQL.mErrors );
				CError tError = new CError();
				tError.moduleName = "GrpPolApproveBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "LCGrpPolDBSet修改失败!";
				this.mErrors .addOneError(tError) ;
				conn.rollback();
				conn.close();
				return false;
			}

// 修改团体单中险种单
                         tExeSQL = new ExeSQL( conn );
                         sql = "update LCPol "
                                                + "set ApproveCode = '" + mLCGrpContSchema.getApproveCode() + "', "
                                                + "ApproveDate = '" + mLCGrpContSchema.getApproveDate() + "', "
                                                + "ApproveFlag = '" + mLCGrpContSchema.getApproveFlag() + "', "
                                                + "ModifyDate = '" + mLCGrpContSchema.getModifyDate() + "', "
                                                + "ModifyTime = '" + mLCGrpContSchema.getModifyTime() + "' "
                                                + "where GrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() + "'"
                                                //+ "and riskcode in (select riskcode from lmriskapp where subriskflag='S')"
                                                + "and appflag = '0'"
                                                + "and approveflag <> '9'";
                        if( tExeSQL.execUpdateSQL( sql ) == false )
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors( tExeSQL.mErrors );
                                CError tError = new CError();
                                tError.moduleName = "GrpPolApproveBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "LCPolDBSet修改失败!";
                                this.mErrors .addOneError(tError) ;
                                conn.rollback();
                                conn.close();
                                return false;
                        }

	/*		// 修改集体投保单总表
			LCGrpContDB tLCGrpContDB = new LCGrpContDB( conn );
			tLCGrpContDB.setSchema( mLCGrpContSchema );
			if( tLCGrpContDB.update() == false )
			{
				// @@错误处理
    			this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
				CError tError = new CError();
				tError.moduleName = "GrpPolApproveBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "LCGrpCont修改失败!";
				this.mErrors .addOneError(tError) ;
				conn.rollback();
                                conn.close();
				return false;
		           }*/
                           // 修改个人投保单
                           // sun要求修改 2003-1-22	解决大数据的问题
                           tExeSQL = new ExeSQL( conn );
                           sql = "update LCCont "
                                                   + "set ApproveCode = '" + mLCGrpContSchema.getApproveCode() + "', "
                                                   + "ApproveDate = '" + mLCGrpContSchema.getApproveDate() + "', "
                                                   + "ApproveFlag = '" + mLCGrpContSchema.getApproveFlag() + "', "
                                                   + "ModifyDate = '" + mLCGrpContSchema.getModifyDate() + "', "
                                                   + "ModifyTime = '" + mLCGrpContSchema.getModifyTime() + "' "
                                                   + "where GrpContNo = '" +  mLCGrpContSchema.getProposalGrpContNo() + "'"
                                       + "and appflag = '0'"
                                       + "and approveflag <> '9'";
                           if( tExeSQL.execUpdateSQL( sql ) == false )
                           {
                                   // @@错误处理
                               this.mErrors.copyAllErrors( tExeSQL.mErrors );
                                   CError tError = new CError();
                                   tError.moduleName = "GrpPolApproveBLS";
                                   tError.functionName = "saveData";
                                   tError.errorMessage = "LCContDBSet修改失败!";
                                   this.mErrors .addOneError(tError) ;
                                   conn.rollback();
                                   conn.close();
                                   return false;
                           }

			conn.commit() ;
                        conn.close();
		} // end of try
		catch( Exception ex )
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpPolApproveBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError( tError );
			try{ conn.rollback(); conn.close(); } catch( Exception e ){}
			return false;
		}

		if(mLCGrpContSchema.getApproveFlag().equals("1"))
		{
		  // @@错误处理
		  CError tError = new CError();
		  tError.moduleName = "GrpPolApproveBLS";
		  tError.functionName = "submitData";
		  tError.errorMessage = "有未回复的操作员问题件,新单复核不通过,待复核修改状态!";
		  this.mErrors .addOneError( tError );
		  return false;
		}
	    return true;
	}
}
