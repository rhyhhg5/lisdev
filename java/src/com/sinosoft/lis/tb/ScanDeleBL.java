package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.ES_DOC_PAGESDB;
import com.sinosoft.lis.db.ES_DOC_RELATIONDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_MAINBSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESBSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONBSchema;
import com.sinosoft.lis.vschema.ES_DOC_PAGESBSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONBSet;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SysConst;

public class ScanDeleBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 扫描件类型 */
    private ES_DOC_MAINSchema mES_DOC_MAINSchema = null;

    private ES_DOC_PAGESSet mES_DOC_PAGESSet = null;

    private ES_DOC_RELATIONSet mES_DOC_RELATIONSet = null;

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        try
        {
            mOperate = cOperate;

            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            if (mGlobalInput == null)
            {
                buildError("getInputData", "处理超时，请重新登录。");
                return false;
            }

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mTransferData == null)
            {
                buildError("getInputData", "所需参数不完整。");
                return false;
            }

            String tDocId = (String) mTransferData.getValueByName("DocId");
            if (!getScannerInfo(tDocId))
            {
                buildError("getInputData", "编号为[" + tDocId + "] 的扫描件信息不存在或不完整。");
                return false;
            }
        }
        catch (Exception ex)
        {
            buildError("getInputData", "未知异常：" + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * checkData
     * 
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 
     * @return boolean
     */
    private boolean dealData()
    {
        MMap tTmpMap = null;

        // 备份扫描件信息。
        tTmpMap = backupScannerInfo();
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        // -----------------------------

        // 删除扫描件信息。
        tTmpMap = delScanner();
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        // -----------------------------

        // 创建日志。
        createTraceLog();
        // -----------------------------

        return true;
    }

    /**
     * 备份扫描件所有相关信息。
     * @return
     */
    private MMap backupScannerInfo()
    {
        MMap tMMap = new MMap();

        // 备份Es_Doc_Main
        ES_DOC_MAINBSchema tES_DOC_MAINBSchema = new ES_DOC_MAINBSchema();
        new Reflections().transFields(tES_DOC_MAINBSchema, mES_DOC_MAINSchema);
        tMMap.put(tES_DOC_MAINBSchema, SysConst.INSERT);
        // -----------------------------------------

        // 备份Es_Doc_Pages
        ES_DOC_PAGESBSet tES_DOC_PAGESBSet = new ES_DOC_PAGESBSet();
        tES_DOC_PAGESBSet.add(new ES_DOC_PAGESBSchema());
        new Reflections().transFields(tES_DOC_PAGESBSet, mES_DOC_PAGESSet);
        tMMap.put(tES_DOC_PAGESBSet, SysConst.INSERT);
        // -----------------------------------------

        // 备份Es_Doc_Relation
        ES_DOC_RELATIONBSet tES_DOC_RELATIONBSet = new ES_DOC_RELATIONBSet();
        tES_DOC_RELATIONBSet.add(new ES_DOC_RELATIONBSchema());
        new Reflections()
                .transFields(tES_DOC_RELATIONBSet, mES_DOC_RELATIONSet);
        tMMap.put(tES_DOC_RELATIONBSet, SysConst.INSERT);
        // -----------------------------------------

        return tMMap;
    }

    /**
     * 删除扫描件信息。
     * @return
     */
    private MMap delScanner()
    {
        MMap tMMap = new MMap();

        tMMap.put(mES_DOC_MAINSchema, SysConst.DELETE);
        tMMap.put(mES_DOC_PAGESSet, SysConst.DELETE);
        tMMap.put(mES_DOC_RELATIONSet, SysConst.DELETE);

        return tMMap;
    }

    /**
     * 记录日志
     * @return
     */
    private MMap createTraceLog()
    {
        return null;
    }

    /**
     * 获取该扫描件所有相关信息。
     * @param cDocId 扫描件id
     * @return 
     */
    private boolean getScannerInfo(String cDocId)
    {
        try
        {
            if (cDocId == null || cDocId.equals(""))
            {
                return false;
            }

            // 获取扫描主表信息。
            ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
            tES_DOC_MAINDB.setDocID(cDocId);
            if (!tES_DOC_MAINDB.getInfo())
            {
                return false;
            }
            mES_DOC_MAINSchema = tES_DOC_MAINDB.getSchema();
            // -----------------------------------------------

            // 获取扫描件全部页信息。
            ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
            tES_DOC_PAGESDB.setDocID(cDocId);
            mES_DOC_PAGESSet = tES_DOC_PAGESDB.query();
            if (mES_DOC_PAGESSet == null || mES_DOC_PAGESSet.size() <= 0)
            {
                return false;
            }
            // -----------------------------------------------

            // 获取扫描件业务关系信息。
            ES_DOC_RELATIONDB tES_DOC_RELATIONDB = new ES_DOC_RELATIONDB();
            tES_DOC_RELATIONDB.setDocID(cDocId);
            mES_DOC_RELATIONSet = tES_DOC_RELATIONDB.query();
            if (mES_DOC_RELATIONSet == null || mES_DOC_RELATIONSet.size() <= 0)
            {
                return false;
            }
            // -----------------------------------------------

            return true;
        }
        catch (Exception e)
        {
            buildError("getScannerMainInfo", e.getMessage());
            return false;
        }
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ScanDeleBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
