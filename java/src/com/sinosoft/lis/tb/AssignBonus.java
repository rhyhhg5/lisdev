package com.sinosoft.lis.tb;

import java.util.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import java.text.*;
import java.sql.*;

//import com.sinosoft.utility.JdbcUrl;
/**
 * <p>Title: </p>
 * <p>Description: 每天运行，用于分配红利</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class AssignBonus {

  public int COUNT = 100;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private GlobalInput mGI = new GlobalInput();
  public CErrors mErrors = new CErrors();

  /*转换精确位数的对象   */
  private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
  private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

  public AssignBonus() {
    mGI.ManageCom = "86";
    mGI.Operator = "";
  }

  public static void main(String[] args) {
    //用于批量后台－分红处理
    AssignBonus assignBonus1 = new AssignBonus();
    assignBonus1.setGlobalInput("86", "001");
    assignBonus1.runAssignBonus();
  }

  /**
   * 接收传入的数据
   * @param tGI
   * @return
   */
  public boolean setGlobalInput(GlobalInput tGI) {
    if (tGI == null) {
      CError tCError = new CError();
      tCError.functionName = "getGlobalInput";
      tCError.moduleName = "CalBonus";
      tCError.errorMessage = "请传入GlobalInput数据！";
      mErrors.addOneError(tCError);
    }
    mGI = tGI;
    return true;
  }

  public boolean setGlobalInput(String ManageCom, String Operator) {
    mGI.ManageCom = ManageCom;
    mGI.Operator = Operator;
    return true;
  }

  /**
   * 分配红利程序
   * @return
   */
  public boolean runAssignBonus() {

    String errDescribe = "";
    String curYear = StrTool.getYear(); //当前年
    int lastYear = Integer.valueOf(StrTool.getYear()).intValue() - 1; //上一年度
    String CurDate = PubFun.getCurrentDate(); //当天日期
    ExeSQL tExeSQL = new ExeSQL();
    String strSQL =
        "select count(*) from LOBonusPol where BonusFlag='0' and FiscalYear='" +
        lastYear + "' and SGetDate<='" + PubFun.getCurrentDate() +
        "' and AGetDate is null";
    SSRS tSSRS = tExeSQL.execSQL(strSQL);
    String strCount = tSSRS.GetText(1, 1);
    int SumCount = Integer.parseInt(strCount);
    if (SumCount == 0) {
      return true;
    }
    String tLimit = PubFun.getNoLimit(mGI.ManageCom);
    String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //产生流水号码
    int Num = 1;
    strSQL =
        "select PolNo,BonusMoney from LOBonusPol where BonusFlag='0' and FiscalYear='" +
        lastYear + "' and SGetDate<='" + PubFun.getCurrentDate() +
        "' and AGetDate is null";
    String tPolNo = "";
    String tBonusMoney = "";
    LCPolDB tLCPolDB = new LCPolDB();
    LCPolSchema tLCPolSchema = new LCPolSchema();
    LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB();
    while (Num <= SumCount) {
      Num = Num + COUNT;
      tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(strSQL, 1, COUNT);
      for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        tPolNo = tSSRS.GetText(i, 1);
        tBonusMoney = tSSRS.GetText(i, 2);
        tLCPolDB.setPolNo(tPolNo);
        if (tLCPolDB.getInfo() == false) {
          //纪录错误日志--往日志表中插入纪录
          errDescribe = "查询保单（" + tPolNo + "）失败：" +
              tLCPolDB.mErrors.getFirstError();
          //insertErrTable(tSerialNo,tPolNo,errDescribe);
          continue;
        }
        tLCPolSchema = tLCPolDB.getSchema();
        if (Double.parseDouble(tBonusMoney) == 0) {
          updateLCPolBonusForZero(tLCPolSchema, lastYear);
          continue;
        }

        tLOBonusPolDB = new LOBonusPolDB();
        tLOBonusPolDB.setPolNo(tLCPolSchema.getPolNo());
        tLOBonusPolDB.setFiscalYear(lastYear);
        if (tLOBonusPolDB.getInfo() == false) {
          //纪录错误日志--往日志表中插入纪录
          errDescribe = "保单红利表中没有找到保单号为:" + tLCPolSchema.getPolNo() + ",会计年度为:" +
              lastYear + "的数据!";
          insertErrLog(tSerialNo, tPolNo, errDescribe,
                       tLCPolSchema.getBonusGetMode());
          continue;
        }
        LOBonusPolSchema tLOBonusPolSchema = tLOBonusPolDB.getSchema();

        int tDay = PubFun.calInterval(tLCPolSchema.getPaytoDate(),
                                      tLOBonusPolSchema.getSGetDate(), "D");
        if (tDay > 0) {
          //纪录错误日志--往日志表中插入纪录
          errDescribe = tLOBonusPolSchema.getPolNo() + "保单的交至日期小于红利应分配日期!";
          insertErrLog(tSerialNo, tPolNo, errDescribe,
                       tLCPolSchema.getBonusGetMode());
          continue;
        }
        int tDay2 = PubFun.calInterval(tLOBonusPolSchema.getSGetDate(),
                                       PubFun.getCurrentDate(), "D");
        if (tDay2 < 0) {
          //纪录错误日志--往日志表中插入纪录
          errDescribe = tLOBonusPolSchema.getPolNo() + "未到红利应分配日期，不能分配该保单红利!";
          insertErrLog(tSerialNo, tPolNo, errDescribe,
                       tLCPolSchema.getBonusGetMode());
          continue;
        }

        System.out.println("BonusGetMode:" + tLCPolSchema.getBonusGetMode());
        if (tLCPolSchema.getBonusGetMode() == null) {
          tLCPolSchema.setBonusGetMode("1");
        }
        if (tLCPolSchema.getBonusGetMode().trim().equals("")) {
          tLCPolSchema.setBonusGetMode("1");
        }
        //如果红利领取方式是现金
        if (tLCPolSchema.getBonusGetMode().equals("2")) {
          if (getByCash(tLCPolSchema, tBonusMoney, tSerialNo, lastYear,
                        tLOBonusPolSchema) == false) {
            //纪录错误日志--往日志表中插入纪录
            errDescribe = "getByCash函数内部处理错误:" + mErrors.getFirstError() + "";
            if (insertErrLog(tSerialNo, tPolNo, errDescribe,
                             tLCPolSchema.getBonusGetMode()) == false)
              return false;
          }
          continue;
        }
        //如果红利领取方式是抵交续期保费
        if (tLCPolSchema.getBonusGetMode().equals("3")) {
          if (getByRePay(tLCPolSchema, tBonusMoney, tSerialNo, lastYear,
                         tLOBonusPolSchema) == false) {
            //纪录错误日志--往日志表中插入纪录
            errDescribe = "getByRePay函数内部处理错误:" + mErrors.getFirstError() + "";
            if (insertErrLog(tSerialNo, tPolNo, errDescribe,
                             tLCPolSchema.getBonusGetMode()) == false)
              return false;
          }
          continue;
        }
        //如果红利领取方式是累计生息
        if (tLCPolSchema.getBonusGetMode().equals("1")) {
          if (getBySumInterest(tLCPolSchema, tBonusMoney, tSerialNo, lastYear,
                               tLOBonusPolSchema) == false) {
            //纪录错误日志--往日志表中插入纪录
            errDescribe = "getBySumInterest函数内部处理错误:" + mErrors.getFirstError() +
                "";
            if (insertErrLog(tSerialNo, tPolNo, errDescribe,
                             tLCPolSchema.getBonusGetMode()) == false)
              return false;
          }
          continue;
        }
        //如果红利领取方式是增额交清
        if (tLCPolSchema.getBonusGetMode().equals("5")) {
          if (getByAddPrem(tLCPolSchema, tBonusMoney, tSerialNo, lastYear,
                           tLOBonusPolSchema) == false) {
            //纪录错误日志--往日志表中插入纪录
            errDescribe = "getByAddPrem函数内部处理错误:" + mErrors.getFirstError() +
                "";
            if (insertErrLog(tSerialNo, tPolNo, errDescribe,
                             tLCPolSchema.getBonusGetMode()) == false)
              return false;
          }
          continue;
        }
      }
    }
    return true;
  }

  /**
   * 红利给付方式为领取现金时的处理--向实付总表插入一条纪录,更新
   * @param tLCPolSchema
   * @param tBonusMoney
   * @return
   */
  private boolean getByCash(LCPolSchema tLCPolSchema, String tBonusMoney,
                            String tSNo, int tBonusYear,
                            LOBonusPolSchema tLOBonusPolSchema) {
    String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
    String tActuGetNo = PubFun1.CreateMaxNo("GETNO", tLimit); //产生实付号码
    String tGetNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit); //产生即付通知书号

    String tDrawerID = "";
    LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
    tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
    tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
    if (tLCAppntIndDB.getInfo() == false) {
      tDrawerID = "";
    }
    else {
      tDrawerID = tLCAppntIndDB.getIDNo();
    }

    //实付数据-总表
    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    tLJAGetSchema.setActuGetNo(tActuGetNo);
    tLJAGetSchema.setOtherNo(tLCPolSchema.getPolNo());
    tLJAGetSchema.setOtherNoType("7");
    tLJAGetSchema.setPayMode("1");
    tLJAGetSchema.setAppntNo(tLCPolSchema.getAppntNo());
    tLJAGetSchema.setSumGetMoney(tBonusMoney);
    tLJAGetSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
    tLJAGetSchema.setShouldDate(tLOBonusPolSchema.getSGetDate());
    tLJAGetSchema.setApproveCode(tLCPolSchema.getApproveCode());
    tLJAGetSchema.setApproveDate(tLCPolSchema.getApproveDate());
    tLJAGetSchema.setGetNoticeNo(tGetNoticeNo);
    tLJAGetSchema.setDrawer(tLCPolSchema.getAppntName());
    tLJAGetSchema.setDrawerID(tDrawerID);
    tLJAGetSchema.setSerialNo(tSNo);
    tLJAGetSchema.setOperator(tLCPolSchema.getOperator());
    tLJAGetSchema.setMakeDate(CurrentDate);
    tLJAGetSchema.setMakeTime(CurrentTime);
    tLJAGetSchema.setModifyDate(CurrentDate);
    tLJAGetSchema.setModifyTime(CurrentTime);
    tLJAGetSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJAGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJAGetSchema.setAgentType(tLCPolSchema.getAgentType());
    tLJAGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJAGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());

    //实付子表-红利给付实付表
    LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
    tLJABonusGetSchema.setActuGetNo(tActuGetNo);
    tLJABonusGetSchema.setOtherNo(tLCPolSchema.getPolNo());
    tLJABonusGetSchema.setOtherNoType("7");
    tLJABonusGetSchema.setPayMode("1");
    tLJABonusGetSchema.setBonusYear(String.valueOf(tBonusYear));
    tLJABonusGetSchema.setGetMoney(tBonusMoney);
    tLJABonusGetSchema.setGetDate(tLOBonusPolSchema.getSGetDate());
    tLJABonusGetSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJABonusGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJABonusGetSchema.setAgentType(tLCPolSchema.getAgentType());
    tLJABonusGetSchema.setAPPntName(tLCPolSchema.getAppntName());
    tLJABonusGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    tLJABonusGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJABonusGetSchema.setFeeFinaType("HLTF"); //红利退费
    tLJABonusGetSchema.setFeeOperationType("HLTF");
    tLJABonusGetSchema.setState("0");
    tLJABonusGetSchema.setSerialNo(tSNo);
    tLJABonusGetSchema.setOperator(tLCPolSchema.getOperator());
    tLJABonusGetSchema.setMakeDate(CurrentDate);
    tLJABonusGetSchema.setMakeTime(CurrentTime);
    tLJABonusGetSchema.setModifyDate(CurrentDate);
    tLJABonusGetSchema.setModifyTime(CurrentTime);
    tLJABonusGetSchema.setGetNoticeNo(tGetNoticeNo);
    //保单红利表更新
    String strSQL = "update LOBonusPol set AGetDate='" + CurrentDate +
        "' , BonusFlag='1'";
    strSQL = strSQL + " , ModifyDate='" + CurrentDate + "' , ModifyTime='" +
        CurrentTime + "'";
    strSQL = strSQL + " where PolNo='" + tLCPolSchema.getPolNo() +
        "' and FiscalYear=" + tBonusYear;

    //事务更新
    Connection conn = DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByCash";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    try {
      conn.setAutoCommit(false);

      /** 实付总表 */
      System.out.println("Start 实付总表...");
      LJAGetDB tLJAGetDB = new LJAGetDB(conn);
      tLJAGetDB.setSchema(tLJAGetSchema);
      if (!tLJAGetDB.insert()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByCash";
        tError.errorMessage = "实付总表数据保存失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /** 实付子表 */
      System.out.println("Start 实付子表...");
      LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB(conn);
      tLJABonusGetDB.setSchema(tLJABonusGetSchema);
      if (!tLJABonusGetDB.insert()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByCash";
        tError.errorMessage = "实付子表数据保存失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /**保单红利表更新**/
      ExeSQL tExeSQL = new ExeSQL(conn);
      if (!tExeSQL.execUpdateSQL(strSQL)) {
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByCash";
        tError.errorMessage = "保单红利表数据更新失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

//            LOPRTManagerSchema tLOPRTManagerSchema=getPrintData(tLCPolSchema);
//            if(tLOPRTManagerSchema==null)
//            {
//                CError tError = new CError();
//                tError.moduleName = "AssignBonus";
//                tError.functionName = "getByCash";
//                tError.errorMessage = "保单红利表打印数据准备失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }
//
//            LOPRTManagerDB tLOPRTManagerDB=new LOPRTManagerDB();
//            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
//            if(tLOPRTManagerDB.insert()==false)
//            {
//                this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "AssignBonus";
//                tError.functionName = "getByCash";
//                tError.errorMessage = "保单红利表打印数据保存失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }

      conn.commit();
      conn.close();
    }
    catch (Exception ex) {
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByCash";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      try {
        conn.rollback();
      }
      catch (Exception e) {}
      return false;
    }
    return true;
  }

  /**
   * 红利用于抵交续期保费的处理
   * @param tLCPolSchema
   * @param tBonusMoney
   * @param tSNo
   * @param tBonusYear
   * @return
   */
  private boolean getByRePay(LCPolSchema tLCPolSchema, String tBonusMoney,
                             String tSNo, int tBonusYear,
                             LOBonusPolSchema tLOBonusPolSchema) {
    String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
    String tActuGetNo = PubFun1.CreateMaxNo("GETNO", tLimit); //产生实付号码
    String tGetNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit); //产生即付通知书号
    String tTempFeeNo = PubFun1.CreateMaxNo("PAYNO", tLimit); //产生暂交费号
    String tappntname = "";
    String tDrawerID = "";
    LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
    tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
    tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
    if (tLCAppntIndDB.getInfo() == false) {
      tDrawerID = "";
    }
    else {
      tDrawerID = tLCAppntIndDB.getIDNo();
      tappntname = tLCAppntIndDB.getName();
    }

    //添加实付数据-总表
    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    tLJAGetSchema.setActuGetNo(tActuGetNo);
    tLJAGetSchema.setOtherNo(tLCPolSchema.getPolNo());
    tLJAGetSchema.setOtherNoType("7");
    tLJAGetSchema.setPayMode("5"); //内部转账
    tLJAGetSchema.setAppntNo(tLCPolSchema.getAppntNo());
    tLJAGetSchema.setSumGetMoney(tBonusMoney);
    tLJAGetSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
    tLJAGetSchema.setShouldDate(tLOBonusPolSchema.getSGetDate());
    tLJAGetSchema.setEnterAccDate(CurrentDate);
    tLJAGetSchema.setConfDate(CurrentDate);
    tLJAGetSchema.setApproveCode(tLCPolSchema.getApproveCode());
    tLJAGetSchema.setApproveDate(tLCPolSchema.getApproveDate());
    tLJAGetSchema.setGetNoticeNo(tGetNoticeNo);
    tLJAGetSchema.setDrawer(tLCPolSchema.getAppntName());
    tLJAGetSchema.setDrawerID(tDrawerID);
    tLJAGetSchema.setSerialNo(tSNo);
    tLJAGetSchema.setOperator(tLCPolSchema.getOperator());
    tLJAGetSchema.setMakeDate(CurrentDate);
    tLJAGetSchema.setMakeTime(CurrentTime);
    tLJAGetSchema.setModifyDate(CurrentDate);
    tLJAGetSchema.setModifyTime(CurrentTime);
    tLJAGetSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJAGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJAGetSchema.setAgentType(tLCPolSchema.getAgentType());
    tLJAGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJAGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    /*Lis5.3 upgrade get
     tLJAGetSchema.setBankCode(tLCPolSchema.getBankCode());
     tLJAGetSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
     */
    //添加实付子表-红利给付实付表
    LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
    tLJABonusGetSchema.setActuGetNo(tActuGetNo);
    tLJABonusGetSchema.setOtherNo(tLCPolSchema.getPolNo());
    tLJABonusGetSchema.setOtherNoType("7");
    tLJABonusGetSchema.setPayMode("5"); //内部转账
    tLJABonusGetSchema.setBonusYear(String.valueOf(tBonusYear));
    tLJABonusGetSchema.setGetMoney(tBonusMoney);
    tLJABonusGetSchema.setGetDate(CurrentDate);
    tLJABonusGetSchema.setEnterAccDate(tLOBonusPolSchema.getSGetDate());
    tLJABonusGetSchema.setConfDate(tLOBonusPolSchema.getSGetDate());
    tLJABonusGetSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJABonusGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJABonusGetSchema.setAgentType(tLCPolSchema.getAgentType());
    tLJABonusGetSchema.setAPPntName(tLCPolSchema.getAppntName());
    tLJABonusGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    tLJABonusGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJABonusGetSchema.setFeeFinaType("HLTF"); //红利退费
    tLJABonusGetSchema.setFeeOperationType("HLTF");
    tLJABonusGetSchema.setSerialNo(tSNo);
    tLJABonusGetSchema.setOperator(tLCPolSchema.getOperator());
    tLJABonusGetSchema.setState("0");
    tLJABonusGetSchema.setMakeDate(CurrentDate);
    tLJABonusGetSchema.setMakeTime(CurrentTime);
    tLJABonusGetSchema.setModifyDate(CurrentDate);
    tLJABonusGetSchema.setModifyTime(CurrentTime);
    tLJABonusGetSchema.setGetNoticeNo(tGetNoticeNo);

    //财务实付总表
    LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
    tLJFIGetSchema.setActuGetNo(tLJAGetSchema.getActuGetNo());
    tLJFIGetSchema.setPayMode(tLJAGetSchema.getPayMode());
    tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
    tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
    tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
    tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
    tLJFIGetSchema.setEnterAccDate(tLJAGetSchema.getEnterAccDate());
    tLJFIGetSchema.setConfDate(tLJAGetSchema.getConfDate());
    tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
    tLJFIGetSchema.setManageCom(tLJAGetSchema.getManageCom());
    tLJFIGetSchema.setAPPntName(tappntname);
    tLJFIGetSchema.setAgentCom(tLJAGetSchema.getAgentCom());
    tLJFIGetSchema.setAgentType(tLJAGetSchema.getAgentType());
    tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
    tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
    tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
    tLJFIGetSchema.setDrawer(tLJAGetSchema.getDrawer());
    tLJFIGetSchema.setDrawerID(tLJAGetSchema.getDrawerID());
    tLJFIGetSchema.setOperator(tLJAGetSchema.getOperator());
    tLJFIGetSchema.setMakeTime(tLJAGetSchema.getMakeTime());
    tLJFIGetSchema.setMakeDate(tLJAGetSchema.getMakeDate());
    tLJFIGetSchema.setState("0");
    tLJFIGetSchema.setModifyDate(tLJAGetSchema.getModifyDate());
    tLJFIGetSchema.setModifyTime(tLJAGetSchema.getModifyTime());

    //添加暂交费纪录
    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
    tLJTempFeeSchema.setTempFeeNo(tTempFeeNo);
    tLJTempFeeSchema.setTempFeeType("7");
    tLJTempFeeSchema.setRiskCode(tLCPolSchema.getRiskCode());
    tLJTempFeeSchema.setPayIntv(tLCPolSchema.getPayIntv());
    tLJTempFeeSchema.setOtherNo(tLCPolSchema.getPolNo());
    tLJTempFeeSchema.setOtherNoType("0");
    tLJTempFeeSchema.setPayMoney(tBonusMoney);
    tLJTempFeeSchema.setPayDate(CurrentDate);
    tLJTempFeeSchema.setEnterAccDate(CurrentDate);
    tLJTempFeeSchema.setConfDate(CurrentDate);
    tLJTempFeeSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
    tLJTempFeeSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJTempFeeSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJTempFeeSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    tLJTempFeeSchema.setAgentType(tLCPolSchema.getAgentType());
    tLJTempFeeSchema.setAPPntName(tLCPolSchema.getAppntName());
    tLJTempFeeSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJTempFeeSchema.setConfFlag("1");
    tLJTempFeeSchema.setSerialNo(tSNo);
    tLJTempFeeSchema.setOperator(tLCPolSchema.getOperator());
    tLJTempFeeSchema.setMakeDate(CurrentDate);
    tLJTempFeeSchema.setMakeTime(CurrentTime);
    tLJTempFeeSchema.setModifyDate(CurrentDate);
    tLJTempFeeSchema.setModifyTime(CurrentTime);
    tLJTempFeeSchema.setConfMakeDate(CurrentDate);
    //添加暂交费子表
    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    tLJTempFeeClassSchema.setTempFeeNo(tTempFeeNo);
    tLJTempFeeClassSchema.setPayMode("5"); //内部转账
    tLJTempFeeClassSchema.setPayMoney(tBonusMoney);
    //2005.1.20杨明注释掉问题代码tLJTempFeeClassSchema.setAPPntName(tLCPolSchema.getAppntName());
    tLJTempFeeClassSchema.setPayDate(CurrentDate);
    tLJTempFeeClassSchema.setApproveDate(tLCPolSchema.getApproveDate());
    tLJTempFeeClassSchema.setConfDate(CurrentDate);
    tLJTempFeeClassSchema.setEnterAccDate(CurrentDate);
    tLJTempFeeClassSchema.setConfFlag("1");
    tLJTempFeeClassSchema.setSerialNo(tSNo);
    tLJTempFeeClassSchema.setOperator(tLCPolSchema.getOperator());
    tLJTempFeeClassSchema.setMakeDate(CurrentDate);
    tLJTempFeeClassSchema.setMakeTime(CurrentTime);
    tLJTempFeeClassSchema.setModifyDate(CurrentDate);
    tLJTempFeeClassSchema.setModifyTime(CurrentTime);
    tLJTempFeeClassSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJTempFeeClassSchema.setConfMakeDate(CurrentDate);
    //添加实收总表纪录
    LJAPaySchema tLJAPaySchema = new LJAPaySchema();
    tLJAPaySchema.setPayNo(tTempFeeNo);
    tLJAPaySchema.setIncomeNo(tLCPolSchema.getPolNo());
    tLJAPaySchema.setIncomeType("2");
    tLJAPaySchema.setAppntNo(tLCPolSchema.getAppntNo());
    tLJAPaySchema.setSumActuPayMoney(tBonusMoney);
    tLJAPaySchema.setPayDate(CurrentDate);
    tLJAPaySchema.setEnterAccDate(CurrentDate);
    tLJAPaySchema.setConfDate(CurrentDate);
    tLJAPaySchema.setApproveCode(tLCPolSchema.getApproveCode());
    tLJAPaySchema.setApproveDate(tLCPolSchema.getApproveDate());
    tLJAPaySchema.setSerialNo(tSNo);
    tLJAPaySchema.setOperator(tLCPolSchema.getOperator());
    tLJAPaySchema.setMakeDate(CurrentDate);
    tLJAPaySchema.setMakeTime(CurrentTime);
    tLJAPaySchema.setModifyDate(CurrentDate);
    tLJAPaySchema.setModifyTime(CurrentTime);
    tLJAPaySchema.setGetNoticeNo(tGetNoticeNo);
    tLJAPaySchema.setManageCom(tLCPolSchema.getManageCom());
    tLJAPaySchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJAPaySchema.setAgentType(tLCPolSchema.getAgentType());
    tLJAPaySchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    /*Lis5.3 upgrade get
     tLJAPaySchema.setBankCode(tLCPolSchema.getBankCode());      //银行编码
     tLJAPaySchema.setBankAccNo(tLCPolSchema.getBankAccNo());   //银行帐号
     */
    tLJAPaySchema.setRiskCode(tLCPolSchema.getRiskCode()); // 险种编码

    //添加实收个人子表纪录
    LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
    tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());
    tLJAPayPersonSchema.setPayCount(2); //-记为续期
    tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
    tLJAPayPersonSchema.setContNo(tLCPolSchema.getContNo());
    tLJAPayPersonSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
    tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
    tLJAPayPersonSchema.setPayNo(tTempFeeNo);
    if (tLCPolSchema.getGrpPolNo().equals("00000000000000000000") &&
        tLCPolSchema.getContNo().equals("00000000000000000000")) {
      tLJAPayPersonSchema.setPayAimClass("1"); //交费目的分类
    }
    if (!tLCPolSchema.getGrpPolNo().equals("00000000000000000000") &&
        tLCPolSchema.getContNo().equals("00000000000000000000")) {
      tLJAPayPersonSchema.setPayAimClass("2"); //交费目的分类
    }
    tLJAPayPersonSchema.setDutyCode("0000000000"); //责任编码
    tLJAPayPersonSchema.setPayPlanCode("00000000"); //交费计划编码
    tLJAPayPersonSchema.setSumActuPayMoney(tBonusMoney);
    tLJAPayPersonSchema.setSumDuePayMoney(tBonusMoney);
    tLJAPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());
    tLJAPayPersonSchema.setPayDate(CurrentDate);
    tLJAPayPersonSchema.setPayType("ZC");
    tLJAPayPersonSchema.setEnterAccDate(CurrentDate);
    tLJAPayPersonSchema.setConfDate(CurrentDate);
    tLJAPayPersonSchema.setSerialNo(tSNo);
    tLJAPayPersonSchema.setOperator(tLCPolSchema.getOperator());
    tLJAPayPersonSchema.setMakeDate(CurrentDate);
    tLJAPayPersonSchema.setMakeTime(CurrentTime);
    tLJAPayPersonSchema.setModifyDate(CurrentDate);
    tLJAPayPersonSchema.setModifyTime(CurrentTime);
    tLJAPayPersonSchema.setGetNoticeNo(tGetNoticeNo);
    tLJAPayPersonSchema.setInInsuAccState("0"); //转入保险帐户状态
    tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
    double douBonusMoney = Double.parseDouble(tBonusMoney);

    //事务更新
    Connection conn = DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByRePay";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    try {
      conn.setAutoCommit(false);

      /** 实付总表 */
      System.out.println("Start 实付总表...");
      LJAGetDB tLJAGetDB = new LJAGetDB(conn);
      tLJAGetDB.setSchema(tLJAGetSchema);
      if (!tLJAGetDB.insert()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实付总表数据保存失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /** 实付子表 */
      System.out.println("Start 实付子表...");
      LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB(conn);
      tLJABonusGetDB.setSchema(tLJABonusGetSchema);
      if (!tLJABonusGetDB.insert()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实付子表数据保存失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /** 财务实付表 */
      System.out.println("Start 财务实付表...");
      LJFIGetDB tLJFIGetDB = new LJFIGetDB(conn);
      tLJFIGetDB.setSchema(tLJFIGetSchema);
      if (!tLJFIGetDB.insert()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实付子表数据保存失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      String strSQL = "update LOBonusPol set AGetDate='" + CurrentDate +
          "' , BonusFlag='1'";
      strSQL = strSQL + " , ModifyDate='" + CurrentDate + "' , ModifyTime='" +
          CurrentTime + "'";
      strSQL = strSQL + " where PolNo='" + tLCPolSchema.getPolNo() +
          "' and FiscalYear=" + tBonusYear;
      /**保单红利表更新**/
      ExeSQL tExeSQL = new ExeSQL(conn);
      if (!tExeSQL.execUpdateSQL(strSQL)) {
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "保单红利表数据更新失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /**添加暂交费纪录*/
      LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB(conn);
      tLJTempFeeDB.setSchema(tLJTempFeeSchema);
      if (tLJTempFeeDB.insert() == false) {
        this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "暂交费主表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(conn);
      tLJTempFeeClassDB.setSchema(tLJTempFeeClassSchema);
      if (tLJTempFeeClassDB.insert() == false) {
        this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "暂交费子表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      LJAPayDB tLJAPayDB = new LJAPayDB(conn);
      tLJAPayDB.setSchema(tLJAPaySchema);
      if (tLJAPayDB.insert() == false) {
        this.mErrors.copyAllErrors(tLJAPayDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实收总表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB(conn);
      tLJAPayPersonDB.setSchema(tLJAPayPersonSchema);
      if (tLJAPayPersonDB.insert() == false) {
        this.mErrors.copyAllErrors(tLJAPayPersonDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实收个人表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /**个人保单表更新**/
      //更新个人保单表
      String strSQL2 = "update lcpol set LeavingMoney=" +
          (tLCPolSchema.getLeavingMoney() + douBonusMoney);
      strSQL2 = strSQL2 + ",ModifyDate='" + CurrentDate + "',ModifyTime='" +
          CurrentTime + "'  where polno='" + tLCPolSchema.getPolNo() + "' ";
      tExeSQL = new ExeSQL(conn);
      if (!tExeSQL.execUpdateSQL(strSQL2)) {
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "个人保单表数据更新失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

//            LOPRTManagerSchema tLOPRTManagerSchema=getPrintData(tLCPolSchema);
//            if(tLOPRTManagerSchema==null)
//            {
//                CError tError = new CError();
//                tError.moduleName = "AssignBonus";
//                tError.functionName = "getByCash";
//                tError.errorMessage = "保单红利表打印数据准备失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }
//
//            LOPRTManagerDB tLOPRTManagerDB=new LOPRTManagerDB(conn);
//            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
//            if(tLOPRTManagerDB.insert()==false)
//            {
//                this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "AssignBonus";
//                tError.functionName = "getByCash";
//                tError.errorMessage = "保单红利表打印数据保存失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }

      conn.commit();
      conn.close();
    }
    catch (Exception ex) {
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByRePay";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      try {
        conn.rollback();
      }
      catch (Exception e) {}
      return false;
    }

    return true;
  }

  /**
   * 红利用于累计生息
   * @param tLCPolSchema
   * @param tBonusMoney
   * @param tSNo
   * @param tBonusYear
   * @return
   */
  private boolean getBySumInterest(LCPolSchema tLCPolSchema, String tBonusMoney,
                                   String tSNo, int tBonusYear,
                                   LOBonusPolSchema tLOBonusPolSchema) {
    //1-看红利帐户是否存在，如果没有生成
    String operType = "";
    LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
    LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
    tLCInsureAccDB.setPolNo(tLCPolSchema.getPolNo());
    tLCInsureAccDB.setAccType("004"); //帐户类型为红利帐户
    LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();

    if (tLCInsureAccSet.size() == 0) {
      //效率做改进
      LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
      tLMRiskInsuAccDB.setAccType("004");
      LMRiskInsuAccSet tLMRiskInsuAccSet = tLMRiskInsuAccDB.query();
      if (tLMRiskInsuAccSet.size() == 0) {
        this.mErrors.copyAllErrors(tLMRiskInsuAccDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getBySumInterest";
        tError.errorMessage = "查询LMRiskInsuAcc表中帐户类型为004时失败！";
        this.mErrors.addOneError(tError);
        return false;
      }
      LMRiskInsuAccSchema tLMRiskInsuAccSchema = tLMRiskInsuAccSet.get(1);
      tLCInsureAccSchema = new LCInsureAccSchema();
      tLCInsureAccSchema.setPolNo(tLCPolSchema.getPolNo());
      tLCInsureAccSchema.setInsuAccNo(tLMRiskInsuAccSchema.getInsuAccNo());
      tLCInsureAccSchema.setRiskCode(tLCPolSchema.getRiskCode());
      tLCInsureAccSchema.setAccType("004");
      //2005.1.20杨明 注释掉问题代码tLCInsureAccSchema.setOtherNo(tLCPolSchema.getPolNo());
      //2005.1.20杨明 注释掉问题代码tLCInsureAccSchema.setOtherType("1");
      tLCInsureAccSchema.setContNo(tLCPolSchema.getContNo());
      tLCInsureAccSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
      tLCInsureAccSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
      //2005.1.20杨明 注释掉问题代码tLCInsureAccSchema.setAppntName(tLCPolSchema.getAppntName());
      tLCInsureAccSchema.setSumPay(0);
      tLCInsureAccSchema.setInsuAccBala(tBonusMoney);
      tLCInsureAccSchema.setUnitCount(0);
      tLCInsureAccSchema.setInsuAccGetMoney(tBonusMoney);
      tLCInsureAccSchema.setFrozenMoney(0);
      tLCInsureAccSchema.setAccComputeFlag(tLMRiskInsuAccSchema.
                                           getAccComputeFlag());
      tLCInsureAccSchema.setManageCom(tLCPolSchema.getManageCom());
      tLCInsureAccSchema.setOperator(tLCPolSchema.getOperator());
      tLCInsureAccSchema.setBalaDate(tLOBonusPolSchema.getSGetDate());
      tLCInsureAccSchema.setMakeDate(CurrentDate);
      tLCInsureAccSchema.setMakeTime(CurrentTime);
      tLCInsureAccSchema.setModifyDate(CurrentDate);
      tLCInsureAccSchema.setModifyTime(CurrentTime);
      operType = "INSERT";
    }
    else {
      tLCInsureAccSchema = tLCInsureAccSet.get(1);
      tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema.getInsuAccBala() +
                                        Double.parseDouble(tBonusMoney));
      tLCInsureAccSchema.setInsuAccGetMoney(tLCInsureAccSchema.
                                            getInsuAccGetMoney() +
                                            Double.parseDouble(tBonusMoney));
      tLCInsureAccSchema.setModifyDate(CurrentDate);
      tLCInsureAccSchema.setModifyTime(CurrentTime);
      operType = "UPDATE";
    }

    String tLimit = PubFun.getNoLimit(mGI.ManageCom);
    String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //产生流水号码

    //填充保险帐户表记价履历表
    LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
    tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
    //2005.1.20杨明 注释掉问题代码tLCInsureAccTraceSchema.setInsuredNo(tLCInsureAccSchema.getInsuredNo());
    tLCInsureAccTraceSchema.setPolNo(tLCInsureAccSchema.getPolNo());
    tLCInsureAccTraceSchema.setMoneyType("HL");
    tLCInsureAccTraceSchema.setRiskCode(tLCInsureAccSchema.getRiskCode());
    tLCInsureAccTraceSchema.setOtherNo(tLCInsureAccSchema.getPolNo());
    tLCInsureAccTraceSchema.setOtherType("1");
    tLCInsureAccTraceSchema.setMoney(tBonusMoney);
    tLCInsureAccTraceSchema.setContNo(tLCInsureAccSchema.getContNo());
    tLCInsureAccTraceSchema.setGrpPolNo(tLCInsureAccSchema.getGrpPolNo());
    tLCInsureAccTraceSchema.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
    /*Lis5.3 upgrade set
     tLCInsureAccTraceSchema.setAppntName(tLCInsureAccSchema.getAppntName());
     */
    tLCInsureAccTraceSchema.setState(tLCInsureAccSchema.getState());
    tLCInsureAccTraceSchema.setManageCom(tLCInsureAccSchema.getManageCom());
    tLCInsureAccTraceSchema.setOperator(tLCInsureAccSchema.getOperator());
    tLCInsureAccTraceSchema.setMakeDate(CurrentDate);
    tLCInsureAccTraceSchema.setMakeTime(CurrentTime);
    tLCInsureAccTraceSchema.setModifyDate(CurrentDate);
    tLCInsureAccTraceSchema.setModifyTime(CurrentTime);
    tLCInsureAccTraceSchema.setPayDate(tLOBonusPolSchema.getSGetDate());

    //事务更新
    Connection conn = DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByRePay";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    try {
      conn.setAutoCommit(false);

      tLCInsureAccDB = new LCInsureAccDB();
      tLCInsureAccDB.setSchema(tLCInsureAccSchema);
      if (operType.equals("INSERT")) {
        if (tLCInsureAccDB.insert() == false) {
          this.mErrors.copyAllErrors(tLCInsureAccDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "AssignBonus";
          tError.functionName = "getBySumInterest";
          tError.errorMessage = "添加保险帐户LCInsureAcc表数据失败！";
          this.mErrors.addOneError(tError);
          return false;
        }
      }
      else {
        if (tLCInsureAccDB.update() == false) {
          this.mErrors.copyAllErrors(tLCInsureAccDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "AssignBonus";
          tError.functionName = "getBySumInterest";
          tError.errorMessage = "更新保险帐户LCInsureAcc表数据失败！";
          this.mErrors.addOneError(tError);
          return false;
        }
      }

      LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
      tLCInsureAccTraceDB.setSchema(tLCInsureAccTraceSchema);
      if (tLCInsureAccTraceDB.insert() == false) {
        this.mErrors.copyAllErrors(tLCInsureAccTraceDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getBySumInterest";
        tError.errorMessage = "添加保险帐户表记价履历表LCInsureAccTrace表数据失败！";
        this.mErrors.addOneError(tError);
        return false;
      }

      String strSQL = "update LOBonusPol set AGetDate='" + CurrentDate +
          "', BonusFlag='1'";
      strSQL = strSQL + " , ModifyDate='" + CurrentDate + "' , ModifyTime='" +
          CurrentTime + "'";
      strSQL = strSQL + " where PolNo='" + tLCPolSchema.getPolNo() +
          "' and FiscalYear=" + tBonusYear;

      /**保单红利表更新**/
      ExeSQL tExeSQL = new ExeSQL(conn);
      if (!tExeSQL.execUpdateSQL(strSQL)) {
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByCash";
        tError.errorMessage = "保单红利表数据更新失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

//            LOPRTManagerSchema tLOPRTManagerSchema=getPrintData(tLCPolSchema);
//            if(tLOPRTManagerSchema==null)
//            {
//                CError tError = new CError();
//                tError.moduleName = "AssignBonus";
//                tError.functionName = "getByCash";
//                tError.errorMessage = "保单红利表打印数据准备失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }
//
//            LOPRTManagerDB tLOPRTManagerDB=new LOPRTManagerDB();
//            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
//            if(tLOPRTManagerDB.insert()==false)
//            {
//                this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "AssignBonus";
//                tError.functionName = "getByCash";
//                tError.errorMessage = "保单红利表打印数据保存失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }

      conn.commit();
      conn.close();
    }
    catch (Exception ex) {
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByRePay";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      try {
        conn.rollback();
      }
      catch (Exception e) {}
      return false;
    }
    return true;
  }

  /**
   * 红利用于增额交清
   * @param tLCPolSchema
   * @param tBonusMoney
   * @param tSNo
   * @param tBonusYear
   * @return
   */
  private boolean getByAddPrem(LCPolSchema tLCPolSchema, String tBonusMoney,
                               String tSNo, int tBonusYear,
                               LOBonusPolSchema tLOBonusPolSchema) {
    //1-查询该保单的责任信息
    LCDutySchema tLCDutySchema = new LCDutySchema();
    LCPremSchema tLCPremSchema = new LCPremSchema();
    LCGetSchema tLCGetSchema = new LCGetSchema();
    LCGetSet tSaveLCGetSet = new LCGetSet();

    LCDutyDB tLCDutyDB = new LCDutyDB();
    tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
    LCDutySet tLCDutySet = tLCDutyDB.query();
    if (tLCDutySet.size() == 0) {
      this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
      return false;
    }
    int serNo = 0;
    for (int i = 1; i <= tLCDutySet.size(); i++) {
      if (tLCDutySet.get(i).getDutyCode().length() != 6) {
        if (tLCDutySet.get(i).getDutyCode().length() == 10) {
          serNo = serNo + 1;
        }
        continue;
      }
      else {
        //得到一条正常责任项
        tLCDutySchema = tLCDutySet.get(i);
      }
    }
    LCPremDB tLCPremDB = new LCPremDB();
    tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
    tLCPremDB.setDutyCode(tLCDutySchema.getDutyCode());
    LCPremSet tLCPremSet = tLCPremDB.query();
    if (tLCPremSet.size() == 0) {
      this.mErrors.copyAllErrors(tLCPremDB.mErrors);
      return false;
    }
    for (int n = 1; n <= tLCPremSet.size(); n++) {
      if (tLCPremSet.get(n).getPayPlanCode().length() != 6) {
        continue;
      }
      //得到一条正常保费项纪录
      tLCPremSchema = tLCPremSet.get(n);
      break;
    }
    LCGetDB tLCGetDB = new LCGetDB();
    tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
    tLCGetDB.setDutyCode(tLCDutySchema.getDutyCode());
    LCGetSet tLCGetSet = tLCGetDB.query();
    if (tLCGetSet.size() == 0) {
      this.mErrors.copyAllErrors(tLCGetDB.mErrors);
      return false;
    }
    for (int n = 1; n <= tLCGetSet.size(); n++) {
      if (tLCGetSet.get(n).getGetDutyCode().length() != 6) {
        continue;
      }
      //得到一条正常领取项纪录
      tLCGetSchema = new LCGetSchema();
      tLCGetSchema = tLCGetSet.get(n);
      tSaveLCGetSet.add(tLCGetSchema);
    }

    //计算保单年度值--需要校验:即不满一年的需要加 1 --润年问题
    int PolYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),
                                     tLOBonusPolSchema.getSGetDate(), "Y");
    //int ActuAge=PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),tLOBonusPolSchema.getSGetDate(),"Y");
    int ActuAge = tLCPolSchema.getInsuredAppAge();
    CalBase mCalBase = new CalBase();
    //mCalBase.setAmnt(tLCPolSchema.getAmnt());
    mCalBase.setPrem(Double.parseDouble(tBonusMoney));
    mCalBase.setAppAge(ActuAge);
    mCalBase.setSex(tLCPolSchema.getInsuredSex());
    mCalBase.setPayEndYear(tLCPolSchema.getPayEndYear());
    mCalBase.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag());
    mCalBase.setAppAge(ActuAge);

    Calculator mCalculator = new Calculator();
    LMRiskBonusDB tLMRiskBonusDB = new LMRiskBonusDB();
    tLMRiskBonusDB.setRiskCode(tLCPolSchema.getRiskCode());
    if (tLMRiskBonusDB.getInfo() == false) {
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByAddPrem";
      tError.errorMessage = "没有找到增额交清对应的险种描述项";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (tLMRiskBonusDB.getAddAmntCoefCode() == null) {
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByAddPrem";
      tError.errorMessage = "没有找到增额交清对应的险种算法";
      this.mErrors.addOneError(tError);
      return false;
    }
    mCalculator.setCalCode(tLMRiskBonusDB.getAddAmntCoefCode());

    //增加基本要素
    mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
    //mCalculator.addBasicFactor("Amnt", mCalBase.getAmnt() );
    mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
    mCalculator.addBasicFactor("Sex", mCalBase.getSex());
    mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
    mCalculator.addBasicFactor("PayEndYearFlag", mCalBase.getPayEndYearFlag());
    mCalculator.addBasicFactor("PolYear", String.valueOf(PolYear)); //添加保单年度值
    mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());
    //计算得到新的保额
    String tStr = "";
    double mValue;
    tStr = mCalculator.calculate();
    if (tStr.trim().equals(""))
      mValue = 0;
    else
      mValue = Double.parseDouble(tStr);

    if (mValue == 0)return false;
    //格式化，取两位精度-校验
    String strCalPrem = mDecimalFormat.format(mValue); //转换计算后的保费(规定的精度)
    mValue = Double.parseDouble(strCalPrem);
    double douBonusMoney = Double.parseDouble(tBonusMoney);
    //更新个人保单表:保费增加,保额增加
    String strSQL2 = "update lcpol set Prem=" +
        (tLCPolSchema.getPrem() + douBonusMoney);
    strSQL2 = strSQL2 + ",SumPrem=" + (tLCPolSchema.getPrem() + douBonusMoney);
    strSQL2 = strSQL2 + ",Amnt=" + (tLCPolSchema.getAmnt() + mValue);
    strSQL2 = strSQL2 + ",RiskAmnt=" + (tLCPolSchema.getRiskAmnt() + mValue);
    strSQL2 = strSQL2 + ",ModifyDate='" + CurrentDate + "',ModifyTime='" +
        CurrentTime + "' where polno='" + tLCPolSchema.getPolNo() + "'";

    FDate tFDate = new FDate();

    //被保人新的年龄
    String newInsuredAge = Integer.toString(PubFun.calInterval(tLCPolSchema.
        getInsuredBirthday(), tLOBonusPolSchema.getSGetDate(), "Y"));
    //当前年龄和原年龄之间的间隔
    String intvAge = Integer.toString(Integer.parseInt(newInsuredAge) -
                                      tLCPolSchema.getInsuredAppAge());
    //新的保险年期
    String insuYear = Integer.toString(tLCPolSchema.getInsuYear() - PolYear);
    //新的交费年期
    String newPayYears = Integer.toString(tLCPolSchema.getPayYears() - PolYear);
    //新的交至日期=当前日期+新的保险年期
    String payToDate = tFDate.getString(PubFun.calDate(tFDate.getDate(
        tLOBonusPolSchema.getSGetDate()), Integer.parseInt(insuYear), "Y", null));
    //新的终交日期
    String payEndDate = payToDate;
    //新的终交年龄年期
    int newPayEndYear = tLCPolSchema.getPayEndYear();
    //终交年龄年期标志
    String payEndYearFlag = tLCPolSchema.getPayEndYearFlag();
    if (payEndYearFlag.equals("Y")) {
      newPayEndYear = tLCPolSchema.getPayEndYear() - Integer.parseInt(intvAge);
    }
    //新的保险年龄年期
    int newInsuYear = tLCPolSchema.getInsuYear();
    //终交年龄年期标志
    String insuYearFlag = tLCPolSchema.getInsuYearFlag();
    if (insuYearFlag.equals("Y")) {
      newInsuYear = tLCPolSchema.getInsuYear() - Integer.parseInt(intvAge);
    }

    //准备责任项，保费项，领取项数据--原来的责任编码为6位），第7位为1，剩余的3位为流水号
    serNo = serNo + 1;
    String newSerNo = "";
    String strSerNo = String.valueOf(serNo);
    if (strSerNo.length() == 1)
      newSerNo = "00" + strSerNo;
    if (strSerNo.length() == 2)
      newSerNo = "0" + strSerNo;
    if (strSerNo.length() == 3)
      newSerNo = strSerNo;
    tLCDutySchema.setDutyCode(tLCDutySchema.getDutyCode() + "1" + newSerNo);
    tLCDutySchema.setMult(1);
    tLCDutySchema.setPrem(tBonusMoney);
    tLCDutySchema.setStandPrem(tBonusMoney);
    tLCDutySchema.setSumPrem(tBonusMoney);
    tLCDutySchema.setAmnt(mValue);
    tLCDutySchema.setRiskAmnt(mValue);
    tLCDutySchema.setFirstPayDate(tLOBonusPolSchema.getSGetDate());
    tLCDutySchema.setPaytoDate(payToDate);
    tLCDutySchema.setPayEndDate(payToDate);
    tLCDutySchema.setPayIntv(0);
    tLCDutySchema.setPayEndYear(newPayEndYear);
    tLCDutySchema.setPayEndYearFlag(payEndYearFlag);
    tLCDutySchema.setInsuYear(newInsuYear);
    tLCDutySchema.setInsuYearFlag(insuYearFlag);
    tLCDutySchema.setPayYears(newPayYears);
    tLCDutySchema.setYears(insuYear);
    tLCDutySchema.setModifyDate(CurrentDate);
    tLCDutySchema.setModifyTime(CurrentTime);

    //准备保费项
    tLCPremSchema.setPolNo(tLCPolSchema.getPolNo());
    tLCPremSchema.setDutyCode(tLCDutySchema.getDutyCode());
    tLCPremSchema.setPayTimes(1);
    tLCPremSchema.setPayIntv(0);
    /*Lis5.3 upgrade set
             tLCPremSchema.setMult(1);
     */
    tLCPremSchema.setStandPrem(tBonusMoney);
    tLCPremSchema.setPrem(tBonusMoney);
    tLCPremSchema.setSumPrem(tBonusMoney);
    tLCPremSchema.setPayStartDate(tLOBonusPolSchema.getSGetDate());
    tLCPremSchema.setPayEndDate(payEndDate);
    tLCPremSchema.setPaytoDate(payToDate);
    tLCPremSchema.setUrgePayFlag("N");
    tLCPremSchema.setState("");
    tLCPremSchema.setModifyDate(CurrentDate);
    tLCPremSchema.setModifyTime(CurrentTime);

    //准备领取项
    for (int i = 1; i <= tSaveLCGetSet.size(); i++) {
      tSaveLCGetSet.get(i).setPolNo(tLCPolSchema.getPolNo());
      tSaveLCGetSet.get(i).setDutyCode(tLCDutySchema.getDutyCode());
      tSaveLCGetSet.get(i).setStandMoney(mValue);
      //tSaveLCGetSet.get(i).setActuGet(mValue);
      tSaveLCGetSet.get(i).setBalaDate("");
      tSaveLCGetSet.get(i).setModifyDate(CurrentDate);
      tSaveLCGetSet.get(i).setModifyTime(CurrentTime);
    }

    String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
    String tActuGetNo = PubFun1.CreateMaxNo("GETNO", tLimit); //产生实付号码
    String tGetNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit); //产生即付通知书号
    String tTempFeeNo = PubFun1.CreateMaxNo("PAYNO", tLimit); //产生暂交费号
    String tappntname = "";
    String tDrawerID = "";
    LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
    tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
    tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
    if (tLCAppntIndDB.getInfo() == false) {
      tDrawerID = "";
    }
    else {
      tDrawerID = tLCAppntIndDB.getIDNo();
      tappntname = tLCAppntIndDB.getName();
    }

    //添加实付数据-总表
    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    tLJAGetSchema.setActuGetNo(tActuGetNo);
    tLJAGetSchema.setOtherNo(tLCPolSchema.getPolNo());
    tLJAGetSchema.setOtherNoType("7");
    tLJAGetSchema.setPayMode("5"); //内部转账
    tLJAGetSchema.setAppntNo(tLCPolSchema.getAppntNo());
    tLJAGetSchema.setSumGetMoney(tBonusMoney);
    tLJAGetSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
    tLJAGetSchema.setShouldDate(tLOBonusPolSchema.getSGetDate());
    tLJAGetSchema.setEnterAccDate(CurrentDate);
    tLJAGetSchema.setConfDate(CurrentDate);

    tLJAGetSchema.setApproveCode(tLCPolSchema.getApproveCode());
    tLJAGetSchema.setApproveDate(tLCPolSchema.getApproveDate());
    tLJAGetSchema.setGetNoticeNo(tGetNoticeNo);
    tLJAGetSchema.setDrawer(tLCPolSchema.getAppntName());
    tLJAGetSchema.setDrawerID(tDrawerID);
    tLJAGetSchema.setSerialNo(tSNo);
    tLJAGetSchema.setOperator(tLCPolSchema.getOperator());
    tLJAGetSchema.setMakeDate(CurrentDate);
    tLJAGetSchema.setMakeTime(CurrentTime);
    tLJAGetSchema.setModifyDate(CurrentDate);
    tLJAGetSchema.setModifyTime(CurrentTime);
    tLJAGetSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJAGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJAGetSchema.setAgentType(tLCPolSchema.getAgentType());
    tLJAGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJAGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    /*Lis5.3 upgrade get
             tLJAGetSchema.setBankCode(tLCPolSchema.getBankCode());
             tLJAGetSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
     */
    //添加实付子表-红利给付实付表
    LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
    tLJABonusGetSchema.setActuGetNo(tActuGetNo);
    tLJABonusGetSchema.setOtherNo(tLCPolSchema.getPolNo());
    tLJABonusGetSchema.setOtherNoType("7");
    tLJABonusGetSchema.setPayMode("5"); //内部转账
    tLJABonusGetSchema.setBonusYear(String.valueOf(tBonusYear));
    tLJABonusGetSchema.setGetMoney(tBonusMoney);
    tLJABonusGetSchema.setGetDate(CurrentDate);
    tLJABonusGetSchema.setEnterAccDate(CurrentDate);
    tLJABonusGetSchema.setConfDate(CurrentDate);

    tLJABonusGetSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJABonusGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJABonusGetSchema.setAgentType(tLCPolSchema.getAgentType());
    tLJABonusGetSchema.setAPPntName(tLCPolSchema.getAppntName());
    tLJABonusGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    tLJABonusGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJABonusGetSchema.setFeeFinaType("HLTF"); //红利退费
    tLJABonusGetSchema.setFeeOperationType("HLTF");
    tLJABonusGetSchema.setSerialNo(tSNo);
    tLJABonusGetSchema.setOperator(tLCPolSchema.getOperator());
    tLJABonusGetSchema.setState("0");
    tLJABonusGetSchema.setMakeDate(CurrentDate);
    tLJABonusGetSchema.setMakeTime(CurrentTime);
    tLJABonusGetSchema.setModifyDate(CurrentDate);
    tLJABonusGetSchema.setModifyTime(CurrentTime);
    tLJABonusGetSchema.setGetNoticeNo(tGetNoticeNo);

    //财务实付总表
    LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
    tLJFIGetSchema.setActuGetNo(tLJAGetSchema.getActuGetNo());
    tLJFIGetSchema.setPayMode(tLJAGetSchema.getPayMode());
    tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
    tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
    tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
    tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
    tLJFIGetSchema.setEnterAccDate(tLJAGetSchema.getEnterAccDate());
    tLJFIGetSchema.setConfDate(tLJAGetSchema.getConfDate());
    tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
    tLJFIGetSchema.setManageCom(tLJAGetSchema.getManageCom());
    tLJFIGetSchema.setAPPntName(tappntname);
    tLJFIGetSchema.setAgentCom(tLJAGetSchema.getAgentCom());
    tLJFIGetSchema.setAgentType(tLJAGetSchema.getAgentType());
    tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
    tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
    tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
    tLJFIGetSchema.setDrawer(tLJAGetSchema.getDrawer());
    tLJFIGetSchema.setDrawerID(tLJAGetSchema.getDrawerID());
    tLJFIGetSchema.setOperator(tLJAGetSchema.getOperator());
    tLJFIGetSchema.setMakeTime(tLJAGetSchema.getMakeTime());
    tLJFIGetSchema.setMakeDate(tLJAGetSchema.getMakeDate());
    tLJFIGetSchema.setState("0");
    tLJFIGetSchema.setModifyDate(tLJAGetSchema.getModifyDate());
    tLJFIGetSchema.setModifyTime(tLJAGetSchema.getModifyTime());
    tLJFIGetSchema.setConfMakeDate(CurrentDate);

    //添加暂交费纪录
    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
    tLJTempFeeSchema.setTempFeeNo(tTempFeeNo);
    tLJTempFeeSchema.setTempFeeType("7");
    tLJTempFeeSchema.setRiskCode(tLCPolSchema.getRiskCode());
    tLJTempFeeSchema.setPayIntv(tLCPolSchema.getPayIntv());
    tLJTempFeeSchema.setOtherNo(tLCPolSchema.getPolNo());
    tLJTempFeeSchema.setOtherNoType("0");
    tLJTempFeeSchema.setPayMoney(tBonusMoney);
    tLJTempFeeSchema.setPayDate(CurrentDate);
    tLJTempFeeSchema.setEnterAccDate(CurrentDate);
    tLJTempFeeSchema.setConfDate(CurrentDate);
    tLJTempFeeSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
    tLJTempFeeSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJTempFeeSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJTempFeeSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    tLJTempFeeSchema.setAgentType(tLCPolSchema.getAgentType());
    tLJTempFeeSchema.setAPPntName(tLCPolSchema.getAppntName());
    tLJTempFeeSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJTempFeeSchema.setConfFlag("1");
    tLJTempFeeSchema.setSerialNo(tSNo);
    tLJTempFeeSchema.setOperator(tLCPolSchema.getOperator());
    tLJTempFeeSchema.setMakeDate(CurrentDate);
    tLJTempFeeSchema.setMakeTime(CurrentTime);
    tLJTempFeeSchema.setModifyDate(CurrentDate);
    tLJTempFeeSchema.setModifyTime(CurrentTime);
    tLJTempFeeSchema.setConfMakeDate(CurrentDate);
    //添加暂交费子表
    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    tLJTempFeeClassSchema.setTempFeeNo(tTempFeeNo);
    tLJTempFeeClassSchema.setPayMode("5"); //内部转账
    tLJTempFeeClassSchema.setPayMoney(tBonusMoney);
//    tLJTempFeeClassSchema.setAPPntName(tLCPolSchema.getAppntName());
    tLJTempFeeClassSchema.setPayDate(CurrentDate);
    tLJTempFeeClassSchema.setApproveDate(tLCPolSchema.getApproveDate());
    tLJTempFeeClassSchema.setConfDate(CurrentDate);
    tLJTempFeeClassSchema.setEnterAccDate(CurrentDate);
    tLJTempFeeClassSchema.setConfFlag("1");
    tLJTempFeeClassSchema.setSerialNo(tSNo);
    tLJTempFeeClassSchema.setOperator(tLCPolSchema.getOperator());
    tLJTempFeeClassSchema.setMakeDate(CurrentDate);
    tLJTempFeeClassSchema.setMakeTime(CurrentTime);
    tLJTempFeeClassSchema.setModifyDate(CurrentDate);
    tLJTempFeeClassSchema.setModifyTime(CurrentTime);
    tLJTempFeeClassSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJTempFeeClassSchema.setConfMakeDate(CurrentDate);

    //添加实收总表纪录
    LJAPaySchema tLJAPaySchema = new LJAPaySchema();
    tLJAPaySchema.setPayNo(tTempFeeNo);
    tLJAPaySchema.setIncomeNo(tLCPolSchema.getPolNo());
    tLJAPaySchema.setIncomeType("2");
    tLJAPaySchema.setAppntNo(tLCPolSchema.getAppntNo());
    tLJAPaySchema.setSumActuPayMoney(tBonusMoney);
    tLJAPaySchema.setPayDate(CurrentDate);
    tLJAPaySchema.setEnterAccDate(CurrentDate);
    tLJAPaySchema.setConfDate(CurrentDate);
    tLJAPaySchema.setApproveCode(tLCPolSchema.getApproveCode());
    tLJAPaySchema.setApproveDate(tLCPolSchema.getApproveDate());
    tLJAPaySchema.setSerialNo(tSNo);
    tLJAPaySchema.setOperator(tLCPolSchema.getOperator());
    tLJAPaySchema.setMakeDate(CurrentDate);
    tLJAPaySchema.setMakeTime(CurrentTime);
    tLJAPaySchema.setModifyDate(CurrentDate);
    tLJAPaySchema.setModifyTime(CurrentTime);
    tLJAPaySchema.setGetNoticeNo(tGetNoticeNo);
    tLJAPaySchema.setManageCom(tLCPolSchema.getManageCom());
    tLJAPaySchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJAPaySchema.setAgentType(tLCPolSchema.getAgentType());
    tLJAPaySchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    /*Lis5.3 upgrade get
             tLJAPaySchema.setBankCode(tLCPolSchema.getBankCode());      //银行编码
             tLJAPaySchema.setBankAccNo(tLCPolSchema.getBankAccNo());   //银行帐号
     */
    tLJAPaySchema.setRiskCode(tLCPolSchema.getRiskCode()); // 险种编码

    //添加实收个人子表纪录
    LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
    tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());
    tLJAPayPersonSchema.setPayCount(2); //-记为续期
    tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
    tLJAPayPersonSchema.setContNo(tLCPolSchema.getContNo());
    tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
    tLJAPayPersonSchema.setPayNo(tTempFeeNo);
    if (tLCPolSchema.getGrpPolNo().equals("00000000000000000000") &&
        tLCPolSchema.getContNo().equals("00000000000000000000")) {
      tLJAPayPersonSchema.setPayAimClass("1"); //交费目的分类
    }
    if (!tLCPolSchema.getGrpPolNo().equals("00000000000000000000") &&
        tLCPolSchema.getContNo().equals("00000000000000000000")) {
      tLJAPayPersonSchema.setPayAimClass("2"); //交费目的分类
    }
    tLJAPayPersonSchema.setDutyCode(tLCDutySchema.getDutyCode()); //责任编码
    tLJAPayPersonSchema.setPayPlanCode("00000000"); //交费计划编码
    tLJAPayPersonSchema.setSumActuPayMoney(tBonusMoney);
    tLJAPayPersonSchema.setSumDuePayMoney(tBonusMoney);
    tLJAPayPersonSchema.setPayIntv(0);
    tLJAPayPersonSchema.setPayDate(CurrentDate);
    tLJAPayPersonSchema.setPayType("ZC");
    tLJAPayPersonSchema.setEnterAccDate(CurrentDate);
    tLJAPayPersonSchema.setConfDate(CurrentDate);
    tLJAPayPersonSchema.setLastPayToDate(tLCDutySchema.getFirstPayDate()); //原交至日期
    tLJAPayPersonSchema.setCurPayToDate(tLCDutySchema.getPaytoDate()); //现交至日期
    tLJAPayPersonSchema.setInInsuAccState("0"); //转入保险帐户状态
    tLJAPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode()); //复核人编码
    tLJAPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate()); //复核日期
    tLJAPayPersonSchema.setSerialNo(tSNo);
    tLJAPayPersonSchema.setOperator(tLCPolSchema.getOperator());
    tLJAPayPersonSchema.setMakeDate(CurrentDate);
    tLJAPayPersonSchema.setMakeTime(CurrentTime);
    tLJAPayPersonSchema.setModifyDate(CurrentDate);
    tLJAPayPersonSchema.setModifyTime(CurrentTime);
    tLJAPayPersonSchema.setGetNoticeNo(tGetNoticeNo);
    tLJAPayPersonSchema.setInInsuAccState("0"); //转入保险帐户状态
    tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
    tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
    tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
    tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
    tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());

    //事务更新
    Connection conn = DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByRePay";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    try {
      conn.setAutoCommit(false);
      /**插入责任项表纪录*/
      LCDutyDB tempLCDutyDB = new LCDutyDB(conn);
      tempLCDutyDB.setSchema(tLCDutySchema);
      if (tempLCDutyDB.insert() == false) {
        this.mErrors.copyAllErrors(tempLCDutyDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByAddPrem";
        tError.errorMessage = "责任项表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /**插入保费项表纪录*/
      LCPremDB tempLCPremDB = new LCPremDB(conn);
      tempLCPremDB.setSchema(tLCPremSchema);
      if (tempLCPremDB.insert() == false) {
        this.mErrors.copyAllErrors(tempLCPremDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByAddPrem";
        tError.errorMessage = "保费项表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /**插入领取项表纪录*/
      LCGetDBSet tempLCGetDBSet = new LCGetDBSet(conn);
      tempLCGetDBSet.set(tSaveLCGetSet);
      if (tempLCGetDBSet.insert() == false) {
        this.mErrors.copyAllErrors(tempLCGetDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByAddPrem";
        tError.errorMessage = "领取项表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /** 实付总表 */
      System.out.println("Start 实付总表...");
      LJAGetDB tLJAGetDB = new LJAGetDB(conn);
      tLJAGetDB.setSchema(tLJAGetSchema);
      if (!tLJAGetDB.insert()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实付总表数据保存失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /** 实付子表 */
      System.out.println("Start 实付子表...");
      LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB(conn);
      tLJABonusGetDB.setSchema(tLJABonusGetSchema);
      if (!tLJABonusGetDB.insert()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实付子表数据保存失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /** 财务实付表 */
      System.out.println("Start 财务实付表...");
      LJFIGetDB tLJFIGetDB = new LJFIGetDB(conn);
      tLJFIGetDB.setSchema(tLJFIGetSchema);
      if (!tLJFIGetDB.insert()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实付表数据保存失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /**添加暂交费纪录*/
      LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB(conn);
      tLJTempFeeDB.setSchema(tLJTempFeeSchema);
      if (tLJTempFeeDB.insert() == false) {
        this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "暂交费主表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(conn);
      tLJTempFeeClassDB.setSchema(tLJTempFeeClassSchema);
      if (tLJTempFeeClassDB.insert() == false) {
        this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "暂交费子表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      LJAPayDB tLJAPayDB = new LJAPayDB(conn);
      tLJAPayDB.setSchema(tLJAPaySchema);
      if (tLJAPayDB.insert() == false) {
        this.mErrors.copyAllErrors(tLJAPayDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实收总表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB(conn);
      tLJAPayPersonDB.setSchema(tLJAPayPersonSchema);
      if (tLJAPayPersonDB.insert() == false) {
        this.mErrors.copyAllErrors(tLJAPayPersonDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "实收个人表数据添加失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      /**个人保单表更新**/
      ExeSQL tExeSQL = new ExeSQL(conn);
      if (!tExeSQL.execUpdateSQL(strSQL2)) {
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByAddPrem";
        tError.errorMessage = "个人保单表数据更新失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      String strSQL = "update LOBonusPol set AGetDate='" + CurrentDate +
          "' , BonusFlag='1'";
      strSQL = strSQL + " , ModifyDate='" + CurrentDate + "' , ModifyTime='" +
          CurrentTime + "'";
      strSQL = strSQL + " where PolNo='" + tLCPolSchema.getPolNo() +
          "' and FiscalYear=" + tBonusYear;

      /**保单红利表更新**/
      tExeSQL = new ExeSQL(conn);
      if (!tExeSQL.execUpdateSQL(strSQL)) {
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByCash";
        tError.errorMessage = "保单红利表数据更新失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      conn.commit();
      conn.close();
    }
    catch (Exception ex) {
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByAddPrem";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      try {
        conn.rollback();
      }
      catch (Exception e) {}
      return false;
    }

    return true;
  }

  private boolean updateLCPolBonusForZero(LCPolSchema tLCPolSchema,
                                          int tBonusYear) {

    Connection conn = DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByRePay";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    try {
      conn.setAutoCommit(false);
      String strSQL = "update LOBonusPol set AGetDate='" + CurrentDate +
          "' , BonusFlag='1'";
      strSQL = strSQL + " , ModifyDate='" + CurrentDate + "' , ModifyTime='" +
          CurrentTime + "'";
      strSQL = strSQL + " where PolNo='" + tLCPolSchema.getPolNo() +
          "' and FiscalYear=" + tBonusYear;
      /**保单红利表更新**/
      ExeSQL tExeSQL = new ExeSQL(conn);
      if (!tExeSQL.execUpdateSQL(strSQL)) {
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "AssignBonus";
        tError.functionName = "getByRePay";
        tError.errorMessage = "保单红利表数据更新失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }
    }
    catch (Exception ex) {
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "getByRePay";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      try {
        conn.rollback();
      }
      catch (Exception e) {}
      return false;
    }
    return true;
  }

  /**
   * 纪录错误信息
   * @param tSerialNo
   * @param tPolNo
   * @param errDescribe
   * @param tGetMode
   * @return
   */
  public boolean insertErrLog(String tSerialNo, String tPolNo,
                              String errDescribe, String tGetMode) {
    LOBonusAssignErrLogDB tLOBonusAssignErrLogDB = new LOBonusAssignErrLogDB();
    tLOBonusAssignErrLogDB.setSerialNo(tSerialNo);
    tLOBonusAssignErrLogDB.setPolNo(tPolNo);
    tLOBonusAssignErrLogDB.setGetMode(tGetMode);
    tLOBonusAssignErrLogDB.seterrMsg(errDescribe);
    tLOBonusAssignErrLogDB.setmakedate(PubFun.getCurrentDate());
    tLOBonusAssignErrLogDB.setmaketime(PubFun.getCurrentTime());
    if (tLOBonusAssignErrLogDB.insert() == false) {
      CError tError = new CError();
      tError.moduleName = "AssignBonus";
      tError.functionName = "insertErrLog";
      tError.errorMessage = "纪录错误日志时发生错误：" +
          tLOBonusAssignErrLogDB.mErrors.getFirstError() + "；解决该问题后，请再次分配当日红利";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 分配红利后插入打印数据
   * @param tLCPolSchema
   * @return
   */
  public LOPRTManagerSchema getPrintData(LCPolSchema tLCPolSchema) {
    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
    try {
      String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
      String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
      tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
      tLOPRTManagerSchema.setOtherNo(tLCPolSchema.getPolNo());
      tLOPRTManagerSchema.setOtherNoType("00");
      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_BONUSPAY);
      tLOPRTManagerSchema.setManageCom(tLCPolSchema.getManageCom());
      tLOPRTManagerSchema.setAgentCode(tLCPolSchema.getAgentCode());
      tLOPRTManagerSchema.setReqCom(tLCPolSchema.getManageCom());
      tLOPRTManagerSchema.setReqOperator(tLCPolSchema.getOperator());
      tLOPRTManagerSchema.setPrtType("0");
      tLOPRTManagerSchema.setStateFlag("0");
      tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
      tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
    }
    catch (Exception ex) {
      return null;
    }

    return tLOPRTManagerSchema;
  }
}
