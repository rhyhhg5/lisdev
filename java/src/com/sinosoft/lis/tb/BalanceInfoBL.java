/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCProjectInfoDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCGrpContSubSchema;
import com.sinosoft.lis.schema.LCProjectInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class BalanceInfoBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	//录入
	private LCGrpContSubSchema mLCGrpContSubSchema = new LCGrpContSubSchema();
	private LCProjectInfoSchema mLCProjectInfoSchema = new LCProjectInfoSchema();

	private MMap mMap = new MMap();

	public BalanceInfoBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BalanceInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败BalanceInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start BalanceInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BalanceInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	////////////////////////////////////////////////
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mLCGrpContSubSchema = (LCGrpContSubSchema) cInputData.getObjectByObjectName("LCGrpContSubSchema", 0);
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		if(mLCGrpContSubSchema.getPrtNo() == null || "".equals(mLCGrpContSubSchema.getPrtNo())){
			CError tError = new CError();
			tError.moduleName = "BalanceInfoBL";
			tError.functionName = "dealData";
			tError.errorMessage = "未获取到需要处理的保单！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(mLCGrpContSubSchema.getProjectName() == null || "".equals(mLCGrpContSubSchema.getProjectName())){
			CError tError = new CError();
			tError.moduleName = "BalanceInfoBL";
			tError.functionName = "dealData";
			tError.errorMessage = "未获取到需要处理的项目名称！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	private boolean dealData() {
		
		if("INSERT||MAIN".equals(mOperate)){
			String tManageCom = mGlobalInput.ManageCom;
			if(tManageCom == null || "".equals(tManageCom) || tManageCom.length() != 8){
				String tSQL = "select managecom from lcgrpcont where prtno = '"+mLCGrpContSubSchema.getPrtNo()+"' ";
				tManageCom = new ExeSQL().getOneValue(tSQL);
			}
			if(tManageCom == null || "".equals(tManageCom) || tManageCom.length() != 8){
				CError tError = new CError();
				tError.moduleName = "BalanceInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "保单管理机构非8位机构，无法增加社保项目要素信息！";
				this.mErrors.addOneError(tError);
				return false;
			}
			String tProjectNo = getProjectNo(tManageCom,mLCGrpContSubSchema.getProjectName());
			if(tProjectNo == null || "".equals(tProjectNo)){
				CError tError = new CError();
				tError.moduleName = "BalanceInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "根据管理机构，生成项目编码失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			mLCGrpContSubSchema.setProjectNo(tProjectNo);
			mLCGrpContSubSchema.setManageCom(tManageCom);
			mLCGrpContSubSchema.setOperator(mGlobalInput.Operator);
			mLCGrpContSubSchema.setMakeDate(PubFun.getCurrentDate());
			mLCGrpContSubSchema.setMakeTime(PubFun.getCurrentTime());
			mLCGrpContSubSchema.setModifyDate(PubFun.getCurrentDate());
			mLCGrpContSubSchema.setModifyTime(PubFun.getCurrentTime());
			mMap.put(this.mLCGrpContSubSchema, SysConst.DELETE_AND_INSERT);
			//add by zjd 项目制统一 同步到项目制表中
			LCProjectInfoDB tLCProjectInfo=new LCProjectInfoDB();
			tLCProjectInfo.setProjectNo(tProjectNo);
			if(!tLCProjectInfo.getInfo()){
				mLCProjectInfoSchema.setProjectNo(tProjectNo);
				mLCProjectInfoSchema.setProjectName(mLCGrpContSubSchema.getProjectName());
				mLCProjectInfoSchema.setManageCom(mLCGrpContSubSchema.getManageCom());
				mLCProjectInfoSchema.setState("01");
				mLCProjectInfoSchema.setCreateDate(PubFun.getCurrentDate());
				mLCProjectInfoSchema.setCreateTime(PubFun.getCurrentTime());
				mLCProjectInfoSchema.setOperator(mGlobalInput.Operator);
				mLCProjectInfoSchema.setMakeDate(PubFun.getCurrentDate());
				mLCProjectInfoSchema.setMakeTime(PubFun.getCurrentTime());
				mLCProjectInfoSchema.setModifyDate(PubFun.getCurrentDate());
				mLCProjectInfoSchema.setModifyTime(PubFun.getCurrentTime());
				mMap.put(this.mLCProjectInfoSchema, SysConst.DELETE_AND_INSERT);	
			}else{
				mLCProjectInfoSchema=tLCProjectInfo.getSchema();
				mLCProjectInfoSchema.setProjectName(mLCGrpContSubSchema.getProjectName());
				mMap.put(this.mLCProjectInfoSchema, SysConst.UPDATE);	
			}
		} else	if (mOperate.equals("DELETE||MAIN")){
			mMap.put(this.mLCGrpContSubSchema, SysConst.DELETE);
		}else if("UPDATE||MAIN".equals(mOperate)){
			
		}
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BalanceInfoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}
	
	private String getProjectNo(String aManageCom,String aProjectName){
		String tProjectNo = "";
		String tSQL = "select projectno from lcgrpcontsub where projectname = '"+aProjectName+"' union select  lcp.projectno from LCProjectInfo lcp where lcp.projectname = '"+aProjectName+"' ";
		tProjectNo = new ExeSQL().getOneValue(tSQL);
		if("".equals(tProjectNo)){
			String tMaxProjectNoSQL = "select max(a.projectno) from (select projectno from lcgrpcontsub where managecom = '"+aManageCom+"' and projectno like '"+aManageCom+"%' union  select projectno from LCProjectInfo lcp where lcp.managecom = '"+aManageCom+"' and lcp.projectno like '"+aManageCom+"%') a "; 
			String tMaxProjectNo = new ExeSQL().getOneValue(tMaxProjectNoSQL);
			if("".equals(tMaxProjectNo)){
				tProjectNo = aManageCom + "00" + "0001";
			}else{
				String tOldProjectSQL = "select projectname,projectno from lcgrpcontsub where prtno = '"+mLCGrpContSubSchema.getPrtNo()+"' ";
				SSRS tOldSSRS = new ExeSQL().execSQL(tOldProjectSQL);
				if(tOldSSRS != null && tOldSSRS.MaxRow >= 1){
					String tOldProjectName = tOldSSRS.GetText(1, 1); 
					String tOldProjectNo = tOldSSRS.GetText(1, 2);
					String tOtherSQL = "select 1 from lcgrpcontsub where projectname = '"+tOldProjectName+"' and prtno != '"+mLCGrpContSubSchema.getPrtNo()+"' ";
					String tOtherFlag = new ExeSQL().getOneValue(tOtherSQL);
					if("1".equals(tOtherFlag)){
						tProjectNo = String.valueOf(Long.parseLong(tMaxProjectNo)+1);
					}else{
						tProjectNo = tOldProjectNo;
					}
				}else{
					tProjectNo = String.valueOf(Long.parseLong(tMaxProjectNo)+1);
				}
			}
		}
		return tProjectNo;
	}

}
