package com.sinosoft.lis.tb;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.tb.TaxApplyBL;

public class TaxApplyUI {
	/** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    public TaxApplyUI(){}
    
    public boolean submitData(VData cInputData, String cOperate){
    	this.mOperate = cOperate;
    	this.mInputData = (VData)cInputData.clone();
    	TaxApplyBL tTaxApplyBL = new TaxApplyBL();
    	try{
    		if(!tTaxApplyBL.submitData(mInputData,mOperate)){
    			this.mErrors.copyAllErrors(tTaxApplyBL.mErrors);
    			CError tError = new CError();
    			tError.moduleName = "TaxApplyUI";
    			tError.functionName = "submitData";
    			tError.errorMessage = "BL类处理失败！";
    			this.mErrors.addOneError(tError);
    			return false;
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    		return false;
    	}
    	
    	mResult = tTaxApplyBL.getResult();
    	return true;
    }
    
    public VData getResult(){
        return mResult;
    }
}
