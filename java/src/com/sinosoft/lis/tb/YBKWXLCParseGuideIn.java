/*
 * @(#)LCParseGuideIn.java	2005-01-05
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.tb;

import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.BPOBatchInfoDB;
import com.sinosoft.lis.db.BPOMissionStateDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpImportLogDB;
import com.sinosoft.lis.db.LDImpartDB;
import com.sinosoft.lis.db.LDOccupationDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.BPOMissionDetailStateSchema;
import com.sinosoft.lis.schema.BPOMissionStateSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCCustomerImpartDetailSchema;
import com.sinosoft.lis.schema.LCCustomerImpartSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vbl.LCBnfBLSet;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vschema.BPOMissionStateSet;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCCustomerImpartDetailSet;
import com.sinosoft.lis.vschema.LCCustomerImpartParamsSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LCInsuredRelatedSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLPathTool;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:LCParseGuideIn </p>
 * <p>Description: 个单磁盘投保入口类文件 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-01-05
 */

public class YBKWXLCParseGuideIn
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public CErrors logErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /**内存文件暂存*/
    private org.jdom.Element myElement;

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String FileName;

    private String XmlFileName;

    private String mPreFilePath;

   // private String FilePath = "E:/temp/";
    private String FilePath = "";

    private String ParseRootPath = "/DATASET/BATCHID";

    private String ParseRootPathBPO = "/DATASET/BPOBATCHNO";

    private String ParsePath = "/DATASET/CONTTABLE/ROW";

    //配置文件Xml节点描述
    private String ImportFileName;

    private String ConfigFileName;

    private String mBatchNo = "";

    private String mBPOBatchNo = null;

    private String mPrtNo = "";

    private String mContID = null;

    private String mContNo = "";

    private String mManageCom = "";

    private String mAppntNo = "";

    private String mAppntName = "";

    private String mAgentCode = "";

    private String mImportDate = "";

    private LCPolBL mainPolBL = new LCPolBL();

    private org.w3c.dom.Document m_doc = null;

    private LCPolImpInfo m_LCPolImpInfo = new LCPolImpInfo();

    private String[] m_strDataFiles = null;
    
    private String[] mPrtNoArr;

    //个单合同的集体合同号码默认值
    private static final String STATIC_GRPCONTNO = "00000000000000000000";

    //个单合同的总单类型默认值
    private static final String STATIC_CONTTYPE = "1";

	private static final String STATIC_PROCESSID = "0000000003";

    //统计同一合同下被保人序列号
    private int iSequenceNo = 1;

    /** 险种序号 */
    private int mRiskSeqNo = 1;

    private boolean ShowSchedule = false;

    /** 单个保单最小缴费频次 */
    private int mPayIntv = 0;

    boolean mIsBPO = false;

    private Reflections ref = new Reflections();

    private MMap mBPOMap = new MMap(); //只保存外包保单数据

    private HashSet mNoDealContIDSet = new HashSet(); //不需要处理的保单的ContID

    private String mBPOMissionState = null; //外包录入状态

    private String mSubType = null; //投保书扫描件类型
    
	private static final String STATIC_ActivityID = "0000001001";

	private static final String STATIC_EActivityID = "0000001022";

	private String mMissionid = "";

    public YBKWXLCParseGuideIn()
    {
        bulidDocument();
    }

    public YBKWXLCParseGuideIn(String fileName)
    {
        bulidDocument();
        FileName = fileName;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        getInputData();
        if (!checkData())
        {
            return false;
        }
        System.out.println("开始时间:" + PubFun.getCurrentTime());
        try
        {
                if (!ParseXml())
                {
                    CError tError = new CError();
                    tError.moduleName = "LCParseGuideIn";
                    tError.functionName = "checkData";
                    tError.errorMessage = this.mErrors.getFirstError();
                    logErrors.addOneError(tError);
                    return false;
                }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "导入文件格式有误!";
            logErrors.addOneError(tError);
        }

        mErrors = logErrors;
        System.out.println("结束时间:" + PubFun.getCurrentTime());
        if (mErrors.getErrorCount() > 0)
        {
            return false;
        }

        // 重算豁免险，郁闷。。。。。。。。。。无语。。。没办法了
        
        for(int row = 0; row < mPrtNoArr.length; row++){
        	LCPolSchema tLCPolSchema = new LCPolSchema();
           	tLCPolSchema.setPrtNo(mPrtNoArr[row]);
           
           	VData exemptionVD = new VData();
    		exemptionVD.addElement(mGlobalInput);
    		exemptionVD.addElement(tLCPolSchema);
    		ExemptionRiskBL tExemptionRiskBL = new ExemptionRiskBL();
    		// 豁免险处理
    		if(!tExemptionRiskBL.submitData(exemptionVD, "")){
    		}
        }
        return true;
    }

    /**
     * 得到传入数据
     */
    private void getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        mPreFilePath = (String) mTransferData.getValueByName("FilePath");
        System.out.println(mPreFilePath);
    }

    /**
     * 校验传输数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无操作员信息，请重新登录!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无导入文件信息，请重新导入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FileName = (String) mTransferData.getValueByName("FileName");
            System.out.println("FileName" + FileName);
        }
        return true;
    }

    /**
     * 得到生成文件路径
     *
     * @return boolean
     */
    private boolean getFilePath()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("XmlPath");
        if (!tLDSysVarDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "缺少文件导入路径!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FilePath = tLDSysVarDB.getSysVarValue();
            //TODO 测试用
            FilePath="D:\\YBK";
            
        }

        return true;
    }

    /**
     * 检验文件是否存在
     *
     * @return boolean
     */
    private boolean checkXmlFileName()
    {
        File tFile = new File(mPreFilePath);
        if (!tFile.exists())
        {
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("XmlPath");
            if (!tLDSysVarDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "缺少文件导入路径!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                FilePath = tLDSysVarDB.getSysVarValue();
            }

            File tFile1 = new File(FilePath);
            if (!tFile1.exists())
            {
                tFile1.mkdirs();
            }
            XmlFileName = FilePath + XmlFileName;
            File tFile2 = new File(XmlFileName);
            if (!tFile2.exists())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "请上传相应的数据文件到指定路径" + FilePath + "!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        //得到批次号
        XMLPathTool tXPT = new XMLPathTool(mPreFilePath);
        if (!getBatchNo(tXPT))
        {
            return false;
        }

        return true;
    }

    /**
     * 检查导入配置文件是否存在
     *
     * @return boolean
     */
    private boolean checkImportConfig()
    {
        this.getFilePath();

        String filePath = mPreFilePath + FilePath;
        //String filePath = "E:/v.1.1/ui/temp/";
        File tFile1 = new File(filePath);
        if (!tFile1.exists())
        {
            //初始化创建目录
            tFile1.mkdirs();
        }

        ConfigFileName = filePath + "LCImport.xml";
        File tFile2 = new File(ConfigFileName);
        if (!tFile2.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "请上传配置文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 初始化上传文件
     *
     * @return boolean
     */
    private boolean initImportFile()
    {
        this.getFilePath();
        ImportFileName = mPreFilePath + FilePath + FileName;
        //ImportFileName = "E:/v.1.1/ui/temp/" + FileName;

        File tFile = new File(ImportFileName);
        if (!tFile.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "未上传文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-----导入文件");
        return true;
    }
    /**
     * 提交第一层内的循环体到第一层循环内的指定结点下
     * ???目前不知道有啥用 -wujs
     * @param aVT Vector
     * @param aElt Element
     */
    public void AddMuliElement(Vector aVT, org.jdom.Element aElt)
    {
        int NodeCount = aVT.size();
        //org.jdom.Element tElt1 = (org.jdom.Element)myElement.clone();
        org.jdom.Element tElt = myElement;
        int i = 0;
        while (NodeCount > 0)
        {
            i++;
            String NodeName = (String) aVT.get(i - 1);
            tElt = (org.jdom.Element) tElt.getChildren((String) aVT.get(i - 1))
                    .get(tElt.getChildren((String) aVT.get(i - 1)).size() - 1);
            NodeCount--;
        }
        tElt.addContent(aElt);
    }

    public boolean parseBPOXml(String cXmlFileName, GlobalInput cGI)
    {
        return parseBPOXml(cXmlFileName, cGI, true);
    }

    /**
     * 解析外包录入提供的xml
     * @return boolean
     */
    public boolean parseBPOXml(String cXmlFileName, GlobalInput cGI,
            boolean tIsSaveBpo)
    {
        this.mGlobalInput = cGI;

        String[] files = { cXmlFileName };
        m_strDataFiles = files;

        XmlFileName = cXmlFileName;
        mIsBPO = true;

        if (!checkXmlFileName())
        {
            return false;
        }

        if (tIsSaveBpo)
        {
            //若是外包调用，则先存储xml信息
            XMLPathTool tXPT = new XMLPathTool(XmlFileName);
            NodeList nodeList = tXPT.parseN(ParsePath);
            if (!this.saveBPOData(nodeList))
            {
                return false;
            }
        }

        //解析xml文件
        boolean succFlag = ParseXml();

        return succFlag;
    }

    /**
     * 存储外包返回的xml数据
     * @return boolean
     */
    private boolean saveBPOData(NodeList nodeList)
    {
        if (!mIsBPO)
        {
            return true;
        }

        BPOSchemaCreator tBPOCreateSchema = new BPOSchemaCreator();

        //循环每个保单
        for (int i = 0; i < nodeList.getLength(); i++)
        {
            boolean hasAppnt = true;
            boolean hasInsured = true;
            boolean hasPol = true;

            MMap tOneContMMap = new MMap(); //一个保单的节点信息

            Node node = nodeList.item(i);

            try
            {
                node = transformNode(node);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                continue;
            }

            NodeList contNodeList = node.getChildNodes();
            if (contNodeList.getLength() <= 0)
            {
                continue;
            }
            Node contNode = null;
            String nodeName = "";

            String tContID = "";
            String tPrtNo = "";

            //循环保单的每个节点
            for (int j = 0; j < contNodeList.getLength(); j++)
            {
                contNode = contNodeList.item(j);
                nodeName = contNode.getNodeName();

                if (nodeName.equals("#text") || nodeName.equals("#comment"))
                {
                    continue;
                }

                if (nodeName.equals("CONTID"))
                {
                    tContID = contNode.getFirstChild().getNodeValue();
                    System.out.println(PubFun.getCurrentDate() + " "
                            + PubFun.getCurrentTime()
                            + ":saveBPOData生成Schema，解析保单Contid:" + tContID);
                    continue;
                }
                if (nodeName.equals("PrtNo"))
                {
                    tPrtNo = contNode.getFirstChild().getNodeValue();
                    System.out.println(PubFun.getCurrentDate() + " "
                            + PubFun.getCurrentTime()
                            + ":saveBPOData生成Schema，解析保单PrtNo:" + tPrtNo);
                    continue;
                }

                if (nodeName.equals("APPNTTABLE"))
                {
                    SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                            "BPOLCAppnt", mBPOBatchNo, tPrtNo,
                            mGlobalInput.Operator);
                    if (set != null && set.size() == 0)
                    {
                        hasAppnt = false;
                    }
                    for (int count = 1; count <= set.size(); count++)
                    {
                        tOneContMMap.put(set.getObj(count),
                                SysConst.DELETE_AND_INSERT);
                    }
                    continue;
                }

                if (nodeName.equals("INSUREDTABLE"))
                {
                    //解析被保险人XML
                    SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                            "BPOLCInsured", mBPOBatchNo, tPrtNo,
                            mGlobalInput.Operator);
                    if (set != null && set.size() == 0)
                    {
                        hasAppnt = false;
                    }
                    for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(set.getObj(m),
                                SysConst.DELETE_AND_INSERT);
                    }

                    continue;
                }
                if (nodeName.equals("LCPOLTABLE"))
                {
                    //解析一个险种保单节点
                    SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                            "BPOLCPol", mBPOBatchNo, tPrtNo,
                            mGlobalInput.Operator);
                    if (set != null && set.size() == 0)
                    {
                        hasAppnt = false;
                    }
                    for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(set.getObj(m),
                                SysConst.DELETE_AND_INSERT);
                    }

                    continue;
                }
                if (nodeName.equals("BNFTABLE"))
                {
                    //解析受益人XML
                    SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                            "BPOLCBnf", mBPOBatchNo, tPrtNo,
                            mGlobalInput.Operator);
                    for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(set.getObj(m),
                                SysConst.DELETE_AND_INSERT);
                    }

                    continue;
                }
                if (nodeName.equals("IMPARTTABLE"))
                {
                    //解析客户告知XML
                    SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                            "BPOLCImpart", mBPOBatchNo, tPrtNo,
                            mGlobalInput.Operator);
                    for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(set.getObj(m),
                                SysConst.DELETE_AND_INSERT);
                    }

                    continue;
                }

                if (nodeName.equals("ISSUETABLE"))
                {
                    SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                            "BPOIssue", mBPOBatchNo, tPrtNo,
                            mGlobalInput.Operator);
                    for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(set.getObj(m),
                                SysConst.DELETE_AND_INSERT);
                    }

                    continue;
                }
                if (nodeName.equals("NATIONTABLE"))
                {
                    SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                            "BPOLCNation", mBPOBatchNo, tPrtNo,
                            mGlobalInput.Operator);
                    for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(set.getObj(m),
                                SysConst.DELETE_AND_INSERT);
                    }

                    continue;
                }
                if (nodeName.equals("RISKDUTYWRAP"))
                {
                    SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                            "BPOLCRiskDutyWrap", mBPOBatchNo, tPrtNo,
                            mGlobalInput.Operator);
                    for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(set.getObj(m),
                                SysConst.DELETE_AND_INSERT);
                    }

                    continue;
                }
                
                // 为了综合开拓新加的 郁闷了 需求的时候没看这块的程序。。只能单独处理了
                // 这块实在不想这么写
                // 张成轩
                if (nodeName.equals("ASSISTTABLE"))
                {
                	BPOAssistSchemaDeal tBPOAssistSchema = new BPOAssistSchemaDeal();
                	SchemaSet set = tBPOAssistSchema.setSchema(contNode, tPrtNo, mGlobalInput.Operator);
                	SchemaSet deleteSet = tBPOAssistSchema.getSet(tPrtNo);
                	for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(deleteSet.getObj(m), SysConst.DELETE);
                    }
                    for (int m = 1; set != null && m <= set.size(); m++)
                    {
                        tOneContMMap.put(set.getObj(m), SysConst.INSERT);
                    }
                    continue;
                }
            }
            mBPOMap.add(tOneContMMap);

            if (tBPOCreateSchema.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tBPOCreateSchema.mErrors);

                tBPOCreateSchema.mErrors.clearErrors();
                dealContError(tContID, hasAppnt, hasInsured, hasPol);
                mNoDealContIDSet.add(tContID); //发生错误的保单不需要处理
            }
        }

        BPOBatchInfoDB tBPOBatchInfoDB = new BPOBatchInfoDB();
        tBPOBatchInfoDB.setBatchNo(mBPOBatchNo);
        if (!tBPOBatchInfoDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "saveBPOData";
            tError.errorMessage = "查询外包批次信息出错:" + mBPOBatchNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        tBPOBatchInfoDB.setState(BPO.BATCH_STATE_IMPORT);
        tBPOBatchInfoDB.setDocNumberBack(nodeList.getLength());
        tBPOBatchInfoDB.setOperator(mGlobalInput.Operator);
        tBPOBatchInfoDB.setReceiveDate(PubFun.getCurrentDate());
        tBPOBatchInfoDB.setReceiveTime(PubFun.getCurrentTime());
        tBPOBatchInfoDB.setModifyDate(PubFun.getCurrentDate());
        tBPOBatchInfoDB.setModifyTime(PubFun.getCurrentTime());
        mBPOMap.put(tBPOBatchInfoDB.getSchema(), SysConst.UPDATE);

        VData data = new VData();
        data.add(this.mBPOMap);
        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "parseBPOXml";
            tError.errorMessage = "外包批次号" + mBPOBatchNo + "xml数据保存失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        mBPOMap = new MMap();

        return true;
    }

    private void dealContError(String tContID, boolean hasAppnt,
            boolean hasInsured, boolean hasPol)
    {
        if (!hasAppnt)
        {
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "saveBPOData";
            tError.errorMessage = "xml文件中合同ID" + tContID + "没有投保人信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
        }
        if (!hasInsured)
        {
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "saveBPOData";
            tError.errorMessage = "xml文件中合同ID" + tContID + "没有被保人信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
        }
        if (!hasPol)
        {
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "saveBPOData";
            tError.errorMessage = "xml文件中合同ID" + tContID + "没有险种信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
        }

        m_LCPolImpInfo.logError(mBatchNo, tContID, "", "", "", null,
                this.mErrors, mGlobalInput, mPrtNo);
        mErrors.clearErrors();
    }

    /**
     * 解析xml
     *
     * @return boolean
     */
    private boolean ParseXml()
    {
        if (!checkXmlFileName())
        {
            return false;
        }
        this.mErrors.clearErrors();

        //得到保单的传入信息
        XMLPathTool tXPT = new XMLPathTool(mPreFilePath);
        NodeList nodeList = tXPT.parseN(ParsePath);

        //循环处理每个保单节点，生成保单数据
        try
        {
            if (!createCont(nodeList))
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("生成保单信息失败");
        }

        if (this.mErrors.needDealError())
        {
            System.out.println(this.mErrors.getErrorCount() + "error:"
                    + this.mErrors.getFirstError());
        }

        return true;
    }

    /**
     * updateNoBPODataState
     */
    private void updateNoBPODataState()
    {
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            return;
        }

        String sql = "update BPOMissionState set State = '"
                + BPO.MISSION_STATE_NO_DATA + "' " + "where BPOBatchNo = '"
                + mBatchNo + "' " + "   and State = '"
                + BPO.MISSION_STATE_SEND_SUCC + "' ";
        new ExeSQL().execUpdateSQL(sql);
        
        String essql="update ES_DOC_MAIN esd set state='04' where "
        	    + "exists (select 1 from BPOMissionState where BPOBatchNo = '"
                + mBatchNo + "' and state= '"
                + BPO.MISSION_STATE_NO_DATA + "' and bussno=esd.DocCode )";
        new ExeSQL().execUpdateSQL(essql);
    }

    /**
     * getBatchNo
     * 得到批次信息BatchNo、BPOBatchNo
     * @return boolean
     */
    private boolean getBatchNo(XMLPathTool tXPT)
    {
        try
        {
            //批次号
            mBatchNo = tXPT.parseX(ParseRootPath).getFirstChild()
                    .getNodeValue();

            Node node = tXPT.parseX(ParseRootPathBPO);
            if (node != null)
            {
                mBPOBatchNo = node.getFirstChild().getNodeValue();
            }

            if (mBatchNo == null || mBatchNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LCParseGuideIn";
                tError.functionName = "ParseXml";
                tError.errorMessage = "没有获得批次号";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            mBPOBatchNo = mBPOBatchNo == null ? mBatchNo : mBPOBatchNo;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "getBatchNo";
            tError.errorMessage = "获取批次号信息出现异常。";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * createCont
     * 循环处理每个保单节点，生成保单数据
     * @return boolean
     */
    private boolean createCont(NodeList nodeList)
    {
        LCPolParserYBKWX tLCPolParser = new LCPolParserYBKWX();
        mPrtNoArr = new String[nodeList.getLength()];
        //循环每个保单
        for (int i = 0; i < nodeList.getLength(); i++)
        {
            mPrtNo = "";
            mBPOMissionState = BPO.MISSION_STATE_DATA_ERR; //数据错误

            Node node = nodeList.item(i);

            String tIssueExists = "0"; //存在外包问题件标记

            try
            {
                node = transformNode(node);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                node = null;
            }

            NodeList contNodeList = node.getChildNodes();
            if (contNodeList.getLength() <= 0)
            {
                continue;
            }

            TransferData tContData;
            //解析保单数据
            try
            {
                tContData = dealOneCont(tLCPolParser, contNodeList);

                    LCContSchema tLCContSchema = (LCContSchema) tContData
                            .getValueByName("tLCContSchema"); //合同
                    VData tAppntVData = (VData) tContData
                            .getValueByName("tAppntVData"); //投保人
                    VData tInsuredVData = (VData) tContData
                            .getValueByName("tInsuredVData"); //被保人
                    LCBnfSet tLCBnfSet = (LCBnfSet) tContData
                            .getValueByName("tLCBnfSet"); //受益人
                    LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = (LCCustomerImpartDetailSet) tContData
                            .getValueByName("tLCCustomerImpartDetailSet"); //客户告知

                    //将客户身份证号码中的x转换成大写（被保险人） 2009-02-05 liuyp
                    LCInsuredSet tLCInsuredSet = (LCInsuredSet) tInsuredVData
                            .getObjectByObjectName("LCInsuredSet", 0);

                    if (tLCInsuredSet != null && tLCInsuredSet.size() != 0)
                    {
                        for (int j = 1; j <= tLCInsuredSet.size(); j++)
                        {
                            if (tLCInsuredSet.get(j).getIDType() != null
                                    && tLCInsuredSet.get(j).getIDNo() != null)
                            {
                                if (tLCInsuredSet.get(j).getIDType()
                                        .equals("0"))
                                {
                                    String tLCInsuredIdNo = tLCInsuredSet
                                            .get(j).getIDNo().toUpperCase();
                                    tLCInsuredSet.get(j)
                                            .setIDNo(tLCInsuredIdNo);
                                }
                            }

                        }

                    }

                    LCAppntSet tLCAppntSet = (LCAppntSet) tAppntVData
                            .getObjectByObjectName("LCAppntSet", 0);

                    //将客户身份证号码中的x转换成大写（投保人） 2009-02-05 liuyp
                    if (tLCAppntSet.get(1).getIDType() != null
                            && tLCAppntSet.get(1).getIDNo() != null)
                    {
                        if (tLCAppntSet.get(1).getIDType().equals("0"))
                        {
                            String tLCAppntIdNo = tLCAppntSet.get(1).getIDNo()
                                    .toUpperCase();
                            tLCAppntSet.get(1).setIDNo(tLCAppntIdNo);
                        }
                    }

                    boolean tInsuredCheck = true;
                    if (tInsuredVData == null
                            && !mNoDealContIDSet.contains(mContID))
                    {
                        CError.buildErr(this, tLCPolParser.mErrors
                                .getFirstError());
                        m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "",
                                null, mErrors, mGlobalInput, mPrtNo);
                        this.logErrors.copyAllErrors(tLCPolParser.mErrors);
                        this.mErrors.clearErrors();
                        tInsuredCheck = false;
                    }
                    if (tInsuredCheck && !mNoDealContIDSet.contains(mContID))
                    {
                        if (!m_LCPolImpInfo.init(mBatchNo, this.mGlobalInput,
                                tLCContSchema, tInsuredVData, tAppntVData,
                                tLCBnfSet, tLCCustomerImpartDetailSet))
                        {

                            m_LCPolImpInfo.logError(mBatchNo, mContID, "", "",
                                    "", null, this.mErrors, mGlobalInput,
                                    mPrtNo);
                            this.mErrors.clearErrors();
                            tInsuredCheck = false;
                        }
                    }

                    if (tInsuredCheck && !mNoDealContIDSet.contains(mContID))
                    {
                        //按合同导入
                        boolean bl = DiskContImport(mContID);

                        if (!bl)
                        {
                            System.out.println("导入失败");
                        }
                        else
                        {
                            System.out.println("导入成功");
                            mBPOMissionState = BPO.MISSION_STATE_CREATE_CONT; //生成保单
                            mResult.add(mPrtNo);
                            //TODO ??????????????????????????????????????????
                        }
                    }

                /*if (BPO.MISSION_STATE_CREATE_CONT.equals(mBPOMissionState))
                {
                    //工作流跳转到新单复核
                    if (inputConfirm(mBPOBatchNo, mPrtNo))
                    {
                        mBPOMissionState = BPO.MISSION_STATE_INPUT_CONT; //已录入
                    }
                }*/
            }
            catch (NumberFormatException ex)
            {
                CError.buildErr(this, "数据类型转换失败:" + ex.getMessage());
                ex.printStackTrace();
                m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "", null,
                        mErrors, mGlobalInput, mPrtNo);
                this.logErrors.copyAllErrors(tLCPolParser.mErrors);
                this.mErrors.clearErrors();
            }
            catch (Exception ex)
            {
                CError.buildErr(this, "未知异常导致数据导入失败：" + ex.getMessage());
                ex.printStackTrace();
                m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "", null,
                        mErrors, mGlobalInput, mPrtNo);
                this.logErrors.copyAllErrors(tLCPolParser.mErrors);
                this.mErrors.clearErrors();
            }

            //dealBPOState(mBPOMissionState, tIssueExists); //生成日志
            
            // 用于豁免险重算
            mPrtNoArr[i] = mPrtNo;
        }

        return true;
    }

    /**
     * getSubType
     *
     * @return String
     */
    private String getSubType(String aPrtNo)
    {
        String sql = "select SubType " + "from ES_Doc_Main "
                + "where DocCode = '" + aPrtNo + "' ";
        String subType = new ExeSQL().getOneValue(sql);

        return subType;
    }

    /**
     * 生成日志
     * 若批次号mBatchNo已导入，则报错
     * @param nodeList NodeList：保单节点
     * @param tXPT XMLPathTool
     * @return boolean
     */
    private boolean dealImportLog(NodeList nodeList, XMLPathTool tXPT)
    {
        LCGrpImportLogDB dLCGrpImportLogDB = new LCGrpImportLogDB();
        LCGrpImportLogSet dLCGrpImportLogSet = new LCGrpImportLogSet();
        dLCGrpImportLogDB.setBatchNo(mBatchNo);
        //        dLCGrpImportLogDB.setBatchNo("X");
        dLCGrpImportLogSet = dLCGrpImportLogDB.query();
        if (dLCGrpImportLogSet.size() > 0)
        {
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "该文件已经导入过，请更改文件名后再导入！";
            this.mErrors.addOneError(tError);
            return false;
        }

        int nNodeCount = 0;

        if (nodeList != null)
        {
            nNodeCount = nodeList.getLength();
        }

        //循环中，每一次都调用一次承包后台程序，首先挑出一条保单纪录，
        //再通过ID号找到保费项，责任，给付等，组合成Set集合
        //此时新单是没有投保单号的，因此在保单，保费项，
        //责任等纪录中投保单号为空,后台自动生成投保单号
        //最后将数据放入VData中，传给GrpPollmport模块。一次循环完毕
        /**
         * yangming:为显示进度条要将投保文件的合同数存储在日志表中
         */
        if (!ShowSchedule)
        {
            MMap map = new MMap();
            /**
             * 第一个sql语句用记录xml文件中的合同数,ContID=C代表合同数
             * 第二个sql语句用于记录磁盘文件解析成为几个xml文件,ContID=X代表Xml数
             */
            String strSql1 = "insert into LCGrpImportLog (GRPCONTNO,BATCHNO,CONTID,ERRORINFO,OPERATOR,MAKEDATE,MAKETIME)"
                    + " values ('00000000000000000000','"
                    + mBatchNo
                    + "','C','"
                    + nNodeCount
                    + "','"
                    + mGlobalInput.Operator
                    + "','"
                    + PubFun.getCurrentDate()
                    + "','"
                    + PubFun.getCurrentTime() + "' )";
            String strSql2 = "insert into LCGrpImportLog (GRPCONTNO,BATCHNO,CONTID,ERRORINFO,OPERATOR,MAKEDATE,MAKETIME)"
                    + " values ('00000000000000000000','"
                    + mBatchNo
                    + "','X','"
                    + m_strDataFiles.length
                    + "','"
                    + mGlobalInput.Operator
                    + "','"
                    + PubFun.getCurrentDate()
                    + "','" + PubFun.getCurrentTime() + "' )";
            String delSql1 = "delete from LCGrpImportLog where BatchNo='"
                    + mBatchNo + "' and contid='X'";
            String delSql2 = "delete from LCGrpImportLog where BatchNo='"
                    + mBatchNo + "' and contid='C'";
            map.put(delSql1, "DELETE");
            map.put(delSql2, "DELETE");
            map.put(strSql1, "INSERT");
            map.put(strSql2, "INSERT");
            VData tempVData = new VData();
            tempVData.add(map);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(tempVData, "INSERT"))
            {
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "在准备反显进度条数据时发生错误！";
                this.mErrors.addOneError(tError);
                return false;
            }
            ShowSchedule = true;
        }

        return true;
    }

    /**
     * dealOneCont
     */
    private TransferData dealOneCont(LCPolParserYBKWX tLCPolParser,
            NodeList contNodeList)
    {
        TransferData tTransferData = new TransferData();

        Node contNode = null;
        String nodeName = "";

        BPOSchemaCreator tBPOCreateSchema = new BPOSchemaCreator();

        for (int j = 0; j < contNodeList.getLength(); j++)
        {
            contNode = contNodeList.item(j);
            nodeName = contNode.getNodeName();
            if (nodeName.equals("#text"))
            {
                continue;
            }
            if (nodeName.equals("CONTID"))
            {
                mContID = contNode.getFirstChild().getNodeValue();
                System.out.println(PubFun.getCurrentDate() + " "
                        + PubFun.getCurrentTime() + ":生成保单Contid:" + mContID);
                //不需要处理
                if (mNoDealContIDSet.contains(mContID))
                {
                    break;
                }
                continue;
            }

            if (nodeName.equals("BPOBATCHNO"))
            {
                mBPOBatchNo = contNode.getFirstChild().getNodeValue();
                continue;
            }

            if (nodeName.equals("APPNTTABLE"))
            {
                //解析投保人XML
                VData tAppntVData = tLCPolParser.getLCAppntData(contNode);
                tTransferData.setNameAndValue("tAppntVData", tAppntVData);
                continue;
            }

            if (nodeName.equals("INSUREDTABLE"))
            {
                //解析被保险人XML
                VData tInsuredVData = tLCPolParser.getLCInsuredData(contNode);
                tTransferData.setNameAndValue("tInsuredVData", tInsuredVData);
                continue;
            }
            if (nodeName.equals("LCPOLTABLE"))
            {
                //解析一个险种保单节点
                VData tVData = tLCPolParser.parseLCPolNode(contNode);
                if (tVData == null || tVData.size() == 0)
                {
                    return new TransferData();
                }
                LCContSchema tLCContSchema = (LCContSchema) tVData
                        .getObjectByObjectName("LCContSchema", 0);
                if (mPrtNo == null || mPrtNo.equals(""))
                {
                    mPrtNo = (String) ((VData) tVData.getObjectByObjectName(
                            "VData", 0)).getObjectByObjectName("String", 0);
                }
                tVData.removeElement(tLCContSchema);

                //以合同号为索引保存险种保单信息
                m_LCPolImpInfo.addContPolData(mContID, tVData);
                tLCContSchema.setDueFeeMsgFlag(tLCPolParser.getMDueFeeMsgFlag());
                tLCContSchema.setExiSpec(tLCPolParser.getMExiSpec());
                //TODO
                tLCContSchema.setPayMode("4");
                tLCContSchema.setInputDate(PubFun.getCurrentDate());
                tLCContSchema.setInputTime(PubFun.getCurrentTime());
                tLCContSchema.setInputOperator("YBK");
                tLCContSchema.setExiSpec("N");
                tTransferData.setNameAndValue("tLCContSchema", tLCContSchema);

                continue;
            }
            if (nodeName.equals("BNFTABLE"))
            {
                //解析受益人XML
                LCBnfSet tLCBnfSet = (LCBnfSet) tLCPolParser
                        .getLCBnfSet(contNode);
                tTransferData.setNameAndValue("tLCBnfSet", tLCBnfSet);

                continue;
            }
            if (nodeName.equals("IMPARTTABLE"))
            {
                //解析客户告知XML
                LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = (LCCustomerImpartDetailSet) tLCPolParser
                        .getImpartSet(contNode);
                tTransferData.setNameAndValue("tLCCustomerImpartDetailSet",
                        tLCCustomerImpartDetailSet);
                continue;
            }

            if (nodeName.equals("ISSUETABLE"))
            {
                SchemaSet set = tBPOCreateSchema.createSchema(contNode,
                        "BPOIssue", mBatchNo, mPrtNo, mGlobalInput.Operator);
                mBPOMap.put(set, SysConst.INSERT);
                continue;
            }
        }

        return tTransferData;
    }

    /**
     * dealBPOState
     * 1、修改外包任务状态
     * 2、生成保单导入轨迹
     * @param cBPOMissionState String
     * @param cIssueExists String
     */
    private void dealBPOState(String cBPOMissionState, String cIssueExists)
    {
        if (!mIsBPO)
        {
            return;
        }

        if (mPrtNo == null || mPrtNo.equals(""))
        {
            return;
        }

        //查询外包日志
        BPOMissionStateDB db = new BPOMissionStateDB();
        db.setBPOBatchNo(mBPOBatchNo == null ? mBatchNo : mBPOBatchNo);
        db.setBussNoType("TB");
        db.setBussNo(mPrtNo);

        BPOMissionStateSet set = db.query();
        if (set.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "ParseXml";
            tError.errorMessage = "没有查询到外包任务状态";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);

            m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "", null,
                    mErrors, mGlobalInput, mPrtNo);
            mErrors.clearErrors();
            return;
        }

        //修改外包任务状态
        BPOMissionStateSchema tBPOMissionStateSchema = set.get(1);
        tBPOMissionStateSchema.setState(cBPOMissionState);
        tBPOMissionStateSchema.setImportCount(tBPOMissionStateSchema
                .getImportCount() + 1);
        tBPOMissionStateSchema.setIssueExist(cIssueExists);
        tBPOMissionStateSchema.setOperator(mGlobalInput.Operator);
        tBPOMissionStateSchema.setModifyDate(PubFun.getCurrentDate());
        tBPOMissionStateSchema.setModifyTime(PubFun.getCurrentTime());

        BPOMissionDetailStateSchema schema = new BPOMissionDetailStateSchema();
        ref.transFields(schema, tBPOMissionStateSchema);

        String sql = "select max(SequenceNo) + 1 "
                + "from BPOMissionDetailState " + "where BPOBatchNo = '"
                + mBPOBatchNo + "' " + "   and MissionID = '"
                + schema.getMissionID() + "' " + "   and BussNoType = 'TB' "
                + "   and BussNo = '" + schema.getBussNo() + "' ";
        String nextNo = new ExeSQL().getOneValue(sql);
        if ("".equals(nextNo) || "null".equals(nextNo))
        {
            nextNo = "1";
        }
        schema.setSequenceNo(nextNo);
        schema.setOperateType(BPO.OPERATE_TYPE_IMPORT);
        schema.setOperateResult(BPO.MISSION_STATE_INPUT_CONT
                .equals(cBPOMissionState) ? "1" : "0");

        String content = "";
        String tES_Doc_Main_State = "";
        if (BPO.MISSION_STATE_INPUT_CONT.equals(cBPOMissionState))
        {
            tES_Doc_Main_State = "06";
            content = "已跳转到复核工作流";
        }
        else if (BPO.MISSION_STATE_CREATE_CONT.equals(cBPOMissionState))
        {
            tES_Doc_Main_State = "06";
            content = "已生成保单但无法跳转到复核工作流";
        }
        else if (BPO.MISSION_STATE_DATA_ERR.equals(cBPOMissionState))
        {
            tES_Doc_Main_State = "05";
            content = "数据无法生成保单";
        }
        schema.setContent(content);
        schema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(schema);

        MMap tMMap = new MMap();
        tMMap.put(set.get(1).getSchema(), SysConst.UPDATE);
        tMMap.put(schema, SysConst.INSERT);

        //修改扫描件状态
        sql = "update ES_DOC_MAIN " + "set State = '" + tES_Doc_Main_State
                + "', " + "   Operator = '" + mGlobalInput.Operator + "', "
                + "   ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + "   ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + "where DocCode = '" + mPrtNo + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        VData data = new VData();
        data.add(tMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println(p.mErrors.getErrContent());
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "dealBPOState";
            tError.errorMessage = "生成xml保单导入历史失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);

            m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "", null,
                    mErrors, mGlobalInput, mPrtNo);
            mErrors.clearErrors();
            return;
        }
    }

    /**
     * inputConfirm
     *
     * @param cBatchNo String
     * @param cPrtNo String
     * @return boolean
     */
    private boolean inputConfirm(String cBPOBatchNo, String cPrtNo)
    {
        if (!mIsBPO)
        {
            return false;
        }

        BPOMissionStateDB db = new BPOMissionStateDB();
        db.setBPOBatchNo(cBPOBatchNo);
        db.setBussNo(cPrtNo);
        BPOMissionStateSet set = db.query();
        if (set.size() == 0 || set.size() > 1)
        {
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "inputConfirm";
            tError.errorMessage = "同一批次同一印刷号应该有且只有一个保单";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setPrtNo(cPrtNo);
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "LCParseGuideIn";
            tError.functionName = "inputConfirm";
            tError.errorMessage = "没有查询到以下印刷号的保单" + cPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("ContNo", tLCContSet.get(1).getContNo());
        mTransferData.setNameAndValue("PrtNo", cPrtNo);
        mTransferData
                .setNameAndValue("AppntNo", tLCContSet.get(1).getAppntNo());
        mTransferData.setNameAndValue("AppntName", tLCContSet.get(1)
                .getAppntName());
        mTransferData.setNameAndValue("AgentCode", tLCContSet.get(1)
                .getAgentCode());
        mTransferData.setNameAndValue("ManageCom", tLCContSet.get(1)
                .getManageCom());
        mTransferData.setNameAndValue("Operator", mGlobalInput.Operator);
        mTransferData.setNameAndValue("MakeDate", tLCContSet.get(1)
                .getMakeDate());
        mTransferData.setNameAndValue("MissionID", set.get(1).getMissionID());
        mTransferData.setNameAndValue("SubMissionID", set.get(1)
                .getSubMissionID());
//by gzh 添加问题件数据
        if(!"".equals(cPrtNo) && cPrtNo !=null){
        	String sql = "select 1 from bpoissue where prtno = '"+cPrtNo+"' ";
            String BPOText = new ExeSQL().getOneValue(sql);
            if("1".equals(BPOText)){
            	mTransferData.setNameAndValue("BPOText",BPOText);
            }
        }
//---- by gzh end
        VData tVData = new VData();
        tVData.add(mTransferData);
        tVData.add(mGlobalInput);
        TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
        if (!tTbWorkFlowUI.submitData(tVData, set.get(1).getActivityID()))
        {
            mErrors.copyAllErrors(tTbWorkFlowUI.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 将准备好的数据提交给 proposalBL
     * 处理险种保单,计算责任项,保费项,给付项
     * @param prepareData VData
     * @return MMap
     */
    private MMap submitDatatToProposalBL(VData prepareData)
    {

        MMap submitMap = new MMap();

        //取出合同、被保人、投保人的创建信息
        MMap prepareMap = (MMap) prepareData.getObjectByObjectName("MMap", 0);
        if (prepareMap != null)
        {
            submitMap.add(prepareMap);
        }

        //险种ID
        String strPolId = (String) ((TransferData) prepareData
                .getObjectByObjectName("TransferData", 0)).getValueByName("ID");
        //险种保单关键字
        String PolKey = (String) ((TransferData) prepareData
                .getObjectByObjectName("TransferData", 0))
                .getValueByName("PolKey");

        //从险种保单关键字中解析出合同ID
        String strContId = m_LCPolImpInfo.getContKey(PolKey);

        ProposalBL tProposalBL = new ProposalBL();
        if (!tProposalBL.PrepareSubmitData(prepareData, "INSERT||PROPOSAL"))
        {
            //保单提交计算失败
            this.mErrors.copyAllErrors(tProposalBL.mErrors);
            return null;
        }
        else
        {
            VData resultData = tProposalBL.getSubmitResult();
            if (resultData == null)
            {
                CError.buildErr(this, "保单提交计算失败!");
                return null;
            }

            //险种保单
            LCPolSchema rPolSchema = (LCPolSchema) resultData
                    .getObjectByObjectName("LCPolSchema", 0);

            rPolSchema.setSaleChnlDetail("01");
            rPolSchema.setPayIntv("0");
            rPolSchema.setAppntNo(mAppntNo);
            rPolSchema.setAppntName(mAppntName);
            rPolSchema.setRiskSeqNo(String.valueOf(this.mRiskSeqNo++));
            //保费项
            LCPremBLSet rPremBLSet = (LCPremBLSet) resultData
                    .getObjectByObjectName("LCPremBLSet", 0);
            if (rPremBLSet == null)
            {
                CError.buildErr(this, "保单提交计算保费项数据准备有误!");
                return null;
            }
            LCPremSet rPremSet = new LCPremSet();
            for (int i = 1; i <= rPremBLSet.size(); i++)
            {
            	rPremBLSet.get(i).setPayIntv("0");
                rPremSet.add(rPremBLSet.get(i));
            }

            //责任项
            LCDutyBLSet rDutyBLSet = (LCDutyBLSet) resultData
                    .getObjectByObjectName("LCDutyBLSet", 0);
            if (rDutyBLSet == null)
            {
                CError.buildErr(this, "保单提交计算责任项数据准备有误!");
                return null;
            }
            LCDutySet rDutySet = new LCDutySet();
            for (int i = 1; i <= rDutyBLSet.size(); i++)
            {
            	rDutyBLSet.get(i).setPayIntv("0");
                rDutySet.add(rDutyBLSet.get(i));
            }

            //给付项
            LCGetBLSet rGetBLSet = (LCGetBLSet) resultData
                    .getObjectByObjectName("LCGetBLSet", 0);
            if (rGetBLSet == null)
            {
                CError.buildErr(this, "保单提交计算给付项数据准备有误!");
                return null;
            }
            LCGetSet rGetSet = new LCGetSet();
            for (int i = 1; i <= rGetBLSet.size(); i++)
            {
                rGetSet.add(rGetBLSet.get(i));
            }

            //受益人
            LCBnfBLSet rBnfBLSet = (LCBnfBLSet) resultData
                    .getObjectByObjectName("LCBnfBLSet", 0);
            LCBnfSet rBnfSet = new LCBnfSet();
            if (rBnfBLSet != null)
            {
                for (int g = 1; g <= rBnfBLSet.size(); g++)
                {
                    String tSql11 = "select 1 from LMRiskEdorItem where edorcode='BC' and riskcode ='"
                            + rPolSchema.getRiskCode() + "'";
                    System.out.println("tSql11" + tSql11);
                    SSRS tSSRS = new SSRS();
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(tSql11);
                    System.out.println("tSSRS" + tSSRS.getMaxRow());
                    if (tSSRS.getMaxRow() == 1)
                    {
                        System.out.println("zhuzhuzhu!!!!!!!!!!!");
                        rBnfSet.add(rBnfBLSet.get(g));
                    }
                }
            }

            //连身被保人
            LCInsuredRelatedSet tLCInsuredRelatedSet = (LCInsuredRelatedSet) resultData
                    .getObjectByObjectName("LCInsuredRelatedSet", 0);

            LCContSchema tLCContSchema = m_LCPolImpInfo
                    .findLCContfromCache(strContId);
            if (tLCContSchema == null)
            {
                CError.buildErr(this, "查找合同[" + strContId + "]失败");
                return null;
            }

            //            //取出合同号
            //            mContNo = tLCContSchema.getContNo();

            //更新合同的相关数据
            tLCContSchema.setPrem(PubFun.setPrecision(tLCContSchema.getPrem()
                    + rPolSchema.getPrem(), "0.00"));
            tLCContSchema.setAmnt(PubFun.setPrecision(tLCContSchema.getAmnt()
                    + rPolSchema.getAmnt(), "0.00"));
            tLCContSchema.setSumPrem(PubFun.setPrecision(tLCContSchema
                    .getSumPrem()
                    + rPolSchema.getSumPrem(), "0.00"));
            tLCContSchema.setMult(PubFun.setPrecision(tLCContSchema.getMult()
                    + rPolSchema.getMult(), "0.00"));
            tLCContSchema.setCardFlag("0");
            tLCContSchema.setSaleChnlDetail("01");

            //将更新过的合同信息重新缓存
            m_LCPolImpInfo.cacheLCContSchema(strContId, tLCContSchema);

            //防止引用同一对象
            LCContSchema tContSchema = new LCContSchema();
            tContSchema.setSchema(tLCContSchema);
            LCContSet tContSet = new LCContSet();
            tContSet.add(tContSchema);

            submitMap.put(tContSet, "UPDATE");
            submitMap.put(rPolSchema, "INSERT");
            submitMap.put(rPremSet, "INSERT");
            submitMap.put(rDutySet, "INSERT");
            submitMap.put(rGetSet, "INSERT");
            submitMap.put(rBnfSet, "INSERT");
            submitMap.put(tLCInsuredRelatedSet, "INSERT");

            Date date = new Date();
            Random rd = new Random(date.getTime());
            long u = rd.nextLong();

            StringBuffer sbSql = new StringBuffer();
            sbSql.append(" update lccont set ").append(" peoples = ( ").append(
                    " select count(distinct insuredno) from lcpol ").append(
                    " where ").append(u).append(" = ").append(u).append(
                    " and contno = '").append(tContSchema.getContNo()).append(
                    "')").append(" where PolType = '0' and contno = '").append(
                    tContSchema.getContNo()).append("'");

            submitMap.put(sbSql.toString(), "UPDATE");

            //缓存险种保单信息
            m_LCPolImpInfo.cachePolInfo(strPolId, rPolSchema);

        }

        return submitMap;
    }

    private boolean batchSave(MMap map)
    {
        PubSubmit pubSubmit = new PubSubmit();
        VData sData = new VData();
        sData.add(map);
        boolean tr = pubSubmit.submitData(sData, "");

        if (!tr)
        {
            if (pubSubmit.mErrors.getErrorCount() > 0)
            {
                //错误回退
                mErrors.copyAllErrors(pubSubmit.mErrors);
                pubSubmit.mErrors.clearErrors();
            }
            else
            {
                CError.buildErr(this, "保存数据库的时候失败！");
            }
            return false;
        }
        return true;
    }

    /**
     * 以合同为单位做磁盘投保
     *
     * @return boolean
     * @param contIndex String
     */
    private boolean DiskContImport(String contIndex)
    {
        LCInsuredSchema insuredSchema = null;
        MMap subMap = null; //提交结果集缓存
        boolean state = true; //导入状态，
        boolean saveState = true;
        String logRiskCode = "";
        String logPolId = "";
        String insuredIndex = "";
        subMap = null;

        //全局变量，每个合同重新清空
        mContNo = "";
        mManageCom = "";
        mAppntNo = "";
        mAppntName = "";
        mAgentCode = "";
        mImportDate = "";
        iSequenceNo = 1;
        this.mRiskSeqNo = 1;

        LCGrpImportLogSchema logSchema = m_LCPolImpInfo.getLogInfo(mBatchNo,
                contIndex);

        if (logSchema != null && "0".equals(logSchema.getErrorState()))
        {
            //该批次号该合同已经导入成功
            buildError("DiskContImport", "导入的合同系统中已存在！");
            logError(contIndex, insuredIndex, logPolId, logRiskCode,
                    insuredSchema);
            return false;
        }

        //根据合同ID取得该合同ID下的险种保单数据集
        VData tContPolData = (VData) m_LCPolImpInfo.getContPolData(contIndex);
        //        LCAppntSet tmpLCAppntSet = new LCAppntSet();
        //        tmpLCAppntSet.set((LCAppntSet) m_LCPolImpInfo.
        //                             getContPolData(contIndex));
        if (tContPolData != null)
        {
            LCPolSchema tLCPolSchema = null;
            VData tPolData = null;
            for (int u = 0; u < tContPolData.size(); u++)
            {
                tPolData = (VData) tContPolData.get(u);
                
                if (state == false)
                {
                    break;
                }
                /** 需要取所有险种最小的缴费频次 */
                for (int i = 0; i < tPolData.size(); i++)
                {
                    LCPolSchema tmpLCPolSchema = (LCPolSchema) ((VData) tPolData
                            .get(i)).getObjectByObjectName("LCPolSchema", 0);
                    //DETELE BY JL
                    /*if (mPayIntv > tmpLCPolSchema.getPayIntv())
                    {
                        mPayIntv = tmpLCPolSchema.getPayIntv();
                    }*/
                    
                }
                
                for (int i = 0; i < tPolData.size(); i++)
                {
                    //清空，避免重复使用
                    logRiskCode = "";
                    logPolId = "";

                    if (state == false)
                    {
                        break;
                    }

                    VData onePolData = (VData) tPolData.get(i);
                    if (onePolData == null)
                    {
                        continue;
                    }
                    //缓存保单数据备用
                    tLCPolSchema = (LCPolSchema) onePolData
                            .getObjectByObjectName("LCPolSchema", 0);
                    
                    String riskcode = tLCPolSchema.getRiskCode();
                	// 判断是否为豁免险 kingcode=S 为豁免险
                	String sql = "select 1 from lmriskapp where riskcode='" + riskcode + "' and kindcode='S'";
                	ExeSQL tExeSQL = new ExeSQL();
                	if("1".equals(tExeSQL.getOneValue(sql))){
                		tLCPolSchema.setAmnt(0);
                	}
                    
                    logRiskCode = tLCPolSchema.getRiskCode();
                    insuredIndex = tLCPolSchema.getInsuredNo();
                    contIndex = tLCPolSchema.getContNo();

                    VData tPrepareData = prepareProposalBLData(onePolData);
                    if (tPrepareData == null)
                    {
                        System.out.println("向ProposalBL准备数据的时候出错！");
                        state = false;
                    }
                    else
                    {
                        MMap tMap = submitDatatToProposalBL(tPrepareData);
                        if (tMap == null)
                        {
                            state = false;
                        } else {
                        	LCPolSchema tLCPol = (LCPolSchema)tMap.getObjectByObjectName("LCPolSchema", 1);
                        }
                        if (subMap == null)
                        {
                            subMap = tMap;
                        }
                        else
                        {
                            subMap.add(tMap);
                        }
                    }
                }
                
            }
        }

        if (state == false)
        {
            logError(contIndex, insuredIndex, logPolId, logRiskCode,
                    insuredSchema);
            saveState = false;
            return false;
        }

        //======ADD===2005-03-22======ZHANGTAO============BGN========================
        //客户告知信息处理
        LCCustomerImpartDetailSet tImpartDetailSet = m_LCPolImpInfo
                .findImpartDetailSet(contIndex);
        LCCustomerImpartDetailSet newImpartDetailSet = new LCCustomerImpartDetailSet();

        LCCustomerImpartSet tImpartSet = new LCCustomerImpartSet();
        LCCustomerImpartSchema tImpartSchema = new LCCustomerImpartSchema();
        LCCustomerImpartParamsSet tImpartParamsSet = new LCCustomerImpartParamsSet();
        LCContSchema tLCContSchema = m_LCPolImpInfo
                .findLCContfromCache(contIndex);

        Reflections ref = new Reflections();

        String strNo;
        String strCustomerID = "";
        String strCustomerNo = "";
        String strCustomerNoType = "";

        if (tImpartDetailSet != null && tImpartDetailSet.size() > 0)
        {
            int ttNo = 0;
            for (int t = 1; t <= tImpartDetailSet.size(); t++)
            {
                LCCustomerImpartDetailSchema tImpartDetailSchema = new LCCustomerImpartDetailSchema();
                LCCustomerImpartSchema mImpartSchema = new LCCustomerImpartSchema();
                tImpartDetailSchema.setSchema(tImpartDetailSet.get(t));
                for (int count = 1; count <= tImpartDetailSet.size(); count++)
                {
                    if (StrTool.cTrim(tImpartDetailSet.get(t).getImpartVer())
                            .equals(tImpartDetailSet.get(count).getImpartVer())
                            & StrTool.cTrim(
                                    tImpartDetailSet.get(t).getImpartCode())
                                    .equals(
                                            tImpartDetailSet.get(count)
                                                    .getImpartCode())
                            & StrTool
                                    .cTrim(
                                            tImpartDetailSet.get(t)
                                                    .getDiseaseContent())
                                    .equals(
                                            tImpartDetailSet.get(count)
                                                    .getDiseaseContent())
                            & StrTool.cTrim(
                                    tImpartDetailSet.get(t).getStartDate())
                                    .equals(
                                            tImpartDetailSet.get(count)
                                                    .getStartDate())
                            & StrTool.cTrim(
                                    tImpartDetailSet.get(t).getEndDate())
                                    .equals(
                                            tImpartDetailSet.get(count)
                                                    .getEndDate())
                            & StrTool
                                    .cTrim(tImpartDetailSet.get(t).getProver())
                                    .equals(
                                            tImpartDetailSet.get(count)
                                                    .getProver())
                            & StrTool.cTrim(
                                    tImpartDetailSet.get(t).getCurrCondition())
                                    .equals(
                                            tImpartDetailSet.get(count)
                                                    .getCurrCondition())
                            & StrTool.cTrim(
                                    tImpartDetailSet.get(t).getIsProved())
                                    .equals(
                                            tImpartDetailSet.get(count)
                                                    .getIsProved())
                            & StrTool.cTrim(
                                    tImpartDetailSet.get(t)
                                            .getImpartDetailContent()).equals(
                                    tImpartDetailSet.get(count)
                                            .getImpartDetailContent())
                            & count != t)
                    {
                        // @@错误处理
                        buildError("DiskContImport", "合同ID：" + contIndex
                                + "被保人ID：" + strCustomerID
                                + "健康状况告知中的告知版本，告知编码，告知内容，疾病内容录入重复，请检查！");
                        state = false;
                        logError(contIndex, insuredIndex, logPolId,
                                logRiskCode, insuredSchema);
                        saveState = false;
                        return false;
                    }
                }

                strCustomerID = tImpartDetailSchema.getCustomerNo();
                strCustomerNoType = tImpartDetailSchema.getCustomerNoType();
                if ("0".equals(strCustomerNoType))
                {
                    //投保人
                    LCAppntSchema ttLCAppntSchema = m_LCPolImpInfo
                            .findAppntfromCache(strCustomerID);
                    if (ttLCAppntSchema == null)
                    {
                        buildError("DiskContImport", "投保人信息查询失败！" + "合同ID："
                                + contIndex + "投保人ID：" + strCustomerID);
                        state = false;
                        logError(contIndex, insuredIndex, logPolId,
                                logRiskCode, insuredSchema);
                        saveState = false;
                        return false;

                    }
                    strCustomerNo = ttLCAppntSchema.getAppntNo();
                }
                if ("I".equals(strCustomerNoType))
                {
                    //被保人
                    LCInsuredSchema ttLCInsuredSchema = m_LCPolImpInfo
                            .findInsuredfromCache(strCustomerID);
                    if (ttLCInsuredSchema == null)
                    {
                        buildError("DiskContImport", "被保人信息查询失败！" + "合同ID："
                                + contIndex + "被保人ID：" + strCustomerID);
                        state = false;
                        logError(contIndex, insuredIndex, logPolId,
                                logRiskCode, insuredSchema);
                        saveState = false;
                        return false;

                    }
                    strCustomerNo = ttLCInsuredSchema.getInsuredNo();
                }

                tImpartDetailSchema.setGrpContNo(tLCContSchema.getGrpContNo());
                tImpartDetailSchema.setContNo(tLCContSchema.getContNo());
                tImpartDetailSchema.setProposalContNo(tLCContSchema
                        .getProposalContNo());
                tImpartDetailSchema.setPrtNo(tLCContSchema.getPrtNo());

                tImpartDetailSchema.setSubSerialNo(String.valueOf(t));
                tImpartDetailSchema.setCustomerNo(strCustomerNo);

                tImpartDetailSchema.setOperator(mGlobalInput.Operator);
                tImpartDetailSchema.setMakeDate(PubFun.getCurrentDate());
                tImpartDetailSchema.setMakeTime(PubFun.getCurrentTime());
                tImpartDetailSchema.setModifyDate(PubFun.getCurrentDate());
                tImpartDetailSchema.setModifyTime(PubFun.getCurrentTime());
                //同样告知编码不进行保存
                int SameFlag = 0;
                if (tImpartSet.size() > 0)
                {
                    for (int c1 = 1; c1 <= tImpartSet.size(); c1++)
                    {
                        if (StrTool.cTrim(tImpartSet.get(c1).getImpartCode())
                                .equals(tImpartDetailSchema.getImpartCode())
                                & StrTool.cTrim(
                                        tImpartSet.get(c1).getImpartVer())
                                        .equals(
                                                tImpartDetailSchema
                                                        .getImpartVer())
                                & StrTool.cTrim(
                                        tImpartSet.get(c1).getProposalContNo())
                                        .equals(
                                                tImpartDetailSchema
                                                        .getProposalContNo())
                                & StrTool.cTrim(
                                        tImpartSet.get(c1).getGrpContNo())
                                        .equals(
                                                tImpartDetailSchema
                                                        .getGrpContNo())
                                & StrTool.cTrim(
                                        tImpartSet.get(c1).getCustomerNo())
                                        .equals(
                                                tImpartDetailSchema
                                                        .getCustomerNo())
                                & StrTool.cTrim(
                                        tImpartSet.get(c1).getCustomerNoType())
                                        .equals(
                                                tImpartDetailSchema
                                                        .getCustomerNoType()))
                        {
                            SameFlag = 1;
                            break;
                        }
                    }
                    if (SameFlag != 1)
                    {
                        ref.transFields(mImpartSchema, tImpartDetailSchema);
                    }
                }
                if (tImpartSet == null | tImpartSet.size() == 0)
                {
                    ref.transFields(mImpartSchema, tImpartDetailSchema);
                }
                mImpartSchema.setImpartParamModle(tImpartDetailSchema
                        .getImpartDetailContent());
                LDImpartDB tLDImpartDB = new LDImpartDB();
                tLDImpartDB.setImpartVer(tImpartDetailSchema.getImpartVer());
                tLDImpartDB.setImpartCode(tImpartDetailSchema.getImpartCode());
                if (!tLDImpartDB.getInfo())
                {
                    buildError("DiskContImport", "客户告知内容查询失败！"
                            + "请检查告知版别和告知编码是否正确" + "合同ID：" + contIndex
                            + "客户ID：" + strCustomerID);
                    state = false;
                    logError(contIndex, insuredIndex, logPolId, logRiskCode,
                            insuredSchema);
                    saveState = false;
                    return false;

                }
                tImpartDetailSchema.setImpartDetailContent(tLDImpartDB
                        .getImpartContent());

                mImpartSchema.setImpartContent(tImpartDetailSchema
                        .getImpartDetailContent());

                if (tImpartDetailSchema.getDiseaseContent() != null
                        && !"".equals(tImpartDetailSchema.getDiseaseContent()))
                {
                    newImpartDetailSet.add(tImpartDetailSchema);
                }
                if (SameFlag != 1)
                {
                    tImpartSet.add(mImpartSchema);
                }
            }

            CustomerImpartBL mCustomerImpartBL = new CustomerImpartBL();
            VData tempVData = new VData();
            tempVData.add(tImpartSet);
            tempVData.add(mGlobalInput);
            mCustomerImpartBL.submitData(tempVData, "IMPART||DEAL");
            if (mCustomerImpartBL.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "dealData";
                tError.errorMessage = mCustomerImpartBL.mErrors.getFirstError()
                        .toString();
                this.mErrors.addOneError(tError);
                state = false;
                logError(contIndex, insuredIndex, logPolId, logRiskCode,
                        insuredSchema);
                saveState = false;
                return false;
            }

            tempVData.clear();
            tempVData = mCustomerImpartBL.getResult();
            if (null != (LCCustomerImpartSet) tempVData.getObjectByObjectName(
                    "LCCustomerImpartSet", 0))
            {
                tImpartSet = (LCCustomerImpartSet) tempVData
                        .getObjectByObjectName("LCCustomerImpartSet", 0);
                System.out.println("告知条数" + tImpartSet.size());
            }
            else
            {
                System.out.println("告知条数为空");
            }
            if (null != (LCCustomerImpartParamsSet) tempVData
                    .getObjectByObjectName("LCCustomerImpartParamsSet", 0))
            {
                tImpartParamsSet = (LCCustomerImpartParamsSet) tempVData
                        .getObjectByObjectName("LCCustomerImpartParamsSet", 0);
            }

        }

        subMap.put(tImpartSet, "INSERT");
        subMap.put(tImpartParamsSet, "INSERT");
        if (newImpartDetailSet != null && newImpartDetailSet.size() > 0)
        {
            subMap.put(newImpartDetailSet, "INSERT");
        }
        //======ADD===2005-03-22======ZHANGTAO============BGN========================

        if (state == false)
        {
            logError(contIndex, insuredIndex, logPolId, logRiskCode,
                    insuredSchema);
            saveState = false;
            return false;
        }

           /* //合同导入成功，需要创建工作流节点！！！！
            TransferData missionTransferData = new TransferData();
            //准备工作流节点数据
            missionTransferData.setNameAndValue("BatchNo", mBatchNo);
            missionTransferData.setNameAndValue("PrtNo", mPrtNo);
            missionTransferData.setNameAndValue("ContID", contIndex);
            missionTransferData.setNameAndValue("ImportDate", mImportDate);
            missionTransferData.setNameAndValue("ManageCom", mManageCom);
            missionTransferData.setNameAndValue("Operator",
                    mGlobalInput.Operator);
            missionTransferData.setNameAndValue("ContNo", mContNo);
            missionTransferData.setNameAndValue("AppntNo", mAppntNo);
            missionTransferData.setNameAndValue("AppntName", mAppntName);
            missionTransferData.setNameAndValue("AgentCode", mAgentCode);
            VData missionData = new VData();
            missionData.add(mGlobalInput);
            missionData.add(missionTransferData);
            MMap missionMap = m_LCPolImpInfo.createMission(missionData);
            if (missionMap == null)
            {
                state = false;
            }
            else
            {
                subMap.add(missionMap);
            }

            if (state == false)
            {
                logError(contIndex, insuredIndex, logPolId, logRiskCode,
                        insuredSchema);
                saveState = false;
                return false;
            }*/
         // 准备新单复核的工作流
     		if (true) {
     			MMap missionMap = new MMap();
     			mMissionid = PubFun1.CreateMaxNo("MissionID", 20);
     			LWMissionSchema tLWMissionSchema = new LWMissionSchema();
     			tLWMissionSchema.setMissionID(mMissionid);
     			tLWMissionSchema.setSubMissionID("1");
     			tLWMissionSchema.setProcessID(STATIC_PROCESSID);
     			tLWMissionSchema.setActivityID(STATIC_ActivityID);
     			tLWMissionSchema.setActivityStatus("1");
     			tLWMissionSchema.setMissionProp1(mContNo);
     			tLWMissionSchema.setMissionProp2(mPrtNo);
     			tLWMissionSchema.setMissionProp3(mAppntNo);
     			tLWMissionSchema.setMissionProp4(mAppntName);
     			tLWMissionSchema.setMissionProp5(mGlobalInput.Operator);
     			tLWMissionSchema.setMissionProp6(mImportDate);
     			tLWMissionSchema.setMissionProp7(mAgentCode);
     			tLWMissionSchema.setMissionProp8(mManageCom);
     			tLWMissionSchema.setLastOperator(mGlobalInput.Operator);
     			tLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
     			tLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
     			tLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
     			tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
     			tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
     			tLWMissionSchema.setDefaultOperator("001");
     			missionMap.put(tLWMissionSchema, "INSERT");
     			LWMissionSchema ttLWMissionSchema = new LWMissionSchema();
     			ttLWMissionSchema.setMissionID(mMissionid);
     			ttLWMissionSchema.setSubMissionID("1");
     			ttLWMissionSchema.setProcessID(STATIC_PROCESSID);
     			ttLWMissionSchema.setActivityID(STATIC_EActivityID);
     			ttLWMissionSchema.setActivityStatus("1");
     			ttLWMissionSchema.setMissionProp1(mPrtNo);
     			ttLWMissionSchema.setMissionProp2(mContNo);
     			ttLWMissionSchema.setLastOperator(mGlobalInput.Operator);
     			ttLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
     			ttLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
     			ttLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
     			ttLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
     			ttLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
     			ttLWMissionSchema.setDefaultOperator("001");
     			missionMap.put(ttLWMissionSchema, "INSERT");
     			if (missionMap == null) {
     				state = false;
     			} else {
     				subMap.add(missionMap);
     			}

     			if (state == false) {
     				saveState = false;
     				return false;
     			}
     		}
        
        boolean bs = true;
        if (state && subMap != null && subMap.keySet().size() > 0)
        {
            MMap logMap = m_LCPolImpInfo.logSucc(mBatchNo, contIndex, mPrtNo,
                    mContNo, insuredIndex, mGlobalInput);
/*            *//**
             *Yangming:在磁盘导入成功后判断工作流表中是否存在扫描件
             * 若有删除
             *//*
            String Sql = "delete from lwmission where "
                    + " activityid = '0000001099' and processid = '0000000003'"
                    + " and missionprop1='" + mPrtNo + "'";
            String cSql = "update es_doc_main set InputStartDate='"
                    + PubFun.getCurrentDate() + "',InputStartTime='"
                    + PubFun.getCurrentTime() + "',ModifyDate='"
                    + PubFun.getCurrentDate() + "',ModifyTime='"
                    + PubFun.getCurrentTime() + "' where doccode='" + mPrtNo
                    + "'";

            if (!mIsBPO)
            {
                subMap.put(Sql, "DELETE");
            }
            subMap.put(cSql, "UPDATE");*/
            subMap.add(logMap);

            //导入所有准备的Map
            bs = batchSave(subMap);
        }

        if (!bs || !state)
        {
            System.out.println("合同[" + contIndex + "]导入失败！！"
                    + this.mErrors.getErrContent());

            //导入失败日志，数据库保存，只能定位到合同上的失败
            logError(contIndex, "", "", "", null);

            //保存导入信息
            saveState = false;
        }
        else
        {
            System.out.println("合同[" + contIndex + "]导入成功！！");
        }

        return saveState;
    }

    public String getExtendFileName(String aFileName)
    {
        File tFile = new File(aFileName);
        String aExtendFileName = "";
        String name = tFile.getName();
        for (int i = name.length() - 1; i >= 0; i--)
        {
            if (i < 1)
            {
                i = 1;
            }
            if (name.substring(i - 1, i).equals("."))
            {
                aExtendFileName = name.substring(i, name.length());
                System.out.println("ExtendFileName;" + aExtendFileName);
                return aExtendFileName;
            }
        }
        return aExtendFileName;
    }

    /**
     * 字符串替换
     *
     * @param s1 String
     * @param OriginStr String
     * @param DestStr String
     * @return java.lang.String
     */
    public static String replace(String s1, String OriginStr, String DestStr)
    {
        s1 = s1.trim();
        int mLenOriginStr = OriginStr.length();
        for (int j = 0; j < s1.length(); j++)
        {
            int befLen = s1.length();
            if (s1.substring(j, j + 1) == null
                    || s1.substring(j, j + 1).trim().equals(""))
            {
                continue;
            }
            else
            {
                if (OriginStr != null && DestStr != null)
                {
                    if (j + mLenOriginStr <= s1.length())
                    {

                        if (s1.substring(j, j + mLenOriginStr)
                                .equals(OriginStr))
                        {

                            OriginStr = s1.substring(j, j + mLenOriginStr);

                            String startStr = s1.substring(0, j);
                            String endStr = s1.substring(j + mLenOriginStr, s1
                                    .length());

                            s1 = startStr + DestStr + endStr;

                            j = j + s1.length() - befLen;
                            if (j < 0)
                            {
                                j = 0;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return s1;
    }

    /**
     * 得到日志显示结果
     *
     * @return com.sinosoft.utility.VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {

        //        LCAppntSchema schema = new LCAppntSchema();
        //        schema.setAppntBirthday("2005-4-32");
        //        String a=schema.getAppntBirthday();
        //        String[] month = PubFun.calFLDate(schema.getAppntBirthday());
        //        if (Integer.parseInt(schema.getAppntBirthday().split("-")[2]) >
        //            Integer.parseInt(month[1].split("-")[2]) ||
        //            Integer.parseInt(schema.getAppntBirthday().split("-")[1]) <
        //            Integer.parseInt(month[0].split("-")[2])) {
        //        }

        LCParseGuideIn tPGI = new LCParseGuideIn();

        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FileName", "2099994501_01.xls");
        tTransferData.setNameAndValue("FilePath", "E:/");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "PolImport";
        tG.ManageCom = "86";
        long secode = (new Date()).getTime();
        //        String secode = time.substring(time.lastIndexOf(":") + 1);
        //        Time time = new Time();
        //        while (true) {
        //            Date date = new Date();
        //            if (date.getTime() - secode > 3000) {
        //                break;
        //            }
        //        }

        tVData.add(tTransferData);
        tVData.add(tG);

        String strStartTime = PubFun.getCurrentTime();

        tPGI.submitData(tVData, ""); //提交
        
        if (tPGI.mErrors.needDealError())
        {
            System.out
                    .println("tPGI.mErrors : " + tPGI.mErrors.getFirstError());
        }
        String strEndTime = PubFun.getCurrentTime();

        System.out.println("===============================================");
        System.out.println("      the start time is : " + strStartTime);
        System.out.println("===============================================");

        System.out.println("===============================================");
        System.out.println("       the end time is : " + strEndTime);
        System.out.println("===============================================");

    }

    /**
     * Build a instance document object for function transfromNode()
     */
    private void bulidDocument()
    {
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware(true);

        try
        {
            m_doc = dfactory.newDocumentBuilder().newDocument();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Detach node from original document, fast XPathAPI process.
     *
     * @param node Node
     * @return org.w3c.dom.Node
     * @throws Exception
     */
    private org.w3c.dom.Node transformNode(org.w3c.dom.Node node)
            throws Exception
    {
        Node nodeNew = m_doc.importNode(node, true);

        return nodeNew;
    }

    /**
     * 险种保单数据准备(按险种单导入)
     * @param polData VData     险种保单数据集
     * @return VData           返回数据集
     */
    private VData prepareProposalBLData(VData polData)
    {
        VData prepareData = new VData();
        MMap prepareMap = new MMap();

        TransferData tTransferData = (TransferData) polData
                .getObjectByObjectName("TransferData", 0);

        LCPolSchema tLCPolSchema = (LCPolSchema) polData.getObjectByObjectName(
                "LCPolSchema", 0);

        LCDutySet tLCDutySet = (LCDutySet) polData.getObjectByObjectName(
                "LCDutySet", 0);

        //合同ID
        String strContId = tLCPolSchema.getContNo();
        //险种ID
        String strPolID = (String) tTransferData.getValueByName("ID");
        //主险ID
        String strMainPolId = StrTool.cTrim(tLCPolSchema.getMainPolNo());
        //险种代码
        String strRiskCode = tLCPolSchema.getRiskCode();
        //投保人Id
        String strAppntId = tLCPolSchema.getAppntNo();
        //被保人ID
        String strInsuredId = tLCPolSchema.getInsuredNo();
        //连身被保人ID 缓存在 appflag 字段
        String strRelaInsId = tLCPolSchema.getAppFlag();
        //借用完及时清空字段
        tLCPolSchema.setAppFlag(null);

        //合同、投保人、被保人
        LCContSchema contSchema = null;
        LCInsuredSchema insuredSchema = null;
        LCAppntSchema appntSchema = null;

        //************************************************
        //判断是主险还是附加险
        //如果为附加险，则直接用主险相关信息设置险种保单的信息
        //如果为主险，需要查找主险相应的合同、被保人、投保人
        //利用合同、被保人、投保人来设置险种保单的信息
        //************************************************

        //根据代理人编码查询代理人组别
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCPolSchema.getAgentCode());
        System.out.println("tLCPolSchema.getAgentCode()-----"+tLCPolSchema.getAgentCode());
//        tLAAgentDB.setBranchType("1");
//        tLAAgentDB.setBranchType2("01");
        if (!tLAAgentDB.getInfo())
        {
            buildError("prepareProposalBLData", "代理人组别查询失败" + "合同ID："
                    + strContId);
            return null;
        }
        if (Integer.parseInt(tLAAgentDB.getAgentState()) > 2)
        {
            buildError("prepareProposalBLData", "代理人:" + tLAAgentDB.getName()
                    + ",代理人编码:" + tLAAgentDB.getAgentCode() + ",已离职" + ",合同ID："
                    + strContId);
            return null;
        }


        tLCPolSchema.setRemark("");
        tLCPolSchema.setAgentGroup(tLAAgentDB.getAgentGroup());

        //判断是主险还是附加险
        if (!"".equals(strMainPolId) && !strPolID.equals(strMainPolId))
        {
            //为附加险，则查找主险 (主险是在附险之前创建)
            LCPolSchema tMainLCPolSchema = m_LCPolImpInfo
                    .findCacheLCPolSchema(strMainPolId);
            if (tMainLCPolSchema == null)
            {
                //主险还没有被创建
                buildError("prepareProposalBLData", "数据库和导入轨迹中找不到附加险("
                        + strPolID + ")对应的个人主险保单(" + strMainPolId + ")纪录");
                return null;
            }

            mainPolBL.setSchema(tMainLCPolSchema);

            //将主险的相应信息赋给该附加险
            tLCPolSchema.setMainPolNo(tMainLCPolSchema.getPolNo());
            tLCPolSchema.setContNo(StrTool.cTrim(tMainLCPolSchema.getContNo()
                    .trim()));
            tLCPolSchema.setInsuredNo(StrTool.cTrim(tMainLCPolSchema
                    .getInsuredNo().trim()));
            tLCPolSchema.setAppntNo(StrTool
                    .cTrim(tMainLCPolSchema.getAppntNo()));
            tLCPolSchema.setInsuredPeoples("1");
            if (tLCPolSchema.getCValiDate() == null
                    || "".equals(tLCPolSchema.getCValiDate())
                    || "N".equals(tLCPolSchema.getSpecifyValiDate()))
            {
                //excel页面数据中没有指定生效日期
                //tLCPolSchema.setSpecifyValiDate("0");

                // 附加险校验主险是否为万能类险种，如果是同主险处理生效日期。
                //if (CommonBL.isULIRisk(tMainLCPolSchema.getRiskCode()))
                //{
                dealULIValiDate(tLCPolSchema);
                //}
            }
            else
            {
                tLCPolSchema.setSpecifyValiDate("1");
            }

            //为防止错误的校验
            String insuredIndex = tLCPolSchema.getInsuredNo();
            insuredSchema = m_LCPolImpInfo.findInsuredfromCache(strInsuredId);
            if (insuredSchema == null)
            {
                mErrors.clearErrors();
                buildError("prepareProposalBLData", "新增附加险:找不到对应主险的被保人信息,ID="
                        + insuredIndex);
                return null;
            }

        }
        else
        {
            //本身为主险保单,设置主险保单号为空
            tLCPolSchema.setMainPolNo("");

            // 处理生效日期
            if (tLCPolSchema.getCValiDate() == null
                    || "".equals(tLCPolSchema.getCValiDate())
                    || "N".equals(tLCPolSchema.getSpecifyValiDate()))
            {
                dealULIValiDate(tLCPolSchema);
            }
            else
            {
                tLCPolSchema.setSpecifyValiDate("1");
            }
            // --------------------
        }
        //判断合同是否已经创建过
        contSchema = m_LCPolImpInfo.findLCContfromCache(strContId);
        if (contSchema == null)
        {
            //*******************************************
            //合同未创建
            //创建 合同、被保人、投保人
            //将 合同、被保人、投保人相互赋值
            //将新创建的 合同、被保人、投保人缓存起来(cache)
            //将 合同、被保人、投保人一起扔进 map (insert)
            //*******************************************

            //检验该印刷号是否已经产生合同(同一印刷号只能产生一条合同)
            if (!m_LCPolImpInfo.checkPrtNO(tLCPolSchema.getPrtNo()))
            {
                return null;
            }
            if (!"00".equals(tLCPolSchema.getMasterPolNo()))
            {
                CError.buildErr(this, "主被保人必须放在最前!" + "合同ID:" + strContId
                        + "险种ID:" + strPolID);
                return null;

            }
            //创建 被保人
            if (strInsuredId != null)
            {
                insuredSchema = m_LCPolImpInfo.prepareInsured(strInsuredId);
                if (insuredSchema == null)
                {
                    //创建被保人出错
                    return null;
                }
            }
            if (StrTool.cTrim(insuredSchema.getOccupationCode()).equals("")
                    || insuredSchema.getOccupationCode().equals("null"))
            {
                CError.buildErr(this, "没保险人没有录入职业代码");
                return null;
            }
            if (StrTool.cTrim(insuredSchema.getOccupationType()).equals("")
                    || insuredSchema.getOccupationCode().equals("null"))
            {
                CError.buildErr(this, "没保险人没有录入职业代码");
                return null;
            }
            if (StrTool.cTrim(insuredSchema.getName()).equals(""))
            {
                CError.buildErr(this, "没有录入被保险人姓名");
                return null;
            }

            //*******************************************
            //判断被保人与投保人的关系
            //如果为同一人，则用被保人信息赋给投保人
            //不为同一人，则创建投保人
            //*******************************************

            //判断被保人与投保人的关系
            /**
             * yangming:上一程序员使用lcpol中的签单机构字段缓存一投保人关系
             * 此方法不好容易混淆.有待改进.
             */
            LCAppntSchema tempLCAppntSchema = new LCAppntSchema();
            if (m_LCPolImpInfo.prepareAppntAccInfo(strAppntId) == null)
            {
                CError.buildErr(this, "找不到ID为:" + strAppntId + "的投保人！");
                return null;
            }
            else
            {
                tempLCAppntSchema.setSchema((LCAppntSchema) m_LCPolImpInfo
                        .prepareAppntAccInfo(strAppntId));
            }
            if ("00".equals(tLCPolSchema.getSignCom()))
            {
                //为同一人，用被保人信息设置投保人
                /**
                 * yangMing: 虽然投保人和被保人是同一个人信息应该一样
                 * 但是银行帐户信息的概念不同,被保人银行帐户时理赔金帐户
                 * 与投保帐户有概念上的不同,个人感觉应该注掉此段,但是
                 * 考虑到上一个编程者可能另有用意,因此,在此处只把银行帐户
                 * 覆盖回投保人帐户.
                 */
                Reflections ref = new Reflections();
                appntSchema = new LCAppntSchema();
                ref.transFields(appntSchema, insuredSchema);
                appntSchema.setAccName(tempLCAppntSchema.getAccName());
                appntSchema.setBankAccNo(tempLCAppntSchema.getBankAccNo());
                appntSchema.setBankCode(tempLCAppntSchema.getBankCode());
                appntSchema.setPosition(tempLCAppntSchema.getPosition());
                appntSchema.setSalary(tempLCAppntSchema.getSalary());
                appntSchema.setAppntNo(insuredSchema.getInsuredNo());
                appntSchema.setAppntName(insuredSchema.getName());
                appntSchema.setAppntSex(insuredSchema.getSex());
                appntSchema.setAppntBirthday(insuredSchema.getBirthday());
                
                appntSchema.setExiSpec(tempLCAppntSchema.getExiSpec());
                
                appntSchema.setOperator(mGlobalInput.Operator);
                appntSchema.setMakeDate(PubFun.getCurrentDate());
                appntSchema.setMakeTime(PubFun.getCurrentTime());
                appntSchema.setModifyDate(appntSchema.getMakeDate());
                appntSchema.setModifyTime(appntSchema.getMakeTime());
            }
            else
            {
                //不为同一人,创建投保人
                //将暂存特约信息的职位清空。

                if (strAppntId != null)
                {
                    appntSchema = m_LCPolImpInfo.prepareAppnt(strAppntId);
                  //  if (appntSchema != null)
                  //  {
                  //      appntSchema.setPosition("");
                  //  }
                    if (appntSchema == null)
                    {
                        //创建投保人出错
                        return null;
                    }
                    if (StrTool.cTrim(appntSchema.getAppntName()).equals(""))
                    {
                        CError.buildErr(this, "没有录入投保人姓名！");
                        return null;
                    }
                }
            }
            //创建完成投保人信息将投保人客户号填入LCPol表
            tLCPolSchema.setAppntNo(appntSchema.getAppntNo());
            //设置合同的关键信息
            contSchema = new LCContSchema();
            contSchema.setSchema(m_LCPolImpInfo.getLCContSchema());

            // 处理“续期提醒”标志的默认值，如果不填，视为：否-N
            String tTmpDueFeeMsgFlag = StrTool.cTrim(contSchema
                    .getDueFeeMsgFlag());
            if (tTmpDueFeeMsgFlag.equals(""))
            {
                contSchema.setDueFeeMsgFlag("N");
            }
            // --------------------

            String nolimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
            String strProposalContNo = PubFun1.CreateMaxNo("ProposalContNo",
                    nolimit);

            Reflections ref = new Reflections();
            /**
             * yangming:需要保存特约信息，由于picc需要的特约是针对
             * 整单的，而不是针对险种，因此最佳解决方案为在投保人信息
             * 的sheet中添加一条特约信息，但是难度较大，目前暂时存在
             * 险种界面，然后保存到schema中，因此在做下面操作前应先
             * 备份特约信息,在做完复制操作后进行覆盖。
             * 2005-06-20 yangming：在磁盘投保模板中进行修改将特约
             * 信息放到投保人界面。这样符合业务逻辑，因为特约是针对合
             * 同的，而一个合同就有一个投保人。但是目前仍然存在问题，
             * 在投保人界面的特约信息很难存储到险种信息界面，因此借用
             * 投保人的职位暂存特约信息。
             */
//            ref.transFields(contSchema, tLCPolSchema);
            contSchema.setManageCom(tLCPolSchema.getManageCom());
            if (!contSchema.getManageCom().startsWith(mGlobalInput.ManageCom))
            {
                CError.buildErr(this, "保单管理机构为：" + contSchema.getManageCom()
                        + "当前登陆机构为：" + mGlobalInput.ManageCom
                        + "请使用保单管理机构或直属上级机构导入！");
                return null;
            }
            contSchema.setRemark(tempLCAppntSchema.getWorkType());
            contSchema.setPremScope(tempLCAppntSchema.getBMI());
            contSchema.setGrpContNo(STATIC_GRPCONTNO);
            contSchema.setContNo(strProposalContNo);
            contSchema.setProposalContNo(strProposalContNo);
            contSchema.setPrtNo(tLCPolSchema.getPrtNo());
            contSchema.setContType(STATIC_CONTTYPE);
            contSchema.setAppFlag("0");
            contSchema.setStateFlag("0");
            /**
             * yangming:对保单最新的标志ProposalType
             * 01-普通个单投保书
             * 02-普通团单投保书
             * 03-家庭个单投保书
             * 04-询价团单投保书
             * 05-银代个单投保书
             */
            contSchema.setProposalType("03");
            //个单保单类型默认值
            contSchema.setPolType("0");
            //yangming:签单机构应在签单时填写,不应在现在,因此注掉
            contSchema.setSignCom(mGlobalInput.ManageCom);
            //家庭单类型被缓存在保单类型标记字段
            if ("".equals(tLCPolSchema.getPolTypeFlag()))
            {
                //excel数据为空，取默认值
                /**
                 * yangming:Picch的个单是家庭单
                 */
                contSchema.setFamilyType("1");
            }
            else
            {
                contSchema.setFamilyType(tLCPolSchema.getPolTypeFlag());
            }

            contSchema.setManageCom(tLCPolSchema.getManageCom());
            contSchema.setExecuteCom(tLCPolSchema.getManageCom());
            contSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
            contSchema.setAgentCode(tLCPolSchema.getAgentCode());
            contSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            contSchema.setAgentCom(tLCPolSchema.getAgentCom());
            contSchema.setPolApplyDate(tLCPolSchema.getPolApplyDate());
            contSchema.setCValiDate(tLCPolSchema.getCValiDate());
            contSchema.setPayIntv(this.mPayIntv);
            //DELETE BY JL
           /* *//** 缴费频次存储完成后将其恢复为12,用于下一被保险人 *//*
            mPayIntv = 12;*/
            contSchema.setPayMode(tLCPolSchema.getPayMode());

            //设置合同的被保人信息
            contSchema.setInsuredNo(insuredSchema.getInsuredNo());
            contSchema.setInsuredName(insuredSchema.getName());
            contSchema.setInsuredSex(insuredSchema.getSex());
            contSchema.setInsuredBirthday(insuredSchema.getBirthday());
            contSchema.setInsuredIDType(insuredSchema.getIDType());
            contSchema.setInsuredIDNo(insuredSchema.getIDNo());

            //设置合同的投保人信息
            contSchema.setAppntNo(appntSchema.getAppntNo());
            contSchema.setAppntName(appntSchema.getAppntName());
            contSchema.setAppntSex(appntSchema.getAppntSex());
            contSchema.setAppntBirthday(appntSchema.getAppntBirthday());
            contSchema.setAppntIDType(appntSchema.getIDType());
            contSchema.setAppntIDNo(appntSchema.getIDNo());

            //设置合同的银行账户信息
            contSchema.setBankCode(appntSchema.getBankCode());
            contSchema.setBankAccNo(appntSchema.getBankAccNo());
            contSchema.setAccName(appntSchema.getAccName());

            contSchema.setOperator(mGlobalInput.Operator);
            contSchema.setMakeDate(PubFun.getCurrentDate());
            contSchema.setMakeTime(PubFun.getCurrentTime());
            contSchema.setModifyDate(contSchema.getMakeDate());
            contSchema.setModifyTime(contSchema.getMakeTime());
            
            //TODO ADD BY JL
            contSchema.setPayMethod("4");

            //设置投保人的有关合同信息
            appntSchema.setGrpContNo(contSchema.getGrpContNo());
            appntSchema.setContNo(contSchema.getContNo());
            appntSchema.setPrtNo(contSchema.getPrtNo());
            appntSchema.setManageCom(contSchema.getManageCom());

            //设置被保人的有关合同信息
            insuredSchema.setSequenceNo("1");
            insuredSchema.setGrpContNo(contSchema.getGrpContNo());
            insuredSchema.setContNo(contSchema.getContNo());
            insuredSchema.setPrtNo(contSchema.getPrtNo());
            insuredSchema.setManageCom(contSchema.getManageCom());
            insuredSchema.setExecuteCom(tLCPolSchema.getManageCom());
            insuredSchema.setAppntNo(appntSchema.getAppntNo());
            insuredSchema.setManageCom(contSchema.getManageCom());

            if (!"".equals(insuredSchema.getOccupationCode()))
            {
                LDOccupationDB tLDOccupationDB = new LDOccupationDB();
                tLDOccupationDB.setOccupationCode(insuredSchema
                        .getOccupationCode());
                if (!tLDOccupationDB.getInfo())
                {
                    CError.buildErr(this, "被保人职业工种查询失败!" + "被保人ID:"
                            + strInsuredId + "职业代码:"
                            + insuredSchema.getOccupationCode());
                    return null;
                }
                insuredSchema.setOccupationType(tLDOccupationDB
                        .getOccupationType());
            }

            //与投保人关系 被缓存在 签单机构 字段
            insuredSchema.setRelationToAppnt(tLCPolSchema.getSignCom());
            //与主被保人关系 缓存在 [主被保人保单号]
            insuredSchema.setRelationToMainInsured(tLCPolSchema
                    .getMasterPolNo());

            //将新创建的 合同、被保人、投保人缓存起来
            m_LCPolImpInfo.cacheLCContSchema(strContId, contSchema);
            m_LCPolImpInfo.cacheLCInsuredSchema(strInsuredId, insuredSchema);
            m_LCPolImpInfo.cacheLCAppntSchema(strAppntId, appntSchema);

            //将新创建的 合同、被保人、投保人一起扔进 map
            prepareMap.put(contSchema, "INSERT");
            prepareMap.put(insuredSchema, "INSERT");
            prepareMap.put(appntSchema, "INSERT");
        }
        else
        {
            //合同已创建
            //====== ADD ===== zhangtao ====== 2005-03-17 ============= BGN ===============
            //创建 被保人
            if (strInsuredId != null)
            {
                insuredSchema = m_LCPolImpInfo.prepareInsured(strInsuredId);
                if (insuredSchema == null)
                {
                    //创建被保人出错
                    return null;
                }
            }

            if (!insuredSchema.getContNo().equals(contSchema.getContNo()))
            {
                if ("00".equals(tLCPolSchema.getMasterPolNo()))
                {
                    CError.buildErr(this, "同一合同不能有多个主被保人!" + "合同ID:"
                            + strContId);
                    return null;

                }

                iSequenceNo++;
                /** 只要新建一个被保险人救就重置险种序号 */
                this.mRiskSeqNo = 1;
                //设置被保人的有关合同信息
                insuredSchema.setSequenceNo(String.valueOf(iSequenceNo));
                insuredSchema.setGrpContNo(contSchema.getGrpContNo());
                insuredSchema.setContNo(contSchema.getContNo());
                insuredSchema.setPrtNo(contSchema.getPrtNo());
                insuredSchema.setManageCom(contSchema.getManageCom());
                insuredSchema.setExecuteCom(tLCPolSchema.getManageCom());
                insuredSchema.setAppntNo(contSchema.getAppntNo());
                insuredSchema.setManageCom(tLCPolSchema.getManageCom());

                //与投保人关系 被缓存在 签单机构 字段
                insuredSchema.setRelationToAppnt(tLCPolSchema.getSignCom());
                //与主被保人关系 缓存在 [主被保人保单号]
                insuredSchema.setRelationToMainInsured(tLCPolSchema
                        .getMasterPolNo());

                if (!"".equals(insuredSchema.getOccupationCode()))
                {
                    LDOccupationDB tLDOccupationDB = new LDOccupationDB();
                    tLDOccupationDB.setOccupationCode(insuredSchema
                            .getOccupationCode());
                    if (!tLDOccupationDB.getInfo())
                    {
                        CError.buildErr(this, "被保人职业工种查询失败!" + "被保人ID:"
                                + strInsuredId + "职业代码:"
                                + insuredSchema.getOccupationCode());
                        return null;
                    }
                    insuredSchema.setOccupationType(tLDOccupationDB
                            .getOccupationType());
                }

                //将新创建的被保人缓存起来
                m_LCPolImpInfo
                        .cacheLCInsuredSchema(strInsuredId, insuredSchema);

                //将新创建的被保人扔进 map
                prepareMap.put(insuredSchema, "INSERT");

            }

            //====== ADD ===== zhangtao ====== 2005-03-17 ============= END ===============
            //            }

            //*******************************************
            //从缓存中取出 合同、投保人、被保人
            //利用合同、被保人、投保人来设定险种保单的信息
            //*******************************************

            //从缓存中取出 合同
            contSchema = m_LCPolImpInfo.findLCContfromCache(strContId);
            if (contSchema == null)
            {
                CError.buildErr(this, "未找到合同[" + strContId + "]");
                return null;
            }

            //从缓存中取出 投保人
            appntSchema = m_LCPolImpInfo.findAppntfromCache(strAppntId);
            if (appntSchema == null)
            {
                CError.buildErr(this, "未找到投保人[" + strAppntId + "]");
                return null;
            }

            //从缓存中取出 被保人
            insuredSchema = m_LCPolImpInfo.findInsuredfromCache(strInsuredId);
            if (insuredSchema == null)
            {
                CError.buildErr(this, "未找到被保险人[" + strInsuredId + "]");
                return null;
            }

            //利用合同、被保人、投保人来设定险种保单的信息
            tLCPolSchema.setContNo(contSchema.getContNo().trim());
            tLCPolSchema.setPolTypeFlag(contSchema.getPolType());
            tLCPolSchema.setPrtNo(insuredSchema.getPrtNo().trim());
            tLCPolSchema.setInsuredNo(insuredSchema.getInsuredNo());
            tLCPolSchema.setInsuredName(insuredSchema.getName());
            tLCPolSchema.setInsuredSex(insuredSchema.getSex());
            tLCPolSchema.setInsuredBirthday(insuredSchema.getBirthday());
            tLCPolSchema.setOccupationType(insuredSchema.getOccupationType());
            tLCPolSchema.setAppntNo(appntSchema.getAppntNo());
            tLCPolSchema.setAppntName(appntSchema.getAppntName());
            tLCPolSchema.setInsuredPeoples("1");

            // 处理生效日期
            if (tLCPolSchema.getCValiDate() == null
                    || "".equals(tLCPolSchema.getCValiDate())
                    || "N".equals(tLCPolSchema.getSpecifyValiDate()))
            {
                dealULIValiDate(tLCPolSchema);
            }
            else
            {
                tLCPolSchema.setSpecifyValiDate("1");
            }
            // --------------------

            mainPolBL.setSchema(tLCPolSchema);
        }

        //处理连身被保险人
        LCInsuredRelatedSet tRelaInsSet = null;
        //连身被保人数组 (连身被保人可能有多个)
        String[] relaIns = null;
        if (!"".equals(StrTool.cTrim(strRelaInsId)))
        {
            relaIns = strRelaInsId.split(",");
            if (relaIns == null)
            {
                strRelaInsId.split(";");
            }
            if (relaIns != null)
            {
                tRelaInsSet = m_LCPolImpInfo.prepareInsuredRela(tLCPolSchema
                        .getInsuredNo(), //主被保人客户号
                        relaIns);
                if (tRelaInsSet == null)
                {
                    //准备连身被保险人出错
                    return null;
                }
            }
        }

        //处理受益人
        LCBnfSet tLCBnfSet = m_LCPolImpInfo.findContBnfSet(strContId,
                strInsuredId);
        if (tLCBnfSet != null && tLCBnfSet.size() > 0)
        {
            tLCBnfSet = m_LCPolImpInfo.prepareBnf(tLCBnfSet, insuredSchema);
        }

        //处理责任项
        LCDutySet tempDutySet = null;
        if (tLCDutySet == null || tLCDutySet.size() <= 1)
        {
            //责任项页签的数据为空或少于一条责任项
            //则以险种保单页签的数据为准
            LCDutySchema ttempDutySchema = new LCDutySchema();
            setDutyByPolInfo(ttempDutySchema, tLCPolSchema);
            tempDutySet = new LCDutySet();
            tempDutySet.add(ttempDutySchema);
        }
        else
        {
            tempDutySet = tLCDutySet;
        }

        //为防止出错的校验
        if (tempDutySet == null || tempDutySet.size() <= 0)
        {
            CError.buildErr(this, "险种[" + strRiskCode + "]责任不能为空");
            return null;
        }

        //借用完及时清空字段
        tLCPolSchema.setUWCode("");
        tLCPolSchema.setAppFlag(null);
        tLCPolSchema.setApproveFlag("");
        tLCPolSchema.setUWFlag("");
        tLCPolSchema.setSignCom("");
        tLCPolSchema.setPolTypeFlag("");
        tLCPolSchema.setMasterPolNo("");

        //从缓存中查找险种描述信息
        LMRiskAppSchema tLMRiskAppSchema = m_LCPolImpInfo
                .findLMRiskAppFromCache(strRiskCode);

        if (tLMRiskAppSchema == null)
        {
            //缓存中没有，去数据库查找
            tLMRiskAppSchema = m_LCPolImpInfo.findLMRiskAppFromDB(strRiskCode);

            if (tLMRiskAppSchema == null)
            {
                buildError("prepareData", strRiskCode + "险种对应的险种承保描述没有找到!");
                return null;
            }
            //将查找的险种描述信息缓存！！
            m_LCPolImpInfo.cacheLMRiskApp(strRiskCode, tLMRiskAppSchema);
        }

        //从缓存中查找险种描述信息
        LMRiskSchema tLMRiskSchema = m_LCPolImpInfo
                .findLMRiskFromCache(strRiskCode);
        if (tLMRiskSchema == null)
        {
            tLMRiskSchema = m_LCPolImpInfo.findLMRiskFromDB(strRiskCode);

            if (tLMRiskSchema == null)
            {
                buildError("prepareData", strRiskCode + "险种对应的险种承保描述没有找到!");
                return null;
            }
            //将查找的险种描述信息缓存！！
            m_LCPolImpInfo.cacheLMRisk(strRiskCode, tLMRiskSchema);
        }

        String PolKey = m_LCPolImpInfo.getPolKey(strContId, strInsuredId,
                strRiskCode);

        tTransferData.setNameAndValue("PolKey", PolKey);
        tTransferData.setNameAndValue("samePersonFlag", 0);
        tTransferData.setNameAndValue("GrpImport", 1);

        //合同数据创建成功，保存全局变量备用
        mContNo = contSchema.getContNo();
        mManageCom = contSchema.getManageCom();
        //        mPrtNo = contSchema.getPrtNo();
        mAppntNo = contSchema.getAppntNo();
        mAppntName = contSchema.getAppntName();
        mAgentCode = contSchema.getAgentCode();
        mImportDate = contSchema.getMakeDate();

        prepareData.add(tTransferData);
        prepareData.add(mGlobalInput);
        prepareData.add(contSchema);
        prepareData.add(insuredSchema);
        prepareData.add(appntSchema);
        prepareData.add(tLCPolSchema);
        prepareData.add(tRelaInsSet);
        prepareData.add(tLCBnfSet);
        prepareData.add(tempDutySet);

        prepareData.add(tLMRiskAppSchema);
        prepareData.add(tLMRiskSchema);
        prepareData.add(mainPolBL);
        prepareData.add(prepareMap);

        return prepareData;
    }

    /**
     * 根据险种保单的数据设置责任项的数据
     * @param dutySchema LCDutySchema
     * @param tLCPolSchema LCPolSchema
     */
    private void setDutyByPolInfo(LCDutySchema dutySchema,
            LCPolSchema tLCPolSchema)
    {
        if (tLCPolSchema.getPayIntv() > 0)
        {
            dutySchema.setPayIntv(tLCPolSchema.getPayIntv());
        }
        if (tLCPolSchema.getInsuYear() > 0)
        {
            dutySchema.setInsuYear(tLCPolSchema.getInsuYear());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getInsuYearFlag())))
        {
            dutySchema.setInsuYearFlag(tLCPolSchema.getInsuYearFlag());
        }
        if (tLCPolSchema.getPayEndYear() > 0)
        {
            dutySchema.setPayEndYear(tLCPolSchema.getPayEndYear());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getPayEndYearFlag())))
        {
            dutySchema.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag());
        }
        if (tLCPolSchema.getGetYear() > 0)
        {
            dutySchema.setGetYear(tLCPolSchema.getGetYear());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getGetYearFlag())))
        {
            dutySchema.setGetYearFlag(tLCPolSchema.getGetYearFlag());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getGetStartType())))
        {
            dutySchema.setGetStartType(tLCPolSchema.getGetStartType());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getBonusGetMode())))
        {
            dutySchema.setBonusGetMode(tLCPolSchema.getBonusGetMode());
        }

        if (!"".equals(StrTool.cTrim(tLCPolSchema.getPremToAmnt())))
        {
            dutySchema.setPremToAmnt(tLCPolSchema.getPremToAmnt());
        }
        if (tLCPolSchema.getMult() > 0)
        {
            dutySchema.setMult(tLCPolSchema.getMult());
        }
        if (tLCPolSchema.getPrem() > 0)
        {
            dutySchema.setPrem(tLCPolSchema.getPrem());
        }
        if (tLCPolSchema.getAmnt() > 0)
        {
            dutySchema.setAmnt(tLCPolSchema.getAmnt());
        }
        //计算规则被缓存在 最终核保人编码 UWCode 字段
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getUWCode())))
        {
            dutySchema.setCalRule(tLCPolSchema.getUWCode());
        }
        //费率
        if (tLCPolSchema.getFloatRate() > 0)
        {
            dutySchema.setFloatRate(tLCPolSchema.getFloatRate());
        }
        //免赔额
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getApproveFlag())))
        {
            dutySchema.setGetLimit(tLCPolSchema.getApproveFlag());
        }
        //赔付比例
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getUWFlag())))
        {
            dutySchema.setGetRate(tLCPolSchema.getUWFlag());
        }
        //备用字段
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag1())))
        {
            dutySchema.setStandbyFlag1(tLCPolSchema.getStandbyFlag1());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag2())))
        {
            dutySchema.setStandbyFlag2(tLCPolSchema.getStandbyFlag2());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag3())))
        {
            dutySchema.setStandbyFlag3(tLCPolSchema.getStandbyFlag3());
        }

    }

    /**
     * 创建错误日志
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ParseGuideIn";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 记录日志信息
     *
     * @param contId String
     * @param insuredId String
     * @param polId String
     * @param riskcode String
     * @param insuredSchema LCInsuredSchema
     */
    private void logError(String contId, String insuredId, String polId,
            String riskcode, LCInsuredSchema insuredSchema)
    {
        if (m_LCPolImpInfo.mErrors.getErrorCount() >= 0)
        {
            this.mErrors.copyAllErrors(m_LCPolImpInfo.mErrors);
        }

        m_LCPolImpInfo.logError(mBatchNo, contId, insuredId, polId, riskcode,
                insuredSchema, mErrors, mGlobalInput, mPrtNo);

        logErrors.copyAllErrors(this.mErrors);
        this.mErrors.clearErrors();
        m_LCPolImpInfo.mErrors.clearErrors();

    }

    /**
     * 处理万能类险种生效日期，该险种生效日期录入时默认为系统当天。
     * @param tLCPolSchema 
     */
    private boolean dealULIValiDate(LCPolSchema tLCPolSchema)
    {
        String tPolApplyDate = tLCPolSchema.getPolApplyDate();
        if (tPolApplyDate == null || tPolApplyDate.equals(""))
        {
            buildError("dealULIValiDate", "投保单投保日期不存在。");
            return false;
        }
        tLCPolSchema.setCValiDate(tPolApplyDate);
        tLCPolSchema.setSpecifyValiDate("N");

        return true;
    }
    
    /**
     * 清楚从LCPOL同步过来
     * 
     */

}
