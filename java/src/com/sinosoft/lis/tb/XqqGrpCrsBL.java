package com.sinosoft.lis.tb;

/**
 * <p>Title: FinBankAddBL</p>
 * <p>Description:添加银行 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : zhangjun
 * @date:2006-04-05
 * @version 1.0
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.sinosoft.lis.db.LBBankDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LOMixAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LOMixAgentSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import java.util.Date;
import java.text.SimpleDateFormat;

public class XqqGrpCrsBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
         public CErrors mErrors = new CErrors();
//         private VData mResult = new VData();
         String repRiskCode = "";
         String Crs_Salechnl = "";
         String Crs_Busstype = "";
         String repCodeGrp = "";
         String repCodePep = "";
         String repCodeName = "";
         String repCodeIdno = "";
         String bdlx = "";
         String bdh = "";
         private TransferData mAddElement = new TransferData(); //获取时间
         private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
         private LCContSchema mLCContSchema=new LCContSchema();
         private LCGrpContSchema mLCGrpContSchema=new LCGrpContSchema();
         private LOMixAgentSchema mLOMixAgentSchema=new LOMixAgentSchema();
         
        private String mResult;
    public XqqGrpCrsBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	
    	System.out.println("Start XqqGrpCrsBL Submit ...");
        try{
        if (!cOperate.equals("HEAD") &&!cOperate.equals("REPAIR") && !cOperate.equals("DELETE")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult="";
       
        if (cOperate.equals("HEAD"))
        {
            if (!setRisk()) {
                  return false;
            }

        }
        else if (cOperate.equals("REPAIR"))
        {
            if (!updateRisk()) {
                  return false;
            }

        }
        else if (cOperate.equals("DELETE"))
        {
            if (!delRisk()) {
                  return false;
            }

        }
        }catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "XqqGrpCrsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理错误! " + e.getMessage();
            this.mErrors .addOneError(tError);
            return false;
          }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) { //打印付费
        //全局变量
        mAddElement = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        repRiskCode = (String) mAddElement.getValueByName("repRiskCode");
        Crs_Salechnl = (String) mAddElement.getValueByName("Crs_Salechnl");
        Crs_Busstype = (String) mAddElement.getValueByName("Crs_Busstype");
        repCodeGrp = (String) mAddElement.getValueByName("repCodeGrp");
        repCodePep = (String) mAddElement.getValueByName("repCodePep");
        repCodeName = (String) mAddElement.getValueByName("repCodeName");
        repCodeIdno = (String) mAddElement.getValueByName("repCodeIdno");
        bdlx = (String)mAddElement.getValueByName("bdlx");
        bdh = (String)mAddElement.getValueByName("bdh"); 
        System.out.println("BL--repRiskCode:"+repRiskCode);
        System.out.println("BL--Crs_Salechnl:"+Crs_Salechnl);
        System.out.println("BL--Crs_Busstype:"+Crs_Busstype);
        System.out.println("BL--repCodeGrp:"+repCodeGrp);
        System.out.println("BL--repCodePep:"+repCodePep);
        System.out.println("BL--repCodeName:"+repCodeName);
        System.out.println("BL--repCodeIdno:"+repCodeIdno);
        System.out.println("BL--bdlx:"+bdlx);
        System.out.println("BL--bdh:"+bdh);
        return true;
    }

    public String getResult() {
        return mResult;
    }
    
    public LCContSchema getLCContSchema(){
        return mLCContSchema;
    }
    public LCGrpContSchema getLCGrpContSchema(){
        return mLCGrpContSchema;
    }
    public LOMixAgentSchema getLOMixAgentSchema(){
        return mLOMixAgentSchema;
    }
    
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "XqqGrpCrsBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean updateRisk()
    {  
    	System.out.println("bdlx:" +bdlx);
    	if(bdlx.equals("0"))
    	{
        	
    		System.out.println("团单开始");
    		LCGrpContDB mLCGrpContDB = new LCGrpContDB();
    		mLCGrpContDB.setGrpContNo(repRiskCode);
        	mLCGrpContDB.setCrs_SaleChnl(Crs_Salechnl);
        	mLCGrpContDB.setCrs_BussType(Crs_Busstype);
        	mLCGrpContDB.setGrpAgentCom(repCodeGrp);
        	mLCGrpContDB.setGrpAgentCode(repCodePep);
        	mLCGrpContDB.setGrpAgentName(repCodeName);
        	mLCGrpContDB.setGrpAgentIDNo(repCodeIdno);
        	
        	mLCGrpContDB.setGrpContNo(repRiskCode);
            if(mLCGrpContDB.getInfo()){
            	mLCGrpContSchema = mLCGrpContDB.getSchema();
            	System.out.println("查询到LCGrpCont数据");
            }
            
            System.out.println("repCodeIdno:"+repCodeIdno);
            mLCGrpContSchema.setGrpContNo(repRiskCode);
            mLCGrpContSchema.setCrs_SaleChnl(Crs_Salechnl);
            mLCGrpContSchema.setCrs_BussType(Crs_Busstype);
            mLCGrpContSchema.setGrpAgentCom(repCodeGrp);
            mLCGrpContSchema.setGrpAgentCode(repCodePep);
            mLCGrpContSchema.setGrpAgentName(repCodeName);
            mLCGrpContSchema.setGrpAgentIDNo(repCodeIdno);
            
            
            mLCGrpContDB.setSchema(mLCGrpContSchema);
            if (!mLCGrpContDB.update())
            {
            	System.out.println("成功插入LCGrpCont数据");
                buildError("XqqGrpCrsBL->update","更新失败");
                return false;
    	}
            
            mLOMixAgentSchema.setGrpContNo(repRiskCode);
         	mLOMixAgentSchema.setContNo("0000");
         	mLOMixAgentSchema.setSaleChnl(Crs_Salechnl);
         	mLOMixAgentSchema.setRemark("集团交叉提数");
         	mLOMixAgentSchema.setStandbyFlag1(null);
         	mLOMixAgentSchema.setStandbyFlag2(null);
         	mLOMixAgentSchema.setOperator("郭忠华");
         	mLOMixAgentSchema.setContType("2");
         	
        	SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm.ss");
         	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
         	Date date = new Date();
         	mLOMixAgentSchema.setMakeDate(date);
         	mLOMixAgentSchema.setMakeTime(sdf1.format(new Date()));
         	mLOMixAgentSchema.setModifyDate(date);
         	mLOMixAgentSchema.setModifyTime(sdf1.format(new Date()));
         	
         	LOMixAgentDB mLOMixAgentDB = new LOMixAgentDB();
         	mLOMixAgentDB.setGrpContNo(repRiskCode);
         	mLOMixAgentDB.setContNo("0000");
         	mLOMixAgentDB.setContType("2");
        	if(mLOMixAgentDB.getInfo()){
            	mLOMixAgentSchema = mLOMixAgentDB.getSchema();
            	System.out.println("查询到LOMixAgent数据");
            }
            mLOMixAgentSchema.setGrpContNo(repRiskCode);
                   
            mLOMixAgentDB.setSchema(mLOMixAgentSchema);
            if (!mLOMixAgentDB.update())
            {
                buildError("XqqGrpCrsBL->update","更新失败");
                return false;
            }
            return true;
    }else{
    	System.out.println("个单开始");
    	LCContDB mLCContDB = new LCContDB();
		mLCContDB.setContNo(repRiskCode);
    	mLCContDB.setCrs_SaleChnl(Crs_Salechnl);
   	    mLCContDB.setCrs_BussType(Crs_Busstype);
    	mLCContDB.setGrpAgentCom(repCodeGrp);
    	mLCContDB.setGrpAgentCode(repCodePep);
   	    mLCContDB.setGrpAgentName(repCodeName);
    	mLCContDB.setGrpAgentIDNo(repCodeIdno);
    	
        if(mLCContDB.getInfo()){
        	mLCContSchema = mLCContDB.getSchema();
        	System.out.println("查询到LCCont数据");
        }
       
        mLCContSchema.setContNo(repRiskCode);
    	mLCContSchema.setCrs_SaleChnl(Crs_Salechnl);
   	    mLCContSchema.setCrs_BussType(Crs_Busstype);
    	mLCContSchema.setGrpAgentCom(repCodeGrp);
    	mLCContSchema.setGrpAgentCode(repCodePep);
   	    mLCContSchema.setGrpAgentName(repCodeName);
    	mLCContSchema.setGrpAgentIDNo(repCodeIdno);
        mLCContDB.setSchema(mLCContSchema);
        if (!mLCContDB.update())
        {   
            buildError("XqqGrpCrsBL->lccontUpdate","更新失败");
            return false;
        }
        
        mLOMixAgentSchema.setGrpContNo("0000");
     	mLOMixAgentSchema.setContNo(repRiskCode);
     	mLOMixAgentSchema.setSaleChnl(Crs_Salechnl);
     	mLOMixAgentSchema.setRemark("集团交叉提数");
     	mLOMixAgentSchema.setStandbyFlag1(null);
     	mLOMixAgentSchema.setStandbyFlag2(null);
     	mLOMixAgentSchema.setOperator("郭忠华");
        mLOMixAgentSchema.setContType("2");
     	
    	SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm.ss");
     	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
     	Date date = new Date();
     	mLOMixAgentSchema.setMakeDate(date);
     	mLOMixAgentSchema.setMakeTime(sdf1.format(new Date()));
     	mLOMixAgentSchema.setModifyDate(date);
     	mLOMixAgentSchema.setModifyTime(sdf1.format(new Date()));
     	
     	LOMixAgentDB mLOMixAgentDB = new LOMixAgentDB();
     	mLOMixAgentDB.setGrpContNo("0000");
     	mLOMixAgentDB.setContNo(repRiskCode);
     	mLOMixAgentDB.setContType("2"); 
    	if(mLOMixAgentDB.getInfo()){
        	mLOMixAgentSchema = mLOMixAgentDB.getSchema();
        }
        mLOMixAgentSchema.setContNo(repRiskCode);
               
        mLOMixAgentDB.setSchema(mLOMixAgentSchema);
        if (!mLOMixAgentDB.update())
        { 
            buildError("XqqGrpCrsBL->update","更新失败");
            return false;
        }
        
    }
    	
    	return true;
    }
    
//  添加总行
    private boolean setRisk()
    {
          //添插入参数
    	
    	if(bdlx.equals("1")){
    		System.out.println("个单开始");
    		 mLCContSchema.setContNo(bdh);
             mLCContSchema.setCrs_SaleChnl(Crs_Salechnl);
             mLCContSchema.setCrs_BussType(Crs_Busstype);
             mLCContSchema.setGrpAgentCom(repCodeGrp);
             mLCContSchema.setGrpAgentCode(repCodePep);
         	 mLCContSchema.setGrpAgentName(repCodeName);
         	 mLCContSchema.setGrpAgentIDNo(repCodeIdno);

             LCContDB mLCContDB = new LCContDB();
             mLCContDB.setSchema(mLCContSchema);
             mLCContDB.setContNo(bdh);
             
             if(mLCContDB.getInfo()){
              	mLCContSchema = mLCContDB.getSchema();
              	System.out.println("查询到LCCont数据");
              }
         	  System.out.println("repCodeIdno:"+repCodeIdno);
              mLCContSchema.setContNo(bdh);
              mLCContSchema.setCrs_SaleChnl(Crs_Salechnl);
              mLCContSchema.setCrs_BussType(Crs_Busstype);
              mLCContSchema.setGrpAgentCom(repCodeGrp);
              mLCContSchema.setGrpAgentCode(repCodePep);
              mLCContSchema.setGrpAgentName(repCodeName);
              mLCContSchema.setGrpAgentIDNo(repCodeIdno);
         	   mLCContDB.setSchema(mLCContSchema);
             if (!mLCContDB.update())
             {
                 buildError("XqqGrpCrsBL->update","更新失败");
                 return false;
             }
             mLOMixAgentSchema.setGrpContNo("0000");
          	 mLOMixAgentSchema.setContNo(bdh);
          	 mLOMixAgentSchema.setSaleChnl(Crs_Salechnl);
          	 mLOMixAgentSchema.setRemark("集团交叉提数");
          	 mLOMixAgentSchema.setStandbyFlag1(null);
          	 mLOMixAgentSchema.setStandbyFlag2(null);
          	 mLOMixAgentSchema.setOperator("郭忠华");
             mLOMixAgentSchema.setContType("2");
         	
         	SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm.ss");
         	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
         	Date date = new Date();
         	mLOMixAgentSchema.setMakeDate(date);
         	mLOMixAgentSchema.setMakeTime(sdf1.format(new Date()));
         	mLOMixAgentSchema.setModifyDate(date);
         	mLOMixAgentSchema.setModifyTime(sdf1.format(new Date()));
         	
         	LOMixAgentDB mLOMixAgentDB = new LOMixAgentDB();
         	mLOMixAgentDB.setGrpContNo(bdh);
         	mLOMixAgentDB.setContNo("0000");
         	mLOMixAgentDB.setContType("2");
        	if(mLOMixAgentDB.getInfo()){
            	mLOMixAgentSchema = mLOMixAgentDB.getSchema();
            	System.out.println("查询到LOMixAgent数据");
            }
        	mLOMixAgentSchema.setGrpContNo(bdh);
            mLOMixAgentSchema.setStandbyFlag1(null);
         	mLOMixAgentSchema.setStandbyFlag2(null);      
            mLOMixAgentDB.setSchema(mLOMixAgentSchema);
            if (!mLOMixAgentDB.insert())
            {
                buildError("XqqGrpCrsBL->Headinset","插入失败");
                return false;
            }
            
    	}else{
    		System.out.println("团单开始");
    		LCGrpContDB mLCGrpContDB = new LCGrpContDB();
    		mLCGrpContDB.setGrpContNo(bdh);
        	mLCGrpContDB.setCrs_SaleChnl(Crs_Salechnl);
        	mLCGrpContDB.setCrs_BussType(Crs_Busstype);
        	mLCGrpContDB.setGrpAgentCom(repCodeGrp);
        	mLCGrpContDB.setGrpAgentCode(repCodePep);
        	mLCGrpContDB.setGrpAgentName(repCodeName);
        	mLCGrpContDB.setGrpAgentIDNo(repCodeIdno);
        	
        	mLCGrpContDB.setGrpContNo(bdh);
        	 if(mLCGrpContDB.getInfo()){
             	mLCGrpContSchema = mLCGrpContDB.getSchema();
             	System.out.println("查询到LCGrpCont数据");
             }
        	 System.out.println("repCodeIdno:"+repCodeIdno);
             mLCGrpContSchema.setGrpContNo(bdh);
             mLCGrpContSchema.setCrs_SaleChnl(Crs_Salechnl);
             mLCGrpContSchema.setCrs_BussType(Crs_Busstype);
             mLCGrpContSchema.setGrpAgentCom(repCodeGrp);
             mLCGrpContSchema.setGrpAgentCode(repCodePep);
             mLCGrpContSchema.setGrpAgentName(repCodeName);
             mLCGrpContSchema.setGrpAgentIDNo(repCodeIdno);
        	 mLCGrpContDB.setSchema(mLCGrpContSchema);
             if (!mLCGrpContDB.update())
             {
                 buildError("XqqGrpCrsBL->Headupdate","更新失败");
                 return false;
             }
            
         	mLOMixAgentSchema.setGrpContNo(bdh);
         	mLOMixAgentSchema.setContNo("0000");
         	mLOMixAgentSchema.setSaleChnl(Crs_Salechnl);
         	mLOMixAgentSchema.setRemark("集团交叉提数");
         	mLOMixAgentSchema.setStandbyFlag1(null);
         	mLOMixAgentSchema.setStandbyFlag2(null);
         	mLOMixAgentSchema.setOperator("郭忠华");
         	mLOMixAgentSchema.setContType("2");
         	
         	SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm.ss");
         	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
         	Date date = new Date();
         	mLOMixAgentSchema.setMakeDate(date);
         	mLOMixAgentSchema.setMakeTime(sdf1.format(new Date()));
         	mLOMixAgentSchema.setModifyDate(date);
         	mLOMixAgentSchema.setModifyTime(sdf1.format(new Date()));
         	
         	LOMixAgentDB mLOMixAgentDB = new LOMixAgentDB();
         	mLOMixAgentDB.setGrpContNo(bdh);
         	mLOMixAgentDB.setContNo("0000");
         	mLOMixAgentDB.setContType("2");
        	if(mLOMixAgentDB.getInfo()){
            	mLOMixAgentSchema = mLOMixAgentDB.getSchema();
            	System.out.println("查询到LOMixAgent数据");
            }
            mLOMixAgentSchema.setGrpContNo(bdh);
            mLOMixAgentSchema.setStandbyFlag1(null);
         	mLOMixAgentSchema.setStandbyFlag2(null);      
            mLOMixAgentDB.setSchema(mLOMixAgentSchema);
            if (!mLOMixAgentDB.insert())
            {
                buildError("XqqGrpCrsBL->Headinset","插入失败");
                return false;
            }
    	}
          
           return true;
    }
    
    private boolean delRisk()
    {    
    	if(bdlx.equals("1")){
       System.out.println("个单开始");
        mLCContSchema.setContNo(repRiskCode);
        mLCContSchema.setCrs_SaleChnl(null);
        mLCContSchema.setCrs_BussType(null);
        mLCContSchema.setGrpAgentCom(null);
        mLCContSchema.setGrpAgentCode(null);
    	mLCContSchema.setGrpAgentName(null);
        mLCContSchema.setGrpAgentIDNo(null);
        
        LCContDB mLCContDB = new LCContDB();
        mLCContDB.setContNo(repRiskCode);
       if(mLCContDB.getInfo()){
       	mLCContSchema = mLCContDB.getSchema();
       }
       mLCContSchema.setGrpContNo(repRiskCode);
       mLCContSchema.setCrs_SaleChnl(null);
       mLCContSchema.setCrs_BussType(null);
       mLCContSchema.setGrpAgentCom(null);
       mLCContSchema.setGrpAgentCode(null);
   	   mLCContSchema.setGrpAgentName(null);
       mLCContSchema.setGrpAgentIDNo(null);
       mLCContDB.setSchema(mLCContSchema);
       if (!mLCContDB.update())
       {
           buildError("XqqGrpCrsBL->LCContupdate","mLCContDB更新失败");
           return false;
           }
         
        mLOMixAgentSchema.setGrpContNo("0000");
    	mLOMixAgentSchema.setContNo(repRiskCode);
    	mLOMixAgentSchema.setSaleChnl(Crs_Salechnl);
    	mLOMixAgentSchema.setRemark("集团交叉提数");
    	mLOMixAgentSchema.setStandbyFlag1(null);
    	mLOMixAgentSchema.setStandbyFlag2(null);
    	mLOMixAgentSchema.setOperator("郭忠华");
        mLOMixAgentSchema.setContType("2");
     	
    	SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm.ss");
     	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
     	Date date = new Date();
     	mLOMixAgentSchema.setMakeDate(date);
     	mLOMixAgentSchema.setMakeTime(sdf1.format(new Date()));
     	mLOMixAgentSchema.setModifyDate(date);
     	mLOMixAgentSchema.setModifyTime(sdf1.format(new Date()));
     	
     	LOMixAgentDB mLOMixAgentDB = new LOMixAgentDB();
     	mLOMixAgentDB.setGrpContNo(repRiskCode);
     	mLOMixAgentDB.setContNo("0000");
     	mLOMixAgentDB.setContType("2");
     	if(mLOMixAgentDB.getInfo()){
        	mLOMixAgentSchema = mLOMixAgentDB.getSchema();
        	System.out.println("查询到LOMixAgent数据");
        }
        mLOMixAgentSchema.setGrpContNo(repRiskCode);
               
        mLOMixAgentDB.setSchema(mLOMixAgentSchema);
    	
       if (!mLOMixAgentDB.delete())
       {
           buildError("XqqGrpCrsBL->HeadRisk","删除失败");
           return false;
       }
       
       }else{
    	   System.out.println("团单开始");
    	   mLCGrpContSchema.setGrpContNo(repRiskCode);
           mLCGrpContSchema.setCrs_SaleChnl(null);
           mLCGrpContSchema.setCrs_BussType(null);
           mLCGrpContSchema.setGrpAgentCom(null);
           mLCGrpContSchema.setGrpAgentCode(null);
       	   mLCGrpContSchema.setGrpAgentName(null);
           mLCGrpContSchema.setGrpAgentIDNo(null);
           LCGrpContDB mLCGrpContDB = new LCGrpContDB();
           mLCGrpContDB.setGrpContNo(repRiskCode);
          
          if(mLCGrpContDB.getInfo()){
          	mLCGrpContSchema = mLCGrpContDB.getSchema();
          }
          mLCGrpContSchema.setGrpContNo(repRiskCode);
          mLCGrpContSchema.setCrs_SaleChnl(null);
          mLCGrpContSchema.setCrs_BussType(null);
          mLCGrpContSchema.setGrpAgentCom(null);
          mLCGrpContSchema.setGrpAgentCode(null);
      	  mLCGrpContSchema.setGrpAgentName(null);
          mLCGrpContSchema.setGrpAgentIDNo(null);
          
          mLCGrpContDB.setSchema(mLCGrpContSchema);
          if (!mLCGrpContDB.update())
          {
              buildError("XqqGrpCrsBL->update()","LCGrpContDB更新失败");
              return false;
              }
          
        mLOMixAgentSchema.setGrpContNo(repRiskCode);
      	mLOMixAgentSchema.setContNo("0000");
      	mLOMixAgentSchema.setSaleChnl(Crs_Salechnl);
      	mLOMixAgentSchema.setRemark("集团交叉提数");
      	mLOMixAgentSchema.setStandbyFlag1(null);
      	mLOMixAgentSchema.setStandbyFlag2(null);
      	mLOMixAgentSchema.setOperator("郭忠华");
      	mLOMixAgentSchema.setContType("2");
     	
    	SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm.ss");
     	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
     	Date date = new Date();
     	mLOMixAgentSchema.setMakeDate(date);
     	mLOMixAgentSchema.setMakeTime(sdf1.format(new Date()));
     	mLOMixAgentSchema.setModifyDate(date);
     	mLOMixAgentSchema.setModifyTime(sdf1.format(new Date()));
     	
     	LOMixAgentDB mLOMixAgentDB = new LOMixAgentDB();
     	mLOMixAgentDB.setGrpContNo(repRiskCode);
     	mLOMixAgentDB.setContNo("0000");
     	mLOMixAgentDB.setContType("2");
    	if(mLOMixAgentDB.getInfo()){
        	mLOMixAgentSchema = mLOMixAgentDB.getSchema();
        	System.out.println("查询到LOMixAgent数据");
        }
        mLOMixAgentSchema.setGrpContNo(repRiskCode);
               
        mLOMixAgentDB.setSchema(mLOMixAgentSchema);
         if (!mLOMixAgentDB.delete())
         {
             buildError("XqqGrpCrsBL->HeadRisk","删除失败");
             return false;
         }
          
       }
    	 
        return true;
    }
      
}
