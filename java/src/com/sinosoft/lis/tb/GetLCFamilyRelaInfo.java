package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:得到家庭关系
 * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在
 * </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author yuanaq
 * @version 1.0
 */

public class GetLCFamilyRelaInfo
{
    private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    public LCFamilyRelaInfoSet tLCFamilyRelaInfoSet = new LCFamilyRelaInfoSet(); //返回到工作流
    public LCFamilyInfoSet tLCFamilyInfoSet = new LCFamilyInfoSet();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    public GetLCFamilyRelaInfo()
    {
    }

    public static void main(String[] args)
    {
    }

    public boolean GetFamilyRelaInfo(VData cInputData,
                                                 String cOperate)
    {
        String contno = getInputData(cInputData);

        LCContCustomerRelaInfoDB tLCContCustomerRelaInfoDB = new
                LCContCustomerRelaInfoDB();
        String sql = "select *from LCContCustomerRelaInfo where contno='" +
                     contno + "'";
        LCContCustomerRelaInfoSet tLCContCustomerRelaInfoSet = new
                LCContCustomerRelaInfoSet();
        tLCContCustomerRelaInfoSet = tLCContCustomerRelaInfoDB.executeQuery(sql);
        for (int i = 1; i <= tLCContCustomerRelaInfoSet.size(); i++) //循环处理查出的家庭关系信息表的数据
        {
            LCContCustomerRelaInfoSchema tLCContCustomerRelaInfoSchema = new
                    LCContCustomerRelaInfoSchema();
            tLCContCustomerRelaInfoSchema = tLCContCustomerRelaInfoSet.get(i);
            if (!checkiffamily(tLCContCustomerRelaInfoSchema.getCustomerNo())) //如果该用户已经在某家庭中了
            {
                continue;
            }
            else
            {
                if (checkdirectiffamily(tLCContCustomerRelaInfoSchema)) //如果直系亲属在某家庭中，添加到该家庭中去（两层含义1，可能在库里有该家庭2，本次添加的家庭Set中已有）
                {
                    continue; //如果有直系亲属在某家庭就填充到该家庭，并且继续下一次循环
                }
                else //建立家庭，该家庭只有该客户自己
                {
                    LCFamilyRelaInfoSchema tLCFamilyRelaInfoSchema = new
                            LCFamilyRelaInfoSchema();
                    String tNo = PubFun1.CreateMaxNo("FamilyNo", 10);
                    String FamilyStatus = getFamilyStatus(
                            tLCContCustomerRelaInfoSchema);
                    String LevelRela = getLevelRela(
                            tLCContCustomerRelaInfoSchema);
                    tLCFamilyRelaInfoSchema.setFamilyNo(tNo);
                    tLCFamilyRelaInfoSchema.setCustomerNo(
                            tLCContCustomerRelaInfoSchema.getCustomerNo());
                    tLCFamilyRelaInfoSchema.setFamilyStatus(FamilyStatus);
                    tLCFamilyRelaInfoSchema.setLevelRela(LevelRela);
                    tLCFamilyRelaInfoSchema.setRemark("");
                    tLCFamilyRelaInfoSchema.setMakeDate(CurrentDate);
                    tLCFamilyRelaInfoSchema.setMakeTime(CurrentTime);
                    tLCFamilyRelaInfoSchema.setModifyDate(CurrentDate);
                    tLCFamilyRelaInfoSchema.setModifyTime(CurrentTime);
                    tLCFamilyRelaInfoSet.add(tLCFamilyRelaInfoSchema);
                    LCFamilyInfoSchema tLCFamilyInfoSchema=new LCFamilyInfoSchema();//新添加的家庭
                    tLCFamilyInfoSchema.setFamilyNo(tNo);
                    tLCFamilyInfoSchema.setMakeDate(CurrentDate);
                    tLCFamilyInfoSchema.setMakeTime(CurrentTime);
                    tLCFamilyInfoSchema.setModifyDate(CurrentDate);
                    tLCFamilyInfoSchema.setModifyTime(CurrentTime);
                    tLCFamilyInfoSet.add(tLCFamilyInfoSchema);
                }

            }

        }
        mResult.clear();
        mResult.add(tLCFamilyRelaInfoSet);
        mResult.add(tLCFamilyInfoSet);
        return true;
    }

    private String getInputData(VData cInputData)
    {
        String contno = "";
        TransferData mTransferData = new TransferData();
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        contno = (String) mTransferData.getValueByName("ContNo");
        return contno;
    }


    //检查是否在家庭中
    private boolean checkiffamily(String customerno)
    {
        String sql = "select *from LCFamilyRelaInfo where CustomerNo='" +
                     customerno + "'";
        LCFamilyRelaInfoDB tLCFamilyRelaInfoDB = new LCFamilyRelaInfoDB();
        LCFamilyRelaInfoSet tLCFamilyRelaInfoSet = new LCFamilyRelaInfoSet();
        tLCFamilyRelaInfoSet = tLCFamilyRelaInfoDB.executeQuery(sql);
        if (tLCFamilyRelaInfoSet.size() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    //检查直系亲属是否在家庭中,如果有一个在就加入，因为其他直系亲属也肯定在该家庭中
    private boolean checkdirectiffamily(LCContCustomerRelaInfoSchema
                                        mLCContCustomerRelaInfoSchema)
    {
        boolean treturn = false;
        String sql1 = "select *from  LCCustomerRelaInfo where CustomerNo='" +
                      mLCContCustomerRelaInfoSchema.getCustomerNo() + "'"
                      + " and (Relation='01' or Relation='02' or Relation='03' or Relation='04' or Relation='05' or Relation='12')";
        LCCustomerRelaInfoDB tLCCustomerRelaInfoDB = new LCCustomerRelaInfoDB();
        LCCustomerRelaInfoSet tLCCustomerRelaInfoSet = new
                LCCustomerRelaInfoSet();
        tLCCustomerRelaInfoSet = tLCCustomerRelaInfoDB.executeQuery(sql1);
        for (int j = 1; j <= tLCCustomerRelaInfoSet.size(); j++) //循环直系亲属
        {
            LCCustomerRelaInfoSchema tLCCustomerRelaInfoSchema = new
                    LCCustomerRelaInfoSchema();
            tLCCustomerRelaInfoSchema = tLCCustomerRelaInfoSet.get(j);
            String sql = "select *from LCFamilyRelaInfo where CustomerNo='" +
                         tLCCustomerRelaInfoSchema.getRelaCustomerNo() + "'";
            LCFamilyRelaInfoDB mLCFamilyRelaInfoDB = new LCFamilyRelaInfoDB();
            LCFamilyRelaInfoSet mLCFamilyRelaInfoSet = new LCFamilyRelaInfoSet();
            mLCFamilyRelaInfoSet = mLCFamilyRelaInfoDB.executeQuery(sql);
            if (mLCFamilyRelaInfoSet.size() > 0) //直系亲属在家庭中,添加到该家庭中
            { //查询出该条数据后填充新的数据
                //???需要添加客户的判断--即该客户是否允许添加到该家庭中去？
                //判断方式：（1）首先判断婚姻状况
                //         （2）if(非未婚)，判断与直系家属的关系，如果是03或者12的关系就不添加
                if (!getout(mLCContCustomerRelaInfoSchema,
                            tLCCustomerRelaInfoSchema)) //如果被一个家庭剔出来，再去查看是否有别的家庭可以添加
                    continue;
                LCFamilyRelaInfoSchema mLCFamilyRelaInfoSchema = new
                        LCFamilyRelaInfoSchema();
                mLCFamilyRelaInfoSchema = mLCFamilyRelaInfoSet.get(1); //数据待填充,直接复用查询出来的schema，个别子段进行修改
                mLCFamilyRelaInfoSchema.setCustomerNo(
                        mLCContCustomerRelaInfoSchema.getCustomerNo());
                mLCFamilyRelaInfoSchema.setFamilyStatus(getFamilyStatus(
                        mLCContCustomerRelaInfoSchema));
                mLCFamilyRelaInfoSchema.setLevelRela(getFamilyStatus(
                        mLCContCustomerRelaInfoSchema));
                mLCFamilyRelaInfoSchema.setMakeDate(CurrentDate);
                mLCFamilyRelaInfoSchema.setMakeTime(CurrentTime);
                mLCFamilyRelaInfoSchema.setModifyDate(CurrentDate);
                mLCFamilyRelaInfoSchema.setModifyTime(CurrentTime);
                tLCFamilyRelaInfoSet.add(mLCFamilyRelaInfoSchema);
                treturn = true;
                break; //根据直系亲属的关联性，只找一个可以填充到的家庭就可以了
            }
        }
        if (!treturn) //如果在库里没有就在本地检查
        {
            boolean tt = false;
            for (int j = 1; j <= tLCCustomerRelaInfoSet.size(); j++)//循环直系亲属
            {
                LCCustomerRelaInfoSchema tLCCustomerRelaInfoSchema = new
                        LCCustomerRelaInfoSchema();
                tLCCustomerRelaInfoSchema=tLCCustomerRelaInfoSet.get(j);
                if (!getout(mLCContCustomerRelaInfoSchema,
                            tLCCustomerRelaInfoSchema))
                {
                    continue;
                }
                for (int x = 1; x <= tLCFamilyRelaInfoSet.size(); x++)//循环查找直系亲属的家庭
                {
                    LCFamilyRelaInfoSchema tLCFamilyRelaInfoSchema = new
                            LCFamilyRelaInfoSchema();
                    tLCFamilyRelaInfoSchema=tLCFamilyRelaInfoSet.get(x);
                    if (tLCCustomerRelaInfoSchema.getRelaCustomerNo().equals(
                            tLCFamilyRelaInfoSchema.getCustomerNo()))
                    { //数据待填充
                        LCFamilyRelaInfoSchema ttLCFamilyRelaInfoSchema = new
                            LCFamilyRelaInfoSchema();
                        ttLCFamilyRelaInfoSchema.setSchema(tLCFamilyRelaInfoSchema);
                        ttLCFamilyRelaInfoSchema.setCustomerNo(
                                mLCContCustomerRelaInfoSchema.getCustomerNo());
                        ttLCFamilyRelaInfoSchema.setFamilyStatus(getFamilyStatus(
                                mLCContCustomerRelaInfoSchema));
                        ttLCFamilyRelaInfoSchema.setLevelRela(getFamilyStatus(
                                mLCContCustomerRelaInfoSchema));
                        ttLCFamilyRelaInfoSchema.setMakeDate(CurrentDate);
                        ttLCFamilyRelaInfoSchema.setMakeTime(CurrentTime);
                        ttLCFamilyRelaInfoSchema.setModifyDate(CurrentDate);
                        ttLCFamilyRelaInfoSchema.setModifyTime(CurrentTime);
                        tLCFamilyRelaInfoSet.add(ttLCFamilyRelaInfoSchema);

                        treturn = true;
                        tt = true;
                        break;
                    }
                }
                if (tt)
                {
                    break;
                }
            }
        }
        return treturn;
    }


    //如果非未婚并且符合特定关系，剔出该家庭（不作添加操作）
    private boolean getout(LCContCustomerRelaInfoSchema
                           mLCContCustomerRelaInfoSchema,
                           LCCustomerRelaInfoSchema tLCCustomerRelaInfoSchema)
    {
        if(mLCContCustomerRelaInfoSchema.getMarriage()!=null)
        {
            if (mLCContCustomerRelaInfoSchema.getMarriage().equals("Y") &&
                (
                        tLCCustomerRelaInfoSchema.getRelation().equals("03") ||
                        tLCCustomerRelaInfoSchema.getRelation().equals(
                                "12"))) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }


    //推理取得家庭身份
    private String getFamilyStatus(LCContCustomerRelaInfoSchema
                                   mtLCContCustomerRelaInfoSchema) {
        String FamilyStatus = "";
        if (mtLCContCustomerRelaInfoSchema.getMarriage() != null)
        {
            if (mtLCContCustomerRelaInfoSchema.getMarriage().equals("Y") &&
                mtLCContCustomerRelaInfoSchema.getSex().equals("0")) {
                FamilyStatus = "1";
            } else if (mtLCContCustomerRelaInfoSchema.getMarriage().equals("Y") &&
                       mtLCContCustomerRelaInfoSchema.getSex().equals("1")) {
                FamilyStatus = "2";
            } else {
                FamilyStatus = "3";
            }
        }
        else
        {
            FamilyStatus = "3";
        }
        return FamilyStatus;
    }


    //推理取得家庭等级
    private String getLevelRela(LCContCustomerRelaInfoSchema
                                mtLCContCustomerRelaInfoSchema)
    {
        String LevelRela = "";
        if (mtLCContCustomerRelaInfoSchema.getMarriage() != null){
            if (mtLCContCustomerRelaInfoSchema.getMarriage().equals("Y")) {
                LevelRela = "1";
            } else {
                LevelRela = "2";
            }
        }
        else
        {
            LevelRela = "2";
        }
        return LevelRela;
    }
    public VData getResult()
    {
        return this.mResult;
    }

}
