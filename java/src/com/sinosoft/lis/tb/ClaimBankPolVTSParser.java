/**
 * 2012-3-9
 */
package com.sinosoft.lis.tb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

import com.f1j.ss.BookModelImpl;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;

/**
 * @author LY
 *
 */
public class ClaimBankPolVTSParser
{
    public CErrors mErrors = new CErrors();

    private String m_strBatchNo = "";

    // 保存磁盘投保文件的文件名
    private String m_strFileName = "";

    private String m_strPathName = "";

    // 保存列名元素名映射的模板文件的文件名
    private String m_strConfigFileName = "";

    // 保存Sheet相关的信息，如最大行，最大列，当前处理到的行等
    private Hashtable[] m_hashInfo = null;

    // 常量定义
    private static String PROP_MAX_ROW = "max_row";

    private static String PROP_MAX_COL = "max_col";

    private static String PROP_CUR_ROW = "cur_row";

    private static String PROP_COL_NAME = "col_name";

    private static int PROP_FIRSTDATA_ROWID = 2;

    private static int PROP_TITLE_ROWID = 1;

    // excel文件的sheet数量
    private static int SHEET_COUNT = 1;

    // sheet序列号,以及索引所在的列号
    private int SHEET_LCCONT = 0;

    private int STRID_COL_LCCONT = 1;

    // 一个文件中处理的行数
    private static int ROW_LIMIT = 1000;

    private BookModelImpl m_book = new BookModelImpl();

    // 一个excel文件可能被解析成多个XML文件
    // 用来保存每一个小部分生成的XML文件名
    private List m_listFiles = new ArrayList();

    /**
     * 构造函数
     */
    public ClaimBankPolVTSParser()
    {
        m_hashInfo = new Hashtable[SHEET_COUNT];

        for (int nIndex = 0; nIndex < SHEET_COUNT; nIndex++)
        {
            m_hashInfo[nIndex] = new Hashtable();
            m_hashInfo[nIndex].put(PROP_CUR_ROW, "2");
            // 从第一行开始解析
        }
    }

    /**
     * 设置要解析的文件名
     */
    public boolean setFileName(String strFileName)
    {
        File file = new File(strFileName);
        if (!file.exists())
        {
            buildError("setFileName", "指定的文件不存在！");
            return false;
        }

        m_strFileName = strFileName;
        m_strPathName = file.getParent();

        int nPos = strFileName.lastIndexOf('.');

        if (nPos == -1)
        {
            nPos = strFileName.length();
        }

        m_strBatchNo = strFileName.substring(strFileName.lastIndexOf("/") + 1, nPos);

        nPos = m_strBatchNo.lastIndexOf('\\');

        if (nPos != -1)
        {
            m_strBatchNo = m_strBatchNo.substring(nPos + 1);
        }

        nPos = m_strBatchNo.lastIndexOf('/');

        if (nPos != -1)
        {
            m_strBatchNo = m_strBatchNo.substring(nPos + 1);
        }

        return true;
    }

    /**
     * 取得要处理的文件名
     */
    public String getFileName()
    {
        return m_strFileName;
    }

    /**
     * 设置列名映射模板文件名
     */
    public boolean setConfigFileName(String strConfigFileName)
    {
        File file = new File(strConfigFileName);
        if (!file.exists())
        {
            buildError("setFileName", "指定的文件不存在！");
            return false;
        }

        m_strConfigFileName = strConfigFileName;
        return true;
    }

    /**
     * 取得列名映射模板文件名
     */
    public String getConfigFileName()
    {
        return m_strConfigFileName;
    }

    /**
     * 取得生成的XML文件名列表
     */
    public String[] getDataFiles()
    {
        String[] files = new String[m_listFiles.size()];

        for (int nIndex = 0; nIndex < m_listFiles.size(); nIndex++)
        {
            files[nIndex] = (String) m_listFiles.get(nIndex);
        }

        return files;
    }

    /**
     *  将磁盘投保文件转换成指定格式的XML文件。
     * @return boolean
     */
    public boolean transform()
    {

        String strFileName = "";
        int nCount = 0;

        try
        {
            verify();

            while (hasMoreData())
            {
                strFileName = m_strPathName + File.separator + m_strBatchNo + String.valueOf(nCount++) + ".xml";
                System.out.println("strFileName:" + strFileName);
                genPart(strFileName);

                m_listFiles.add(strFileName);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("transfrom", ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 校验磁盘投保excel文件格式
     * 要求磁盘投保文件中的数据按照唯一标识，从小到大排列
     */
    private void verify() throws Exception
    {
        if (m_strFileName.equals(""))
        {
            throw new Exception("请先调用setFileName函数设置要处理的文件名。");
        }

        m_book.read(m_strFileName, new com.f1j.ss.ReadParams());
        if (m_book.getNumSheets() < SHEET_COUNT)
        {
            throw new Exception("磁盘投保文件不完整，缺少Sheet。");
        }
        int nMaxID = -1;
        int nID = -1;
        String strID = "";
        // 检查数据是否是按“唯一号”排序的
        for (int nIndex = 0; nIndex < SHEET_COUNT; nIndex++)
        {

            nMaxID = -1;

            for (int nRow = PROP_FIRSTDATA_ROWID; nRow < getMaxRow(nIndex); nRow++)
            {
                strID = m_book.getText(nIndex, nRow, 0);
                nID = Integer.parseInt(strID);
                if (nID > nMaxID)
                {
                    nMaxID = nID;
                }
                else if (nID == nMaxID)
                {
                    // do nothing
                }
                else
                {
                    throw new Exception("投保文件中的数据不是按照唯一号从小到大排序的请检查Sheet" + (nIndex + 1) + ",第" + nID + "行数据是否录入有误！");
                }
                /** 不能存在 '  字符,因此查看姓名,地址,公司名,等信息替换为全角字符*/
                //replaceAll(nIndex, nRow);
            }
        }

        // 将第一行中元素的值设为对应的XML元素的名字
        // 如在Sheet0中，每行的第一列对应的XML元素名字为ID。
        DOMBuilder db = new DOMBuilder();
        Document doc = db.build(new FileInputStream(m_strConfigFileName));
        Element eleRoot = doc.getRootElement();
        Element ele = null;
        String strColName = "";

        for (int nIndex = 0; nIndex < SHEET_COUNT; nIndex++)
        {
            String sheetId = "Sheet" + String.valueOf(nIndex + 1);
            try
            {
                ele = eleRoot.getChild(sheetId);
                int nMaxCol = getMaxCol(nIndex);
                String[] strArr = new String[nMaxCol];
                //读取列名映射模板文件中的列名
                for (int nCol = 0; nCol < nMaxCol; nCol++)
                {
                    strColName = ele.getChildText("COL" + String.valueOf(nCol));
                    strArr[nCol] = strColName;
                }

                setPropArray(nIndex, PROP_COL_NAME, strArr);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();

                throw new Exception("找不到对应的配置信息，Sheet" + String.valueOf(nIndex + 1));
            }

        }
    }

    /**
     * 执行一次生成，将ROW_LIMIT行数的VTS数据生成一个Document
     * @param strFileName String
     * @throws Exception
     */
    private void genPart(String strFileName) throws Exception
    {
        Element root = new Element("DATASET");
        Document doc = new Document(root);

        Element ele = new Element("BATCHNO");
        ele.setText(m_strBatchNo);
        root.addContent(ele);

        ele = new Element("CONTTABLE");
        root.addContent(ele);
        genXMLPart(ele, this.SHEET_LCCONT);

        XMLOutputter xo = new XMLOutputter("  ", true, "UTF-8");
        xo.output(doc, new FileOutputStream(strFileName));
    }

    /**
     * 判断是否还有数据没有处理-以险种保单sheet为准
     * 如果在存放保单信息的Sheet中，已经处理到了最大行，返回false；
     * 否则，返回true;
     * @return
     */
    private boolean hasMoreData()
    {
        int nCurRow = getPropNumber(0, PROP_CUR_ROW);
        int nMaxRow = getPropNumber(0, PROP_MAX_ROW);

        if (nCurRow >= nMaxRow)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * 获取指定sheet的最大行数
     * @param nSheetIndex int
     */
    private int getMaxRow(int nSheetIndex) throws Exception
    {
        String str = "";
        int nMaxRow = getPropNumber(nSheetIndex, PROP_MAX_ROW);

        if (nMaxRow == -1)
        {
            for (nMaxRow = 0; nMaxRow < m_book.getMaxRow(); nMaxRow++)
            {
                str = m_book.getText(nSheetIndex, nMaxRow, 0);
                if (str == null || str.trim().equals(""))
                {
                    break;
                }
            }
            setPropNumber(nSheetIndex, PROP_MAX_ROW, nMaxRow);
        }

        return nMaxRow;
    }

    /**
     * 获取指定sheet的最大列数
     * @param nSheetIndex int
     */
    private int getMaxCol(int nSheetIndex) throws Exception
    {
        String str = "";
        int nMaxCol = getPropNumber(nSheetIndex, PROP_MAX_COL);

        if (nMaxCol == -1)
        {
            for (nMaxCol = 0; nMaxCol < m_book.getMaxRow(); nMaxCol++)
            {
                str = m_book.getText(nSheetIndex, PROP_TITLE_ROWID, nMaxCol);
                if (str == null || str.trim().equals(""))
                {
                    break;
                }
            }

            setPropNumber(nSheetIndex, PROP_MAX_COL, nMaxCol);
        }
        return nMaxCol;
    }

    /**
     * 获取指定sheet相关参数
     * @param nSheetIndex int
     * @param strPropName String
     */
    private int getPropNumber(int nSheetIndex, String strPropName)
    {
        Hashtable hash = m_hashInfo[nSheetIndex];
        String str = (String) hash.get(strPropName);
        if (str != null && !str.equals(""))
        {
            return Integer.parseInt(str);
        }
        else
        {
            return -1;
        }
    }

    /**
     * 设置指定sheet相关参数
     * @param nSheetIndex int
     * @param strPropName String
     * @param nValue int
     */
    /**
     * @param nSheetIndex
     * @param strPropName
     * @param nValue
     */
    private void setPropNumber(int nSheetIndex, String strPropName, int nValue)
    {
        Hashtable hash = m_hashInfo[nSheetIndex];
        hash.put(strPropName, String.valueOf(nValue));
    }

    /**
     * 获取指定sheet相关参数数组
     * @param nSheetIndex int
     * @param strPropName String
     * @return String[]
     */
    private String[] getPropArray(int nSheetIndex, String strPropName)
    {
        Hashtable hash = m_hashInfo[nSheetIndex];
        String[] strArr = (String[]) hash.get(strPropName);
        return strArr;
    }

    /**
     * 设置指定sheet相关参数
     * @param nSheetIndex int
     * @param strPropName String
     * @param strArr String[]
     */
    private void setPropArray(int nSheetIndex, String strPropName, String[] strArr)
    {
        Hashtable hash = m_hashInfo[nSheetIndex];
        hash.put(strPropName, strArr);
    }

    /**
     * 生成xml主体
     * @param eleParent Element
     * @param sheetNo int
     */
    private void genXMLPart(Element eleParent, int sheetNo) throws Exception
    {

        // 保单信息存放在第一个Sheet中。
        int nCurRow = getPropNumber(sheetNo, PROP_CUR_ROW);
        int nMaxRow = getMaxRow(sheetNo);

        String strID = "";

        int nRow = 0;
        int nCount = 0;

        // 对于有主附险的磁盘导入文件，只是前面的保单信息不同，
        // 后面的投保人，被保险人，责任及受益人等信息都是相同的，不需要每次都扫描。
        // 主附险的唯一号是一样的。
        String strIDCached = "";

        for (nRow = nCurRow, nCount = 0; nRow < nMaxRow; nRow++)
        {
            // 取得险种保单sheet的唯一号信息 - 合同号
            strID = m_book.getText(sheetNo, nRow, this.STRID_COL_LCCONT);
            //nCount控制xml中合同行递增
            //如果超出最小行限制并且合同ID已变 则跳出生成新文件
            //目的是保证同一合同的信息保存在同一文件中
            if (++nCount > ClaimBankPolVTSParser.ROW_LIMIT && !strID.equals(strIDCached))
            {
                break;
            }

            // 如果是同一合同号，后面的信息不用重新生成
            if (!strID.equals(strIDCached))
            {
                strIDCached = strID; // 缓存上一次操作的合同号

                Element eleRow = new Element("ROW");
                eleParent.addContent(eleRow);
                Element eleCached = new Element("CACHED");

                // 每一个保单信息都要重新生成ContId
                Element eleField = new Element("PRTNO");
                eleField.setText(strID);
                eleCached.removeChild("PRTNO");
                eleCached.addContent(eleField);

                // 险种保单信息
                Element elePol = new Element("POLICYTABLE");
                eleCached.removeChild("POLICYTABLE");
                eleCached.addContent(elePol);
                genPartSubTable(elePol, this.SHEET_LCCONT, strID, this.STRID_COL_LCCONT);

                eleRow.setMixedContent(eleCached.getMixedContent());
            } //end if
        } //end for
        setPropNumber(sheetNo, PROP_CUR_ROW, nRow);
    }

    /**
     * 处理子表
     * 因为在Sheet的数据中，是按照“唯一号”ID排序的，所以对子表的数据，
     * 我们也只需要扫描一次就可以了。
     * @param eleParent       父元素
     * @param nSheetIndex     sheet序列号
     * @param strID           合同号
     * @param strIDColIndex   索引列号-合同号所在列号
     * @throws Exception
     */
    private void genPartSubTable(Element eleParent, int nSheetIndex, String strID, int strIDColIndex) throws Exception
    {
        //        int nCurRow = getPropNumber(nSheetIndex, PROP_CUR_ROW);
        int nCurRow = 1;
        int nMaxRow = getMaxRow(nSheetIndex);
        int nMaxCol = getMaxCol(nSheetIndex);
        //获取保存的列名数组
        String[] strColName = getPropArray(nSheetIndex, PROP_COL_NAME);
        int nRow = 0;
        for (nRow = nCurRow; nRow < nMaxRow; nRow++)
        {
            //不是同一合同跳出
            if (!strID.equals(m_book.getText(nSheetIndex, nRow, strIDColIndex)))
            {
                continue;
            }

            //            Element eleRow = new Element("ROW");
            //            eleParent.addContent(eleRow);

            for (int nCol = 0; nCol < nMaxCol; nCol++)
            {
                Element ele = new Element(strColName[nCol]);

                String tStrText = m_book.getText(nSheetIndex, nRow, nCol);
                tStrText = replaceAll(tStrText);

                ele.setText(tStrText);
                eleParent.addContent(ele);
            }

        } //end for

        setPropNumber(nSheetIndex, PROP_CUR_ROW, nRow);
    }

    /**
     * 创建错误信息
     * @param szFunc String 函数名称
     * @param szErrMsg String 错误信息
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "GrpPolVTSParser";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 主要是将 ‘ 替换
     * @param sheetNum int
     * @param rowNum int
     * @param colNum int
     * @param str String
     */
    private String replaceAll(String cText)
    {
        String tChgText = null;

        if (cText != null)
        {
            tChgText = cText;
            tChgText = StrTool.replace(tChgText, "'", "’");
            tChgText = StrTool.replace(tChgText, "(", "（");
            tChgText = StrTool.replace(tChgText, ")", "）");
        }

        return tChgText;
    }
}
