package com.sinosoft.lis.tb;

/**
 * <p>Title: FinBankAddBL</p>
 * <p>Description:添加银行 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : zhangjun
 * @date:2006-04-05
 * @version 1.0
 */
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class LmriskPracticeAddBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
         public CErrors mErrors = new CErrors();
//         private VData mResult = new VData();
         private String RiskCode ;
         private String CodeName;
         private TransferData mAddElement = new TransferData(); //获取时间
         private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
         private LDCodeSchema mLDCodeSchema = new LDCodeSchema();
         
        private String mResult;
    public LmriskPracticeAddBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try{
        if (!cOperate.equals("HEAD") &&!cOperate.equals("REPAIR") && !cOperate.equals("DELETE")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult="";
       
        if (cOperate.equals("HEAD"))
        {
            if (!setRisk()) {
                  return false;
            }

        }
        else if (cOperate.equals("REPAIR"))
        {
            if (!updateRisk()) {
                  return false;
            }

        }
        else if (cOperate.equals("DELETE"))
        {
            if (!delRisk()) {
                  return false;
            }

        }
        }catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LmriskPracticeAddBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理错误! " + e.getMessage();
            this.mErrors .addOneError(tError);
            return false;
          }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) { //打印付费
        //全局变量
        mAddElement = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        RiskCode = (String) mAddElement.getValueByName("RiskCode");
        CodeName = (String) mAddElement.getValueByName("CodeName");
        return true;
    }

    public String getResult() {
        return mResult;
    }
    
    public LDCodeSchema getLDCodeSchema(){
        return mLDCodeSchema;
    }
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LmriskPracticeAddBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean updateRisk()
    {
    	LDCodeDB mLDCodeDB = new LDCodeDB();
    	mLDCodeDB.setCodeType("accidentageamnt");
    	mLDCodeDB.setCode(RiskCode);
        if(mLDCodeDB.getInfo()){
        	mLDCodeSchema = mLDCodeDB.getSchema();
        }
        mLDCodeSchema.setCodeName(CodeName);
               
        mLDCodeDB.setSchema(mLDCodeSchema);
        if (!mLDCodeDB.update())
        {
            buildError("LmriskPracticeAddBL->Risk","更新保额失败");
            return false;
        }
        return true;
    }
    
//  添加总行
    private boolean setRisk()
    {
          //添插入参数
           mLDCodeSchema.setCodeType("accidentageamnt");
           mLDCodeSchema.setCode(RiskCode);
           mLDCodeSchema.setCodeName(CodeName);

           LDCodeDB mLDCodeDB = new LDCodeDB();
           mLDCodeDB.setSchema(mLDCodeSchema);
           if (!mLDCodeDB.insert())
           {
               buildError("LmriskPracticeAddBL->HeadRisk","插入保额失败");
               return false;
           }
           return true;
    }
    
    private boolean delRisk()
    {
        mLDCodeSchema.setCodeType("accidentageamnt");
        mLDCodeSchema.setCode(RiskCode);
        mLDCodeSchema.setCodeName(CodeName);
        LDCodeDB mLDCodeDB = new LDCodeDB();
        mLDCodeDB.setCodeType("accidentageamnt");
        mLDCodeDB.setCode(RiskCode);
        if(mLDCodeDB.getInfo()){
        	mLDCodeSchema = mLDCodeDB.getSchema();
        }
        
        if (!mLDCodeDB.delete())
        {
            buildError("LmriskPracticeAddBL->delRisk","删除保额失败");
            return false;
            }
        
        return true;
    }

    
    
}
