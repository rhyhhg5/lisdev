/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.tb;

import java.sql.Connection;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vdb.LCGrpFeeDBSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;


/**
 * 保障计划数据提交类
 * <p>Title: </p>
 * <p>Description: 根据传入的操作类型，进行新增、删除、修改操作 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author 朱向峰
 * @version 1.0
 */
public class LCAskUWNoteBLS {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 数据操作字符串 */
    private String mOperate;
    public LCAskUWNoteBLS() {
    }

    public static void main(String[] args) {
    }


    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LCAskUWNoteBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN")) {
            tReturn = saveLCAskUWNote(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            tReturn = updateLCAskUWNote(cInputData);
        }
        if (this.mOperate.equals("UPDATE||UWPLAN")) {
            tReturn = updateLCAskUWNote(cInputData);
        }
        if (tReturn)
            System.out.println(" sucessful");
        else
            System.out.println("Save failed");
        System.out.println("End LCAskUWNoteBLS Submit...");
        return tReturn;
    }


    /**
     * 新增处理
     * @param mInputData VData
     * @return boolean
     */
    private boolean saveLCAskUWNote(VData mInputData) {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LCAskUWNoteDBSet tLCAskUWNoteDBSet = new LCAskUWNoteDBSet(conn);
            tLCAskUWNoteDBSet.set((LCAskUWNoteSet) mInputData.
                                  getObjectByObjectName(
                                          "LCAskUWNoteSet", 0));

            LCAskUWNoteDB tLCAskUWNoteDB = new LCAskUWNoteDB(conn);
            tLCAskUWNoteDB.setNoteID(tLCAskUWNoteDBSet.get(1).getNoteID());
            tLCAskUWNoteDB.setGrpContNo(tLCAskUWNoteDBSet.get(1).getGrpContNo());

            LCAskUWNoteSet tLCAskUWNoteSet = tLCAskUWNoteDB.query();
            if (tLCAskUWNoteSet.size() == 0) {
                if (!tLCAskUWNoteDBSet.insert()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAskUWNoteBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {}
        }
        return tReturn;
    }

    /**
     * 修改处理
     * @param mInputData VData
     * @return boolean
     */
    private boolean updateLCAskUWNote(VData mInputData) {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);
            System.out.println("Start 修改...");
            LCAskUWNoteDBSet tLCAskUWNoteDBSet = new LCAskUWNoteDBSet(conn);
            tLCAskUWNoteDBSet.set((LCAskUWNoteSet) mInputData.
                                  getObjectByObjectName(
                                          "LCAskUWNoteSet", 0));

            LCAskUWNoteDB tLCAskUWNoteDB = new LCAskUWNoteDB(conn);
            tLCAskUWNoteDB.setNoteID(tLCAskUWNoteDBSet.get(1).getNoteID());
            tLCAskUWNoteDB.setGrpContNo(tLCAskUWNoteDBSet.get(1).getGrpContNo());

            LCAskUWNoteSet tLCAskUWNoteSet = tLCAskUWNoteDB.query();
            if (tLCAskUWNoteSet.size() != 0) {

                if (!tLCAskUWNoteDBSet.update()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAskUWNoteBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {}
        }
        return tReturn;
    }
}
