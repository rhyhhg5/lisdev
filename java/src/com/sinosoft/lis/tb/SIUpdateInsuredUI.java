package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author  zhangxing
 * @version 1.0
 */

public class SIUpdateInsuredUI {

    private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    public SIUpdateInsuredUI() {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        SIUpdateInsuredBL tSIUpdateInsuredBL = new SIUpdateInsuredBL();
        System.out.println("Start LGrpUpdateInsuredBL Submit...");
        tSIUpdateInsuredBL.submitData(mInputData, cOperate);

        System.out.println("End LGrpUpdateInsuredB Submit...");

        //如果有需要处理的错误，则返回
        if (tSIUpdateInsuredBL.mErrors.needDealError()) {
            mErrors.copyAllErrors(tSIUpdateInsuredBL.mErrors);
            return false;
        }
        mResult.clear();
        mResult = tSIUpdateInsuredBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult() {
        return mResult;
    }


}
