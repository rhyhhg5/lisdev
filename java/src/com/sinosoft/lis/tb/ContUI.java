package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class ContUI {
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    // @Constructor
    public ContUI() {}

    // @Main
    public static void main(String[] args) {
        ContUI tGroupPolUI = new ContUI();
        LCContSchema mLCContSchema = new LCContSchema();
        LCAppntSchema mLCAppntSchema = new LCAppntSchema();
        LDPersonSet mLDPersonSet = new LDPersonSet();
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        LCAddressSchema mLCAddressSchema = new LCAddressSchema();
        LCAccountSchema mLCAccountSchema = new LCAccountSchema();
        // LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
        mLCAddressSchema.setHomeAddress("asdfasdf");
        mLCContSchema.setPrtNo("16000006668");
        mLCContSchema.setContNo("13000228749");
        mLCContSchema.setAgentCode("1101000001");
        mLCContSchema.setAgentGroup("000000000002");
        mLCContSchema.setManageCom("86110000");
        mLCContSchema.setSignCom("86110000");
        mLCContSchema.setPolType("0");
        mLCContSchema.setContType("1");
        mLCContSchema.setCInValiDate("2007-05-31");
        mLCAppntSchema.setAppntNo("000007147");
        mLCAppntSchema.setAppntName("边海峰");
        mLCAppntSchema.setAppntSex("1");
        mLCAppntSchema.setAppntBirthday("1976-7-21");
        mLCAppntSchema.setIDType("0");
        mLCAppntSchema.setIDNo("142223197607213628");
        //mLDPersonSet.add(tLDPersonSchema);

//          LDPersonSchema tLDPersonSchema1=new LDPersonSchema();
//          tLDPersonSchema1.setName("BBBBa");
//          tLDPersonSchema1.setSex("1");
//          tLDPersonSchema1.setBirthday("1979-01-01");
//          tLDPersonSchema1.setIDType("0");
//          tLDPersonSchema1.setIDNo("422103197901010871");
//
//         mLDPersonSet.add(tLDPersonSchema1);
//          LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
//          LCCustomerImpartSchema tLCCustomerImpartSchema =new LCCustomerImpartSchema();
//          tLCCustomerImpartSchema.setImpartCode("010");
//          tLCCustomerImpartSchema.setImpartVer("001");
//          tLCCustomerImpartSchema.setCustomerNoType("A");
//          tLCCustomerImpartSchema.setImpartContent("175,20");
//          tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpNo", "00000003");
        tTransferData.setNameAndValue("GrpName", "东信集团");
        VData tInputData = new VData();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.Operator = "000006";
        //tInputData.add(tLDPersonSchema);
//          tInputData.add(tLCCustomerImpartSet);
        tInputData.add(mLCContSchema);
        tInputData.add(mLCAppntSchema);
        tInputData.add(mLCAddressSchema);
        tInputData.add(tTransferData);
        tInputData.add(mLCAccountSchema);
        //tInputData.add( mLCInsuredSchema);
        tInputData.add(tGI);
        tGroupPolUI.submitData(tInputData, "UPDATE||CONT");
    }

    // @Method
    /**
     * 数据提交方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return: boolean
     **/
    public boolean submitData(VData cInputData, String cOperate) {
        // 数据操作字符串拷贝到本类中
        this.mOperate = cOperate;

        ContBL tContBL = new ContBL();

        System.out.println("---UI BEGIN---");
        if (tContBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tContBL.mErrors);
            return false;
        } else {
            mResult = tContBL.getResult();
        }
        System.out.print(mErrors.toString());
        return true;
    }

    public VData getResult() {
        return mResult;
    }

}
