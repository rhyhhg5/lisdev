/**
 * 2008-1-21
 */
package com.sinosoft.lis.tb;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ModifyIPForScanUI
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    public ModifyIPForScanUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            System.out.println("Start TestUI Submit ...");
            ModifyIPForScanBL tModifyBL = new ModifyIPForScanBL();

            if (!tModifyBL.submitData(cInputData, cOperate))
            {
                if (tModifyBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tModifyBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData", "TestBL 发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tModifyBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "意外错误");
            return false;
        }
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "TempFeeAppUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
    }

}
