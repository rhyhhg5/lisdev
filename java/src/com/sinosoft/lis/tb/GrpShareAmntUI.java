package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;


public class GrpShareAmntUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    public GrpShareAmntUI() {}


    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        GrpShareAmntBL tGrpShareAmntBL = new GrpShareAmntBL();

        System.out.println("---GrpShareAmntBL UI BEGIN---");
        if (tGrpShareAmntBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpShareAmntBL.mErrors);
            mResult.clear();
            return false;
        }
        mResult = tGrpShareAmntBL.getResult();
        return true;
    }

    /**
     * 传输数据的公共方法
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
