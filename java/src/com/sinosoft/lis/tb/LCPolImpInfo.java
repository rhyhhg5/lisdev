/*
 * @(#)LCPolImpInfo.java	2005-01-17
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import java.util.HashMap;
import java.util.Iterator;



import com.sinosoft.workflowengine.ActivityOperator;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 保存及填充个单磁盘投保过程中需要的一些信息 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author zhangtao
 * @version 1.0
 * @date 2005-01-17
 */
public class LCPolImpInfo {
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    //集体信息表
    private LDGrpSet m_LDGrpSet = new LDGrpSet();
    private String m_strBatchNo = "";

    private LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
    private LCBnfSet mLCBnfSet = new LCBnfSet();
    private LCInsuredRelatedSet mLCInsuredRelatedSet = new LCInsuredRelatedSet();

    //保存excel导入的原始数据
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
    private LCAppntSet mLCAppntSet = new LCAppntSet();
    private LCAddressSet mLCInsuredAddressSet = new LCAddressSet();
    private LCAddressSet mLCAppntAddressSet = new LCAddressSet();
    private LCCustomerImpartDetailSet
            mLCCustomerImpartDetailSet = new LCCustomerImpartDetailSet();

    //保存经过创建或系统查询出来后的信息
    private HashMap mInsuredMap = new HashMap();
    private HashMap mAppntMap = new HashMap();
    private HashMap mMainPolMap = new HashMap();

    private HashMap mContCache = new HashMap();
    private HashMap mContPolData = new HashMap();

    private HashMap m_LMRiskAppMap = new HashMap();
    private HashMap m_LMRiskMap = new HashMap();

    private static final String STATIC_GRPCONTNO = "00000000000000000000";
    //活动结点创建参数
    private static final String STATIC_PROCESSID = "0000000003";
    private static final String STATIC_ACTIVITYID = "0000001097";

    public LCPolImpInfo() {
        //  reset();
    }


    /**
     * 通过个单磁盘投保文件中解析的数据初始化一些信息
     * @param strBatchNo String
     * @param GlobalInput GlobalInput
     * @param VData tInsuredVData
     * @param VData tAppntVData
     * @param tLCBnfSet LCBnfSet
     * @return boolean
     */
    public boolean init(String strBatchNo,
                        GlobalInput GlobalInput,
                        LCContSchema tLCContSchema,
                        VData tInsuredVData,
                        VData tAppntVData,
                        LCBnfSet tLCBnfSet,
                        LCCustomerImpartDetailSet tLCCustomerImpartDetailSet) {

        if (strBatchNo == null || strBatchNo.equals("")) {
            buildError("init", "传入非法的磁盘投保批次号");
            return false;
        }

        m_strBatchNo = strBatchNo;

        //清空
        mContCache.clear();
        mInsuredMap.clear();
        mMainPolMap.clear();
        mAppntMap.clear();

        LCInsuredSet tLCInsuredSet = (LCInsuredSet)tInsuredVData.getObjectByObjectName("LCInsuredSet", 0);

        LCAppntSet tLCAppntSet = (LCAppntSet)tAppntVData.getObjectByObjectName("LCAppntSet",0);

        LCAddressSet tLCInsuredAddressSet = (LCAddressSet)tInsuredVData.getObjectByObjectName("LCAddressSet", 0);

        LCAddressSet tLCAppntAddressSet = (LCAddressSet)tAppntVData.getObjectByObjectName("LCAddressSet", 0);

        mGlobalInput = GlobalInput;
        mLCContSchema = tLCContSchema;
        mLCBnfSet = tLCBnfSet;
        mLCAppntSet = tLCAppntSet;
        mLCAppntAddressSet = tLCAppntAddressSet;
        mLCInsuredSet = tLCInsuredSet;
        mLCInsuredAddressSet = tLCInsuredAddressSet;
        mLCCustomerImpartDetailSet = tLCCustomerImpartDetailSet;

        String tempContId = "";
        for (int i = 1; i <= tLCInsuredSet.size(); i++) {
            tempContId = tLCInsuredSet.get(i).getContNo();
            if ("".equals(tempContId)) {
                buildError("init", "被保人信息中有没有填写的合同号!");
                return false;
            }
            if (!mContCache.containsKey(tempContId)) {
                mContCache.put(tempContId, null);
            }
        }

        return true;

    }

    /**
     * 取得excel文件提交的被保人信息
     * @return LCInsuredSet
     */
    public LCInsuredSet getLCInsuredSet() {
        return this.mLCInsuredSet;
    }

    /**
     * 根据合同ID,主被保险人ID,险种代码查找连身被保险人信息
     * @param contId String
     * @param mainInsured String
     * @param riskcode String
     * @return String[]
     */
    public String[] getInsuredRela(String contId, String mainInsured,
                                   String riskcode) {
        StringBuffer sb = null;
        for (int i = 1; i <= mLCInsuredRelatedSet.size(); i++) {
            if (contId.equals(mLCInsuredRelatedSet.get(i).getOperator())
                &&
                mainInsured.equals(mLCInsuredRelatedSet.get(i).
                                   getMainCustomerNo()) //借用段
                && riskcode.equals(mLCInsuredRelatedSet.get(i).getPolNo())) {
                if (sb == null) {
                    sb = new StringBuffer();

                } else {
                    sb.append(",");
                }
                sb.append(mLCInsuredRelatedSet.get(i).getCustomerNo());
            }

        }
        if (sb != null) {
            return sb.toString().split(",");
        }
        return null;
    }

    /**
     * 通过险种ID从excel提交的数据中查找受益人信息
     * @param strPolId String  险种ID
     * @return LCBnfSet
     */
    public LCBnfSet findBnfSet(String strPolId) {
        LCBnfSet tBnfSet = new LCBnfSet();
        LCBnfSchema tSchema = null;
        for (int i = 1; i <= mLCBnfSet.size(); i++) {
            tSchema = mLCBnfSet.get(i);
            if (strPolId.equals(tSchema.getPolNo())) {
                LCBnfSchema newSchema = new LCBnfSchema();
                newSchema.setSchema(tSchema);
                tBnfSet.add(newSchema);
            }
        }
        return tBnfSet;

    }

    /**
     * 通过合同ID从excel提交的数据中查找受益人信息
     * @param strPolId String  险种ID
     * @return LCBnfSet
     */
    public LCBnfSet findContBnfSet(String strContId,String InsuredId) {
        LCBnfSet tBnfSet = new LCBnfSet();
        LCBnfSchema tSchema = null;
        for (int i = 1; mLCBnfSet != null && i <= mLCBnfSet.size(); i++) {
            tSchema = mLCBnfSet.get(i);
            if (strContId.equals(tSchema.getContNo())&&
                 InsuredId.equals(tSchema.getInsuredNo())) {
                LCBnfSchema newSchema = new LCBnfSchema();
                newSchema.setSchema(tSchema);
                tBnfSet.add(newSchema);
            }
        }
        return tBnfSet;

    }

    /**
     * 通过合同ID从excel提交的数据中查找客户告知信息
     * @param strContId String  合同ID
     * @return LCCustomerImpartDetailSet
     */
    public LCCustomerImpartDetailSet findImpartDetailSet(String strContId) {
        LCCustomerImpartDetailSet
                tImpartDetailSet = new LCCustomerImpartDetailSet();
        LCCustomerImpartDetailSchema tSchema = null;
        for (int i = 1; mLCCustomerImpartDetailSet != null && i <= mLCCustomerImpartDetailSet.size(); i++) {
            tSchema = mLCCustomerImpartDetailSet.get(i);
            if (strContId.equals(tSchema.getContNo())) {
                LCCustomerImpartDetailSchema newSchema = new LCCustomerImpartDetailSchema();
                newSchema.setSchema(tSchema);
                tImpartDetailSet.add(newSchema);
            }
        }
        return tImpartDetailSet;
    }

    /**
     * 生成险种保单索引
     * @param contId String
     * @param insuredId String
     * @param riskcode String
     * @return String
     */
    public String getPolKey(String contId, String insuredId, String riskcode) {
        return contId + "-" + insuredId + "-" + riskcode;
    }

//    /**
//     * 格式化险种保单信息
//     * 合同和被保人没有创建的需要创建
//     * @param tLCPolSchema  险种信息
//     * @param strID  险种ID
//     * @return
//     */
//    public VData formatLCPol(LCPolSchema tLCPolSchema,
//                             String strID)
//    {
//        VData tReturnData = new VData();
//        LCInsuredSchema tLCInsuredSchema = null;
//
//        if (tLCPolSchema.getCValiDate() == null ||
//            "".equals(tLCPolSchema.getCValiDate()))
//        {
//            //excel页面数据中没有指定生效日期
//            tLCPolSchema.setSpecifyValiDate("0");
//        }
//        else
//        {
//            tLCPolSchema.setSpecifyValiDate("1");
//        }
//
//        //主险ID
//        String mainPolId = StrTool.cTrim(tLCPolSchema.getMainPolNo());
//
//        if (!"".equals(mainPolId) && !strID.equals(mainPolId))
//        {
//            //为附加险，则查找主险 (主险是在附险之前创建)
//            LCPolSchema tMainLCPolSchema = findCacheLCPolSchema(mainPolId);
//            if (tMainLCPolSchema == null)
//            {
//                //如果从导入轨迹中没有找到主险
//                buildError("formatLCPol",
//                           "数据库和导入轨迹中找不到附加险(" +
//                           strID + ")对应的个人主险保单(" + mainPolId + ")纪录");
//                return null;
//            }
//
//            tLCPolSchema.setMainPolNo(tMainLCPolSchema.getPolNo());
//            tLCPolSchema.setContNo(tMainLCPolSchema.getContNo().trim());
//            tLCPolSchema.setInsuredNo(tMainLCPolSchema.getInsuredNo().trim());
//            tLCPolSchema.setAppntNo(tMainLCPolSchema.getAppntNo().trim());
//
//            String insuredIndex = tLCPolSchema.getInsuredNo();
//            tLCInsuredSchema = findInsuredfromCache(insuredIndex);
//            if (tLCInsuredSchema == null)
//            {
//                mErrors.clearErrors();
//                buildError("formatLCPol",
//                           "新增附加险:找不到对应主险的被保人信息,ID=" +
//                           insuredIndex);
//                return null;
//            }
//
//        }
//        else
//        {
//            //本身为主险保单,设置主险保单号空
//            tLCPolSchema.setMainPolNo("");
//
//            tLCInsuredSchema = findInsuredfromCache(
//                                        tLCPolSchema.getInsuredNo());
//            if (tLCInsuredSchema == null)
//            {
//                //创建被保险人和合同
//                VData tData = getContData(tLCPolSchema);
//                if (tData == null)
//                {
//                    return null;
//                }
//
//                MMap map = (MMap) tData.getObjectByObjectName("MMap", 0);
//                if (map != null && map.keySet().size() > 0)
//                {
//                    tReturnData.add(map);
//                }
//
//                tLCInsuredSchema = (LCInsuredSchema)
//                                   tData.getObjectByObjectName(
//                                                    "LCInsuredSchema", 0);
//            }
//
//            tLCPolSchema.setContNo(tLCInsuredSchema.getContNo().trim());
//            tLCPolSchema.setPrtNo(tLCInsuredSchema.getPrtNo().trim());
//            tLCPolSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
//            tLCPolSchema.setInsuredName(tLCInsuredSchema.getName());
//            tLCPolSchema.setInsuredSex(tLCInsuredSchema.getSex());
//            tLCPolSchema.setInsuredBirthday(tLCInsuredSchema.getBirthday());
//            tLCPolSchema.setOccupationType(tLCInsuredSchema.getOccupationType());
//
//        }
//
//        tReturnData.add(tLCPolSchema);
//        return tReturnData;
//    }


    /**
     * 通过计划附加险查找主险Id
     * @param contId String
     * @param insuredId String
     * @param riskcode String
     * @return String
     */
    public String getMainRiskCode(String planId, String riskcode) {
        LCContPlanRiskSchema schema = null;
        String mainRiskCode = "";
        for (int i = 1; i <= this.mLCContPlanRiskSet.size(); i++) {
            schema = mLCContPlanRiskSet.get(i);
            if (schema.getContPlanCode().equals(planId)
                && schema.getRiskCode().equals(riskcode)) {
                if (schema.getMainRiskCode().equals("000000")
                    || "".equals(StrTool.cTrim(schema.getMainRiskCode()))) {
                    mainRiskCode = riskcode;
                } else {
                    mainRiskCode = schema.getMainRiskCode();
                }
                break;
            }
        }
        return mainRiskCode;
    }

    /**
     * 根据投保人ID从投保人列表中查找投保人
     * 没有创建的需要创建
     * @param appntNo String
     * @return LCAppntSchema
     */
    public LCAppntSchema prepareAppnt(String appntID) {
        LCAppntSchema tSchema = findAppntfromCache(appntID);
        if (tSchema == null) {
            //需要创建新的投保人
            tSchema = createAppnt(appntID);
            if (tSchema == null) {
                CError.buildErr(this, "在准备投保人[" +
                                appntID + "]的时候出现错误");
                return null;
            }
        }

        return tSchema;
    }

    /**
     * 杨明添加,用于在个人磁盘导入时
     * 填充投保人的帐户信息
     * @param appntID String
     * @return LCAppntSchema
     */
    public LCAppntSchema prepareAppntAccInfo(String appntID) {
        LCAppntSchema tSchema = findAppntfromCache(appntID);
        if (tSchema == null) {
            //需要创建新的投保人
            tSchema = findAppnt(appntID);
        }
        return tSchema;
    }

    /**
     * 根据被保人ID从被保人列表中查找被保人
     * 没有创建的需要创建
     * @param insuredID String
     * @return LCInsuredSchema
     */
    public LCInsuredSchema prepareInsured(String insuredID) {
        LCInsuredSchema tSchema = findInsuredfromCache(insuredID);
        if (tSchema == null) {
            //需要创建新的被保人
            tSchema = createInsured(insuredID);
            if (tSchema == null) {
                CError.buildErr(this, "在准备被保人[" +
                                insuredID + "]的时候出现错误");
                return null;
            }
        }

        return tSchema;
    }


    /**
     * 根据连身被保人ID从被保险人列表中查找被保险人
     * 没有创建的需要创建
     * @param mainInsuredNo String
     * @param relaIns String[]
     * @return LCInsuredRelatedSet
     */
    public LCInsuredRelatedSet prepareInsuredRela(String mainInsuredNo,
                                                  String[] relaIns) {
        LCInsuredRelatedSet tLCInsuredRelatedSet = new LCInsuredRelatedSet();

        for (int i = 0; i < relaIns.length; i++) {
            String tmpId = relaIns[i];
            LCInsuredSchema tSchema = this.findInsuredfromCache(tmpId);
            if (tSchema == null) {
                //需要创建新的被保人
                tSchema = createInsured(tmpId);
                if (tSchema == null) {
                    CError.buildErr(this, "在准备连身被保险人[" +
                                    tmpId + "]的时候出现错误");
                    return null;
                }
            }
            if (tSchema != null) {
                LCInsuredRelatedSchema tRelaSchema =
                        new LCInsuredRelatedSchema();

                tRelaSchema.setCustomerNo(tSchema.getInsuredNo());
                tRelaSchema.setMainCustomerNo(mainInsuredNo);
                tRelaSchema.setRelationToInsured(tSchema.
                                                 getRelationToMainInsured());
                tRelaSchema.setAddressNo(tSchema.getAddressNo());
                tRelaSchema.setName(tSchema.getName());
                tRelaSchema.setSex(tSchema.getSex());
                tRelaSchema.setBirthday(tSchema.getBirthday());
                tRelaSchema.setIDType(tSchema.getIDType());
                tRelaSchema.setIDNo(tSchema.getIDNo());

                tRelaSchema.setOperator(mGlobalInput.Operator);
                tRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tRelaSchema.setModifyDate(tRelaSchema.getMakeDate());
                tRelaSchema.setModifyTime(tRelaSchema.getMakeTime());

                tLCInsuredRelatedSet.add(tRelaSchema);
            }

        }
        return tLCInsuredRelatedSet;
    }

    /**
     * 缓存数据准备成功的险种保单信息
     * @param strID String
     * @param aLCPolSchema LCPolSchema
     */
    public void cachePolInfo(String strID, LCPolSchema aLCPolSchema) {
        mMainPolMap.put(strID, aLCPolSchema);
    }

    /**
     * 通过主险保单号查找险种保单
     * @param mainPolNo String
     * @return LCPolSchema
     */
    public LCPolSchema findMainPolSchema(String mainPolNo) {
        Iterator it = mMainPolMap.keySet().iterator();
        LCPolSchema tmpSchema;
        while (it.hasNext()) {
            tmpSchema = (LCPolSchema) mMainPolMap.get(it.next());
            if (tmpSchema.getPolNo().equals(mainPolNo)) {
                return tmpSchema;
            }

        }
        return null;
    }

    /**
     * 缓存创建的合同信息
     * @param strID String
     * @param aLCContSchema LCContSchema
     */
    public void cacheLCContSchema(String strID, LCContSchema aLCContSchema) {
        mContCache.put(strID, aLCContSchema);
    }

    /**
     * 缓存被保险人信息，该被保险人被创建或查询出具体信息
     * @param strID String
     * @param schema LCInsuredSchema
     */
    public void cacheLCInsuredSchema(String strID, LCInsuredSchema schema) {
        mInsuredMap.put(strID, schema);
    }

    /**
     * 缓存投保人信息
     * @param strID String 投保人ID
     * @param schema LCInsuredSchema
     */
    public void cacheLCAppntSchema(String strID, LCAppntSchema schema) {
        mAppntMap.put(strID, schema);
    }

    /**
     * 通过投保人ID查找缓存的投保人信息
     * @param strID String 投保人ID
     * @return LCPolSchema
     */
    public LCAppntSchema findAppntfromCache(String strID) {
        return (LCAppntSchema) mAppntMap.get(strID);
    }


    /**
     * 通过保单险种号码查找缓存的险种保单信息
     * @param strID String 保单险种号码
     * @return LCPolSchema
     */
    public LCPolSchema findCacheLCPolSchema(String strID) {
        return (LCPolSchema)this.mMainPolMap.get(strID);
    }

    /**
     * 通过被保险人ID查找缓存的被保险人
     * @param strID String
     * @return LCInsuredSchema
     */
    public LCInsuredSchema findInsuredfromCache(String strID) {
        return (LCInsuredSchema) mInsuredMap.get(strID);
    }

    /**
     * 通过合同ID查找缓存的合同
     * @param strID String
     * @return LCContSchema
     */
    public LCContSchema findLCContfromCache(String strID) {
        Object dest = mContCache.get(strID);
        if (dest == null) {
            return null;
        }
        if (dest.getClass().getName().equals("java.lang.String")) {
            return null;
        }
        return (LCContSchema) dest;
    }

    /**
     * 根据被保人ID号从excel提交的数据中获取被保险人信息
     * @param index String  被保人ID
     * @return LCInsuredSchema
     */
    private LCInsuredSchema findInsured(String index) {
        for (int i = 1; i <= mLCInsuredSet.size(); i++) {
            //被保人序号被保存在prtNo字段
            if (mLCInsuredSet.get(i).getPrtNo().equals(index)) {
                return mLCInsuredSet.get(i);
            }
        }
        return null;
    }

    /**
     * 根据被保人ID号从excel提交的数据中获取被保险人地址信息
     * @param index String  被保人ID
     * @return LCAddressSchema
     */
    private LCAddressSchema findInsuredAddress(String index) {
        for (int i = 1; i <= mLCInsuredAddressSet.size(); i++) {
            //被保人序号被保存在CustomerNo字段
            if (mLCInsuredAddressSet.get(i).getCustomerNo().equals(index)) {
                return mLCInsuredAddressSet.get(i);
            }
        }
        return null;
    }

    /**
     * 根据投保人ID号从excel提交的数据中获取投保人信息
     * @param index String 投保人ID
     * @return LCAppntSchema
     */
    private LCAppntSchema findAppnt(String index) {
        for (int i = 1; i <= mLCAppntSet.size(); i++) {
            //投保人序号被保存在prtNo字段
            if (mLCAppntSet.get(i).getPrtNo().equals(index)) {
                return mLCAppntSet.get(i);
            }
        }
        return null;
    }

    /**
     * 根据投保人ID号从excel提交的数据中获取投保人地址信息
     * @param index String  投保人ID
     * @return LCAddressSchema
     */
    private LCAddressSchema findAppntAddress(String index) {
        for (int i = 1; i <= mLCAppntAddressSet.size(); i++) {
            //投保人序号被保存在CustomerNo字段
            if (mLCAppntAddressSet.get(i).getCustomerNo().equals(index)) {
                return mLCAppntAddressSet.get(i);
            }
        }
        return null;
    }

    /**
     * 记录失败导入的日志
     * @param batchNo String
     * @param contId String
     * @param InsuredId String
     * @param polId String
     * @param contPlanCode String
     * @param riskCode String
     * @param errInfo CErrors
     * @param globalInput GlobalInput
     */
    public boolean logError(String batchNo,
                            String contId,
                            String insuredId,
                            //String polId,
                            String riskCode,
                            LCInsuredSchema insuredSchema,
                            CErrors errInfo,
                            GlobalInput globalInput) {
        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
        LCGrpImportLogSchema
                delLCGrpImportLogSchema = new LCGrpImportLogSchema();
        // 设置主键
        delLCGrpImportLogSchema.setContID(contId);
        delLCGrpImportLogSchema.setBatchNo(batchNo);

        LCGrpImportLogSchema
                tLCGrpImportLogSchema = new LCGrpImportLogSchema();
        //避免报错
        if (insuredSchema == null) {
            insuredSchema = new LCInsuredSchema();
        }

        // 设置主键
//        tLCGrpImportLogSchema.setIdNo(polId);
        tLCGrpImportLogSchema.setContID(contId);
        tLCGrpImportLogSchema.setBatchNo(batchNo);
        tLCGrpImportLogSchema.setRiskCode(riskCode);
        tLCGrpImportLogSchema.setGrpContNo(STATIC_GRPCONTNO);
        tLCGrpImportLogSchema.setInsuredID(insuredId);
        tLCGrpImportLogSchema.setInsuredNo(insuredSchema.getInsuredNo());
        tLCGrpImportLogSchema.setInsuredName(insuredSchema.getName());
        tLCGrpImportLogSchema.setOperator(globalInput.Operator);
        tLCGrpImportLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpImportLogSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpImportLogSchema.setErrorType("1");
        tLCGrpImportLogSchema.setErrorState("1");
        String errMess = "";
        for (int i = 0; i < errInfo.getErrorCount(); i++) {
            errMess += errInfo.getError(i).errorMessage + ";";
        }

        if ("".equals(errMess)) {
            errMess = "未捕捉到的错误。";
        }
        errMess.replaceAll("\n", "");
        tLCGrpImportLogSchema.setErrorInfo(errMess);

        tLCGrpImportLogDB.setSchema(delLCGrpImportLogSchema);
        boolean res = true;
        res = tLCGrpImportLogDB.delete();
        if (res) {
            tLCGrpImportLogDB.setSchema(tLCGrpImportLogSchema);
            res = tLCGrpImportLogDB.insert();
        }
        return res;
    }
    /**
     * 记录失败导入的日志
     * @param batchNo String
     * @param contId String
     * @param InsuredId String
     * @param polId String
     * @param contPlanCode String
     * @param riskCode String
     * @param errInfo CErrors
     * @param globalInput GlobalInput
     */
    public boolean logError(String batchNo,
                            String contId,
                            String insuredId,
                            String polId,
                            String riskCode,
                            LCInsuredSchema insuredSchema,
                            CErrors errInfo,
                            GlobalInput globalInput,
                            String prtNo) {
        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
        LCGrpImportLogSchema
                delLCGrpImportLogSchema = new LCGrpImportLogSchema();
        // 设置主键
        delLCGrpImportLogSchema.setContID(contId);
        delLCGrpImportLogSchema.setBatchNo(batchNo);

        LCGrpImportLogSchema
                tLCGrpImportLogSchema = new LCGrpImportLogSchema();
        //避免报错
        if (insuredSchema == null) {
            insuredSchema = new LCInsuredSchema();
        }

        // 设置主键
        tLCGrpImportLogSchema.setPrtNo(prtNo);
        tLCGrpImportLogSchema.setIdNo(polId);
        tLCGrpImportLogSchema.setContID(contId);
        tLCGrpImportLogSchema.setBatchNo(batchNo);
        tLCGrpImportLogSchema.setRiskCode(riskCode);
        tLCGrpImportLogSchema.setGrpContNo(STATIC_GRPCONTNO);
        tLCGrpImportLogSchema.setInsuredID(insuredId);
        tLCGrpImportLogSchema.setInsuredNo(insuredSchema.getInsuredNo());
        tLCGrpImportLogSchema.setInsuredName(insuredSchema.getName());
        tLCGrpImportLogSchema.setOperator(globalInput.Operator);
        tLCGrpImportLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpImportLogSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpImportLogSchema.setErrorType("1");
        tLCGrpImportLogSchema.setErrorState("1");
        String errMess = "";
        for (int i = 0; i < errInfo.getErrorCount(); i++) {
            errMess += errInfo.getError(i).errorMessage + ";";
        }

        if ("".equals(errMess)) {
            errMess = "未捕捉到的错误。";
        }
        errMess.replaceAll("\n", "");
        tLCGrpImportLogSchema.setErrorInfo(errMess);

        tLCGrpImportLogDB.setSchema(delLCGrpImportLogSchema);
        boolean res = true;
        res = tLCGrpImportLogDB.delete();
        if (res) {
            tLCGrpImportLogDB.setSchema(tLCGrpImportLogSchema);
            res = tLCGrpImportLogDB.insert();
        }
        return res;
    }


    /**
     * 记录成功导入的信息,待完成
     * @param batchNo String
     * @param contId String
     * @param prtNo String
     * @param contNo String
     * @param InsuredId String
     * @param globalInput GlobalInput
     */
    public MMap logSucc(String batchNo, String contId,
                        String prtNo, String contNo,
                        String insuredId,
                        GlobalInput globalInput) {
        MMap tmpMap = new MMap();
        LCGrpImportLogSchema delLCGrpImportLogSchema = new LCGrpImportLogSchema();

        // 设置主键
        delLCGrpImportLogSchema.setContID(contId);
        delLCGrpImportLogSchema.setBatchNo(batchNo);
        tmpMap.put(delLCGrpImportLogSchema, "DELETE");

        LCGrpImportLogSchema tLCGrpImportLogSchema = new LCGrpImportLogSchema();
        tLCGrpImportLogSchema.setContID(contId);
        tLCGrpImportLogSchema.setBatchNo(batchNo);
        tLCGrpImportLogSchema.setGrpContNo(STATIC_GRPCONTNO);
        tLCGrpImportLogSchema.setContNo(contNo);
        tLCGrpImportLogSchema.setInsuredID(insuredId);
        tLCGrpImportLogSchema.setOperator(globalInput.Operator);
        tLCGrpImportLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpImportLogSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpImportLogSchema.setErrorType("1");
        tLCGrpImportLogSchema.setErrorState("0");
        tLCGrpImportLogSchema.setPrtNo(prtNo);
        tLCGrpImportLogSchema.setErrorInfo("导入成功");
        tmpMap.put(tLCGrpImportLogSchema, "INSERT");

        return tmpMap;
    }

    /**
     * 查询系统中是否已经有改日志信息
     * @param batchNo String
     * @param ContId String
     * @param InsuredId String
     * @param polId String
     * @return boolean
     */
    public LCGrpImportLogSchema getLogInfo(String batchNo, String contId) {
        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
        tLCGrpImportLogDB.setBatchNo(batchNo);
        tLCGrpImportLogDB.setContID(contId);
        boolean bExist = tLCGrpImportLogDB.getInfo();
        if (bExist) {
            return tLCGrpImportLogDB.getSchema();
        }
        return null;
    }

    /**
     * 获取错误信息
     * @return LCGrpImportLogSet
     */
    public LCGrpImportLogSet getErrors() {
        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();

        String strSQL = "SELECT * FROM LCGrpImportLog"
                        + " WHERE BatchNo = '" + m_strBatchNo + "'"
                        + " AND ( ErrorState = '1' OR ErrorInfo IS NOT NULL )"
                        + " ORDER BY TO_NUMBER(IDNo)";

        return tLCGrpImportLogDB.executeQuery(strSQL);
    }

    /**
     * 创建错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "GrpPolImpInfo";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 查找集体单位信息
     * @param GrpNo String
     * @return LDGrpSchema
     */
    public LDGrpSchema findLDGrpSchema(String GrpNo) {
        LDGrpSchema tLDGrpSchema = null;
        for (int nIndex = 0; nIndex < m_LDGrpSet.size(); nIndex++) {
            tLDGrpSchema = m_LDGrpSet.get(nIndex + 1);
            if (tLDGrpSchema.getCustomerNo().equals(GrpNo)) {
                return tLDGrpSchema;
            }

        }
        return null;
    }

    /**
     * 取出excel合同信息
     * @param GrpNo String
     * @return LDGrpSchema
     */
    public LCContSchema getLCContSchema() {
        return mLCContSchema;
    }

    /**
     *获取缓存的合同信息
     * @return HashMap
     */
    public HashMap getContCache() {
        return this.mContCache;
    }

//    /**
//     * 合同信息查找或创建，涉及到被保险人
//     * 当被保险人不存在的时候;创建被保险人，存在则取用
//     * 合同不存在，创建;存在，取用
//     * @param polSchema LCPolSchema
//     * @param insuredSchema LCInsuredSchema
//     * @return boolean
//     */
//    public VData getContData(LCPolSchema tLCPolSchema)
//    {
//        String contId = tLCPolSchema.getContNo();
//        String insuredId = tLCPolSchema.getInsuredNo();
//
//        VData tInputData = new VData();
//        VData tResult = new VData();
//        LCContSchema contSchema = null;
//        LCInsuredSchema tLCInsuerdSchema = null;
//
//        if (contId != null)
//        {
//            contSchema = findLCContfromCache(contId);
//        }
//
//        if (insuredId != null) {
//            tLCInsuerdSchema = findInsuredfromCache(insuredId);
//        }
//
//        if (tLCInsuerdSchema != null &&
//            contSchema != null &&
//            !"".equals(StrTool.cTrim(contSchema.getContNo())))
//        {
//            //合同和被保险人都已经创建，直接返回
//            tResult.add(contSchema);
//            tResult.add(tLCInsuerdSchema);
//            return tResult;
//        }
//
//        Reflections ref = new Reflections();
//        boolean createIns = false;
//        boolean createCont = false;
//
//        if (contSchema == null)
//        {
//            //标识需要创建合同
//            createCont = true;
//        }
//        if (tLCInsuerdSchema == null)
//        {
//            //需要创建被保险人
//            createIns = true;
//            tLCInsuerdSchema = findInsured(insuredId);
//            if (tLCInsuerdSchema == null)
//            {
//                CError.buildErr(this, "导入文件中没有被保险人[" +
//                                insuredId + "]");
//                return null;
//            }
//            if (!createCont)
//            {
//                //合同已经创建
//                tLCInsuerdSchema.setContNo(contSchema.getContNo());
//            }
//        }
//
//        LDPersonSchema tPersonSchema = new LDPersonSchema();
//        ref.transFields(tPersonSchema, tLCInsuerdSchema);
//        tPersonSchema.setCustomerNo(tLCInsuerdSchema.getInsuredNo());
//        LDPersonSchema nPersonSchema = checkInSystemPerson(tPersonSchema);
//        LCInsuredSchema cacheInsuredSchema = new LCInsuredSchema();
//        cacheInsuredSchema.setSchema(tLCInsuerdSchema );
//
//        if (nPersonSchema != null)
//        {
//            tPersonSchema = nPersonSchema;
//            ref.transFields(tLCInsuerdSchema, tPersonSchema);
//            tLCInsuerdSchema.setInsuredNo(nPersonSchema.getCustomerNo());
//        }
//
//        MMap map = new MMap();
//        if (createCont)
//        {
//            //需要创建合同
//            contSchema = new LCContSchema();
//
//            if (!createIns)
//            {
//                //被保险人已经创建
//                contSchema.setInsuredNo(tPersonSchema.getCustomerNo());
//            }
//            else
//            {
//                contSchema.setInsuredNo("");
//            }
//=============================需要修改！！！===================BGN============
//            contSchema.setInsuredSex(tPersonSchema.getSex());
//            contSchema.setInsuredName(tPersonSchema.getName());
//            contSchema.setInsuredBirthday(tPersonSchema.getBirthday());
//            contSchema.setBankAccNo(tLCInsuerdSchema.getBankAccNo());
//            contSchema.setBankCode(tLCInsuerdSchema.getBankCode());
//            contSchema.setAccName(tLCInsuerdSchema.getAccName());
//            contSchema.setContType("2");
//
//            contSchema.setAgentCode(tLCPolSchema.getAgentCode());
//            contSchema.setAgentCom(tLCPolSchema.getAgentCom())；
//            contSchema.setPrtNo(tLCGrpPolSchema.getPrtNo());
//            contSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
//            contSchema.setMakeDate(tLCGrpPolSchema.getMakeDate());
//            contSchema.setMakeTime(tLCGrpPolSchema.getMakeTime());
//            contSchema.setModifyDate(tLCGrpPolSchema.getModifyDate());
//            contSchema.setModifyTime(tLCGrpPolSchema.getModifyTime());
//=============================需要修改！！！====================END===========
//        }
//
//        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
//        tLCInsuredDB.setSchema(tLCInsuerdSchema);
//        tLCInsuredDB.setGrpContNo(STATIC_GRPCONTNO);
//
//        LCCustomerImpartSet impartSet = new LCCustomerImpartSet();
//
//        TransferData transferData = new TransferData();
//        //家庭单类型被缓存在保单类型标记字段
//        transferData.setNameAndValue("FamilyType",
//                                     tLCPolSchema.getPolTypeFlag());
//        transferData.setNameAndValue("ContType", "2");
//        transferData.setNameAndValue("PolTypeFlag","0");
//        transferData.setNameAndValue("InsuredPeoples","1");
//        //为 ContInsuredBL 准备初始化数据
//        tInputData.add(mGlobalInput);
//        tInputData.add(contSchema);
//        tInputData.add(tLCInsuerdSchema);
//        tInputData.add(tLCInsuredDB);
//
//        LCAddressSchema inLCAddressSchema = null;
//        if (insuredId != null) {
//            inLCAddressSchema = findInsuredAddress(insuredId);
//        }
//
//        tInputData.add(inLCAddressSchema);
//        tInputData.add(tPersonSchema);
//        tInputData.add(impartSet);
//        tInputData.add(transferData);
//
//        //提交生成或得到个单合同
//        ContInsuredBL contInsuredBl = new ContInsuredBL();
//        boolean cRes = contInsuredBl.preparesubmitData(tInputData,
//                "INSERT||CONTINSURED");
//        if (!cRes)
//        {
//            this.mErrors.copyAllErrors( contInsuredBl.mErrors );
//            return null;
//        }
//
//        tResult = contInsuredBl.getResult();
//
//        map.add(
//            (MMap) tResult.getObjectByObjectName("MMap", 0));
//
//        LCContSchema rContSchema =
//            (LCContSchema) tResult
//                    .getObjectByObjectName("LCContSchema", 0);
//        LCInsuredSchema rInsureSchema =
//                (LCInsuredSchema) tResult
//                        .getObjectByObjectName("LCInsuredSchema", 0);
//        LCAddressSchema tLCAddressSchema =
//                (LCAddressSchema) tResult
//                        .getObjectByObjectName("LCAddressSchema", 0);
//        if (tLCAddressSchema != null &&
//            tLCAddressSchema.getAddressNo() != null)
//        {
//            map.put(tLCAddressSchema, "INSERT");
//            rInsureSchema.setAddressNo(tLCAddressSchema.getAddressNo());
//        }
//
//        //有些客户信息涉及到实时性，要以excel中的数据为准
//        if (!StrTool.cTrim(cacheInsuredSchema.getOccupationType()).equals(""))
//        {
//            rInsureSchema.setOccupationType(cacheInsuredSchema.
//                                               getOccupationType());
//        }
//
//        if (!StrTool.cTrim(cacheInsuredSchema.getOccupationCode()).equals(""))
//        {
//            rInsureSchema.setOccupationCode(cacheInsuredSchema.
//                                               getOccupationCode());
//        }
//
//        if (!StrTool.cTrim(cacheInsuredSchema.getPluralityType()).equals(""))
//        {
//            rInsureSchema.setPluralityType(cacheInsuredSchema.
//                                                getPluralityType());
//        }
//
//        //缓存新创建的合同和被保人
//        if (createCont)
//        {
//            cacheLCContSchema(contId, rContSchema);
//        }
//        if (createIns)
//        {
//            cacheLCInsuredSchema(insuredId, rInsureSchema);
//        }
//
//        tResult.add(rContSchema);
//        tResult.add(rInsureSchema);
//        if (map != null) tResult.add(map);
//        return tResult;
//    }


    /**
     * 准备受益人信息
     * 当受益人跟主被保险人为同一人，则利用主被保险人的信息
     * 如果不是主被保险人，查找被保险人是否已经创建
     * @param tLCBnfSet LCBnfSet 受益人
     * @param insuredSchema LCInsuredSchema 主被保险人
     * @return LCBnfSet
     */
    public LCBnfSet prepareBnf(LCBnfSet tLCBnfSet,
                               LCInsuredSchema insuredSchema) {
        //校验处理受益人的被保险人信息
        LCBnfSchema tBnfSchema = null;
        for (int t = 1; t <= tLCBnfSet.size(); t++) {
            tBnfSchema = tLCBnfSet.get(t);
            //合同号(与主被保险人一致)
            tBnfSchema.setContNo(insuredSchema.getContNo());

            if ("1".equals(StrTool.cTrim(tBnfSchema.getBnfType())) &&
                "00".equals(StrTool.cTrim(
                        tBnfSchema.getRelationToInsured()))) {
                CError.buildErr(this,
                                "合同[" + tBnfSchema.getContNo() +
                                "]有受益人类型为'死亡受益人'，不能指定为被保人本人");
                return null;
            }

            else if ("00".equals(StrTool.cTrim(
                    tBnfSchema.getRelationToInsured()))) {
                //受益人为主被保险人本人
                tBnfSchema.setInsuredNo(insuredSchema.getInsuredNo());
                tBnfSchema.setCustomerNo(insuredSchema.getInsuredNo());
                tBnfSchema.setName(insuredSchema.getName());
                tBnfSchema.setSex(insuredSchema.getSex());
                tBnfSchema.setBirthday(insuredSchema.getBirthday());
                tBnfSchema.setIDType(insuredSchema.getIDType());
                tBnfSchema.setIDNo(insuredSchema.getIDNo());
            } else {
                //查询受益人对应的被保险人
                LCInsuredSchema tInsuredSchema =
                        findInsuredfromCache(
                                tBnfSchema.getInsuredNo());
                if (tInsuredSchema == null) {
                    CError.buildErr(this,
                                    "受益人信息没有查找到对应的被保险人[" +
                                    tBnfSchema.getInsuredNo() +
                                    "]信息");
                    return null;
                }

                //被保险人客户号
                tBnfSchema.setInsuredNo(tInsuredSchema.getInsuredNo());

                //查看系统中是否需要创建新的客户
//                LDPersonSchema person = checkInSystem_Bnf(tBnfSchema);
//                if (person == null) {
//                    CError.buildErr(this, "受益人信息创建失败");
//                    return null;
//                }
//
//                //受益人客户号
//                tBnfSchema.setCustomerNo(person.getCustomerNo());
            }

            if ("".equals(tBnfSchema.getBnfGrade())) {
                //如果受益人级别为空，则取默认值
                tBnfSchema.setBnfGrade("1");
            }
            if (tBnfSchema.getBnfLot() == 0) {
                //如果受益份额为空，则取默认值
                tBnfSchema.setBnfLot("1");
            }
            /** 受益人份额 */
            if(tBnfSchema.getBnfLot()>1){
                tBnfSchema.setBnfLot(tBnfSchema.getBnfLot()/100);
            }
            tBnfSchema.setOperator(mGlobalInput.Operator);
            tBnfSchema.setMakeDate(PubFun.getCurrentDate());
            tBnfSchema.setMakeTime(PubFun.getCurrentTime());
            tBnfSchema.setModifyDate(tBnfSchema.getMakeDate());
            tBnfSchema.setModifyTime(tBnfSchema.getMakeTime());
        }

        return tLCBnfSet;
    }

    /**
     *  查询Person是否已经存在于系统,不存在则创建LDPerson
     * @param schema LDPersonSchema
     * @return LDPersonSchema
     */
    private LDPersonSchema checkInSystemPerson(LDPersonSchema schema) {
        LDPersonDB tdb = new LDPersonDB();
        LDPersonSet iSet = null;
        if (!"".equals(StrTool.cTrim(schema.getCustomerNo()))) {
            tdb = new LDPersonDB();
            tdb.setCustomerNo(schema.getCustomerNo());
            iSet = tdb.query();
        }

        if (iSet != null && iSet.size() >= 0) {
            return iSet.get(iSet.size());
        }

        boolean err = false;
        if ("".equals(StrTool.cTrim(schema.getName()))) {
            CError.buildErr(this, "客户姓名不能为空！");
            err = true;
        }
        if (!"".equals(StrTool.cTrim(schema.getSex())) &&
            !"".equals(StrTool.cTrim(schema.getBirthday())) &&
            !"".equals(StrTool.cTrim(schema.getIDType())) &&
            !"".equals(StrTool.cTrim(schema.getIDNo()))) {
            tdb.setIDNo(schema.getIDNo());
            tdb.setIDType(schema.getIDType());
            tdb.setBirthday(schema.getBirthday());
            tdb.setName(schema.getName());
            tdb.setSex(schema.getSex());

            iSet = tdb.query();
            //查询到，返回
            if (!tdb.mErrors.needDealError() &&
                iSet != null &&
                iSet.size() > 0) {
                return iSet.get(iSet.size());
            }
        }

        return schema;
    }

    /**
     * 查询或创建受益人信息
     * 不在系统客户列表中的则创建新客户
     * @param schema LCInsuredSchema
     * @return LCInsuredSchema
     */

    private LDPersonSchema checkInSystem_Bnf(LCBnfSchema schema) {
        LDPersonDB tdb = new LDPersonDB();
        LDPersonSchema tPerson = null;
        LDPersonSet iSet = null;

        if (!"".equals(StrTool.cTrim(schema.getCustomerNo()))) {
            //根据客户号查询
            tdb = new LDPersonDB();
            tdb.setCustomerNo(schema.getCustomerNo());
            iSet = tdb.query();
        }

        if (iSet == null) {
            //没有客户号或者用客户号没有查询到,则用客户关键属性查询
            if ("".equals(StrTool.cTrim(schema.getName()))) {
                CError.buildErr(this, "客户姓名不能为空！");
                return null;
            }
            if ("".equals(StrTool.cTrim(schema.getSex()))) {
                CError.buildErr(this, "客户性别不能为空！");
                return null;

            }
//            if ("".equals(StrTool.cTrim(schema.getBirthday())))
//            {
//                CError.buildErr(this, "客户生日不能为空！");
//                return null;
//            }

            tdb.setIDNo(schema.getIDNo());
            tdb.setIDType(schema.getIDType());
            /** 如果证件类型是身份证,则从身份证号码中提取生日信息 */
            if (StrTool.cTrim(schema.getIDType()).equals("0") &&
                !StrTool.cTrim(schema.getIDNo()).equals("")) {
                tdb.setBirthday(PubFun.getBirthdayFromId(schema.getIDNo()));
            }
            tdb.setName(schema.getName());
            tdb.setSex(schema.getSex());
            tdb.setBirthday(schema.getBirthday());

            iSet = tdb.query();
        }

        if (!tdb.mErrors.needDealError() && iSet != null && iSet.size() > 0) {
            //系统中存在该客户
            tPerson = iSet.get(iSet.size());
        } else {
            //需要创建客户
            tPerson = new LDPersonSchema();

            String nolimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
            String customerno = PubFun1.CreateMaxNo("CustomerNo", nolimit);

            tPerson.setCustomerNo(customerno);
            tPerson.setName(schema.getName());
            tPerson.setSex(schema.getSex());
            tPerson.setBirthday(schema.getBirthday());
            tPerson.setIDType(schema.getIDType());
            tPerson.setIDNo(schema.getIDNo());
            /** 如果证件类型是身份证,则从身份证号码中提取生日信息 */
            if (StrTool.cTrim(schema.getIDType()).equals("0") &&
                !StrTool.cTrim(schema.getIDNo()).equals("")) {
                tdb.setBirthday(PubFun.getBirthdayFromId(schema.getIDNo()));
            }
            tPerson.setOperator(mGlobalInput.Operator);
            tPerson.setMakeDate(PubFun.getCurrentDate());
            tPerson.setMakeTime(PubFun.getCurrentTime());
            tPerson.setModifyDate(tPerson.getMakeDate());
            tPerson.setModifyTime(tPerson.getMakeTime());

            tdb.setSchema(tPerson);
            if (!tdb.insert()) {
                CError.buildErr(this, "创建客户时出错0:", tdb.mErrors);
                return null;
            }
        }

        return tPerson;
    }

    /**
     * 查询或创建被保人信息
     * 不在系统客户列表中的需要创建客户
     * @param insuredId String
     * @return VData
     */
    public LCInsuredSchema createInsured(String insuredId) {
        LDPersonDB tdb = new LDPersonDB();
        LDPersonSchema tPerson = null;
        Reflections ref = new Reflections();

        //从excel导入数据中取得被保人信息
        LCInsuredSchema excelSchema = this.findInsured(insuredId);
        if (excelSchema == null) {
            CError.buildErr(this,"创建被保险人时未从导入数据中查找到被保险人[" + insuredId + "]");
            return null;
        }

        //拷贝一份
        LCInsuredSchema schema = new LCInsuredSchema();
        schema.setSchema(excelSchema);
        schema.setContNo("");

        LDPersonSet iSet = null;

        if (!"".equals(StrTool.cTrim(schema.getInsuredNo()))) {
            //有客户号则用客户号查询
            tdb.setCustomerNo(schema.getInsuredNo());
            iSet = tdb.query();
        }
        if (iSet == null) {
            //没有客户号或者用客户号没有查询到,则用客户关键属性查询
            if ("".equals(StrTool.cTrim(schema.getName()))) {
                CError.buildErr(this, "客户姓名不能为空！");
                return null;
            }
            if ("".equals(StrTool.cTrim(schema.getSex()))) {
                CError.buildErr(this, "客户性别不能为空！");
                return null;

            }
            if ("".equals(StrTool.cTrim(schema.getBirthday()))) {
                CError.buildErr(this, "客户生日不能为空！");
                return null;
            }
            String[] month = PubFun.calFLDate(schema.getBirthday());
            if (Integer.parseInt(schema.getBirthday().split("-")[2]) >
                Integer.parseInt(month[1].split("-")[2]) ||
                Integer.parseInt(schema.getBirthday().split("-")[2]) <
                Integer.parseInt(month[0].split("-")[2])) {
                CError.buildErr(this, "被保险人生日录入错误！"+schema.getBirthday());
                return null;
            }
            
            if(schema.getIDType().equals("0")||schema.getIDType()=="0"){
            	tdb.setName(schema.getName());
            	tdb.setIDNo(schema.getIDNo());
                tdb.setIDType(schema.getIDType());
            }else{
            	tdb.setIDNo(schema.getIDNo());
                tdb.setIDType(schema.getIDType());
                tdb.setName(schema.getName());
                tdb.setSex(schema.getSex());
                tdb.setBirthday(schema.getBirthday()); 	
            }
                        
            iSet = tdb.query();
        }
        //要取出工作单为就要将地址信息提前
        LCAddressSchema tLCAddressSchema =
                findInsuredAddress(insuredId);
        if (!tdb.mErrors.needDealError() &&
            iSet != null && iSet.size() > 0) {
            //系统中存在该客户
            tPerson = iSet.get(1);
        ref.transFields(schema, tPerson);
            schema.setInsuredNo(tPerson.getCustomerNo());
            schema.setNativePlace(excelSchema.getNativePlace());
            schema.setNativeCity(excelSchema.getNativeCity());
            schema.setOccupationCode(excelSchema.getOccupationCode());
//            for (int i = 1; i <= schema.FIELDNUM ; i++) {
//                System.out.print("数据库信息："+schema.getFieldName(i)+" : ");
//                System.out.println(excelSchema.getV(excelSchema.getFieldName(i)));
//                System.out.print("磁盘导入信息："+excelSchema.getFieldName(i)+" : ");
//                System.out.println(excelSchema.getV(excelSchema.getFieldName(i)));
//            }
            schema.setOperator(mGlobalInput.Operator);
            schema.setMakeDate(PubFun.getCurrentDate());
            schema.setMakeTime(PubFun.getCurrentTime());
            schema.setModifyDate(schema.getMakeDate());
            schema.setModifyTime(schema.getMakeTime());
            //modify by zxs 
            tPerson.setAuthorization("1");
            tdb.setSchema(tPerson);
            if (!tdb.update()) {
                CError.buildErr(this, "更新客户时出错2:", tdb.mErrors);
                return null;
            }

        } else {
            //系统中不存在该客户，需要新创建
            tPerson = new LDPersonSchema();
            ref.transFields(tPerson, schema);

            String customerno = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
            tPerson.setCustomerNo(customerno);
//            tPerson.setName(schema.getName());
//            tPerson.setSex(schema.getSex());
//            tPerson.setBirthday(schema.getBirthday());
//            tPerson.setIDType(schema.getIDType());
//            tPerson.setIDNo(schema.getIDNo());
//            tPerson.setNativePlace(schema.getNativePlace());
//            tPerson.setRgtAddress(schema.getRgtAddress());
//            tPerson.setMarriage(schema.getMarriage());
//            tPerson.setNationality(schema.getNationality());
//            tPerson.setOccupationCode(schema.getOccupationCode());
//            tPerson.setOccupationType(schema.getOccupationType());
//            tPerson.setSmokeFlag(schema.getSmokeFlag());
//            tPerson.setJoinCompanyDate(schema.getJoinCompanyDate());
//            tPerson.setSalary(schema.getSalary());
            tPerson.setGrpName(tLCAddressSchema.getGrpName());
            tPerson.setOperator(mGlobalInput.Operator);
            tPerson.setMakeDate(PubFun.getCurrentDate());
            tPerson.setMakeTime(PubFun.getCurrentTime());
            tPerson.setModifyDate(tPerson.getMakeDate());
            tPerson.setModifyTime(tPerson.getMakeTime());
            //modify by zxs
            tPerson.setAuthorization("1");
            tdb.setSchema(tPerson);
            if (!tdb.insert()) {
                CError.buildErr(this, "创建客户时出错1:", tdb.mErrors);
                return null;
            }
            ref.transFields(schema, tPerson);
            schema.setInsuredNo(tPerson.getCustomerNo());

        }

        //查询创建该客户的银行账户信息
        LCAccountSchema tLCAccountSchema = new LCAccountSchema();
        tLCAccountSchema.setCustomerNo(tPerson.getCustomerNo());
        tLCAccountSchema.setAccName(excelSchema.getAccName());
        tLCAccountSchema.setBankAccNo(excelSchema.getBankAccNo());
        tLCAccountSchema.setBankCode(excelSchema.getBankCode());
        if (!createLCAccount(tLCAccountSchema)) {
            //创建客户账户失败
            return null;
        }
        //由于要去出工作单位存到ldperson中去,因此将地址信息填充提前
        //查询创建该客户的地址信息
//        LCAddressSchema tLCAddressSchema =
//                                findInsuredAddress(insuredId);

        tLCAddressSchema.setCustomerNo(
                tPerson.getCustomerNo());
        String strAddressNo = getAddressNo(tLCAddressSchema);
        if (strAddressNo == null) {
            //获取客户地址号码失败
            return null;
        }

        schema.setAddressNo(strAddressNo);

        //有些客户信息涉及到实时性，要以excel中的数据为准
        //职业,工资,职位,学历,婚姻状况
        if (!StrTool.cTrim(excelSchema.getOccupationType()).equals("")) {
            schema.setOccupationType(excelSchema.
                                     getOccupationType());
        }

        if (!StrTool.cTrim(excelSchema.getOccupationCode()).
            equals("")) {
            schema.setOccupationCode(excelSchema.
                                     getOccupationCode());
        }

        if (!StrTool.cTrim(excelSchema.getPluralityType()).
            equals("")) {
            schema.setPluralityType(excelSchema.
                                    getPluralityType());
        }

        if (!StrTool.cTrim(excelSchema.getWorkType()).
            equals("")) {
            schema.setWorkType(excelSchema.
                               getWorkType());
        }
        if (!StrTool.cTrim(excelSchema.getDegree()).
            equals("")) {
            schema.setDegree(excelSchema.
                             getDegree());
        }

        if (!StrTool.cTrim(excelSchema.getPosition()).
            equals("")) {
            schema.setPosition(excelSchema.
                               getPosition());
        }

        if (excelSchema.getSalary() > 0) {
            schema.setSalary(excelSchema.
                             getSalary());
        }

        if (!StrTool.cTrim(excelSchema.getMarriage()).
            equals("")) {
            schema.setMarriage(excelSchema.
                               getMarriage());
        }

        //缓存新创建的被保险人
        cacheLCInsuredSchema(insuredId, schema);

        return schema;
    }

    /**
     * 查询或创建投保人信息
     * 不在系统客户列表中的需要创建客户
     * @param insuredId String
     * @return VData
     */
    private LCAppntSchema createAppnt(String appntId) {
        LDPersonDB tdb = new LDPersonDB();
        LDPersonSchema tPerson = null;
        Reflections ref = new Reflections();

        //从excel导入数据中取得投保人信息
        LCAppntSchema excelSchema = findAppnt(appntId);
        if (excelSchema == null) {
            CError.buildErr(this,
                            "创建投保人时未从导入数据中查找到投保人[" +
                            appntId + "]");
            return null;
        }

        //拷贝一份
        LCAppntSchema schema = new LCAppntSchema();
        schema.setSchema(excelSchema);
        schema.setContNo("");

        LDPersonSet iSet = null;
        LCAddressSchema tLCAddressSchema =
                findAppntAddress(appntId);
        if (!"".equals(StrTool.cTrim(schema.getAppntNo()))) {
            //有客户号则用客户号查询
            tdb.setCustomerNo(schema.getAppntNo());
            iSet = tdb.query();    
        if (iSet == null) {
            //没有客户号或者用客户号没有查询到,则用客户关键属性查询
            if ("".equals(StrTool.cTrim(schema.getAppntName()))) {
                CError.buildErr(this, "客户姓名不能为空！");
                return null;
            }
            if ("".equals(StrTool.cTrim(schema.getAppntSex()))) {
                CError.buildErr(this, "客户性别不能为空！");
                return null;

            }
            if ("".equals(StrTool.cTrim(schema.getAppntBirthday()))) {
                CError.buildErr(this, "客户生日不能为空！");
                return null;
            }
            String[] month = PubFun.calFLDate(schema.getAppntBirthday());
            if (Integer.parseInt(schema.getAppntBirthday().split("-")[2]) >
                Integer.parseInt(month[1].split("-")[2]) ||
                Integer.parseInt(schema.getAppntBirthday().split("-")[2]) <
                Integer.parseInt(month[0].split("-")[2])) {
                CError.buildErr(this, "投保人生日录入错误！" + schema.getAppntBirthday());
                return null;
            }
            
            if(schema.getIDType().equals("0")||schema.getIDType()=="0"){
            	tdb.setIDNo(schema.getIDNo());
                tdb.setIDType(schema.getIDType());
                tdb.setName(schema.getAppntName());
                
            }else{
            	tdb.setIDNo(schema.getIDNo());
                tdb.setIDType(schema.getIDType());
                tdb.setName(schema.getAppntName());
                tdb.setSex(schema.getAppntSex());
                tdb.setBirthday(schema.getAppntBirthday());
            }

            

            iSet = tdb.query();
        }

        if (!tdb.mErrors.needDealError() &&
            iSet != null && iSet.size() > 0) {
            //系统中存在该客户
            tPerson = iSet.get(1);
            ref.transFields(schema, tPerson);
            schema.setAppntNo(tPerson.getCustomerNo());
            schema.setAppntName(tPerson.getName());
            schema.setAppntSex(tPerson.getSex());
            schema.setAppntBirthday(tPerson.getBirthday());

            schema.setOperator(mGlobalInput.Operator);
            schema.setMakeDate(PubFun.getCurrentDate());
            schema.setMakeTime(PubFun.getCurrentTime());
            schema.setModifyDate(schema.getMakeDate());
            schema.setModifyTime(schema.getMakeTime());
            //modify by zxs 
            tPerson.setAuthorization("1");
            tdb.setSchema(tPerson);
            if (!tdb.update()) {
                CError.buildErr(this, "更新客户时出错2:", tdb.mErrors);
                return null;
            }
         }

        } else {
            //系统中不存在该客户，需要新创建
            tPerson = new LDPersonSchema();
            ref.transFields(tPerson, schema);
            String customerno = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");

            tPerson.setCustomerNo(customerno);
            tPerson.setName(schema.getAppntName());
            tPerson.setSex(schema.getAppntSex());
            tPerson.setBirthday(schema.getAppntBirthday());
//            tPerson.setIDType(schema.getIDType());
//            tPerson.setIDNo(schema.getIDNo());
//            tPerson.setNativePlace(schema.getNativePlace());
//            tPerson.setRgtAddress(schema.getRgtAddress());
//            tPerson.setMarriage(schema.getMarriage());
//            tPerson.setNationality(schema.getNationality());
//            tPerson.setOccupationCode(schema.getOccupationCode());
//            tPerson.setOccupationType(schema.getOccupationType());
//            tPerson.setSmokeFlag(schema.getSmokeFlag());
//            tPerson.setJoinCompanyDate(schema.getJoinCompanyDate());
//            tPerson.setSalary(schema.getSalary());
            //此处为缓存数据。
            tPerson.setOperator(mGlobalInput.Operator);
            tPerson.setMakeDate(PubFun.getCurrentDate());
            tPerson.setMakeTime(PubFun.getCurrentTime());
            tPerson.setModifyDate(tPerson.getMakeDate());
            tPerson.setModifyTime(tPerson.getMakeTime());
            tPerson.setGrpName(tLCAddressSchema.getGrpName());
            tPerson.setPosition("");
            //modify by zxs
            tPerson.setAuthorization("1");
            tdb.setSchema(tPerson);
            if (!tdb.insert()) {
                CError.buildErr(this, "创建客户时出错2:", tdb.mErrors);
                return null;
            }
            ref.transFields(schema, tPerson);
            schema.setAppntNo(tPerson.getCustomerNo());
            schema.setAppntName(tPerson.getName());
            schema.setAppntSex(tPerson.getSex());
            schema.setAppntBirthday(tPerson.getBirthday());
        }

        //查询创建该客户的银行账户信息
        LCAccountSchema tLCAccountSchema = new LCAccountSchema();
        tLCAccountSchema.setCustomerNo(tPerson.getCustomerNo());
        tLCAccountSchema.setAccName(excelSchema.getAccName());
        tLCAccountSchema.setBankAccNo(excelSchema.getBankAccNo());
        tLCAccountSchema.setBankCode(excelSchema.getBankCode());
        if (!createLCAccount(tLCAccountSchema)) {
            //创建客户账户失败
            return null;
        }

        //查询创建该客户的地址信息

        tLCAddressSchema.setCustomerNo(
                tPerson.getCustomerNo());
        String strAddressNo = getAddressNo(tLCAddressSchema);
        if (strAddressNo == null) {
            //获取客户地址号码失败
            return null;
        }

        schema.setAddressNo(strAddressNo);

        //有些客户信息涉及到实时性，要以excel中的数据为准
        if (!StrTool.cTrim(excelSchema.getOccupationType()).equals("")) {
            schema.setOccupationType(excelSchema.
                                     getOccupationType());
        }   //modify by lxs pad出单国籍问题 20160721
        if (!StrTool.cTrim(excelSchema.getNativePlace()).equals("")) {
            schema.setNativePlace(excelSchema.
            		getNativePlace());
        }
        if (!StrTool.cTrim(excelSchema.getNativeCity()).equals("")) {
            schema.setNativeCity(excelSchema.
            		getNativeCity());
        }
        if (!StrTool.cTrim(excelSchema.getOccupationCode()).
            equals("")) {
            schema.setOccupationCode(excelSchema.
                                     getOccupationCode());
        }

        if (!StrTool.cTrim(excelSchema.getPluralityType()).
            equals("")) {
            schema.setPluralityType(excelSchema.
                                    getPluralityType());
        }

        if (!StrTool.cTrim(excelSchema.getWorkType()).
            equals("")) {
            schema.setWorkType(excelSchema.
                               getWorkType());
        }
        if (!StrTool.cTrim(excelSchema.getDegree()).
            equals("")) {
            schema.setDegree(excelSchema.
                             getDegree());
        }

        if (!StrTool.cTrim(excelSchema.getPosition()).
            equals("")) {
            schema.setPosition(excelSchema.
                               getPosition());
        }

        if (excelSchema.getSalary() > 0) {
            schema.setSalary(excelSchema.
                             getSalary());
        }

        if (!StrTool.cTrim(excelSchema.getMarriage()).
            equals("")) {
            schema.setMarriage(excelSchema.
                               getMarriage());
        }
        if (!StrTool.cTrim(excelSchema.getMarriage()).
                    equals("")) {
                    schema.setMarriage(excelSchema.
                                       getMarriage());
                    tPerson.setGrpName(schema.getModifyDate());
        }
        //缓存新创建的投保人
        cacheLCAppntSchema(appntId, schema);
        return schema;
    }

    /**
     * 取得客户地址号码,不存在则创建客户地址
     * @param tLCAddressSchema LCAddressSchema 客户地址信息
     * @return String 地址号码
     */
    private String getAddressNo(LCAddressSchema tLCAddressSchema) {
        String strAddressNo = null;

        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setSchema(tLCAddressSchema);
        LCAddressSet tLCAddressSet = tLCAddressDB.query();

        if (!tLCAddressDB.mErrors.needDealError() &&
            tLCAddressSet != null && tLCAddressSet.size() > 0) {
            //系统中存在该地址
            strAddressNo = tLCAddressSet.get(1).getAddressNo();
        } else {
            //新创建客户地址
//======== ADD ======= 2005-03-17 ========= zhang tao ======= BGN ============
            try {
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(integer(AddressNo)) Is Null Then 0 Else max(integer(AddressNo)) End from LCAddress where CustomerNo='"
                             + tLCAddressSchema.getCustomerNo() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                int iAddressNo = 1 + Integer.parseInt(tSSRS.GetText(1, 1));
                strAddressNo = String.valueOf(iAddressNo);
                System.out.println("得到的地址码是：" + strAddressNo);
            } catch (Exception e) {
                CError tError = new CError();
                tError.moduleName = "ContIsuredBL";
                tError.functionName = "createAddressNo";
                tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!" + e;
                this.mErrors.addOneError(tError);
            }

//======== ADD ======= 2005-03-17 ========= zhang tao ======= END ============
            //tLCAddressDB.setGrpName();
            tLCAddressDB.setAddressNo(strAddressNo);
            tLCAddressDB.setOperator(mGlobalInput.Operator);
            tLCAddressDB.setMakeDate(PubFun.getCurrentDate());
            tLCAddressDB.setMakeTime(PubFun.getCurrentTime());
            tLCAddressDB.setModifyDate(tLCAddressDB.getMakeDate());
            tLCAddressDB.setModifyTime(tLCAddressDB.getMakeTime());

            if (!tLCAddressDB.insert()) {
                CError.buildErr(this, "创建客户地址时出错:",
                                tLCAddressDB.mErrors);
                return null;
            }
        }
        return strAddressNo;
    }

    /**
     * 创建客户账户
     * @param tLCAddressSchema LCAddressSchema 客户地址信息
     * @return String 地址号码
     */
    private boolean createLCAccount(LCAccountSchema tLCAccountSchema) {
        LCAccountDB tLCAccountDB = new LCAccountDB();
        tLCAccountDB.setSchema(tLCAccountSchema);
        LCAccountSet tLCAccountSet = tLCAccountDB.query();

        if (!tLCAccountDB.mErrors.needDealError() &&
            tLCAccountSet != null && tLCAccountSet.size() > 0) {
            //系统中存在该账户 doNothing
        } else {
            if (tLCAccountDB.getCustomerNo() != null ||
                "".equals(tLCAccountDB.getCustomerNo()) ||
                tLCAccountDB.getAccName() != null ||
                "".equals(tLCAccountDB.getAccName()) ||
                tLCAccountDB.getBankAccNo() != null ||
                "".equals(tLCAccountDB.getBankAccNo()) ||
                tLCAccountDB.getBankCode() != null ||
                "".equals(tLCAccountDB.getBankCode())) {
                //不能创建客户账户
                return true;
            }

            //新创建客户账户
            tLCAccountDB.setAccKind("N");
            tLCAccountDB.setOperator(mGlobalInput.Operator);
            tLCAccountDB.setMakeDate(PubFun.getCurrentDate());
            tLCAccountDB.setMakeTime(PubFun.getCurrentTime());
            tLCAccountDB.setModifyDate(tLCAccountDB.getMakeDate());
            tLCAccountDB.setModifyTime(tLCAccountDB.getMakeTime());

            if (!tLCAccountDB.insert()) {
                CError.buildErr(this, "创建客户账户时出错:",
                                tLCAccountDB.mErrors);
                return false;
            }
        }
        return true;
    }

    /**
     * 根据合同号获取excel导入的险种保单数据
     * @param contId String
     * @return Object
     */
    public Object getContPolData(String contId) {
        return mContPolData.get(contId);
    }

    /**
     * 初始化险种保单数据
     */
    public void intiContPolData() {
        mContPolData = new HashMap();

    }

    /**
     * 按合同号保存excel导入的险种保单数据
     * @param contId String
     * @param data Object
     */
    public void addContPolData(String contId, Object data) {

        if (mContPolData.containsKey(contId)) {
            VData tVData = (VData) mContPolData.get(contId);
            tVData.add(data);
        } else {
            VData contpolData = new VData();
            contpolData.add(data);
            mContPolData.put(contId, contpolData);
        }
    }

    /**
     * 从险种保单关键字中解析出合同ID
     * @param polKey String like contid-polid-insuredid
     * @return String
     */
    public String getContKey(String polKey) {
        String rs = polKey;
        if (polKey.indexOf("-") > 0) {
            Object[] obj = polKey.split("-");
            rs = (String) obj[0];
        }
        return rs;
    }

    /**
     * 从缓存中查找险种编码对应的险种描述信息
     * @param strRiskCode
     * @return LMRiskAppSchema
     */
    public LMRiskAppSchema findLMRiskAppFromCache(String strRiskCode) {
        return (LMRiskAppSchema) m_LMRiskAppMap.get(strRiskCode);
    }

    /**
     * 缓存险种描述信息
     * @param strRiskCode String
     * @param tLMRiskAppSchema LMRiskAppSchema
     */
    public void cacheLMRiskApp(String strRiskCode,
                               LMRiskAppSchema tLMRiskAppSchema) {
        m_LMRiskAppMap.put(strRiskCode, tLMRiskAppSchema);
    }


    /**
     * 从DB中查找险种编码对应的险种描述信息
     * @param strRiskCode
     * @return
     */
    public LMRiskAppSchema findLMRiskAppFromDB(String strRiskCode) {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(strRiskCode);
        if (!tLMRiskAppDB.getInfo()) {
            mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
            return null;
        }

        return tLMRiskAppDB.getSchema();

    }

    /**
     * 从缓存中查找险种编码对应的险种描述信息
     * @param strRiskCode
     * @return
     */
    public LMRiskSchema findLMRiskFromCache(String strRiskCode) {
        return (LMRiskSchema) m_LMRiskMap.get(strRiskCode);
    }

    /**
     * 缓存险种描述信息
     * @param strRiskCode String
     * @param tLMRiskAppSchema LMRiskAppSchema
     */
    public void cacheLMRisk(String strRiskCode,
                            LMRiskSchema tLMRiskSchema) {
        m_LMRiskMap.put(strRiskCode, tLMRiskSchema);
    }

    /**
     * 从DB中查找险种编码对应的险种描述信息
     * @param strRiskCode
     * @return
     */
    public LMRiskSchema findLMRiskFromDB(String strRiskCode) {
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(strRiskCode);
        if (!tLMRiskDB.getInfo()) {
            mErrors.copyAllErrors(tLMRiskDB.mErrors);
            return null;
        }

        return tLMRiskDB.getSchema();

    }

    /**
     * 检验该印刷号是否已经产生合同
     * (同一印刷号只能产生一条合同)
     * @param strPrtNo
     * @return boolean
     */
    public boolean checkPrtNO(String strPrtNo) {
        LCContDB tContDB = new LCContDB();
        tContDB.setPrtNo(strPrtNo);
        LCContSet tLCContSet = tContDB.query();
        if (tLCContSet != null && tLCContSet.size() > 0) {
            CError.buildErr(this, "印刷号[" + strPrtNo + "]已经产生合同");
            return false;
        }
        return true;
    }

    /**
     * 创建工作流节点
     * @param strPrtNo
     * @return boolean
     */
    public MMap createMission(VData missionData) {
        ActivityOperator tActivityOperator = new ActivityOperator();
        boolean missionCreate = tActivityOperator.
                                CreateStartMission(STATIC_PROCESSID,
                STATIC_ACTIVITYID,
                missionData);
        if (!missionCreate) {
            CError.buildErr(this, "创建工作流节点失败");
            return null;
        }

        MMap missionMap = (MMap)
                          tActivityOperator.getResult().
                          getObjectByObjectName("MMap", 0);

        return missionMap;
    }

}
