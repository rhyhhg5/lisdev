package com.sinosoft.lis.tb;

import org.apache.poi.hssf.record.formula.functions.And;

import com.sinosoft.lis.db.CrsInfoListDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LOMixAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.CrsInfoListSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LOMixAgentSchema;
import com.sinosoft.lis.vschema.CrsInfoListSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpContSet;


import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

import examples.newsgroups;

public class CrsInfoBL {
	public CErrors mErrors = new CErrors();
	private String mOperate;

	private GlobalInput mGlobalInput = new GlobalInput();
	private TransferData mTransferData = new TransferData();
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
	private CrsInfoListSchema mCrsInfoListSchema = new CrsInfoListSchema();
	private LCContSchema mLCContSchema = new LCContSchema();

	private LDCodeSchema mLDCodeSchema = new LDCodeSchema();

	private String mContType;
	private String mPrtNo;
	private String mContNo;
	private String mSaleChnl;
	private String mGrpAgentCom;
	private String mGrpAgentCode;
	private String mGrpAgentName;
	private String mGrpAgentIdNo;
	private String mCrsSaleChnl;
	private String mCrsBussType;
	private String mAppFlag;
	private String stateFlag;
	private String mAgentCode;
	private String mAgentCom;
	private String mAgentGroup;
	private String mEdorNo;
	public CrsInfoBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		this.mOperate = cOperate;
		System.out.println("动作为：" + mOperate);
		if("deletesucc".equals(mOperate)){
			if (!dealData()) {
				return false;
			}
			return true;
		}else{
			
			
			if (!getInputData(cInputData)) {
				return false;
			}
	
			if (!checkData()) {
				return false;
			}
	
			if (!dealData()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 校验录入的数据是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		

		return true;
	}

	/**
	 * dealData 处理业务数据
	 *
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		// 维护销售渠道
		if (mOperate.equals("modify")) {
			if (!doSaleChnl()) {
				return false;
			}
		}	
		if (mOperate.equals("delete")) {
			if (!dodelete()) {
				return false;
			}
		}
		if (mOperate.equals("deletesucc")) {
			if (!dodeletesucc()) {
				return false;
			}
		}
		return true;
	}

	private boolean dodeletesucc() {
		CrsInfoListDB crsInfoListDB=new CrsInfoListDB();
		CrsInfoListSet crsInfoListSet=new CrsInfoListSet();
		MMap tMMap = new MMap();
		crsInfoListSet=crsInfoListDB.executeQuery("select * from crsinfolist where stateflag='3'");
		if(crsInfoListSet.size()==0){
			CError tError = new CError();
			tError.moduleName = "CrsInfoBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "没有可以删除的保单!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		for(int i=1;i<=crsInfoListSet.size();i++){
			CrsInfoListSchema crsinfolistSchema=new CrsInfoListSchema();
			crsinfolistSchema=crsInfoListSet.get(i);
			tMMap.put(crsinfolistSchema, "DELETE");
			
			
		}
		VData tVData = new VData();
		tVData.add(tMMap);
		
		
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "DELETE")) {
			CError tError = new CError();
			tError.moduleName = "CrsInfoBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "删除失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dodelete() {
		MMap tMMap = new MMap();
		
		System.out.println(mCrsInfoListSchema.getEdorNo());
		tMMap.put(mCrsInfoListSchema, "DELETE");

		

		VData tVData = new VData();
		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "DELETE")) {
			CError tError = new CError();
			tError.moduleName = "CrsInfoBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "维护销售渠道信息失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
		
		
		
		
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mTransferData == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsInfoBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
			this.mErrors.addOneError(tError);

			return false;
		}		

		if (mTransferData.getValueByName("tContNo") == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsInfoBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单号失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		mContNo = (String) mTransferData.getValueByName("tContNo");
		System.out.println("保单号为：" + mContNo);
		mSaleChnl = (String) mTransferData.getValueByName("tSaleChnl");
		mEdorNo=(String) mTransferData.getValueByName("tEdorNo");
		System.out.println("流水号为：" + mEdorNo);
		if (mOperate.equals("modify")) {
			mGrpAgentCom = (String) mTransferData
					.getValueByName("tGrpAgentCom");
			mGrpAgentCode = (String) mTransferData
					.getValueByName("tGrpAgentCode");
			mGrpAgentName = (String) mTransferData
					.getValueByName("tGrpAgentName");
			mGrpAgentIdNo = (String) mTransferData
					.getValueByName("tGrpAgentIdNo");
			mCrsSaleChnl = (String) mTransferData
					.getValueByName("tCrsSaleChnl");
			mCrsBussType = (String) mTransferData
					.getValueByName("tCrsBussType");
			mAgentCode = (String) mTransferData
					.getValueByName("tAgentCode");
			mAgentCom = (String) mTransferData
					.getValueByName("tAgentCom");
			mAgentGroup = (String) mTransferData
					.getValueByName("tAgentGroup");
			
		}		
		
		if (!getContMessage()) {
			return false;
		}

		return true;
	}

	// 获取保单号
	private boolean getContMessage() {
		
			CrsInfoListDB tcCrsInfoListDB = new CrsInfoListDB();
			CrsInfoListSet tCrsInfoListSet = new CrsInfoListSet();
			tCrsInfoListSet=tcCrsInfoListDB.executeQuery("select * from crsinfolist where contno='"+ mContNo+"'" + " and EdorNo='"+mEdorNo+"'");
			ExeSQL eSql=new ExeSQL();
			SSRS ssrs=new SSRS();
			mCrsInfoListSchema= tCrsInfoListSet.get(1);
			if("modify".equals(mOperate)){
				
				try {
					
				
				if(!"".equals(mAgentCode)){
					String agentgroup=mAgentGroup;
			    	System.out.println(agentgroup);
			    	if((agentgroup==null||"".equals(agentgroup))&&(!"".equals(mAgentCode))){
			    		ssrs=eSql.execSQL("select agentgroup from laagent where agentcode='"
			    				+mAgentCode+"'" );
			    		String magentgroup=ssrs.GetText(1, 1);
			    		System.out.println(magentgroup);
			    		mCrsInfoListSchema.setAgentGroup(magentgroup);    		
			    	}
					
				}
				} catch (Exception e) {
					CError tError = new CError();
					tError.moduleName = "CrsInfoBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "业务员组别不存在!";
					this.mErrors.addOneError(tError);
					return false;
				}
			
			}
		return true;

	}

	private boolean update() {
		System.out.println("-------------- 开始进行维护----------------------");
		

			if (mSaleChnl != null && !"".equals(mSaleChnl))
				mCrsInfoListSchema.setSaleChnl(mSaleChnl);

			mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
			mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
			
			if (mAgentCode != null && !"".equals(mAgentCode))
				mCrsInfoListSchema.setAgentCode(mAgentCode);

			if (mAgentCom != null && !"".equals(mAgentCom))
				mCrsInfoListSchema.setAgentCom(mAgentCom);

			if (mAgentGroup != null && !"".equals(mAgentGroup))
				mCrsInfoListSchema.setAgentGroup(mAgentGroup);

			
			if (mGrpAgentCode != null && !"".equals(mGrpAgentCode))
				mCrsInfoListSchema.setGrpagentCode(mGrpAgentCode);

			if (mGrpAgentCom != null && !"".equals(mGrpAgentCom))
				mCrsInfoListSchema.setGrpagentCom(mGrpAgentCom);

			if (mGrpAgentIdNo != null && !"".equals(mGrpAgentIdNo))
				mCrsInfoListSchema.setGrpagentIdNo(mGrpAgentIdNo);

			if (mGrpAgentName != null && !"".equals(mGrpAgentName))
				mCrsInfoListSchema.setGrpagentName(mGrpAgentName);

			if (mCrsSaleChnl != null && !"".equals(mCrsSaleChnl))
				mCrsInfoListSchema.setCrs_SaleChnl(mCrsSaleChnl);

			if (mCrsBussType != null && !"".equals(mCrsBussType))
				mCrsInfoListSchema.setCrs_BussType(mCrsBussType);
			stateFlag=mCrsInfoListSchema.getOther();
			mCrsInfoListSchema.setStateFlag(stateFlag);
		

			

		
		return true;
	}

	// 维护销售渠道传值
	private boolean doSaleChnl() {
		if (!update()) {
			CError tError = new CError();
			tError.moduleName = "CrsInfoBL";
			tError.functionName = "doSaleChnl";
			tError.errorMessage = "销售渠道信息传递失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		MMap tMMap = new MMap();
		
		mCrsInfoListSchema.setFalseReason("");
		mCrsInfoListSchema.setOther("");
		tMMap.put(mCrsInfoListSchema, "UPDATE");

		

		VData tVData = new VData();
		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "UPDATE")) {
			CError tError = new CError();
			tError.moduleName = "CrsInfoBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "维护销售渠道信息失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	

}
