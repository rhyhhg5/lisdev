package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BankMessUpdBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 提交数据的容器 */
    private MMap map = new MMap();

    /** 公共输入信息 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    //  录入
    private String mPrtNo = "";
    private String mBackCode = "";
    private String mAccName = "";
    private String mBankAccNo = "";

    private MMap mMap = new MMap();   
    public BankMessUpdBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LcpolAmntModifyBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        
        //保存数据
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ProjectComRateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    
    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        
        mPrtNo = (String)tTransferData.getValueByName("PrtNo");
        
        mBackCode = (String)tTransferData.getValueByName("BackCode");
        if("".equals(mBackCode)){
            mBackCode=null;
        }
        mAccName = (String)tTransferData.getValueByName("AccName");
        if("".equals(mAccName)){
            mAccName=null;
        }
        mBankAccNo = (String)tTransferData.getValueByName("BankAccNo");
        if("".equals(mBankAccNo)){
            mBankAccNo=null;
        }
        mOperate = (String)tTransferData.getValueByName("Operator");
        return true;
    }
    
    private boolean dealData(){
        String tSQLLcupd;
 
        tSQLLcupd=" update lccont set bankcode =";
        if(mBackCode==null){
            tSQLLcupd+=""+mBackCode+",accname = ";
        }else{
            tSQLLcupd+="'"+mBackCode+"',accname = ";
        }
        
        if(mAccName==null){
            tSQLLcupd+=""+mAccName+",BankAccNo = ";
        }else{
            tSQLLcupd+="'"+mAccName+"',BankAccNo = ";
        }
        
        if(mBankAccNo==null){
            tSQLLcupd+=""+mBankAccNo+", ";
        }else{
            tSQLLcupd+="'"+mBankAccNo+"',";
        }
        
        tSQLLcupd+=" modifydate='"+mCurrentDate+"',modifytime='"+mCurrentTime+"',operator='"+mOperate+"' where prtno = '"+mPrtNo+"'";
        
        mMap.put(tSQLLcupd, SysConst.UPDATE);
        
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData(){
        try {
            mInputData = new VData();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProjectComRateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private void jbInit() throws Exception {
    }
    
    public VData getResult() {
        return mResult;
      }

}
