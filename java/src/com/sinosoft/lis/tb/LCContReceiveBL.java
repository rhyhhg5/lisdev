package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCContReceiveBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private VData mResult = new VData();
    private String mOperate = "";
    MMap map = new MMap();

//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContReceiveSchema mLCContReceiveSchema = new LCContReceiveSchema();
    private LCContReceiveSchema m2LCContReceiveSchema = new LCContReceiveSchema();

    public LCContReceiveBL() {
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContReceiveUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("RECEIVE") && !cOperate.equals("REJECT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        //全局变量赋值
        mOperate = cOperate;

        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("End getInputData");
        if (!checkdata()) {
            return false;
        }
        System.out.println("End checkdata");
        if (this.dealData() == false) {
            return false;
        }
        System.out.println("End dealData");
        //统一递交数据
        mResult.add(map);
        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(mResult, cOperate)) {
            mErrors.copyAllErrors(pubsubmit.mErrors);
            return false;
        }
        System.out.println("---End pubsubmit---");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        //获取数据的时候不需要区分打印模式
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));
        mLCContReceiveSchema.setSchema((LCContReceiveSchema) cInputData.
                                       getObjectByObjectName(
                                               "LCContReceiveSchema", 0));

        return true;
    }

    private boolean checkdata() {
        String cOperate = this.mOperate;
        LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
        LCContReceiveSet tLCContReceiveSet = new LCContReceiveSet();
        tLCContReceiveDB.setReceiveID(mLCContReceiveSchema.getReceiveID());
        tLCContReceiveDB.setContNo(mLCContReceiveSchema.getContNo());
        tLCContReceiveSet = tLCContReceiveDB.query();
        if (tLCContReceiveSet.size() != 1) {
            buildError("checkdata", "根据主键查询失败");
            return false;
        }
        LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
        tLCContReceiveSchema = tLCContReceiveSet.get(1).getSchema();
        String tReceiveState = tLCContReceiveSchema.getReceiveState();
        String DealState = tLCContReceiveSchema.getDealState();
        if (!tReceiveState.equals("0")) {
            buildError("checkdata", "只有处于未接收状态的记录可以进行合同接收或退回操作");
            return false;
        }
        if (!DealState.equals("0")) {
            buildError("checkdata", "该记录已经进行过处理");
            return false;
        }
        if (cOperate.equals("REJECT")) {
            if (this.mLCContReceiveSchema.getBackReasonCode().equals("")) {
                buildError("checkdata", "进行合同退回时需要选择退回原因!");
                return false;
            }

        }
        return true;
    }

    private boolean dealData() {
        String cOperate = this.mOperate;
        LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
        LCContReceiveSet tLCContReceiveSet = new LCContReceiveSet();
        tLCContReceiveDB.setReceiveID(mLCContReceiveSchema.getReceiveID());
        tLCContReceiveDB.setContNo(mLCContReceiveSchema.getContNo());
        tLCContReceiveSet = tLCContReceiveDB.query();
        LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
        tLCContReceiveSchema = tLCContReceiveSet.get(1).getSchema();
        tLCContReceiveSchema.setReceiveManageCom(mGlobalInput.ManageCom);
        tLCContReceiveSchema.setReceiveOperator(mGlobalInput.Operator);
        tLCContReceiveSchema.setReceiveDate(PubFun.getCurrentDate());
        tLCContReceiveSchema.setReceiveTime(PubFun.getCurrentTime());
        tLCContReceiveSchema.setReceiveState("1");
        tLCContReceiveSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContReceiveSchema.setModifyTime(PubFun.getCurrentTime());
        if (cOperate.equals("REJECT")) {
            tLCContReceiveSchema.setReceiveState("2");
            tLCContReceiveSchema.setBackReasonCode(mLCContReceiveSchema.
                    getBackReasonCode());
            tLCContReceiveSchema.setBackReason(mLCContReceiveSchema.
                                               getBackReason());
        }
        this.m2LCContReceiveSchema = tLCContReceiveSchema;
        map.put(tLCContReceiveSchema, "UPDATE");
        if (this.dealReceive() == false) {
            return false;
        }

        return true;
    }

    private boolean dealReceive() {
        String cOperate = this.mOperate;
        if (cOperate.equals("RECEIVE")) {
            LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
            LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
            LCContGetPolSet tLCContGetPolSet = new LCContGetPolSet();
            tLCContGetPolDB.setContNo(mLCContReceiveSchema.getContNo());
            tLCContGetPolSet = tLCContGetPolDB.query();
            if (tLCContGetPolSet.size() != 1) {
                tLCContGetPolSchema.setContNo(m2LCContReceiveSchema.getContNo());
                tLCContGetPolSchema.setPrtNo(m2LCContReceiveSchema.getPrtNo());
                tLCContGetPolSchema.setContType(m2LCContReceiveSchema.
                                                getContType());
                tLCContGetPolSchema.setCValiDate(m2LCContReceiveSchema.
                                                 getCValiDate());
                tLCContGetPolSchema.setAppntNo(m2LCContReceiveSchema.getAppntNo());
                tLCContGetPolSchema.setAppntName(m2LCContReceiveSchema.
                                                 getAppntName());
                tLCContGetPolSchema.setPrem(m2LCContReceiveSchema.getPrem());
                tLCContGetPolSchema.setAgentCode(m2LCContReceiveSchema.
                                                 getAgentCode());
                tLCContGetPolSchema.setAgentName(m2LCContReceiveSchema.
                                                 getAgentName());
                tLCContGetPolSchema.setSignDate(m2LCContReceiveSchema.
                                                getSignDate());
                tLCContGetPolSchema.setManageCom(m2LCContReceiveSchema.
                                                 getManageCom());
                tLCContGetPolSchema.setReceiveManageCom(m2LCContReceiveSchema.
                        getReceiveManageCom());
                tLCContGetPolSchema.setReceiveOperator(m2LCContReceiveSchema.
                        getReceiveOperator());
                tLCContGetPolSchema.setReceiveDate(m2LCContReceiveSchema.
                        getReceiveDate());
                tLCContGetPolSchema.setReceiveTime(m2LCContReceiveSchema.
                        getReceiveTime());
                tLCContGetPolSchema.setGetpolState("0");
                tLCContGetPolSchema.setMakeDate(PubFun.getCurrentDate());
                tLCContGetPolSchema.setMakeTime(PubFun.getCurrentTime());
                tLCContGetPolSchema.setModifyDate(PubFun.getCurrentDate());
                tLCContGetPolSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(tLCContGetPolSchema, "INSERT");
            }

        }
        return true;
    }
}
