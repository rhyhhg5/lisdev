package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SysConst;

public class SysSignActivityStatusBL {

	public CErrors mErrors = new CErrors();
    private String mPrtNo = "";
    private String mAction = "";
    private String mMissionID = "";
    private String mActivityID = "";
    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    
    public SysSignActivityStatusBL() {}
    
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "SysSignActivityStatusBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public boolean submitData(VData cInputData, String cOperate) {
    	if (!cOperate.equals("UPDATE")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("End getInputData");
        
        //签单解锁
        if(mAction.equals("breaklock"))
        {
        	BreakLockSQL(mPrtNo,mActivityID,mMissionID);
        }
        
        //签单加锁
        if(mAction.equals("addlock"))
        {
        	AddLockSQL(mPrtNo,mActivityID,mMissionID);
        }
        
        //大团单签单转换
        if(mAction.equals("toBigGrpSign"))
        {
        	ToBigGrpSignSQL(mPrtNo,mActivityID,mMissionID);
        }
        
        VData vData = new VData();
        vData.add(map);
        
        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(vData, "")) {
            mErrors.copyAllErrors(pubsubmit.mErrors);
            return false;
        }
        System.out.println("---End pubsubmit---");
        System.out.println("操作成功！");
    	return true;
    }
    
    private boolean getInputData(VData cInputData) {
        //获取数据的时候不需要区分打印模式
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));
        mPrtNo = (String)cInputData.getObjectByObjectName("String", 0);
        mAction = (String)cInputData.getObjectByObjectName("String", 1);
        mMissionID = (String)cInputData.getObjectByObjectName("String", 2);
        mActivityID = (String)cInputData.getObjectByObjectName("String", 3);
        
        System.out.println(" PrtNo = " + mPrtNo 
        		+ "\n Action = " + mAction 
        		+ "\n MissionID = " + mMissionID 
        		+ "\n ActivityID = " + mActivityID );
        return true;
    }
    
    //签单解锁功能
    public boolean BreakLockSQL(String PrtNo, String ActivityID, String MissionID)
    {
        String sql = "update lwmission set activitystatus = '1' where missionid = '" + MissionID 
        	+ "' and activityid = '"+ ActivityID + "' and missionprop2 = '" + PrtNo + "'";
        System.out.println("Excu SQL : " + sql);
        map.put(sql, SysConst.UPDATE);
        return true;
    }
    
    //签单加锁功能
    public boolean AddLockSQL(String PrtNo, String ActivityID, String MissionID)
    {
    	String sql = "update lwmission set activitystatus = '0' where missionid = '" + MissionID 
    	+ "' and activityid = '"+ ActivityID + "' and missionprop2 = '" + PrtNo + "'";
        System.out.println("Excu SQL : " + sql);
        map.put(sql, SysConst.UPDATE); 
        return true;
    }
    
    //大团单签单转换
    public boolean ToBigGrpSignSQL(String PrtNo, String ActivityID, String MissionID)
    {
    	String sql = "update lwmission set activityid = '0000002007', activitystatus = '1' where missionid = '" + MissionID 
    	+ "' and activityid = '"+ ActivityID + "' and missionprop2 = '" + PrtNo + "'";
        System.out.println("Excu SQL : " + sql);
        map.put(sql, SysConst.UPDATE); 
    	return true;
    }
}
