package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 本类用来进行外包录入错误数据修改的保单合同级别信息修改
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOContBL
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private BPOLCAppntSchema mBPOLCAppntSchema = null;

    private BPOLCPolSet tBPOLCPolSet = null;

    private MMap map = new MMap();

    private String mOperate = null;

    public BPOContBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     *
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if (getSubmitData(cInputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CrmPolicyChangeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getSubmitData
     *
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return boolean：true提交成功, false提交失败
     */
    private MMap getSubmitData(VData cInputData, String operate)
    {
        this.mOperate = operate;

        if (!getInputData(cInputData))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGI == null)
        {
            CError tError = new CError();
            tError.moduleName = "BPOContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请传入操作员信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (mBPOLCAppntSchema == null
                || mBPOLCAppntSchema.getBPOBatchNo() == null
                || mBPOLCAppntSchema.getBPOBatchNo().equals("")
                || tBPOLCPolSet == null || tBPOLCPolSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOContBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        for (int i = 1; i <= tBPOLCPolSet.size(); i++)
        {
            if (tBPOLCPolSet.get(i).getBPOBatchNo() == null
                    || tBPOLCPolSet.get(i).getBPOBatchNo().equals("")
                    || tBPOLCPolSet.get(i).getPolID() == null
                    || tBPOLCPolSet.get(i).getPolID().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "BPOContBL";
                tError.functionName = "checkData";
                tError.errorMessage = "保单信息不完整，无法进行业务操作，请联系IT人员";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        }

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        if (SysConst.UPDATE.equals(mOperate))
        {
            mBPOLCAppntSchema.setOperator(mGI.Operator);
            mBPOLCAppntSchema.setModifyDate(PubFun.getCurrentDate());
            mBPOLCAppntSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(this.mBPOLCAppntSchema, SysConst.UPDATE);

            for (int i = 1; i <= tBPOLCPolSet.size(); i++)
            {
                tBPOLCPolSet.get(i).setOperator(mGI.Operator);
                tBPOLCPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                tBPOLCPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
            }
            map.put(this.tBPOLCPolSet, SysConst.UPDATE);

            //若投保人是被保人，则同时修改被保人信息
            String sql = "select distinct a.* "
                    + "from BPOLCInsured a, BPOLCPol b "
                    + "where a.BPOBatchNo = b.BPOBatchNo "
                    + "   and a.ContID = b.ContID "
                    + "   and a.InsuredID = b.InsuredID "
                    + "   and b.RelationToAppnt = '00' "
                    + "   and a.BPOBatchNo = '"
                    + mBPOLCAppntSchema.getBPOBatchNo() + "' "
                    + "   and a.ContID = '" + mBPOLCAppntSchema.getContID()
                    + "' ";
            System.out.println(sql);

            BPOLCInsuredDB tBPOLCInsuredDB = new BPOLCInsuredDB();
            BPOLCInsuredSet set = tBPOLCInsuredDB.executeQuery(sql);
            if (tBPOLCInsuredDB.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "BPOContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询投保人是否是被保人时出错";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            Reflections ref = new Reflections();
            for (int i = 1; i <= set.size(); i++)
            {
                ref.transFields(set.get(i), mBPOLCAppntSchema);
                set.get(i).setOperator(mGI.Operator);
                set.get(i).setModifyDate(PubFun.getCurrentDate());
                set.get(i).setModifyTime(PubFun.getCurrentTime());
            }
            map.put(set, SysConst.UPDATE);
            
            backupDate();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mBPOLCAppntSchema = (BPOLCAppntSchema) data.getObjectByObjectName(
                "BPOLCAppntSchema", 0);
        tBPOLCPolSet = (BPOLCPolSet) data.getObjectByObjectName("BPOLCPolSet",
                0);

        return true;
    }
    
    /**
     * backupDate
     * 将数据备份到轨迹表
     * @param args
     */
    private boolean backupDate()
    {
        String sql = "select distinct a.BPOBatchNo, a.ContID from BPOLCAppnt a where BPOBatchNo = '" 
        		+ mBPOLCAppntSchema.getBPOBatchNo() + "' and ContID = '" + mBPOLCAppntSchema.getContID() + "'";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);

        String[] tBPOContTables = { "BPOLCAppnt", "BPOLCInsured", "BPOLCPol"};
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            for (int index = 0; index < tBPOContTables.length; index++)
            {
                sql = "insert into " + tBPOContTables[index] + "B "
                        + "(select (select count(1) + 1 from " + tBPOContTables[index] + "B "
                        + " where BPOBatchNo = a.BPOBatchNo and ContID = a.ContID), a.* from " 
                        + tBPOContTables[index] + " a where BPOBatchNo = '" + tSSRS.GetText(i, 1)
                        + "' and ContID = '" + tSSRS.GetText(i, 2) + "') ";
                map.put(sql, SysConst.INSERT);
            }
        }
        return true;
    }

    public static void main(String[] args)
    {
        BPOContBL bl = new BPOContBL();
    }
}
