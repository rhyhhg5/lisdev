/**
 * 2008-1-17
 */
package com.sinosoft.lis.tb;

import java.math.BigDecimal;

import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_COM_SERVERSchema;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class TestBL
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String mOperate = "";

    private MMap mMap = new MMap();

    private String[] mOperateType = { "CreateTempFeeOfSign" };

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private LCContSchema mLCContSchema = null;

    private String mEnterAccDate = null;

    private String mConfMakeDate = null;

    private String mPayDate = null;

    private String mSerialNo = null;

    // 帐户保费与到帐保费之间的差额。
    private double mDifMoney = 0d;

    // 是否为先收费的标志。
    private boolean mIsFirstPayContFlag = false;

    /**
     * 是否进行数据库提交。
     * 需要注意业务中直接调用其它的业务类UI，有可能发生直接提交的情况。
     * true:提交；false:不提交
     */
    private boolean mIsSubmitFlag = true;

    public TestBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData data, String cOperate)
    {
        mOperate = cOperate;
        mTransferData = (TransferData) data.getObjectByObjectName(
                "TransferData", 0);
        mGlobalInput = (GlobalInput) data.getObjectByObjectName("GlobalInput",
                0);

        if (mGlobalInput == null)
        {
            buildError("checkData", "处理超时，请重新登录。");
            return false;
        }

        if (mTransferData == null)
        {
            buildError("checkData", "所需参数不完整。");
            return false;
        }

        if (mOperate == null)
        {
            buildError("checkData", "操作类型错误。");
            return false;
        }

        /*if (!loadContInfo())
        {
            return false;
        }

        mIsFirstPayContFlag = isFirstPay();

        if (!getEnterAccOfTempFeeInfo())
        {
            return false;
        }*/

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    /**
     * 获取保费差异。
     * @return 
     */
    private boolean getDifMoney()
    {
        // 校验到帐保费是否足额。
        String tStrSql = "select nvl(sum(prem), 0) from LCPol where uwflag in ('4', '9') "
                + " and ContNo = '" + mLCContSchema.getContNo() + "'";
        String tPrem = new ExeSQL().getOneValue(tStrSql);
        BigDecimal tEnterAccFee = TestBL
                .getContEnterAccOfTempFee(mLCContSchema.getTempFeeNo());
        mDifMoney = tEnterAccFee.subtract(new BigDecimal(tPrem)).doubleValue();
        if (mDifMoney < 0)
        {
            buildError("checkData", "到帐保费小于承保保费。");
            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealData()
    {
    	ES_COM_SERVERSchema esComServerSchema = new ES_COM_SERVERSchema();
    	String mComCode = (String)mTransferData.getValueByName("comCode");
    	String mIpAddr = (String)mTransferData.getValueByName("ipAddr");
    	esComServerSchema.setManageCom(mComCode);
    	esComServerSchema.setHostName(mIpAddr);
    	
    	if(mOperate.equals("save")){//添加
    		mMap.put(esComServerSchema, SysConst.INSERT);
    	}else if(mOperate.equals("change")){//修改
    		String strSQL = "update es_com_server set HostName = '" +mIpAddr+ "' where Managecom = '" +mComCode+"'";
    		mMap.put(strSQL, SysConst.UPDATE);
    	}else if(mOperate.equals("delete")){//删除
    		String strSQL = "delete from es_com_server where Managecom= '"+mComCode+"' and Hostname = '"+mIpAddr+"'"; 
    		mMap.put(strSQL, SysConst.DELETE);
    	}
    	
    	if(mComCode.equals("")||mIpAddr.equals("")||mComCode==null||mIpAddr==null){
    		return false;
    	}else{
    		return true;
    	}
    	
    }

    /**
     * 加载合同信息。
     * @return
     */
    private boolean loadContInfo()
    {
        String tContNo = (String) mTransferData.getValueByName("ContNo");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo())
        {
            buildError("loadContInfo", "未找到相关合同信息。");
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();
        return true;
    }

    /**
     * 判断是否为先收费保单。
     * @return
     */
    private boolean isFirstPay()
    {
        // 只对个单进行处理。
        if (!"1".equals(mLCContSchema.getContType()))
        {
            return false;
        }
        // ------------------

        return CommonBL.getIsFristPayOfSingleCont(mLCContSchema.getContNo());
    }

    /**
     * 对到帐暂缴保费进行拆分。
     * @return
     */
    private boolean dealTempFee()
    {
        String tStrSql = "select * from LCPol where uwflag in ('4', '9') "
                + " and ProposalContNo = '" + mLCContSchema.getProposalContNo()
                + "'";
        LCPolSet tLCPolSet = new LCPolDB().executeQuery(tStrSql);
        if (tLCPolSet == null || tLCPolSet.size() <= 0)
        {
            System.out.println("未查到承保险种信息。");
            buildError("dealTempFee", "未查到承保险种信息。");
            return false;
        }

        if (!checkTempFee())
        {
            if (!mIsSubmitFlag)
            {
                return true;
            }
            System.out.println("该保单对应的暂缴费用不正确，请核查。");
            buildError("dealTempFee", "该保单对应的暂缴费用不正确，请核查。");
            return false;
        }

        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();

        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            LJTempFeeSchema tLJTempFeeSchema = createTempFee(tLCPolSchema);
            tLJTempFeeSet.add(tLJTempFeeSchema);
        }
        PubFun.fillDefaultField(tLJTempFeeSet);
        mMap.put(tLJTempFeeSet, SysConst.INSERT);

        if (mDifMoney > 0 && !dealMarginMoney())
        {
            System.out.println("未查到险种信息。");
            buildError("dealTempFee", "未查到险种信息。");
            return false;
        }

        return true;
    }

    /**
     * 拆分暂缴保费。
     * TempFeeType－16：表示为先收费类型。
     * @param tLCPolSchema
     * @return
     */
    private LJTempFeeSchema createTempFee(LCPolSchema tLCPolSchema)
    {
        String tFirstPayTempFeeNo = "16";

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();

        tLJTempFeeSchema.setTempFeeNo(mLCContSchema.getTempFeeNo());
        tLJTempFeeSchema.setTempFeeType(tFirstPayTempFeeNo);
        tLJTempFeeSchema.setRiskCode(tLCPolSchema.getRiskCode());
        tLJTempFeeSchema.setOtherNo(tLCPolSchema.getPrtNo());
        tLJTempFeeSchema.setOtherNoType("4");
        tLJTempFeeSchema.setPayMoney(tLCPolSchema.getPrem());
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
        tLJTempFeeSchema.setManageCom(tLCPolSchema.getManageCom());
        tLJTempFeeSchema.setPolicyCom(tLCPolSchema.getManageCom());
        tLJTempFeeSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
        tLJTempFeeSchema.setAgentCode(tLCPolSchema.getAgentCode());
        tLJTempFeeSchema.setConfFlag("0");

        // 初始财务到帐信息
        tLJTempFeeSchema.setEnterAccDate(mEnterAccDate);
        tLJTempFeeSchema.setPayDate(mPayDate);
        tLJTempFeeSchema.setSerialNo(mSerialNo);
        tLJTempFeeSchema.setConfMakeDate(mConfMakeDate);
        // ------------------------

        return tLJTempFeeSchema;
    }

    /**
     * 对溢缴部分保费进行处理。
     * @return
     */
    private boolean dealMarginMoney()
    {
        if (mDifMoney > 0)
        {
            String tFirstPayTempFeeNo = "16";
            String tMarginRiskCode = "000000";

            LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();

            tLJTempFeeSchema.setTempFeeNo(mLCContSchema.getTempFeeNo());
            tLJTempFeeSchema.setTempFeeType(tFirstPayTempFeeNo);
            tLJTempFeeSchema.setRiskCode(tMarginRiskCode);
            tLJTempFeeSchema.setOtherNo(mLCContSchema.getPrtNo());
            tLJTempFeeSchema.setOtherNoType("4");
            tLJTempFeeSchema.setPayMoney(mDifMoney);
            tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
            tLJTempFeeSchema.setSaleChnl(mLCContSchema.getSaleChnl());
            tLJTempFeeSchema.setManageCom(mLCContSchema.getManageCom());
            tLJTempFeeSchema.setPolicyCom(mLCContSchema.getManageCom());
            tLJTempFeeSchema.setAgentGroup(mLCContSchema.getAgentGroup());
            tLJTempFeeSchema.setAgentCode(mLCContSchema.getAgentCode());
            tLJTempFeeSchema.setConfFlag("0");

            // 初始财务到帐信息
            tLJTempFeeSchema.setEnterAccDate(mEnterAccDate);
            tLJTempFeeSchema.setPayDate(mPayDate);
            tLJTempFeeSchema.setSerialNo(mSerialNo);
            tLJTempFeeSchema.setConfMakeDate(mConfMakeDate);
            // ------------------------

            PubFun.fillDefaultField(tLJTempFeeSchema);

            mMap.put(tLJTempFeeSchema, SysConst.INSERT);

            if (!dealMarginMoneyToAppAcc())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 溢缴金额存入投保人帐户。
     * @return
     */
    private boolean dealMarginMoneyToAppAcc()
    {
        if ("1".equals(mLCContSchema.getContType()))
        {
            if (mLCContSchema.getAppntNo() == null
                    || "".equals(mLCContSchema.getAppntNo()))
            {
                System.out.println("保单投保人代码不存在。");
                buildError("dealMarginMoneyToAppAcc", "保单投保人代码不存在。");
                return false;
            }

            // 生成投保人账户
            LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
            tLCAppAccTraceSchema.setCustomerNo(mLCContSchema.getAppntNo());
            tLCAppAccTraceSchema.setMoney(mDifMoney);
            tLCAppAccTraceSchema.setOtherNo(mLCContSchema.getContNo());
            tLCAppAccTraceSchema.setOtherType("1");
            tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
            AppAcc tAppAcc = new AppAcc();
            MMap tMap = tAppAcc.accShiftToXQY(tLCAppAccTraceSchema, "0");
            if (tMap == null)
            {
                System.out.println("生成投保人帐户轨迹失败。");
                buildError("dealMarginMoneyToAppAcc", "生成投保人帐户轨迹失败。");
                return false;
            }
            mMap.add(tMap);
        }
        return true;
    }

    /**
     * 获取暂缴到帐的相关信息。
     * @return
     */
    private boolean getEnterAccOfTempFeeInfo()
    {
        String tStrSql = "select max(ljtfc.EnterAccDate), max(ljtfc.PayDate), "
                + " max(ljtfc.ConfMakeDate), min(ljtfc.SerialNo) "
                + " from LJTempFeeClass ljtfc where ljtfc.tempfeeno = '"
                + mLCContSchema.getTempFeeNo() + "'";
        SSRS tResult = new ExeSQL().execSQL(tStrSql);
        if (tResult != null && tResult.getMaxRow() <= 0)
        {
            System.out.println("未查到险种信息。");
            buildError("dealTempFee", "未查到险种信息。");
            return false;
        }
        mEnterAccDate = tResult.GetText(1, 1);
        mPayDate = tResult.GetText(1, 2);
        mConfMakeDate = tResult.GetText(1, 3);
        mSerialNo = tResult.GetText(1, 4);

        return true;
    }

    /**
     * 检查该保单的LJTempFee情况。
     * @return
     */
    private boolean checkTempFee()
    {
        String tStrSql = "select count(1) from LJTempFee ljtf "
                + " where ljtf.TempFeeType != '16' "
                + " and ljtf.TempFeeNo = '" + mLCContSchema.getTempFeeNo()
                + "' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if (!"0".equals(tResult))
        {
            String tStrError = mLCContSchema.getTempFeeNo() + "该暂缴号已被使用。";
            System.out.println(tStrError);
            buildError("dealTempFee", tStrError);
            return false;
        }

        // 校验保单是否存在已经拆分。
        tStrSql = "select count(1) from LJTempFee ljtf "
                + " where ljtf.EnterAccDate is not null and ljtf.ConfMakeDate is not null "
                + " and ljtf.ConfFlag = '0' and ljtf.TempFeeType = '16' "
                + " and ljtf.TempFeeNo = '" + mLCContSchema.getTempFeeNo()
                + "' " + " and ljtf.OtherNoType = '4' and ljtf.OtherNo = '"
                + mLCContSchema.getPrtNo() + "' ";
        tResult = new ExeSQL().getOneValue(tStrSql);
        if (!"0".equals(tResult))
        {
            System.out.println("该先收费保单已经拆分，不进行再次拆分。");
            mIsSubmitFlag = false;
            return false;
        }
        // ----------------------

        return true;
    }

    /**
     * 提交数据库
     * @return
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (mIsSubmitFlag && !p.submitData(data, ""))
        {
            System.out.println("提交数据失败。");
            buildError("submit", "提交数据失败。");

            return false;
        }

        return true;
    }

    private boolean checkOperate()
    {
        // 无指定操作符类型时，不进行校验。
        if (mOperateType == null)
            return true;

        for (int i = 0; i < mOperateType.length; i++)
        {
            if (mOperate.equals(mOperateType[i]))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * 获取某一保单到帐保费与承保保费之间的差额。
     * @param tContNo 保单号
     * @return
     */
    public static final BigDecimal getDifEnterAccOfTempFee(String tContNo)
    {
        return null;
    }

    /**
     * 查询某暂缴号到帐情况。
     * LJTempFeeClass：EnterAccDate is not null && ConfMakeDate is not null
     * @param tTempFeeNo    暂缴凭证号
     * @return  返回该暂缴凭证号对应的到帐保费。
     */
    public static final BigDecimal getContEnterAccOfTempFee(String tTempFeeNo)
    {
        String tSql = "select nvl(sum(PayMoney), 0) "
                + " from LJTempFeeClass ljtfc "
                + " where ljtfc.EnterAccDate is not null and ljtfc.ConfMakeDate is not null "
                + " and ConfFlag = '0' " + " and ljtfc.TempFeeNo = '"
                + tTempFeeNo + "' ";

        return new BigDecimal(new ExeSQL().getOneValue(tSql));
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "TempFeeAppBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {}

}
