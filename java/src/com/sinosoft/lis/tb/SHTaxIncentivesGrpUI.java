package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class SHTaxIncentivesGrpUI {
	
	/** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    public SHTaxIncentivesGrpUI(){}
    
    public boolean submitData(VData cInputData, String cOperate){
    	this.mInputData = (VData)cInputData.clone();
        this.mOperate = cOperate;
        SHTaxIncentivesGrpBL tTaxIncentivesGrpBL = new SHTaxIncentivesGrpBL();
        if(tTaxIncentivesGrpBL.submitData(mInputData, mOperate) == false)	{
            // @@错误处理
            this.mErrors.copyAllErrors(tTaxIncentivesGrpBL.mErrors);
            mResult.clear();
            return false;
          }	else {
            mResult = tTaxIncentivesGrpBL.getResult();
          }
    	return true;
    }
    
    public VData getResult(){
        return mResult;
    }
}
