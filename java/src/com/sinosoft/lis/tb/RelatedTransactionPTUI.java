package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class RelatedTransactionPTUI {
	
	/** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    public RelatedTransactionPTUI(){}
    
    public boolean submitData(VData cInputData){
    	this.mInputData = (VData)cInputData.clone();
        RelatedTransactionPTBL tRelatedTransactionPTBL = new RelatedTransactionPTBL();
        if(tRelatedTransactionPTBL.submitData(mInputData) == false)	{
            // @@错误处理
            this.mErrors.copyAllErrors(tRelatedTransactionPTBL.mErrors);
            mResult.clear();
            return false;
          }	else {
            return true;
          }
    }
    
    public VData getResult(){
        return mResult;
    }
}
