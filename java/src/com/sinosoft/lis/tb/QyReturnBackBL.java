package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.LCContGetPolDB;
import com.sinosoft.lis.vschema.LCContGetPolSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class QyReturnBackBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private String mOperate = "";

	MMap map = new MMap();

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private LCContGetPolSchema mLCContGetPolSchema = new LCContGetPolSchema();

	private LCContGetPolSchema m2LCContGetPolSchema = new LCContGetPolSchema();

	public QyReturnBackBL() {
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LCContReceiveUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("INSERT") && !cOperate.equals("UPDATE")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		//全局变量赋值
		mOperate = cOperate;
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("End getInputData");
		if (!checkdata()) {
			return false;
		}
		System.out.println("End checkdata");
		if (this.dealData() == false) {
			return false;
		}
		System.out.println("End dealData");
		//统一递交数据
		mResult.add(map);
		PubSubmit pubsubmit = new PubSubmit();
		if (!pubsubmit.submitData(mResult, cOperate)) {
			mErrors.copyAllErrors(pubsubmit.mErrors);
			return false;
		}
		System.out.println("---End pubsubmit---");
		return true;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mLCContGetPolSchema.setSchema((LCContGetPolSchema) cInputData
				.getObjectByObjectName("LCContGetPolSchema", 0));

		return true;
	}

	private boolean checkdata() {
		LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
		LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
		LCContGetPolSet tLCContGetPolSet = new LCContGetPolSet();
		tLCContGetPolDB.setContNo(mLCContGetPolSchema.getContNo());
		tLCContGetPolSet = tLCContGetPolDB.query();
		System.out.println("tLCContGetPolSet.size is "
				+ tLCContGetPolSet.size());
		if (tLCContGetPolSet.size() != 1) {
			buildError("checkdata", "根据主键查询失败");
			return false;
		}
		tLCContGetPolSchema = tLCContGetPolSet.get(1).getSchema();
		m2LCContGetPolSchema.setSchema(tLCContGetPolSchema);
		return true;
	}

	private boolean dealData() {

		String tContType = m2LCContGetPolSchema.getContType();
		String strSQL1 = "";
		if (mOperate.equals("UPDATE")) {
			// 个单
			if (tContType.equals("1")) {
				strSQL1 = "update lccont set GetPolDate='"
						+ PubFun.getCurrentDate() + "',CustomGetPolDate='"
						+ mLCContGetPolSchema.getGetpolDate() + "' ,"
						+ " Modifydate='" + PubFun.getCurrentDate()
						+ "', Modifytime='" + PubFun.getCurrentTime()
						+ "'where contno='" + m2LCContGetPolSchema.getContNo()
						+ "'";
				map.put(strSQL1, "UPDATE");
			}
			// 团单
			if (tContType.equals("2")) {

				strSQL1 = "update lCGrpcont set GetPolDate='"
						+ PubFun.getCurrentDate() + "', CustomGetPolDate='"
						+ mLCContGetPolSchema.getGetpolDate() + "',"
						+ " Modifydate='" + PubFun.getCurrentDate()
						+ "', Modifytime='" + PubFun.getCurrentTime()
						+ "' where grpcontno='"
						+ m2LCContGetPolSchema.getContNo() + "'";
				map.put(strSQL1, "UPDATE");
			}
            
			// 若有客户签收人员则修改
			if (!mLCContGetPolSchema.getGetpolMan().equals("")) {
				m2LCContGetPolSchema.setGetpolMan(mLCContGetPolSchema.getGetpolMan());
			}
            // 若有递交人员则修改
			if (!mLCContGetPolSchema.getSendPolMan().equals("")) {
				m2LCContGetPolSchema.setSendPolMan(mLCContGetPolSchema.getSendPolMan());
			}
			m2LCContGetPolSchema.setGetpolDate(mLCContGetPolSchema.getGetpolDate());
			m2LCContGetPolSchema.setModifyDate(PubFun.getCurrentDate());
			m2LCContGetPolSchema.setModifyTime(PubFun.getCurrentTime());
			map.put(m2LCContGetPolSchema, "UPDATE");
		}
		return true;
	}

}
