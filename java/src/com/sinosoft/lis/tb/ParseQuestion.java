package com.sinosoft.lis.tb;


import javax.xml.parsers.*;
import java.util.Random;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class ParseQuestion
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    private CErrors mErrors = new CErrors();
    private CErrors logErrors = new CErrors();


    /** 往前面传输数据的容器 */
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //private TransferData mTransferData = new TransferData();
    //private LCGrpImportLogSet mLCGrpImportLogSet = new LCGrpImportLogSet();
    //团体自动下发问题件
    private LCGrpIssuePolSet mLCGrpIssuePolSet = new LCGrpIssuePolSet();
    private GrpPolImpInfo m_GrpPolImpInfo = new GrpPolImpInfo();
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();

    private LCCustomerImpartSchema mLCCustomerImpartSchema = new
            LCCustomerImpartSchema();
    private LCCustomerImpartSchema tLCCustomerImpartSchema = new
            LCCustomerImpartSchema();

    private LCGrpContSet chkLCGrpContSet = new LCGrpContSet();
    private LCGrpAddressSet chkLCGrpAddressSet = new LCGrpAddressSet();


    private LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
    private LCContPlanSchema mLCContPlanSchema = new LCContPlanSchema();
    private LCContPlanRiskSchema mLCContPlanRiskSchema = new
            LCContPlanRiskSchema();
    private LCContPlanRiskSchema tLCContPlanRiskSchema = new
            LCContPlanRiskSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    private LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema();
    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();
    private LDGrpSchema tLDGrpSchema   = new LDGrpSchema();
    private LDGrpSchema mLDGrpSchema = new LDGrpSchema();
    private String mGrpContNo;
    private String mCustomerNo;
    private String mAddressNo;
    private String mProposalContNo;
    private String mProposalGrpContNo;
    private String mAgentCode;
    private String mImpartCode;
    private String mImpartVer;
    private String mCustomerNoType;
    private String mContPlanCode;
    private String mPlanType;
    private String mMainRiskCode;
    private String mRiskCode;
    private VData cInputData;
    private  LCInsuredSchema chkLCInsuredSchema = new LCInsuredSchema();
    //配置文件Xml节点描述
    //private String ImportFileName;
    //private String ConfigFileName;
    //private String ConfigRootPath = "/CONFIG";
    private String mBatchNo = "";
    private String mPrtNo = "";

    String GrpData = "";


    //private org.w3c.dom.Document m_doc = null; // 临时的Document对象
    /** 数据操作字符串 */
    private String mOperate;


    public ParseQuestion() {
    }

    public static void main(String[] args) {
ParseQuestion tParseQuestion = new ParseQuestion();
 VData tVData = new VData();
 LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();      //集体保单
 LDGrpSchema tLDGrpSchema   = new LDGrpSchema();                //团体客户
 LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema(); //团体客户地址
 LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema() ;//客户告知
 LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema() ;
 LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema() ;
 LAAgentSchema tLAAgentSchema= new LAAgentSchema() ;
 GlobalInput tG = new GlobalInput();


 //团体客户地址  LCGrpAddress
     tLCGrpAddressSchema.setCustomerNo("00000006");            //客户号码
     tLCGrpAddressSchema.setAddressNo("1");      //地址号码
     LCGrpAddressSet mLCGrpAddressSet = new LCGrpAddressSet() ;
     mLCGrpAddressSet.add(tLCGrpAddressSchema);




     tLCCustomerImpartSchema.setGrpContNo("00000000000000000000") ;
     tLCCustomerImpartSchema.setProposalContNo("13000000243") ;
     tLCCustomerImpartSchema.setImpartCode("010") ;
     tLCCustomerImpartSchema.setImpartVer("000") ;
     tLCCustomerImpartSchema.setCustomerNo("000000208") ;
     tLCCustomerImpartSchema.setCustomerNoType("I") ;


     //集体保单信息  LCGrpCont
     tLCGrpContSchema.setGrpContNo("0000000104");
     LCGrpContSet mLCGrpContSet = new LCGrpContSet() ;
     mLCGrpContSet.add(tLCGrpContSchema) ;

            tLAAgentSchema.setAgentCode("1102000001") ;



            tLCContPlanSchema.setGrpContNo("0000000104") ;
            tLCContPlanSchema.setContPlanCode("A") ;
            tLCContPlanSchema.setPlanType("0") ;


                  //团体客户信息  LDGrp
          tLDGrpSchema.setCustomerNo("00000006") ;


          tLCContPlanRiskSchema.setRiskCode("1604") ;
          tLCContPlanRiskSchema.setRiskVersion("2002") ;
          tLCContPlanRiskSchema.setContPlanCode("A") ;
          tLCContPlanRiskSchema.setProposalGrpContNo("1400000009") ;
          tLCContPlanRiskSchema.setPlanType("0") ;
          tLCContPlanRiskSchema.setMainRiskCode("1604") ;


         tVData.add(tLAAgentSchema);
         tVData.add(tLCCustomerImpartSchema);
         tVData.add(tLCContPlanSchema);
         tVData.add(tLDGrpSchema);
         tVData.add(tLCContPlanRiskSchema);
         tVData.add(mLCGrpContSet);
         tVData.add(mLCGrpAddressSet);

         tG.Operator="001";
         tG.ComCode="86";
         tG.ManageCom="86110000";
         tVData.add(tG);


         tParseQuestion.submitData(tVData,"INSERT||MAIN");


    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseQuestion";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ParseQuestion-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    private boolean getInputData(VData tVData) {

        mInputData = (VData) tVData.clone();
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        mLCInsuredSet = (LCInsuredSet) mInputData.getObjectByObjectName(
                "LCInsuredSet", 0);


        this.chkLCGrpContSet = (LCGrpContSet) mInputData.getObjectByObjectName(
                "LCGrpContSet", 0);
            int maxCValiDate = chkLCGrpContSet.size() ;
            for (int n = 1; n <= maxCValiDate; n++) {
            mLCGrpContSchema = chkLCGrpContSet.get(n);}

        this.chkLCGrpAddressSet=(LCGrpAddressSet) mInputData.getObjectByObjectName(
                "LCGrpAddressSet", 0);
            int maxLCGrpAddress = chkLCGrpAddressSet.size() ;
            for (int n = 1; n <= maxLCGrpAddress; n++) {
            mLCGrpAddressSchema = chkLCGrpAddressSet.get(n);}


        this.mLDGrpSchema.setSchema((LDGrpSchema) mInputData.
                                       getObjectByObjectName(
                                               "LDGrpSchema", 0));


        this.mLCCustomerImpartSchema.setSchema((LCCustomerImpartSchema) mInputData.
                                      getObjectByObjectName(
                                              "LCCustomerImpartSchema", 0));
        this.mLCContPlanSchema.setSchema((LCContPlanSchema) mInputData.
                               getObjectByObjectName(
                                       "LCContPlanSchema", 0));
        this.mLCContPlanRiskSchema.setSchema((LCContPlanRiskSchema) mInputData.
                              getObjectByObjectName(
                                      "LCContPlanRiskSchema", 0));
        this.mLAAgentSchema.setSchema((LAAgentSchema) mInputData.
                              getObjectByObjectName(
                                      "LAAgentSchema", 0));

        mGrpContNo = mLCGrpContSchema.getGrpContNo();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        tLCGrpContDB.getInfo();
        tLCGrpContSchema = tLCGrpContDB.getSchema();

        mCustomerNo = mLCGrpAddressSchema.getCustomerNo();
        mAddressNo = mLCGrpAddressSchema.getAddressNo() ;
        LCGrpAddressDB tLCGrpAddressDB =new LCGrpAddressDB() ;
        tLCGrpAddressDB.setCustomerNo(mCustomerNo) ;
        tLCGrpAddressDB.setAddressNo(mAddressNo) ;
        tLCGrpAddressDB.getInfo() ;
        tLCGrpAddressSchema=tLCGrpAddressDB.getSchema() ;

        mCustomerNo=mLDGrpSchema.getCustomerNo();
        LDGrpDB tLDGrpDB = new LDGrpDB();
        tLDGrpDB.setCustomerNo(mCustomerNo);
        tLDGrpDB.getInfo() ;
        tLDGrpSchema=tLDGrpDB.getSchema() ;

        mCustomerNo = mLCCustomerImpartSchema.getCustomerNo();
         mGrpContNo = mLCCustomerImpartSchema.getGrpContNo();
         mProposalContNo = mLCCustomerImpartSchema.getProposalContNo();
         mImpartCode = mLCCustomerImpartSchema.getImpartCode();
          mImpartVer = mLCCustomerImpartSchema.getImpartVer();
          mCustomerNoType = mLCCustomerImpartSchema.getCustomerNoType();
        LCCustomerImpartDB tLCCustomerImpartDB = new LCCustomerImpartDB();
        tLCCustomerImpartDB.setCustomerNo(mCustomerNo);
        tLCCustomerImpartDB.setGrpContNo(mGrpContNo);
        tLCCustomerImpartDB.setProposalContNo(mProposalContNo);
        tLCCustomerImpartDB.setImpartCode(mImpartCode);
        tLCCustomerImpartDB.setImpartVer(mImpartVer);
        tLCCustomerImpartDB.setCustomerNoType(mCustomerNoType);
        tLCCustomerImpartDB.getInfo();
         tLCCustomerImpartSchema = tLCCustomerImpartDB.getSchema();

        mGrpContNo = mLCContPlanSchema.getGrpContNo();
        mContPlanCode = mLCContPlanSchema.getContPlanCode();
        mPlanType = mLCContPlanSchema.getPlanType();
        LCContPlanDB tLCGrpPlanDB = new LCContPlanDB();
        tLCGrpPlanDB.setGrpContNo(mGrpContNo);
        tLCGrpPlanDB.setContPlanCode(mContPlanCode);
        tLCGrpPlanDB.setPlanType(mPlanType);
        tLCGrpPlanDB.getInfo();
         tLCContPlanSchema = tLCGrpPlanDB.getSchema();

        mProposalGrpContNo = mLCContPlanRiskSchema.getProposalGrpContNo();
        mMainRiskCode = mLCContPlanRiskSchema.getMainRiskCode();
        mRiskCode = mLCContPlanRiskSchema.getRiskCode();
        mContPlanCode = mLCContPlanRiskSchema.getContPlanCode();
        mPlanType = mLCContPlanRiskSchema.getPlanType();
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        tLCContPlanRiskDB.setProposalGrpContNo(mProposalGrpContNo);
        tLCContPlanRiskDB.setMainRiskCode(mMainRiskCode);
        tLCContPlanRiskDB.setRiskCode(mRiskCode);
        tLCContPlanRiskDB.setContPlanCode(mContPlanCode);
        tLCContPlanRiskDB.setPlanType(mPlanType);
        tLCContPlanRiskDB.getInfo();
         tLCContPlanRiskSchema = tLCContPlanRiskDB.getSchema();

        mAgentCode = mLAAgentSchema.getAgentCode();
        LAAgentDB tLCLAAgentDB = new LAAgentDB();
        tLCLAAgentDB.setAgentCode(mAgentCode);
        tLCLAAgentDB.getInfo();
        tLAAgentSchema = tLCLAAgentDB.getSchema();

        return true;
    }

     private boolean dealData() {
    //       this.getInputData2(this.mResult);
        if (!this.checkGrpName()) {

            System.out.println("投保单位名称与印章内容不符，请重新确认。");

        }
        if (!this.checkphone()) {
            System.out.println("联系电话未填");

        }

        this.CheckGrpAddress();

        this.checkGrpZipCode();
        this.checkLinkMan1();
        this.checkPhone1();
        this.checkGrpNature();
        this.checkBusinessType();
        this.checkPeoples();
        this.checkOnWorkPeoples();
        this.checkOffWorkPeoples();
        this.checkOtherPeoples();
        this.checkPeoples3();
        this.checkcontOnWorkPeoples();
        this.checkcontOffWorkPeoples();
        this.checkcontOtherPeoples() ;
        this.checkRelaPeoples();
        this.checkRelaMatePeoples();
        this.checkRelaYoungPeoples();
        this.checkRelaOtherPeoples();
        this.checkImpartParamModle();
        this.checkPayIntv();
        this.checkPayMode();

        this.checkCValiDate();
        this.checkCInValiDate();
        this.checkEnterKind() ;
        this.checkPlanPeoples3() ;
        this.checkPeoples2();
        this.checkRiskCode();
        this.checkRiskVersion();
        this.checkHandlerName();
        this.checkHandlerPrint();
        this.checkHandlerDate();
        this.checkAgentCode();
        this.checkAgentDate();
        this.checkname();

        return true;
    }

    /**
     * 被保险人的信息校验包括:
     * 生日校验
     * 姓名校验
     * 身份证位数校验
     * 性别校验
     * @param chkLCInsuredSet LCInsuredSet
     * @return boolean
     * @author YangMing
     */
    private boolean checkInsured() {

        int maxInsuredNo = mLCInsuredSet.size();
        for (int n = 1; n <= maxInsuredNo; n++) {
            LCInsuredSchema chkLCInsuredSchema = new LCInsuredSchema();
            chkLCInsuredSchema = mLCInsuredSet.get(n);
            String Birthday = chkLCInsuredSchema.getBirthday();
            if (Birthday.length() != 10) {
                CError.buildErr(this,
                                "被保险人[" + chkLCInsuredSchema.getName() +
                                "]的生日信息有误");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                                         this.mGrpContNo,
                                         chkLCInsuredSchema.getContNo(),
                                         chkLCInsuredSchema.getInsuredNo(),
                                         "", "", "", null, this.mErrors,
                                         mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "00001",
                             "被保险人", "Birthday",
                             "生日", chkLCInsuredSchema.getBirthday());
                this.mErrors.clearErrors();
                return false;
            }
            String Year = Birthday.substring(0, 4);
            String Month = Birthday.substring(5, 7);
            String Day = Birthday.substring(8);
            if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500) {
                CError.buildErr(this,
                                "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                "]的出生年份是否有误");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                                         this.mGrpContNo,
                                         chkLCInsuredSchema.getContNo(),
                                         chkLCInsuredSchema.getInsuredNo(),
                                         "", "", "", null, this.mErrors,
                                         mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "00002",
                             "被保险人", "Birthday",
                             "生日", chkLCInsuredSchema.getBirthday());
                this.mErrors.clearErrors();
                return false;
            }
            if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12) {
                CError.buildErr(this,
                                "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                "]的出生月份是否有误");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                                         this.mGrpContNo,
                                         chkLCInsuredSchema.getContNo(),
                                         chkLCInsuredSchema.getInsuredNo(),
                                         "", "", "", null, this.mErrors,
                                         mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "00003",
                             "被保险人", "Birthday",
                             "生日", chkLCInsuredSchema.getBirthday());
                this.mErrors.clearErrors();
                return false;
            }
            if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32) {
                CError.buildErr(this,
                                "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                "]的出生日期份是否有误");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                                         this.mGrpContNo,
                                         chkLCInsuredSchema.getContNo(),
                                         chkLCInsuredSchema.getInsuredNo(),
                                         "", "", "", null, this.mErrors,
                                         mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "00004",
                             "被保险人", "Birthday",
                             "生日", chkLCInsuredSchema.getBirthday());
                this.mErrors.clearErrors();
                return false;
            }
            if (chkLCInsuredSchema.getName() == null ||
                "".equals(chkLCInsuredSchema.getName())) {
                CError.buildErr(this,
                                "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                "]的姓名是否有误 ");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                                         this.mGrpContNo,
                                         chkLCInsuredSchema.getContNo(),
                                         chkLCInsuredSchema.getInsuredNo(),
                                         "", "", "", null, this.mErrors,
                                         mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "00005",
                             "被保险人", "Name",
                             "姓名", chkLCInsuredSchema.getName());
                this.mErrors.clearErrors();
                return false;
            }
            if (chkLCInsuredSchema.getSex() == null ||
                "".equals(chkLCInsuredSchema.getSex()) ||
                (!"1".equals(chkLCInsuredSchema.getSex()) &&
                 !"0".equals(chkLCInsuredSchema.getSex()))) {
                CError.buildErr(this,
                                "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                "]的性别是否有误 ");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                                         this.mGrpContNo,
                                         chkLCInsuredSchema.getContNo(),
                                         chkLCInsuredSchema.getInsuredNo(),
                                         "", "", "", null, this.mErrors,
                                         mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "00006",
                             "被保险人", "Sex",
                             "性别", chkLCInsuredSchema.getSex());
                this.mErrors.clearErrors();
                return false;
            }
            String IDNo = chkLCInsuredSchema.getIDNo();
            int IDNoBit = IDNo.length();
            if (chkLCInsuredSchema.getIDType() == null ||
                "".equals(chkLCInsuredSchema.getIDType())) {
                CError.buildErr(this,
                                "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                "]的证件类型否有误 ");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                                         this.mGrpContNo,
                                         chkLCInsuredSchema.getContNo(),
                                         chkLCInsuredSchema.getInsuredNo(),
                                         "", "", "", null, this.mErrors,
                                         mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "00007",
                             "被保险人", "IDType",
                             "证件类型", chkLCInsuredSchema.getIDType());
                this.mErrors.clearErrors();
                return false;
            }
            if ("0".equals(chkLCInsuredSchema.getIDType())) {
                if (IDNoBit != 15 && IDNoBit != 18) {
                    CError.buildErr(this,
                                    "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                    "]的身份证号码位数是否有误！");
                    m_GrpPolImpInfo.logError(this.mBatchNo,
                                             this.mGrpContNo,
                                             chkLCInsuredSchema.getContNo(),
                                             chkLCInsuredSchema.getInsuredNo(),
                                             "", "", "", null, this.mErrors,
                                             mGlobalInput);
                    GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "00008",
                                 "被保险人", "IDNo",
                                 "证件号码", chkLCInsuredSchema.getIDNo());
                    this.mErrors.clearErrors();
                    return false;
                }
                if (IDNoBit == 15) {

                    String subYear = IDNo.substring(6, 8);
                    String subMonth = IDNo.substring(8, 10);
                    String subDay = IDNo.substring(10, 12);
                    String subSex = IDNo.substring(13);
                    String Choice = "";
                    String Sex = chkLCInsuredSchema.getSex();
                    if (Integer.parseInt(subSex) % 2 == 1) {
                        Choice = "0";
                    }
                    if (Integer.parseInt(subSex) % 2 == 0) {
                        Choice = "1";
                    }
                    if (!Choice.equals(Sex)) {
                        CError.buildErr(this,
                                        "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                        "]的身份证号码与被保险人性别是否对应！");
                        m_GrpPolImpInfo.logError(this.mBatchNo,
                                                 this.mGrpContNo,
                                                 chkLCInsuredSchema.getContNo(),
                                                 chkLCInsuredSchema.
                                                 getInsuredNo(),
                                                 "", "", "", null, this.mErrors,
                                                 mGlobalInput);
                        GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                                     "00009",
                                     "被保险人", "IDNo",
                                     "证件号码", chkLCInsuredSchema.getIDNo());
                        this.mErrors.clearErrors();
                        return false;
                    }
                    if (!Year.substring(2).equals(subYear) ||
                        !subMonth.equals(Month) || !subDay.equals(Day)) {
                        CError.buildErr(this,
                                        "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                        "]的身份证号码与被保险人生日是否对应！");
                        m_GrpPolImpInfo.logError(this.mBatchNo,
                                                 this.mGrpContNo,
                                                 chkLCInsuredSchema.getContNo(),
                                                 chkLCInsuredSchema.
                                                 getInsuredNo(),
                                                 "", "", "", null, this.mErrors,
                                                 mGlobalInput);
                        GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                                     "00010",
                                     "被保险人", "IDNo",
                                     "证件号码", chkLCInsuredSchema.getIDNo());

                        this.mErrors.clearErrors();
                        return false;
                    }
                }

                if (IDNoBit == 18) {
                    String subYear = IDNo.substring(6, 10);
                    String subMonth = IDNo.substring(10, 12);
                    String subDay = IDNo.substring(12, 14);
                    String subSex = IDNo.substring(16, 17);
                    String Sex = chkLCInsuredSchema.getSex();
                    String Choice = "";
                    if (Integer.parseInt(subSex) % 2 == 1) {
                        Choice = "0";
                    }
                    if (Integer.parseInt(subSex) % 2 == 0) {
                        Choice = "1";
                    }
                    if (!Choice.equals(Sex)) {
                        CError.buildErr(this,
                                        "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                        "]的身份证号码与被保险人性别是否对应！");
                        m_GrpPolImpInfo.logError(this.mBatchNo,
                                                 this.mGrpContNo,
                                                 chkLCInsuredSchema.getContNo(),
                                                 chkLCInsuredSchema.
                                                 getInsuredNo(),
                                                 "", "", "", null, this.mErrors,
                                                 mGlobalInput);
                        GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                                     "00009",
                                     "被保险人", "IDNo",
                                     "证件号码", chkLCInsuredSchema.getIDNo());
                        this.mErrors.clearErrors();
                        return false;
                    }

                    if (!Year.equals(subYear) ||
                        !subMonth.equals(Month) || !subDay.equals(Day)) {
                        CError.buildErr(this,
                                        "请检查被保险人[" + chkLCInsuredSchema.getName() +
                                        "]的身份证号码与被保险人生日是否对应！");
                        m_GrpPolImpInfo.logError(this.mBatchNo,
                                                 this.mGrpContNo,
                                                 chkLCInsuredSchema.getContNo(),
                                                 chkLCInsuredSchema.
                                                 getInsuredNo(),
                                                 "", "", "", null, this.mErrors,
                                                 mGlobalInput);
                    }
                }
            }

        }
        return true;
    }

    /**
     }        * Yangming:用于自动下发问题件
     * @param IssueType 用于返回问题代码
     * @param BackObjType 返回对象
     * @param QuestionObj 问题对象(投保人,被保人,合同,业务员)
     * @param ErrField 错误字段
     * @param ErrFieldName 错误字段名
     * @param ErrContent 原填写内容
     */
    private boolean GrpAutoQuest(LCGrpContSchema tempLCGrpContSchema,
                                 CErrors tCError, String BackObjType,
                                 String IssueType,
                                 String QuestionObj, String ErrField,
                                 String ErrFieldName,
                                 String ErrContent) {
        String GroContNo = tempLCGrpContSchema.getGrpContNo();
        String BackObj = "";
        for (int n = 1; n < mLCGrpIssuePolSet.size(); n++) {
            if (BackObjType.equals(mLCGrpIssuePolSet.get(n).getBackObjType()) &&
                QuestionObj.equals(mLCGrpIssuePolSet.get(n).getQuestionObj()) &&
                ErrField.equals(mLCGrpIssuePolSet.get(n).getErrField()) &&
                ErrFieldName.equals(mLCGrpIssuePolSet.get(n).getErrFieldName()) &&
                ErrContent.equals(mLCGrpIssuePolSet.get(n).getErrContent()) &&
                IssueType.equals(mLCGrpIssuePolSet.get(n).getIssueType())) {
                return false;
            }
        }
        if (GroContNo == null || "".equals(GroContNo)) {
            return false;
        }
        if (tCError == null) {
            return false;
        }
        /**
         * BackObjType退回对象类型,
         * 1 －－ 操作员
         * 2 －－ 业务员
         * 3 －－ 保户
         * 4 －－ 机构
         */
        if ("1".equals(BackObjType)) {
            BackObj = tempLCGrpContSchema.getOperator();
        }
        if ("2".equals(BackObjType)) {
            BackObj = tempLCGrpContSchema.getAgentCode();
        }
        if ("3".equals(BackObjType)) {
            BackObj = tempLCGrpContSchema.getAppntNo();
        }
        if ("4".equals(BackObjType)) {
            BackObj = tempLCGrpContSchema.getManageCom();
        }
        String errMess = "";
        errMess = tCError.getError(tCError.getErrorCount() - 1).errorMessage +
                  ";";
        LCGrpIssuePolSchema tLCGrpIssuePolSchema = new LCGrpIssuePolSchema();
        tLCGrpIssuePolSchema.setGrpContNo(GroContNo);
        tLCGrpIssuePolSchema.setProposalGrpContNo(GroContNo);
        tLCGrpIssuePolSchema.setSerialNo(PubFun1.CreateMaxNo("QuestSer", 10));
        tLCGrpIssuePolSchema.setIssueType(IssueType);
        tLCGrpIssuePolSchema.setOperatePos("10");
        tLCGrpIssuePolSchema.setBackObjType(BackObjType);
        tLCGrpIssuePolSchema.setBackObj(BackObj);
        tLCGrpIssuePolSchema.setIssueCont(errMess);
        tLCGrpIssuePolSchema.setNeedPrint("N");
        tLCGrpIssuePolSchema.setOperator(this.mGlobalInput.Operator);
        tLCGrpIssuePolSchema.setManageCom(this.mGlobalInput.ManageCom);
        tLCGrpIssuePolSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpIssuePolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpIssuePolSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpIssuePolSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpIssuePolSchema.setQuestionObj(QuestionObj);
        tLCGrpIssuePolSchema.setErrField(ErrField);
        tLCGrpIssuePolSchema.setErrFieldName(ErrFieldName);
        tLCGrpIssuePolSchema.setErrContent(ErrContent);

        LCGrpIssuePolDB tLCGrpIssuePolDB = new LCGrpIssuePolDB();
        tLCGrpIssuePolDB.setSchema(tLCGrpIssuePolSchema);
        tLCGrpIssuePolDB.insert();
        mLCGrpIssuePolSet.add(tLCGrpIssuePolSchema);
        return true;
    }


    private boolean checkGrpName() {

        String GrpName = tLCGrpContSchema.getGrpName();
        System.out.println(tLCGrpContSchema.getGrpName());
        if (GrpName == null ||
            "".equals(GrpName.trim())
                ) {
            GrpName="";
            GrpData = GrpData + "投保单位名称未填。#" + "" + "^ ";

        }
        if (!GrpName.equals(tLCGrpContSchema.getHandlerPrint())) {
            System.out.println(tLCGrpContSchema.getHandlerPrint());
            GrpData = GrpData + "投保单位名称与印章内容不符，请重新确认。#" + GrpName + "^";
            System.out.println("投保单位名称与印章内容不符，请重新确认。");
        }
        return true;
    }

    private boolean checkphone() {
        String phone = tLCGrpContSchema.getPhone();
        if (phone == null ||
            "".equals(phone.trim())
                ) {
           phone="";
            GrpData = GrpData + "联系电话未填。#" + "" + "^";
            System.out.println("联系电话未填");
        }
        return true;
    }

    private boolean CheckGrpAddress() {
        String GrpAddress=tLCGrpAddressSchema.getGrpAddress() ;
        System.out.println(GrpAddress);
        if (GrpAddress == null ||
            "".equals(GrpAddress.trim())
                ) {
           GrpAddress="";
            GrpData = GrpData + "单位地址未填。#" + "" + "^";
            System.out.println("单位地址未填");

        }

        return true;
    }

    private boolean checkGrpZipCode() {


        int maxZipCodeNo = chkLCGrpAddressSet.size();
        for (int n = 1; n <= maxZipCodeNo; n++) {
            LCGrpAddressSchema chkLCGrpAddressSchema = new LCGrpAddressSchema();
            chkLCGrpAddressSchema = chkLCGrpAddressSet.get(n);
            String GrpZipCode = chkLCGrpAddressSchema.getGrpZipCode();

            if (GrpZipCode == null) {
                GrpZipCode = "";
                //GrpData = GrpData + "邮政编码未填。#" + "" + "^";
                 //System.out.println("邮政编码未填");
            }

            if (GrpZipCode.length() != 6) {
                GrpData = GrpData + "邮政编码位数有误，请重新确认。#" + GrpZipCode + "^";
                 System.out.println("邮政编码位数有误，请重新确认。");
            }
        }
        return true;
    }

    private boolean checkLinkMan1() {
        String LinkMan1=tLCGrpAddressSchema.getLinkMan1();
        if (LinkMan1 == null ||
            "".equals(LinkMan1.trim())
                ) {
            LinkMan1="";
            GrpData = GrpData + "联系人姓名未填。#" + "" + "^";
            System.out.println("联系人姓名未填");
        }
        return true;
    }

    private boolean checkPhone1() {
        String Phone1=tLCGrpAddressSchema.getPhone1();
        if (Phone1 == null ||
            "".equals(Phone1.trim())
                ) {
            Phone1="";
            GrpData = GrpData + "联系人电话未填。#"+""+"^";
            System.out.println("联系人电话未填");
        }
        return true;
    }

    private boolean checkGrpNature() {
        String GrpNature=tLCGrpContSchema.getGrpNature();
        if (GrpNature == null ||
            "".equals(GrpNature.trim())
                ) {
            GrpNature="";
            GrpData = GrpData + "企业类型编码未填。#"+""+"^";
            System.out.println("企业类型编码未填");
        }
        return true;
    }

    private boolean checkBusinessType() {
        String BusinessType=tLCGrpContSchema.getBusinessType();
        if (BusinessType == null ||
            "".equals(BusinessType.trim())
                ) {
            BusinessType="";
            GrpData = GrpData + "行业编码未填。#"+""+"^";
            System.out.println("行业编码未填");
        }
        return true;
    }

    private boolean checkPeoples() {
       int Peoples= tLDGrpSchema.getPeoples();
        if (tLDGrpSchema.getPeoples() == 0) {
        Peoples=0;
        GrpData = GrpData + "员工总人数未填。#"+0+"^";
        System.out.println("员工总人数未填");
        }
        if (tLDGrpSchema.getPeoples() !=
            tLDGrpSchema.getOffWorkPeoples() + tLDGrpSchema.getOnWorkPeoples() +
            tLDGrpSchema.getOtherPeoples()) {
            GrpData = GrpData + "员工总人数与在职、退休人数存在矛盾之处，请重新确认。#"+Peoples+"^";
            System.out.println("员工总人数与在职、退休人数存在矛盾之处，请重新确认");
        }
        return true;
    }

    private boolean checkOnWorkPeoples() {
        int OnWorkPeoples=tLDGrpSchema.getOnWorkPeoples();
        if (OnWorkPeoples == 0) {
            OnWorkPeoples=0;
            GrpData = GrpData + "在职人数未填。#"+0+"^";
            System.out.println("在职人数未填");
        }

        return true;
    }

    private boolean checkOffWorkPeoples() {
        int OffWorkPeoples=tLDGrpSchema.getOffWorkPeoples();
        if (OffWorkPeoples == 0) {
            OffWorkPeoples=0;
            GrpData = GrpData + "退休人数未填。#"+0+"^";
            System.out.println("退休人数未填");
        }

        return true;
    }

    private boolean checkOtherPeoples() {
        int OtherPeoples=tLDGrpSchema.getOtherPeoples();
        if (OtherPeoples == 0) {
            OtherPeoples=0;
            GrpData = GrpData + "其它人数未填。#"+0+"^";
            System.out.println("其它人数未填");
        }

        return true;
    }

    private boolean checkPeoples3() {
        int Peoples3= tLCGrpContSchema.getPeoples3();
        if (Peoples3 == 0) {
            Peoples3=0;
            GrpData = GrpData + "被保险人数(成员)未填。#"+0+"^";
            System.out.println("被保险人数(成员)未填");
        }
        if (tLCGrpContSchema.getPeoples3() !=
            tLCGrpContSchema.getOffWorkPeoples() +
            tLCGrpContSchema.getOnWorkPeoples() +
            tLCGrpContSchema.getOtherPeoples()) {
            GrpData = GrpData + "被保险人数与在职、退休、其它参保人员人数存在矛盾之处，请重新确认。#"+Peoples3+"^";
            System.out.println("被保险人数与在职、退休、其它参保人员人数存在矛盾之处，请重新确认。");}

        return true;
    }

    private boolean checkcontOnWorkPeoples() {
        int OnWorkPeoples=tLCGrpContSchema.getOnWorkPeoples();
        if (tLCGrpContSchema.getOnWorkPeoples() == 0) {
            OnWorkPeoples=0;
            GrpData = GrpData + "参保在职人数未填。#"+""+"^";
            System.out.println("参保在职人数未填");
        }
        if (tLCGrpContSchema.getPeoples3() !=
            tLCGrpContSchema.getOffWorkPeoples() +
            tLCGrpContSchema.getOnWorkPeoples() +
            tLCGrpContSchema.getOtherPeoples()) {
            GrpData = GrpData + "被保险人数与在职、退休、其它参保人员人数存在矛盾之处，请重新确认。#"+OnWorkPeoples+"^";
            System.out.println("被保险人数与在职、退休、其它参保人员人数存在矛盾之处，请重新确认。");
        }
        return true;
    }

    private boolean checkcontOffWorkPeoples() {
        int OffWorkPeoples =tLCGrpContSchema.getOffWorkPeoples();
        if (tLCGrpContSchema.getOffWorkPeoples() == 0) {
            OffWorkPeoples=0;
            GrpData = GrpData + "参保退休人数未填。#"+0+"^";
            System.out.println("参保退休人数未填");
        }
        if (tLCGrpContSchema.getPeoples3() !=
            tLCGrpContSchema.getOffWorkPeoples() +
            tLCGrpContSchema.getOnWorkPeoples() +
            tLCGrpContSchema.getOtherPeoples()) {
            GrpData = GrpData + "被保险人数与在职、退休、其它参保人员人数存在矛盾之处，请重新确认。#"+OffWorkPeoples+"^";
            System.out.println("被保险人数与在职、退休、其它参保人员人数存在矛盾之处，请重新确认");
        }
        return true;
    }

    private boolean checkcontOtherPeoples() {
        int OtherPeoples =tLCGrpContSchema.getOtherPeoples();
        if (OtherPeoples == 0) {
            OtherPeoples=0;
            GrpData = GrpData + "参保退休人数未填。#"+0+"^";
            System.out.println("参保退休人数未填");
        }
        if (tLCGrpContSchema.getPeoples3() !=
            tLCGrpContSchema.getOffWorkPeoples() +
            tLCGrpContSchema.getOnWorkPeoples() +
            tLCGrpContSchema.getOtherPeoples()) {
            GrpData = GrpData + "被保险人数与在职、退休、其它参保人员人数存在矛盾之处，请重新确认。#"+OtherPeoples+"^";
            System.out.println("被保险人数与在职、退休、其它参保人员人数存在矛盾之处，请重新确认");
        }
        return true;
    }


    private boolean checkRelaPeoples() {
       int RelaPeoples=tLCGrpContSchema.getRelaPeoples();
        if (tLCGrpContSchema.getRelaPeoples() == 0) {
            RelaPeoples=0;
            GrpData = GrpData + "连带被保险人数(家属)未填。#"+0+"^";
            System.out.println("连带被保险人数(家属)未填");
        }
        if (tLCGrpContSchema.getRelaPeoples() !=
            tLCGrpContSchema.getRelaMatePeoples() +
            tLCGrpContSchema.getRelaYoungPeoples() +
            tLCGrpContSchema.getRelaOtherPeoples()) {
            GrpData = GrpData +
                           "连带被保险人数与配偶、子女及其它参保人员人数存在矛盾之处，请重新确认。^"+RelaPeoples+"^";
                 System.out.println("连带被保险人数与配偶、子女及其它参保人员人数存在矛盾之处，请重新确认");
        }
        return true;
    }

    private boolean checkRelaMatePeoples() {
        int RelaMatePeoples=tLCGrpContSchema.getRelaMatePeoples() ;
        if (tLCGrpContSchema.getRelaMatePeoples() == 0) {
            RelaMatePeoples=0;
            GrpData = GrpData + "配偶人数未填。#"+0+"^";
            System.out.println("配偶人数未填");
        }
        if (tLCGrpContSchema.getRelaPeoples() !=
            tLCGrpContSchema.getRelaMatePeoples() +
            tLCGrpContSchema.getRelaYoungPeoples() +
            tLCGrpContSchema.getRelaOtherPeoples()) {
            GrpData = GrpData +
                           "连带被保险人数与配偶、子女及其它参保人员人数存在矛盾之处，请重新确认。#"+RelaMatePeoples+"^";
                 System.out.println("连带被保险人数与配偶、子女及其它参保人员人数存在矛盾之处，请重新确认");
        }
        return true;
    }

    private boolean checkRelaYoungPeoples() {
       int RelaYoungPeoples= tLCGrpContSchema.getRelaYoungPeoples();
        if (tLCGrpContSchema.getRelaYoungPeoples() == 0) {
            RelaYoungPeoples=0;
            GrpData = GrpData + "子女人数未填。#"+0+"^";
            System.out.println("子女人数未填");
        }
        if (tLCGrpContSchema.getRelaPeoples() !=
            tLCGrpContSchema.getRelaMatePeoples() +
            tLCGrpContSchema.getRelaYoungPeoples() +
            tLCGrpContSchema.getRelaOtherPeoples()) {
            GrpData = GrpData +
                           "连带被保险人数与配偶、子女及其它参保人员人数存在矛盾之处，请重新确认。#"+RelaYoungPeoples+"^";
                 System.out.println("连带被保险人数与配偶、子女及其它参保人员人数存在矛盾之处，请重新确认");
        }
        return true;
    }

    private boolean checkRelaOtherPeoples() {
       int  RelaOtherPeoples=tLCGrpContSchema.getRelaOtherPeoples();
        if (tLCGrpContSchema.getRelaOtherPeoples() == 0) {
            RelaOtherPeoples=0;
            GrpData = GrpData + "其它连带被保人数未填。#"+0+"^";
            System.out.println("其它连带被保人数未填");
        }
        if (tLCGrpContSchema.getRelaPeoples() !=
            tLCGrpContSchema.getRelaMatePeoples() +
            tLCGrpContSchema.getRelaYoungPeoples() +
            tLCGrpContSchema.getRelaOtherPeoples()) {
            GrpData = GrpData +
                           "连带被保险人数与配偶、子女及其它参保人员人数存在矛盾之处，请重新确认。#"+RelaOtherPeoples+"^";
                 System.out.println("连带被保险人数与配偶、子女及其它参保人员人数存在矛盾之处，请重新确认。");
        }
        return true;
    }

    private boolean checkImpartParamModle() {
       String ImpartParamModle= tLCCustomerImpartSchema.getImpartParamModle();
        if (ImpartParamModle == null ||
            "".equals(ImpartParamModle.trim())) {
            ImpartParamModle="";
            GrpData = GrpData + "缴费主体未填。#"+""+"^";
            System.out.println("缴费主体未填");
        }
        return true;
    }

    private boolean checkPayIntv() {
       int PayIntv= tLCGrpContSchema.getPayIntv();

        if (PayIntv != -1 ||
            PayIntv != 0 ||
            PayIntv != 1 ||
            PayIntv != 3 ||
            PayIntv != 6 ||
            PayIntv != 12) {

            GrpData = GrpData + "缴费频次未填。#"+tLCGrpContSchema.getPayIntv()+"^";
            System.out.println("缴费频次未填");
        }
        return true;
    }

    private boolean checkPayMode() {
        String PayMode=tLCGrpContSchema.getPayMode();
        if (PayMode == null ||
            "".equals(PayMode.trim())
                ) {
            PayMode="";
            GrpData = GrpData + "缴费方式未填。#"+""+"^";
            System.out.println("缴费方式未填");
        }
        if(tLCGrpContSchema.getPayMode().equals("4") ){
      this.checkBankCode() ;
      this.checkAccName() ;
      this.checkBankAccNo() ;
      }

        return true;
    }

    private boolean checkBankCode()
    {
        String BankCode = tLCGrpContSchema.getBankCode() ;
        if (tLCGrpContSchema.getBankCode() == null ||
           "".equals(BankCode.trim())
               ) {
           BankCode="";
           GrpData = GrpData + "开户银行未填。#"+""+"^";
           System.out.println("开户银行未填");
        }
       return true;
    }

    private boolean checkAccName()
    {
        String AccName = tLCGrpContSchema.getAccName() ;
        if (tLCGrpContSchema.getAccName()  == null ||
           "".equals(AccName.trim())
               ) {
           AccName="";
           GrpData = GrpData + "开户银行户名未填。#"+""+"^";
           System.out.println("开户银行户名未填");
        }
      return true;
    }

    private boolean checkBankAccNo()
    {
        String BankAccNo = tLCGrpContSchema.getBankAccNo() ;
        if (tLCGrpContSchema.getBankAccNo()  == null ||
           "".equals(BankAccNo.trim())
               ) {
           BankAccNo="";
           GrpData = GrpData + "开户银行帐号未填。#"+""+"^";
           System.out.println("开户银行帐号未填");
        }

        return true;
    }
    private boolean checkCValiDate() {

        int maxCValiDate = chkLCGrpContSet.size() ;
        for (int n = 1; n <= maxCValiDate; n++) {
            LCGrpContSchema chkLCGrpContSchema = new LCGrpContSchema();
            chkLCGrpContSchema = chkLCGrpContSet.get(n);
            String CValiDate = chkLCGrpContSchema.getCValiDate();
            if (CValiDate == null) {
               CValiDate = "";
               //GrpData = GrpData + "保险责任生效日未填。#" + "" + "^";
               //System.out.println("保险责任生效日未填");
           }

            if (CValiDate.length() != 10) {
                GrpData = GrpData + "保险责任生效日期位数有误，请重新确认。#" + CValiDate + "^";
                System.out.println("保险责任生效日期位数有误，请重新确认。");
                return false;
            }
            String Year = CValiDate.substring(0, 4);
            String Month = CValiDate.substring(5, 7);
            String Day = CValiDate.substring(8);
            if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500) {
                GrpData = GrpData + "保险责任生效日期的年份有误，请重新确认。#" + CValiDate + "^";
                System.out.println("保险责任生效日期的年份有误，请重新确认。");
                return false;
            }
            if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12) {
                GrpData = GrpData + "保险责任生效日期的月份有误，请重新确认。#" + CValiDate + "^";
                System.out.println("保险责任生效日期的月份有误，请重新确认。");
                return false;
            }
            if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32) {
                GrpData = GrpData + "保险责任生效日期有误，请重新确认。#" + CValiDate + "^";
                System.out.println("保险责任生效日期有误，请重新确认。");
                return false;
            }
        }
        return true;
    }

    private boolean checkCInValiDate() {

        int maxCInValiDate = chkLCGrpContSet.size();
        for (int n = 1; n <= maxCInValiDate; n++) {
            LCGrpContSchema chkLCGrpContSchema = new LCGrpContSchema();
            chkLCGrpContSchema = chkLCGrpContSet.get(n);
            String CInValiDate = chkLCGrpContSchema.getCInValiDate();
            if (CInValiDate == null) {
              CInValiDate = "";
              //GrpData = GrpData + "保险责任终止日期未填。#" + "" + "^";
              //System.out.println("保险责任终止日期未填");
          }

            if (CInValiDate.length() != 10) {
                GrpData = GrpData + "保险责任终止日期位数有误，请重新确认。#" + CInValiDate + "^";
                System.out.println("保险责任终止日期位数有误，请重新确认");
                return false;
            }
            String Year = CInValiDate.substring(0, 4);
            String Month = CInValiDate.substring(5, 7);
            String Day = CInValiDate.substring(8);
            if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500) {
                GrpData = GrpData + "保险责任终止日期的年份有误，请重新确认。#" + CInValiDate + "^";
                System.out.println("保险责任终止日期的年份有误，请重新确认");
                return false;
            }
            if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12) {
                GrpData = GrpData + "保险责任终止日期的月份有误，请重新确认。#" + CInValiDate + "^";
                System.out.println("保险责任终止日期的月份有误，请重新确认。");
                return false;
            }
            if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32) {
                GrpData = GrpData + "保险责任终止日期有误，请重新确认。#" + CInValiDate + "^";
                System.out.println("保险责任终止日期有误，请重新确认。");
                return false;
            }
        }
        return true;
    }

    private boolean checkEnterKind(){
        String  EnterKind=tLCGrpContSchema.getEnterKind() ;
        if (tLCGrpContSchema.getEnterKind() == null) {
            EnterKind = "";
        }

        if (EnterKind.equals( "1") ||
            EnterKind.equals( "2") ||
            EnterKind.equals( "3") ||
            EnterKind.equals( "4") ||
            EnterKind.equals( "5"))
            {

                GrpData = GrpData + "保障计划区分标准未填。#"+EnterKind+"^";
                System.out.println("保障计划区分标准未填");
        }

    return true;
    }

    private boolean checkPlanPeoples3() {
            int Peoples3= tLCContPlanSchema.getPeoples3();
            if (Peoples3 == 0) {
                Peoples3=0;
                GrpData = GrpData + "应保人数未填。#"+0+"^";
                System.out.println("应保人数未填");
            }

            return true;
    }


    private boolean checkPeoples2() {
        int Peoples2=tLCContPlanSchema.getPeoples2();
        if (Peoples2 == 0) {
            Peoples2=0;
            GrpData = GrpData + "参保人数未填。#" + 0 + "^";
            System.out.println("参保人数未填");
        }

        ExeSQL tExeSQL = new ExeSQL();
        String sql="";
        sql= "SELECT char(sum(Peoples2))  FROM LCCONTPLAN where GrpContNo='"+tLCContPlanSchema.getGrpContNo() +"'";
        String Num ="";
        try {
             Num = tExeSQL.getOneValue(sql);
        } catch (Exception ex) {
            System.out.println("未查询到结果！");
            Num = "0";
            ex.printStackTrace();
        }


        int tNum = Integer.parseInt(Num);
        if (tNum >
            (tLCGrpContSchema.getPeoples3() + tLCGrpContSchema.getRelaPeoples())) {
            GrpData = GrpData + "所有保障计划的参保人数合计不应超过被保险人和连带被保险人之和。#" + tNum + "^";
            System.out.println("所有保障计划的参保人数合计不应超过被保险人和连带被保险人之和");
            System.out.println(tNum);
            System.out.println(tLCGrpContSchema.getPeoples3() + tLCGrpContSchema.getRelaPeoples());
        }

        return true;


    }

    private boolean checkRiskCode() {
        String RiskCode=tLCContPlanRiskSchema.getRiskCode();
        if (RiskCode == null ||
            "".equals(RiskCode.trim())
                ) {
            RiskCode="";
             GrpData = GrpData + "险种代码未填。#" + "" + "^";
             System.out.println("险种代码未填");
        }
        return true;
    }

    private boolean checkRiskVersion() {
        String RiskVersion=tLCContPlanRiskSchema.getRiskVersion() ;
        if (RiskVersion == null ||
            "".equals(RiskVersion.trim())
                ) {
            RiskVersion="";
             GrpData = GrpData + "险种名称未填。#" + "" + "^";
             System.out.println("险种名称未填");
        }
        return true;
    }

    private boolean checkHandlerName() {
        String HandlerName=tLCGrpContSchema.getHandlerName();
        if (HandlerName == null ||
            "".equals(HandlerName.trim())
                ) {
            HandlerName="";
            GrpData = GrpData + "经办人姓名未填。#" + "" + "^";
            System.out.println("经办人姓名未填");
        }
        return true;
    }

    private boolean checkHandlerPrint() {
        String HandlerPrint=tLCGrpContSchema.getHandlerPrint();
        if (HandlerPrint == null ||
            "".equals(HandlerPrint.trim())
                ) {
            HandlerPrint="";
            GrpData = GrpData + "投保单位盖章内容未填。#" + "" + "^";
            System.out.println("投保单位盖章内容未填");
        }
        return true;
    }

    private boolean checkHandlerDate() {

        int maxHandlerDate = chkLCGrpContSet.size();
        for (int n = 1; n <= maxHandlerDate; n++) {
            LCGrpContSchema chkLCGrpContSchema = new LCGrpContSchema();
            chkLCGrpContSchema = chkLCGrpContSet.get(n);
            String HandlerDate = chkLCGrpContSchema.getCInValiDate();
            if (HandlerDate == null) {
               HandlerDate = "";
               //GrpData = GrpData + "投保单位填写日期未填。#" + "" + "^";
               //System.out.println("投保单位填写日期未填。");
           }

            if (HandlerDate.length() != 10) {
                GrpData = GrpData + "投保单位填写日期位数有误，请确认。#" + HandlerDate + "^";
                System.out.println("投保单位填写日期位数有误，请确认。");
                return false;
            }
            String Year = HandlerDate.substring(0, 4);
            String Month = HandlerDate.substring(5, 7);
            String Day = HandlerDate.substring(8);
            if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500) {
                GrpData = GrpData + "投保单位填写日期的年份有误，请确认。#" + HandlerDate + "^";
                System.out.println("投保单位填写日期的年份有误，请确认。");
                return false;
            }
            if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12) {
                GrpData = GrpData + "投保单位填写日期的月份有误，请确认。#" + HandlerDate + "^";
                System.out.println("投保单位填写日期的月份有误，请确认。");
                return false;
            }
            if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32) {
                GrpData = GrpData + "投保单位填写日期有误，请确认。#" + HandlerDate + "^";
                System.out.println("投保单位填写日期有误，请确认。");
                return false;
            }
        }
        return true;
    }


    private boolean checkAgentDate() {

        int maxAgentDate = chkLCGrpContSet.size();
        for (int n = 1; n <= maxAgentDate; n++) {
            LCGrpContSchema chkLCGrpContSchema = new LCGrpContSchema();
            chkLCGrpContSchema = chkLCGrpContSet.get(n);
            String AgentDate = chkLCGrpContSchema.getCInValiDate();
            if (AgentDate == null) {
              AgentDate = "";
              //GrpData = GrpData + "业务员填写日期未填。#" + "" + "^";
              //System.out.println("业务员填写日期未填");
}

            if (AgentDate.length() != 10) {
                GrpData = GrpData + "业务员填写日期位数有误，请确认。#" + AgentDate + "^";
                System.out.println("业务员填写日期位数有误，请确认。");
                return false;
            }
            String Year = AgentDate.substring(0, 4);
            String Month = AgentDate.substring(5, 7);
            String Day = AgentDate.substring(8);
            if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500) {
                GrpData = GrpData + "业务员填写日期的年份有误，请确认。#" + AgentDate + "^";
                System.out.println("业务员填写日期的年份有误，请确认。");
                return false;
            }
            if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12) {
                GrpData = GrpData + "业务员填写日期的月份有误，请确认。#" + AgentDate + "^";
                System.out.println("业务员填写日期的月份有误，请确认。");
                return false;
            }
            if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32) {
                GrpData = GrpData + "业务员填写日期有误，请确认。#" + AgentDate + "^";
                System.out.println("业务员填写日期有误，请确认。");
                return false;
            }
        }
        return true;
    }

    private boolean checkAgentCode() {
        String AgentCode=tLCGrpContSchema.getAgentCode();
        if (AgentCode == null ||
            "".equals(AgentCode.trim())
                ) {
            AgentCode="";
            GrpData = GrpData + "业务员编码未填。#" + "" + "^";
            System.out.println("业务员编码未填");
        }
        return true;
    }

    private boolean checkname() {
        String Name=tLAAgentSchema.getName();
        if (Name == null ||
            "".equals(Name.trim())
                ) {
            Name="";
            GrpData = GrpData + "业务员姓名未填。#" + "" + "^";
            System.out.println("业务员姓名未填");
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */




    public String getGrpData() {
        return this.GrpData;
    }





}
