package com.sinosoft.lis.tb;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
public class GroupContBLS {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /**业务处理变量*/
  private LCGrpContSchema mLCGrpContSchema=new LCGrpContSchema();
  private LCGrpAppntSchema mLCGrpAppntSchema=new LCGrpAppntSchema();
  private LCGrpAddressSchema mLCGrpAddressSchema=new LCGrpAddressSchema();
  private LDGrpSchema mLDGrpSchema =new LDGrpSchema();
  /**构造子*/
  public GroupContBLS() {
  }
  /**
   * 根据操作类型和操作数据操作数据库,保存用户提交
   * @param cInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData( VData cInputData, String cOperate ){
    //将数据取到本类变量中
    mInputData=(VData)cInputData.clone();
    this.mOperate=cOperate;
    //将VData数据还原成业务需要的类
    if(!getInputData()){
      return false;
    }
    //保存修改
    if(!saveGroupCont()){
      return false;
    }
    System.out.println("GroupContBLS--submitData-->successful");
    return true;
  }
  /**
   * 分解上一层传来的业务数据
   * @return boolean
   */
  private boolean getInputData(){
    //团体保单实例
    mLCGrpContSchema.setSchema((LCGrpContSchema)mInputData.getObjectByObjectName(
      "LCGrpContSchema",0));
    //团单投保人实例
    mLCGrpAppntSchema.setSchema((LCGrpAppntSchema)mInputData.getObjectByObjectName(
      "LCGrpAppntSchema",0)) ;
    //团体客户地址实例
    mLCGrpAddressSchema.setSchema((LCGrpAddressSchema)mInputData.getObjectByObjectName(
      "LCGrpAddressSchema",0));
    //团体客户实例
    mLDGrpSchema.setSchema((LDGrpSchema)mInputData.getObjectByObjectName(
      "LDGrpSchema",0));
    return true;
  }
  /**
   * 保存修改；判断操作类型，决定调用相应的操作
   * 数据库连接在此取得并且关闭
   * @return boolean
   */
  private boolean saveGroupCont(){
    boolean tReturn=false;
    Connection con=null;
    con=DBConnPool.getConnection();
    if(con==null){
      mErrors.addOneError(new CError("数据库连接建立失败"));
      return false;
    }
    try{
      con.setAutoCommit(false);
    }catch(Exception e){
      e.printStackTrace();
      mErrors.addOneError(new CError("setAutoCommit=false失败"));
      return false;
    }
    if(this.mOperate.equals("INSERT||GROUPPOL")){
      tReturn=insertGroupCont(con);
    }
    if(this.mOperate.equals("UPDATE||GROUPPOL")){
      tReturn=updateGroupCont(con);
    }
    if(this.mOperate.equals("DELETE||GROUPPOL")){
      tReturn=deleteGroupCont(con);
    }

    try{
      //事务操作失败则回滚
      if(tReturn==false){
        con.rollback();
      }else{
        con.commit();
      }
    }catch(Exception e){
      mErrors.addOneError(new CError("事务操作发生意外"));
      tReturn=false;
    }finally{
      try{
        con.setAutoCommit(true) ;
        con.close();
      }catch(Exception e){  }
    }
    return tReturn;
  }
  /**
   * 增加团单,最多需要向4张表中插入数据
   * 如果用户提交的数据中客户号码是从查询中取得的,那么
   * 客户地址表和团体客户表只需要做更新操作
   * @param con Connection
   * @return boolean
   */
  private boolean insertGroupCont(Connection con){
    //插入团体保单实例
    LCGrpContDB tLCGrpContDB=new LCGrpContDB(con);
    tLCGrpContDB.setSchema(mLCGrpContSchema);
    if(!tLCGrpContDB.insert()){
      mErrors.copyAllErrors(tLCGrpContDB.mErrors);
      return false;
    }
    //插入团单投保人实例
    LCGrpAppntDB tLCGrpAppntDB=new LCGrpAppntDB(con);
    tLCGrpAppntDB.setSchema(mLCGrpAppntSchema);
    if(!tLCGrpAppntDB.insert()){
      mErrors.copyAllErrors(tLCGrpAppntDB.mErrors);
      return false;
    }
    //插入团体客户地址,如果表中已经存在相同主键的纪录，则执行update
    LCGrpAddressDB tLCGrpAddressDB=new LCGrpAddressDB(con);
    tLCGrpAddressDB.setSchema(mLCGrpAddressSchema);
    if(tLCGrpAddressDB.getInfo()){
      if(!tLCGrpAddressDB.update()){
        mErrors.copyAllErrors(tLCGrpAddressDB.mErrors);
        return false;
      }
    }else{
      if (!tLCGrpAddressDB.insert()) {
        mErrors.copyAllErrors(tLCGrpAddressDB.mErrors);
        return false;
      }
    }
    //插入团体客户实例,如果表中已经存在相同主键的纪录，则执行update
    LDGrpDB tLDGrpDB=new LDGrpDB(con);
    tLDGrpDB.setSchema(mLDGrpSchema);
    if(tLDGrpDB.getInfo()){
      if(!tLDGrpDB.update()){
        mErrors.copyAllErrors(tLDGrpDB.mErrors);
        return false;
      }
    }else{
      if(!tLDGrpDB.insert()){
        mErrors.copyAllErrors(tLDGrpDB.mErrors);
        return false;
      }
    }

    return true;
  }
  /**
   * 修改团体保单
   * 如果操作员没有录入客户号码,需要在客户地址表和团体客户表中增加记录
   * @param con Connection
   * @return boolean
   */
  private boolean updateGroupCont(Connection con){
    //修改团体保单实例
    LCGrpContDB tLCGrpContDB=new LCGrpContDB(con);
    tLCGrpContDB.setSchema(mLCGrpContSchema);
    if(!tLCGrpContDB.insert()){
      mErrors.copyAllErrors(tLCGrpContDB.mErrors);
      return false;
    }
    //修改团单投保人实例
    LCGrpAppntDB tLCGrpAppntDB=new LCGrpAppntDB(con);
    tLCGrpAppntDB.setSchema(mLCGrpAppntSchema);
    if(!tLCGrpAppntDB.insert()){
      mErrors.copyAllErrors(tLCGrpAppntDB.mErrors);
      return false;
    }
    //插入团体客户地址,如果表中没有相同主键的纪录，则执行insert
    LCGrpAddressDB tLCGrpAddressDB=new LCGrpAddressDB(con);
    tLCGrpAddressDB.setSchema(mLCGrpAddressSchema);
    if(tLCGrpAddressDB.getInfo()){
      if(!tLCGrpAddressDB.update()){
        mErrors.copyAllErrors(tLCGrpAddressDB.mErrors);
        return false;
      }
    }else{
      if (!tLCGrpAddressDB.insert()) {
        mErrors.copyAllErrors(tLCGrpAddressDB.mErrors);
        return false;
      }
    }
    //插入团体客户实例,如果表中没有相同主键的纪录，则执行insert
    LDGrpDB tLDGrpDB=new LDGrpDB(con);
    tLDGrpDB.setSchema(mLDGrpSchema);
    if(tLDGrpDB.getInfo()){
      if(!tLDGrpDB.update()){
        mErrors.copyAllErrors(tLDGrpDB.mErrors);
        return false;
      }
    }else{
      if(!tLDGrpDB.insert()){
        mErrors.copyAllErrors(tLDGrpDB.mErrors);
        return false;
      }
    }

    return true;
  }
  /**
   * 删除团单
   * @return boolean
   */
  private boolean deleteGroupCont(Connection con){
    mLCGrpAppntSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
    //保单保险计划
    LCContPlanSchema mLCContPlanSchema=new LCContPlanSchema();
    //保单险种保险计划
    LCContPlanRiskSchema mLCContPlanRiskSchema=new LCContPlanRiskSchema();
    //总括保单处理分单表
    LCGeneralSchema mLCGeneralSchema=new LCGeneralSchema();
    //集体险种表
    LCGrpPolSchema mLCGrpPolSchema=new LCGrpPolSchema();
     //保险计划责任
    LCContPlanDutySchema mLCContPlanDutySchema=new LCContPlanDutySchema();
    //保险计划责任交费
    LCContPlanDutyPaySchema mLCContPlanDutyPaySchema=new LCContPlanDutyPaySchema();
    //保险计划责任给付
    LCContPlanDutyGetSchema mLCContPlanDutyGetSchema=new LCContPlanDutyGetSchema();
    //总括保单处理险种关联表
    LCGeneralToRiskSchema mLCGeneralToRiskSchema=new LCGeneralToRiskSchema();
    //个人保单表
    LCContSchema mLCContSchema=new LCContSchema();
    //个单投保人表
    LCAppntSchema mLCAppntSchema=new LCAppntSchema();
    //客户告知
    LCCustomerImpartSchema mLCCustomerImpartSchema=new LCCustomerImpartSchema();
    //个单被保人表
    LCInsuredSchema mLCInsuredSchema=new LCInsuredSchema();
    //个人险种表
    LCPolSchema mLCPolSchema=new LCPolSchema();
    //个单连带被保人表
    LCInsuredRelatedSchema mLCInsuredRelatedSchema=new LCInsuredRelatedSchema();
    //受益人
    LCBnfSchema mLCBnfSchema=new LCBnfSchema();
    //保单险种责任表
    LCDutySchema mLCDutySchema=new LCDutySchema();
    //领取项表
    LCGetSchema mLCGetSchema=new LCGetSchema();
    //保费项表
    LCPremSchema mLCPremSchema=new LCPremSchema();
//    LCPolSchema mLCPolSchema=new LCPolSchema();
//    LCPolSchema mLCPolSchema=new LCPolSchema();
//    LCPolSchema mLCPolSchema=new LCPolSchema();
//    LCPolSchema mLCPolSchema=new LCPolSchema();
//    LCPolSchema mLCPolSchema=new LCPolSchema();
//    LCPolSchema mLCPolSchema=new LCPolSchema();
    return true;
  }
}
