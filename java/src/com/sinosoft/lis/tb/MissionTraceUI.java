/**
 * 2008-8-25
 */
package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class MissionTraceUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public MissionTraceUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            MissionTraceBL tMissionTraceBL = new MissionTraceBL();
            if (!tMissionTraceBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tMissionTraceBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
