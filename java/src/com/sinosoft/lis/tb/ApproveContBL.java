package com.sinosoft.lis.tb;

import com.sinosoft.lis.bl.LCAppntBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.Vector;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.cbcheck.RecalculationPremBL;
import com.sinosoft.lis.config.OLDBankRateBL;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: BL层业务逻辑处理类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class ApproveContBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局数据 */

	private GlobalInput mGlobalInput = new GlobalInput();

	private MMap map = new MMap();

	// 统一更新日期，时间
	private String theCurrentDate = PubFun.getCurrentDate();

	private String theCurrentTime = PubFun.getCurrentTime();

	/** 业务处理相关变量 */
	private LCContSchema mLCContSchema = new LCContSchema();

	private LCApproveSchema mLCApprove = new LCApproveSchema();

	private LCApproveSchema moldLCApprove = new LCApproveSchema();

	private LCApproveDB mLCApproveDB = new LCApproveDB();

	private LCInsuredDB mLCInsuredDB = new LCInsuredDB();
	
	private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();

	private LCApproveSet mLCApproveSet = new LCApproveSet();

	private LCInsuredSet mLCInsuredSet = new LCInsuredSet();

	private String mCustomerType;

	// @Constructor
	public ApproveContBL() {
	}

	/**
	 * 数据提交的公共方法
	 * 
	 * @param: cInputData 传入的数据 cOperate 数据操作字符串
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将传入的数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		System.out.println("now in ApproveContBL submit");
		// 将外部传入的数据分解到本类的属性中，准备处理
		if (this.getInputData() == false) {
			return false;
		}
		System.out.println("---getInputData---");

		// 根据业务逻辑对数据进行处理

		if (this.dealData() == false) {
			return false;
		}

		// 装配处理好的数据，准备给后台进行保存
		this.prepareOutputData();
		System.out.println("---prepareOutputData---");

		// 数据提交、保存

		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start tPRnewManualDunBLS Submit...");

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);

			CError tError = new CError();
			tError.moduleName = "ContBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		System.out.println("---commitData---");

		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		try {
			// 全局变量
			mGlobalInput.setSchema((GlobalInput) mInputData
					.getObjectByObjectName("GlobalInput", 0));
			// 合同表
			mLCApprove.setSchema((LCApproveSchema) mInputData
					.getObjectByObjectName("LCApproveSchema", 0));
			mLCInsuredDB = (LCInsuredDB) mInputData
			.getObjectByObjectName("LCInsuredDB", 0);
			if (!mOperate.equals("DELETE||CONTINSURED")) {
				mLCInsuredSchema.setSchema((LCInsuredSchema) mInputData
						.getObjectByObjectName("LCInsuredSchema", 0));
			}
			
			return true;
		} catch (Exception ex) {
			CError tError = new CError();
			tError.moduleName = "ProposalBL";
			tError.functionName = "checkData";
			tError.errorMessage = ex.toString();
			this.mErrors.addOneError(tError);
			return false;

		}

	}

	/**
	 * 根据业务逻辑对数据进行处理
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		mCustomerType = mLCApprove.getCustomerType();
		String tPrtNo = mLCApprove.getPrtNo();
		System.out.println("复录的客户类型为：" + mCustomerType);
		if (mCustomerType.equals("00")) {
			LCContDB tLCContDB = new LCContDB();
			LCContSet tLCContSet = tLCContDB
					.executeQuery("select * from lccont where prtno='" + tPrtNo
							+ "'");
			if (tLCContSet.size() > 0) {
				mLCContSchema = tLCContSet.get(1);
				mLCApproveSet = mLCApproveDB
						.executeQuery("select * from LCApprove where prtno='"
								+ tPrtNo + "' and customertype='00'");
				if (mLCApproveSet.size() > 0) {
					moldLCApprove = mLCApproveSet.get(1);
					mLCApprove.setMakeDate(moldLCApprove.getMakeDate());
					mLCApprove.setMakeTime(moldLCApprove.getMakeTime());
					map.put(moldLCApprove, "DELETE");
				} else {
					mLCApprove.setMakeDate(theCurrentDate);
					mLCApprove.setMakeTime(theCurrentTime);
				}
				mLCApprove.setAgentCode(mLCContSchema.getAgentCode());
				mLCApprove.setAgentCom(mLCContSchema.getAgentCom());
				mLCApprove.setAgentGroup(mLCContSchema.getAgentGroup());
				mLCApprove.setAgentSaleCode(mLCContSchema.getAgentSaleCode());
				mLCApprove.setBankAccName(mLCContSchema.getAccName());
				mLCApprove.setBankAccNo(mLCContSchema.getBankAccNo());
				mLCApprove.setBankCode(mLCContSchema.getBankCode());
				mLCApprove.setBrithday(mLCContSchema.getAppntBirthday());
				mLCApprove.setContType(mLCContSchema.getContType());
				mLCApprove.setCrs_BussType(mLCContSchema.getCrs_BussType());
				mLCApprove.setCrs_SaleChnl(mLCContSchema.getCrs_SaleChnl());
				mLCApprove.setCustomerNo(mLCContSchema.getAppntNo());
				mLCApprove.setGrpAgentCode(mLCContSchema.getGrpAgentCode());
				mLCApprove.setGrpAgentCom(mLCContSchema.getGrpAgentCom());
				mLCApprove.setGrpAgentIDNo(mLCContSchema.getGrpAgentIDNo());
				mLCApprove.setGrpAgentName(mLCContSchema.getGrpAgentName());
				mLCApprove.setIdNo(mLCContSchema.getAppntIDNo());
				mLCApprove.setIdType(mLCContSchema.getAppntIDType());
				mLCApprove.setName(mLCContSchema.getAppntName());
				mLCApprove.setPayIntv(mLCContSchema.getPayIntv());
				mLCApprove.setSaleChnl(mLCContSchema.getSaleChnl());
				mLCApprove.setSex(mLCContSchema.getAppntSex());
				mLCApprove.setModifyDate(theCurrentDate);
				mLCApprove.setModifyTime(theCurrentTime);
				mLCApprove.setOperator(mGlobalInput.Operator);
				mLCApprove.setManageCom(mGlobalInput.ManageCom);

			}
			map.put(mLCApprove, "INSERT");
			System.out.println("Map : " + map.size());
		} else {
			mLCApprove.setCustomerType("01");
			mLCApprove.setModifyDate(theCurrentDate);
			mLCApprove.setModifyTime(theCurrentTime);
			mLCApprove.setOperator(mGlobalInput.Operator);
			mLCApprove.setManageCom(mGlobalInput.ManageCom);
			
			if (mOperate.equals("Approve")) {
				LCApproveDB tLCApproveDB = new LCApproveDB();
			    tLCApproveDB.setPrtNo(tPrtNo);
			    tLCApproveDB.setCustomerType("01");
			    tLCApproveDB.setCustomerNo(mLCApprove.getCustomerNo());
			    if(tLCApproveDB.getInfo()){
			    	moldLCApprove= tLCApproveDB.getSchema();
			    	mLCApprove.setMakeDate(moldLCApprove.getMakeDate());
					mLCApprove.setMakeTime(moldLCApprove.getMakeTime());
					map.put(moldLCApprove, "DELETE");
			    }
			    LCInsuredDB tLCContDB = new LCInsuredDB();
				LCInsuredSet tLCContSet = tLCContDB
						.executeQuery("select * from lcinsured where prtno='" + tPrtNo
								+ "' and insuredno='"+mLCApprove.getCustomerNo()+"' ");
				if (tLCContSet.size() > 0) {
					LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
					tLCInsuredSchema = tLCContSet.get(1);
					mLCApproveSet = mLCApproveDB
							.executeQuery("select * from LCApprove where prtno='"
									+ tPrtNo + "' and customertype='01' and customerno='"+mLCApprove.getCustomerNo()+"' ");
					if (mLCApproveSet.size() > 0) {
						moldLCApprove = mLCApproveSet.get(1);
						mLCApprove.setMakeDate(moldLCApprove.getMakeDate());
						mLCApprove.setMakeTime(moldLCApprove.getMakeTime());
						map.put(moldLCApprove, "DELETE");
					} else {
						mLCApprove.setMakeDate(theCurrentDate);
						mLCApprove.setMakeTime(theCurrentTime);
					}
					
					mLCApprove.setBrithday(tLCInsuredSchema.getBirthday());
					mLCApprove.setCustomerNo(tLCInsuredSchema.getInsuredNo());
					mLCApprove.setIdNo(tLCInsuredSchema.getIDNo());
					mLCApprove.setIdType(tLCInsuredSchema.getIDType());
					mLCApprove.setName(tLCInsuredSchema.getName());
					mLCApprove.setSex(tLCInsuredSchema.getSex());
					mLCApprove.setRelation(tLCInsuredSchema.getRelationToAppnt());
				}
				map.put(mLCApprove, "INSERT");
			                   
			} else {
				if (mOperate.equals("INSERT||CONTINSURED")) {
					mLCApprove.setCustomerNo(mLCInsuredSchema.getInsuredNo());
					mLCApprove.setName(mLCInsuredSchema.getName());
					mLCApprove.setSex(mLCInsuredSchema.getSex());
					mLCApprove.setBrithday(mLCInsuredSchema.getBirthday());
					mLCApprove.setIdNo(mLCInsuredSchema.getIDNo());
					mLCApprove.setIdType(mLCInsuredSchema.getIDType());
					mLCApprove.setRelation(mLCInsuredSchema.getRelationToAppnt());
					mLCApprove.setMakeDate(theCurrentDate);
					mLCApprove.setMakeTime(theCurrentTime);
					map.put(mLCApprove, "INSERT");
				} else if (mOperate.equals("DELETE||CONTINSURED")) {
				    LCApproveDB tLCApproveDB = new LCApproveDB();
				    tLCApproveDB.setPrtNo(tPrtNo);
				    tLCApproveDB.setCustomerType("01");
				    tLCApproveDB.setCustomerNo(mLCInsuredDB.getInsuredNo());
				    if(tLCApproveDB.getInfo()){
				    	mLCApprove= tLCApproveDB.getSchema();
						map.put(mLCApprove, "DELETE");
				    }					
				} else if (mOperate.equals("UPDATE||CONTINSURED")) {
					mLCApprove.setCustomerNo(mLCInsuredSchema.getInsuredNo());
					mLCApprove.setName(mLCInsuredSchema.getName());
					mLCApprove.setSex(mLCInsuredSchema.getSex());
					mLCApprove.setBrithday(mLCInsuredSchema.getBirthday());
					mLCApprove.setIdNo(mLCInsuredSchema.getIDNo());
					mLCApprove.setIdType(mLCInsuredSchema.getIDType());
					mLCApprove.setRelation(mLCInsuredSchema.getRelationToAppnt());
					LCApproveDB tLCApproveDB = new LCApproveDB();
				    tLCApproveDB.setPrtNo(tPrtNo);
				    tLCApproveDB.setCustomerType("01");
				    tLCApproveDB.setCustomerNo(mLCInsuredDB.getInsuredNo());				    
					if (tLCApproveDB.getInfo()) {
						moldLCApprove = tLCApproveDB.getSchema();
						mLCApprove.setMakeDate(moldLCApprove.getMakeDate());
						mLCApprove.setMakeTime(moldLCApprove.getMakeTime());
						map.put(moldLCApprove, "DELETE");
					} else {
						mLCApprove.setMakeDate(theCurrentDate);
						mLCApprove.setMakeTime(theCurrentTime);
					}
					mLCApprove.setCustomerNo(mLCInsuredSchema.getInsuredNo());
					map.put(mLCApprove, "INSERT");
				}

			}

			System.out.println("Map : " + map.size());
		}

		return true;
	}

	/**
	 * 根据业务逻辑对数据进行处理
	 * 
	 */
	private void prepareOutputData() {
		mInputData.clear();
		mInputData.add(map);
	}

	/**
	 * 得到处理后的结果集
	 * 
	 * @return 结果集
	 */

	public VData getResult() {
		return mResult;
	}

}
