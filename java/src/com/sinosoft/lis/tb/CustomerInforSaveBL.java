package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2019-08-06
 */
public class CustomerInforSaveBL
{
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	String prtno ="";

	// @Constructor
	public CustomerInforSaveBL()
	{
	}

	/**
	 * 数据提交的公共方法
	 *
	 * @param: cInputData 传入的数据 cOperate 数据操作字符串
	 * @param cOperate String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		// 将传入的数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		System.out.println("now in ContBL submit");
		// 将外部传入的数据分解到本类的属性中，准备处理
		if (this.getInputData() == false)
		{
			return false;
		}
		System.out.println("---getInputData---");
		// 根据业务逻辑对数据进行处理

		if (this.dealData() == false)
		{
			return false;
		}

		System.out.println("---prepareOutputData---");

		//　数据提交、保存
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "CustomerInforSaveBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 *
	 * @return boolean
	 */
	private boolean getInputData()
	{       
		TransferData tTransferData = (TransferData) mInputData
				.getObjectByObjectName("TransferData", 0);
		prtno = (String) tTransferData.getValueByName("prtNo");
		LCContDB tlContDB = new LCContDB();
		LCContSet tLcContSet = tlContDB.executeQuery("select 1 from lccont where prtno ='"+prtno+"' union select 1 from lbcont where prtno ='"+prtno+"' ");
		if(tLcContSet!=null &&tLcContSet.size()<=0){
			CError tError = new CError();
			tError.moduleName = "CustomerInforSaveBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "该印刷号不存在，请检查！";
			this.mErrors.addOneError(tError);
			return false;	
		}
		return true;

	}


	/**
	 * 根据业务逻辑对数据进行处理
	 *
	 * @return boolean
	 */
	private boolean dealData()
	{
		mInputData.clear();
		ES_DOC_MAINDB tEs_DOC_MAINDB = new ES_DOC_MAINDB();
		tEs_DOC_MAINDB.setDocCode(prtno);
		tEs_DOC_MAINDB.setSubType("TB32");
		ES_DOC_MAINSet tEs_DOC_MAINSet = tEs_DOC_MAINDB.query();
		if(tEs_DOC_MAINSet!=null&&tEs_DOC_MAINSet.size()!=1){
			CError tError = new CError();
			tError.moduleName = "CustomerInforSaveBL";
			tError.functionName = "dealData";
			tError.errorMessage = "该保单未上传相应客户回销资料，请核实！";
			this.mErrors.addOneError(tError);
			return false;	
		}else{
			ES_DOC_MAINSchema tEs_DOC_MAINSchema = tEs_DOC_MAINSet.get(1);
			tEs_DOC_MAINSchema.setState("06");
			MMap mMap = new MMap();
			mMap.put(tEs_DOC_MAINSchema, "UPDATE");
			mInputData.add(mMap);
		}
		return true;
	}

	/**
	 * 得到处理后的结果集
	 * @return 结果集
	 */

	public VData getResult()
	{
		return mResult;
	}

}
