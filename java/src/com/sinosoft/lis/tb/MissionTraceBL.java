/**
 * 2008-8-25
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.vschema.LBMissionSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class MissionTraceBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mMissionID = null;

    private String mSubMissionID = null;

    private String mActivityID = null;

    public MissionTraceBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null || mMissionID.equals(""))
        {
            buildError("getInputData", "工作流信息缺失。");
            return false;
        }

        mSubMissionID = (String) mTransferData.getValueByName("SubMissionID");
        if (mSubMissionID == null || mSubMissionID.equals(""))
        {
            buildError("getInputData", "工作流信息缺失。");
            return false;
        }

        mActivityID = (String) mTransferData.getValueByName("ActivityID");
        if (mActivityID == null || mActivityID.equals(""))
        {
            buildError("getInputData", "工作流信息缺失。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        LWMissionSet tLWMissionSet = getMissionTrace(mMissionID, mSubMissionID,
                mActivityID);
        if (tLWMissionSet.size() <= 0)
        {
            buildError("getInputData", "未找到要处理的工作流信息。");
            return false;
        }

        // 备份要处理的工作流。
        tTmpMap = backMissionTrace(tLWMissionSet);
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        // ---------------------

        // 备份要处理的工作流。
        tTmpMap = delMissionTrace(tLWMissionSet);
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        // ---------------------

        return true;
    }

    /**
     * 获取待处理工作流信息。
     * @param cMissionID
     * @param cSubMissionID
     * @param cActivityID
     * @return
     */
    private LWMissionSet getMissionTrace(String cMissionID,
            String cSubMissionID, String cActivityID)
    {
        String tStrSql = " select * from LWMission lwm "
                + " where MissionID = '" + cMissionID + "' "
                + " and SubMissionID = '" + cSubMissionID + "' "
                + " and ActivityID = '" + cActivityID + "' ";

        LWMissionDB tLWMissionDB = new LWMissionDB();

        return tLWMissionDB.executeQuery(tStrSql);
    }

    /**
     * 备份工作流数据。
     * @param cLWMissionSet
     * @return
     */
    private MMap backMissionTrace(LWMissionSet cLWMissionSet)
    {
        MMap tMMap = null;

        try
        {
            tMMap = new MMap();

            LBMissionSet tLBMissionSet = new LBMissionSet();
            Reflections tReflections = new Reflections();

            for (int i = 1; i <= cLWMissionSet.size(); i++)
            {
                String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);

                LBMissionSchema tLBMissionSchema = new LBMissionSchema();
                tReflections
                        .transFields(tLBMissionSchema, cLWMissionSet.get(i));
                tLBMissionSchema.setSerialNo(tSerielNo);

                tLBMissionSet.add(tLBMissionSchema);
            }
            tMMap.put(tLBMissionSet, SysConst.INSERT);
        }
        catch (Exception ex)
        {
            buildError("backMissionTrace", ex.getMessage());
            return null;
        }

        return tMMap;
    }

    /**
     * 删除要处理的工作流。
     * @param cLWMissionSet
     * @return
     */
    private MMap delMissionTrace(LWMissionSet cLWMissionSet)
    {
        MMap tMMap = new MMap();

        tMMap.put(cLWMissionSet, SysConst.DELETE);

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWQuestBackBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
