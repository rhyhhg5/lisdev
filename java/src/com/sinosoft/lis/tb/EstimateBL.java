/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.tb;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LCEstimateDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCEstimateSchema;
import com.sinosoft.lis.vschema.LCEstimateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class EstimateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	//录入
	private LCEstimateSchema mLCEstimateSchema = new LCEstimateSchema();
	private String mGrpContNo = "";


	private MMap mMap = new MMap();

	public EstimateBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		
		if(!checkData()){
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "EstimateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败EstimateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start EstimateBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "EstimateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	////////////////////////////////////////////////
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mLCEstimateSchema = (LCEstimateSchema) cInputData.getObjectByObjectName("LCEstimateSchema", 0);
		this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		return true;

	}
	
	private boolean checkData() {
		mGrpContNo = mLCEstimateSchema.getGrpContNo();
		if(mGrpContNo == null || "".equals(mGrpContNo)){
			CError tError = new CError();
			tError.moduleName = "EstimateBL";
			tError.functionName = "dealData";
			tError.errorMessage = "未获取到需要处理的保单！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(mLCEstimateSchema.getProjectName() == null || "".equals(mLCEstimateSchema.getProjectName())){
			CError tError = new CError();
			tError.moduleName = "EstimateBL";
			tError.functionName = "dealData";
			tError.errorMessage = "未获取到需要处理的项目名称！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData() {
		
		if("INSERT||MAIN".equals(mOperate)){
			SSRS tSSRS = getPremAndPayNo();
			if(tSSRS == null || tSSRS.MaxRow != 1){
				CError tError = new CError();
				tError.moduleName = "EstimateBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取保单首期保费失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			String tSumPrem = tSSRS.GetText(1, 1);
			String tPayNo = tSSRS.GetText(1, 2);
			LCEstimateSet tLCEstimateSet = getLCEstimateSet(tSumPrem,tPayNo);
			mMap.put(tLCEstimateSet, SysConst.INSERT);
		} else if("UPDATE||MAIN".equals(mOperate)){
			LCEstimateSet tLCEstimateSet = new LCEstimateSet();
			String tCurrentDate = PubFun.getCurrentDate();
			String tSQL = "select * from LCEstimate where grpcontno = '"+mGrpContNo+"' and makedate = '"+tCurrentDate+"' ";
			LCEstimateDB aLCEstimateDB = new LCEstimateDB();
			tLCEstimateSet = aLCEstimateDB.executeQuery(tSQL);
			if(tLCEstimateSet == null || tLCEstimateSet.size() == 0){
				SSRS tSSRS = getPremAndPayNo();
				if(tSSRS == null || tSSRS.MaxRow != 1){
					CError tError = new CError();
					tError.moduleName = "EstimateBL";
					tError.functionName = "dealData";
					tError.errorMessage = "获取保单首期保费失败！";
					this.mErrors.addOneError(tError);
					return false;
				}
				String tSumPrem = tSSRS.GetText(1, 1);
				String tPayNo = tSSRS.GetText(1, 2);
				tLCEstimateSet = getLCEstimateSet(tSumPrem,tPayNo);
				if(tLCEstimateSet == null || tLCEstimateSet.size() == 0){
					CError tError = new CError();
					tError.moduleName = "EstimateBL";
					tError.functionName = "dealData";
					tError.errorMessage = "修改评估信息时，因日期变更，生成新数据失败！";
					this.mErrors.addOneError(tError);
					return false;
				}
				mMap.put(tLCEstimateSet, SysConst.INSERT);
			}else{
				String tContEstimatePrem = mLCEstimateSchema.getContEstimatePrem();
				if(tContEstimatePrem == null || "".equals(tContEstimatePrem)){
					tContEstimatePrem = "0";
				}
				String aManageCom = mGlobalInput.ManageCom;
				String aOperator = mGlobalInput.Operator;
				String aCurrentDate = PubFun.getCurrentDate();
				String aCurrentTime = PubFun.getCurrentTime();
				for(int i=1;i<=tLCEstimateSet.size();i++){
					tLCEstimateSet.get(i).setProjectName(mLCEstimateSchema.getProjectName());
					tLCEstimateSet.get(i).setProjectNo(mLCEstimateSchema.getProjectNo());
					tLCEstimateSet.get(i).setEstimateRate(mLCEstimateSchema.getEstimateRate());
					tLCEstimateSet.get(i).setEstimateBalanceRate(mLCEstimateSchema.getEstimateBalanceRate());
					tLCEstimateSet.get(i).setContEstimatePrem(mLCEstimateSchema.getContEstimatePrem());
					tLCEstimateSet.get(i).setBackEstimatePrem(mLCEstimateSchema.getBackEstimatePrem());
					tLCEstimateSet.get(i).setReason(mLCEstimateSchema.getReason());
					
					String tRiskPremRate = tLCEstimateSet.get(i).getRiskPremRate();
					String[] tPremAndSumPrem = tRiskPremRate.split("/");
					String aRiskEstimatePrem = getRiskEstimatePrem(tContEstimatePrem,tPremAndSumPrem[0],tPremAndSumPrem[1]);
					tLCEstimateSet.get(i).setRiskEstimatePrem(aRiskEstimatePrem);
					
					tLCEstimateSet.get(i).setManageCom(aManageCom);
					tLCEstimateSet.get(i).setOperator(aOperator);
					tLCEstimateSet.get(i).setModifyDate(aCurrentDate);
					tLCEstimateSet.get(i).setModifyTime(aCurrentTime);
				}
				mMap.put(tLCEstimateSet, SysConst.UPDATE);
			}
		}else	if ("DELETE||MAIN".equals(mOperate)){
			String tDeleteSQL = "delete from LCEstimate where grpcontno = '"+mGrpContNo+"' and makedate = '"+PubFun.getCurrentDate()+"' ";
			mMap.put(tDeleteSQL,SysConst.DELETE);
		}
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "EstimateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}
	
	private SSRS getPremAndPayNo(){
		SSRS aSSRS = new SSRS();
		String aSQL = "select sumactupaymoney,payno from ljapay where incomeno = '"+mGrpContNo+"' and duefeetype = '0' ";
		aSSRS = new ExeSQL().execSQL(aSQL);
		if(aSSRS == null || aSSRS.MaxRow != 1){
			CError tError = new CError();
			tError.moduleName = "EstimateBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取保单首期保费失败！";
			this.mErrors.addOneError(tError);
			return null;
		}
		return aSSRS;
	}
	
	private LCEstimateSet getLCEstimateSet(String aSumPrem,String aPayNo){
		LCEstimateSet aLCEstimateSet = new LCEstimateSet();
		String tRiskSQL = "select riskcode,sumduepaymoney from ljapaygrp where payno = '"+aPayNo+"' ";
		SSRS tRiskSSRS = new ExeSQL().execSQL(tRiskSQL);
		if(tRiskSSRS == null || tRiskSSRS.MaxRow < 1){
			CError tError = new CError();
			tError.moduleName = "EstimateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "结余返还拆分到险种时，获取险种信息失败。";
			this.mErrors.addOneError(tError);
			return null;
		}
		String aContEstimatePrem = mLCEstimateSchema.getContEstimatePrem();
		if(aContEstimatePrem == null || "".equals(aContEstimatePrem)){
			aContEstimatePrem = "0";
		}
		String aManageCom = mGlobalInput.ManageCom;
		String aOperator = mGlobalInput.Operator;
		String aCurrentDate = PubFun.getCurrentDate();
		String aCurrentTime = PubFun.getCurrentTime();
		for(int i=1;i<=tRiskSSRS.MaxRow;i++){
			LCEstimateSchema aLCEstimateSchema = new LCEstimateSchema();
			String tSerNo = PubFun1.CreateMaxNo("EstimateSerNo", 20);//自动生成最大序号
			aLCEstimateSchema.setSerNo(tSerNo);
			aLCEstimateSchema.setGrpContNo(mGrpContNo);
			aLCEstimateSchema.setProjectName(mLCEstimateSchema.getProjectName());
			aLCEstimateSchema.setProjectNo(mLCEstimateSchema.getProjectNo());
			aLCEstimateSchema.setEstimateRate(mLCEstimateSchema.getEstimateRate());
			aLCEstimateSchema.setEstimateBalanceRate(mLCEstimateSchema.getEstimateBalanceRate());
			aLCEstimateSchema.setContEstimatePrem(mLCEstimateSchema.getContEstimatePrem());
			aLCEstimateSchema.setBackEstimatePrem(mLCEstimateSchema.getBackEstimatePrem());
			aLCEstimateSchema.setRiskCode(tRiskSSRS.GetText(i, 1));
			
			String aRiskPremRate = tRiskSSRS.GetText(i, 2) + "/" + aSumPrem;//该字段意义不大，仅作参考
			aLCEstimateSchema.setRiskPremRate(aRiskPremRate);
			
			String aRiskEstimatePrem = getRiskEstimatePrem(aContEstimatePrem,tRiskSSRS.GetText(i, 2),aSumPrem);
			aLCEstimateSchema.setRiskEstimatePrem(aRiskEstimatePrem);
			aLCEstimateSchema.setReason(mLCEstimateSchema.getReason());
			aLCEstimateSchema.setManageCom(aManageCom);
			aLCEstimateSchema.setOperator(aOperator);
			aLCEstimateSchema.setMakeDate(aCurrentDate);
			aLCEstimateSchema.setMakeTime(aCurrentTime);
			aLCEstimateSchema.setModifyDate(aCurrentDate);
			aLCEstimateSchema.setModifyTime(aCurrentTime);
			aLCEstimateSet.add(aLCEstimateSchema);
		}
		return aLCEstimateSet;
	}
	
	private String getRiskEstimatePrem(String aContEstimatePrem,String aFirstRiskPrem,String aSumPrem){
		DecimalFormat df=new DecimalFormat("#.#");
		String aRiskEstimatePrem = "";
		aRiskEstimatePrem = String.valueOf(df.format(Arith.div(Arith.mul(Double.parseDouble(aContEstimatePrem), Double.parseDouble(aFirstRiskPrem)), Double.parseDouble(aSumPrem), 2)));
		return aRiskEstimatePrem;
	}
	
}