package com.sinosoft.lis.tb;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;


/**
 * <p>Title: 更新投保单险种操作类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author wzw
 * @version 1.0
 */
public class ReCalInsuredBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /**存放委派者传入数据的容器*/
    private VData mInputData;
    GlobalInput mGlobalInput = new GlobalInput();
    /**存放传递给委派者数据的容器*/
    private VData mResult = new VData();


    /**将数据和操作放在其中*/
    private MMap map = new MMap();


    //业务处理类，处理后的数据封装在MMap中传给外界
    //计算之前
    private LCAppntDB preLCAppntDB = new LCAppntDB();
    private LCGrpAppntDB preLCGrpAppntDB = new LCGrpAppntDB();
    private LCContSchema preLCContSchema = new LCContSchema();
    private LCInsuredSchema preLCInsuredSchema = new LCInsuredSchema();
    private LCPolSet preLCPolSet = new LCPolSet();
    private LCDutySet preLCDutySet = new LCDutySet();
    private LCPremSet preLCPremSet = new LCPremSet();
    private LCGetSet preLCGetSet = new LCGetSet();


    //计算之后
    private LCContSchema aftLCContSchema = new LCContSchema();
    private LCInsuredSchema aftLCInsuredSchema = new LCInsuredSchema();
    private LCPolSet aftLCPolSet = new LCPolSet();
    private LCDutySet aftLCDutySet = new LCDutySet();
    private LCPremSet aftLCPremSet = new LCPremSet();
    private LCGetSet aftLCGetSet = new LCGetSet();


    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    boolean updateContFlag = false;
    String updateLCContStr = "";
    private String mNewInsuredNo = null;
    public ReCalInsuredBL(LCInsuredSchema pLCInsuredSchema, LCPolSet tLCPolSet,
                          LCContSchema preLCContSchema,
                          GlobalInput tGlobalInput) {
        preLCPolSet.set(tLCPolSet);
        preLCInsuredSchema.setSchema(pLCInsuredSchema);
        mGlobalInput.setSchema(tGlobalInput);
    }

    public ReCalInsuredBL(LCInsuredSchema pLCInsuredSchema,
                          GlobalInput tGlobalInput) {
        preLCInsuredSchema.setSchema(pLCInsuredSchema);
        mGlobalInput.setSchema(tGlobalInput);
    }

    /**
     * ReCalInsuredBL
     *
     * @param tLCInsuredSchema LCInsuredSchema
     * @param tLCPolSet LCPolSet
     * @param tLCContSchema LCContSchema
     * @param tGlobalInput GlobalInput
     * @param tNewInsuredNo String
     */
    public ReCalInsuredBL(LCInsuredSchema tLCInsuredSchema, LCPolSet tLCPolSet,
                          LCContSchema tLCContSchema, GlobalInput tGlobalInput,
                          String tNewInsuredNo) {
        this(tLCInsuredSchema, tLCPolSet, tLCContSchema, tGlobalInput);
        mNewInsuredNo = tNewInsuredNo;
    }


    /**
     * 对比投保单下属被保人重要信息有无变更，有则重算保额保费
     * 操作完成后将险种、险种责任、保额、保费数据压入Map，准备
     * 到后台写数据库
     * @param cont Schema
     * Schema参数为团单实例或者是个单实例
     * @return boolean
     */
    public boolean reCalInsured() {
        boolean tReturn = false;
        tReturn = updateInsured();
        prepareData();
        return tReturn;
    }


    /**
     * 个单更新
     * @param cont LCContSchema  个单实例对象
     * @return boolean
     */
    private boolean updateInsured() {
        //合同信息
        LCContDB tContDB = new LCContDB();
        boolean updateFlag = false; //涉及保费保额计算的信息是否更改的标志  false-未更改
        tContDB.setContNo(preLCInsuredSchema.getContNo());
        tContDB.getInfo();
        if (tContDB.mErrors.needDealError()) {
            mErrors.copyAllErrors(tContDB.mErrors);
            return false;
        }
        preLCContSchema.setSchema(tContDB.getSchema());
        aftLCContSchema.setSchema(tContDB.getSchema());
        //
        if (preLCContSchema.getContType().equals("1")) {
            preLCAppntDB.setContNo(preLCContSchema.getContNo());
            preLCAppntDB.setAppntNo(preLCContSchema.getAppntNo());
        } else {
            preLCGrpAppntDB.setGrpContNo(preLCContSchema.getContNo());
            preLCGrpAppntDB.setCustomerNo(preLCContSchema.getAppntNo());
        }
        if (preLCPolSet.size() == 0) {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setContNo(preLCInsuredSchema.getContNo());
            tLCPolDB.setInsuredNo(preLCInsuredSchema.getInsuredNo());
            preLCPolSet = tLCPolDB.query(); //被保人下的险种集合
            if (preLCPolSet.size() == 0) {
                return true;
            }
        }
        updateFlag = balance(preLCInsuredSchema, preLCPolSet.get(1));
        if (updateFlag) {
            System.out.println("被保人" + preLCInsuredSchema.getName()
                               + "的重要信息变更\n删除原有保额保费信息");
            if (!addDelSql(preLCInsuredSchema)) { //add sql into map
                return false;
            }
        }
        LCPolSchema tLCPolSchema = new LCPolSchema();
        for (int j = 1; j <= preLCPolSet.size(); j++) {
            tLCPolSchema = preLCPolSet.get(j);
            //************先将tLCInsuredSchema中的信息更新到tLCPolSchema中
             if (!upInsurToPol(preLCInsuredSchema, tLCPolSchema)) {
                 return false;
             }
            //************判断updateFlag标志，如果为true则更改保额保费
             if (updateFlag) {
                 System.out.println("被保人" + preLCInsuredSchema.getName()
                                    + "的重要信息变更\n重算原有保额保费信息");
                 aftLCContSchema.setPrem(preLCContSchema.getPrem() -
                                         tLCPolSchema.getPrem());
                 aftLCContSchema.setSumPrem(preLCContSchema.getSumPrem() -
                                            tLCPolSchema.getSumPrem());
                 aftLCContSchema.setAmnt(preLCContSchema.getAmnt() -
                                         tLCPolSchema.getAmnt());
                 aftLCContSchema.setMult(preLCContSchema.getMult() -
                                         tLCPolSchema.getMult());
                 aftLCContSchema.setPeoples(preLCContSchema.getPeoples() -
                                            tLCPolSchema.getInsuredPeoples());
                 String fromPart = "from LCPol a where a.ContNo='"
                                   + aftLCContSchema.getContNo() + "')";
                 updateLCContStr = "update LCCont b set "
                                   + "Prem=(select SUM(a.Prem) " + fromPart
                                   + ", Amnt=(select SUM(a.Amnt) " + fromPart
                                   + ", SumPrem=(select SUM(a.SumPrem) " +
                                   fromPart
                                   + ", Mult=(select SUM(a.Mult) " + fromPart
                                   + " where b.grpcontno='"
                                   + aftLCContSchema.getGrpContNo() +
                                   "' and b.contno='" +
                                   aftLCContSchema.getContNo() + "'";

                 updateContFlag = true;
                 //查出保费项，领取项，责任项等加入计算
                 LCDutySet tLCDutySet = new LCDutySet();
                 LCDutyDB tLCDutyDB = new LCDutyDB();
                 tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
                 tLCDutySet = tLCDutyDB.query();
                 if (tLCDutyDB.mErrors.needDealError()) {
                     mErrors.copyAllErrors(tLCDutyDB.mErrors);
                     return false;
                 }
                 LCPremSet tLCPremSet = new LCPremSet();
                 LCPremDB tLCPremDB = new LCPremDB();
                 tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
                 tLCPremSet = tLCPremDB.query();
                 if (tLCPremDB.mErrors.needDealError()) {
                     mErrors.copyAllErrors(tLCPremDB.mErrors);
                     return false;
                 }

                 LCGetSet tLCGetSet = new LCGetSet();
                 LCGetDB tLCGetDB = new LCGetDB();
                 tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
                 tLCGetSet = tLCGetDB.query();
                 if (tLCGetDB.mErrors.needDealError()) {
                     mErrors.copyAllErrors(tLCGetDB.mErrors);
                     return false;
                 }
                 //根据险种定义，是否需要传入保费项进行计算
                 LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
                 tLMRiskAppDB.setRiskCode(tLCPolSchema.getRiskCode());
                 tLMRiskAppDB.getInfo();
                 if (tLMRiskAppDB.mErrors.needDealError()) {
                     mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
                     return false;
                 }
                 LCPolBL pLCPolBL = new LCPolBL();
                 LCDutyBLSet pLCDutyBLSet = new LCDutyBLSet();
                 LCGetBLSet pLCGetBLSet = new LCGetBLSet();
                 pLCPolBL.setSchema(tLCPolSchema);
                 pLCDutyBLSet.set(tLCDutySet);
                 pLCGetBLSet.set(tLCGetSet);
                 CalBL tCalBL = new CalBL(pLCPolBL, pLCDutyBLSet, pLCGetBLSet,
                                          "");
                 if (("Y").equals(tLMRiskAppDB.getInpPayPlan())) { //保费项为录入
                     if (tCalBL.calPol2(tLCPremSet) == false) {
                         CError.buildErr(this,
                                         "在重算险种" + tLMRiskAppDB.getRiskName()
                                         + "时失败，原因可能是：变更后年龄、性别不在投保范围内",
                                         tCalBL.mErrors);
                         return false;
                     }

                 } else {

                     if (!tCalBL.calPol()) {
                         CError.buildErr(this,
                                         "在重算险种" + tLMRiskAppDB.getRiskName()
                                         + "时失败，原因可能是：变更后年龄、性别不在投保范围内",
                                         tCalBL.mErrors);
                         return false;
                     }

                 }

//         ProposalBL tProposalBL = new ProposalBL();
//         VData tvar = new VData();
//
//         TransferData tTransferData = new TransferData();
//         if (preLCContSchema.getContType().equals("1")){
//           tvar.add(preLCAppntDB.getSchema());
//         }
//         else
//           tvar.add(preLCGrpAppntDB.getSchema());
//         tvar.add(tLCPolSchema);
//         tvar.add(preLCContSchema);
//         tvar.add(preLCInsuredSchema);
//         tvar.add(tGlobalInput);
//         tvar.add(tTransferData);
//         tvar.add(tLCDutySet);
//         tvar.add(tLCGetSet);
//         if ( ("Y").equals(tLMRiskAppDB.getInpPayPlan())) {
//           tvar.add(tLCPremSet);
//         }
//
//        tProposalBL.PrepareSubmitData(tvar, "UPDATE||PROPOSAL");
//         if (tProposalBL.mErrors.needDealError()) {
//           mErrors.copyAllErrors(tProposalBL.mErrors);
//           return false;
//         }
//          VData calResult = tProposalBL.getSubmitResult();
                 //准备好计算后的数据
                 if (!prepareCalData(tCalBL.getLCPol(), tCalBL.getLCDuty(),
                                     tCalBL.getLCPrem(), tCalBL.getLCGet())) {
                     return false;
                 }

             } else {
                 aftLCPolSet.add(tLCPolSchema.getSchema());
             }
        } //end tLCPolSet Cyc

        return true;
    }


    /**
     * 对比险种中关系到保额保费计算的被保人信息是否有更改
     * @param vLCInsuredSchema LCInsuredSchema
     * @param vLCPolSchema LCPolSchema
     * @return boolean    true--更改  false--未更改
     */
    private boolean balance(LCInsuredSchema vLCInsuredSchema,
                            LCPolSchema vLCPolSchema) {
        //性别
        if (!(vLCInsuredSchema.getSex().trim()).equals(vLCPolSchema.
                getInsuredSex().
                trim())) {
            System.out.println("性别:" + vLCInsuredSchema.getSex() + ":"
                               + vLCPolSchema.getInsuredSex());
            return true;
        }
        //生日
        if (!(vLCInsuredSchema.getBirthday().trim()).equals(vLCPolSchema.
                getInsuredBirthday().trim())) {
            System.out.println("生日:" + vLCInsuredSchema.getBirthday() + ":"
                               + vLCPolSchema.getInsuredBirthday());
            return true;
        }
        //职业类别
        if (vLCInsuredSchema.getOccupationType() != null) {
            if (!StrTool.compareString(vLCInsuredSchema.getOccupationType(),
                                       vLCPolSchema.getOccupationType())) {
                System.out.println("职业:" + vLCInsuredSchema.getOccupationType()
                                   + ":" + vLCPolSchema.getOccupationType());
                return true;
            }
        } else {
            if (vLCPolSchema.getOccupationType() != null) {
                return true;
            }
        }

        return false;
    }


    /**
     *根据参数提供的被保人信息
     *向map中添加sql语句，用于删除保单险种责任表、领取项表、保费项表、
     *保费项附表1－银行不定期扣款表中的内容
     * @param LCInsuredSchema LCInsuredSchema
     * 参数为被保人实例
     * @return boolean
     */
    private boolean addDelSql(LCInsuredSchema LCInsuredSchema) {
        String ContNO = LCInsuredSchema.getContNo();
        String InsuredNo = LCInsuredSchema.getInsuredNo();
        if (this.mNewInsuredNo != null) {
            InsuredNo = mNewInsuredNo;
            if(StrTool.cTrim(LCInsuredSchema.getRelationToAppnt()).equals("00")){
              InsuredNo = preLCPolSet.get(1).getInsuredNo();
            }
        }
        if (ContNO == null || ContNO.equals("") || InsuredNo == null ||
            InsuredNo.equals("")) {
            mErrors.addOneError(new CError("addDelSql方法出错：信息不完整，合同号或被保人信息缺少"));
            return false;
        }
        String subStr0 = "select a.PolNo from LCPol a where ContNo ='" + ContNO +
                         "' and "
                         + " InsuredNo ='" + InsuredNo + "'";
        map.put("delete from LCDuty b where b.PolNo in (" + subStr0 + ")"
                , "DELETE"); //保单险种责任表
        map.put("delete from LCPrem b where b.PolNo in (" + subStr0 + ")"
                , "DELETE"); //保费项表
        map.put("delete from LCGet b where b.PolNo in (" + subStr0 + ")"
                , "DELETE"); //领取项表
//      map.put("delete from LCPrem_1 where PolNo in ("+subStr0+")"
//              ,"DELETE");            //保费项附表1

        return true;
    }


    /**
     * 将被保人中的信息更新到对应的险种中
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean upInsurToPol(LCInsuredSchema tLCInsuredSchema,
                                 LCPolSchema tLCPolSchema) {
        tLCPolSchema.setInsuredBirthday(tLCInsuredSchema.getBirthday());
        tLCPolSchema.setInsuredSex(tLCInsuredSchema.getSex());
        tLCPolSchema.setOccupationType(tLCInsuredSchema.getOccupationType());
        tLCPolSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
        int tInsuredAge = 0;

        tInsuredAge = PubFun.calInterval(tLCInsuredSchema.getBirthday(),
                                         tLCPolSchema.getCValiDate(), "Y");
        tLCPolSchema.setInsuredAppAge(tInsuredAge);
        tLCPolSchema.setUWDate(tLCPolSchema.getUWDate());
        tLCPolSchema.setUWTime(tLCPolSchema.getUWTime());

        //tLCPolSchema.setInsuredName(tLCInsuredSchema.getName());
        return true;
    }


    /**
     * 准备计算后的数据，从VData中提取Set，放在全局业务处理类中
     * @param calData VData
     * @return booleantCalBL.getLCPol(), tCalBL.getLCDuty(),
                           tCalBL.getLCPrem(), tCalBL.getLCGet()
     */
    private boolean prepareCalData(LCPolBL pLCPolBL, LCDutyBLSet pLCDutyBLSet,
                                   LCPremBLSet pLCPremBLSet,
                                   LCGetBLSet pLCGetBLSet) {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCDutySet tLCDutySet = new LCDutySet();
        LCPremSet tLCPremSet = new LCPremSet();
        LCGetSet tLCGetSet = new LCGetSet();
        if (pLCPolBL == null || pLCDutyBLSet == null || pLCPremBLSet == null ||
            pLCGetBLSet == null) {
            mErrors.addOneError(new CError("计算保额保费时返回数据为空!!!"));
            return false;
        }
        tLCPolSchema.setSchema(pLCPolBL.getSchema());
        tLCDutySet.set(pLCDutyBLSet);
        tLCPremSet.set(pLCPremBLSet);
        tLCGetSet.set(pLCGetBLSet);
        for (int i = 1; i <= tLCPremSet.size(); i++) {
            tLCPremSet.get(i).setOperator(mGlobalInput.Operator);
            tLCPremSet.get(i).setMakeDate(theCurrentDate);
            tLCPremSet.get(i).setMakeTime(theCurrentTime);
        }
        for (int i = 1; i <= tLCGetSet.size(); i++) {
            tLCGetSet.get(i).setOperator(mGlobalInput.Operator);
            tLCGetSet.get(i).setMakeDate(theCurrentDate);
            tLCGetSet.get(i).setMakeTime(theCurrentTime);
        }

        aftLCPolSet.add(tLCPolSchema);
        aftLCDutySet.add(tLCDutySet);
        aftLCPremSet.add(tLCPremSet);
        aftLCGetSet.add(tLCGetSet);
        aftLCContSchema.setPrem(aftLCContSchema.getPrem() +
                                tLCPolSchema.getPrem());
        aftLCContSchema.setAmnt(aftLCContSchema.getAmnt() +
                                tLCPolSchema.getAmnt());
        aftLCContSchema.setMult(aftLCContSchema.getMult() +
                                tLCPolSchema.getMult());
        aftLCContSchema.setPeoples(aftLCContSchema.getPeoples() +
                                   tLCPolSchema.getInsuredPeoples());

        return true;
    }


    /**
     * 准备数据。只有险种相关信息的数据，含合同数据
     */
    private void prepareData() {
        mResult.clear();

        map.put(aftLCPolSet, "UPDATE");
        map.put(aftLCDutySet, "INSERT");
        map.put(aftLCPremSet, "INSERT");
        map.put(aftLCGetSet, "INSERT");
        if (updateContFlag) {
            map.put(updateLCContStr, "UPDATE");
        }
        mResult.add(map);
        if (updateContFlag) {
            mResult.add(aftLCContSchema);
        }
        mResult.add(aftLCPolSet);

    }


    /**
     * 得到运算结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        LCPolSet preLCPolSet = new LCPolSet();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo("130110000000119");
        tLCInsuredDB.setInsuredNo("86110000000112");
        tLCInsuredDB.getInfo();
        GlobalInput tGlobalInput = new GlobalInput();
        ReCalInsuredBL rr = new ReCalInsuredBL(tLCInsuredDB.getSchema(),
                                               tGlobalInput);
        VData var = new VData();
        if (!rr.reCalInsured()) {
            System.out.println("ERROR:" + rr.mErrors.getFirstError());
        } else {
            System.out.println(rr.getResult().size());
            MMap map = (MMap) rr.getResult().get(0);
            System.out.println(map.keySet().size());
            for (int i = 0; i < map.keySet().size(); i++) {
                Object key = map.getKeyByOrder(String.valueOf(i + 1));
                if (key instanceof LCPremSet) {
                    System.out.println(((LCPremSet) key).size());
                }
            }
        }
    }

}
