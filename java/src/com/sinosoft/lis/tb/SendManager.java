package com.sinosoft.lis.tb;

import java.rmi.RemoteException;

import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SMSClient;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessageNew;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsMessagesNew;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;

/**
 * 短信发送类
 * 
 * @author 张成轩
 */
public class SendManager {

	/** 手机号校验正则表达式 */
	private final String mobileReg = "^1[0-9]{10}$";

	/** 管理机构校验正则表达式 */
	private final String manageComReg = "86[0-9]{2}([0-9]?){4}";

	/** 错误处理类 */
	private CErrors mErrors = new CErrors();

	/**
	 * 短信发送处理还输
	 * 
	 * @param sendMobile
	 *            手机号
	 * @param sendContent
	 *            短信内容
	 * @param manageCom
	 *            管理机构
	 * @return
	 */
	public boolean sendMsg(String sendMobile, String sendContent,
			String manageCom) {

		// 校验手机号码
		if (!checkMobile(sendMobile)) {
			return false;
		}

		// 校验短信内容
		if (!checkContent(sendContent)) {
			return false;
		}

		// 校验管理机构
		if (!checkManageCom(manageCom)) {
			return false;
		}

		System.out.println("---短信发送处理开始---");
		System.out.println("---手机号：" + sendMobile + "---");

		SmsMessageNew tMsg = new SmsMessageNew();
		// 设置短信发送的手机号
		tMsg.setReceiver(sendMobile);
		// 设置短信内容
		tMsg.setContents(sendContent);
		// 设置所属机构
		tMsg.setOrgCode(manageCom);

//		SmsServiceSoapBindingStub binding = null;
//		try {
//			// 创建短信发送对象
//			binding = (SmsServiceSoapBindingStub) new SmsServiceServiceLocator()
//					.getSmsService();
//		} catch (javax.xml.rpc.ServiceException jre) {
//			jre.printStackTrace();
//		}
//
//		binding.setTimeout(60000);
		Response value = null;
		SMSClient smsClient=new SMSClient();
		// 创建短信对象
		SmsMessagesNew msgs = new SmsMessagesNew();
		msgs.setOrganizationId("86");
		msgs.setExtension("false");		
		msgs.setStartDate(PubFun.getCurrentDate());
		msgs.setStartTime("9:00");
		msgs.setEndDate(PubFun.getCurrentDate());
		msgs.setEndTime("20:00");
		msgs.setTaskValue(SMSClient.mes_taskVlaue15);
		msgs.setServiceType(SMSClient.mes_serviceType1);
		
		msgs.setMessages(new SmsMessageNew[] { tMsg });
		smsClient.sendSMS(msgs);
//		try {
//			// 提交该批短信，UserName是短信服务平台管理员分配的用户名，Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
////			value = binding.sendSMS("Admin", "Admin", msgs);
//
//			System.out.println(value.getStatus());
//			System.out.println(value.getMessage());
//		} catch (RemoteException ex) {
//			ex.printStackTrace();
//			mErrors.addOneError("短信发送失败");
//			return false;
//		}

		return true;
	}

	/**
	 * 校验手机号
	 * 
	 * @param mobile
	 *            手机号
	 * @return
	 */
	private boolean checkMobile(String mobile) {
		// 校验手机号是否符合规则
		if (mobile != null && mobile.trim().matches(mobileReg)) {
			return true;
		}
		System.out.println("--手机号不符合校验规则--" + mobile);
		mErrors.addOneError("手机号不符合校验规则");
		return false;
	}

	/**
	 * 校验短信内容
	 * 
	 * @param content
	 *            短信内容
	 * @return
	 */
	private boolean checkContent(String content) {
		// 校验信息内容是否符合规则
		if (content != null && !"".equals(content.trim())) {
			return true;
		}
		System.out.println("--信息内容不符合校验规则--" + content);
		mErrors.addOneError("信息内容不符合校验规则");
		return false;
	}

	/**
	 * 校验管理机构
	 * 
	 * @param manageCom
	 *            管理机构
	 * @return
	 */
	private boolean checkManageCom(String manageCom) {
		// 校验管理机构是否符合规则
		if (manageCom != null && manageCom.trim().matches(manageComReg)) {
			return true;
		}
		System.out.println("--管理机构不符合校验规则--" + manageCom);
		mErrors.addOneError("管理机构不符合校验规则");
		return false;
	}

	/**
	 * 获取错误对象
	 * 
	 * @return
	 */
	public CErrors getCErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {
		SendManager tSendManager = new SendManager();
		 tSendManager.sendMsg("13141245562", "测试短信发送", "86910000");
		// tSendManager.checkMobile("13581651604");
		// tSendManager.checkContent(null);
//		tSendManager.checkManageCom("86");
	}
}
