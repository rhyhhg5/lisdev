package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: 大项目新单复核 </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class GrpBigProjectApproveBL {

    /** 报错信息 */
    public CErrors mErrors = new CErrors();
    /** 录入数据 */
    private VData mInputData;
    /** 操作符 */
    private String mOperate;
    /** 团体合同信息 */
    private LCGrpContSchema mLCGrpContSchema;
    /** 团体险种信息 */
    private LCGrpPolSet mLCGrpPolSet;
    /** 团体合同号码 */
    private String mGrpContNo;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 最终递交信息 */
    private MMap map = new MMap();
    /** 最后递交对象 */
    private VData mResult = new VData();

    public GrpBigProjectApproveBL() {
    }

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        System.out.println("进入BL程序开始处理....");

        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!checkData()) {
            return false;
        }

        if (!getInputData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }
        /** 开始递交 */
        if (!this.mOperate.equals("WORKFLAG||BIGPROJCET")) {
            PubSubmit tPubSubmit = new PubSubmit();
            if (tPubSubmit.submitData(this.mResult, "INSERT")) {
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                return false;
            }
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        mResult.add(map);
        mResult.add(mLCGrpContSchema);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        /** 校验保单，人数，保费等信息 */
        if (!checkCont()) {
            return false;
        }
        if (!creatSql()) {
            return false;
        }
        if (!prepareMission()) {
            return false;
        }
        return true;
    }

    /**
     * prepareMission
     *
     * @return boolean
     */
    private boolean prepareMission() {

        return true;
    }

    /**
     * creatSql
     *
     * @return boolean
     */
    private boolean creatSql() {
        String sql_1 = "update lccont set ApproveFlag='9',ApproveCode='" +
                       mGlobalInput.Operator + "',ApproveDate='" +
                       PubFun.getCurrentDate() + "',ApproveTime='" +
                       PubFun.getCurrentTime() + "',UWFlag='9',UWOperator='" +
                       mGlobalInput.Operator + "',UWDate='" +
                       PubFun.getCurrentDate() + "',UWTime='" +
                       PubFun.getCurrentTime() + "' where grpcontno='" +
                       this.mGrpContNo + "'";
        String sql_2 = "update lcpol set ApproveFlag='9',ApproveCode='" +
                       mGlobalInput.Operator + "',ApproveDate='" +
                       PubFun.getCurrentDate() + "',ApproveTime='" +
                       PubFun.getCurrentTime() + "',UWFlag='9',UWCode='" +
                       mGlobalInput.Operator + "',UWDate='" +
                       PubFun.getCurrentDate() + "',UWTime='" +
                       PubFun.getCurrentTime() + "' where grpcontno='" +
                       this.mGrpContNo + "'";
        String sql_3 = "update LCGrpPol set ApproveFlag='9',ApproveCode='" +
                       mGlobalInput.Operator + "',ApproveDate='" +
                       PubFun.getCurrentDate() + "',ApproveTime='" +
                       PubFun.getCurrentTime() + "',UWFlag='9',UWOperator='" +
                       mGlobalInput.Operator + "',UWDate='" +
                       PubFun.getCurrentDate() + "',UWTime='" +
                       PubFun.getCurrentTime() + "' where grpcontno='" +
                       this.mGrpContNo + "'";
        String sql_4 = "update LCGrpCont set ApproveFlag='9',ApproveCode='" +
                       mGlobalInput.Operator + "',ApproveDate='" +
                       PubFun.getCurrentDate() + "',ApproveTime='" +
                       PubFun.getCurrentTime() + "',UWFlag='9',UWOperator='" +
                       mGlobalInput.Operator + "',UWDate='" +
                       PubFun.getCurrentDate() + "',UWTime='" +
                       PubFun.getCurrentTime() + "' where grpcontno='" +
                       this.mGrpContNo + "'";
        map.put(sql_1, "UPDATE");
        map.put(sql_2, "UPDATE");
        map.put(sql_3, "UPDATE");
        map.put(sql_4, "UPDATE");
        return true;
    }

    /**
     * checkCont
     *
     * @return boolean
     */
    private boolean checkCont() {

        /** 校验合同信息 */
        if (this.mLCGrpContSchema.getPrem() <= 0) {
            buildError("checkCont", "保费为0，请检查合同信息！");
            return false;
        }

        /** 校验险种信息 */
        for (int i = 1; i <= mLCGrpPolSet.size(); i++) {
            if (mLCGrpPolSet.get(i).getPrem() <= 0) {
                buildError("checkCont", "险种保费为0！");
                return false;
            }
        }

        /** 校验被保人 */
        String people =
                (new ExeSQL()).getOneValue(
                        "select sum(Peoples) from lccont where grpcontno='" +
                        this.mGrpContNo + "' with ur");
        if (people == null || people.equals("") || people.equals("null") ||
            people.equals("0")) {
            buildError("checkCont", "团单下被保人人数为null！");
            return false;
        }

        if (mLCGrpContSchema.getPeoples2() != Integer.parseInt(people)) {
            buildError("checkCont", "被保人人数与总单信息不一致！");
            return false;
        }

        /** 校验保费 */
        if (!checkPrem()) {
            return false;
        }

        /** 校验相关日期 */
        if (!checkDate()) {
            return false;
        }

        return true;
    }

    /**
     * checkDate
     *
     * @return boolean
     */
    private boolean checkDate() {
//        StringBuffer sql = new StringBuffer();
//        sql.append("select count(1) from lccont where grpcontno='");
//        sql.append(this.mGrpContNo);
//        sql.append("' and where paytodate is null");
//        String chk = (new ExeSQL()).getOneValue(sql.toString());
//        int chk_num = Integer.parseInt(chk);
//
//        if (chk_num > 0) {
//            buildError("checkDate", "团体下个人存在交至日期为null的情况出现！");
//            return false;
//        }
//
//        sql = new StringBuffer();
//        sql.append("select count(1) from lccont where ");
        return true;
    }

    /**
     * 校验团单下的保费信息是否正确
     *
     * @return boolean
     */
    private boolean checkPrem() {

        /** 校验被保人保费 */
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT * FROM (SELECT A.GRPCONTNO D,A.PREM E,SUM(B.PREM) F FROM LCGRPCONT A,LCCONT B WHERE A.GRPCONTNO=B.GRPCONTNO AND A.GRPCONTNO='");
        sql.append(this.mGrpContNo);
        sql.append("' GROUP BY A.GRPCONTNO,A.PREM) AS X WHERE E<>F");
        System.out.println("执行校验保费SQL : \n" + sql.toString());
        SSRS ssrs = (new ExeSQL()).execSQL(sql.toString());
        if (ssrs != null && ssrs.getMaxRow() > 0) {
            String error = "团体保单:" + ssrs.GetText(1, 1) + ",整单保费:" +
                           ssrs.GetText(1, 2) + ",个人总保费:" + ssrs.GetText(1, 3) +
                           "金额不符";
            System.out.println("保费错误 : " + error);
            buildError("checkPrem", error);
            return false;
        }
        /** 校验险种的总保费 */
        sql = new StringBuffer();
        sql.append("select * from (select a.grpcontno a,a.prem b,sum(b.prem) c from lcgrpcont a,lcgrppol b where a.grpcontno=b.grpcontno and a.grpcontno='");
        sql.append(this.mGrpContNo);
        sql.append("' group by a.grpcontno,a.prem) as x where b<>c");
        ssrs = (new ExeSQL()).execSQL(sql.toString());
        if (ssrs != null && ssrs.getMaxRow() > 0) {
            String error = "团体保单:" + ssrs.GetText(1, 1) + ",整单保费:" +
                           ssrs.GetText(1, 2) + ",险种总保费:" + ssrs.GetText(1, 3) +
                           "金额不符";
            System.out.println("保费错误 : " + error);
            buildError("checkPrem", error);
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        this.mLCGrpContSchema = (LCGrpContSchema) mInputData.
                                getObjectByObjectName("LCGrpContSchema", 0);

        if (mLCGrpContSchema == null) {
            buildError("getInputData", "传入合同信息为null！");
            return false;
        }

        if (mLCGrpContSchema.getGrpContNo() == null ||
            mLCGrpContSchema.getGrpContNo().equals("")) {
            buildError("getInputData", "传入合同号码为null！");
            return false;
        }

        LCGrpContDB tLCGrpContDB = mLCGrpContSchema.getDB();
        if (!tLCGrpContDB.getInfo()) {
            buildError("getInputData", "查询合同信息失败！");
            return false;
        }
        /** 封装团体合同信息 */
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        mGrpContNo = mLCGrpContSchema.getGrpContNo();

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        mLCGrpPolSet = tLCGrpPolDB.query();

        if (mLCGrpPolSet == null || mLCGrpPolSet.size() <= 0) {
            buildError("getInputData", "查询团体下险种信息失败！");
            return false;
        }

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);

        if (mGlobalInput == null) {
            buildError("getInputData", "页面已超时请重新登陆！");
            return false;
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {

        System.out.println("开始数据教研....");

        if (this.mInputData == null) {
            buildError("checkData", "传入参数信息为null！");
            return false;
        }

        if (this.mOperate == null) {
            buildError("checkData", "传入操作符为null！");
            return false;
        }

        return true;
    }

    /**
     * getResult
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "GrpBigProjectApproveBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
}
