package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
public class EstmakedateUI {

	    /** �������� */
	    public CErrors mErrors = new CErrors();

	    public EstmakedateUI()
	    {
	    }

	    public boolean submitData(VData cInputData, String cOperate)
	    {
	        try
	        {
	        	EstmakedateBL tEstmakedateBL = new EstmakedateBL();
            if (!tEstmakedateBL.submitData(cInputData, cOperate))
            {
	                this.mErrors.copyAllErrors(tEstmakedateBL.mErrors);
	                return false;
	            }
	            return true;
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	            this.mErrors.addOneError(e.getMessage());
	            return false;
	        }
	    }
	}

