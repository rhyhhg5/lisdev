/*
 * @(#)LCContSignBL.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.tb;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 合同签单处理类</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author wujs
 * @version 6.0
 */
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import com.f1j.chart.tc;
import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LCSpecDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPolSpecRelaSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPolSpecRelaSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCInsuredListDB;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.f1print.PrintPDFManagerBL;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.bq.CommonBL;

public class LCContSignBL
{
    /**存放结果*/
    public VData mVResult = new VData();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCContSet mLCContSet = null;

    private final String INSERT = "INSERT";

    private final String UPDATE = "UPDATE";

    private final String DELETE = "DELETE";

    private final int ERROR = 100;

    //交费日期相关
    private Date mFirstPayDate = null;

    private Date mPayToDate;

    private Date maxPayDate;

    private Date maxEnterAccDate;

    TransferData mTransferData;

    //当前时间
    private String mCurrentDate;

    private String mCurrentTime;

    private String mAppntNo;

    private MMap map1 = new MMap();

    //记录已有交费记录
    private HashMap mLJTempFeeMap = new HashMap();

    private int tAppntnum;

    /**险种保单签单类*/
    private ProposalSignBL proposalSignBL = new ProposalSignBL();

    private LCPolSpecRelaSet mLCPolSpecRelaSet = new LCPolSpecRelaSet();

    private TransferData payToDateTransferData = new TransferData();

    /** 操作类型，主要是为预打保单操作 */
    private String mOperate;

    /** 保全号码EdorNo */
    private String mEdorNo;

    private LCPolSet mLCPolSet = new LCPolSet();

    /**
     * 得到签单后险种信息，保全用
     * @return LCPolSet
     */
    public LCPolSet getLCPolSet()
    {
        return mLCPolSet;
    }

    public VData getResult()
    {
        return this.mVResult;
    }

    /**
     * 数据处理接口
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (this.getInputData() == false)
        {
            return false;
        }

        // 数据操作业务处理
        if (this.signLCContPol(mLCContSet) == false)
        {
            return false;
        }
        return true;
    }

    /**
     * 对合同进行签单，
     *
     * @param inLCContSet LCContSet
     * @return boolean 修改
     */
    private boolean signLCContPol(LCContSet inLCContSet)
    {
        this.mVResult = new VData();
        MMap contMap = new MMap();//避免空指针异常 2008-12-18
        LCContSchema tLCContSchema = null;
        LCPolDB tLCPolDB = null;
        LCPolSet tLCPolSet = null;
        LCPolSchema tPolSchema = null;
        LCContDB tContDB = null;
        VData bufferData = new VData();
        int signCount = 0;
        if (inLCContSet == null || inLCContSet.size() <= 0)
        {
            CError.buildErr(this, "没有传入要签单的数据");
            return false;
        }
        /**以一个合同为单位签单*/
        for (int i = 1; i <= inLCContSet.size(); i++)
        {
            //        //个单有错误的直接退出(团单还可以继续处理)
            //        if ( tLCContSchema!=null && "1".equals(tLCContSchema.getContType()) && this.mErrors.getErrorCount()>0)
            //         {
            //                    return false;
            //         }

            tLCContSchema = inLCContSet.get(i);
            if (tLCContSchema == null)
            {
                continue;
            }
            //获取合同所有信息
            tContDB = new LCContDB();
            String tContNo = tLCContSchema.getContNo();
            if (StrTool.cTrim(tLCContSchema.getContType()).equals("2"))
            {
                tContNo = tLCContSchema.getProposalContNo();
            }
            tContDB.setProposalContNo(tContNo);
            LCContSet tLCContSet = tContDB.query();
            if (tLCContSet.size() > 1 || tLCContSet.size() <= 0)
            {
                buildError("signLCContPol", "查询合同信息失败！");
                return false;
            }

            tLCContSchema.setSchema(tLCContSet.get(1).getSchema());
            if (tLCContSchema.getContType().equals("1"))
            {
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = new SSRS();
                tSSRS = tExeSQL.execSQL("select distinct agentcode from lcpol where prtno='" + tLCContSchema.getPrtNo()
                        + "'  ");
                if (tSSRS.getMaxRow() != 1)
                {
                    CError.buildErr(this, "查找个单合同['" + tLCContSchema.getPrtNo() + "']代理人信息失败!");
                    return false;
                }
                if (!(tLCContSchema.getAgentCode().equals(tSSRS.GetText(1, 1))))
                {
                    buildError("checkdate", "查找个单合同['" + tLCContSchema.getPrtNo() + "']代理人代码信息失败!");
                    return false;
                }

                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLCContSchema.getAgentCode());
                if (tLAAgentDB.getInfo() == false)
                {
                    // @@错误处理
                    buildError("checkdate", "请您确认：代理人编码没有输入错误!");
                    return false;
                }
                if (!tLCContSchema.getManageCom().equals(tLAAgentDB.getManageCom()))
                {
                    buildError("checkdate", "您录入的管理机构和数据库中代理人编码对应的管理机构不符合！");
                    return false;
                }

                // 加入签单限制：
                // 2008年1月1日开始，不能对生效日期为2007年的个单进行签发。
                //if (!checkSignDate(tLCContSchema))
                //{
                //    return false;
                //}
                // -------------------------------------
            }
            mAppntNo = tLCContSchema.getAppntNo();
            //复核,核保校验
            if (tLCContSchema.getAppFlag().equals("1"))
            {

                System.out.println("提示信息:合同(" + tLCContSchema.getContNo() + ")已签单;");
                //                if ( "1".equals(tLCContSchema.getContType()) )
                //                {
                //                    return true;
                //                }
                continue;
            }
            if (!"4".equals(tLCContSchema.getApproveFlag()) && !"9".equals(tLCContSchema.getApproveFlag()))
            {
                CError.buildErr(this, "合同(" + tLCContSchema.getContNo() + ")未复核通过;");

                continue;
            }
            if (!"4".equals(tLCContSchema.getUWFlag()) && !"9".equals(tLCContSchema.getUWFlag()))
            {
                CError.buildErr(this, "合同(" + tLCContSchema.getContNo() + ")未核保通过;");
                continue;
            }
            //获取合同下所有核保通过的保单
            tLCPolDB = new LCPolDB();
            String sql = "select * from lcpol where contno ='" + tLCContSchema.getContNo() + "' and appflag!='1'";
            //* ↓ *** liuhao *** 2005-06-09 *** modify **********
            //sql += " and uwflag in ('4','9') order by polno";
            sql += " and uwflag in ('4','9') order by InsuredNo,RiskCode";
            //* ↑ *** liuhao *** 2005-06-09 *** modify **********
            System.out.println("sql==" + sql);
            tLCPolSet = tLCPolDB.executeQuery(sql);
            if (tLCPolSet == null || tLCPolSet.size() <= 0)
            {
                continue;
            }
            //个人合同单
            if ("1".equals(tLCContSchema.getContType()))
            {
                //财务交费校验
                /** 多被保1险人不支持次种比较，去掉 2005-3-9
                 for ( int j=1;j<=tLCPolSet.size();j++)
                 {
                 //调用交费比较
                 if (!compareTempFeeRiskErr(tLCPolSet.get(j)))
                 {
                 //没有交费或交费不足或没有到帐，错误，返回
                 return false;
                 }
                 }
                 wujs**/

                //校验是否银行在途
                String bankOnTheWay = new ExeSQL()
                        .getOneValue("select db2inst1.nvl(count(1), 0) from LJSPay where OtherNo = '"
                                + tLCContSchema.getPrtNo()
                                + "' and OtherNoType in ('9','16') and BankOnTheWayFlag = '1'");
                if (!bankOnTheWay.equals("0.0"))
                {
                    CError tError = new CError();
                    tError.moduleName = "UWSendPrintBL";
                    tError.functionName = "prepareBankData";
                    tError.errorMessage = "保单银行在途，请待回盘之后进行相同操作！";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                MMap mmap = new MMap();
                mmap.put("update ljspay set CanSendBank = '1' where otherno = '" + tLCContSchema.getPrtNo()
                        + "' and (BankOnTheWayFlag is null or BankOnTheWayFlag = '0')", "UPDATE");
                VData mVData = new VData();
                mVData.add(mmap);
                PubSubmit mPubSubmit = new PubSubmit();
                if (!mPubSubmit.submitData(mVData, "UPDATE"))
                {
                    this.mErrors.copyAllErrors(mPubSubmit.mErrors);
                    return false;
                }
                //删除非银行在途，且锁定的财务应收记录
                MMap tmap = new MMap();
                SSRS getNoticeNo = new ExeSQL().execSQL("select distinct a.tempfeeno from ljtempfee "
                        + " a left join ljspay b " + " on a.tempfeeno = b.getnoticeno "
                        + " and (b.BankOnTheWayFlag is null or b.BankOnTheWayFlag = '0') "
                        + " and b.OtherNoType in ('9','16') " + " and b.CanSendBank = '1' " + " where a.otherno='"
                        + tLCContSchema.getPrtNo() + "'" + " and a.enteraccdate is null ");
                for (int m = 1; m <= getNoticeNo.MaxRow; m++)
                {
                    String getNoticeNoString = getNoticeNo.GetText(m, 1);
                    tmap.put("delete from LJSPayB where GetNoticeNo = '" + getNoticeNoString + "'", "DELETE");
                    //备份到B表
                    tmap
                            .put(
                                    "insert into LJSPayB (select GetNoticeNo, OtherNo, OtherNoType, AppntNo, SumDuePayMoney, PayDate, "
                                            + "BankOnTheWayFlag, BankSuccFlag, SendBankCount, ApproveCode, ApproveDate, SerialNo, Operator, MakeDate, "
                                            + "MakeTime, ModifyDate, ModifyTime, ManageCom, AgentCom, AgentType, BankCode, BankAccNo, RiskCode, "
                                            + "AgentCode, AgentGroup, AccName, StartPayDate, PayTypeFlag, '7', '', '1',markettype,salechnl "
                                            + "from LJSPay where GetNoticeNo = '" + getNoticeNoString + "')", "INSERT");
                    //删除未到帐的财务暂收记录
                    tmap.put("delete from LJSPay where GetNoticeNo = '" + getNoticeNoString + "'", "DELETE");
                    tmap.put(
                            "delete from ljtempfeeclass where tempfeeno in(select tempfeeno from  LJSPay where GetNoticeNo = '"
                                    + getNoticeNoString + "' and OtherNoType = '4') and EnterAccDate is null ",
                            "DELETE");
                    tmap.put("delete from ljtempfee where tempfeeno = '" + getNoticeNoString + "'"
                            + " and OtherNoType = '4' and EnterAccDate is null ", "DELETE");
                }//end modify
                contMap.add(tmap);

                // 校验保单暂收保费是否足额
                if (!chkContTempFee(tLCPolSet))
                {
                    return false;
                }
                // --------------------

                //                if (!compareTempFeeRiskErr(tLCPolSet))
                //                {
                //                    //暂交费不足或没有到帐
                //                    return false;
                //                }

                //                int compareFee = this.compareTempFee(tLCContSchema.getPrtNo(),tLCPolSet);
                //                if (compareFee == this.ERROR)
                //                {
                //                    CError.buildErr(this,
                //                                    "合同(" + tLCContSchema.getContNo() + ")没有交费信息!");
                //                    return false;
                //                }
                //                if (compareFee < 0)
                //                {
                //                    CError.buildErr(this,
                //                                    "合同(" + tLCContSchema.getContNo() + ")交费不足或没有到帐!");
                //                    return false;
                //                }
            }
            String newContNo = null;
            String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
            if (tLCContSchema.getAppFlag().equals("9"))
            {
                newContNo = tLCContSchema.getContNo();
            }
            if (tLCContSchema.getContType().equals("1") && !tLCContSchema.getAppFlag().equals("9"))
            {
                if (!CheckAppntNum(tLCContSchema))
                {
                    return false;
                }

                newContNo = PubFun1.CreateMaxNo("CONTNO", mAppntNo);
            }
            else if (tLCContSchema.getContType().equals("2"))
            {
                boolean needCreat = true;
                if (mEdorNo != null && !mEdorNo.equals(""))
                {
                    LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
                    tLCInsuredListDB.setGrpContNo(tLCContSchema.getGrpContNo());
                    tLCInsuredListDB.setContNo(tLCContSchema.getContNo());
                    tLCInsuredListDB.setContPlanCode("FM");
                    if (tLCInsuredListDB.getCount() > 0)
                    {
                        needCreat = false;
                        newContNo = tLCContSchema.getContNo();
                    }
                }
                if (needCreat)
                {
                    newContNo = PubFun1.CreateMaxNo("GRPPERSONCONTNO", "");
                }
            }
            if (newContNo.trim().equals(""))
            {
                mErrors.addOneError(new CError("生成CONTNO或者GRPPERSONCONTNO失败!"));
                return false;
            }

            String tPaySerialNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
            if (tPaySerialNo.trim().equals(""))
            {
                mErrors.addOneError(new CError("生成PaySerialNo失败!"));
                return false;
            }

            VData tInput = new VData();
            tInput.add(this.mGlobalInput);
            tInput.add(tLCPolSet);
            tInput.add(newContNo);
            tInput.add(mTransferData);
            if (mInputData.getObjectByObjectName("TransferData", 0) != null)
            {
                tInput.add(mInputData.getObjectByObjectName("TransferData", 0)); //保全签单会附加其他信息，将这些信息传到险种签单中使用
            }
            //提交保单签单
            boolean signResult = proposalSignBL.submitData(tInput, this.mOperate);
            if (!signResult)
            {
                if (proposalSignBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(proposalSignBL.mErrors);
                }

                CError.buildErr(this, "合同(" + tLCContSchema.getContNo() + ")签单没有通过.");

                continue;
            }

            VData oneCont = new VData();
            oneCont = proposalSignBL.getResult();
            MMap srvResult = (MMap) oneCont.getObjectByObjectName("MMap", 0);

            TransferData tTransferData = (TransferData) oneCont.getObjectByObjectName("TransferData", 0);
            String tInsuredAppAgeChangeFlag = (String) tTransferData.getValueByName("InsuredAppAgeChangeFlag");
            String tCValidateChangeFlag = (String) tTransferData.getValueByName("CValidateChangeFlag");

            //if ( this.mNewGrpContNo.equals(this.GRPPOL_FLAG)){
            if ("1".equals(tLCContSchema.getContType()))
            {
                //财务处理
                VData bufferdPolData = proposalSignBL.getBufferedPolData();
                MMap finaMap = this.dealFinance(tLCContSchema, bufferdPolData, newContNo, tPaySerialNo);
                if (finaMap == null)
                {
                    return false;
                }
                srvResult.add(finaMap);
            }
            LCPolSet tempLCPolSet = (LCPolSet) oneCont.getObjectByObjectName("LCPolSet", 0);
            this.mLCPolSet.add(tempLCPolSet);

            FDate tFDate = new FDate();
            mPayToDate = this.maxPayDate;
            if (mPayToDate == null)
            {
                mPayToDate = tFDate.getDate("1900-01-01");
            }
            if (maxPayDate == null)
            {
                maxPayDate = tFDate.getDate("1900-01-01");
            }
            for (int m = 1; m <= tempLCPolSet.size(); m++)
            {
                //                System.out.println("mPayToDate" + mPayToDate);
                //                System.out.println("maxPayDate" + maxPayDate);
                if (mPayToDate.before(tFDate.getDate(tempLCPolSet.get(m).getPaytoDate())))
                {
                    this.mPayToDate = tFDate.getDate(tempLCPolSet.get(m).getPaytoDate());
                }
                getLCPolSpecRela(tempLCPolSet.get(m), m);
            }
            if (maxPayDate.before(mPayToDate))
            {
                maxPayDate = mPayToDate;
            }

            // 校验生日单保费变化
            if ("true".equals(tInsuredAppAgeChangeFlag))
            {
                for (int idx1 = 1; idx1 <= tLCPolSet.size(); idx1++)
                {
                    LCPolSchema tTmpOldPolInfo = tLCPolSet.get(idx1);
                    for (int idx2 = 1; idx2 <= tempLCPolSet.size(); idx2++)
                    {
                        LCPolSchema tTmpNewPolInfo = tempLCPolSet.get(idx2);
                        if (tTmpNewPolInfo.getPolNo().equals(tTmpOldPolInfo.getPolNo()))
                        {
                            if (tTmpNewPolInfo.getPrem() != tTmpOldPolInfo.getPrem())
                            {
                                buildError("signLCContPol", "生日单，保费发生变化！");
                                return false;
                            }

                            if (tTmpNewPolInfo.getAmnt() != tTmpOldPolInfo.getAmnt())
                            {
                                buildError("signLCContPol", "生日单，保额发生变化！");
                                return false;
                            }
                        }
                    }
                }
            }
            // --------------------

            //指定合同生效日
            FDate fdate = new FDate();
            Date cValidate = null;
            Date tPayToDate = null;
            Date paytodate;
            for (int l = 1; l <= tLCPolSet.size(); l++)
            {
                //                if (tLCPolSet.get(l) != null &&
                //                    tLCPolSet.get(l).getContNo().equals(tLCContSchema.getContNo())) {
                paytodate = fdate.getDate(tLCPolSet.get(l).getPaytoDate());
                if (tPayToDate == null || tPayToDate.after(paytodate))
                {
                    tPayToDate = paytodate;
                }
                if (tLCPolSet.get(l).getCValiDate() != null)
                {
                    if (cValidate == null)
                    {
                        cValidate = fdate.getDate(tLCPolSet.get(l).getCValiDate());
                    }
                    if (fdate.getDate(tLCPolSet.get(l).getCValiDate()).before(cValidate))
                    {
                        cValidate = fdate.getDate(tLCPolSet.get(l).getCValiDate());
                    }
                }
                //                }
            }
            tLCContSchema.setPaytoDate(this.mPayToDate);
            System.out.println("PayTodate : " + tLCContSchema.getPaytoDate());
            /**
             * 总期交保费
             */
            double tempSumPrem = 0.0;
            for (int m = 1; m <= tempLCPolSet.size(); m++)
            {
                tempSumPrem += tempLCPolSet.get(m).getSumPrem();
            }
            tLCContSchema.setSumPrem(tempSumPrem);
            if (tLCContSchema.getCValiDate() == null || "Y".equals(tCValidateChangeFlag))
            {
                if (cValidate == null)
                {
                    cValidate = PubFun.calDate(new Date(), 1, "D", new Date());
                }
                tLCContSchema.setCValiDate(cValidate);
            }
            //  tLCContSchema.setCValiDate(cValidate);

            /**@author:Yangming 在处理个单时根据保险起见计算出满期日期 */
            LCDutySet tempLCDutySet = null;
            if ("Y".equals(tCValidateChangeFlag))
            {
                tempLCDutySet = (LCDutySet) oneCont.getObjectByObjectName("LCDutySet", 0);
            }
            if (!dealCValiDate(tLCContSchema, tempLCDutySet))
            {
                return false;
            }

            //合同本身处理
            srvResult.add(prepareCont(tLCContSchema, newContNo));

            //处理一些需要更新合同号的表
            srvResult.add(dealElseUpdateTable(tLCContSchema.getContNo(), newContNo));

            contMap.add(srvResult);

            // 签单动作前，对保单进行锁定，时效为5分钟。
            MMap tTmpMap = null;
            tTmpMap = lockCont(tLCContSchema);
            if (tTmpMap == null)
            {
                return false;
            }
            contMap.add(tTmpMap);
            // -------------------------------

            if ("1".equals(tLCContSchema.getContType()))
            {
                // 生成个单投保人帐户
                MMap tTmpAppAccMap = null;
                tTmpAppAccMap = dealSingleContAppAcc(tLCContSchema, newContNo);
                if (tTmpAppAccMap == null)
                {
                    return false;
                }
                contMap.add(tTmpAppAccMap);
                // --------------------
            }
            signCount++;
            //System.out.println("个人单追加险种特约关联表信息[Start]");
            //* ↓ *** LiuHao *** 2005-05-08 *** add *******************
            //个人单追加险种特约关联表信息收集
        }

        //个人单追加险种特约关联表信息入库
        if ("1".equals(tLCContSchema.getContType()))
        {
            if (mLCPolSpecRelaSet != null)
            {
                //LCPolSpecRelaSubmit();
                contMap.put(mLCPolSpecRelaSet, SysConst.DELETE_AND_INSERT);
            }
        }

        //* ↑ *** LiuHao *** 2005-05-08 *** add *******************
        //System.out.println("个人单追加险种特约关联表信息[End]");
        if (StrTool.cTrim(tLCContSchema.getContType()).equals("1"))
        {
            /** 投保序号加一 */
            String sql = "update LDPerson set AppntNum=to_zero(AppntNum)+1,modifydate = current date,modifytime='"+PubFun.getCurrentTime()+"' where CustomerNo='" + this.mAppntNo + "'";
            if (!tLCContSchema.getAppFlag().equals("9"))
            {
                contMap.put(sql, "UPDATE");
            }
        }
        
        if (StrTool.cTrim(tLCContSchema.getExPayMode())!=null&&!StrTool.cTrim(tLCContSchema.getExPayMode()).equals(""))
        {
            /** 签单后用续期缴费方式更新缴费方式字段 */
            String sql = "update LCCont set PayMode=ExPayMode where PrtNo='" + tLCContSchema.getPrtNo() + "'";
            contMap.put(sql, "UPDATE");
            
            String sqlLCpol = "update LCpol set PayMode = '"+tLCContSchema.getExPayMode()+"'," +
            		"ExPayMode = '"+tLCContSchema.getExPayMode()+"'  where PrtNo='" + tLCContSchema.getPrtNo() + "'";
            contMap.put(sqlLCpol, "UPDATE");
        }
        this.mVResult.add(contMap);

        return true;
    }

    /**
     * dealCValiDate
     *
     * @param tLCContSchema LCContSchema
     * @return boolean
     */
    private boolean dealCValiDate(LCContSchema tLCContSchema, LCDutySet cLCDutySet)
    {
        LCDutySet tLCDutySet = null;

        if (cLCDutySet == null || cLCDutySet.size() == 0)
        {
            LCDutyDB tLCDutyDB = new LCDutyDB();
            tLCDutyDB.setContNo(tLCContSchema.getContNo());
            tLCDutySet = tLCDutyDB.query();
        }
        else
        {
            tLCDutySet = cLCDutySet;
        }

        /**@author:Yangming 这个时候Duty里面不应该没有东西 */
        if (tLCDutySet.size() <= 0)
        {
            // @@错误处理
            System.out.println("LCContSignBLLCContSignBL.java中" + "dealCValiDate方法报错，" + "在程序427行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "LCContSignBL.java";
            tError.functionName = "dealCValiDate";
            tError.errorMessage = "没有查询到责任！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String strDate = tLCDutySet.get(1).getEndDate();
        FDate fDate = new FDate();
        Date CValiDate = fDate.getDate(strDate);
        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            if (CValiDate.before(fDate.getDate(tLCDutySet.get(i).getEndDate())))
            {
                CValiDate = fDate.getDate(tLCDutySet.get(i).getEndDate());
            }
        }
        /**@author:Yangming 兼容性调整：原来存储的时候
         * LCDuty中的EndDaty日期比较乱，以后终身的将全部采取3000-1-1 */
        //        int year = fDate.getDate(PubFun.getCurrentDate()).getYear();
        //        int end = CValiDate.getYear();
        //jdk 1.1以后版本弃用getYear()方法，所以使用Calendar类实现 add by chen xiang wei 20080820
        Calendar calendar = Calendar.getInstance();
        //获取当前年
        calendar.setTime(new Date());
        int year = calendar.get(Calendar.YEAR) - 1900;
        //从数据库中取得年
        calendar.setTime(CValiDate);
        int end = calendar.get(Calendar.YEAR) - 1900;

        if (end - year > 200)
        {
            CValiDate = fDate.getDate("3000-1-1");
        }
        tLCContSchema.setCInValiDate(CValiDate);
        return true;
    }

    /**
     * 生成仅仅需要更新合同号的表的sql
     * @param oldContNo String
     * @param newContNo String
     * @return MMap
     */
    private MMap dealElseUpdateTable(String oldContNo, String newContNo)
    {
        //* ↓ *** liuhao *** 2005-06-21 *** modify **********
        //String[] tables =
        //        {"LCInsured",
        //        "LCAppnt", "LCCustomerImpart",
        //        "LCCustomerImpartParams",};
        String[] tables = { "LCInsured", "LCAppnt", "LCCustomerImpart", "LCCustomerImpartParams", "LCCUWMaster",
                "LCNation", "LCPENotice", "LCSpec", "lcpenoticeitem" };
        //* ↑ *** liuhao *** 2005-06-21 *** modify **********
        String condition = " contno='" + newContNo + "', ModifyDate='" + PubFun.getCurrentDate() + "', ModifyTime='"
                + PubFun.getCurrentTime() + "' ";
        String wherepart = " contno='" + oldContNo + "'";
        Vector vec = PubFun.formUpdateSql(tables, condition, wherepart);
        String[] tLCRiskDutyWraptable = { "LCRiskDutyWrap" };
        String condition2 = "contno='" + newContNo + "'";
        Vector vec2 = PubFun.formUpdateSql(tLCRiskDutyWraptable, condition2, wherepart);
        if (vec != null)
        {
            MMap tmpMap = new MMap();
            for (int i = 0; i < vec.size(); i++)
            {
                tmpMap.put((String) vec.get(i), this.UPDATE);
            }
            tmpMap.put((String) vec2.get(0), this.UPDATE);
            return tmpMap;
        }
        return null;
    }

    /**
     * 校验财务收费，比较一批保单保费之和与所有交费的大小
     *
     * @param prtNo String
     * @param inPolSet LCPolSet return 比较 交费?保单保费 大小 1 > 0 = -1 <100
     *   error
     * @return int
     */
    private int compareTempFee(String prtNo, LCPolSet inPolSet)
    {
        double sumPolPrem = 0.0;
        double sumTmpFee = 0.0;
        LCPolSchema tmpPolSchema = null;
        LJTempFeeSet tLJTempFeeSet = null;
        // --交费校验精确到印刷号，暂时被否定-2005-3-8
        //        tLJTempFeeSet = queryTempFee(prtNo);
        //        if (tLJTempFeeSet == null || tLJTempFeeSet.size() <= 0)
        //         return this.ERROR;
        //        for (int j = 1; j <= tLJTempFeeSet.size(); j++)
        //        {
        //         //到帐
        //         if (tLJTempFeeSet.get(j).getEnterAccDate() != null)
        //         {
        //             sumTmpFee += tLJTempFeeSet.get(j).getPayMoney();
        //         }
        //        }
        //
        //        for (int i = 1; i <= inPolSet.size(); i++)
        //        {
        //
        //            tmpPolSchema = (LCPolSchema) inPolSet.get(i);
        //            sumPolPrem += tmpPolSchema.getPrem();
        //
        //        }
        //
        //        if (sumTmpFee > sumPolPrem)return 1;
        //        if (sumTmpFee < sumPolPrem)return -1;

        return 0;
    }

    /**
     * 根据投保单查询投保单所有的交费
     *
     * @param prtNo LCPolSchema
     * @return LJTempFeeSet
     */
    private LJTempFeeSet queryTempFee(String prtNo)
    {
        LJTempFeeSet tLJTempFeeSet;
        LJTempFeeSchema tLJTempFeeSchema = null;
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setOtherNo(prtNo);
        tLJTempFeeSchema.setOtherNoType("4");
        tLJTempFeeSchema.setConfFlag("0");
        // tLJTempFeeSchema.setRiskCode( tmpPolSchema.getRiskCode());
        tLJTempFeeDB.setSchema(tLJTempFeeSchema);
        tLJTempFeeSet = tLJTempFeeDB.query();
        if (tLJTempFeeDB.mErrors.needDealError())
        {
            CError.buildErr(this, "queryTempFee", tLJTempFeeDB.mErrors);
            return null;
        }

        return tLJTempFeeSet;
    }

    /**
     * 查询保单的交费 --精确到险种
     * @param tmpPolSchema LCPolSchema
     * @return LJTempFeeSet
     */
    private LJTempFeeSet queryTempFeeRisk(LCPolSchema tmpPolSchema)
    {
        LJTempFeeSet tLJTempFeeSet;
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();

        String tStrSql = " select * from LJTempFee ljtf " + " where ljtf.OtherNoType = '4' " // 个单暂收核销前标志
                // + " and ljtf.RiskCode != '000000' " // 除去保全相关暂收数据
                + " and ljtf.ConfFlag = '0' " // 可用销暂收数据
                + " and ljtf.EnterAccDate is not null " // 财务到帐
                + " and ljtf.OtherNo = '" + tmpPolSchema.getPrtNo() + "' ";

        tLJTempFeeSet = tLJTempFeeDB.executeQuery(tStrSql);
        System.out.println("sql: " + tStrSql);
        if (tLJTempFeeDB.mErrors.needDealError())
        {
            CError.buildErr(this, "queryTempFee", tLJTempFeeDB.mErrors);
            return null;
        }
        return tLJTempFeeSet;
    }

    /**
     * 比较保单的交费 --精确到险种 --没查到报错
     *
     * @param inPolSet LCPolSchema
     * @return LJTempFeeSet
     */
    private boolean compareTempFeeRiskErr(LCPolSet inPolSet)
    {
        //安险种分组保单交费求和
        String sql = "select sum( paymoney ),riskcode from ljtempfee where otherno='" + inPolSet.get(1).getPrtNo()
                + "' and othernotype='4' and " + " confflag='0'  and (EnterAccDate is not null ) "
                + " and riskcode != '000000' " + " group by riskcode  ";
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = exesql.execSQL(sql);
        if (ssrs == null)
        {
            CError.buildErr(this, "交费不足或交费没有到帐");
            return false;
        }
        double sumtmpfeemoney = 0;
        double sumpolprem = 0;
        //按险种比较交费信息
        for (int i = 1; i <= ssrs.getMaxRow(); i++)
        {
            String riskcode = (String) ssrs.GetText(i, 2);
            double tempfeemoney = Double.parseDouble(ssrs.GetText(i, 1));
            sumtmpfeemoney += tempfeemoney;
            double polprem = 0;
            double tDSupplementaryPrem = 0d;
            for (int j = 1; j <= inPolSet.size(); j++)
            {
                if (inPolSet.get(j).getRiskCode().equals(riskcode))
                {
                    polprem += inPolSet.get(j).getPrem();

                    // 累计新单追加保费。目前只作用于万能类险种。
                    tDSupplementaryPrem = getPolSupplementaryPrem(inPolSet.get(j).getPolNo(), riskcode);
                    polprem += tDSupplementaryPrem;
                    // ------------------------------
                }
            }
            double differ = Math.abs(tempfeemoney - polprem);
            if (differ > 0.001)
            {
                CError.buildErr(this, "财务暂收费'" + tempfeemoney + "'保单保费'" + polprem + "',保费不一致请确认!");
                return false;
            }
        }

        //整个个人保单交费保存
        for (int j = 1; j <= inPolSet.size(); j++)
        {
            sumpolprem += inPolSet.get(j).getPrem();
        }
        double differ = PubFun.setPrecision(sumtmpfeemoney, "0.00") - PubFun.setPrecision(sumpolprem, "0.00");
        if (differ < 0.0)
        {
            CError.buildErr(this, "保单总交费不足或交费没有到帐");
            return false;
        }
        //* ↓ *** LiuHao *** 2005-05-08 *** add **************
        if (differ < -0.001)
        {
            CError.buildErr(this, "保单总交费大于总保费！");
            return false;
        }
        //* ↑ *** LiuHao *** 2005-05-08 *** add **************

        /*
         LJTempFeeSet tLJTempFeeSet;
         LJTempFeeSchema tLJTempFeeSchema = null;
         LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
         tLJTempFeeSchema = new LJTempFeeSchema();
         tLJTempFeeSchema.setOtherNo(tmpPolSchema.getPrtNo());
         tLJTempFeeSchema.setOtherNoType("4");
         tLJTempFeeSchema.setConfFlag("0");
         tLJTempFeeSchema.setRiskCode(tmpPolSchema.getRiskCode());
         tLJTempFeeDB.setSchema(tLJTempFeeSchema);

         tLJTempFeeSet = tLJTempFeeDB.query();

         if (tLJTempFeeDB.mErrors.needDealError())
         {
         CError.buildErr(this, "交费信息查询错误", tLJTempFeeDB.mErrors);
         return false;
         }
         if ( tLJTempFeeSet == null || tLJTempFeeSet.size()==0 )
         {
         CError.buildErr(this,"险种["+ tmpPolSchema.getRiskCode() +"]没有交费");
         return false;
         }
         double prem = 0.00;
         for ( int i=1;i<= tLJTempFeeSet.size();i++)
         {
         if (tLJTempFeeSet.get(i).getEnterAccDate()==null)
         {
         CError.buildErr(this,"险种["+ tmpPolSchema.getRiskCode() +"]交费没有到帐" );
         return false;
         }
         //所交保费求和
         prem += tLJTempFeeSet.get(i).getPayMoney();
         }
         if ( Math.abs( tmpPolSchema.getPrem() - prem )<=0.001)
         {
         CError.buildErr(this,"险种["+ tmpPolSchema.getRiskCode() +"]交费金额与保单载明金额不一致，请查证");
         return false;
         }
         }*/
        return true;

    }

    /**
     * 比较保单的交费 --精确到险种  --没查到报错
     * @param tmpPolSchema LCPolSchema
     * @return LJTempFeeSet
     */
    private boolean compareTempFeeRiskErr(LCPolSchema tmpPolSchema)
    {
        LJTempFeeSet tLJTempFeeSet;
        LJTempFeeSchema tLJTempFeeSchema = null;
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setOtherNo(tmpPolSchema.getPrtNo());
        tLJTempFeeSchema.setOtherNoType("4");
        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setRiskCode(tmpPolSchema.getRiskCode());
        tLJTempFeeDB.setSchema(tLJTempFeeSchema);

        tLJTempFeeSet = tLJTempFeeDB.query();

        if (tLJTempFeeDB.mErrors.needDealError())
        {
            CError.buildErr(this, "交费信息查询错误", tLJTempFeeDB.mErrors);
            return false;
        }
        if (tLJTempFeeSet == null || tLJTempFeeSet.size() == 0)
        {
            CError.buildErr(this, "险种[" + tmpPolSchema.getRiskCode() + "]没有交费");
            return false;
        }
        double prem = 0.00;
        for (int i = 1; i <= tLJTempFeeSet.size(); i++)
        {
            if (tLJTempFeeSet.get(i).getEnterAccDate() == null)
            {
                CError.buildErr(this, "险种[" + tmpPolSchema.getRiskCode() + "]交费没有到帐");
                return false;
            }
            //所交保费求和
            prem += tLJTempFeeSet.get(i).getPayMoney();
        }
        if (Math.abs(tmpPolSchema.getPrem() - prem) <= 0.001)
        {
            CError.buildErr(this, "险种[" + tmpPolSchema.getRiskCode() + "]交费金额与保单载明金额不一致，请查证");
            return false;
        }
        return true;

    }

    /**
     * 校验保单暂收保费是否足额。
     * <br />如果暂收总保费为0，或暂收总保费小于保单总保费，均视为暂缴金额不足。
     * @param cPrtNo 保单印刷号
     * @return
     */
    private boolean chkContTempFee(LCPolSet cPolInfoList)
    {
        String tPrtNo = null;
        // 险种信息prtno一定一致，因此只取第一条即可
        if (cPolInfoList == null || cPolInfoList.size() == 0)
        {
            CError.buildErr(this, "未找到保单险种信息。");
            return false;
        }
        else
        {
            tPrtNo = cPolInfoList.get(1).getPrtNo();
        }

        String tStrSql = " select nvl(sum(ljtf.PayMoney), 0) from LJTempFee ljtf " + " where ljtf.OtherNoType = '4' " // 个单暂收核销前标志
                // + " and ljtf.RiskCode != '000000' " // 除去保全相关暂收数据
                + " and ljtf.ConfFlag = '0' " // 可用销暂收数据
                + " and ljtf.EnterAccDate is not null " // 财务到帐
                + " and ljtf.OtherNo = '" + tPrtNo + "' ";
        ExeSQL exesql = new ExeSQL();
        String tResult = exesql.getOneValue(tStrSql);

        double tTempFeeSumPrem = Double.parseDouble(tResult);
        double tContSumPrem = 0d;
        for (int i = 1; i <= cPolInfoList.size(); i++)
        {
            LCPolSchema tTmpPolInfo = cPolInfoList.get(i);
            tContSumPrem += tTmpPolInfo.getPrem(); // 累计期缴保费
            tContSumPrem += tTmpPolInfo.getSupplementaryPrem(); // 累计追加保费
        }

        if (tContSumPrem == 0 || tTempFeeSumPrem < tContSumPrem)
        {
            if (Math.abs(tContSumPrem - tTempFeeSumPrem) >= 0.001 || tContSumPrem == 0)
            {
                String tStrErr = "首期保费为0，或暂收保费为[" + tTempFeeSumPrem + "]，少于首期保费[" + tContSumPrem + "]";
                CError.buildErr(this, tStrErr);
                return false;
            }
        }

        // 如果需要对财务数据进行其他的相关校验，写到该函数中。
        // --------------------

        return true;
    }

    /**
     * 处理财务数据 包括暂交费，实交费表，退费处理等,更新LCCont中保费保额
     *
     * @param tLCContSchema LCContSchema
     * @param financeData VData financeData ---(VData) PolData --- LCPolSchema
     *   --- LCPremSet ---- LCTempFeeSet //(运行过程种，加入暂交费信息) ----
     *   计算过程仲改变险种保单的living money
     * @param newContNo String
     * @param tPaySerialNo String
     * @return MMap
     */
    private MMap dealFinance(LCContSchema tLCContSchema, VData financeData, String newContNo, String tPaySerialNo)
    {
        VData result = new VData();
        VData onePolVData = null;
        LCPolSchema tmpPolSchema = null;
        LCPremSet tmpPremSet = null;
        VData lackVData = new VData(); //保费不足的保单列表
        VData redundantVData = new VData(); //保费有余的保单列表
        //     VData balanceVData = new VData();
        double sumPrem = 0.0;
        double sumAmnt = 0.0;
        double sumContPrem = 0.0;

        MMap tmpMap = new MMap();
        double sumTmpRiskFee = 0.0;
        //循环合同保单中所有险种保单，进行保费均匀
        for (int i = 0; i < financeData.size(); i++)
        {
            onePolVData = (VData) financeData.get(i);
            if (onePolVData == null || onePolVData.size() <= 0)
            {
                continue;
            }

            tmpPolSchema = (LCPolSchema) onePolVData.getObjectByObjectName("LCPolSchema", 0);

            sumAmnt += tmpPolSchema.getAmnt();
            tmpPremSet = (LCPremSet) onePolVData.getObjectByObjectName("LCPremSet", 0);
            //求总保费
            for (int t = 1; t <= tmpPremSet.size(); t++)
            {
                sumPrem += tmpPremSet.get(t).getPrem();
            }

            //sumContPrem += sumPrem; /** 如果多被保险人同一险种交费，会造成保费求和错误，替换算法 2005-3-10 **/
            sumContPrem += tmpPolSchema.getPrem();

            /** 如果多被保险人同一险种交费，会造成保费求和，故去掉 2005-3-9  wujs**/
            /*** tmpPolSchema.setPrem(PubFun.setPrecision(sumPrem,"0.00")); ***/

            //根据险种取暂交费
            LJTempFeeSet riskTempFeeSet = this.queryTempFeeRisk(tmpPolSchema);
            //            if (riskTempFeeSet == null || riskTempFeeSet.size() <= 0)
            //            {
            //                System.out.println("没有查询到险种保单相关的暂交费:");
            //                continue;
            //            }
            if (riskTempFeeSet != null && riskTempFeeSet.size() > 0)
            {
                //更新暂交费信息,
                tmpMap.add(this.prepareFeeInfo(tmpPolSchema, riskTempFeeSet, tmpPremSet, tPaySerialNo, newContNo));

                //缓存暂交费信息

                this.cacheTempFee(tmpPolSchema.getRiskCode(), riskTempFeeSet.get(1));

            }

            //生成个人实交表
            tmpMap.add(prepareLJAPayPerson(tmpPolSchema, tmpPremSet, tPaySerialNo));

            // 核销万能类险种，契约追加保费。
            String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
            String tSupplementaryPremPaySerialNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
            MMap tTmpMap = dealSupplementaryPrem(tmpPolSchema, tSupplementaryPremPaySerialNo);
            if (tTmpMap != null)
            {
                tmpMap.add(tTmpMap);
            }
            // ----------------------------

            //求同一险种暂交费总和
            double tmpRiskFee = sumPayMoney(riskTempFeeSet);
            /**   均保费,无意义，去掉 2005-3-9  wujs**/
            /**tmpPolSchema.setLeavingMoney(PubFun.setPrecision(tmpRiskFee - tmpPolSchema.getPrem(),"0.00"));***/
            sumTmpRiskFee += tmpRiskFee;
            //暂交费信息加入到计算元素中
            onePolVData.add(riskTempFeeSet);

            //划分两个列表:不足与有余
            //            if (tmpRiskFee > tmpPolSchema.getPrem()) {
            //                redundantVData.add(onePolVData);
            //            }
            //            if (tmpRiskFee < tLCContSchema.getPrem()) {
            //                lackVData.add(onePolVData);
            //            }
            //            System.out.println("校验溢缴，原来的方式是针对单险种的情况，目前应该采取新的方式，针对险种溢缴");
            //            for (int m = 1; m <= riskTempFeeSet.size(); m++) {
            //                if (riskTempFeeSet.get(m).getRiskCode().equals(
            //                        tmpPolSchema.getRiskCode())) {
            //                    if (riskTempFeeSet.get(m).getPayMoney() >
            //                        tmpPolSchema.getPrem()) {
            //                        System.out.println("溢缴！！！！！！");
            //                        redundantVData.add(onePolVData);
            //                    }
            //                }
            //            }
        } //end for financedata

        PubFun.setPrecision(sumContPrem, "0.00");
        //如果是个单,生成实交个人表和实交总表,并均匀保费-暂交费
        if ("1".equals(tLCContSchema.getContType()))
        {
            // 如果先收费保单，如果有溢缴情况，对溢缴部分数据进行核销。
            //            MMap tTmpMarginTempFee = dealMarginMoneyTempFee(tLCContSchema,
            //                    newContNo);
            //            if (tTmpMarginTempFee != null)
            //                tmpMap.add(tTmpMarginTempFee);
            // ----------------------------

            //生成总实交
            MMap JAPayMap = prepareLJAPay(tLCContSchema, sumContPrem, newContNo, tPaySerialNo);
            tmpMap.add(JAPayMap);

            //更新合同保费保额
            /** tLCContSchema.setPrem(sumContPrem); wujs 2003-03-10 **/
            /** tLCContSchema.setAmnt(sumAmnt); wujs 2003-03-10 **/

            /**@author:Yangmin  下列操作为溢交和不足处理，目前溢交将会在
             * LJAPay中生成YET财务数据，在LJAPayPerson针对险种溢交，下列操作将会
             * 作以余额补不足，溢交退费，溢交转续期，但是目前只生成溢交数据，
             * 等待财务处理*/
            if (redundantVData.size() > 0)
            {
                System.out.println("开始处理财务溢交！");
                String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
                String tPaySerialNo_Over = PubFun1.CreateMaxNo("PAYNO", tLimit);
                if (tPaySerialNo_Over.trim().equals(""))
                {
                    mErrors.addOneError(new CError("生成PAYNO失败!"));
                    return null;
                }

                VData overFlowData = new VData();
                LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
                for (int i = 1; i <= redundantVData.size(); i++)
                {
                    VData ontPolData = (VData) redundantVData.getObject(i - 1);
                    LCPolSchema tLCPolSchema = (LCPolSchema) ontPolData.getObjectByObjectName("LCPolSchema", 0);
                    LJTempFeeSet tLJTempFeeSet = (LJTempFeeSet) ontPolData.getObjectByObjectName("LJTempFeeSet", 0);
                    LCPremSet tLCPremSet = (LCPremSet) ontPolData.getObjectByObjectName("LCPremSet", 0);

                    /**@author:Yangming 开始处理 */
                    overFlowData = dealOverFlowPrem(tLCPolSchema, tLCPremSet, tLJTempFeeSet, tPaySerialNo_Over,
                            newContNo);
                    if ((LJAPayPersonSet) overFlowData.getObjectByObjectName("LJAPayPersonSet", 0) != null)
                    {
                        tLJAPayPersonSet
                                .add((LJAPayPersonSet) overFlowData.getObjectByObjectName("LJAPayPersonSet", 0));
                    }
                    if (overFlowData == null)
                    {
                        return null;
                    }
                    MMap overFlowMap = (MMap) overFlowData.getObjectByObjectName("MMap", 0);
                    if (overFlowMap.size() > 0)
                    {
                        tmpMap.add(overFlowMap);
                    }
                }
                /**@author:Yangming 如果有溢交上面就处理完成LJAPayPerson，下面开始处理LJAPay */
                if (tLJAPayPersonSet.size() > 0)
                {
                    double sumPayMoney = 0.0;
                    LJAPaySet tLJAPaySet = new LJAPaySet();
                    for (int i = 1; i <= tLJAPayPersonSet.size(); i++)
                    {
                        sumPayMoney += tLJAPayPersonSet.get(i).getSumActuPayMoney();
                    }
                    if (sumPayMoney > 0)
                    {
                        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
                        tLJAPaySchema.setPayNo(tPaySerialNo_Over);
                        tLJAPaySchema.setIncomeNo(newContNo);
                        tLJAPaySchema.setIncomeType("2");
                        tLJAPaySchema.setAppntNo(tLCContSchema.getAppntNo());
                        tLJAPaySchema.setSumActuPayMoney(sumPayMoney);
                        tLJAPaySchema.setPayDate(maxPayDate);
                        tLJAPaySchema.setEnterAccDate(maxEnterAccDate);
                        tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
                        tLJAPaySchema.setSerialNo(tPaySerialNo);
                        tLJAPaySchema.setOperator(mGlobalInput.Operator);
                        tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
                        tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
                        tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
                        tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
                        tLJAPaySchema.setManageCom(tLCContSchema.getManageCom());
                        tLJAPaySchema.setAgentCom(tLCContSchema.getAgentCom());
                        tLJAPaySchema.setAgentType(tLCContSchema.getAgentType());
                        tLJAPaySet.add(tLJAPaySchema);
                        tLCContSchema.setDif(sumPayMoney);
                    }
                    if (tLJAPaySet.size() > 0)
                    {
                        tmpMap.put(tLJAPaySet, "INSERT");
                    }
                }
            }
            /**   均保费,结构不支持，去掉 2005-3-9  wujs**/
            //            if (false) {
            //                if (lackVData.size() <= 0) {
            //                    System.out.println("没有少交保费的保单");
            //                }
            //                if (redundantVData.size() <= 0) {
            //                    System.out.println("没有保费多余的保单");
            //                }
            //                int redundantPos = 0;
            //                int lackPos = 0;
            //                if (lackVData.size() > 0 && redundantVData.size() > 0) {
            //                    VData oneLackData = null;
            //                    VData oneRedundantData = null;
            //                    LCPolSchema lackPol = null;
            //                    LCPolSchema redundantPolSchema = null;
            //                    LJTempFeeSet lackFeeSet = null;
            //                    LJTempFeeSet redundantFeeSet = null;
            //                    double adjustMoney = 0.0; //用以调配的money
            //
            //                    double left = 0.0;
            //                    //以富余补不足
            //                    while (lackPos < lackVData.size() &&
            //                           redundantPos < redundantVData.size()) {
            //                        if (left >= 0) {
            //                            oneLackData = (VData) lackVData.get(lackPos);
            //                            lackPol = (LCPolSchema) oneLackData.
            //                                      getObjectByObjectName(
            //                                              "LCPolSchema", 0);
            //                            lackFeeSet = (LJTempFeeSet) oneLackData.
            //                                         getObjectByObjectName(
            //                                                 "LJTempFeeSet", 0);
            //                            //还缺多少钱
            //                            lackPol.setLeavingMoney(PubFun.setPrecision(
            //                                    sumPayMoney(lackFeeSet) -
            //                                    lackPol.getPrem(), "0.00"));
            //                        }
            //                        if (left <= 0) {
            //                            oneRedundantData = (VData) redundantVData.get(
            //                                    redundantPos);
            //                            redundantPolSchema = (LCPolSchema) oneRedundantData.
            //                                                 getObjectByObjectName(
            //                                    "LCPolSchema", 0);
            //                            redundantFeeSet = (LJTempFeeSet) oneRedundantData.
            //                                              getObjectByObjectName(
            //                                    "LJTempFeeSet", 0);
            //                            //还多多少钱
            //                            redundantPolSchema.setLeavingMoney(PubFun.
            //                                    setPrecision(sumPayMoney(
            //                                            redundantFeeSet) -
            //                                                 redundantPolSchema.getPrem(),
            //                                                 "0.00"));
            //                        }
            //
            //                        double lackmoney = lackPol.getLeavingMoney();
            //                        double redundantmoney = redundantPolSchema.
            //                                                getLeavingMoney();
            //
            //                        left = redundantmoney - lackmoney;
            //                        //   System.out.println("adjustMoney:" + adjustMoney);
            //                        PubFun.setPrecision(left, "0.00");
            //                        if (left >= 0) { //富余足以加给不足
            //                            adjustMoney = lackmoney;
            //                            tmpMap.add(createTempFee(redundantPolSchema,
            //                                    lackPol, adjustMoney));
            //                            tmpMap.add(this.prepareUpdateTempFee(oneLackData));
            //                            lackPol.setLeavingMoney(0);
            //                            redundantPolSchema.setLeavingMoney(PubFun.
            //                                    setPrecision(
            //                                            redundantPolSchema.
            //                                            getLeavingMoney() -
            //                                            adjustMoney, "0.00"));
            //                            lackPos++;
            //                        } else { //富余不足加给不足
            //                            //把富足的全部分给不足
            //                            adjustMoney = redundantmoney;
            //                            tmpMap.add(createTempFee(redundantPolSchema,
            //                                    lackPol,
            //                                    adjustMoney));
            //                            tmpMap.add(this.prepareUpdateTempFee(
            //                                    oneRedundantData));
            //                            redundantPolSchema.setLeavingMoney(0);
            //                            lackPol.setLeavingMoney(PubFun.setPrecision(lackPol.
            //                                    getLeavingMoney() -
            //                                    adjustMoney, "0.00"));
            //                            redundantPos++;
            //                        }
            //
            //                    }
            //
            //                } //end if
            //
            //                //还有少交保费的保单,要生成催收
            //                if (lackPos < lackVData.size()) {
            //                    // System.out.println("还有少交保费的保单,要生成催收");
            //                    return null;
            //                }
            //                if ("1".equals(tLCContSchema.getOutPayFlag())) { //溢交退费
            //                    //还有有保费多余的保单,要生成退费
            //                    if (redundantPos < redundantVData.size()) {
            //                        LCPolSet withdrawLCPolSet = new LCPolSet();
            ////                        System.out.println("还有有保费多余的保单,要生成退费");
            //                        for (int t = redundantPos; t < redundantVData.size(); t++) {
            //                            VData oneRedundantData = (VData) redundantVData.get(
            //                                    t);
            //                            withdrawLCPolSet.add((LCPolSchema) oneRedundantData.
            //                                                 getObjectByObjectName(
            //                                    "LCPolSchema", 0));
            //
            //                            NewPolFeeWithdrawBL tNewPolFeeWithdrawBL = new
            //                                    NewPolFeeWithdrawBL();
            //                            VData inputData = new VData();
            //                            inputData.add(withdrawLCPolSet);
            //                            inputData.add(tLCContSchema);
            //                            inputData.add(this.mGlobalInput);
            //                            MMap map = tNewPolFeeWithdrawBL.submitDataAllNew(
            //                                    inputData);
            //                            tmpMap.add(map);
            //                        }
            //                    }
            //                } else { //移交到续期
            //                    tLCContSchema.setDif(PubFun.setPrecision(sumTmpRiskFee -
            //                            sumContPrem, "0.00"));
            //                }
            //
            //            } //end if GrpPol_flag
            //            // } //end for financedata
        }
        return tmpMap;
    }

    /**
     * 处理溢交
     *
     * @param tLCPolSchema LCPolSchema
     * @param tLCPremSet LCPremSet
     * @param tLJTempFeeSet String
     * @param tPaySerialNo String
     * @param tNewContNo String
     * @return boolean
     */
    private VData dealOverFlowPrem(LCPolSchema tLCPolSchema, LCPremSet tLCPremSet, LJTempFeeSet tLJTempFeeSet,
            String tPaySerialNo, String tNewContNo)
    {
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        VData tOverData = new VData();
        MMap map = new MMap();
        /**@author:Yangming 先处理全部LJAPayPerson */
        for (int i = 1; i <= tLJTempFeeSet.size(); i++)
        {
            if (tLJTempFeeSet.get(i).getRiskCode().equals(tLCPolSchema.getRiskCode()))
            {
                if (tLJTempFeeSet.get(i).getPayMoney() > tLCPolSchema.getPrem())
                {
                    double subPrem = tLJTempFeeSet.get(i).getPayMoney() - tLCPolSchema.getPrem();
                    //险种溢交的钱应该放入险种表
                    tLCPolSchema.setLeavingMoney(subPrem);
                    /**@author:Yangming 应该不会报错 */
                    if (tLCPremSet.size() <= 0)
                    {
                        // @@错误处理
                        System.out.println("LCContSignBLLCContSignBL.java中" + "dealOveFlowPrem方法报错，"
                                + "在程序955行，Author:Yangming");
                        CError tError = new CError();
                        tError.moduleName = "LCContSignBL.java";
                        tError.functionName = "dealOveFlowPrem";
                        tError.errorMessage = "查询缴费项失败！";
                        this.mErrors.addOneError(tError);
                        return null;
                    }
                    for (int m = 1; m <= tLCPremSet.size(); m++)
                    {
                        LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                        tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
                        tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
                        tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                        tLJAPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
                        tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
                        tLJAPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
                        tLJAPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());
                        tLJAPayPersonSchema.setApproveTime(tLCPolSchema.getApproveTime());
                        tLJAPayPersonSchema.setConfDate(tLJTempFeeSet.get(i).getConfDate());
                        tLJAPayPersonSchema.setContNo(tNewContNo);
                        tLJAPayPersonSchema.setCurPayToDate(tLCPolSchema.getPaytoDate());
                        tLJAPayPersonSchema.setDutyCode(tLCPremSet.get(m).getDutyCode());
                        tLJAPayPersonSchema.setEnterAccDate(tLJTempFeeSet.get(i).getEnterAccDate());
                        tLJAPayPersonSchema.setGrpContNo(tLCPremSet.get(m).getGrpContNo());
                        tLJAPayPersonSchema.setGrpPolNo("00000000000000000000");
                        tLJAPayPersonSchema.setLastPayToDate(tLCPolSchema.getCValiDate());
                        PubFun.fillDefaultField(tLJAPayPersonSchema);
                        tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
                        tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
                        tLJAPayPersonSchema.setPayAimClass("1");
                        tLJAPayPersonSchema.setPayCount(1);
                        tLJAPayPersonSchema.setPayDate(this.maxPayDate);
                        tLJAPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());
                        tLJAPayPersonSchema.setPayNo(tPaySerialNo);
                        tLJAPayPersonSchema.setPayPlanCode(tLCPremSet.get(m).getPayPlanCode());
                        tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());
                        tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                        tLJAPayPersonSchema.setSumActuPayMoney(subPrem);
                        tLJAPayPersonSchema.setSumDuePayMoney(subPrem);
                        tLJAPayPersonSchema.setPayType("YET");
                        tLJAPayPersonSet.add(tLJAPayPersonSchema);
                    }
                }
            }
        }
        if (tLJAPayPersonSet.size() > 0)
        {
            tOverData.add(tLJAPayPersonSet);
            map.put(tLJAPayPersonSet, "INSERT");
        }
        if (map.size() > 0)
        {
            tOverData.add(map);
        }
        return tOverData;
    }

    /**
     * 生成LJAPay的数据
     *
     * @param tLCContSchema LCContSchema
     * @param sumpay double
     * @param newContNo String
     * @param tPaySerialNo String
     * @return MMap
     */
    private MMap prepareLJAPay(LCContSchema tLCContSchema, double sumpay, String newContNo, String tPaySerialNo)
    {
        MMap tmpmap = new MMap();
        sumpay = PubFun.setPrecision(sumpay, "0.00");
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        tLJAPaySchema.setPayNo(tPaySerialNo);
        tLJAPaySchema.setIncomeNo(newContNo);
        tLJAPaySchema.setIncomeType("2");
        tLJAPaySchema.setAppntNo(tLCContSchema.getAppntNo());
        tLJAPaySchema.setSumActuPayMoney(sumpay);
        tLJAPaySchema.setPayDate(maxPayDate);
        //        /**@author:Yangming 目前PayDate存储实收日期（签单日期） */
        //        tLJAPaySchema.setPayDate(mCurrentDate);
        tLJAPaySchema.setEnterAccDate(maxEnterAccDate);
        tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
        tLJAPaySchema.setSerialNo(tPaySerialNo);
        tLJAPaySchema.setOperator(mGlobalInput.Operator);
        tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
        tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
        tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());

        tLJAPaySchema.setManageCom(tLCContSchema.getManageCom());
        tLJAPaySchema.setAgentCom(tLCContSchema.getAgentCom());
        tLJAPaySchema.setAgentType(tLCContSchema.getAgentType());
        tLJAPaySchema.setSaleChnl(tLCContSchema.getSaleChnl());

        // 契约新单核销实收标志。契约标志：“0”
        tLJAPaySchema.setDueFeeType("0");
        // ------------------------------

        LJAPaySet tLJAPaySet = new LJAPaySet();
        tLJAPaySet.add(tLJAPaySchema);
        tmpmap.put(tLJAPaySet, this.INSERT);
        //求收费总额
        //      String updatesql ="update ljapay set SumActuPayMoney=(select sum(t.SumActuPayMoney) from ljapayperson t where t.payno=ljapay.payno)"
        //      +" where payno='"+ tPaySerialNo+ "'";
        //      tmpmap.put( updatesql, "UPDATE");

        return tmpmap;
    }

    /**
     * 根据传入的暂交费集合,求暂交费和
     * @param riskTempFeeSet LJTempFeeSet
     * @return double
     */
    private double sumPayMoney(LJTempFeeSet riskTempFeeSet)
    {
        double tmpRiskFee = 0.0;
        for (int j = 1; j <= riskTempFeeSet.size(); j++)
        {
            tmpRiskFee += riskTempFeeSet.get(j).getPayMoney();
        }
        return tmpRiskFee;
    }

    /**
     * 合同状态改变
     * @param inContSchema LCContSchema
     * @param newContNo String
     * @return MMap
     */
    private MMap prepareCont(LCContSchema inContSchema, String newContNo)
    {
        MMap tmpMap = new MMap();
        LCContSchema contSchema = new LCContSchema();
        contSchema.setSchema(inContSchema);
        contSchema.setContNo(newContNo);

        //保单收据号
        String tContPremFeeNo = "";
        //客户回执号
        String tCusReceiptNo = "";

        /*
         //保费收据号
         contSchema.setContPremFeeNo(PubFun1.CreateMaxNo("ContPremFeeNo", ""));
         //客户回执号
         contSchema.setCustomerReceiptNo(PubFun1.CreateMaxNo("CusReceiptNo",
         inContSchema.getContNo()));
         */

        contSchema.setSignCom(mGlobalInput.ManageCom);
        //非团单处理签单时间、收据号、回执号
        if ("1".equals(contSchema.getContType()))
        {
            //* ↓ *** LiuHao *** 2005-05-19 *** add *******************
            //保单收据号
            tContPremFeeNo = PubFun1.CreateMaxNo("ContPremFeeNo", null);
            if (tContPremFeeNo.trim().equals(""))
            {
                mErrors.addOneError(new CError("生成ContPremFeeNo失败!"));
                return null;
            }

            //客户回执号
            tCusReceiptNo = PubFun1.CreateMaxNo("CusReceiptNo", newContNo);
            if (tCusReceiptNo.trim().equals(""))
            {
                mErrors.addOneError(new CError("生成CusReceiptNo失败!"));
                return null;
            }

            //* ↑ *** LiuHao *** 2005-05-19 *** add *******************

            contSchema.setSignDate(mCurrentDate);
            contSchema.setSignTime(mCurrentTime);
            //* ↓ *** LiuHao *** 2005-05-19 *** modify *******************
            //保费收据号
            contSchema.setContPremFeeNo(tContPremFeeNo);
            //客户回执号
            contSchema.setCustomerReceiptNo(tCusReceiptNo);
            //* ↑ *** LiuHao *** 2005-05-19 *** modify *******************

            //加个单签单时间
            String polupdate = "update lcpol set signDate ='" + mCurrentDate + "'" + ", signtime='" + mCurrentTime
                    + "'" + " where contno='" + newContNo + "'";
            //加账户成立日

            tmpMap.put(polupdate, "UPDATE");
        }
        if (StrTool.cTrim(this.mOperate).equals("PREVIEW"))
        {
            contSchema.setAppFlag("9");
        }
        else
        {
            contSchema.setAppFlag("1");
            contSchema.setStateFlag("1");
            contSchema.setModifyDate(mCurrentDate);
            contSchema.setModifyTime(mCurrentTime);
        }
        contSchema.setPaytoDate(maxPayDate);
        //        System.out.println("max PayDate "+this.maxPayDate.toString());

        contSchema.setFirstPayDate(mFirstPayDate);

        //        //卡单标志 //意外险手动回执回销  --|| "2".equals(contSchema.getCardFlag())
        if ("1".equals(contSchema.getCardFlag()))
        {
            contSchema.setGetPolDate(PubFun.getCurrentDate());
            contSchema.setGetPolTime(PubFun.getCurrentTime());
            contSchema.setCustomGetPolDate(PubFun.getCurrentDate());
            contSchema.setPrintCount(1);
            //            contSchema.setCValiDate(PubFun.getCurrentDate());
        }
        /**
         * 跟新LCCont表中的Prem值，sumPrem值保持和LCpol一致
         * modify by zhangxing
         * 2005-1-10
         */
        String tSql = "select sum(prem) from lcpol where contno = '" + inContSchema.getContNo()
                + "' and uwflag in ('4','9') ";
        ExeSQL tsql = new ExeSQL();
        String newContPrem = tsql.getOneValue(tSql);
        double ContPrem = 0.0;
        if (newContPrem == null || newContPrem.equals("0") || newContPrem.equals(""))
        {
            ContPrem = 0.0;
        }
        else
        {
            ContPrem = Double.parseDouble(newContPrem);
        }

        contSchema.setPrem(ContPrem);

        tmpMap.put(inContSchema, "DELETE");
        tmpMap.put(contSchema, "INSERT");
        mVResult.add(contSchema);
        return tmpMap;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        //全局变量
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        //@@可以处理保单集和单个保单的情况
        mLCContSet = (LCContSet) mInputData.getObjectByObjectName("LCContSet", 0);

        if (mLCContSet == null || mLCContSet.size() <= 0)
        {
            CError.buildErr(this, "没有传入数据集!");
            return false;
        }
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);

        //签单使用统一时间
        if (mTransferData != null)
        {
            mCurrentDate = (String) mTransferData.getValueByName("CurrentDate");
            mCurrentTime = (String) mTransferData.getValueByName("CurrentTime");
            mEdorNo = (String) mTransferData.getValueByName("EdorNo");
        }
        if (mCurrentDate == null)
        {
            mCurrentDate = PubFun.getCurrentDate();
            mCurrentTime = PubFun.getCurrentTime();

        }

        return true;

    }

    /**
     * 根据保单生成一张新的暂交费表
     *
     * @param grpmorePolSchema LCPolSchema
     * @param grplackPolSchema LCPolSchema
     * @param money double
     * @return MMap
     */
    private MMap createTempFee(LCPolSchema grpmorePolSchema, LCPolSchema grplackPolSchema, double money)
    {
        MMap tmpMap = new MMap();
        LJTempFeeSchema srcTempFeeSchema = (LJTempFeeSchema) this.getTempFee(grpmorePolSchema.getRiskCode());
        if (srcTempFeeSchema == null)
        {
            CError.buildErr(this, "没有查找到可用于借的暂交费");
            return null;
        }
        String confDate = mCurrentDate;
        String confTime = mCurrentTime;
        PubFun.setPrecision(money, "0.00");
        if (money < 0)
        {
            money = -money;
        }

        //创建借的科目
        LJTempFeeSchema tjieSchema = new LJTempFeeSchema();
        tjieSchema.setSchema(srcTempFeeSchema);
        tjieSchema.setTempFeeType("9");
        tjieSchema.setRiskCode(grplackPolSchema.getRiskCode());
        tjieSchema.setPayMoney(money);
        tjieSchema.setMakeDate(confDate);
        tjieSchema.setMakeTime(confTime);
        tjieSchema.setModifyDate(PubFun.getCurrentDate());
        tjieSchema.setModifyTime(PubFun.getCurrentTime());

        //创建贷的科目
        LJTempFeeSchema tdaiSchema = new LJTempFeeSchema();
        tdaiSchema.setSchema(srcTempFeeSchema);
        tdaiSchema.setTempFeeType("8");
        tdaiSchema.setRiskCode(grplackPolSchema.getRiskCode());
        tdaiSchema.setPayMoney(money);
        tdaiSchema.setMakeDate(confDate);
        tdaiSchema.setMakeTime(confTime);
        tdaiSchema.setModifyDate(PubFun.getCurrentDate());
        tdaiSchema.setModifyTime(PubFun.getCurrentTime());
        //缓存
        if (this.getTempFee(grplackPolSchema.getRiskCode()) == null)
        {
            this.cacheTempFee(grplackPolSchema.getRiskCode(), tdaiSchema);
        }
        tmpMap.put(tjieSchema, "INSERT");
        tmpMap.put(tdaiSchema, "INSERT");
        return tmpMap;
    }

    /**
     * 根据保单生成一张新的暂交费表
     * @param polSchema LCPolSchema
     * @param money double
     * @return MMap
     */
    //    private MMap createTempFee(LCPolSchema moreSchema,LCPolSchema lackPolSchema, double money)
    //    {
    //        MMap tmpMap = new MMap();
    //        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
    //        //转换精度
    //        money = PubFun.setPrecision(money, "0.00");
    //        String tPaySerialNo = lackPolSchema.getPrtNo();
    //        tLJTempFeeSchema.setTempFeeNo(tPaySerialNo);
    //        tLJTempFeeSchema.setTempFeeType("8");
    //        tLJTempFeeSchema.setRiskCode(lackPolSchema.getRiskCode());
    //        tLJTempFeeSchema.setOtherNo(lackPolSchema.getPolNo());
    //        String modifyDate = PubFun.getCurrentDate();
    //        String modifyTime = PubFun.getCurrentTime();
    //        tLJTempFeeSchema.setOtherNoType("0");
    //        tLJTempFeeSchema.setConfDate(mCurrentDate);
    //        tLJTempFeeSchema.setConfFlag("1");
    //        tLJTempFeeSchema.setMakeDate(modifyDate);
    //        tLJTempFeeSchema.setMakeTime(modifyTime);
    //        tLJTempFeeSchema.setModifyDate(modifyDate);
    //        tLJTempFeeSchema.setModifyTime(modifyTime);
    //        tLJTempFeeSchema.setPayDate(lackPolSchema.getPaytoDate());
    //        tLJTempFeeSchema.setAgentCode(lackPolSchema.getAgentCode());
    //        tLJTempFeeSchema.setAgentCom(lackPolSchema.getAgentCom());
    //        tLJTempFeeSchema.setAgentGroup(lackPolSchema.getAgentGroup());
    //        tLJTempFeeSchema.setAgentType(lackPolSchema.getAgentType());
    //        tLJTempFeeSchema.setAPPntName(lackPolSchema.getAppntName());
    //     //  tLJTempFeeSchema.setEnterAccDate(confDate);
    //        tLJTempFeeSchema.setSerialNo(tPaySerialNo);
    //
    //        tLJTempFeeSchema.setPayMoney(money);
    //        tLJTempFeeSchema.setConfMakeDate(mCurrentDate);
    //        tLJTempFeeSchema.setSaleChnl(lackPolSchema.getSaleChnl());
    //        tLJTempFeeSchema.setManageCom(lackPolSchema.getManageCom());
    //        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
    //            //  tLJTempFeeSchema.decode(lackPolSchema.getMasterPolNo());
    //        tmpMap.put(tLJTempFeeSchema, this.INSERT);
    //        /*
    //        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    //        tLJTempFeeClassSchema.setTempFeeNo(tPaySerialNo);
    //        tLJTempFeeClassSchema.setPayMode("7");
    //        tLJTempFeeClassSchema.setConfMakeDate(mCurrentDate);
    //        tLJTempFeeClassSchema.setConfDate(mCurrentDate);
    //        tLJTempFeeClassSchema.setConfFlag("1");
    //        tLJTempFeeClassSchema.setPayMoney(money);
    //        tLJTempFeeClassSchema.setSerialNo(tPaySerialNo);
    //           //   tLJTempFeeClassSchema.setAccName();
    //        tLJTempFeeClassSchema.setMakeDate(mCurrentDate);
    //        tLJTempFeeClassSchema.setMakeTime(mCurrentTime);
    //        tLJTempFeeClassSchema.setModifyDate( mCurrentDate );
    //        tLJTempFeeClassSchema.setModifyTime( mCurrentTime );
    //        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
    //        //tLJTempFeeClassSchema.set
    //        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    //        tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
    //
    //        tmpMap.put(tLJTempFeeClassSet, INSERT);
    //     */
    //        return tmpMap;
    //    }
    /**
     * 根据传入的暂交费信息生成更新的数据
     *
     * @param tmpFeeData VData
     * @return MMap
     */
    private MMap prepareUpdateTempFee(VData tmpFeeData)
    {

        MMap tmpMap = new MMap();
        LJTempFeeSet tLJTempFeeSet = (LJTempFeeSet) tmpFeeData.getObjectByObjectName("LJTempFeeSet", 0);
        LCPolSchema polSchema = (LCPolSchema) tmpFeeData.getObjectByObjectName("LCPolSchema", 0);
        LJTempFeeSchema tLCTempSchema = null;
        String modifyDate = PubFun.getCurrentDate();
        String modifyTime = PubFun.getCurrentTime();
        //拷贝一份增加到删除队列
        tmpMap.put(PubFun.copySchemaSet(tLJTempFeeSet), this.DELETE);

        for (int i = 1; i <= tLJTempFeeSet.size(); i++)
        {
            tLCTempSchema = tLJTempFeeSet.get(i);
            tLCTempSchema.setOtherNo(polSchema.getPolNo());
            //2005-3-8更改为2
            ///tLCTempSchema.setOtherNoType("0");
            tLCTempSchema.setOtherNoType("2");
            tLCTempSchema.setConfDate(mCurrentDate);
            tLCTempSchema.setConfFlag("1");
            tLCTempSchema.setModifyDate(modifyDate);
            tLCTempSchema.setModifyTime(modifyTime);
        }
        //增加队列
        tmpMap.put(PubFun.copySchemaSet(tLJTempFeeSet), this.INSERT);
        return tmpMap;
    }

    /**
     * 暂交费更新 输出：如果发生错误则返回null,否则返回VData
     * 修改tLCPolSchema的firstpaydate
     *
     * @param tLCPolSchema LCPolSchema
     * @param tLJTempFeeSet LJTempFeeSet
     * @param tLCPremSet LCPremSet
     * @param tPaySerialNo String
     * @param newContNo String
     * @return MMap
     */
    private MMap prepareFeeInfo(LCPolSchema tLCPolSchema, LJTempFeeSet tLJTempFeeSet, LCPremSet tLCPremSet,
            String tPaySerialNo, String newContNo)
    {

        MMap tReturn = new MMap();
        String tOldPolNo = tLCPolSchema.getProposalNo();

        String modifyDate = PubFun.getCurrentDate();
        String modifyTime = PubFun.getCurrentTime();
        double sumPay = 0;
        double left = 0;
        FDate fDate = new FDate();
        Date tFirstPayDate = null;
        Date tmaxPayDate = null;
        Date tmaxEnterAccDate = null;

        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();

        int n = tLJTempFeeSet.size();

        for (int i = 1; i <= n; i++)
        {
            LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i);

            LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
            tLJTempFeeClassDB.setTempFeeNo(tLJTempFeeSchema.getTempFeeNo());
            LJTempFeeClassSet tLJTempFeeClassSet1 = tLJTempFeeClassDB.query();

            if (tLJTempFeeClassDB.mErrors.needDealError())
            {
                CError.buildErr(this, "LJTempFeeClass表取数失败!");
                return null;
            }

            // end of if
            int m = tLJTempFeeClassSet1.size();

            for (int j = 1; j <= m; j++)
            {
                LJTempFeeClassSchema tLJTempFeeClassSchema = tLJTempFeeClassSet1.get(j);
                tLJTempFeeClassSchema.setConfDate(mCurrentDate);
                tLJTempFeeClassSchema.setConfFlag("1");
                tLJTempFeeClassSchema.setModifyDate(modifyDate);
                tLJTempFeeClassSchema.setModifyTime(modifyTime);
                tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
            }

            // end of for
            sumPay += tLJTempFeeSchema.getPayMoney();

            tLJTempFeeSchema.setOtherNo(newContNo);

            tLJTempFeeSchema.setOtherNoType("2");
            tLJTempFeeSchema.setConfDate(mCurrentDate);
            tLJTempFeeSchema.setConfFlag("1");
            tLJTempFeeSchema.setModifyDate(modifyDate);
            tLJTempFeeSchema.setModifyTime(modifyTime);

            // 取首次交费日期
            Date payDate = fDate.getDate(tLJTempFeeSchema.getPayDate());

            if (tFirstPayDate == null || payDate.before(tFirstPayDate))
            {
                tFirstPayDate = payDate;
            }
            // LJAPay中使用
            if (tmaxPayDate == null || tmaxPayDate.before(payDate))
            {
                tmaxPayDate = payDate;
            }
            Date enterAccDate = fDate.getDate(tLJTempFeeSchema.getEnterAccDate());
            if (tmaxEnterAccDate == null || tmaxEnterAccDate.before(enterAccDate))
            {
                tmaxEnterAccDate = enterAccDate;
            }
            tLCPolSchema.setFirstPayDate(tFirstPayDate);
            tLJTempFeeSet.set(i, tLJTempFeeSchema);
        }

        //全局交至日期，到帐日期等
        if (maxEnterAccDate == null || maxEnterAccDate.before(tmaxEnterAccDate))
        {
            maxEnterAccDate = tmaxEnterAccDate;
        }
        if (mFirstPayDate == null || tFirstPayDate.before(mFirstPayDate))
        {
            mFirstPayDate = tFirstPayDate;
        }
        if (maxPayDate == null || tmaxPayDate.before(maxPayDate))
        {
            maxPayDate = tmaxPayDate;
        }

        if (tLJTempFeeClassSet.size() > 0)
        {
            tReturn.put(tLJTempFeeClassSet, this.UPDATE);
        }
        if (tLJTempFeeSet.size() > 0)
        {
            tReturn.put(tLJTempFeeSet, this.UPDATE);
        }
        // end of for
        //   }

        return tReturn;
    }

    /**
     * 创建个人实交
     * @param tLCPolSchema LCPolSchema
     * @param tLCPremSet LCPremSet
     * @param tPaySerialNo String
     * @return MMap
     */
    private MMap prepareLJAPayPerson(LCPolSchema tLCPolSchema, LCPremSet tLCPremSet, String tPaySerialNo

    )
    {
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        String modifyDate = PubFun.getCurrentDate();
        String modifyTime = PubFun.getCurrentTime();
        // 实交个人表
        int m = tLCPremSet.size();
        if (m > 0)
        {
            MMap tReturn = new MMap();
            for (int i = 1; i <= m; i++)
            {
                //      String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
                //   String tPayNo = PubFun1.CreateMaxNo("PayNo", tLimit);
                LCPremSchema tLCPremSchema = tLCPremSet.get(i);
                LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());
                tLJAPayPersonSchema.setPayCount(1);
                tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                tLJAPayPersonSchema.setContNo(tLCPolSchema.getContNo());
                tLJAPayPersonSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
                tLJAPayPersonSchema.setPayNo(tPaySerialNo);
                tLJAPayPersonSchema.setPayAimClass("1");
                tLJAPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
                tLJAPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
                tLJAPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem());
                tLJAPayPersonSchema.setSumActuPayMoney(tLCPremSchema.getPrem());
                tLJAPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());
                tLJAPayPersonSchema.setPayDate(maxPayDate);
                //                /**@author:Yangming 目前PayDate存储实收日期（签单日期） */
                //                tLJAPayPersonSchema.setPayDate(mCurrentDate);
                tLJAPayPersonSchema.setPayType("ZC");
                tLJAPayPersonSchema.setEnterAccDate(maxEnterAccDate);
                tLJAPayPersonSchema.setConfDate(mCurrentDate);
                /**@author:Yangming 签单时上期交至日期应为生效日期 */
                tLJAPayPersonSchema.setLastPayToDate(tLCPolSchema.getCValiDate());
                tLJAPayPersonSchema.setCurPayToDate(tLCPremSchema.getPaytoDate());
                //         tLJAPayPersonSchema.setSerialNo(mSerialNo);
                tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
                tLJAPayPersonSchema.setMakeDate(modifyDate);
                tLJAPayPersonSchema.setMakeTime(modifyTime);
                tLJAPayPersonSchema.setModifyDate(modifyDate);
                tLJAPayPersonSchema.setModifyTime(modifyTime);

                tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
                tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
                tLJAPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
                tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
                tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());

                tLJAPayPersonSet.add(tLJAPayPersonSchema);
            }
            tReturn.put(tLJAPayPersonSet, this.INSERT);
            return tReturn;
        }
        return null;
    }

    private MMap dealSupplementaryPrem(LCPolSchema tLCPolSchema, String tPaySerialNo)
    {
        MMap tTmpMap = null;

        // 不是万能类险种，不进行追加保费的核销处理。
        if (!CommonBL.isULIRisk(tLCPolSchema.getRiskCode()))
        {
            return null;
        }

        if (tLCPolSchema.getSupplementaryPrem() > 0)
        {
            tTmpMap = new MMap();

            String modifyDate = PubFun.getCurrentDate();
            String modifyTime = PubFun.getCurrentTime();

            // 核销LJAPayPerson表，PayType为“ZB”，DutyCode与PayPlanCode为“000000”
            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());
            tLJAPayPersonSchema.setPayCount(1);
            tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
            tLJAPayPersonSchema.setContNo(tLCPolSchema.getContNo());
            tLJAPayPersonSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
            tLJAPayPersonSchema.setPayNo(tPaySerialNo);
            tLJAPayPersonSchema.setPayAimClass("1");
            tLJAPayPersonSchema.setDutyCode("000000");
            tLJAPayPersonSchema.setPayPlanCode("222222");
            tLJAPayPersonSchema.setSumDuePayMoney(tLCPolSchema.getSupplementaryPrem());
            tLJAPayPersonSchema.setSumActuPayMoney(tLCPolSchema.getSupplementaryPrem());
            tLJAPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());
            tLJAPayPersonSchema.setPayDate(maxPayDate);
            tLJAPayPersonSchema.setPayType("ZB");
            tLJAPayPersonSchema.setEnterAccDate(maxEnterAccDate);
            tLJAPayPersonSchema.setConfDate(mCurrentDate);
            tLJAPayPersonSchema.setLastPayToDate(tLCPolSchema.getCValiDate());
            tLJAPayPersonSchema.setCurPayToDate(tLCPolSchema.getPaytoDate());
            tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
            tLJAPayPersonSchema.setMakeDate(modifyDate);
            tLJAPayPersonSchema.setMakeTime(modifyTime);
            tLJAPayPersonSchema.setModifyDate(modifyDate);
            tLJAPayPersonSchema.setModifyTime(modifyTime);
            tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
            tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
            tLJAPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
            tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
            tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());

            tTmpMap.put(tLJAPayPersonSchema, SysConst.INSERT);
            // ----------------------------------------------------------------

            // 核销LJAPay
            LJAPaySchema tLJAPaySchema = new LJAPaySchema();
            tLJAPaySchema.setPayNo(tPaySerialNo);
            tLJAPaySchema.setIncomeNo(tLCPolSchema.getContNo());
            tLJAPaySchema.setIncomeType("2");
            tLJAPaySchema.setSumActuPayMoney(tLCPolSchema.getSupplementaryPrem());
            tLJAPaySchema.setPayDate(maxPayDate);
            tLJAPaySchema.setEnterAccDate(maxEnterAccDate);
            tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
            tLJAPaySchema.setSerialNo(tPaySerialNo);
            tLJAPaySchema.setOperator(mGlobalInput.Operator);
            tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
            tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
            tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
            tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
            tLJAPaySchema.setManageCom(tLCPolSchema.getManageCom());
            tLJAPaySchema.setAgentCom(tLCPolSchema.getAgentCom());
            tLJAPaySchema.setAgentType(tLCPolSchema.getAgentType());
            tLJAPaySchema.setSaleChnl(tLCPolSchema.getSaleChnl());
            tLJAPaySchema.setDueFeeType("0");

            tTmpMap.put(tLJAPaySchema, SysConst.INSERT);
            // ------------------------------------------------
        }

        return tTmpMap;
    }

    /**
     * 打印所有错误信息
     */
    public void outErrors()
    {
        if (this.mErrors.needDealError())
        {
            System.out.println("错误信息:");
            for (int i = 0; i < this.mErrors.getErrorCount(); i++)
            {
                System.out.println(this.mErrors.getError(i).errorMessage);
            }
        }
    }

    /**
     * 缓存暂交费信息
     * @param key Object
     * @param tempFeeSchema LJTempFeeSchema
     */
    private void cacheTempFee(Object key, LJTempFeeSchema tempFeeSchema)
    {
        this.mLJTempFeeMap.put(key, tempFeeSchema);
    }

    /**
     * 获取暂交费信息.
     * @param key String
     * @return Object
     */
    private Object getTempFee(String key)
    {
        return mLJTempFeeMap.get(key);
    }

    //* ↓ *** LiuHao *** 2005-05-08 *** add *******************
    //追加险种特约关联表信息
    private void getLCPolSpecRela(LCPolSchema tLCPolSchema, int m)
    {

        LCSpecSet mLCSpecSet = getSpecInfo(tLCPolSchema, m);
        getAddFeeInfo(tLCPolSchema, m, mLCSpecSet);
        getBnfInfo(tLCPolSchema, m, mLCSpecSet);
    }

    private void getBnfInfo(LCPolSchema tLCPolSchema, int m, LCSpecSet mLCSpecSet)
    {
        LCBnfDB tLCBnfDB = new LCBnfDB();
        tLCBnfDB.setPolNo(tLCPolSchema.getProposalNo());
        String strSql = "select * from lcbnf where polno in ('" + tLCPolSchema.getPolNo() + "','"
                + tLCPolSchema.getProposalNo() + "')";
        LCBnfSet tLCBnfSet = tLCBnfDB.executeQuery(strSql);
        /** 当受益人大于一人时添加注视 */
        int count = this.mLCPolSpecRelaSet.size();
        if (tLCBnfSet.size() > 0)
        {
            for (int i = 1; i <= tLCBnfSet.size(); i++)
            {
                LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
                tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
                tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
                tLCPolSpecRelaSchema.setSpecCode(String.valueOf(m));
                tLCPolSpecRelaSchema.setOperator(mGlobalInput.Operator);
                tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i + count));
                tLCPolSpecRelaSchema.setSpecContent(getBnfContent(tLCBnfSet.get(i)));
                mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
            }
        }
    }

    /**
     * getBnfInfo
     *
     * @param tLCBnfSchema LCBnfSchema
     * @return String
     */
    private String getBnfContent(LCBnfSchema tLCBnfSchema)
    {
        if (tLCBnfSchema != null)
        {
            return "受益人：" + tLCBnfSchema.getName() + " 证件号码：" + StrTool.cTrim(tLCBnfSchema.getIDNo()) + " 受益顺位："
                    + tLCBnfSchema.getBnfGrade() + " 受益比例：" + tLCBnfSchema.getBnfLot() * 100 + "％";
        }
        return "";
    }

    private LCSpecSet getSpecInfo(LCPolSchema tLCPolSchema, int m)
    {
        LCSpecDB mLCSpecDB = new LCSpecDB();
        mLCSpecDB.setPolNo(tLCPolSchema.getProposalNo());
        LCSpecSet mLCSpecSet = mLCSpecDB.query();
        if (mLCSpecSet.size() > 0)
        {
            for (int i = 1; i <= mLCSpecSet.size(); i++)
            {
                LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
                tLCPolSpecRelaSchema.setGrpContNo(mLCSpecSet.get(i).getGrpContNo());
                tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
                tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
                tLCPolSpecRelaSchema.setSpecCode(String.valueOf(m));
                tLCPolSpecRelaSchema.setOperator(mGlobalInput.Operator);
                tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i));
                tLCPolSpecRelaSchema.setSpecContent(mLCSpecSet.get(i).getSpecContent());
                mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
            }
        }
        return mLCSpecSet;
    }

    private void getAddFeeInfo(LCPolSchema tLCPolSchema, int m, LCSpecSet mLCSpecSet)
    {
        String addFeeSql = "select * from lcprem where polno='" + tLCPolSchema.getProposalNo()
                + "' and payplancode like '000000%'";
        LCPremDB tLCPremDB = new LCPremDB();
        LCPremSet tLCPremSet = tLCPremDB.executeQuery(addFeeSql);
        if (tLCPremSet.size() > 0)
        {
            for (int i = 1; i <= tLCPremSet.size(); i++)
            {
                LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
                tLCPolSpecRelaSchema.setGrpContNo(tLCPremSet.get(i).getGrpContNo());
                tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
                tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
                tLCPolSpecRelaSchema.setSpecCode(String.valueOf(m));
                tLCPolSpecRelaSchema.setOperator(mGlobalInput.Operator);
                tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i + mLCSpecSet.size()));
                double addfee = tLCPremSet.get(i).getPrem();
                double addFeeRate = tLCPremSet.get(i).getRate();
                String StrarDate = tLCPremSet.get(i).getPayStartDate();
                String EndDate = tLCPremSet.get(i).getPayEndDate();
                String addFeeResult = "";
                if (addfee > 0 || addFeeRate > 0)
                {
                    addFeeResult = "附加风险保费";
                    addFeeResult += (addFeeRate > 0 ? String.valueOf(addFeeRate * 100) + "％" : String.valueOf(addfee))
                            + "; ";
                    addFeeResult += "加费自" + StrarDate.split("-")[0] + "年" + StrarDate.split("-")[1] + "月"
                            + StrarDate.split("-")[2] + "日" + "开始 ";
                    if (!StrTool.cTrim(EndDate).equals(""))
                    {
                        addFeeResult += "至" + EndDate.split("-")[0] + "年" + EndDate.split("-")[1] + "月"
                                + EndDate.split("-")[2] + "日" + "终止";
                    }
                }
                tLCPolSpecRelaSchema.setSpecContent("附加保费" + tLCPremSet.get(i).getPrem() + "元");
                mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
            }
        }
    }

    //追加险种特约关联表信息入库
    private void LCPolSpecRelaSubmit()
    {
        PubSubmit ps = new PubSubmit();
        VData tInput = new VData();
        MMap tmpMap = new MMap();

        tmpMap.put(mLCPolSpecRelaSet, "INSERT");
        tInput.add(tmpMap);
        if (!ps.submitData(tInput, null))
        {
            CError.buildErr(this, "险种特约关联信息插入失败！");
        }

        mLCPolSpecRelaSet = null;
    }

    //* ↑ *** LiuHao *** 2005-05-08 *** add *******************

    public static void main(String[] args)
    {
        /**@author:Yangming 调试用 */
        String ContNo = "13002082562";

        LCContSet cont = new LCContSet();
        LCContSchema schema = new LCContSchema();
        GlobalInput mGlobalInput = new GlobalInput();
        VData d = new VData();
        mGlobalInput.Operator = "CP9506";
        mGlobalInput.ManageCom = "86950000";
        mGlobalInput.ComCode = "86950000";
        schema.setContNo(ContNo);
        cont.add(schema);
        d.add(cont);
        d.add(mGlobalInput);
        LCContSignBL t = new LCContSignBL();
        if (!t.submitData(d, ""))
        {
            System.out.println("报错 ：" + t.mErrors.getFirstError());
        }
        VData m = t.getResult();
        PubSubmit ps = new PubSubmit();
        ps.submitData(m, "INSERT");
        if (ps.mErrors.needDealError())
        {
            System.out.println("报错 ：　" + ps.mErrors.getFirstError());
        }
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCContSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    private boolean CheckAppntNum(LCContSchema mLCContSchema)
    {
        int Appntnum = 0;
        StringBuffer sql = new StringBuffer();
        int numString = mLCContSchema.getAppntNo().length();

        // 修正投保人校验问题。
        // 如果投保人投过多次保单，而最近一次的保单做过保全投保人变更的话，
        // 会造成ContNo生成冲突。
        sql.append(" select max(maxno) from ( ");
        sql.append(" select max(int(substr(contno," + (numString + 1) + "))) maxno from ( ");
        sql.append(" select contno from lccont where appntno='" + mLCContSchema.getAppntNo()
                + "' AND CONTTYPE='1' and appflag in('1','9') and contno not like '%E%' ");
        sql.append(" union ");
        sql.append(" select contno from lbcont where appntno='" + mLCContSchema.getAppntNo() + "' AND CONTTYPE='1' and contno not like '%E%' ");
        sql.append(" union ");
        sql.append(" select contno from lobcont where appntno='" + mLCContSchema.getAppntNo()
                + "' AND CONTTYPE='1' and appflag in('1','9') and contno not like '%E%' ");
        sql.append(" ) as X  ");
        sql.append(" Union ");
        sql.append(" select max(int(substr(contno," + (numString + 1) + "))) maxno from ( ");
        sql.append(" select contno from lccont where ContNo like '" + mLCContSchema.getAppntNo()
                + "%' AND CONTTYPE='1' and appflag in('1','9') and contno not like '%E%' ");
        sql.append(" union ");
        sql.append(" select contno from lbcont where ContNo like '" + mLCContSchema.getAppntNo()
                + "%' AND CONTTYPE='1' and contno not like '%E%' ");
        sql.append(" union ");
        sql.append(" select contno from lobcont where ContNo like '" + mLCContSchema.getAppntNo()
                + "%' AND CONTTYPE='1' and appflag in('1','9') and contno not like '%E%' ");
        sql.append(" ) as Y  ");
        sql.append(" ) as temp ");

        System.out.println("SQL" + sql.toString());
        SSRS tSSRS = (new ExeSQL()).execSQL(sql.toString());
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSet tLDPersonSet = new LDPersonSet();
        tLDPersonDB.setCustomerNo(mLCContSchema.getAppntNo());
        tLDPersonSet = tLDPersonDB.query();
        if (tLDPersonSet.size() == 1)
        {
            Appntnum = tLDPersonSet.get(1).getAppntNum();
            System.out.println("Appntnum" + Appntnum);
        }
        else
        {
            buildError("CheckAppntNum", "该客户在客户表中的记录有误！");
            return false;
        }
        int newAppntnum = Integer.parseInt(tSSRS.GetText(1, 1));
        System.out.println("newAppntnum" + newAppntnum);
        if (Appntnum != newAppntnum)
        {
            tAppntnum = newAppntnum;
        }
        else
        {
            tAppntnum = newAppntnum;
        }
        String sql1 = "update LDPerson set AppntNum=" + tAppntnum + ",modifydate = current date,modifytime='"+PubFun.getCurrentTime()+"' where CustomerNo='" + mLCContSchema.getAppntNo()
                + "'";
        map1.put(sql1, "UPDATE");
        PubSubmit tPubSubmit = new PubSubmit();
        VData tVData = new VData();
        tVData.add(map1);
        if (!tPubSubmit.submitData(tVData, ""))
        {
            buildError("CheckAppntNum", "更新客户信息表出现错误！");
            return false;
        }
        return true;
    }

    /**
     * 加入签单限制：
     * 2008年1月1日开始，不能对生效日期为2007年的个单进行签发。
     * @return
     */
    private boolean checkSignDate(LCContSchema tLCContSchema)
    {
        Date tCurrentDate = new FDate().getDate(mCurrentDate);
        Date tLimitDate = new FDate().getDate("2008-01-01");
        Date tLimitDate_2007 = new FDate().getDate("2007-12-31");

        if (tCurrentDate.after(tLimitDate_2007))
        {
            Date tCValiDate = new FDate().getDate(tLCContSchema.getCValiDate());
            if (tCValiDate.before(tLimitDate))
            {
                System.out.println("保单生效日期在2008-01-01日之前，不能签单。");
                buildError("checkSignDate", "保单生效日期在2008-01-01日之前，不能签单。");
                return false;
            }
        }

        return true;
    }

    /**
     * 对暂缴保费中溢缴部分，进行核销处理。
     * <br />riskcode为“000000”的，为溢缴部分保费。
     * @param tLCContSchema 要进行核销的保单信息
     * @param tNewContNo    新保单合同号
     * @return
     */
    private MMap dealMarginMoneyTempFee(LCContSchema tLCContSchema, String tNewContNo)
    {
        MMap tResult = null;
        String tStrSql = "select * from LJTempFee ljtf "
                + " where ljtf.OtherNoType = '4' and ljtf.RiskCode = '000000' "
                + " and ljtf.ConfFlag = '0' and ljtf.EnterAccDate is not null " + " and ljtf.OtherNo = '"
                + tLCContSchema.getPrtNo() + "' ";

        // 根据险种取暂交费
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeDB().executeQuery(tStrSql);

        if (tLJTempFeeSet != null && tLJTempFeeSet.size() > 0)
        {
            if (tResult == null)
                tResult = new MMap();

            String modifyDate = PubFun.getCurrentDate();
            String modifyTime = PubFun.getCurrentTime();

            for (int i = 1; i <= tLJTempFeeSet.size(); i++)
            {
                LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i);

                tLJTempFeeSchema.setOtherNo(tNewContNo);
                tLJTempFeeSchema.setOtherNoType("2");

                tLJTempFeeSchema.setConfDate(mCurrentDate);
                tLJTempFeeSchema.setConfFlag("1");

                tLJTempFeeSchema.setModifyDate(modifyDate);
                tLJTempFeeSchema.setModifyTime(modifyTime);
                tResult.put(tLJTempFeeSchema, SysConst.UPDATE);
            }
        }

        return tResult;
    }

    /**
     * 获取保单险种的追加保费金额。
     * <br />目前只有万能类险种，有新契约追加保费概念。
     * @param cPolNo 保单险种流水号
     * @param cRiskCode 险种代码
     * @return 追加保费金额，如果险种无新契约追加保费概念，则返回0。
     */
    private double getPolSupplementaryPrem(String cPolNo, String cRiskCode)
    {
        double tSupplementaryPrem = 0d;

        // 目前只有万能类险种，在新单发盘时累计追加保费。
        if (com.sinosoft.lis.bq.CommonBL.isULIRisk(cRiskCode))
        {
            String tStrSql = null;

            tStrSql = "select sum(lcp.SupplementaryPrem) from LCPol lcp " + " where lcp.UWFlag in ('4', '9') "
                    + " and lcp.PolNo = '" + cPolNo + "' " + " and lcp.RiskCode = '" + cRiskCode + "' ";

            String tResult = new ExeSQL().getOneValue(tStrSql);

            if (!"".equals(tResult))
            {
                tSupplementaryPrem = Double.parseDouble(tResult);
            }
        }

        return tSupplementaryPrem;
    }

    /**
     * 锁定签单动作。
     * @param cLCContSchema
     * @return
     */
    private MMap lockCont(LCContSchema cLCContSchema)
    {
        MMap tMMap = null;

        // 签单锁定标志为：“SC”
        String tLockNoType = "SC";
        // -----------------------

        // 锁定有效时间
        String tAIS = "300";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return tMMap;
    }

    /**
     * 处理个单投保人帐户。
     * <p /><br />投保人帐户金额计算原则：
     * <br />应进帐户金额（溢缴保费） = 暂收总保费 - (保单首期保费 + 追加保费)
     * <p /><br />当溢缴保费为负值时，视为错误数据，阻断。
     * <br />当溢缴保费大于零时，同时下发溢缴退费通知书。
     * @param cContInfo 
     * @return
     */
    private MMap dealSingleContAppAcc(LCContSchema cContInfo, String cNewContNo)
    {
        MMap tMMap = null;

        String tPrtNo = cContInfo.getPrtNo();

        String tStrSql = "select LF_PayMoneyNo('" + tPrtNo + "') from Dual";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("".equals(tResult))
        {
            String tStrErr = "以印刷号查询，未查到合同号。";
            System.out.println(tStrErr);
            buildError("dealSingleContAppAcc", tStrErr);
            return null;
        }

        double tOutPrem = 0;
        try
        {
            tOutPrem = Double.parseDouble(tResult);
            if (tOutPrem < 0)
            {
                String tStrErr = "暂收总保费金额不足。";
                System.out.println(tStrErr);
                buildError("dealSingleContAppAcc", tStrErr);
                return null;
            }
        }
        catch (Exception e)
        {
            String tStrErr = "溢出保费结果异常。";
            System.out.println(tStrErr);
            buildError("dealSingleContAppAcc", tStrErr);
            return null;
        }

        // 生成投保人账户
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(cContInfo.getAppntNo());
        tLCAppAccTraceSchema.setMoney(tOutPrem); //由于个单不能溢缴，账户余额默认0
        tLCAppAccTraceSchema.setOtherNo(cNewContNo);
        tLCAppAccTraceSchema.setOtherType("2"); //与保全统一，调整为2
        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
        AppAcc tAppAcc = new AppAcc();

        tMMap = tAppAcc.accShiftToXQY(tLCAppAccTraceSchema, "0");
        if (tMMap == null)
        {
            String tStrErr = "投保人帐户创建失败。";
            System.out.println(tStrErr);
            buildError("dealAppAcc", tStrErr);
            return null;
        }

        // 发放溢缴通知书
        MMap tTmpMap = null;
        if (tOutPrem > 0)
        {
            tTmpMap = sendYTPrint(cContInfo, cNewContNo);
            if (tTmpMap == null)
            {
                return null;
            }
            tMMap.add(tTmpMap);
            tTmpMap = null;
        }
        // --------------------

        return tMMap;
    }

    /**
     * 生成溢缴通知书待打印数据。
     * @return
     */
    private MMap sendYTPrint(LCContSchema cContInfo, String cNewContNo)
    {
        MMap tMMap = new MMap();

        LOPRTManagerSchema tPayPrintInfo = new LOPRTManagerSchema();

        // 获取结算单缴费凭证号
        String tPrtSeq = null;
        tPrtSeq = PubFun1.CreateMaxNo("YTPAYNO", cContInfo.getPrtNo());

        if (tPrtSeq == null || "".equals(tPrtSeq))
        {
            String tStrErr = "获取溢缴通知书凭证号失败。";
            buildError("sendYTPrint", tStrErr);
            return null;
        }
        tPayPrintInfo.setPrtSeq(tPrtSeq);
        // ----------------------------------

        tPayPrintInfo.setOtherNo(cNewContNo);
        tPayPrintInfo.setOtherNoType(PrintPDFManagerBL.ONT_INDPOL);

        tPayPrintInfo.setCode("07_YT");

        tPayPrintInfo.setAgentCode(cContInfo.getAgentCode());
        tPayPrintInfo.setManageCom(cContInfo.getManageCom());

        tPayPrintInfo.setReqCom(mGlobalInput.ManageCom);
        tPayPrintInfo.setReqOperator(mGlobalInput.Operator);

        tPayPrintInfo.setPrtType(PrintPDFManagerBL.PT_FRONT);

        tPayPrintInfo.setStateFlag("0");

        tPayPrintInfo.setMakeDate(PubFun.getCurrentDate());
        tPayPrintInfo.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tPayPrintInfo, SysConst.INSERT);

        return tMMap;
    }
}
