package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCSpecDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCInsuredRelatedSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 豁免险保额计算处理
 * 
 * @author 张成轩
 */
public class ExemptionRiskBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 登录信息封装 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 保单号 */
	private String mContNo = "";

	/** 保单是否存在豁免险 */
	private boolean exists = false;

	/** 豁免险更新操作符 */
	private final String OPERATE = "UPDATE||PROPOSAL";

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据
	 * @param cOperate
	 *            数据操作
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 判断是否存在豁免险
		if (this.exists) {
			// 进行业务处理
			if (!dealData()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {

		this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		LCPolSchema tLCPolSchema = (LCPolSchema) cInputData
				.getObjectByObjectName("LCPolSchema", 0);

		if (this.mGlobalInput == null || tLCPolSchema == null) {
			addError("getInputData", "接收传入的信息失败!");
			return false;
		}

		ExeSQL tExSql = new ExeSQL();

		this.mContNo = tLCPolSchema.getContNo();
		String tPrtNo = tLCPolSchema.getPrtNo();

		if (this.mContNo == null || "".equals(this.mContNo)) {
			this.mContNo = tExSql
					.getOneValue("select contno from lccont where prtno='"
							+ tPrtNo + "' and conttype='1'");
			if (this.mContNo == null && "".equals(this.mContNo)) {
				addError("getInputData", "接收保单信息失败!");
				return false;
			}
		}

		// 检查是否有豁免险
		String sql = "select 1 from lcpol where contno='"
				+ this.mContNo
				+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";

		if ("1".equals(tExSql.getOneValue(sql))) {
			// 存在豁免险
			this.exists = true;
		} else {
			// 没有豁免险
			this.exists = false;
		}

		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		try {
//			// 获取其他险种总保费
//			String sql = "select sum(prem) from lcpol where contno='"
//					+ this.mContNo
//					+ "' and not exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
			String sql1 = "select 1 from lcpol a where contno = '" + this.mContNo + "' and exists (select 1 from lmriskapp where riskcode=a.riskcode and kindcode='S') ";
			String sql2 = "select 1 from ldcode1 b where codetype='checkexemptionrisk' and exists ("+sql1+" and a.riskcode=b.code1 )  ";
			String sql3 = "select 1 from ldcode1 c where codetype='checkappendrisk' and exists ("+sql2+" and c.code1=b.code ) ";
			String sql = "select sum(prem) from lcpol lc where contno = '" + this.mContNo + "' and ( exists ("+sql2+" and lc.riskcode= b.code) or exists ("+sql3+" and lc.riskcode=c.code) ) ";
			ExeSQL tExSql = new ExeSQL();
			double sumPrem = Double.parseDouble(tExSql.getOneValue(sql));

			// 判断豁免险
			sql = "select * from lcpol where contno='"
					+ this.mContNo
					+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
			LCPolDB tLCPolDB = new LCPolDB();
			LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);

			// 豁免险处理
			for (int index = 1; index <= tLCPolSet.size(); index++) {

				// 设置险种保额
				LCPolSchema tLCPolSchema = tLCPolSet.get(index);
				tLCPolSchema.setAmnt(sumPrem);
				mInputData.addElement(tLCPolSchema);

				LCContSchema tLCContSchema = new LCContSchema();
				tLCContSchema.setContNo(mContNo);
				mInputData.addElement(tLCContSchema);

				// 获取投保人信息
				LCAppntDB tLCAppntDB = new LCAppntDB();
				tLCAppntDB.setContNo(mContNo);
				if (!tLCAppntDB.getInfo()) {
					addError("dealData", "获取保单信息失败!");
					return false;
				}
				LCAppntSchema tLCAppntSchema = tLCAppntDB.getSchema();
				mInputData.addElement(tLCAppntSchema);

				// 获取被保险人信息
				LCInsuredDB tLCInsuredDB = new LCInsuredDB();
				tLCInsuredDB.setContNo(mContNo);
				tLCInsuredDB.setInsuredNo(tLCPolSchema.getInsuredNo());
				if (!tLCInsuredDB.getInfo()) {
					addError("dealData", "获取保单信息失败!");
					return false;
				}
				LCInsuredSchema tLCInsuredSchema = tLCInsuredDB.getSchema();
				mInputData.addElement(tLCInsuredSchema);

				// 不知道干啥用的。。。
				LCInsuredRelatedSet mLCInsuredRelatedSet = new LCInsuredRelatedSet();
				mInputData.addElement(mLCInsuredRelatedSet);

				// 获取受益人信息
				LCBnfDB tLCBnfDB = new LCBnfDB();
				tLCBnfDB.setContNo(mContNo);
				tLCBnfDB.setPolNo(tLCPolSchema.getPolNo());
				tLCBnfDB.setInsuredNo(tLCPolSchema.getInsuredNo());
				LCBnfSet tLCBnfSet = tLCBnfDB.query();
				mInputData.addElement(tLCBnfSet);

				// 获取告知信息
				/*
				 * LCCustomerImpartDB tLCCustomerImpartDB = new
				 * LCCustomerImpartDB(); tLCCustomerImpartDB.setContNo(mContNo);
				 * LCCustomerImpartSet tLCCustomerImpartSet =
				 * tLCCustomerImpartDB .query();
				 */
				mInputData.addElement(new LCCustomerImpartSet());

				// 獲取特变约定
				LCSpecDB tLCSpecDB = new LCSpecDB();
				tLCSpecDB.setContNo(mContNo);
				LCSpecSet tLCSpecSet = tLCSpecDB.query();
				mInputData.addElement(tLCSpecSet);

				// 获取责任信息
				LCDutyDB tLCDutyDB = new LCDutyDB();
				tLCDutyDB.setContNo(mContNo);
				tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
				LCDutySet tLCDutySet = tLCDutyDB.query();
				if (tLCDutySet.size() == 0) {
					addError("dealData", "获取保单责任信息失败!");
					return false;
				}
				LCDutySchema tLCDutySchema = tLCDutySet.get(1);
				tLCDutySchema.setAmnt(sumPrem);

				mInputData.addElement(tLCDutySchema);

				mInputData.addElement(mGlobalInput);

				// 很有问题 要好好看一下都是啥意思
				TransferData tTransferData = new TransferData();
				tTransferData.setNameAndValue("getIntv", "");
				tTransferData.setNameAndValue("GetDutyKind", "");
				tTransferData.setNameAndValue("samePersonFlag", "0");
				tTransferData.setNameAndValue("deleteAccNo", "1");
				tTransferData.setNameAndValue("ChangePlanFlag", "1");
				tTransferData.setNameAndValue("SavePolType", null);
				tTransferData.setNameAndValue("EdorType", null);
				mInputData.addElement(tTransferData);

				ProposalUI tProposalUI = new ProposalUI();
				if (!tProposalUI.submitData(mInputData, this.OPERATE)) {
					addError("dealData", "豁免险保费计算失败!"
							+ tProposalUI.mErrors.getError(0).errorMessage);
					return false;
				}
				mInputData.clear();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			addError("dealData", "豁免险保费计算失败!");
			return false;
		}
		return true;
	}

	/**
	 * 判断险种是否为豁免险
	 * 
	 * @param riskcode
	 *            险种代码
	 * @return true是豁免险 false不是豁免险
	 */
	public boolean checkExemption(String riskcode) {
		ExeSQL tExSql = new ExeSQL();
		String sql = "select 1 from lmriskapp where riskcode='" + riskcode
				+ "' and kindcode='S'";
		if ("1".equals(tExSql.getOneValue(sql))) {
			// 是豁免险
			return true;
		} else {
			// 不是豁免险
			return false;
		}
	}

	/**
	 * 获取保单现保费总和
	 * 
	 * @param riskcode
	 *            险种代码
	 * @return true是豁免险 false不是豁免险
	 */
	public double getSumPrem(String contno,String riskcode) {
		double sumPrem = 0;
		ExeSQL tExSql = new ExeSQL();
		// 检查是否有豁免险
		String sql2 = "select 1 from ldcode1 b where codetype='checkexemptionrisk' and code1='"+riskcode+"' ";
        String sql3 = "select 1 from ldcode1 c where codetype='checkappendrisk' and exists ("+sql2+" and c.code1=b.code ) ";
		String sql = "select sum(prem) from lcpol lc where contno = '" +contno + "' and ( exists ("+sql2+" and lc.riskcode= b.code) or exists ("+sql3+" and lc.riskcode=c.code) ) ";
		
		try {
			sumPrem = Double.parseDouble(tExSql.getOneValue(sql));
		} catch (Exception ex) {
			sumPrem = 0;
		}
		return sumPrem;
	}

	/**
	 * 检查保单豁免险保额是否正常
	 * 
	 * @param contno
	 *            保单号
	 * @return true保额正常 false保额需重算
	 */
	public boolean checkContAmnt(String contno) {
		ExeSQL tExeSQL = new ExeSQL();
		// 判断是否存在豁免险
		String sql = "select 1 from lcpol where contno = '"
				+ contno
				+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
		String result = tExeSQL.getOneValue(sql);
		if ("1".equals(result)) {
			
			String sql1 = "select 1 from lcpol a where contno = '" + contno + "' and exists (select 1 from lmriskapp where riskcode=a.riskcode and kindcode='S') ";
			String sql2 = "select 1 from ldcode1 b where codetype='checkexemptionrisk' and exists ("+sql1+" and a.riskcode=b.code1 )  ";
			String sql3 = "select 1 from ldcode1 c where codetype='checkappendrisk' and exists ("+sql2+" and c.code1=b.code ) ";
			String sql4 = "select sum(prem) from lcpol lc where contno = '" + contno + "' and ( exists ("+sql2+" and lc.riskcode= b.code) or exists ("+sql3+" and lc.riskcode=c.code) ) ";
			
			// 获取除豁免险外保单总保额
			sql = "select 1 from lcpol where contno = '"
					+ contno
					+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S') "
					+ "and amnt <> ("+sql4+")";
			result = tExeSQL.getOneValue(sql);
			if ("1".equals(result)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 检查保单豁免险是否存在加费
	 * 
	 * @param contno
	 *            保单号
	 * @return true存在加费 false不存在加费
	 */
	public boolean checkPrem(String contno) {
		ExeSQL tExeSQL = new ExeSQL();
		// 判断是否存在豁免险
		String sql = "select 1 from lcprem where polno in (select polno from lcpol where contno='"
				+ contno
				+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')) and payplancode like '000000%' with ur";
		String result = tExeSQL.getOneValue(sql);
		if ("1".equals(result)) {
			return true;
		}
		return false;
	}

	private void addError(String function, String manager) {
		CError tError = new CError();
		tError.moduleName = "ExemptionRiskBL";
		tError.functionName = function;
		tError.errorMessage = manager;
		this.mErrors.addOneError(tError);
	}

	public static void main() {

	}
}
