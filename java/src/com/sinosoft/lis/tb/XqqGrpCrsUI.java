package com.sinosoft.lis.tb;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author : lys
 * @date:2003-06-04
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LOMixAgentSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class XqqGrpCrsUI {
    public CErrors mErrors = new CErrors();
    private String mResult;
    private LCContSchema mLCContSchema=new LCContSchema();
    private LCGrpContSchema mLCGrpContSchema=new LCGrpContSchema();
    private LOMixAgentSchema mLOMixAgentSchema=new LOMixAgentSchema();
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mDealElement = new TransferData();

    public XqqGrpCrsUI() {
    }
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("HEAD") &&!cOperate.equals("REPAIR") && !cOperate.equals("DELETE"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            VData vData = new VData();
            if (!prepareOutputData(vData))
            {
                return false;
            }

            XqqGrpCrsBL tRiskAddBL = new XqqGrpCrsBL();
            System.out.println("Start XqqGrpCrsUI Submit ...");
            if (!tRiskAddBL.submitData(vData, cOperate))
            { 
                if (tRiskAddBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tRiskAddBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "tNewFinDayCheckYFBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tRiskAddBL.getResult();
                mLCContSchema=tRiskAddBL.getLCContSchema();
                mLCGrpContSchema= tRiskAddBL.getLCGrpContSchema();
                mLOMixAgentSchema = tRiskAddBL.getLOMixAgentSchema();
                
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "tXqqGrpCrsUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mDealElement);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /*
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mDealElement = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getVResult() {
        VData tVData=new VData();
        tVData.add(this.mErrors.getFirstError());
        return tVData;
      }
  
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "XqqGrpCrsUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    public String getResult() {
        return mResult;
      }
    public LCContSchema getLCContSchema(){
        return mLCContSchema;
    }
    public LCContSchema getLCGrpContSchema(){
        return mLCContSchema;
    }
    public LCContSchema getLOMixAgentSchema(){
        return mLCContSchema;
    }
}
