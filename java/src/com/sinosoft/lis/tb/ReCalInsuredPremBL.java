package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpImportLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.ActivityOperator;

/**
 * <p>Title: 工作流节点任务:团单保费重算 </p>
 * <p>Description: 团单保费重算</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: SinoSoft</p>
 * @author zhangxing
 * @version 1.0
 */

public class ReCalInsuredPremBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 往工作流引擎中传输数据的容器 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private TransferData mTransferData = new TransferData();

  /**工作流引擎 */
  ActivityOperator mActivityOperator = new ActivityOperator();

  /** 业务数据操作字符串 */
  private String mGrpContNo;

  /**保单表*/
  private LCContSet mLCContSet = new LCContSet();

  public ReCalInsuredPremBL() {
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData, cOperate)) {
      return false;
    }

    //校验是否有未打印的体检通知书
    if (!checkData()) {
      return false;
    }

    //进行业务处理
    if (!dealData()) {
      return false;
    }

    return true;
  }

  /**
   * 校验业务数据
   * @return
   */
  private boolean checkData() {
    //校验保单信息
    LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    tLCGrpContDB.setGrpContNo(mGrpContNo);
    if (!tLCGrpContDB.getInfo()) {
      CError tError = new CError();
      tError.moduleName = "ReCalInsuredPremBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在查询LCGrpCont时失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    LCContDB tLCContDB = new LCContDB();
    tLCContDB.setGrpContNo(mGrpContNo);
    mLCContSet = tLCContDB.query();
    if (mLCContSet.size() == 0) {
      CError tError = new CError();
      tError.moduleName = "ReCalInsuredPremBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在删除被保险人时查询LCCont失败原因可能是没有被保险人信息!";
      this.mErrors.addOneError(tError);
      return false;

    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    /**
     * ————————————————————————————
     * 首先删除该保单下的所有被保险人
     * -----------------------------------------------
     */
    VData tVData = new VData();
    tVData.add(mLCContSet);
    tVData.add(mGlobalInput);
    tVData.add(mTransferData);

    LCContDelPUI tLCContDelPUI = new LCContDelPUI();

    if (tLCContDelPUI.submitData(tVData, "DELETE") == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCContDelPUI.mErrors);

      return false;
    }
    else {
      mResult = tLCContDelPUI.getResult();
    }
    
    //增加对团体保单被保险人导入信息的删除
    LCGrpImportLogDB importLogDB = new LCGrpImportLogDB();
    importLogDB.setGrpContNo(mGrpContNo);
    LCGrpImportLogSet importLogSet = importLogDB.query();
    for(int i = 1; i <= importLogSet.size(); i++)
    {
        LCGrpImportLogSchema importLogSchema= importLogSet.get(i);
        importLogDB.setSchema(importLogSchema);
        importLogDB.delete();
    }
    
    /**
     * --------------------------------------------------
     * 计算保费
     * --------------------------------------------------
     */
    VData aVDate = new VData();
    aVDate.add(mGlobalInput);
    aVDate.add(mTransferData);

    ParseGuideInUI tParseGuideInUI = new ParseGuideInUI();
    if (tParseGuideInUI.submitData(aVDate, "INSERT||DATABASE") == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tParseGuideInUI.mErrors);
      return false;
    }
    else {
      mResult = tParseGuideInUI.getResult();
    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData, String cOperate) {
    //从输入数据中得到所有对象
    //获得全局公共数据
    mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
        "TransferData", 0);

    if (mGlobalInput == null) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCContDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWAutoHealthAfterInitService";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
    if (mGrpContNo == null) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCContDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWAutoHealthAfterInitService";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输业务数据中ContNo失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }



  /**
   * 返回处理后的结果
   * @return VData
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 返回工作流中的Lwfieldmap所描述的值
   * @return TransferData
   */
  public TransferData getReturnTransferData() {
    return mTransferData;
  }

  /**
   * 返回错误对象
   * @return CErrors
   */
  public CErrors getErrors() {
    return mErrors;
  }

  public static void main(String[] args) {
    GlobalInput mGlobalInput = new GlobalInput();
    mGlobalInput.ManageCom = "86";
    mGlobalInput.Operator = "001";
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("GrpContNo", "1400000960");
    tTransferData.setNameAndValue("RelatePremDelete", "1");

    VData tVData = new VData();
    tVData.add(tTransferData);
    tVData.add(mGlobalInput);

    ReCalInsuredPremBL tReCalInsuredPremBL = new ReCalInsuredPremBL();
    tReCalInsuredPremBL.submitData(tVData, "");

  }
}
