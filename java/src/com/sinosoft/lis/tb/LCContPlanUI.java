/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
//import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.utility.TransferData;

/**
 * 保障计划前台数据传入接收类
 * <p>Title: </p>
 * <p>Description: 接收前台传入的数据，转入BL类 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author 朱向峰
 * @version 1.0
 */
public class LCContPlanUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new
            LCContPlanDutyParamSet();
    private String mGrpContNo = "";
    private String mContPlanCode = "";
    private String mProposalGrpContNo = "";
    private String mPlanSql = "";
    private String mPlanType = "";
    private String mPeoples3 = "";
    private String mPeoples2 = "";
    private LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
    private LCGrpFeeSet mLCGrpFeeSet = new LCGrpFeeSet();
    private TransferData tTransferData = new TransferData();

    public LCContPlanUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        LCContPlanBL tLCContPlanBL = new LCContPlanBL();
        System.out.println("Start LCContPlan UI Submit...");
        tLCContPlanBL.submitData(mInputData, mOperate);
        System.out.println("End LCContPlan UI Submit...");
        //如果有需要处理的错误，则返回
        if (tLCContPlanBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContPlanBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCContPlanUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("QUERY||MAIN")) {
            this.mResult.clear();
            this.mResult = tLCContPlanBL.getResult();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args) {
        LCContPlanDutyParamSet tLCContPlanDutyParamSet = new
                LCContPlanDutyParamSet();
        LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new
                LCContPlanDutyParamSchema();
        LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
        String tGrpContNo = "1400005699";
        String tGrpPolNo = "12000010838";
        String tRiskCode_1 = "5503";
        String tContPlanCode = "A";
        String tRiskWrapCode = "WR0001";
        //第一个险种
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
        tLCContPlanDutyParamSchema.setDutyCode("619001");
        tLCContPlanDutyParamSchema.setCalFactorValue("500000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYearFlag");
        tLCContPlanDutyParamSchema.setDutyCode("619001");
        tLCContPlanDutyParamSchema.setCalFactorValue("M");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYear");
        tLCContPlanDutyParamSchema.setDutyCode("619001");
        tLCContPlanDutyParamSchema.setCalFactorValue("12");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        tLCContPlanDutyParamSchema.setDutyCode("619001");
        tLCContPlanDutyParamSchema.setCalFactorValue("9.85");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
        tLCContPlanDutyParamSchema.setDutyCode("619002");
        tLCContPlanDutyParamSchema.setCalFactorValue("300000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYearFlag");
        tLCContPlanDutyParamSchema.setDutyCode("619002");
        tLCContPlanDutyParamSchema.setCalFactorValue("M");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYear");
        tLCContPlanDutyParamSchema.setDutyCode("619002");
        tLCContPlanDutyParamSchema.setCalFactorValue("12");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        tLCContPlanDutyParamSchema.setDutyCode("619002");
        tLCContPlanDutyParamSchema.setCalFactorValue("4.55");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
        tLCContPlanDutyParamSchema.setDutyCode("619003");
        tLCContPlanDutyParamSchema.setCalFactorValue("300000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYearFlag");
        tLCContPlanDutyParamSchema.setDutyCode("619003");
        tLCContPlanDutyParamSchema.setCalFactorValue("M");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYear");
        tLCContPlanDutyParamSchema.setDutyCode("619003");
        tLCContPlanDutyParamSchema.setCalFactorValue("12");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        tLCContPlanDutyParamSchema.setDutyCode("619003");
        tLCContPlanDutyParamSchema.setCalFactorValue("4.55");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
        tLCContPlanDutyParamSchema.setDutyCode("619004");
        tLCContPlanDutyParamSchema.setCalFactorValue("100000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYearFlag");
        tLCContPlanDutyParamSchema.setDutyCode("619004");
        tLCContPlanDutyParamSchema.setCalFactorValue("M");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYear");
        tLCContPlanDutyParamSchema.setDutyCode("619004");
        tLCContPlanDutyParamSchema.setCalFactorValue("12");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        tLCContPlanDutyParamSchema.setDutyCode("619004");
        tLCContPlanDutyParamSchema.setCalFactorValue("27.87");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
        tLCContPlanDutyParamSchema.setDutyCode("619005");
        tLCContPlanDutyParamSchema.setCalFactorValue("100000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYearFlag");
        tLCContPlanDutyParamSchema.setDutyCode("619005");
        tLCContPlanDutyParamSchema.setCalFactorValue("M");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("InsuYear");
        tLCContPlanDutyParamSchema.setDutyCode("619005");
        tLCContPlanDutyParamSchema.setCalFactorValue("12");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode_1);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setContPlanCode(tContPlanCode);
        tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        tLCContPlanDutyParamSchema.setDutyCode("619005");
        tLCContPlanDutyParamSchema.setCalFactorValue("33.18");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setRiskCode("5503");
        tLCContPlanRiskSchema.setRiskWrapCode(tRiskWrapCode);
        tLCContPlanRiskSchema.setRiskWrapFlag("Y");
        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
//        tLCContPlanDutyParamSchema.setDutyCode("619002");
//        tLCContPlanDutyParamSchema.setCalFactorValue("100000");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("InsuYearFlag");
//        tLCContPlanDutyParamSchema.setDutyCode("619002");
//        tLCContPlanDutyParamSchema.setCalFactorValue("M");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("InsuYear");
//        tLCContPlanDutyParamSchema.setDutyCode("619002");
//        tLCContPlanDutyParamSchema.setCalFactorValue("12");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("Prem");
//        tLCContPlanDutyParamSchema.setDutyCode("619002");
//        tLCContPlanDutyParamSchema.setCalFactorValue("33.18");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("CalRule");
//        tLCContPlanDutyParamSchema.setDutyCode("619002");
//        tLCContPlanDutyParamSchema.setCalFactorValue("3");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
//        tLCContPlanDutyParamSchema.setDutyCode("619003");
//        tLCContPlanDutyParamSchema.setCalFactorValue("100000");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("InsuYearFlag");
//        tLCContPlanDutyParamSchema.setDutyCode("619003");
//        tLCContPlanDutyParamSchema.setCalFactorValue("M");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("InsuYear");
//        tLCContPlanDutyParamSchema.setDutyCode("619003");
//        tLCContPlanDutyParamSchema.setCalFactorValue("12");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("Prem");
//        tLCContPlanDutyParamSchema.setDutyCode("619003");
//        tLCContPlanDutyParamSchema.setCalFactorValue("27.87");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setRiskCode("5503");
//        tLCContPlanDutyParamSchema.setMainRiskCode("5503");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400005698");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000010852");
//        tLCContPlanDutyParamSchema.setCalFactor("CalRule");
//        tLCContPlanDutyParamSchema.setDutyCode("619003");
//        tLCContPlanDutyParamSchema.setCalFactorValue("3");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        /*
            tLCContPlanRiskSchema = new LCContPlanRiskSchema();
            tLCContPlanRiskSchema.setRiskCode("1601");
            tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

            tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
            tLCContPlanDutyParamSchema.setGrpContNo("1400000789");
            tLCContPlanDutyParamSchema.setRiskCode("1601");
            tLCContPlanDutyParamSchema.setMainRiskCode("1601");
            tLCContPlanDutyParamSchema.setProposalGrpContNo("1400000789");
            tLCContPlanDutyParamSchema.setContPlanCode("A");
            tLCContPlanDutyParamSchema.setGrpPolNo("12000001248");
            tLCContPlanDutyParamSchema.setCalFactor("Mult");
            tLCContPlanDutyParamSchema.setDutyCode("601002");
            tLCContPlanDutyParamSchema.setCalFactorValue("");
            tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

            tLCContPlanRiskSchema = new LCContPlanRiskSchema();
            tLCContPlanRiskSchema.setRiskCode("1601");
            tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

            tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
            tLCContPlanDutyParamSchema.setGrpContNo("1400000789");
            tLCContPlanDutyParamSchema.setRiskCode("1601");
            tLCContPlanDutyParamSchema.setMainRiskCode("1601");
            tLCContPlanDutyParamSchema.setProposalGrpContNo("1400000789");
            tLCContPlanDutyParamSchema.setContPlanCode("A");
            tLCContPlanDutyParamSchema.setGrpPolNo("12000001248");
            tLCContPlanDutyParamSchema.setCalFactor("Prem");
            tLCContPlanDutyParamSchema.setDutyCode("601002");
            tLCContPlanDutyParamSchema.setCalFactorValue("100");
            tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

            tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
            tLCContPlanDutyParamSchema.setGrpContNo("1400000789");
            tLCContPlanDutyParamSchema.setRiskCode("1601");
            tLCContPlanDutyParamSchema.setMainRiskCode("1601");
            tLCContPlanDutyParamSchema.setProposalGrpContNo("1400000789");
            tLCContPlanDutyParamSchema.setContPlanCode("A");
            tLCContPlanDutyParamSchema.setGrpPolNo("12000001248");
            tLCContPlanDutyParamSchema.setCalFactor("Prem");
            tLCContPlanDutyParamSchema.setDutyCode("601001");
            tLCContPlanDutyParamSchema.setCalFactorValue("100");
            tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

            tLCContPlanRiskSchema = new LCContPlanRiskSchema();
            tLCContPlanRiskSchema.setRiskCode("1601");
            tLCContPlanRiskSet.add(tLCContPlanRiskSchema);
         */
        //第二个险种
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400000704");
//        tLCContPlanDutyParamSchema.setRiskCode("1603");
//        tLCContPlanDutyParamSchema.setMainRiskCode("1603");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400000704");
//        tLCContPlanDutyParamSchema.setContPlanCode("B");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000001023");
//        tLCContPlanDutyParamSchema.setCalFactor("GetRate");
//        tLCContPlanDutyParamSchema.setDutyCode("604001");
//        tLCContPlanDutyParamSchema.setCalFactorValue("");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
//        tLCContPlanRiskSchema.setRiskCode("1603");
//        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400000704");
//        tLCContPlanDutyParamSchema.setRiskCode("1603");
//        tLCContPlanDutyParamSchema.setMainRiskCode("1603");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400000704");
//        tLCContPlanDutyParamSchema.setContPlanCode("B");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000001023");
//        tLCContPlanDutyParamSchema.setCalFactor("Prem");
//        tLCContPlanDutyParamSchema.setDutyCode("604001");
//        tLCContPlanDutyParamSchema.setCalFactorValue("");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
//        tLCContPlanRiskSchema.setRiskCode("1603");
//        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);
//
//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400000704");
//        tLCContPlanDutyParamSchema.setRiskCode("1603");
//        tLCContPlanDutyParamSchema.setMainRiskCode("1603");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400000704");
//        tLCContPlanDutyParamSchema.setContPlanCode("B");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000001023");
//        tLCContPlanDutyParamSchema.setCalFactor("FloatRate");
//        tLCContPlanDutyParamSchema.setDutyCode("604001");
//        tLCContPlanDutyParamSchema.setCalFactorValue("");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
//
//        tLCContPlanRiskSchema = new LCContPlanRiskSchema();
//        tLCContPlanRiskSchema.setRiskCode("1603");
//        tLCContPlanRiskSet.add(tLCContPlanRiskSchema);

//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400000690");
//        tLCContPlanDutyParamSchema.setRiskCode("1601");
//        tLCContPlanDutyParamSchema.setMainRiskCode("1601");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400000690");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000000682");
//        tLCContPlanDutyParamSchema.setCalFactor("1");
//        tLCContPlanDutyParamSchema.setDutyCode("605001");
//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

//        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
//        tLCContPlanDutyParamSchema.setGrpContNo("1400000690");
//        tLCContPlanDutyParamSchema.setRiskCode("1601");
//        tLCContPlanDutyParamSchema.setMainRiskCode("1601");
//        tLCContPlanDutyParamSchema.setProposalGrpContNo("1400000690");
//        tLCContPlanDutyParamSchema.setContPlanCode("A");
//        tLCContPlanDutyParamSchema.setGrpPolNo("12000000682");
//        tLCContPlanDutyParamSchema.setCalFactor("1");
//        tLCContPlanDutyParamSchema.setDutyCode("605001");

//        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "8611";
        tG.ManageCom = "8611";
        tG.Operator = "actest";
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tLCContPlanDutyParamSet);
        tVData.add(tGrpContNo);
        tVData.add(tGrpContNo);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("RiskFlag", "Wrap");
        tVData.add(tContPlanCode);
        tVData.add("0");
        tVData.add("0");
        tVData.add("0");
        tVData.add("0");
        tVData.add(tLCContPlanRiskSet);
        tVData.add(tTransferData);
        LCContPlanUI tLCContPlanUI = new LCContPlanUI();
        tLCContPlanUI.submitData(tVData, "INSERT||MAIN");
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLCContPlanDutyParamSet);
            mInputData.add(this.mProposalGrpContNo);
            mInputData.add(this.mGrpContNo);
            mInputData.add(this.mContPlanCode);
            mInputData.add(this.mPlanType);
            mInputData.add(this.mPlanSql);
            mInputData.add(this.mPeoples3);
            this.mInputData.add(this.mPeoples2);
            mInputData.add(this.mLCContPlanRiskSet);
            mInputData.add(this.mLCGrpFeeSet);
            mInputData.add(this.tTransferData);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLCContPlanDutyParamSet.set((LCContPlanDutyParamSet) cInputData.
                                         getObjectByObjectName(
                                                 "LCContPlanDutyParamSet", 0));
        this.mProposalGrpContNo = (String) cInputData.get(2);
        this.mGrpContNo = (String) cInputData.get(3);
        this.mContPlanCode = (String) cInputData.get(4);
        this.mPlanType = (String) cInputData.get(5);
        this.mPlanSql = (String) cInputData.get(6);
        this.mPeoples3 = (String) cInputData.get(7);
        this.mPeoples2 = (String) cInputData.get(8);
        this.mLCContPlanRiskSet.set((LCContPlanRiskSet) cInputData.
                                    getObjectByObjectName(
                                            "LCContPlanRiskSet", 0));
        this.mLCGrpFeeSet.set((LCGrpFeeSet) cInputData.getObjectByObjectName(
                "LCGrpFeeSet", 0));
        this.tTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获取返回结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}
