package com.sinosoft.lis.tb;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 按原简易险平台要求准备数据，并调用类BriefSingleContInptBL.submitData生成保单。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
import com.sinosoft.lis.brieftb.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class BPOSimCheck
{
    private String mBatchNo = "";

    private String mContID = "";

    public CErrors mErrors = new CErrors();

    private LCNationSet mLCNationSet = new LCNationSet();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LCAppntSchema mLCAppntSchema = new LCAppntSchema();

    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();

    private LCCustomerImpartSchema mLCCustomerImpartSchema = new LCCustomerImpartSchema();

    private LCPolSet mLCPolSet = new LCPolSet();

    private LCBnfSet mLCBnfSet = new LCBnfSet();

    private LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();

    private LCAddressSchema mAppntAddressSchema = new LCAddressSchema();

    private LCAddressSchema mInsuredAddressSchema = new LCAddressSchema();

    private String mGrpName;

    //外包批次状态
    public BPOSimCheck()
    {
    }

    public boolean submitData(String mBatchNo, String mContID,
            GlobalInput mGlobalInput)
    {
        this.mBatchNo = mBatchNo;
        this.mContID = mContID;
        if (!getInputData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        BriefSingleContInputBL tBriefSingleContInputBL = new BriefSingleContInputBL();
        try
        {
            VData tVData = new VData();
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("AppntAddress", mAppntAddressSchema);
            tTransferData.setNameAndValue("InsuredAddress",
                    mInsuredAddressSchema);
            tTransferData.setNameAndValue("WorkName", mGrpName);
            tTransferData.setNameAndValue("MissionProp5", "6");

            tVData.add(mLCContSchema);
            tVData.add(mLCInsuredSchema);
            tVData.add(mLCAppntSchema);
            tVData.add(mLCNationSet);
            tVData.add(mLCPolSet);
            tVData.add(mLCCustomerImpartSchema);
            tVData.add(mGlobalInput);
            tVData.add(tTransferData);
            tVData.add(mLCBnfSet);
            tVData.add(mLCRiskDutyWrapSet);
            String tAction = "INSERT||MAIN";

            MMap tMMap = tBriefSingleContInputBL.getSubmitMap(tVData, tAction);
            if (tMMap == null)
            {
                mErrors.copyAllErrors(tBriefSingleContInputBL.mErrors);
                return false;
            }

            VData data = new VData();
            data.add(tMMap);

            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(data, ""))
            {
                mErrors.addOneError("保存简易保单失败");
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "提交错误";
            tError.functionName = "";
            tError.errorMessage = "tBriefSingleContInputBL提交错误";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    public boolean dealData()
    {
        System.out.println(mBatchNo);
        System.out.println(mContID);

        //插入套餐信息
        BPOLCRiskDutyWrapDB tBPOLCRiskDutyWrapDB = new BPOLCRiskDutyWrapDB();
        tBPOLCRiskDutyWrapDB.setBPOBatchNo(mBatchNo);
        tBPOLCRiskDutyWrapDB.setContID(mContID);
        BPOLCRiskDutyWrapSet tBPOLCRiskDutyWrapSet = new BPOLCRiskDutyWrapSet();
        tBPOLCRiskDutyWrapSet = tBPOLCRiskDutyWrapDB.query();
        if (tBPOLCRiskDutyWrapSet.size() > 0)
        {
            for (int i = 1; i <= tBPOLCRiskDutyWrapSet.size(); i++)
            {
                BPOLCRiskDutyWrapSchema tBPOLCRiskDutyWrapSchema = new BPOLCRiskDutyWrapSchema();
                tBPOLCRiskDutyWrapSchema = tBPOLCRiskDutyWrapSet.get(i);
                LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
                tLCRiskDutyWrapSchema.setRiskWrapCode(tBPOLCRiskDutyWrapSchema
                        .getRiskWrapCode());
                tLCRiskDutyWrapSchema.setCalFactor(tBPOLCRiskDutyWrapSchema
                        .getCalFactor());
                tLCRiskDutyWrapSchema
                        .setCalFactorValue(tBPOLCRiskDutyWrapSchema
                                .getCalFactorValue());
                tLCRiskDutyWrapSchema.setRiskCode(tBPOLCRiskDutyWrapSchema
                        .getRiskCode());
                tLCRiskDutyWrapSchema.setDutyCode(tBPOLCRiskDutyWrapSchema
                        .getDutyCode());
                mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
            }
        }

        //准备国家的信息LCNation
        BPOLCNationDB tBPOLCNationDB = new BPOLCNationDB();
        tBPOLCNationDB.setBPOBatchNo(mBatchNo);
        tBPOLCNationDB.setContID(mContID);

        BPOLCNationSet tBPOLCNationSet = new BPOLCNationSet();
        tBPOLCNationSet = tBPOLCNationDB.query();
        if (tBPOLCNationSet.size() > 0)
        {
            for (int i = 1; i <= tBPOLCNationSet.size(); i++)
            {
                try
                {
                    LCNationSchema tLCNationSchema = new LCNationSchema();
                    tLCNationSchema.setGrpContNo("00000000000000000000");
                    tLCNationSchema.setNationNo(tBPOLCNationSet.get(i)
                            .getNationNo());
                    tLCNationSchema.setContNo("");
                    tLCNationSchema.setChineseName(tBPOLCNationSet.get(i)
                            .getChineseName());
                    mLCNationSet.add(tLCNationSchema);
                }
                catch (Exception ex)
                {
                    System.out.println("处理国家信息LCNation时出现未知异常");
                    mErrors.addOneError("处理国家信息LCNation时出现未知异常");
                    ex.printStackTrace();
                    return false;
                }
            }
        }
        //准备保单的信息LCCont
        try
        {
            mLCContSchema.setGrpContNo("");
            mLCContSchema.setContNo("");

            //准备险种信息lcpol
            BPOLCPolDB tBPOLCPolDB = new BPOLCPolDB();
            tBPOLCPolDB.setBPOBatchNo(mBatchNo);
            tBPOLCPolDB.setContID(mContID);
            BPOLCPolSet tBPOLCPolSet = new BPOLCPolSet();
            tBPOLCPolSet = tBPOLCPolDB.query();
            if (tBPOLCPolSet.size() > 0)
            {
                for (int k = 1; k <= tBPOLCPolSet.size(); k++)
                {
                    BPOLCPolSchema tBPOLCPolSchema = new BPOLCPolSchema();
                    tBPOLCPolSchema = tBPOLCPolSet.get(k);

                    //外包返回的000000险种不需要处理
                    if ("000000".equals(tBPOLCPolSchema.getRiskCode()))
                    {
                        continue;
                    }
                    LCPolSchema tLCPolSchema = new LCPolSchema();
                    tLCPolSchema.setRiskCode(tBPOLCPolSchema.getRiskCode());
                    tLCPolSchema.setMult(tBPOLCPolSchema.getMult());
                    tLCPolSchema.setAmnt(tBPOLCPolSchema.getAmnt());
                    tLCPolSchema.setManageCom(tBPOLCPolSchema.getManageCom());
                    tLCPolSchema.setOperator(tBPOLCPolSchema.getOperator());
                    tLCPolSchema.setSaleChnl(tBPOLCPolSchema.getSaleChnl());
                    tLCPolSchema.setAgentCode(tLCPolSchema.getAgentCode());
                    tLCPolSchema.setPrtNo(tBPOLCPolSchema.getPrtNo());
                    tLCPolSchema.setCopys(tBPOLCPolSchema.getCopys());
                    tLCPolSchema.setPayIntv(tBPOLCPolSchema.getPayIntv());

                    if (tBPOLCPolSchema.getPayIntv() == null
                            || tBPOLCPolSchema.getPayIntv().equals(""))
                    {
                        tLCPolSchema
                                .setPayIntv(getOnePolPayIntv(tBPOLCPolSchema
                                        .getRiskCode()));
                    }

                    mLCPolSet.add(tLCPolSchema);
                }
            }

            //准备保单的信息LCAppnt的信息
            BPOLCAppntSchema tBPOLCAppntSchema = new BPOLCAppntSchema();
            BPOLCAppntDB tBPOLCAppntDB = new BPOLCAppntDB();
            tBPOLCAppntDB.setBPOBatchNo(mBatchNo);
            tBPOLCAppntDB.setContID(mContID);
            BPOLCAppntSet tBPOLCAppntSet = new BPOLCAppntSet();
            tBPOLCAppntSet = tBPOLCAppntDB.query();
            if (tBPOLCAppntSet.size() > 0)
            {
                tBPOLCAppntSchema = tBPOLCAppntSet.get(1);
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "BPOLCPol";
                tError.functionName = "";
                tError.errorMessage = "没有得到lcappnt对应的临时表数据";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            String tContType = "";
            tContType = tBPOLCAppntSchema.getContType();
            tContType = (tContType == "" ? "1" : tContType);
            tBPOLCPolSet = tBPOLCPolDB.query();
            mLCContSchema.setProposalContNo("");
            mLCContSchema.setPrtNo(tBPOLCPolSet.get(1).getPrtNo());
            mLCContSchema.setContType(tContType);
            mLCContSchema.setFamilyType(tBPOLCPolSet.get(1).getFamilyType());
            mLCContSchema.setFamilyID("");
            mLCContSchema.setPolType(tBPOLCAppntSchema.getPolType());
            mLCContSchema.setCardFlag("6");
            mLCContSchema.setManageCom(tBPOLCPolSet.get(1).getManageCom());
            mLCContSchema.setExecuteCom(tBPOLCPolSet.get(1).getManageCom());
            mLCContSchema.setAgentCom(tBPOLCPolSet.get(1).getAgentCom());
            mLCContSchema.setAgentCode(tBPOLCPolSet.get(1).getAgentCode());

            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tBPOLCPolSet.get(1).getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAAgent";
                tError.functionName = "";
                tError.errorMessage = "没有得到LAAgent表数据";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            try
            {
                mLCAppntSchema.setGrpContNo("");
                mLCAppntSchema.setContNo("");
                mLCAppntSchema.setPrtNo(tBPOLCAppntSchema.getPrtNo());
                mLCAppntSchema.setAppntNo(tBPOLCAppntSchema.getCustomerNo());
                mLCAppntSchema.setAppntGrade("");
                mLCAppntSchema.setAddressNo(tBPOLCAppntSchema.getAddressNo());
                mLCAppntSchema.setNativePlace(tBPOLCAppntSchema
                        .getNativePlace());
                mLCAppntSchema.setNationality(tBPOLCAppntSchema
                        .getNationality());
                mLCAppntSchema.setRgtAddress(tBPOLCAppntSchema.getRgtAddress());
                mLCAppntSchema.setMarriage(tBPOLCAppntSchema.getMarriage());
                mLCAppntSchema.setMarriageDate(tBPOLCAppntSchema
                        .getMarriageDate());
                mLCAppntSchema.setHealth(tBPOLCAppntSchema.getHealth());
                mLCAppntSchema.setStature(tBPOLCAppntSchema.getStature());
                mLCAppntSchema.setAvoirdupois(tBPOLCAppntSchema
                        .getAvoirdupois());
                mLCAppntSchema.setDegree(tBPOLCAppntSchema.getDegree());
                mLCAppntSchema.setCreditGrade(tBPOLCAppntSchema
                        .getCreditGrade());
                mLCAppntSchema.setBankCode(tBPOLCAppntSchema.getBankCode());
                mLCAppntSchema.setBankAccNo(tBPOLCAppntSchema.getBankAccNo());
                mLCAppntSchema.setAccName(tBPOLCAppntSchema.getAccName());
                mLCAppntSchema.setJoinCompanyDate(tBPOLCAppntSchema
                        .getJoinCompanyDate());
                mLCAppntSchema.setStartWorkDate(tBPOLCAppntSchema
                        .getStartWorkDate());
                mLCAppntSchema.setPosition(tBPOLCAppntSchema.getPosition());
                mLCAppntSchema.setSalary(tBPOLCAppntSchema.getSalary());
                mLCAppntSchema.setOccupationType(tBPOLCAppntSchema
                        .getOccupationType());
                mLCAppntSchema.setOccupationCode(tBPOLCAppntSchema
                        .getOccupationCode());
                mLCAppntSchema.setWorkType(tBPOLCAppntSchema.getWorkType());
                mLCAppntSchema.setPluralityType(tBPOLCAppntSchema
                        .getPluralityType());
                mLCAppntSchema.setSmokeFlag(tBPOLCAppntSchema.getSmokeFlag());
                mLCAppntSchema.setOperator(tBPOLCAppntSchema.getOperator());
                mLCAppntSchema.setManageCom(tBPOLCPolSet.get(1).getManageCom());
                mLCAppntSchema.setMakeDate(tBPOLCAppntSchema.getMakeDate());
                mLCAppntSchema.setMakeTime(tBPOLCAppntSchema.getMakeTime());
                mLCAppntSchema.setModifyDate(tBPOLCAppntSchema.getModifyDate());
                mLCAppntSchema.setModifyTime(tBPOLCAppntSchema.getModifyTime());
                mLCAppntSchema.setOthIDType(tBPOLCAppntSchema.getOthIDType());
                mLCAppntSchema.setOthIDNo(tBPOLCAppntSchema.getOthIDNo());
                mLCAppntSchema.setEnglishName(tBPOLCAppntSchema
                        .getEnglishName());
                mLCAppntSchema.setAppntName(tBPOLCAppntSchema.getName());
                mLCAppntSchema.setAppntSex(tBPOLCAppntSchema.getSex());
                mLCAppntSchema
                        .setAppntBirthday(tBPOLCAppntSchema.getBirthday());
                mLCAppntSchema.setIDType(tBPOLCAppntSchema.getIDType());
                mLCAppntSchema.setIDNo(tBPOLCAppntSchema.getIDNo());

                mAppntAddressSchema.setCustomerNo(tBPOLCAppntSchema
                        .getCustomerNo());
                mAppntAddressSchema.setAddressNo(tBPOLCAppntSchema
                        .getAddressNo());
                mAppntAddressSchema.setPostalAddress(tBPOLCAppntSchema
                        .getPostalAddress());
                mAppntAddressSchema.setZipCode(tBPOLCAppntSchema.getZipCode());
                mAppntAddressSchema.setPhone(tBPOLCAppntSchema.getPhone());
                mAppntAddressSchema.setHomePhone(tBPOLCAppntSchema
                        .getHomePhone());
                mAppntAddressSchema.setMobile(tBPOLCAppntSchema.getMobile());
                mAppntAddressSchema.setCompanyPhone(tBPOLCAppntSchema
                        .getCompanyPhone());
                mAppntAddressSchema.setEMail(tBPOLCAppntSchema.getEMail());
                mGrpName = tBPOLCAppntSchema.getGrpName();
                //增加身份证号信息校验
//                String tFlag = PubFun.CheckIDNo(tBPOLCAppntSchema.getIDType(), tBPOLCAppntSchema.getIDNo(), tBPOLCAppntSchema.getBirthday(), tBPOLCAppntSchema.getSex());
//                if(!tFlag.equals("")){
//                	CError tError = new CError();
//                    tError.moduleName = "BPOLCImpart";
//                    tError.functionName = "dealData()";
//                    tError.errorMessage = tFlag;
//                    mErrors.addOneError(tError);
//                    System.out.println(tError.errorMessage);
//                    return false;
//                }
            }
            catch (Exception ex)
            {
                System.out.println("处理投保人时出现未知异常");
                mErrors.addOneError("处理投保人时出现未知异常");
                ex.printStackTrace();
                return false;
            }

            //准备被保人信息lcinsured
            BPOLCInsuredDB tBPOLCInsuredDB = new BPOLCInsuredDB();
            tBPOLCInsuredDB.setContID(mContID);
            tBPOLCInsuredDB.setBPOBatchNo(mBatchNo);
            BPOLCInsuredSet tBPOLCInsuredSet = new BPOLCInsuredSet();
            tBPOLCInsuredSet = tBPOLCInsuredDB.query();
            if (tBPOLCInsuredSet.size() > 0)
            {
                BPOLCInsuredSchema tBPOLCInsuredSchema = new BPOLCInsuredSchema();
                tBPOLCInsuredSchema = tBPOLCInsuredSet.get(1);
                try
                {
                    mLCInsuredSchema.setGrpContNo("");
                    mLCInsuredSchema.setContNo("");
                    mLCInsuredSchema.setInsuredNo(tBPOLCInsuredSchema
                            .getCustomerNo());
                    mLCInsuredSchema.setPrtNo(tBPOLCInsuredSchema.getPrtNo());
                    mLCInsuredSchema.setAppntNo(mLCAppntSchema.getAppntNo());
                    mLCInsuredSchema.setManageCom(tBPOLCPolSet.get(1)
                            .getManageCom());
                    mLCInsuredSchema.setExecuteCom(tBPOLCPolSet.get(1)
                            .getManageCom());
                    mLCInsuredSchema.setFamilyID(tBPOLCPolSet.get(1)
                            .getFamilyType());
                    mLCInsuredSchema.setRelationToAppnt(tBPOLCPolSet.get(1)
                            .getRelationToAppnt());
                    mLCInsuredSchema.setRelationToMainInsured(tBPOLCPolSet.get(
                            1).getRelationToMainInsured());
                    mLCInsuredSchema.setAddressNo(tBPOLCInsuredSchema
                            .getAddressNo());
                    mLCInsuredSchema.setSequenceNo("1");
                    mLCInsuredSchema.setNativePlace(tBPOLCInsuredSchema
                            .getNativePlace());
                    mLCInsuredSchema.setNationality(tBPOLCInsuredSchema
                            .getNationality());
                    mLCInsuredSchema.setRgtAddress(tBPOLCInsuredSchema
                            .getRgtAddress());
                    mLCInsuredSchema.setMarriage(tBPOLCInsuredSchema
                            .getMarriage());
                    mLCInsuredSchema.setMarriageDate(tBPOLCInsuredSchema
                            .getMarriageDate());
                    mLCInsuredSchema.setHealth(tBPOLCInsuredSchema.getHealth());
                    mLCInsuredSchema.setStature(tBPOLCInsuredSchema
                            .getStature());
                    mLCInsuredSchema.setAvoirdupois(tBPOLCInsuredSchema
                            .getAvoirdupois());
                    mLCInsuredSchema.setDegree(tBPOLCInsuredSchema.getDegree());
                    mLCInsuredSchema.setCreditGrade(tBPOLCInsuredSchema
                            .getCreditGrade());
                    mLCInsuredSchema.setBankCode("");
                    mLCInsuredSchema.setBankAccNo("");
                    mLCInsuredSchema.setAccName("");
                    mLCInsuredSchema.setJoinCompanyDate(tBPOLCInsuredSchema
                            .getJoinCompanyDate());
                    mLCInsuredSchema.setStartWorkDate(tBPOLCInsuredSchema
                            .getStartWorkDate());
                    mLCInsuredSchema.setPosition(tBPOLCInsuredSchema
                            .getPosition());
                    mLCInsuredSchema.setSalary(tBPOLCInsuredSchema.getSalary());
                    mLCInsuredSchema.setOccupationType(tBPOLCInsuredSchema
                            .getOccupationType());
                    mLCInsuredSchema.setOccupationCode(tBPOLCInsuredSchema
                            .getOccupationCode());
                    mLCInsuredSchema.setWorkType(tBPOLCInsuredSchema
                            .getWorkType());
                    mLCInsuredSchema.setPluralityType(tBPOLCInsuredSchema
                            .getPluralityType());
                    mLCInsuredSchema.setSmokeFlag(tBPOLCInsuredSchema
                            .getSmokeFlag());
                    mLCInsuredSchema.setContPlanCode("");
                    mLCInsuredSchema.setOperator(tBPOLCInsuredSchema
                            .getOperator());
                    mLCInsuredSchema.setInsuredStat(tBPOLCInsuredSchema
                            .getInsuredStat());
                    mLCInsuredSchema.setMakeDate(tBPOLCInsuredSchema
                            .getMakeDate());
                    mLCInsuredSchema.setMakeTime(tBPOLCInsuredSchema
                            .getMakeTime());
                    mLCInsuredSchema.setModifyDate(tBPOLCInsuredSchema
                            .getModifyDate());
                    mLCInsuredSchema.setModifyTime(tBPOLCInsuredSchema
                            .getModifyTime());
                    mLCInsuredSchema.setUWFlag("");
                    mLCInsuredSchema.setUWCode("");
                    mLCInsuredSchema.setUWDate("");
                    mLCInsuredSchema.setUWTime("");
                    mLCInsuredSchema.setBMI(tBPOLCInsuredSchema.getBMI());
                    mLCInsuredSchema.setInsuredPeoples("1");
                    mLCInsuredSchema.setContPlanCount("");
                    mLCInsuredSchema.setDiskImportNo("");
                    mLCInsuredSchema.setGrpInsuredPhone("");
                    mLCInsuredSchema.setName(tBPOLCInsuredSchema.getName());
                    mLCInsuredSchema.setEnglishName(tBPOLCInsuredSchema
                            .getEnglishName());
                    mLCInsuredSchema.setSex(tBPOLCInsuredSchema.getSex());
                    mLCInsuredSchema.setBirthday(tBPOLCInsuredSchema
                            .getBirthday());
                    mLCInsuredSchema.setIDType(tBPOLCInsuredSchema.getIDType());
                    mLCInsuredSchema.setIDNo(tBPOLCInsuredSchema.getIDNo());
                    mLCInsuredSchema.setOthIDNo(tBPOLCInsuredSchema
                            .getOthIDNo());
                    mLCInsuredSchema.setOthIDType(tBPOLCInsuredSchema
                            .getOccupationType());
                  //增加身份证号信息校验
//                    String tFlag = PubFun.CheckIDNo(tBPOLCInsuredSchema.getIDType(), tBPOLCInsuredSchema.getIDNo(), tBPOLCInsuredSchema.getBirthday(), tBPOLCInsuredSchema.getSex());
//                    if(!tFlag.equals("")){
//                    	CError tError = new CError();
//                        tError.moduleName = "BPOLCImpart";
//                        tError.functionName = "dealData()";
//                        tError.errorMessage = tFlag;
//                        mErrors.addOneError(tError);
//                        System.out.println(tError.errorMessage);
//                        return false;
//                    }
                    System.out.println("处理完成被保人信息！");

                    mInsuredAddressSchema.setCustomerNo(tBPOLCInsuredSchema
                            .getCustomerNo());
                    mInsuredAddressSchema.setAddressNo(tBPOLCInsuredSchema
                            .getAddressNo());
                    mInsuredAddressSchema.setPostalAddress(tBPOLCInsuredSchema
                            .getPostalAddress());
                    mInsuredAddressSchema.setZipCode(tBPOLCInsuredSchema
                            .getZipCode());
                    mInsuredAddressSchema.setPhone(tBPOLCInsuredSchema
                            .getPhone());
                    mInsuredAddressSchema.setHomePhone(tBPOLCInsuredSchema
                            .getHomePhone());
                    mInsuredAddressSchema.setMobile(tBPOLCInsuredSchema
                            .getMobile());
                    mInsuredAddressSchema.setCompanyPhone(tBPOLCInsuredSchema
                            .getCompanyPhone());
                    mInsuredAddressSchema.setEMail(tBPOLCInsuredSchema
                            .getEMail());
                }
                catch (Exception ex)
                {
                    System.out.println("处理被保人时出现未知异常");
                    mErrors.addOneError("处理被保人时出现未知异常");
                    ex.printStackTrace();
                    return false;
                }
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "BPOLCPol";
                tError.functionName = "";
                tError.errorMessage = "没有得到lcappnt对应的临时表数据";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            mLCContSchema.setAgentGroup(tLAAgentDB.getAgentGroup());
            mLCContSchema.setAgentCode1("");
            mLCContSchema.setAgentType("");
            mLCContSchema.setSaleChnl(tBPOLCPolSet.get(1).getSaleChnl());
            mLCContSchema.setHandler("");
            mLCContSchema.setPassword(tBPOLCAppntSchema.getPassword());
            mLCContSchema.setAppntNo(tBPOLCAppntSchema.getCustomerNo());
            mLCContSchema.setAppntName(tBPOLCAppntSchema.getName());
            mLCContSchema.setAppntSex(tBPOLCAppntSchema.getSex());
            mLCContSchema.setAppntBirthday(tBPOLCAppntSchema.getBirthday());
            mLCContSchema.setAppntIDType(tBPOLCAppntSchema.getIDType());
            mLCContSchema.setAppntIDNo(tBPOLCAppntSchema.getIDNo());
            mLCContSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());
            mLCContSchema.setInsuredName(mLCInsuredSchema.getName());
            mLCContSchema.setInsuredSex(mLCInsuredSchema.getSex());
            mLCContSchema.setInsuredBirthday(mLCInsuredSchema.getBirthday());
            mLCContSchema.setInsuredIDType(mLCInsuredSchema.getIDType());
            mLCContSchema.setInsuredIDNo(mLCInsuredSchema.getIDNo());
            mLCContSchema.setPayIntv(tBPOLCPolSet.get(1).getPayIntv());
            if (tBPOLCPolSet.get(1).getPayIntv() == null
                    || tBPOLCPolSet.get(1).getPayIntv().equals(""))
            {
                mLCContSchema.setPayIntv(getContPayIntv());
            }
            mLCContSchema.setPayMode(tBPOLCPolSet.get(1).getPayMode());
            mLCContSchema.setPayLocation("");
            mLCContSchema.setDisputedFlag("");
            mLCContSchema.setOutPayFlag(tBPOLCPolSet.get(1).getOutPayFlag());
            mLCContSchema.setGetPolMode(tBPOLCAppntSchema.getGetPolMode());
            mLCContSchema.setSignCom(tBPOLCPolSet.get(1).getManageCom());
            mLCContSchema.setSignDate("");
            mLCContSchema.setSignTime("");
            mLCContSchema.setConsignNo("");
            mLCContSchema.setBankCode(mLCAppntSchema.getBankCode());
            mLCContSchema.setBankAccNo(mLCAppntSchema.getBankAccNo());
            mLCContSchema.setAccName(mLCAppntSchema.getAccName());
            mLCContSchema.setPrintCount(0);
            mLCContSchema.setLostTimes("");
            mLCContSchema.setLang(tBPOLCAppntSchema.getLang());
            mLCContSchema.setCurrency(tBPOLCAppntSchema.getCurrency());
            mLCContSchema.setRemark(tBPOLCAppntSchema.getRemark() == null ? ""
                    : tBPOLCAppntSchema.getRemark());
            mLCContSchema.setPeoples(tBPOLCAppntSchema.getPeoples());
            mLCContSchema.setMult(tBPOLCPolSet.get(1).getMult());
            mLCContSchema.setPrem(0);
            mLCContSchema.setAmnt(0);
            mLCContSchema.setSumPrem(0);
            mLCContSchema.setDif("");
            mLCContSchema.setPaytoDate("");
            mLCContSchema.setFirstPayDate("");
            mLCContSchema.setCValiDate(tBPOLCPolSet.get(1).getCValiDate());
            mLCContSchema.setInputOperator("");
            mLCContSchema.setInputDate("");
            mLCContSchema.setInputTime("");
            mLCContSchema.setApproveFlag("");
            mLCContSchema.setApproveCode("");
            mLCContSchema.setApproveDate("");
            mLCContSchema.setApproveTime("");
            mLCContSchema.setUWFlag("");
            mLCContSchema.setUWOperator("");
            mLCContSchema.setUWDate("");
            mLCContSchema.setUWTime("");
            mLCContSchema.setAppFlag("");
            mLCContSchema.setPolApplyDate("");
            mLCContSchema.setGetPolDate("");
            mLCContSchema.setGetPolTime("");
            mLCContSchema.setCustomGetPolDate("");
            mLCContSchema.setState("");
            mLCContSchema.setOperator(tBPOLCAppntSchema.getOperator());
            mLCContSchema.setMakeDate(tBPOLCAppntSchema.getMakeDate());
            mLCContSchema.setMakeTime(tBPOLCAppntSchema.getMakeTime());
            mLCContSchema.setModifyDate(tBPOLCAppntSchema.getModifyDate());
            mLCContSchema.setModifyTime(tBPOLCAppntSchema.getModifyTime());
            mLCContSchema.setFirstTrialOperator(tBPOLCPolSet.get(1)
                    .getFirstTrialOperator());
            mLCContSchema.setFirstTrialDate(tBPOLCPolSet.get(1)
                    .getFirstTrialDate());
            mLCContSchema.setFirstTrialTime(tBPOLCPolSet.get(1)
                    .getFirstTrialTime());
            mLCContSchema.setReceiveOperator(tBPOLCPolSet.get(1)
                    .getReceiveOperator());
            mLCContSchema.setReceiveDate(tBPOLCPolSet.get(1).getReceiveDate());
            mLCContSchema.setReceiveTime(tBPOLCPolSet.get(1).getReceiveTime());
            mLCContSchema.setTempFeeNo(tBPOLCAppntSchema.getTempFeeNo());
            mLCContSchema.setProposalType("");
            mLCContSchema.setSaleChnlDetail("");
            mLCContSchema.setContPrintLoFlag("");
            mLCContSchema.setContPremFeeNo("");
            mLCContSchema.setCustomerReceiptNo("");
            mLCContSchema.setCInValiDate("");
            mLCContSchema.setCopys(tBPOLCPolSet.get(1).getCopys());
            mLCContSchema.setDegreeType(tBPOLCAppntSchema.getDegreeType());
        }
        catch (Exception ex)
        {
            System.out.println("处理合同信息时出现未知异常");
            mErrors.addOneError("处理合同信息时出现未知异常");
            ex.printStackTrace();
            return false;
        }
        //插入受益人信息
        BPOLCBnfDB tBPOLCBnfDB = new BPOLCBnfDB();
        tBPOLCBnfDB.setBPOBatchNo(mBatchNo);
        tBPOLCBnfDB.setContID(mContID);
        BPOLCBnfSet tBPOLCBnfSet = new BPOLCBnfSet();
        tBPOLCBnfSet = tBPOLCBnfDB.query();
        if (tBPOLCBnfSet.size() > 0)
        {
            for (int i = 1; i <= tBPOLCBnfSet.size(); i++)
            {
                LCBnfSchema xLCBnfSchema = new LCBnfSchema();
                BPOLCBnfSchema tBPOLCBnfSchema = new BPOLCBnfSchema();
                tBPOLCBnfSchema = tBPOLCBnfSet.get(i);
                xLCBnfSchema.setBnfType(tBPOLCBnfSchema.getBnfType());
                xLCBnfSchema.setName(tBPOLCBnfSchema.getName());
                xLCBnfSchema.setSex(tBPOLCBnfSchema.getSex());
                xLCBnfSchema.setIDType(tBPOLCBnfSchema.getIDType());
                xLCBnfSchema.setIDNo(tBPOLCBnfSchema.getIDNo());
              //增加身份证号信息校验
//                String tFlag = PubFun.CheckIDNo(tBPOLCBnfSchema.getIDType(), tBPOLCBnfSchema.getIDNo(), tBPOLCBnfSchema.getBirthday(), tBPOLCBnfSchema.getSex());
//                if(!tFlag.equals("")){
//                	CError tError = new CError();
//                    tError.moduleName = "BPOLCImpart";
//                    tError.functionName = "dealData()";
//                    tError.errorMessage = tFlag;
//                    mErrors.addOneError(tError);
//                    System.out.println(tError.errorMessage);
//                    return false;
//                }
                xLCBnfSchema.setRelationToInsured(tBPOLCBnfSchema
                        .getRelationToInsured());
                xLCBnfSchema.setBnfLot(tBPOLCBnfSchema.getBnfLot());
                xLCBnfSchema.setBnfGrade(tBPOLCBnfSchema.getBnfGrade());
                mLCBnfSet.add(xLCBnfSchema);
            }
        }

        //插入客户告知表LCCustomerImpart
        BPOLCImpartSchema tBPOLCImpartSchema = new BPOLCImpartSchema();
        BPOLCImpartDB tBPOLCImpartDB = new BPOLCImpartDB();
        tBPOLCImpartDB.setBPOBatchNo(mBatchNo);
        tBPOLCImpartDB.setContID(mContID);
        BPOLCImpartSet tBPOLCImpartSet = new BPOLCImpartSet();
        tBPOLCImpartSet = tBPOLCImpartDB.query();
        if (tBPOLCImpartDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOLCImpart";
            tError.functionName = "";
            tError.errorMessage = "查询临时受益人信息时出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        for (int i = 1; i < tBPOLCImpartSet.size(); i++)
        {
            //暂不处理
        }

        mLCCustomerImpartSchema.setGrpContNo("00000000000000000000");
        mLCCustomerImpartSchema.setProposalContNo("");
        mLCCustomerImpartSchema.setContNo("");
        mLCCustomerImpartSchema.setPrtNo(mLCContSchema.getPrtNo());
        mLCCustomerImpartSchema.setCustomerNo(mLCInsuredSchema.getInsuredNo());
        mLCCustomerImpartSchema.setCustomerNoType("I");
        mLCCustomerImpartSchema.setImpartCode("001");
        mLCCustomerImpartSchema.setImpartContent("□商务　□旅行　□其它");
        mLCCustomerImpartSchema.setImpartParamModle("");
        mLCCustomerImpartSchema.setImpartVer("013");

        return true;
    }

    /**
     * getContPayIntv
     * 得到当前保单的缴费频次:套餐的最小频次
     * @return int
     */
    private String getContPayIntv()
    {
        int tPayIntv = Integer.MIN_VALUE;
        for (int i = 1; i <= mLCRiskDutyWrapSet.size(); i++)
        {
            if (!"PayIntv".equals(mLCRiskDutyWrapSet.get(i).getCalFactor()))
            {
                continue;
            }

            int tWrapIntv = Integer.parseInt(mLCRiskDutyWrapSet.get(i)
                    .getCalFactorValue());
            if (tPayIntv == Integer.MIN_VALUE)
            {
                tPayIntv = tWrapIntv;
            }
            else if (tWrapIntv == 0)
            {
                continue;
            }
            else if (tPayIntv == 0)
            {
                tPayIntv = tWrapIntv;
            }
            else if (tWrapIntv < tPayIntv)
            {
                tPayIntv = tWrapIntv;
            }
        }

        return tPayIntv == Integer.MIN_VALUE ? "" : "" + tPayIntv;
    }

    /**
     * getOnePolPayIntv
     * 若套餐不录入RiskCode，则返回aRiskCode所在套餐的第一个PayIntv
     * @return int
     */
    private String getOnePolPayIntv(String aRiskCode)
    {
        for (int i = 1; i <= mLCRiskDutyWrapSet.size(); i++)
        {
            if (!"PayIntv".equals(mLCRiskDutyWrapSet.get(i).getCalFactor()))
            {
                continue;
            }

            //若套餐录入RiskCode，则应返回本mLCRiskDutyWrapSet匹配的PayIntv
            if (mLCRiskDutyWrapSet.get(i).getRiskCode() != null
                    && !mLCRiskDutyWrapSet.get(i).getRiskCode()
                            .equals("000000"))
            {
                if (mLCRiskDutyWrapSet.get(i).getRiskCode().equals(aRiskCode))
                {
                    return mLCRiskDutyWrapSet.get(i).getRiskCode();
                }
                continue;
            }

            //若套餐不录入RiskCode，且套餐编码对应的套餐内包括了aRiskCode，则返回本循环的PayIntv
            LDRiskWrapDB db = new LDRiskWrapDB();
            db.setRiskWrapCode(mLCRiskDutyWrapSet.get(i).getRiskWrapCode());
            LDRiskWrapSet set = db.query();
            for (int j = 1; j <= set.size(); j++)
            {
                if (set.get(j).equals(aRiskCode))
                {
                    return mLCRiskDutyWrapSet.get(i).getCalFactorValue();
                }
            }
        }

        return "";
    }

    public boolean getInputData()
    {
        if (mBatchNo == null || mBatchNo.trim().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "BPOSimCheck";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到数据mBatchNo";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        if (mContID == null || mContID.trim().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "BPOSimCheck";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到数据mContID";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        BPOSimCheck bpo = new BPOSimCheck();
    }
}
