package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.f1print.*;
import java.text.*;
import java.lang.*;
import java.sql.*;

/**
 * <p>Title: </p>
 * <p>Description: 团体分红</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class GrpAssignBonus {

  private String mOperate;
  private String mIsueManageCom;
  private String mManageCom;
  private int COUNT = 100;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  public CErrors mErrors = new CErrors();

  /*转换精确位数的对象   */
  private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
  private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

  public GrpAssignBonus() {
  }

  public static void main(String[] args) {
    GrpAssignBonus grpAssignBonus1 = new GrpAssignBonus();
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.ManageCom = "8611";
    tGlobalInput.Operator = "001";
    tGlobalInput.ComCode = "8611";
    LOBonusGrpPolParmSet tLOBonusGrpPolParmSet = new LOBonusGrpPolParmSet();
    LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = new
        LOBonusGrpPolParmSchema();
    tLOBonusGrpPolParmSchema.setGrpPolNo("86110020030220000054");
    tLOBonusGrpPolParmSchema.setFiscalYear(2003);
    tLOBonusGrpPolParmSet.add(tLOBonusGrpPolParmSchema);

    VData cInputData = new VData();
    cInputData.add(tGlobalInput);
    cInputData.add(tLOBonusGrpPolParmSet);

    grpAssignBonus1.runCalBonus(cInputData);

//        grpAssignBonus1.runAssignBonus(cInputData);
  }

  /**
   * 自动大批量算团体红利
   * @param cInputData
   * @return
   */
  public boolean runCalBonusAuto(VData cInputData) {
    GlobalInput tGlobalInput = ( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    if (tGlobalInput == null) {
      mOperate = "001";
      mManageCom = "86";
    }
    else {
      mOperate = tGlobalInput.Operator;
      mManageCom = tGlobalInput.ManageCom;
    }

    String tLimit = PubFun.getNoLimit(mManageCom);
    String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //产生流水号码
    String curYear = StrTool.getYear(); //当前年
    int tFiscalYear = Integer.valueOf(StrTool.getYear()).intValue() - 1; //上一年度即会计年度
    //设置批量查询条件
    //for循环即可
    return true;
  }

  /**
   * 前台传递团体保单号，小批量处理
   * @param cInputData
   * @return
   */
  public boolean runCalBonus(VData cInputData) {
    GlobalInput tGlobalInput = ( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    LOBonusGrpPolParmSet tLOBonusGrpPolParmSet = ( (LOBonusGrpPolParmSet)
                                                  cInputData.
                                                  getObjectByObjectName(
        "LOBonusGrpPolParmSet", 0));

    if (tGlobalInput == null || tLOBonusGrpPolParmSet == null) {
      CError.buildErr(this, "没有传入数据!");
      return false;
    }

    mOperate = tGlobalInput.Operator;
    mManageCom = tGlobalInput.ManageCom;
    String tLimit = PubFun.getNoLimit(mManageCom);
    String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //产生流水号码
    //String curYear=StrTool.getYear();//当前年
    //int tFiscalYear=Integer.valueOf(StrTool.getYear()).intValue()-1;//上一年度即会计年度
    boolean errFlag = false;
    for (int i = 1; i <= tLOBonusGrpPolParmSet.size(); i++) {
      LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = tLOBonusGrpPolParmSet.
          get(i);
      mErrors.clearErrors();
      String tGrpPolNo = tLOBonusGrpPolParmSchema.getGrpPolNo();
      if (CalOneGrpPol(tGrpPolNo, tLOBonusGrpPolParmSchema.getFiscalYear()) == false) {
        insertErrLog(tSerialNo, tGrpPolNo, mErrors.getFirstError(),
                     tLOBonusGrpPolParmSchema.getFiscalYear(), "1");
        errFlag = true;
      }
    }
    if (errFlag)
      return false;

    return true;
  }

  /**
   * 处理单个团体保单红利
   * @param tGrpPolNo
   * @return
   */
  private boolean CalOneGrpPol(String tGrpPolNo, int tFiscalYear) {
    //查询团体保单表
    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    tLCGrpPolDB.setGrpPolNo(tGrpPolNo);
    if (tLCGrpPolDB.getInfo() == false) {
      CError.buildErr(this, "没有查询到团单:" + tGrpPolNo);
      return false;
    }
    LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolDB.getSchema();
    //查询团体红利参数表
    LOBonusGrpPolParmDB tLOBonusGrpPolParmDB = new LOBonusGrpPolParmDB();
    tLOBonusGrpPolParmDB.setFiscalYear(tFiscalYear);
    tLOBonusGrpPolParmDB.setGrpPolNo(tGrpPolNo);
    if (tLOBonusGrpPolParmDB.getInfo() == false) {
      CError.buildErr(this,
                      "没有查询到团单:" + tGrpPolNo + "对应在团体红利参数表中会计年度为" + tFiscalYear +
                      "的数据");
      return false;
    }
    LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = tLOBonusGrpPolParmDB.
        getSchema();
    //如果该团单不处于待计算-返回
    if (!tLOBonusGrpPolParmSchema.getComputeState().equals("0")) {
      return true;
    }

    if (tLOBonusGrpPolParmSchema.getEnsuRate() == 0 &&
        tLOBonusGrpPolParmSchema.getEnsuRateDefault() == 0) {
      CError.buildErr(this, "团单:" + tGrpPolNo + "在团体红利参数表中保证收益和默认保证收益不能都为0");
      return false;
    }
    //判断应计算日期是否小于等于当天
    //计算保单年度值--需要校验:即不满一年的需要加 1 --润年问题
    int PolYear = PubFun.calInterval(tLCGrpPolSchema.getCValiDate(),
                                     PubFun.getCurrentDate(), "Y");
    //计算实际红利应该分配日期--校验
    FDate tD = new FDate();
    String strSGetDate = PubFun.calDate(tLCGrpPolSchema.getCValiDate(), PolYear,
                                        "Y", null);

    int interval = PubFun.calInterval(strSGetDate, PubFun.getCurrentDate(), "D");
    if (interval < 0) {
      CError.buildErr(this, "团单:" + tGrpPolNo + "未到应计算红利日期");
      return false;
    }

    ExeSQL tExeSQL = new ExeSQL();
    String strSQL = "select count(*) from LCInsureAcc where  GrpPolNo='" +
        tGrpPolNo + "' and otherno=polno"
        + " and InsuAccNo in ('000002','000003') ";
    SSRS tSSRS = tExeSQL.execSQL(strSQL);
    String strCount = tSSRS.GetText(1, 1);
    int SumCount = Integer.parseInt(strCount);
    if (SumCount == 0) {
      return true;
    }
    TransferData tTransferData = new TransferData();
    int Num = 1;
    strSQL =
        "select PolNo,InsuAccNo,InsuAccBala from LCInsureAcc where  GrpPolNo='" +
        tGrpPolNo + "' and otherno=polno"
        + " and InsuAccNo in ('000002','000003') order by polno";
    while (Num <= SumCount) {
      tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(strSQL, Num, COUNT);
      for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        //查讯团体分红表是否已经有记录，有则表明处理过了,跳过
        LOBonusGrpPolDB tLOBonusGrpPolDB = new LOBonusGrpPolDB();
        tLOBonusGrpPolDB.setPolNo(tSSRS.GetText(i, 1));
        tLOBonusGrpPolDB.setGrpPolNo(tGrpPolNo);
        tLOBonusGrpPolDB.setInsuAccNo(tSSRS.GetText(i, 2));
        tLOBonusGrpPolDB.setFiscalYear(tFiscalYear);
        if (tLOBonusGrpPolDB.getInfo() == true)
          continue;
        tTransferData = new TransferData();
        tTransferData.setNameAndValue("PolNo", tSSRS.GetText(i, 1));
        tTransferData.setNameAndValue("InsuAccNo", tSSRS.GetText(i, 2));
        tTransferData.setNameAndValue("SGetDate", strSGetDate);
        tTransferData.setNameAndValue("LOBonusGrpPolParm",
                                      tLOBonusGrpPolParmSchema);
        tTransferData.setNameAndValue("FiscalYear", tFiscalYear);
        tTransferData.setNameAndValue("PolYear", PolYear);
        tTransferData.setNameAndValue("InsuAccBala", tSSRS.GetText(i, 3));
        tTransferData.setNameAndValue("LCGrpPolSchema", tLCGrpPolSchema);

        if (CalOnePol(tTransferData) == false)
          return false;
      }
      Num = Num + COUNT;
    }

    //求红利总和
    tExeSQL = new ExeSQL();
    strSQL = "select sum(BonusMoney) from LOBonusGrpPol where GrpPolNo='" +
        tGrpPolNo + "' and FiscalYear=" + tFiscalYear;
    tSSRS = tExeSQL.execSQL(strSQL);
    String SumBonus = tSSRS.GetText(1, 1);
    tLOBonusGrpPolParmSchema.setSumBonus(SumBonus);
    //更新LOBonusGrpPolParm的ComputeState计算状态
    tLOBonusGrpPolParmSchema.setComputeState("1");
    tLOBonusGrpPolParmSchema.setModifyDate(PubFun.getCurrentDate());
    tLOBonusGrpPolParmSchema.setModifyTime(PubFun.getCurrentTime());
    tLOBonusGrpPolParmSchema.setSGetDate(strSGetDate);
    tLOBonusGrpPolParmSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
    //提交数据库
    if (CalGrpsubmitDB(tLOBonusGrpPolParmSchema, tLCGrpPolSchema) == false)
      return false;

    return true;
  }

  /**
   * 处理单个保单帐户
   * @return
   */
  private boolean CalOnePol(TransferData tTransferData) {
    try {
      String PolNo = (String) tTransferData.getValueByName("PolNo");
      String InsuAccNo = (String) tTransferData.getValueByName("InsuAccNo");
      String SGetDate = (String) tTransferData.getValueByName("SGetDate");
      LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = (
          LOBonusGrpPolParmSchema) tTransferData.getValueByName(
          "LOBonusGrpPolParm");
      int FiscalYear = ( (Integer) tTransferData.getValueByName("FiscalYear")).
          intValue();
      int PolYear = ( (Integer) tTransferData.getValueByName("PolYear")).
          intValue();
      String StrInsuAccBala = (String) tTransferData.getValueByName(
          "InsuAccBala");

      LCPolDB tLCPolDB = new LCPolDB();
      tLCPolDB.setPolNo(PolNo);
      if (tLCPolDB.getInfo() == false) {
        CError.buildErr(this,
                        "保单查询数据失败:" + PolNo + tLCPolDB.mErrors.getFirstError());
        return false;
      }
      LCPolSchema tLCPolSchema = tLCPolDB.getSchema();

      //计算保单上周年日
      String LastPolDate = PubFun.calDate(SGetDate, -1, "Y", null);
      //计算保单上周年日到该计算红利这天的天数
      double TDays = 365;
      //double TDays=PubFun.calInterval(LastPolDate,SGetDate,"D");

      //求从保单上周年日至今的帐户资金调整总和
      ExeSQL tExeSQL = new ExeSQL();
      String strSQL =
          "select sum(Money) from LCInsureAccTrace where InsuAccNo='" +
          InsuAccNo + "'"
          + " and PolNo='" + PolNo + "' and PayDate>='" + LastPolDate +
          "' and PayDate<='" + PubFun.getCurrentDate() + "'";
      SSRS tSSRS = tExeSQL.execSQL(strSQL);
      String strSumMoney = tSSRS.GetText(1, 1);
      double SumMoney = Double.parseDouble(strSumMoney); //保单上周年日至今的帐户资金调整总和
      double InsuAccBala = Double.parseDouble(StrInsuAccBala); //账户现在的余额
      double FoundMoney = InsuAccBala - SumMoney; //上周年日的帐户资金余额

      //保单上周年日到本周年日间的资金轨迹-对每条轨迹计算
      tExeSQL = new ExeSQL();
      strSQL = "select Money,PayDate from LCInsureAccTrace where InsuAccNo='" +
          InsuAccNo + "'"
          + " and PolNo='" + PolNo + "' and PayDate>='" + LastPolDate +
          "' and PayDate<'" + SGetDate + "'";
      tSSRS = tExeSQL.execSQL(strSQL);
      double sumCFMoneyIa = 0.0;
      double sumCFMoneyIg = 0.0;

      double ActuRate = tLOBonusGrpPolParmSchema.getActuRate();
      double EnsuRate = tLOBonusGrpPolParmSchema.getEnsuRate();
      double EnsuRateDefault = tLOBonusGrpPolParmSchema.getEnsuRateDefault();
      double AssignRate = tLOBonusGrpPolParmSchema.getAssignRate();
      if (EnsuRate == 0)
        EnsuRate = EnsuRateDefault;

      for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        String strMoney = tSSRS.GetText(i, 1);
        double CFMoney = Double.parseDouble(strMoney);
        String strPayDate = tSSRS.GetText(i, 2);
        //计算入机时间到保单上周年日的天数
        double intvDays = PubFun.calInterval(LastPolDate, strPayDate, "D");
        double powIa = Math.pow( (1 + ActuRate), ( (TDays - intvDays) / TDays));
        double tempResultIa = CFMoney * powIa;
        sumCFMoneyIa = sumCFMoneyIa + tempResultIa; //实际收益
        double powIg = Math.pow( (1 + EnsuRate), ( (TDays - intvDays) / TDays));
        double tempResultIg = CFMoney * powIg;
        sumCFMoneyIg = sumCFMoneyIg + tempResultIg; //保证收益
      }

      //查询－会计年度末的应派发的利息是否已经派发－如果是则上面已经计算红利，下面跳过
      boolean calInter = false;
      LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
      LCInsureAccTraceSchema tLCInsureAccTraceSchema = new
          LCInsureAccTraceSchema();
      LCInsureAccTraceSet qLCInsureAccTraceSet = new LCInsureAccTraceSet();
      LCInsureAccTraceDB qLCInsureAccTraceDB = new LCInsureAccTraceDB();
      qLCInsureAccTraceDB.setPolNo(PolNo);
      qLCInsureAccTraceDB.setInsuAccNo(InsuAccNo);
      qLCInsureAccTraceDB.setMoneyType("LX");
      //派发利息日期
      String payInterest = String.valueOf(FiscalYear) + "-12-31";
      qLCInsureAccTraceDB.setPayDate(payInterest);
      qLCInsureAccTraceSet = qLCInsureAccTraceDB.query();
      if (qLCInsureAccTraceSet.size() > 0) {
        //已经派发，不用计算
        calInter = false;
      }
      else {
        calInter = true;
        //将会计年度末的应派发的利息计算红利
        TransferData IntData = CalPolInterest(tLCPolSchema, tTransferData);
        if (IntData == null) {
          CError.buildErr(this, "处理单个保单帐户:计算会计年度末利息失败!" + mErrors.getFirstError());
          return false;
        }
        else {
          tLCInsureAccSchema = (LCInsureAccSchema) IntData.getValueByName(
              "UPLCInsureAcc");
          tLCInsureAccTraceSchema = (LCInsureAccTraceSchema) IntData.
              getValueByName("INLCInsureAccTrace");
          String iStrMoney = (String) IntData.getValueByName("InterMoney");
          String iEnterAccDate = (String) IntData.getValueByName(
              "InterEnterDate");
          double iMoney = Float.parseFloat(iStrMoney);
          //计算入机时间到保单上周年日的天数
          double intvDays = PubFun.calInterval(LastPolDate, iEnterAccDate, "D");
          double powIa = Math.pow( (1 + ActuRate), ( (TDays - intvDays) / TDays));
          double tempResultIa = iMoney * powIa;
          sumCFMoneyIa = sumCFMoneyIa + tempResultIa; //实际收益
          double powIg = Math.pow( (1 + EnsuRate), ( (TDays - intvDays) / TDays));
          double tempResultIg = iMoney * powIg;
          sumCFMoneyIg = sumCFMoneyIg + tempResultIg; //保证收益
        }
      }

      double sumBonus = FoundMoney * (1 + ActuRate) -
          FoundMoney * (1 + EnsuRate) + sumCFMoneyIa - sumCFMoneyIg;
      sumBonus = sumBonus * AssignRate;
      if (sumBonus == 0)
        return true;

      LOBonusGrpPolSchema tLOBonusGrpPolSchema = new LOBonusGrpPolSchema();
      tLOBonusGrpPolSchema.setPolNo(PolNo);
      tLOBonusGrpPolSchema.setGrpPolNo(tLOBonusGrpPolParmSchema.getGrpPolNo());
      tLOBonusGrpPolSchema.setSGetDate(SGetDate);
      tLOBonusGrpPolSchema.setInsuAccNo(InsuAccNo);
      tLOBonusGrpPolSchema.setFiscalYear(FiscalYear);
      tLOBonusGrpPolSchema.setPolYear(PolYear);
      tLOBonusGrpPolSchema.setPolTypeFlag(tLCPolSchema.getPolTypeFlag());
      tLOBonusGrpPolSchema.setBonusFlag("0");
      tLOBonusGrpPolSchema.setBonusMakeDate(PubFun.getCurrentDate());
      tLOBonusGrpPolSchema.setBonusMoney(sumBonus);
      tLOBonusGrpPolSchema.setMakeDate(PubFun.getCurrentDate());
      tLOBonusGrpPolSchema.setMakeTime(PubFun.getCurrentTime());
      tLOBonusGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
      tLOBonusGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
      tLOBonusGrpPolSchema.setOperator(mOperate);

      //事务提交
      Connection conn = DBConnPool.getConnection();
      if (conn == null) {
        CError.buildErr(this, "数据库连接失败");
        return false;
      }
      try {
        conn.setAutoCommit(false);
        LOBonusGrpPolDB tLOBonusGrpPolDB = new LOBonusGrpPolDB(conn);
        tLOBonusGrpPolDB.setSchema(tLOBonusGrpPolSchema);
        if (tLOBonusGrpPolDB.insert() == false) {
          conn.rollback();
          conn.close();
          CError.buildErr(this, "处理单个保单帐户:向数据库插入数据失败!");
          return false;
        }

        if (calInter) {

          LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB(conn);
          tLCInsureAccDB.setSchema(tLCInsureAccSchema);
          if (tLCInsureAccDB.update() == false) {
            conn.rollback();
            conn.close();
            CError.buildErr(this, "帐户表更新失败");
            return false;
          }

          LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB(conn);
          tLCInsureAccTraceDB.setSchema(tLCInsureAccTraceSchema);
          if (tLCInsureAccTraceDB.insert() == false) {
            conn.rollback();
            conn.close();
            CError.buildErr(this, "帐户表轨迹插入失败");
            return false;
          }
        }

        conn.commit();
        conn.close();
        System.out.println("commit over");
      }
      catch (Exception ex) {
        CError.buildErr(this, "插入数据库发生错误!" + ex);
        conn.rollback();
        conn.close();
        return false;
      }

    }
    catch (Exception ex) {
      CError.buildErr(this, "处理单个保单帐户发生错误!" + ex);
      return false;
    }
    return true;
  }

  /**
   * 纪录错误信息
   * @param tSerialNo
   * @param tPolNo
   * @param errDescribe
   * @param tGetMode
   * @return
   */
  private boolean insertErrLog(String tSerialNo, String tGrpPolNo,
                               String errDescribe, int tFiscalYear,
                               String tType) {
    LOBonusAssignGrpErrLogDB tLOBonusAssignGrpErrLogDB = new
        LOBonusAssignGrpErrLogDB();
    tLOBonusAssignGrpErrLogDB.setSerialNo(tSerialNo);
    tLOBonusAssignGrpErrLogDB.setGrpPolNo(tGrpPolNo);
    tLOBonusAssignGrpErrLogDB.setFiscalYear(tFiscalYear);
    tLOBonusAssignGrpErrLogDB.setType(tType);
    tLOBonusAssignGrpErrLogDB.setErrMsg(errDescribe);
    tLOBonusAssignGrpErrLogDB.setMakeDate(PubFun.getCurrentDate());
    tLOBonusAssignGrpErrLogDB.setMakeTime(PubFun.getCurrentTime());
    if (tLOBonusAssignGrpErrLogDB.insert() == false) {
      CError tError = new CError();
      tError.moduleName = "GrpAssignBonus";
      tError.functionName = "insertErrLog";
      tError.errorMessage = "纪录错误日志时发生错误：" +
          tLOBonusAssignGrpErrLogDB.mErrors.getFirstError() +
          "；解决该问题后，请再次分配当日红利";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  //--上面是团体分红计算

  //--下面是团体分红分配

  /**
   * 分配红利
   * @param tVData
   * @return
   */
  public boolean runAssignBonus(VData tVData) {
    GlobalInput tGlobalInput = ( (GlobalInput) tVData.getObjectByObjectName(
        "GlobalInput", 0));
    LOBonusGrpPolParmSet tLOBonusGrpPolParmSet = ( (LOBonusGrpPolParmSet)
                                                  tVData.getObjectByObjectName(
        "LOBonusGrpPolParmSet", 0));

    if (tGlobalInput == null || tLOBonusGrpPolParmSet == null) {
      CError.buildErr(this, "没有传入数据!");
      return false;
    }
    mOperate = tGlobalInput.Operator;
    mManageCom = tGlobalInput.ManageCom;
    String tLimit = PubFun.getNoLimit(mManageCom);
    String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //产生流水号码
    boolean errFlag = false;
    for (int i = 1; i <= tLOBonusGrpPolParmSet.size(); i++) {
      LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = tLOBonusGrpPolParmSet.
          get(i);
      mErrors.clearErrors();
      String tGrpPolNo = tLOBonusGrpPolParmSet.get(i).getGrpPolNo();
      if (AssignOneGrpPol(tGrpPolNo, tLOBonusGrpPolParmSchema.getFiscalYear()) == false) {
        insertErrLog(tSerialNo, tGrpPolNo, mErrors.getFirstError(),
                     tLOBonusGrpPolParmSchema.getFiscalYear(), "2");
        errFlag = true;
      }
    }
    if (errFlag)
      return false;
    return true;
  }

  /**
   * 处理单个团体保单红利
   * @param tGrpPolNo
   * @return
   */
  private boolean AssignOneGrpPol(String tGrpPolNo, int tFiscalYear) {
    //查询团体保单表
    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    tLCGrpPolDB.setGrpPolNo(tGrpPolNo);
    if (tLCGrpPolDB.getInfo() == false) {
      CError.buildErr(this, "没有查询到团单:" + tGrpPolNo);
      return false;
    }
    LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolDB.getSchema();
    //查询团体红利参数表
    LOBonusGrpPolParmDB tLOBonusGrpPolParmDB = new LOBonusGrpPolParmDB();
    tLOBonusGrpPolParmDB.setFiscalYear(tFiscalYear);
    tLOBonusGrpPolParmDB.setGrpPolNo(tGrpPolNo);
    if (tLOBonusGrpPolParmDB.getInfo() == false) {
      CError.buildErr(this,
                      "没有查询到团单:" + tGrpPolNo + "对应在团体红利参数表中会计年度为" + tFiscalYear +
                      "的数据");
      return false;
    }
    LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = tLOBonusGrpPolParmDB.
        getSchema();
    //如果该团单不处于待计算-返回
    if (!tLOBonusGrpPolParmSchema.getComputeState().equals("1")) {
      CError.buildErr(this, "团单:" + tGrpPolNo + " 对应在团体红利参数表中的状态不是待分配!");
      return false;
    }

    ExeSQL tExeSQL = new ExeSQL();
    String strSQL = "select count(*) from LOBonusGrpPol where  GrpPolNo='" +
        tGrpPolNo + "' and FiscalYear=" + tFiscalYear
        + " and BonusFlag='0'";
    SSRS tSSRS = tExeSQL.execSQL(strSQL);
    String strCount = tSSRS.GetText(1, 1);
    int SumCount = Integer.parseInt(strCount);
    if (SumCount == 0) {
      //更新LOBonusGrpPolParm的ComputeState计算状态
      tLOBonusGrpPolParmSchema.setComputeState("2");
      tLOBonusGrpPolParmSchema.setModifyDate(PubFun.getCurrentDate());
      tLOBonusGrpPolParmSchema.setModifyTime(PubFun.getCurrentTime());
      tLOBonusGrpPolParmDB = new LOBonusGrpPolParmDB();
      tLOBonusGrpPolParmDB.setSchema(tLOBonusGrpPolParmSchema);
      if (tLOBonusGrpPolParmDB.update() == false) {
        CError.buildErr(this, "团单:" + tGrpPolNo + " 更新团体红利参数表失败");
        return false;
      }
      return true;
    }
    TransferData tTransferData = new TransferData();
    int Num = 1;
    strSQL = "select PolNo,InsuAccNo from LOBonusGrpPol where  GrpPolNo='" +
        tGrpPolNo + "' and FiscalYear=" + tFiscalYear
        + " and BonusFlag='0'";
    while (Num <= SumCount) {
      Num = Num + COUNT;
      tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(strSQL, 1, COUNT);
      for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        tTransferData = new TransferData();
        tTransferData.setNameAndValue("PolNo", tSSRS.GetText(i, 1));
        tTransferData.setNameAndValue("InsuAccNo", tSSRS.GetText(i, 2));
        tTransferData.setNameAndValue("GrpPolNo", tGrpPolNo);
        tTransferData.setNameAndValue("FiscalYear", tFiscalYear);
        if (AssignOnePol(tTransferData) == false)
          return false;
      }
    }

    //更新LOBonusGrpPolParm的ComputeState计算状态
    tLOBonusGrpPolParmSchema.setComputeState("2");
    tLOBonusGrpPolParmSchema.setModifyDate(PubFun.getCurrentDate());
    tLOBonusGrpPolParmSchema.setModifyTime(PubFun.getCurrentTime());
    tLOBonusGrpPolParmDB = new LOBonusGrpPolParmDB();
    tLOBonusGrpPolParmDB.setSchema(tLOBonusGrpPolParmSchema);
    if (tLOBonusGrpPolParmDB.update() == false) {
      CError.buildErr(this, "团单:" + tGrpPolNo + " 更新团体红利参数表失败");
      return false;
    }

    return true;
  }

  /**
   * 分配单个保单帐户
   * @return
   */
  private boolean AssignOnePol(TransferData tTransferData) {
    try {
      String PolNo = (String) tTransferData.getValueByName("PolNo");
      String InsuAccNo = (String) tTransferData.getValueByName("InsuAccNo");
      String GrpPolNo = (String) tTransferData.getValueByName("GrpPolNo");
      int FiscalYear = ( (Integer) tTransferData.getValueByName("FiscalYear")).
          intValue();

      LOBonusGrpPolDB tLOBonusGrpPolDB = new LOBonusGrpPolDB();
      tLOBonusGrpPolDB.setPolNo(PolNo);
      tLOBonusGrpPolDB.setGrpPolNo(GrpPolNo);
      tLOBonusGrpPolDB.setInsuAccNo(InsuAccNo);
      tLOBonusGrpPolDB.setFiscalYear(FiscalYear);
      if (tLOBonusGrpPolDB.getInfo() == false) {
        CError.buildErr(this, "团单下个单:" + PolNo + " 的团体保单红利表查询失败");
        return false;
      }
      LOBonusGrpPolSchema tLOBonusGrpPolSchema = tLOBonusGrpPolDB.getSchema();
      double BonusMoney = tLOBonusGrpPolSchema.getBonusMoney();
      String SGetDate = tLOBonusGrpPolSchema.getSGetDate();

      LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
      tLCInsureAccDB.setPolNo(PolNo);
      tLCInsureAccDB.setInsuAccNo(InsuAccNo);
      //2005.1.20杨明注释掉问题代码tLCInsureAccDB.setOtherNo(PolNo);
      if (tLCInsureAccDB.getInfo() == false) {
        CError.buildErr(this, "团单下个单:" + PolNo + " 的帐户表查询失败");
        return false;
      }
      LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.getSchema();
      //添加帐户轨迹信息
      String tLimit = PubFun.getNoLimit(tLCInsureAccSchema.getManageCom());
      String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //产生流水号码
      LCInsureAccTraceSchema tLCInsureAccTraceSchema = new
          LCInsureAccTraceSchema();
      tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
      tLCInsureAccTraceSchema.setInsuAccNo(InsuAccNo);
      tLCInsureAccTraceSchema.setPolNo(PolNo);
      tLCInsureAccTraceSchema.setContNo("00000000000000000000");
      tLCInsureAccTraceSchema.setMoneyType("HL");
      tLCInsureAccTraceSchema.setRiskCode(tLCInsureAccSchema.getRiskCode());
      //2005.1.20杨明注释掉问题代码tLCInsureAccTraceSchema.setOtherNo(tLCInsureAccSchema.getOtherNo());
      //2005.1.20杨明注释掉问题代码tLCInsureAccTraceSchema.setOtherType(tLCInsureAccSchema.getOtherType());
      tLCInsureAccTraceSchema.setMoney(BonusMoney);
      tLCInsureAccTraceSchema.setGrpPolNo(GrpPolNo);
      //2005.1.20杨明注释掉问题代码tLCInsureAccTraceSchema.setInsuredNo(tLCInsureAccSchema.getInsuredNo());
      /*Lis5.3 upgrade get
       tLCInsureAccTraceSchema.setAppntName(tLCInsureAccSchema.getAppntName());
       */
      tLCInsureAccTraceSchema.setState("0");
      tLCInsureAccTraceSchema.setManageCom(tLCInsureAccSchema.getManageCom());
      tLCInsureAccTraceSchema.setOperator(mOperate);
      tLCInsureAccTraceSchema.setMakeDate(PubFun.getCurrentDate());
      tLCInsureAccTraceSchema.setMakeTime(PubFun.getCurrentTime());
      tLCInsureAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
      tLCInsureAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
      tLCInsureAccTraceSchema.setPayDate(SGetDate);

      //修改帐户金额
      tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema.getInsuAccBala() +
                                        BonusMoney);
      tLCInsureAccSchema.setInsuAccGetMoney(tLCInsureAccSchema.
                                            getInsuAccGetMoney() + BonusMoney);
      tLCInsureAccSchema.setSumPay(tLCInsureAccSchema.getSumPay() + BonusMoney);
      tLCInsureAccSchema.setModifyDate(PubFun.getCurrentDate());
      tLCInsureAccSchema.setModifyTime(PubFun.getCurrentTime());

      //更新团体红利分配表
      tLOBonusGrpPolSchema.setBonusFlag("1");
      tLOBonusGrpPolSchema.setAGetDate(SGetDate);
      tLOBonusGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
      tLOBonusGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
      //事务提交
      Connection conn = DBConnPool.getConnection();
      if (conn == null) {
        CError.buildErr(this, "数据库连接失败");
        return false;
      }
      try {
        conn.setAutoCommit(false);

        tLOBonusGrpPolDB = new LOBonusGrpPolDB(conn);
        tLOBonusGrpPolDB.setSchema(tLOBonusGrpPolSchema);
        if (tLOBonusGrpPolDB.update() == false) {
          conn.rollback();
          conn.close();
          CError.buildErr(this, "团体保单红利表更新失败");
          return false;
        }
        tLCInsureAccDB = new LCInsureAccDB(conn);
        tLCInsureAccDB.setSchema(tLCInsureAccSchema);
        if (tLCInsureAccDB.update() == false) {
          conn.rollback();
          conn.close();
          CError.buildErr(this, "帐户表更新失败");
          return false;
        }
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB(conn);
        tLCInsureAccTraceDB.setSchema(tLCInsureAccTraceSchema);
        if (tLCInsureAccTraceDB.insert() == false) {
          conn.rollback();
          conn.close();
          CError.buildErr(this, "帐户表轨迹插入失败");
          return false;
        }
      }
      catch (Exception ex) {
        CError.buildErr(this, "插入数据库发生错误!" + ex);
        return false;
      }
      conn.commit();
      conn.close();
      System.out.println("commit over");

    }
    catch (Exception ex) {
      CError.buildErr(this, "分配单个保单帐户发生错误!" + ex);
      return false;
    }
    return true;
  }

  /**
   * 计算会计年度末的账户利息
   * @param tLCPolSchema
   * @param tTransferData
   * @return
   */
  private TransferData CalPolInterest(LCPolSchema tLCPolSchema,
                                      TransferData tTransferData) {
    String PolNo = (String) tTransferData.getValueByName("PolNo");
    String InsuAccNo = (String) tTransferData.getValueByName("InsuAccNo");
    String SGetDate = (String) tTransferData.getValueByName("SGetDate");
    LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = (LOBonusGrpPolParmSchema)
        tTransferData.getValueByName("LOBonusGrpPolParm");
    int FiscalYear = ( (Integer) tTransferData.getValueByName("FiscalYear")).
        intValue();
    int PolYear = ( (Integer) tTransferData.getValueByName("PolYear")).intValue();
    String StrInsuAccBala = (String) tTransferData.getValueByName("InsuAccBala");

    double interRateMoney = 0.0;
    //1-求会计年度初到会计年度末帐户每条资金轨迹的利息
    String FiscalYearBegin = String.valueOf(FiscalYear) + "-1-1";
    String FiscalYearEnd = String.valueOf(FiscalYear) + "-12-31";

    ExeSQL iExeSQL = new ExeSQL();
    String iSQL =
        "select Money,PayDate from LCInsureAccTrace where InsuAccNo='" +
        InsuAccNo + "'"
        + " and PolNo='" + PolNo + "' and PayDate<='" + FiscalYearEnd +
        "' and PayDate>='" + FiscalYearBegin + "'";
    SSRS tSSRS = iExeSQL.execSQL(iSQL);
    double iMoney = 0;
    double iMoneyAccSum = 0;
    AccountManage tAccountManage = new AccountManage();
    LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
    tLMRiskInsuAccDB.setInsuAccNo(InsuAccNo);
    if (tLMRiskInsuAccDB.getInfo() == false) {
      CError.buildErr(this,
                      "账户描述表查询失败:" + InsuAccNo + tLMRiskInsuAccDB.mErrors.getFirstError());
      return null;
    }
    for (int i = 1; i <= tSSRS.MaxRow; i++) {
      String iStrMoneyTrace = tSSRS.GetText(i, 1);
      double iMoneyTrace = Double.parseDouble(iStrMoneyTrace); //会计年度的资金轨迹
      //2-求计算利息的天数-计算
      String iCalStartDate = tSSRS.GetText(i, 2); //计算起期
      String iCalEndDate = FiscalYearEnd; //计算止期
      //计算-利息金额
      iMoney = tAccountManage.getMultiAccInterest(InsuAccNo,
                                                  tLMRiskInsuAccDB.getSchema(),
                                                  iMoneyTrace, iCalStartDate,
                                                  iCalEndDate, "C", "D");
      iMoneyAccSum = iMoneyAccSum + iMoney;
    }
    //2-求会计年度初的资金总和。算利息.只有会计第二个保单年度才会有资金总和，第一个保单年度的资金总和＝0
    if (PolYear > 1) {
      //上个会计年度末的资金总和
      ExeSQL tExeSQL = new ExeSQL();
      String strSQL =
          "select sum(Money) from LCInsureAccTrace where InsuAccNo='" +
          InsuAccNo + "'"
          + " and PolNo='" + PolNo + "' and PayDate>='" + FiscalYearBegin +
          "' and PayDate<='" + PubFun.getCurrentDate() + "'";
      tSSRS = tExeSQL.execSQL(strSQL);
      String strSumMoney = tSSRS.GetText(1, 1);
      double SumMoney = Double.parseDouble(strSumMoney); //会计年度初至今的帐户资金调整总和
      double InsuAccBala = Double.parseDouble(StrInsuAccBala); //账户现在的余额
      double FoundMoney = InsuAccBala - SumMoney; //会计年度初的帐户资金余额

      double iSumMoney = 0;
      String iCalStartDate = FiscalYearBegin;
      String iCalEndDate = FiscalYearEnd; //计算止期
      int iCalDay = PubFun.calInterval(iCalStartDate, iCalEndDate, "D"); //计息天数
      iMoney = tAccountManage.getMultiAccInterest(InsuAccNo,
                                                  tLMRiskInsuAccDB.getSchema(),
                                                  FoundMoney, iCalStartDate,
                                                  iCalEndDate, "C", "D");
      iMoneyAccSum = iMoneyAccSum + iMoney;
    }

    //3- 准备账户轨迹，更新账户金额
    String iEnterAccDate = String.valueOf(FiscalYear) + "-12-31"; //利息计入账户的日期-会计年度末

    LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
    if (mManageCom == null || mManageCom.endsWith("")) {
      mManageCom = tLCPolSchema.getManageCom();
    }
    String tLimit = PubFun.getNoLimit(mManageCom);
    String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //产生流水号码
    tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
    tLCInsureAccTraceSchema.setInsuAccNo(InsuAccNo);
    tLCInsureAccTraceSchema.setPolNo(PolNo);
    tLCInsureAccTraceSchema.setMoneyType("LX");
    tLCInsureAccTraceSchema.setRiskCode(tLCPolSchema.getRiskCode());
    tLCInsureAccTraceSchema.setOtherNo(PolNo);
    tLCInsureAccTraceSchema.setOtherType("1");
    tLCInsureAccTraceSchema.setMoney(iMoneyAccSum);
    tLCInsureAccTraceSchema.setContNo(tLCPolSchema.getContNo());
    tLCInsureAccTraceSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
    //2005.1.20杨明注释掉问题代码tLCInsureAccTraceSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
    /*Lis5.3 upgrade get
             tLCInsureAccTraceSchema.setAppntName(tLCPolSchema.getAppntName());
     */
    tLCInsureAccTraceSchema.setState("0");
    tLCInsureAccTraceSchema.setManageCom(tLCPolSchema.getManageCom());
    tLCInsureAccTraceSchema.setOperator(tLCPolSchema.getOperator());
    tLCInsureAccTraceSchema.setMakeDate(PubFun.getCurrentDate());
    tLCInsureAccTraceSchema.setMakeTime(PubFun.getCurrentTime());
    tLCInsureAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
    tLCInsureAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
    tLCInsureAccTraceSchema.setPayDate(iEnterAccDate);

    LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
    tLCInsureAccDB.setPolNo(PolNo);
    tLCInsureAccDB.setInsuAccNo(InsuAccNo);
   //2005.1.20杨明注释掉问题代码 tLCInsureAccDB.setOtherNo(PolNo);
    if (tLCInsureAccDB.getInfo() == false) {
      CError.buildErr(this,
                      "保单账户查询数据失败:" + PolNo + tLCInsureAccDB.mErrors.getFirstError());
      return null;
    }
    LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.getSchema();
    tLCInsureAccSchema.setSumPay(tLCInsureAccSchema.getSumPay() + iMoneyAccSum);
    tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema.getInsuAccBala() +
                                      iMoneyAccSum);
    tLCInsureAccSchema.setInsuAccGetMoney(mDecimalFormat.format(
        tLCInsureAccSchema.getInsuAccGetMoney() + iMoneyAccSum));

    tLCInsureAccSchema.setBalaDate(iEnterAccDate); //利息结算日期
    tLCInsureAccSchema.setModifyDate(PubFun.getCurrentDate());
    tLCInsureAccSchema.setModifyTime(PubFun.getCurrentTime());

    //4－后续处理利息的红利计算--见后
    TransferData reData = new TransferData();
    reData.setNameAndValue("UPLCInsureAcc", tLCInsureAccSchema); //更新账户表
    reData.setNameAndValue("INLCInsureAccTrace", tLCInsureAccTraceSchema); //插入轨迹表-注意交费时间为会计年度末
    reData.setNameAndValue("InterMoney", String.valueOf(iMoneyAccSum)); //利息
    reData.setNameAndValue("InterEnterDate", iEnterAccDate); //利息计入账户的日期-会计年度末
    return reData;
  }

  /**
   *
   * @param tLOBonusGrpPolParmSchema
   * @param tLCGrpPolSchema
   * @return
   */
  private boolean CalGrpsubmitDB(LOBonusGrpPolParmSchema
                                 tLOBonusGrpPolParmSchema,
                                 LCGrpPolSchema tLCGrpPolSchema) {
    Connection conn = DBConnPool.getConnection();
    if (conn == null) {
      CError.buildErr(this, "数据库连接失败");
      return false;
    }
    try {
      conn.setAutoCommit(false);
      LOBonusGrpPolParmDB tLOBonusGrpPolParmDB = new LOBonusGrpPolParmDB(conn);
      tLOBonusGrpPolParmDB.setSchema(tLOBonusGrpPolParmSchema);
      if (tLOBonusGrpPolParmDB.update() == false) {
        CError.buildErr(this,
                        "团单:" + tLCGrpPolSchema.getGrpPolNo() + " 更新团体红利参数表fail");
        conn.rollback();
        conn.close();
        return false;
      }

      //添加打印表
      LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
      String tLimit = PubFun.getNoLimit(tLCGrpPolSchema.getManageCom());
      String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
      tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
      tLOPRTManagerSchema.setOtherNo(tLCGrpPolSchema.getGrpPolNo());
      tLOPRTManagerSchema.setOtherNoType("01");
      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_GRPBONUSPAY);
      tLOPRTManagerSchema.setManageCom(tLCGrpPolSchema.getManageCom());
      tLOPRTManagerSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
      tLOPRTManagerSchema.setReqCom(tLCGrpPolSchema.getManageCom());
      tLOPRTManagerSchema.setReqOperator(tLCGrpPolSchema.getOperator());
      tLOPRTManagerSchema.setPrtType("0");
      tLOPRTManagerSchema.setStateFlag("0");
      tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
      tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

      LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB(conn);
      tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
      if (tLOPRTManagerDB.insert() == false) {
        CError.buildErr(this,
                        "打印管理表插入失败:" + tLOPRTManagerDB.mErrors.getFirstError());
        conn.rollback();
        conn.close();
        return false;
      }
      conn.commit();
      conn.close();
    }
    catch (Exception ex) {
      CError.buildErr(this, "插入数据库发生错误!" + ex);
      try {
        conn.rollback();
        conn.close();
      }
      catch (Exception e) {}
      return false;
    }
    return true;
  }
}
