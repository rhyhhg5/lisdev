package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;

/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-07-01
 */

public class ProposalSignUI
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    // @Constructor
    public ProposalSignUI() {}

    // @Method
    public static void main(String[] args)
    {


        LCPolSet p = new LCPolSet();
        //pA1 A类主险，pA2 A类附险，pA3 A类附险
        LCPolSchema pA1 = new LCPolSchema();
        pA1.setPolNo( "86330120040110000113" );
        pA1.setPrtNo("86110100043840 ");

        LCPolSchema pA2 = new LCPolSchema();
        pA2.setPolNo( "86110020030110054440" );
        pA2.setPrtNo("8001004001");

        LCPolSchema pA3 = new LCPolSchema();
        pA3.setPolNo( "86110020030110006386" );
        pA2.setPrtNo("20030714009003");

        //pB1 B类主险，pB2 B类附险，pB3 B类附险
        LCPolSchema pB1 = new LCPolSchema();
        pB1.setPolNo( "86110020030110006263" );

        LCPolSchema pB2 = new LCPolSchema();
        pB2.setPolNo( "86110020030110006268" );

        LCPolSchema pB3 = new LCPolSchema();
        pB3.setPolNo( "86110020030110001356" );


        p.add( pA1 );
//        p.add( pA2 );
//        p.add( pA3 );
/*
        p.add( pB1 );
        p.add( pB2 );
        p.add( pA3 );

        p.add( pB3 );
*/
        GlobalInput g = new GlobalInput();
        g.ManageCom ="86110000";
        g.ComCode = "86110000";
        g.Operator = "001";
        VData v = new VData();
        v.add( p );
        v.add( g );
        ProposalSignUI ui = new ProposalSignUI();
        if( ui.submitData( v, "" ) == true )
            System.out.println("-----ok-----");
        else
            System.out.println("------NO------");
    }

	/**
	* 数据提交方法
	* @param: 传入数据、数据操作字符串
	* @return: boolean
	**/
	public boolean submitData( VData cInputData, String cOperate )
	{
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;

		ProposalSignBL tProposalSignBL = new ProposalSignBL();

		if( tProposalSignBL.submitData( cInputData, mOperate ) == false )
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tProposalSignBL.mErrors );
			return false;
		}

		return true;
	}

}