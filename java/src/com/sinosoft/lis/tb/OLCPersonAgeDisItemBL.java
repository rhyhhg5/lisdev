/*
 * <p>ClassName: OLCPersonAgeDisItemBL </p>
 * <p>Description: OLCPersonAgeDisItemBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-07-19 15:10:09
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

public class OLCPersonAgeDisItemBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LCPersonAgeDisItemSchema mLCPersonAgeDisItemSchema = new
            LCPersonAgeDisItemSchema();
    private LCPersonAgeDisItemSet mLCPersonAgeDisItemSet = new
            LCPersonAgeDisItemSet();
    /** 时间信息　*/
    private String tCurrentDate = PubFun.getCurrentDate();
    private String tCurrentTime = PubFun.getCurrentTime();
    /** 递交对象　*/
    private MMap map = new MMap();
    private String tGrpContNo = "";

    public OLCPersonAgeDisItemBL() {
    }

    public static void main(String[] args) {
        LCPersonAgeDisItemSet tLCPersonAgeDisItemSet = new
                LCPersonAgeDisItemSet();
        OLCPersonAgeDisItemUI tOLCPersonAgeDisItemUI = new
                OLCPersonAgeDisItemUI();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "pc";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        String tNum[] = {"1", "2"};
        String tStartAge[] = {"0", "9"};
        String tEndAge[] = {"8", "17"};
        String tOnWorkMCount[] = {"48", "25"};
        String tOnWorkFCount[] = {"70", "25"};
        String tOffWorkMCount[] = {"50", "18"};
        String tOffWorkFCount[] = {"90", "45"};
        String tMateMCount[] = {"20", "64"};
        String tMateFCount[] = {"0", "0"};
        String tYoungMCount[] = {"0", "0"};
        String tYoungFCount[] = {"", ""};
        String tOtherMCount[] = {"", ""};
        String tOtherFCount[] = {"", ""};
        int tCount = 0;
        if (tNum != null) {
            tCount = tNum.length;
        }
        if (tNum != null) {
            tCount = tNum.length;
        }
        for (int i = 0; i < tCount; i++) {
            LCPersonAgeDisItemSchema tLCPersonAgeDisItemSchema = new
                    LCPersonAgeDisItemSchema();

            tLCPersonAgeDisItemSchema.setGrpContNo("GrpContNo");
            tLCPersonAgeDisItemSchema.setPrtNo("PrtNo");
            tLCPersonAgeDisItemSchema.setStartAge(tStartAge[i]);
            tLCPersonAgeDisItemSchema.setEndAge(tEndAge[i]);
            tLCPersonAgeDisItemSchema.setOnWorkMCount(tOnWorkMCount[i]);
            tLCPersonAgeDisItemSchema.setOnWorkFCount(tOnWorkFCount[i]);
            tLCPersonAgeDisItemSchema.setOffWorkMCount(tOffWorkMCount[i]);
            tLCPersonAgeDisItemSchema.setOffWorkFCount(tOffWorkFCount[i]);
            tLCPersonAgeDisItemSchema.setMateMCount(tMateMCount[i]);
            tLCPersonAgeDisItemSchema.setMateFCount(tMateFCount[i]);
            tLCPersonAgeDisItemSchema.setYoungMCount(tYoungMCount[i]);
            tLCPersonAgeDisItemSchema.setYoungFCount(tYoungFCount[i]);
            tLCPersonAgeDisItemSchema.setOtherMCount(tOtherMCount[i]);
            tLCPersonAgeDisItemSchema.setOtherFCount(tOtherFCount[i]);
            tLCPersonAgeDisItemSet.add(tLCPersonAgeDisItemSchema);
        }
        VData tVData = new VData();
        tVData.add(tLCPersonAgeDisItemSet);
        tVData.add(tG);
        tOLCPersonAgeDisItemUI.submitData(tVData, "INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLCPersonAgeDisItemBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLCPersonAgeDisItemBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLCPersonAgeDisItemBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            tPubSubmit.submitData(mInputData, mOperate);
            System.out.println("End OLCPersonAgeDisItemBL Submit...");
            //如果有需要处理的错误，则返回
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            System.out.println("进行INSERT||MAIN操作");
            System.out.println(
                    "OLCPersonAgeDisItemBL.dealData()  \n--Line:97  --Author:YangMing");
            if (mLCPersonAgeDisItemSet.size() <= 0) {
                System.out.println(
                        "OLCPersonAgeDisItemBL.dealData()  \n--Line:100  --Author:YangMing");
                CError tError = new CError();
                tError.moduleName = "LCPersonAgeDisItemBL";
                tError.functionName = "dealData";
                tError.errorMessage = "前台传入的年龄分布信息不完整！";
                this.mErrors.addOneError(tError);
                return false;
            }

            String tSerialNo = PubFun1.CreateMaxNo("SerialNo", 10);
            int tTotalMaleCount = 0;
            int tTotalFemalCount = 0;
            int tTotalOnWorkMCount = 0;
            int tTotalOnWorkFCount = 0;
            int tTotalOffWorkMCount = 0;
            int tTotalOffWorkFCount = 0;
            int tTotalMateMCount = 0;
            int tTotalMateFCount = 0;
            int tTotalYoungMCount = 0;
            int tTotalYoungFCount = 0;
            int tTotalOtherMCount = 0;
            int tTotalOtherFCount = 0;

            String tmpPrtNo = "";
            double tAverageMaleAge = 0.0;
            double tAverageFemalAge = 0.0;
            double tAverageOnWorkMAge = 0.0;
            double tAverageOnWorkFAge = 0.0;
            double tAverageOffWorkMAge = 0.0;
            double tAverageOffWorkFAge = 0.0;
            double tAverageMateMAge = 0.0;
            double tAverageMateFAge = 0.0;
            double tAverageYoungMAge = 0.0;
            double tAverageYoungFAge = 0.0;
            double tAverageOtherMAge = 0.0;
            double tAverageOtherFAge = 0.0;
            for (int i = 1; i <= this.mLCPersonAgeDisItemSet.size(); i++) {

                int tMaleCount = 0; //每一年龄段的人数合计
                int tFemalCount = 0;

                mLCPersonAgeDisItemSet.get(i).setSerialNo(tSerialNo);

                tGrpContNo = mLCPersonAgeDisItemSet.get(i).getGrpContNo();
                tmpPrtNo = mLCPersonAgeDisItemSet.get(i).getPrtNo();
                mLCPersonAgeDisItemSet.get(i).setMakeDate(this.tCurrentDate);
                mLCPersonAgeDisItemSet.get(i).setModifyDate(this.tCurrentDate);
                mLCPersonAgeDisItemSet.get(i).setMakeTime(this.tCurrentTime);
                mLCPersonAgeDisItemSet.get(i).setModifyTime(this.tCurrentTime);
                tMaleCount = 0;
                tMaleCount += mLCPersonAgeDisItemSet.get(i).getOnWorkMCount();
                tMaleCount += mLCPersonAgeDisItemSet.get(i).getOffWorkMCount();
                tMaleCount += mLCPersonAgeDisItemSet.get(i).getMateMCount();
                tMaleCount += mLCPersonAgeDisItemSet.get(i).getYoungMCount();
                tMaleCount += mLCPersonAgeDisItemSet.get(i).getOtherMCount();
                tFemalCount = 0;
                tFemalCount += mLCPersonAgeDisItemSet.get(i).getOnWorkFCount();
                tFemalCount += mLCPersonAgeDisItemSet.get(i).getOffWorkFCount();
                tFemalCount += mLCPersonAgeDisItemSet.get(i).getMateFCount();
                tFemalCount += mLCPersonAgeDisItemSet.get(i).getYoungFCount();
                tFemalCount += mLCPersonAgeDisItemSet.get(i).getOtherFCount();

                mLCPersonAgeDisItemSet.get(i).setMaleCount(tMaleCount);
                mLCPersonAgeDisItemSet.get(i).setFemalCount(tFemalCount);
                //处理年龄合计
                tTotalMaleCount += tMaleCount;
                tTotalFemalCount += tFemalCount;
                tTotalOnWorkMCount += mLCPersonAgeDisItemSet.get(i).
                        getOnWorkMCount();
                tTotalOnWorkFCount += mLCPersonAgeDisItemSet.get(i).
                        getOnWorkFCount();
                tTotalOffWorkMCount += mLCPersonAgeDisItemSet.get(i).
                        getOffWorkMCount();
                tTotalOffWorkFCount += mLCPersonAgeDisItemSet.get(i).
                        getOffWorkFCount();
                tTotalMateMCount += mLCPersonAgeDisItemSet.get(i).getMateMCount();
                tTotalMateFCount += mLCPersonAgeDisItemSet.get(i).getMateFCount();
                tTotalYoungMCount += mLCPersonAgeDisItemSet.get(i).
                        getYoungMCount();
                tTotalYoungFCount += mLCPersonAgeDisItemSet.get(i).
                        getYoungFCount();
                tTotalOtherMCount += mLCPersonAgeDisItemSet.get(i).
                        getOtherMCount();
                tTotalOtherFCount += mLCPersonAgeDisItemSet.get(i).
                        getOtherFCount();
                //处理平均年龄
                int tStartAge = mLCPersonAgeDisItemSet.get(i).getStartAge();
                int tEndAge = mLCPersonAgeDisItemSet.get(i).getEndAge();
                tAverageMaleAge += ((tStartAge + tEndAge) / 2) * tMaleCount;
                tAverageFemalAge += ((tStartAge + tEndAge) / 2) * tFemalCount;
                tAverageOnWorkMAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getOnWorkMCount();
                tAverageOnWorkFAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getOnWorkFCount();
                tAverageOffWorkMAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getOffWorkMCount();
                tAverageOffWorkFAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getOffWorkFCount();
                tAverageMateMAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getMateMCount();
                tAverageMateFAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getMateFCount();
                tAverageYoungMAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getYoungMCount();
                tAverageYoungFAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getYoungFCount();
                tAverageOtherMAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getOtherMCount();
                tAverageOtherFAge += ((tStartAge + tEndAge) / 2) *
                        mLCPersonAgeDisItemSet.get(i).getOtherFCount();
            }
            //年龄人数总计
            LCPersonClassDisInfoSchema tLCPersonClassDisInfoSchema =
                    new LCPersonClassDisInfoSchema();
            tLCPersonClassDisInfoSchema.setGrpContNo(tGrpContNo);
            tLCPersonClassDisInfoSchema.setSerialNo(tSerialNo);
            tLCPersonClassDisInfoSchema.setPrtNo(tmpPrtNo);
            tLCPersonClassDisInfoSchema.setMakeDate(this.tCurrentDate);
            tLCPersonClassDisInfoSchema.setModifyDate(this.tCurrentDate);
            tLCPersonClassDisInfoSchema.setMakeTime(this.tCurrentTime);
            tLCPersonClassDisInfoSchema.setModifyTime(this.tCurrentTime);
            tLCPersonClassDisInfoSchema.setTotalMaleCount(tTotalMaleCount);
            tLCPersonClassDisInfoSchema.setTotalFemalCount(tTotalFemalCount);
            tLCPersonClassDisInfoSchema.setTotalOnWorkMCount(tTotalOnWorkMCount);
            tLCPersonClassDisInfoSchema.setTotalOnWorkFCount(tTotalOnWorkFCount);
            tLCPersonClassDisInfoSchema.setTotalOffWorkMCount(
                    tTotalOffWorkMCount);
            tLCPersonClassDisInfoSchema.setTotalOffWorkFCount(
                    tTotalOffWorkFCount);
            tLCPersonClassDisInfoSchema.setTotalMateMCount(tTotalMateMCount);
            tLCPersonClassDisInfoSchema.setTotalMateFCount(tTotalMateFCount);
            tLCPersonClassDisInfoSchema.setTotalYoungMCount(tTotalYoungMCount);
            tLCPersonClassDisInfoSchema.setTotalYoungFCount(tTotalYoungFCount);
            tLCPersonClassDisInfoSchema.setTotalOtherMCount(tTotalOtherMCount);
            tLCPersonClassDisInfoSchema.setTotalOtherFCount(tTotalOtherFCount);

            //处理平均年龄
            if (tTotalMaleCount > 0) {
                tAverageMaleAge = tAverageMaleAge / tTotalMaleCount;
            }
            System.out.println(" tTotalFemalCount :" + tTotalOnWorkMCount);
            if (tTotalFemalCount > 0) {
                tAverageFemalAge = tAverageFemalAge / tTotalFemalCount;
                System.out.println(" tAverageFemalAge :" +
                                   tAverageFemalAge);
            }
            System.out.println(" tTotalOffWorkMCount :" + tTotalOnWorkMCount);
            if (tTotalOnWorkMCount > 0) {
                tAverageOnWorkMAge = tAverageOnWorkMAge / tTotalOnWorkMCount;
                System.out.println(" tAverageOnWorkMAge :" +
                                   tAverageOnWorkMAge);
            }
            System.out.println(" tTotalOffWorkFCount :" + tTotalOnWorkMCount);
            if (tTotalOnWorkFCount > 0) {
                tAverageOnWorkFAge = tAverageOnWorkFAge / tTotalOnWorkFCount;
                System.out.println(" tAverageOnWorkFAge :" +
                            tAverageOnWorkFAge);
            }
            if (tTotalOffWorkMCount > 0) {
                tAverageOffWorkMAge = tAverageOffWorkMAge / tTotalOffWorkMCount;
            }
            System.out.println(" tTotalOffWorkFCount :" + tTotalOffWorkFCount);
            if (tTotalOffWorkFCount > 0) {
                tAverageOffWorkFAge = tAverageOffWorkFAge / tTotalOffWorkFCount;
                System.out.println(" tAverageOffWorkFAge :" +
                                   tAverageOffWorkFAge);
            }
            if (tTotalMateMCount > 0) {
                tAverageMateMAge = tAverageMateMAge / tTotalMateMCount;
            }
            if (tTotalMateFCount > 0) {
                tAverageMateFAge = tAverageMateFAge / tTotalMateFCount;
            }
            if (tTotalMateFCount > 0) {
                tAverageMateFAge = tAverageMateFAge / tTotalMateFCount;
            }
            if (tTotalYoungMCount > 0) {
                tAverageYoungMAge = tAverageYoungMAge / tTotalYoungMCount;
            }
            if (tTotalYoungFCount > 0) {
                tAverageYoungFAge = tAverageYoungFAge / tTotalYoungFCount;
            }
            if (tTotalOtherMCount > 0) {
                tAverageOtherMAge = tAverageOtherMAge / tTotalOtherMCount;
            }
            if (tTotalOtherFCount > 0) {
                tAverageOtherFAge = tAverageOtherFAge / tTotalOtherFCount;
            }

            String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
            DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

            LCPersonAgeDisInfoSchema tLCPersonAgeDisInfoSchema = new
                    LCPersonAgeDisInfoSchema();
            tLCPersonAgeDisInfoSchema.setGrpContNo(tGrpContNo);
            tLCPersonAgeDisInfoSchema.setSerialNo(tSerialNo);
            tLCPersonAgeDisInfoSchema.setPrtNo(tmpPrtNo);
            tLCPersonAgeDisInfoSchema.setMakeDate(this.tCurrentDate);
            tLCPersonAgeDisInfoSchema.setModifyDate(this.tCurrentDate);
            tLCPersonAgeDisInfoSchema.setMakeTime(this.tCurrentTime);
            tLCPersonAgeDisInfoSchema.setModifyTime(this.tCurrentTime);
            tLCPersonAgeDisInfoSchema.setAverageMaleAge(mDecimalFormat.format(
                    tAverageMaleAge));
            tLCPersonAgeDisInfoSchema.setAverageFemalAge(mDecimalFormat.format(
                    tAverageFemalAge));
            tLCPersonAgeDisInfoSchema.setAverageOnWorkMAge(mDecimalFormat.
                    format(tAverageOnWorkMAge));
            tLCPersonAgeDisInfoSchema.setAverageOnWorkFAge(mDecimalFormat.
                    format(tAverageOnWorkFAge));
            tLCPersonAgeDisInfoSchema.setAverageOffWorkMAge(mDecimalFormat.
                    format(
                            tAverageOffWorkMAge));
            tLCPersonAgeDisInfoSchema.setAverageOffWorkFAge(mDecimalFormat.
                    format(
                            tAverageOffWorkFAge));
            tLCPersonAgeDisInfoSchema.setAverageMateMAge(mDecimalFormat.format(
                    tAverageMateMAge));
            tLCPersonAgeDisInfoSchema.setAverageMateFAge(mDecimalFormat.format(
                    tAverageMateFAge));
            tLCPersonAgeDisInfoSchema.setAverageYoungMAge(mDecimalFormat.format(
                    tAverageYoungMAge));
            tLCPersonAgeDisInfoSchema.setAverageYoungFAge(mDecimalFormat.format(
                    tAverageYoungFAge));
            tLCPersonAgeDisInfoSchema.setAverageOtherMAge(mDecimalFormat.format(
                    tAverageOtherMAge));
            tLCPersonAgeDisInfoSchema.setAverageOtherFAge(mDecimalFormat.format(
                    tAverageOtherFAge));

            if (mLCPersonAgeDisItemSet.size() > 0) {
                map.put("delete from LCPersonAgeDisItem where grpcontno='"
                        + tGrpContNo + "'", "DELETE");
                map.put(this.mLCPersonAgeDisItemSet, "INSERT");
                map.put(tLCPersonClassDisInfoSchema, "DELETE&INSERT");
                map.put(tLCPersonAgeDisInfoSchema, "DELETE&INSERT");
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLCPersonAgeDisItemSet.set((LCPersonAgeDisItemSet) cInputData.
                                        getObjectByObjectName(
                                                "LCPersonAgeDisItemSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LCPersonAgeDisItemDB tLCPersonAgeDisItemDB = new LCPersonAgeDisItemDB();
        tLCPersonAgeDisItemDB.setSchema(this.mLCPersonAgeDisItemSchema);
        //如果有需要处理的错误，则返回
        if (tLCPersonAgeDisItemDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPersonAgeDisItemDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPersonAgeDisItemBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.map);
            mResult.clear();
            mResult.add(this.mLCPersonAgeDisItemSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPersonAgeDisItemBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
