package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LSInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class TaxConfirmListBL {

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	private GlobalInput mGlobalInput = null;

	private TransferData mTransferData = null;

	/** 传输到后台处理的容器 */
	private MMap mMap = new MMap();

	private VData mInputData = new VData();

	private LSInsuredListSet mLSInsuredListSet = new LSInsuredListSet();

	/** 数据操作字符串 */
	private String mOperate = "";

	private String mBatchNo = null;

	public TaxConfirmListBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
		if (!Checkscale()) {
			return false;

		}
		if (!dealData()) {
			return false;
		}
		if (!prepareData()) {
			return false;
		}

		PubSubmit p = new PubSubmit();
		if (!p.submitData(mInputData, "")) {
			System.out.println("提交数据失败");
			buildError("submitData", "提交数据失败");
			return false;
		}

		return true;
	}

	private boolean getInputData(VData cInputData, String cOperate) {
		mOperate = cOperate;
		this.mLSInsuredListSet.set((LSInsuredListSet) cInputData
				.getObjectByObjectName("LSInsuredListSet", 0));

		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		if (mGlobalInput == null) {
			buildError("getInputData", "处理超时，请重新登录。");
			return false;
		}

		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mTransferData == null) {
			buildError("getInputData", "所需参数不完整。");
			return false;
		}

		mBatchNo = (String) mTransferData.getValueByName("BatchNo");
		if (mBatchNo == null || "".equals(mBatchNo)) {
			buildError("getInputData", "获取批次信息失败。");
			return false;
		}

		return true;
	}

	private boolean checkData() {
		if ("ImportDelete".equals(mOperate)) {
			if (mLSInsuredListSet == null || mLSInsuredListSet.size() < 1) {
				buildError("checkData", "没有获取到要删除的客户信息！");
				return false;
			}
		}

		return true;
	}

	private boolean dealData() {
		String tSQL = null;
		if ("ImportConfirm".equals(mOperate)) {
			tSQL = "update lsbatchinfo set batchstate='3',modifydate=current date,modifytime=current time where batchno='"
					+ mBatchNo + "'";
			mMap.put(tSQL, SysConst.UPDATE);
			tSQL = null;
		}
		if ("ImportDelete".equals(mOperate)) {
			mMap.put(this.mLSInsuredListSet, SysConst.DELETE);
		}
		return true;
	}

	private boolean prepareData() {
		mInputData.add(mMap);
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "TaxConfirmListBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public VData getResult() {
		return mResult;
	}

	private boolean Checkscale() {
		ExeSQL tExeSql = new ExeSQL();
		String GrpNo = "";
		int yage = 1;
		int ypeople = 1;
		// 查团体客户号 modify by lxs 20160412
		String tStrSql = "Select grpno From Lsbatchinfo  Where batchno ='"
				+ mBatchNo + "'";
		GrpNo = new ExeSQL().getOneValue(tStrSql);
		System.out.println("查团体客户号" + GrpNo);
		// 查询在职人数
		String tStrSq5 = "select Peoples from lsgrp where grpno='" + GrpNo
				+ "'";
		double Peoples = Integer.parseInt(new ExeSQL().getOneValue(tStrSq5));
		System.out.println("查询在职人数" + Peoples);
	/*	if (Peoples < 3) {
			buildError("Checkscale", "查询在职人数为" + Peoples + "小于3不符合要求");
			return false;
		}*/
		// 查已承保客户总年龄// 查已承保客户总人数
		String tStrSql1 = "Select sum(InsuredAppAge),count(distinct insuredno) From lcpol Where Prtno In (Select Prtno From Lsinsuredlist Where Batchno In (Select Batchno  From Lsbatchinfo Where Grpno ='"
				+ GrpNo
				+ "'))And Conttype = '1' And Appflag = '1' And Stateflag = '1'";
		SSRS checkSSRS2 = tExeSql.execSQL(tStrSql1);
		yage = Integer.parseInt(checkSSRS2.GetText(1, 1));
		System.out.println("已承保客户总年龄" + yage);
		ypeople = Integer.parseInt(checkSSRS2.GetText(1, 2));
		System.out.println("已承保客户总人数" + ypeople);
		String tStrSql4 = "Select count(distinct insuredno) From lcpol Where Prtno In (Select Prtno From Lsinsuredlist Where Batchno In (Select Batchno  From Lsbatchinfo Where Grpno ='"
				+ GrpNo
				+ "'))And Conttype = '1' And Appflag = '1' And Stateflag = '1' and InsuredAppAge>='50'";
		int Peoples50 = Integer.parseInt(new ExeSQL().getOneValue(tStrSql4));
		System.out.println("已承保年龄超过50的人数" + Peoples50);
		String tStrSq2 = "select birthday from Lsinsuredlist where batchno='"
				+ mBatchNo
				+ "'  and customerno not in (Select distinct insuredno From lcpol Where Prtno In (Select Prtno From Lsinsuredlist Where Batchno In (Select Batchno  From Lsbatchinfo Where Grpno ='"
				+ GrpNo
				+ "'))And Conttype = '1' And Appflag = '1' And Stateflag = '1') ";
		SSRS list = tExeSql.execSQL(tStrSq2);
		System.out.println("数组长度" + list.MaxRow);
		for (int i = 1; i <=list.MaxRow; i++) {

			yage = yage
					+ PubFun.calInterval(list.GetText(i, 1), PubFun
							.getCurrentDate(), "Y");
			ypeople = ypeople + 1;
			if (PubFun.calInterval(list.GetText(i, 1), PubFun.getCurrentDate(),
					"Y") >= 50)// 查询导入的人中年龄超过50岁的（含）
			{
				Peoples50 = Peoples50 + 1;
			}
		//	System.out.println("第" + i + "次循环总年龄为" + yage + "总人数为" + ypeople);

		}
		
		double avgPeoples50=(Peoples50/Peoples)*100;
		System.out.println("年龄超过50岁的"+avgPeoples50);
		if (avgPeoples50 > 30) {
			buildError("Checkscale", "该团体下投保成员" + ypeople + "人，在职人员" + (int)Peoples
					+ "人，其中年龄50周岁（含）以上的在职人员" + Peoples50 + "人，占比已超过30%25.");
			return false;
		}
	/*	// 平均年龄
		// Code投保比例，RiskWrapPlanName是否包含，Code1年龄起CodeName年龄止CodeAlias人数起ComCode人数止
		double aveage = yage / ypeople;

		String tStrSq3 = "select Code,RiskWrapPlanName from ldcode1 where CodeType='SYTBBL' and int(code1) <= "
				+ aveage
				+ " and int(codename) >="
				+ aveage
				+ " and int(CodeAlias) <= "
				+ Peoples
				+ " and int(ComCode) >= "
				+ Peoples + " with ur";

		SSRS list1 = tExeSql.execSQL(tStrSq3);
		double scale=(ypeople/Peoples) * 100;
System.out.println("计算得出的投保比例"+scale);
		if (scale < Integer.parseInt(list1.GetText(1, 1))) {
			System.out.println();
			buildError("Checkscale", "该团体下投保成员" + ypeople + "人，在职人员" + (int)Peoples
					+ "人，团体中投保成员平均年龄" + list1.GetText(1, 2)
					+ "，目前投保比例小于团体最低投保比例" + list1.GetText(1, 1) + "%25.");
			return false;
		}*/
		return true;

	}
}
