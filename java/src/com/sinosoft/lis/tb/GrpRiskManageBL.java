package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDRiskManageSchema;
import com.sinosoft.lis.vschema.LDRiskManageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GrpRiskManageBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */
	private VData mInputData = new VData();

	/** 输出数据的容器 */
	private VData mResult = new VData();

	/** 提交数据的容器 */
	private MMap map = new MMap();

	/** 公共输入信息 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	// 统一更新日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 统一更新时间
	private String mCurrentTime = PubFun.getCurrentTime();

	/** 传入的规则 */

	private LDRiskManageSet tLDRiskManageSet = new LDRiskManageSet();

	private LDRiskManageSchema mLDRiskManageSchema = new LDRiskManageSchema();

	private LDRiskManageSchema tLDRiskManageSchema = new LDRiskManageSchema();

	public GrpRiskManageBL() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;

		if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			buildError("submitData", "提交数据时出错");
			return false;
		}

		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData(VData cInputData) {
		try {
			this.tLDRiskManageSet = (LDRiskManageSet) cInputData.getObjectByObjectName("LDRiskManageSet", 0);
			tLDRiskManageSchema = tLDRiskManageSet.get(1);
			mLDRiskManageSchema = tLDRiskManageSet.get(2);
			this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		} catch (Exception e) {
			buildError("getInputData", "传入的数据不完整" + e);
			System.out.println("传入的数据不完整");
		}
		return true;
	}

	/**
	 * 根据业务逻辑对数据进行处理
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean dealData() {

		try {

			tLDRiskManageSchema.setModifyDate(mCurrentDate);
			tLDRiskManageSchema.setModifyTime(mCurrentTime);

			if (mOperate.equals("INSERT||MAIN")) {
				tLDRiskManageSchema.setSerialNo(PubFun1.CreateMaxNo("SERIALNO", null));
				tLDRiskManageSchema.setCheckType("lgrprisk");
				tLDRiskManageSchema.setRiskCode(tLDRiskManageSchema.getRiskCode());
				tLDRiskManageSchema.setState(tLDRiskManageSchema.getState());
				tLDRiskManageSchema.setStartDate(tLDRiskManageSchema.getStartDate());
				tLDRiskManageSchema.setEndDate(tLDRiskManageSchema.getEndDate());
				tLDRiskManageSchema.setMakeDate(mCurrentDate);
				tLDRiskManageSchema.setModifyTime(mCurrentTime);
				tLDRiskManageSchema.setOperator(mGlobalInput.Operator);
				tLDRiskManageSchema.setManageCom(mGlobalInput.ManageCom);
				tLDRiskManageSchema.setMakeTime(mCurrentTime);

				map.put(tLDRiskManageSchema, "INSERT");

				// 将本机构的本规则内容对应的其他退则的默认标志置为不使用: 0

				String clrDefault = "";

				if (!clrDefault.equals("")) {
					map.put(clrDefault, "UPDATE");
				}
			} else if (mOperate.equals("UPDATE||MAIN")) {
				
				String Riskcode = mLDRiskManageSchema.getRiskCode();
				String State = mLDRiskManageSchema.getState();
				String StartDate = mLDRiskManageSchema.getStartDate();
				String EndDate = mLDRiskManageSchema.getEndDate();
				if (EndDate == null || "".equals(EndDate)) {
					String sql = "update LDRiskManage " + "set State='" + State + "', " + "StartDate='" + StartDate + "', " + "operator='"
							+ mGlobalInput.Operator + "', " + "ModifyDate='" + mCurrentDate + "', " + "ModifyTime='" + mCurrentTime + "' "
							+ "where Riskcode='" + Riskcode + "'  and checktype = 'lgrprisk'";
					map.put(sql, "UPDATE");
				} else {
					String sql = "update LDRiskManage " + "set State='" + State + "', " + "StartDate='" + StartDate + "', " + "EndDate='" + EndDate
							+ "', " + "operator='" + mGlobalInput.Operator + "', " + "ModifyDate='" + mCurrentDate + "', " + "ModifyTime='"
							+ mCurrentTime + "' " + "where Riskcode='" + Riskcode + "' and checktype = 'lgrprisk'";
					map.put(sql, "UPDATE");
				}

				System.out.println("UPDATE||MAIN");

			}

			else if (mOperate.equals("DELETE||MAIN")) {
				deleteRule();
			}
		} catch (Exception e) {
			buildError("submitData", "处理数据出错" + e);
			System.out.println("Catch Exception in submitData");
		}
		return true;
	}

	/**
	 * 根据业务逻辑对数据进行处理
	 * 
	 * @param: 无
	 * @return: void
	 */
	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception e) {
			buildError("prepareOutputData", "准备输往后台数据时出错");
			System.out.println("prepareOutputData, 准备输往后台数据时出错");
		}

		return true;
	}

	/** 生成错误信息 */
	private void buildError(String funcName, String errMes) {
		CError tError = new CError();

		tError.moduleName = "LGAutoDeliver";
		tError.functionName = funcName;
		tError.errorMessage = errMes;

		this.mErrors.addOneError(tError);
	}

	private boolean deleteRule() {
		String sql = "delete from LDRiskManage " + "where RiskCode='" + mLDRiskManageSchema.getRiskCode() + "' and checktype = 'lgrprisk'";
		map.put(sql, "DELETE");

		return true;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	public VData getResult() {
		return mResult;
	}

	private void jbInit() throws Exception {
	}

}
