package com.sinosoft.lis.tb;

import java.io.*;
import java.util.zip.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LDComSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ProposalDownloadNewBL {

    static final int BUFFER = 2048;
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private LCContPrintSet mLCContPrintSet = new LCContPrintSet();
    private String zippath;
    private String cDate = "";
    private String lispath = "";
    private boolean treturn;
    private TransferData mTransferData;
    private String mRealPath;

    public boolean submitData(VData cInputData, String cOperate) {
        mInputData = (VData) cInputData.clone();
        System.out.println("now in ProposalDownloadBL submit");
        if (this.getInputData(mInputData) == false) {
            return false;
        }
        System.out.println("---getInputData end---");

        if (this.dealData(mInputData) == false) {
            return false;
        } else {
            return true;
        }

    }

    private boolean getInputData(VData cInputData) {

        this.mLCContPrintSet.set((LCContPrintSet) cInputData.
                                 getObjectByObjectName(
                                         "LCContPrintSet", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        System.out.println(mLCContPrintSet.size());
        if (mTransferData == null) {
            buildError("getInputData",
                       "改造保单下载,生成文件路径不应是静态描述路径<br>author:Ｙａｎｇｍｉｎｇ "
                       + "<br>出现此问题让dongjb修改！1");
            return false;
        }
        mRealPath = (String) mTransferData.getValueByName("OutZipPath");
        if (StrTool.cTrim(this.mRealPath).equals("")) {
            buildError("getInputData",
                       "改造保单下载,生成文件路径不应是静态描述路径<br>author:Ｙａｎｇｍｉｎｇ "
                       + "<br>出现此问题让dongjb修改！2");
            return false;
        }
        if (mLCContPrintSet.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private boolean dealData(VData cInputData) {
        treturn = true;
        cDate = PubFun.getCurrentDate2();
        //String[] EntryItems=new String[mLCContSet.size()];
        //生成放zip文件的文件名***************************************************
//        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
//        tLDSysVarDB.setSysVar("UIRoot");
//        LDSysVarSet tLDSysVarSet = tLDSysVarDB.query();
        zippath = mRealPath + "" + "zipfile/" +
                  "downloadpic.zip";
        System.out.println("生成的zip包是:" + zippath);
        lispath = mRealPath + "";
//        Xmlpath = tLDSysVarSet.get(1).getSysVarValue() + "printdata/data/";
//        PrePicURL = ttLDSysVarSet.get(1).getSysVarValue();
//        System.out.println("prepicurl is "+PrePicURL);
        //下面定义一个数组,看该数组的大小*******************************************
        LCContPrintSet cLCContPrintSet = new LCContPrintSet();
        cLCContPrintSet = mLCContPrintSet;
        System.out.println("mLCContPrintSet.size is " + mLCContPrintSet.size());
//        for (int i = 1; i <= mLCContPrintSet.size(); i++) {
//            LCContPrintDB tLCContPrintDB = new LCContPrintDB();
//            tLCContPrintDB.setPrtNo(mLCContPrintSet.get(i).getPrtNo());
//            LCContPrintSet tLCContPrintSet = tLCContPrintDB.query();
//            System.out.println("tLCContPrintSet de daxiao shi " +
//                               tLCContPrintSet.size());
//            for (int j = 1; j <= tLCContPrintSet.size(); j++) {
//                cLCContPrintSet.add(tLCContPrintSet.get(j).getSchema());
//                System.out.println("jiaru cccccccccccc");
//            }
//        }
//
        String[] InputEntry = new String[cLCContPrintSet.size() + 1];
        String[] OutputEntry = new String[cLCContPrintSet.size() + 1];

        if (cLCContPrintSet.size() > 0) {
            LCContPrintSet tLCContPrintSet = new LCContPrintSet();
            for (int k = 1; k <= cLCContPrintSet.size(); k++) {
                int cflag = 1;
                for (int m = 1; m <= tLCContPrintSet.size(); m++) {
                    if (cLCContPrintSet.get(k).getOtherNo().equals(
                            tLCContPrintSet.get(m).getOtherNo())) {
                        cflag = 0;
                        break;
                    }
                }
                if (cflag == 1) {
                    tLCContPrintSet.add(cLCContPrintSet.get(k).getSchema());
                } else {
                    continue;
                }
            }
            if (tLCContPrintSet.size() > 0) {
                String[][] mToExcel = new String[tLCContPrintSet.size() + 2][4];
                mToExcel[0][0] = "机构编码";
                mToExcel[0][1] = "机构名称";
                mToExcel[0][2] = "保单号码";
                mToExcel[0][3] = "保单性质";

                for (int c = 1; c <= tLCContPrintSet.size(); c++) {
                    mToExcel[c][0] = tLCContPrintSet.get(c).getManageCom();
                    LDComDB tLDComDB = new LDComDB();
                    tLDComDB.setComCode(tLCContPrintSet.get(c).getManageCom());
                    LDComSet tLDComSet = new LDComSet();
                    tLDComSet = tLDComDB.query();
                    mToExcel[c][1] = tLDComSet.get(1).getName();
                    mToExcel[c][2] = tLCContPrintSet.get(c).getOtherNo();
                    String mOtherNoType = tLCContPrintSet.get(c).getOtherNoType();
                    if (mOtherNoType.equals("1")) {
                        mToExcel[c][3] = "个单";
                    } else if (mOtherNoType.equals("2")) {
                        mToExcel[c][3] = "团单";
                    } else {
                        mToExcel[c][3] = "";
                    }
                    mToExcel[tLCContPrintSet.size() + 1][0] = "合计";
                    mToExcel[tLCContPrintSet.size() +
                            1][1] = tLCContPrintSet.size() + "";
                    mToExcel[tLCContPrintSet.size() + 1][2] = "";
                    mToExcel[tLCContPrintSet.size() + 1][3] = "";
                    try {
                        WriteToExcel t = new WriteToExcel("proposaldownlog.xls");
                        t.createExcelFile();
                        String[] sheetName = {cDate};
                        t.addSheet(sheetName);
                        t.setData(0, mToExcel);
                        System.out.println("zipphat  : " + zippath);
                        t.write(zippath);
                    } catch (Exception ex) {
                        ex.toString();
                        ex.printStackTrace();
                    }
                }
            }
            System.out.println("总的压缩的文件数目为：" + cLCContPrintSet.size());
            for (int i = 0; i < cLCContPrintSet.size(); i++) {
                int j = i + 1;
                InputEntry[i] = lispath +
                                cLCContPrintSet.get(j).getFilePath();
                System.out.println("读取路径：" + InputEntry[i]);
                OutputEntry[i] = cDate + "/" +
                                 cLCContPrintSet.get(j).getManageCom() +
                                 "/" +
                                 cLCContPrintSet.get(j).getFilePath().substring(
                                         cLCContPrintSet.get(j).getFilePath().
                                         lastIndexOf("/") + 1);
                System.out.println("写入文件：" + OutputEntry[i]);
            }
            InputEntry[cLCContPrintSet.size()] = zippath +
                                                 "proposaldownlog.xls";
            OutputEntry[cLCContPrintSet.size()] = cDate + "\\" + cDate + ".xls";
        } else {
            treturn = false;
        }
        if (this.CreatZipFile(InputEntry, OutputEntry, zippath) == false) {
            treturn = false;
        } else {
            GlobalInput tGlobalInput = (GlobalInput) mInputData.
                                       getObjectByObjectName(
                                               "GlobalInput", 0);
            for (int i = 1; i <= cLCContPrintSet.size(); i++) {
                cLCContPrintSet.get(i).setDownloadCount(1);
                cLCContPrintSet.get(i).setDownloadDate(PubFun.getCurrentDate());
                cLCContPrintSet.get(i).setDownloadTime(PubFun.getCurrentTime());
                cLCContPrintSet.get(i).setDownOperator(tGlobalInput.Operator);
            }
            LCContPrintDBSet tLCContPrintDBSet = new LCContPrintDBSet();
            tLCContPrintDBSet.set(cLCContPrintSet);
            if (!tLCContPrintDBSet.update()) {
                treturn = false;
            } else {
                treturn = true;
            }
        }

        return treturn;
    }

    /**
     * substring
     *
     * @param i int
     * @return String
     */

    public boolean CreatZipFile(String[] tInputEntry, String[] tOutputEntry,
                                String tzippath) {
        System.out.println("===============开始创建压缩文件===============");
        System.out.println("tInputEntry.length is" + tInputEntry.length);
        System.out.println("tOutputEntry.length is" + tOutputEntry.length);
        System.out.println("tzippath is " + tzippath);
        if (tInputEntry.length != tOutputEntry.length) {
            return false;
        } else {
            try {
                System.out.println("开始将压缩文件放入压缩包");
                BufferedInputStream origin = null;
                System.out.println("tzippath is " + tzippath);
                FileOutputStream f = new FileOutputStream(tzippath);
                ZipOutputStream out = new ZipOutputStream(new
                        BufferedOutputStream(
                                f));
                byte data[] = new byte[BUFFER];
                for (int i = 0; i < tInputEntry.length; i++) {
                    FileInputStream fi = new FileInputStream(tInputEntry[i]);
                    origin = new BufferedInputStream(fi, BUFFER);
                    ZipEntry entry = new ZipEntry(tOutputEntry[i]);
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0,
                                                BUFFER)) != -1) {
                        out.write(data, 0, count);
                    }
                    origin.close();
                }
                out.close();
            } catch (Exception ex) {
            }
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ProposalDownloadBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }


    public static void main(String[] args) {
        LCContDB tLCContDB = new LCContDB();
        String str = "select * from lccont";
        //LCContSet tLCContSet = tLCContDB.executeQuery(str);
        String file =
                "sdflsjflsdfjsldjf/sldkjfsldjflsdj/sdflsdjsldjflsdjf/sdfsfsdf.xml".
                substring(
                        "sdflsjflsdfjsldjf/sldkjfsldjflsdj/sdflsdjsldjflsdjf/sdfsfsdf.xml".
                        lastIndexOf("/") + 1);
        System.out.println("String  : " + file);
    }
}
