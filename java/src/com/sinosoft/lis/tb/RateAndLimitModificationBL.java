package com.sinosoft.lis.tb;
import java.util.Set;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class RateAndLimitModificationBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */
	private VData mInputData = new VData();

	/** 输出数据的容器 */
	private VData mResult = new VData();

	/** 提交数据的容器 */
	private MMap map = new MMap();

	/** 公共输入信息 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	// 统一更新日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 统一更新时间
	private String mCurrentTime = PubFun.getCurrentTime();

    //	录入
	private String mGrpContNo = "";
	private String mContPlanCode = "";
	private String mRiskCode = "";
	private String mDutyCode = "";
	private String mGetRate = "";
	private String mGetlimit = "";

	private MMap mMap = new MMap();   
	public RateAndLimitModificationBL() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "RateAndLimitModificationBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
        //保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		
		mGrpContNo = (String)tTransferData.getValueByName("GrpContNo");
		mContPlanCode = (String)tTransferData.getValueByName("ContPlanCode");
		mRiskCode = (String)tTransferData.getValueByName("RiskCode");
		mDutyCode = (String)tTransferData.getValueByName("DutyCode");
		mGetRate = (String)tTransferData.getValueByName("GetRate");
		mGetlimit = (String)tTransferData.getValueByName("Getlimit");
		
		return true;

	}
	
	private boolean dealData(){
		String tSQLGet;
		String tSQLDuty;
		String tSQL1;
		String tSQL2;		
		String tSetS = "";
	
		if (mGetlimit!=null && !"".equals(mGetlimit)){
			if("".equals(tSetS)){
				tSetS = tSetS + " set Getlimit = "+mGetlimit+"";
			}else{
				tSetS = tSetS + " ,Getlimit = "+mGetlimit+"";
			}
		}
		if (mGetRate!=null && !"".equals(mGetRate)){
			if("".equals(tSetS)){
				tSetS = tSetS + " set GetRate = "+mGetRate+"";
			}else{
				tSetS = tSetS + " ,GetRate = "+mGetRate+"";
			}
		}
		
		tSQLGet = "update lcget "+tSetS+" where polno in (select polno from lcpol where grpcontno = '"+mGrpContNo+"' and contplancode = '"+mContPlanCode+"' and riskcode ='"+mRiskCode+"') and dutycode like '%"+mDutyCode+"%' ";
		tSQLDuty ="update lcduty "+tSetS+" where polno in (select polno from lcpol where grpcontno ='"+mGrpContNo+"' and contplancode ='"+mContPlanCode+"' and riskcode = '"+mRiskCode+"') and dutycode like '%"+mDutyCode+"%' ";
		tSQL1 ="update lccontplandutyparam set calfactorvalue = '"+mGetlimit+"' where grpcontno = '"+mGrpContNo+"' and contplancode = '"+mContPlanCode+"' and riskcode ='"+mRiskCode+"' and calfactor = 'GetLimit' and dutycode = '"+mDutyCode+"' ";
		tSQL2 ="update lccontplandutyparam set calfactorvalue = '"+mGetRate+"' where grpcontno = '"+mGrpContNo+"' and contplancode = '"+mContPlanCode+"' and riskcode ='"+mRiskCode+"'and calfactor = 'GetRate' and dutycode = '"+mDutyCode+"' ";
		
		mMap.put(tSQLGet, SysConst.UPDATE);
		mMap.put(tSQLDuty, SysConst.UPDATE);
		mMap.put(tSQL1, SysConst.UPDATE);
		mMap.put(tSQL2, SysConst.UPDATE);
		
		return true;
	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private void jbInit() throws Exception {
	}

}
