/*
 * <p>ClassName: OES_DOC_MAINBLS </p>
 * <p>Description: ES_DOC_MAINBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: EasyScan
 * @CreateDate：2002-11-27
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

 public class ModifyStateES_DOC_MAINBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
 /** 数据操作字符串 */
private String mOperate;
public ModifyStateES_DOC_MAINBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
     boolean tReturn =false;
     //将操作数据拷贝到本类中
     this.mOperate =cOperate;

    System.out.println("Start ES_DOC_MAINBLS Submit...");
    if(this.mOperate.equals("INSERT||MAIN"))
    {tReturn=saveES_DOC_MAIN(cInputData);}
    if (this.mOperate.equals("DELETE||MAIN"))
    {tReturn=deleteES_DOC_MAIN(cInputData);}
    if (this.mOperate.equals("UPDATE||MAIN"))
    {tReturn=updateES_DOC_MAIN(cInputData);}
    if (tReturn)
        System.out.println(" sucessful");
    else
        System.out.println("Save failed") ;
        System.out.println("End ES_DOC_MAINBLS Submit...");
  return tReturn;
}
 /**
* 保存函数
*/
private boolean saveES_DOC_MAIN(VData mInputData)
{
  boolean tReturn =true;
  System.out.println("Start Save...");
  Connection conn;
  conn=null;
  conn=DBConnPool.getConnection();
  if (conn==null)
  {
  		// @@错误处理
 		CError tError = new CError();
           tError.moduleName = "ES_DOC_MAINBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
	        return false;
   }
	try{
 		conn.setAutoCommit(false);
		System.out.println("Start 保存...");
           ES_DOC_MAINDB tES_DOC_MAINDB=new ES_DOC_MAINDB(conn);
           tES_DOC_MAINDB.setSchema((ES_DOC_MAINSchema)mInputData.getObjectByObjectName("ES_DOC_MAINSchema",0));
           if (!tES_DOC_MAINDB.insert())
           {
		// @@错误处理
	   	this.mErrors.copyAllErrors(tES_DOC_MAINDB.mErrors);
		CError tError = new CError();
           tError.moduleName = "ES_DOC_MAINBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据保存失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback();
           return false;
}
           conn.commit() ;
}
           catch (Exception ex)
           {
           // @@错误处理
               CError tError =new CError();
               tError.moduleName="ES_DOC_MAINBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               tReturn=false;
               try{conn.rollback() ;} catch(Exception e){}
	       }
               return tReturn;
   }
    /**
    * 保存函数
    */
    private boolean deleteES_DOC_MAIN(VData mInputData)
    {
        boolean tReturn =true;
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "ES_DOC_MAINBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           ES_DOC_MAINDB tES_DOC_MAINDB=new ES_DOC_MAINDB(conn);
           tES_DOC_MAINDB.setSchema((ES_DOC_MAINSchema)mInputData.getObjectByObjectName("ES_DOC_MAINSchema",0));
           if (!tES_DOC_MAINDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tES_DOC_MAINDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "ES_DOC_MAINBLS";
		    tError.functionName = "saveData";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               return false;
           }
               conn.commit() ;
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="ES_DOC_MAINBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          tReturn=false;
          try{conn.rollback() ;} catch(Exception e){}
         }
         return tReturn;
}
/**
  * 保存函数
*/
private boolean updateES_DOC_MAIN(VData mInputData)
{
     boolean tReturn =true;
     System.out.println("Start Save...");
     Connection conn;
     conn=null;
     conn=DBConnPool.getConnection();
     if (conn==null)
     {
	     CError tError = new CError();
        tError.moduleName = "ES_DOC_MAINBLS";
        tError.functionName = "updateData";
        tError.errorMessage = "数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
     }
     try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           ES_DOC_MAINDB tES_DOC_MAINDB=new ES_DOC_MAINDB(conn);
		tES_DOC_MAINDB.setSchema((ES_DOC_MAINSchema)mInputData.getObjectByObjectName("ES_DOC_MAINSchema",0));
           if (!tES_DOC_MAINDB.update())
           {
	          // @@错误处理
	         this.mErrors.copyAllErrors(tES_DOC_MAINDB.mErrors);
	         CError tError = new CError();
	         tError.moduleName = "ES_DOC_MAINBLS";
	         tError.functionName = "saveData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback();
            return false;
            }
            conn.commit() ;
       }
       catch (Exception ex)
       {
       // @@错误处理
               CError tError =new CError();
               tError.moduleName="ES_DOC_MAINBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               tReturn=false;
               try{conn.rollback() ;} catch(Exception e){}
     }
               return tReturn;
     }
}
