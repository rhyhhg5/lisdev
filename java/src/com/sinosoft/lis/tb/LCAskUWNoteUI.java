/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCAskUWNoteSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCContPlanDB;


/**
 * 保障计划前台数据传入接收类
 * <p>Title: </p>
 * <p>Description: 接收前台传入的数据，转入BL类 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author 朱向峰
 * @version 1.0
 */
public class LCAskUWNoteUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCAskUWNoteSchema mLCAskUWNoteSchema = new
            LCAskUWNoteSchema();
    public LCAskUWNoteUI() {
    }


    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        LCAskUWNoteBL tLCAskUWNoteBL = new LCAskUWNoteBL();
        System.out.println("Start LCAskUWNote UI Submit...");
        boolean isDone = true;
        isDone = tLCAskUWNoteBL.submitData(mInputData, mOperate);

        System.out.println("End LCAskUWNote UI Submit...");
        //如果有需要处理的错误，则返回
        if (tLCAskUWNoteBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCAskUWNoteBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return isDone;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {

            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLCAskUWNoteSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码

        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLCAskUWNoteSchema.setSchema((LCAskUWNoteSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LCAskUWNoteSchema", 0));
        mInputData = cInputData;
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        LCAskUWNoteSchema cLCAskUWNoteSchema = new LCAskUWNoteSchema();
        cLCAskUWNoteSchema.setNoteID("1");
        cLCAskUWNoteSchema.setGrpContNo("1400000533");
        cLCAskUWNoteSchema.setPrtNo("20050725002");
        cLCAskUWNoteSchema.setProPlan("ddddd");
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86110000";
        tG.Operator = "picch";
        LCAskUWNoteUI cLCAskUWNoteUI = new LCAskUWNoteUI();
        VData tVData = new VData();
        tVData.addElement(tG);
        tVData.addElement(cLCAskUWNoteSchema);
        try {
            if (cLCAskUWNoteUI.submitData(tVData, "UPDATE||MAIN")) {

            } else {
                System.out.println(
                        "+++++++++++++++++++++++ERR+++++++++++++++++");
            }

        } catch (Exception ex) {
            ex.toString();
            System.out.println(ex.toString());

        }

    }

}
