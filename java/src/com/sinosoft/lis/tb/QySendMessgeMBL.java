package com.sinosoft.lis.tb;

import java.rmi.RemoteException;
import java.util.Vector;

import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QySendMessgeMBL
{

    private GlobalInput mG = new GlobalInput();

    private TransferData mTransferData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private ExeSQL mExeSQL = new ExeSQL();

    private String mContTypeFlag = "";
    
    private String mPrtNo = "";

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public QySendMessgeMBL()
    {
    }

    private boolean getInputData(VData cInputData)
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            System.out.println("所需参数不完整");
            return false;
        }
        mContTypeFlag = mTransferData.getValueByName("ContTypeFlag").toString();
        mPrtNo = mTransferData.getValueByName("PrtNo").toString();
        System.out.println("-----------------ContTypeFlag = " + mContTypeFlag);
        System.out.println("-----------------PrtNo = " + mPrtNo);
        if(mPrtNo == null || "".equals(mPrtNo)){
        	return false;
        }
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData))
        {
            mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            if (mG.Operator == null || mG.Operator.equals("") || mG.Operator.equals("null"))
            {
                mG.Operator = "001";
            }
            if (mG.ManageCom == null || mG.ManageCom.equals("") || mG.ManageCom.equals("null"))
            {
                mG.ManageCom = "86";
            }
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("QYMSG"))
        {
            sendMsg();
        }
        else
        {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg()
    {

        System.out.println("签单信息短信通知批处理开始......");
        SmsServiceSoapBindingStub binding = null;
        try
        {
            binding = (SmsServiceSoapBindingStub) new SmsServiceServiceLocator().getSmsService(); //创建binding对象
        }
        catch (javax.xml.rpc.ServiceException jre)
        {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。

        Vector vec = new Vector();

        vec = getMessage();
        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec=" + vec.size());

        if (!vec.isEmpty())
        {
            for (int i = 0; i < vec.size(); i++)
            {
                tempVec.clear();
                tempVec.add(vec.get(i));

                SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
                msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                msgs.setServiceType("xuqi"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                msgs.setStartTime("00:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                msgs.setEndTime("23:59"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

                msgs.setMessages((SmsMessage[]) tempVec.toArray(new SmsMessage[tempVec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
                try
                {
                    value = binding.sendSMS("Admin", "Admin", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                    System.out.println(value.getStatus());
                    System.out.println(value.getMessage());
                }
                catch (RemoteException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        else
        {
            System.out.print("签单无符合条件的短信！");
        }
        System.out.println("签单信息短信通知批处理正常结束......");
        return true;
    }

    private Vector getMessage()
    {

        Vector tVector = new Vector();

        /* 
         * 第三部分:团单渠道
         * 标准团单的标志：lcgrpcont表中cardflag is null 。
         */
        if (mContTypeFlag.equals("2"))
        {
            String tSQLsucc = "select distinct lgc.prtno, "
                    + "lga.Name, "
                    + "lgc.Cvalidate, "
                    + "ljf.PayDate, "
                    + "lgc.Prem, "
                    + "lgads.Phone1, "
                    + "lgc.ManageCom "
                    + "from lcgrpcont lgc, LCGrpAppnt lga, ljtempfee ljf, lcgrpaddress lgads "
                    + "where 1 = 1 "
                    + "and lga.grpcontno = lgc.grpcontno "
                    + "and lgads.CustomerNo = lga.CustomerNo "
                    + "and lgads.addressno = lga.addressno "
                    + "and ljf.otherno = lgc.grpcontno "
                    + "and lgc.appflag = '1' "
                    + "and lgc.stateflag = '1' "
//                    + "and lgc.cardflag is null "
                    + "and lgc.prtno = '"+mPrtNo+"' ";
            System.out.println(tSQLsucc);
            SSRS tMsgSuccSSRS = mExeSQL.execSQL(tSQLsucc);

            for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++)
            {
                String tPrtNo = tMsgSuccSSRS.GetText(i, 1);
                String tAppntName = tMsgSuccSSRS.GetText(i, 2);
                String tCvalidate = tMsgSuccSSRS.GetText(i, 3);
                String tPayDate = tMsgSuccSSRS.GetText(i, 4);
                String tPrem = tMsgSuccSSRS.GetText(i, 5);
                String tPhone = tMsgSuccSSRS.GetText(i, 6);
                String tManageCom = tMsgSuccSSRS.GetText(i, 7);

                if (tAppntName == null || "".equals(tAppntName) || "null".equals(tAppntName))
                {
                    continue;
                }
                if (tPrtNo == null || "".equals(tPrtNo) || "null".equals(tPrtNo))
                {
                    continue;
                }
                if (tCvalidate == null || "".equals(tCvalidate) || "null".equals(tCvalidate))
                {
                    continue;
                }
                if (tPayDate == null || "".equals(tPayDate) || "null".equals(tPayDate))
                {
                    continue;
                }
                if (tPrem == null || "".equals(tPrem) || "null".equals(tPrem))
                {
                    continue;
                }
                if (tPhone == null || "".equals(tPhone) || "null".equals(tPhone))
                {
                    continue;
                }
                if (tManageCom == null || "".equals(tManageCom) || "null".equals(tManageCom))
                {
                    continue;
                }
                System.out.println("团单渠道:PrtNo = " + tPrtNo);

                //短信内容        	
                String tAgentContents = "";
                SmsMessage tAgentMsg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                tAgentContents = "尊敬的" + tAppntName + ",您好!" + "您的保单号为" + tPrtNo + "的保单已于" + tCvalidate + "生效,并于"
                        + tPayDate + "成功缴纳了保费,缴费金额为" + tPrem + "元.感谢您的支持,祝您健康。全国客户服务热线：95591或4006695518。";
                tAgentMsg.setReceiver(tPhone); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                //和IT讨论后，归类到二级机构
                tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                //添加SmsMessage对象                   
                tVector.add(tAgentMsg);
            }
        }

        return tVector;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData mTransferData = new TransferData();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        
        QySendMessgeMBL tQySendMessgeMBL = new QySendMessgeMBL();
        
        String mContTypeFlag = "2";
        
        mTransferData.setNameAndValue("ContTypeFlag", mContTypeFlag);
        mTransferData.setNameAndValue("PrtNo", "18121101001");
        mVData.add(mTransferData);
        
        tQySendMessgeMBL.submitData(mVData, "QYMSG");
    }

}
