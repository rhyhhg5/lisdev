package com.sinosoft.lis.tb;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.ES_DOC_PAGESDB;
import com.sinosoft.lis.db.ES_DOC_RELATIONDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DownLoadGifBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 扫描件类型 */
    private ES_DOC_MAINSchema mES_DOC_MAINSchema = null;

    private ES_DOC_PAGESSet mES_DOC_PAGESSet = null;

    private ES_DOC_RELATIONSet mES_DOC_RELATIONSet = null;
    
    private String  tDocId;

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }
        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        try
        {
            mOperate = cOperate;

            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            if (mGlobalInput == null)
            {
                buildError("getInputData", "处理超时，请重新登录。");
                return false;
            }

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mTransferData == null)
            {
                buildError("getInputData", "所需参数不完整。");
                return false;
            }

             tDocId = (String) mTransferData.getValueByName("DocId");
            if (!getScannerInfo(tDocId))
            {
                buildError("getInputData", "编号为[" + tDocId + "] 的扫描件信息不存在或不完整。");
                return false;
            }
        }
        catch (Exception ex)
        {
            buildError("getInputData", "未知异常：" + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * checkData
     * 
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 
     * @return boolean
     */
    private boolean dealData()
    {
    	 ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
         tES_DOC_PAGESDB.setDocID(tDocId);
         mES_DOC_PAGESSet = tES_DOC_PAGESDB.query();
         String imageTrealPath = new ExeSQL().getOneValue("select SysVarValue from LDSysVar where SysVar = 'UIRoot'"); 
         String pdfPath = imageTrealPath+mES_DOC_MAINSchema.getDocCode()+".pdf";
     	try {
			String imagePath = null;
			FileOutputStream fos = new FileOutputStream(pdfPath);
			Document doc = new Document(null, 0, 0, 0, 0);
			PdfWriter.getInstance(doc, fos);
			BufferedImage img = null;
			Image image = null;
				
			for (int m=0;m<mES_DOC_PAGESSet.size();m++) {
				String tpath = mES_DOC_PAGESSet.get(m + 1).getPicPath();
				if (mES_DOC_PAGESSet.get(m+1).getPageSuffix().toLowerCase().endsWith(".gif")) {
					// System.out.println(file1.getName());
//					imagePath = trealpath + tpath+mES_DOC_PAGESSet.get(m + 1).getPageName()+mES_DOC_PAGESSet.get(m + 1).getPageSuffix();
					imagePath =  imageTrealPath+tpath+mES_DOC_PAGESSet.get(m + 1).getPageName()+mES_DOC_PAGESSet.get(m + 1).getPageSuffix();
					// 读取图片流
					img = ImageIO.read(new File(imagePath));
					// 根据图片大小设置文档大小
					doc.setPageSize(new Rectangle(img.getWidth(), img.getHeight()));
					// 实例化图片
					image = Image.getInstance(imagePath);
					// 添加图片到文档
					doc.open();
					doc.add(image);
				}
			}
			// 关闭文档
			doc.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        return true;
    }
    /**
     * 获取该扫描件所有相关信息。
     * @param cDocId 扫描件id
     * @return 
     */
    private boolean getScannerInfo(String cDocId)
    {
        try
        {
            if (cDocId == null || cDocId.equals(""))
            {
                return false;
            }

            // 获取扫描主表信息。
            ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
            tES_DOC_MAINDB.setDocID(cDocId);
            if (!tES_DOC_MAINDB.getInfo())
            {
                return false;
            }
            mES_DOC_MAINSchema = tES_DOC_MAINDB.getSchema();
            // -----------------------------------------------

            // 获取扫描件全部页信息。
            ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
            tES_DOC_PAGESDB.setDocID(cDocId);
            mES_DOC_PAGESSet = tES_DOC_PAGESDB.query();
            if (mES_DOC_PAGESSet == null || mES_DOC_PAGESSet.size() <= 0)
            {
                return false;
            }
            // -----------------------------------------------

            // 获取扫描件业务关系信息。
            ES_DOC_RELATIONDB tES_DOC_RELATIONDB = new ES_DOC_RELATIONDB();
            tES_DOC_RELATIONDB.setDocID(cDocId);
            mES_DOC_RELATIONSet = tES_DOC_RELATIONDB.query();
            if (mES_DOC_RELATIONSet == null || mES_DOC_RELATIONSet.size() <= 0)
            {
                return false;
            }
            // -----------------------------------------------

            return true;
        }
        catch (Exception e)
        {
            buildError("getScannerMainInfo", e.getMessage());
            return false;
        }
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ScanDeleBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return mResult;
    }
    
}
