package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSBatchInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class TaxApplyBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput mGlobalInput = new GlobalInput();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	/** 数据操作字符串 */
	private String mOperate;
	
	//需要使用到的Schema
	private LSBatchInfoSchema tLSBatchInfoSchema = new LSBatchInfoSchema();
	
	public TaxApplyBL(){}
	
	public boolean submitData(VData cInputData, String cOperate){
		this.mOperate = cOperate;
		
		if(!getInputData(cInputData)){
			return false;
		}
		
		if(!checkData()){
			return false;
		}
		
		if(!dealData()){
			CError tError = new CError();
			tError.moduleName = "TaxApplyBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败TaxApplyBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		if(!prepareData()){
			return false;
		}
		
		PubSubmit tPubSubmit = new PubSubmit();
		if(!tPubSubmit.submitData(mInputData, mOperate)){
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "TaxApplyBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		mInputData = null;
		return true;
	}
	
	private boolean getInputData(VData cInputData){
		this.tLSBatchInfoSchema = (LSBatchInfoSchema)cInputData.getObjectByObjectName("LSBatchInfoSchema", 0);
		this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0));
		
		return true;
	}
	
	private boolean checkData(){
		if(tLSBatchInfoSchema.getBatchNo()==null || "".equals(tLSBatchInfoSchema.getBatchNo())){
			CError tError = new CError();
            tError.moduleName = "TaxApplyBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未获得批次号!";
            this.mErrors.addOneError(tError);
            return false;
		}
		if(tLSBatchInfoSchema.getGrpNo()==null || "".equals(tLSBatchInfoSchema.getGrpNo())){
			CError tError = new CError();
            tError.moduleName = "TaxApplyBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未获得税优团体客户号号!";
            this.mErrors.addOneError(tError);
            return false;
		}
		
		return true;
	}
	
	private boolean dealData(){
		tLSBatchInfoSchema.setApplyDate(mCurrentDate);
		tLSBatchInfoSchema.setManageCom(mGlobalInput.ComCode);
		tLSBatchInfoSchema.setOperator(mGlobalInput.Operator);
		tLSBatchInfoSchema.setBatchState("0");
		tLSBatchInfoSchema.setMakeDate(mCurrentDate);
		tLSBatchInfoSchema.setMakeTime(mCurrentTime);
		tLSBatchInfoSchema.setModifyDate(mCurrentDate);
		tLSBatchInfoSchema.setModifyTime(mCurrentTime);
		
		map.put(tLSBatchInfoSchema, SysConst.INSERT);
		
		return true;
	}
	
	private boolean prepareData(){
		try{
			mInputData = new VData();
			mInputData.add(map);
		}catch(Exception e){
			CError tError = new CError();
            tError.moduleName = "TaxApplyBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "准备往后台传输数据是出错!";
            this.mErrors.addOneError(tError);
            return false;
		}
		
		return true;
	}
	
	public VData getResult(){
        return mResult;
    }
}
