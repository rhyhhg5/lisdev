package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class UpdateRemarkBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	
	//录入的数据如下：
	private String mPrtNo = "";
	private String mAfterUpdateRemark = "";
	private String contType = "";
	private String action="";//添加数据库操作标识
	
	private MMap mMap = new MMap();

	public UpdateRemarkBL(){}
	
	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpdateRemarkBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start UpdateRemarkBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		
		mPrtNo = (String)tTransferData.getValueByName("PrtNo");
		contType = (String)tTransferData.getValueByName("ContType");
		action = (String)tTransferData.getValueByName("action");
		
		if (mPrtNo == null || "".equals(mPrtNo)){
			CError tError = new CError();
			tError.moduleName = "UpdateRemarkBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "印刷号为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mAfterUpdateRemark = (String)tTransferData.getValueByName("AfterUpdateRemark");
		
		return true;
	}

	private boolean dealData(){
		String newDate = "";
		String uSql = "";
		newDate = "modifydate = '"+CurrentDate+"',modifytime = '"+CurrentTime+"' ";
		
		//根据传过来的参数进行数据库操作
		if("update".equals(action)){
			if("1".equals(contType)){	//1表示个人
				uSql = "update lccont set remark = '"+mAfterUpdateRemark+"', "+newDate+" "
							+ "where prtno = '"+mPrtNo+"'";	
			}else if("2".equals(contType)){//2表示团体
				uSql = "update lcgrpcont set remark = '"+mAfterUpdateRemark+"', "+newDate+" "
							+ "where prtno = '"+mPrtNo+"'";
			}

			mMap.put(uSql, SysConst.UPDATE);
			return true;
		}
		
		return false;
	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
