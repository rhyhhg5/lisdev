package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCGrpSubInsuredImportDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.lis.vschema.LCGrpSubInsuredImportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;


public class LGrpUpdateInsuredBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /**提交数据的容器*/
    private MMap map = new MMap();

    /**公共输入信息*/
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;


    /**传入的规则*/
   
    private LCGrpSubInsuredImportSchema mLCGrpSubInsuredImportSchema = new LCGrpSubInsuredImportSchema();
    
    public LGrpUpdateInsuredBL()
    {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!dealData())
        {
            return false; 
        }

        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, ""))
        {
            buildError("submitData", "提交数据时出错");
            return false;
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mLCGrpSubInsuredImportSchema =
                    (LCGrpSubInsuredImportSchema) cInputData.
                    getObjectByObjectName("LCGrpSubInsuredImportSchema", 0);
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
        }
        catch(Exception e)
        {
            buildError("getInputData", "传入的数据不完整" + e);
            System.out.println("传入的数据不完整");
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        try
        {
        	String tSQL = "select cvalidate from lcgrpcont where prtno = '"+mLCGrpSubInsuredImportSchema.getPrtNo()+"' ";
        	String tCValiDate = new ExeSQL().getOneValue(tSQL);
        	if("".equals(StrTool.cTrim(tCValiDate))){
        		buildError("dealdata", "获取待处理保单生效日期失败！");
                return false;
        	}
        	int tAppAge = PubFun.getInsuredAppAge(tCValiDate, mLCGrpSubInsuredImportSchema.getBirthday());
        	String tExeOperate = "";
        	if("INSERT||MAIN".equals(mOperate)){
        		tExeOperate = SysConst.UPDATE;
            	tSQL = "select * from LCGrpSubInsuredImport where seqno = '"+mLCGrpSubInsuredImportSchema.getSeqNo()+"' ";
            	LCGrpSubInsuredImportDB tLCGrpSubInsuredImportDB = new LCGrpSubInsuredImportDB();
            	LCGrpSubInsuredImportSet tLCGrpSubInsuredImportSet = tLCGrpSubInsuredImportDB.executeQuery(tSQL);
            	if(tLCGrpSubInsuredImportSet != null && tLCGrpSubInsuredImportSet.size() > 0){
            		buildError("dealdata", "该被保人信息已经存在，请进行修改！");
                    return false;
            	}
        		tExeOperate = SysConst.INSERT;
        		tSQL = "select batchno from LCGrpSubInsuredImport where prtno = '"+mLCGrpSubInsuredImportSchema.getPrtNo()+"' ";
        		String tSeqNo = PubFun1.CreateMaxNo("BIGGRP", 20);
        		mLCGrpSubInsuredImportSchema.setSeqNo(tSeqNo);
        		mLCGrpSubInsuredImportSchema.setInsuredAppAge(tAppAge);
        		mLCGrpSubInsuredImportSchema.setStateFlag("0");
        		mLCGrpSubInsuredImportSchema.setDataChkFlag("00");
        		mLCGrpSubInsuredImportSchema.setAppFlag("0");
        		mLCGrpSubInsuredImportSchema.setOperator(mGlobalInput.Operator);
        		mLCGrpSubInsuredImportSchema.setMakeDate(PubFun.getCurrentDate());
        		mLCGrpSubInsuredImportSchema.setMakeTime(PubFun.getCurrentTime());
        		mLCGrpSubInsuredImportSchema.setModifyDate(PubFun.getCurrentDate());
        		mLCGrpSubInsuredImportSchema.setModifyTime(PubFun.getCurrentTime());
        		mLCGrpSubInsuredImportSchema.setCalState("00");
        		mLCGrpSubInsuredImportSchema.setBatchNo(mLCGrpSubInsuredImportSchema.getPrtNo()+"_"+mGlobalInput.Operator);
        		
            }else if("UPDATE||MAIN".equals(mOperate)){
            	tExeOperate = SysConst.UPDATE;
            	tSQL = "select * from LCGrpSubInsuredImport where seqno = '"+mLCGrpSubInsuredImportSchema.getSeqNo()+"' ";
            	LCGrpSubInsuredImportDB tLCGrpSubInsuredImportDB = new LCGrpSubInsuredImportDB();
            	LCGrpSubInsuredImportSet tLCGrpSubInsuredImportSet = tLCGrpSubInsuredImportDB.executeQuery(tSQL);
            	if(tLCGrpSubInsuredImportSet == null || tLCGrpSubInsuredImportSet.size() != 1){
            		buildError("dealdata", "获取待处理被保人信息失败！");
                    return false;
            	}
            	mLCGrpSubInsuredImportSchema.setInsuredAppAge(tAppAge);
            	mLCGrpSubInsuredImportSchema.setStateFlag("0");
            	mLCGrpSubInsuredImportSchema.setDataChkFlag("00");
            	mLCGrpSubInsuredImportSchema.setAppFlag("0");
            	mLCGrpSubInsuredImportSchema.setCalState("00");
            	mLCGrpSubInsuredImportSchema.setOperator(mGlobalInput.Operator);
            	mLCGrpSubInsuredImportSchema.setMakeDate(tLCGrpSubInsuredImportSet.get(1).getMakeDate());
        		mLCGrpSubInsuredImportSchema.setMakeTime(tLCGrpSubInsuredImportSet.get(1).getMakeTime());
            	mLCGrpSubInsuredImportSchema.setModifyDate(PubFun.getCurrentDate());
        		mLCGrpSubInsuredImportSchema.setModifyTime(PubFun.getCurrentTime());
        		
            	
            }else if("DELETE||MAIN".equals(mOperate)){
            	tSQL = "select * from LCGrpSubInsuredImport where seqno = '"+mLCGrpSubInsuredImportSchema.getSeqNo()+"' ";
            	LCGrpSubInsuredImportDB tLCGrpSubInsuredImportDB = new LCGrpSubInsuredImportDB();
            	LCGrpSubInsuredImportSet tLCGrpSubInsuredImportSet = tLCGrpSubInsuredImportDB.executeQuery(tSQL);
            	if(tLCGrpSubInsuredImportSet == null || tLCGrpSubInsuredImportSet.size() != 1){
            		buildError("dealdata", "获取待处理被保人信息失败！");
                    return false;
            	}
            	tExeOperate = SysConst.DELETE;
            }
        	
        	map.put(mLCGrpSubInsuredImportSchema, tExeOperate);
        }
        catch (Exception e)
        {
            buildError("submitData", "处理数据出错" + e);
            System.out.println("Catch Exception in submitData");
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch(Exception e)
        {
            buildError("prepareOutputData", "准备输往后台数据时出错");
            System.out.println("prepareOutputData, 准备输往后台数据时出错");
        }

        return true;
    }


    /**生成错误信息*/
    private void buildError(String funcName, String errMes)
    {
        CError tError = new CError();

        tError.moduleName = "LGAutoDeliver";
        tError.functionName = funcName;
        tError.errorMessage = errMes;

        this.mErrors.addOneError(tError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }


    private void jbInit() throws Exception {
    }

}


