package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBigProjectInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class BigProjectBL {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 提交数据的容器 */
    private MMap map = new MMap();

    /** 公共输入信息 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();
    
    private LCBigProjectInfoSchema tLCBigProjectInfoSchema = new LCBigProjectInfoSchema();
    
    public BigProjectBL(){}
    
    public boolean submitData(VData cInputData, String cOperate) {
    	
    	mOperate = cOperate;
    	mInputData = cInputData;
    	
    	if(!getInputData(mInputData)){
            return false;
        }

        if(!checkData()){
            return false;
        }

        if(!dealData()){
            return false;
        }
        
        VData data = new VData();
        data.add(map);
        
        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "BigProjectBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        
    	return true;
    }
	
    
    
    public boolean getInputData(VData tVData){
    	tLCBigProjectInfoSchema = (LCBigProjectInfoSchema)tVData.getObjectByObjectName("LCBigProjectInfoSchema", 0);
    	return true;
    }
    
    public boolean checkData(){
    	if(tLCBigProjectInfoSchema == null){
    		CError tError = new CError();
            tError.moduleName = "BigProjectBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有传入大项目";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
    	}
    	return true;
    }
    
    public boolean dealData(){
    	if("1".equals(mOperate)){
    		map.put(this.tLCBigProjectInfoSchema, SysConst.INSERT);
    	}
    	if("2".equals(mOperate)){
    		map.put(this.tLCBigProjectInfoSchema, SysConst.UPDATE);
    	}
    	return true;
    }
    
    public VData getResult() {
        return mResult;
    }
}
