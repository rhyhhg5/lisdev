package com.sinosoft.lis.tb;


import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LimitRiskBL {
    /** 数据操作字符串 */
    private String mOperateType;
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();//返回结果
    LDCode1Schema mLDCode1Schema = new LDCode1Schema();
    
    LDCode1Set lDCode1Set = new LDCode1Set();
    
    public boolean submitData(VData cInputData, String cOperate ){
//    	将操作数据拷贝到本类中
    	
        this.mOperateType = cOperate;
        mInputData = (VData)cInputData.clone();
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkData()){
        	return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult,null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }
    private boolean getInputData(VData cInputData){
    	lDCode1Set = (LDCode1Set) cInputData.getObjectByObjectName(
                "LDCode1Set", 1);
    	return true;
    }
    private boolean checkData() {
    	LDCode1DB mLDCode1DB = new LDCode1DB(); 
    	if("INSERT".equals(mOperateType)){
        	ExeSQL tExeSQL = new ExeSQL();
    		String tCode1 = lDCode1Set.get(1).getCode1();
    		String tCodeName = lDCode1Set.get(1).getCodeName();
    		String tSQL = "select 1 from LDCode1 where codetype='limitrisk'"
    					+ " and Code1='" + tCode1 + "'"
    					+ " and codename='" + tCodeName + "'";
    		String result=tExeSQL.getOneValue(tSQL);
        	if(result=="1"||"1".equals(result)){
        		mErrors.addOneError("已经存在该销售渠道下此险种的限制，不能重复添加");
        		return false;
        	}
    	}
    	if("INSERTMANAGECOM".equals(mOperateType)){
        	ExeSQL tExeSQL = new ExeSQL();
    		String tCode1 = lDCode1Set.get(1).getCode1();
    		String tCodeName = lDCode1Set.get(1).getCodeName();
    		String tcodealias = lDCode1Set.get(1).getCodeAlias();
    		String tSQL = "select 1 from LDCode1 where codetype='limitrisk1'"
    					+ " and Code1='" + tCode1 + "'"
    					+ " and codename='" + tCodeName + "'"
    					+ " and codealias='" + tcodealias + "'";
    		String result=tExeSQL.getOneValue(tSQL);
        	if(result=="1"||"1".equals(result)){
        		mErrors.addOneError("此险种下已经存在该管理机构，不能重复添加");
        		return false;
        	}
    	}
    	
    	return true;
    }
    private boolean dealData() {
    	if("INSERT".equals(mOperateType)){
	    	ExeSQL tExeSQL = new ExeSQL();
			String tSQL = "select max(int(code)) from LDCode1 where codetype='limitrisk'";
	        String tMaxNo = tExeSQL.getOneValue(tSQL);
	        if(tMaxNo==null||"".equals(tMaxNo)){
	        	lDCode1Set.get(1).setCode("1");
	        }else{
		        int MaxNo = Integer.parseInt(tMaxNo);
		        MaxNo+=1;
		        lDCode1Set.get(1).setCode(String.valueOf(MaxNo));
	        }
	    	return true;
    	}
    	if("DELETE".equals(mOperateType)){
    		String tCode1 = lDCode1Set.get(1).getCode1();
    		String tCodeName = lDCode1Set.get(1).getCodeName();
    		String tSQL = " select * from LDCode1 where codetype='limitrisk'"
    					+ " and Code1='" + tCode1 + "'"
    					+ " and codename='" + tCodeName + "'"
    					+ " union all "
    					+ " select * from LDCode1 where codetype='limitrisk1'"
    					+ " and codename='" + tCode1 + "'"
    					+ " and codealias = '" + tCodeName +"'";
    		LDCode1DB tLDCode1DB = new LDCode1DB();
    		lDCode1Set = tLDCode1DB.executeQuery(tSQL);
    		return true;
    	}
    	if("INSERTMANAGECOM".equals(mOperateType)){
	    	ExeSQL tExeSQL = new ExeSQL();
			String tSQL = "select max(int(code)) from LDCode1 where codetype='limitrisk1'";
	        String tMaxNo = tExeSQL.getOneValue(tSQL);
	        if(tMaxNo==null||"".equals(tMaxNo)){
	        	lDCode1Set.get(1).setCode("1");
	        }else{
		        int MaxNo = Integer.parseInt(tMaxNo);
		        MaxNo+=1;
		        lDCode1Set.get(1).setCode(String.valueOf(MaxNo));
	        }
	        mOperateType="INSERT";
	    	return true;
    	}
    	if("DELETEMANAGECOM".equals(mOperateType)){
    		String tCode1 = lDCode1Set.get(1).getCode1();
    		String tCodeName = lDCode1Set.get(1).getCodeName();
    		String tSQL = "select * from LDCode1 where codetype='limitrisk1'"
    					+ " and Code1='" + tCode1 + "'"
    					+ " and codename='" + tCodeName + "'";
    		LDCode1DB tLDCode1DB = new LDCode1DB();
    		lDCode1Set = tLDCode1DB.executeQuery(tSQL);
    		mOperateType="DELETE";
    		return true;
    	}
    	return true;
    }
    private boolean prepareOutputData(){
    	MMap map = new MMap();
    	
    	map.put(lDCode1Set,mOperateType);
    	mResult.add(map);
    	return true;
    }
}
