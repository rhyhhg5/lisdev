package com.sinosoft.lis.tb;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;

import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全删除业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author lh modified by Alex
 * @version 1.0
 */

//功能：个人保单一个保全项目更新批单号处理
//入口参数：个单的保单号、批单号和批改类型
//出口参数：各个表单要更新的记录数据
public class ChangeNo {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /**将原来的updateSql都放入map后作为返回结果，以后在PubSubmit中作数据库操作----Alex*/
    MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;
    private String newGrpContNo;  //新集体合同号
    private String newContNo;     //新合同号
    private String newProposalGrpContNo;   //新团体保单号
    private String newProposalContNo;  //新个人保单号
    private String newGrpPolNo;        //新团体险种号
    private String newPolNo;           //新个人保险种号码
    private String mLastEdorno;
    private String mManageCom;
    TransferData tTransferData=new TransferData();

    private boolean isGrpEdor;//true--团单，false--个单

    public ChangeNo() {
        mLastEdorno = "";
        mManageCom = "";
        this.isGrpEdor = true;
    }

    public ChangeNo(String GrpContNo,String newContNo,String ProposalGrpContNo,
                    String ProposalContNo, boolean isGrp) {
        newGrpContNo = GrpContNo;
        newContNo = newContNo;
        newProposalGrpContNo= ProposalGrpContNo;
        newProposalContNo=ProposalContNo;


        this.isGrpEdor = isGrp;
    }

    public String getNewEdorno() {
        return newGrpContNo;
    }

    public void setNewEdorno(String strNewEdorno) {
        newGrpContNo = strNewEdorno;
    }

    private boolean updateAsscociateTable() {

        /*String strLimit = PubFun.getNoLimit(mManageCom);
        if (isGrpEdor) {
            newGrpContNo = PubFun1.CreateMaxNo("EDORGRPNO", strLimit);
        } else {
            newGrpContNo = PubFun1.CreateMaxNo("EDORNO", strLimit);
        }*/

//        if (mLastEdorno.charAt(12) == '1') { //41为个单申请，只有此时才需要更换批单号
//            String strLimit = PubFun.getNoLimit(mManageCom);
//            mNewEdorno = PubFun1.CreateMaxNo("EDORNO", strLimit);
//        } else if (mLastEdorno.charAt(12) == '3') { //43为团单申请，只有此时才需要更换批单号
//            String strLimit = PubFun.getNoLimit(mManageCom);
//            mNewEdorno = PubFun1.CreateMaxNo("EDORGRPNO", strLimit);
//        } else {
//            String strLimit = PubFun.getNoLimit(mManageCom);
//            mNewEdorno = PubFun1.CreateMaxNo("OTHEREDORNO", strLimit);
//        }
           String strLimit = (String)tTransferData.getValueByName("ManageCom");

           newGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo", strLimit);
           newProposalGrpContNo = newGrpContNo;
           newProposalContNo = PubFun1.CreateMaxNo("ProposalContNo", strLimit);
           newContNo = newProposalContNo;
           newGrpPolNo = PubFun1.CreateMaxNo("GRPPOLNO", strLimit);
           newPolNo = PubFun1.CreateMaxNo("POLNO", strLimit);

           System.out.println("newGrpContNo : "+ newGrpContNo);
           System.out.println("newProposalGrpContNo : "+ newProposalGrpContNo);
           System.out.println("newProposalContNo : "+ newProposalContNo);
           System.out.println("newContNo : "+ newContNo);
           if (newGrpContNo == null || newGrpContNo.equals(""))
           {
             CError.buildErr(this, "生成新团体合同号失败!");
             return false;
           }
           if (newProposalGrpContNo == null || newProposalGrpContNo.equals(""))
           {
             CError.buildErr(this, "生成新团体保单号失败!");
             return false;
           }
           if (newContNo == null || newContNo.equals(""))
           {
             CError.buildErr(this, "生成新合同号失败!");
             return false;
           }
           if (newProposalContNo == null || newProposalContNo.equals(""))
           {
             CError.buildErr(this, "生成新保单号失败!");
             return false;
           }
           //查询条件
           String GrpContNo = (String)tTransferData.getValueByName("GrpContNo");
           System.out.println("GrpContNo : "+GrpContNo);
           //个单投保人表

           LCAppntSet tLCAppntSet = new LCAppntSet();
           LCAppntDB tLCAppntDB = new LCAppntDB();
           String sqlstr = "select * from lcappnt where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCAppntSet = tLCAppntDB.executeQuery(sqlstr);
           LCAppntSet mLCAppntSet = new LCAppntSet();
           for (int i = 1; i <= tLCAppntSet.size(); i++)

           {
             //newGrpContNo = PubFun1.CreateMaxNo("EDORGRPNO", strLimit);
             LCAppntSchema tLCAppntSchema = new LCAppntSchema();
             tLCAppntSchema = tLCAppntSet.get(i);
             tLCAppntSchema.setGrpContNo(newGrpContNo);
             tLCAppntSchema.setContNo(newContNo);
             mLCAppntSet.add(tLCAppntSchema);
           }
           map.put(mLCAppntSet, "INSERT");
           //团单投保人表
           LCGrpAppntSet tLCGrpAppntSet = new LCGrpAppntSet();
           LCGrpAppntSet mLCGrpAppntSet = new LCGrpAppntSet();
           LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
           sqlstr = "select * from LCGrpAppnt where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCGrpAppntSet = tLCGrpAppntDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCGrpAppntSet.size(); i++) {
             LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema();
             tLCGrpAppntSchema = tLCGrpAppntSet.get(i);
             tLCGrpAppntSchema.setGrpContNo(newGrpContNo);
             mLCGrpAppntSet.add(tLCGrpAppntSchema);
           }
           map.put(mLCGrpAppntSet, "INSERT");
           //团单服务信息表
           LCGrpServInfoSet tLCGrpServInfoSet = new LCGrpServInfoSet();
           LCGrpServInfoSet mLCGrpServInfoSet = new LCGrpServInfoSet();
           LCGrpServInfoDB tLCGrpServInfoDB = new LCGrpServInfoDB();
           sqlstr = "select * from LCGrpServInfo where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCGrpServInfoSet = tLCGrpServInfoDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCGrpServInfoSet.size(); i++) {
             LCGrpServInfoSchema tLCGrpServInfoSchema = new LCGrpServInfoSchema();
             tLCGrpServInfoSchema = tLCGrpServInfoSet.get(i);
             tLCGrpServInfoSchema.setGrpContNo(newGrpContNo);
             tLCGrpServInfoSchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCGrpServInfoSet.add(tLCGrpServInfoSchema);
           }
           map.put(mLCGrpServInfoSet, "INSERT");
           //个单服务信息表
           LCServInfoSet tLCServInfoSet = new LCServInfoSet();
           LCServInfoSet mLCServInfoSet = new LCServInfoSet();
           LCServInfoDB tLCServInfoDB = new LCServInfoDB();
           sqlstr = "select * from LCServInfo where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCServInfoSet = tLCServInfoDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCServInfoSet.size(); i++) {
             LCServInfoSchema tLCServInfoSchema = new LCServInfoSchema();
             tLCServInfoSchema = tLCServInfoSet.get(i);
             tLCServInfoSchema.setGrpContNo(newGrpContNo);
             tLCServInfoSchema.setContNo(newContNo);
             tLCServInfoSchema.setProposalContNo(newProposalContNo);
             mLCServInfoSet.add(tLCServInfoSchema);
           }
           map.put(mLCServInfoSet, "INSERT");
           //告知明细表
           LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = new
               LCCustomerImpartDetailSet();
           LCCustomerImpartDetailSet mLCCustomerImpartDetailSet = new
               LCCustomerImpartDetailSet();
           LCCustomerImpartDetailDB tLCCustomerImpartDetailDB = new
               LCCustomerImpartDetailDB();
           sqlstr = "select * from LCCustomerImpartDetail where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCCustomerImpartDetailSet = tLCCustomerImpartDetailDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCCustomerImpartDetailSet.size(); i++) {
             LCCustomerImpartDetailSchema tLCCustomerImpartDetailSchema = new
                 LCCustomerImpartDetailSchema();
             tLCCustomerImpartDetailSchema = tLCCustomerImpartDetailSet.get(i);
             tLCCustomerImpartDetailSchema.setGrpContNo(newGrpContNo);
             tLCCustomerImpartDetailSchema.setContNo(newContNo);
             tLCCustomerImpartDetailSchema.setProposalContNo(newProposalContNo);
             mLCCustomerImpartDetailSet.add(tLCCustomerImpartDetailSchema);
           }
           map.put(mLCCustomerImpartDetailSet, "INSERT");
           //客户告知参数
           LCCustomerImpartParamsSet tLCCustomerImpartParamsSet = new
               LCCustomerImpartParamsSet();
           LCCustomerImpartParamsSet mLCCustomerImpartParamsSet = new
               LCCustomerImpartParamsSet();
           LCCustomerImpartParamsDB tLCCustomerImpartParamsDB = new
               LCCustomerImpartParamsDB();
           sqlstr = "select * from LCCustomerImpartParams where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCCustomerImpartParamsSet = tLCCustomerImpartParamsDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCCustomerImpartDetailSet.size(); i++) {
             LCCustomerImpartParamsSchema tLCCustomerImpartParamsSchema = new
                 LCCustomerImpartParamsSchema();
             tLCCustomerImpartParamsSchema = tLCCustomerImpartParamsSet.get(i);
             tLCCustomerImpartParamsSchema.setGrpContNo(newGrpContNo);
             tLCCustomerImpartParamsSchema.setContNo(newContNo);
             tLCCustomerImpartParamsSchema.setProposalContNo(newProposalContNo);
             mLCCustomerImpartParamsSet.add(tLCCustomerImpartParamsSchema);
           }
           map.put(mLCCustomerImpartParamsSet, "INSERT");
           //客户告知
           LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
           LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
           LCCustomerImpartDB tLCCustomerImpartDB = new LCCustomerImpartDB();
           sqlstr = "select * from LCCustomerImpart where GrpContNo='"
               + (String) GrpContNo + "'";
           System.out.println(sqlstr);
           tLCCustomerImpartSet = tLCCustomerImpartDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCCustomerImpartSet.size(); i++) {
             LCCustomerImpartSchema tLCCustomerImpartSchema = new
                 LCCustomerImpartSchema();
             tLCCustomerImpartSchema = tLCCustomerImpartSet.get(i);
             tLCCustomerImpartSchema.setGrpContNo(newGrpContNo);
             tLCCustomerImpartSchema.setContNo(newContNo);
             tLCCustomerImpartSchema.setProposalContNo(newProposalContNo);
             mLCCustomerImpartSet.add(tLCCustomerImpartSchema);
           }
           map.put(mLCCustomerImpartSet, "INSERT");
           //个人保单
           LCContSet tLCContSet = new LCContSet();
           LCContSet mLCContSet = new LCContSet();
           LCContDB tLCContDB = new LCContDB();
           sqlstr = "select * from LCCont where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCContSet = tLCContDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCContSet.size(); i++) {
             LCContSchema tLCContSchema = new LCContSchema();
             tLCContSchema = tLCContSet.get(i);
             tLCContSchema.setGrpContNo(newGrpContNo);
             tLCContSchema.setContNo(newContNo);
             tLCContSchema.setProposalContNo(newProposalContNo);
             mLCContSet.add(tLCContSchema);
           }
           map.put(mLCContSet, "INSERT");
           //集体保单表
           LCGrpContSet tLCGrpContSet = new LCGrpContSet();
           LCGrpContSet mLCGrpContSet = new LCGrpContSet();
           LCGrpContDB tLCGrpContDB = new LCGrpContDB();
           sqlstr = "select * from LCGrpCont where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCGrpContSet = tLCGrpContDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCGrpContSet.size(); i++) {
             LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
             tLCGrpContSchema = tLCGrpContSet.get(i);
             tLCGrpContSchema.setGrpContNo(newGrpContNo);
             tLCGrpContSchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCGrpContSet.add(tLCGrpContSchema);
           }
           map.put(mLCGrpContSet, "INSERT");
           //保单计划
           LCContPlanSet tLCContPlanSet = new LCContPlanSet();
           LCContPlanSet mLCContPlanSet = new LCContPlanSet();
           LCContPlanDB tLCContPlanDB = new LCContPlanDB();
           sqlstr = "select * from LCContPlan where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCContPlanSet = tLCContPlanDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCContPlanSet.size(); i++) {
             LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
             tLCContPlanSchema = tLCContPlanSet.get(i);
             tLCContPlanSchema.setGrpContNo(newGrpContNo);
             tLCContPlanSchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCContPlanSet.add(tLCContPlanSchema);
           }
           map.put(mLCContPlanSet, "INSERT");
           //保单险种保险计划表
           LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
           LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
           LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
           sqlstr = "select * from LCContPlanRisk where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCContPlanRiskSet = tLCContPlanRiskDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCContPlanRiskSet.size(); i++) {
             LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
             tLCContPlanRiskSchema = tLCContPlanRiskSet.get(i);
             tLCContPlanRiskSchema.setGrpContNo(newGrpContNo);
             tLCContPlanRiskSchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
           }
           map.put(mLCContPlanRiskSet, "INSERT");
           //保险计划责任要素值
           LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
           LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
           LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
           sqlstr = "select * from LCContPlanDutyParam where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCContPlanDutyParamSet.size(); i++) {
             LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new
                 LCContPlanDutyParamSchema();
             tLCContPlanDutyParamSchema = tLCContPlanDutyParamSet.get(i);
             tLCContPlanDutyParamSchema.setGrpContNo(newGrpContNo);
             tLCContPlanDutyParamSchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
           }
           map.put(mLCContPlanDutyParamSet, "INSERT");
           //健康险要素计算Sql集合
           LCContPlanFactorySet tLCContPlanFactorySet = new LCContPlanFactorySet();
           LCContPlanFactorySet mLCContPlanFactorySet = new LCContPlanFactorySet();
           LCContPlanFactoryDB tLCContPlanFactoryDB = new LCContPlanFactoryDB();
           sqlstr = "select * from LCContPlanFactory where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           mLCContPlanFactorySet = tLCContPlanFactoryDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCContPlanFactorySet.size(); i++) {
             LCContPlanFactorySchema tLCContPlanFactorySchema = new
                 LCContPlanFactorySchema();
             tLCContPlanFactorySchema = tLCContPlanFactorySet.get(i);
             tLCContPlanFactorySchema.setGrpContNo(newGrpContNo);
             tLCContPlanFactorySchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCContPlanFactorySet.add(tLCContPlanFactorySchema);
           }
           map.put(mLCContPlanFactorySet, "INSERT");
           //健康险要素计算Sql中的计算子要素信息，以便支持对他们的访问、引用
           LCContPlanParamSet tLCContPlanParamSet = new LCContPlanParamSet();
           LCContPlanParamSet mLCContPlanParamSet = new LCContPlanParamSet();
           LCContPlanParamDB tLCContPlanParamDB = new LCContPlanParamDB();
           sqlstr = "select * from LCContPlanParam where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCContPlanParamSet = tLCContPlanParamDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCContPlanParamSet.size(); i++) {
             LCContPlanParamSchema tLCContPlanParamSchema = new LCContPlanParamSchema();
             tLCContPlanParamSchema = tLCContPlanParamSet.get(i);
             tLCContPlanParamSchema.setGrpContNo(newGrpContNo);
             tLCContPlanParamSchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCContPlanParamSet.add(tLCContPlanParamSchema);
           }
           map.put(mLCContPlanParamSet, "INSERT");
           //总括保单处理分单表
           LCGeneralSet tLCGeneralSet = new LCGeneralSet();
           LCGeneralSet mLCGeneralSet = new LCGeneralSet();
           LCGeneralDB tLCGeneralDB = new LCGeneralDB();
           sqlstr = "select * from LCGeneral where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCGeneralSet = tLCGeneralDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCGeneralSet.size(); i++) {
             LCGeneralSchema tLCGeneralSchema = new LCGeneralSchema();
             tLCGeneralSchema = tLCGeneralSet.get(i);
             tLCGeneralSchema.setGrpContNo(newGrpContNo);
             //tLCGeneralSchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCGeneralSet.add(tLCGeneralSchema);
           }
           map.put(mLCGeneralSet, "INSERT");
           //集体险种表
           LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
           LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
           LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
           sqlstr = "select * from LCGrpPol where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCGrpPolSet = tLCGrpPolDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
             LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
             tLCGrpPolSchema = tLCGrpPolSet.get(i);
             tLCGrpPolSchema.setGrpContNo(newGrpContNo);
             tLCGrpPolSchema.setGrpPolNo(newGrpPolNo);
             //tLCGrpPolSchema.setProposalGrpContNo(newProposalGrpContNo);
             mLCGrpPolSet.add(tLCGrpPolSchema);
           }
           map.put(mLCGrpPolSet, "INSERT");
           //个人险种表
           LCPolSet tLCPolSet = new LCPolSet();
           LCPolSet mLCPolSet = new LCPolSet();
           LCPolDB tLCPolDB = new LCPolDB();
           sqlstr = "select * from LCPol where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCPolSet = tLCPolDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCPolSet.size(); i++) {
             LCPolSchema tLCPolSchema = new LCPolSchema();
             tLCPolSchema = tLCPolSet.get(i);
             tLCPolSchema.setGrpContNo(newGrpContNo);
             tLCPolSchema.setGrpPolNo(newGrpPolNo);
             tLCPolSchema.setPolNo(newPolNo);
             tLCPolSchema.setContNo(newContNo);
             mLCPolSet.add(tLCPolSchema);
           }
           map.put(mLCPolSet, "INSERT");
           //个单被保人表
           LCInsuredSet tLCInsuredSet = new LCInsuredSet();
           LCInsuredSet mLCInsuredSet = new LCInsuredSet();
           LCInsuredDB tLCInsuredDB = new LCInsuredDB();
           sqlstr = "select * from LCInsured where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCInsuredSet = tLCInsuredDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCInsuredSet.size(); i++) {
             LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
             tLCInsuredSchema = tLCInsuredSet.get(i);
             tLCInsuredSchema.setGrpContNo(newGrpContNo);
             tLCInsuredSchema.setContNo(newContNo);
             mLCInsuredSet.add(tLCInsuredSchema);
           }
           map.put(mLCInsuredSet, "INSERT");
           //受益人
           LCBnfSet tLCBnfSet = new LCBnfSet();
           LCBnfSet mLCBnfSet = new LCBnfSet();
           LCBnfDB tLCBnfdDB = new LCBnfDB();
           sqlstr = "select * from LCBnfd where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCBnfSet = tLCBnfdDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCBnfSet.size(); i++) {
             LCBnfSchema tLCBnfSchema = new LCBnfSchema();
             tLCBnfSchema = tLCBnfSet.get(i);
             //tLCBnfSchema.setPolNo(newGrpContNo);
             tLCBnfSchema.setPolNo(newPolNo);
             tLCBnfSchema.setContNo(newContNo);
             mLCBnfSet.add(tLCBnfSchema);
           }
           map.put(mLCBnfSet, "INSERT");
           //保单险种责任表
           LCDutySet tLCDutySet = new LCDutySet();
           LCDutySet mLCDutySet = new LCDutySet();
           LCDutyDB tLCDutyDB = new LCDutyDB();
           sqlstr = "select * from LCDuty where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCDutySet = tLCDutyDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCDutySet.size(); i++) {
             LCDutySchema tLCDutySchema = new LCDutySchema();
             tLCDutySchema = tLCDutySet.get(i);
             //tLCDutySchema.setPolNo(newGrpContNo);
             tLCDutySchema.setContNo(newContNo);
             tLCDutySchema.setPolNo(newPolNo);
             mLCDutySet.add(tLCDutySchema);
           }
           map.put(mLCDutySet, "INSERT");
           //保费项表
           LCPremSet tLCPremSet = new LCPremSet();
           LCPremSet mLCPremSet = new LCPremSet();
           LCPremDB tLCPremDB = new LCPremDB();
           sqlstr = "select * from LCPrem where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCPremSet = tLCPremDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCPremSet.size(); i++) {
             LCPremSchema tLCPremSchema = new LCPremSchema();
             tLCPremSchema = tLCPremSet.get(i);
             tLCPremSchema.setContNo(newContNo);
             tLCPremSchema.setGrpContNo(newGrpContNo);
             tLCPremSchema.setPolNo(newPolNo);
             mLCPremSet.add(tLCPremSchema);
           }
           map.put(mLCPremSet, "INSERT");
           //领取项表
           LCGetSet tLCGetSet = new LCGetSet();
           LCGetSet mLCGetSet = new LCGetSet();
           LCGetDB tLCGetDB = new LCGetDB();
           sqlstr = "select * from LCGet where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCGetSet = tLCGetDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCGetSet.size(); i++) {
             LCGetSchema tLCGetSchema = new LCGetSchema();
             tLCGetSchema = tLCGetSet.get(i);
             tLCGetSchema.setContNo(newContNo);
             tLCGetSchema.setGrpContNo(newGrpContNo);
             tLCGetSchema.setPolNo(newPolNo);
             mLCGetSet.add(tLCGetSchema);
           }
           map.put(mLCGetSet, "INSERT");
           //保险帐户表
           LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
           LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();
           LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
           sqlstr = "select * from LCInsureAcc where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCInsureAccSet = tLCInsureAccDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCInsureAccSet.size(); i++) {
             LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
             tLCInsureAccSchema = tLCInsureAccSet.get(i);
             tLCInsureAccSchema.setContNo(newContNo);
             tLCInsureAccSchema.setGrpContNo(newGrpContNo);
             tLCInsureAccSchema.setGrpPolNo(newGrpPolNo);
             tLCInsureAccSchema.setPolNo(newPolNo);
             mLCInsureAccSet.add(tLCInsureAccSchema);
           }
           map.put(mLCInsureAccSet, "INSERT");
           //保险账户分类表
           LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
           LCInsureAccClassSet mLCInsureAccClassSet = new LCInsureAccClassSet();
           LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
           sqlstr = "select * from LCInsureAccClass where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCInsureAccClassSet = tLCInsureAccClassDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCInsureAccClassSet.size(); i++) {
             LCInsureAccClassSchema tLCInsureAccClassSchema = new
                 LCInsureAccClassSchema();
             tLCInsureAccClassSchema = tLCInsureAccClassSet.get(i);
             tLCInsureAccClassSchema.setContNo(newContNo);
             tLCInsureAccClassSchema.setGrpContNo(newGrpContNo);
             tLCInsureAccClassSchema.setGrpPolNo(newGrpPolNo);
             tLCInsureAccClassSchema.setPolNo(newPolNo);
             mLCInsureAccClassSet.add(tLCInsureAccClassSchema);
           }
           map.put(mLCInsureAccClassSet, "INSERT");
           //保险帐户表记价履历表
           LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
           LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
           LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
           sqlstr = "select * from LCInsureAccTrace where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++) {
             LCInsureAccTraceSchema tLCInsureAccTraceSchema = new
                 LCInsureAccTraceSchema();
             tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(i);
             tLCInsureAccTraceSchema.setContNo(newContNo);
             tLCInsureAccTraceSchema.setGrpPolNo(newGrpPolNo);
             tLCInsureAccTraceSchema.setPolNo(newPolNo);
             tLCInsureAccTraceSchema.setGrpContNo(newGrpContNo);
             mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
           }
           map.put(mLCInsureAccTraceSet, "INSERT");
           //保险帐户管理费表
           LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
           LCInsureAccFeeSet mLCInsureAccFeeSet = new LCInsureAccFeeSet();
           LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
           sqlstr = "select * from LCInsureAccFee where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCInsureAccFeeSet = tLCInsureAccFeeDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCInsureAccFeeSet.size(); i++) {
             LCInsureAccFeeSchema tLCInsureAccFeeSchema = new LCInsureAccFeeSchema();
             tLCInsureAccFeeSchema = tLCInsureAccFeeSet.get(i);
             tLCInsureAccFeeSchema.setContNo(newContNo);
             tLCInsureAccFeeSchema.setGrpPolNo(newGrpPolNo);
             tLCInsureAccFeeSchema.setPolNo(newPolNo);
             tLCInsureAccFeeSchema.setGrpContNo(newGrpContNo);
             mLCInsureAccFeeSet.add(tLCInsureAccFeeSchema);
           }
           map.put(mLCInsureAccFeeSet, "INSERT");
           //保险账户管理费分类表
           LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
           LCInsureAccClassFeeSet mLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
           LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
           sqlstr = "select * from LCInsureAccClassFee where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCInsureAccClassFeeSet.size(); i++) {
             LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = new
                 LCInsureAccClassFeeSchema();
             tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(i);
             tLCInsureAccClassFeeSchema.setContNo(newContNo);
             tLCInsureAccClassFeeSchema.setGrpContNo(newGrpContNo);
             tLCInsureAccClassFeeSchema.setGrpPolNo(newGrpPolNo);
             tLCInsureAccClassFeeSchema.setPolNo(newPolNo);
             mLCInsureAccClassFeeSet.add(tLCInsureAccClassFeeSchema);
           }
           map.put(mLCInsureAccClassFeeSet, "INSERT");
           //集体险种管理费描述表
           LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
           LCGrpFeeSet mLCGrpFeeSet = new LCGrpFeeSet();
           LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
           sqlstr = "select * from LCGrpFee where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCGrpFeeSet = tLCGrpFeeDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCGrpFeeSet.size(); i++) {
             LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
             tLCGrpFeeSchema = tLCGrpFeeSet.get(i);
             //tLCGrpFeeSchema.setContNo(newContNo);
             tLCGrpFeeSchema.setGrpContNo(newGrpContNo);
             tLCGrpFeeSchema.setGrpPolNo(newGrpPolNo);
             mLCGrpFeeSet.add(tLCGrpFeeSchema);
           }
           map.put(mLCGrpFeeSet, "INSERT");
           //团单管理费计算参数
           LCGrpFeeParamSet tLCGrpFeeParamSet = new LCGrpFeeParamSet();
           LCGrpFeeParamSet mLCGrpFeeParamSet = new LCGrpFeeParamSet();
           LCGrpFeeParamDB tLCGrpFeeParamDB = new LCGrpFeeParamDB();
           sqlstr = "select * from LCGrpFeeParam where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCGrpFeeParamSet = tLCGrpFeeParamDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCGrpFeeParamSet.size(); i++) {
             LCGrpFeeParamSchema tLCGrpFeeParamSchema = new LCGrpFeeParamSchema();
             tLCGrpFeeParamSchema = tLCGrpFeeParamSet.get(i);
             //tLCGrpFeeParamSchema.setContNo(newContNo);
             tLCGrpFeeParamSchema.setGrpContNo(newGrpContNo);
             tLCGrpFeeParamSchema.setGrpPolNo(newGrpPolNo);
             mLCGrpFeeParamSet.add(tLCGrpFeeParamSchema);
           }
           map.put(mLCGrpFeeParamSet, "INSERT");
           //缴费规则定义
           LCPayRuleFactorySet tLCPayRuleFactorySet = new LCPayRuleFactorySet();
           LCPayRuleFactorySet mLCPayRuleFactorySet = new LCPayRuleFactorySet();
           LCPayRuleFactoryDB tLCPayRuleFactoryDB = new LCPayRuleFactoryDB();
           sqlstr = "select * from LCPayRuleFactory where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCPayRuleFactorySet = tLCPayRuleFactoryDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCPayRuleFactorySet.size(); i++) {
             LCPayRuleFactorySchema tLCPayRuleFactorySchema = new
                 LCPayRuleFactorySchema();
             tLCPayRuleFactorySchema = tLCPayRuleFactorySet.get(i);
             // tLCPayRuleFactorySchema.setContNo(newContNo);
             tLCPayRuleFactorySchema.setGrpContNo(newGrpContNo);
             tLCPayRuleFactorySchema.setGrpPolNo(newGrpPolNo);
             mLCPayRuleFactorySet.add(tLCPayRuleFactorySchema);
           }
           map.put(mLCPayRuleFactorySet, "INSERT");
           //缴费规则要素
           LCPayRuleParamsSet tLCPayRuleParamsSet = new LCPayRuleParamsSet();
           LCPayRuleParamsSet mLCPayRuleParamsSet = new LCPayRuleParamsSet();
           LCPayRuleParamsDB tLCPayRuleParamsDB = new LCPayRuleParamsDB();
           sqlstr = "select * from LCPayRuleParams where GrpContNo='"
               + GrpContNo + "'";
           System.out.println(sqlstr);
           tLCPayRuleParamsSet = tLCPayRuleParamsDB.executeQuery(sqlstr);
           for (int i = 1; i <= tLCPayRuleParamsSet.size(); i++) {
             LCPayRuleParamsSchema tLCPayRuleParamsSchema = new LCPayRuleParamsSchema();
             tLCPayRuleParamsSchema = tLCPayRuleParamsSet.get(i);
             // tLCPayRuleFactorySchema.setContNo(newContNo);
             tLCPayRuleParamsSchema.setGrpContNo(newGrpContNo);
             tLCPayRuleParamsSchema.setGrpPolNo(newGrpPolNo);
             mLCPayRuleParamsSet.add(tLCPayRuleParamsSchema);
           }
           map.put(mLCPayRuleParamsSet, "INSERT");

           mResult.add(map);
           return true;
         }

         public boolean updateEdorNo() {
           if (mLastEdorno.equals("")) {
             this.buildError("ChangeNo", "没有制订新批单号！");
             return false;
           }

           if (!updateAsscociateTable()) {
             this.buildError("ChangeNo", "取更新关联表失败！");
             return false;
           }
           return true;
         }

         private void buildError(String szFunc, String szErrMsg) {
           CError cError = new CError();

           cError.moduleName = "ChangNo";
           cError.functionName = szFunc;
           cError.errorMessage = szErrMsg;
           this.mErrors.addOneError(cError);
         }

         public VData getResult() {
           return mResult;
         }

         public boolean submitData(VData cInputData, String cOperate) {
           //将数据取到本类变量中
           mInputData = (VData) cInputData.clone();
           this.mOperate = cOperate;
           //将VData数据还原成业务需要的类
           if (this.getInputData() == false) {
             return false;
           }
           System.out.println("---getInputData successful---");
           //if (this.checkData() == false)
           //return false;
           System.out.println("---checkData successful---");
           if (this.updateAsscociateTable() == false) {
             return false;
           }
           System.out.println("---dealdata successful---");

           // 装配处理好的数据，准备给后台进行保存
           System.out.println("---prepareOutputData---");
           PubSubmit tPubSubmit = new PubSubmit();
           //tPubSubmit.submitData(mInputData, cOperate);
           if (tPubSubmit.submitData(mResult, cOperate) == false) {
             // @@错误处理
             this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             return false;
           }

           return true;
         }

         private boolean getInputData() {
           //全局变量实例
           tTransferData = (TransferData) mInputData.getObjectByObjectName(
               "TransferData", 0);
           return true;
         }
         public static void main(String[] args) {
           VData tVData = new VData();
           TransferData tTransferData = new TransferData();
           tTransferData.setNameAndValue("GrpContNo","140110000000374");
           tTransferData.setNameAndValue("Department1","86110000");
           tVData.addElement(tTransferData);
           ChangeNo mChangeNo=new ChangeNo();

           mChangeNo.submitData(tVData,"");

         }

       }
