package com.sinosoft.lis.tb;

import java.util.Date;
import java.util.HashMap;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class GrpSignAfterPrintBL
{
    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    /** 返回结果 */
    private VData mResult = new VData();

    /** 传入数据 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** 团体合同表 */
    private LCGrpContSchema mLCGrpContSchema;

    /** 团体下个人数据 */
    private LCContSet mLCContSet;

    /** 团体保单号 */
    private String mGrpContNo;

    /** 团体险种信息 */
    private LCGrpPolSet mLCGrpPolSet;

    /** mGlobalInput */
    private GlobalInput mGlobalInput;

    /** 暂收费 */
    private LJTempFeeSet mLJTempFeeSet;

    /** 首期交费日期 */
    private Date mFirstPayDate;

    /** 最大缴费日期 */
    private Date mMaxPayDate;

    /** 险种 */
    private LCPolSet mLCPolSet;

    /** 缴费 */
    private LCPremSet mLCPremSet;

    /** 最后递交的数据 */
    private MMap map = new MMap();

    /** 需要处理溢缴 */
    private boolean needDealOverfall = false;

    /** 交费总金额 */
    private double mPayMoney = 0.0;

    /** 总差额 */
    private double mDiff = 0.0;

    /** 交至日期缓存信息 */
    private HashMap mPayToDate = new HashMap();

    /** 终交日期 */
    private HashMap mPayEndDate = new HashMap();

    /** 实收号码 */
    private String mPayNo;

    /** 客户通知书号码 */
    private String mGetNoticeNo;

    public GrpSignAfterPrintBL()
    {
    }

    /**
     * submitData
     *
     * @param mInputData VData
     * @param mOperate String
     * @return boolean
     */
    public boolean submitData(VData mInputData, String mOperate)
    {
        this.mInputData = mInputData;
        this.mOperate = mOperate;
        if (!getInputData())
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, "INSERT"))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        System.out.println("开始校验前台传入的数据！");
        if (this.mLCGrpContSchema == null)
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBLGrpSignAfertPrintBL.java中"
                    + "checkData方法报错，" + "在程序67行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "前台出入的数据为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (StrTool.cTrim(this.mLCGrpContSchema.getGrpContNo()).equals(""))
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBLGrpSignAfertPrintBL.java中"
                    + "checkData方法报错，" + "在程序67行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "前台出入的数据为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            return false;
        }
        /** 封装mLCGrpContSchema */
        mLCGrpContSchema = tLCGrpContDB.getSchema();
        if (!StrTool.cTrim(this.mLCGrpContSchema.getAppFlag()).equals("9"))
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBLGrpSignAfertPrintBL.java中"
                    + "checkData方法报错，" + "在程序79行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "传入保单不是预打保单数据！";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("完成一般性校验");
        /** 完成一般性校验 */
        mGrpContNo = mLCGrpContSchema.getGrpContNo();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        mLCGrpPolSet = tLCGrpPolDB.query();
        if (mLCGrpPolSet.size() <= 0)
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBLGrpSignAfertPrintBL.java中"
                    + "checkData方法报错，" + "在程序107行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "查询团体险种失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= this.mLCGrpPolSet.size(); i++)
        {
            if (!StrTool.cTrim(mLCGrpPolSet.get(i).getAppFlag()).equals("9"))
            {
                // @@错误处理
                System.out
                        .println("GrpSignAfertPrintBLGrpSignAfertPrintBL.java中"
                                + "checkData方法报错，" + "在程序120行，Author:Yangming");
                CError tError = new CError();
                tError.moduleName = "GrpSignAfertPrintBL.java";
                tError.functionName = "checkData";
                tError.errorMessage = "险种状态不是预打保单状态！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (!checkCont())
        {
            return false;
        }
        System.out.println("完成险种校验，开始财务收费校验");
        /** 开始财务收费校验，首先要有应收数据，其次要有暂收数据，再有应收数据因该和暂收数据相同，同时也等于保费收入 */
        if (!checkFee())
        {
            return false;
        }
        return true;
    }

    /**
     * checkCont
     *
     * @return boolean
     */
    private boolean checkCont()
    {
        /** 校验险种和合同信息 */
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setGrpContNo(this.mGrpContNo);
        if (tLCContDB.getCount() <= 0)
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBL中" + "checkCont方法报错，"
                    + "在程序183行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL";
            tError.functionName = "checkCont";
            tError.errorMessage = "查询合同失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        StringBuffer sql = new StringBuffer(255);
        sql
                .append("select count(1) from lccont where appflag<>'9' and grpcontno='");
        sql.append(this.mGrpContNo);
        sql.append("'");
        int chk = Integer.parseInt((new ExeSQL()).getOneValue(sql.toString()));
        if (chk > 0)
        {
            buildError("checkCont", "预打签单失败,原因是签单标志不是9！");
            return false;
        }
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(this.mGrpContNo);
        //        this.mLCPolSet = tLCPolDB.query();
        if (tLCPolDB.getCount() <= 0)
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBL中" + "checkCont方法报错，"
                    + "在程序215行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL";
            tError.functionName = "checkCont";
            tError.errorMessage = "查询险种信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        sql = new StringBuffer(255);
        sql
                .append("select count(1) from lcpol where appflag<>'9' and grpcontno='");
        sql.append(this.mGrpContNo);
        sql.append("'");
        chk = Integer.parseInt((new ExeSQL()).getOneValue(sql.toString()));
        if (chk > 0)
        {
            buildError("checkCont", "预打签单失败,原因是签单标志不是9！");
            return false;
        }

        //        for (int i = 1; i <= mLCPolSet.size(); i++) {
        //            if (!StrTool.cTrim(mLCPolSet.get(i).getAppFlag()).equals("9")) {
        //                // @@错误处理
        //                System.out.println("GrpSignAfertPrintBL中"
        //                                   + "checkCont方法报错，"
        //                                   + "在程序228行，Author:Yangming");
        //                CError tError = new CError();
        //                tError.moduleName = "GrpSignAfertPrintBL";
        //                tError.functionName = "checkCont";
        //                tError.errorMessage = "被保险人" +
        //                                      this.mLCPolSet.get(i).getInsuredName() +
        //                                      "的险种信息存在问题！";
        //                this.mErrors.addOneError(tError);
        //                return false;
        //            }
        //        }
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setGrpContNo(this.mGrpContNo);
        //        this.mLCPremSet = tLCPremDB.query();
        if (tLCPremDB.getCount() <= 0)
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBL中" + "checkCont方法报错，"
                    + "在程序248行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL";
            tError.functionName = "checkCont";
            tError.errorMessage = "缴费项有误！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * checkFee
     *
     * @return boolean
     */
    private boolean checkFee()
    {
        /** 首先校验应收数据 */
        //        LJSPayDB tLJSPayDB = new LJSPayDB();
        //        tLJSPayDB.setOtherNo(this.mGrpContNo);
        //        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        //        if (tLJSPaySet.size() <= 0) {
        //            // @@错误处理
        //            System.out.println("GrpSignAfertPrintBLGrpSignAfertPrintBL.java中"
        //                               + "checkFee方法报错，"
        //                               + "在程序150行，Author:Yangming");
        //            CError tError = new CError();
        //            tError.moduleName = "GrpSignAfertPrintBL.java";
        //            tError.functionName = "checkFee";
        //            tError.errorMessage = "没有找到应收数据！";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        //        if (tLJSPaySet.size() > 1) {
        //            // @@错误处理
        //            System.out.println("GrpSignAfertPrintBLGrpSignAfertPrintBL.java中"
        //                               + "checkFee方法报错，"
        //                               + "在程序162行，Author:Yangming");
        //            CError tError = new CError();
        //            tError.moduleName = "GrpSignAfertPrintBL.java";
        //            tError.functionName = "checkFee";
        //            tError.errorMessage = "同一保单查询到多条应收数据！";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        //        if (this.mLCGrpContSchema.getPrem() !=
        //            tLJSPaySet.get(1).getSumDuePayMoney()) {
        //            // @@错误处理
        //            System.out.println("GrpSignAfertPrintBLGrpSignAfertPrintBL.java中"
        //                               + "checkFee方法报错，"
        //                               + "在程序174行，Author:Yangming");
        //            CError tError = new CError();
        //            tError.moduleName = "GrpSignAfertPrintBL.java";
        //            tError.functionName = "checkFee";
        //            tError.errorMessage = "应收钱数与保单保费不符，请联系开发人员！";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        System.out.println("开始校验暂收费");
        /** 暂收费校验 */
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setOtherNo(this.mLCGrpContSchema.getPrtNo());
        mLJTempFeeSet = tLJTempFeeDB
                .executeQuery("select * from ljtempfee where otherno='"
                        + this.mLCGrpContSchema.getPrtNo()
                        + "' and enteraccdate is not null and confdate is null ");
        if (mLJTempFeeSet.size() <= 0)
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBL中" + "checkFee方法报错，"
                    + "在程序192行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL.java";
            tError.functionName = "checkFee";
            tError.errorMessage = "未查询到暂收费！";
            this.mErrors.addOneError(tError);
            return false;
        }
        /** 纪录总交金额 */
        for (int i = 1; i <= mLJTempFeeSet.size(); i++)
        {
            this.mPayMoney += mLJTempFeeSet.get(i).getPayMoney();
        }
        if (mPayMoney == 0)
        {
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL";
            tError.functionName = "checkFee";
            tError.errorMessage = "交费金额为零！";
            System.out.println("程序第349行出错，" + "请检查GrpSignAfertPrintBL中的"
                    + "checkFee方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }

        if (this.mLCGrpPolSet.size() != mLJTempFeeSet.size())
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBL中" + "checkFee方法报错，"
                    + "在程序204行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL";
            tError.functionName = "checkFee";
            tError.errorMessage = "暂收费交费险种个数与险种数目不同！";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            for (int i = 1; i <= mLCGrpPolSet.size(); i++)
            {
                for (int m = 1; m <= mLJTempFeeSet.size(); m++)
                {
                    if (mLCGrpPolSet.get(i).getRiskCode().equals(
                            mLJTempFeeSet.get(m).getRiskCode())
                            && mLCGrpPolSet.get(i).getPrem() > mLJTempFeeSet
                                    .get(m).getPayMoney())
                    {
                        // @@错误处理
                        System.out.println("GrpSignAfertPrintBL中"
                                + "checkFee方法报错，" + "在程序221行，Author:Yangming");
                        CError tError = new CError();
                        tError.moduleName = "GrpSignAfertPrintBL";
                        tError.functionName = "checkFee";
                        tError.errorMessage = "险种"
                                + mLCGrpPolSet.get(i).getRiskCode() + "保费："
                                + mLCGrpPolSet.get(i).getPrem() + "<br>"
                                + "交费金额：" + mLJTempFeeSet.get(m).getPayMoney()
                                + "，金额不同！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    else if (mLCGrpPolSet.get(i).getRiskCode().equals(
                            mLJTempFeeSet.get(m).getRiskCode())
                            && mLCGrpPolSet.get(i).getPrem() < mLJTempFeeSet
                                    .get(m).getPayMoney())
                    {
                        System.out.println("需要溢缴处理！");
                        needDealOverfall = true;
                    }
                }
            }
        }
        System.out.println("完成全部的财务校验！");

        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("开始获取数据！");
        if (this.mInputData == null)
        {
            // @@错误处理
            System.out.println("GrpSignAfertPrintBL中" + "getInputData方法报错，"
                    + "在程序384行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "GrpSignAfertPrintBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入参数为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema = (LCGrpContSchema) this.mInputData
                .getObjectByObjectName("LCGrpContSchema", 0);
        mGlobalInput = (GlobalInput) this.mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        /** 开始处理财务核销 */
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (!insertData())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData()
    {
        System.out.println("开始财务核销");
        /** 为了解决大事务一次递交问题,将LJAPayPerson作为小事务,分别递交 */
        /** 首先看是否存在PayNo */
        getNo(this.mGrpContNo);
        //        tPayNo = PubFun1.CreateMaxNo("PAYNO", PubFun.getNoLimit(
        //                this.mLCGrpContSchema.getManageCom()));
        if (mPayNo == null || mPayNo.equals("") || mPayNo.indexOf("null") != -1)
        {
            buildError("insertData",
                    "生成PayNo错误,原因可能是曾经生成过没有查询到,也可能是LDMaxNo存在问题！");
            return false;
        }
        if (mGetNoticeNo == null || mGetNoticeNo.equals("")
                || mGetNoticeNo.indexOf("null") != -1)
        {
            buildError("insertData", "生成客户通知书号码失败！");
            return false;
        }
        //        String newActNoticeNo = PubFun1.CreateMaxNo("GPAYNOTICENO",
        //                mLCGrpContSchema.getPrtNo());
        /** 首期交费日期 */
        FDate fDate = new FDate();
        this.mFirstPayDate = fDate.getDate(this.mLJTempFeeSet.get(1)
                .getPayDate());
        this.mMaxPayDate = fDate
                .getDate(this.mLJTempFeeSet.get(1).getPayDate());
        double sumPrem = 0.0;
        for (int i = 1; i <= mLJTempFeeSet.size(); i++)
        {
            if (fDate.getDate(mLJTempFeeSet.get(i).getPayDate()).before(
                    this.mFirstPayDate))
            {
                mFirstPayDate = fDate
                        .getDate(mLJTempFeeSet.get(i).getPayDate());
            }
            if (fDate.getDate(mLJTempFeeSet.get(i).getPayDate()).after(
                    this.mMaxPayDate))
            {
                mMaxPayDate = fDate.getDate(mLJTempFeeSet.get(i).getPayDate());
            }
            sumPrem += mLJTempFeeSet.get(i).getPayMoney();
        }
        /** 实收总表 */
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        /** LJAPayGrp */
        LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
        /** 个人应收表 */
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        tLJAPaySchema.setPayNo(this.mPayNo);
        tLJAPaySchema.setIncomeNo(this.mGrpContNo);
        tLJAPaySchema.setIncomeType("1");
        tLJAPaySchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLJAPaySchema.setSumActuPayMoney(mLCGrpContSchema.getPrem());
        tLJAPaySchema.setPayDate(mMaxPayDate);
        tLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());
        tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
        tLJAPaySchema.setApproveCode(mGlobalInput.Operator);
        tLJAPaySchema.setApproveDate(PubFun.getCurrentDate());
        tLJAPaySchema.setSerialNo(this.mPayNo);
        tLJAPaySchema.setOperator(mGlobalInput.Operator);
        tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
        tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
        tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
        tLJAPaySchema.setStartPayDate(mFirstPayDate);
        tLJAPaySchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLJAPaySchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        tLJAPaySchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLJAPaySchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLJAPaySchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        tLJAPaySchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLJAPaySchema.setBankAccNo(mLCGrpContSchema.getBankAccNo());
        tLJAPaySchema.setBankCode(mLCGrpContSchema.getBankCode());
        tLJAPaySchema.setAccName(mLCGrpContSchema.getAccName());
        tLJAPaySchema.setPayTypeFlag(mLCGrpContSchema.getPayMode());
        tLJAPaySchema.setMarketType(mLCGrpContSchema.getMarketType());
        tLJAPaySchema.setSaleChnl(mLCGrpContSchema.getSaleChnl());
        tLJAPaySchema.setGetNoticeNo(this.mGetNoticeNo);

        // 契约新单核销实收标志。契约标志：“0”
        tLJAPaySchema.setDueFeeType("0");
        // ------------------------------

        System.out.println("完成实收总表操作!");

        // 对LCInsureAccFee、LCInsureAccFeeTrace表中OtherNo进行更新，保存值为PayNo。
        MMap tTmpMap = prepareInsureAcc(mLCGrpContSchema.getGrpContNo(),
                this.mPayNo);
        map.add(tTmpMap);
        //-------------------------------------------
//      by gzh 20111223 增加约定缴费计划明细信息
        if(mLCGrpContSchema.getPayIntv() == -1){
        	GrpPayPlanDetailBL tGrpPayPlanDetailBL = new GrpPayPlanDetailBL();
            VData tVData = new VData();
        	
        	TransferData mTransferData = new TransferData();
        	mTransferData.setNameAndValue("ProposalGrpContNo", mLCGrpContSchema.getProposalGrpContNo());
        	
        	tVData.add(mGlobalInput);
        	tVData.add(mTransferData);
            if(!tGrpPayPlanDetailBL.submitData(tVData, "")){
            	CError.buildErr(this, "拆分约定缴费计划有误", tGrpPayPlanDetailBL.mErrors);
                return false;
            }
            map.add(tGrpPayPlanDetailBL.getMMMap());
        }
//        by gzh end

        /** 开始处理LJAPayPerson */
        LCPremSet tLCPremSet = new LCPremSet();
        RSWrapper tRSWrapper = new RSWrapper();
        tRSWrapper.prepareData(tLCPremSet,
                "select * from lcprem where grpcontno='" + this.mGrpContNo
                        + "' with ur");
        do
        {
            tRSWrapper.getData();
            for (int i = 1; i <= tLCPremSet.size(); i++)
            {
                if (!dealLJAPayPerson(tLJAPayPersonSet, tLCPremSet.get(i)))
                {
                    return false;
                }
            }
        }
        while (tLCPremSet.size() > 0
                && tLCPremSet.size() == SysConst.FETCHCOUNT);
        /** 完成上述操作后,需要校验核销金额是否和保费相同,如果相同则表示核销完成 */
        if (!checkPayPerson())
        {
            return false;
        }
        /** 如果签单核销完成,操作核销日期 */
        if (!dealConfDate())
        {
            return false;
        }
        /** 完成实收总表操作 */
        for (int i = 1; i <= this.mLCGrpPolSet.size(); i++)
        {
            if (!dealLJAPayGrp(tLJAPayGrpSet, mLCGrpPolSet.get(i)))
            {
                return false;
            }
        }

        for (int i = 1; i <= mLCGrpPolSet.size(); i++)
        {
            mLCGrpPolSet.get(i).setPaytoDate(
                    (String) mPayToDate.get(mLCGrpPolSet.get(i).getRiskCode()));
            mLCGrpPolSet.get(i)
                    .setPayEndDate(
                            (String) mPayEndDate.get(mLCGrpPolSet.get(i)
                                    .getRiskCode()));
            mLCGrpPolSet.get(i).setAppFlag("1");
            mLCGrpPolSet.get(i).setStateFlag("1");
            mLCGrpPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLCGrpPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLCGrpPolSet.get(i).setFirstPayDate(fDate.getString(mFirstPayDate));
        }
        System.out.println("完成LJAPayGrp表操作");
        /** 完成LJAPayGrp表操作 */
        System.out.println("完成财务核销动作，开始处理保单状态！");
        map.put(tLJAPaySchema, "INSERT");
        map.put(tLJAPayGrpSet, "INSERT");
        System.out.println(tLJAPayGrpSet.get(1).getCurPayToDate());
        //        map.put(tLJAPayPersonSet, "INSERT");
        /** 开始保单状态，1、全部的Appflag。2、签单日期，签单机构 */
        StringBuffer sql = new StringBuffer();
        /** 更新Lcgrpcont */
        sql.append("update LCGrpCont set Appflag='1',StateFlag='1',signdate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',SignTime='");
        sql.append(PubFun.getCurrentTime());
        sql.append("',modifydate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',modifyTime='");
        sql.append(PubFun.getCurrentTime());
        if (StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals("0"))
        {
            sql.append("',CustomGetPolDate='");
            sql.append(PubFun.getCurrentDate());
            sql.append("',PrintCount=1");
            sql.append(",GetPolDate='");
            sql.append(PubFun.getCurrentDate());
            sql.append("',GetPolTime='");
            sql.append(PubFun.getCurrentTime());
        }
        sql.append("',SignCom='");
        sql.append(mGlobalInput.ManageCom);
        if (this.needDealOverfall
                && mPayMoney > this.mLCGrpContSchema.getPrem())
        {
            sql.append("',dif=");
            sql.append(mPayMoney - this.mLCGrpContSchema.getPrem());
            sql.append(" where grpcontno='");
        }
        else
        {
            sql.append("' where grpcontno='");
        }
        sql.append(this.mGrpContNo);
        sql.append("'");
        map.put(sql.toString(), "UPDATE");
        /** 更新LCCont */
        sql = new StringBuffer();
        sql.append("update LCCont set Appflag='1',StateFlag='1',signdate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',SignTime='");
        sql.append(PubFun.getCurrentTime());
        sql.append("',modifydate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',modifyTime='");
        sql.append(PubFun.getCurrentTime());
        sql.append("',SignCom='");
        sql.append(mGlobalInput.ManageCom);
        sql.append("',FirstPayDate='");
        sql.append(fDate.getString(mFirstPayDate));
        sql.append("' where grpcontno='");
        sql.append(this.mGrpContNo);
        sql.append("'");
        map.put(sql.toString(), "UPDATE");
        /** 更新LCPol */
        sql = new StringBuffer();
        sql.append("update LCPol set Appflag='1',StateFlag='1',signdate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',SignTime='");
        sql.append(PubFun.getCurrentTime());
        sql.append("',modifydate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',modifyTime='");
        sql.append(PubFun.getCurrentTime());
        sql.append("',SignCom='");
        sql.append(mGlobalInput.ManageCom);
        sql.append("',FirstPayDate='");
        sql.append(fDate.getString(mFirstPayDate));
        sql.append("' where grpcontno='");
        sql.append(this.mGrpContNo);
        sql.append("'");
        map.put(sql.toString(), "UPDATE");
        //        /** 更新LCGrpPol */
        map.put(this.mLCGrpPolSet, "UPDATE");
        //        sql = new StringBuffer();
        //        sql.append("update LCGrpPol set Appflag='1'");
        //        sql.append(",FirstPayDate='");
        //        sql.append(fDate.getString(mFirstPayDate));
        //        sql.append("' where grpcontno='");
        //        sql.append(this.mGrpContNo);
        //        sql.append("'");
        //        map.put(sql.toString(), "UPDATE");
        /** 更新LJTempfee */
        sql = new StringBuffer();
        sql.append("update LJTempFee set ConfFlag='1',ConfDate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',ModifyTime='");
        sql.append(PubFun.getCurrentTime());
        sql.append("',ModifyDate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("', OtherNo='");
        sql.append(this.mGrpContNo);
        sql.append("', OtherNoType='7' where OtherNo='");
        sql.append(this.mLCGrpContSchema.getPrtNo());
        sql.append("' and ConfFlag='0' and ConfDate is null ");
        map.put(sql.toString(), "UPDATE");
        sql = new StringBuffer();
        sql.append("update ljtempfeeclass set ConfFlag='1',ConfDate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',ModifyTime='");
        sql.append(PubFun.getCurrentTime());
        sql.append("',ModifyDate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("' where tempfeeno in (select tempfeeno from ljtempfee ");
        sql.append("where otherno='");
        sql.append(this.mLCGrpContSchema.getGrpContNo());
        sql.append("')");
        map.put(sql.toString(), "UPDATE");
        return true;
    }

    /**
     * dealConfDate
     *
     * @return boolean
     */
    private boolean dealConfDate()
    {
        StringBuffer sql = new StringBuffer(255);
        sql.append("update ljapayperson set confdate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("' where grpcontno='");
        sql.append(this.mGrpContNo);
        sql.append("' and payno='");
        sql.append(this.mPayNo);
        sql.append("'");
        this.map.put(sql.toString(), "UPDATE");
        return true;
    }

    /**
     * checkPayPerson
     *
     * @return boolean
     */
    private boolean checkPayPerson()
    {
        StringBuffer sql = new StringBuffer();
        sql
                .append("select sum(SumActuPayMoney) from ljapayperson where grpcontno='");
        sql.append(this.mGrpContNo);
        sql
                .append("' union all select sum(prem) from lcprem where grpcontno='");
        sql.append(this.mGrpContNo);
        sql.append("'");
        SSRS ssrs = (new ExeSQL()).execSQL(sql.toString());
        if (ssrs == null || ssrs.getMaxRow() < 2)
        {
            buildError("checkPayPerson", "校验核销中保费失败,原因是核销过程中存在问题,请重新点击签发保单！");
            return false;
        }

        double tSumActuPayMoney = Double.parseDouble(ssrs.GetText(1, 1));
        double tPrem = Double.parseDouble(ssrs.GetText(2, 1));

        if (tPrem - tSumActuPayMoney > 0.001
                || tSumActuPayMoney - tPrem > 0.001)
        {
            buildError("checkPayPerson", "核销未完成,请重新签单！");
            return false;
        }
        return true;
    }

    /**
     * getPayNo
     *
     * @param tGrpContNo String
     * @return String
     */
    private void getNo(String tGrpContNo)
    {
        String tPayNo = null;
        String tGetNoticeNo = null;
        if (tGrpContNo == null || tGrpContNo.equals(""))
        {
            buildError("getPayNo", "传入合同号码为null！");
            return;
        }
        /** 开始校验是否已经存在实收数据 */
        StringBuffer sql = new StringBuffer(255);
        sql.append("select y.PayNo,y.GetNoticeNo from LJAPayPerson y,(");
        sql
                .append(" select PolNo a,DutyCode b,PayPlanCode c,ContNo d,GrpContNo E ");
        sql.append(" from  lcprem ");
        sql.append("where grpcontno='");
        sql.append(tGrpContNo);
        sql.append("' fetch first 1 rows only");
        sql.append(") as x");
        sql
                .append(" where x.a=y.PolNo and x.b=y.DutyCode and x.c=y.PayPlanCode and x.d=y.ContNo and y.PayAimClass='2' and y.PayType='ZC'");
        sql.append(" fetch first 1 rows only");
        /** 上述功能就是解决如果已经核销过一部分的实收数据,不另外生成PayNo */
        SSRS ssrs = (new ExeSQL()).execSQL(sql.toString());
        if (ssrs != null && ssrs.getMaxRow() > 0)
        {
            tPayNo = ssrs.GetText(1, 1);
            tGetNoticeNo = ssrs.GetText(1, 2);
        }
        if (tPayNo == null || tPayNo.equals("") || tPayNo.equals("null"))
        {
            tPayNo = PubFun1.CreateMaxNo("PAYNO", PubFun
                    .getNoLimit(this.mLCGrpContSchema.getManageCom()));
        }
        if (tGetNoticeNo == null || tGetNoticeNo.equals("")
                || tGetNoticeNo.equals("null"))
        {
            tGetNoticeNo = PubFun1.CreateMaxNo("GPAYNOTICENO", mLCGrpContSchema
                    .getPrtNo());
        }

        this.mPayNo = tPayNo;
        this.mGetNoticeNo = tGetNoticeNo;
        return;
    }

    /**
     * dealLJAPayPerson
     *
     * @param tLJAPayPersonSet LJAPayPersonSet
     * @param tLCPremSchema LCPremSchema
     * @param tPayNo String
     * @param tGetNoticeNo String
     * @return boolean
     */
    private boolean dealLJAPayPerson(LJAPayPersonSet tLJAPayPersonSet,
            LCPremSchema tLCPremSchema)
    {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tLCPremSchema.getPolNo());
        if (!tLCPolDB.getInfo())
        {
            buildError("dealLJAPayPerson", "查询险种信息失败！");
            return false;
        }

        tLCPolSchema.setSchema(tLCPolDB);
        mPayToDate.put(tLCPolSchema.getRiskCode(), tLCPolSchema.getPaytoDate());
        mPayEndDate.put(tLCPolSchema.getRiskCode(), tLCPolSchema
                .getPayEndDate());

        /** 首先校验是否曾经核销过 */
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPolNo(tLCPremSchema.getPolNo());
        tLJAPayPersonDB.setContNo(tLCPremSchema.getContNo());
        tLJAPayPersonDB.setDutyCode(tLCPremSchema.getDutyCode());
        tLJAPayPersonDB.setPayPlanCode(tLCPremSchema.getPayPlanCode());
        tLJAPayPersonDB.setPayType("ZC");
        /** 存在此实收数据 */
        if (tLJAPayPersonDB.getCount() > 0)
        {
            return true;
        }
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        for (int i = 1; i <= this.mLJTempFeeSet.size(); i++)
        {
            if (mLJTempFeeSet.get(i).getRiskCode().equals(
                    tLCPolSchema.getRiskCode()))
            {
                tLJTempFeeSchema = mLJTempFeeSet.get(i);
                break;
            }
        }
        //        String tSerialNo = PubFun1.CreateMaxNo("LJAPayPerson", 20);
        LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
        tLJAPayPersonSchema.setPolNo(tLCPremSchema.getPolNo());
        tLJAPayPersonSchema.setPayCount(1);
        tLJAPayPersonSchema.setGrpContNo(tLCPremSchema.getGrpContNo());
        tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        tLJAPayPersonSchema.setContNo(tLCPremSchema.getContNo());
        tLJAPayPersonSchema.setManageCom(tLCPremSchema.getManageCom());
        tLJAPayPersonSchema.setAgentCom(this.mLCGrpContSchema.getAgentCom());
        tLJAPayPersonSchema.setAgentType(this.mLCGrpContSchema.getAgentType());
        tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
        tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
        tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
        //        tLJAPayPersonSchema.setPayTypeFlag();
        tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
        tLJAPayPersonSchema.setPayNo(this.mPayNo);
        tLJAPayPersonSchema.setPayAimClass("2");
        tLJAPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
        tLJAPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
        tLJAPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem());
        tLJAPayPersonSchema.setSumActuPayMoney(tLCPremSchema.getPrem());
        tLJAPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());
        tLJAPayPersonSchema.setPayDate(this.mMaxPayDate);
        tLJAPayPersonSchema.setPayType("ZC");
        tLJAPayPersonSchema.setEnterAccDate(tLJTempFeeSchema.getEnterAccDate());
        //        tLJAPayPersonSchema.setConfDate(PubFun.getCurrentDate());
        tLJAPayPersonSchema.setLastPayToDate(this.mLCGrpContSchema
                .getCValiDate());
        tLJAPayPersonSchema.setCurPayToDate(tLCPremSchema.getPaytoDate());
        tLJAPayPersonSchema.setInInsuAccState("0");
        tLJAPayPersonSchema.setPayIntv(mLCGrpContSchema.getPayIntv());
        tLJAPayPersonSchema.setApproveCode(mLCGrpContSchema.getApproveCode());
        tLJAPayPersonSchema.setApproveDate(mLCGrpContSchema.getApproveDate());
        tLJAPayPersonSchema.setApproveTime(mLCGrpContSchema.getApproveTime());
        //        tLJAPayPersonSchema.setSerialNo(tSerialNo);
        tLJAPayPersonSchema.setOperator(this.mGlobalInput.Operator);
        tLJAPayPersonSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAPayPersonSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAPayPersonSchema.setGetNoticeNo(this.mGetNoticeNo);
        tLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());
        MMap map = new MMap();
        map.put(tLJAPayPersonSchema, "INSERT");
        VData tResult = new VData();
        tResult.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tResult, null))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        //        tLJAPayPersonSet.add(tLJAPayPersonSchema);
        return true;
    }

    /**
     * dealLJAPayGrp
     *
     * @param tLJAPayGrpSet LJAPayGrpSet
     * @param tLCGrpPolSchema LCGrpPolSchema
     * @param tPayNo String
     * @param tGetNoticeNo String
     * @return boolean
     */
    private boolean dealLJAPayGrp(LJAPayGrpSet tLJAPayGrpSet,
            LCGrpPolSchema tLCGrpPolSchema)
    {
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        for (int i = 1; i <= this.mLJTempFeeSet.size(); i++)
        {
            if (tLCGrpPolSchema.getRiskCode().equals(
                    mLJTempFeeSet.get(i).getRiskCode()))
            {
                tLJTempFeeSchema = mLJTempFeeSet.get(i).getSchema();
                break;
            }
        }
        LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
        tLJAPayGrpSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
        tLJAPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
        tLJAPayGrpSchema.setSumActuPayMoney(tLCGrpPolSchema.getPrem());
        tLJAPayGrpSchema.setSumDuePayMoney(tLCGrpPolSchema.getPrem());
        tLJAPayGrpSchema.setPayType("ZC");
        tLJAPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
        tLJAPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
        tLJAPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
        tLJAPayGrpSchema.setAppntNo(tLCGrpPolSchema.getCustomerNo());
        tLJAPayGrpSchema.setApproveCode(mGlobalInput.Operator);
        tLJAPayGrpSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
        tLJAPayGrpSchema.setPayCount(1);
        tLJAPayGrpSchema.setPayNo(this.mPayNo);
        tLJAPayGrpSchema.setPayDate(tLJTempFeeSchema.getPayDate());
        tLJAPayGrpSchema.setEnterAccDate(tLJTempFeeSchema.getEnterAccDate());
        tLJAPayGrpSchema.setLastPayToDate(mLCGrpContSchema.getCValiDate());
        //下一次交费日期
        tLJAPayGrpSchema.setCurPayToDate((String) mPayToDate
                .get(tLCGrpPolSchema.getRiskCode()));
        System.out.println(tLJAPayGrpSchema.getCurPayToDate());
        tLJAPayGrpSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
        tLJAPayGrpSchema.setConfDate(PubFun.getCurrentDate());
        tLJAPayGrpSchema.setGrpContNo(this.mGrpContNo);
        tLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
        tLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
        tLJAPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
        tLJAPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
        tLJAPayGrpSchema.setGetNoticeNo(this.mGetNoticeNo);
        tLJAPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
        if (this.needDealOverfall
                && (tLJTempFeeSchema.getPayMoney() - tLCGrpPolSchema.getPrem()) > 0)
        {
            System.out.println("开始处理溢缴数据！");
            LJAPayGrpSchema overLJAPayGrpSchema = new LJAPayGrpSchema();
            overLJAPayGrpSchema.setSchema(tLJAPayGrpSchema);
            overLJAPayGrpSchema.setSumActuPayMoney(tLJTempFeeSchema
                    .getPayMoney()
                    - tLCGrpPolSchema.getPrem());
            //            tLJAPayGrpSchema.setSumDuePayMoney(tLJTempFeeSchema.getPayMoney() -
            //                                               tLCGrpPolSchema.getPrem());
            overLJAPayGrpSchema.setPayType("YET");
            tLJAPayGrpSet.add(overLJAPayGrpSchema);
        }
        tLJAPayGrpSet.add(tLJAPayGrpSchema);
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        if (map.size() > 0)
        {
            this.mResult.add(map);
        }
        return true;
    }

    /**
     * getResult
     *
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000036901");
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86110000";
        tGlobalInput.Operator = "001";
        tGlobalInput.ComCode = "86110000";
        VData tVData = new VData();
        tVData.add(tLCGrpContSchema);
        tVData.add(tGlobalInput);
        StringBuffer sql = new StringBuffer();
        sql.append("update ljtempfeeclass set ConfFlag='1',ConfDate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("',ModifyTime='");
        sql.append(PubFun.getCurrentTime());
        sql.append("',ModifyDate='");
        sql.append(PubFun.getCurrentDate());
        sql.append("' where tempfeeno in (select tempfeeno from ljtempfee ");
        sql.append("where otherno='");
        sql.append(tLCGrpContSchema.getGrpContNo());
        sql.append("')");
        System.out.println(sql.toString());
        GrpSignAfterPrintBL tGrpSignAfterPrintBL = new GrpSignAfterPrintBL();
        if (!tGrpSignAfterPrintBL.submitData(tVData, "INSERT||MAIN"))
        {
            System.out.println(tGrpSignAfterPrintBL.mErrors.getContent());
        }
    }

    private MMap prepareInsureAcc(String cGrpContNo, String cPayNo)
    {
        MMap tTmpMap = new MMap();
        String[] tInsuAccTableNames = { "LCInsureAccClassFee",
                "LCInsureAccFeeTrace", "LCInsureAccClass", "LCInsureAccTrace" };

        for (int i = 0; i < tInsuAccTableNames.length; i++)
        {
            String tStrSql = " update " + tInsuAccTableNames[i]
                    + " set OtherNo = '" + cPayNo + "' "
                    + " where GrpContNo = '" + cGrpContNo
                    + "' and OtherType = '1' ";
            tTmpMap.put(tStrSql, SysConst.UPDATE);
        }

        return tTmpMap;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpSignAfterPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
