package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.BPOLCAppntDB;
import com.sinosoft.lis.db.LDNationDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.BPOLCInsuredSchema;
import com.sinosoft.lis.schema.LCExtendSchema;
import com.sinosoft.lis.vschema.BPOLCAppntSet;
import com.sinosoft.lis.vschema.BPOLCBnfSet;
import com.sinosoft.lis.vschema.BPOLCImpartSet;
import com.sinosoft.lis.vschema.BPOLCNationSet;
import com.sinosoft.lis.vschema.BPOLCPolSet;
import com.sinosoft.lis.vschema.BPOLCRiskDutyWrapSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOInsuredBL
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private BPOLCInsuredSchema mBPOLCInsuredSchema = null;
    private BPOLCNationSet mBPOLCNationSet = null;
    private BPOLCRiskDutyWrapSet mBPOLCRiskDutyWrapSet = null;
    private BPOLCPolSet mBPOLCPolSet = null;
    private BPOLCImpartSet mBPOLCImpartSet = null;
    private BPOLCBnfSet mBPOLCBnfSet = null;
    private LCExtendSchema mLCExtendSchema = null;

    private VData mVData = new VData();
    private String mOperate = null;
    private String subType = null; //扫描类型
    private Reflections ref = new Reflections();

    private MMap map = new MMap();

    public BPOInsuredBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     *
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if(getSubmitData(cInputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CrmPolicyChangeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getSubmitData
     *
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return boolean：true提交成功, false提交失败
     */
    private MMap getSubmitData(VData cInputData, String operate)
    {
        mOperate = operate;
        mVData = cInputData;

        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        if(mGI == null)
        {
            CError tError = new CError();
            tError.moduleName = "BPOInsuredBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有接收到操作员信息，可能已超时";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if(mBPOLCInsuredSchema == null || mBPOLCInsuredSchema.getBPOBatchNo() == null
           || mBPOLCInsuredSchema.getBPOBatchNo().equals("")
           || mBPOLCInsuredSchema.getContID() == null
           || mBPOLCInsuredSchema.getContID().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "BPOInsuredBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有传入被保人信息关键字";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        //TB05银行保险有抵达国家信息和套餐信息，但没有险种信息和告知信息
        if(subType != null && subType.equals("TB05"))
        {
            if(mBPOLCRiskDutyWrapSet == null || mBPOLCRiskDutyWrapSet.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "BPOInsuredBL";
                tError.functionName = "checkData";
                tError.errorMessage = "没有传入套餐信息";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        }
        else
        {
            if(mBPOLCPolSet == null || mBPOLCPolSet.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "BPOInsuredBL";
                tError.functionName = "checkData";
                tError.errorMessage = "没有传入险种信息";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            if(mBPOLCImpartSet == null || mBPOLCImpartSet.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "BPOInsuredBL";
                tError.functionName = "checkData";
                tError.errorMessage = "请录入客户告知信息";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        }

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        if(SysConst.UPDATE.equals(mOperate))
        {
            if(!dealInsured())
            {
                return false;
            }

            //TB05银行保险有抵达国家信息和套餐信息，但没有险种信息和告知信息
            if(subType != null && subType.equals("TB05"))
            {
                if(!dealNation())
                {
                    return false;
                }

                if(!dealWrap())
                {
                    return false;
                }
            }
            else
            {
                if(!dealPol())
                {
                    return false;
                }

                if(!dealImpart())
                {
                    return false;
                }
            }

            if(!dealBnf())
            {
                return false;
            }

            //备份数据
            if(!backupDate())
            {
            	return false;
            }
            
            if(!delExtendInfo()){
            	return false;
            }
        }

        return true;
    }

    /**
     * dealImpart
     * 处理客户告知
     * @return boolean
     */
    private boolean dealBnf()
    {
        BPOLCBnfSet set = (BPOLCBnfSet)mVData
                          .getObjectByObjectName("BPOLCBnfSet", 0);
        if(set == null || set.size() == 0)
        {
            return true;
        }

        BPOBnfBL tBPOBnfBL = new BPOBnfBL();
        tBPOBnfBL.setmDealType(tBPOBnfBL.ONE_INSURED_POL);
        MMap tMMap = tBPOBnfBL.getSubmitMap(mVData, mOperate);
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tBPOBnfBL.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * dealImpart
     * 处理客户告知
     * @return boolean
     */
    private boolean dealImpart()
    {
        BPOImpartBL tBPOImpartBL = new BPOImpartBL();
        MMap tMMap = tBPOImpartBL.getSubmitMap(mVData, mOperate);
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tBPOImpartBL.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * dealPol
     *
     * @return boolean
     */
    private boolean dealPol()
    {
        BPOPolBL tBPOPolBL = new BPOPolBL();
        tBPOPolBL.setmDealType(BPOPolBL.ONE_INSURED_POL);
        MMap tMMap = tBPOPolBL.getSubmitMap(mVData, mOperate);
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tBPOPolBL.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * dealInsured
     * 处理被保人信息
     * @return boolean
     */
    private boolean dealInsured()
    {
        mBPOLCInsuredSchema.setOperator(mGI.Operator);
        mBPOLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
        mBPOLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(this.mBPOLCInsuredSchema, SysConst.UPDATE);

        if(!dealAppnt())
        {
            return false;
        }

        return true;
    }

    /**
     * dealAppnt
     * 如被保人是投保人，则需要同时更新投保人信息
     */
    private boolean dealAppnt()
    {
        String sql = "select 1 "
                     + "from BPOLCPol a "
                     + "where a.BPOBatchNo = '"
                     + mBPOLCInsuredSchema.getBPOBatchNo() + "' "
                     + "   and a.ContID = '"
                     + mBPOLCInsuredSchema.getContID() + "' "
                     + "   and a.InsuredID = '"
                     + mBPOLCInsuredSchema.getInsuredID() + "' "
                     + "   and a.RelationToAppnt = '00' ";
        System.out.println(sql);

        ExeSQL tExeSQL = new ExeSQL();
        String temp = tExeSQL.getOneValue(sql);
        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOInsuredBL";
            tError.functionName = "dealAppnt";
            tError.errorMessage = "查询被保人是否投保人时出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if(!temp.equals(""))
        {
            BPOLCAppntDB db = new BPOLCAppntDB();
            db.setBPOBatchNo(mBPOLCInsuredSchema.getBPOBatchNo());
            db.setContID(mBPOLCInsuredSchema.getContID());
            BPOLCAppntSet set = db.query();
            if(set.size() > 0)
            {
                ref.transFields(set.get(1), mBPOLCInsuredSchema);
                set.get(1).setModifyDate(PubFun.getCurrentDate());
                set.get(1).setModifyTime(PubFun.getCurrentTime());
                map.put(set.get(1), SysConst.UPDATE);
            }
        }

        return true;
    }

    /**
     * dealNation
     * 处理抵达国家信息
     * @param args
     */
    private boolean dealNation()
    {
        //抵达国家
        LDNationDB tLDNationDB = new LDNationDB();
        for (int i = 1; i <= this.mBPOLCNationSet.size(); i++)
        {
            if (StrTool.cTrim(mBPOLCNationSet.get(i).getNationNo()).equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "dealDate";
                tError.errorMessage = "没有国家代码！";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
            tLDNationDB.setNationNo(mBPOLCNationSet.get(i).getNationNo());
            if (!tLDNationDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "ContInsuredIntlBL";
                tError.functionName = "dealNation";
                tError.errorMessage = "没有找到国家代码对应的国家名称！";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
            mBPOLCNationSet.get(i).setOperator(this.mGI.Operator);
        }
        PubFun.fillDefaultField(mBPOLCNationSet);

        if(SysConst.UPDATE.equals(this.mOperate))
        {
        	map.put("delete from BPOLCNation where BPOBatchNo='" + mBPOLCInsuredSchema.getBPOBatchNo()
        			+ "' and ContID = '" + mBPOLCInsuredSchema.getContID() + "'", "DELETE");
        	map.put(mBPOLCNationSet, "INSERT");
        }
        return true;
    }

    /**
     * dealWrap
     * 处理套餐信息
     * @param args
     */
    private boolean dealWrap() {
        if(SysConst.UPDATE.equals(this.mOperate))
        {
        	map.put("delete from BPOLCRiskDutyWrap where BPOBatchNo='" + mBPOLCInsuredSchema.getBPOBatchNo()
        			+ "' and ContID = '" + mBPOLCInsuredSchema.getContID() + "'", "DELETE");
        	map.put(mBPOLCRiskDutyWrapSet, "INSERT");
        }
        PubFun.fillDefaultField(mBPOLCRiskDutyWrapSet);
		return true;
	}

    /**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 *
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mBPOLCInsuredSchema = (BPOLCInsuredSchema) data
                            .getObjectByObjectName("BPOLCInsuredSchema", 0);
        mBPOLCNationSet = (BPOLCNationSet) data.getObjectByObjectName("BPOLCNationSet", 0);
        mBPOLCRiskDutyWrapSet = (BPOLCRiskDutyWrapSet) data.getObjectByObjectName("BPOLCRiskDutyWrapSet", 0);
        mBPOLCPolSet = (BPOLCPolSet) data.getObjectByObjectName("BPOLCPolSet", 0);
        mBPOLCImpartSet = (BPOLCImpartSet) data.getObjectByObjectName("BPOLCImpartSet", 0);
        mBPOLCBnfSet = (BPOLCBnfSet) data.getObjectByObjectName("BPOLCBnfSet", 0);
        mLCExtendSchema = (LCExtendSchema) data.getObjectByObjectName("LCExtendSchema", 0);
        TransferData mTransferData = (TransferData) data.getObjectByObjectName("TransferData", 0);
        subType = (String) mTransferData.getValueByName("SubType");

        return true;
    }

    /**
     * backupDate
     * 将数据备份到轨迹表
     * @param args
     */
    private boolean backupDate()
    {
    	String sql = "select distinct a.BPOBatchNo, a.ContID from BPOLCAppnt a where BPOBatchNo = '"
        		+ mBPOLCInsuredSchema.getBPOBatchNo() + "' and ContID = '" + mBPOLCInsuredSchema.getContID() + "'";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);

        String[] tBPOContTables = { "BPOLCAppnt", "BPOLCInsured", "BPOLCPol",
                "BPOLCBnf", "BPOLCImpart", "BPOIssue", "BPOLCNation", "BPOLCRiskDutyWrap" };
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            for (int index = 0; index < tBPOContTables.length; index++)
            {
                sql = "insert into " + tBPOContTables[index] + "B "
                        + "(select (select count(1) + 1 from " + tBPOContTables[index] + "B "
                        + " where BPOBatchNo = a.BPOBatchNo and ContID = a.ContID), a.* from "
                        + tBPOContTables[index] + " a where BPOBatchNo = '" + tSSRS.GetText(i, 1)
                        + "' and ContID = '" + tSSRS.GetText(i, 2) + "') ";
                map.put(sql, SysConst.INSERT);
            }
        }
        return true;
    }
    
	/**
	 * 处理综合开拓数据。
	 * 
	 * @param cGrpContNo
	 * @return
	 */
	private boolean delExtendInfo() {
		MMap tMMap = null;

		if(this.mLCExtendSchema == null) {
			return true;
		}
		LCAssistSalechnlBL tAssistSalechnlBL = new LCAssistSalechnlBL();

		VData tVData = new VData();
		tVData.add(mGI);
		tVData.add(mLCExtendSchema);

		tMMap = tAssistSalechnlBL.submitData(tVData, "");

		if (tMMap == null) {
			CError tError = new CError();
			tError.moduleName = "ContBL";
			tError.functionName = "delExtendInfo";
			tError.errorMessage = "综合开拓数据处理失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		map.add(tMMap);
		return true;
	}

    public static void main(String[] args)
    {
        BPOInsuredBL bl = new BPOInsuredBL();
    }
}
