/*
 * <p>ClassName: OES_DOC_MAINUI </p>
 * <p>Description: ES_DOC_MAINUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: EasyScan
 * @CreateDate：2002-11-27
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.onetable.*;
import com.sinosoft.lis.pubfun.*;

public class ModifyStateES_DOC_MAINUI {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData =new VData();
/** 数据操作字符串 */
private String mOperate;
//业务处理相关变量

public ModifyStateES_DOC_MAINUI ()
{
}
/**
传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中
  this.mOperate =cOperate;
  this.mInputData = cInputData;

  ModifyStateES_DOC_MAINBL tModifyStateES_DOC_MAINBL=new ModifyStateES_DOC_MAINBL();
  System.out.println("Start OES_DOC_MAIN UI Submit...");
  tModifyStateES_DOC_MAINBL.submitData(mInputData,mOperate);
  System.out.println("End OES_DOC_MAIN UI Submit...");
  //如果有需要处理的错误，则返回
  if (tModifyStateES_DOC_MAINBL.mErrors .needDealError() )
  {
    // @@错误处理
    this.mErrors.copyAllErrors(tModifyStateES_DOC_MAINBL.mErrors);
    CError tError = new CError();
    tError.moduleName = "OES_DOC_MAINUI";
    tError.functionName = "submitData";
    tError.errorMessage = "数据提交失败!";
    this.mErrors .addOneError(tError) ;
    return false;
  }
  if (mOperate.equals("QUERY||MAIN"))
  {
    this.mResult.clear();
    this.mResult=tModifyStateES_DOC_MAINBL.getResult();
  }
  mInputData=null;
  return true;
}

/**
 * 根据前面的输入数据，进行UI逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean dealData()
{
  boolean tReturn =false;
  //此处增加一些校验代码
  tReturn=true;
  return tReturn ;
}

public VData getResult()
{
  return this.mResult;
}

public static void main(String[] args) {
  ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
//  tES_DOC_MAINSchema.setDOC_CODE("861120020300000011");

  VData tVData = new VData();
	tVData.add(tES_DOC_MAINSchema);

  ModifyStateES_DOC_MAINUI tModifyStateES_DOC_MAINUI = new ModifyStateES_DOC_MAINUI();
  tModifyStateES_DOC_MAINUI.submitData(tVData, "UPDATE||MAIN");

}
}
