package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCIllnessInsuredListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCIllnessInsuredListSchema;
import com.sinosoft.lis.vschema.LCIllnessInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;


public class SIUpdateInsuredBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /**提交数据的容器*/
    private MMap map = new MMap();

    /**公共输入信息*/
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;


    /**传入的规则*/
   
    private LCIllnessInsuredListSchema mLCIllnessInsuredListSchema = new LCIllnessInsuredListSchema();
    
    private LCIllnessInsuredListSet mLCIllnessInsuredListSet = new LCIllnessInsuredListSet();
    
    public SIUpdateInsuredBL()
    {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!dealData())
        {
            return false; 
        }

        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, ""))
        {
            buildError("submitData", "提交数据时出错");
            return false;
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
        	this.mLCIllnessInsuredListSet = (LCIllnessInsuredListSet) cInputData.getObjectByObjectName(
    				"LCIllnessInsuredListSet", 0);
            this.mLCIllnessInsuredListSchema =
                    (LCIllnessInsuredListSchema) cInputData.
                    getObjectByObjectName("LCIllnessInsuredListSchema", 0);
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
        }
        catch(Exception e)
        {
            buildError("getInputData", "传入的数据不完整" + e);
            System.out.println("传入的数据不完整");
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {	String mSql= "";
        try
        {
        	String tSQL = "select cvalidate from lcgrpcont where prtno = '"+mLCIllnessInsuredListSchema.getPrtNo()+"' ";
        	String tCValiDate = new ExeSQL().getOneValue(tSQL);
//        	if("".equals(StrTool.cTrim(tCValiDate))){
//        		buildError("dealdata", "获取待处理保单生效日期失败！");
//                return false;
//        	}
        	//int tAppAge = PubFun.getInsuredAppAge(tCValiDate, mLCIllnessInsuredListSchema.getBirthday());
        	String tExeOperate = "";
        	
        	if("INSERT||MAIN".equals(mOperate)){
            	tSQL = "select * from LCIllnessInsuredList where seqno = '"+mLCIllnessInsuredListSchema.getSeqNo()+"' ";
            	LCIllnessInsuredListDB tLCIllnessInsuredListDB = new LCIllnessInsuredListDB();
            	LCIllnessInsuredListSet tLCIllnessInsuredListSet = tLCIllnessInsuredListDB.executeQuery(tSQL);
            	if(tLCIllnessInsuredListSet != null && tLCIllnessInsuredListSet.size() > 0){
            		buildError("dealdata", "该被保人信息已经存在，请进行修改！");
                    return false;
            	}
        		tExeOperate = SysConst.INSERT;
        		tSQL = "select batchno from LCGrpSubInsuredImport where prtno = '"+mLCIllnessInsuredListSchema.getPrtNo()+"' ";
        		String tSeqNo = PubFun1.CreateMaxNo("BIGGRP", 20);
        		mLCIllnessInsuredListSchema.setSeqNo(tSeqNo);
        		//mLCIllnessInsuredListSchema.setInsuredAppAge(tAppAge);
        		mLCIllnessInsuredListSchema.setStateFlag("0");
        		mLCIllnessInsuredListSchema.setDataChkFlag("00");
        		mLCIllnessInsuredListSchema.setAppFlag("0");
        		mLCIllnessInsuredListSchema.setOperator(mGlobalInput.Operator);
        		mLCIllnessInsuredListSchema.setMakeDate(PubFun.getCurrentDate());
        		mLCIllnessInsuredListSchema.setMakeTime(PubFun.getCurrentTime());
        		mLCIllnessInsuredListSchema.setModifyDate(PubFun.getCurrentDate());
        		mLCIllnessInsuredListSchema.setModifyTime(PubFun.getCurrentTime());
        		mLCIllnessInsuredListSchema.setCalState("00");
        		mLCIllnessInsuredListSchema.setBatchNo(mLCIllnessInsuredListSchema.getPrtNo()+"_"+mGlobalInput.Operator);
        		
        		map.put(mLCIllnessInsuredListSchema, tExeOperate);
        		
            }else if("UPDATE||MAIN".equals(mOperate)){
            	tExeOperate = SysConst.UPDATE;
            	tSQL = "select * from LCIllnessInsuredList where seqno = '"+mLCIllnessInsuredListSchema.getSeqNo()+"' ";
            	System.out.println(tSQL);
            	LCIllnessInsuredListDB tLCIllnessInsuredListDB = new LCIllnessInsuredListDB();
            	LCIllnessInsuredListSet tLCIllnessInsuredListSet = tLCIllnessInsuredListDB.executeQuery(tSQL);
            	if(tLCIllnessInsuredListSet == null || tLCIllnessInsuredListSet.size() != 1){
            		buildError("dealdata", "获取待处理被保人信息失败！");
                    return false;
            	}
            	//mLCIllnessInsuredListSchema.setInsuredAppAge(tAppAge);
            	mLCIllnessInsuredListSchema.setStateFlag("0");
            	mLCIllnessInsuredListSchema.setDataChkFlag("00");
            	mLCIllnessInsuredListSchema.setAppFlag("0");
            	mLCIllnessInsuredListSchema.setCalState("00");
            	mLCIllnessInsuredListSchema.setOperator(mGlobalInput.Operator);
            	mLCIllnessInsuredListSchema.setMakeDate(tLCIllnessInsuredListSet.get(1).getMakeDate());
        		mLCIllnessInsuredListSchema.setMakeTime(tLCIllnessInsuredListSet.get(1).getMakeTime());
            	mLCIllnessInsuredListSchema.setModifyDate(PubFun.getCurrentDate());
        		mLCIllnessInsuredListSchema.setModifyTime(PubFun.getCurrentTime());
        		map.put(mLCIllnessInsuredListSchema, tExeOperate);
            	
            }else if("DELETE||MAIN".equals(mOperate)){
            	int cCont = mLCIllnessInsuredListSet.size();
        		LCIllnessInsuredListSet tLCIllnessInsuredListSet = new LCIllnessInsuredListSet();
        		for (int i = 1; i <=cCont; i++) {
//        			LCIllnessInsuredListDB tLCIllnessInsuredListDB = new LCIllnessInsuredListDB();
//        			tLCIllnessInsuredListDB.setSeqNo(mLCIllnessInsuredListSet.get(i).getSeqNo());
        			tSQL = "select * from LCIllnessInsuredList where seqno = '"+mLCIllnessInsuredListSet.get(i).getSeqNo()+"' ";
                	System.out.println(tSQL);
                	LCIllnessInsuredListDB tLCIllnessInsuredListDB = new LCIllnessInsuredListDB();
                	LCIllnessInsuredListSet tempLCIllnessInsuredListSet = tLCIllnessInsuredListDB.executeQuery(tSQL);
                	if(tempLCIllnessInsuredListSet == null || tempLCIllnessInsuredListSet.size() != 1){
                		buildError("dealdata", "获取待处理被保人信息失败！");
                        return false;
                	}
        			LCIllnessInsuredListSchema tLCIllnessInsuredListSchema = tempLCIllnessInsuredListSet.get(1);
        			
        			tLCIllnessInsuredListSet.add(tLCIllnessInsuredListSchema);
        		}
        		mLCIllnessInsuredListSet = tLCIllnessInsuredListSet;
        		tExeOperate = SysConst.DELETE;
        		map.put(mLCIllnessInsuredListSet, tExeOperate);
            	
            }else if("DELETEALL".equals(mOperate)){
            	/*
            	tSQL = "select * from LCIllnessInsuredList where grpcontno = '"+mLCIllnessInsuredListSchema.getPrtNo()+"' ";
            	System.out.println(tSQL);
            	LCIllnessInsuredListDB tLCIllnessInsuredListDB = new LCIllnessInsuredListDB();
            	LCIllnessInsuredListSet tLCIllnessInsuredListSet = tLCIllnessInsuredListDB.executeQuery(tSQL);
            	if(tLCIllnessInsuredListSet == null || tLCIllnessInsuredListSet.size() < 0){
            		buildError("dealdata", "获取待处理被保人信息失败！");
                    return false;
            	}
            	*/
            	 mSql = "delete from LCIllnessInsuredList where grpcontno = '"+mLCIllnessInsuredListSchema.getPrtNo()+"' ";
            	tExeOperate = SysConst.DELETE;
            	map.put(mSql, tExeOperate);
            }
        	
        	
        }
        catch (Exception e)
        {
            buildError("submitData", "处理数据出错" + e);
            System.out.println("Catch Exception in submitData");
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch(Exception e)
        {
            buildError("prepareOutputData", "准备输往后台数据时出错");
            System.out.println("prepareOutputData, 准备输往后台数据时出错");
        }

        return true;
    }

    private boolean prepareOutputData1() {
		int cCont = mLCIllnessInsuredListSet.size();
		LCIllnessInsuredListSet tLCIllnessInsuredListSet = new LCIllnessInsuredListSet();
		for (int i = 1; i <=cCont; i++) {
			LCIllnessInsuredListDB tLCIllnessInsuredListDB = new LCIllnessInsuredListDB();
			tLCIllnessInsuredListDB.setSeqNo(mLCIllnessInsuredListSet.get(i).getSeqNo());
			LCIllnessInsuredListSchema tLCIllnessInsuredListSchema = tLCIllnessInsuredListDB.query()
					.get(1);
			
			tLCIllnessInsuredListSet.add(tLCIllnessInsuredListSchema);
		}
		mLCIllnessInsuredListSet = tLCIllnessInsuredListSet;
		return true;

	}
    

    /**生成错误信息*/
    private void buildError(String funcName, String errMes)
    {
        CError tError = new CError();

        tError.moduleName = "LGAutoDeliver";
        tError.functionName = funcName;
        tError.errorMessage = errMes;

        this.mErrors.addOneError(tError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }


    private void jbInit() throws Exception {
    }

}


