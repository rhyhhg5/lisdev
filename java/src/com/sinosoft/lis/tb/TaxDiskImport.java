package com.sinosoft.lis.tb;

import java.util.HashMap;

import com.sinosoft.lis.pubfun.diskimport.MultiSheetImporter;
import com.sinosoft.lis.vschema.LSInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;

public class TaxDiskImport {
	public TaxDiskImport(){}
	
	/** 从xls文件读入Sheet的开始行 */
	private static final int STARTROW = 2;
	/** 使用默认的导入方法 */
	private MultiSheetImporter importer;
	/** 错误处理 */
	public CErrors mErrors = new CErrors();
	/** Sheet Name */
	private String[] mSheetName;
	/** Schema的名字 */
	private static final String[] mTableNames = {"LSInsuredList"};
	
	public TaxDiskImport(String fileName,String configFileName,String[] sheetName){
		mSheetName = sheetName;
		importer = new MultiSheetImporter(fileName,configFileName,mSheetName);
	}
	
	/**
	 * 执行导入
	 * @return boolean
	 */
	public boolean doImport(){
		System.out.println("Into TaxDiskImport doImport...");
		importer.setTableName(mTableNames);
		importer.setMStartRows(STARTROW);
		if(!importer.doImport()){
			buildError("doImport","模板导入时出错");
			return false;
		}
		return true;
	}
	/**
	 * 出错处理
	 * @param szFunc String
	 * @param szErrMsg String
	 */
	private void buildError(String szFunc,String szErrMsg){
		CError cError = new CError();
		cError.moduleName = "TaxDiskImport";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	/**
	 * 得到导入数据
	 * @return Schema
	 */
	public HashMap getResult(){
		return importer.getResult();
	}
	/**
	 * 用于获取导入文件版本号
	 * @return String
	 */
	public String getVersion(){
		HashMap tHashMap = importer.getResult();
		String version = "";
		version = (String)tHashMap.get(mSheetName[1]);
		return version;
	}
	/**
	 * getSchemaSet 获取从MutiSheetImporter获得的SchemaSet
	 * @return LSInsuredListSet
	 */
	public LSInsuredListSet getSchemaSet(){
		LSInsuredListSet tLSInsuredListSet = new LSInsuredListSet();
		HashMap tHashMap = importer.getResult();
		tLSInsuredListSet.add((LSInsuredListSet)tHashMap.get(mSheetName[0]));
		return tLSInsuredListSet;
	}
}
