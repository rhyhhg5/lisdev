/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import java.util.*;

import com.sinosoft.lis.db.LCContPlanDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LMRiskAccPayDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LMRiskAccPaySchema;
import com.sinosoft.lis.schema.LMRiskFeeSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.vschema.LMRiskDutyFactorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.StrTool;

/**
 * 数据准备类
 * <p>Title: </p>
 * <p>Description: 根据操作类型不同，对数据进行校验、准备处理 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author 朱向峰
 * @version 1.0
 */
public class LCContPlanBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LCContPlanSchema mLCContPlanSchema = new LCContPlanSchema();

    private LCContPlanSet mLCContPlanSet = new LCContPlanSet();

    private LCContPlanRiskSchema mLCContPlanRiskSchema = new LCContPlanRiskSchema();

    private LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();

    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();

    private LCGrpFeeSet mLCGrpFeeSet = new LCGrpFeeSet();

    private LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();

    private String mGrpContNo = ""; //团体合同号

    private String mContPlanCode = ""; //保险计划或默认值编码

    private String mProposalGrpContNo = "";

    private String mPlanType = "";

    private String mPlanSql = "";

    private String mSql = "";

    private String mPeoples3 = "";

    private String mPeoples2 = "";

    private TransferData mTransferData;

    private String mRiskFlag;

    public LCContPlanBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        
        //数据校验
        if (!checkData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCContPlanBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            //  CError tError = new CError();
            //  tError.moduleName = "LCContPlanBL";
            //  tError.functionName = "submitData";
            //   tError.errorMessage = "数据处理失败LCContPlanBL-->dealData!";
            //   this.mErrors.addOneError(tError);
            return false;
        }
//        健享全球团体医疗保险，录入“方案编码”、“保障地区”、“特别医院给付比例”存到LCContPlanDutyParam ，每个责任存一遍。
        if (!prepareDutyParam())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LCContPlanBL Submit...");
            LCContPlanBLS tLCContPlanBLS = new LCContPlanBLS();
            tLCContPlanBLS.submitData(mInputData, cOperate);
            System.out.println("End LCContPlanBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLCContPlanBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCContPlanBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "LCContPlanBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {
        //如果是删除或修改操作，需要校验是否有被保人拥有该计划，如果有，则该计划不允许删除、修改
        if (this.mOperate.compareTo("DELETE||MAIN") == 0
                || this.mOperate.compareTo("UPDATE||MAIN") == 0)
        {
            String tSql = "";
            ExeSQL texeSQL = new ExeSQL();
            /*
             判定是计划还是默认值处理
             如果是默认值则，则只要保单下有被保人拥有险种信息后，就不可删除、修改
             如果是保险计划，则如果属于该计划的被保人拥有险种信息后，就不可删除、修改
             */
            if (this.mContPlanCode.compareTo("00") == 0)
            {
                tSql = "select count(1) from LCInsured a,LCPol b where a.PrtNo = b.PrtNo "
                        + " and a.GrpContNo = b.GrpContNo and a.ContNo = b.ContNo "
                        + " and a.InsuredNo = b.InsuredNo and a.GrpContNo = '"
                        + this.mGrpContNo + "'";
            }
            else
            {
                tSql = "select count(1) from LCInsured a,LCPol b where a.PrtNo = b.PrtNo "
                        + " and a.GrpContNo = b.GrpContNo and a.ContNo = b.ContNo "
                        + " and a.InsuredNo = b.InsuredNo and a.GrpContNo = '"
                        + this.mGrpContNo
                        + "' and a.ContPlanCode = '"
                        + this.mContPlanCode + "'";
            }
            SSRS ssrs = texeSQL.execSQL(tSql);
            if (ssrs.GetText(1, 1).compareTo("0") > 0)
            {
                CError tError = new CError();
                tError.moduleName = "LCContPlanBL";
                tError.functionName = "checkData";
                tError.errorMessage = "该团单"
                        + this.mLCContPlanDutyParamSet.get(1).getGrpContNo()
                        + "下，有被保人拥有险种信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            //            LCInsuredSet tLCInsuredSet = new LCInsuredSet();
            //            tLCInsuredDB.setContPlanCode(this.mContPlanCode);
            //            tLCInsuredDB.setGrpContNo(this.mGrpContNo);
            //            tLCInsuredSet = tLCInsuredDB.query();
            //            if (tLCInsuredSet.size() > 0)
            //            {
            //                CError tError = new CError();
            //                tError.moduleName = "LCContPlanBL";
            //                tError.functionName = "checkData";
            //                tError.errorMessage = "该团单" +
            //                                      this.mLCContPlanDutyParamSet.get(1).
            //                                      getGrpContNo() + "下，有被保人拥有该" +
            //                                      this.mLCContPlanDutyParamSet.get(1).
            //                                      getContPlanCode() + "保险计划！";
            //                this.mErrors.addOneError(tError);
            //                return false;
            //            }
        }
        else
        {
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            for (int m = 1; m <= mLCContPlanDutyParamSet.size(); m++)
            {
                //改用这样的查询方式，可以把录入细化到险种信息一层
                tLCContPlanRiskDB = new LCContPlanRiskDB();
                tLCContPlanRiskDB.setProposalGrpContNo(mLCContPlanDutyParamSet
                        .get(m).getProposalGrpContNo());
                tLCContPlanRiskDB.setMainRiskCode(mLCContPlanDutyParamSet
                        .get(m).getMainRiskCode());
                tLCContPlanRiskDB.setRiskCode(mLCContPlanDutyParamSet.get(m)
                        .getRiskCode());
                tLCContPlanRiskDB.setContPlanCode(mLCContPlanDutyParamSet
                        .get(m).getContPlanCode());
                tLCContPlanRiskDB.setPlanType(mPlanType);
                if (tLCContPlanRiskDB.getInfo())
                {
                    //如果是新增的时候，则需要出错处理
                    if (this.mOperate.compareTo("INSERT||MAIN") == 0)
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "LCContPlanBL";
                        tError.functionName = "checkData";
                        if (tLCContPlanRiskDB.getContPlanCode().compareTo("00") != 0)
                        {
                            tError.errorMessage = "该团单"
                                    + tLCContPlanRiskDB.getGrpContNo() + "保险计划"
                                    + tLCContPlanRiskDB.getContPlanCode()
                                    + "中，已存在" + tLCContPlanRiskDB.getRiskCode()
                                    + "险种信息，如果你要变动，请使用保险计划修改功能！";
                        }
                        else
                        {
                            tError.errorMessage = "该团单"
                                    + tLCContPlanRiskDB.getGrpContNo() + "已存在"
                                    + tLCContPlanRiskDB.getRiskCode()
                                    + "险种默认值信息，如果您要变动，请使用险种信息更新功能！";
                        }
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
            /**@author:Yangming 保费添加方式改变，使添加的责任不是顺序的，现将保费要素按照责任排序 */
            LCContPlanDutyParamSet LCContPlanDutyParamSetAfterSave = new LCContPlanDutyParamSet();
            LCContPlanDutyParamSet tempLCContPlanDutyParamSet2 = new LCContPlanDutyParamSet();
            tempLCContPlanDutyParamSet2.set(mLCContPlanDutyParamSet);
            System.out.println("mLCContPlanDutyParamSet :"
                    + mLCContPlanDutyParamSet.size());
            for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++)
            {

                for (int m = 1; m <= tempLCContPlanDutyParamSet2.size(); m++)
                {
                    if (mLCContPlanDutyParamSet.get(i).getDutyCode().equals(
                            tempLCContPlanDutyParamSet2.get(m).getDutyCode()))
                    {
                        LCContPlanDutyParamSetAfterSave
                                .add(tempLCContPlanDutyParamSet2.get(m)
                                        .getSchema());
                        tempLCContPlanDutyParamSet2.removeRange(m, m);
                    }
                }
            }
            mLCContPlanDutyParamSet = null;
            mLCContPlanDutyParamSet = LCContPlanDutyParamSetAfterSave;
            System.out.println("测试排序完成后 又没有问题 ：");
            for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++)
            {
                System.out.println("mLCContPlanDutyParamSet "
                        + mLCContPlanDutyParamSet.get(i).getDutyCode());
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        //新增处理，修改处理
        if (this.mOperate.compareTo("DELETE||MAIN") != 0)
        {
            String RiskCode = "";
            String MainRiskCode = "";
            String DutyCode = "";
            int k = 0;

            LMRiskDutyFactorDB tLMRiskDutyFactorDB = new LMRiskDutyFactorDB();
            LMRiskDutyFactorSet tLMRiskDutyFactorSet = new LMRiskDutyFactorSet();

            //获取InsuPlanCode
            String tInsuPlanCode = getInsuPlanCode();
            //当获取到的方案编码为F11的时候，只保存保障计划，不校验。
            if(tInsuPlanCode != null && (tInsuPlanCode.equals("F1") || tInsuPlanCode.equals("F2") 
					 					 || tInsuPlanCode.equals("F3") || tInsuPlanCode.equals("F4") 
					 					 || tInsuPlanCode.equals("F5") || tInsuPlanCode.equals("F6") 
					 					 || tInsuPlanCode.equals("F7") || tInsuPlanCode.equals("F8") 
					 					 || tInsuPlanCode.equals("F9") || tInsuPlanCode.equals("F10") 
					 					 || tInsuPlanCode.equals("F11") || tInsuPlanCode.equals("F12")) ){
            	for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++){
            		if (RiskCode.compareTo(this.mLCContPlanDutyParamSet.get(i).getRiskCode()) != 0
                        || MainRiskCode.compareTo(this.mLCContPlanDutyParamSet.get(i).getMainRiskCode()) != 0)
                    {
            			//险种更换，则责任编码初始化
                        RiskCode = this.mLCContPlanDutyParamSet.get(i)
                                .getRiskCode();
                        MainRiskCode = this.mLCContPlanDutyParamSet.get(i)
                                .getMainRiskCode();
                        DutyCode = "";

                        //准备保单险种保险计划表数据，只与险种和计划挂钩，不关心责任、要素信息
                        mLCContPlanRiskSchema = new LCContPlanRiskSchema();
                        mLCContPlanRiskSchema.setPlanType(this.mPlanType);
                        mLCContPlanRiskSchema
                                .setGrpContNo(this.mLCContPlanDutyParamSet.get(i)
                                        .getGrpContNo());
                        mLCContPlanRiskSchema
                                .setProposalGrpContNo(this.mLCContPlanDutyParamSet
                                        .get(i).getProposalGrpContNo());
                        mLCContPlanRiskSchema
                                .setRiskCode(this.mLCContPlanDutyParamSet.get(i)
                                        .getRiskCode());
                        mLCContPlanRiskSchema
                                .setRiskVersion(this.mLCContPlanDutyParamSet.get(i)
                                        .getRiskVersion());
                        mLCContPlanRiskSchema
                                .setContPlanCode(this.mLCContPlanDutyParamSet
                                        .get(i).getContPlanCode());
                        mLCContPlanRiskSchema
                                .setContPlanName(this.mLCContPlanDutyParamSet
                                        .get(i).getContPlanName());
                        mLCContPlanRiskSchema
                                .setMainRiskCode(this.mLCContPlanDutyParamSet
                                        .get(i).getMainRiskCode());
                        mLCContPlanRiskSchema
                                .setMainRiskVersion(this.mLCContPlanDutyParamSet
                                        .get(i).getMainRiskVersion());
                        mLCContPlanRiskSchema
                                .setOperator(this.mGlobalInput.Operator);
                        //                    System.out.println("测试　保险计划中ＬＣＣｏｎｔＰｌａｎＲｉｓｋ未保存问题。");
                        //                    System.out.println("mLCContPlanRiskSchema : "+mLCContPlanRiskSchema.encode());
                        /*
                         *处理总保费问题
                         *1、从前台传入tLCContPlanRiskSet,其中包含险种代码和总保费信息
                         *2、将险种代码相同项目的总保费付给后台
                         */
                        if (tLCContPlanRiskSet.size() > 0)
                        {
                            mLCContPlanRiskSchema.setRiskPrem(tLCContPlanRiskSet
                                    .get(i).getRiskPrem());
                            mLCContPlanRiskSchema.setRiskAmnt(tLCContPlanRiskSet
                                    .get(i).getRiskAmnt());
                            mLCContPlanRiskSchema
                                    .setRiskWrapFlag(tLCContPlanRiskSet.get(i)
                                            .getRiskWrapFlag());
                            mLCContPlanRiskSchema
                                    .setRiskWrapCode(tLCContPlanRiskSet.get(i)
                                            .getRiskWrapCode());
                            mLCContPlanRiskSchema.setMakeDate(PubFun
                                    .getCurrentDate());
                            mLCContPlanRiskSchema.setMakeTime(PubFun
                                    .getCurrentTime());
                            mLCContPlanRiskSchema.setModifyDate(PubFun
                                    .getCurrentDate());
                            mLCContPlanRiskSchema.setModifyTime(PubFun
                                    .getCurrentTime());
                            mLCContPlanRiskSet.add(mLCContPlanRiskSchema);
                        }
                        if (DutyCode.compareTo(this.mLCContPlanDutyParamSet.get(i)
                                .getDutyCode()) != 0)
                        {
                            //责任更换
                            DutyCode = this.mLCContPlanDutyParamSet.get(i)
                                    .getDutyCode();
                            //如果要素信息是Deductible则，需要对要素信息做一定处理
                            if (this.mLCContPlanDutyParamSet.get(i).getCalFactor()
                                    .compareTo("Deductible") == 0)
                            {
                                if (this.mLCContPlanDutyParamSet.get(i)
                                        .getCalFactorValue().compareTo("") == 0)
                                {
                                    String lssql = "select b.Deductible from LMDutyGetRela a,LMDutyGetClm b where a.GetDutyCode = b.GetDutyCode and a.DutyCode = '"
                                            + DutyCode + "'";
                                    ExeSQL exeSQL = new ExeSQL();
                                    SSRS ssrs = exeSQL.execSQL(lssql);
                                    mLCContPlanDutyParamSet.get(i)
                                            .setCalFactorValue(ssrs.GetText(1, 1));
                                }
                            }
                            //查询当前险种、当前责任下的要素个数
                            tLMRiskDutyFactorDB = new LMRiskDutyFactorDB();
                            //判定是险种信息录入，还是保障计划录入
                            if (this.mPlanType.compareTo("0") == 0)
                            {
                                mSql = "select * from LMRiskDutyFactor where RiskCode = '"
                                        + RiskCode
                                        + "' and ChooseFlag in ('0','2','3') and DutyCode = '"
                                        + DutyCode + "'";
                            }
                            else
                            {
                                mSql = "select * from LMRiskDutyFactor where RiskCode = '"
                                        + RiskCode
                                        + "' and ChooseFlag in ('1','2') and DutyCode = '"
                                        + DutyCode + "'";
                            }
                            tLMRiskDutyFactorSet = tLMRiskDutyFactorDB
                                    .executeQuery(mSql);
                        }
                    }else{
                    	if (DutyCode.compareTo(this.mLCContPlanDutyParamSet.get(i)
                                .getDutyCode()) != 0)
                        {
                    		//责任更换
                            DutyCode = this.mLCContPlanDutyParamSet.get(i)
                                    .getDutyCode();
                            if (this.mLCContPlanDutyParamSet.get(i).getCalFactor()
                                    .compareTo("Deductible") == 0)
                            {
                                if (this.mLCContPlanDutyParamSet.get(i)
                                        .getCalFactorValue().compareTo("") == 0)
                                {
                                    String lssql = "select b.Deductible from LMDutyGetRela a,LMDutyGetClm b where a.GetDutyCode = b.GetDutyCode and a.DutyCode = '"
                                            + DutyCode + "'";
                                    ExeSQL exeSQL = new ExeSQL();
                                    SSRS ssrs = exeSQL.execSQL(lssql);
                                    mLCContPlanDutyParamSet.get(i)
                                            .setCalFactorValue(ssrs.GetText(1, 1));
                                }
                                //查询当前险种、当前责任下的要素个数
                                tLMRiskDutyFactorDB = new LMRiskDutyFactorDB();
                                //判定是险种信息录入，还是保障计划录入
                                if (this.mPlanType.compareTo("0") == 0)
                                {
                                    mSql = "select * from LMRiskDutyFactor where RiskCode = '"
                                            + RiskCode
                                            + "' and ChooseFlag in ('0','2','3') and DutyCode = '"
                                            + DutyCode + "'";
                                }
                                else
                                {
                                    mSql = "select * from LMRiskDutyFactor where RiskCode = '"
                                            + RiskCode
                                            + "' and ChooseFlag in ('1','2') and DutyCode = '"
                                            + DutyCode + "'";
                                }
                                tLMRiskDutyFactorSet = tLMRiskDutyFactorDB
                                        .executeQuery(mSql);
                            }
                        }else{
                        	if (this.mLCContPlanDutyParamSet.get(i).getCalFactor()
                                    .compareTo("Deductible") == 0)
                            {
                                if (this.mLCContPlanDutyParamSet.get(i)
                                        .getCalFactorValue().compareTo("") == 0)
                                {
                                    String lssql = "select b.Deductible from LMDutyGetRela a,LMDutyGetClm b where a.GetDutyCode = b.GetDutyCode and a.DutyCode = '"
                                            + DutyCode + "'";
                                    ExeSQL exeSQL = new ExeSQL();
                                    SSRS ssrs = exeSQL.execSQL(lssql);
                                    mLCContPlanDutyParamSet.get(i)
                                            .setCalFactorValue(ssrs.GetText(1, 1));
                                }
                            }
                        }
                    }
            	}
            }else{
            
            //循环保险计划责任要素值集合，添加了主险校验
            for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++)
            {
                if (RiskCode.compareTo(this.mLCContPlanDutyParamSet.get(i)
                        .getRiskCode()) != 0
                        || MainRiskCode.compareTo(this.mLCContPlanDutyParamSet
                                .get(i).getMainRiskCode()) != 0)
                {
                    //当险种更换后，校验要素个数，且第一次进入时不校验
                    System.out.println("K : " + k);
                    System.out.println("tLMRiskDutyFactorSet.size() : "
                            + tLMRiskDutyFactorSet.size());
                    if (RiskCode.compareTo("") != 0
                            && k != tLMRiskDutyFactorSet.size()
                            && !StrTool.cTrim(this.mRiskFlag).equals("Wrap"))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "LCContPlanBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "险种" + RiskCode + "下的" + DutyCode
                                + "责任要素信息有误！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    //险种更换，则责任编码初始化
                    RiskCode = this.mLCContPlanDutyParamSet.get(i)
                            .getRiskCode();
                    MainRiskCode = this.mLCContPlanDutyParamSet.get(i)
                            .getMainRiskCode();
                    DutyCode = "";

                    //准备保单险种保险计划表数据，只与险种和计划挂钩，不关心责任、要素信息
                    mLCContPlanRiskSchema = new LCContPlanRiskSchema();
                    mLCContPlanRiskSchema.setPlanType(this.mPlanType);
                    mLCContPlanRiskSchema
                            .setGrpContNo(this.mLCContPlanDutyParamSet.get(i)
                                    .getGrpContNo());
                    mLCContPlanRiskSchema
                            .setProposalGrpContNo(this.mLCContPlanDutyParamSet
                                    .get(i).getProposalGrpContNo());
                    mLCContPlanRiskSchema
                            .setRiskCode(this.mLCContPlanDutyParamSet.get(i)
                                    .getRiskCode());
                    mLCContPlanRiskSchema
                            .setRiskVersion(this.mLCContPlanDutyParamSet.get(i)
                                    .getRiskVersion());
                    mLCContPlanRiskSchema
                            .setContPlanCode(this.mLCContPlanDutyParamSet
                                    .get(i).getContPlanCode());
                    mLCContPlanRiskSchema
                            .setContPlanName(this.mLCContPlanDutyParamSet
                                    .get(i).getContPlanName());
                    mLCContPlanRiskSchema
                            .setMainRiskCode(this.mLCContPlanDutyParamSet
                                    .get(i).getMainRiskCode());
                    mLCContPlanRiskSchema
                            .setMainRiskVersion(this.mLCContPlanDutyParamSet
                                    .get(i).getMainRiskVersion());
                    mLCContPlanRiskSchema
                            .setOperator(this.mGlobalInput.Operator);
                    //                    System.out.println("测试　保险计划中ＬＣＣｏｎｔＰｌａｎＲｉｓｋ未保存问题。");
                    //                    System.out.println("mLCContPlanRiskSchema : "+mLCContPlanRiskSchema.encode());
                    /*
                     *处理总保费问题
                     *1、从前台传入tLCContPlanRiskSet,其中包含险种代码和总保费信息
                     *2、将险种代码相同项目的总保费付给后台
                     */
                    if (tLCContPlanRiskSet.size() > 0)
                    {
                        mLCContPlanRiskSchema.setRiskPrem(tLCContPlanRiskSet
                                .get(i).getRiskPrem());
                        mLCContPlanRiskSchema.setRiskAmnt(tLCContPlanRiskSet
                                .get(i).getRiskAmnt());
                        mLCContPlanRiskSchema
                                .setRiskWrapFlag(tLCContPlanRiskSet.get(i)
                                        .getRiskWrapFlag());
                        mLCContPlanRiskSchema
                                .setRiskWrapCode(tLCContPlanRiskSet.get(i)
                                        .getRiskWrapCode());
                        mLCContPlanRiskSchema.setMakeDate(PubFun
                                .getCurrentDate());
                        mLCContPlanRiskSchema.setMakeTime(PubFun
                                .getCurrentTime());
                        mLCContPlanRiskSchema.setModifyDate(PubFun
                                .getCurrentDate());
                        mLCContPlanRiskSchema.setModifyTime(PubFun
                                .getCurrentTime());
                        mLCContPlanRiskSet.add(mLCContPlanRiskSchema);
                    }
                    System.out.println("测试　："
                            + this.mLCContPlanRiskSet.encode());
                    //新险种的第一个责任项校验
                    if (DutyCode.compareTo(this.mLCContPlanDutyParamSet.get(i)
                            .getDutyCode()) != 0)
                    {
                        //责任更换
                        DutyCode = this.mLCContPlanDutyParamSet.get(i)
                                .getDutyCode();
                        //如果要素信息是Deductible则，需要对要素信息做一定处理
                        if (this.mLCContPlanDutyParamSet.get(i).getCalFactor()
                                .compareTo("Deductible") == 0)
                        {
                            if (this.mLCContPlanDutyParamSet.get(i)
                                    .getCalFactorValue().compareTo("") == 0)
                            {
                                String lssql = "select b.Deductible from LMDutyGetRela a,LMDutyGetClm b where a.GetDutyCode = b.GetDutyCode and a.DutyCode = '"
                                        + DutyCode + "'";
                                ExeSQL exeSQL = new ExeSQL();
                                SSRS ssrs = exeSQL.execSQL(lssql);
                                mLCContPlanDutyParamSet.get(i)
                                        .setCalFactorValue(ssrs.GetText(1, 1));
                            }
                        }
                        //查询当前险种、当前责任下的要素个数
                        tLMRiskDutyFactorDB = new LMRiskDutyFactorDB();
                        //判定是险种信息录入，还是保障计划录入
                        if (this.mPlanType.compareTo("0") == 0)
                        {
                            mSql = "select * from LMRiskDutyFactor where RiskCode = '"
                                    + RiskCode
                                    + "' and ChooseFlag in ('0','2','3') and DutyCode = '"
                                    + DutyCode + "'";
                        }
                        else
                        {
                            mSql = "select * from LMRiskDutyFactor where RiskCode = '"
                                    + RiskCode
                                    + "' and ChooseFlag in ('1','2') and DutyCode = '"
                                    + DutyCode + "'";
                        }
                        tLMRiskDutyFactorSet = tLMRiskDutyFactorDB
                                .executeQuery(mSql);
                    }
                    k = 1;
                }
                else
                {
                    //同一险种下的不同责任校验
                    if (DutyCode.compareTo(this.mLCContPlanDutyParamSet.get(i)
                            .getDutyCode()) != 0)
                    {
                        //同一险种下，责任更换时，校验要素个数
                        System.out.println("K : " + k);
                        System.out.println("tLMRiskDutyFactorSet.size() : "
                                + tLMRiskDutyFactorSet.size());
                        if (k != tLMRiskDutyFactorSet.size()
                                && !StrTool.cTrim(mRiskFlag).equals("Wrap"))
                        {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "LCContPlanBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "险种" + RiskCode + "下的"
                                    + DutyCode + "责任要素信息有误！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        //责任更换
                        DutyCode = this.mLCContPlanDutyParamSet.get(i)
                                .getDutyCode();
                        if (this.mLCContPlanDutyParamSet.get(i).getCalFactor()
                                .compareTo("Deductible") == 0)
                        {
                            if (this.mLCContPlanDutyParamSet.get(i)
                                    .getCalFactorValue().compareTo("") == 0)
                            {
                                String lssql = "select b.Deductible from LMDutyGetRela a,LMDutyGetClm b where a.GetDutyCode = b.GetDutyCode and a.DutyCode = '"
                                        + DutyCode + "'";
                                ExeSQL exeSQL = new ExeSQL();
                                SSRS ssrs = exeSQL.execSQL(lssql);
                                mLCContPlanDutyParamSet.get(i)
                                        .setCalFactorValue(ssrs.GetText(1, 1));
                            }
                        }
                        k = 1;
                        //查询当前险种、当前责任下的要素个数
                        tLMRiskDutyFactorDB = new LMRiskDutyFactorDB();
                        //判定是险种信息录入，还是保障计划录入
                        if (this.mPlanType.compareTo("0") == 0)
                        {
                            mSql = "select * from LMRiskDutyFactor where RiskCode = '"
                                    + RiskCode
                                    + "' and ChooseFlag in ('0','2','3') and DutyCode = '"
                                    + DutyCode + "'";
                        }
                        else
                        {
                            mSql = "select * from LMRiskDutyFactor where RiskCode = '"
                                    + RiskCode
                                    + "' and ChooseFlag in ('1','2') and DutyCode = '"
                                    + DutyCode + "'";
                        }
                        tLMRiskDutyFactorSet = tLMRiskDutyFactorDB
                                .executeQuery(mSql);
                    }
                    else
                    {
                        if (this.mLCContPlanDutyParamSet.get(i).getCalFactor()
                                .compareTo("Deductible") == 0)
                        {
                            if (this.mLCContPlanDutyParamSet.get(i)
                                    .getCalFactorValue().compareTo("") == 0)
                            {
                                String lssql = "select b.Deductible from LMDutyGetRela a,LMDutyGetClm b where a.GetDutyCode = b.GetDutyCode and a.DutyCode = '"
                                        + DutyCode + "'";
                                ExeSQL exeSQL = new ExeSQL();
                                SSRS ssrs = exeSQL.execSQL(lssql);
                                mLCContPlanDutyParamSet.get(i)
                                        .setCalFactorValue(ssrs.GetText(1, 1));
                            }
                        }
                        k = k + 1;
                    }
                }
            }
            System.out.println("K : " + k);
            System.out.println("tLMRiskDutyFactorSet.size() :"
                    + tLMRiskDutyFactorSet.size());
            if (k != tLMRiskDutyFactorSet.size()
                    && !StrTool.cTrim(mRiskFlag).equals("Wrap"))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCContPlanBL";
                tError.functionName = "dealData";
                tError.errorMessage = "险种" + RiskCode + "下的" + DutyCode
                        + "责任要素信息有误！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            }
            
            /**
             * 将账户录入的要素信息补充到计划的责任下；
             */
            LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
            tLCContPlanDutyParamDB.setGrpContNo(this.mGrpContNo);
            //contplancode为11 表示帐户计划，要将账户计划查询出来填充到

            StringBuffer sql = new StringBuffer();
            sql.append("select * from LCContPlanDutyParam where grpcontno='");
            sql.append(this.mGrpContNo);
            sql.append("' and contplancode='11'");
            sql.append(" and riskcode in ");
            sql.append("(");
            for (int i = 1; i <= this.mLCContPlanRiskSet.size(); i++)
            {
                sql.append("'");
                sql.append(mLCContPlanRiskSet.get(i).getRiskCode());
                sql.append("'");
                if (i != this.mLCContPlanRiskSet.size())
                {
                    sql.append(",");
                }
            }
            sql.append(")");
            LCContPlanDutyParamSet tLCContPlanDutyParamSet = tLCContPlanDutyParamDB
                    .executeQuery(sql.toString());
            String dutyCode = mLCContPlanDutyParamSet.get(1).getDutyCode();
            int theFinal = mLCContPlanDutyParamSet.size();
            for (int i = 1; i <= theFinal; i++)
            {
                if (i == 1
                        || !dutyCode.equals(this.mLCContPlanDutyParamSet.get(i)
                                .getDutyCode()))
                {
                    for (int n = 1; n <= tLCContPlanDutyParamSet.size(); n++)
                    {
                        if (tLCContPlanDutyParamSet.get(n).getRiskCode()
                                .equals(
                                        mLCContPlanDutyParamSet.get(i)
                                                .getRiskCode()))
                        {
                            LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = tLCContPlanDutyParamSet
                                    .get(n).getSchema();
                            tLCContPlanDutyParamSchema
                                    .setDutyCode(mLCContPlanDutyParamSet.get(i)
                                            .getDutyCode());
                            tLCContPlanDutyParamSchema
                                    .setMainRiskCode(mLCContPlanDutyParamSet
                                            .get(i).getMainRiskCode());
                            tLCContPlanDutyParamSchema
                                    .setContPlanCode(mLCContPlanDutyParamSet
                                            .get(i).getContPlanCode());
                            tLCContPlanDutyParamSchema
                                    .setContPlanName(mLCContPlanDutyParamSet
                                            .get(i).getContPlanName());
                            mLCContPlanDutyParamSet
                                    .add(tLCContPlanDutyParamSchema);
                        }
                    }
                }
                dutyCode = mLCContPlanDutyParamSet.get(i).getDutyCode();
            }
            //            mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSet);

            //准备保单保险计划表数据
            mLCContPlanSchema = new LCContPlanSchema();
            mLCContPlanSchema.setGrpContNo(this.mLCContPlanDutyParamSet.get(1)
                    .getGrpContNo());
            mLCContPlanSchema.setProposalGrpContNo(this.mLCContPlanDutyParamSet
                    .get(1).getProposalGrpContNo());
            mLCContPlanSchema.setContPlanCode(this.mLCContPlanDutyParamSet.get(
                    1).getContPlanCode());
            mLCContPlanSchema.setContPlanName(this.mLCContPlanDutyParamSet.get(
                    1).getContPlanName());
            mLCContPlanSchema.setPlanType(this.mPlanType); //计划类别，此处全部为固定计划
            mLCContPlanSchema.setPlanSql(this.mPlanSql); //分类说明
            mLCContPlanSchema.setPeoples3(this.mPeoples3); //可保人数
            mLCContPlanSchema.setPeoples2(this.mPeoples2);
            mLCContPlanSchema.setOperator(this.mGlobalInput.Operator);
            mLCContPlanSchema.setMakeDate(PubFun.getCurrentDate());
            mLCContPlanSchema.setMakeTime(PubFun.getCurrentTime());
            mLCContPlanSchema.setModifyDate(PubFun.getCurrentDate());
            mLCContPlanSchema.setModifyTime(PubFun.getCurrentTime());
            mLCContPlanSet.add(mLCContPlanSchema);
            /**
             * 处理管理费相关问题
             * 1.校验险种描述
             * 2.填充管理费相关项
             */
            if (mLCGrpFeeSet.size() > 0)
            {
                System.out.println("在LCContPlanBL中处理 ： ");
                System.out.println("前台传入的管理费含有" + mLCGrpFeeSet.size() + "条数据");
                for (int i = 1; i <= mLCGrpFeeSet.size(); i++)
                {
                    System.out.println("正在处理管理费"
                            + mLCGrpFeeSet.get(i).getFeeCode());
                    LMRiskAccPayDB tLMRiskAccPayDB = new LMRiskAccPayDB();
                    tLMRiskAccPayDB.setRiskCode(this.mLCGrpFeeSet.get(i)
                            .getRiskCode());
                    tLMRiskAccPayDB.setInsuAccNo(mLCGrpFeeSet.get(i)
                            .getInsuAccNo());
                    tLMRiskAccPayDB.setPayPlanCode(mLCGrpFeeSet.get(i)
                            .getPayPlanCode());
                    if (!tLMRiskAccPayDB.getInfo())
                    {
                        System.out.println("程序第481行报错;author:YangMing");
                        CError tError = new CError();
                        tError.moduleName = "LCContPlanBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "在处理管理费时查询管险种保险帐户缴费表失败！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    LMRiskAccPaySchema tLMRiskAccPaySchema = tLMRiskAccPayDB
                            .getSchema();
                    LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
                    tLMRiskFeeDB.setFeeCode(mLCGrpFeeSet.get(i).getFeeCode());
                    tLMRiskFeeDB.setInsuAccNo(mLCGrpFeeSet.get(i)
                            .getInsuAccNo());
                    tLMRiskFeeDB.setPayPlanCode(mLCGrpFeeSet.get(i)
                            .getPayPlanCode());
                    /**
                     * 暂时不支持管理费比例录入因此在查询时只查询固定值
                     */
                    tLMRiskFeeDB.setFeeCalMode("02");
                    if (!tLMRiskFeeDB.getInfo())
                    {
                        System.out.println("程序第500行报错;author:YangMing");
                        CError tError = new CError();
                        tError.moduleName = "LCContPlanBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "在处理管理费时查询管理费描述表失败！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    LMRiskFeeSchema tLMRiskFeeSchema = tLMRiskFeeDB.getSchema();
                    String strSql = "select grppolno from lcgrppol where grpcontno='"
                            + this.mGrpContNo
                            + "' and riskcode='"
                            + this.mLCGrpFeeSet.get(i).getRiskCode() + "'";
                    ExeSQL tExeSQL = new ExeSQL();
                    String GrpPolNo = "";
                    try
                    {
                        GrpPolNo = tExeSQL.getOneValue(strSql);
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                        System.out.println("程序第518行报错;author:YangMing");
                    }
                    if (GrpPolNo.trim().equals("") || GrpPolNo.equals("null"))
                    {
                        System.out.println("程序第522行报错;author:YangMing");
                        CError tError = new CError();
                        tError.moduleName = "LCContPlanBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "在处理管理费时查询团体险种号失败！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    mLCGrpFeeSet.get(i).setGrpPolNo(GrpPolNo);
                    mLCGrpFeeSet.get(i).setGrpContNo(this.mGrpContNo);
                    mLCGrpFeeSet.get(i).setRiskCode(
                            this.mLCGrpFeeSet.get(i).getRiskCode());
                    mLCGrpFeeSet.get(i).setPayInsuAccName(
                            tLMRiskFeeSchema.getPayInsuAccName());
                    mLCGrpFeeSet.get(i).setFeeCalMode(
                            tLMRiskFeeSchema.getFeeCalMode());
                    mLCGrpFeeSet.get(i).setFeeCalModeType(
                            tLMRiskFeeSchema.getFeeCalModeType());
                    mLCGrpFeeSet.get(i).setFeeCalCode(
                            tLMRiskFeeSchema.getFeeCalCode());
                    mLCGrpFeeSet.get(i).setFeePeriod(
                            tLMRiskFeeSchema.getFeePeriod());
                    mLCGrpFeeSet.get(i).setMaxTime(
                            tLMRiskFeeSchema.getMaxTime());
                    mLCGrpFeeSet.get(i).setDefaultFlag(
                            tLMRiskFeeSchema.getDefaultFlag());
                    mLCGrpFeeSet.get(i).setOperator(this.mGlobalInput.Operator);
                    mLCGrpFeeSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLCGrpFeeSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLCGrpFeeSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCGrpFeeSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            System.out.println("保险计划　：" + mLCContPlanDutyParamSet.size());
            for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++)
            {
                System.out.println("mLCContPlanDutyParamSet : "
                        + mLCContPlanDutyParamSet.get(i).getCalFactor());
                System.out.println("mLCContPlanDutyParamSet : "
                        + mLCContPlanDutyParamSet.get(i).getCalFactorValue());
            }
            //用于校验险种责任要素
            //@author Yangming
            VData tempVData = new VData();
            tempVData.add(mLCContPlanDutyParamSet);
            CheckRiskDutyFactor tCheckRiskDutyFactor = new CheckRiskDutyFactor();

            // 加入团单保单层信息。
            LCGrpContSet tLCGrpContSet = null;
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(this.mGrpContNo);
            tLCGrpContSet = tLCGrpContDB.query();
            tempVData.add(tLCGrpContSet);
            
            if (!tCheckRiskDutyFactor.submitData(tempVData, ""))
            {
                System.out.println("校验报错！");
            }
            VData Result = tCheckRiskDutyFactor.getResult();
            if (Result.size() > 0)
            {
                String Error = (String) Result.get(0);
                System.out.println("Error" + Error);
                if (Error != null)
                {
                    if (!Error.equals(""))
                    {
                        System.out.println("LCContPlanBL.dealData()");
                        System.out.println("程序第453行报错;author:YangMing");
                        CError tError = new CError();
                        tError.moduleName = "LCContPlanBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = Error;
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }

        }
        //由于删除操作可能没有multine数据，因此需要一些特殊参数，并且根据参数查询要删除的数据
        if (this.mOperate.compareTo("DELETE||MAIN") == 0)
        {
            //准备保单保险计划的删除数据
            LCContPlanDB tLCContPlanDB = new LCContPlanDB();
            tLCContPlanDB.setGrpContNo(this.mGrpContNo);
            tLCContPlanDB.setContPlanCode(this.mContPlanCode);
            tLCContPlanDB.setPlanType(this.mPlanType);
            tLCContPlanDB.setProposalGrpContNo(this.mProposalGrpContNo);
            mLCContPlanSet = tLCContPlanDB.query();

            //准备保单险种保险计划的删除数据
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setContPlanCode(this.mContPlanCode);
            tLCContPlanRiskDB.setGrpContNo(this.mGrpContNo);
            tLCContPlanRiskDB.setPlanType(this.mPlanType);
            tLCContPlanRiskDB.setProposalGrpContNo(this.mProposalGrpContNo);
            mLCContPlanRiskSet = tLCContPlanRiskDB.query();

            //准备保险计划责任要素值的删除数据
            LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
            tLCContPlanDutyParamDB.setContPlanCode(this.mContPlanCode);
            tLCContPlanDutyParamDB.setGrpContNo(this.mGrpContNo);
            tLCContPlanDutyParamDB.setPlanType(this.mPlanType);
            tLCContPlanDutyParamDB
                    .setProposalGrpContNo(this.mProposalGrpContNo);
            mLCContPlanDutyParamSet = tLCContPlanDutyParamDB.query();
            //删除管理费
            //            LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
            //            tLCGrpFeeDB.setGrpContNo(this.mGrpContNo);
            //            tLCGrpFeeDB.setRiskCode();
        }

        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLCContPlanDutyParamSet.set((LCContPlanDutyParamSet) cInputData
                .getObjectByObjectName("LCContPlanDutyParamSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        this.mProposalGrpContNo = (String) cInputData.get(2);
        this.mGrpContNo = (String) cInputData.get(3);
        this.mContPlanCode = (String) cInputData.get(4);
        this.mPlanType = (String) cInputData.get(5);
        this.mPlanSql = (String) cInputData.get(6);
        this.mPeoples3 = (String) cInputData.get(7);
        this.mPeoples2 = (String) cInputData.get(8);
        this.tLCContPlanRiskSet = (LCContPlanRiskSet) cInputData
                .getObjectByObjectName("LCContPlanRiskSet", 0);
        this.mLCGrpFeeSet.set((LCGrpFeeSet) cInputData.getObjectByObjectName(
                "LCGrpFeeSet", 0));
        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mTransferData != null)
        {
            mRiskFlag = (String) mTransferData.getValueByName("RiskFlag");
        }
        System.out.println("mTransferData : " + mTransferData);
        System.out.println("在Bl中测试 是否存在RiskFlag " + mRiskFlag);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LCContPlanBLQuery Submit...");
        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        tLCContPlanDB.setSchema(this.mLCContPlanSchema);
        this.mLCContPlanSet = tLCContPlanDB.query();
        this.mResult.add(this.mLCContPlanSet);
        System.out.println("End LCContPlanBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLCContPlanDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLCContPlanSet);
            System.out
                    .println("LCContPlanBL.prepareOutputData()  \n--Line:669  --Author:YangMing");
            System.out.println("mLCContPlanRiskSet : "
                    + mLCContPlanRiskSet.encode());
            this.mInputData.add(this.mLCContPlanRiskSet);
            this.mInputData.add(this.mLCContPlanDutyParamSet);
            this.mInputData.add(this.mLCGrpFeeSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 健享全球团体医疗保险，录入“方案编码”、“保障地区”、“特别医院给付比例”存到LCContPlanDutyParam ，每个责任存一遍。
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean prepareDutyParam()
    {
    	if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "prepareDutyParam";
            tError.errorMessage = "获取页面传输的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
    	
    	HashMap tHashMap = (HashMap) mTransferData.getValueByName("NewFactor");
    	//HashMap为空直接返回true
    	if(tHashMap==null || tHashMap.isEmpty())
    	{
    		return true;
    	}
    	
    	//1、先将每个责任取一条临时的
    	LCContPlanDutyParamSet tempLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
    	for(int i=1; i<=mLCContPlanDutyParamSet.size(); i++)
    	{
    		String tDutyCode = mLCContPlanDutyParamSet.get(i).getDutyCode();
    		if(tempLCContPlanDutyParamSet.size()==0)
    		{
    			tempLCContPlanDutyParamSet.add(mLCContPlanDutyParamSet.get(i).getSchema());
    		}
    		else
    		{
    			boolean flag = false;
    			for(int j=1; j<=tempLCContPlanDutyParamSet.size();j++)
    			{
    				if(tDutyCode.equals(tempLCContPlanDutyParamSet.get(j).getSchema().getDutyCode()))
    				{
    					flag = true;
    					break;
    				}
    			}
    			if(!flag)
    			{
    				tempLCContPlanDutyParamSet.add(mLCContPlanDutyParamSet.get(i).getSchema());
    			}
    		}
    	}
    	
    	//开始序列号
    	int order = mLCContPlanDutyParamSet.size();
    	
    	//2、每个责任都存一套“方案编码”、“保障地区”、“特别医院给付比例”
    	for(int k=1; k<=tempLCContPlanDutyParamSet.size();k++)
    	{
    		LCContPlanDutyParamSchema tempLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
    		tempLCContPlanDutyParamSchema.setSchema(tempLCContPlanDutyParamSet.get(k).getSchema());
    		
    		Iterator iter = tHashMap.entrySet().iterator();
    		while(iter.hasNext()) 
    		{
    			Map.Entry entry = (Map.Entry) iter.next();
    			String key = String.valueOf(entry.getKey());
    			String value = String.valueOf(entry.getValue());
    			LCContPlanDutyParamSchema newLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        		Reflections ref = new Reflections();
        		ref.transFields(newLCContPlanDutyParamSchema, tempLCContPlanDutyParamSchema);
    			if("InsuPlanCode160306".equals(key)){//因160306险种，增加方案处理，且该险种可和其他险种一起承保（该点与健享全球不同），因此其他险种保障计划要素需过滤方案编码。
    				if("160306".equals(tempLCContPlanDutyParamSchema.getRiskCode())){
    					newLCContPlanDutyParamSchema.setCalFactor(key);
                		newLCContPlanDutyParamSchema.setCalFactorValue(value);
                		newLCContPlanDutyParamSchema.setOrder(order);
                		mLCContPlanDutyParamSet.add(newLCContPlanDutyParamSchema);
    				}
    			}else{
    				newLCContPlanDutyParamSchema.setCalFactor(key);
            		newLCContPlanDutyParamSchema.setCalFactorValue(value);
            		newLCContPlanDutyParamSchema.setOrder(order);
            		mLCContPlanDutyParamSet.add(newLCContPlanDutyParamSchema);
    			}
        		
        		order++;
    		}
    	}
    	return true;
    }
    
    private String getInsuPlanCode(){
    	
    	String tInsuPlanCode = "";
    	
    	if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "prepareDutyParam";
            tError.errorMessage = "获取页面传输的数据时出错。";
            this.mErrors.addOneError(tError);
            return "";
        }
    	HashMap tHashMap = (HashMap) mTransferData.getValueByName("NewFactor");
    	//HashMap为空直接返回true
    	if(tHashMap==null || tHashMap.isEmpty())
    	{
    		return "";
    	}
    	if(tHashMap.containsKey("InsuPlanCode")){
    		tInsuPlanCode = (String)(tHashMap.get("InsuPlanCode"));
    	}
    	System.out.println("tInsuPlanCode = " + tInsuPlanCode);
    	return tInsuPlanCode;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
