package com.sinosoft.lis.tb;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class StopRiskUI {
	/** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate="";
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
   
   
    public boolean submitData(VData cInputData, String cOperate){
//    	将操作数据拷贝到本类中
    	this.mInputData = cInputData;
        this.mOperate = cOperate;
        StopRiskBL stopRiskBL = new StopRiskBL();
        System.out.println("StopRiskBL BEGIN");
        if(!stopRiskBL.submitData(cInputData,mOperate)){
        	 // @@错误处理
            this.mErrors.copyAllErrors(stopRiskBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ReportUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        } 
        return true;
    }

}
