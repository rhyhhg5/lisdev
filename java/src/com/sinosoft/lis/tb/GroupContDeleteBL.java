package com.sinosoft.lis.tb;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title:团单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class GroupContDeleteBL {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    /** 判断是否直接删除 扫描件  */
    private boolean delScan = false;

    public GroupContDeleteBL() {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //将VData数据还原成业务需要的类
        if (this.getInputData() == false) {
            return false;
        }

        System.out.println("---getInputData successful---");

        if (this.dealData() == false) {
            return false;
        }

        System.out.println("---dealdata successful---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mResult, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            return false;
        }

        return true;
    }

    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData() {
        //全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null) {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        //团体保单实例
        mLCGrpContSchema.setSchema((LCGrpContSchema) mInputData.
                                   getObjectByObjectName("LCGrpContSchema", 0));

        if (mLCGrpContSchema == null) {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));

            return false;
        }

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = null;
        if (!StrTool.cTrim(mLCGrpContSchema.getGrpContNo()).equals("")) {
            tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLCGrpContSet = tLCGrpContDB.query();
        } else if (this.mLCGrpContSchema.getPrtNo() != null) {
            delScan = true;
        }

        if (tLCGrpContSet != null && tLCGrpContSet.size() > 0) {
            mLCGrpContSchema.setSchema(tLCGrpContSet.get(1));
        }
        System.out.println("完成数据解析 结果为 " + mLCGrpContSchema.getPrtNo() + "  " +
                           mLCGrpContSchema.getGrpContNo());
        return true;
    }

    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * @return boolean
     */
    private boolean dealData() {
        String tPrtNo = mLCGrpContSchema.getPrtNo();
    	
        //单独删除扫描件的操作
        if(StrTool.cTrim(mLCGrpContSchema.getGrpContNo()).equals("")){
            System.out.println("单独删除投保书扫描件!!!!!");
                //删除投保书扫描件,工作流
               mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='" +
                        tPrtNo + "' and subtype in ('TB02','TB07'))", "DELETE");
               mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='" +
                        tPrtNo + "'and subtype in ('TB02','TB07'))", "DELETE");
               mMap.put("delete from es_doc_main where doccode='" + tPrtNo +
                        "'and subtype in ('TB02','TB07')", "DELETE");
               
               //删除团体工伤险的扫描件
               mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='" +
                       tPrtNo + "' and subtype='TB08')", "DELETE");
               mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='" +
                       tPrtNo + "'and subtype='TB08')", "DELETE");
               mMap.put("delete from es_doc_main where doccode='" + tPrtNo +
                       "'and subtype='TB08'", "DELETE");
               
               mMap.put("delete from lwmission where MissionProp1 = '" +
                        tPrtNo + "' and activityid in ('0000002098','0000002099') ",
                        "DELETE");
               
               //删除团体工伤险的工作流
               mMap.put("delete from lwmission where MissionProp1 = '" +
                       tPrtNo + "' and activityid = '0000012001' ",
                       "DELETE");
                   return true;
        }
        String tGrpContNo = mLCGrpContSchema.getGrpContNo();

         tPrtNo = mLCGrpContSchema.getPrtNo();
        mLCDelPolLog.setOtherNo(StrTool.cTrim(tGrpContNo));
        mLCDelPolLog.setOtherNoType("0");
        mLCDelPolLog.setPrtNo(mLCGrpContSchema.getPrtNo());

        if ("1".equals(mLCGrpContSchema.getAppFlag())) {
            mLCDelPolLog.setIsPolFlag("1");
        } else {
            mLCDelPolLog.setIsPolFlag("0");
        }

        mLCDelPolLog.setOperator(mOperator);
        mLCDelPolLog.setManageCom(mManageCom);
        mLCDelPolLog.setMakeDate(theCurrentDate);
        mLCDelPolLog.setMakeTime(theCurrentTime);
        mLCDelPolLog.setModifyDate(theCurrentDate);
        mLCDelPolLog.setModifyTime(theCurrentTime);
        if (!this.delScan) {

            mMap.put("insert into LOBInsuredRelated (select * from LCInsuredRelated where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCInsuredRelated where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put("insert into LOBDuty (select * from LCDuty where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put(
                    "delete from LCDuty where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                    tGrpContNo + "')", "DELETE");

            mMap.put("insert into LOBPrem_1 (select * from LCPrem_1 where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCPrem_1 where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put("insert into LOBPremToAcc (select * from LCPremToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCPremToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put("insert into LOBGetToAcc (select * from LCGetToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCGetToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put(
                    "insert into LOBPol (select * from LCPol where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCPol where GrpContNo = '" + tGrpContNo + "'",
                     "DELETE");

            mMap.put("insert into LOBBnf (select * from LCBnf where ContNo in (select ContNo from LCCont where GrpContNo = '" +
                     tGrpContNo + "'))", "INSERT");
            mMap.put("delete from LCBnf where ContNo in (select ContNo from LCCont where GrpContNo = '" +
                     tGrpContNo + "')", "DELETE");

            mMap.put(
                    "insert into LOBCont (select * from LCCont where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCont where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGrpPol (select * from LCGrpPol where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpPol where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBPrem (select * from LCPrem where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCPrem where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGet (select * from LCGet where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGet where GrpContNo = '" + tGrpContNo + "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBInsureAccFee (select * from LCInsureAccFee where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccFee where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put("insert into LOBInsureAccClassFee (select * from LCInsureAccClassFee where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccClassFee where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put("insert into LOBInsureAccClass (select * from LCInsureAccClass where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccClass where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put(
                    "insert into LOBInsureAcc (select * from LCInsureAcc where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAcc where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put("insert into LOBInsureAccTrace (select * from LCInsureAccTrace where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsureAccTrace where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put(
                    "insert into LOBInsured (select * from LCInsured where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCInsured where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put("insert into LOBCustomerImpart (select * from LCCustomerImpart where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCustomerImpart where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put("insert into LOBCustomerImpartParams (select * from LCCustomerImpartParams where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCustomerImpartParams where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

//          mMap.put(
//                  "insert into LOBAppnt (select * from LCAppnt where GrpContNo = '" +
//                  tGrpContNo + "')", "INSERT");
//          mMap.put("delete from LCAppnt where GrpContNo = '" + tGrpContNo +
//                   "'",
//                   "DELETE");

            mMap.put(
                    "insert into LOBGrpAppnt (select * from LCGrpAppnt where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpAppnt where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGrpFee (select * from LCGrpFee where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpFee where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");
            
//            mMap.put(
//                    "insert into LOBGrpInterest (select * from LCGrpFee where GrpContNo = '" +
//                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpInterest where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGrpFeeParam (select * from LCGrpFeeParam where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpFeeParam where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBContPlan (select * from LCContPlan where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlan where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGeneral (select * from LCGeneral where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGeneral where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put("insert into LOBGeneralToRisk (select * from LCGeneralToRisk where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGeneralToRisk where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBContPlanRisk (select * from LCContPlanRisk where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlanRisk where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put("insert into LOBContPlanDutyParam (select * from LCContPlanDutyParam where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlanDutyParam where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put("insert into LOBContPlanFactory (select * from LCContPlanFactory where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlanFactory where GrpContNo = '" +
                     tGrpContNo + "'", "DELETE");

            mMap.put("insert into LOBContPlanParam (select * from LCContPlanParam where GrpContNo = '" +
                     tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCContPlanParam where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBUWError (select * from LCUWError where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCUWError where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBUWSub (select * from LCUWSub where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCUWSub where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBUWMaster (select * from LCUWMaster where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCUWMaster where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBCUWError (select * from LCCUWError where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCUWError where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBCUWSub (select * from LCCUWSub where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCUWSub where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBCUWMaster (select * from LCCUWMaster where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCUWMaster where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGUWError (select * from LCGUWError where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGUWError where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGUWSub (select * from LCGUWSub where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGUWSub where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGUWMaster (select * from LCGUWMaster where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGUWMaster where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGCUWError (select * from LCGCUWError where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGCUWError where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGCUWSub (select * from LCGCUWSub where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGCUWSub where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBGCUWMaster (select * from LCGCUWMaster where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGCUWMaster where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBUWReport (select * from LCUWReport where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCUWReport where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBNotePad (select * from LCNotePad where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCNotePad where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            mMap.put(
                    "insert into LOBRReport (select * from LCRReport where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCRReport where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

//            mMap.put(
//                    "insert into LOBIssuePol (select * from LCIssuePol where GrpContNo = '" +
//                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCIssuePol where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBGrpIssuePol (select * from LCGrpIssuePol where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCGrpIssuePol where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBPENoticeItem (select * from LCPENoticeItem where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCPENoticeItem where GrpContNo = '" +
                     tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBPENotice (select * from LCPENotice where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCPENotice where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

            mMap.put(
                    "insert into LOBCGrpSpec (select * from LCCGrpSpec where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCCGrpSpec where GrpContNo = '" + tGrpContNo +
                     "'", "DELETE");

//            mMap.put(
//                    "insert into LOBCSpec (select * from LCCSpec where GrpContNo = '" +
//                    tGrpContNo + "')", "INSERT");
//            mMap.put("delete from LCCSpec where GrpContNo = '" + tGrpContNo +
//                     "'",
//                     "DELETE");
 //增加万能型的契约业务表
            mMap.put(
                    "insert into lobAscriptionrulefactory (select * from lcAscriptionrulefactory where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from lcAscriptionrulefactory where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");
            
            mMap.put(
                    "insert into Lobascriptionruleparams (select * from Lcascriptionruleparams where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from Lcascriptionruleparams where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");
            
            mMap.put(
                    "insert into lobriskztfee (select * from lcriskztfee where GrpContNo = '" +
                    tGrpContNo + "')", "INSERT");
            mMap.put("delete from lcriskztfee where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");
            
            mMap.put("delete from lcgrpposition where GrpContNo = '" + tGrpContNo +
                    "'",
                    "DELETE");
            
//          mMap.put(
//                  "insert into LOBSpec (select * from LCSpec where GrpContNo = '" +
//                  tGrpContNo + "')", "INSERT");
            mMap.put("delete from LCSpec where GrpContNo = '" + tGrpContNo +
                     "'",
                     "DELETE");

            //删除被保人清单表和团体定期结算计划表数据
            mMap.put("delete from LCInsuredList where GrpContNo = '" + tGrpContNo + "'", "DELETE");
            mMap.put("delete from LCGrpBalPlan where GrpContNo = '" + tGrpContNo + "'", "DELETE");
            
            //删除一切通知书
            mMap.put("delete from loprtmanager where otherno = '" + mLCGrpContSchema.getProposalGrpContNo() +
                     "'",
                     "DELETE");
            
//          删除约定缴费计划
            mMap.put("delete from LCGrpPayplan where ProposalGrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() +
                     "'",
                     "DELETE");
//          删除约定缴费计划
            mMap.put("delete from LCGrpPayDue where ProposalGrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() +
                     "'",
                     "DELETE");
//          删除综合开拓
            mMap.put("delete from LCExtend where prtno = '" + tPrtNo +
                     "'",
                     "DELETE");
        }
        
//      删除结余返还
        mMap.put("delete from LCGrpContSub where prtno = '" + tPrtNo + "'","DELETE");
        
        //删除投保书扫描件
        mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='" +
                 tPrtNo + "' and subtype in ('TB02','TB07'))", "DELETE");
        mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='" +
                 tPrtNo + "'and subtype in ('TB02','TB07'))", "DELETE");
        mMap.put("delete from es_doc_main where doccode='" + tPrtNo +
                 "'and subtype in ('TB02','TB07')", "DELETE");
        
        //删除团体工伤险的扫描件
        mMap.put("delete from es_doc_pages where docid in (select docid from es_doc_main where doccode='" +
                tPrtNo + "' and subtype='TB08')", "DELETE");
        mMap.put("delete from es_doc_relation where docid in (select docid from es_doc_main where doccode='" +
                tPrtNo + "'and subtype='TB08')", "DELETE");
        mMap.put("delete from es_doc_main where doccode='" + tPrtNo +
                "'and subtype='TB08'", "DELETE");
        
        //删除保单操作轨迹表
        mMap.put("delete from LDSysTrace where PolNo = '" + tPrtNo + "'", "DELETE");
        
        //删除被保险人导入信息
        mMap.put("delete from LCGrpImportLog where GrpContNo = '" + tGrpContNo + "'", "DELETE");

        mMap.put("delete from lcnation where grpcontno='" + tGrpContNo + "'", "DELETE");

        //删除工作流
        String strSql = "select activityid from lwmission where (MissionProp1 = '" +
                tPrtNo + "' or MissionProp2 = '" + tPrtNo + "')";
        //String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(strSql);
        System.out.println("tSSRS.getMaxRow()=" + tSSRS.getMaxRow());
        if (tSSRS.getMaxRow() != 0) {
            for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
                String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
                String activityid = tSSRS.GetText(row, 1);
                System.out.println("row==" + row);
                System.out.println("tSerielNo==" + tSerielNo);
                System.out.println("activityid==" + activityid);
                if (activityid.equals("0000001099") ||
                    activityid.equals("0000001098") ||
                    activityid.equals("0000001100") ||
                    activityid.equals("0000002099") ||
                    activityid.equals("0000002098") ||
                    activityid.equals("0000002004") ||
                    activityid.equals("0000012001")) {
                    mMap.put("insert into LBmission (select '" + tSerielNo +
                             "',lwmission.* from lwmission where MissionProp1 = '" +
                             tPrtNo + "' and activityid = '" + activityid +
                             "')", "INSERT");
                    mMap.put("delete from lwmission where MissionProp1 = '" +
                             tPrtNo + "' and activityid = '" + activityid + "'",
                             "DELETE");
                }
                if (activityid.equals("0000001001") ||
                    activityid.equals("0000002001") ||
                    activityid.equals("0000002004") ||
                    activityid.equals("0000002006") ||
                    activityid.equals("0000002005") ||
                    activityid.equals("0000012002")) {
                    mMap.put("insert into LBmission (select '" + tSerielNo +
                             "', lwmission.* from lwmission where MissionProp2 = '" +
                             tPrtNo + "' and activityid = '" + activityid +
                             "')", "INSERT");
                    mMap.put("delete from lwmission where MissionProp2 = '" +
                             tPrtNo + "' and activityid = '" + activityid + "'",
                             "DELETE");
                }
            }
        }
        
        // 清除相关共保要素。
        MMap tTmpMap = null;
        tTmpMap = delCoInsuranceInfo(tGrpContNo);
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        // ----------------------------

        mMap.put(
                "insert into LOBGrpCont (select * from LCGrpCont where GrpContNo = '" +
                tGrpContNo + "')", "INSERT");
        mMap.put("delete from LCGrpCont where GrpContNo = '" + tGrpContNo + "'",
                 "DELETE");
        if (!this.delScan) {
            mMap.put(mLCDelPolLog, "INSERT");
        }
        
        if("2".equals(mLCGrpContSchema.getCardFlag())){
        	mMap.put("update licertifyinsured set grpcontno = null,proposalgrpcontno = null,prtno = null," 
        			 + "state = '01', " 
        			 + "ModifyDate ='" + theCurrentDate + "'," + "ModifyTime ='" + theCurrentTime
                     + "' where prtno = '" + mLCGrpContSchema.getPrtNo() + "'" ,"UPDATE");
        	mMap.put("update licertify set grpcontno = null,proposalgrpcontno = null,prtno = null," 
       			 	+ "state = '01', " 
       			 	+ "ModifyDate ='" + theCurrentDate + "'," + "ModifyTime ='" + theCurrentTime
                    + "' where prtno = '" + mLCGrpContSchema.getPrtNo() + "'" ,"UPDATE");
            mMap.put("update libcertify set grpcontno = null,proposalgrpcontno = null,prtno = null," 
                    + "state = '01', " 
                    + "ModifyDate ='" + theCurrentDate + "'," + "ModifyTime ='" + theCurrentTime
                    + "' where prtno = '" + mLCGrpContSchema.getPrtNo() + "'" ,"UPDATE");
        }
        //add by zjd 删除共用保额计划
        mMap.put("delete from LCGrpShareAmnt where GrpContNo = '" + tGrpContNo +
                "'",
                "DELETE");
        
        mMap.put("delete from LCGrpRiskShareAmnt where GrpContNo = '" + tGrpContNo +
                "'",
                "DELETE");
        //伤残给付比例删除
        mMap.put("delete from LCContPlanDutyDefGrade where GrpContNo = '" + tGrpContNo +
                "'",
                "DELETE");
        
      //djj 2017-10-25
      //不允许银行在途和收费成功状态的保单进行新单删除操作，只有银行转帐锁定状态的保单可以进行新单删除
        //校验是否银行在途
    	String bankOnTheWay = new ExeSQL().getOneValue("select db2inst1.nvl(count(1), 0) from LJSPay where OtherNo = '" 
    			+ mLCGrpContSchema.getPrtNo() + "' and OtherNoType in ('9','16') and BankOnTheWayFlag = '1'");
    	if(!bankOnTheWay.equals("0.0"))
    	{
    		CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareBankData";
            tError.errorMessage = "保单银行在途，不能进行新单删除操作！";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	
    	 //如果是银行锁定状态则可以新单删除
        String paymodeSql = "select paymode from lcgrpcont where prtno = '" + mLCGrpContSchema.getPrtNo() + "'";
        ExeSQL exeSql = new ExeSQL();
        String paymode = exeSql.getOneValue(paymodeSql);  //缴费方式为银行转账
        System.out.println("缴费方式为:" + paymode);
        //判断是否是银行锁定
        String cansendbankSql = "select cansendbank from ljspay where otherno = '" + mLCGrpContSchema.getPrtNo() + "'";
        String cansendbank = exeSql.getOneValue(cansendbankSql);
        System.out.println("cansendbank值为1则为银行锁定，当前值为:" + cansendbank);
        
        //如果缴费方式为转账银行paymode =4 并且银行状态已锁定，则可以新单删除并且删除 ljtempfee，ljtempfeeclass，ljspay 信息
        if ("4".equals(paymode) && "1".equals(cansendbank)) {
        	//先获取暂收表的tempfeeno
        	String tempfeenoSql = "select tempfeeno from ljtempfee where otherno = '" + mLCGrpContSchema.getPrtNo() + "'";
        	String tempfeeno = exeSql.getOneValue(tempfeenoSql); 
        	System.out.println("暂收号tempfeeno ：" + tempfeeno);
        	 //备份到B表
        	mMap.put("insert into LJSPayB (select GetNoticeNo, OtherNo, OtherNoType, AppntNo, SumDuePayMoney, PayDate, "
                     + "BankOnTheWayFlag, BankSuccFlag, SendBankCount, ApproveCode, ApproveDate, SerialNo, Operator, MakeDate, "
                     + "MakeTime, ModifyDate, ModifyTime, ManageCom, AgentCom, AgentType, BankCode, BankAccNo, RiskCode, "
                     + "AgentCode, AgentGroup, AccName, StartPayDate, PayTypeFlag, '7', '', '1',markettype,salechnl "
                     + "from LJSPay where GetNoticeNo = '"
                     + tempfeeno + "')","INSERT");
            //删除未到帐的财务暂收记录
        	mMap.put("delete from LJSPay where GetNoticeNo = '"
                     + tempfeeno + "'", "DELETE");
        	mMap.put("delete from ljtempfeeclass where tempfeeno = '"
                     + tempfeeno+"'"+" and EnterAccDate is null ", "DELETE");
        	mMap.put("delete from ljtempfee where tempfeeno = '"
                     + tempfeeno + "'"+" and OtherNoType = '5' and EnterAccDate is null ", "DELETE");
        	
        }
        
        return true;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData() {
        //记录当前操作员
        mResult.clear();
        mResult.add(mMap);
    }
    
    /**
     * 删除共保要素。
     * @param cGrpContNo
     * @return
     */
    private MMap delCoInsuranceInfo(String cGrpContNo)
    {
        MMap tMMap = null;

        CoInsuranceGrpContBL tCoInsuranceGrpContBL = new CoInsuranceGrpContBL();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpContNo", cGrpContNo);
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        tMMap = tCoInsuranceGrpContBL.getSubmitMap(tVData, "Delete");

        if (tMMap == null)
        {
            buildError("delCoInsuranceInfo", "共保要素处理失败。");
            return null;
        }

        return tMMap;
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
    
    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GroupContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
}
