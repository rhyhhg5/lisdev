package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收IntlDownloadContInfoUI.java传入的Sql信息直接进行查询，保证所见即所得。
 * 根据查询得到的保单信息，调用WriteToExcel生成excel。并调用通用下载组件下载生成的excel。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class DownloadES_DOC_MAINBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mEXESql = null;
    private String mOutXmlPath = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public DownloadES_DOC_MAINBL()
    {
    }

    /**
     *外部调用的接口。得到客户端的查询Sql，调用业务逻辑方法dealData生成excel。
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、BString对象：页面查询的sql
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();

        System.out.println(mEXESql);
        SSRS tSSRS = tExeSQL.execSQL(mEXESql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "DownloadES_DOC_MAINBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        String[][] mToExcel = new String[tSSRS.getMaxRow() + 1][10];

        //标题
        mToExcel[0][0] = "扫描号";
        mToExcel[0][1] = "单证号码";
        mToExcel[0][2] = "单证类型";
        mToExcel[0][3] = "页数";
        mToExcel[0][4] = "扫描机构";
        mToExcel[0][5] = "扫描日期";
        mToExcel[0][6] = "扫描时间";
        mToExcel[0][7] = "扫描人";
        mToExcel[0][8] = "状态";
        mToExcel[0][9] = "外包公司";
        
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "DownloadES_DOC_MAINBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mEXESql = (String) tf.getValueByName("EXESql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");

        if(mEXESql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "DownloadES_DOC_MAINBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {

        DownloadES_DOC_MAINBL tDownloadES_DOC_MAINBL = new
            DownloadES_DOC_MAINBL();


    }
}
