package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCContPlanDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class TbGrpContractorBL {
    public TbGrpContractorBL() {
    }

    /** 错误容器 */
    public CErrors mErrors = new CErrors();
    /** 数据容器 */
    private VData mInputData;
    /** 操作符 */
    private String mOperate;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 保险计划要素表 */
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet;
    /** 记录当前操作的GrpContNo */
    private String mGrpContNo;
    /** 险种缓存 */
    private CachedRiskInfo cRiskCached = CachedRiskInfo.getInstance();
    /** 递交数据 */
    private MMap map = new MMap();
    /** 操作结果 */
    private VData mResult = new VData();
    /**
     * 数据接口
     * @param tInputData VData
     * @param tOperate String
     * @return boolean
     */
    public boolean submitData(VData tInputData, String tOperate) {
        this.mInputData = tInputData;
        this.mOperate = tOperate;

        System.out.println("已经进入BL层");

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, this.mOperate)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {

        System.out.println("进入prepareOutputData......");

        if (this.mOperate.equals("INSERT||MAIN")) {
            this.map.put(this.mLCContPlanDutyParamSet, "INSERT");
        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            this.map.put(this.mLCContPlanDutyParamSet, "DELETE");
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            this.map.put(this.mLCContPlanDutyParamSet, "UPDATE");
        }


        this.mResult.add(map);

        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {

        System.out.println("进入dealData......");

        if (this.mOperate.equals("INSERT||MAIN")) {
            System.out.println("执行保存操作");
            if (!insertData()) {
                return false;
            }
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            System.out.println("执行更新操作");
            if (!updateData()) {
                return false;
            }

        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            System.out.println("执行删除操作");
            if (!deleteData()) {
                return false;
            }
        }

        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData() {
        /** 执行删除与执行插入的业务逻辑初始化时相同的, 唯一的不同是在存储map时是执行Delete */
        System.out.println("进入updateData");
        System.out.println("执行更新与执行插入的业务逻辑初始化时相同的, 唯一的不同是在存储map时是执行Delete");
        insertData();
        return true;
    }

    /**
     * deleteData
     *
     * @return boolean
     */
    private boolean deleteData() {
        /** 执行删除与执行插入的业务逻辑初始化时相同的, 唯一的不同是在存储map时是执行Delete */
        System.out.println("进入deleteData");
        System.out.println("执行删除与执行插入的业务逻辑初始化时相同的, 唯一的不同是在存储map时是执行Delete");
        insertData();
        return true;
    }

    /**
     * insertDAta
     *
     * @return boolean
     */
    private boolean insertData() {
        System.out.println("进入insertData");

        LCGrpPolSet tLCGrpPolSet = getLCPolSet();

        if (tLCGrpPolSet == null || tLCGrpPolSet.size() <= 0) {
            buildError("insertData", "没有找到建工险产品！");
            return false;
        }
        LCContPlanDutyParamSet tLCContPlanDutyParamSet = new
                LCContPlanDutyParamSet();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            for (int m = 1; m <= this.mLCContPlanDutyParamSet.size(); m++) {
                mLCContPlanDutyParamSet.get(m).setContPlanCode("11");
                mLCContPlanDutyParamSet.get(m).setContPlanName("默认计划");
                mLCContPlanDutyParamSet.get(m).setDutyCode("000000");
                mLCContPlanDutyParamSet.get(m).setGetDutyCode("000000");
                mLCContPlanDutyParamSet.get(m).setInsuAccNo("000000");
                mLCContPlanDutyParamSet.get(m).setPayPlanCode("000000");
                mLCContPlanDutyParamSet.get(m).setPlanType("0");
                mLCContPlanDutyParamSet.get(m).setGrpPolNo(
                        tLCGrpPolSet.get(i).getGrpPolNo());
                mLCContPlanDutyParamSet.get(m).setRiskCode(
                        tLCGrpPolSet.get(i).getRiskCode());
                mLCContPlanDutyParamSet.get(m).setRiskVersion(
                        getRiskVersion(tLCGrpPolSet.get(i).getRiskCode()));
                mLCContPlanDutyParamSet.get(m).setMainRiskCode(
                        getRiskVersion(tLCGrpPolSet.get(i).getRiskCode()));
                mLCContPlanDutyParamSet.get(m).setMainRiskVersion(
                        tLCGrpPolSet.get(i).getRiskCode());
                mLCContPlanDutyParamSet.get(m).setProposalGrpContNo(mGrpContNo);
            }
            tLCContPlanDutyParamSet.add(PubFun.copySchemaSet(
                    mLCContPlanDutyParamSet));
        }
        mLCContPlanDutyParamSet.clear();
        mLCContPlanDutyParamSet.set(tLCContPlanDutyParamSet);
        return true;
    }

    private LCGrpPolSet getLCPolSet() {
        StringBuffer sql = new StringBuffer(255);
        sql.append("select * from lcgrppol where riskcode in (select riskcode from lmriskapp where risktype8='4') and grpcontno='");
        sql.append(this.mGrpContNo);
        sql.append("'");
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery(sql.toString());
        return tLCGrpPolSet;
    }

    /**
     * getRiskVersion
     *
     * @param tRiskCode String
     * @return String
     */
    private String getRiskVersion(String tRiskCode) {
        LMRiskSchema tLMRiskSchema = cRiskCached.findRiskByRiskCode(tRiskCode);
        return tLMRiskSchema.getRiskVer();
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("进入checkData");

        if (this.mGlobalInput == null) {
            buildError("checkData", "登陆信息为null！");
            return false;
        }

        if (this.mLCContPlanDutyParamSet == null ||
            this.mLCContPlanDutyParamSet.size() <= 0) {
            buildError("checkData", "要素信息为空,请确认是否录入了建工要素信息！");
            return false;
        }

        mGrpContNo = mLCContPlanDutyParamSet.get(1).getGrpContNo();

        if (this.mGrpContNo == null || this.mGrpContNo.equals("")) {
            buildError("checkData", "没有传入保单号码信息！");
            return false;
        }

        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        tLCContPlanDB.setGrpContNo(this.mGrpContNo);
        LCContPlanSet tLCContPlanSet = tLCContPlanDB.query();
        if (tLCContPlanSet == null || tLCContPlanSet.size() <= 0) {
            buildError("dealData", "险种默认要素丢失，请程序员检查添加险种是否存在要素丢失问题！");
            return false;
        }

        for (int i = 1; i <= tLCContPlanSet.size(); i++) {
            if (!tLCContPlanSet.get(i).getContPlanCode().equals("11")) {
                buildError("dealData", "已经添加了保障计划信息,暂时不支持添加保障计划信息后的建工要素添加！");
                return false;
            }
        }

        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("进入getInputData ");
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mLCContPlanDutyParamSet =
                (LCContPlanDutyParamSet) mInputData.
                getObjectByObjectName("LCContPlanDutyParamSet", 0);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "TbGrpContractorBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
