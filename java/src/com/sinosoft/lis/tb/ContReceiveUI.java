/**
 * 2007-11-17
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ContReceiveUI
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    public ContReceiveUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            System.out.println("Start ContReceiveUI Submit ...");
            ContReceiveBL tContReceiveBL = new ContReceiveBL();

            if (!tContReceiveBL.submitData(cInputData, cOperate))
            {
                if (tContReceiveBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tContReceiveBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData", "ContReceiveBL 发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tContReceiveBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "意外错误");
            return false;
        }
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ContReceiveUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.Operator = "001";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PrtNo", "200712120016");
        tTransferData.setNameAndValue("ContNo", "000914186000001");

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        ContReceiveUI tContReceiveUI = new ContReceiveUI();

        if (!tContReceiveUI.submitData(tVData, "CreateReceive"))
        {
            if (tContReceiveUI.mErrors.needDealError())
            {
                System.out.println(tContReceiveUI.mErrors.getFirstError());
            }
        }
        else
        {
            System.out.println("ok...");
        }
    }
}
