package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ContDownloadUI {
	
	public CErrors mErrors = new CErrors();
	
	public String downloadFile(VData cInputData){
		
		ContDownloadBL bl = new ContDownloadBL();
		String tFilePath = bl.downloadFile(cInputData);
		if(tFilePath == null){
			mErrors = bl.mErrors;
			return null;
		}
		
		return tFilePath;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
