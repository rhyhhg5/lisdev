package com.sinosoft.lis.tb;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:客户关系类（界面输入）
 * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在
 * </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author yuanaq
 * @version 1.0
 */

public class CustomerdRelaInfoUI
{

    private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    private LDPersonSet mLDPersonSet;
    public CustomerdRelaInfoUI()
    {
    }

    public static void main(String[] args)
    {
        TransferData mTransferData=new TransferData();
        mTransferData.setNameAndValue("ContNo","130110000013082");
        CustomerdRelaInfoUI er =new CustomerdRelaInfoUI();
        VData tInputData =new VData();
        tInputData.add(mTransferData);
        er.submitData(tInputData,"");
        LCFamilyRelaInfoSet tLCFamilyRelaInfoSet = new LCFamilyRelaInfoSet();
        LCFamilyInfoSet tLCFamilyInfoSet=new LCFamilyInfoSet();
        GetLCFamilyRelaInfo frGetLCFamilyRelaInfo=new GetLCFamilyRelaInfo();
        frGetLCFamilyRelaInfo.GetFamilyRelaInfo(tInputData,"INSERT");
        VData fresultdata =new VData();
        fresultdata=frGetLCFamilyRelaInfo.getResult();
        tLCFamilyRelaInfoSet=(LCFamilyRelaInfoSet)fresultdata.getObjectByObjectName("LCFamilyRelaInfoSet",0);
        tLCFamilyInfoSet=(LCFamilyInfoSet)fresultdata.getObjectByObjectName("LCFamilyInfoSet",0);

         MMap map = new MMap();
        if(tLCFamilyRelaInfoSet.size()>0)
        {
        map.put(tLCFamilyRelaInfoSet, "INSERT");
        }
        if(tLCFamilyInfoSet.size()>0)
        {
        map.put(tLCFamilyInfoSet, "INSERT");
        }

        VData mInputData =new VData();
        mInputData.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "INSERT");

    }
    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        CustomerdRelaInfoBL tCustomerdRelaInfoBL= new CustomerdRelaInfoBL();
        tCustomerdRelaInfoBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tCustomerdRelaInfoBL.mErrors.needDealError())
        this.mErrors.copyAllErrors(tCustomerdRelaInfoBL.mErrors);
        //取得结果返回前台
        this.mResult.clear();
        this.mResult = tCustomerdRelaInfoBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
