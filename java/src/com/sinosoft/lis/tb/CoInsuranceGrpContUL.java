/**
 * 2008-10-25
 */
package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CoInsuranceGrpContUL
{
    public CErrors mErrors = new CErrors();

    public CoInsuranceGrpContUL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            CoInsuranceGrpContBL tCoInsuranceGrpContBL = new CoInsuranceGrpContBL();
            if (!tCoInsuranceGrpContBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCoInsuranceGrpContBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
