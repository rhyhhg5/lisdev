package com.sinosoft.lis.tb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOInsuredUI
{
    public CErrors mErrors = new CErrors();

    public BPOInsuredUI()
    {
    }

    public boolean submitData(VData data, String operator)
    {
        BPOInsuredBL tBPOInsuredBL = new BPOInsuredBL();
        if(!tBPOInsuredBL.submitData(data, operator))
        {
            mErrors.copyAllErrors(tBPOInsuredBL.mErrors);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        BPOInsuredUI bpoinsuredui = new BPOInsuredUI();
    }
}
