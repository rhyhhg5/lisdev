package com.sinosoft.lis.tb;


import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class HJRiskBL {
    /** 数据操作字符串 */
    private String mOperateType;
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();//返回结果
    LDCodeSchema mLDCodeSchema = new LDCodeSchema();
    
    LDCodeSet lDCodeSet = new LDCodeSet();
    
    public boolean submitData(VData cInputData, String cOperate ){
//    	将操作数据拷贝到本类中
    	
        this.mOperateType = cOperate;
        mInputData = (VData)cInputData.clone();
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkData()){
        	return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult,null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }
    private boolean getInputData(VData cInputData){
    	lDCodeSet = (LDCodeSet) cInputData.getObjectByObjectName(
                "LDCodeSet", 1);
    	return true;
    }
    private boolean checkData() {
    	LDCodeDB mLDCodeDB = new LDCodeDB(); 
    	if("INSERT".equals(mOperateType)){
    		mLDCodeDB.setCode(lDCodeSet.get(1).getCode());
        	mLDCodeDB.setCodeType("grphjrisk");
        	if(mLDCodeDB.getInfo()){
        		mErrors.addOneError("已经存在此险种编码，不能重复添加");
        		return false;
        	}
    	}
    	
    	return true;
    }
    private boolean dealData() {
    	
    	return true;
    }
    private boolean prepareOutputData(){
    	MMap map = new MMap();
    	
    	map.put(lDCodeSet,mOperateType);
    	//mResult = new VData();
    	mResult.add(map);
    	return true;
    }
}
