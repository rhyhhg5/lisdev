package com.sinosoft.lis.tb;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:投保功能类（界面输入）
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YT
 * @version 1.0
 */
public class ProposalGrpUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;


    public ProposalGrpUI() {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
    

    	ProposalGrpBL tProposalGrpBL = new ProposalGrpBL();
    	tProposalGrpBL.submitData(cInputData, cOperate);

        //如果有需要处理的错误，则返回
        if (tProposalGrpBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tProposalGrpBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ProposalUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        mResult = tProposalGrpBL.getResult();

        return true;
    }

    public VData getResult() {
        return mResult;
    }
}
