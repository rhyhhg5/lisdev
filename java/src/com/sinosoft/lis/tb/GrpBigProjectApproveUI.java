package com.sinosoft.lis.tb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class GrpBigProjectApproveUI {
    public GrpBigProjectApproveUI() {
    }
    private VData mResult = new VData();

    public CErrors mErrors = new CErrors();

    public boolean submitData(VData nInputData, String cOperate) {
        GrpBigProjectApproveBL tGrpBigProjectApproveBL = new
                GrpBigProjectApproveBL();
        if (!tGrpBigProjectApproveBL.submitData(nInputData, cOperate)) {
            this.mErrors.copyAllErrors(tGrpBigProjectApproveBL.mErrors);
            return false;
        } else {
            mResult = tGrpBigProjectApproveBL.getResult();
        }
        return true;
    }

    /**
     * �������
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

}
