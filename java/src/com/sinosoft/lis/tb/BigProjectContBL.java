package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCBigProjectYearSchema;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class BigProjectContBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	private VData mInputData2 = new VData();

	/** 数据操作字符串 */
	private String mOperate;
	
	//是否要存储大项目年度标记:"Y",是；"N",不是。
    private String isNewBPYear = "";
	
	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	//录入
	private LCBigProjectContSet mLCBigProjectContSet = new LCBigProjectContSet();
	private LCBigProjectContTrackSet mLCBigProjectContTrackSet = new LCBigProjectContTrackSet();
	private LCBigProjectYearSchema mLCBigProjectYearSchema = new LCBigProjectYearSchema();

	private MMap mMap = new MMap();
	private MMap mMap2 = new MMap();
	
	public BigProjectContBL(){}
	
	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		if(!checkData()){
			return false;
		}
		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BigProjectContBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectContBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		PubSubmit tPubSubmit2 = new PubSubmit();
		System.out.println("Start BigProjectContBL Submit...");
		if("INSERT||MAIN".equals(mOperate)){
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "BigProjectContBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}else if("DELETE||MAIN".equals(mOperate)){
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "BigProjectContBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			if (!tPubSubmit2.submitData(mInputData2,"INSERT||MAIN")) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit2.mErrors);
				CError tError = new CError();
				tError.moduleName = "BigProjectContBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		
		mInputData = null;
		return true;
	}
	
	private boolean getInputData(VData cInputData) {
		this.mLCBigProjectContSet.set((LCBigProjectContSet) cInputData.getObjectByObjectName("LCBigProjectContSet", 0));
		this.mLCBigProjectContTrackSet.set((LCBigProjectContTrackSet) cInputData.getObjectByObjectName("LCBigProjectContTrackSet", 0));
		mLCBigProjectYearSchema = (LCBigProjectYearSchema)cInputData.getObjectByObjectName("LCBigProjectYearSchema", 0);
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		if(mLCBigProjectContSet == null || mLCBigProjectContSet.size()<1){
			CError tError = new CError();
			tError.moduleName = "BigProjectContBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "未获取到相应的数据！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	private boolean checkData(){
		if("INSERT||MAIN".equals(mOperate)){
			if(mLCBigProjectYearSchema.getBigProjectNo() == null || "".equals(mLCBigProjectYearSchema.getBigProjectNo())){
				isNewBPYear = "N";
			}else{
				isNewBPYear = "Y";
			}
		}
	    return true;
	}
	
	private boolean dealData() {
		// 轨迹表并不删除记录，仅以保单归属状态（00：归属；01解除归属）进行添加。
		if("INSERT||MAIN".equals(mOperate)){
			mMap.put(this.mLCBigProjectContSet, SysConst.INSERT);
			mMap.put(this.mLCBigProjectContTrackSet, SysConst.INSERT);
			if("Y".equals(isNewBPYear)){
				mMap.put(this.mLCBigProjectYearSchema, SysConst.INSERT);
    		}
		}else if(mOperate.equals("DELETE||MAIN")){
			mMap.put(this.mLCBigProjectContSet, SysConst.DELETE);
			mMap2.put(this.mLCBigProjectContTrackSet, SysConst.INSERT);
		}
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			if("INSERT||MAIN".equals(mOperate)){
				mInputData = new VData();
				this.mInputData.add(mMap);
			}else if("DELETE||MAIN".equals(mOperate)){
				mInputData = new VData();
				this.mInputData.add(mMap);
				mInputData2 = new VData();
				this.mInputData2.add(mMap2);
			}
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BigProjectContBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}


	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}
}
