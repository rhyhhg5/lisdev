package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.GrpBalanceOnTime;
import com.sinosoft.utility.*;

/**
 * <p>Title: 新契约-团单投保单处理逻辑</p>
 * <p>Description: 新契约-团单投保单处理逻辑</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Yangming
 * @version 1.0
 * 修 改 人：闫少杰
 * 修改日期：2006-03-13
 * 修改内容:
 *          1 修改约定缴费的处理方式：保存在特约信息改为用LCGrpSpecFee表来保存相关内容。
 */
public class GroupContBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    private String mCustomerNO = ""; //用于将CustomerNo传送到告知数据库中

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    Reflections ref = new Reflections();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();

    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    private LDGrpSchema mLDGrpSchema = new LDGrpSchema();

    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();

    private LCCustomerImpartParamsSet mLCCustomerImpartParamsSet = new LCCustomerImpartParamsSet();

    private LCHistoryImpartSet mLCHistoryImpartSet = new LCHistoryImpartSet();

    private LCDiseaseImpartSet mLCDiseaseImpartSet = new LCDiseaseImpartSet();

    private LCDiseaseImpartSchema mLCDiseaseImpartSchema = new LCDiseaseImpartSchema();

    private LCHistoryImpartSchema mLCHistoryImpartSchema = new LCHistoryImpartSchema();

    private LCGrpServInfoSet mLCGrpServInfoSet = new LCGrpServInfoSet();

    private LCCustomerImpartSchema mLCCustomerImpartSchema = new LCCustomerImpartSchema();

    //2006-03-13 闫少杰 修改约定缴费的处理方式 ----- START
    private LCGrpSpecFeeSet mLCGrpSpecFeeSet = new LCGrpSpecFeeSet();
    
    //2006-03-13 闫少杰 修改约定缴费的处理方式 ----- END
    
    private LCCustomerImpartDetailSet mLCCustomerImpartDetailSet = new LCCustomerImpartDetailSet();
    
    private LCGrpContBnfSet mLCGrpContBnfSet = new LCGrpContBnfSet();
    
    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();

    private String theCurrentTime = PubFun.getCurrentTime();
    
    //by gzh 20130408 对综合开拓数据处理
    private LCExtendSchema mLCExtendSchema = new LCExtendSchema();

    public GroupContBL()
    {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("begin submit here:");
        //将VData数据还原成业务需要的类
        if (this.getInputData() == false)
        {
            return false;
        }
        System.out.println("---getInputData successful---");
        if (this.checkData() == false)
        {
            return false;
        }
        System.out.println("---checkData successful---");
        if (this.dealData() == false)
        {
            return false;
        }
        System.out.println("---dealdata successful---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("map " + map.size());
        //tPubSubmit.submitData(mInputData, cOperate);
        if (tPubSubmit.submitData(mInputData, cOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData()
    {
        //全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            mErrors.addOneError(new CError("没有得到全局量信息"));
            return false;
        }
        //团体保单实例
        mLCGrpContSchema.setSchema((LCGrpContSchema) mInputData
                .getObjectByObjectName("LCGrpContSchema", 0));
        //团单投保人实例
        mLCGrpAppntSchema.setSchema((LCGrpAppntSchema) mInputData
                .getObjectByObjectName("LCGrpAppntSchema", 0));
        //团体客户地址实例
        mLCGrpAddressSchema.setSchema((LCGrpAddressSchema) mInputData
                .getObjectByObjectName("LCGrpAddressSchema", 0));
        //团体客户实例
        mLDGrpSchema.setSchema((LDGrpSchema) mInputData.getObjectByObjectName(
                "LDGrpSchema", 0));
        //团体告知
        mLCCustomerImpartSet.set((LCCustomerImpartSet) mInputData
                .getObjectByObjectName("LCCustomerImpartSet", 0));
        //既往情况告知
        mLCHistoryImpartSet.set((LCHistoryImpartSet) mInputData
                .getObjectByObjectName("LCHistoryImpartSet", 0));
        //严重疾病告知
        mLCDiseaseImpartSet.set((LCDiseaseImpartSet) mInputData
                .getObjectByObjectName("LCDiseaseImpartSet", 0));
        //客户服务信息
        mLCGrpServInfoSet.set((LCGrpServInfoSet) mInputData
                .getObjectByObjectName("LCGrpServInfoSet", 0));
        //预定缴费方式
        //System.out.println("约定缴费方式~");
        mLCGrpSpecFeeSet.set((LCGrpSpecFeeSet) mInputData
                .getObjectByObjectName("LCGrpSpecFeeSet", 0));
        mLCExtendSchema = (LCExtendSchema) mInputData.getObjectByObjectName("LCExtendSchema", 0);
        if(mLCExtendSchema == null ){
        	mLCExtendSchema = new LCExtendSchema();
        }
        this.mLCCustomerImpartDetailSet.set((LCCustomerImpartDetailSet)mInputData
                .getObjectByObjectName("LCCustomerImpartDetailSet", 0));
      //受益所有人
        mLCGrpContBnfSet.set((LCGrpContBnfSet) mInputData.
        		getObjectByObjectName("LCGrpContBnfSet", 0));
        TransferData tTransferData = (TransferData) mInputData
                .getObjectByObjectName("TransferData", 0);
        System.out.println("end getinput here:");
        return true;
    }

    /**
     * 处理保存逻辑，保存处理过的数据
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean insertData()
    {
        //校验代理人编码并置上代理人组别
        if (mLCGrpContSchema.getAgentCode() == null
                || mLCGrpContSchema.getAgentCode().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "dealData";
            tError.errorMessage = "请您确认有：代理人编码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mLCGrpContSchema.getAgentCode());
            if (tLAAgentDB.getInfo() == false)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "请您确认：代理人编码没有输入错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLAAgentDB.getManageCom() == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "代理人编码对应数据库中的管理机构为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!tLAAgentDB.getManageCom().equals(
                    mLCGrpContSchema.getManageCom()))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "您录入的管理机构和数据库中代理人编码对应的管理机构不符合！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLCGrpContSchema.setAgentGroup(tLAAgentDB.getAgentGroup());

        }

        boolean customerFlag = false;
        boolean addressFlag = false;
        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        
      //社保渠道,非社保渠道,投保单位名称校验
        if(mLCGrpAppntSchema.getCustomerNo()==null ||mLCGrpAppntSchema.getCustomerNo().trim().equals("")){
        	System.out.println("客户名称:"+mLDGrpSchema.getGrpName()+"  长度:"+mLDGrpSchema.getGrpName().length()); 
        	   String tName="";
        	   String mName="";
         	 String pName="";
         	String tSql2="";
         	String tSql3="";
         	String tSql5="";
         	String tCodeName2="";
         	String tCodeName1="";
         	String tCodeName3="";
        	 String tSale=mLCGrpContSchema.getSaleChnl();
             SSRS tSSRS = new SSRS();
     		  ExeSQL tExeSQL = new ExeSQL();
     		 if(tSale.equals("16")){
             	 //  String tSql=" select  code from ldcode  where codetype ='GrpCustSalechnlChk' and code='"+tSale+"'";
         			 String tSql4="select  customerno  from ldgrp where  grpname='"+mLDGrpSchema.getGrpName()+"'"; 
         			 if(tSql4!=null || tSql4!=""){
              	   String tCustomerNo=tExeSQL.getOneValue(tSql4);
    	          	mCustomerNO = tCustomerNo;
    	          	mLCGrpContSchema.setAppntNo(mCustomerNO);
    	          	mLCGrpAppntSchema.setCustomerNo(mCustomerNO);
    	          	mLDGrpSchema.setCustomerNo(mCustomerNO);
    	          	mLCGrpAddressSchema.setCustomerNo(mCustomerNO);
    	         	}
         		 } else if(tSale!="16" &&  mLDGrpSchema.getGrpName().length()>=4 ){
         			  //后两位
                    tName=mLDGrpSchema.getGrpName().substring(mLDGrpSchema.getGrpName().length()-1, mLDGrpSchema.getGrpName().length());     		 
                      //最后一位  
                    mName=mLDGrpSchema.getGrpName().substring(mLDGrpSchema.getGrpName().length());                 
                    //后三位
              	    pName=mLDGrpSchema.getGrpName().substring(mLDGrpSchema.getGrpName().length()-2, mLDGrpSchema.getGrpName().length()) ; 	 
                   tSql2="select  codename from ldcode where codetype='GrpCustChk2' and  codename=' "+tName+"' ";
               	   tSql3="select codename from ldcode where codetype='GrpCustChk1' and  codename='"+mName+"' ";
               	   tSql5="select  codename from ldcode where codetype='GrpCustChk2' and  codename='"+pName+"' ";
               	     tCodeName2=tExeSQL.getOneValue(tSql2);
                     tCodeName1=tExeSQL.getOneValue(tSql3);
                     tCodeName3=tExeSQL.getOneValue(tSql5);
                      if(tCodeName2!=null){
                    	  String tSql4="select  customerno  from ldgrp where  grpname='"+mLDGrpSchema.getGrpName()+"'"; 
                    	  if(tSql4!=null){
                         	   String tCustomerNo=tExeSQL.getOneValue(tSql4);
               	          	mCustomerNO = tCustomerNo;
               	          	mLCGrpContSchema.setAppntNo(mCustomerNO);
               	          	mLCGrpAppntSchema.setCustomerNo(mCustomerNO);
               	          	mLDGrpSchema.setCustomerNo(mCustomerNO);
               	          	mLCGrpAddressSchema.setCustomerNo(mCustomerNO);
               	          	System.out.println("后两位的"+tCustomerNo);
                           }
                      }
                      else if(tCodeName3!=null ){
                    	  String tSql4="select  customerno  from ldgrp where  grpname='"+mLDGrpSchema.getGrpName()+"'"; 
                    	  if(tSql4!=null){
                         	   String tCustomerNo=tExeSQL.getOneValue(tSql4);
               	          	mCustomerNO = tCustomerNo;
               	          	mLCGrpContSchema.setAppntNo(mCustomerNO);
               	          	mLCGrpAppntSchema.setCustomerNo(mCustomerNO);
               	          	mLDGrpSchema.setCustomerNo(mCustomerNO);
               	          	mLCGrpAddressSchema.setCustomerNo(mCustomerNO);
               	         System.out.println("后3位的");
                           }
                      }
                      else if (tCodeName1!=null)
                       {        String tSql4="select  customerno  from ldgrp where  grpname='"+mLDGrpSchema.getGrpName()+"'"; 
    			            	  if(tSql4!=null){
    			                	   String tCustomerNo=tExeSQL.getOneValue(tSql4);
    			      	          	mCustomerNO = tCustomerNo;
    			      	          	mLCGrpContSchema.setAppntNo(mCustomerNO);
    			      	          	mLCGrpAppntSchema.setCustomerNo(mCustomerNO);
    			      	          	mLDGrpSchema.setCustomerNo(mCustomerNO);
    			      	          	mLCGrpAddressSchema.setCustomerNo(mCustomerNO);
    			      	          System.out.println("后1位的");
    			                         }
                    	   
                        }
                                                                     		 
         		 } 
     }     


        //如果缺少客户号码，生成一个
        if (mLCGrpAppntSchema.getCustomerNo() == null
                || mLCGrpAppntSchema.getCustomerNo().trim().equals(""))
        {
            customerFlag = true;
            tLimit = "SN";
            String CustomerNO = PubFun1.CreateMaxNo("GRPNO", tLimit);
            if (!CustomerNO.equals(""))
            {
                mCustomerNO = CustomerNO;
                mLCGrpContSchema.setAppntNo(CustomerNO);
                mLCGrpAppntSchema.setCustomerNo(CustomerNO);
                mLDGrpSchema.setCustomerNo(CustomerNO);
                mLCGrpAddressSchema.setCustomerNo(CustomerNO);
            }
            else
            {
                mErrors.addOneError(new CError("客户号码生成失败"));
                return false;
            }
            //
        }

        //生成ProposalGrpContNo
        String tProposalGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo",
                mLCGrpAppntSchema.getCustomerNo());
        if (!tProposalGrpContNo.equals(""))
        {
            mLCGrpContSchema.setProposalGrpContNo(tProposalGrpContNo);
            mLCGrpContSchema.setGrpContNo(tProposalGrpContNo);
            mLCGrpAppntSchema.setGrpContNo(tProposalGrpContNo);
        }
        else
        {
            mErrors.addOneError(new CError("集体投保单号生成失败"));
            return false;
        }

        //如果缺少地址号码，生成一个
        if (mLCGrpContSchema.getAddressNo() == null
                || mLCGrpContSchema.getAddressNo().trim().equals(""))
        {
            try
            {
                addressFlag = true;
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(integer(AddressNo)) Is Null Then 0 Else max(integer(AddressNo)) End from LCGrpAddress where CustomerNo='"
                        + mLCGrpAppntSchema.getCustomerNo() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                String AddressNo = integer.toString();
                System.out.println("得到的地址码是：" + AddressNo);
                if (!AddressNo.equals(""))
                {
                    mLCGrpContSchema.setAddressNo(AddressNo);
                    mLCGrpAppntSchema.setAddressNo(AddressNo);
                    mLCGrpAddressSchema.setAddressNo(AddressNo);
                }
                else
                {
                    mErrors.addOneError(new CError("客户地址号码生成失败"));
                    return false;
                }
            }
            catch (Exception e)
            {
                CError tError = new CError();
                tError.moduleName = "groupContBL";
                tError.functionName = "createAddressNo";
                tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
                this.mErrors.addOneError(tError);
            }
        }
        if (mLCGrpContSchema.getAppFlag() == null
                || mLCGrpContSchema.getAppFlag().equals(""))
        {
            mLCGrpContSchema.setAppFlag("0");
            mLCGrpContSchema.setStateFlag("0");
        }
        if (StrTool.cTrim(mLCGrpContSchema.getPolApplyDate()).equals(""))
        {
            mLCGrpContSchema.setPolApplyDate(theCurrentDate);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getInputDate()).equals(""))
        {
            mLCGrpContSchema.setInputDate(theCurrentDate);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getInputTime()).equals(""))
        {
            mLCGrpContSchema.setInputTime(theCurrentTime);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getAppFlag()).equals("8"))
        {
            String AskGrpContNo = PubFun1.CreateMaxNo("AskGrpContNo",
                    mLCGrpAppntSchema.getCustomerNo());
            if (AskGrpContNo.trim().equals(""))
            {
                mErrors.addOneError(new CError("询价号生成失败!"));
                return false;
            }
            mLCGrpContSchema.setAskGrpContNo(AskGrpContNo);
        }
        mLCGrpContSchema.setApproveFlag("0");
        mLCGrpContSchema.setUWFlag("0");
        mLCGrpContSchema.setSpecFlag("0");
        //记录入机日期
        mLCGrpContSchema.setMakeDate(theCurrentDate);
        mLCGrpAppntSchema.setMakeDate(theCurrentDate);
        //记录入机时间
        mLCGrpContSchema.setMakeTime(theCurrentTime);
        mLCGrpAppntSchema.setMakeTime(theCurrentTime);
        if (customerFlag)
        {
            //记录入机日期

            mLDGrpSchema.setMakeDate(theCurrentDate);
            mLDGrpSchema.setMakeTime(theCurrentTime);
            map.put(mLDGrpSchema, "INSERT");
        }
        if (addressFlag)
        {
            //记录入机时间
            mLCGrpAddressSchema.setMakeTime(theCurrentTime);
            mLCGrpAddressSchema.setMakeDate(theCurrentDate);
            map.put(mLCGrpAddressSchema, "INSERT");
        }
       
        	/** 如果页面没有传入行业性质,就从数据库里查询出来添上 */
            if (StrTool.cTrim(mLCGrpContSchema.getBusinessBigType()).equals(""))
            {
                String sql = "select CodeAlias from ldcode where codetype='businesstype'"
                        + " and code='" + mLCGrpContSchema.getBusinessType() + "'";
                String bigType = (new ExeSQL()).getOneValue(sql);
                
                //如果投保人属性为自然人行业编码和企业类型编码可以为空
                String sql2 = "select * from LCGrpAppnt where grpcontno = '" + mLCGrpContSchema.getBusinessType() + "'";
                String bigType2 = (new ExeSQL()).getOneValue(sql2);
                if ("1".equals(bigType2)) {
                	 if (StrTool.cTrim(bigType).equals("")
                             || StrTool.cTrim(bigType).equals("null"))
                     {
                         buildError("insertData", "行业性质录入错误！");
                         return false;
                     }
				}
               
                mLCGrpContSchema.setBusinessBigType(bigType);
            }
        
        
        map.put(mLCGrpContSchema, "INSERT");
        map.put(mLCGrpAppntSchema, "INSERT");

        if (mLCCustomerImpartSet != null && mLCCustomerImpartSet.size() > 0)
        {
            System.out.println("asdfjasdfja;dfljka");
            for (int i = 1; i <= mLCCustomerImpartSet.size(); i++)
            {
                System.out.println("-----------mCustomerNO-----------"
                        + mCustomerNO);
                if (mCustomerNO != "")
                {
                    mLCCustomerImpartSet.get(i).setCustomerNo(mCustomerNO);
                    mLCCustomerImpartSet.get(i).setContNo(SysConst.ZERONO);
                    mLCCustomerImpartSet.get(i).setGrpContNo(
                            mLCGrpContSchema.getGrpContNo());
                    mLCCustomerImpartSet.get(i).setPrtNo(
                            mLCGrpContSchema.getPrtNo());
                    mLCCustomerImpartSet.get(i).setProposalContNo(
                            SysConst.ZERONO);
                    mLCCustomerImpartSet.get(i).setOperator(
                            mGlobalInput.Operator);
                    mLCCustomerImpartSet.get(i).setMakeDate(
                            PubFun.getCurrentDate());
                    mLCCustomerImpartSet.get(i).setMakeTime(
                            PubFun.getCurrentTime());
                    mLCCustomerImpartSet.get(i).setModifyDate(
                            PubFun.getCurrentDate());
                    mLCCustomerImpartSet.get(i).setModifyTime(
                            PubFun.getCurrentTime());
                }
                if (mLCCustomerImpartSet.get(i).getCustomerNo() != "")
                {
                    mLCCustomerImpartSet.get(i).setContNo(SysConst.ZERONO);
                    mLCCustomerImpartSet.get(i).setGrpContNo(
                            mLCGrpContSchema.getGrpContNo());
                    mLCCustomerImpartSet.get(i).setPrtNo(
                            mLCGrpContSchema.getPrtNo());
                    mLCCustomerImpartSet.get(i).setProposalContNo(
                            SysConst.ZERONO);
                    mLCCustomerImpartSet.get(i).setOperator(
                            mGlobalInput.Operator);
                    mLCCustomerImpartSet.get(i).setMakeDate(
                            PubFun.getCurrentDate());
                    mLCCustomerImpartSet.get(i).setMakeTime(
                            PubFun.getCurrentTime());
                    mLCCustomerImpartSet.get(i).setModifyDate(
                            PubFun.getCurrentDate());
                    mLCCustomerImpartSet.get(i).setModifyTime(
                            PubFun.getCurrentTime());
                }
                System.out.println("asdfjasdfja;7896979789789");
            }
            CustomerImpartBL mCustomerImpartBL = new CustomerImpartBL();
            VData tempVData = new VData();
            tempVData.add(mLCCustomerImpartSet);
            tempVData.add(mGlobalInput);
            mCustomerImpartBL.submitData(tempVData, "IMPART||DEAL");
            if (mCustomerImpartBL.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "GroupContBL";
                tError.functionName = "insert";
                tError.errorMessage = mCustomerImpartBL.mErrors.getFirstError()
                        .toString();
                this.mErrors.addOneError(tError);
                return false;
            }
            tempVData.clear();
            tempVData = mCustomerImpartBL.getResult();
            if (null != (LCCustomerImpartSet) tempVData.getObjectByObjectName(
                    "LCCustomerImpartSet", 0))
            {
                mLCCustomerImpartSet = (LCCustomerImpartSet) tempVData
                        .getObjectByObjectName("LCCustomerImpartSet", 0);
                System.out.println("告知条数" + mLCCustomerImpartSet.size());
            }
            else
            {
                System.out.println("告知条数为空");
            }
            if (null != (LCCustomerImpartParamsSet) tempVData
                    .getObjectByObjectName("LCCustomerImpartParamsSet", 0))
            {
                mLCCustomerImpartParamsSet = (LCCustomerImpartParamsSet) tempVData
                        .getObjectByObjectName("LCCustomerImpartParamsSet", 0);
            }

            if (mLCCustomerImpartSet != null)
            {
                map.put(mLCCustomerImpartSet, "INSERT");

            }
            if (mLCCustomerImpartParamsSet != null)
            {
                map.put(mLCCustomerImpartParamsSet, "INSERT");
            }
        }

        //System.out.println(mLCHistoryImpartSet.get(1).getInsuYear());
        if (mLCHistoryImpartSet != null && mLCHistoryImpartSet.size() > 0)
        {
            FDate tFDate = new FDate();
            for (int i = 1; i <= mLCHistoryImpartSet.size(); i++)
            {
                /** 判断开始和起始时间是否录入 */
                //miaoxz于2009-9-21注释掉了下面的程序校验，因团险投保书变更所致
            	/**if (StrTool
                        .cTrim(mLCHistoryImpartSet.get(i).getInsuStartYear())
                        .equals("")
                        || StrTool.cTrim(
                                mLCHistoryImpartSet.get(i).getInsuEndYear())
                                .equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "GrpContBL";
                    tError.functionName = "dealData";
                    //tError.errorMessage = "请录入保障情况告知的保障起始时间和保障终止时间";
                    tError.errorMessage = "请录入保障情况告知的起始时间和终止时间，且格式应该为YYYY-MM-DD";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                /** 开始和起始时间的先后顺序 */
            	 /**if (!tFDate.getDate(
                        mLCHistoryImpartSet.get(i).getInsuStartYear()).before(
                        tFDate.getDate(mLCHistoryImpartSet.get(i)
                                .getInsuEndYear())))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GrpContBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "录入保障情况告知的保障起始时间大于保障终止时间";
                    this.mErrors.addOneError(tError);
                    return false;
                }*/
                mLCHistoryImpartSet.get(i).setGrpContNo(
                        mLCGrpContSchema.getGrpContNo());
                String tHisImpSerlNo = PubFun1.CreateMaxNo("HisImpSerlNo", 20);
                if (tHisImpSerlNo.trim().equals(""))
                {
                    mErrors.addOneError(new CError("生成HisImpSerlNo失败"));
                    return false;
                }
                mLCHistoryImpartSet.get(i).setSerialNo(tHisImpSerlNo);
                mLCHistoryImpartSet.get(i)
                        .setPrtNo(mLCGrpContSchema.getPrtNo());
                mLCHistoryImpartSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLCHistoryImpartSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLCHistoryImpartSet.get(i).setModifyDate(
                        PubFun.getCurrentDate());
                mLCHistoryImpartSet.get(i).setModifyTime(
                        PubFun.getCurrentTime());
                //System.out.println(mLCHistoryImpartSet.get(i).getPeoples());
            }

            if (mLCHistoryImpartSet != null)
            {
                map.put(mLCHistoryImpartSet, "INSERT");
            }
        }
        
//      处理告知明细信息
        if (mLCCustomerImpartDetailSet != null &&
            mLCCustomerImpartDetailSet.size() > 0) {
            //设置所有告知明细信息得客户号码
            for (int i = 1; i <= mLCCustomerImpartDetailSet.size(); i++) {
                if (mLCCustomerImpartDetailSet.get(i).getImpartVer().trim().
                    equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getImpartCode().trim().
                    equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getImpartDetailContent().
                    trim().equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getDiseaseContent().trim().
                    equals("")
                        ) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "dealData";
                    tError.errorMessage =
                            "基本告知或健康告知中详细告知的告知版本，告知编码，告知项目，说明内容录入不能为空，请检查！";
                    this.mErrors.addOneError(tError);
                    break;
                }
                //增加对同一告知不同告知内容的存储
                for (int count = 1; count <= mLCCustomerImpartDetailSet.size();
                                 count++) {
                    if (StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartVer()).equals(
                                              mLCCustomerImpartDetailSet.get(
                            count).getImpartVer())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartCode()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getImpartCode())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getDiseaseContent()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getDiseaseContent())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getStartDate()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getStartDate())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getEndDate()).
                        equals(mLCCustomerImpartDetailSet.get(count).getEndDate())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getProver()).
                        equals(mLCCustomerImpartDetailSet.get(count).getProver())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getCurrCondition()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getCurrCondition())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getIsProved()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getIsProved())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartDetailContent()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getImpartDetailContent()
                        ) & count != i
                            ) {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ContInsuredBL";
                        tError.functionName = "dealData";
                        tError.errorMessage =
                                "基本告知或健康告知中详细告知的告知版本，告知编码，告知项目，说明内容录入重复，请检查！";
                        this.mErrors.addOneError(tError);
                        break;
                    }
                }
                //tNo = PubFun1.CreateMaxNo("SubSerialNo", 7);
//                System.out.println("得到的告知码是：" + tNo);
                mLCCustomerImpartDetailSet.get(i).setSubSerialNo(String.valueOf(
                        i));
                mLCCustomerImpartDetailSet.get(i).setGrpContNo(mLCGrpContSchema.
                        getGrpContNo());
                mLCCustomerImpartDetailSet.get(i).setContNo("00000000000000000000");
                mLCCustomerImpartDetailSet.get(i).setPrtNo(mLCGrpContSchema.
                        getPrtNo());
                mLCCustomerImpartDetailSet.get(i).setProposalContNo(
                		"00000000000000000000");
                mLCCustomerImpartDetailSet.get(i).setCustomerNo(
                        mLCGrpAppntSchema.
                        getCustomerNo());
                mLCCustomerImpartDetailSet.get(i).setOperator(mGlobalInput.
                        Operator);
                mLCCustomerImpartDetailSet.get(i).setMakeDate(theCurrentDate);
                mLCCustomerImpartDetailSet.get(i).setMakeTime(theCurrentTime);
                mLCCustomerImpartDetailSet.get(i).setModifyDate(theCurrentDate);
                mLCCustomerImpartDetailSet.get(i).setModifyTime(theCurrentTime);
            }
            if (mLCCustomerImpartDetailSet != null)
            {
                map.put(mLCCustomerImpartDetailSet, "INSERT");
            }
        }
        
        if (mLCGrpContBnfSet != null && mLCGrpContBnfSet.size()>0){
        	for(int i=1;i<=mLCGrpContBnfSet.size();i++){
        		mLCGrpContBnfSet.get(i).setPrtNo(mLCGrpContSchema.getPrtNo());
        		mLCGrpContBnfSet.get(i).setOperator(mGlobalInput.Operator);
        		mLCGrpContBnfSet.get(i).setMakeDate(theCurrentDate);
        		mLCGrpContBnfSet.get(i).setMakeTime(theCurrentTime);
        		mLCGrpContBnfSet.get(i).setModifyDate(theCurrentDate);
        		mLCGrpContBnfSet.get(i).setModifyTime(theCurrentTime);
        	}
        	if(mLCGrpContBnfSet != null){
        		map.put(mLCGrpContBnfSet, "INSERT");
        	}
        }
        
        if (mLCDiseaseImpartSet != null && mLCDiseaseImpartSet.size() > 0)
        {
            for (int i = 1; i <= mLCDiseaseImpartSet.size(); i++)
            {
                mLCDiseaseImpartSet.get(i).setGrpContNo(
                        mLCGrpContSchema.getGrpContNo());
                String tDisImpSerlNo = PubFun1.CreateMaxNo("DisImpSerlNo", 20);
                if (tDisImpSerlNo.trim().equals(""))
                {
                    mErrors.addOneError(new CError("生成DisImpSerlNo失败"));
                    return false;
                }
                mLCDiseaseImpartSet.get(i).setSerialNo(tDisImpSerlNo);
                mLCDiseaseImpartSet.get(i)
                        .setPrtNo(mLCGrpContSchema.getPrtNo());
                mLCDiseaseImpartSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLCDiseaseImpartSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLCDiseaseImpartSet.get(i).setModifyDate(
                        PubFun.getCurrentDate());
                mLCDiseaseImpartSet.get(i).setModifyTime(
                        PubFun.getCurrentTime());
                //System.out.println(mLCHistoryImpartSet.get(i).getPeoples());
            }

            if (mLCDiseaseImpartSet != null)
            {
                map.put(mLCDiseaseImpartSet, "INSERT");
            }
        }
        //客户服务信息
        if (mLCGrpServInfoSet != null && mLCGrpServInfoSet.size() > 0)
        {
            System.out.println("--Star servInfo--");
            for (int i = 1; i <= mLCGrpServInfoSet.size(); i++)
            {
                mLCGrpServInfoSet.get(i).setGrpContNo(
                        mLCGrpContSchema.getGrpContNo());
                mLCGrpServInfoSet.get(i).setProposalGrpContNo(
                        mLCGrpContSchema.getProposalGrpContNo());
                mLCGrpServInfoSet.get(i).setPrtNo(mLCGrpContSchema.getPrtNo());
                mLCGrpServInfoSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLCGrpServInfoSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLCGrpServInfoSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLCGrpServInfoSet.get(i).setModifyTime(PubFun.getCurrentTime());
                //System.out.println(mLCHistoryImpartSet.get(i).getPeoples());

                // add by zhangxing 增加保全定期结算接口
                if (mLCGrpServInfoSet.get(i).getServKind().equals("1")
                        && mLCGrpServInfoSet.get(i).getServDetail().equals("3"))
                {
                    GrpBalanceOnTime tGrpBalanceOnTime = new GrpBalanceOnTime();
                    System.out.println("mLCGrpContSchema.getGrpContNo():"
                            + mLCGrpContSchema.getGrpContNo());
                    System.out.println(" mLCGrpContSchema.getCValiDate():"
                            + mLCGrpContSchema.getCValiDate());
                    System.out
                            .println("mLCGrpServInfoSet.get(i).getServChoose():"
                                    + mLCGrpServInfoSet.get(i).getServChoose());
                    System.out.println("mLCGrpContSchema.getManageCom():"
                            + mLCGrpContSchema.getManageCom());
                    System.out.println("mLCGrpContSchema.getOperator():"
                            + mLCGrpContSchema.getOperator());
                    map.add(tGrpBalanceOnTime.addLCGrpBalPlan(mLCGrpContSchema
                            .getGrpContNo(), mLCGrpContSchema.getCValiDate(),
                            mLCGrpServInfoSet.get(i).getServChoose(),
                            mLCGrpContSchema.getManageCom(),
                            mGlobalInput.Operator));
                    System.out.println("map%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

                }

            }

            if (mLCGrpServInfoSet != null)
            {
                map.put(mLCGrpServInfoSet, "INSERT");
            }
        }

        //2006-03-15 闫少杰 团单约定缴费信息处理
        if (this.mLCGrpSpecFeeSet != null && this.mLCGrpSpecFeeSet.size() > 0)
        {
            for (int i = 1; i <= mLCGrpSpecFeeSet.size(); i++)
            {
                mLCGrpSpecFeeSet.get(i).setManageCom(mGlobalInput.ManageCom);
                mLCGrpSpecFeeSet.get(i).setOperator(mGlobalInput.Operator);
                mLCGrpSpecFeeSet.get(i).setMakeDate(theCurrentDate);
                mLCGrpSpecFeeSet.get(i).setModifyDate(theCurrentDate);
                mLCGrpSpecFeeSet.get(i).setMakeTime(theCurrentTime);
                mLCGrpSpecFeeSet.get(i).setModifyTime(theCurrentTime);
            }
            if (mLCGrpSpecFeeSet != null)
            {
                map.put(mLCGrpSpecFeeSet, "INSERT");
            }
        }
        
        //by gzh 20130408 对综合开拓数据处理
        if (!"".equals(StrTool.cTrim(mLCExtendSchema.getAssistSalechnl())))
        {
            MMap tTmpMap = null;
            tTmpMap = delExtendInfo(mLCExtendSchema);
            if (tTmpMap == null)
            {
                return false;
            }
            map.add(tTmpMap);
        }else{
        	String tSql = "delete from LCExtend where prtno = '"+mLCExtendSchema.getPrtNo()+"'";
        	map.put(tSql, SysConst.DELETE);
        }
        return true;
    }

    /**
     * 处理更新逻辑，更新处理过的数据
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        //校验代理人编码并置上代理人组别
        if (mLCGrpContSchema.getAgentCode() == null
                || mLCGrpContSchema.getAgentCode().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "dealData";
            tError.errorMessage = "请您确认有：代理人编码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mLCGrpContSchema.getAgentCode());
            if (tLAAgentDB.getInfo() == false)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "请您确认：代理人编码没有输入错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLAAgentDB.getManageCom() == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "代理人编码对应数据库中的管理机构为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!tLAAgentDB.getManageCom().equals(
                    mLCGrpContSchema.getManageCom()))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "您录入的管理机构和数据库中代理人编码对应的管理机构不符合！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLCGrpContSchema.setAgentGroup(tLAAgentDB.getAgentGroup());

        }

        LCGrpContDB oldLCGrpContDB = new LCGrpContDB();
        oldLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if (!oldLCGrpContDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(oldLCGrpContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GroupContBL";
            tError.functionName = "dealData";
            tError.errorMessage = "LCGrpCont表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCPolDB mLCPolDB = new LCPolDB();
        mLCPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        int lccontNum = mLCPolDB.getCount();
        if (mLCPolDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "updateData";
            tError.errorMessage = "查询个人合同失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (lccontNum > 0
                & !StrTool.compareString(mLCGrpContSchema.getCValiDate(),
                        oldLCGrpContDB.getCValiDate()))
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "updateData";
            tError.errorMessage = "该总单下已经有个人险种，不能修改保险责任生效日期!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (lccontNum > 0
                & !StrTool.compareString(mLCGrpContSchema.getCInValiDate(),
                        oldLCGrpContDB.getCInValiDate()))
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "updateData";
            tError.errorMessage = "该总单下已经有个人险种，不能修改保险责任终止日期!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCGrpAppntDB oldLCGrpAppntDB = new LCGrpAppntDB();
        oldLCGrpAppntDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        //oldLCGrpAppntDB.setCustomerNo(mLCGrpContSchema.getAppntNo());
        oldLCGrpAppntDB.setSchema(oldLCGrpAppntDB.query().get(1));
        if (oldLCGrpAppntDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(oldLCGrpContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GroupContBL";
            tError.functionName = "dealData";
            tError.errorMessage = "LCGrpAppnt表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!StrTool.compareString(mLCGrpContSchema.getCValiDate(),
                oldLCGrpContDB.getCValiDate()))
        {
            StringBuffer sql = new StringBuffer();
            sql.append("update lcgrppol set CValiDate ='");
            sql.append(mLCGrpContSchema.getCValiDate());
            sql.append("' where grpcontno='");
            sql.append(mLCGrpContSchema.getGrpContNo());
            sql.append("'");
            map.put(sql.toString(), "UPDATE");
            StringBuffer sql1 = new StringBuffer();
            sql1.append("update lcpol set CValiDate ='");
            sql1.append(mLCGrpContSchema.getCValiDate());
            sql1.append("' where grpcontno='");
            sql1.append(mLCGrpContSchema.getGrpContNo());
            sql1.append("'");
            map.put(sql1.toString(), "UPDATE");
            StringBuffer sql2 = new StringBuffer();
            sql2.append("update lccont set CValiDate ='");
            sql2.append(mLCGrpContSchema.getCValiDate());
            sql2.append("' where grpcontno='");
            sql2.append(mLCGrpContSchema.getGrpContNo());
            sql2.append("'");
            map.put(sql2.toString(), "UPDATE");
        }

        if (!StrTool.compareString(mLCGrpContSchema.getPayMode(),
                oldLCGrpContDB.getPayMode()))
        {
            StringBuffer sql = new StringBuffer();
            sql.append("update lcgrppol set PayMode ='");
            sql.append(mLCGrpContSchema.getPayMode());
            sql.append("' where grpcontno='");
            sql.append(mLCGrpContSchema.getGrpContNo());
            sql.append("'");
            map.put(sql.toString(), "UPDATE");
            StringBuffer sql1 = new StringBuffer();
            sql1.append("update lcpol set PayMode ='");
            sql1.append(mLCGrpContSchema.getPayMode());
            sql1.append("' where grpcontno='");
            sql1.append(mLCGrpContSchema.getGrpContNo());
            sql1.append("'");
            map.put(sql1.toString(), "UPDATE");
            StringBuffer sql2 = new StringBuffer();
            sql2.append("update lccont set PayMode ='");
            sql2.append(mLCGrpContSchema.getPayMode());
            sql2.append("' where grpcontno='");
            sql2.append(mLCGrpContSchema.getGrpContNo());
            sql2.append("'");
            map.put(sql2.toString(), "UPDATE");
        }
        if (mLCGrpContSchema.getPayIntv() != oldLCGrpContDB.getPayIntv())
        {
            StringBuffer sql = new StringBuffer();
            sql.append("update lcgrppol set PayIntv =");
            sql.append(mLCGrpContSchema.getPayIntv());
            sql.append(" where grpcontno='");
            sql.append(mLCGrpContSchema.getGrpContNo());
            sql.append("'");
            map.put(sql.toString(), "UPDATE");
            StringBuffer sql1 = new StringBuffer();
            sql1.append("update lcpol set PayIntv =");
            sql1.append(mLCGrpContSchema.getPayIntv());
            sql1.append(" where grpcontno='");
            sql1.append(mLCGrpContSchema.getGrpContNo());
            sql1.append("'");
            map.put(sql1.toString(), "UPDATE");
            StringBuffer sql2 = new StringBuffer();
            sql2.append("update lccont set PayIntv =");
            sql2.append(mLCGrpContSchema.getPayIntv());
            sql2.append(" where grpcontno='");
            sql2.append(mLCGrpContSchema.getGrpContNo());
            sql2.append("'");
            map.put(sql2.toString(), "UPDATE");
            StringBuffer sql3 = new StringBuffer();
            sql3.append("update lcprem set PayIntv =");
            sql3.append(mLCGrpContSchema.getPayIntv());
            sql3.append(" where grpcontno='");
            sql3.append(mLCGrpContSchema.getGrpContNo());
            sql3.append("'");
            map.put(sql3.toString(), "UPDATE");
            StringBuffer sql4 = new StringBuffer();
            sql4.append("update lcduty set PayIntv =");
            sql4.append(mLCGrpContSchema.getPayIntv());
            sql4
                    .append(" where contno in (select contno from lccont where grpcontno='");
            sql4.append(mLCGrpContSchema.getGrpContNo());
            sql4.append("' and conttype='2' )");
            map.put(sql4.toString(), "UPDATE");

        }

        LCGrpAddressDB oldLCGrpAddressDB = new LCGrpAddressDB();
        oldLCGrpAddressDB.setCustomerNo(oldLCGrpAppntDB.getCustomerNo());
        oldLCGrpAddressDB.setAddressNo(oldLCGrpAppntDB.getAddressNo());
        if (!oldLCGrpAddressDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(oldLCGrpContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GroupContBL";
            tError.functionName = "dealData";
            tError.errorMessage = "LCGrpAddress表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        boolean customerFlag = false;
        boolean addressFlag = false;
        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        //mLCGrpAppntSchema.getCustomerNo().trim().equals("")
        //社保渠道,非社保渠道,投保单位名称校验
        if(mLCGrpAppntSchema.getCustomerNo()==null || "".equals(mLCGrpAppntSchema.getCustomerNo().trim()) ){
        	System.out.println("客户名称:"+mLDGrpSchema.getGrpName()+"  长度:"+mLDGrpSchema.getGrpName().length());   
        	String tName="";
        	   String mName="";
         	 String pName="";
         	String tSql2="";
         	String tSql3="";
         	String tSql5="";
         	String tCodeName2="";
         	String tCodeName1="";
         	String tCodeName3="";
         	 String tSaleChnl="";
        	 String tSale=mLCGrpContSchema.getSaleChnl();
             SSRS tSSRS = new SSRS();
     		ExeSQL tExeSQL = new ExeSQL();
     		 if(tSale.equals("16")){
         	 //  String tSql=" select  code from ldcode  where codetype ='GrpCustSalechnlChk' and code='"+tSale+"'";
     			 String tSql4="select  customerno  from ldgrp where  grpname='"+mLDGrpSchema.getGrpName()+"'"; 
     			 if(tSql4!=null || tSql4!=""){
          	   String tCustomerNo=tExeSQL.getOneValue(tSql4);
	          	mCustomerNO = tCustomerNo;
	          	mLCGrpContSchema.setAppntNo(mCustomerNO);
	          	mLCGrpAppntSchema.setCustomerNo(mCustomerNO);
	          	mLDGrpSchema.setCustomerNo(mCustomerNO);
	          	mLCGrpAddressSchema.setCustomerNo(mCustomerNO);
	         	}
     		 } else if(tSale!="16" &&  mLDGrpSchema.getGrpName().length()>=4 ){
     			  //后两位
                tName=mLDGrpSchema.getGrpName().substring(mLDGrpSchema.getGrpName().length()-1, mLDGrpSchema.getGrpName().length());     		 
                  //最后一位  
                mName=mLDGrpSchema.getGrpName().substring(mLDGrpSchema.getGrpName().length());                 
                //后三位
          	    pName=mLDGrpSchema.getGrpName().substring(mLDGrpSchema.getGrpName().length()-2, mLDGrpSchema.getGrpName().length()) ; 	 
               tSql2="select  codename from ldcode where codetype='GrpCustChk2' andcodename='"+tName+"'";
           	   tSql3="select codename from ldcode where codetype='GrpCustChk1' and codename='"+mName+"'";
           	   tSql5="select  codename from ldcode where codetype='GrpCustChk2' and codename='"+pName+"' ";
           	     tCodeName2=tExeSQL.getOneValue(tSql2);
                 tCodeName1=tExeSQL.getOneValue(tSql3);
                 tCodeName3=tExeSQL.getOneValue(tSql5);
                  if(tCodeName2!=null ){
                	  String tSql4="select  customerno  from ldgrp where  grpname='"+mLDGrpSchema.getGrpName()+"'"; 
                	  if(tSql4!=null || tSql4!=""){
                     	   String tCustomerNo=tExeSQL.getOneValue(tSql4);
           	          	mCustomerNO = tCustomerNo;
           	          	mLCGrpContSchema.setAppntNo(mCustomerNO);
           	          	mLCGrpAppntSchema.setCustomerNo(mCustomerNO);
           	          	mLDGrpSchema.setCustomerNo(mCustomerNO);
           	          	mLCGrpAddressSchema.setCustomerNo(mCustomerNO);
                       }
                  }
                  else if(tCodeName3!=null ){
                	  String tSql4="select  customerno  from ldgrp where  grpname='"+mLDGrpSchema.getGrpName()+"'"; 
                	  if(tSql4!=null || tSql4!=""){
                     	   String tCustomerNo=tExeSQL.getOneValue(tSql4);
           	          	mCustomerNO = tCustomerNo;
           	          	mLCGrpContSchema.setAppntNo(mCustomerNO);
           	          	mLCGrpAppntSchema.setCustomerNo(mCustomerNO);
           	          	mLDGrpSchema.setCustomerNo(mCustomerNO);
           	          	mLCGrpAddressSchema.setCustomerNo(mCustomerNO);
                       }
                  }
                  else if (tCodeName1!=null )
                   {        String tSql4="select  customerno  from ldgrp where  grpname='"+mLDGrpSchema.getGrpName()+"'"; 
			            	  if(tSql4!=null || tSql4!=""){
			                	   String tCustomerNo=tExeSQL.getOneValue(tSql4);
			      	          	mCustomerNO = tCustomerNo;
			      	          	mLCGrpContSchema.setAppntNo(mCustomerNO);
			      	          	mLCGrpAppntSchema.setCustomerNo(mCustomerNO);
			      	          	mLDGrpSchema.setCustomerNo(mCustomerNO);
			      	          	mLCGrpAddressSchema.setCustomerNo(mCustomerNO);
			                         }
                	   
                    }
                                                                 		 
     		 } 
         	       
     }     
        //如果缺少客户号码，生成一个

        if (mLCGrpAppntSchema.getCustomerNo() == null
                || mLCGrpAppntSchema.getCustomerNo().trim().equals(""))
        {
            customerFlag = true;
            tLimit = "SN";

            String CustomerNO = PubFun1.CreateMaxNo("GRPNO", tLimit);

            if (!CustomerNO.equals(""))
            {
                mLCGrpContSchema.setAppntNo(CustomerNO);
                mLCGrpAppntSchema.setCustomerNo(CustomerNO);
                mLDGrpSchema.setCustomerNo(CustomerNO);
                mLCGrpAddressSchema.setCustomerNo(CustomerNO);
            }
            else
            {
                mErrors.addOneError(new CError("客户号码生成失败"));
                return false;
            }
        }
        if (!StrTool.compareString(mLCGrpContSchema.getAppntNo(),
                oldLCGrpContDB.getAppntNo())
                || !StrTool.compareString(mLCGrpContSchema.getGrpName(),
                        oldLCGrpContDB.getGrpName()))
        {
            StringBuffer sql = new StringBuffer();
            sql.append("update lcgrppol set CustomerNo ='");
            sql.append(mLCGrpContSchema.getAppntNo());
            sql.append("', GrpName='");
            sql.append(mLCGrpContSchema.getGrpName());
            sql.append("' where grpcontno='");
            sql.append(mLCGrpContSchema.getGrpContNo());
            sql.append("'");
            map.put(sql.toString(), "UPDATE");
            StringBuffer sql1 = new StringBuffer();
            sql1.append("update lcpol set AppntNo ='");
            sql1.append(mLCGrpContSchema.getAppntNo());
            sql1.append("', AppntName='");
            sql1.append(mLCGrpContSchema.getGrpName());
            sql1.append("' where grpcontno='");
            sql1.append(mLCGrpContSchema.getGrpContNo());
            sql1.append("'");
            map.put(sql1.toString(), "UPDATE");
            StringBuffer sql2 = new StringBuffer();
            sql2.append("update lccont set AppntNo ='");
            sql2.append(mLCGrpContSchema.getAppntNo());
            sql2.append("', AppntName='");
            sql2.append(mLCGrpContSchema.getGrpName());
            sql2.append("' where grpcontno='");
            sql2.append(mLCGrpContSchema.getGrpContNo());
            sql2.append("'");
            map.put(sql2.toString(), "UPDATE");
        }

        mLCGrpAppntSchema.setMakeDate(oldLCGrpAddressDB.getMakeDate());
        mLCGrpAppntSchema.setMakeTime(oldLCGrpAddressDB.getMakeTime());

        //如果缺少地址号码，生成一个
        if (mLCGrpContSchema.getAddressNo() == null
                || mLCGrpContSchema.getAddressNo().trim().equals(""))
        {
            try
            {
                addressFlag = true;
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(integer(AddressNo)) Is Null Then 0 Else max(integer(AddressNo)) End from  LCGrpAddress where CustomerNo='"
                        + mLCGrpAppntSchema.getCustomerNo() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                String AddressNo = integer.toString();
                System.out.println("得到的地址码是：" + AddressNo);
                if (AddressNo == null || (!AddressNo.equals("")))
                {
                    mLCGrpContSchema.setAddressNo(AddressNo);
                    mLCGrpAppntSchema.setAddressNo(AddressNo);
                    mLCGrpAddressSchema.setAddressNo(AddressNo);
                }
                else
                {
                    mErrors.addOneError(new CError("客户地址号码生成失败"));
                    return false;
                }

            }
            catch (Exception e)
            {
                CError tError = new CError();
                tError.moduleName = "groupContBL";
                tError.functionName = "createAddressNo";
                tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
                this.mErrors.addOneError(tError);
            }
        }
        else
        {
            mLCGrpAddressSchema.setAddressNo(mLCGrpContSchema.getAddressNo()
                    .trim());
        }

        mLCGrpAddressSchema.setMakeDate(oldLCGrpAddressDB.getMakeDate());
        mLCGrpAddressSchema.setMakeTime(oldLCGrpAddressDB.getMakeTime());

        mLCGrpContSchema.setAppFlag(oldLCGrpContDB.getAppFlag());
        mLCGrpContSchema.setStateFlag(oldLCGrpContDB.getStateFlag());
        //录入日期
        mLCGrpContSchema.setInputDate(oldLCGrpContDB.getInputDate());
        mLCGrpContSchema.setInputTime(oldLCGrpContDB.getInputTime());
        //复核标记
        mLCGrpContSchema.setApproveFlag(oldLCGrpContDB.getApproveFlag());
        mLCGrpContSchema.setApproveCode(oldLCGrpContDB.getApproveCode());
        mLCGrpContSchema.setApproveDate(oldLCGrpContDB.getApproveDate());
        mLCGrpContSchema.setApproveTime(oldLCGrpContDB.getApproveTime());
        //签单标记
        mLCGrpContSchema.setUWFlag(oldLCGrpContDB.getUWFlag());
        mLCGrpContSchema.setUWOperator(oldLCGrpContDB.getUWOperator());
        mLCGrpContSchema.setUWDate(oldLCGrpContDB.getApproveDate());
        mLCGrpContSchema.setUWTime(oldLCGrpContDB.getApproveTime());
        mLCGrpContSchema.setPeoples2(oldLCGrpContDB.getPeoples2());
        mLCGrpContSchema.setSpecFlag(oldLCGrpContDB.getSpecFlag());
        mLCGrpContSchema.setPrem(oldLCGrpContDB.getPrem());
        mLCGrpContSchema.setSumPrem(oldLCGrpContDB.getSumPrem());
        mLCGrpContSchema.setAmnt(oldLCGrpContDB.getAmnt());
        mLCGrpContSchema.setMult(oldLCGrpContDB.getMult());
        mLCGrpContSchema.setMakeDate(oldLCGrpContDB.getMakeDate());
        mLCGrpContSchema.setMakeTime(oldLCGrpContDB.getMakeTime());
        mLCGrpContSchema.setPolApplyDate(oldLCGrpContDB.getPolApplyDate());
        if (customerFlag)
        {
            //记录入机日期

            mLDGrpSchema.setMakeDate(theCurrentDate);
            mLDGrpSchema.setMakeTime(theCurrentTime);
            map.put(mLDGrpSchema, "INSERT");
        }

        //记录入机时间
        mLCGrpAddressSchema.setMakeTime(theCurrentTime);
        mLCGrpAddressSchema.setMakeDate(theCurrentDate);
        map.put(mLCGrpAddressSchema, "DELETE&INSERT");

        //map.put(oldLCGrpAddressDB.getSchema(), "DELETE");

        map.put(mLCGrpContSchema, "UPDATE");
        map.put(oldLCGrpAppntDB.getSchema(), "DELETE");
        map.put(mLCGrpAppntSchema, "INSERT");
        /*要更新所有的LCCont,LCInsured,LCPol,LCGeneral,LCInsuredRelated,LCCustomerImpart,LCCustomerImpartParams
         的ManageCom,SaleChnl，AgentCom，AgentType，AgentCode，AgentGroup,AgentCode1
         */
        String wherePart = "GrpContNo='" + mLCGrpContSchema.getGrpContNo()
                + "'";
        if (!StrTool.compareString(mLCGrpContSchema.getCValiDate(),
                oldLCGrpContDB.getCValiDate()))
        {
            String updatecontsql = "update lccont set " + "CValiDate ='"
                    + mLCGrpContSchema.getCValiDate() + "'," + "ModifyDate ='"
                    + theCurrentDate + "'," + "ModifyTime ='" + theCurrentTime
                    + "' " + "where " + wherePart;
            map.put(updatecontsql, "UPDATE");
        }
        if (!mLCGrpContSchema.getPrtNo().equals(oldLCGrpContDB.getPrtNo()))
        {
            String updateSql = "";
            updateSql = updateSql + "PrtNo='" + mLCGrpContSchema.getPrtNo()
                    + "'," + "ModifyDate ='" + theCurrentDate + "',"
                    + "ModifyTime ='" + theCurrentTime + "' " + "where "
                    + wherePart;

            String updateLCCustomerImpartSql = "update LCCustomerImpart set "
                    + updateSql;
            String updateLCCustomerImpartParamsSql = "update LCCustomerImpartParams set "
                    + updateSql;

            map.put(updateLCCustomerImpartSql, "UPDATE");
            map.put(updateLCCustomerImpartParamsSql, "UPDATE");
        }
        if (!mLCGrpContSchema.getManageCom().equals(
                oldLCGrpContDB.getManageCom())
                || !mLCGrpContSchema.getPrtNo().equals(
                        oldLCGrpContDB.getPrtNo()))
        {
            String updateSql = "";
            updateSql = updateSql + "ManageCom='"
                    + mLCGrpContSchema.getManageCom() + "',";
            updateSql = updateSql + "PrtNo='" + mLCGrpContSchema.getPrtNo()
                    + "',";
            updateSql = updateSql + "ModifyDate ='" + theCurrentDate + "',"
                    + "ModifyTime ='" + theCurrentTime + "' " + "where "
                    + wherePart;

            String updateLCGeneralSql = "update LCGeneral set " + updateSql;
            map.put(updateLCGeneralSql, "UPDATE");
        }

        if (!mLCGrpContSchema.getManageCom().equals(
                oldLCGrpContDB.getManageCom())
                || !mLCGrpContSchema.getPrtNo().equals(
                        oldLCGrpContDB.getPrtNo())
                || !mLCGrpContSchema.getAppntNo().equals(
                        oldLCGrpContDB.getAppntNo())
                || !mLCGrpContSchema.getGrpName().equals(
                        oldLCGrpContDB.getGrpName()))
        {

            String updateLCInsuredSql1 = "update LCInsured set "
                    + "ManageCom='" + mLCGrpContSchema.getManageCom() + "',"
                    + "PrtNo='" + mLCGrpContSchema.getPrtNo() + "',"
                    + "AppntNo='" + mLCGrpContSchema.getAppntNo() + "',"
                    + "ModifyDate ='" + theCurrentDate + "'," + "ModifyTime ='"
                    + theCurrentTime + "' " + "where " + wherePart;

            map.put(updateLCInsuredSql1, "UPDATE");
            
            //同步修改LCPrem和LCGet表中的管理机构   by zhangyang 2011-09-16
            String tUpdateLCPremSql = "update LCPrem set "
            	+ "ManageCom = '" + mLCGrpContSchema.getManageCom() 
            	+ "' where PolNo in (select PolNo from LCPol where GrpContNo = '" 
            	+ mLCGrpContSchema.getGrpContNo() + "') ";
            String tUpdateLCGetSql = "update LCGet set "
            	+ "ManageCom = '" + mLCGrpContSchema.getManageCom() 
            	+ "' where PolNo in (select PolNo from LCPol where GrpContNo = '" 
            	+ mLCGrpContSchema.getGrpContNo() + "') ";
            map.put(tUpdateLCPremSql, "UPDATE");
            map.put(tUpdateLCGetSql, "UPDATE");
            //--------------------------------------------------------
        }

        if ((!StrTool.compareString(mLCGrpContSchema.getManageCom(),
                oldLCGrpContDB.getManageCom()))
                || (!StrTool.compareString(mLCGrpContSchema.getPrtNo(),
                        oldLCGrpContDB.getPrtNo()))
                || (!StrTool.compareString(mLCGrpContSchema.getPolApplyDate(),
                        oldLCGrpContDB.getPolApplyDate()))
                || (!StrTool.compareString(mLCGrpContSchema.getAppntNo(),
                        oldLCGrpContDB.getAppntNo()))
                || (!StrTool.compareString(mLCGrpContSchema.getGrpName(),
                        oldLCGrpContDB.getGrpName()))
                || (!StrTool.compareString(mLCGrpContSchema.getAgentCom(),
                        oldLCGrpContDB.getAgentCom()))
                || (!StrTool.compareString(mLCGrpContSchema.getAgentCode(),
                        oldLCGrpContDB.getAgentCode()))
                || (!StrTool.compareString(mLCGrpContSchema.getAgentGroup(),
                        oldLCGrpContDB.getAgentGroup()))
                || (!StrTool.compareString(mLCGrpContSchema.getAgentCode1(),
                        oldLCGrpContDB.getAgentCode1()))
                || (!StrTool.compareString(mLCGrpContSchema.getAgentType(),
                        oldLCGrpContDB.getAgentType()))
                || (!StrTool.compareString(mLCGrpContSchema.getSaleChnl(),
                        oldLCGrpContDB.getSaleChnl()))
                || (!StrTool.compareString(mLCGrpContSchema.getPayMode(),
                        oldLCGrpContDB.getPayMode())))
        {
            String updateLCContSql = "update LCCont set ";
            if (mLCGrpContSchema.getManageCom() != null)
            {
                updateLCContSql = updateLCContSql + "ManageCom='"
                        + mLCGrpContSchema.getManageCom() + "',";
            }
            if (mLCGrpContSchema.getManageCom() != null)
            {
                updateLCContSql = updateLCContSql + "ExecuteCom='"
                        + mLCGrpContSchema.getManageCom() + "',";
            }
            if (mLCGrpContSchema.getPrtNo() != null)
            {
                updateLCContSql = updateLCContSql + "PrtNo='"
                        + mLCGrpContSchema.getPrtNo() + "',";
            }
            if (mLCGrpContSchema.getPolApplyDate() != null)
            {
                updateLCContSql = updateLCContSql + "PolApplyDate='"
                        + mLCGrpContSchema.getPolApplyDate() + "',";
            }
            if (mLCGrpContSchema.getAppntNo() != null)
            {
                updateLCContSql = updateLCContSql + "AppntNo='"
                        + mLCGrpContSchema.getAppntNo() + "',";
            }
            if (mLCGrpContSchema.getGrpName() != null)
            {
                updateLCContSql = updateLCContSql + "AppntName='"
                        + mLCGrpContSchema.getGrpName() + "',";
            }
            if (mLCGrpContSchema.getAgentCom() != null)
            {
                updateLCContSql = updateLCContSql + "AgentCom='"
                        + mLCGrpContSchema.getAgentCom() + "',";
            }
            if (mLCGrpContSchema.getAgentCode() != null)
            {
                updateLCContSql = updateLCContSql + "AgentCode='"
                        + mLCGrpContSchema.getAgentCode() + "',";
            }
            if (mLCGrpContSchema.getAgentGroup() != null)
            {
                updateLCContSql = updateLCContSql + "AgentGroup='"
                        + mLCGrpContSchema.getAgentGroup() + "',";
            }

            if (mLCGrpContSchema.getSaleChnl() != null)
            {
                updateLCContSql = updateLCContSql + "SaleChnl='"
                        + mLCGrpContSchema.getSaleChnl() + "',";
            }
            if (mLCGrpContSchema.getPayMode() != null)
            {
                updateLCContSql = updateLCContSql + "PayMode='"
                        + mLCGrpContSchema.getPayMode() + "',";
            }
            System.out.println("payMode+:" + mLCGrpContSchema.getPayMode());
            //          + "PayLocation='" + mLCGrpContSchema.getPayLocation() + "',"
            updateLCContSql = updateLCContSql + "ModifyDate ='"
                    + theCurrentDate + "',";
            updateLCContSql = updateLCContSql + "ModifyTime ='"
                    + theCurrentTime + "' ";
            updateLCContSql = updateLCContSql + "where " + wherePart;
            String updateLCPolSql = "update LCPol set ";
            if (mLCGrpContSchema.getManageCom() != null)
            {
                updateLCPolSql = updateLCPolSql + "ManageCom='"
                        + mLCGrpContSchema.getManageCom() + "',";
            }
            if (mLCGrpContSchema.getPrtNo() != null)
            {
                updateLCPolSql = updateLCPolSql + "PrtNo='"
                        + mLCGrpContSchema.getPrtNo() + "',";
            }
            if (mLCGrpContSchema.getPolApplyDate() != null)
            {
                updateLCPolSql = updateLCPolSql + "PolApplyDate='"
                        + mLCGrpContSchema.getPolApplyDate() + "',";
            }
            if (mLCGrpContSchema.getAgentCom() != null)
            {
                updateLCPolSql = updateLCPolSql + "AgentCom='"
                        + mLCGrpContSchema.getAgentCom() + "',";
            }
            if (mLCGrpContSchema.getAgentCode() != null)
            {
                updateLCPolSql = updateLCPolSql + "AgentCode='"
                        + mLCGrpContSchema.getAgentCode() + "',";
            }
            if (mLCGrpContSchema.getAgentGroup() != null)
            {
                updateLCPolSql = updateLCPolSql + "AgentGroup='"
                        + mLCGrpContSchema.getAgentGroup() + "',";
            }
            if (mLCGrpContSchema.getSaleChnl() != null)
            {
                updateLCPolSql = updateLCPolSql + "SaleChnl='"
                        + mLCGrpContSchema.getSaleChnl() + "',";
            }
            if (mLCGrpContSchema.getPayMode() != null)
            {
                updateLCPolSql = updateLCPolSql + "PayMode='"
                        + mLCGrpContSchema.getPayMode() + "',";
            }
            if (mLCGrpContSchema.getAppntNo() != null)
            {
                updateLCPolSql = updateLCPolSql + "appntNo='"
                        + mLCGrpContSchema.getAppntNo() + "',";
            }
            if (mLCGrpContSchema.getGrpName() != null)
            {
                updateLCPolSql = updateLCPolSql + "appntName='"
                        + mLCGrpContSchema.getGrpName() + "',";
            }

            updateLCPolSql = updateLCPolSql + "ModifyDate ='" + theCurrentDate
                    + "'," + "ModifyTime ='" + theCurrentTime + "' " + "where "
                    + wherePart;
            String updateLCGrpPolSql = "update lcgrppol set ";
            if (mLCGrpContSchema.getManageCom() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "ManageCom='"
                        + mLCGrpContSchema.getManageCom() + "',";
            }
            if (mLCGrpContSchema.getPrtNo() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "PrtNo='"
                        + mLCGrpContSchema.getPrtNo() + "',";
            }
            if (mLCGrpContSchema.getAgentCom() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "AgentCom='"
                        + mLCGrpContSchema.getAgentCom() + "',";
            }
            if (mLCGrpContSchema.getAgentCode() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "AgentCode='"
                        + mLCGrpContSchema.getAgentCode() + "',";
            }
            if (mLCGrpContSchema.getAgentGroup() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "AgentGroup='"
                        + mLCGrpContSchema.getAgentGroup() + "',";
            }
            if (mLCGrpContSchema.getSaleChnl() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "SaleChnl='"
                        + mLCGrpContSchema.getSaleChnl() + "',";
            }
            if (mLCGrpContSchema.getPayMode() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "PayMode='"
                        + mLCGrpContSchema.getPayMode() + "',";
            }
            if (mLCGrpContSchema.getAppntNo() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "customerNo='"
                        + mLCGrpContSchema.getAppntNo() + "',";
            }
            if (mLCGrpContSchema.getGrpName() != null)
            {
                updateLCGrpPolSql = updateLCGrpPolSql + "grpName='"
                        + mLCGrpContSchema.getGrpName() + "',";
            }

            updateLCGrpPolSql = updateLCGrpPolSql + "ModifyDate ='"
                    + theCurrentDate + "'," + "ModifyTime ='" + theCurrentTime
                    + "' " + "where " + wherePart;
            map.put(updateLCPolSql, "UPDATE");
            map.put(updateLCGrpPolSql, "UPDATE");
            map.put(updateLCContSql, "UPDATE");
        }
        //团体告知修改
        if (mLCCustomerImpartSet.size() == 0)
        {
            String strSql = "delete from LCCustomerImpart where GrpContNo='"
                    + mLCGrpContSchema.getGrpContNo() + "' and CustomerNo='"
                    + mLCGrpContSchema.getAppntNo() + "'";
            System.out.println("-------------" + strSql);
            map.put(strSql, "DELETE");
        }
        if (mLCCustomerImpartSet != null && mLCCustomerImpartSet.size() > 0)
        {
            for (int i = 1; i <= mLCCustomerImpartSet.size(); i++)
            {
                if (mCustomerNO != "")
                {
                    mLCCustomerImpartSet.get(i).setCustomerNo(mCustomerNO);
                }
                mLCCustomerImpartSet.get(i).setContNo(SysConst.ZERONO);
                mLCCustomerImpartSet.get(i).setGrpContNo(
                        mLCGrpContSchema.getGrpContNo());
                mLCCustomerImpartSet.get(i).setPrtNo(
                        mLCGrpContSchema.getPrtNo());
                mLCCustomerImpartSet.get(i).setProposalContNo(SysConst.ZERONO);
                mLCCustomerImpartSet.get(i).setOperator(mGlobalInput.Operator);
                mLCCustomerImpartSet.get(i).setCustomerNo(
                        mLCGrpContSchema.getAppntNo());
                mLCCustomerImpartSet.get(i)
                        .setMakeDate(PubFun.getCurrentDate());
                mLCCustomerImpartSet.get(i)
                        .setMakeTime(PubFun.getCurrentTime());
                mLCCustomerImpartSet.get(i).setModifyDate(
                        PubFun.getCurrentDate());
                mLCCustomerImpartSet.get(i).setModifyTime(
                        PubFun.getCurrentTime());
            }
            //处理告知参数表
            CustomerImpartBL mCustomerImpartBL = new CustomerImpartBL();
            VData tempVData = new VData();
            tempVData.add(mLCCustomerImpartSet);
            tempVData.add(mGlobalInput);
            mCustomerImpartBL.submitData(tempVData, "IMPART||DEAL");
            if (mCustomerImpartBL.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "GroupContBL";
                tError.functionName = "insert";
                tError.errorMessage = mCustomerImpartBL.mErrors.getFirstError()
                        .toString();
                this.mErrors.addOneError(tError);
                return false;
            }
            tempVData.clear();
            tempVData = mCustomerImpartBL.getResult();
            if (null != (LCCustomerImpartSet) tempVData.getObjectByObjectName(
                    "LCCustomerImpartSet", 0))
            {
                mLCCustomerImpartSet = (LCCustomerImpartSet) tempVData
                        .getObjectByObjectName("LCCustomerImpartSet", 0);
                System.out.println("告知条数" + mLCCustomerImpartSet.size());
            }
            else
            {
                System.out.println("告知条数为空");
            }
            if (null != (LCCustomerImpartParamsSet) tempVData
                    .getObjectByObjectName("LCCustomerImpartParamsSet", 0))
            {
                mLCCustomerImpartParamsSet = (LCCustomerImpartParamsSet) tempVData
                        .getObjectByObjectName("LCCustomerImpartParamsSet", 0);
            }

            if (mLCCustomerImpartParamsSet != null)
            {
                map.put("delete from LCCustomerImpartParams where GrpContNo='"
                        + mLCGrpContSchema.getGrpContNo()
                        + "' and CustomerNo='" + mLCGrpContSchema.getAppntNo()
                        + "'", "DELETE");
                map.put(mLCCustomerImpartParamsSet, "DELETE&INSERT");
            }
            //修改团体告知
            if (mLCCustomerImpartSet != null)
            {
                String strSql = "delete from LCCustomerImpart where GrpContNo='"
                        + mLCGrpContSchema.getGrpContNo()
                        + "' and CustomerNo='"
                        + mLCGrpContSchema.getAppntNo()
                        + "'";
                System.out.println("mLCGrpContSchema.getAppntNo()" + strSql);
                map.put(strSql, "DELETE");
                map.put(mLCCustomerImpartSet, "DELETE&INSERT");
            }
        }

        //受益所有人
        if (mLCGrpContBnfSet.size() == 0)
        {
            String strSql = "delete from LCGrpContBnf where PrtNo='"
                    + mLCGrpContSchema.getPrtNo()+ "'";
            System.out.println("-------------" + strSql);
            map.put(strSql, "DELETE");
        }
        if (mLCGrpContBnfSet != null && mLCGrpContBnfSet.size() > 0)
        {
        	String strSql = "delete from LCGrpContBnf where PrtNo='"
                    + mLCGrpContSchema.getPrtNo()+ "'";
            System.out.println("-------------" + strSql);
            map.put(strSql, "DELETE");
            for (int i = 1; i <= mLCGrpContBnfSet.size(); i++)
            {	
            	mLCGrpContBnfSet.get(i).setPrtNo(
                        mLCGrpContSchema.getPrtNo());
            	mLCGrpContBnfSet.get(i).setOperator(mGlobalInput.Operator);
            	mLCGrpContBnfSet.get(i)
                        .setMakeDate(PubFun.getCurrentDate());
            	mLCGrpContBnfSet.get(i)
                        .setMakeTime(PubFun.getCurrentTime());
            	mLCGrpContBnfSet.get(i).setModifyDate(
                        PubFun.getCurrentDate());
            	mLCGrpContBnfSet.get(i).setModifyTime(
                        PubFun.getCurrentTime());
            }
            map.put(mLCGrpContBnfSet, "DELETE&INSERT");
        }
        
        if (mLCHistoryImpartSet != null && mLCHistoryImpartSet.size() > 0)
        {
            for (int i = 1; i <= mLCHistoryImpartSet.size(); i++)
            {
                FDate tFDate = new FDate();
                /** 判断开始和起始时间是否录入 */
//              //miaoxz于2009-9-21注释掉了下面的程序校验，因团险投保书变更所致
                /**if (StrTool
                        .cTrim(mLCHistoryImpartSet.get(i).getInsuStartYear())
                        .equals("")
                        || StrTool.cTrim(
                                mLCHistoryImpartSet.get(i).getInsuEndYear())
                                .equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "GrpContBL";
                    tError.functionName = "dealData";
                    //tError.errorMessage = "请录入保障情况告知的保障起始时间和保障终止时间";
                    tError.errorMessage = "请录入保障情况告知的起始时间和终止时间，且格式应该为YYYY-MM-DD";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                /** 开始和起始时间的先后顺序 */
                /**if (!tFDate.getDate(
                        mLCHistoryImpartSet.get(i).getInsuStartYear()).before(
                        tFDate.getDate(mLCHistoryImpartSet.get(i)
                                .getInsuEndYear())))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GrpContBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "录入保障情况告知的保障起始时间大于保障终止时间";
                    this.mErrors.addOneError(tError);
                    return false;
                }*/
                mLCHistoryImpartSet.get(i).setGrpContNo(
                        mLCGrpContSchema.getGrpContNo());
                if ("".equals(mLCHistoryImpartSet.get(i).getSerialNo())
                        || mLCHistoryImpartSet.get(i).getSerialNo() == null)
                {
                    String tHisImpSerlNo = PubFun1.CreateMaxNo("HisImpSerlNo",
                            20);
                    if (tHisImpSerlNo.trim().equals(""))
                    {
                        mErrors.addOneError(new CError("生成HisImpSerlNo失败!"));
                        return false;
                    }
                    mLCHistoryImpartSet.get(i).setSerialNo(tHisImpSerlNo);
                }
                mLCHistoryImpartSet.get(i)
                        .setPrtNo(mLCGrpContSchema.getPrtNo());
                mLCHistoryImpartSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLCHistoryImpartSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLCHistoryImpartSet.get(i).setModifyDate(
                        PubFun.getCurrentDate());
                mLCHistoryImpartSet.get(i).setModifyTime(
                        PubFun.getCurrentTime());
                //System.out.println(mLCHistoryImpartSet.get(i).getPeoples());
            }

            if (mLCHistoryImpartSet != null)
            {
                String strSql = "delete from LCHistoryImpart where GrpContNo='"
                        + mLCGrpContSchema.getGrpContNo() + "'";
                map.put(strSql, "DELETE");
                map.put(mLCHistoryImpartSet, "DELETE&INSERT");
            }
        }
        else
        {
            String tGrpContNo = mLCGrpContSchema.getGrpContNo();
            if (mLCHistoryImpartSet.size() == 0)
            {
                map.put("delete from LCHistoryImpart where GrpContNo='"
                        + tGrpContNo + "'", "DELETE");
            }
        }
//      处理告知明细信息
        if (mLCCustomerImpartDetailSet != null &&
            mLCCustomerImpartDetailSet.size() > 0) {
            //设置所有告知明细信息得客户号码
            for (int i = 1; i <= mLCCustomerImpartDetailSet.size(); i++) {
                if (mLCCustomerImpartDetailSet.get(i).getImpartVer().trim().
                    equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getImpartCode().trim().
                    equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getImpartDetailContent().
                    trim().equals("") ||
                    mLCCustomerImpartDetailSet.get(i).getDiseaseContent().trim().
                    equals("")
                        ) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ContInsuredBL";
                    tError.functionName = "dealData";
                    tError.errorMessage =
                            "基本告知或健康告知中详细告知的告知版本，告知编码，告知项目，说明内容录入不能为空，请检查！";
                    this.mErrors.addOneError(tError);
                    break;
                }
                //增加对同一告知不同告知内容的存储
                for (int count = 1; count <= mLCCustomerImpartDetailSet.size();
                                 count++) {
                    if (StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartVer()).equals(
                                              mLCCustomerImpartDetailSet.get(
                            count).getImpartVer())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartCode()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getImpartCode())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getDiseaseContent()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getDiseaseContent())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getStartDate()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getStartDate())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getEndDate()).
                        equals(mLCCustomerImpartDetailSet.get(count).getEndDate())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getProver()).
                        equals(mLCCustomerImpartDetailSet.get(count).getProver())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getCurrCondition()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getCurrCondition())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getIsProved()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getIsProved())
                        &
                        StrTool.cTrim(mLCCustomerImpartDetailSet.get(i).
                                      getImpartDetailContent()).
                        equals(mLCCustomerImpartDetailSet.get(count).
                               getImpartDetailContent()
                        ) & count != i
                            ) {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ContInsuredBL";
                        tError.functionName = "dealData";
                        tError.errorMessage =
                                "基本告知或健康告知中详细告知的告知版本，告知编码，告知项目，说明内容录入重复，请检查！";
                        this.mErrors.addOneError(tError);
                        break;
                    }
                }
                //tNo = PubFun1.CreateMaxNo("SubSerialNo", 7);
//                System.out.println("得到的告知码是：" + tNo);
                mLCCustomerImpartDetailSet.get(i).setSubSerialNo(String.valueOf(
                        i));
                mLCCustomerImpartDetailSet.get(i).setGrpContNo(mLCGrpContSchema.
                        getGrpContNo());
                mLCCustomerImpartDetailSet.get(i).setContNo("00000000000000000000");
                mLCCustomerImpartDetailSet.get(i).setPrtNo(mLCGrpContSchema.
                        getPrtNo());
                mLCCustomerImpartDetailSet.get(i).setProposalContNo(
                		"00000000000000000000");
                mLCCustomerImpartDetailSet.get(i).setCustomerNo(
                        mLCGrpAppntSchema.
                        getCustomerNo());
                mLCCustomerImpartDetailSet.get(i).setOperator(mGlobalInput.
                        Operator);
                mLCCustomerImpartDetailSet.get(i).setMakeDate(theCurrentDate);
                mLCCustomerImpartDetailSet.get(i).setMakeTime(theCurrentTime);
                mLCCustomerImpartDetailSet.get(i).setModifyDate(theCurrentDate);
                mLCCustomerImpartDetailSet.get(i).setModifyTime(theCurrentTime);
            }
            if (mLCCustomerImpartDetailSet != null)
            {
                String strSql = "delete from LCCustomerImpartDetail where GrpContNo='"
                        + mLCGrpContSchema.getGrpContNo() + "'";
                map.put(strSql, "DELETE");
                map.put(mLCCustomerImpartDetailSet, "DELETE&INSERT");
            }
        }

        if (mLCDiseaseImpartSet != null && mLCDiseaseImpartSet.size() > 0)
        {
            for (int i = 1; i <= mLCDiseaseImpartSet.size(); i++)
            {
                mLCDiseaseImpartSet.get(i).setGrpContNo(
                        mLCGrpContSchema.getGrpContNo());
                if ("".equals(mLCDiseaseImpartSet.get(i).getSerialNo())
                        || mLCDiseaseImpartSet.get(i).getSerialNo() == null)
                {
                    String tDisImpSerlNo = PubFun1.CreateMaxNo("DisImpSerlNo",
                            20);
                    if (tDisImpSerlNo.trim().equals(""))
                    {
                        mErrors.addOneError(new CError("生成DisImpSerlNo失败!"));
                        return false;
                    }
                    mLCDiseaseImpartSet.get(i).setSerialNo(tDisImpSerlNo);
                }
                mLCDiseaseImpartSet.get(i)
                        .setPrtNo(mLCGrpContSchema.getPrtNo());
                mLCDiseaseImpartSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLCDiseaseImpartSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLCDiseaseImpartSet.get(i).setModifyDate(
                        PubFun.getCurrentDate());
                mLCDiseaseImpartSet.get(i).setModifyTime(
                        PubFun.getCurrentTime());
                //System.out.println(mLCHistoryImpartSet.get(i).getPeoples());
            }

            if (mLCDiseaseImpartSet != null)
            {
                String strSql = "delete from LCDiseaseImpart where GrpContNo='"
                        + mLCGrpContSchema.getGrpContNo() + "'";
                map.put(strSql, "DELETE");
                map.put(mLCDiseaseImpartSet, "DELETE&INSERT");
            }
        }
        else
        {
            String tGrpContNo = mLCGrpContSchema.getGrpContNo();
            if (mLCDiseaseImpartSet.size() == 0)
            {
                map.put("delete from LCDiseaseImpart where GrpContNo='"
                        + tGrpContNo + "'", "DELETE");
            }
        }

        //客户服务信息
        if (mLCGrpServInfoSet != null && mLCGrpServInfoSet.size() > 0)
        {
            System.out.println("--Star servInfo--");
            System.out.println("zhangxing张星ljjljjjjjjjj");
            for (int i = 1; i <= mLCGrpServInfoSet.size(); i++)
            {
                mLCGrpServInfoSet.get(i).setGrpContNo(
                        mLCGrpContSchema.getGrpContNo());
                mLCGrpServInfoSet.get(i).setProposalGrpContNo(
                        mLCGrpContSchema.getProposalGrpContNo());
                mLCGrpServInfoSet.get(i).setPrtNo(mLCGrpContSchema.getPrtNo());
                mLCGrpServInfoSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLCGrpServInfoSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLCGrpServInfoSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLCGrpServInfoSet.get(i).setModifyTime(PubFun.getCurrentTime());
                System.out.println("进入里面************************");
                // add by zhangxing 增加保全定期结算接口
                if (mLCGrpServInfoSet.get(i).getServKind().equals("1")
                        && mLCGrpServInfoSet.get(i).getServDetail().equals("3"))
                {
                    GrpBalanceOnTime tGrpBalanceOnTime = new GrpBalanceOnTime();
                    System.out.println("mLCGrpContSchema.getGrpContNo():"
                            + mLCGrpContSchema.getGrpContNo());
                    System.out.println(" mLCGrpContSchema.getCValiDate():"
                            + mLCGrpContSchema.getCValiDate());
                    System.out
                            .println(" mLCGrpServInfoSet.get(i).getServChoose():"
                                    + mLCGrpServInfoSet.get(i).getServChoose());
                    System.out.println("mLCGrpContSchema.getManageCom(),:"
                            + mLCGrpContSchema.getManageCom());
                    System.out.println("mGlobalInput.Operator:"
                            + mGlobalInput.Operator);
                    map.add(tGrpBalanceOnTime.addLCGrpBalPlan(mLCGrpContSchema
                            .getGrpContNo(), mLCGrpContSchema.getCValiDate(),
                            mLCGrpServInfoSet.get(i).getServChoose(),
                            mLCGrpContSchema.getManageCom(),
                            mGlobalInput.Operator));
                    System.out.println("Map.keySet()" + map.keySet().size());
                    System.out.println("map.oSet()" + map.getOrder().size());
                    System.out.println("map********:"
                            + map
                                    .getObjectByObjectName(
                                            "LCGrpBalPlanSchema", 0));
                }

            }

            if (mLCGrpServInfoSet != null)
            {
                map.put(mLCGrpServInfoSet, "DELETE&INSERT");
            }
        }
        if (mLDGrpSchema != null)
        {
            LDGrpDB tLDGrpDB = new LDGrpDB();
            LDGrpSet tLDGrpSet = new LDGrpSet();
            tLDGrpDB.setCustomerNo(mLDGrpSchema.getCustomerNo());
            tLDGrpSet = tLDGrpDB.query();
            if (tLDGrpSet.size() > 0)
            {
                //解决签单时，丢GrpAppntNum的问题
                if (tLDGrpSet.get(1).getGrpAppntNum() != 0)
                    mLDGrpSchema.setGrpAppntNum(tLDGrpSet.get(1)
                            .getGrpAppntNum());
            }
            mLDGrpSchema.setOperator(mGlobalInput.Operator);
            mLDGrpSchema.setMakeDate(PubFun.getCurrentDate());
            mLDGrpSchema.setMakeTime(PubFun.getCurrentTime());
            mLDGrpSchema.setModifyDate(PubFun.getCurrentDate());
            mLDGrpSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLDGrpSchema, "DELETE&INSERT");
        }

        //2006-03-15 闫少杰 团单约定缴费信息处理
        if (this.mLCGrpSpecFeeSet != null && this.mLCGrpSpecFeeSet.size() > 0)
        {
            for (int i = 1; i <= mLCGrpSpecFeeSet.size(); i++)
            {
                mLCGrpSpecFeeSet.get(i).setManageCom(mGlobalInput.ManageCom);
                mLCGrpSpecFeeSet.get(i).setOperator(mGlobalInput.Operator);
                mLCGrpSpecFeeSet.get(i).setMakeDate(theCurrentDate);
                mLCGrpSpecFeeSet.get(i).setModifyDate(theCurrentDate);
                mLCGrpSpecFeeSet.get(i).setMakeTime(theCurrentTime);
                mLCGrpSpecFeeSet.get(i).setModifyTime(theCurrentTime);
            }
            if (mLCGrpSpecFeeSet != null)
            {
                map.put("delete from LCGrpSpecFee where GrpContNo='"
                        + mLCGrpContSchema.getGrpContNo() + "'", "DELETE");
                map.put(mLCGrpSpecFeeSet, "INSERT");
            }
        }
        if (this.mLCGrpSpecFeeSet.size() == 0)
        {
            map.put("delete from LCGrpSpecFee where GrpContNo='"
                    + mLCGrpContSchema.getGrpContNo() + "'", "DELETE");
        }

        // 如果保单为非共保保单，清除相关共保要素。
        if ("0".equals(mLCGrpContSchema.getCoInsuranceFlag()))
        {
            MMap tTmpMap = null;
            tTmpMap = delCoInsuranceInfo(mLCGrpContSchema.getGrpContNo());
            if (tTmpMap == null)
            {
                return false;
            }
            map.add(tTmpMap);
        }
//      by gzh 20130408 对综合开拓数据处理
        if (!"".equals(mLCExtendSchema.getAssistSalechnl()))
        {
            MMap tTmpMap = null;
            tTmpMap = delExtendInfo(mLCExtendSchema);
            if (tTmpMap == null)
            {
                return false;
            }
            map.add(tTmpMap);
        }else{
        	String tSql = "delete from LCExtend where prtno = '"+mLCExtendSchema.getPrtNo()+"'";
        	map.put(tSql, SysConst.DELETE);
        }
        // ----------------------------

        return true;
    }

    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * @return boolean
     */
    private boolean dealData()
    {
        if (mOperate.equals("DELETE||GROUPPOL"))
        {
            return deleteData();
        }
        else if (mOperate.equals("INSERT||GROUPPOL"))
        {
            return insertData();
        }
        else
        { //mOperate.equals("UPDATE||GROUPPOL")
            return updateData();
        }
    }

    /**
     * 根据业务需要进行合法性检查，不满足条件返回false
     * @return boolean
     */
    private boolean checkData()
    {
        CommonCheck ck = new CommonCheck();
        if (mOperate.equals("DELETE||GROUPPOL"))
        {
            if (mLCGrpContSchema.getGrpContNo() == null
                    || mLCGrpContSchema.getGrpContNo().equals(""))
            {
                mErrors.addOneError(new CError("没有找到集体保单号"));
                return false;
            }
            return true;
        }
        if (mOperate.equals("INSERT||GROUPPOL"))
        {

            String strSQL = "select count(*) from LCGrpCont where prtno='"
                    + mLCGrpContSchema.getPrtNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(strSQL);
            String strCount = tSSRS.GetText(1, 1);
            int SumCount = Integer.parseInt(strCount);
            if (SumCount > 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "该投保单印刷号下已经存在合同!";
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        if (!mOperate.equals("DELETE||GROUPPOL"))
        {
            if (mLCGrpContSchema.getManageCom() == null
                    || mLCGrpContSchema.getManageCom().trim().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpContBL";
                tError.functionName = "checkData";
                tError.errorMessage = "请录入管理机构!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!checkLCGrpAddress())
            {
                return false;
            }
            //非空检查
            ck.checknoempty("印刷号码", mLCGrpContSchema.getPrtNo());
            ck.checknoempty("单位名称", mLCGrpContSchema.getGrpName());
            ck.checknoempty("保单生效日", mLCGrpContSchema.getCValiDate());
            //重复值检查
            //匹配值检查
            //如果数据检查中出现了违反完整性的数据
            if (ck.mErrors.getErrorCount() != 0)
            {
                mErrors.copyAllErrors(ck.mErrors);
                return false;
            }
        }
        return true;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {
        //记录当前操作员
        mLCGrpContSchema.setOperator(mGlobalInput.Operator);
        mLCGrpAppntSchema.setOperator(mGlobalInput.Operator);
        mLCGrpAddressSchema.setOperator(mGlobalInput.Operator);
        mLDGrpSchema.setOperator(mGlobalInput.Operator);
        //记录最后一次修改日期
        mLCGrpContSchema.setModifyDate(theCurrentDate);
        mLCGrpAppntSchema.setModifyDate(theCurrentDate);
        mLCGrpAddressSchema.setModifyDate(theCurrentDate);
        mLDGrpSchema.setModifyDate(theCurrentDate);
        //记录最后一次修改时间
        mLCGrpContSchema.setModifyTime(theCurrentTime);
        mLCGrpAppntSchema.setModifyTime(theCurrentTime);
        mLCGrpAddressSchema.setModifyTime(theCurrentTime);
        mLDGrpSchema.setModifyTime(theCurrentTime);

        mInputData.clear();
        mInputData.add(map);
        //数据放入容器，传向UI层
        mResult.clear();
        mResult.add(mLCGrpContSchema);
        mResult.add(mLCGrpAppntSchema);
        mResult.add(mLCGrpAddressSchema);
        mResult.add(mLDGrpSchema);
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 处理删除逻辑，删除处理过的数据
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        LCContDB mLCContDB = new LCContDB();
        mLCContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        int lccontNum = mLCContDB.getCount();
        if (mLCContDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "deleteData";
            tError.errorMessage = "查询个人合同失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (lccontNum > 0)
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "deleteData";
            tError.errorMessage = "该团体合同下还有个人合同没有删除，不能进行删除操作！";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCGrpPolDB mLCGrpPolDB = new LCGrpPolDB();
        mLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        int lcGrpPolNum = mLCGrpPolDB.getCount();
        if (mLCGrpPolDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "deleteData";
            tError.errorMessage = "查询集体险种失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (lcGrpPolNum > 0)
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "deleteData";
            tError.errorMessage = "该团体合同下还有集体险种没有删除，不能进行删除操作！";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCGeneralDB mLCGeneralDB = new LCGeneralDB();
        mLCGeneralDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        int lcGeneralNum = mLCGeneralDB.getCount();
        if (mLCGeneralDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "deleteData";
            tError.errorMessage = "查询总括保单分单失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (lcGeneralNum > 0)
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "deleteData";
            tError.errorMessage = "该团体合同下还有总括分单没有删除，不能进行删除操作！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCGrpAppntDB mLCGrpAppntDB = new LCGrpAppntDB();
        mLCGrpAppntDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        mLCGrpAppntDB.setCustomerNo(mLCGrpContSchema.getAppntNo());
        if (!mLCGrpAppntDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "deleteData";
            tError.errorMessage = "查询集体投保人失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //开始日志记录
        String tGrpContNo = mLCGrpContSchema.getGrpContNo();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if (tLCGrpContDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpContBL";
            tError.functionName = "dealData";
            tError.errorMessage = "请您确认：要删除的集体合同代码输入错误!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCDelPolLog.setOtherNo(tLCGrpContDB.getGrpContNo());
        mLCDelPolLog.setOtherNoType("0");
        mLCDelPolLog.setPrtNo(tLCGrpContDB.getPrtNo());
        if (tLCGrpContDB.getAppFlag().equals("1"))
        {
            mLCDelPolLog.setIsPolFlag("1");
        }
        else
        {
            mLCDelPolLog.setIsPolFlag("0");
        }
        mLCDelPolLog.setOperator(mGlobalInput.Operator);
        mLCDelPolLog.setManageCom(mGlobalInput.ManageCom);
        mLCDelPolLog.setMakeDate(theCurrentDate);
        mLCDelPolLog.setMakeTime(theCurrentTime);
        mLCDelPolLog.setModifyDate(theCurrentDate);
        mLCDelPolLog.setModifyTime(theCurrentTime);
        map.put(mLCDelPolLog, "INSERT");

        // 如果保单为非共保保单，清除相关共保要素。
        MMap tTmpMap = null;
        tTmpMap = delCoInsuranceInfo(mLCGrpContSchema.getGrpContNo());
        if (tTmpMap == null)
        {
            return false;
        }
        map.add(tTmpMap);
        // ----------------------------

        //开始备份
        map.put(
                "insert into LOBGrpCont (select * from LCGrpCont where GrpContNo='"
                        + tGrpContNo + " ')", "INSERT");
        //map.put("insert into LOBCont (select * from LCCont where GrpContNo='"+tGrpContNo+" ')", "INSERT");
        map.put(
                "insert into LOBGrpAppnt (select * from LCGrpAppnt where GrpContNo='"
                        + tGrpContNo + " ')", "INSERT");
        //map.put("insert into LOBGrpPol (select * from LCGrpPol where GrpContNo='"+tGrpContNo+" ')", "INSERT");
        //map.put("insert into LOBPol (select * from LCPol where GrpContNo='"+tGrpContNo+" ')", "INSERT");
        //开始删除
        map.put(mLCGrpAppntDB.getSchema(), "DELETE");
        map.put(mLCGrpContSchema, "DELETE");
        map.put(this.mLCGrpSpecFeeSet, "DELETE");
        map.put("delete from LCDiseaseImpart where GrpContNo='" + tGrpContNo
                + "'", "DELETE");
        map.put("delete from LCHistoryImpart where GrpContNo='" + tGrpContNo
                + "'", "DELETE");
        map.put("delete from LCGrpServInfo where GrpContNo='" + tGrpContNo
                + "'", "DELETE");
        map.put("delete from LCCustomerImpart where GrpContNo='" + tGrpContNo
                + "'", "DELETE");
        //add by zhangxing  增加保全定期结算接口
        map.put("delete from LCGrpBalPlan where GrpContNo = '" + tGrpContNo
                + "'", "DELETE");
        
        map.put("delete from LCExtend where prtno = '"+mLCGrpContSchema.getPrtNo()+"' ", "DELETE");
        return true;
    }

    /**
     * 检查地址数据是否正确
     * 如果在处理过程中出错或者数据有错误，则返回false,否则返回true
     */

    private boolean checkLCGrpAddress()
    {

        if (mLCGrpAddressSchema != null)
        {
            if (mLCGrpAddressSchema.getAddressNo() != null)
            {
                //如果有地址号
                if (!mLCGrpAddressSchema.getAddressNo().equals(""))
                {
                    LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();

                    tLCGrpAddressDB.setAddressNo(mLCGrpAddressSchema
                            .getAddressNo());
                    tLCGrpAddressDB.setCustomerNo(mLCGrpAddressSchema
                            .getCustomerNo());
                    if (tLCGrpAddressDB.getInfo() == false)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "数据库查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    //                    if (!StrTool.compareString(mLCGrpAddressSchema.
                    //                                               getCustomerNo(),
                    //                                               tLCGrpAddressDB.getCustomerNo()))
                    //                    {
                    //                        CError tError = new CError();
                    //                        tError.moduleName = "ContBL";
                    //                        tError.functionName = "checkAddress";
                    //                        tError.errorMessage =
                    //            "您输入的地址信息中的客户号"+mLCGrpAddressSchema.
                    //                                               getCustomerNo()+"与数据库里对应地址的客户号"+tLCGrpAddressDB.getCustomerNo()+"不符，请去掉地址号，重新生成!";
                    //                        this.mErrors.addOneError(tError);
                    //
                    //                        return false;
                    //
                    //                    }
                    //
                    //                    if (!StrTool.compareString(mLCGrpAddressSchema.
                    //                                               getAddressNo(),
                    //                                               tLCGrpAddressDB.getAddressNo()))
                    //                    {
                    //                        CError tError = new CError();
                    //                        tError.moduleName = "ContBL";
                    //                        tError.functionName = "checkAddress";
                    //                        tError.errorMessage =
                    //            "您输入的地址信息中的客户号"+mLCGrpAddressSchema.
                    //                                               getCustomerNo()+"与数据库里对应地址的客户号"+tLCGrpAddressDB.getCustomerNo()+"不符，请去掉地址号，重新生成!";
                    //                        this.mErrors.addOneError(tError);
                    //
                    //                        return false;
                    //
                    //                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema
                            .getGrpAddress(), tLCGrpAddressDB.getGrpAddress()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的单位地址"
                                + mLCGrpAddressSchema.getGrpAddress()
                                + "与数据库里对应地址的单位地址"
                                + tLCGrpAddressDB.getGrpAddress()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema
                            .getGrpZipCode(), tLCGrpAddressDB.getGrpZipCode()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的单位邮政编码"
                                + mLCGrpAddressSchema.getGrpZipCode()
                                + "与数据库里对应地址的单位邮政编码"
                                + tLCGrpAddressDB.getGrpZipCode()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema
                            .getLinkMan1(), tLCGrpAddressDB.getLinkMan1()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人一"
                                + mLCGrpAddressSchema.getLinkMan1()
                                + "与数据库里对应地址的联系人一"
                                + tLCGrpAddressDB.getLinkMan1()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool
                            .compareString(
                                    mLCGrpAddressSchema.getDepartment1(),
                                    tLCGrpAddressDB.getDepartment1()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人一部门"
                                + mLCGrpAddressSchema.getDepartment1()
                                + "与数据库里对应地址的联系人一部门"
                                + tLCGrpAddressDB.getDepartment1()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema
                            .getHeadShip1(), tLCGrpAddressDB.getHeadShip1()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人一职务"
                                + mLCGrpAddressSchema.getDepartment1()
                                + "与数据库里对应地址的联系人一职务"
                                + tLCGrpAddressDB.getDepartment1()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema.getPhone1(),
                            tLCGrpAddressDB.getPhone1()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人一电话"
                                + mLCGrpAddressSchema.getDepartment1()
                                + "与数据库里对应地址的联系人一电话"
                                + tLCGrpAddressDB.getDepartment1()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(
                            mLCGrpAddressSchema.getE_Mail1(), tLCGrpAddressDB
                                    .getE_Mail1()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人一EMAIL"
                                + mLCGrpAddressSchema.getDepartment1()
                                + "与数据库里对应地址的联系人一EMAIL"
                                + tLCGrpAddressDB.getDepartment1()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema.getFax1(),
                            tLCGrpAddressDB.getFax1()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人一传真"
                                + mLCGrpAddressSchema.getDepartment1()
                                + "与数据库里对应地址的联系人一传真"
                                + tLCGrpAddressDB.getDepartment1()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema
                            .getLinkMan2(), tLCGrpAddressDB.getLinkMan2()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人二"
                                + mLCGrpAddressSchema.getLinkMan2()
                                + "与数据库里对应地址的联系人二"
                                + tLCGrpAddressDB.getLinkMan2()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool
                            .compareString(
                                    mLCGrpAddressSchema.getDepartment2(),
                                    tLCGrpAddressDB.getDepartment2()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人二部门"
                                + mLCGrpAddressSchema.getDepartment2()
                                + "与数据库里对应地址的联系人二部门"
                                + tLCGrpAddressDB.getDepartment2()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema
                            .getHeadShip2(), tLCGrpAddressDB.getHeadShip2()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人二职务"
                                + mLCGrpAddressSchema.getDepartment2()
                                + "与数据库里对应地址的联系人二职务"
                                + tLCGrpAddressDB.getDepartment2()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema.getPhone2(),
                            tLCGrpAddressDB.getPhone2()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人二电话"
                                + mLCGrpAddressSchema.getDepartment2()
                                + "与数据库里对应地址的联系人二电话"
                                + tLCGrpAddressDB.getDepartment2()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(
                            mLCGrpAddressSchema.getE_Mail2(), tLCGrpAddressDB
                                    .getE_Mail2()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人二EMAIL"
                                + mLCGrpAddressSchema.getDepartment2()
                                + "与数据库里对应地址的联系人二EMAIL"
                                + tLCGrpAddressDB.getDepartment2()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                    if (!StrTool.compareString(mLCGrpAddressSchema.getFax2(),
                            tLCGrpAddressDB.getFax2()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息中的联系人二传真"
                                + mLCGrpAddressSchema.getDepartment2()
                                + "与数据库里对应地址的联系人儿传真"
                                + tLCGrpAddressDB.getDepartment2()
                                + "不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                }
            }

        }

        return true;
    }

    /**
     * 删除共保要素。
     * @param cGrpContNo
     * @return
     */
    private MMap delCoInsuranceInfo(String cGrpContNo)
    {
        MMap tMMap = null;

        CoInsuranceGrpContBL tCoInsuranceGrpContBL = new CoInsuranceGrpContBL();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpContNo", cGrpContNo);
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        tMMap = tCoInsuranceGrpContBL.getSubmitMap(tVData, "Delete");

        if (tMMap == null)
        {
            buildError("delCoInsuranceInfo", "共保要素处理失败。");
            return null;
        }

        return tMMap;
    }
    
    /**
     * 处理综合开拓数据。
     * @param cGrpContNo
     * @return
     */
    private MMap delExtendInfo(LCExtendSchema aLCExtendSchema)
    {
        MMap tMMap = null;

        ExtendBL tExtendBL = new ExtendBL();

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(aLCExtendSchema);

        tMMap = tExtendBL.submitData(tVData, "");

        if (tMMap == null)
        {
            buildError("delCoInsuranceInfo", "共保要素处理失败。");
            return null;
        }

        return tMMap;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GroupContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
}
