package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.File;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOContImportBL
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private ArrayList mContList = null;

    private MMap map = new MMap();

    public BPOContImportBL()
    {
    }

    /**
     * 数据提交的公共方法，调用getSubmitMap进行业务处理，
     * 处理成功后将返回结果保存入内部VData对象中
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if (getSubmitMap(cInputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据库失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getSubmitMMap
     * 外包调用的接口方法。本方法调用checkData进行数据校验，调用dealData进行业务逻辑处
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return MMap：处理成功，返回处理后的带提交数据库集合, 处理失败，返回null
     */
    public MMap getSubmitMap(VData cInputData, String operate)
    {
        if (!getInputData(cInputData))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法，是否可进行业务逻辑处理
     * @return boolean：true合法，false不合法
     */
    private boolean checkData()
    {
        if (mContList == null || mContList.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOImportContBL";
            tError.functionName = "checkData";
            tError.errorMessage = "请传入保单信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * dealData
     * 本类的核心方法，用来按业务逻辑处理传入的数据
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        boolean allSuccFlag = true;

        //先生成xml
        BPOCreateContXMLBL bl = new BPOCreateContXMLBL();
        ArrayList tXmlPathList = bl.createXML(mContList);
        mErrors.copyAllErrors(bl.mErrors);
        if (tXmlPathList == null)
        {
            return false;
        }

        String tErrorInfo = "";
        Iterator tIterator = tXmlPathList.iterator();
        while (tIterator.hasNext())
        {
            String tXmlPath = (String) tIterator.next();

            LCParseGuideIn tLCParseGuideIn = new LCParseGuideIn();
            if (!tLCParseGuideIn.parseBPOXml(tXmlPath, mGI, false))
            {
                allSuccFlag = false;
            }

            mErrors.copyAllErrors(tLCParseGuideIn.mErrors);

            String tBatchNo = tXmlPath.substring(tXmlPath.lastIndexOf("/") + 1,
                    tXmlPath.indexOf(".xml"));
            LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
            tLCGrpImportLogDB.setBatchNo(tBatchNo);
            String sql = "select PrtNo, ErrorInfo " + "from LCGrpImportLog "
                    + "where BatchNo = '" + tBatchNo + "' "
                    + "   and ContID not in('C', 'X') "
                    + "   and (ErrorState is null or ErrorState != '0') ";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                tErrorInfo += tSSRS.GetText(i, 1) + " " + tSSRS.GetText(i, 2);
            }
        }

        if (!"".equals(tErrorInfo))
        {
            CError tError = new CError();
            tError.moduleName = "BPOContImportBL";
            tError.functionName = "dealData";
            tError.errorMessage = tErrorInfo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        allSuccFlag = mErrors.needDealError() ? false : allSuccFlag;

        return allSuccFlag;
    }

    /**
     * getInputData
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mContList = (ArrayList) data.getObjectByObjectName("ArrayList", 0);

        return true;
    }

}
