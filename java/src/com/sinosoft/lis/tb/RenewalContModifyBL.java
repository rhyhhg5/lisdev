package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:网销续保维护LCduty feeRate 维护为0,UI层 </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: sinosoft</p>
 * @author licaiyan
 * @version 1.0
 */
public class RenewalContModifyBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    private String mOperator;

    private String mManageCom;


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCDutySet mLCDutySet = new LCDutySet();


    public RenewalContModifyBL()
    {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //将VData数据还原成业务需要的类
        if (!this.getInputData())
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!this.dealData())
        {
            return false;
        }

        System.out.println("---dealdata successful---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            return false;
        }
        return true;
    }


    private boolean checkData()
    {
        return true;
    }

    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData()
    {
        //全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

        if (mGlobalInput == null){
            mErrors.addOneError(new CError("没有得到全局量信息"));
            return false;
        }
        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        mLCDutySet.set((LCDutySet)mInputData.getObjectByObjectName("LCDutySet",0));

        return true;
    }

    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }


    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {
        //记录当前操作员
        mResult.clear();

        mResult.add(mMap);
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }
}
