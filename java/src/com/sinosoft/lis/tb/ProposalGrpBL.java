/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import java.util.Hashtable;

import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKGrpPolSchema;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YT
 * @version 1.0
 */
public class ProposalGrpBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	/** 数据操作字符串 */
	private String mOperate;

	/**报价单责任信息*/
	private LKGrpPolSchema mLKgrppolSchema ; 

	private GlobalInput mGlobalInput ;

	private static String NOFOUND = "NOFOUND";

	private Hashtable m_hashLMCalMode = new Hashtable();

	//统一更新日期，时间
	private String theCurrentDate = PubFun.getCurrentDate();

	private String theCurrentTime = PubFun.getCurrentTime();

	//算法对应SQL语句所在表结构
	private LMCalModeSchema mLMCalMode = new LMCalModeSchema();

	public ProposalGrpBL()
	{
	}

	/**
	 * 传输数据的公共方法
	 *
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @param cOperate String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (getInputData(cInputData) == false)
		{
			return false;
		}

		//数据操作校验
		if (checkData() == false)
		{
			return false;
		}

		//进行业务处理
		if (dealData() == false)
		{
			return false;
		}
		return true;
	}


	/**
	 * 数据处理校验 如果在处理过程中出错，则返回false,否则返回true
	 *
	 * @return boolean
	 */
	private boolean checkData()
	{
		if(mLKgrppolSchema.getDutyCode().equals("ai001")){
			if("".equals(mLKgrppolSchema.getOccupationType())||mLKgrppolSchema.getOccupationType()==null){
				CError tError = new CError();
				tError.moduleName = "ProposalGrpBl";
				tError.functionName = "checkData";
				tError.errorMessage ="报价单算费要素传入有误，请检查！";
				this.mErrors.addOneError(tError);
				return false;
			}
			if("".equals(mLKgrppolSchema.getOccupationType())||mLKgrppolSchema.getOccupationType()==null){
				CError tError = new CError();
				tError.moduleName = "ProposalGrpBl";
				tError.functionName = "checkData";
				tError.errorMessage ="报价单算费要素传入有误，请检查！";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		return true;
	}




	/**
	 * 根据前面的输入数据，进行逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 *
	 * @return boolean
	 */
	private boolean dealData()
	{
		if (!getSQL()){
			CError tError = new CError();
			tError.moduleName = "ProposalGrpBl";
			tError.functionName = "dealData";
			tError.errorMessage ="获取算费SQL出现问题，请检查！";
			this.mErrors.addOneError(tError);
			return false;
		}

		Double prem = calPrem();
		if(mLKgrppolSchema.getPeoples()==0||"".equals(mLKgrppolSchema.getPeoples())){
			CError tError = new CError();
			tError.moduleName = "ProposalGrpBl";
			tError.functionName = "dealData";
			tError.errorMessage ="录入人数出现问题，请检查！";
			this.mErrors.addOneError(tError);
			return false;
		}
		Double sumPrem = prem*mLKgrppolSchema.getPeoples();
		System.out.println("总保费："+sumPrem+"Operate="+mOperate);
		System.out.println("managecom="+mGlobalInput.ManageCom);
		//暂时放在这里
		mLKgrppolSchema.setMonthsalary(sumPrem);
		//创建lkgrppol数据
		if(!createLKGrppol()){
			CError tError = new CError();
			tError.moduleName = "ProposalGrpBl";
			tError.functionName = "dealData";
			tError.errorMessage ="插入数据失败";
			this.mErrors.addOneError(tError);
			return false;  
		}

		return true ;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData)
	{
		mLKgrppolSchema = (LKGrpPolSchema) cInputData.getObjectByObjectName("LKgrppolSchema", 0);   
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		if(mLKgrppolSchema==null||"".equals(mLKgrppolSchema))
		{
			CError tError = new CError();
			tError.moduleName = "ProposalGrpBl";
			tError.functionName = "getInputData";
			tError.errorMessage ="报价单操作员有误，请检查！";
			this.mErrors.addOneError(tError);
			return false;
		}

		if(mGlobalInput==null||"".equals(mGlobalInput))
		{

			CError tError = new CError();
			tError.moduleName = "ProposalGrpBl";
			tError.functionName = "getInputData";
			tError.errorMessage ="报价单操作员有误，请检查！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult()
	{
		return mResult;
	}

	/**
	 * 根据算法编码查找LMCalMode
	 *
	 * @param strCalCode String
	 * @return com.sinosoft.lis.schema.LMCalModeSchema
	 */
	public LMCalModeSchema findCalModeByCalCode(String strCalCode) {

		Hashtable hash = m_hashLMCalMode;

		Object obj = hash.get(strCalCode);

		if( obj != null ) {
			if( obj instanceof String ) {
				return null;
			} else {
				return (LMCalModeSchema)obj;
			}
		} else {
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();

			tLMCalModeDB.setCalCode(strCalCode);

			if( !tLMCalModeDB.getInfo() ) {
				mErrors.copyAllErrors(tLMCalModeDB.mErrors);
				hash.put(strCalCode, NOFOUND);
				return null;
			}

			hash.put(strCalCode, tLMCalModeDB.getSchema());
			return tLMCalModeDB.getSchema();
		}
	}

	private boolean getSQL()
	{
		CachedRiskInfo cri = CachedRiskInfo.getInstance();
		LMCalModeSchema tLMCalModeSchema = cri.findCalModeByCalCode(mLKgrppolSchema.getDutyCode());
		if (tLMCalModeSchema == null)
		{
			CError tError = new CError();
			tError.moduleName = "Calculator";
			tError.functionName = "getSql";
			tError.errorMessage = "得到" + mLKgrppolSchema.getDutyCode() + "的SQL语句时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		mLMCalMode.setSchema(tLMCalModeSchema);
		return true;
	}

	private Double calPrem(){
		// 计算
		Calculator mCalculator = new Calculator();
		mCalculator.setCalCode(mLMCalMode.getCalCode());
		//增加基本要素
		mCalculator.addBasicFactor("Amnt",String.valueOf(mLKgrppolSchema.getAmnt()) );
		mCalculator.addBasicFactor("OccuType",mLKgrppolSchema.getOccupationType() );
		mCalculator.addBasicFactor("Age",String.valueOf(mLKgrppolSchema.getAvgAge()));
		String tStr = "";
		tStr = mCalculator.calculate();
		Double mValue;
		if (tStr == null || tStr.trim().equals("")) {
			mValue = 0.00;
		} else {
			mValue = Double.parseDouble(tStr);
		}
		System.out.println(PubFun.setPrecision(mValue, "0.00")) ;
		return mValue;
	}

	private boolean createLKGrppol(){
		System.out.println("managecom="+mGlobalInput.ManageCom);
		String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
		String aGrpPolNo = PubFun1.CreateMaxNo("KGRPPOLNO", tLimit);
		System.out.println("aGrpPolNo="+aGrpPolNo);
		mLKgrppolSchema.setManageCom(mGlobalInput.ManageCom);
		mLKgrppolSchema.setOperator(mGlobalInput.Operator);
		mLKgrppolSchema.setGrpPolNo(aGrpPolNo);
		mLKgrppolSchema.setMakeDate(theCurrentDate);
		mLKgrppolSchema.setModifyDate(theCurrentDate);
		mLKgrppolSchema.setMakeTime(theCurrentTime);
		mLKgrppolSchema.setModifyTime(theCurrentTime);
		PubSubmit mPubSubmit = new PubSubmit();
		VData vData = new VData();
		MMap map = new MMap();
		map.put(mLKgrppolSchema, "INSERT");
		vData.add(map);
		if(!mPubSubmit.submitData(vData, "")){
			CError tError = new CError();
			tError.moduleName = "ProposalGrpBl";
			tError.functionName = "createLKGrppol";
			tError.errorMessage ="创建LKGRPpol数据失败，请检查！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
