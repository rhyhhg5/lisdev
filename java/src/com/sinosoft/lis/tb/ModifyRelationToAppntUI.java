/**
 * created 2009-1-22
 * by LY
 */
package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ModifyRelationToAppntUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public ModifyRelationToAppntUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	ModifyRelationToAppntBL tModifyRelationToAppntBL = new ModifyRelationToAppntBL();
            if (!tModifyRelationToAppntBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tModifyRelationToAppntBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
