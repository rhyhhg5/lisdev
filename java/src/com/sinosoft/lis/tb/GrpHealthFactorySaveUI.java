package com.sinosoft.lis.tb;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.workflow.bq.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class GrpHealthFactorySaveUI
{

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public GrpHealthFactorySaveUI() {}
  public static void main(String[] args) {
	VData tVData = new VData();
	GlobalInput mGlobalInput = new GlobalInput();
	TransferData mTransferData = new TransferData();

	/** 全局变量 */
	mGlobalInput.Operator ="S001";
	mGlobalInput.ComCode  ="86";
	mGlobalInput.ManageCom="86";

  // 准备传输数据 VData
    mTransferData.setNameAndValue("GrpPolNo","86110020040120000059");
    mTransferData.setNameAndValue("PolNo","00000000000000000000");
    mTransferData.setNameAndValue("ContNo","00000000000000000000");
    mTransferData.setNameAndValue("RiskCode","211801");

	LCFactorySet tLCFactorySet=new LCFactorySet();

	LCFactorySchema tLCFactorySchema = new LCFactorySchema();
	tLCFactorySchema.setGrpPolNo("86110020040120000059");
	tLCFactorySchema.setPolNo("00000000000000000000");
	tLCFactorySchema.setContNo("00000000000000000000");
	tLCFactorySchema.setOtherNo( "111111");
	tLCFactorySchema.setFactoryType( "000001");
	tLCFactorySchema.setFactoryCode( "0000013");
	tLCFactorySchema.setParams("1000");

    tLCFactorySet.add(tLCFactorySchema);

//	tLCFactorySchema = new LCFactorySchema();
//	tLCFactorySchema.setGrpPolNo("86110020040120000026");
//	tLCFactorySchema.setPolNo("00000000000000000000");
//	tLCFactorySchema.setContNo("00000000000000000000");
//	tLCFactorySchema.setOtherNo( "211001");
//	tLCFactorySchema.setFactoryType( "000001");
//	tLCFactorySchema.setFactoryCode( "0000011");
//	tLCFactorySchema.setParams("1000,2000,0.85");
//    tLCFactorySet.add(tLCFactorySchema);
	mTransferData.setNameAndValue("LCFactorySet",tLCFactorySet) ;


	/**总变量*/
	tVData.add(mGlobalInput) ;
	tVData.add(mTransferData) ;



	GrpHealthFactorySaveUI tGrpHealthFactorySaveBL = new GrpHealthFactorySaveUI();
	try{
	  if(tGrpHealthFactorySaveBL.submitData(tVData,"save"))
	   {
		   VData  tResult = new VData();
	       //tResult = tActivityOperator.getResult() ;
	   }
	}
   catch (Exception ex) {
		ex.printStackTrace();
		 }

  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
	//将操作数据拷贝到本类中
	this.mOperate = cOperate;

	GrpHealthFactorySaveBL tGrpHealthFactorySaveBL = new GrpHealthFactorySaveBL();

	System.out.println("---GrpHealthFactorySave UI BEGIN---");
	if (tGrpHealthFactorySaveBL.submitData(cInputData,mOperate) == false)
	{
		  // @@错误处理
	  this.mErrors.copyAllErrors(tGrpHealthFactorySaveBL.mErrors);
	  mResult.clear();
	  return false;
	}
	return true;
  }
}