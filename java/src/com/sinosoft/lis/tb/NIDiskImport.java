package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.diskimport.MultiSheetImporter;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LCInsuredListPolSet;
import java.util.HashMap;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class NIDiskImport {
/** 从xls文件读入Sheet的开始行*/
    private static final int STARTROW = 2;

    /** Sheet对应table的名字*/
    private static final String[] mTableNames = {"LCInsuredList", "",
                                            "LCInsuredListPol"};

    /** 使用默认的导入方式 */
    private MultiSheetImporter importer = null;
    /** Sheet Name */
    private String[] mSheetName;
    /** 错误处理 */
    public CErrors mErrrors = new CErrors();

    public NIDiskImport(String fileName, String configFileName,
                         String [] sheetName) {
        mSheetName = sheetName;
        importer = new MultiSheetImporter(fileName, configFileName, mSheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport() {
        importer.setTableName(mTableNames);
        importer.setMStartRows(STARTROW);
        if (!importer.doImport()) {
            mErrrors.copyAllErrors(importer.mErrors);
            return false;
        }
        String localVersion = getVersion();
        if (localVersion == null) {
            String str = "导入模版错误，请更换最新的磁盘导入模版!";
            mErrrors.addOneError(str);
            return false;
        }
        String strSql =
                "select SysVarValue from LDSysvar where SysVar='NiDiskImportVer' or SysVar='NiDiskComImportVer'";
        strSql += " or SysVar='NiDiskImportVer2.2' order by sysvarvalue desc with ur";
            ExeSQL exeSql = new ExeSQL();
            System.out.println("系统中存在的所有版本号："+strSql);

            SSRS ssrs = new SSRS();
            ssrs = exeSql.execSQL(strSql);
            if (ssrs.GetText(1, 1) == null || "".equals(ssrs.GetText(1, 1))) {
                String str = "数据库中没有存储版本号的信息!";
                mErrrors.addOneError(str);
                return false;
            }
            System.out.println("最新版本号："+ssrs.GetText(1, 1));
            if (!localVersion.equals(ssrs.GetText(2, 1)) && !localVersion.equals(ssrs.GetText(1, 1))&& !localVersion.equals(ssrs.GetText(3, 1))) 
            {
                mErrrors.addOneError("导入模版版本不正确，请下载最新模板进行导入。");
                return false;
            }
            System.out.println("版本号效验通过，NIDiskImport完成。");
        return true;
    }

    /**
     * getSchemaSet 获取从MutiSheetImporter获得的SchemaSet
     *
     * @return LCInsuredListSet
     */
    public LCInsuredListSet getSchemaSet() {
        LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
        HashMap tHashMap = importer.getResult();
        tLCInsuredListSet.add((LCInsuredListSet) tHashMap.get(mSheetName[0]));
        return tLCInsuredListSet;
    }
    public LCInsuredListPolSet getLCInsuredListPolSet() {
    LCInsuredListPolSet tLCInsuredListPolSet = new LCInsuredListPolSet();
    HashMap tHashMap = importer.getResult();
    tLCInsuredListPolSet.add((LCInsuredListPolSet) tHashMap.get(mSheetName[
            2]));
    return tLCInsuredListPolSet;
}


    public static void main(String[] args) {
        String path = "D:\\workspace\\UI\\temp\\18000013201_01.xls";
        String config = "D:\\workspace\\UI\\temp\\GrpDiskImport.xml";
        NIDiskImport grpdiskimport = new NIDiskImport(path, config, new String[1]);
        grpdiskimport.doImport();
        LCInsuredListSet tLCInsuredListSet =
                (LCInsuredListSet) grpdiskimport.getSchemaSet();
        for (int i = 1; i <= tLCInsuredListSet.size(); i++) {
            System.out.println("处理数据 " + i +
                               tLCInsuredListSet.get(i).encode().replace('|',
                    '\t'));
        }
    }
    /**
 * 用于获取导入文件版本号
 * @return String
 */
public String getVersion() {
    HashMap tHashMap = importer.getResult();
    String version = "";
    version = (String) tHashMap.get(mSheetName[1]);
    return version;
}


}
