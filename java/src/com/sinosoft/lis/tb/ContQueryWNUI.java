package com.sinosoft.lis.tb;

//程序名称：ContQueryWNUI.java
//程序功能：
//创建日期：2007-11-10
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ContQueryWNUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public ContQueryWNUI()
    {
    	System.out.println("UI->ContQueryWNUI()");
    }

    /**
     * 接收页面传入的保单号， 调用IntlDownloadCustCardBL.java生成客户卡信息。
     * @param cInputData VData：包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	LCContSet mLCContSet：保单信息
     * @param cOperator String：操作方式，可为“”
     * @return boolean：成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperator)
    {
    	System.out.println("UI->submitDate()");
    	ContQueryWNBL bl = new ContQueryWNBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        ContQueryWNUI tContQueryWNUI = new
            ContQueryWNUI();
    }
}
