package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class TbGrpContractorUI {

    /** �������� */
    public CErrors mErrors = new CErrors();


    public TbGrpContractorUI() {
    }

    /**
     * submitData
     *
     * @param tInputData VData
     * @param tOperation String
     * @return boolean
     */
    public boolean submitData(VData tInputData, String tOperate) {

        TbGrpContractorBL tTbGrpContractorBL = new TbGrpContractorBL();

        if (!tTbGrpContractorBL.submitData(tInputData, tOperate)) {
            mErrors.copyAllErrors(tTbGrpContractorBL.mErrors);
            return false;
        }

        return true;
    }

    /**
     * ���Ժ���
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        /** case 1 */
        String tCalFactor[] = {"StandbyFlag1", "StandbyFlag2", "StandbyFlag3"};
        String tCalFactorValue[] = {"1", "2", "3"};
        String tCalFactorType[] = {"1", "2", "3"};
        String tGrpContNo = "1400002584";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        tG.Operator = "001";
        String tOperate = "INSERT||MAIN";
        /** case 2 */
//        String tCalFactor[] = {"StandbyFlag1", "StandbyFlag2", "StandbyFlag3"};
//        String tCalFactorValue[] = {"1", "2", "3"};
//        String tCalFactorType[] = {"1", "2", "3"};
//        String tGrpContNo = "1400002584";
//        tG.ManageCom = "86";
//        tG.ComCode = "86";
//        tG.Operator = "001";
//        String tOperate = "DELETE||MAIN";

        LCContPlanDutyParamSet tLCContPlanDutyParamSet = new
                LCContPlanDutyParamSet();
        for (int i = 0; i < tCalFactor.length; i++) {
            LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new
                    LCContPlanDutyParamSchema();
            tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
            tLCContPlanDutyParamSchema.setCalFactor(tCalFactor[i]);
            tLCContPlanDutyParamSchema.setCalFactorType(tCalFactorType[i]);
            tLCContPlanDutyParamSchema.setCalFactorValue(tCalFactorValue[i]);
            tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        }
        VData tVData = new VData();
        tVData.add(tLCContPlanDutyParamSet);
        tVData.add(tG);

        TbGrpContractorBL tTbGrpContractorBL = new TbGrpContractorBL();
        if (!tTbGrpContractorBL.submitData(tVData, tOperate)) {
            System.out.println(tTbGrpContractorBL.mErrors.getContent());
        }
    }
}
