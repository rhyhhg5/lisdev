package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class RelatedTransactionQueryUI {
	
	/** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    public RelatedTransactionQueryUI(){}
    
    public boolean submitData(VData cInputData, String cOperate){
    	this.mInputData = (VData)cInputData.clone();
        this.mOperate = cOperate;
        RelatedTransactionQueryBL tRelatedTransactionQueryBL = new RelatedTransactionQueryBL();
        if(tRelatedTransactionQueryBL.submitData(mInputData, mOperate) == false)	{
            // @@错误处理
            this.mErrors.copyAllErrors(tRelatedTransactionQueryBL.mErrors);
            mResult.clear();
            return false;
          }	else {
            mResult = tRelatedTransactionQueryBL.getResult();
          }
    	return true;
    }
    
    public VData getResult(){
        return mResult;
    }
}
