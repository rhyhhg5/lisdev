
package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LcpolAmntModifyBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */
	private VData mInputData = new VData();

	/** 公共输入信息 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

    //	录入
	private String mPolNo = "";
	private String mGrpContNo = "";
	private String mRiskCode = "";
	private String mAmnt = "";
	private String mContNo = "";

	private MMap mMap = new MMap();   
	public LcpolAmntModifyBL() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LcpolAmntModifyBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
        //保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		
		mPolNo = (String)tTransferData.getValueByName("PolNo");
		mGrpContNo = (String)tTransferData.getValueByName("GrpContNo");
		mRiskCode = (String)tTransferData.getValueByName("RiskCode");
		mAmnt = (String)tTransferData.getValueByName("Amnt");
		mContNo = (String)tTransferData.getValueByName("ContNo");

		return true;
	}
	
	private boolean dealData(){
		String tSQLLCGet;
		String tSQLLCDuty;
		String tSQLLCPol;
		String tSQLLCCont;
		String tSQLLCGrpPol;
		String tSQLLCGrpCont;
		
		tSQLLCGet = "update lcget set standmoney = "+mAmnt+",actuget = " +mAmnt+ ", modifydate = current date, modifytime = current time where polno ='"+mPolNo+"'";
		tSQLLCDuty = "update lcduty set amnt = "+mAmnt+",riskamnt = "+mAmnt+",modifydate = current date, modifytime = current time where polno ='"+mPolNo+"'";
		tSQLLCPol = "update lcpol set amnt = "+mAmnt+",riskamnt = "+mAmnt+",modifydate = current date, modifytime = current time where polno='"+mPolNo+"'";
		tSQLLCCont = "update lccont set amnt = (select sum(amnt) from lcpol lcp where lcp.contno = '"+mContNo+"'),modifydate = current date, modifytime = current time where grpcontno = '"+mGrpContNo+"' and contno = '"+mContNo+"' ";
		tSQLLCGrpPol = "update lcgrppol set amnt = (select sum(lcp.amnt) from lcpol lcp where lcp.grpcontno = '"+mGrpContNo+"' and riskcode = '"+mRiskCode+"'),modifydate = current date,modifytime = current time where grpcontno = '"+mGrpContNo+"' and riskcode = '"+mRiskCode+"' ";
		tSQLLCGrpCont = "update lcgrpcont set amnt = (select sum(lcp.amnt) from lcpol lcp where lcp.grpcontno = '"+mGrpContNo+"'), modifydate = current date, modifytime = current time where grpcontno = '"+mGrpContNo+"' ";
		
		mMap.put(tSQLLCGet, SysConst.UPDATE);
		mMap.put(tSQLLCDuty, SysConst.UPDATE);
		mMap.put(tSQLLCPol, SysConst.UPDATE);
		mMap.put(tSQLLCCont, SysConst.UPDATE);
		mMap.put(tSQLLCGrpPol, SysConst.UPDATE);
		mMap.put(tSQLLCGrpCont, SysConst.UPDATE);
		
		return true;
	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private void jbInit() throws Exception {
	}

}
