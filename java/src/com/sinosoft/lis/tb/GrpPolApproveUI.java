package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-17
 */
public class GrpPolApproveUI
{
	/** 传入数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	// @Constructor
	public GrpPolApproveUI() {}

	// @Main
	public static void main(String[] args)
	{
        GlobalInput tG = new GlobalInput();
        tG.Operator = "aaa";
        LCGrpContSchema p = new LCGrpContSchema();
        p.setProposalGrpContNo( "120110000000006" );
        p.setApproveFlag( "9" );
        VData tVData = new VData();
        tVData.add( p );
        tVData.add( tG );
        GrpPolApproveUI ui = new GrpPolApproveUI();
        if( ui.submitData( tVData, "" ) == true )
            System.out.println("---ok---");
        else
            System.out.println("---NO---");
	}

	// @Method
	/**
	* 数据提交方法
	* @param: cInputData 传入的数据
	*		  cOperate 数据操作字符串
	* @return: boolean
	**/
	public boolean submitData( VData cInputData, String cOperate )
	{
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;

		GrpPolApproveBL tGrpPolApproveBL = new GrpPolApproveBL();

        System.out.println("----UI BEGIN---");
		if( tGrpPolApproveBL.submitData( cInputData, mOperate ) == false )
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tGrpPolApproveBL.mErrors );
			return false;
		}
        System.out.println("----UI END---");
		return true;
	}

}
