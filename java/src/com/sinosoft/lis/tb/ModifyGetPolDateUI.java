/**
 * created 2009-1-22
 * by LY
 */
package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ModifyGetPolDateUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public ModifyGetPolDateUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	ModifyGetPolDateBL tModifyGetPolDateBL = new ModifyGetPolDateBL();
            if (!tModifyGetPolDateBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tModifyGetPolDateBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
