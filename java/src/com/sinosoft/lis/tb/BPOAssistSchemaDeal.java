package com.sinosoft.lis.tb;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sinosoft.lis.db.LCExtendDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCExtendSchema;
import com.sinosoft.lis.vschema.LCExtendSet;

/**
 * 外包综合开拓信息处理
 * 
 * @author 张成轩
 *
 */
public class BPOAssistSchemaDeal {
	
	/**
	 * 节点处理
	 * 
	 * @param cContNode
	 * @param cPrtno
	 * @param cOperator
	 * @return
	 */
	public LCExtendSet setSchema(Node cContNode, String cPrtno, String cOperator) {
		LCExtendSet tLCExtendSet = new LCExtendSet();

		XObject tAssistSalechnl = null;
		XObject tAssistAgentCode = null;
		
		NodeList tRowNodeList = null;
		
		try
        {
            tRowNodeList = XPathAPI.selectNodeList(cContNode, "ROW");
        }
        catch(TransformerException ex)
        {
            ex.printStackTrace();
            return null;
        }
        
        if(tRowNodeList == null){
        	return null;
        }
		
        for(int i = 0; i < tRowNodeList.getLength(); i++)
        {
        	Node tRowNode = tRowNodeList.item(i);
        	
        	try {
    			tAssistSalechnl = XPathAPI.eval(tRowNode, "AssistSalechnl");
    		} catch (TransformerException ex) {
    			System.out.println("--无AssistSalechnl节点--");
    			return null;
    		}
    		
    		try {
    			tAssistAgentCode = XPathAPI.eval(tRowNode, "AssistAgentCode");
    		} catch (TransformerException ex) {
    			System.out.println("--无AssistAgentCode节点--");
    			return null;
    		}

    		String salechnl = tranNode(tAssistSalechnl);
    		String agentcode = tranNode(tAssistAgentCode);
    		
    		if(isNull(salechnl) && isNull(agentcode)){
    			continue;
    		}
    		
    		LCExtendSchema tLCExtendSchema = new LCExtendSchema();
    		
    		tLCExtendSchema.setAssistSalechnl(salechnl);
    		tLCExtendSchema.setAssistAgentCode(agentcode);
    		
    		tLCExtendSchema.setSerNo(PubFun1.CreateMaxNo("ExtendSerNo", 20));
    		tLCExtendSchema.setPrtNo(cPrtno);
    		tLCExtendSchema.setContFlag("1");
    		tLCExtendSchema.setManageCom("86");
    		tLCExtendSchema.setOperator(cOperator);
    		tLCExtendSchema.setMakeDate(PubFun.getCurrentDate());
    		tLCExtendSchema.setMakeTime(PubFun.getCurrentTime());
    		tLCExtendSchema.setModifyDate(PubFun.getCurrentDate());
    		tLCExtendSchema.setModifyTime(PubFun.getCurrentTime());
    		
    		tLCExtendSet.add(tLCExtendSchema);
        }
		return tLCExtendSet;
	}
	
	/**
	 * 获取原有数据
	 * 
	 * @param cContNode
	 * @param cPrtno
	 * @param cOperator
	 * @return
	 */
	public LCExtendSet getSet(String cPrtno) {
		LCExtendDB tLCExtendDB = new LCExtendDB();
		
		LCExtendSet tLCExtendSet = tLCExtendDB.executeQuery("select * from lcextend where prtno='" + cPrtno + "'");
		
		return tLCExtendSet;
	}

	/**
	 * 校验节点
	 * 
	 * @param tXObject
	 * @return
	 */
	private String tranNode(XObject tXObject) {
		
		String tFieldValue = (tXObject == null ? "" : tXObject.toString());
		if (tFieldValue.startsWith("\n ") || tFieldValue.startsWith("\n\r ")) {
			tFieldValue = ""; // 外包给的空值xml节点以回车+空格开始的字符串，特做此调整
		}
		return tFieldValue;
	}
	
	private boolean isNull(String value) {
		if(value == null || "".equals(value) || "null".equals(value)){
			return true;
		}
		return false;
	}
}
