package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOContImportUI
{
    public CErrors mErrors = new CErrors();

    public BPOContImportUI()
    {
    }

    public boolean submitData(VData data, String operator)
    {
        BPOContImportBL tBPOContImportBL = new BPOContImportBL();
        if(!tBPOContImportBL.submitData(data, operator))
        {
            mErrors.copyAllErrors(tBPOContImportBL.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        BPOContImportUI ui = new BPOContImportUI();
    }
}
