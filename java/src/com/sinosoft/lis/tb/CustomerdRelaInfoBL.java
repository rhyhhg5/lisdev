package com.sinosoft.lis.tb;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author yuanaq
 * @version 1.0
 */

public class CustomerdRelaInfoBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private TransferData mTransferData = new TransferData();
    private VData mResult = new VData();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LCContCustomerRelaInfoSet tLCContCustomerRelaInfoSet = new
            LCContCustomerRelaInfoSet();
    private LCCustomerRelaInfoSet tLCCustomerRelaInfoSet = new
            LCCustomerRelaInfoSet();
    private LCContCustomerRelaInfoSet tsecLCContCustomerRelaInfoSet = new
            LCContCustomerRelaInfoSet();
    private LCCustomerRelaInfoSet tsecLCCustomerRelaInfoSet = new
            LCCustomerRelaInfoSet();

    private MMap map = new MMap();
    private String pivotalman = "";
    private MMap map2 = new MMap();
    public CustomerdRelaInfoBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        //得到输入数据
        if (!getInputData())
        {
            return false;
        }
        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理1---基本家庭关系确立
        if (!dealDataone(cOperate))
        {
            return false;
        }
        //进行业务处理2---二级家庭关系确立
        if (!dealDatatwo(cOperate))
        {
            return false;
        }

        mInputData = null;
        return true;
    }


    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData()
    {
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果输入数据有错，则返回false,否则返回true
    private boolean checkInputData()
    {
        return true;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealDataone(String cOperate)
    {
        boolean tReturn = false;
        //处理客户关系信息表
        String mContNo = (String) mTransferData.getValueByName("ContNo");
        LCAppntSet tLCAppntSet = new LCAppntSet();
        LCAppntDB tLCAppntDB = new LCAppntDB();
        LCInsuredSchema tLCInsuredSchema = null ;
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCInsuredSet ttLCInsuredSet = new LCInsuredSet();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        String sqlstr1 = "select *from Lcappnt where ContNo='" + mContNo + "'";
        String sqlstr2 = "select *from Lcinsured where ContNo='" + mContNo +"'";
                        // "' and RelationToMainInsured<>'00'";
        String sqlstr3 = "select *from Lcinsured where ContNo='" + mContNo +
                         "' and RelationToMainInsured='00'";
        tLCInsuredSet = tLCInsuredDB.executeQuery(sqlstr3);
        tLCInsuredSchema = tLCInsuredSet.get(1); //主被保险人信息
        if (tLCInsuredSchema != null)
        {
            pivotalman = tLCInsuredSchema.getInsuredNo();
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "CustomerdRelaInfoBL";
            tError.functionName = "dealDataone";
            tError.errorMessage = "不存在与主被保险人关系是本人被保险人！";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLCAppntSet = tLCAppntDB.executeQuery(sqlstr1);
        if (tLCAppntSet.size() > 0&&!tLCInsuredSchema.getRelationToAppnt().equals("00"))
        {
            LCAppntSchema tLCAppntSchema = new LCAppntSchema();
            tLCAppntSchema = tLCAppntSet.get(1);
            LCContCustomerRelaInfoSchema tLCContCustomerRelaInfoSchema = new
                    LCContCustomerRelaInfoSchema();
            tLCContCustomerRelaInfoSchema.setContNo(tLCAppntSchema.getContNo());
            tLCContCustomerRelaInfoSchema.setRelaCustomerNo(tLCInsuredSchema.
                    getInsuredNo());
            tLCContCustomerRelaInfoSchema.setRelaCustomerKind("0");
            tLCContCustomerRelaInfoSchema.setCustomerNo(tLCAppntSchema.
                    getAppntNo());
            tLCContCustomerRelaInfoSchema.setCustomerKind("A");
            tLCContCustomerRelaInfoSchema.setRelationToRelaCustomer(getrelation(
                    tLCInsuredSchema.getRelationToAppnt(),
                    tLCAppntSchema.getAppntSex()));
            if (tLCAppntSchema.getMarriage() != null)
            {
                if (tLCAppntSchema.getMarriage() == null || tLCAppntSchema.getMarriage().equals("0")) {
                    tLCContCustomerRelaInfoSchema.setMarriage("N");
                } else {
                    tLCContCustomerRelaInfoSchema.setMarriage("Y");
                }
            }
            else
            {
                tLCContCustomerRelaInfoSchema.setMarriage("N");
            }
            tLCContCustomerRelaInfoSchema.setSex(tLCAppntSchema.getAppntSex());
            tLCContCustomerRelaInfoSchema.setAge(PubFun.calInterval(
                    tLCAppntSchema.
                    getAppntBirthday(), CurrentDate, "Y"));
            if (tLCInsuredSchema.getMarriage() != null){
                if (tLCInsuredSchema.getMarriage().equals("0")) {
                    tLCContCustomerRelaInfoSchema.setRelaMarriage("N");
                } else {
                    tLCContCustomerRelaInfoSchema.setRelaMarriage("Y");
                }
            }
            else
            {
                tLCContCustomerRelaInfoSchema.setRelaMarriage("N");
            }
            tLCContCustomerRelaInfoSchema.setRelaSex(tLCInsuredSchema.getSex());
            tLCContCustomerRelaInfoSchema.setRelaAge(PubFun.calInterval(
                    tLCInsuredSchema.getBirthday(), CurrentDate, "Y"));
            tLCContCustomerRelaInfoSchema.setMakeDate(CurrentDate);
            tLCContCustomerRelaInfoSchema.setMakeTime(CurrentTime);
            tLCContCustomerRelaInfoSchema.setModifyDate(CurrentDate);
            tLCContCustomerRelaInfoSchema.setModifyTime(CurrentTime);
            tLCContCustomerRelaInfoSet.add(tLCContCustomerRelaInfoSchema);
        }
        ttLCInsuredSet = tLCInsuredDB.executeQuery(sqlstr2);
        for (int x = 1; x <= ttLCInsuredSet.size(); x++)
        {
            LCInsuredSchema ttLCInsuredSchema = new LCInsuredSchema();
            ttLCInsuredSchema = ttLCInsuredSet.get(x);
            LCContCustomerRelaInfoSchema ttLCContCustomerRelaInfoSchema = new
                    LCContCustomerRelaInfoSchema();
            ttLCContCustomerRelaInfoSchema.setContNo(ttLCInsuredSchema.
                    getContNo());
            ttLCContCustomerRelaInfoSchema.setRelaCustomerNo(tLCInsuredSchema.
                    getInsuredNo());
            ttLCContCustomerRelaInfoSchema.setRelaCustomerKind("0");
            ttLCContCustomerRelaInfoSchema.setCustomerNo(ttLCInsuredSchema.
                    getInsuredNo());
            ttLCContCustomerRelaInfoSchema.setCustomerKind("I");
            ttLCContCustomerRelaInfoSchema.setRelationToRelaCustomer(
                    ttLCInsuredSchema.getRelationToMainInsured());
            if(ttLCInsuredSchema.getMarriage()!=null){
                if (ttLCInsuredSchema.getMarriage().equals("0")) {
                    ttLCContCustomerRelaInfoSchema.setMarriage("N");
                } else {
                    ttLCContCustomerRelaInfoSchema.setMarriage("Y");
                }
            }
            else
            {
                ttLCContCustomerRelaInfoSchema.setMarriage("N");
            }
            ttLCContCustomerRelaInfoSchema.setSex(ttLCInsuredSchema.getSex());
            ttLCContCustomerRelaInfoSchema.setAge(PubFun.calInterval(
                    ttLCInsuredSchema.
                    getBirthday(), CurrentDate, "Y"));
            if (tLCInsuredSchema.getMarriage() != null){
                if (tLCInsuredSchema.getMarriage().equals("0")) {
                    ttLCContCustomerRelaInfoSchema.setRelaMarriage("N");
                } else {
                    ttLCContCustomerRelaInfoSchema.setRelaMarriage("Y");
                }
            }
            else
            {
                ttLCContCustomerRelaInfoSchema.setRelaMarriage("N");
            }
            ttLCContCustomerRelaInfoSchema.setRelaSex(tLCInsuredSchema.getSex());
            ttLCContCustomerRelaInfoSchema.setRelaAge(PubFun.calInterval(
                    tLCInsuredSchema.getBirthday(), CurrentDate, "Y"));
            ttLCContCustomerRelaInfoSchema.setMakeDate(CurrentDate);
            ttLCContCustomerRelaInfoSchema.setMakeTime(CurrentTime);
            ttLCContCustomerRelaInfoSchema.setModifyDate(CurrentDate);
            ttLCContCustomerRelaInfoSchema.setModifyTime(CurrentTime);
            tLCContCustomerRelaInfoSet.add(ttLCContCustomerRelaInfoSchema);

        }
        //开始根据客户关系信息表处理客户关系表的数据
        for (int y = 1; y <= tLCContCustomerRelaInfoSet.size(); y++)
        {
            LCContCustomerRelaInfoSchema mLCContCustomerRelaInfoSchema = new
                    LCContCustomerRelaInfoSchema();
            mLCContCustomerRelaInfoSchema = tLCContCustomerRelaInfoSet.get(y);
            if (!getcustomerelation(mLCContCustomerRelaInfoSchema))
            {
                return false;
            }
        }

        //解决家庭单信息冗余问题，将保单修改过被保险人保存在LCContCustomerRelaInfo表中的信息全部删除
        //然后再添加新的信息。                               by zhangyang 20110429
        String strSQL = "delete from LCContCustomerRelaInfo where contno = '" + mContNo + "' ";
        map.put(strSQL,"DELETE");
        
        map.put(tLCContCustomerRelaInfoSet, "DELETE&INSERT");
        map.put(tLCCustomerRelaInfoSet, "DELETE&INSERT");
        //准备往后台的数据
        if (!prepareOutputData(map))
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "CustomerdRelaInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tReturn = true;
        return tReturn;
    }
    //处理二级家庭关系
    private boolean dealDatatwo(String cOperate)
    {
        boolean tReturn = false;
        String strsql="select *from LCCustomerRelaInfo where RelaCustomerNo='"+pivotalman+"'";
        LCCustomerRelaInfoDB secLCCustomerdRelaInfoDB=new LCCustomerRelaInfoDB();
        LCCustomerRelaInfoSet secLCCustomerRelaInfoSet=new LCCustomerRelaInfoSet();
        secLCCustomerRelaInfoSet=secLCCustomerdRelaInfoDB.executeQuery(strsql);
               //处理二级关系,通过
               for (int z = 1; z <= secLCCustomerRelaInfoSet.size(); z++) {
                   LCCustomerRelaInfoSchema secLCCustomerRelaInfoSchema = new
                           LCCustomerRelaInfoSchema();
                   secLCCustomerRelaInfoSchema = secLCCustomerRelaInfoSet.get(z);
                   if (secLCCustomerRelaInfoSchema.getRelation().equals(
                           "19")) {
                       if (secLCCustomerRelaInfoSet.size() > 0)
                           for (int z1 = 1; z1 <= secLCCustomerRelaInfoSet.size(); z1++) {
                               LCCustomerRelaInfoSchema m1LCCustomerRelaInfoSchema = new
                                       LCCustomerRelaInfoSchema();
                               m1LCCustomerRelaInfoSchema = secLCCustomerRelaInfoSet.
                                       get(z1);
                               if (m1LCCustomerRelaInfoSchema.getRelation().
                                   equals("04")) {
                                   if (!getcustomerelationnew(
                                           secLCCustomerRelaInfoSchema,
                                           m1LCCustomerRelaInfoSchema, "04")) {
                                       return false;
                                   }
                               }
                               if (m1LCCustomerRelaInfoSchema.getRelation().
                                   equals("05")) {
                                   if (!getcustomerelationnew(
                                           secLCCustomerRelaInfoSchema,
                                           m1LCCustomerRelaInfoSchema, "11")) {
                                       return false;
                                   }
                               }

                           }
                   }
                    if (secLCCustomerRelaInfoSchema.getRelation().equals(
                            "20"))
                    {
                        for (int z1 = 1; z1 <= secLCCustomerRelaInfoSet.size(); z1++)
                        {
                            LCCustomerRelaInfoSchema m1LCCustomerRelaInfoSchema = new
                                    LCCustomerRelaInfoSchema();
                            m1LCCustomerRelaInfoSchema = secLCCustomerRelaInfoSet.
                                    get(z1);
                            if (m1LCCustomerRelaInfoSchema.getRelation().
                                equals("04"))
                            {
                                if (!getcustomerelationnew(
                                        secLCCustomerRelaInfoSchema,
                                        m1LCCustomerRelaInfoSchema, "05"))
                                {
                                    return false;
                                }
                            }
                            if (m1LCCustomerRelaInfoSchema.getRelation().
                                equals("05"))
                            {
                                if (!getcustomerelationnew(
                                        secLCCustomerRelaInfoSchema,
                                        m1LCCustomerRelaInfoSchema, "11"))
                                {
                                    return false;
                                }
                            }

                        }
                    }
                    if (secLCCustomerRelaInfoSchema.getRelation().equals(
                            "01"))
                    {
                        for (int z1 = 1; z1 <= secLCCustomerRelaInfoSet.size(); z1++)
                        {
                            LCCustomerRelaInfoSchema m1LCCustomerRelaInfoSchema = new
                                    LCCustomerRelaInfoSchema();
                            m1LCCustomerRelaInfoSchema = secLCCustomerRelaInfoSet.
                                    get(z1);
                            if (m1LCCustomerRelaInfoSchema.getRelation().
                                equals("04") ||
                                m1LCCustomerRelaInfoSchema.getRelation().
                                equals("05"))
                            {
                                if (!getcustomerelationnew(
                                        secLCCustomerRelaInfoSchema,
                                        m1LCCustomerRelaInfoSchema, "11"))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    if (secLCCustomerRelaInfoSchema.getRelation().equals(
                            "02"))
                    {
                        for (int z1 = 1; z1 <= secLCCustomerRelaInfoSet.size(); z1++)
                        {
                            LCCustomerRelaInfoSchema m1LCCustomerRelaInfoSchema = new
                                    LCCustomerRelaInfoSchema();
                            m1LCCustomerRelaInfoSchema = secLCCustomerRelaInfoSet.
                                    get(z1);
                            if (m1LCCustomerRelaInfoSchema.getRelation().
                                equals("04") ||
                                m1LCCustomerRelaInfoSchema.getRelation().
                                equals("05"))
                            {
                                if (!getcustomerelationnew(
                                        secLCCustomerRelaInfoSchema,
                                        m1LCCustomerRelaInfoSchema, "08"))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    if (secLCCustomerRelaInfoSchema.getRelation().equals(
                            "04"))
                    {
                        for (int z1 = 1; z1 <= secLCCustomerRelaInfoSet.size(); z1++)
                        {
                            LCCustomerRelaInfoSchema m1LCCustomerRelaInfoSchema = new
                                    LCCustomerRelaInfoSchema();
                            m1LCCustomerRelaInfoSchema = secLCCustomerRelaInfoSet.
                                    get(z1);
                            if (m1LCCustomerRelaInfoSchema.getRelation().
                                equals("12"))
                            {
                                if (!getcustomerelationnew(
                                        secLCCustomerRelaInfoSchema,
                                        m1LCCustomerRelaInfoSchema, "04"))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    if (secLCCustomerRelaInfoSchema.getRelation().equals(
                            "05"))
                    {
                        for (int z1 = 1; z1 <= secLCCustomerRelaInfoSet.size(); z1++)
                        {
                            LCCustomerRelaInfoSchema m1LCCustomerRelaInfoSchema = new
                                    LCCustomerRelaInfoSchema();
                            m1LCCustomerRelaInfoSchema = secLCCustomerRelaInfoSet.
                                    get(z1);
                            if (m1LCCustomerRelaInfoSchema.getRelation().
                                equals("12"))
                            {
                                if (!getcustomerelationnew(
                                        secLCCustomerRelaInfoSchema,
                                        m1LCCustomerRelaInfoSchema, "05"))
                                {
                                    return false;
                                }
                            }
                        }
                    }

                }
        map2.put(tsecLCCustomerRelaInfoSet, "DELETE&INSERT");
        if (!prepareOutputData(map2))
        {
            return false;
        }

        PubSubmit ttPubSubmit = new PubSubmit();
        if (!ttPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(ttPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "CustomerdRelaInfoBL";
            tError.functionName = "dealDatatwo";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tReturn = true;
        return tReturn;

    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData(MMap map)
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPersonBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    //两个客户之间的关系
    private String getrelation(String SequenceRelation, String sex)
    {
        String AgainstRelation = "";
        AgainstRelation = getrelation1(SequenceRelation);
        if (SequenceRelation.equals("01"))
        {
            AgainstRelation = "02";
        }
        if (SequenceRelation.equals("02"))
        {
            AgainstRelation = "01";
        }
        if (SequenceRelation.equals("03") && sex.equals("0"))
        {
            AgainstRelation = "04";
        }
        if (SequenceRelation.equals("03") && sex.equals("1"))
        {
            AgainstRelation = "05";
        }
        if (SequenceRelation.equals("04") || SequenceRelation.equals("05"))
        {
            AgainstRelation = "03";
        }
        if (SequenceRelation.equals("06") || SequenceRelation.equals("07"))
        {
            AgainstRelation = "08";
        }
        if (SequenceRelation.equals("08") && sex.equals("0"))
        {
            AgainstRelation = "06";
        }
        if (SequenceRelation.equals("08") && sex.equals("1"))
        {
            AgainstRelation = "07";
        }
        if (SequenceRelation.equals("09") || SequenceRelation.equals("10"))
        {
            AgainstRelation = "11";
        }
        if (SequenceRelation.equals("11") && sex.equals("0"))
        {
            AgainstRelation = "09";
        }
        if (SequenceRelation.equals("11") && sex.equals("1"))
        {
            AgainstRelation = "10";
        }
        if (SequenceRelation.equals("14") || SequenceRelation.equals("15") ||
            SequenceRelation.equals("16") || SequenceRelation.equals("17"))
        {
            AgainstRelation = "18";
        }
        if (SequenceRelation.equals("19") || SequenceRelation.equals("20"))
        {
            AgainstRelation = "21";
        }
        if (SequenceRelation.equals("21") && sex.equals("0"))
        {
            AgainstRelation = "19";
        }
        if (SequenceRelation.equals("21") && sex.equals("1"))
        {
            AgainstRelation = "20";
        }
        if (SequenceRelation.equals("22") || SequenceRelation.equals("23"))
        {
            AgainstRelation = "24";
        }
        if (SequenceRelation.equals("24") && sex.equals("0"))
        {
            AgainstRelation = "22";
        }
        if (SequenceRelation.equals("24") && sex.equals("1"))
        {
            AgainstRelation = "23";
        }

        if (SequenceRelation.equals("28"))
        {
            AgainstRelation = "29";
        }
        if (SequenceRelation.equals("29"))
        {
            AgainstRelation = "28";
        }

        return AgainstRelation;
    }


    //两个客户之间的关系为同类关系时
    private String getrelation1(String SequenceRelation)
    {
        String AgainstRelation = "";
        if (SequenceRelation.equals("00")||SequenceRelation.equals("12") || SequenceRelation.equals("13") ||
            SequenceRelation.equals("25") || SequenceRelation.equals("26") ||
            SequenceRelation.equals("27"))
        {
            AgainstRelation = SequenceRelation;
        }
        return AgainstRelation;
    }
   //客户关系确立
    private boolean getcustomerelation(LCContCustomerRelaInfoSchema
                                       mLCContCustomerRelaInfoSchema)
    {
        LCCustomerRelaInfoSchema toneLCCustomerRelaInfoSchema = new
                LCCustomerRelaInfoSchema();
        LCCustomerRelaInfoSchema ttwoLCCustomerRelaInfoSchema = new
                LCCustomerRelaInfoSchema();
        toneLCCustomerRelaInfoSchema.setCustomerNo(
                mLCContCustomerRelaInfoSchema.getCustomerNo());
        toneLCCustomerRelaInfoSchema.setRelaCustomerNo(
                mLCContCustomerRelaInfoSchema.getRelaCustomerNo());
        toneLCCustomerRelaInfoSchema.setRelation(
                mLCContCustomerRelaInfoSchema.getRelationToRelaCustomer());
        toneLCCustomerRelaInfoSchema.setRemark("");
        toneLCCustomerRelaInfoSchema.setMakeDate(CurrentDate);
        toneLCCustomerRelaInfoSchema.setMakeTime(CurrentTime);
        toneLCCustomerRelaInfoSchema.setModifyDate(CurrentDate);
        toneLCCustomerRelaInfoSchema.setModifyTime(CurrentTime);
        if(!"00".equals(mLCContCustomerRelaInfoSchema.getRelationToRelaCustomer()))
        {
            ttwoLCCustomerRelaInfoSchema.setCustomerNo(
                    mLCContCustomerRelaInfoSchema.getRelaCustomerNo());
            ttwoLCCustomerRelaInfoSchema.setRelaCustomerNo(
                    mLCContCustomerRelaInfoSchema.getCustomerNo());
            ttwoLCCustomerRelaInfoSchema.setRelation(getrelation(
                    mLCContCustomerRelaInfoSchema.getRelationToRelaCustomer(),
                    mLCContCustomerRelaInfoSchema.getRelaSex()));
            ttwoLCCustomerRelaInfoSchema.setRemark("");
            ttwoLCCustomerRelaInfoSchema.setMakeDate(CurrentDate);
            ttwoLCCustomerRelaInfoSchema.setMakeTime(CurrentTime);
            ttwoLCCustomerRelaInfoSchema.setModifyDate(CurrentDate);
            ttwoLCCustomerRelaInfoSchema.setModifyTime(CurrentTime);
            tLCCustomerRelaInfoSet.add(ttwoLCCustomerRelaInfoSchema);
        }
        tLCCustomerRelaInfoSet.add(toneLCCustomerRelaInfoSchema);
        return true;

    }
    //二级客户关系确立,现在只在客户关系表中添加，是否应该在客户关系信息表中同样添加呢？
    private boolean getcustomerelationnew(LCCustomerRelaInfoSchema
                                          m1LCCustomerRelaInfoSchema
                                          ,
                                          LCCustomerRelaInfoSchema
                                          m2LCCustomerRelaInfoSchema
                                          , String relation)
    {
        LCCustomerRelaInfoSchema toneLCCustomerRelaInfoSchema = new
                LCCustomerRelaInfoSchema();
        LCCustomerRelaInfoSchema ttwoLCCustomerRelaInfoSchema = new
                LCCustomerRelaInfoSchema();
        toneLCCustomerRelaInfoSchema.setCustomerNo(
                m1LCCustomerRelaInfoSchema.getCustomerNo());
        toneLCCustomerRelaInfoSchema.setRelaCustomerNo(
                m2LCCustomerRelaInfoSchema.getCustomerNo());
        toneLCCustomerRelaInfoSchema.setRelation(
                relation);
        toneLCCustomerRelaInfoSchema.setRemark("");
        toneLCCustomerRelaInfoSchema.setMakeDate(CurrentDate);
        toneLCCustomerRelaInfoSchema.setMakeTime(CurrentTime);
        toneLCCustomerRelaInfoSchema.setModifyDate(CurrentDate);
        toneLCCustomerRelaInfoSchema.setModifyTime(CurrentTime);
        ttwoLCCustomerRelaInfoSchema.setCustomerNo(
                m2LCCustomerRelaInfoSchema.getCustomerNo());
        ttwoLCCustomerRelaInfoSchema.setRelaCustomerNo(
                m1LCCustomerRelaInfoSchema.getCustomerNo());
        ttwoLCCustomerRelaInfoSchema.setRelation(getrelation(
                relation,
                getsex(m2LCCustomerRelaInfoSchema)));
        ttwoLCCustomerRelaInfoSchema.setRemark("");
        ttwoLCCustomerRelaInfoSchema.setMakeDate(CurrentDate);
        ttwoLCCustomerRelaInfoSchema.setMakeTime(CurrentTime);
        ttwoLCCustomerRelaInfoSchema.setModifyDate(CurrentDate);
        ttwoLCCustomerRelaInfoSchema.setModifyTime(CurrentTime);
        tsecLCCustomerRelaInfoSet.add(toneLCCustomerRelaInfoSchema);
        tsecLCCustomerRelaInfoSet.add(ttwoLCCustomerRelaInfoSchema);
        return true;

    }
    public String getsex(LCCustomerRelaInfoSchema mLCCustomerRelaInfoSchema)
    {
        String sex="";
        String sql="select sex from ldperson where customerno='"+mLCCustomerRelaInfoSchema.getCustomerNo()+"'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS ssrs=tExeSQL.execSQL(sql);
        sex=ssrs.GetText(1,1);
        return sex;
    }

    //得到结果
    public VData getResult()
    {
        return this.mResult;
    }


}
