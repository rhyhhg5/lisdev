package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class ContLogicVerifyUI {
        /** 传入数据的容器 */
     private VData mInputData = new VData();

     /** 往前面传输数据的容器 */
     private VData mResult = new VData();

     /** 数据操作字符串 */
     private String mOperate;

     /** 错误处理类 */
     public CErrors mErrors = new CErrors();

     public ContLogicVerifyUI() {
     }
    /**
     * 向BL传递的接口
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        ContLogicVerifyBL tContLogicVerifyBL = new ContLogicVerifyBL();
        if (!tContLogicVerifyBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tContLogicVerifyBL.mErrors);
            return false;
        }
        return true;
    }
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
    ContLogicVerifyBL tContLogicVerifyBL = new ContLogicVerifyBL();
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.ComCode ="86";
    tGlobalInput.Operator="001";
    LCContSchema mLCContSchema = new
            LCContSchema();
    mLCContSchema.setContNo("13000485999");
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ContNo","13000485999");
    VData tVData = new VData();
    tVData.add(mLCContSchema);
    tVData.add(tGlobalInput);
    tVData.add(tTransferData);
    tContLogicVerifyBL.submitData(tVData, "");
}

}
