package com.sinosoft.lis.tb;

import java.rmi.RemoteException;
import java.util.Vector;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import com.sinosoft.lis.certify.SysOperatorNoticeBL;
import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QySendMessgeBTBL
{

    private GlobalInput mG = new GlobalInput();

    private TransferData mTransferData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private ExeSQL mExeSQL = new ExeSQL();

    private String mContTypeFlag = "";

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    //public String prtno;

    public QySendMessgeBTBL()
    {
    }

    private boolean getInputData(VData cInputData)
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            System.out.println("所需参数不完整");
            return false;
        }
        mContTypeFlag = mTransferData.getValueByName("ContTypeFlag").toString();
        System.out.println("-----------------ContTypeFlag = " + mContTypeFlag);
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData))
        {
            mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            if (mG.Operator == null || mG.Operator.equals("") || mG.Operator.equals("null"))
            {
                mG.Operator = "001";
            }
            if (mG.ManageCom == null || mG.ManageCom.equals("") || mG.ManageCom.equals("null"))
            {
                mG.ManageCom = "86";
            }
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("QYMSG"))
        {
            sendMsg();
        }
        else
        {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg()
    {

        System.out.println("签单信息短信通知批处理开始......");
        SmsServiceSoapBindingStub binding = null;
        try
        {
            binding = (SmsServiceSoapBindingStub) new SmsServiceServiceLocator().getSmsService(); //创建binding对象
        }
        catch (javax.xml.rpc.ServiceException jre)
        {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。

        Vector vec = new Vector();

        vec = getMessage();
        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec=" + vec.size());

        if (!vec.isEmpty())
        {
            for (int i = 0; i < vec.size(); i++)
            {
                tempVec.clear();
                tempVec.add(vec.get(i));

                SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
                msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                msgs.setServiceType("xuqi"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

                msgs.setMessages((SmsMessage[]) tempVec.toArray(new SmsMessage[tempVec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
                try
                {
                    value = binding.sendSMS("Admin", "Admin", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                    System.out.println(value.getStatus());
                    System.out.println(value.getMessage());
                }
                catch (RemoteException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        else
        {
            System.out.print("签单无符合条件的短信！");
        }
        System.out.println("签单信息短信通知批处理正常结束......");
        return true;
    }

    private Vector getMessage()
    {

        Vector tVector = new Vector();
        /* 
         * 第一部分:非银保通渠道(标准个单)
         * 标准个单的标志：lccont表中conttype = 1 且 salechnl = 01 。
         */
        if (mContTypeFlag.equals("1"))
        {
            String tSQLsucc = "select distinct lc.prtno, "
                    + "case lc.appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end, "
                    + "lc.appntname, "
                    + "lc.Cvalidate, "
                    + "la.Mobile, "
                    + "lc.ManageCom, "
                    + " lc.PayIntv "
                    + "from lccont lc, lcappnt lca, lcaddress la "
                    + "where 1 = 1 "
                    + "and lc.ContNo = lca.ContNo "
                    + "and la.CustomerNo = lca.appntno "
                    + "and la.AddressNo = lca.AddressNo "
                    + "and lc.appflag = '1' "
                    + "and lc.stateflag = '1'"
                    + "and lc.conttype = '1' "
                    + "and lc.salechnl in ('01','10') "
                    + "and lc.cardflag not in ('9','c','d') " //剔除银保通出单保单
//                    + "and lc.signdate = current date - 1 day "
                     + "and lc.signdate = '2017-01-01' "
                    + "and 1 = (case (select count(1) from ldcode1 where codetype = 'signsmsflag' and code= 'S') when 0 then 1 else (select distinct 1 from ldcode1 where codetype = 'signsmsflag' and code= 'S' and code1 = lc.ManageCom) end ) "
                    + "and not exists (select 1 from LCRNewStateLog lrns where (lrns.ContNo = lc.contno or lrns.NewContNo = lc.contno))"
                    + "and not exists (select 1 from lcpol p where p.contno = lc.contno and p.riskcode = '511601' and lc.cardflag = 'b') "
                    ;
            System.out.println(tSQLsucc);
            SSRS tMsgSuccSSRS = mExeSQL.execSQL(tSQLsucc);

            for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++)
            {
                String tPrtNo = tMsgSuccSSRS.GetText(i, 1);
                String tAppntSex = tMsgSuccSSRS.GetText(i, 2);
                String tAppntName = tMsgSuccSSRS.GetText(i, 3);
                String tCValiDate = tMsgSuccSSRS.GetText(i, 4);
                String tAppntMobile = tMsgSuccSSRS.GetText(i, 5);
                String tManageCom = tMsgSuccSSRS.GetText(i, 6);
                String tPayIntv = tMsgSuccSSRS.GetText(i, 7);

                if (tAppntName == null || "".equals(tAppntName) || "null".equals(tAppntName))
                {
                    continue;
                }
                if (tAppntSex == null || "".equals(tAppntSex) || "null".equals(tAppntSex))
                {
                    continue;
                }
                if (tPrtNo == null || "".equals(tPrtNo) || "null".equals(tPrtNo))
                {
                    continue;
                }
                if (tCValiDate == null || "".equals(tCValiDate) || "null".equals(tCValiDate))
                {
                    continue;
                }
                if (tAppntMobile == null || "".equals(tAppntMobile) || "null".equals(tAppntMobile))
                {
                    continue;
                }
                if (tManageCom == null || "".equals(tManageCom) || "null".equals(tManageCom))
                {
                    continue;
                }
                if (tPayIntv == null || "".equals(tPayIntv) || "null".equals(tPayIntv))
                {
                    continue;
                }
                System.out.println("非银保通渠道(标准个单):PrtNo = " + tPrtNo + "，手机号：" + tAppntMobile);

                //短信内容
                String tAgentContents = "";
                SmsMessage tAgentMsg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                if ("8644".equals(tManageCom.substring(0, 4)))
                {//特殊处理广东短信
                    tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！您投保申请" + tPrtNo + "号保单"
                            + ("0".equals(tPayIntv) ? "" : "首期") + "保费已成功缴纳，保单于" + tCValiDate
                            + "生效，保险合同将于近日送达，为了保障您的权益请及时确认并签收合同回执单。"
                            + "自回执签收之日起您享有15天犹豫期，在此期间，我司将对您进行电话回访，请注意接听客服电话95591。祝您健康。";
                } else if ("8637".equals(tManageCom.substring(0, 4))){
                	// 山东短信
                	
                	// 获取生效日期 格式 *月*日
					String dateSql = "Select Distinct Trim(Char(Month(Cc.Cvalidate))) || '月' || "
							+ "Trim(Char(Day(Cc.Cvalidate))) || '日',prem,contno "
							+ "From Lccont Cc "
							+ "Where Cc.Prtno = '"
							+ tPrtNo
							+ "'"
							+ "And Cc.Appflag = '1' "
							+ "And Cc.Stateflag = '1' "
							+ "And Cc.Signdate = '2017-01-01' " 
							+ "With Ur";
					SSRS tSSRS = mExeSQL.execSQL(dateSql);
					if(tSSRS.getMaxRow() == 0){
						continue;
					}
					String cvaliDate = tSSRS.GetText(1, 1);
					String prem = tSSRS.GetText(1, 2);
					String contNo = tSSRS.GetText(1, 3);
					
					if (cvaliDate == null || "".equals(cvaliDate) || prem == null || "".equals(prem) || contNo == null || "".equals(contNo)) {
						continue;
					}
					// 获取产品名称
					String wrapName = getRiskName(tPrtNo);
					if (wrapName == null) {
						continue;
					}
					tAgentMsg = new SmsMessage();

					tAgentContents = "（人保健康）温馨提示：感谢您投保" + wrapName + "产品，" 
							+ "保单已于" + cvaliDate + "生效，保单号为" + contNo + "，"
							+ ("0".equals(tPayIntv) ? "" : "首期") + "保费为" + prem + "元。" 
							+ "为维护您的权益，公司近期将电话回访您，请留意接听。" 
							+ "如有疑问请致电95591。";

					System.out.println(tAgentContents);
					
                }else if("8621".equals(tManageCom.substring(0, 4))){
                	 tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！您投保申请" + tPrtNo + "号保单"
                                     + ("0".equals(tPayIntv) ? "" : "首期") + "保费已成功缴纳，保单于" + tCValiDate
                                     + "生效，保险合同将于近日送达，为了保障您的权益请及时确认并签收合同回执单，"
                                     +"我们将于近期通过010-95591回访您，请您接听回访电话。祝您健康。客服电话：95591。";
                	
                }else if("8632".equals(tManageCom.substring(0, 4))){
               	     tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！您的投保申请" + tPrtNo + ""
                                    + ("0".equals(tPayIntv) ? "" : "首期") + "保费已成功缴纳，保单于" + tCValiDate
                                    + "生效，保险合同将于近日送达，为保障您的权益，请您及时确认，签收合同回执单，"
                                    + "并留意接听我公司95591客服热线对您进行的电话回访。祝您健康。";

                }else if("8614".equals(tManageCom.substring(0, 4))){
                    //获取投保单申请日期 格式 *年*月*日
					String dateSql = "Select Distinct Trim(Char(Year(Cc.Polapplydate))) || '年' || Trim(Char(Month(Cc.Polapplydate))) || '月' || "
							+ "Trim(Char(Day(Cc.Polapplydate))) || '日',prem "
							+ "From Lccont Cc "
							+ "Where Cc.Prtno = '" + tPrtNo + "'"
							+ "And Cc.Appflag = '1'"
							+ "And Cc.Stateflag = '1'"
							+ "And Cc.Signdate = '2017-01-01' With Ur";
					
					String polApplyDate = mExeSQL.execSQL(dateSql).GetText(1, 1);
					String prem=mExeSQL.execSQL(dateSql).GetText(1, 2);
					if(polApplyDate == null || "".equals(polApplyDate)){
						continue;
					}
					if(prem == null || "".equals(prem)){
						continue;
					}
//					获取缴费频次等
	                 String paySql = "select payintv,"
							+ "trim(char(payendyear)) || "
							+ "(select codename from ldcode where codetype='payendyearflag' and code=payendyearflag),"
							+ "trim(char(insuyear)) || "
	                        + "(select codename from ldcode where codetype='insuyearflag' and code=insuyearflag) ,codename('payintv',payintv) "
							+ "from lcpol where prtno='"
							+ tPrtNo
							+ "' and signdate = '2017-01-01' and stateflag = '1' "
							+ " and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M' and riskcode not in ('340501') ) with ur";
	                 SSRS tSSRS =  mExeSQL.execSQL(paySql);
	                 if (tSSRS.getMaxRow() == 0){
	                	 continue;
	                 } 
	                 
	                 String payintv = tSSRS.GetText(1, 1);
	                 String payendyear = tSSRS.GetText(1, 2);
	                 String insuyear = tSSRS.GetText(1, 3);
	                 String payintvname = tSSRS.GetText(1, 4);
	                 String sendPath = "";
	                 String sendPath1 = "";
					if(payintvname == null || "".equals(payintvname)){
						continue;
					}
					if (insuyear.endsWith("岁")) {
						insuyear = "至" + insuyear;
					}
					if (payendyear.endsWith("岁")) {
						payendyear = "至" + payendyear;
					}
					sendPath = "，保险期间"+insuyear+"，";
					
					if ("0".equals(payintv)) {
	                	 sendPath1 = "该产品为趸交产品，已交费"+prem+"元。";
	                 }else{
	                	 sendPath1 = "该产品为期交产品，缴费频次为"+payintvname+"，每次交费"+prem+"元，交费期间为"+payendyear+"。";
	                 }
					String riskType = getRiskType(tPrtNo);
					String wrapName = getRiskName(tPrtNo, riskType);
					if (wrapName == null) {
						continue;
					}
					if ("4".equals(riskType)) {
						tAgentContents = "人保健康温馨提示：保险让生活更美好。感谢您于" + polApplyDate
								+ "投保了" + wrapName + "产品" + sendPath 
								+ "犹豫期为签收保单日起15天内。" + sendPath1
								+ "为维护您的权益，请您仔细阅读保险合同条款，并请重点关注："
								+ "1、费用扣除的项目、比例和金额；"
								+ "2、犹豫期退保，扣除十元工本费，退还全部保费；"
								+ "3、犹豫期后退保可能产生损失；" + "4、最低保证利率之上的收益不确定。"
								+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
						System.out.println(tAgentContents);
					} else if ("2".equals(riskType)) {
						tAgentContents = "人保健康温馨提示：保险让生活更美好。感谢您于"
								+ polApplyDate + "投保了"
								+ wrapName + "产品" + sendPath 
								+ "犹豫期为签收保单日起15天内。"  + sendPath1
								+ "为维护您的权益，请您仔细阅读保险合同条款，并重点关注："
								+ "1、犹豫期退保，扣除十元工本费，退还全部保费；"
								+ "2、犹豫期后退保可能产生损失；" + "3、分红不确定，取决于公司经营状况。"
								+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
						System.out.println(tAgentContents);
					}else{
						tAgentContents= "人保健康温馨提示：保险让生活更美好。感谢您于"
							+ polApplyDate + "投保了"
							+ wrapName + "产品" + sendPath 
							+ "犹豫期为签收保单日起15天内。"  + sendPath1
							+ "为维护您的权益，请您仔细阅读保险合同条款，并重点关注："
							+ "1、犹豫期退保，扣除十元工本费，退还全部保费；"
							+ "2、犹豫期后退保可能产生损失；"
							+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
					   System.out.println(tAgentContents);
					}
            	 
             }else
                {
                    tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！您投保申请" + tPrtNo + "号保单"
                            + ("0".equals(tPayIntv) ? "" : "首期") + "保费已成功缴纳，保单于" + tCValiDate
                            + "生效，保险合同将于近日送达，为了保障您的权益请及时确认并签收合同回执单。祝您健康。客服电话：95591。";
                }
                tAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                //和IT讨论后，归类到二级机构
                tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                //添加SmsMessage对象                   
                tVector.add(tAgentMsg);

                // 对江苏分公司加入特殊
                if ("8632".equals(tManageCom.substring(0, 4)))
                {
                    tAgentMsg = new SmsMessage();
                    tAgentContents = "";

                    tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，"
                            + "您好！为保护您的合法权益，请再次确认您所购买的保单保险是您需要的，并对我公司及销售人员的销售行为进行监督。祝您健康，客服电话：95591。";
                    tAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                    tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                    //添加SmsMessage对象                   
                    tVector.add(tAgentMsg);
//                } else if ("8614".equals(tManageCom.substring(0, 4))) {
//					// 获取保单险种类型
//					String riskType = getRiskType(tPrtNo);
//					// 若为万能险或分红险
//					if ("4".equals(riskType) || "2".equals(riskType)) {
//						// 获取投保单申请日期 格式 *年*月*日
//						String dateSql = "Select Distinct Trim(Char(Year(Cc.Polapplydate))) || '年' || Trim(Char(Month(Cc.Polapplydate))) || '月' || "
//								+ "Trim(Char(Day(Cc.Polapplydate))) || '日'"
//								+ "From Lccont Cc "
//								+ "Where Cc.Prtno = '" + tPrtNo + "'"
//								+ "And Cc.Appflag = '1'"
//								+ "And Cc.Stateflag = '1'"
//								+ "And Cc.Signdate = Current Date - 1 Day With Ur";
//						
//						String polApplyDate = mExeSQL.getOneValue(dateSql);
//						if(polApplyDate == null || "".equals(polApplyDate)){
//							continue;
//						}
//						// 获取产品名称
//						String wrapName = getRiskName(tPrtNo,riskType);
//						if(wrapName == null){
//							continue;
//						}
//						
//						tAgentMsg = new SmsMessage();
//	                    tAgentContents = "";
//
//						if ("4".equals(riskType)) {
//							tAgentContents = "人保健康温馨提示：感谢您于" + polApplyDate
//									+ "投保了" + wrapName + "产品。为维护您的权益，请重点关注："
//									+ "1、费用扣除的项目、比例和金额；"
//									+ "2、犹豫期（签收保单日起15日内）退保，扣除10元工本费，退还全部保费；"
//									+ "3、犹豫期后退保可能产生损失；" + "4、最低保证利率之上的收益不确定。"
//									+ "公司近期将电话回访您，请留意接听。";
//							System.out.println(tAgentContents);
//						} else if ("2".equals(riskType)) {
//							tAgentContents = "人保健康温馨提示：感谢您于"
//									+ polApplyDate + "投保了"
//									+ wrapName + "产品。为维护您的权益，请重点关注："
//									+ "1、犹豫期（签收保单日起15日）内退保，扣除10元工本费，退还全部保费；"
//									+ "2、犹豫期后退保可能产生损失；" + "3、分红不确定，取决于公司经营状况。"
//									+ "公司近期将电话回访您，请留意接听。";
//							System.out.println(tAgentContents);
//						}
//						
//						tAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//	                    tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
//	                    //和IT讨论后，归类到二级机构
//	                    tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
//	                    //添加SmsMessage对象                   
//	                    tVector.add(tAgentMsg);
//	                    
//					} else {
//						continue;
//					}
                } else if("8615".equals(tManageCom.substring(0, 4))){
					// 获取保单险种类型
					String riskType = getRiskType(tPrtNo);
					// 主险万能险
					if ("4".equals(riskType)) {
						// 获取投保单申请日期 格式 *年*月*日
						String dateSql = "Select Distinct Trim(Char(Year(Cc.Polapplydate))) || '年' || Trim(Char(Month(Cc.Polapplydate))) || '月' || "
								+ "Trim(Char(Day(Cc.Polapplydate))) || '日',prem "
								+ "From Lccont Cc "
								+ "Where Cc.Prtno = '"
								+ tPrtNo
								+ "'"
								+ "And Cc.Appflag = '1'"
								+ "And Cc.Stateflag = '1'"
								+ "And Cc.Signdate = '2017-01-01' With Ur";
						SSRS tSSRS = mExeSQL.execSQL(dateSql);
						if(tSSRS.getMaxRow() == 0){
							continue;
						}
						String polApplyDate = tSSRS.GetText(1, 1);
						String prem = tSSRS.GetText(1, 2);
						if (polApplyDate == null || "".equals(polApplyDate) || prem == null || "".equals(prem)) {
							continue;
						}
						// 获取产品名称
						String wrapName = getRiskName(tPrtNo, riskType);
						if (wrapName == null) {
							continue;
						}
						tAgentMsg = new SmsMessage();

						String paySql = "select payintv,"
								+ "trim(char(payendyear)) || "
								+ "(select codename from ldcode where codetype='payendyearflag' and code=payendyearflag),"
								// 对福泽一生产品进行特殊处理 修改缴费年期为5年，保单上显示为75岁，但业务反馈只需要交5年 
								+ "(select count(1) from lcpol where prtno='" + tPrtNo + "' and riskcode='332701') "
								+ "from lcpol where prtno='"
								+ tPrtNo
								+ "' and signdate = '2017-01-01' and stateflag = '1' and riskcode not in ('340501') fetch first 1 rows only with ur";
						tSSRS = mExeSQL.execSQL(paySql);

						if (tSSRS.getMaxRow() == 0) {
							continue;
						}

						String payintv = tSSRS.GetText(1, 1);
						String payendyear = tSSRS.GetText(1, 2);
						
						String sendPath = "";

						if (payendyear == null || "".equals(payendyear)) {
							continue;
						} else if (payendyear.endsWith("岁")) {
							payendyear = "至" + payendyear;
						}

						// 对福泽一生产品进行特殊处理 修改缴费年期为5年，保单上显示为75岁，但业务反馈只需要交5年
						if(!"0".equals(tSSRS.GetText(1, 3))){
							payendyear = "5年";
						}
						
						if ("0".equals(payintv)) {
							// 趸缴
							sendPath = "保费为一次性缴清" + prem + "元";
						} else if ("1".equals(payintv)) {
							// 月缴
							sendPath = "每月缴费" + prem + "元，可缴费" + payendyear;
						} else if ("12".equals(payintv)) {
							// 年缴
							sendPath = "每年缴费" + prem + "元，可缴费" + payendyear;
						} else if ("3".equals(payintv)) {
							// 季缴
							sendPath = "每季缴费" + prem + "元，可缴费" + payendyear;
						} else if ("6".equals(payintv)) {
							// 半年缴
							sendPath = "每半年缴费" + prem + "元，可缴费" + payendyear;
						} else {
							continue;
						}

						tAgentContents = "中国人保健康保险公司温馨提示：您于" + polApplyDate
								+ "投保的" + wrapName + "保险产品，" + sendPath + "，"
								+ "本产品收益具有不确定性；"
								+ "犹豫期（签收保单之日起15天）内退保可退还全部保费，"
								+ "犹豫期后退保可能产生损失。犹豫期内公司将对您电话回访，"
								+ "请留意接听。如有疑问请致电“4006695518”。";

						System.out.println(tAgentContents);
						tAgentMsg.setReceiver(tAppntMobile); // 设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
						tAgentMsg.setContents(tAgentContents); // 设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
						// 和IT讨论后，归类到二级机构
						tAgentMsg.setOrgCode(tManageCom.substring(0, 4)
								+ "0000"); // 设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
						// 添加SmsMessage对象
						tVector.add(tAgentMsg);
					} else {
						continue;
					}
                } else if ("86370600".equals(tManageCom) || "86371000".equals(tManageCom)){
					SmsMessage newAgentMsg = new SmsMessage();
					String newAgentContents = "";

					newAgentContents = "（人保健康）投保风险提示：1、投保有风险，请您认真阅读保险条款。2、谨慎对待签名，不可代签名。3、犹豫期（签收保单日起15日）内可全额退保，犹豫期后退保可能产生损失。";
					newAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
					newAgentMsg.setContents(newAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
					//和IT讨论后，归类到二级机构
					newAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
					//添加SmsMessage对象                   
					tVector.add(newAgentMsg);
				}
                // --------------------
            }
        }

        /* 
         * 第二部分:银保通渠道  
         * 银保通的标志：lccont表中conttype = 1 且 salechnl = 4(或13)。
         */
        //#1833 银保通出单承保短信需求  将银保通出单保单剔除 lccont 中cardflag <> '9'
        if (mContTypeFlag.equals("3"))
        {
            String tSQLsucc = "select distinct lc.prtno, "
                    + "case lc.appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end, "
                    + "lc.appntname, "
                    + "lc.Cvalidate, "
                    + "la.Mobile, "
                    + "lc.ManageCom, "
                    + " lc.PayIntv "
                    + "from lccont lc, lcappnt lca, lcaddress la "
                    + "where 1 = 1 "
                    + "and lc.ContNo = lca.ContNo "
//                    + "and lc.prtno in ('162016123191','162016123192')"
                    + "and la.CustomerNo = lca.appntno "
                    + "and la.AddressNo = lca.AddressNo "
                    + "and lc.appflag = '1' "
                    + "and lc.stateflag = '1' "
                    + "and lc.conttype = '1' "
                    + "and lc.salechnl in ('04','13') "
                    + "and lc.cardflag not in ('9','c','d') " //剔除银保通出单保单
                    + "and 1 = (case (select count(1) from ldcode1 where codetype = 'signsmsflag' and code= 'Y') when 0 then 1 else (select distinct 1 from ldcode1 where codetype = 'signsmsflag' and code= 'Y' and code1 = lc.ManageCom) end ) "
                    + "and lc.signdate = '2017-01-01' "
//                    + "and lc.signdate = '2017-01-03'"
                    + "and not exists (select 1 from LCRNewStateLog lrns where (lrns.ContNo = lc.contno or lrns.NewContNo = lc.contno)) "
                    + "and not exists (select 1 from lcpol p where p.contno = lc.contno and p.riskcode = '511601' and lc.cardflag = 'b') "
                    ;
            System.out.println(tSQLsucc);
            SSRS tMsgSuccSSRS = mExeSQL.execSQL(tSQLsucc);

            for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++)
            {
                String tPrtNo = tMsgSuccSSRS.GetText(i, 1);
                String tAppntSex = tMsgSuccSSRS.GetText(i, 2);
                String tAppntName = tMsgSuccSSRS.GetText(i, 3);
                String tCValiDate = tMsgSuccSSRS.GetText(i, 4);
                String tAppntMobile = tMsgSuccSSRS.GetText(i, 5);
                String tManageCom = tMsgSuccSSRS.GetText(i, 6);
                String tPayIntv = tMsgSuccSSRS.GetText(i, 7);

                if (tAppntName == null || "".equals(tAppntName) || "null".equals(tAppntName))
                {
                    continue;
                }
                if (tAppntSex == null || "".equals(tAppntSex) || "null".equals(tAppntSex))
                {
                    continue;
                }
                if (tPrtNo == null || "".equals(tPrtNo) || "null".equals(tPrtNo))
                {
                    continue;
                }
                if (tCValiDate == null || "".equals(tCValiDate) || "null".equals(tCValiDate))
                {
                    continue;
                }
                if (tAppntMobile == null || "".equals(tAppntMobile) || "null".equals(tAppntMobile))
                {
                    continue;
                }
                if (tManageCom == null || "".equals(tManageCom) || "null".equals(tManageCom))
                {
                    continue;
                }
                if (tPayIntv == null || "".equals(tPayIntv) || "null".equals(tPayIntv))
                {
                    continue;
                }
                System.out.println("银保通渠道:PrtNo = " + tPrtNo);

                
                //短信内容        	
                String tAgentContents = "";
                SmsMessage tAgentMsg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                if("8644".equals(tManageCom.substring(0, 4))){
                	tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！您投保申请的" + tPrtNo + "号保单"
                		+ ("0".equals(tPayIntv) ? "" : "首期")
                	 	+ "保费已成功缴纳，保单于" + tCValiDate + "生效。保险合同将于近日送达，为了保障您的权益请及时确认并签收合同回执。" 
                	 	+ "自回执签收之日起您享有15天的犹豫期，在此期间，我司将对您进行电话回访，请注意接听客服电话95591。祝您健康";
                } else if ("8637".equals(tManageCom.substring(0, 4))){
                	
                	// 获取生效日期 格式 *月*日
					String dateSql = "Select Distinct Trim(Char(Month(Cc.Cvalidate))) || '月' || "
							+ "Trim(Char(Day(Cc.Cvalidate))) || '日',prem,contno "
							+ "From Lccont Cc "
							+ "Where Cc.Prtno = '"
							+ tPrtNo
							+ "'"
							+ "And Cc.Appflag = '1' "
							+ "And Cc.Stateflag = '1' "
							+ "And Cc.Signdate = '2017-01-01' " 
							+ "With Ur";
					SSRS tSSRS = mExeSQL.execSQL(dateSql);
					if(tSSRS.getMaxRow() == 0){
						continue;
					}
					String cvaliDate = tSSRS.GetText(1, 1);
					String prem = tSSRS.GetText(1, 2);
					String contNo = tSSRS.GetText(1, 3);
					
					if (cvaliDate == null || "".equals(cvaliDate) || prem == null || "".equals(prem) || contNo == null || "".equals(contNo)) {
						continue;
					}
					// 获取产品名称
					String wrapName = getWrapName(tPrtNo);
					if(wrapName == null){
						wrapName = getRiskName(tPrtNo);
						if (wrapName == null) {
							continue;
						}
					}
					
					tAgentMsg = new SmsMessage();

					tAgentContents = "（人保健康）温馨提示：感谢您投保" + wrapName + "产品，" 
							+ "保单已于" + cvaliDate + "生效，保单号为" + contNo + "，"
							+ ("0".equals(tPayIntv) ? "" : "首期") + "保费为" + prem + "元。" 
							+ "为维护您的权益，公司近期将电话回访您，请留意接听。" 
							+ "如有疑问请致电95591。";

					System.out.println(tAgentContents);
					
                } else if("8614".equals(tManageCom.substring(0, 4))){
                    //获取投保单申请日期 格式 *年*月*日
					String dateSql = "Select Distinct Trim(Char(Year(Cc.Polapplydate))) || '年' || Trim(Char(Month(Cc.Polapplydate))) || '月' || "
							+ "Trim(Char(Day(Cc.Polapplydate))) || '日',prem "
							+ "From Lccont Cc "
							+ "Where Cc.Prtno = '" + tPrtNo + "'"
							+ "And Cc.Appflag = '1'"
							+ "And Cc.Stateflag = '1'"
							+ "And Cc.Signdate = '2017-01-01' With Ur";
					
					String polApplyDate = mExeSQL.execSQL(dateSql).GetText(1, 1);
					String prem=mExeSQL.execSQL(dateSql).GetText(1, 2);
					if(polApplyDate == null || "".equals(polApplyDate)){
						continue;
					}
					if(prem == null || "".equals(prem)){
						continue;
					}
//					获取缴费频次等
	                 String paySql = "select payintv,"
							+ "trim(char(payendyear)) || "
							+ "(select codename from ldcode where codetype='payendyearflag' and code=payendyearflag),"
							+ "trim(char(insuyear)) || "
	                        + "(select codename from ldcode where codetype='insuyearflag' and code=insuyearflag) ,codename('payintv',payintv) "
							+ "from lcpol where prtno='"
							+ tPrtNo
							+ "' and signdate = '2017-01-01' and stateflag = '1' "
							+ " and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M' and riskcode not in ('340501')) with ur";
	                 SSRS tSSRS =  mExeSQL.execSQL(paySql);
	                 if (tSSRS.getMaxRow() == 0){
	                	 continue;
	                 } 
	                 
	                 String payintv = tSSRS.GetText(1, 1);
	                 String payendyear = tSSRS.GetText(1, 2);
	                 String insuyear = tSSRS.GetText(1, 3);
	                 String payintvname = tSSRS.GetText(1, 4);
	                 String sendPath = "";
	                 String sendPath1 = "";
					if(payintvname == null || "".equals(payintvname)){
						continue;
					}
					if (insuyear.endsWith("岁")) {
						insuyear = "至" + insuyear;
					}
					if (payendyear.endsWith("岁")) {
						payendyear = "至" + payendyear;
					}
					sendPath = "，保险期间"+insuyear+"，";
					
					if ("0".equals(payintv)) {
	                	 sendPath1 = "该产品为趸交产品，已交费"+prem+"元。";
	                 }else{
	                	 sendPath1 = "该产品为期交产品，缴费频次为"+payintvname+"，每次交费"+prem+"元，交费期间为"+payendyear+"。";
	                 }
					String riskType = getRiskType(tPrtNo);
					String wrapName = getRiskName(tPrtNo, riskType);
					if (wrapName == null) {
						continue;
					}
					if ("4".equals(riskType)) {
						tAgentContents = "人保健康温馨提示：保险让生活更美好。感谢您于" + polApplyDate
								+ "投保了" + wrapName + "产品" + sendPath 
								+ "犹豫期为签收保单日起15天内。" + sendPath1
								+ "为维护您的权益，请您仔细阅读保险合同条款，并请重点关注："
								+ "1、费用扣除的项目、比例和金额；"
								+ "2、犹豫期退保，扣除十元工本费，退还全部保费；"
								+ "3、犹豫期后退保可能产生损失；" + "4、最低保证利率之上的收益不确定。"
								+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
						System.out.println(tAgentContents);
					} else if ("2".equals(riskType)) {
						tAgentContents = "人保健康温馨提示：保险让生活更美好。感谢您于"
								+ polApplyDate + "投保了"
								+ wrapName + "产品" + sendPath 
								+ "犹豫期为签收保单日起15天内。"  + sendPath1
								+ "为维护您的权益，请您仔细阅读保险合同条款，并重点关注："
								+ "1、犹豫期退保，扣除十元工本费，退还全部保费；"
								+ "2、犹豫期后退保可能产生损失；" + "3、分红不确定，取决于公司经营状况。"
								+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
						System.out.println(tAgentContents);
					}else{
						tAgentContents= "人保健康温馨提示：保险让生活更美好。感谢您于"
							+ polApplyDate + "投保了"
							+ wrapName + "产品" + sendPath 
							+ "犹豫期为签收保单日起15天内。"  + sendPath1
							+ "为维护您的权益，请您仔细阅读保险合同条款，并重点关注："
							+ "1、犹豫期退保，扣除十元工本费，退还全部保费；"
							+ "2、犹豫期后退保可能产生损失；"
							+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
					   System.out.println(tAgentContents);
					}
            	 
             }else if("8615".equals(tManageCom.substring(0, 4))){
                	// 获取投保单申请日期 格式 *年*月*日
					String dateSql = "Select Distinct Trim(Char(Year(Cc.Polapplydate))) || '年' || Trim(Char(Month(Cc.Polapplydate))) || '月' || "
							+ "Trim(Char(Day(Cc.Polapplydate))) || '日',prem "
							+ "From Lccont Cc "
							+ "Where Cc.Prtno = '"
							+ tPrtNo
							+ "'"
							+ "And Cc.Appflag = '1'"
							+ "And Cc.Stateflag = '1'"
							+ "And Cc.Signdate = '2017-01-01' With Ur";
					SSRS tSSRS = mExeSQL.execSQL(dateSql);
					if(tSSRS.getMaxRow() == 0){
						continue;
					}
					String polApplyDate = tSSRS.GetText(1, 1);
					String prem = tSSRS.GetText(1, 2);
					if (polApplyDate == null || "".equals(polApplyDate) || prem == null || "".equals(prem)) {
						continue;
					}
					// 获取保单险种类型
					String riskType = getRiskType(tPrtNo);
					//2015-1-3 内蒙分公司万能不发送短信
//					if("4".equals(riskType)){
//						continue;
//					}
					// 获取产品名称
					String wrapName = getRiskName(tPrtNo, riskType);
					if (wrapName == null) {
						continue;
					}
					tAgentMsg = new SmsMessage();

					String paySql = "select payintv,"
							+ "trim(char(payendyear)) || "
							+ "(select codename from ldcode where codetype='payendyearflag' and code=payendyearflag),"
							// 对福泽一生产品进行特殊处理 修改缴费年期为5年，保单上显示为75岁，但业务反馈只需要交5年 
							+ "(select count(1) from lcpol where prtno='" + tPrtNo + "' and riskcode='332701') "
							+ "from lcpol where prtno='"
							+ tPrtNo
							+ "' and signdate = '2017-01-01' and stateflag = '1' and riskcode not in ('340501') with ur";
					tSSRS = mExeSQL.execSQL(paySql);

					if (tSSRS.getMaxRow() == 0) {
						continue;
					}

					String payintv = tSSRS.GetText(1, 1);
					String payendyear = tSSRS.GetText(1, 2);
					
					String sendPath = "";

					if (payendyear == null || "".equals(payendyear)) {
						continue;
					} else if (payendyear.endsWith("岁")) {
						payendyear = "至" + payendyear;
					}
					
					// 对福泽一生产品进行特殊处理 修改缴费年期为5年，保单上显示为75岁，但业务反馈只需要交5年
					if(!"0".equals(tSSRS.GetText(1, 3))){
						payendyear = "5年";
					}

					if ("0".equals(payintv)) {
						// 趸缴
						sendPath = "保费为一次性缴清" + prem + "元";
					} else if ("1".equals(payintv)) {
						// 月缴
						sendPath = "每月缴费" + prem + "元，可缴费" + payendyear;
					} else if ("12".equals(payintv)) {
						// 年缴
						sendPath = "每年缴费" + prem + "元，可缴费" + payendyear;
					} else if ("3".equals(payintv)) {
						// 季缴
						sendPath = "每季缴费" + prem + "元，可缴费" + payendyear;
					} else if ("6".equals(payintv)) {
						// 半年缴
						sendPath = "每半年缴费" + prem + "元，可缴费" + payendyear;
					} else {
						continue;
					}

					tAgentContents = "中国人保健康保险公司温馨提示：您于" + polApplyDate
							+ "投保的" + wrapName + "保险产品，" + sendPath + "，"
							+ "本产品收益具有不确定性；"
							+ "犹豫期（签收保单之日起15天）内退保可退还全部保费，"
							+ "犹豫期后退保可能产生损失。犹豫期内公司将对您电话回访，"
							+ "请留意接听。如有疑问请致电“4006695518”。";

					System.out.println(tAgentContents);
                } else if ("8612".equals(tManageCom.substring(0, 4))) {
					System.out.println("银保通渠道:PrtNo = " + tPrtNo);
					// 获取产品名称
					String wrapName = getWrapName(tPrtNo);
					if (wrapName == null) {
						wrapName = getRiskName(tPrtNo);
						if (wrapName == null) {
							continue;
						}
					}
					// 获取缴费频次等
					String paySql = "select payintv,"
							+ "trim(char(payendyear)) || "
							+ "(select codename from ldcode where codetype='payendyearflag' and code=payendyearflag),"
							+ "trim(char(insuyear)) || "
							+ "(select codename from ldcode where codetype='insuyearflag' and code=insuyearflag) "
							+ "from lcpol where prtno='"
							+ tPrtNo
							+ "' and signdate = '2017-01-01' and stateflag = '1' "
							+ " and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M' and riskcode not in ('340501') ) with ur";
					SSRS tSSRS = mExeSQL.execSQL(paySql);
					if (tSSRS.getMaxRow() == 0) {
						continue;
					}

					String payintv = tSSRS.GetText(1, 1);
					String payendyear = tSSRS.GetText(1, 2);
					String insuyear = tSSRS.GetText(1, 3);
					String sendPath = "";
					if (payendyear == null || "".equals(payendyear)) {
						continue;
					} else if (payendyear.endsWith("岁")) {
						payendyear = "至" + payendyear;
					}
					if (insuyear == null || "".equals(insuyear)) {
						continue;
					} else if (insuyear.endsWith("岁")) {
						insuyear = "至" + insuyear;
					}

					if ("0".equals(payintv)) {
						sendPath = "，该产品为趸交产品，保险期间" + insuyear + "，";
					} else {
						sendPath = "，该产品为期交产品，交费期间" + payendyear + "，保险期间"
								+ insuyear + "，";
					}
					tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！"
                    + "感谢您投保中国人民健康保险股份有限公司" + wrapName + sendPath 
                    + "为保障您的权益，请您认真阅读保险合同条款。祝您健康。客服电话：95591。";
					System.out.println(tAgentContents);
				} else if("8621".equals(tManageCom.substring(0, 4))){
                  	 tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！您投保申请" + tPrtNo + "号保单"
                                    + ("0".equals(tPayIntv) ? "" : "首期") + "保费已成功缴纳，保单于" + tCValiDate
                                    + "生效，保险合同将于近日送达，为了保障您的权益请及时确认并签收合同回执单，"
                                    + "我们将于近期通过010-95591回访您，请您接听回访电话。祝您健康。客服电话：95591。";

                } else if("8632".equals(tManageCom.substring(0, 4))){
              	     tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！您的投保申请" + tPrtNo + ""
                                    + ("0".equals(tPayIntv) ? "" : "首期") + "保费已成功缴纳，保单于" + tCValiDate
                                    + "生效，保险合同将于近日送达，为保障您的权益，请您及时确认，签收合同回执单，"
                                    + "并留意接听我公司95591客服热线对您进行的电话回访。祝您健康。";

                } else {
                	tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！您投保申请" + tPrtNo + "号保单"
            			+ ("0".equals(tPayIntv) ? "" : "首期") + "保费已成功缴纳，保单于" + tCValiDate + "生效。祝您健康。客服电话：95591。";
                }
                tAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                //和IT讨论后，归类到二级机构
                tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                //添加SmsMessage对象                   
                tVector.add(tAgentMsg);

                // 对江苏分公司加入特殊
                if ("8632".equals(tManageCom.substring(0, 4)))
                {
                    tAgentMsg = new SmsMessage();
                    tAgentContents = "";

                    tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，"
                            + "您好！为保护您的合法权益，请再次确认您所购买的保单保险是您需要的，并对我公司及销售人员的销售行为进行监督。祝您健康，客服电话：95591。";
                    tAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                    tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                    //添加SmsMessage对象                   
                    tVector.add(tAgentMsg);
                }  else if ("86370600".equals(tManageCom) || "86371000".equals(tManageCom)){
					SmsMessage newAgentMsg = new SmsMessage();
					String newAgentContents = "";

					newAgentContents = "（人保健康）投保风险提示：1、投保有风险，请您认真阅读保险条款。2、谨慎对待签名，不可代签名。3、犹豫期（签收保单日起15日）内可全额退保，犹豫期后退保可能产生损失。";
					newAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
					newAgentMsg.setContents(newAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
					//和IT讨论后，归类到二级机构
					newAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
					//添加SmsMessage对象                   
					tVector.add(newAgentMsg);
				}
                // --------------------
            }
        }

        /* 
         * 第三部分:团单渠道
         * 标准团单的标志：lcgrpcont表中cardflag is null 。
         */
        if (mContTypeFlag.equals("2"))
        {
            String tSQLsucc = "select distinct lgc.prtno, "
                    + "lga.Name, "
                    + "lgc.Cvalidate, "
                    + "ljf.PayDate, "
                    + "lgc.Prem, "
                    + "lgads.Phone1, "
                    + "lgc.ManageCom "
                    + "from lcgrpcont lgc, LCGrpAppnt lga, ljtempfee ljf, lcgrpaddress lgads "
                    + "where 1 = 1 "
                    + "and lga.grpcontno = lgc.grpcontno "
                    + "and lgads.CustomerNo = lga.CustomerNo "
                    + "and lgads.addressno = lga.addressno "
                    + "and ljf.otherno = lgc.grpcontno "
                    + "and lgc.appflag = '1' "
                    + "and lgc.stateflag = '1' "
                    + "and lgc.cardflag is null "
                    + "and 1 = (case (select count(1) from ldcode1 where codetype = 'signsmsflag' and code= 'G') when 0 then 1 else (select distinct 1 from ldcode1 where codetype = 'signsmsflag' and code= 'G' and code1 = lc.ManageCom) end "
                    + "and lgc.signdate = '2017-01-01' "
                    + "and not exists (select 1 from LCRNewStateLog lrns where (lrns.GrpContNo = lgc.grpcontno or lrns.NewGrpContNo = lgc.grpcontno))"
                    + "fetch first 1000 rows only ";
            System.out.println(tSQLsucc);
            SSRS tMsgSuccSSRS = mExeSQL.execSQL(tSQLsucc);

            for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++)
            {
                String tPrtNo = tMsgSuccSSRS.GetText(i, 1);
                String tAppntName = tMsgSuccSSRS.GetText(i, 2);
                String tCvalidate = tMsgSuccSSRS.GetText(i, 3);
                String tPayDate = tMsgSuccSSRS.GetText(i, 4);
                String tPrem = tMsgSuccSSRS.GetText(i, 5);
                String tPhone = tMsgSuccSSRS.GetText(i, 6);
                String tManageCom = tMsgSuccSSRS.GetText(i, 7);

                if (tAppntName == null || "".equals(tAppntName) || "null".equals(tAppntName))
                {
                    continue;
                }
                if (tPrtNo == null || "".equals(tPrtNo) || "null".equals(tPrtNo))
                {
                    continue;
                }
                if (tCvalidate == null || "".equals(tCvalidate) || "null".equals(tCvalidate))
                {
                    continue;
                }
                if (tPayDate == null || "".equals(tPayDate) || "null".equals(tPayDate))
                {
                    continue;
                }
                if (tPrem == null || "".equals(tPrem) || "null".equals(tPrem))
                {
                    continue;
                }
                if (tPhone == null || "".equals(tPhone) || "null".equals(tPhone))
                {
                    continue;
                }
                if (tManageCom == null || "".equals(tManageCom) || "null".equals(tManageCom))
                {
                    continue;
                }
                System.out.println("团单渠道:PrtNo = " + tPrtNo);

                //短信内容        	
                String tAgentContents = "";
                SmsMessage tAgentMsg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                tAgentContents = "尊敬的" + tAppntName + ",您好!" + "您的保单号为" + tPrtNo + "的保单已于" + tCvalidate + "生效,并于"
                        + tPayDate + "成功缴纳了保费,缴费金额为" + tPrem + "元.感谢您的支持,祝您健康。全国客户服务热线：95591或4006695518。";
                tAgentMsg.setReceiver(tPhone); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                //和IT讨论后，归类到二级机构
                tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                //添加SmsMessage对象                   
                tVector.add(tAgentMsg);
            }
        }
        /* 
         * 第四部分:个单渠道 银保通出单保单
         * 个单的标志：lccont表中conttype = 1 且 cardflag = '9'。
         */
        if (mContTypeFlag.equals("4")){
        	String tSQLsucc = "select distinct lc.prtno, "
                + "case lc.appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end, "
                + "lc.appntname, "
                + "lc.Cvalidate, "
                + "la.Mobile, "
                + "lc.ManageCom, "
                + " lc.PayIntv "
                + "from lccont lc, lcappnt lca, lcaddress la "
                + "where 1 = 1 "
                + "and lc.ContNo = lca.ContNo "
//                + "and lc.prtno in  ('552016020203','552016020214') "
                + "and la.CustomerNo = lca.appntno "
                + "and la.AddressNo = lca.AddressNo "
                + "and lc.appflag = '1' "
                + "and lc.stateflag = '1' "
                + "and lc.conttype = '1' "
                + "and lc.cardflag in ('9','c','d') "
                + "and la.Mobile is not null "
                + "and lc.signdate = '2017-01-01' "
                + "and not exists (select 1 from LCRNewStateLog lrns where (lrns.ContNo = lc.contno or lrns.NewContNo = lc.contno)) ";
        	 System.out.println(tSQLsucc);
             SSRS tMsgSuccSSRS = mExeSQL.execSQL(tSQLsucc);
             
             for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++){
            	 String tPrtNo = tMsgSuccSSRS.GetText(i, 1);
                 String tAppntSex = tMsgSuccSSRS.GetText(i, 2);
                 String tAppntName = tMsgSuccSSRS.GetText(i, 3);
                 String tCValiDate = tMsgSuccSSRS.GetText(i, 4);
                 String tAppntMobile = tMsgSuccSSRS.GetText(i, 5);
                 String tManageCom = tMsgSuccSSRS.GetText(i, 6);
                 String tPayIntv = tMsgSuccSSRS.GetText(i, 7);

                 if (tAppntName == null || "".equals(tAppntName) || "null".equals(tAppntName))
                 {
                     continue;
                 }
                 if (tAppntSex == null || "".equals(tAppntSex) || "null".equals(tAppntSex))
                 {
                     continue;
                 }
                 if (tPrtNo == null || "".equals(tPrtNo) || "null".equals(tPrtNo))
                 {
                     continue;
                 }
                 if (tCValiDate == null || "".equals(tCValiDate) || "null".equals(tCValiDate))
                 {
                     continue;
                 }
                 if (tAppntMobile == null || "".equals(tAppntMobile) || "null".equals(tAppntMobile))
                 {
                     continue;
                 }
                 if (tManageCom == null || "".equals(tManageCom) || "null".equals(tManageCom))
                 {
                     continue;
                 }
                 if (tPayIntv == null || "".equals(tPayIntv) || "null".equals(tPayIntv))
                 {
                     continue;
                 }
                 //短信内容        	
                 String tAgentContents = "";
                 SmsMessage tAgentMsg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                 System.out.println("银保通渠道:PrtNo = " + tPrtNo);
                 //获取产品名称
                 String wrapName = getWrapName(tPrtNo);
                 if(wrapName == null){
                	 wrapName = getRiskName(tPrtNo);
                	 if (wrapName == null) {
							continue;
						}
                 }
                 //获取缴费频次等
                 String paySql = "select payintv,"
						+ "trim(char(payendyear)) || "
						+ "(select codename from ldcode where codetype='payendyearflag' and code=payendyearflag),"
						+ "trim(char(insuyear)) || "
                        + "(select codename from ldcode where codetype='insuyearflag' and code=insuyearflag) ,codename('payintv',payintv) "
						+ "from lcpol where prtno='"
						+ tPrtNo
						+ "' and signdate = '2017-01-01' and stateflag = '1' "
						+ " and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M') with ur";
                 SSRS tSSRS =  mExeSQL.execSQL(paySql);
                 if (tSSRS.getMaxRow() == 0){
                	 continue;
                 } 
                 
                 String payintv = tSSRS.GetText(1, 1);
                 String payendyear = tSSRS.GetText(1, 2);
                 String insuyear = tSSRS.GetText(1, 3);
                 String payintvname = tSSRS.GetText(1, 4);
                 String sendPath = "";
                 String sendPath1 = "";
                 if (payendyear == null || "".equals(payendyear)) {
                	 continue;
                 } else if (payendyear.endsWith("岁")) {
						payendyear = "至" + payendyear;
				 }
                 if (insuyear == null || "".equals(insuyear)) {
                	 continue;
                 } else if (insuyear.endsWith("岁")) {
                	 insuyear = "至" + insuyear;
				 }
                 if (payintvname == null || "".equals(payintvname)) {
                	 continue;
                 }
                 
                 if ("0".equals(payintv)) {
                	 sendPath = "，该产品为趸交产品，保险期间"+insuyear+"，";
                 }else{
                	 sendPath = "，该产品为期交产品，交费期间"+payendyear+"，保险期间"+insuyear+"，";
                 }
                 
                 String dateSQL = "select Trim(Char(Year(signdate))) || '年' || Trim(Char(Month(signdate))) || '月' || Trim(Char(Day(signdate))) || '日' ,"
                	            + "Trim(Char(Year(signdate + 14 day))) || '年' || Trim(Char(Month(signdate + 14 day))) || '月' || Trim(Char(Day(signdate + 14 day))) || '日' , prem "
                	            + "from lccont where prtno='"+tPrtNo+"' with ur" ;
                 tSSRS =  mExeSQL.execSQL(dateSQL);
                 if (tSSRS.getMaxRow() == 0){
                	 continue;
                 } 
                 String startDate = tSSRS.GetText(1, 1);
                 String endDate = tSSRS.GetText(1, 2); 
                 String prem = tSSRS.GetText(1, 3);
                 if(startDate == null || "".equals(startDate)){
                	 continue;
                 }
                 if(endDate == null || "".equals(endDate)){
                	 continue;
                 }
                 if(prem == null || "".equals(prem)){
                	 continue;
                 }
                 
                 String riskType = getRiskType(tPrtNo);
                 boolean riskFL = getRiskFL(tPrtNo);
                 
                 if("8614".equals(tManageCom.substring(0, 4))){
                        //获取投保单申请日期 格式 *年*月*日
						String dateSql = "Select Distinct Trim(Char(Year(Cc.Polapplydate))) || '年' || Trim(Char(Month(Cc.Polapplydate))) || '月' || "
								+ "Trim(Char(Day(Cc.Polapplydate))) || '日'"
								+ "From Lccont Cc "
								+ "Where Cc.Prtno = '" + tPrtNo + "'"
								+ "And Cc.Appflag = '1'"
								+ "And Cc.Stateflag = '1'"
								+ "And Cc.Signdate = '2017-01-01' With Ur";
						
						String polApplyDate = mExeSQL.getOneValue(dateSql);
						if(polApplyDate == null || "".equals(polApplyDate)){
							continue;
						}
						if(payintvname == null || "".equals(payintvname)){
							continue;
						}
						
						sendPath = "，保险期间"+insuyear+"，";
						
						if ("0".equals(payintv)) {
		                	 sendPath1 = "该产品为趸交产品，已交费"+prem+"元。";
		                 }else{
		                	 sendPath1 = "该产品为期交产品，缴费频次为"+payintvname+"，每次交费"+prem+"元，交费期间为"+payendyear+"。";
		                 }
						
						if ("4".equals(riskType)) {
							tAgentContents = "人保健康温馨提示：保险让生活更美好。感谢您于" + polApplyDate
									+ "投保了" + wrapName + "产品" + sendPath 
									+ "犹豫期自" + startDate + "起至" + endDate + "止。" + sendPath1
									+ "为维护您的权益，请您仔细阅读保险合同条款，并请重点关注："
									+ "1、费用扣除的项目、比例和金额；"
									+ "2、犹豫期退保，扣除十元工本费，退还全部保费；"
									+ "3、犹豫期后退保可能产生损失；" + "4、最低保证利率之上的收益不确定。"
									+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
							System.out.println(tAgentContents);
						} else if ("2".equals(riskType)) {
							tAgentContents = "人保健康温馨提示：保险让生活更美好。感谢您于"
									+ polApplyDate + "投保了"
									+ wrapName + "产品" + sendPath 
									+ "犹豫期自" + startDate + "起至" + endDate + "止。"  + sendPath1
									+ "为维护您的权益，请您仔细阅读保险合同条款，并重点关注："
									+ "1、犹豫期退保，扣除十元工本费，退还全部保费；"
									+ "2、犹豫期后退保可能产生损失；" + "3、分红不确定，取决于公司经营状况。"
									+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
							System.out.println(tAgentContents);
						}else{
							tAgentContents= "人保健康温馨提示：保险让生活更美好。感谢您于"
								+ polApplyDate + "投保了"
								+ wrapName + "产品" + sendPath 
								+ "犹豫期自" + startDate + "起至" + endDate + "止。"  + sendPath1
								+ "为维护您的权益，请您仔细阅读保险合同条款，并重点关注："
								+ "1、犹豫期退保，扣除十元工本费，退还全部保费；"
								+ "2、犹豫期后退保可能产生损失；"
								+ "公司近期将电话回访您，请留意接听。客服电话：95591。";
						   System.out.println(tAgentContents);
						}
                	 
                 }else if (!"8612".equals(tManageCom.substring(0, 4))&&riskFL){
                     tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！"
                                    + "感谢您投保中国人民健康保险股份有限公司" + wrapName + "，该产品为趸交产品，保险期间5年（每1000元保险费一年后现金价值1035元／1034元，具体详见现金价值表），"
                                    + "犹豫期自" + startDate + "起至" + endDate + "止。"
                                    + "为保障您的权益，请您认真阅读保险合同条款。祝您健康。客服电话：95591。";
                 }else{
                	 String tjContent1 = "";
                     String tjContent2 = "。";
                     if("8612".equals(tManageCom.substring(0, 4))){   
                    	 tjContent1 = "您签收保单之日起有十五天犹豫期，";
                    	 tjContent2 = "，在此期间如果您解除合同，公司将退还您所交保费（扣除10元工本费），犹豫期后解除合同，将根据合同约定退还您对应年度的现金价值。";
                     }
                     tAgentContents = "尊敬的" + tAppntName + tAppntSex + "，您好！"
                                    + "感谢您投保中国人民健康保险股份有限公司" + wrapName + sendPath + tjContent1
                                    + "犹豫期自" + startDate + "起至" + endDate + "止" + tjContent2
                                    + "为保障您的权益，请您认真阅读保险合同条款。祝您健康。客服电话：95591。";
                 }
                 
                 tAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                 tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                 //和IT讨论后，归类到二级机构
                 tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                 //添加SmsMessage对象                   
                 tVector.add(tAgentMsg);
                 
                 if ("86370600".equals(tManageCom) || "86371000".equals(tManageCom)){
 					SmsMessage newAgentMsg = new SmsMessage();
 					String newAgentContents = "";

 					newAgentContents = "（人保健康）投保风险提示：1、投保有风险，请您认真阅读保险条款。2、谨慎对待签名，不可代签名。3、犹豫期（签收保单日起15日）内可全额退保，犹豫期后退保可能产生损失。";
 					newAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
 					newAgentMsg.setContents(newAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
 					//和IT讨论后，归类到二级机构
 					newAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
 					//添加SmsMessage对象                   
 					tVector.add(newAgentMsg);
 				}
                                 					
             }
             
        }
        
        /*
         * 第五部分：个单渠道 信保通出单承保短信
         * 个单的标志：lccont表中conttype = 1 且 cardflag = '9'。
         * 信保通标志：Cardflag=a
         */
        if(mContTypeFlag.equals("5")) {
        	//查询保单基本信息
        	String tSQLsucc = "select lc.contno, "
                    + "case lc.appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end, "
                    + "lc.appntname, "
                    + "lci.name, "
                    + "lc.Cvalidate, "
                    + "la.Mobile, "
                    + "lc.ManageCom, "
                    + "lc.Prtno "
                    + "from lccont lc, lcappnt lca, lcaddress la, lcinsured lci "
                    + "where 1 = 1 "
                    + "and lc.ContNo = lca.ContNo "
                    + "and lca.ContNo = lci.ContNo "
                    + "and la.CustomerNo = lca.appntno "
                    + "and la.AddressNo = lca.AddressNo "
                    + "and lc.appflag = '1' "
                    + "and lc.stateflag = '1' "
                    + "and lc.conttype = '1' "
                    + "and lc.cardflag = 'a' "  //修改为 a，代表是信保通平台出单
                    + "and la.Mobile is not null "
                    + "and exists (select 1 from Lcriskdutywrap where  Riskwrapcode in ('XBT014', 'XBT013', 'XBT012', 'XBT011', 'XBT010', 'XBT009') and contno=lc.contno) "
                    + "and lc.signdate = '2017-01-01' "
                    + "and not exists (select 1 from LCRNewStateLog lrns where (lrns.ContNo = lc.contno or lrns.NewContNo = lc.contno)) ";
        	//在控制台输出查询语句，方便维护
        	System.out.println(tSQLsucc);
        	//执行查询
        	SSRS tMsgSuccSSRS = mExeSQL.execSQL(tSQLsucc);
        	
        	System.out.println("数据条数："+tMsgSuccSSRS.getMaxRow());
        	//循环查询出的每条记录，进行操作
        	for(int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++) {
        		//获取每条记录的信息，放置在一个字符串中，待操作
        		String tContNo = tMsgSuccSSRS.GetText(i, 1);        //保单号
                String tAppntSex = tMsgSuccSSRS.GetText(i, 2);      //投保人性别
                String tAppntName = tMsgSuccSSRS.GetText(i, 3);     //投保人姓名
                String tName = tMsgSuccSSRS.GetText(i, 4);          //被保人姓名
                String tCValiDate = tMsgSuccSSRS.GetText(i, 5);     //保单生效日期
                String tAppntMobile = tMsgSuccSSRS.GetText(i, 6);   //投保人电话
                String tManageCom = tMsgSuccSSRS.GetText(i, 7);     //管理机构
                String tPrtNo = tMsgSuccSSRS.GetText(i, 8);         //印刷号
                System.out.println("保单号："+tContNo);
                System.out.println("投保人性别："+tAppntSex);
                System.out.println("投保人姓名："+tAppntName);
                System.out.println("被保人姓名："+tName);
                System.out.println("保单生效日期："+tCValiDate);
                System.out.println("投保人电话："+tAppntMobile);
                System.out.println("管理机构："+tManageCom);
                System.out.println("印刷号："+tPrtNo);
                /*
                 * 判断具体字段是否为空，或者空字符串，或者“null”，若是则结束本次循环
                 */
                if(tContNo == null || "".equals(tContNo) || "null".equals(tContNo)) {
                	continue;
                }
                if(tAppntSex == null || "".equals(tAppntSex) || "null".equals(tAppntSex)) {
                	continue;
                }
                if(tAppntName == null || "".equals(tAppntName) || "null".equals(tAppntName)) {
                	continue;
                }
                if(tName == null || "".equals(tName) || "null".equals(tName)) {
                	continue;
                }
                if(tCValiDate == null || "".equals(tCValiDate) || "null".equals(tCValiDate)) {
                	continue;
                }
                if(tAppntMobile == null || "".equals(tAppntMobile) || "null".equals(tAppntMobile)) {
                	continue;
                }
                if(tManageCom == null || "".equals(tManageCom) || "null".equals(tManageCom)) {
                	continue;
                }
                if(tPrtNo == null || "".equals(tPrtNo) || "null".equals(tPrtNo)) {
                	continue;
                }
                
                //获取套餐名称
                String riskType = getRiskType(tPrtNo);
                System.out.println("险种信息（4-万能险， 2-分红险， 0-其他）:"+riskType);
				String wrapName = getRiskName(tPrtNo, riskType);
				System.out.println("套餐名："+wrapName);
                //定义短信内容        	
                String tAgentContents = "";
                //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                SmsMessage tAgentMsg = new SmsMessage(); 
                //获取年月日，需要测试一下
                String year = tCValiDate.substring(0, 4);
                String month = tCValiDate.substring(5, 7);
                String date = tCValiDate.substring(8, 10);
                tAgentContents = "尊敬的"+tAppntName+tAppntSex+"，您好！您为"+tName+"投保的"+wrapName+"已于"+year+"年"+month+"月"+date+"日零时生效，"
                		+ "保险期间365天，保单号为"+tContNo+"。请尽快登录至您投保预留的电子邮箱查收电子保单，或登录至我公司官网http://www.picchealth.com"
                		+ " 下载电子保单。自电子保单发送之日起10日为犹豫期，请您仔细阅读保险合同条款，特别注意保险责任及免责条款说明。祝您健康！";
                System.out.println("短信内容:"+tAgentContents);
                System.out.println("=================一条结束================");
                tAgentMsg.setReceiver(tAppntMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                //和IT讨论后，归类到二级机构
                tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                //添加SmsMessage对象                   
                tVector.add(tAgentMsg);
        	}  
        	
        }

        return tVector;
    }

    /**
     * 获取保单险种类别
     * 
     * @param prtNo 印刷号
     * @return 4-万能险 2-分红险 0-其他
     */
    private String getRiskType(String prtNo){
    	String riskSql = "select case "
			+ "when exists (select 1 from lcpol where prtno='" + prtNo + "' "
			+ "and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and RiskType4='4' and subriskflag='M' and riskcode not in ('340501'))) " 
			+ "then '4' "
			+ "when exists (select 1 from lcpol where prtno='" + prtNo + "' "
			+ "and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and RiskType4='2' and subriskflag='M' and riskcode not in ('340501'))) " 
			+ "then '2' "
			+ "else '0' end " 
			+ "from dual with ur";
    	return mExeSQL.getOneValue(riskSql);
    }

	/**
	 * 获取套餐、主险名称
	 * 
	 * @param tPrtNo
	 *            印刷号
	 * @param riskType
	 *            险类
	 * @return 名称
	 */
	private String getRiskName(String tPrtNo, String riskType) {
		String wrapName;
		String risktypeSQL="";
		if("2".equals(riskType)||"4".equals(riskType)){
			risktypeSQL = " and RiskType4='"+riskType+"' ";
		}
		// 获取套餐名称
		String sendInfSql = "Select (select wrapname from ldwrap where Riskwrapcode=cr.Riskwrapcode) From Lccont Cc left Join Lcriskdutywrap Cr On Cc.Contno = Cr.Contno Where cc.prtno='"
				+ tPrtNo
				+ "' and exists (select 1 from lmriskapp where riskcode=Cr.riskcode "
				+ risktypeSQL
				+ " And Subriskflag = 'M' and riskcode not in ('340501'))"
				+ "and cc.appflag = '1' and cc.stateflag = '1' and cc.signdate = '2017-01-01' with ur ";
		SSRS tSSRS = mExeSQL.execSQL(sendInfSql);
		// 若套餐名称为空，取主险名称
		if (tSSRS.MaxRow == 0 || tSSRS.GetText(1, 1) == null || "".equals(tSSRS.GetText(1, 1))) {
			String riskSql = "select mra.riskname from lcpol cp "
					+ "inner join lmriskapp mra on cp.riskcode = mra.riskcode "
					+ "where cp.prtno='" + tPrtNo
					+ "' and mra.subriskflag='M' and mra.riskcode not in ('340501') "
					+ risktypeSQL + " and cp.signdate = '2017-01-01' with ur";
			SSRS tRiskSSRS = mExeSQL.execSQL(riskSql);
			if (tRiskSSRS.MaxRow == 0) {
				return null;
			} else {
				// 多主险随即取第一条
				wrapName = tRiskSSRS.GetText(1, 1);
			}
		} else {
			wrapName = tSSRS.GetText(1, 1);
		}
		return wrapName;
	}
	
	/**
	 * 获取主险名称
	 * 
	 * @param tPrtNo 印刷号
	 * @return 主险名称
	 */
	private String getRiskName(String tPrtNo){
		String riskSql = "select mra.riskname from lcpol cp "
			+ "inner join lmriskapp mra on cp.riskcode = mra.riskcode "
			+ "where cp.prtno='" + tPrtNo
			+ "' and mra.subriskflag='M' and mra.riskcode not in ('340501') " 
			+ "and cp.signdate = '2017-01-01' " 
			+ "with ur";
    	SSRS tRiskSSRS = mExeSQL.execSQL(riskSql);
    	if (tRiskSSRS.MaxRow == 0) {
    		return null;
    	} else {
    		// 多主险随即取第一条
    		return tRiskSSRS.GetText(1, 1);
    	}
	}
	
	/**
	 * 获取套餐名称
	 * 
	 * @param tPrtNo
	 *            印刷号
	 * @return 名称
	 */
	private String getWrapName(String tPrtNo){
		// 获取套餐名称
		String sendInfSql = "Select (select wrapname from ldwrap where Riskwrapcode=cr.Riskwrapcode) From Lccont Cc left Join Lcriskdutywrap Cr On Cc.Contno = Cr.Contno Where cc.prtno='"
				+ tPrtNo
				+ "' and exists (select 1 from lmriskapp where riskcode=Cr.riskcode "
				+ " And Subriskflag = 'M' and riskcode not in ('340501') )"
				+ "and cc.appflag = '1' and cc.stateflag = '1' " 
				+ "and cc.signdate = '2017-01-01' " 
				+ "with ur";
		SSRS tSSRS = mExeSQL.execSQL(sendInfSql);
		if (tSSRS.MaxRow == 0) {
    		return null;
    	} else {
    		// 多主险随即取第一条
    		return tSSRS.GetText(1, 1);
    	}
	}
	
	/**
	 * 校验字符串是否为空
	 * 
	 * @param check 被校验字符串
	 * @return 为空--true 不为空--false
	 */
	private boolean isNull(String check){
		if (check == null || "".equals(check) || "null".equals(check))
        {
            return true;
        } else {
        	return false;
        }
	}
	
	/**
     * 是否承保福利或福惠双全产品
     */
    private boolean getRiskFL(String tprtNo){
    	String riskFL = "select 1 from lcpol where riskcode in ('333701','334001') and prtno='"+tprtNo+"'";
    	SSRS tSSRS = mExeSQL.execSQL(riskFL);
		if (tSSRS.MaxRow == 0) {
    		return false;
    	} else {
    		return true;
    	}
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData mTransferData = new TransferData();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        
        //团险
    	//String mContTypeFlag = "2";
        
        // 银保
        //QySendMessgeBL tQySendMessgeBL = new QySendMessgeBL();
        //String mContTypeFlag = "4";
        //tQySendMessgeBL.prtno = "'557201211070','557201211071'";
        
        // 个险
        //String mContTypeFlag = "1";
        //tQySendMessgeBL.prtno = "'167201211050','167201211051'";
        
        //银保通出单
        //String mContTypeFlag = "4";
        
        //信保通
        QySendMessgeBL tQySendMessgeBL = new QySendMessgeBL();
        String mContTypeFlag = "5";
        
        mTransferData.setNameAndValue("ContTypeFlag", mContTypeFlag);
        mVData.add(mTransferData);
        tQySendMessgeBL.submitData(mVData, "QYMSG");
    }

}
