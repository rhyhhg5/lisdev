package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LdSpecialProductsBL {
//	错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private GlobalInput mGlobalInput;

    private MMap map = new MMap();
    // 个人信息
    private LDCodeSchema mLDCodeSchema = new LDCodeSchema();
    public LdSpecialProductsBL()
    {
    }
	public void setGlobalInput(GlobalInput aGlobalInput) {
		mGlobalInput = aGlobalInput;
	}
	public GlobalInput getGlobalInput() {
		return mGlobalInput;
	}


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        //得到输入数据
        if (!getInputData())
        {
            return false;
        }
        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDWorkTimeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData()
    {
        // 个人信息

    	mLDCodeSchema = (LDCodeSchema) mInputData.getObjectByObjectName("LDCodeSchema", 0);
    	//mLDPromiseRateSchema=mLDPromiseRateSet.get(1);

        if (mLDCodeSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LdSpecialProductsBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "特殊产品配置，请您确认有：完整的信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果输入数据有错，则返回false,否则返回true
    private boolean checkInputData()
    {
        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData(String cOperate)
    {
    	LDCodeDB tLDCodeDB=new LDCodeDB();
    	tLDCodeDB.setCodeType(mLDCodeSchema.getCodeType());
    	tLDCodeDB.setCode(mLDCodeSchema.getCode());
    	LDCodeSet tLDCodeSet= tLDCodeDB.query();
    	//添加纪录
        if (cOperate.equals("INSERT"))
        {
        	
        	if(tLDCodeSet.size()>0){
                CError tError = new CError();
                tError.moduleName = "LdSpecialProductsBL.java";
                tError.functionName = "dealData";
                tError.errorMessage = "已存在该特殊产品的配置信息！不能保存。";
                this.mErrors.addOneError(tError);
                return false;
        	}
            map.put(mLDCodeSchema, "INSERT");
        }
        //更新纪录
        if (cOperate.equals("UPDATE"))
        {
        	if(tLDCodeSet.size() < 1){
                CError tError = new CError();
                tError.moduleName = "LdSpecialProductsBL.java";
                tError.functionName = "dealData";
                tError.errorMessage = "不存在该特殊产品的配置信息！不能修改。";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	if(tLDCodeSet.size() > 1){
                CError tError = new CError();
                tError.moduleName = "LdSpecialProductsBL.java";
                tError.functionName = "dealData";
                tError.errorMessage = "存在多条该特殊产品的配置信息！请核查数据。";
                this.mErrors.addOneError(tError);
                return false;
        	}
                map.put(mLDCodeSchema, "UPDATE");
        }
        //删除纪录
        if (cOperate.equals("DELETE"))
        {
        	if(tLDCodeSet.size() < 1){
                CError tError = new CError();
                tError.moduleName = "LdSpecialProductsBL.java";
                tError.functionName = "dealData";
                tError.errorMessage = "不存在该特殊产品的配置信息！不能删除。";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	map.put(mLDCodeSchema, "DELETE");
        }
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        try
        {

            mInputData.clear();
            mInputData.add(map);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPromiseRateBL.java";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //得到结果
    public VData getResult()
    {
        return this.mResult;
    }
}
