package com.sinosoft.lis.tb;

/**
 * <p>Title:CrsBTUI.java </p>
 *
 * <p>Description:交叉销售补提批处理 </p>
 * 
 * time:2016-12-26新增
 * 
 * @author yukun
 */

public class CrsBTUI {
	
	/*批处理类*/
	CrsBTBL tCrsBTBL=new CrsBTBL();
	
	public CrsBTUI() {}
	
	/*将前台的两个参数传递到后台*/
	public void setTime(String stratTime , String endTime){
		tCrsBTBL.setTime(stratTime,endTime);
	}
	
	public void run(){
		tCrsBTBL.run();
	}
	
}
