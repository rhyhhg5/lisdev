package com.sinosoft.lis.tb;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统签单功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class GrpPolSignBLS
{
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();
	/** 业务处理相关变量 */
	private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();
	private String mPartFlag = "";		// 部分签单标记 "0"--非部分签单 "1"--部分签单


	public GrpPolSignBLS() {}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
//	    mInputData = ( VData )cInputData.clone() ;
//		if( this.signOneGrp( mInputData ) == false )
//			return false;
//	    mInputData = null;
	    return true;
	}

	/**
	* 保存与一张保单相关的数据（没有事务）
	* 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
	public boolean saveGrpPol( VData tGrpPol )
	{
		// 部分签单传过来的VData为空的，不处理这个部分
		if( tGrpPol.size() == 0 )
			return true;

                //新增：上面传过来的VData为空还需要补充， 如果集体单的投保单和保单号不同，说明是部分签单。
                //单是 从BL中传过来的集体LCGrpPolSchema中的GrpPolNo可能是新产生的号，所以要根据投保单号从数据库中查出来原始数据
                //如果从数据库中查到的集体数据的投保单号和保单号不同，则认为是部分签单。不修改集体保单
                LCGrpPolSchema tLCGrpPolSchema = ( LCGrpPolSchema )tGrpPol.getObjectByObjectName( "LCGrpPolSchema", 0 );
                LCGrpPolDB tempLCGrpPolDB=new LCGrpPolDB();
                tempLCGrpPolDB.setGrpPolNo(tLCGrpPolSchema.getGrpProposalNo());
                if(tempLCGrpPolDB.getInfo()==false)
                    return true;

		Connection conn = null;
		conn = DBConnPool.getConnection();
	    if( conn == null )
	    {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpPolSignBLS";
			tError.functionName = "saveGrpPol";
			tError.errorMessage = "数据库连接失败!";
			this.mErrors .addOneError(tError) ;
			return false;
	    }

		try
		{
			conn.setAutoCommit(false);

			// 删除数据
                        if( this.deleteGrpPol( tGrpPol, conn ) == false )
                        {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "GrpPolSignBLS";
                            tError.functionName = "saveGrpPol";
                            tError.errorMessage = "签单集体部分失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback();
                            conn.close();
                            return false;
                        }

                        // 插入数据
                        if( this.insertGrpPol( tGrpPol, conn ) == false )
                        {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "GrpPolSignBLS";
                            tError.functionName = "saveGrpPol";
                            tError.errorMessage = "签单集体部分失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback();
                            conn.close();
                            return false;
                        }

                        conn.commit();
                        conn.close();
		} // end of try
                catch (Exception ex)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GrpPolSignBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = ex.toString();
                    this.mErrors .addOneError(tError);
                    try{conn.rollback(); conn.close(); } catch(Exception e){}
                    return false;
                }
                return true;
	}

	/**
	* 保存与一张集体保单相关的所有数据
	* 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
//	public boolean signOneGrp( VData tInputData )
//	{
//		// 集体部分的信息
//		VData tGrpDetail = ( VData )tInputData.get( 0 );
//		if( this.saveGrpPol( tGrpDetail ) == false )
//			return false;
//
//		// 集体下的个人信息
//		ProposalSignBLS tSignBLS = new ProposalSignBLS();
//		int n = tInputData.size();
//		for( int i = 1; i < n; i++ )
//		{
//			VData tPolDetail = ( VData )tInputData.get( i );
//			if( tSignBLS.saveOnePol( tPolDetail ) == false )
//				return false;
//		}
//
//		// 更新集体投保单的状态
//		if( this.updateGrpAppFlag( tGrpDetail ) == false )
//			return false;
//
//		return true;
//	}

	/**
	* 更新集体投保单的状态
	* 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
	public boolean updateGrpAppFlag( VData tGrpPol )
	{
		// 集体投保单号码
		LCGrpPolSchema tLCGrpPolSchema = ( LCGrpPolSchema )tGrpPol.getObjectByObjectName( "LCGrpPolSchema", 0 );
		//tLCGrpPolSchema.setAppFlag( "1" );
		LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
		tLCGrpPolDB.setSchema( tLCGrpPolSchema );
		if( tLCGrpPolDB.update() == false )
		{
			// @@错误处理
		    this.mErrors.copyAllErrors( tLCGrpPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "ProposalSignBLS";
			tError.functionName = "updateGrpAppFlag";
			tError.errorMessage = "集体投保单状态标记更新失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}

		return true;
	}

	/**
	* 保存与一张集体保单相关的数据
	* 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
	private boolean insertGrpPol( VData tGrpPol, Connection conn )
	{
		// 集体投保单主表
		LCGrpPolSchema tLCGrpPolSchema = ( LCGrpPolSchema )tGrpPol.getObjectByObjectName( "LCGrpPolSchema", 0 );
		LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB( conn );
		tLCGrpPolDB.setSchema( tLCGrpPolSchema );
		if( tLCGrpPolDB.insert() == false )
		{
			// @@错误处理
		    this.mErrors.copyAllErrors( tLCGrpPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "ProposalSignBLS";
			tError.functionName = "insertGrpPol";
			tError.errorMessage = "LCGrpPol表保存失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}

		// 核保主表
		LCGUWMasterSet tLCGUWMasterSet = ( LCGUWMasterSet )tGrpPol.getObjectByObjectName( "LCGUWMasterSet", 0 );
		LCGUWMasterDBSet tLCGUWMasterDBSet = new LCGUWMasterDBSet( conn );
		tLCGUWMasterDBSet.set( tLCGUWMasterSet );
		if (tLCGUWMasterDBSet.insert() == false)
		{
			// @@错误处理
		    this.mErrors.copyAllErrors(tLCGUWMasterDBSet.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProposalSignBLS";
			tError.functionName = "insertGrpPol";
			tError.errorMessage = "LCGUWMaster表保存失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}

		// 核保子表
		LCGUWSubSet tLCGUWSubSet = ( LCGUWSubSet )tGrpPol.getObjectByObjectName( "LCGUWSubSet", 0 );
		LCGUWSubDBSet tLCGUWSubDBSet = new LCGUWSubDBSet( conn );
		tLCGUWSubDBSet.set( tLCGUWSubSet );
		if (tLCGUWSubDBSet.insert() == false)
		{
			// @@错误处理
		    this.mErrors.copyAllErrors(tLCGUWSubDBSet.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProposalSignBLS";
			tError.functionName = "insertGrpPol";
			tError.errorMessage = "LCGUWSub表保存失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}

		// 核保错误信息表
		LCGUWErrorSet tLCGUWErrorSet = ( LCGUWErrorSet )tGrpPol.getObjectByObjectName( "LCGUWErrorSet", 0 );
		LCGUWErrorDBSet tLCGUWErrorDBSet = new LCGUWErrorDBSet( conn );
		tLCGUWErrorDBSet.set( tLCGUWErrorSet );
		if (tLCGUWErrorDBSet.insert() == false)
		{
			// @@错误处理
		    this.mErrors.copyAllErrors(tLCGUWErrorDBSet.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProposalSignBLS";
			tError.functionName = "insertGrpPol";
			tError.errorMessage = "LCGUWError表保存失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}

                //单独抽取出来--或加个判断--如果是保全走保全财务
                TransferData tBQData = ( TransferData )tGrpPol.getObjectByObjectName( "TransferData", 0 );
                if(tBQData==null)
                {
                    //正常财务处理
                    if(insertFinance(tGrpPol,conn)==false)
                    return false;
                }
                else
                {
                    String inEdorType = (String)tBQData.getValueByName("EdorType");
                    // 插入数据
                    if( this.insertGrpPolBQ( tGrpPol, conn , inEdorType) == false )
                        return false;
                }

		return true;
	}

	/**
	* 删除与一张保单相关的数据
	* 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
	private boolean deleteGrpPol( VData tGrpPol, Connection conn )
	{
		LCGrpPolSchema tLCGrpPolSchema = ( LCGrpPolSchema )tGrpPol.getObjectByObjectName( "LCGrpPolSchema", 0 );
		String tOldGrpPolNo = tLCGrpPolSchema.getGrpProposalNo();

		// 核保错误信息
		LCGUWErrorDB tLCGUWErrorDB = new LCGUWErrorDB( conn );
		tLCGUWErrorDB.setGrpPolNo( tOldGrpPolNo );
		if (tLCGUWErrorDB.deleteSQL() == false)
		{
			// @@错误处理
		    this.mErrors.copyAllErrors(tLCGUWErrorDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProposalSignBLS";
			tError.functionName = "deleteGrpPol";
			tError.errorMessage = "LCGUWError表删除失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}

		// 核保子表
		LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB( conn );
		tLCGUWSubDB.setGrpPolNo( tOldGrpPolNo );
		if (tLCGUWSubDB.deleteSQL() == false)
		{
			// @@错误处理
		    this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProposalSignBLS";
			tError.functionName = "deleteGrpPol";
			tError.errorMessage = "LCGUWSub表删除失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}

		// 核保主表
		LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB( conn );
		tLCGUWMasterDB.setGrpPolNo( tOldGrpPolNo );
		if (tLCGUWMasterDB.deleteSQL() == false)
		{
			// @@错误处理
		    this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProposalSignBLS";
			tError.functionName = "deleteGrpPol";
			tError.errorMessage = "LCGUWMaster表删除失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}

		// 投保单
		LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB( conn );
		tLCGrpPolDB.setGrpPolNo( tOldGrpPolNo );
		if (tLCGrpPolDB.deleteSQL() == false)
		{
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "GrpPolSignBLS";
                    tError.functionName = "deleteGrpPol";
                    tError.errorMessage = "LCGrpPol表删除失败!";
                    this.mErrors .addOneError(tError) ;
                    return false;
		}

		return true;
	}

        /**
         * 根据集体投保单号删除磁盘导入日志
         * @param GrpPolNo
         * @return
         */
        public boolean delDiskInfo(String GrpPolNo)
        {
            LCGrpImportLogDB tLCGrpImportLogDB=new LCGrpImportLogDB();
//            tLCGrpImportLogDB.setGrpPolNo(GrpPolNo);
            if(tLCGrpImportLogDB.deleteSQL()==false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGrpImportLogDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpPolSignBLS";
                tError.functionName = "delDiskInfo";
                tError.errorMessage = "LCGrpImportLog表删除失败!";
                this.mErrors .addOneError(tError) ;
                return false;
            }
            return true;
        }

        /**
         * 正常的财务核销
         * @param tGrpPol
         * @param conn
         * @return
         */
        private boolean insertFinance(VData tGrpPol,Connection conn)
        {
            // 暂交费
            LJTempFeeSet tLJTempFeeSet = ( LJTempFeeSet )tGrpPol.getObjectByObjectName( "LJTempFeeSet", 0 );
            LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet( conn );
            tLJTempFeeDBSet.set( tLJTempFeeSet );
            if (tLJTempFeeDBSet.update() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ProposalSignBLS";
                tError.functionName = "insertGrpPol";
                tError.errorMessage = "LJTempFee表更新失败!";
                this.mErrors .addOneError(tError) ;
                return false;
            }

            // 暂交费子表
            LJTempFeeClassSet tLJTempFeeClassSet = ( LJTempFeeClassSet )tGrpPol.getObjectByObjectName( "LJTempFeeClassSet", 0 );
            LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet( conn );
            tLJTempFeeClassDBSet.set( tLJTempFeeClassSet );
            if( tLJTempFeeClassDBSet.update() == false )
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ProposalSignBLS";
                tError.functionName = "insertGrpPol";
                tError.errorMessage = "LJTempFeeClass表更新失败!";
                this.mErrors .addOneError(tError) ;
                return false;
            } // end of if

            // 实收总表
            VData tAPay = ( VData )tGrpPol.getObjectByObjectName( "VData", 0 );
            LJAPaySet tLJAPaySet = ( LJAPaySet )tAPay.getObjectByObjectName( "LJAPaySet", 0 );
            String tAction = ( String )tAPay.get( 0 );
            LJAPayDBSet tLJAPayDBSet = new LJAPayDBSet( conn );
            tLJAPayDBSet.set( tLJAPaySet );
            if( tAction.equals( "INSERT" ))
            {
                if (tLJAPayDBSet.insert() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJAPayDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ProposalSignBLS";
                    tError.functionName = "savePol";
                    tError.errorMessage = "LJAPay表保存失败!";
                    this.mErrors .addOneError(tError) ;
                    return false;
                }
            }
            if( tAction.equals( "UPDATE" ))
            {
                if (tLJAPayDBSet.update() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJAPayDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ProposalSignBLS";
                    tError.functionName = "savePol";
                    tError.errorMessage = "LJAPay表保存失败!";
                    this.mErrors .addOneError(tError) ;
                    return false;
                }
            }

            // 实收集体表
            LJAPayGrpSet tLJAPayGrpSet = ( LJAPayGrpSet )tGrpPol.getObjectByObjectName( "LJAPayGrpSet", 0 );
            LJAPayGrpDBSet tLJAPayGrpDBSet = new LJAPayGrpDBSet( conn );
            tLJAPayGrpDBSet.set( tLJAPayGrpSet );
            if (tLJAPayGrpDBSet.insert() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJAPayGrpDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ProposalSignBLS";
                tError.functionName = "insertGrpPol";
                tError.errorMessage = "LJAPayGrp表保存失败!";
                this.mErrors .addOneError(tError) ;
                return false;
            }

            return true;

        }

        /**
         * 团体保全财务核销
         * @param tGrpPol
         * @param conn
         * @param EType
         * @return
         */
        private boolean insertGrpPolBQ( VData tGrpPol, Connection conn ,String EType)
        {
            //团体新增附加险
            if(EType.equals("NS"))
            {
                //得到数据，插入数据库
                LJAPayGrpSet tLJAPayGrpSet=(LJAPayGrpSet)tGrpPol.getObjectByObjectName("LJAPayGrpSet",0);
                LJAPayGrpDBSet LJAPayGrpDBSet=new LJAPayGrpDBSet(conn);
                LJAPayGrpDBSet.set(tLJAPayGrpSet);
                if(LJAPayGrpDBSet.insert()==false)
                {
                    CError tError = new CError();
                    tError.moduleName = "GrpPolSignBLS";
                    tError.functionName = "insertGrpPolBQ";
                    tError.errorMessage = "保全集体实收表插入失败!";
                    this.mErrors .addOneError(tError) ;
                    return false;
                }
            }
            return true;

        }
}
