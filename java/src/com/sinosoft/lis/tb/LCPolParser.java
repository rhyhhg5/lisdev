/*
 * @(#)LCPolParser.java	2005-01-15
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.tb;

import org.w3c.dom.*;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;



import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDOccupationDB;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.LAAgentDB;


/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 个单磁盘投保的解析类，从一个XML数据流中解析出被保险人信息，
 * 险种保单，保费项信息，责任项信息，受益人信息 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author zhangtao
 * @version 1.0
 * @date 2005-01-15
 */
public class LCPolParser {

    //投保人 XML Path
    private static String PATH_APPNT = "ROW";
    private static String PATH_APPNT_AppntID = "AppntID";
    private static String PATH_APPNT_ContID = "ContID";
    private static String PATH_APPNT_CustomerNo = "CustomerNo";
    private static String PATH_APPNT_Name = "Name";
    private static String PATH_APPNT_Sex = "Sex";
    private static String PATH_APPNT_Birthday = "Birthday";
    private static String PATH_APPNT_IDType = "IDType";
    private static String PATH_APPNT_IDNo = "IDNo";
    private static String PATH_APPNT_NativePlace = "NativePlace";
    private static String PATH_APPNT_RgtAddress = "RgtAddress";
    private static String PATH_APPNT_Marriage = "Marriage";
    private static String PATH_APPNT_Nationality = "Nationality";
    private static String PATH_APPNT_OccupationCode = "OccupationCode";
    private static String PATH_APPNT_OccupationType = "OccupationType";
    private static String PATH_APPNT_WorkType = "WorkType";
    private static String PATH_APPNT_PluralityType = "PluralityType";
    private static String PATH_APPNT_BankCode = "BankCode";
    private static String PATH_APPNT_BankAccNo = "BankAccNo";
    private static String PATH_APPNT_AccName = "AccName";
    private static String PATH_APPNT_SmokeFlag = "SmokeFlag";
    private static String PATH_APPNT_PostalAddress = "PostalAddress";
    private static String PATH_APPNT_ZipCode = "ZipCode";
    private static String PATH_APPNT_Phone = "Phone";
    private static String PATH_APPNT_Fax = "Fax";
    private static String PATH_APPNT_Mobile = "Mobile";
    private static String PATH_APPNT_EMail = "EMail";
    private static String PATH_APPNT_HomeAddress = "HomeAddress";
    private static String PATH_APPNT_HomeZipCode = "HomeZipCode";
    private static String PATH_APPNT_HomePhone = "HomePhone";
    private static String PATH_APPNT_HomeFax = "HomeFax";
    private static String PATH_APPNT_CompanyAddress = "CompanyAddress";
    private static String PATH_APPNT_CompanyZipCode = "CompanyZipCode";
    private static String PATH_APPNT_CompanyPhone = "CompanyPhone";
    private static String PATH_APPNT_CompanyFax = "CompanyFax";
    private static String PATH_APPNT_GrpNo = "GrpNo";
    private static String PATH_APPNT_GrpName = "GrpName";
    private static String PATH_APPNT_JoinCompanyDate = "JoinCompanyDate";
    private static String PATH_APPNT_Degree = "Degree";
    private static String PATH_APPNT_Position = "Position";
    private static String PATH_APPNT_Salary = "Salary";
    private static String PATH_APPNT_Remark = "Remark";
    private static String PATH_APPNT_PremScope = "PremScope";
    private static String PATH_APPNT_DueFeeMsgFlag = "DueFeeMsgFlag";
    private static String PATH_APPNT_IDStartDate = "IDStartDate";
    private static String PATH_APPNT_IDEndDate = "IDEndDate";
    private static String PATH_APPNT_ExiSpec = "ExiSpec";
    private static String PATH_APPNT_NativeCity = "NativeCity";
    private static String PATH_APPNT_PostalProvince = "PostalProvince";
    private static String PATH_APPNT_PostalCity = "PostalCity";
    private static String PATH_APPNT_PostalCounty = "PostalCounty";
    private static String PATH_APPNT_PostalStreet = "PostalStreet";
    private static String PATH_APPNT_PostalCommunity = "PostalCommunity";
	  //modify by zxs 
    private static String PATH_APPNT_Authorization = "Authorization";
    private static String PATH_APPNT_SharedMark = "SharedMark";
    private static String PATH_APPNT_SpecialLimitMark = "SpecialLimitMark";
    private static String PATH_APPNT_BusinessLink = "BusinessLink";
    private static String PATH_APPNT_CustomerContact = "CustomerContact";
    private static String PATH_APPNT_AuthType = "AuthType";

    //被保险人 XML Path
    private static String PATH_INSURED = "ROW";
    private static String PATH_INSURED_InsuredID = "InsuredID";
    private static String PATH_INSURED_ContID = "ContID";
    private static String PATH_INSURED_CustomerNo = "CustomerNo";
    private static String PATH_INSURED_Name = "Name";
    private static String PATH_INSURED_Sex = "Sex";
    private static String PATH_INSURED_Birthday = "Birthday";
    private static String PATH_INSURED_IDType = "IDType";
    private static String PATH_INSURED_IDNo = "IDNo";
    private static String PATH_INSURED_NativePlace = "NativePlace";
    private static String PATH_INSURED_RgtAddress = "RgtAddress";
    private static String PATH_INSURED_Marriage = "Marriage";
    private static String PATH_INSURED_Nationality = "Nationality";
    private static String PATH_INSURED_OccupationCode = "OccupationCode";
    private static String PATH_INSURED_OccupationType = "OccupationType";
    private static String PATH_INSURED_WorkType = "WorkType";
    private static String PATH_INSURED_PluralityType = "PluralityType";
    private static String PATH_INSURED_BankCode = "BankCode";
    private static String PATH_INSURED_BankAccNo = "BankAccNo";
    private static String PATH_INSURED_AccName = "AccName";
    private static String PATH_INSURED_SmokeFlag = "SmokeFlag";
    private static String PATH_INSURED_PostalAddress = "PostalAddress";
    private static String PATH_INSURED_ZipCode = "ZipCode";
    private static String PATH_INSURED_Phone = "Phone";
    private static String PATH_INSURED_Fax = "Fax";
    private static String PATH_INSURED_Mobile = "Mobile";
    private static String PATH_INSURED_EMail = "EMail";
    private static String PATH_INSURED_HomeAddress = "HomeAddress";
    private static String PATH_INSURED_HomeZipCode = "HomeZipCode";
    private static String PATH_INSURED_HomePhone = "HomePhone";
    private static String PATH_INSURED_HomeFax = "HomeFax";
    private static String PATH_INSURED_GrpName = "GrpName";
    private static String PATH_INSURED_CompanyAddress = "CompanyAddress";
    private static String PATH_INSURED_CompanyZipCode = "CompanyZipCode";
    private static String PATH_INSURED_CompanyPhone = "CompanyPhone";
    private static String PATH_INSURED_CompanyFax = "CompanyFax";
    private static String PATH_INSURED_GrpNo = "GrpNo";
    private static String PATH_INSURED_JoinCompanyDate = "JoinCompanyDate";
    private static String PATH_INSURED_Degree = "Degree";
    private static String PATH_INSURED_Position = "Position";
    private static String PATH_INSURED_Salary = "Salary";
    private static String PATH_INSURED_IDStartDate = "IDStartDate";
    private static String PATH_INSURED_IDEndDate = "IDEndDate";
    private static String PATH_INSURED_NativeCity = "NativeCity";
    private static String PATH_INSURED_PostalProvince = "PostalProvince";
    private static String PATH_INSURED_PostalCity = "PostalCity";
    private static String PATH_INSURED_PostalCounty = "PostalCounty";
    private static String PATH_INSURED_PostalStreet = "PostalStreet";
    private static String PATH_INSURED_PostalCommunity = "PostalCommunity";
	 //modify by zxs 
    private static String PATH_INSURED_Authorization = "Authorization";
    
    private static String PATH_INSURED__SharedMark = "SharedMark";
    private static String PATH_INSURED__SpecialLimitMark = "SpecialLimitMark";
    private static String PATH_INSURED__BusinessLink = "BusinessLink";
    private static String PATH_INSURED_CustomerContact = "CustomerContact";
    private static String PATH_INSURED__AuthType = "AuthType";

    //险种保单 XML Path
    private static String PATH_LCPOL = "ROW";
    private static String PATH_LCPOL_PolID = "PolID";
    private static String PATH_LCPOL_ContID = "ContID";
    private static String PATH_LCPOL_PrtNo = "PrtNo";
    private static String PATH_LCPOL_RiskCode = "RiskCode";
    private static String PATH_LCPOL_FamilyType = "FamilyType";
    private static String PATH_LCPOL_MainPolID = "MainPolID";
    private static String PATH_LCPOL_InsuredID = "InsuredID";
    private static String PATH_LCPOL_RelationToAppnt = "RelationToAppnt";
    private static String PATH_LCPOL_RelationToMainInsured =
            "RelationToMainInsured";
    private static String PATH_LCPOL_AppntID = "AppntID";
    private static String PATH_LCPOL_RelaId = "RelaId";
    private static String PATH_LCPOL_PolApplyDate = "PolApplyDate";
    private static String PATH_LCPOL_CValiDate = "CValiDate";
    private static String PATH_LCPOL_FirstTrialOperator = "FirstTrialOperator";
    private static String PATH_LCPOL_FirstTrialDate = "FirstTrialDate";
    private static String PATH_LCPOL_FirstTrialTime = "FirstTrialTime";
    private static String PATH_LCPOL_ReceiveOperator = "ReceiveOperator";
    private static String PATH_LCPOL_ReceiveDate = "ReceiveDate";
    private static String PATH_LCPOL_ReceiveTime = "ReceiveTime";
    private static String PATH_LCPOL_TempFeeNo = "TempFeeNo";
    private static String PATH_LCPOL_PayMode = "PayMode";
    private static String PATH_LCPOL_LiveGetMode = "LiveGetMode";
    private static String PATH_LCPOL_DeadGetMode = "DeadGetMode";
    private static String PATH_LCPOL_PayIntv = "PayIntv";
    private static String PATH_LCPOL_InsuYear = "InsuYear";
    private static String PATH_LCPOL_InsuYearFlag = "InsuYearFlag";
    private static String PATH_LCPOL_PayEndYear = "PayEndYear";
    private static String PATH_LCPOL_PayEndYearFlag = "PayEndYearFlag";
    private static String PATH_LCPOL_GetYear = "GetYear";
    private static String PATH_LCPOL_GetYearFlag = "GetYearFlag";
    private static String PATH_LCPOL_GetStartType = "GetStartType";
    private static String PATH_LCPOL_GetDutyKind = "GetDutyKind";
    private static String PATH_LCPOL_BonusGetMode = "BonusGetMode";
    private static String PATH_LCPOL_PremToAmnt = "PremToAmnt";
    private static String PATH_LCPOL_Mult = "Mult";
    private static String PATH_LCPOL_Prem = "Prem";
    private static String PATH_LCPOL_Amnt = "Amnt";
    private static String PATH_LCPOL_CalRule = "CalRule";
    private static String PATH_LCPOL_FloatRate = "FloatRate";
    private static String PATH_LCPOL_GetLimit = "GetLimit";
    private static String PATH_LCPOL_GetRate = "GetRate";
    private static String PATH_LCPOL_HealthCheckFlag = "HealthCheckFlag";
    private static String PATH_LCPOL_OutPayFlag = "OutPayFlag";
    private static String PATH_LCPOL_ManageCom = "ManageCom";
    private static String PATH_LCPOL_SaleChnl = "SaleChnl";
    private static String PATH_LCPOL_AgentCode = "AgentCode";
    private static String PATH_LCPOL_AgentName = "AgentName";
    private static String PATH_LCPOL_AgentCom = "AgentCom";
    private static String PATH_LCPOL_BankWorkSite = "BankWorkSite";
    private static String PATH_LCPOL_StandbyFlag1 = "StandbyFlag1";
    private static String PATH_LCPOL_StandbyFlag2 = "StandbyFlag2";
    private static String PATH_LCPOL_StandbyFlag3 = "StandbyFlag3";
    private static String PATH_LCPOL_SupplementaryPrem = "SupplementaryPrem";
    private static String PATH_LCPOL_ExPayMode = "ExPayMode";
    private static String PATH_LCPOL_Crs_SaleChnl = "Crs_SaleChnl";
    private static String PATH_LCPOL_Crs_BussType = "Crs_BussType";
    private static String PATH_LCPOL_GrpAgentCom = "GrpAgentCom";
    private static String PATH_LCPOL_GrpAgentCode = "GrpAgentCode";
    private static String PATH_LCPOL_GrpAgentName = "GrpAgentName";
    private static String PATH_LCPOL_GrpAgentIDNo = "GrpAgentIDNo";
    

    //责任项 XML Path
    private static String PATH_SUBDUTY = "SUBDUTYTABLE/ROW";
    private static String PATH_SUBDUTY_PolID = "PolID";
    private static String PATH_SUBDUTY_InsuredID = "InsuredID";
    private static String PATH_SUBDUTY_RiskCode = "RiskCode";
    private static String PATH_SUBDUTY_DutyCode = "DutyCode";
    private static String PATH_SUBDUTY_PayIntv = "PayIntv";
    private static String PATH_SUBDUTY_InsuYear = "InsuYear";
    private static String PATH_SUBDUTY_InsuYearFlag = "InsuYearFlag";
    private static String PATH_SUBDUTY_PayEndYear = "PayEndYear";
    private static String PATH_SUBDUTY_PayEndYearFlag = "PayEndYearFlag";
    private static String PATH_SUBDUTY_GetYear = "GetYear";
    private static String PATH_SUBDUTY_GetYearFlag = "GetYearFlag";
    private static String PATH_SUBDUTY_GetStartType = "GetStartType";
    private static String PATH_SUBDUTY_GetDutyKind = "GetDutyKind";
    private static String PATH_SUBDUTY_BonusGetMode = "BonusGetMode";
    private static String PATH_SUBDUTY_PremToAmnt = "PremToAmnt";
    private static String PATH_SUBDUTY_Mult = "Mult";
    private static String PATH_SUBDUTY_Prem = "Prem";
    private static String PATH_SUBDUTY_Amnt = "Amnt";
    private static String PATH_SUBDUTY_CalRule = "CalRule";
    private static String PATH_SUBDUTY_FloatRate = "FloatRate";
    private static String PATH_SUBDUTY_GetLimit = "GetLimit";
    private static String PATH_SUBDUTY_GetRate = "GetRate";
    private static String PATH_SUBDUTY_StandbyFlag1 = "StandbyFlag1";
    private static String PATH_SUBDUTY_StandbyFlag2 = "StandbyFlag2";
    private static String PATH_SUBDUTY_StandbyFlag3 = "StandbyFlag3";

    //受益人 XML Path
    private static String PATH_BNF = "ROW";
    private static String PATH_BNF_ContId = "ContId";
    private static String PATH_BNF_PolID = "PolID";
    private static String PATH_BNF_InsuredId = "InsuredId";
    private static String PATH_BNF_RiskCode = "RiskCode";
    private static String PATH_BNF_BnfType = "BnfType";
    private static String PATH_BNF_Name = "Name";
    private static String PATH_BNF_Sex = "Sex";
    private static String PATH_BNF_IDType = "IDType";
    private static String PATH_BNF_IDNo = "IDNo";
    private static String PATH_BNF_Birthday = "Birthday";
    private static String PATH_BNF_RelationToInsured = "RelationToInsured";
    private static String PATH_BNF_BnfGrade = "BnfGrade";
    private static String PATH_BNF_BnfLot = "BnfLot";

    //客户告知 XML Path
    private static String PATH_IMPART = "ROW";
    private static String PATH_IMPART_ContId = "ContId";
    private static String PATH_IMPART_CustomerNoType = "CustomerNoType";
    private static String PATH_IMPART_CustomerNo = "CustomerNo";
    private static String PATH_IMPART_ImpartVer = "ImpartVer";
    private static String PATH_IMPART_ImpartCode = "ImpartCode";
    private static String PATH_IMPART_ImpartParamModle = "ImpartParamModle";
    private static String PATH_IMPART_DiseaseContent = "DiseaseContent";
    private static String PATH_IMPART_StartDate = "StartDate";
    private static String PATH_IMPART_EndDate = "EndDate";
    private static String PATH_IMPART_Prover = "Prover";
    private static String PATH_IMPART_CurrCondition = "CurrCondition";
    private static String PATH_IMPART_IsProved = "IsProved";
    
    //税优标识 XML Path
    private static String PATH_LCContSub ="ROW";
    private static String PATH_LCContSub_TaxFlag ="TaxFlag";
    private static String PATH_LCContSub_GrpNo = "GrpNo";
    private static String PATH_LCContSub_TaxPayerType = "TaxPayerType";
    private static String PATH_LCContSub_TransFlag = "TransFlag";
    private static String PATH_LCContSub_TaxNo = "TaxNo";
    private static String PATH_LCContSub_CreditCode = "CreditCode";
    private static String PATH_LCContSub_GOrgancomCode = "GOrgancomCode";
    private static String PATH_LCContSub_GTaxNo = "GTaxNo";
    private static String PATH_LCContSub_Agediscountfactor = "Agediscountfactor";
    private static String PATH_LCContSub_Supdiscountfactor = "Supdiscountfactor";
    private static String PATH_LCContSub_Grpdiscountfactor = "Grpdiscountfactor";
    private static String PATH_LCContSub_Totaldiscountfactor = "Totaldiscountfactor";
    private static String PATH_LCContSub_PremMult = "PremMult";
    public CErrors mErrors = new CErrors();
    
    private String mDueFeeMsgFlag = "";
    
    private String mExiSpec = "";

    /**
     * 构造函数
     */
    public LCPolParser() {

    }

    /**
     * 解析险种保单结点
     * @param node Node
     * @return VData
     */
    public VData parseLCPolNode(Node node) {
        NodeList nodeList = null;
        boolean contFlag = false;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_LCPOL);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        VData reData = new VData();
        int nLength = nodeList == null ? 0 : nodeList.getLength();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            VData tData = new VData();
            if (parseOneNode(nodeList.item(nIndex), tData)) {
                LCContSchema
                        tLCContSchema = (LCContSchema)
                                        tData.
                                        getObjectByObjectName("LCContSchema", 0);
                if (contFlag == false && tLCContSchema != null) {
                    reData.add(tLCContSchema);
                    contFlag = true;
                }
                //reData.add((String)tData.getObjectByObjectName("String",0));
                reData.add(tData);
            }
        }

        return reData;
    }

    /**
     * 解析一个DOM树的节点，在这个节点中，包含个人险种保单的所有信息。
     *
     * @param node 要解析的节点
     * @param vReturn 存放返回数据的VData
     * @return boolean
     */
    public boolean parseOneNode(Node node,
                                VData vReturn) {
        try {
            if (vReturn == null) {
                buildError("parseOneNode", "存放返回数据的VData是非法的");
                return false;
            }

            // 一些特殊的信息
            TransferData tTransferData = getTransferData(node);

            // 个人险种信息
            LCPolSchema tLCPolSchema = getLCPolSchema(node);

            LCContSchema tLCContSchema = getLCContSchema(node);

            String id = (String) tTransferData.getValueByName("ID");

            if ("".equals(StrTool.cTrim(tLCPolSchema.getInsuredNo()))) {
                CError.buildErr(this, "保单[" + id + "]没有填写被保险人ID");
                return false;
            }
            if ("".equals(StrTool.cTrim(tLCPolSchema.getContNo()))) {
                CError.buildErr(this, "保单[" + id + "]没有填写合同ID");
                return false;
            }
            if ("".equals(StrTool.cTrim(tLCPolSchema.getRiskCode()))) {
                CError.buildErr(this, "保单[" + id + "]没有填写险种代码");
                return false;
            }
            String PolKey = tLCPolSchema.getContNo() + "-"
                            + tLCPolSchema.getInsuredNo() + "-"
                            + tLCPolSchema.getRiskCode();
            tTransferData.setNameAndValue("PolKey", PolKey);
            tTransferData.setNameAndValue("ContId", tLCPolSchema.getContNo());

            // 得到保险责任计算信息
            LCDutySet tLCDutySet = getLCDutySet(node);

            // 返回数据
            vReturn.add(tTransferData);
            vReturn.add(tLCPolSchema);
            vReturn.add(tLCContSchema);
            vReturn.add(tLCDutySet);
            vReturn.add(tLCPolSchema.getPrtNo());
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }


    /**
     * 利用XPathAPI取得某个节点的节点值
     *
     * @param node Node
     * @param strPath String
     * @return java.lang.String
     */
    private static String parseNode(Node node,
                                    String strPath) {
        String strValue = "";

        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();
            if(strValue.startsWith("\n ") || strValue.startsWith("\n\r "))
            {
                strValue = "";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return strValue;
    }

    /**
     * 得到一些不好用Schema表示的信息
     * @param node Node
     * @return TransferData
     */
    private TransferData getTransferData(Node node) {
        TransferData tTransferData = new TransferData();

        tTransferData.setNameAndValue("GetDutyKind",
                                      parseNode(node,
                                                PATH_LCPOL_GetDutyKind));
        tTransferData.setNameAndValue("ID",
                                      parseNode(node,
                                                PATH_LCPOL_PolID));

        return tTransferData;
    }


    /**
     * 得到个人险种保单的信息
     *
     * @param node Node
     * @return com.sinosoft.lis.schema.LCPolSchema
     */
    private LCPolSchema getLCPolSchema(Node node) {
        LCPolSchema tLCPolSchema = new LCPolSchema();

        tLCPolSchema.setPolNo(parseNode(node, PATH_LCPOL_PolID));
        tLCPolSchema.setContNo(parseNode(node, PATH_LCPOL_ContID));
        tLCPolSchema.setPrtNo(parseNode(node, PATH_LCPOL_PrtNo));
        tLCPolSchema.setRiskCode(parseNode(node, PATH_LCPOL_RiskCode));
        //借用[保单类型标记]保存家庭单类型
        tLCPolSchema.setPolTypeFlag(parseNode(node, PATH_LCPOL_FamilyType));
        tLCPolSchema.setMainPolNo(parseNode(node, PATH_LCPOL_MainPolID));
        tLCPolSchema.setInsuredNo(parseNode(node, PATH_LCPOL_InsuredID));
        //借用[签单机构]缓存 与投保人关系
        tLCPolSchema.setSignCom(parseNode(node, PATH_LCPOL_RelationToAppnt));
        //借用[主被保人保单号]缓存 与主被保人关系
        tLCPolSchema.setMasterPolNo(parseNode(node,
                                              PATH_LCPOL_RelationToMainInsured));
        tLCPolSchema.setAppntNo(parseNode(node, PATH_LCPOL_AppntID));
        //借用[AppFlag]缓存连身被保险人
        tLCPolSchema.setAppFlag(parseNode(node, PATH_LCPOL_RelaId));
        tLCPolSchema.setPolApplyDate(parseNode(node, PATH_LCPOL_PolApplyDate));
        tLCPolSchema.setCValiDate(parseNode(node, PATH_LCPOL_CValiDate));
        tLCPolSchema.setPayMode(parseNode(node, PATH_LCPOL_PayMode));
        tLCPolSchema.setLiveGetMode(parseNode(node, PATH_LCPOL_LiveGetMode));
        tLCPolSchema.setDeadGetMode(parseNode(node, PATH_LCPOL_DeadGetMode));
        tLCPolSchema.setPayIntv(parseNode(node, PATH_LCPOL_PayIntv));
        if(parseNode(node, PATH_LCPOL_InsuYear).indexOf("A")>=0||
           parseNode(node, PATH_LCPOL_InsuYear).indexOf("Y")>=0){
            //CError.buildErr(this, "险种"+parseNode(node, PATH_LCPOL_RiskCode)+"的保险期间录入错误！！");
            tLCPolSchema.setInsuYear(0);
        }else{
            tLCPolSchema.setInsuYear(parseNode(node, PATH_LCPOL_InsuYear));
        }
        tLCPolSchema.setInsuYearFlag(parseNode(node, PATH_LCPOL_InsuYearFlag));
        tLCPolSchema.setPayEndYear(parseNode(node, PATH_LCPOL_PayEndYear));
        tLCPolSchema.setPayEndYearFlag(parseNode(node,
                                                 PATH_LCPOL_PayEndYearFlag));
        tLCPolSchema.setGetYear(parseNode(node, PATH_LCPOL_GetYear));
        tLCPolSchema.setGetYearFlag(parseNode(node, PATH_LCPOL_GetYearFlag));
        tLCPolSchema.setGetStartType(parseNode(node, PATH_LCPOL_GetStartType));
        //年金类型保存在 TrasferData 中
        tLCPolSchema.setBonusGetMode(parseNode(node, PATH_LCPOL_BonusGetMode));
        tLCPolSchema.setPremToAmnt(parseNode(node, PATH_LCPOL_PremToAmnt));
        if(parseNode(node, PATH_LCPOL_Mult).indexOf("-")>=0){
        tLCPolSchema.setMult("1000");
        }else{
          tLCPolSchema.setMult(parseNode(node, PATH_LCPOL_Mult));
        }
        tLCPolSchema.setPrem(parseNode(node, PATH_LCPOL_Prem));
        tLCPolSchema.setAmnt(parseNode(node, PATH_LCPOL_Amnt));
        //借用[最终核保人编码]UWCode缓存计算规则
        tLCPolSchema.setUWCode(parseNode(node, PATH_LCPOL_CalRule));
        tLCPolSchema.setFloatRate(parseNode(node, PATH_LCPOL_FloatRate));
        //借用[复核状态]缓存 免赔额
        tLCPolSchema.setApproveFlag(parseNode(node, PATH_LCPOL_GetLimit));
        //借用[核保状态]缓存 赔付比例
        tLCPolSchema.setUWFlag(parseNode(node, PATH_LCPOL_GetRate));
        tLCPolSchema.setHealthCheckFlag(parseNode(node,
                                                  PATH_LCPOL_HealthCheckFlag));
        //借用[]缓存 溢交处理方式 待做！！
        tLCPolSchema.setManageCom(parseNode(node, PATH_LCPOL_ManageCom));
        tLCPolSchema.setSaleChnl(parseNode(node, PATH_LCPOL_SaleChnl));
        String tagentcode=parseNode(node, PATH_LCPOL_AgentCode);
        if(!"".equals(tagentcode)){
        	LAAgentDB tLAAgentDB = new LAAgentDB();
        	LAAgentSet tLAAgentSet=tLAAgentDB.executeQuery("select * from laagent where groupagentcode='"+tagentcode+"' with ur");
        	if(tLAAgentSet.size()>0){
        		tagentcode=tLAAgentSet.get(1).getAgentCode();
        	}else{
        		tagentcode="";
        	}
        }
        tLCPolSchema.setAgentCode(tagentcode);
        tLCPolSchema.setAgentCom(parseNode(node, PATH_LCPOL_AgentCom));
        tLCPolSchema.setRemark(parseNode(node, PATH_LCPOL_AgentName));
        //借用[]缓存 银行营业网点 待做！！
        tLCPolSchema.setStandbyFlag1(parseNode(node, PATH_LCPOL_StandbyFlag1));
        tLCPolSchema.setStandbyFlag2(parseNode(node, PATH_LCPOL_StandbyFlag2));
        tLCPolSchema.setStandbyFlag3(parseNode(node, PATH_LCPOL_StandbyFlag3));
        
        // 保存契约险种追加保费。
        tLCPolSchema.setSupplementaryPrem(parseNode(node, PATH_LCPOL_SupplementaryPrem));
        // ---------------------------
        
        // 保存契约险种续期缴费方式。
        tLCPolSchema.setExPayMode(parseNode(node, PATH_LCPOL_ExPayMode));
        // ---------------------------

        return tLCPolSchema;
    }

    /**
     * 得到个人合同的相关信息
     *
     * @param node Node
     * @return com.sinosoft.lis.schema.LCContSchema
     */
    private LCContSchema getLCContSchema(Node node) {
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setFirstTrialOperator(parseNode(node,
                PATH_LCPOL_FirstTrialOperator));
        tLCContSchema.setFirstTrialDate(parseNode(node,
                                                  PATH_LCPOL_FirstTrialDate));
        tLCContSchema.setFirstTrialTime(parseNode(node,
                                                  PATH_LCPOL_FirstTrialTime));
        tLCContSchema.setReceiveOperator(parseNode(node,
                PATH_LCPOL_ReceiveOperator));
        tLCContSchema.setReceiveDate(parseNode(node, PATH_LCPOL_ReceiveDate));
        tLCContSchema.setReceiveTime(parseNode(node, PATH_LCPOL_ReceiveTime));
        tLCContSchema.setTempFeeNo(parseNode(node, PATH_LCPOL_TempFeeNo));
        //tLCContSchema.setRemark(parseNode(node, PATH_LCPOL_Remark));
        tLCContSchema.setPayMode(parseNode(node, PATH_LCPOL_PayMode));
        tLCContSchema.setAccName(parseNode(node, PATH_APPNT_AccName));
        tLCContSchema.setBankAccNo(parseNode(node, PATH_APPNT_BankAccNo));
        tLCContSchema.setBankCode(parseNode(node, PATH_APPNT_BankCode));
        tLCContSchema.setDueFeeMsgFlag(parseNode(node, PATH_APPNT_DueFeeMsgFlag));
        tLCContSchema.setExPayMode(parseNode(node, PATH_LCPOL_ExPayMode));
        tLCContSchema.setExiSpec(parseNode(node, PATH_APPNT_ExiSpec));
        tLCContSchema.setCrs_BussType(parseNode(node, PATH_LCPOL_Crs_BussType));
        tLCContSchema.setCrs_SaleChnl(parseNode(node, PATH_LCPOL_Crs_SaleChnl));
        tLCContSchema.setGrpAgentCom(parseNode(node, PATH_LCPOL_GrpAgentCom));
        tLCContSchema.setGrpAgentCode(parseNode(node, PATH_LCPOL_GrpAgentCode));
        tLCContSchema.setGrpAgentName(parseNode(node, PATH_LCPOL_GrpAgentName));
        tLCContSchema.setGrpAgentIDNo(parseNode(node, PATH_LCPOL_GrpAgentIDNo));
        return tLCContSchema;
    }

    /**
     * 得到保单责任计算信息
     *
     * @param node Node
     * @return com.sinosoft.lis.vschema.LCDutySet
     */
    private LCDutySet getLCDutySet(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_SUBDUTY);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LCDutySet tLCDutySet = new LCDutySet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeDuty = nodeList.item(nIndex);

            LCDutySchema tLCDutySchema = new LCDutySchema();

            tLCDutySchema.setPolNo(parseNode(nodeDuty, PATH_SUBDUTY_PolID));
            //被保险人 暂时未作处理！！
            //险种代码 暂时未作处理！！
            tLCDutySchema.setDutyCode(parseNode(nodeDuty,
                                                PATH_SUBDUTY_DutyCode));
            tLCDutySchema.setPayIntv(parseNode(nodeDuty,
                                               PATH_SUBDUTY_PayIntv));
            tLCDutySchema.setInsuYear(parseNode(nodeDuty,
                                                PATH_SUBDUTY_InsuYear));
            tLCDutySchema.setInsuYearFlag(parseNode(nodeDuty,
                    PATH_SUBDUTY_InsuYearFlag));
            tLCDutySchema.setPayEndYear(parseNode(nodeDuty,
                                                  PATH_SUBDUTY_PayEndYear));
            tLCDutySchema.setPayEndYearFlag(parseNode(nodeDuty,
                    PATH_SUBDUTY_PayEndYearFlag));
            tLCDutySchema.setGetYear(parseNode(nodeDuty,
                                               PATH_SUBDUTY_GetYear));
            tLCDutySchema.setGetYearFlag(parseNode(nodeDuty,
                    PATH_SUBDUTY_GetYearFlag));
            tLCDutySchema.setGetStartType(parseNode(nodeDuty,
                    PATH_SUBDUTY_GetStartType));
            //年金领取类型 getDutyKind 暂时未作处理！！
            tLCDutySchema.setBonusGetMode(parseNode(nodeDuty,
                    PATH_SUBDUTY_BonusGetMode));
            tLCDutySchema.setPremToAmnt(parseNode(nodeDuty,
                                                  PATH_SUBDUTY_PremToAmnt));
            tLCDutySchema.setMult(parseNode(nodeDuty,
                                            PATH_SUBDUTY_Mult));
            tLCDutySchema.setPrem(parseNode(nodeDuty,
                                            PATH_SUBDUTY_Prem));
            tLCDutySchema.setAmnt(parseNode(nodeDuty,
                                            PATH_SUBDUTY_Amnt));
            tLCDutySchema.setCalRule(parseNode(nodeDuty,
                                               PATH_SUBDUTY_CalRule));
            tLCDutySchema.setFloatRate(parseNode(nodeDuty,
                                                 PATH_SUBDUTY_FloatRate));
            tLCDutySchema.setGetLimit(parseNode(nodeDuty,
                                                PATH_SUBDUTY_GetLimit));
            tLCDutySchema.setGetRate(parseNode(nodeDuty,
                                               PATH_SUBDUTY_GetRate));
            tLCDutySchema.setStandbyFlag1(parseNode(nodeDuty,
                    PATH_SUBDUTY_StandbyFlag1));
            tLCDutySchema.setStandbyFlag2(parseNode(nodeDuty,
                    PATH_SUBDUTY_StandbyFlag2));
            tLCDutySchema.setStandbyFlag3(parseNode(nodeDuty,
                    PATH_SUBDUTY_StandbyFlag3));

            tLCDutySet.add(tLCDutySchema);
        }

        return tLCDutySet;
    }

    /**
     * 从XML中解析投保人信息
     *
     * @param node Node
     * @return com.sinosoft.utility.VData
     */
    public VData getLCAppntData(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_APPNT);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();

        VData tAppntVData = new VData();
        LCAppntSet tLCAppntSet = new LCAppntSet();
        LCAddressSet tLCAppntAddressSet = new LCAddressSet();
        LCPersonTraceSet tlLcPersonTraceSet = new LCPersonTraceSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeInsured = nodeList.item(nIndex);
            LCAppntSchema tLCAppntSchema = new LCAppntSchema();
            LCAddressSchema tLCAppntAddressSchema = new LCAddressSchema();
            String tPostalAddress = parseNode(nodeInsured,PATH_APPNT_PostalAddress);
            String tHomePhone = parseNode(nodeInsured,PATH_APPNT_HomePhone);
//            String[] ttPostalAddress=tPostalAddress.split("[$]",-1);
            String[] ttHomePhone=tHomePhone.split("-",-1);
            //借用prtNo保存投保人序号
            tLCAppntSchema.setPrtNo(parseNode(nodeInsured,PATH_APPNT_AppntID));
            tLCAppntSchema.setContNo(parseNode(nodeInsured,PATH_APPNT_ContID));
            tLCAppntSchema.setAppntNo(parseNode(nodeInsured,PATH_APPNT_CustomerNo));
            tLCAppntSchema.setAppntName(parseNode(nodeInsured,PATH_APPNT_Name));
            tLCAppntSchema.setAppntSex(parseNode(nodeInsured,PATH_APPNT_Sex));
            tLCAppntSchema.setAppntBirthday(parseNode(nodeInsured,PATH_APPNT_Birthday));
            tLCAppntSchema.setIDType(parseNode(nodeInsured,PATH_APPNT_IDType));
            tLCAppntSchema.setIDNo(parseNode(nodeInsured,PATH_APPNT_IDNo));
            tLCAppntSchema.setNativePlace(parseNode(nodeInsured,PATH_APPNT_NativePlace));
            tLCAppntSchema.setRgtAddress(parseNode(nodeInsured,PATH_APPNT_RgtAddress));
            tLCAppntSchema.setMarriage(parseNode(nodeInsured,PATH_APPNT_Marriage));
            tLCAppntSchema.setNationality(parseNode(nodeInsured,PATH_APPNT_Nationality));
            tLCAppntSchema.setOccupationCode(parseNode(nodeInsured,PATH_APPNT_OccupationCode));
            /**
             * picc磁盘投保文件中没有职业类别因此根据职业代码查询职业类别
             */
            LDOccupationDB tLDOccupationDB = new LDOccupationDB();
            tLDOccupationDB.setOccupationCode(tLCAppntSchema.getOccupationCode());
            tLDOccupationDB.getInfo();

            tLCAppntSchema.setOccupationType(tLDOccupationDB.getOccupationType());

            tLCAppntSchema.setWorkType(parseNode(nodeInsured,                                                 PATH_APPNT_WorkType));
            tLCAppntSchema.setPluralityType(parseNode(nodeInsured,                    PATH_APPNT_PluralityType));
            tLCAppntSchema.setBankCode(parseNode(nodeInsured,                                                 PATH_APPNT_BankCode));
            tLCAppntSchema.setBankAccNo(parseNode(nodeInsured,                                                  PATH_APPNT_BankAccNo));
            tLCAppntSchema.setAccName(parseNode(nodeInsured,                                                PATH_APPNT_AccName));
            tLCAppntSchema.setSmokeFlag(parseNode(nodeInsured,                                                  PATH_APPNT_SmokeFlag));
            
            if("OS".equals(tLCAppntSchema.getNativePlace())){
            	String tNativeCity="";
            	LDCodeDB tLDCodeDB=new LDCodeDB();
        		tLDCodeDB.setCodeType("nativecity");
        		tLDCodeDB.setCode(parseNode(nodeInsured,PATH_APPNT_NativeCity));
        		if(tLDCodeDB.getInfo()){
        			tNativeCity=tLDCodeDB.getCode();
        		}
        		tLCAppntSchema.setNativeCity(tNativeCity);
            }else{
                tLCAppntSchema.setNativeCity(parseNode(nodeInsured,PATH_APPNT_NativeCity));
            }
            
			   //modify by zxs
            tLCAppntSchema.setAuthorization(parseNode(nodeInsured,PATH_APPNT_Authorization));
            //用CustomerNo保存 投保人ID 做索引
            tLCAppntAddressSchema.setCustomerNo(parseNode(nodeInsured,                    PATH_APPNT_AppntID));

//         tLCAppntAddressSchema.setPostalAddress(tPostalAddress.replace("$", ""));
            tLCAppntAddressSchema.setZipCode(parseNode(nodeInsured,PATH_APPNT_ZipCode));
            tLCAppntAddressSchema.setPhone(parseNode(nodeInsured,PATH_APPNT_Phone));
            tLCAppntAddressSchema.setFax(parseNode(nodeInsured,PATH_APPNT_Fax));
            tLCAppntAddressSchema.setMobile(parseNode(nodeInsured,PATH_APPNT_Mobile));
            tLCAppntAddressSchema.setEMail(parseNode(nodeInsured,PATH_APPNT_EMail));
            tLCAppntAddressSchema.setHomeAddress(parseNode(nodeInsured,PATH_APPNT_HomeAddress));
            tLCAppntAddressSchema.setHomeZipCode(parseNode(nodeInsured,PATH_APPNT_HomeZipCode));
            tLCAppntAddressSchema.setHomePhone(tHomePhone.replace("-", ""));
            tLCAppntAddressSchema.setHomeFax(parseNode(nodeInsured,PATH_APPNT_HomeFax));
            tLCAppntAddressSchema.setCompanyAddress(parseNode(nodeInsured,PATH_APPNT_CompanyAddress));
            tLCAppntAddressSchema.setCompanyPhone(parseNode(nodeInsured,PATH_APPNT_CompanyPhone));
            tLCAppntAddressSchema.setCompanyZipCode(parseNode(nodeInsured,PATH_APPNT_CompanyZipCode));
            tLCAppntAddressSchema.setCompanyFax(parseNode(nodeInsured,PATH_APPNT_CompanyFax));
            tLCAppntAddressSchema.setGrpName(parseNode(nodeInsured,PATH_APPNT_GrpName));
            //新增字段
//            if(ttPostalAddress.length==5){
            String PostalProvince = parseNode(nodeInsured,PATH_APPNT_PostalProvince);
            String PostalCity = parseNode(nodeInsured,PATH_APPNT_PostalCity);
            String PostalCounty = parseNode(nodeInsured,PATH_APPNT_PostalCounty);
            String PostalStreet = parseNode(nodeInsured,PATH_APPNT_PostalStreet);
            String PostalCommunity = parseNode(nodeInsured,PATH_APPNT_PostalCommunity);
        	tLCAppntAddressSchema.setPostalProvince(PostalProvince);
        	tLCAppntAddressSchema.setPostalCity(PostalCity);
        	tLCAppntAddressSchema.setPostalCounty(PostalCounty);
        	tLCAppntAddressSchema.setPostalStreet(PostalStreet);
        	tLCAppntAddressSchema.setPostalCommunity(PostalCommunity);
        	String ProvinceName =  new ExeSQL().getOneValue("select codename from ldcode1 where codetype = 'province1' and code='"+PostalProvince+"' and code1='0'");
        	String CityName =  new ExeSQL().getOneValue("select codename from ldcode1 where codetype = 'city1' and code='"+PostalCity+"' and code1='"+PostalProvince+"'");
        	String CountyName =  new ExeSQL().getOneValue("select codename from ldcode1 where codetype = 'county1' and code='"+PostalCounty+"' and code1='"+PostalCity+"'");
        	if((!"".equals(ProvinceName) && ProvinceName != null) || (!"".equals(CityName) && CityName != null) || (!"".equals(CountyName) && CountyName != null)  ){
        		tLCAppntAddressSchema.setPostalAddress(ProvinceName+CityName+CountyName+PostalStreet+PostalCommunity);
        	}else{
        		tLCAppntAddressSchema.setPostalAddress(tPostalAddress.replace("$", ""));
        	}
//            }
            if(ttHomePhone.length==2){
            	tLCAppntAddressSchema.setHomeCode(ttHomePhone[0]);
            	tLCAppntAddressSchema.setHomeNumber(ttHomePhone[1]);
            }
//单位编码
//            tLCAppntSchema.setGrpNo(parseNode(nodeInsured,
//                                                 PATH_APPNT_GrpNo));
            tLCAppntSchema.setJoinCompanyDate(parseNode(nodeInsured,                    PATH_APPNT_JoinCompanyDate));
            tLCAppntSchema.setDegree(parseNode(nodeInsured,                                               PATH_APPNT_Degree));
            tLCAppntSchema.setPosition(parseNode(nodeInsured,                                                 PATH_APPNT_Position));
            if(StrTool.cTrim(parseNode(nodeInsured,PATH_APPNT_Salary)).equals("")){
                tLCAppntSchema.setSalary(-1);
            }else{
            tLCAppntSchema.setSalary(parseNode(nodeInsured,PATH_APPNT_Salary));
            }
            /**
             *PICC中磁盘投保没有职业工种，因此借用职位这个字段暂存特约信息
             */
            tLCAppntSchema.setWorkType(parseNode(nodeInsured,this.PATH_APPNT_Remark));
            tLCAppntSchema.setBMI(parseNode(nodeInsured,this.PATH_APPNT_PremScope));
            
            // 获取DueFeeMsgFlag
            mDueFeeMsgFlag = parseNode(nodeInsured, this.PATH_APPNT_DueFeeMsgFlag);
            
            mExiSpec = parseNode(nodeInsured, this.PATH_APPNT_ExiSpec);

            tLCAppntSchema.setExiSpec(parseNode(nodeInsured, PATH_APPNT_ExiSpec));

            tLCAppntSchema.setIDStartDate(parseNode(nodeInsured, PATH_APPNT_IDStartDate));

            tLCAppntSchema.setIDEndDate(parseNode(nodeInsured, PATH_APPNT_IDEndDate));
            
            LCPersonTraceSchema tLcPersonTraceSchema = new LCPersonTraceSchema();
            tLcPersonTraceSchema.setBusinessLink(parseNode(nodeInsured,PATH_APPNT_BusinessLink));
            tLcPersonTraceSchema.setAuthType(parseNode(nodeInsured,PATH_APPNT_AuthType));
            tLcPersonTraceSchema.setCustomerContact(parseNode(nodeInsured,PATH_APPNT_CustomerContact));
            tLcPersonTraceSchema.setAuthorization(parseNode(nodeInsured,PATH_APPNT_Authorization));
            tLcPersonTraceSchema.setSharedMark(parseNode(nodeInsured,PATH_APPNT_SharedMark));
            tLcPersonTraceSchema.setSpecialLimitMark(parseNode(nodeInsured,PATH_APPNT_SpecialLimitMark));
            

            //中英,通过身份证算生日性别
            if ("0".equals(StrTool.cTrim(tLCAppntSchema.getIDType()))) {
				if ("".equals(StrTool.cTrim(tLCAppntSchema.getAppntBirthday())) || tLCAppntSchema.getAppntBirthday() == null||"null".equals(tLCAppntSchema.getAppntBirthday())) {
					tLCAppntSchema.setAppntBirthday(PubFun.getBirthdayFromId(tLCAppntSchema.getIDNo()));
				}
				if ("".equals(StrTool.cTrim(tLCAppntSchema.getAppntSex()))|| tLCAppntSchema.getAppntSex() == null||"null".equals(tLCAppntSchema.getAppntBirthday())) {
					tLCAppntSchema.setAppntSex(PubFun.getSexFromId(tLCAppntSchema.getIDNo()));
				}
			    }
			   if (tLCAppntSchema.getAppntBirthday() == null|| "".equals(StrTool.cTrim(tLCAppntSchema.getAppntBirthday()))||"null".equals(tLCAppntSchema.getAppntBirthday())) {
				FDate fdate = new FDate();
				tLCAppntSchema.setAppntBirthday(fdate.getDate("1900-01-01"));
			    }
            tLCAppntSet.add(tLCAppntSchema);
            tLCAppntAddressSet.add(tLCAppntAddressSchema);
            tlLcPersonTraceSet.add(tLcPersonTraceSchema);
        }
        tAppntVData.add(tLCAppntSet);
        tAppntVData.add(tLCAppntAddressSet);
        tAppntVData.add(tlLcPersonTraceSet);

        return tAppntVData;
    }


    /**
     * 从XML中解析被保人信息
     *
     * @param node Node
     * @return com.sinosoft.utility.VData
     */
    public VData getLCInsuredData(Node node) {
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();

        VData tInsuredVData = new VData();
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCAddressSet tLCInsuredAddressSet = new LCAddressSet();
        
        LCPersonTraceSet tLcPersonTraceSet = new LCPersonTraceSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeInsured = nodeList.item(nIndex);

            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            LCAddressSchema tLCInsuredAddressSchema = new LCAddressSchema();
            String tPostalAddress = parseNode(nodeInsured,PATH_INSURED_PostalAddress);
            String tHomePhone = parseNode(nodeInsured,PATH_INSURED_HomePhone);
//            String[] ttPostalAddress=tPostalAddress.split("[$]",-1);
            String[] ttHomePhone=tHomePhone.split("-",-1);
            //借用prtNo保存被保人序号
            tLCInsuredSchema.setPrtNo(parseNode(nodeInsured,
                                                PATH_INSURED_InsuredID));
            tLCInsuredSchema.setContNo(parseNode(nodeInsured,
                                                 PATH_INSURED_ContID));
            tLCInsuredSchema.setInsuredNo(parseNode(nodeInsured,
                    PATH_INSURED_CustomerNo));
            tLCInsuredSchema.setName(parseNode(nodeInsured,
                                               PATH_INSURED_Name));
            tLCInsuredSchema.setSex(parseNode(nodeInsured,
                                              PATH_INSURED_Sex));
            tLCInsuredSchema.setBirthday(parseNode(nodeInsured,
                    PATH_INSURED_Birthday));
            tLCInsuredSchema.setIDType(parseNode(nodeInsured,
                                                 PATH_INSURED_IDType));
            tLCInsuredSchema.setIDNo(parseNode(nodeInsured,
                                               PATH_INSURED_IDNo));
            tLCInsuredSchema.setNativePlace(parseNode(nodeInsured,
                    PATH_INSURED_NativePlace));
            tLCInsuredSchema.setRgtAddress(parseNode(nodeInsured,
                    PATH_INSURED_RgtAddress));
            tLCInsuredSchema.setMarriage(parseNode(nodeInsured,
                    PATH_INSURED_Marriage));
            tLCInsuredSchema.setNationality(parseNode(nodeInsured,
                    PATH_INSURED_Nationality));
            tLCInsuredSchema.setOccupationCode(parseNode(nodeInsured,
                    PATH_INSURED_OccupationCode));
					 //modify by zxs
            tLCInsuredSchema.setAuthorization(parseNode(nodeInsured,
                    PATH_INSURED_Authorization));
            /**
             * 解决职业类型没有存进去的问题,但是此方法严重影响效率,以后解决
             */
            if (StrTool.cTrim(tLCInsuredSchema.getOccupationCode()).equals("")) {
                this.buildError("getLCInsuredData",
                                "被保人" + tLCInsuredSchema.getName() +
                                "没有录入职业代码！");
                return null;
            }
            LDOccupationDB tLDOccupationDB = new LDOccupationDB();
            tLDOccupationDB.setOccupationCode(tLCInsuredSchema.
                                              getOccupationCode());
            tLDOccupationDB.getInfo();

            tLCInsuredSchema.setOccupationType(tLDOccupationDB.
                                               getOccupationType());

            tLCInsuredSchema.setWorkType(parseNode(nodeInsured,
                    PATH_INSURED_WorkType));
            tLCInsuredSchema.setPluralityType(parseNode(nodeInsured,
                    PATH_INSURED_PluralityType));
            tLCInsuredSchema.setBankCode(parseNode(nodeInsured,
                    PATH_INSURED_BankCode));
            tLCInsuredSchema.setBankAccNo(parseNode(nodeInsured,
                    PATH_INSURED_BankAccNo));
            tLCInsuredSchema.setAccName(parseNode(nodeInsured,
                                                  PATH_INSURED_AccName));
            tLCInsuredSchema.setSmokeFlag(parseNode(nodeInsured,
                    PATH_INSURED_SmokeFlag));
            
            if("OS".equals(tLCInsuredSchema.getNativePlace())){
            	String tNativeCity="";
            	LDCodeDB tLDCodeDB=new LDCodeDB();
        		tLDCodeDB.setCodeType("nativecity");
        		tLDCodeDB.setCode(parseNode(nodeInsured,
                        PATH_INSURED_NativeCity));
        		if(tLDCodeDB.getInfo()){
        			tNativeCity=tLDCodeDB.getCode();
        		}
        		tLCInsuredSchema.setNativeCity(tNativeCity);
            }else{
            	tLCInsuredSchema.setNativeCity(parseNode(nodeInsured,PATH_INSURED_NativeCity));
            }
            
            //用CustomerNo保存被保险人ID做索引
            tLCInsuredAddressSchema.setCustomerNo(parseNode(nodeInsured,
                    PATH_INSURED_InsuredID));
//         tLCInsuredAddressSchema.setPostalAddress(tPostalAddress.replace("$", ""));
            tLCInsuredAddressSchema.setPhone(parseNode(nodeInsured,
                    PATH_INSURED_Phone));
            tLCInsuredAddressSchema.setZipCode(parseNode(nodeInsured,
                    PATH_INSURED_ZipCode));
            tLCInsuredAddressSchema.setFax(parseNode(nodeInsured,
                    PATH_INSURED_Fax));
            tLCInsuredAddressSchema.setMobile(parseNode(nodeInsured,
                    PATH_INSURED_Mobile));
            tLCInsuredAddressSchema.setEMail(parseNode(nodeInsured,
                    PATH_INSURED_EMail));
            tLCInsuredAddressSchema.setHomeAddress(parseNode(nodeInsured,
                    PATH_INSURED_HomeAddress));
            tLCInsuredAddressSchema.setHomeZipCode(parseNode(nodeInsured,
                    PATH_INSURED_HomeZipCode));
            tLCInsuredAddressSchema.setHomePhone(tHomePhone.replace("-", ""));
            tLCInsuredAddressSchema.setHomeFax(parseNode(nodeInsured,
                    PATH_INSURED_HomeFax));
            tLCInsuredAddressSchema.setCompanyAddress(parseNode(nodeInsured,
                    PATH_INSURED_CompanyAddress));
            tLCInsuredAddressSchema.setCompanyPhone(parseNode(nodeInsured,
                    PATH_INSURED_CompanyPhone));
            tLCInsuredAddressSchema.setCompanyZipCode(parseNode(nodeInsured,
                    PATH_INSURED_CompanyZipCode));
            tLCInsuredAddressSchema.setCompanyFax(parseNode(nodeInsured,
                    PATH_INSURED_CompanyFax));
            tLCInsuredAddressSchema.setGrpName(parseNode(nodeInsured,PATH_INSURED_GrpName));
//          新增字段
//            if(ttPostalAddress.length==5){
            String PostalProvince = parseNode(nodeInsured,PATH_INSURED_PostalProvince);
            String PostalCity = parseNode(nodeInsured,PATH_INSURED_PostalCity);
            String PostalCounty = parseNode(nodeInsured,PATH_INSURED_PostalCounty);
            String PostalStreet = parseNode(nodeInsured,PATH_INSURED_PostalStreet);
            String PostalCommunity = parseNode(nodeInsured,PATH_INSURED_PostalCommunity);
        	tLCInsuredAddressSchema.setPostalProvince(PostalProvince);
        	tLCInsuredAddressSchema.setPostalCity(PostalCity);
        	tLCInsuredAddressSchema.setPostalCounty(PostalCounty);
        	tLCInsuredAddressSchema.setPostalStreet(PostalStreet);
        	tLCInsuredAddressSchema.setPostalCommunity(PostalCommunity);
        	String ProvinceName =  new ExeSQL().getOneValue("select codename from ldcode1 where codetype = 'province1' and code='"+PostalProvince+"' and code1='0'");
        	String CityName =  new ExeSQL().getOneValue("select codename from ldcode1 where codetype = 'city1' and code='"+PostalCity+"' and code1='"+PostalProvince+"'");
        	String CountyName =  new ExeSQL().getOneValue("select codename from ldcode1 where codetype = 'county1' and code='"+PostalCounty+"' and code1='"+PostalCity+"'");
        	if((!"".equals(ProvinceName) && ProvinceName != null) || (!"".equals(CityName) && CityName != null) || (!"".equals(CountyName) && CountyName != null)  ){
        		tLCInsuredAddressSchema.setPostalAddress(ProvinceName+CityName+CountyName+PostalStreet+PostalCommunity);
        	}else{
        		tLCInsuredAddressSchema.setPostalAddress(tPostalAddress.replace("$", ""));
        	}
//            }
            if(ttHomePhone.length==2){
            	tLCInsuredAddressSchema.setHomeCode(ttHomePhone[0]);
            	tLCInsuredAddressSchema.setHomeNumber(ttHomePhone[1]);
            }
            //单位编码
//            tLCInsuredAddressSchema.setGrpNo(parseNode(nodeInsured,
//                                                 PATH_INSURED_GrpNo));
            tLCInsuredSchema.setJoinCompanyDate(parseNode(nodeInsured,
                    PATH_INSURED_JoinCompanyDate));
            tLCInsuredSchema.setDegree(parseNode(nodeInsured,
                                                 PATH_INSURED_Degree));
            tLCInsuredSchema.setPosition(parseNode(nodeInsured,
                    PATH_INSURED_Position));
            
            tLCInsuredSchema.setIDStartDate(parseNode(nodeInsured,
                    PATH_INSURED_IDStartDate));
            
            tLCInsuredSchema.setIDEndDate(parseNode(nodeInsured,
                    PATH_INSURED_IDEndDate));
            
         if(StrTool.cTrim(parseNode(nodeInsured,PATH_INSURED_Salary)).equals("")){
             tLCInsuredSchema.setSalary(-1);
             }else{
             tLCInsuredSchema.setSalary(parseNode(nodeInsured,PATH_INSURED_Salary));
             }
            //与主被保险人的关系，暂时取默认
            tLCInsuredSchema.setRelationToMainInsured("00");
            
            LCPersonTraceSchema tLcPersonTraceSchema = new LCPersonTraceSchema();
            tLcPersonTraceSchema.setBusinessLink(parseNode(nodeInsured,PATH_INSURED__BusinessLink));
            tLcPersonTraceSchema.setAuthType(parseNode(nodeInsured,PATH_INSURED__AuthType));
            tLcPersonTraceSchema.setCustomerContact(parseNode(nodeInsured,PATH_INSURED_CustomerContact));
            tLcPersonTraceSchema.setAuthorization(parseNode(nodeInsured,PATH_INSURED_Authorization));
            tLcPersonTraceSchema.setSharedMark(parseNode(nodeInsured,PATH_INSURED__SharedMark));
            tLcPersonTraceSchema.setSpecialLimitMark(parseNode(nodeInsured,PATH_INSURED__SpecialLimitMark));
            tLcPersonTraceSchema.setSpare1(parseNode(nodeInsured, PATH_INSURED_Name));
            //中英,通过身份证算生日性别
			if ("0".equals(StrTool.cTrim(tLCInsuredSchema.getIDType()))) {
				if ("".equals(StrTool.cTrim(tLCInsuredSchema.getBirthday())) || tLCInsuredSchema.getBirthday() == null||"null".equals(tLCInsuredSchema.getBirthday())) {
					tLCInsuredSchema.setBirthday(PubFun.getBirthdayFromId(tLCInsuredSchema.getIDNo()));
				}
				if ("".equals(StrTool.cTrim(tLCInsuredSchema.getSex()))|| tLCInsuredSchema.getSex() == null||"null".equals(tLCInsuredSchema.getSex())) {
					tLCInsuredSchema.setSex(PubFun.getSexFromId(tLCInsuredSchema.getIDNo()));
				}
			    }
			   if (tLCInsuredSchema.getBirthday() == null|| "".equals(StrTool.cTrim(tLCInsuredSchema.getBirthday()))||"null".equals(tLCInsuredSchema.getBirthday())) {
				FDate fdate = new FDate();
				tLCInsuredSchema.setBirthday(fdate.getDate("1900-01-01"));
			    }

			tLCInsuredSet.add(tLCInsuredSchema);
			tLCInsuredAddressSet.add(tLCInsuredAddressSchema);
			tLcPersonTraceSet.add(tLcPersonTraceSchema);
		        }
        tInsuredVData.add(tLcPersonTraceSet);
		tInsuredVData.add(tLCInsuredSet);
		tInsuredVData.add(tLCInsuredAddressSet);
		return tInsuredVData;
    }


    /**
     * 从XML中解析受益人的信息
     *
     * @param node Node
     * @return com.sinosoft.lis.vschema.LCBnfSet
     */
    public LCBnfSet getLCBnfSet(Node node) {
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_BNF);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LCBnfSet tLCBnfSet = new LCBnfSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeBnf = nodeList.item(nIndex);
            LCBnfSchema tLCBnfSchema = new LCBnfSchema();

            tLCBnfSchema.setContNo(parseNode(nodeBnf,
                                             PATH_BNF_ContId));
            if(tLCBnfSchema.getContNo() == null
                || tLCBnfSchema.getContNo().equals(""))
            {
                tLCBnfSchema.setContNo(parseNode(nodeBnf, "ContID"));  //在做外包录入节点
            }

            tLCBnfSchema.setPolNo(parseNode(nodeBnf,
                                            PATH_BNF_PolID));
            tLCBnfSchema.setInsuredNo(parseNode(nodeBnf,
                                                PATH_BNF_InsuredId));
            if(tLCBnfSchema.getInsuredNo() == null
                || tLCBnfSchema.getInsuredNo().equals(""))
            {
                tLCBnfSchema.setInsuredNo(parseNode(nodeBnf, "InsuredID"));
            }

            //借用 BnfNo 缓存 险种代码
            if(!StrTool.cTrim(parseNode(nodeBnf,
                      PATH_BNF_RiskCode)).equals("")){
              tLCBnfSchema.setBnfNo(Integer.parseInt(parseNode(nodeBnf,
                      PATH_BNF_RiskCode)));
          }else{
             tLCBnfSchema.setBnfNo(1);
          }
          System.out.println("BnfNo"+tLCBnfSchema.getBnfNo());

            tLCBnfSchema.setBnfType(parseNode(nodeBnf,
                                              PATH_BNF_BnfType));
            tLCBnfSchema.setName(parseNode(nodeBnf,
                                           PATH_BNF_Name));
            tLCBnfSchema.setSex(parseNode(nodeBnf,
                                          PATH_BNF_Sex));
            tLCBnfSchema.setIDType(parseNode(nodeBnf,
                                             PATH_BNF_IDType));
            tLCBnfSchema.setIDNo(parseNode(nodeBnf,
                                           PATH_BNF_IDNo));
            tLCBnfSchema.setBirthday(parseNode(nodeBnf,
                                               PATH_BNF_Birthday));
            tLCBnfSchema.setRelationToInsured(parseNode(nodeBnf,
                    PATH_BNF_RelationToInsured));
            tLCBnfSchema.setBnfGrade(parseNode(nodeBnf,
                                               PATH_BNF_BnfGrade));
            tLCBnfSchema.setBnfLot(parseNode(nodeBnf,
                                             PATH_BNF_BnfLot));

            tLCBnfSet.add(tLCBnfSchema);
        }

        return tLCBnfSet;
    }

//======ADD===2005-03-22======ZHANGTAO============BGN========================
    /**
     * 从XML中解析客户告知信息
     *
     * @param node Node
     * @return com.sinosoft.lis.vschema.LCCustomerImpartDetailSet
     */
    public LCCustomerImpartDetailSet getImpartSet(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_IMPART);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();

        LCCustomerImpartDetailSet
                tLCCustomerImpartDetailSet = new LCCustomerImpartDetailSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeImpart = nodeList.item(nIndex);
            LCCustomerImpartDetailSchema
                    tLCCustomerImpartDetailSchema = new
                    LCCustomerImpartDetailSchema();

            tLCCustomerImpartDetailSchema.setContNo(parseNode(nodeImpart,
                    PATH_IMPART_ContId));
            if(tLCCustomerImpartDetailSchema.getContNo() == null
                || tLCCustomerImpartDetailSchema.getContNo().equals(""))
            {
                tLCCustomerImpartDetailSchema.setContNo(parseNode(nodeImpart, "ContID"));  //在做外包录入节点
            }

            tLCCustomerImpartDetailSchema.setCustomerNoType(parseNode(
                    nodeImpart,
                    PATH_IMPART_CustomerNoType));
            tLCCustomerImpartDetailSchema.setCustomerNo(parseNode(nodeImpart,
                    PATH_IMPART_CustomerNo));
            tLCCustomerImpartDetailSchema.setImpartVer(parseNode(nodeImpart,
                    PATH_IMPART_ImpartVer));
            tLCCustomerImpartDetailSchema.setImpartCode(parseNode(nodeImpart,
                    PATH_IMPART_ImpartCode));
            // 借用 告知内容 缓存 告知填写内容[ImpartParamModle]
            tLCCustomerImpartDetailSchema.setImpartDetailContent(parseNode(
                    nodeImpart,
                    PATH_IMPART_ImpartParamModle));
            tLCCustomerImpartDetailSchema.setDiseaseContent(parseNode(
                    nodeImpart,
                    PATH_IMPART_DiseaseContent));
            tLCCustomerImpartDetailSchema.setStartDate(parseNode(nodeImpart,
                    PATH_IMPART_StartDate));
            tLCCustomerImpartDetailSchema.setEndDate(parseNode(nodeImpart,
                    PATH_IMPART_EndDate));
            tLCCustomerImpartDetailSchema.setProver(parseNode(nodeImpart,
                    PATH_IMPART_Prover));
            tLCCustomerImpartDetailSchema.setCurrCondition(parseNode(nodeImpart,
                    PATH_IMPART_CurrCondition));
            tLCCustomerImpartDetailSchema.setIsProved(parseNode(nodeImpart,
                    PATH_IMPART_IsProved));
            tLCCustomerImpartDetailSchema.setSubSerialNo(String.valueOf(
                        nIndex));
            tLCCustomerImpartDetailSet.add(tLCCustomerImpartDetailSchema);
        }

        return tLCCustomerImpartDetailSet;
    }
    
    /**
     * 从XML中解析税优信息
     *
     * @param node Node
     * @return com.sinosoft.utility.VData
     */
    public LCContSubSet getLCContSubData(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_LCContSub );
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();

        LCContSubSet tLCContSubSet = new LCContSubSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeLCContSub = nodeList.item(nIndex);
            LCContSubSchema tLCContSubSchema = new LCContSubSchema();
            //封装实体类
            //税优标识 XML Path
            tLCContSubSchema.setTaxFlag(parseNode(nodeLCContSub,PATH_LCContSub_TaxFlag));
            tLCContSubSchema.setGrpNo(parseNode(nodeLCContSub,PATH_LCContSub_GrpNo));
            tLCContSubSchema.setTaxPayerType(parseNode(nodeLCContSub,PATH_LCContSub_TaxPayerType));
            tLCContSubSchema.setTransFlag(parseNode(nodeLCContSub,PATH_LCContSub_TransFlag));
            tLCContSubSchema.setTaxNo(parseNode(nodeLCContSub,PATH_LCContSub_TaxNo));
            tLCContSubSchema.setCreditCode(parseNode(nodeLCContSub,PATH_LCContSub_CreditCode));
            tLCContSubSchema.setGOrgancomCode(parseNode(nodeLCContSub,PATH_LCContSub_GOrgancomCode));
            tLCContSubSchema.setGTaxNo(parseNode(nodeLCContSub,PATH_LCContSub_GTaxNo));
            tLCContSubSchema.setAgediscountfactor(parseNode(nodeLCContSub,PATH_LCContSub_Agediscountfactor));
            tLCContSubSchema.setSupdiscountfactor(parseNode(nodeLCContSub,PATH_LCContSub_Supdiscountfactor));
            tLCContSubSchema.setGrpdiscountfactor(parseNode(nodeLCContSub,PATH_LCContSub_Grpdiscountfactor));
            tLCContSubSchema.setTotaldiscountfactor(parseNode(nodeLCContSub,PATH_LCContSub_Totaldiscountfactor));
            tLCContSubSchema.setPremMult(parseNode(nodeLCContSub,PATH_LCContSub_PremMult));
            tLCContSubSet.add(tLCContSubSchema);
        }

        return tLCContSubSet;
    }

//======ADD===2005-03-22======ZHANGTAO============END========================
    /**
     * 创建错误信息
     * @param szFunc String 函数名称
     * @param szErrMsg String 错误信息
     */
    private void buildError(String szFunc,
                            String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCPolParser";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

	public String getMDueFeeMsgFlag() {
		return mDueFeeMsgFlag;
	}

    public String getMExiSpec() {
        return mExiSpec;
    }

}
