/*
 * @(#)LCGrpContSignBL.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.tb;

import java.util.*;
import java.sql.*;
import java.util.Date;

import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certifybusiness.CertifyContConst;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.finfee.NewGrpContFeeWithdrawBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保签单业务逻辑处理类-团体签单</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author WUJS
 * @version 6.0
 */

public class LCGrpContSignBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private MMap mMapResult = new MMap();

    private VData mResult = new VData();

    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    //    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    private HashMap mLJTempFeeMap = new HashMap();

    private MMap map1 = new MMap();

    private int tAppntnum;

    //全局需要用到的计算日期
    private Date mFirstPayDate = null;

    private Date maxPayDate;

    private Date maxEnterAccDate;

    private Date maxPayEndDate;

    private String mCurrentDate;

    private String mCurrentTime;

    //处理类型
    private String mPreview;

    // TransferData mTransferData ;
    private HashMap paytoDateMap = new HashMap();

    //保费溢额
    private double mGrpContDiff = 0.00;

    //预打保单指定类型
    private final String CPREVIEW = "PREVIEW";

    //暂收费
    private double mSumTmpPay = 0.00;

    MMap mMap = new MMap();

    private int DB_ERROR = 100;

    private HashMap PolNoMap = new HashMap();

    public LCGrpContSignBL()
    {
    }

    public static void main(String[] args)
    {
        VData cInputData = new VData();
        String cOperate = "";
        //        String Priview = "PREVIEW";
        boolean expectedReturn = true;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "UW0011";
        tGlobalInput.ManageCom = "86310000";
        tGlobalInput.ComCode = "86310000";
        cInputData.add(tGlobalInput);
        String[] testContNo = { "1400007726" };
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        for (int i = 0; i < testContNo.length; i++)
        {
            LCGrpContSchema tLCContSchema = new LCGrpContSchema();
            tLCContSchema.setGrpContNo(testContNo[i]);
            //tLCContSchema.setAppFlag("9");
            tLCGrpContSet.add(tLCContSchema);
        }

        cInputData.add(tLCGrpContSet);
        //        cInputData.add(Priview);
        LCGrpContSignBL lCGrpContSignBL = new LCGrpContSignBL();
        boolean actualReturn = lCGrpContSignBL.submitData(cInputData, cOperate);
    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        //   this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (this.getInputData(cInputData) == false)
        {
            return false;
        }

        //初始化系统当前日期
        //        mCurrentDate = PubFun.getCurrentDate() ;
        //        mCurrentTime = PubFun.getCurrentTime() ;

        //    // 数据操作业务处理
        //    if (this.dealData() == false)
        //      return false;
        //填充要签单的数据集
        LCGrpContSchema lcGrpContSchema = mLCGrpContSet.get(1);
        String time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        String tsq = "INSERT INTO ldtimetest VALUES ('"+lcGrpContSchema.getGrpContNo()+"',current time,'"+time+"','填充签单的数据集开始','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        if (!fillGrpCont())
        {
            return false;
        }
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
         tsq = "INSERT INTO ldtimetest VALUES ('"+lcGrpContSchema.getGrpContNo()+"',current time,'"+time+"','填充签单的数据集结束','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        //签单
        //System.out.println("测试............."+ mLCGrpContSet.get(1).getGrpContNo());
        if (!signGrpCont())
        {
            return false;
        }

        return true;
    }

    /**
     * 团体保单签单
     * @return boolean
     */
    private boolean signGrpCont()
    {
    	
        LCGrpContSchema signSchema = null;
        LCContSet tSignLCContSet = null;
        String newGrpContNo = null;

        //        for (int i = 1; i <= mLCGrpContSet.size(); i++)
        //        {
        signSchema = mLCGrpContSet.get(1);
        this.mLCGrpContSchema = mLCGrpContSet.get(1).getSchema();
        String time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        String tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','签单开始:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        //是否已签单
        if ("1".equals(signSchema.getAppFlag()))
        {
            return true;
        }

        //是否已预打保单
        if ("9".equals(signSchema.getAppFlag()))
        {
            return true;
        }
         time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
         tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','签单校验开始:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        //签单校验
        if (!checkGrpCont(signSchema))
        {
            return false;
        }
         time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
         tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','签单校验结束:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        //进行暂缴费的验证处理
        //预打保单时不做此验证处理
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','暂交费验证处理开始:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        if (!CPREVIEW.equals(mPreview))
        {
            int cpFee = CheckTempFee(signSchema);
            if (cpFee == this.DB_ERROR)
            {
                CError.buildErr(this, "团单合同(" + signSchema.getGrpContNo()
                        + ")交费信息查找错误!");
                return false;
            }
            if (cpFee < 0)
            {
                CError.buildErr(this, "团单合同(" + signSchema.getGrpContNo()
                        + ")交费不足或还有未到帐的交费!");
                return false;
            }
            //* ↓ *** LiuHao *** 2005-05-08 *** add *******************
            if (cpFee == 1)
            {
                //CError.buildErr(this,"团单合同(" + signSchema.getGrpContNo() + ")暂交费大于总保费!");
                //return false;
            }
            //* ↑ *** LiuHao *** 2005-05-08 *** add *******************
        }
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','暂交费验证处理结束:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','换号处理开始:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        /** 2005-03-19 先换可以换的号码**/
        //未换号,则先换集体合同号，否则不用换号
        if (signSchema.getGrpContNo().equals(signSchema.getProposalGrpContNo()))
        {
            //未换号，则先换集体保单号
            String sql = "select grppolno,GrpProposalNo from lcgrppol where grpcontno='"
                    + signSchema.getGrpContNo() + "'";
            ExeSQL exesql = new ExeSQL();
            SSRS ssrs = exesql.execSQL(sql);
            if (ssrs == null)
            {
                CError.buildErr(this, "团单下没有相应的集体保单");
                return false;
            }
            //               String limitNo = PubFun.getNoLimit(signSchema.getManageCom());
            String limitNo = signSchema.getAppntNo(); //picch保单号
            newGrpContNo = PubFun1.CreateMaxNo("GRPCONTNO", limitNo);
            if (StrTool.cTrim(newGrpContNo).equals(""))
            {
                buildError("signGrpCont", " 生成合同号码错误！");
                return false;
            }

            MMap tmpMap = new MMap();
            //险种单相关8
            for (int i = 1; i <= ssrs.getMaxRow(); i++)
            {
                String oldProposalNo = (String) ssrs.GetText(i, 1);
                String newGrpPolNo = PubFun1.CreateMaxNo("GrpPolNo", limitNo);
                tmpMap.add(prepareOtherGrpPolSql(signSchema, newGrpContNo,
                        oldProposalNo, newGrpPolNo));
            }
            //合同相关
            tmpMap.add(this.prepareOtherUpdateGrpCont(newGrpContNo, signSchema
                    .getGrpContNo()));

            PubSubmit ps = new PubSubmit();
            VData sd = new VData();
            sd.add(tmpMap);
            if (!ps.submitData(sd, null))
            {
                CError.buildErr(this, "合同号换号保存错误");
                return false;
            }
            //更新成功
            signSchema.setGrpContNo(newGrpContNo);

        }
        else
        {
            newGrpContNo = signSchema.getGrpContNo();
        }
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','换号处理结束:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        /**/

        //查询获取所有合同单( 个单 )
        //        tSignLCContSet = queryLCContSet(signSchema);
        //        if (tSignLCContSet == null || tSignLCContSet.size() <= 0) {
        //            CError.buildErr(this,
        //                            "团体合同(" + signSchema.getGrpContNo() +
        //                            ")下没有查找到合同单;");
        //            return false;
        //            //可能是由于团体更新导致签单中途失败，不能报错
        //        }
        tSignLCContSet = new LCContSet();
        RSWrapper tRSWrapper = new RSWrapper();
        tRSWrapper.prepareData(tSignLCContSet,
                "select * from lccont where grpcontno='"
                        + signSchema.getGrpContNo() + "' with ur");
        boolean tOneResult = false;
        //加锁
        String tLockSQL = " select prtno from lcgrpcont where proposalgrpcontno = '"+mLCGrpContSet.get(1).getSchema().getProposalGrpContNo()+"' ";
    	String PrtNo = new ExeSQL().getOneValue(tLockSQL);
    	if(PrtNo != null && !"".equals(PrtNo)){
    		String tempSQL = " select 1 from ljtempfee where otherno = '"+PrtNo+"' or otherno = '"+mLCGrpContSet.get(1).getSchema().getProposalGrpContNo()+"'";
    		String temp = new ExeSQL().getOneValue(tempSQL);
    		if(temp != null && !"".equals(temp)){
    			String deleteSQL = "delete from lcurgeverifylog where serialno='"+PrtNo+"' and riskflag='2' and operatetype='5' and operateflag='5' and dealstate='5'";
    			String insertSQL = "INSERT INTO lcurgeverifylog VALUES ('"+PrtNo+"','2',NULL,NULL,NULL,'5','5',"
    					 + "'it001','5',current date,current time,current date,current time,NULL)";
    			System.out.println("提交sql:"+deleteSQL);
    			System.out.println("提交sql:"+insertSQL);
    			new ExeSQL().execUpdateSQL(deleteSQL);
    			new ExeSQL().execUpdateSQL(insertSQL);
    		}
    		
    	}
        //集体单签单
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','集体单签单开始:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        try
        {
            do
            {
            	 tRSWrapper.getData();
                tOneResult = SignOneGrpCont(signSchema, tSignLCContSet,
                        newGrpContNo);
            }
            while (tSignLCContSet.size() > 0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            tRSWrapper.close();
        }
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','集体单签单结束:"+mLCGrpContSchema.getPrtNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        try
        {
            tRSWrapper.close();
        }
        catch (Exception ex)
        {
        }
        if (!tOneResult)
        {
            if (!mErrors.needDealError())
            {
                CError.buildErr(this, "团体合同(" + signSchema.getGrpContNo()
                        + "签单发生错误!");
            }
            return false;
        }
        /**--------------------------------------------------
         * add by zhangxing，投保人帐户新契约溢交转入
         *-------------------------------------------------  */
        if (mGrpContDiff >= 0)
        {
            LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
            tLCAppAccTraceSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
            tLCAppAccTraceSchema.setMoney(mGrpContDiff);
            tLCAppAccTraceSchema.setOtherNo(newGrpContNo);
            tLCAppAccTraceSchema.setOtherType("1");
            tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
            AppAcc tAppAcc = new AppAcc();
            mMap = tAppAcc.accShiftToXQY(tLCAppAccTraceSchema, "1");
        }
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','更新团单相关信息开始','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        //更新整个团单相关信息
        if (!dealOneGrpCont(signSchema, newGrpContNo))
        {
            if (!mErrors.needDealError())
            {
                CError.buildErr(this, "团体合同(" + signSchema.getGrpContNo()
                        + "签单发生错误!");
            }
            //System.out.println(this.mErrors.getErrContent());
            return false;
        }
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','更新团单相关信息结束','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
       
        //预打保单需要生成应收数据交给财务处理
        /* 处理应交的位置不合理  删除  移动到dealOneGrpCont更新整个团单相关信息处理中
         if(CPREVIEW.equals(mPreview))
         {
         if(!dealShodFee(signSchema))
         {
         CError.buildErr(this,
         "团体合同(" + signSchema.getGrpContNo() + "应收提交错误!");
         //return false;
         return true;
         }
         }  LiuHao */
        //    }
        //add by zjd 社保项目
         time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
         tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','签单结束:"+mLCGrpContSchema.getGrpContNo()+"','1',NULL) ";
        new ExeSQL().execUpdateSQL(tsq);
        return true;
    }
    
    /**
     * add by  zjd 20150130 项目制
     * @return VData
     */
    public boolean projectUW(){
    	MMap mMap = new MMap();
    	ExeSQL tExeSQL=new ExeSQL();
    	String mProjectNo="";
    	String muwoperator=tExeSQL.getOneValue("select UWOperator from LCGrpCont where prtno='"+mLCGrpContSchema.getPrtNo()+"' ");
    	String tsql="select * from LCGrpContSub where prtno='"+mLCGrpContSchema.getPrtNo()+"' ";
    	LCGrpContSubDB tLCGrpContSubDB=new LCGrpContSubDB();
    	LCGrpContSubSet tLCGrpContSubSet=tLCGrpContSubDB.executeQuery(tsql);
    	LCProjectUWSet ttLCProjectUW=new LCProjectUWSet();//自动新增的结论
    	if(tLCGrpContSubSet!=null && tLCGrpContSubSet.size()>0){
    		String tprostate="select state from LCProjectInfo where ProjectNo='"+tLCGrpContSubSet.get(1).getProjectNo()+"' ";
    	    mProjectNo=tLCGrpContSubSet.get(1).getProjectNo();
    		String tstate=tExeSQL.getOneValue(tprostate);
    		if(!"05".equals(tstate) && !"07".equals(tstate)){//项目制状态只要不是 05 的都 都变为 立项状态 。分工是和总公司默认审核通过
    			//处理项目信息，并添加项目轨迹
    			LCProjectInfoSet tLCProjectInfoSet = new LCProjectInfoSet();
    			LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
    			String tProjectSQL = "select * from LCProjectInfo where ProjectNo = '"+mProjectNo+"'";
    			tLCProjectInfoSet = tLCProjectInfoDB.executeQuery(tProjectSQL);
    			LCProjectInfoSchema tLCProjectInfoSchema = new LCProjectInfoSchema();
    			tLCProjectInfoSchema = tLCProjectInfoSet.get(1);
    			//处理年度信息
    			LCProjectYearInfoSet tLCProjectYearInfoSet = new LCProjectYearInfoSet();
    			LCProjectYearInfoDB tLCProjectYearInfoDB = new LCProjectYearInfoDB();
    			String tYearSQL = "select * from LCProjectYearInfo where ProjectNo = '"+mProjectNo+"'";
    			tLCProjectYearInfoSet = tLCProjectYearInfoDB.executeQuery(tYearSQL);
    			//处理审核结论
    			LCProjectUWSet tLCProjectUWSet = new LCProjectUWSet();
    			LCProjectUWDB tLCProjectUWDB = new LCProjectUWDB();
    			String tUWSQL = "select * from LCProjectUW where ProjectNo = '"+mProjectNo+"' ";
    			tLCProjectUWSet = tLCProjectUWDB.executeQuery(tUWSQL);
    			if(tLCProjectUWSet == null || tLCProjectUWSet.size()==0){
    				for(int i=1 ;i<=2;i++){
    				LCProjectUWSchema tLCProjectUWSchema=new LCProjectUWSchema();
        	    	String tUWSerialNo = PubFun1.CreateMaxNo("PUWSerialNo", 20);
        	    	tLCProjectUWSchema.setSerialNo(tUWSerialNo);
        	    	String tProjectSerialNo = PubFun1.CreateMaxNo("ProjectSerialNo", 20);
        	    	if(tProjectSerialNo == null || "".equals(tProjectSerialNo)){
        	    		     CError tError = new CError();
        	    			 tError.moduleName = "ProjectUWBL";
        	    			 tError.functionName = "dealData";
        	    			 tError.errorMessage = "获取项目流水号失败！";
        	    			 this.mErrors.addOneError(tError);
        	    			 return false;
        	    		     }
        	    	tLCProjectUWSchema.setProjectSerialNo(tProjectSerialNo);
        	    	tLCProjectUWSchema.setProjectNo(mProjectNo);
        	    	if(i==1){
        	    		tLCProjectUWSchema.setSendOperator("sys");
        	    		
        	    	}else{
        	    		tLCProjectUWSchema.setSendOperator(muwoperator);
        	    	}
        	    	tLCProjectUWSchema.setConclusion("2");
        	    	tLCProjectUWSchema.setSendDate(PubFun.getCurrentDate());
        	        tLCProjectUWSchema.setAuditFlag("0"+i);
        	        tLCProjectUWSchema.setAuditOpinion("同意。");
        	    	String tAuditTimes = getAuditTimes(mProjectNo);
        	        tLCProjectUWSchema.setAuditTimes(tAuditTimes);
        	        tLCProjectUWSchema.setManageCom(mGlobalInput.ManageCom);
        	    	tLCProjectUWSchema.setOperator(mGlobalInput.Operator);
        	    	tLCProjectUWSchema.setMakeDate(PubFun.getCurrentDate());
        	    	tLCProjectUWSchema.setMakeTime(PubFun.getCurrentTime());
        	    	tLCProjectUWSchema.setModifyDate(PubFun.getCurrentDate());
        	    	tLCProjectUWSchema.setModifyTime(PubFun.getCurrentTime());
        	    	ttLCProjectUW.add(tLCProjectUWSchema);
        	    	//mMap.put(tLCProjectUWSchema, SysConst.INSERT);
    				}
    			}else if(tLCProjectUWSet != null && tLCProjectUWSet.size()==1){//到分公司审核
    				tLCProjectUWSet.get(1).setConclusion("2");
    				tLCProjectUWSet.get(1).setAuditOpinion("同意。");
    				String tAuditTimes = getAuditTimes(mProjectNo);
    				tLCProjectUWSet.get(1).setAuditTimes(tAuditTimes);
    				tLCProjectUWSet.get(1).setOperator(mGlobalInput.Operator);
    				tLCProjectUWSet.get(1).setMakeDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(1).setMakeTime(PubFun.getCurrentTime());
    				tLCProjectUWSet.get(1).setModifyDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(1).setModifyTime(PubFun.getCurrentTime());
    				//增加总公司审核结论
    				LCProjectUWSchema tLCProjectUWSchema=new LCProjectUWSchema();
        	    	String tUWSerialNo = PubFun1.CreateMaxNo("PUWSerialNo", 20);
        	    	tLCProjectUWSchema.setSerialNo(tUWSerialNo);
        	    	String tProjectSerialNo = PubFun1.CreateMaxNo("ProjectSerialNo", 20);;
        	    	if(tProjectSerialNo == null || "".equals(tProjectSerialNo)){
        	    		     CError tError = new CError();
        	    			 tError.moduleName = "ProjectUWBL";
        	    			 tError.functionName = "dealData";
        	    			 tError.errorMessage = "获取项目流水号失败！";
        	    			 this.mErrors.addOneError(tError);
        	    			 return false;
        	    		     }
        	    	tLCProjectUWSchema.setProjectSerialNo(tProjectSerialNo);
        	    	tLCProjectUWSchema.setProjectNo(mProjectNo);
        	    	tLCProjectUWSchema.setSendOperator(muwoperator);
        	    	tLCProjectUWSchema.setConclusion("2");
        	    	tLCProjectUWSchema.setSendDate(PubFun.getCurrentDate());
        	        tLCProjectUWSchema.setAuditFlag("02");
        	        tLCProjectUWSchema.setAuditOpinion("同意。");
        	        tLCProjectUWSchema.setAuditTimes(tAuditTimes);
        	        tLCProjectUWSchema.setManageCom(mGlobalInput.ManageCom);
        	    	tLCProjectUWSchema.setOperator(mGlobalInput.Operator);
        	    	tLCProjectUWSchema.setMakeDate(PubFun.getCurrentDate());
        	    	tLCProjectUWSchema.setMakeTime(PubFun.getCurrentTime());
        	    	tLCProjectUWSchema.setModifyDate(PubFun.getCurrentDate());
        	    	tLCProjectUWSchema.setModifyTime(PubFun.getCurrentTime());
        	    	ttLCProjectUW.add(tLCProjectUWSchema);
        	    	//mMap.put(tLCProjectUWSchema, SysConst.INSERT);
    			}else if(tLCProjectUWSet != null && tLCProjectUWSet.size()==2){//到总公司审核
    				tLCProjectUWSet.get(2).setConclusion("2");
    				tLCProjectUWSet.get(2).setAuditOpinion("同意。");
    				tLCProjectUWSet.get(2).setSendOperator(muwoperator);
    				String tAuditTimes = getAuditTimes(mProjectNo);
    				tLCProjectUWSet.get(2).setAuditTimes(tAuditTimes);
    				tLCProjectUWSet.get(2).setOperator(muwoperator);
    				tLCProjectUWSet.get(2).setMakeDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(2).setMakeTime(PubFun.getCurrentTime());
    				tLCProjectUWSet.get(2).setModifyDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(2).setModifyTime(PubFun.getCurrentTime());
    			}
    			
    	        //年度信息处理
    			for(int i=1;i<=tLCProjectYearInfoSet.size();i++){
		        	tLCProjectYearInfoSet.get(i).setState("05");
		        	tLCProjectYearInfoSet.get(i).setModifyDate(PubFun.getCurrentDate());
		        	tLCProjectYearInfoSet.get(i).setModifyTime(PubFun.getCurrentTime());
		        }
    			mMap.put(tLCProjectYearInfoSet, SysConst.UPDATE);
    			//处理项目制状态
    			tLCProjectInfoSchema.setState("05");
    			tLCProjectInfoSchema.setModifyDate(PubFun.getCurrentDate());
    			tLCProjectInfoSchema.setModifyTime(PubFun.getCurrentTime());
    			mMap.put(ttLCProjectUW, SysConst.DELETE_AND_INSERT);
    			mMap.put(tLCProjectInfoSchema, SysConst.UPDATE);
    			mMap.put(tLCProjectUWSet, SysConst.UPDATE);
    			
    		}
    	}
    	mResult.add(mMap);
    	return true;
    }
    
    private String getAuditTimes(String projectno){
    	String aAuditTimes = "";
    	String aAuditTimesSql = "select count(distinct ProjectSerialNo) from LCProjectUW where ProjectNo = '"+projectno+"' and AuditFlag = '02'";
    	aAuditTimes = new ExeSQL().getOneValue(aAuditTimesSql);
    	if(aAuditTimes == null || "".equals(aAuditTimes)){
    		aAuditTimes = "0";
    	}else{
    		aAuditTimes = (Integer.parseInt(aAuditTimes) +1)+"";
    	}
    	return aAuditTimes;
    }

    /**
     * 集体保单签单
     * @param signSchema LCGrpContSchema
     * @param tSignLCContSet LCContSet
     * @param newGrpContNo String
     * @return boolean
     */
    private boolean SignOneGrpCont(LCGrpContSchema signSchema,
            LCContSet tSignLCContSet, String newGrpContNo)
    {
    	String  time =null;
    	String tsq = null;
        boolean signResult = true;
        boolean result = true;
        if (tSignLCContSet != null && tSignLCContSet.size() > 0)
        {
            for (int i = 1; i <= tSignLCContSet.size(); i++)
            {
                LCContSchema tLCContSchema = tSignLCContSet.get(i).getSchema();
                LCContSignBL tLCContSignBL = new LCContSignBL();
                if (tLCContSchema.getAppFlag().equals("1")
                        || tLCContSchema.getAppFlag().equals("9"))
                {
                    //曾经签单成功，跳出
                    //                    tSignLCContSet.removeRange(1, 1);
                    continue;
                }
                LCContSet signSet = new LCContSet();
                signSet.add(tLCContSchema);
                VData tInput = new VData();
                tInput.add(mGlobalInput);
                ;
                tInput.add(signSet);
                //  tInput.add( mTransferData );
                signResult = tLCContSignBL.submitData(tInput, this.mPreview);
                if (!signResult)
                {
                    this.mErrors.copyAllErrors(tLCContSignBL.mErrors);
                    result = false;
                }
                else
                {
                    //处理签单
                    VData contData = new VData();
                    contData.add(mGlobalInput);
                    MMap tmpMap = (MMap) tLCContSignBL.mVResult
                            .getObjectByObjectName("MMap", 0);
                    if (tmpMap == null || tmpMap.keySet().size() <= 0)
                    {
                        //没有要签单的数据，准备下一个合同
                        return true;
                    }
                    contData.add(tmpMap);
                    PubSubmit pubSubmit = new PubSubmit();
                    signResult = pubSubmit.submitData(contData, "");
                    if (!signResult)
                    {
                        if (pubSubmit.mErrors.getErrorCount() > 0)
                        {
                            this.mErrors.copyAllErrors(pubSubmit.mErrors);
                        }
                        else
                        {
                            CError.buildErr(this, "合同签单["
                                    + tLCContSchema.getContNo() + "]保存有误!");
                        }
                        result = false;
                    }
                }
               
            }
          
            //            for (int i = 1; i <= tSignLCContSet.size(); i++) {
            //                //调用合同签单
            //                LCContSignBL tLCContSignBL = new LCContSignBL();
            //                if (tSignLCContSet.get(i).getAppFlag().equals("1")) {
            //                    //曾经签单成功，跳出
            //                    continue;
            //                }
            //                LCContSet signSet = new LCContSet();
            //                signSet.add(tSignLCContSet.get(i));
            //                VData tInput = new VData();
            //                tInput.add(mGlobalInput); ;
            //                tInput.add(signSet);
            //                //  tInput.add( mTransferData );
            //                signResult = tLCContSignBL.submitData(tInput, this.mPreview);
            //                if (!signResult) {
            //                    this.mErrors.copyAllErrors(tLCContSignBL.mErrors);
            //                    result = false;
            //                } else {
            //
            //                    //处理签单
            //                    VData contData = new VData();
            //                    contData.add(mGlobalInput);
            //                    MMap tmpMap = (MMap) tLCContSignBL.mVResult.
            //                                  getObjectByObjectName(
            //                                          "MMap", 0);
            //                    if (tmpMap == null || tmpMap.keySet().size() <= 0) {
            //                        //没有要签单的数据，准备下一个合同
            //                        return true;
            //                    }
            //                    contData.add(tmpMap);
            //                    PubSubmit pubSubmit = new PubSubmit();
            //                    signResult = pubSubmit.submitData(contData, "");
            //                    if (!signResult) {
            //                        if (pubSubmit.mErrors.getErrorCount() > 0) {
            //                            this.mErrors.copyAllErrors(pubSubmit.mErrors);
            //                        } else {
            //                            CError.buildErr(this,
            //                                            "合同签单[" + signSet.get(i).getContNo() +
            //                                            "]保存有误!");
            //                        }
            //                        result = false;
            //                    }
            //                }
            //            }
        }
        //修改险种序号
        /** 杨明注掉，由于没有用，且效率太低 */
        //        System.out.println("处理险种序号");
        //        if (!dealRiskSeqNo(signSchema)) {
        //            return false;
        //        }
        System.out.println("处理险种序号完毕");
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','执行险种特约关联表填写开始','1',NULL) ";
       new ExeSQL().execUpdateSQL(tsq);
        //险种特约关联表填写信息
        setLCPolSpecRela(newGrpContNo);
        time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        tsq = "INSERT INTO ldtimetest VALUES ('"+mLCGrpContSchema.getProposalGrpContNo()+"',current time,'"+time+"','执行险种特约关联表填写结束','1',NULL) ";
       new ExeSQL().execUpdateSQL(tsq);
        return result;
    }

    /**
     * 处理团险险种序号
     *
     * @param pmLCGrpContSchema LCContSchema
     * @return boolean
     */
    private boolean dealRiskSeqNo(LCGrpContSchema pmLCGrpContSchema)
    {
        PubSubmit ps = new PubSubmit();
        VData tInput = new VData();
        MMap tmpMap = new MMap();

        String tSql = "";
        LCPolSet tLCPolSet = null;
        LCPolDB tLCPolDB = new LCPolDB();
        int tRiskSeqNo = 0;

        tSql = "select * from LCPol where GrpContNo = '"
                + pmLCGrpContSchema.getGrpContNo() + "' "
                + " order by InsuredNo,RiskCode";

        RSWrapper tRSWrapper = new RSWrapper();
        tRSWrapper.prepareData(tLCPolSet, tSql);

        System.out.println("险种个数：" + tLCPolSet.size());
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            tRiskSeqNo = tRiskSeqNo + 1;
            String strSql = "UPDATE LCPOL SET RISKSEQNO='"
                    + (((1000 + tRiskSeqNo) + "").substring(2, 4)) + "' "
                    + "WHERE POLNO='" + tLCPolSet.get(i).getPolNo() + "'";
            tmpMap.put(strSql, "UPDATE");
        }
        tInput.add(tmpMap);
        if (!ps.submitData(tInput, null))
        {
            System.out.println("团险险种序号更新失败！");
            CError.buildErr(this, "险种序号更新失败！");
        }

        return true;
    }

    /**
     * 处理与团体合同有关的相关表
     * @param tLCGrpContSchema LCGrpContSchema
     * @param inNewGrpContNo String
     * @return boolean
     */
    private boolean dealOneGrpCont(LCGrpContSchema tLCGrpContSchema,
            String inNewGrpContNo)
    {
        Vector sqlVec = new Vector();
        MMap tmpMap = new MMap();
        Connection conn = DBConnPool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Statement st = null;
        int notSignCount = 0;

        LCGrpPolSet moreGrpPolSet = new LCGrpPolSet();
        LCGrpPolSet lackGrpPolSet = new LCGrpPolSet();
        LCGrpPolSet balanceGrpPolSet = new LCGrpPolSet();

        try
        {
            //  conn.setAutoCommit( false );
            //查询LCCont的更新情况
            String sql = "select count(*) from lccont where grpcontno ='"
                    + tLCGrpContSchema.getGrpContNo()
                    + "' and (uwflag in ('4','9' ) and appflag!='1' "
                    + "or ( approveflag is null ) or (uwflag is null) ) with ur";
            //  System.out.println(sql);

            try
            {
                ps = conn.prepareStatement(sql);
                rs = ps.executeQuery();
                if (rs.next())
                {
                    notSignCount = rs.getInt(1);
                    rs.close();
                    ps.close();
                }

            }
            catch (SQLException ex)
            {
                ex.printStackTrace();
            }
            //还有签单的个单合同,不进行团体签单
            if (notSignCount > 0
                    && !StrTool.cTrim(this.mPreview).equals("PREVIEW"))
            {
                CError.buildErr(this, "团单合同(" + tLCGrpContSchema.getGrpContNo()
                        + ")还有个单合同未签单完毕,请检查个单合同，险种保单信息");
                return false;
            }
            sqlVec.clear();
            double sumPrem = 0.000;
            double sumAmnt = 0.000;
            double sumAllPrem = 0.000;
            double sumAllAmnt = 0.000;
            double sumdiff = 0.000;
            double polSumPrem = 0.000; //保单总实付
            double grpcontsumPrem = 0.000;
            mCurrentDate = PubFun.getCurrentDate();
            mCurrentTime = PubFun.getCurrentTime();
            String tPayNo = PubFun1.CreateMaxNo("PAYNO", PubFun
                    .getNoLimit(tLCGrpContSchema.getManageCom()));
            String outPayFlag = tLCGrpContSchema.getOutPayFlag();

            //更新集体合同,求总保费,总保额,更新到LCGrpPol
            sql = "select grppolno , sum(prem),sum(amnt),sum(sumprem) from lcpol where grpcontno='"
                    + tLCGrpContSchema.getGrpContNo()
                    + "'"
                    + " and appflag in('1','9')  group by grppolno order by grppolno";
            try
            {
                ps = conn.prepareStatement(sql);
                rs = ps.executeQuery();
                String lpGrpPolNo = "";

                sqlVec.clear();
                //对每个集体保单更新，同时计算统计值到lcgrpcont表中
                while (rs.next())
                {
                    lpGrpPolNo = rs.getString(1);
                    //                    String newGrpPolNo = PubFun1.CreateMaxNo("GrpPolNo",
                    //                            tLCGrpContSchema.getManageCom());
                    //                    if (newGrpPolNo.equals(""))
                    //                    {
                    //                        CError.buildErr(this, "创建集体保单号出错!");
                    //                        continue;
                    //                    }
                    sumPrem = rs.getDouble(2);
                    sumAmnt = rs.getDouble(3);
                    polSumPrem = rs.getDouble(4);
                    sumAllPrem += sumPrem;
                    sumAllAmnt += sumAmnt;
                    grpcontsumPrem += polSumPrem;

                    LCGrpPolSchema grpPolSchema = this.queryGrpPol(lpGrpPolNo);
                    if (grpPolSchema == null)
                    {
                        CError.buildErr(this, "查询集体险种出错!");
                        return false;
                    }
                    grpPolSchema.setPrem(sumPrem);
                    grpPolSchema.setAmnt(sumAmnt);
                    grpPolSchema.setSumPrem(polSumPrem);

                    //保存新旧集体单号
                    // PolNoMap.put(StrTool.cTrim(lpGrpPolNo), newGrpPolNo);
                    PolNoMap.put(StrTool.cTrim(lpGrpPolNo), StrTool
                            .cTrim(lpGrpPolNo));
                    double sumTmpPay = 0.00;
                    //暂交费
                    LJTempFeeSet grpTempFeeSet = this.queryTempFeeSet(
                            tLCGrpContSchema.getPrtNo(), grpPolSchema
                                    .getRiskCode());

                    if (grpTempFeeSet == null
                            || this.mPreview.equals("PREVIEW"))
                    {
                        //本险种没有暂交费信息
                        //System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<:"+grpPolSchema.getRiskCode());
                        grpTempFeeSet = new LJTempFeeSet();
                    }
                    //                    else{
                    //
                    //                         this.cacheTempFee( grpPolSchema.getRiskCode(),grpTempFeeSet.get(1));
                    //                    }

                    //处理实交信息－－－－－
                    //                    MMap APayMap = dealGrpAPay(grpPolSchema, tPayNo,
                    //                                               newGrpPolNo,inNewGrpContNo);
                    //                    tmpMap.add(APayMap);

                    //得到暂收费
                    sumTmpPay = this.sumPayMoney(grpTempFeeSet);
                    mSumTmpPay += sumTmpPay;
                    //更新暂交费信息
                    tmpMap.add(prepareUpdateTempFeeSet(grpTempFeeSet,
                            inNewGrpContNo, grpPolSchema));

                    ///new
                    balanceGrpPolSet.add(grpPolSchema);
                    /* ↓ add liuh 2005-4-24 */
                    //解决签单后财务无实收数据的问题
                    //缓存暂交费信息
                    if (grpTempFeeSet.size() > 0)
                    {
                        this.cacheTempFee(grpPolSchema.getRiskCode(),
                                grpTempFeeSet.get(1));
                    }
                    /* ↑ add liuh 2005-4-24 */

                    //                    if (false) {
                    //                        //缓存暂交费信息
                    //                        if (grpTempFeeSet.size() > 0) {
                    //                            this.cacheTempFee(grpPolSchema.getRiskCode(),
                    //                                              grpTempFeeSet.get(1));
                    //                        }
                    //
                    //                        //保费差额
                    //                        double tmpdiff = sumTmpPay - sumPrem;
                    //                        sumdiff = sumdiff + tmpdiff;
                    //                        grpPolSchema.setDif(PubFun.setPrecision(tmpdiff, "0.00"));
                    //                        tLCGrpContSchema.setDif(PubFun.setPrecision(
                    //                                tLCGrpContSchema.getDif() + sumdiff, "0.00"));
                    //                        if ("1".equals(outPayFlag)) {
                    //                            grpPolSchema.setOutPayFlag("1");
                    //                            //划分富余与不足的保费
                    //                            if (sumTmpPay >= sumPrem) {
                    //                                if (sumTmpPay > sumPrem) { //溢交退费
                    //                                    moreGrpPolSet.add(grpPolSchema);
                    //                                } else { //移交续期交费
                    //                                    balanceGrpPolSet.add(grpPolSchema);
                    //                                }
                    //                            } else {
                    //                                lackGrpPolSet.add(grpPolSchema);
                    //                            }
                    //                        } else {
                    //                            balanceGrpPolSet.add(grpPolSchema);
                    //                        }
                    //                    }
                    //其余涉及到的sql
                    //                    MMap otherGrp = prepareOtherGrpPolSql(tLCGrpContSchema,
                    //                            inNewGrpContNo, lpGrpPolNo, newGrpPolNo);
                    //                    tmpMap.add(otherGrp);
                } //while rs.netx()

            }
            catch (SQLException ex)
            {
                ex.printStackTrace();
            }

            //

            //更新集体单信息，包括集体实收表
            MMap grpPolSet3 = prepareNewGrpPolSet(balanceGrpPolSet,
                    inNewGrpContNo, tPayNo);
            if (grpPolSet3 == null && !CPREVIEW.equals(mPreview))
            {
                //                CError.buildErr(this, "团单合同(" + tLCGrpContSchema.getGrpContNo()
                //                        + ")非预打保单，暂收信息有错误,请检查.");
                return false;
            }
            tmpMap.add(grpPolSet3);

            //总实收
            if (!CPREVIEW.equals(mPreview))
            {
                MMap jaPayMap = prepareLJAPAY(tLCGrpContSchema, inNewGrpContNo,
                        tPayNo);
                tmpMap.add(jaPayMap);

                // 回置卡折清单状态，已结算
                MMap tTmpMap = null;
                tTmpMap = dealCertifyContListState(inNewGrpContNo);
                if (tTmpMap != null)
                {
                    tmpMap.add(tTmpMap);
                }
                // ---------------------------
            }

            // 处理double精度问题
            sumAllPrem = dealDouble(sumAllPrem);
            sumAllAmnt = dealDouble(sumAllAmnt);
            grpcontsumPrem = dealDouble(grpcontsumPrem);
            // --------------------

            //  sqlVec.clear();
            String tsql = "update lcgrpcont set  grpcontno='" + inNewGrpContNo
                    + "'" + ",prem=" + sumAllPrem + ",Amnt=" + sumAllAmnt
                    + ",sumprem=" + grpcontsumPrem + ",dif="
                    + this.mGrpContDiff;

            // 设置保单状态 区别预打保单和正常保单
            tsql += ", CustomerReceiptNo ='"
                    + PubFun1.CreateMaxNo("CusReceiptNo", inNewGrpContNo) + "'";
            if (CPREVIEW.equals(mPreview))
            {
                tsql += ", appflag='9'";
            }
            else
            {
                tsql += ", appflag='1',stateflag='1'";
                //增加保单收据号
                tsql += ", ContPremFeeNo='"
                        + PubFun1.CreateMaxNo("GContPremFeeNo", "") + "'"
                        + ", signdate='" + mCurrentDate + "' ,modifydate='"
                        + mCurrentDate + "',modifytime='" + mCurrentTime
                        + "', signTime ='" + mCurrentTime + "'"
                        + ", signCom ='" + this.mGlobalInput.ManageCom + "'";
            }
            /** 境外救援直接添加保单获取时间，保单打印次数 */
            if (StrTool.cTrim(tLCGrpContSchema.getCardFlag()).equals("0"))
            {
                tsql += ",CustomGetPolDate='" + mCurrentDate + "',GetPolDate='"
                        + mCurrentDate + "',GetPolTime='" + mCurrentTime
                        + "',PrintCount=1";
            }
            else
            {
                //卡折默认回执回销日期为当天
                if (StrTool.cTrim(tLCGrpContSchema.getCardFlag()).equals("2"))
                {
                    tsql += ",CustomGetPolDate='" + mCurrentDate
                            + "',GetPolDate='" + mCurrentDate
                            + "',GetPolTime='" + mCurrentTime
                            + "',PrintCount=1 ";
                }
                //汇交件默认回执回销日期为当天 add by cz 2016-1-25
                if("5".equals(StrTool.cTrim(tLCGrpContSchema.getContPrintType()))){
                	tsql += ",CustomGetPolDate='" + mCurrentDate
                            + "',GetPolDate='" + mCurrentDate
                            + "',GetPolTime='" + mCurrentTime 
                            + "'";
                }
            }

            tsql += " where grpcontno='" + tLCGrpContSchema.getGrpContNo()
                    + "'";
            tmpMap.put(tsql, "UPDATE");

            //其他需要更新团单合同的表--已经在签单前换号
            //            tmpMap.add(prepareOtherUpdateGrpCont(inNewGrpContNo,
            //                                                 tLCGrpContSchema.getGrpContNo()));
            //更新签单时间
            
          //对于团体万能产品的保单，更新生效日
            String tsqll = "select count(*) from lcgrppol where grpcontno='" 
            			 + tLCGrpContSchema.getGrpContNo() 
            			 + "' riskcode in ('162701','162901')";
            ExeSQL ttExeSQL = new ExeSQL();
            String tresult = ttExeSQL.getOneValue(tsqll);
            if(!tresult.equals("0")){
            	String tGrpConttSql = " update lcgrpcont a set " 
    						 		+ " cinvalidate = (select (max(enddate) - 1 day) from lcpol where grpcontno=a.grpcontno) " 
    						 		+ " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "'";
            	tmpMap.put(tGrpConttSql, "UPDATE");
            }
            
            //对于团体万能产品的保单，更新生效日
            String tGrpULISQL = "select count(*) from lcgrppol where grpcontno='" + tLCGrpContSchema.getGrpContNo() 
            			+ "' and riskcode in (select riskcode from lmriskapp where riskprop='G' and (risktype4='4' or "
            			+ "riskcode in ('162401','162501','170501','162601','162801')))";
            ExeSQL tExeSQL = new ExeSQL();
            String tGrpULICount = tExeSQL.getOneValue(tGrpULISQL);
            if(!tGrpULICount.equals("0")){
            	String tRiskCode = new ExeSQL().getOneValue("select riskcode from lcgrppol where grpcontno='"+tLCGrpContSchema.getGrpContNo()+"'");
            	String tGrpContSql = "";
            	String tGrpPolSql = "";
				String tLJaPayGrpSql = "";
				String tLJaPayPersonSql = "";
            	if(!"170501".equals(tRiskCode)
            			&&!"370301".equals(tRiskCode)
            			&&!"162501".equals(tRiskCode)
            			&&!"162401".equals(tRiskCode)
            			&&!"162601".equals(tRiskCode)
            			&&!"162801".equals(tRiskCode)){
            		tGrpContSql = "update lcgrpcont a set cvalidate = (select min(cvalidate) from lcpol where grpcontno=a.grpcontno)," 
            					+ " cinvalidate = (select max(enddate) from lcpol where grpcontno=a.grpcontno) " 
            					+ " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "'";
            		tGrpPolSql = "update lcgrppol a set cvalidate = (select min(cvalidate) from lcpol where grppolno=a.grppolno) " 
							   + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "'";
					tLJaPayGrpSql = "update LJaPayGrp a set lastpaytodate = (select min(cvalidate) from lcpol where grpcontno=a.grpcontno) "
							+ " where grpcontno='"
							+ tLCGrpContSchema.getGrpContNo() + "'";
					tLJaPayPersonSql = "update LJaPayPerson a set lastpaytodate = (select min(cvalidate) from lcpol where grpcontno=a.grpcontno) "
							+ " where grpcontno='"
							+ tLCGrpContSchema.getGrpContNo() + "'";
            	}else{
            		tGrpContSql = "update lcgrpcont a set cvalidate = (select min(cvalidate) from lcpol where grpcontno=a.grpcontno)," 
        					    + " cinvalidate = (select max(enddate) from lcpol where grpcontno=a.grpcontno) - 1 day " 
        					    + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "'";
            		tGrpPolSql = "update lcgrppol a set cvalidate = (select min(cvalidate) from lcpol where grppolno=a.grppolno) " 
							   + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "'";
					tLJaPayGrpSql = "update LJaPayGrp a set lastpaytodate = (select min(cvalidate) from lcpol where grpcontno=a.grpcontno) "
							+ " where grpcontno='"
							+ tLCGrpContSchema.getGrpContNo() + "'";
					tLJaPayPersonSql = "update LJaPayPerson a set lastpaytodate = (select min(cvalidate) from lcpol where grpcontno=a.grpcontno) "
							+ " where grpcontno='"
							+ tLCGrpContSchema.getGrpContNo() + "'";
            	}
            	//对险种162601及162801保额签单后更新
            	if("162601".equals(tRiskCode)||"162801".equals(tRiskCode)){
            		String lcpolamnt = "update lcpol set amnt=(select riskamnt from lccontplanrisk where grpcontno='"
            						 + tLCGrpContSchema.getGrpContNo() + "' and contplancode <> '11' fetch first 1 rows only) where grpcontno='"
            						 + tLCGrpContSchema.getGrpContNo() + "'";
            		String lcgrppolamnt = "update lcgrppol set amnt=(select sum(amnt) from lccont where grpcontno='"
   						 			    + tLCGrpContSchema.getGrpContNo() + "') where grpcontno='"
   						 			    + tLCGrpContSchema.getGrpContNo() + "'";
            		String lcgrpcontamnt = "update lcgrpcont set amnt=(select sum(amnt) from lccont where grpcontno='"
   						 				 + tLCGrpContSchema.getGrpContNo() + "') where grpcontno='"
   						 				 + tLCGrpContSchema.getGrpContNo() + "'";
            		System.out.println("针对险种162601及162801保额签单后更新！！！！！");
            		tmpMap.put(lcpolamnt, "UPDATE");
            		tmpMap.put(lcgrppolamnt, "UPDATE");
            		tmpMap.put(lcgrpcontamnt, "UPDATE");
            	}
            	tmpMap.put(tGrpContSql, "UPDATE");
            	tmpMap.put(tGrpPolSql, "UPDATE");
				tmpMap.put(tLJaPayGrpSql, "UPDATE");
				tmpMap.put(tLJaPayPersonSql, "UPDATE");
            }
            // 将 170501险种与团体万能险(370101,370201)放在一起考虑，故将以下代码注释掉
//            //170501险种生效日期维护
//            tGrpULISQL = "select count(*) from lcgrppol where grpcontno='" + tLCGrpContSchema.getGrpContNo() 
//			           + "' and riskcode = '170501' ";
//            tExeSQL = new ExeSQL();
//            tGrpULICount = tExeSQL.getOneValue(tGrpULISQL);
//            if(!tGrpULICount.equals("0")){
//            	String tGrpContSql = "update lcgrpcont a set cvalidate = (select min(cvalidate) from lcpol where grpcontno=a.grpcontno)," 
//            					+ " cinvalidate = (select max(enddate) from lcpol where grpcontno=a.grpcontno) - 1 day " 
//            					+ " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "'";
//            	String tGrpPolSql = "update lcgrppol a set cvalidate = (select min(cvalidate) from lcpol where grppolno=a.grppolno) " 
//								+ " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "'";
//            	tmpMap.put(tGrpContSql, "UPDATE");
//            	tmpMap.put(tGrpPolSql, "UPDATE");
//            }
            
            tmpMap.add(prepareUpdateSignDate(tLCGrpContSchema.getGrpContNo(), tGrpULICount));


            
            /**
             * 预打保单处理方式改变,不需要存应收数据.
             */
            // ↓ add liuhao ***应收数据处理*****************
            //预打保单需要生成应收数据交给财务处理
            //            if (CPREVIEW.equals(mPreview)) {
            //                LJSPaySchema tLJSPaySchema = new LJSPaySchema(); //总应收数据对象
            //                LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet(); //应收集体交费数据对象
            //                LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet(); //应收个人交费数据对象
            //                //取得通知书号码
            //                String StrLimit = PubFun.getNoLimit("86");
            //                String newActNoticeNo = PubFun1.CreateMaxNo("GPAYNOTICENO",
            //                        tLCGrpContSchema.getPrtNo());
            //
            //                //准备应收个人交费数据
            //                tLJSPayPersonSet = prepareLJSPayPerson(tLCGrpContSchema,
            //                        newActNoticeNo);
            //                if (tLJSPayPersonSet.size() > 0) {
            //                    tmpMap.put(tLJSPayPersonSet, "INSERT");
            //                }
            //                //准备应收集体交费数据
            //                tLJSPayGrpSet = prepareLJSPayGrp(tLCGrpContSchema,
            //                                                 newActNoticeNo);
            //                if (tLJSPayGrpSet.size() > 0) {
            //                    tmpMap.put(tLJSPayGrpSet, "INSERT");
            //                }
            //                //准备总应收数据
            //                tLJSPaySchema = prepareLJSPAY(tLCGrpContSchema, newActNoticeNo);
            //                if (tLJSPaySchema != null) {
            //                    tmpMap.put(tLJSPaySchema, "INSERT");
            //                }
            //            }
            // ↑ add liuhao *****************************
            //处理被保险人清单,将被保险人清单挪到备份表中
            tmpMap.add(dealLCInuredList(inNewGrpContNo));
            //处理被保险人险种清单,将被保险人险种清单挪到备份表中
            tmpMap.add(dealLCInuredListPol(inNewGrpContNo));
            /** 团体客户投保次数加一 */
            String sql_update = "update LDGrp set GrpAppntNum=GrpAppntNum+1 where CustomerNo='"
                    + this.mLCGrpContSchema.getAppntNo() + "'";
            tmpMap.put(sql_update, "UPDATE");

            // 对LCInsureAccFee、LCInsureAccFeeTrace表中OtherNo进行更新，保存值为PayNo。
            MMap tTmpMap = prepareInsureAcc(tLCGrpContSchema.getGrpContNo(),
                    tPayNo);
            tmpMap.add(tTmpMap);
            //-------------------------------------------
            
//            by gzh 20111223 增加约定缴费计划明细信息
            if(tLCGrpContSchema.getPayIntv() == -1){
            	GrpPayPlanDetailBL tGrpPayPlanDetailBL = new GrpPayPlanDetailBL();
                VData tVData = new VData();
            	
            	TransferData mTransferData = new TransferData();
            	mTransferData.setNameAndValue("ProposalGrpContNo", tLCGrpContSchema.getProposalGrpContNo());
            	
            	tVData.add(mGlobalInput);
            	tVData.add(mTransferData);
                if(!tGrpPayPlanDetailBL.submitData(tVData, "")){
                	CError.buildErr(this, "拆分约定缴费计划有误", tGrpPayPlanDetailBL.mErrors);
                    return false;
                }
                tmpMap.add(tGrpPayPlanDetailBL.getMMMap());
            }
//            by gzh end

            //提交保存
            VData contData = new VData();
            contData.add(mGlobalInput);
            if (mMap != null)
            {
                tmpMap.add(mMap);
            }
            contData.add(tmpMap);

            PubSubmit pubSubmit = new PubSubmit();
            if (!pubSubmit.submitData(contData, ""))
            {
                CError.buildErr(this, "合同签单更新有误", pubSubmit.mErrors);
                return false;
            }

        }

        catch (Exception ex2)
        {
        }
        finally
        {
            try
            {
                if (st != null)
                {
                    st.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (conn != null)
                {
                    conn.close();
                }

            }
            catch (Exception e)
            {
            }
        }

        return true;
    }

    /**
     * 更新暂交费信息
     *
     * @param tmpFeeSet LJTempFeeSet
     * @param newGrpContNo String
     * @param grppolSchema LCGrpPolSchema
     * @return MMap
     */
    private MMap prepareUpdateTempFeeSet(LJTempFeeSet tmpFeeSet,
            String newGrpContNo, LCGrpPolSchema grppolSchema)
    {
        MMap tmpMap = new MMap();
        String confDate = mCurrentDate;
        String confTime = mCurrentTime;
        // String sql = "";
        FDate fDate = new FDate();
        Date tFirstPayDate = null;
        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        for (int i = 1; i <= tmpFeeSet.size(); i++)
        {
            LJTempFeeSchema tLJTempFeeSchema = tmpFeeSet.get(i);
            tFirstPayDate = fDate.getDate(tLJTempFeeSchema.getPayDate());
            if (maxEnterAccDate == null
                    || maxEnterAccDate.before(tFirstPayDate))
            {
                maxEnterAccDate = tFirstPayDate;
            }
            if (mFirstPayDate == null || tFirstPayDate.before(mFirstPayDate))
            {
                mFirstPayDate = tFirstPayDate;
            }
            if (maxPayDate == null || maxPayDate.before(tFirstPayDate))
            {
                maxPayDate = tFirstPayDate;
            }
            tLJTempFeeSchema.setAgentCode(grppolSchema.getAgentCode());
            tLJTempFeeSchema.setAgentCom(grppolSchema.getAgentCom());
            tLJTempFeeSchema.setAgentGroup(grppolSchema.getAgentGroup());
            tLJTempFeeSchema.setAgentType(grppolSchema.getAgentType());
            tLJTempFeeSchema.setConfDate(confDate);
            //            tLJTempFeeSchema.setConfMakeDate(confDate);
            tLJTempFeeSchema.setConfFlag("1");
            //            tLJTempFeeSchema.setConfMakeTime(confTime);
            tLJTempFeeSchema.setSaleChnl(grppolSchema.getSaleChnl());
            tLJTempFeeSchema.setPolicyCom(grppolSchema.getManageCom());
            tLJTempFeeSchema.setAPPntName(grppolSchema.getGrpName());
            tLJTempFeeSchema.setOtherNo(newGrpContNo);
            tLJTempFeeSchema.setOtherNoType("7");
            tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());

            tLJTempFeeClassDB.setTempFeeNo(tLJTempFeeSchema.getTempFeeNo());
            LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.query();
            if (tLJTempFeeClassSet != null && tLJTempFeeClassSet.size() > 0)
            {
                for (int j = 1; j <= tLJTempFeeClassSet.size(); j++)
                {
                    LJTempFeeClassSchema tSchema = tLJTempFeeClassSet.get(j);
                    // tSchema.setAccName(grppolSchema.getAcc);
                    tSchema.setApproveDate(confDate);
                    tSchema.setConfDate(confDate);
                    tSchema.setAppntName(grppolSchema.getGrpName());
                    //                    tSchema.setConfMakeDate(confDate);
                    //                    tSchema.setConfMakeTime(confTime);
                    tSchema.setConfFlag("1");
                    tSchema.setModifyDate(PubFun.getCurrentDate());
                    tSchema.setModifyTime(PubFun.getCurrentTime());

                }
                tmpMap.put(tLJTempFeeClassSet, "UPDATE");
            }
        }
        tmpMap.put(PubFun.copySchemaSet(tmpFeeSet), "UPDATE");
        return tmpMap;
    }

    /**
     * 更新集体合同号
     *
     * @param grpPolSet LCGrpPolSet
     * @param newGrpContNo String
     * @param tPayNo String
     * @return MMap
     */
    private MMap prepareNewGrpPolSet(LCGrpPolSet grpPolSet,
            String newGrpContNo, String tPayNo)
    {
        MMap tmpmap = new MMap();
        //* ↓ *** liuhao *** 2005-05-13 *** add **********
        //险种序号
        int iRiskSeqNo = 0;
        //* ↑ *** liuhao *** 2005-05-13 *** add **********

        for (int i = 1; i <= grpPolSet.size(); i++)
        {
            //            String newGrpPolNo = this.getNewPolNo(StrTool.cTrim(grpPolSet.get(i).
            //                                                  getGrpPolNo()));
            //            tmpmap.put("delete from lcgrppol where grppolno='" +
            //                       grpPolSet.get(i).getGrpPolNo() + "'", "DELETE");

            //更新暂交费信息
            MMap APayMap = null;
            if (!CPREVIEW.equals(mPreview))
            {
                APayMap = dealGrpAPay(grpPolSet.get(i), tPayNo, grpPolSet
                        .get(i).getGrpPolNo(), newGrpContNo);
            }

            if (APayMap == null && !CPREVIEW.equals(mPreview))
            {
                buildError("prepareNewGrpPolSet", "非预打保单,查询暂收费失败！");
                return null;
            }
            grpPolSet.get(i).setPayEndDate(maxPayEndDate);

            //从缓存中找改险种下一次交费日期
            grpPolSet.get(i).setPaytoDate(
                    (String) paytoDateMap.get(grpPolSet.get(i).getRiskCode()));

            grpPolSet.get(i).setFirstPayDate(mFirstPayDate);

            //             grpPolSet.get(i).setGrpPolNo(newGrpPolNo);
            //             grpPolSet.get(i).setGrpContNo(newGrpContNo);
            //设置正常保单/预打保单类型
            if (CPREVIEW.equals(mPreview))
            {
                grpPolSet.get(i).setAppFlag("9");
            }
            else
            {
                grpPolSet.get(i).setAppFlag("1");
                grpPolSet.get(i).setStateFlag("1");
                grpPolSet.get(i).setModifyTime(mCurrentTime);
                grpPolSet.get(i).setModifyDate(mCurrentDate);
            }
            grpPolSet.get(i).setState("00019999");

            //* ↓ *** liuhao *** 2005-05-13 *** add **********
            //险种序号加 1
            iRiskSeqNo = iRiskSeqNo + 1;
            grpPolSet.get(i).setRiskSeqNo(
                    ("" + (1000 + iRiskSeqNo)).substring(2, 4));
            //* ↑ *** liuhao *** 2005-05-13 *** add **********

            tmpmap.add(APayMap);
        }
        tmpmap.put(grpPolSet, "UPDATE");
        return tmpmap;
    }

    /**
     * 均匀集体单保费,因为前面已经有不足保费条件的过滤，因此这里的暂交费总和肯定不小于应交保费
     * @param tLCGrpContSchema LCGrpContSchema
     * @param moreGrpPolSet LCGrpPolSet
     * @param lackGrpPolSet LCGrpPolSet
     * @param inNewGrpContNo String
     * @return MMap
     */
    private MMap balanceFee(LCGrpContSchema tLCGrpContSchema,
            LCGrpPolSet moreGrpPolSet, LCGrpPolSet lackGrpPolSet,
            String inNewGrpContNo)
    {
        MMap tmpMap = new MMap();
        if (lackGrpPolSet.size() <= 0)
        {
            System.out.println("没有少交保费的保单");
        }
        if (moreGrpPolSet.size() <= 0)
        {
            System.out.println("没有保费多余的保单");
        }
        int redundantPos = 1;
        int lackPos = 1;
        double left = 0.000;
        LCGrpPolSchema lackGrpPol = null;
        LCGrpPolSchema redundantGrpPolSchema = null;
        if (lackGrpPolSet.size() > 0 && moreGrpPolSet.size() > 0)
        {
            //     String tPaySerialNo = PubFun1.CreateMaxNo("PAYNO",PubFun.getNoLimit(mGlobalInput.ComCode));

            double adjustMoney = 0.000; //用以调配的money

            //以富余补不足
            while (lackPos <= lackGrpPolSet.size()
                    && redundantPos <= moreGrpPolSet.size())
            {
                if (left >= 0.0)
                {
                    lackGrpPol = lackGrpPolSet.get(lackPos);
                }
                if (left <= 0.0)
                {
                    redundantGrpPolSchema = moreGrpPolSet.get(redundantPos);
                }

                double lackmoney = lackGrpPol.getDif();
                double redundantmoney = redundantGrpPolSchema.getDif();

                left = redundantmoney + lackmoney;
                //System.out.println("adjustMoney:" + adjustMoney);

                if (left >= 0.0)
                { //富余足以加给不足
                    adjustMoney = lackmoney;
                    tmpMap.add(createTempFee(redundantGrpPolSchema, lackGrpPol,
                            adjustMoney, inNewGrpContNo));
                    lackGrpPol.setDif(0);
                    redundantGrpPolSchema.setDif(redundantGrpPolSchema.getDif()
                            + adjustMoney);
                    lackPos++;
                }
                else
                { //富余不够加给不足
                    //把富足的全部分给不足
                    adjustMoney = redundantmoney;
                    tmpMap.add(createTempFee(redundantGrpPolSchema, lackGrpPol,
                            adjustMoney, inNewGrpContNo));
                    redundantGrpPolSchema.setDif(0);
                    lackGrpPol.setDif(lackGrpPol.getDif() + adjustMoney);
                    redundantPos++;
                }

            }
        }
        //还有少交保费的保单,要生成催收?
        if (lackPos < lackGrpPolSet.size())
        {
            System.out.println("还有少交保费的保单,要生成催收");
            return null;
        }

        //还有有保费多余的保单,要生成退费
        if (redundantPos > 0 && redundantPos <= moreGrpPolSet.size())
        {
            // LCGrpPolSet withdrawLCGrpPolSet = new LCGrpPolSet();
            //
            System.out.println("还有有保费多余的保单,要生成退费");
            double SumDiff = 0.000;
            for (int t = redundantPos; t <= moreGrpPolSet.size(); t++)
            {
                redundantGrpPolSchema = moreGrpPolSet.get(t);
                SumDiff += redundantGrpPolSchema.getDif();
                redundantGrpPolSchema.setDif(0);
                //                withdrawLCGrpPolSet.add(redundantGrpPolSchema);
                //                NewGrpPolFeeWithdrawBL tNewGrpPolFeeWithdrawBL = new
                //                        NewGrpPolFeeWithdrawBL();
                //                VData inputData = new VData();
                //                inputData.add(withdrawLCGrpPolSet);
                //                inputData.add(this.mGlobalInput);
                //                VData withdraw = tNewGrpPolFeeWithdrawBL.submitDataAllNew(
                //                        inputData);
                //                if (withdraw == null)
                //                {
                //                    CError.buildErr(this, "暂交费退费发生错误!");
                //                    return null;
                //                }
                //
                //                LJAGetSet getSet = (LJAGetSet) withdraw.getObjectByObjectName(
                //                        "LJAGetSet", 0);
                //                this.replaceOtherNo(getSet, inNewGrpContNo);
                //                tmpMap.put(getSet, "INSERT");
                //                LJAGetOtherSet getOtherSet = (LJAGetOtherSet) withdraw.
                //                                             getObjectByObjectName(
                //                        "LJAGetOtherSet", 0);
                //                this.replaceOtherNo(getOtherSet, inNewGrpContNo);
                //                tmpMap.put(getOtherSet, "INSERT");
                //                LOPRTManagerSet tLOPRTManagerSet = (LOPRTManagerSet) withdraw.
                //                        getObjectByObjectName("LOPRTManagerSet", 0);
                //                tmpMap.put(tLOPRTManagerSet, "INSERT");

            }
            tLCGrpContSchema.setDif(SumDiff);
        }

        return tmpMap;
    }

    private void replaceOtherNo(LJAGetOtherSet otherSet, String newOtherNo)
    {
        for (int i = 1; i <= otherSet.size(); i++)
        {
            otherSet.get(i).setOtherNo(newOtherNo);
            // otherSet.get(i).setConfDate( null );

        }

    }

    private void replaceOtherNo(LJAGetSet agetSet, String newOtherNo)
    {
        for (int i = 1; i <= agetSet.size(); i++)
        {
            agetSet.get(i).setOtherNo(newOtherNo);
            // agetSet.get(i).setConfDate( mCurrentDate );
            agetSet.get(i).setShouldDate(mCurrentDate);
        }

    }

    /**
     * 根据保单生成一张新的暂交费表
     *
     * @param grpmorePolSchema LCPolSchema
     * @param grplackPolSchema LCGrpPolSchema
     * @param money double
     * @param inNewGrpContNo String
     * @return MMap
     */
    private MMap createTempFee(LCGrpPolSchema grpmorePolSchema,
            LCGrpPolSchema grplackPolSchema, double money, String inNewGrpContNo)
    {
        MMap tmpMap = new MMap();
        LJTempFeeSchema srcTempFeeSchema = (LJTempFeeSchema) this
                .getTempFee(grpmorePolSchema.getRiskCode());
        if (srcTempFeeSchema == null)
        {
            CError.buildErr(this, "没有查找到可用于借的暂交费");
            return null;
        }
        String confDate = mCurrentDate;
        String confTime = mCurrentTime;
        PubFun.setPrecision(money, "0.00");
        if (money < 0)
        {
            money = -money;
        }

        //创建借的科目
        LJTempFeeSchema tjieSchema = new LJTempFeeSchema();
        tjieSchema.setSchema(srcTempFeeSchema);
        tjieSchema.setTempFeeType("9");
        tjieSchema.setRiskCode(grplackPolSchema.getRiskCode());
        tjieSchema.setPayMoney(money);
        tjieSchema.setMakeDate(confDate);
        tjieSchema.setMakeTime(confTime);
        tjieSchema.setModifyDate(PubFun.getCurrentDate());
        tjieSchema.setModifyTime(PubFun.getCurrentTime());

        //创建贷的科目
        LJTempFeeSchema tdaiSchema = new LJTempFeeSchema();
        tdaiSchema.setSchema(srcTempFeeSchema);
        tdaiSchema.setTempFeeType("8");
        tdaiSchema.setRiskCode(grplackPolSchema.getRiskCode());
        tdaiSchema.setPayMoney(money);
        tdaiSchema.setMakeDate(confDate);
        tdaiSchema.setMakeTime(confTime);
        tdaiSchema.setModifyDate(PubFun.getCurrentDate());
        tdaiSchema.setModifyTime(PubFun.getCurrentTime());
        //缓存
        if (this.getTempFee(grplackPolSchema.getRiskCode()) == null)
        {
            this.cacheTempFee(grplackPolSchema.getRiskCode(), tdaiSchema);
        }
        tmpMap.put(tjieSchema, "INSERT");
        tmpMap.put(tdaiSchema, "INSERT");
        return tmpMap;
    }

    private LCGrpPolSchema queryGrpPol(String grpPolNo)
    {
        LCGrpPolDB grpPolDB = new LCGrpPolDB();
        grpPolDB.setGrpPolNo(grpPolNo);
        if (!grpPolDB.getInfo())
        {
            return null;
        }
        return grpPolDB.getSchema();
    }

    /**
     * 查询暂交费
     * @param prtNo String
     * @param riskcode String
     * @return LJTempFeeSet
     */
    private LJTempFeeSet queryTempFeeSet(String prtNo, String riskcode)
    {
        LJTempFeeSet tLJTempFeeSet;
        LJTempFeeSchema tLJTempFeeSchema = null;
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setOtherNo(prtNo);

        // 暂缴表中LJTempFee.OtherNoType字段值“5”表示团体保单印刷号。
        tLJTempFeeSchema.setOtherNoType("5");
        // -------------------------------------

        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setRiskCode(riskcode);
        tLJTempFeeDB.setSchema(tLJTempFeeSchema);
        tLJTempFeeSet = tLJTempFeeDB.query();
        if (tLJTempFeeDB.mErrors.needDealError())
        {
            CError.buildErr(this, "queryTempFee", tLJTempFeeDB.mErrors);
            System.out.println("errrrrrrrrrr:"
                    + tLJTempFeeDB.mErrors.getErrContent());
            return null;
        }
        return tLJTempFeeSet;
    }

    /**
     * 暂交费交费求和
     * @param tmpFeeSet LJTempFeeSet
     * @return double
     */
    private double sumPayMoney(LJTempFeeSet tmpFeeSet)
    {
        double sumPrem = 0.000;
        for (int i = 1; i <= tmpFeeSet.size(); i++)
        {
            sumPrem += tmpFeeSet.get(i).getPayMoney();
        }
        return sumPrem;

    }

    /**
     * 更新团单合同换号
     * @param inNewGrpContNo String
     * @param oldGrpContNo String
     * @return MMap
     */
    private MMap prepareOtherUpdateGrpCont(String inNewGrpContNo,
            String oldGrpContNo)
    {
        String condition = " GrpContNo='" + inNewGrpContNo + "'";
        String wherepart = " grpcontno='" + oldGrpContNo + "'";
        //涉及到团单合同号的表
        //* ↓ *** LiuHao *** 2005-05-26 *** modify *******************
        /*
         String[] grpCont =
         { "LCGrpCont","LCCont","LCGrpAppnt", "LCContPlan",
         "LCContPlanRisk", "LCGeneral",
         "LCInsured", "LCCustomerImpart",
         "LCCustomerImpartParams", "LCGet", "LCPrem"
         , "LCUWError", "LCUWSub", "LCUWMaster", "LCCSpec",
         "LCSpec","LCGCUWMaster","LCCUWMaster"
         , "LCCUWSub", "LCCUWError", "LCCUWMaster",
         "LCRReport", "LCNotePad", "LCUWReport"
         , "LCIssuePol", "LCGrpIssuePol", "LCPENoticeItem",
         "LCPENotice", "LCInsureAccClass"};
         */
        String[] grpCont = { "LCGrpCont", "LCCont", "LCGrpAppnt", "LCContPlan",
                "LCContPlanRisk", "LCGeneral", "LCInsured", "LCCustomerImpart",
                "LCCustomerImpartParams", "LCGet", "LCPrem", "LCUWError",
                "LCUWSub", "LCUWMaster", "LCSpec", "LCGCUWMaster",
                "LCCUWMaster", "LCCUWSub", "LCCUWError", "LCCUWMaster",
                "LCRReport", "LCNotePad", "LCUWReport", "LCIssuePol",
                "LCGrpIssuePol", "LCPENoticeItem", "LCPENotice",
                "LCInsureAccClass", "LCGCUWSub", "LCGCUWError",
                "LCInsuredList", "LCNation", "LCGrpBalPlan",
                "LCInsuredListPol", "LCIGrpCont", "LCCoInsuranceParam",
                "LICertify", "LICertifyInsured", "LIBCertify" ,"LCGrpposition","LCGrpShareAmnt","LCGrpRiskShareAmnt","LCContPlanDutyDefGrade"};
        //* ↑ *** LiuHao *** 2005-05-26 *** modify *******************F
        Vector tmpVec = PubFun.formUpdateSql(grpCont, condition, wherepart);
        Vector sqlVec = new Vector();
        sqlVec.addAll(tmpVec);

        //        //加个单签单时间
        //        String[] signDateTime = {"LCCont"};
        //        condition = condition +" ,signdate = '" + mCurrentDate +"'"
        //                   + ",signTime = '" + mCurrentTime +"'";
        //        Vector contVec = PubFun.formUpdateSql( signDateTime, condition,wherepart);
        //        sqlVec.addAll( contVec );
        MMap tmpMap = new MMap();
        tmpMap.add(this.getUpdateSql(sqlVec));
        return tmpMap;
    }

    /**
     * 个单合同签单时间校验
     *
     * @param grpcontno String
     * @return MMap
     */
    private MMap prepareUpdateSignDate(String grpcontno, String tGrpULICount)
    {
        Vector sqlVec = new Vector();
        String condition = " signdate = '" + mCurrentDate + "',modifydate='"
                + mCurrentDate + "',modifytime='" + mCurrentTime + "'"
                + ",signTime = '" + mCurrentTime + "'";
        String wherepart = " grpcontno='" + grpcontno + "'";
        //加个单签单时间
        String[] signDateTime = { "LCCont", "LCPol" };

        Vector contVec = PubFun.formUpdateSql(signDateTime, condition,
                wherepart);
        if (!StrTool.cTrim(this.mPreview).equals("PREVIEW"))
        {
            sqlVec.addAll(contVec);
        }
        
        if(tGrpULICount != null && tGrpULICount.equals("0")){        
	        //加账户相关
	        String[] AccTable = { "LCInsureAcc", "LCInsureAccClass",
	                "LCInsureAccFee", "LCInsureAccClassFee" };
	        String conditionAcc = "AccFoundDate='" + mCurrentDate + "'"
	                + ",AccFoundTime='" + mCurrentTime + "'" + ",BalaDate='"
	                + mCurrentDate + "'" + ",BalaTime ='" + mCurrentTime + "'";
	        Vector AccVec = PubFun.formUpdateSql(AccTable, conditionAcc, wherepart);
	        sqlVec.addAll(AccVec);
	        
	        String[] AccTraceTable = { "LCInsureAccTrace" };
	        String conditionTrace = "paydate='" + mCurrentDate + "'";
	        Vector AccVecTrace = PubFun.formUpdateSql(AccTraceTable,
	                conditionTrace, wherepart);
	        sqlVec.addAll(AccVecTrace);
        }

        MMap tmpMap = new MMap();
        tmpMap.add(this.getUpdateSql(sqlVec));
        return tmpMap;

    }

    /**
     * 准备只需要更新团单合同号,集体单合同号的sql
     * @param tLCGrpContSchema LCGrpContSchema
     * @param inNewGrpContNo String
     * @param lpGrpPolNo String
     * @param newGrpPolNo String
     * @return MMap
     */
    private MMap prepareOtherGrpPolSql(LCGrpContSchema tLCGrpContSchema,
            String inNewGrpContNo, String lpGrpPolNo, String newGrpPolNo)
    {

        //更新团体合同,集体险种保单表相关
        String[] GrpPolContTables = { "LCGeneralToRisk", "LCContPlanDutyParam",
                "LCGUWError", "LCGUWSub", "LCGUWMaster", "LCGrpFee",
                "LCGrpFeeParam", "LCPayRuleFactory",
                "LCPayRuleParams",
                //现换号后签单后增加的
                "LCGrpPol", "LCPol", "LCInsureAcc", "LCInsureAccClass",
                "LCInsureAccFee", "LCInsureAccClassFee", "LCInsureAccTrace",
                "LCRiskWrap", "LCGrpInterest", "LCRiskZTFee", "LCAscriptionRuleFactory", "LCAscriptionRuleParams"};
        //        //集体险种保单表相关
        //        String[] GrpPolTables =
        //                {"LCInsureAccTrace"};
        Vector sqlVec = new Vector();

        //其余表的更新sql
        String condition = " GrpContNo='" + inNewGrpContNo + "',GrpPolNo='"
                + newGrpPolNo + "'";
        String wherepart = " grppolno='" + lpGrpPolNo + "'";
        Vector VecContPol = PubFun.formUpdateSql(GrpPolContTables, condition,
                wherepart);
        sqlVec.addAll(VecContPol);

        //        //加个单签单时间
        //        String[] signDateTime =
        //                                {"LCPol"};
        //       String conditionsign = condition + " ,signdate = '" + mCurrentDate + "'"
        //                    + ",signTime = '" + mCurrentTime + "'";
        //        Vector contVec = PubFun.formUpdateSql(signDateTime, conditionsign,
        //                                              wherepart);
        //        sqlVec.addAll(contVec);
        //
        //         //加账户相关
        //         String[] AccTable ={"LCInsureAcc","LCInsureAccClass","LCInsureAccFee","LCInsureAccClassFee"};
        //        String conditionAcc =condition +",AccFoundDate='" + mCurrentDate +"'"
        //                             +",AccFoundTime='" + mCurrentTime+"'"
        //                             +",BalaDate='"+ mCurrentDate +"'"
        //                            +",BalaTime ='"  + mCurrentTime +"'"
        //                         ;
        //        Vector AccVec = PubFun.formUpdateSql(AccTable, conditionAcc,
        //                                              wherepart);
        //         sqlVec.addAll( AccVec );
        //         String[] AccTraceTable={"LCInsureAccTrace"};
        //         String conditionTrace = condition +",paydate='"+ mCurrentDate +"'";
        //         Vector AccVecTrace = PubFun.formUpdateSql(AccTraceTable, conditionTrace,
        //                                              wherepart);
        //         sqlVec.addAll( AccVecTrace );

        //  sqlVec.add(sql1 );

        return getUpdateSql(sqlVec);
    }

    /**
     * sql转换
     * @param sql Vector
     * @return MMap
     */
    private MMap getUpdateSql(Vector sql)
    {
        MMap tmpMap = new MMap();
        for (int i = 0; i < sql.size(); i++)
        {
            tmpMap.put((String) sql.get(i), "INSERT");
        }
        return tmpMap;

    }

    //    /**
    //     * 获取暂交费的更新信息
    //     * 输出：如果发生错误则返回false,否则返回true
    //     */
    //    private MMap dealTempFee(LCGrpPolSchema tLCGrpPolSchema,
    //                             String tNewGrpContNo)
    //    {
    //
    //        MMap tmpMap = new MMap();
    //        VData tReturn = new VData();
    //        String tOldGrpPolNo = tLCGrpPolSchema.getGrpContNo();
    //
    //        String confDate = PubFun.getCurrentDate();
    //        String confTime = PubFun.getCurrentTime();
    //
    //        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    //        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    //
    //   // if (mPolType.equals("3"))
    //   // { // 合同下的集体的投保单
    //   //     tReturn.add(tLJTempFeeSet);
    //   //     tReturn.add(tLJTempFeeClassSet);
    //   // }
    //   // if (mPolType.equals("2"))
    //   // {
    //        // 暂交费信息
    //        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
    //        tLJTempFeeDB.setOtherNo(tOldGrpPolNo);
    //        tLJTempFeeDB.setOtherNoType("1");
    //        tLJTempFeeDB.setTempFeeType("1");
    //        tLJTempFeeDB.setConfFlag("0");
    //
    //        tLJTempFeeSet = tLJTempFeeDB.query();
    //        if (tLJTempFeeDB.mErrors.needDealError())
    //        {
    //            CError.buildErr(this, "LJTempFee表取数失败!");
    //            return null;
    //        }
    //        //houzm增加的部分：将暂加费纪录类型为银行扣款的也查询出来
    //        LJTempFeeDB tempLJTempFeeDB = new LJTempFeeDB();
    //        tempLJTempFeeDB.setOtherNo(tLCGrpPolSchema.getPrtNo()); //印刷号
    //        tempLJTempFeeDB.setOtherNoType("4"); //其它号码类型为印刷号
    //        //tempLJTempFeeDB.setTempFeeType( "5" ); //暂交费类型为银行扣款
    //        tempLJTempFeeDB.setRiskCode(tLCGrpPolSchema.getRiskCode());
    //        tempLJTempFeeDB.setConfFlag("0"); //核销标记为假
    //        LJTempFeeSet tempLJTempFeeSet = tempLJTempFeeDB.query();
    //        if (tempLJTempFeeDB.mErrors.needDealError())
    //        {
    //            CError.buildErr(this, "财务信息取数失败表取数失败!");
    //            return null;
    //
    //        }
    //        tLJTempFeeSet.add(tempLJTempFeeSet); //将后续查询到的纪录存放到set
    //        tmpMap.put(PubFun.copySchemaSet(tLJTempFeeSet), "DELETE");
    //        //以上为houzm添加
    //        int n = tLJTempFeeSet.size();
    //        for (int i = 1; i <= n; i++)
    //        {
    //            LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i);
    //
    //            LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
    //            tLJTempFeeClassDB.setTempFeeNo(tLJTempFeeSchema.getTempFeeNo());
    //            tLJTempFeeClassSet = tLJTempFeeClassDB.query();
    //            if (tLJTempFeeClassDB.mErrors.needDealError())
    //            {
    //                CError.buildErr(this, "LJTempFeeClass表取数失败!");
    //                continue;
    //            } // end of if
    //
    //            int m = tLJTempFeeClassSet.size();
    //            if (m > 0)
    //            {
    //                tmpMap.put(PubFun.copySchemaSet(tLJTempFeeClassSet), "DELETE");
    //                for (int j = 1; j <= m; j++)
    //                {
    //                    LJTempFeeClassSchema tLJTempFeeClassSchema =
    //                            tLJTempFeeClassSet.get(j);
    //                    tLJTempFeeClassSchema.setConfDate(confDate);
    //                    tLJTempFeeClassSchema.setConfFlag("1");
    //                    tLJTempFeeClassSchema.setModifyDate(confDate);
    //                    tLJTempFeeClassSchema.setModifyTime(confTime);
    //
    //                    tLJTempFeeClassSet.set(j, tLJTempFeeClassSchema);
    //                } // end of for
    //
    //                tLJTempFeeSchema.setOtherNo(tNewGrpContNo);
    //                tLJTempFeeSchema.setOtherNoType("1");
    //                tLJTempFeeSchema.setConfDate(confDate);
    //                tLJTempFeeSchema.setConfFlag("1");
    //                tLJTempFeeSchema.setModifyDate(confDate);
    //                tLJTempFeeSchema.setModifyTime(confTime);
    //
    //                tLJTempFeeSet.set(i, tLJTempFeeSchema);
    //
    //            }
    //
    //        } // end of for
    //        // } // end of if
    //        tmpMap.put(tLJTempFeeSet, "INSERT");
    //
    //        return tmpMap;
    //    }

    /**
     * 处理集体保单财务实交信息
     *
     * @param tLCGrpPolSchema LCGrpContSchema
     * @param tPaySerialNo String
     * @param newGrpPolNo String
     * @param inNewGrpContNo String
     * @return MMap
     */
    private MMap dealGrpAPay(LCGrpPolSchema tLCGrpPolSchema,
            String tPaySerialNo, String newGrpPolNo, String inNewGrpContNo)
    {

        MMap tmpMap = new MMap();
        String confDate = mCurrentDate;
        String confTime = mCurrentTime;
        LCPremSet tPremSet = new LCPremSet();
        LCPolSet tPolSet = new LCPolSet();
        LCPolSchema tPolSchema = null;
        LCPremDB tPremDB = new LCPremDB();
        LJAPayGrpSchema PayGrpSchema = null;

        double tSumPrem = 0.00;

        FDate fdate = new FDate();
        Date tdate;
        
        LJTempFeeSchema tempFeeSchema = (LJTempFeeSchema) this
        .getTempFee(tLCGrpPolSchema.getRiskCode());
        if (tempFeeSchema == null)
        {
            CError.buildErr(this, "没有暂交费信息!");
            return null;
        }
        
        String querySQL = "select * from lcpol where grppolno = '" 
            + tLCGrpPolSchema.getGrpPolNo() + "' and AppFlag = '1' ";
        
        RSWrapper tRSWrapper = new RSWrapper();
        tRSWrapper.prepareData(tPolSet, querySQL);
        try
        {
            do
            {
                tRSWrapper.getData();
                
                if (tPolSet == null)
                {
                    return null;
                }
                
                LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
                
                for (int j = 1; j <= tPolSet.size(); j++)
                {
                    tPolSchema = tPolSet.get(j);
                    //缓存下一次交费日期
                    paytoDateMap.put(tPolSchema.getRiskCode(), tPolSchema
                            .getPaytoDate());
                    //实收信息,初始化基本信息
                    tPolSchema = tPolSet.get(j);
                    //个人保单保费项查询
                    tPremDB.setPolNo(tPolSchema.getPolNo());
                    tPremSet = tPremDB.query();
                    if (tPremSet == null)
                    {
                        continue;
                    }
                    tdate = fdate.getDate(tPolSchema.getPayEndDate());
                    if (maxPayEndDate == null || maxPayEndDate.before(tdate))
                    {
                        maxPayEndDate = tdate;
                    }
                    int m = tPremSet.size();
                    if (m > 0)
                    {
                        for (int k = 1; k <= m; k++)
                        {
                            LCPremSchema tLCPremSchema = tPremSet.get(k);
                            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                            tLJAPayPersonSchema.setPolNo(tPolSchema.getPolNo());
                            tLJAPayPersonSchema.setPayCount(1);
                            tLJAPayPersonSchema.setGrpPolNo(newGrpPolNo);
                            tLJAPayPersonSchema.setContNo(tPolSchema.getContNo());
                            tLJAPayPersonSchema.setGrpContNo(inNewGrpContNo);
                            tLJAPayPersonSchema.setAppntNo(tPolSchema.getAppntNo());
                            tLJAPayPersonSchema.setPayNo(tPaySerialNo);
                            tLJAPayPersonSchema.setPayAimClass("1");
                            tLJAPayPersonSchema
                                    .setDutyCode(tLCPremSchema.getDutyCode());
                            tLJAPayPersonSchema.setPayPlanCode(tLCPremSchema
                                    .getPayPlanCode());
                            tLJAPayPersonSchema.setSumDuePayMoney(tLCPremSchema
                                    .getPrem());
                            tLJAPayPersonSchema.setSumActuPayMoney(tLCPremSchema
                                    .getPrem());
                            tLJAPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());
                            tLJAPayPersonSchema.setCurPayToDate(tLCPremSchema
                                    .getPaytoDate());
                            tLJAPayPersonSchema.setPayDate(tempFeeSchema.getPayDate());
                            tLJAPayPersonSchema.setPayType("ZC");
                            tLJAPayPersonSchema.setEnterAccDate(tempFeeSchema
                                    .getEnterAccDate());
                            tLJAPayPersonSchema.setConfDate(confDate);
                            tLJAPayPersonSchema.setLastPayToDate(this.mLCGrpContSchema
                                    .getCValiDate());
                            tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
                            tLJAPayPersonSchema.setApproveCode(mGlobalInput.Operator);
                            tLJAPayPersonSchema.setApproveDate(confDate);
                            tLJAPayPersonSchema.setApproveTime(confTime);
                            tLJAPayPersonSchema.setMakeDate(confDate);
                            tLJAPayPersonSchema.setMakeTime(confTime);
                            tLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
                            tLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());
                            tLJAPayPersonSchema.setManageCom(tPolSchema.getManageCom());
                            tLJAPayPersonSchema.setAgentCom(tPolSchema.getAgentCom());
                            tLJAPayPersonSchema.setAgentType(tPolSchema.getAgentType());
                            tLJAPayPersonSchema.setRiskCode(tPolSchema.getRiskCode());
                            tLJAPayPersonSchema.setAgentCode(tPolSchema.getAgentCode());
                            tLJAPayPersonSchema.setAgentGroup(tPolSchema
                                    .getAgentGroup());
        
                            //取得总应交
                            tSumPrem += tLCPremSchema.getPrem();
        
                            tLJAPayPersonSet.add(tLJAPayPersonSchema);
                        }
                    }
                    //集体保单实交信息
                    //            fillLJAPayGrp(PayGrpSchema, tPremSet);
                }
                tmpMap.put(tLJAPayPersonSet, SysConst.INSERT);
            }
            while(tPolSet.size() > 0);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                tRSWrapper.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

        LJAPayGrpSet PayGrpSet = new LJAPayGrpSet();
        /**　下面的操作就是为了填充ｌｊａｐａｙｇｒｐ，ｌｊａｐａｙｐｅｒｓｏｎ */
        if (!dealLJAPayGrp(PayGrpSet, tLCGrpPolSchema, tPaySerialNo))
        {
            return null;
        }

        //进行溢交处理,按险种处理
        if (PubFun.setPrecision(tSumPrem, "0.00") < tempFeeSchema.getPayMoney())
        {
            PayGrpSchema = new LJAPayGrpSchema();
            PayGrpSchema.setGrpPolNo(newGrpPolNo);
            PayGrpSchema.setRiskCode(tPolSchema.getRiskCode());
            //设置实交，溢交的部分
            PayGrpSchema.setSumActuPayMoney(PubFun.setPrecision((tempFeeSchema
                    .getPayMoney() - tSumPrem), "0.00"));
            PayGrpSchema.setSumDuePayMoney(PubFun
                    .setPrecision(tSumPrem, "0.00"));
            PayGrpSchema.setPayType("YET");
            PayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            PayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
            PayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
            PayGrpSchema.setAppntNo(tLCGrpPolSchema.getCustomerNo());
            PayGrpSchema.setApproveCode(mGlobalInput.Operator);
            PayGrpSchema.setApproveDate(confDate);
            PayGrpSchema.setPayCount(1);
            PayGrpSchema.setPayNo(tPaySerialNo);
            PayGrpSchema.setPayDate(tempFeeSchema.getPayDate());
            PayGrpSchema.setEnterAccDate(tempFeeSchema.getEnterAccDate());
            PayGrpSchema.setLastPayToDate(mLCGrpContSchema.getCValiDate());
            //下一次交费日期
            PayGrpSchema.setCurPayToDate(tPolSchema.getPaytoDate());
            PayGrpSchema.setApproveTime(confTime);
            PayGrpSchema.setConfDate(confDate);
            PayGrpSchema.setGrpContNo(inNewGrpContNo);
            PayGrpSchema.setMakeDate(confDate);
            PayGrpSchema.setMakeTime(confTime);
            PayGrpSchema.setModifyDate(PubFun.getCurrentDate());
            PayGrpSchema.setModifyTime(PubFun.getCurrentTime());
            PayGrpSchema.setOperator(mGlobalInput.Operator);
            PayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            PayGrpSchema.setAgentCode(tPolSchema.getAgentCode());
            PayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            PayGrpSet.add(PayGrpSchema);
        }
        //   }
        tmpMap.put(PayGrpSet, "INSERT");
        //更新japaygrp
        //        String sql ="update ljapaygrp set SumDuePayMoney=(select sum(t.SumDuePayMoney) "
        //                    +" from ljapayperson t where t.payno= ljapaygrp.payno and t.GrpPolNo=ljapaygrp.GrpPolNo)"
        //                    +" where payno='"+ tPaySerialNo +"'";
        //        String sql1 ="update ljapaygrp set SumActuPayMoney=(select sum(t.SumActuPayMoney) "
        //                   +" from ljapayperson t where t.payno= ljapaygrp.payno and t.GrpPolNo=ljapaygrp.GrpPolNo)"
        //                   +" where payno='"+ tPaySerialNo +"'";
        //        tmpMap.put(sql,"UPDATE");
        //        tmpMap.put(sql1,"UPDATE");
        return tmpMap;
    }

    /**
     * dealLJAPayGrp
     *
     * @param tLJAPayGrpSet LJAPayGrpSet
     * @param tLCGrpPolSchema LCGrpPolSchema
     * @param tPayNo String
     * @return boolean
     */
    private boolean dealLJAPayGrp(LJAPayGrpSet tLJAPayGrpSet,
            LCGrpPolSchema tLCGrpPolSchema, String tPayNo)
    {
        LJTempFeeSchema tLJTempFeeSchema = (LJTempFeeSchema) this.mLJTempFeeMap
                .get(tLCGrpPolSchema.getRiskCode());

        LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
        tLJAPayGrpSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
        tLJAPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
        tLJAPayGrpSchema.setSumActuPayMoney(tLCGrpPolSchema.getPrem());
        tLJAPayGrpSchema.setSumDuePayMoney(tLCGrpPolSchema.getPrem());
        tLJAPayGrpSchema.setPayType("ZC");
        tLJAPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
        tLJAPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
        tLJAPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
        tLJAPayGrpSchema.setAppntNo(tLCGrpPolSchema.getCustomerNo());
        tLJAPayGrpSchema.setApproveCode(mGlobalInput.Operator);
        tLJAPayGrpSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
        tLJAPayGrpSchema.setPayCount(1);
        tLJAPayGrpSchema.setPayNo(tPayNo);
        tLJAPayGrpSchema.setPayDate(tLJTempFeeSchema.getPayDate());
        tLJAPayGrpSchema.setEnterAccDate(tLJTempFeeSchema.getEnterAccDate());
        tLJAPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.getCValiDate());
        //下一次交费日期
        tLJAPayGrpSchema.setCurPayToDate((String) paytoDateMap
                .get(tLCGrpPolSchema.getRiskCode()));
        tLJAPayGrpSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
        tLJAPayGrpSchema.setConfDate(PubFun.getCurrentDate());
        tLJAPayGrpSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
        tLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
        tLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
        tLJAPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
        tLJAPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
        tLJAPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
        tLJAPayGrpSet.add(tLJAPayGrpSchema);
        return true;
    }

    /**
     * 总实收
     *
     * @param tLCGrpContSchema LCGrpContSchema
     * @param newGrpContNo String
     * @param tPayNo String
     * @return MMap
     */
    private MMap prepareLJAPAY(LCGrpContSchema tLCGrpContSchema,
            String newGrpContNo, String tPayNo)
    {
        String confDate = mCurrentDate;
        String confTime = mCurrentTime;
        MMap tmpMap = new MMap();
        // 生成流水号
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        tLJAPaySchema.setPayNo(tPayNo);
        tLJAPaySchema.setIncomeNo(newGrpContNo);
        tLJAPaySchema.setIncomeType("1");
        tLJAPaySchema.setAppntNo(tLCGrpContSchema.getAppntNo());
        //* ↓ *** LiuHao *** 2005-06-01 *** modify *******************
        //实交总表里总保费以暂收总保费为准
        //tLJAPaySchema.setSumActuPayMoney(sumGrpContPrem);
        tLJAPaySchema.setSumActuPayMoney(mSumTmpPay);
        //* ↑ *** LiuHao *** 2005-06-01 *** modify *******************
        tLJAPaySchema.setPayDate(maxPayDate);
        tLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());
        tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
        tLJAPaySchema.setApproveCode(mGlobalInput.Operator);
        tLJAPaySchema.setApproveDate(confDate);
        tLJAPaySchema.setSerialNo(tPayNo);
        tLJAPaySchema.setOperator(mGlobalInput.Operator);
        tLJAPaySchema.setMakeDate(confDate);
        tLJAPaySchema.setMakeTime(confTime);
        tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
        tLJAPaySchema.setStartPayDate(mFirstPayDate);
        tLJAPaySchema.setManageCom(tLCGrpContSchema.getManageCom());
        tLJAPaySchema.setAgentCom(tLCGrpContSchema.getAgentCom());
        tLJAPaySchema.setAgentType(tLCGrpContSchema.getAgentType());
        tLJAPaySchema.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLJAPaySchema.setAgentGroup(tLCGrpContSchema.getAgentGroup());
        tLJAPaySchema.setAgentType(tLCGrpContSchema.getAgentType());
        tLJAPaySchema.setBankAccNo(tLCGrpContSchema.getBankAccNo());
        tLJAPaySchema.setBankCode(tLCGrpContSchema.getBankCode());
        tLJAPaySchema.setAccName(tLCGrpContSchema.getAccName());
//        tLJAPaySchema.setPayTypeFlag(tLCGrpContSchema.getPayMode()); //当网销业务时，网销paymode为12银行转账，但PayTypeFlag字段长度为1，与财务确认，此字段没有用到，故注释 by gzh
        tLJAPaySchema.setSaleChnl(tLCGrpContSchema.getSaleChnl());
        tLJAPaySchema.setMarketType(tLCGrpContSchema.getMarketType());

        // 契约新单核销实收标志。契约标志：“0”
        tLJAPaySchema.setDueFeeType("0");
        // ------------------------------

        tmpMap.put(tLJAPaySchema, "INSERT");
        //求收费总额
        //        String updatesql ="update ljapay set SumActuPayMoney=(select sum(t.SumActuPayMoney) from ljapayperson t where t.payno=ljapay.payno)"
        //        +" where payno='"+ tPayNo+ "'";
        //        tmpMap.put( updatesql, "UPDATE");
        return tmpMap;
    }

    /**
     * 根据传入保费项，求集体保单总应交和总实交
     *
     * @param tPayGrpSchema LJAPayGrpSchema
     * @param tPremSet LCPremSet
     */
    private void fillLJAPayGrp(LJAPayGrpSchema tPayGrpSchema, LCPremSet tPremSet)
    {
        //求保费
        double sumPrem = 0.00;
        double sumStandPrem = 0.00;
        LCPremSchema tPremSchema = null;
        for (int t = 1; t <= tPremSet.size(); t++)
        {
            tPremSchema = tPremSet.get(t);
            sumPrem += tPremSchema.getPrem();
            sumStandPrem += tPremSchema.getStandPrem();
        }
        tPayGrpSchema.setSumActuPayMoney(PubFun.setPrecision(tPayGrpSchema
                .getSumActuPayMoney()
                + mSumTmpPay, "0.00"));
        tPayGrpSchema.setSumDuePayMoney(PubFun.setPrecision(tPayGrpSchema
                .getSumDuePayMoney()
                + sumStandPrem, "0.00"));

    }

    /**
     * 执行
     *
     * @param conn Connection
     * @param sql String
     * @return boolean
     */
    private double execSumQuery(Connection conn, String sql)
    {
        PreparedStatement st = null;
        //   Connection conn = null;
        ResultSet rs = null;
        try
        {
            // conn = DBConnPool.getConnection();
            if (conn == null)
            {
                return 0;
            }
            st = conn.prepareStatement(sql);
            if (st == null)
            {
                return 0;
            }
            rs = st.executeQuery();
            if (rs.next())
            {
                return rs.getDouble(1);
            }
            return 0;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return 0;
        }
        finally
        {
            try
            {
                st.close();
                rs.close();
            }
            catch (Exception e)
            {
            }

        }
    }

    /**
     * 签单校验
     * @param tLCGrpContSchema LCGrpContSchema
     * @return boolean
     */
    private boolean checkGrpCont(LCGrpContSchema tLCGrpContSchema)
    {

        //复核是否通过
        if (!"4".equals(tLCGrpContSchema.getApproveFlag())
                && !"9".equals(tLCGrpContSchema.getApproveFlag()))
        {
            CError.buildErr(this, "团单合同(" + tLCGrpContSchema.getGrpContNo()
                    + ")未复核通过;");
            return false;
        }
        //核保是否通过
        if (!"4".equals(tLCGrpContSchema.getUWFlag())
                && !"9".equals(tLCGrpContSchema.getUWFlag()))
        {
            CError.buildErr(this, "团单合同(" + tLCGrpContSchema.getGrpContNo()
                    + ")未核保通过;");
            return false;
        }
        
      //校验险种是否停售
        String sqlts = "select code from ldcode where codetype='RiskStopSale' and code in (select distinct riskcode from lcgrppol where prtno='"+tLCGrpContSchema.getPrtNo()+"') ";
        SSRS tSSRSts = new ExeSQL().execSQL(sqlts);
        String risklist = "";
        if(tSSRSts.getMaxRow()>0){
        	for(int i=1;i<=tSSRSts.getMaxRow();i++){
        		if("".equals(risklist)){
        			risklist = risklist + tSSRSts.GetText(i, 1);
        		}else{
        			risklist = risklist + "," + tSSRSts.GetText(i, 1);
        		}
        	}
            System.out
                    .println("险种 " + risklist + " 已停售！");
            CError.buildErr(this, "险种 " + risklist
                    + " 已停售！");
            return false;
        }

        //校验分公司是否停售万能险种
        String universalSaleSQL = "select * from LDCode where CodeType = 'UniversalSale' "
                + "and Code = '"
                + tLCGrpContSchema.getManageCom().substring(0, 4)
                + "' "
                + "and exists (select 1 from LMRiskApp where RiskCode in (select distinct riskcode from lcgrppol where prtno='"+tLCGrpContSchema.getPrtNo()+"') and RiskType4 = '4')";
        SSRS tSSRSwnts = new ExeSQL().execSQL(universalSaleSQL);
        if (tSSRSwnts.getMaxRow()>0)
        {
            System.out.println("分公司 " + tLCGrpContSchema.getManageCom()
                    + " 万能险已停售！");
            CError.buildErr(this, "分公司 " + tLCGrpContSchema.getManageCom()
                    + " 万能险已停售！");
            return false;
        }
        
        //处理被保人数一直不一致的问题
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        SSRS tSSRS1 = new SSRS();
        SSRS tSSRS2 = new SSRS();
        MMap PeoplesMMap = new MMap();
        tSSRS = tExeSQL
                .execSQL("select count(insuredno) from lccont where grpcontno='"
                        + tLCGrpContSchema.getGrpContNo()
                        + "'  and  insuredname not in ('公共账户','无名单') with ur");
        tSSRS1 = tExeSQL
                .execSQL("select count(insuredno) from lcinsured where grpcontno='"
                        + tLCGrpContSchema.getGrpContNo()
                        + "'  and  name not in ('公共账户','无名单') and relationtomaininsured='00'  with ur ");
        if (!tSSRS.GetText(1, 1).equals(tSSRS1.GetText(1, 1)))
        {
            buildError("checkdate", "取被保人数失败!");
            return false;
        }
        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS2 = tExeSQL
                .execSQL("select count(1) from lcinsured where grpcontno='"
                        + tLCGrpContSchema.getGrpContNo()
                        + "'  and  name in ('无名单') ");
        System.out.println("tSSRS2.GetText(1,1)" + tSSRS2.GetText(1, 1));
        if (StrTool.cTrim(tSSRS2.GetText(1, 1)).equals("0"))
        {
            tSSRS = tExeSQL
                    .execSQL("select sum(peoples) from lccont where grpcontno='"
                            + tLCGrpContSchema.getGrpContNo()
                            + "'  and  insuredname not in ('公共账户','无名单')  with ur");
            tSSRS1 = tExeSQL
                    .execSQL("select count(insuredno) from lcinsured where grpcontno='"
                            + tLCGrpContSchema.getGrpContNo()
                            + "'  and  name not in ('公共账户','无名单')  with ur ");
            if (!(tSSRS1.GetText(1, 1).equals(tSSRS.GetText(1, 1))))
            {
                buildError("checkdate", "取团体被保人数失败!");
                return false;
            }
            else if (!(String.valueOf(tLCGrpContSchema.getPeoples2())
                    .equals(tSSRS1.GetText(1, 1))))
            {
                PeoplesMMap.put("update lcgrpcont set peoples2="
                        + tSSRS1.GetText(1, 1) + " where grpcontno='"
                        + tLCGrpContSchema.getGrpContNo() + "' ", "UPDATE");
                PubSubmit ps = new PubSubmit();
                VData sd = new VData();
                sd.add(PeoplesMMap);
                if (!ps.submitData(sd, null))
                {
                    CError.buildErr(this, "更新被保人数失败！");
                    return false;
                }

            }

        }
        int Appntnum = 0;
        StringBuffer sql = new StringBuffer();
        int numString = tLCGrpContSchema.getAppntNo().length();
        sql.append(" select max(int(substr(grpcontno," + (numString + 1)
                + "))) from ( ");
        sql.append(" select grpcontno from lcgrpcont where appntno='"
                + tLCGrpContSchema.getAppntNo() + "' and appflag in('1','9') ");
        sql.append(" union ");
        sql.append(" select grpcontno from lbgrpcont where appntno='"
                + tLCGrpContSchema.getAppntNo() + "' ");
        sql.append(" union ");
        sql.append(" select grpcontno from lobgrpcont where appntno='"
                + tLCGrpContSchema.getAppntNo() + "' and appflag in('1','9') ");
        sql.append(" ) as X  ");
        System.out.println("SQL" + sql.toString());
        SSRS mSSRS = (new ExeSQL()).execSQL(sql.toString());
        LDGrpDB tLDGrpDB = new LDGrpDB();
        LDGrpSet tLDGrpSet = new LDGrpSet();
        tLDGrpDB.setCustomerNo(tLCGrpContSchema.getAppntNo());
        tLDGrpSet = tLDGrpDB.query();
        if (tLDGrpSet.size() == 1)
        {
            Appntnum = tLDGrpSet.get(1).getGrpAppntNum();
            System.out.println("Appntnum" + Appntnum);
        }
        else
        {
            buildError("CheckAppntNum", "该客户在客户表中的记录有误！");
            return false;
        }
        int newAppntnum = Integer.parseInt(mSSRS.GetText(1, 1));
        System.out.println("newAppntnum" + newAppntnum);
        tAppntnum = newAppntnum;
        String sql1 = "update LDgrp set GrpAppntNum=" + tAppntnum
                + " where CustomerNo='" + tLCGrpContSchema.getAppntNo() + "'";
        map1.put(sql1, "UPDATE");
        PubSubmit tPubSubmit = new PubSubmit();
        VData tVData = new VData();
        tVData.add(map1);
        if (!tPubSubmit.submitData(tVData, ""))
        {
            buildError("CheckAppntNum", "更新客户信息表出现错误！");
            return false;
        }
        return true;
    }

    /**
     * 团单暂交费查询比较
     * @param tLCGrpContSchema LCGrpContSchema
     * @return int
     */
    private int CheckTempFee(LCGrpContSchema tLCGrpContSchema)
    {
        //暂交费是否交够
        double sumPrem = 0.00;
        double sumTmpFee = 0.00;
        double dRiskPrem = 0.00;
        Connection conn = null;
        try
        {
            conn = DBConnPool.getConnection();
            if (conn == null)
            {
                CError.buildErr(this, "用尽的数据库连接资源,少后再试");
                return this.DB_ERROR;
            }
            String sql0 = "select count(1)  from ljtempfee where TempFeeType in ('0','1') and othernotype='5' and otherno='"
                    + tLCGrpContSchema.getPrtNo()
                    + "'"
                    + " and ConfFlag='0' and EnterAccDate is  null";
            double sumcount = this.execSumQuery(conn, sql0);
            if (sumcount > 0)
            {
                return -1;
            }
            else
            {
                String sql = "select sum(Prem) as sumprem from lcpol where grpcontno='"
                        + tLCGrpContSchema.getGrpContNo()
                        + "' and uwflag in ('4','9')";
                sumPrem = this.execSumQuery(conn, sql);

                sql = " select sum(PayMoney) as sumpay from ljtempfee where TempFeeType in ('0','1') and othernotype='5' and otherno='"
                        + tLCGrpContSchema.getPrtNo() + "'";
                sql += " and ConfFlag='0' and EnterAccDate is not null ";
                sumTmpFee = this.execSumQuery(conn, sql);

                //* ↓ *** LiuHao *** 2005-05-09 *** add *******************
                //查询总保费 如果存在以总保费作验证 如果不存在以下面的操作做验证
                //                 String sqlRisk =
                //                         "select sum(RiskPrem) from LCContPlanRisk where ProposalGrpContNo='" +
                //                         tLCGrpContSchema.getGrpContNo() + "'";
                //                dRiskPrem = this.execSumQuery(conn, sqlRisk);
                //                //如果查询有总保费 则进行总保费的验证
                //                if (dRiskPrem > 0) {
                //                    sumPrem = dRiskPrem;
                //                }
                //总保费验证完毕
                //* ↑ *** LiuHao *** 2005-05-09 *** add *******************
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (Exception ex)
            {
            }
        }
        if (sumTmpFee <= 0.0)
        {
            //CError.buildErr(this,"没有暂交费信息,不能签单");
            return -1;
        }
        if (sumTmpFee < sumPrem)
        {
            //CError.buildErr(this,"暂交费没有交够,不能签单");
            return -1;
        }
        if (sumTmpFee > sumPrem)
        {

            this.mGrpContDiff = PubFun
                    .setPrecision(sumTmpFee - sumPrem, "0.00");

            return 1;
        }
        return 0;

    }

    /**
     * 查询团体下所有合同
     * @param tLCGrpContSchema LCGrpContSchema
     * @return LCContSet
     */
    private LCContSet queryLCContSet(LCGrpContSchema tLCGrpContSchema)
    {
        //        tLCContDB.setGrpContNo(tLCGrpContSchema.getGrpContNo());

        String sql = "select * from lccont where grpcontno='"
                + tLCGrpContSchema.getGrpContNo() + "'";
        //  + " and appflag='0' ";
        System.out.println("sql==" + sql);
        LCContSet tLCContSet = null;
        LCContDB tLCContDB = new LCContDB();
        tLCContSet = tLCContDB.executeQuery(sql);
        if (tLCContSet == null || tLCContSet.size() <= 0)
        {
            return null;
        }
        return tLCContSet;

    }

    /**
     * 查询并填充LCGrpContSet的数据
     * @return boolean
     */
    private boolean fillGrpCont()
    {
        LCGrpContDB db = new LCGrpContDB();

        for (int i = 1; i <= mLCGrpContSet.size(); i++)
        {
            /*
             db.setGrpContNo(mLCGrpContSet.get(i).getGrpContNo());
             db.getInfo();
             if (db.mErrors.needDealError())
             */
            String tGrpContNo = mLCGrpContSet.get(i).getGrpContNo();
            db.setProposalGrpContNo(tGrpContNo);
            mLCGrpContSet = db.query();
            if (mLCGrpContSet.size() < 1)
            {
                // CError.buildErr(this, db.mErrors.getErrContent());
                //this.mErrors.copyAllErrors( db.mErrors );
                CError.buildErr(this, "查找团单合同['" + tGrpContNo + "']信息失败");
                return false;
            }
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = new SSRS();
            SSRS tSSRS1 = new SSRS();
            SSRS tSSRS2 = new SSRS();
            tSSRS = tExeSQL
                    .execSQL("select distinct agentcode from lcgrppol where prtno='"
                            + this.mLCGrpContSet.get(1).getPrtNo() + "'");
            tSSRS1 = tExeSQL
                    .execSQL("select distinct agentcode from lccont where prtno='"
                            + this.mLCGrpContSet.get(1).getPrtNo() + "'   ");
            tSSRS2 = tExeSQL
                    .execSQL("select distinct agentcode from lcpol where prtno='"
                            + this.mLCGrpContSet.get(1).getPrtNo() + "'  ");
            if (tSSRS.getMaxRow() != 1 || tSSRS1.getMaxRow() != 1
                    || tSSRS2.getMaxRow() != 1)
            {
                CError.buildErr(this, "查找团单合同['" + tGrpContNo + "']代理人信息失败!");
                return false;
            }
            if (!(mLCGrpContSet.get(1).getAgentCode().equals(tSSRS
                    .GetText(1, 1)))
                    || !(mLCGrpContSet.get(1).getAgentCode().equals(tSSRS1
                            .GetText(1, 1)))
                    || !(mLCGrpContSet.get(1).getAgentCode().equals(tSSRS2
                            .GetText(1, 1))))
            {
                buildError("checkdate", "查找团单合同['" + tGrpContNo
                        + "']代理人代码信息失败!");
                return false;
            }
            
            if(!"cd".equals(mLCGrpContSet.get(i).getCardFlag())){
            //校验业务员是否离职
            String tAgentState = tExeSQL
                        .getOneValue("SELECT agentstate FROM laagent where agentcode='"
                                + mLCGrpContSet.get(1).getAgentCode() + "'");
            if ((tAgentState == null) || tAgentState.equals(""))
            {
                CError.buildErr(this, "无法查到保单业务员[" + mLCGrpContSet.get(1).getAgentCode() +"]的状态!");
                return false;
            }
            if (Integer.parseInt(tAgentState) >= 6)
            {
                CError.buildErr(this, "该保单业务员[" + mLCGrpContSet.get(1).getAgentCode() + "]已经离职，不能签单!");
                return false;
            }
            //mLCGrpContSet.set(i, db.getSchema());
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName(
                "LCGrpContSchema", 0);
        if (mLCGrpContSchema == null)
        {
            mLCGrpContSet = (LCGrpContSet) cInputData.getObjectByObjectName(
                    "LCGrpContSet", 0);
        }
        else
        {
            if (StrTool.cTrim(mLCGrpContSchema.getGrpContNo()).equals(""))
            {

                CError.buildErr(this, "集体投保单号传入失败!");
                return false;
            }

            mLCGrpContSet.add(mLCGrpContSchema);
        }

        if (mLCGrpContSet.size() <= 0)
        {
            CError.buildErr(this, "没有得到要签单的数据集");
            return false;
        }
        /** mLCGrpContSchema 这个变量没有传入 */

        //得到预打报单状态
        if ("9".equals(mLCGrpContSet.get(1).getAppFlag()))
        {
            mPreview = CPREVIEW;
        }
        else
        {
            mPreview = "";
        }

        return true;
    }

    private String getNewPolNo(String oldPolNo)
    {
        String polNo = (String) PolNoMap.get(oldPolNo);
        if (polNo == null)
        {
            polNo = oldPolNo;
        }
        return polNo;
    }

    private void cacheTempFee(Object key, LJTempFeeSchema tempFeeSchema)
    {
        this.mLJTempFeeMap.put(key, tempFeeSchema);
    }

    private Object getTempFee(String key)
    {
        return mLJTempFeeMap.get(key);
    }

    private boolean dealShodFee(LCGrpContSchema tLCGrpContSchema)
    {
        MMap temMap = new MMap();
        LJSPaySchema tLJSPaySchema = new LJSPaySchema(); //总应收数据对象
        LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet(); //应收集体交费数据对象
        //取得通知书号码
        String StrLimit = PubFun.getNoLimit("86");
        String newActNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", StrLimit);

        //准备应收集体交费数据
        tLJSPayGrpSet = prepareLJSPayGrp(tLCGrpContSchema, newActNoticeNo);
        if (tLJSPayGrpSet.size() > 0)
        {
            temMap.put(tLJSPayGrpSet, "INSERT");
        }
        //准备总应收数据
        tLJSPaySchema = prepareLJSPAY(tLCGrpContSchema, newActNoticeNo);
        if (tLJSPaySchema != null)
        {
            temMap.put(tLJSPaySchema, "INSERT");
        }

        //提交保存
        VData contData = new VData();
        //contData.add(mGlobalInput);
        contData.add(temMap);
        PubSubmit pubSubmit = new PubSubmit();
        if (!pubSubmit.submitData(contData, ""))
        {
            //CError.buildErr(this, "应交费写入数据失败!", pubSubmit.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 总应收
     *
     * @param tLCGrpContSchema LCGrpContSchema
     * @param pmActNoticeNo String
     * @return MMap
     */
    private LJSPaySchema prepareLJSPAY(LCGrpContSchema tLCGrpContSchema,
            String pmActNoticeNo)
    {
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();

        tLJSPaySchema.setGetNoticeNo(pmActNoticeNo);
        tLJSPaySchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
        tLJSPaySchema.setOtherNoType("5"); //表示集体投保单号
        // * ↓ *** add *** liuhao *** 2005-06-15 *****
        //如果是预打保单设置状态为‘11’ 用于在预打保单缴费后状态的区分问题
        if (CPREVIEW.equals(mPreview))
        {
            tLJSPaySchema.setOtherNoType("11"); //表示集体投保单号
        }
        // * ↑ *** add *** liuhao *** 2005-06-15 *****
        tLJSPaySchema.setAppntNo(tLCGrpContSchema.getAppntNo());
        tLJSPaySchema.setApproveCode("");
        tLJSPaySchema.setApproveDate("");
        tLJSPaySchema.setSumDuePayMoney(tLCGrpContSchema.getPrem());
        tLJSPaySchema.setPayDate(PubFun.getCurrentDate());
        tLJSPaySchema.setOperator(mGlobalInput.Operator);
        tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
        tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
        tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
        tLJSPaySchema.setManageCom(tLCGrpContSchema.getManageCom());
        tLJSPaySchema.setAgentCode(tLCGrpContSchema.getAgentCode());

        return tLJSPaySchema;
    }

    /**
     * 应收集体交费
     *
     * @param tLCGrpContSchema LCGrpContSchema
     * @param pmActNoticeNo String
     * @return MMap
     */
    private LJSPayGrpSet prepareLJSPayGrp(LCGrpContSchema tLCGrpContSchema,
            String pmActNoticeNo)
    {
        LJSPayGrpSet rtLJSPayGrpSet = new LJSPayGrpSet(); //返回值
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();

        //通过保单号,查询集体险种表
        tLCGrpPolDB.setGrpContNo(tLCGrpContSchema.getGrpContNo());
        tLCGrpPolSet = tLCGrpPolDB.query();

        //设置应收集体交费数据
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
            tLJSPayGrpSchema.setGrpContNo(tLCGrpPolSet.get(i).getGrpContNo()); //集体保单号
            tLJSPayGrpSchema.setGrpPolNo(tLCGrpPolSet.get(i).getGrpPolNo()); //集体保单险种号
            tLJSPayGrpSchema.setGetNoticeNo(pmActNoticeNo); //通知书号码
            //tLJSPayGrpSchema.setPayType(tLCGrpPolSet.get(i).getPayMode());     //交费类型
            tLJSPayGrpSchema.setPayType("1"); //交费类型
            tLJSPayGrpSchema.setOperator(mGlobalInput.Operator); //操作员
            tLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            tLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            tLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            tLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
            tLJSPayGrpSchema.setManageCom(this.mGlobalInput.ManageCom); //管理机构
            tLJSPayGrpSchema.setAgentCode(tLCGrpContSchema.getAgentCode()); //代理机构
            tLJSPayGrpSchema.setRiskCode(tLCGrpPolSet.get(i).getRiskCode()); //险种编码
            tLJSPayGrpSchema.setAgentCode(tLCGrpPolSet.get(i).getAgentCode()); //代理人编码
            tLJSPayGrpSchema.setAgentGroup(tLCGrpPolSet.get(i).getAgentGroup()); //代理人组别
            tLJSPayGrpSchema.setAppntNo(tLCGrpPolSet.get(i).getCustomerNo()); //投保人客户号码
            tLJSPayGrpSchema.setSumDuePayMoney(tLCGrpPolSet.get(i).getPrem()); //总应交金额
            tLJSPayGrpSchema.setSumActuPayMoney(0.00); //总实交金额
            rtLJSPayGrpSet.add(tLJSPayGrpSchema);
        }

        return rtLJSPayGrpSet;
    }

    // * ↓ *** add *** liuhao *** 2005-05-17 *****
    /**
     * 应收个人交费
     *
     * @param tLCGrpContSchema LCGrpContSchema
     * @param pmActNoticeNo String
     * @return LJSPayPersonSet
     */
    private LJSPayPersonSet prepareLJSPayPerson(
            LCGrpContSchema tLCGrpContSchema, String pmActNoticeNo)
    {
        LJSPayPersonSet rtLJSPayPersonSet = new LJSPayPersonSet();
        //流水号
        String tSerialNo = "0";

        //查询个人险种表
        LCPolSet tLCPolSet = new LCPolDBSet();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(tLCGrpContSchema.getGrpContNo());
        tLCPolSet = tLCPolDB.query();

        //设置应收个交费数据
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            //生成流水号
            //tSerialNo = tSerialNo + 1;
            tSerialNo = PubFun1.CreateMaxNo("SERIALNO", 20);
            LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
            tLJSPayPersonSchema.setPolNo(tLCPolSet.get(i).getPolNo());
            tLJSPayPersonSchema.setPayCount(1);
            tLJSPayPersonSchema.setGrpContNo(tLCPolSet.get(i).getGrpContNo());
            tLJSPayPersonSchema.setGrpPolNo(tLCPolSet.get(i).getGrpPolNo());
            tLJSPayPersonSchema.setContNo(tLCPolSet.get(i).getContNo());
            tLJSPayPersonSchema.setManageCom(this.mGlobalInput.ManageCom);
            tLJSPayPersonSchema.setAgentCom(tLCPolSet.get(i).getAgentCom());
            tLJSPayPersonSchema.setAgentType(tLCPolSet.get(i).getAgentType());
            tLJSPayPersonSchema.setRiskCode(tLCPolSet.get(i).getRiskCode());
            tLJSPayPersonSchema.setAgentCode(tLCPolSet.get(i).getAgentCode());
            tLJSPayPersonSchema.setAgentGroup(tLCPolSet.get(i).getAgentGroup());
            tLJSPayPersonSchema.setPayTypeFlag("1");
            tLJSPayPersonSchema.setAppntNo(tLCPolSet.get(i).getAppntNo());
            tLJSPayPersonSchema.setGetNoticeNo(pmActNoticeNo);
            tLJSPayPersonSchema.setPayAimClass("2");
            tLJSPayPersonSchema.setSumDuePayMoney(tLCPolSet.get(i).getPrem());
            tLJSPayPersonSchema.setSumActuPayMoney(0.00);
            tLJSPayPersonSchema.setPayIntv(tLCPolSet.get(i).getPayIntv());
            //tLJSPayPersonSchema.setPayDate();
            tLJSPayPersonSchema.setPayType("ZC");
            tLJSPayPersonSchema.setLastPayToDate(tLCPolSet.get(i)
                    .getCValiDate());
            //tLJSPayPersonSchema.setCurPayToDate();
            //tLJSPayPersonSchema.setInInsuAccState();
            tLJSPayPersonSchema.setBankCode(tLCGrpContSchema.getBankCode());
            tLJSPayPersonSchema.setBankAccNo(tLCGrpContSchema.getBankAccNo());
            tLJSPayPersonSchema.setBankOnTheWayFlag("0");
            //tLJSPayPersonSchema.setBankSuccFlag("0");
            tLJSPayPersonSchema.setApproveCode(tLCPolSet.get(i)
                    .getApproveCode());
            tLJSPayPersonSchema.setApproveDate(tLCPolSet.get(i)
                    .getApproveDate());
            tLJSPayPersonSchema.setApproveTime(tLCPolSet.get(i)
                    .getApproveTime());
            tLJSPayPersonSchema.setSerialNo(tSerialNo);
            tLJSPayPersonSchema.setInputFlag("0");
            tLJSPayPersonSchema.setOperator(this.mGlobalInput.Operator);
            tLJSPayPersonSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSPayPersonSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPayPersonSchema.setModifyTime(PubFun.getCurrentTime());

            //查询险种责任编码、交费计划编码
            LCPremDB tLCPremDB = new LCPremDB();
            LCPremSet tLCPremSet = new LCPremBLSet();
            tLCPremDB.setPolNo(tLCPolSet.get(i).getPolNo());
            tLCPremSet = tLCPremDB.query();
            //保单下存在多个险种和交费计划编码，将他们分别保存起来
            for (int j = 1; j <= tLCPremSet.size(); j++)
            {
                tLJSPayPersonSchema
                        .setDutyCode(tLCPremSet.get(j).getDutyCode());
                tLJSPayPersonSchema.setPayPlanCode(tLCPremSet.get(j)
                        .getPayPlanCode());

                rtLJSPayPersonSet.add(tLJSPayPersonSchema);
            }
        }
        return rtLJSPayPersonSet;
    }

    // * ↑ *** add *** liuhao *** 2005-05-19 *****

    //追加险种特约关联表信息
    private void setLCPolSpecRela(String pmNewGrpContNo)
    {
        LCSpecSet mLCSpecSet = null;
        LCPolSpecRelaSet mLCPolSpecRelaSet = null;
        PubSubmit ps = new PubSubmit();
        VData tInput = new VData();
        MMap tmpMap = new MMap();

        LCSpecDB mLCSpecDB = new LCSpecDB();
        mLCSpecDB.setGrpContNo(pmNewGrpContNo);
        mLCSpecSet = mLCSpecDB.query();
        for (int n = 1; n <= mLCSpecSet.size(); n++)
        {
            LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
            tLCPolSpecRelaSchema.setGrpContNo(mLCSpecSet.get(n).getGrpContNo());
            tLCPolSpecRelaSchema.setContNo(mLCSpecSet.get(n).getContNo());
            tLCPolSpecRelaSchema.setPolNo(mLCSpecSet.get(n).getPolNo());
            tLCPolSpecRelaSchema.setSpecCode(mLCSpecSet.get(n).getSpecCode());
            tLCPolSpecRelaSchema.setOperator(mGlobalInput.Operator);
            tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
            tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
            tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
            tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
            mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
        }
        //tmpMap.put(mLCPolSpecRelaSet,"INSERT");
        tmpMap.put(mLCPolSpecRelaSet, "INSERT");
        tInput.add(tmpMap);
        if (!ps.submitData(tInput, null))
        {
            CError.buildErr(this, "险种特约关联信息插入失败！");
        }
    }

    private MMap dealLCInuredList(String NewGrpContNo)
    {
        MMap mMap = new MMap();
        String SerialNo = PubFun1.CreateMaxNo("SerialNo", 20);
        mMap.put("insert into LBInsuredList (select '" + SerialNo
                + "',LCInsuredList.* from LCInsuredList where GrpContNo = '"
                + NewGrpContNo + "')", "INSERT");
        mMap.put("delete from LCInsuredList where GrpContNo = '" + NewGrpContNo
                + "'", "DELETE");
        return mMap;
    }

    private MMap dealLCInuredListPol(String NewGrpContNo)
    {
        MMap mMap = new MMap();
        String SerialNo = PubFun1.CreateMaxNo("SerialNo", 20);
        mMap
                .put(
                        "insert into LBInsuredListPol (select '"
                                + SerialNo
                                + "',LCInsuredListPol.* from LCInsuredListPol where GrpContNo = '"
                                + NewGrpContNo + "')", "INSERT");
        mMap.put("delete from LCInsuredListPol where GrpContNo = '"
                + NewGrpContNo + "'", "DELETE");
        return mMap;

    }

    private MMap prepareInsureAcc(String cGrpContNo, String cPayNo)
    {
        MMap tTmpMap = new MMap();
        String[] tInsuAccTableNames = { "LCInsureAccClassFee",
                "LCInsureAccFeeTrace", "LCInsureAccClass", "LCInsureAccTrace" };

        for (int i = 0; i < tInsuAccTableNames.length; i++)
        {
            String tStrSql = " update " + tInsuAccTableNames[i]
                    + " set OtherNo = '" + cPayNo + "' "
                    + " where GrpContNo = '" + cGrpContNo
                    + "' and OtherType = '1' ";
            tTmpMap.put(tStrSql, SysConst.UPDATE);
        }

        return tTmpMap;
    }

    private MMap dealCertifyContListState(String cNewGrpContNo)
    {
        MMap tMMap = new MMap();

        String tStrSql = " update LICertify set State = '"
                + CertifyContConst.CC_SETTLEMENT_CONFIRM
                + "' where GrpContNo = '" + cNewGrpContNo + "' and State = '"
                + CertifyContConst.CC_SETTLEMENT + "' ";

        tMMap.put(tStrSql, SysConst.UPDATE);

        // 回置退保单证状态
        tStrSql = " update LIBCertify set State = '"
                + CertifyContConst.CC_SETTLEMENT_CONFIRM
                + "' where GrpContNo = '" + cNewGrpContNo + "' and State = '"
                + CertifyContConst.CC_SETTLEMENT + "' ";

        tMMap.put(tStrSql, SysConst.UPDATE);
        // --------------------

        return tMMap;
    }

    /**
     * 处理double精度问题。
     * <br />1、对值加一个极小值
     * <br />2、将第一步计算之和进行四舍五入
     * @param cValue
     * @return
     */
    private double dealDouble(double cValue)
    {
        double tResult = 0d;
        double tSN = 0.000000001d;
        tResult = Arith.round(cValue + tSN, 2);
        return tResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCGrpContSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
