package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.tb.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCContGetPolUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private String mOperate = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContGetPolSchema mLCContGetPolSchema = new LCContGetPolSchema();

    public LCContGetPolUI() {
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContReceiveUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public boolean submitData(VData cInputData, String cOperate) {
        try {
            if (!cOperate.equals("INSERT") // 保存
                && !cOperate.equals("UPDATE")) { //修改
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            this.mInputData = cInputData;
            this.mOperate = cOperate;
            LCContGetPolBL tLCContGetPolBL = new LCContGetPolBL();

            System.out.println("LCContGetPolBL BEGIN");
            if (tLCContGetPolBL.submitData(cInputData, mOperate) == false) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCContGetPolBL.mErrors);
                return false;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "group";

        LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
        LCContGetPolUI tLCContGetPolUI = new LCContGetPolUI();
        tLCContGetPolSchema.setContNo("00001097901");
        tLCContGetPolSchema.setGetpolDate("2006-01-24");
        tLCContGetPolSchema.setGetpolMan("核保保AAAA");
        tLCContGetPolSchema.setSendPolMan("JHJHJ");
        tLCContGetPolSchema.setGetPolOperator("group");
        tLCContGetPolSchema.setManageCom("86110000");
        tLCContGetPolSchema.setAgentCode("1101000001");
        tLCContGetPolSchema.setContType("1");

        VData vData = new VData();

        vData.addElement(tLCContGetPolSchema);
        vData.add(globalInput);

        try {
            if (!tLCContGetPolUI.submitData(vData, "UPDATE")) {
                if (tLCContGetPolUI.mErrors.needDealError()) {
                    System.out.println(tLCContGetPolUI.mErrors.getFirstError());
                } else {
                    System.out.println("保存失败，但是没有详细的原因");
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            ex.getMessage();
        }
    }

}
