package com.sinosoft.lis.tb;

import java.util.HashMap;
import java.util.Iterator;

import com.sinosoft.lis.pubfun.diskimport.MultiSheetImporter;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.vschema.CrsInfoListSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LDCodeInterfaceSet;
import com.sinosoft.lis.vschema.LCInsuredListPolSet;


/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class crsDiskImport {
    public crsDiskImport() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //    /** 团单被保人清单Schema */
//    private static final String schemaClassName =
//            "com.sinosoft.lis.schema.LCInsuredListSchema";
//    /** 团单被保人清单Schema */
//    private static final String schemaSetClassName =
//            "com.sinosoft.lis.vschema.LCInsuredListSet";
    /** 从xls文件读入Sheet的开始行*/
    private static final int STARTROW = 2;
    /** 使用默认的导入方式 */
    private MultiSheetImporter importer;
    /** 错误处理 */
    public CErrors mErrors = new CErrors();
    /** Sheet Name */
    private String[] mSheetName;
    /** Schema 的名字 */
    private LCInsuredListSet mLCInsureListSet;
    /** Sheet对应table的名字*/
    private static final String[] mTableNames = {"CrsInfoList",""
                                                };

    public crsDiskImport(String fileName, String configFileName,
                         String[] sheetName) {
        mSheetName = sheetName;
        importer = new MultiSheetImporter(fileName, configFileName, mSheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport() {
      System.out.println(" Into GrpDiskImport doImport...");
        importer.setTableName(mTableNames);
        importer.setMStartRows(STARTROW);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrors);
            return false;
        } 

       
        System.out.println("版本号效验通过，GrpDiskImport完成。");
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "MultiSheetImporter";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public HashMap getResult() {
        return importer.getResult();
    }

    /**
     * 调试用的主函数
     * @param args String[]
     */
//    public static void main(String[] args) {
//        String path = "D:/wuligang/workspace/testData/16000002222.xls";
//        String config = "D:/wuligang/workspace/testData/GrpDiskImport.xml";
//        String[] sheet = {"Sheet1", "Ver", "Sheet3"};
//        crsDiskImport grpdiskimport = new crsDiskImport(path, config, sheet);
//        grpdiskimport.doImport();
//        System.out.println("import finished!");
//        LCInsuredListSet tLCInsuredListSet =
//                (LCInsuredListSet) grpdiskimport.getSchemaSet();
//        LCInsuredListPolSet tLCInsuredListPolSet = (LCInsuredListPolSet)
//                grpdiskimport.
//                getLCInsuredListPolSet();
//        for (int i = 1; i <= tLCInsuredListSet.size(); i++) {
//            System.out.println("处理数据 " + i +
//                               tLCInsuredListSet.get(i).encode().replace('|',
//                    '\t'));
//
//        }
//        for (int i = 1; i <= tLCInsuredListPolSet.size(); i++) {
//            System.out.println("处理数据 " + i +
//                               tLCInsuredListPolSet.get(i).encode().replace('|',
//                    '\t'));
//
//        }
//    }

    /**
     * 用于获取导入文件版本号
     * @return String
     */
    public String getVersion() {
        HashMap tHashMap = importer.getResult();
        String version = "";
        version = (String) tHashMap.get(mSheetName[1]);
        return version;
    }

    /**
     * getSchemaSet 获取从MutiSheetImporter获得的SchemaSet
     *
     * @return LCInsuredListSet
     */
    public CrsInfoListSet getSchemaSet() {
        CrsInfoListSet tLCInsuredListSet = new CrsInfoListSet();
        HashMap tHashMap = importer.getResult();
        tLCInsuredListSet.add((CrsInfoListSet) tHashMap.get(mSheetName[0]));
        return tLCInsuredListSet;
    }

   


    private void jbInit() throws Exception {
    }
}
