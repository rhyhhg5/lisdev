package com.sinosoft.lis.tb;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BusinessRuleConfigUI
{
    public CErrors mErrors = new CErrors();

    public BusinessRuleConfigUI()
    {
    }

    /**
     * 外部提交的方法，本方法不直接处理业务
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        BusinessRuleConfigBL bl = new BusinessRuleConfigBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        BusinessRuleConfigUI businessruleconfigui = new BusinessRuleConfigUI();
    }
}
