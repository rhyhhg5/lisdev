/*
 * <p>ClassName: FamilyInsuredBL </p>
 * <p>Description: FamilyInsuredBL类文件 生成家庭单</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2004-11-18 10:09:44
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class FamilyInsuredBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private Reflections ref = new Reflections();
  private TransferData mTransferData = new TransferData();
  private MMap map = new MMap();

  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  private LCInsuredSet mLCInsuredSet = new LCInsuredSet();

  //private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
  //private LDPersonSchema tLDPersonSchema = new LDPersonSchema();
  private LCContSchema mLCContSchema = new LCContSchema();

  //private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
  //private LCAccountSchema mLCAccountSchema = new LCAccountSchema();
  //private String ContNo;

//private LCInsuredSet mLCInsuredSet=new LCInsuredSet();
  public FamilyInsuredBL() {
  }

  public static void main(String[] args) {
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    if (!mOperate.equals("DELETE||MAIN")) {
      if (this.checkData() == false)
        return false;
      System.out.println("---checkData---");
    }

    //进行业务处理
    if (!dealData()) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "FamilyInsuredBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败FamilyInsuredBL-->dealData!";
      this.mErrors.addOneError(tError);
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN")) {
      this.submitquery();
    }
    else {
      //　数据提交、保存

      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start tPRnewManualDunBLS Submit...");

      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "ContBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        //this.mErrors .addOneError(tError) ;
        return false;
      }

    }
    mInputData = null;
    return true;
  }

  /**
   * 校验传入的数据
   * @param: 无
   * @return: boolean
   */
  private boolean checkData() {
    for (int i = 1; i <= mLCInsuredSet.size(); i++) {
      if (mLCInsuredSet.get(i).getRelationToAppnt().equals("0")) { //主被保险人
        mLCContSchema.setInsuredNo(mLCInsuredSet.get(i).getInsuredNo());
        mLCContSchema.setInsuredBirthday(mLCInsuredSet.get(i).getBirthday());
        mLCContSchema.setInsuredIDNo(mLCInsuredSet.get(i).getIDNo());
        mLCContSchema.setInsuredIDType(mLCInsuredSet.get(i).getIDType());
        mLCContSchema.setInsuredName(mLCInsuredSet.get(i).getName());
        mLCContSchema.setInsuredSex(mLCInsuredSet.get(i).getSex());
      }
    }
    if (mLCContSchema.getInsuredNo() == null ||
        ("").equals(mLCContSchema.getInsuredNo())) {
      CError tError = new CError();
      tError.moduleName = "FamilyInsuredBL";
      tError.functionName = "checkData";
      tError.errorMessage = "没有主被保险人的信息,请确认指定了主被保险人!";
      this.mErrors.addOneError(tError);
      return false;

    }



    return true;

  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
    String tNo = PubFun1.CreateMaxNo("FamilyID", tLimit);
    //填写所有被保险人家庭号,
    for (int i=1;i<=mLCInsuredSet.size();i++){
      mLCInsuredSet.get(i).setFamilyID(tNo);
    }
    mLCContSchema.setFamilyType("1");//家庭单标记

    map.put(mLCInsuredSet, "UPDATE");
    map.put(mLCContSchema, "UPDATE");
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean updateData() {
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean deleteData() {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    this.mLCInsuredSet.set( (LCInsuredSet) cInputData.
                                   getObjectByObjectName("LCInsuredSet", 0));
    this.mLCContSchema.setSchema( (LCContSchema) cInputData.
                                   getObjectByObjectName("LCContSchema", 0));
 //    mLCInsuredSet.add

//    ref.transFields(mLDPersonSchema, mLCInsuredSchema); //客户表
//    ContNo = (String) mTransferData.getValueByName("ContNo");
    if (mLCInsuredSet == null || mLCInsuredSet.size() < 1) {
      // @@错误处理

      CError tError = new CError();
      tError.moduleName = "FamilyInsuredBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "没有任何被保险人信息!";
      this.mErrors.addOneError(tError);
      return false;

    }
    //根据传入的客户号，查出客户的具体信息
    for (int i = 1; i <= mLCInsuredSet.size(); i++) {
      LCInsuredDB tLCInsuredDB = new LCInsuredDB();
      tLCInsuredDB.setContNo(mLCInsuredSet.get(i).getContNo());
      tLCInsuredDB.setInsuredNo(mLCInsuredSet.get(i).getInsuredNo());
      tLCInsuredDB.getInfo();
      if (tLCInsuredDB.mErrors.needDealError()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCInsuredDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "FamilyInsuredBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "未查到客户号为" + tLCInsuredDB.getInsuredNo() +
            "的被保险人信息!";
        this.mErrors.addOneError(tError);
        return false;
      }
    }
    LCContDB tLCContDB = new LCContDB();
    tLCContDB.setContNo(mLCContSchema.getContNo());
    tLCContDB.getInfo();
    if (tLCContDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCContDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "FamilyInsuredBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "未查到相关合同信息!";
      this.mErrors.addOneError(tError);
      return false;
    }

    mLCContSchema.setSchema(tLCContDB);

    //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean submitquery() {
//    this.mResult.clear();
//    LCInsuredDB tLCInsuredDB = new LCInsuredDB();
//    tLCInsuredDB.setSchema(this.mLCInsuredSchema);
//    //如果有需要处理的错误，则返回
//    if (tLCInsuredDB.mErrors.needDealError()) {
//      // @@错误处理
//      this.mErrors.copyAllErrors(tLCInsuredDB.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "LCInsuredBL";
//      tError.functionName = "submitData";
//      tError.errorMessage = "数据提交失败!";
//      this.mErrors.addOneError(tError);
//      return false;
//    }
//    mInputData = null;
    return true;
  }

  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      mInputData.add(map);
      mResult.clear();
//      mResult.add(this.mLCInsuredSchema);
//      mResult.add(this.mLCAddressSchema);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LCInsuredBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  public VData getResult() {
    return this.mResult;
  }



}
