package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class TaxEIPCheckUI {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	
	private GlobalInput mGlobalInput = null;
	private MMap mMap = new MMap();
	private VData mInputData = new VData();
	private TransferData mTransferData = null;
	
	private String mOperate = "";
	private String mBatchNo = null;
	
	public TaxEIPCheckUI(){}
	
	public boolean submitData(VData cInputData,String cOperate){
		//先处理一部分数据。
		mOperate = cOperate;
		//更新提交内检的批次的状态为2:待确认;
		if(!submitPartData(cInputData,mOperate)){
			return false;
		}
		//继续将数据递交至后台处理
		try{
			TaxEIPCheckBL tTaxEIPCheckBL = new TaxEIPCheckBL();
			if(!tTaxEIPCheckBL.submitData(cInputData,cOperate)){
				this.mErrors.copyAllErrors(tTaxEIPCheckBL.mErrors);
				return false;
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			this.mErrors.addOneError(e.getMessage());
			return false;
		}
	}
	
	private boolean submitPartData(VData cInputData,String cOperate){
		if(!getInputData(cInputData,cOperate)){
			return false;
		}
		if(!checkData()){
			return false;
		}
		if(!dealData()){
			buildError("dealData", "处理数据失败");
			return false;
		}
		if(!prepareData()){
			buildError("prepareData", "封装数据失败");
			return false;
		}
		if(!handOverData()){
			return false;
		}
		return true;
	}
	
	private boolean getInputData(VData cInputData,String cOperate){
		mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
		if(mGlobalInput==null){
			buildError("getInputData", "处理超时，请重新登录。");
            return false;
		}
		
		mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
		if(mTransferData==null){
			buildError("getInputData", "所需参数不完整。");
			return false;
		}
		
		mBatchNo = (String)mTransferData.getValueByName("BatchNo");
		if(mBatchNo==null || "".equals(mBatchNo)){
			buildError("getInputData", "获取批次信息失败。");
            return false;
		}
		
		return true;
	}
	
	private boolean checkData(){
		String tSQL = "select batchstate from lsbatchinfo where batchno='"+mBatchNo+"'";
		String tBatchState = new ExeSQL().getOneValue(tSQL);
		//批次状态为0：待导入;3：已确认;则不能调用接口
		//批次状态为1:待内检;2:待确认;可以调用中报信接口
		if("0".equals(tBatchState) || "3".equals(tBatchState)){
			buildError("checkData", "批次状态不是待内检或待确认，不符合调用接口的标准。");
            return false;
		}
		return true;
	}
	
	private boolean dealData(){
		String tUpSql = "update lsbatchinfo set batchstate='2',modifydate=current date,modifytime=current time where batchno='"+mBatchNo+"'";
		mMap.put(tUpSql, SysConst.UPDATE);
		return true;
	}
	
	private boolean prepareData(){
		mInputData.add(mMap);
		return true;
	}
	
	private boolean handOverData(){
		PubSubmit ps = new PubSubmit();
		if(!ps.submitData(mInputData, "")){
			System.out.println("提交数据失败");
			buildError("handOverData","提交数据失败");
			return false;
		}
		return true;
	}
	
	private void buildError(String szFunc, String szErrMsg){
        CError cError = new CError();
        cError.moduleName = "TaxEIPCheckUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
