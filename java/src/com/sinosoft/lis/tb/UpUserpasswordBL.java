package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class UpUserpasswordBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private String mOperate = "";

	MMap map = new MMap();

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private LDUserSchema mLDUserSchema = new LDUserSchema();

	public UpUserpasswordBL() {
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LCContReceiveUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public boolean submitData(VData cInputData, String cOperate) {

		this.mOperate = cOperate;
		System.out.println("动作为：" + mOperate);

		if (!cOperate.equals("password") && !cOperate.equals("level")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}

		// 全局变量赋值
		mOperate = cOperate;
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("End getInputData");
		if (this.dealData() == false) {
			return false;
		}
		System.out.println("End dealData");
		// 统一递交数据
		mResult.add(map);
		PubSubmit pubsubmit = new PubSubmit();
		if (!pubsubmit.submitData(mResult, cOperate)) {
			mErrors.copyAllErrors(pubsubmit.mErrors);
			return false;
		}
		System.out.println("---End pubsubmit---");
		return true;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mLDUserSchema.setSchema((LDUserSchema) cInputData
				.getObjectByObjectName("LDUserSchema", 0));

		return true;
	}

	private boolean dealData() {

		String tUsercode = mLDUserSchema.getUserCode();
		String tOperator = mLDUserSchema.getOperator();
		String strSQL1 = "";
		// 重置密码
		if (mOperate.equals("password")) {

			// 取初始密码
			String initPassWord = "";
			String getInitPwd = "select code from ldcode where codetype = 'initpassward'";
			initPassWord = new ExeSQL().getOneValue(getInitPwd);
			if (initPassWord == null || "".equals(initPassWord)) {
				buildError("UpUserpasswordBL", "系统中未配置初始密码，请联系运维人员！");
				return false;
			}

			// 检查用户是否存在
			String strSQL = " select 1 from lduser where usercode='"
					+ tUsercode + "' ";
			String tResult = new ExeSQL().getOneValue(strSQL);
			if (!"1".equals(tResult)) {
				buildError("dealTempFee", "未查询到用户编码为'" + tUsercode + "'的客户!!");
				return false;
			}

			strSQL1 = "update lduser set password = '" + initPassWord + "',"
					+ " Modifydate='" + PubFun.getCurrentDate()
					+ "', Modifytime='" + PubFun.getCurrentTime()
					+ "'where usercode='" + tUsercode + "'";
			map.put(strSQL1, "UPDATE");
		}

		// 修改上级
		if (mOperate.equals("level")) {

			// 检查上级用户是否存在
			String strSQL = " select 1 from lduser where usercode='"
					+ tOperator + "' ";
			String tResult = new ExeSQL().getOneValue(strSQL);
			if (!"1".equals(tResult)) {
				buildError("dealTempFee", "未查询到用户编码为'" + tOperator
						+ "'的用户，不能进行上级用户修改");
				return false;
			}

			strSQL1 = "update lduser set Operator = '" + tOperator + "',"
					+ " Modifydate='" + PubFun.getCurrentDate()
					+ "', Modifytime='" + PubFun.getCurrentTime()
					+ "'where usercode='" + tUsercode + "'";
			map.put(strSQL1, "UPDATE");
		}

		return true;
	}

}
