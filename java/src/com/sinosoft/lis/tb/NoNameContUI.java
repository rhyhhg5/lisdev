package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author  zhangxing
 * @version 1.0
 */

public class NoNameContUI {

    private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    public NoNameContUI() {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        NoNameContBL tNoNameContBL = new NoNameContBL();
        System.out.println("Start NoNameContUI Submit...");
        tNoNameContBL.submitData(mInputData, cOperate);

        System.out.println("End NoNameContUI Submit...");

        //如果有需要处理的错误，则返回
        if (tNoNameContBL.mErrors.needDealError()) {
            mErrors.copyAllErrors(tNoNameContBL.mErrors);
            return false;
        }
        mResult.clear();
        mResult = tNoNameContBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "Yang";
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("GrpContNo", "1400000440");
        //mTransferData.setNameAndValue("Peoples","20");
        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(mTransferData);
        tVData.add(tG);

        // 数据传输
        NoNameContUI tNoNameContUI = new NoNameContUI();
        if (tNoNameContUI.submitData(tVData, "") == false) {
            System.out.println(tNoNameContUI.mErrors.getError(0).
                               errorMessage);
        }

    }

}
