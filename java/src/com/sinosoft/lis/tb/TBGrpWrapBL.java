package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCRiskWrapDB;
import com.sinosoft.lis.db.LDRiskWrapDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCRiskWrapSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCRiskWrapSet;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 1.0
 */
public class TBGrpWrapBL {
  public TBGrpWrapBL() {
  }

  public static void main(String[] args) {
    VData tVData = new VData();
    /** 测试用例1 执行插入新的套餐 */
    String tGrpContNo = "1400002651";
    String tWrapCode = "WR0002";
    String managecom = "86";
    String comCode = "86";
    String operator = "001";
    String fmAction = "INSERT||GROUPRISK";
    /** 测试用例2 执行删除已存在的套餐 */
//    LCRiskWrapSchema tLCRiskWrapSchema;
//    LCRiskWrapSet tmLCRiskWrapSet = new LCRiskWrapSet();
//    String tGrpContNo = "1400002650";
//    String tWrapCode = null; //= "WR0001";
//    tLCRiskWrapSchema = new LCRiskWrapSchema();
//    tLCRiskWrapSchema.setRiskWrapCode("WR0001");
//    tmLCRiskWrapSet.add(tLCRiskWrapSchema);
//    tVData.add(tmLCRiskWrapSet);
//    String managecom = "86";
//    String comCode = "86";
//    String operator = "001";
//    String fmAction = "DELETE||GROUPRISK";

    TBGrpWrapBL tbgrpwrapbl = new TBGrpWrapBL();
    TransferData tTransferData = new TransferData();
    GlobalInput tGI = new GlobalInput();
    tGI.ManageCom = managecom;
    tGI.ComCode = comCode;
    tGI.Operator = operator;
    tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
    tTransferData.setNameAndValue("WrapCode", tWrapCode);

    tVData.add(tTransferData);
    tVData.add(tGI);

    boolean b = tbgrpwrapbl.submitData(tVData, fmAction);
    if (!b) {
      System.out.println("程序报错 : " + tbgrpwrapbl.mErrors.getErrContent());
    }
  }

  /** 传入参数 */
  private VData mInputData;
  /** 传入操作符 */
  private String mOperate;
  /** 登陆信息 */
  private GlobalInput mGlobalInput;
  /** 报错存储对象 */
  public CErrors mErrors = new CErrors();
  /** 最后保存结果 */
  private VData mResult = new VData();
  /** 最后递交Map */
  private MMap map = new MMap();
  /** 前台传入参数对象 */
  private TransferData mTransferData;
  /** 套餐表 */
  private LCRiskWrapSet mLCRiskWrapSet;
  private String mWrapCode;
  private LCRiskWrapSchema mLCRiskWrapSchema;
  /** 团体合同号码 */
  private String mGrpContNo;
  /** 团体合同信息 */
  private LCGrpContSchema mLCGrpContSchema;
  /** 险种描述信息 */
  private LMRiskAppSet mLMRiskAppSet;
  /** 团体险种信息 */
  private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
  /** 记录计算方向 */
  private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
  /** 保障计划险种信息 */
  private LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
  /**
   * submitData
   *
   * @param nInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData nInputData, String cOperate) {
    System.out.println("into BriefCardSignBL...");
    this.mInputData = nInputData;
    this.mOperate = cOperate;

    if (!getInputData()) {
      return false;
    }
    if (!checkData()) {
      return false;
    }
    if (!dealData()) {
      return false;
    }
    if (!prepareOutputData()) {
      return false;
    }

    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      return false;
    }
    System.out.println("BriefCardSignBL finished...");
    return true;
  }

  /**
   * getInputData
   *
   * @return boolean
   */
  private boolean getInputData() {
    System.out.println("into BriefCardSignBL.getInputData()...");
    mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
        "GlobalInput", 0);

    mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);

    mLCRiskWrapSet = (LCRiskWrapSet) mInputData.getObjectByObjectName("LCRiskWrapSet", 0);

    return true;
  }

  /**
   * checkData
   *
   * @return boolean
   */
  private boolean checkData() {
    System.out.println("into BriefCardSignBL.checkData()...");
    if (this.mGlobalInput == null) {
      String str = "登陆信息为null，可能是页面超时，请重新登陆!";
      buildError("checkData", str);
      return false;
    }

    if (mTransferData == null) {
      String str = "传入参数信息不完整!";
      buildError("checkData", str);
      System.out.println("在程序TBGrpWrapBL.checkData() - 115 : " + str);
      return false;
    }

    this.mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");

    if (mGrpContNo == null || mGrpContNo.equals("")) {
      String str = "传入团体合同信息不完整!";
      buildError("checkData", str);
      System.out.println("在程序TBGrpWrapBL.checkData() - 124 : " + str);
      return false;
    }

    LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    tLCGrpContDB.setGrpContNo(mGrpContNo);
    if (!tLCGrpContDB.getInfo()) {
      String str = "查询团体合同信息失败!";
      buildError("checkData", str);
      System.out.println("在程序TBGrpWrapBL.checkData() - 134 : " + str);
      return false;
    }
    mLCGrpContSchema = tLCGrpContDB.getSchema();

    mWrapCode = (String) mTransferData.getValueByName("WrapCode");
    /** 校验套餐 */
    LCRiskWrapDB tLCRiskWrapDB = new LCRiskWrapDB();
    tLCRiskWrapDB.setGrpContNo(this.mGrpContNo);
    tLCRiskWrapDB.setRiskWrapCode(this.mWrapCode);
    int count = tLCRiskWrapDB.getCount();
    if (this.mOperate.equals("INSERT||GROUPRISK")) {
      if (count > 0) {
        String str = "此套餐已存在不能重复添加此套餐!";
        buildError("checkData", str);
        System.out.println("在程序TBGrpWrapBL.checkData() - 160 : " + str);
        return false;
      }
    }
    if (this.mOperate.equals("DELETE||GROUPRISK")) {
      LCRiskWrapSet tempLCRiskWrapSet = new LCRiskWrapSet();
      if (mLCRiskWrapSet == null || mLCRiskWrapSet.size() <= 0) {
        String str = "传入要删除的套餐代码为null!";
        buildError("checkData", str);
        System.out.println("在程序TBGrpWrapBL.checkData() - 214 : " + str);
        return false;
      }
      String tWrapCode = "";

      for (int i = 1; i <= mLCRiskWrapSet.size(); i++) {
        if (!tWrapCode.equals(mLCRiskWrapSet.get(i).getRiskWrapCode())) {
          tWrapCode = mLCRiskWrapSet.get(i).getRiskWrapCode();
          LCRiskWrapDB tempLCRiskWrapDB = new LCRiskWrapDB();
          tempLCRiskWrapDB.setGrpContNo(this.mGrpContNo);
          tempLCRiskWrapDB.setRiskWrapCode(tWrapCode);
          if (tempLCRiskWrapDB.getCount() <= 0) {
            String str = "传入的套餐编码查询失败，系统中不存在这样的套餐!";
            buildError("checkData", str);
            System.out.println("在程序TBGrpWrapBL.checkData() - 228 : " + str);
            return false;
          }
          tempLCRiskWrapSet.add(tempLCRiskWrapDB.query());
        }
      }
      mLCRiskWrapSet.set(tempLCRiskWrapSet);
      /** 如果是执行删除操作 WrapCode 是null 所以查询的是本合同的全部套餐 */
      if (count < 0) {
        String str = "执行操作是删除操作，但是此套餐还没有保存至系统!";
        buildError("checkData", str);
        System.out.println("在程序TBGrpWrapBL.checkData() - 167 : " + str);
        return false;
      }
      /** 针对删除套餐进行一定校验,主要是1,险种信息完整性.2,不存在当前险种的套餐 */
      LCRiskWrapSet tLCRiskWrapSet = tLCRiskWrapDB.query();
      if (tLCRiskWrapSet == null || tLCRiskWrapSet.size() <= 0) {
        String str = "执行删除操作但是没有查询到套餐信息!";
        buildError("checkData", str);
        System.out.println("在程序TBGrpWrapBL.checkData() - 220 : " + str);
        return false;
      }

      /** 查询处全部险种 */
      for (int i = 1; i <= tLCRiskWrapSet.size(); i++) {
        String tRiskWrapCode = tLCRiskWrapSet.get(i).getRiskWrapCode();
        String tRiskCode = tLCRiskWrapSet.get(i).getRiskCode();
        for (int m = 1; m <= tempLCRiskWrapSet.size(); m++) {
          if (!tRiskWrapCode.equals(tempLCRiskWrapSet.get(m).getRiskWrapCode()) &&
              tRiskCode.equals(tempLCRiskWrapSet.get(m).getRiskCode())) {
            /** 只有不包含在其他套餐内的险种才进行删除动作 */
            tempLCRiskWrapSet.removeRange(m, m);
            System.out.println("只有不包含在其他套餐内的险种才进行删除动作");
          }
        }
      }
      for (int i = 1; i <= tempLCRiskWrapSet.size(); i++) {
        String tRiskCode = tempLCRiskWrapSet.get(i).getRiskCode();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(this.mGrpContNo);
        tLCGrpPolDB.setRiskCode(tRiskCode);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        if (tLCGrpPolSet == null || tLCGrpPolSet.size() <= 0) {
          String str = "查询团体险种失败!";
          buildError("checkData", str);
          System.out.println("在程序TBGrpWrapBL.checkData() - 247 : " + str);
          return false;
        }
        if (tLCGrpPolSet.size() > 1) {
          String str = "险种多余一条，数据错误!";
          buildError("checkData", str);
          System.out.println("在程序TBGrpWrapBL.checkData() - 253 : " + str);
          return false;
        }
        mLCGrpPolSet.add(tLCGrpPolSet);
      }
    }

    return true;
  }

  /**
   * dealData
   *
   * @return boolean
   */
  private boolean dealData() {
    System.out.println("into BriefCardSignBL.dealData()...");
    if (this.mOperate.equals("INSERT||GROUPRISK")) {
      if (!insertData()) {
        return false;
      }
    }
    if (this.mOperate.equals("DELETE||GROUPRISK")) {
      if (!deleteData()) {
        return false;
      }
    }
    return true;
  }

  /**
   * deleteData
   *
   * @return boolean
   */
  private boolean deleteData() {
    /** 校验全部险种是否已存在险种 或存在于其他套餐 */
    for (int i = 1; i <= mLCGrpPolSet.size(); i++) {
      String tGrpPolNo = mLCGrpPolSet.get(i).getGrpPolNo();
      String tRiskCode = mLCGrpPolSet.get(i).getRiskCode();
      /** 先校验是否存在被保险人 */
      LCPolDB tLCPolDB = new LCPolDB();
      tLCPolDB.setGrpContNo(this.mGrpContNo);
      tLCPolDB.setGrpPolNo(tGrpPolNo);
      if (tLCPolDB.getCount() > 0) {
        String str = "已存在被保险人,请删除被保险人后进行套餐删除!";
        buildError("checkData", str);
        System.out.println("在程序TBGrpWrapBL.checkData() - 249 : " + str);
        return false;
      }
      /** 校验是否存在保障计划 */
//          pk[0] = "ProposalGrpContNo";
//          pk[1] = "MainRiskCode";
//          pk[2] = "RiskCode";
//          pk[3] = "ContPlanCode";
//          pk[4] = "PlanType";

      LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
      tLCContPlanRiskDB.setProposalGrpContNo(this.mGrpContNo);
      tLCContPlanRiskDB.setRiskCode(tRiskCode);
      tLCContPlanRiskDB.setMainRiskCode(tRiskCode);
      /** 如果查询到也不一定时错误,是否是默认计划, */
      LCContPlanRiskSchema tLCContPlanRiskSchema = null;
      LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();
      if (tLCContPlanRiskSet != null && tLCContPlanRiskSet.size() > 0) {
        for (int m = 1; m <= tLCContPlanRiskSet.size(); m++) {
          tLCContPlanRiskSchema = tLCContPlanRiskSet.get(m);
          if (!tLCContPlanRiskSchema.getContPlanCode().equals("11")) {
            String str = "套餐内险种已经存在保障计划!";
            buildError("checkData", str);
            System.out.println("在程序TBGrpWrapBL.checkData() - 291 : " + str);
            return false;
          }
          mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
        }
      }
    }
    for (int i = 1; i <= mLCContPlanRiskSet.size(); i++) {
      LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
      tLCContPlanDutyParamDB.setGrpContNo(this.mGrpContNo);
      tLCContPlanDutyParamDB.setRiskCode(mLCContPlanRiskSet.get(i).getRiskCode());
      tLCContPlanDutyParamDB.setContPlanCode(mLCContPlanRiskSet.get(i).getContPlanCode());
      this.mLCContPlanDutyParamSet.add(tLCContPlanDutyParamDB.query());
    }
    map.put(this.mLCGrpPolSet, "DELETE");
    map.put(this.mLCContPlanRiskSet, "DELETE");
    map.put(this.mLCContPlanDutyParamSet, "DELETE");
    map.put(this.mLCRiskWrapSet, "DELETE");
    return true;
  }

  /**
   * insertData
   *
   * @return boolean
   */
  private boolean insertData() {
    /** 首先查询产品描述 */
    if (!getRiskInfo()) {
      return false;
    }
    if (!dealGrpPolInfo()) {
      return false;
    }
    if (!dealCalRule()) {
      return false;
    }
    if (!dealRiskWrap()) {
      return false;
    }
    return true;
  }

  /**
   * dealRiskWrap
   *
   * @return boolean
   */
  private boolean dealRiskWrap() {
    if (mLCRiskWrapSet == null) {
      mLCRiskWrapSet = new LCRiskWrapSet();
    }
    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    tLCGrpPolDB.setGrpContNo(this.mGrpContNo);
    LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
    if (tLCGrpPolSet == null) {
      tLCGrpPolSet = new LCGrpPolSet();
    }
    for (int i = 1; i <= this.mLMRiskAppSet.size(); i++) {
      String tRiskCode = mLMRiskAppSet.get(i).getRiskCode();
      LCGrpPolSchema tLCGrpPolSchema = null;
      for (int m = 1; m <= mLCGrpPolSet.size(); m++) {
        if (mLCGrpPolSet.get(m).getRiskCode().equals(tRiskCode)) {
          tLCGrpPolSchema = mLCGrpPolSet.get(m);
          break;
        }
      }
      for (int m = 1; m <= tLCGrpPolSet.size(); m++) {
        if (tRiskCode.equals(tLCGrpPolSet.get(m).getRiskCode())) {
          tLCGrpPolSchema = tLCGrpPolSet.get(m);
          break;
        }
      }
      if (tLCGrpPolSchema == null) {
        String str = "抽取险种信息失败!";
        buildError("dealRiskWrap", str);
        System.out.println("在程序TBGrpWrapBL.dealRiskWrap() - 437 : " + str);
        return false;
      }
      LCRiskWrapSchema tLCRiskWrapSchema = new LCRiskWrapSchema();
      tLCRiskWrapSchema.setGrpContNo(this.mGrpContNo);
      tLCRiskWrapSchema.setProposalGrpContNo(this.mGrpContNo);
      tLCRiskWrapSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
      tLCRiskWrapSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
      tLCRiskWrapSchema.setPrtNo(tLCGrpPolSchema.getPrtNo());
      tLCRiskWrapSchema.setRiskWrapCode(this.mWrapCode);
      tLCRiskWrapSchema.setOperator(this.mGlobalInput.Operator);
      PubFun.fillDefaultField(tLCRiskWrapSchema);
      this.mLCRiskWrapSet.add(tLCRiskWrapSchema);
    }
    map.put(mLCRiskWrapSet, "INSERT");
    return true;
  }

  /**
   * dealCalRule
   *
   * @return boolean
   */
  private boolean dealCalRule() {
    if (mLCContPlanDutyParamSet != null) {
      for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++) {
        LMRiskDutyFactorDB tLMRiskDutyFactorDB = new LMRiskDutyFactorDB();
        tLMRiskDutyFactorDB.setRiskCode(mLCContPlanDutyParamSet.get(i).getRiskCode());
        tLMRiskDutyFactorDB.setCalFactor(mLCContPlanDutyParamSet.get(i).getCalFactor());
        tLMRiskDutyFactorDB.setDutyCode("000000");
        tLMRiskDutyFactorDB.setPayPlanCode("000000");
        tLMRiskDutyFactorDB.setGetDutyCode("000000");
        tLMRiskDutyFactorDB.setInsuAccNo("000000");
        if (!tLMRiskDutyFactorDB.getInfo()) {
          String str = "险种要素信息查询失败!";
          buildError("dealCalRule", str);
          System.out.println("在程序TBGrpWrapBL.dealCalRule() - 243 : " + str);
          return false;
        }
        mLCContPlanDutyParamSet.get(i).setContPlanCode("11");
        mLCContPlanDutyParamSet.get(i).setContPlanName("默认计划");
        mLCContPlanDutyParamSet.get(i).setDutyCode(tLMRiskDutyFactorDB.getDutyCode());
        mLCContPlanDutyParamSet.get(i).setCalFactorType(tLMRiskDutyFactorDB.getCalFactorType());
        mLCContPlanDutyParamSet.get(i).setPlanType("0");
        mLCContPlanDutyParamSet.get(i).setPayPlanCode(tLMRiskDutyFactorDB.getPayPlanCode());
        mLCContPlanDutyParamSet.get(i).setGetDutyCode(tLMRiskDutyFactorDB.getGetDutyCode());
        mLCContPlanDutyParamSet.get(i).setInsuAccNo(tLMRiskDutyFactorDB.getInsuAccNo());
      }

      //如果默认计划已经存在则不添加计划
      StringBuffer sql = new StringBuffer(255);
      sql.append("select count(1) from lccontplan where grpcontno='");
      sql.append(mLCGrpContSchema.getGrpContNo());
      sql.append("' and contplancode='11'");
      ExeSQL tExeSQL = new ExeSQL();
      if (Integer.parseInt(tExeSQL.getOneValue(sql.toString())) <= 0) {
        LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
        tLCContPlanSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCContPlanSchema.setProposalGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCContPlanSchema.setContPlanName("默认计划");
        tLCContPlanSchema.setContPlanCode("11");
        tLCContPlanSchema.setPlanType("0");
        tLCContPlanSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLCContPlanSchema);
        map.put(tLCContPlanSchema, "INSERT");
      }
      if (this.mLCContPlanRiskSet != null && this.mLCContPlanRiskSet.size() > 0) {
        map.put(mLCContPlanRiskSet, "INSERT");
      }
      map.put(mLCContPlanDutyParamSet, "INSERT");
    }
    return true;
  }

  /**
   * dealGrpPolInfo
   *
   * @return boolean
   */
  private boolean dealGrpPolInfo() {
    for (int i = 1; i <= this.mLMRiskAppSet.size(); i++) {
      /** 校验是否已存在此险种 */
      System.out.println("校验是否已存在此险种");
      LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
      tLCGrpPolDB.setGrpContNo(this.mGrpContNo);
      tLCGrpPolDB.setRiskCode(this.mLMRiskAppSet.get(i).getRiskCode());
      LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
      if (tLCGrpPolSet != null && tLCGrpPolSet.size() >= 1) {
        if (tLCGrpPolSet.size() > 1) {
          String str = "添加险种错误,不能出现两险种相同!";
          buildError("dealGrpPolInfo", str);
          System.out.println("在程序TBGrpWrapBL.dealGrpPolInfo() - 327 : " + str);
          return false;
        }
        if (tLCGrpPolSet.size() == 1) {
          if ("N".equals(tLCGrpPolSet.get(1).getRiskWrapFlag())) {
            String str = "已存在一个非套餐内险种,与套餐险种重复!";
            buildError("dealGrpPolInfo", str);
            System.out.println("在程序TBGrpWrapBL.dealGrpPolInfo() - 334 : " + str);
            return false;
          }
          if ("Y".equals(tLCGrpPolSet.get(1).getRiskWrapFlag())) {
            continue;
          }
        }
      }
      String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
      String mGrpPolNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);

      LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
      tLCGrpPolSchema.setRiskCode(mLMRiskAppSet.get(i).getRiskCode());
      tLCGrpPolSchema.setCValiDate(this.mLCGrpContSchema.getCValiDate());
      tLCGrpPolSchema.setPayIntv(this.mLCGrpContSchema.getPayIntv());
      tLCGrpPolSchema.setExpPeoples(this.mLCGrpContSchema.getExpPeoples());
      tLCGrpPolSchema.setGrpContNo(this.mGrpContNo);
      tLCGrpPolSchema.setGrpPolNo(mGrpPolNo); //如果是新增
      tLCGrpPolSchema.setGrpProposalNo(mGrpPolNo);
      tLCGrpPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
      tLCGrpPolSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
      tLCGrpPolSchema.setSaleChnl(mLCGrpContSchema.getSaleChnl());
      tLCGrpPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
      tLCGrpPolSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
      tLCGrpPolSchema.setAgentType(mLCGrpContSchema.getAgentType());
      tLCGrpPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
      tLCGrpPolSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
      tLCGrpPolSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
      tLCGrpPolSchema.setAddressNo(mLCGrpContSchema.getAddressNo());
      tLCGrpPolSchema.setGrpName(mLCGrpContSchema.getGrpName());
      tLCGrpPolSchema.setComFeeRate(mLMRiskAppSet.get(i).getAppInterest());
      tLCGrpPolSchema.setBranchFeeRate(mLMRiskAppSet.get(i).getAppPremRate());
      tLCGrpPolSchema.setAppFlag("0");
      tLCGrpPolSchema.setUWFlag("0");
      tLCGrpPolSchema.setApproveFlag("0");
      tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
      tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
      tLCGrpPolSchema.setOperator(mGlobalInput.Operator);
      tLCGrpPolSchema.setMakeDate(PubFun.getCurrentDate());
      tLCGrpPolSchema.setMakeTime(PubFun.getCurrentTime());
      tLCGrpPolSchema.setPayMode(mLCGrpContSchema.getPayMode());
      tLCGrpPolSchema.setRiskWrapFlag("Y");
      mLCGrpPolSet.add(tLCGrpPolSchema);
      if (!prepareCalRule(mLMRiskAppSet.get(i), mGrpPolNo)) {
        return false;
      }
    }
    map.put(mLCGrpPolSet, "INSERT");
    return true;
  }

  /**
   * dealCalRule
   *
   * @return boolean
   * @param tLMRiskAppSchema String
   * @param tGrpPolNo String
   */
  private boolean prepareCalRule(LMRiskAppSchema tLMRiskAppSchema, String tGrpPolNo) {
    LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
    tLCContPlanDutyParamSchema.setGrpContNo(this.mGrpContNo);
    tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
    tLCContPlanDutyParamSchema.setRiskCode(tLMRiskAppSchema.getRiskCode());
    tLCContPlanDutyParamSchema.setRiskVersion(tLMRiskAppSchema.getRiskVer());
    tLCContPlanDutyParamSchema.setMainRiskCode(tLMRiskAppSchema.getRiskCode());
    tLCContPlanDutyParamSchema.setMainRiskVersion(tLMRiskAppSchema.getRiskVer());
    tLCContPlanDutyParamSchema.setProposalGrpContNo(this.mGrpContNo);
    tLCContPlanDutyParamSchema.setCalFactorValue("3");
    tLCContPlanDutyParamSchema.setCalFactor("CalRule");
    mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

    LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
    tLCContPlanRiskSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
    tLCContPlanRiskSchema.setProposalGrpContNo(mLCGrpContSchema.getGrpContNo());
    tLCContPlanRiskSchema.setMainRiskCode(tLMRiskAppSchema.getRiskCode());
    tLCContPlanRiskSchema.setMainRiskVersion(tLMRiskAppSchema.getRiskVer());
    tLCContPlanRiskSchema.setRiskCode(tLMRiskAppSchema.getRiskCode());
    tLCContPlanRiskSchema.setRiskVersion(tLMRiskAppSchema.getRiskVer());
    tLCContPlanRiskSchema.setContPlanCode("11");
    tLCContPlanRiskSchema.setContPlanName("默认计划");
    tLCContPlanRiskSchema.setPlanType("0");
    tLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);
    PubFun.fillDefaultField(tLCContPlanRiskSchema);
    mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
    return true;
  }

  /**
   * getRiskInfo
   *
   * @return boolean
   */
  private boolean getRiskInfo() {
    LDRiskWrapDB tLDRiskWrapDB = new LDRiskWrapDB();
    tLDRiskWrapDB.setRiskWrapCode(this.mWrapCode);
    LDRiskWrapSet tLDRiskWrapSet = tLDRiskWrapDB.query();
    if (tLDRiskWrapSet == null || tLDRiskWrapSet.size() <= 0) {
      String str = "查询产品套餐定义失败!";
      buildError("getRiskInfo", str);
      System.out.println("在程序TBGrpWrapBL.getRiskInfo() - 202 : " + str);
      return false;
    }
    String getRiskWherePart = getRiskWherePart(tLDRiskWrapSet);
    if (getRiskWherePart == null || getRiskWherePart.equals("")) {
      String str = "生产查询险种条件错误!";
      buildError("getRiskInfo", str);
      System.out.println("在程序TBGrpWrapBL.getRiskInfo() - 209 : " + str);
      return false;
    }

    LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
    this.mLMRiskAppSet = tLMRiskAppDB.executeQuery("select * from lmriskapp where 1=1 " +
                                                   getRiskWherePart);
    if (this.mLMRiskAppSet == null || this.mLMRiskAppSet.size() <= 0) {
      String str = "套餐对应险种不存在!";
      buildError("getRiskInfo", str);
      System.out.println("在程序TBGrpWrapBL.getRiskInfo() - 220 : " + str);
      return false;
    }
    if (mLMRiskAppSet.size() != tLDRiskWrapSet.size()) {
      String str = "套餐定义与险种定义不符!";
      buildError("getRiskInfo", str);
      System.out.println("在程序TBGrpWrapBL.getRiskInfo() - 226 : " + str);
      return false;
    }

    return true;
  }

  /**
   * getRiskWherePart
   *
   * @param tLDRiskWrapSet LDRiskWrapSet
   * @return String
   */
  private String getRiskWherePart(LDRiskWrapSet tLDRiskWrapSet) {
    String sql = " and RiskCode in (";
    for (int i = 1; i <= tLDRiskWrapSet.size(); i++) {
      sql += "'";
      sql += tLDRiskWrapSet.get(i).getRiskCode();
      sql += "'";
      if (i != tLDRiskWrapSet.size()) {
        sql += ",";
      }
    }
    sql += ") ";
    return sql;
  }

  /**
   * prepareOutputData
   *
   * @return boolean
   */
  private boolean prepareOutputData() {
    System.out.println("into BriefCardSignBL.prepareOutputData()...");
    this.mResult.add(map);
    return true;
  }

  /**
   * 出错处理
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();
    cError.moduleName = "BriefCardSignBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

}
