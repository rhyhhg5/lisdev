package com.sinosoft.lis.tb;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class TB
{
    public TB()
    {
    }

    /**国际业务工作流起始*/
    public static final String PROCESSID_INTL = "0000000008";

    /**国际业务扫描录入*/
    public static final String ACTIVITYID_SCAN_INPUT_INTL = "0000008001";

    public static void main(String[] args)
    {
        TB tb = new TB();
    }
}
