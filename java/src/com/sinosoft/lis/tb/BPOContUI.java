package com.sinosoft.lis.tb;

import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOContUI
{
    public CErrors mErrors = new CErrors();

    public BPOContUI()
    {
    }

    public boolean submitData(VData data, String operator)
    {
        BPOContBL tBPOContBL = new BPOContBL();
        if(!tBPOContBL.submitData(data, operator))
        {
            mErrors.copyAllErrors(tBPOContBL.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        BPOContUI bpocontui = new BPOContUI();
    }
}
