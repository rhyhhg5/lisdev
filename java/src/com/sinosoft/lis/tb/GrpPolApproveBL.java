package com.sinosoft.lis.tb;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-17
 */
public class GrpPolApproveBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 数据操作字符串 */
  private String mOperate;
  private String mOperator;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  /** 业务处理相关变量 */
  /** 保单数据 */
  private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
  private LCPolSet mLCPolSet = new LCPolSet();

  /** 复核标记 */
  private String mApproveFlag = "";

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  // @Constructor
  public GrpPolApproveBL() {}

  /**
   * 数据提交的公共方法
   * @param: cInputData 传入的数据
   *		  cOperate 数据操作字符串
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    // 将传入的数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    // 将外部传入的数据分解到本类的属性中，准备处理
    if (this.getInputData() == false)
      return false;
    System.out.println("---getInputData---");

    // 校验传入的数据
    if (this.checkData() == false)
      return false;
    System.out.println("---checkData---");

    // 根据业务逻辑对数据进行处理
    if (this.dealData() == false)
      return false;
    System.out.println("---dealData---");

    // 装配处理好的数据，准备给后台进行保存
    this.prepareOutputData();
    System.out.println("---prepareOutputData---");

    //　数据提交、保存
    GrpPolApproveBLS tGrpPolApproveBLS = new GrpPolApproveBLS();
    if (tGrpPolApproveBLS.submitData(mInputData, cOperate) == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tGrpPolApproveBLS.mErrors);
      return false;
    }
    System.out.println("---commitData---");

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {
    String tProposalGrpContNo = "";

    //全局变量
    mGlobalInput.setSchema( (GlobalInput) mInputData.getObjectByObjectName(
        "GlobalInput", 0));
    //保单
    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    tLCGrpContSchema.setSchema( (LCGrpContSchema) mInputData.
                               getObjectByObjectName("LCGrpContSchema", 0));
    mApproveFlag = tLCGrpContSchema.getApproveFlag();

    System.out.println("mApproveFlag:" + mApproveFlag);
    System.out.println("ProposalGrpContNo:" +
                       tLCGrpContSchema.getProposalGrpContNo());

    mOperator = mGlobalInput.Operator;
    if (mOperator == null || mOperator.trim().equals("")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "集体复核中复核员的ＩＤ传入失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (mApproveFlag == null || StrTool.cTrim(mApproveFlag).equals("")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "集体复核标记没有传入!";
      this.mErrors.addOneError(tError);
      return false;
    }
    tProposalGrpContNo = tLCGrpContSchema.getProposalGrpContNo();

    LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    tLCGrpContDB.setGrpContNo(tProposalGrpContNo);
    if (tLCGrpContDB.getInfo() == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "集体投保单查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLCGrpContSchema.setSchema(tLCGrpContDB);
    /*
        String tRiskCode = mLCGrpContSchema.getRiskCode();
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(tRiskCode);
        if(tLMRiskAppDB.getInfo() == false)
        {
       this.mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "GrpPolApproveBL";
       tError.functionName = "getInputData";
       tError.errorMessage = tRiskCode.trim()+"险种信息查询失败!";
       this.mErrors.addOneError(tError);
       return false;
        }

           System.out.println("SubRiskFlag"+tLMRiskAppDB.getSubRiskFlag()) ;

        if(tLMRiskAppDB.getSubRiskFlag().equals("S") )
        {
        this.mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpPolApproveBL";
        tError.functionName = "getInputData";
        tError.errorMessage = tRiskCode.trim()+"附加险不能单独进行新单复核通过确认操作!";
        this.mErrors.addOneError(tError);
        return false;
        }*/

    // hst	-- 2003-7-18  解决大数据的问题
    /*
       LCPolDB tLCPolDB = new LCPolDB();
       tLCPolDB.setProposalGrpContNo( tProposalGrpContNo );
       mLCPolSet = tLCPolDB.query();
       if( tLCPolDB.mErrors.needDealError() == true )
       {
     // @@错误处理
     mErrors.copyAllErrors( tLCPolDB.mErrors );
     CError tError = new CError();
     tError.moduleName = "GrpPolApproveBL";
     tError.functionName = "dealData";
     tError.errorMessage = "集体投保单下的个人投保单取数失败!";
     this.mErrors .addOneError(tError) ;
     return false;
       }
     */

    return true;
  }

  /**
   * 校验传入的数据
   * @param: 无
   * @return: boolean
   */
  private boolean checkData() {
    if (mLCGrpContSchema.getAppFlag() == null ||StrTool.cTrim(mLCGrpContSchema.getAppFlag()).equals(""))
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "checkData";
      tError.errorMessage = "数据库中数据有误，可能是非正常入库";
      this.mErrors.addOneError(tError);
      return false;

    }
    if (!mLCGrpContSchema.getAppFlag().trim().equals("0")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "checkData";
      tError.errorMessage = "此集体单不是投保单，不能进行复核操作!";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (mLCGrpContSchema.getApproveFlag() == null ||mLCGrpContSchema.getApproveFlag().equals(""))
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "checkData";
      tError.errorMessage = "数据库中数据有误，可能是非正常入库";
      this.mErrors.addOneError(tError);
      return false;

    }

    if (mLCGrpContSchema.getApproveFlag().equals("9")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "checkData";
      tError.errorMessage = "此集体投保单复核已经通过，不能再进行复核通过操作!";
      this.mErrors.addOneError(tError);
      return false;
    }

    // hst	-- 2003-7-18  解决大数据的问题
    /*
       int n = mLCPolSet.size();
       if( n == 0 )
       {
     // @@错误处理
     CError tError = new CError();
     tError.moduleName = "GrpPolApproveBL";
     tError.functionName = "dealData";
     tError.errorMessage = "集体投保单下没有个人投保单，不能进行复核操作!";
     this.mErrors .addOneError(tError) ;
     return false;
       }
     */
    ExeSQL tExeSQL = new ExeSQL();
    String sql = "select count(*) from LCCont "
        + "where GrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() + "'";

    String tStr = "";
    double tCount = -1;
    tStr = tExeSQL.getOneValue(sql);
    if (tStr.trim().equals(""))
      tCount = 0;
    else
      tCount = Double.parseDouble(tStr);

    if (tCount <= 0.0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "dealData";
      tError.errorMessage = "集体投保单下没有个人投保单，不能进行复核操作!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: boolean
   */
  private boolean dealData() {
    // 修改集体投保单下个人投保单的复核人编码和复核日期
    // hst	-- 2003-7-18  解决大数据的问题
    /*
       int n = mLCPolSet.size();
       for( int i = 1; i <= n; i++ )
       {
     LCPolSchema tLCPolSchema = new LCPolSchema();
     tLCPolSchema = ( LCPolSchema )mLCPolSet.get( i );

     tLCPolSchema.setApproveCode( mGlobalInput.Operator );
     tLCPolSchema.setApproveDate( PubFun.getCurrentDate() );
     tLCPolSchema.setApproveFlag( mApproveFlag );
     tLCPolSchema.setModifyDate( PubFun.getCurrentDate() );
     tLCPolSchema.setModifyTime( PubFun.getCurrentTime() );

     mLCPolSet.set( i, tLCPolSchema );
       }
     */

    //判断团体主险投保单下是否有操作员的问题件,有则置该团体保单状态为待复核修改
    ExeSQL tExeSQL = new ExeSQL();
    String sql = "select count(*) from LCGrpIssuePol "
        + "where ProposalGrpContNo = '" + mLCGrpContSchema.getProposalGrpContNo() +
        "'"
        + "and replyman is null "
        + "and backobjtype = '1'";

    String tStr = "";
    double tCount = -1;
    tStr = tExeSQL.getOneValue(sql);
    if (tStr.trim().equals(""))
      tCount = 0;
    else
      tCount = Double.parseDouble(tStr);

    if (tCount > 0.0) {
      mApproveFlag = "1";
    }
    System.out.print(" mApproveFlag: " + mApproveFlag);

    String tCurrentDate = PubFun.getCurrentDate();
    String tCurrentTime = PubFun.getCurrentTime();
    // 修改集体投保单复核人编码和复核日期
    mLCGrpContSchema.setApproveCode(mOperator);
    mLCGrpContSchema.setApproveDate(tCurrentDate);
    mLCGrpContSchema.setApproveFlag(mApproveFlag);
    mLCGrpContSchema.setModifyDate(tCurrentDate);
    mLCGrpContSchema.setModifyTime(tCurrentTime);
    mLCGrpContSchema.setSpecFlag("0");
    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: void
   */
  private void prepareOutputData() {
    mInputData.clear();
    mInputData.add(mLCGrpContSchema);

    // hst	-- 2003-7-18  解决大数据的问题
    //mInputData.add( mLCPolSet );
  }

}
