package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpContRoadDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDPromiseRateDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCDelPolLogSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCDelPolLogSet;
import com.sinosoft.lis.vschema.LCGrpContRoadSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LDPromiseRateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class GroupRiskBL {
    /** 传入数据的容器 */
    private VData mInputData = new VData();


    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();
    private TransferData tTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperate;


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();


    /** 业务处理相关变量 */
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSet mOLDLCGrpPolSet = new LCGrpPolSet();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCDelPolLogSet mLCDelPolLogSet = new LCDelPolLogSet();
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new
            LCContPlanDutyParamSet();

    // @Constructor
    public GroupRiskBL() {}

  /**
   * 数据提交的公共方法
   *
   * @param: cInputData 传入的数据 cOperate 数据操作字符串
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData cInputData, String cOperate) {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false) {
            return false;
        }
        System.out.println("---getInputData---");

        // 校验传入的数据
        if (!mOperate.equals("DELETE||GROUPRISK")) {
            if (this.checkData() == false) {
                return false;
            }
            System.out.println("---checkData---");
        }

        // 根据业务逻辑对数据进行处理
        if (!mOperate.equals("DELETE||GROUPRISK")) {
            if (this.dealData() == false) {
                return false;
            }
        } else {
            if (this.deleteData() == false) {
                return false;
            }
        }

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");
        //　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start GroupRiskBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---commitData---");

        return true;
    }

  /**
   * 将外部传入的数据分解到本类的属性中
   *
   * @return boolean
   */
  private boolean getInputData() {

        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        //集体合同
        mLCGrpContSchema.setSchema((LCGrpContSchema) mInputData.
                                   getObjectByObjectName("LCGrpContSchema", 0));

        //集体险种
        mLCGrpPolSet.set((LCGrpPolSet) mInputData.
                         getObjectByObjectName("LCGrpPolSet", 0));
        tTransferData = (TransferData) mInputData.
                        getObjectByObjectName("TransferData", 0);
        mLCContPlanDutyParamSet = (LCContPlanDutyParamSet) mInputData.
                                  getObjectByObjectName(
                                          "LCContPlanDutyParamSet", 0);
        if (mLCGrpContSchema == null || mLCGrpPolSet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GroupRiskBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在接受数据时没有得到集体合同或者集体险种!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCGrpContDB mLCGrpContDB = new LCGrpContDB();
        mLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if (mLCGrpContDB.getInfo() == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GroupRiskBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有查到该集体合同!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema.setSchema(mLCGrpContDB);

        return true;
    }

  /**
   * 校验传入的数据
   *
   * @return: boolean
   */
  private boolean checkData() {
        if (!mOperate.equals("INSERT||GROUPRISK")
            && !mOperate.equals("DELETE||GROUPRISK")) {
            CError tError = new CError();
            tError.moduleName = "GroupRiskBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "只能新增或者删除!";
            this.mErrors.addOneError(tError);
            return false;

        }
        if (mOperate.equals("INSERT||GROUPRISK")) {
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLCGrpPolDB.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            if (tLCGrpPolDB.getCount() > 0) {
                CError tError = new CError();
                tError.moduleName = "GroupRiskBL";
                tError.functionName = "checkData";
                tError.errorMessage = "该集体合同下已经添加过该险种!";
                this.mErrors.addOneError(tError);
                return false;
            }

        }

        return true;
    }

  /**
   * 根据业务逻辑对数据进行处理
   *
   * @return boolean
   */
  private boolean dealData() {
        //产生集体投保单号码
        if (mOperate.equals("INSERT||GROUPRISK")) {
            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            if (tLMRiskDB.getInfo() == false) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GroupRiskBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有查到险种信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            if (tLMRiskAppDB.getInfo() == false) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GroupRiskBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有查到险种信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
            String tNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);
            mLCGrpPolSet.get(1).setGrpPolNo(tNo); //如果是新增
            mLCGrpPolSet.get(1).setGrpProposalNo(tNo);
            mLCGrpPolSet.get(1).setPrtNo(mLCGrpContSchema.getPrtNo());
            mLCGrpPolSet.get(1).setGrpContNo(mLCGrpContSchema.getGrpContNo());
            mLCGrpPolSet.get(1).setSaleChnl(mLCGrpContSchema.getSaleChnl());
            mLCGrpPolSet.get(1).setManageCom(mLCGrpContSchema.getManageCom());
            mLCGrpPolSet.get(1).setAgentCom(mLCGrpContSchema.getAgentCom());
            mLCGrpPolSet.get(1).setAgentType(mLCGrpContSchema.getAgentType());
            mLCGrpPolSet.get(1).setAgentCode(mLCGrpContSchema.getAgentCode());
            mLCGrpPolSet.get(1).setAgentGroup(mLCGrpContSchema.getAgentGroup());
            mLCGrpPolSet.get(1).setCustomerNo(mLCGrpContSchema.getAppntNo());
            mLCGrpPolSet.get(1).setAddressNo(mLCGrpContSchema.getAddressNo());
            mLCGrpPolSet.get(1).setGrpName(mLCGrpContSchema.getGrpName());
            mLCGrpPolSet.get(1).setComFeeRate(tLMRiskAppDB.getAppInterest());
            mLCGrpPolSet.get(1).setBranchFeeRate(tLMRiskAppDB.getAppPremRate());
            mLCGrpPolSet.get(1).setAppFlag("0");
            mLCGrpPolSet.get(1).setStateFlag("0");
            mLCGrpPolSet.get(1).setUWFlag("0");
            mLCGrpPolSet.get(1).setApproveFlag("0");
            mLCGrpPolSet.get(1).setModifyDate(PubFun.getCurrentDate());
            mLCGrpPolSet.get(1).setModifyTime(PubFun.getCurrentTime());
            mLCGrpPolSet.get(1).setOperator(mGlobalInput.Operator);
            mLCGrpPolSet.get(1).setMakeDate(PubFun.getCurrentDate());
            mLCGrpPolSet.get(1).setMakeTime(PubFun.getCurrentTime());
            mLCGrpPolSet.get(1).setPayMode(mLCGrpContSchema.getPayMode());
            mLCGrpPolSet.get(1).setRiskWrapFlag("N");
            System.out.println("paymode+株株"+mLCGrpContSchema.getPayMode());
            tTransferData.setNameAndValue("RiskName", tLMRiskDB.getRiskName());
            map.put(mLCGrpPolSet, "INSERT");
            /**
             * 处理默认要素
             */
            if (mLCContPlanDutyParamSet != null) {
                for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++) {
                    LMRiskDutyFactorDB tLMRiskDutyFactorDB = new
                            LMRiskDutyFactorDB();
                    tLMRiskDutyFactorDB.setRiskCode(mLCContPlanDutyParamSet.get(
                            i).getRiskCode());
                    tLMRiskDutyFactorDB.setCalFactor(mLCContPlanDutyParamSet.
                            get(i).getCalFactor());
                    tLMRiskDutyFactorDB.setDutyCode("000000");
                    tLMRiskDutyFactorDB.setPayPlanCode("000000");
                    tLMRiskDutyFactorDB.setGetDutyCode("000000");
                    tLMRiskDutyFactorDB.setInsuAccNo("000000");
                    if (!tLMRiskDutyFactorDB.getInfo()) {
                        System.out.println(
                                "程序第247行出错，请检查GroupRiskBL.java中的dealData方法！");
                        CError tError = new CError();
                        tError.moduleName = "GroupRiskBL.java";
                        tError.functionName = "dealData";
                        tError.errorMessage = "未查询到" +
                                              mLCContPlanDutyParamSet.get(i).
                                              getCalFactor() +
                                              "默认要素！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    mLCContPlanDutyParamSet.get(i).setGrpPolNo(tNo);
                    mLCContPlanDutyParamSet.get(i).setMainRiskCode(tLMRiskDB.
                            getRiskCode());
                    mLCContPlanDutyParamSet.get(i).setMainRiskVersion(tLMRiskDB.
                            getRiskVer());
                    mLCContPlanDutyParamSet.get(i).setRiskCode(tLMRiskDB.
                            getRiskCode());
                    mLCContPlanDutyParamSet.get(i).setRiskVersion(tLMRiskDB.
                            getRiskCode());
                    mLCContPlanDutyParamSet.get(i).setContPlanCode("11");
                    mLCContPlanDutyParamSet.get(i).setContPlanName("默认计划");
                    mLCContPlanDutyParamSet.get(i).setDutyCode(
                            tLMRiskDutyFactorDB.getDutyCode());
                    mLCContPlanDutyParamSet.get(i).setCalFactorType(
                            tLMRiskDutyFactorDB.
                            getCalFactorType());
                    mLCContPlanDutyParamSet.get(i).setPlanType("0");
                    mLCContPlanDutyParamSet.get(i).setPayPlanCode(
                            tLMRiskDutyFactorDB.
                            getPayPlanCode());
                    mLCContPlanDutyParamSet.get(i).setGetDutyCode(
                            tLMRiskDutyFactorDB.
                            getGetDutyCode());
                    mLCContPlanDutyParamSet.get(i).setInsuAccNo(
                            tLMRiskDutyFactorDB.getInsuAccNo());
                }
                LCContPlanRiskSchema tLCContPlanRiskSchema = new
                        LCContPlanRiskSchema();
                tLCContPlanRiskSchema.setGrpContNo(mLCGrpContSchema.
                        getGrpContNo());
                tLCContPlanRiskSchema.setProposalGrpContNo(mLCGrpContSchema.
                        getGrpContNo());
                tLCContPlanRiskSchema.setMainRiskCode(tLMRiskDB.getRiskCode());
                tLCContPlanRiskSchema.setMainRiskVersion(tLMRiskDB.getRiskVer());
                tLCContPlanRiskSchema.setRiskCode(tLMRiskDB.getRiskCode());
                tLCContPlanRiskSchema.setRiskVersion(tLMRiskDB.getRiskVer());
                tLCContPlanRiskSchema.setContPlanCode("11");
                tLCContPlanRiskSchema.setContPlanName("默认计划");
                tLCContPlanRiskSchema.setPlanType("0");
                tLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(tLCContPlanRiskSchema);
                //如果默认计划已经存在则不添加计划
                StringBuffer sql = new StringBuffer(255);
                sql.append("select count(1) from lccontplan where grpcontno='");
                sql.append(mLCGrpContSchema.getGrpContNo());
                sql.append("' and contplancode='11'");
                ExeSQL tExeSQL = new ExeSQL();
                if (Integer.parseInt(tExeSQL.getOneValue(sql.toString())) <= 0) {
                    LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
                    tLCContPlanSchema.setGrpContNo(mLCGrpContSchema.
                            getGrpContNo());
                    tLCContPlanSchema.setProposalGrpContNo(mLCGrpContSchema.
                            getGrpContNo());
                    tLCContPlanSchema.setContPlanName("默认计划");
                    tLCContPlanSchema.setContPlanCode("11");
                    tLCContPlanSchema.setPlanType("0");
                    tLCContPlanSchema.setOperator(mGlobalInput.Operator);
                    PubFun.fillDefaultField(tLCContPlanSchema);
                    map.put(tLCContPlanSchema, "INSERT");
                }
                map.put(tLCContPlanRiskSchema, "INSERT");
                map.put(mLCContPlanDutyParamSet, "INSERT");
            }
        }
        return true;
    }

  /**
   * 删除传入的险种
   *
   * @return boolean
   */
  private boolean deleteData() {

        for (int i = 1; i <= mLCGrpPolSet.size(); i++) {
            String riskcode = mLCGrpPolSet.get(i).getRiskCode();
            String GrpContNo = mLCGrpPolSet.get(i).getGrpContNo();
            String wherepart = "RiskCode ='" + riskcode + "' and GrpContNo = '"
                               + GrpContNo + "'";
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setGrpContNo(GrpContNo);
            tLCPolDB.setRiskCode(riskcode);
          //判断是否应该删除一带一路要素
            if(riskcode=="163001"||"163001".equals(riskcode)||riskcode=="163002"||"163002".equals(riskcode)){
            	String prtno= mLCGrpContSchema.getPrtNo();
            	LCGrpContRoadDB tLCGrpContRoadDB = new LCGrpContRoadDB();
            	tLCGrpContRoadDB.setPrtNo(prtno);
            	LCGrpContRoadSet tLCGrpContRoadSet = tLCGrpContRoadDB.query();
            	if (tLCGrpContRoadSet.size()>= 0) {
            		map.put(tLCGrpContRoadSet, SysConst.DELETE);
                }
            }
            //判断是否删除最低保证利率
            if(riskcode=="370301"||"370301".equals(riskcode)){
            	String prtno= mLCGrpContSchema.getPrtNo();
            	LDPromiseRateDB tLDPromiseRateDB = new LDPromiseRateDB();
            	tLDPromiseRateDB.setPrtNo(prtno);
            	LDPromiseRateSet tLDPromiseRateSet = tLDPromiseRateDB.query();
            	if (tLDPromiseRateSet.size()>= 0) {
            		map.put(tLDPromiseRateSet, SysConst.DELETE);
                }
            }
            int polCount = tLCPolDB.getCount();
            if (tLCPolDB.mErrors.needDealError()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GroupRiskBL";
                tError.functionName = "deleteData";
                tError.errorMessage = "查询个人险种保单时失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (polCount > 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GroupRiskBL";
                tError.functionName = "deleteData";
                tError.errorMessage = "险种" + riskcode
                                      + "下还有个人保单，请先删除险种下个人保单再删除险种！";
                this.mErrors.addOneError(tError);
                return false;
            }
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setGrpContNo(GrpContNo);
            tLCContPlanRiskDB.setRiskCode(riskcode);
            StringBuffer sql = new StringBuffer(255);
            sql.append("select * from lccontplanrisk where grpcontno='");
            sql.append(GrpContNo);
            sql.append("' and riskcode='");
            sql.append(riskcode);
            sql.append("' and contplancode<>'11'");
            int planRiskCount = tLCContPlanRiskDB.executeQuery(sql.toString()).
                                size();
            if (tLCContPlanRiskDB.mErrors.needDealError()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GroupRiskBL";
                tError.functionName = "deleteData";
                tError.errorMessage = "查询保险计划时失败!";
                this.mErrors.addOneError(tError);
                return false;

            }
            if (planRiskCount > 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GroupRiskBL";
                tError.functionName = "deleteData";
                tError.errorMessage = "险种" + riskcode
                                      + "还在被保险计划使用，请先从保险计划中删除该险种！";
                this.mErrors.addOneError(tError);
                return false;
            }

            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpContNo(GrpContNo);
            tLCGrpPolDB.setRiskCode(riskcode);
            LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
            if (tLCGrpPolSet == null || tLCGrpPolSet.size() <= 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "请您确认：要删除的集体险种代码传入错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();
            LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
            tLCGrpPolSchema.setSchema(tLCGrpPolSet.get(1));
            mLCDelPolLog.setOtherNo(tLCGrpPolSchema.getGrpPolNo());
            mLCDelPolLog.setOtherNoType("1");
            mLCDelPolLog.setPrtNo(tLCGrpPolSchema.getPrtNo());
            if (tLCGrpPolSchema.getAppFlag().equals("1")) {
                mLCDelPolLog.setIsPolFlag("1");
            } else {
                mLCDelPolLog.setIsPolFlag("0");
            }
            mLCDelPolLog.setOperator(mGlobalInput.Operator);
            mLCDelPolLog.setManageCom(mGlobalInput.ManageCom);
            mLCDelPolLog.setMakeDate(PubFun.getCurrentDate());
            mLCDelPolLog.setMakeTime(PubFun.getCurrentTime());
            mLCDelPolLog.setModifyDate(PubFun.getCurrentDate());
            mLCDelPolLog.setModifyTime(PubFun.getCurrentTime());
            mLCDelPolLogSet.add(mLCDelPolLog);
            if (i == mLCGrpPolSet.size()) {
                map.put(mLCDelPolLogSet, "INSERT");
            }
            map.put("insert into LOBGrpPol (select * from LCGrpPol where "
                    + wherepart + ")", "INSERT");
            map.put("delete from LCGrpPol where " + wherepart, "DELETE");
            //map.put("delete from LCPol where "+wherepart,"DELETE");
            //删除默认计划要素
            StringBuffer strSql = new StringBuffer();
            strSql.append("delete from LCContPlanDutyParam ");
            strSql.append("where grpcontno='");
            strSql.append(this.mLCGrpPolSet.get(i).getGrpContNo());
            strSql.append("' and GrpPolNo='");
            strSql.append(tLCGrpPolSchema.getGrpPolNo());
            strSql.append("' and RiskCode='");
            strSql.append(tLCGrpPolSchema.getRiskCode());
            strSql.append("' and ContPlanCode='11' ");
            map.put(strSql.toString(), "DELETE");
            //删除默认险种计划
            strSql = new StringBuffer();
            strSql.append("delete from LCContPlanRisk ");
            strSql.append("where grpcontno='");
            strSql.append(this.mLCGrpPolSet.get(i).getGrpContNo());
            strSql.append("' and RiskCode='");
            strSql.append(tLCGrpPolSchema.getRiskCode());
            strSql.append("' and ContPlanCode='11' ");
            map.put(strSql.toString(), "DELETE");
            
            String sql1 = "select 1 from LCRiskZTFee where grpcontno='" + this.mLCGrpPolSet.get(i).getGrpContNo()
            			+ "' and riskcode='" 
            			+ tLCGrpPolSchema.getRiskCode()
            			+ "'";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS mSSRSSelf = new SSRS();
            mSSRSSelf = tExeSQL.execSQL(sql1);
            if (mSSRSSelf.getMaxRow() > 0){
            	strSql = new StringBuffer();
        		strSql.append("delete from LCRiskZTFee ");
        		strSql.append("where grpcontno='");
        		strSql.append(this.mLCGrpPolSet.get(i).getGrpContNo());
        		strSql.append("' and RiskCode='");
        		strSql.append(tLCGrpPolSchema.getRiskCode());
             	strSql.append("' ");
             	map.put(strSql.toString(), "DELETE");
            }        
        }
        //判断是否应该删除总计划信息
        StringBuffer contPlan = new StringBuffer(255);
        contPlan.append("select count(1) from lccontplanrisk where grpcontno='");
        contPlan.append(this.mLCGrpContSchema.getGrpContNo());
        contPlan.append("'");
        ExeSQL tExeSQL = new ExeSQL();
        if (this.mLCDelPolLogSet.size() ==
            Integer.parseInt(tExeSQL.getOneValue(contPlan.toString()))) {
            //删除默认险种计划
            contPlan = new StringBuffer();
            contPlan.append("delete from LCContPlan ");
            contPlan.append("where grpcontno='");
            contPlan.append(this.mLCGrpContSchema.getGrpContNo());
            contPlan.append("' and ContPlanCode='11' ");
            map.put(contPlan.toString(), "DELETE");
        }
        if (mLCGrpPolSet.size() > 0) {
            String strSql = "select count(1) from lcpol where grpcontno ='" +
                            mLCGrpPolSet.get(1).getGrpContNo() + "'";
            if (Integer.parseInt(tExeSQL.getOneValue(strSql)) > 0) {
                String fromPart = "from LCPol where GrpContNo='"
                                  + mLCGrpPolSet.get(1).getGrpContNo() + "')";
                map.put("update LCGrpCont set "
                        + "Prem=(select to_zero(SUM(Prem)) " + fromPart
                        + ", Amnt=(select to_zero(SUM(Amnt)) " + fromPart
                        + ", SumPrem=(select to_zero(SUM(SumPrem)) " + fromPart
                        + ", Mult=(select to_zero(SUM(Mult)) " + fromPart
                        +
                        ", Peoples2=(select to_zero(sum(Peoples)) from LCCont where GrpContNo= '" +
                        mLCGrpPolSet.get(1).getGrpContNo() + "')"
                        + " where GrpContNo='" +
                        mLCGrpPolSet.get(1).getGrpContNo() +
                        "'"
                        , "UPDATE");
            }
        }

        return true;
    }

  /**
   * 根据业务逻辑对数据进行处理
   *
   */
  private void prepareOutputData() {
        mResult.clear();
        mResult.add(mLCGrpPolSet);
        mResult.add(tTransferData);
        mResult.add(mLCContPlanDutyParamSet);
        mInputData.clear();
        mInputData.add(map);

    }


    /**
     * 得到处理后的结果集
     * @return 结果集
     */

    public VData getResult() {
        return mResult;
    }

}
