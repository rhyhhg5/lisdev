package com.sinosoft.lis.tb;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class JSFLUI {
public CErrors mErrors = new CErrors();
	
	private JSFLBL jstlBL;
	
	public JSFLUI() {
		this.jstlBL = new JSFLBL();
	}
	
	public boolean submiteData(VData cInputData) {
		if(!jstlBL.submitData(cInputData))
        {
            mErrors.copyAllErrors(jstlBL.mErrors);
            return false;
        }
        return true;
	}
	

}
