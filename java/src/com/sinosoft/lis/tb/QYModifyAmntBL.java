/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QYModifyAmntBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();


	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	//录入
	private String mGrpContNo = "";
	private String mContPlanCode = "";
	private String mRiskCode = "";
	private String mDutyCode = "";
	private String mContType = "";
	private String mModifyAmnt = "";
	private String mOldAmnt = "";
	private String mAmntType = "";

	private MMap mMap = new MMap();

	public QYModifyAmntBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start QYModifyAmntBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		
		mGrpContNo = (String)tTransferData.getValueByName("GrpContNo");
		if (mGrpContNo == null || "".equals(mGrpContNo)){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保单号为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mContType = (String)tTransferData.getValueByName("ContType");
		if (mContType == null || "".equals(mContType)){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保单类型为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if("2".equals(mContType)){
			mContPlanCode = (String)tTransferData.getValueByName("ContPlanCode");
			if (mContPlanCode == null || "".equals(mContPlanCode)){
				CError tError = new CError();
				tError.moduleName = "QYModifyAmntBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "保障计划编码为空！";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		mRiskCode = (String)tTransferData.getValueByName("RiskCode");
		if (mRiskCode == null || "".equals(mRiskCode)){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "险种编码为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mDutyCode = (String)tTransferData.getValueByName("DutyCode");
		if (mDutyCode == null || "".equals(mDutyCode)){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "责任编码为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		mModifyAmnt = (String)tTransferData.getValueByName("ModifyAmnt");
		if (mModifyAmnt == null || "".equals(mModifyAmnt)){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "变更后保额为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mOldAmnt = (String)tTransferData.getValueByName("OldAmnt");
		if (mOldAmnt == null || "".equals(mOldAmnt)){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "变更前保额为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mAmntType = (String)tTransferData.getValueByName("AmntType");
		if (mAmntType == null || "".equals(mAmntType)){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保额类型为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	private boolean dealData(){
		
		String newDate ="modifydate = '"+CurrentDate+"',modifytime = '"+CurrentTime+"' ";
		
		String tWhere = "";
		String tWhere1 = "";
		String tWhere2 = "";
		if("1".equals(mContType)){
			tWhere = " where contno = '"+mGrpContNo+"' and riskcode = '"+mRiskCode+"' and dutycode = '"+mDutyCode+"' ";
			tWhere1 = " where contno = '"+mGrpContNo+"' and riskcode = '"+mRiskCode+"' ";
			tWhere2 = "where contno = '"+mGrpContNo+"' ";
		}else if("2".equals(mContType)){
			tWhere = " where grpcontno = '"+mGrpContNo+"' and contplancode = '"+mContPlanCode+"' and riskcode = '"+mRiskCode+"' and dutycode = '"+mDutyCode+"' ";
			tWhere1 = " where grpcontno = '"+mGrpContNo+"' and contplancode = '"+mContPlanCode+"' and riskcode = '"+mRiskCode+"' ";
			tWhere2 = "where grpcontno = '"+mGrpContNo+"' ";
		}else{
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单类型失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		String tSQL1 = "select distinct standmoney from lcget where polno in (select polno from lcpol "+tWhere1+") and dutycode like '%"+mDutyCode+"%' ";
		SSRS tSSRS1 = new ExeSQL().execSQL(tSQL1);
		if(tSSRS1 == null || tSSRS1.MaxRow != 1){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取变更前保额失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(Double.parseDouble(mOldAmnt) != Double.parseDouble(tSSRS1.GetText(1, 1))){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "变更前保额数据存在不一致情况！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		String tSQL2 = "select distinct amnt from lcduty where polno in (select polno from lcpol "+tWhere1+") and dutycode like '%"+mDutyCode+"%' ";
		SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
		if(tSSRS2 == null || tSSRS2.MaxRow != 1){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取变更前责任保额失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(Double.parseDouble(mOldAmnt) != Double.parseDouble(tSSRS2.GetText(1, 1))){
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "变更前责任保额数据存在不一致情况！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		String tSQLGet = "update lcget set standmoney = "+mModifyAmnt+",actuget = "+mModifyAmnt+","+newDate+" where polno in (select polno from lcpol "+tWhere1+") and dutycode like '%"+mDutyCode+"%' ";
		String tSQLDuty = "update lcduty set amnt = "+mModifyAmnt+",riskamnt = "+mModifyAmnt+","+newDate+" where polno in (select polno from lcpol "+tWhere1+") and dutycode like '%"+mDutyCode+"%' ";
		String tSQLPol = "update lcpol lcp set lcp.amnt = (select sum(lcd.amnt) from lcduty lcd where lcd.polno = lcp.polno),lcp.riskamnt = (select sum(lcd.riskamnt) from lcduty lcd where lcd.polno = lcp.polno),"+newDate+ tWhere1;
		String tSQLCont = "update lccont lcc set lcc.amnt = (select sum(lcp.amnt) from lcpol lcp where lcp.contno = lcc.contno ) ,"+newDate+tWhere2;
		
		mMap.put(tSQLGet, SysConst.UPDATE);
		mMap.put(tSQLDuty, SysConst.UPDATE);
		mMap.put(tSQLPol, SysConst.UPDATE);
		mMap.put(tSQLCont, SysConst.UPDATE);
		
		if("2".equals(mContType)){
			String tSQLContPlanP = "";
			if("1".equals(mAmntType)){//处理保额
				String tSQL3 = "select calfactorvalue from lccontplandutyparam "+tWhere+" and calfactor = 'DutyAmnt' ";
				SSRS tSSRS3 = new ExeSQL().execSQL(tSQL3);
				if(tSSRS3 != null && tSSRS3.MaxRow == 1){
					String tParamValue = tSSRS3.GetText(1, 1);
					if(tParamValue == null || "".equals(tParamValue)){
						tParamValue = "0";
					}
					if(Double.parseDouble(mOldAmnt) == Double.parseDouble(tParamValue)){
						tSQLContPlanP = "update lccontplandutyparam set calfactorvalue = '"+mModifyAmnt+"' "+tWhere+" and calfactor = 'DutyAmnt'";
					}else{
						CError tError = new CError();
						tError.moduleName = "QYModifyAmntBL";
						tError.functionName = "getInputData";
						tError.errorMessage = "变更前责任保额与保障计划不一致！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}else{
					CError tError = new CError();
					tError.moduleName = "QYModifyAmntBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "应变更限额，或获取保障计划数据失败！";
					this.mErrors.addOneError(tError);
					return false;
				}
			}else{
				String tSQL4 = "select calfactorvalue from lccontplandutyparam "+tWhere+" and calfactor = 'Amnt' ";
				SSRS tSSRS4 = new ExeSQL().execSQL(tSQL4);
				if(tSSRS4 != null && tSSRS4.MaxRow == 1){
					String tParamValue = tSSRS4.GetText(1, 1);
					if(tParamValue == null || "".equals(tParamValue)){
						tParamValue = "0";
					}
					if(Double.parseDouble(mOldAmnt) == Double.parseDouble(tParamValue)){
						tSQLContPlanP = "update lccontplandutyparam set calfactorvalue = '"+mModifyAmnt+"' "+tWhere+" and calfactor = 'Amnt'";
					}else{
						CError tError = new CError();
						tError.moduleName = "QYModifyAmntBL";
						tError.functionName = "getInputData";
						tError.errorMessage = "变更前责任保额与保障计划不一致！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}else{
					CError tError = new CError();
					tError.moduleName = "QYModifyAmntBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "应变更保额，或获取保障计划数据失败！";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			
			String tSQLGPol = "update lcgrppol lgp set lgp.amnt = (select sum(lcp.amnt) from lcpol lcp where lcp.grpcontno = '"+mGrpContNo+"' and lcp.riskcode = '"+mRiskCode+"'),"+newDate+" where lgp.grpcontno = '"+mGrpContNo+"' and lgp.riskcode = '"+mRiskCode+"' ";
			String tSQLGCont = "update lcgrpcont lgc set lgc.amnt = (select sum(lcp.amnt) from lcpol lcp where lcp.grpcontno = '"+mGrpContNo+"'),"+newDate+" where lgc.grpcontno = '"+mGrpContNo+"' ";
			
			mMap.put(tSQLContPlanP, SysConst.UPDATE);
			mMap.put(tSQLGPol, SysConst.UPDATE);
			mMap.put(tSQLGCont, SysConst.UPDATE);
			
		}
		
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
