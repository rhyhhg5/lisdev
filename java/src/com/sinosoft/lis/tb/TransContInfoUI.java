package com.sinosoft.lis.tb;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class TransContInfoUI {

	public CErrors mErrors = new CErrors();
	
	/** 往前面传输数据的容器 */
    private String mResult = "";
	
	public boolean submitData(VData cInputData, String cOperate) {
		
		TransContInfoBL tTransContInfoBL = new TransContInfoBL();//BL处理类
		
		if (tTransContInfoBL.submitData(cInputData, cOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tTransContInfoBL.mErrors);
			return false;
		}
		
		return true;
	}
	
	/**
     * 得到处理后的结果集
     * @return 结果集
     */

    public String getResult()
    {
        return mResult;
    }
	
}
