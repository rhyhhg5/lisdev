package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @author zhangjianbao
 *
 */
public class ModifyContInfo
{
    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();
    
    private MMap map = new MMap();

    private TransferData transferData = new TransferData();

    private GlobalInput globalInput = new GlobalInput();
    
    private String operate;

    private String prtNo = "";
    
    private String contNo = "";
    
    private String saleChnl = "";
    
    private String agentCom = "";
    
    private String agentCode = "";
    
    private String agentName = "";
    
    private String agentGroup = "";

    public ModifyContInfo()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	operate = cOperate;
        //外部传入数据
        if (!getInputData(cInputData))
        {
            this.bulidError("getInputData", "数据不完整");
            return false;
        }

        //数据合法性
        if (!checkDate())
        {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ModifyContInfo";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    /**
     * 接收数据
     * 
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {

        try
        {
            globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            transferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        }
        catch (Exception e)
        {
            this.mErrors.addOneError("数据不完整！");
            return false;
        }

        return true;
    }

    /**
     * 校验数据
     * 
     * checkDate
     * @return boolean
     */
    private boolean checkDate()
    {
    	prtNo = (String) transferData.getValueByName("PrtNo");
    	contNo = (String) transferData.getValueByName("ContNo");
    	saleChnl = (String) transferData.getValueByName("SaleChnl");
    	agentCom = (String) transferData.getValueByName("AgentCom");
    	agentCode = (String) transferData.getValueByName("AgentCode");
    	agentGroup = (String) transferData.getValueByName("AgentGroup");
    	
    	if(prtNo == null || prtNo.equals("") || contNo == null || contNo.equals(""))
    	{
            CError error = new CError();
            error.moduleName = "ModifyContInfo";
            error.functionName = "checkDate";
            error.errorMessage = "传入的保单数据不全！";
            this.mErrors.addOneError(error);
            return false;
    	}
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setPrtNo(prtNo);
    	LCContSet tLCContSet = tLCContDB.query();
    	if(tLCContSet == null || tLCContSet.size() != 1)
    	{
            CError error = new CError();
            error.moduleName = "ModifyContInfo";
            error.functionName = "checkDate";
            error.errorMessage = "保单信息查询失败！";
            this.mErrors.addOneError(error);
            return false;
    	}
    	LCContSchema tLCContSchema = tLCContSet.get(1);
    	if(!tLCContSchema.getContNo().equals(contNo))
    	{
            CError error = new CError();
            error.moduleName = "ModifyContInfo";
            error.functionName = "checkDate";
            error.errorMessage = "保单信息查询失败！";
            this.mErrors.addOneError(error);
            return false;
    	}
    	if(!tLCContSchema.getAppFlag().equals("0") && !tLCContSchema.getAppFlag().equals("9"))
    	{
            CError error = new CError();
            error.moduleName = "ModifyContInfo";
            error.functionName = "checkDate";
            error.errorMessage = "保单已承保，不能进行修改！";
            this.mErrors.addOneError(error);
            return false;
    	}
    	
    	LAAgentDB tLAAgentDB = new LAAgentDB();
    	tLAAgentDB.setAgentCode(agentCode);
    	LAAgentSet tLAAgentSet = tLAAgentDB.query();
    	if(tLAAgentSet.size() != 1)
    	{
            CError error = new CError();
            error.moduleName = "ModifyContInfo";
            error.functionName = "checkDate";
            error.errorMessage = "业务员信息查询失败！";
            this.mErrors.addOneError(error);
            return false;
    	}
    	agentName = tLAAgentSet.get(1).getName();
    	
        return true;
    }

    /**
     * 处理数据
     * 
     * dealData
     * @return boolean
     */
    private boolean dealData()
    {
    	//修改销售渠道
    	map.put("update LCCont set SaleChnl = '" + saleChnl + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCPol  set SaleChnl = '" + saleChnl + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");

    	//修改代理机构
    	if(agentCom == null || agentCom.equals(""))
    	{
        	map.put("update LCCont         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LCPol          set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJAPay         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where InComeNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJAPayPerson   set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJTempFee      set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSGETCLAIM    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSGETENDORSE  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSPAYB        set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSPAY         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJAGETOTHER    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where OtherNoType='6' and OtherNo in (select PolNo from LCPol where ContNo = '" + contNo + "')", "UPDATE");
        	map.put("update LJAGETENDORSE  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJAGETCLAIM    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSPAYPERSONB  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSPAYPERSON   set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = null, ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	}
    	else
    	{
        	map.put("update LCCont         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LCPol          set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJAPay         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where InComeNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJAPayPerson   set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJTempFee      set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSGETCLAIM    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSGETENDORSE  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSPAYB        set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSPAY         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJAGETOTHER    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNoType='6' and OtherNo in (select PolNo from LCPol where ContNo = '" + contNo + "')", "UPDATE");
        	map.put("update LJAGETENDORSE  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJAGETCLAIM    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSPAYPERSONB  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
        	map.put("update LJSPAYPERSON   set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom = '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	}
    	
    	//修改业务员
    	map.put("update LCUWSub        set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCUWMaster     set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCCUWSub       set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo in (select ProposalContNo from LCCont where ContNo = '" + contNo + "')", "UPDATE");
    	map.put("update LCCUWMaster    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCContReceive  set AgentCode='" + agentCode + "', AgentName='" + agentName + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCContGetPol   set AgentCode='" + agentCode + "', AgentName='" + agentName + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	
        return true;
    }
    
    /**
     * 处理销售渠道变更
     * 
     * dealSaleChnl
     * @return
     */
    private boolean dealSaleChnl()
    {
    	map.put("update LCCont set SaleChnl = '" + saleChnl + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCPol  set SaleChnl = '" + saleChnl + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	
    	return true;
    }
    
    /**
     * 处理业务员信息变更
     * 
     * dealSaleChnl
     * @return
     */
    private boolean dealAgentCode()
    {
        map.put("update LCCont         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCPol          set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJAPay         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where InComeNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJAPayPerson   set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJTempFee      set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSGETCLAIM    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSGETENDORSE  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSPAYB        set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSPAY         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJAGETOTHER    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNoType='6' and OtherNo in (select PolNo from LCPol where ContNo = '" + contNo + "')", "UPDATE");
    	map.put("update LJAGETENDORSE  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJAGETCLAIM    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSPAYPERSONB  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSPAYPERSON   set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCUWSub        set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCUWMaster     set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCCUWSub       set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo in (select ProposalContNo from LCCont where ContNo = '" + contNo + "')", "UPDATE");
    	map.put("update LCCUWMaster    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCContReceive  set AgentCode='" + agentCode + "', AgentName='" + agentName + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCContGetPol   set AgentCode='" + agentCode + "', AgentName='" + agentName + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");

    	return true;
    }
    
    /**
     * 处理代理机构信息变更
     * 
     * dealSaleChnl
     * @return
     */
    private boolean dealAgentCom()
    {
    	map.put("update LCCont         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCPol          set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJAPay         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where InComeNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJAPayPerson   set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJTempFee      set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSGETCLAIM    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSGETENDORSE  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSPAYB        set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSPAY         set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJAGETOTHER    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where OtherNoType='6' and OtherNo in (select PolNo from LCPol where ContNo = '" + contNo + "')", "UPDATE");
    	map.put("update LJAGETENDORSE  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJAGETCLAIM    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSPAYPERSONB  set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LJSPAYPERSON   set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', AgentCom= '" + agentCom + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCUWSub        set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCUWMaster     set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCCUWSub       set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo in (select ProposalContNo from LCCont where ContNo = '" + contNo + "')", "UPDATE");
    	map.put("update LCCUWMaster    set AgentCode='" + agentCode + "', AgentGroup = '" + agentGroup + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCContReceive  set AgentCode='" + agentCode + "', AgentName='" + agentName + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");
    	map.put("update LCContGetPol   set AgentCode='" + agentCode + "', AgentName='" + agentName + "', ModifyDate=Current Date, ModifyTime=Current Time where ContNo = '" + contNo + "'", "UPDATE");

    	return true;
    }

    /**
     * 将数据存放到ListTable中
     * @return ListTable
     */
    private boolean prepareOutputData()
    {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ModifyContInfo";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" + ex.toString();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 写入错误消息
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg)
    {
        CError error = new CError();

        error.moduleName = "ModifyContInfo";
        error.functionName = cFunction;
        error.errorMessage = cErrorMsg;

        this.mErrors.addOneError(error);

    }
    
    /**
     * 获得结果
     * @return VData
     */
    public VData getResult()
    {
        return this.mInputData;
    }

    public static void main(String[] args)
    {
    	ModifyContInfo modifyContInfo = new ModifyContInfo();
    }
}
