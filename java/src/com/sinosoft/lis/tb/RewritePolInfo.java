package com.sinosoft.lis.tb;

import java.lang.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 更新投保单险种操作类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author wzw
 * @version 1.0
 */
public class RewritePolInfo {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /**存放委派者传入数据的容器*/
  private VData mInputData;

  /**存放传递给委派者数据的容器*/
  private VData mResult = new VData();

  /**将数据和操作放在其中*/
  private MMap map = new MMap();

  //业务处理类，处理后的数据封装在MMap中传给外界
  private LCInsuredSet mLCInsuredSet =new LCInsuredSet();
  private LCPolSet mLCPolSet =new LCPolSet();
  private LCDutySet mLCDutySet =new LCDutySet();
  private LCPremSet mLCPremSet=new LCPremSet();
  private LCGetSet mLCGetSet=new LCGetSet();


  public RewritePolInfo() {
  }

  /**
   * 对比投保单下属被保人重要信息有无变更，有则重算保额保费
   * 操作完成后将险种、险种责任、保额、保费数据压入Map，准备
   * 到后台写数据库
   * @param cont Schema
   * Schema参数为团单实例或者是个单实例
   * @return boolean
   */
  public boolean updatePolInfo(Schema cont){
    boolean tReturn=false;
    //判断是团单实例还是个单实例
    if(cont instanceof LCContSchema){
      tReturn= updatePsn((LCContSchema)cont);
    }else if(cont instanceof LCGrpContSchema){
      tReturn= updateGrp((LCGrpContSchema)cont);
    }else{
      mErrors.addOneError(new CError("传入的Schema错误!!!"));
      tReturn= false;
    }
    prepareData();
    return tReturn;
  }
  /**
   * 个单更新
   * @param cont LCContSchema  个单实例对象
   * @return boolean
   */
  private boolean updatePsn(LCContSchema cont){
    LCContSchema tCont=cont;
    LCInsuredDB tLCInsuredDB=new LCInsuredDB();
    tLCInsuredDB.setContNo(tCont.getContNo());
    mLCInsuredSet=tLCInsuredDB.query();    //保单下的被保人集合
    System.out.println("保单下找到"+mLCInsuredSet.size()+"个被保人");
    //个单下存在一个或多个被保人
    if(mLCInsuredSet!=null&&mLCInsuredSet.size()>0){
      LCInsuredSchema tLCInsuredSchema=new LCInsuredSchema();
      LCPolDB tLCPolDB =new LCPolDB();
      LCPolSet tLCPolSet = new LCPolSet();
      LCPolSchema tLCPolSchema=new LCPolSchema();

      //取出mLCInsuredSet中的每个被保人信息并与其险种信息对比
      for(int i=1;i<=mLCInsuredSet.size();i++){
        boolean updateFlag=false;   //涉及保费保额计算的信息是否更改的标志  false-未更改
        tLCInsuredSchema=mLCInsuredSet.get(i);
        tLCPolDB.setContNo(tLCInsuredSchema.getContNo());
        tLCPolDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
        tLCPolSet=tLCPolDB.query() ;     //被保人下的险种集合

        if(tLCPolSet==null||tLCPolSet.size()==0){
          continue;
        }
        updateFlag=balance(tLCInsuredSchema,tLCPolSet.get(1));
        if(updateFlag){
          System.out.println("被保人"+tLCInsuredSchema.getName()
                             +"的重要信息变更\n删除原有保额保费信息");
          if(!addDelSql(tLCInsuredSchema))   //add sql into map
            return false;
        }
        for(int j=1;j<=tLCPolSet.size();j++){
          tLCPolSchema=tLCPolSet.get(j);
          //************先将tLCInsuredSchema中的信息更新到tLCPolSchema中
          if(!upInsurToPol(tLCInsuredSchema,tLCPolSchema)){
            return false;
          }
          //************判断updateFlag标志，如果为true则更改保额保费
          if(updateFlag){
            System.out.println("被保人"+tLCInsuredSchema.getName()
                             +"的重要信息变更\n重算原有保额保费信息");
            //查出保费项，领取项，责任项等加入计算
            LCDutySet tLCDutySet=new LCDutySet();
            LCDutyDB tLCDutyDB=new LCDutyDB();
            tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
            tLCDutySet=tLCDutyDB.query();
            if(tLCDutyDB.mErrors.needDealError()){
              mErrors.copyAllErrors(tLCDutyDB.mErrors);
              return false;
            }
//            LCDutySet tLCDutySet=new LCDutySet();
//            LCDutyDB tLCDutyDB=new LCDutyDB();
//            tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
//            tLCDutySet=tLCDutyDB.query();
//            if(tLCDutyDB.mErrors.needDealError()){
//              mErrors.copyAllErrors(tLCDutyDB.mErrors);
//              return false;
//            }




            LCPremSet tLCPremSet=new LCPremSet();
            LCGetSet  tLCGetSet=new LCGetSet();
            ProposalBL tProposalBL=new ProposalBL();
            VData tvar =new VData();
            GlobalInput tGlobalInput=new GlobalInput();
            TransferData tTransferData=new TransferData();
            tvar.add(tLCPolSchema);
            tvar.add(tCont);
            tvar.add(tGlobalInput);
            tvar.add(tTransferData);
            VData calResult=tProposalBL.appNoChk(tvar,"INSERT||PROPOSAL");
            if(tProposalBL.mErrors.needDealError()){
              mErrors.copyAllErrors(tProposalBL.mErrors);
              return false;
            }

            //准备好计算后的数据
            if(!prepareCalData(calResult)){
              return false;
            }
          }else{
            mLCPolSet.add(tLCPolSchema.getSchema());
          }
        }  //end tLCPolSet Cyc

      }   //end mLCInsuredSet  Cyc
    }  //end if
    return true;
  }

  /**
   * 团单更新
   * @param cont LCGrpContSchema
   * @return boolean
   */
  private boolean updateGrp(LCGrpContSchema cont){
    LCContDB tLCContDB=new LCContDB();
    tLCContDB.setProposalContNo(cont.getProposalGrpContNo());
    LCContSet tLCContSet =tLCContDB.query();
    if(tLCContSet==null||tLCContSet.size()==0){
      mErrors.addOneError(new CError("团单下没有个单，操作失败"));
      return false;
    }
    for(int i=1;i<=tLCContSet.size();i++){
      if(!updatePsn(tLCContSet.get(i)))
        return false;
    }
    return true;
  }
  /**
   * 对比险种中关系到保额保费计算的被保人信息是否有更改
   * @param vLCInsuredSchema LCInsuredSchema
   * @param vLCPolSchema LCPolSchema
   * @return boolean    true--更改  false--未更改
   */
  private boolean balance(LCInsuredSchema vLCInsuredSchema ,LCPolSchema vLCPolSchema){
    //性别
    if(!(vLCInsuredSchema.getSex().trim()).equals(vLCPolSchema.getInsuredSex().trim()))
      return true;
    //生日
    if(!(vLCInsuredSchema.getBirthday().trim()).equals(vLCPolSchema.getInsuredBirthday().trim() ))
      return true;
    //职业类别
    if(vLCInsuredSchema.getOccupationType()!=null){
      if(!(vLCInsuredSchema.getOccupationType().trim()).equals(vLCPolSchema.getOccupationType().trim() ))
        return true;
    }else{
      if(vLCPolSchema.getOccupationType()!=null)
        return true;
    }



    return false;
  }
  /**
   *根据参数提供的被保人信息
   *向map中添加sql语句，用于删除保单险种责任表、领取项表、保费项表、
   *保费项附表1－银行不定期扣款表中的内容
   * @param LCInsuredSchema LCInsuredSchema
   * 参数为被保人实例
   * @return boolean
   */
  private boolean addDelSql(LCInsuredSchema LCInsuredSchema){
    String ContNO=LCInsuredSchema.getContNo();
    String InsuredNo=LCInsuredSchema.getInsuredNo();
    if(ContNO==null||ContNO.equals("")||InsuredNo==null||InsuredNo.equals("")){
      mErrors.addOneError(new CError("addDelSql方法出错：信息不完整，合同号或被保人信息缺少"));
      return false;
    }
    String subStr0="select PolNo from LCInsured where ContNO ='"+ContNO+"' and "
                   +" InsuredNo ='"+InsuredNo+"'";
    map.put("delete from LCDuty where PolNo in ("+subStr0+")"
            ,"DELETE");             //保单险种责任表
    map.put("delete from LCPrem where PolNo in ("+subStr0+")"
            ,"DELETE");             //保费项表
    map.put("delete from LCGet where PolNo in ("+subStr0+")"
            ,"DELETE");            //领取项表
    /*
    map.put("delete from LCPrem_1 where PolNo in ("+subStr0+")"
            ,"DELETE");            //保费项附表1
    */
    return true;
  }
  /**
   * 将被保人中的信息更新到对应的险种中
   * @param tLCPolSchema LCPolSchema
   * @return boolean
   */
  private boolean upInsurToPol(LCInsuredSchema tLCInsuredSchema,LCPolSchema tLCPolSchema){
    tLCPolSchema.setInsuredBirthday(tLCInsuredSchema.getBirthday());
    tLCPolSchema.setInsuredSex(tLCInsuredSchema.getSex());
    tLCPolSchema.setOccupationType(tLCInsuredSchema.getOccupationType());
    //tLCPolSchema.setInsuredName(tLCInsuredSchema.getName());
    return true;
  }
  /**
   * 准备计算后的数据，从VData中提取Set，放在全局业务处理类中
   * @param calData VData
   * @return boolean
   */
  private boolean prepareCalData(VData calData){
    LCPolSchema tLCPolSchema=(LCPolSchema)calData.getObjectByObjectName("LCPolSchema",0);
    LCDutySet tLCDutySet=(LCDutySet)calData.getObjectByObjectName("LCDutySet",0);
    LCPremSet tLCPremSet=(LCPremSet)calData.getObjectByObjectName("LCPremSet",0);
    LCGetSet tLCGetSet=(LCGetSet)calData.getObjectByObjectName("LCGetSet",0);
    if(tLCPolSchema==null||tLCDutySet==null||tLCPremSet==null||tLCGetSet==null){
      mErrors.addOneError(new CError("计算保额保费时返回数据为空!!!"));
      return false;
    }
    mLCPolSet.add(tLCPolSchema.getSchema());
    mLCDutySet.add(tLCDutySet);
    mLCPremSet.add(tLCPremSet);
    mLCGetSet.add(tLCGetSet);
    return true;
  }
  /**
   * 准备数据。只有险种相关信息的数据，不含合同数据
   */
  private void prepareData(){
    mResult.clear();
    map.put(mLCPolSet,"UPDATE");
    map.put(mLCDutySet,"INSERT");
    map.put(mLCPremSet,"INSERT");
    map.put(mLCGetSet,"INSERT");
    mResult.add(map);
  }
  /**
   * 得到运算结果
   * @return VData
   */
  public VData getResult(){
    return mResult;
  }

  public static void main(String[] args){
    RewritePolInfo rr=new RewritePolInfo();
    VData var=new VData();
    LCContSchema cont = new LCContSchema();
    cont.setContNo("00000000000000000006");
    if(!rr.updatePolInfo(cont)){
      System.out.println("ERROR:"+rr.mErrors.getFirstError());
    }else{
      System.out.println(rr.getResult().size());
      MMap map = (MMap) rr.getResult().get(0);
      System.out.println(map.keySet().size());
      for (int i = 0; i < map.keySet().size(); i++) {
        Object key = map.getKeyByOrder(String.valueOf(i + 1));
        if(key instanceof LCPremSet){
          System.out.println(((LCPremSet)key).size());
        }
      }
    }
  }

}
