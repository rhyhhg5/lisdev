package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.CalBase;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.utility.SSRS;
import java.util.HashSet;
import java.util.Iterator;



/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class ContLogicVerifyBL {
    /** 传入参数 */
    private VData mInputData;

    /** 传入操作符 */
    private String mOperate;

    /** 登陆信息 */
    private GlobalInput mGlobalInput;

    /** 报错存储对象 */
    public CErrors mErrors = new CErrors();

    /** 最后保存结果 */
    private VData mResult = new VData();

    /** 最后递交Map */
    private MMap map = new MMap();

    /** 数据操作字符串 */

    private String mManageCom;

    private String mContNo;

    private String mCalCode;

    private String mError = "";

    private double mValue = 0.0;

    /**业务处理相关变量*/

    private TransferData mTransferData = new TransferData();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private CalBase mCalBase = new CalBase();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();

    private String theCurrentTime = PubFun.getCurrentTime();

    public ContLogicVerifyBL() {
    }

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        System.out.println("into ContLogicVerifyBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("ContLogicVerifyBL finished...");
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("into ContLogicVerifyBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        mLCContSchema = (LCContSchema) mInputData.
                        getObjectByObjectName("LCContSchema", 0);

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("into ContLogicVerifyBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }
        if (mTransferData == null) {
            String str = "前台参数传入不完整!";
            buildError("checkData", str);
            System.out.println("在程序ContLogicVerifyBL.checkData() - 168 : " +
                               str);
            return false;
        }
        mContNo = (String) mTransferData.getValueByName("ContNo");

        if (mLCContSchema == null || mLCContSchema.getContNo() == null) {
            String str = "传入的保单号信息为空！";
            buildError("getInputData", str);
            System.out.println("在程序ContLogicVerifyBL.getInputData() - 144 : " +
                               str);
            return false;
        }

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCContSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            String str = "查询保单信息失败!";
            buildError("checkData", str);
            System.out.println("在程序ContLogicVerifyBL.checkData() - 168 : " +
                               str);
            return false;
        }
        mLCContSchema.setSchema(tLCContDB);

        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("into ContLogicVerifyBL.dealData()...");
        /**查询险种信息*/
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCContSchema.getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet == null || tLCPolSet.size() <= 0) {
            String str = "没有查询到险种信息!";
            buildError("dealData", str);
            System.out.println("在程序ContLogicVerifyBL.dealData() - 202 : " + str);
            return false;
        }
        VData RiskCode = new VData();
        LCPolSet tempLCPolSet = new LCPolSet();
        LCContSchema tLCContSchema = new LCContSchema();
        tempLCPolSet.set(tLCPolSet);
        HashSet risk = new HashSet();
        for (int i = 1; i <= tLCPolSet.size(); i++) {
            risk.add(tLCPolSet.get(i).getRiskCode());
        }
        //RiskCode.add(risk);
        System.out.println("查看险种信息个数 : " + risk.size());
        //for (int i = 0; i < risk.size(); i++) {
        //  System.out.println("查看险种信代码 : " );
        //}
        for (int i = 1; i <= tLCPolSet.size(); i++) {
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema = tLCPolSet.get(i);
            String SubRisk = (String) tLCPolSchema.getRiskCode();
            System.out.println("查看险种信代码 : " + SubRisk);
            if (CalContLogicFactor(tLCContSchema, SubRisk)) {
            }
        }
        if (this.mError != null && !mError.equals("")) {
            String str = mError;
            buildError("CalContLogicFactor", str);
            System.out.println(
                    "在程序ContLogicVerifyBL.CalContLogicFactor() - 324 : " + str);
            return false;
        }

        return true;
    }

    private boolean CalContLogicFactor(LCContSchema tLCContSchema,
                                       String rRiskCode) {
        String strSql =
                "select * from lmcalmode where calcode like 'CM%' and riskcode='" +
                rRiskCode + "'";
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        LMCalModeSet tLMCalModeSet = tLMCalModeDB.executeQuery(strSql);
        if (tLMCalModeSet.size() <= 0) {
            System.out.println("程序第132行；没有查询到计算sql  ");
            return true;
        }
        for (int m = 1; m <= tLMCalModeSet.size(); m++) {
            String oneCalCode = tLMCalModeSet.get(m).getCalCode();
            String oneError = tLMCalModeSet.get(m).getRemark();
            if (oneCalCode == null) {
                System.out.println(
                        "程序第138行；oneCalCode == null;\n  ");
                return true;
            }
            Calculator mCalculator = new Calculator();
            mCalculator.setCalCode(oneCalCode);

            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setInsuredNo(mLCContSchema.getInsuredNo());
            LCInsuredSchema tLCInsuredSchema = tLCInsuredDB.getSchema();
            //增加基本要素
            String ContNo = mLCContSchema.getContNo();
            String InusredNo = mLCContSchema.getInsuredNo();
            String InusredName = mLCContSchema.getInsuredName();
            String InsuredBirthday = mLCContSchema.getInsuredBirthday();
            String InsuredIDNo = mLCContSchema.getInsuredIDNo();
            String CValiDate = mLCContSchema.getCValiDate();
            String BankCode = mLCContSchema.getBankCode();
            String PolNo = mLCPolSchema.getPolNo();
            String RelationToMainInsured = tLCInsuredSchema.
                                           getRelationToMainInsured();
            String RelationToAppnt = tLCInsuredSchema.getRelationToAppnt();
            String BankAccNo = mLCContSchema.getBankAccNo();

            mCalculator.addBasicFactor("ContNo", String.valueOf(ContNo));
            mCalculator.addBasicFactor("InusredNo", String.valueOf(InusredNo));
            mCalculator.addBasicFactor("InusredName",
                                       String.valueOf(InusredName));
            mCalculator.addBasicFactor("InsuredBirthday",
                                       String.valueOf(InsuredBirthday));
            mCalculator.addBasicFactor("InsuredIDNo",
                                       String.valueOf(InsuredIDNo));
            mCalculator.addBasicFactor("CValiDate", String.valueOf(CValiDate));
            mCalculator.addBasicFactor("BankCode", String.valueOf(BankCode));
            mCalculator.addBasicFactor("PolNo", String.valueOf(PolNo));
            mCalculator.addBasicFactor("RelationToMainInsured",
                                       String.valueOf(RelationToMainInsured));
            mCalculator.addBasicFactor("RelationToAppnt",
                                       String.valueOf(RelationToAppnt));
            mCalculator.addBasicFactor("BankAccNo", String.valueOf(BankAccNo));

            mCalculator.addBasicFactor("GrpContNo",
                                       mLCContSchema.getGrpContNo());
            mCalculator.addBasicFactor("ProposalContNo",
                                       mLCContSchema.getProposalContNo());
            mCalculator.addBasicFactor("PrtNo",
                                       mLCContSchema.getPrtNo());
            mCalculator.addBasicFactor("ManageCom",
                                       mLCContSchema.getManageCom());
            mCalculator.addBasicFactor("AppntNo",
                                       mLCContSchema.getAppntNo());
            mCalculator.addBasicFactor("AppntName",
                                       mLCContSchema.getAppntName());

            //增加管理机构
            String SQl =
                    "select distinct managecom from lccont where contno='" +
                    mLCContSchema.getContNo() +
                    "'  with ur";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(SQl);
            System.out.println("SQl" + SQl);
            if (tSSRS.MaxRow > 0) {
                System.out.println("tSSRS" + tSSRS.GetText(1, 1));
                mCalculator.addBasicFactor("ManageCom",
                                           String.valueOf(tSSRS.GetText(1,
                        1)));
            }
//       addInsuYear(mCalculator);
            String tStr = "";

            tStr = mCalculator.calculate();
            if (tStr == null || tStr.trim().equals("")) {
                mValue = 0;
            } else {
                mValue = Double.parseDouble(tStr);
                mError += oneError + "<br>";
            }
            System.out.println(mValue);
            System.out.println("校验出错误 ：" + mError);
        }
        return true;
    }


    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("into ContLogicVerifyBL.prepareOutputData()...");
        this.mResult.add(this.mError);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ContLogicVerifyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


}
