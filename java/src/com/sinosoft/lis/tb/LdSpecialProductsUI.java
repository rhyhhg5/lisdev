package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LdSpecialProductsUI {
	
	private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    
    public LdSpecialProductsUI()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        LdSpecialProductsBL tLdSpecialProductsBL = new LdSpecialProductsBL();
        tLdSpecialProductsBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tLdSpecialProductsBL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLdSpecialProductsBL.mErrors);
        }
        //取得结果返回前台
        this.mResult.clear();
        this.mResult = tLdSpecialProductsBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
