/**
 * 2011-8-10
 */
package com.sinosoft.lis.tb;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;

/**
 * @author LY
 *
 */
public final class XmlFileUtil
{
    private static String mEncoding = new String("UTF-8");

    private XmlFileUtil()
    {
    }

    public static Document xmlFile2Doc(String cFileName)
    {
        File tDocFile = new File(cFileName);

        // 判断文件是否存在
        if (!tDocFile.isFile())
        {
            System.out.println("[" + cFileName + "]文件未找到。");
            return null;
        }
        // --------------------

        // 获取文件流，转换成Document对象
        Document tXmlDoc = null;
        try
        {
            InputStream mIs = new FileInputStream(tDocFile);

            byte[] mInXmlBytes = XmlFileUtil.inputStreamToBytes(mIs);
            String mInXmlStr = new String(mInXmlBytes, XmlFileUtil.mEncoding);

            StringReader tInXmlReader = new StringReader(mInXmlStr);
            SAXBuilder tSAXBuilder = new SAXBuilder();
            tXmlDoc = tSAXBuilder.build(tInXmlReader);
        }
        catch (Exception e)
        {
            System.out.println("文件转换Documnet时，出现异常。");
            e.printStackTrace();
            return null;
        }
        // --------------------

        return tXmlDoc;
    }

    private static byte[] inputStreamToBytes(InputStream pIns)
    {
        ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

        try
        {
            byte[] tBytes = new byte[8 * 1024];
            for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));)
            {
                mByteArrayOutputStream.write(tBytes, 0, tReadSize);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }

        return mByteArrayOutputStream.toByteArray();
    }
}
