package com.sinosoft.lis.tb;

import java.util.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class PreviewRiskSignBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 记录传递参数 */
    private TransferData mTransferData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 传入对象 */
    private VData mInputData;
    /** 操作符 */
    private String mOperate;
    /** 险种信息 */
    private LCPolSet mLCPolSet;
    /** 操作日期对象 */
    private FDate mFDate = new FDate();
    /** 保单终止日期(所有险种的最大终止日期) */
    private Date maxEndDate;
    /** 缴费项 */
    private LCPremSet mLCPremSet = new LCPremSet();
    /** 责任 */
    private LCDutySet mLCDutySet = new LCDutySet();
    /** 给付 */
    private LCGetSet mLCGetSet = new LCGetSet();
    /** 保单号码 */
    private String newContNo;
    /** 操作信息 */
    private GlobalInput mGlobalInput;
    /** 递交操作 */
    private MMap map = new MMap();
    public PreviewRiskSignBL() {
    }

    public static void main(String[] args) {
        PreviewRiskSignBL previewrisksignbl = new PreviewRiskSignBL();
    }

    /**
     * submitData
     *
     * @param tInputData VData
     * @param tOperate String
     * @return boolean
     */
    public boolean submitData(VData tInputData, String tOperate) {
        this.mInputData = tInputData;
        this.mOperate = tOperate;

        System.out.println("@@完成程序submitData第41行");

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {

        this.map.put(this.mLCPolSet, "INSERT");
        this.map.put(this.mLCGetSet, "INSERT");
        this.map.put(this.mLCDutySet, "INSERT");
        this.map.put(this.mLCPremSet, "INSERT");
        this.mResult.add(map);
        this.mResult.add(this.mLCPolSet);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {

        System.out.println("开始进行险种操作");
        String tLimit = PubFun.getNoLimit(this.mLCPolSet.get(1).getManageCom());

        for (int i = 1; i <= this.mLCPolSet.size(); i++) {
            String polNo = PubFun1.CreateMaxNo("POLNO", tLimit);

            LCPolSchema tLCPolSchema = mLCPolSet.get(i).getSchema();
            map.put(tLCPolSchema.getSchema(), "DELETE");

            LCPremSet tLCPremSet = dealPrem(tLCPolSchema, polNo);
            if (tLCPremSet == null) {
                return false;
            }
            this.mLCPremSet.add(tLCPremSet);

            LCGetSet tLCGetSet = dealGet(tLCPolSchema, polNo);
            if (tLCGetSet == null) {
                return false;
            }
            this.mLCGetSet.add(tLCGetSet);

            LCDutySet tLCDutySet = dealDuty(tLCPolSchema, tLCPremSet, polNo);
            if (tLCDutySet == null) {
                return false;
            }
            this.mLCDutySet.add(tLCDutySet);
            /** 处理完成上面的信息 */
            System.out.println("完成险种信息");
            if (!dealPolInfo(tLCPolSchema, tLCDutySet, polNo, i)) {
                return false;
            }
            mLCPolSet.set(i, tLCPolSchema);
            /** 换号 */
            if (!changePolNo(tLCPolSchema)) {
                return false;
            }
        }
        return true;
    }

    /**
     * changePolNo
     *
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean changePolNo(LCPolSchema tLCPolSchema) {
        Vector sqlVector = new Vector();
        String wherepart = " PolNo='" + tLCPolSchema.getProposalNo() + "'";
        String[] tables1 = {
                           "LCSpecNote"};
        String condition = " polno='" + tLCPolSchema.getPolNo() + "'";
        sqlVector.addAll(PubFun.formUpdateSql(tables1, condition, wherepart));
        //只更新PolNo的表
        String[] polOnlyTables = {
                                 "LCSpec", "LCInsuredRelated", "LCPrem_1"};
        condition = condition + " , ModifyDate='" + PubFun.getCurrentDate() +
                    "', ModifyTime='" +
                    PubFun.getCurrentTime() + "' ";

        sqlVector.addAll(PubFun.formUpdateSql(polOnlyTables, condition,
                                              wherepart));
        //还需更新ContNo的表
        String[] tables = {
                          "LCUWMaster", "LCUWError", "LCUWSub", "LCBnf"};
        condition = " contno='" + newContNo + "'," + condition;

        sqlVector.addAll(PubFun.formUpdateSql(tables, condition, wherepart));
        if (sqlVector != null) {
            MMap tmpMap = new MMap();
            for (int i = 0; i < sqlVector.size(); i++) {
                map.put((String) sqlVector.get(i), "UPDATE");
            }
        }
        return true;
    }

    /**
     * dealPolInfo
     *
     * @param tLCPolSchema LCPolSchema
     * @param tLCDutySet LCDutySet
     * @param polNo String
     * @return boolean
     */
    private boolean dealPolInfo(LCPolSchema tLCPolSchema, LCDutySet tLCDutySet,
                                String polNo, int i) {
        if (tLCPolSchema.getMainPolNo().equals(tLCPolSchema.getPolNo())) {
            tLCPolSchema.setMainPolNo(polNo);
        }
        tLCPolSchema.setPolNo(polNo);
        tLCPolSchema.setContNo(this.newContNo);
        tLCPolSchema.setSignCom(mGlobalInput.ManageCom);
        tLCPolSchema.setSumPrem(tLCPolSchema.getPrem());
        //在合同统一处理
        if (tLCPolSchema.getRiskSeqNo() == null ||
            tLCPolSchema.getRiskSeqNo().equals("")) {
            tLCPolSchema.setRiskSeqNo(getStringFormate(i));
        }
        tLCPolSchema.setSignDate("");
        tLCPolSchema.setSignTime("");
        tLCPolSchema.setLastRevDate(tLCPolSchema.getCValiDate()); // 把最近复效日期置为起保日期
        tLCPolSchema.setAppFlag("9");
        tLCPolSchema.setPolState("00019999");

        // 交至日期
        Date maxPaytoDate = null;
        Date PayEndDate = null;
        for (int j = 1; j <= tLCDutySet.size(); j++) {
            LCDutySchema tLCDutySchema = tLCDutySet.get(j);

            if (maxPaytoDate == null ||
                mFDate.getDate(tLCDutySchema.getPaytoDate()).after(maxPaytoDate)) {
                System.out.println("maxPaytoDate : " +
                                   mFDate.getString(maxPaytoDate));
                maxPaytoDate = mFDate.getDate(tLCDutySchema.getPaytoDate());
            }
            if (PayEndDate == null ||
                PayEndDate.before(mFDate.getDate(tLCDutySchema.getPayEndDate()))) {
                PayEndDate = mFDate.getDate(tLCDutySchema.getPayEndDate());
            }
        }
        System.out.println("maxPaytoDate : " + mFDate.getString(maxPaytoDate));
        /** 交至日期 */
        maxPaytoDate =
                maxPaytoDate.before(mFDate.getDate(tLCPolSchema.getEndDate())) ?
                maxPaytoDate :
                mFDate.getDate(tLCPolSchema.getEndDate());
        System.out.println("tLCPolSchema EndDate : " + tLCPolSchema.getEndDate());
        System.out.println("maxPaytoDate : " + mFDate.getString(maxPaytoDate));
        //交至日期
        tLCPolSchema.setPaytoDate(mFDate.getString(maxPaytoDate));
        tLCPolSchema.setPayEndDate(mFDate.getString(PayEndDate));
        tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCPolSchema.setModifyTime(PubFun.getCurrentTime());

        return true;
    }

    /**
     * dealPrem
     *
     * @return boolean
     * @param tLCPolSchema LCPolSchema
     * @param newPolNo String
     */
    private LCPremSet dealPrem(LCPolSchema tLCPolSchema, String newPolNo) {
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
        LCPremSet tLCPremSet = tLCPremDB.query();
        if (tLCPremSet.size() <= 0) {
            buildError("dealPrem", "查询保费项为空！");
            return null;
        }
        LCPremSet delLCPremSet = new LCPremSet();
        delLCPremSet.set(tLCPremSet);
        map.put(delLCPremSet, "DELETE");
        for (int i = 1; i <= tLCPremSet.size(); i++) {
            Date payStartDate = mFDate.getDate(tLCPremSet.get(i).
                                               getPayStartDate());
            Date endDate = mFDate.getDate(tLCPolSchema.getEndDate());
            Date payToDate = getPayToDate(payStartDate,
                                          endDate,
                                          tLCPremSet.get(i).getPayIntv());
            if (payToDate == null) {
                buildError("dealPrem", "缴至日期为空！");
                return null;
            }
            System.out.println("险种终止日期 :　" + mFDate.getString(endDate));
            System.out.println("缴至日期 :　" + mFDate.getString(payToDate));
            tLCPremSet.get(i).setSumPrem(tLCPremSet.get(i).getPrem());
            tLCPremSet.get(i).setPolNo(newPolNo);
            tLCPremSet.get(i).setPaytoDate(payToDate);
            tLCPremSet.get(i).setPayTimes(1);
            tLCPremSet.get(i).setContNo(this.newContNo);
            tLCPremSet.get(i).setPaytoDate(payToDate);
            tLCPremSet.get(i).setModifyDate(PubFun.getCurrentDate());
            tLCPremSet.get(i).setModifyTime(PubFun.getCurrentTime());
            tLCPremSet.get(i).setOperator(mGlobalInput.Operator);
            tLCPremSet.get(i).setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCPremSet.get(i).setMakeDate(PubFun.getCurrentDate());
            tLCPremSet.get(i).setMakeTime(PubFun.getCurrentTime());
        }
        return tLCPremSet;
    }

    /**
     * getPayEndDate
     *
     * @param payStartDate Date
     * @param endDate Date
     * @param payIntv int
     * @return Date
     */
    private Date getPayToDate(Date payStartDate, Date endDate, int payIntv) {
        if (payIntv == 0) {
            return endDate;
        }
        if (payStartDate == null || endDate == null) {
            buildError("getPayEndDate", "处理PayEndDate失败！");
            return null;
        }
        Date paytoDate = PubFun.calDate(payStartDate, payIntv, "M", null);
        return paytoDate;
    }

    /**
     * dealGet
     *
     * @return boolean
     * @param tLCPolSchema LCPolSchema
     * @param newPolNo String
     */
    private LCGetSet dealGet(LCPolSchema tLCPolSchema, String newPolNo) {
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
        LCGetSet tLCGetSet = tLCGetDB.query();
        LCGetSet delLCGetSet = new LCGetSet();
        delLCGetSet.set(tLCGetSet);
        if (tLCGetSet.size() <= 0) {
            buildError("dealGet", "查询给付信息失败！");
            return null;
        }
        map.put(delLCGetSet, "DELETE");
        for (int i = 1; i <= tLCGetSet.size(); i++) {
            LCGetSchema tLCGetSchema = tLCGetSet.get(i);
            tLCGetSchema.setPolNo(newPolNo);
            tLCGetSchema.setContNo(this.newContNo);
            tLCGetSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCGetSchema.setMakeDate(tLCPolSchema.getMakeDate());
            tLCGetSchema.setMakeTime(tLCPolSchema.getMakeTime());
            tLCGetSchema.setOperator(mGlobalInput.Operator);
            tLCGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGetSchema.setModifyTime(PubFun.getCurrentTime());
            tLCGetSet.set(i, tLCGetSchema);
        }
        return tLCGetSet;
    }

    /**
     * dealDuty
     *
     * @return boolean
     * @param tLCPolSchema LCPolSchema
     * @param tLCPremSet LCPremSet
     * @param newPolNo String
     */
    private LCDutySet dealDuty(LCPolSchema tLCPolSchema, LCPremSet tLCPremSet,
                               String newPolNo) {
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
        LCDutySet tLCDutySet = tLCDutyDB.query();
        LCDutySet delLCDutySet = new LCDutySet();
        delLCDutySet.set(tLCDutySet);
        if (tLCDutySet.size() <= 0) {
            buildError("dealDuty", "查询责任信息为空！");
            return null;
        }
        map.put(delLCDutySet, "DELETE");
        int n = tLCDutySet.size();

        for (int i = 1; i <= n; i++) {
            LCDutySchema tLCDutySchema = tLCDutySet.get(i);

            tLCDutySchema.setPolNo(newPolNo);
            tLCDutySchema.setSumPrem(tLCDutySchema.getPrem());

            // 交至日期
            Date maxPaytoDate = mFDate.getDate("1900-01-01");
            int m = tLCPremSet.size();
            for (int j = 1; j <= m; j++) {
                LCPremSchema tLCPremSchema = tLCPremSet.get(j);
                if (tLCDutySchema.getDutyCode().equals(tLCPremSchema.
                        getDutyCode()) &&
                    mFDate.getDate(tLCPremSchema.getPaytoDate()).after(
                            maxPaytoDate)) {
                    maxPaytoDate = mFDate.getDate(tLCPremSchema.getPaytoDate());
                }
            }
            /** 处理失效日期和交至日期的问题 */
            maxPaytoDate =
                    maxPaytoDate.before(mFDate.getDate(tLCPolSchema.getEndDate())) ?
                    maxPaytoDate :
                    mFDate.getDate(tLCPolSchema.getEndDate());
            tLCDutySchema.setPaytoDate(maxPaytoDate);
            tLCDutySchema.setContNo(this.newContNo);
            tLCDutySchema.setMakeDate(tLCPolSchema.getMakeDate());
            tLCDutySchema.setMakeTime(tLCPolSchema.getMakeTime());
            tLCDutySchema.setOperator(mGlobalInput.Operator);
            tLCDutySchema.setModifyDate(PubFun.getCurrentDate());
            tLCDutySchema.setModifyTime(PubFun.getCurrentTime());

            tLCDutySet.set(i, tLCDutySchema);
        }
        if (tLCDutySet == null || tLCDutySet.size() <= 0) {
            return null;
        }

        return tLCDutySet;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        //检验代理人是否已经离职
//        ExeSQL tExeSQL = new ExeSQL();
//        String state = tExeSQL.getOneValue(
//                "SELECT agentstate FROM laagent where agentcode='" +
//                mLCPolSet.get(1).getAgentCode() + "'");
//
//        if ((state == null) || state.equals("")) {
//            CError.buildErr(this, "无法查到该代理人的状态!");
//            return false;
//        }
//        if (Integer.parseInt(state) >= 3) {
//            CError.buildErr(this, "该代理人已经离职，不能签单!");
//            return false;
//        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        if (this.mInputData == null) {
            buildError("getInputData", "传入参数为空！");
            return false;
        }
        if (this.mOperate == null) {
            buildError("getInputData", "操作符为空！");
            return false;
        }
        this.mLCPolSet = (LCPolSet) mInputData.getObjectByObjectName("LCPolSet",
                0);
        if (this.mLCPolSet == null || mLCPolSet.size() <= 0) {
            buildError("getInputData", "录入险种信息为空！");
            return false;
        }
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (this.mTransferData == null) {
            buildError("getInputData", "传入更新合同号信息为空！");
            return false;
        }
        this.newContNo = (String) mTransferData.getValueByName("ContNo");
        if (StrTool.cTrim(this.newContNo).equals("")) {
            buildError("getInputData", "生成合同号码为空！");
            return false;
        }
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (this.mGlobalInput == null) {
            buildError("getInputData", "传入操作信息为空！");
            return false;
        }
        return true;
    }

    /**
     * getResult
     *
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PreviewRiskSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    private String getStringFormate(int i) {
        String result = String.valueOf(i);
        return result.length() == 1 ? result : "0" + result;
    }
}
