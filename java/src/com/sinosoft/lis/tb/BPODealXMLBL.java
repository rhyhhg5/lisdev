package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import com.sinosoft.lis.pubfun.ftp.*;
import java.net.*;
import java.io.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;


/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 通过ftp方式从前置机获取保单数据xml文件，进行解析生成保单临时数据和外包错误信息，并根据xml进行磁盘投保。
    可分做三步个步骤进行处理：
 A、	通过ftp方式从前置机获取外包业务数据xml，存储到本地外包商返回xml备份文件夹，删除前置机的xml文件。
 B、	解析xml文件，生成外包保单数据，即刻存储到数据库。
 C、	调用修改后的磁盘导入程序进行保单信息导入，导入成功调用新单录入模块“录入完毕”功能将保单流转到新单复合工作流。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPODealXMLBL
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mDealInfo = "";

    private GlobalInput mGI = null;  //用户信息
    private HashMap mXmlPathMap = new HashMap();  //核心系统xml存储路径，可能一次处理多个xml文件（外包商ID<->核心系统xml路径）

    private MMap map = new MMap();

    public BPODealXMLBL()
    {
    }

    /**
     *  提供给外部的调用入口，循环每个外包商，调用以下方法进行外包返回保单数据xml的处理：
     * getSubmitMap
     * submitData
     * 注：若前置机有xml文件，且文件无误，则本方法执行完毕后，会在核心系统生成xml的备份文件，生成外包商反馈保单信息，生成合同并进入新单复核工作流节点。
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息，若是系统自动，则为用户名为001，机构为86。
     * @param operate String： 数据操作字符串，此为空，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if(getSubmitMap(cInputData, operate) == null)
        {
            return false;
        }

        if(!submit())
        {
            return false;
        }

        return true;
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CrmPolicyChangeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;

    }

    /**
     * getSubmitData
     * 提供给外部的调用入口，调用以下方法进行外包返回保单数据xml的处理：
     * getInputData
     * checkData
     * dealData
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return boolean：
     *   若成功则返回处理后的结果：MMap中应包Insert操作的含外包商保单数据BPOLCAppnt、
     * BPOLCInsured、BPOLCBnf、BPOLCImpart、BPOLCPol、BPOInssue
     * 及update操作的BPOBatchInfo、BPOMissionState、BPOMissionDetailState数据，
     *   操作失败为null
     */
    public MMap getSubmitMap(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * 1、校验是否录入了GlobalInpt信息，若没有录入，则构造如下信息：
     * Operator = “001”；
     * ComCode = ‘86“
     * @return boolean
     */
    private boolean checkData()
    {
        if(mGI == null)
        {
            mGI = new GlobalInput();
            mGI.Operator = "001";
            mGI.ComCode = "86";
            mGI.ManageCom = mGI.ComCode;
        }

        return true;
    }

    /**
     * dealData
     * 1、进行业务逻辑处理，生成处理后的结果集合，放入map中。本方法调用以下方法进行业务处理：
     * getXML
     * saveXMLInfo
     * parseXML
     * 注：本方法执行完毕后，本地xml备份目录中将生成本次获取的xml文件，前置机xml文件被删除
     * 同时会将xml中数据解析到BPOLCAppnt、BPOLCInsured、BPOLCBnf、BPOLCImpart、
     * BPOLCPol、BPOInssue。
     * 若保单无错误数据，在新单复核工作流节点能查询到本次导入的保单。
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        if(!getXML())
        {
            return false;
        }

        if(!parseXML())
        {
            return false;
        }

        if(!saveXMLInfo())
        {
            return false;
        }

        return true;
    }

    /**
     * getXML
     * 1、通过ftp组件FtpClient从前置机获取xml文件，存储到核心系统备份目录，删除前置机文件。
     * 修改批次信息状态为外包返回02。
     * 注：本方法执行完毕后，本地xml备份目录中将生成本次获取的xml文件，前置机xml文件被删除。
     * @return boolean，操作成功true，否则false。
     */
    private boolean getXML()
    {
        String tUIToot = CommonBL.getUIRoot();
        if(tUIToot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }

        int xmlCount = 0;
        String  tBPOId;
        BPOServerInfoDB tBPOServerInfoDB = new BPOServerInfoDB();
        BPOServerInfoSet set = new BPOServerInfoSet();
        LDCodeDB tLDCode=new LDCodeDB();
        LDCodeSet tLDCodeSet=tLDCode.executeQuery("select * from ldcode where codetype='bjwbbl' and othersign='1' ");
        for(int i=1;i<=tLDCodeSet.size();i++){
        	tBPOId=tLDCodeSet.get(i).getCode();
			tBPOServerInfoDB.setBPOID(tBPOId);
			set.add(tBPOServerInfoDB.query().get(1));
        }

        try
        {
            //所有外包商
            for(int i = 1; i <= set.size(); i++)
            {
                BPOServerInfoSchema schema = set.get(i);
                FTPTool tFTPTool = new FTPTool(schema.getServerIP(),
                                               schema.getLogInUser(),
                                               schema.getLogInPwd(),
                                   Integer.parseInt(schema.getServerPort()));
                if(!tFTPTool.loginFTP())
                {
                    CError tError = new CError();
                    tError.moduleName = "BPODealXMLBL";
                    tError.functionName = "getXML";
                    tError.errorMessage = tFTPTool.getErrContent(
                        FTPReplyCodeName.LANGUSGE_CHINESE);
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
                }

                //下载所有文件(最好是逐个文件校验是否是外包任务，若是则下载，这样可以避免非外包任务的文件占用大量网络资源。暂为实现)
                String tFilePathLocal = tUIToot + schema.getBackBackupBasePath();
                String[] tPath = tFTPTool.downloadOneDirectory(schema.getBackBasePath(),
                    tFilePathLocal);
                String errContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                if(tPath == null && null != errContent)
                {
                    mErrors.addOneError(errContent);
                    return false;
                }

                for(int j = 0; j < tPath.length; j++)
                {
                    if(tPath[j] == null)
                    {
                        continue;
                    }

                    //校验文件是否外包录入任务，若不是则不需要处理
                    String tBatchNo = (tPath[j].substring(
                                     0, tPath[j].lastIndexOf(".")));

                    String sql = "select * from BPOBatchInfo "
                                 + "where BatchNo = '" + tBatchNo + "' "
                                 + "   and (State = '"
                                 + BPO.BATCH_STATE_SENT + "' "
                                 + "       or State = '"
                                 + BPO.BATCH_STATE_GET + "') ";
                    System.out.println(sql);
                    BPOBatchInfoDB tBPOBatchInfoDB = new BPOBatchInfoDB();
                    BPOBatchInfoSet tBPOBatchInfoSet = tBPOBatchInfoDB.executeQuery(sql);

                    if(tBPOBatchInfoSet.size() == 0)
                    {
                        File f = new File(tFilePathLocal + tPath[j]);
                        f.delete();
                        continue;
                    }

                    xmlCount++;

                    try
                    {
                        //进行xml导入
                        LCParseGuideIn tLCParseGuideIn = new LCParseGuideIn();
                        if(!tLCParseGuideIn.parseBPOXml(tFilePathLocal + tPath[j], mGI))
                        {
                            mErrors.copyAllErrors(tLCParseGuideIn.mErrors);
                        }
                    }
                    catch(Exception ex)
                    {
                        System.out.println("批次导入失败: " + tPath[j]);
                        mErrors.addOneError("批次导入失败" + tPath[j]);
                        ex.printStackTrace();
                    }

                    //查询校验xml是否已导入，若导入失败，将批次状态修改为导出失败（已返回）
                    tBPOBatchInfoDB.setBatchNo(tBatchNo);
                    tBPOBatchInfoDB.setState(BPO.BATCH_STATE_IMPORT);
                    tBPOBatchInfoSet = tBPOBatchInfoDB.query();
                    if(tBPOBatchInfoSet.size() == 0)
                    {
                        mErrors.addOneError("批次导入失败: " + tPath[j]);

                        sql = "update BPOBatchInfo "
                              + "set State = '" + BPO.BATCH_STATE_GET + "', "
                              + "  ModifyDate = Current Date, ModifyTime = Current Time "
                              + "where BatchNo = '" + tBatchNo + "' "
                              + "   and State ='" + BPO.BATCH_STATE_SENT + "' ";
                        new ExeSQL().execUpdateSQL(sql); //此处可以立即提交
                    }
                    else
                    {
                        mXmlPathMap.put(tFilePathLocal + tPath[j],
                                        schema.getBPOID());
                        tFTPTool.deleteFile(tPath[j]);
                    }
                }
                tFTPTool.logoutFTP();
            }
        }
        catch(SocketException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "BPODealXMLBL";
            tError.functionName = "getXML";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "BPODealXMLBL";
            tError.functionName = "getXML";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if(xmlCount == 0)
        {
            mErrors.addOneError("没有需要导入的数据");
        }

        return true;
    }

    /**
     * saveXMLInfo
     * 1、解析xml文件，将xml文件中APPNTTABLE节点信息存储到BPOLCAppnt表、
     * 2、将INSUREDTABLE节点信息存储到BPOLCInsured表总，
     * 3、将LCPOLTABLE节点信息存储到BPOLCPol表中，
     * 4、将BNFTABLE节点信息存储到BPOLCBnf表中，
     * 5、IMPARTTABLE节点信息存储到BPOLCImpart表中。
     * @return boolean，操作成功true，否则false。
     */
    private boolean saveXMLInfo()
    {
        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);

        return true;
    }

    /**
     * parseXML
     * 1、循环每个xml文件，调用契约磁盘投保类LCParseGuideIn类的ParseXml方法进行磁盘投保处理。
     * 2、注意，需要改造LCParseGuideIn类使得可以在调用parseXml方法前先把xml文件存储目录赋值给全局变量m_strDataFiles。
     * 3、若处理过程中无异常发生，将批次信息修改为以导入03，
     * 并根据每个任务的执行成功与否修改BPOMissionState.State值。
     * 外包没有进行录入的本批次任务状态置为“不录入”
     * 4、本方法执行成功后，应该在新单复核工作池中查询到相应保单
     * @return boolean
     */
    private boolean parseXML()
    {
        boolean allSuccFlag = true;
//        int xmlCount = 0;
//
//        Iterator tIterator = mXmlPathMap.keySet().iterator();
//        while(tIterator.hasNext())
//        {
//            xmlCount++;
//
//
//        }
//
//        if(xmlCount == 0)
//        {
//            this.mDealInfo = "没有需要导入的文件";
//        }

        return allSuccFlag;
    }

    /**
     * 得到处理信息，在没有submitData返回true时通过本方法的到返回信息
     * @return String
     */
    public String getDealInfo()
    {
        return mDealInfo;
    }

    public static void main(String[] args)
    {
        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "group";
        tGI.ComCode = "86";

        VData data = new VData();
        data.add(tGI);

        BPODealXMLBL bl = new BPODealXMLBL();
        if(bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
