package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class TaxConfirmListUI {
	/** �������� */
	public CErrors mErrors = new CErrors();
	
	public TaxConfirmListUI(){}
	
	public boolean submitData(VData cInputData,String cOperate){
		
		try{
			TaxConfirmListBL tTaxConfirmListBL = new TaxConfirmListBL();
			if(!tTaxConfirmListBL.submitData(cInputData,cOperate)){
				this.mErrors.copyAllErrors(tTaxConfirmListBL.mErrors);
				return false;
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			this.mErrors.addOneError(e.getMessage());
			return false;
		}
	}
}
