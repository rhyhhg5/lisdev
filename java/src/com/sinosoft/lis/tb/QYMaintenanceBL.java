package com.sinosoft.lis.tb;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.informix.msg.sblob_en_US;
import com.informix.util.stringUtil;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LCCUWMasterDB;
import com.sinosoft.lis.db.LCCUWSubDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContGetPolDB;
import com.sinosoft.lis.db.LCContReceiveDB;
import com.sinosoft.lis.db.LCGCUWMasterDB;
import com.sinosoft.lis.db.LCGCUWSubDB;
import com.sinosoft.lis.db.LCGUWMasterDB;
import com.sinosoft.lis.db.LCGUWSubDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LCUWSubDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCCUWMasterSet;
import com.sinosoft.lis.vschema.LCCUWSubSet;
import com.sinosoft.lis.vschema.LCContGetPolSet;
import com.sinosoft.lis.vschema.LCContReceiveSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGCUWMasterSet;
import com.sinosoft.lis.vschema.LCGCUWSubSet;
import com.sinosoft.lis.vschema.LCGUWMasterSet;
import com.sinosoft.lis.vschema.LCGUWSubSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.lis.vschema.LCUWSubSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

import examples.newsgroups;

public class QYMaintenanceBL
{
	private static String host = "mail.sinosoft.com.cn";//发送方邮箱所在的smtp主机
	private static String sender = "*******@sinosoft.com.cn";//发送方邮箱  自己改
	private static String password = "*******";//密码 自己改
    public CErrors mErrors = new CErrors();
    private String mOperate; 
    private String mFinancialAction;
    private String mOnlyDoUpdate;
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCContSet mLCContSet = new LCContSet();
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCGCUWSubSet mLCGCUWSubSet = new LCGCUWSubSet();
    private LCGUWSubSet mLCGUWSubSet = new LCGUWSubSet();
    private LCGCUWMasterSet mLCGCUWMasterSet = new LCGCUWMasterSet();
    private LCGUWMasterSet mLCGUWMasterSet = new LCGUWMasterSet();
    private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();
    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
    private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();
    private LCContReceiveSet mLCContReceiveSet = new LCContReceiveSet();
    private LCContGetPolSet mLCContGetPolSet = new LCContGetPolSet();
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    private LJAPaySet mLJAPaySet = new LJAPaySet();
    private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    
    private String mContType ;
    private String mPrtNo;
    private String mContNo;
    private String mSaleChnl;
    private String mAgentCom;
    private String mAgentCode;
    private String mAgentGroup;
   
    
    public QYMaintenanceBL(){}
    
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;
        System.out.println("动作为：" + mOperate);
        
        if("skip".equals(mOperate)){
        	return true;
        }
        if(!getInputData(cInputData))
        {
            return false;
        }
        
        if(!checkData())
        {
            return false;
        }
        
        if(!dealData())
        {
            return false;
        }
        return true;
    }
    
    /**
     * 校验录入的数据是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        //校验保单类型是否正确
        if(!mContType.equals("1") && !mContType.equals("2"))
        {
            //@@错误处理
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "checkData";
            tError.errorMessage = "保单类型数据有误，ContType = '" 
                    + mContType + "'，不符合规范!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        //校验录入的销售渠道是否正确
        if(mOperate.equals("SaleChnl") || mOperate.equals("Both"))
        {
            if(mSaleChnl == null || mSaleChnl.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "checkData";
                tError.errorMessage = "输入的销售渠道有误，SaleChnl = '" 
                    + mSaleChnl + "'，请修改后在进行维护!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        
        //校验录入的中介机构、业务员代码和业务员团队号是否正确
        if(mOperate.equals("AgentCode") || mOperate.equals("Both"))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mAgentCode);
            tLAAgentDB.setAgentGroup(mAgentGroup);
            if(tLAAgentDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "checkData";
                tError.errorMessage = "输入的业务员代码和业务员团队号有误，AgentCode = '" 
                    + mAgentCode + "'，AgentGroup = '" 
                    + mAgentGroup +"'，请修改后在进行维护!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLAAgentSchema = tLAAgentDB.getSchema();
            System.out.println("业务员名字为：" + mLAAgentSchema.getName());
            
            if(mAgentCom != null && !mAgentCom.equals(""))
            {
                LAComDB tLAComDB = new LAComDB();
                tLAComDB.setAgentCom(mAgentCom);
                if(tLAComDB.getInfo() == false)
                {
                    CError tError = new CError();
                    tError.moduleName = "QYMaintenanceBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "输入的中介机构有误，AgentCom = '" 
                        + mAgentCom + "'，请修改后在进行维护!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                LAComToAgentDB tLAComToAgentDB = new LAComToAgentDB();
                tLAComToAgentDB.setAgentCom(mAgentCom);
                tLAComToAgentDB.setAgentCode(mAgentCode);
                if(tLAComToAgentDB.query() == null)
                {
                    CError tError = new CError();
                    tError.moduleName = "QYMaintenanceBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "输入的中介机构中不存在输入的业务员，AgentCode = '" 
                        + mAgentCode + "'，AgentCom = '" 
                        + mAgentCom +"'，请修改后在进行维护!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            
        }
        return true;
    }
    
    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        //维护销售渠道
        if(mOperate.equals("SaleChnl") && mFinancialAction.equals("None"))
        {
            if(!doSaleChnl())
            {
                return false;
            }
        }
        
        //维护业务员
        if(mOperate.equals("AgentCode") && mFinancialAction.equals("None"))
        {
            if(!doAgentCode())
            {
                return false;
            }
        }
        
        //维护销售渠道和业务员
        if(mOperate.equals("Both") && mFinancialAction.equals("None"))
        {
            if(!doSaleChnlAgentCode())
            {
                return false;
            }
        }
        
        //财务充负
        if(mFinancialAction.equals("doNegative"))
        {
            if(!negativeRecoil())
            {
                return false;
            }
        }
        
        //财务充正
        if(mFinancialAction.equals("doPositive"))
        {
            if(!positiveRecoil())
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if(mTransferData.getValueByName("tContType") == null)
        {
            //@@错误处理
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取保单类型失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        mContType = (String)mTransferData.getValueByName("tContType");
        System.out.println("保单类型为：" + mContType);
        
        if(mTransferData.getValueByName("tPrtNo") == null)
        {
            //@@错误处理
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取保单印刷号失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        mPrtNo = (String)mTransferData.getValueByName("tPrtNo");
        System.out.println("保单印刷号为：" + mPrtNo);
        
        if(mTransferData.getValueByName("tContNo") == null)
        {
            //@@错误处理
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取保单号失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        mContNo = (String)mTransferData.getValueByName("tContNo");
        System.out.println("保单号为：" + mContNo);
        
        mSaleChnl = (String)mTransferData.getValueByName("tSaleChnl");
        mAgentCom = (String)mTransferData.getValueByName("tAgentCom");
        mAgentCode = (String)mTransferData.getValueByName("tAgentCode");
        mAgentGroup = (String)mTransferData.getValueByName("tAgentGroup");
        System.out.println("需要修改的信息为：<br>销售渠道：" + mSaleChnl 
                        + "<br>中介机构代码：" + mAgentCom 
                        + "<br>业务员代码：" + mAgentCode 
                        + "<br>业务员团队代码：" + mAgentGroup);
        
        mFinancialAction = (String)mTransferData.getValueByName("tFinancialAction");
        System.out.println("财务动作为：" + mFinancialAction);
        mOnlyDoUpdate = (String)mTransferData.getValueByName("tOnlyDoUpdate");
        System.out.println("更新动作为：" + mOnlyDoUpdate);
        
        if(!getContMessage())
        {
            return false;
        }
        return true;
    }
    
    //获取保单号
    private boolean getContMessage()
    {
        if(mContType.equals("1"))
        {
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setPrtNo(mPrtNo);
            tLCContDB.setContNo(mContNo);
            LCContSet tLCContSet = new LCContSet();
            if(tLCContDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "没有查到个单信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            tLCContSet = tLCContDB.query();
            if(tLCContSet.size() > 1)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "保单号不唯一!";
                this.mErrors.addOneError(tError);
                return false;
            } 
            mLCContSchema = tLCContSet.get(1);
            String tContNo = mLCContSchema.getContNo();
            
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setContNo(tContNo);
            mLCPolSet = tLCPolDB.query();
            if(mLCPolSet == null)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取LCPol数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            //获取保单核保信息
            if(!getUWMessage())
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取核保信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
            tLCContReceiveDB.setContNo(tContNo);
            mLCContReceiveSet = tLCContReceiveDB.query();
            
            LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
            tLCContGetPolDB.setContNo(tContNo);
            mLCContGetPolSet = tLCContGetPolDB.query();
            
            LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
            tLJTempFeeDB.setOtherNo(tContNo);
            mLJTempFeeSet = tLJTempFeeDB.query();
            if(mLJTempFeeSet == null)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取财务暂收数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            //获取财务实收数据
            if(!getFinancialMessage())
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取财务实收数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
        }else if(mContType.equals("2"))
        {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            LCGrpContSet tLCGrpContSet = new LCGrpContSet();
            tLCGrpContDB.setPrtNo(mPrtNo);
            tLCGrpContDB.setGrpContNo(mContNo);
            if(tLCGrpContDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "没有查到团单信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLCGrpContSet = tLCGrpContDB.query();
            if(tLCGrpContSet != null && tLCGrpContSet.size() > 1)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "保单号不唯一!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLCGrpContSchema = tLCGrpContSet.get(1);
            String tGrpContNo = mLCGrpContSchema.getGrpContNo();
            System.out.println("tGrpContNo = " + tGrpContNo);
            
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setPrtNo(mPrtNo);
            tLCGrpPolDB.setGrpContNo(tGrpContNo);
            mLCGrpPolSet = tLCGrpPolDB.query();
            if(mLCGrpPolSet == null)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取团单LCGRPPol数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setGrpContNo(tGrpContNo);
            mLCContSet = tLCContDB.query();
            if(mLCContSet == null)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取团单分单LCCont数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setGrpContNo(tGrpContNo);
            mLCPolSet = tLCPolDB.query();
            if(mLCPolSet == null)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取团单分单LCPol数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            //获取保单核保信息
            if(!getUWMessage())
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取核保信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
            tLCContReceiveDB.setContNo(tGrpContNo);
            mLCContReceiveSet = tLCContReceiveDB.query();
            
            LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
            tLCContGetPolDB.setContNo(tGrpContNo);
            mLCContGetPolSet = tLCContGetPolDB.query();
            
            LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
            tLJTempFeeDB.setOtherNo(tGrpContNo);
            mLJTempFeeSet = tLJTempFeeDB.query();
            if(mLJTempFeeSet == null)
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取财务暂收数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            //获取财务实收数据
            if(!getFinancialMessage())
            {
                CError tError = new CError();
                tError.moduleName = "QYMaintenanceBL";
                tError.functionName = "getContMessage";
                tError.errorMessage = "获取财务实收数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }
    
    //获取保单核保数据
    private boolean getUWMessage()
    {
        if(mContType.equals("1"))
        {
            String tContNo = mLCContSchema.getContNo();
            
            LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
            tLCCUWSubDB.setContNo(tContNo);
            mLCCUWSubSet = tLCCUWSubDB.query();
            
            LCUWSubDB tLCUWSubDB = new LCUWSubDB();
            tLCUWSubDB.setContNo(tContNo);
            mLCUWSubSet = tLCUWSubDB.query();
            
            LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            tLCUWMasterDB.setContNo(tContNo);
            mLCUWMasterSet = tLCUWMasterDB.query();
            
            LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
            tLCCUWMasterDB.setContNo(tContNo);
            mLCCUWMasterSet = tLCCUWMasterDB.query();
            
        }else if(mContType.equals("2"))
        {
            String tGrpContNo = mLCGrpContSchema.getGrpContNo();
            
            LCGCUWSubDB tLCGCUWSubDB = new LCGCUWSubDB();
            tLCGCUWSubDB.setGrpContNo(tGrpContNo);
            mLCGCUWSubSet = tLCGCUWSubDB.query();
            
            LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB();
            tLCGUWSubDB.setGrpContNo(tGrpContNo);
            mLCGUWSubSet = tLCGUWSubDB.query();
            
            LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
            tLCGCUWMasterDB.setGrpContNo(tGrpContNo);
            mLCGCUWMasterSet = tLCGCUWMasterDB.query();
            
            LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB();
            tLCGUWMasterDB.setGrpContNo(tGrpContNo);
            mLCGUWMasterSet = tLCGUWMasterDB.query();
            
            LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
            tLCCUWSubDB.setGrpContNo(tGrpContNo);
            mLCCUWSubSet = tLCCUWSubDB.query();
            
            LCUWSubDB tLCUWSubDB = new LCUWSubDB();
            tLCUWSubDB.setGrpContNo(tGrpContNo);
            mLCUWSubSet = tLCUWSubDB.query();
            
            LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            tLCUWMasterDB.setGrpContNo(tGrpContNo);
            mLCUWMasterSet = tLCUWMasterDB.query();
            
            LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
            tLCCUWMasterDB.setGrpContNo(tGrpContNo);
            mLCCUWMasterSet = tLCCUWMasterDB.query();
        }
        return true;
    }
    
    //获取实收数据
    private boolean getFinancialMessage()
    {
        if(mContType.equals("1"))
        {
            String tContNo = mLCContSchema.getContNo();
            
            LJAPayDB tLJAPayDB = new LJAPayDB();
            tLJAPayDB.setIncomeNo(tContNo);
            tLJAPayDB.setDueFeeType("0");
            mLJAPaySet = tLJAPayDB.query();
            if(mLJAPaySet == null)
            {
                return false;
            }
            
            if(mLJAPaySet != null && mLJAPaySet.size() > 0)
            {
                LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
                System.out.println("PayNo = " + mLJAPaySet.get(1).getPayNo());
                
                tLJAPayPersonDB.setPayNo(mLJAPaySet.get(1).getPayNo());
                tLJAPayPersonDB.setContNo(tContNo);
                mLJAPayPersonSet = tLJAPayPersonDB.query();
                if(mLJAPayPersonSet == null)
                {
                    return false;
                }
            }
            
        }else if(mContType.equals("2"))
        {
            String tGrpContNo = mLCGrpContSchema.getGrpContNo();
            
            LJAPayDB tLJAPayDB = new LJAPayDB();
            tLJAPayDB.setIncomeNo(tGrpContNo);
            tLJAPayDB.setDueFeeType("0");
            mLJAPaySet = tLJAPayDB.query();
            if(mLJAPaySet == null)
            {
                return false;
            }
            
            if(mLJAPaySet != null && mLJAPaySet.size() > 0)
            {
                LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
                System.out.println("PayNo = " + mLJAPaySet.get(1).getPayNo());
                
                tLJAPayGrpDB.setPayNo(mLJAPaySet.get(1).getPayNo());
                tLJAPayGrpDB.setGrpContNo(tGrpContNo);
                mLJAPayGrpSet = tLJAPayGrpDB.query();
                if(mLJAPayGrpSet == null)
                {
                    return false;
                }
                
                LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
                tLJAPayPersonDB.setPayNo(mLJAPaySet.get(1).getPayNo());
                tLJAPayPersonDB.setGrpContNo(tGrpContNo);
                mLJAPayPersonSet = tLJAPayPersonDB.query();
                if(mLJAPayPersonSet == null)
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    //维护销售渠道
    private boolean updateSaleChnl()
    {
        System.out.println("-------------- 开始进行维护销售渠道 -----------------------");
        System.out.println("mSaleChnl = " + mSaleChnl);
        if(mContType.equals("1"))
        {
            mLCContSchema.setSaleChnl(mSaleChnl);
            mLCContSchema.setModifyDate(PubFun.getCurrentDate());
            mLCContSchema.setModifyTime(PubFun.getCurrentTime());
            
            //个单险种层维护销售渠道
            if(mLCPolSet != null && mLCPolSet.size() > 0)
            {
                for(int i = 1; i <= mLCPolSet.size(); i++)
                {
                    mLCPolSet.get(i).setSaleChnl(mSaleChnl);
                    mLCPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //个单暂收数据维护销售渠道
            if(mLJTempFeeSet != null && mLJTempFeeSet.size() > 0)
            {
                for(int i = 1; i <= mLJTempFeeSet.size(); i++)
                {
                    mLJTempFeeSet.get(i).setSaleChnl(mSaleChnl);
                    mLJTempFeeSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLJTempFeeSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            //不充负充正，增加维护实收表 by bxf
            if(mOnlyDoUpdate.equals("do")){
                if(mLJAPaySet!=null&&mLJAPaySet.size()>0){
                	mLJAPaySet.get(1).setSaleChnl(mSaleChnl);
                	mLJAPaySet.get(1).setModifyDate(PubFun.getCurrentDate());
                	mLJAPaySet.get(1).setModifyTime(PubFun.getCurrentTime());
                     
                }
                }
         
            
        }else if(mContType.equals("2"))
        {
            mLCGrpContSchema.setSaleChnl(mSaleChnl);
            mLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
            mLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
            
            //团单险种层维护销售渠道
            if(mLCGrpPolSet != null && mLCGrpPolSet.size() > 0)
            {
                for(int i = 1; i <= mLCGrpPolSet.size(); i++)
                {
                    mLCGrpPolSet.get(i).setSaleChnl(mSaleChnl);
                    mLCGrpPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCGrpPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //团单分单维护销售渠道
            if(mLCContSet != null && mLCContSet.size() > 0)
            {
                for(int i = 1; i <= mLCContSet.size(); i++)
                {
                    mLCContSet.get(i).setSaleChnl(mSaleChnl);
                    mLCContSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCContSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //团单分单险种层维护销售渠道
            if(mLCPolSet != null && mLCPolSet.size() > 0)
            {
                for(int i = 1; i <= mLCPolSet.size(); i++)
                {
                    mLCPolSet.get(i).setSaleChnl(mSaleChnl);
                    mLCPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //暂收数据维护销售渠道
            if(mLJTempFeeSet != null && mLJTempFeeSet.size() > 0)
            {
                for(int i = 1; i <= mLJTempFeeSet.size(); i++)
                {
                    mLJTempFeeSet.get(i).setSaleChnl(mSaleChnl);
                    mLJTempFeeSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLJTempFeeSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            //不充负充正，增加维护实收表 by bxf
            if(mOnlyDoUpdate.equals("do")){
                if(mLJAPaySet!=null&&mLJAPaySet.size()>0){
                	mLJAPaySet.get(1).setSaleChnl(mSaleChnl);
                	mLJAPaySet.get(1).setModifyDate(PubFun.getCurrentDate());
                	mLJAPaySet.get(1).setModifyTime(PubFun.getCurrentTime());
                     
                }
                }
        }
        return true;
    }
    
    
    //维护业务员
    private boolean updateAgentCode()
    {
        System.out.println("-------------- 开始进行维护业务员 -----------------------");
        System.out.println("mAgentCom = " + mAgentCom);
        System.out.println("mAgentCode = " + mAgentCode);
        System.out.println("mAgentGroup = " + mAgentGroup);
        if(mContType.equals("1"))
        {
            mLCContSchema.setAgentCom(mAgentCom);
            mLCContSchema.setAgentCode(mAgentCode);
            mLCContSchema.setAgentGroup(mAgentGroup);
            mLCContSchema.setModifyDate(PubFun.getCurrentDate());
            mLCContSchema.setModifyTime(PubFun.getCurrentTime());
            
            //个单险种层维护业务员
            if(mLCPolSet != null && mLCPolSet.size() > 0)
            {
                for(int i = 1; i <= mLCPolSet.size(); i++)
                {
                    mLCPolSet.get(i).setAgentCom(mAgentCom);
                    mLCPolSet.get(i).setAgentCode(mAgentCode);
                    mLCPolSet.get(i).setAgentGroup(mAgentGroup);
                    mLCPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护个单保单层核保意见
            if(mLCCUWSubSet != null && mLCCUWSubSet.size() > 0)
            {
                for(int i = 1; i <= mLCCUWSubSet.size(); i++)
                {
                    mLCCUWSubSet.get(i).setAgentCode(mAgentCode);
                    mLCCUWSubSet.get(i).setAgentGroup(mAgentGroup);
                    mLCCUWSubSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCCUWSubSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护个单险种层核保意见
            if(mLCUWSubSet != null && mLCUWSubSet.size() > 0)
            {
                for(int i = 1; i <= mLCUWSubSet.size(); i++)
                {
                    mLCUWSubSet.get(i).setAgentCode(mAgentCode);
                    mLCUWSubSet.get(i).setAgentGroup(mAgentGroup);
                    mLCUWSubSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCUWSubSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护个单保单层核保结论
            if(mLCCUWMasterSet != null && mLCCUWMasterSet.size() > 0)
            {
                for(int i = 1; i <= mLCCUWMasterSet.size(); i++)
                {
                    mLCCUWMasterSet.get(i).setAgentCode(mAgentCode);
                    mLCCUWMasterSet.get(i).setAgentGroup(mAgentGroup);
                    mLCCUWMasterSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCCUWMasterSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护个单险种层核保结论
            if(mLCUWMasterSet != null && mLCUWMasterSet.size() > 0)
            {
                for(int i = 1; i <= mLCUWMasterSet.size(); i++)
                {
                    mLCUWMasterSet.get(i).setAgentCode(mAgentCode);
                    mLCUWMasterSet.get(i).setAgentGroup(mAgentGroup);
                    mLCUWMasterSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCUWMasterSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护个单接收表信息
            if(mLCContReceiveSet != null && mLCContReceiveSet.size() > 0)
            {
                for(int i = 1; i <= mLCContReceiveSet.size(); i++)
                {
                    mLCContReceiveSet.get(i).setAgentCode(mAgentCode);
                    mLCContReceiveSet.get(i).setAgentName(mLAAgentSchema.getName());
                    mLCContReceiveSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCContReceiveSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护个单回执回销表信息
            if(mLCContGetPolSet != null && mLCContGetPolSet.size() > 0)
            {
                for(int i = 1; i <= mLCContGetPolSet.size(); i++)
                {
                    mLCContGetPolSet.get(i).setAgentCode(mAgentCode);
                    mLCContGetPolSet.get(i).setAgentName(mLAAgentSchema.getName());
                    mLCContGetPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCContGetPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //暂收数据维护业务员
            if(mLJTempFeeSet != null && mLJTempFeeSet.size() > 0)
            {
                for(int i = 1; i <= mLJTempFeeSet.size(); i++)
                {
                    mLJTempFeeSet.get(i).setAgentCom(mAgentCom);
                    mLJTempFeeSet.get(i).setAgentCode(mAgentCode);
                    mLJTempFeeSet.get(i).setAgentGroup(mAgentGroup);
                    mLJTempFeeSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLJTempFeeSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            //不充负充正，增加维护实收表 by bxf
            if(mOnlyDoUpdate.equals("do")){
            if(mLJAPaySet!=null&&mLJAPaySet.size()>0){
            	mLJAPaySet.get(1).setAgentCom(mAgentCom);
            	mLJAPaySet.get(1).setAgentCode(mAgentCode);
            	mLJAPaySet.get(1).setAgentGroup(mAgentGroup);
            	mLJAPaySet.get(1).setModifyDate(PubFun.getCurrentDate());
            	mLJAPaySet.get(1).setModifyTime(PubFun.getCurrentTime());
                 
            }
            if(mLJAPayPersonSet != null && mLJAPayPersonSet.size() > 0)
            {
                for(int i = 1; i <= mLJAPayPersonSet.size(); i++)
                {
                	mLJAPayPersonSet.get(i).setAgentCom(mAgentCom);
                	mLJAPayPersonSet.get(i).setAgentCode(mAgentCode);
                	mLJAPayPersonSet.get(i).setAgentGroup(mAgentGroup);
                	mLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
                	mLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            }
            
        }else if(mContType.equals("2"))
        {
            mLCGrpContSchema.setAgentCom(mAgentCom);
            mLCGrpContSchema.setAgentCode(mAgentCode);
            mLCGrpContSchema.setAgentGroup(mAgentGroup);
            mLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
            mLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
            
            //团单险种层维护业务员
            if(mLCGrpPolSet != null && mLCGrpPolSet.size() > 0)
            {
                for(int i = 1; i <= mLCGrpPolSet.size(); i++)
                {
                    mLCGrpPolSet.get(i).setAgentCom(mAgentCom);
                    mLCGrpPolSet.get(i).setAgentCode(mAgentCode);
                    mLCGrpPolSet.get(i).setAgentGroup(mAgentGroup);
                    mLCGrpPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCGrpPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //团单分单维护业务员
            if(mLCContSet != null && mLCContSet.size() > 0)
            {
                for(int i = 1; i <= mLCContSet.size(); i++)
                {
                    mLCContSet.get(i).setAgentCom(mAgentCom);
                    mLCContSet.get(i).setAgentCode(mAgentCode);
                    mLCContSet.get(i).setAgentGroup(mAgentGroup);
                    mLCContSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCContSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //团单分单险种层维护业务员
            if(mLCPolSet != null && mLCPolSet.size() > 0)
            {
                for(int i = 1; i <= mLCPolSet.size(); i++)
                {
                    mLCPolSet.get(i).setAgentCom(mAgentCom);
                    mLCPolSet.get(i).setAgentCode(mAgentCode);
                    mLCPolSet.get(i).setAgentGroup(mAgentGroup);
                    mLCPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单整单保单层核保意见
            if(mLCGCUWSubSet != null && mLCGCUWSubSet.size() > 0)
            {
                for(int i = 1; i <= mLCGCUWSubSet.size(); i++)
                {
                    mLCGCUWSubSet.get(i).setAgentCode(mAgentCode);
                    mLCGCUWSubSet.get(i).setAgentGroup(mAgentGroup);
                    mLCGCUWSubSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCGCUWSubSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单整单险种层核保意见
            if(mLCGUWSubSet != null && mLCGUWSubSet.size() > 0)
            {
                for(int i = 1; i <= mLCGUWSubSet.size(); i++)
                {
                    mLCGUWSubSet.get(i).setAgentCode(mAgentCode);
                    mLCGUWSubSet.get(i).setAgentGroup(mAgentGroup);
                    mLCGUWSubSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCGUWSubSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单整单保单层核保结论
            if(mLCGCUWMasterSet != null && mLCGCUWMasterSet.size() > 0)
            {
                for(int i = 1; i <= mLCGCUWMasterSet.size(); i++)
                {
                    mLCGCUWMasterSet.get(i).setAgentCode(mAgentCode);
                    mLCGCUWMasterSet.get(i).setAgentGroup(mAgentGroup);
                    mLCGCUWMasterSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCGCUWMasterSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单整单险种层核保结论
            if(mLCGUWMasterSet != null && mLCGUWMasterSet.size() > 0)
            {
                for(int i = 1; i <= mLCGUWMasterSet.size(); i++)
                {
                    mLCGUWMasterSet.get(i).setAgentCode(mAgentCode);
                    mLCGUWMasterSet.get(i).setAgentGroup(mAgentGroup);
                    mLCGUWMasterSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCGUWMasterSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单分单保单层核保意见
            if(mLCCUWSubSet != null && mLCCUWSubSet.size() > 0)
            {
                for(int i = 1; i <= mLCCUWSubSet.size(); i++)
                {
                    mLCCUWSubSet.get(i).setAgentCode(mAgentCode);
                    mLCCUWSubSet.get(i).setAgentGroup(mAgentGroup);
                    mLCCUWSubSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCCUWSubSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单分单险种层核保意见
            if(mLCUWSubSet != null && mLCUWSubSet.size() > 0)
            {
                for(int i = 1; i <= mLCUWSubSet.size(); i++)
                {
                    mLCUWSubSet.get(i).setAgentCode(mAgentCode);
                    mLCUWSubSet.get(i).setAgentGroup(mAgentGroup);
                    mLCUWSubSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCUWSubSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单分单保单层核保结论
            if(mLCCUWMasterSet != null && mLCCUWMasterSet.size() > 0)
            {
                for(int i = 1; i <= mLCCUWMasterSet.size(); i++)
                {
                    mLCCUWMasterSet.get(i).setAgentCode(mAgentCode);
                    mLCCUWMasterSet.get(i).setAgentGroup(mAgentGroup);
                    mLCCUWMasterSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCCUWMasterSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单分单险种层核保结论
            if(mLCUWMasterSet != null && mLCUWMasterSet.size() > 0)
            {
                for(int i = 1; i <= mLCUWMasterSet.size(); i++)
                {
                    mLCUWMasterSet.get(i).setAgentCode(mAgentCode);
                    mLCUWMasterSet.get(i).setAgentGroup(mAgentGroup);
                    mLCUWMasterSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCUWMasterSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单接收表信息
            if(mLCContReceiveSet != null && mLCContReceiveSet.size() > 0)
            {
                for(int i = 1; i <= mLCContReceiveSet.size(); i++)
                {
                    mLCContReceiveSet.get(i).setAgentCode(mAgentCode);
                    mLCContReceiveSet.get(i).setAgentName(mLAAgentSchema.getName());
                    mLCContReceiveSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCContReceiveSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //维护团单回执回销表信息
            if(mLCContGetPolSet != null && mLCContGetPolSet.size() > 0)
            {
                for(int i = 1; i <= mLCContGetPolSet.size(); i++)
                {
                    mLCContGetPolSet.get(i).setAgentCode(mAgentCode);
                    mLCContGetPolSet.get(i).setAgentName(mLAAgentSchema.getName());
                    mLCContGetPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCContGetPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            //暂收数据维护业务员
            if(mLJTempFeeSet != null && mLJTempFeeSet.size() > 0)
            {
                for(int i = 1; i <= mLJTempFeeSet.size(); i++)
                {
                    mLJTempFeeSet.get(i).setAgentCom(mAgentCom);
                    mLJTempFeeSet.get(i).setAgentCode(mAgentCode);
                    mLJTempFeeSet.get(i).setAgentGroup(mAgentGroup);
                    mLJTempFeeSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLJTempFeeSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            //不充负充正，增加维护实收表 by bxf
            if(mOnlyDoUpdate.equals("do")){
            if(mLJAPaySet!=null&&mLJAPaySet.size()>0){
            	mLJAPaySet.get(1).setAgentCom(mAgentCom);
            	mLJAPaySet.get(1).setAgentCode(mAgentCode);
            	mLJAPaySet.get(1).setAgentGroup(mAgentGroup);
            	mLJAPaySet.get(1).setModifyDate(PubFun.getCurrentDate());
            	mLJAPaySet.get(1).setModifyTime(PubFun.getCurrentTime());
                 
            }
            if(mLJAPayPersonSet != null && mLJAPayPersonSet.size() > 0)
            {
                for(int i = 1; i <= mLJAPayPersonSet.size(); i++)
                {
                	mLJAPayPersonSet.get(i).setAgentCom(mAgentCom);
                	mLJAPayPersonSet.get(i).setAgentCode(mAgentCode);
                	mLJAPayPersonSet.get(i).setAgentGroup(mAgentGroup);
                	mLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
                	mLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            if(mLJAPayGrpSet != null && mLJAPayGrpSet.size() > 0)
            {
                for (int i = 1; i <= mLJAPayGrpSet.size(); i++)
                {
                	mLJAPayGrpSet.get(i).setAgentCom(mAgentCom);
                	mLJAPayGrpSet.get(i).setAgentCode(mAgentCode);
                	mLJAPayGrpSet.get(i).setAgentGroup(mAgentGroup);
                	mLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
                	mLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
            }
            
            }
        }
        return true;
    }
    
    //维护销售渠道传值
    private boolean doSaleChnl()
    {
        if(!updateSaleChnl())
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "doSaleChnl";
            tError.errorMessage = "销售渠道信息传递失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        MMap tMMap = new MMap();
        if(mContType.equals("1"))
        {
            tMMap.put(mLCContSchema, "UPDATE");
            tMMap.put(mLCPolSet, "UPDATE");
            tMMap.put(mLJTempFeeSet, "UPDATE");
            if(mOnlyDoUpdate.equals("do")){
                tMMap.put(mLJAPaySet, "UPDATE");
                }
        }else if(mContType.equals("2"))
        {
            tMMap.put(mLCGrpContSchema, "UPDATE");
            tMMap.put(mLCGrpPolSet, "UPDATE");
            tMMap.put(mLCContSet, "UPDATE");
            tMMap.put(mLCPolSet, "UPDATE");
            tMMap.put(mLJTempFeeSet, "UPDATE");
            if(mOnlyDoUpdate.equals("do")){
            tMMap.put(mLJAPaySet, "UPDATE");
            }
        }
        
        VData tVData = new VData();
        tVData.add(tMMap);
        
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, "UPDATE"))
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "PubSubmit";
            tError.errorMessage = "维护销售渠道信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
   
    
    //维护业务员
    private boolean doAgentCode()
    {
        if(!updateAgentCode())
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "dealData";
            tError.errorMessage = "业务员信息传递失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        MMap tMMap = new MMap();
        if(mContType.equals("1"))
        {
            tMMap.put(mLCContSchema, "UPDATE");
            tMMap.put(mLCPolSet, "UPDATE");
            tMMap.put(mLCCUWSubSet, "UPDATE");
            tMMap.put(mLCUWSubSet, "UPDATE");
            tMMap.put(mLCCUWMasterSet, "UPDATE");
            tMMap.put(mLCUWMasterSet, "UPDATE");
            tMMap.put(mLCContReceiveSet, "UPDATE");
            tMMap.put(mLCContGetPolSet, "UPDATE");
            tMMap.put(mLJTempFeeSet, "UPDATE");
            if(mOnlyDoUpdate.equals("do")){
            	tMMap.put(mLJAPaySet, "UPDATE");
            	tMMap.put(mLJAPayPersonSet, "UPDATE");
            }
            
        }else if(mContType.equals("2"))
        {
            tMMap.put(mLCGrpContSchema, "UPDATE");
            tMMap.put(mLCGrpPolSet, "UPDATE");
            tMMap.put(mLCContSet, "UPDATE");
            tMMap.put(mLCPolSet, "UPDATE");
            tMMap.put(mLCGCUWSubSet, "UPDATE");
            tMMap.put(mLCGUWSubSet, "UPDATE");
            tMMap.put(mLCGCUWMasterSet, "UPDATE");
            tMMap.put(mLCGUWMasterSet, "UPDATE");
            tMMap.put(mLCCUWSubSet, "UPDATE");
            tMMap.put(mLCUWSubSet, "UPDATE");
            tMMap.put(mLCCUWMasterSet, "UPDATE");
            tMMap.put(mLCUWMasterSet, "UPDATE");
            tMMap.put(mLCContReceiveSet, "UPDATE");
            tMMap.put(mLCContGetPolSet, "UPDATE");
            tMMap.put(mLJTempFeeSet, "UPDATE");
            if(mOnlyDoUpdate.equals("do")){
            	tMMap.put(mLJAPaySet, "UPDATE");
            	tMMap.put(mLJAPayGrpSet, "UPDATE");
            	tMMap.put(mLJAPayPersonSet, "UPDATE");
            }
        }
        
        VData tVData = new VData();
        tVData.add(tMMap);
        
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, "UPDATE"))
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "PubSubmit";
            tError.errorMessage = "维护业务员信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    //维护销售渠道和业务员
    private boolean doSaleChnlAgentCode()
    {
        //维护销售渠道传值
        if(!updateSaleChnl())
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "dealData";
            tError.errorMessage = "销售渠道信息传递失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        //维护业务员
        if(!updateAgentCode())
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "dealData";
            tError.errorMessage = "业务员信息传递失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        MMap tMMap = new MMap();
        if(mContType.equals("1"))
        {
            tMMap.put(mLCContSchema, "UPDATE");
            tMMap.put(mLCPolSet, "UPDATE");
            tMMap.put(mLCCUWSubSet, "UPDATE");
            tMMap.put(mLCUWSubSet, "UPDATE");
            tMMap.put(mLCCUWMasterSet, "UPDATE");
            tMMap.put(mLCUWMasterSet, "UPDATE");
            tMMap.put(mLCContReceiveSet, "UPDATE");
            tMMap.put(mLCContGetPolSet, "UPDATE");
            tMMap.put(mLJTempFeeSet, "UPDATE");
            if(mOnlyDoUpdate.equals("do")){
            	tMMap.put(mLJAPaySet, "UPDATE");
            	
            	tMMap.put(mLJAPayPersonSet, "UPDATE");
            }
            
        }else if(mContType.equals("2"))
        {
            tMMap.put(mLCGrpContSchema, "UPDATE");
            tMMap.put(mLCGrpPolSet, "UPDATE");
            tMMap.put(mLCContSet, "UPDATE");
            tMMap.put(mLCPolSet, "UPDATE");
            tMMap.put(mLCGCUWSubSet, "UPDATE");
            tMMap.put(mLCGUWSubSet, "UPDATE");
            tMMap.put(mLCGCUWMasterSet, "UPDATE");
            tMMap.put(mLCGUWMasterSet, "UPDATE");
            tMMap.put(mLCCUWSubSet, "UPDATE");
            tMMap.put(mLCUWSubSet, "UPDATE");
            tMMap.put(mLCCUWMasterSet, "UPDATE");
            tMMap.put(mLCUWMasterSet, "UPDATE");
            tMMap.put(mLCContReceiveSet, "UPDATE");
            tMMap.put(mLCContGetPolSet, "UPDATE");
            tMMap.put(mLJTempFeeSet, "UPDATE");
            if(mOnlyDoUpdate.equals("do")){
            	tMMap.put(mLJAPaySet, "UPDATE");
            	tMMap.put(mLJAPayGrpSet, "UPDATE");
            	tMMap.put(mLJAPayPersonSet, "UPDATE");
            }
        }
        
        VData tVData = new VData();
        tVData.add(tMMap);
        
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, "UPDATE"))
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "PubSubmit";
            tError.errorMessage = "维护销售渠道和业务员失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    //财务充负
    String mdate="2016-05-01";
    double fmoneytax;
    String mMoneytax;
    private boolean negativeRecoil()
    {
        LJAPaySet tLJAPaySet = mLJAPaySet;
        LJAPayGrpSet tLJAPayGrpSet = null;
        LJAPayPersonSet tLJAPayPersonSet = null;
        String tNewPayNo=PubFun1.CreateMaxNo("PAYNO",null);
        System.out.println("充负的PayNo = " + tNewPayNo);
        
        MMap tMMap = new MMap();
        VData tVData = new VData();
        if(mContType.equals("1"))
        {
            tLJAPayPersonSet = mLJAPayPersonSet;
        }else if(mContType.equals("2"))
        {
            tLJAPayGrpSet = mLJAPayGrpSet;
            tLJAPayPersonSet = mLJAPayPersonSet;
        }
            
        if(tLJAPaySet != null && tLJAPaySet.size() > 0)
        {
            tLJAPaySet.get(1).setPayNo(tNewPayNo);
            tLJAPaySet.get(1).setSumActuPayMoney(-1 * mLJAPaySet.get(1).getSumActuPayMoney());
            tLJAPaySet.get(1).setDueFeeType("2");
            tLJAPaySet.get(1).setConfDate(PubFun.getCurrentDate());
            tLJAPaySet.get(1).setMakeDate(PubFun.getCurrentDate());
            tLJAPaySet.get(1).setMakeTime(PubFun.getCurrentTime());
            tLJAPaySet.get(1).setModifyDate(PubFun.getCurrentDate());
            tLJAPaySet.get(1).setModifyTime(PubFun.getCurrentTime());
            tMMap.put(tLJAPaySet, "INSERT");
        }
            
        if(tLJAPayGrpSet != null && tLJAPayGrpSet.size() > 0)
        {
        	if(mLJAPayGrpSet.get(1).getConfDate().compareTo(mdate)<0){
        		for(int i = 1; i <= tLJAPayGrpSet.size(); i++)
        		{
            
	                tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
	                tLJAPayGrpSet.get(i).setSumActuPayMoney(-1 * mLJAPayGrpSet.get(i).getSumActuPayMoney());
	                tLJAPayGrpSet.get(i).setSumDuePayMoney(-1 * mLJAPayGrpSet.get(i).getSumDuePayMoney());
	                tLJAPayGrpSet.get(i).setConfDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
	                tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
	                tLJAPayGrpSet.get(i).setMoneyNoTax((mLJAPayGrpSet.get(i).getSumActuPayMoney())+"");
	                tLJAPayGrpSet.get(i).setMoneyTax("0");
	                tLJAPayGrpSet.get(i).setBusiType("01");
	                tLJAPayGrpSet.get(i).setTaxRate("0.00");
        		}
        		
        	}else if(mLJAPayGrpSet.get(1).getMoneyNoTax()!=null){
        		for(int i = 1; i <= tLJAPayGrpSet.size(); i++)
        		{
        			fmoneytax=Double.parseDouble(mLJAPayGrpSet.get(i).getMoneyTax());
        			fmoneytax = 0.0-fmoneytax;
        			mMoneytax = Double.toString(fmoneytax);
	                tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
	                tLJAPayGrpSet.get(i).setSumActuPayMoney(-1 * mLJAPayGrpSet.get(i).getSumActuPayMoney());
	                tLJAPayGrpSet.get(i).setSumDuePayMoney(-1 * mLJAPayGrpSet.get(i).getSumDuePayMoney());
	                tLJAPayGrpSet.get(i).setConfDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
	                tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
	                tLJAPayGrpSet.get(i).setMoneyNoTax("-"+mLJAPayGrpSet.get(i).getMoneyNoTax());
	                tLJAPayGrpSet.get(i).setMoneyTax(mMoneytax);
        		}
        	}else{
        		for(int i = 1; i <= tLJAPayGrpSet.size(); i++)
	            {
	                tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
	                tLJAPayGrpSet.get(i).setSumActuPayMoney(-1 * mLJAPayGrpSet.get(i).getSumActuPayMoney());
	                tLJAPayGrpSet.get(i).setSumDuePayMoney(-1 * mLJAPayGrpSet.get(i).getSumDuePayMoney());
	                tLJAPayGrpSet.get(i).setConfDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
	                tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
	            }
        	}
            tMMap.put(tLJAPayGrpSet, "INSERT");
        }
            
        if(tLJAPayPersonSet != null && tLJAPayPersonSet.size() > 0)
        {
        	if(mContType.equals("1") && mLJAPayPersonSet.get(1).getConfDate().compareTo(mdate)<0)
        	{
        		 for(int i = 1; i <= tLJAPayPersonSet.size(); i++)
 	            {
 	            	
 	                tLJAPayPersonSet.get(i).setPayNo(tNewPayNo);
 	                tLJAPayPersonSet.get(i).setSumActuPayMoney(-1 * mLJAPayPersonSet.get(i).getSumActuPayMoney());
 	                tLJAPayPersonSet.get(i).setSumDuePayMoney(-1 * mLJAPayPersonSet.get(i).getSumDuePayMoney());
 	                tLJAPayPersonSet.get(i).setConfDate(PubFun.getCurrentDate());
 	                tLJAPayPersonSet.get(i).setMakeDate(PubFun.getCurrentDate());
 	                tLJAPayPersonSet.get(i).setMakeTime(PubFun.getCurrentTime());
 	                tLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
 	                tLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
 	                tLJAPayPersonSet.get(i).setMoneyNoTax((mLJAPayPersonSet.get(i).getSumActuPayMoney())+"");
 	                tLJAPayPersonSet.get(i).setMoneyTax("0");
 	                tLJAPayPersonSet.get(i).setBusiType("01");
 	                tLJAPayPersonSet.get(i).setTaxRate("0.00");
 	            }
        		
        	}else if(mContType.equals("1") && mLJAPayPersonSet.get(1).getMoneyNoTax()!=null){
        		for(int i = 1; i <= tLJAPayPersonSet.size(); i++)
        		{
        			fmoneytax=Double.parseDouble(mLJAPayPersonSet.get(i).getMoneyTax());
        			fmoneytax = 0.0-fmoneytax;
        			mMoneytax = Double.toString(fmoneytax);
        			tLJAPayPersonSet.get(i).setPayNo(tNewPayNo);
        			tLJAPayPersonSet.get(i).setSumActuPayMoney(-1 * mLJAPayPersonSet.get(i).getSumActuPayMoney());
        			tLJAPayPersonSet.get(i).setSumDuePayMoney(-1 * mLJAPayPersonSet.get(i).getSumDuePayMoney());
        			tLJAPayPersonSet.get(i).setConfDate(PubFun.getCurrentDate());
        			tLJAPayPersonSet.get(i).setMakeDate(PubFun.getCurrentDate());
        			tLJAPayPersonSet.get(i).setMakeTime(PubFun.getCurrentTime());
        			tLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
        			tLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
        			tLJAPayPersonSet.get(i).setMoneyNoTax("-"+mLJAPayPersonSet.get(i).getMoneyNoTax());
        			tLJAPayPersonSet.get(i).setMoneyTax(mMoneytax);
        		}
        	}else {
        	
	            for(int i = 1; i <= tLJAPayPersonSet.size(); i++)
	            {
	            	
	                tLJAPayPersonSet.get(i).setPayNo(tNewPayNo);
	                tLJAPayPersonSet.get(i).setSumActuPayMoney(-1 * mLJAPayPersonSet.get(i).getSumActuPayMoney());
	                tLJAPayPersonSet.get(i).setSumDuePayMoney(-1 * mLJAPayPersonSet.get(i).getSumDuePayMoney());
	                tLJAPayPersonSet.get(i).setConfDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setMakeDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setMakeTime(PubFun.getCurrentTime());
	                tLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
	            }
            }
            tMMap.put(tLJAPayPersonSet, "INSERT");
        }
            
        tVData.add(tMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, "INSERT"))
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "PubSubmit";
            tError.errorMessage = "财务数据充负失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    //财务充正
    private boolean positiveRecoil()
    {
        LJAPaySet tLJAPaySet = mLJAPaySet;
        LJAPayGrpSet tLJAPayGrpSet = null;
        LJAPayPersonSet tLJAPayPersonSet = null;
        String tNewPayNo=PubFun1.CreateMaxNo("PAYNO",null);
        System.out.println("充正的PayNo = " + tNewPayNo);
        
        MMap tMMap = new MMap();
        VData tVData = new VData();
        if(mContType.equals("1"))
        {
            tLJAPayPersonSet = mLJAPayPersonSet;
        }else if(mContType.equals("2"))
        {
            tLJAPayGrpSet = mLJAPayGrpSet;
            tLJAPayPersonSet = mLJAPayPersonSet;
        }
        
        tLJAPaySet.get(1).setPayNo(tNewPayNo);
        tLJAPaySet.get(1).setDueFeeType("2");
        tLJAPaySet.get(1).setConfDate(PubFun.getCurrentDate());
        tLJAPaySet.get(1).setMakeDate(PubFun.getCurrentDate());
        tLJAPaySet.get(1).setMakeTime(PubFun.getCurrentTime());
        tLJAPaySet.get(1).setModifyDate(PubFun.getCurrentDate());
        tLJAPaySet.get(1).setModifyTime(PubFun.getCurrentTime());
        
        if(tLJAPayGrpSet != null && tLJAPayGrpSet.size() > 0)
        {	
        	if(mLJAPayGrpSet.get(1).getConfDate().compareTo(mdate)<0){
        		 for(int i = 1; i <= tLJAPayGrpSet.size(); i++)
 	            {
 	                tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
 	                tLJAPayGrpSet.get(i).setConfDate(PubFun.getCurrentDate());
 	                tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
 	                tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
 	                tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
 	                tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
 	                tLJAPayGrpSet.get(i).setMoneyNoTax(mLJAPayGrpSet.get(i).getSumActuPayMoney()+"");
	                tLJAPayGrpSet.get(i).setMoneyTax("0");
	                tLJAPayGrpSet.get(i).setBusiType("01");
	                tLJAPayGrpSet.get(i).setTaxRate("0.00");
 	            }
        	}else{
	            for(int i = 1; i <= tLJAPayGrpSet.size(); i++)
	            {
	                tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
	                tLJAPayGrpSet.get(i).setConfDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
	                tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
	                tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
	            }
        	}
        }
        
        if(tLJAPayPersonSet != null && tLJAPayPersonSet.size() > 0)
        {
        	if(mContType.equals("1") && mLJAPayPersonSet.get(1).getConfDate().compareTo(mdate)<0)
        	{
        		for(int i = 1; i <= tLJAPayPersonSet.size(); i++)
	            {
	                tLJAPayPersonSet.get(i).setPayNo(tNewPayNo);
	                tLJAPayPersonSet.get(i).setConfDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setMakeDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setMakeTime(PubFun.getCurrentTime());
	                tLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
	                tLJAPayPersonSet.get(i).setMoneyNoTax(mLJAPayPersonSet.get(i).getSumActuPayMoney()+"");
 	                tLJAPayPersonSet.get(i).setMoneyTax("0");
 	                tLJAPayPersonSet.get(i).setBusiType("01");
 	                tLJAPayPersonSet.get(i).setTaxRate("0.00");
	            }
        	}else{
	            for(int i = 1; i <= tLJAPayPersonSet.size(); i++)
	            {
	                tLJAPayPersonSet.get(i).setPayNo(tNewPayNo);
	                tLJAPayPersonSet.get(i).setConfDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setMakeDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setMakeTime(PubFun.getCurrentTime());
	                tLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
	                tLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
	            }
        	}
        }
        
        if(mOperate.equals("SaleChnl") || mOperate.equals("Both"))
        {
            tLJAPaySet.get(1).setSaleChnl(mSaleChnl);
        }

        if(mOperate.equals("AgentCode") || mOperate.equals("Both"))
        {
            tLJAPaySet.get(1).setAgentCom(mAgentCom);
            tLJAPaySet.get(1).setAgentCode(mAgentCode);
            tLJAPaySet.get(1).setAgentGroup(mAgentGroup);

            if(tLJAPayGrpSet != null && tLJAPayGrpSet.size() > 0)
            {
                for (int i = 1; i <= tLJAPayGrpSet.size(); i++)
                {
                    tLJAPayGrpSet.get(i).setAgentCom(mAgentCom);
                    tLJAPayGrpSet.get(i).setAgentCode(mAgentCode);
                    tLJAPayGrpSet.get(i).setAgentGroup(mAgentGroup);
                }
            }
            
            if(tLJAPayPersonSet != null && tLJAPayPersonSet.size() > 0)
            {
                for(int i = 1; i <= tLJAPayPersonSet.size(); i++)
                {
                    tLJAPayPersonSet.get(i).setAgentCom(mAgentCom);
                    tLJAPayPersonSet.get(i).setAgentCode(mAgentCode);
                    tLJAPayPersonSet.get(i).setAgentGroup(mAgentGroup);
                }
            }
        }
        tMMap.put(tLJAPaySet, "INSERT");
        if(mContType.equals("2"))
        {
            tMMap.put(tLJAPayGrpSet, "INSERT");
        }
        tMMap.put(tLJAPayPersonSet, "INSERT");
        tVData.add(tMMap);
        
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, "INSERT"))
        {
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "PubSubmit";
            tError.errorMessage = "财务数据充正失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 发送维护邮件
     * (暂时未使用) **处自己修改，如需使用可以将发送人及收件人加入到配置中
     * 
     * by lw 
     * 2017-06-13
     */ 
    public  void sendTextEmail()  { 
    	
		try{
			StringBuffer sb=new StringBuffer();
				if("1".equals(mContType)){
					LCContDB tLCContDB = new LCContDB();
		            tLCContDB.setPrtNo(mPrtNo);
		            tLCContDB.setContNo(mContNo);
		            LCContSet tLCContSet = new LCContSet();	            
		            tLCContSet = tLCContDB.query();
		            
		            LCContSchema tLCContSchema = tLCContSet.get(1);
					String agentcode=tLCContSchema.getAgentCode();
					String Agentcom=tLCContSchema.getAgentCom();
					String agentgroup=tLCContSchema.getAgentGroup();
					String salechnl=tLCContSchema.getSaleChnl();
					
					sb.append("大家好：<br>");
					sb.append("&nbsp&nbsp现有一保单，合同号："+mContNo+"的保单，需要维护业务员。<br>");
					sb.append("&nbsp&nbsp维护前：Contno："+mContNo+"；");
					if(!mAgentCode.equals(agentcode)){
						sb.append("AgentCode:"+agentcode+";");
					}
					if(!mAgentCom.equals(Agentcom)){
						sb.append("AgentCom:"+Agentcom+";");
					}
					if(!mAgentGroup.equals(agentgroup)){
						sb.append("Agentgroup:"+agentgroup+";");
					}
					if(!mSaleChnl.equals(salechnl)){
						sb.append("Salechnl:"+salechnl+";");
					}
					sb.append("<br>");
					sb.append("&nbsp&nbsp维护后：Contno："+mContNo+"；");
					if(!mAgentCode.equals(agentcode)){
						sb.append("AgentCode:"+mAgentCode+";");
					}
					if(!mAgentCom.equals(Agentcom)){
						sb.append("AgentCom:"+mAgentCom+";");
					}
					if(!mAgentGroup.equals(agentgroup)){
						sb.append("Agentgroup:"+mAgentGroup+";");
					}
					if(!mSaleChnl.equals(salechnl)){
						sb.append("Salechnl:"+mSaleChnl+";");
					}
					sb.append("<br>");
					sb.append("&nbsp&nbsp请问契约是否可以直接维护?<br>");
					sb.append("<br>");
					sb.append("<HR width='10%' SIZE=1 style='float:left;'>");
					sb.append("<br>");
					sb.append("&nbsp&nbsp&nbsp**<br>");
					sb.append("&nbsp&nbsp&nbsp"+PubFun.getCurrentDate()+"<br>");
					sb.append("&nbsp&nbsp&nbsp中科软科技股份有限公司");
					
					
				}
				
				Properties props = new Properties();
				//Setup mail server
				props.put("mail.smtp.host", host);//设置smtp主机
				props.put("mail.transport.protocol", "smtp");
				props.put("mail.smtp.auth", "true");//使用smtp身份验证
				//Get session
				Session session = Session.getDefaultInstance(props, null);
				//Define message
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(sender, "**"));
				 // 3. To: 收件人
		        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress("***@sinosoft.com.cn", "USER_CC", "UTF-8"));
		        //    To: 增加收件人（可选）
//		        message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress("dd@receive.com", "USER_DD", "UTF-8"));
		        //    Cc: 抄送（可选）
		       // message.setRecipient(MimeMessage.RecipientType.CC, new InternetAddress("ee@receive.com", "USER_EE", "UTF-8"));
		        //    Bcc: 密送（可选）
//		        message.setRecipient(MimeMessage.RecipientType.BCC, new InternetAddress("ff@receive.com", "USER_FF", "UTF-8"));
				//message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));	
				message.setSubject("测试","UTF-8");		//主题名	
				message.setContent(sb.toString(), "text/html;charset=UTF-8"); //文本样式与HTML相同，如需添加样式参考html
				//添加附件
//				BodyPart messageBodyPart = new MimeBodyPart();     
//	            messageBodyPart.setText("bodypart");     
//	              
//	            Multipart multipart = new MimeMultipart();     
//	            multipart.addBodyPart(messageBodyPart);     
//	              
//	            messageBodyPart = new MimeBodyPart();     
//	              
//	            //	          设置上传的资源  
//	            DataSource source = new FileDataSource("E:\\111.jpg");    
//	            //	          添加到  
//	            messageBodyPart.setDataHandler(new DataHandler(source));     
//	            //	          设置文件名称,记得后缀名  
//	            messageBodyPart.setFileName("test.doc");     
//	            multipart.addBodyPart(messageBodyPart);    
//	              
//	            message.setContent(multipart);     
				
				
				message.saveChanges();
				//Send message
				Transport transport = session.getTransport();	
				System.out.println("******正在连接" + host);
				transport.connect(host, sender, password);
				System.out.println("******正在发送给" + "");
				transport.sendMessage(message, message.getAllRecipients());
				System.out.println("******邮件发送成功");
	        
			
		}catch(Exception e){
			System.out.println("发送普通邮件异常"+e);
			
		}	
		
    } 
    public static void main(String[] args)  {
		QYMaintenanceBL q1=new QYMaintenanceBL();
		q1.mContType="1";
		q1.mAgentCode="123";
		q1.mAgentCom="231";
		q1.mAgentGroup="123";
		q1.mSaleChnl="03";
		q1.mContNo="014077626000020";
		q1.mPrtNo="170612002";
		q1.sendTextEmail();
		
	}
}

