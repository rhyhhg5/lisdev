package com.sinosoft.lis.midplat;

import com.sinosoft.lis.db.LOMixBKAttributeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LOMixBKAttributeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: ybtMixedComBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz 2009-1-23
 * @version 1.0
 */
public class ybtMixedComUpdateBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LOMixBKAttributeSchema mLOMixBKAttributeSchema = new LOMixBKAttributeSchema();
  private Reflections ref = new Reflections();
  //private LARateCommisionBSet mLARateCommisionBSet = new LARateCommisionBSet();

  public ybtMixedComUpdateBL() {

  }

//	public static void main(String[] args)
//	{
//		ybtMixedComBL ybtMixedComBL = new ybtMixedComBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin ybtMixedComBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    System.out.println("..............here3"+this.mOperate);
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start ybtMixedComBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "ybtMixedComBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验
	  
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin ybtMixedComBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLOMixBKAttributeSchema=(LOMixBKAttributeSchema) cInputData.getObjectByObjectName("LOMixBKAttributeSchema",0);
      
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ybtMixedComBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin ybtMixedComBL.dealData........."+mOperate);
    try {
    	if(this.mOperate.equals("INSERT")){
    		this.mLOMixBKAttributeSchema.setOperator(this.mGlobalInput.Operator);
    		this.mLOMixBKAttributeSchema.setMakeDate(this.CurrentDate);
    		this.mLOMixBKAttributeSchema.setMakeTime(this.CurrentTime);
    		this.mLOMixBKAttributeSchema.setModifyDate(this.CurrentDate);
    		this.mLOMixBKAttributeSchema.setModifyTime(this.CurrentTime);
    	}else if(this.mOperate.equals("UPDATE")){
    		LOMixBKAttributeDB tLOMixBKAttributeDB=new LOMixBKAttributeDB();
    		tLOMixBKAttributeDB.setBandCode(this.mLOMixBKAttributeSchema.getBandCode());
    		tLOMixBKAttributeDB.setBankNode(this.mLOMixBKAttributeSchema.getBankNode());
    		tLOMixBKAttributeDB.setZoneNo(this.mLOMixBKAttributeSchema.getZoneNo());
    		tLOMixBKAttributeDB.getInfo();
    		this.mLOMixBKAttributeSchema.setOperator(this.mGlobalInput.Operator);
    		this.mLOMixBKAttributeSchema.setMakeDate(tLOMixBKAttributeDB.getMakeDate());
    		this.mLOMixBKAttributeSchema.setMakeTime(tLOMixBKAttributeDB.getMakeTime());
    		this.mLOMixBKAttributeSchema.setModifyDate(this.CurrentDate);
    		this.mLOMixBKAttributeSchema.setModifyTime(this.CurrentTime);
    		
    		
    	}else if(this.mOperate.equals("DELETE")){
    		
    	}
        map.put(mLOMixBKAttributeSchema, mOperate);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ybtMixedComBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin ybtMixedComBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ybtMixedComBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
