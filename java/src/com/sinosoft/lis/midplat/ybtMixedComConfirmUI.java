package com.sinosoft.lis.midplat;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: ybtMixedComConfirmUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz 2009-1-23
 * @version 1.0
 */
public class ybtMixedComConfirmUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */

	/** 数据操作字符串 */
	private String mOperate = "";

	public ybtMixedComConfirmUI() {
	}

	public static void main(String[] args) {

	}

	/**
	 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("Begin ybtMixedComConfirmUI.submitData.........");
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		try
        {
            mOperate = cOperate;

            ybtMixedComConfirmBL tybtMixedComConfirmBL = new ybtMixedComConfirmBL();

            boolean bReturn = tybtMixedComConfirmBL.submitData(cInputData, mOperate);

            if (!bReturn)
            {
                if (tybtMixedComConfirmBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tybtMixedComConfirmBL.mErrors);
                }
                else
                {
                    buildError("submitData", "ybtMixedComConfirmUI出错，但是没有提供详细的错误信息");
                }
            }

            return bReturn;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", "发生异常");
            return false;
        }
	}
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ybtMixedComConfirmUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
