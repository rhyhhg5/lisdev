package com.sinosoft.lis.midplat;

import com.sinosoft.lis.vschema.LOMixBKAttributeSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: ybtMixedComConfirmBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz 2009-1-23
 * @version 1.0
 */
public class ybtMixedComConfirmBL {
	//错误处理类
	public CErrors mErrors = new CErrors();

	//业务处理相关变量
	/** 全局数据 */
	private VData mInputData = new VData();

	private String mOperate = "";

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	private MMap map = new MMap();

	public GlobalInput mGlobalInput = new GlobalInput();

	private LOMixBKAttributeSet mLOMixBKAttributeSet = new LOMixBKAttributeSet();

	public ybtMixedComConfirmBL() {

	}

	/**
	 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("Begin ybtMixedComConfirmBL.submitData.........");
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!check()) {
			return false;
		}
		//进行业务处理
		if (!dealData()) {
			return false;
		}
		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ybtMixedComConfirmBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ybtMixedComConfirmBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 
	 */
	private boolean check() {
		//进行重复数据校验

		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		//全局变量
		try {
			System.out
					.println("Begin ybtMixedComConfirmBL.getInputData.........");
			this.mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));
			this.mLOMixBKAttributeSet = (LOMixBKAttributeSet) cInputData
					.getObjectByObjectName("LOMixBKAttributeSet", 0);

		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ybtMixedComConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "在读取处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("getInputData end ");
		return true;
	}

	/**
	 * 业务处理主函数
	 */
	private boolean dealData() {
		System.out.println("Begin ybtMixedComConfirmBL.dealData........."
				+ mOperate);
		try {
			if (this.mOperate.equals("UPDATE")) {
				for (int i = 1; i <= mLOMixBKAttributeSet.size(); i++) {
					String tSql = "update LOMixBKAttribute set state = '1',modifydate = '"+CurrentDate+"',modifytime = '"+CurrentTime+"' where BandCode = '"+mLOMixBKAttributeSet.get(i).getBandCode()+"' ";
					map.put(tSql, mOperate);
				}

			}
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ybtMixedComConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "在处理所数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		try {
			System.out
					.println("Begin ybtMixedComConfirmBL.prepareOutputData.........");
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ybtMixedComConfirmBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
