/**
 * Copyright (c) 2007 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import java.util.*;

/*
 * <p>ClassName: TempCessContBL </p>
 * <p>Description: TempCessContBL类文件 </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft </p>
 * @Modify by: Huxl
 * @ModifyDate：2007-08-25
 */
public class TempCessContBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String strOperate;
    private LRTempCessContSchema mLRTempCessContSchema = new
            LRTempCessContSchema();
    private LRTempCessContInfoSet mLRTempCessContInfoSet = new
            LRTempCessContInfoSet();
    private LCPolSet mLCPolSet = new LCPolSet();
    private MMap mMap = new MMap();

    private String mReContCode = ""; //再保公司代码
    private String mRiskCalSort = "";

    public TempCessContBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "保存临分协议信息时出现错误!");
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        String tCValiDate = "2006-1-1";
        String tEdorValiDate = "2005-11-1";
        int years = PubFun.calInterval(tCValiDate, tEdorValiDate, "Y");
        System.out.println(years);

        LDAgentCardCountDB tLDAgentCardCountDB = new LDAgentCardCountDB();
        tLDAgentCardCountDB.setAgentCode("Y");
        tLDAgentCardCountDB.setCertifyCode("AA");
        tLDAgentCardCountDB.setAgentGrade("N");
        tLDAgentCardCountDB.setManageCom("N");

        if (tLDAgentCardCountDB.getInfo()) {
            System.out.println("AAAAAAAAAAAAAAAAAA:" +
                               tLDAgentCardCountDB.getAgentCode());
        }

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TempCessContBL tTempCessContBL = new TempCessContBL();
        try {

            vData.add(globalInput);
            tTempCessContBL.submitData(vData, "INSERT");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TempCessContBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 对于临分保单的分保记录,
     * 1.存储LRTempCessCont LRTempCessContInfo 表.这样方便存储和修改.对于临分保单的长险续期,短险续保暂时不考虑,然后存储LRPOl
     *   将来考虑增加备份表.
     * 2.对于临分保单的保全提数,理赔提数,同合同分保保单一块提取.数据保存在LRPolEdor,LRPolClm
     * 3.临分保单的分保计算,在下达临分结论时,直接生成.保全理赔数据同合同分保计算时生成.保全理赔结果数据保存同LRPolEdorResult,LRPolClmResult;
     * //added by huxl @ 2007-8-24
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();
        Reflections tReflections = new Reflections();

        LRTempCessContDB tLRTempCessContDB = new LRTempCessContDB();
        tLRTempCessContDB.setReContCode(mLRTempCessContSchema.getReContCode());
        if (strOperate.equals("INSERT")) {
            if (tLRTempCessContDB.getInfo()) {
                buildError("dealData", "该临分协议已经存在,请选择新的临分协议!");
                return false;
            }
            //如果存在临分保单，插入LRTempCessCont LRTempCessContInfo LRPol,修改分保标记lcpol.
            if (mLRTempCessContInfoSet.size() > 0) {
                mLRTempCessContSchema.setManageCom(globalInput.ComCode);
                mLRTempCessContSchema.setOperator(globalInput.Operator);
                mLRTempCessContSchema.setMakeDate(currentDate);
                mLRTempCessContSchema.setMakeTime(currentTime);
                mLRTempCessContSchema.setModifyDate(currentDate);
                mLRTempCessContSchema.setModifyTime(currentTime);
                this.mMap.put(mLRTempCessContSchema, "INSERT"); //插入新的临分合同记录

                for (int i = 1; i <= mLRTempCessContInfoSet.size(); i++) {
                    LRTempCessContInfoSchema tLRTempCessContInfoSchema =
                            mLRTempCessContInfoSet.get(i);
                    tLRTempCessContInfoSchema.setManageCom(globalInput.ComCode);
                    tLRTempCessContInfoSchema.setOperator(globalInput.Operator);
                    tLRTempCessContInfoSchema.setMakeDate(currentDate);
                    tLRTempCessContInfoSchema.setMakeTime(currentTime);
                    tLRTempCessContInfoSchema.setModifyDate(currentDate);
                    tLRTempCessContInfoSchema.setModifyTime(currentTime);

                    LCPolDB tLCPolDB = new LCPolDB();
                    tLCPolDB.setProposalNo(tLRTempCessContInfoSchema.
                                           getProposalNo());
                    LCPolSet tLCPolSet = tLCPolDB.query();
                    if (tLCPolSet.size() == 0) {
                        buildError("insertData", "该个人险种保单不存在!");
                        return false;
                    }
                    LCPolSchema tLCPolSchema = tLCPolSet.get(1);
                    LRPolSchema tLRPolSchema = new LRPolSchema();
                    tReflections.transFields(tLRPolSchema, tLCPolSchema);
                    prepareData(tLRPolSchema, tLCPolSchema);
                    tLRPolSchema.setReNewCount(tLCPolSchema.getRenewCount());

                    this.mMap.put(tLRPolSchema, "INSERT"); //添加LRPOl记录
                    this.mMap.put(tLRTempCessContInfoSchema, "INSERT");
                }
            }
            //下面处理非临分保单
            //对于合同分保保单，需要调用分保批处理程序，置分保标记和分保保额。这里由于临分结论下达的时候已经超过了保单的签单日期，所以计算累计保额，
            //累计分保保额都会出现问题，所以以当前日期为基准进行批处理判断(但系统没有记录该日期)。暂不开发。added by huxl @070826
            //对于临分未实现保单，直接修改分保标记。
            for (int i = 1; i <= mLCPolSet.size(); i++) {
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setProposalNo(mLCPolSet.get(i).getProposalNo());
                LCPolSet tLCPolSet = tLCPolDB.query();
                if (tLCPolSet.size() == 0) {
                    buildError("insertData", "该个人险种保单不存在!");
                    return false;
                }
                LCPolSchema tLCPolSchema = tLCPolSet.get(1);
                tLCPolSchema.setReinsureFlag(mLCPolSet.get(i).getReinsureFlag());
                this.mMap.put(tLCPolSchema, "UPDATE");
            }
        } else if (strOperate.equals("UPDATE")) {
            if (mLRTempCessContInfoSet.size() == 0) {
                this.mMap.put(mLRTempCessContSchema, "DELETE"); //删除临分合同记录
            } else {
                for (int i = 1; i <= mLRTempCessContInfoSet.size(); i++) {
                    LRTempCessContInfoSchema tLRTempCessContInfoSchema =
                            mLRTempCessContInfoSet.get(i);
                    tLRTempCessContInfoSchema.setManageCom(globalInput.ComCode);
                    tLRTempCessContInfoSchema.setOperator(globalInput.Operator);
                    tLRTempCessContInfoSchema.setModifyDate(currentDate);
                    tLRTempCessContInfoSchema.setModifyTime(currentTime);

                    LCPolDB tLCPolDB = new LCPolDB();
                    tLCPolDB.setProposalNo(tLRTempCessContInfoSchema.
                                           getProposalNo());
                    LCPolSet tLCPolSet = tLCPolDB.query();
                    if (tLCPolSet.size() == 0) {
                        buildError("insertData", "该个人险种保单不存在!");
                        return false;
                    }
                    LCPolSchema tLCPolSchema = tLCPolSet.get(1);
                    LRPolSchema tLRPolSchema = new LRPolSchema();
                    tReflections.transFields(tLRPolSchema, tLCPolSchema);
                    prepareData(tLRPolSchema, tLCPolSchema);
                    tLRPolSchema.setReNewCount(tLCPolSchema.getRenewCount());

                    this.mMap.put(tLRPolSchema, "DELETE&INSERT"); //添加LRPOl记录
                    this.mMap.put(tLRTempCessContInfoSchema,
                                  "DELETE&INSERT");
                }
            }
            for (int i = 1; i <= mLCPolSet.size(); i++) {
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setProposalNo(mLCPolSet.get(i).getProposalNo());
                LCPolSet tLCPolSet = tLCPolDB.query();
                if (tLCPolSet.size() == 0) {
                    buildError("insertData", "该个人险种保单不存在!");
                    return false;
                }
                LCPolSchema tLCPolSchema = tLCPolSet.get(1);
                tLCPolSchema.setReinsureFlag(mLCPolSet.get(i).getReinsureFlag());
                this.mMap.put(tLCPolSchema, "UPDATE");
                //下面删除曾经临分的保单LRPol LRTempCessContInfo
                LRPolDB tLRPolDB = new LRPolDB();
                tLRPolDB.setReContCode(mLRTempCessContSchema.getReContCode());
                tLRPolDB.setReinsureItem("C");
                tLRPolDB.setPolNo(tLCPolSchema.getPolNo());
                tLRPolDB.setReNewCount(tLCPolSchema.getRenewCount());
                if (tLRPolDB.getInfo()) {
                    this.mMap.put(tLRPolDB.getSchema(), "DELETE");
                }
                LRTempCessContInfoDB tLRTempCessContInfoDB = new
                        LRTempCessContInfoDB();
                tLRTempCessContInfoDB.setReContCode(mLRTempCessContSchema.
                        getReContCode());
                tLRTempCessContInfoDB.setProposalNo(tLCPolSchema.getProposalNo());
                if (tLRTempCessContInfoDB.getInfo()) {
                    this.mMap.put(tLRTempCessContInfoDB.getSchema(), "DELETE");
                }
            }
        }
        return true;
    }


//                    if (cessionModeFlag.equals("2")) { //如果是溢额分保
//                        //得到该险种的算法
//                        String calCode = tCalRiskAmntBL.getCalCode(
//                                mLRTempCessContSchema.getReComCode(),
//                                tLRTempCessContInfoSchema.getRiskCode());
//                        if (calCode == null) {
//                            buildError("insertData",
//                                       tCalRiskAmntBL.mErrors.getFirstError());
//                            return false;
//                        }
//                        //得到需修改状态的LCPol
//                        LCPolDB tLCPolDB = new LCPolDB();
//                        tLCPolDB.setProposalNo(tLRTempCessContInfoSchema.
//                                               getProposalNo());
//                        LCPolSet tLCPolSet = tLCPolDB.query();
//                        if (tLCPolSet.size() == 0) {
//                            buildError("insertData", "该个人险种保单不存在!");
//                            return false;
//                        }
//                        LCPolSchema tLCPolSchema = tLCPolSet.get(1); //！是 get(1) ,因为ProposalNo不是主键所以得到了一个Set
//                        tReflections.transFields(tLRPolSchema, tLCPolSchema); //
//
//                        retainAmnt = tLRTempCessContInfoSchema.
//                                     getRetainAmnt();
//                        riskAmnt = tLCPolSchema.getRiskAmnt();
//                        mCessionAmount = calCessAmnt(retainAmnt, riskAmnt); //计算分保保额
//                        tLRTempCessContInfoSchema.setCessionAmount(
//                                mCessionAmount);
//                        this.mCessPremRate = tCalRiskAmntBL.calCessFeeRate(
//                                calCode,
//                                tLCPolSchema,
//                                tLRTempCessContInfoSchema.getDiseaseAddFee(),
//                                tLRTempCessContInfoSchema.getDeadAddFee()); //计算分保费率
//                        if (this.mCessPremRate == -1) {
//                            buildError("insertData",
//                                       tCalRiskAmntBL.mErrors.getFirstError());
//                            return false;
//                        }
//                        mCessPrem = mCessionAmount * mCessPremRate / 1000; //计算分保保费
//                        tLRTempCessContInfoSchema.setCessionAmount(
//                                mCessionAmount);
//                        tLRTempCessContInfoSchema.setCessionFee(mCessPrem);
//                        System.out.println("分保保费:" + this.mCessPrem);
//                        prepareData(tLRPolSchema, tLCPolSchema);
//                        //回零
//                        this.mCessionAmount = 0;
//                        this.mCessionRate = 0;
//                        this.mCessPrem = 0;
//                        this.mCessPremRate = 0;
//                    } else if (cessionModeFlag == "1") { //如果是成数分保
//                        System.out.println("成数分保： ");
//                        String riskCode = tLRTempCessContInfoSchema.
//                                          getRiskCode();
//
//                        //得到该险种的险种类别
//                        String strSQL =
//                                "select distinct risksort from LRCalFactorValue where riskcode='" +
//                                riskCode + "'";
//                        SSRS riskSort = new ExeSQL().execSQL(strSQL);
//                        String[][] tempdata;
//                        if (riskSort == null || riskSort.getMaxRow() <= 0) {
//                            buildError("insertData", "没有该险种的险种类别!");
//                            return false;
//                        }
//                        tempdata = riskSort.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
//                        this.mRiskCalSort = tempdata[0][0];
//                        tempdata = null;
//
//                        LCPolDB tLCPolDB = new LCPolDB();
//                        tLCPolDB.setProposalNo(tLRTempCessContInfoSchema.
//                                               getProposalNo());
//                        LCPolSet tLCPolSet = tLCPolDB.query();
//                        if (tLCPolSet.size() == 0) {
//                            buildError("insertData", "该个人险种保单不存在!");
//                            return false;
//                        }
//                        LCPolSchema tLCPolSchema = tLCPolSet.get(1);
//                        tReflections.transFields(tLRPolSchema, tLCPolSchema); //!!!将mLCPolSchema的值赋给mLRPolSchema， 即更新LRPol数据
//
//                        System.out.println("自留额: " + retainAmnt);
//                        System.out.println("风险保额: " + riskAmnt);
//                        System.out.println("分保保额: " +
//                                           calCessAmnt(retainAmnt, riskAmnt));
//                        System.out.println("保费: " + tLCPolSchema.getPrem());
//
//                        this.mCessPrem = tLCPolSchema.getPrem() *
//                                         tLRTempCessContInfoSchema.
//                                         getCessionRate();
//                        tLRTempCessContInfoSchema.setCessionAmount(0); //成数分保不计算分保保额
//                        tLRTempCessContInfoSchema.setCessionFee(this.
//                                mCessPrem);
//                        System.out.println("分保比例: " +
//                                           tLRTempCessContInfoSchema.
//                                           getCessionRate());
//                        System.out.println("分保保费: " + this.mCessPrem);
//                        prepareData(tLRPolSchema, tLCPolSchema);
//
//                        //回零
//                        this.mCessionAmount = 0;
//                        this.mCessionRate = 0;
//                        this.mCessPrem = 0;
//                        this.mCessPremRate = 0;
//                    }
    //如果结论为非临分，则删除临分协议关联信息和再保信息
//            LRTempCessContInfoSchema delLRTempCessContInfoSchema;
//            LRTempCessContInfoDB delLRTempCessContInfoDB;
//            LRTempCessContInfoSet tDelLRTempCessContInfoSet;
//            LRTempCessContInfoSet delLRTempCessContInfoSet = new
//                    LRTempCessContInfoSet();
//
//            LRPolSchema delLRPolSchema;
//            LRPolDB delLRPolDB;
//            LRPolSet tDelLRPolSet;
//            LRPolSet delLRPolSet = new LRPolSet();
//            if (mProposalList.size() > 0) {
//                Iterator iterator = mProposalList.iterator();
//                String strProposalNo = "";
//                while (iterator.hasNext()) {
//                    strProposalNo = (String) iterator.next();
//                    delLRTempCessContInfoSchema = new LRTempCessContInfoSchema();
//                    delLRTempCessContInfoDB = new LRTempCessContInfoDB();
//                    tDelLRTempCessContInfoSet = new LRTempCessContInfoSet();
//
//                    delLRTempCessContInfoDB.setProposalNo(strProposalNo);
//                    //delLRTempCessContInfoDB.setReComCode(mLRTempCessContSchema.getReComCode());
//                    delLRTempCessContInfoDB.setReContCode(mLRTempCessContSchema.
//                            getReContCode());
//                    tDelLRTempCessContInfoSet = delLRTempCessContInfoDB.query();
//                    if (tDelLRTempCessContInfoSet.size() > 0) {
//                        delLRTempCessContInfoSchema = tDelLRTempCessContInfoSet.
//                                get(
//                                        1);
//                    }
//
//                    delLRTempCessContInfoSet.add(delLRTempCessContInfoSchema);
//                    delLRPolSchema = new LRPolSchema();
//                    delLRPolDB = new LRPolDB();
//                    delLRPolDB.setProposalNo(strProposalNo);
//                    //delLRPolDB.setReinsureCom(this.mReinsureCom);
//                    delLRPolDB.setReinsureItem("C");
//                    delLRPolDB.setRiskCalSort(this.mRiskCalSort);
//                    tDelLRPolSet = delLRPolDB.query();
//                    if (tDelLRPolSet.size() > 0) {
//                        delLRPolSchema = tDelLRPolSet.get(1);
//                    }
//                    delLRPolSet.add(delLRPolSchema);
//                }
//            }
//
//            LRTempCessContInfoDB uDelLRTempCessContInfoDB = new
//                    LRTempCessContInfoDB();
////        LRTempCessContInfoSchema uDelLRTempCessContInfoSchema=new LRTempCessContInfoSchema();
//            LRTempCessContInfoSet uDelLRTempCessContInfoSet = new
//                    LRTempCessContInfoSet();
//            LRPolDB tuDelLRPolDB;
//            //LRPolSchema tuDelLRPolSchema;
//            LRPolSet tuDelLRPolSet;
//            LRPolSet uDelLRPolSet = new LRPolSet();
//            if (this.strOperate.equals("UPDATE")) {
//                uDelLRTempCessContInfoDB.setReContCode(mLRTempCessContSchema.
//                        getReContCode());
//                uDelLRTempCessContInfoSet = uDelLRTempCessContInfoDB.query();
//                if (uDelLRTempCessContInfoSet.size() > 0) {
//                    for (int i = 1; i <= uDelLRTempCessContInfoSet.size(); i++) {
//                        tuDelLRPolDB = new LRPolDB();
//                        tuDelLRPolSet = new LRPolSet();
//                        //tuDelLRPolSchema= new LRPolSchema();
//                        tuDelLRPolDB.setProposalNo(uDelLRTempCessContInfoSet.
//                                get(i).
//                                getProposalNo());
//                        tuDelLRPolSet = tuDelLRPolDB.query();
//                        if (tuDelLRPolSet.size() > 0) {
//                            uDelLRPolSet.add(tuDelLRPolSet.get(1));
//                        }
//                    }
//                }
//                mMap.put(uDelLRPolSet, "DELETE");
//                mMap.put(uDelLRTempCessContInfoSet, "DELETE");
//            }
//            if (mLRTempCessContInfoSet.size() > 0) {
//                if (this.strOperate.equals("UPDATE")) { //如果是修改
//                    mMap.put(mLRTempCessContSchema, "DELETE&INSERT");
//                } else {
//                    mMap.put(mLRTempCessContSchema, "INSERT");
//                }
//                mMap.put(aLRTempCessContInfoSet, "DELETE");
//                mMap.put(mLRTempCessContInfoSet, "INSERT");
//            }



    /**
     * 计算临分分保保额
     * @return double
     */
    public double calCessAmnt(double retainAmnt, double riskAmnt) {
        double calCessAmnt = riskAmnt - retainAmnt;
        return calCessAmnt;
    }

    /**
     * 准备插入的临分数据
     * @return double
     */
    private void prepareData(LRPolSchema aLRPolSchema,
                             LCPolSchema aLCPolSchema) {
        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        //得到该险种的险种类别
        String strSQL =
                "select distinct risksort from LRCalFactorValue where riskcode='" +
                aLCPolSchema.getRiskCode() + "'";
        SSRS riskSort = new ExeSQL().execSQL(strSQL);
        if (riskSort == null || riskSort.getMaxRow() <= 0) {
            buildError("insertData", "没有该险种的险种类别!");
            return;
        }
        mRiskCalSort = riskSort.GetText(1, 1);
        aLRPolSchema.setReinsureItem("C"); //再保项目 L--法定分保 C--商业分保
        aLRPolSchema.setReContCode(this.mReContCode); //再保公司
        aLRPolSchema.setRiskCalSort(this.mRiskCalSort); //险种计算代码
        aLRPolSchema.setTempCessFlag("Y"); //设置临分标记,Y表示临分
        aLRPolSchema.setCessStart(currentDate);
        aLRPolSchema.setCessEnd("");
        aLRPolSchema.setGetDataDate(mLRTempCessContSchema.getInputDate());//临分日期为合同录入日期
        aLRPolSchema.setMakeDate(currentDate);
        aLRPolSchema.setMakeTime(currentTime);
        aLRPolSchema.setModifyDate(currentDate);
        aLRPolSchema.setModifyTime(currentTime);
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLRTempCessContSchema.setSchema((LRTempCessContSchema) cInputData.
                                             getObjectByObjectName(
                "LRTempCessContSchema", 0));
        this.mLRTempCessContInfoSet.set((LRTempCessContInfoSet) cInputData.
                                        getObjectByObjectName(
                                                "LRTempCessContInfoSet", 0));
        this.mLCPolSet.set((LCPolSet) cInputData.getObjectByObjectName(
                "LCPolSet", 0));
        this.mReContCode = mLRTempCessContSchema.getReContCode();
        this.strOperate = cOperate;
        return true;
    }

    public String getResult() {
        return mLRTempCessContSchema.getReContCode();
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "TempCessContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
}
