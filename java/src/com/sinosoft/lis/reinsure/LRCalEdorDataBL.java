/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

/*
 * <p>ClassName: LRCalEdorDataBL </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: huxl
 * @CreateDate：2007-03-27
 */
public class LRCalEdorDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    //业务处理相关变量
    /** 全局数据 */

    public LRCalEdorDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "86";
        globalInput.Operator = "rm0001";
        globalInput.ManageCom = "86";

        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRCalEdorDataBL tLRCalEdorDataBL = new LRCalEdorDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2009-8-1");
            tTransferData.setNameAndValue("EndDate", "2009-8-31");
            vData.add(tTransferData);

            tLRCalEdorDataBL.submitData(vData, "EDORDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取保全数据
        if (this.strOperate.equals("EDORDATA")) {
            if (!calculateEdorData()) {
                buildError("updateData", "再保保全计算失败");
                return false;
            }
            if (!calculateEdorDataXB()) {
                buildError("updateData", "再保保全计算失败");
                return false;
            }

        }
        return true;
    }

    /**
     * 计算保全数据
     * @return boolean
     */
    private boolean calculateEdorData() {
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");

        String strSQL = "select * from LRPolEdor a where GetDataDate between '" +
                        mStartDate + "' and '" + mEndDate +
                        "' with ur";
        LRPolEdorSet tLRPolEdorSet = new LRPolEdorSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolEdorSet, strSQL);
        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolEdorSet.size(); i++) {
                    LRPolEdorSchema tLRPolEdorSchema = tLRPolEdorSet.get(i);
                    LRPolEdorResultSchema tLRPolEdorResultSchema = new
                            LRPolEdorResultSchema();

                    LRPolDB tLRPolDB = new LRPolDB();
                    tLRPolDB.setPolNo(tLRPolEdorSchema.getPolNo());
                    tLRPolDB.setReContCode(tLRPolEdorSchema.getReContCode());
                    tLRPolDB.setReinsureItem(tLRPolEdorSchema.
                                                 getReinsureItem());
                    tLRPolDB.setReNewCount(tLRPolEdorSchema.getReNewCount());
                    LRPolSet tLRPolSet = tLRPolDB.query();
                    int countDays=0; //保单的有效保障天数。（因为存在S型险和M型险保障不到一年的情况。M型险卖多年暂不处理）
                    if(tLRPolSet.size()>0){
                        String aSql =
                                " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='" +
                                tLRPolSet.get(1).getRiskCode() + "' with ur";
                        String LRiskFlag = new ExeSQL().getOneValue(aSql);
                        if (Integer.parseInt(LRiskFlag) > 0) {
                            countDays = 365;
                        } else {
                            countDays = PubFun.calInterval(tLRPolSet.get(1).getCValiDate(),
                                    tLRPolSet.get(1).getEndDate(), "D");
                        }
                        if (countDays <= 0) {//保单的保障天数。理论上不可能为0。
                            countDays = 365;
                        }
                    }
                    if (tLRPolEdorSchema.getRiskCalSort().equals("1")) {
                        //处理短险成数
                        double cessRate = getCessionRate(tLRPolEdorSchema);
                        double speCemmRate = this.getSpeCemmRate(
                                tLRPolEdorSchema);
                        double reProcFeeRate = this.getReProcFeeRate(
                                tLRPolEdorSchema);
                        if(tLRPolEdorSchema.getRiskCode().equals("5503")){
                            String str =  //获取5503的分保手续费率.由于费率取不定值,所以要从分保计算结果表中取数.
                                    "select REPROCFEERATE from lrpolresult where recontcode='" + tLRPolEdorSchema.getReContCode() +
                                    "' and polno='"+tLRPolEdorSchema.getPolNo()+"' and riskcode='" +
                                    tLRPolEdorSchema.getRiskCode()+ "' and renewcount="+tLRPolEdorSchema.getReNewCount()+" with ur";
                            SSRS tSSRS = new ExeSQL().execSQL(str);
                            if (tSSRS.getMaxRow() <= 0) {
                                reProcFeeRate=0;
                            }else  if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
                               reProcFeeRate=0;
                            }else{
                                reProcFeeRate = Double.parseDouble(tSSRS.GetText(1, 1));
                            }
                        }
                        if (tLRPolSet.size() > 0) {
                            int tPayIntv = tLRPolSet.get(1).getPayIntv();
                            double shouldReturnPrem = 0;
                            int tRemainCount = 0;
                            if (tPayIntv == 1 || tPayIntv == 3 || tPayIntv == 6 ||
                                tPayIntv == 12) {
                                LRPolResultDB tLRPolResultDB = new LRPolResultDB();
                                tLRPolResultDB.setPolNo(tLRPolEdorSchema.getPolNo());
                                tLRPolResultDB.setReContCode(tLRPolEdorSchema.
                                        getReContCode());
                                tLRPolResultDB.setReinsureItem(tLRPolEdorSchema.
                                        getReinsureItem());
                                tLRPolResultDB.setReNewCount(tLRPolEdorSchema.
                                        getReNewCount());
                                LRPolResultSet tLRPolResultSet = tLRPolResultDB.query();
                                if(tLRPolResultSet.size()>0){
                                    shouldReturnPrem = tLRPolResultSet.get(1).
                                            getShouldCessPrem() *
                                            ( -1) *
                                            tLRPolEdorSchema.getNoPassDays() /
                                               countDays;
                                }else{
                                    shouldReturnPrem=0;
                                }
                                LBPolDB tLBPolDB = new LBPolDB();
                                tLBPolDB.setPolNo(tLRPolEdorSchema.getPolNo());
                                tLBPolDB.getInfo();
                                LBPolSchema tLBPolSchema = tLBPolDB.getSchema();
                                tRemainCount = PubFun.calInterval(tLBPolSchema.
                                        getPaytoDate(), tLBPolSchema.getEndDate(),
                                        "M") / tLBPolSchema.getPayIntv();
                            } else {
                                shouldReturnPrem = tLRPolEdorSchema.getGetMoney();
                            }
                            double edorBackFee = shouldReturnPrem * cessRate;
                            if(tLRPolEdorSchema.getFeeoperationType().equals("RB")){
                                tLRPolEdorResultSchema.setEdorBackFee(-edorBackFee); //如果为退保,应该是分出金额,钱数为正
                                tLRPolEdorResultSchema.setEdorProcFee(-edorBackFee *
                                    (speCemmRate + reProcFeeRate)); //保全手续费摊回
                            }else{
                                tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                                tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                    (speCemmRate + reProcFeeRate)); //保全手续费摊回
                            }

                            tLRPolEdorResultSchema.setShouldReturnPrem(
                                    shouldReturnPrem);
                            tLRPolEdorResultSchema.setRemainCount(tRemainCount);
                            prepareLRPolEdorResult(tLRPolEdorSchema,
                                    tLRPolEdorResultSchema);
                            this.mMap.put(tLRPolEdorResultSchema,
                                          "DELETE&INSERT");
                        } else {
                            this.errorLog(tLRPolEdorSchema, "没有该保单的分保数据");
                        }
                    } else {
                        //处理溢额分保,暂时不支持长险成数
                        LRPolResultDB tLRPolResultDB = new LRPolResultDB();
                        tLRPolResultDB.setPolNo(tLRPolEdorSchema.getPolNo());
                        tLRPolResultDB.setReContCode(tLRPolEdorSchema.
                                getReContCode());
                        tLRPolResultDB.setReinsureItem(tLRPolEdorSchema.
                                getReinsureItem());
                        tLRPolResultDB.setReNewCount(tLRPolEdorSchema.
                                getReNewCount());
                        LRPolResultSet tLRPolResultSet = tLRPolResultDB.query();
                        if (tLRPolResultSet.size() > 0) {
                            double cessPrem = tLRPolResultSet.get(1).
                                              getCessPrem(); //分保保费
                            double ChoiRebaFeeRate = tLRPolResultSet.get(1).
                                    getChoiRebaFeeRate(); //取新单计算时的选择折扣,不需要重新计算保单年度
                            double edorBackFee = cessPrem * ( -1) *
                                                 tLRPolEdorSchema.getNoPassDays() / countDays;
                            if(tLRPolEdorSchema.getFeeoperationType().equals("RB")){
                                tLRPolEdorResultSchema.setEdorBackFee(-edorBackFee); //如果为退保,应该是分出金额,钱数为正
                                tLRPolEdorResultSchema.setEdorProcFee(-edorBackFee *
                                    ChoiRebaFeeRate); //手续费摊回
                            }else{
                                tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                                tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                    ChoiRebaFeeRate); //手续费摊回
                            }

                            prepareLRPolEdorResult(tLRPolEdorSchema,
                                    tLRPolEdorResultSchema);
                            this.mMap.put(tLRPolEdorResultSchema,
                                          "DELETE&INSERT");
                        } else {
                            this.errorLog(tLRPolEdorSchema, "没有该保单的分保计算结果");
                        }
                    }
                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolEdorSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
        return true;
    }

    private boolean calculateEdorDataXB() {
       mStartDate = (String) mGetCessData.getValueByName("StartDate");
       mEndDate = (String) mGetCessData.getValueByName("EndDate");

       String strSQL = "select * from LRPolEdor a where GetDataDate between '" +
                       mStartDate + "' and '" + mEndDate +
                       "' and a.feeoperationtype='XB' with ur";
       LRPolEdorSet tLRPolEdorSet = new LRPolEdorSet();
       RSWrapper rswrapper = new RSWrapper();
       rswrapper.prepareData(tLRPolEdorSet, strSQL);
       try {
           do {
               rswrapper.getData();
               mMap = new MMap();
               for (int i = 1; i <= tLRPolEdorSet.size(); i++) {
                   LRPolEdorSchema tLRPolEdorSchema = tLRPolEdorSet.get(i);
                   LRPolEdorResultSchema tLRPolEdorResultSchema = new
                           LRPolEdorResultSchema();

                   LRPolDB tLRPolDB = new LRPolDB();
                   tLRPolDB.setPolNo(tLRPolEdorSchema.getEdorNo()); //如果是退保,分保信息在保全提数时被换号,此处存的是newpolno
                   tLRPolDB.setReContCode(tLRPolEdorSchema.getReContCode());
                   tLRPolDB.setReinsureItem(tLRPolEdorSchema.
                                                getReinsureItem());
                   tLRPolDB.setReNewCount(tLRPolEdorSchema.getReNewCount());
                   LRPolSet tLRPolSet = tLRPolDB.query();
                   int countDays=365; //保单的有效保障天数。（因为存在S型险和M型险保障不到一年的情况。M型险卖多年暂不处理）
                    if(tLRPolSet!=null&&tLRPolSet.size()>0){
                        String aSql =
                                " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='" +
                                tLRPolSet.get(1).getRiskCode() + "' with ur";
                        String LRiskFlag = new ExeSQL().getOneValue(aSql);
                        if (Integer.parseInt(LRiskFlag) > 0) {
                            countDays = 365;
                        } else {
                            countDays = PubFun.calInterval(tLRPolSet.get(1).
                                    getCValiDate(),
                                    tLRPolSet.get(1).getEndDate(), "D");
                        }
                        if (countDays <= 0) { //保单的保障天数。理论上不可能为0。
                            countDays = 365;
                        }

                        LRPolResultDB tLRPolResultDB = new LRPolResultDB();
                        tLRPolResultDB.setPolNo(tLRPolEdorSchema.getPolNo());
                        tLRPolResultDB.setReContCode(tLRPolEdorSchema.
                                getReContCode());
                        tLRPolResultDB.setReinsureItem(tLRPolEdorSchema.
                                getReinsureItem());
                        tLRPolResultDB.setReNewCount(tLRPolEdorSchema.
                                getReNewCount());
                        LRPolResultSet tLRPolResultSet = tLRPolResultDB.query();
                        if (tLRPolResultSet != null &&
                            tLRPolResultSet.size() > 0) {
                            double cessPrem = tLRPolResultSet.get(1).
                                              getCessPrem(); //分保保费
                            double edorBackFee = cessPrem * ( -1) *
                                                 tLRPolEdorSchema.getNoPassDays() /
                                                 countDays;
                            if(tLRPolEdorSchema.getRiskCalSort().equals("1")){
                                double speCemmRate = this.getSpeCemmRate(
                                        tLRPolEdorSchema);
                                double reProcFeeRate = this.getReProcFeeRate(
                                        tLRPolEdorSchema);
                                tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                                tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                    (speCemmRate+reProcFeeRate)); //手续费摊回
                            }else{
                                double ChoiRebaFeeRate = tLRPolResultSet.get(1).
                                        getChoiRebaFeeRate(); //取新单计算时的选择折扣,不需要重新计算保单年度
                                tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                                 tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                    ChoiRebaFeeRate); //手续费摊回

                            }
                            prepareLRPolEdorResult(tLRPolEdorSchema,
                                                   tLRPolEdorResultSchema);
                            this.mMap.put(tLRPolEdorResultSchema,
                                          "DELETE&INSERT");
                            if (tLRPolEdorSchema.getFeeoperationType().equals(
                                    "XB")) { //对于续保回退的摊回,同时修改分保计算结果表的polno信息,将其置换edorno,即newpolno
                                String stru = " update lrpolresult set polno='" +
                                              tLRPolEdorSchema.getEdorNo() +
                                        "',modifydate=current date,modifytime=current time where " +
                                              " PolNo='" +
                                              tLRPolEdorSchema.getPolNo() +
                                              "' and ReContCode='" +
                                              tLRPolEdorSchema.getReContCode() +
                                              "' and ReinsureItem='" +
                                              tLRPolEdorSchema.getReinsureItem() +
                                              "' and ReNewCount= " +
                                              tLRPolEdorSchema.getReNewCount();
                                this.mMap.put(stru, "UPDATE");
                            }
                        } else {
                            this.errorLog(tLRPolEdorSchema, "没有该保单的分保计算结果");
                        }
                    }
                   }
               if (!prepareOutputData()) {
                   return false;
               }
               tPubSubmit = new PubSubmit();
               if (!tPubSubmit.submitData(mInputData, "")) {
                   if (tPubSubmit.mErrors.needDealError()) {
                       buildError("submitData", "保存再保计算结果出错!");
                   }
                   return false;
               }
           } while (tLRPolEdorSet.size() > 0);
           rswrapper.close();
       } catch (Exception ex) {
           ex.printStackTrace();
           rswrapper.close();
       }
       return true;
   }


    private boolean prepareLRPolEdorResult(LRPolEdorSchema aLRPolEdorSchema,
                                           LRPolEdorResultSchema
                                           aLRPolEdorResultSchema) {
        aLRPolEdorResultSchema.setPolNo(aLRPolEdorSchema.getPolNo());
        aLRPolEdorResultSchema.setEdorNo(aLRPolEdorSchema.getEdorNo());
        aLRPolEdorResultSchema.setReContCode(aLRPolEdorSchema.getReContCode());

        String strSQL = "select Recomcode from LRContInfo where RecontCode = '" +
                        aLRPolEdorSchema.getReContCode() + "'";
        aLRPolEdorResultSchema.setReComCode(new ExeSQL().getOneValue(strSQL));
        aLRPolEdorResultSchema.setReinsureItem(aLRPolEdorSchema.getReinsureItem());
        aLRPolEdorResultSchema.setRiskCalSort(aLRPolEdorSchema.getRiskCalSort());
        aLRPolEdorResultSchema.setRiskCode(aLRPolEdorSchema.getRiskCode());
        aLRPolEdorResultSchema.setRiskVersion(aLRPolEdorSchema.getRiskVersion());
        aLRPolEdorResultSchema.setCessStartDate(this.mStartDate);
        aLRPolEdorResultSchema.setCessEndDate(this.mEndDate);
        aLRPolEdorResultSchema.setManageCom(aLRPolEdorSchema.getManageCom());

        aLRPolEdorResultSchema.setOperator(globalInput.Operator);
        aLRPolEdorResultSchema.setMakeDate(mPubFun.getCurrentDate());
        aLRPolEdorResultSchema.setMakeTime(mPubFun.getCurrentTime());
        aLRPolEdorResultSchema.setModifyDate(mPubFun.getCurrentDate());
        aLRPolEdorResultSchema.setModifyTime(mPubFun.getCurrentTime());
        aLRPolEdorResultSchema.setReNewCount(aLRPolEdorSchema.getReNewCount());
        return true;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolEdorSchema aLRPolEdorSchema, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new
                                             LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolEdorSchema.getPolNo());
        tLRErrorLogSchema.setLogType("6"); //6:保全计算
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /**
     * 得到成数分保比例
     * @param LRPolEdorSchema aLRPolEdorSchema
     * @return String
     */
    private double getCessionRate(LRPolEdorSchema aLRPolEdorSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolEdorSchema.getReContCode() +
                "' and upper(factorcode)='CESSIONRATE' and riskcode='" +
                aLRPolEdorSchema.getRiskCode() + "'";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到特殊佣金比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getSpeCemmRate(LRPolEdorSchema aLRPolEdorSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolEdorSchema.getReContCode() +
                "' and upper(factorcode)='SPECOMRATE' and riskcode='" +
                aLRPolEdorSchema.getRiskCode() + "'";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到再保手续费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getReProcFeeRate(LRPolEdorSchema aLRPolEdorSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolEdorSchema.getReContCode() +
                "' and upper(factorcode)='REINPROCFEERATE' and riskcode='" +
                aLRPolEdorSchema.getRiskCode() + "'";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到选择折扣比例,只针对首年度保单
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getChoiRebaFeeRate(LRPolEdorSchema aLRPolEdorSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolEdorSchema.getReContCode() +
                "' and upper(factorcode)='CHOIREBARATE' and riskcode='" +
                aLRPolEdorSchema.getRiskCode() + "'";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
}
