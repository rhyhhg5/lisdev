package com.sinosoft.lis.reinsure;
import java.text.DecimalFormat;
import java.util.Date;

import com.sinosoft.lis.bl.LCPremBL;
import com.sinosoft.lis.bq.EdorCalZT;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LREdorMainDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.db.LRRiskDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LREdorMainSchema;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.schema.LRRiskSchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LREdorMainSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.lis.vschema.LRRiskSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 分保系统计算类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @version 1.0
 */
public class CalRetentBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private String mReinsureCom=""; //再保险公司
  private String mItemFlag="";  //再保险项目：法定或商业
  private String mToday="";      //计算时间
  private LRPolSet mLRPolSet = new LRPolSet();
  private LRPolSet mRetLRPolSet = new LRPolSet();

  private LRPolSchema mLRPolSchema = new LRPolSchema();
  //个人承保信息
  private LCPolSchema mLCPolSchema = new LCPolSchema();

  //再保险险种定义
  private LRRiskSchema mLRRiskSchema = new LRRiskSchema();
  private double mCessionRate=0; //分保比例
  private double mRiskAmnt=0;   //保单风险保额

  public CalRetentBL()
  {
  }

  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.setOperate(cOperate);
    System.out.println("------Operate:"+this.getOperate());

    //得到外部传入的数据,将数据备份到本类中
    getInputData(cInputData)  ;
    System.out.println("after GetInputData.....");

    //从处理日期取得理论计算日期
    setCalDate();
    this.mLRRiskSchema=new LRRiskSchema();
    this.mRetLRPolSet = new LRPolSet();
    LRRiskDB tLRRiskDB = new LRRiskDB();
    tLRRiskDB.setRiskCode(this.mLCPolSchema.getRiskCode());
    tLRRiskDB.setReinsurItem(this.mItemFlag);
    tLRRiskDB.setReinsureCom(this.mReinsureCom);
    LRRiskSet tLRRiskSet = new LRRiskSet();
    tLRRiskSet=tLRRiskDB.query();
    for (int RiskCount=1;RiskCount<=tLRRiskSet.size();RiskCount++)
    {
      this.mLRPolSchema = new LRPolSchema();
      this.mLRRiskSchema=new LRRiskSchema();
      mLRRiskSchema.setSchema(tLRRiskSet.get(RiskCount));
      //数据复杂业务处理(dealData())
      if (!getCessRateAndRiskAmount())
        continue;
      System.out.println("after dealData......");
      //数据准备操作（preparedata())
      this.prepareData();
    }
    this.mResult.addElement(this.mRetLRPolSet);
    return true;
  }
  private void prepareData()
  {
    this.mLRPolSchema.setReinsurItem(this.mItemFlag);
    this.mLRPolSchema.setReinsureCom(this.mReinsureCom);
    this.mLRPolSchema.setRiskCalSort(this.mLRRiskSchema.getRiskCalSort());
    this.mLRPolSchema.setInsuredYear(PubFun.calInterval(this.mLCPolSchema.getCValiDate(),this.mToday,"Y")+1);
    this.mLRPolSchema.setCessionRate(this.mCessionRate);
    this.mLRPolSchema.setCessionAmount(this.mCessionAmount);
    this.mLRPolSchema.setCessPrem(this.getCessionPrem(mLRPolSchema.getCessPremRate()));
    this.mLRPolSchema.setCessPremRate(this.getCessionPremRate());
    this.mLRPolSchema.setCessComm(this.getCommsion(this.mLRPolSchema.getCessPrem(),this.mLRRiskSchema.getCommsionRate()));
    this.mLRPolSchema.setCessCommRate(this.getCommsionRate());

    this.mLRPolSchema.setPolStat("0");
    this.mLRPolSchema.setCessStart(this.mToday);
    this.mLRPolSchema.setCessEnd(PubFun.calDate(this.mToday,1,"Y",""));
    this.mLRPolSchema.setMakeDate(PubFun.getCurrentDate());
    this.mLRPolSchema.setMakeTime(PubFun.getCurrentTime());
    this.mLRPolSchema.setModifyDate(PubFun.getCurrentDate());
    this.mLRPolSchema.setModifyTime(PubFun.getCurrentTime());
    double tAddPrem=getPolAddPrem();
    double tAddRate=tAddPrem/this.mLCPolSchema.getStandPrem();
    double tAddCessPrem=this.mLRPolSchema.getCessPrem()*tAddRate;
    this.mLRPolSchema.setExCessPremRate(tAddRate);
    this.mLRPolSchema.setExCessPrem(tAddCessPrem);
    this.mLRPolSchema.setExPrem(tAddPrem);
    this.mLRPolSchema.setExcessCommRate(this.mLRPolSchema.getCessCommRate());
    double tAddComm=this.mLRPolSchema.getExcessCommRate()*this.mLRPolSchema.getExCessPrem();
    this.mLRPolSchema.setExCessComm(tAddComm);
    this.mLRPolSchema.setSignDate(this.mLCPolSchema.getSignDate());


    LRPolSchema tLRPolSchema = new LRPolSchema();
    tLRPolSchema.setSchema(this.mLRPolSchema);
    this.mRetLRPolSet.add(tLRPolSchema);
  }

  public VData getResult()
  {
    return mResult;
  }

  //保单该年度的分保保额
  private double mCessionAmount=0;


  private boolean getCessRateAndRiskAmount()
  {

      this.mRiskAmnt=setRiskAmnt(this.mLRRiskSchema,this.mLCPolSchema);
      Reflections tReflections = new Reflections();
      tReflections.transFields(mLRPolSchema,mLCPolSchema);

      double CVT=EdorCalZT.getCashValue(this.mLCPolSchema.getPolNo(),this.mToday); //保单t年度的现金价值
      this.mLRPolSchema.setNowRiskAmount(mRiskAmnt-CVT);
      double CVT1=0;
      //保单生效日到当前计算日的年度
      int tYear=PubFun.calInterval(this.mLCPolSchema.getCValiDate(),this.mToday,"Y");
      //计算tYear保单年度对应日
      String tCvalidate=PubFun.calDate(this.mLCPolSchema.getCValiDate(),tYear,"Y","");
      //计算日时在该保单年度已经经过的月数
      int tPassMonth=PubFun.calInterval(tCvalidate,this.mToday,"M");
      //插值法计算计算日时的现金价值
      if (tPassMonth>0)
      {
        String tNextPeriDate=PubFun.calDate(tCvalidate,1,"Y","");
        CVT1=EdorCalZT.getCashValue(this.mLCPolSchema.getPolNo(),tNextPeriDate);
        CVT=CVT+(CVT1-CVT)*tPassMonth/12;
      }

      this.mLRPolSchema.setEnterCA(CVT);

      if (mLRRiskSchema.getReinsurItem().equals("L")) //法定分保
      {
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("cessionrate");
        tLDCodeDB.setCode(this.mLCPolSchema.getCValiDate().substring(0,4));
        if (!tLDCodeDB.getInfo())
        {
          CError tError = new CError();
          tError.moduleName = "CalRetentBL";
          tError.functionName = "getCessRateAndRiskAmount";
          tError.errorMessage = "缺少法定分保分保比例描述!";
          return false;
        }
        this.mCessionRate=Double.parseDouble(tLDCodeDB.getCodeName());
        this.mCessionAmount=this.mRiskAmnt*this.mCessionRate;
        this.mLRPolSchema.setProtItem("T");
        return true;
      }
      if (mLRRiskSchema.getReinsurItem().equals("C")) //商业分保
      {
        //个人寿险 个人重大疾病，采用溢额算法，首先判断是否是首年分保，如果是，计算首年分保金额和分保比例；
        //如果不是首年分保，计算该年度的风险保额，取出首年的分保比例，计算该年度的分保金额
        if (mLRRiskSchema.getCessionMode().equals("S"))
        {
          LRPolDB tLRPolDB = new LRPolDB();
          tLRPolDB.setPolNo(this.mLCPolSchema.getPolNo());
          tLRPolDB.setReinsureCom(this.mReinsureCom);
          tLRPolDB.setReinsurItem(mLRRiskSchema.getReinsurItem());
          tLRPolDB.setInsuredYear(1);
          if (tLRPolDB.getInfo()) //不是首年分保，取出分保比例
          {
            if (tLRPolDB.getCValiDate().equals(this.mLCPolSchema.getCValiDate()))
            {
              this.mCessionRate= tLRPolDB.getCessionRate(); //分保比例
              double DEATHAMNT=this.mRiskAmnt; //死亡保额
              double tRSAT= DEATHAMNT-CVT; //第t保单年度的风险保额
              this.mCessionAmount=tRSAT*mCessionRate; //第t保单年度的分保风险保额
              this.mLRPolSchema.setSumRiskAmount(tLRPolDB.getSumRiskAmount());
              return true;
            }
          }
//        //首年分保
          double RSA0=this.mRiskAmnt; //本保单首年风险保额，个人寿险与个人重大疾病无法定分保，不用考虑法定分保部分
          double tSum[]={0,0};
          tSum=getSumRetain();
          this.mLRPolSchema.setSumRiskAmount(tSum[0]);
          double SUMALLRET=tSum[1]; //现有有效保单累计自留
          double RETAINAMNT=this.mLRRiskSchema.getRetainLine();
          double RESA0= RSA0+SUMALLRET-RETAINAMNT; //首年分保金额
          if (RESA0<=0)
            return false;
          //首年分保金额如果大于自动接收限额，则作临时分保
          if (this.mLRRiskSchema.getCessionLimit()>0 && RESA0>this.mLRRiskSchema.getCessionLimit())
          {
            this.mLRPolSchema.setProtItem("F");
            this.mCessionAmount=0;
            this.mCessionRate=0;
          }
          else
          {
            this.mLRPolSchema.setProtItem("T");
            this.mCessionAmount=RESA0;
            //this.mCessionRate=RESA0/RSA0;
            this.mCessionRate=RESA0/this.mRiskAmnt;
          }
          return true;
        }
        //意外险，采用溢额＋成数分保方式
        if (mLRRiskSchema.getCessionMode().equals("M"))
        {
          double tLegalAmt=getThisLegalCessAmt(); //法定分保保额
          double tRemainAmt=this.mRiskAmnt-tLegalAmt; //剩余风险保额
          double tSum[]={0,0};
          tSum=getSumRetain();
          this.mLRPolSchema.setSumRiskAmount(tSum[0]);
          double SUMALLRET=tSum[1]; //现有有效保单累计自留
          double tRetainLine=this.mLRRiskSchema.getRetainLine()-SUMALLRET; //本次分保的自留限额
          double tRetentAmtCal=getThisRetain(tRemainAmt); //计算自留额
          //计算自留额大于自留额上限，作溢额处理
          if (tRetentAmtCal>tRetainLine)
          {
            double RSA0=tRemainAmt; //本保单首年风险保额，扣除法定分保
            double RETAINAMNT=tRetainLine;
            double RESA0= RSA0-RETAINAMNT; //首年分保金额

            if (RESA0<=0)
            {
              RESA0=0;
              return false;
            }
            if (RESA0>this.mLRRiskSchema.getCessionLimit()) //临时分保处理
            {
              this.mLRPolSchema.setProtItem("F");
              this.mCessionRate=0;
              this.mCessionAmount=0;
            }
            else
            {
              this.mLRPolSchema.setProtItem("T");
              this.mCessionRate=RESA0/mRiskAmnt;
              this.mCessionAmount=RESA0;
            }
            return true;
          }
          else //成数分保
          {
            double tRamainAmt=tRemainAmt*this.mLRRiskSchema.getRetentRate(); //自留额
            double tCessAmt =tRemainAmt- tRamainAmt;
            if (tCessAmt<=0)
            {
              tCessAmt=0;
              return false;
            }
            //意外伤害保险成数分保分出额<自动接收限额
            //this.mCessionRate=tCessAmt/tRemainAmt;
            this.mCessionRate=tCessAmt/mRiskAmnt;
            this.mCessionAmount=tCessAmt;
            this.mLRPolSchema.setProtItem("T");
            return true;
          }
        }
        //个人短期健康险、团体险、附加险
        if (mLRRiskSchema.getCessionMode().equals("Q"))
        {
          double tLegalAmt=getThisLegalCessAmt(); //法定分保保额
          double tRemainAmt=mRiskAmnt-tLegalAmt;
          double tRamainAmt=tRemainAmt*this.mLRRiskSchema.getRetentRate(); //自留额
          double tCessAmt =tRemainAmt- tRamainAmt;
          if (tCessAmt<=0)
          {
            tCessAmt=0;
            return false;
          }
          //处理接收限额
          if (tCessAmt>this.mLRRiskSchema.getCessionLimit()) //临时分保处理
          {
            this.mLRPolSchema.setProtItem("F");
            this.mCessionRate=0;
            this.mCessionAmount=0;
          }
          else
          {
            this.mLRPolSchema.setProtItem("T");
            this.mCessionRate=tCessAmt/mRiskAmnt;
            this.mCessionAmount=tCessAmt;
          }
          return true;
        }
      }

    return false;
  }

  //取得本张保单法定分保分出的保额
  private double getThisLegalCessAmt()
  {
    LRPolDB tLRPolDB = new LRPolDB();
    //法定分保只可能出现一次
    tLRPolDB.setPolNo(this.mLCPolSchema.getPolNo());
    tLRPolDB.setReinsureCom(this.mReinsureCom);
    tLRPolDB.setReinsurItem("L");
    LRPolSet tLRPolSet = new LRPolSet();
    tLRPolSet=tLRPolDB.query();
    double tLegalCessAmnt=0;
    for (int i=1;i<=tLRPolSet.size();i++)
    {
      LRPolSchema tLRPolSchema = new LRPolSchema();
      tLRPolSchema=tLRPolSet.get(i);
      tLegalCessAmnt=tLegalCessAmnt+tLRPolSchema.getCessionAmount();
    }
    //计算保全导致该保单发生的分出保额变化
    LREdorMainDB tLREdorMainDB = new LREdorMainDB();
    tLREdorMainDB.setPolNo(this.mLCPolSchema.getPolNo());
    tLREdorMainDB.setReinsureCom(this.mReinsureCom);
    tLREdorMainDB.setReinsurItem("L");
    tLREdorMainDB.setInsuredYear(PubFun.calInterval(this.mLCPolSchema.getCValiDate(),this.mToday,"Y")+1);
    LREdorMainSet tLREdorMainSet = new LREdorMainSet();
    tLREdorMainSet=tLREdorMainDB.query();
    for (int i=1;i<=tLREdorMainSet.size();i++)
    {
      LREdorMainSchema tLREdorMainSchema = new LREdorMainSchema();
      tLREdorMainSchema=tLREdorMainSet.get(i);
      FDate tFDate = new FDate();
      Date tDate = tFDate.getDate(tLREdorMainSchema.getCessStart());
      Date tDate1 = tFDate.getDate(this.mToday);
      if (tDate1.before(tDate))
      {
        continue;
      }
      tLegalCessAmnt=tLegalCessAmnt-tLREdorMainSchema.getChgCessAmt();
    }
    return tLegalCessAmnt;
  }

  //取得该客户作为被保险人的所有保单的累计风险保额与累计自留保额
  //每张保单风险保额在再保中的定义、加入现金价值的计算要素
  private double[] getSumRetain()
  {
    //计算该被保险人在上个保单年度保全分保保额累计
    //计算所有生效日期小于当前保单的保单
    double tSum[]={0,0};

    String tSql="select * from lcpol ";
    String tBSql="select * from lbpol ";
    String tWhereSql=" where insuredno='"+this.mLCPolSchema.getInsuredNo()+"' and appflag='1' and polno<>'"+this.mLCPolSchema.getPolNo()+"' ";
    tSql=tSql+tWhereSql;
    tBSql=tBSql+tWhereSql;
    String tDateSql=" and (signdate<'"+this.mLCPolSchema.getSignDate()+"' or (signdate='"+this.mLCPolSchema.getSignDate()+"' and (makedate<'"+this.mLCPolSchema.getMakeDate()+"' or (makedate='"+this.mLCPolSchema.getMakeDate()+"' and maketime<'"+this.mLCPolSchema.getMakeTime()+"'))))" ;
    tSql = tSql+tDateSql;
    tBSql=tBSql+tDateSql;
    String tCalKind="'"+this.mLRRiskSchema.getRiskCalSort()+"'";
    String tRiskSql=" and riskcode in(select riskcode from lrrisk where riskcalsort in("+tCalKind+") and reinsuritem='"+this.mLRRiskSchema.getReinsurItem()+"') ";
    tSql = tSql + tRiskSql;
    tBSql=tBSql + tRiskSql;
    tBSql=tBSql+" and modifydate>'"+this.mToday+"'";

    System.out.println(tSql);
    LCPolDB tLCPolDB = new LCPolDB();
    LCPolSet tLCPolSet = new LCPolSet();
    tLCPolSet=tLCPolDB.executeQuery(tSql);

    System.out.println(tBSql);
    LBPolDB tLBPolDB = new LBPolDB();
    LBPolSet tLBPolSet = new LBPolSet();
    tLBPolSet=tLBPolDB.executeQuery(tBSql);
    Reflections tReflections = new Reflections();
    for (int i=1;i<=tLBPolSet.size();i++)
    {
      LBPolSchema tLBPolSchema = new LBPolSchema();
      LCPolSchema tLCPolSchema = new LCPolSchema();
      tLBPolSchema=tLBPolSet.get(i);
      tReflections.transFields(tLCPolSchema,tLBPolSchema);
      tLCPolSet.add(tLCPolSchema);
    }


    double tSumRetainAmount=0;
    double tSumRiskAmnt=0;
    for (int i=1;i<=tLCPolSet.size();i++)
    {
      LCPolSchema tLCPolSchema = new LCPolSchema();
      tLCPolSchema=tLCPolSet.get(i);

      LRPolDB tLRPolDB = new LRPolDB();
      tLRPolDB.setPolNo(tLCPolSchema.getPolNo());
      int tInsuredYear=PubFun.calInterval(tLCPolSchema.getCValiDate(),this.mToday,"Y")+1;
      tLRPolDB.setInsuredYear(tInsuredYear);
      LRPolSet tLRPolSet = new LRPolSet();
      tLRPolSet=tLRPolDB.query();
      double tSumCess=0;

      //查询本次提数处理中的保单
      for (int m=1;m<=this.mLRPolSet.size();m++)
      {
        LRPolSchema tLRPolSchema = new LRPolSchema();
        tLRPolSchema = mLRPolSet.get(m);
        if (tLRPolSchema.getPolNo().equals(tLCPolSchema.getPolNo()) && tLRPolSchema.getInsuredYear()==tInsuredYear)
        {
          tLRPolSet.add(tLRPolSchema);
        }
      }

      for (int j=1;j<=tLRPolSet.size();j++)
      {
        LRPolSchema tLRPolSchema = new LRPolSchema();
        tLRPolSchema=tLRPolSet.get(j);
        if (tLRPolSchema.getReinsurItem().equals("C") && !tLRPolSchema.getRiskCalSort().equals(this.mLRRiskSchema.getRiskCalSort()))
        {
          continue;
        }
        tSumCess=tSumCess+tLRPolSchema.getCessionAmount();
      }
      System.out.println("tSumCess:"+tSumCess);
      //get the risk amount of the policy
      double tRiskAmnt=0;
      LRRiskDB tLRRiskDB = new LRRiskDB();
      tLRRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
      LRRiskSet tLRRiskSet = new LRRiskSet();
      tLRRiskSet=tLRRiskDB.query();
      if (tLRRiskSet.size()>0)
      {
        LRRiskSchema tLRRiskSchema = new LRRiskSchema();
        tLRRiskSchema=tLRRiskSet.get(1);
        tRiskAmnt=setRiskAmnt(tLRRiskSchema,tLCPolSchema);
      }
      else
      {
        tRiskAmnt=tLCPolSchema.getRiskAmnt();
      }
      //end get
      double tPolRetainAmt=tRiskAmnt-tSumCess; //how to decide the risk amount of the dealing policy?
      tSumRiskAmnt=tSumRiskAmnt+tRiskAmnt;
      System.out.println("RiskAmnt:"+tRiskAmnt);

      System.out.println("tPolRetainAmt:"+tPolRetainAmt);
      //查询保全导致分保保额变化
      LREdorMainDB tLREdorMainDB = new LREdorMainDB();
      int tYear=PubFun.calInterval(tLCPolSchema.getCValiDate(),this.mToday,"Y");
      String tSql1 ="select * from LREdorMain where polno='"+tLCPolSchema.getPolNo()+"' and InsuredYear="+tYear+" and Cessstart<='"+this.mToday+"' and Cessend>='"+this.mToday+"' " ;
      LREdorMainSet tLREdorMainSet = new LREdorMainSet();
      tLREdorMainSet=tLREdorMainDB.executeQuery(tSql1);
      for (int k=1;k<=tLREdorMainSet.size();k++)
      {
        LREdorMainSchema tLREdorMainSchema = new LREdorMainSchema();
        tLREdorMainSchema=tLREdorMainSet.get(k);
        tPolRetainAmt=tPolRetainAmt+tLREdorMainSchema.getChgCessAmt();
      }
      tSumRetainAmount=tSumRetainAmount+tPolRetainAmt;

    }
    tSum[0]=tSumRiskAmnt;     //累计风险保额
    tSum[1]=tSumRetainAmount; //累计自留保额
    return tSum;
  }

  //取得本次保单分保的计算自留额
  private double getThisRetain(double aRemainAmnt)
  {
    double tRetainCal = 0;
    tRetainCal=aRemainAmnt*this.mLRRiskSchema.getRetentRate();
    return tRetainCal;
  }

  //取得分保保额
  private double getCessionAmount()
  {
    double tCessionAmount= this.mCessionAmount;
    tCessionAmount = Double.parseDouble(new DecimalFormat("0.00").format(tCessionAmount));
    return tCessionAmount;
  }
  //取得分保保费
  private double getCessionPrem(double aCessionPremRate)
  {
    double tCessionPrem=0;
    if (this.mLRRiskSchema.getCessPremCalCode()!=null && !this.mLRRiskSchema.getCessPremCalCode().equals(""))
    {
      Calculator mCalculator = new Calculator();
      mCalculator.setCalCode( this.mLRRiskSchema.getCessPremCalCode() );
      //增加基本要素
      //保费
      mCalculator.addBasicFactor("StandPrem", String.valueOf(mLCPolSchema.getStandPrem()));
      //分保比例
      mCalculator.addBasicFactor("CessionRate",String.valueOf(mCessionRate));
      //分保保额
      mCalculator.addBasicFactor("CessionAmnt",String.valueOf(mCessionAmount));
      //性别
      mCalculator.addBasicFactor("Sex",String.valueOf(this.mLCPolSchema.getInsuredSex()));
      //被保人当前年龄
      int tYear=PubFun.calInterval(this.mLCPolSchema.getCValiDate(),this.mToday,"Y")+this.mLCPolSchema.getInsuredAppAge();
      mCalculator.addBasicFactor("InsuredAge",String.valueOf(tYear));
      //保单号
      mCalculator.addBasicFactor("PolNo",String.valueOf(this.mLCPolSchema.getPolNo()));

      String tStr = "";
      tStr = mCalculator.calculate() ;
      if (tStr==null || tStr.trim().equals(""))
        tCessionPrem = 0;
      else
        tCessionPrem = Double.parseDouble( tStr );
    }
    else
    {
      tCessionPrem=0;
    }

    //tCessionPrem = Double.parseDouble(new DecimalFormat("0.00").format(tCessionPrem));
    return tCessionPrem;
  }
  //取得分保的佣金
  private double getCommsion(double aPremium,double aCommRate)
  {
    double tCommsion=0;
    if (this.mLRRiskSchema.getCessCommCalCode2()!=null && !this.mLRRiskSchema.getCessCommCalCode2().equals(""))
    {
      Calculator mCalculator = new Calculator();
      mCalculator.setCalCode( this.mLRRiskSchema.getCessCommCalCode2() );
      //增加基本要素
      //保费
      mCalculator.addBasicFactor("CessionPrem", String.valueOf(aPremium));
      //分保手续费比例
      mCalculator.addBasicFactor("CommRate",String.valueOf(aCommRate));
      //保单年度
      int tYear=PubFun.calInterval(this.mLCPolSchema.getCValiDate(),this.mToday,"Y");
      mCalculator.addBasicFactor("InusredYear",String.valueOf(tYear));
      //保单号
      mCalculator.addBasicFactor("PolNo",String.valueOf(this.mLCPolSchema.getPolNo()));
      //标准保费
      mCalculator.addBasicFactor("StandPrem",String.valueOf(this.mLCPolSchema.getStandPrem()));

      String tStr = "";
      tStr = mCalculator.calculate() ;
      if (tStr==null || tStr.trim().equals(""))
        tCommsion = 0;
      else
        tCommsion = Double.parseDouble( tStr );
    }
    else
    {
      tCommsion=0;
    }

    tCommsion = Double.parseDouble(new DecimalFormat("0.00").format(tCommsion));
    return tCommsion;
  }

  private double getCommsionRate()
  {
    if (this.mLRRiskSchema.getCommsionRate()>0) //针对佣金比率恒定
    {
      return this.mLRRiskSchema.getCommsionRate();
    }
    else
    {
      if (this.mLRPolSchema.getCessPrem()==0)
      {
        return 0;
      }
      double trate=0;
      trate=this.mLRPolSchema.getCessComm()/this.mLRPolSchema.getCessPrem();
      return trate;
    }
  }

  //再保险公司费率
  private double getCessPrRateRein(int aInsureYear)
  {
    double tRate=0;
//    ReinsureRate001DB tReinsureRate001DB = new ReinsureRate001DB();
//    tReinsureRate001DB.setSex(this.mLCPolSchema.getInsuredSex());
//    tReinsureRate001DB.setAge(this.mLCPolSchema.getInsuredAppAge()+aInsureYear);
//    if (!tReinsureRate001DB.getinfo())
//      return 0;
//    double tRate = Double.parseDouble(tReinsureRate001DB.getRate());

    return tRate;
  }

  //保单承保费率
  private double getCessPrRateInit(int aInsureYear)
  {
//    LCPolBL tLCPolBL = new LCPolBL();
//      LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
//      LCGetBLSet tLCGetBLSet = new LCGetBLSet();
//
//      tLCPolBL.setSchema(this.mLCPolSchema);
//      LCDutyBL tLCDutyBL = new LCDutyBL();
//      tLCDutyBL.setPolNo(this.mLCPolSchema.getPolNo());
//      tLCDutyBLSet.set(tLCDutyBL.query());
//
//      LCGetBL tLCGetBL = new LCGetBL();
//      tLCGetBL.setPolNo(this.mLCPolSchema.getPolNo());
//      tLCGetBLSet.set(tLCGetBL.query());
//
//      //以第t个保单年度时被保险人年龄和年交保费计算新的费率标准
//      tLCPolBL.setInsuredAppAge(tLCPolBL.getInsuredAppAge()+aInsureYear);
//      tLCPolBL.setPayIntv(12);
//      for (int i=1;i<=tLCDutyBLSet.size();i++)
//      {
//        LCDutySchema tLCDutySchema=new LCDutySchema();
//        tLCDutySchema=tLCDutyBLSet.get(i);
//        tLCDutySchema.setPayIntv(12);
//      }
//      CalBL tCalBL1 = new CalBL(tLCPolBL,tLCDutyBLSet,tLCGetBLSet,"");
//      if(!tCalBL1.calPol())
//      {
//        System.out.println("=================tCalBL failure");
//      }
//      System.out.println("年交保费为："+tCalBL1.getLCPol().getStandPrem());
//      System.out.println("保额为："+tCalBL1.getLCPol().getRiskAmnt());
//      double tAnnulPrem=tCalBL1.getLCPol().getStandPrem();
//      double tRiskAmount=tCalBL1.getLCPol().getRiskAmnt();
//      if (tRiskAmount==0)
//      {
//        return 0;
//      }
//      double tPremRate=tAnnulPrem/tRiskAmount;
//      System.out.println("费率为："+tPremRate);
//      return tPremRate;
    return 0;
  }


  //获得第t个保单年度对应的费率
  private double getCessionPremRate()
  {
//    if (this.mLRRiskSchema.getCessPremMode().equals("2"))
//    {
//      if (this.mCessionMode.equals("Q"))
//      {
//        double tRate=getCessPrRateInit(aInsureYear);
//        return tRate;
//      }
//      else
//      {
//        double tRate=getCessPrRateRein(aInsureYear);
//        return tRate;
//      }
//    }
//
//    if (this.mLRRiskSchema.getCessPremMode().equals("1")) //再保险公司提供费率
//    {
//      double tRate=getCessPrRateRein(aInsureYear);
//      return tRate;
//    }
//    if (this.mLRRiskSchema.getCessPremMode().equals("0")) //原始年交保费费率
//    {
//      double tRate=0;
//      tRate=getCessPrRateInit(aInsureYear);
//      return tRate;
//    }
    double trate=0;
    if (this.mLRPolSchema.getCessionAmount()==0)
    {
      trate=0;
    }
    else
    {
      trate=this.mLRPolSchema.getCessPrem()/this.mLRPolSchema.getCessionAmount();
    }
    return trate;
  }


  private double getPolAddPrem()
  {
    LCPremDB tLCPremDB = new LCPremDB();
    LCPremBL tLCPremBL = new LCPremBL();
    tLCPremBL.setPolNo(this.mLCPolSchema.getPolNo());
    tLCPremBL.setPayPlanType("1");
    tLCPremBL.setModifyDate("");
    tLCPremBL.setModifyTime("");
    LCPremSet tLCPremSet = new LCPremSet();
    tLCPremSet.set(tLCPremBL.query());
    double tAddHePrem=0;
    for (int i=1;i<=tLCPremSet.size();i++)
    {
      LCPremSchema tLCPremSchema = new LCPremSchema();
      tLCPremSchema=tLCPremSet.get(i);
      tAddHePrem=tAddHePrem+tLCPremSchema.getPrem();
    }
    return tAddHePrem;
  }
  public void setOperate(String cOperate)
  {
    this.mOperate=cOperate;
  }
  public  String getOperate()
  {
    return this.mOperate;
  }
  private void getInputData(VData cInputData)
  {
    mLCPolSchema =(LCPolSchema)cInputData.getObjectByObjectName("LCPolSchema",0);
    this.mItemFlag=(String)cInputData.get(1);
    mReinsureCom=(String)cInputData.get(2);
    mToday = (String)cInputData.get(3);
    this.mLRPolSet=(LRPolSet)cInputData.get(4);
  }
  private void setCalDate()
  {
    int tyear=PubFun.calInterval(this.mLCPolSchema.getSignDate(),this.mToday,"Y");
    String tnewDate=PubFun.calDate(this.mLCPolSchema.getCValiDate(),tyear,"Y","");
    this.mToday=tnewDate;
  }
  private double setRiskAmnt(LRRiskSchema tLRRiskSchema, LCPolSchema tLCPolSchema)
  {
    double tRiskAmnt=0;
    if (tLRRiskSchema.getRiskAmntCalCode()!=null && !tLRRiskSchema.getRiskAmntCalCode().equals(""))
    {
      Calculator mCalculator = new Calculator();
      mCalculator.setCalCode( tLRRiskSchema.getRiskAmntCalCode() );
      //增加基本要素
      mCalculator.addBasicFactor("RiskAmnt", String.valueOf(tLCPolSchema.getRiskAmnt()));
      mCalculator.addBasicFactor("Amnt",String.valueOf(tLCPolSchema.getAmnt()));
      mCalculator.addBasicFactor("PolNo",String.valueOf(tLCPolSchema.getPolNo()));
      //被保人当前年龄
      int tYear=PubFun.calInterval(tLCPolSchema.getCValiDate(),this.mToday,"Y")+tLCPolSchema.getInsuredAppAge();
      mCalculator.addBasicFactor("InsuredAge",String.valueOf(tYear));
      //交费满期
      mCalculator.addBasicFactor("PayEndDate",String.valueOf(tLCPolSchema.getPayEndDate()));
      //计算日期
      mCalculator.addBasicFactor("CalDate",String.valueOf(this.mToday));
      String tStr = "";

      tStr = mCalculator.calculate() ;
      if (tStr==null || tStr.trim().equals(""))
        tRiskAmnt = 0;
      else
        tRiskAmnt = Double.parseDouble( tStr );
    }
    else
    {
      tRiskAmnt=0;
    }
    return tRiskAmnt;
  }

     public static void main(String[] args)
   {
       String tSql="";
       String tItemFlag="C";

//       LBPolDB tLBPolDB = new LBPolDB();
//       tLBPolDB.setPolNo("86110020030210017347");
//       String tToday="2003-11-24";
//       tLBPolDB.getInfo();
//       LBPolSchema tLBPolSchema = new LBPolSchema();
//       tLBPolSchema.setSchema(tLBPolDB.getSchema());
//       LCPolSchema tLCPolSchema = new LCPolSchema();
//       Reflections tReflections = new Reflections();
//       tReflections.transFields(tLCPolSchema,tLBPolSchema);


       LCPolSchema tLCPolSchema = new LCPolSchema();
       LCPolDB tLCPolDB = new LCPolDB();
       tLCPolDB.setPolNo("86110020030210015665");
       String tToday="2003-10-31";
       tLCPolDB.getInfo();
       tLCPolSchema.setSchema(tLCPolDB.getSchema());
       //判断该保单是否失效
       if (EdorCalZT.JudgeLapse(tLCPolSchema.getRiskCode(),tLCPolSchema.getPaytoDate(),tToday,String.valueOf(tLCPolSchema.getPayIntv()),tLCPolSchema.getEndDate()))
       {
         System.exit(0);
       }

       LRPolSet tLRPolSet = new LRPolSet();
       VData tVData = new VData();
       tVData.addElement(tLCPolSchema);
       tVData.addElement(tItemFlag);
       tVData.addElement("1001");
       tVData.addElement(tToday);
       tVData.addElement(tLRPolSet);

       CalRetentBL tCalRetentBL = new CalRetentBL();
       tCalRetentBL.submitData(tVData,"Cal");

   }
}