/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

/*
 * <p>ClassName: LRCalCessDataBL </p>
 * <p>Description: LRCalCessDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Huxl
 * @CreateDate：2007-03-20
 */
public class LRCalCessDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    private double mAccQuot; //接受份额
    private double mManageFeeRate; //分保管理费费率
    //用于毛保费成数分保
    private double mCessionRate; //分保比例
    private double mReProcFeeRate; //再保手续费费率
    private double mSpeCemmRate; //特殊佣金费率
    //用于溢额分保
    private double mCessionAmount; //分保保额
    private double mCessPremRate; //分保费率
//    private double mSumAmnt; //累计(风险)保额
//    private double mSumCessAmnt; //累计分出保额
    private double mRetainAmount; //自留额
    private double mChoiRebaFeeRate; //折扣比例

    //业务处理相关变量
    CalRiskAmntBL tCalRiskAmntBL = new CalRiskAmntBL();
    /** 全局数据 */

    public LRCalCessDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRCalCessDataBL tLRCalCessDataBL = new LRCalCessDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2009-1-1");
            tTransferData.setNameAndValue("EndDate", "2009-3-31");
            vData.add(tTransferData);
            tLRCalCessDataBL.submitData(vData, "CESSDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRCalCessDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取分保数据
        if (this.strOperate.equals("CESSDATA")) {
            if (!calculateCessData()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 计算分保数据
     * @return boolean
     */
    private boolean calculateCessData() {
        System.out.println("Come to calCessData()...............");
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");
        this.mMap = new MMap();

        if (!calCessData1()) {
            buildError("calCessData1", "再保计算成数分保失败!");
        }
        if (!calCessData2()) {
            buildError("calCessData2", "再保计算溢额分保失败!");
        }
//        if (!calCessData3()) {
//            buildError("calCessData3", "再保计算临时分保失败!");
//        }
        return true;
    }

    /**
     * 处理医疗险成数分保
     * @return boolean
     */
    private boolean calCessData1() {
        //先处理提数区间内承保的医疗险,根据提数规则,可能出现财务已经缴费但是尚未提取到LRPol中的情况
        //因此先对处于提数期间的LRPol表中的医疗险,LRPolDetail表中截止日期前的缴费进行计算
        String strSQL =
                "select * from LRPol a where a.GetDataDate between '" +
                mStartDate + "' and '" + mEndDate +
                "' and a.TempCessFlag = 'N' " +
                " and exists (select 1 from LRContInfo where RecontCode = a.RecontCode and CessionMode = '1') with ur ";
        // //用来处理尚未计算的医疗保单
        LRPolSet tLRPolSet = new LRPolSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolSet, strSQL);

        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolSet.size(); i++) {
                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                    this.mCessionRate = getCessionRate(tLRPolSchema); //成数分保比例
                    this.mAccQuot = getAccQuot(tLRPolSchema); //接受份额
                    this.mReProcFeeRate = getReProcFeeRate(tLRPolSchema); //再保手续费比例

                    //09/04/30 5503的手续费费率按保费的折扣比例不同而不同
                    if(tLRPolSchema.getRiskCode().equals("5503")&& tLRPolSchema.getStandPrem()>0)
                    {
                       double FloatRateFee = tLRPolSchema.getPrem()/tLRPolSchema.getStandPrem();
                       if(FloatRateFee>=0.2 && FloatRateFee <1){
                           this.mReProcFeeRate = 0.21+FloatRateFee*0.5;
                       }else if (FloatRateFee >=1){
                           this.mReProcFeeRate = 0.71;
                       }
                    }
                    this.mSpeCemmRate = this.getSpeCemmRate(tLRPolSchema); //特殊佣金比例
                    this.mManageFeeRate = this.getManageFeeRate(tLRPolSchema); //管理费比例
                    strSQL = "select * from LRPolDetail where PolNo = '" +
                             tLRPolSchema.getPolNo() + "' and RenewCount =" +
                             tLRPolSchema.getReNewCount() +
                             " and GetDataDate <= '" + mEndDate + "' with ur";
                    LRPolDetailSet tLRPolDetailSet = new LRPolDetailDB().
                            executeQuery(strSQL);
                    for (int j = 1; j <= tLRPolDetailSet.size(); j++) {
                        LRPolDetailSchema tLRPolDetailSchema = tLRPolDetailSet.
                                get(j);
                        LRPolResultSchema tLRPolResultSchema = new
                                LRPolResultSchema();
                        this.prepareLRPolResult(tLRPolSchema,
                                                tLRPolDetailSchema,
                                                tLRPolResultSchema);
                        this.mMap.put(tLRPolResultSchema.getSchema(),
                                      "DELETE&INSERT");
                    }
                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
        //这里原先实收实付制时处理本期缴费的原医疗险再保保单，权责发生制的情况下，续期数据不再提取，这里只会计算07年前分保的保单的此后第一次缴费记录。
        //按照权责发生制标准算费即可。
//        strSQL =
//                "select * from LRPol a where (select CessionMode from LRContInfo where RecontCode = a.RecontCode) = '1' and a.GetDataDate < '"
//                + mStartDate + "' and a.TempCessFlag = 'N' and exists (select 1 from LRPolDetail where polno = a.polno and RenewCount = a.RenewCount and GetDataDate between '" +
//                mStartDate + "' and '" + mEndDate + "') with ur";
//        rswrapper = new RSWrapper();
//        rswrapper.prepareData(tLRPolSet, strSQL);
//        try {
//            do {
//                rswrapper.getData();
//                mMap = new MMap();
//                for (int i = 1; i <= tLRPolSet.size(); i++) {
//                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
//                    this.mCessionRate = getCessionRate(tLRPolSchema); //成数分保比例
//                    this.mAccQuot = getAccQuot(tLRPolSchema); //接受份额
//                    this.mReProcFeeRate = getReProcFeeRate(tLRPolSchema); //再保手续费比例
//                    this.mSpeCemmRate = getSpeCemmRate(tLRPolSchema); //特殊佣金比例
//                    this.mManageFeeRate = this.getManageFeeRate(tLRPolSchema); //管理费比例
//                    strSQL = "select * from LRPolDetail where PolNo = '" +
//                             tLRPolSchema.getPolNo() +
//                             "' and GetDataDate between '" +
//                             mStartDate + "' and '" + mEndDate + "' with ur";
//                    LRPolDetailSet tLRPolDetailSet = new LRPolDetailDB().
//                            executeQuery(strSQL);
//                    for (int j = 1; j <= tLRPolDetailSet.size(); j++) {
//                        LRPolDetailSchema tLRPolDetailSchema = tLRPolDetailSet.
//                                get(j);
//                        LRPolResultSchema tLRPolResultSchema = new
//                                LRPolResultSchema();
//                        this.prepareLRPolResult(tLRPolSchema,
//                                                tLRPolDetailSchema,
//                                                tLRPolResultSchema);
//                        this.mMap.put(tLRPolResultSchema.getSchema(),
//                                      "DELETE&INSERT");
//                    }
//                }
//                if (!prepareOutputData()) {
//                    return false;
//                }
//                tPubSubmit = new PubSubmit();
//                if (!tPubSubmit.submitData(mInputData, "")) {
//                    if (tPubSubmit.mErrors.needDealError()) {
//                        buildError("submitData", "保存再保计算结果出错!");
//                    }
//                    return false;
//                }
//            } while (tLRPolSet.size() > 0);
//            rswrapper.close();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            rswrapper.close();
//        }
        return true;
    }


    /**
     * 处理溢额分保
     * @return boolean
     */
    private boolean calCessData2() {
        String strSQL =
                "select * from LRPol a where (select CessionMode from LRContInfo where RecontCode = a.RecontCode) = '2' and a.GetDataDate between '" +
                mStartDate + "' and '" + mEndDate +
                "' and a.TempCessFlag = 'N' " +
                " with ur";
        LRPolSet tLRPolSet = new LRPolSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolSet, strSQL);

        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolSet.size(); i++) {
                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                    this.mAccQuot = getAccQuot(tLRPolSchema); //接受份额
                    this.mChoiRebaFeeRate = this.getChoiRebaFeeRate(
                            tLRPolSchema); //选择折扣比例
                    this.mManageFeeRate = this.getManageFeeRate(tLRPolSchema); //管理费比例
                    if(!tLRPolSchema.getRiskCode().equals("530301")){  //530301暂时不用取费率表计算08/08/06
                        String CessPremModeCode = tCalRiskAmntBL.getCalCode(
                                tLRPolSchema, "CessPremMode");
                        if (CessPremModeCode == null ||
                            CessPremModeCode.equals("")) {
                            this.errorLog(tLRPolSchema, "获取再保分保费率算法失败");
                            continue;
                        }
                        this.mCessPremRate = tCalRiskAmntBL.calCessFeeRate(
                                CessPremModeCode, tLRPolSchema);
                        if (CessPremModeCode == null ||
                            Math.abs(mCessPremRate + 1) < 0.00001) {
                            this.errorLog(tLRPolSchema, "计算再保分保费率失败");
                            continue;
                        }
                    }
                    strSQL = "select CessAmnt from LCPol where PolNo = '" +
                             tLRPolSchema.getPolNo() +
                             "' union select CessAmnt from LBPol where PolNo = '" +
                             tLRPolSchema.getPolNo() + "'";
                    String CessAmnt = new ExeSQL().getOneValue(strSQL);
                    if (CessAmnt.equals("")) {
                        this.errorLog(tLRPolSchema, "获取分保保额失败");
                        continue;
                    }
                    this.mCessionAmount = Double.parseDouble(CessAmnt);
                    LRPolResultSchema tLRPolResultSchema = new
                            LRPolResultSchema();
                    prepareLRPolResult(tLRPolSchema, tLRPolResultSchema);
                    this.mMap.put(tLRPolResultSchema, "DELETE&INSERT");
                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }

        return true;
    }

//    private boolean calCessData3() {
//        String strSQL =
//                "select * from LRPol a where  a.GetDataDate between '" +
//                mStartDate + "' and '" + mEndDate +
//                "' and a.TempCessFlag = 'Y' with ur";
//        LRPolSet tLRPolSet = new LRPolSet();
//        RSWrapper rswrapper = new RSWrapper();
//        rswrapper.prepareData(tLRPolSet, strSQL);
//
//        try {
//            do {
//                rswrapper.getData();
//                mMap = new MMap();
//                for (int i = 1; i <= tLRPolSet.size(); i++) {
//                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
//                    LRTempCessContInfoDB tLRTempCessContInfoDB = new
//                            LRTempCessContInfoDB();
//                    tLRTempCessContInfoDB.setReContCode(tLRPolSchema.
//                            getReContCode());
//                    tLRTempCessContInfoDB.setProposalNo(tLRPolSchema.
//                            getProposalNo());
//                    if (!tLRTempCessContInfoDB.getInfo()) {
//                        this.errorLog(tLRPolSchema, "没有找到该保单的临时分保协议");
//                        continue;
//                    }
//                    LRTempCessContInfoSchema tLRTempCessContInfoSchema =
//                            tLRTempCessContInfoDB.getSchema();
//                    LRPolResultSchema tLRPolResultSchema = new
//                            LRPolResultSchema();
//                    if (tLRTempCessContInfoSchema.getCalMode().equals("S")) {
//                        prepareLRPolResult(tLRPolSchema, tLRPolResultSchema,
//                                           tLRTempCessContInfoSchema);
//                    } else {
//                        if (Math.abs(tLRTempCessContInfoSchema.getCessionRate() -
//                                     0.00) <= 0.0001) {
//                            this.mAccQuot = 1;
//                            this.mChoiRebaFeeRate = tLRTempCessContInfoSchema.
//                                    getReinProcFeeRate();
//                            this.mManageFeeRate = 0;
//                            String CessPremModeCode = tCalRiskAmntBL.getCalCode(
//                                    tLRTempCessContInfoSchema, tLRPolSchema);
//                            if (CessPremModeCode == null ||
//                                CessPremModeCode.equals("")) {
//                                this.errorLog(tLRPolSchema, "获取再保分保费率算法失败");
//                                continue;
//                            }
//                            this.mCessPremRate = tCalRiskAmntBL.calCessFeeRate(
//                                    CessPremModeCode, tLRPolSchema,
//                                    tLRTempCessContInfoSchema.getDiseaseAddFee(),
//                                    tLRTempCessContInfoSchema.getDeadAddFee());
//                            if (Math.abs(mCessPremRate + 1) <
//                                0.00001) {
//                                this.errorLog(tLRPolSchema, "计算再保分保费率失败");
//                                continue;
//                            }
//                            this.mCessionAmount = tLRPolSchema.getRiskAmnt() -
//                                                  tLRTempCessContInfoSchema.
//                                                  getRetainAmnt();
//                            prepareLRPolResult(tLRPolSchema, tLRPolResultSchema);
//                        }
//                    }
//                    this.mMap.put(tLRPolResultSchema, "DELETE&INSERT");
//                }
//                if (!prepareOutputData()) {
//                    return false;
//                }
//                tPubSubmit = new PubSubmit();
//                if (!tPubSubmit.submitData(mInputData, "")) {
//                    if (tPubSubmit.mErrors.needDealError()) {
//                        buildError("submitData", "保存再保计算结果出错!");
//                    }
//                    return false;
//                }
//            } while (tLRPolSet.size() > 0);
//            rswrapper.close();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            rswrapper.close();
//        }
//        return true;
//   }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolSchema aLRPolSchema, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolSchema.getPolNo());
        tLRErrorLogSchema.setLogType("5"); //5:新单计算
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "INSERT");

        return true;
    }

    /**
     *
     * @param aLRPolSchema LRPolSchema
     * @param aLRPolDetailSchema LRPolDetailSchema
     * @param aLRPolResultSchema LRPolResultSchema
     * @return boolean
     */
    private boolean prepareLRPolResult(LRPolSchema aLRPolSchema,
                                       LRPolDetailSchema aLRPolDetailSchema,
                                       LRPolResultSchema aLRPolResultSchema) {
        aLRPolResultSchema.setPolNo(aLRPolSchema.getPolNo());
        aLRPolResultSchema.setReContCode(aLRPolSchema.getReContCode());
        aLRPolResultSchema.setReinsureItem(aLRPolSchema.getReinsureItem());
        aLRPolResultSchema.setTempCessFlag(aLRPolSchema.getTempCessFlag());
        aLRPolResultSchema.setReNewCount(aLRPolSchema.getReNewCount());
        aLRPolResultSchema.setPayCount(aLRPolDetailSchema.getPayCount());
        aLRPolResultSchema.setRiskCalSort(aLRPolSchema.getRiskCalSort());
        aLRPolResultSchema.setRiskCode(aLRPolSchema.getRiskCode());
        aLRPolResultSchema.setRiskVersion(aLRPolSchema.getRiskVersion());
        aLRPolResultSchema.setCessStartDate(this.mStartDate);
        aLRPolResultSchema.setCessEndDate(this.mEndDate);

        aLRPolResultSchema.setCurrentPrem(aLRPolDetailSchema.getPayMoney()); //当期保费
        int tPayIntv = aLRPolSchema.getPayIntv();
        if (tPayIntv == 1 || tPayIntv == 3 || tPayIntv == 6) {
            int tRemainMonths = PubFun.calInterval(aLRPolDetailSchema.
                    getCurPayToDate(), aLRPolSchema.getEndDate(), "M") /
                                tPayIntv;
            aLRPolResultSchema.setShouldCessPrem(aLRPolDetailSchema.getPayMoney() +
                                                 aLRPolSchema.getPrem() *
                                                 tRemainMonths);
            aLRPolResultSchema.setShouldCount(tRemainMonths + 1);
            aLRPolResultSchema.setRemainCount(tRemainMonths);
        } else {
            aLRPolResultSchema.setShouldCessPrem(aLRPolDetailSchema.getPayMoney());
            aLRPolResultSchema.setShouldCount(1);
            aLRPolResultSchema.setRemainCount(0);
        }
        aLRPolResultSchema.setCessionRate(this.mCessionRate); //分保比例
        double cessPrem = aLRPolResultSchema.getShouldCessPrem() *
                          mCessionRate * mAccQuot;
        aLRPolResultSchema.setCessPrem(cessPrem); //分保保费

        aLRPolResultSchema.setSpeCemmRate(this.mSpeCemmRate); //特殊佣金比例
        aLRPolResultSchema.setReProcFeeRate(this.mReProcFeeRate); //再保手续费比例
        aLRPolResultSchema.setReProcFee(cessPrem *
                                        (mSpeCemmRate + mReProcFeeRate)); //再保手续费
        aLRPolResultSchema.setManageFeeRate(this.mManageFeeRate); //再保管理费比例
        aLRPolResultSchema.setManageFee(cessPrem * this.mManageFeeRate); //再保管理费

        aLRPolResultSchema.setManageCom(aLRPolSchema.getManageCom());
        aLRPolResultSchema.setOperator(globalInput.Operator);
        aLRPolResultSchema.setMakeDate(mPubFun.getCurrentDate());
        aLRPolResultSchema.setMakeTime(mPubFun.getCurrentTime());
        aLRPolResultSchema.setModifyDate(mPubFun.getCurrentDate());
        aLRPolResultSchema.setModifyTime(mPubFun.getCurrentTime());
        return true;
    }

//    private boolean prepareLRPolResult(LRPolSchema aLRPolSchema,
//                                       LRPolResultSchema aLRPolResultSchema,
//                                       LRTempCessContInfoSchema
//                                       aLRTempCessContInfoSchema) {
//        aLRPolResultSchema.setPolNo(aLRPolSchema.getPolNo());
//        aLRPolResultSchema.setReContCode(aLRPolSchema.getReContCode());
//        aLRPolResultSchema.setReinsureItem(aLRPolSchema.getReinsureItem());
//        aLRPolResultSchema.setTempCessFlag(aLRPolSchema.getTempCessFlag());
//        aLRPolResultSchema.setReNewCount(aLRPolSchema.getReNewCount());
//        aLRPolResultSchema.setPayCount(0); //溢额分保不考虑期交保费,此字段默认为0
//        aLRPolResultSchema.setRiskCalSort(aLRPolSchema.getRiskCalSort());
//        aLRPolResultSchema.setRiskCode(aLRPolSchema.getRiskCode());
//        aLRPolResultSchema.setRiskVersion(aLRPolSchema.getRiskVersion());
//        aLRPolResultSchema.setCessStartDate(this.mStartDate);
//        aLRPolResultSchema.setCessEndDate(this.mEndDate);
//
//        aLRPolResultSchema.setCessionRate(aLRTempCessContInfoSchema.
//                                          getCessionAmount() /
//                                          aLRPolSchema.getAmnt()); //分保比例
//        aLRPolResultSchema.setCessionAmount(aLRTempCessContInfoSchema.
//                                            getCessionAmount()); //分保保额
//        aLRPolResultSchema.setCessPremRate(""); //分保费率
//        aLRPolResultSchema.setCessPrem(aLRTempCessContInfoSchema.getCessionFee()); //分保保费
//        aLRPolResultSchema.setChoiRebaFeeRate(aLRTempCessContInfoSchema.
//                                              getReinProcFeeRate()); //选择折扣比例
//        aLRPolResultSchema.setReProcFee(aLRTempCessContInfoSchema.getCessionFee() *
//                                        aLRTempCessContInfoSchema.
//                                        getReinProcFeeRate()); //再保手续费=分保保费*选择折扣比例
//        aLRPolResultSchema.setManageFeeRate(0); //再保管理费比例
//        aLRPolResultSchema.setManageFee(0); //再保管理费
//
//        aLRPolResultSchema.setManageCom(aLRPolSchema.getManageCom());
//        aLRPolResultSchema.setOperator(globalInput.Operator);
//        aLRPolResultSchema.setMakeDate(mPubFun.getCurrentDate());
//        aLRPolResultSchema.setMakeTime(mPubFun.getCurrentTime());
//        aLRPolResultSchema.setModifyDate(mPubFun.getCurrentDate());
//        aLRPolResultSchema.setModifyTime(mPubFun.getCurrentTime());
//        return true;
//    }

    /**
     * 溢额分保计算结果数据准备。
    */
    private boolean prepareLRPolResult(LRPolSchema aLRPolSchema,
                                       LRPolResultSchema
                                       aLRPolResultSchema) {
        aLRPolResultSchema.setPolNo(aLRPolSchema.getPolNo());
        aLRPolResultSchema.setReContCode(aLRPolSchema.getReContCode());
        aLRPolResultSchema.setReinsureItem(aLRPolSchema.getReinsureItem());
        aLRPolResultSchema.setTempCessFlag(aLRPolSchema.getTempCessFlag());
        aLRPolResultSchema.setReNewCount(aLRPolSchema.getReNewCount());
        aLRPolResultSchema.setPayCount(0); //溢额分保不考虑期交保费,此字段默认为0
        aLRPolResultSchema.setRiskCalSort(aLRPolSchema.getRiskCalSort());
        aLRPolResultSchema.setRiskCode(aLRPolSchema.getRiskCode());
        aLRPolResultSchema.setRiskVersion(aLRPolSchema.getRiskVersion());
        aLRPolResultSchema.setCessStartDate(this.mStartDate);
        aLRPolResultSchema.setCessEndDate(this.mEndDate);
        if(aLRPolSchema.getRiskCode().equals("530301")){
            this.mRetainAmount = getRemainAmnt(aLRPolSchema);  //自留额
            aLRPolResultSchema.setCessionRate((aLRPolSchema.getAmnt()*2-mRetainAmount) /
                                          (aLRPolSchema.getAmnt()*2));
        }else{
            aLRPolResultSchema.setCessionRate(this.mCessionAmount /
                                              aLRPolSchema.getAmnt()); //分保比例
        }
        aLRPolResultSchema.setCessionAmount(this.mCessionAmount); //分保保额
        if(aLRPolSchema.getRiskCode().equals("530301")){
            this.mCessPremRate=0.08;
        }else{
            aLRPolResultSchema.setCessPremRate(this.mCessPremRate); //分保费率
        }
        String strSQL =
                "select RiskPeriod from LMRiskApp where riskcode = '" +
                aLRPolSchema.getRiskCode() + "'";
        String tRiskPeriod = new ExeSQL().getOneValue(strSQL);
        double cessPrem = 0;
        //如果是短期险
        if (tRiskPeriod.equals("L")) {
            cessPrem = mCessionAmount / 1000 * mCessPremRate * mAccQuot;
        } else {
           int days = mPubFun.calInterval(aLRPolSchema.getCValiDate(),
                                          aLRPolSchema.getEndDate(), "D");
           cessPrem = mCessionAmount / 1000 * mCessPremRate *
                      mAccQuot * days / 365;
       }
       aLRPolResultSchema.setCessPrem(cessPrem); //分保保费

       String validate = aLRPolSchema.getCValiDate();
       int currYear = PubFun.calInterval(validate,
                                         aLRPolSchema.getGetDataDate(),
                                         "Y") + 1; //计算保单年度,第一年的保单年度为1
       if (currYear > 1) {
           mChoiRebaFeeRate = 0; //如果非首次保单年度,则没有折扣
       }
       aLRPolResultSchema.setChoiRebaFeeRate(this.mChoiRebaFeeRate); //选择折扣比例
       aLRPolResultSchema.setReProcFee(cessPrem *
                                       this.mChoiRebaFeeRate); //再保手续费=分保保费*选择折扣比例
       aLRPolResultSchema.setManageFeeRate(this.mManageFeeRate); //再保管理费比例
       aLRPolResultSchema.setManageFee(cessPrem * this.mManageFeeRate); //再保管理费

       aLRPolResultSchema.setManageCom(aLRPolSchema.getManageCom());
       aLRPolResultSchema.setOperator(globalInput.Operator);
       aLRPolResultSchema.setMakeDate(mPubFun.getCurrentDate());
       aLRPolResultSchema.setMakeTime(mPubFun.getCurrentTime());
       aLRPolResultSchema.setModifyDate(mPubFun.getCurrentDate());
       aLRPolResultSchema.setModifyTime(mPubFun.getCurrentTime());
       return true;
   }

    /**
     * 得到再保手续费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getReProcFeeRate(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='REINPROCFEERATE' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到特殊佣金比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getSpeCemmRate(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='SPECOMRATE' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到选择折扣比例,只针对首年度保单
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getChoiRebaFeeRate(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='CHOIREBARATE' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到管理费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getManageFeeRate(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='REINMANFEERATE' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到成数分保比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getCessionRate(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='CESSIONRATE' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到自留额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getRemainAmnt(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='RETAINAMNT' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 获取再保合同的接受份额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getAccQuot(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='ACCQUOT' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 获取再保合同的接受份额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getLowReAmnt(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='LOWREAMNT' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRCalCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
}
