/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;

import java.util.Date;

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: huxl
 * @CreateDate：2006-11-01
 */
public class LRNewGetEdorDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";

    /**前面台传过来的合同号*/
    private String mReContCode ="";
    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    private String mCurrentDate = new PubFun().getCurrentDate();

    private String mCurrentTime = new PubFun().getCurrentTime();
    private	String mRiskCode;
    private	String mGrpContNo;
    private	String mContNo;
    private	String mPolNo;
    private	String mOperator;
    
    private String mSQL ;

    //业务处理相关变量
    /** 全局数据 */

    public LRNewGetEdorDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL->Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }
           
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取保全数据
        if (!getEdorData()) {
            buildError("insertData", "单证管理员信息有误!");
            return false;
        }
        return true;
    }


    /**
     * 提取保全数据
     * @return boolean
     */
    private boolean insertEdorData() {
        System.out.println("come in insertEdorData()..");
        //提取再保保全数据
        String ttSql =
                "select a.GrpContNo A,"+
       "a.GrpPolNo B,"+
       "a.ContNo C,"+
       "a.PolNo D,"+
       "(select max(EdorNo)"+
       " from LPEdorItem"+
      " where EdorType = 'FX'"+
        " and ContNo = a.ContNo) E,"+
       "a.MainPolNo F,"+
       "a.MasterPolNo G,"+
       "a.RiskCode H,"+
       "a.RiskVersion I,"+
       "(select max(EdorValidate)"+
          " from LPEdorItem"+
         " where EdorType = 'FX'"+
           " and ContNo = a.ContNo) J,"+
       " a.PayToDate K,"+
       
       " finaInterTable.FeeType L,"+
       "sum(finaInterTable.prem) M,"+
       " '" + this.mToday + "',"+
       "a.Amnt O,"+
       "a.ManageCom P,"+
       "a.ReNewCount Q,"+
       "a.agentGroup R,"+
       "a.CValidate S,"+
       "a.enddate T"+
  " from lcpol a,"+
       " (select bb.FeeType,"+
               " bb.contno,"+
               " bb.polno,"+
               " sum(case aa.finitemtype"+
                     " when 'C' then"+
                      " aa.summoney"+
                     " else"+
                      " -aa.summoney"+
                   " end) as prem"+
          " from FiVoucherDataDetail aa, FiAbStandardData bb"+
         " where aa.accountcode = '6031000000'"+
           " and aa.accountdate = '"+this.mToday+"'"+
           " and aa.classtype in ('B-02','B-02-Y')"+
           " and aa.serialno = bb.serialno"+
           " and bb.FeeType ='FX' "+
         " group by bb.FeeType,bb.contno, bb.polno) as finaInterTable"+
 " where a.polno = finaInterTable.polno"+
   " and a.conttype = '1'"+
   " and exists (select 1 from LRPol where polno = a.polno and (recontcode ='"+mReContCode+"' or 1='"+mReContCode+"' ))  " +mSQL+ //复效提取
   " group by a.GrpContNo,"+
   " a.GrpPolNo,"+
   " a.ContNo,"+
   " a.PolNo,"+
   " a.MainPolNo,"+
   " a.MasterPolNo,"+
   " a.RiskCode,"+
   " a.RiskVersion,"+
   " a.PayToDate,"+
   " finaInterTable.FeeType,"+
   " a.Amnt,"+
   " a.ManageCom,"+
   " a.ReNewCount,"+
   " a.agentGroup,"+
   " a.CValidate,"+
   " a.enddate" + 
                " union all " +
                "select a.GrpContNo A,"+
       "a.GrpPolNo B,"+
       "a.ContNo C,"+
       "a.PolNo D,"+
       "a.EdorNo E,"+
       "a.MainPolNo F,"+
       "a.MasterPolNo G,"+
       "a.RiskCode H,"+
       "a.RiskVersion I,"+
       "(select max(EdorValidate)"+
          " from LPEdorItem"+
         " where EdorNo = a.edorno"+
           " and ContNo = a.ContNo) J,"+
       " a.PayToDate K,"+
       
       " finaInterTable.FeeType L,"+
       "sum(finaInterTable.prem) M,"+
       " '" + this.mToday + "',"+
       "a.Amnt O,"+
       "a.ManageCom P,"+
       "a.ReNewCount Q,"+
       "a.agentGroup R,"+
       "a.CValidate S,"+
       "a.enddate T"+
  " from lbpol a,"+
       " (select bb.FeeType,"+
               " bb.contno,"+
               " bb.polno,"+
               " sum(case aa.finitemtype"+
                     " when 'C' then"+
                      " aa.summoney"+
                     " else"+
                      " -aa.summoney"+
                   " end) as prem"+
          " from FiVoucherDataDetail aa, FiAbStandardData bb"+
         " where aa.accountcode = '6031000000'"+
           " and aa.accountdate = '"+this.mToday+"'"+
           " and aa.classtype in ('B-02','B-02-Y')"+
           " and aa.serialno = bb.serialno"+
           " and bb.FeeType in ('WT', 'XT', 'ZT', 'CT')"+
         " group by bb.FeeType,bb.contno, bb.polno) as finaInterTable"+
 " where a.polno = finaInterTable.polno"+
   " and a.conttype = '1'"+
   " and exists (select 1 from LRPol where polno = a.polno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"'))  " +mSQL+ //团单犹豫期退保
   " group by a.GrpContNo,"+
   " a.GrpPolNo,"+
   " a.ContNo,"+
   " a.PolNo,"+
   " a.EdorNo,"+
   " a.MainPolNo,"+
   " a.MasterPolNo,"+
   " a.RiskCode,"+
   " a.RiskVersion,"+
   " a.PayToDate,"+
   " finaInterTable.FeeType,"+
   " a.Amnt,"+
   " a.ManageCom,"+
   " a.ReNewCount,"+
   " a.agentGroup,"+
   " a.CValidate,"+
   " a.enddate" + 
                " union all " +
                "select a.GrpContNo A,"+
                "a.GrpPolNo B,"+
                "a.ContNo C,"+
                "a.PolNo D,"+
                "a.EdorNo E,"+
                "a.MainPolNo F,"+
                "a.MasterPolNo G,"+
                "a.RiskCode H,"+
                "a.RiskVersion I,"+
                "(select max(EdorValidate)"+
                   " from LPGrpEdorItem"+
                  " where EdorNo = a.edorno"+
                    " and GrpContNo = a.GrpContNo) J,"+
                " a.PayToDate K,"+
                
                " b.FeeOperationType L,"+
                "sum(b.getmoney) M,"+
                " '" + this.mToday + "',"+
                "a.Amnt O,"+
                "a.ManageCom P,"+
                "a.ReNewCount Q,"+
                "a.agentGroup R,"+
                "a.CValidate S,"+
                "a.enddate T"+
  " from lbpol a,"+
                " LJAGetEndorse b,"+
                " (select bb.indexno,"+
                " bb.grpcontno,"+
                " bb.grppolno,"+
                " sum(case aa.finitemtype"+
                " when 'C' then"+
                " aa.summoney"+
                " else"+
                " -aa.summoney"+
                " end) as prem"+
                " from FiVoucherDataDetail aa, FiAbStandardData bb"+
                " where aa.accountcode = '6031000000'"+
                " and aa.accountdate = '"+this.mToday+"'"+
                " and aa.classtype in ('B-03', 'B-04','B-03-Y', 'B-04-Y')"+
                " and aa.serialno = bb.serialno"+
                " and bb.FeeType in ('WT','XT','ZT','CT')"+
                " group by bb.indexno, bb.grpcontno, bb.grppolno) as finaInterTable"+
                " where a.grpcontno = finaInterTable.grpcontno"+
                " and a.grppolno = finaInterTable.grppolno"+
                " and a.conttype = '2'"+
                " and a.polno = b.polno "+
                " and a.edorno = b.EndorsementNo"+
                " and b.actugetno = finaInterTable.indexno"+
                " and b.FeeFinaType <> 'GB'"+
                " and exists (select 1 from LRPol where polno = a.polno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"')) "+
                " and not exists (select 1 from LRPol where polno = a.polno and TempCessFlag='Y') "+mSQL+
                " group by a.GrpContNo,"+
                " a.GrpPolNo,"+
                " a.ContNo,"+
                " a.PolNo,"+
                " a.EdorNo,"+
                " a.MainPolNo,"+
                " a.MasterPolNo,"+
                " a.RiskCode,"+
                " a.RiskVersion,"+
                " a.PayToDate,"+
                " b.FeeOperationType,"+
                " a.Amnt,"+
                " a.ManageCom,"+
                " a.ReNewCount,"+
                " a.agentGroup,"+
                " a.CValidate,"+
                " a.enddate" + //个单协议退保
                " union all " +
                "select a.GrpContNo A,"+
                "a.GrpPolNo B,"+
                "a.ContNo C,"+
                "a.PolNo D,"+
                "b.EndorsementNo E,"+
                "a.MainPolNo F,"+
                "a.MasterPolNo G,"+
                "a.RiskCode H,"+
                "a.RiskVersion I,"+
                "(select max(EdorValidate)"+
                   " from LPGrpEdorItem"+
                  " where EdorNo = b.EndorsementNo"+
                    " and GrpContNo = a.GrpContNo) J,"+
                " a.PayToDate K,"+
                
                " b.FeeOperationType L,"+
                "sum(b.getmoney) M,"+
                " '" + this.mToday + "',"+
                "a.Amnt O,"+
                "a.ManageCom P,"+
                "a.ReNewCount Q,"+
                "a.agentGroup R,"+
                "a.CValidate S,"+
                "a.enddate T"+
  " from lcpol a,"+
                " LJAGetEndorse b,"+
                " (select bb.indexno,"+
                " bb.grpcontno,"+
                " bb.grppolno,"+
                " sum(case aa.finitemtype"+
                " when 'C' then"+
                " aa.summoney"+
                "    else"+
                "     -aa.summoney"+
                " end) as prem"+
                " from FiVoucherDataDetail aa, FiAbStandardData bb"+
                " where aa.accountcode = '6031000000'"+
                "  and aa.accountdate = '"+this.mToday+"'"+
                " and aa.serialno = bb.serialno"+
                "  and ("+
                " (aa.classtype in ('B-03', 'B-04','B-03-Y', 'B-04-Y') and bb.FeeType in ('WZ','WJ'))"+
                " )"+
                " group by bb.indexno, bb.grpcontno,bb.grppolno) as finaInterTable"+
                " where a.grpcontno = finaInterTable.grpcontno"+
                " and a.grppolno = finaInterTable.grppolno"+
                " and a.conttype = '2'"+
                " and a.polno = b.polno "+
                " and a.contno = b.contno"+
                " and b.actugetno = finaInterTable.indexno"+
                " and b.FeeFinaType <> 'GB'"+
                " and exists (select 1 from LRPol where polno = a.polno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"')) "+
                " and not exists (select 1 from LRPol where polno = a.polno and TempCessFlag='Y') "+mSQL+
                " group by a.GrpContNo,"+
                "  a.GrpPolNo,"+
                " a.ContNo,"+
                " a.PolNo,"+
                " b.EndorsementNo,"+
                " a.MainPolNo,"+
                " a.MasterPolNo,"+
                " a.RiskCode,"+
                " a.RiskVersion,"+
                " a.PayToDate,"+
                " b.FeeOperationType,"+
                " a.Amnt,"+
                " a.ManageCom,"+
                " a.ReNewCount,"+
                " a.agentGroup,"+
                " a.CValidate,"+
                " a.enddate" + //团单协议退保
                " union all " +
//2013-8-28 冲销年度化保费的情况统一用下面的SQL提取 ,这个SQL注释掉
//                "select a.GrpContNo A,"+
//                "a.GrpPolNo B,"+
//                "a.ContNo C,"+
//                "a.PolNo D,"+
//                "a.EdorNo E,"+
//                "a.MainPolNo F,"+
//                "a.MasterPolNo G,"+
//                "a.RiskCode H,"+
//                "a.RiskVersion I,"+
//                "(select max(EdorValidate)"+
//                   " from LPEdorItem"+
//                  " where EdorNo = a.edorno"+
//                    " and GrpContNo = a.GrpContNo) J,"+
//                " a.PayToDate K,"+
//                
//                " b.FeeOperationType L,"+
//                "sum(finaInterTable.prem) M,"+
//                " '" + this.mToday + "',"+
//                "a.Amnt O,"+
//                "a.ManageCom P,"+
//                "a.ReNewCount Q,"+
//                "a.agentGroup R,"+
//                "a.CValidate S,"+
//                "a.enddate T"+
//  " from lbpol a,"+
//                " LJAGetEndorse b,"+
//                " (select bb.indexno,"+
//                " bb.grpcontno,"+
//                " bb.grppolno,"+
//                " sum(case aa.finitemtype"+
//                " when 'C' then"+
//                " aa.summoney"+
//                " else"+
//                " -aa.summoney"+
//                " end) as prem,bb.indexno"+
//                " from FiVoucherDataDetail aa, FiAbStandardData bb"+
//                " where aa.accountcode = '6031000000' and aa.classid='Y-BQ-FC-000003' "+
//                " and aa.accountdate = '"+this.mToday+"'"+
//                " and aa.classtype in ('B-03', 'B-04','B-03-Y', 'B-04-Y')"+
//                " and aa.serialno = bb.serialno"+
//                " group by bb.indexno, bb.grpcontno, bb.grppolno,bb.indexno) as finaInterTable"+
//                " where a.grpcontno = finaInterTable.grpcontno"+
//                " and a.grppolno = finaInterTable.grppolno"+
//                " and b.feeoperationtype in ('WT','XT','ZT','CT') and b.getmoney=0 "+
//                " and finaInterTable.indexno is null and a.conttype = '2'"+
//                " and a.polno = b.polno and a.poltypeflag='1' "+
//                " and a.edorno = b.EndorsementNo"+
//                " and b.actugetno = finaInterTable.actugetno"+
//                " and b.FeeFinaType <> 'GB'"+
//                " and exists (select 1 from LRPol where polno = a.polno) "+
//                " and not exists (select 1 from LRPol where polno = a.polno and TempCessFlag='Y') "+
//                " group by a.GrpContNo,"+
//                " a.GrpPolNo,"+
//                " a.ContNo,"+
//                " a.PolNo,"+
//                " a.EdorNo,"+
//                " a.MainPolNo,"+
//                " a.MasterPolNo,"+
//                " a.RiskCode,"+
//                " a.RiskVersion,"+
//                " a.PayToDate,"+
//                " b.FeeOperationType,"+
//                " a.Amnt,"+
//                " a.ManageCom,"+
//                " a.ReNewCount,"+
//                " a.agentGroup,"+
//                " a.CValidate,"+
//                " a.enddate,finaInterTable.prem" + //个单协议退保
//                " union all " +
                "select a.GrpContNo A,"+
                "a.GrpPolNo B,"+
                "a.ContNo C,"+
                "a.PolNo D,"+
                "finaInterTable.indexno E,"+
                "a.MainPolNo F,"+
                "a.MasterPolNo G,"+
                "a.RiskCode H,"+
                "a.RiskVersion I,"+
                "(select max(EdorValidate)"+
                   " from LPGrpEdorItem"+
                  " where EdorNo = a.edorno"+
                    " and GrpContNo = a.GrpContNo) J,"+
                " a.PayToDate K,"+
                
                " b.FeeOperationType L,"+
                "sum(finaInterTable.prem) M,"+
                " '" + this.mToday + "',"+
                "a.Amnt O,"+
                "a.ManageCom P,"+
                "a.ReNewCount Q,"+
                "a.agentGroup R,"+
                "a.CValidate S,"+
                "a.enddate T"+
  " from lbpol a,"+
                " LJAGetEndorse b,"+
                " (select bb.indexno,"+
                " bb.grpcontno,"+
                " bb.grppolno,"+
                " sum(case aa.finitemtype"+
                " when 'C' then"+
                " aa.summoney"+
                " else"+
                " -aa.summoney"+
                " end) as prem "+
                " from FiVoucherDataDetail aa, FiAbStandardData bb"+
                " where aa.accountcode = '6031000000' and bb.BusTypeID in ('Y-BQ-FC-000003','Y-BQ-FC-000002') "+ //保全退保-约定缴费反冲	Y-BQ-FC-000002 保全退保冲销年度化	Y-BQ-FC-000003
                " and aa.accountdate = '"+this.mToday+"'"+
                " and aa.classtype in ('B-03', 'B-04','B-03-Y', 'B-04-Y')"+
                " and aa.serialno = bb.serialno"+
                " group by bb.indexno, bb.grpcontno, bb.grppolno) as finaInterTable"+
                " where a.grpcontno = finaInterTable.grpcontno"+
                " and a.grppolno = finaInterTable.grppolno"+
                " and b.feeoperationtype in ('WT','XT','ZT','CT')  "+
                " and a.conttype = '2'"+
                " and a.polno = b.polno and a.poltypeflag='1' "+
                " and a.edorno = b.EndorsementNo"+
                " and b.actugetno = finaInterTable.indexno"+
                " and b.FeeFinaType <> 'GB'"+
                " and exists (select 1 from LRPol where polno = a.polno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"')) "+
                " and not exists (select 1 from LRPol where polno = a.polno and TempCessFlag='Y') " +mSQL+
                " group by a.GrpContNo,"+
                " a.GrpPolNo,"+
                " a.ContNo,"+
                " a.PolNo,"+
                " finaInterTable.indexno,"+
                " a.MainPolNo,"+
                " a.MasterPolNo,"+
                " a.RiskCode,"+
                " a.RiskVersion,"+
                " a.PayToDate,"+
                " b.FeeOperationType,"+
                " a.Amnt,"+
                " a.ManageCom,"+
                " a.ReNewCount,"+
                " a.agentGroup,"+
                " a.CValidate,"+
                " a.enddate,finaInterTable.prem,a.EdorNo" + //个单协议退保
                /*
                 * 新增BB,BJ,ZF的处理逻辑
                 * 20130605
                 * */
				" union all " +
				"select a.GrpContNo A,"+
				"a.GrpPolNo B,"+
				"a.ContNo C,"+
				"a.PolNo D,"+
				"b.EndorsementNo E,"+
				"a.MainPolNo F,"+
				"a.MasterPolNo G,"+
				"a.RiskCode H,"+
				"a.RiskVersion I,"+
				"(select max(EdorValidate)"+
				   " from LPgrpEdorItem"+
				  " where EdorNo = b.EndorsementNo"+
				    " and GrpContNo = a.GrpContNo) J,"+
				" a.PayToDate K,"+
				" b.FeeOperationType L,"+
				"finaInterTable.prem M,"+
				" '" + this.mToday + "',"+
				"a.Amnt O,"+
				"a.ManageCom P,"+
				"a.ReNewCount Q,"+
				"a.agentGroup R,"+
				"a.CValidate S,"+
				"a.enddate T"+
				" from lcpol a,"+
				" LJAGetEndorse b,"+
				" (select bb.indexno,"+
				" bb.grpcontno,"+
				" bb.grppolno,"+
				" sum(case aa.finitemtype"+
				" when 'C' then"+
				" aa.summoney"+
				"    else"+
				"     -aa.summoney"+
				" end) as prem"+
				" from FiVoucherDataDetail aa, FiAbStandardData bb"+
				" where aa.accountcode = '6031000000'"+
				"  and aa.accountdate = '"+this.mToday+"'"+
				" and aa.serialno = bb.serialno"+
				"  and ("+
				" (aa.classtype in ('B-03', 'B-03-Y') and bb.FeeType in ('BB','BJ'))"+
				" )"+
				" group by bb.indexno, bb.grpcontno,bb.grppolno) as finaInterTable"+
				" where a.grpcontno = finaInterTable.grpcontno"+
				" and a.grppolno = finaInterTable.grppolno"+
				" and a.conttype = '2'"+
				" and b.actugetno = finaInterTable.indexno"+
				" and b.FeeFinaType <> 'GB'"+
				" and a.poltypeflag='1' "+
				" and exists (select 1 from LRPol where polno = a.polno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"')) "+
				" and not exists (select 1 from LRPol where polno = a.polno and TempCessFlag='Y') " +mSQL+
				" group by a.GrpContNo,"+
				"  a.GrpPolNo,"+
				" a.ContNo,"+
				" a.PolNo,"+
				" b.EndorsementNo,"+
				" a.MainPolNo,"+
				" a.MasterPolNo,"+
				" finaInterTable.prem,"+ 
				" a.RiskCode,"+
				" a.RiskVersion,"+
				" a.PayToDate,"+
				" b.FeeOperationType,"+
				" a.Amnt,"+
				" a.ManageCom,"+
				" a.ReNewCount,"+
				" a.agentGroup,"+
				" a.CValidate,"+
				" a.enddate" + 
				" union all " +
				"select a.GrpContNo A,"+
				"a.GrpPolNo B,"+
				"a.ContNo C,"+
				"a.PolNo D,"+
				"b.EndorsementNo E,"+
				"a.MainPolNo F,"+
				"a.MasterPolNo G,"+
				"a.RiskCode H,"+
				"a.RiskVersion I,"+
				"(select max(EdorValidate)"+
				   " from LPgrpEdorItem"+
				  " where EdorNo = b.EndorsementNo"+
				    " and GrpContNo = a.GrpContNo) J,"+
				" a.PayToDate K,"+
				" b.FeeOperationType L,"+
				"finaInterTable.prem M,"+
				" '" + this.mToday + "',"+
				"a.Amnt O,"+
				"a.ManageCom P,"+
				"a.ReNewCount Q,"+
				"a.agentGroup R,"+
				"a.CValidate S,"+
				"a.enddate T"+
				" from lcpol a,"+
				" LJAGetEndorse b,"+
				" (select bb.indexno,"+
				" bb.grpcontno,"+
				" bb.grppolno,"+
				" sum(case aa.finitemtype"+
				" when 'C' then"+
				" aa.summoney"+
				"    else"+
				"     -aa.summoney"+
				" end) as prem"+
				" from FiVoucherDataDetail aa, FiAbStandardData bb"+
				" where aa.accountcode = '6031000000'"+
				"  and aa.accountdate = '"+this.mToday+"'"+
				" and aa.serialno = bb.serialno"+
				"  and ("+
				" (aa.classtype in ('B-03', 'B-03-Y') and bb.FeeType in ('BB','BJ'))"+
				" )"+
				" group by bb.indexno, bb.grpcontno,bb.grppolno) as finaInterTable"+
				" where a.grpcontno = finaInterTable.grpcontno"+
				" and a.grppolno = finaInterTable.grppolno"+
				" and a.conttype = '2'"+
				" and b.actugetno = finaInterTable.indexno"+
				" and b.FeeFinaType <> 'GB'"+
				" and not exists (select 1 from lcpol where grpcontno=b.grpcontno and poltypeflag='1') "+
				" and a.polno = (select max(polno) from LRPol where grpcontno= a.grpcontno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"')) "+
				" and not exists (select 1 from LRPol where polno = a.polno and TempCessFlag='Y') " +mSQL+
				" group by a.GrpContNo,"+
				"  a.GrpPolNo,"+
				" a.ContNo,"+
				" a.PolNo,"+
				" b.EndorsementNo,"+
				" a.MainPolNo,"+
				" a.MasterPolNo,"+
				" finaInterTable.prem,"+ 
				" a.RiskCode,"+
				" a.RiskVersion,"+
				" a.PayToDate,"+
				" b.FeeOperationType,"+
				" a.Amnt,"+
				" a.ManageCom,"+
				" a.ReNewCount,"+
				" a.agentGroup,"+
				" a.CValidate,"+
				" a.enddate" + 
				" union all " +
				"select a.GrpContNo A,"+
				"a.GrpPolNo B,"+
				"a.ContNo C,"+
				"a.PolNo D,"+
				"b.edorno E,"+
				"a.MainPolNo F,"+
				"a.MasterPolNo G,"+
				"a.RiskCode H,"+
				"a.RiskVersion I,"+
				"(select max(EdorValidate)"+
				   " from LPgrpEdorItem"+
				  " where EdorNo = b.edorno"+
				    " and GrpContNo = a.GrpContNo) J,"+
				" a.PayToDate K,"+
				
				" b.edortype L,"+
				"sum(finaInterTable.prem) M,"+
				" '" + this.mToday + "',"+
				"a.Amnt O,"+
				"a.ManageCom P,"+
				"a.ReNewCount Q,"+
				"a.agentGroup R,"+
				"a.CValidate S,"+
				"a.enddate T"+
				" from lcpol a,"+
				" lpgrpedoritem b,"+
				" (select bb.indexno,"+
				" bb.grpcontno,"+
				" bb.grppolno,"+
				" sum(case aa.finitemtype"+
				" when 'C' then"+
				" aa.summoney"+
				"    else"+
				"     -aa.summoney"+
				" end) as prem"+
				" from FiVoucherDataDetail aa, FiAbStandardData bb"+
				" where aa.accountcode = '6031000000'"+
				"  and aa.accountdate = '"+this.mToday+"'"+
				" and aa.serialno = bb.serialno"+
				"  and ("+
				" (aa.classtype in ('B-03', 'B-03-Y') and bb.FeeType in ('ZF'))"+
				" )"+
				" group by bb.indexno, bb.grpcontno,bb.grppolno) as finaInterTable"+
				" where a.grpcontno = finaInterTable.grpcontno"+
				" and a.grppolno = finaInterTable.grppolno"+
				" and a.conttype = '2'"+
				" and a.grpcontno = b.grpcontno "+
				" and b.edorno = finaInterTable.indexno"+
				" and a.polno = (select max(polno) from LRPol where grpcontno= a.grpcontno) "+
				" and exists (select 1 from LRPol where polno = a.polno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"')) "+
				" and not exists (select 1 from LRPol where polno = a.polno and TempCessFlag='Y') "+mSQL+
				" group by a.GrpContNo,"+
				"  a.GrpPolNo,"+
				" a.ContNo,"+
				" a.PolNo,"+
				" b.edorno,"+
				" a.MainPolNo,"+
				" a.MasterPolNo,"+
				" a.RiskCode,"+
				" a.RiskVersion,"+
				" a.PayToDate,"+
				" b.edortype,"+
				" a.Amnt,"+
				" a.ManageCom,"+
				" a.ReNewCount,"+
				" a.agentGroup,"+
				" a.CValidate,"+
				" a.enddate" + 
                //20130605修改结束 
//                " union all " +
//                "select a.GrpContNo A,"+
//                "a.GrpPolNo B,"+
//                "a.GrpContNo C,"+
//                "a.GrpPolNo D,"+
//                "b.edorno E,"+
//                "a.GrpPolNo F,"+
//                "a.GrpPolNo G,"+
//                "a.RiskCode H,"+
//                "'2002' I,"+
//                "b.EdorValidate J,"+
//                " a.PayToDate K,"+
//                
//                " b.edortype L,"+
//                "finaInterTable.prem M,"+
//                " '" + this.mToday + "',"+
//                "a.Amnt O,"+
//                "a.ManageCom P,"+
//                "0 Q,"+
//                "a.agentGroup R,"+
//                "a.CValidate S,"+
//                "(select cinvalidate from lcgrpcont where grpcontno=a.grpcontno) T"+
//  " from lcgrppol a,"+
//                " lpgrpedoritem b,"+
//                " (select "+
//                " bb.grpcontno,"+
//                " bb.grppolno,"+
//                " sum(case aa.finitemtype"+
//                " when 'C' then"+
//                " aa.summoney"+
//                " else"+
//                " -aa.summoney"+
//                " end) as prem"+
//                " from FiVoucherDataDetail aa, FiAbStandardData bb"+
//                " where aa.accountcode = '6031000000'  "+
//                " and aa.accountdate = '"+this.mToday+"'"+
//                " and aa.classtype in ('B-03', 'B-04','B-03-Y', 'B-04-Y')"+
//                " and aa.serialno = bb.serialno"+
//                " group by  bb.grpcontno, bb.grppolno) as finaInterTable"+
//                " where a.grpcontno = finaInterTable.grpcontno"+
//                " and a.grppolno = finaInterTable.grppolno"+
//                " and b.edortype ='ZF'  "+
//                " and a.grpcontno = b.grpcontno  "+
//                " and exists (select 1 from LRPol where grpcontno = a.grpcontno and grppolno = a.grppolno) "+
//                " and not exists (select 1 from LRPol where grpcontno = a.grpcontno and grppolno = a.grppolno and TempCessFlag='Y') "+
//                " group by a.GrpContNo,"+
//                " a.GrpPolNo,"+
//                " b.edorno,"+
//                " a.RiskCode,"+
//                " a.RiskVersion,"+
//                " a.PayToDate,"+
//                " b.edortype,"+
//                " a.Amnt,"+
//                " a.ManageCom,"+
//                " a.agentGroup,"+
//                " a.CValidate,"+
//                " finaInterTable.prem,b.EdorValidate"+
                " with ur ";

        int start = 1;
        int nCount = 5000;
        while (true) {
            SSRS tSSRS = new ExeSQL().execSQL(ttSql, start, nCount);
            if (tSSRS.getMaxRow() <= 0) {
                break;
            }
            mMap = new MMap();
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                    LRPolEdorSchema tLRPolEdorSchema = new LRPolEdorSchema();
                    //添加其他数据
                    tLRPolEdorSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolEdorSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolEdorSchema.setContNo(tSSRS.GetText(i, 3));
                    tLRPolEdorSchema.setPolNo(tSSRS.GetText(i, 4));
                    tLRPolEdorSchema.setEdorNo(tSSRS.GetText(i, 5));
                    tLRPolEdorSchema.setMainPolNo(tSSRS.GetText(i, 6));
                    tLRPolEdorSchema.setMasterPolNo(tSSRS.GetText(i, 7));
                    tLRPolEdorSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolEdorSchema.setRiskVersion(tSSRS.GetText(i, 9));
                    tLRPolEdorSchema.setEdorValidate(tSSRS.GetText(i, 10));
                    int noPassDays ; //未经过天数
                    String mSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
                    String LRiskFlag = new ExeSQL().getOneValue(mSql);
                    if(Integer.parseInt(LRiskFlag)>0){//长险，未经过天数＝交至日期－保全生效日
                         noPassDays =PubFun.calInterval(tSSRS.GetText(i,10),tSSRS.GetText(i,11),"D")%365;
                    }else{//短险，未经过天数＝终止日期－保全生效日
                         noPassDays =PubFun.calInterval(tSSRS.GetText(i,10),tSSRS.GetText(i,20),"D");
                    }
                    if (noPassDays <= 0) {
                        noPassDays = 0; //保全摊回的时候，失效日期可以大于缴至日期,此时摊回零.
                    }

                    tLRPolEdorSchema.setNoPassDays(noPassDays);
                    tLRPolEdorSchema.setFeeoperationType(tSSRS.GetText(i, 12));
                    tLRPolEdorSchema.setGetMoney(tSSRS.GetText(i, 13));
                    tLRPolEdorSchema.setGetConfDate(tSSRS.GetText(i, 14));
                    tLRPolEdorSchema.setSumBackAmnt(tSSRS.GetText(i, 15));
                    tLRPolEdorSchema.setManageCom(tSSRS.GetText(i, 16));
                    tLRPolEdorSchema.setGetDataDate(this.mToday); //数据提取日期
                    tLRPolEdorSchema.setOperator(strOperate);
                    tLRPolEdorSchema.setMakeDate(this.mCurrentDate);
                    tLRPolEdorSchema.setMakeTime(this.mCurrentTime);
                    tLRPolEdorSchema.setModifyDate(this.mCurrentDate);
                    tLRPolEdorSchema.setModifyTime(this.mCurrentTime);
//                    String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
//                    String LRiskFlag = new ExeSQL().getOneValue(aSql);
                    String tcvdatey=tSSRS.GetText(i, 19).substring(0,4);
                	String ttodatey=this.mToday.substring(0,4);
                	int tyear=Integer.parseInt(ttodatey)-Integer.parseInt(tcvdatey)+1;
                    if(Integer.parseInt(LRiskFlag)>0){
                    	
                        tLRPolEdorSchema.setReNewCount(tyear); //保单年度
                    }else{
                        tLRPolEdorSchema.setReNewCount(tSSRS.GetText(i, 17)); //续保次数
                    }
                     //新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"+
                                 tSSRS.GetText(i,18)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolEdorSchema.setCostCenter(CostCenter);
                    //String PolYear = tSSRS.GetText(i,19);//保单年度,长险lrpol表的renewcount存的是保单年度,短险存续保数
                    String tSql =  "select a.ReContCode,'C',a.RiskCalSort, (case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                            "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode " +
                            "from LRPol a,LBPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount "+
                            "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod != 'L') and a.PolNo = '" +
                            tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount = b.ReNewCount) "+
                            " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                            " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                            " union all "+
                            "select a.ReContCode,'C',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                            "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode " +
                            "from LRPol a,LBPol b where a.PolNo = b.PolNO "+
                            " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.PolNo = '" +
                            tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO ) "+
                            //此处是为了兼容旧系统的分保，长险时lrpol表的renewcount存的是0
                            " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                            " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                            "union all "+
                            "select a.ReContCode,'C',a.RiskCalSort,(select ReComCode from LRContInfo where ReContCode = " +
                            "a.ReContCode) from LRPol a,LBPol b where a.PolNo = b.PolNO and a.ReNewCount =0  and a.getdatadate<'2008-10-1'"+   //上正式机时需要修改日期==========
                            " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.PolNo = '" +
                            tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount =0) "+
                            " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                            " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode with ur";
                    if(tSSRS.GetText(i, 12).equals("WZ")||tSSRS.GetText(i, 12).equals("WJ")||tSSRS.GetText(i, 12).equals("ZF")||tSSRS.GetText(i, 12).equals("BJ")||tSSRS.GetText(i, 12).equals("BB")||tSSRS.GetText(i, 12).equals("FX")){
                    	tSql =  "select a.ReContCode,'C',a.RiskCalSort, (case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                        "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode " +
                        "from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount "+
                        "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod != 'L') and a.PolNo = '" +
                        tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount = b.ReNewCount) "+
                        " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                        " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                        "union all "+
                        "select a.ReContCode,'C',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                        "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode " +
                        "from LRPol a,LCPol b where a.PolNo = b.PolNO "+
                        " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.PolNo = '" +
                        tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO ) "+
                        //此处是为了兼容旧系统的分保，长险时lrpol表的renewcount存的是0
                        " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                        " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                        "union all "+
                        "select a.ReContCode,'C',a.RiskCalSort,(select ReComCode from LRContInfo where ReContCode = " +
                        "a.ReContCode) from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount =0  and a.getdatadate<'2008-10-1'"+   //上正式机时需要修改日期==========
                        " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.PolNo = '" +
                        tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount =0) "+
                        " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                        " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                        " with ur";
                    }
//                    if(tSSRS.GetText(i, 12).equals("ZF")){
//                    	tSql =  "select a.ReContCode,'C',a.RiskCalSort, (case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
//                        "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode " +
//                        "from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount "+
//                        "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod != 'L') and a.GRPPOLNO = '" +
//                        tSSRS.GetText(i, 2) + "' and a.GrpContNo='" +
//                        tSSRS.GetText(i, 1) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount = b.ReNewCount) "+
//                        " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01' and cessionmode='1') " +
//                        " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
//                        "union all "+
//                        "select a.ReContCode,'C',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
//                        "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode " +
//                        "from LRPol a,LCPol b where a.PolNo = b.PolNO "+
//                        " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.GRPPOLNO = '" +
//                        tSSRS.GetText(i, 2) + "' and a.GrpContNo='" +
//                        tSSRS.GetText(i, 1) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO ) "+
//                        //此处是为了兼容旧系统的分保，长险时lrpol表的renewcount存的是0
//                        " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01' and cessionmode='1') " +
//                        " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
//                        "union all "+
//                        "select a.ReContCode,'C',a.RiskCalSort,(select ReComCode from LRContInfo where ReContCode = " +
//                        "a.ReContCode) from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount =0  and a.getdatadate<'2008-10-1'"+   //上正式机时需要修改日期==========
//                        " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.GRPPOLNO = '" +
//                        tSSRS.GetText(i, 2) + "' and a.GrpContNo='" +
//                        tSSRS.GetText(i, 1) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount =0) "+
//                        " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01' and cessionmode='1') " +
//                        
//                        " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
//                        " with ur";
//                    }
//                    System.out.println("querycontcode:"+tSql);
                    SSRS cSSRS = new ExeSQL().execSQL(tSql);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                    	String tRecontCode =cSSRS.GetText(j, 1);
                    	//20141120   杨阳
                    	//如果前台合同号不为空，需要匹配此合同才插入，否则跳过
                    	if(!"1".equals(mReContCode))
                    	{
                    		if(!tRecontCode.equals(mReContCode))
                    		{
                    			continue;
                    		}
                    	}
                        tLRPolEdorSchema.setReContCode(tRecontCode);
                        if(cSSRS.GetText(j, 2).equals("F")){  //当取出来的是反冲数据时，跳过不提取。
                            continue;
                        }
                        tLRPolEdorSchema.setReinsureItem(cSSRS.GetText(j, 2));
                        tLRPolEdorSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolEdorSchema.setReComCode(cSSRS.GetText(j, 4));
                        mMap.put(tLRPolEdorSchema.getSchema(), "DELETE&INSERT");
                    }
                }
            }
            if (!prepareOutputData()) {
            }
            //如果提交不成功,不能返回
            tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(this.mInputData, "")) {
                if (tPubSubmit.mErrors.needDealError()) {
                    buildError("insertData", "提取再保保全数据出错!");
                }
            }
            start += nCount;
        }
        return true;
    }

    /**
        * insertEdorData2
        * //处理续保回退的保单
        * @return boolean
        */
       private boolean insertEdorData2() {
           System.out.println("come in insertEdorData2()..");
          String ttSql =          //续保回退保单提数
                  "select a.GrpContNo A, a.GrpPolNo B,a.ContNo C,a.PolNo D, max(b.NewPolNo),a.MainPolNo F,a.MasterPolNo G, " +
                  "a.RiskCode H,a.RiskVersion I,a.enddate,a.PayToDate K, 'XB', -a.sumprem, max(b.modifydate), " +
                  "a.Amnt O, a.ManageCom P, b.ReNewCount Q, a.agentGroup R, a.cvalidate S " +
                  "from lcpol a,lbrnewstatelog b,lrpol c where a.polno = b.polno and a.renewcount<c.renewcount and a.ContType = '1'"+
                  " and  c.polno = a.polno and c.renewcount=b.renewcount and c.renewcount=(select max(renewcount) from lrpol c where c.polno=b.polno) " +
                  " and not exists (select 1 from lbpol d where d.polno=c.polno and d.renewcount=c.renewcount) "+
                  " and b.modifydate ='"+  this.mToday +"'  " +mSQL+   //以修改日期为提数时间
                  "group by a.GrpContNo, a.GrpPolNo,a.ContNo,a.PolNo,a.MainPolNo,a.MasterPolNo, a.RiskCode,a.RiskVersion,a.enddate,a.PayToDate, a.sumprem, a.Amnt, a.ManageCom, b.ReNewCount, a.agentGroup, a.cvalidate"+
                  " with ur ";
          int start = 1;
          int nCount = 5000;
          while (true) {
              SSRS tSSRS = new ExeSQL().execSQL(ttSql, start, nCount);
              if (tSSRS.getMaxRow() <= 0) {
                  break;
              }
              mMap = new MMap();
              if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                  for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                      LRPolEdorSchema tLRPolEdorSchema = new LRPolEdorSchema();
                      //添加其他数据
                      tLRPolEdorSchema.setGrpContNo(tSSRS.GetText(i, 1));
                      tLRPolEdorSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                      tLRPolEdorSchema.setContNo(tSSRS.GetText(i, 3));
                      tLRPolEdorSchema.setPolNo(tSSRS.GetText(i, 4));
                      tLRPolEdorSchema.setEdorNo(tSSRS.GetText(i, 5));
                      tLRPolEdorSchema.setMainPolNo(tSSRS.GetText(i, 6));
                      tLRPolEdorSchema.setMasterPolNo(tSSRS.GetText(i, 7));
                      tLRPolEdorSchema.setRiskCode(tSSRS.GetText(i, 8));
                      tLRPolEdorSchema.setRiskVersion(tSSRS.GetText(i, 9));
                      tLRPolEdorSchema.setEdorValidate(tSSRS.GetText(i, 10));
                      int noPassDays ; //未经过天数
                      noPassDays =PubFun.calInterval(tSSRS.GetText(i,19),tSSRS.GetText(i,10),"D");
                      if (noPassDays <= 0) {
                          noPassDays = 0; //保全摊回的时候，失效日期可以大于缴至日期,此时摊回零.
                      }
                      tLRPolEdorSchema.setNoPassDays(noPassDays);
                      tLRPolEdorSchema.setFeeoperationType(tSSRS.GetText(i, 12));
                      tLRPolEdorSchema.setGetMoney(tSSRS.GetText(i, 13));
                      tLRPolEdorSchema.setGetConfDate(tSSRS.GetText(i, 14));
                      tLRPolEdorSchema.setSumBackAmnt(tSSRS.GetText(i, 15));
                      tLRPolEdorSchema.setManageCom(tSSRS.GetText(i, 16));
                      tLRPolEdorSchema.setGetDataDate(this.mToday); //数据提取日期
                      tLRPolEdorSchema.setOperator(strOperate.equals("Server")?strOperate:globalInput.Operator);
                      tLRPolEdorSchema.setMakeDate(this.mCurrentDate);
                      tLRPolEdorSchema.setMakeTime(this.mCurrentTime);
                      tLRPolEdorSchema.setModifyDate(this.mCurrentDate);
                      tLRPolEdorSchema.setModifyTime(this.mCurrentTime);
                      tLRPolEdorSchema.setReNewCount(tSSRS.GetText(i, 17)); //新增续保次数
                       //新增成本中心
                      String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"+
                                   tSSRS.GetText(i,18)+"') with ur";
                      String CostCenter = new ExeSQL().getOneValue(sql);
                      tLRPolEdorSchema.setCostCenter(CostCenter);

                      String tSql =
                              "select a.ReContCode,'C',a.RiskCalSort,(select ReComCode from LRContInfo where ReContCode = " +
                              "a.ReContCode) from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount = "+tSSRS.GetText(i, 17)+" and a.PolNo = '" +
                              tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO " +
                              		"and ReNewCount = "+tSSRS.GetText(i, 17)+") " +
                              				" and exists " +
                              				"(select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01')" +
                              				"group by a.ReContCode,a.RiskCalSort,a.recontcode WITH UR";
                      SSRS cSSRS = new ExeSQL().execSQL(tSql);
                      //处理一个险种对应多个再保合同
                      for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                    	  String tRecontCode =cSSRS.GetText(j, 1);
							//20141120   杨阳
							//如果前台合同号不为空，需要匹配此合同才插入，否则跳过
							if(!"1".equals(mReContCode))
							{
								if(!tRecontCode.equals(mReContCode))
								{
									continue;
								}
							}
                          tLRPolEdorSchema.setReContCode(tRecontCode);
                          tLRPolEdorSchema.setReinsureItem(cSSRS.GetText(j, 2));
                          tLRPolEdorSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                          tLRPolEdorSchema.setReComCode(cSSRS.GetText(j, 4));
                          mMap.put(tLRPolEdorSchema.getSchema(), "DELETE&INSERT");
//                          String stru = " update lrpol set polno='"+tLRPolEdorSchema.getEdorNo()+"',modifydate=current date,modifytime=current time where "+
//                                        " PolNo='"+tLRPolEdorSchema.getPolNo()+"' and ReContCode='"+tLRPolEdorSchema.getReContCode()+
//                                        "' and ReinsureItem='"+tLRPolEdorSchema.getReinsureItem()+"' and ReNewCount= "+tLRPolEdorSchema.getReNewCount();
//                          mMap.put(stru, "UPDATE");
                      }
                  }
              }
              if (!prepareOutputData()) {
              }
              //如果提交不成功,不能返回
              tPubSubmit = new PubSubmit();
              if (!tPubSubmit.submitData(this.mInputData, "")) {
                  if (tPubSubmit.mErrors.needDealError()) {
                      buildError("insertData", "提取再保保全数据出错!");
                  }
              }
              start += nCount;
          }

           return true;
    }

    
     /**
     * 提取保全数据
     * @return boolean
     */
    private boolean getEdorDataT() {
        

        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
        

       
            this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!insertEdorData()) {
                buildError("getEdorData", "提取" + mToday + "号保全数据出错");
                return false;
            }
            if(!insertEdorData2()){
               buildError("insertEdorData2", "提取" + mToday + "号保全数据出错");
               return false;
           }
            
        
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    /**
     * 提取保全数据
     * @return boolean
     */
    private boolean getEdorData() {
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(mEndDate); //录入的终止日期，用FDate处理
        
        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!insertEdorData()) {
                buildError("getEdorData", "提取" + mToday + "号保全数据出错");
                return false;
            }
            if(!insertEdorData2()){
               buildError("insertEdorData2", "提取" + mToday + "号保全数据出错");
               return false;
           }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");
        mReContCode = (String) mGetCessData.getValueByName("ReContCode");
        if("".equals(mReContCode)||null == mReContCode)
        {
        	mReContCode = "1";
        }
        if("filldata".equals(strOperate))
        {
        	 mRiskCode = (String) mGetCessData.getValueByName("RiskCode");
             mGrpContNo = (String) mGetCessData.getValueByName("GrpContNo");
             mContNo = (String) mGetCessData.getValueByName("ContNo");
             mPolNo = (String) mGetCessData.getValueByName("PolNo");
             mOperator = (String) mGetCessData.getValueByName("Operator");
             strOperate ="fd"+PubFun.getCurrentDate2();
             mSQL ="";
             if(!"".equals(mGrpContNo))
         	{
             	mSQL +=" and a.grpcontno ='"+mGrpContNo+"'";
         	}
         	if(!"".equals(mRiskCode))
         	{
         		mSQL +=" and a.riskcode ='"+mRiskCode+"'";
         	}
         	if(!"".equals(mContNo))
         	{
         		mSQL +=" and a.ContNo ='"+mContNo+"'";
         	}
         	if(!"".equals(mPolNo))
         	{
         		mSQL +=" and a.PolNo ='"+mPolNo+"'";
         	}
         	if(!"".equals(mOperator))
         	{
         		mSQL +=" and a.Operator ='"+mOperator+"'";
         	}
        }
        else
        {
        	strOperate = globalInput.Operator;
        }
        return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(String mToDay) {
       
        mStartDate = mToDay;
        
        return true;
    }
    public String getResult() {
        return "没有传数据";
    }
    

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
//        cError.moduleName = "LRGetEdorDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
        LRNewGetEdorDataBL tLRNewGetEdorDataBL = new LRNewGetEdorDataBL();
        try {
        	tLRNewGetEdorDataBL.submitData(null, "");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
