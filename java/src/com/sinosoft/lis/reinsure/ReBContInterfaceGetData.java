package com.sinosoft.lis.reinsure;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import com.sinosoft.lis.reinsure.*;

import com.sinosoft.lis.db.LRGetDataLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;


import com.sinosoft.lis.taskservice.TaskThread;

import com.sinosoft.lis.vschema.LRGetDataLogSet;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;

import com.sinosoft.utility.VData;

public class ReBContInterfaceGetData extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */

    public String mCurrentDate = PubFun.getCurrentDate();

    private String mToDate =null;

    //业务处理相关变量
    /** 全局数据 */

    public ReBContInterfaceGetData() {
    }
    
    
    public boolean run(String tDate) {
        VData aVData = new VData();
        globalInput.ComCode = null;
        aVData.addElement(globalInput);
        mToDate = tDate;
        if (!submitData()) {
        	
            return false;
        }
        return true;
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData( ) {
    	
        System.out.println("after getInputData()........");

        if (!dealData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "Server";
        ReBContInterfaceGetData a=new ReBContInterfaceGetData();
        a.run();
    }


 

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        Connection con=null;
        
        try{
            	con=DBConnPool.getConnection();
            	con.setAutoCommit(false);
            	FDate chgdate = new FDate();
            	String currentDate=PubFun.calDate(mCurrentDate,-1,"D",null);
            	String Serialno="";//提数日志表流水号
            	Date dbdate = chgdate.getDate(currentDate); //录入的起始日期，用FDate处理
                Date dedate = chgdate.getDate(currentDate); //录入的终止日期，用FDate处理
            	if(!StrTool.cTrim(this.mToDate).equals("")&&this.mToDate!=null){
            		
            		dbdate = chgdate.getDate(this.mToDate); 
            		dedate = chgdate.getDate(this.mToDate);
                    
                }
            	
                

                 while (dbdate.compareTo(dedate) <= 0) {
           
            
            	
             //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            	//mToDate = chgdate.getString(dbdate); //录入的正在统计的日期
            	
            	SSRS tSSRS = new ExeSQL().execSQL("select SerialNo from LRGetDataLog where enddate='"+mToDate+"' with ur");
                if(tSSRS!=null && tSSRS.MaxRow>0){
                	Serialno=tSSRS.GetText(1, 1);
                }
                else{
                    Serialno=PubFun1.CreateMaxNo("ReContGetData", 15);
                    
                    }
                Statement stmt = con.createStatement();
                ResultSet tRs = null;
                String ttSql = "select calsql,tablename,calcode from LRGetDataClassDef where calcode<>'b4' and dealclass='BT'  order by ordernum with ur";
                ExeSQL tESQL = new ExeSQL();
                tSSRS = tESQL.execSQL(ttSql);
                if(tSSRS!=null && tSSRS.MaxRow>0){
                    for(int index=1;index<=tSSRS.MaxRow;index++){
                        String exeSql="";
                        PubCalculator tPubCalculator = new PubCalculator();
                        tPubCalculator.addBasicFactor("ToDay",mToDate);
                        tPubCalculator.addBasicFactor("Operator", "Server");
                        tPubCalculator.addBasicFactor("SerialNo", Serialno);
                        tPubCalculator.addBasicFactor("DataState", "4");//数据状态 '0' 正常状态  '1' 分保完成  '-1' 数据补提
                        tPubCalculator.addBasicFactor("SingRecontFlag", "0");//0 非单再保合同补提  1 单再保合同补提 
                        tPubCalculator.setCalSql(tSSRS.GetText(index, 1));
                        exeSql = tPubCalculator.calculateEx();
                        System.out.println(tSSRS.GetText(index, 3)+":::"+exeSql);
                        tRs=stmt.executeQuery(exeSql);
                        if(insert(tRs,tSSRS.GetText(index, 2),con)==false){
                            return false;
                        }
                    }
                    con.commit();
                }
               
                        String ttSql1 = "select calsql,tablename from LRGetDataClassDef where calcode='b4' and dealclass='BT' order by ordernum with ur";
                      
                        SSRS tSSRS1 = tESQL.execSQL(ttSql1);
                        if(tSSRS1!=null && tSSRS1.MaxRow>0){
                           for(int index=1;index<=tSSRS1.MaxRow;index++){
                        	   String exeSql="";
                        	   PubCalculator tPubCalculator = new PubCalculator();
                        	   tPubCalculator.addBasicFactor("ToDay",mToDate);
                        	   tPubCalculator.addBasicFactor("Operator", "Server");
                        	   tPubCalculator.addBasicFactor("SerialNo", Serialno);
                     
                        	   tPubCalculator.setCalSql(tSSRS1.GetText(1, 1));
                        	   exeSql = tPubCalculator.calculateEx();
                    
                        	   ResultSet tRs1=stmt.executeQuery(exeSql);
                        	   if(insert(tRs1,tSSRS1.GetText(1, 2),con)==false){
                        		   return false;
                        	   }
                        	   con.commit();
                        
                            }
                        }
                       
                if(GetCalDate(mToDate)==false){
            		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
            		return false;
            	}
            con.commit();

            con.close();
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
        }catch(Exception e){
            try{
                e.printStackTrace();
                con.close();
                return false;
            }catch(Exception c){}
        }
        return true;
    }
    
    /**
     * 处理中间表提数后的操作
     * @param mToDate
     * @return
     */
    private boolean GetCalDate(String mToDate){
    	
    	
    	
    	if(!cessDeal()){//新单续期续保提数计算
    		return false;
    	}
    	
    	
    	return true;
    }
    

    
    /**新单续期续保提数计算
     * cessDeal
     * @return boolean
     */
    private boolean cessDeal(){
    	
    	LRBNewGetCessDataTBL tLRBNewGetCessDataTBL=new LRBNewGetCessDataTBL();
    	if(tLRBNewGetCessDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRBNewCalCessDataTBL tLRBNewCalCessDataTBL=new LRBNewCalCessDataTBL();
    	if(tLRBNewCalCessDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
    /**保全摊回提数计算
     * edorDeal
     * @return boolean
     */
    private boolean edorDeal(){
    	
    	LRNewGetEdorDataTBL tLRNewGetEdorDataTBL=new LRNewGetEdorDataTBL();
    	if(tLRNewGetEdorDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRNewCalEdorDataTBL tLRNewCalEdorDataTBL=new LRNewCalEdorDataTBL();
    	if(tLRNewCalEdorDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
    /**理赔摊回提数计算
     * clmDeal
     * @return boolean
     */
    private boolean clmDeal(){
    	
    	LRNewGetClaimDataTBL tLRNewGetClaimDataTBL=new LRNewGetClaimDataTBL();
    	if(tLRNewGetClaimDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRNewCalClaimDataTBL tLRNewCalClaimDataTBL=new LRNewCalClaimDataTBL();
    	if(tLRNewCalClaimDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
    /**清单提数
     * listDeal
     * @return boolean
     */
    private boolean listDeal(){
    	
    	LRCessListTBL tLRCessListTBL=new LRCessListTBL();//分保清单
    	if(tLRCessListTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRClaimListTBL tLRClaimListTBL=new LRClaimListTBL();
    	if(tLRClaimListTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    private boolean insert(ResultSet rs,String table,Connection con){
        Statement stmt = null;
        ResultSet tRs = null;
        ResultSetMetaData rsmd = null;

        ResultSetMetaData rsmd1 = null;
        try{
            stmt = con.createStatement();
            tRs = stmt.executeQuery("select * from " + table+" fetch first 1 rows only");
            rsmd = tRs.getMetaData();
            rsmd1 = rs.getMetaData();
            int columnCount=rsmd.getColumnCount();
            int columnCount1=rsmd1.getColumnCount();
            if(columnCount!=columnCount1){
//                this.mErrors.addOneError("与目标的列数不符");
                return false;
            }
            System.out.println("begin......");
            while(rs.next()){
                StringBuffer insertSql = new StringBuffer();
                StringBuffer whereSql=new StringBuffer();
                insertSql .append("insert into "+table+" (");
                int flag=0;
                for (int i = 0; i < columnCount; i++) {
                    if(flag==1){
                        insertSql.append(",");
                        whereSql.append(",");
                    }
                    insertSql .append(rsmd.getColumnName(i+1));

                    int mDataType=rsmd.getColumnType(i+1);
//                  判断数据类型
                    if ((mDataType == Types.CHAR) || (mDataType == Types.VARCHAR)) {
                        if(rs.getString(i+1)==null){
                            whereSql.append(rs.getObject(i+1));
                        }else{
                            whereSql.append("'");
                            whereSql.append(StrTool.cTrim(rs.getString(i+1)));
                            whereSql.append("'");
                        }
                    }
                    else if ((mDataType == Types.TIMESTAMP) || (mDataType == Types.DATE)) {
                        if(rs.getDate(i+1)==null){
                            whereSql.append(rs.getObject(i+1));
                        }else{
                            whereSql.append("'");
                            whereSql.append(rs.getDate(i+1));
                            whereSql.append("'");
                        }
                    }else{
                        whereSql.append(rs.getObject(i+1));
                    }
                    flag=1;
                }
                insertSql .append(") values ( ");
                insertSql .append(whereSql.toString());
                insertSql .append(")");
                String ExeSql=insertSql.toString();
                try{
                    //System.out.println(ExeSql);
                    stmt.executeUpdate(ExeSql);
                }catch(Exception insertExc){
                    System.out.println("Error Insert......"+ExeSql);
                }
            }
            stmt.close();
        }catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
//            this.mErrors.addOneError("插入语句拼写错误");
            return false;
        }
        return true;
    }





    /*
     *
     */
//    private void buildError(String szFunc, String szErrMsg) {
//        CError cError = new CError();
//
//        cError.moduleName = "ReContInterfaceGetDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
//    }
}
