package com.sinosoft.lis.reinsure;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import com.sinosoft.lis.reinsure.*;

import com.sinosoft.lis.db.LRGetDataLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;


import com.sinosoft.lis.taskservice.TaskThread;

import com.sinosoft.lis.vschema.LRGetDataLogSet;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;

import com.sinosoft.utility.VData;

public class ReGContInterfaceGetData
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */

    public String mCurrentDate = PubFun.getCurrentDate();

    private String mToDate =null;

    //业务处理相关变量
    /** 全局数据 */

    public ReGContInterfaceGetData() {
    }
    
   
    public boolean run(String tDate) {
        VData aVData = new VData();
        globalInput.ComCode = null;
        aVData.addElement(globalInput);
        mToDate = tDate;
        if (!submitData()) {
            return false;
        }
        return true;
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData() {
    	
        System.out.println("after getInputData()........");

        if (!dealData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "Server";
        ReGContInterfaceGetData a=new ReGContInterfaceGetData();
        
    }


 

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        Connection con=null;
        
        try{
            	con=DBConnPool.getConnection();
            	con.setAutoCommit(false);
            	
            	String currentDate=PubFun.calDate(mCurrentDate,-1,"D",null);
            	String Serialno="";//提数日志表流水号
            	
            	FDate chgdate = new FDate();
            	 Date dbdate = chgdate.getDate(currentDate); //录入的起始日期，用FDate处理
                 Date dedate = chgdate.getDate(currentDate); //录入的终止日期，用FDate处理
                 if(!StrTool.cTrim(this.mToDate).equals("")&&this.mToDate!=null){
             		
             		dbdate = chgdate.getDate(this.mToDate); 
             		dedate = chgdate.getDate(this.mToDate);
                     
                 }
                 while (dbdate.compareTo(dedate) <= 0) {
            
            	
             //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            	mToDate = chgdate.getString(dbdate); //录入的正在统计的日期
            	           	
            	
                SSRS tSSRS = new ExeSQL().execSQL("select SerialNo from LRGetDataLog where enddate='"+mToDate+"' with ur");
                if(tSSRS!=null && tSSRS.MaxRow>0){
                	Serialno=tSSRS.GetText(1, 1);
                }
                else{
                	Serialno=PubFun1.CreateMaxNo("ReContGetData", 15);
                }
        
                
                Statement stmt = con.createStatement();
                ResultSet tRs = null;

                ExeSQL tESQL = new ExeSQL();

                String ttSqlt = "select calsql,tablename,calcode from LRGetDataClassDef where  dealclass='GT'  order by ordernum with ur";
                ExeSQL tESQLt = new ExeSQL();
                tSSRS = tESQLt.execSQL(ttSqlt);
                if(tSSRS!=null && tSSRS.MaxRow>0){
                    for(int index=1;index<=tSSRS.MaxRow;index++){
                        String exeSql="";
                        PubCalculator tPubCalculator = new PubCalculator();
                        tPubCalculator.addBasicFactor("ToDay",mToDate);
                        tPubCalculator.addBasicFactor("Operator", "Server");
                        tPubCalculator.addBasicFactor("SerialNo", Serialno);
                        tPubCalculator.addBasicFactor("DataState", "3");//数据状态 '0' 正常状态  '1' 分保完成  '-1' 数据补提 '2' 临分提数
                        tPubCalculator.addBasicFactor("SingRecontFlag", "0");//0 非单再保合同补提  1 单再保合同补提 
                        tPubCalculator.setCalSql(tSSRS.GetText(index, 1));
                        exeSql = tPubCalculator.calculateEx();
                        System.out.println(tSSRS.GetText(index, 3)+":::"+exeSql);
                        tRs=stmt.executeQuery(exeSql);
                        if(insert(tRs,tSSRS.GetText(index, 2),con)==false){
                            return false;
                        }
                    }
                    con.commit();
                }
                        
                       
                if(GetCalDate(mToDate)==false){
            		
            		return false;
            	}
                con.commit();
                con.close();
                dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
                 }
        }catch(Exception e){
            try{
                e.printStackTrace();
                con.close();
                return false;
            }catch(Exception c){}
        }
        return true;
    }
    
    /**
     * 处理中间表提数后的操作
     * @param mToDate
     * @return
     */
    private boolean GetCalDate(String mToDate){
    	
    
//    	
    	if(!cessDeal()){//新单续期续保提数计算
    		return false;
    	}
    	if(!edorDeal()){//保全摊回提数计算
    		return false;
    	}
    	if(!clmDeal()){//理赔摊回提数计算
    		return false;
    	}
    	if(!listDeal()){//清单提数
    		return false;
    	}
    	
    	return true;
    }
    

    
    /**新单续期续保提数计算
     * cessDeal
     * @return boolean
     */
    private boolean cessDeal(){
    	
    	LRGNewGetCessDataBL tLRGNewGetCessDataTBL=new LRGNewGetCessDataBL();
    	if(tLRGNewGetCessDataTBL.submitData("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRGNewCalCessDataBL tLRGNewCalCessDataTBL=new LRGNewCalCessDataBL();
    	if(tLRGNewCalCessDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
    /**保全摊回提数计算
     * edorDeal
     * @return boolean
     */
    private boolean edorDeal(){
    	
    	LRGNewGetEdorDataBL tLRGNewGetEdorDataBL=new LRGNewGetEdorDataBL();
    	if(tLRGNewGetEdorDataBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRGNewCalEdorDataBL tLRGNewCalEdorDataBL=new LRGNewCalEdorDataBL();
    	if(tLRGNewCalEdorDataBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
    /**理赔摊回提数计算
     * clmDeal
     * @return boolean
     */
    private boolean clmDeal(){
    	
    	LRGNewGetClaimDataBL tLRGNewGetClaimDataBL=new LRGNewGetClaimDataBL();
    	LRGTempNewGetClaimDateBL tLRGTempNewGetClaimDateBL=new LRGTempNewGetClaimDateBL();
    	if(tLRGNewGetClaimDataBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	
    	if(tLRGTempNewGetClaimDateBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRGNewCalClaimDataBL tLRGNewCalClaimDataBL=new LRGNewCalClaimDataBL();
    	if(tLRGNewCalClaimDataBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
    /**清单提数
     * listDeal
     * @return boolean
     */
    private boolean listDeal(){
    	
    	LRGCessListBL tLRGCessListBL=new LRGCessListBL();//分保清单
    	if(tLRGCessListBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRGClaimListBL tLRGClaimListBL=new LRGClaimListBL();
    	if(tLRGClaimListBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    private boolean insert(ResultSet rs,String table,Connection con){
        Statement stmt = null;
        ResultSet tRs = null;
        ResultSetMetaData rsmd = null;

        ResultSetMetaData rsmd1 = null;
        try{
            stmt = con.createStatement();
            tRs = stmt.executeQuery("select * from " + table+" fetch first 1 rows only");
            rsmd = tRs.getMetaData();
            rsmd1 = rs.getMetaData();
            int columnCount=rsmd.getColumnCount();
            int columnCount1=rsmd1.getColumnCount();
            if(columnCount!=columnCount1){
//                this.mErrors.addOneError("与目标的列数不符");
                return false;
            }
            System.out.println("begin......");
            while(rs.next()){
                StringBuffer insertSql = new StringBuffer();
                StringBuffer whereSql=new StringBuffer();
                insertSql .append("insert into "+table+" (");
                int flag=0;
                for (int i = 0; i < columnCount; i++) {
                    if(flag==1){
                        insertSql.append(",");
                        whereSql.append(",");
                    }
                    insertSql .append(rsmd.getColumnName(i+1));

                    int mDataType=rsmd.getColumnType(i+1);
//                  判断数据类型
                    if ((mDataType == Types.CHAR) || (mDataType == Types.VARCHAR)) {
                        if(rs.getString(i+1)==null){
                            whereSql.append(rs.getObject(i+1));
                        }else{
                            whereSql.append("'");
                            whereSql.append(StrTool.cTrim(rs.getString(i+1)));
                            whereSql.append("'");
                        }
                    }
                    else if ((mDataType == Types.TIMESTAMP) || (mDataType == Types.DATE)) {
                        if(rs.getDate(i+1)==null){
                            whereSql.append(rs.getObject(i+1));
                        }else{
                            whereSql.append("'");
                            whereSql.append(rs.getDate(i+1));
                            whereSql.append("'");
                        }
                    }else{
                        whereSql.append(rs.getObject(i+1));
                    }
                    flag=1;
                }
                insertSql .append(") values ( ");
                insertSql .append(whereSql.toString());
                insertSql .append(")");
                String ExeSql=insertSql.toString();
                try{
                    //System.out.println(ExeSql);
                    stmt.executeUpdate(ExeSql);
                }catch(Exception insertExc){
                    System.out.println("Error Insert......"+ExeSql);
                }
            }
            stmt.close();
        }catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
//            this.mErrors.addOneError("插入语句拼写错误");
            return false;
        }
        return true;
    }





    /*
     *
     */
//    private void buildError(String szFunc, String szErrMsg) {
//        CError cError = new CError();
//
//        cError.moduleName = "ReContInterfaceGetDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
//    }
}
