package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.LCReInsurTaskSchema;
import com.sinosoft.lis.schema.LCReInsurUWIdeaSchema;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCReInsurTaskDB;
import com.sinosoft.lis.db.LCReInsurUWIdeaDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LCUWSendTraceDB;
import com.sinosoft.lis.vschema.LCReInsurTaskSet;
import com.sinosoft.lis.vschema.LCReInsurUWIdeaSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.schema.LCReInsurIdeaSchema;
import com.sinosoft.lis.vschema.LCReInsurIdeaSet;
import com.sinosoft.lis.db.LCReInsurIdeaDB;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 新契约</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class AnswerUWReInsureBL {
    public AnswerUWReInsureBL() {
    }

    /** 传入参数 */
    private VData mInputData;
    /** 传入操作符 */
    private String mOperate;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 报错存储对象 */
    public CErrors mErrors = new CErrors();
    /** 最后保存结果 */
    private VData mResult = new VData();
    /** 最后递交Map */
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperator;
    private String mManageCom;
    private String mPolNo;
    private String mRemark;
    private String mFilePath;
    private String mFileName;
    private int mNo; //实际申请的个数
    private TransferData mTransferData = new TransferData();
    /**再保审核任务表*/
    private LCReInsurTaskSchema mLCReInsurTaskSchema = new LCReInsurTaskSchema();

    /**再保审核任务核保意见表*/
    private LCReInsurIdeaSchema mLCReInsurIdeaSchema = new
            LCReInsurIdeaSchema();


    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        System.out.println("into AnswerUWReInsurBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("AnswerUWReInsurBL finished...");
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("into AnswerUWReInsurBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "AnswerUWReInsurBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("into AnswerUWReInsurBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("into AnswerUWReInsurBL.dealData()...");
        LCPolDB tLCPolDB = new LCPolDB();
        mPolNo = (String) mTransferData.getValueByName("PolNo");
        mRemark = (String) mTransferData.getValueByName("Remark");
        mFilePath = (String) mTransferData.getValueByName("FilePath");
        mFileName = (String) mTransferData.getValueByName("FileName");
        System.out.println(mFileName);
        tLCPolDB.setPolNo(mPolNo);
        if (!tLCPolDB.getInfo()) {
            String str = "查询险种信息失败 !";
            buildError("dealData", str);
            System.out.println("在程序AnswerUWReInsurBL.dealData() - 189 : " + str);
            return false;
        }

        int uwno = 0; //核保顺序号
        LCReInsurTaskDB tLCReInsurTaskDB = new LCReInsurTaskDB();
        StringBuffer sql = new StringBuffer(255);
        sql.append("select * from LCReInsurTask where polno='");
        sql.append(mPolNo+ "' and uwno=(select max(uwno) from lcreinsurtask where polno='"+mPolNo+"')");
        sql.append(" order by uwno desc");
        LCReInsurTaskSet tLCReInsurTaskSet = new LCReInsurTaskSet();
        tLCReInsurTaskSet = tLCReInsurTaskDB.executeQuery(sql.toString());
        System.out.println(" sql: "+sql.toString());

        if (tLCReInsurTaskSet == null || tLCReInsurTaskSet.size() == 0) {
            uwno = 1;
        } else {

            uwno = tLCReInsurTaskSet.size(); //再保申请发起次数

        }

        System.out.println("uwno: "+uwno);
        mLCReInsurTaskSchema=tLCReInsurTaskSet.get(1);

//再保审核任务表
//        mLCReInsurTaskSchema.setUWNo(uwno);
//        mLCReInsurTaskSchema.setPolNo(mPolNo);
        mLCReInsurTaskSchema.setState("01");
        mLCReInsurTaskSchema.setOperator(mGlobalInput.Operator);
        mLCReInsurTaskSchema.setManageCom(mGlobalInput.ManageCom);
//        mLCReInsurTaskSchema.setMakeDate(PubFun.getCurrentDate());
//        mLCReInsurTaskSchema.setMakeTime(PubFun.getCurrentTime());
        mLCReInsurTaskSchema.setModifyDate(PubFun.getCurrentDate());
        mLCReInsurTaskSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(mLCReInsurTaskSchema,"UPDATE");

//再保审核任务核保意见表
        int tuwno = 0; //核保顺序号
        LCReInsurIdeaDB tLCReInsurIdeaDB = new LCReInsurIdeaDB();
        StringBuffer tsql = new StringBuffer(255);
        tsql.append("select * from LCReInsurIdea where polno='");
        tsql.append(mPolNo);
        tsql.append("' order by uwno desc");
        LCReInsurIdeaSet LCReInsurIdeaSet = new LCReInsurIdeaSet();
        LCReInsurIdeaSet = tLCReInsurIdeaDB.executeQuery(tsql.toString());
        if (LCReInsurIdeaSet == null || LCReInsurIdeaSet.size() == 0) {
            tuwno = 1;
        } else {

            tuwno = LCReInsurIdeaSet.size() + 1; //再保回复次数加1
        }

        System.out.println("tuwno: "+tuwno);

        //String strOthNo[] = mFileName.split("=");
        mLCReInsurIdeaSchema.setUWNo(tuwno);
        mLCReInsurIdeaSchema.setPolNo(mPolNo);
        mLCReInsurIdeaSchema.setAdjunctPath(mFilePath + mFileName);
        mLCReInsurIdeaSchema.setReInsurer(mGlobalInput.Operator);
        mLCReInsurIdeaSchema.setOperator(mGlobalInput.Operator);
        mLCReInsurIdeaSchema.setUWIdea(mRemark);
        mLCReInsurIdeaSchema.setManageCom(mGlobalInput.ManageCom);
        mLCReInsurIdeaSchema.setMakeDate(PubFun.getCurrentDate());
        mLCReInsurIdeaSchema.setMakeTime(PubFun.getCurrentTime());
        mLCReInsurIdeaSchema.setModifyDate(PubFun.getCurrentDate());
        mLCReInsurIdeaSchema.setModifyTime(PubFun.getCurrentTime());

        map.put("update LWMission set  ActivityStatus='" + 1 +
                "' where Activityid in ('0000001100','0000001160') and missionprop2 ='" +
                tLCPolDB.getProposalContNo() + "'", "UPDATE");
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("into BriefCardSignBL.prepareOutputData()...");
        //map.put(mLCReInsurTaskSchema, "INSERT");
        map.put(mLCReInsurIdeaSchema, "INSERT");
        this.mResult.add(map);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefCardSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
