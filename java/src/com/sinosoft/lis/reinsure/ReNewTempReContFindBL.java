/**
 * Copyright (c) 2008 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import java.io.File;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LRTempCessContSet;

/*
 * <p>ClassName: ReNewContDetailBL </p>
 * <p>Description: ReNewContDetailBL类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: Sun yu
 * @CreateDate：2008-10-11
 */
public class ReNewTempReContFindBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();
    private LRTempCessContSet mLRTempCessContSet=new LRTempCessContSet();

    //业务处理相关变量
    /** 全局数据 */

    public ReNewTempReContFindBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if(this.strOperate.equals("ONLOAD")){//下载清单
            if(onload()==false){
                buildError("onload()","下载清单失败");
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLRTempCessContSet=(LRTempCessContSet) cInputData.getObjectByObjectName(
                "LRTempCessContSet", 0);
        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
    private boolean onload(){
        String tContType = (String) mTransferData.getValueByName("ContType");
        String tContNo = (String) mTransferData.getValueByName("ContNo");
        String tAppnt = (String) mTransferData.getValueByName("Appnt"); //投保人/单位名称
        String tStartDate = (String) mTransferData.getValueByName("StartDate");
        String tEndDate = (String) mTransferData.getValueByName("EndDate");
        String tReContNo = (String) mTransferData.getValueByName("ReContNo");

        String strWhere="";
        String tWhere="";
        if(!StrTool.cTrim(tContType).equals("")){
            tWhere+=" and a.conttype='"+tContType+"' ";
            if(StrTool.cTrim(tContType).equals("1")&&tContNo!=null&&!StrTool.cTrim(tContNo).equals("")){
                strWhere+=" and temp.ContNo='"+tContNo+"' ";
            }else if(StrTool.cTrim(tContType).equals("2")&&tContNo!=null&&!StrTool.cTrim(tContNo).equals("")){
                strWhere+=" and temp.grpContNo='"+tContNo+"' ";
            }
        }
        if(!StrTool.cTrim(tAppnt).equals("")){
            strWhere+=" and temp.appntname='"+tAppnt+"' ";
        }
        if(!StrTool.cTrim(tStartDate).equals("")){
            strWhere+=" and temp.signdate>='"+tStartDate+"' ";
        }
        if(!StrTool.cTrim(tEndDate).equals("")){
            strWhere+=" and temp.signdate<='"+tEndDate+"' ";
        }

        String tReContNoStr="";
        if(this.mLRTempCessContSet!=null){
            for(int i=1;i<=this.mLRTempCessContSet.size();i++){
                if(tReContNoStr.equals("")){
                    tReContNoStr+="'"+mLRTempCessContSet.get(i).getTempContCode()+"'";
                }else{
                    tReContNoStr+=",";
                    tReContNoStr+="'"+mLRTempCessContSet.get(i).getTempContCode()+"'";
                }
            }
        }
        if(!StrTool.cTrim(tReContNo).equals("")){
            if(tReContNoStr.equals("")){
                tReContNoStr+="'"+tReContNo+"'";
            }else{
                tReContNoStr+=",";
                tReContNoStr+="'"+tReContNo+"'";
            }
        }
        if(!tReContNoStr.equals("")){
            strWhere+=" and temp.tempcontcode in ("+tReContNoStr+") ";
        }

        String strSQL="select tempcontcode,grpcontno,contno,polno,riskcode,insuredno, "
        +"insuredname,appntno,appntname,cvalidate,enddate,signdate,"
        +"sum(prem),sum(amnt) from (select b.tempcontcode,a.grpcontno,a.contno,a.polno,a.riskcode,"
        +"a.insuredno,a.insuredname,a.appntno,a.appntname,a.cvalidate,a.enddate,"
        +"a.signdate,a.prem,a.amnt from lcpol a, LRTempCessCont b,lrinterface c "
        +"where '1230196201000' = '1230196201000' and c.polstat = '3' and a.AppFlag = '1' and a.polno=c.polno and a.renewcount=c.renewcount "
        +"and (a.contno = b.contno or a.grpcontno=b.contno) and b.ReContState = '01' "
        + tWhere
        +"union all "
        +"select b.tempcontcode,a.grpcontno,a.contno,a.polno,a.riskcode,a.insuredno,"
        +"a.insuredname,a.appntno,a.appntname,a.cvalidate,a.enddate,a.signdate,"
        +"a.prem,a.amnt from lbpol a, LRTempCessCont b,lrinterface c where c.polstat = '3' "
        +"and a.AppFlag = '1' and a.polno=c.polno and a.renewcount=c.renewcount "
        + tWhere
        +"and (a.contno = b.contno or a.grpcontno=b.contno) and b.ReContState = '01') temp "
        +"where 1 = 1 "
        + strWhere
        +"group by tempcontcode,grpcontno,contno,"
        +"polno,riskcode,insuredno,insuredname, appntno,appntname,cvalidate, enddate,signdate "
        +"order by signdate, tempcontcode, grpcontno, contno with ur ";

//         System.out.println(strSQL);
        try{
            String strArr[][] = new String[1][15];
            String name = "临时分保"+PubFun.getCurrentDate()+"_"+PubFun.getCurrentTime()+".xls";
            WriteToExcel t = new WriteToExcel(name);
            t.createExcelFile();
            String[] sheetName = {
                "sheet1"};
            t.addSheet(sheetName);

            strArr[0][0]="临分合同编码";
            strArr[0][1]="团单号";
            strArr[0][2]="保单号";
            strArr[0][3]="险种保单号";
            strArr[0][4]="险种编码";
            strArr[0][5]="被保人号";
            strArr[0][6]="被保人姓名";
            strArr[0][7]="投保人";
            strArr[0][8]="投保人姓名";
            strArr[0][9]="生效日期";
            strArr[0][10]="终止日期";
            strArr[0][11]="签单日期";
            strArr[0][12]="保费";
            strArr[0][13]="保额";

            t.setData(0, strArr);
            t.setData(0, strSQL);
            String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
            File f=new File(tServerPath+"reinsure/"+name);
            f.delete();
            t.write(tServerPath+"reinsure/");

            String tURL = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerURL' ");
            String resultURL=tURL+"reinsure/"+name;
            mResult.clear();
            mResult.addElement(resultURL);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

