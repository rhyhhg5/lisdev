/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Zhang Bin
 * @CreateDate：2006-07-30
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

public class LRExportClaimDataBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 前台传入的公共变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String strOperate="";

    private LCPolSet mLCPolSet = new LCPolSet();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();


    //业务处理相关变量
    /** 全局数据 */

    public LRExportClaimDataBL()
    {
    }

    /**
    * 提交数据处理方法
    * @param cInputData 传入的数据,VData对象
    * @param cOperate 数据操作字符串
    * @return 布尔值（true--提交成功, false--提交失败）
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       System.out.println("Come in BL.Submit..............");
       this.strOperate = cOperate;
       if (strOperate.equals(""))
       {
           buildError("verifyOperate", "不支持的操作字符串");
           return false;
       }
       if (!getInputData(cInputData))return false;

       // 准备所有要打印的数据
       if (!getPrintData()) {
           return false;
       }


       return true;
   }

   public static void main(String[] args)
      {
          GlobalInput globalInput = new GlobalInput();
          globalInput.ComCode = "8611";
          globalInput.Operator = "001";

          // prepare main plan
          // 准备传输数据 VData
          VData vData = new VData();
          LRExportClaimDataBL tLRExportClaimDataBL = new LRExportClaimDataBL();
          try
          {
              vData.add(globalInput);
              tLRExportClaimDataBL.submitData(vData,"INSERT");
          }
          catch (Exception ex)
          {
              ex.printStackTrace();
          }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        System.out.println("Come to prepareOutputData()...........");
        try
        {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean getPrintData() {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        //统计的起讫时间
        ListTable multTable = new ListTable();
        multTable.setName("MULT");
        String[] Title = {"col1", "col2", "col3", "col4", "col5",
                         "col6", "col7", "col8", "col9", "col10",
                         "col11", "col12", "col13", "col14", "col15",
                         "col16", "col17", "col18", "col19", "col20",
                         "col21", "col22", "col23", "col24", "col25",
                         "col26", "col27"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql = " select a.ClmNo,a.PolNo,a.ReinsureCom,a.ReinsurItem,a.InsuredYear,a.RgtNo,a.CaseNo,a.RiskCode,a.SaleChnl, " //1-9
                    + " a.InsuredNo,a.InsuredName,a.AppntNo,a.AppntName,a.CValiDate,a.EndDate,a.AccidentDate,a.ClaimMoney, " //17
                    + " a.RealPay,a.EndCaseDate,a.ConfDate,a.RgtDate,a.AccDesc, " //22
                    + " a.LeaveHospDate,a.Diagnoses,a.CessPersonSug,a.CessionAmount,a.ClaimCessRate " //27
                    + " from LRClaimPolicy a where 1=1 " //+ " and ConfDate>='2006-11-02' and ConfDate<='2006-11-10' "
                    + " with ur "
            ;
            System.out.println("SQL: "+sql);

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            for (int i = 0; i < count; i++) {
                strCol = new String[27];

                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];
                strCol[9] = temp[i][9];
                strCol[10] = temp[i][10];
                strCol[11] = temp[i][11];
                strCol[12] = temp[i][12];
                strCol[13] = temp[i][13];
                strCol[14] = temp[i][14];
                strCol[15] = temp[i][15];
                strCol[16] = temp[i][16];
                strCol[17] = temp[i][17];
                strCol[18] = temp[i][18];
                strCol[19] = temp[i][19];
                strCol[20] = temp[i][20];
                strCol[21] = temp[i][21];

                strCol[22] = temp[i][22];
                strCol[23] = temp[i][23];
                strCol[24] = temp[i][24];
                strCol[25] = temp[i][25];
                strCol[26] = temp[i][26];

                multTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "OrderCollectBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LRExportClaim.vts", "printer"); //最好紧接着就初始化xml文档

        //OrderCollectBL模版元素
        texttag.add("ManageComName", mGlobalInput.ComCode);
        texttag.add("CurrentDate", PubFun.getCurrentDate());
        texttag.add("Operator",mGlobalInput.Operator);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        //保存信息

        xmlexport.addListTable(multTable, Title);

        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        System.out.println("Come to RiskPayRateBL getInputData()..........");
         //全局变量
         this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
         mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                 "TransferData", 0));
         if (mTransferData == null) {
             buildError("getInputData", "没有得到足够的信息！");
             return false;
         }

         return true;

    }

    public VData getResult() {
        return mResult;
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
