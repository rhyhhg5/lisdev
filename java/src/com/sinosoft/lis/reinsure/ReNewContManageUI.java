/*
 * <p>ClassName: ReNewContManageUI </p>
 * <p>Description: ReNewContManageUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2008-10-30
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.*;

public class ReNewContManageUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private String mResult = new String();

    //业务处理相关变量
    /** 全局数据 */

    public ReNewContManageUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        ReNewContManageBL tReNewContManageBL = new ReNewContManageBL();
        if (!tReNewContManageBL.submitData(cInputData, cOperate)) {
            if (tReNewContManageBL.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tReNewContManageBL.mErrors);
            } else {
                buildError("submitData", "ReNewContManageBL发生错误，但是没有提供详细信息！");
            }
            return false;
        }
        mResult = tReNewContManageBL.getResult();
        return true;
    }

    public String getResult() {
        return this.mResult;
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        // 准备传输数据 VData
        VData vData = new VData();
        try {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
