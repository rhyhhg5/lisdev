/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.Hashtable;

import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LRCalFactorValueSet;
import com.sinosoft.lis.vschema.LRCalModeSet;
import com.sinosoft.lis.vschema.LRContInfoSet;
import com.sinosoft.lis.vschema.LRDutySet;
import com.sinosoft.lis.vschema.LRInterfaceSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.lis.vschema.LRTempCessContInfoSet;
import com.sinosoft.lis.vschema.LRTempCessContSet;

import java.util.*;

/*
 * <p>ClassName: LRNewGetCessDataBL </p>
 * <p>Description: 提取再保新单数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 孙宇
 * @CreateDate：2008-10-30
 */
public class LRNewTempCessDataBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
//    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mReContCode = "";
    private String mGrpPolNo = "";
    private String mPayNo = "";
    private String tFlag="";
    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private String mRecontCode; //再保合同代码
    private String mRiskCalSort = "";
    private Hashtable m_hashLMCheckField = new Hashtable();
    //业务处理相关变量
    /** 全局数据 */

    public LRNewTempCessDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

 
    
  


   

    /**
     * 提取分保数据
     * @return boolean
     */
    public boolean getCessData(
    		LRTempCessContSchema tLRTempCessContSchema,Date dbdate) {
        FDate chgdate = new FDate();
       

        Connection con=DBConnPool.getConnection();
        try{
            //con.setAutoCommit(false);
           
        	 //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            	mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            	mGrpPolNo="s";
            	mPayNo="s";
            
                if (!insertPolData(tLRTempCessContSchema,con)) {
                    buildError("getCessData", "提取" + mToday + "日新单数据出错");
                    return false;
                }
                dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
                con.commit();
            
            
            con.close();
        }catch(Exception e){
            try{
                e.printStackTrace();
                con.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 提取合同分保保单信息
     * @return boolean
     */
    private boolean insertPolData( 
    		LRTempCessContSchema tLRTempCessContSchema,Connection con) {
    	
    	String tSql="";
		tSql="select 1 from lrpol a where recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
				"and  (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' fetch first 1 rows only with ur ";
		SSRS tSSRS = new ExeSQL().execSQL(tSql);
		 //判断首期该保单的临分数据是否存在。存在的话即对当前日期中间表中polno与所传进相等的数据进行处理,
		 // 否则进入判断是否需要反冲的处理
		 if(tSSRS!=null && tSSRS.MaxRow>0){
			 if(!cessComEData(tLRTempCessContSchema,con))
				 return false; 		 
		 }
		 else{
			 if(!cessComNData(tLRTempCessContSchema,con))
				 return false;
		 }
        return true;
    }
    
    /**
     * 计算正常提取到的数据的分保提数处理
     * @param tLRInterfaceSchema
     * @param tDate
     * @param con
     * @return boolean
     */
    private boolean cessComEData(
    		LRTempCessContSchema tLRTempCessContSchema,Connection con){
    	
    	String tGetD="select * from lrinterface  where  state='2' and polstat='3'  " +
		"and  (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' and dutydate='"+mToday+"'  " ;
		if("2".equals(tLRTempCessContSchema.getCessionMode())){
			tGetD+=" and  renewalflag<>'Y' and poltypeflag<>'1' ";

		}
    	tGetD+=" with ur ";

		LRInterfaceSet tLRInterfaceSet=new LRInterfaceSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRInterfaceSet, tGetD);
        try {
            do {
                rswrapper.getData();
                
        for(int i=1;i<=tLRInterfaceSet.size();i++){
        	
        	LRInterfaceSchema tLRInterfaceSchema=tLRInterfaceSet.get(i);
        	if(tLRInterfaceSchema.getContType().equals("2")
            		&&(!mGrpPolNo.equals(tLRInterfaceSchema.getGrpPolNo())
            		||(mGrpPolNo.equals(tLRInterfaceSchema.getGrpPolNo())
            				&&!mPayNo.equals(tLRInterfaceSchema.getPayNo())))){
            	tFlag="1";
            	mGrpPolNo=tLRInterfaceSchema.getGrpPolNo();
            	mPayNo=tLRInterfaceSchema.getPayNo();
            }
            else
            	tFlag="0";
        	if(!dealTempCont(tLRInterfaceSchema,tLRTempCessContSchema,con)){
        		continue;
    		}
        	
        }
            }
            while (tLRInterfaceSet.size() > 0);
            rswrapper.close();
            }
         catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
     
    	return true;
    }
    private boolean cessComNData(
    		LRTempCessContSchema tLRTempCessContSchema,Connection con){

    	
    	String tCSql="";
    	
		tCSql="select 1 from lrpol a,lrinterface b where a.polno=b.polno and b.state='2' and b.polstat='3'  " +
				"and  (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then a.contno else a.grpcontno end )='"+tLRTempCessContSchema.getContno()+"' fetch first 1 rows only with ur ";
		
		SSRS tCSSRS = new ExeSQL().execSQL(tCSql);
	
		if(tCSSRS!=null && tCSSRS.MaxRow>0){
			if(!insertOFC(tLRTempCessContSchema))//反冲普通合同数据
				return false;
		}
		
		String tGetD="select * from lrinterface  where  state='2' and polstat='3'  " +
		"and  (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' and dutydate<='"+mToday+"' and renewalflag='N'  ";
		if("2".equals(tLRTempCessContSchema.getCessionMode())){
			tGetD+="  and poltypeflag<>'1' ";

		}
    	tGetD+=" with ur ";
		LRInterfaceSet tLRInterfaceSet=new LRInterfaceSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRInterfaceSet, tGetD);
        try {
            do {
                rswrapper.getData();
                
        for(int i=1;i<=tLRInterfaceSet.size();i++){
        	
        	LRInterfaceSchema tLRInterfaceSchema=tLRInterfaceSet.get(i);
        	if(!dealTempCont(tLRInterfaceSchema,tLRTempCessContSchema,con)){
        		continue;
    		}
        	
        }
            }
            while (tLRInterfaceSet.size() > 0);
            rswrapper.close();
            }
         catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
         
 			
        	 String tGetX="select * from lrinterface  where  state='2' and polstat='3' " +
        	 		"and  grpcontno='"+tLRTempCessContSchema.getContno()+"' and dutydate<='"+mToday+"' and renewalflag<>'N'  ";
        	 if("2".equals(tLRTempCessContSchema.getCessionMode())){
        		 tGetX+="  and renewalflag<>'Y' and poltypeflag<>'1' ";

     		}
        	 tGetX+=" with ur ";

        	 LRInterfaceSet tXLRInterfaceSet=new LRInterfaceSet();
        	 RSWrapper rXswrapper = new RSWrapper();
        	 rXswrapper.prepareData(tXLRInterfaceSet, tGetX);
        	 try {
        		 do {
        			 rXswrapper.getData();
                 
        			 for(int i=1;i<=tXLRInterfaceSet.size();i++){
         	
        				 LRInterfaceSchema tLRInterfaceSchema=tXLRInterfaceSet.get(i);
        				 if(!dealTempCont(tLRInterfaceSchema,tLRTempCessContSchema,con)){
        					 continue;
        				 }
         	
        			 }
        		 }
        		 while (tXLRInterfaceSet.size() > 0);
        		 rXswrapper.close();
             	}
        	 catch (Exception ex) {
        		 ex.printStackTrace();
        		 rXswrapper.close();
        	 }
         
    	
     
    	
    
    	return true;
    }
    
    private boolean dealTempCont(LRInterfaceSchema tLRInterfaceSchema,LRTempCessContSchema tLRTempCessContSchema,Connection con){
    	
    	//判断是否首期
    	
        String agentGroup = tLRInterfaceSchema.getAgentGroup();
        String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                  +agentGroup+"' )with ur";
        String CostCenter = new ExeSQL().getOneValue(sql);
        String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tLRInterfaceSchema.getRiskCode()+"' with ur";
        String LRiskFlag = new ExeSQL().getOneValue(aSql);
        LCPolDB tLCPolDB=new LCPolDB(con);
        tLCPolDB.setPolNo(tLRInterfaceSchema.getPolNo());
        tLCPolDB.setRenewCount(tLRInterfaceSchema.getReNewCount());
       
     
        
       
         String tCode=tLRInterfaceSchema.getRiskCode();
         double tRiskAmnt=0.00;//风险保额
         double tRealCessRate=0.00;//真实分保比例
         double tCessAmnt=0.00;//分保保额
        
        	 
         
         String tCalRiskAmnt="RC017";//获取风险保额计算公式编码，如果没有描述对应公式则该字段存0
        
        	 double tCash=0.00;//现金价值
        	 
             double tcheck=calculatorRiskAmnt(tCalRiskAmnt,tLRInterfaceSchema,tCash,
            		 tCode,tLRInterfaceSchema.getDutyDate());//调用风险保额计算函数
             if(tcheck==-1){//计算错误继续下一合同计算
            	 return false;
             }
             else{
            	 tRiskAmnt=tcheck;
             }
       
         
         
        	 if("Y".equals(tLRInterfaceSchema.getRenewalFlag())){//如果是非首期保单则直接从原有数据中查出分保比例
        		 String sqlrate = "select deadaddfeerate from lrpol where " +
        		 		"polno='"+tLRInterfaceSchema.getPolNo()+"' " +
        		 				" and recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
        		 						"fetch first 1 rows only with ur";
        		 SSRS tSSRS = new ExeSQL().execSQL(sqlrate);
        		 if(tSSRS!=null && tSSRS.MaxRow>0){
        			 String tRate = tSSRS.GetText(1, 1);
            		 tRealCessRate=Double.parseDouble(tRate);
                }
        		 else        		 return false;
        		
        	 }
        	 else{
        	        	 
             double trcheck=calculatorRate(tLRInterfaceSchema,tLRTempCessContSchema,tCode,tRiskAmnt);//调用实际分保比例
             if(trcheck==-1){//计算错误或不具备分保条件继续下一合同计算
            	 return false;
             }
             else{
            	 tRealCessRate=trcheck;
             }
        	 }
        	 if(tRealCessRate<=0){
            	 this.errorLog(tLRInterfaceSchema.getPolNo(), "该保单分保比例小于等于0，为"+tRealCessRate);
            	 return false;
             }
        	 tCessAmnt=tRiskAmnt*tRealCessRate;
        
        
         
         /*
          * 如果是非首期数据成数分保判断是否需要插入年度化保费反冲数据
          * 因为首期年度化后还会取到续期数据，所以在取到这些续期数据时需要往lrpol表中插入负数据反冲
          */
         String tCessionMode=tLRTempCessContSchema.getCessionMode();
         mRiskCalSort = "4";
         if(tCessionMode.equals("1")){
        	 mRiskCalSort = "1";
        	 if(tLRInterfaceSchema.getContType().equals("1")||(tLRInterfaceSchema.getContType().equals("2")
        			))
        	 insertFC(tLRInterfaceSchema,tLRTempCessContSchema);
         }
         	
        
         
         mRecontCode = tLRTempCessContSchema.getTempContCode();
     
         
         LRPolSchema tLRPolSchema = new LRPolSchema();
         Reflections tReflections = new Reflections();
         tReflections.transFields(tLRPolSchema, tLRInterfaceSchema);
         
         //获取成本中心，财务接口使用。
         
         if(agentGroup != null&& !agentGroup.equals(""))
         {
             tLRPolSchema.setCostCenter(CostCenter);
         }
         prepareLRPolData(tLRPolSchema, tLRInterfaceSchema.getStandPrem(),
                 tLRInterfaceSchema.getPrem(),tRealCessRate);
         if(Integer.parseInt(LRiskFlag)>0){
             tLRPolSchema.setReNewCount(tLRInterfaceSchema.getPolYear());
         }
         else{
             tLRPolSchema.setReNewCount(tLRInterfaceSchema.getReNewCount());
         }
         tLRPolSchema.setPayNo(tLRInterfaceSchema.getPayNo());
         LRPolDB tLRPolDB= new LRPolDB(con);
         tLRPolDB.setSchema(tLRPolSchema);
         tLRPolDB.setCessAmnt(tCessAmnt);
         tLRPolDB.setReType(tLRTempCessContSchema.getReType());
         if("1".equals(tLRTempCessContSchema.getCessionMode())&&"Y".equals(tLRInterfaceSchema.getRenewalFlag())){
        	 tLRPolDB.setPolStat("2");//临分非首期标志
         }
         else if("1".equals(tLRTempCessContSchema.getCessionMode())&&"-".equals(tLRInterfaceSchema.getRenewalFlag())){
        	 tLRPolDB.setPolStat("8");//临分有名单保全增人
         }
         else{
        	 tLRPolDB.setPolStat("7");//临分首期数据
         }
      
         if(tLRInterfaceSchema.getRiskCode().equals("5601")&&
        		 (tLRInterfaceSchema.getOccupationType()==null||
        				 tLRInterfaceSchema.getOccupationType().equals("")||
        				 tLRInterfaceSchema.getOccupationType().equals("0"))){
        	 tLRPolDB.setOccupationType("1");
         }
         
         tLRPolDB.delete();
         tLRPolDB.insert();
        
         
     
     
    	 LRInterfaceDB tLRInterfaceDB= new LRInterfaceDB(con);
    	 tLRInterfaceDB.setSchema(tLRInterfaceSchema);
         tLRInterfaceDB.setState("1");
         tLRInterfaceDB.update();
     
    	return true;
    }
    //在成数续期时插入首期保费的负记录，以计算出续期反冲数据
    private boolean insertFC(LRInterfaceSchema tLRInterfaceSchema,LRTempCessContSchema tLRTempCessContSchema){
        
        if(tLRTempCessContSchema.getCessionMode().equals("1")&&tLRInterfaceSchema.getReNewCount()==0&&"Y".equals(tLRInterfaceSchema.getRenewalFlag())&&
        		tLRInterfaceSchema.getPayIntv()!=0&&tLRInterfaceSchema.getPayIntv()!=12){
        	String tRe="";
        	
        	 tRe="select count(1) from lrpol" +
			" where recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
					" and getdatadate<='"+this.mToday+"' and renewcount=0 and ReinsureItem='C' ";
			if("2".equals(tLRTempCessContSchema.getContType()))
			{
				tRe+=" and GrpPolNo='"+tLRInterfaceSchema.getGrpPolNo()+"' with ur "; 
			}
			else if("1".equals(tLRTempCessContSchema.getContType()))
			{
				tRe+=" and polno='"+tLRInterfaceSchema.getPolNo()+"' with ur"; 
			}
        	
        	
        	SSRS tsRe = new ExeSQL().execSQL(tRe);
        	String tnRe=tsRe.GetText(1, 1);
        	if(Integer.parseInt(tnRe)>0){
        		String tChecksql1="";
        		
        			tChecksql1="select count(1) from lrpol" +
        			" where   recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
        					" and getdatadate='"+this.mToday+"' and renewcount=0 and ReinsureItem='F' with ur";
        		
        			if("2".equals(tLRTempCessContSchema.getContType()))
        			{
        				tChecksql1+=" and GrpPolNo='"+tLRInterfaceSchema.getGrpPolNo()+"' with ur "; 
        			}
        			else if("1".equals(tLRTempCessContSchema.getContType()))
        			{
        				tChecksql1+=" and polno='"+tLRInterfaceSchema.getPolNo()+"' with ur"; 
        			}
        			
        	SSRS tCheck1 = new ExeSQL().execSQL(tChecksql1);
        	String tcheck1=tCheck1.GetText(1, 1);
        	
        if(Integer.parseInt(tcheck1)==0){
        	
        	
        	
        		String str1="";
        		
        			        		
        		str1="select GrpContNo,GrpPolNo," +
        		"ContNo,PolNo,ProposalNo,PrtNo," +
        		"ReContCode,'F','Y'," +
        		"ReNewCount,ContType,ManageCom,RiskCalSort,MainPolNo," +
        		"MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
        		"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate," +
        		"EndDate,StandPrem,-prem,SumPrem,Amnt,InsurePeriod,OccupationType," +
        		"RiskAmnt,PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear," +
        		"InsuYearFlag,InsuYear,ProtItem,CessStart,CessEnd,'2',SignDate," +
        		"SumRiskAmount,NowRiskAmount,AppntNo,AppntName,AppntType,SaleChnl," +
        		"DeadAddFeeRate,DiseaseAddFeeRate,'"+this.mToday+"',Operator,current date,current time," +
        		"current date,current time,CostCenter,ReType,'','',CessAmnt,'"+mPayNo+"',contplancode,'"+tLRInterfaceSchema.getPolTypeFlag()+"' from lrpol a" +
        		" where (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' and riskcode='"+tLRInterfaceSchema.getRiskCode()+"'" +
				" and renewcount=0  and reinsureitem='C' and polstat='7' and " +
				" recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
				" and getdatadate=(select min(getdatadate) from lrpol where " +
				" (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' " +
				" and riskcode='"+tLRInterfaceSchema.getRiskCode()+"' and renewcount=0 and " +
				" recontcode='"+tLRTempCessContSchema.getTempContCode()+"')";
        		
        		ExeSQL tExeSQL = new ExeSQL();
        		tExeSQL.execUpdateSQL("insert into lrpol(GrpContNo,GrpPolNo,ContNo," +
            		"PolNo,ProposalNo,PrtNo,ReContCode,ReinsureItem," +
            		"TempCessFlag,ReNewCount,ContType,ManageCom,RiskCalSort," +
            		"MainPolNo,MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
            		"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate,EndDate," +
            		"StandPrem,Prem,SumPrem,Amnt,InsurePeriod,OccupationType,RiskAmnt," +
            		"PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear,InsuYearFlag,InsuYear," +
            		"ProtItem,CessStart,CessEnd,PolStat,SignDate,SumRiskAmount,NowRiskAmount,AppntNo," +
            		"AppntName,AppntType,SaleChnl,DeadAddFeeRate,DiseaseAddFeeRate,GetDataDate,Operator," +
            		"MakeDate,MakeTime,ModifyDate,ModifyTime,CostCenter,ReType,ActuGetState,ActuGetNo," +
            		"CessAmnt,payno,contplancode,poltypeflag) " +str1
            		);
        		
        	}
        	}
        }
        return true ;
    }
    
    private boolean insertOFC(LRTempCessContSchema tLRTempCessContSchema){
       
        			        		
        		try{
        			String ecsql="select 1 from lrpol where " +
        			" ReinsureItem  in ('CF','FF') " +
        					" and polno = (select polno from lrinterface where  polstat='3' " +
        		       				" and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' fetch first 1 rows only) fetch first 1 rows only";
        			SSRS tecSSRS = new ExeSQL().execSQL(ecsql);
        			if(tecSSRS==null || tecSSRS.MaxRow<=0){
        				
        				
        			String FCesssql="select GrpContNo,GrpPolNo," +
        		       		"ContNo,PolNo,ProposalNo,PrtNo," +
        		       		"ReContCode,case ReinsureItem when 'C' then 'CF' when 'F' then 'FF' when 'G' then 'GF' end,TempCessFlag," +
        		       		"ReNewCount,ContType,ManageCom,RiskCalSort,MainPolNo," +
        		       		"MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
        		       		"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate," +
        		       		"EndDate,StandPrem,-prem,SumPrem,Amnt,InsurePeriod,OccupationType," +
        		       		"RiskAmnt,PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear," +
        		       		"InsuYearFlag,InsuYear,ProtItem,CessStart,CessEnd,polstat,SignDate," +
        		       		"SumRiskAmount,NowRiskAmount,AppntNo,AppntName,AppntType,SaleChnl," +
        		       		"DeadAddFeeRate,DiseaseAddFeeRate,'"+this.mToday+"',Operator,current date,current time," +
        		       		"current date,current time,CostCenter,ReType,'','',-CessAmnt,(case a.payno when '00000000000'" +
        		       		" then (select distinct b.payno"+
              	       		" from lidatatransresult c, LIABORIGINALDATA b"+
        		       		" where c.accountcode = '6031000000'"+
        		       		" and c.accountdate = a.getdatadate"+  
        		       		" and (c.classtype in ('X-03', 'N-04', 'N-05') " +
        		       		"or (c.classtype in ('B-03','B-04') and b.feeoperationtype in ('NI'))) "+
        		       		" and c.standbystring3 = b.serialno"+
        		       		" and  b.grpcontno=a.grpcontno and"+
        		       		" b.grppolno=a.grppolno and "+
        		       		" c.riskcode=a.riskcode"+
        		       		" fetch first 1 rows only) else a.payno end),contplancode,poltypeflag " +
        		       		"from lrpol a" +
        		       		" where tempcessflag='N' and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' "+
	       				    " and polno in " +
		       				" (select polno from lrinterface where  polstat='3' and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"')" +
       						" and ReinsureItem not in ('CF','FF') and recontcode<>'"+tLRTempCessContSchema.getTempContCode()+"' with ur";
        		
        			ExeSQL tExeSQL = new ExeSQL();
        			tExeSQL.execUpdateSQL("insert into lrpol(GrpContNo,GrpPolNo,ContNo," +
        					"PolNo,ProposalNo,PrtNo,ReContCode,ReinsureItem," +
        					"TempCessFlag,ReNewCount,ContType,ManageCom,RiskCalSort," +
        					"MainPolNo,MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
        					"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate,EndDate," +
        					"StandPrem,Prem,SumPrem,Amnt,InsurePeriod,OccupationType,RiskAmnt," +
        					"PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear,InsuYearFlag,InsuYear," +
        					"ProtItem,CessStart,CessEnd,PolStat,SignDate,SumRiskAmount,NowRiskAmount,AppntNo," +
        					"AppntName,AppntType,SaleChnl,DeadAddFeeRate,DiseaseAddFeeRate,GetDataDate,Operator," +
        					"MakeDate,MakeTime,ModifyDate,ModifyTime,CostCenter,ReType,ActuGetState,ActuGetNo," +
        					"CessAmnt,payno,contplancode,poltypeflag) " +FCesssql
            		);
        			}
        			String eesql="select 1 from lrpoledor where " +
        			" ReinsureItem  in ('CF','FF') " +
					" and polno = (select polno from lrinterface where  polstat='3' " +
       				" and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' fetch first 1 rows only) fetch first 1 rows only";
        			SSRS teeSSRS = new ExeSQL().execSQL(eesql);
        			if(teeSSRS==null || teeSSRS.MaxRow<=0){
        			String FEdorsql="select GrpContNo,GrpPolNo," +
        		       		"ContNo,PolNo,EdorNo,ReContCode," +
        		       		"case ReinsureItem when 'C' then 'CF' when 'F' then 'FF' when 'G' then 'GF' end," +
        		       		"ReComCode," +
        					"RiskCalSort,MainPolNo,MasterPolNo,RiskCode,RiskVersion," +
        					"EdorValidate,NoPassDays,FeeoperationType,-GetMoney,GetConfDate,SumBackAmnt," +
        					"'"+this.mToday+"',Operator,ManageCom,current date,current time,current date," +
        					"current time,ReNewCount,CostCenter,ReType,ActuGetState,ActuGetNo" +
        		       		" from lrpoledor a" +
        		       		" where  (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' "+
        		       		"and polno in " +
		       				" (select polno from lrinterface where  polstat='3' and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"')" +
       						" and ReinsureItem not in ('CF','FF') and recontcode<>'"+tLRTempCessContSchema.getTempContCode()+"' " +
							" and not exists (select 1 from lrtempcesscont where tempcontcode=a.recontcode) with ur";
        		
        			ExeSQL tEdorSQL = new ExeSQL();
        			tEdorSQL.execUpdateSQL("insert into lrpoledor(GrpContNo,GrpPolNo,ContNo," +
        					"PolNo,EdorNo,ReContCode,ReinsureItem,ReComCode," +
        					"RiskCalSort,MainPolNo,MasterPolNo,RiskCode,RiskVersion," +
        					"EdorValidate,NoPassDays,FeeoperationType,GetMoney,GetConfDate,SumBackAmnt," +
        					"GetDataDate,Operator,ManageCom,MakeDate,MakeTime,ModifyDate," +
        					"ModifyTime,ReNewCount,CostCenter,ReType,ActuGetState,ActuGetNo) " +FEdorsql
            		);
        			}
        			String eclsql="select 1 from lrpolclm where " +
        			" ReinsureItem  in ('CF','FF') " +
        					" and polno = (select polno from lrinterface where  polstat='3' " +
        		       				" and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' fetch first 1 rows only) fetch first 1 rows only";
        			SSRS tclSSRS = new ExeSQL().execSQL(eclsql);
        			if(tclSSRS==null || tclSSRS.MaxRow<=0){
        			String FClmsql="select GrpContNo,GrpPolNo," +
		       		"ContNo,PolNo,ClmNo,RgtNo,CaseNo,ReContCode," +
		       		"case ReinsureItem when 'C' then 'CF' when 'F' then 'FF' when 'G' then 'GF' end," +
		       		"ReComCode," +
					"RiskCalSort,GetDutyKind,RiskCode,RiskVer,AccidentDate," +
					"ClmState,StandPay,-RealPay,ClmUWer,EndCaseDate,ReturnPay," +
					"AccountPeriod,RgtDate,ConfDate,ClaimMoney,UWFlag,AccDesc," +
					"CessPersonSug,LeaveHospDate,Diagnoses,Remark,'"+this.mToday+"',Operator," +
							"ManageCom,current date,current time,current date," +
        					"current time,GiveTypeDesc,ReNewCount,CostCenter,ReType,ActuGetState,ActuGetNo," +
        					"GetDutyCode " +
		       		" from lrpolclm a" +
		       		" where  (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' "+
       				" and polno in " +
       				" (select polno from lrinterface where  polstat='3' " +
       				" and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"')" +
					" and ReinsureItem not in ('CF','FF') and recontcode<>'"+tLRTempCessContSchema.getTempContCode()+"' " +
					" and not exists (select 1 from lrtempcesscont where tempcontcode=a.recontcode) with ur";
		
			ExeSQL tClmSQL = new ExeSQL();
			tClmSQL.execUpdateSQL("insert into lrpolclm(GrpContNo,GrpPolNo,ContNo," +
					"PolNo,ClmNo,RgtNo,CaseNo,ReContCode,ReinsureItem,ReComCode," +
					"RiskCalSort,GetDutyKind,RiskCode,RiskVer,AccidentDate," +
					"ClmState,StandPay,RealPay,ClmUWer,EndCaseDate,ReturnPay," +
					"AccountPeriod,RgtDate,ConfDate,ClaimMoney,UWFlag,AccDesc," +
					"CessPersonSug,LeaveHospDate,Diagnoses,Remark,GetDataDate,Operator," +
							"ManageCom,makedate,maketime,modifydate," +
        					"modifytime,GiveTypeDesc,ReNewCount,CostCenter,ReType,ActuGetState,ActuGetNo," +
        					"GetDutyCode) " +FClmsql
    		);
        		
        			}
        			}
        		 catch (Exception ex) {
                     ex.printStackTrace();
                     return false ;
                 }
        
        return true ;
    }

   
    
   
    
    /**
     * 合同校验传参函数
     * @param tcalCode
     * @param tPolNO
     * @return 查询数据库描述中的结果
     */
    private String calculatorReCont(String tcalCode,String tPolNO,String tDate){
    	Calculator tCalculator = new Calculator();
    	tCalculator.setCalCode(tcalCode);
    	String tresult="0";
    	String t="";
        //增加基本要素
      
    	tCalculator.addBasicFactor("PolNO", tPolNO);
    	tCalculator.addBasicFactor("mDate", tDate);
    	t = tCalculator.calculate();
        if (t == null ||
        		t.equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的合同");
                return tresult;
         }
        tresult=t;
        return tresult;
        
    }
    
    /**
     * 调用计算风险保额函数
     * @param tcalCode
     * @param tLRInterfaceSchema
     * @param tCash
     * @param tRetype
     * @param tCode
     * @return 风险保额
     */
    private double calculatorRiskAmnt(String tcalCode,LRInterfaceSchema tLRInterfaceSchema,
    		double tCash,String tCode,String tDate){
    	Calculator tCalculator = new Calculator();
    	tCalculator.setCalCode(tcalCode);
    	double tresult=-1;

        //增加基本要素
      
    	tCalculator.addBasicFactor("PolNO", tLRInterfaceSchema.getPolNo());
    	tCalculator.addBasicFactor("RiskCode", tLRInterfaceSchema.getRiskCode());
    	tCalculator.addBasicFactor("Cash", String.valueOf(tCash));
    	tCalculator.addBasicFactor("mDate", tDate);
    	
    	String t=tCalculator.calculate();
        if (t == null ||
        		t.equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的风险保额计算公式");
                return tresult;
         }
        
        tresult=Double.parseDouble(t);
        return tresult;
        
    }
    
    /**
     * 计算具体的分保比例
     * @param tcalCode
     * @param tLRInterfaceSchema
     * @param tLRContInfoSchema
     * @param tCode
     * @param tCessamnt
     * @return 分保比例
     */
    private double calculatorRate(LRInterfaceSchema tLRInterfaceSchema,
    		LRTempCessContSchema tLRTempCessContSchema,String tCode,double tRiskAmnt){
    	
    	double tresult=-1;
    	double tlimvalue=0.00;//最低再保保额
    	double tmaxvalue=0.00;//最高再保保额
    	double trate=0.00;//页面录入的分保比例
    	String sqlrate = "select calmode,retainamnt,cessionrate from LRTempCessContInfo where " +
 		"riskcode='"+tLRInterfaceSchema.getRiskCode()+"' " +
 				" and TempContCode='"+tLRTempCessContSchema.getTempContCode()+"' " +
 						"fetch first 1 rows only with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(sqlrate);
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		
    		
    		tlimvalue=Double.parseDouble(tSSRS.GetText(1, 1));//最低分保保额
        	tmaxvalue=Double.parseDouble(tSSRS.GetText(1, 2));//最高分保保额
        	trate=Double.parseDouble(tSSRS.GetText(1, 3));//页面录入分保比例
    	}
    	
        //增加基本要素
      
//    	tCalculator.addBasicFactor("PolNO", tLRInterfaceSchema.getPolNo());
//    	tCalculator.addBasicFactor("RiskAmnt", String.valueOf(tRiskAmnt));
//    	tCalculator.addBasicFactor("LowReAmnt", String.valueOf(tlimvalue));
//    	tCalculator.addBasicFactor("AutoLimit", String.valueOf(tmaxvalue));
//    	tCalculator.addBasicFactor("CessionRate", String.valueOf(trate));
//    	
//    	String t=tCalculator.calculate();
//        if (t == null ||
//        		t.equals("")) {
//                buildError("calCessFeeRate", "没有适用此保单的分保比例计算公式");
//                return tresult;
//         }
        
        //tresult=Double.parseDouble(t);
    	if(tRiskAmnt<=tlimvalue){
    		return tresult;
    	}
    	else if(tRiskAmnt>tlimvalue&&tRiskAmnt<=tmaxvalue){
    		tresult=(tRiskAmnt-tlimvalue)*trate/tRiskAmnt;
    	}
    	else if(tRiskAmnt>tmaxvalue){
    		tresult=(tmaxvalue-tlimvalue)*trate/tRiskAmnt;
    	}
    	else
    		return tresult;
        return tresult;
        
    }
    
    /**
     * 取合同要素值函数
     * @param strRiskCode
     * @param tReContCode
     * @param tFactorCode
     * @return 描述表schema
     */
    public LRTempCessContInfoSchema findCheckFieldByRiskCodeF(String strRiskCode, String tReContCode,String tFactorCode) {

        Hashtable hash = m_hashLMCheckField;
        String strPK = strRiskCode.trim() + tReContCode.trim()+tFactorCode.trim();
        Object obj = hash.get(strPK);

        if( obj != null ) {
          if( obj instanceof String ) {
            return null;
          } else {
            return (LRTempCessContInfoSchema)obj;
          }
        } else {
        	LRTempCessContInfoDB tLRTempCessContInfoDB = new LRTempCessContInfoDB();

        	tLRTempCessContInfoDB.setRiskCode(strRiskCode);
        	tLRTempCessContInfoDB.setTempContCode(tReContCode);
        	
        	LRTempCessContInfoSet tLRTempCessContInfoSet=tLRTempCessContInfoDB.query();
        	if(tLRTempCessContInfoSet.size()<=0){
        		hash.put(strPK, "NOFOUND");
        		return null;
        		
        	}
        	LRTempCessContInfoSchema tLRTempCessContInfoSchema = tLRTempCessContInfoSet.get(1);

          if( tLRTempCessContInfoDB.mErrors.needDealError() ) {
            mErrors.copyAllErrors(tLRTempCessContInfoDB.mErrors);
            hash.put(strPK, "NOFOUND");
            return null;
          }

          hash.put(strPK, tLRTempCessContInfoSchema);
          return tLRTempCessContInfoSchema;
        }
      }
    
   /**
    * 按要素取所需值
    * @param tRiskCode
    * @param tReContCode
    * @param tRetype
    * @return 要素值
    */
//    private double getFactorValue(String tRiskCode,String tReContCode,String tRetype) {
//       
//    	LRTempCessContInfoSchema tLRTempCessContInfoSchema
//    	=findCheckFieldByRiskCodeF(tRiskCode,tReContCode,tRetype);
//        
//        if (tLRTempCessContInfoSchema==null) {
//            return 0;
//        }
////        if (tLRTempCessContInfoSchema.getFactorValue() == null||tLRTempCessContInfoSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
////            return 0;
////        }
////        String t=tLRTempCessContInfoSchema.getFactorValue();
//        double tresult=Double.parseDouble(t);
//        return tresult;
//    }
   
    /**
     * 计算险种当年现金价值
     * @param cLCPolSet
     * @return tCash 保单现金价值
     */
    private double getCashValue(LCPolSet cLCPolSet)
    
    {
    	//特殊标签
    	double tCash=0.00;
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS = new SSRS();
    	SSRS ttSSRS = new SSRS();

    	//现价存在标志
    	boolean tFlag = false;
    	int j = 0;

    	//循环处理险种下的现金价值信息
    	for (int i = 1; i <= cLCPolSet.size(); i++)
    	{
    		String tRiskCode = cLCPolSet.get(i).getRiskCode();

        

    		//得到现金价值的算法描述
    		LMCalModeDB tLMCalModeDB = new LMCalModeDB();
    		//获取险种信息
    		tLMCalModeDB.setRiskCode(tRiskCode);
    		tLMCalModeDB.setType("X");
    		LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

    		//解析得到的SQL语句
    		String strSQL = "";
    		//如果描述记录唯一时处理
    		if (tLMCalModeSet.size() == 1)
    		{
    			strSQL = tLMCalModeSet.get(1).getCalSQL();
    		}
    		// 这个险种不需要取现金价值的数据
    		if (strSQL.equals(""))
    		{
    		}
    		else
    		{
    			tFlag = true;
    			
    			String tSql = "select riskname from lmriskapp where riskcode = '"
                    	+ cLCPolSet.get(i).getRiskCode() + "'";
    			tSSRS = tExeSQL.execSQL(tSql);

    			
    			j = j + 1;

    			Calculator calculator = new Calculator();
    			//设置基本的计算参数
    			calculator.addBasicFactor("ContNo", cLCPolSet.get(i)
    					.getContNo());
    			calculator.addBasicFactor("InsuredNo", cLCPolSet.get(i)
    					.getInsuredNo());
    			calculator.addBasicFactor("InsuredSex", cLCPolSet.get(i)
    					.getInsuredSex());
    			calculator.addBasicFactor("InsuredAppAge",  String
    					.valueOf(cLCPolSet.get(i).getInsuredAppAge()));
    			calculator.addBasicFactor("PayIntv", String.valueOf(cLCPolSet
    					.get(i).getPayIntv()));
    			calculator.addBasicFactor("PayEndYear", String
    					.valueOf(cLCPolSet.get(i).getPayEndYear()));
    			calculator.addBasicFactor("PayEndYearFlag", String
    					.valueOf(cLCPolSet.get(i).getPayEndYearFlag()));
    			calculator.addBasicFactor("PayYears", String.valueOf(cLCPolSet
    					.get(i).getPayYears()));
    			calculator.addBasicFactor("InsuYear", String.valueOf(cLCPolSet
    					.get(i).getInsuYear()));
    			calculator.addBasicFactor("Prem", String.valueOf(cLCPolSet.get(
    					i).getPrem()));
    			calculator.addBasicFactor("Amnt", String.valueOf(cLCPolSet.get(
    					i).getAmnt()));
    			calculator.addBasicFactor("FloatRate", String.valueOf(cLCPolSet
    					.get(i).getFloatRate()));
    			calculator.addBasicFactor("Mult", String.valueOf(cLCPolSet.get(
    					i).getMult()));
    			//add by yt 2004-3-10
    			calculator.addBasicFactor("InsuYearFlag", String
    					.valueOf(cLCPolSet.get(i).getInsuYearFlag()));
    			calculator.addBasicFactor("GetYear", String.valueOf(cLCPolSet
    					.get(i).getGetYear()));
    			calculator.addBasicFactor("GetYearFlag", String
    					.valueOf(cLCPolSet.get(i).getGetYearFlag()));
    			calculator.addBasicFactor("CValiDate", String.valueOf(cLCPolSet
    					.get(i).getCValiDate()));
    			calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
    			strSQL = calculator.getCalSQL();
    			String tyearsql="select year(date('"+mToday+"')-cvalidate) from lcpol where " +
    			"polno='"+cLCPolSet.get(i).getPolNo()+"'  with ur";
    	    	SSRS tSSRSY = new ExeSQL().execSQL(tyearsql);
    	    	int tyears=Integer.parseInt(tSSRSY.GetText(1, 1));
    			

    			System.out.println(strSQL);
    			//执行现金价值计算sql，获取现金价值列表
    				ttSSRS = tExeSQL.execSQL(strSQL);
    				if(ttSSRS.MaxRow>0)
    					if(tyears!=0)
    					tCash=Double.parseDouble(ttSSRS.GetText(tyears, 3));
    					System.out.println("********************"+tCash+"**************");
    					
    				
    		}
    }
   
    	return tCash;
    }
    /*针对新合同的分保保额的计算*/
    
   
    /**
     *
     * @return double
     */
    private boolean prepareLRPolData(LRPolSchema aLRPolSchema,
                                     double aStandPrem, double aPrem,double tRealCessRate) {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        aLRPolSchema.setReContCode(this.mRecontCode); //再保合同代码
        aLRPolSchema.setReinsureItem("C"); //再保项目 L--法定分保 C--商业分保
        aLRPolSchema.setTempCessFlag("Y"); //临分
        aLRPolSchema.setRiskCalSort(this.mRiskCalSort); //险种计算代码
        aLRPolSchema.setGetDataDate(this.mToday);
        aLRPolSchema.setOperator("Server");
        aLRPolSchema.setMakeDate(currentDate);
        aLRPolSchema.setMakeTime(currentTime);
        aLRPolSchema.setModifyDate(currentDate);
        aLRPolSchema.setModifyTime(currentTime);
        double tAddPremRate = getPolAddPremRate(aStandPrem, aPrem); //得到加费率,
        aLRPolSchema.setDiseaseAddFeeRate(tAddPremRate);
        aLRPolSchema.setDeadAddFeeRate(tRealCessRate);//该字段现在存储分保比例值

        return true;
    }

    /**
     * 计算死亡加费，重疾加费,这里由于契约不区分死亡加费和重疾加费，所以统一计算并存储于DiseaseAddFeeRate
     * @return double
     */
    private double getPolAddPremRate(double aStandPrem, double aPrem) {
        if (Math.abs(aStandPrem - 0.00) < 0.00001 || (aPrem - aStandPrem) <= 0) {
            return 0;
        }
        return (aPrem - aStandPrem) / aStandPrem;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(String aPolNo, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aPolNo);
        tLRErrorLogSchema.setLogType("2");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator("Server");
        tLRErrorLogSchema.setManageCom("86");
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }



    /**
     * 得到险种类别
     * @return boolean
     */
    private String getRiskCalSort(LRInterfaceSchema tLRInterfaceSchema, String aLRContCode) {
        SSRS tRS = new ExeSQL().execSQL("select retype from LRContInfo where ReContCode='"+aLRContCode+"'");
        if (tRS == null || tRS.getMaxRow() <= 0) {
            buildError("insertData", "没有该合同信息!");
        }
            SSRS tSSRS=new SSRS();
            String strSQL ="select distinct risksort from LRCalFactorValue where "+
            " ReContCode = '" + aLRContCode + "' with ur";
            tSSRS = new ExeSQL().execSQL(strSQL);

        if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
            buildError("insertData", "没有该险种的险种类别!");
            return "";
        }
        return tSSRS.GetText(1, 1);
    }

    /**
    * 获取责任的给付责任号
    * @param aLRPolSchema LRPolSchema
    * @return double
    */
   private String getGetDutyCode(LRContInfoSchema aLRContInfoSchema) {
       String strSQL =
               "select distinct(riskcode) from lrcalfactorvalue where  " +
               " recontcode='" + aLRContInfoSchema.getReContCode() +
               "' with ur";
       SSRS tSSRS = new ExeSQL().execSQL(strSQL);
       if (tSSRS.getMaxRow() <= 0) {
           return "-1";
       }
       if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
           return "-1";
       }
       return tSSRS.GetText(1, 1);
   }
   




 
    

    public String getResult() {
        return "没有传数据";
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }


    public static void main(String[] args) {
        LRNewGetCessDataBL tLRNewGetCessDataBL = new LRNewGetCessDataBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";
        String mOperateType = "CESSDATA";

        String startDate = "2008-10-10";
        String endDate = "2008-10-10";
        TransferData getCessData = new TransferData();
        getCessData.setNameAndValue("StartDate", startDate);
        getCessData.setNameAndValue("EndDate", endDate);

        VData tVData = new VData();
        tVData.addElement(globalInput);
        tVData.addElement(getCessData);

        tLRNewGetCessDataBL.submitData(tVData, mOperateType);
    }
}
