/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.Hashtable;

import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LRCalFactorValueSet;
import com.sinosoft.lis.vschema.LRCalModeSet;
import com.sinosoft.lis.vschema.LRContInfoSet;
import com.sinosoft.lis.vschema.LRDutySet;
import com.sinosoft.lis.vschema.LRInterfaceSet;

import java.util.*;

/*
 * <p>ClassName: LRNewGetCessDataBL </p>
 * <p>Description: 提取再保新单数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 孙宇
 * @CreateDate：2008-10-30
 */
public class LRGNewGetCessDataBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
//    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mReContCode = "";
   
    private String tFlag="";
    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private String mRecontCode; //再保合同代码
    private String mRiskCalSort = "";
    private Hashtable m_hashLMCheckField = new Hashtable();
    //业务处理相关变量
    /** 全局数据 */

    public LRGNewGetCessDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(String cOperate,String mToDay) {
        
        if (!getInputData(mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }
    
    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT(String cOperate,String mToDay) {
        
        if (!getInputData(mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

       
            if (!getCessData()) {
                return false;
            }
        
        return true;
    }

    /**
     * 提取分保数据
     * @return boolean
     */
    private boolean getCessData() {
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
       

        Connection con=DBConnPool.getConnection();
        try{
            //con.setAutoCommit(false);
           
                mToday = chgdate.getString(dbdate); //录入的正在统计的日期
                
                if (!insertPolData(con)) {
//                    buildError("getCessData", "提取" + mToday + "日新单数据出错");
                    return false;
                }
                
                con.commit();
            
            
            con.close();
        }catch(Exception e){
            try{
                e.printStackTrace();
                con.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 提取合同分保保单信息
     * @return boolean
     */
    private boolean insertPolData(Connection con) {
    	/**
    	 * 先判断是否为数据补提
    	 */
    	String tSql="";
    	
    		tSql="select * from lrinterface a where DutyDate='"+mToday+"' " +
    				"and  State='3' and polstat='1' order by grppolno,renewalflag with ur ";
    
    	
        
        
        LRInterfaceSet tLRInterfaceSet=new LRInterfaceSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRInterfaceSet, tSql);
        try {
            do {
                rswrapper.getData();
                
        for(int i=1;i<=tLRInterfaceSet.size();i++){
            LRInterfaceSchema tLRInterfaceSchema=tLRInterfaceSet.get(i);
            
           
           
          /**
           * 首先判断是否有临分合同的校验sql，如果该保单有临分合同数据则进入临分合同处理流程
           */
            String tTCsql="select 1 from lrpol where polno='"+tLRInterfaceSchema.getPolNo()+"' and " +
            		"tempcessflag='Y'";
            SSRS tTCSSRS = new ExeSQL().execSQL(tTCsql);
            if(tTCSSRS!=null && tTCSSRS.MaxRow>0){
            	//临分处理(单独写方法在主流程外)
            	continue;
            }
            else{
            	   		if (!cessComData(tLRInterfaceSchema,mToday,con,"","C")) {
                        continue;
                    }
                  	
            	
            }

         }
        }
        while (tLRInterfaceSet.size() > 0);
        rswrapper.close();
        }
     catch (Exception ex) {
        ex.printStackTrace();
        rswrapper.close();
    }
        return true;
    }
    
    /**
     * 计算正常提取到的数据的分保提数处理
     * @param tLRInterfaceSchema
     * @param tDate
     * @param con
     * @return boolean
     */
    private boolean cessComData(LRInterfaceSchema tLRInterfaceSchema,String tDate,Connection con,String mReContCode,String tType){
    	
    	LRPolDB tLRPolDB= new LRPolDB(con);
    	LRInterfaceDB tLRInterfaceDB= new LRInterfaceDB(con);
        Reflections tReflections = new Reflections();
        
    	//判断是否首期
    	String tReflag=tLRInterfaceSchema.getRenewalFlag();
    	String tContDte=tDate;//判断属于哪个合同范围的时间变量
    	String strSql="";
    	
    	 strSql = "select * from LRContInfo where  ReContState='01' and (ReType='01' and exists " +
         " (select 1 from LRcalfactorvalue where Recontcode=LRContInfo.Recontcode and riskcode='" +
         tLRInterfaceSchema.getRiskCode() +
         "')) and ((Rinvalidate is not null and '" +
         tContDte +
         "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
         tContDte + "' >= Rvalidate)) " +
         " and exists (select 1 from lmriskapp where riskcode='"+tLRInterfaceSchema.getRiskCode()+"') "+
         "order by Rvalidate desc with ur";
     
    	
     LRContInfoSet tLRContInfoSet = new LRContInfoDB().executeQuery(
             strSql);
     System.out.println("*********"+strSql+"************");
//   多家再保公司共保一个险种或责任，得到此针对险种的所有再保合同
     
     if (tLRContInfoSet == null || tLRContInfoSet.size() <= 0) {
         return false;
     }
     
     String agentGroup = tLRInterfaceSchema.getAgentGroup();
     String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                  +agentGroup+"' )with ur";
     String CostCenter = new ExeSQL().getOneValue(sql);
     String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tLRInterfaceSchema.getRiskCode()+"' with ur";
     String LRiskFlag = new ExeSQL().getOneValue(aSql);
    
     boolean tStateFlag=false;//是否更新中间表的标志校验
     for (int j = 1; j <= tLRContInfoSet.size(); j++) {
         LRContInfoSchema tLRContInfoSchema = tLRContInfoSet.get(j);
         String tContNo = "";
         //增加对已不需分保保单的校验
         tContNo = tLRInterfaceSchema.getGrpContNo();
    	 String noLongerSql = "select 1 from LRNoLongerCont where  " +
    	 						"recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
 		 						" and contno='"+tContNo+"' fetch first 1 rows only with ur";
 		 SSRS tSSRSNL = new ExeSQL().execSQL(noLongerSql);
 		 if(tSSRSNL!=null && tSSRSNL.getMaxRow()>0){
 			 continue;
         }
         String tCessionMode=tLRContInfoSchema.getCessionMode();
         //溢额合同不处理续期及无名单的数据
         if(tCessionMode.equals("2")){
        	 continue;
         }
         String tRetype=tLRContInfoSchema.getReType();
         String tCode="";
         
         double tRealCessRate=0.00;//真实分保比例
         double tCessAmnt=0.00;//分保保额
        
        	 tCode=tLRInterfaceSchema.getRiskCode();
        
         String tCalCont = getCalCode(tLRContInfoSchema,tCode,"C","recont");//获取合同校验的校验编码
         //取得到的合同是否有特殊判断条件
//         if(!"0".equals(tCalCont)){
//             String tcheck=calculatorReCont(tCalCont,tLRInterfaceSchema.getPolNo(),tDate);//调用校验函数，当不符合该合同条件时候返回“-1”
//             if("0".equals(tcheck)){//不符合条件继续处理下个合同
//            	 continue;
//             }
//         }
         
         
         
//         String tCalRate=getCalCode(tLRContInfoSchema,tCode,"C","rate");//获取实际分保比例计算公式编码，如果没有描述对应公式则该字段存0
//         if(!"0".equals(tCalRate)){
        	 
        		 String sqlrate = "select DeadAddFeeRate from lrpol where DeadAddFeeRate>0 and " +
        		 		"grppolno='"+tLRInterfaceSchema.getGrpPolNo()+"' " +
        		 				" and recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
        		 						"fetch first 1 rows only with ur";
        		 SSRS tSSRS = new ExeSQL().execSQL(sqlrate);
        		 if(tSSRS!=null && tSSRS.MaxRow>0){
        			 String tRate = tSSRS.GetText(1, 1);
            		 tRealCessRate=Double.parseDouble(tRate);
                }
        		 else continue;
        		
        	 
        	
        	 tCessAmnt=0;
         //}
         
         if(tRealCessRate<=0){
        	 this.errorLog(tLRInterfaceSchema.getPolNo(), "该保单分保比例小于等于0，为"+tRealCessRate);
        	 continue;
         }
        
       
         	
        
         
         mRecontCode = tLRContInfoSchema.getReContCode();
     
         mRiskCalSort = getRiskCalSort(tLRInterfaceSchema,
                                       mRecontCode);
         LRPolSchema tLRPolSchema = new LRPolSchema();
         tReflections.transFields(tLRPolSchema, tLRInterfaceSchema);
         
         //获取成本中心，财务接口使用。
         
         if(agentGroup != null&& !agentGroup.equals(""))
         {
             tLRPolSchema.setCostCenter(CostCenter);
         }
         prepareLRPolData(tLRPolSchema, tLRInterfaceSchema.getStandPrem(),
                 tLRInterfaceSchema.getPrem(),tRealCessRate);
         if(Integer.parseInt(LRiskFlag)>0){
             tLRPolSchema.setReNewCount(tLRInterfaceSchema.getPolYear());
         }
         else{
             tLRPolSchema.setReNewCount(tLRInterfaceSchema.getReNewCount());
         }
         tLRPolDB.setSchema(tLRPolSchema);
         tLRPolDB.setCessAmnt(tCessAmnt);
         tLRPolDB.setRiskAmnt("0");
         tLRPolDB.setReType(tLRContInfoSchema.getReType());
         if("1".equals(tLRContInfoSchema.getCessionMode())&&"X".equals(tLRInterfaceSchema.getRenewalFlag())){
        	 tLRPolDB.setPolStat("4");//非首期标志
         }
         else{
        	 tLRPolDB.setPolStat("7");//首期或者续保数据
         }
      
         if(tLRInterfaceSchema.getRiskCode().equals("5601")&&
        		 (tLRInterfaceSchema.getOccupationType()==null||
        				 tLRInterfaceSchema.getOccupationType().equals("")||
        				 tLRInterfaceSchema.getOccupationType().equals("0"))){
        	 tLRPolDB.setOccupationType("1");
         }
         
         tLRPolDB.delete();
         tLRPolDB.insert();
         tStateFlag=true;//只要有保单满足一个再保合同，则将tStateFlag置为true
         
     }
     if(tStateFlag){
    	 tLRInterfaceDB.setSchema(tLRInterfaceSchema);
         tLRInterfaceDB.setState("1");
         tLRInterfaceDB.update();
     }
     
    	return true;
    }
   

    /**
     * 获取lrcalmode中计算编码的方法
     * @param strRiskCode 险种号
     * @param tReContCode 合同号
     * @param tReinsuritem 合同类型 ：正常，临分
     * @return tLRCalModeSchema
     */
    public LRCalModeSchema findCheckFieldByRiskCode(String strRiskCode, String tReContCode,String tReinsuritem) {

        Hashtable hash = m_hashLMCheckField;
        String strPK = strRiskCode.trim() + tReContCode.trim()+tReinsuritem.trim();
        Object obj = hash.get(strPK);

        if( obj != null ) {
          if( obj instanceof String ) {
            return null;
          } else {
            return (LRCalModeSchema)obj;
          }
        } else {
        	LRCalModeDB tLRCalModeDB = new LRCalModeDB();

        	tLRCalModeDB.setRiskCode(strRiskCode);
        	tLRCalModeDB.setReContCode(tReContCode);
        	//tLRCalModeDB.setFactorCode(tFactorCode);
        	LRCalModeSet tLRCalModeSet=tLRCalModeDB.query();
        	if(tLRCalModeSet.size()<=0){
        		hash.put(strPK, "NOFOUND");
        		return null;
        		
        	}
        	LRCalModeSchema tLRCalModeSchema = tLRCalModeSet.get(1);

          if( tLRCalModeDB.mErrors.needDealError() ) {
            mErrors.copyAllErrors(tLRCalModeDB.mErrors);
            hash.put(strPK, "NOFOUND");
            return null;
          }

          hash.put(strPK, tLRCalModeSchema);
          return tLRCalModeSchema;
        }
      }
    
    /**
     * 保单是否符合合同条件的方法
     * @param aLRPolSchema
     * @return CALCODE 计算编码
     */
    private String getCalCode(LRContInfoSchema tLRContInfoSchema,String tCode,String tReinsuritem,String tCalCode) {
    	
    	String t="0";
    	LRCalModeSchema tLRCalModeSchema
    	=findCheckFieldByRiskCode(tCode,tLRContInfoSchema.getReContCode(),tReinsuritem);
        
        if (tLRCalModeSchema==null) {
            return t;
        }
        if ((tLRCalModeSchema.getNegoCalCode() == null||tLRCalModeSchema.getNegoCalCode().equals("null")
        		||tLRCalModeSchema.getNegoCalCode().equals(""))&&"recont".equals(tCalCode)) {
            return t;
        }
        else if ((tLRCalModeSchema.getRiskAmntCalCode() == null||tLRCalModeSchema.getRiskAmntCalCode().equals("null")
        		||tLRCalModeSchema.getRiskAmntCalCode().equals(""))&&"riskamnt".equals(tCalCode)) {
            return t;
        }
        else if ((tLRCalModeSchema.getCessCommCalCode() == null||tLRCalModeSchema.getCessCommCalCode().equals("null")
        		||tLRCalModeSchema.getCessCommCalCode().equals(""))&&"rate".equals(tCalCode)) {
            return t;
        }
        if("recont".equals(tCalCode))//再保合同校验
        	t=tLRCalModeSchema.getNegoCalCode();
        else if("riskamnt".equals(tCalCode))//取风险保额计算公式
        	t=tLRCalModeSchema.getRiskAmntCalCode();
        else if("rate".equals(tCalCode))//取分保比例计算公式
        	t=tLRCalModeSchema.getCessCommCalCode();
        return t;
    }
    
   
    
   
    
   
    
    /**
     * 取合同要素值函数
     * @param strRiskCode
     * @param tReContCode
     * @param tFactorCode
     * @return 描述表schema
     */
    public LRCalFactorValueSchema findCheckFieldByRiskCodeF(String strRiskCode, String tReContCode,String tFactorCode) {

        Hashtable hash = m_hashLMCheckField;
        String strPK = strRiskCode.trim() + tReContCode.trim()+tFactorCode.trim();
        Object obj = hash.get(strPK);

        if( obj != null ) {
          if( obj instanceof String ) {
            return null;
          } else {
            return (LRCalFactorValueSchema)obj;
          }
        } else {
        	LRCalFactorValueDB tLRCalFactorValueDB = new LRCalFactorValueDB();

        	tLRCalFactorValueDB.setRiskCode(strRiskCode);
        	tLRCalFactorValueDB.setReContCode(tReContCode);
        	tLRCalFactorValueDB.setFactorCode(tFactorCode);
        	LRCalFactorValueSet tLRCalFactorValueSet=tLRCalFactorValueDB.query();
        	if(tLRCalFactorValueSet.size()<=0){
        		hash.put(strPK, "NOFOUND");
        		return null;
        		
        	}
          LRCalFactorValueSchema tLRCalFactorValueSchema = tLRCalFactorValueSet.get(1);

          if( tLRCalFactorValueDB.mErrors.needDealError() ) {
            mErrors.copyAllErrors(tLRCalFactorValueDB.mErrors);
            hash.put(strPK, "NOFOUND");
            return null;
          }

          hash.put(strPK, tLRCalFactorValueSchema);
          return tLRCalFactorValueSchema;
        }
      }
    
   
   
   
    /*针对新合同的分保保额的计算*/
    
   
    /**
     *
     * @return double
     */
    private boolean prepareLRPolData(LRPolSchema aLRPolSchema,
                                     double aStandPrem, double aPrem,double tRealCessRate) {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        aLRPolSchema.setReContCode(this.mRecontCode); //再保合同代码
        aLRPolSchema.setReinsureItem("G"); //再保项目 L--法定分保 C--商业分保
        aLRPolSchema.setTempCessFlag("N"); //非临分
        aLRPolSchema.setRiskCalSort(this.mRiskCalSort); //险种计算代码
        aLRPolSchema.setGetDataDate(this.mToday);
        aLRPolSchema.setOperator("Sever");
        aLRPolSchema.setMakeDate(currentDate);
        aLRPolSchema.setMakeTime(currentTime);
        aLRPolSchema.setModifyDate(currentDate);
        aLRPolSchema.setModifyTime(currentTime);
        double tAddPremRate = getPolAddPremRate(aStandPrem, aPrem); //得到加费率,
        aLRPolSchema.setDiseaseAddFeeRate(tAddPremRate);
        aLRPolSchema.setDeadAddFeeRate(tRealCessRate);//该字段现在存储分保比例值

        return true;
    }

    /**
     * 计算死亡加费，重疾加费,这里由于契约不区分死亡加费和重疾加费，所以统一计算并存储于DiseaseAddFeeRate
     * @return double
     */
    private double getPolAddPremRate(double aStandPrem, double aPrem) {
        if (Math.abs(aStandPrem - 0.00) < 0.00001 || (aPrem - aStandPrem) <= 0) {
            return 0;
        }
        return (aPrem - aStandPrem) / aStandPrem;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(String aPolNo, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aPolNo);
        tLRErrorLogSchema.setLogType("2");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator("Sever");
        tLRErrorLogSchema.setManageCom("86");
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }



    /**
     * 得到险种类别
     * @return boolean
     */
    private String getRiskCalSort(LRInterfaceSchema tLRInterfaceSchema, String aLRContCode) {
        SSRS tRS = new ExeSQL().execSQL("select retype from LRContInfo where ReContCode='"+aLRContCode+"'");
        if (tRS == null || tRS.getMaxRow() <= 0) {
            buildError("insertData", "没有该合同信息!");
        }
            SSRS tSSRS=new SSRS();
            String strSQL ="select distinct risksort from LRCalFactorValue where "+
            " ReContCode = '" + aLRContCode + "' with ur";
            tSSRS = new ExeSQL().execSQL(strSQL);

        if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
            buildError("insertData", "没有该险种的险种类别!");
            return "";
        }
        return tSSRS.GetText(1, 1);
    }

  




    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
   private boolean getInputData(String mToDay) {
       
       mStartDate = mToDay;
       
       return true;
   }

  
 

    public String getResult() {
        return "没有传数据";
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }


    public static void main(String[] args) {
        LRNewGetCessDataBL tLRNewGetCessDataBL = new LRNewGetCessDataBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";
        String mOperateType = "CESSDATA";

        String startDate = "2008-10-10";
        String endDate = "2008-10-10";
        TransferData getCessData = new TransferData();
        getCessData.setNameAndValue("StartDate", startDate);
        getCessData.setNameAndValue("EndDate", endDate);

        VData tVData = new VData();
        tVData.addElement(globalInput);
        tVData.addElement(getCessData);

        tLRNewGetCessDataBL.submitData(tVData, mOperateType);
    }
}
