package com.sinosoft.lis.reinsure;

import java.util.Date;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRCessListSchema;
import com.sinosoft.lis.schema.LRClaimVerifySchema;
import com.sinosoft.lis.vschema.LRCessListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: LRClaimVeriBL </p>
 * <p>Description: 提取理赔数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 
 * @CreateDate：2008-10-30
 */
public class LRClaimVeriBL {
	


		    /** 错误处理类，每个需要错误处理的类中都放置该类 */
		    public CErrors mErrors = new CErrors();

		    /** 前台传入的公共变量 */
		    private GlobalInput globalInput = new GlobalInput();


		    /** 往后面传输数据的容器 */
//		    private VData mInputData = new VData();

		    /** 数据操作字符串 */
		    private String strOperate = "";
		    private String mToday = "";
		    private String mStartDate = "";
		    private String mEndDate = "";

		    private TransferData mGetCessData = new TransferData();

		    private MMap tmap = new MMap();
		    private VData mOutputData = new VData();
		    private LRCessListSet mLRCessListSet = new LRCessListSet();


		    //业务处理相关变量
		    /** 全局数据 */

		    public LRClaimVeriBL() {
		    }

		    /**
		     * 提交数据处理方法
		     * @param cInputData 传入的数据,VData对象
		     * @param cOperate 数据操作字符串
		     * @return 布尔值（true--提交成功, false--提交失败）
		     */
		    public boolean submitData(VData cInputData, String cOperate) {
		        System.out.println("Begin LRCessListBL.Submit..............");
		       
		        if (!getInputData(cInputData)) {
		            return false;
		        }
		        if (!dealData()) {
		            return false;
		        }
		        //准备往后台的数据
		        if (!prepareOutputData())
		        {
		            return false;
		        }

		        PubSubmit tPubSubmit = new PubSubmit();
		        tPubSubmit.submitData(mOutputData, "");
		        //如果有需要处理的错误，则返回
		        if (tPubSubmit.mErrors.needDealError())
		        {
		            // @@错误处理
		            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
		            CError tError = new CError();
		            tError.moduleName = "LRCessListBL";
		            tError.functionName = "submitDat";
		            tError.errorMessage = "数据提交失败!";
		            this.mErrors.addOneError(tError);
		            return false;
		        }
		        
		        return true;
		    }


		    /**
		     * 根据前面的输入数据，进行UI逻辑处理
		     * 如果在处理过程中出错，则返回false,否则返回true
		     */
		    private boolean dealData() {

		       
		            if (!getCessData()) {
		                return false;
		            }
		        
		        return true;
		    }

		    /**
		     * 提取分保数据
		     * @return boolean
		     */
		    private boolean getCessData() {
		        FDate chgdate = new FDate();
		        
		        try{		           
		            
		               
		                if (!insertPolData()) {
		                    buildError("getCessData", "提取" + mToday + "日新单数据出错");
		                    return false;
		                }
		            
		            
		            
		        }catch(Exception e){
		                e.printStackTrace();
		        }

		        return true;
		    }
		    
		     /**
		     * 准备后台的数据
		     * @return boolean
		     */
		    private boolean prepareOutputData()
		    {
		        try
		        {
//		        	tmap.put(this.mLRCessListSet, "INSERT");
		        	this.mOutputData.clear();
		            this.mOutputData.add(tmap);
		        }
		        catch (Exception ex)
		        {
		            // @@错误处理
		            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
		            return false;
		        }
		        return true;
		    }
		    //执行SQL,并将取出的数据插入到:
		    private boolean insertPolData(){
		    	ExeSQL tExeSQL = new ExeSQL();
        		tExeSQL.execUpdateSQL("delete from LRClaimVerify with ur " 
            		);
		    	String ttSqlt = "select calsql,tablename,calcode from LRGetDataClassDef where  dealclass='C'  order by ordernum with ur";
                ExeSQL tESQLt = new ExeSQL();
                SSRS tSSRS = new SSRS();
                tSSRS = tESQLt.execSQL(ttSqlt);
                if(tSSRS!=null && tSSRS.MaxRow>0){
                    for(int index=1;index<=tSSRS.MaxRow;index++){
                        String exeSql="";
                        PubCalculator tPubCalculator = new PubCalculator();
                        tPubCalculator.addBasicFactor("StartDate",mStartDate);
                        
                        tPubCalculator.addBasicFactor("EndDate", mEndDate);
                         
                        tPubCalculator.setCalSql(tSSRS.GetText(index, 1));
                        exeSql = tPubCalculator.calculateEx();
                        System.out.println(tSSRS.GetText(index, 3)+":::"+exeSql);
                        //SSRS teSSRS = new ExeSQL().execSQL(exeSql);
                        if(insertData(exeSql)==false){
                            return false;
                        }
                    }
                    
                }
                String tuSqlt = "select calsql,tablename,calcode from LRGetDataClassDef where  dealclass='CU'  order by ordernum with ur";
                ExeSQL tuESQLt = new ExeSQL();
                SSRS tuSSRS = new SSRS();
                tuSSRS = tuESQLt.execSQL(tuSqlt);
                if(tuSSRS!=null && tuSSRS.MaxRow>0){
                    for(int index=1;index<=tuSSRS.MaxRow;index++){
                        String exeSql="";
                        PubCalculator tPubCalculator = new PubCalculator();
                       
                         
                        tPubCalculator.setCalSql(tuSSRS.GetText(index, 1));
                        exeSql = tPubCalculator.calculateEx();
                        System.out.println(tSSRS.GetText(index, 3)+":::"+exeSql);
                        tuESQLt.execUpdateSQL(exeSql); 
                		
                    }
                    
                }
		    	return true;
		    }
		    
		    //错误信息
		    private void buildError(String szFunc, String szErrMsg) {
		        CError cError = new CError();
		        cError.moduleName = "LRCessListBL";
		        cError.functionName = szFunc;
		        cError.errorMessage = szErrMsg;
		        this.mErrors.addOneError(cError);
		    }

		    private boolean insertData(String sqlC ){
		    	try{

		        	
		        	SSRS tSSRS = new SSRS();
		        	RSWrapper rsWrapper = new RSWrapper();
		        	if (!rsWrapper.prepareData(null, sqlC))
		            {
		                System.out.println("数据准备失败! ");
		                return false;
		            }

		        	 do {
		             	tSSRS = rsWrapper.getSSRS();
		             	tmap = new MMap();
		     			if (tSSRS != null && tSSRS.MaxRow > 0) {

		        	
		        	for(int i=1;i<=tSSRS.MaxRow;i++){
		        		
		        		String aGrpContNo         = tSSRS.GetText(i,1);
		        		String aContNo            = tSSRS.GetText(i,2); 
		        		String aPolNo             = tSSRS.GetText(i,3);
//		        		String aClmNo             = tSSRS.GetText(i,4);
		        		String aRiskCode          = tSSRS.GetText(i,4); 
		        		String aCValiDate         = tSSRS.GetText(i,5);
		        		String aSignDate          = tSSRS.GetText(i,6); 
		        		String aReContState       = tSSRS.GetText(i,7); 
		        		String aRealPay       = tSSRS.GetText(i,8); 
		        		String aRePay       = tSSRS.GetText(i,9); 
		        		String aClaimBackFee       = tSSRS.GetText(i,10); 
		        		String aRate       = tSSRS.GetText(i,11); 
		        		String aDiffPay       = tSSRS.GetText(i,12); 
		        		String aResult       = "摊回金额与业务不符"; 
		        		String lrclmsql="select 1 from lrpolclm where " +
	        			" polno='"+aPolNo+"' fetch first 1 rows only with ur ";
	                	SSRS lcSSRS = new ExeSQL().execSQL(lrclmsql);
	                	if(lcSSRS==null || lcSSRS.MaxRow<=0){
	                		aResult="未提入lrpolclm";
	                	}
		        		
	                	else{
	                		String lrpolsql="select 1 from lrpol where " +
		        			" polno='"+aPolNo+"' fetch first 1 rows only with ur ";
		                	SSRS lrSSRS = new ExeSQL().execSQL(lrpolsql);
		                	if(lrSSRS==null || lrSSRS.MaxRow<=0){
		                		aResult="未提入lrpol";
		                	}
		                	else{
		                		String intersql="select 1 from lrinterface where " +
			        			" polno='"+aPolNo+"' fetch first 1 rows only with ur ";
			                	SSRS interSSRS = new ExeSQL().execSQL(intersql);
			                	if(interSSRS==null || interSSRS.MaxRow<=0){
			                		aResult="未提入中间表";
			                	}
		                	}
		                	
	                	}
	                	
		        		
		        		
		        		
		        		LRClaimVerifySchema tLRClaimVerifySchema = new LRClaimVerifySchema();
		        		
		        		tLRClaimVerifySchema.setRiskCode(aRiskCode); 
		        		tLRClaimVerifySchema.setContNo(aContNo); 
		        		tLRClaimVerifySchema.setGrpContNo(aGrpContNo);
		        		tLRClaimVerifySchema.setCValiDate(aCValiDate);
		        		tLRClaimVerifySchema.setSignDate(aSignDate); 
		        		tLRClaimVerifySchema.setClmNo("000000"); 
		        		tLRClaimVerifySchema.setResult(aResult);
		        		tLRClaimVerifySchema.setReContState(aReContState); 
		        		tLRClaimVerifySchema.setRealPay(aRealPay); 
		        		tLRClaimVerifySchema.setRePay(aRePay); 
		        		tLRClaimVerifySchema.setClaimBackFee(aClaimBackFee); 
		        		tLRClaimVerifySchema.setRate(aRate); 
		        		tLRClaimVerifySchema.setDiffPay(aDiffPay); 
		        		tLRClaimVerifySchema.setMakeDate(PubFun.getCurrentDate()); 
		        		tLRClaimVerifySchema.setMakeTime(PubFun.getCurrentTime());  
		        		tLRClaimVerifySchema.setModifyDate(PubFun.getCurrentDate());  
		        		tLRClaimVerifySchema.setModifyTime(PubFun.getCurrentTime());  
		        		tLRClaimVerifySchema.setOperator(globalInput.Operator);  
		        		
		        		tLRClaimVerifySchema.setPolNo(aPolNo);
		        		
		        		tmap.put(tLRClaimVerifySchema, "DELETE&INSERT");
//		        		mLRCessListSet.add(tLRClaimVerifySchema);
		        	
		        	}
		        	if (!prepareOutputData()) {
	                    return false;
	                }
		        	 PubSubmit tPubSubmit = new PubSubmit();
				     tPubSubmit.submitData(mOutputData, "");
				     //如果有需要处理的错误，则返回
				     if (tPubSubmit.mErrors.needDealError())
				       {
				            // @@错误处理
				            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				            CError tError = new CError();
				            tError.moduleName = "LRCessListBL";
				            tError.functionName = "submitDat";
				            tError.errorMessage = "数据提交失败!";
				            this.mErrors.addOneError(tError);
				            return false;
				        }
		        	
		     			}
		     			}while (tSSRS != null && tSSRS.MaxRow > 0);
		     			rsWrapper.close();

		     		
		    		 
		        	
		    	}catch(Exception ex){
		    		ex.printStackTrace();
		    		return false;
		    	}
		    	return true;
		    }
		    private boolean getInputData(VData cInputData) {
		        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
		                "GlobalInput", 0));
		        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
		                "TransferData", 0);
		        mStartDate = (String) mGetCessData.getValueByName("StartDate");
		        mEndDate = (String) mGetCessData.getValueByName("EndDate");
		        System.out.println("mStartDate:"+mStartDate+" mEndDate:"+mEndDate);
		        
		        return true;
		    }
		    /**
		     * 从输入数据中得到所有对象
		     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		     */

		    public String getResult() {
		        return "没有传数据";
		    }
		    

		    public static void main(String[] args) {
		        LRNewGetCessDataBL tLRNewGetCessDataBL = new LRNewGetCessDataBL();
		        GlobalInput globalInput = new GlobalInput();
		        globalInput.ManageCom = "86";
		        globalInput.Operator = "rm0002";
		        String mOperateType = "CESSDATA";

		        String startDate = "2008-10-10";
		        String endDate = "2008-10-10";
		        TransferData getCessData = new TransferData();
		        getCessData.setNameAndValue("StartDate", startDate);
		        getCessData.setNameAndValue("EndDate", endDate);

		        VData tVData = new VData();
		        tVData.addElement(globalInput);
		        tVData.addElement(getCessData);

		        tLRNewGetCessDataBL.submitData(tVData, mOperateType);
		    }
}
