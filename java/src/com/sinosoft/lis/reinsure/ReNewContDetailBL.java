/**
 * Copyright (c) 2008 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import java.io.File;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LRPolClmSet;
import com.sinosoft.lis.vschema.LRPolResultSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.db.LRPolResultDB;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;

/*
 * <p>ClassName: ReNewContDetailBL </p>
 * <p>Description: 新单分保明细清单文件处理 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: Sun yu
 * @CreateDate：2008-10-11
 */
public class ReNewContDetailBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();
    private LRPolSet mLRPolSet=new LRPolSet();

    private FileWriter mFileWriter ;
    private BufferedWriter mBufferedWriter ;

    //业务处理相关变量
    /** 全局数据 */

    public ReNewContDetailBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

        if (this.strOperate.equals("UPDATE")) {//修改分保信息
            if(update()==false){
                buildError("update()","修改失败");
                return false;
            }
        }else if(this.strOperate.equals("CHECK")){//审核通过，修改状态
            if(check()==false){
                buildError("check()","审核失败");
                return false;
            }
        }else if(this.strOperate.equals("ONLOAD")){//下载清单
            if(onload()==false){
                buildError("onload()","下载清单失败");
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLRPolSet=(LRPolSet) cInputData.getObjectByObjectName(
                "LRPolSet", 0);
        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    /**
     * 修改分保信息
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean update(){
        if(this.mLRPolSet.size()>0){
            mMap =new MMap();
            LRPolSchema tLRPolSchema=new LRPolSchema();
            LRPolDB ttLRPolDB = new LRPolDB();
            ttLRPolDB.setPolNo(mLRPolSet.get(1).getPolNo());
            ttLRPolDB.setReNewCount(mLRPolSet.get(1).getReNewCount());
            ttLRPolDB.setReContCode(mLRPolSet.get(1).getReContCode());
            ttLRPolDB.setReinsureItem(mLRPolSet.get(1).getReinsureItem());
            LRPolSet tLRPolSet = ttLRPolDB.query();
            if(tLRPolSet!=null && tLRPolSet.size()!=0){
                tLRPolSchema = tLRPolSet.get(1);
            }else{
                buildError("update()","查询保单（"+mLRPolSet.get(1).getPolNo()+"）信息出错");
                return false;
            }
            LRPolResultDB tLRPolResultDB=new LRPolResultDB();
            tLRPolResultDB.setPolNo(tLRPolSchema.getPolNo());
            tLRPolResultDB.setReNewCount(tLRPolSchema.getReNewCount());
            tLRPolResultDB.setReContCode(tLRPolSchema.getReContCode());
            tLRPolResultDB.setReinsureItem(tLRPolSchema.getReinsureItem());
            LRPolResultSet tLRPolResultSet=tLRPolResultDB.query();
            if(tLRPolResultSet==null){
                buildError("update()","查询保单（"+mLRPolSet.get(1).getPolNo()+"）结果信息出错");
                return false;
            }
            String tCessionAmount = (String) mTransferData.getValueByName("CessionAmount");
            String tCessPrem = (String) mTransferData.getValueByName("CessPrem");
            String tReProcFee = (String) mTransferData.getValueByName("ReProcFee");
            String tChoiRebaFee = (String) mTransferData.getValueByName("ChoiRebaFee");
            String AState = (String) mTransferData.getValueByName("AState");//录入的状态,
            String BState = (String) mTransferData.getValueByName("BState");//录入的状态,

            if(tLRPolResultSet.size()>0){
                LRPolResultSchema tLRPolResultSchema=tLRPolResultSet.get(1);
                tLRPolResultSchema.setCessionAmount(tCessionAmount);
                if(tLRPolResultSchema.getRiskCalSort().equals("1")){//成数，分保保费直接获取。呵呵
                    tLRPolResultSchema.setCessPrem(tCessPrem);
                }else{//溢额，分保保费需要计算，麻烦。 暂不支持责任的分保………………
                    double mAccQuot = getAccQuot(tLRPolSchema);//获取接受份额
                    DealRiskAmntBL tDealRiskAmntBL = new DealRiskAmntBL();
                    String CessPremModeCode = tDealRiskAmntBL.getCalCode(
                                tLRPolSchema, "CessPremMode");
                        if (CessPremModeCode == null ||
                            CessPremModeCode.equals("")) {
                            buildError("ReNewContDetailBL.update()", "获取对应再保分保费率表失败");
                        }
                        double mCessPremRate = tDealRiskAmntBL.calCessFeeRate(
                                CessPremModeCode, tLRPolSchema);
                        if (CessPremModeCode == null ||
                            Math.abs(mCessPremRate + 1) < 0.00001) {
                            buildError("ReNewContDetailBL.update()", "计算再保分保费率失败");
                        }
                       tLRPolResultSchema.setCessPremRate(mCessPremRate); //分保费率
                      String strSQL = "select RiskPeriod from LMRiskApp where riskcode = '" +
                                     tLRPolSchema.getRiskCode() + "'";
                      String tRiskPeriod = new ExeSQL().getOneValue(strSQL);
                      double cessPrem = 0;
                      double mCessionAmount = Double.parseDouble(tCessionAmount);
                      //如果是短期险
                      if (tRiskPeriod.equals("L")) {
                          cessPrem = mCessionAmount/1000 * mCessPremRate * mAccQuot;
                      } else {
                          int days = PubFun.calInterval(tLRPolSchema.getCValiDate(),
                                           tLRPolSchema.getEndDate(), "D");
                          cessPrem = mCessionAmount / 1000 * mCessPremRate *
                                     mAccQuot * days / 365;
                      }
                      tLRPolResultSchema.setCessPrem(cessPrem); //分保保费
                }
                if(tLRPolResultSchema.getRiskCalSort().equals("1")){//成数分保，分保比例=分保保费/应分保保费
                    if(tLRPolResultSchema.getShouldCessPrem()!=0){
                        tLRPolResultSchema.setCessionRate(Double.parseDouble(tCessPrem)/tLRPolResultSchema.getShouldCessPrem());//计算分保比例在理赔摊回时会用到
                    }else{
                        buildError("update()","保单"+tLRPolResultSchema.getPolNo()+"的应分保保费为0");
                        return false;
                    }
                }else{//溢额分保,分保比例＝分保保额/险种保额
                    if(tLRPolSchema.getAmnt()!=0){
                        tLRPolResultSchema.setCessionRate(Double.parseDouble(
                                tCessionAmount) / tLRPolSchema.getAmnt());
                    }else{
                        buildError("update()","保单"+tLRPolResultSchema.getPolNo()+"的保额为0");
                        return false;
                    }
                }
                tLRPolResultSchema.setReProcFee(tReProcFee);
                if(tLRPolResultSchema.getRiskCalSort().equals("1")){//成数分保，手续费比例重新计算
                    tLRPolResultSchema.setReProcFeeRate(Double.parseDouble(tReProcFee)/Double.parseDouble(tCessPrem));
                }else{//溢额分保，选择折扣比例重新计算
                    tLRPolResultSchema.setChoiRebaFeeRate(Double.parseDouble(tChoiRebaFee)/tLRPolResultSchema.getCessPrem());
                }
                tLRPolResultSchema.setOperator(globalInput.Operator);
                tLRPolResultSchema.setModifyDate(PubFun.getCurrentDate());
                tLRPolResultSchema.setModifyTime(PubFun.getCurrentTime());
                this.mMap.put(tLRPolResultSchema, "UPDATE");

                if(tLRPolSchema.getRiskCalSort().equals("1")){//成数分保,置账单状态
                    tLRPolSchema.setActuGetState(BState);
                }else{//溢额分保
                    tLRPolSchema.setActuGetState(AState);
                }
                this.mMap.put(tLRPolSchema, "UPDATE");
            }
            if (!prepareOutputData()) {
                    return false;
                 }
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, "")) {
               if (tPubSubmit.mErrors.needDealError()) {
                  buildError("submitData", "保存再保清单修改出错!");
                 }
                 return false;
           }

        }
        return true;
    }

    /**
     * 审核通过，修改状态
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean check(){
        String tReComCode = (String) mTransferData.getValueByName("ReComCode");
        String tCessionMode = (String) mTransferData.getValueByName("CessionMode");
        String tDiskKind = (String) mTransferData.getValueByName("DiskKind");
        String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
        String tContNo = (String) mTransferData.getValueByName("ContNo");
        String tAppntName = (String) mTransferData.getValueByName("AppntName");
        String tInsuredName = (String) mTransferData.getValueByName("InsuredName");
        String tReContCode = (String)mTransferData.getValueByName("ReContCode");
        String tActuGetState = (String)mTransferData.getValueByName("ActuGetState");
        String tYear = (String) mTransferData.getValueByName("Year");
        String tMonth = (String) mTransferData.getValueByName("Month");
        String tStartDate="";//起始日期
        String tEndDate="";//终止日期
        if(!StrTool.cTrim(tYear).equals("") && !StrTool.cTrim(tMonth).equals("")){
           Calendar calendar=Calendar.getInstance();
           calendar.set(Calendar.YEAR,Integer.parseInt(tYear));
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth));
           calendar.set(Calendar.DATE,1);
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           String tCurrentDate = sdf.format(calendar.getTime());
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth)-1);
           if("12".equals(tMonth))
        	   tStartDate=tYear+"-"+tMonth+"-1";
           else
        	   tStartDate=sdf.format(calendar.getTime());
           
           tEndDate= PubFun.calDate(tCurrentDate,-1,"D","2009-1-31");  //结束日期取本月最后一天
       }

       String strWhere="";
       String strWhereT="";
       if(!StrTool.cTrim(tReComCode).equals("")){
           strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='"+tReComCode+"') ";
           strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='"+tReComCode+"') ";
       }
       if(!StrTool.cTrim(tCessionMode).equals("")){
           strWhere +=" and a.RiskCalSort='"+tCessionMode+"' ";
           strWhereT +=" and a.RiskCalSort='"+tCessionMode+"' ";
       }
       if(!StrTool.cTrim(tDiskKind).equals("")){
           strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+tDiskKind+"') ";
           strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+tDiskKind+"') ";
       }
       if(!StrTool.cTrim(tYear).equals("")){
           strWhere+=" and a.GetDataDate between '"+tStartDate+"' and '"+tEndDate+"' ";
           strWhereT += " and a.GetDataDate between '"+tStartDate+"' and '"+tEndDate+"' ";
       }
       if(!StrTool.cTrim(tRiskCode).equals("")){
           strWhere+=" and a.riskcode='"+tRiskCode+"' ";
           strWhereT +=" and a.riskcode='"+tRiskCode+"' ";
       }
       if(!StrTool.cTrim(tContNo).equals("")){
           strWhere+=" and a.ContNo='"+tContNo+"' ";
           strWhereT +=" and a.ContNo='"+tContNo+"' ";
       }
       if(!StrTool.cTrim(tAppntName).equals("")){
           strWhere+=" and a.AppntName like '%"+tAppntName+"%' ";
           strWhereT +=" and a.AppntName like '%"+tAppntName+"%' ";
       }
       if(!StrTool.cTrim(tInsuredName).equals("")){
           strWhere+=" and a.InsuredName like '%"+tInsuredName+"%' ";
           strWhereT +=" and a.InsuredName like '%"+tInsuredName+"%' ";
       }
       if(!StrTool.cTrim(tReContCode).equals("")){
            strWhere+=" and a.ReContCode ='"+tReContCode+"' ";
            strWhereT +=" and a.ReContCode ='"+tReContCode+"' ";
        }
        if(!StrTool.cTrim(tActuGetState).equals("")){
            strWhere+=" and a.ActuGetState ='"+tActuGetState+"' ";
            strWhereT +=" and a.ActuGetState ='"+tActuGetState+"' ";
        }


       String strSQL="select a.* "
                   +"from LRPol a,LRPolResult c "
                   +"where a.polno=c.polno  and a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                   +"and a.ReinsureItem=c.ReinsureItem  "
                   + strWhere;
      if(!StrTool.cTrim(tReComCode).equals("")||!StrTool.cTrim(tDiskKind).equals("")){
          //如果机构号和险种类型不为空，则为临时分保单独写一个sql;
          strSQL +=" union select a.* "
                   +"from LRPol a,LRPolResult c "
                   +"where a.polno=c.polno  and "
                   +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                   +"and a.ReinsureItem=c.ReinsureItem "
                   + strWhereT;
      }
      strSQL +=" with ur";

       LRPolSet tLRPolSet = new LRPolSet();
       RSWrapper rswrapper = new RSWrapper();
       rswrapper.prepareData(tLRPolSet, strSQL);
       try {
            do {
                 rswrapper.getData();
                 mMap=new MMap();
                 for(int j=1;j<=tLRPolSet.size();j++){
                   LRPolSchema tLRPolSchema=tLRPolSet.get(j);
                   tLRPolSchema.setActuGetState("01");
                   tLRPolSchema.setModifyDate(PubFun.getCurrentDate());
                   tLRPolSchema.setModifyTime(PubFun.getCurrentTime());
                   this.mMap.put(tLRPolSchema, "UPDATE");
                 }
                 if (!prepareOutputData()) {
                    return false;
                 }
                PubSubmit tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算清单审核结果出错!");
                    }
                    return false;
                }
               } while (tLRPolSet.size() > 0);
                rswrapper.close();
           } catch (Exception ex) {
                ex.printStackTrace();
                rswrapper.close();
            }
        return true;
    }

    private boolean onload(){
        String tReComCode = (String) mTransferData.getValueByName("ReComCode");
        String tCessionMode = (String) mTransferData.getValueByName("CessionMode");
        String tDiskKind = (String) mTransferData.getValueByName("DiskKind");
        String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
        String tContNo = (String) mTransferData.getValueByName("ContNo");
        String tAppntName = (String) mTransferData.getValueByName("AppntName");
        String tInsuredName = (String) mTransferData.getValueByName("InsuredName");
        String tReContCode = (String)mTransferData.getValueByName("ReContCode");
        String tActuGetState = (String)mTransferData.getValueByName("ActuGetState");
        String tYear = (String) mTransferData.getValueByName("Year");
        String tMonth = (String) mTransferData.getValueByName("Month");
        String tStartDate="";//起始日期
        String tEndDate="";//终止日期
        if(!StrTool.cTrim(tYear).equals("") && !StrTool.cTrim(tMonth).equals("")){
           Calendar calendar=Calendar.getInstance();
           calendar.set(Calendar.YEAR,Integer.parseInt(tYear));
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth));
           calendar.set(Calendar.DATE,1);
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           String tCurrentDate = sdf.format(calendar.getTime());
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth)-1);
           if("12".equals(tMonth))
        	   tStartDate=tYear+"-"+tMonth+"-1";
           else
        	   tStartDate=sdf.format(calendar.getTime());
           tEndDate= PubFun.calDate(tCurrentDate,-1,"D","2009-1-31");  //结束日期取本月最后一天
       }

       String tSysPath = (String) mTransferData.getValueByName("SysPath"); //获取地址;
       tSysPath +="reinsure/";
       String tPath="";
       try{
            tPath = "XinDanMingXi"+PubFun.getCurrentDate2() + "_" + PubFun.getCurrentTime2();
            mFileWriter = new FileWriter(tSysPath+tPath+".txt");
            mBufferedWriter = new BufferedWriter(mFileWriter);

            String[] strArrHead = new String[29];
            strArrHead[0] = "机构代码";
            strArrHead[1] = "产品代码";
            strArrHead[2] = "保单号";
            strArrHead[3] = "保单生效日期";
            strArrHead[4] = "保单终止日期";
            //strArrHead[5] = "增人生效日期";
            strArrHead[5] = "签单日";
            //strArrHead[0][6] = "交至日";
            strArrHead[6] = "缴费方式";
            //strArrHead[0][8] = "保险期间";
            //strArrHead[0][7] = "缴费期间";
            //strArrHead[0][10] = "保单年度";
            //strArrHead[0][11] = "核保结论"; //暂时没有，应该增加LRPol的字段

            //strArrHead[0][12] = "团体名称";
            strArrHead[7] = "投保人客户号";
            strArrHead[8] = "投保人姓名";
            strArrHead[9] = "被保人客户号";
            strArrHead[10] = "被保险人姓名";
            strArrHead[11] = "平均年龄/生日";
            strArrHead[12] = "性别";
            strArrHead[13] = "身份证号码";
            strArrHead[14] = "职业类别";
            //strArrHead[0][20] = "吸烟状态";
            strArrHead[15] = "加费率";
            //strArrHead[0][22] = "重疾加费率";
            strArrHead[16] = "原始保额";
            strArrHead[17] = "累计保额";    //  目前存储在LRPolCessAmnt中
            //strArrHead[0][25] = "累计准备金";  //  目前存储在LRPolCessAmnt中
            //strArrHead[0][26] = "累计风险保额"; //  目前存储在LRPolCessAmnt中
            strArrHead[18] = "累计分出保额"; 
            strArrHead[19] = "标准保费";
            strArrHead[20] = "全年应收保费";
            //strArrHead[0][18] = "折扣比例";

            strArrHead[21] = "接受份额";
            strArrHead[22] = "自留额";
            strArrHead[23] = "分出保额";
            strArrHead[24] = "分出比例";
            strArrHead[25] = "净分保费率";
            strArrHead[26] = "净分保费";
            strArrHead[27] = "选择折扣率";
            strArrHead[28] = "分保手续费";

            String head = "";
            for(int m=0;m<strArrHead.length;m++){
                        head += strArrHead[m]+"|";
            }
            mBufferedWriter.write(head+"\r\n");
            mBufferedWriter.flush();
        }catch(Exception ex1){
            ex1.printStackTrace();
        }

        String strWhere="";
//        String strWhereT="";
        if(!StrTool.cTrim(tReComCode).equals("")){//sql中暂不加这个条件
//            strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='"+tReComCode+"') ";
//            strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='"+tReComCode+"') ";
        }
        if(!StrTool.cTrim(tCessionMode).equals("")){
            strWhere +=" and a.RiskCalSort='"+tCessionMode+"' ";
//            strWhereT +=" and a.RiskCalSort='"+tCessionMode+"' ";
        }
        if(!StrTool.cTrim(tDiskKind).equals("")){//sql中暂不加这个条件
//            strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+tDiskKind+"') ";
//            strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+tDiskKind+"') ";
        }
        if(!StrTool.cTrim(tYear).equals("")){
            strWhere+=" and a.GetDataDate between '"+tStartDate+"' and '"+tEndDate+"' ";
//            strWhereT += " and a.GetDataDate between '"+tStartDate+"' and '"+tEndDate+"' ";
        }
        if(!StrTool.cTrim(tRiskCode).equals("")){
            strWhere+=" and a.riskcode='"+tRiskCode+"' ";
//            strWhereT +=" and a.riskcode='"+tRiskCode+"' ";
        }
        if(!StrTool.cTrim(tContNo).equals("")){
            strWhere+=" and a.ContNo='"+tContNo+"' ";
//            strWhereT +=" and a.tContNo='"+tContNo+"' ";
        }
        if(!StrTool.cTrim(tAppntName).equals("")){
            strWhere+=" and a.AppntName like '%"+tAppntName+"%' ";
//            strWhereT +=" and a.AppntName like '%"+tAppntName+"%' ";
        }
        if(!StrTool.cTrim(tInsuredName).equals("")){
            strWhere+=" and a.InsuredName like '%"+tInsuredName+"%' ";
//            strWhereT +=" and a.InsuredName like '%"+tInsuredName+"%' ";
        }
        if(!StrTool.cTrim(tReContCode).equals("")){
            strWhere+=" and a.ReContCode = '"+tReContCode+"' ";
//            strWhereT +=" and a.ReContCode = '"+tReContCode+"' ";
        }
        if(!StrTool.cTrim(tActuGetState).equals("")){
            strWhere+=" and a.ActuGetState = '"+tActuGetState+"' ";
//            strWhereT +=" and a.ActuGetState = '"+tActuGetState+"' ";
        }
        String strSQL="select a.ManageCom,a.recontcode,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.SignDate,(select CodeName " +
                              "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                              "a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,f.SumAmnt,a.CessAmnt,a.StandPrem,a.Prem, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='AccQuot'), " +
                              "f.RemainAmnt, " +
                              "c.CessionAmount," +
                              "c.CessionRate, c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee "
                   +"from LRPol a,LRPolDetail b,LRPolResult c,lrinterface d,LCCont e,LRPolCessAmnt f "
                   +"where c.cessprem>0 and a.contno=e.contno and a.conttype='1' and a.polno=b.polno  and a.polno=c.polno and "
                   +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                   +"and a.ReNewCount=b.ReNewCount  and a.polno=d.polno  and a.PolNo = f.Polno "
                   + strWhere
                   +"group by a.ManageCom,a.recontcode,f.RemainAmnt,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.CValidate,a.SignDate,a.PayIntv,a.OccupationType,a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,a.InsuredBirthday,a.InsuredSex,a.RecontCode,a.DiseaseAddFeeRate,a.Amnt,f.SumAmnt, a.CessAmnt, a.StandPrem,a.Prem,c.CessionAmount,c.CessionRate,c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee  "
                   +" union all "
                   +"select a.ManageCom,a.recontcode,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.SignDate,(select CodeName " +
                   "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                   "a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                   "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,f.SumAmnt,a.CessAmnt,a.StandPrem,a.Prem, " +
                   "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='AccQuot'), " +
                   "f.RemainAmnt, " +
                   "c.CessionAmount," +
                   "c.CessionRate, c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee "
                   +"from LRPol a,LRPolDetail b,LRPolResult c,lrinterface d,LCCont e,LRPolCessAmnt f "
                   +"where c.cessprem>0 and a.contno=e.contno and a.conttype='1' and d.polno=b.polno  and a.polno=c.polno and "
                   +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount  and a.PolNo = f.Polno "
                   +"and a.ReNewCount=b.ReNewCount  and c.polno=(select e.polno from lbrnewstatelog e where e.newpolno=a.polno and e.renewcount=a.renewcount )  "
                   + strWhere
                   +"group by a.ManageCom,a.recontcode,f.RemainAmnt,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.CValidate,a.SignDate,a.PayIntv,a.OccupationType,a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,a.InsuredBirthday,a.InsuredSex,a.RecontCode,a.DiseaseAddFeeRate,a.Amnt,f.SumAmnt, a.CessAmnt, a.StandPrem,a.Prem,c.CessionAmount,c.CessionRate,c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee  "
                   +"union all "  
                   +"select a.ManageCom,a.recontcode,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.SignDate,(select CodeName " +
                   "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                   "a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                   "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,f.SumAmnt,a.CessAmnt,a.StandPrem,a.Prem, " +
                   "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='AccQuot'), " +
                   "f.RemainAmnt, " +
                   "c.CessionAmount," +
                   "c.CessionRate, c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee "
        +"from LRPol a,LRPolDetail b,LRPolResult c,lrinterface d,LBCont e,LRPolCessAmnt f "
        +"where c.cessprem>0 and a.contno=e.contno and a.conttype='1' and a.polno=b.polno  and a.polno=c.polno and "
        +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
        +"and a.ReNewCount=b.ReNewCount  and a.polno=d.polno  and a.PolNo = f.Polno "
        + strWhere
        +"group by a.ManageCom,a.recontcode,f.RemainAmnt,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.CValidate,a.SignDate,a.PayIntv,a.OccupationType,a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,a.InsuredBirthday,a.InsuredSex,a.RecontCode,a.DiseaseAddFeeRate,a.Amnt,f.SumAmnt, a.CessAmnt, a.StandPrem,a.Prem,c.CessionAmount,c.CessionRate,c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee  "
        +" union all "
        +"select a.ManageCom,a.recontcode,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.SignDate,(select CodeName " +
        "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
        "a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
        "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,f.SumAmnt,a.CessAmnt,a.StandPrem,a.Prem, " +
        "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='AccQuot'), " +
        "f.RemainAmnt, " +
        "c.CessionAmount," +
        "c.CessionRate, c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee "
        +"from LRPol a,LRPolDetail b,LRPolResult c,lrinterface d,LBCont e,LRPolCessAmnt f "
        +"where c.cessprem>0 and a.contno=e.contno and a.conttype='1' and d.polno=b.polno  and a.polno=c.polno and "
        +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount  and a.PolNo = f.Polno "
        +"and a.ReNewCount=b.ReNewCount  and c.polno=(select e.polno from lbrnewstatelog e where e.newpolno=a.polno and e.renewcount=a.renewcount )  "
        + strWhere
        +"group by a.ManageCom,a.recontcode,f.RemainAmnt,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.CValidate,a.SignDate,a.PayIntv,a.OccupationType,a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,a.InsuredBirthday,a.InsuredSex,a.RecontCode,a.DiseaseAddFeeRate,a.Amnt,f.SumAmnt, a.CessAmnt, a.StandPrem,a.Prem,c.CessionAmount,c.CessionRate,c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee  "
        +"union all " //团单成数分保
                   +"select a.ManageCom,a.recontcode,a.RiskCode,a.GrpContNo,a.CValidate,a.enddate,a.SignDate,(select CodeName " +
                              "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                              "a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,f.SumAmnt,a.CessAmnt,a.StandPrem,a.Prem, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='AccQuot'), " +
                              "f.RemainAmnt, " +
                              "c.CessionAmount," +
                              "c.CessionRate, c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee "
                   +"from LRPol a,LRPolDetail b,LRPolResult c,lrinterface d ,LRPolCessAmnt f "
                   +"where c.cessprem>0 and a.conttype='2' and a.RiskCalSort = '1' and a.polno=b.polno  and a.polno=c.polno and "
                   +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                   +"and a.ReNewCount=b.ReNewCount  and a.polno=d.polno  and a.PolNo = f.Polno "
                   + strWhere
                   +"group by a.ManageCom,a.recontcode,f.RemainAmnt,a.RiskCode,a.GrpContNo,a.CValidate,a.enddate,a.SignDate,a.PayIntv,a.OccupationType,a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,a.InsuredBirthday,a.InsuredSex,a.RecontCode,a.DiseaseAddFeeRate,a.Amnt,f.SumAmnt, a.CessAmnt, a.StandPrem,a.Prem,c.CessionAmount,c.CessionRate,c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee  "
                   +"union all "
                   +"select a.ManageCom,a.recontcode,a.RiskCode,a.GrpContNo,a.CValidate,a.enddate,a.SignDate,(select CodeName " +
                              "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                              "a.AppntNo,a.AppntName,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,f.SumAmnt,a.CessAmnt,a.StandPrem,a.Prem, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='AccQuot'), " +
                              "f.RemainAmnt, " +
                              "c.CessionAmount," +
                              "c.CessionRate, c.CessPremRate,c.CessPrem,c.ChoiRebaFeeRate,c.ReProcFee "
                   +"from LRPol a,LRPolDetail b,LRPolResult c,lrinterface d,LRPolCessAmnt f "
                   +"where c.cessprem>0 and a.conttype='2' and a.RiskCalSort = '2' and a.polno=b.polno  and a.polno=c.polno and "
                   +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                   +"and a.ReNewCount=b.ReNewCount  and a.polno=d.polno  and a.PolNo = f.Polno "
                   + strWhere
                   +" with ur ";


//      if(!StrTool.cTrim(tReComCode).equals("")||!StrTool.cTrim(tDiskKind).equals("")){
//          //如果机构号和险种类型不为空，则为临时分保单独写一个sql;
//          strSQL +=" union select a.riskcode,a.ReContCode, "
//                   +" (case a.RiskcalSort when '1' then '成数' when '2' then '溢额' end ) RiskCalSort, "
//                   +"a.ContNo contno, "
//                   +"a.AppntName,a.InsuredName,a.CValiDate,a.signdate,a.Amnt,a.StandPrem,b.PayMoney, "
//                   +"c.CessionAmount,c.CessPrem,c.ReProcFee sum "
//                   +"from LRPol a,LRPolDetail b,LRPolResult c "
//                   +"where a.polno=b.polno  and b.polno=c.polno and "
//                   +"a.RecontCode=c.RecontCode and a.ReNewCount=b.ReNewCount "
//                   +"and b.ReNewCount=c.ReNewCount and b.paycount=c.paycount "//????Paycount???
//                   + strWhereT;
//      }
//      strSQL +=" order by ReContCode,RiskCalSort,CValiDate,signdate  with ur";
      int start = 1;
      int nCount = 10000;
        while (true) {
             SSRS tSSRS = new ExeSQL().execSQL(strSQL, start, nCount);
             int count=0;
             if (tSSRS.getMaxRow() <= 0) {
                  break;
              }else{
                  count=tSSRS.getMaxRow();
              }
            if (tSSRS != null && count > 0) {
              for (int i = 1; i <= count; i++) {
                try{
                    String result="";
                    for(int m=1;m<=tSSRS.getMaxCol();m++){
                          result += tSSRS.GetText(i,m)+"|";
                    }
                     mBufferedWriter.write(result+"\r\n");
                     mBufferedWriter.flush();
                 } catch (IOException ex2) {
                        ex2.printStackTrace();
                  }
                 }
             }
             start += nCount;
           }
         try {
            mBufferedWriter.close();
          } catch (IOException ex3) {
          }
          String[] FilePaths = new String[1];
          FilePaths[0] = tSysPath+tPath+".txt";
          String[] FileNames = new String[1];
          FileNames[0] =tPath+".txt";
          String newPath = tSysPath +tPath+".zip";
          String FullPath = tPath+".zip";
          CreateZip(FilePaths,FileNames,newPath);
          try{
              File fd = new File(FilePaths[0]);
              fd.delete();
          }catch(Exception ex4){
              ex4.printStackTrace();
          }
        mResult.add(FullPath);
        return true;
    }
    //生成压缩文件
  public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                           String tZipPath) {
      ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
      if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
          System.out.println("生成压缩文件失败");
          CError.buildErr(this, "生成压缩文件失败");
          return false;
      }
      return true;
  }
  /**
     * 获取再保合同的接受份额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getAccQuot(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='ACCQUOT' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

}

