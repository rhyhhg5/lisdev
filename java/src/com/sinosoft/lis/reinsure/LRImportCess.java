package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入的被保人清单添加到数据库 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class LRImportCess {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    /** 保全号 */
    private String mEdorNo = null;
    /** 批次号 */
    private String mBatchNo = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();
    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    /** 节点名 */
    private String sheetName = "Sheet1";
    /** 配置文件名 */
    private String configName = "LRCessImport.xml";
    private LRPolSet mLRPolSet = new LRPolSet(); //再保险保单表
    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInput
     */
    public LRImportCess(GlobalInput gi) {
        this.mGlobalInput = gi;
    }

    /**
     * 添加被保人清单
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName){
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        LRImportAct tLRImportAct = new LRImportAct(path +
                fileName,
                path + configName,
                sheetName);
        if (!tLRImportAct.doImport()) {
            this.mErrors.copyAllErrors(tLRImportAct.mErrrors);
            return false;
        }
        LRPolSet tLRPolSet = (LRPolSet) tLRImportAct.getSchemaSet();
        System.out.println("LRPolSet的长度: "+tLRPolSet.size());
        //校验导入的数据
        if (!checkData(tLRPolSet)) {
            return false;
        }
        //存放Insert Into语句的容器
        MMap map = new MMap();
        for (int i = 1; i <= tLRPolSet.size(); i++) {
            addOneLRPol(tLRPolSet.get(i));
        }
        map.put(mLRPolSet, "DELETE&INSERT");
        this.mResult.add(tLRPolSet);

        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }

        return true;
    }

    private void deletePayList(MMap map){

    }

    /**
     * 校验导入数据
     * @param cLZCardPaySet LZCardPaySet
     * @return boolean
     */
    private boolean checkData(LRPolSet aLRPolSet) {
        for (int i = 1; i <= aLRPolSet.size(); i++) {
            LRPolSchema schema = aLRPolSet.get(i);

            return true;
        }
        return true;
    }

    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private boolean addOneLRPol(LRPolSchema aLRPolSchema) {
        LRPolDB tLRPolDB = new LRPolDB();
        tLRPolDB.setPolNo(aLRPolSchema.getPolNo());
        tLRPolDB.setReinsureCom(aLRPolSchema.getReinsureCom());
        tLRPolDB.setReinsurItem(aLRPolSchema.getReinsurItem());
        tLRPolDB.setInsuredYear(aLRPolSchema.getInsuredYear());
        if(!tLRPolDB.getInfo())
        {
           buildError("addOneLRPol","险种保单号为:"+tLRPolDB.getPolNo()+"的保单出错!");
            return false;
        }

        LRPolSchema tLRPolSchema = tLRPolDB.getSchema();

        tLRPolSchema.setPolNo(aLRPolSchema.getPolNo());
        tLRPolSchema.setReinsureCom(aLRPolSchema.getReinsureCom());
        tLRPolSchema.setReinsurItem(aLRPolSchema.getReinsurItem());
        tLRPolSchema.setInsuredYear(aLRPolSchema.getInsuredYear());
        tLRPolSchema.setGrpContNo(aLRPolSchema.getGrpContNo());
        tLRPolSchema.setPrtNo(aLRPolSchema.getPrtNo());
        tLRPolSchema.setRiskCode(aLRPolSchema.getRiskCode());
        tLRPolSchema.setInsuredNo(aLRPolSchema.getInsuredNo());
        tLRPolSchema.setInsuredName(aLRPolSchema.getInsuredName());
        tLRPolSchema.setInsuredSex(aLRPolSchema.getInsuredSex());
        tLRPolSchema.setInsuredBirthday(aLRPolSchema.getInsuredBirthday());
        tLRPolSchema.setInsuredAppAge(aLRPolSchema.getInsuredAppAge());
        tLRPolSchema.setAppntNo(aLRPolSchema.getAppntNo());
        tLRPolSchema.setAppntName(aLRPolSchema.getAppntName());
        tLRPolSchema.setCValiDate(aLRPolSchema.getCValiDate());
        tLRPolSchema.setEndDate(aLRPolSchema.getEndDate());
        tLRPolSchema.setStandPrem(aLRPolSchema.getStandPrem());
        tLRPolSchema.setPrem(aLRPolSchema.getPrem());
        tLRPolSchema.setAmnt(aLRPolSchema.getAmnt());
        tLRPolSchema.setRiskAmnt(aLRPolSchema.getRiskAmnt());
        tLRPolSchema.setSumRiskAmount(aLRPolSchema.getSumRiskAmount());
        tLRPolSchema.setPayIntv(aLRPolSchema.getPayIntv());
        tLRPolSchema.setPayMode(aLRPolSchema.getPayMode());
        tLRPolSchema.setPayYears(aLRPolSchema.getPayYears());
        tLRPolSchema.setCessionRate(aLRPolSchema.getCessionRate());
        tLRPolSchema.setCessionAmount(aLRPolSchema.getCessionAmount());
        tLRPolSchema.setCessPremRate(aLRPolSchema.getCessPremRate());
        tLRPolSchema.setCessPrem(aLRPolSchema.getCessPrem());
        tLRPolSchema.setCessCommRate(aLRPolSchema.getCessCommRate());
        tLRPolSchema.setCessComm(aLRPolSchema.getCessComm());
        tLRPolSchema.setSpeCemmRate(aLRPolSchema.getSpeCemmRate());
        tLRPolSchema.setReProcFeeRate(aLRPolSchema.getReProcFeeRate());
        tLRPolSchema.setDeadAddFeeRate(aLRPolSchema.getDeadAddFeeRate());
        tLRPolSchema.setDiseaseAddFeeRate(aLRPolSchema.getDiseaseAddFeeRate());
        tLRPolSchema.setLvevlExAddFee(aLRPolSchema.getLvevlExAddFee());
        tLRPolSchema.setInsurePeriod(aLRPolSchema.getInsurePeriod());
        tLRPolSchema.setSaleChnl(aLRPolSchema.getSaleChnl());
System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX: "+aLRPolSchema.getSaleChnl());
        tLRPolSchema.setOperator(mGlobalInput.Operator);
        tLRPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLRPolSchema.setModifyTime(PubFun.getCurrentTime());

        mLRPolSet.add(tLRPolSchema);
        return true;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";
        String grpContNo = "1400000915";
        String path = "d:\\";
        String fileName = "cessdata.xls";
        path = "D:/project/ui/temp/";
        String config = "D:/project/ui/temp/LRCessImport.xml";
        LRImportCess tLRImportCess = new LRImportCess(tGI);
        if (!tLRImportCess.doAdd(path, fileName)) {
            System.out.println(tLRImportCess.mErrors.getFirstError());
            System.out.println("磁盘导入失败！");
        }
    }
  }
