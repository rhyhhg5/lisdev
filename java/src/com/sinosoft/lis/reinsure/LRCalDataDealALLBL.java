/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import java.util.Hashtable;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LRCalCessDataBL </p>
 * <p>Description: LRCalCessDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2008-11-20
 */
public class LRCalDataDealALLBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    private double mAccQuot; //接受份额
    private double mManageFeeRate; //分保管理费费率
    //用于毛保费成数分保
    private double mCessionRate; //分保比例
    private double mReProcFeeRate; //再保手续费费率
    private double mSpeCemmRate; //特殊佣金费率
    //用于溢额分保
    private double mCessionAmount; //分保保额
    private double mCessPremRate; //分保费率
    private double mChoiRebaFeeRate; //折扣比例
    private double mCessAmnt;
    private Hashtable m_hashLMCheckField = new Hashtable();
    private String mReContCode = "";
    //业务处理相关变量
    DealRiskAmntBL tDealRiskAmntBL = new DealRiskAmntBL();
    /** 全局数据 */

    public LRCalDataDealALLBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }
    
    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT(String cOperate,String mToDay) {
        
        if (!getInputDataT(mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
//        GlobalInput globalInput = new GlobalInput();
//        globalInput.ManageCom = "86";
//        globalInput.Operator = "ytz";
//
//        // prepare main plan
//        // 准备传输数据 VData
//        VData vData = new VData();
//        TransferData tTransferData = new TransferData();
//        LRCalDataDealALLBL tLRCalDataDealALLBL = new LRCalDataDealALLBL();
//        try {
//            vData.add(globalInput);
//            tTransferData.setNameAndValue("StartDate", "2012-11-01");
//            tTransferData.setNameAndValue("EndDate", "2012-11-01");
//            vData.add(tTransferData);
//            tLRCalDataDealALLBL.submitData(vData, "CESSDATA");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    	
        //    	分保特别险种计算
    	LRCalDataDealALLBL tLRCalDataDealALLBL=new LRCalDataDealALLBL();
    	if(tLRCalDataDealALLBL.submitDataT("Server","2013-9-1")==false){
    		System.out.println("fsdfsd");
    	}    	

		System.out.println("end:"+PubFun.getCurrentDate()+PubFun.getCurrentTime());
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRCalCessDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 分类计算分保数据
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取分保数据
        System.out.println("Come to calCessData()...............");
        
        this.mMap = new MMap();

//        if (!calCessData1()) {
//            buildError("calCessData1", "再保计算成数分保失败!");
//        }
//        if (!calCessData2()) {
//            buildError("calCessData2", "再保计算溢额分保失败!");
//        }
//        if (!calCessData3()) {
//            buildError("calCessData3", "再保计算临时分保失败!");
//        }
        if (!calCessData4()) {
            buildError("calCessData4", "再保计算风险溢额分保失败!");
        }
        return true;

    }
    public LRCalFactorValueSchema findCheckFieldByRiskCode(String strRiskCode, String tReContCode,String tFactorCode) {

        Hashtable hash = m_hashLMCheckField;
        String strPK = strRiskCode.trim() + tReContCode.trim()+tFactorCode.trim();
        Object obj = hash.get(strPK);

        if( obj != null ) {
          if( obj instanceof String ) {
            return null;
          } else {
            return (LRCalFactorValueSchema)obj;
          }
        } else {
        	LRCalFactorValueDB tLRCalFactorValueDB = new LRCalFactorValueDB();

        	tLRCalFactorValueDB.setRiskCode(strRiskCode);
        	tLRCalFactorValueDB.setReContCode(tReContCode);
        	tLRCalFactorValueDB.setFactorCode(tFactorCode);
        	LRCalFactorValueSet tLRCalFactorValueSet=tLRCalFactorValueDB.query();
        	if(tLRCalFactorValueSet.size()<=0){
        		hash.put(strPK, "NOFOUND");
        		return null;
        		
        	}
          LRCalFactorValueSchema tLRCalFactorValueSchema = tLRCalFactorValueSet.get(1);

          if( tLRCalFactorValueDB.mErrors.needDealError() ) {
            mErrors.copyAllErrors(tLRCalFactorValueDB.mErrors);
            hash.put(strPK, "NOFOUND");
            return null;
          }

          hash.put(strPK, tLRCalFactorValueSchema);
          return tLRCalFactorValueSchema;
        }
      }
  


  

   

  
    
    /**
     * 风险溢额分保的计算
     * @return boolean
     */
    private boolean calCessData4() {
    	String strSQL =
            "select * from LRPol a where  (select CessionMode from LRContInfo where RecontCode = a.RecontCode) = '2' " +
            "and a.GetDataDate between '" +
            mStartDate + "' and '" + mEndDate +
            "' and a.TempCessFlag = 'N' and ( " +
            "riskcode in ('530501', '531201') or " +
//            用actugetstate来标识一条lrpol是否已经处理,处理之后设置为99
            " (riskcode in (select code from ldcode where codetype='reinrisk') and a.actugetstate is null))  " ;
         if(mReContCode!=null&&!"".equals(mReContCode)){            	
            strSQL+=" and recontcode='"+mReContCode+"'  ";
          }
          strSQL+=" with ur ";
          LRPolSet tLRPolSet = new LRPolSet();
          LRPolResultSet tLRPolResultSet=new LRPolResultSet();
          RSWrapper rswrapper = new RSWrapper();
          rswrapper.prepareData(tLRPolSet, strSQL);
    	try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolSet.size(); i++) {
                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                    double tCessionRate=tLRPolSchema.getDeadAddFeeRate();
                    double tCessPrem=0.00;
                    double tChoiRebaFee=0.00;
                    double tChoiRebaFeeR=0;
                    this.mCessAmnt = tLRPolSchema.getAmnt();
                   
                    this.mCessPremRate = CommonBL.carry(0.85);
//                    this.mCessPremRate=1;
                   
                    //查询该合同是按责任还是险种分保
                    strSQL =
                            "select ReType from Lrcontinfo where recontcode = '" +
                            tLRPolSchema.getReContCode() + "'";
                    String ReType = new ExeSQL().getOneValue(strSQL);

                    if (ReType.equals("01")) {
                    	this.mManageFeeRate=getManageFeeRate(tLRPolSchema);
                    	tCessPrem = getCessPremA(tLRPolSchema.getPolNo(),tLRPolSchema.getReContCode(),tLRPolSchema.getRiskCode(),mCessPremRate);
                    	
                    	/*                    	关于分出保费的计算分两种情况：分出保费按照每个月计算
                    	 * 
                    	                    	1、   首次生效日期在2012年6月1日前的保单，分出保费=基本保额*万分之8.5*1/12
                    	                    	
                    	                    	2、   首次生效日期在2012年6月1日以后的保单，分出保费=基本保额*万分之8.5*（本月天数-本月生效日已经过天数+1）
                    	                    	
                    	                 例如6月10日为首次生效日期，分出保费=基本保额*万分之8.5*（30-10+1），这个保单在下个月的计算方法为：分出保费=基本保额*万分之8.5*1/12
                    	*/
//                        String checkDateSQL="select 1 from dual where "+tLRPolSchema.getCValiDate()+" >='2012-06-01' ";
//                    	                    	 
//                        String checkDateData = new ExeSQL().getOneValue(checkDateSQL);
//                    	                        
//                    	                        
//                        String checkDateSQLDay="select day('"+tLRPolSchema.getCValiDate()+"') from dual";
//                    	                   	 
//                    	                        
//                        String checkDateDataDay = new ExeSQL().getOneValue(checkDateSQLDay);
//                    	                        
//                    	                        
//                        int wCheckDayNum=Integer.parseInt(checkDateDataDay);
                    	                         
//                    	                        
//                        if(checkDateData.equals("1"))
////                    	                        
//                        	{                	
//                        	tCessPrem=this.mCessAmnt*mCessPremRate*(30+1-wCheckDayNum);                       	
//                        	System.out.println(this.mCessAmnt*mCessPremRate*(30+1-wCheckDayNum));
//                        	}
//                        else
//                        {
                        	
//                        }                   	
                    
                    	
                    		
                    } 
                    
                    if (ReType.equals("02")) {
                    	if(tLRPolSchema.getRiskCode().equals("730101")){
                    		tCessPrem = tLRPolSchema.getCessAmnt()*0.5*0.52/10000 ;
                    		mCessAmnt=tLRPolSchema.getCessAmnt();
                    		mCessPremRate=0.52;
                    		mManageFeeRate=0;
//                    		设置状态已处理
                    		tLRPolSchema.setActuGetState("99");
                    		this.mMap.put(tLRPolSchema,SysConst.UPDATE);                    		
                    	}
                    }
               LRPolResultSchema tLRPolResultSchema = new
                       LRPolResultSchema();
               LRPolResultSchema aLRPolResultSchema=new LRPolResultSchema(
            		   );
               
               
               aLRPolResultSchema.setPolNo(tLRPolSchema.getPolNo());
               aLRPolResultSchema.setReContCode(tLRPolSchema.getReContCode());
               aLRPolResultSchema.setReinsureItem(tLRPolSchema.getReinsureItem());
               aLRPolResultSchema.setTempCessFlag(tLRPolSchema.getTempCessFlag());
               aLRPolResultSchema.setReNewCount(tLRPolSchema.getReNewCount());
               aLRPolResultSchema.setPayCount(0); 
               aLRPolResultSchema.setRiskCalSort(tLRPolSchema.getRiskCalSort());
               aLRPolResultSchema.setRiskCode(tLRPolSchema.getRiskCode());
               aLRPolResultSchema.setRiskVersion(tLRPolSchema.getRiskVersion());
               aLRPolResultSchema.setCessStartDate(tLRPolSchema.getGetDataDate());
               aLRPolResultSchema.setCessEndDate(this.mEndDate);
               aLRPolResultSchema.setCessionRate(tCessionRate); //分保比例
               aLRPolResultSchema.setReinsureItem(tLRPolSchema.getReinsureItem());
               aLRPolResultSchema.setCessionAmount(this.mCessAmnt); //分保保额
               aLRPolResultSchema.setCessPremRate(this.mCessPremRate); //分保费率
               tCessPrem = tCessPrem/12;
           	   System.out.println(tCessPrem);
               aLRPolResultSchema.setCessPrem(tCessPrem); //分保保费

               String validate = tLRPolSchema.getCValiDate();
               int currYear = PubFun.calInterval(validate,
               		tLRPolSchema.getGetDataDate(),
                                                 "Y") + 1; //计算保单年度,第一年的保单年度为1
               if (currYear > 1) {
               	tChoiRebaFeeR = 0; //如果非首次保单年度,则没有折扣
               }
               aLRPolResultSchema.setChoiRebaFeeRate(tChoiRebaFeeR); //选择折扣比例
               aLRPolResultSchema.setReProcFee(tCessPrem*tChoiRebaFeeR); //再保手续费=分保保费*选择折扣比例
               aLRPolResultSchema.setManageFeeRate(this.mManageFeeRate); //再保管理费比例
               aLRPolResultSchema.setManageFee(tCessPrem * this.mManageFeeRate); //再保管理费
               aLRPolResultSchema.setPayNo(tLRPolSchema.getPayNo());
               aLRPolResultSchema.setManageCom(tLRPolSchema.getManageCom());
               aLRPolResultSchema.setOperator("ytz");
               aLRPolResultSchema.setMakeDate(mPubFun.getCurrentDate());
               aLRPolResultSchema.setMakeTime(mPubFun.getCurrentTime());
               aLRPolResultSchema.setModifyDate(mPubFun.getCurrentDate());
               aLRPolResultSchema.setModifyTime(mPubFun.getCurrentTime());
               
               //tLRPolResultSet.add(aLRPolResultSchema);
           
           this.mMap.put(aLRPolResultSchema.getSchema(),"DELETE&INSERT");
//           }
                    
           
                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        //buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolSet.size() > 0);
            rswrapper.close();
            
           
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
    	return true;
    }

   
    private double getCessPremA(String tPolNo,String tReContCode,String tRiskCode,double tCessPremRate){
    	double tCessPrem=0.00;
    	double tCessAmnt=0.00;
    	double tAccQuot=1;
    	
    	String sql2="";
//    	
    	sql2="select FactorValue from lrcalfactorvalue where " +
    			"recontcode='"+tReContCode+"' and FactorCode='AccQuot' and riskcode='"+tRiskCode+"' with ur";
    	SSRS tAQ = new ExeSQL().execSQL(sql2);
//    	
    	tCessAmnt=this.mCessAmnt;
    	if(tAQ.GetText(1, 1)!=null&&!"".equals(tAQ.GetText(1, 1)))
    	tAccQuot=Double.parseDouble(tAQ.GetText(1, 1));
    	tCessPrem=tCessAmnt*tCessPremRate*tAccQuot;
    	tCessPrem=tCessPrem/1000;
    	return tCessPrem;
    	
    }
    
   
    
  
    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolSchema aLRPolSchema, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolSchema.getPolNo());
        tLRErrorLogSchema.setLogType("5"); //5:新单计算
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator("ytz");
        tLRErrorLogSchema.setManageCom("86");
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "DELETE&INSERT");

        return true;
    }

  
   

   

    /**
     * 得到再保手续费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getReProcFeeRate(LRPolSchema aLRPolSchema) {
       
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"ReinProcFeeRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 得到特殊佣金比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getSpeCemmRate(LRPolSchema aLRPolSchema) {
       
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"SpeComRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 得到选择折扣比例,只针对首年度保单
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getChoiRebaFeeRate(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='CHOIREBARATE' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 按责任分保的得到选择折扣比例,只针对首年度保单
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getChoiRebaFeeRate(LRPolSchema aLRPolSchema,
                                      LRDutySchema aLRDutySchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='CHOIREBARATE' and riskcode='" +
                aLRDutySchema.getGetDutyCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }


    /**
     * 得到管理费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getManageFeeRate(LRPolSchema aLRPolSchema) {
        
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"ReinManFeeRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 获取按责任分保的管理费比例
     * @param aLRPolSchema LRPolSchema
     * @param aLRDutySchema LRDutySchema
     * @return double
     */
    private double getManageFeeRate(LRPolSchema aLRPolSchema,
                                    LRDutySchema aLRDutySchema) {
        String strSQL = "select factorvalue from lrcalfactorvalue where upper(factorcode)='REINMANFEERATE' " +
                        "and recontcode='" + aLRPolSchema.getReContCode() +
                        "' and riskcode='" + aLRDutySchema.getGetDutyCode() +
                        "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }
    
    /**
     * 获取按责任分保的管理费比例
     * @param aLRPolSchema LRPolSchema
     * @param aLRDutySchema LRDutySchema
     * @return double
     */
    private double getManageFeeRate(String tReContCode,
                                    String tGetDutyCode) {
        String strSQL = "select factorvalue from lrcalfactorvalue where upper(factorcode)='REINMANFEERATE' " +
                        "and recontcode='" + tReContCode +
                        "' and riskcode='" + tGetDutyCode +
                        "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }


    /**
     * 得到成数分保比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getCessionDRate(LRPolSchema aLRPolSchema,String tRiskcode) {
    	LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(tRiskcode,aLRPolSchema.getReContCode(),"CessionRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }
    
    private double getCessionRate(LRPolSchema aLRPolSchema) {
    	LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"CessionRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 获取再保合同的接受份额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getAccQuot(LRPolSchema aLRPolSchema) {
        
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"AccQuot");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 获取责任分保的接受份额
     * @param aLRPolSchema LRPolSchema
     * @param aLRDutySchema LRDutySchema
     * @return double
     */
    private double getAccQuot(LRPolSchema aLRPolSchema,
                              LRDutySchema aLRDutySchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where upper(factorcode)='ACCQUOT' " +
                "and recontcode='" + aLRPolSchema.getReContCode() +
                "' and riskcode='" + aLRDutySchema.getGetDutyCode() + "'";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");
//        mReContCode = (String) mGetCessData.getValueByName("ReContCode");
        return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(String mToDay) {
        
        mStartDate = mToDay;
        mEndDate = mToDay;
//        mReContCode = (String) mGetCessData.getValueByName("ReContCode");
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
//        CError cError = new CError();
//        cError.moduleName = "LRCalCessDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
}
