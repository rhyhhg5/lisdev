package com.sinosoft.lis.reinsure;


import com.sinosoft.lis.pubfun.diskimport.DefaultDiskImporter;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.lis.schema.LRPolEdorSchema;
import com.sinosoft.lis.vschema.LRPolEdorSet;
import com.sinosoft.lis.vschema.LRClaimPolicySet;


/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class LRImportClaimAct {
    /** 团单被保人清单Schema */
    private static final String schemaClassName =
            "com.sinosoft.lis.schema.LRClaimPolicySchema";
    /** 团单被保人清单Schema */
    private static final String schemaSetClassName =
            "com.sinosoft.lis.vschema.LRClaimPolicySet";
    /** 使用默认的导入方式 */
    private DefaultDiskImporter importer = null;
    /** 错误处理 */
    public CErrors mErrrors = new CErrors();

    public LRImportClaimAct(String fileName, String configFileName,
                         String sheetName) {
        importer = new DefaultDiskImporter(fileName, configFileName, sheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport() {
        importer.setSchemaClassName(schemaClassName);
        importer.setSchemaSetClassName(schemaSetClassName);
        if (!importer.doImport()) {
            mErrrors.copyAllErrors(importer.mErrors);
            String errMess = "";
            for (int i=0;i<mErrrors.getErrorCount();i++)
            {
                errMess+= mErrrors.getError(i).errorMessage ;
            }

        System.out.println("mErrrors信息: "+errMess );
            return false;
        }
        return true;
    }

    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public SchemaSet getSchemaSet() {
        return importer.getSchemaSet();
    }

    public static void main(String[] args) {
        String path = "D:/project/ui/temp/lrclaimdata.xls";
        String config = "D:/project/ui/temp/LRClaimImport.xml";
        LRImportClaimAct tLRImportClaimAct = new LRImportClaimAct(path, config, "Sheet1");
        tLRImportClaimAct.doImport();
        LRClaimPolicySet tLRClaimPolicySet=(LRClaimPolicySet) tLRImportClaimAct.getSchemaSet();
        System.out.println("SIZE: "+tLRClaimPolicySet.size());
        for (int i = 1; i <= tLRClaimPolicySet.size(); i++) {
            System.out.println("处理数据 " + i + tLRClaimPolicySet.get(i).encode().replace('|','\t'));
        }
    }
}
