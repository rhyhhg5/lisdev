package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.LRClaimPolicyDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRClaimPolicySchema;
import com.sinosoft.lis.vschema.LRClaimPolicySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入的被保人清单添加到数据库 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class LRImportClaim {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    /** 保全号 */
    private String mEdorNo = null;
    /** 批次号 */
    private String mBatchNo = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();
    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    /** 节点名 */
    private String sheetName = "Sheet1";
    /** 配置文件名 */
    private String configName = "LRClaimImport.xml";
    private LRClaimPolicySet mLRClaimPolicySet = new LRClaimPolicySet(); //再保理赔表
    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInput
     */
    public LRImportClaim(GlobalInput gi) {
        this.mGlobalInput = gi;
    }

    /**
     * 添加被保人清单
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName){
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        LRImportClaimAct tLRImportClaimAct = new LRImportClaimAct(path +
                fileName,
                path + configName,
                sheetName);
        if (!tLRImportClaimAct.doImport()) {
            this.mErrors.copyAllErrors(tLRImportClaimAct.mErrrors);
            return false;
        }
        LRClaimPolicySet tLRClaimPolicySet = (LRClaimPolicySet) tLRImportClaimAct.getSchemaSet();
        System.out.println("LRPolSet的长度: "+tLRClaimPolicySet.size());
        //校验导入的数据
        if (!checkData(tLRClaimPolicySet)) {
            return false;
        }
        //存放Insert Into语句的容器
        MMap map = new MMap();
        for (int i = 1; i <= tLRClaimPolicySet.size(); i++) {
            addOneLRPol(tLRClaimPolicySet.get(i));
        }
        map.put(mLRClaimPolicySet, "DELETE&INSERT");
        this.mResult.add(tLRClaimPolicySet);

        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }

        return true;
    }

    private void deletePayList(MMap map){

    }

    /**
     * 校验导入数据
     * @param cLZCardPaySet LZCardPaySet
     * @return boolean
     */
    private boolean checkData(LRClaimPolicySet aLRClaimPolicySet) {
        for (int i = 1; i <= aLRClaimPolicySet.size(); i++) {
            LRClaimPolicySchema schema = aLRClaimPolicySet.get(i);

            return true;
        }
        return true;
    }

    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private boolean addOneLRPol(LRClaimPolicySchema aLRClaimPolicySchema) {
        LRClaimPolicyDB tLRClaimPolicyDB = new LRClaimPolicyDB();
        tLRClaimPolicyDB.setClmNo(aLRClaimPolicySchema.getClmNo());
        tLRClaimPolicyDB.setPolNo(aLRClaimPolicySchema.getPolNo());
        tLRClaimPolicyDB.setReinsureCom(aLRClaimPolicySchema.getReinsureCom());
        tLRClaimPolicyDB.setReinsurItem(aLRClaimPolicySchema.getReinsurItem());
        tLRClaimPolicyDB.setInsuredYear(aLRClaimPolicySchema.getInsuredYear());
        tLRClaimPolicyDB.setInsuredYear(aLRClaimPolicySchema.getInsuredYear());
        tLRClaimPolicyDB.setRgtNo(aLRClaimPolicySchema.getRgtNo());
        tLRClaimPolicyDB.setCaseNo(aLRClaimPolicySchema.getCaseNo());

        if(!tLRClaimPolicyDB.getInfo())
        {
           buildError("addOneLRPol","险种保单号为:"+tLRClaimPolicyDB.getPolNo()+"的保单出错!");
            return false;
        }
        LRClaimPolicySchema tLRClaimPolicySchema = tLRClaimPolicyDB.getSchema();

        tLRClaimPolicySchema.setClmNo(aLRClaimPolicySchema.getClmNo());
        tLRClaimPolicySchema.setPolNo(aLRClaimPolicySchema.getPolNo());
        tLRClaimPolicySchema.setReinsureCom(aLRClaimPolicySchema.getReinsureCom());
        tLRClaimPolicySchema.setReinsurItem(aLRClaimPolicySchema.getReinsurItem());
        tLRClaimPolicySchema.setInsuredYear(aLRClaimPolicySchema.getInsuredYear());
        tLRClaimPolicySchema.setRgtNo(aLRClaimPolicySchema.getRgtNo());
        tLRClaimPolicySchema.setCaseNo(aLRClaimPolicySchema.getCaseNo());
        tLRClaimPolicySchema.setRiskCode(aLRClaimPolicySchema.getRiskCode());
        tLRClaimPolicySchema.setSaleChnl(aLRClaimPolicySchema.getSaleChnl());
        tLRClaimPolicySchema.setInsuredNo(aLRClaimPolicySchema.getInsuredNo());
        tLRClaimPolicySchema.setInsuredName(aLRClaimPolicySchema.getInsuredName());
        tLRClaimPolicySchema.setAppntNo(aLRClaimPolicySchema.getAppntNo());
        tLRClaimPolicySchema.setAppntName(aLRClaimPolicySchema.getAppntName());
        tLRClaimPolicySchema.setCValiDate(aLRClaimPolicySchema.getCValiDate());
        tLRClaimPolicySchema.setEndDate(aLRClaimPolicySchema.getEndDate());
        tLRClaimPolicySchema.setAccidentDate(aLRClaimPolicySchema.getAccidentDate());
        tLRClaimPolicySchema.setClaimMoney(aLRClaimPolicySchema.getClaimMoney());
        tLRClaimPolicySchema.setRealPay(aLRClaimPolicySchema.getRealPay());
        tLRClaimPolicySchema.setEndCaseDate(aLRClaimPolicySchema.getEndCaseDate());
        tLRClaimPolicySchema.setConfDate(aLRClaimPolicySchema.getConfDate());
        tLRClaimPolicySchema.setRgtDate(aLRClaimPolicySchema.getRgtDate());
        tLRClaimPolicySchema.setAccountPeriod(aLRClaimPolicySchema.getAccountPeriod());
        tLRClaimPolicySchema.setUWFlag(aLRClaimPolicySchema.getUWFlag());
        tLRClaimPolicySchema.setAccDesc(aLRClaimPolicySchema.getAccDesc());
        tLRClaimPolicySchema.setLeaveHospDate(aLRClaimPolicySchema.getLeaveHospDate());
        tLRClaimPolicySchema.setDiagnoses(aLRClaimPolicySchema.getDiagnoses());
        tLRClaimPolicySchema.setCessPersonSug(aLRClaimPolicySchema.getCessPersonSug());
        tLRClaimPolicySchema.setCessionAmount(aLRClaimPolicySchema.getCessionAmount());
        tLRClaimPolicySchema.setClaimCessRate(aLRClaimPolicySchema.getClaimCessRate());


        tLRClaimPolicySchema.setOperator(mGlobalInput.Operator);
        tLRClaimPolicySchema.setModifyDate(PubFun.getCurrentDate());
        tLRClaimPolicySchema.setModifyTime(PubFun.getCurrentTime());

        mLRClaimPolicySet.add(tLRClaimPolicySchema);
        return true;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";
        String grpContNo = "1400000915";
        String path = "d:\\";
        String fileName = "lrclaimdata.xls";
        path = "D:/project/ui/temp/";
        String config = "D:/project/ui/temp/LRClaimImport.xml";
        LRImportClaim tLRImportClaim = new LRImportClaim(tGI);
        if (!tLRImportClaim.doAdd(path, fileName)) {
            System.out.println(tLRImportClaim.mErrors.getFirstError());
            System.out.println("磁盘导入失败！");
        }
    }
  }
