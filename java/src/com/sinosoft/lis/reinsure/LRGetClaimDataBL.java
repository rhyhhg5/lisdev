/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import java.util.Date;

/*
 * <p>ClassName: LRGetClaimDataBL </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: huxl
 * @CreateDate：2007-03-13
 */
public class LRGetClaimDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    private PubFun mPubFun = new PubFun();
    private String mCurrentDate = new PubFun().getCurrentDate();

    private String mCurrentTime = new PubFun().getCurrentTime();

    //业务处理相关变量
    /** 全局数据 */

    public LRGetClaimDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL->Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取理赔数据
        if (this.strOperate.equals("CLAIMDATA")) {
            if (!getClaimData()) {
                // @@错误处理
                buildError("insertData", "删除单证管理员时出现错误!");
                return false;
            }
        }
        return true;
    }

    /**
     * 提取理赔数据，以结算日期为准
     * @return boolean
     */
    private boolean insertClaimData() {
        //删除该提数日期范围内的记录
        System.out.println("Come to InsertClaimData ...");
//        String strDelClm = "Delete from LRPolClm a where a.GetDataDate = '" +
//                           this.mToday +
//                           "' and exists (select 1 from LRPol where PolNo = a.PolNo and TempCessFlag = 'N')";
//        this.mMap.put(strDelClm, "DELETE");
        //得到当天给付的理赔案件,提数原则 理赔赔付日期 = 提数日期
        String tSql =
                "select a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
                "(select min(a1.AccDate) from llsubreport a1,llcaserela a2  where a1.SubRptNo = a2.SubRptNo and a2.CaseNo=c.CaseNo) 出险日期, " +
                "(select lp.ClmState from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),sum(b.StandPay),sum(b.RealPay) 赔付金额, " +
                "(select lp.ClmUwer from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),c.EndCaseDate 结案日期,c.RgtDate 索赔日期,d.makedate 赔付日期, " +
                "(select sum(ClaimMoney) from LLClaimDetail where caseno=c.caseno and polno = a.PolNO and ClmNo = b.ClmNo) 索赔额,a.UWFlag, " +
                "(select min(a2.accdesc) from llcaserela a1,llsubreport a2 where a1.SubRptNo=a2.SubRptNo and a1.caseNo=c.CaseNo) 出险原因, " +
                "(select max(HospEndDate) from llfeemain where caseno=c.caseno) 出院日期, " +
                "(select DiseaseName from LLCaseCure where caseno=c.caseno order by serialno desc fetch first row only) 诊断," +
                "a.ManageCom ,a.agentGroup,a.masterPolno,d.ConfDate " +
                "from LCPol a, llclaimPolicy b, llcase c, ljaget d " +
                "where exists (select 1 from lrpol where polno=a.polno union all select 1 from lrpol where polno = a.masterPolno) and " +
                "a.PolNo = b.PolNo and b.CaseNo = c.CaseNo  and not exists (select 1 from lrpolclm where polno=a.polno and clmno=b.clmno and getdatadate >='2009-8-1') " +
                "and a.Appflag = '1' and (c.caseno=d.otherno or c.rgtno=d.otherno) and d.makedate = '" +
                this.mToday +
                "' group by a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
                "c.EndCaseDate,c.RgtDate,d.makedate,a.UWFlag,a.ManageCom,a.agentGroup,a.masterPolno,d.ConfDate " +
                " union " +
                "select a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
                "(select min(a1.AccDate) from llsubreport a1,llcaserela a2  where a1.SubRptNo = a2.SubRptNo and a2.CaseNo=c.CaseNo) 出险日期, " +
                "(select lp.ClmState from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),sum(b.StandPay),sum(b.RealPay) 赔付金额, " +
                "(select lp.ClmUwer from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),c.EndCaseDate 结案日期,c.RgtDate 索赔日期,d.makedate 赔付日期, " +
                "(select sum(ClaimMoney) from LLClaimDetail where caseno=c.caseno and polno = a.PolNO and ClmNo = b.ClmNo) 索赔额,a.UWFlag, " +
                "(select min(a2.accdesc) from llcaserela a1,llsubreport a2 where a1.SubRptNo=a2.SubRptNo and a1.caseNo=c.CaseNo) 出险原因, " +
                "(select max(HospEndDate) from llfeemain where caseno=c.caseno) 出院日期, " +
                "(select DiseaseName from LLCaseCure where caseno=c.caseno order by serialno desc fetch first row only) 诊断," +
                "a.ManageCom ,a.agentGroup,a.masterPolno,d.ConfDate  " +
                "from LBPol a, llclaimPolicy b, llcase c, ljaget d " +
                "where exists (select 1 from lrpol where polno=a.polno union all select 1 from lrpol where polno = a.masterPolno) and " +
                "a.PolNo = b.PolNo and b.CaseNo = c.CaseNo  and not exists (select 1 from lrpolclm where polno=a.polno and clmno=b.clmno and getdatadate >='2009-8-1')" +
                "and a.Appflag = '1' and (c.caseno=d.otherno or c.rgtno=d.otherno) and d.makedate = '" +
                this.mToday +
                "' group by a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
                "c.EndCaseDate,c.RgtDate,d.makedate,a.UWFlag,a.ManageCom,a.agentGroup,a.masterPolno,d.ConfDate " +
//                " union " +
//                "select a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
//                "(select min(a1.AccDate) from llsubreport a1,llcaserela a2  where a1.SubRptNo = a2.SubRptNo and a2.CaseNo=c.CaseNo) 出险日期, " +
//                "(select lp.ClmState from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),sum(b.StandPay),sum(b.RealPay) 赔付金额, " +
//                "(select lp.ClmUwer from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),c.EndCaseDate 结案日期,c.RgtDate 索赔日期,d.makedate 赔付日期, " +
//                "(select sum(ClaimMoney) from LLClaimDetail where caseno=c.caseno and polno = a.PolNO and ClmNo = b.ClmNo) 索赔额,a.UWFlag, " +
//                "(select min(a2.accdesc) from llcaserela a1,llsubreport a2 where a1.SubRptNo=a2.SubRptNo and a1.caseNo=c.CaseNo) 出险原因, " +
//                "(select max(HospEndDate) from llfeemain where caseno=c.caseno) 出院日期, " +
//                "(select DiseaseName from LLCaseCure where caseno=c.caseno order by serialno desc fetch first row only) 诊断," +
//                "a.ManageCom ,a.agentGroup,a.masterPolno,d.ConfDate  " +
//                "from LCPol a, llclaimPolicy b, llcase c, ljaget d " +
//                "where exists (select 1 from lrpol where polno=a.polno union all select 1 from lrpol where polno = a.masterPolno) and " +
//                "a.PolNo = b.PolNo and b.CaseNo = c.CaseNo  and not exists (select 1 from lrpolclm where polno=a.polno and clmno=b.clmno ) " +
//                "and a.Appflag = '1' and (c.caseno=d.otherno or c.rgtno=d.otherno) and d.makedate<='2009-12-31' and (d.confdate is null or d.confdate>='2009-12-1')" +//实付表生成日期是20090601日期以前，财务核日期在20090601之后的。此处日期修改以上线为准.
//                " group by a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
//                "c.EndCaseDate,c.RgtDate,d.makedate,a.UWFlag,a.ManageCom,a.agentGroup,a.masterPolno,d.ConfDate " +
//                " union " +
//                "select a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
//                "(select min(a1.AccDate) from llsubreport a1,llcaserela a2  where a1.SubRptNo = a2.SubRptNo and a2.CaseNo=c.CaseNo) 出险日期, " +
//                "(select lp.ClmState from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),sum(b.StandPay),sum(b.RealPay) 赔付金额, " +
//                "(select lp.ClmUwer from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),c.EndCaseDate 结案日期,c.RgtDate 索赔日期,d.makedate 赔付日期, " +
//                "(select sum(ClaimMoney) from LLClaimDetail where caseno=c.caseno and polno = a.PolNO and ClmNo = b.ClmNo) 索赔额,a.UWFlag, " +
//                "(select min(a2.accdesc) from llcaserela a1,llsubreport a2 where a1.SubRptNo=a2.SubRptNo and a1.caseNo=c.CaseNo) 出险原因, " +
//                "(select max(HospEndDate) from llfeemain where caseno=c.caseno) 出院日期, " +
//                "(select DiseaseName from LLCaseCure where caseno=c.caseno order by serialno desc fetch first row only) 诊断," +
//                "a.ManageCom ,a.agentGroup,a.masterPolno,d.ConfDate  " +
//                "from LBPol a, llclaimPolicy b, llcase c, ljaget d " +
//                "where exists (select 1 from lrpol where polno=a.polno union all select 1 from lrpol where polno = a.masterPolno) and " +
//                "a.PolNo = b.PolNo and b.CaseNo = c.CaseNo  and not exists (select 1 from lrpolclm where polno=a.polno and clmno=b.clmno )" +
//                "and a.Appflag = '1' and (c.caseno=d.otherno or c.rgtno=d.otherno) and d.makedate<='2009-12-31' and (d.confdate is null or d.confdate>='2009-12-1')" +//实付表生成日期是20090601日期以前，财务核日期在20090601之后的。此处日期修改以上线为准.
//                " group by a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
//                "c.EndCaseDate,c.RgtDate,d.makedate,a.UWFlag,a.ManageCom,a.agentGroup,a.masterPolno,d.ConfDate " +
                " with ur ";

        int start = 1;
        int nCount = 5000;
        while (true) {
            SSRS tSSRS = new ExeSQL().execSQL(tSql, start, nCount);
            if (tSSRS.getMaxRow() <= 0) {
                break;
            }
            mMap = new MMap();
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                    LRPolClmSchema tLRPolClmSchema = new LRPolClmSchema();
                    tLRPolClmSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolClmSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolClmSchema.setContNo(tSSRS.GetText(i, 3));
                    tLRPolClmSchema.setPolNo(tSSRS.GetText(i, 4)); //保单号
                    tLRPolClmSchema.setClmNo(tSSRS.GetText(i, 5));
                    tLRPolClmSchema.setRgtNo(tSSRS.GetText(i, 6));
                    tLRPolClmSchema.setCaseNo(tSSRS.GetText(i, 7));
                    tLRPolClmSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolClmSchema.setRiskVer(tSSRS.GetText(i, 9));
                    tLRPolClmSchema.setAccidentDate(tSSRS.GetText(i, 10)); //出险日期
                    tLRPolClmSchema.setClmState(tSSRS.GetText(i, 11));
                    tLRPolClmSchema.setStandPay(tSSRS.GetText(i, 12));
                    tLRPolClmSchema.setRealPay(tSSRS.GetText(i, 13)); //赔付金额
                    tLRPolClmSchema.setClmUWer(tSSRS.GetText(i, 14));
                    tLRPolClmSchema.setEndCaseDate(tSSRS.GetText(i, 15)); //结案日期
                    tLRPolClmSchema.setRgtDate(tSSRS.GetText(i, 16)); //索赔日期
                    tLRPolClmSchema.setConfDate(tSSRS.GetText(i, 26)); //赔付日期
                    tLRPolClmSchema.setClaimMoney(tSSRS.GetText(i, 18)); //索赔额
                    tLRPolClmSchema.setUWFlag(tSSRS.GetText(i, 19));
                    tLRPolClmSchema.setAccDesc(tSSRS.GetText(i, 20)); //出险原因
                    tLRPolClmSchema.setLeaveHospDate(tSSRS.GetText(i, 21)); //出院日期
                    tLRPolClmSchema.setDiagnoses(tSSRS.GetText(i, 22)); //诊断
                    if(tLRPolClmSchema.getRealPay()>=0){
                        tLRPolClmSchema.setGiveTypeDesc("正常给付"); //理赔结论
                    }else{
                        tLRPolClmSchema.setGiveTypeDesc("纠错"); //理赔结论
                    }
                    tLRPolClmSchema.setManageCom(tSSRS.GetText(i, 23)); //保单管理机
                    //080714新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                                 +tSSRS.GetText(i,24)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolClmSchema.setCostCenter(CostCenter);

                    tLRPolClmSchema.setGetDataDate(tSSRS.GetText(i,17));
                    tLRPolClmSchema.setOperator(globalInput.Operator);
                    tLRPolClmSchema.setMakeDate(this.mCurrentDate);
                    tLRPolClmSchema.setMakeTime(this.mCurrentTime);
                    tLRPolClmSchema.setModifyDate(this.mCurrentDate);
                    tLRPolClmSchema.setModifyTime(this.mCurrentTime);
                    String reNewCount = "0";
                    if (!tSSRS.GetText(i, 10).equals("")) {
                        //下面计算根据出险日期计算出险时的续保次数
                        String tSql1 = " select RenewCount from LCPol where PolNo = '" +
                               tSSRS.GetText(i, 4) + "' and '" +
                               tSSRS.GetText(i, 10) +
                               "' between CValidate and EndDate - 1 Day " +
                               " union " +
                               " select RenewCount from LBPol where PolNo = '" +
                               tSSRS.GetText(i, 4) + "' and '" +
                               tSSRS.GetText(i, 10) +
                               "' between CValidate and EndDate - 1 Day" +
                               " union " +
                               " select RenewCount from LBPol where PolNo in (select NewPolNo from LCRNewStateLog where PolNo = '" +
                               tSSRS.GetText(i, 4) + "') and '" +
                               tSSRS.GetText(i, 10) +
                               "' between CValidate and EndDate - 1 Day with ur";
                        reNewCount = new ExeSQL().getOneValue(tSql1); //这里主要是考虑到出险日期可能不在保单生效期间 by huxl @070724
                        if (reNewCount.equals("")) {
                            reNewCount = "0";
                        }
                    }
                    tLRPolClmSchema.setReNewCount(reNewCount); //续保次数
                    String masterPolno = tSSRS.GetText(i, 25); //主被保人保单号码
                    String tSql2 =
                            "select distinct a.ReContCode,a.ReinsureItem,a.RiskCalSort,(select ReComCode from LRContInfo where ReContCode = " +
                            "a.ReContCode) from LRPol a where (a.PolNo = '" +
                            tSSRS.GetText(i, 4)+ "' or a.polno='"+masterPolno+"') and RenewCount = " +
                            reNewCount;
                    SSRS cSSRS = new ExeSQL().execSQL(tSql2);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                        tLRPolClmSchema.setReContCode(cSSRS.GetText(j, 1)); //新增字段
                        tLRPolClmSchema.setReinsureItem(cSSRS.GetText(j, 2));
                        tLRPolClmSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolClmSchema.setReComCode(cSSRS.GetText(j, 4));
                        mMap.put(tLRPolClmSchema.getSchema(), "DELETE&INSERT");
                    }
                    if(cSSRS.getMaxRow()==0){
                    	start += 1;
                    }
                }
             }
                //提取每天的合同再保数据，作为一个事务
                if (!prepareOutputData()) {
                }
                //如果提交不成功,不能返回
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(this.mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        // @@错误处理
                        buildError("insertData", "保存临分协议信息时出现错误!");
                    }
                }
            
        }
        return true;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolClmSchema aLRPolClmSchema,
                             String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new
                                             LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolClmSchema.getPolNo());
        tLRErrorLogSchema.setLogType("4");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }

    /**
     * 提取理赔数据
     *
     * @return boolean
     */
    private boolean getClaimData() {
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");

        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(mEndDate); //录入的终止日期，用FDate处理

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!insertClaimData()) {
                buildError("getClaimData", "提取" + mToday + "号理赔数据出错!");
                return false;
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LRGetClaimDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "86";
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0001";

        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRGetClaimDataBL tLRGetClaimDataBL = new LRGetClaimDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2009-8-31");
            tTransferData.setNameAndValue("EndDate", "2009-8-31");
            System.out.println("in main: " +
                               (String) tTransferData.
                               getValueByName("StartDate") + "   " +
                               (String) tTransferData.getValueByName("EndDate"));
            vData.add(tTransferData);
            tLRGetClaimDataBL.submitData(vData, "CLAIMDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
