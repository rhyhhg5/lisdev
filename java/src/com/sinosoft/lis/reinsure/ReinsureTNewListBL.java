package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LRAccountsSet;
import com.sinosoft.utility.*;
import java.io.File;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;

/**
 * <p>Title: 分保计算结果明细</p>
 * <p>Description: 分保计算结果明细清单</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author huxl
 * @version 1.0
 * @date 2007-03-31
 */

public class ReinsureTNewListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    String mYear = "";
    String mMonth = "";
    String mDate = "";
    String RecontCode = "";
    String ReRiskCode = "";
    String SysPath = ""; //存放生成文件的路径
    String FullPath = "";
    
    private LRAccountsSet mLRAccountsSet = new LRAccountsSet();
    public ReinsureTNewListBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cSysPath) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        SysPath = cSysPath;
        // 准备所有要打印的数据
        if (!downloadCessData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLRAccountsSet = (LRAccountsSet)cInputData.getObjectByObjectName("LRAccountsSet", 0);
        //System.out.println(mLRAccountsSet.get(1).getActuGetNo());
        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        mYear = (String) tTransferData.getValueByName("Year");
        mMonth = (String) tTransferData.getValueByName("Month");
        mDate=mYear+"-"+mMonth+"-1";
//        ReRiskSort = (String) tTransferData.getValueByName("ReRiskSort");
//        RecontCode = (String) tTransferData.getValueByName("RecontCode");
//        ReRiskCode = (String) tTransferData.getValueByName("ReRiskCode");
        return true;
    }

    public String getResult() {
        return FullPath;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean downloadCessData() {
        try {
            SysPath = SysPath + "reinsure/";
            String Path = PubFun.getCurrentDate() + "-" + PubFun.getCurrentTime() +
                          ".xls";
            Path = Path.replaceAll(":", "-");
            WriteToExcel tWriteToExcel = new WriteToExcel(Path);
            String[] FilePaths = new String[1];
            FilePaths[0] = SysPath + Path;
            String[] FileNames = new String[1];
            FileNames[0] = "ReCessData" +
                          
                            "_" + mYear+
                           
                            "_" + mMonth +
                           ".xls";
            tWriteToExcel.createExcelFile();
            String[] sheetName = {mYear + "__" + mMonth};
            tWriteToExcel.addSheet(sheetName);

            
                String sql = "select ReComCode,ReComName,RecontCode,RecontName,CessPrem,ReProcFee,ClaimBackFee," +
                		"BalanceLend,PayDate,ReProcFee+BalanceLend,CessPrem from LRAccounts" +
                		" where startdate='"+mDate+"' with ur";
                
                String[][] strArr1Head = new String[1][11];
                strArr1Head[0][0] = "再保公司编码";
                strArr1Head[0][1] = "再保公司名称";
                strArr1Head[0][2] = "合同编码";
                strArr1Head[0][3] = "合同名称";
                strArr1Head[0][4] = "分保费";
                strArr1Head[0][5] = "分保手续费";
                strArr1Head[0][6] = "分摊赔款";
                strArr1Head[0][7] = "分保余额";
                strArr1Head[0][8] = "账单结算日期";

                strArr1Head[0][9] = "借款合计";
                strArr1Head[0][10] = "贷款合计";
//                strArr1Head[0][11] = "被保险人人数/姓名";
//                strArr1Head[0][12] = "平均年龄/生日";
//                strArr1Head[0][13] = "性别";
//                strArr1Head[0][14] = "身份证号码";
//                strArr1Head[0][15] = "职业类别";
//                strArr1Head[0][16] = "限额/保额";
//                strArr1Head[0][17] = "保险期间";
//                strArr1Head[0][18] = "期交保费";
//                strArr1Head[0][19] = "当期保费";
//                strArr1Head[0][20] = "应缴期数";
//                strArr1Head[0][21] = "待缴期数";
//                strArr1Head[0][22] = "全年应收保费收入";
//
//                strArr1Head[0][23] = "分保类型";
//                strArr1Head[0][24] = "成数分出比例";
//                strArr1Head[0][25] = "成数分出保额";
//                strArr1Head[0][26] = "成数分出保费";
//                strArr1Head[0][27] = "分保手续费率";
//                strArr1Head[0][28] = "特殊佣金费率";
//                strArr1Head[0][29] = "分保手续费";
                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql);
                tWriteToExcel.setData(0, sql);
            
            tWriteToExcel.write(SysPath);
            String NewPath = PubFun.getCurrentDate() + "-" +
                             PubFun.getCurrentTime() + ".zip";
            NewPath = NewPath.replaceAll(":", "-");
            FullPath = NewPath;
            NewPath = SysPath + NewPath;
            CreateZip(FilePaths, FileNames, NewPath);
            File fd = new File(FilePaths[0]);
            fd.delete();
            System.out.println("FullPath!  : " + NewPath);

        } catch (Exception ex) {
            ex.toString();
            ex.printStackTrace();
        }
        return true;
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this, "生成压缩文件失败");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
    }
}
