package com.sinosoft.lis.reinsure;

import java.util.Date;

import com.sinosoft.lis.bq.EdorCalZT;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LRClaimPolicySet;
import com.sinosoft.lis.vschema.LREdorMainSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class CalPolRetentBL {

  public CErrors mErrors=new CErrors();
  private GlobalInput mGlobalInput =new GlobalInput() ;
  private VData mInputData ;
  private String mOperate;
  private LRPolSet mLRPolSet= new LRPolSet();
  private LREdorMainSet mLREdorMainSet = new LREdorMainSet();
  private LRClaimPolicySet mLRClaimPolicySet = new LRClaimPolicySet();
  private VData mResult = new VData();
  private String mStartDate = "";
  private String mEndDate = "";
  private String mToday = "";
  private String mReinsureCom="";


  public CalPolRetentBL() {
  }

  public boolean submitData(VData cInputData, String cOperate)
  {
    System.out.println("beging bl.......");
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    try
    {
      if( !mOperate.equals("CalLegal")&& !mOperate.equals("CalComm") )
      {
        buildError("submitData", "不支持的操作字符串");
        return false;
      }

      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) )
      {
        return false;
      }
      mLRPolSet.clear();
      mResult.clear();
      FDate chgdate = new FDate();
      Date dbdate = chgdate.getDate(mStartDate);
      Date dedate = chgdate.getDate(mEndDate);

      while(dbdate.compareTo(dedate) <= 0)
      {
        mToday = chgdate.getString(dbdate);
        // 提取分保数据
        if( !getCessData() )
        {
          buildError("submitData", "错误");
          return false;
        }


        dbdate=PubFun.calDate(dbdate,1,"D",null);
      }

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      buildError("submit", "发生异常");
      return false;
    }
    return true;
  }
  private void buildError(String szFunc, String szErrMsg)
    {
      CError cError = new CError( );
      cError.moduleName = "CalLegalRetentBL";

      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      this.mErrors.addOneError(cError);
  }
  private boolean getInputData(VData cInputData)
    {
      mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      mStartDate = (String)cInputData.get(1);
      mEndDate = (String)cInputData.get(2);
      mReinsureCom=(String)cInputData.get(3);

      if (mStartDate.equals(""))
      {
        buildError("getInputData","没有起始日期!");
        return false;
      }

      if (mEndDate.equals(""))
      {
        buildError("getInputData","没有终止日期!");
        return false;
      }

      if( mGlobalInput==null )
      {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
      }
      return true;
    }

    private boolean getCessData() //法定分保保单年度处理
    {
      String tSql="";
      String tItemFlag="";
      if (this.mOperate.equals("CalLegal"))
      {
        tItemFlag="L";
      }
      else
      {
        tItemFlag="C";
      }
      mLRPolSet= new LRPolSet();
      //查询所有对应保单周年的保单
      tSql = "select * from LCPol where to_char(signdate,'MM')='"+this.mToday.substring(5,7)+"' and to_char(signdate,'DD')='"+this.mToday.substring(8,10)+"' and ManageCom like '"+mGlobalInput.ManageCom+"%' and appflag='1' and signdate<='"+this.mToday+"' and riskcode in(select riskcode from lrrisk) ";
//      tSql = tSql+" and polno=''";
      tSql = tSql + "order by signdate,makedate,maketime";

      System.out.println(tSql);
      LCPolDB tLCPolDB = new LCPolDB();
      LCPolSet tLCPolSet = new LCPolSet();
      tLCPolSet=tLCPolDB.executeQuery(tSql);
      for (int i=1;i<=tLCPolSet.size();i++)
      {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema=tLCPolSet.get(i);
        //判断该保单是否失效
        if (EdorCalZT.JudgeLapse(tLCPolSchema.getRiskCode(),tLCPolSchema.getPaytoDate(),this.mToday,String.valueOf(tLCPolSchema.getPayIntv()),tLCPolSchema.getEndDate()))
        {
          //dealing with the policies whose signdate is not earlier than their insure end date(the policies of travel Accident, for instance).
          FDate tFDate = new FDate();
          Date tDate = tFDate.getDate(tLCPolSchema.getSignDate());
          Date tDate1 = tFDate.getDate(tLCPolSchema.getEndDate());
          if (tDate1.before(tDate))
          {
            continue;
          }
        }

        VData tVData = new VData();
        tVData.addElement(tLCPolSchema);
        tVData.addElement(tItemFlag);
        tVData.addElement(this.mReinsureCom);
        tVData.addElement(mToday);
        tVData.add(this.mLRPolSet);

        CalRetentBL tCalRetentBL = new CalRetentBL();
        if (tCalRetentBL.submitData(tVData,this.mOperate))
        {
          VData tResult = new VData();
          tResult=tCalRetentBL.getResult();
          LRPolSet tLRPolSet= new LRPolSet();
          tLRPolSet=(LRPolSet) tResult.getObjectByObjectName("LRPolSet",0);
          for (int LRPolCount=1;LRPolCount<=tLRPolSet.size();LRPolCount++)
          {
            LRPolSchema ttLRPolSchema = new LRPolSchema();
            ttLRPolSchema=tLRPolSet.get(LRPolCount);
            LRPolDB tLRPolDB = new LRPolDB();
            tLRPolDB.setSchema(ttLRPolSchema);
            System.out.println("Checking the insurance policy:"+ttLRPolSchema.getPolNo());
            if (!tLRPolDB.getInfo())
              this.mLRPolSet.add(ttLRPolSchema);
          }
        }
      }
      //处理责任终止保单
      tSql = "select * from LBPol where to_char(signdate,'MM')='"+this.mToday.substring(5,7)+"' and to_char(signdate,'DD')='"+this.mToday.substring(8,10)+"' and ManageCom like '"+mGlobalInput.ManageCom+"%' and appflag='1' and signdate<='"+this.mToday+"' and riskcode in(select riskcode from lrrisk) ";
      //tSql = tSql+" and insuredno='0000007177' and riskcode='112203' and polno in('86110020030210006693','86110020030210006694')";
      tSql = tSql + "order by signdate,makedate,maketime";

      System.out.println(tSql);
      LBPolDB tLBPolDB = new LBPolDB();
      LBPolSet tLBPolSet = new LBPolSet();
      tLBPolSet=tLBPolDB.executeQuery(tSql);
      Reflections tReflections = new Reflections();
      for (int i=1;i<=tLBPolSet.size();i++)
      {
        LBPolSchema tLBPolSchema = new LBPolSchema();
        tLBPolSchema=tLBPolSet.get(i);
        LCPolSchema tLCPolSchema = new LCPolSchema();

        tReflections.transFields(tLCPolSchema,tLBPolSchema);

        VData tVData = new VData();
        tVData.addElement(tLCPolSchema);
        tVData.addElement(tItemFlag);
        tVData.addElement(this.mReinsureCom);
        tVData.addElement(mToday);
        tVData.add(this.mLRPolSet);

        CalRetentBL tCalRetentBL = new CalRetentBL();
        if (tCalRetentBL.submitData(tVData,this.mOperate))
        {
          VData tResult = new VData();
          tResult=tCalRetentBL.getResult();
          LRPolSet tLRPolSet= new LRPolSet();
          tLRPolSet=(LRPolSet) tResult.getObjectByObjectName("LRPolSet",0);
          for (int LRPolCount=1;LRPolCount<=tLRPolSet.size();LRPolCount++)
          {
            LRPolSchema ttLRPolSchema = new LRPolSchema();
            ttLRPolSchema=tLRPolSet.get(LRPolCount);
            LRPolDB tLRPolDB = new LRPolDB();
            tLRPolDB.setSchema(ttLRPolSchema);
            System.out.println("Checking the insurance policy:"+ttLRPolSchema.getPolNo());
            if (!tLRPolDB.getInfo())
              this.mLRPolSet.add(ttLRPolSchema);
          }
        }
      }
      System.out.println("记录数："+this.mLRPolSet.size());
      this.prepareData();
      CalPolRetentBLS tCalPolRetentBLS = new CalPolRetentBLS();
      if (!tCalPolRetentBLS.submitData(mInputData,this.mOperate))
      {
        System.out.println("tCalLegalRetentBLS error out");
        this.mErrors.copyAllErrors(tCalPolRetentBLS.mErrors);
        CError tError = new CError();
        tError.moduleName = "CalRetentBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      return true;
    }



    private void prepareData()
      {
      this.mInputData.clear();
      this.mResult.clear();
      this.mInputData.add(this.mLRPolSet);
      this.mResult.addElement(this.mLRPolSet);
      }

      public VData getResult()
      {
        return mResult;
      }



  public static void main(String[] args) {
    CalPolRetentBL tCalPolRetentBL = new CalPolRetentBL();
   VData vData = new VData();
    GlobalInput tG = new GlobalInput();

    tG.Operator="001";
    tG.ManageCom="86";
    String bdate="2003-08-30";
    String edate="2003-08-30";
    vData.addElement(tG);
    vData.addElement(bdate);
    vData.addElement(edate);
    vData.addElement("1001");
    tCalPolRetentBL.submitData(vData, "CalComm");
//    tCalPolRetentBL.submitData(vData, "CalLegal");
  }
}