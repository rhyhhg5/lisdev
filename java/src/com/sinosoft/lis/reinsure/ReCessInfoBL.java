/*
 * <p>ClassName: ReCessInfoBL </p>
 * <p>Description: ReCessInfoBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Zhang Bin
 * @CreateDate：2006-07-30
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

public class ReCessInfoBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String strOperate;
    private LRCalFactorValueSet mLRCalFactorValueSet = new LRCalFactorValueSet();

    private LRContInfoSchema mLRContInfoSchema = new LRContInfoSchema();

    private MMap mMap = new MMap();

    private PubSubmit tPubSubmit = new PubSubmit();


    //业务处理相关变量
    /** 全局数据 */

    public ReCessInfoBL() {
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        ReCessInfoBL tReCessInfoBL = new ReCessInfoBL();
        try {

            vData.add(globalInput);
            tReCessInfoBL.submitData(vData, "INSERT");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //进行插入数据
        if (this.strOperate.equals("INSERT")) {
            if (!insertData()) {
                // @@错误处理
                buildError("insertData", "增加单证管理员时出现错误!");
                return false;
            }
        }
        //对数据进行修改操作
        if (this.strOperate.equals("UPDATE")) {
            if (!updateData()) {
                // @@错误处理
                buildError("updateData", "单证管理员信息有误!");
                return false;
            }
        }
        //对数据进行删除操作
        if (this.strOperate.equals("DELETE")) {
            if (!deleteData()) {
                // @@错误处理
                buildError("deleteData", "删除单证管理员时出现错误!");
                return false;
            }
        }
        return true;
    }

    /**
     * deleteData
     *
     * @return boolean
     */
    private boolean deleteData() {

        LRCalFactorValueSet tLRCalFactorValueSet = new LRCalFactorValueSet();
        LRCalFactorValueDB tLRCalFactorValueDB = new LRCalFactorValueDB();

        tLRCalFactorValueDB.setReContCode(mLRContInfoSchema.getReContCode());
        tLRCalFactorValueDB.setRiskSort(mLRContInfoSchema.getDiskKind());

        tLRCalFactorValueSet = tLRCalFactorValueDB.query();
        if (tLRCalFactorValueSet.size() == 0) {
            buildError("insertData", "不存在该分保信息!");
            return false;
        }
        mMap.put(tLRCalFactorValueSet, "DELETE");

        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("updateData", "修改时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;
        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData() {
        LRCalFactorValueSet tLRCalFactorValueSet = new LRCalFactorValueSet();
        LRCalFactorValueSet aLRCalFactorValueSet = new LRCalFactorValueSet();
        LRCalFactorValueDB tLRCalFactorValueDB = new LRCalFactorValueDB();

        tLRCalFactorValueDB.setReContCode(mLRContInfoSchema.getReContCode());
        tLRCalFactorValueDB.setRiskSort(mLRContInfoSchema.getDiskKind());

        aLRCalFactorValueSet = tLRCalFactorValueDB.query();
        if (aLRCalFactorValueSet.size() == 0) {
            buildError("updateData", "不存在该分保信息!");
            return false;
        }

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        for (int i = 0; i < mLRCalFactorValueSet.size(); i++) {
            LRCalFactorValueSchema tLRCalFactorValueSchema = new
                    LRCalFactorValueSchema();
            tLRCalFactorValueSchema = mLRCalFactorValueSet.get(i + 1);

            tLRCalFactorValueSchema.setManageCom(globalInput.ComCode);
            tLRCalFactorValueSchema.setOperator(globalInput.Operator);
            tLRCalFactorValueSchema.setMakeDate(currentDate);
            tLRCalFactorValueSchema.setMakeTime(currentTime);
            tLRCalFactorValueSchema.setModifyDate(currentDate);
            tLRCalFactorValueSchema.setModifyTime(currentTime);

            tLRCalFactorValueSet.add(tLRCalFactorValueSchema);
        }

        String strSQL = "delete from LRCalFactorValue where RecontCode='" +
                        mLRContInfoSchema.getReContCode() + "'"
                        ;
        mMap.put(strSQL, "DELETE");
        mMap.put(tLRCalFactorValueSet, "INSERT");

        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "保存再保公司信息时时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;

    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData() {
        System.out.println("come in insert........");
        LRCalFactorValueSet tLRCalFactorValueSet = new LRCalFactorValueSet();
        LRCalFactorValueSet aLRCalFactorValueSet = new LRCalFactorValueSet();
        LRCalFactorValueDB tLRCalFactorValueDB = new LRCalFactorValueDB();

        tLRCalFactorValueDB.setReContCode(mLRContInfoSchema.getReContCode());
        tLRCalFactorValueDB.setRiskSort(mLRContInfoSchema.getDiskKind());

        aLRCalFactorValueSet = tLRCalFactorValueDB.query();
        if (aLRCalFactorValueSet.size() != 0) {
            buildError("insertData", "已经添加了分保信息!");
            return false;
        }

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        for (int i = 0; i < mLRCalFactorValueSet.size(); i++) {
            LRCalFactorValueSchema tLRCalFactorValueSchema = new
                    LRCalFactorValueSchema();
            tLRCalFactorValueSchema = mLRCalFactorValueSet.get(i + 1);

            tLRCalFactorValueSchema.setManageCom(globalInput.ComCode);
            tLRCalFactorValueSchema.setOperator(globalInput.Operator);
            tLRCalFactorValueSchema.setMakeDate(currentDate);
            tLRCalFactorValueSchema.setMakeTime(currentTime);
            tLRCalFactorValueSchema.setModifyDate(currentDate);
            tLRCalFactorValueSchema.setModifyTime(currentTime);

            tLRCalFactorValueSet.add(tLRCalFactorValueSchema);
        }

        mMap.put(tLRCalFactorValueSet, "INSERT");

        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "保存再保公司信息时时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    public String createWorkNo(String ags) {
        String tWorkNo = "";
        if (ags.equals("RECOMCODE")) {
            tWorkNo = PubFun1.CreateMaxNo("RECOMCODE", 3);
        } else {
            tWorkNo = PubFun1.CreateMaxNo("RELATION", 6);
        }
        return tWorkNo;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLRContInfoSchema.setSchema((LRContInfoSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LRContInfoSchema", 0));
        this.mLRCalFactorValueSet.set((LRCalFactorValueSet) cInputData.
                                      getObjectByObjectName(
                                              "LRCalFactorValueSet", 0));
        return true;
    }

    public String getResult() {
        return mLRContInfoSchema.getReContCode();
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
