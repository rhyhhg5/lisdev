package com.sinosoft.lis.reinsure;

/**
 * Copyright (c) 2008 sinosoft  Co. Ltd.
 * All right reserved.
 */

import java.io.File;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.vschema.LRAccountsSet;
import com.sinosoft.lis.vschema.LRPolClmSet;
import com.sinosoft.lis.vschema.LRPolEdorSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.lis.db.LRAccountsDB;
import com.sinosoft.lis.db.LRPolClmDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.db.LRPolEdorDB;
import com.sinosoft.lis.f1print.PDFPrintBatchManagerBL;

/*
 * <p>ClassName: ReNewAccountPrintBL </p>
 * <p>Description: ReNewAccountPrintBL类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: Sun yu
 * @CreateDate：2008-10-11
 */
public class ReNewAccountPrintBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();
    private LRPolSet mLRPolSet=new LRPolSet();

    //业务处理相关变量
    /** 全局数据 */

    public ReNewAccountPrintBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (this.strOperate.equals("UPDATE") || this.strOperate.equals("CHECK")) {
            //准备往后台的数据
            if (!prepareOutputData()) return false;
            System.out.println("---End prepareOutputData---");

            System.out.println("Start PubSubmit BLS Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, strOperate)) {
              // @@错误处理
              this.mErrors.copyAllErrors(tPubSubmit.mErrors);
              return false;
            }
            System.out.println("End PubSubmit BLS Submit...");
          }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
    }
    
    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        LOPRTManagerSet mLOPRTManagerSet = new
        LOPRTManagerSet();
        for(int i=1;i<=this.mLRPolSet.size();i++){
            LRPolSchema tLRPolSchema=mLRPolSet.get(i);
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            String tLimit = PubFun.getNoLimit(this.globalInput.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLRPolSchema.getActuGetNo());//存放前台传来的caseno
            tLOPRTManagerSchema.setOtherNoType("19");
            tLOPRTManagerSchema.setCode("zbzddy01");
            tLOPRTManagerSchema.setManageCom(this.globalInput.ManageCom);
            tLOPRTManagerSchema.setAgentCode("");
            tLOPRTManagerSchema.setReqCom(this.globalInput.ManageCom);
            tLOPRTManagerSchema.setReqOperator(this.globalInput.ManageCom);
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            mLOPRTManagerSet.add(tLOPRTManagerSchema);
        }
        
        
        VData tVData = new VData();
        tVData.add(this.globalInput);
        tVData.add(mLOPRTManagerSet);
        PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
        if(!tPDFPrintBatchManagerBL.submitData(tVData,strOperate)){
             mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
             return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLRPolSet=(LRPolSet) cInputData.getObjectByObjectName(
                "LRPolSet", 0);
        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
    
    
}

