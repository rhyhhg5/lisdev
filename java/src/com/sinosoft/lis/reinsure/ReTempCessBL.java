package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.sql.PreparedStatement;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSetMetaData;

/*
 * <p>ClassName: ReTempCessBL </p>
 * <p>Description: 提取再保临时分保数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Liu Li
 * @CreateDate：2008-12-29
 */
public class ReTempCessBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    //临分合同信息
    private LRTempCessContSchema mLRTempCessContSchema = new
            LRTempCessContSchema();

    private MMap mMap = null;

    public ReTempCessBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串,为空串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRGetCessDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //处理已经合同分保的保单。
        if (!verifyContCess()) {
            return false;
        }
        if (!saveData()) {//保存数据
            return false;
        }
        //再保临分提数，向lrpol表置数
        if (!getCessData()) {
            return false;
        }
        if (!saveData()) {//保存数据
           return false;
       }
        return true;
    }
    /**
     * 保存数据
     * @return boolean
     */
    private boolean saveData(){
        if (!prepareOutputData()) {
           return false;
       }
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(mInputData, "")) {
           if (tPubSubmit.mErrors.needDealError()) {
               buildError("submitData", "处理临分时已经合同分保数据,出现错误!");
           }
           return false;
       }
           return true;
    }

    /**
     * verifyContCess
     * 处理已经合同提数或者合同分保的数据。
     * @return boolean
     */
    private boolean verifyContCess() {
        LRDutySet tLRDutySet = new LRDutySet();
        LRInterfaceSet tLRInterfaceSet = new LRInterfaceSet();
        SSRS tSSRS = null;
        mMap = new MMap();
        if (mLRTempCessContSchema.getContType().equals("1")) { //个单
            //个单，查询该保单需要分临分的保单，到polno级别
            String strPol =
                    "select a.polno,a.riskcode,a.renewcount,year(current date - a.CValidate) + 1 as polyear from lcpol a where conttype='1' and contno='" +
                    mLRTempCessContSchema.getContno() +
                    "' and exists (select 1 from LRTempCessContInfo b where b.TempContCode='" +
                    mLRTempCessContSchema.getTempContCode() +
                    "' and b.riskcode=a.riskcode) with ur ";
            tSSRS = new ExeSQL().execSQL(strPol);
        } else if (mLRTempCessContSchema.getContType().equals("2")) { //团单
            String strPol =
                    "select a.polno,a.riskcode,a.renewcount,year(current date - a.CValidate) + 1 as polyear " +
                    "from lcpol a,lccontplandutyparam c where a.grpcontno=c.grpcontno and " +
                    " a.grppolno=c.grppolno and a.riskcode=c.riskcode and a.contplancode=c.contplancode" +
                    " and  conttype='2' and a.grpcontno='" +
                    mLRTempCessContSchema.getContno() +
                    "' and c.contplancode in  (select b.contplancode from LRTempCessContInfo b " +
                    "where b.TempContCode='" +
                    mLRTempCessContSchema.getTempContCode() +
                    "' and b.riskcode=a.riskcode and b.Contplancode=c.contplancode) " +
                    " group by a.polno,a.riskcode,a.renewcount,a.CValidate with ur ";
            tSSRS = new ExeSQL().execSQL(strPol);
        }
        if (tSSRS.getMaxRow() <= 0) {
            buildError("verifyContCess", "保单查询出错！");
            return false;
        }

        //根据查询出需要临分的险种保单信息，查询这些信息在接口表中有没有提数
        int count = tSSRS.getMaxRow();
        for (int i = 1; i <= count; i++) {
//            String lcsql = "update lcpol set reinsureflag='3' where polno='"+tSSRS.GetText(i, 1)+"'";
//            mMap.put(lcsql,"UPDATE");  //更新lcpol表的分保标记，

            LRInterfaceDB ttLRInterfaceDB = new LRInterfaceDB();
            ttLRInterfaceDB.setPolNo(tSSRS.GetText(i, 1));
            ttLRInterfaceDB.setReNewCount(tSSRS.GetText(i, 3));
            ttLRInterfaceDB.setPolYear(tSSRS.GetText(i, 4));
            LRInterfaceSet ttLRInterfaceSet = null;
            ttLRInterfaceSet = ttLRInterfaceDB.query();
            //此保单已提数，则对已提数信息进行保单分保状态标记为临分。polstate＝'3'
            if (ttLRInterfaceSet.size()>0) {
                LRInterfaceSchema ttLRInterfaceSchema = ttLRInterfaceSet.get(1); //获取中间表该保单的合同分保信息
                ttLRInterfaceSchema.setPolStat("3"); //将该保单置为临时分保
                ttLRInterfaceSchema.setDutyDate(PubFun.getCurrentDate());
                tLRInterfaceSet.add(ttLRInterfaceSchema);
                mMap.put(ttLRInterfaceSchema, "UPDATE");
            } else { //此险种保单没有提数，则对保单进行提数。并置分保标记polstate＝'3'
                String sqlpolno = " select c.GrpContNo,c.GrpPolNo,c.ContNo,c.PolNo,c.ProposalNo,c.PrtNo,c.ReNewCount,c.ContType,c.ManageCom,"
                                  + "c.MainPolNo,c.MasterPolNo,c.RiskCode,c.RiskVersion,c.InsuredNo,c.InsuredName,c.InsuredSex,c.InsuredBirthday,"
                                  + "c.InsuredAppAge,c.Years,c.CValiDate,c.EndDate,c.StandPrem,c.Prem,c.SumPrem,c.Amnt,c.OccupationType,c.PayIntv,"
                                  + "c.PayMode,c.PayYears,c.PayEndYearFlag,c.PayEndYear,c.InsuYearFlag,c.InsuYear,'F','3',c.SignDate,c.AppntNo," //F是临时分保，3是临分标记
                                  + "c.AppntName,'',c.SaleChnl,'11',"
                                  + "c.PayToDate, current date, '0',"
                                  + " year(current date - CValidate) + 1, c.agentgroup, c.cessamnt,current date,current time, '000000', '" +
                                  globalInput.Operator + "',"
                                  + " c.MakeDate,c.MakeTime, c.ModifyDate, c.ModifyTime"
                                  + "  from lcpol c"
                                  + " where c.AppFlag = '1'"
//                        "   and (c.stateFlag in ('0', '1') or c.StateFlag is null) "
//                                  +
//                        "   and (reinsureflag <> '4' or reinsureflag is null) "+  //去掉该条件限制,临时分保时有可能已经合同分保.
                                  + "and c.polno='" + tSSRS.GetText(i, 1) +
                                  "' with ur";
                System.out.println("lrinterface:::"+sqlpolno);
                Connection con = DBConnPool.getConnection();;
                Statement stmt = null;
                ResultSet tRs = null;
                PreparedStatement pstmt = null;
                try {
                	try{
                		con.setAutoCommit(false);
                	}catch(Exception ex){
                		System.err.println("set AutoCommit Exception!");
                	}
                	pstmt = con.prepareStatement(StrTool.GBKToUnicode(sqlpolno),
                            ResultSet.TYPE_FORWARD_ONLY
                            , ResultSet.CONCUR_READ_ONLY);
               	 tRs = pstmt.executeQuery();
               	 if (insert(tRs, "LRInterface", con) == false) {
                    	con.rollback();
                        con.close();
                        stmt.close();
                        tRs.close();
                        pstmt.close();
                        return false;
                    }else{
                    	con.commit();
                        con.close();
                        stmt.close();
                        tRs.close();
                        pstmt.close();
                    }
                } catch (Exception e) {
                    try {
                        tRs.close();
                        stmt.close();
                        con.close();
                        pstmt.close();
                        return false;
                    } catch (Exception ex) {}
                }
            }
        }

        String sqlduty = "select getdutycode from LRTempCessContInfo b where exists (select 1 from LRTempCessCont a where a.tempcontcode=b.tempcontcode and a.ReType='02' and a.tempcontcode='" +
                         mLRTempCessContSchema.getTempContCode() +
                         "') and b.getdutycode!='A' with ur";
        String getduty = new ExeSQL().getOneValue(sqlduty);
        //如果是责任分保，处理该保单下有责任分保的提数
        if (getduty != null && !getduty.equals("")) {
            for (int j = 1; j <= count; j++) {
                LRDutyDB ttLRDutyDB = new LRDutyDB();
                ttLRDutyDB.setGetDutyCode(getduty);
                ttLRDutyDB.setPolNo(tSSRS.GetText(j, 1));
                String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(j, 2)+"' with ur";
                String LRiskFlag = new ExeSQL().getOneValue(aSql);
                if(Integer.parseInt(LRiskFlag)>0){//长险存保单年度
                    ttLRDutyDB.setPolYear(tSSRS.GetText(j, 4));
                }else{//短险存续保次数
                    ttLRDutyDB.setPolYear(tSSRS.GetText(j, 3));
                }
                ttLRDutyDB.setGetDutyCode(getduty);
                LRDutySet ttLRDutySet = null;
                ttLRDutySet = ttLRDutyDB.query();
                if (ttLRDutySet.size()>0) {
                    LRDutySchema ttLRDutySchema = ttLRDutySet.get(1);
                    ttLRDutySchema.setReinsureFlag("3"); //将该保单责任信息置为临时分保
                    tLRDutySet.add(ttLRDutySchema);
                    mMap.put(ttLRDutySchema, "UPDATE");
                } else { //如果责任没有信息，则进行责任提数a
                    String sqlgetd = "select a.PolNo,case when (select riskperiod from lmriskapp c where c.riskcode=b.riskcode)='L'  then b.polyear else b.renewcount end, a.GetDutyCode, a.ContNo,a.GrpContNo,b.Grppolno,a.InsuredNo,b.riskcode,a.standmoney, '0',"
                                     +
                            "current date, current time, a.ModifyDate, a.ModifyTime,'" +
                                     globalInput.Operator + "', '000000','3',a.getdutycode "
                                     +
                            "from lcget a, lrinterface b where  a.polno = b.polno and a.contno = b.contno " +
                            " and a.grpcontno = b.grpcontno  and a.polno='" +
                                     tSSRS.GetText(j, 1) + "' and exists "
                                     + " (select 1 from LRTempCessContInfo where getdutycode = a.getdutycode and tempcontcode='" +
                            mLRTempCessContSchema.getTempContCode()+
                                     "') with ur  ";
                    Connection con = DBConnPool.getConnection();;
                    Statement stmt = null;
                    ResultSet tRs = null;
                    try {
                        stmt = con.createStatement();
                        tRs = stmt.executeQuery(sqlgetd);
                        if (insert(tRs, "LRDuty", con) == false) {
                            con.close();
                            stmt.close();
                            tRs.close();
                            return false;
                        }else{
                            con.close();
                            stmt.close();
                            tRs.close();
                        }
                    } catch (Exception e) {
                        try {
                            tRs.close();
                            stmt.close();
                            con.close();
                            return false;
                        } catch (Exception ex) {}
                    }
                }
            }
        }

        if (!verifyPolCess(tLRInterfaceSet)) { //处理lrpol表中的提数，重新提一批反冲数据
            return false;
        }
        return true;
    }

    /**
     * verifyPolCess
     * 提取相对应于lrpol表的反冲数据,
     * 在lrpol表中新插入记录，除ReinsureItem，GetDataDate字段外，其它记录一样。
     * @return boolean
     */
    private boolean verifyPolCess(LRInterfaceSet tLRInterfaceSet) {
        LRInterfaceSet ttLRInterfaceSet = tLRInterfaceSet;
        LRPolDB tLRPolDB = new LRPolDB();
        LRPolSet tLRPolSet = new LRPolSet();
        for (int m = 1; m <= ttLRInterfaceSet.size(); m++) {
            String sqlper = "select Riskperiod from lmriskapp where riskcode='" +
                            ttLRInterfaceSet.get(m).getRiskCode() + "'";
            String period = new ExeSQL().getOneValue(sqlper);
            if (period == null) {
                buildError("verifyPolCess", "查询险种类型（长险OR短险）出错");
            }
            tLRPolDB.setPolNo(ttLRInterfaceSet.get(m).getPolNo());
            tLRPolDB.setReinsureItem("C");//只提取合同分保的数据.
            //查询lrpol表数据时，如果长险，则赋值ReNewCount=接口表的保单年度，如果是短险，则赋值ReNewCount=接口表的续保次数。
            if (period.equals("L")) {
                 tLRPolDB.setReNewCount(ttLRInterfaceSet.get(m).getPolYear());
            } else {
                 tLRPolDB.setReNewCount(ttLRInterfaceSet.get(m).
                                           getReNewCount());
             }
            LRPolSet ttLRPolSet = new LRPolSet();
            ttLRPolSet = tLRPolDB.query(); //获取中间表该保单的合同分保信息
            tLRPolSet.add(ttLRPolSet);
        }
        if (tLRPolSet.size() > 0) {
            for (int i = 1; i <= tLRPolSet.size(); i++) {
                tLRPolSet.get(i).setReinsureItem("F"); //标记为这条记录是之前记录的反冲数据
                tLRPolSet.get(i).setGetDataDate(PubFun.getCurrentDate()); //反冲时分保日期是当前临分提数日期。
                tLRPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                tLRPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                //还需要修改部分字段值，待更改中＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
            }
            mMap.put(tLRPolSet, "DELETE&INSERT"); //插入分保的反冲摊回钱数。
        }
        return true;
    }

    /**
     * 提取分保数据
     * @return boolean
     */
    private boolean getCessData() {
        mMap = new MMap();
        String tSql = "select * from lrinterface a where  contno='"+mLRTempCessContSchema.getContno()+"' "+
                      "and conttype = '1'and dutydate = current date and polstat ='3'and State = '0' "+
                      " union "+
                      "select * from lrinterface where grpcontno = '"+mLRTempCessContSchema.getContno()+"' "+
                      "and conttype = '2' and dutydate = current date and polstat = '3' "+
                      "and State = '0' with ur";
        LRInterfaceDB tLRInterfaceDB = new LRInterfaceDB();
        LRInterfaceSet tLRInterfaceSet = tLRInterfaceDB.executeQuery(tSql);
        if (tLRInterfaceSet.size()<=0) {
            buildError("getCessData-tSql","接口表中查询临分数据为空！");
            return false;
        }

        LRPolSet tLRPolSet = new LRPolSet();
        DealRiskAmntBL tDealRiskAmntBL = new DealRiskAmntBL(PubFun.getCurrentDate());
//        DealRiskAmntBL tDealRiskAmntBL = new DealRiskAmntBL("2008-10-10");
        for (int i = 1; i <= tLRInterfaceSet.size(); i++) {
            LRInterfaceSchema ttLRInterfaceSchema = tLRInterfaceSet.get(i);
            LRPolSchema tLRPolSchema = new LRPolSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLRPolSchema, ttLRInterfaceSchema); //数据复制
            tLRPolSchema.setReContCode(mLRTempCessContSchema.getTempContCode()); //临分合同编号
            String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+ttLRInterfaceSchema.getRiskCode()+"' with ur";
            String LRiskFlag = new ExeSQL().getOneValue(aSql);
            if(Integer.parseInt(LRiskFlag)>0){
               tLRPolSchema.setReNewCount(ttLRInterfaceSchema.getPolYear());//长险置保单年度+++
            }else{
               tLRPolSchema.setReNewCount(ttLRInterfaceSchema.getReNewCount()); //短信置续保次数+++
           }
            tLRPolSchema.setReinsureItem("T"); //借用再保项目字段标记临分
            tLRPolSchema.setTempCessFlag("Y"); //临分标记，合同标记为“N”
            tLRPolSchema.setRiskCalSort(mLRTempCessContSchema.getCessionMode()); //险种计算方式，1为成数，2为溢额
            tLRPolSchema.setCessStart(""); //============
            tLRPolSchema.setCessEnd("");//==============
            tLRPolSchema.setReType(mLRTempCessContSchema.getReType());  //分保类型
            String agentGroup = ttLRInterfaceSchema.getAgentGroup();
            if(agentGroup != null&& !agentGroup.equals(""))
             {
                  String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                                +agentGroup+"' )with ur";
                  String CostCenter = new ExeSQL().getOneValue(sql);
                   tLRPolSchema.setCostCenter(CostCenter);//成本中心
             }
            if (mLRTempCessContSchema.getReType().equals("01")) { //按险种分保
                if (mLRTempCessContSchema.getCessionMode().equals("2")) { //溢额分保
                    double sumAmnt = tDealRiskAmntBL.accAmnt(ttLRInterfaceSchema,
                            mLRTempCessContSchema.getTempContCode()); //获取累计保额
                    tLRPolSchema.setRiskAmnt(sumAmnt); //临分时，借用风险保额存储累计原始保额
                    double sumRiskAmnt = tDealRiskAmntBL.accRiskAmnt(
                            ttLRInterfaceSchema, mLRTempCessContSchema.getTempContCode()); //获取累计风险保额
                    tLRPolSchema.setSumRiskAmount(sumRiskAmnt); //累计风险保额
                    double sumCessAmntTemp = tDealRiskAmntBL.sumCessAmnt(
                            ttLRInterfaceSchema, mLRTempCessContSchema.getTempContCode());
                    tLRPolSchema.setNowRiskAmount(sumCessAmntTemp); //累计已分出保额
                    double tAddPremRate = getPolAddPremRate(ttLRInterfaceSchema.
                            getStandPrem(), ttLRInterfaceSchema.getPrem()); //得到加费率,
                    tLRPolSchema.setDiseaseAddFeeRate(tAddPremRate);
                    tLRPolSchema.setDeadAddFeeRate(tAddPremRate);

                    double minRetainAmnt = getCalMode(); //获取最低分保保额
                    double maxRetainAmnt = getRetainAmnt(); //获取最高分保保额
                    double cessAmnt;
                    if (mLRTempCessContSchema.getCessionFeeMode().equals("1")) {
                        cessAmnt = Math.min(sumRiskAmnt, maxRetainAmnt) -
                                   sumCessAmntTemp - minRetainAmnt;
                    } else {
                        cessAmnt = Math.min(sumAmnt, maxRetainAmnt) -
                                   sumCessAmntTemp - minRetainAmnt;
                    }
                    if(cessAmnt<0){
                        cessAmnt=0;  //如果分保保额计算为负,则置分保保费为0
                    }
                    tLRPolSchema.setCessAmnt(cessAmnt); //分保保额
//                    String lccess = "update lcpol set cessamnt="+cessAmnt+"  where polno='"+tLRPolSchema.getPolNo()+"'";
//                    mMap.put(lccess,"UPDATE");  //置lcpol表中的分保金额
                } else { //成数分保
                   insertPolDetailData(ttLRInterfaceSchema);//提取保单交费信息
                }
            } else if (mLRTempCessContSchema.getReType().equals("02")) { //按责任分保,责任分保只支持溢额分保
                //查询该合同该保单需要分保的责任.lrpol的renewcount字段短险存续保次数，长险存保单年度。按保额高低排序，这样如果最低保额<该责任的保额，则第一个责任会全部分保。
                String sqla = "select * from lrduty a where reinsureflag='3' and exists (select 1 from LRTempCessContInfo b " +
                              "where b.riskcode=a.riskcode and b.GetDutyCode=a.GetDutyCode and b.TempContCode='" +
                              mLRTempCessContSchema.getTempContCode() + "') " +
                              "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod='L') and polno='" + ttLRInterfaceSchema.getPolNo() +
                              "' and PolYear=" + ttLRInterfaceSchema.getPolYear() +
                              " union all "+
                              "select * from lrduty a where reinsureflag='3' and exists (select 1 from LRTempCessContInfo b " +
                              "where b.riskcode=a.riskcode and b.GetDutyCode=a.GetDutyCode and b.TempContCode='" +
                              mLRTempCessContSchema.getTempContCode() + "') " +
                              "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod!='L') and polno='" + ttLRInterfaceSchema.getPolNo() +
                              "' and PolYear=" + ttLRInterfaceSchema.getReNewCount() +
                              " with ur";
                System.out.println(sqla);
                LRDutySet tLRDutySet = new LRDutyDB().executeQuery(sqla);
                double sumAmnt = 0; //所有分保责任的总保额
                if(tLRDutySet.size()>0){
                    sumAmnt = tLRDutySet.get(1).getAmnt();
                }
                double minRetainAmnt = getCalMode(); //获取最低分保保额
                double maxRetainAmnt = getRetainAmnt(); //获取最高分保保额
                double cessamnt = Math.min(sumAmnt, maxRetainAmnt) -
                                  minRetainAmnt; //分保保额＝min（）－最低
                if(cessamnt<0){
                    cessamnt=0;
                }
                tLRPolSchema.setCessAmnt(cessamnt); //分保保额
//                String lccess = "update lcpol set cessamnt="+cessamnt+"  where polno='"+tLRPolSchema.getPolNo()+"'";
//                mMap.put(lccess,"UPDATE");  //置lcpol表中的分保金额
            }
            tLRPolSchema.setGetDataDate(PubFun.getCurrentDate()); //分保日期为临分提数日期
            tLRPolSchema.setOperator(globalInput.Operator);
            tLRPolSchema.setMakeDate(PubFun.getCurrentDate());
            tLRPolSchema.setMakeTime(PubFun.getCurrentTime());
            tLRPolSchema.setModifyDate(PubFun.getCurrentDate());
            tLRPolSchema.setModifyTime(PubFun.getCurrentTime());
            tLRPolSet.add(tLRPolSchema);
        }

        mMap.put(tLRPolSet,"DELETE&INSERT");
        return true;
    }

    /**
     * getRetainAmnt
     * 获取最高分保保额
     * @return double
     */
    private double getRetainAmnt() {
        String strSQL =
                "select distinct RetainAmnt from LRTempCessContInfo where TempContCode='" +
                mLRTempCessContSchema.getTempContCode() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0 || tSSRS.getMaxRow() > 1) {
            return 9999999999.99; //返回无穷大。
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * getCalMode
     * 获取最低分保保额
     * @return double
     */
    private double getCalMode() {
        String strSQL =
                "select CalMode from LRTempCessContInfo where TempContCode='" +
                mLRTempCessContSchema.getTempContCode() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0; //返回0
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 根据结果查询结果向指定表中插入数据
     * @return boolean
     */
    private boolean insert(ResultSet rs, String table, Connection con) {
        Statement stmt = null;
        ResultSet tRs = null;
        ResultSetMetaData rsmd = null;

        ResultSetMetaData rsmd1 = null;
        try {
        	
        	PreparedStatement pstmt = null;
        	String tSql = "select * from " + table + " fetch first 1 rows only";
        	 pstmt = con.prepareStatement(StrTool.GBKToUnicode(tSql),
                     ResultSet.TYPE_FORWARD_ONLY
                     , ResultSet.CONCUR_READ_ONLY);
        	 tRs = pstmt.executeQuery();
        	 rsmd = tRs.getMetaData();

//            stmt = con.createStatement();
//            tRs = stmt.executeQuery("select * from " + table +
//                                    " fetch first 1 rows only");
//            rsmd = tRs.getMetaData();
            
            rsmd1 = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            int columnCount1 = rsmd1.getColumnCount();
            if (columnCount != columnCount1) {
                this.mErrors.addOneError("与目标的列数不符");
                return false;
            }
            System.out.println("begin......");
            while (rs.next()) {
                StringBuffer insertSql = new StringBuffer();
                StringBuffer whereSql = new StringBuffer();
                insertSql.append("insert into " + table + " (");
                int flag = 0;
                for (int i = 0; i < columnCount; i++) {
                    if (flag == 1) {
                        insertSql.append(",");
                        whereSql.append(",");
                    }
                    insertSql.append(rsmd.getColumnName(i + 1));

                    int mDataType = rsmd.getColumnType(i + 1);
//                  判断数据类型
                    if ((mDataType == Types.CHAR) ||
                        (mDataType == Types.VARCHAR)) {
                        if (rs.getString(i + 1) == null) {
                            whereSql.append(rs.getObject(i + 1));
                        } else {
                            whereSql.append("'");
                            whereSql.append(StrTool.cTrim(rs.getString(i + 1)));
                            whereSql.append("'");
                        }
                    } else if ((mDataType == Types.TIMESTAMP) ||
                               (mDataType == Types.DATE)) {
                        if (rs.getDate(i + 1) == null) {
                            whereSql.append(rs.getObject(i + 1));
                        } else {
                            whereSql.append("'");
                            whereSql.append(rs.getDate(i + 1));
                            whereSql.append("'");
                        }
                    } else {
                        whereSql.append(rs.getObject(i + 1));
                    }
                    flag = 1;
                }
                insertSql.append(") values ( ");
                insertSql.append(whereSql.toString());
                insertSql.append(")");
                String ExeSql = insertSql.toString();
                try {
                    System.out.println(ExeSql);
                    ExeSQL tExeSQL =new ExeSQL(con);
                    tExeSQL.execUpdateSQL(ExeSql);
//                    stmt.executeUpdate(ExeSql);
                } catch (Exception insertExc) {
                    System.out.println("Error Insert......" + ExeSql);
                }
            }
//            stmt.close();
            try {
            	pstmt.close();
            	tRs.close();
            }catch(Exception ex){}
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            this.mErrors.addOneError("插入语句拼写错误");
            return false;
        }
        return true;
    }

    /**
     * 提取本期合同分保保单的过往缴费信息 ConfDate < this.mStartDate ,并且由于提数的延迟性，部分数据可能被上期提取到数据库中，因此用Delete&Insert
     * @return boolean
     */
    private boolean insertPolDetailData(LRInterfaceSchema tLRInterfaceSchema) {
        String tSql = "select PolNo,PayCount,PayNo,ConfDate,sum(SumActuPayMoney),LastPayToDate,CurPayToDate "+
            "from LJAPayPerson a where  PayCount = 1  "+
            "and exists (select 1 from lcpol where conttype='1' and a.polno='"+tLRInterfaceSchema.getPolNo()+"' "+
            "and PolNo = a.PolNo and a.LastPayToDate >= Cvalidate and a.CurPayToDate <= EndDate) "+
            "group by  polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate "+
            "union all "+
            "select PolNo,PayCount,PayNo,ConfDate,sum(SumActuPayMoney),LastPayToDate,CurPayToDate  "+
            "from LJAPayPerson a where  PayCount = 1  "+
            "and exists (select 1 from lbpol where conttype='1' and a.polno='"+tLRInterfaceSchema.getPolNo()+"' "+
            "and PolNo = a.PolNo and a.LastPayToDate >= Cvalidate and a.CurPayToDate <= EndDate) "+
            "group by  polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate "+
            //团单,09年开始团单不可以续保,为了兼容lastpaytodate<cvalidate的情况，取消此条件限制
            "union all  "+
            "select PolNo,PayCount,PayNo,ConfDate,sum(SumActuPayMoney),LastPayToDate,CurPayToDate  "+
            "from LJAPayPerson a where  PayCount = 1  "+
            "and exists (select 1 from lcpol where conttype='2' and a.polno='"+tLRInterfaceSchema.getPolNo()+"'  "+
            "and PolNo = a.PolNo ) "+
            "and not exists (select 1 from ljagetendorse c where c.polno=a.polno and c.feeoperationtype = 'NI') "+
            "group by  polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate "+
            "union all  "+
            "select PolNo,PayCount,PayNo,ConfDate,sum(SumActuPayMoney),LastPayToDate,CurPayToDate "+
            "from LJAPayPerson a where  PayCount = 1  "+
            "and exists (select 1 from lbpol where conttype='2' and a.polno='"+tLRInterfaceSchema.getPolNo()+"' "+
            "and PolNo = a.PolNo )  "+
            "and not exists (select 1 from ljagetendorse c where c.polno=a.polno and c.feeoperationtype = 'NI')  "+
            "group by  polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate  "+
            "union all "+
            //增人,关联保全交费信息提取
            "select a.PolNo,PayCount,PayNo,ConfDate,sum(x.getMoney),LastPayToDate,CurPayToDate  "+
            "from LJAPayPerson a,ljagetendorse x where a.payno = x.actugetno and a.polno=x.polno and x.polno ='"+tLRInterfaceSchema.getPolNo()+"' "+
            "and PayCount = 1 and x.feeoperationtype = 'NI' and a.riskcode=x.riskcode and "+
            "(exists (select 1 from lcpol where polno= x.polno ) "+
            " or exists (select 1 from lbpol where polno=x.polno)) "+
            "group by a.polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate   "+
            "with ur ";

        SSRS tSSRS = new ExeSQL().execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LRPolDetailSchema tLRPolDetailSchema = new LRPolDetailSchema();
            tLRPolDetailSchema.setGrpContNo(tLRInterfaceSchema.getGrpContNo());
            tLRPolDetailSchema.setGrpPolNo(tLRInterfaceSchema.getGrpPolNo());
            tLRPolDetailSchema.setContNo(tLRInterfaceSchema.getContNo());
            tLRPolDetailSchema.setPolNo(tLRInterfaceSchema.getPolNo());
            tLRPolDetailSchema.setProposalNo(tLRInterfaceSchema.getProposalNo());
            tLRPolDetailSchema.setPrtNo(tLRInterfaceSchema.getPrtNo());
            tLRPolDetailSchema.setManageCom(tLRInterfaceSchema.getManageCom());
            tLRPolDetailSchema.setContType(tLRInterfaceSchema.getContType());
            tLRPolDetailSchema.setReNewCount(tLRInterfaceSchema.getReNewCount());
            tLRPolDetailSchema.setPayCount(tSSRS.GetText(i, 2));
            tLRPolDetailSchema.setPayNo(tSSRS.GetText(i,3));
            if (tSSRS.GetText(i, 4) != null && !tSSRS.GetText(i, 4).equals("")) {
                tLRPolDetailSchema.setPayConfDate(tSSRS.GetText(i, 4));
            } else {
                tLRPolDetailSchema.setPayConfDate(PubFun.getCurrentDate()); //如果ConfData为空，则置当前日期
            }
            tLRPolDetailSchema.setPayMoney(tSSRS.GetText(i, 5));
            tLRPolDetailSchema.setRiskCode(tLRInterfaceSchema.getRiskCode());
            tLRPolDetailSchema.setRiskVersion(tLRInterfaceSchema.getRiskVersion());
            tLRPolDetailSchema.setLastPayToDate(tSSRS.GetText(i, 6));
            tLRPolDetailSchema.setCurPayToDate(tSSRS.GetText(i, 7));
            tLRPolDetailSchema.setGetDataDate(tSSRS.GetText(i, 4));
            tLRPolDetailSchema.setRiskCalSort("0"); //废弃该字段,因此设置为默认值0
            tLRPolDetailSchema.setOperator(globalInput.Operator);
            tLRPolDetailSchema.setMakeDate(PubFun.getCurrentDate());
            tLRPolDetailSchema.setMakeTime(PubFun.getCurrentTime());
            tLRPolDetailSchema.setModifyDate(PubFun.getCurrentDate());
            tLRPolDetailSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLRPolDetailSchema, "DELETE&INSERT");
        }
        return true;
    }

    /**
     * 计算死亡加费，重疾加费,这里由于契约不区分死亡加费和重疾加费，所以统一计算并存储于DiseaseAddFeeRate
     * @return double
     */
    private double getPolAddPremRate(double aStandPrem, double aPrem) {
        if (Math.abs(aStandPrem - 0.00) < 0.00001 || (aPrem - aStandPrem) <= 0) {
            return 0;
        }
        return (aPrem - aStandPrem) / aStandPrem;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(String aPolNo, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aPolNo);
        tLRErrorLogSchema.setLogType("2");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        TransferData tTransfer = new TransferData();
        tTransfer = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        String tempcode = (String) tTransfer.getValueByName("TempContCode");

        LRTempCessContDB tLRTempCessContDB = new LRTempCessContDB();
        tLRTempCessContDB.setTempContCode(tempcode);
        LRTempCessContSet tLRTempCessContSet = new LRTempCessContSet();
        tLRTempCessContSet = tLRTempCessContDB.query();
        if (tLRTempCessContSet.size() > 0) {
            mLRTempCessContSchema = tLRTempCessContSet.get(1); //获取临分合同信息
        }
        return true;
    }

    public String getResult() {
        return mLRTempCessContSchema.getTempContCode();
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
        ReTempCessBL tReTempCessBL = new ReTempCessBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("TempCessCode", "");

        VData tVData = new VData();
        tVData.addElement(globalInput);
        tVData.addElement(tTransferData);

        tReTempCessBL.submitData(tVData, "");
    }
}
