/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import java.util.Hashtable;

import weblogic.soap.util.StrUtil;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LRCalCessDataBL </p>
 * <p>Description: LRCalCessDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2008-11-20
 */
public class LRNewCalCessDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    private double mAccQuot; //接受份额
    private double mManageFeeRate; //分保管理费费率
    //用于毛保费成数分保
    private double mCessionRate; //分保比例
    private double mReProcFeeRate; //再保手续费费率
    private double mSpeCemmRate; //特殊佣金费率
    //用于溢额分保
    private double mCessionAmount; //分保保额
    private double mCessPremRate; //分保费率
    private double mChoiRebaFeeRate; //折扣比例
    private double mCessAmnt;
    private Hashtable m_hashLMCheckField = new Hashtable();
    private String mReContCode = "";
    
    private	String mRiskCode = "";
    private	String mGrpContNo = "";
    private	String mContNo = "";
    private	String mPolNo = "";
    private	String mOperator = "";
    private String mSQL = "";
    //业务处理相关变量
    DealRiskAmntBL tDealRiskAmntBL = new DealRiskAmntBL();
    /** 全局数据 */

    public LRNewCalCessDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }
    
    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT(VData cInputData, String cOperate,String mToDay) {
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputDataT(cInputData,mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRNewCalCessDataBL tLRNewCalCessDataBL = new LRNewCalCessDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2017-1-1");
            tTransferData.setNameAndValue("EndDate", "2017-1-1");
            tTransferData.setNameAndValue("ReContCode", "");
            vData.add(tTransferData);
            tLRNewCalCessDataBL.submitData(vData, "CESSDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRCalCessDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 分类计算分保数据
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取分保数据
        System.out.println("Come to calCessData()...............");
        
        this.mMap = new MMap();

        if (!calCessData1()) {
            buildError("calCessData1", "再保计算成数分保失败!");
        }
//        if (!calCessData2()) {
//            buildError("calCessData2", "再保计算溢额分保失败!");
//        }
        if (!calCessData3()) {
            buildError("calCessData3", "再保计算临时分保失败!");
        }
        if (!calCessData4()) {
            buildError("calCessData4", "再保计算风险溢额分保失败!");
        }
        return true;

    }
    public LRCalFactorValueSchema findCheckFieldByRiskCode(String strRiskCode, String tReContCode,String tFactorCode) {

        Hashtable hash = m_hashLMCheckField;
        String strPK = strRiskCode.trim() + tReContCode.trim()+tFactorCode.trim();
        Object obj = hash.get(strPK);

        if( obj != null ) {
          if( obj instanceof String ) {
            return null;
          } else {
            return (LRCalFactorValueSchema)obj;
          }
        } else {
        	LRCalFactorValueDB tLRCalFactorValueDB = new LRCalFactorValueDB();

        	tLRCalFactorValueDB.setRiskCode(strRiskCode);
        	tLRCalFactorValueDB.setReContCode(tReContCode);
        	tLRCalFactorValueDB.setFactorCode(tFactorCode);
        	LRCalFactorValueSet tLRCalFactorValueSet=tLRCalFactorValueDB.query();
        	if(tLRCalFactorValueSet.size()<=0){
        		hash.put(strPK, "NOFOUND");
        		return null;
        		
        	}
          LRCalFactorValueSchema tLRCalFactorValueSchema = tLRCalFactorValueSet.get(1);

          if( tLRCalFactorValueDB.mErrors.needDealError() ) {
            mErrors.copyAllErrors(tLRCalFactorValueDB.mErrors);
            hash.put(strPK, "NOFOUND");
            return null;
          }

          hash.put(strPK, tLRCalFactorValueSchema);
          return tLRCalFactorValueSchema;
        }
      }
    /**
     * 成数分保计算
     * @return boolean
     */
    private boolean calCessData1() {
        //先处理提数区间内承保的医疗险,根据提数规则,可能出现财务已经缴费但是尚未提取到LRPol中的情况
        //因此先对处于提数期间的LRPol表中的医疗险,LRPolDetail表中截止日期前的缴费进行计算
    	System.out.println("jinlai1");
        String strSQL =
                "select * from LRPol a where " +
                "  a.GetDataDate between '" +
                mStartDate + "' and '" + mEndDate +
                "' and a.TempCessFlag = 'N'"  ;
    	strSQL+=" and exists (select 1 from LRContInfo where RecontCode = " +
		"a.RecontCode and CessionMode = '1' and recontstate='01') "
		+ " and not exists(select 1 from lrpolresult where  a.polno = polno and a.recontcode = recontcode and a.reinsureitem = reinsureitem and a.renewcount = renewcount and a.getdatadate = cessstartdate  and a.payno = payno)  ";
    	strSQL += mSQL;
        strSQL+=" with ur ";
        LRPolSet tLRPolSet = new LRPolSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolSet, strSQL);

        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolSet.size(); i++) {
                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                    this.mCessionRate = tLRPolSchema.getDeadAddFeeRate(); //成数分保比例
                    this.mAccQuot = getAccQuot(tLRPolSchema); //接受份额
                    this.mReProcFeeRate = getReProcFeeRate(tLRPolSchema); //再保手续费比例
                    //09/04/30 5503的手续费费率按保费的折扣比例不同而不同
                    if(tLRPolSchema.getRiskCode().equals("5503")&& tLRPolSchema.getStandPrem()>0)
                    {
                       double FloatRateFee = tLRPolSchema.getPrem()/tLRPolSchema.getStandPrem();
                       if(FloatRateFee>=0.2 && FloatRateFee <1){
                           this.mReProcFeeRate = 0.21+FloatRateFee*0.5;
                       }else if (FloatRateFee >=1){
                           this.mReProcFeeRate = 0.71;
                       }
                    }
                    this.mSpeCemmRate = this.getSpeCemmRate(tLRPolSchema); //特殊佣金比例
                    this.mManageFeeRate = this.getManageFeeRate(tLRPolSchema); //管理费比例
                    
                    
                        LRPolResultSchema tLRPolResultSchema = new
                                LRPolResultSchema();
                        this.preLRPolResultC(tLRPolSchema,
                        			               tLRPolResultSchema);
                        this.mMap.put(tLRPolResultSchema.getSchema(),
                                      "DELETE&INSERT");

                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
        return true;
    }


    /**
     * 溢额分保计算
     * @return boolean
     */
    private boolean calCessData2() {
        String strSQL =
                "select * from LRPol a where (select CessionMode from LRContInfo where RecontCode = a.RecontCode) = '3' and a.GetDataDate between '" +
                mStartDate + "' and '" + mEndDate +
                "' and a.TempCessFlag = 'N' " +
                " with ur";
        LRPolSet tLRPolSet = new LRPolSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolSet, strSQL);

        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolSet.size(); i++) {
                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                   //查询该合同是按责任还是险种分保
                    strSQL =
                            "select ReType from Lrcontinfo where recontcode = '" +
                            tLRPolSchema.getReContCode() + "'";
                    String ReType = new ExeSQL().getOneValue(strSQL);

                    if (ReType.equals("01")) { //如果按险种分保，则处理如下
                        this.mAccQuot = getAccQuot(tLRPolSchema); //接受份额
                        this.mChoiRebaFeeRate = this.getChoiRebaFeeRate(
                             tLRPolSchema); //选择折扣比例
                        this.mManageFeeRate = this.getManageFeeRate(tLRPolSchema); //管理费比例

                        String CessPremModeCode = tDealRiskAmntBL.getCalCode(
                                tLRPolSchema, "CessPremMode");
                        if (CessPremModeCode == null ||
                            CessPremModeCode.equals("")) {
                            this.errorLog(tLRPolSchema, "获取对应再保分保费率表失败");
                            continue;
                        }
                        this.mCessPremRate = tDealRiskAmntBL.calCessFeeRate(
                                CessPremModeCode, tLRPolSchema);
                        if (CessPremModeCode == null ||
                            Math.abs(mCessPremRate + 1) < 0.00001) {
                            this.errorLog(tLRPolSchema, "计算再保分保费率失败");
                            continue;
                        }

                        //溢额分保从lrpol表中取本保单本合同的分保保额
                        this.mCessionAmount = tLRPolSchema.getCessAmnt();

                        LRPolResultSchema tLRPolResultSchema = new
                                LRPolResultSchema();
                        prepareLRPolResult(tLRPolSchema, tLRPolResultSchema);
                        this.mMap.put(tLRPolResultSchema, "DELETE&INSERT");
                    } else { //按责任分保
                        dealDutyData(tLRPolSchema,mMap); //处理按责任分保信息，向lrpoldutyresult表中插入数据
                    }
                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }

        return true;
    }

    /**
     * dealDutyData
     * 按责任分保时在这里计算每一单的分保保额，分保保费，并存储在lrpoldutyresult表中
     * @param tLRPolSchema LRPolSchema
     */
    private boolean dealDutyData(LRPolSchema tLRPolSchema,MMap aMap) {
//        LRPolDutyResultSet tLRPolDutyResultSet = new LRPolDutyResultSet();
        //查询该合同该保单需要分保的责任.lrpol的renewcount字段短险存续保次数，长险存保单年度。按保额高低排序，这样如果最低保额<该责任的保额，则第一个责任会全部分保。
        String sqla = "select * from lrduty a where  exists (select 1 from lrcalfactorvalue b " +
                      "where b.riskcode=a.getdutycode and b.recontcode='" +
                      tLRPolSchema.getReContCode() + "') " +
                      "and polno='" + tLRPolSchema.getPolNo() +
                      "' and PolYear=" + tLRPolSchema.getReNewCount() +
                      " order by a.amnt with ur";
        System.out.println(sqla);
        LRDutySet tLRDutySet = new LRDutyDB().executeQuery(sqla);
        if (tLRDutySet.size() > 1) { //查询结果>1则报错。
            buildError("dealDutyData",
                       "该合同下分保多个责任！请核对分保合同" + tLRPolSchema.getReContCode());
        }else if(tLRDutySet.size() < 1){
            buildError("dealDutyData",
                       "该责任数据查出有误" + tLRPolSchema.getReContCode());
            return false;
        }
        LRPolDutyResultSchema tLRPolDutyResultSchema = new
                LRPolDutyResultSchema();
        LRDutySchema tLRDutySchema = tLRDutySet.get(1);
        this.mAccQuot = getAccQuot(tLRPolSchema, tLRDutySchema); //接受份额
        this.mChoiRebaFeeRate = getChoiRebaFeeRate(tLRPolSchema,
                tLRDutySchema); //选择折扣比例
        this.mManageFeeRate = getManageFeeRate(tLRPolSchema, tLRDutySchema); //管理费比例

//        double tLowReAmnt = getLowReAmnt(tLRPolSchema, tLRDutySchema); //获取最低分保保额
//        double tAutoLimit = getAutoLimit(tLRPolSchema, tLRDutySchema); //最高分保保额
//        double Amnt = tLRDutySet.get(1).getAmnt(); //分保责任的保额, 一个合同只分保一个责任。所以只有一个查询结果
//        double tCessionAmnt = Math.min(tAutoLimit, Amnt) - tLowReAmnt -
//                              Amnt; //总分保保额=min()-最低再保保额－本单已分保保额
        mCessionAmount = tLRPolSchema.getCessAmnt(); //分保保额
        //开始获取计算分保费率
       String CessPremModeCode = tDealRiskAmntBL.getCalCode(
                                tLRPolSchema, "CessPremMode");
       if (CessPremModeCode == null ||
            CessPremModeCode.equals("")) {
            this.errorLog(tLRPolSchema, "获取对应的再保分保费率表失败");
       }
       this.mCessPremRate = tDealRiskAmntBL.calCessFeeRate(
                                CessPremModeCode, tLRPolSchema);
       if (CessPremModeCode == null ||
            Math.abs(mCessPremRate + 1) < 0.00001) {
            this.errorLog(tLRPolSchema, "计算再保分保费率失败");
       }

        prepareLRDutyResult(tLRPolSchema, tLRPolDutyResultSchema, tLRDutySchema); //准备数据

        //向lrpol表插入数据。基本信息同责任表信息。
//        LRPolDutyResultSchema ttLRPolDutyResultSchema = tLRPolDutyResultSet.get(
//                1);
        LRPolResultSchema ttLRPolResultSchema = new LRPolResultSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(ttLRPolResultSchema, tLRPolDutyResultSchema); //将责任表信处置入险种责任计算结果表
        ttLRPolResultSchema.setCessionAmount(tLRPolDutyResultSchema.
                                             getCessionAmount()); //责任分保保额
        ttLRPolResultSchema.setCessionRate((tLRPolDutyResultSchema.
                                            getCessionAmount()) /
                                           tLRPolSchema.getAmnt()); //分保比例，摊回时计算要用到
        ttLRPolResultSchema.setCessPrem(tLRPolDutyResultSchema.getCessPrem()); //所有责任分保保费合计
        ttLRPolResultSchema.setReProcFee(tLRPolDutyResultSchema.getReProcFee()); //所有责任分保手续费合计
        ttLRPolResultSchema.setChoiRebaFee(tLRPolDutyResultSchema.
                                           getChoiRebaFee()); //所有责任选择折扣费
        ttLRPolResultSchema.setReinsureItem(tLRPolSchema.getReinsureItem());
        aMap.put(tLRPolDutyResultSchema, "DELETE&INSERT");
        aMap.put(ttLRPolResultSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 临时分保的计算
     * @return boolean
     */
    private boolean calCessData3() {
    	System.out.println("jinlai2");
        String strSQL =
                "select * from LRPol a where  a.GetDataDate between '" +
                mStartDate + "' and '" + mEndDate +
                "' and a.TempCessFlag = 'Y' "
                + " and not exists(select 1 from lrpolresult where  a.polno = polno and a.recontcode = recontcode and a.reinsureitem = reinsureitem and a.renewcount = renewcount and a.getdatadate = cessstartdate  and a.payno = payno)  ";
        strSQL += mSQL;
        strSQL+=" with ur";
        LRPolSet tLRPolSet = new LRPolSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolSet, strSQL);

        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolSet.size(); i++) {
                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                    LRTempCessContInfoDB tLRTempCessContInfoDB = new
                            LRTempCessContInfoDB();
                    tLRTempCessContInfoDB.setRiskCode(tLRPolSchema.getRiskCode());
                    String continfo = "select * from LRTempCessContInfo where Tempcontcode='"+tLRPolSchema.getReContCode()+"' and riskcode='"+tLRPolSchema.getRiskCode()+"' with ur";
                    LRTempCessContInfoSet tLRTempCessContInfoSet =  tLRTempCessContInfoDB.executeQuery(continfo);
                    if (tLRTempCessContInfoSet.size()<=0) {
                        this.errorLog(tLRPolSchema, "没有找到该保单的临时分保协议");
                        continue;
                    }
                    LRTempCessContInfoSchema tLRTempCessContInfoSchema =
                            tLRTempCessContInfoSet.get(1);

                    LRTempCessContDB tLRTempCessContDB = new
                            LRTempCessContDB();
                    tLRTempCessContDB.setTempContCode(tLRPolSchema.
                            getReContCode());
                    tLRTempCessContDB.setContPlanCode("1");
                    if (!tLRTempCessContDB.getInfo()) {
                        this.errorLog(tLRPolSchema, "没有找到该保单的临时分保协议");
                        continue;
                    }
                    LRTempCessContSchema tLRTempCessContSchema =
                            tLRTempCessContDB.getSchema();

                    LRPolResultSchema tLRPolResultSchema = new
                            LRPolResultSchema();
                    //险种溢额分保
                    if (tLRTempCessContSchema.getCessionMode().equals("2") &&
                        tLRTempCessContSchema.getReType().equals("01")) {
                        this.mChoiRebaFeeRate = tLRTempCessContInfoSchema.getChoiRebaRate(); //选择折扣比例
                        this.mAccQuot = tLRTempCessContInfoSchema.getAccQuot(); //接受份额
                        this.mReProcFeeRate = tLRTempCessContInfoSchema.
                                              getReinProcFeeRate(); //再保手续费比例
                        this.mManageFeeRate = tLRTempCessContInfoSchema.getReinManFeeRate(); //管理费比例
                        //开始获取计算分保费率
                        String CessPremModeCode = tDealRiskAmntBL.getCalCode(
                                tLRPolSchema, "CessPremMode");
                        if (CessPremModeCode == null ||
                            CessPremModeCode.equals("")) {
                            this.errorLog(tLRPolSchema, "获取对应的再保分保费率表失败");
                            continue;
                        }
                        this.mCessPremRate =
//                        		tLRTempCessContInfoSchema.getReserve1(); 
                        	tDealRiskAmntBL.calCessFeeRate(
                                CessPremModeCode, tLRPolSchema);
                        if (CessPremModeCode == null ||
                            Math.abs(mCessPremRate + 1) < 0.00001) {
                            this.errorLog(tLRPolSchema, "计算再保分保费率失败");
                            continue;
                        }

                        //溢额分保从lrpol表中取本保单本合同的分保保额
                        this.mCessionAmount = tLRPolSchema.getCessAmnt();
                        prepareLRPolResult(tLRPolSchema, tLRPolResultSchema);
                        this.mMap.put(tLRPolResultSchema, "DELETE&INSERT");
                    } else { //临分成数分保
                        this.mCessionRate = tLRPolSchema.getDeadAddFeeRate(); //成数分保比例
                        this.mAccQuot = tLRTempCessContInfoSchema.getAccQuot(); //接受份额
                        this.mReProcFeeRate = tLRTempCessContInfoSchema.
                                              getReinProcFeeRate(); //再保手续费比例
                        this.mSpeCemmRate = tLRTempCessContInfoSchema.getSpeComRate(); //特殊佣金比例
                        this.mManageFeeRate =tLRTempCessContInfoSchema.getReinManFeeRate(); //管理费比例
                       
                        LRPolResultSchema ttLRPolResultSchema = new
                        LRPolResultSchema();
                            this.preLRPolResultC(tLRPolSchema,
                            		        ttLRPolResultSchema); //调用成数分保的准备数据
                            this.mMap.put(ttLRPolResultSchema.getSchema(),
                                          "DELETE&INSERT");
                      
                    }
                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
        return true;
    }
    
    /**
     * 风险溢额分保的计算
     * @return boolean
     */
    private boolean calCessData4() {
    	String strSQL =
            "select * from LRPol a where (select CessionMode from LRContInfo where RecontCode = a.RecontCode) = '2' " +
            "and a.GetDataDate between '" +
            mStartDate + "' and '" + mEndDate +
            "' and a.TempCessFlag = 'N' " 
            + " and not exists(select 1 from lrpolresult where  a.polno = polno and a.recontcode = recontcode and a.reinsureitem = reinsureitem and a.renewcount = renewcount and a.getdatadate = cessstartdate  and a.payno = payno)  ";
    	  strSQL += mSQL;
          strSQL+=" with ur ";
          LRPolSet tLRPolSet = new LRPolSet();
          LRPolResultSet tLRPolResultSet=new LRPolResultSet();
          RSWrapper rswrapper = new RSWrapper();
          rswrapper.prepareData(tLRPolSet, strSQL);
    	try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolSet.size(); i++) {
                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                    double tCessionRate=tLRPolSchema.getDeadAddFeeRate();
                    double tCessPrem=0.00;
                    double tChoiRebaFee=0.00;
                    double tChoiRebaFeeR=0;
                    this.mCessAmnt=tLRPolSchema.getCessAmnt();
                    String CessPremModeCode = tDealRiskAmntBL.getCalCode(
                            tLRPolSchema, "CessPremMode");
                    if (CessPremModeCode == null ||
                    		CessPremModeCode.equals("")) {
                    	this.errorLog(tLRPolSchema, "获取对应的再保分保费率表失败");
                    }
                    this.mCessPremRate = tDealRiskAmntBL.calCessFeeRate(
                            	CessPremModeCode, tLRPolSchema);
//                    this.mCessPremRate=1;
                    
                    if (CessPremModeCode == null ||
                    		Math.abs(mCessPremRate + 1) < 0.00001) {
                    	this.errorLog(tLRPolSchema, "计算再保分保费率失败");
                    }
                    if(mCessPremRate<=0)
                    	mCessPremRate=0;
                    //查询该合同是按责任还是险种分保
                    strSQL =
                            "select ReType from Lrcontinfo where recontcode = '" +
                            tLRPolSchema.getReContCode() + "'";
                    String ReType = new ExeSQL().getOneValue(strSQL);

                    if (ReType.equals("01")) {
                    	this.mManageFeeRate=getManageFeeRate(tLRPolSchema);
                    	String tCalType ="";
//                    	int tCalType4=0;
//                    	String tsql4=CalSQL(tLRPolSchema.getRiskCode(),"4",tLRPolSchema.getReContCode());
//                    	String tCalType = new ExeSQL().getOneValue(tsql4);
//                    	if(tCalType!=null&&!"".equals(tCalType)){
//                    		tCalType4=Integer.parseInt(tCalType);
//                    		switch(tCalType4){
//                    			case 1:tCessionRate=getCessionRate(tLRPolSchema.getPolNo(),"0000",tLRPolSchema.getReNewCount());
//                    			break;
//                    		}
//                    		if(tCessionRate<0) continue;
//                    	}
                    	int tCalType5=0;
                    	String tsql5=CalSQL(tLRPolSchema.getRiskCode(),"5",tLRPolSchema.getReContCode());
                    	tCalType = new ExeSQL().getOneValue(tsql5);
                    	if(tCalType!=null&&!"".equals(tCalType)){
                    		tCalType5=Integer.parseInt(tCalType);
                    		switch(tCalType5){
                    			case 1:
                    				tCessPrem=getCessPremA(tLRPolSchema.getPolNo(),tLRPolSchema.getReNewCount(),
                    						tLRPolSchema.getReContCode(),tLRPolSchema.getRiskCode(),mCessPremRate);
                    				break;
                    			case 2:
                    				tCessPrem=getCessPremB(tLRPolSchema.getPolNo(),tLRPolSchema.getReNewCount(),
                    						tLRPolSchema.getReContCode(),tLRPolSchema.getRiskCode(),mCessPremRate,tCessionRate);
                    				break;
                    		}
                    	}
                    	int tCalType6=0;
                    	String tsql6=CalSQL(tLRPolSchema.getRiskCode(),"6",tLRPolSchema.getReContCode());
                    	tCalType = new ExeSQL().getOneValue(tsql6);
                    	if(tCalType!=null&&!"".equals(tCalType)){
                    		tCalType6=Integer.parseInt(tCalType);
                    		String sql1="";
                        	sql1="select FactorValue from lrcalfactorvalue where " +
                        	"recontcode='"+tLRPolSchema.getReContCode()+"' " +
                        	"and FactorCode='ChoiRebaRate' and riskcode='"+tLRPolSchema.getRiskCode()+"' with ur";
                        	SSRS tCR =new SSRS();
                        	tCR= new ExeSQL().execSQL(sql1);
                        	//System.out.println("****"+tCR.GetText(1, 1)+"&&&&&&");
                        	if(tCR.GetText(1, 1)!=null&&tCR.GetText(1, 1)!="")
                        	tChoiRebaFeeR=Double.parseDouble(tCR.GetText(1, 1));
                        	else tChoiRebaFeeR=0;
                    		switch(tCalType6){
                    			case 1:tChoiRebaFee=
                    				getChoiRebaFeeA(tCessPrem,tChoiRebaFeeR);
                    			break;
                    		}
                    	}
                    		
                    } else { //按责任分保
                        String tsql="select distinct riskcode from lrcalfactorvalue where" +
                        		" recontcode='"+tLRPolSchema.getReContCode()+"' with ur";
                        SSRS tRD = new ExeSQL().execSQL(tsql); 
                        String tGetDutyCode=tRD.GetText(1, 1);
                        tCessionRate=tLRPolSchema.getDeadAddFeeRate();
                        this.mManageFeeRate=getManageFeeRate(tLRPolSchema.getReContCode(),tGetDutyCode);
//                        int tCalType4=0;
//                    	String tsql4=CalSQL(tGetDutyCode,"4",tLRPolSchema.getReContCode());
//                    	String tCalType = new ExeSQL().getOneValue(tsql4);
//                    	if(tCalType!=null&&!"".equals(tCalType)){
//                    		tCalType4=Integer.parseInt(tCalType);
//                    		switch(tCalType4){
//                    			case 1:tCessionRate=getCessionRate(tLRPolSchema.getPolNo(),tGetDutyCode,tLRPolSchema.getReNewCount());
//                    			break;
//                    		}
//                    	}
                    	int tCalType5=0;
                    	String tsql5=CalSQL(tGetDutyCode,"5",tLRPolSchema.getReContCode());
                    	String tCalType = new ExeSQL().getOneValue(tsql5);
                    	if(tCalType!=null&&!"".equals(tCalType)){
                    		tCalType5=Integer.parseInt(tCalType);
                    		switch(tCalType5){
                    			case 1:
                    				tCessPrem=getCessPremA(tLRPolSchema.getPolNo(),tLRPolSchema.getReNewCount(),
                    						tLRPolSchema.getReContCode(),tGetDutyCode,mCessPremRate);
                    				break;
                    			case 2:
                    				tCessPrem=getCessPremB(tLRPolSchema.getPolNo(),tLRPolSchema.getReNewCount(),
                    						tLRPolSchema.getReContCode(),tGetDutyCode,mCessPremRate,tCessionRate);
                    				break;
                    		}
                    	}
                    	int tCalType6=0;
                    	String tsql6=CalSQL(tGetDutyCode,"6",tLRPolSchema.getReContCode());
                    	tCalType = new ExeSQL().getOneValue(tsql6);
                    	if(tCalType!=null&&!"".equals(tCalType)){
                    		tCalType6=Integer.parseInt(tCalType);
                    		String sql1="";
                        	sql1="select FactorValue from lrcalfactorvalue where " +
                        	"recontcode='"+tLRPolSchema.getReContCode()+"' " +
                        	"and FactorCode='ChoiRebaRate' and riskcode='"+tGetDutyCode+"' with ur";
                        	SSRS tCR =new SSRS();
                        	tCR= new ExeSQL().execSQL(sql1);
                        	//System.out.println("****"+tCR.GetText(1, 1)+"&&&&&&");
                        	if(tCR.GetText(1, 1)!=null&&tCR.GetText(1, 1)!="")
                        	tChoiRebaFeeR=Double.parseDouble(tCR.GetText(1, 1));
                        	else 
                        		tChoiRebaFeeR=0;
                    		switch(tCalType6){
                    		
                    			case 1:tChoiRebaFee=
                    				getChoiRebaFeeA(tCessPrem,tChoiRebaFeeR);
                    			break;
                    		}
                    	}
                    }
//                    strSQL = "select * from LRPolDetail where PolNo = '" +
//                    tLRPolSchema.getPolNo() + "' and RenewCount =" +
//                    tLRPolSchema.getReNewCount() +
//                    " and GetDataDate <= '" + mEndDate +
//                    "' order by paymoney with ur";
//           LRPolDetailSet tLRPolDetailSet = new LRPolDetailDB().
//                   executeQuery(strSQL);
//           for (int j = 1; j <= tLRPolDetailSet.size(); j++) {
//               LRPolDetailSchema tLRPolDetailSchema = tLRPolDetailSet.
//                       get(j);
                    String tdays = "select days('"+tLRPolSchema.getEndDate()+"')-days('"+tLRPolSchema.getCValiDate()+"') from dual where 1=1 with ur";
                    SSRS tday =new SSRS();
                    tday= new ExeSQL().execSQL(tdays);
                    double tdaydur=Double.parseDouble(tday.GetText(1, 1));
                    double tdrate=tdaydur/365;
                    if(tdaydur<365){
                    	tCessPrem=tCessPrem*tdrate;
                    }
	               LRPolResultSchema tLRPolResultSchema = new
	                       LRPolResultSchema();
	               LRPolResultSchema aLRPolResultSchema=new LRPolResultSchema();
	               aLRPolResultSchema.setPolNo(tLRPolSchema.getPolNo());
	               aLRPolResultSchema.setReContCode(tLRPolSchema.getReContCode());
	               aLRPolResultSchema.setReinsureItem(tLRPolSchema.getReinsureItem());
	               aLRPolResultSchema.setTempCessFlag(tLRPolSchema.getTempCessFlag());
	               aLRPolResultSchema.setReNewCount(tLRPolSchema.getReNewCount());
	               aLRPolResultSchema.setPayCount(0); 
	               aLRPolResultSchema.setRiskCalSort(tLRPolSchema.getRiskCalSort());
	               aLRPolResultSchema.setRiskCode(tLRPolSchema.getRiskCode());
	               aLRPolResultSchema.setRiskVersion(tLRPolSchema.getRiskVersion());
	               aLRPolResultSchema.setCessStartDate(tLRPolSchema.getGetDataDate());
	               aLRPolResultSchema.setCessEndDate(this.mEndDate);
	               aLRPolResultSchema.setCessionRate(tCessionRate); //分保比例
	               aLRPolResultSchema.setReinsureItem(tLRPolSchema.getReinsureItem());
	               aLRPolResultSchema.setCessionAmount(this.mCessAmnt); //分保保额
	               aLRPolResultSchema.setCessPremRate(this.mCessPremRate); //分保费率
	               
	               aLRPolResultSchema.setCessPrem(tCessPrem); //分保保费
	
	               String validate = tLRPolSchema.getCValiDate();
	               int currYear = PubFun.calInterval(validate,
	               		tLRPolSchema.getGetDataDate(),
	                                                 "Y") + 1; //计算保单年度,第一年的保单年度为1
	               if (currYear > 1&&!"MUR2016A01".equals(aLRPolResultSchema.getReContCode())
	            		 &&!"MUR2016A03".equals(aLRPolResultSchema.getReContCode())) {
	               	tChoiRebaFeeR = 0; //如果非首次保单年度,则没有折扣
	               }
	               aLRPolResultSchema.setChoiRebaFeeRate(tChoiRebaFeeR); //选择折扣比例
	               aLRPolResultSchema.setReProcFee(tCessPrem*tChoiRebaFeeR); //再保手续费=分保保费*选择折扣比例
	               aLRPolResultSchema.setManageFeeRate(this.mManageFeeRate); //再保管理费比例
	               aLRPolResultSchema.setManageFee(tCessPrem * this.mManageFeeRate); //再保管理费
	               aLRPolResultSchema.setPayNo(tLRPolSchema.getPayNo());
	               aLRPolResultSchema.setManageCom(tLRPolSchema.getManageCom());
	               aLRPolResultSchema.setOperator(strOperate);
	               aLRPolResultSchema.setMakeDate(mPubFun.getCurrentDate());
	               aLRPolResultSchema.setMakeTime(mPubFun.getCurrentTime());
	               aLRPolResultSchema.setModifyDate(mPubFun.getCurrentDate());
	               aLRPolResultSchema.setModifyTime(mPubFun.getCurrentTime());
	               
	               //tLRPolResultSet.add(aLRPolResultSchema);
	           
	           this.mMap.put(aLRPolResultSchema.getSchema(),"DELETE&INSERT");
	//           }
	                    
	           
	                }
	                if (!prepareOutputData()) {
	                    return false;
	                }
	                tPubSubmit = new PubSubmit();
	                if (!tPubSubmit.submitData(mInputData, "")) {
	                    if (tPubSubmit.mErrors.needDealError()) {
	                        buildError("submitData", "保存再保计算结果出错!");
	                    }
	                    return false;
	                }
	            } while (tLRPolSet.size() > 0);
	            rswrapper.close();
	            
	           
	        } catch (Exception ex) {
	            ex.printStackTrace();
	            rswrapper.close();
	        }
	    	return true;
    }
/**
 * 获取计算类型*/
    private String CalSQL(String tRiskCode,String tCalState,String tRecontCode){
    	String tcalsql="select CalType from lrpolrelation where RiskCode='"+tRiskCode+"' " +
    					"and CalState='"+tCalState+"' and recontcode='"+tRecontCode+"' with ur";
    	SSRS tCal = new ExeSQL().execSQL(tcalsql);
    	if(tCal.getMaxRow() <= 0||tCal==null){
    		tcalsql="select CalType from lrpolrelation where RiskCode='"+tRiskCode+"' " +
			"and CalState='"+tCalState+"' with ur";
    		
    	}
    	return tcalsql;
    }
    /**
     * 计算再保比例*/    
    private double getCessionRate(String tPolNo,String tGetDutyCode,int tReNewCount){
    	double tCessionRate=0.00;
    	double tRemainAmnt=0.00;
    	double tRiskAmnt=0.00;
    	String sql1="";
    	String sql2="";
    	sql1="select RemainAmnt from LRPolCessAmnt where PolNo='"+tPolNo+"' " +
    			"  and GetDutyCode='"+tGetDutyCode+"' and renewcount=0 and paycount=1 ";
    	SSRS tRA = new ExeSQL().execSQL(sql1); 
     	String tRiskAmntsql="select sumriskamnt from lrpolcessamnt where " +
		"polno='"+tPolNo+"' and renewcount=0 and paycount=1 and getdutycode='"+tGetDutyCode+"' with ur";
    	SSRS ttSSRS = new ExeSQL().execSQL(tRiskAmntsql);
    	if(ttSSRS.MaxRow>0&&!ttSSRS.GetText(1, 1).equals("") && ttSSRS.GetText(1, 1) != null)
    		tRiskAmnt=Double.parseDouble(ttSSRS.GetText(1, 1));
    	  	//SSRS tAM = new ExeSQL().execSQL(sql2);
    	if(tRA.MaxRow>0&&!tRA.GetText(1, 1).equals("") && tRA.GetText(1, 1) != null)
     		tRemainAmnt=Double.parseDouble(tRA.GetText(1, 1));
    	//tAmnt=Double.parseDouble(tAM.GetText(1, 1));
    	if(tRiskAmnt>0){
    		tCessionRate=1-(tRemainAmnt/tRiskAmnt);
    		this.mCessAmnt=this.mCessAmnt*tCessionRate;
    	}
    	return tCessionRate;
    }
    
    private double getCessPremA(String tPolNo,int tReNewCount,String tReContCode,String tRiskCode,double tCessPremRate){
    	double tCessPrem=0.00;
    	double tCessAmnt=0.00;
    	double tAccQuot=1;
    	String sql1="";
    	String sql2="";
//    	sql1="select CessAmnt from lrpol where PolNo='"+tPolNo+"' and RecontCode='"+tReContCode+"'" +
//    			" and renewcount="+tReNewCount+" with ur";
//    	SSRS tCA = new ExeSQL().execSQL(sql1); 
    	sql2="select FactorValue from lrcalfactorvalue where " +
    			"recontcode='"+tReContCode+"' and FactorCode='AccQuot' and riskcode='"+tRiskCode+"' with ur";
    	SSRS tAQ = new ExeSQL().execSQL(sql2);
//    	tCessAmnt=Double.parseDouble(tCA.GetText(1, 1));
    	tCessAmnt=this.mCessAmnt;
    	if(tAQ.GetText(1, 1)!=null&&!"".equals(tAQ.GetText(1, 1)))
    	tAccQuot=Double.parseDouble(tAQ.GetText(1, 1));
    	tCessPrem=tCessAmnt*tCessPremRate*tAccQuot;
    	tCessPrem=tCessPrem/1000;
    	return tCessPrem;
    	
    }
    
    private double getCessPremB(String tPolNo,int tReNewCount,String tReContCode,
    		String tRiskCode,double tCessPremRate,double tCessionRate){
    	double tCessPrem=0.00;
    	double tCessAmnt=0.00;
    	double tAccQuot=1;
    	double tprem=0.00;
    	double tsprem=0.00;
    	String sql1="";
    	String sql2="";
    	String sql3="";
//    	sql1="select CessAmnt from lrpol where PolNo='"+tPolNo+"' and RecontCode='"+tReContCode+"'" +
//    			" and renewcount="+tReNewCount+" with ur";
//    	SSRS tCA = new ExeSQL().execSQL(sql1); 
    	sql2="select FactorValue from lrcalfactorvalue where " +
    			"recontcode='"+tReContCode+"' and FactorCode='AccQuot' and riskcode='"+tRiskCode+"' with ur";
    	SSRS tAQ = new ExeSQL().execSQL(sql2);
    	sql3="select prem,standprem from lcpol where polno='"+tPolNo+"' union " +
    			"select prem,standprem from lbpol where polno='"+tPolNo+"' with ur";
    	SSRS tPrem = new ExeSQL().execSQL(sql3);
//    	tCessAmnt=Double.parseDouble(tCA.GetText(1, 1));
    	tCessAmnt=this.mCessAmnt;
    	if(tAQ.GetText(1, 1)!=null&&!"".equals(tAQ.GetText(1, 1)))
    	tAccQuot=Double.parseDouble(tAQ.GetText(1, 1));
    	tprem=Double.parseDouble(tPrem.GetText(1, 1));
    	tsprem=Double.parseDouble(tPrem.GetText(1, 2));
    	tCessPrem=(tCessAmnt*tCessPremRate*tAccQuot)/1000+(tprem-tsprem)*tCessionRate*tAccQuot;
    	if(tCessPrem<0)
    		tCessPrem=0;
    	return tCessPrem;
    	
    }
    
    private double getChoiRebaFeeA(double tCessPrem,double tChoiRebaFeeR){
    	double tChoiRebaFee=0.00;
    	
    	tChoiRebaFee=tCessPrem*0.5;
    	return tChoiRebaFee;
    	
    }
    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolSchema aLRPolSchema, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolSchema.getPolNo());
        tLRErrorLogSchema.setLogType("5"); //5:新单计算
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "DELETE&INSERT");

        return true;
    }

    /**
     *成数分保准备数据
     * @param aLRPolSchema LRPolSchema
     * @param aLRPolDetailSchema LRPolDetailSchema
     * @param aLRPolResultSchema LRPolResultSchema
     * @return boolean
     */
    private boolean preLRPolResultC(LRPolSchema aLRPolSchema,
    								              LRPolResultSchema aLRPolResultSchema) {
//    	String sql="select cessionfeemode from lrcontinfo where recontcode='"+aLRPolSchema.getReContCode()+"'";
//    	SSRS tSSRS = new ExeSQL().execSQL(sql);
//    	String tCessionfeemode="";
//    	if(tSSRS.GetText(1, 1)!=null&&!"".equals(tSSRS.GetText(1, 1)))
//    		tCessionfeemode=tSSRS.GetText(1, 1);
        aLRPolResultSchema.setPolNo(aLRPolSchema.getPolNo());
        aLRPolResultSchema.setReContCode(aLRPolSchema.getReContCode());
        aLRPolResultSchema.setReinsureItem(aLRPolSchema.getReinsureItem());
        aLRPolResultSchema.setTempCessFlag(aLRPolSchema.getTempCessFlag());
        aLRPolResultSchema.setReNewCount(aLRPolSchema.getReNewCount());
        aLRPolResultSchema.setPayCount(aLRPolSchema.getReNewCount());
        aLRPolResultSchema.setRiskCalSort(aLRPolSchema.getRiskCalSort());
        aLRPolResultSchema.setRiskCode(aLRPolSchema.getRiskCode());
        aLRPolResultSchema.setRiskVersion(aLRPolSchema.getRiskVersion());
        aLRPolResultSchema.setCessStartDate(aLRPolSchema.getGetDataDate());
        aLRPolResultSchema.setCessEndDate(this.mEndDate);

        aLRPolResultSchema.setCurrentPrem(aLRPolSchema.getPrem()); //当期保费
        int tPayIntv = aLRPolSchema.getPayIntv();
        if ((tPayIntv == 1 || tPayIntv == 3 || tPayIntv == 6)&&aLRPolSchema.getReNewCount()==0
        		&&"7".equals(aLRPolSchema.getPolStat())&&!"124302".equals(aLRPolSchema.getRiskCode())) {
            int tRemainMonths = PubFun.calInterval(aLRPolSchema.getCValiDate()
                   , aLRPolSchema.getEndDate(), "M") /
                                tPayIntv;
            aLRPolResultSchema.setShouldCessPrem(aLRPolSchema.getPrem() *
                                                 tRemainMonths);
            aLRPolResultSchema.setShouldCount(tRemainMonths );
            aLRPolResultSchema.setRemainCount(tRemainMonths-1);
            //海外就医产品保费折标处理  2017/12/5 wang
            if("123401".equals(aLRPolResultSchema.getRiskCode())||"123301".equals(aLRPolResultSchema.getRiskCode())){
            	System.out.println("开始处理海外就医1");
            	double tStandPrem = getStandPrem(aLRPolSchema)*tRemainMonths;
            	aLRPolResultSchema.setShouldCessPrem(tStandPrem);
            	System.out.println("海外就医处理结束1");
            }
            if("162901".equals(aLRPolResultSchema.getRiskCode())||"162701".equals(aLRPolResultSchema.getRiskCode())){
            	System.out.println("开始处理海外就医2");
            	double tStandPrem = getGrpStandPrem(aLRPolSchema)*tRemainMonths;
            	aLRPolResultSchema.setShouldCessPrem(tStandPrem);
            	System.out.println("开始处理海外就医2");
            }
            if("162601".equals(aLRPolResultSchema.getRiskCode())||"162801".equals(aLRPolResultSchema.getRiskCode())){
            	System.out.println("开始处理海外就医3");
            	double tStandPrem = getFamStandPrem(aLRPolSchema)*tRemainMonths;
//            	System.out.println("标准保费为");
            	aLRPolResultSchema.setShouldCessPrem(tStandPrem);
            	System.out.println("海外就医处理结束3");
            }

        } else {
            aLRPolResultSchema.setShouldCessPrem(aLRPolSchema.getPrem());
            aLRPolResultSchema.setShouldCount(1);
            aLRPolResultSchema.setRemainCount(0);
            //海外就医产品保费折标处理  2017/12/5 wang
            //海外就医产品保费折标处理  2017/12/5 wang
            if("123401".equals(aLRPolResultSchema.getRiskCode())||"123301".equals(aLRPolResultSchema.getRiskCode())){
            	System.out.println("开始处理海外就医1");
            	double tStandPrem = getStandPrem(aLRPolSchema);
            	aLRPolResultSchema.setShouldCessPrem(tStandPrem);
            	System.out.println("海外就医处理结束1");
            }
            if("162901".equals(aLRPolResultSchema.getRiskCode())||"162701".equals(aLRPolResultSchema.getRiskCode())){
            	System.out.println("开始处理海外就医2");
            	double tStandPrem = getGrpStandPrem(aLRPolSchema);
            	aLRPolResultSchema.setShouldCessPrem(tStandPrem);
            	System.out.println("海外就医处理结束2");
            }
            if("162601".equals(aLRPolResultSchema.getRiskCode())||"162801".equals(aLRPolResultSchema.getRiskCode())){;
                System.out.println("开始处理海外就医3");	
                double tStandPrem = getFamStandPrem(aLRPolSchema);
            	aLRPolResultSchema.setShouldCessPrem(tStandPrem);
            	System.out.println("海外就医处理结束3");
            }

        }
        aLRPolResultSchema.setCessionRate(this.mCessionRate); //分保比例
        double cessPrem = aLRPolResultSchema.getShouldCessPrem() *
                          mCessionRate * mAccQuot;
//        if(aLRPolSchema.getReinsureItem().equals("F")){//如果是反冲数据，则置分保保费为负数
//            cessPrem=cessPrem;
//        }

        	aLRPolResultSchema.setCessPrem(cessPrem); //分保保费

        aLRPolResultSchema.setSpeCemmRate(this.mSpeCemmRate); //特殊佣金比例
        aLRPolResultSchema.setReProcFeeRate(this.mReProcFeeRate); //再保手续费比例
        aLRPolResultSchema.setReProcFee(cessPrem *
                                        (mSpeCemmRate + mReProcFeeRate)); //再保手续费
        aLRPolResultSchema.setManageFeeRate(this.mManageFeeRate); //再保管理费比例
        aLRPolResultSchema.setManageFee(cessPrem * this.mManageFeeRate); //再保管理费
        aLRPolResultSchema.setPayNo(aLRPolSchema.getPayNo());
        aLRPolResultSchema.setManageCom(aLRPolSchema.getManageCom());
        aLRPolResultSchema.setOperator(strOperate);
        aLRPolResultSchema.setMakeDate(mPubFun.getCurrentDate());
        aLRPolResultSchema.setMakeTime(mPubFun.getCurrentTime());
        aLRPolResultSchema.setModifyDate(mPubFun.getCurrentDate());
        aLRPolResultSchema.setModifyTime(mPubFun.getCurrentTime());
        return true;
    }

    /**
     * 责任分保时，准备数据责任分保结果表
     * @return boolean
     */
    private boolean prepareLRDutyResult(LRPolSchema tLRPolSchema,
                                        LRPolDutyResultSchema
                                        tLRPolDutyResultSchema,
                                        LRDutySchema tLRDutySchema) {
        tLRPolDutyResultSchema.setPolNo(tLRPolSchema.getPolNo());
        tLRPolDutyResultSchema.setReContCode(tLRPolSchema.getReContCode());
        tLRPolDutyResultSchema.setGetDutyCode(tLRDutySchema.getGetDutyCode());
        tLRPolDutyResultSchema.setTempCessFlag(tLRPolSchema.getTempCessFlag());
        tLRPolDutyResultSchema.setReNewCount(tLRPolSchema.getReNewCount());
        tLRPolDutyResultSchema.setPayCount(0); //溢额分保不考虑期交保费,此字段默认为0
        tLRPolDutyResultSchema.setRiskCalSort(tLRPolSchema.getRiskCalSort());
        tLRPolDutyResultSchema.setRiskCode(tLRPolSchema.getRiskCode());
        tLRPolDutyResultSchema.setRiskVersion(tLRPolSchema.getRiskVersion());
        tLRPolDutyResultSchema.setCessStartDate(tLRPolSchema.getGetDataDate());
        tLRPolDutyResultSchema.setCessEndDate(this.mEndDate);
        tLRPolDutyResultSchema.setCessionRate(this.mCessionAmount /
                                              tLRDutySchema.getAmnt()); //分保比例=分保保额/责任保额
        if(tLRPolSchema.getReinsureItem().equals("F")){//如果为反冲数据，则置分保保额为负数
            this.mCessionAmount=-this.mCessionAmount;
        }
        tLRPolDutyResultSchema.setCessionAmount(this.mCessionAmount); //分保保额
        tLRPolDutyResultSchema.setCessPremRate(this.mCessPremRate); //分保费率==========-------------=========没计算
        String strSQL =
                "select RiskPeriod from LMRiskApp where riskcode = '" +
                tLRPolDutyResultSchema.getRiskCode() + "'";
        String tRiskPeriod = new ExeSQL().getOneValue(strSQL);
        double cessPrem = 0;
        //分保费率问题mCessPremRate没有计算
        if (tRiskPeriod.equals("L")) {
            cessPrem = mCessionAmount / 1000 * this.mCessPremRate * mAccQuot;
        } else {
            int days = mPubFun.calInterval(tLRPolSchema.getCValiDate(),
                                           tLRPolSchema.getEndDate(), "D");
            cessPrem = mCessionAmount / 1000 * this.mCessPremRate *
                       mAccQuot * days / 365;
        }
        tLRPolDutyResultSchema.setCessPrem(cessPrem); //分保保费

        if (tLRDutySchema.getPolYear() > 1) {
            mChoiRebaFeeRate = 0; //如果非首次保单年度,则没有折扣
        }
        tLRPolDutyResultSchema.setChoiRebaFeeRate(this.mChoiRebaFeeRate); //选择折扣比例
        tLRPolDutyResultSchema.setReProcFee(cessPrem *
                                            this.mChoiRebaFeeRate); //再保手续费=分保保费*选择折扣比例
        tLRPolDutyResultSchema.setManageFeeRate(this.mManageFeeRate); //再保管理费比例
        tLRPolDutyResultSchema.setManageFee(cessPrem * this.mManageFeeRate); //再保管理费

        tLRPolDutyResultSchema.setManageCom(tLRPolSchema.getManageCom());
        tLRPolDutyResultSchema.setOperator(globalInput.Operator);
        tLRPolDutyResultSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRPolDutyResultSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRPolDutyResultSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRPolDutyResultSchema.setModifyTime(mPubFun.getCurrentTime());
        return true;
    }

    /**
     * 溢额分保计算结果数据准备。
     */
    private boolean prepareLRPolResult(LRPolSchema aLRPolSchema,
                                       LRPolResultSchema
                                       aLRPolResultSchema) {
        aLRPolResultSchema.setPolNo(aLRPolSchema.getPolNo());
        aLRPolResultSchema.setReContCode(aLRPolSchema.getReContCode());
        aLRPolResultSchema.setReinsureItem(aLRPolSchema.getReinsureItem());
        aLRPolResultSchema.setTempCessFlag(aLRPolSchema.getTempCessFlag());
        aLRPolResultSchema.setReNewCount(aLRPolSchema.getReNewCount());
        aLRPolResultSchema.setPayCount(0); //溢额分保不考虑期交保费,此字段默认为0
        aLRPolResultSchema.setRiskCalSort(aLRPolSchema.getRiskCalSort());
        aLRPolResultSchema.setRiskCode(aLRPolSchema.getRiskCode());
        aLRPolResultSchema.setRiskVersion(aLRPolSchema.getRiskVersion());
        aLRPolResultSchema.setCessStartDate(aLRPolSchema.getGetDataDate());
        aLRPolResultSchema.setCessEndDate(this.mEndDate);
        if(aLRPolSchema.getAmnt()<=0.00)
        	aLRPolResultSchema.setCessionRate(0);
        else
        	aLRPolResultSchema.setCessionRate(
                                          aLRPolSchema.getDeadAddFeeRate()); //分保比例
//        if(aLRPolSchema.getReinsureItem().equals("F")){//如果为反冲数据，置分保保额为负数
//            this.mCessionAmount=-this.mCessionAmount;
//        }
        aLRPolResultSchema.setCessionAmount(this.mCessionAmount); //分保保额
        aLRPolResultSchema.setCessPremRate(this.mCessPremRate); //分保费率
        String strSQL =
                "select RiskPeriod from LMRiskApp where riskcode = '" +
                aLRPolSchema.getRiskCode() + "'";
        String tRiskPeriod = new ExeSQL().getOneValue(strSQL);
        double cessPrem = 0;
        //如果是短期险
        if (tRiskPeriod.equals("L")) {
            cessPrem = mCessionAmount / 1000 * mCessPremRate * mAccQuot;
        } else {
            int days = mPubFun.calInterval(aLRPolSchema.getCValiDate(),
                                           aLRPolSchema.getEndDate(), "D");
            if(365<days)
            {
            	days=365;
            }
            cessPrem = mCessionAmount / 1000 * mCessPremRate *
                       mAccQuot * days / 365;
        }
        aLRPolResultSchema.setCessPrem(cessPrem); //分保保费

        String validate = aLRPolSchema.getCValiDate();
        int currYear = PubFun.calInterval(validate,
                                          aLRPolSchema.getGetDataDate(),
                                          "Y") + 1; //计算保单年度,第一年的保单年度为1
        if (currYear > 1) {
            mChoiRebaFeeRate = 0; //如果非首次保单年度,则没有折扣
        }
        aLRPolResultSchema.setChoiRebaFeeRate(this.mChoiRebaFeeRate); //选择折扣比例
        aLRPolResultSchema.setReProcFee(cessPrem *
                                        (this.mChoiRebaFeeRate+this.mReProcFeeRate)); //再保手续费=分保保费*选择折扣比例
        aLRPolResultSchema.setManageFeeRate(this.mManageFeeRate); //再保管理费比例
        aLRPolResultSchema.setManageFee(cessPrem * this.mManageFeeRate); //再保管理费
        aLRPolResultSchema.setPayNo(aLRPolSchema.getPayNo());
        aLRPolResultSchema.setManageCom(aLRPolSchema.getManageCom());
        aLRPolResultSchema.setOperator(strOperate);
        aLRPolResultSchema.setMakeDate(mPubFun.getCurrentDate());
        aLRPolResultSchema.setMakeTime(mPubFun.getCurrentTime());
        aLRPolResultSchema.setModifyDate(mPubFun.getCurrentDate());
        aLRPolResultSchema.setModifyTime(mPubFun.getCurrentTime());
        return true;
    }

    /**
     * 得到再保手续费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getReProcFeeRate(LRPolSchema aLRPolSchema) {
       
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"ReinProcFeeRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 得到特殊佣金比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getSpeCemmRate(LRPolSchema aLRPolSchema) {
       
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"SpeComRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 得到选择折扣比例,只针对首年度保单
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getChoiRebaFeeRate(LRPolSchema aLRPolSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='CHOIREBARATE' and riskcode='" +
                aLRPolSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 按责任分保的得到选择折扣比例,只针对首年度保单
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getChoiRebaFeeRate(LRPolSchema aLRPolSchema,
                                      LRDutySchema aLRDutySchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolSchema.getReContCode() +
                "' and upper(factorcode)='CHOIREBARATE' and riskcode='" +
                aLRDutySchema.getGetDutyCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }


    /**
     * 得到管理费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getManageFeeRate(LRPolSchema aLRPolSchema) {
        
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"ReinManFeeRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 获取按责任分保的管理费比例
     * @param aLRPolSchema LRPolSchema
     * @param aLRDutySchema LRDutySchema
     * @return double
     */
    private double getManageFeeRate(LRPolSchema aLRPolSchema,
                                    LRDutySchema aLRDutySchema) {
        String strSQL = "select factorvalue from lrcalfactorvalue where upper(factorcode)='REINMANFEERATE' " +
                        "and recontcode='" + aLRPolSchema.getReContCode() +
                        "' and riskcode='" + aLRDutySchema.getGetDutyCode() +
                        "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }
    
    /**
     * 获取按责任分保的管理费比例
     * @param aLRPolSchema LRPolSchema
     * @param aLRDutySchema LRDutySchema
     * @return double
     */
    private double getManageFeeRate(String tReContCode,
                                    String tGetDutyCode) {
        String strSQL = "select factorvalue from lrcalfactorvalue where upper(factorcode)='REINMANFEERATE' " +
                        "and recontcode='" + tReContCode +
                        "' and riskcode='" + tGetDutyCode +
                        "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }


    /**
     * 得到成数分保比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getCessionDRate(LRPolSchema aLRPolSchema,String tRiskcode) {
    	LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(tRiskcode,aLRPolSchema.getReContCode(),"CessionRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }
    
    private double getCessionRate(LRPolSchema aLRPolSchema) {
    	LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"CessionRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 获取再保合同的接受份额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getAccQuot(LRPolSchema aLRPolSchema) {
        
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"AccQuot");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 获取责任分保的接受份额
     * @param aLRPolSchema LRPolSchema
     * @param aLRDutySchema LRDutySchema
     * @return double
     */
    private double getAccQuot(LRPolSchema aLRPolSchema,
                              LRDutySchema aLRDutySchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where upper(factorcode)='ACCQUOT' " +
                "and recontcode='" + aLRPolSchema.getReContCode() +
                "' and riskcode='" + aLRDutySchema.getGetDutyCode() + "'";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");
        mReContCode = (String) mGetCessData.getValueByName("ReContCode");
        if("filldata".equals(strOperate))
        {
        	 mRiskCode = (String) mGetCessData.getValueByName("RiskCode");
             mGrpContNo = (String) mGetCessData.getValueByName("GrpContNo");
             mContNo = (String) mGetCessData.getValueByName("ContNo");
             mPolNo = (String) mGetCessData.getValueByName("PolNo");
             mOperator = (String) mGetCessData.getValueByName("Operator");
             strOperate ="fd"+PubFun.getCurrentDate2();
             mSQL = "";
             if(mReContCode!=null&&!"".equals(mReContCode)){
         		
            	 mSQL+=" and a.recontcode='"+mReContCode+"'  ";
         	}
             if(!"".equals(mGrpContNo))
         	{
            	 mSQL +=" and grpcontno ='"+mGrpContNo+"'";
         	}
         	if(!"".equals(mRiskCode))
         	{
         		mSQL +=" and riskcode ='"+mRiskCode+"'";
         	}
         	if(!"".equals(mContNo))
         	{
         		mSQL +=" and ContNo ='"+mContNo+"'";
         	}
         	if(!"".equals(mPolNo))
         	{
         		mSQL +=" and PolNo ='"+mPolNo+"'";
         	}
         	if(!"".equals(mOperator))
         	{
         		mSQL +=" and Operator ='"+mOperator+"'";
         	}
        }
        else
        {
        	strOperate = globalInput.Operator;
        }
        return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(VData cInputData,String mToDay) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = mToDay;
        mEndDate = mToDay;
        mReContCode = (String) mGetCessData.getValueByName("ReContCode");
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRCalCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private double getStandPrem(LRPolSchema aLRPolSchema) {
    	double t=0;
        String strSQL = "select lcp.InsuredAppAge,left(lcp.mult, 1)  from lcpol lcp, lccont lcc "
        		+ "where lcp.contno=lcc.contno "+
        		" and riskcode = '"+aLRPolSchema.getRiskCode()+
        		"' and  lcc.contno ='" +aLRPolSchema.getContNo()+
        		"' and lcp.polno='"+aLRPolSchema.getPolNo()+
                "' and lcc.appflag = '1'" +
        		" and lcc.conttype = '1' "+
                " with ur ";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
        	return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null||
        		tSSRS.GetText(1, 2).equals("") || tSSRS.GetText(1, 2) == null) {
        	return 0;
        }
    	if("123301".equals(aLRPolSchema.getRiskCode())){
    		if("1".equals(tSSRS.GetText(1, 2))){
    			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
    				t=292.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
    				t=1313.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
    				t=2297.0;
    			}else{
    				t=2297.0;
    			}
    		}else{
    			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
    				t=549.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
    				t=2505.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
    				t=4385.0;
    			}else{
    				t=4385.0;
    			}
    			
    		}
    	}
        	if("123401".equals(aLRPolSchema.getRiskCode())){
        		if("1".equals(tSSRS.GetText(1, 2))){
        			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
        				t=222.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
        				t=992.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
        				t=1987.0;
        			}else{
        				t=1987.0;
        			}
        		}else{
        			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
        				t=426.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
        				t=1912.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
        				t=3655.0;
        			}else{
        				t=3655.0;
        			}
        			
        		}

    	   }
    	
//        double tresult=Double.parseDouble(t);
        return t;
    }
    /**
     * 海外医疗产品团险险种获取标准保费
     * @return String
     */
    private double getGrpStandPrem(LRPolSchema aLRPolSchema) {
    	double t=0;
        String strSQL = "select lcp.InsuredAppAge,"
        		+ " (select distinct calfactorvalue from lccontplandutyparam where grpcontno = lcg.grpcontno and calfactor = 'Mult' and contplancode = lcp.contplancode)  "
        		+ "from lcgrpcont lcg, lcpol lcp where lcg.grpcontno = lcp.grpcontno"+
        		"  and lcp.riskcode = '"+aLRPolSchema.getRiskCode()+
        		"' and lcg.grpcontno ='" +aLRPolSchema.getGrpContNo()+
        		"' and lcp.polno = '"+aLRPolSchema.getPolNo()+
                "' and lcg.appflag = '1'" +
                " with ur ";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
        	return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null||
        		tSSRS.GetText(1, 2).equals("") || tSSRS.GetText(1, 2) == null) {
        	return 0;
        }
        if("162701".equals(aLRPolSchema.getRiskCode())){
    		if("1".equals(tSSRS.GetText(1, 2))){
    			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
    				t=292.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
    				t=1313.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
    				t=2297.0;
    			}else{
    				t=2297.0;
    			}
    		}else{
    			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
    				t=549.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
    				t=2505.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
    				t=4385.0;
    			}else{
    				t=4385.0;
    			}
    			
    		}
    	}
        	if("162901".equals(aLRPolSchema.getRiskCode())){
        		if("1".equals(tSSRS.GetText(1, 2))){
        			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
        				t=222.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
        				t=992.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
        				t=1987.0;
        			}else{
        				t=1987.0;
        			}
        		}else{
        			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
        				t=426.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
        				t=1912.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
        				t=3655.0;
        			}else{
        				t=3655.0;
        			}
        			
        		}

    	   }
            String strSQL1 = "select peoples2 from lcgrppol where grppolno = '"+aLRPolSchema.getGrpPolNo()+"'";
            SSRS tSSRS1 = new ExeSQL().execSQL(strSQL1);
            if (tSSRS1.getMaxRow() <= 0) {
            	return 0;
            }
            if (tSSRS1.GetText(1, 1).equals("") || tSSRS1.GetText(1, 1) == null) {
            	return 0;
            }
            int peoplenum = Integer.parseInt(tSSRS1.GetText(1, 1));
            double count = 0;
            if(peoplenum-24<=0){
            	 count = 1;
            }else if(peoplenum-24>0&&peoplenum-100<=0){
            	 count = 0.925; 
            }else{
            	 count = 0.9;
            }
            t= t*count;
        return t;
    }
    private double getFamStandPrem(LRPolSchema aLRPolSchema) {
    	double t=0;
        String strSQL = "select lcp.InsuredAppAge,"
        		+ " (select distinct calfactorvalue from lccontplandutyparam where grpcontno = lcg.grpcontno and calfactor = 'Mult' and contplancode = lcp.contplancode)  "
        		+ ",lcp.polno from lcgrpcont lcg, lcpol lcp where lcg.grpcontno=lcp.grpcontno and "+
        		" lcp.riskcode = '"+aLRPolSchema.getRiskCode()+
        		"' and  lcg.grpcontno ='" +aLRPolSchema.getGrpContNo()+
        		"' and lcp.polno = '"+aLRPolSchema.getPolNo()+
                "' and lcg.appflag = '1' " +
                " with ur ";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        SSRS tSSRS3=null;
        if (tSSRS.getMaxRow() <= 0) {
        	return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null||
        		tSSRS.GetText(1, 2).equals("") || tSSRS.GetText(1, 2) == null) {
        	return 0;
        }
        if("162601".equals(aLRPolSchema.getRiskCode())){
    		if("1".equals(tSSRS.GetText(1, 2))){
    			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
    		        String strSQL3 = "select  count(1) from lcpol where grppolno = '"+aLRPolSchema.getGrpPolNo()+"' and riskcode = '"
    		                +aLRPolSchema.getRiskCode()+"' and InsuredAppAge<=18 with ur";
    		        tSSRS3 = new ExeSQL().execSQL(strSQL3);
    		        int childCount = Integer.parseInt(tSSRS3.GetText(1, 1));
    				if(tSSRS3.GetText(1, 1) == null||("").equals(tSSRS3.GetText(1, 1))){
    					t=0;
    				}else{
    					t=292.0/childCount;
    				}
    				tSSRS3.Clear();
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
    				t=1313.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
    				t=2297.0;
    			}else{
    				t=2297.0;
    			}
    		}else{
    			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
    				t=549.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
    				t=2505.0;
    			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
    				t=4385.0;
    			}else{
    				t=4385.0;
    			}
    			
    		}
    	}
        	if("162801".equals(aLRPolSchema.getRiskCode())){
        		if("1".equals(tSSRS.GetText(1, 2))){
        			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
//        		        String strSQL3 = "select  max(polno) from lcpol where grppolno = '"+aLRPolSchema.getGrpPolNo()+"' and riskcode = '"
//		                +aLRPolSchema.getRiskCode()+"' and InsuredAppAge<=18 with ur";
				        String strSQL3 = "select  count(1) from lcpol where grppolno = '"+aLRPolSchema.getGrpPolNo()+"' and riskcode = '"
		                +aLRPolSchema.getRiskCode()+"' and InsuredAppAge<=18 with ur";
				        tSSRS3 = new ExeSQL().execSQL(strSQL3);
				        int childCount = Integer.parseInt(tSSRS3.GetText(1, 1));
						if(tSSRS3.GetText(1, 1) == null||("").equals(tSSRS3.GetText(1, 1))){
							t=0;
		//				}
		//				if(tSSRS.GetText(1, 3).equals(tSSRS3.GetText(1, 1))){
		//    				t=222.0;
						}else{
							t=222.0/childCount;
						}
						tSSRS3.Clear();
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
        				t=992.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
        				t=1987.0;
        			}else{
        				t=1987.0;
        			}
        		}else{
        			if(Integer.parseInt(tSSRS.GetText(1, 1))-18<=0){
        				t=426.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-19>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-64<=0){
        				t=1912.0;
        			}else if(Integer.parseInt(tSSRS.GetText(1, 1))-65>=0&&Integer.parseInt(tSSRS.GetText(1, 1))-74<=0){
        				t=3655.0;
        			}else{
        				t=3655.0;
        			}
        			
        		}

    	   }
        	//团单对被保总人数有个折标
            String strSQL1 = "select peoples2 from lcgrppol where grppolno = '"+aLRPolSchema.getGrpPolNo()+"'";
            SSRS tSSRS1 = new ExeSQL().execSQL(strSQL1);
            if (tSSRS1.getMaxRow() <= 0) {
            	return 0;
            }
            if (tSSRS1.GetText(1, 1).equals("") || tSSRS1.GetText(1, 1) == null) {
            	return 0;
            }
            int peoplenum = Integer.parseInt(tSSRS1.GetText(1, 1));
            
            String strSQL2 = "select count(1) from lcpol where grppolno = '"+aLRPolSchema.getGrpPolNo()+"' and InsuredAppAge<=18 and riskcode ='"+aLRPolSchema.getRiskCode()+"'";
            SSRS tSSRS2 = new ExeSQL().execSQL(strSQL2);
            int nonage =0;
            if (tSSRS2.getMaxRow() <= 0) {
            	nonage= 0;
            }
            if (tSSRS2.GetText(1, 1).equals("") || tSSRS2.GetText(1, 1) == null) {
            	nonage= 0;
            }else{
            	nonage = Integer.parseInt(tSSRS2.GetText(1, 1));
            }
            if(nonage>=1){
            	nonage=nonage-1; 
            }
            double count = 0;
            if(peoplenum-nonage-2==0){
            	 count = 0.95;
            }else if(peoplenum-nonage-3==0||peoplenum-nonage-4==0){
            	 count = 0.9; 
            }else{
            	 count = 0.875;
            }
            t= t*count;
        return t;
    }
    
    
    private void jbInit() throws Exception {
    }
}
