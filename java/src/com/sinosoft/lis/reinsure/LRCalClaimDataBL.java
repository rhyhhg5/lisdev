/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LRPolClmSet;
import com.sinosoft.lis.vschema.LRPolResultSet;
import com.sinosoft.lis.db.LRPolResultDB;

/*
 * <p>ClassName: LRCalClaimDataBL </p>
 * <p>Description: LRCalClaimDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: huxl
 * @CreateDate：2007-03-27
 */
public class LRCalClaimDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private PubFun mPubFun = new PubFun();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    //业务处理相关变量
    /** 全局数据 */

    public LRCalClaimDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRCalClaimDataBL tLRCalClaimDataBL = new LRCalClaimDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2007-7-1");
            tTransferData.setNameAndValue("EndDate", "2007-11-20");
            System.out.println("in main: " +
                               (String) tTransferData.
                               getValueByName("StartDate") + "   " +
                               (String) tTransferData.getValueByName("EndDate"));
            vData.add(tTransferData);
            tLRCalClaimDataBL.submitData(vData, "CLAIMDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取理赔数据
        if (this.strOperate.equals("CLAIMDATA")) {
            if (!calculateClaimData()) {
                // @@错误处理
                buildError("insertData", "");
                return false;
            }
        }
        return true;
    }


    /**
     * 提取理赔数据
     * @return boolean
     */
    private boolean calculateClaimData() {
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");

        String strSQL = "select * from LRPolClm where GetDataDate between '" +
                        mStartDate + "' and '" + mEndDate + "' with ur";
        LRPolClmSet tLRPolClmSet = new LRPolClmSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolClmSet, strSQL);
        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolClmSet.size(); i++) {
                    LRPolClmSchema tLRPolClmSchema = tLRPolClmSet.get(i);
                    LRPolClmResultSchema tLRPolClmResultSchema = new
                            LRPolClmResultSchema();
                    if (tLRPolClmSchema.getRiskCalSort().equals("1")) {
                        //处理医疗险理赔退费
                        double cessRate = getCessionRate(tLRPolClmSchema);
                        double claimBackFee = tLRPolClmSchema.getRealPay() *
                                              cessRate;
                        tLRPolClmResultSchema.setClaimBackFee(claimBackFee); //理赔摊回
                        prepareLRPolClmResult(tLRPolClmSchema,
                                              tLRPolClmResultSchema);
                        this.mMap.put(tLRPolClmResultSchema, "DELETE&INSERT");
                    } else {
                        LRPolResultDB tLRPolResultDB = new LRPolResultDB();
                        tLRPolResultDB.setPolNo(tLRPolClmSchema.getPolNo());
                        tLRPolResultDB.setReContCode(tLRPolClmSchema.
                                getReContCode());
                        tLRPolResultDB.setReinsureItem(tLRPolClmSchema.
                                getReinsureItem());
                        tLRPolResultDB.setReNewCount(tLRPolClmSchema.
                                getReNewCount());
                        LRPolResultSet tLRPolResultSet = tLRPolResultDB.query();
                        if (tLRPolResultSet.size() > 0) {
                            double cessionAmnt = tLRPolResultSet.get(1).
                                                 getCessionAmount(); //分保保额
                            double accQuot = this.getAccQuot(tLRPolClmSchema);
                            String str =
                                    "select Amnt from LRPol where Polno = '" +
                                    tLRPolClmSchema.getPolNo() +
                                    "' and ReContCode = '" +
                                    tLRPolClmSchema.getReContCode() +
                                    "' and ReinsureItem = '" +
                                    tLRPolClmSchema.getReinsureItem() +
                                    "' and ReNewCount = " +
                                    tLRPolClmSchema.getReNewCount();
                            String amnt = new ExeSQL().getOneValue(str);
                            double claimBackFee = cessionAmnt * accQuot *
                                                  tLRPolClmSchema.getRealPay() /
                                                  Double.parseDouble(amnt);
                            tLRPolClmResultSchema.setClaimBackFee(claimBackFee); //理赔摊回
                            prepareLRPolClmResult(tLRPolClmSchema,
                                                  tLRPolClmResultSchema);
                            this.mMap.put(tLRPolClmResultSchema,
                                          "DELETE&INSERT");
                        } else {
                            this.errorLog(tLRPolClmSchema, "没有该保单的分保计算结果");
                        }
                    }
                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolClmSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
        return true;
    }

    private boolean prepareLRPolClmResult(LRPolClmSchema aLRPolClmSchema,
                                          LRPolClmResultSchema
                                          aLRPolClmResultSchema) {
        aLRPolClmResultSchema.setPolNo(aLRPolClmSchema.getPolNo());
        aLRPolClmResultSchema.setClmNo(aLRPolClmSchema.getClmNo());
        aLRPolClmResultSchema.setRgtNo(aLRPolClmSchema.getRgtNo());
        aLRPolClmResultSchema.setCaseNo(aLRPolClmSchema.getCaseNo());
        aLRPolClmResultSchema.setReContCode(aLRPolClmSchema.
                                            getReContCode());
        String strSQL =
                "select Recomcode from LRContInfo where RecontCode = '" +
                aLRPolClmResultSchema.getReContCode() + "'";
        aLRPolClmResultSchema.setReComCode(new ExeSQL().getOneValue(
                strSQL));
        aLRPolClmResultSchema.setReinsureItem(aLRPolClmSchema.
                                              getReinsureItem());
        aLRPolClmResultSchema.setRiskCalSort(aLRPolClmSchema.
                                             getRiskCalSort());
        aLRPolClmResultSchema.setRiskCode(aLRPolClmSchema.getRiskCode());
        aLRPolClmResultSchema.setRiskVer(aLRPolClmSchema.getRiskVer());
        aLRPolClmResultSchema.setCessStartDate(this.mStartDate);
        aLRPolClmResultSchema.setCessEndDate(this.mEndDate);

        aLRPolClmResultSchema.setOperator(globalInput.Operator);
        aLRPolClmResultSchema.setManageCom(globalInput.ManageCom);
        aLRPolClmResultSchema.setMakeDate(mPubFun.getCurrentDate());
        aLRPolClmResultSchema.setMakeTime(mPubFun.getCurrentTime());
        aLRPolClmResultSchema.setModifyDate(mPubFun.getCurrentDate());
        aLRPolClmResultSchema.setModifyTime(mPubFun.getCurrentTime());
        return true;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolClmSchema aLRPolClmSchema,
                             String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new
                                             LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolClmSchema.getPolNo());
        tLRErrorLogSchema.setLogType("7"); //5:新单计算
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }

    /**
     * 得到成数分保比例
     * @param LRPolEdorSchema aLRPolEdorSchema
     * @return String
     */
    private double getCessionRate(LRPolClmSchema aLRPolClmSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolClmSchema.getReContCode() +
                "' and upper(factorcode)='CESSIONRATE' and riskcode='" +
                aLRPolClmSchema.getRiskCode() + "'";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 获取再保合同的接受份额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getAccQuot(LRPolClmSchema aLRPolClmSchema) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                aLRPolClmSchema.getReContCode() +
                "' and upper(factorcode)='ACCQUOT' and riskcode='" +
                aLRPolClmSchema.getRiskCode() + "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
}
