/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import java.util.Date;

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: huxl
 * @CreateDate：2006-11-01
 */
public class LRGNewGetEdorDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    
    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    private String mCurrentDate = new PubFun().getCurrentDate();

    private String mCurrentTime = new PubFun().getCurrentTime();

    //业务处理相关变量
    /** 全局数据 */

    public LRGNewGetEdorDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

  
    
    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT(String cOperate,String mToDay) {
        System.out.println("Come in BL->Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputDataT(mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
//            tError.moduleName = "LDComBL";
//            tError.functionName = "prepareData";
//            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
//            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取保全数据
       
            if (!getEdorData()) {
                buildError("insertData", "单证管理员信息有误!");
                return false;
            }
        
        return true;
    }

    /**
     * 提取保全数据
     * @return boolean
     */
    private boolean insertEdorData() {
        System.out.println("come in insertEdorData()..");
        //提取共保保全数据，2014-9-2 杨阳
        //财务接口表提取共保数据时，对于签单日期和生效日期孰后的原则，在2014-1-1之后的保单，提取规则不一样,提取所有的共保数据，全用新规则进行提取
        String dataSql ="";
        //提取再保保全数据
        String tSql =
                "select a.GrpContNo A,"+
                "a.GrpPolNo B,"+
                "'00000000000' C,"+
                "(select PolNo from lbpol where grppolno=a.grppolno and prem<>0 fetch first  1 rows only) D,"+
                "(select max(EndorsementNo) from ljagetendorse c where c.grpcontno = a.grpcontno and c.grppolno = a.grppolno " +
                "	and c.riskcode = a.riskcode and c.makedate= '"+this.mToday+"' and exists(select 1 from lbpol where c.grppolno = grppolno and c.polno = polno " +
                "	union all select 1 from lcpol where c.grppolno = grppolno and c.polno = polno )) E,"+
                "'00000000000' F,"+
                "'00000000000' G,"+
                "a.RiskCode H,"+
                "(select RiskVer from lmrisk where riskcode=a.riskcode) I,"+
                "null J,"+
                " a.PayToDate K,"+
                " '' L,"+
                "finaInterTable.prem M,"+
                " '" + this.mToday + "',"+
                "a.Amnt O,"+
                "a.ManageCom P,"+
                "0 Q,"+
                "a.agentGroup R,"+
                "a.CValidate S,"+
                "a.payenddate T"+
  " from lbgrppol a,"+
                " (select bb.indexno,"+
                " bb.grpcontno,"+
                " bb.grppolno,"+
                " sum(case aa.finitemtype"+
                " when 'C' then"+
                " aa.summoney"+
                " else"+
                " -aa.summoney"+
                " end) as prem"+
                " from fivoucherdatadetail aa, fiabstandarddata bb"+
                " where aa.accountcode = '6031000000'"+
                " and aa.accountdate = '"+this.mToday+"'"+
                " and aa.certificateid = 'BQ-GB-QR'"+
                " and aa.classtype in ('N-05','B-03', 'B-04','N-05-Y','B-03-Y', 'B-04-Y') "+
                " and aa.serialno = bb.serialno"+
                " group by bb.indexno, bb.grpcontno, bb.grppolno) as finaInterTable"+
                " where a.grpcontno = finaInterTable.grpcontno"+
                " and a.grppolno = finaInterTable.grppolno " +
                " and exists (select 1 from LRPol where  grppolno=a.grppolno ) "+
                " and not exists (select 1 from LRPol where  grppolno=a.grppolno and TempCessFlag='Y') "+
                " union all " +
                "select a.GrpContNo A,"+
                "a.GrpPolNo B,"+
                "'00000000000' C,"+
                "(select PolNo from lbpol where grppolno=a.grppolno and prem<>0 fetch first  1 rows only) D,"+
                "(select max(EndorsementNo) from ljagetendorse c where c.grpcontno = a.grpcontno and c.grppolno = a.grppolno " +
                "	and c.riskcode = a.riskcode and c.makedate= '"+this.mToday+"' and exists(select 1 from lbpol where c.grppolno = grppolno and c.polno = polno " +
                "	union all select 1 from lcpol where c.grppolno = grppolno and c.polno = polno )) E,"+
                " '00000000000' F,"+
                "'00000000000' G,"+
                "a.RiskCode H,"+
                "(select RiskVer from lmrisk where riskcode=a.riskcode) I,"+
                " null  J,"+
                " a.PayToDate K,"+
                " '' L,"+
                "finaInterTable.prem M,"+
                " '" + this.mToday + "',"+
                "a.Amnt O,"+
                "a.ManageCom P,"+
                "0 Q,"+
                "a.agentGroup R,"+
                "a.CValidate S,"+
                "a.payenddate T"+
  " from lcgrppol a,"+
                " (select bb.indexno,"+
                " bb.grpcontno,"+
                " bb.grppolno," +
                " sum(case aa.finitemtype"+
                " when 'C' then"+
                " aa.summoney"+
                " else"+
                " -aa.summoney"+
                " end) as prem"+
                " from fivoucherdatadetail aa, fiabstandarddata bb"+
                " where aa.accountcode = '6031000000'"+
                " and aa.accountdate = '"+this.mToday+"'"+
                " and aa.certificateid = 'BQ-GB-QR'"+
                " and aa.classtype in ('N-05','B-03', 'B-04','N-05-Y','B-03-Y', 'B-04-Y') "+
                " and aa.serialno = bb.serialno"+
                " group by bb.indexno, bb.grpcontno, bb.grppolno) as finaInterTable"+
                " where a.grpcontno = finaInterTable.grpcontno"+
                " and a.grppolno = finaInterTable.grppolno  " +
                " and exists (select 1 from LRPol where  grppolno=a.grppolno ) "+
                " and not exists (select 1 from LRPol where  grppolno=a.grppolno and TempCessFlag='Y') "+
                " with ur ";
//        System.out.println(tSql);
        int start = 1;
        int nCount = 5000;
        while (true) {
            SSRS tSSRS = new ExeSQL().execSQL(tSql, start, nCount);
            if (tSSRS.getMaxRow() <= 0) {
                break;
            }
            mMap = new MMap();
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                    LRPolEdorSchema tLRPolEdorSchema = new LRPolEdorSchema();
                    /*2014-7-25 16：10 杨阳修改
                     * BUG描述：
                     * 对于共保中的增人，客户资料变更，保费追加，无名单增人等
                    	1.只会向lcpol表中插入数，
                    	2.并且未发生其它导致向lbpol表插入数据的保全项
                    	按以上sql提取到的polno为空，
                    	修改此bug，会对于这样的数据进行polno重新赋值，即从lcpol表中查询                     
                     */	
                    String polno =tSSRS.GetText(i, 4);
                    if("".equals(polno)||null==polno)
                    {
                    	polno = new ExeSQL().getOneValue("select PolNo from lcpol where grppolno='"+tSSRS.GetText(i, 2)+"' and prem<>0 fetch first  1 rows only");
                    }
                    //添加其他数据
                    tLRPolEdorSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolEdorSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolEdorSchema.setContNo(tSSRS.GetText(i, 3));
                    tLRPolEdorSchema.setPolNo(polno);
                    tLRPolEdorSchema.setEdorNo(tSSRS.GetText(i, 5));
                    tLRPolEdorSchema.setMainPolNo(tSSRS.GetText(i, 6));
                    tLRPolEdorSchema.setMasterPolNo(tSSRS.GetText(i, 7));
                    tLRPolEdorSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolEdorSchema.setRiskVersion(tSSRS.GetText(i, 9));
                    //为了对于2014-1-1之后财务接口提取再保数据新规则，2014-8-28 杨阳
                    //为了减少查询sql的逻辑，对于保全生效日期在代码中进行查询
                    String edorValidate = new ExeSQL().getOneValue("select max(EdorValidate) from LPGrpEdorItem where EdorNo = '"+tSSRS.GetText(i, 5)+"'");
                    tLRPolEdorSchema.setEdorValidate(edorValidate);
                    int noPassDays ; //未经过天数
                    String mSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
                    String LRiskFlag = new ExeSQL().getOneValue(mSql);
                    if(Integer.parseInt(LRiskFlag)>0){//长险，未经过天数＝交至日期－保全生效日
                         noPassDays =PubFun.calInterval(tSSRS.GetText(i,10),tSSRS.GetText(i,11),"D")%365;
                    }else{//短险，未经过天数＝终止日期－保全生效日
                         noPassDays =PubFun.calInterval(tSSRS.GetText(i,10),tSSRS.GetText(i,20),"D");
                    }
                    if (noPassDays <= 0) {
                        noPassDays = 0; //保全摊回的时候，失效日期可以大于缴至日期,此时摊回零.
                    }

                    tLRPolEdorSchema.setNoPassDays(noPassDays);
                    //为了对于2014-1-1之后财务接口提取再保数据新规则，2014-8-28 杨阳
                    //为了减少查询sql的逻辑，对于保全类型在代码中进行查询
                    String feeoperationType = new ExeSQL().getOneValue("select max(edortype) from LPGrpEdorItem where EdorNo = '"+tSSRS.GetText(i, 5)+"'");
                    tLRPolEdorSchema.setFeeoperationType(feeoperationType);
                    tLRPolEdorSchema.setGetMoney(tSSRS.GetText(i, 13));
                    tLRPolEdorSchema.setGetConfDate(tSSRS.GetText(i, 14));
                    tLRPolEdorSchema.setSumBackAmnt(tSSRS.GetText(i, 15));
                    tLRPolEdorSchema.setManageCom(tSSRS.GetText(i, 16));
                    tLRPolEdorSchema.setGetDataDate(this.mToday); //数据提取日期
                    tLRPolEdorSchema.setOperator("ServerG");
                    tLRPolEdorSchema.setMakeDate(this.mCurrentDate);
                    tLRPolEdorSchema.setMakeTime(this.mCurrentTime);
                    tLRPolEdorSchema.setModifyDate(this.mCurrentDate);
                    tLRPolEdorSchema.setModifyTime(this.mCurrentTime);
//                    String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
//                    String LRiskFlag = new ExeSQL().getOneValue(aSql);
                    String tcvdatey=tSSRS.GetText(i, 19).substring(0,4);
                	String ttodatey=this.mToday.substring(0,4);
                	int tyear=Integer.parseInt(ttodatey)-Integer.parseInt(tcvdatey)+1;
                    if(Integer.parseInt(LRiskFlag)>0){
                    	
                        tLRPolEdorSchema.setReNewCount(tyear); //保单年度
                    }else{
                        tLRPolEdorSchema.setReNewCount(tSSRS.GetText(i, 17)); //续保次数
                    }
                     //新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"+
                                 tSSRS.GetText(i,18)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolEdorSchema.setCostCenter(CostCenter);
                    //String PolYear = tSSRS.GetText(i,19);//保单年度,长险lrpol表的renewcount存的是保单年度,短险存续保数
                    tSql =  "select distinct a.ReContCode,a.ReinsureItem,a.RiskCalSort, (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                            " comcode " +
                            "from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount "+
                            "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod != 'L') and a.grppolno = '" +
                            tSSRS.GetText(i, 2) + "'   "+
                            " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                            "union all "+
                            "select distinct a.ReContCode,a.ReinsureItem,a.RiskCalSort, (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                            " comcode " +
                            "from LRPol a,LBPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount "+
                            "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod != 'L') and a.grppolno = '" +
                            tSSRS.GetText(i, 2) + "'   "+
                            //此处是为了兼容旧系统的分保，长险时lrpol表的renewcount存的是0
                            
                            " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                            " with ur";
                    if(feeoperationType.equals("WZ")||feeoperationType.equals("WJ")){
                    	tSql =  "select distinct a.ReContCode,a.ReinsureItem,a.RiskCalSort, (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                        " comcode " +
                        "from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount "+
                        "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod != 'L') and a.grppolno = '" +
                        tSSRS.GetText(i, 2) + "'   "+
                        " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                        " union all "+
                        "select distinct a.ReContCode,a.ReinsureItem,a.RiskCalSort,(select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                        " comcode " +
                        "from LRPol a,LCPol b where a.PolNo = b.PolNO "+
                        " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.grppolno = '" +
                        tSSRS.GetText(i, 2) + "'  "+
                        //此处是为了兼容旧系统的分保，长险时lrpol表的renewcount存的是0
                        
                        " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01') " +
                        " with ur";
                    }
//                    System.out.println("querycontcode:"+tSql);
                    SSRS cSSRS = new ExeSQL().execSQL(tSql);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                        tLRPolEdorSchema.setReContCode(cSSRS.GetText(j, 1));
                        if(cSSRS.GetText(j, 2).equals("F")){  //当取出来的是反冲数据时，跳过不提取。
                            continue;
                        }
                        tLRPolEdorSchema.setReinsureItem("G");
                        tLRPolEdorSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolEdorSchema.setReComCode(cSSRS.GetText(j, 4));
                        mMap.put(tLRPolEdorSchema.getSchema(), "DELETE&INSERT");
                    }
                }
            }
            if (!prepareOutputData()) {
            }
            //如果提交不成功,不能返回
            tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(this.mInputData, "")) {
                if (tPubSubmit.mErrors.needDealError()) {
                    buildError("insertData", "提取再保保全数据出错!");
                }
            }
            start += nCount;
        }
        return true;
    }

 


    /**
     * 提取保全数据
     * @return boolean
     */
    private boolean getEdorData() {
        

        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
        

       
            this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!insertEdorData()) {
                buildError("getEdorData", "提取" + mToday + "号保全数据出错");
                return false;
            }
           
            
        
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }



    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(String mToDay) {
       
        mStartDate = mToDay;
        
        return true;
    }
    public String getResult() {
        return "没有传数据";
    }
    
    

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
//        cError.moduleName = "LRGetEdorDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "86";
      
        LRGNewGetEdorDataBL tLRNewGetEdorDataBL = new LRGNewGetEdorDataBL();
        try {
            tLRNewGetEdorDataBL.submitDataT("ceshi", "2014-9-4");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
