/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import java.util.Date;

import com.sinosoft.lis.db.LRTempCessContDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRPolEdorSchema;
import com.sinosoft.lis.schema.LRTempCessContSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: huxl
 * @CreateDate：2006-11-01
 */
public class LRTempGetEdorDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    
    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    private String mCurrentDate = new PubFun().getCurrentDate();

    private String mCurrentTime = new PubFun().getCurrentTime();

    //业务处理相关变量
    /** 全局数据 */

    public LRTempGetEdorDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

  
    
   

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
//            tError.moduleName = "LDComBL";
//            tError.functionName = "prepareData";
//            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
//            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

   

    /**
     * 提取保全数据
     * @return boolean
     */
    private boolean insertEdorData(LRTempCessContSchema tLRTempCessContSchema) {
        System.out.println("come in insertEdorData()..");
        //提取再保保全数据
        String tSql =
                  "select a.GrpContNo A,"+
                "a.GrpPolNo B,"+
                "a.ContNo C,"+
                "a.PolNo D,"+
                "a.EdorNo E,"+
                "a.MainPolNo F,"+
                "a.MasterPolNo G,"+
                "a.RiskCode H,"+
                "a.RiskVersion I,"+
                "(select max(EdorValidate)"+
                   " from LPGrpEdorItem"+
                  " where EdorNo = a.edorno"+
                    " and GrpContNo = a.GrpContNo) J,"+
                " a.PayToDate K,"+
                
                " b.FeeOperationType L,"+
                "sum(b.getmoney) M,"+
                " '" + this.mToday + "',"+
                "a.Amnt O,"+
                "a.ManageCom P,"+
                "a.ReNewCount Q,"+
                "a.agentGroup R,"+
                "a.CValidate S,"+
                "a.enddate T"+
  " from lbpol a,"+
                " LJAGetEndorse b,"+
                " (select bb.payno,"+
                " bb.grpcontno,"+
                " bb.grppolno,"+
                " sum(case aa.finitemtype"+
                " when 'C' then"+
                " aa.summoney"+
                " else"+
                " -aa.summoney"+
                " end) as prem"+
                " from lidatatransresult aa, LIABORIGINALDATA bb"+
                " where aa.accountcode = '6031000000'"+
                " and aa.accountdate <= '"+this.mToday+"'"+
                " and aa.classtype in ('B-03', 'B-04','B-03-Y', 'B-04-Y')"+
                " and aa.standbystring3 = bb.serialno"+
                " and bb.feeoperationtype in ('WT','XT','ZT','CT')"+
                " and bb.grpcontno ='"+tLRTempCessContSchema.getContno()+"' "+
                " group by bb.payno, bb.grpcontno, bb.grppolno) as finaInterTable"+
                " where a.grpcontno = finaInterTable.grpcontno"+
                " and a.grppolno = finaInterTable.grppolno"+
                " and a.conttype = '2'"+
                " and a.polno = b.polno "+
                " and a.edorno = b.EndorsementNo"+
                " and b.actugetno = finaInterTable.payno"+
                " and b.FeeFinaType <> 'GB'"+
                " and exists (select 1 from LRPol where polno = a.polno " +
                " and recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
                		" and grpcontno='"+tLRTempCessContSchema.getContno()+"')  "+
                " group by a.GrpContNo,"+
                " a.GrpPolNo,"+
                " a.ContNo,"+
                " a.PolNo,"+
                " a.EdorNo,"+
                " a.MainPolNo,"+
                " a.MasterPolNo,"+
                " a.RiskCode,"+
                " a.RiskVersion,"+
                " a.PayToDate,"+
                " b.FeeOperationType,"+
                " a.Amnt,"+
                " a.ManageCom,"+
                " a.ReNewCount,"+
                " a.agentGroup,"+
                " a.CValidate,"+
                " a.enddate" + //个单协议退保
				" union all " +
				"select a.GrpContNo A,"+
				"a.GrpPolNo B,"+
				"a.ContNo C,"+
				"a.PolNo D,"+
				"b.EndorsementNo E,"+
				"a.MainPolNo F,"+
				"a.MasterPolNo G,"+
				"a.RiskCode H,"+
				"a.RiskVersion I,"+
				"(select max(EdorValidate)"+
				   " from LPgrpEdorItem"+
				  " where EdorNo = b.EndorsementNo"+
				    " and GrpContNo = a.GrpContNo) J,"+
				" a.PayToDate K,"+
				" b.FeeOperationType L,"+
				"sum(finaInterTable.prem) M,"+
				" '" + this.mToday + "',"+
				"a.Amnt O,"+
				"a.ManageCom P,"+
				"a.ReNewCount Q,"+
				"a.agentGroup R,"+
				"a.CValidate S,"+
				"a.enddate T"+
				" from lcpol a,"+
				" LJAGetEndorse b,"+
				" (select bb.payno,"+
				" bb.grpcontno,"+
				" bb.grppolno,"+
				" sum(case aa.finitemtype"+
				" when 'C' then"+
				" aa.summoney"+
				"    else"+
				"     -aa.summoney"+
				" end) as prem"+
				" from lidatatransresult aa, LIABORIGINALDATA bb"+
				" where aa.accountcode = '6031000000'"+
				"  and aa.accountdate = '"+this.mToday+"'"+
				" and aa.standbystring3 = bb.serialno"+
				"  and ("+
				" (aa.classtype in ('B-03', 'B-03-Y') and bb.feeoperationtype in ('BB','BJ'))"+
				" ) and bb.grpcontno ='"+tLRTempCessContSchema.getContno()+"' "+
				" group by bb.payno, bb.grpcontno,bb.grppolno) as finaInterTable"+
				" where a.grpcontno = finaInterTable.grpcontno"+
				" and a.grppolno = finaInterTable.grppolno"+
				" and a.conttype = '2'"+
				" and b.actugetno = finaInterTable.payno"+
				" and b.FeeFinaType <> 'GB'"+
				" and a.polno = (select max(polno) from LRPol where grpcontno= a.grpcontno) "+
                " and exists (select 1 from LRPol where polno = a.polno " +
                " and recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
                		" and grpcontno='"+tLRTempCessContSchema.getContno()+"')  "+
				" group by a.GrpContNo,"+
				"  a.GrpPolNo,"+
				" a.ContNo,"+
				" a.PolNo,"+
				" b.EndorsementNo,"+
				" a.MainPolNo,"+
				" a.MasterPolNo,"+
				" a.RiskCode,"+
				" a.RiskVersion,"+
				" a.PayToDate,"+
				" b.FeeOperationType,"+
				" a.Amnt,"+
				" a.ManageCom,"+
				" a.ReNewCount,"+
				" a.agentGroup,"+
				" a.CValidate,"+
				" a.enddate" + 
                " union all " +
                "select a.GrpContNo A,"+
                "a.GrpPolNo B,"+
                "a.ContNo C,"+
                "a.PolNo D,"+
                "b.EndorsementNo E,"+
                "a.MainPolNo F,"+
                "a.MasterPolNo G,"+
                "a.RiskCode H,"+
                "a.RiskVersion I,"+
                "(select max(EdorValidate)"+
                   " from LPGrpEdorItem"+
                  " where EdorNo = b.EndorsementNo"+
                    " and GrpContNo = a.GrpContNo) J,"+
                " a.PayToDate K,"+
                
                " b.FeeOperationType L,"+
                "sum(b.getmoney) M,"+
                " '" + this.mToday + "',"+
                "a.Amnt O,"+
                "a.ManageCom P,"+
                "a.ReNewCount Q,"+
                "a.agentGroup R,"+
                "a.CValidate S,"+
                "a.enddate T"+
  " from lcpol a,"+
                " LJAGetEndorse b,"+
                " (select bb.payno,"+
                " bb.grpcontno,"+
                " bb.grppolno,"+
                " sum(case aa.finitemtype"+
                " when 'C' then"+
                " aa.summoney"+
                "    else"+
                "     -aa.summoney"+
                " end) as prem"+
                " from lidatatransresult aa, LIABORIGINALDATA bb"+
                " where aa.accountcode = '6031000000'"+
                "  and aa.accountdate <= '"+this.mToday+"'"+
                " and aa.standbystring3 = bb.serialno"+
                "  and ("+
                " (aa.classtype in ('B-03', 'B-04','B-03-Y', 'B-04-Y') and bb.feeoperationtype in ('RB','WZ','WJ'))"+
                " ) and bb.grpcontno ='"+tLRTempCessContSchema.getContno()+"' "+
                " group by bb.payno, bb.grpcontno,bb.grppolno) as finaInterTable"+
                " where a.grpcontno = finaInterTable.grpcontno"+
                " and a.grppolno = finaInterTable.grppolno"+
                " and a.conttype = '2'"+
                " and a.polno = b.polno "+
                " and a.contno = b.contno"+
                " and b.actugetno = finaInterTable.payno"+
                " and b.FeeFinaType <> 'GB'"+
                " and exists (select 1 from LRPol where polno = a.polno " +
                " and recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
                		" and grpcontno='"+tLRTempCessContSchema.getContno()+"')  "+
                " group by a.GrpContNo,"+
                "  a.GrpPolNo,"+
                " a.ContNo,"+
                " a.PolNo,"+
                " b.EndorsementNo,"+
                " a.MainPolNo,"+
                " a.MasterPolNo,"+
                " a.RiskCode,"+
                " a.RiskVersion,"+
                " a.PayToDate,"+
                " b.FeeOperationType,"+
                " a.Amnt,"+
                " a.ManageCom,"+
                " a.ReNewCount,"+
                " a.agentGroup,"+
                " a.CValidate,"+
                " a.enddate" + //团单协议退保
                " with ur ";

        int start = 1;
        int nCount = 5000;
        while (true) {
            SSRS tSSRS = new ExeSQL().execSQL(tSql, start, nCount);
            if (tSSRS.getMaxRow() <= 0) {
                break;
            }
            mMap = new MMap();
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                	String esql="select 1 from lrpoledor where " +
                			"recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
                					"and polno='"+tSSRS.GetText(i, 4)+"' and edorno='"+tSSRS.GetText(i, 5)+"' with ur ";
                	 SSRS teSSRS = new ExeSQL().execSQL(esql);
            		 if(teSSRS!=null && teSSRS.MaxRow>0){
            			 continue;
                    }
                    LRPolEdorSchema tLRPolEdorSchema = new LRPolEdorSchema();
                    //添加其他数据
                    tLRPolEdorSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolEdorSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolEdorSchema.setContNo(tSSRS.GetText(i, 3));
                    tLRPolEdorSchema.setPolNo(tSSRS.GetText(i, 4));
                    tLRPolEdorSchema.setEdorNo(tSSRS.GetText(i, 5));
                    tLRPolEdorSchema.setMainPolNo(tSSRS.GetText(i, 6));
                    tLRPolEdorSchema.setMasterPolNo(tSSRS.GetText(i, 7));
                    tLRPolEdorSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolEdorSchema.setRiskVersion(tSSRS.GetText(i, 9));
                    tLRPolEdorSchema.setEdorValidate(tSSRS.GetText(i, 10));
                    int noPassDays ; //未经过天数
                    String mSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
                    String LRiskFlag = new ExeSQL().getOneValue(mSql);
                    if(Integer.parseInt(LRiskFlag)>0){//长险，未经过天数＝交至日期－保全生效日
                         noPassDays =PubFun.calInterval(tSSRS.GetText(i,10),tSSRS.GetText(i,11),"D")%365;
                    }else{//短险，未经过天数＝终止日期－保全生效日
                         noPassDays =PubFun.calInterval(tSSRS.GetText(i,10),tSSRS.GetText(i,20),"D");
                    }
                    if (noPassDays <= 0) {
                        noPassDays = 0; //保全摊回的时候，失效日期可以大于缴至日期,此时摊回零.
                    }

                    tLRPolEdorSchema.setNoPassDays(noPassDays);
                    tLRPolEdorSchema.setFeeoperationType(tSSRS.GetText(i, 12));
                    tLRPolEdorSchema.setGetMoney(tSSRS.GetText(i, 13));
                    tLRPolEdorSchema.setGetConfDate(tSSRS.GetText(i, 14));
                    tLRPolEdorSchema.setSumBackAmnt(tSSRS.GetText(i, 15));
                    tLRPolEdorSchema.setManageCom(tSSRS.GetText(i, 16));
                    tLRPolEdorSchema.setGetDataDate(this.mToday); //数据提取日期
                    tLRPolEdorSchema.setOperator("Server");
                    tLRPolEdorSchema.setMakeDate(this.mCurrentDate);
                    tLRPolEdorSchema.setMakeTime(this.mCurrentTime);
                    tLRPolEdorSchema.setModifyDate(this.mCurrentDate);
                    tLRPolEdorSchema.setModifyTime(this.mCurrentTime);
//                    String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
//                    String LRiskFlag = new ExeSQL().getOneValue(aSql);
                    String tcvdatey=tSSRS.GetText(i, 19).substring(0,4);
                	String ttodatey=this.mToday.substring(0,4);
                	int tyear=Integer.parseInt(ttodatey)-Integer.parseInt(tcvdatey)+1;
                    if(Integer.parseInt(LRiskFlag)>0){
                    	
                        tLRPolEdorSchema.setReNewCount(tyear); //保单年度
                    }else{
                        tLRPolEdorSchema.setReNewCount(tSSRS.GetText(i, 17)); //续保次数
                    }
                     //新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"+
                                 tSSRS.GetText(i,18)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolEdorSchema.setCostCenter(CostCenter);
                    //String PolYear = tSSRS.GetText(i,19);//保单年度,长险lrpol表的renewcount存的是保单年度,短险存续保数
                    String ttSql =  "select a.ReContCode,'C',a.RiskCalSort,  (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode)  comcode " +
                            "from LRPol a,LBPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount "+
                            "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod != 'L') and a.PolNo = '" +
                            tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount = b.ReNewCount) "+
                            "  and a.recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
                            " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                            " union all "+
                            "select a.ReContCode,'C',a.RiskCalSort,(select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode)  comcode " +
                            "from LRPol a,LBPol b where a.PolNo = b.PolNO "+
                            " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.PolNo = '" +
                            tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO ) "+
                            //此处是为了兼容旧系统的分保，长险时lrpol表的renewcount存的是0
                            " and a.recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
                            " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode  with ur";
                    if(tSSRS.GetText(i, 12).equals("WZ")||tSSRS.GetText(i, 12).equals("WJ")||tSSRS.GetText(i, 12).equals("BB")||tSSRS.GetText(i, 12).equals("BJ")){
                    	ttSql =  "select a.ReContCode,'C',a.RiskCalSort, (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode)  comcode " +
                        "from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount "+
                        "and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod != 'L') and a.PolNo = '" +
                        tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount = b.ReNewCount) "+
                        " and a.recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
                        " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                        "union all "+
                        "select a.ReContCode,'C',a.RiskCalSort,(select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode)  comcode " +
                        "from LRPol a,LCPol b where a.PolNo = b.PolNO "+
                        " and exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.PolNo = '" +
                        tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO ) "+
                        //此处是为了兼容旧系统的分保，长险时lrpol表的renewcount存的是0
                        " and a.recontcode='"+tLRTempCessContSchema.getTempContCode()+"' " +
                        " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                        
                        " with ur";
                    }
                    
                    SSRS cSSRS = new ExeSQL().execSQL(ttSql);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                        tLRPolEdorSchema.setReContCode(cSSRS.GetText(j, 1));
                        if(!cSSRS.GetText(j, 2).equals("C")){  //当取出来的是反冲数据时，跳过不提取。
                            continue;
                        }
                        tLRPolEdorSchema.setReinsureItem(cSSRS.GetText(j, 2));
                        tLRPolEdorSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolEdorSchema.setReComCode(cSSRS.GetText(j, 4));
                        mMap.put(tLRPolEdorSchema.getSchema(), SysConst.DELETE_AND_INSERT);
                    }
                }
            }
            if (!prepareOutputData()) {
            }
            //如果提交不成功,不能返回
            tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(this.mInputData, "")) {
                if (tPubSubmit.mErrors.needDealError()) {
                    buildError("insertData", "提取再保保全数据出错!");
                }
            }
            start += nCount;
        }
        return true;
    }

   

    /**
     * 提取保全数据
     * @return boolean
     */
    public boolean getEdorData(LRTempCessContSchema tLRTempCessContSchema,Date dbdate) {
        

        FDate chgdate = new FDate();
        
        

       
            this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!insertEdorData(tLRTempCessContSchema)) {
                buildError("getEdorData", "提取" + mToday + "号保全数据出错");
                return false;
            }
          
            
        
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }



    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(String mToDay) {
       
        mStartDate = mToDay;
        
        return true;
    }
    public String getResult() {
        return "没有传数据";
    }
    
    

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
//        cError.moduleName = "LRGetEdorDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "86";
      

        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRTempGetEdorDataBL tLRNewGetEdorDataBL = new LRTempGetEdorDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2008-9-26");
            tTransferData.setNameAndValue("EndDate", "2008-9-30");
            System.out.println("in main: " +
                               (String) tTransferData.
                               getValueByName("StartDate") + "   " +
                               (String) tTransferData.getValueByName("EndDate"));
            vData.add(tTransferData);
            LRTempCessContDB LRTempCessContS =new LRTempCessContDB();
            LRTempCessContS.setContno("00008506000022");
            LRTempCessContSchema tLRTempCessContSchema =new LRTempCessContSchema();
            tLRTempCessContSchema.setSchema(LRTempCessContS.query().get(1));
            tLRNewGetEdorDataBL.getEdorData(tLRTempCessContSchema,new FDate().getDate("2013-8-6"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
