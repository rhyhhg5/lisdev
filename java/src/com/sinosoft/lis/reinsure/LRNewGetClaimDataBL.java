package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;

import java.util.Date;

/*
 * <p>ClassName: LRNewGetClaimDataBL </p>
 * <p>Description: LRNewGetClaimDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2008-11-27
 */
public class LRNewGetClaimDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private String mReContCode ="";
    private TransferData mGetCessData = new TransferData();
    
    /**添加时间2014-7-11 修改人：杨阳，目地：为了比较出险日期和合同有效期间*/
    private FDate mFDate = new FDate();
    
    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    private PubFun mPubFun = new PubFun();
    private String mCurrentDate = new PubFun().getCurrentDate();

    private String mCurrentTime = new PubFun().getCurrentTime();
    
    private	String mRiskCode;
    private	String mGrpContNo;
    private	String mContNo;
    private	String mPolNo;
    private	String mOperator;
    private String mSQL;
    //业务处理相关变量
    /** 全局数据 */

    public LRNewGetClaimDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL->Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取理赔数据
        if (!getClaimData()) {
            // @@错误处理
            buildError("insertData", "理赔摊回提数出现错误!");
            return false;
        }
        return true;
    }

    /**
     * 提取理赔数据，以结算日期为准
     * @return boolean
     */
    private boolean insertClaimData() {
        //删除该提数日期范围内的记录
        System.out.println("Come to InsertClaimData ...");
        //得到当天给付的理赔案件,提数原则 理赔赔付日期 = 提数日期
        //09/05/19修改,提数日期=ljaget的记录生成日期（makedate）理赔状态：通知给付、结案   ---liuli
        
        String tSql ="select a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
        "(select min(a1.AccDate) from llsubreport a1,llcaserela a2  where a1.SubRptNo = a2.SubRptNo and a2.CaseNo=c.CaseNo) 出险日期, " +
        "(select lp.ClmState from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),sum(b.StandPay)," +
        "(select sum(pay) from ljagetclaim where polno=d.polno and otherno=d.otherno and makedate=d.makedate and othernotype='5') 赔付金额, " +
        "(select lp.ClmUwer from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),c.EndCaseDate 结案日期,c.RgtDate 索赔日期,d.ConfDate 赔付日期, " +
        "(select sum(ClaimMoney) from LLClaimDetail where caseno=c.caseno and polno = a.PolNO and ClmNo = b.ClmNo) 索赔额,a.UWFlag, " +
        "(select min(a2.accdesc) from llcaserela a1,llsubreport a2 where a1.SubRptNo=a2.SubRptNo and a1.caseNo=c.CaseNo) 出险原因, " +
        "(select max(HospEndDate) from llfeemain where caseno=c.caseno) 出院日期, " +
        "(select DiseaseName from LLCaseCure where caseno=c.caseno order by serialno desc fetch first row only) 诊断," +
        "a.ManageCom ,a.agentGroup,a.masterPolno," +
        "(select distinct givetypedesc from llclaim where caseno=c.caseno  and ClmNo = b.ClmNo) 理赔结论,a.cvalidate  " +
        "from LCPol a, llclaimdetail b, llcase c, ljagetclaim d " +
        "where d.otherno not like 'P94%' and exists (select 1 from lrpol where polno=a.polno and (recontcode ='"+mReContCode+"'  or 1 ='"+mReContCode+"') union all select 1 from lrpol where polno = a.masterPolno and  (recontcode ='"+mReContCode+"'  or 1 ='"+mReContCode+"')) and " +
        "  not exists (select 1  from lrtempcesscont w,lrtempcesscontinfo t where w.tempcontcode=t.tempcontcode and w.contno=d.grpcontno  and t.riskcode=d.riskcode and t.contplancode=a.contplancode) and "+
        "a.PolNo = b.PolNo and b.CaseNo = c.CaseNo and a.polno=d.polno  " +
        "and a.Appflag = '1' and (c.caseno=d.otherno or c.rgtno=d.otherno) and d.othernotype='5' and d.MakeDate = '" +
        this.mToday +"'"+mSQL+
        " group by a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
        "c.EndCaseDate,c.RgtDate,d.ConfDate,a.UWFlag,a.ManageCom,a.agentGroup,a.masterPolno,a.cvalidate,d.makedate,d.otherno,d.polno " +
        " union " +
        "select a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
        "(select min(a1.AccDate) from llsubreport a1,llcaserela a2  where a1.SubRptNo = a2.SubRptNo and a2.CaseNo=c.CaseNo) 出险日期, " +
        "(select lp.ClmState from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),sum(b.StandPay)," +
        "(select sum(pay) from ljagetclaim where polno=d.polno and otherno=d.otherno and makedate=d.makedate and othernotype='5') 赔付金额, " +
        "(select lp.ClmUwer from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),c.EndCaseDate 结案日期,c.RgtDate 索赔日期,d.ConfDate 赔付日期, " +
        "(select sum(ClaimMoney) from LLClaimDetail where caseno=c.caseno and polno = a.PolNO and ClmNo = b.ClmNo) 索赔额,a.UWFlag, " +
        "(select min(a2.accdesc) from llcaserela a1,llsubreport a2 where a1.SubRptNo=a2.SubRptNo and a1.caseNo=c.CaseNo) 出险原因, " +
        "(select max(HospEndDate) from llfeemain where caseno=c.caseno) 出院日期, " +
        "(select DiseaseName from LLCaseCure where caseno=c.caseno order by serialno desc fetch first row only) 诊断," +
        "a.ManageCom ,a.agentGroup,a.masterPolno ," +
        "(select distinct givetypedesc from llclaim where caseno=c.caseno  and ClmNo = b.ClmNo) 理赔结论,a.cvalidate  " +
        "from LBPol a, llclaimdetail b, llcase c, ljagetclaim d " +
        "where d.otherno not like 'P94%' and exists (select 1 from lrpol where polno=a.polno and (recontcode ='"+mReContCode+"'  or 1 ='"+mReContCode+"') union all select 1 from lrpol where polno = a.masterPolno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"') ) and " +
        "  not exists (select 1 from lrpol where polno=a.polno and TempCessFlag='Y' union all select 1 from lrpol where polno = a.masterPolno and TempCessFlag='Y' ) and "+
        "a.PolNo = b.PolNo and b.CaseNo = c.CaseNo and a.polno=d.polno " +
        "and a.Appflag = '1' and (c.caseno=d.otherno or c.rgtno=d.otherno) and d.othernotype='5' and d.MakeDate = '" +
        this.mToday +"'"+mSQL+
        " group by a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
        "c.EndCaseDate,c.RgtDate,d.ConfDate,a.UWFlag,a.ManageCom,a.agentGroup,a.masterPolno,a.cvalidate,d.makedate,d.otherno,d.polno " +
        " with ur ";
        SSRS tSSRS = new SSRS();
    	RSWrapper rsWrapper = new RSWrapper();
    	if (!rsWrapper.prepareData(null, tSql))
        {
            System.out.println("数据准备失败! ");
            return false;
        }
        do {
        	
        	tSSRS = rsWrapper.getSSRS();
         	
 			
            
            
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            	for(int i=1;i<=tSSRS.MaxRow;i++){ 
            		mMap = new MMap();
                    LRPolClmSchema tLRPolClmSchema = new LRPolClmSchema();
                    tLRPolClmSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolClmSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolClmSchema.setContNo(tSSRS.GetText(i, 3));
                    tLRPolClmSchema.setPolNo(tSSRS.GetText(i, 4)); //保单号
                    tLRPolClmSchema.setClmNo(tSSRS.GetText(i, 5));
                    tLRPolClmSchema.setRgtNo(tSSRS.GetText(i, 6));
                    tLRPolClmSchema.setCaseNo(tSSRS.GetText(i, 7));
                    tLRPolClmSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolClmSchema.setRiskVer(tSSRS.GetText(i, 9));
                    tLRPolClmSchema.setAccidentDate(tSSRS.GetText(i, 10)); //出险日期
                    tLRPolClmSchema.setClmState(tSSRS.GetText(i, 11));
                    tLRPolClmSchema.setStandPay(tSSRS.GetText(i, 12));
                    tLRPolClmSchema.setRealPay(tSSRS.GetText(i, 13)); //赔付金额
                    tLRPolClmSchema.setClmUWer(tSSRS.GetText(i, 14));
                    tLRPolClmSchema.setEndCaseDate(tSSRS.GetText(i, 15)); //结案日期
                    tLRPolClmSchema.setRgtDate(tSSRS.GetText(i, 16)); //索赔日期
                    tLRPolClmSchema.setConfDate(tSSRS.GetText(i, 17)); //赔付日期
                    tLRPolClmSchema.setClaimMoney(tSSRS.GetText(i, 18)); //索赔额
                    tLRPolClmSchema.setUWFlag(tSSRS.GetText(i, 19));
                    tLRPolClmSchema.setAccDesc(tSSRS.GetText(i, 20)); //出险原因
                    tLRPolClmSchema.setLeaveHospDate(tSSRS.GetText(i, 21)); //出院日期
                    tLRPolClmSchema.setDiagnoses(tSSRS.GetText(i, 22)); //诊断
//                    if(tLRPolClmSchema.getRealPay()>=0){
                        tLRPolClmSchema.setGiveTypeDesc(tSSRS.GetText(i, 26)); //理赔结论
//                    }else{
//                        tLRPolClmSchema.setGiveTypeDesc("纠错"); //理赔结论
//                    }
                    tLRPolClmSchema.setManageCom(tSSRS.GetText(i, 23)); //保单管理机
                    //080714新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                                 +tSSRS.GetText(i,24)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolClmSchema.setCostCenter(CostCenter);

                    tLRPolClmSchema.setGetDataDate(this.mToday);
                    tLRPolClmSchema.setOperator(strOperate);
                    tLRPolClmSchema.setMakeDate(this.mCurrentDate);
                    tLRPolClmSchema.setMakeTime(this.mCurrentTime);
                    tLRPolClmSchema.setModifyDate(this.mCurrentDate);
                    tLRPolClmSchema.setModifyTime(this.mCurrentTime);
                    String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
                    String LRiskFlag = new ExeSQL().getOneValue(aSql);
                    String reNewCount = "0";
                    if(Integer.parseInt(LRiskFlag)>0){//长险获取保单年度
                        int PolYear = PubFun.calInterval3(tSSRS.GetText(i, 27),tSSRS.GetText(i, 10),"Y");//
                        tLRPolClmSchema.setReNewCount(PolYear); //保单年度
                    }else{
                        if (tSSRS.GetText(i, 10)!=null&&!tSSRS.GetText(i, 10).equals("")) {//短险获取保单续保次数
                            //下面计算根据出险日期计算出险时的续保次数
                            String tSql1 = " select RenewCount from LCPol where PolNo = '" +
                                   tSSRS.GetText(i, 4) + "' and '" +
                                   tSSRS.GetText(i, 10) +
                                   "' between CValidate and EndDate - 1 Day " +
                                   " union " +
                                   " select RenewCount from LBPol where PolNo = '" +
                                   tSSRS.GetText(i, 4) + "' and '" +
                                   tSSRS.GetText(i, 10) +
                                   "' between CValidate and EndDate - 1 Day" +
                                   " union " +
                                   " select RenewCount from LBPol where PolNo in (select NewPolNo from LCRNewStateLog where PolNo = '" +
                                   tSSRS.GetText(i, 4) + "') and '" +
                                   tSSRS.GetText(i, 10) +
                                   "' between CValidate and EndDate - 1 Day with ur";
                            reNewCount = new ExeSQL().getOneValue(tSql1); //这里主要是考虑到出险日期可能不在保单生效期间 by huxl @070724
                            if (reNewCount==null||reNewCount.equals("")) {
                                reNewCount = "0";
                            }
                        }
                        tLRPolClmSchema.setReNewCount(reNewCount); //续保次数
                    }
                    String masterPolno = tSSRS.GetText(i,25);
                    String tSql2 = "select a.ReContCode,'C',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                    "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode, " +
                    "(select ReType from lRContInfo where ReContCode=a.ReContCode) from LRPol a "+
                    "where (a.PolNo = '" +
                    tSSRS.GetText(i, 4) +"' or a.polno='"+masterPolno+"') and RenewCount = " +tLRPolClmSchema.getReNewCount()+
                    " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01')" +
                    " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                    "union all "+
                    "select a.ReContCode,'C',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                    "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode, " +
                    "(select ReType from lRContInfo where ReContCode=a.ReContCode) from LRPol a "+
                    "where exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and (a.PolNo = '" +
                    tSSRS.GetText(i, 4) +"' or a.polno='"+masterPolno+"') and RenewCount =0  and a.getdatadate<='2009-9-1' " +
                    		" and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01')" +
                    		" group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode" +
                    		" with ur"; 
                    SSRS cSSRS = new ExeSQL().execSQL(tSql2);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                    	String tRecontCode = cSSRS.GetText(j, 1);
                    	//20141120   杨阳
                    	//如果前台合同号不为空，需要匹配此合同才插入，否则跳过
                    	if(!"1".equals(mReContCode))
                    	{
                    		if(!tRecontCode.equals(mReContCode))
                    		{
                    			continue;
                    		}
                    	}
                        tLRPolClmSchema.setReContCode(tRecontCode); //合同名称
                        if(cSSRS.GetText(j, 2).equals("F")){  //当取出来的是反冲数据时，跳过不提取。
                            continue;
                        }
                        /* 修改日期 2014-7-11  修改人：杨阳
                         * 原因：两个合同SII2012A01,WIL2012A01终止日期从2042-1-1改为2014-5-31
                         * 由于这两个合同对应险种530501,531201属于特殊险种，分保方式为月份，即每月1号进行分出，每次只分出一个月
						 * 故理赔摊回时，出险日期在合同的有效期间内才理赔摊回，否则不摊回
                        **/
                        String recontCode = tLRPolClmSchema.getReContCode();
                        if("SII2012A01".equals(recontCode)||"WIL2012A01".equals(recontCode))
                		{
                        	//出险日期
                        	Date accidentDate = mFDate.getDate(tLRPolClmSchema.getAccidentDate());
                        	//合同生效日期  写固定值是因为考虑到这两个合同的生效日期与终止日期一样,可以少查数据库
                        	Date cvalidate = mFDate.getDate("2012-6-1");
                        	//合同终止日期
                        	Date enddate = mFDate.getDate("2014-5-31");
                        	//如果出险日期小于合同生效日期或大于合同终止日期，这样的数据不处理
                        	if(accidentDate.compareTo(cvalidate)<0||enddate.compareTo(accidentDate)<0)
                        	{
                        		continue;
                        	}
                        	
                		}
                        tLRPolClmSchema.setReinsureItem(cSSRS.GetText(j, 2));
                        tLRPolClmSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolClmSchema.setReComCode(cSSRS.GetText(j, 4));
                        if(cSSRS.GetText(j,5).equals("02")){  //如果是按责任分保，则提取该责任的理赔赔付金额
                          String sqlduty = "select distinct  riskcode from lrcalfactorvalue b where b.recontcode='"+tLRPolClmSchema.getReContCode()+"' with ur";
                          String getdutycode = new ExeSQL().getOneValue(sqlduty);  //查询分保给付责任号
                          if(getdutycode!=null){
                             sqlduty = "select  sum(standPay),sum(ClaimMoney),sum(realpay) from LLClaimDetail where caseno='"+tLRPolClmSchema.getCaseNo()+
                                           "' and getdutycode='"+getdutycode+"' and polno='"+tLRPolClmSchema.getPolNo()+"'with ur";
                              SSRS dSSRS = new ExeSQL().execSQL(sqlduty);
                              if (dSSRS.getMaxRow() <= 0) {
                          		continue;
                              }
                              if ((dSSRS.GetText(1, 1) == null||dSSRS.GetText(1, 1).equals("")||dSSRS.GetText(1, 1).equals("null"))&& 
                            		  (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 2).equals("null"))&&
                            		  (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 1).equals("null"))) {
                            	  continue;
                              }
                              if (dSSRS.GetText(1, 1) == null||dSSRS.GetText(1, 1).equals("")||dSSRS.GetText(1, 1).equals("null"))
                            	  tLRPolClmSchema.setStandPay(0);
                              else
                            	  tLRPolClmSchema.setStandPay(dSSRS.GetText(1,1));
                              if (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 2).equals("null"))
                            	  tLRPolClmSchema.setClaimMoney(0); //责任索赔额
                              else
                            	  tLRPolClmSchema.setClaimMoney(dSSRS.GetText(1,2)); //责任索赔额
                              if (dSSRS.GetText(1, 3) == null||dSSRS.GetText(1, 3).equals("")||dSSRS.GetText(1, 3).equals("null"))
                            	  tLRPolClmSchema.setRealPay(0);
                              else
                            	  tLRPolClmSchema.setRealPay(dSSRS.GetText(1,3)); //责任实赔付
                              tLRPolClmSchema.setGetDutyCode(getdutycode);
                          }else{ //是按责任分保但是计算要素中查询不到分保给付责任号，则置理赔费用为0
                               tLRPolClmSchema.setStandPay(0);
                               tLRPolClmSchema.setClaimMoney(0);
                               tLRPolClmSchema.setRealPay(0);
                               tLRPolClmSchema.setGetDutyCode("aa"); //按责任分保但是没有查到责任
                          }
                        }
                        mMap.put(tLRPolClmSchema.getSchema(), "DELETE&INSERT");
                    }
                    if (!prepareOutputData()) {
                    }
                    //如果提交不成功,不能返回
                    tPubSubmit = new PubSubmit();
                    if (!tPubSubmit.submitData(this.mInputData, "")) {
                        if (tPubSubmit.mErrors.needDealError()) {
                            // @@错误处理
                            buildError("insertData", "保存临分协议信息时出现错误!");
                        }
                    }
            	  }
                
                //提取每天的合同再保数据，作为一个事务
                
            }
            
 			
           
        }while (tSSRS != null && tSSRS.MaxRow > 0);
				rsWrapper.close();
        return true;
    }

    /**
     * 提取理赔数据，以结算日期为准
     * @return boolean
     */
    private boolean insertTBData() {
        //提取提数日期范围内的退保金记录
        System.out.println("Come to InsertTBData ...");
        
        String tSql ="select a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,aa.edorno,aa.edorno,aa.accountcode,a.RiskCode,a.RiskVersion, "+
        		"nvl((select edorvalidate from lpedoritem where edorno=aa.edorno fetch first 1 row only),(select edorvalidate from lpgrpedoritem where edorno=aa.edorno fetch first 1 row only)) 出险日期,d.edorstate,-aa.summoney,-aa.summoney 赔付金额,d.operator, "+
        		"d.confdate 结案日期,d.confdate 索赔日期,d.ConfDate 赔付日期,-aa.summoney 索赔额,d.UWstate,'退保金' 出险原因, "+
        		"'' 出院日期,'退保金' 诊断,a.ManageCom ,a.agentGroup,a.masterPolno,'退保金' 理赔结论,a.cvalidate   "+
        		"from  "+
        		"( select  b.polno polno,b.indexno indexno,a.accountcode accountcode,(select otherno from ljaget where actugetno=b.indexno union select incomeno from ljapay where payno=b.indexno ) edorno, "+
        		"sum (case a.finitemtype when 'C' then a.summoney else -a.summoney end) summoney "+
        		"from fivoucherdatadetail a,fiabstandarddata b  "+
        		"where a.serialno = b.serialno "+
        		"	and a.accountdate = '"+this.mToday+"' "+
        		"	and a.vouchertype in('X4','Y4') "+
        		"	and a.accountcode in ('6531000000','2611020701','2611020702') "+
        		"group by b.polno,b.indexno,a.accountcode) as aa "+
        		"inner join lbpol a on aa.polno=a.polno "+
        		"left join lpedorapp d on d.edoracceptno=aa.edorno "+
        		"where 1=1 "+mSQL+
        		" and exists (select 1 from lrpol where polno=a.polno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"')  union all select 1 from lrpol where polno = a.masterPolno and (recontcode ='"+mReContCode+"' or 1 ='"+mReContCode+"') ) "+
        		"and not exists (select 1  from lrtempcesscont w,lrtempcesscontinfo t where w.tempcontcode=t.tempcontcode and w.contno=a.grpcontno  and t.riskcode=a.riskcode and t.contplancode=a.contplancode) "+
        		"and a.appflag='1' "+
        		"and a.riskcode in (select distinct riskcode from lrcalfactorvalue where factorcode ='TBLmt') "+
        		"with ur ";
        
        SSRS tSSRS = new SSRS();
    	RSWrapper rsWrapper = new RSWrapper();
    	if (!rsWrapper.prepareData(null, tSql))
        {
            System.out.println("数据准备失败! ");
            return false;
        }
        do {
        	
        	tSSRS = rsWrapper.getSSRS();
         	
 			
            
            
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            	for(int i=1;i<=tSSRS.MaxRow;i++){ 
            		mMap = new MMap();
                    LRPolClmSchema tLRPolClmSchema = new LRPolClmSchema();
                    tLRPolClmSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolClmSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolClmSchema.setContNo(tSSRS.GetText(i, 3));
                    tLRPolClmSchema.setPolNo(tSSRS.GetText(i, 4)); //保单号
                    tLRPolClmSchema.setClmNo(tSSRS.GetText(i, 5));
                    tLRPolClmSchema.setRgtNo(tSSRS.GetText(i, 6));
                    tLRPolClmSchema.setCaseNo(tSSRS.GetText(i, 7));
                    tLRPolClmSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolClmSchema.setRiskVer(tSSRS.GetText(i, 9));
                    tLRPolClmSchema.setAccidentDate(tSSRS.GetText(i, 10)); //出险日期
                    tLRPolClmSchema.setClmState(tSSRS.GetText(i, 11));
                    tLRPolClmSchema.setStandPay(tSSRS.GetText(i, 12));
                    tLRPolClmSchema.setRealPay(tSSRS.GetText(i, 13)); //赔付金额
                    tLRPolClmSchema.setClmUWer(tSSRS.GetText(i, 14));
                    tLRPolClmSchema.setEndCaseDate(tSSRS.GetText(i, 15)); //结案日期
                    tLRPolClmSchema.setRgtDate(tSSRS.GetText(i, 16)); //索赔日期
                    tLRPolClmSchema.setConfDate(tSSRS.GetText(i, 17)); //赔付日期
                    tLRPolClmSchema.setClaimMoney(tSSRS.GetText(i, 18)); //索赔额
                    tLRPolClmSchema.setUWFlag(tSSRS.GetText(i, 19));
                    tLRPolClmSchema.setAccDesc(tSSRS.GetText(i, 20)); //出险原因
                    tLRPolClmSchema.setLeaveHospDate(tSSRS.GetText(i, 21)); //出院日期
                    tLRPolClmSchema.setDiagnoses(tSSRS.GetText(i, 22)); //诊断
//                    if(tLRPolClmSchema.getRealPay()>=0){
                        tLRPolClmSchema.setGiveTypeDesc(tSSRS.GetText(i, 26)); //理赔结论
//                    }else{
//                        tLRPolClmSchema.setGiveTypeDesc("纠错"); //理赔结论
//                    }
                    tLRPolClmSchema.setManageCom(tSSRS.GetText(i, 23)); //保单管理机
                    //080714新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                                 +tSSRS.GetText(i,24)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolClmSchema.setCostCenter(CostCenter);

                    tLRPolClmSchema.setGetDataDate(this.mToday);
                    tLRPolClmSchema.setOperator("Server");
                    tLRPolClmSchema.setMakeDate(this.mCurrentDate);
                    tLRPolClmSchema.setMakeTime(this.mCurrentTime);
                    tLRPolClmSchema.setModifyDate(this.mCurrentDate);
                    tLRPolClmSchema.setModifyTime(this.mCurrentTime);
                    String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
                    String LRiskFlag = new ExeSQL().getOneValue(aSql);
                    String reNewCount = "0";
                    if(Integer.parseInt(LRiskFlag)>0){//长险获取保单年度
                        int PolYear = PubFun.calInterval3(tSSRS.GetText(i, 27),tSSRS.GetText(i, 10),"Y");//
                        tLRPolClmSchema.setReNewCount(PolYear); //保单年度
                    }else{
                        if (tSSRS.GetText(i, 10)!=null&&!tSSRS.GetText(i, 10).equals("")) {//短险获取保单续保次数
                            //下面计算根据出险日期计算出险时的续保次数
                            String tSql1 = " select RenewCount from LCPol where PolNo = '" +
                                   tSSRS.GetText(i, 4) + "' and '" +
                                   tSSRS.GetText(i, 10) +
                                   "' between CValidate and EndDate - 1 Day " +
                                   " union " +
                                   " select RenewCount from LBPol where PolNo = '" +
                                   tSSRS.GetText(i, 4) + "' and '" +
                                   tSSRS.GetText(i, 10) +
                                   "' between CValidate and EndDate - 1 Day" +
                                   " union " +
                                   " select RenewCount from LBPol where PolNo in (select NewPolNo from LCRNewStateLog where PolNo = '" +
                                   tSSRS.GetText(i, 4) + "') and '" +
                                   tSSRS.GetText(i, 10) +
                                   "' between CValidate and EndDate - 1 Day with ur";
                            reNewCount = new ExeSQL().getOneValue(tSql1); //这里主要是考虑到出险日期可能不在保单生效期间 by huxl @070724
                            if (reNewCount==null||reNewCount.equals("")) {
                                reNewCount = "0";
                            }
                        }
                        tLRPolClmSchema.setReNewCount(reNewCount); //续保次数
                    }
                    String masterPolno = tSSRS.GetText(i,25);
                    String tSql2 = "select a.ReContCode,'T',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                    "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode, " +
                    "(select ReType from lRContInfo where ReContCode=a.ReContCode),max(renewcount) from LRPol a "+
                    "where (a.PolNo = '" +
                    tSSRS.GetText(i, 4) +"' or a.polno='"+masterPolno+"') " +
                    " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01')" +
                    " and exists (select 1 from lrcalfactorvalue where recontcode=a.recontcode and factorcode ='TBLmt')" +
                    " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                    		" with ur"; 
                    SSRS cSSRS = new ExeSQL().execSQL(tSql2);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                    	String tRecontCode = cSSRS.GetText(j, 1);
                    	//20141120   杨阳
                    	//如果前台合同号不为空，需要匹配此合同才插入，否则跳过
                    	if(!"1".equals(mReContCode))
                    	{
                    		if(!tRecontCode.equals(mReContCode))
                    		{
                    			continue;
                    		}
                    	}
                        tLRPolClmSchema.setReContCode(tRecontCode); //合同名称
                        if(cSSRS.GetText(j, 2).equals("F")){  //当取出来的是反冲数据时，跳过不提取。
                            continue;
                        }
                        tLRPolClmSchema.setReinsureItem(cSSRS.GetText(j, 2));
                        tLRPolClmSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolClmSchema.setReComCode(cSSRS.GetText(j, 4));
                        tLRPolClmSchema.setReNewCount(cSSRS.GetText(j, 6));
                        if(cSSRS.GetText(j,5).equals("02")){  //如果是按责任分保，则提取该责任的理赔赔付金额
                          String sqlduty = "select distinct  riskcode from lrcalfactorvalue b where b.recontcode='"+tLRPolClmSchema.getReContCode()+"' with ur";
                          String getdutycode = new ExeSQL().getOneValue(sqlduty);  //查询分保给付责任号
                          if(getdutycode!=null){
                             sqlduty = "select  sum(standPay),sum(ClaimMoney),sum(realpay) from LLClaimDetail where caseno='"+tLRPolClmSchema.getCaseNo()+
                                           "' and getdutycode='"+getdutycode+"' and polno='"+tLRPolClmSchema.getPolNo()+"'with ur";
                              SSRS dSSRS = new ExeSQL().execSQL(sqlduty);
                              if (dSSRS.getMaxRow() <= 0) {
                          		continue;
                              }
                              if ((dSSRS.GetText(1, 1) == null||dSSRS.GetText(1, 1).equals("")||dSSRS.GetText(1, 1).equals("null"))&& 
                            		  (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 2).equals("null"))&&
                            		  (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 1).equals("null"))) {
                            	  continue;
                              }
                              if (dSSRS.GetText(1, 1) == null||dSSRS.GetText(1, 1).equals("")||dSSRS.GetText(1, 1).equals("null"))
                            	  tLRPolClmSchema.setStandPay(0);
                              else
                            	  tLRPolClmSchema.setStandPay(dSSRS.GetText(1,1));
                              if (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 2).equals("null"))
                            	  tLRPolClmSchema.setClaimMoney(0); //责任索赔额
                              else
                            	  tLRPolClmSchema.setClaimMoney(dSSRS.GetText(1,2)); //责任索赔额
                              if (dSSRS.GetText(1, 3) == null||dSSRS.GetText(1, 3).equals("")||dSSRS.GetText(1, 3).equals("null"))
                            	  tLRPolClmSchema.setRealPay(0);
                              else
                            	  tLRPolClmSchema.setRealPay(dSSRS.GetText(1,3)); //责任实赔付
                              tLRPolClmSchema.setGetDutyCode(getdutycode);
                          }else{ //是按责任分保但是计算要素中查询不到分保给付责任号，则置理赔费用为0
                               tLRPolClmSchema.setStandPay(0);
                               tLRPolClmSchema.setClaimMoney(0);
                               tLRPolClmSchema.setRealPay(0);
                               tLRPolClmSchema.setGetDutyCode("aa"); //按责任分保但是没有查到责任
                          }
                        }
                        mMap.put(tLRPolClmSchema.getSchema(), "DELETE&INSERT");
                    }
                    if (!prepareOutputData()) {
                    }
                    //如果提交不成功,不能返回
                    tPubSubmit = new PubSubmit();
                    if (!tPubSubmit.submitData(this.mInputData, "")) {
                        if (tPubSubmit.mErrors.needDealError()) {
                            // @@错误处理
                            buildError("insertData", "保存临分协议信息时出现错误!");
                        }
                    }
            	  }
                
                //提取每天的合同再保数据，作为一个事务
                
            }
            
 			
           
        }while (tSSRS != null && tSSRS.MaxRow > 0);
				rsWrapper.close();
        return true;
    }
    
    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolClmSchema aLRPolClmSchema,
                             String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new
                                             LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolClmSchema.getPolNo());
        tLRErrorLogSchema.setLogType("4");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }

    /**
     * 提取理赔数据
     *
     * @return boolean
     */
    private boolean getClaimData() {
        
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(mEndDate); //录入的终止日期，用FDate处理

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!insertClaimData()) {
                buildError("getClaimData", "提取" + mToday + "号理赔数据出错!");
                return false;
            }
            if (!insertTBData()) {
                buildError("getClaimData", "提取" + mToday + "号退保金数据出错!");
                return false;
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");
        mReContCode = (String) mGetCessData.getValueByName("ReContCode");
        if("".equals(mReContCode)||null == mReContCode)
        {
        	mReContCode = "1";
        }
        if("filldata".equals(strOperate))
        {
        	 mRiskCode = (String) mGetCessData.getValueByName("RiskCode");
             mGrpContNo = (String) mGetCessData.getValueByName("GrpContNo");
             mContNo = (String) mGetCessData.getValueByName("ContNo");
             mPolNo = (String) mGetCessData.getValueByName("PolNo");
             mOperator = (String) mGetCessData.getValueByName("Operator");
             strOperate ="fd"+PubFun.getCurrentDate2();
             mSQL ="";
             if(!"".equals(mGrpContNo))
         	{
             	mSQL +=" and a.grpcontno ='"+mGrpContNo+"'";
         	}
         	if(!"".equals(mRiskCode))
         	{
         		mSQL +=" and a.riskcode ='"+mRiskCode+"'";
         	}
         	if(!"".equals(mContNo))
         	{
         		mSQL +=" and a.ContNo ='"+mContNo+"'";
         	}
         	if(!"".equals(mPolNo))
         	{
         		mSQL +=" and a.PolNo ='"+mPolNo+"'";
         	}
         	if(!"".equals(mOperator))
         	{
         		mSQL +=" and a.Operator ='"+mOperator+"'";
         	}
        }
        else
        {
        	strOperate = globalInput.Operator;
        }
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LRGetClaimDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "86";
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRGetClaimDataBL tLRGetClaimDataBL = new LRGetClaimDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2008-9-26");
            tTransferData.setNameAndValue("EndDate", "2008-9-30");
            System.out.println("in main: " +
                               (String) tTransferData.
                               getValueByName("StartDate") + "   " +
                               (String) tTransferData.getValueByName("EndDate"));
            vData.add(tTransferData);
            tLRGetClaimDataBL.submitData(vData, "CLAIMDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
