/*
 * <p>ClassName: ReNewContManageBL </p>
 * <p>Description: ReNewContManageBL类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2008-10-30
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LRContInfoSchema;
import com.sinosoft.lis.schema.LRCalFactorValueSchema;
import com.sinosoft.lis.schema.LRAmntRelRiskSchema;
import com.sinosoft.lis.vschema.LRCalFactorValueSet;
import com.sinosoft.lis.vschema.LRAmntRelRiskSet;
import com.sinosoft.lis.db.LRContInfoDB;

public class ReNewContManageBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String strOperate;
    private LRContInfoSchema mLRContInfoSchema = new LRContInfoSchema();
    private LRCalFactorValueSet mLRCalFactorValueSet = new LRCalFactorValueSet();
    private LRAmntRelRiskSet mLRAmntRelRiskSet = new LRAmntRelRiskSet();
    private MMap mMap = new MMap();

    private PubSubmit tPubSubmit = new PubSubmit();


    //业务处理相关变量
    /** 全局数据 */

    public ReNewContManageBL() {
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        System.out.println("before getInputData()........");
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("after getInputData()........");

        if (!dealData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        ReNewContManageBL tReContManageBL = new ReNewContManageBL();
        VData vData = new VData();

        try {
            tReContManageBL.submitData(vData, "INSERT");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
//            this.mInputData=new VData();
//            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //进行插入数据
        if (this.strOperate.equals("INSERT")) {
            if (!insertData()) {
                // @@错误处理
                buildError("insertData", "增加再保合同时出现错误!");
                return false;
            }
        }
        //对数据进行修改操作
        if (this.strOperate.equals("UPDATE")) {
            if (!updateData()) {
                // @@错误处理
                buildError("UpdateData", "更新再保合同时出现错误!!");
                return false;
            }
        }
        //对数据进行删除操作
        if (this.strOperate.equals("DELETE")) {
            if (!deleteData()) {
                // @@错误处理
                buildError("DeleteData", "删除再保合同时出现错误!");
                return false;
            }
        }
        //对再保合同进行审核
        if (this.strOperate.equals("CONFIRM")) {
            if (!confirmData()) {
                // @@错误处理
                buildError("DeleteData", "再保合同审核确认成功!");
                return false;
            }
        }

        return true;
    }
    /**
     * confirmData
     * 合同审核,修改合同状态为有效状态
     * @return boolean
     */
    private boolean confirmData(){
        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
        tLRContInfoDB.setReContCode(mLRContInfoSchema.getReContCode());
        System.out.println("come in confirm........");
        if (!tLRContInfoDB.getInfo()) {
            buildError("insertData", "该再保合同编号不存在!");
            return false;
        }
        LRContInfoSchema tLRContInfoSchema = tLRContInfoDB.getSchema();
        String sqla = "update lrcontinfo set recontstate='01',modifydate=current date,modifytime=current time,"
                      +" Operator='"+mGlobalInput.Operator+"',ManageCom='"+mGlobalInput.ManageCom+"'"
                      +" where recontcode='"+tLRContInfoSchema.getReContCode()+"' with ur";
        mMap.put(sqla, "UPDATE");
        if (!prepareOutputData()) {
           return false;
       }
       if (!tPubSubmit.submitData(this.mInputData, "")) {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("updateData", "修改时出现错误!");
               return false;
           }
       }
       System.out.println("after PubSubmit ............");
       mMap = null;
       tPubSubmit = null;
       return true;
    }

    /**
     * deleteData
     * 删险合同信息
     * @return boolean
     */
    private boolean deleteData() {
        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
        tLRContInfoDB.setReContCode(mLRContInfoSchema.getReContCode());
        System.out.println("come in delete........");
        if (!tLRContInfoDB.getInfo()) {
            buildError("insertData", "该再保合同编号不存在!");
            return false;
        }

        String strSQL1 = "delete from LRContInfo where ReContCode='" +
                         mLRContInfoSchema.getReContCode() + "'";
        mMap.put(strSQL1, "DELETE");
        String strSQL2 = "delete from LRAmntRelRisk where ReContCode='" +
                         mLRContInfoSchema.getReContCode() + "'";
        mMap.put(strSQL2, "DELETE");
        String strSQL3 = "delete from LRCalFactorValue where ReContCode='" +
                         mLRContInfoSchema.getReContCode() + "'";
        mMap.put(strSQL3, "DELETE");

        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("updateData", "修改时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;
        return true;
    }

    /**
     * updateData
     * 修改合同信息
     * @return boolean
     */
    private boolean updateData() {
        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
       tLRContInfoDB.setReContCode(mLRContInfoSchema.getReContCode());
       System.out.println("come in update........");
       if (!tLRContInfoDB.getInfo()) {
           buildError("insertData", "该再保合同编号不存在!");
           return false;
       }

       PubFun tPubFun = new PubFun();
       String currentDate = tPubFun.getCurrentDate();
       String currentTime = tPubFun.getCurrentTime();

       mLRContInfoSchema.setOperator(mGlobalInput.Operator);
       mLRContInfoSchema.setManageCom(mGlobalInput.ManageCom);
       mLRContInfoSchema.setMakeDate(currentDate);
       mLRContInfoSchema.setMakeTime(currentTime);
       mLRContInfoSchema.setModifyDate(currentDate);
       mLRContInfoSchema.setModifyTime(currentTime);

       mMap.put(mLRContInfoSchema, "UPDATE");

       String strSQLa = "delete from LRCalFactorValue where ReContCode='" +
                         mLRContInfoSchema.getReContCode() + "'";
       mMap.put(strSQLa, "DELETE");
       for(int i=1;i<mLRCalFactorValueSet.size()+1;i++){
    	   
    	   if(mLRCalFactorValueSet.get(i).getFactorCode().equals("TBLmt")){
       		mLRCalFactorValueSet.get(i).setFactorValue(mLRCalFactorValueSet.get(i).getValueType());
       		if(mLRCalFactorValueSet.get(i).getFactorValue().equals("1")){
       		mLRCalFactorValueSet.remove(mLRCalFactorValueSet.get(i--));
       		continue;
       		}
       	}
         mLRCalFactorValueSet.get(i).setMakeDate(currentDate);
         mLRCalFactorValueSet.get(i).setMakeTime(currentTime);
         mLRCalFactorValueSet.get(i).setModifyDate(currentDate);
         mLRCalFactorValueSet.get(i).setModifyTime(currentTime);
         mLRCalFactorValueSet.get(i).setOperator(mGlobalInput.Operator);
         mLRCalFactorValueSet.get(i).setManageCom(mGlobalInput.ManageCom);
       }
       mMap.put(mLRCalFactorValueSet,"INSERT");

       String strSQLb= "delete from LRAmntRelRisk where ReContCode='" +
                                mLRContInfoSchema.getReContCode() + "'";
       mMap.put(strSQLb, "DELETE");
       for(int i=1;i<mLRAmntRelRiskSet.size()+1;i++){
         mLRAmntRelRiskSet.get(i).setMakeDate(currentDate);
         mLRAmntRelRiskSet.get(i).setMakeTime(currentTime);
         mLRAmntRelRiskSet.get(i).setModifyDate(currentDate);
         mLRAmntRelRiskSet.get(i).setModifyTime(currentTime);
         mLRAmntRelRiskSet.get(i).setOperator(mGlobalInput.Operator);
         mLRAmntRelRiskSet.get(i).setManageCom(mGlobalInput.ManageCom);
       }
       mMap.put(mLRAmntRelRiskSet,"INSERT");


       if (!prepareOutputData()) {
           return false;
       }
       if (!tPubSubmit.submitData(this.mInputData, "")) {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("insertData", "修改再保合同信息时出现错误!");
               return false;
           }
       }
       System.out.println("after PubSubmit ............");
       mMap = null;
       tPubSubmit = null;
       return true;
    }

    /**
     * insertData
     * 新增合同信息
     * @return boolean
     */
    private boolean insertData() {
        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
        tLRContInfoDB.setReContCode(mLRContInfoSchema.getReContCode());
        System.out.println("come in insert........");
        if (tLRContInfoDB.getInfo()) {
            buildError("insertData", "该再保合同编号已经存在!");
            return false;
        }

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        mLRContInfoSchema.setOperator(mGlobalInput.Operator);
        mLRContInfoSchema.setManageCom(mGlobalInput.ManageCom);
        mLRContInfoSchema.setMakeDate(currentDate);
        mLRContInfoSchema.setMakeTime(currentTime);
        mLRContInfoSchema.setModifyDate(currentDate);
        mLRContInfoSchema.setModifyTime(currentTime);

        mMap.put(mLRContInfoSchema, "INSERT");

        for(int i=1;i<mLRCalFactorValueSet.size()+1;i++){
        	if(mLRCalFactorValueSet.get(i).getFactorCode().equals("TBLmt")){
        		mLRCalFactorValueSet.get(i).setFactorValue(mLRCalFactorValueSet.get(i).getValueType());
        		if(mLRCalFactorValueSet.get(i).getFactorValue().equals("1")){
        		mLRCalFactorValueSet.remove(mLRCalFactorValueSet.get(i--));
        		continue;
        		}
        	}
          mLRCalFactorValueSet.get(i).setMakeDate(currentDate);
          mLRCalFactorValueSet.get(i).setMakeTime(currentTime);
          mLRCalFactorValueSet.get(i).setModifyDate(currentDate);
          mLRCalFactorValueSet.get(i).setModifyTime(currentTime);
          mLRCalFactorValueSet.get(i).setOperator(mGlobalInput.Operator);
          mLRCalFactorValueSet.get(i).setManageCom(mGlobalInput.ManageCom);
        }
        mMap.put(mLRCalFactorValueSet,"INSERT");

        for(int i=1;i<mLRAmntRelRiskSet.size()+1;i++){
          mLRAmntRelRiskSet.get(i).setMakeDate(currentDate);
          mLRAmntRelRiskSet.get(i).setMakeTime(currentTime);
          mLRAmntRelRiskSet.get(i).setModifyDate(currentDate);
          mLRAmntRelRiskSet.get(i).setModifyTime(currentTime);
          mLRAmntRelRiskSet.get(i).setOperator(mGlobalInput.Operator);
          mLRAmntRelRiskSet.get(i).setManageCom(mGlobalInput.ManageCom);
        }
        mMap.put(mLRAmntRelRiskSet,"INSERT");


        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "保存再保合同信息时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    public String createWorkNo(String ags) {
        String tWorkNo = "";
        if (ags.equals("RECOMCODE")) {
            tWorkNo = PubFun1.CreateMaxNo("RECOMCODE", 3);
        } else {
            tWorkNo = PubFun1.CreateMaxNo("RELATION", 6);
        }
        return tWorkNo;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLRContInfoSchema.setSchema((LRContInfoSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LRContInfoSchema", 0));
        this.mLRCalFactorValueSet.set((LRCalFactorValueSet) cInputData.
                                         getObjectByObjectName(
                                                 "LRCalFactorValueSet", 0));
        this.mLRAmntRelRiskSet.set((LRAmntRelRiskSet) cInputData.
                                         getObjectByObjectName(
                                                 "LRAmntRelRiskSet", 0));


        return true;
    }

    public String getResult() {
        return mLRContInfoSchema.getReContCode();
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
