package com.sinosoft.lis.reinsure;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.certifybusiness.CardActiveRenewUI;
import com.sinosoft.lis.certifybusiness.CertifyContConst;


import com.sinosoft.lis.certifybusiness.CertifyInfoColl;
import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.LICertifyDB;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LRNoLongerContSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LBMissionSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.lis.vschema.LRNoLongerContSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class LRNoLongerListBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mFilePath = null;

    private String mFileName = null;

 

    private LRNoLongerContSet mLRNoLongerContSet;

   

    private String[] mSheetName = { "LRNoLongerCont", "Ver" };

    private String mConfigName = "LRNoLongerListDiskImport.xml";

    public LRNoLongerListBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

       

       

       
       

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Import".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = importCertifyList();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

//        if ("ImportConfirm".equals(mOperate))
//        {
//            tTmpMap = null;
//            tTmpMap = confirmImportList();
//            if (tTmpMap == null)
//            {
//                return false;
//            }
//            mMap.add(tTmpMap);
//            tTmpMap = null;
//        }
//
//        if ("ImportDelete".equals(mOperate))
//        {
//            tTmpMap = null;
//            tTmpMap = deleteImportList();
//            if (tTmpMap == null)
//            {
//                return false;
//            }
//            mMap.add(tTmpMap);
//            tTmpMap = null;
//        }
//
//        if ("ActiveImport".equals(mOperate))
//        {
//            tTmpMap = null;
//            tTmpMap = activeImportList();
//            if (tTmpMap == null)
//            {
//                return false;
//            }
//            mMap.add(tTmpMap);
//            tTmpMap = null;
//        }

        return true;
    }

    private MMap importCertifyList()
    {
        MMap tMMap = new MMap();

        MMap tTmpMap = null;

        // 准备导入所需数据。
        if (!prepareBeforeImportListDatas())
        {
            return null;
        }
        // ----------------------------

        // 获取导入文件数据。
        if (!parseDiskFile())
        {
            return null;
        }
        // ----------------------------

        // 创建卡单信息。
        tTmpMap = null;
        tTmpMap = createLRNoLongerContList();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ----------------------------

        return tMMap;
    }

    /**
     * 获取磁盘导入文件相关数据。
     * <br />如不存在数据信息，则置 size为0的set集合。
     * @return true：获取数据信息成功；false：获取数据信息失败
     */
    private boolean parseDiskFile()
    {
        String tUIRoot = CommonBL.getUIRoot();
//        tUIRoot="E:/lis/ui/";
        if (tUIRoot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }

        String tConfigPath = new ExeSQL()
                .getOneValue("select SysVarValue from LDSysVar where SysVar = 'XmlPath'");

        LRNoLongerImportBL importFile = new LRNoLongerImportBL(mFilePath
                + mFileName, tUIRoot + tConfigPath + mConfigName, mSheetName);

        if (!importFile.doImport())
        {
            this.mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        mLRNoLongerContSet = importFile.getLRNoLongerContSet();
        if (mLRNoLongerContSet == null)
        {
        	mLRNoLongerContSet = new LRNoLongerContSet();
        }

//        LICertifyInsuredSet tLICertifyInsuredSet = importFile
//                .getLICertifyInsuredSet();
//        if (tLICertifyInsuredSet == null)
//        {
//            tLICertifyInsuredSet = new LICertifyInsuredSet();
//        }
//
//        mCertifyInfoList = new CertifyInfoColl();
//        if (!mCertifyInfoList.loadCertifyInfo(tLICertifySet,
//                tLICertifyInsuredSet))
//        {
//            String tStrErr = "获取单证导入数据失败。";
//            buildError("parseDiskFile", tStrErr);
//            return false;
//        }
//
//        if (mCertifyInfoList.size() == 0)
//        {
//            String tStrErr = "清单数据信息为空。请核实后重新导入。";
//            buildError("parseDiskFile", tStrErr);
//            return false;
//        }

        return true;
    }

    /**
     * 创建单证清单。
     * @return
     */
    private MMap createLRNoLongerContList()
    {
        MMap tTmpMap = new MMap();
        boolean tAllSuccFlag = true;
        String tStrSql = null;

        for (int i = 1; i <= mLRNoLongerContSet.size(); i++)
        {
        	LRNoLongerContSchema tLRNoLongerContSchema = null;
        	tLRNoLongerContSchema = mLRNoLongerContSet.get(i);

           

            if (tLRNoLongerContSchema == null)
            {
                continue;
            }

           
            tLRNoLongerContSchema.setRiskCode("0000");
            tLRNoLongerContSchema.setStandby1("01");
            tLRNoLongerContSchema.setOprater(mGlobalInput.Operator);
            tLRNoLongerContSchema.setMakeDate(PubFun.getCurrentDate());
            tLRNoLongerContSchema.setMakeTime(PubFun.getCurrentTime());
            tLRNoLongerContSchema.setModifyDate(PubFun.getCurrentDate());
            tLRNoLongerContSchema.setModifyTime(PubFun.getCurrentTime());

           
            // ---------------------------

            tTmpMap.put(tLRNoLongerContSchema, "DELETE&INSERT");
           

            // ---------------------------

            
        }

//        if (tAllSuccFlag && tTmpMap != null)
//        {
//            // 终止导入日志。
//            MMap tTmpLogEnd = null;
//            
//            if (tTmpLogEnd == null)
//            {
//                String tStrErr = "终止导入日志动作失败。";
//                buildError("createCertifyList", tStrErr);
//                System.out.println(tStrErr);
//                return null;
//            }
//            tTmpMap.add(tTmpLogEnd);
//            tTmpLogEnd = null;
//            // ---------------------------
//        }

        return (tAllSuccFlag ? tTmpMap : null);
    }


  

    

  

   

  

    /**
     * 准备导入前所需相关数据。
     * @return
     */
    private boolean prepareBeforeImportListDatas()
    {
        mFileName = (String) mTransferData.getValueByName("FileName");
        if (mFileName == null || mFileName.equals(""))
        {
            buildError("getInputData", "参数[导入文件名（FileName）]不存在。");
            return false;
        }

        mFilePath = (String) mTransferData.getValueByName("FilePath");
        if (mFilePath == null || mFilePath.equals(""))
        {
            buildError("getInputData", "参数[导入文件路径（FilePath）]不存在。");
            return false;
        }

        return true;
    }



    

    
   

  

    /**
     * 根据卡号从数据库加载卡信息。
     * @param cCardNo
     * @return
     */
    private LICertifySchema loadCertifyInfo(String cCardNo)
    {
        LICertifyDB tLICertifyDB = new LICertifyDB();
        tLICertifyDB.setCardNo(cCardNo);
        if (!tLICertifyDB.getInfo())
        {
            return null;
        }

        return tLICertifyDB.getSchema();
    }

   

  

  

    

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LRNoLongerContListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
