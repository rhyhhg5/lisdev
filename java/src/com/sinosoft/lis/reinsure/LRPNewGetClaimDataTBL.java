package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import java.util.Date;

/*
 * <p>ClassName: LRNewGetClaimDataBL </p>
 * <p>Description: LRNewGetClaimDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2008-11-27
 */
public class LRPNewGetClaimDataTBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
   

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    private PubFun mPubFun = new PubFun();
    private String mCurrentDate = new PubFun().getCurrentDate();

    private String mCurrentTime = new PubFun().getCurrentTime();

    //业务处理相关变量
    /** 全局数据 */

    public LRPNewGetClaimDataTBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT( String cOperate,String mToDay) {
        System.out.println("Come in BL->Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputDataT(mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
//            tError.moduleName = "LDComBL";
//            tError.functionName = "prepareData";
//            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
//            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取理赔数据
        
            if (!getClaimData()) {
                // @@错误处理
                buildError("insertData", "理赔摊回提数出现错误!");
                return false;
            }
        
        return true;
    }

    /**
     * 提取理赔数据，以结算日期为准
     * @return boolean
     */
    private boolean insertClaimData() {
        //删除该提数日期范围内的记录
        System.out.println("Come to InsertClaimData ...");
        //得到当天给付的理赔案件,提数原则 理赔赔付日期 = 提数日期
        //09/05/19修改,提数日期=ljaget的记录生成日期（makedate）理赔状态：通知给付、结案   ---liuli
        
        String tSql ="select a.GrpContNo,'0000','0000','0000',d.actugetno,d.otherno,d.otherno,d.RiskCode,d.RiskVersion, " +
        "'' 出险日期, " +
        "'1',sum(d.Pay),sum(d.Pay) 赔付金额, " +
        "d.operator,'' 结案日期,'' 索赔日期,d.ConfDate 赔付日期, " +
        "('') 索赔额,'1', " +
        "('青岛大额') 出险原因, " +
        "('') 出院日期, " +
        "('') 诊断," +
        "a.ManageCom ,a.agentGroup,'0000'," +
        "('') 理赔结论,a.cvalidate  " +
        "from LCGrpCont a,  ljagetclaim d " +
        "where d.otherno  like 'P94%' and exists (select 1 from lrpol where grpcontno=a.grpcontno and riskcode=d.riskcode" +
        " ) and " +
        "a.grpcontno = d.grpcontno   and d.othernotype='5'  " +
        "  and d.MakeDate = '" +
        this.mToday +
        "' group by a.GrpContNo,d.actugetno,d.RiskCode,d.RiskVersion, " +
        "d.ConfDate,a.ManageCom,a.agentGroup,a.cvalidate,d.operator,d.otherno " +
        " union " +
        "select a.GrpContNo,'0000','0000','0000',d.actugetno,d.otherno,d.otherno,d.RiskCode,d.RiskVersion, " +
        "'' 出险日期, " +
        "'1',sum(d.Pay),sum(d.Pay) 赔付金额, " +
        "d.operator,'' 结案日期,'' 索赔日期,d.ConfDate 赔付日期, " +
        "('') 索赔额,'1', " +
        "('青岛大额') 出险原因, " +
        "('') 出院日期, " +
        "('') 诊断," +
        "a.ManageCom ,a.agentGroup,'0000'," +
        "('') 理赔结论,a.cvalidate  " +
        "from LBGrpCont a,  ljagetclaim d " +
        "where d.otherno  like 'P94%' and exists (select 1 from lrpol where grpcontno=a.grpcontno and riskcode=d.riskcode" +
        " ) and " +
        "a.grpcontno = d.grpcontno and d.othernotype='5'   " +
        "  and d.MakeDate = '" +
        this.mToday +
        "' group by a.GrpContNo,d.actugetno,d.RiskCode,d.RiskVersion, " +
        "d.ConfDate,a.ManageCom,a.agentGroup,a.cvalidate,d.operator,d.otherno " +
        " with ur ";
        SSRS tSSRS = new SSRS();
    	RSWrapper rsWrapper = new RSWrapper();
    	if (!rsWrapper.prepareData(null, tSql))
        {
            System.out.println("数据准备失败! ");
            return false;
        }
        do {
        	
        	tSSRS = rsWrapper.getSSRS();
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            	mMap = new MMap();
                for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                    LRPolClmSchema tLRPolClmSchema = new LRPolClmSchema();
                    tLRPolClmSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolClmSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolClmSchema.setContNo(tSSRS.GetText(i, 3));
                    tLRPolClmSchema.setPolNo(tSSRS.GetText(i, 4)); //保单号
                    tLRPolClmSchema.setClmNo(tSSRS.GetText(i, 5));
                    tLRPolClmSchema.setRgtNo(tSSRS.GetText(i, 6));
                    tLRPolClmSchema.setCaseNo(tSSRS.GetText(i, 7));
                    tLRPolClmSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolClmSchema.setRiskVer(tSSRS.GetText(i, 9));
                    tLRPolClmSchema.setAccidentDate(tSSRS.GetText(i, 10)); //出险日期
                    tLRPolClmSchema.setClmState(tSSRS.GetText(i, 11));
                    tLRPolClmSchema.setStandPay(tSSRS.GetText(i, 12));
                    tLRPolClmSchema.setRealPay(tSSRS.GetText(i, 13)); //赔付金额
                    tLRPolClmSchema.setClmUWer(tSSRS.GetText(i, 14));
                    tLRPolClmSchema.setEndCaseDate(tSSRS.GetText(i, 15)); //结案日期
                    tLRPolClmSchema.setRgtDate(tSSRS.GetText(i, 16)); //索赔日期
                    tLRPolClmSchema.setConfDate(tSSRS.GetText(i, 17)); //赔付日期
                    tLRPolClmSchema.setClaimMoney(tSSRS.GetText(i, 18)); //索赔额
                    tLRPolClmSchema.setUWFlag(tSSRS.GetText(i, 19));
                    tLRPolClmSchema.setAccDesc(tSSRS.GetText(i, 20)); //出险原因
                    tLRPolClmSchema.setLeaveHospDate(tSSRS.GetText(i, 21)); //出院日期
                    tLRPolClmSchema.setDiagnoses(tSSRS.GetText(i, 22)); //诊断
//                  if(tLRPolClmSchema.getRealPay()>=0){
                    tLRPolClmSchema.setGiveTypeDesc(tSSRS.GetText(i, 26)); //理赔结论
//                }else{
//                    tLRPolClmSchema.setGiveTypeDesc("纠错"); //理赔结论
//                }
                    tLRPolClmSchema.setManageCom(tSSRS.GetText(i, 23)); //保单管理机
                    //080714新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                                 +tSSRS.GetText(i,24)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolClmSchema.setCostCenter(CostCenter);

                    tLRPolClmSchema.setGetDataDate(this.mToday);
                    tLRPolClmSchema.setOperator("Server");
                    tLRPolClmSchema.setMakeDate(this.mCurrentDate);
                    tLRPolClmSchema.setMakeTime(this.mCurrentTime);
                    tLRPolClmSchema.setModifyDate(this.mCurrentDate);
                    tLRPolClmSchema.setModifyTime(this.mCurrentTime);
                    String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
                    String LRiskFlag = new ExeSQL().getOneValue(aSql);
                    String reNewCount = "0";
                  
                        
                        tLRPolClmSchema.setReNewCount(0); //续保次数
                    
                    String masterPolno = tSSRS.GetText(i,25);
                    String tSql2 =  "select a.ReContCode,'P',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                            "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode, " +
                            "(select ReType from lRContInfo where ReContCode=a.ReContCode) from LRPol a "+
                            "where (a.GRPCONTNO = '" +
                            tSSRS.GetText(i, 1) +"' AND A.RISKCODE='"+tSSRS.GetText(i, 8)+"') "+
                            " and exists (select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01'" +
                            " union all select 1 from LRTempCessCont where tempcontcode=a.recontcode and recontstate='01')" +
                            " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode " +
                                        		" with ur"; 
                    SSRS cSSRS = new ExeSQL().execSQL(tSql2);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                        tLRPolClmSchema.setReContCode(cSSRS.GetText(j, 1)); //合同名称
                        if(cSSRS.GetText(j, 2).equals("F")){  //当取出来的是反冲数据时，跳过不提取。
                            continue;
                        }
                        tLRPolClmSchema.setReinsureItem(cSSRS.GetText(j, 2));
                        tLRPolClmSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolClmSchema.setReComCode(cSSRS.GetText(j, 4));
                        
                        mMap.put(tLRPolClmSchema.getSchema(), "DELETE&INSERT");
                    }
                   
                }

                //提取每天的合同再保数据，作为一个事务
                if (!prepareOutputData()) {
                }
                //如果提交不成功,不能返回
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(this.mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        // @@错误处理
                        buildError("insertData", "保存临分协议信息时出现错误!");
                    }
                }
            }
           
        }while (tSSRS != null && tSSRS.MaxRow > 0);
		rsWrapper.close();
        return true;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolClmSchema aLRPolClmSchema,
                             String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new
                                             LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolClmSchema.getPolNo());
        tLRErrorLogSchema.setLogType("4");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator("Server");
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }

    /**
     * 提取理赔数据
     *
     * @return boolean
     */
    private boolean getClaimData() {
        
        

    	

    	FDate chgdate = new FDate();
    	String currentDate=PubFun.calDate(mCurrentDate,-1,"D",null);
    	 	
    	
        Date dbdate = chgdate.getDate(currentDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(currentDate); //录入的终止日期，用FDate处理
        if(!StrTool.cTrim(this.mToday).equals("")&&this.mToday!=null){
    		
    		dbdate = chgdate.getDate(this.mToday); 
    		dedate = chgdate.getDate(this.mToday);
            
        }
         while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
             this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
             if (!insertClaimData()) {
                 buildError("getClaimData", "提取" + mToday + "号理赔数据出错!");
                 return false;
             }
             LRPNewCalClaimDataTBL tLRPNewCalClaimDataTBL=new LRPNewCalClaimDataTBL();
             tLRPNewCalClaimDataTBL.submitDataT(this.mToday);
             LRPClaimListBL tLRPClaimListBL=new LRPClaimListBL();
             tLRPClaimListBL.submitDataT(this.mToday);
             dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
         }
        
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(String tToDay) {
        
    	this.mToday = tToDay;
       
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

//        cError.moduleName = "LRGetClaimDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
       

        
        LRPNewGetClaimDataTBL tLRPNewGetClaimDataTBL = new LRPNewGetClaimDataTBL();
        
        try {
           
          
        	tLRPNewGetClaimDataTBL.getClaimData();
        	
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
