package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.taskservice.TaskThread;

import java.util.Date;

/*
 * <p>ClassName: LRNewGetClaimDataBL </p>
 * <p>Description: LRNewGetClaimDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2008-11-27
 */
public class LRNewGetClaimDataRBBL{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";



    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    private PubFun mPubFun = new PubFun();
    private String mCurrentDate = new PubFun().getCurrentDate();

    private String mCurrentTime = new PubFun().getCurrentTime();
    private String mToDate =null;
    //业务处理相关变量
    /** 全局数据 */

    public LRNewGetClaimDataRBBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
  
    
    public boolean run(String tDate) {
        VData aVData = new VData();
        globalInput.ComCode = null;
        aVData.addElement(globalInput);
        mToDate = tDate;
        if (!submitData()) {
            return false;
        }
        return true;
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData() {
       
      
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取理赔数据
      
            if (!getClaimData()) {
                // @@错误处理
                buildError("insertData", "理赔摊回提数出现错误!");
                return false;
            }
       
        return true;
    }

    /**
     * 提取理赔数据，以结算日期为准
     * @return boolean
     */
    private boolean insertClaimData() {
        //删除该提数日期范围内的记录
        System.out.println("Come to InsertClaimData ...");
        //得到当天给付的理赔案件,提数原则 理赔赔付日期 = 提数日期
        //09/05/19修改,提数日期=ljaget的记录生成日期（makedate）理赔状态：通知给付、结案   ---liuli
        
        String tSql = "select a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
            "(select min(a1.AccDate) from llsubreport a1,llcaserela a2  where a1.SubRptNo = a2.SubRptNo and a2.CaseNo=c.CaseNo) 出险日期, " +
            "(select lp.ClmState from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),sum(b.StandPay)," +
            "(select sum(pay) from ljagetclaim where polno=d.polno and otherno=d.otherno and makedate=d.makedate and othernotype='5') 赔付金额, " +
            "(select lp.ClmUwer from llclaimpolicy lp where lp.ClmNo=b.ClmNo and lp.polno=a.polno  fetch first 1 rows only ),c.EndCaseDate 结案日期,c.RgtDate 索赔日期,d.ConfDate 赔付日期, " +
            " sum(b.ClaimMoney)  索赔额,a.UWFlag, " +
            "(select min(a2.accdesc) from llcaserela a1,llsubreport a2 where a1.SubRptNo=a2.SubRptNo and a1.caseNo=c.CaseNo) 出险原因, " +
            "(select max(HospEndDate) from llfeemain where caseno=c.caseno) 出院日期, " +
            "(select DiseaseName from LLCaseCure where caseno=c.caseno order by serialno desc fetch first row only) 诊断," +
            "a.ManageCom ,a.agentGroup,a.masterPolno ," +
            "(select distinct givetypedesc from llclaim where caseno=c.caseno  and ClmNo = b.ClmNo) 理赔结论,a.cvalidate,a.prtno,a.RenewCount,a.insuredno  " +
            "from LBPol a, llclaimdetail b, llcase c, ljagetclaim d " +
            "where exists (select 1 from lrpol where prtno=a.prtno and renewcount=a.renewcount and riskcode=a.riskcode and insuredno=a.insuredno) and " +
            "  not exists (select 1 from LRPol where prtno=a.prtno and riskcode=a.riskcode and insuredno=a.insuredno and renewcount=a.renewcount and TempCessFlag='Y') and "+
            "a.PolNo = b.PolNo and b.CaseNo = c.CaseNo and a.polno=d.polno and a.edorno like 'xb%' " +
            "and a.Appflag = '1' and (c.caseno=d.otherno or c.rgtno=d.otherno) and d.othernotype='5' and d.MakeDate = '" +
            this.mToday +
            "' group by a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,b.ClmNo,c.RgtNo,c.CaseNo,a.RiskCode,a.RiskVersion, " +
            "c.EndCaseDate,c.RgtDate,d.ConfDate,a.UWFlag,a.ManageCom,a.agentGroup,a.masterPolno,a.cvalidate,a.RenewCount,a.insuredno,a.prtno,d.makedate,d.otherno,d.polno " +
            " with ur ";
        SSRS tSSRS = new SSRS();
    	RSWrapper rsWrapper = new RSWrapper();
    	if (!rsWrapper.prepareData(null, tSql))
        {
            System.out.println("数据准备失败! ");
            return false;
        }
        do {
        	
        	tSSRS = rsWrapper.getSSRS();
         	
 			if (tSSRS != null || tSSRS.MaxRow > 0) {
            
            mMap = new MMap();
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            	for(int i=1;i<=tSSRS.MaxRow;i++){ 
            		 String tSqlC =  "select contno,polno from lrpol where prtno='"+tSSRS.GetText(i, 28)+"' " +
            		 		"and insuredno='"+tSSRS.GetText(i, 30)+"' and riskcode='"+tSSRS.GetText(i, 8)+"' fetch first 1 row only with ur"; //?????日期上正式机需要修改。此处是为了兼容以前的长险分保，续保次数都为０
            		 SSRS ccSSRS = new ExeSQL().execSQL(tSqlC);
                    LRPolClmSchema tLRPolClmSchema = new LRPolClmSchema();
                    tLRPolClmSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolClmSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolClmSchema.setContNo(ccSSRS.GetText(1, 1));
                    tLRPolClmSchema.setPolNo(ccSSRS.GetText(1, 2)); //保单号
                    tLRPolClmSchema.setClmNo(tSSRS.GetText(i, 5));
                    tLRPolClmSchema.setRgtNo(tSSRS.GetText(i, 6));
                    tLRPolClmSchema.setCaseNo(tSSRS.GetText(i, 7));
                    tLRPolClmSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolClmSchema.setRiskVer(tSSRS.GetText(i, 9));
                    tLRPolClmSchema.setAccidentDate(tSSRS.GetText(i, 10)); //出险日期
                    tLRPolClmSchema.setClmState(tSSRS.GetText(i, 11));
                    tLRPolClmSchema.setStandPay(tSSRS.GetText(i, 12));
                    tLRPolClmSchema.setRealPay(tSSRS.GetText(i, 13)); //赔付金额
                    tLRPolClmSchema.setClmUWer(tSSRS.GetText(i, 14));
                    tLRPolClmSchema.setEndCaseDate(tSSRS.GetText(i, 15)); //结案日期
                    tLRPolClmSchema.setRgtDate(tSSRS.GetText(i, 16)); //索赔日期
                    tLRPolClmSchema.setConfDate(tSSRS.GetText(i, 17)); //赔付日期
                    tLRPolClmSchema.setClaimMoney(tSSRS.GetText(i, 18)); //索赔额
                    tLRPolClmSchema.setUWFlag(tSSRS.GetText(i, 19));
                    tLRPolClmSchema.setAccDesc(tSSRS.GetText(i, 20)); //出险原因
                    tLRPolClmSchema.setLeaveHospDate(tSSRS.GetText(i, 21)); //出院日期
                    tLRPolClmSchema.setDiagnoses(tSSRS.GetText(i, 22)); //诊断
//                    if(tLRPolClmSchema.getRealPay()>=0){
                        tLRPolClmSchema.setGiveTypeDesc(tSSRS.GetText(i, 26)); //理赔结论
//                    }else{
//                        tLRPolClmSchema.setGiveTypeDesc("纠错"); //理赔结论
//                    }
                    tLRPolClmSchema.setManageCom(tSSRS.GetText(i, 23)); //保单管理机
                    //080714新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                                 +tSSRS.GetText(i,24)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolClmSchema.setCostCenter(CostCenter);

                    tLRPolClmSchema.setGetDataDate(this.mToday);
                    tLRPolClmSchema.setOperator("ServerR");
                    tLRPolClmSchema.setMakeDate(this.mCurrentDate);
                    tLRPolClmSchema.setMakeTime(this.mCurrentTime);
                    tLRPolClmSchema.setModifyDate(this.mCurrentDate);
                    tLRPolClmSchema.setModifyTime(this.mCurrentTime);
                    
                    String reNewCount = "0";
                  
                       
                            
                            reNewCount = tSSRS.GetText(i, 29); //这里主要是考虑到出险日期可能不在保单生效期间 by huxl @070724
                            if (reNewCount==null||reNewCount.equals("")) {
                                reNewCount = "0";
                            }
                        
                        tLRPolClmSchema.setReNewCount(reNewCount); //续保次数
                    
                    String masterPolno = tSSRS.GetText(i,25);
                    String tSql2 =  "select distinct a.ReContCode,'C',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                            "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode, " +
                            "(select ReType from lRContInfo where ReContCode=a.ReContCode) from LRPol a "+
                            "where a.PolNo = '" +
                            ccSSRS.GetText(1, 2) +"'  and RenewCount = " +tLRPolClmSchema.getReNewCount()+
                            " and exists " +
                            "(select 1 from lrcontinfo where recontcode=a.recontcode and recontstate='01')" +
                            " group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode" +
                            " union all "+
                            "select distinct a.ReContCode,'C',a.RiskCalSort,(case  when a.TempCessFlag='N' then (select recomcode from LRContInfo b where b.recontcode=a.recontcode) "+
                            "else (select comcode from LRTempCessCont b where b.tempcontcode=a.recontcode) end) comcode, " +
                            "(select ReType from lRContInfo where ReContCode=a.ReContCode) from LRPol a "+
                            "where exists (select 1 from lmriskapp m where m.riskcode=a.riskcode and m.riskperiod = 'L') and a.PolNo = '" +
                            ccSSRS.GetText(1, 2) +"'  and RenewCount =0  and a.getdatadate<='2009-9-1' " +
                            		" and exists (select 1 from lrcontinfo " +
                            		" where recontcode=a.recontcode and recontstate='01')" +
                            		" group by a.ReContCode,a.RiskCalSort,a.TempCessFlag,a.recontcode with ur"; //?????日期上正式机需要修改。此处是为了兼容以前的长险分保，续保次数都为０
                    SSRS cSSRS = new ExeSQL().execSQL(tSql2);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                        tLRPolClmSchema.setReContCode(cSSRS.GetText(j, 1)); //合同名称
                        if(cSSRS.GetText(j, 2).equals("F")){  //当取出来的是反冲数据时，跳过不提取。
                            continue;
                        }
                        tLRPolClmSchema.setReinsureItem(cSSRS.GetText(j, 2));
                        tLRPolClmSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolClmSchema.setReComCode(cSSRS.GetText(j, 4));
                        if(cSSRS.GetText(j,5).equals("02")){  //如果是按责任分保，则提取该责任的理赔赔付金额
                          String sqlduty = "select distinct  riskcode from lrcalfactorvalue b where b.recontcode='"+tLRPolClmSchema.getReContCode()+"' with ur";
                          String getdutycode = new ExeSQL().getOneValue(sqlduty);  //查询分保给付责任号
                          if(getdutycode!=null){
                             sqlduty = "select  sum(standPay),sum(ClaimMoney),sum(realpay) from LLClaimDetail where caseno='"+tLRPolClmSchema.getCaseNo()+
                                           "' and getdutycode='"+getdutycode+"' and polno='"+tSSRS.GetText(i, 4)+"'with ur";
                              SSRS dSSRS = new ExeSQL().execSQL(sqlduty);
                              if (dSSRS.getMaxRow() <= 0) {
                          		continue;
                              }
                              if ((dSSRS.GetText(1, 1) == null||dSSRS.GetText(1, 1).equals("")||dSSRS.GetText(1, 1).equals("null"))&& 
                            		  (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 2).equals("null"))&&
                            		  (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 1).equals("null"))) {
                            	  continue;
                              }
                              if (dSSRS.GetText(1, 1) == null||dSSRS.GetText(1, 1).equals("")||dSSRS.GetText(1, 1).equals("null"))
                            	  tLRPolClmSchema.setStandPay(0);
                              else
                            	  tLRPolClmSchema.setStandPay(dSSRS.GetText(1,1));
                              if (dSSRS.GetText(1, 2) == null||dSSRS.GetText(1, 2).equals("")||dSSRS.GetText(1, 2).equals("null"))
                            	  tLRPolClmSchema.setClaimMoney(0); //责任索赔额
                              else
                            	  tLRPolClmSchema.setClaimMoney(dSSRS.GetText(1,2)); //责任索赔额
                              if (dSSRS.GetText(1, 3) == null||dSSRS.GetText(1, 3).equals("")||dSSRS.GetText(1, 3).equals("null"))
                            	  tLRPolClmSchema.setRealPay(0);
                              else
                            	  tLRPolClmSchema.setRealPay(dSSRS.GetText(1,3)); //责任实赔付
                              tLRPolClmSchema.setGetDutyCode(getdutycode);
                          }else{ //是按责任分保但是计算要素中查询不到分保给付责任号，则置理赔费用为0
                               tLRPolClmSchema.setStandPay(0);
                               tLRPolClmSchema.setClaimMoney(0);
                               tLRPolClmSchema.setRealPay(0);
                               tLRPolClmSchema.setGetDutyCode("aa"); //按责任分保但是没有查到责任
                          }
                        }
                        mMap.put(tLRPolClmSchema.getSchema(), "DELETE&INSERT");
                    }
                    LRClaimXBRelationSchema tLRClaimXBRelationSchema=new LRClaimXBRelationSchema();
                    tLRClaimXBRelationSchema.setOPolNo(ccSSRS.GetText(1, 2));
                    tLRClaimXBRelationSchema.setNPolNo2(tSSRS.GetText(i, 4));
                    tLRClaimXBRelationSchema.setCValiDate(tSSRS.GetText(i, 27));
                    tLRClaimXBRelationSchema.setReNewCount(tSSRS.GetText(i, 29));
                    tLRClaimXBRelationSchema.setStandby1("1");
                    tLRClaimXBRelationSchema.setStandby2("1");
                    tLRClaimXBRelationSchema.setOperator("Server");
                    tLRClaimXBRelationSchema.setMakeDate(this.mCurrentDate);
                    tLRClaimXBRelationSchema.setMakeTime(this.mCurrentTime);
                    tLRClaimXBRelationSchema.setModifyDate(this.mCurrentDate);
                    tLRClaimXBRelationSchema.setModifyTime(this.mCurrentTime);
                    mMap.put(tLRClaimXBRelationSchema.getSchema(), "DELETE&INSERT");
            	  }
                
                //提取每天的合同再保数据，作为一个事务
                if (!prepareOutputData()) {
                }
                //如果提交不成功,不能返回
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(this.mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        // @@错误处理
                        buildError("insertData", "保存临分协议信息时出现错误!");
                    }
                }
            }
            }
 			
           
        }while (tSSRS != null && tSSRS.MaxRow > 0);
				rsWrapper.close();
        return true;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolClmSchema aLRPolClmSchema,
                             String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new
                                             LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolClmSchema.getPolNo());
        tLRErrorLogSchema.setLogType("4");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator("Server");
        tLRErrorLogSchema.setManageCom("86");
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }

    /**
     * 提取理赔数据
     *
     * @return boolean
     */
    private boolean getClaimData() {
        
        

    	

    	FDate chgdate = new FDate();
    	String currentDate=PubFun.calDate(mCurrentDate,-1,"D",null);
    	 	
    	
        Date dbdate = chgdate.getDate(currentDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(currentDate); //录入的终止日期，用FDate处理
        if(!StrTool.cTrim(this.mToDate).equals("")&&this.mToDate!=null){
     		
     		dbdate = chgdate.getDate(this.mToDate); 
     		dedate = chgdate.getDate(this.mToDate);
             
         }
         while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
             this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
             if (!insertClaimData()) {
                 buildError("getClaimData", "提取" + mToday + "号理赔数据出错!");
                 return false;
             }
             dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
         }
        
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }



    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

       
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
       

        
        LRNewGetClaimDataRBBL tLRNewGetClaimDataRBBL = new LRNewGetClaimDataRBBL();
        try {
           
          
            tLRNewGetClaimDataRBBL.submitData();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
