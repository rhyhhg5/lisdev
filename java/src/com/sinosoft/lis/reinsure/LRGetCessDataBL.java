/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import java.util.Date;
import com.sinosoft.lis.vschema.LRContInfoSet;
import java.util.*;

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: 提取再保新单数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Zhang Bin
 * @Modified by huxl：权责发生制改造 at 2007-12-19
 * @CreateDate：2006-07-30
 */
public class LRGetCessDataBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private String mRecontCode; //再保合同代码
    private String mRiskCalSort = "";

    //业务处理相关变量
    /** 全局数据 */

    public LRGetCessDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRGetCessDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.strOperate.equals("CESSDATA")) {
            if (!getCessData()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 提取分保数据
     * @return boolean
     */
    private boolean getCessData() {
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");

        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(mEndDate); //录入的终止日期，用FDate处理

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!insertPolData()) {
                buildError("getCessData", "提取" + mToday + "日新单数据出错");
                return false;
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
//        if (!insertPolDetailData()) {
//            buildError("insertPolDetailData", "分保保单的交费数据时出错！");
//            return false;
//        }

        return true;
    }

    /**
     * 提取合同分保保单信息
     * @return boolean
     */
    private boolean insertPolData() {
        //得到当天的新单及保单周年日的保单,应该包括已经退保的保单
        //契约新单:Max(保单生效日期、第一次交费核销日)=提数日期
        //续保保单:Max(保单续保生效日期、第一次交费核销日)=提数日期
        //保全增人:Max(保单生效日期、保全结案日期)=提数日期,由于系统无法区分保全增人和契约新单,因此增人处理同契约新单.
        //长险续期:保单周年日 = 提数日期
        //这里对于保单周年日的提取,限制必须是长期险,同时本保单年度发生过缴费
        String tSql =  //提取新单和增人的数据
                "select a.PolNo ,'0' from LCPol a where ((cvalidate < signdate and signdate = '" +
                mToday + "') " + "or (cvalidate >= signdate and cvalidate = '" +
                mToday + "')) " +
                " and a.AppFlag = '1' and a.ReinsureFlag = '4' and not exists (select 1 from lrpol where polno = a.polno and RenewCount = a.RenewCount) " +
                //提取长险趸缴的数据，此时提数日期为保单周年日
                " union select a.PolNo,'1' from LCPol a where a.AppFlag = '1' and a.ReinsureFlag = '4' and a.RiskCode in " +
                "(select riskcode from lmriskapp where riskperiod = 'L') and PayIntv = 0 and PayToDate > '" +
                mToday + "' and Month(CValidate) = Month('" + mToday +
                "') and day(CValidate)=day('" + mToday +
                "') and Year(CValidate) < Year('" + mToday + "')  " +
                //提取长险非趸缴数据，提数日期：新的保单年度第一次缴费核销日期
                " union select a.PolNo,'1' from LCPol a where a.AppFlag = '1' and a.ReinsureFlag = '4' and a.RiskCode in " +
                "(select riskcode from lmriskapp where riskperiod = 'L') and PayIntv <> 0 and exists ( select 1 from ljapayperson where PolNo = a.PolNo and ConfDate = '" +
                mToday + "' and LastPayToDate = a.Cvalidate + year('" + mToday +
                "' - a.Cvalidate) years)" +
                //提取因保全或理赔备份到B表的新单和增人的数据
                " union select a.PolNo ,'2' from LBPol a where a.edorno not like 'xb&' and ((cvalidate < signdate and signdate = '" +
                mToday + "') " + "or (cvalidate >= signdate and cvalidate = '" +
                mToday + "')) " +
                " and a.AppFlag in ('1','3') and a.ReinsureFlag = '4' and not exists (select 1 from lrpol where polno = a.polno and RenewCount = a.RenewCount)" +
                //提取因保全或理赔备份到B表的长险趸缴数据，情况比较复杂，需要判断提数日期在失效日期前。目前这种数据暂时没有，所以先不处理
                "union select a.PolNo,'3' from LBPol a where a.AppFlag in ('1','3') and a.ReinsureFlag = '4' and a.RiskCode in " +
                "(select riskcode from lmriskapp where riskperiod = 'L') and PayIntv = 0 and PayToDate > '" +
                mToday + "' and Month(CValidate) = Month('" + mToday +
                "') and day(CValidate)=day('" + mToday +
                "') and Year(CValidate) < Year('" + mToday + "') " +
                " and exists (select 1 from ljagetendorse c where c.FeeOperationType in ('WT','CT','ZT','XT') "+ // 09/06/17增加限制条件,只提取分保提数日期小于保全退保生效日期,090729协议退保类型
                " and FeeFinaType <> 'GB' and c.polno=a.polno and c.makedate>'" + mToday + "') "+ //长险趸交数据退保后不再提数分保
                //提取因保全或理赔备份到B表的长险非趸缴数据,提数日期：新的保单年度第一次缴费核销日期。
                " union select a.PolNo,'3' from LBPol a where a.AppFlag in ('1','3') and a.ReinsureFlag = '4' and a.RiskCode in " +
                "(select riskcode from lmriskapp where riskperiod = 'L') and PayIntv <> 0 and exists ( select 1 from ljapayperson where PolNo = a.PolNo and ConfDate = '" +
                mToday + "' and LastPayToDate = a.Cvalidate + year('" +
                mToday + "' - a.Cvalidate) years) "+
                " with ur ";
         //   System.out.println(tSql);
        SSRS tSSRS = new ExeSQL().execSQL(tSql);
        Reflections tReflections = new Reflections();

        //对新单和保单周年日的保单进行循环
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            mMap = new MMap();
            if (tSSRS.GetText(i, 2).equals("0") ||
                tSSRS.GetText(i, 2).equals("1")) {

                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tSSRS.GetText(i, 1));
                tLCPolDB.getInfo();
                LCPolSchema tLCPolSchema = tLCPolDB.getSchema();
                LRPolSchema tLRPolSchema = new LRPolSchema();
                tReflections.transFields(tLRPolSchema, tLCPolSchema);
                tLRPolSchema.setReNewCount(tLCPolSchema.getRenewCount()); //由于数据库的ReNewCount大小写问题,这里暂时单独处理,以后重新生成Schema
//                if(tSSRS.GetText(i, 2).equals("0")){
//                  this.mToday=tLCPolSchema.getSignDate();
//                }else{
//                  this.mToday = tLCPolSchema.getCValiDate();
//                }
                //获取成本中心，财务接口使用。
                String agentGroup = tLCPolSchema.getAgentGroup();
                if(agentGroup != null&& !agentGroup.equals(""))
                {
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                                 +agentGroup+"' )with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolSchema.setCostCenter(CostCenter);
                }

                //多家再保公司共保一个险种，得到此针对险种的所有再保合同
                String strSql = "select * from LRContInfo where Recontcode in " +
                                " (select distinct recontcode from LRcalfactorvalue where riskcode='" +
                                tLCPolSchema.getRiskCode() +
                                "') and ((Rinvalidate is not null and '" +
                                this.mToday +
                                "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
                                this.mToday +
                                "' >= Rvalidate)) order by Rvalidate desc with ur";
                LRContInfoSet tLRContInfoSet = new LRContInfoDB().executeQuery(
                        strSql);
                if (tLRContInfoSet == null || tLRContInfoSet.size() <= 0) {
                    errorLog(tLCPolSchema.getPolNo(), "没有得到适合该险种的再保合同");
                }
                for (int j = 1; j <= tLRContInfoSet.size(); j++) {
                    LRContInfoSchema tLRContInfoSchema = tLRContInfoSet.get(j);
                    mRecontCode = tLRContInfoSchema.getReContCode();
                    mRiskCalSort = getRiskCalSort(tLCPolSchema.getRiskCode(),
                                                  mRecontCode);
                    prepareLRPolData(tLRPolSchema, tLCPolSchema.getStandPrem(),
                                     tLCPolSchema.getPrem());
                    mMap.put(tLRPolSchema.getSchema(), "DELETE&INSERT");
                }
                //保单交费明细。
                if(!insertPolDetailData(tLCPolSchema.getPolNo())){
                    buildError("insertPolDetailData", "分保保单的交费数据时出错！");
                    return false;
                }
            } else {
                LBPolDB tLBPolDB = new LBPolDB();
                tLBPolDB.setPolNo(tSSRS.GetText(i, 1));
                tLBPolDB.getInfo();
                LBPolSchema tLBPolSchema = tLBPolDB.getSchema();
                LRPolSchema tLRPolSchema = new LRPolSchema();
                tReflections.transFields(tLRPolSchema, tLBPolSchema);
                tLRPolSchema.setReNewCount(tLBPolSchema.getRenewCount()); //由于数据库的ReNewCount大小写问题,这里暂时单独处理,以后重新生成Schema

                //获取成本中心，财务接口使用。
                String agentGroup = tLBPolSchema.getAgentGroup();
                if(agentGroup != null&& !agentGroup.equals(""))
                {
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"+agentGroup+"' )with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolSchema.setCostCenter(CostCenter);
                }


                //多家再保公司共保一个险种，得到此针对险种的所有再保合同号,合同生效日期,分保保费方式
                String strSql = "select * from LRContInfo where Recontcode in " +
                                " (select distinct recontcode from LRcalfactorvalue where riskcode='" +
                                tLBPolSchema.getRiskCode() +
                                "') and ((Rinvalidate is not null and '" +
                                this.mToday +
                                "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
                                this.mToday +
                                "' >= Rvalidate)) order by Rvalidate desc with ur";
                LRContInfoSet tLRContInfoSet = new LRContInfoDB().executeQuery(
                        strSql);
                if (tLRContInfoSet == null || tLRContInfoSet.size() <= 0) {
                    errorLog(tLBPolSchema.getPolNo(), "没有得到适合该险种的再保合同");
                }
                for (int j = 1; j <= tLRContInfoSet.size(); j++) {
                    LRContInfoSchema tLRContInfoSchema = tLRContInfoSet.get(j);
                    mRecontCode = tLRContInfoSchema.getReContCode();
                    mRiskCalSort = getRiskCalSort(tLBPolSchema.getRiskCode(),
                                                  mRecontCode);
                    prepareLRPolData(tLRPolSchema, tLBPolSchema.getStandPrem(),
                                     tLBPolSchema.getPrem());
                    mMap.put(tLRPolSchema.getSchema(), "DELETE&INSERT");
                }
                //保单交费明细。
                if(!insertPolDetailData(tLBPolSchema.getPolNo())){
                    buildError("insertPolDetailData","分保保单的交费数据时出错！");
                    return false;
                }

            }
            prepareOutputData();
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(this.mInputData, "")) {
                if (tPubSubmit.mErrors.needDealError()) {
                    buildError("insertPolData", "保存合同分保保单"+tSSRS.GetText(i+1,1)+"信息时出现错误!");
                }
                return false;
            }
        }
        return true;
    }

    /**
     * 提取本期合同分保保单的过往缴费信息 ConfDate < this.mStartDate ,并且由于提数的延迟性，部分数据可能被上期提取到数据库中，因此用Delete&Insert
     * @return boolean
     */
    private boolean insertPolDetailData(String PolNo) {
        String tSql =
                      "select PolNo,PayCount,PayNo,ConfDate,sum(SumActuPayMoney),LastPayToDate,CurPayToDate "+
                      "from LJAPayPerson a where  PayCount = 1  "+
                      "and exists (select 1 from lcpol where conttype='1' and a.polno='"+PolNo+"' "+
                      "and PolNo = a.PolNo and a.LastPayToDate >= Cvalidate and a.CurPayToDate <= EndDate) "+
                      "group by  polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate "+
                      "union all "+
                      "select PolNo,PayCount,PayNo,ConfDate,sum(SumActuPayMoney),LastPayToDate,CurPayToDate  "+
                      "from LJAPayPerson a where  PayCount = 1  "+
                      "and exists (select 1 from lbpol where conttype='1' and a.polno='"+PolNo+"' "+
                      "and PolNo = a.PolNo and a.LastPayToDate >= Cvalidate and a.CurPayToDate <= EndDate) "+
                      "group by  polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate "+
                      //团单,09年开始团单不可以续保,为了兼容lastpaytodate<cvalidate的情况，取消此条件限制
                      "union all  "+
                      "select PolNo,PayCount,PayNo,ConfDate,sum(SumActuPayMoney),LastPayToDate,CurPayToDate  "+
                      "from LJAPayPerson a where  PayCount = 1  "+
                      "and exists (select 1 from lcpol where conttype='2' and a.polno='"+PolNo+"'  "+
                      "and PolNo = a.PolNo ) "+
                      "and not exists (select 1 from ljagetendorse c where c.polno=a.polno and c.feeoperationtype = 'NI') "+
                      "group by  polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate "+
                      "union all  "+
                      "select PolNo,PayCount,PayNo,ConfDate,sum(SumActuPayMoney),LastPayToDate,CurPayToDate "+
                      "from LJAPayPerson a where  PayCount = 1  "+
                      "and exists (select 1 from lbpol where conttype='2' and a.polno='"+PolNo+"' "+
                      "and PolNo = a.PolNo )  "+
                      "and not exists (select 1 from ljagetendorse c where c.polno=a.polno and c.feeoperationtype = 'NI')  "+
                      "group by  polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate  "+
                      "union all "+
                      //增人,关联保全交费信息提取
                      "select a.PolNo,PayCount,PayNo,ConfDate,sum(x.getMoney),LastPayToDate,CurPayToDate  "+
                      "from LJAPayPerson a,ljagetendorse x where a.payno = x.actugetno and a.polno=x.polno and x.polno ='"+PolNo+"' "+
                      "and PayCount = 1 and x.feeoperationtype = 'NI' and a.riskcode=x.riskcode and "+
                      "(exists (select 1 from lcpol where polno= x.polno ) "+
                      " or exists (select 1 from lbpol where polno=x.polno)) "+
                      "group by a.polno,PayCount,PayNo,ConfDate,LastPayToDate,CurPayToDate   "+
                      "with ur ";

        SSRS tSSRS = new ExeSQL().execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//            mMap = new MMap();
            LRPolDetailSchema tLRPolDetailSchema = new LRPolDetailSchema();
            prepareDetailData(tLRPolDetailSchema, tSSRS.GetText(i, 1));
            tLRPolDetailSchema.setPayCount(tSSRS.GetText(i, 2));
            tLRPolDetailSchema.setPayNo(tSSRS.GetText(i, 3));
            if(tSSRS.GetText(i, 4)!=null && !tSSRS.GetText(i, 4).equals("")){
                tLRPolDetailSchema.setPayConfDate(tSSRS.GetText(i, 4));
            }else{
                tLRPolDetailSchema.setPayConfDate(PubFun.getCurrentDate());//如果ConfData为空，则置当前日期
            }
            tLRPolDetailSchema.setPayMoney(tSSRS.GetText(i, 5));
            tLRPolDetailSchema.setLastPayToDate(tSSRS.GetText(i, 6));
            tLRPolDetailSchema.setCurPayToDate(tSSRS.GetText(i, 7));
            tLRPolDetailSchema.setGetDataDate(tSSRS.GetText(i, 4)); //考虑到再保计算
            mMap.put(tLRPolDetailSchema, "DELETE&INSERT");
//            prepareOutputData();
//            PubSubmit tPubSubmit = new PubSubmit();
//            if (!tPubSubmit.submitData(this.mInputData, "")) {
//                if (tPubSubmit.mErrors.needDealError()) {
//                    buildError("insertPolDetailData", "保存合同分保缴费信息时出现错误!");
//                }
//                return false;
//            }
        }
        return true;
    }


    /**
     *
     * @return double
     */
    private boolean prepareLRPolData(LRPolSchema aLRPolSchema,
                                     double aStandPrem, double aPrem) {
        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        aLRPolSchema.setReContCode(this.mRecontCode); //再保合同代码
        aLRPolSchema.setReinsureItem("C"); //再保项目 L--法定分保 C--商业分保
        aLRPolSchema.setTempCessFlag("N"); //非临分
        aLRPolSchema.setRiskCalSort(this.mRiskCalSort); //险种计算代码
        aLRPolSchema.setGetDataDate(this.mToday);
        aLRPolSchema.setOperator(globalInput.Operator);
        aLRPolSchema.setMakeDate(currentDate);
        aLRPolSchema.setMakeTime(currentTime);
        aLRPolSchema.setModifyDate(currentDate);
        aLRPolSchema.setModifyTime(currentTime);
        double tAddPremRate = getPolAddPremRate(aStandPrem, aPrem); //得到加费率,
        aLRPolSchema.setDiseaseAddFeeRate(tAddPremRate);
        aLRPolSchema.setDeadAddFeeRate(tAddPremRate);

        return true;
    }

    /**
     * 计算死亡加费，重疾加费,这里由于契约不区分死亡加费和重疾加费，所以统一计算并存储于DiseaseAddFeeRate
     * @return double
     */
    private double getPolAddPremRate(double aStandPrem, double aPrem) {
        if (Math.abs(aStandPrem - 0.00) < 0.00001 || (aPrem - aStandPrem) <= 0) {
            return 0;
        }
        return (aPrem - aStandPrem) / aStandPrem;
    }

    private boolean prepareDetailData(LRPolDetailSchema aLRPolDetailSchema,
                                      String aPolNo) {
        PubFun tPubFun = new PubFun();
        Reflections tReflections = new Reflections();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(aPolNo);
        if (tLCPolDB.getInfo()) {
            tReflections.transFields(aLRPolDetailSchema, tLCPolDB.getSchema());
            aLRPolDetailSchema.setReNewCount(tLCPolDB.getRenewCount()); //由于数据库的ReNewCount大小写问题,这里暂时单独处理,以后重新生成Schema
        } else {
            LBPolDB tLBPolDB = new LBPolDB();
            tLBPolDB.setPolNo(aPolNo);
            tLBPolDB.getInfo();
            tReflections.transFields(aLRPolDetailSchema, tLBPolDB.getSchema());
            aLRPolDetailSchema.setReNewCount(tLBPolDB.getRenewCount()); //由于数据库的ReNewCount大小写问题,这里暂时单独处理,以后重新生成Schema
        }
        aLRPolDetailSchema.setRiskCalSort("0"); //废弃该字段,因此设置为默认值0
        aLRPolDetailSchema.setOperator(globalInput.Operator);
        aLRPolDetailSchema.setMakeDate(currentDate);
        aLRPolDetailSchema.setMakeTime(currentTime);
        aLRPolDetailSchema.setModifyDate(currentDate);
        aLRPolDetailSchema.setModifyTime(currentTime);
        return true;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(String aPolNo, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aPolNo);
        tLRErrorLogSchema.setLogType("2");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }

    /**
     * 得到特殊佣金比例
     * @param aLCPolSchema LCPolSchema
     * @param recontCode String
     * @return String
     */
    private String getSpeCemmRate(LCPolSchema aLCPolSchema, String recontCode) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                recontCode +
                "' and upper(factorcode)='SPECOMRATE' and riskcode='" +
                aLCPolSchema.getRiskCode() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL(); //用于sql查询
        tSSRS = tExeSQL.execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return "0";
        }
        String[][] tempRDate = tSSRS.getAllData(); //所有再保合同生效日期
        System.out.println("sql: " + strSQL);
        System.out.println(
                "得到特殊佣金比例:.................................................. " +
                tempRDate[0][0]);
        if (tempRDate[0][0].equals("") || tempRDate[0][0] == null) {
            return "0";
        }
        return tempRDate[0][0];
    }

    /**
     * 得到再保手续费比例
     * @param aLCPolSchema LCPolSchema
     * @param recontCode String
     * @return String
     */
    private String getReProcFeeRate(LCPolSchema aLCPolSchema, String recontCode) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                recontCode +
                "' and upper(factorcode)='REINPROCFEERATE' and riskcode='" +
                aLCPolSchema.getRiskCode() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL(); //用于sql查询
        tSSRS = tExeSQL.execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return "0";
        }
        String[][] tempRDate = tSSRS.getAllData(); //所有再保合同生效日期
        System.out.println("sql: " + strSQL);
        System.out.println(
                "得到再保手续费比例:.................................................. " +
                tempRDate[0][0]);
        if (tempRDate[0][0].equals("") || tempRDate[0][0] == null) {
            return "0";
        }
        return tempRDate[0][0];
    }

    /**
     * 得到选择折扣比例
     * @param aLCPolSchema LCPolSchema
     * @param recontCode String
     * @return String
     */
    private String getChoiRebaFeeRate(LCPolSchema aLCPolSchema,
                                      String recontCode) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                recontCode +
                "' and upper(factorcode)='CHOIREBARATE' and riskcode='" +
                aLCPolSchema.getRiskCode() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL(); //用于sql查询
        tSSRS = tExeSQL.execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return "0";
        }
        String[][] tempRDate = tSSRS.getAllData(); //所有再保合同生效日期
        System.out.println("sql: " + strSQL);
        System.out.println(
                "得到选择折扣比例:.................................................. " +
                tempRDate[0][0]);
        if (tempRDate[0][0].equals("") || tempRDate[0][0] == null) {
            return "0";
        }
        return tempRDate[0][0];
    }

    /**
     * 得到管理费比例
     * @param aLCPolSchema LCPolSchema
     * @param recontCode String
     * @return String
     */
    private String getManageFeeRate(LCPolSchema aLCPolSchema, String recontCode) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                recontCode +
                "' and upper(factorcode)='REINMANFEERATE' and riskcode='" +
                aLCPolSchema.getRiskCode() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL(); //用于sql查询
        tSSRS = tExeSQL.execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return "0";
        }
        String[][] tempRDate = tSSRS.getAllData(); //所有再保合同生效日期
        System.out.println("sql: " + strSQL);
        System.out.println(
                "得到管理费比例:.................................................. " +
                tempRDate[0][0]);
        if (tempRDate[0][0].equals("") || tempRDate[0][0] == null) {
            return "0";
        }
        return tempRDate[0][0];
    }

    /**
     * 得到盈余佣金比例
     * @param aLCPolSchema LCPolSchema
     * @param recontCode String
     * @return String
     */
    private String getProfitCommRate(LCPolSchema aLCPolSchema,
                                     String recontCode) {
        String strSQL =
                "select factorvalue from lrcalfactorvalue where recontcode='" +
                recontCode +
                "' and upper(factorcode)='PROFITCOM' and riskcode='" +
                aLCPolSchema.getRiskCode() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL(); //用于sql查询
        tSSRS = tExeSQL.execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return "0";
        }
        String[][] tempRDate = tSSRS.getAllData(); //所有再保合同生效日期
        System.out.println("sql: " + strSQL);
        System.out.println(
                "得到盈余佣金比例:.................................................. " +
                tempRDate[0][0]);
        if (tempRDate[0][0].equals("") || tempRDate[0][0] == null) {
            return "0";
        }
        return tempRDate[0][0];
    }

    /**
     * 得到险种类别
     * @return boolean
     */
    private String getRiskCalSort(String aRiskCode, String aLRContCode) {
        String strSQL =
                "select distinct risksort from LRCalFactorValue where riskcode='" +
                aRiskCode + "' and ReContCode = '" +
                aLRContCode + "'";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
            buildError("insertData", "没有该险种的险种类别!");
        }
        return tSSRS.GetText(1, 1);
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

//    private void deletePolDetial() {
//        String delPolDetialSql = "select * from LRPolDetail a where exists (select 1 from lcgrpcont where grpcontno = a.grpcontno and " +
//                                 " markettype = '2')";
//        RSWrapper rswrapper = new RSWrapper();
//        LRPolDetailSet tLRPolDetailSet = new LRPolDetailSet();
//        rswrapper.prepareData(tLRPolDetailSet, delPolDetialSql);
//
//        try {
//            do {
//                rswrapper.getData();
//                for (int i = 0; i < tLRPolDetailSet.size(); i++) {
//                    LRPolDetailSchema tLRPolDetailSchema = tLRPolDetailSet.get(
//                            i + 1);
//                    mMap = new MMap();
//                    mMap.put(tLRPolDetailSchema, "DELETE");
//                    mInputData.clear();
//                    mInputData.add(mMap);
//                    PubSubmit mPubSubmit = new PubSubmit();
//                    mPubSubmit.submitData(this.mInputData, "DELETE");
//                }
//            } while (tLRPolDetailSet.size() > 0);
//            rswrapper.close();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            rswrapper.close();
//        }
//    }

    public static void main(String[] args) {
        LRGetCessDataBL tLRGetCessDataBL = new LRGetCessDataBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0001";
        String mOperateType = "CESSDATA";

        String startDate = "2009-8-31";
        String endDate = "2009-8-31";
        TransferData getCessData = new TransferData();
        getCessData.setNameAndValue("StartDate", startDate);
        getCessData.setNameAndValue("EndDate", endDate);

        VData tVData = new VData();
        tVData.addElement(globalInput);
        tVData.addElement(getCessData);

        tLRGetCessDataBL.submitData(tVData, mOperateType);
    }
}
