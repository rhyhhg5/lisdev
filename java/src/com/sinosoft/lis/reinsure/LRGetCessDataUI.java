package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.*;
import com.sinosoft.lis.reinsure.*;

/*
 * <p>ClassName: ReComManageUI </p>
 * <p>Description: ReComManageUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Zhang Bin
 * @CreateDate：2006-07-30
 */
public class LRGetCessDataUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private String mResult = new String();

    public LRGetCessDataUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("come in UI........");
        LRGetCessDataBL tLRGetCessDataBL = new LRGetCessDataBL();
        if (!tLRGetCessDataBL.submitData(cInputData, cOperate))
        {
            if (tLRGetCessDataBL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLRGetCessDataBL.mErrors);
            }
            else
            {
                buildError("submitData", "tCardPlanBL发生错误，但是没有提供详细信息！");
            }
            return false;
        }
         mResult = tLRGetCessDataBL.getResult();
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        // 准备传输数据 VData
        VData vData = new VData();
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        return true;
    }

    public String getResult()
    {
        return this.mResult;
    }

    /*
     * add by kevin
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardPlanUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
