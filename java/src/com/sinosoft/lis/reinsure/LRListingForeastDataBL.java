package com.sinosoft.lis.reinsure;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LRAccountSchema;
import com.sinosoft.lis.vschema.LRAccountSet;

public class LRListingForeastDataBL {
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null;
	// private String mtype = null;
	private String mYear ="";
	private String mMonth ="";
	private VData mOutputData = new VData();
	private LRAccountSet mLRAccountSet = new LRAccountSet();
	private String mCurDate = PubFun.getCurrentDate();
	private String mCurTime = PubFun.getCurrentTime();
	private MMap mMap = new MMap();
	private ExeSQL mExeSQL = new ExeSQL();
	private String mStartDate ="";
	TransferData tf = null;
	public LRListingForeastDataBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
		    CError tError = new CError();
            tError.moduleName = "LRListingForeastDataBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LRListingForeastDateBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
		}
//		if (!prepareOutputData())
//        {
//            return false;
//        }
//		 PubSubmit tPubSubmit = new PubSubmit();
//	     if(!tPubSubmit.submitData(mOutputData, "")){
//	    	 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//             CError tError = new CError();
//             tError.moduleName = "LRListingForeastDateBL";
//             tError.functionName = "submitData";
//             tError.errorMessage = "数据提交失败!";
//             this.mErrors.addOneError(tError);
//             return false;
//	     }
	     
		return true;
	}

	/**
	 * 校验操作是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {

		return true;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		String quarter = "";
		int mon = Integer.parseInt(mMonth);
    	if(mon<=3){
    		quarter = "01";
    	}else if(mon>3&&mon<=6){
    		quarter = "02";;
    	}else if (mon>6&&mon<=9){
    		quarter = "03";
    	}else{
    		quarter ="04";
    	}
    	String strWhere =" and getdatadate between '"+mStartDate+"' and Date('"+mYear+"-"+mMonth+"-01')+1 months -1 days ";
		String tSQL2 = "select COALESCE(lraccount.recontcode, list.recontcode) as recontcode,"
				+ "COALESCE(lraccount.riskcode, list.riskcode) as riskcode,"
				+ "COALESCE(lraccount.grpcontno, list.grpcontno) as grpcontno,"
				+ "'00000000000000000000' contno,'00' as DATATYPE,'1' as IMPORTCOUNT,'"+mYear+"' as BELONGYEAR,'' as BELONGQUARTER,'"+mMonth+"' as BELONGMONTH,(Date('"+mYear+"-'||'"+mMonth+"-01')+1 months -1 days) as FINACCOUNTDATE,"
		        +" (nvl(list.cessprem,0) - nvl(lraccount.cessprem,0)) as CESSPREM,'00' as CESSPREMSTATE,(nvl(list.REPROCFEE,0) - nvl(lraccount.REPROCFEE,0)) as REPROCFEE,'00' as REPROCFEESTATE, (nvl(list.CLAIMBACKFEE ,0)- nvl(lraccount.CLAIMBACKFEE,0)) as CLAIMBACKFEE,  '00' as CLAIMBACKFEESTATE," 
		        +" COALESCE(lraccount.ManageCom, list.ManageCom) as ManageCom,"
		        +" COALESCE(lraccount.CostCenter, list.CostCenter) as CostCenter,"
		        +" COALESCE(lraccount.markettype, list.markettype) as markettype,"
		        +" COALESCE(lraccount.TempCessFlag, list.TempCessFlag) as TempCessFlag,"
		        +"'' as BRANCHNO,0 as ACCOUNTMONEY,null as ACCOUNTDATE,'' as ACCOUNTOPRATER,'001' as OPRATER, '2015-01-01'   MAKEDATE, '00:00:02'  MAKETIME, '2015-01-01'   MODIFYDATE,'00:00:02'  MODIFYTIME,'' as LRSERIALNO"
		        +" from ("  +
		        "select managecom,riskcode,recontcode,costcenter,grpcontno"+
				" ,sum(cessprem) as cessprem,sum(reprocfee) as reprocfee,sum(claimbackfee) as claimbackfee "+
				" ,tempcessflag, markettype from  " +
				" (select managecom,riskcode,recontcode,costcenter,(case grpcontno when '00000000000000000000' then contno else grpcontno end) as grpcontno,cessprem as cessprem,reprocfee as  " +
				" reprocfee,0 as claimbackfee,(case when TempCessFlag ='合同分保' then 'C' when TempCessFlag='临时分保' then 'T' end)  as tempcessflag,(case when nvl(markettype,'A9') ='' then 'A9' else nvl(markettype,'A9') end ) as markettype from lrcesslist  where  1=1" +strWhere+
		 		"  and SUBSTR(costcenter,5,2)='93'  "  +
				" union all " + 
				"select  managecom,riskcode,recontcode,costcenter,(case grpcontno when '00000000000000000000' then contno else grpcontno end) as grpcontno,0 as cessprem," +
				" 0 as reprocfee,claimbackfee" +
				" as claimbackfee,(case when TempCessFlag ='合同分保' then 'C' when TempCessFlag='临时分保' then 'T' end)  as tempcessflag,(case when nvl(markettype,'A9') ='' then 'A9' else nvl(markettype,'A9') end ) as markettype  from lrclaimlist where 1=1 "+strWhere+
				" and SUBSTR(costcenter,5,2)='93'   "+
	 	 		" union all"+
				" select managecom,riskcode,recontcode,costcenter,'00000000000000000000' as grpcontno,cessprem as cessprem,"+ 
				"reprocfee as reprocfee,0 as claimbackfee,(case when TempCessFlag ='合同分保' then 'C' when TempCessFlag='临时分保' then 'T' end)  as tempcessflag,(case when nvl(markettype,'A9') ='' then 'A9' else nvl(markettype,'A9') end ) as markettype from lrcesslist  where 1=1 "+strWhere+
		 		" and SUBSTR(costcenter,5,2)<>'93'  "+
				" union all "+ 
				"select  managecom,riskcode,recontcode,costcenter,'00000000000000000000' as grpcontno,0 as cessprem," +
				" 0 as reprocfee,claimbackfee as claimbackfee,(case when TempCessFlag ='合同分保' then 'C' when TempCessFlag='临时分保' then 'T' end)  as tempcessflag,(case when nvl(markettype,'A9') ='' then 'A9' else nvl(markettype,'A9') end ) as markettype from lrclaimlist where 1=1 "+strWhere+
				" and SUBSTR(costcenter,5,2)<>'93'  ) "+
				" as aa  group by   recontcode,riskcode,costcenter,grpcontno,managecom,tempcessflag,markettype) list "
				+ " full outer join "
				+ " (select managecom,riskcode,recontcode,costcenter,tempcessflag,grpcontno,markettype,"
				+ " sum(cessprem) as cessprem,sum(reprocfee) as reprocfee ,sum(claimbackfee) as claimbackfee   "
				+ " from (select managecom,riskcode,recontcode,costcenter,tempcessflag,grpcontno,(case when nvl(markettype,'A9') ='' then 'A9' else nvl(markettype,'A9') end ) as markettype,cessprem,reprocfee,claimbackfee"
				+ " from db2inst1.lraccount bb where datatype ='00' and belongyear||belongmonth >='"+mYear+"'||'01' and belongyear||belongmonth <'"+mYear+mMonth+"') detail  "
				+ " group by   recontcode,riskcode,costcenter,managecom,tempcessflag,markettype ,grpcontno)  lraccount "
				+ " on  list.managecom = lraccount.managecom "
				+ " and list.riskcode = lraccount.riskcode "
				+ " and list.recontcode = lraccount.recontcode "
				+ " and list.costcenter = lraccount.costcenter "
				+ " and list.grpcontno = lraccount.grpcontno "
				+ " and list.tempcessflag = lraccount.tempcessflag "
				+ " and list.markettype =lraccount.markettype with ur";
		       
		RSWrapper tRSWrapper = new RSWrapper();
		LRAccountSet tLRAccountSet = new LRAccountSet();
		tRSWrapper.prepareData(tLRAccountSet, tSQL2);
		do
		{
			LRAccountSet aLRAccountSet = new LRAccountSet();
			tLRAccountSet = (LRAccountSet) tRSWrapper.getData();
			for(int i =1;i<=tLRAccountSet.size();i++)
			{
				LRAccountSchema tLRAccountSchema = new LRAccountSchema();
				tLRAccountSchema = tLRAccountSet.get(i);
				if(tLRAccountSchema.getCessPrem()==0 && tLRAccountSchema.getReProcFee()==0&& tLRAccountSchema.getClaimBackFee()==0 )
				{
					continue;
				}
				tLRAccountSchema.setBelongQuarter(quarter);
				if("A9".equals(tLRAccountSchema.getMarketType()))
				{
					tLRAccountSchema.setMarketType("");
				}
				String aTypeNo = "LR"+mCurDate.substring(0,4)+mCurDate.substring(5,7)+mCurDate.substring(8,10);
        		String aLRSerialNo =aTypeNo + PubFun1.CreateMaxNo(aTypeNo, 7);
				tLRAccountSchema.setLRSerialNo(aLRSerialNo);
//				System.out.println(tLRAccountSchema.getCessPrem()+"____"+tLRAccountSchema.getReProcFee()+"----"+tLRAccountSchema.getClaimBackFee());
//				String tReContCode = tLRAccountSchema.getReContCode();
//				String tManageCom = tLRAccountSchema.getManageCom();
//				String tGrpContNo = tLRAccountSchema.getGrpContNo();
//				String tCostCenter = tLRAccountSchema.getCostCenter();
//				String tTempCessFlag = tLRAccountSchema.getTempCessFlag();
//				String tMarketType = tLRAccountSchema.getMarketType();
//				System.out.println(tGrpContNo);
//				if("".equals(tGrpContNo)||null==tGrpContNo)
//				{
//					tGrpContNo = "00000000000000000000";
//				}
//				tLRAccountSchema.setGrpContNo(tGrpContNo);
//				String tContNo = tLRAccountSchema.getContNo();
//				if("".equals(tContNo)||null==tContNo)
//				{
//					tContNo = "00000000000000000000";
//				}
//				
//				tLRAccountSchema.setContNo(tContNo);
//				String tRiskCode = tLRAccountSchema.getRiskCode();
//				String tSql ="select coalesce(sum(cessprem),0),coalesce(sum(reprocfee),0),coalesce(sum(claimbackfee),0) from lraccount where managecom ='"+tManageCom+"'" +
//						" and recontcode ='"+tReContCode+"' and riskcode ='"+tRiskCode+"' and grpcontno ='"+tGrpContNo+"'  and datatype='00'" +
//						" and CostCenter='" +tCostCenter+"' and TempCessFlag='"+tTempCessFlag+"' " ;
//				if("".equals(tMarketType)||null==tMarketType)
//				{
//					tSql+="and ( MarketType is null or MarketType='') ";
//				}
//				else
//				{
//					tSql+="and MarketType='"+tMarketType+"' ";
//				}
//				tSql+=" and belongyear||belongmonth >='"+mYear+"'||'01' and belongyear||belongmonth <'"+mYear+mMonth+"' with ur";
//				SSRS tSSRS = new SSRS();
//				tSSRS = mExeSQL.execSQL(tSql);
//				if(0<tSSRS.getMaxRow())
//				{
//					double tCessprem = 0;
//					double tReprocfee =0;
//					double tClaimBackFee =0;
//					if(null!=tSSRS.GetText(1, 1)&&!"".equals(tSSRS.GetText(1, 1)))
//					{
//						tCessprem = tLRAccountSchema.getCessPrem()-Double.parseDouble(tSSRS.GetText(1, 1));
//						tLRAccountSchema.setCessPrem(tCessprem);
//					}
//					if(null!=tSSRS.GetText(1, 2)&&!"".equals(tSSRS.GetText(1, 2)))
//					{
//						tReprocfee = tLRAccountSchema.getReProcFee()-Double.parseDouble(tSSRS.GetText(1, 2));
//						tLRAccountSchema.setReProcFee(tReprocfee);
//					}
//					if(null!=tSSRS.GetText(1, 3)&&!"".equals(tSSRS.GetText(1, 3)))
//					{
//						tClaimBackFee = tLRAccountSchema.getClaimBackFee()-Double.parseDouble(tSSRS.GetText(1, 3));
//						tLRAccountSchema.setClaimBackFee(tClaimBackFee);
//					}
//					if(tCessprem==0 && tReprocfee==0&& tClaimBackFee==0 )
//					{
//						continue;
//					}
//				}
				
        		tLRAccountSchema.setOprater(mGI.Operator);
        		tLRAccountSchema.setMakeDate(PubFun.getCurrentDate());
        		tLRAccountSchema.setMakeTime(PubFun.getCurrentTime());
        		tLRAccountSchema.setModifyDate(PubFun.getCurrentDate());
        		tLRAccountSchema.setModifyTime(PubFun.getCurrentTime());
				aLRAccountSet.add(tLRAccountSchema);
			}
			mMap = new MMap();
			mMap.put(aLRAccountSet, "INSERT");
			this.mOutputData.clear();
            this.mOutputData.add(this.mMap);
            PubSubmit tPubSubmit = new PubSubmit();
   	     	if(!tPubSubmit.submitData(mOutputData, "")){
   	    	 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LRListingForeastDateBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
   	     	}
		}while(tLRAccountSet.size()>0);
//		ExeSQL tExeSQL = new ExeSQL();
//		SSRS tSSRS = tExeSQL.execSQL(tSQL2);
//		if (tExeSQL.mErrors.needDealError()) {
//			System.out.println(tExeSQL.mErrors.getErrContent());
//
//			CError tError = new CError();
//			tError.moduleName = "LRListingForeastDataBL";
//			tError.functionName = "dealData";
//			tError.errorMessage = "没有查询";
//			mErrors.addOneError(tError);
//			System.out.println(tError.errorMessage);
//			return false;
//		}
//		
//		String currentdate=PubFun.getCurrentDate();
//    	String currenttime=PubFun.getCurrentTime();
//    	String operator= mGI.Operator;
//    	String year = (String) tf.getValueByName("year");
//    	String month = (String) tf.getValueByName("month");
//    	
//		
//		if (tSSRS != null || tSSRS.MaxRow > 0) {
//			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//				LRAccountSchema tLRAccountSchema = new LRAccountSchema();
//				String aReContCode      = tSSRS.GetText(i,3);
//        		String aRiskCode        = tSSRS.GetText(i,2);
//        		String aGrpContNo       = tSSRS.GetText(i,11);
//        		String aContNo          = tSSRS.GetText(i,12);
//        		String aTempCessFlag   = "";
//        		String aDataType        = "00";
//        		String aBelongYear      = year;
//        		String aBelongQuarter   = quarter;
//        		String aBelongMonth     = month;
//        		String aFinAccountDate  = currentdate;
//        		String aCessPrem        = tSSRS.GetText(i,7);
//        		String aCessPremState   = "00";
//        		String aReProcFee       = tSSRS.GetText(i,8);
//        		String aReProcFeeState  = "00";
//        		String aClaimBackFee    = tSSRS.GetText(i,9);
//        		String aClaimBackFeeState = "00";
//        		String aManageCom       = tSSRS.GetText(i,1);
//        		String aCostCenter      = tSSRS.GetText(i,4);
//        		String aMarketType      = tSSRS.GetText(i,13);
//        		String tTempCessFlag    = tSSRS.GetText(i,10);
//        		if(tTempCessFlag.equals("合同分保")){
//        			aTempCessFlag = "C";
//        		}
//        		if(tTempCessFlag.equals("临时分保")){
//        			aTempCessFlag.equals("T");
//        		}
//        		String aOprater         = operator;
//        		String aMakeDate        = currentdate;
//        		String aMakeTime        = currenttime;
//        		String aModifyDate      = currentdate;
//        		String aModifyTime      = currenttime;
//        		int   aImportCount      = 1;
//        		String aTypeNo = "LR"+mCurDate.substring(0,4)+mCurDate.substring(5,7)+mCurDate.substring(8,10);
//        		String aLRSerialNo =aTypeNo + PubFun1.CreateMaxNo(aTypeNo, 7);
//        		
//        		tLRAccountSchema.setReContCode(aReContCode);
//        		tLRAccountSchema.setRiskCode(aRiskCode);
//        		tLRAccountSchema.setGrpContNo(aGrpContNo);
//        		tLRAccountSchema.setContNo(aContNo);
//        		tLRAccountSchema.setDataType(aDataType);
//        		tLRAccountSchema.setBelongYear(aBelongYear);
//        		tLRAccountSchema.setBelongQuarter(aBelongQuarter);
//        		tLRAccountSchema.setBelongMonth(aBelongMonth);
//        		tLRAccountSchema.setFinAccountDate(aFinAccountDate);
//        		tLRAccountSchema.setCessPrem(aCessPrem);
//        		tLRAccountSchema.setCessPremState(aCessPremState);
//        		tLRAccountSchema.setReProcFee(aReProcFee);
//        		
//        		tLRAccountSchema.setReProcFeeState(aReProcFeeState);
//        		tLRAccountSchema.setClaimBackFee(aClaimBackFee);
//        		tLRAccountSchema.setClaimBackFeeState(aClaimBackFeeState);
//        		tLRAccountSchema.setManageCom(aManageCom);
//        		tLRAccountSchema.setCostCenter(aCostCenter);
//        		tLRAccountSchema.setMarketType(aMarketType);
//        		tLRAccountSchema.setTempCessFlag(aTempCessFlag);
//        		tLRAccountSchema.setOprater(aOprater);
//        		tLRAccountSchema.setMakeDate(aMakeDate);
//        		tLRAccountSchema.setMakeTime(aMakeTime);
//        		tLRAccountSchema.setModifyDate(aModifyDate);
//        		tLRAccountSchema.setModifyTime(aModifyTime);
//        		tLRAccountSchema.setImportCount(aImportCount);
//        		tLRAccountSchema.setLRSerialNo(aLRSerialNo);
//        		mLRAccountSet.add(tLRAccountSchema);       		
//			}
//			mMap.put(mLRAccountSet, "INSERT");
//		}
		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		 tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);
		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "LAStandardRCDownloadBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		mYear = (String) tf.getValueByName("year");
		mMonth = (String) tf.getValueByName("month");
		mStartDate =mYear+"-1-1";
		if (mYear == null || mMonth==null) {
			CError tError = new CError();
			tError.moduleName = "LAStandardRCDownloadBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		return true;
	}
private boolean prepareOutputData()
	  {
	      try
	       {
//	        	tmap.put(this.mLRCessListSet, "INSERT");
	        	this.mOutputData.clear();
	            this.mOutputData.add(this.mMap);
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
	            return false;
	        }
	        return true;
	    }
	public static void main(String[] args) {
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("year","2015");
			tTransferData.setNameAndValue("month","04");
			GlobalInput tG = new GlobalInput();
			tG.Operator="001";
		  // 准备传输数据 VData
		  	VData tVData = new VData();
			tVData.add(tTransferData);
		  	tVData.add(tG);
		  	LRListingForeastDataBL tLRListingForeastDataBL = new LRListingForeastDataBL();
		  	tLRListingForeastDataBL.submitData(tVData, "");
	}
}
