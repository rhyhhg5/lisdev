/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: huxl
 * @CreateDate：2007-08-27
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

public class GrpTempCessContBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private TransferData grpCessData = new TransferData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mRiskCalSort = "";


    private LRTempCessContSchema mLRTempCessContSchema = new
            LRTempCessContSchema();
    private MMap mMap = new MMap();

    private PubSubmit tPubSubmit = new PubSubmit();


    //业务处理相关变量
    /** 全局数据 */

    public GrpTempCessContBL() {
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        GrpTempCessContBL tGrpTempCessContBL = new GrpTempCessContBL();
        try {
            vData.add(globalInput);
            tGrpTempCessContBL.submitData(vData, "INSERT");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (!insertData()) {
            // @@错误处理
            buildError("dealData", "团体保单临分处理失败!");
            return false;
        }
        return true;
    }

    /**
     * insertData
     * @return boolean
     */
    private boolean insertData() {
        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();
        Reflections tReflections = new Reflections();
        //获取团体临分结论信息
        String strProposalContNo = (String) grpCessData.getValueByName(
                "ProposalContNo");
        String strReComCode = (String) grpCessData.getValueByName("ReComCode");
        String strRetainAmnt = (String) grpCessData.getValueByName("RetainAmnt"); //自留额
        String strCessionRate = (String) grpCessData.getValueByName(
                "CessionRate"); //分保比例
        String strGrpProposalNo = (String) grpCessData.getValueByName(
                "GrpProposalNo");
        String strReinProcFeeRate = (String) grpCessData.getValueByName(
                "ReinProcFeeRate"); //再保手续费
        String strReUWConlcusion = (String) grpCessData.getValueByName(
                "ReUWConlcusion"); //再保审核结论
        String strTempCessConclusion = (String) grpCessData.getValueByName(
                "TempCessConclusion"); //临分结论

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpProposalNo(strGrpProposalNo); //得到团体险种保单
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        if (tLCGrpPolSet.size() <= 0) {
            buildError("insertData", "没有此团体保单!");
            return false;
        }

        if (this.strOperate.equals("INSERT")) {
            LRTempCessContDB tLRTempCessContDB = new LRTempCessContDB();
            tLRTempCessContDB.setReContCode(mLRTempCessContSchema.
                                            getReContCode()); //向临分协议总表插入数据
            if (tLRTempCessContDB.getInfo()) {
                buildError("insertData", "该临分协议已经存在!");
                return false;
            }
            //判断是否有临分申请的保单(ReinsureFlag="2")
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setGrpPolNo(tLCGrpPolSet.get(1).getGrpPolNo());
            tLCPolDB.setReinsureFlag("2");
            LCPolSet tLCPolSet = tLCPolDB.query(); //得到LCGrpPol下所有临分的保单
            if (tLCPolSet.size() <= 0) {
                buildError("insertData", "没有临分申请的保单!");
                return false;
            }
            if (strTempCessConclusion.equals("3")) {
                mLRTempCessContSchema.setManageCom(globalInput.ComCode);
                mLRTempCessContSchema.setOperator(globalInput.Operator);
                mLRTempCessContSchema.setMakeDate(currentDate);
                mLRTempCessContSchema.setMakeTime(currentTime);
                mLRTempCessContSchema.setModifyDate(currentDate);
                mLRTempCessContSchema.setModifyTime(currentTime);
                this.mMap.put(mLRTempCessContSchema, "INSERT"); //插入新的临分合同记录

                for (int i = 1; i <= tLCPolSet.size(); i++) {
                    LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                    LRPolSchema tLRPolSchema = new LRPolSchema();
                    tLCPolSchema.setReinsureFlag("3");
                    tReflections.transFields(tLRPolSchema, tLCPolSchema);
                    prepareData(tLRPolSchema, tLCPolSchema);
                    tLRPolSchema.setReNewCount(tLCPolSchema.getRenewCount());

                    LRTempCessContInfoSchema tLRTempCessContInfoSchema = new
                            LRTempCessContInfoSchema();
                    tLRTempCessContInfoSchema.setReComCode(strReComCode);
                    tLRTempCessContInfoSchema.setReContCode(
                            mLRTempCessContSchema.getReContCode());
                    tLRTempCessContInfoSchema.setRiskCode(tLCPolSchema.
                            getRiskCode());
                    tLRTempCessContInfoSchema.setRetainAmnt(strRetainAmnt);
                    tLRTempCessContInfoSchema.setCessionRate(strCessionRate);
                    tLRTempCessContInfoSchema.setReinProcFeeRate(
                            strReinProcFeeRate);
                    tLRTempCessContInfoSchema.setProposalContNo(tLCPolSchema.
                            getProposalContNo());
                    tLRTempCessContInfoSchema.setProposalNo(tLCPolSchema.
                            getProposalNo());
                    tLRTempCessContInfoSchema.setInsuredNo(tLCPolSchema.
                            getInsuredNo());
                    tLRTempCessContInfoSchema.setReUWConlcusion(
                            strReUWConlcusion);
                    tLRTempCessContInfoSchema.setCessFeeMode("C");
                    tLRTempCessContInfoSchema.setManageCom(globalInput.ComCode);
                    tLRTempCessContInfoSchema.setOperator(globalInput.Operator);
                    tLRTempCessContInfoSchema.setMakeDate(currentDate);
                    tLRTempCessContInfoSchema.setMakeTime(currentTime);
                    tLRTempCessContInfoSchema.setModifyDate(currentDate);
                    tLRTempCessContInfoSchema.setModifyTime(currentTime);

                    this.mMap.put(tLRPolSchema, "INSERT"); //添加LRPOl记录
                    this.mMap.put(tLRTempCessContInfoSchema, "INSERT");
                    this.mMap.put(tLCPolSchema, "UPDATE");
                }
            } else {
                for (int i = 1; i <= tLCPolSet.size(); i++) {
                    LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                    tLCPolSchema.setReinsureFlag(strTempCessConclusion);
                    this.mMap.put(tLCPolSchema, "UPDATE");
                }
            }
        } else if (this.strOperate.equals("UPDATE")) {
            //如果是update则查出reinsureflag为'3','4','5'的LCPol
            String strSQL = "select * from LCPol where GrpPolNo='" +
                            tLCGrpPolSet.get(1).getGrpPolNo() +
                            "' and ReinsureFlag in ('3','4','5')";
            LCPolSet tLCPolSet = new LCPolDB().executeQuery(strSQL); //得到LCGrpPol下需修改的保单
            if (tLCPolSet.size() <= 0) {
                buildError("insertData", "没有需临分处理的保单!");
                return false;
            }
            if (strTempCessConclusion.equals("3")) {
                for (int i = 1; i <= tLCPolSet.size(); i++) {
                    LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                    LRPolSchema tLRPolSchema = new LRPolSchema();
                    tLCPolSchema.setReinsureFlag("3");
                    tReflections.transFields(tLRPolSchema, tLCPolSchema);
                    prepareData(tLRPolSchema, tLCPolSchema);
                    tLRPolSchema.setReNewCount(tLCPolSchema.getRenewCount());

                    LRTempCessContInfoSchema tLRTempCessContInfoSchema = new
                            LRTempCessContInfoSchema();
                    tLRTempCessContInfoSchema.setReComCode(strReComCode);
                    tLRTempCessContInfoSchema.setReContCode(
                            mLRTempCessContSchema.getReContCode());
                    tLRTempCessContInfoSchema.setRiskCode(tLCPolSchema.
                            getRiskCode());
                    tLRTempCessContInfoSchema.setRetainAmnt(strRetainAmnt);
                    tLRTempCessContInfoSchema.setCessionRate(strCessionRate);
                    tLRTempCessContInfoSchema.setReinProcFeeRate(
                            strReinProcFeeRate);
                    tLRTempCessContInfoSchema.setProposalContNo(tLCPolSchema.
                            getProposalContNo());
                    tLRTempCessContInfoSchema.setProposalNo(tLCPolSchema.
                            getProposalNo());
                    tLRTempCessContInfoSchema.setInsuredNo(tLCPolSchema.
                            getInsuredNo());
                    tLRTempCessContInfoSchema.setReUWConlcusion(
                            strReUWConlcusion);
                    tLRTempCessContInfoSchema.setCessFeeMode("C");
                    tLRTempCessContInfoSchema.setManageCom(globalInput.ComCode);
                    tLRTempCessContInfoSchema.setOperator(globalInput.Operator);
                    tLRTempCessContInfoSchema.setMakeDate(currentDate);
                    tLRTempCessContInfoSchema.setMakeTime(currentTime);
                    tLRTempCessContInfoSchema.setModifyDate(currentDate);
                    tLRTempCessContInfoSchema.setModifyTime(currentTime);

                    this.mMap.put(tLRPolSchema, "DELETE&INSERT"); //添加LRPOl记录
                    this.mMap.put(tLRTempCessContInfoSchema, "DELETE&INSERT");
                    this.mMap.put(tLCPolSchema, "UPDATE");
                }
            } else {
                for (int i = 1; i <= tLCPolSet.size(); i++) {
                    LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                    tLCPolSchema.setReinsureFlag(strTempCessConclusion);
                    this.mMap.put(tLCPolSchema, "UPDATE");
                    //下面删除曾经临分的保单LRPol LRTempCessContInfo
                    LRPolDB tLRPolDB = new LRPolDB();
                    tLRPolDB.setReContCode(mLRTempCessContSchema.getReContCode());
                    tLRPolDB.setReinsureItem("C");
                    tLRPolDB.setPolNo(tLCPolSchema.getPolNo());
                    tLRPolDB.setReNewCount(tLCPolSchema.getRenewCount());
                    if (tLRPolDB.getInfo()) {
                        this.mMap.put(tLRPolDB.getSchema(), "DELETE");
                    }
                    LRTempCessContInfoDB tLRTempCessContInfoDB = new
                            LRTempCessContInfoDB();
                    tLRTempCessContInfoDB.setReContCode(mLRTempCessContSchema.
                            getReContCode());
                    tLRTempCessContInfoDB.setProposalNo(tLCPolSchema.
                            getProposalNo());
                    if (tLRTempCessContInfoDB.getInfo()) {
                        this.mMap.put(tLRTempCessContInfoDB.getSchema(),
                                      "DELETE");
                    }
                }
            }
        }
        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "保存临分协议信息时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        return true;
    }

    /**
     * 计算分保保额
     * 参数1：自留额
     * 参数2：风险保额
     * @return double
     */
    private double calCessAmnt(double retainAmnt, double riskAmnt) {
        double calCessAmnt = riskAmnt - retainAmnt;
        Calculator mCalculator = new Calculator();

        return calCessAmnt;
    }

    private void prepareData(LRPolSchema aLRPolSchema,
                             LCPolSchema aLCPolSchema) {
        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        //得到该险种的险种类别
        String strSQL =
                "select distinct risksort from LRCalFactorValue where riskcode='" +
                aLCPolSchema.getRiskCode() + "'";
        SSRS riskSort = new ExeSQL().execSQL(strSQL);
        if (riskSort == null || riskSort.getMaxRow() <= 0) {
            buildError("insertData", "没有该险种的险种类别!");
            return;
        }
        mRiskCalSort = riskSort.GetText(1, 1);
        aLRPolSchema.setReinsureItem("C"); //再保项目 L--法定分保 C--商业分保
        aLRPolSchema.setReContCode(mLRTempCessContSchema.getReContCode());
        aLRPolSchema.setRiskCalSort(this.mRiskCalSort); //险种计算代码
        aLRPolSchema.setTempCessFlag("Y"); //设置临分标记,Y表示临分
        aLRPolSchema.setCessStart(currentDate);
        aLRPolSchema.setCessEnd("");
        aLRPolSchema.setGetDataDate(mLRTempCessContSchema.getInputDate()); //临分日期为合同录入日期
        aLRPolSchema.setMakeDate(currentDate);
        aLRPolSchema.setMakeTime(currentTime);
        aLRPolSchema.setModifyDate(currentDate);
        aLRPolSchema.setModifyTime(currentTime);

    }

    private double getPolAddPrem() {
        return 0;
    }

    /**
     * deleteData
     *
     * @return boolean
     */
    private boolean deleteData() {
        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("updateData", "修改时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;
        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData() {
        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "保存再保公司信息时时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;

    }

    public String createWorkNo(String ags) {
        String tWorkNo = "";
        if (ags.equals("RECOMCODE")) {
            tWorkNo = PubFun1.CreateMaxNo("RECOMCODE", 3);
        } else {
            tWorkNo = PubFun1.CreateMaxNo("RELATION", 6);
        }
        return tWorkNo;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        this.mLRTempCessContSchema.setSchema((LRTempCessContSchema)
                                             cInputData.
                                             getObjectByObjectName(
                "LRTempCessContSchema", 0));
        this.grpCessData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        return true;
    }

    public String getResult() {
        return mLRTempCessContSchema.getReContCode();
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "GrpTempCessContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
