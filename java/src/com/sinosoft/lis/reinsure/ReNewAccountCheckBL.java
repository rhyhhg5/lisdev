package com.sinosoft.lis.reinsure;

/**
 * Copyright (c) 2008 sinosoft  Co. Ltd.
 * All right reserved.
 */

import java.io.File;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LRComRiskResultSet;
import com.sinosoft.lis.db.LRComRiskResultDB;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;

/*
 * <p>ClassName: ReNewAccountCheckBL </p>
 * <p>Description: ReNewAccountCheckBL类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: Sun yu
 * @CreateDate：2008-10-11
 */
public class ReNewAccountCheckBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();
    private LRComRiskResultSet mLRComRiskResultSet=new LRComRiskResultSet();
    private FileWriter mFileWriter ;
    private BufferedWriter mBufferedWriter ;

    //业务处理相关变量
    /** 全局数据 */

    public ReNewAccountCheckBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
//        if (!dealData()) {
//            return false;
//        }
        if(this.strOperate.equals("CONFIRM")){//审核通过，修改状态
           if(check()==false){
               buildError("check()","审核失败");
               return false;
           }
           CreateAccountBL tCreateAccountBL=new CreateAccountBL();
           if(tCreateAccountBL.submitData(cInputData,strOperate)){
               buildError("CreateAccountBL.submitData()",tCreateAccountBL.mErrors.getFirstError());
           }
        }else if(this.strOperate.equals("ONLOAD")){//下载清单
           if(onload()==false){
               buildError("onload()","下载清单失败");
               return false;
           }
       }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
//    private boolean dealData() {
//        return true;
//    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLRComRiskResultSet=(LRComRiskResultSet) cInputData.getObjectByObjectName(
                "LRComRiskResultSet", 0);
        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    /**
     * 审核通过，修改状态
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean check(){
        for(int i=1;i<=this.mLRComRiskResultSet.size();i++){
            LRComRiskResultSchema tLRComRiskResultSchema=mLRComRiskResultSet.get(i);

//            String tSql="update LRPolClm set ActuGetState ='02',modifydate=current date,"
//                +"modifytime=current time where ActuGetState='01' and ActuGetNo='"+tLRPolSchema.getActuGetNo()+"' "
//                +" and recontcode='"+tLRPolSchema.getReContCode()+"' and riskcode='"+tLRPolSchema.getRiskCode()+"' with ur";
//            this.mMap.put(tSql, "UPDATE");
//
//            tSql="update LRPolEdor set ActuGetState='02',modifydate=current date,"
//                +"modifytime=current time where ActuGetState='01' and ActuGetNo='"+tLRPolSchema.getActuGetNo()+"' "
//                +" and recontcode='"+tLRPolSchema.getReContCode()+"' and riskcode='"+tLRPolSchema.getRiskCode()+"' with ur";
//            this.mMap.put(tSql, "UPDATE");
//
//            tSql="update LRPol set ActuGetState='02',modifydate=current date,"
//                +"modifytime=current time where ActuGetState='01' and ActuGetNo='"+tLRPolSchema.getActuGetNo()+"' "
//                +" and recontcode='"+tLRPolSchema.getReContCode()+"' and riskcode='"+tLRPolSchema.getRiskCode()+"' with ur";
            String tSql = "update  LRComRiskResult set standbyflag2='02' where riskcode='"+tLRComRiskResultSchema.getRiskCode()+
                          "' and standbyflag1='"+tLRComRiskResultSchema.getStandbyFlag1()+"' with ur";
            this.mMap.put(tSql, "UPDATE");  //更新分机构分险种表的账单状态
           }
            //准备往后台的数据
            if (!prepareOutputData()) return false;
            System.out.println("---End prepareOutputData---");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, strOperate)) {
              // @@错误处理
              this.mErrors.copyAllErrors(tPubSubmit.mErrors);
              return false;
            }

        return true;
    }

    private boolean onload(){
        String tReContCode = (String)mTransferData.getValueByName("ReContCode");
        String tReComCode = (String) mTransferData.getValueByName("ReComCode");
        String tCessionMode = (String) mTransferData.getValueByName("CessionMode");
        String tDiskKind = (String) mTransferData.getValueByName("DiskKind");
        String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
        String tYear = (String) mTransferData.getValueByName("Year");
        String tMonth = (String) mTransferData.getValueByName("Month");
        String tStartDate="";//起始日期
        String tEndDate="";//终止日期
        if(!StrTool.cTrim(tYear).equals("") && !StrTool.cTrim(tMonth).equals("")){
           Calendar calendar=Calendar.getInstance();
           calendar.set(Calendar.YEAR,Integer.parseInt(tYear));
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth));
           calendar.set(Calendar.DATE,1);
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           String tCurrentDate = sdf.format(calendar.getTime());
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth)-1);
           tStartDate=sdf.format(calendar.getTime());
           tEndDate= PubFun.calDate(tCurrentDate,-1,"D","2009-1-31");  //结束日期取本月最后一天
       }
       String tSysPath = (String) mTransferData.getValueByName("SysPath"); //获取地址;
       tSysPath +="reinsure/";
       String tPath="";
       try{
            tPath = "ZhangDanXiangXi"+PubFun.getCurrentDate2() + "_" + PubFun.getCurrentTime2();
            mFileWriter = new FileWriter(tSysPath+tPath+".txt");
            mBufferedWriter = new BufferedWriter(mFileWriter);

            String[] strArrHead = new String[11];
              strArrHead[0]="机构代码";
              strArrHead[1]="险种代码";
              strArrHead[2]="再保合同号";
              strArrHead[3]="成本中心";
              strArrHead[4]="再保公司";
              strArrHead[5]="分保保费";
              strArrHead[6]="分保手续费";
              strArrHead[7]="摊回保费";
              strArrHead[8]="摊回手续费";
              strArrHead[9]="摊回赔款";
              strArrHead[10]="账单状态";
            String head = "";
            for(int m=0;m<strArrHead.length;m++){
                        head += strArrHead[m]+"|";
            }
            mBufferedWriter.write(head+"\r\n");
            mBufferedWriter.flush();
        }catch(Exception ex1){
            ex1.printStackTrace();
        }


        String strWhere="";
        String strWhereT="";
        if(!StrTool.cTrim(tReComCode).equals("")){
            strWhere+=" and b.ReComCode='"+tReComCode+"' ";
            strWhereT += " and b.ComCode='"+tReComCode+"' ";
        }
        if(!StrTool.cTrim(tCessionMode).equals("")){
            strWhere+=" and b.CessionMode='"+tCessionMode+"' ";
            strWhereT += " and b.CessionMode='"+tCessionMode+"' ";
        }
        if(!StrTool.cTrim(tDiskKind).equals("")){
            strWhere+=" and b.DiskKind='"+tDiskKind+"' ";
            strWhereT += " and b.DiskKind='"+tDiskKind+"' ";
        }
        if(tStartDate!=""&&tEndDate!=""){
            strWhere+=" and a.StartDate='"+tStartDate+"' and a.EndData = '"+tEndDate+"' ";
            strWhereT +=" and a.StartDate='"+tStartDate+"' and a.EndData = '"+tEndDate+"' ";
        }
        if(!StrTool.cTrim(tRiskCode).equals("")){
            strWhere+=" and a.RiskCode='"+tRiskCode+"' ";
            strWhereT +=" and a.RiskCode='"+tRiskCode+"' ";
        }
        if(!StrTool.cTrim(tReContCode).equals("")){
            strWhere+=" and a.ReContCode='"+tReContCode+"' ";
            strWhereT +=" and a.ReContCode='"+tReContCode+"' ";
        }


        String strSQL  = "select a.managecom,a.riskcode,a.recontcode,a.costcenter, b.recomcode, a.CessPrem,ReProcFee,EdorBackFee,a.EdorProcFee,a.claimbackfee,(select 1 from lraccounts b where a.StandbyFlag1=b.ActuGetNo) "+
        "from LRComRiskResult a,lRContInfo b where a.recontcode=b.recontcode "+
//        "exists (select 1 from lraccounts b where a.StandbyFlag1=b.ActuGetNo and b.PayFlag='01') "+   //只查询清单审核完成的账单
        strWhere+
//       " group by a.riskcode, a.recontcode, a.recontcode,b.recomcode "+
       " union all "+
        "select a.managecom,a.riskcode,a.recontcode,a.costcenter, b.ComCode,a.CessPrem,ReProcFee,EdorBackFee,a.EdorProcFee,a.claimbackfee,(select 1 from lraccounts b where a.StandbyFlag1=b.ActuGetNo) "+
        "from LRComRiskResult a,LRTempCessCont b where a.recontcode=b.TempContCode  "+
//        "exists (select 1 from lraccounts b where a.StandbyFlag1=b.ActuGetNo and b.PayFlag='01') "+   //只查询清单审核完成的账单
        strWhereT+
//       " group by a.riskcode, a.recontcode, a.recontcode,b.ComCode "+
        " with ur";


    int start = 1;
   int nCount = 10000;
     while (true) {
          SSRS tSSRS = new ExeSQL().execSQL(strSQL, start, nCount);
          int count=0;
          if (tSSRS.getMaxRow() <= 0) {
               break;
           }else{
               count=tSSRS.getMaxRow();
           }
         if (tSSRS != null && count > 0) {
           for (int i = 1; i <= count; i++) {
             try{
                 String result="";
                 for(int m=1;m<=tSSRS.getMaxCol();m++){
                       result += tSSRS.GetText(i,m)+"|";
                 }
                  mBufferedWriter.write(result+"\r\n");
                  mBufferedWriter.flush();
              } catch (IOException ex2) {
                     ex2.printStackTrace();
               }
              }
          }
          start += nCount;
        }
      try {
         mBufferedWriter.close();
       } catch (IOException ex3) {
       }
       String[] FilePaths = new String[1];
       FilePaths[0] = tSysPath+tPath+".txt";
       String[] FileNames = new String[1];
       FileNames[0] =tPath+".txt";
       String newPath = tSysPath +tPath+".zip";
       String FullPath = tPath+".zip";
       CreateZip(FilePaths,FileNames,newPath);
       try{
           File fd = new File(FilePaths[0]);
           fd.delete();
       }catch(Exception ex4){
           ex4.printStackTrace();
       }
     mResult.add(FullPath);
        return true;
    }
    //生成压缩文件
public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                         String tZipPath) {
    ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
    if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
        System.out.println("生成压缩文件失败");
        CError.buildErr(this, "生成压缩文件失败");
        return false;
    }
    return true;
 }

}

