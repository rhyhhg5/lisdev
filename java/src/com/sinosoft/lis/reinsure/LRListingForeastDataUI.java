package com.sinosoft.lis.reinsure;

import com.sinosoft.utility.*;
public class LRListingForeastDataUI {
	public  CErrors mErrors=new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData =new VData();
	private String mOperate;


	public boolean submitData(VData cInputData,String cOperate)
	{
	  //将操作数据拷贝到本类中
	  mOperate=cOperate;
	  this.mInputData =(VData)cInputData.clone() ;
	  LRListingForeastDataBL tLRListingForeastDataBL=new LRListingForeastDataBL();
	 try{ 
	  if(!tLRListingForeastDataBL.submitData(cInputData,mOperate)){
		  if (tLRListingForeastDataBL.mErrors .needDealError() )
		  {
			  // @@错误处理
			  this.mErrors.copyAllErrors(tLRListingForeastDataBL.mErrors);
			  CError tError = new CError();
			  tError.moduleName = "UI";
			  tError.functionName = "submitData";
			  tError.errorMessage = "数据提交失败!";
			  this.mErrors .addOneError(tError) ;
			  return false;
		  }
	  }
	 }catch(Exception e){
		 e.printStackTrace();
	 }
//	 mResult=tLRListingForeastDataBL.getResult();
	  mInputData=null;
	  return true;
	}
	public VData getResult()
	{
	  return this.mResult;
	}
}
