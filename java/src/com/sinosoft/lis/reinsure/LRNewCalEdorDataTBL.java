/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

/*
 * <p>ClassName: LRCalEdorDataBL </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2009-02-09
 */
public class LRNewCalEdorDataTBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mStartDate = "";
    

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    //业务处理相关变量
    /** 全局数据 */

    public LRNewCalEdorDataTBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT(String cOperate,String mToDay) {
        System.out.println("Come in BL->Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputDataT(mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        
        globalInput.ManageCom = "8611";

        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRNewCalEdorDataBL tLRNewCalEdorDataBL = new LRNewCalEdorDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2008-09-10");
            tTransferData.setNameAndValue("EndDate", "2008-09-10");
            vData.add(tTransferData);

            tLRNewCalEdorDataBL.submitData(vData, "EDORDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
//            tError.moduleName = "LDComBL";
//            tError.functionName = "prepareData";
//            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
//            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取保全数据
       
            if (!calculateEdorData()) {
                buildError("updateData", "再保保全计算失败");
                return false;
            }
            if (!calculateEdorDataXB()) {  //计算续保的摊回结果
               buildError("updateData", "再保保全计算失败");
               return false;
           }
       
        return true;
    }

    /**
     * 计算保全数据
     * @return boolean
     */
    private boolean calculateEdorData() {
      

        String strSQL = "select * from LRPolEdor a where GetDataDate = '" +
                        mStartDate + "' with ur";
        LRPolEdorSet tLRPolEdorSet = new LRPolEdorSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolEdorSet, strSQL);
        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolEdorSet.size(); i++) {
                	
                    LRPolEdorSchema tLRPolEdorSchema = tLRPolEdorSet.get(i);
                    LRPolEdorResultSchema tLRPolEdorResultSchema = new
                            LRPolEdorResultSchema();

                    LRPolDB tLRPolDB = new LRPolDB();
                    LRPolResultDB tLRPolResultDB = new LRPolResultDB();
                    tLRPolDB.setPolNo(tLRPolEdorSchema.getPolNo());
                    tLRPolDB.setReContCode(tLRPolEdorSchema.getReContCode());
//                    tLRPolDB.setReinsureItem(tLRPolEdorSchema.
//                                                 getReinsureItem());
//                    tLRPolDB.setPolStat("7");
                    LRPolSet tLRPolSet = tLRPolDB.query();
                    
                    tLRPolResultDB.setPolNo(tLRPolEdorSchema.getPolNo());
                    tLRPolResultDB.setReContCode(tLRPolEdorSchema.getReContCode());
//                    tLRPolResultDB.setReinsureItem(tLRPolEdorSchema.
//                                                 getReinsureItem());
//                    tLRPolDB.setPolStat("7");
                    LRPolResultSet tLRPolResultSet = tLRPolResultDB.query();
                    int countCYC=0; //保单的剩余周期
                    

                    if (tLRPolEdorSchema.getRiskCalSort().equals("1")) {
                        //处理短险成数
//                    	String tdate=mPubFun.getBeforeDate(tLRPolSet.get(1).getGetDataDate(),"2010-1-1");
                    	
                        
                        double speCemmRate = this.getSpeCemmRate(
                                tLRPolEdorSchema);
                        double reProcFeeRate = this.getReProcFeeRate(
                                tLRPolEdorSchema);

                        if (tLRPolResultSet.size() > 0) {
                        	double cessRate = tLRPolResultSet.get(1).getCessionRate();
                            int tPayIntv = tLRPolSet.get(1).getPayIntv();
                            double shouldReturnPrem = 0;
                            int tRemainCount = 0;
                           
                                   shouldReturnPrem = tLRPolEdorSchema.getGetMoney();
                              
                            double edorBackFee = shouldReturnPrem * cessRate;
                            if(tLRPolEdorSchema.getFeeoperationType().equals("RB")){
                                tLRPolEdorResultSchema.setEdorBackFee(-edorBackFee); //如果为退保,应该是分出金额,钱数为正
                                tLRPolEdorResultSchema.setEdorProcFee(-edorBackFee *
                                    (speCemmRate + reProcFeeRate)); //保全手续费摊回
                            }else{
                                tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                                tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                    (speCemmRate + reProcFeeRate)); //保全手续费摊回
                            }
                            tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                            tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                    (speCemmRate + reProcFeeRate)); //保全手续费摊回
                            tLRPolEdorResultSchema.setShouldReturnPrem(
                                    shouldReturnPrem);
                            tLRPolEdorResultSchema.setRemainCount(tRemainCount);
                            prepareLRPolEdorResult(tLRPolEdorSchema,
                                    tLRPolEdorResultSchema);
                            this.mMap.put(tLRPolEdorResultSchema,
                                          "DELETE&INSERT");
                           
                                
                                if ((tLRPolEdorSchema.getFeeoperationType().equals("WT")||
                                		tLRPolEdorSchema.getFeeoperationType().equals("XT")||
                                		tLRPolEdorSchema.getFeeoperationType().equals("CT"))&&
                                		(tPayIntv==1||tPayIntv==3||tPayIntv==6)
                                		&&tLRPolSet.get(1).getContType().equals("2")) {
                                   
                                    countCYC = PubFun.calInterval(tLRPolEdorSchema.getGetDataDate()
                                            , tLRPolSet.get(1).getEndDate(), "M") /
                                            tPayIntv;
                                }
                                if (countCYC > 0) {//保单的保障天数。理论上不可能为0。
                                	double trate=speCemmRate + reProcFeeRate;
                                	insertFC(tLRPolSet,tLRPolEdorSchema,countCYC,trate);
                                }
                            
                        } else {
                            this.errorLog(tLRPolEdorSchema, "没有该保单的分保数据");
                        }
                    } else {
                        //处理溢额分保,暂时不支持长险成数
                   	 int countDays=0; //保单的有效保障天数。（因为存在S型险和M型险保障不到一年的情况。M型险卖多年暂不处理）
                        if(tLRPolSet.size()>0){
                            String aSql =
                                    " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='" +
                                    tLRPolSet.get(1).getRiskCode() + "' with ur";
                            String LRiskFlag = new ExeSQL().getOneValue(aSql);
                            if (Integer.parseInt(LRiskFlag) > 0) {
                                countDays = 365;
                            } else {
                                countDays = PubFun.calInterval(tLRPolSet.get(1).getCValiDate(),
                                        tLRPolSet.get(1).getEndDate(), "D");
                            }
                            if (countDays <= 0) {//保单的保障天数。理论上不可能为0。
                                countDays = 365;
                            }
                        }
                       //LRPolResultDB tLRPolResultDB = new LRPolResultDB();
                       tLRPolResultDB.setPolNo(tLRPolEdorSchema.getPolNo());
                       tLRPolResultDB.setReContCode(tLRPolEdorSchema.
                               getReContCode());
//                       tLRPolResultDB.setReinsureItem(tLRPolEdorSchema.
//                               getReinsureItem());
                       tLRPolResultDB.setReNewCount(tLRPolEdorSchema.
                               getReNewCount());
                       tLRPolResultSet = tLRPolResultDB.query();
                       if (tLRPolResultSet.size() > 0) {
                           double cessPrem = tLRPolResultSet.get(1).
                                             getCessPrem(); //分保保费
                           double ChoiRebaFeeRate = tLRPolResultSet.get(1).
                                   getChoiRebaFeeRate(); //取分保结果中的选择折扣,不需要重新计算保单年度
                           double edorBackFee = cessPrem * ( -1) *
                           tLRPolEdorSchema.getNoPassDays() /
                           countDays;
                           if(tLRPolEdorSchema.getFeeoperationType().equals("RB")){
                               tLRPolEdorResultSchema.setEdorBackFee(-edorBackFee); //如果为退保,应该是分出金额,钱数为正
                               tLRPolEdorResultSchema.setEdorProcFee(-edorBackFee *
                                   ChoiRebaFeeRate); //手续费摊回
                           }else{
                               tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                               tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                   ChoiRebaFeeRate); //手续费摊回
                           }
                           tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                           tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                   ChoiRebaFeeRate); //手续费摊回
                           prepareLRPolEdorResult(tLRPolEdorSchema,
                                   tLRPolEdorResultSchema);
                           this.mMap.put(tLRPolEdorResultSchema,
                                         "DELETE&INSERT");
                       } else {
                           this.errorLog(tLRPolEdorSchema, "没有该保单的分保计算结果");
                       }
                   }
                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolEdorSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
        return true;
    }
    
  //在成数续期时插入首期保费的负记录，以计算出续期反冲数据
    private boolean insertFC(LRPolSet tLRPolSet,LRPolEdorSchema tLRPolEdorSchema,int countCYC,double trate){
        SSRS tSSRS=null;
        
        	String tRe="select count(1) from lrpoledor" +
			" where polno='"+tLRPolEdorSchema.getPolNo()+"' and recontcode='"+tLRPolEdorSchema.getReContCode()+"' " +
					"   with ur";
        	SSRS tsRe = new ExeSQL().execSQL(tRe);
        	String tnRe=tsRe.GetText(1, 1);
        	if(Integer.parseInt(tnRe)>0){
        	String tChecksql1="select count(1) from lrpoledorresult " +
			" where polno in (select polno from lrpoledor where grppolno='"+tLRPolSet.get(1).getGrpPolNo()+"') " +
					"and recontcode='"+tLRPolEdorSchema.getReContCode()+"' " +
					"  and ReinsureItem='F' with ur";
        	SSRS tCheck1 = new ExeSQL().execSQL(tChecksql1);
        	String tcheck1=tCheck1.GetText(1, 1);
        	
        if(Integer.parseInt(tcheck1)==0){
        	
        		String str1="";
        		str1="select PolNo,EdorNo," +
        		"ReContCode,ReComCode,'F',RiskCalSort," +
        		"RiskCode,RiskVersion,'"+this.mStartDate+"'," +
        		"'"+this.mStartDate+"',(select -prem from lrpol where polno=a.polno " +
        				"and renewcount=0 and polstat='7' and recontcode='"+tLRPolEdorSchema.getReContCode()+"' and getdatadate=(select min(getdatadate) from lrpol where " +
        						"grpcontno='"+tLRPolEdorSchema.getGrpContNo()+"' " +
        								" and grppolno='"+tLRPolSet.get(1).getGrpPolNo()+"' and renewcount=0 and conttype='2' and " +
        								" recontcode='"+tLRPolEdorSchema.getReContCode()+"'))*"+countCYC+"," +
        				"(select -prem from lrpol where polno=a.polno " +
        				"and renewcount=0 and polstat='7' and recontcode='"+tLRPolEdorSchema.getReContCode()+"' and getdatadate=(select min(getdatadate) from lrpol where " +
        						"grpcontno='"+tLRPolEdorSchema.getGrpContNo()+"' " +
        								" and grppolno='"+tLRPolSet.get(1).getGrpPolNo()+"' and renewcount=0 and conttype='2' and " +
        								" recontcode='"+tLRPolEdorSchema.getReContCode()+"'))*"+countCYC+"*"+trate+",Operator,ManageCom," +
        		"MakeDate,MakeTime,current date,current time,ReNewCount," +
        		""+countCYC+",0 from lrpoledor a" +
        		" where grpcontno='"+tLRPolSet.get(1).getGrpContNo()+"' and grppolno='"+tLRPolSet.get(1).getGrpPolNo()+"'" +
        				" and polno in (select polno from lrpol where grppolno='"+tLRPolSet.get(1).getGrpPolNo()+"' and " +
        						"getdatadate=(select min(getdatadate) from lrpol where " +
        						"grpcontno='"+tLRPolEdorSchema.getGrpContNo()+"' " +
        								" and grppolno='"+tLRPolSet.get(1).getGrpPolNo()+"' " +
        										"and renewcount=0 and conttype='2' and " +
        								" recontcode='"+tLRPolEdorSchema.getReContCode()+"'))";
        		
        		
        		ExeSQL tExeSQL = new ExeSQL();
        		tExeSQL.execUpdateSQL("insert into lrpoledorresult(PolNo,EdorNo,ReContCode," +
            		"ReComCode,ReinsureItem,RiskCalSort,RiskCode,RiskVersion," +
            		"CessStartDate,CessEndDate,EdorBackFee,EdorProcFee,Operator," +
            		"ManageCom,MakeDate,MakeTime,ModifyDate,ModifyTime,ReNewCount," +
            		"RemainCount,ShouldReturnPrem) " +str1
            		);
        		
        	
        	}
        	}
        return true ;
    }
    private boolean calculateEdorDataXB() {
     

      String strSQL = "select * from LRPolEdor a where GetDataDate = '" +
                      mStartDate + "'  and a.feeoperationtype='XB' with ur";
      LRPolEdorSet tLRPolEdorSet = new LRPolEdorSet();
      RSWrapper rswrapper = new RSWrapper();
      rswrapper.prepareData(tLRPolEdorSet, strSQL);
      try {
          do {
              rswrapper.getData();
              mMap = new MMap();
              for (int i = 1; i <= tLRPolEdorSet.size(); i++) {
                  LRPolEdorSchema tLRPolEdorSchema = tLRPolEdorSet.get(i);
                  LRPolEdorResultSchema tLRPolEdorResultSchema = new
                          LRPolEdorResultSchema();

                  LRPolDB tLRPolDB = new LRPolDB();
                  tLRPolDB.setPolNo(tLRPolEdorSchema.getEdorNo()); //如果是退保,分保信息在保全提数时被换号,此处存的是newpolno
                  tLRPolDB.setReContCode(tLRPolEdorSchema.getReContCode());
                  tLRPolDB.setReinsureItem(tLRPolEdorSchema.
                                               getReinsureItem());
                  //tLRPolDB.setReNewCount(tLRPolEdorSchema.getReNewCount());
                  LRPolSet tLRPolSet = tLRPolDB.query();
                  int countDays=365; //保单的有效保障天数。（因为存在S型险和M型险保障不到一年的情况。M型险卖多年暂不处理）
                   if(tLRPolSet!=null&&tLRPolSet.size()>0){
                       String aSql =
                               " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='" +
                               tLRPolSet.get(1).getRiskCode() + "' with ur";
                       String LRiskFlag = new ExeSQL().getOneValue(aSql);
                       if (Integer.parseInt(LRiskFlag) > 0) {
                           countDays = 365;
                       } else {
                           countDays = PubFun.calInterval(tLRPolSet.get(1).
                                   getCValiDate(),
                                   tLRPolSet.get(1).getEndDate(), "D");
                       }
                       if (countDays <= 0) { //保单的保障天数。理论上不可能为0。
                           countDays = 365;
                       }

                       LRPolResultDB tLRPolResultDB = new LRPolResultDB();
                       tLRPolResultDB.setPolNo(tLRPolEdorSchema.getPolNo());
                       tLRPolResultDB.setReContCode(tLRPolEdorSchema.
                               getReContCode());
                       tLRPolResultDB.setReinsureItem(tLRPolEdorSchema.
                               getReinsureItem());
                       tLRPolResultDB.setReNewCount(tLRPolEdorSchema.
                               getReNewCount());
                       LRPolResultSet tLRPolResultSet = tLRPolResultDB.query();
                       if (tLRPolResultSet != null &&
                           tLRPolResultSet.size() > 0) {
                           double cessPrem = tLRPolResultSet.get(1).
                                             getCessPrem(); //分保保费
                           double edorBackFee = cessPrem * ( -1) *
                                                tLRPolEdorSchema.getNoPassDays() /
                                                countDays;
                           if(tLRPolEdorSchema.getRiskCalSort().equals("1")){
                               double speCemmRate = this.getSpeCemmRate(
                                       tLRPolEdorSchema);
                               double reProcFeeRate = this.getReProcFeeRate(
                                       tLRPolEdorSchema);
                               tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                               tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                   (speCemmRate+reProcFeeRate)); //手续费摊回
                           }else{
                               double ChoiRebaFeeRate = tLRPolResultSet.get(1).
                                       getChoiRebaFeeRate(); //取新单计算时的选择折扣,不需要重新计算保单年度
                               tLRPolEdorResultSchema.setEdorBackFee(edorBackFee); //保全退费摊回
                                tLRPolEdorResultSchema.setEdorProcFee(edorBackFee *
                                   ChoiRebaFeeRate); //手续费摊回

                           }
                           prepareLRPolEdorResult(tLRPolEdorSchema,
                                                  tLRPolEdorResultSchema);
                           this.mMap.put(tLRPolEdorResultSchema,
                                         "DELETE&INSERT");
                           if (tLRPolEdorSchema.getFeeoperationType().equals(
                                   "XB")) { //对于续保回退的摊回,同时修改分保计算结果表的polno信息,将其置换edorno,即newpolno
                               String stru = " update lrpolresult set polno='" +
                                             tLRPolEdorSchema.getEdorNo() +
                                       "',modifydate=current date,modifytime=current time where " +
                                             " PolNo='" +
                                             tLRPolEdorSchema.getPolNo() +
                                             "' and ReContCode='" +
                                             tLRPolEdorSchema.getReContCode() +
                                             "' and ReinsureItem='" +
                                             tLRPolEdorSchema.getReinsureItem() +
                                             "' and ReNewCount= " +
                                             tLRPolEdorSchema.getReNewCount();
                               this.mMap.put(stru, "UPDATE");
                           }
                       } else {
                           this.errorLog(tLRPolEdorSchema, "没有该保单的分保计算结果");
                       }
                   }
                  }
              if (!prepareOutputData()) {
                  return false;
              }
              tPubSubmit = new PubSubmit();
              if (!tPubSubmit.submitData(mInputData, "")) {
                  if (tPubSubmit.mErrors.needDealError()) {
                      buildError("submitData", "保存再保计算结果出错!");
                  }
                  return false;
              }
          } while (tLRPolEdorSet.size() > 0);
          rswrapper.close();
      } catch (Exception ex) {
          ex.printStackTrace();
          rswrapper.close();
      }
      return true;
  }

    private boolean prepareLRPolEdorResult(LRPolEdorSchema aLRPolEdorSchema,
                                           LRPolEdorResultSchema
                                           aLRPolEdorResultSchema) {
        aLRPolEdorResultSchema.setPolNo(aLRPolEdorSchema.getPolNo());
        aLRPolEdorResultSchema.setEdorNo(aLRPolEdorSchema.getEdorNo());
        aLRPolEdorResultSchema.setReContCode(aLRPolEdorSchema.getReContCode());

//        String strSQL = "select Recomcode from LRContInfo where RecontCode = '" +
//                        aLRPolEdorSchema.getReContCode() + "'";
        aLRPolEdorResultSchema.setReComCode(aLRPolEdorSchema.getReComCode());
        aLRPolEdorResultSchema.setReinsureItem(aLRPolEdorSchema.getReinsureItem());
        aLRPolEdorResultSchema.setRiskCalSort(aLRPolEdorSchema.getRiskCalSort());
        aLRPolEdorResultSchema.setRiskCode(aLRPolEdorSchema.getRiskCode());
        aLRPolEdorResultSchema.setRiskVersion(aLRPolEdorSchema.getRiskVersion());
        aLRPolEdorResultSchema.setCessStartDate(this.mStartDate);
        aLRPolEdorResultSchema.setCessEndDate(this.mStartDate);
        aLRPolEdorResultSchema.setManageCom(aLRPolEdorSchema.getManageCom());

        aLRPolEdorResultSchema.setOperator("Server");
        aLRPolEdorResultSchema.setMakeDate(mPubFun.getCurrentDate());
        aLRPolEdorResultSchema.setMakeTime(mPubFun.getCurrentTime());
        aLRPolEdorResultSchema.setModifyDate(mPubFun.getCurrentDate());
        aLRPolEdorResultSchema.setModifyTime(mPubFun.getCurrentTime());
        aLRPolEdorResultSchema.setReNewCount(aLRPolEdorSchema.getReNewCount());
        return true;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(LRPolEdorSchema aLRPolEdorSchema, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new
                                             LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aLRPolEdorSchema.getPolNo());
        tLRErrorLogSchema.setLogType("6"); //6:保全计算
        tLRErrorLogSchema.setErrorInfo(errorInf);
        tLRErrorLogSchema.setOperator("Server");
        tLRErrorLogSchema.setManageCom(aLRPolEdorSchema.getManageCom());
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        mMap.put(tLRErrorLogSchema, "DELETE&INSERT");
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(String mToDay) {
      
        mStartDate = mToDay;
        
        return true;
    }
 
    public String getResult() {
        return "没有传数据";
    }

    /**
     * 得到成数分保比例
     * @param LRPolEdorSchema aLRPolEdorSchema
     * @return String
     */
    private double getCessionRate(LRPolEdorSchema aLRPolEdorSchema) {
        String strSQL =
                "select varchar(factorvalue) from lrcalfactorvalue where recontcode='" +
                aLRPolEdorSchema.getReContCode() +
                "' and upper(factorcode)='CESSIONRATE'" +  //去掉险种限制，以兼容责任分保
                " union all "+  //临分的分保比例
                "select varchar(CessionRate) from LRTempCessContInfo where TempContCode='"+
                 aLRPolEdorSchema.getReContCode() +
                "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到特殊佣金比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getSpeCemmRate(LRPolEdorSchema aLRPolEdorSchema) {
        String strSQL =
                "select varchar(factorvalue) from lrcalfactorvalue where recontcode='" +
                aLRPolEdorSchema.getReContCode() +
                "' and upper(factorcode)='SPECOMRATE' " +
                " union all "+  //临分的特殊佣金比例
                "select varchar(SpeComRate) from LRTempCessContInfo where TempContCode='"+
                 aLRPolEdorSchema.getReContCode() +
                "' with ur";
;
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /**
     * 得到再保手续费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getReProcFeeRate(LRPolEdorSchema aLRPolEdorSchema) {
        String strSQL =
                "select varchar(factorvalue) from lrcalfactorvalue where recontcode='" +
                aLRPolEdorSchema.getReContCode() +
                "' and upper(factorcode)='REINPROCFEERATE' " +
                " union all "+  //临分的再保手续费比例
                "select varchar(ReinProcFeeRate) from LRTempCessContInfo where TempContCode='"+
                 aLRPolEdorSchema.getReContCode() +
                "' with ur";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() <= 0) {
            return 0;
        }
        if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
            return 0;
        }
        return Double.parseDouble(tSSRS.GetText(1, 1));
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

//        cError.moduleName = "ReComManageBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
}
