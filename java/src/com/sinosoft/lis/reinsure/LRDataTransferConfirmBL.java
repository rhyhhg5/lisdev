package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.LRAccountDB;
import com.sinosoft.lis.fininterface_v3.LRAccountDataExtractTask;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRAccountSchema;
import com.sinosoft.lis.vschema.LRAccountSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LRDataTransferConfirmBL {
	public CErrors mErrors = new CErrors();
	private String mFeeType = "";
	private String mOprater = "";
	private String mBelongYear;
	private String mBelongMonth;
	private String flag = "01";
	private GlobalInput mGI = null;
	TransferData tf = null;
	private VData mOutputData = new VData();
	private LRAccountSet mLRAccountSet = new LRAccountSet();
	private LRAccountSchema mLRAccountSchema = new LRAccountSchema();
	private LRAccountSet tLRAccountSet = new LRAccountSet();
	private String aModifyDate = PubFun.getCurrentDate();
	private String aModifyTime = PubFun.getCurrentTime();
	private MMap mMap = new MMap();
	private MMap mMap1 = new MMap();
	private LRAccountDB tLRAccountDB = new LRAccountDB();
	public boolean submitData(VData cInputData, String operate) {
		if(!getInputData(cInputData)){
			return false;
		}
//		if (!dealData()) {
//		    CError tError = new CError();
//            tError.moduleName = "LRDataTransferConfirmBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据处理失败LRDataTransferConfirmBL-->dealData!";
//            this.mErrors.addOneError(tError);
//            return false;
//		}
//		if (!prepareOutputData())
//        { 
//            return false;
//        }
		String mUpDateSqlCessPrem ="";
		String mUpDateSqlReprocFee ="";
		if(mFeeType.equals("cess")){
			mUpDateSqlCessPrem  = "update lraccount a set  cesspremstate = '01' where cesspremstate = '00'  and belongyear ='"+mBelongYear+"' and belongmonth ='"+mBelongMonth+"' ";
			mMap1.put(mUpDateSqlCessPrem, "UPDATE");
		}else if(mFeeType.equals("claim")){
			mUpDateSqlReprocFee= "update lraccount a set  reprocfeestate = '01',claimbackfeestate = '01' where reprocfeestate='00' and claimbackfeestate='00' and belongyear ='"+mBelongYear+"' and belongmonth ='"+mBelongMonth+"' ";
			mMap1.put(mUpDateSqlReprocFee, "UPDATE");
		}
		this.mOutputData.clear();
        this.mOutputData.add(this.mMap1);
		 PubSubmit tPubSubmit = new PubSubmit();
	     if(!tPubSubmit.submitData(mOutputData, "")){
	    	 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             CError tError = new CError();
             tError.moduleName = "LRDataTransferConfirmBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据提交失败!";
             this.mErrors.addOneError(tError);
             return false;
	     }
	    LRAccountDataExtractTask mLRAccountDataExtractTask = new LRAccountDataExtractTask();
	    mLRAccountDataExtractTask.dealData();
	    String mSQL = "";
	    String mSQL1 = "";
	    String mAllSQLCess = "";
	    String mAllSQLReprocfee = "";
	    String mAllSQLClaim = "";
		if(mFeeType.equals("cess")){
			mSQL = "update lraccount a set  cesspremstate = '02' where cesspremstate = '01' and exists (select 1 from fivoucherdatadetail where serialno in( select serialno from fiabstandarddata where 1=1 and indexno = a.lrserialno) and checkflag = '00' )";
			mSQL1 = "update lraccount a set  cesspremstate = '00' where cesspremstate = '01' and exists (select 1 from fivoucherdatadetail where serialno in( select serialno from fiabstandarddata where 1=1 and indexno = a.lrserialno) and checkflag = '01' )";
			mAllSQLCess ="update lraccount  set  cesspremstate = '02' where cesspremstate  in( '01' ,'00') and cessprem =0  and belongyear ='"+mBelongYear+"' and belongmonth ='"+mBelongMonth+"' ";
			mMap.put(mSQL,"UPDATE");
			mMap.put(mSQL1, "UPDATE");
			mMap.put(mAllSQLCess,"UPDATE");
		}else if(mFeeType.equals("claim")){
			mSQL= "update lraccount a set  reprocfeestate = '02',claimbackfeestate = '02' where reprocfeestate = '01' and claimbackfeestate = '01' and exists (select 1 from fivoucherdatadetail where serialno in( select serialno from fiabstandarddata where 1=1 and indexno = a.lrserialno) and checkflag = '00' )";	
			mSQL1= "update lraccount a set  reprocfeestate = '00',claimbackfeestate = '00' where reprocfeestate = '01' and claimbackfeestate = '01' and exists (select 1 from fivoucherdatadetail where serialno in( select serialno from fiabstandarddata where 1=1 and indexno = a.lrserialno) and checkflag = '01' )";
			mAllSQLClaim ="UPDATE LRACCOUNT A SET  CLAIMBACKFEESTATE = '02' WHERE CLAIMBACKFEESTATE in( '01' ,'00')AND CLAIMBACKFEE =0  AND BELONGYEAR ='"+mBelongYear+"' AND BELONGMONTH ='"+mBelongMonth+"' ";
			mAllSQLReprocfee ="update lraccount a set  reprocfeestate = '02' where reprocfeestate in( '01' ,'00') and reprocfee =0  and belongyear ='"+mBelongYear+"' and belongmonth ='"+mBelongMonth+"' ";
			mMap.put(mSQL,"UPDATE");
			mMap.put(mSQL1, "UPDATE");
			mMap.put(mAllSQLReprocfee,"UPDATE");
			mMap.put(mAllSQLClaim,"UPDATE");
		}else{
		    CError tError = new CError();
            tError.moduleName = "LRDataTransferConfirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "流转类型为空LRDataTransferConfirmBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
		}
	
	
		
//		if (!dealData()) {
//		    CError tError = new CError();
//            tError.moduleName = "LRDataTransferConfirmBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据处理失败LRDataTransferConfirmBL-->dealData!";
//            this.mErrors.addOneError(tError);
//            return false;
//		}
	
		if (!prepareOutputData())
        {
            return false;
        }
		 tPubSubmit = new PubSubmit();
	     if(!tPubSubmit.submitData(mOutputData, "")){
	    	 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             CError tError = new CError();
             tError.moduleName = "LRDataTransferConfirmBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据提交失败!";
             this.mErrors.addOneError(tError);
             return false;
	     }
	     mLRAccountSet.clear();
	     flag = "";
		return true;
	}

	private boolean dealData(){		
		for(int i=1;i<=tLRAccountSet.size();i++){
			mLRAccountSchema = tLRAccountSet.get(i);
//			tLRAccountDB.setReContCode(mLRAccountSchema.getReContCode());
//			tLRAccountDB.setRiskCode(mLRAccountSchema.getRiskCode());
//			tLRAccountDB.setGrpContNo(mLRAccountSchema.getGrpContNo());
//			tLRAccountDB.setContNo(mLRAccountSchema.getContNo());
//			tLRAccountDB.setBelongYear(mLRAccountSchema.getBelongYear());
//			tLRAccountDB.setBelongMonth(mLRAccountSchema.getBelongMonth());
//			tLRAccountDB.setImportCount(mLRAccountSchema.getImportCount());
//			tLRAccountDB.setDataType(mLRAccountSchema.getDataType());
			tLRAccountDB.setLRSerialNo(mLRAccountSchema.getLRSerialNo());
			if(tLRAccountDB.getInfo()){
				if(mFeeType.equals("cess")&&flag.equals("01")){
					mLRAccountSchema.setCessPremState("01");
				}else if(mFeeType.equals("claim")&&flag.equals("01")){
					mLRAccountSchema.setReProcFeeState("01");
					mLRAccountSchema.setClaimBackFeeState("01");
				}else if(mFeeType.equals("cess")&&flag.equals("02")){
					mLRAccountSchema.setCessPremState("02");
				}else if(mFeeType.equals("claim")&&flag.equals("02")){
					mLRAccountSchema.setReProcFeeState("02");
					mLRAccountSchema.setClaimBackFeeState("02");					
					mLRAccountSchema.setAccountMoney(mLRAccountSchema.getCessPrem()-mLRAccountSchema.getReProcFee()-mLRAccountSchema.getClaimBackFee());
				}else if(mFeeType.equals("cess")&&flag.equals("00")){
					mLRAccountSchema.setCessPremState("00");
				}else{
					mLRAccountSchema.setReProcFeeState("00");
					mLRAccountSchema.setClaimBackFeeState("00");
				}
				mLRAccountSchema.setModifyDate(aModifyDate);
				mLRAccountSchema.setModifyTime(aModifyTime);
				mLRAccountSchema.setOprater(mOprater);
				mLRAccountSet.add(mLRAccountSchema);
			}else{
				CError tError = new CError();
				tError.errorMessage="记录不存在";
				return false;
			}
		}
		mMap.put(mLRAccountSet, "UPDATE");
		return true;
	}
	private boolean prepareOutputData(){
		try
	       {
	        	this.mOutputData.clear();
	            this.mOutputData.add(this.mMap);
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
	            return false;
	        }
		return true;
	}
	private boolean getInputData(VData data){
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
//		tLRAccountSet = (LRAccountSet) data.getObjectByObjectName("LRAccountSet", 0);
		
		if (mGI == null || tf == null|| tLRAccountSet == null) {
			CError tError = new CError();
			tError.moduleName = "LRDataTransferConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		mBelongYear=(String) tf.getValueByName("BelongYear");
		mBelongMonth=(String) tf.getValueByName("BelongMonth");
		mFeeType = (String)tf.getValueByName("feetype");
		mOprater = mGI.Operator;
		
		return true;
	}
}

