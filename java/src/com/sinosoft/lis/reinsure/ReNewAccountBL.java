package com.sinosoft.lis.reinsure;

/**
 * Copyright (c) 2008 sinosoft  Co. Ltd.
 * All right reserved.
 */

import java.io.File;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LRAccountsSet;
import com.sinosoft.lis.vschema.LRPolClmSet;
import com.sinosoft.lis.vschema.LRPolEdorSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.lis.db.LRAccountsDB;
import com.sinosoft.lis.db.LRPolClmDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.db.LRPolEdorDB;
import com.sinosoft.lis.vschema.LRComRiskResultSet;

/*
 * <p>ClassName: ReNewAccountBL </p>
 * <p>Description: ReNewAccountBL类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: Sun yu
 * @CreateDate：2008-10-11
 */
public class ReNewAccountBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();
    private LRComRiskResultSet mLRComRiskResultSet=new LRComRiskResultSet();

    //业务处理相关变量
    /** 全局数据 */

    public ReNewAccountBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
//        if (!dealData()) {
//            return false;
//        }
        if(this.strOperate.equals("JIESUAN")){//审核通过，修改状态
           if(check()==false){
               buildError("check()","审核失败");
               return false;
           }
           CreateAccountBL tCreateAccountBL=new CreateAccountBL();
           if(tCreateAccountBL.submitData(cInputData,strOperate)){
               buildError("CreateAccountBL.submitData()",tCreateAccountBL.mErrors.getFirstError());
           }
       }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
//    private boolean dealData() {
//
//        if(this.strOperate.equals("CHECK")){//审核通过，修改状态
//            if(check()==false){
//                buildError("check()","审核失败");
//                return false;
//            }
//        }
//        return true;
//    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
//        mTransferData = (TransferData) cInputData.getObjectByObjectName(
//                "TransferData", 0);
        mLRComRiskResultSet=(LRComRiskResultSet) cInputData.getObjectByObjectName(
                "LRComRiskResultSet", 0);
        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    /**
     * 审核通过，修改状态
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean check(){
        for(int i=1;i<=this.mLRComRiskResultSet.size();i++){
//            LRPolSchema tLRPolSchema=mLRPolSet.get(i);
//            String tSql="update LRPolClm set ActuGetState ='03',modifydate=current date,"
//                +"modifytime=current time where ActuGetState='02' and ActuGetNo='"+tLRPolSchema.getActuGetNo()+"' "
//                +" and recontcode='"+tLRPolSchema.getReContCode()+"' and riskcode='"+tLRPolSchema.getRiskCode()+"' with ur";
//            this.mMap.put(tSql, "UPDATE");
//
//            tSql="update LRPolEdor set ActuGetState='03',modifydate=current date,"
//                +"modifytime=current time where ActuGetState='02' and ActuGetNo='"+tLRPolSchema.getActuGetNo()+"' "
//                +" and recontcode='"+tLRPolSchema.getReContCode()+"' and riskcode='"+tLRPolSchema.getRiskCode()+"' with ur";
//            this.mMap.put(tSql, "UPDATE");
//
//            tSql="update LRPol set ActuGetState='03',modifydate=current date,"
//                +"modifytime=current time where ActuGetState='02' and ActuGetNo='"+tLRPolSchema.getActuGetNo()+"' "
//                +" and recontcode='"+tLRPolSchema.getReContCode()+"' and riskcode='"+tLRPolSchema.getRiskCode()+"' with ur";
             LRComRiskResultSchema tLRComRiskResultSchema=mLRComRiskResultSet.get(i);
             String tSql = "update  LRComRiskResult set standbyflag2='03' where riskcode='"+tLRComRiskResultSchema.getRiskCode()+
                          "' and standbyflag1='"+tLRComRiskResultSchema.getStandbyFlag1()+"' with ur";
            this.mMap.put(tSql, "UPDATE");
        }
            //准备往后台的数据
            if (!prepareOutputData()) return false;
            System.out.println("---End prepareOutputData---");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, strOperate)) {
              // @@错误处理
              this.mErrors.copyAllErrors(tPubSubmit.mErrors);
              return false;
            }
        return true;
    }

}

