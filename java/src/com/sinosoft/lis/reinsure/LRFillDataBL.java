/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.LRCessListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRCessListSchema;
import com.sinosoft.lis.vschema.LRCessListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: LRNewGetCessDataBL </p>
 * <p>Description: 再保补提数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 杨阳
 * @CreateDate：2016-11-21
 */
public class LRFillDataBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private LRCessListSet mLRCessListSet = new LRCessListSet();
    /** 数据操作字符串 */
    private String strOperate = "";

    //业务处理相关变量
    /** 全局数据 */

    public LRFillDataBL() {
        try {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if("getcessdata".equals(strOperate))
    	{
    		LRNewGetCessDataBL tLRNewGetCessDataBL = new LRNewGetCessDataBL();
    		tLRNewGetCessDataBL.submitData(cInputData, "filldata");
    	}
    	if("getedordata".equals(strOperate))
    	{
    		LRNewGetEdorDataBL tLRNewGetEdorDataBL = new LRNewGetEdorDataBL();
        	tLRNewGetEdorDataBL.submitData(cInputData, "filldata");
    	}
    	if("getclaimdata".equals(strOperate))
    	{
    		LRNewGetClaimDataBL tLRNewGetClaimDataBL = new LRNewGetClaimDataBL();
    		tLRNewGetClaimDataBL.submitData(cInputData, "filldata");
    	}
    	if("calcessdata".equals(strOperate))
    	{
    		LRNewCalCessDataBL tLRNewCalCessDataBL = new LRNewCalCessDataBL();
    		tLRNewCalCessDataBL.submitData(cInputData, "filldata");
    	}
    	if("caledordata".equals(strOperate))
    	{
    		LRNewCalEdorDataBL tLRNewCalEdorDataBL = new LRNewCalEdorDataBL();
    		tLRNewCalEdorDataBL.submitData(cInputData, "filldata");
    	}
    	if("calclaimdata".equals(strOperate))
    	{
    		LRNewCalClaimDataBL tLRNewCalClaimDataBL = new LRNewCalClaimDataBL();
    		tLRNewCalClaimDataBL.submitData(cInputData, "filldata");
    	}
    	if("getcesslist".equals(strOperate))
    	{
    		LRCessListBL tLRCessListBL = new LRCessListBL();
    		tLRCessListBL.submitData(cInputData, "filldata");
    	}
    	if("getclaimlist".equals(strOperate))
    	{
    		LRClaimListBL tLRClaimListBL = new LRClaimListBL();
    		tLRClaimListBL.submitData(cInputData, "filldata");
    	}
    	if("filltask".equals(strOperate))
    	{
    		ReContInterfaceFillTask tReContInterfaceFillTask = new ReContInterfaceFillTask();
    		tReContInterfaceFillTask.submitData(cInputData);
    	}
        return true;
    }
    public boolean submitHMData(VData cInputData, String cOperate) 
    {
    	if(getInputData(cInputData)==false)
    	{
    		return false;
    	}
    	if (!dealData()) {
            return false;
        }
    	PubSubmit tPubSubmit = new PubSubmit();
    	if(!tPubSubmit.submitData(mInputData, "INSERT"))
    	{
    		buildError("PubSubmit", "插入数据失败");
            return false;
    	}
    	return false;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
    	LRCessListSet tLRCessListSet = new LRCessListSet();
    	ExeSQL tExeSQL = new ExeSQL();
    	for(int i =1;i<=mLRCessListSet.size();i++)
    	{
    		LRCessListSet tOldLRCessListSet = new LRCessListSet();
    		LRCessListSchema tOldLRCessListSchema = new LRCessListSchema();
    		LRCessListSchema tLRCessListSchema = new LRCessListSchema();
    		tLRCessListSchema = mLRCessListSet.get(i);
    		String tRecontcodeSQL ="select cessionmode from lrcontinfo where  recontcode ='"+tLRCessListSchema.getReContCode()+"'";
    		String tCessionMode = tExeSQL.getOneValue(tRecontcodeSQL);
    		if(!"1".equals(tCessionMode))
    		{
    			buildError("dealData", "此功能只能对于成数合同进行维护！");
                return false;
    		}
    		String tOldDataSQL = "select * from lrcesslist where grpcontno ='"+tLRCessListSchema.getGrpContNo()+"'"
    				+ " and contno ='"+tLRCessListSchema.getContNo()+"'"
					+ " and polno = '"+tLRCessListSchema.getPolNo()+"'"
					+ " and recontcode = '"+tLRCessListSchema.getReContCode()+"'"
					+ " and reinsureitem = '"+tLRCessListSchema.getReinsureItem()+"'"
					+ " and payno ='"+tLRCessListSchema.getPayNo()+"'"
					+ " and renewcount = '"+tLRCessListSchema.getReNewCount()+"'"
					+ " and getdatadate = '"+tLRCessListSchema.getGetDataDate()+"'";
    		LRCessListDB tLRCessListDB = new LRCessListDB();
    		tOldLRCessListSet = tLRCessListDB.executeQuery(tOldDataSQL);
    		if(tOldLRCessListSet.size()>1)
    		{
    			buildError("dealData", "查询条数多于一条");
                return false;
    		}
    		tOldLRCessListSchema = tOldLRCessListSet.get(1);
    		double tPrem = tLRCessListSchema.getPrem();
    		double tCessPrem = tPrem * tOldLRCessListSchema.getCessionRate();
    		double tReprocfee = tCessPrem*tOldLRCessListSchema.getReProcFee()/tOldLRCessListSchema.getCessPrem();
    		tOldLRCessListSchema.setPrem(tLRCessListSchema.getPrem());
    		tOldLRCessListSchema.setCessPrem(tCessPrem);
    		tOldLRCessListSchema.setReProcFee(tReprocfee);
    		tOldLRCessListSchema.setGetDataDate(tLRCessListSchema.getMakeDate());
    		tOldLRCessListSchema.setReinsureItem(tLRCessListSchema.getAppntName());
    		tOldLRCessListSchema.setPayNo(tLRCessListSchema.getInsuredName());
    		tOldLRCessListSchema.setOprater("HM"+PubFun.getCurrentDate2());
    		tOldLRCessListSchema.setModifyDate(PubFun.getCurrentDate());
    		tOldLRCessListSchema.setModifyTime(PubFun.getCurrentTime());
    		tLRCessListSet.add(tOldLRCessListSchema);
    	}
    	MMap tMMap = new MMap();
    	tMMap.put(tLRCessListSet, "INSERT");
    	mInputData.add(tMMap);
        return true;
    }


    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLRCessListSet = (LRCessListSet) cInputData.getObjectByObjectName(
                "LRCessListSet", 0);
        return true;
    }

    public static void main(String[] args) {
        LRFillDataBL tLRNewGetCessDataBL = new LRFillDataBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";
        String mOperateType = "CESSDATA";

        String startDate = "2016-1-1";
        String endDate = "2016-8-31";
        TransferData getCessData = new TransferData();
        getCessData.setNameAndValue("StartDate", startDate);
        getCessData.setNameAndValue("EndDate", endDate);
        getCessData.setNameAndValue("ReContCode", "GER2010D02");
        VData tVData = new VData();
        tVData.addElement(globalInput);
        tVData.addElement(getCessData);

        tLRNewGetCessDataBL.submitData(tVData, mOperateType);
    }
}
