/**
 * Copyright (c) 2007 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.*;
import com.sinosoft.lis.reinsure.*;
/*
 * <p>ClassName: TempCessContUI </p>
 * <p>Description: TempCessContUI类文件 </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft </p>
 * @Modify by: huxl
 * @ModifyDate：2007-05-09
 */
public class TempCessContUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private String mResult = new String();

    public TempCessContUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        TempCessContBL tTempCessContBL = new TempCessContBL();
        if (!tTempCessContBL.submitData(cInputData, cOperate))
        {
            if (tTempCessContBL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tTempCessContBL.mErrors);
            }
            else
            {
                buildError("submitData", "TempCessContBL发生错误，但是没有提供详细信息！");
            }
            return false;
        }
         mResult = tTempCessContBL.getResult();
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        // 准备传输数据 VData
        VData vData = new VData();
        try
        {
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "TempCessContUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
