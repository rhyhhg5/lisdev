package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.LRAccountDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRAccountSchema;
import com.sinosoft.lis.vschema.LRAccountSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LRAccountSettleBL {
	public CErrors mErrors = new CErrors();
	private GlobalInput mGI = null;
	TransferData tf = null;
	private VData mOutputData = new VData();
	private LRAccountSet mLRAccountSet = new LRAccountSet();
	private LRAccountSet tLRAccountSet = new LRAccountSet();
	private String mModifyDate = PubFun.getCurrentDate();
	private String mModifyTime = PubFun.getCurrentTime();
	private String mAccountDate = PubFun.getCurrentDate();
	private MMap mMap = new MMap();
	private LRAccountDB tLRAccountDB = new LRAccountDB();
	private String mType ="";
	private String mYear = "";
	private String mQuarter = "";
	private String mMonth = "";
	private String mReComCode = "";
	private String mRecontType ="";
	private String mRecontCode ="";
	private String mOprater = "";
	private String mBranchNo ="";
	
	public boolean submitData(VData cInputData,String operate){
		if(!getInputData(cInputData)){
			return false;
		}
		if (!dealData()) {
		    CError tError = new CError();
            tError.moduleName = "LRListingForeastDataBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LRAccountSettleBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
		}
		if (!prepareOutputData())
        {
            return false;
        }
		 PubSubmit tPubSubmit = new PubSubmit();
	     if(!tPubSubmit.submitData(mOutputData, "")){
	    	 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             CError tError = new CError();
             tError.moduleName = "LRListingForeastDateBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据提交失败!";
             this.mErrors.addOneError(tError);
             return false;
	     }
	     return true;
	}
	
	private boolean getInputData(VData data){
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
		mLRAccountSet = (LRAccountSet) data.getObjectByObjectName("LRAccountSet", 0);
		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "LRAccountSettleBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		if("SEL".equals(mType)&&null==mLRAccountSet)
		{
			CError tError = new CError();
			tError.moduleName = "LRAccountSettleBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		mYear = (String)tf.getValueByName("year");
		mQuarter = (String)tf.getValueByName("quarter");
		mMonth = (String)tf.getValueByName("month");
		mReComCode = (String)tf.getValueByName("ReComCode");
		mRecontType = (String)tf.getValueByName("RecontType");
		mRecontCode = (String)tf.getValueByName("RecontCode");
		mType = (String)tf.getValueByName("Type");
		mOprater = mGI.Operator;
		
		return true;
	}
	
	private boolean dealData(){
		String tNoType = mYear+mQuarter;
		String aNo = PubFun1.CreateMaxNo(tNoType, 2);
		mBranchNo = tNoType+aNo;
		if("ALL".equals(mType))
		{
			LRAccountDB tLRAccountDB = new LRAccountDB();
			String sql ="select * from lraccount where cesspremstate ='02' and reprocfeestate='02' and claimbackfeestate ='02'  and belongyear ='"+mYear+"'  ";
			if(!"".equals(mQuarter))
			{
				sql+=" and belongQuarter ='"+mQuarter+"'";
			}
			if(!"".equals(mMonth))
			{
				sql+=" and belongmonth ='"+mMonth+"'";
			}
			if(!"".equals(mReComCode))
			{
				sql+=" and exists(select 1 from lrcontinfo where recomcode='"+mReComCode+"' and  recontcode = c.recontcode union select 1 from lrtempcesscont where comcode='"+mReComCode+"' and tempcontcode = c.recontcode)";
			}
			if("C".equals(mRecontType))
			{
				sql+=" and exists(select 1 from lrcontinfo where  recontcode = c.recontcode)";
			}
			if("T".equals(mRecontType))
			{
				sql+=" and exists(select 1 from lrtempcesscont where tempcontcode  = c.recontcode)";
			}
			if(!"".equals(mRecontCode))
			{
				sql+=" and RecontCode ='"+mRecontCode+"'";
			}
			mLRAccountSet = tLRAccountDB.executeQuery(sql);
		}
		try{
		for(int i=1;i<=mLRAccountSet.size();i++){
			LRAccountSchema tLRAccountSchema = new LRAccountSchema();
			tLRAccountSchema = mLRAccountSet.get(i);
			tLRAccountSchema.setAccountDate(mAccountDate);
			tLRAccountSchema.setAccountOprater(mOprater);
			tLRAccountSchema.setCessPremState("03");
			tLRAccountSchema.setReProcFeeState("03");
			tLRAccountSchema.setClaimBackFeeState("03");
			tLRAccountSchema.setModifyDate(mModifyDate);
			tLRAccountSchema.setModifyTime(mModifyTime);
			tLRAccountSchema.setBranchNo(mBranchNo);
			double aAccountMoney = tLRAccountSchema.getCessPrem()-tLRAccountSchema.getReProcFee()-tLRAccountSchema.getClaimBackFee();
			tLRAccountSchema.setAccountMoney(aAccountMoney);
			tLRAccountSet.add(tLRAccountSchema);
//			}
		}
		mMap.put(tLRAccountSet, "UPDATE");
		}catch(Exception e){		
			CError tError = new CError();
			tError.errorMessage  = "操作失败，原因是:数据处理失败LRAccountSettleBL-->dealData!" + e.toString();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	private boolean prepareOutputData(){
		try
	       {
	        	this.mOutputData.clear();
	            this.mOutputData.add(this.mMap);
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
	            return false;
	        }
		return true;
	}
}
