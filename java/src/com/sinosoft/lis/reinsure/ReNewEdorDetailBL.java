/**
 * Copyright (c) 2008 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import java.io.File;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LRPolClmSet;
import com.sinosoft.lis.vschema.LRPolEdorSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.lis.db.LRPolClmDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.db.LRPolEdorDB;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;

/*
 * <p>ClassName: ReNewContDetailBL </p>
 * <p>Description: ReNewContDetailBL类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: Sun yu
 * @CreateDate：2008-10-11  liuli modify 090318
 */
public class ReNewEdorDetailBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";

    private FileWriter mFileWriter;
    private BufferedWriter mBufferedWriter;

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();
    private String mStartDate = ""; //起始日期
    private String mEndDate = ""; //终止日期

    //业务处理相关变量
    /** 全局数据 */

    public ReNewEdorDetailBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

        if (this.strOperate.equals("CHECK")) { //审核通过，修改状态
            if (check() == false) {
                buildError("check()", "审核失败");
                return false;
            }
        } else if (this.strOperate.equals("ONLOAD")) { //下载清单
            if (onload() == false) {
                buildError("onload()", "下载清单失败");
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
//        mLRPolSet=(LRPolSet) cInputData.getObjectByObjectName(
//                "LRPolSet", 0);
        String tYear = (String) mTransferData.getValueByName("Year");
        String tMonth = (String) mTransferData.getValueByName("Month");
        if(!StrTool.cTrim(tYear).equals("") && !StrTool.cTrim(tMonth).equals("")){
            Calendar calendar=Calendar.getInstance();
            calendar.set(Calendar.YEAR,Integer.parseInt(tYear));
            calendar.set(Calendar.MONTH,Integer.parseInt(tMonth));
            calendar.set(Calendar.DATE,1);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            String tCurrentDate = sdf.format(calendar.getTime());
            calendar.set(Calendar.MONTH,Integer.parseInt(tMonth)-1);
            if("12".equals(tMonth))
            	mStartDate=tYear+"-"+tMonth+"-1";
            else
            	mStartDate=sdf.format(calendar.getTime());
            mEndDate= PubFun.calDate(tCurrentDate,-1,"D","2009-1-31");  //结束日期取本月最后一天
        } else {
            buildError("getInputData()", "请重新录入日期");
            return false;
        }

        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    /**
     * 审核通过，修改状态
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean check() {
        String tReComCode = (String) mTransferData.getValueByName("ReComCode");
        String tCessionMode = (String) mTransferData.getValueByName(
                "CessionMode");
        String tDiskKind = (String) mTransferData.getValueByName("DiskKind");
        String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
        String tContNo = (String) mTransferData.getValueByName("ContNo");
        String tAppntName = (String) mTransferData.getValueByName("AppntName");
        String tInsuredName = (String) mTransferData.getValueByName(
                "InsuredName");
        String tReContCode = (String) mTransferData.getValueByName("ReContCode");
        String tActuGetState = (String) mTransferData.getValueByName(
                "ActuGetState");
        String strWhere = " and b.getdatadate between '" + mStartDate +
                          "' and '" + mEndDate + "'";
        String strWhereT = " and b.getdatadate between '" + mStartDate +
                           "' and '" + mEndDate + "'";
        if (!StrTool.cTrim(tReComCode).equals("")) {
            strWhere += " and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='" +
                    tReComCode + "') ";
            strWhereT += " and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='" +
                    tReComCode + "') ";
        }
        if (!StrTool.cTrim(tCessionMode).equals("")) {
            strWhere += " and a.RiskCalSort='" + tCessionMode + "' ";
            strWhereT += " and a.RiskCalSort='" + tCessionMode + "' ";
        }
        if (!StrTool.cTrim(tDiskKind).equals("")) {
            strWhere += " and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='" +
                    tDiskKind + "') ";
            strWhereT += " and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='" +
                    tDiskKind + "') ";
        }
        if (!StrTool.cTrim(tRiskCode).equals("")) {
            strWhere += " and a.riskcode='" + tRiskCode + "' ";
            strWhereT += " and a.riskcode='" + tRiskCode + "' ";
        }
        if (!StrTool.cTrim(tContNo).equals("")) {
            strWhere += " and a.ContNo='" + tContNo + "' ";
            strWhereT += " and a.ContNo='" + tContNo + "' ";
        }
        if (!StrTool.cTrim(tAppntName).equals("")) {
            strWhere += " and a.AppntName like '%" + tAppntName + "%' ";
            strWhereT += " and a.AppntName like '%" + tAppntName + "%' ";
        }
        if (!StrTool.cTrim(tInsuredName).equals("")) {
            strWhere += " and a.InsuredName like '%" + tInsuredName + "%' ";
            strWhereT += " and a.InsuredName like '%" + tInsuredName + "%' ";
        }
        if (!StrTool.cTrim(tReContCode).equals("")) {
            strWhere += " and b.ReContCode ='" + tReContCode + "' ";
            strWhereT += " and b.ReContCode ='" + tReContCode + "' ";
        }
        if (!StrTool.cTrim(tActuGetState).equals("")) {
            strWhere += " and b.ActuGetState ='" + tActuGetState + "' ";
            strWhereT += " and b.ActuGetState ='" + tActuGetState + "' ";
        }
        String strSQL = "select b.* "
                        +"from LRPol a, LRPolEdor b,LRPolEdorResult c "
                        +" where a.polno = b.polno and b.polno = c.polno "
                        +"and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode "
                        +"  and b.ReNewCount = c.ReNewCount and a.reinsureitem=b.reinsureitem  "
                        + strWhere;
        if (!StrTool.cTrim(tReComCode).equals("") ||
            !StrTool.cTrim(tDiskKind).equals("")) {
            strSQL += " union all select b.* "
                    +"from LRPol a, LRPolEdor b,LRPolEdorResult c "
                    +" where a.polno = b.polno and b.polno = c.polno  "
                    +"and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode "
                    +" and b.ReNewCount = c.ReNewCount and a.reinsureitem = b.reinsureitem "
                    + strWhereT;
        }
        strSQL += " with ur";
        LRPolEdorSet tLRPolEdorSet = new LRPolEdorSet();
       RSWrapper rswrapper = new RSWrapper();
       rswrapper.prepareData(tLRPolEdorSet, strSQL);
       try {
            do {
                 rswrapper.getData();
                 mMap=new MMap();
                 for(int j=1;j<=tLRPolEdorSet.size();j++){
                   LRPolEdorSchema tLRPolEdorSchema=tLRPolEdorSet.get(j);
                   tLRPolEdorSchema.setActuGetState("01");
                   tLRPolEdorSchema.setModifyDate(PubFun.getCurrentDate());
                   tLRPolEdorSchema.setModifyTime(PubFun.getCurrentTime());
                   this.mMap.put(tLRPolEdorSchema, "UPDATE");
                 }
                 if (!prepareOutputData()) {
                    return false;
                 }
                PubSubmit tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算清单审核结果出错!");
                    }
                    return false;
                }
               } while (tLRPolEdorSet.size() > 0);
                rswrapper.close();
           } catch (Exception ex) {
                ex.printStackTrace();
                rswrapper.close();
            }
        return true;
    }

    private boolean onload() {
        String tReComCode = (String) mTransferData.getValueByName("ReComCode");
        String tCessionMode = (String) mTransferData.getValueByName(
                "CessionMode");
        String tDiskKind = (String) mTransferData.getValueByName("DiskKind");
        String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
        String tContNo = (String) mTransferData.getValueByName("ContNo");
        String tAppntName = (String) mTransferData.getValueByName("AppntName");
        String tInsuredName = (String) mTransferData.getValueByName(
                "InsuredName");
        String tReContCode = (String) mTransferData.getValueByName("ReContCode");
        String tActuGetState = (String) mTransferData.getValueByName(
                "ActuGetState");

        String tSysPath = (String) mTransferData.getValueByName("SysPath"); //获取地址;
        tSysPath += "reinsure/";
        String tPath = "";
        try {
            tPath = "BaoQuanMingXi" + PubFun.getCurrentDate2() + "_" +
                    PubFun.getCurrentTime2();
            mFileWriter = new FileWriter(tSysPath + tPath + ".txt");
            mBufferedWriter = new BufferedWriter(mFileWriter);

            String[] strArrHead = new String[15];
            strArrHead[0] = "险种代码";
            strArrHead[1] = "再保合同号";
            strArrHead[2] = "分保类型";
            strArrHead[3] = "保单号";
            strArrHead[4] = "投保人";
            strArrHead[5] = "被保险人";
            strArrHead[6] = "生效日期";
            strArrHead[7] = "签单日期";
            strArrHead[8] = "保全类型";
            strArrHead[9] = "保全起效日期";
            strArrHead[10] = "保额";
            strArrHead[11] = "标准保费";
            strArrHead[12] = "分出保额";
            strArrHead[13] = "分出保费";
            strArrHead[14] = "再保手续费";
            String head = "";
            for (int m = 0; m < strArrHead.length; m++) {
                head += strArrHead[m] + "|";
            }
            mBufferedWriter.write(head + "\r\n");
            mBufferedWriter.flush();
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }

        String strWhere = " and b.getdatadate between '" + mStartDate +
                          "' and '" + mEndDate + "'";
//        String strWhereT = "and b.getdatadate between '" + mStartDate +
//                           "' and '" + mEndDate + "'";
        if (!StrTool.cTrim(tReComCode).equals("")) {//去掉再保机构做为条件
//            strWhere += " and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='" +
//                    tReComCode + "') ";
//            strWhereT += " and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='" +
//                    tReComCode + "') ";
        }
        if (!StrTool.cTrim(tCessionMode).equals("")) {
            strWhere += " and a.RiskCalSort='" + tCessionMode + "' ";
//            strWhereT += " and a.RiskCalSort='" + tCessionMode + "' ";
        }
        if (!StrTool.cTrim(tDiskKind).equals("")) {//弃用险种类型为条件
//            strWhere += " and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='" +
//                    tDiskKind + "') ";
//            strWhereT += " and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='" +
//                    tDiskKind + "') ";
        }
        if (!StrTool.cTrim(tRiskCode).equals("")) {
            strWhere += " and b.riskcode='" + tRiskCode + "' ";
//            strWhereT += " and a.riskcode='" + tRiskCode + "' ";
        }
        if (!StrTool.cTrim(tContNo).equals("")) {
            strWhere += " and a.ContNo='" + tContNo + "' ";
//            strWhereT += " and a.ContNo='" + tContNo + "' ";
        }
        if (!StrTool.cTrim(tAppntName).equals("")) {
            strWhere += " and a.AppntName like '%" + tAppntName + "%' ";
//            strWhereT += " and a.AppntName like '%" + tAppntName + "%' ";
        }
        if (!StrTool.cTrim(tInsuredName).equals("")) {
            strWhere += " and a.InsuredName like '%" + tInsuredName + "%' ";
//            strWhereT += " and a.InsuredName like '%" + tInsuredName + "%' ";
        }
        if (!StrTool.cTrim(tReContCode).equals("")) {
            strWhere += " and b.ReContCode = '" + tReContCode + "' ";
//            strWhereT += " and a.ReContCode = '" + tReContCode + "' ";
        }
        if (!StrTool.cTrim(tActuGetState).equals("")) {
            strWhere += " and b.ActuGetState = '" + tActuGetState + "' ";
//            strWhereT += " and a.ActuGetState = '" + tActuGetState + "' ";
        }

        String strSQL = "select a.riskcode,a.ReContCode,"
                        +"(case a.RiskcalSort when '1' then '成数' when '2' then '溢额' end) CalSort, "
                        +" a.ContNo contno, a.AppntName, a.InsuredName, a.CValiDate, a.signdate,"
                        +"(case b.FeeoperationType when 'RB' then '退保回退' "
                        +" when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' "
                        +" when 'ZT' then '减人' else b.FeeoperationType end) FeeoperationType,"
                        +" b.EdorValidate,a.Amnt,a.StandPrem,c.CessionAmount,c.CessPrem,c.ReProcFee "
                        +"from LRPol a, LRPolEdor b, LRPolResult c, LRPolEdorResult e "
                        +" where a.polno = b.polno and b.polno = c.polno and c.polno = e.polno "
                        + "and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode and b.edorno=e.edorno "
                        +"and c.RecontCode = e.RecontCode and a.ReNewCount = c.ReNewCount "
                        +"and b.ReNewCount = e.ReNewCount  "
                        +"and a.conttype='1' " //个单的摊回（溢额和成数）
                        + strWhere
                        +" union all "
                        +"select a.riskcode,a.ReContCode,"
                        +"(case a.RiskcalSort when '1' then '成数' when '2' then '溢额' end) CalSort, "
                        +" a.ContNo contno, a.AppntName, a.InsuredName, a.CValiDate, a.signdate,"
                        + "'实收保费转出', "
                        + " b.EdorValidate,a.Amnt,a.StandPrem,c.CessionAmount,c.CessPrem,c.ReProcFee "
                        +"from LRPol a, LRPolEdor b, LRPolResult c, LRPolEdorResult e "
                        +" where a.polno = b.edorno and a.polno = c.polno and b.polno = e.polno and b.FeeoperationType = 'XB' "
                        + "and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode and b.edorno=e.edorno "
                        +"and c.RecontCode = e.RecontCode and a.ReNewCount = c.ReNewCount "
                        +"and b.ReNewCount = e.ReNewCount  "
                        +"and a.conttype='1' " //个单续保摊回
                        + strWhere
                        +"union all "
                        +"select a.riskcode,a.ReContCode,"
                        +"(case a.RiskcalSort when '1' then '成数' when '2' then '溢额' end) CalSort, "
                        +" a.GrpContNo grpcontno, a.AppntName, char(count(a.grpcontno)) suminsured, a.CValiDate, a.signdate,"
                        + "(case b.FeeoperationType when 'RB' then '退保回退' "
                        +" when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' "
                        +" when 'ZT' then '减人' else b.FeeoperationType end) FeeoperationType,"
                        + " b.EdorValidate,sum(a.Amnt),sum(a.StandPrem),sum(c.CessionAmount),sum(c.CessPrem),sum(c.ReProcFee) "
                        +"from LRPol a, LRPolEdor b, LRPolResult c, LRPolEdorResult e "
                        +" where a.polno = b.polno and b.polno = c.polno and c.polno = e.polno "
                        + "and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode and b.edorno=e.edorno "
                        +"and c.RecontCode = e.RecontCode and a.ReNewCount = c.ReNewCount "
                        +"and b.ReNewCount = e.ReNewCount  "
                        +"and a.conttype='2' and a.riskcalsort='1' " //团单的成数
                        + strWhere
                        +"group by a.riskcode,a.ReContCode,a.riskcalsort,a.grpcontno,a.AppntName,a.CValiDate, a.signdate,FeeoperationType,b.EdorValidate "
                        +"union all "
                        +"select a.riskcode,a.ReContCode,"
                        +"(case a.RiskcalSort when '1' then '成数' when '2' then '溢额' end) CalSort, "
                        +" a.GrpContNo grpcontno, a.AppntName,a.insuredname, a.CValiDate, a.signdate,"
                        + "(case b.FeeoperationType when 'RB' then '退保回退' "
                        +" when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' "
                        +" when 'ZT' then '减人' else b.FeeoperationType end) FeeoperationType,"
                        + " b.EdorValidate,a.Amnt,a.StandPrem,c.CessionAmount,c.CessPrem,c.ReProcFee "
                        +"from LRPol a, LRPolEdor b, LRPolResult c, LRPolEdorResult e "
                        +" where a.polno = b.polno and b.polno = c.polno and c.polno = e.polno "
                        + "and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode and b.edorno=e.edorno "
                        +"and c.RecontCode = e.RecontCode and a.ReNewCount = c.ReNewCount "
                        +"and b.ReNewCount = e.ReNewCount  "
                        +"and a.conttype='2' and a.riskcalsort='2' " //团险溢额
                        + strWhere
                        +"with ur ";


//        if (!StrTool.cTrim(tReComCode).equals("") ||
//            !StrTool.cTrim(tDiskKind).equals("")) {
//            strSQL += " union all select a.riskcode,a.ReContCode,"
//                    + "(select case CessionMode when '1' then '成数' when '2' then '溢额' end "
//                    +" from lrcontinfo where recontcode = a.recontcode) CessionMode, "
//                    +" a.ContNo contno, a.AppntName, a.InsuredName, a.CValiDate, a.signdate,"
//                    + "(case b.FeeoperationType when 'BF' then '保障调整' "
//                    +"when 'CM' then '客户资料变更' when 'CT' then '退保' when 'FX' then '个单复效' "
//                    + "when 'LQ' then '领取' when 'LR' then '保单遗失补发' when 'MJ' then '满期' "
//                    +"when 'NI' then '增人' when 'NS' then '新增险种' when 'TB' then '投保事项变更' "
//                    +"when 'WJ' then '无名单减人' when 'WT' then '犹豫期退保' when 'WZ' then '无名单增人' "
//                    + "when 'XT' then '协议退保' when 'ZB' then '追加保费' when 'ZT' then '减人' else b.FeeoperationType end) FeeoperationType,"
//                    +" b.EdorValidate,a.Amnt,a.StandPrem,c.CessionAmount,c.CessPrem,"
//                    +"c.ReProcFee + c.ChoiRebaFee + c.SpeCemm sum,a.ReNewCount,a.polno "
//                    +"from LRPol a, LRPolEdor b, LRPolResult c, LRPolEdorResult e "
//                    +" where a.polno = b.polno and b.polno = c.polno and c.polno = e.polno "
//                    +"and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode "
//                    + "and c.RecontCode = e.RecontCode and a.ReNewCount = b.ReNewCount "
//                    + "and b.ReNewCount = c.ReNewCount and c.ReNewCount = e.ReNewCount "
//                    + strWhereT;
//        }
//        strSQL += " order by ReContCode,FeeoperationType, EdorValidate desc with ur";

        int start = 1;
        int nCount = 10000;
        while (true) {
            SSRS tSSRS = new ExeSQL().execSQL(strSQL, start, nCount);
            int count = 0;
            if (tSSRS.getMaxRow() <= 0) {
                break;
            } else {
                count = tSSRS.getMaxRow();
            }
            if (tSSRS != null && count > 0) {
                for (int i = 1; i <= count; i++) {
                    try {
                        String result = "";
                        for (int m = 1; m <= tSSRS.getMaxCol(); m++) {
                            result += tSSRS.GetText(i, m) + "|";
                        }
                        mBufferedWriter.write(result + "\r\n");
                        mBufferedWriter.flush();
                    } catch (IOException ex2) {
                        ex2.printStackTrace();
                    }
                }
            }
            start += nCount;
        }
        try {
            mBufferedWriter.close();
        } catch (IOException ex3) {
        }
        String[] FilePaths = new String[1];
        FilePaths[0] = tSysPath + tPath + ".txt";
        String[] FileNames = new String[1];
        FileNames[0] = tPath + ".txt";
        String newPath = tSysPath + tPath + ".zip";
        String FullPath = tPath + ".zip";
        CreateZip(FilePaths, FileNames, newPath);
        try {
            File fd = new File(FilePaths[0]);
            fd.delete();
        } catch (Exception ex4) {
            ex4.printStackTrace();
        }
        mResult.add(FullPath);
        return true;
    }

    //生成压缩文件
    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this, "生成压缩文件失败");
            return false;
        }
        return true;
    }

}
