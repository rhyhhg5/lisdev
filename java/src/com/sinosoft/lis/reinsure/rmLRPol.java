package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.db.LRPolResultDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LRPolResultSchema;
import com.sinosoft.lis.vschema.LRPolResultSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;

public class rmLRPol {
	
	public static boolean rmRenewCont() {
		String dBeginDate = "2010-08-01";
		// String tSql =
		// "select * from lrpolresult where polno = '21023822130' and cessstartdate >='"
		// + dBeginDate + "' with ur";
		// String tSql = "select * from lrpolresult where cessstartdate >='" +
		// dBeginDate + "' with ur";
		String tSql = " select a.* from lrpolresult a "
				+ " where a.cessstartdate >='2010-08-01'"
				+ " and a.cessstartdate <='2010-08-31' "
				+ " and exists(select 1 from lcpol b where b.polno = a.polno and b.cvalidate <'2010-08-01') "
				+ " with ur";
		// String tSql = "select * from lrpol where getdatadate >='2010-08-01'";
		int count = 0;

		RSWrapper rsWrapper = new RSWrapper();
		LRPolResultSet tLRPolResultSet = new LRPolResultSet();
		try {
			if (!rsWrapper.prepareData(tLRPolResultSet, tSql)) {
				System.out.println("处理数据准备失败! ");
				return false;
			}

			do {
				rsWrapper.getData();
				if (tLRPolResultSet != null && tLRPolResultSet.size() > 0) {
					for (int i = 1; i <= tLRPolResultSet.size(); i++) {
						// do something
						LRPolResultSchema tLRPolResultSchema = new LRPolResultSchema();
						tLRPolResultSchema = tLRPolResultSet.get(i);
						String tPolNo = tLRPolResultSchema.getPolNo();
						String tDutyDate = tLRPolResultSchema
								.getCessStartDate();

						// String tQSql =
						// "select count(*) from lcpol where polno = '"+ tPolNo
						// +"' and cvalidate >= '" + dBeginDate + "' with ur";
						// ExeSQL tExeSQL = new ExeSQL();
						// String tFlag = tExeSQL.getOneValue(tQSql);
						// if(tFlag.equals("0")){//不在提数区间的新单，则删除LRPOL
						// LRPOLRESULT
						LRPolResultDB tLRPolResultDB = new LRPolResultDB();
						tLRPolResultDB.setSchema(tLRPolResultSchema);
						tLRPolResultDB.delete();

						tLRPolResultDB.setCessStartDate(PubFun.calDate(
								tDutyDate, 1, "Y", null));
						tLRPolResultDB.insert();

						LRPolDB tLRPolDB = new LRPolDB();
						tLRPolDB.setPolNo(tPolNo);
						tLRPolDB.setGetDataDate(tDutyDate);
						LRPolSet tLRPolSet = tLRPolDB.query();

						LRPolDB aLRPolDB = tLRPolSet.get(1).getDB();
						aLRPolDB.delete();

						aLRPolDB.setGetDataDate(PubFun.calDate(tDutyDate, 1,
								"Y", null));
						aLRPolDB.insert();
						count++;
						// }
					}
				}
			} while (tLRPolResultSet != null && tLRPolResultSet.size() > 0);

		} finally {
			rsWrapper.close();
		}

		System.out.println("共更新数据：" + count + "条。");
		return true;
	}
	
	public static void main(String[] args){
		
		rmLRPol.rmRenewCont();
//		System.out.println(PubFun.calDate("2010-08-01", 1, "Y", null));
		return;
	}
}
