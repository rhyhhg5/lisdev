/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.LRCessListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRCessListSchema;
import com.sinosoft.lis.schema.LRInterfaceSchema;
import com.sinosoft.lis.vschema.LRCessListSet;
import com.sinosoft.lis.vschema.LRInterfaceSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: LRNewGetCessDataBL </p>
 * <p>Description: 再保补提数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 杨阳
 * @CreateDate：2016-11-21
 */
public class LRInterfaceImportBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private LRCessListSet mLRCessListSet = new LRCessListSet();
    private TransferData mGetCessData = new TransferData();
    /** 数据操作字符串 */
    private String strOperate = "";

    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private	String mRiskCode = "";
    private String mGRiskCode = "";
    private	String mRiskCodeList = "";
    private	String mGRiskCodeList = "";
    private String mContNo = "";
    private String mGrpContNo = "";
    private String mPolNo = ""; 
    private String mCoperate = "";
    private	String mOperator = "";
    private String tSQL = "";
    //业务处理相关变量
    /** 全局数据 */

    public LRInterfaceImportBL() {
        try {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */

    public boolean submitData(VData cInputData, String cOperate) 
    {
    	if(!getInputData(cInputData,cOperate))
    	{
    		
    		return false;
    	}
    	if (!dealData()) {
            return false;
        }
//    	PubSubmit tPubSubmit = new PubSubmit();
//    	if(!tPubSubmit.submitData(mInputData, "INSERT"))
//    	{
//    		buildError("PubSubmit", "插入数据失败");
//            return false;
//    	}
    	return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
    	if("group".equals(mCoperate)){
    		 tSQL = "select c.GrpContNo, c.GrpPolNo,c.ContNo,c.PolNo, c.ProposalNo,c.PrtNo, c.ReNewCount,c.ContType,c.ManageCom, c.MainPolNo, c.MasterPolNo, c.RiskCode,c.RiskVersion, c.InsuredNo, c.InsuredName, c.InsuredSex,c.InsuredBirthday, c.InsuredAppAge, c.Years, c.CValiDate, c.EndDate, c.StandPrem,"
    			        +"db2inst1.GETREGRPPREM(c.grpcontno, finaInterTable.indexno,  c.polno, c.riskcode,  date(finaInterTable.accountdate)) as prem,"	
    				    +" c.SumPrem,  c.Amnt, c.OccupationType, c.PayIntv,  c.PayMode,  c.PayYears, c.PayEndYearFlag, c.PayEndYear, c.InsuYearFlag, c.InsuYear,"
    			        +"(select MarketType from lcgrpcont where GrpContNo = c.GrpContNo) as ProtItem,"
    				    +"'0',  c.SignDate, c.AppntNo,  c.AppntName,'',  c.SaleChnl, '01',c.PayToDate,finaInterTable.accountdate,'0', year(finaInterTable.accountdate - CValidate) + 1,c.agentgroup,c.cessamnt, current date,current time,'000000000011523','"+strOperate+"',c.MakeDate, c.MakeTime, c.ModifyDate,c.ModifyTime,'N' as RenewalFlag,  "
    			        +" (select case when cvalidate > signdate then cvalidate else signdate end   from lcgrpcont where grpcontno = c.grpcontno) as BelongDate, finaInterTable.indexno, c.contplancode, c.poltypeflag "
    			        +" from lcpol c,"
    			        +"(select b.indexno,   "
    			        +"b.grpcontno, "
    			        +"b.grppolno,  "
    			        +"a.riskcode,  "
    			        +"a.accountdate,"
    			        +"sum(case a.finitemtype "
    			        +" when 'C' then  "
    			        +" a.summoney    "
    			        +" else "
    			        +" -a.summoney   "
    			        +"   end) as prem"
    			        +"  from FivoucherDataDetail a, FiAbStandardData b    "
    			        +" where a.accountcode = '6031000000' "
    			        +"   and a.checkflag = '00'   "
    			        +"   and a.accountdate between '"+mStartDate+"' and '"+mEndDate+"'  "
    			        +"   and a.classtype in ('N-04', 'N-05') "
    			        +"   and a.serialno = b.serialno    "
    			        +"   and a.riskcode in ('"+mGRiskCode+"')  "
    			        +"   and exists (select 1"
    			        +"  from (select a.riskcode, d.recontcode"
    			        +"  from lmriskduty a,"
    			        +" lmdutygetrela b,"
    			        +" lrcalfactorvalue c,"
    			        +" lrcontinfo d"
    			        +" where d.retype = '02' "
    			        +"   and d.recontstate = '01' "
    			        +"   and d.recontcode = c.recontcode  "
    			        +"   and c.riskcode = b.getdutycode   "
    			        +"   and b.dutycode = a.dutycode "
    			        +"union"
    			        +" select c.riskcode, d.recontcode"
    			        +"  from lrcalfactorvalue c, lrcontinfo d  "
    			        +" where d.retype = '01' "
    			        +"   and d.recontcode = c.recontcode) as RecontRisk"
    			        +" where ('0' = '0' ) and RecontRisk.riskcode = a.riskcode)    "
    			        +" group by b.indexno, b.grpcontno, b.grppolno, a.riskcode,a.accountdate) as finaInterTable"
    			        +" where c.appflag= '1'"
    			        +"   and c.grppolno = finaInterTable.grppolno "
    			        +"   and c.grpcontno = finaInterTable.grpcontno   "
    			        +"   and c.riskcode in ('"+mGRiskCode+"')"
    			        +"   and c.conttype = '2' "
    			        +"   and exists (select 1"
    			        +"  from ljapayperson "
    			        +" where payno = finaInterTable.indexno"
    			        +"   and polno = c.polno)"
    			        +"   and not exists (select 1 "
    			        +"  from LRTempCessCont a, LRTempCessContInfo b"
    			        +" where a.tempcontcode = b.tempcontcode "
    			        +"   and a.contno = c.grpcontno"
    			        +"   and b.riskcode = c.riskcode "
    			        +"   and b.contplancode = c.contplancode "
    			        +"   and a.recontstate = '01') "
    			        +"   and not exists (select 1 "
    			        +" from lrinterface m "
    			        +" where c.polno=m.polno"
    			        +" and	c.renewcount=m.renewcount"
    			        +" and finaInterTable.accountdate=m.dutydate"
    			        +" and finaInterTable.indexno =m.payno "
    			        +" and  (year(finaInterTable.accountdate - CValidate) + 1)=m.polyear )"
    			        + " fetch first 10000 rows only with ur";
    	}
    	if("groupY".equals(mCoperate)){
    		tSQL="";
    	}
    	if("person".equals(mCoperate)){
    		
    	}
    	if("personY".equals(mCoperate)){
    		
    	}
    		SSRS tSSRS = new SSRS ();
    		ExeSQL tExeSQL = new ExeSQL();

    		do{
    			MMap tMMap = new MMap();
        		tSSRS = tExeSQL.execSQL(tSQL);
        		if(0<tSSRS.getMaxRow()){
    	    		LRInterfaceSet tNewLRInterfaceSet = new LRInterfaceSet();
        			for(int i = 1; i<=tSSRS.getMaxRow();i++){
        				
        				LRInterfaceSchema tNewLRInterfaceSchema = new LRInterfaceSchema();
        				
        				String aGrpContNo = tSSRS.GetText(i, 1);
        				String aGrpPolNo = tSSRS.GetText(i, 2);
        				String aContNo = tSSRS.GetText(i, 3);
        				String aPolNo = tSSRS.GetText(i, 4);
        				String aProposalNo = tSSRS.GetText(i, 5);
        				String aPrtNo = tSSRS.GetText(i, 6);
        				String aReNewCount = tSSRS.GetText(i, 7);
        				String aContType = tSSRS.GetText(i, 8);
        				String aManageCom = tSSRS.GetText(i, 9);
        				String aMainPolNo = tSSRS.GetText(i, 10);
        				String aMasterPolNo = tSSRS.GetText(i, 11);
        				String aRiskCode = tSSRS.GetText(i, 12);
        				String aRiskVersion = tSSRS.GetText(i, 13);
        				String aInsuredNo = tSSRS.GetText(i, 14);
        				String aInsuredName = tSSRS.GetText(i, 15);
        				String aInsuredSex = tSSRS.GetText(i, 16);
        				String aInsuredBirthday = tSSRS.GetText(i, 17);
        				String aInsuredAppAge = tSSRS.GetText(i, 18);
        				String aYears = tSSRS.GetText(i, 19);
        				String aCValiDate = tSSRS.GetText(i, 20);
        				String aEndDate = tSSRS.GetText(i, 21);
        				String aStandPrem = tSSRS.GetText(i, 22);
        				String aPrem = tSSRS.GetText(i, 23);
        				String aSumPrem = tSSRS.GetText(i, 24);
        				String aAmnt = tSSRS.GetText(i, 25);
        				String aOccupationType = tSSRS.GetText(i, 26);
        				String aPayIntv = tSSRS.GetText(i, 27);
        				String aPayMode = tSSRS.GetText(i, 28);
        				String aPayYears = tSSRS.GetText(i, 29);
        				String aPayEndYearFlag = tSSRS.GetText(i, 30);
        				String aPayEndYear = tSSRS.GetText(i, 31);
        				String aInsuYearFlag = tSSRS.GetText(i, 32);
        				String aInsuYear = tSSRS.GetText(i, 33);
        				String aProtItem = tSSRS.GetText(i, 34);
        				String aPolStat = tSSRS.GetText(i, 35);
        				String aSignDate = tSSRS.GetText(i, 36);
        				String aAppntNo = tSSRS.GetText(i, 37);
        				String aAppntName = tSSRS.GetText(i, 38);
        				String aAppntType = tSSRS.GetText(i, 39);
        				String aSaleChnl = tSSRS.GetText(i, 40);
        				String aPADate = tSSRS.GetText(i, 41);
        				String aPayToDate = tSSRS.GetText(i, 42);
        				String aDutyDate = tSSRS.GetText(i, 43);
        				String aState = tSSRS.GetText(i, 44);
        				String aPolYear = tSSRS.GetText(i, 45);
        				String aAgentGroup = tSSRS.GetText(i, 46);
        				String aCessAmnt = tSSRS.GetText(i, 47);
        				String aCreatDate = tSSRS.GetText(i, 48);
        				String aCreatTime = tSSRS.GetText(i, 49);
        				String aSerialNo = tSSRS.GetText(i, 50);
        				String aOperator = tSSRS.GetText(i, 51);
        				String aMakeDate = tSSRS.GetText(i, 52);
        				String aMakeTime = tSSRS.GetText(i, 53);
        				String aModifyDate = tSSRS.GetText(i, 54);
        				String aModifyTime = tSSRS.GetText(i, 55);
        				String aRenewalFlag = tSSRS.GetText(i, 56);
        				String aBelongDate = tSSRS.GetText(i, 57);
        				String aPayNo = tSSRS.GetText(i, 58);
        				String aContPlanCode = tSSRS.GetText(i, 59);
        				String aPolTypeFlag = tSSRS.GetText(i, 60);
        				
        				tNewLRInterfaceSchema.setGrpContNo(aGrpContNo);      	
        				tNewLRInterfaceSchema.setGrpPolNo(aGrpPolNo);
        				tNewLRInterfaceSchema.setContNo(aContNo);
        				tNewLRInterfaceSchema.setPolNo(aPolNo);
        				tNewLRInterfaceSchema.setProposalNo(aProposalNo);
        				tNewLRInterfaceSchema.setPrtNo(aPrtNo);
        				tNewLRInterfaceSchema.setReNewCount(aReNewCount);
        				tNewLRInterfaceSchema.setContType(aContType);
        				tNewLRInterfaceSchema.setManageCom(aManageCom);
        				tNewLRInterfaceSchema.setMainPolNo(aMainPolNo);
        				tNewLRInterfaceSchema.setMasterPolNo(aMasterPolNo);
        				tNewLRInterfaceSchema.setRiskCode(aRiskCode);
        				tNewLRInterfaceSchema.setRiskVersion(aRiskVersion);
        				tNewLRInterfaceSchema.setInsuredNo(aInsuredNo);
        				tNewLRInterfaceSchema.setInsuredName(aInsuredName);
        				tNewLRInterfaceSchema.setInsuredSex(aInsuredSex);
        				tNewLRInterfaceSchema.setInsuredBirthday(aInsuredBirthday);
        				tNewLRInterfaceSchema.setInsuredAppAge(aInsuredAppAge);
        				tNewLRInterfaceSchema.setYears(aYears);
        				tNewLRInterfaceSchema.setCValiDate(aCValiDate);
        				tNewLRInterfaceSchema.setEndDate(aEndDate);
        				tNewLRInterfaceSchema.setStandPrem(aStandPrem);
        				tNewLRInterfaceSchema.setPrem(aPrem);
        				tNewLRInterfaceSchema.setSumPrem(aSumPrem);
        				tNewLRInterfaceSchema.setAmnt(aAmnt);
        				tNewLRInterfaceSchema.setOccupationType(aOccupationType);
        				tNewLRInterfaceSchema.setPayIntv(aPayIntv);
        				tNewLRInterfaceSchema.setPayMode(aPayMode);
        				tNewLRInterfaceSchema.setPayYears(aPayYears);
        				tNewLRInterfaceSchema.setPayEndYearFlag(aPayEndYearFlag);
        				tNewLRInterfaceSchema.setPayEndYear(aPayEndYear);
        				tNewLRInterfaceSchema.setInsuYearFlag(aInsuYearFlag);
        				tNewLRInterfaceSchema.setInsuYear(aInsuYear);
        				tNewLRInterfaceSchema.setProtItem(aProtItem);
        				tNewLRInterfaceSchema.setPolStat(aPolStat);
        				tNewLRInterfaceSchema.setSignDate(aSignDate);
        				tNewLRInterfaceSchema.setAppntNo(aAppntNo);
        				tNewLRInterfaceSchema.setAppntName(aAppntName);
        				tNewLRInterfaceSchema.setAppntType(aAppntType);
        				tNewLRInterfaceSchema.setSaleChnl(aSaleChnl);
        				tNewLRInterfaceSchema.setPADate(aPADate);
        				if("".equals(aDutyDate)){
        					continue;
        				}
        				tNewLRInterfaceSchema.setPayToDate(aPayToDate);
        				if("".equals(aDutyDate)){
        					continue;
        				}
        				tNewLRInterfaceSchema.setDutyDate(aDutyDate);
        				tNewLRInterfaceSchema.setState(aState);
        				tNewLRInterfaceSchema.setPolYear(aPolYear);
        				tNewLRInterfaceSchema.setAgentGroup(aAgentGroup);
        				tNewLRInterfaceSchema.setCessAmnt(aCessAmnt);
        				tNewLRInterfaceSchema.setCreatDate(aCreatDate);
        				tNewLRInterfaceSchema.setCreatTime(aCreatTime);
        				tNewLRInterfaceSchema.setSerialNo(aSerialNo);
        				tNewLRInterfaceSchema.setOperator(aOperator);
        				tNewLRInterfaceSchema.setMakeDate(aMakeDate);
        				tNewLRInterfaceSchema.setMakeTime(aMakeTime);
        				tNewLRInterfaceSchema.setModifyDate(aModifyDate);
        				tNewLRInterfaceSchema.setModifyTime(aModifyTime);
        				tNewLRInterfaceSchema.setRenewalFlag(aRenewalFlag);
        				tNewLRInterfaceSchema.setBelongDate(aBelongDate);
        				tNewLRInterfaceSchema.setPayNo(aPayNo);
        				tNewLRInterfaceSchema.setContPlanCode(aContPlanCode);
        				tNewLRInterfaceSchema.setPolTypeFlag(aPolTypeFlag);
        				tNewLRInterfaceSet.add(tNewLRInterfaceSchema);
        			}
    	    	if(0<tNewLRInterfaceSet.size()){
	    			tMMap.put(tNewLRInterfaceSet, "INSERT");
	    		}
	        	VData tVData = new VData();
	        	tVData.add(tMMap);
	    		PubSubmit tPubSubmit = new PubSubmit();
	    		if(!tPubSubmit.submitData(tVData, "")){
	    			buildError("PubSubmit", "插入数据失败");
	    			return false;
	    		};
        		}
    		}while(0<tSSRS.getMaxRow());
    		
    		buildError("","数据已全部提交完毕");
//    		LRInterfaceSet tLRInterfaceSet = new LRInterfaceSet();
//    		RSWrapper tRSWrapper = new RSWrapper();
//    		tRSWrapper.prepareData(tLRInterfaceSet, tSQL);
//    		do{
//        		tRSWrapper.getData();
//    			MMap tMMap = new MMap();
//	    		LRInterfaceSet tNewLRInterfaceSet = new LRInterfaceSet();
//	    		for (int i =1;i<=tLRInterfaceSet.size();i++){
//	    			LRInterfaceSchema tLRInterfaceSchema = new LRInterfaceSchema();
//	    			LRInterfaceSchema tNewLRInterfaceSchema = new LRInterfaceSchema();
//	    			tLRInterfaceSchema = tLRInterfaceSet.get(i);
//	    			Reflections tReflections = new Reflections ();
//	    			tReflections.transFields(tNewLRInterfaceSchema, tLRInterfaceSchema);
//	    			tNewLRInterfaceSet.add(tNewLRInterfaceSchema);  			
//	    		}
//	    		if(0<tNewLRInterfaceSet.size()){
//	    			tMMap.put(tNewLRInterfaceSet, "INSERT");
//	    		}
//	        	VData tVData = new VData();
//	        	tVData.add(tMMap);
//	    		PubSubmit tPubSubmit = new PubSubmit();
//	    		if(!tPubSubmit.submitData(tVData, "")){
//	    			buildError("PubSubmit", "插入数据失败");
//	    			return false;
//	    		};
//	    		
//    		}while(0<tLRInterfaceSet.size());
    	

        return true;
    }


    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData,String cOperate) {
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");
	    mRiskCode = (String) mGetCessData.getValueByName("RiskCode");
	    mGRiskCode = (String) mGetCessData.getValueByName("GRiskCode");
	    mRiskCodeList = (String) mGetCessData.getValueByName("RiskCodeList");
	    mGRiskCodeList = (String) mGetCessData.getValueByName("GRiskCodeList");
        mGrpContNo = (String) mGetCessData.getValueByName("GrpContNo");
        mContNo = (String) mGetCessData.getValueByName("ContNo");
        mPolNo = (String) mGetCessData.getValueByName("PolNo");
        mOperator = (String) mGetCessData.getValueByName("Operator");
        mCoperate = cOperate;
        strOperate ="rx"+PubFun.getCurrentDate2();
        return true;
    }

    public static void main(String[] args) {
        LRInterfaceImportBL tLRNewGetCessDataBL = new LRInterfaceImportBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";
        String mOperateType = "CESSDATA";

        String startDate = "2016-1-1";
        String endDate = "2016-8-31";
        TransferData getCessData = new TransferData();
        getCessData.setNameAndValue("StartDate", startDate);
        getCessData.setNameAndValue("EndDate", endDate);
        getCessData.setNameAndValue("ReContCode", "GER2010D02");
        VData tVData = new VData();
        tVData.addElement(globalInput);
        tVData.addElement(getCessData);

        tLRNewGetCessDataBL.submitData(tVData, mOperateType);
    }
}
