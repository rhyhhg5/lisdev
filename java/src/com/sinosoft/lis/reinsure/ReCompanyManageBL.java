package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LRReComInfoSchema;
import com.sinosoft.lis.db.LRReComInfoDB;
import com.sinosoft.lis.vschema.LRReComInfoSet;
import com.sinosoft.lis.vschema.LRReComRelaSet;
import com.sinosoft.lis.db.LRReComRelaDB;
import com.sinosoft.lis.schema.LRReComRelaSchema;

public class ReCompanyManageBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
   public CErrors mErrors = new CErrors();

   /** 前台传入的公共变量 */
   private GlobalInput globalInput = new GlobalInput();

   /** 往后面传输数据的容器 */
   private VData mInputData = new VData();


   /** 数据操作字符串 */
   private String strOperate;
   private LRReComInfoSchema mLRReComInfoSchema = new LRReComInfoSchema();

   private LRReComRelaSet mLRReComRelaSet = new LRReComRelaSet();

   private MMap mMap = new MMap();

   private PubSubmit tPubSubmit = new PubSubmit();
   PubFun tPubFun = new PubFun();
   String currentDate = tPubFun.getCurrentDate();
   String currentTime = tPubFun.getCurrentTime();

   //业务处理相关变量
   /** 全局数据 */

   public ReCompanyManageBL() {
   }

   /**
    * 提交数据处理方法
    * @param cInputData 传入的数据,VData对象
    * @param cOperate 数据操作字符串
    * @return 布尔值（true--提交成功, false--提交失败）
    */
   public boolean submitData(VData cInputData, String cOperate) {
       System.out.println("Come in BL.Submit..............");
       this.strOperate = cOperate;
       if (strOperate == null || strOperate.equals("")) {
           buildError("verifyOperate", "不支持的操作字符串");
           return false;
       }

       System.out.println("before getInputData()........");
       if (!getInputData(cInputData)) {
           return false;
       }
       System.out.println("after getInputData()........");

       if (!dealData()) {
           return false;
       }

       return true;
   }

   public static void main(String[] args) {
       GlobalInput globalInput = new GlobalInput();
       globalInput.ComCode = "8611";
       globalInput.Operator = "001";

       ReComManageBL tReComManageBL = new ReComManageBL();

       LRReComInfoSet tLRReComInfoSet = new LRReComInfoSet();
       LRReComInfoSchema tLRReComInfoSchema = new LRReComInfoSchema();
//          tLRReComInfoSchema.setCertifyCode("HT01/05B");
//          tLRReComInfoSchema.setOrderMoney("0.26");

       tLRReComInfoSet.add(tLRReComInfoSchema);
       // prepare main plan
       // 准备传输数据 VData
       VData vData = new VData();

       try {
           vData.add(tLRReComInfoSchema);
           vData.add(tLRReComInfoSet);
           vData.add(globalInput);
           tReComManageBL.submitData(vData, "INSERT");
       } catch (Exception ex) {
           ex.printStackTrace();
       }
   }


   /**
    * 准备往后层输出所需要的数据
    * 输出：如果准备数据时发生错误则返回false,否则返回true
    */
   private boolean prepareOutputData() {
       System.out.println("Come to prepareOutputData()...........");
       try {
           this.mInputData.clear();
           //this.mInputData=new VData();
           //this.mInputData.add(this.mGlobalInput);
           this.mInputData.add(mMap);

       } catch (Exception ex) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LDComBL";
           tError.functionName = "prepareData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;

   }

   /**
    * 根据前面的输入数据，进行UI逻辑处理
    * 如果在处理过程中出错，则返回false,否则返回true
    */
   private boolean dealData() {
       //进行插入数据
       if (this.strOperate.equals("INSERT")) {
           if (!insertData()) {
               return false;
           }
       }
       //对数据进行修改操作
       if (this.strOperate.equals("UPDATE")) {
           if (!updateData()) {
               buildError("updateData", "修改再保公司信息有误!");
               return false;
           }
       }
       //对数据进行删除操作
       if (this.strOperate.equals("DELETE")) {
           if (!deleteData()) {
               buildError("deleteData", "删除再保公司时出现错误!");
               return false;
           }
       }
       return true;
   }

   /**
    * deleteData
    *
    * @return boolean
    */
   private boolean deleteData() {
       LRReComInfoDB tLRReComInfoDB = new LRReComInfoDB();
       tLRReComInfoDB.setReComCode(mLRReComInfoSchema.getReComCode());
       if (!tLRReComInfoDB.getInfo()) {
           buildError("deleteData", "该再保公司不存在!");
           return false;
       }
       LRReComInfoSchema tLRReComInfoSchema = tLRReComInfoDB.getSchema();
       tLRReComInfoSchema.setReComCode(mLRReComInfoSchema.getReComCode());
       mMap.put(tLRReComInfoSchema, "DELETE");

       if (!prepareOutputData()) {
           return false;
       }
       if (!tPubSubmit.submitData(this.mInputData, "")) {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("updateData", "修改时出现错误!");
               return false;
           }
       }
       System.out.println("after PubSubmit ............");
       mMap = null;
       tPubSubmit = null;
       return true;
   }

   /**
    * updateData
    *
    * @return boolean
    */
   private boolean updateData() {
       LRReComInfoDB tLRReComInfoDB = new LRReComInfoDB();
       tLRReComInfoDB.setReComCode(mLRReComInfoSchema.getReComCode());
       if (!tLRReComInfoDB.getInfo()) {
           buildError("insertData", "该再保公司不存在!");
           return false;
       }

       LRReComInfoSchema tLRReComInfoSchema = tLRReComInfoDB.getSchema();
       tLRReComInfoSchema.setReComCode(mLRReComInfoSchema.getReComCode());
       tLRReComInfoSchema.setReComName(mLRReComInfoSchema.getReComName());
       tLRReComInfoSchema.setSimpName(mLRReComInfoSchema.getSimpName());
//       tLRReComInfoSchema.setEngName(mLRReComInfoSchema.getEngName());
       tLRReComInfoSchema.setPostalCode(mLRReComInfoSchema.getPostalCode());
       tLRReComInfoSchema.setAddress(mLRReComInfoSchema.getAddress());
       tLRReComInfoSchema.setFaxNo(mLRReComInfoSchema.getFaxNo());
       tLRReComInfoSchema.setSAPAppntNo(mLRReComInfoSchema.getSAPAppntNo());
       tLRReComInfoSchema.setCompanyCode(mLRReComInfoSchema.getCompanyCode());
       tLRReComInfoSchema.setModifyDate(currentDate);
       tLRReComInfoSchema.setModifyTime(currentTime);
//       tLRReComInfoSchema.setWebAddress(mLRReComInfoSchema.getWebAddress());
//       tLRReComInfoSchema.setNote(mLRReComInfoSchema.getNote());

       mMap.put(tLRReComInfoSchema, "UPDATE");

       if (!prepareOutputData()) {
           return false;
       }
       if (!tPubSubmit.submitData(this.mInputData, "")) {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("updateData", "修改信息时出现错误!");
               return false;
           }
       }
       System.out.println("after PubSubmit ............");
       mMap = null;
       tPubSubmit = null;
       return true;
   }

   /**
    * insertData
    *
    * @return boolean
    */
   private boolean insertData() {
       LRReComInfoDB tLRReComInfoDB = new LRReComInfoDB();
       tLRReComInfoDB.setReComCode(mLRReComInfoSchema.getReComCode());
       System.out.println("come in insert........");
       if (tLRReComInfoDB.getInfo()) {
           buildError("insertData", "该再保公司已经存在!");
           return false;
       }



       mLRReComInfoSchema.setReComCode(createWorkNo("RECOMCODE"));
       mLRReComInfoSchema.setOperator(globalInput.Operator);
       mLRReComInfoSchema.setManageCom(globalInput.ManageCom);
       mLRReComInfoSchema.setMakeDate(currentDate);
       mLRReComInfoSchema.setMakeTime(currentTime);
       mLRReComInfoSchema.setModifyDate(currentDate);
       mLRReComInfoSchema.setModifyTime(currentTime);

       mMap.put(mLRReComInfoSchema, "INSERT");

       if (!prepareOutputData()) {
           return false;
       }
       if (!tPubSubmit.submitData(this.mInputData, "")) {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("insertData", "保存再保公司信息时时出现错误!");
               return false;
           }
       }
       System.out.println("after PubSubmit ............");
       mMap = null;
       tPubSubmit = null;

       return true;
   }

   public String createWorkNo(String ags) {
       String tWorkNo = "";
//       if (ags.equals("RECOMCODE")) {
       tWorkNo = PubFun1.CreateMaxNo("RECOMCODE", 3);
//       } else {
////           tWorkNo = PubFun1.CreateMaxNo("RELATION", 6);
//       }
       return tWorkNo;
   }

   /**
    * 从输入数据中得到所有对象
    * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) {
       globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
               "GlobalInput", 0));
       this.mLRReComInfoSchema.setSchema((LRReComInfoSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LRReComInfoSchema", 0));
       return true;
   }

   public String getResult() {
       return mLRReComInfoSchema.getReComCode();
   }


   /*
    * add by kevin, 2002-10-14
    */
   private void buildError(String szFunc, String szErrMsg) {
       CError cError = new CError();

       cError.moduleName = "ReComManageBL";
       cError.functionName = szFunc;
       cError.errorMessage = szErrMsg;
       this.mErrors.addOneError(cError);
   }

}
