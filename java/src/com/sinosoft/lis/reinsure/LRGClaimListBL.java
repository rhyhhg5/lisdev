package com.sinosoft.lis.reinsure;

import java.util.Date;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRClaimListSchema;
import com.sinosoft.lis.vschema.LRClaimListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: LRClaimListBL </p>
 * <p>Description: 提取再保新单数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 孙宇
 * @CreateDate：2008-10-30
 */
public class LRGClaimListBL {
	


		    /** 错误处理类，每个需要错误处理的类中都放置该类 */
		    public CErrors mErrors = new CErrors();

		    /** 前台传入的公共变量 */
		    private GlobalInput globalInput = new GlobalInput();


		    /** 往后面传输数据的容器 */
//		    private VData mInputData = new VData();

		    /** 数据操作字符串 */
		    private String strOperate = "";
		    private String mToday = "";
		    private String mStartDate = "";
		   

		    private TransferData mGetCessData = new TransferData();

		    private MMap tmap = new MMap();
		    private VData mOutputData = new VData();
		    


		    //业务处理相关变量
		    /** 全局数据 */

		    public LRGClaimListBL() {
		    }

		    /**
		     * 提交数据处理方法
		     * @param cInputData 传入的数据,VData对象
		     * @param cOperate 数据操作字符串
		     * @return 布尔值（true--提交成功, false--提交失败）
		     */
		    public boolean submitDataT(String cOperate,String tToDate) {
		        System.out.println("Begin LRClaimListBL.Submit..............");
		        this.strOperate = cOperate;
		        if (strOperate.equals("")) {
		            buildError("verifyOperate", "不支持的操作字符串");
		            return false;
		        }
		        if (!getInputData(tToDate)) {
		            return false;
		        }
		        if (!dealData()) {
		            return false;
		        }
		        //准备往后台的数据
		        
		        
		        return true;
		    }


		    /**
		     * 根据前面的输入数据，进行UI逻辑处理
		     * 如果在处理过程中出错，则返回false,否则返回true
		     */
		    private boolean dealData() {

		        
		            if (!getCessData()) {
		                return false;
		            }
		        
		        return true;
		    }

		    /**
		     * 提取分保数据
		     * @return boolean
		     */
		    private boolean getCessData() {
		        FDate chgdate = new FDate();
		        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
		       
             
//		        Connection con=DBConnPool.getConnection();
		        try{
		            //con.setAutoCommit(false);
		           
		                System.out.println("开始比较:"+dbdate);
		            	mToday = chgdate.getString(dbdate); //录入的正在统计的日期
		                if (!insertPolData()) {
		                    buildError("getCessData", "提取" + mToday + "日新单数据出错");
		                    return false;
		                }
		                
		            
		            
		            
		        }catch(Exception e){
		                e.printStackTrace();
		        }

		        return true;
		    }
		    
		     /**
		     * 准备后台的数据
		     * @return boolean
		     */
		    private boolean prepareOutputData()
		    {
		        try
		        {
		        	this.mOutputData.clear();
		            this.mOutputData.add(tmap);
		        }
		        catch (Exception ex)
		        {
		            // @@错误处理
		            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
		            return false;
		        }
		        return true;
		    }
		    //执行SQL,并将取出的数据插入到:
		    private boolean insertPolData(){
		    	String currentdate=PubFun.getCurrentDate();
		    	String currenttime=PubFun.getCurrentTime();
		    	String sql = null ;
		    	//临时分保->0  合同分保->1
		    	sql=" select  a.ManageCom as 机构代码,a.recontcode as 再保合同号,a.RiskCode as 产品代码,"+
                " a.ContNo as 保单号,a.grpContNo as 团单号,"+
                " (case t.conttype when '1' then t.CValidate" +
                " else (select CValidate from lcgrpcont where grpcontno=a.grpContNo " +
                " union all select CValidate from lbgrpcont where grpcontno=a.grpContNo) end ) as 生效日期," +
                "t.enddate as 失效日期," +
                " (case t.conttype when '1' then t.SignDate" +
                " else (select SignDate from lcgrpcont where grpcontno=a.grpContNo " +
                " union all select SignDate from lbgrpcont where grpcontno=a.grpContNo) end) as 签单日期,t.AppntName as 投保人,"+
                " t.AppntNo as 投保人客户号,t.InsuredName as 被保险人,char(t.InsuredBirthday) as 出生日期,t.InsuredSex as 性别,"+
                " (select IdNo from LDPerson where CustomerNo = t.InsuredNo) as 身份证号码,"+
                " (case when t.OccupationType is null then '1' else t.OccupationType end) as 职业类别,"+
                " t.amnt as 保额,a.ClaimMoney as 索赔额,a.RealPay as 赔付金额,a.Diagnoses as 诊断,a.AccDesc as 出险原因,a.AccidentDate as 出险日期,"+
                " a.LeaveHospDate as 出院日期,a.RgtDate as 索赔日期,a.getdatadate as 结案日期,a.GiveTypeDesc as 理赔结论,"+
                " (case (select count(1) from lrcontinfo where recontcode = a.recontcode) when 0 then '临时分保' else '合同分保' end) as 分保类型,"+
                " (select Coalesce((select c.cessionrate from lrpolresult c where (polno=t.masterPolno or polno=a.polno) and c.RecontCode = a.RecontCode   fetch first  1 rows only),0) from dual)  as 分保比例,"+
                " (select Coalesce(sum(c.CessionAmount),0)  from lrpolresult c where (polno=t.masterPolno or polno=a.polno)"+
                " and c.RecontCode = a.RecontCode and c.renewcount=a.renewcount and c.reinsureitem=a.reinsureitem ) as 分保保额,d.ClaimBackFee as 分保摊回赔款,a.polno,a.ClmNo,a.ReinsureItem,a.costcenter  "+
                " FROM LRPolClm a, LRPolClmResult d,(select conttype,CValidate,enddate,SignDate,AppntName,AppntNo,InsuredBirthday,"+
                " InsuredSex,InsuredNo,OccupationType,amnt,polno,InsuredName,masterPolno from lcpol where polno in"+
                " (select polno from lrpolclm where GetDataDate = '"+mToday+"') union all"+
                " select conttype,CValidate,enddate,SignDate,AppntName,AppntNo,InsuredBirthday,"+
                " InsuredSex,InsuredNo,OccupationType,amnt,polno,InsuredName,masterPolno from lbpol  where polno in"+
                " (select polno from lrpolclm where GetDataDate = '"+mToday+"')) as t"+
                " where  a.reinsureitem='G' and a.PolNo = d.Polno and a.RecontCode = d.RecontCode " +
                "and a.ClmNo = d.ClmNo and a.reinsureitem=d.reinsureitem "+
                " and a.polno=t.polno and a.GetDataDate = '"+mToday+"' and d.ClaimBackFee<>0 with ur ";
		    	try{

		    		SSRS tSSRS = new SSRS();
		        	RSWrapper rsWrapper = new RSWrapper();
		        	if (!rsWrapper.prepareData(null, sql))
		            {
		                System.out.println("数据准备失败! ");
		                return false;
		            }

		        	 do {
		             	tSSRS = rsWrapper.getSSRS();
		             	tmap = new MMap();
		     			if (tSSRS != null || tSSRS.MaxRow > 0) {
		        	
		        	for(int i=1;i<=tSSRS.MaxRow;i++){
		        		LRClaimListSchema tLRClaimListSchema = new LRClaimListSchema();
		        		String aReContCode      = tSSRS.GetText(i,2);
		        		String aRiskCode        = tSSRS.GetText(i,3);
		        		String aContNo          = tSSRS.GetText(i,4);
		        		String aGrpContNo       = tSSRS.GetText(i,5);
		        		String aCValiDate       = tSSRS.GetText(i,6);
		        		String aEndDate         = tSSRS.GetText(i,7);
		        		String aSignDate        = tSSRS.GetText(i,8);
		        		String aAppntName       = tSSRS.GetText(i,9);
		        		String aAppntNo         = tSSRS.GetText(i,10);
		        		String aInsuredName     = tSSRS.GetText(i,11);
		        		String aInsuredBirthday = tSSRS.GetText(i,12);
		        		String aInsuredSex      = tSSRS.GetText(i,13);
		        		String aInsuredIdNo     = tSSRS.GetText(i,14);
		        		String aOccupationType  = tSSRS.GetText(i,15);
		        		String aAmnt            = tSSRS.GetText(i,16);
		        		String aClaimMoney      = tSSRS.GetText(i,17);
		        		String aRealPay         = tSSRS.GetText(i,18);
		        		String aDiagnoses       = tSSRS.GetText(i,19);
		        		String aAccDesc         = tSSRS.GetText(i,20);
		        		String aAccidentDate    = tSSRS.GetText(i,21);
		        		String aLeaveHospDate   = tSSRS.GetText(i,22);
		        		String aRgtDate         = tSSRS.GetText(i,23);
		        		String agetdatadate     = tSSRS.GetText(i,24);
		        		String aGiveTypeDesc    = tSSRS.GetText(i,25);
		        		String aTempCessFlag    = tSSRS.GetText(i,26);
		        		String aCessionRate     = tSSRS.GetText(i,27);
		        		String aCessionAmount   = tSSRS.GetText(i,28);
		        		String aClaimBackFee    = tSSRS.GetText(i,29);
		        		String aPolNo           = tSSRS.GetText(i,30);
		        		String aClmNo           = tSSRS.GetText(i,31);
		        		String aReinsureItem    = tSSRS.GetText(i,32);
		        		String aManageCom       = tSSRS.GetText(i,1);
		        		String aCostCenter       = tSSRS.GetText(i,33);
		        		
		        		tLRClaimListSchema.setReContCode(aReContCode);     
		        		tLRClaimListSchema.setRiskCode(aRiskCode);		        		
		        		tLRClaimListSchema.setContNo(aContNo);          
		        		tLRClaimListSchema.setGrpContNo(aGrpContNo);		        		
		        		tLRClaimListSchema.setCValiDate(aCValiDate);       
		        		tLRClaimListSchema.setEndDate(aEndDate);         
		        		tLRClaimListSchema.setSignDate(aSignDate);        
		        		tLRClaimListSchema.setAppntName(aAppntName);       
		        		tLRClaimListSchema.setAppntNo(aAppntNo);         
		        		tLRClaimListSchema.setInsuredName(aInsuredName);     
		        		tLRClaimListSchema.setInsuredBirthday(aInsuredBirthday); 
		        		tLRClaimListSchema.setInsuredSex(aInsuredSex);      
		        		tLRClaimListSchema.setInsuredIdNo(aInsuredIdNo);     
		        		tLRClaimListSchema.setOccupationType(aOccupationType);  
		        		tLRClaimListSchema.setAmnt(aAmnt);            
		        		tLRClaimListSchema.setClaimMoney(aClaimMoney);      
		        		tLRClaimListSchema.setRealPay(aRealPay);         
		        		tLRClaimListSchema.setDiagnoses(aDiagnoses);       
		        		tLRClaimListSchema.setAccDesc(aAccDesc);         
		        		tLRClaimListSchema.setAccidentDate(aAccidentDate);    
		        		tLRClaimListSchema.setLeaveHospDate(aLeaveHospDate);   
		        		tLRClaimListSchema.setRgtDate(aRgtDate);         
		        		tLRClaimListSchema.setgetdatadate(agetdatadate);     
		        		tLRClaimListSchema.setGiveTypeDesc(aGiveTypeDesc);    
		        		tLRClaimListSchema.setTempCessFlag(aTempCessFlag);
		        		tLRClaimListSchema.setClmNo(aClmNo);
		        		tLRClaimListSchema.setReinsureItem(aReinsureItem);
		        		tLRClaimListSchema.setCostCenter(aCostCenter);
//		        		System.out.println("aCessionRate:" +aCessionRate+":aCessionRate");
		        		if(aCessionRate != null && !aCessionRate.equals(""))
		        		    {
		        			    
		        			    tLRClaimListSchema.setCessionRate(aCessionRate); 
		        			}//
		        		else{
		        			aCessionRate="0";
		        		}
		        		if(aCessionAmount != null && !aCessionAmount.equals(""))
	        		    {
		        			
		        			tLRClaimListSchema.setCessionAmount(aCessionAmount); 
	        		    }
		        		else{
		        			aCessionAmount="0";
		        		}
		        		if(aClaimBackFee != null && !aClaimBackFee.equals(""))
		        		{
		        			
		        			tLRClaimListSchema.setClaimBackFee(aClaimBackFee);
		        		}		
		        		else{
		        			aClaimBackFee="0";
		        		}
		        		tLRClaimListSchema.setManageCom(aManageCom);       
		        		tLRClaimListSchema.setListDate(agetdatadate);        
		        		tLRClaimListSchema.setMakeDate(currentdate);        
		        		tLRClaimListSchema.setMakeTime(currenttime);        
		        		tLRClaimListSchema.setModifyDate(currentdate);      
		        		tLRClaimListSchema.setModifyTime(currenttime);      
		        		tLRClaimListSchema.setOprater("Server");         
		        		tLRClaimListSchema.setPolNo(aPolNo);
		        		tmap.put(tLRClaimListSchema, "DELETE&INSERT");
//		        		mLRClaimListSet.add(tLRClaimListSchema);
		        	}
		        	if (!prepareOutputData()) {
	                    return false;
	                }
	        	 PubSubmit tPubSubmit = new PubSubmit();
			     tPubSubmit.submitData(mOutputData, "");
			     //如果有需要处理的错误，则返回
			     if (tPubSubmit.mErrors.needDealError())
			       {
			            // @@错误处理
			            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			            CError tError = new CError();
			            tError.moduleName = "LRCessListBL";
			            tError.functionName = "submitDat";
			            tError.errorMessage = "数据提交失败!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
	     			}
	     			}while (tSSRS != null && tSSRS.MaxRow > 0);
	     			rsWrapper.close();
		    		 
		        	
		    	}catch(Exception ex){
		    		ex.printStackTrace();
		    		return false;
		    	}
		    	
		    	
		    	
		    	return true;
		    }
		    
		    //错误信息
		    private void buildError(String szFunc, String szErrMsg) {
		        CError cError = new CError();
//		        cError.moduleName = "LRClaimListBL";
//		        cError.functionName = szFunc;
//		        cError.errorMessage = szErrMsg;
//		        this.mErrors.addOneError(cError);
		    }

		    
		    private boolean getInputData(String tToDate) {
		       
		        mStartDate =tToDate;
		       
		        
		        return true;
		    }
		    /**
		     * 从输入数据中得到所有对象
		     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		     */

		    public String getResult() {
		        return "没有传数据";
		    }
		    

		    public static void main(String[] args) {
		        LRNewGetCessDataBL tLRNewGetCessDataBL = new LRNewGetCessDataBL();
		        GlobalInput globalInput = new GlobalInput();
		        globalInput.ManageCom = "86";
		   
		        String mOperateType = "CESSDATA";

		        String startDate = "2008-10-10";
		        String endDate = "2008-10-10";
		        TransferData getCessData = new TransferData();
		        getCessData.setNameAndValue("StartDate", startDate);
		        getCessData.setNameAndValue("EndDate", endDate);

		        VData tVData = new VData();
		        tVData.addElement(globalInput);
		        tVData.addElement(getCessData);

		        tLRNewGetCessDataBL.submitData(tVData, mOperateType);
		    }
}
