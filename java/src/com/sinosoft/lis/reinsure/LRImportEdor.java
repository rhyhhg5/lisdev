package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.LRPolEdorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRPolEdorSchema;
import com.sinosoft.lis.vschema.LRPolEdorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入的被保人清单添加到数据库 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class LRImportEdor {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    /** 保全号 */
    private String mEdorNo = null;
    /** 批次号 */
    private String mBatchNo = null;
    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();
    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    /** 节点名 */
    private String sheetName = "Sheet1";
    /** 配置文件名 */
    private String configName = "LREdorImport.xml";
    private LRPolEdorSet mLRPolEdorSet = new LRPolEdorSet(); //再保险保单表
    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInput
     */
    public LRImportEdor(GlobalInput gi) {
        this.mGlobalInput = gi;
    }

    /**
     * 添加被保人清单
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName){
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        LRImportEdorAct tLRImportEdorAct = new LRImportEdorAct(path +
                fileName,
                path + configName,
                sheetName);
        if (!tLRImportEdorAct.doImport()) {
            this.mErrors.copyAllErrors(tLRImportEdorAct.mErrrors);
            return false;
        }
        LRPolEdorSet tLRPolEdorSet = (LRPolEdorSet) tLRImportEdorAct.getSchemaSet();
        System.out.println("LRPolEdorSet的长度: "+tLRPolEdorSet.size());
        //校验导入的数据
        if (!checkData(tLRPolEdorSet)) {
            return false;
        }
        //存放Insert Into语句的容器
        MMap map = new MMap();
        for (int i = 1; i <= tLRPolEdorSet.size(); i++) {
            addOneLRPolEdor(tLRPolEdorSet.get(i));
        }
        map.put(mLRPolEdorSet, "DELETE&INSERT");
        //this.mResult.add(tLRPolSet);

        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }

        return true;
    }

    /**
     * 校验导入数据
     * @param cLZCardPaySet LZCardPaySet
     * @return boolean
     */
    private boolean checkData(LRPolEdorSet aLRPolEdorSet) {
        for (int i = 1; i <= aLRPolEdorSet.size(); i++) {
            LRPolEdorSchema schema = aLRPolEdorSet.get(i);
            return true;
        }
        return true;
    }

    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private boolean addOneLRPolEdor(LRPolEdorSchema aLRPolEdorSchema) {
        LRPolEdorDB tLRPolEdorDB = new LRPolEdorDB();
        tLRPolEdorDB.setPolNo(aLRPolEdorSchema.getPolNo());
        tLRPolEdorDB.setReinsureCom(aLRPolEdorSchema.getReinsureCom());
        tLRPolEdorDB.setReinsurItem(aLRPolEdorSchema.getReinsurItem());
        tLRPolEdorDB.setInsuredYear(aLRPolEdorSchema.getInsuredYear());
        if(!tLRPolEdorDB.getInfo())
        {
           buildError("addOneLRPol","险种保单号为:"+tLRPolEdorDB.getPolNo()+"的保单出错!");
            return false;
        }
        LRPolEdorSchema tLRPolEdorSchema = tLRPolEdorDB.getSchema();

        tLRPolEdorSchema.setPolNo(aLRPolEdorSchema.getPolNo());
        tLRPolEdorSchema.setReinsureCom(aLRPolEdorSchema.getReinsureCom());
        tLRPolEdorSchema.setReinsurItem(aLRPolEdorSchema.getReinsurItem());
        tLRPolEdorSchema.setInsuredYear(aLRPolEdorSchema.getInsuredYear());
        tLRPolEdorSchema.setGrpContNo(aLRPolEdorSchema.getGrpContNo());
        tLRPolEdorSchema.setPrtNo(aLRPolEdorSchema.getPrtNo());
        tLRPolEdorSchema.setRiskCode(aLRPolEdorSchema.getRiskCode());
        tLRPolEdorSchema.setInsuredNo(aLRPolEdorSchema.getInsuredNo());
        tLRPolEdorSchema.setInsuredName(aLRPolEdorSchema.getInsuredName());
        tLRPolEdorSchema.setInsuredSex(aLRPolEdorSchema.getInsuredSex());
        tLRPolEdorSchema.setInsuredBirthday(aLRPolEdorSchema.getInsuredBirthday());
        tLRPolEdorSchema.setInsuredAppAge(aLRPolEdorSchema.getInsuredAppAge());
        tLRPolEdorSchema.setAppntNo(aLRPolEdorSchema.getAppntNo());
        tLRPolEdorSchema.setAppntName(aLRPolEdorSchema.getAppntName());
        tLRPolEdorSchema.setCValiDate(aLRPolEdorSchema.getCValiDate());
        tLRPolEdorSchema.setEndDate(aLRPolEdorSchema.getEndDate());
        tLRPolEdorSchema.setStandPrem(aLRPolEdorSchema.getStandPrem());
        tLRPolEdorSchema.setPrem(aLRPolEdorSchema.getPrem());
        tLRPolEdorSchema.setAmnt(aLRPolEdorSchema.getAmnt());
        tLRPolEdorSchema.setRiskAmnt(aLRPolEdorSchema.getRiskAmnt());
        tLRPolEdorSchema.setSumRiskAmount(aLRPolEdorSchema.getSumRiskAmount());
        tLRPolEdorSchema.setPayIntv(aLRPolEdorSchema.getPayIntv());
        tLRPolEdorSchema.setPayMode(aLRPolEdorSchema.getPayMode());
        tLRPolEdorSchema.setPayYears(aLRPolEdorSchema.getPayYears());
        tLRPolEdorSchema.setInsurePeriod(aLRPolEdorSchema.getInsurePeriod());
        tLRPolEdorSchema.setSaleChnl(aLRPolEdorSchema.getSaleChnl());
        tLRPolEdorSchema.setEdorEndDate(aLRPolEdorSchema.getEdorEndDate());
        tLRPolEdorSchema.setBackPrem(aLRPolEdorSchema.getBackPrem());
        tLRPolEdorSchema.setSumBackAmnt(aLRPolEdorSchema.getSumBackAmnt());
        tLRPolEdorSchema.setBackCessRate(aLRPolEdorSchema.getBackCessRate());
        tLRPolEdorSchema.setNoPassMonth(aLRPolEdorSchema.getNoPassMonth());
        tLRPolEdorSchema.setFeeoperationType(aLRPolEdorSchema.getFeeoperationType());
        tLRPolEdorSchema.setBackFeeDate(aLRPolEdorSchema.getBackFeeDate());

        tLRPolEdorSchema.setOperator(mGlobalInput.Operator);
        tLRPolEdorSchema.setModifyDate(PubFun.getCurrentDate());
        tLRPolEdorSchema.setModifyTime(PubFun.getCurrentTime());

        mLRPolEdorSet.add(tLRPolEdorSchema);
        return true;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LRImportEdor";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";
        String grpContNo = "1400000915";
        String path = "d:\\";
        String fileName = "lredordata.xls";
        path = "D:/project/ui/temp/";
        String config = "D:/project/ui/temp/LREdorImport.xml";
        LRImportEdor tLRImportCess = new LRImportEdor(tGI);
        if (!tLRImportCess.doAdd(path, fileName)) {
            System.out.println(tLRImportCess.mErrors.getFirstError());
            System.out.println("磁盘导入失败！");
        }
    }
  }
