package com.sinosoft.lis.reinsure;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import com.sinosoft.lis.reinsure.*;
import com.sinosoft.lis.schema.LRErrorLogSchema;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.db.LRGetDataLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;


import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.vschema.LRGetDataLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LRBTReContInterfaceTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */

    public String mCurrentDate = PubFun.getCurrentDate();

    private String mToDate =null;
    
    private String mStartDate= null;
    private String mEndDate =null;
    private String mReContCode= "";

    //业务处理相关变量
    /** 全局数据 */
    private PubFun mPubFun = new PubFun();
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    private TransferData mTransferData = new TransferData();
    
    public LRBTReContInterfaceTask() {
    }
    
    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     **/
//    public void run() {
//        VData aVData = new VData();
//        globalInput.ComCode = null;
//        aVData.addElement(globalInput);
//
//        if (!submitData()) {
//            return;
//        }
//    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate ) {
    	
        System.out.println("before getInputData()........");
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("after getInputData()........");
        if (!dealData()) {
            return false;
        }
        
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "Server";
        LRBTReContInterfaceTask a=new LRBTReContInterfaceTask();
        a.run();
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        this.mStartDate = (String) mTransferData.getValueByName("StartDate");
        this.mEndDate = (String) mTransferData.getValueByName("EndDate");
        System.out.println("开始日期"+mStartDate+"结束日期"+mEndDate);
        this.mReContCode = (String) mTransferData.getValueByName("ReContCode");
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        Connection con=null;
        
        try{
            	con=DBConnPool.getConnection();
            	con.setAutoCommit(false);
            	FDate chgdate = new FDate();
//            	String currentDate=PubFun.calDate(mCurrentDate,-1,"D",null);
            	String Serialno="";//提数日志表流水号

            	Date tStartDate = chgdate.getDate(this.mStartDate); //录入的起始日期，用FDate处理
            	Date tEndDate = chgdate.getDate(this.mEndDate);
            	Date dbdate = tStartDate; //起始日期，用FDate处理
            	Date dedate = tEndDate; //终止日期，用FDate处理
            	
            	while (dbdate.compareTo(dedate) <= 0) {
            		//为 了手工控制 批处理是否运行
		             //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
		            	mToDate = chgdate.getString(dbdate); //录入的正在统计的日期
		            	SSRS tSSRSA = new ExeSQL().execSQL("select * from fivouchemanage  where startdate<='"+mToDate+"'  and enddate>='"+mToDate+"' and  makedate>'"+mToDate+"'and state in ('30','50') with ur");
		            	if(tSSRSA==null || tSSRSA.MaxRow<=0){     
		            		System.out.println("当日尚未财务提数");
		           		 	break;
		            	}
		               
		            	//B表数据中间表提数
//		            	ReBContInterfaceGetData tReBContInterfaceGetData = new ReBContInterfaceGetData();
//		            	if(!tReBContInterfaceGetData.run(mToDate)){
//		            		this.errorLog("提取"+mToDate+"日b表的中间表数据时出错");
//		            		return false;
//		            	}
		            	SSRS tSSRS =null; 
	            			            
			            
			            Statement stmt = con.createStatement();
			            ResultSet tRs = null;
			            String ttSql = "select calsql,tablename,calcode from LRGetDataClassDef where calcode<>'BT05' and dealclass='BTT'  order by ordernum with ur";
			            ExeSQL tESQL = new ExeSQL();
			            tSSRS = tESQL.execSQL(ttSql);
			            if(tSSRS!=null && tSSRS.MaxRow>0){
			                for(int index=1;index<=tSSRS.MaxRow;index++){
			                    String exeSql="";
			                    PubCalculator tPubCalculator = new PubCalculator();
			                    tPubCalculator.addBasicFactor("ToDay",mToDate);
			                    tPubCalculator.addBasicFactor("Operator", "Server");
			                    tPubCalculator.addBasicFactor("SerialNo", Serialno);
			                    tPubCalculator.addBasicFactor("DataState", "0");//数据状态 '0' 正常状态  '1' 分保完成  '-1' 数据补提
			                    tPubCalculator.addBasicFactor("SingRecontFlag", "0");//0 非单再保合同补提  1 单再保合同补提 
			                    tPubCalculator.setCalSql(tSSRS.GetText(index, 1));
			                    exeSql = tPubCalculator.calculateEx();
			                    System.out.println(tSSRS.GetText(index, 3)+":::"+exeSql);
			                    tRs=stmt.executeQuery(exeSql);
			                    if(insert(tRs,tSSRS.GetText(index, 2),con)==false){
			                        return false;
			                    }
			                }
			                con.commit();
			            }
			                    String ttSql1 = "select calsql,tablename from LRGetDataClassDef where calcode='BT05' and dealclass='BTT' order by ordernum with ur";
			                  
			                    SSRS tSSRS1 = tESQL.execSQL(ttSql1);
			                    if(tSSRS1!=null && tSSRS1.MaxRow>0){
			                       for(int index=1;index<=tSSRS1.MaxRow;index++){
			                    	   String exeSql="";
			                    	   PubCalculator tPubCalculator = new PubCalculator();
			                    	   tPubCalculator.addBasicFactor("ToDay",mToDate);
			                    	   tPubCalculator.addBasicFactor("Operator", "Server");
			                    	   tPubCalculator.addBasicFactor("SerialNo", Serialno);
			                 
			                    	   tPubCalculator.setCalSql(tSSRS1.GetText(1, 1));
			                    	   exeSql = tPubCalculator.calculateEx();
			                
			                    	   ResultSet tRs1=stmt.executeQuery(exeSql);
			                    	   if(insert(tRs1,tSSRS1.GetText(1, 2),con)==false){
			                    		   return false;
			                    	   }
			                    	   con.commit();
			                    
			                        }
			                    }
			                   
			                    dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
			               if("1".equals(this.mReContCode)){     
				            if(GetCalDate()==false){
				        		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
				        		return false;
				        	}
			               } 
				            con.commit();
				            
				           
				            
//				            tLRGetDataLogDB.setOperator("CH");
//				            tLRGetDataLogDB.setSerialNo(Serialno);
//				            tLRGetDataLogDB.update();
				            con.commit();
				            
				            
				            
		            
            	}
            	con.close();
        }catch(Exception e){
            try{
                e.printStackTrace();
                con.close();
                return false;
            }catch(Exception c){}
        }
        
        return true;
    }
    
    private boolean insert(ResultSet rs,String table,Connection con){
        Statement stmt = null;
        ResultSet tRs = null;
        ResultSetMetaData rsmd = null;

        ResultSetMetaData rsmd1 = null;
        try{
            stmt = con.createStatement();
            tRs = stmt.executeQuery("select * from " + table+" fetch first 1 rows only");
            rsmd = tRs.getMetaData();
            rsmd1 = rs.getMetaData();
            int columnCount=rsmd.getColumnCount();
            int columnCount1=rsmd1.getColumnCount();
            if(columnCount!=columnCount1){
//                this.mErrors.addOneError("与目标的列数不符");
                return false;
            }
            System.out.println("begin......");
            while(rs.next()){
                StringBuffer insertSql = new StringBuffer();
                StringBuffer whereSql=new StringBuffer();
                insertSql .append("insert into "+table+" (");
                int flag=0;
                for (int i = 0; i < columnCount; i++) {
                    if(flag==1){
                        insertSql.append(",");
                        whereSql.append(",");
                    }
                    insertSql .append(rsmd.getColumnName(i+1));

                    int mDataType=rsmd.getColumnType(i+1);
//                  判断数据类型
                    if ((mDataType == Types.CHAR) || (mDataType == Types.VARCHAR)) {
                        if(rs.getString(i+1)==null){
                            whereSql.append(rs.getObject(i+1));
                        }else{
                            whereSql.append("'");
                            whereSql.append(StrTool.cTrim(rs.getString(i+1)));
                            whereSql.append("'");
                        }
                    }
                    else if ((mDataType == Types.TIMESTAMP) || (mDataType == Types.DATE)) {
                        if(rs.getDate(i+1)==null){
                            whereSql.append(rs.getObject(i+1));
                        }else{
                            whereSql.append("'");
                            whereSql.append(rs.getDate(i+1));
                            whereSql.append("'");
                        }
                    }else{
                        whereSql.append(rs.getObject(i+1));
                    }
                    flag=1;
                }
                insertSql .append(") values ( ");
                insertSql .append(whereSql.toString());
                insertSql .append(")");
                String ExeSql=insertSql.toString();
                try{
                    //System.out.println(ExeSql);
                    stmt.executeUpdate(ExeSql);
                }catch(Exception insertExc){
                    System.out.println("Error Insert......"+ExeSql);
                }
            }
            stmt.close();
        }catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
//            this.mErrors.addOneError("插入语句拼写错误");
            return false;
        }
        return true;
    }
    
    /**
     * 处理中间表提数后的操作
     * @param mToDate
     * @return
     */
    private boolean GetCalDate(){
    	
    	//再保各种提数
    	if(!getData()){
    		
    		return false;
    	}
    	
    	//再保各种计算
    	if(!calDate()){
    		
    		return false;
    	}
    	
    	if(!GBDeal()){//共保提数
    		return false;
    	}
    	if(!listDeal()){//清单提数
    		return false;
    	}
    	
    	return true;
    }
    
    /**
     * 各类计算函数
     * @return
     */
    private boolean calDate() {
    	
    	//分保计算
    	LRNewCalCessDataTBL tLRNewCalCessDataTBL=new LRNewCalCessDataTBL();
    	if(tLRNewCalCessDataTBL.submitDataT("Server",mToDate)==false){
    		this.errorLog("计算"+mToDate+"日分保数据时出错");
    		return false;
    	}
    	
        //    	分保特别险种计算
    	LRCalDataDealALLBL tLRCalDataDealALLBL=new LRCalDataDealALLBL();
    	if(tLRCalDataDealALLBL.submitDataT("Server",mToDate)==false){
    		return false;
    	}    	
    	
    	
    	//保全摊回计算
    	LRNewCalEdorDataTBL tLRNewCalEdorDataTBL=new LRNewCalEdorDataTBL();
    	if(tLRNewCalEdorDataTBL.submitDataT("Server",mToDate)==false){
    		this.errorLog("计算"+mToDate+"日保全数据时出错");
    		return false;
    	}
    	
    	//理赔摊回计算
    	LRNewCalClaimDataTBL tLRNewCalClaimDataTBL=new LRNewCalClaimDataTBL();
    	if(tLRNewCalClaimDataTBL.submitDataT("Server",mToDate)==false){
    		this.errorLog("计算"+mToDate+"日理赔数据时出错");
    		return false;
    	}
    	
		return true;
	}

	/**
     * 各种再保提数类调用
     * @return
     */
    private boolean getData() {
    	
    	//普通合同分保提数调用
    	LRNewGetCessDataTBL tLRNewGetCessDataTBL = new LRNewGetCessDataTBL();
    	if(tLRNewGetCessDataTBL.submitDataT("Server",mToDate) == false){
    		this.errorLog("提取"+mToDate+"日正常分保数据时出错");
    		return false;
    	}
    	
        //    	普通合同特别险种分保提数调用
    	LRAllDataDealBL tLRAllDataDealBL=new LRAllDataDealBL();
    	if(tLRAllDataDealBL.submitDataT("Server",mToDate)==false){
    		return false;
    	}
    	
    	//普通合同保全摊回提数
    	LRNewGetEdorDataTBL tLRNewGetEdorDataTBL = new LRNewGetEdorDataTBL();
    	if(tLRNewGetEdorDataTBL.submitDataT("Server",mToDate) == false){
    		this.errorLog("提取"+mToDate+"日正常保全摊回数据时出错");
    		return false;
    	}
    	
    	//普通合同理赔摊回提数
    	LRNewGetClaimDataTBL tLRNewGetClaimDataTBL = new LRNewGetClaimDataTBL();
    	if(tLRNewGetClaimDataTBL.submitDataT("Server",mToDate) == false){
    		this.errorLog("提取"+mToDate+"日正常理赔摊回数据时出错");
    		return false;
    	}
    	
    	//约定缴费应收数据分保提取
    	LRNewYDGetCessDataBL tLRNewYDGetCessDataBL = new LRNewYDGetCessDataBL();
    	if(tLRNewYDGetCessDataBL.submitDataT(mInputData,"Server",mToDate) == false){
    		this.errorLog("提取"+mToDate+"日约定缴费应收分保数据时出错");
    		return false;
    	}
    	
    	//续保理赔摊回数据提取
    	LRNewGetClaimDataRBBL tLRNewGetClaimDataRBBL = new LRNewGetClaimDataRBBL();
    	if(tLRNewGetClaimDataRBBL.run(mToDate) == false){
    		this.errorLog("提取"+mToDate+"日续保理赔摊回数据时出错");
    		return false;
    	}
    	
    	//青岛大额理赔摊回数据提取
    	LRPNewGetClaimDataTBL tLRPNewGetClaimDataTBL = new LRPNewGetClaimDataTBL();
    	if(tLRPNewGetClaimDataTBL.submitDataT("Server",mToDate)==false){
    		this.errorLog("提取"+mToDate+"日青岛大额理赔摊回数据时出错");
    		return false;
    	}
    	
    	//临分提数
    	if(!tempDeal()){
    		this.errorLog("提取"+mToDate+"日临分数据时出错");
    		return false;
    	}
    	

    	
		return true;
	}

    
    
    
    
	private boolean tempDeal(){//调用临分程序
    	ReTempGetDateTBL tReTempGetDateTBL=new ReTempGetDateTBL();
    	if(!tReTempGetDateTBL.run(mToDate)){
    		return false;
    	}
    	return true;
    }
    
    private boolean GBDeal(){//调用共保提数程序
    	ReGContInterfaceGetData tReGContInterfaceGetData=new ReGContInterfaceGetData();
    	if(!tReGContInterfaceGetData.run(mToDate)){
    		this.errorLog("处理"+mToDate+"日共保数据时出错");
    		return false;
    	}
    	return true;
    }
    
  
    
    /**清单提数
     * listDeal
     * @return boolean
     */
    private boolean listDeal(){
    	
    	LRCessListTBL tLRCessListTBL=new LRCessListTBL();//分保清单
    	if(tLRCessListTBL.submitDataT("Server",mToDate)==false){
    		this.errorLog("提取"+mToDate+"日分保清单数据时出错");
    		return false;
    	}
    	LRClaimListTBL tLRClaimListTBL=new LRClaimListTBL();
    	if(tLRClaimListTBL.submitDataT("Server",mToDate)==false){
    		this.errorLog("提取"+mToDate+"日理赔摊回清单数据时出错");
    		return false;
    	}
    	return true;
    	
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo("0000");
        tLRErrorLogSchema.setLogType("9"); 
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator("Server");
        tLRErrorLogSchema.setManageCom("86");
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "DELETE&INSERT");

        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
           
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            
            return false;
        }
        return true;
    }




    /*
     *
     */
//    private void buildError(String szFunc, String szErrMsg) {
//        CError cError = new CError();
//
//        cError.moduleName = "ReContInterfaceGetDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
//    }
}
