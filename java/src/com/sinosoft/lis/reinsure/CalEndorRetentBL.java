package com.sinosoft.lis.reinsure;

import java.util.Date;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.bq.EdorCalZT;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.db.LREdorMainDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LREdorMainSchema;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LPEdorMainSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.vschema.LREdorMainSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class CalEndorRetentBL {

  public CErrors mErrors=new CErrors();
  private GlobalInput mGlobalInput =new GlobalInput() ;
  private VData mInputData ;
  private String mOperate;
  private LREdorMainSet mLREdorMainSet = new LREdorMainSet();
  private LRPolSet mLRPolSet = new LRPolSet();
  private VData mResult = new VData();
  private String mStartDate = "";
  private String mEndDate = "";
  private String mToday = "";
  private String mReinsureCom="";


  public CalEndorRetentBL() {
  }

  public boolean submitData(VData cInputData, String cOperate)
  {
    System.out.println("beging bl.......");
//    LREdorMainDB tLREdorMainDB = new LREdorMainDB();
//    tLREdorMainDB.deleteSQL();
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    try
    {
      if(  !mOperate.equals("CalEdor") )
      {
        buildError("submitData", "不支持的操作字符串");
        return false;
      }

      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) )
      {
        return false;
      }
      this.mLREdorMainSet.clear();
      mResult.clear();
      FDate chgdate = new FDate();
      Date dbdate = chgdate.getDate(mStartDate);
      Date dedate = chgdate.getDate(mEndDate);

      while(dbdate.compareTo(dedate) <= 0)
      {
        mToday = chgdate.getString(dbdate);
        if (this.mOperate.equals("CalEdor"))
        {
          if (!getEndorseData())
          {
            buildError("submitData", "错误");
            return false;
          }
        }

        dbdate=PubFun.calDate(dbdate,1,"D",null);
      }

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      buildError("submit", "发生异常");
      return false;
    }
    return true;
  }
  private void buildError(String szFunc, String szErrMsg)
    {
      CError cError = new CError( );
      cError.moduleName = "CalEdorRetentBL";

      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      this.mErrors.addOneError(cError);
  }
  private boolean getInputData(VData cInputData)
    {
      mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      mStartDate = (String)cInputData.get(1);
      mEndDate = (String)cInputData.get(2);
      mReinsureCom=(String)cInputData.get(3);

      if (mStartDate.equals(""))
      {
        buildError("getInputData","没有起始日期!");
        return false;
      }

      if (mEndDate.equals(""))
      {
        buildError("getInputData","没有终止日期!");
        return false;
      }

      if( mGlobalInput==null )
      {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
      }
      return true;
    }
    public static String getaaa()
    {
      String t=PubFun.getCurrentDate();
      return "";
    }

    //保单发生保全或者失效的处理
    private boolean getEndorseData()
    {
      //保全事件处理

      this.mLREdorMainSet = new LREdorMainSet();
      String tSql="select * from lpedormain where edorvalidate='"+this.mToday+"' and EdorState='0' ";

      //tSql=tSql+" and edorno='86330020030420000051'";
      System.out.println(tSql);
      LPEdorMainDB tLPEdormainDB= new LPEdorMainDB();
      LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
      tLPEdorMainSet=tLPEdormainDB.executeQuery(tSql);
      for (int i=1;i<=tLPEdorMainSet.size();i++)
      {
        LPEdorMainSchema tPEdorMainSchema = new LPEdorMainSchema();
        tPEdorMainSchema=tLPEdorMainSet.get(i);
        LPPolSet tLPPolSet = new LPPolSet();
        //查询保单信息
        LCPolDB tLCPolDB = new LCPolDB();
        //2005.1.20杨明注释掉错误代码tLCPolDB.setPolNo(tPEdorMainSchema.getPolNo());
        if (!tLCPolDB.getInfo())
        {
          //查询销户保单，涉及附加险
          LBPolDB tLBPolDB = new LBPolDB();
          LBPolSet tLBPolSet = new LBPolSet();
          //2005.1.20杨明注释掉的问题代码tLBPolDB.setMainPolNo(tPEdorMainSchema.getPolNo());
          /*Lis5.3 upgrade set
          tLBPolDB.setEdorNo(tPEdorMainSchema.getEdorNo());
          */
          tLBPolSet=tLBPolDB.query();
          if (tLBPolSet.size()==0)
            continue;
          else
          {
            Reflections tReflections = new Reflections();
            for (int lbpolcount=1;lbpolcount<=tLBPolSet.size();lbpolcount++)
            {
              LBPolSchema ttLBPolSchema = new LBPolSchema();
              ttLBPolSchema=tLBPolSet.get(lbpolcount);
              LPPolSchema ttLPPolSchema = new LPPolSchema();
              tReflections.transFields(ttLPPolSchema,ttLBPolSchema);
              tLPPolSet.add(ttLPPolSchema);
            }
         }
        }
        else //未销户保单
        {
          LCPolSchema tLCPolSchema = new LCPolSchema();
          tLCPolSchema.setSchema(tLCPolDB.getSchema());
          LPPolDB tLPPolDB = new LPPolDB();
          //2005.1.20杨明注释掉的问题代码tLPPolDB.setEdorType(tPEdorMainSchema.getEdorType());
          tLPPolDB.setEdorNo(tPEdorMainSchema.getEdorNo());
          tLPPolDB.setPolNo(tLCPolSchema.getPolNo());

          if (tLPPolDB.getInfo())
          {
            LPPolSchema tLPPolSchema = new LPPolSchema();
            tLPPolSchema.setSchema(tLPPolDB.getSchema());
            tLPPolSet.add(tLPPolSchema);
          }
          else
          {
            continue;
          }
        }

        for (int k=1;k<=tLPPolSet.size();k++)
        {
          LPPolSchema tLPPolSchema = new LPPolSchema();
          tLPPolSchema=tLPPolSet.get(k);

          double tChgAmnt=0;
          double tChgPrem=0;
          if (/*2005.1.20杨明注释掉问题代码,添加了一个flase
              tPEdorMainSchema.getEdorType().equals("ZT") ||
              tPEdorMainSchema.getEdorType().equals("GT") ||
              tPEdorMainSchema.getEdorType().equals("WT")*/false)
          {
            tChgAmnt=0-tLPPolSchema.getAmnt();
            tChgPrem=0-tLPPolSchema.getPrem();
          }
          else
          {
            tChgAmnt=tPEdorMainSchema.getChgAmnt();
            tChgPrem=tPEdorMainSchema.getChgPrem();
          }
          //计算保全确认时的保单年度
          int tInsureYear=PubFun.calInterval(tLPPolSchema.getCValiDate(),tPEdorMainSchema.getEdorValiDate(),"Y")+1;
          LRPolDB tLRPolDB = new LRPolDB();
          tLRPolDB.setPolNo(tLPPolSchema.getPolNo());
          tLRPolDB.setInsuredYear(tInsureYear);
          tLRPolDB.setReinsureCom(this.mReinsureCom);
          LRPolSet tLRPolSet = new LRPolSet();
          tLRPolSet=tLRPolDB.query();
          int tReinsureCount=tLRPolSet.size();
          Reflections tReflections = new Reflections();

          //如果没有分保，无分保比例
          if (tReinsureCount==0)
          {
            double tNewAmnt=tChgAmnt;
            if (/*2005.1.20杨明注释掉的问题代码,并添加了一个false
                     tChgAmnt>0 || tPEdorMainSchema.getEdorType().equals("RE")*/false)
            {
              if (/*2005.1.20杨明注释掉错误代码,并添加了一个false
                     tPEdorMainSchema.getEdorType().equals("RE")*/false)
              {
                tNewAmnt=tLPPolSchema.getAmnt();
              }
              for (int j=1;j<=2;j++)
              {
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tReflections.transFields(tLCPolSchema,tLPPolSchema);
                tLCPolSchema.setCValiDate(tPEdorMainSchema.getEdorValiDate());
                tLCPolSchema.setAmnt(tNewAmnt);
                tLCPolSchema.setRiskAmnt(tNewAmnt);
                tLCPolSchema.setSignDate(tPEdorMainSchema.getEdorValiDate());
                tLCPolSchema.setMakeDate(tPEdorMainSchema.getModifyDate());
                tLCPolSchema.setMakeTime(tPEdorMainSchema.getModifyTime());

                VData tVData = new VData();
                tVData.addElement(tLCPolSchema);
                if (j==1)
                {
                  tVData.addElement("L");
                }
                else
                {
                  tVData.addElement("C");
                }
                LRPolSet ttLRPolSet = new LRPolSet();
                tVData.addElement(this.mReinsureCom);
                tVData.addElement(mToday);
                tVData.addElement(ttLRPolSet);

                CalRetentBL tCalRetentBL = new CalRetentBL();
                if (tCalRetentBL.submitData(tVData,this.mOperate))
                {
                  VData tResult = new VData();
                  tResult=tCalRetentBL.getResult();
                  LRPolSet tRetLRPolSet= new LRPolSet();
                  tRetLRPolSet=(LRPolSet) tResult.getObjectByObjectName("LRPolSet",0);

                  for (int LRPolCount=1;LRPolCount<=tRetLRPolSet.size();LRPolCount++)
                  {
                    LRPolSchema ttLRPolSchema = new LRPolSchema();
                    ttLRPolSchema=tLRPolSet.get(LRPolCount);
                    LREdorMainSchema tLREdorMainSchema = new LREdorMainSchema();
                    tReflections.transFields(tLREdorMainSchema,tPEdorMainSchema);
                    tLREdorMainSchema.setPolNo(tLPPolSchema.getPolNo());
                    tLREdorMainSchema.setChgPrem(tChgPrem);
                    tLREdorMainSchema.setReinsureCom(ttLRPolSchema.getReinsureCom());
                    tLREdorMainSchema.setReinsurItem(ttLRPolSchema.getReinsurItem());
                    tLREdorMainSchema.setRiskCalSort(ttLRPolSchema.getRiskCalSort());
                    tLREdorMainSchema.setShRePrem(ttLRPolSchema.getCessPrem());
                    tLREdorMainSchema.setShReComm(ttLRPolSchema.getCessComm());
                    tLREdorMainSchema.setChgCessAmt(ttLRPolSchema.getCessionAmount());
                    tLREdorMainSchema.setCessStart(tLREdorMainSchema.getEdorValiDate());
                    tLREdorMainSchema.setInsuredYear(ttLRPolSchema.getInsuredYear());
                    tLREdorMainSchema.setMakeDate(PubFun.getCurrentDate());
                    tLREdorMainSchema.setMakeTime(PubFun.getCurrentTime());
                    tLREdorMainSchema.setModifyDate(PubFun.getCurrentDate());
                    tLREdorMainSchema.setModifyTime(PubFun.getCurrentTime());
                    String tCessEnd=PubFun.calDate(tLPPolSchema.getCValiDate(),tInsureYear,"Y","");
                    tLREdorMainSchema.setCessEnd(tCessEnd);
                    LREdorMainDB tLREdorMainDB = new LREdorMainDB();
                    tLREdorMainDB.setSchema(tLREdorMainSchema);
                    System.out.println("得到一条记录"+tLREdorMainSchema.getPolNo());
                    if (!tLREdorMainDB.getInfo())
                    {
                      System.out.println("已插入一条记录");
                      this.mLREdorMainSet.add(tLREdorMainSchema);
                    }
                  }
                }
              }
            }
          }
          //分保结束日
          String tNextPerDay=PubFun.calDate(tLPPolSchema.getCValiDate(),tInsureYear,"Y","");

          if (tChgAmnt!=0 ) //本次保全引起保额变化
          {
            //查询保全对应的保单年度是否有分保
            int tIntvl=PubFun.calInterval(tPEdorMainSchema.getEdorValiDate(),tNextPerDay,"D");
            double tReturnRate=tIntvl/365.00;
            for (int j=1;j<=tLRPolSet.size();j++)
            {
              LRPolSchema tLRPolSchema = new LRPolSchema();
              tLRPolSchema=tLRPolSet.get(j);
              LREdorMainSchema tLREdorMainSchema = new LREdorMainSchema();
              tReflections.transFields(tLREdorMainSchema,tPEdorMainSchema);
              double tRRP1= tReturnRate*tLRPolSchema.getCessPrem();
              double tAmountRate=tChgAmnt/tLRPolSchema.getAmnt();
              double tRRP = tRRP1*tAmountRate;
              double tRRC = tRRP* tLRPolSchema.getCessCommRate();
              tLREdorMainSchema.setPolNo(tLPPolSchema.getPolNo());
              tLREdorMainSchema.setChgPrem(tChgPrem);
              tLREdorMainSchema.setReinsureCom(tLRPolSchema.getReinsureCom());
              tLREdorMainSchema.setReinsurItem(tLRPolSchema.getReinsurItem());
              tLREdorMainSchema.setRiskCalSort(tLRPolSchema.getRiskCalSort());
              tLREdorMainSchema.setInsuredYear(tLRPolSchema.getInsuredYear());
              tLREdorMainSchema.setShRePrem(tRRP);
              tLREdorMainSchema.setShReComm(tRRC);
              tLREdorMainSchema.setChgCessAmt(tLRPolSchema.getCessionAmount()*tAmountRate);
              tLREdorMainSchema.setCessStart(tLREdorMainSchema.getEdorValiDate());
              tLREdorMainSchema.setCessEnd(tLRPolSchema.getCessEnd());
              tLREdorMainSchema.setMakeDate(PubFun.getCurrentDate());
              tLREdorMainSchema.setMakeTime(PubFun.getCurrentTime());
              tLREdorMainSchema.setModifyDate(PubFun.getCurrentDate());
              tLREdorMainSchema.setModifyTime(PubFun.getCurrentTime());
              LREdorMainDB tLREdorMainDB = new LREdorMainDB();
              tLREdorMainDB.setSchema(tLREdorMainSchema);
              if (!tLREdorMainDB.getInfo())
                this.mLREdorMainSet.add(tLREdorMainSchema);
            }
          }

          if (/*2005.1.20杨明注释掉的问题代码
                     tPEdorMainSchema.getEdorType().equals("RE")*/false)
        {
          String tLapsDate=EdorCalZT.CalLapseDate(tLPPolSchema.getRiskCode(),tLPPolSchema.getPaytoDate());
          for (int j=1;j<=tLRPolSet.size();j++)
          {
            LRPolSchema tLRPolSchema = new LRPolSchema();
            tLRPolSchema=tLRPolSet.get(j);
            FDate tFDate = new FDate();
            if (tFDate.getDate(tLRPolSchema.getCessStart()).before(tFDate.getDate(tLapsDate)))
            {
              LREdorMainSchema tLREdorMainSchema = new LREdorMainSchema();
              tReflections.transFields(tLREdorMainSchema,tPEdorMainSchema);
              tLREdorMainSchema.setPolNo(tLPPolSchema.getPolNo());
              double tIntv=PubFun.calInterval(tLREdorMainSchema.getEdorValiDate(),tNextPerDay,"D");
              double tRRP=tIntv*tLRPolSchema.getCessPrem()/365;
              tLREdorMainSchema.setShRePrem(tRRP);
              double tRRC = tRRP* tLRPolSchema.getCessCommRate();
              tLREdorMainSchema.setShReComm(tRRC);
              tLREdorMainSchema.setReinsureCom(tLRPolSchema.getReinsureCom());
              tLREdorMainSchema.setReinsurItem(tLRPolSchema.getReinsurItem());
              tLREdorMainSchema.setRiskCalSort(tLRPolSchema.getRiskCalSort());
              tLREdorMainSchema.setChgCessAmt(tLRPolSchema.getCessionAmount());
              tLREdorMainSchema.setInsuredYear(tLRPolSchema.getInsuredYear());
              tLREdorMainSchema.setCessStart(tLREdorMainSchema.getEdorValiDate());
              tLREdorMainSchema.setCessEnd(tLRPolSchema.getCessEnd());
              tLREdorMainSchema.setMakeDate(PubFun.getCurrentDate());
              tLREdorMainSchema.setMakeTime(PubFun.getCurrentTime());
              tLREdorMainSchema.setModifyDate(PubFun.getCurrentDate());
              tLREdorMainSchema.setModifyTime(PubFun.getCurrentTime());
              LREdorMainDB tLREdorMainDB = new LREdorMainDB();
              tLREdorMainDB.setSchema(tLREdorMainSchema);
              if (!tLREdorMainDB.getInfo())
                this.mLREdorMainSet.add(tLREdorMainSchema);
            }
          }
        }
        }

      }

      //本保单年度失效保单处理
      tSql="select * from LRPol where CessStart<='"+this.mToday+"' and CessEnd>='"+this.mToday+"' and payintv<>0 and payintv<>-1 ";
      LRPolDB tLRPolDB1 = new LRPolDB();
      LRPolSet tLRPolSet = new LRPolSet();
      tLRPolSet=tLRPolDB1.executeQuery(tSql);
      for (int i=1;i<=tLRPolSet.size();i++)
      {
        LRPolSchema tLRPolSchema = new LRPolSchema();
        tLRPolSchema=tLRPolSet.get(i);
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(tLRPolSchema.getPolNo());
        System.out.println("LCPolBL.getinfo begin");
        if (!tLCPolBL.getInfo())
          continue;

        if (!EdorCalZT.JudgeLapse(tLCPolBL.getRiskCode(),tLCPolBL.getPaytoDate(),this.mToday,String.valueOf(tLCPolBL.getPayIntv()),tLCPolBL.getEndDate()))
        {
          continue;
        }
        String tLapsDate=EdorCalZT.CalLapseDate(tLCPolBL.getRiskCode(),tLCPolBL.getPaytoDate());
        //取得所有本保单年度内的分保记录
        int tPolPremYear=PubFun.calInterval(tLCPolBL.getCValiDate(),this.mToday,"Y");
        String tNextPerDay=PubFun.calDate(tLCPolBL.getCValiDate(),tPolPremYear+1,"Y","");
        System.out.println("tLapsDate="+tLapsDate);
        System.out.println("tNextPerDay="+tNextPerDay);
        int tIntvl=PubFun.calInterval(tLapsDate,tNextPerDay,"D");
        System.out.println("tIntv="+tIntvl);

        double tReturnRate=tIntvl/365.0;
        double tRRP=-tReturnRate*tLRPolSchema.getCessPrem();
        double tRRC=tRRP*tLRPolSchema.getCessCommRate();
        double tRRA=-tLRPolSchema.getCessionAmount();
        LREdorMainSchema tLREdorMainSchema = new LREdorMainSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLREdorMainSchema,tLRPolSchema);

        tLREdorMainSchema.setPolNo(tLRPolSchema.getPolNo());
        tLREdorMainSchema.setEdorType("LA");
        tLREdorMainSchema.setReinsureCom(tLRPolSchema.getReinsureCom());
        tLREdorMainSchema.setReinsurItem(tLRPolSchema.getReinsurItem());
        tLREdorMainSchema.setRiskCalSort(tLRPolSchema.getRiskCalSort());
        tLREdorMainSchema.setInsuredYear(tLRPolSchema.getInsuredYear());
        tLREdorMainSchema.setChgCessAmt(tRRA);
        tLREdorMainSchema.setShRePrem(tRRP);
        tLREdorMainSchema.setShReComm(tRRC);
        tLREdorMainSchema.setCessStart(tLapsDate);
        tLREdorMainSchema.setCessEnd(tLRPolSchema.getCessEnd());
        tLREdorMainSchema.setOperator(this.mGlobalInput.Operator);
        tLREdorMainSchema.setMakeDate(PubFun.getCurrentDate());
        tLREdorMainSchema.setMakeTime(PubFun.getCurrentTime());
        tLREdorMainSchema.setModifyDate(PubFun.getCurrentDate());
        tLREdorMainSchema.setModifyTime(PubFun.getCurrentTime());
        LREdorMainDB tLREdorMainDB = new LREdorMainDB();
        tLREdorMainDB.setPolNo(tLRPolSchema.getPolNo());
        tLREdorMainDB.setEdorType("LA");
        tLREdorMainDB.setReinsureCom(tLRPolSchema.getReinsureCom());
        tLREdorMainDB.setReinsurItem(tLRPolSchema.getReinsurItem());
        tLREdorMainDB.setInsuredYear(tLRPolSchema.getInsuredYear());
        tLREdorMainDB.setRiskCalSort(tLRPolSchema.getRiskCalSort());
        tLREdorMainDB.setCessStart(tLapsDate);
        if (tLREdorMainDB.getCount()==0)
        {
          tLREdorMainSchema.setEdorNo(PubFun1.CreateMaxNo("SERIALNO",""));
          this.mLREdorMainSet.add(tLREdorMainSchema);
        }
      }

      System.out.println("记录数："+this.mLREdorMainSet.size());
      if (this.mLREdorMainSet.size()>0)
      {
        this.prepareData();
        CalEndorRetentBLS tCalEndorRetentBLS = new CalEndorRetentBLS();
        if (!tCalEndorRetentBLS.submitData(mInputData,this.mOperate))
        {
          System.out.println("tCalEndorRetentBLS error out");
          this.mErrors.copyAllErrors(tCalEndorRetentBLS.mErrors);
          CError tError = new CError();
          tError.moduleName = "CalEndorRetentBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors .addOneError(tError) ;
          return false;
        }
      }

      return true;
    }



    private void prepareData()
      {
      this.mInputData.clear();
      this.mResult.clear();
      this.mInputData.add(this.mLREdorMainSet);
      this.mResult.addElement(this.mLREdorMainSet);
      }

      public VData getResult()
      {
        return mResult;
      }



  public static void main(String[] args) {
    CalEndorRetentBL tCalEndorRetentBL = new CalEndorRetentBL();
   VData vData = new VData();
    GlobalInput tG = new GlobalInput();

    tG.Operator="001";
    tG.ManageCom="86";
    String bdate="2003-12-30";
    String edate="2003-12-30";
    vData.addElement(tG);
    vData.addElement(bdate);
    vData.addElement(edate);
    vData.addElement("1001");
    tCalEndorRetentBL.submitData(vData, "CalEdor");

  }
}
