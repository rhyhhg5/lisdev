/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.Hashtable;

import com.sinosoft.lis.vdb.LRPolDBSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LRCalFactorValueSet;
import com.sinosoft.lis.vschema.LRCalModeSet;
import com.sinosoft.lis.vschema.LRContInfoSet;
import com.sinosoft.lis.vschema.LRDutySet;
import com.sinosoft.lis.vschema.LRInterfaceSet;
import com.sinosoft.lis.vschema.LRPolSet;

import java.util.*;

/*
 * <p>ClassName: LRNewGetCessDataBL </p>
 * <p>Description: 提取再保新单数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 孙宇
 * @CreateDate：2008-10-30
 */
public class LRNewYDGetCessDataBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
//    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mReContCode = "";
    private String mRecontCodeC = "";
    private String mGrpPolNo = "";
    private String mPayNo = "";
    private String mContPlanCode="";
    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private String mRecontCode; //再保合同代码
    private String mRiskCalSort = "";
    private Hashtable m_hashLMCheckField = new Hashtable();
    //业务处理相关变量
    /** 全局数据 */

    public LRNewYDGetCessDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }
    
    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT(VData cInputData, String cOperate,String mToDay) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputDataT(cInputData,mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

       
            if (!getCessData()) {
                return false;
            }
        
        return true;
    }

    /**
     * 提取分保数据
     * @return boolean
     */
    private boolean getCessData() {
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(mEndDate); //录入的终止日期，用FDate处理

        Connection con=DBConnPool.getConnection();
        try{
            //con.setAutoCommit(false);
            while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
                mToday = chgdate.getString(dbdate); //录入的正在统计的日期
                mGrpPolNo="s";
                mRecontCodeC = "s";
                mContPlanCode = "s";
                if (!insertPolData(con)) {
                    buildError("getCessData", "提取" + mToday + "日新单数据出错");
                    return false;
                }
                dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
                con.commit();
            }
            
            con.close();
        }catch(Exception e){
            try{
                e.printStackTrace();
                con.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 提取合同分保保单信息
     * @return boolean
     */
    private boolean insertPolData(Connection con) {
    	/**
    	 * 先判断是否为数据补提
    	 */
    	String tSql="";
    	if(mReContCode==null||"".equals(mReContCode)){//非补提，取状态为0的数据
    		tSql="select * from lrpol a where getdatadate='"+mToday+"' " +
    				" and payintv=-1 and polstat='7' and conttype='2' and reinsureitem='C' " +
    				" and exists (select 1 from lrcontinfo where recontcode=a.recontcode" +
    				" and CessionMode='1' and recontstate='01'" +
    				" union all" +
    				" select 1 from lrtempcesscont where tempcontcode=a.recontcode" +
    				" and CessionMode='1' and recontstate='01') order by grppolno with ur ";
    	}
    	else{//补提，取状态为-1的数据
    		tSql="select * from lrpol a where getdatadate='"+mToday+"' " +
			"and   payintv=-1 and polstat='7' and conttype='2' and reinsureitem='C' and  " +
			" recontcode='"+mReContCode+"' and exists (select 1 from lrcontinfo" +
					" where recontcode=a.recontcode" +
    				" and CessionMode='1' and recontstate='01'" +
    				" union all" +
    				" select 1 from lrtempcesscont where tempcontcode=a.recontcode" +
    				" and CessionMode='1' and recontstate='01') order by grppolno with ur ";
    	}
        
        LRPolDB tLRPolDB=new LRPolDB(con);
        LRPolSet tLRPolSet=new LRPolSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolSet, tSql);
        try {
            do {
                rswrapper.getData();
                
        for(int i=1;i<=tLRPolSet.size();i++){
        	LRPolSchema tLRPolSchema=tLRPolSet.get(i);
            
            if(!mGrpPolNo.equals(tLRPolSchema.getGrpPolNo())
            		||(mGrpPolNo.equals(tLRPolSchema.getGrpPolNo())
            				&&!mContPlanCode.equals(tLRPolSchema.getContPlanCode()))||
            				(mGrpPolNo.equals(tLRPolSchema.getGrpPolNo())
                    				&&mContPlanCode.equals(tLRPolSchema.getContPlanCode())&&
                    				!mRecontCodeC.equals(tLRPolSchema.getReContCode()))){
            	mContPlanCode=tLRPolSchema.getContPlanCode();
            	mGrpPolNo=tLRPolSchema.getGrpPolNo();
            	mRecontCodeC=tLRPolSchema.getReContCode();
            }
            else
            	continue;
         if(!insertFC(tLRPolSchema)){
        	 buildError("insertFC", "生成" + mGrpPolNo + "号,实收号为"+mPayNo+",保障计划为"+mContPlanCode+"" +
        	 			"的数据时发生错误");
             return false;
         }

         }
        }
        while (tLRPolSet.size() > 0);
        rswrapper.close();
        }
     catch (Exception ex) {
        ex.printStackTrace();
        rswrapper.close();
    }
        return true;
    }
    
    /**
     * 计算正常提取到的数据的分保提数处理
     * @param tLRInterfaceSchema
     * @param tDate
     * @param con
     * @return boolean
     */

    //在成数续期时插入首期保费的负记录，以计算出续期反冲数据
    private boolean insertFC(LRPolSchema tLRPolSchema){
   
        	String tChecksql1="";
        	tChecksql1="select count(1) from lrpol" +
        			" where GrpPolNo='"+tLRPolSchema.getGrpPolNo()+"' and recontcode='"+tLRPolSchema.getReContCode()+"' " +
        					" and getdatadate='"+this.mToday+"' and renewcount=0 and ReinsureItem='C' " +
//        							"and payno='"+tLRPolSchema.getPayNo()+"'" +
        									" and contplancode='"+tLRPolSchema.getContPlanCode()+"'" +
        											" and polstat='9' with ur";
        		
        		
        			
        	SSRS tCheck1 = new ExeSQL().execSQL(tChecksql1);
        	String tcheck1=tCheck1.GetText(1, 1);
        	
        if(Integer.parseInt(tcheck1)==0){
        	
        	try{
        		double tShouldMoney=0.00;
        		double tRate =0.00;
        		String tshouldMSql = "select sum(prem) from  lcgrppayplandetail where " +
        							"prtno='"+tLRPolSchema.getPrtNo()+"'  " +
        									" and riskcode='"+tLRPolSchema.getRiskCode()+"'" +
        											" and contplancode='"+tLRPolSchema.getContPlanCode()+"'" +
        													" and plancode<>'1' with ur";
        		 SSRS tMoneySSRS = new ExeSQL().execSQL(tshouldMSql);
        		 if(tMoneySSRS!=null && tMoneySSRS.MaxRow>0){
        			 
        			 tShouldMoney=Double.parseDouble(tMoneySSRS.GetText(1, 1));
                }
        		 
        		 String tRateSql="select factorvalue from lrcalfactorvalue where factorcode='CessionRate' " +
        		 		"and  recontcode='"+tLRPolSchema.getReContCode()+"' " +
        		 				" and riskcode='"+tLRPolSchema.getRiskCode()+"' " +
        		 						" union all " +
        		 						" select char(cessionrate) from lrtempcesscontinfo " +
        		 						" where tempcontcode='"+tLRPolSchema.getReContCode()+"' " +
        		 								" and riskcode='"+tLRPolSchema.getRiskCode()+"' " +
        		 										" and contplancode='"+tLRPolSchema.getContPlanCode()+"' with ur";
         		 SSRS tRateSSRS = new ExeSQL().execSQL(tRateSql);
         		 if(tRateSSRS!=null && tRateSSRS.MaxRow>0){
         			 
         			tRate=Double.parseDouble(tRateSSRS.GetText(1, 1));
                 }
        		String str1="";
        		str1="select GrpContNo,GrpPolNo," +
        		"ContNo,PolNo,ProposalNo,PrtNo," +
        		"ReContCode,'D',TempCessFlag," +
        		"ReNewCount,ContType,ManageCom,RiskCalSort,MainPolNo," +
        		"MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
        		"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate," +
        		"EndDate,StandPrem,"+tShouldMoney+",SumPrem,Amnt,InsurePeriod,OccupationType," +
        		"RiskAmnt,PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear," +
        		"InsuYearFlag,InsuYear,ProtItem,CessStart,CessEnd,'9',SignDate," +
        		"SumRiskAmount,NowRiskAmount,AppntNo,AppntName,AppntType,SaleChnl," +
        		""+tRate+",DiseaseAddFeeRate,'"+this.mToday+"',Operator,current date,current time," +
        		"current date,current time,CostCenter,ReType,'','',CessAmnt,payno," +
        				"contplancode,PolTypeFlag from lrpol a" +
        		" where grpcontno='"+tLRPolSchema.getGrpContNo()+"' and riskcode='"+tLRPolSchema.getRiskCode()+"'" +
        				" and renewcount=0 and conttype='2' and reinsureitem='C' and polstat='7' and " +
        				" recontcode='"+tLRPolSchema.getReContCode()+"' " +
        						" and contplancode='"+tLRPolSchema.getContPlanCode()+"'" +
//        								" and payno='"+tLRPolSchema.getPayNo()+"'" +
        						" and getdatadate='"+this.mToday+"' fetch first 1 rows only ";

        		
        		ExeSQL tExeSQL = new ExeSQL();
        		tExeSQL.execUpdateSQL("insert into lrpol(GrpContNo,GrpPolNo,ContNo," +
            		"PolNo,ProposalNo,PrtNo,ReContCode,ReinsureItem," +
            		"TempCessFlag,ReNewCount,ContType,ManageCom,RiskCalSort," +
            		"MainPolNo,MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
            		"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate,EndDate," +
            		"StandPrem,Prem,SumPrem,Amnt,InsurePeriod,OccupationType,RiskAmnt," +
            		"PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear,InsuYearFlag,InsuYear," +
            		"ProtItem,CessStart,CessEnd,PolStat,SignDate,SumRiskAmount,NowRiskAmount,AppntNo," +
            		"AppntName,AppntType,SaleChnl,DeadAddFeeRate,DiseaseAddFeeRate,GetDataDate,Operator," +
            		"MakeDate,MakeTime,ModifyDate,ModifyTime,CostCenter,ReType,ActuGetState,ActuGetNo," +
            		"CessAmnt,payno,contplancode,poltypeflag) " +str1
            		);
        		
        	}
        	catch(Exception ex) {
                ex.printStackTrace();
                return false;
                
            }
        		
        		
        	}
        
        	
        
        return true ;
    }

    
    

    












 
   




    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");
        mReContCode = (String) mGetCessData.getValueByName("ReContCode");
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(VData cInputData,String mToDay) {
       
        mStartDate = mToDay;
        mEndDate = mToDay;
        mReContCode = null;
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }


    public static void main(String[] args) {
    	LRNewYDGetCessDataBL tLRNewYDGetCessDataBL = new LRNewYDGetCessDataBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";
        String mOperateType = "CESSDATA";

        String startDate = "2012-2-22";
        String endDate = "2012-2-22";
        TransferData getCessData = new TransferData();
        getCessData.setNameAndValue("StartDate", startDate);
        getCessData.setNameAndValue("EndDate", endDate);

        VData tVData = new VData();
        tVData.addElement(globalInput);
        tVData.addElement(getCessData);

        tLRNewYDGetCessDataBL.submitData(tVData, mOperateType);
    }
}
