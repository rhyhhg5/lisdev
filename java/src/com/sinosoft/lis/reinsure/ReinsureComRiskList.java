package com.sinosoft.lis.reinsure;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.vschema.LRComRiskResultSet;
import com.sinosoft.lis.schema.LRComRiskResultSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.db.LRComRiskResultDB;

public class ReinsureComRiskList {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  MMap mMap;
  PubSubmit mPubSubmit = null;
  private VData tVData = new VData();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  String StartDate = "";
  String EndDate = "";
  String mComid = "";
  String RecontCode = "";
  String ReRiskCode = "";
  public ReinsureComRiskList() {
  }

  /**
   传输数据的公共方法
   */
  public boolean submitData(VData cInputData) {
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(cInputData)) {
          return false;
      }
      // 准备数据
      if (!dealData()) {
          return false;
      }

      return true;
  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
      //全局变量
      mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
              "GlobalInput", 0));

      TransferData tTransferData = (TransferData) cInputData.
                                   getObjectByObjectName("TransferData", 0);
      StartDate = (String) tTransferData.getValueByName("StartDate");
      EndDate = (String) tTransferData.getValueByName("EndDate");
      mComid = (String) tTransferData.getValueByName("MngCom");//管理机构
      RecontCode = (String) tTransferData.getValueByName("RecontCode");//再保合同号
      ReRiskCode = (String) tTransferData.getValueByName("ReRiskCode");//分保险种
      return true;
  }

  public String getResult() {
      return "";
  }

  public CErrors getErrors() {
      return mErrors;
  }

  private boolean dealData() {
          String terms = " and a.GetDataDate between '"+StartDate+"' AND '"+EndDate+"' ";
          if(mComid != null && mComid!= ""){
              terms = terms + " and a.managecom = '"+mComid+"'";
          }
          if(RecontCode != null && RecontCode!="" ){
              terms = terms + " and a.recontcode = '"+RecontCode+"'";
          }
          if(ReRiskCode != null && ReRiskCode != ""){
              terms  = terms + " and a.riskcode = '"+ReRiskCode+"'";
          }
          String cessprem = "select a.managecom,a.riskcode,a.recontcode,a.costcenter,sum(d.CessPrem),sum(d.ReProcFee) from lrpol a,lrpolresult d"
                      +" where a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ReNewCount = d.ReNewCount"
                      + terms
                      +" group by a.managecom,a.riskcode,a.recontcode,a.costcenter with ur";
          System.out.println(cessprem);
          SSRS tSSRS =  new ExeSQL().execSQL(cessprem);
          int count = tSSRS.getMaxRow();
          LRComRiskResultSet tLRComRiskResultSet = new LRComRiskResultSet();
          for(int i=1;i<=count;i++){
              LRComRiskResultSchema tLRComRiskResultSchema = new LRComRiskResultSchema();
              tLRComRiskResultSchema.setManageCom(tSSRS.GetText(i,1));
              tLRComRiskResultSchema.setRiskCode(tSSRS.GetText(i,2));
              tLRComRiskResultSchema.setRecontCode(tSSRS.GetText(i,3));
              if(tSSRS.GetText(i,4)!= null&&!tSSRS.GetText(i,4).equals("")){
                  tLRComRiskResultSchema.setCostCenter(tSSRS.GetText(i, 4));
              }else{
                  tLRComRiskResultSchema.setCostCenter("aa");
              }
              tLRComRiskResultSchema.setCessPrem(Double.parseDouble(tSSRS.GetText(i,5)));// 分保保费
              tLRComRiskResultSchema.setReProcFee(Double.parseDouble(tSSRS.GetText(i,6)));//分保手续费
              tLRComRiskResultSchema.setSumPrem(Double.parseDouble(tSSRS.GetText(i,5)));
              tLRComRiskResultSchema.setSumFee(Double.parseDouble(tSSRS.GetText(i,6)));
              tLRComRiskResultSchema.setStartDate(StartDate);
              tLRComRiskResultSchema.setEndData(EndDate);
              tLRComRiskResultSchema.setOperator(mGlobalInput.Operator);
              tLRComRiskResultSchema.setMakeDate(PubFun.getCurrentDate());
              tLRComRiskResultSchema.setMakeTime(PubFun.getCurrentTime());
              tLRComRiskResultSchema.setModifyDate(PubFun.getCurrentDate());
              tLRComRiskResultSchema.setModifyTime(PubFun.getCurrentTime());
              tLRComRiskResultSet.add(tLRComRiskResultSchema);
          }
          try{
              mMap = new MMap();
              mMap.put(tLRComRiskResultSet, "DELETE&INSERT");
              tVData.clear();
              tVData.add(mMap);
              mPubSubmit = new PubSubmit();
              mPubSubmit.submitData(this.tVData, "");
              System.out.println("－－－－－－－－－－分保数据保存完成－－－－－－－－－－");
          }catch(Exception e){
              mErrors = mPubSubmit.mErrors;
          }

          String  edorfee = "select a.managecom,a.riskcode,a.recontcode,a.costcenter,sum(d.EdorBackFee), sum(d.EdorProcFee) from LRPolEdor a, LRPolEdorResult d"
                            +" where a.polno = d.Polno and a.RecontCode = d.RecontCode "
                            + terms
                            +" group by a.managecom,a.riskcode,a.recontcode,a.costcenter"
                            +" with ur";
          System.out.println(edorfee);
          SSRS tEDORSSRS =  new ExeSQL().execSQL(edorfee);
          int countedor = tEDORSSRS.getMaxRow();
          tLRComRiskResultSet.clear(); //清空set中分保保费有关的值。
          for(int j=1;j<=countedor;j++){
              String costcenterterm = null;//如果成本中心字段为空，则置统一值;若成本中心有值，则将成本中心值赋给该字段。兼容之前的成本中心为空的记录。
              if(tEDORSSRS.GetText(j,4)==null ||tEDORSSRS.GetText(j,4).equals("")){
                  costcenterterm = "aa";
              }else{
                  costcenterterm = tEDORSSRS.GetText(j,4);
              }
              String result1 = "select * from LRComRiskResult where managecom='"+tEDORSSRS.GetText(j,1)
                               +"' and RiskCode='"+tEDORSSRS.GetText(j,2)
                               +"' and recontcode='"+tEDORSSRS.GetText(j,3)
                               +"' and costcenter='"+costcenterterm
                               +"' and StartDate='"+StartDate+"' and EndData='"+EndDate+"' with ur";
              System.out.println(result1);
             LRComRiskResultDB tLRComRiskResultDB = new LRComRiskResultDB();
             LRComRiskResultSet ttLRComRiskResultSet = tLRComRiskResultDB.executeQuery(result1);
             if(ttLRComRiskResultSet.size()==1){
                 //该条记录已经存在，则把它的保全分保数据更新。
                ttLRComRiskResultSet.get(1).setEdorBackFee(Double.parseDouble(tEDORSSRS.GetText(j,5)));
                ttLRComRiskResultSet.get(1).setEdorProcFee(Double.parseDouble(tEDORSSRS.GetText(j,6)));
                ttLRComRiskResultSet.get(1).setSumPrem(ttLRComRiskResultSet.get(1).getSumPrem()+Double.parseDouble(tEDORSSRS.GetText(j,5)));//分保保费合计
                ttLRComRiskResultSet.get(1).setSumFee(ttLRComRiskResultSet.get(1).getSumFee()+Double.parseDouble(tEDORSSRS.GetText(j,6)));//手续费合计
                tLRComRiskResultSet.add(ttLRComRiskResultSet);
             }else if(ttLRComRiskResultSet.size()<=0){
                 //该条记录不存在，则创建新的记录，记录它的保全分保数据。
              LRComRiskResultSchema ttLRComRiskResultSchema = new LRComRiskResultSchema();
              ttLRComRiskResultSchema.setManageCom(tEDORSSRS.GetText(j,1));
              ttLRComRiskResultSchema.setRiskCode(tEDORSSRS.GetText(j,2));
              ttLRComRiskResultSchema.setRecontCode(tEDORSSRS.GetText(j,3));
              if(tEDORSSRS.GetText(j,4)!= null&&!tEDORSSRS.GetText(j,4).equals("")){
                  ttLRComRiskResultSchema.setCostCenter(tEDORSSRS.GetText(j, 4));
              }else{
                  ttLRComRiskResultSchema.setCostCenter("aa");
              }

              ttLRComRiskResultSchema.setEdorBackFee(Double.parseDouble(tEDORSSRS.GetText(j,5)));// 保全摊回分保保费
              ttLRComRiskResultSchema.setEdorProcFee(Double.parseDouble(tEDORSSRS.GetText(j,6)));//保全摊回分保手续费
              ttLRComRiskResultSchema.setSumPrem(Double.parseDouble(tEDORSSRS.GetText(j,5)));//分保保费合计
              ttLRComRiskResultSchema.setSumFee(Double.parseDouble(tEDORSSRS.GetText(j,6)));//手续费合计
              ttLRComRiskResultSchema.setStartDate(StartDate);
              ttLRComRiskResultSchema.setEndData(EndDate);
              ttLRComRiskResultSchema.setOperator(mGlobalInput.Operator);
              ttLRComRiskResultSchema.setMakeDate(PubFun.getCurrentDate());
              ttLRComRiskResultSchema.setMakeTime(PubFun.getCurrentTime());
              ttLRComRiskResultSchema.setModifyDate(PubFun.getCurrentDate());
              ttLRComRiskResultSchema.setModifyTime(PubFun.getCurrentTime());
              tLRComRiskResultSet.add(ttLRComRiskResultSchema);
             }
          }
          try{
            mMap = new MMap();
            mMap.put(tLRComRiskResultSet, "DELETE&INSERT");
            tVData.clear();
            tVData.add(mMap);
            mPubSubmit = new PubSubmit();
            mPubSubmit.submitData(this.tVData, "DELETE&INSERT");
            System.out.println("－－－－－－－－－－保全数据保存完成－－－－－－－－－－");
         }catch(Exception e){
            mErrors = mPubSubmit.mErrors;
         }
         //处理理赔数据，查询出所有理赔赔款数据。
         String  claimfee = "select a.managecom,a.riskcode,a.recontcode,a.costcenter,sum(e.ClaimBackFee) from LRPolClm a, LRPolClmResult e"
                           +" where  a.PolNo = e.Polno and a.RecontCode = e.RecontCode and a.ClmNo = e.ClmNo"
                           + terms
                           +" group by a.managecom,a.riskcode,a.recontcode,a.costcenter"
                           +" with ur";
        System.out.println(claimfee);//
         SSRS tCLAIMSSRS =  new ExeSQL().execSQL(claimfee);
         int countclaim = tCLAIMSSRS.getMaxRow();
         tLRComRiskResultSet.clear(); //清空set中保全分保保费有关的值。
         for(int k=1;k<=countclaim;k++){
             String costcenterterm = null;//如果成本中心字段为空，则置统一值;若成本中心有值，则将成本中心值赋给该字段。兼容之前的成本中心为空的记录。
              if(tCLAIMSSRS.GetText(k,4)==null ||tCLAIMSSRS.GetText(k,4).equals("")){
                  costcenterterm = "aa";
              }else{
                  costcenterterm = tCLAIMSSRS.GetText(k,4);
              }

             String result1 = "select * from LRComRiskResult where managecom='"+tCLAIMSSRS.GetText(k,1)
                              +"' and RiskCode='"+tCLAIMSSRS.GetText(k,2)
                              +"' and recontcode='"+tCLAIMSSRS.GetText(k,3)
                              +"' and costcenter='"+costcenterterm
                              +"' and StartDate='"+StartDate+"' and EndData='"+EndDate+"' with ur";
             System.out.println(result1);//
            LRComRiskResultDB tLRComRiskResultDB = new LRComRiskResultDB();
            LRComRiskResultSet tttLRComRiskResultSet = tLRComRiskResultDB.executeQuery(result1);
            if(tttLRComRiskResultSet.size()==1){
                //该条记录已经存在，则把它的保全分保数据更新。
               tttLRComRiskResultSet.get(1).setClaimBackFee(Double.parseDouble(tCLAIMSSRS.GetText(k,5)));
               tLRComRiskResultSet.add(tttLRComRiskResultSet);
            }else if(tttLRComRiskResultSet.size()<=0){
                //该条记录不存在，则创建新的记录，记录它的保全分保数据。
             LRComRiskResultSchema tttLRComRiskResultSchema = new LRComRiskResultSchema();
             tttLRComRiskResultSchema.setManageCom(tCLAIMSSRS.GetText(k,1));
             tttLRComRiskResultSchema.setRiskCode(tCLAIMSSRS.GetText(k,2));
             tttLRComRiskResultSchema.setRecontCode(tCLAIMSSRS.GetText(k,3));
             if(tCLAIMSSRS.GetText(k,4)!=null && !tCLAIMSSRS.GetText(k,4).equals("")){
                 tttLRComRiskResultSchema.setCostCenter(tCLAIMSSRS.GetText(k,4));
             }else{
                 tttLRComRiskResultSchema.setCostCenter("aa");
             }
             tttLRComRiskResultSchema.setClaimBackFee(Double.parseDouble(tCLAIMSSRS.GetText(k,5)));// 理赔摊回
             tttLRComRiskResultSchema.setStartDate(StartDate);
             tttLRComRiskResultSchema.setEndData(EndDate);
             tttLRComRiskResultSchema.setOperator(mGlobalInput.Operator);
             tttLRComRiskResultSchema.setMakeDate(PubFun.getCurrentDate());
             tttLRComRiskResultSchema.setMakeTime(PubFun.getCurrentTime());
             tttLRComRiskResultSchema.setModifyDate(PubFun.getCurrentDate());
             tttLRComRiskResultSchema.setModifyTime(PubFun.getCurrentTime());
             tLRComRiskResultSet.add(tttLRComRiskResultSchema);
            }
         }
         try{
           mMap = new MMap();
           mMap.put(tLRComRiskResultSet, "DELETE&INSERT");
           tVData.clear();
           tVData.add(mMap);
           mPubSubmit = new PubSubmit();
           mPubSubmit.submitData(this.tVData, "DELETE&INSERT");
           System.out.println("－－－－－－－－－－理赔数据保存完成－－－－－－－－－－");
        }catch(Exception e){
           mErrors = mPubSubmit.mErrors;
        }


     return true;
  }


  public static void main(String[] args) {
       GlobalInput globalInput = new GlobalInput();
       globalInput.ManageCom = "86";
       globalInput.Operator = "zhangb";

       // prepare main plan
       // 准备传输数据 VData
       VData vData = new VData();
       TransferData tTransferData = new TransferData();
       ReinsureComRiskList tReinsureComRiskList = new ReinsureComRiskList();
       try {
           vData.add(globalInput);
           tTransferData.setNameAndValue("StartDate", "2008-9-1");
           tTransferData.setNameAndValue("EndDate", "2008-9-30");
           System.out.println("in main: " +
                              (String) tTransferData.
                              getValueByName("StartDate") + "   " +
                              (String) tTransferData.getValueByName("EndDate"));
           vData.add(tTransferData);
           tReinsureComRiskList.submitData(vData);
       } catch (Exception ex) {
           ex.printStackTrace();
       }
  }
}
