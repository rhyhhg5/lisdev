package com.sinosoft.lis.reinsure;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import com.sinosoft.lis.db.LRContInfoDB;
import com.sinosoft.lis.db.LRGetDataLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRContInfoSchema;
import com.sinosoft.lis.vschema.LRContInfoSet;
import com.sinosoft.lis.vschema.LRGetDataLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ReContInterfaceGetData
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String strOperate;


    private PubSubmit tPubSubmit = new PubSubmit();

    private TransferData mTransferData = new TransferData();
    private String mStartDate="";
    private String mEndDate="";
    private String mReContCode="";
    private String mToDate =null;

    //业务处理相关变量
    /** 全局数据 */

    public ReContInterfaceGetData() {
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        mInputData=cInputData;
        this.strOperate = cOperate;
        if (strOperate == null || strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        System.out.println("before getInputData()........");
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("after getInputData()........");

        if (!dealData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";
        ReContInterfaceGetData a=new ReContInterfaceGetData();
        a.dealData();
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReContInterfaceGetDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        Connection con=null;
        //String mToDate =null;
        try{
            con=DBConnPool.getConnection();
            con.setAutoCommit(false);
            FDate chgdate = new FDate();
            String currentDate=PubFun.calDate(PubFun.getCurrentDate(),-1,"D",null);
            String Serialno="";//提数日志表流水号
            Date startdate = chgdate.getDate(currentDate); //录入的起始日期，用FDate处理
            Date dedate = chgdate.getDate(currentDate); //录入的终止日期，用FDate处理
            if(!StrTool.cTrim(this.mStartDate).equals("") && !StrTool.cTrim(this.mEndDate).equals("")){
                startdate = chgdate.getDate(this.mStartDate); //录入的起始日期，用FDate处理
                dedate = chgdate.getDate(this.mEndDate); //录入的终止日期，用FDate处理
            }
                       	
            
            Date dbdate = startdate; //录入的终止日期，用FDate处理
            dbdate = startdate; //录入的终止日期，用FDate处理
            
            LRGetDataLogDB tLRGetDataLogDB=new LRGetDataLogDB(con);
            while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            	mToDate = chgdate.getString(dbdate); //录入的正在统计的日期
                SSRS tSSRS = new ExeSQL().execSQL("select SerialNo from LRGetDataLog where enddate='"+mToDate+"' with ur");
               
               
                	//校验日期是否提过数
                if(tSSRS!=null && tSSRS.MaxRow>0){
                	 /**
                     * 校验是否补提数，通过页面是否录入过合同号进行判断
                     */
                	 if(this.mReContCode==null||"".equals(this.mReContCode)){
                		 this.mErrors.addOneError("日期"+mToDate+"已经提过数");
                		 dbdate = PubFun.calDate(dbdate, 1, "D", null);
                		continue;
                	 }
                	 else{
                     	Serialno=tSSRS.GetText(1, 1);//如果为补提则直接用之前数据库里存的流水号处理
                     }
                }
                else{
                Serialno=PubFun1.CreateMaxNo("ReContGetData", 15);
                
                
                tLRGetDataLogDB.setEndDate(mToDate);
                tLRGetDataLogDB.setStartDate(mToDate);
                tLRGetDataLogDB.setMakeDate(PubFun.getCurrentDate());
                tLRGetDataLogDB.setMakeTime(PubFun.getCurrentTime());
//                tLRGetDataLogDB.setOperator(this.globalInput.Operator);
                tLRGetDataLogDB.setSerialNo(Serialno);
                tLRGetDataLogDB.insert();
                }
                Statement stmt = con.createStatement();
                ResultSet tRs = null;
                String ttSql = "select calsql,tablename,calcode from LRGetDataClassDef where calcode<>'004' and dealclass='T'  order by ordernum with ur";
                ExeSQL tESQL = new ExeSQL();
                tSSRS = tESQL.execSQL(ttSql);
                if(tSSRS!=null && tSSRS.MaxRow>0){
                    for(int index=1;index<=tSSRS.MaxRow;index++){
                        String exeSql="";
                        PubCalculator tPubCalculator = new PubCalculator();
                        tPubCalculator.addBasicFactor("ToDay",mToDate);
                        tPubCalculator.addBasicFactor("Operator", this.globalInput.Operator);
                        tPubCalculator.addBasicFactor("SerialNo", Serialno);
                       //校验是否补提数，通过页面是否录入过合同号进行判断
                         if(this.mReContCode==null||"".equals(this.mReContCode)){//正常提数
                        	 tPubCalculator.addBasicFactor("DataState", "0");//数据状态 '0' 正常状态  '1' 分保完成  '-1' 数据补提
                        	 tPubCalculator.addBasicFactor("SingRecontFlag", "0");//0 非单再保合同补提  1 单再保合同补提 
                         }
                         else{//补提
                        	  //先判断补提合同的险种是否已经存在于中间表中
//                            	 SSRS ttSSRS = new ExeSQL().execSQL("select 1 from lrinterface " +
//                            	 		"where dutydate='"+mToDate+"' and " +
//                            	 		" riskcode  in (select distinct(riskcode) from " +
//                            	 		"lrcalfactorvalue where recontcode='"+mReContCode+"' union all " +
//                            	 		" select distinct(riskcode) from lmriskduty where dutycode in" +
//                            	 		"(select dutycode from lmdutygetrela where getdutycode " +
//                            	 		"in (select distinct(riskcode) from lrcalfactorvalue " +
//                            	 		"where recontcode='"+mReContCode+"'))) fetch first 1 rows only with ur");
//                            	 
//                                 if(ttSSRS!=null && ttSSRS.MaxRow>0){//险种存在，直接更新State字段为-1，补提状态
//                                	 ExeSQL tREExeSQL = new ExeSQL();
//                                	 String mEDate = chgdate.getString(dedate);
//                                     tREExeSQL.execUpdateSQL("update lrinterface " +
//                                     		"set State='-1',modifydate=current date,modifytime=current time " +
//                                     		"where dutydate between '"+mToDate+"' and '"+mEDate+"' and " +
//                                     		" riskcode in (select distinct(riskcode) " +
//                                     		"from lrcalfactorvalue where recontcode='"+mReContCode+"' union all " +
//                                     		" select distinct(riskcode) from lmriskduty " +
//                                     		"where dutycode in" +
//                                     		"(select dutycode from lmdutygetrela where getdutycode " +
//                                     		"in (select distinct(riskcode) from lrcalfactorvalue " +
//                                     		"where recontcode='"+mReContCode+"')))");
//                                     if(GetCalDate()==false){
//                                 		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
//                                 		return false;
//                                 	}
//                                     	con.commit();
//                                     	con.close();
//                                     	return true;
//                                     
//                                 }
                        	 
//                                 else{//补提险种不存在，插入到中间表
                        		 
                                	 tPubCalculator.addBasicFactor("DataState", "-1");
                                	 tPubCalculator.addBasicFactor("SingRecontFlag", "1");
                                	 tPubCalculator.addBasicFactor("ReContCode", this.mReContCode);
//                                 }
                         }
                        tPubCalculator.setCalSql(tSSRS.GetText(index, 1));
                        exeSql = tPubCalculator.calculateEx();
                        System.out.println(tSSRS.GetText(index, 3)+":::"+exeSql);
                        tRs=stmt.executeQuery(exeSql);
                        if(insert(tRs,tSSRS.GetText(index, 2),con)==false){
                            return false;
                        }
                    }
                }
                String ttSqlt = "select calsql,tablename,calcode from LRGetDataClassDef where calcode<>'004' and dealclass='TT'  order by ordernum with ur";
                ExeSQL tESQLt = new ExeSQL();
                tSSRS = tESQLt.execSQL(ttSqlt);
                if(tSSRS!=null && tSSRS.MaxRow>0){
                    for(int index=1;index<=tSSRS.MaxRow;index++){
                        String exetSql="";
                        PubCalculator ttPubCalculator = new PubCalculator();
                        ttPubCalculator.addBasicFactor("ToDay",mToDate);
                        ttPubCalculator.addBasicFactor("Operator", "Server");
                        ttPubCalculator.addBasicFactor("SerialNo", Serialno);
                        ttPubCalculator.addBasicFactor("DataState", "2");//数据状态 '0' 正常状态  '1' 分保完成  '-1' 数据补提 '2' 临分提数
                        //ttPubCalculator.addBasicFactor("SingRecontFlag", "0");//0 非单再保合同补提  1 单再保合同补提 
                        if(this.mReContCode==null||"".equals(this.mReContCode)){//正常提数
                        	ttPubCalculator.addBasicFactor("SingRecontFlag", "0");//0 非单再保合同补提  1 单再保合同补提 
                        }
                        else{
                        	ttPubCalculator.addBasicFactor("SingRecontFlag", "1");
                        	ttPubCalculator.addBasicFactor("ReContCode", this.mReContCode);
                        }
                        ttPubCalculator.setCalSql(tSSRS.GetText(index, 1));
                        exetSql = ttPubCalculator.calculateEx();
                        System.out.println(tSSRS.GetText(index, 3)+":::"+exetSql);
                        tRs=stmt.executeQuery(exetSql);
                        if(insert(tRs,tSSRS.GetText(index, 2),con)==false){
                            return false;
                        }
                    }
                    con.commit();
                }
                con.commit();
                
         
                mToDate = chgdate.getString(startdate); //录入的正在统计的日期
                startdate = PubFun.calDate(startdate, 1, "D", null); //将其日期+1天
                LRGetDataLogDB ttLRGetDataLogDB=new LRGetDataLogDB();
                ttLRGetDataLogDB.setStartDate(mToDate);
                ttLRGetDataLogDB.setEndDate(mToDate);
//                ttLRGetDataLogDB.setOperator(this.globalInput.Operator);
                LRGetDataLogSet tLRGetDataLogSet=ttLRGetDataLogDB.query();
                if(tLRGetDataLogSet!=null){
                    for(int i=1;i<=tLRGetDataLogSet.size();i++){
                        //con.setAutoCommit(false);
                        //提取给付责任数据
                        String ttSql1 = "select calsql,tablename from LRGetDataClassDef where calcode='004'  order by ordernum with ur";
                        ExeSQL tESQL1 = new ExeSQL();
                        SSRS tSSRS1 = tESQL.execSQL(ttSql1);
                        String exeSql="";
                        PubCalculator tPubCalculator = new PubCalculator();
                        tPubCalculator.addBasicFactor("ToDay",mToDate);
                        tPubCalculator.addBasicFactor("Operator", this.globalInput.Operator);
                        tPubCalculator.addBasicFactor("SerialNo", tLRGetDataLogSet.get(i).getSerialNo());
                     
                        tPubCalculator.setCalSql(tSSRS1.GetText(1, 1));
                        exeSql = tPubCalculator.calculateEx();
                        Statement stmt1 = con.createStatement();
                        ResultSet tRs1=stmt.executeQuery(exeSql);
                        if(insert(tRs1,tSSRS1.GetText(1, 2),con)==false){
                            return false;
                        }
                        
                       
                        con.commit();
                        dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
                        
                        if(GetCalDate(mToDate)==false){
                    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
                    		return false;
                    	}
                        
                    }
                }
            }
            con.commit();
//            if(this.mReContCode!=null&&!"".equals(this.mReContCode)){
//            	tLRGetDataLogDB.delete();
//            }
            con.close();
        }catch(Exception e){
            try{
                e.printStackTrace();
                con.close();
                return false;
            }catch(Exception c){}
        }
        return true;
    }
    /**
     * 处理中间表提数后的操作
     * @param mToDate
     * @return
     */
    private boolean GetCalDate(String mToDate){
    	
    	//tempDeal();//临分提数
    	
    	if(!cessDeal()){//新单续期续保提数计算
    		return false;
    	}
    	
    	
    	
//    	if(!BDeal()){//提前提取b表数据
//        	System.out.println("当日B表数据提取出错");
//        }
    	if(this.mReContCode==null||"".equals(this.mReContCode)){
    		System.out.println("*******************98");
        	if(!CHeckcessDeal()){//新单续期续保提数计算
        		return false;
        	} 
        	
    		if(!edorDeal()){//保全摊回提数计算
        		return false;
        	}
        	if(!clmDeal()){//理赔摊回提数计算
        		return false;
        	}
        	
//        	if(!GBDeal()){//共保提数
//        		return false;
//        	}
        	if(!listDeal()){//清单提数
        		return false;
        	}
    	}
    	
    	
    	return true;
    }
    
//    private void tempDeal(){//调用临分程序
//    	ReTempGetDateTBL tReTempGetDateTBL=new ReTempGetDateTBL();
//    	tReTempGetDateTBL.run();
//    	
//    }
//    
//    private boolean GBDeal(){//调用共保提数程序
//    	ReGContInterfaceGetData tReGContInterfaceGetData=new ReGContInterfaceGetData();
//    	if(!tReGContInterfaceGetData.submitData()){
//    		System.out.println("当日共保数据提取出错");
//    		return false;
//    	}
//    	return true;
//    }
//    
//    private boolean BDeal(){//调用B表提数程序
//    	ReBContInterfaceGetData tReBContInterfaceGetData=new ReBContInterfaceGetData();
//        if(!tReBContInterfaceGetData.submitData()){
//        	System.out.println("当日B表数据提取出错");
//        	return false;
//        }
//    	return true;
//    }
    
    /**新单续期续保提数计算
     * cessDeal
     * @return boolean
     */
    private boolean cessDeal(){
    	
    	LRNewGetCessDataBL tLRNewGetCessDataBL=new LRNewGetCessDataBL();
    	if(tLRNewGetCessDataBL.submitDataT(mInputData, this.globalInput.Operator,mToDate)==false){
    		return false;
    	}
    	
    	LRNewYDGetCessDataBL tLRNewYDGetCessDataBL=new LRNewYDGetCessDataBL();
    	if(tLRNewYDGetCessDataBL.submitDataT(mInputData, this.globalInput.Operator,mToDate)==false){
    		return false;
    	}

    	LRNewCalCessDataBL tLRNewCalCessDataBL=new LRNewCalCessDataBL();
    	if(tLRNewCalCessDataBL.submitDataT(mInputData, this.globalInput.Operator,mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
	   /**特别险种新单续期续保提数计算
     * cessDeal
     * @return boolean
     */
    private boolean CHeckcessDeal(){
    	
    	LRAllDataDealBL tLRAllDataDealBL=new LRAllDataDealBL();
    	if(tLRAllDataDealBL.submitDataT(this.globalInput.Operator,mToDate)==false){
    		return false;
    	}
    	
    	LRCalDataDealALLBL tLRCalDataDealALLBL=new LRCalDataDealALLBL();
    	if(tLRCalDataDealALLBL.submitDataT(this.globalInput.Operator,mToDate)==false){
    		return false;
    	}
    	return true;
    	
    }
    
    /**保全摊回提数计算
     * edorDeal
     * @return boolean
     */
    private boolean edorDeal(){
    	
    	LRNewGetEdorDataTBL tLRNewGetEdorDataTBL=new LRNewGetEdorDataTBL();
    	if(tLRNewGetEdorDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRNewCalEdorDataTBL tLRNewCalEdorDataTBL=new LRNewCalEdorDataTBL();
    	if(tLRNewCalEdorDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
    /**理赔摊回提数计算
     * clmDeal
     * @return boolean
     */
    private boolean clmDeal(){
    	
    	LRNewGetClaimDataTBL tLRNewGetClaimDataTBL=new LRNewGetClaimDataTBL();
    	if(tLRNewGetClaimDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
//    	LRNewGetClaimDataRBBL tLRNewGetClaimDataRBBL =new LRNewGetClaimDataRBBL();
//    	if(tLRNewGetClaimDataRBBL.submitData()==false){
//    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
//    		return false;
//    	}
    	LRNewCalClaimDataTBL tLRNewCalClaimDataTBL=new LRNewCalClaimDataTBL();
    	if(tLRNewCalClaimDataTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    
    /**清单提数
     * listDeal
     * @return boolean
     */
    private boolean listDeal(){
    	
    	LRCessListTBL tLRCessListTBL=new LRCessListTBL();//分保清单
    	if(tLRCessListTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	LRClaimListTBL tLRClaimListTBL=new LRClaimListTBL();
    	if(tLRClaimListTBL.submitDataT("Server",mToDate)==false){
    		//System.out.println("批次"+tLRGetDataLogSet.get(i).getSerialNo()+"算分保保费失败");
    		return false;
    	}
    	return true;
    	
    }
    private boolean insert(ResultSet rs,String table,Connection con){
        Statement stmt = null;
        ResultSet tRs = null;
        ResultSetMetaData rsmd = null;

        ResultSetMetaData rsmd1 = null;
        try{
            stmt = con.createStatement();
            tRs = stmt.executeQuery("select * from " + table+" fetch first 1 rows only");
            rsmd = tRs.getMetaData();
            rsmd1 = rs.getMetaData();
            int columnCount=rsmd.getColumnCount();
            int columnCount1=rsmd1.getColumnCount();
            if(columnCount!=columnCount1){
                this.mErrors.addOneError("与目标的列数不符");
                return false;
            }
            System.out.println("begin......");
            while(rs.next()){
                StringBuffer insertSql = new StringBuffer();
                StringBuffer whereSql=new StringBuffer();
                insertSql .append("insert into "+table+" (");
                int flag=0;
                for (int i = 0; i < columnCount; i++) {
                    if(flag==1){
                        insertSql.append(",");
                        whereSql.append(",");
                    }
                    insertSql .append(rsmd.getColumnName(i+1));

                    int mDataType=rsmd.getColumnType(i+1);
//                  判断数据类型
                    if ((mDataType == Types.CHAR) || (mDataType == Types.VARCHAR)) {
                        if(rs.getString(i+1)==null){
                            whereSql.append(rs.getObject(i+1));
                        }else{
                            whereSql.append("'");
                            whereSql.append(StrTool.cTrim(rs.getString(i+1)));
                            whereSql.append("'");
                        }
                    }
                    else if ((mDataType == Types.TIMESTAMP) || (mDataType == Types.DATE)) {
                        if(rs.getDate(i+1)==null){
                            whereSql.append(rs.getObject(i+1));
                        }else{
                            whereSql.append("'");
                            whereSql.append(rs.getDate(i+1));
                            whereSql.append("'");
                        }
                    }else{
                        whereSql.append(rs.getObject(i+1));
                    }
                    flag=1;
                }
                insertSql .append(") values ( ");
                insertSql .append(whereSql.toString());
                insertSql .append(")");
                String ExeSql=insertSql.toString();
                try{
                    //System.out.println(ExeSql);
                    stmt.executeUpdate(ExeSql);
                }catch(Exception insertExc){
                    System.out.println("Error Insert......"+ExeSql);
                }
            }
            con.commit();
            stmt.close();
        }catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
            this.mErrors.addOneError("插入语句拼写错误");
            return false;
        }
        
        return true;
    }



    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        this.mStartDate = (String) mTransferData.getValueByName("StartDate");
        this.mEndDate = (String) mTransferData.getValueByName("EndDate");
        this.mReContCode = (String) mTransferData.getValueByName("ReContCode");
        return true;
    }

    /*
     *
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReContInterfaceGetDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
