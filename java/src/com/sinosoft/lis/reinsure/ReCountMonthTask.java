package com.sinosoft.lis.reinsure;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.db.LRAccountsDB;
import com.sinosoft.lis.db.LRPolClmDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.db.LRPolEdorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.vschema.LRPolClmSet;
import com.sinosoft.lis.vschema.LRPolEdorSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ReCountMonthTask
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String strOperate;


    private PubSubmit tPubSubmit = new PubSubmit();

    private TransferData mTransferData = new TransferData();
    private String mToDate =null;
    private String mYear;
    private String mMonth;


    //业务处理相关变量
    /** 全局数据 */

    public ReCountMonthTask() {
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;

        System.out.println("before getInputData()........");
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("after getInputData()........");

        if (!dealData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";
        ReCountMonthTask a=new ReCountMonthTask();
        a.dealData();
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReContInterfaceGetDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        String tCurrentDate=PubFun.getCurrentDate();
        String tStartDate=this.getLastMonth(new Date());//初始值赋值
        String tEndDate=tCurrentDate;//初始值赋值
        if(!StrTool.cTrim(this.mYear).equals("") && !StrTool.cTrim(this.mMonth).equals("")){
            Calendar calendar=Calendar.getInstance();
            calendar.set(Calendar.YEAR,Integer.parseInt(this.mYear));
            calendar.set(Calendar.MONTH,Integer.parseInt(this.mMonth));
            calendar.set(Calendar.DATE,1);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            tCurrentDate = sdf.format(calendar.getTime());
            calendar.set(Calendar.MONTH,Integer.parseInt(this.mMonth)-1);
            if("12".equals(this.mMonth))
            	tStartDate=this.mYear+"-"+this.mMonth+"-1";
            else
            	tStartDate=sdf.format(calendar.getTime());
            tEndDate= PubFun.calDate(tCurrentDate,-1,"D","2009-1-31");  //结束日期取本月最后一天
        }
        String[] tDate=tCurrentDate.split("-");
        if(tDate==null){
            return false;
        }
        if(!tDate[2].equals("01")){
            return false;
        }
        Connection con=null;
        con=DBConnPool.getConnection();

//        LRContInfoDB tLRContInfoDB=new LRContInfoDB();
        //查询系统中当前生效的合同。不支持如果反冲数据的合同已失效的情况。………………
        String contInfoSql="select recontcode,recontname,Recomcode from lrcontinfo a where ReContState='01' and " + //查询所有在账单日期范围内有效的合同
                       "((Rinvalidate is not null and Rinvalidate>='"+tStartDate+"') or ( Rinvalidate is  null and Rvalidate<= '"+tEndDate+"')) and " +
                       		"exists (select 1 from lrpol where getdatadate between '"+tStartDate+"' and '"+tEndDate+"' and recontcode=a.recontcode) "+
                       "union all "+
                       "select TempContCode,TempContName,ComCode from LRTempCessCont b where ReContState='01' "+ //临分的保单以合同录入日期(MakeDate)为准。
                       "and  makedate between '"+tStartDate+"' and '"+tEndDate+"'  and " +
                       	"exists (select 1 from lrpol where getdatadate " +
                       	"between '"+tStartDate+"' and '"+tEndDate+"' and recontcode=b.TempContCode) with ur";

        SSRS cSSRS= new ExeSQL().execSQL(contInfoSql);
        if(cSSRS == null || cSSRS.getMaxRow()<=0){
             buildError("DealData","没有查询到有效的合同！");
             return false;
        }
        try{
            //con.setAutoCommit(false);
            int count=cSSRS.getMaxRow();
            for(int index=1;index<=count;index++){
//                LRContInfoSchema tLRContInfoSchema=tLRContInfoSet.get(index);
                int flag=0;//表示是否往账单表里插入数据
                String tSql="select count(1) from lrpol where  (ActuGetState is null or trim(ActuGetState)='') and (ActuGetNo is null or trim(ActuGetNo)='') and "+
                        "recontcode='"+cSSRS.GetText(index,1)+"' and getdatadate between '"+tStartDate+"' and '"+tEndDate+"' "+
                                                "with ur";
//                RSWrapper rswrapper = new RSWrapper();
//                LRPolSet tLRPolSet = new LRPolSet();
//                rswrapper.prepareData(tLRPolSet, tSql);
                String lrscount = new ExeSQL().getOneValue(tSql);
                int lrcount = Integer.parseInt(lrscount); 
                String tActugetNo="";
               if((lrcount> 0)){
                	//rswrapper.getData();
                	tActugetNo=PubFun1.CreateMaxNo("ReActugetNo", 20);
//                    for (int i = 1; i <= tLRPolSet.size(); i++) {
//                        flag=1;
//                        LRPolSchema tLRPolSchema = tLRPolSet.get(i);
//                        LRPolDB tLRPolDB=new LRPolDB(con);
//                        tLRPolDB.setSchema(tLRPolSchema);
//                        tLRPolDB.setActuGetNo(tActugetNo);
//                        tLRPolDB.setModifyDate(tCurrentDate);
//                        tLRPolDB.setModifyTime(PubFun.getCurrentTime());
//                        tLRPolDB.setActuGetState("00");
//                        tLRPolDB.update();
//                    }
                    flag=1;
                    ExeSQL tExeSQL = new ExeSQL();
                    tExeSQL.execUpdateSQL("update lrpol set ActuGetNo='"+tActugetNo+"',ModifyDate='"+tCurrentDate+"'," +
                    						"ModifyTime='"+PubFun.getCurrentTime()+"',ActuGetState='00' where " +
//                    						" (ActuGetState is null or trim(ActuGetState)='') and " +
//                    						"(ActuGetNo is null or trim(ActuGetNo)='') and "+
                    						"recontcode='"+cSSRS.GetText(index,1)+"' and " +
                    						"getdatadate between '"+tStartDate+"' and '"+tEndDate+"' ");
                 }  ;
                 if(flag==1){  //插入账单表数据
                    LRAccountsDB tLRAccountsDB=new LRAccountsDB(con);
                    tLRAccountsDB.setActuGetNo(tActugetNo);
                    tLRAccountsDB.setFeeNo("0");
                    tLRAccountsDB.setRecontCode(cSSRS.GetText(index,1));
                    tLRAccountsDB.setRecontName(cSSRS.GetText(index,2));
                    tLRAccountsDB.setReComCode(cSSRS.GetText(index,3));
                    String strSQL = "select recomname from lrrecominfo where recomcode='"+cSSRS.GetText(index,3)+"' with ur";
                    String comName = new ExeSQL().getOneValue(strSQL);
                    tLRAccountsDB.setReComName(comName);
                    tLRAccountsDB.setStartDate(tStartDate);
                    tLRAccountsDB.setEndData(tEndDate);
                    tLRAccountsDB.setPayFlag("00");
                    tLRAccountsDB.setStandby3("0");
                    tLRAccountsDB.setCessPrem(0);
                    tLRAccountsDB.setReProcFee(0);
                    tLRAccountsDB.setClaimBackFee(0);
                    tLRAccountsDB.setMakeDate(PubFun.getCurrentDate());
                    tLRAccountsDB.setMakeTime(PubFun.getCurrentTime());
                    tLRAccountsDB.setModifyDate(PubFun.getCurrentDate());
                    tLRAccountsDB.setModifyTime(PubFun.getCurrentTime());
                    tLRAccountsDB.setOperator("server");
                    tLRAccountsDB.insert();
               }
             }
            //con.commit();
            con.close();
         }catch(Exception e){
            try{
                con.close();
            }catch(Exception c){}
         }
         System.out.println("账单批处理分保信息处理完成！");

//             处理保全数据
         String  edorsql = "select distinct recontcode from lrpoledor where getdatadate between '"+tStartDate+"' and '"+tEndDate+"' with ur" ;
         SSRS eSSRS = new ExeSQL().execSQL(edorsql);
         if(eSSRS==null || eSSRS.getMaxRow()<=0){
              buildError("DealData","没有查询到保全摊回所涉及的合同信息！");
         }else{
             try{
                 //con.setAutoCommit(false);
                 for (int index = 1; index <= eSSRS.getMaxRow(); index++) {
                     String cSql = "select actugetno from LRAccounts where recontcode='"+eSSRS.GetText(index, 1)+"' and startdate='"+tStartDate+"' and enddata='"+tEndDate+"' with ur";//判断该合同该时间段的账单是否有记录
                     String cactugetno = new ExeSQL().getOneValue(cSql);
                     String tActugetNo = "";
                     if(cactugetno==null||cactugetno.equals("")){
                       tActugetNo=PubFun1.CreateMaxNo("ReActugetNo", 20);
                     }else{
                         tActugetNo = cactugetno;
                     }
                     String tSql =
                             "select count(1) from LRPolEdor where (ActuGetState is null or trim(ActuGetState)='') and (ActuGetNo is null or trim(ActuGetNo)='') and " +
                             "recontcode='" + eSSRS.GetText(index, 1) +
                             "' and GetDataDate between '" + tStartDate +
                             "' and '" + tEndDate + "' with ur";
//                     LRPolEdorSet tLRPolEdorSet = new LRPolEdorSet();
//                     RSWrapper erswrapper = new RSWrapper();
//                     erswrapper.prepareData(tLRPolEdorSet, tSql);
                     String lrescount = new ExeSQL().getOneValue(tSql);
                     int lrecount = Integer.parseInt(lrescount); 
                     if((lrecount > 0)){
                         //erswrapper.getData();
//                         for (int i = 1; i <= tLRPolEdorSet.size(); i++) {
//                             LRPolEdorDB tLRPolEdorDB = new LRPolEdorDB(con);
//                             tLRPolEdorDB.setSchema(tLRPolEdorSet.get(i));
//                             tLRPolEdorDB.setActuGetNo(tActugetNo);
//                             tLRPolEdorDB.setActuGetState("00");
//                             tLRPolEdorDB.setModifyDate(tCurrentDate);
//                             tLRPolEdorDB.setModifyTime(PubFun.getCurrentTime());
//                             tLRPolEdorDB.update();
//                         }
                         ExeSQL tExeSQL = new ExeSQL();
                         tExeSQL.execUpdateSQL("update LRPolEdor set ActuGetNo='"+tActugetNo+"',ModifyDate='"+tCurrentDate+"'," +
                         						"ModifyTime='"+PubFun.getCurrentTime()+"',ActuGetState='00' where " +
//                         						"(ActuGetState is null or trim(ActuGetState)='') and " +
//                         						"(ActuGetNo is null or trim(ActuGetNo)='') and " +
                         						"recontcode='" + eSSRS.GetText(index, 1) +
                         						"' and GetDataDate between '" + tStartDate +
                         						"' and '" + tEndDate + "'");
                     }  ;
                     if(cactugetno==null||cactugetno.equals("")){
                         LRAccountsDB tLRAccountsDB=new LRAccountsDB(con);
                         tLRAccountsDB.setActuGetNo(tActugetNo);
                         tLRAccountsDB.setFeeNo("0");
                         tLRAccountsDB.setRecontCode(eSSRS.GetText(index,1));
                         String continfo = "select recontname,Recomcode from lrcontinfo where  recontcode='"+eSSRS.GetText(index,1)+"'"+
                                           " union all " +
                                           "select TempContName,ComCode from LRTempCessCont where tempcontcode='"+eSSRS.GetText(index,1)+"' with ur";
                         SSRS cfSSRS = new ExeSQL().execSQL(continfo);
                         if(cfSSRS.getMaxRow()>0){
                             tLRAccountsDB.setRecontName(cfSSRS.GetText(1,1));
                             tLRAccountsDB.setReComCode(cfSSRS.GetText(1,2));
                             String strSQL = "select recomname from lrrecominfo where recomcode='"+cfSSRS.GetText(1,2)+"' with ur";
                             String comName = new ExeSQL().getOneValue(strSQL);
                             tLRAccountsDB.setReComName(comName);
                         }
                         tLRAccountsDB.setStartDate(tStartDate);
                         tLRAccountsDB.setEndData(tEndDate);
                         tLRAccountsDB.setPayFlag("00");
                         tLRAccountsDB.setStandby3("0");
                         tLRAccountsDB.setCessPrem(0);
                         tLRAccountsDB.setReProcFee(0);
                         tLRAccountsDB.setClaimBackFee(0);
                         tLRAccountsDB.setMakeDate(PubFun.getCurrentDate());
                         tLRAccountsDB.setMakeTime(PubFun.getCurrentTime());
                         tLRAccountsDB.setModifyDate(PubFun.getCurrentDate());
                         tLRAccountsDB.setModifyTime(PubFun.getCurrentTime());
                         tLRAccountsDB.setOperator("server");
                         tLRAccountsDB.insert();
                     }
                 }
                  //con.commit();
                  con.close();
             }catch(Exception e){
                try{
                    con.close();
                }catch(Exception c){}
             }
          }
           System.out.println("账单批处理保全数据处理完毕");
                //处理理赔
           String clmsql="select distinct recontcode from lrpolclm where getdatadate between '"+tStartDate+"' and '"+tEndDate+"' with ur" ;
           SSRS clmSSRS = new ExeSQL().execSQL(clmsql);
           if(clmSSRS==null || clmSSRS.getMaxRow()<=0){
               buildError("DealData","没有查询到理赔摊回所涉及的合同信息！");
           }else{
               try{
                   //con.setAutoCommit(false);
                   for(int index=1;index<=clmSSRS.getMaxRow();index++){
                     String cSql = "select actugetno from LRAccounts where recontcode='"+clmSSRS.GetText(index, 1)+"' and startdate='"+tStartDate+"' and enddata='"+tEndDate+"' with ur";//判断该合同该时间段的账单是否有记录
                     String cactugetno = new ExeSQL().getOneValue(cSql);
                     String tActugetNo = "";
                     if(cactugetno==null||cactugetno.equals("")){
                          tActugetNo=PubFun1.CreateMaxNo("ReActugetNo", 20);
                      }else{
                          tActugetNo = cactugetno;
                      }
                    String  clmSql ="select count(1) from LRPolClm where (ActuGetState is null or trim(ActuGetState)='') and (ActuGetNo is null or trim(ActuGetNo)='') and " +
                            "recontcode='" + clmSSRS.GetText(index, 1) +
                            "' and GetDataDate between '"+ tStartDate +"' and '"+ tEndDate +"' " +
                            "with ur";
//                      LRPolClmSet tLRPolClmSet = new LRPolClmSet();
//                      RSWrapper rswrapper = new RSWrapper();
//                      rswrapper.prepareData(tLRPolClmSet, clmSql);
                    	String lrcscount = new ExeSQL().getOneValue(clmSql);
                    	int lrccount = Integer.parseInt(lrcscount); 
                      if(lrccount > 0){
                        //rswrapper.getData();
//                         for (int i = 1; i <= tLRPolClmSet.size(); i++) {
//                           LRPolClmDB tLRPolClmDB = new LRPolClmDB(con);
//                           tLRPolClmDB.setSchema(tLRPolClmSet.get(i));
//                           tLRPolClmDB.setActuGetNo(tActugetNo);
//                           tLRPolClmDB.setActuGetState("00");
//                           tLRPolClmDB.setModifyDate(tCurrentDate);
//                           tLRPolClmDB.setModifyTime(PubFun.getCurrentTime());
//                           tLRPolClmDB.update();
//                       }
                        ExeSQL tExeSQL = new ExeSQL();
                        tExeSQL.execUpdateSQL("update LRPolClm set ActuGetNo='"+tActugetNo+"',ModifyDate='"+tCurrentDate+"'," +
         										"ModifyTime='"+PubFun.getCurrentTime()+"',ActuGetState='00' where " +
//         										"(ActuGetState is null or trim(ActuGetState)='') and " +
//         										"(ActuGetNo is null or trim(ActuGetNo)='') and " +
         										"recontcode='" + clmSSRS.GetText(index, 1) +
         										"' and GetDataDate between '"+ tStartDate +"' and '"+ tEndDate +"' ");
                   }  ;
                   if(cactugetno==null||cactugetno.equals("")){
                       LRAccountsDB tLRAccountsDB = new LRAccountsDB(con);
                       tLRAccountsDB.setActuGetNo(tActugetNo);
                       tLRAccountsDB.setFeeNo("0");
                       tLRAccountsDB.setRecontCode(clmSSRS.GetText(index, 1));
                       String continfo = "select recontname,Recomcode from lrcontinfo where  recontcode='"+clmSSRS.GetText(index,1)+"'"+
                                           " union all " +
                                           "select TempContName,ComCode from LRTempCessCont where tempcontcode='"+clmSSRS.GetText(index,1)+"' with ur";
                       SSRS cfSSRS = new ExeSQL().execSQL(continfo);
                       if(cfSSRS.getMaxRow()>0){
                             tLRAccountsDB.setRecontName(cfSSRS.GetText(1,1));
                             tLRAccountsDB.setReComCode(cfSSRS.GetText(1,2));
                             String strSQL =
                               "select recomname from lrrecominfo where recomcode='" +
                               cfSSRS.GetText(1, 2) + "' with ur";
                             String comName = new ExeSQL().getOneValue(strSQL);
                             tLRAccountsDB.setReComName(comName);
                       }
                       tLRAccountsDB.setStartDate(tStartDate);
                       tLRAccountsDB.setEndData(this.getLastDate(new Date()));
                       tLRAccountsDB.setPayFlag("00");
                       tLRAccountsDB.setStandby3("0");
                       tLRAccountsDB.setCessPrem(0);
                       tLRAccountsDB.setReProcFee(0);
                       tLRAccountsDB.setClaimBackFee(0);
                       tLRAccountsDB.setMakeDate(PubFun.getCurrentDate());
                       tLRAccountsDB.setMakeTime(PubFun.getCurrentTime());
                       tLRAccountsDB.setModifyDate(PubFun.getCurrentDate());
                       tLRAccountsDB.setModifyTime(PubFun.getCurrentTime());
                       tLRAccountsDB.setOperator("server");
                       tLRAccountsDB.insert();
                   }
               }
               //con.commit();
               con.close();
           }catch(Exception e){
              try{
                  con.close();
              }catch(Exception c){}
           }
        }
        System.out.println("账单批处理理赔数据处理完毕");
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        this.mYear = (String) mTransferData.getValueByName("Year");
        this.mMonth = (String) mTransferData.getValueByName("Month");
        return true;
    }

    /*
     *
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReCountMonthTask";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public  String getLastMonth(Date date){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        int MONTH=calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH,MONTH-1);

        String tDate =null;
        try {
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            tDate = sdf.format(calendar.getTime());
            return tDate;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    public  String getLastDate(Date date){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        int DATE=calendar.get(Calendar.DATE);
        calendar.set(Calendar.DATE,DATE-1);

        String tDate =null;
        try {
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            tDate = sdf.format(calendar.getTime());
            return tDate;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
