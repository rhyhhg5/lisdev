/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import java.util.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: huxl
 * @CreateDate：2006-11-01
 */
public class LRGetEdorDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
    private String mEndDate = "";

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    private String mCurrentDate = new PubFun().getCurrentDate();

    private String mCurrentTime = new PubFun().getCurrentTime();

    //业务处理相关变量
    /** 全局数据 */

    public LRGetEdorDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL->Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取保全数据
        if (this.strOperate.equals("EDORDATA")) {
            if (!getEdorData()) {
                buildError("insertData", "单证管理员信息有误!");
                return false;
            }
        }
        return true;
    }

    /**
     * 提取保全数据
     * @return boolean
     */
    private boolean insertEdorData() {
        System.out.println("come in insertEdorData()..");
        //提取再保保全数据
        String tSql =
                "select A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S from ( " +
                "select a.GrpContNo A,a.GrpPolNo B,a.ContNo C,a.PolNo D,a.EdorNo E,a.MainPolNo F,a.MasterPolNo G,a.RiskCode H, " +
                " a.RiskVersion I,(select max(EdorValidate) from LPEdorItem where EdorNo " +
                " = b.EndorsementNo and ContNo = a.ContNo)  J " +
                " ,a.PayToDate K,b.FeeOperationType L,b.mon M,b.makedate N,a.Amnt O,a.ManageCom P ,a.ReNewCount Q ,a.agentGroup R,a.enddate S" +  //080714新增成本中心字段
                " from lbpol a ,(select PolNo ,sum(GetMoney) mon,EndorsementNo ,FeeOperationType,MakeDate " +
                " from LJAGetEndorse c where FeeOperationType = 'WT' and FeeFinaType <> 'GB' " +
                " and exists (select 1 from LRPol where polno = c.polno ) group by PolNo,EndorsementNo,FeeOperationType,MakeDate) b " +
                " where a.polno = b.polno  and a.ContType = '1' " +
                " and a.edorno = b.EndorsementNo " +
                ") as X where (J >= N and J= '" + this.mToday +
                "') or (J < N and N = '" + this.mToday + "') " + //个单犹豫期退保
                " union all " +
                "select A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S from ( " +
                "select a.GrpContNo A,a.GrpPolNo B,a.ContNo C,a.PolNo D,a.EdorNo E,a.MainPolNo F,a.MasterPolNo G,a.RiskCode H, " +
                " a.RiskVersion I,(select max(EdorValidate) from LPGrpEdorItem where EdorNo " +
                " = b.EndorsementNo and GrpContNo = a.GrpContNo and edortype = 'WT')  J " +
                " ,a.PayToDate K,b.FeeOperationType L,-a.Prem M,b.makedate N,a.Amnt O,a.ManageCom P,a.ReNewCount Q ,a.agentGroup R,a.enddate S " +
                " from lbpol a ,(select GrpContNo ,sum(GetMoney) mon,EndorsementNo ,FeeOperationType,MakeDate " +
                " from LJAGetEndorse c where FeeOperationType = 'WT' and FeeFinaType <> 'GB' " +
                " and exists (select 1 from LRPol where polno = c.polno ) group by GrpContNo,EndorsementNo,FeeOperationType,MakeDate) b " +
                " where a.GrpContNo = b.GrpContNo  and a.ContType = '2' " +
                " and a.edorno = b.EndorsementNo  " +
                ") as X where (J >= N and J= '" + this.mToday +
                "') or (J < N and N = '" + this.mToday + "') " + //团单犹豫期退保
                " union all " +
                "select A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S from ( " +
                "select a.GrpContNo A,a.GrpPolNo B,a.ContNo C,a.PolNo D,a.EdorNo E,a.MainPolNo F,a.MasterPolNo G,a.RiskCode H, " +
                " a.RiskVersion I,(select max(EdorValidate) from LPEdorItem where EdorNo " +
                " = b.EndorsementNo and ContNo = a.ContNo )  J " +
                " ,a.PayToDate K,b.FeeOperationType L,b.mon M,b.makedate N,a.Amnt O,a.ManageCom P ,a.ReNewCount Q ,a.agentGroup R,a.enddate S" +
                " from lbpol a ,(select PolNo ,sum(GetMoney) mon,EndorsementNo ,FeeOperationType,MakeDate " +
                " from LJAGetEndorse c where FeeOperationType in ('ZT','CT') " +
                " and exists (select 1 from LRPol where polno = c.polno ) group by PolNo,EndorsementNo,FeeOperationType,MakeDate) b " +
                " where a.polno = b.polno " +
                " and a.edorno = b.EndorsementNo " +
                ") as X where (J >= N and J= '" + this.mToday +
                "') or (J < N and N = '" + this.mToday + "') "+      //减人,解约
                " union all "+
                "select A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S from ( " +
                "select a.GrpContNo A,a.GrpPolNo B,a.ContNo C,a.PolNo D,a.EdorNo E,a.MainPolNo F,a.MasterPolNo G,a.RiskCode H, " +
                " a.RiskVersion I,(select max(EdorValidate) from LPEdorItem where EdorNo " +
                " = b.EndorsementNo and ContNo = a.ContNo)  J " +
                " ,a.PayToDate K,b.FeeOperationType L,b.mon M,b.makedate N,a.Amnt O,a.ManageCom P ,a.ReNewCount Q ,a.agentGroup R,a.enddate S" +  //080714新增成本中心字段
                " from lbpol a ,(select PolNo ,sum(GetMoney) mon,EndorsementNo ,FeeOperationType,MakeDate " +
                " from LJAGetEndorse c where FeeOperationType in ('XT','RB') and FeeFinaType <> 'GB' " +
                " and exists (select 1 from LRPol where polno = c.polno ) and not exists (select 1 from lrpoledor where polno=c.polno)group by PolNo,EndorsementNo,FeeOperationType,MakeDate) b " +
                " where a.polno = b.polno  and a.ContType = '1' " +
                " and a.edorno = b.EndorsementNo " +
                ") as X where (J >= N and J= '" + this.mToday +"') or (J < N and N = '" + this.mToday + "') " + //个单协议退保
                " union all " +
                "select A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S from ( " +
                "select a.GrpContNo A,a.GrpPolNo B,a.ContNo C,a.PolNo D,a.EdorNo E,a.MainPolNo F,a.MasterPolNo G,a.RiskCode H, " +
                " a.RiskVersion I,(select max(EdorValidate) from LPGrpEdorItem where EdorNo " +
                " = b.EndorsementNo and GrpContNo = a.GrpContNo and edortype in ('XT','RB') )  J " +
                " ,a.PayToDate K,b.FeeOperationType L,-a.Prem M,b.makedate N,a.Amnt O,a.ManageCom P,a.ReNewCount Q ,a.agentGroup R,a.enddate S " +
                " from lbpol a ,(select GrpContNo ,sum(GetMoney) mon,EndorsementNo ,FeeOperationType,MakeDate " +
                " from LJAGetEndorse c where FeeOperationType in ('XT','RB') and FeeFinaType <> 'GB' " +
                " and exists (select 1 from LRPol where polno = c.polno ) group by GrpContNo,EndorsementNo,FeeOperationType,MakeDate) b " +
                " where a.GrpContNo = b.GrpContNo  and a.ContType = '2' " +
                " and a.edorno = b.EndorsementNo  " +
                ") as X where (J >= N and J= '" + this.mToday +"') or (J < N and N = '" + this.mToday + "') " + //团单协议退保
                " with ur ";
        int start = 1;
        int nCount = 5000;
        while (true) {
            SSRS tSSRS = new ExeSQL().execSQL(tSql, start, nCount);
            if (tSSRS.getMaxRow() <= 0) {
                break;
            }
            mMap = new MMap();
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                    LRPolEdorSchema tLRPolEdorSchema = new LRPolEdorSchema();
                    //添加其他数据
                    tLRPolEdorSchema.setGrpContNo(tSSRS.GetText(i, 1));
                    tLRPolEdorSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                    tLRPolEdorSchema.setContNo(tSSRS.GetText(i, 3));
                    tLRPolEdorSchema.setPolNo(tSSRS.GetText(i, 4));
                    tLRPolEdorSchema.setEdorNo(tSSRS.GetText(i, 5));
                    tLRPolEdorSchema.setMainPolNo(tSSRS.GetText(i, 6));
                    tLRPolEdorSchema.setMasterPolNo(tSSRS.GetText(i, 7));
                    tLRPolEdorSchema.setRiskCode(tSSRS.GetText(i, 8));
                    tLRPolEdorSchema.setRiskVersion(tSSRS.GetText(i, 9));
                    tLRPolEdorSchema.setEdorValidate(tSSRS.GetText(i, 10));
                    int noPassDays ; //未经过天数
                    String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tSSRS.GetText(i, 8)+"' with ur";
                    String LRiskFlag = new ExeSQL().getOneValue(aSql);
                    if(Integer.parseInt(LRiskFlag)>0){ //09.061.7长险趸交的未经过天数有误,此处改用以前的处理方法，除365取余
                         noPassDays =PubFun.calInterval(tSSRS.GetText(i,10),tSSRS.GetText(i,11),"D")%365;
                    }else{
                         noPassDays =PubFun.calInterval(tSSRS.GetText(i,10),tSSRS.GetText(i,19),"D");
                    }
                    if (noPassDays <= 0) {
                        noPassDays = 0; //保全摊回的时候，失效日期可以大于缴至日期,此时摊回零.
                    }
                    tLRPolEdorSchema.setNoPassDays(noPassDays);
                    tLRPolEdorSchema.setFeeoperationType(tSSRS.GetText(i, 12));
                    tLRPolEdorSchema.setGetMoney(tSSRS.GetText(i, 13));
                    tLRPolEdorSchema.setGetConfDate(tSSRS.GetText(i, 14));
                    tLRPolEdorSchema.setSumBackAmnt(tSSRS.GetText(i, 15));
                    tLRPolEdorSchema.setManageCom(tSSRS.GetText(i, 16));
                    tLRPolEdorSchema.setGetDataDate(this.mToday); //数据提取日期
                    tLRPolEdorSchema.setOperator(globalInput.Operator);
                    tLRPolEdorSchema.setMakeDate(this.mCurrentDate);
                    tLRPolEdorSchema.setMakeTime(this.mCurrentTime);
                    tLRPolEdorSchema.setModifyDate(this.mCurrentDate);
                    tLRPolEdorSchema.setModifyTime(this.mCurrentTime);
                    tLRPolEdorSchema.setReNewCount(tSSRS.GetText(i, 17)); //新增续保次数
                     //新增成本中心
                    String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"+
                                 tSSRS.GetText(i,18)+"') with ur";
                    String CostCenter = new ExeSQL().getOneValue(sql);
                    tLRPolEdorSchema.setCostCenter(CostCenter);

                    tSql =
                            "select a.ReContCode,a.ReinsureItem,a.RiskCalSort,(select ReComCode from LRContInfo where ReContCode = " +
                            "a.ReContCode) from LRPol a,LBPol b where a.PolNo = b.PolNO and a.ReNewCount = b.ReNewCount and a.PolNo = '" +
                            tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount = b.ReNewCount) ";
                    SSRS cSSRS = new ExeSQL().execSQL(tSql);
                    //处理一个险种对应多个再保合同
                    for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                        tLRPolEdorSchema.setReContCode(cSSRS.GetText(j, 1)); //新增字段
                        tLRPolEdorSchema.setReinsureItem(cSSRS.GetText(j, 2));
                        tLRPolEdorSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                        tLRPolEdorSchema.setReComCode(cSSRS.GetText(j, 4));
                        mMap.put(tLRPolEdorSchema.getSchema(), "DELETE&INSERT");
                    }
                }
            }
            if (!prepareOutputData()) {
            }
            //如果提交不成功,不能返回
            tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(this.mInputData, "")) {
                if (tPubSubmit.mErrors.needDealError()) {
                    buildError("insertData", "提取再保保全数据出错!");
                }
            }
            start += nCount;
        }
        return true;
    }

    /**
        * insertEdorData2
        * //处理续保回退的保单
        * @return boolean
        */
       private boolean insertEdorData2() {
           System.out.println("come in insertEdorData2()..");
          String tSql =          //续保回退保单提数
                  "select a.GrpContNo A, a.GrpPolNo B,a.ContNo C,a.PolNo D, max(b.NewPolNo),a.MainPolNo F,a.MasterPolNo G, " +
                  "a.RiskCode H,a.RiskVersion I,a.enddate,a.PayToDate K, 'XB', -a.sumprem, max(b.modifydate), " +
                  "a.Amnt O, a.ManageCom P, b.ReNewCount Q, a.agentGroup R, a.cvalidate S " +
                  "from lcpol a,lbrnewstatelog b,lrpol c where a.polno = b.polno and a.renewcount<c.renewcount and a.ContType = '1'"+
                  " and  c.polno = a.polno and c.renewcount=b.renewcount and c.renewcount=(select max(renewcount) from lrpol c where c.polno=b.polno) " +
                  " and not exists (select 1 from lbpol d where d.polno=c.polno and d.renewcount=c.renewcount) "+
                  " and b.modifydate ='"+  this.mToday +"'  " +   //以修改日期为提数时间
                  "group by a.GrpContNo, a.GrpPolNo,a.ContNo,a.PolNo,a.MainPolNo,a.MasterPolNo, a.RiskCode,a.RiskVersion,a.enddate,a.PayToDate, a.sumprem, a.Amnt, a.ManageCom, b.ReNewCount, a.agentGroup, a.cvalidate"+
                  " with ur ";
          int start = 1;
          int nCount = 5000;
          while (true) {
              SSRS tSSRS = new ExeSQL().execSQL(tSql, start, nCount);
              if (tSSRS.getMaxRow() <= 0) {
                  break;
              }
              mMap = new MMap();
              if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                  for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                      LRPolEdorSchema tLRPolEdorSchema = new LRPolEdorSchema();
                      //添加其他数据
                      tLRPolEdorSchema.setGrpContNo(tSSRS.GetText(i, 1));
                      tLRPolEdorSchema.setGrpPolNo(tSSRS.GetText(i, 2));
                      tLRPolEdorSchema.setContNo(tSSRS.GetText(i, 3));
                      tLRPolEdorSchema.setPolNo(tSSRS.GetText(i, 4));
                      tLRPolEdorSchema.setEdorNo(tSSRS.GetText(i, 5));
                      tLRPolEdorSchema.setMainPolNo(tSSRS.GetText(i, 6));
                      tLRPolEdorSchema.setMasterPolNo(tSSRS.GetText(i, 7));
                      tLRPolEdorSchema.setRiskCode(tSSRS.GetText(i, 8));
                      tLRPolEdorSchema.setRiskVersion(tSSRS.GetText(i, 9));
                      tLRPolEdorSchema.setEdorValidate(tSSRS.GetText(i, 10));
                      int noPassDays ; //未经过天数
                      noPassDays =PubFun.calInterval(tSSRS.GetText(i,19),tSSRS.GetText(i,10),"D");
                      if (noPassDays <= 0) {
                          noPassDays = 0; //保全摊回的时候，失效日期可以大于缴至日期,此时摊回零.
                      }
                      tLRPolEdorSchema.setNoPassDays(noPassDays);
                      tLRPolEdorSchema.setFeeoperationType(tSSRS.GetText(i, 12));
                      tLRPolEdorSchema.setGetMoney(tSSRS.GetText(i, 13));
                      tLRPolEdorSchema.setGetConfDate(tSSRS.GetText(i, 14));
                      tLRPolEdorSchema.setSumBackAmnt(tSSRS.GetText(i, 15));
                      tLRPolEdorSchema.setManageCom(tSSRS.GetText(i, 16));
                      tLRPolEdorSchema.setGetDataDate(this.mToday); //数据提取日期
                      tLRPolEdorSchema.setOperator(globalInput.Operator);
                      tLRPolEdorSchema.setMakeDate(this.mCurrentDate);
                      tLRPolEdorSchema.setMakeTime(this.mCurrentTime);
                      tLRPolEdorSchema.setModifyDate(this.mCurrentDate);
                      tLRPolEdorSchema.setModifyTime(this.mCurrentTime);
                      tLRPolEdorSchema.setReNewCount(tSSRS.GetText(i, 17)); //新增续保次数
                       //新增成本中心
                      String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"+
                                   tSSRS.GetText(i,18)+"') with ur";
                      String CostCenter = new ExeSQL().getOneValue(sql);
                      tLRPolEdorSchema.setCostCenter(CostCenter);

                      tSql =
                              "select a.ReContCode,a.ReinsureItem,a.RiskCalSort,(select ReComCode from LRContInfo where ReContCode = " +
                              "a.ReContCode) from LRPol a,LCPol b where a.PolNo = b.PolNO and a.ReNewCount = "+tSSRS.GetText(i, 17)+" and a.PolNo = '" +
                              tSSRS.GetText(i, 4) + "' and a.GetDataDate = (select max(GetDataDate) from LRPOL where PolNo = b.PolNO and ReNewCount = "+tSSRS.GetText(i, 17)+") ";
                      SSRS cSSRS = new ExeSQL().execSQL(tSql);
                      //处理一个险种对应多个再保合同
                      for (int j = 1; j <= cSSRS.getMaxRow(); j++) {
                          tLRPolEdorSchema.setReContCode(cSSRS.GetText(j, 1)); //新增字段
                          tLRPolEdorSchema.setReinsureItem(cSSRS.GetText(j, 2));
                          tLRPolEdorSchema.setRiskCalSort(cSSRS.GetText(j, 3));
                          tLRPolEdorSchema.setReComCode(cSSRS.GetText(j, 4));
                          mMap.put(tLRPolEdorSchema.getSchema(), "DELETE&INSERT");
                          String stru = " update lrpol set polno='"+tLRPolEdorSchema.getEdorNo()+"',modifydate=current date,modifytime=current time where "+
                                        " PolNo='"+tLRPolEdorSchema.getPolNo()+"' and ReContCode='"+tLRPolEdorSchema.getReContCode()+
                                        "' and ReinsureItem='"+tLRPolEdorSchema.getReinsureItem()+"' and ReNewCount= "+tLRPolEdorSchema.getReNewCount();
                          mMap.put(stru, "UPDATE");
                      }
                  }
              }
              if (!prepareOutputData()) {
              }
              //如果提交不成功,不能返回
              tPubSubmit = new PubSubmit();
              if (!tPubSubmit.submitData(this.mInputData, "")) {
                  if (tPubSubmit.mErrors.needDealError()) {
                      buildError("insertData", "提取再保保全数据出错!");
                  }
              }
              start += nCount;
          }

           return true;
    }
    /**
     * 提取保全数据
     * @return boolean
     */
    private boolean getEdorData() {
        mStartDate = (String) mGetCessData.getValueByName("StartDate");
        mEndDate = (String) mGetCessData.getValueByName("EndDate");

        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(mEndDate); //录入的终止日期，用FDate处理

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            this.mToday = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!insertEdorData()) {
                buildError("insertEdorData", "提取" + mToday + "号保全数据出错");
                return false;
            }
            if(!insertEdorData2()){
                buildError("insertEdorData2", "提取" + mToday + "号保全数据出错");
                return false;
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }



    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetEdorDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "86";
        globalInput.Operator = "rm0001";

        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRGetEdorDataBL tLRGetEdorDataBL = new LRGetEdorDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2009-9-1");
            tTransferData.setNameAndValue("EndDate", "2009-9-27");
            System.out.println("in main: " +
                               (String) tTransferData.
                               getValueByName("StartDate") + "   " +
                               (String) tTransferData.getValueByName("EndDate"));
            vData.add(tTransferData);
            tLRGetEdorDataBL.submitData(vData, "EDORDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
