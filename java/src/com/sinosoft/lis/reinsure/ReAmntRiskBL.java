/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Zhang Bin
 * @CreateDate：2006-07-30
 */

package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

public class ReAmntRiskBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String strOperate;
    private LRAmntRelRiskSet mLRAmntRelRiskSet = new LRAmntRelRiskSet();
    private LRContInfoSchema mLRContInfoSchema = new LRContInfoSchema();

    private MMap mMap = new MMap();

    private PubSubmit tPubSubmit = new PubSubmit();


    //业务处理相关变量
    /** 全局数据 */

    public ReAmntRiskBL() {
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        System.out.println("before getInputData()........");
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("after getInputData()........");

        if (!dealData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        ReAmntRiskBL tReAmntRiskBL = new ReAmntRiskBL();

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();

        try {
            vData.add(globalInput);
            tReAmntRiskBL.submitData(vData, "INSERT");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //进行插入数据
        if (this.strOperate.equals("INSERT")) {
            if (!insertData()) {
                // @@错误处理
                buildError("insertData", "增加单证管理员时出现错误!");
                return false;
            }
        }
        //对数据进行修改操作
        if (this.strOperate.equals("UPDATE")) {
            if (!updateData()) {
                // @@错误处理
                buildError("updateData", "单证管理员信息有误!");
                return false;
            }
        }
        //对数据进行删除操作
        if (this.strOperate.equals("DELETE")) {
            if (!deleteData()) {
                // @@错误处理
                buildError("deleteData", "删除单证管理员时出现错误!");
                return false;
            }
        }
        return true;
    }

    /**
     * deleteData
     *
     * @return boolean
     */
    private boolean deleteData() {
        System.out.println("come in delete........");
        LRAmntRelRiskSet tLRAmntRelRiskSet = new LRAmntRelRiskSet();
        LRAmntRelRiskDB tLRAmntRelRiskDB = new LRAmntRelRiskDB();

        tLRAmntRelRiskDB.setReContCode(mLRContInfoSchema.getReContCode());

        tLRAmntRelRiskSet = tLRAmntRelRiskDB.query();
        if (tLRAmntRelRiskSet.size() == 0) {
            buildError("insertData", "没有该合同的累计险种信息!");
            return false;
        }

        mMap.put(tLRAmntRelRiskSet, "DELETE");

        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("updateData", "修改时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;
        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData() {
        System.out.println("come in delete........");
        LRAmntRelRiskSet tLRAmntRelRiskSet = new LRAmntRelRiskSet();
        LRAmntRelRiskDB tLRAmntRelRiskDB = new LRAmntRelRiskDB();

        tLRAmntRelRiskDB.setReContCode(mLRContInfoSchema.getReContCode());

        tLRAmntRelRiskSet = tLRAmntRelRiskDB.query();
        if (tLRAmntRelRiskSet.size() == 0) {
            buildError("insertData", "没有该合同的累计险种信息!");
            return false;
        }

        tLRAmntRelRiskSet.clear();

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        for (int i = 0; i < mLRAmntRelRiskSet.size(); i++) {
            LRAmntRelRiskSchema tLRAmntRelRiskSchema = new LRAmntRelRiskSchema();
            tLRAmntRelRiskSchema = mLRAmntRelRiskSet.get(i + 1);

            tLRAmntRelRiskSchema.setManageCom(globalInput.ComCode);
            tLRAmntRelRiskSchema.setOperator(globalInput.Operator);
            tLRAmntRelRiskSchema.setMakeDate(currentDate);
            tLRAmntRelRiskSchema.setMakeTime(currentTime);
            tLRAmntRelRiskSchema.setModifyDate(currentDate);
            tLRAmntRelRiskSchema.setModifyTime(currentTime);

            tLRAmntRelRiskSet.add(tLRAmntRelRiskSchema);
        }

        String strSQL = "delete from LRAmntRelRisk where RecontCode='" +
                        mLRContInfoSchema.getReContCode() + "'"
                        ;
        mMap.put(strSQL, "DELETE");
        mMap.put(tLRAmntRelRiskSet, "INSERT");

        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "保存再保公司信息时时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData() {
        System.out.println("come in insert........");
        LRAmntRelRiskSet tLRAmntRelRiskSet = new LRAmntRelRiskSet();
        LRAmntRelRiskDB tLRAmntRelRiskDB = new LRAmntRelRiskDB();

        tLRAmntRelRiskDB.setReContCode(mLRContInfoSchema.getReContCode());

        tLRAmntRelRiskSet = tLRAmntRelRiskDB.query();
        if (tLRAmntRelRiskSet.size() != 0) {
            buildError("insertData", "已经添加了该险种的累计险种信息!");
            return false;
        }

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        for (int i = 0; i < mLRAmntRelRiskSet.size(); i++) {
            LRAmntRelRiskSchema tLRAmntRelRiskSchema = new LRAmntRelRiskSchema();
            tLRAmntRelRiskSchema = mLRAmntRelRiskSet.get(i + 1);

            tLRAmntRelRiskSchema.setManageCom(globalInput.ComCode);
            tLRAmntRelRiskSchema.setOperator(globalInput.Operator);
            tLRAmntRelRiskSchema.setMakeDate(currentDate);
            tLRAmntRelRiskSchema.setMakeTime(currentTime);
            tLRAmntRelRiskSchema.setModifyDate(currentDate);
            tLRAmntRelRiskSchema.setModifyTime(currentTime);

        }
        mMap.put(mLRAmntRelRiskSet, "INSERT");

        if (!prepareOutputData()) {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "保存再保公司信息时时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLRAmntRelRiskSet.set((LRAmntRelRiskSet) cInputData.
                                   getObjectByObjectName("LRAmntRelRiskSet", 0));
        this.mLRContInfoSchema.setSchema((LRContInfoSchema) cInputData.
                                         getObjectByObjectName(
                "LRContInfoSchema", 0));
        return true;
    }

    public String getResult() {
        return mLRContInfoSchema.getReContCode();
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
