package com.sinosoft.lis.reinsure;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.sinosoft.lis.bq.PolicyAbateDealBL;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LRContInfoDB;
import com.sinosoft.lis.db.LRInterfaceDB;
import com.sinosoft.lis.db.LRPolCessAmntDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LRContInfoSchema;
import com.sinosoft.lis.schema.LRInterfaceSchema;
import com.sinosoft.lis.schema.LRPolCessAmntSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LRInterfaceSet;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.lis.db.LRTempCessContDB;
import com.sinosoft.lis.schema.LRTempCessContSchema;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.db.LRDutyDB;
import com.sinosoft.lis.vschema.LRDutySet;
/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 * 再保计算公共函数
 * <p>Copyright: Copyright (c) 2005</p>
 * modify by 2009/2/5
 * <p>Company: </p>
 * sinosoft
 * @author not attributable
 * @version 1.0
 */
public class DealRiskAmntBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private Connection mCon;
    private String mSerialno;
    private String mCurrentDate;  //保存计算所需要的日期
    public DealRiskAmntBL (Connection con,String Serialno,String tCurrentDate){
        mCon=con;
        mSerialno=Serialno;
        mCurrentDate=tCurrentDate;
    }
    public DealRiskAmntBL (String tCurrentDate){
        mCurrentDate=tCurrentDate;
    }
    public DealRiskAmntBL(){
    }

    public boolean deal(){
        if(mCon==null || StrTool.cTrim(mSerialno).equals("") || StrTool.cTrim(mCurrentDate).equals("")){
            return false;
        }
        try{
            if(signCessFlag()==false){
                return false;
            }
        }catch(Exception e ){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 算累计分保保费
     * @return boolean
     */
    private boolean signCessFlag() {
        System.out.println("come in signCessFlag()..");
        String sql="select polno,renewcount,polyear,dutydate from lrinterface where dutydate='"+this.mCurrentDate+"' and polstat='0' with ur";
        System.out.println(sql);
        try{
            Statement stmt = mCon.createStatement();
            ResultSet tRs = stmt.executeQuery(sql);
            dealData(tRs);
            stmt.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return true;
    }
    public static void main(String[] args){
        Connection con=null;
        con=DBConnPool.getConnection();
        try{
            con.setAutoCommit(false); 
            DealRiskAmntBL tDealRiskAmntBL=new DealRiskAmntBL(con,"000000000000077","2008-03-06");
            if(tDealRiskAmntBL.deal()==false){
            }
            con.close();
        }catch(Exception e){}
    }



    public boolean dealData(ResultSet rs){
        try {
            if(rs==null) return false;
            System.out.println("begin to recont.....");
            while(rs.next()) {
                LRInterfaceDB tLRInterfaceDB=new LRInterfaceDB();
                tLRInterfaceDB.setPolNo(rs.getString(1));
                tLRInterfaceDB.setReNewCount(rs.getInt(2));
                tLRInterfaceDB.setPolYear(rs.getInt(3));
                tLRInterfaceDB.setDutyDate(rs.getString(4));
                LRInterfaceSet tLRInterfaceSet=tLRInterfaceDB.query();
                if(tLRInterfaceSet==null || tLRInterfaceSet.size()==0) continue;
                LRInterfaceSchema tLRInterfaceSchema=tLRInterfaceSet.get(1);
                //mCurrentDate=tLRInterfaceSchema.getDutyDate();//lh
                sumCessAmntByRiskCode(tLRInterfaceSchema);//按险种
                sumCessAmntByDuty(tLRInterfaceSchema);//按责任
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
}
    /**
     * 计算累计分保保额，用于批处理
     * add by zhangbin
     */
    public double sumCessAmnt(LRInterfaceSchema aLRInterfaceSchema, String aRecontCode) {
        System.out.println("开始计算累计分保保额......");
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();
        LRInterfaceSchema tLRInterfaceSchema = aLRInterfaceSchema;
        double sumCessAmnt = 0;

        //得到累计计算保额分组名
        ExeSQL exesql = new ExeSQL();
        String strSQL = "select distinct GroupName from LRAmntRelRisk where "
                        + " Recontcode='" + aRecontCode + "' and RiskCode='" +
                        tLRInterfaceSchema.getRiskCode() + "' with ur";
        SSRS COUNT = exesql.execSQL(strSQL);
        //得到该组中的所有险种
        if (COUNT == null || COUNT.getMaxRow() <= 0) { //如果没有GroupName
            return 0;
        }
        String tempdata1[][];
        tempdata1 = COUNT.getAllData(); //得到GroupName
        String groupName = tempdata1[0][0];
        strSQL = "select RiskCode from LRAmntRelRisk where GroupName = '" +
                 groupName + "' and recontcode='" + aRecontCode + "' with ur"
                 ;
        COUNT = exesql.execSQL(strSQL);
        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return 0;
        }
        //得到被保险人所有累积险种的保单  !!不包括当前保单
        //这里还需要保证同一天的保单，应该分在较后的保单上，也就是保证统计先前的保单时，不统计比自己晚的。modified by huxl
        //09/9/9 Modify 在9月1号之后,从lrpolcessamnt表中查数据.9月1号之前的保单,在lcpol表中查数据
        strSQL = "select a.GrpContNo,a.ContNo,a.PolNo,b.CessAmnt from LCPol a,lrpolcessamnt b where a.polno=b.polno and a.renewcount=b.renewcount and b.paycount=(year(current date - a.CValidate) + 1) and a.InsuredNo='" +
        tLRInterfaceSchema.getInsuredNo() +
                 "' and a.signdate>='2009-1-1' and a.appflag = '1' and  a.RiskCode in ("   //09年9月份上线,签单在09年9月之后的保单从lrpolcessamnt中取分出保额
                 + " select RiskCode from LRAmntRelRisk where GroupName = '" +
                 groupName + "' and recontcode='" + aRecontCode +
                 "') and a.PolNo<>'" + tLRInterfaceSchema.getPolNo() +
                 "' and a.EndDate > '" + mCurrentDate + "' and((a.SignDate <'" +
                 mCurrentDate + "' and a.Cvalidate < '" + mCurrentDate +
                 "') or (a.SignDate <= a.Cvalidate and a.CvaliDate = '" +
                 mCurrentDate + "' and a.MakeTime <= '" +
                 tLRInterfaceSchema.getMakeTime() +
                 "') or (a.SignDate > a.Cvalidate and a.SignDate = '" +
                 mCurrentDate + "' and a.MakeTime <= '" +
                 tLRInterfaceSchema.getMakeTime() + "')) "+
                 " union "+
                 "select a.GrpContNo,a.ContNo,a.PolNo,a.CessAmnt from LCPol a where a.InsuredNo='" +
                   tLRInterfaceSchema.getInsuredNo() +
                 "'and a.signdate<'2009-1-1' and a.appflag = '1' and  a.RiskCode in ("//09年9月份上线,签单在09年9月之前的保单从lcpol中取分出保额
                 + " select RiskCode from LRAmntRelRisk where GroupName = '" +
                 groupName + "' and recontcode='" + aRecontCode +
                 "') and a.PolNo<>'" + tLRInterfaceSchema.getPolNo() +
                 "' and a.EndDate > '" + mCurrentDate + "' and((a.SignDate <'" +
                 mCurrentDate + "' and a.Cvalidate < '" + mCurrentDate +
                 "') or (a.SignDate <= a.Cvalidate and a.CvaliDate = '" +
                 mCurrentDate + "' and a.MakeTime <= '" +
                 tLRInterfaceSchema.getMakeTime() +
                 "') or (a.SignDate > a.Cvalidate and a.SignDate = '" +
                 mCurrentDate + "' and a.MakeTime <= '" +
                 tLRInterfaceSchema.getMakeTime() + "')) "+
                 "with ur ";
        System.out.println("cessAmtn:::"+strSQL);
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
//        LCPolDB aLCPolDB = new LCPolDB();
//        LCPolSet aLCPolSet = new LCPolSet();
//        aLCPolSet = aLCPolDB.executeQuery(strSQL); //得到该被保险人的所有险种保单
        if (tSSRS!=null && tSSRS.getMaxRow() > 0) {
            for (int i = 1; i <= tSSRS.MaxRow; i++) { //对该被保险人所有保单进行循环
                //判断保单是否失效
                if (tSSRS.GetText(i,1).equals(
                        "00000000000000000000")) { //如果是个单
                    if (tPolicyAbateDealBL.booPolState(tSSRS.GetText(i,3),
                            "Available", "1")) { //判断保单失效状态
                        continue;
                    }
                    System.out.println("个单未失效。。。。");
                    if (tPolicyAbateDealBL.booPolState(tSSRS.GetText(i,3),
                            "Terminate", "1")) { //判断保单终止状态
                        continue;
                    }
                    System.out.println("个单未终止。。。。");
                } else { //如果是团单
                    if (tPolicyAbateDealBL.booGrpPolicyState(tSSRS.GetText(i,1), "Pause", "1")) { //判断保单失效状态
                        continue;
                    }
                    if (tPolicyAbateDealBL.booGrpPolicyState(tSSRS.GetText(i,1), "Terminate", "1")) { //判断保单终止状态
                        continue;
                    }
                }
                System.out.println("得到分保保额 " + tSSRS.GetText(i,4));
                //得到累加分保保额,!!分保保额是历史数据
                sumCessAmnt = sumCessAmnt + Double.parseDouble(tSSRS.GetText(i,4));
            }
        }
        return sumCessAmnt;
    }

    /**
     * 计算累计保额，按保额方式计算，用于批处理
     * add by zhangbin
     */
    public double accAmnt(LRInterfaceSchema aLRInterfaceSchema, String recontCode) {
        LRInterfaceSchema tLRInterfaceSchema = aLRInterfaceSchema;

        //得到累计计算保额分组名,根据累计计算险种定义，此处每一个险种只能位于一个分组中
        String strSQL =
                "select GroupName from LRAmntRelRisk where recontcode = '" +
                recontCode + "' and RiskCode = '" + tLRInterfaceSchema.getRiskCode() +
                "'";
        SSRS COUNT = new ExeSQL().execSQL(strSQL);

        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return tLRInterfaceSchema.getAmnt(); //不需要累计计算
        }
        double accAmnt = 0;
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();

//        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
//        tLRContInfoDB.setReContCode(recontCode);
//        if (!tLRContInfoDB.getInfo()) {
//            System.out.println("没有此合同！");
//            return 0;
//        }
//        LRContInfoSchema tLRContInfoSchema = tLRContInfoDB.getSchema();
//
//        //如果是成数分保
//        if (tLRContInfoSchema.getCessionMode().equals("1")) {
//            return 0;
//        }

        strSQL = "select * from LCPol where InsuredNo = '" +
        tLRInterfaceSchema.getInsuredNo() +
                 "' and appflag = '1' and RiskCode in ( select RiskCode from LRAmntRelRisk where GroupName = '" +
                 COUNT.GetText(1, 1) + "' and recontcode='" + recontCode +
                 "') and EndDate > '" + mCurrentDate + "' and ((SignDate < '" +
                 mCurrentDate + "' and Cvalidate < '" + mCurrentDate +
                 "') or (SignDate <= Cvalidate and CvaliDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLRInterfaceSchema.getMakeTime() +
                 "') or (SignDate > Cvalidate and SignDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLRInterfaceSchema.getMakeTime() + "')) with ur ";
        System.out.println("accAmnt::"+strSQL);
        LCPolSet aLCPolSet = new LCPolDB().executeQuery(strSQL); //得到该被保险人的所有累计险种保单
        for (int i = 1; i <= aLCPolSet.size(); i++) { //对该被保险人所有保单进行循环
            if (aLCPolSet.get(i).getContType().equals("1")) { //计算个单累计风险保额
                if (!tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Available", "1") &&
                    !tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Terminate", "1")) {
                    accAmnt += aLCPolSet.get(i).getAmnt();
                }
            } else { //计算团单累计保额
                if (!tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getGrpContNo(), "Pause", "1") &&
                    !tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getGrpContNo(), "Terminate", "1")) {
                    accAmnt += aLCPolSet.get(i).getAmnt();
                }
            }
        }
        System.out.println("累计保额：" + accAmnt);
        return accAmnt;
    }

    /**
     * 计算累计风险保额，按风险保额方式计算,用于批处理
     * add by zhangbin
     */
    public double accRiskAmnt(LRInterfaceSchema aLRInterfaceSchema, String recontCode) {
        LRInterfaceSchema tLRInterfaceSchema = aLRInterfaceSchema;

        //得到累计计算风险保额分组名,根据累计计算险种定义，此处每一个险种只能位于一个分组中
        String strSQL =
                "select GroupName from LRAmntRelRisk where recontcode = '" +
                recontCode + "' and RiskCode = '" + tLRInterfaceSchema.getRiskCode() +
                "'";
        SSRS COUNT = new ExeSQL().execSQL(strSQL);
        LCPolSet tLCPolSet = new LCPolDB().executeQuery("select * from lcpol where polno='"+tLRInterfaceSchema.getPolNo()+"' and renewcount="+tLRInterfaceSchema.getReNewCount()+" with ur"); //得到该被保险人的所有累计险种保单

        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return calRiskAmnt(tLCPolSet.get(1)); //风险保额
        }
        double accRiskAmnt = 0;
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();

//        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
//        tLRContInfoDB.setReContCode(recontCode);
//        if (!tLRContInfoDB.getInfo()) {
//            System.out.println("没有此合同！");
//            return 0;
//        }
//        LRContInfoSchema tLRContInfoSchema = tLRContInfoDB.getSchema();
//
//        //如果是成数分保
//        if (tLRContInfoSchema.getCessionMode().equals("1")) {
//            return 0;
//        }

        strSQL = "select * from LCPol where InsuredNo = '" +
        tLRInterfaceSchema.getInsuredNo() +
                 "' and appflag = '1' and  RiskCode in ( select RiskCode from LRAmntRelRisk where GroupName = '" +
                 COUNT.GetText(1, 1) + "' and recontcode='" + recontCode +
                 "') and EndDate > '" + mCurrentDate + "' and ((SignDate < '" +
                 mCurrentDate + "' and Cvalidate < '" + mCurrentDate +
                 "') or (SignDate <= Cvalidate and CvaliDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLRInterfaceSchema.getMakeTime() +
                 "') or (SignDate > Cvalidate and SignDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLRInterfaceSchema.getMakeTime() + "')) with ur ";
        System.out.println("accRiskAmnt:::"+strSQL);
        LCPolSet aLCPolSet = new LCPolDB().executeQuery(strSQL); //得到该被保险人的所有累计险种保单
        for (int i = 1; i <= aLCPolSet.size(); i++) { //对该被保险人所有保单进行循环
            if (aLCPolSet.get(i).getContType().equals("1")) { //计算个单累计风险保额
                if (!tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Available", "1") &&
                    !tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Terminate", "1")) {
                    accRiskAmnt += calRiskAmnt(aLCPolSet.get(i));
                }
            } else { //计算团单累计风险保额
                if (!tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getGrpContNo(), "Pause", "1") &&
                    !tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getGrpContNo(), "Terminate", "1")) {
                    accRiskAmnt += calRiskAmnt(aLCPolSet.get(i));
                }
            }
        }
        System.out.println("累计风险保额：" + accRiskAmnt);
        return accRiskAmnt;
    }

    /**
     * 计算风险保额，用于批处理
     */
    private double calRiskAmnt(LCPolSchema aLCPolSchema) {
        double amnt = aLCPolSchema.getAmnt();
        String reservMoney = reserveMoney(aLCPolSchema);
        System.out.println("准备金： " + reservMoney);
        if (reservMoney == null || reservMoney.equals("")) {
            reservMoney = "0";
        }
        double riskAmnt = amnt - Float.parseFloat(reservMoney);
        return riskAmnt;
    }

    /**
     * 只有长期险,需要计算准备金和保单年度,此处以Cvalidate为准
     * 计算险种保单的期初准备金，用于批处理
     */
    private String reserveMoney(LCPolSchema tLCPolSchema) {
        String riskCode = tLCPolSchema.getRiskCode();
        double amnt = tLCPolSchema.getAmnt();
        String validate = tLCPolSchema.getCValiDate();
        int currYear = PubFun.calInterval(validate, mCurrentDate, "Y") + 1; //计算保单年度,第一年的保单年度为1

        String strSQL = "";
        ExeSQL exesql = new ExeSQL();
        SSRS COUNT;
        String[][] tempdata;
        String reserveMoney = "0";
        if (riskCode.equals("2301")) {
            int payEndYear = tLCPolSchema.getPayEndYear(); //终交年龄年期
            int insuredSex = Integer.parseInt(tLCPolSchema.getInsuredSex());
            int insuredYear = tLCPolSchema.getInsuYear(); //保险年龄年期,保险期间
            int insuredAppAge = tLCPolSchema.getInsuredAppAge();

            strSQL = "select " + amnt + "*reserve/10000 from reserve204 "
                     + " where InsuYear =" + insuredYear + " and PayEndYear =" +
                     payEndYear + " and Sex =" + insuredSex + " and Age =" +
                     insuredAppAge + " and CurrYear =" + currYear;
            System.out.println("2301准备金SQL: " + strSQL);
            COUNT = exesql.execSQL(strSQL);
            if (COUNT != null && COUNT.getMaxRow() > 0) {
                tempdata = COUNT.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
                reserveMoney = tempdata[0][0];
            }
        } else if (riskCode.equals("2401")) {
            int payEndYear = tLCPolSchema.getPayEndYear(); //终交年龄年期
            int insuredSex = Integer.parseInt(tLCPolSchema.getInsuredSex());
            int insuredAppAge = tLCPolSchema.getInsuredAppAge();

            strSQL = "select " + amnt + "*reserve/10000 from reserve205 "
                     + " where PayEndYear =" + payEndYear + " and Sex =" +
                     insuredSex + " and Age =" + insuredAppAge
                     + " and CurrYear = " + currYear;
            System.out.println("2401准备金SQL: " + strSQL);
            COUNT = exesql.execSQL(strSQL);
            if (COUNT != null && COUNT.getMaxRow() > 0) {
                tempdata = COUNT.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
                reserveMoney = tempdata[0][0];
            }
        } else if (riskCode.equals("330106")) {
//          暂时未开发
        } else {
            reserveMoney = "0";
        }
        return reserveMoney;
    }
    /**
     * 按责任分保处理，计算分保保额，置lcpol是否分保标记
     * @param tLRInterfaceSchema LRInterfaceSchema
     * @return boolean
     * 09/9/7 modify 不再对lcpol表置分保标出记reinsureflag和分保金额cessamnt
     */
    private boolean sumCessAmntByRiskCode(LRInterfaceSchema tLRInterfaceSchema){
        double lowAmnt = 0; //最低再保保额
        double highAmnt = 0; //最高再保保额
        double cessionAmnt = 0; //分保保额
        double sumCessAmnt = 0; //累计分出保额
        double retainAmnt = 0; //自留额

        //得到所有该险种的合同号和生效日期
        String tSql = "";
        if(tLRInterfaceSchema.getRiskCode().equals("5601")){//2010年5601因为不同职业类别分保到了两个不同的合同，所以在此用了两个不同的sql，同时用了硬编码。后期修改的时候要将合同的硬编码信息修改。同时建议用一特殊字段区分
        	if(tLRInterfaceSchema.getOccupationType()==null||tLRInterfaceSchema.getOccupationType().equals("")||tLRInterfaceSchema.getOccupationType().equals("0"))
        		tLRInterfaceSchema.setOccupationType("1");
        	if(tLRInterfaceSchema.getOccupationType().equals("4")||
        			tLRInterfaceSchema.getOccupationType().equals("5")||
        			tLRInterfaceSchema.getOccupationType().equals("6")){
        		tSql = "select Rvalidate,RecontCode,CessionMode,CessionFeeMode,ReType from LRContInfo where recontcode='CLR2010A02' and ReContState='01' and (ReType='01' and exists " +
                " (select 1 from LRcalfactorvalue where Recontcode=LRContInfo.Recontcode  and riskcode='" +
                tLRInterfaceSchema.getRiskCode() +
                "'))  and exists (select 1 from lmriskapp where  riskcode='"+tLRInterfaceSchema.getRiskCode()+"')  and ((Rinvalidate is not null and '" +
                mCurrentDate +
                "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
                mCurrentDate + "' >= Rvalidate)) order by Rvalidate desc  with ur" ; 
        	}
        	else 
        		tSql = "select Rvalidate,RecontCode,CessionMode,CessionFeeMode,ReType from LRContInfo where recontcode<>'CLR2010A02' and ReContState='01' and (ReType='01' and exists " +
                " (select 1 from LRcalfactorvalue where Recontcode=LRContInfo.Recontcode  and riskcode='" +
                tLRInterfaceSchema.getRiskCode() +
                "'))  and exists (select 1 from lmriskapp where  riskcode='"+tLRInterfaceSchema.getRiskCode()+"')  and ((Rinvalidate is not null and '" +
                mCurrentDate +
                "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
                mCurrentDate + "' >= Rvalidate)) order by Rvalidate desc  with ur" ; 
        }
        else
        tSql="select Rvalidate,RecontCode,CessionMode,CessionFeeMode,ReType from LRContInfo where ReContState='01' and (ReType='01' and exists " +
        	" (select 1 from LRcalfactorvalue where Recontcode=LRContInfo.Recontcode and riskcode='" +
        	tLRInterfaceSchema.getRiskCode() +
        	"'))  and exists (select 1 from lmriskapp where  riskcode='"+tLRInterfaceSchema.getRiskCode()+"')  and ((Rinvalidate is not null and '" +
        	mCurrentDate +
        	"' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
        	mCurrentDate + "' >= Rvalidate)) order by Rvalidate desc  with ur" ; //因为长期险续期不能根据签单日期计算

        SSRS tSSRS = new ExeSQL().execSQL(tSql);
        if (tSSRS==null || tSSRS.getMaxRow() <= 0) {
            return false;
        }
        String strRecontCode = tSSRS.GetText(1, 2);
        String strCesssionMode = tSSRS.GetText(1, 3);
        String strCessionFeeMode = tSSRS.GetText(1, 4);
        String tReType = tSSRS.GetText(1, 5);
        String sql="";
//      如果是成数分保则直接置合同分保标志
        if(strCesssionMode.equals("1") && StrTool.cTrim(tReType).equals("01")){

            try{
            	LRInterfaceDB ttLRInterfaceDB = new LRInterfaceDB();
            	ttLRInterfaceDB.setSchema(tLRInterfaceSchema);
            	
            	
            	ttLRInterfaceDB.setPolStat("4");
            	ttLRInterfaceDB.setModifyDate(PubFun.getCurrentDate());
            	ttLRInterfaceDB.setModifyTime(PubFun.getCurrentTime());
            	ttLRInterfaceDB.update();
            	
               }catch(Exception e){
                   e.printStackTrace();
               }        }

        else if (strCesssionMode.equals("2") && StrTool.cTrim(tReType).equals("01")) {//风险溢额分保
        	
        	 String tArrayRecont="";
        	 String tCalsql="";
             for(int i=1;i<=tSSRS.getMaxRow();i++){
                 if(tArrayRecont.equals("")){
                     tArrayRecont="'"+tSSRS.GetText(i, 2)+"'";
                 }else{
                     tArrayRecont+=",'"+tSSRS.GetText(i, 2)+"'";
                 }
             
             if(tLRInterfaceSchema.getRiskCode().equals("5601")&&(tLRInterfaceSchema.getOccupationType().equals("4")
 					||tLRInterfaceSchema.getOccupationType().equals("5")||
 					tLRInterfaceSchema.getOccupationType().equals("6"))){
            	 tCalsql="Select CalState,CalType from lrpolrelation where " +
     			"RiskCode='"+tLRInterfaceSchema.getRiskCode()+"' and recontcode='CLR2010A02' " +
     			" and CalState in ('1','2')" +
     			" group by CalType,CalState order by calstate with ur";
     			
     		}
     	else{
        	tCalsql="Select CalState,CalType from lrpolrelation where " +
			"RiskCode='"+tLRInterfaceSchema.getRiskCode()+"' " +
			" and CalState in ('1','2')" +
			" group by CalType,CalState order by calstate with ur";
     	}
        	SSRS tCal = new ExeSQL().execSQL(tCalsql);
        	int maxline=tCal.getMaxRow();
        	double tRiskAmnt=0.00;
        	double tRemainAmn=0.00;
        	
        	if(tCal!=null&&maxline>0){
              LCPolDB tLCPolDB=new LCPolDB(this.mCon);
              tLCPolDB.setPolNo(tLRInterfaceSchema.getPolNo());
              tLCPolDB.setRenewCount(tLRInterfaceSchema.getReNewCount());
              LCPolSet tLCPolSet=tLCPolDB.query();
              if(tLCPolSet==null || tLCPolSet.size()==0){
                  System.out.println("分保保单的置分保金额时出错！.....");
                  return false;
              }
        	for(int n=1;n<=maxline;n++){
        		String tCalState=tCal.GetText(n, 1);

        		int tCalType=Integer.parseInt(tCal.GetText(n, 2));
        		if(tCalState.equals("1")){//风险保额
        			switch (tCalType){
        			case 1 :
        				tRiskAmnt=getRiskAmntA(tLCPolSet);
        				break;
        			case 2 :
        				tRiskAmnt=getRiskAmntB(tLCPolSet);
        				break;
        			case 4 :
    					tRiskAmnt=getRiskAmntD(tLCPolSet,tLRInterfaceSchema.getRiskCode());
    					break;
        			}
        		}
        		if(tCalState.equals("2")){//自留额
        			switch (tCalType){
        			case 1 :
        				tRemainAmn=getRemainAmnA(tLRInterfaceSchema.getPolNo(),tLRInterfaceSchema.getRiskCode(),tRiskAmnt,"1");
        				break;
        			case 2 :
        				tRemainAmn=getRemainAmnB(tLRInterfaceSchema.getRiskCode());
        				break;
        			}
        		}
        		
        	}
        	double tcheck=tRiskAmnt-tRemainAmn;
        	LRInterfaceDB ttLRInterfaceDB = new LRInterfaceDB();
        	ttLRInterfaceDB.setSchema(tLRInterfaceSchema);
        	
        	if(tcheck>0){
        		ttLRInterfaceDB.setPolStat("4");
        		LRPolCessAmntSchema tLRPolCessAmntSchema = new
            	LRPolCessAmntSchema();
            	tLRPolCessAmntSchema.setPolNo(tLRInterfaceSchema.getPolNo());
            	tLRPolCessAmntSchema.setGetDutyCode("0000");
            	tLRPolCessAmntSchema.setReNewCount(tLRInterfaceSchema.getReNewCount());
            	tLRPolCessAmntSchema.setPayCount(tLRInterfaceSchema.getPolYear());
            	tLRPolCessAmntSchema.setSumRiskAmnt(tRiskAmnt);
            	tLRPolCessAmntSchema.setRemainAmnt(tRemainAmn);
            	tLRPolCessAmntSchema.setOperator("Server");
            	tLRPolCessAmntSchema.setMakeDate(PubFun.getCurrentDate());
            	tLRPolCessAmntSchema.setMakeTime(PubFun.getCurrentTime());
            	tLRPolCessAmntSchema.setModifyDate(PubFun.
            			getCurrentDate());
            	tLRPolCessAmntSchema.setModifyTime(PubFun.getCurrentTime());
            	LRPolCessAmntDB tLRPolCessAmntDB=new LRPolCessAmntDB(this.mCon);
            	tLRPolCessAmntDB.setSchema(tLRPolCessAmntSchema);
            	tLRPolCessAmntDB.delete();
            	tLRPolCessAmntDB.insert();
        	}
        	else
        		ttLRInterfaceDB.setPolStat("6");
        	ttLRInterfaceDB.setModifyDate(PubFun.getCurrentDate());
        	ttLRInterfaceDB.setModifyTime(PubFun.getCurrentTime());
        	ttLRInterfaceDB.update();
        	
        }
        }
        }
        return true;
        
    }
    /**
     * 计算按责任分保的分保保额
     * @param tLRInterfaceSchema LRInterfaceSchema
     * @return boolean
     */
    private boolean sumCessAmntByDuty(LRInterfaceSchema tLRInterfaceSchema){
        LRDutyDB tLRDutyDB = new LRDutyDB(this.mCon);
        tLRDutyDB.setPolNo(tLRInterfaceSchema.getPolNo());
        String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tLRInterfaceSchema.getRiskCode()+"' with ur";
        String LRiskFlag = new ExeSQL().getOneValue(aSql);
        if(Integer.parseInt(LRiskFlag)>0){
          tLRDutyDB.setPolYear(tLRInterfaceSchema.getPolYear());
        }else{
           tLRDutyDB.setPolYear(tLRInterfaceSchema.getReNewCount());
       }

        LRDutySet tLRDutySet = tLRDutyDB.query();
        for(int i=1;i<=tLRDutySet.size();i++){
//      得到所有该责任的合同号和生效日期
            String tSql = "select Rvalidate,RecontCode,CessionMode,CessionFeeMode,ReType from LRContInfo where ReContState='01' and (ReType='02' and exists " +
                          " (select 1 from LRcalfactorvalue a,lrduty b where a.Recontcode=LRContInfo.Recontcode " +
                          "and a.riskcode=b.getdutycode and b.polno='" +
                          tLRInterfaceSchema.getPolNo() + "' and b.GetDutyCode='"+tLRDutySet.get(i).getGetDutyCode()+"')) and " +
                          " ((Rinvalidate is not null and '" +
                          mCurrentDate +
                          "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
                          mCurrentDate +
                          "' >= Rvalidate)) order by Rvalidate desc  with ur";
            SSRS tSSRS = new ExeSQL().execSQL(tSql);
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
//          往lcpol表中置分保标记。？？？？？需要增加判断是否大于最低保额
                LCPolDB tLCPolDB = new LCPolDB(this.mCon);
                tLCPolDB.setPolNo(tLRInterfaceSchema.getPolNo());
                tLCPolDB.setRenewCount(tLRInterfaceSchema.getReNewCount());
                LCPolSet tLCPolSet = tLCPolDB.query();
                if (tLCPolSet == null || tLCPolSet.size() == 0) {
                    System.out.println("查询保单出错！.....");
                    return false;
                }
                String tArrayRecont = "";
                String tCessionMode = "";
                for (int m = 1; m <= tSSRS.getMaxRow(); m++) {
                    if (tArrayRecont.equals("")) {
                        tArrayRecont = "'" + tSSRS.GetText(m, 2) + "'";
                    } else {
                        tArrayRecont += ",'" + tSSRS.GetText(m, 2) + "'";
                    }
                }
                for (int m = 1; m <= tSSRS.getMaxRow(); m++) {
                	String check=tSSRS.GetText(1, 3);
                	tCessionMode = tSSRS.GetText(m, 3);
                    if (!check.equals(tCessionMode)) {
                    	
                    	System.out.println("责任分保方式出现错误。同一给付责任分保方式出现不同");
                        return false;
                    	
                    } 
                }
                if(tCessionMode.equals("1"))//成数分保直接在lrduty表里置分保标记
                {
               
                    //置接口表分保标记
                    tLRDutyDB.setSchema(tLRDutySet.get(i));
                    tLRDutyDB.setReinsureFlag("4");
                    tLRDutyDB.setModifyDate(PubFun.getCurrentDate());
                    tLRDutyDB.setModifyTime(PubFun.getCurrentTime());
                    tLRDutyDB.update();
//                    LRInterfaceDB ttLRInterfaceDB = new LRInterfaceDB();
//                    ttLRInterfaceDB.setSchema(tLRInterfaceSchema);
//                    ttLRInterfaceDB.setPolStat("4");
//                    ttLRInterfaceDB.setModifyDate(PubFun.getCurrentDate());
//                    ttLRInterfaceDB.setModifyTime(PubFun.getCurrentTime());
//                    ttLRInterfaceDB.update();
                
                
                }
                if(tCessionMode.equals("2"))
                {
                	String tCalsql="Select CalType,CalState from lrpolrelation where " +
                						"RiskCode='"+tLRDutySet.get(i).getGetDutyCode()+"' " +
                						" and CalState in ('1','2')" +
                						" group by CalType,CalState order by CalState with ur";
                	SSRS tCal = new ExeSQL().execSQL(tCalsql);
                	int maxline=tCal.getMaxRow();
                	double tRiskAmnt=0.00;
                	double tRemainAmn=0.00;
                	//double tCessAmnt=0.00;
                	if(tCal!=null&&maxline>0){
                	                	
                		for(int n=1;n<=maxline;n++){
                			String tCalState=tCal.GetText(n, 2);
                			
                			int tCalType=Integer.parseInt(tCal.GetText(n, 1));
                			if(tCalState.equals("1")){//风险保额
                				switch (tCalType){
                				case 1 :
                					tRiskAmnt=getRiskAmntA(tLCPolSet,tLRDutySet.get(i).getDutyCode(),tLRDutySet.get(i).getGetDutyCode());
                					break;
                				case 2 :
                					tRiskAmnt=getRiskAmntB(tLCPolSet,tLRDutySet.get(i).getDutyCode(),tLRDutySet.get(i).getGetDutyCode());
                					break;
                				case 3 :
                					tRiskAmnt=getRiskAmntC(tLCPolSet,tLRDutySet.get(i).getDutyCode(),tLRDutySet.get(i).getGetDutyCode());
                					break;
                				case 4 :
                					tRiskAmnt=getRiskAmntD(tLCPolSet,tLRDutySet.get(i).getGetDutyCode());
                					break;
                				}
                			}
                			if(tCalState.equals("2")){//自留额
                				switch (tCalType){
                				case 1 :
                					tRemainAmn=getRemainAmnA(tLRDutySet.get(i).getPolNo(),tLRDutySet.get(i).getGetDutyCode(),tRiskAmnt,"2");
                					break;
                				case 2 :
                					tRemainAmn=getRemainAmnB(tLRDutySet.get(i).getGetDutyCode());
                					break;
                				}
                			}
                			
                		}
                		tLRDutyDB.setSchema(tLRDutySet.get(i));
                		double tcheck=tRiskAmnt-tRemainAmn;
                		if(tcheck>0){
                        tLRDutyDB.setReinsureFlag("4");
                        tLRDutyDB.setModifyDate(PubFun.getCurrentDate());
                        tLRDutyDB.setModifyTime(PubFun.getCurrentTime());
                        tLRDutyDB.update();
                        LRInterfaceDB ttLRInterfaceDB = new LRInterfaceDB();
                        ttLRInterfaceDB.setSchema(tLRInterfaceSchema);
                        ttLRInterfaceDB.setPolStat("4");
                        ttLRInterfaceDB.setModifyDate(PubFun.getCurrentDate());
                        ttLRInterfaceDB.setModifyTime(PubFun.getCurrentTime());
                        ttLRInterfaceDB.update();
                        LRPolCessAmntSchema tLRPolCessAmntSchema = new
                        LRPolCessAmntSchema();
                        tLRPolCessAmntSchema.setPolNo(tLRInterfaceSchema.getPolNo());
                        tLRPolCessAmntSchema.setGetDutyCode(tLRDutySet.get(i).getGetDutyCode());
                        tLRPolCessAmntSchema.setReNewCount(tLRInterfaceSchema.getReNewCount());
                        tLRPolCessAmntSchema.setPayCount(tLRInterfaceSchema.getPolYear());
                        tLRPolCessAmntSchema.setSumRiskAmnt(tRiskAmnt);
                        tLRPolCessAmntSchema.setRemainAmnt(tRemainAmn);
                        tLRPolCessAmntSchema.setOperator("Server");
                        tLRPolCessAmntSchema.setMakeDate(PubFun.getCurrentDate());
                        tLRPolCessAmntSchema.setMakeTime(PubFun.getCurrentTime());
                        tLRPolCessAmntSchema.setModifyDate(PubFun.
                        getCurrentDate());
                        tLRPolCessAmntSchema.setModifyTime(PubFun.getCurrentTime());
                        LRPolCessAmntDB tLRPolCessAmntDB=new LRPolCessAmntDB(this.mCon);
                        tLRPolCessAmntDB.setSchema(tLRPolCessAmntSchema);
                        tLRPolCessAmntDB.delete();
                        tLRPolCessAmntDB.insert();
                		}
                		else{
                			 tLRDutyDB.setReinsureFlag("6");
                             tLRDutyDB.setModifyDate(PubFun.getCurrentDate());
                             tLRDutyDB.setModifyTime(PubFun.getCurrentTime());
                             tLRDutyDB.update();
                		}
                			
                		
                	}
                }
            }
        }
        return true;
    }
    
    /*第一类风险保额计算方式,分保方式为责任*/
    private double getRiskAmntA(LCPolSet tLCPolSet,String tDutyCode,String tGetDutyCode){
    	
    	double tRiskAmnt=0.00;
    	double tCash=0.00;
    	double tPay=0.00;
    	String tAmntsql="select standmoney from lcget where " +
    				"polno='"+tLCPolSet.get(1).getPolNo()+"' and dutycode='"+tDutyCode+"' " +
    				"and getdutycode='"+tGetDutyCode+"' with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(tAmntsql);
    	double tStandAmnt=Double.parseDouble(tSSRS.GetText(1, 1));
    	tPay=getClaimValue(tLCPolSet);
    	tCash= getCashValue(tLCPolSet);
    	tRiskAmnt=tStandAmnt*5-tCash-tPay;
    	
    	return tRiskAmnt;
    	}
    /*第一类风险保额计算方式,分保方式为险种*/
    private double getRiskAmntA(LCPolSet tLCPolSet){
    	
    	double tRiskAmnt=0.00;
    	double tCash=0.00;
    	double tPay=0.00;
    	String tAmntsql="select amnt from lcpol where " +
    				"polno='"+tLCPolSet.get(1).getPolNo()+"' with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(tAmntsql);
    	double tStandAmnt=Double.parseDouble(tSSRS.GetText(1, 1));
    	tCash= getCashValue(tLCPolSet);
    	tPay=getClaimValue(tLCPolSet);
    	tRiskAmnt=tStandAmnt*5-tCash-tPay;
    	
    	return tRiskAmnt;
    	}
    
    /*第二类风险保额计算方式,分保方式为责任*/
    private double getRiskAmntB(LCPolSet tLCPolSet,String tDutyCode,String tGetDutyCode){
    	
    	double tRiskAmnt=0.00;
    	double tCash=0.00;
    	String tAmntsql="select standmoney from lcget where " +
    				"polno='"+tLCPolSet.get(1).getPolNo()+"' and dutycode='"+tDutyCode+"' " +
    				"and getdutycode='"+tGetDutyCode+"' with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(tAmntsql);
    	double tStandAmnt=Double.parseDouble(tSSRS.GetText(1, 1));
    	tCash= getCashValue(tLCPolSet);
    	tRiskAmnt=tStandAmnt-tCash;
    	
    	return tRiskAmnt;
    	}
    
    /*第二类风险保额计算方式,分保方式为险种*/
    private double getRiskAmntB(LCPolSet tLCPolSet){
    	
    	double tRiskAmnt=0.00;
    	double tCash=0.00;
    	String tAmntsql="select amnt from lcpol where " +
    				"polno='"+tLCPolSet.get(1).getPolNo()+"' with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(tAmntsql);
    	double tStandAmnt=Double.parseDouble(tSSRS.GetText(1, 1));
    	tCash= getCashValue(tLCPolSet);
    	tRiskAmnt=tStandAmnt-tCash;
    	
    	return tRiskAmnt;
    	}
    
    /*第三类风险保额计算方式,分保方式为责任*/
    private double getRiskAmntC(LCPolSet tLCPolSet,String tDutyCode,String tGetDutyCode){
    	
    	double tRiskAmnt=0.00;
    	double tCash=0.00;
    	String tAmntsql="select standmoney from lcget where " +
    				"polno='"+tLCPolSet.get(1).getPolNo()+"' and dutycode='"+tDutyCode+"' " +
    				"and getdutycode='"+tGetDutyCode+"' with ur";
    	String tyearsql="select year(date('"+mCurrentDate+"')-cvalidate) from lcpol where " +
		"polno='"+tLCPolSet.get(1).getPolNo()+"'  with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(tAmntsql);
    	double tStandAmnt=Double.parseDouble(tSSRS.GetText(1, 1));
    	tSSRS = new ExeSQL().execSQL(tyearsql);
    	int tyears=Integer.parseInt(tSSRS.GetText(1, 1));
    	tCash= getCashValue(tLCPolSet);
    	tRiskAmnt=tStandAmnt*(1+(0.05*tyears))-tCash;
    	
    	return tRiskAmnt;
    	}
    
    /*第四类风险保额计算方式,分保方式为责任*/
   private double getRiskAmntD(LCPolSet tLCPolSet,String tRiskCode){
    	
    	double tRiskAmnt=0.00;
    	double tCash=0.00;
    	String tAmntsql="select sum(money) from lcinsureacctrace where riskcode=" +
    				"(select code1 from ldcode1 where code='"+tLCPolSet.get(1).getRiskCode()+"' and codetype = 'checkappendrisk') " +
    				" and contno=(select contno from lcpol where polno='"+tLCPolSet.get(1).getPolNo()+"') " +
    						"and makedate<='"+this.mCurrentDate+"' with ur";
    	
    	SSRS tSSRS = new ExeSQL().execSQL(tAmntsql);
    	String tRate="select Argu2 from lrpolrelation where " +
		"RiskCode='"+tRiskCode+"' and CalState='2' with ur";
    	SSRS trSSRS = new ExeSQL().execSQL(tRate);
    	double tArgu2=Double.parseDouble(trSSRS.GetText(1, 1));
    	double tStandAmnt=Double.parseDouble(tSSRS.GetText(1, 1));
    	tCash= getCashValue(tLCPolSet);
    	tRiskAmnt=tStandAmnt*tArgu2-tCash;
    	return tRiskAmnt;
    	}

    /*第一类自留额计算方式*/
    private double getRemainAmnA(String tPolNo,String tRiskCode,double tRiskAmnt,String tcheck){
    	double tRemainAmnt=0.00;
    	String tGetDutyCode="0000";
    	String tAmntsql="select Argu1,Argu2 from lrpolrelation where " +
    				"RiskCode='"+tRiskCode+"' and CalState='2' with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(tAmntsql);
    	double tArgu1=Double.parseDouble(tSSRS.GetText(1, 1));
    	double tArgu2=Double.parseDouble(tSSRS.GetText(1, 2));
    	if("2".equals(tcheck))
    		tGetDutyCode=tRiskCode;
    		tRemainAmnt=Math.min(tArgu1, tArgu2*tRiskAmnt);
    	
    	return tRemainAmnt;
    	}
    
    /*第二类自留额计算方式*/
    private double getRemainAmnB(String tRiskCode){
    	double tRemainAmnt=0.00;
    	String tAmntsql="select Argu1 from lrpolrelation where " +
    				"RiskCode='"+tRiskCode+"' and CalState='2' with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(tAmntsql);
    	double tArgu1=Double.parseDouble(tSSRS.GetText(1, 1));
    	tRemainAmnt=tArgu1;
    	
    	return tRemainAmnt;
    	}
    
    

    private double getCashValue(LCPolSet cLCPolSet)
   
{
	//特殊标签
	double tCash=0.00;
	ExeSQL tExeSQL = new ExeSQL();
	SSRS tSSRS = new SSRS();
	SSRS ttSSRS = new SSRS();

	//现价存在标志
	boolean tFlag = false;
	int j = 0;

	//循环处理险种下的现金价值信息
	for (int i = 1; i <= cLCPolSet.size(); i++)
	{
		String tRiskCode = cLCPolSet.get(i).getRiskCode();

    

		//得到现金价值的算法描述
		LMCalModeDB tLMCalModeDB = new LMCalModeDB();
		//获取险种信息
		tLMCalModeDB.setRiskCode(tRiskCode);
		tLMCalModeDB.setType("X");
		LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

		//解析得到的SQL语句
		String strSQL = "";
		//如果描述记录唯一时处理
		if (tLMCalModeSet.size() == 1)
		{
			strSQL = tLMCalModeSet.get(1).getCalSQL();
		}
		// 这个险种不需要取现金价值的数据
		if (strSQL.equals(""))
		{
		}
		else
		{
			tFlag = true;
			
			String tSql = "select riskname from lmriskapp where riskcode = '"
                	+ cLCPolSet.get(i).getRiskCode() + "'";
			tSSRS = tExeSQL.execSQL(tSql);

			
			j = j + 1;

			Calculator calculator = new Calculator();
			//设置基本的计算参数
			calculator.addBasicFactor("ContNo", cLCPolSet.get(i)
					.getContNo());
			calculator.addBasicFactor("InsuredNo", cLCPolSet.get(i)
					.getInsuredNo());
			calculator.addBasicFactor("InsuredSex", cLCPolSet.get(i)
					.getInsuredSex());
			calculator.addBasicFactor("InsuredAppAge",  String
					.valueOf(cLCPolSet.get(i).getInsuredAppAge()));
			calculator.addBasicFactor("PayIntv", String.valueOf(cLCPolSet
					.get(i).getPayIntv()));
			calculator.addBasicFactor("PayEndYear", String
					.valueOf(cLCPolSet.get(i).getPayEndYear()));
			calculator.addBasicFactor("PayEndYearFlag", String
					.valueOf(cLCPolSet.get(i).getPayEndYearFlag()));
			calculator.addBasicFactor("PayYears", String.valueOf(cLCPolSet
					.get(i).getPayYears()));
			calculator.addBasicFactor("InsuYear", String.valueOf(cLCPolSet
					.get(i).getInsuYear()));
			calculator.addBasicFactor("Prem", String.valueOf(cLCPolSet.get(
					i).getPrem()));
			calculator.addBasicFactor("Amnt", String.valueOf(cLCPolSet.get(
					i).getAmnt()));
			calculator.addBasicFactor("FloatRate", String.valueOf(cLCPolSet
					.get(i).getFloatRate()));
			calculator.addBasicFactor("Mult", String.valueOf(cLCPolSet.get(
					i).getMult()));
			//add by yt 2004-3-10
			calculator.addBasicFactor("InsuYearFlag", String
					.valueOf(cLCPolSet.get(i).getInsuYearFlag()));
			calculator.addBasicFactor("GetYear", String.valueOf(cLCPolSet
					.get(i).getGetYear()));
			calculator.addBasicFactor("GetYearFlag", String
					.valueOf(cLCPolSet.get(i).getGetYearFlag()));
			calculator.addBasicFactor("CValiDate", String.valueOf(cLCPolSet
					.get(i).getCValiDate()));
			calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
			strSQL = calculator.getCalSQL();
			String tyearsql="select year(date('"+mCurrentDate+"')-cvalidate) from lcpol where " +
			"polno='"+cLCPolSet.get(i).getPolNo()+"'  with ur";
	    	SSRS tSSRSY = new ExeSQL().execSQL(tyearsql);
	    	int tyears=Integer.parseInt(tSSRSY.GetText(1, 1))+1;
			

			System.out.println(strSQL);
			//执行现金价值计算sql，获取现金价值列表
				ttSSRS = tExeSQL.execSQL(strSQL);
				if(ttSSRS.MaxRow>0)
					if(tyears-1!=0)
					tCash=Double.parseDouble(ttSSRS.GetText(tyears-1, 3));
					System.out.println("********************"+tCash+"**************");
					
				
		}
}
//如果有现价，才显示标签
/*        if (tFlag)
 {
 //全部现金价值信息取完毕后，将xmlDataList数据添加到xmlDataset中
 }*/
	return tCash;
}
    
    private double getClaimValue(LCPolSet cLCPolSet ){
    	double trealpay=0.00;
    	String clmsql="select sum(a.realpay) from LLClaimDetail a,LLCase b where " +
    			" a.CaseNo=b.CaseNo " +
    			"and a.polno='"+cLCPolSet.get(1).getPolNo()+"' " +
    					"and a.riskcode='"+cLCPolSet.get(1).getRiskCode()+"' " +
    							"and b.endcasedate <='"+mCurrentDate+"' with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(clmsql);
    	if(tSSRS!=null&&tSSRS.GetText(1, 1)!=null&&!"".equals(tSSRS.GetText(1, 1))
    			&&!"null".equals(tSSRS.GetText(1, 1)))
    	trealpay=Double.parseDouble(tSSRS.GetText(1, 1));
    	return trealpay;
    }
    /**
     * 合同分保得到CalCode
     * @param aLRPolSchema LRPolSchema
     * @param aCalCodeType String
     * @return String
     */
    public String getCalCode(LRPolSchema aLRPolSchema, String aCalCodeType) {
        LRPolSchema tLRPolSchema = aLRPolSchema;
        String strSQL = "";
        SSRS calSortSSRS = new SSRS();
        String tRiskCode=tLRPolSchema.getRiskCode();
        if("02".equals(aLRPolSchema.getReType())){
            
            String tsql="select distinct riskcode from lrcalfactorvalue where" +
    		" recontcode='"+aLRPolSchema.getReContCode()+"' with ur";
            SSRS tRD = new ExeSQL().execSQL(tsql); 
            tRiskCode=tRD.GetText(1, 1);
            }
        if (aCalCodeType.equals("CessPremMode")) {
            strSQL = "select CessPremMode from LRCalMode where RecontCode ='" +
                     tLRPolSchema.getReContCode() + "' and RiskCode='" +tRiskCode
                     
                     + "' and  ReinsurItem!='F' and RiskSort='" +//反冲数据时ReinsureItem='F',所以增加等于C的值
                     tLRPolSchema.getRiskCalSort() + "'";
            calSortSSRS = new ExeSQL().execSQL(strSQL);
        } else if (aCalCodeType.equals("RiskAmntCalCode")) {
            strSQL =
                    "select RiskAmntCalCode from LRCalMode where RecontCode ='" +
                    tLRPolSchema.getReContCode() + "' and RiskCode='" +
                    tLRPolSchema.getRiskCode()
                    + "' and and  ReinsurItem!='F' and RiskSort='" + //反冲数据时ReinsureItem='F',所以增加等于C的值
                    tLRPolSchema.getRiskCalSort() + "'";
            calSortSSRS = new ExeSQL().execSQL(strSQL);
        }
        if (calSortSSRS.getMaxRow() <= 0) {
            System.out.println("获取再保LRCalMode算法代码失败!");
            return null;
        }
        return calSortSSRS.GetText(1, 1);
    }

    /**
     * 计算合同分保保费率
     * 参数1： CalCode
     * 参数2： LRPolSchema
     * @return double
     */
    public double calCessFeeRate(String calCode, LRPolSchema aLRPolSchema) {
        double cessFeeRate = 0;
        double diseaseAddFee = aLRPolSchema.getDiseaseAddFeeRate();
        double deadAddFee = aLRPolSchema.getDeadAddFeeRate();
        String tRiskCode = aLRPolSchema.getRiskCode();
        String tRiskCalSort = aLRPolSchema.getRiskCalSort();
        if("02".equals(aLRPolSchema.getReType())){
            
            String tsql="select distinct riskcode from lrcalfactorvalue where" +
    		" recontcode='"+aLRPolSchema.getReContCode()+"' with ur";
            SSRS tRD = new ExeSQL().execSQL(tsql); 
            tRiskCode=tRD.GetText(1, 1);
        }
        String tManagecom =aLRPolSchema.getManageCom();
        String tOccType = aLRPolSchema.getOccupationType();
        //由于系统中存在职业类别为空的客户，经过与再保讨论，这种情况下默认为职业类别1
         if (tOccType == null || tOccType.equals("")|| tOccType.equals("0")) {
        	 tOccType = "1";
         }
	    String tSex = aLRPolSchema.getInsuredSex();
	    String tAge = PubFun.calInterval(aLRPolSchema.getInsuredBirthday(),aLRPolSchema.getGetDataDate(), "Y")+"" ;  
	    String tGetDutyCode ="";
	    String tPolYear =PubFun.calInterval(aLRPolSchema.getCValiDate(),aLRPolSchema.getGetDataDate(), "Y")+"";
	    if (tRiskCode.equals("2602") ||tRiskCode.equals("260301")) { //如果是2602
            //2009/4/30当险种260301承保的是死亡责任保险金时，取团险死亡费率。此考中方法只适用于现有人两个责任时，如果有新增责任，需要重新审查。
            String sqla = "select getdutycode from lcget a where getdutycode='634202'"
                        +" and polno='"+aLRPolSchema.getPolNo()+"' "+
                        " union all "+
                        "select getdutycode from lbget a where getdutycode='634202'"
                        +" and polno='"+aLRPolSchema.getPolNo()+"' "+
                        " with ur ";
            SSRS aSSRS = new ExeSQL().execSQL(sqla);
            if(aSSRS!=null&&aSSRS.getMaxRow()>0)
            {
            	tGetDutyCode = aSSRS.GetText(1, 1);
            }
	    }
	    Calculator tCalculator = new Calculator();
	    tCalculator.addBasicFactor("ManageCom", tManagecom);
	    tCalculator.addBasicFactor("RiskCode", tRiskCode);
	    tCalculator.addBasicFactor("CalCode", calCode);
	    tCalculator.addBasicFactor("ReContCode", aLRPolSchema.getReContCode());
	    tCalculator.addBasicFactor("Sex", tSex);
	    tCalculator.addBasicFactor("Age", tAge);
	    tCalculator.addBasicFactor("DiseaseAddFee", diseaseAddFee+"");
	    tCalculator.addBasicFactor("DeadAddFee", deadAddFee+"");
	    tCalculator.addBasicFactor("GetDutyCode", tGetDutyCode);
	    tCalculator.addBasicFactor("GetDataDate", aLRPolSchema.getGetDataDate());
	    tCalculator.addBasicFactor("Occtype", tOccType);
	    tCalculator.addBasicFactor("PolYear", tPolYear);
	    tCalculator.addBasicFactor("POLTYPEFLAG",aLRPolSchema.getPolTypeFlag() );
	    tCalculator.addBasicFactor("GrpPolNo", aLRPolSchema.getGrpPolNo());
	    tCalculator.addBasicFactor("PolNo", aLRPolSchema.getPolNo());
        tCalculator.setCalCode(calCode);
        String tFeeRate = tCalculator.calculate();
        if(!"".equals(tFeeRate)&&null!=tFeeRate)
        {
        	return Double.parseDouble(tFeeRate);
        }
        if(tRiskCode.equals("531001")) return CommonBL.carry(0.108);
        if(tRiskCode.equals("299201")) return CommonBL.carry(0.35);
        if(tRiskCode.equals("299202")) return CommonBL.carry(0.06);
        if(tRiskCode.equals("530301")) return CommonBL.carry(0.08);
        if(tRiskCode.equals("531201")) return CommonBL.carry(0.85);
        if(tRiskCode.equals("530501")) return CommonBL.carry(0.85);
//      20140121 描述CLR2014A06新产品费率,这程序恶心到家了   
        if(tRiskCode.equals("550706")&&aLRPolSchema.getReContCode().equals("CLR2014A06")) return CommonBL.carry(0.4);
        if(tRiskCode.equals("550806")&&aLRPolSchema.getReContCode().equals("CLR2014A06")) return CommonBL.carry(0.48);
        if(tRiskCode.equals("511101")&&aLRPolSchema.getReContCode().equals("CLR2014A06")) return CommonBL.carry(0.61);
        
        if(tRiskCode.equals("344202")&&aLRPolSchema.getReContCode().equals("GER2013A01")) return CommonBL.carry(0.36);
        if(tRiskCode.equals("532501")&&aLRPolSchema.getReContCode().equals("GER2013A02")) return CommonBL.carry(0.61);
        if(tRiskCode.equals("344202")&&aLRPolSchema.getReContCode().equals("SII2013A01")) return CommonBL.carry(0.36);
        if(tRiskCode.equals("532501")&&aLRPolSchema.getReContCode().equals("SII2013A02")) return CommonBL.carry(0.52);
        if(tRiskCode.equals("344202")&&aLRPolSchema.getReContCode().equals("CPR2015A01")) return CommonBL.carry(0.36);
        if(tRiskCode.equals("532501")&&aLRPolSchema.getReContCode().equals("CPR2015A02")) return CommonBL.carry(0.52);
        if(tRiskCode.equals("511101")&&aLRPolSchema.getReContCode().equals("SWR2016A01")) return CommonBL.carry(0.47);
        TransferData tT = new TransferData();
        tT = getCalFactor(aLRPolSchema);   //获取费率的要素值
        if (tT == null) {
            buildError("calCessFeeRate().getCalFactor()", "费率要素值无法获取");
            return -1;
        }
        
        
        String manageCom = (String) tT.getValueByName("ManageCom");
        String startDate = (String) tT.getValueByName("StartDate");
        String occType = (String) tT.getValueByName("OccType");
        String sex = (String) tT.getValueByName("Sex");
        String age = (String) tT.getValueByName("Age");
        
        Calculator tCalculator1 = new Calculator();
        Calculator tCalculator2 = new Calculator();
        Calculator tCalculator3 = new Calculator();
        tCalculator1.setCalCode(calCode);
        tCalculator2.setCalCode(calCode);
        tCalculator3.setCalCode(calCode);

        //增加基本要素
        if (tRiskCode.equals("5201")||tRiskCode.equals("550706")||tRiskCode.equals("550806")) { //如果是5201
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Occtype", occType);
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的费率");
                return -1;
            }
            cessFeeRate = Double.parseDouble(tCalculator1.calculate());
            System.out.println("ManageCom: " + aLRPolSchema.getManageCom() +
                               " StartDate： " + " 2005-1-1" + "Occtype： " +
                               occType + " calculate: " +
                               tCalculator1.calculate());
        } else if (tRiskCode.equals("5601")) { //如果是5601, 临分的5601险默认使用2006-1-1的费率表
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Occtype", occType);
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的费率");
                return -1;
            }
            cessFeeRate = Double.parseDouble(tCalculator1.calculate());
            System.out.println("ManageCom: " + aLRPolSchema.getManageCom() +
                               " StartDate： " + " 2005-1-1" + "Occtype： " +
                               occType + " calculate: " +
                               tCalculator1.calculate());
        } else if (tRiskCode.equals("2301") || tRiskCode.equals("230201") || tRiskCode.equals("240201")) { //如果是2301
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null ||
                tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRate = feeRate1 * (1 + diseaseAddFee) +
                          feeRate2 * (1 + deadAddFee);
            System.out.println("费率1: " + feeRate1 + "重疾加费：" + diseaseAddFee);
            System.out.println("费率2: " + feeRate2 + "死亡加费：" + deadAddFee);

        } else if (tRiskCode.equals("2401")) { //如果是2401
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null
                || tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRate = feeRate1 * (1 + diseaseAddFee) +
                          feeRate2 * (1 + deadAddFee);
            System.out.println("费率1: " + feeRate1 + "重疾加费：" + diseaseAddFee);
            System.out.println("费率2: " + feeRate2 + "死亡加费：" + deadAddFee);

        } else if (tRiskCode.equals("2601") || tRiskCode.equals("260401")) { //如果是2601
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1;
            System.out.println("费率1: " + feeRate1);
        } else if (tRiskCode.equals("2602") ||tRiskCode.equals("260301")) { //如果是2602
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");

            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null ||
                tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
//            cessFeeRate = feeRate1 + feeRate2;
            //2009/4/30当险种260301承保的是死亡责任保险金时，取团险死亡费率。此考中方法只适用于现有人两个责任时，如果有新增责任，需要重新审查。
            String sqla = "select getdutycode from lcget a where getdutycode='634202'"
                        +" and polno='"+aLRPolSchema.getPolNo()+"' "+
                        " union all "+
                        "select getdutycode from lbget a where getdutycode='634202'"
                        +" and polno='"+aLRPolSchema.getPolNo()+"' "+
                        " with ur ";
            SSRS aSSRS = new ExeSQL().execSQL(sqla);
            if(aSSRS !=null && aSSRS.getMaxRow()>0 ){
                if(aSSRS.GetText(1,1).equals("634202")){
                    cessFeeRate = feeRate2;
                }
            }else{
               cessFeeRate = feeRate1 + feeRate2;
            }
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);
        }
        else if (tRiskCode.equals("240301")) { //如果是240301
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");

            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null ||
                tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRate = feeRate1 + feeRate2;
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);
        }
        else if (tRiskCode.equals("229201") || tRiskCode.equals("234201")) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1;
            System.out.println("费率1: " + feeRate1);
        }
        else if (tRiskCode.equals("229203") || tRiskCode.equals("229205")
        		|| tRiskCode.equals("234203")
        		|| tRiskCode.equals("234205")) {
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1/2;
            System.out.println("费率1: " + feeRate1/2);
        }
        else if (tRiskCode.equals("230601")) { //如果是240301
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");

            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null ||
                tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRate = feeRate1 + feeRate2;
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);
        }
        else if (tRiskCode.equals("250201") || tRiskCode.equals("237201")|| tRiskCode.equals("560201")|| tRiskCode.equals("560801")) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1;
            System.out.println("费率1: " + feeRate1);
        }
        else if (tRiskCode.equals("250204") || tRiskCode.equals("237204")|| tRiskCode.equals("530206")
        		|| tRiskCode.equals("530207")) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1/2;
            System.out.println("费率1: " + feeRate1/2);
        }
        else if (tRiskCode.equals("284202")||tRiskCode.equals("270202")) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");

            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            
            tCalculator3.addBasicFactor("ManageCom", manageCom);
            tCalculator3.addBasicFactor("StartDate", startDate);
            tCalculator3.addBasicFactor("Type", "3");
            tCalculator3.addBasicFactor("Sex", sex);
            tCalculator3.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null 
            		|| tCalculator3.calculate() == null ||
                tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")
                ||tCalculator3.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            double feeRate3 = Double.parseDouble(tCalculator3.calculate());
            cessFeeRate = feeRate1 - feeRate2+feeRate3;
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);
            System.out.println("费率3: " + feeRate3);
        }
        else if (tRiskCode.equals("284201") || tRiskCode.equals("270201")) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "2");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1;
            System.out.println("费率1: " + feeRate1);
        }
        else if (tRiskCode.equals("540101") ) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1;
            System.out.println("费率1: " + feeRate1);
        }

        else if (tRiskCode.equals("230801")) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator1.addBasicFactor("Sex", sex);
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1;
            System.out.println("费率1: " + feeRate1);
        }
        else if (tRiskCode.equals("230901")||tRiskCode.equals("280101")) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");

            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            
            
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null 
            		||tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")
                ) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            
            cessFeeRate = feeRate1 + feeRate2;
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);
            
        }
        else if (tRiskCode.equals("380202")||tRiskCode.equals("380203")||tRiskCode.equals("380204")||tRiskCode.equals("380205")) { 
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator1.addBasicFactor("Sex", sex);
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1;
            System.out.println("费率1: " + feeRate1);
            return cessFeeRate;
        }
        return CommonBL.carry(cessFeeRate);
    }

    /**
         * 得到合同再保费率表要素
         * 参数: LCPolSchema对象
         * @return boolean
         */
        private TransferData getCalFactor(LRPolSchema aLRPolSchema) {
            TransferData tTransferData = new TransferData();
            String tRiskCode = aLRPolSchema.getRiskCode();
            String tGetDutyCode="";
            if("02".equals(aLRPolSchema.getReType())){
                        
            String tsql="select distinct riskcode from lrcalfactorvalue where" +
    		" recontcode='"+aLRPolSchema.getReContCode()+"' with ur";
            SSRS tRD = new ExeSQL().execSQL(tsql); 
            tGetDutyCode=tRD.GetText(1, 1);
            }
            /*简化机构配置问题
             *查询配置表中有没有配置该机构的费率,如果没有,则将机构号赋值为'8600'
             */
           String rateTable = "";
           if (aLRPolSchema.getRiskCode().equals("5201")) {
               rateTable = "Rate5201";
           } else if (aLRPolSchema.getRiskCode().equals("5601")) {
               rateTable = "Rate5601";

           } else if (aLRPolSchema.getRiskCode().equals("2301") ||
                      aLRPolSchema.getRiskCode().equals("2401") ||
                      aLRPolSchema.getRiskCode().equals("230201")||
                      aLRPolSchema.getRiskCode().equals("240201")) {
               rateTable = "Rate2301";
           } else if (aLRPolSchema.getRiskCode().equals("2601") ||
                   aLRPolSchema.getRiskCode().equals("260401")) {
               rateTable = "Rate2601";
           } else if (aLRPolSchema.getRiskCode().equals("2602")||
                   aLRPolSchema.getRiskCode().equals("260301")) {
               rateTable = "Rate2602";
           }
           else if (aLRPolSchema.getRiskCode().equals("240301")) {
               rateTable = "Rate230701";
           }
           else if (tGetDutyCode.equals("229201")||tGetDutyCode.equals("234201")) {
               rateTable = "Rate229201";
           }
           else if (tGetDutyCode.equals("229203")||tGetDutyCode.equals("229205")
        		   ||tGetDutyCode.equals("234203")
        		   ||tGetDutyCode.equals("234205")) {
               rateTable = "Rate229203";
           }
           else if (aLRPolSchema.getRiskCode().equals("230601")) {
               rateTable = "Rate230601";
           }
           else if (tGetDutyCode.equals("250201")||tGetDutyCode.equals("237201")) {
               rateTable = "rate250201";
           }
           else if (tGetDutyCode.equals("250204")||tGetDutyCode.equals("237204")
        		   ||aLRPolSchema.getRiskCode().equals("530206")||
        		   aLRPolSchema.getRiskCode().equals("530207")) {
               rateTable = "rate250204";
           }
           else if (tGetDutyCode.equals("284201")||tGetDutyCode.equals("284202")
        		   ||tGetDutyCode.equals("270201")
        		   ||tGetDutyCode.equals("270202")) {
               rateTable = "Rate230501";
           }
           else if (aLRPolSchema.getRiskCode().equals("540101")) {
               rateTable = "Re540101";
           }
           else if (aLRPolSchema.getRiskCode().equals("230801")) {
               rateTable = "Re230801";
           }
           else if (aLRPolSchema.getRiskCode().equals("230901")) {
               rateTable = "Re230901";
           }
           else if(aLRPolSchema.getRiskCode().equals("550706")){//090627新增
               rateTable = "Rate550706";
           } else if(aLRPolSchema.getRiskCode().equals("550806")){//090627新增
               rateTable = "Rate550806";
           }else if(aLRPolSchema.getRiskCode().equals("280101")){//090627新增
               rateTable = "Rate280101";
           }
           else if(aLRPolSchema.getRiskCode().equals("560201")){
               rateTable = "Rate560201";
           }
//          20140121 描死添加!!!太恶了 CLR2014A01
           else if(aLRPolSchema.getRiskCode().equals("560801")){
               rateTable = "Rate5608";
           }
           else if (tGetDutyCode.equals("380202"))
           {
           	 rateTable = "RE380202";
           }
           else if (tGetDutyCode.equals("380203")||tGetDutyCode.equals("380204")||tGetDutyCode.equals("380205"))
           {
           	 rateTable = "RE38020345";
           }
            if(rateTable==null || rateTable.equals("")){
                return null;
            }
            String msql = "select Managecom from "+ rateTable +" where Managecom='"+aLRPolSchema.getManageCom().substring(0,4) + "'" ;
            SSRS mSSRS = new ExeSQL().execSQL(msql);
            if(mSSRS.MaxRow==0){
                aLRPolSchema.setManageCom("8600");
            }else{
                aLRPolSchema.setManageCom(mSSRS.GetText(1,1));
            }

            if ((tRiskCode.equals("5201") || tRiskCode.equals("5601"))&&aLRPolSchema.getRiskCalSort().equals("2")
            		||
                    tRiskCode.equals("550706")||tRiskCode.equals("550806")) {
                if (getRateDate(rateTable,aLRPolSchema) == null) {
                    return null;
                }
                String startDate = getRateDate(rateTable,aLRPolSchema);
                String occType = aLRPolSchema.getOccupationType();
               //由于系统中存在职业类别为空的客户，经过与再保讨论，这种情况下默认为职业类别1
                if (occType == null || occType.equals("")|| occType.equals("0")) {
                    occType = "1";
                }
                String manageCom = aLRPolSchema.getManageCom();

                tTransferData.setNameAndValue("ManageCom", manageCom);
                tTransferData.setNameAndValue("StartDate", startDate);
                tTransferData.setNameAndValue("OccType", occType);
                tTransferData.setNameAndValue("Sex", null);
                tTransferData.setNameAndValue("Age", null);
            } else if (tRiskCode.equals("2301") || tRiskCode.equals("2401") ||
                       tRiskCode.equals("2601") || tRiskCode.equals("2602") ||
                      tRiskCode.equals("230201")||tRiskCode.equals("240201") ||
                      tRiskCode.equals("260301")||tRiskCode.equals("260401")|| 
                      tRiskCode.equals("240301")||tGetDutyCode.equals("229201")|| 
                      tGetDutyCode.equals("234201")||tGetDutyCode.equals("229203")
                      ||tGetDutyCode.equals("229205")||tGetDutyCode.equals("234203")
                      ||tGetDutyCode.equals("234205")||tRiskCode.equals("230601")
                      ||tGetDutyCode.equals("250201")||tGetDutyCode.equals("250204")
                      ||tGetDutyCode.equals("237201")||tGetDutyCode.equals("237204")
                      ||tRiskCode.equals("530206")||tRiskCode.equals("530207")
                      ||tGetDutyCode.equals("284201")||tGetDutyCode.equals("284202")
                      ||tGetDutyCode.equals("270201")||tGetDutyCode.equals("270202")||tRiskCode.equals("280101")
                      ||tRiskCode.equals("560201")||tRiskCode.equals("560801")) {
                if (getRateDate(rateTable,aLRPolSchema) == null) {
                    System.out.println("获取费率表日期参数失败！");
                    return null;
                }
                String manageCom = aLRPolSchema.getManageCom();
                String startDate = getRateDate(rateTable,aLRPolSchema);
                String sex = aLRPolSchema.getInsuredSex();
                String age = PubFun.calInterval(aLRPolSchema.getInsuredBirthday(),
                                                aLRPolSchema.getGetDataDate(), "Y") +
                             "";//这里考虑到提数日期的延迟性，采用应分保日期来计算被保人年龄。modified by huxl @ 2007-12-25
                tTransferData.setNameAndValue("ManageCom", manageCom);
                tTransferData.setNameAndValue("StartDate", startDate);
                tTransferData.setNameAndValue("Sex", sex);
                tTransferData.setNameAndValue("Age", age);
                tTransferData.setNameAndValue("OccType", null);
            } 

            else if (tRiskCode.equals("540101")) {
                if (getRateDate(rateTable,aLRPolSchema) == null) {
                    return null;
                }
                String startDate = getRateDate(rateTable,aLRPolSchema);
                
               
                String manageCom = aLRPolSchema.getManageCom();
                String age = PubFun.calInterval(aLRPolSchema.getInsuredBirthday(),
                        aLRPolSchema.getGetDataDate(), "Y") +
     "";
                tTransferData.setNameAndValue("ManageCom", manageCom);
                tTransferData.setNameAndValue("StartDate", startDate);
//                tTransferData.setNameAndValue("OccType", null);
//                tTransferData.setNameAndValue("Sex", null);
                tTransferData.setNameAndValue("Age", age);
            }
            else if (tRiskCode.equals("230801")||tRiskCode.equals("230901")) {
                if (getRateDate(rateTable,aLRPolSchema) == null) {
                    return null;
                }
                String startDate = getRateDate(rateTable,aLRPolSchema);
                String manageCom = aLRPolSchema.getManageCom();
                String age = PubFun.calInterval(aLRPolSchema.getInsuredBirthday(),
                        aLRPolSchema.getGetDataDate(), "Y") +
     "";
                String sex = aLRPolSchema.getInsuredSex();
                tTransferData.setNameAndValue("ManageCom", manageCom);
                tTransferData.setNameAndValue("StartDate", startDate);
                tTransferData.setNameAndValue("OccType", null);
                tTransferData.setNameAndValue("Sex", sex);
                tTransferData.setNameAndValue("Age", age);
            }
            else if (tGetDutyCode.equals("380202"))
            {
            	 String manageCom = aLRPolSchema.getManageCom();
                 String startDate = getRateDate(rateTable,aLRPolSchema);
                 String sex = aLRPolSchema.getInsuredSex();
                 String age = PubFun.calInterval(aLRPolSchema.getInsuredBirthday(),
                                                 aLRPolSchema.getGetDataDate(), "Y") +
                              "";//这里考虑到提数日期的延迟性，采用应分保日期来计算被保人年龄。modified by huxl @ 2007-12-25
                 tTransferData.setNameAndValue("ManageCom", manageCom);
                 tTransferData.setNameAndValue("StartDate", startDate);
                 tTransferData.setNameAndValue("Sex", sex);
                 tTransferData.setNameAndValue("Age", age);
            }
            else if (tGetDutyCode.equals("380203")||tGetDutyCode.equals("380204")||tGetDutyCode.equals("380205"))
            {
            	 String manageCom = aLRPolSchema.getManageCom();
                 String startDate = getRateDate(rateTable,aLRPolSchema);
                 String sex = aLRPolSchema.getInsuredSex();
                 String age = PubFun.calInterval(aLRPolSchema.getInsuredBirthday(),
                                                 aLRPolSchema.getGetDataDate(), "Y") +
                              "";//这里考虑到提数日期的延迟性，采用应分保日期来计算被保人年龄。modified by huxl @ 2007-12-25
                 tTransferData.setNameAndValue("ManageCom", manageCom);
                 tTransferData.setNameAndValue("StartDate", startDate);
                 tTransferData.setNameAndValue("Sex", sex);
                 tTransferData.setNameAndValue("Age", age);
            }
           else {
                return null;
            }
            return tTransferData;
    }

    /**
    * 得到适用的费率表的生效日期
    * 参数: LCPolSchema对象
    * @return boolean
    */
   private String getRateDate(String calCode,LRPolSchema aLRPolSchema) {
       String rateTable = calCode;
       if(rateTable==null ||rateTable.equals("")){
           System.out.print("getRateDate 中参数错误");
           return null;
       }
       String tSql = "select distinct StartDate From " + rateTable +
                     " where Managecom='" + aLRPolSchema.getManageCom() + "'"
                     + " order by StartDate desc with ur "; //起始日期从大到小排列
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL(); //用于sql查询
       tSSRS = tExeSQL.execSQL(tSql);
       if (tSSRS.getMaxRow() <= 0) {
           System.out.println("保单：" + aLRPolSchema.getPolNo() +
                              " 没有对应再保合同生效日期！");
           return null;
       }
       String[][] tempRDate = tSSRS.getAllData(); //所有再保合同生效日期
       String contDate = null;
       //根据保单提数日期和费率表的生效日期，得到该保单适用的再保费率的起始日期
       for (int j = 0; j < tempRDate.length; j++) {
           FDate chgdate = new FDate();
           Date rateDate = chgdate.getDate(tempRDate[j][0]);
           Date getDataDate = chgdate.getDate(aLRPolSchema.getGetDataDate());
           if (rateDate.compareTo(getDataDate) <= 0) {
               contDate = tempRDate[j][0];
               break;
           }
       }

       return contDate;
   }

    private void buildError(String szFunc, String szErrMsg) {
           CError cError = new CError();
           cError.moduleName = "CalRiskAmntBL";
           cError.functionName = szFunc;
           cError.errorMessage = szErrMsg;
           this.mErrors.addOneError(cError);
    }
}
