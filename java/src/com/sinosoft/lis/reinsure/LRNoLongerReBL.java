package com.sinosoft.lis.reinsure;

import java.util.Date;

import com.sinosoft.lis.db.LRNoLongerContDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRCessListSchema;
import com.sinosoft.lis.schema.LRCessVerifySchema;
import com.sinosoft.lis.schema.LRErrorLogSchema;
import com.sinosoft.lis.schema.LRNoLongerContSchema;
import com.sinosoft.lis.schema.LRPolEdorSchema;
import com.sinosoft.lis.vschema.LRCessListSet;
import com.sinosoft.lis.vschema.LRNoLongerContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: LRClaimVeriBL </p>
 * <p>Description: 提取理赔数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 
 * @CreateDate：2008-10-30
 */
public class LRNoLongerReBL {
	


		    /** 错误处理类，每个需要错误处理的类中都放置该类 */
		    public CErrors mErrors = new CErrors();

		    /** 前台传入的公共变量 */
		    private GlobalInput globalInput = new GlobalInput();


		    /** 往后面传输数据的容器 */
//		    private VData mInputData = new VData();

		    /** 数据操作字符串 */
		    private String strOperate = "";
		    private String mToday = "";
		    private String mRecontCode = "";
		    private String mContno = "";
		    private String mRiskCode = "";
		    private String mStandby1 = "";
		    private String mOperateType = "";

		    private TransferData mGetCessData = new TransferData();

		    private MMap tmap = new MMap();
		    private VData mOutputData = new VData();
		    private LRCessListSet mLRCessListSet = new LRCessListSet();


		    //业务处理相关变量
		    /** 全局数据 */

		    public LRNoLongerReBL() {
		    }

		    /**
		     * 提交数据处理方法
		     * @param cInputData 传入的数据,VData对象
		     * @param cOperate 数据操作字符串
		     * @return 布尔值（true--提交成功, false--提交失败）
		     */
		    public boolean submitData(VData cInputData, String cOperate) {
		        System.out.println("Begin LRCessListBL.Submit..............");
		       
		        if (!getInputData(cInputData)) {
		            return false;
		        }
		        if("Save".equals(mOperateType)){
		        	
		        	if (!dealData()) {
			            return false;
			        }
			        //准备往后台的数据
			        if (!prepareOutputData())
			        {
			            return false;
			        }
		        	
		        }
		        
		         if("ImpDelete".equals(mOperateType)){

		        	
		        	 try{  
			                if (!deleteCessData()) {
			                    buildError("deleteCessData", "删除数据出错");
			                    return false;
			                }
			             
			        }catch(Exception e){
			                e.printStackTrace();
			        }
		        	
		        
		        	
		        }

			        PubSubmit tPubSubmit = new PubSubmit();
			        tPubSubmit.submitData(mOutputData, "");
			        //如果有需要处理的错误，则返回
			        if (tPubSubmit.mErrors.needDealError())
			        {
			            // @@错误处理
			            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			            CError tError = new CError();
			            tError.moduleName = "LRNoLongerReBL";
			            tError.functionName = "submitDat";
			            tError.errorMessage = "数据提交失败!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
		       
		        
		        return true;
		    }


		    /**
		     * 根据前面的输入数据，进行UI逻辑处理
		     * 如果在处理过程中出错，则返回false,否则返回true
		     */
		    private boolean dealData() {

		       
		    	 try{	
		                if (!insertData()) {
		                    buildError("insertData", "插入数据出错");
		                    return false;
		                }
		                    
		            
		            
		        }catch(Exception e){
		                e.printStackTrace();
		        }
		        
		        return true;
		    }

		
		    
		     /**
		     * 准备后台的数据
		     * @return boolean
		     */
		    private boolean prepareOutputData()
		    {
		        try
		        {
//		        	tmap.put(this.mLRCessListSet, "INSERT");
		        	this.mOutputData.clear();
		            this.mOutputData.add(tmap);
		        }
		        catch (Exception ex)
		        {
		            // @@错误处理
		            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
		            return false;
		        }
		        return true;
		    }
		    //执行SQL,并将取出的数据插入到:
		    private boolean deleteCessData(){
		    	
		    	
		    	LRNoLongerContDB tLRNoLongerContDB = new LRNoLongerContDB();
		    	LRNoLongerContSet tLRNoLongerContSet = new LRNoLongerContSet();
		    	String NoLongerSql = "select * from LRNoLongerCont where Standby1='01' with ur" ;
		    	tLRNoLongerContSet = tLRNoLongerContDB.executeQuery(NoLongerSql);
		    	if (tLRNoLongerContSet == null || tLRNoLongerContSet.size() <= 0) {
		    		CError.buildErr(this,"已不存在导入未删除再保数据的记录。");
		            return false;
		        }
		    	for (int j = 1; j <= tLRNoLongerContSet.size(); j++) {
		    		
		    		LRNoLongerContSchema tLRNoLongerContSchema = new LRNoLongerContSchema();
		    		tLRNoLongerContSchema = tLRNoLongerContSet.get(j);
		    		String ContNo = " GrpContno " ;	
		    		String tContNo = tLRNoLongerContSchema.getContNo() ;
		    		String tRecontCode = tLRNoLongerContSchema.getReContCode() ;
			    	ExeSQL tExeSQL = new ExeSQL();
			    	SSRS tSSRS = new SSRS();
			    	String contTypeSql = "select 1 from lcgrpcont where grpcontno = '"+tContNo+"'" +
			    							" union all select 1 from lbgrpcont where grpcontno = '"+tContNo+"'  fetch first 1 rows only with ur" ;
			    	tSSRS = tExeSQL.execSQL(contTypeSql);
			    	if(tSSRS==null||tSSRS.getMaxRow()<1){
			    		ContNo = " Contno " ;
			    	}
	        		tExeSQL.execUpdateSQL("delete from LRCessList where "+ContNo+" = '"+tContNo+"' " +
	        								"and recontcode = '"+tRecontCode+"' with ur " 
	            		);
	        		System.out.println("LRCessList表删除");
	        		
	        		tExeSQL.execUpdateSQL("delete from LRClaimList where "+ContNo+" = '"+tContNo+"' " +
											"and recontcode = '"+tRecontCode+"' with ur " 
	        								);
	        		System.out.println("LRClaimList表删除");
	        		
	        		tExeSQL.execUpdateSQL("delete from LRPolResult a where exists " +
	        						"(select 1 from lrpol where polno=a.polno and " +
	        							""+ContNo+" = '"+tContNo+"' " +
	        							"and recontcode = '"+tRecontCode+"') " +
	        									" and recontcode = '"+tRecontCode+"' with ur " 
							);
	        		System.out.println("LRPolResult表删除");

	        		tExeSQL.execUpdateSQL("delete from LRPolEdorResult a where exists " +
							"(select 1 from lrpoledor where polno=a.polno and " +
								""+ContNo+" = '"+tContNo+"' " +
								"and recontcode = '"+tRecontCode+"') " +
										" and recontcode = '"+tRecontCode+"' with ur " 
					);
	        		System.out.println("LRPolEdorResult表删除");

	        		tExeSQL.execUpdateSQL("delete from LRPolClmResult a where exists " +
							"(select 1 from lrpolclm where polno=a.polno and " +
								""+ContNo+" = '"+tContNo+"' " +
								"and recontcode = '"+tRecontCode+"') " +
										" and recontcode = '"+tRecontCode+"' with ur " 
					);
	        		System.out.println("LRPolClmResult表删除");
	        		
	        		tExeSQL.execUpdateSQL("delete from LRPol where "+ContNo+" = '"+tContNo+"' " +
							"and recontcode = '"+tRecontCode+"' with ur " 
							);
	        		System.out.println("LRPol表删除");

	        		tExeSQL.execUpdateSQL("delete from LRPolEdor where "+ContNo+" = '"+tContNo+"' " +
							"and recontcode = '"+tRecontCode+"' with ur " 
							);
	        		System.out.println("LRPolEdor表删除");
	        		
	        		tExeSQL.execUpdateSQL("delete from LRPolClm where "+ContNo+" = '"+tContNo+"' " +
							"and recontcode = '"+tRecontCode+"' with ur " 
							);
	        		System.out.println("LRPolClm表删除");
	        		
	        		tExeSQL.execUpdateSQL("update  LRNoLongerCont set standby1='02' where contno = '"+tContNo+"' " +
							"and recontcode = '"+tRecontCode+"' with ur " 
							);
	        		System.out.println("删除状态更新");
		    	}
		    	
                
          
		    	return true;
		    }
		    
		    //错误信息
		    private void buildError(String szFunc, String szErrMsg) {
		        CError cError = new CError();
		        cError.moduleName = "LRCessListBL";
		        cError.functionName = szFunc;
		        cError.errorMessage = szErrMsg;
		        this.mErrors.addOneError(cError);
		    }

		    private boolean insertData(){
		    	try{
		        		
		    			LRNoLongerContSchema tLRNoLongerContSchema = new LRNoLongerContSchema();
		    			
		        		tLRNoLongerContSchema.setRiskCode(mRiskCode); 
		    			tLRNoLongerContSchema.setContNo(mContno);
		    			tLRNoLongerContSchema.setReContCode(mRecontCode);
		    			tLRNoLongerContSchema.setStandby1(mStandby1);
		        		
		    			tLRNoLongerContSchema.setMakeDate(PubFun.getCurrentDate()); 
		    			tLRNoLongerContSchema.setMakeTime(PubFun.getCurrentTime());  
		    			tLRNoLongerContSchema.setModifyDate(PubFun.getCurrentDate());  
		    			tLRNoLongerContSchema.setModifyTime(PubFun.getCurrentTime());  
		    			tLRNoLongerContSchema.setOprater(globalInput.Operator);  
		        		
		        		
		        		tmap.put(tLRNoLongerContSchema, "DELETE&INSERT");

		        	
		        	
		        	
		     		
		    		 
		        	
		    	}catch(Exception ex){
		    		ex.printStackTrace();
		    		
			    					    	
		    		return false;
		    	}
		    	return true;
		    }
		    
		    
//		    private boolean updateData(){}
		    private boolean getInputData(VData cInputData) {
		        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
		                "GlobalInput", 0));
		        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
		                "TransferData", 0);
		        mRecontCode = (String) mGetCessData.getValueByName("RecontCode");
		        mContno = (String) mGetCessData.getValueByName("Contno");
		        mRiskCode = (String) mGetCessData.getValueByName("RiskCode");
		        mStandby1 = (String) mGetCessData.getValueByName("Standby1");
		        mOperateType = (String) mGetCessData.getValueByName("OperateType");
		       
		        
		        return true;
		    }
		    /**
		     * 从输入数据中得到所有对象
		     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		     */

		    public String getResult() {
		        return "没有传数据";
		    }
		    
		    /**
		     * 记录错误日志
		     * @return boolean
		     */
		    private boolean errorLog(String calCode, String errorInf) {
		        LRErrorLogSchema tLRErrorLogSchema = new
		                                             LRErrorLogSchema();
		        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
		        tLRErrorLogSchema.setSerialNo(serialNo);
		        tLRErrorLogSchema.setPolNo(calCode);
		        tLRErrorLogSchema.setLogType("C"); //C:分保校验sql执行失败
		        tLRErrorLogSchema.setErrorInfo(errorInf);
		        tLRErrorLogSchema.setOperator(globalInput.Operator);
		        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
		        tLRErrorLogSchema.setMakeDate(PubFun.getCurrentDate());
		        tLRErrorLogSchema.setMakeTime(PubFun.getCurrentTime());
		        tLRErrorLogSchema.setModifyDate(PubFun.getCurrentDate());
		        tLRErrorLogSchema.setModifyTime(PubFun.getCurrentTime());

		        tmap.put(tLRErrorLogSchema, "DELETE&INSERT");
		        return true;
		    }
		    public static void main(String[] args) {
		        LRNewGetCessDataBL tLRNewGetCessDataBL = new LRNewGetCessDataBL();
		        GlobalInput globalInput = new GlobalInput();
		        globalInput.ManageCom = "86";
		        globalInput.Operator = "rm0002";
		        String mOperateType = "CESSDATA";

		        String startDate = "2008-10-10";
		        String endDate = "2008-10-10";
		        TransferData getCessData = new TransferData();
		        getCessData.setNameAndValue("StartDate", startDate);
		        getCessData.setNameAndValue("EndDate", endDate);

		        VData tVData = new VData();
		        tVData.addElement(globalInput);
		        tVData.addElement(getCessData);

		        tLRNewGetCessDataBL.submitData(tVData, mOperateType);
		    }
}
