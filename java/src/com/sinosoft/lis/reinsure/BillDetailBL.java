package com.sinosoft.lis.reinsure;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.sinosoft.lis.db.LRAccountsDB;
import com.sinosoft.lis.db.LRPolResultDB;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRAccountsSchema;
import com.sinosoft.lis.schema.LRPolResultSchema;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.vschema.LRAccountsSet;
import com.sinosoft.lis.vschema.LRPolResultSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class BillDetailBL {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
  

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();
    private LRPolSchema mLRPolSchema=new LRPolSchema();
    private LRAccountsSchema mLRAccountsSchema = new LRAccountsSchema();

    private FileWriter mFileWriter ;
    private BufferedWriter mBufferedWriter ;
    private ArrayList mArrayList = new ArrayList();
    //业务处理相关变量
    /** 全局数据 */

    public BillDetailBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }
    
    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BillDetailBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

        if (this.strOperate.equals("UPDATE")) {//修改分保信息
        	System.out.println("成功进入更新*********");
            if(update()==false){
                buildError("update()","修改失败");
                return false;
            }
        }else if(this.strOperate.equals("CHECK")){//审核通过，修改状态
            if(check()==false){
                buildError("check()","审核失败");
                return false;
            }
        }else if(this.strOperate.equals("ONLOAD")){//下载清单
            if(onload()==false){
                buildError("onload()","下载清单失败");
                return false;
            }
        }
        
        prepareOutputData();
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, strOperate))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "BillDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            mErrors.addOneError(tError);
            return false;
        }
        
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLRPolSchema = (LRPolSchema)cInputData.getObjectByObjectName(
                "LRPolSchema", 0);
        mLRAccountsSchema=(LRAccountsSchema)cInputData.
        				getObjectByObjectName("LRAccountsSchema", 0);
        mArrayList = (ArrayList)mTransferData.getValueByName("arrayList");
        
        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
    
    /**
     * 修改分保信息
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean update(){
    	LRPolResultDB tLRPolResultDB=new LRPolResultDB();
    	tLRPolResultDB.setPolNo(mLRPolSchema.getPolNo());
    	tLRPolResultDB.setReNewCount(mLRPolSchema.getReNewCount());
    	tLRPolResultDB.setReContCode(mLRPolSchema.getReContCode());
    	tLRPolResultDB.setReinsureItem(mLRPolSchema.getReinsureItem());
        
    	LRPolResultSet tLRPolResultSet=tLRPolResultDB.query();
        if(tLRPolResultSet.size()<1){
        	CError.buildErr(this, "LRPolResult数据获取失败！");
        	return false;
        }
        LRPolResultSchema tempLRPolResultSchema=tLRPolResultSet.get(1);
        //CessionAmount CessPrem sum
        tempLRPolResultSchema.setCessionAmount(mTransferData.getValueByName("CessionAmount").toString());
        System.out.println(mTransferData.getValueByName("CessionAmount").toString()+"啥啥啥");
        tempLRPolResultSchema.setCessPrem(mLRPolSchema.getCessAmnt());
        tempLRPolResultSchema.setReProcFee(mTransferData.getValueByName("sum").toString());
        tempLRPolResultSchema.setOperator(globalInput.Operator);
        tempLRPolResultSchema.setModifyDate(PubFun.getCurrentDate());
        tempLRPolResultSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tempLRPolResultSchema, "UPDATE");
        return true;
    }

    /**
     * 审核通过，修改状态
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean check(){
    	 String tReComCode = (String) mTransferData.getValueByName("ReComCode");
         String tCessionMode = (String) mTransferData.getValueByName("CessionMode");
         String tDiskKind = (String) mTransferData.getValueByName("DiskKind");
         String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
         String tContNo = (String) mTransferData.getValueByName("ContNo");
         String tAppntName = (String) mTransferData.getValueByName("AppntName");
         String tInsuredName = (String) mTransferData.getValueByName("InsuredName");
         String tReContCode = (String)mTransferData.getValueByName("ReContCode");
         String tActuGetState = (String)mTransferData.getValueByName("ActuGetState");
         String tYear = (String) mTransferData.getValueByName("Year");
         String tMonth = (String) mTransferData.getValueByName("Month");
         String mRecontCode = mArrayList.toString();
         if(!StrTool.cTrim(mRecontCode).equals("[]")){
         	
         	mRecontCode=StrTool.replace(mRecontCode,"[", "'");
         	mRecontCode=StrTool.replace(mRecontCode,"]", "'");
         	mRecontCode=StrTool.replace(mRecontCode,", ", "','");
         	System.out.println(mRecontCode);
         }
         String tStartDate="";//起始日期
         String tEndDate="";//终止日期
         if(!StrTool.cTrim(tYear).equals("") && !StrTool.cTrim(tMonth).equals("")){
            Calendar calendar=Calendar.getInstance();
            calendar.set(Calendar.YEAR,Integer.parseInt(tYear));
            calendar.set(Calendar.MONTH,Integer.parseInt(tMonth));
            calendar.set(Calendar.DATE,1);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            String tCurrentDate = sdf.format(calendar.getTime());
            calendar.set(Calendar.MONTH,Integer.parseInt(tMonth)-1);
            if("12".equals(tMonth))
         	   tStartDate=tYear+"-"+tMonth+"-1";
            else
         	   tStartDate=sdf.format(calendar.getTime());
            
            tEndDate= PubFun.calDate(tCurrentDate,-1,"D","2009-1-31");  //结束日期取本月最后一天
        }

        String strWhere="";
        String strWhereT="";
        if(!StrTool.cTrim(tReComCode).equals("")){
            strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='"+tReComCode+"') ";
            strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='"+tReComCode+"') ";
        }
        if(!StrTool.cTrim(tCessionMode).equals("")){
            strWhere +=" and a.RiskCalSort='"+tCessionMode+"' ";
            strWhereT +=" and a.RiskCalSort='"+tCessionMode+"' ";
        }
        if(!StrTool.cTrim(tDiskKind).equals("")){
            strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+tDiskKind+"') ";
            strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+tDiskKind+"') ";
        }
        if(!StrTool.cTrim(tYear).equals("")){
            strWhere+=" and a.GetDataDate between '"+tStartDate+"' and '"+tEndDate+"' ";
            strWhereT += " and a.GetDataDate between '"+tStartDate+"' and '"+tEndDate+"' ";
        }
        if(!StrTool.cTrim(tRiskCode).equals("")){
            strWhere+=" and a.riskcode='"+tRiskCode+"' ";
            strWhereT +=" and a.riskcode='"+tRiskCode+"' ";
        }
        if(!StrTool.cTrim(tContNo).equals("")){
            strWhere+=" and a.ContNo='"+tContNo+"' ";
            strWhereT +=" and a.ContNo='"+tContNo+"' ";
        }
        if(!StrTool.cTrim(tAppntName).equals("")){
            strWhere+=" and a.AppntName like '%"+tAppntName+"%' ";
            strWhereT +=" and a.AppntName like '%"+tAppntName+"%' ";
        }
        if(!StrTool.cTrim(tInsuredName).equals("")){
            strWhere+=" and a.InsuredName like '%"+tInsuredName+"%' ";
            strWhereT +=" and a.InsuredName like '%"+tInsuredName+"%' ";
        }
        if(!StrTool.cTrim(tReContCode).equals("")){
             strWhere+=" and a.ReContCode ='"+tReContCode+"' ";
             strWhereT +=" and a.ReContCode ='"+tReContCode+"' ";
         }
        if(!StrTool.cTrim(mRecontCode).equals("[]")){
            strWhere+=" and a.ReContCode in ("+mRecontCode+") ";
            strWhereT +=" and a.ReContCode in ("+tReContCode+") ";
        }
         if(!StrTool.cTrim(tActuGetState).equals("")){
             strWhere+=" and a.ActuGetState ='"+tActuGetState+"' ";
             strWhereT +=" and a.ActuGetState ='"+tActuGetState+"' ";
         }


        String strSQL="select count(a.polno) "
                    +"from LRPol a,LRPolResult c "
                    +"where a.polno=c.polno  and a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                    +"and a.ReinsureItem=c.ReinsureItem  "
                    + strWhere;
       if(!StrTool.cTrim(tReComCode).equals("")||!StrTool.cTrim(tDiskKind).equals("")){
           //如果机构号和险种类型不为空，则为临时分保单独写一个sql;
           strSQL +=" union select count(a.polno) "
                    +"from LRPol a,LRPolResult c "
                    +"where a.polno=c.polno  and "
                    +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                    +"and a.ReinsureItem=c.ReinsureItem "
                    + strWhereT;
       }
       strSQL +=" with ur";
       
       //保全摊回
       String strESQL = "select count(a.polno) "
           +"from LRPol b, LRPolEdor a,LRPolEdorResult c "
           +" where b.polno = a.polno and a.polno = c.polno "
           +"and b.RecontCode = a.RecontCode and a.RecontCode = c.RecontCode "
           +"  and a.ReNewCount = c.ReNewCount and b.reinsureitem=a.reinsureitem  "
           + strWhere;
       if (!StrTool.cTrim(tReComCode).equals("") ||
    		   !StrTool.cTrim(tDiskKind).equals("")) {
    	   strESQL += " union all select count(a.polno) "
    		   		+"from LRPol b, LRPolEdor a,LRPolEdorResult c "
    		   		+" where b.polno = a.polno and a.polno = c.polno  "
    		   		+"and b.RecontCode = a.RecontCode and a.RecontCode = c.RecontCode "
    		   		+" and a.ReNewCount = c.ReNewCount and b.reinsureitem = a.reinsureitem "
    		   		+ strWhereT;
}
       strESQL += " with ur";

//        LRPolSet tLRPolSet = new LRPolSet();
//        RSWrapper rswrapper = new RSWrapper();
//        rswrapper.prepareData(tLRPolSet, strSQL);
       String lrscount = new ExeSQL().getOneValue(strSQL);
       int lrcount = Integer.parseInt(lrscount); 
       String lrescount = new ExeSQL().getOneValue(strESQL);
       int lrecount = Integer.parseInt(lrescount); 
        try {
             if(lrcount>0){
                  //rswrapper.getData();
                 // mMap=new MMap();
//                  for(int j=1;j<=tLRPolSet.size();j++){
//                    LRPolSchema tLRPolSchema=tLRPolSet.get(j);
//                    tLRPolSchema.setActuGetState("01");
//                    tLRPolSchema.setModifyDate(PubFun.getCurrentDate());
//                    tLRPolSchema.setModifyTime(PubFun.getCurrentTime());
//                    this.mMap.put(tLRPolSchema, "UPDATE");
//                  }
            	 String updateSQL1=" update "
  									+" LRPol a set a.ActuGetState='01'," +
  									" a.ModifyDate='"+PubFun.getCurrentDate()+"'," +
  									"a.ModifyTime='"+PubFun.getCurrentTime()+"' "
  									+"where 1=1 "
  									+ strWhere;
            	 ExeSQL tExeSQL = new ExeSQL();
        		 tExeSQL.execUpdateSQL(updateSQL1);
            	 if(!StrTool.cTrim(tReComCode).equals("")||!StrTool.cTrim(tDiskKind).equals("")){
            		 //如果机构号和险种类型不为空，则为临时分保单独写一个sql;
            		 String updateSQL2 =" update "
                     					+" LRPol a set a.ActuGetState='01'," +
                     					" a.ModifyDate='"+PubFun.getCurrentDate()+"'," +
                     					"a.ModifyTime='"+PubFun.getCurrentTime()+"' "
                     					+"where 1=1"
                     					+ strWhereT;
            		 
            		 tExeSQL.execUpdateSQL(updateSQL2);
            	 }
        }
             if(lrecount>0){
            	 String updateSQL1=" update "
						+" LRPolEdor a set a.ActuGetState='01'," +
						" a.ModifyDate='"+PubFun.getCurrentDate()+"'," +
						"a.ModifyTime='"+PubFun.getCurrentTime()+"' "
						+"where 1=1 "
						+ strWhere;
            	 ExeSQL tExeSQL = new ExeSQL();
            	 tExeSQL.execUpdateSQL(updateSQL1);
            	 if(!StrTool.cTrim(tReComCode).equals("")||!StrTool.cTrim(tDiskKind).equals("")){
            		 //如果机构号和险种类型不为空，则为临时分保单独写一个sql;
            		 String updateSQL2 =" update "
      					+" LRPolEdor a set a.ActuGetState='01'," +
      					" a.ModifyDate='"+PubFun.getCurrentDate()+"'," +
      					"a.ModifyTime='"+PubFun.getCurrentTime()+"' "
      					+"where 1=1"
      					+ strWhereT;
		 
            		 tExeSQL.execUpdateSQL(updateSQL2);
            	 }
             }
                  if (!prepareOutputData()) {
                     return false;
                  }
                 PubSubmit tPubSubmit = new PubSubmit();
                 if (!tPubSubmit.submitData(mInputData, "")) {
                     if (tPubSubmit.mErrors.needDealError()) {
                         buildError("submitData", "保存再保计算清单审核结果出错!");
                     }
                     return false;
                 }
              
                 //rswrapper.close();
            } catch (Exception ex) {
                 ex.printStackTrace();
                 //rswrapper.close();
             }
         return true;
    }

    private boolean onload(){
        String tRecontCode = (String) mTransferData.getValueByName("RecontCode");
        String tAppntName = (String) mTransferData.getValueByName("AppntName");
        String tInsuredName = (String) mTransferData.getValueByName("InsuredName");
        String tYear = (String) mTransferData.getValueByName("Year");
        String tMonth = (String) mTransferData.getValueByName("Month");
        String tTempCessFlag = (String) mTransferData.getValueByName("TempCessFlag");
        String tDiskKind = (String) mTransferData.getValueByName("DiskKind");
        String tRiskCalSort = (String) mTransferData.getValueByName("RiskCalSort");
        String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
        String tContNo = (String)mTransferData.getValueByName("ContNo");
        String tReinsureCom = (String)mTransferData.getValueByName("ReinsureCom");
        String mRecontCode = mArrayList.toString();
        if(!StrTool.cTrim(mRecontCode).equals("[]")){
        	
        	mRecontCode=StrTool.replace(mRecontCode,"[", "'");
        	mRecontCode=StrTool.replace(mRecontCode,"]", "'");
        	mRecontCode=StrTool.replace(mRecontCode,", ", "','");
        	System.out.println(mRecontCode);
        }
        String tStartDate="";//起始日期
        String tEndDate="";//终止日期
        if(!StrTool.cTrim(tYear).equals("") && !StrTool.cTrim(tMonth).equals("")){
           Calendar calendar=Calendar.getInstance();
           calendar.set(Calendar.YEAR,Integer.parseInt(tYear));
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth));
           calendar.set(Calendar.DATE,1);
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           String tCurrentDate = sdf.format(calendar.getTime());
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth)-1);
           if("12".equals(tMonth))
        	   tStartDate=tYear+"-"+tMonth+"-1";
           else
        	   tStartDate=sdf.format(calendar.getTime());
           tEndDate= PubFun.calDate(tCurrentDate,-1,"D","2009-1-31");  //结束日期取本月最后一天
       }

       String tSysPath = (String) mTransferData.getValueByName("SysPath"); //获取地址;
       tSysPath +="/reinsure/";
       String tPath="";
       try{
            tPath = "QingDanMingXi"+PubFun.getCurrentDate2() + "_" + PubFun.getCurrentTime2();
            mFileWriter = new FileWriter(tSysPath+tPath+".txt");
            mBufferedWriter = new BufferedWriter(mFileWriter);

            String[] strArrHead = new String[26];
            
            //机构代码、产品代码、保单号、生效日期、失效日期、增减人日期、签单

            //日期/保全日期、投保人、投保人客户号、被保险人（无名单显示总人数

           // ）、出生日期、性别、身份证号码、职业类别、核保加费率、限额/保额

            //、标准保费、实收保费、折扣率、分保类型、分出比例、分出保额、分出

            //
            //保额、分保手续费
            strArrHead[0] = "机构代码";
            strArrHead[1] = "再保合同号";
            strArrHead[2] = "产品代码";
            strArrHead[3] = "保单号";
            strArrHead[4] = "生效日期";
            strArrHead[5] = "失效日期";
            strArrHead[6] = "增减人日期";
            strArrHead[7] = "签单日期/保全日期";
            strArrHead[8] = "投保人";
            strArrHead[9] = "投保人客户号";
            strArrHead[10] = "被保险人";
            strArrHead[11] = "出生日期";
            strArrHead[12] = "性别";
            strArrHead[13] = "身份证号码";
            strArrHead[14] = "职业类别";
            strArrHead[15] = "核保加费率";
            strArrHead[16] = "限额/保额";
            strArrHead[17] = "标准保费";
            strArrHead[18] = "实收保费";   
            strArrHead[19] = "折扣率"; 
            strArrHead[20] = "分保类型";
            strArrHead[21] = "分出比例";

            strArrHead[22] = "分出保额";
            strArrHead[23] = "分出保费/退保金额";
            strArrHead[24] = "分保手续费/退回手续费";
            strArrHead[25] = "清单时间";

            String head = "";
            for(int m=0;m<strArrHead.length;m++){
                        head += strArrHead[m]+";";
            }
            mBufferedWriter.write(head+"\r\n");
            mBufferedWriter.flush();
        }catch(Exception ex1){
            ex1.printStackTrace();
        }

        String strWhere="";
        String strWhereT="";

        if(!StrTool.cTrim(tRiskCalSort).equals("")){
            strWhere +=" and a.RiskCalSort='"+tRiskCalSort+"' ";
//            strWhereT +=" and a.RiskCalSort='"+tCessionMode+"' ";
        }
        if(!StrTool.cTrim(tDiskKind).equals("")){//sql中暂不加这个条件
            strWhere+=" and (exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+tDiskKind+"') ";
            //strWhere +=" or exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+tDiskKind+"')) ";
        }
        if(!StrTool.cTrim(tYear).equals("")){
            strWhere+=" and a.GetDataDate between '"+tStartDate+"' and '"+tEndDate+"' ";
            strWhereT += " and a.GetDataDate between '"+tStartDate+"' and '"+tEndDate+"' ";
        }
        if(!StrTool.cTrim(tRiskCode).equals("")){
            strWhere+=" and a.riskcode='"+tRiskCode+"' ";
            strWhereT +=" and a.riskcode='"+tRiskCode+"' ";
        }
        if(!StrTool.cTrim(tContNo).equals("")){
            strWhere+=" and a.ContNo='"+tContNo+"' ";
            strWhereT +=" and a.tContNo='"+tContNo+"' ";
        }
        if(!StrTool.cTrim(tAppntName).equals("")){
            strWhere+=" and a.AppntName like '%"+tAppntName+"%' ";
//            strWhereT +=" and a.AppntName like '%"+tAppntName+"%' ";
        }
        if(!StrTool.cTrim(tInsuredName).equals("")){
            strWhere+=" and a.InsuredName like '%"+tInsuredName+"%' ";
//            strWhereT +=" and a.InsuredName like '%"+tInsuredName+"%' ";
        }
        if(!StrTool.cTrim(tReinsureCom).equals("")){
            strWhere+=" and a.ReinsureCom = '"+tReinsureCom+"' ";
//            strWhereT +=" and a.ReContCode = '"+tReContCode+"' ";
        }
        
        if(!StrTool.cTrim(tRecontCode).equals("")){
            strWhere+=" and a.ReContCode = '"+tRecontCode+"' ";
            strWhereT +=" and a.ReContCode = '"+tRecontCode+"' ";
        }
        if(!StrTool.cTrim(mRecontCode).equals("[]")){
            strWhere+=" and a.ReContCode in ("+mRecontCode+") ";
            strWhereT +=" and a.ReContCode in ("+mRecontCode+") ";
        }

        String strSQL="select a.ManageCom,a.recontcode,a.RiskCode,'''' || trim(a.ContNo) || '''',e.CValidate,e.CInvalidate,'',a.SignDate," +
                              "a.AppntName,'''' || trim(a.AppntNo) || '''',a.InsuredName,char(a.InsuredBirthday),'''' || trim(a.InsuredSex) || '''',(select '''' || trim(IdNo) || '''' from LDPerson where CustomerNo = a.InsuredNo), " +
                              "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,a.StandPrem,a.Prem, " +
                              "c.ChoiRebaFeeRate, " +
//                              "c.CessionAmount," +
                              "(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end),"+
                              "c.CessionRate,c.CessionAmount," +
                              "c.CessPrem," +
                              "c.ReProcFee,substr(replace('"+tStartDate+"','-',''),1,6) "
                   +"from LRPol a,LRPolResult c,LCCont e "
                   +"where c.cessprem>0 and a.contno=e.contno and a.conttype='1'  and a.polno=c.polno and "
                   +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                   
                   + strWhere
                   +"group by a.ManageCom,a.recontcode,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.SignDate,a.AppntName,a.AppntNo,a.InsuredName," +
                   		"a.InsuredBirthday,a.InsuredSex,a.InsuredNo,a.OccupationType,a.DiseaseAddFeeRate,a.Amnt,a.StandPrem,a.Prem,"+
                   		"c.ChoiRebaFeeRate,c.CessionAmount,a.TempCessFlag,c.CessionRate,c.CessionAmount,c.CessPrem," +
                   		"c.ReProcFee,a.polno "
                   +" union all "
                   +"select a.ManageCom,a.recontcode,a.RiskCode,'''' || trim(a.ContNo) || '''',e.CValidate,e.CInvalidate,'',a.SignDate," +
                              "a.AppntName,'''' || trim(a.AppntNo) || '''',a.InsuredName,char(a.InsuredBirthday),'''' || trim(a.InsuredSex) || '''',(select '''' || trim(IdNo) || '''' from LDPerson where CustomerNo = a.InsuredNo), " +
                              "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,a.StandPrem,a.Prem, " +
                              "c.ChoiRebaFeeRate, " +
//                              "c.CessionAmount," +
                              "(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end),"+
                              "c.CessionRate,c.CessionAmount," +
                              "c.CessPrem," +
                              "c.ReProcFee,substr(replace('"+tStartDate+"','-',''),1,6) "
                   +"from LRPol a,LRPolResult c,LCCont e "
                   +"where c.cessprem>0 and a.contno=e.contno and a.conttype='1'   and a.polno=c.polno and "
                   +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount  "
                   +"  and c.polno=(select e.polno from lbrnewstatelog e where e.newpolno=a.polno and e.renewcount=a.renewcount )  "
                   + strWhere
                   +"group by a.ManageCom,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.SignDate,a.AppntName,a.AppntNo,a.InsuredName," +
              		"a.InsuredBirthday,a.InsuredSex,a.InsuredNo,a.OccupationType,a.DiseaseAddFeeRate,a.Amnt,a.StandPrem,a.Prem,"+
              		"c.ChoiRebaFeeRate,c.CessionAmount,a.TempCessFlag,c.CessionRate,c.CessionAmount,c.CessPrem," +
              		"c.ReProcFee,a.polno,a.recontcode"
                   +" union all "  
                   +" select a.ManageCom,a.recontcode,a.RiskCode,'''' || trim(a.ContNo) || '''',e.CValidate,e.CInvalidate,'',a.SignDate," +
                              "a.AppntName,'''' || trim(a.AppntNo) || '''',a.InsuredName,char(a.InsuredBirthday),'''' || trim(a.InsuredSex) || '''',(select '''' || trim(IdNo) || '''' from LDPerson where CustomerNo = a.InsuredNo), " +
                              "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,a.StandPrem,a.Prem, " +
                              "c.ChoiRebaFeeRate, " +
//                              "c.CessionAmount," +
                              "(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end),"+
                              "c.CessionRate,c.CessionAmount," +
                              "c.CessPrem," +
                              "c.ReProcFee,substr(replace('"+tStartDate+"','-',''),1,6) "
        +"from LRPol a,LRPolResult c,LBCont e "
        +"where c.cessprem>0 and a.contno=e.contno and a.conttype='1'   and a.polno=c.polno and "
        +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
        
        + strWhere
        +"group by a.ManageCom,a.recontcode,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.SignDate,a.AppntName,a.AppntNo,a.InsuredName," +
   		"a.InsuredBirthday,a.InsuredSex,a.InsuredNo,a.OccupationType,a.DiseaseAddFeeRate,a.Amnt,a.StandPrem,a.Prem,"+
   		"c.ChoiRebaFeeRate,c.CessionAmount,a.TempCessFlag,c.CessionRate,c.CessionAmount,c.CessPrem," +
   		"c.ReProcFee,a.polno "
        +" union all "
        +" select a.ManageCom,a.recontcode,a.RiskCode,'''' || trim(a.ContNo) || '''',e.CValidate,e.CInvalidate,'',a.SignDate," +
                              "a.AppntName,'''' || trim(a.AppntNo) || '''',a.InsuredName,char(a.InsuredBirthday),'''' || trim(a.InsuredSex) || '''',(select '''' || trim(IdNo) || '''' from LDPerson where CustomerNo = a.InsuredNo), " +
                              "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,a.StandPrem,a.Prem, " +
                              "c.ChoiRebaFeeRate, " +
//                              "c.CessionAmount," +
                              "(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end),"+
                              "c.CessionRate,c.CessionAmount," +
                              "c.CessPrem," +
                              "c.ReProcFee,substr(replace('"+tStartDate+"','-',''),1,6) "
        +"from LRPol a,LRPolResult c,LBCont e "
        +"where c.cessprem>0 and a.contno=e.contno and a.conttype='1'   and a.polno=c.polno and "
        +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount   "
        +"  and c.polno=(select e.polno from lbrnewstatelog e where e.newpolno=a.polno and e.renewcount=a.renewcount )  "
        + strWhere
        +"group by a.ManageCom,a.recontcode,a.RiskCode,a.ContNo,e.CValidate,e.CInvalidate,a.SignDate,a.AppntName,a.AppntNo,a.InsuredName," +
   		"a.InsuredBirthday,a.InsuredSex,a.InsuredNo,a.OccupationType,a.DiseaseAddFeeRate,a.Amnt,a.StandPrem,a.Prem,"+
   		"c.ChoiRebaFeeRate,c.CessionAmount,a.TempCessFlag,c.CessionRate,c.CessionAmount,c.CessPrem," +
   		"c.ReProcFee,a.polno "
        +"union all " 
                   +" select a.ManageCom,a.recontcode,a.RiskCode,'''' || trim(a.GrpContNo) || '''',a.CValidate,a.enddate,'',a.SignDate," +
                              "a.AppntName,'''' || trim(a.AppntNo) || '''',a.InsuredName,char(a.InsuredBirthday),'''' || trim(a.InsuredSex) || '''',(select '''' || trim(IdNo) || '''' from LDPerson where CustomerNo = a.InsuredNo), " +
                              "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,a.StandPrem,a.Prem, " +
                              "c.ChoiRebaFeeRate, " +
//                              "c.CessionAmount," +
                              "(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end),"+
                              "c.CessionRate,c.CessionAmount," +
                              "c.CessPrem," +
                              "c.ReProcFee,substr(replace('"+tStartDate+"','-',''),1,6) "
                   +"from LRPol a,LRPolResult c  "
                   +"where c.cessprem>0 and a.conttype='2'    and a.polno=c.polno and "
                   +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
                   +"    and  a.masterpolno is null and a.insuredname not like '%无名单%' "
                   + strWhere
                   +"group by a.ManageCom,a.recontcode,a.RiskCode,a.GrpContNo,a.CValidate,a.enddate,a.SignDate,a.AppntName,a.AppntNo,a.InsuredName," +
              		"a.InsuredBirthday,a.InsuredSex,a.InsuredNo,a.OccupationType,a.DiseaseAddFeeRate,a.Amnt,a.StandPrem,a.Prem,"+
              		"c.ChoiRebaFeeRate,c.CessionAmount,a.TempCessFlag,c.CessionRate,c.CessionAmount,c.CessPrem," +
              		"c.ReProcFee,a.polno,a.reinsureitem,a.renewcount,a.recontcode "
                   +" union all "
                   +" select a.ManageCom,a.recontcode,a.RiskCode,'''' || trim(a.GrpContNo) || '''',a.CValidate,a.enddate,'',a.SignDate," +
                   "a.AppntName,'''' || trim(a.AppntNo) || '''',char((select Coalesce((select count(distinct(insuredno)) from lcpol where grpcontno=a.grpcontno and poltypeflag<>'1'),0) from dual))," +
                   "char(a.InsuredBirthday),'''' || trim(a.InsuredSex) || '''',(select '''' || trim(IdNo) || '''' from LDPerson where CustomerNo = a.InsuredNo), " +
                   "(case when  a.OccupationType is null then '1' else a.OccupationType end),varchar(a.DiseaseAddFeeRate),a.Amnt,a.StandPrem,a.Prem, " +
                   "c.ChoiRebaFeeRate, " +
//                   "c.CessionAmount," +
                   "(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end),"+
                   "c.CessionRate,c.CessionAmount," +
                   "c.CessPrem," +
                   "c.ReProcFee,substr(replace('"+tStartDate+"','-',''),1,6) "
        +"from LRPol a,LRPolResult c "
        +"where c.cessprem>0 and a.conttype='2'    and a.polno=c.polno and "
        +"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount "
        +"    and  a.masterpolno is null and a.insuredname like '%无名单%' "
        + strWhere
        +"group by a.ManageCom,a.recontcode,a.RiskCode,a.GrpContNo,a.CValidate,a.enddate,a.SignDate,a.AppntName,a.AppntNo,a.InsuredName," +
   		"a.InsuredBirthday,a.InsuredSex,a.InsuredNo,a.OccupationType,a.DiseaseAddFeeRate,a.Amnt,a.StandPrem,a.Prem,"+
   		"c.ChoiRebaFeeRate,c.CessionAmount,a.TempCessFlag,c.CessionRate,c.CessionAmount,c.CessPrem," +
   		"c.ReProcFee,a.polno "
         +" union  "
                   +"select d.ManageCom,a.recontcode,d.RiskCode,'''' || trim(d.ContNo) || '''',d.CValidate,d.enddate,char(a.edorValidate),d.SignDate," +
                              "d.AppntName,'''' || trim(d.AppntNo) || '''',d.InsuredName,char(d.InsuredBirthday),'''' || trim(d.InsuredSex) || '''',(select '''' || trim(IdNo) || '''' from LDPerson where CustomerNo = d.InsuredNo), " +
                              "(case when  d.OccupationType is null then '1' else d.OccupationType end),varchar(d.DiseaseAddFeeRate),d.Amnt,d.StandPrem,d.Prem, " +
                              "c.ChoiRebaFeeRate, " +
//                              "c.CessionAmount," +
                              "(case d.TempCessFlag when 'N' then '合同分保' else '临时分保' end),"+
                              "c.CessionRate,c.CessionAmount," +
                              "b.EdorBackFee," +
                              "b.EdorProcFee,substr(replace('"+tStartDate+"','-',''),1,6) "
                   +"from LRPol d,LRPolResult c,lrpoledor a,lrpoledorresult b "
                   +"where  d.polno=c.polno and "
                   +"d.RecontCode=c.RecontCode and d.ReNewCount=c.ReNewCount and  "
                   +" a.polno = b.Polno and a.RecontCode = b.RecontCode and a.edorno=b.edorno and a.polno=d.polno" +
                   		" and a.polno=c.polno and a.recontcode=d.recontcode and a.recontcode=c.recontcode " +
                   		"and b.polno=d.polno  and b.polno=c.polno " +
                   		"and b.recontcode=d.recontcode and b.recontcode=c.recontcode "
                   + strWhereT
                   +"group by d.ManageCom,a.recontcode,d.RiskCode,d.ContNo,d.CValidate,d.enddate,d.SignDate,d.AppntName,d.AppntNo,d.InsuredName," +
              		"d.InsuredBirthday,d.InsuredSex,d.InsuredNo,d.DiseaseAddFeeRate,d.Amnt,d.StandPrem,d.Prem,"+
              		"c.ChoiRebaFeeRate,c.CessionAmount,d.TempCessFlag,c.CessionRate,c.CessionAmount,b.EdorBackFee," +
              		"b.EdorProcFee,a.edorValidate,d.OccupationType,d.InsuredNo "  +
                   		"with ur ";

      int start = 1;
      int nCount = 10000;
        while (true) {
             SSRS tSSRS = new ExeSQL().execSQL(strSQL, start, nCount);
             int count=0;
             if (tSSRS.getMaxRow() <= 0) {
                  break;
              }else{
                  count=tSSRS.getMaxRow();
              }
            if (tSSRS != null && count > 0) {
              for (int i = 1; i <= count; i++) {
                try{
                    String result="";
                    for(int m=1;m<=tSSRS.getMaxCol();m++){
                          result += tSSRS.GetText(i,m)+";";
                    }
                     mBufferedWriter.write(result+"\r\n");
                     mBufferedWriter.flush();
                 } catch (IOException ex2) {
                        ex2.printStackTrace();
                  }
                 }
             }
             start += nCount;
           }
         try {
            mBufferedWriter.close();
          } catch (IOException ex3) {
          }
          String[] FilePaths = new String[1];
          FilePaths[0] = tSysPath+tPath+".txt";
          String[] FileNames = new String[1];
          FileNames[0] =tPath+".txt";
          String newPath = tSysPath +tPath+".zip";
          String FullPath = tPath+".zip";
          CreateZip(FilePaths,FileNames,newPath);
          try{
              File fd = new File(FilePaths[0]);
              fd.delete();
          }catch(Exception ex4){
              ex4.printStackTrace();
          }
        mResult.add(FullPath);
        return true;
    }
    //生成压缩文件
  public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                           String tZipPath) {
      ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
      if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
          System.out.println("生成压缩文件失败");
          CError.buildErr(this, "生成压缩文件失败");
          return false;
      }
      return true;
  }
  /**
     * 获取再保合同的接受份额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getAccQuot(LRPolSchema aLRPolSchema) {
        return 0.0;
    }
}