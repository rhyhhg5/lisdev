package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LRAccountsSet;
import com.sinosoft.utility.*;
import java.io.File;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;

/**
 * <p>Title: 分保计算结果明细</p>
 * <p>Description: 分保计算结果明细清单</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author huxl
 * @version 1.0
 * @date 2007-03-31
 */

public class LRNewClmListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    String RecontCode = "";
    String ReRiskCode = "";
    String SysPath = ""; //存放生成文件的路径
    String FullPath = "";
    
    private LRAccountsSet mLRAccountsSet = new LRAccountsSet();
    public LRNewClmListBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cSysPath) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        SysPath = cSysPath;
        // 准备所有要打印的数据
        if (!downloadCessData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLRAccountsSet = (LRAccountsSet)cInputData.getObjectByObjectName("LRAccountsSet", 0);
//        //System.out.println(mLRAccountsSet.get(1).getActuGetNo());
//        TransferData tTransferData = (TransferData) cInputData.
//                                     getObjectByObjectName("TransferData", 0);
//        mYear = (String) tTransferData.getValueByName("Year");
//        mMonth = (String) tTransferData.getValueByName("Month");
//        mDate=mYear+"-"+mMonth+"-1";
////        ReRiskSort = (String) tTransferData.getValueByName("ReRiskSort");
////        RecontCode = (String) tTransferData.getValueByName("RecontCode");
////        ReRiskCode = (String) tTransferData.getValueByName("ReRiskCode");
        return true;
    }

    public String getResult() {
        return FullPath;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean downloadCessData() {
        try {
            SysPath = SysPath + "reinsure/";
            String Path = PubFun.getCurrentDate() + "-" + PubFun.getCurrentTime() +
                          ".xls";
            Path = Path.replaceAll(":", "-");
            WriteToExcel tWriteToExcel = new WriteToExcel(Path);
            String[] FilePaths = new String[1];
            FilePaths[0] = SysPath + Path;
            String[] FileNames = new String[1];
            FileNames[0] = "LiPeiJianCeData" +
                           ".xls";
            tWriteToExcel.createExcelFile();
            String[] sheetName = {"__"};
            tWriteToExcel.addSheet(sheetName);

            
                String sql = " select riskcode,recontcode,(case riskcalsort "
      			   +"when '1' then '成数' when '2' then '溢额' end) " 
     			   +"riskcalsort, contno,'','','','','','','','','','' from " 
     			   +"lrpolclm where 1=1";
                
                String[][] strArr1Head = new String[1][15];
                strArr1Head[0][0] = "序号";
                strArr1Head[0][1] = "险种编码";
                strArr1Head[0][2] = "再保合同号";
                strArr1Head[0][3] = "分保类型";
                strArr1Head[0][4] = "保单号";
                strArr1Head[0][5] = "投保人";
                strArr1Head[0][6] = "被保险人";
                strArr1Head[0][7] = "生效日期";
                strArr1Head[0][8] = "签单日期";

                strArr1Head[0][9] = "保额";
                strArr1Head[0][10] = "分出保额";
                strArr1Head[0][11] = "出险日期";
                strArr1Head[0][12] = "结案日期";
                strArr1Head[0][13] = "赔付金额";
                strArr1Head[0][14] = "摊回赔款";

                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql);
                tWriteToExcel.setData(0, sql);
            
            tWriteToExcel.write(SysPath);
            String NewPath = PubFun.getCurrentDate() + "-" +
                             PubFun.getCurrentTime() + ".zip";
            NewPath = NewPath.replaceAll(":", "-");
            FullPath = NewPath;
            NewPath = SysPath + NewPath;
            CreateZip(FilePaths, FileNames, NewPath);
            File fd = new File(FilePaths[0]);
            fd.delete();
            System.out.println("FullPath!  : " + NewPath);

        } catch (Exception ex) {
            ex.toString();
            ex.printStackTrace();
        }
        return true;
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this, "生成压缩文件失败");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
    }
}
