package com.sinosoft.lis.reinsure;

import java.util.Date;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRCessListSchema;
import com.sinosoft.lis.vschema.LRCessListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: LRCessListBL </p>
 * <p>Description: 提取理赔数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 龙程彬
 * @CreateDate：2008-10-30
 */
public class LRCessListBL {
	


		    /** 错误处理类，每个需要错误处理的类中都放置该类 */
		    public CErrors mErrors = new CErrors();

		    /** 前台传入的公共变量 */
		    private GlobalInput globalInput = new GlobalInput();


		    /** 往后面传输数据的容器 */
//		    private VData mInputData = new VData();

		    /** 数据操作字符串 */
		    private String strOperate = "";
		    private String mToday = "";
		    private String mStartDate = "";
		    private String mEndDate = "";

		    private TransferData mGetCessData = new TransferData();

		    private MMap tmap = new MMap();
		    private VData mOutputData = new VData();
		    
		    private String mReContCode = "";
		    private	String mRiskCode;
		    private	String mGrpContNo;
		    private	String mContNo;
		    private	String mPolNo;
		    private	String mOperator;
		    private LRCessListSet mLRCessListSet = new LRCessListSet();


		    //业务处理相关变量
		    /** 全局数据 */

		    public LRCessListBL() {
		    }

		    /**
		     * 提交数据处理方法
		     * @param cInputData 传入的数据,VData对象
		     * @param cOperate 数据操作字符串
		     * @return 布尔值（true--提交成功, false--提交失败）
		     */
		    public boolean submitData(VData cInputData, String cOperate) {
		        System.out.println("Begin LRCessListBL.Submit..............");
		        this.strOperate = cOperate;
		        if (strOperate.equals("")) {
		            buildError("verifyOperate", "不支持的操作字符串");
		            return false;
		        }
		        if (!getInputData(cInputData)) {
		            return false;
		        }
		        if (!dealData()) {
		            return false;
		        }
		        //准备往后台的数据
		        if (!prepareOutputData())
		        {
		            return false;
		        }

		        PubSubmit tPubSubmit = new PubSubmit();
		        tPubSubmit.submitData(mOutputData, "");
		        //如果有需要处理的错误，则返回
		        if (tPubSubmit.mErrors.needDealError())
		        {
		            // @@错误处理
		            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
		            CError tError = new CError();
		            tError.moduleName = "LRCessListBL";
		            tError.functionName = "submitDat";
		            tError.errorMessage = "数据提交失败!";
		            this.mErrors.addOneError(tError);
		            return false;
		        }
		        
		        return true;
		    }


		    /**
		     * 根据前面的输入数据，进行UI逻辑处理
		     * 如果在处理过程中出错，则返回false,否则返回true
		     */
		    private boolean dealData() {

	            if (!getCessData()) {
	                return false;
		        }
		        return true;
		    }

		    /**
		     * 提取分保数据
		     * @return boolean
		     */
		    private boolean getCessData() {
		        FDate chgdate = new FDate();
		        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
		        Date dedate = chgdate.getDate(mEndDate); //录入的终止日期，用FDate处理
		        try{		           
		            while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
		                mToday = chgdate.getString(dbdate); //录入的正在统计的日期
		                if (!insertPolData()) {
		                    buildError("getCessData", "提取" + mToday + "日新单数据出错");
		                    return false;
		                }
		                dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
		            }
		            
		            
		        }catch(Exception e){
		                e.printStackTrace();
		        }

		        return true;
		    }
		    
		     /**
		     * 准备后台的数据
		     * @return boolean
		     */
		    private boolean prepareOutputData()
		    {
		        try
		        {
//		        	tmap.put(this.mLRCessListSet, "INSERT");
		        	this.mOutputData.clear();
		            this.mOutputData.add(tmap);
		        }
		        catch (Exception ex)
		        {
		            // @@错误处理
		            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
		            return false;
		        }
		        return true;
		    }
		    //执行SQL,并将取出的数据插入到:
		    private boolean insertPolData(){
		    	String currentdate=PubFun.getCurrentDate();
		    	String currenttime=PubFun.getCurrentTime();
//		    	String sql[] = new String[6];
		    	String sql = null;
//		    	临时分保->0  合同分保->1
		    	String strSQL="";
		    	if(mReContCode!=null&&!"".equals(mReContCode)){
	            	
		            strSQL+=" and a.recontcode='"+mReContCode+"'  ";
		        }
		        if(!"".equals(mGrpContNo))
		     	{
		         	strSQL +=" and a.grpcontno ='"+mGrpContNo+"'";
		     	}
		     	if(!"".equals(mRiskCode))
		     	{
		     		strSQL +=" and a.riskcode ='"+mRiskCode+"'";
		     	}
		     	if(!"".equals(mContNo))
		     	{
		     		strSQL +=" and a.ContNo ='"+mContNo+"'";
		     	}
		     	if(!"".equals(mPolNo))
		     	{
		     		strSQL +=" and a.PolNo ='"+mPolNo+"'";
		     	}
		     	if(!"".equals(mOperator))
		     	{
		     		strSQL +=" and a.Operator ='"+mOperator+"'";
		     	}
	    		sql=

	    		    " select a.ManageCom as 机构代码,a.recontcode as 再保合同号,a.RiskCode as 产品代码," +
	    		    "a.ContNo, a.GrpContNo ,(case a.conttype when '1' then a.CValidate" +
	    		    " else (select CValidate from lcgrpcont where grpcontno=a.grpContNo " +
	    		    " union all select CValidate from lbgrpcont where grpcontno=a.grpContNo) end ) as 生效日期," +
	    		    "(case a.conttype when '1' then a.enddate" +
	    		    " else (select cinvalidate from lcgrpcont where grpcontno=a.grpContNo " +
	    		    " union all select cinvalidate from lbgrpcont where grpcontno=a.grpContNo) end ) as 失效日期,'' as 增减人日期," +
	    		    "(case a.conttype when '1' then a.SignDate" +
	    		    " else (select SignDate from lcgrpcont where grpcontno=a.grpContNo " +
	    		    " union all select SignDate from lbgrpcont where grpcontno=a.grpContNo) end ) as 签单日期,a.AppntName as 投保人,a.AppntNo as 投保人客户号," +
	    		    "a.InsuredName as 被保险人,char(a.InsuredBirthday) as 出生日期,a.InsuredSex as 性别," +
	    		    "(select IdNo from LDPerson where CustomerNo = a.InsuredNo) as 身份证号码," +
	    		    "a.OccupationType as 职业类别," +
	    		    " (case conttype when '1' then char(a.DiseaseAddFeeRate)" +
	    		    " else '0' end ) as 核保加费率," +
	    		    "a.Amnt as 保额,a.StandPrem as 标准保费,a.Prem as 承保保费,0 as 折扣率,(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end) as 分保类型,c.CessionRate as 分出比例,c.CessionAmount as 分出保额,c.CessPrem as 分出保费（退保金额）,c.ReProcFee as 分保（退回）手续费,a.GetDataDate as 清单时间,'0',a.polno,a.ReinsureItem,a.ReNewCount,a.GetDataDate,a.costcenter,a.payno,a.contplancode " 
                    +",(select markettype from lcgrpcont where grpcontno=a.grpcontno union all select markettype from lbgrpcont where grpcontno=a.grpcontno) as 市场类型 "//增加市场类型字段
	    		    +" from LRPol a, LRPolResult c "
                    +" where  a.payno=c.payno and a.polno = c.polno and a.RecontCode = c.RecontCode and a.getdatadate=c.cessstartdate and a.ReNewCount = c.ReNewCount  and (a.poltypeflag<>'1' or a.poltypeflag is null) "
                    +" and a.reinsureitem=c.reinsureitem and a.GetDataDate ='"+mToday+"' and c.CessPrem<>0 " +
                    " and not exists (select 1 from lrinterface where polno=a.polno " +
                    " and renewcount=a.renewcount and dutydate=a.getdatadate and renewalflag='-') "+strSQL
                    +" union all "
                    +" select a.ManageCom as 机构代码,a.recontcode as 再保合同号,a.RiskCode as 产品代码,a.ContNo, a.GrpContNo ," +
                    " (select CValidate from lcgrpcont where grpcontno=a.grpContNo " +
	    		    " union all select CValidate from lbgrpcont where grpcontno=a.grpContNo) as 生效日期," +
	    		    "(select cinvalidate from lcgrpcont where grpcontno=a.grpContNo " +
	    		    " union all select cinvalidate from lbgrpcont where grpcontno=a.grpContNo) as 失效日期,char(a.getdatadate) as 增减人日期," +
	    		    "(select SignDate from lcgrpcont where grpcontno=a.grpContNo " +
	    		    " union all select SignDate from lbgrpcont where grpcontno=a.grpContNo) as 签单日期,a.AppntName as 投保人,a.AppntNo as 投保人客户号,a.InsuredName as 被保险人,char(a.InsuredBirthday) as 出生日期,a.InsuredSex as 性别,(select IdNo from LDPerson where CustomerNo = a.InsuredNo) as 身份证号码,a.OccupationType as 职业类别,'0' as 核保加费率,a.Amnt as 保额,a.StandPrem as 标准保费,a.Prem as 承保保费,0 as 折扣率,(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end) as 分保类型,c.CessionRate as 分出比例,c.CessionAmount as 分出保额,c.CessPrem as 分出保费（退保金额）,c.ReProcFee as 分保（退回）手续费,a.GetDataDate as 清单时间,'0',a.polno,a.ReinsureItem,a.ReNewCount,a.GetDataDate,a.costcenter,a.payno,a.contplancode "
	    		    +",(select markettype from lcgrpcont where grpcontno=a.grpcontno union all select markettype from lbgrpcont where grpcontno=a.grpcontno) as 市场类型 "//增加市场类型字段
                    +" from LRPol a, LRPolResult c "
                    +" where  a.payno=c.payno and a.conttype = '2' and a.polno = c.polno and a.RecontCode = c.RecontCode and a.getdatadate=c.cessstartdate and a.ReNewCount = c.ReNewCount  and (a.poltypeflag<>'1' or a.poltypeflag is null) "
                    +" and a.reinsureitem=c.reinsureitem and a.GetDataDate ='"+mToday+"' and c.CessPrem<>0 " +
                    " and  exists (select 1 from lrinterface where polno=a.polno " +
                    " and renewcount=a.renewcount and dutydate=a.getdatadate and renewalflag='-') "+strSQL
                 +" union all "
	    		    +" select a.ManageCom as 机构代码,a.recontcode as 再保合同号,a.RiskCode as 产品代码,a.ContNo, a.GrpContNo ,a.CValidate as 生效日期,a.enddate as 失效日期,'' as 增减人日期,a.SignDate as 签单日期,a.AppntName as 投保人,a.AppntNo as 投保人客户号,char((select Coalesce((select count(distinct(insuredno))from lcpol where grpcontno = a.grpcontno and poltypeflag <> '1') , 0)from dual)) as 被保险人,char(a.InsuredBirthday) as 出生日期,a.InsuredSex as 性别,(select IdNo from LDPerson where CustomerNo = a.InsuredNo) as 身份证号码, a.OccupationType  as 职业类别,'0' as 核保加费率,a.Amnt as 保额,a.StandPrem as 标准保费,a.Prem as 承保保费,0 as 折扣率,(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end) as 分保类型,c.CessionRate as 分出比例,c.CessionAmount as 分出保额,c.CessPrem as 分出保费（退保金额）,c.ReProcFee as 分保（退回）手续费,a.GetDataDate as 清单时间,'0',a.polno,a.ReinsureItem,a.ReNewCount,a.GetDataDate,a.costcenter,a.payno,a.contplancode  " 
	    		    +",(select markettype from lcgrpcont where grpcontno=a.grpcontno union all select markettype from lbgrpcont where grpcontno=a.grpcontno) as 市场类型 "//增加市场类型字段
                    +" from LRPol a, LRPolResult c "
                    +" where a.payno=c.payno and a.conttype = '2' and a.polno = c.polno and a.RecontCode = c.RecontCode and a.ReNewCount = c.ReNewCount and a.getdatadate=c.cessstartdate  and a.poltypeflag='1' "
                    +" and a.reinsureitem=c.reinsureitem and a.GetDataDate = '"+mToday+"' and c.CessPrem<>0 "+strSQL
                    
//                    +" with ur ";
                 +" union all "+
	    			" select  d.ManageCom as 机构代码, a.recontcode as 再保合同号, d.RiskCode as 产品代码," +
	    			"t.ContNo, t.GrpContNo ,(case t.conttype when '1' then t.CValidate" +
	    			" else (select CValidate from lcgrpcont where grpcontno=a.grpContNo " +
	    			" union all select CValidate from lbgrpcont where grpcontno=a.grpContNo) end ) as 生效日期," +
	    			"(case t.conttype when '1' then t.enddate" +
	    			" else (select cinvalidate from lcgrpcont where grpcontno=a.grpContNo " +
	    			" union all select cinvalidate from lbgrpcont where grpcontno=a.grpContNo) end) as 失效日期," +
	    			"(case when a.FeeOperationType in ('ZT', 'WZ', 'WJ') then  char(a.GetDataDate) else null"+
	    			" end) as 增减人日期, (case t.conttype when '1' then t.SignDate" +
	    			" else (select SignDate from lcgrpcont where grpcontno=a.grpContNo " +
	    			" union all select SignDate from lbgrpcont where grpcontno=a.grpContNo) end) as 签单日期,t.AppntName as 投保人,t.AppntNo as 投保人客户号,"+
	    			"t.InsuredName as 被保险人,char(t.InsuredBirthday) as 出生日期,t.InsuredSex as 性别,(select IdNo from LDPerson where CustomerNo = t.InsuredNo) as 身份证号码," +
	    			"(case when t.OccupationType is null then '1' when t.OccupationType = '0' then '1' else t.OccupationType end) as 职业类别,"+
	    			"(case t.conttype when '1' then (select char(DiseaseAddFeeRate) from lrpol where recontcode=a.recontcode and polno=a.polno " +
	    			" and renewcount=a.renewcount fetch first 1 rows only )" +
	    			"else '0' end ) as 核保加费率, t.Amnt as 保额," +
	    			"t.StandPrem as 标准保费,t.Prem as 承保保费, " +
	    			"0 as 折扣率, " +
	    			"(case(select count(1) from lrcontinfo where recontcode = a.recontcode)  when 0 then   '临时分保' else '合同分保' end) as 分保类型," +
	    			"  (select Coalesce((select c.cessionrate from lrpolresult c where (polno=t.masterPolno or polno=a.polno) and " +
	    			"c.RecontCode = a.RecontCode and c.renewcount=a.renewcount " +
	    			"  fetch first  1 rows only),0) from dual) as 分保比例," +
	    			"(select Coalesce(sum(c.CessionAmount),0)  from lrpolresult c " +
	    			"where (polno=t.masterPolno or polno=a.polno)and c.RecontCode = a.RecontCode " +
	    			"and c.renewcount=a.renewcount  ) as 分保保额, d.EdorBackFee as 分出保费（退保金额）," +
	    			" d.EdorProcFee as 分保（退回）手续费, a.GetDataDate as 清单时间," +
	    			"'1',a.polno,a.ReinsureItem,a.ReNewCount,a.GetDataDate,a.costcenter,a.edorno,'11'  "
	    			+",(select markettype from lcgrpcont where grpcontno=a.grpcontno union all select markettype from lbgrpcont where grpcontno=a.grpcontno) as 市场类型 "//增加市场类型字段
	    			 +"FROM LRPoledor a, LRPoledorResult d," +
	    			"(select conttype,CValidate,enddate,SignDate,AppntName,AppntNo,InsuredBirthday,InsuredSex," +
	    			"InsuredNo,OccupationType,amnt,polno,standprem,InsuredName,contno,grpcontno,prem,masterPolno " +
	    			"from lcpol where polno in (select polno from LRPoledor where " +
	    			" GetDataDate ='"+mToday+"') union all " +
	    			"select conttype,CValidate,enddate,SignDate,AppntName,AppntNo,InsuredBirthday," +
	    			"InsuredSex,InsuredNo,OccupationType,amnt,polno,standprem,InsuredName," +
	    			"contno,grpcontno,prem,masterPolno from lbpol  where polno in " +
	    			"(select polno from LRPoledor where GetDataDate ='"+mToday+"')) as t" +
	    			" where a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.edorno = d.edorno " +
	    			" and a.reinsureitem=d.reinsureitem " +
	    			"and a.polno=t.polno " +
	    			"and a.GetDataDate ='"+mToday+"' and d.EdorBackFee<>0 "+strSQL
                 +" with ur ";
//	    		for(int j=0;j<=5;j++){
		    	try{

		        	ExeSQL tExeSQL = new ExeSQL(); 
		        	SSRS tSSRS = new SSRS();
		        	RSWrapper rsWrapper = new RSWrapper();
		        	if (!rsWrapper.prepareData(null, sql))
		            {
		                System.out.println("数据准备失败! ");
		                return false;
		            }

		        	 do {
		             	tSSRS = rsWrapper.getSSRS();
		             	tmap = new MMap();
		     			if (tSSRS != null || tSSRS.MaxRow > 0) {

		        	
		        	for(int i=1;i<=tSSRS.MaxRow;i++){
		        		String aReContCode        = tSSRS.GetText(i,2); 
		        		String aRiskCode          = tSSRS.GetText(i,3); 
		        		String aContNo            = tSSRS.GetText(i,4); 
		        		String aGrpContNo         = tSSRS.GetText(i,5);
		        		String aCValiDate         = tSSRS.GetText(i,6);
		        		String aEndDate           = tSSRS.GetText(i,7); 
		        		String aEdorValidate      = tSSRS.GetText(i,8); 
		        		String aSignDate          = tSSRS.GetText(i,9); 
		        		String aAppntName         = tSSRS.GetText(i,10); 
		        		String aAppntNo           = tSSRS.GetText(i,11); 
		        		String aInsuredName       = tSSRS.GetText(i,12); 
		        		String aInsuredBirthday   = tSSRS.GetText(i,13); 
		        		String aInsuredSex        = tSSRS.GetText(i,14); 
		        		String aInsuredIdNo       = tSSRS.GetText(i,15); 
		        		String aOccupationType    = tSSRS.GetText(i,16); 
		        		String aDiseaseAddFeeRate = tSSRS.GetText(i,17); 
		        		String aAmnt              = tSSRS.GetText(i,18); 
		        		String aStandPrem         = tSSRS.GetText(i,19); 
		        		String aPrem              = tSSRS.GetText(i,20); 
		        		String aManageFeeRate     = tSSRS.GetText(i,21); 
		        		String aTempCessFlag      = tSSRS.GetText(i,22); 
		        		String aCessionRate       = tSSRS.GetText(i,23); 
		        		String aCessionAmount     = tSSRS.GetText(i,24); 
		        		String aCessPrem          = tSSRS.GetText(i,25); 
		        		String aReProcFee         = tSSRS.GetText(i,26); 
		        		String aListType          = tSSRS.GetText(i,28);
		        		String aPolNo             = tSSRS.GetText(i,29);
		        		String aReinsureItem      = tSSRS.GetText(i,30);
		        		String aReNewCount        = tSSRS.GetText(i,31);
		        		String aGetDataDate       = tSSRS.GetText(i,32);
		        		String aManageCom         = tSSRS.GetText(i,1); 
		        		String aCostCenter       = tSSRS.GetText(i,33);
		        		String aPayNo       = tSSRS.GetText(i,34);
		        		String aContPlanCode       = tSSRS.GetText(i,35);
		        		String aMarketType     = tSSRS.GetText(i,36);
		        		
		        		
		        		LRCessListSchema tLRCessListSchema = new LRCessListSchema();
		        		tLRCessListSchema.setReContCode(aReContCode); 
		        		tLRCessListSchema.setRiskCode(aRiskCode); 
		        		tLRCessListSchema.setContNo(aContNo); 
		        		tLRCessListSchema.setGrpContNo(aGrpContNo);
		        		tLRCessListSchema.setCValiDate(aCValiDate);
		        		tLRCessListSchema.setEndDate(aEndDate); 
		        		tLRCessListSchema.setEdorValidate(aEdorValidate); 
		        		tLRCessListSchema.setSignDate(aSignDate); 
		        		tLRCessListSchema.setAppntName(aAppntName); 
		        		tLRCessListSchema.setAppntNo(aAppntNo); 
		        		tLRCessListSchema.setInsuredName(aInsuredName); 
		        		tLRCessListSchema.setInsuredBirthday(aInsuredBirthday); 
		        		tLRCessListSchema.setInsuredSex(aInsuredSex); 
		        		tLRCessListSchema.setInsuredIdNo(aInsuredIdNo); 
		        		tLRCessListSchema.setOccupationType(aOccupationType); 
		        		tLRCessListSchema.setDiseaseAddFeeRate(aDiseaseAddFeeRate); 
		        		tLRCessListSchema.setAmnt(aAmnt); 
		        		tLRCessListSchema.setStandPrem(aStandPrem); 
		        		tLRCessListSchema.setPrem(aPrem); 
		        		tLRCessListSchema.setManageFeeRate(aManageFeeRate); 
		        		tLRCessListSchema.setTempCessFlag(aTempCessFlag); 
		        		tLRCessListSchema.setCessionRate(aCessionRate); 
		        		tLRCessListSchema.setCessionAmount(aCessionAmount); 
		        		tLRCessListSchema.setCessPrem(aCessPrem); 
		        		tLRCessListSchema.setReProcFee(aReProcFee); 
		        		tLRCessListSchema.setManageCom(aManageCom); 
//		        		tLRCessListSchema.setListDate(aGetDataDate); 
		        		tLRCessListSchema.setMakeDate(currentdate); 
		        		tLRCessListSchema.setMakeTime(currenttime);  
		        		tLRCessListSchema.setModifyDate(currentdate);  
		        		tLRCessListSchema.setModifyTime(currenttime);  
		        		tLRCessListSchema.setOprater(strOperate);  
		        		tLRCessListSchema.setListType(aListType);
		        		tLRCessListSchema.setPolNo(aPolNo);
		        		tLRCessListSchema.setReinsureItem(aReinsureItem);
		        		tLRCessListSchema.setReNewCount(aReNewCount);
		        		tLRCessListSchema.setGetDataDate(aGetDataDate);
		        		tLRCessListSchema.setCostCenter(aCostCenter);
		        		tLRCessListSchema.setPayNo(aPayNo);
		        		tLRCessListSchema.setContPlanCode(aContPlanCode);
		        		tLRCessListSchema.setMarketType(aMarketType);
		        		tmap.put(tLRCessListSchema, "INSERT");
//		        		mLRCessListSet.add(tLRCessListSchema);
		        	}
		        	 if (!prepareOutputData()) {
		                    return false;
		                }
		        	 PubSubmit tPubSubmit = new PubSubmit();
				     tPubSubmit.submitData(mOutputData, "");
				     //如果有需要处理的错误，则返回
				     if (tPubSubmit.mErrors.needDealError())
				       {
				            // @@错误处理
				            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				            CError tError = new CError();
				            tError.moduleName = "LRCessListBL";
				            tError.functionName = "submitDat";
				            tError.errorMessage = "数据提交失败!";
				            this.mErrors.addOneError(tError);
				            return false;
				        }
		     			}
		     			}while (tSSRS != null && tSSRS.MaxRow > 0);
		     			rsWrapper.close();

		     		
		    		 
		        	
		    	}catch(Exception ex){
		    		ex.printStackTrace();
		    		return false;
		    	}
//	    		}
		    	
		    	
		    	return true;
		    }
		    
		    //错误信息
		    private void buildError(String szFunc, String szErrMsg) {
		        CError cError = new CError();
		        cError.moduleName = "LRCessListBL";
		        cError.functionName = szFunc;
		        cError.errorMessage = szErrMsg;
		        this.mErrors.addOneError(cError);
		    }

		    
		    private boolean getInputData(VData cInputData) {
		        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
		                "GlobalInput", 0));
		        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
		                "TransferData", 0);
		        mStartDate = (String) mGetCessData.getValueByName("StartDate");
		        mEndDate = (String) mGetCessData.getValueByName("EndDate");
		        System.out.println("mStartDate:"+mStartDate+" mEndDate:"+mEndDate);
		        if("filldata".equals(strOperate))
		        {
		        	 mReContCode = (String) mGetCessData.getValueByName("ReContCode");
		        	 mRiskCode = (String) mGetCessData.getValueByName("RiskCode");
		             mGrpContNo = (String) mGetCessData.getValueByName("GrpContNo");
		             mContNo = (String) mGetCessData.getValueByName("ContNo");
		             mPolNo = (String) mGetCessData.getValueByName("PolNo");
		             mOperator = (String) mGetCessData.getValueByName("Operator");
		             strOperate ="fd"+PubFun.getCurrentDate2();
		        }
		        else
		        {
		        	strOperate = globalInput.Operator;
		        }
		        
		        
		        return true;
		    }
		    /**
		     * 从输入数据中得到所有对象
		     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		     */

		    public String getResult() {
		        return "没有传数据";
		    }
		    

		    public static void main(String[] args) {
		        LRNewGetCessDataBL tLRNewGetCessDataBL = new LRNewGetCessDataBL();
		        GlobalInput globalInput = new GlobalInput();
		        globalInput.ManageCom = "86";
		        globalInput.Operator = "rm0002";
		        String mOperateType = "CESSDATA";

		        String startDate = "2008-10-10";
		        String endDate = "2008-10-10";
		        TransferData getCessData = new TransferData();
		        getCessData.setNameAndValue("StartDate", startDate);
		        getCessData.setNameAndValue("EndDate", endDate);

		        VData tVData = new VData();
		        tVData.addElement(globalInput);
		        tVData.addElement(getCessData);

		        tLRNewGetCessDataBL.submitData(tVData, mOperateType);
		    }
}
