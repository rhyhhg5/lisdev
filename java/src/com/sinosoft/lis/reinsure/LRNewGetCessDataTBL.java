/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.Hashtable;

import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LRCalFactorValueSet;
import com.sinosoft.lis.vschema.LRCalModeSet;
import com.sinosoft.lis.vschema.LRContInfoSet;
import com.sinosoft.lis.vschema.LRDutySet;
import com.sinosoft.lis.vschema.LRInterfaceSet;

import java.util.*;

/*
 * <p>ClassName: LRNewGetCessDataBL </p>
 * <p>Description: 提取再保新单数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 孙宇
 * @CreateDate：2008-10-30
 */
public class LRNewGetCessDataTBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
//    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mToday = "";
    private String mStartDate = "";
   
    private String mReContCode = "";
    private String mGrpPolNo = "";
    private String mPayNo = "";
    private String tFlag="";
    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private String mRecontCode; //再保合同代码
    private String mRiskCalSort = "";
    private Hashtable m_hashLMCheckField = new Hashtable();
    //业务处理相关变量
    /** 全局数据 */

    public LRNewGetCessDataTBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    
    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT(String cOperate,String mToDay) {
       
        if (!getInputDataT(mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

       
            if (!getCessData()) {
                return false;
            }
        
        return true;
    }

    /**
     * 提取分保数据
     * @return boolean
     */
    private boolean getCessData() {
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
       

        Connection con=DBConnPool.getConnection();
        try{
            //con.setAutoCommit(false);
           
                mToday = chgdate.getString(dbdate); //录入的正在统计的日期
                mGrpPolNo="s";
                mPayNo="s";
                if (!insertPolData(con)) {
//                    buildError("getCessData", "提取" + mToday + "日新单数据出错");
                    return false;
                }
                
                con.commit();
            
            
            con.close();
        }catch(Exception e){
            try{
                e.printStackTrace();
                con.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 提取合同分保保单信息
     * @return boolean
     */
    private boolean insertPolData(Connection con) {
    	
    	String tSql="";
    	
    	tSql="select * from lrinterface a where DutyDate='"+mToday+"' " +
		"and  State='0' and polstat<>'3' order by grppolno,renewalflag with ur ";
    	
        LRInterfaceDB tLRInterfaceDB=new LRInterfaceDB(con);
        LRInterfaceSet tLRInterfaceSet=new LRInterfaceSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRInterfaceSet, tSql);
        try {
            do {
                rswrapper.getData();
                
        for(int i=1;i<=tLRInterfaceSet.size();i++){
            LRInterfaceSchema tLRInterfaceSchema=tLRInterfaceSet.get(i);
            
            if(tLRInterfaceSchema.getContType().equals("2")
            		&&(!mGrpPolNo.equals(tLRInterfaceSchema.getGrpPolNo())
            		||(mGrpPolNo.equals(tLRInterfaceSchema.getGrpPolNo())
            				&&!mPayNo.equals(tLRInterfaceSchema.getPayNo())))){
            	tFlag="1";
            	mGrpPolNo=tLRInterfaceSchema.getGrpPolNo();
            	mPayNo=tLRInterfaceSchema.getPayNo();
            }
            else
            	tFlag="0";
          /**
           * 首先判断是否有临分合同的校验sql，如果该保单有临分合同数据则进入临分合同处理流程
           */
            String tTCsql="select 1 from lrpol where polno='"+tLRInterfaceSchema.getPolNo()+"' and " +
            		"tempcessflag='Y'";
            SSRS tTCSSRS = new ExeSQL().execSQL(tTCsql);
            if(tTCSSRS!=null && tTCSSRS.MaxRow>0){
            	//临分处理(单独写方法在主流程外)
            	continue;
            }
            else{
            		if (!cessComData(tLRInterfaceSchema,mToday,con,"","C")) {
                        continue;
                    }
            	
            }

         }
        }
        while (tLRInterfaceSet.size() > 0);
        rswrapper.close();
        }
     catch (Exception ex) {
        ex.printStackTrace();
        rswrapper.close();
    }
        return true;
    }
    
    /**
     * 计算正常提取到的数据的分保提数处理
     * @param tLRInterfaceSchema
     * @param tDate
     * @param con
     * @return boolean
     */
    private boolean cessComData(LRInterfaceSchema tLRInterfaceSchema,String tDate,Connection con,String mReContCode,String tType){
    	
    	LRPolDB tLRPolDB= new LRPolDB(con);
    	LRInterfaceDB tLRInterfaceDB= new LRInterfaceDB(con);
        Reflections tReflections = new Reflections();
        
    	//判断是否首期
    	String tReflag=tLRInterfaceSchema.getRenewalFlag();
    	String tContDte=tDate;//判断属于哪个合同范围的时间变量
    	String strSql="";
    	if("C".equals(tType)){
    		if("Y".equals(tReflag)||"-".equals(tReflag)||"L".equals(tReflag)){//非首期按BelongDate再保合同归属日判断属于哪个合同
    	
    		tContDte=tLRInterfaceSchema.getBelongDate();
    	}
    	 strSql = "select * from LRContInfo where  (CessionDetailType is null or trim(CessionDetailType)='' or CessionDetailType = '02') and  ReContState='01' and (ReType='01' and exists " +
         " (select 1 from LRcalfactorvalue where Recontcode=LRContInfo.Recontcode and riskcode='" +
         tLRInterfaceSchema.getRiskCode() +
         "')) and ((Rinvalidate is not null and '" +
         tContDte +
         "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
         tContDte + "' >= Rvalidate)) " +
         " and exists (select 1 from lmriskapp where riskcode='"+tLRInterfaceSchema.getRiskCode()+"') "+
         "union "+
         "select * from LRContInfo where (CessionDetailType is null or trim(CessionDetailType)='' or CessionDetailType = '02') and ReContState='01' and (ReType='02' and exists " +
         "(select 1 from LRcalfactorvalue a,lrduty b where  a.Recontcode=LRContInfo.Recontcode "+
         "and a.riskcode=b.getdutycode and b.polno='"+tLRInterfaceSchema.getPolNo()+"')) "+
         "and ((Rinvalidate is not null and '" +
         tContDte +
         "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
         tContDte + "' >= Rvalidate)) " +
         "union "+
         "select * from LRContInfo where  CessionDetailType = '01' and  ReContState='01' and (ReType='01' and exists " +
         " (select 1 from LRcalfactorvalue where Recontcode=LRContInfo.Recontcode and riskcode='" +
         tLRInterfaceSchema.getRiskCode() +
         "')) and ((Rinvalidate is not null and '" +
         tContDte +
         "' between Rvalidate and Rinvalidate and '" +
         tDate +
         "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
         tContDte + "' >= Rvalidate and '"+tDate+"' >= Rvalidate )) " +
         " and exists (select 1 from lmriskapp where riskcode='"+tLRInterfaceSchema.getRiskCode()+"') "+
         "union "+
         "select * from LRContInfo where CessionDetailType = '01' and ReContState='01' and (ReType='02' and exists " +
         "(select 1 from LRcalfactorvalue a,lrduty b where  a.Recontcode=LRContInfo.Recontcode "+
         "and a.riskcode=b.getdutycode and b.polno='"+tLRInterfaceSchema.getPolNo()+"')) "+
         "and ((Rinvalidate is not null and '" +
         tContDte +
         "' between Rvalidate and Rinvalidate and '" +
         tDate +
         "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
         tContDte + "' >= Rvalidate and '"+tDate+"' >= Rvalidate )) " +
         "order by Rvalidate desc with ur";
      	}
    	
     LRContInfoSet tLRContInfoSet = new LRContInfoDB().executeQuery(
             strSql);
     System.out.println("*********"+strSql+"************");
//   多家再保公司共保一个险种或责任，得到此针对险种的所有再保合同
     
     if (tLRContInfoSet == null || tLRContInfoSet.size() <= 0) {
         return false;
     }
     
     String agentGroup = tLRInterfaceSchema.getAgentGroup();
     String sql = "select costcenter from labranchgroup where agentgroup = (select substr(branchseries,1,12) from labranchgroup where agentgroup='"
                  +agentGroup+"' )with ur";
     String CostCenter = new ExeSQL().getOneValue(sql);
     String aSql = " select count(1) from lmriskapp where riskperiod = 'L' and riskcode ='"+tLRInterfaceSchema.getRiskCode()+"' with ur";
     String LRiskFlag = new ExeSQL().getOneValue(aSql);
     boolean tStateFlag=false;//是否更新中间表的标志校验
     for (int j = 1; j <= tLRContInfoSet.size(); j++) {
         LRContInfoSchema tLRContInfoSchema = tLRContInfoSet.get(j);
         String tContNo = "";
         //增加对已不需分保保单的校验
         if("1".equals(tLRInterfaceSchema.getContType()))
        	 tContNo = tLRInterfaceSchema.getContNo();
         else if("2".equals(tLRInterfaceSchema.getContType()))
        	 tContNo = tLRInterfaceSchema.getGrpContNo();
    	 String noLongerSql = "select 1 from LRNoLongerCont where  " +
    	 						"recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
 		 						" and contno='"+tContNo+"' fetch first 1 rows only with ur";
 		 SSRS tSSRSNL = new ExeSQL().execSQL(noLongerSql);
 		 if(tSSRSNL!=null && tSSRSNL.getMaxRow()>0){
 			 continue;
         }
         String tCessionMode=tLRContInfoSchema.getCessionMode();
         //溢额合同不处理续期及无名单的数据
         if((tCessionMode.equals("2")&&"Y".equals(tLRInterfaceSchema.getRenewalFlag()))||
        		 //2014-03-28修改：成数合同中不处理周年溢额数据
        		 (tCessionMode.equals("1")&&"L".equals(tLRInterfaceSchema.getRenewalFlag()))||
        		 (tCessionMode.equals("2")&&"1".equals(tLRInterfaceSchema.getPolTypeFlag())
        				 //中再合同需处理无名单
        				 &&!"CLR2017A".equals(tLRContInfoSchema.getReContCode().substring(0, 8)))){
        	 continue;
         }
         String tRetype=tLRContInfoSchema.getReType();
         String tCode="";
         double tRiskAmnt=0.00;//风险保额
         double tRealCessRate=0.00;//真实分保比例
         double tCessAmnt=0.00;//分保保额
         if("01".equals(tRetype)){//险种分保则把该号码置为险种编码
        	 tCode=tLRInterfaceSchema.getRiskCode();
         }
         else if("02".equals(tRetype)){//责任分保则要从lrcalfactorvalue这个表取出该合同分保的责任编码
        	 tCode=getGetDutyCode(tLRContInfoSchema);
        	 if("-1".equals(tCode)){
        		 System.out.println("取得责任分保合同的给付责任号时出现错误！出现问题的合同号是"+tLRContInfoSchema.getReContCode()+"");
                 continue;
        	 }
         }
         String tCalCont = getCalCode(tLRContInfoSchema,tCode,"C","recont");//获取合同校验的校验编码
         //取得到的合同是否有特殊判断条件
         if(!"0".equals(tCalCont)){
             String tcheck=calculatorReCont(tCalCont,tLRInterfaceSchema.getPolNo(),tDate);//调用校验函数，当不符合该合同条件时候返回“-1”
             if("0".equals(tcheck)){//不符合条件继续处理下个合同
            	 continue;
             }
         }
         
         String tCalRiskAmnt=getCalCode(tLRContInfoSchema,tCode,"C","riskamnt");//获取风险保额计算公式编码，如果没有描述对应公式则该字段存0
         if(!"0".equals(tCalRiskAmnt)){
             double tcheck=calculatorRiskAmnt(tCalRiskAmnt,tLRInterfaceSchema,0,
            		 tLRContInfoSchema.getReType(),tCode,tDate,LRiskFlag);//调用风险保额计算函数
             if(tcheck==-1){//计算错误继续下一合同计算
            	 continue;
             }
             else{
            	 tRiskAmnt=tcheck;
             }
         }
         
         String tCalRate=getCalCode(tLRContInfoSchema,tCode,"C","rate");//获取实际分保比例计算公式编码，如果没有描述对应公式则该字段存0
         if(!"0".equals(tCalRate)){
        	 if("Y".equals(tLRInterfaceSchema.getRenewalFlag())||"L".equals(tLRInterfaceSchema.getRenewalFlag())){//如果是非首期保单则直接从原有数据中查出分保比例
        		 String sqlrate = "select CessionRate from lrpolresult where CessionRate>0 and " +
        		 		"polno='"+tLRInterfaceSchema.getPolNo()+"' " +
        		 				" and recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
        		 						"fetch first 1 rows only with ur";
        		 SSRS tSSRS = new ExeSQL().execSQL(sqlrate);
        		 if(tSSRS!=null && tSSRS.MaxRow>0){
        			 String tRate = tSSRS.GetText(1, 1);
            		 tRealCessRate=Double.parseDouble(tRate);
                }
        		 else continue;
        		
        	 }
        	 else{
        	        	 
             double tcheck=calculatorRate(tCalRate,tLRInterfaceSchema,tLRContInfoSchema,tCode,tRiskAmnt);//调用实际分保比例
             if(tcheck==-1){//计算错误或不具备分保条件继续下一合同计算
            	 continue;
             }
             else{
            	 tRealCessRate=tcheck;
             }
        	 }
         }
         else{//直接获取页面值
        	
        	 tRealCessRate=getFactorValue(tCode,tLRContInfoSchema.getReContCode(),"CessionRate");
         }
         if(tRealCessRate<=0){
        	 this.errorLog(tLRInterfaceSchema.getPolNo(), "该保单分保比例小于等于0，为"+tRealCessRate);
        	 continue;
         }
         tCessAmnt=tRiskAmnt*tRealCessRate;
         //添加判断，如果是首期数据反冲的话，分出保额应该为负值
         if(0>tLRInterfaceSchema.getPrem())
         {
        	 tCessAmnt = -1*tCessAmnt;
         }
         /*
          * 如果是非首期数据成数分保判断是否需要插入年度化保费反冲数据
          * 因为首期年度化后还会取到续期数据，所以在取到这些续期数据时需要往lrpol表中插入负数据反冲
          */
         
         if(tCessionMode.equals("1")){
        	 if(tLRInterfaceSchema.getContType().equals("1")||(tLRInterfaceSchema.getContType().equals("2")
        			 &&tFlag.equals("1")))
        	 insertFC(tLRInterfaceSchema,tLRContInfoSchema);
         }
         	
        
         
         mRecontCode = tLRContInfoSchema.getReContCode();
     
         mRiskCalSort = getRiskCalSort(tLRInterfaceSchema,
                                       mRecontCode);
         LRPolSchema tLRPolSchema = new LRPolSchema();
         tReflections.transFields(tLRPolSchema, tLRInterfaceSchema);
         
         //获取成本中心，财务接口使用。
         
         if(agentGroup != null&& !agentGroup.equals(""))
         {
             tLRPolSchema.setCostCenter(CostCenter);
         }
         prepareLRPolData(tLRPolSchema, tLRInterfaceSchema.getStandPrem(),
                 tLRInterfaceSchema.getPrem(),tRealCessRate);
         if(Integer.parseInt(LRiskFlag)>0){
             tLRPolSchema.setReNewCount(tLRInterfaceSchema.getPolYear());
         }
         else{
             tLRPolSchema.setReNewCount(tLRInterfaceSchema.getReNewCount());
         }
         tLRPolDB.setSchema(tLRPolSchema);
         tLRPolDB.setCessAmnt(tCessAmnt);
         tLRPolDB.setRiskAmnt(tRiskAmnt);
         tLRPolDB.setReType(tLRContInfoSchema.getReType());
         if("1".equals(tLRContInfoSchema.getCessionMode())&&"Y".equals(tLRInterfaceSchema.getRenewalFlag())){
        	 tLRPolDB.setPolStat("4");//非首期标志
         }
         else if("1".equals(tLRContInfoSchema.getCessionMode())&&"-".equals(tLRInterfaceSchema.getRenewalFlag())){
        	 tLRPolDB.setPolStat("8");//保全有名单增人标志
         }
         else{
        	 tLRPolDB.setPolStat("7");//首期或者续保数据
         }
      
         if(tLRInterfaceSchema.getRiskCode().equals("5601")&&
        		 (tLRInterfaceSchema.getOccupationType()==null||
        				 tLRInterfaceSchema.getOccupationType().equals("")||
        				 tLRInterfaceSchema.getOccupationType().equals("0"))){
        	 tLRPolDB.setOccupationType("1");
         }
         
         tLRPolDB.delete();
         tLRPolDB.insert();
         tStateFlag=true;//只要有保单满足一个再保合同，则将tStateFlag置为true
         
     }
     if(tStateFlag){
    	 tLRInterfaceDB.setSchema(tLRInterfaceSchema);
         tLRInterfaceDB.setState("1");
         tLRInterfaceDB.update();
     }
     
    	return true;
    }
    //在成数续期时插入首期保费的负记录，以计算出续期反冲数据
    private boolean insertFC(LRInterfaceSchema tLRInterfaceSchema,LRContInfoSchema tLRContInfoSchema){
        SSRS tSSRS=null;
        if(tLRContInfoSchema.getCessionMode().equals("1")&&tLRInterfaceSchema.getReNewCount()==0&&"Y".equals(tLRInterfaceSchema.getRenewalFlag())&&
        		tLRInterfaceSchema.getPayIntv()!=0&&tLRInterfaceSchema.getPayIntv()!=12&&tLRInterfaceSchema.getPayIntv()!=-1){
        	String tRe="";
        	if(tLRInterfaceSchema.getContType().equals("2")){
        	 tRe="select count(1) from lrpol" +
			" where GrpPolNo='"+tLRInterfaceSchema.getGrpPolNo()+"' and recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
					" and getdatadate<='"+this.mToday+"' and renewcount=0 and ReinsureItem='C' with ur";
        	}
        	if(tLRInterfaceSchema.getContType().equals("1")){
           	 tRe="select count(1) from lrpol" +
   			" where polno='"+tLRInterfaceSchema.getPolNo()+"' and recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
   					" and getdatadate<='"+this.mToday+"' and renewcount=0 and ReinsureItem='C' with ur";
           	}
        	SSRS tsRe = new ExeSQL().execSQL(tRe);
        	String tnRe=tsRe.GetText(1, 1);
        	if(Integer.parseInt(tnRe)>0){
        		String tChecksql1="";
        		if(tLRInterfaceSchema.getContType().equals("2")){
        			tChecksql1="select count(1) from lrpol" +
        			" where GrpPolNo='"+tLRInterfaceSchema.getGrpPolNo()+"' and recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
        					" and getdatadate='"+this.mToday+"' and renewcount=0 and ReinsureItem='F' " +
        							"and payno='"+tLRInterfaceSchema.getPayNo()+"' with ur";
        		}
        		if(tLRInterfaceSchema.getContType().equals("1")){
        			tChecksql1="select count(1) from lrpol" +
        			" where PolNo='"+tLRInterfaceSchema.getPolNo()+"' and recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
					" and getdatadate='"+this.mToday+"' and renewcount=0 and ReinsureItem='F' " +
							"and payno='"+tLRInterfaceSchema.getPayNo()+"' with ur";
        		}
        			
        	SSRS tCheck1 = new ExeSQL().execSQL(tChecksql1);
        	String tcheck1=tCheck1.GetText(1, 1);
        	
        if(Integer.parseInt(tcheck1)==0){
        	
        	
        	
        		String str1="";
        		if(tLRInterfaceSchema.getContType().equals("2")){
        			        		
        		str1="select GrpContNo,GrpPolNo," +
        		"ContNo,PolNo,ProposalNo,PrtNo," +
        		"ReContCode,'F',TempCessFlag," +
        		"ReNewCount,ContType,ManageCom,RiskCalSort,MainPolNo," +
        		"MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
        		"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate," +
        		"EndDate,StandPrem,-prem,SumPrem,Amnt,InsurePeriod,OccupationType," +
        		"RiskAmnt,PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear," +
        		"InsuYearFlag,InsuYear,ProtItem,CessStart,CessEnd,'4',SignDate," +
        		"SumRiskAmount,NowRiskAmount,AppntNo,AppntName,AppntType,SaleChnl," +
        		"DeadAddFeeRate,DiseaseAddFeeRate,'"+this.mToday+"',Operator,current date,current time," +
        		"current date,current time,CostCenter,ReType,'','',CessAmnt,'"+mPayNo+"'," +
        				"contplancode,'"+tLRInterfaceSchema.getPolTypeFlag()+"' from lrpol a" +
        		" where grpcontno='"+tLRInterfaceSchema.getGrpContNo()+"' and riskcode='"+tLRInterfaceSchema.getRiskCode()+"'" +
        				" and renewcount=0 and conttype='2' and reinsureitem='C' and polstat='7' and " +
        				" recontcode='"+tLRContInfoSchema.getReContCode()+"' " +
        						"and getdatadate=(select min(getdatadate) from lrpol where " +
        						"grpcontno='"+tLRInterfaceSchema.getGrpContNo()+"' " +
        								" and riskcode='"+tLRInterfaceSchema.getRiskCode()+"' and renewcount=0 and conttype='2' and " +
        								" recontcode='"+tLRContInfoSchema.getReContCode()+"')";
//        		if("1".equals(tLRInterfaceSchema.getPolTypeFlag())||"无名单".equals(tLRInterfaceSchema.getInsuredName())){
//        			str1+=" fetch first 1 rows only ";
//        		}
        		}
        		else if (tLRInterfaceSchema.getContType().equals("1")){
        		str1="select GrpContNo,GrpPolNo," +
        		"ContNo,PolNo,ProposalNo,PrtNo," +
        		"ReContCode,'F',TempCessFlag," +
        		"ReNewCount,ContType,ManageCom,RiskCalSort,MainPolNo," +
        		"MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
        		"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate," +
        		"EndDate,StandPrem,-prem,SumPrem,Amnt,InsurePeriod,OccupationType," +
        		"RiskAmnt,PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear," +
        		"InsuYearFlag,InsuYear,ProtItem,CessStart,CessEnd,'4',SignDate," +
        		"SumRiskAmount,NowRiskAmount,AppntNo,AppntName,AppntType,SaleChnl," +
        		"DeadAddFeeRate,DiseaseAddFeeRate,'"+this.mToday+"',Operator,current date,current time," +
        		"current date,current time,CostCenter,ReType,'','',CessAmnt,'"+mPayNo+"'," +
        				"contplancode,'"+tLRInterfaceSchema.getPolTypeFlag()+"' from lrpol a" +
        		" where grpcontno='"+tLRInterfaceSchema.getGrpContNo()+"' and polno='"+tLRInterfaceSchema.getPolNo()+"' " +
        				"and renewcount=0 and conttype='1' and riskcode='"+tLRInterfaceSchema.getRiskCode()+"' and " +
        				" recontcode='"+tLRContInfoSchema.getReContCode()+"' "+
						" and reinsureitem='C' and polstat='7' and getdatadate=(select min(getdatadate) from lrpol where " +
						"grpcontno='"+tLRInterfaceSchema.getGrpContNo()+"' " +
								"and polno='"+tLRInterfaceSchema.getPolNo()+"' " +
								"and renewcount=0 and conttype='1' and riskcode='"+tLRInterfaceSchema.getRiskCode()+"' " +
										"and " +
								" recontcode='"+tLRContInfoSchema.getReContCode()+"') fetch first 1 rows only ";
        		}
        		ExeSQL tExeSQL = new ExeSQL();
        		tExeSQL.execUpdateSQL("insert into lrpol(GrpContNo,GrpPolNo,ContNo," +
            		"PolNo,ProposalNo,PrtNo,ReContCode,ReinsureItem," +
            		"TempCessFlag,ReNewCount,ContType,ManageCom,RiskCalSort," +
            		"MainPolNo,MasterPolNo,RiskCode,RiskVersion,InsuredNo,InsuredName," +
            		"InsuredSex,InsuredBirthday,InsuredAppAge,Years,CValiDate,EndDate," +
            		"StandPrem,Prem,SumPrem,Amnt,InsurePeriod,OccupationType,RiskAmnt," +
            		"PayIntv,PayMode,PayYears,PayEndYearFlag,PayEndYear,InsuYearFlag,InsuYear," +
            		"ProtItem,CessStart,CessEnd,PolStat,SignDate,SumRiskAmount,NowRiskAmount,AppntNo," +
            		"AppntName,AppntType,SaleChnl,DeadAddFeeRate,DiseaseAddFeeRate,GetDataDate,Operator," +
            		"MakeDate,MakeTime,ModifyDate,ModifyTime,CostCenter,ReType,ActuGetState,ActuGetNo," +
            		"CessAmnt,payno,contplancode,poltypeflag) " +str1
            		);
        		
        	}
        	}
        }
        return true ;
    }

    /**
     * 获取lrcalmode中计算编码的方法
     * @param strRiskCode 险种号
     * @param tReContCode 合同号
     * @param tReinsuritem 合同类型 ：正常，临分
     * @return tLRCalModeSchema
     */
    public LRCalModeSchema findCheckFieldByRiskCode(String strRiskCode, String tReContCode,String tReinsuritem) {

        Hashtable hash = m_hashLMCheckField;
        String strPK = strRiskCode.trim() + tReContCode.trim()+tReinsuritem.trim();
        Object obj = hash.get(strPK);

        if( obj != null ) {
          if( obj instanceof String ) {
            return null;
          } else {
            return (LRCalModeSchema)obj;
          }
        } else {
        	LRCalModeDB tLRCalModeDB = new LRCalModeDB();

        	tLRCalModeDB.setRiskCode(strRiskCode);
        	tLRCalModeDB.setReContCode(tReContCode);
        	//tLRCalModeDB.setFactorCode(tFactorCode);
        	LRCalModeSet tLRCalModeSet=tLRCalModeDB.query();
        	if(tLRCalModeSet.size()<=0){
        		hash.put(strPK, "NOFOUND");
        		return null;
        		
        	}
        	LRCalModeSchema tLRCalModeSchema = tLRCalModeSet.get(1);

          if( tLRCalModeDB.mErrors.needDealError() ) {
            mErrors.copyAllErrors(tLRCalModeDB.mErrors);
            hash.put(strPK, "NOFOUND");
            return null;
          }

          hash.put(strPK, tLRCalModeSchema);
          return tLRCalModeSchema;
        }
      }
    
    /**
     * 保单是否符合合同条件的方法
     * @param aLRPolSchema
     * @return CALCODE 计算编码
     */
    private String getCalCode(LRContInfoSchema tLRContInfoSchema,String tCode,String tReinsuritem,String tCalCode) {
    	
    	String t="0";
    	LRCalModeSchema tLRCalModeSchema
    	=findCheckFieldByRiskCode(tCode,tLRContInfoSchema.getReContCode(),tReinsuritem);
        
        if (tLRCalModeSchema==null) {
            return t;
        }
        if ((tLRCalModeSchema.getNegoCalCode() == null||tLRCalModeSchema.getNegoCalCode().equals("null")
        		||tLRCalModeSchema.getNegoCalCode().equals(""))&&"recont".equals(tCalCode)) {
            return t;
        }
        else if ((tLRCalModeSchema.getRiskAmntCalCode() == null||tLRCalModeSchema.getRiskAmntCalCode().equals("null")
        		||tLRCalModeSchema.getRiskAmntCalCode().equals(""))&&"riskamnt".equals(tCalCode)) {
            return t;
        }
        else if ((tLRCalModeSchema.getCessCommCalCode() == null||tLRCalModeSchema.getCessCommCalCode().equals("null")
        		||tLRCalModeSchema.getCessCommCalCode().equals(""))&&"rate".equals(tCalCode)) {
            return t;
        }
        if("recont".equals(tCalCode))//再保合同校验
        	t=tLRCalModeSchema.getNegoCalCode();
        else if("riskamnt".equals(tCalCode))//取风险保额计算公式
        	t=tLRCalModeSchema.getRiskAmntCalCode();
        else if("rate".equals(tCalCode))//取分保比例计算公式
        	t=tLRCalModeSchema.getCessCommCalCode();
        return t;
    }
    
    /**
     * 合同校验传参函数
     * @param tcalCode
     * @param tPolNO
     * @return 查询数据库描述中的结果
     */
    private String calculatorReCont(String tcalCode,String tPolNO,String tDate){
    	Calculator tCalculator = new Calculator();
    	tCalculator.setCalCode(tcalCode);
    	String tresult="0";
    	String t="";
        //增加基本要素
      
    	tCalculator.addBasicFactor("PolNO", tPolNO);
    	tCalculator.addBasicFactor("mDate", tDate);
    	t = tCalculator.calculate();
        if (t == null ||
        		t.equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的合同");
                return tresult;
         }
        tresult=t;
        return tresult;
        
    }
    
    /**
     * 调用计算风险保额函数
     * @param tcalCode
     * @param tLRInterfaceSchema
     * @param tCash
     * @param tRetype
     * @param tCode
     * @return 风险保额
     */
    private double calculatorRiskAmnt(String tcalCode,LRInterfaceSchema tLRInterfaceSchema,
    		double tCash,String tRetype,String tCode,String tDate,String tLRiskFlag){
    	Calculator tCalculator = new Calculator();
    	tCalculator.setCalCode(tcalCode);
    	double tresult=-1;

        //增加基本要素
      
    	tCalculator.addBasicFactor("PolNO", tLRInterfaceSchema.getPolNo());
    	tCalculator.addBasicFactor("RiskCode", tLRInterfaceSchema.getRiskCode());
    	tCalculator.addBasicFactor("Cash", String.valueOf(tCash));
    	tCalculator.addBasicFactor("mDate", tDate);
    	tCalculator.addBasicFactor("Amnt", String.valueOf(tLRInterfaceSchema.getAmnt()));
    	tCalculator.addBasicFactor("PolYear", String.valueOf(tLRInterfaceSchema.getPolYear()));
    	if("02".equals(tRetype)){//责任分保需要传入给付责任和责任编码
    	    	
    	LRDutyDB tLRDutyDB = new LRDutyDB();
        tLRDutyDB.setPolNo(tLRInterfaceSchema.getPolNo());
        tLRDutyDB.setGetDutyCode(tCode);
        
        if(Integer.parseInt(tLRiskFlag)>0){
          tLRDutyDB.setPolYear(tLRInterfaceSchema.getPolYear());
        }else{
           tLRDutyDB.setPolYear(tLRInterfaceSchema.getReNewCount());
       }

        LRDutySet tLRDutySet = tLRDutyDB.query();
        if(tLRDutySet.size()<=0){
    		
    		return tresult;
    		
    	}
        LRDutySchema tLRDutySchema=tLRDutySet.get(1).getSchema();
        tCalculator.addBasicFactor("DutyCode", tLRDutySchema.getDutyCode());
        tCalculator.addBasicFactor("GetDutyCode", tLRDutySchema.getGetDutyCode());
    	}
    	String t=tCalculator.calculate();
        if (t == null ||
        		t.equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的风险保额计算公式");
                return tresult;
         }
        
        tresult=Double.parseDouble(t);
        return tresult;
        
    }
    
    /**
     * 计算具体的分保比例
     * @param tcalCode
     * @param tLRInterfaceSchema
     * @param tLRContInfoSchema
     * @param tCode
     * @param tCessamnt
     * @return 分保比例
     */
    private double calculatorRate(String tcalCode,LRInterfaceSchema tLRInterfaceSchema,
    		LRContInfoSchema tLRContInfoSchema,String tCode,double tRiskAmnt){
    	Calculator tCalculator = new Calculator();
    	tCalculator.setCalCode(tcalCode);
    	double tresult=-1;
    	double tlimvalue=0.00;//最低再保保额
    	double tmaxvalue=0.00;//最高再保保额
    	double trate=0.00;//页面录入的分保比例
    	
    	tlimvalue=getFactorValue(tCode,tLRContInfoSchema.getReContCode(),"LowReAmnt");//最低分保保额
    	tmaxvalue=getFactorValue(tCode,tLRContInfoSchema.getReContCode(),"AutoLimit");//最高分保保额
    	trate=getFactorValue(tCode,tLRContInfoSchema.getReContCode(),"CessionRate");//页面录入分保比例
        //增加基本要素
      
//    	tCalculator.addBasicFactor("PolNO", tLRInterfaceSchema.getPolNo());
//    	tCalculator.addBasicFactor("RiskAmnt", String.valueOf(tRiskAmnt));
//    	tCalculator.addBasicFactor("LowReAmnt", String.valueOf(tlimvalue));
//    	tCalculator.addBasicFactor("AutoLimit", String.valueOf(tmaxvalue));
//    	tCalculator.addBasicFactor("CessionRate", String.valueOf(trate));
//    	
//    	String t=tCalculator.calculate();
//        if (t == null ||
//        		t.equals("")) {
//                buildError("calCessFeeRate", "没有适用此保单的分保比例计算公式");
//                return tresult;
//         }
        
        //tresult=Double.parseDouble(t);
    	if(tRiskAmnt<=tlimvalue){
    		return tresult;
    	}
    	else if(tRiskAmnt>tlimvalue&&tRiskAmnt<=tmaxvalue){
    		tresult=(tRiskAmnt-tlimvalue)*trate/tRiskAmnt;
    	}
    	else if(tRiskAmnt>tmaxvalue){
    		tresult=(tmaxvalue-tlimvalue)*trate/tRiskAmnt;
    	}
    	else
    		return tresult;
        return tresult;
        
    }
    
    /**
     * 取合同要素值函数
     * @param strRiskCode
     * @param tReContCode
     * @param tFactorCode
     * @return 描述表schema
     */
    public LRCalFactorValueSchema findCheckFieldByRiskCodeF(String strRiskCode, String tReContCode,String tFactorCode) {

        Hashtable hash = m_hashLMCheckField;
        String strPK = strRiskCode.trim() + tReContCode.trim()+tFactorCode.trim();
        Object obj = hash.get(strPK);

        if( obj != null ) {
          if( obj instanceof String ) {
            return null;
          } else {
            return (LRCalFactorValueSchema)obj;
          }
        } else {
        	LRCalFactorValueDB tLRCalFactorValueDB = new LRCalFactorValueDB();

        	tLRCalFactorValueDB.setRiskCode(strRiskCode);
        	tLRCalFactorValueDB.setReContCode(tReContCode);
        	tLRCalFactorValueDB.setFactorCode(tFactorCode);
        	LRCalFactorValueSet tLRCalFactorValueSet=tLRCalFactorValueDB.query();
        	if(tLRCalFactorValueSet.size()<=0){
        		hash.put(strPK, "NOFOUND");
        		return null;
        		
        	}
          LRCalFactorValueSchema tLRCalFactorValueSchema = tLRCalFactorValueSet.get(1);

          if( tLRCalFactorValueDB.mErrors.needDealError() ) {
            mErrors.copyAllErrors(tLRCalFactorValueDB.mErrors);
            hash.put(strPK, "NOFOUND");
            return null;
          }

          hash.put(strPK, tLRCalFactorValueSchema);
          return tLRCalFactorValueSchema;
        }
      }
    
   /**
    * 按要素取所需值
    * @param tRiskCode
    * @param tReContCode
    * @param tRetype
    * @return 要素值
    */
    private double getFactorValue(String tRiskCode,String tReContCode,String tRetype) {
       
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCodeF(tRiskCode,tReContCode,tRetype);
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }
   
    /**
     * 计算险种当年现金价值
     * @param cLCPolSet
     * @return tCash 保单现金价值
     */
    private double getCashValue(LCPolSet cLCPolSet)
    
    {
    	//特殊标签
    	double tCash=0.00;
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS = new SSRS();
    	SSRS ttSSRS = new SSRS();

    	//现价存在标志
    	boolean tFlag = false;
    	int j = 0;

    	//循环处理险种下的现金价值信息
    	for (int i = 1; i <= cLCPolSet.size(); i++)
    	{
    		String tRiskCode = cLCPolSet.get(i).getRiskCode();

        

    		//得到现金价值的算法描述
    		LMCalModeDB tLMCalModeDB = new LMCalModeDB();
    		//获取险种信息
    		tLMCalModeDB.setRiskCode(tRiskCode);
    		tLMCalModeDB.setType("X");
    		LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

    		//解析得到的SQL语句
    		String strSQL = "";
    		//如果描述记录唯一时处理
    		if (tLMCalModeSet.size() == 1)
    		{
    			strSQL = tLMCalModeSet.get(1).getCalSQL();
    		}
    		// 这个险种不需要取现金价值的数据
    		if (strSQL.equals(""))
    		{
    		}
    		else
    		{
    			tFlag = true;
    			
    			String tSql = "select riskname from lmriskapp where riskcode = '"
                    	+ cLCPolSet.get(i).getRiskCode() + "'";
    			tSSRS = tExeSQL.execSQL(tSql);

    			
    			j = j + 1;

    			Calculator calculator = new Calculator();
    			//设置基本的计算参数
    			calculator.addBasicFactor("ContNo", cLCPolSet.get(i)
    					.getContNo());
    			calculator.addBasicFactor("InsuredNo", cLCPolSet.get(i)
    					.getInsuredNo());
    			calculator.addBasicFactor("InsuredSex", cLCPolSet.get(i)
    					.getInsuredSex());
    			calculator.addBasicFactor("InsuredAppAge",  String
    					.valueOf(cLCPolSet.get(i).getInsuredAppAge()));
    			calculator.addBasicFactor("PayIntv", String.valueOf(cLCPolSet
    					.get(i).getPayIntv()));
    			calculator.addBasicFactor("PayEndYear", String
    					.valueOf(cLCPolSet.get(i).getPayEndYear()));
    			calculator.addBasicFactor("PayEndYearFlag", String
    					.valueOf(cLCPolSet.get(i).getPayEndYearFlag()));
    			calculator.addBasicFactor("PayYears", String.valueOf(cLCPolSet
    					.get(i).getPayYears()));
    			calculator.addBasicFactor("InsuYear", String.valueOf(cLCPolSet
    					.get(i).getInsuYear()));
    			calculator.addBasicFactor("Prem", String.valueOf(cLCPolSet.get(
    					i).getPrem()));
    			calculator.addBasicFactor("Amnt", String.valueOf(cLCPolSet.get(
    					i).getAmnt()));
    			calculator.addBasicFactor("FloatRate", String.valueOf(cLCPolSet
    					.get(i).getFloatRate()));
    			calculator.addBasicFactor("Mult", String.valueOf(cLCPolSet.get(
    					i).getMult()));
    			//add by yt 2004-3-10
    			calculator.addBasicFactor("InsuYearFlag", String
    					.valueOf(cLCPolSet.get(i).getInsuYearFlag()));
    			calculator.addBasicFactor("GetYear", String.valueOf(cLCPolSet
    					.get(i).getGetYear()));
    			calculator.addBasicFactor("GetYearFlag", String
    					.valueOf(cLCPolSet.get(i).getGetYearFlag()));
    			calculator.addBasicFactor("CValiDate", String.valueOf(cLCPolSet
    					.get(i).getCValiDate()));
    			calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
    			strSQL = calculator.getCalSQL();
    			String tyearsql="select year(date('"+mToday+"')-cvalidate) from lcpol where " +
    			"polno='"+cLCPolSet.get(i).getPolNo()+"'  with ur";
    	    	SSRS tSSRSY = new ExeSQL().execSQL(tyearsql);
    	    	int tyears=Integer.parseInt(tSSRSY.GetText(1, 1));
    			

    			System.out.println(strSQL);
    			//执行现金价值计算sql，获取现金价值列表
    				ttSSRS = tExeSQL.execSQL(strSQL);
    				if(ttSSRS.MaxRow>0)
    					if(tyears!=0)
    					tCash=Double.parseDouble(ttSSRS.GetText(tyears, 3));
    					System.out.println("********************"+tCash+"**************");
    					
    				
    		}
    }
   
    	return tCash;
    }
    /*针对新合同的分保保额的计算*/
    
   
    /**
     *
     * @return double
     */
    private boolean prepareLRPolData(LRPolSchema aLRPolSchema,
                                     double aStandPrem, double aPrem,double tRealCessRate) {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        aLRPolSchema.setReContCode(this.mRecontCode); //再保合同代码
        aLRPolSchema.setReinsureItem("C"); //再保项目 L--法定分保 C--商业分保
        aLRPolSchema.setTempCessFlag("N"); //非临分
        aLRPolSchema.setRiskCalSort(this.mRiskCalSort); //险种计算代码
        aLRPolSchema.setGetDataDate(this.mToday);
        aLRPolSchema.setOperator("Server");
        aLRPolSchema.setMakeDate(currentDate);
        aLRPolSchema.setMakeTime(currentTime);
        aLRPolSchema.setModifyDate(currentDate);
        aLRPolSchema.setModifyTime(currentTime);
        double tAddPremRate = getPolAddPremRate(aStandPrem, aPrem); //得到加费率,
        aLRPolSchema.setDiseaseAddFeeRate(tAddPremRate);
        aLRPolSchema.setDeadAddFeeRate(tRealCessRate);//该字段现在存储分保比例值

        return true;
    }

    /**
     * 计算死亡加费，重疾加费,这里由于契约不区分死亡加费和重疾加费，所以统一计算并存储于DiseaseAddFeeRate
     * @return double
     */
    private double getPolAddPremRate(double aStandPrem, double aPrem) {
        if (Math.abs(aStandPrem - 0.00) < 0.00001 || (aPrem - aStandPrem) <= 0) {
            return 0;
        }
        return (aPrem - aStandPrem) / aStandPrem;
    }

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(String aPolNo, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aPolNo);
        tLRErrorLogSchema.setLogType("2");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator("Server");
        tLRErrorLogSchema.setManageCom("86");
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }



    /**
     * 得到险种类别
     * @return boolean
     */
    private String getRiskCalSort(LRInterfaceSchema tLRInterfaceSchema, String aLRContCode) {
        SSRS tRS = new ExeSQL().execSQL("select retype from LRContInfo where ReContCode='"+aLRContCode+"'");
        if (tRS == null || tRS.getMaxRow() <= 0) {
            buildError("insertData", "没有该合同信息!");
        }
            SSRS tSSRS=new SSRS();
            String strSQL ="select distinct risksort from LRCalFactorValue where "+
            " ReContCode = '" + aLRContCode + "' with ur";
            tSSRS = new ExeSQL().execSQL(strSQL);

        if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
            buildError("insertData", "没有该险种的险种类别!");
            return "";
        }
        return tSSRS.GetText(1, 1);
    }

    /**
    * 获取责任的给付责任号
    * @param aLRPolSchema LRPolSchema
    * @return double
    */
   private String getGetDutyCode(LRContInfoSchema aLRContInfoSchema) {
       String strSQL =
               "select distinct(riskcode) from lrcalfactorvalue where  " +
               " recontcode='" + aLRContInfoSchema.getReContCode() +
               "' with ur";
       SSRS tSSRS = new ExeSQL().execSQL(strSQL);
       if (tSSRS.getMaxRow() <= 0) {
           return "-1";
       }
       if (tSSRS.GetText(1, 1).equals("") || tSSRS.GetText(1, 1) == null) {
           return "-1";
       }
       return tSSRS.GetText(1, 1);
   }
   




   
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(String mToDay) {
       
        mStartDate = mToDay;
        
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
//        cError.moduleName = "LRGetCessDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }


    public static void main(String[] args) {
        LRNewGetCessDataTBL tLRNewGetCessDataBL = new LRNewGetCessDataTBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        String testRecontCode = "CLR2017A01-1";
        String mOperateType = "CESSDATA";

        String startDate = "2017-1-1";

//        tLRNewGetCessDataBL.submitDataT(mOperateType,startDate);
        System.out.println(testRecontCode.substring(0, 8));
    }
}
