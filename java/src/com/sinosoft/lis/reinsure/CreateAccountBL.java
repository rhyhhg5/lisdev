package com.sinosoft.lis.reinsure;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Date;

import com.sinosoft.lis.db.LRAccountsDB;
import com.sinosoft.lis.db.LRComRiskResultDB;
import com.sinosoft.lis.db.LRContInfoDB;
import com.sinosoft.lis.db.LRPolClmDB;
import com.sinosoft.lis.db.LRPolClmResultDB;
import com.sinosoft.lis.db.LRPolDB;
import com.sinosoft.lis.db.LRPolEdorDB;
import com.sinosoft.lis.db.LRPolEdorResultDB;
import com.sinosoft.lis.db.LRPolResultDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRAccountsSchema;
import com.sinosoft.lis.schema.LRComRiskResultSchema;
import com.sinosoft.lis.schema.LRPolClmSchema;
import com.sinosoft.lis.schema.LRPolEdorSchema;
import com.sinosoft.lis.schema.LRPolSchema;
import com.sinosoft.lis.vschema.LRAccountsSet;
import com.sinosoft.lis.vschema.LRComRiskResultSet;
import com.sinosoft.lis.vschema.LRContInfoSet;
import com.sinosoft.lis.vschema.LRPolClmResultSet;
import com.sinosoft.lis.vschema.LRPolClmSet;
import com.sinosoft.lis.vschema.LRPolEdorResultSet;
import com.sinosoft.lis.vschema.LRPolEdorSet;
import com.sinosoft.lis.vschema.LRPolResultSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import com.sinosoft.utility.StrTool;

public class CreateAccountBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String strOperate;
    private PubSubmit tPubSubmit = new PubSubmit();
    private TransferData mTransferData = new TransferData();
    private String mStartDate="";
    private String mEndDate="";
    private String mActugetNo="";
    private String mRecontNo="";
    private LRComRiskResultSet mLRComRiskResultSet;


    //业务处理相关变量
    /** 全局数据 */

    public CreateAccountBL() {
    }


    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReContInterfaceGetDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in CreateAccoutBL.Submit..............");
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        //清单审核
        if(strOperate.equals("CHECK")){
            if (!dealData()) {
                return false;
            }
        }
        //账单审核通过
        if(strOperate.equals("CONFIRM")){
           if (!dealAccountData()) {
               return false;
           }
       }
        //账单结算
        if(strOperate.equals("JIESUAN")){
           if (!Account()) {
               return false;
           }
       }
        return true;
    }


    /**
     * 如果分保,理赔,保全明细全部确认通过，账单置为待审核状态01
     */
    public  boolean dealData() {
           String tReContCode = (String)mTransferData.getValueByName("ReContCode");
           String strWhere = "";
           if(!StrTool.cTrim(tReContCode).equals("")){
              strWhere+=" and ReContCode ='"+tReContCode+"' ";
           }
           //查询是否有未审核的清单存在
            String tSql="select count(1) from ("
            + "select 1 from lrpol a,lrpolresult c where a.polno=c.polno and a.recontcode=c.recontcode  and a.actugetstate='00' and a.getdatadate between'"+mStartDate+"' and '"+mEndDate+"' "+strWhere
            +"union all select 1 from lrpoledor a,lrpoledorresult b where a.polno=b.polno and a.recontcode=b.recontcode and a.edorno=b.edorno and " +
            		" a.actugetstate='00' and a.getdatadate between'"+mStartDate+"' and '"+mEndDate+"' "+strWhere
            +"union all select 1 from lrpolclm a,lrpolclmresult b where a.polno=b.polno and a.recontcode=b.recontcode and a.clmno=b.clmno and " +
            		"actugetstate='00' and getdatadate between'"+mStartDate+"' and '"+mEndDate+"'" +strWhere+" ) v with ur";
            SSRS tSSRS = new ExeSQL().execSQL(tSql);
           if(tSSRS==null || tSSRS.getMaxRow()==0){
                return false;
            }
           if(Integer.parseInt(tSSRS.GetText(1, 1))==0){
                LRAccountsDB tLRAccountsDB=new LRAccountsDB();
                tLRAccountsDB.setStartDate(mStartDate);
                tLRAccountsDB.setEndData(mEndDate);
                if(!StrTool.cTrim(tReContCode).equals("")){
                    tLRAccountsDB.setRecontCode(tReContCode);
                }
                LRAccountsSet tLRAccountsSet=tLRAccountsDB.query();
                if(tLRAccountsSet.size()>0){
                      Connection con=null;
                      con=DBConnPool.getConnection();
                    try{
                        con.setAutoCommit(false);
                        for (int i = 1; i <= tLRAccountsSet.size(); i++) {
                            LRAccountsSchema tLRAccountsSchema = tLRAccountsSet.
                                    get(i);
                            tLRAccountsSchema.setPayFlag("01");
                            tLRAccountsSchema.setModifyDate(PubFun.
                                    getCurrentDate());
                            tLRAccountsSchema.setModifyTime(PubFun.
                                    getCurrentTime());
                            prepareAccount(tLRAccountsSchema); //准备账单中的各种钱数
                            LRAccountsDB ttLRAccountsDB = new LRAccountsDB();
                            ttLRAccountsDB.setSchema(tLRAccountsSchema);
                            ttLRAccountsDB.update();
                            insertFeeInterface(con, tLRAccountsSchema);//准备分机构分险种数据
                        }
                        con.commit();
                   }catch(Exception e){
                    try{
                       con.close();
                     }catch(Exception ex){}
                     }
                }
            }
        return true;
    }
    /**
     * 准备再保账单表数据。分保保费，分保手续费，理赔摊回
     * @param tLRAccountsSchema LRAccountsSchema
     * @return boolean
     */
    public boolean prepareAccount(LRAccountsSchema tLRAccountsSchema){
        String actuno = tLRAccountsSchema.getActuGetNo();
        String sqlCess = "select value(sum(b.Cessprem),0),value(sum(ReProcFee),0) from lrpol a ,lrpolresult b where a.polno=b.polno and a.renewcount=b.renewcount "+
                         "and a.recontcode=b.recontcode and a.reinsureitem=b.reinsureitem and a.ActuGetNo='"+actuno+"' "+
                         "and a.ActuGetState in ('01','02') and a.getdatadate between'"+mStartDate+"' and '"+mEndDate+"' with ur";
        SSRS cessRes = new ExeSQL().execSQL(sqlCess);
        String sqlEdor = "select value(sum(b.EdorBackFee),0),value(sum(EdorProcFee),0) from lrpoledor a ,lrpoledorresult b where a.polno=b.polno and a.edorno=b.edorno "+
                         "and a.recontcode=b.recontcode and a.reinsureitem=b.reinsureitem and a.ActuGetNo='"+actuno+"'"+
                         "and a.ActuGetState in ('01','02') and a.getdatadate between'"+mStartDate+"' and '"+mEndDate+"' with ur";
        SSRS edorRes = new ExeSQL().execSQL(sqlEdor);
        String sqlClm = "select value(sum(b.ClaimBackFee),0) from lrpolclm a ,lrpolclmresult b where a.polno=b.polno and a.clmno=b.clmno "+
                        "and a.recontcode=b.recontcode and a.reinsureitem=b.reinsureitem and a.ActuGetNo='"+actuno+"' "+
                        "and a.ActuGetState in ('01','02') and a.getdatadate between'"+mStartDate+"' and '"+mEndDate+"' with ur";
        String clmRes = new ExeSQL().getOneValue(sqlClm);
        double aCessPrem=0.0;
        double aReprocFee = 0.0;
        double aClaimBackFee= 0.0;
         aCessPrem = Double.parseDouble(cessRes.GetText(1,1))+Double.parseDouble(edorRes.GetText(1,1));
         aReprocFee = Double.parseDouble(cessRes.GetText(1,2))+Double.parseDouble(edorRes.GetText(1,2));
         aClaimBackFee = Double.parseDouble(clmRes);
        tLRAccountsSchema.setCessPrem(new DecimalFormat("0.00").format(aCessPrem));
        tLRAccountsSchema.setReProcFee(new DecimalFormat("0.00").format(aReprocFee));
        tLRAccountsSchema.setClaimBackFee(new DecimalFormat("0.00").format(aClaimBackFee));
        tLRAccountsSchema.setBalanceLend(aCessPrem-aReprocFee-aClaimBackFee);
        tLRAccountsSchema.setSumLoan(aCessPrem);
        tLRAccountsSchema.setSumLead(aCessPrem);
        return true;
    }
    /**
     * 如果账单审核通过，账单置为结算状态02
     */
    public  boolean dealAccountData() {
         if(mLRComRiskResultSet.size()<=0){
             return false;
         }
//        if(actugetno==null)return false;
        for(int i=1;i<=mLRComRiskResultSet.size();i++){
            String tSql="select count(*) from LRComRiskResult where  standbyflag2!='02' and standbyflag1='"+mLRComRiskResultSet.get(i).getStandbyFlag1()+"' with ur";
            SSRS tSSRS = new ExeSQL().execSQL(tSql);
            if(tSSRS==null || tSSRS.getMaxRow()==0){
                return false;
            }
            if(Integer.parseInt(tSSRS.GetText(1, 1))==0){
                LRAccountsDB tLRAccountsDB=new LRAccountsDB();
//                tLRAccountsDB.setStartDate(mStartDate);
//                tLRAccountsDB.setEndData(mEndDate);
                tLRAccountsDB.setActuGetNo(mLRComRiskResultSet.get(i).getStandbyFlag1());
                LRAccountsSet tLRAccountsSet=tLRAccountsDB.query();
                if(tLRAccountsSet.size()>0){
                    tLRAccountsDB.setSchema(tLRAccountsSet.get(1));
                    tLRAccountsDB.setPayFlag("02");
                    tLRAccountsDB.setModifyDate(PubFun.getCurrentDate());
                    tLRAccountsDB.setModifyTime(PubFun.getCurrentTime());
                    tLRAccountsDB.update();
                }
            }
        }
        return true;
    }
    /**
     * 如果所有明细全部审核通过，账单置为结算状态03
     */
    public  boolean Account() {
        if(mLRComRiskResultSet.size()<=0){
             return false;
         }
        for(int i=1;i<=mLRComRiskResultSet.size();i++){
            String tSql="select count(*) from LRComRiskResult where  standbyflag2!='03' and standbyflag1='"+mLRComRiskResultSet.get(i).getStandbyFlag1()+"' with ur";
            SSRS tSSRS = new ExeSQL().execSQL(tSql);
            if(tSSRS==null || tSSRS.getMaxRow()==0){
                return false;
            }
            if(Integer.parseInt(tSSRS.GetText(1, 1))==0){
                LRAccountsDB tLRAccountsDB=new LRAccountsDB();
                tLRAccountsDB.setActuGetNo(mLRComRiskResultSet.get(i).getStandbyFlag1());
                LRAccountsSet tLRAccountsSet=tLRAccountsDB.query();
                if(tLRAccountsSet.size()>0){
                    tLRAccountsDB.setSchema(tLRAccountsSet.get(1));
                    tLRAccountsDB.setPayFlag("03");
                    tLRAccountsDB.setPayDate(PubFun.getCurrentDate());
                    tLRAccountsDB.setModifyDate(PubFun.getCurrentDate());
                    tLRAccountsDB.setModifyTime(PubFun.getCurrentTime());
                    tLRAccountsDB.update();
                }
            }
        }
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if(mTransferData==null){
             buildError("getInputData().mTransferData","数据信息不足");
            return false;
        }
         mLRComRiskResultSet = (LRComRiskResultSet) cInputData.getObjectByObjectName(
                "LRComRiskResultSet", 0);
        if(strOperate.equals("CONFIRM")&& mLRComRiskResultSet==null){
            buildError("getInputData().mLRComRiskResultSet","数据信息不足");
            return false;
        }
        String tYear = (String)mTransferData.getValueByName("Year");
        String tMonth = (String)mTransferData.getValueByName("Month");
        if(!StrTool.cTrim(tYear).equals("") && !StrTool.cTrim(tMonth).equals("")){
           Calendar calendar=Calendar.getInstance();
           calendar.set(Calendar.YEAR,Integer.parseInt(tYear));
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth));
           calendar.set(Calendar.DATE,1);
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           String tCurrentDate = sdf.format(calendar.getTime());
           calendar.set(Calendar.MONTH,Integer.parseInt(tMonth)-1);
           if("12".equals(tMonth))
           	mStartDate=tYear+"-"+tMonth+"-1";
           else
           	mStartDate=sdf.format(calendar.getTime());
           
           mEndDate= PubFun.calDate(tCurrentDate,-1,"D","2009-1-31");  //结束日期取本月最后一天
       }else{
           buildError("getInputData","处理日期出错");
           return false;
       }
        return true;
    }

    /*
     *
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "CreateAccountBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 向账单表中置总钱数 …………暂时弃之不用
     * @param tLRAccountsDB LRAccountsDB
     * @return boolean
     */
    private boolean dealAccount(LRAccountsDB tLRAccountsDB){
        double tClaimBackFee=0;//摊回赔款
        double tCessPrem=0;//分出保费
        double tReprocFee=0;//再保手续费
        String tSql="select * from lrpol where ActuGetNo ='"+tLRAccountsDB.getActuGetNo()+"' with ur ";
        RSWrapper rswrapper = new RSWrapper();
        LRPolSet tLRPolSet = new LRPolSet();
        rswrapper.prepareData(tLRPolSet, tSql);
        do {
            rswrapper.getData();
            for (int i = 1; i <= tLRPolSet.size(); i++) {
                LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                LRPolResultDB tLRPolResultDB=new LRPolResultDB();
                tLRPolResultDB.setPolNo(tLRPolSchema.getPolNo());
                tLRPolResultDB.setReNewCount(tLRPolSchema.getReNewCount());
                LRPolResultSet tLRPolResultSet=tLRPolResultDB.query();
                for(int j=1;j<=tLRPolResultSet.size();j++){
                    tCessPrem+=tLRPolResultSet.get(j).getCessPrem();
                    tReprocFee+=tLRPolResultSet.get(j).getChoiRebaFee();
                    tReprocFee+=tLRPolResultSet.get(j).getReProcFee();
                    tReprocFee+=tLRPolResultSet.get(j).getSpeCemm();
                }
            }
         }while (tLRPolSet.size() > 0);
//      保全
        tSql="select * from LRPolEdor where ActuGetNo ='"+tLRAccountsDB.getActuGetNo()+"' with ur";
        LRPolEdorSet tLRPolEdorSet=new LRPolEdorSet();
        rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolEdorSet, tSql);
        do {
            rswrapper.getData();
            for (int i = 1; i <= tLRPolEdorSet.size(); i++) {
                LRPolEdorSchema tLRPolEdorSchema=tLRPolEdorSet.get(i);
                LRPolEdorResultDB tLRPolEdorResultDB=new LRPolEdorResultDB();
                tLRPolEdorResultDB.setPolNo(tLRPolEdorSchema.getPolNo());
                tLRPolEdorResultDB.setReNewCount(tLRPolEdorSchema.getReNewCount());
                tLRPolEdorResultDB.setEdorNo(tLRPolEdorSchema.getEdorNo());
                LRPolEdorResultSet tLRPolEdorResultSet=tLRPolEdorResultDB.query();
                for(int j=1;j<=tLRPolEdorResultSet.size();j++){
                    tCessPrem-=tLRPolEdorResultSet.get(j).getEdorBackFee();
                    tReprocFee-=tLRPolEdorResultSet.get(j).getEdorProcFee();
                }
            }
         }while (tLRPolEdorSet.size() > 0);
        //理赔
        tSql="select * from LRPolClm where ActuGetNo ='"+tLRAccountsDB.getActuGetNo()+"' with ur";
        LRPolClmSet tLRPolClmSet=new LRPolClmSet();
        rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolClmSet, tSql);
        do {
            rswrapper.getData();
            for (int i = 1; i <= tLRPolClmSet.size(); i++) {
                LRPolClmSchema tLRPolClmSchema=tLRPolClmSet.get(i);
                LRPolClmResultDB tLRPolClmResultDB=new LRPolClmResultDB();
                tLRPolClmResultDB.setPolNo(tLRPolClmSchema.getPolNo());
                tLRPolClmResultDB.setReNewCount(tLRPolClmSchema.getReNewCount());
                tLRPolClmResultDB.setCaseNo(tLRPolClmSchema.getCaseNo());
                tLRPolClmResultDB.setReinsureItem(tLRPolClmSchema.getReinsureItem());
                LRPolClmResultSet tLRPolClmResultSet=tLRPolClmResultDB.query();
                for(int j=1;j<=tLRPolClmResultSet.size();j++){
                    tClaimBackFee+=tLRPolClmResultSet.get(j).getClaimBackFee();
                }
            }
         }while (tLRPolClmSet.size() > 0);
        tLRAccountsDB.setCessPrem(new DecimalFormat("0.00").format(tCessPrem));
        tLRAccountsDB.setReProcFee(new DecimalFormat("0.00").format(tReprocFee));
        tLRAccountsDB.setClaimBackFee(new DecimalFormat("0.00").format(tClaimBackFee));

        return true;
    }

    /**
     * 准备账单机构险种表数据--modify 09/03/20
     * @param con Connection
     * @param StartDate String
     * @param EndDate String
     * @param RecontCode String
     * @return boolean
     */
    private boolean insertFeeInterface(Connection con,LRAccountsSchema tLRAccountsSchema){
        String StartDate = tLRAccountsSchema.getStartDate();
        String EndDate = tLRAccountsSchema.getEndData();
        String terms = " and a.GetDataDate between '"+StartDate+"' AND '"+EndDate+"' ";
        terms = terms + " and a.recontcode = '"+tLRAccountsSchema.getRecontCode()+"'";

        String cessprem = "select a.managecom,a.riskcode,a.recontcode,a.costcenter,sum(d.CessPrem),sum(d.ReProcFee) from lrpol a,lrpolresult d"
                    +" where a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ReNewCount = d.ReNewCount"
                    + terms
                    +" group by a.managecom,a.riskcode,a.recontcode,a.costcenter with ur";
        System.out.println(cessprem);
        SSRS tSSRS =  new ExeSQL().execSQL(cessprem);
        int count = tSSRS.getMaxRow();
//        LRComRiskResultSet tLRComRiskResultSet = new LRComRiskResultSet();
        for(int i=1;i<=count;i++){
            LRComRiskResultSchema tLRComRiskResultSchema = new LRComRiskResultSchema();
            tLRComRiskResultSchema.setManageCom(tSSRS.GetText(i,1));
            tLRComRiskResultSchema.setRiskCode(tSSRS.GetText(i,2));
            tLRComRiskResultSchema.setRecontCode(tSSRS.GetText(i,3));
            if(tSSRS.GetText(i,4)!= null&&!tSSRS.GetText(i,4).equals("")){
                tLRComRiskResultSchema.setCostCenter(tSSRS.GetText(i, 4));
            }else{
                tLRComRiskResultSchema.setCostCenter("aa");
            }
            tLRComRiskResultSchema.setCessPrem(Double.parseDouble(tSSRS.GetText(i,5)));// 分保保费
            tLRComRiskResultSchema.setReProcFee(Double.parseDouble(tSSRS.GetText(i,6)));//分保手续费
            tLRComRiskResultSchema.setSumPrem(Double.parseDouble(tSSRS.GetText(i,5)));
            tLRComRiskResultSchema.setSumFee(Double.parseDouble(tSSRS.GetText(i,6)));
            tLRComRiskResultSchema.setStartDate(tLRAccountsSchema.getStartDate());
            tLRComRiskResultSchema.setEndData(EndDate);
            tLRComRiskResultSchema.setStandbyFlag1(tLRAccountsSchema.getActuGetNo());//存储账单号码
            tLRComRiskResultSchema.setOperator(this.globalInput.Operator);
            tLRComRiskResultSchema.setMakeDate(PubFun.getCurrentDate());
            tLRComRiskResultSchema.setMakeTime(PubFun.getCurrentTime());
            tLRComRiskResultSchema.setModifyDate(PubFun.getCurrentDate());
            tLRComRiskResultSchema.setModifyTime(PubFun.getCurrentTime());
            LRComRiskResultDB tLRComRiskResultDB=new LRComRiskResultDB(con);
            tLRComRiskResultDB.setSchema(tLRComRiskResultSchema);
            tLRComRiskResultDB.delete();
            tLRComRiskResultDB.insert();
        }

        String  edorfee = "select a.managecom,a.riskcode,a.recontcode,a.costcenter,sum(d.EdorBackFee), sum(d.EdorProcFee) from LRPolEdor a, LRPolEdorResult d"
                          +" where a.polno = d.Polno and a.RecontCode = d.RecontCode "
                          + terms
                          +" group by a.managecom,a.riskcode,a.recontcode,a.costcenter"
                          +" with ur";
        System.out.println(edorfee);
        SSRS tEDORSSRS =  new ExeSQL().execSQL(edorfee);
        int countedor = tEDORSSRS.getMaxRow();
//        tLRComRiskResultSet.clear(); //清空set中分保保费有关的值。
        for(int j=1;j<=countedor;j++){
            String costcenterterm = null;//如果成本中心字段为空，则置统一值;若成本中心有值，则将成本中心值赋给该字段。兼容之前的成本中心为空的记录。
            if(tEDORSSRS.GetText(j,4)==null ||tEDORSSRS.GetText(j,4).equals("")){
                costcenterterm = "aa";
            }else{
                costcenterterm = tEDORSSRS.GetText(j,4);
            }
            String result1 = "select * from LRComRiskResult where managecom='"+tEDORSSRS.GetText(j,1)
                             +"' and RiskCode='"+tEDORSSRS.GetText(j,2)
                             +"' and recontcode='"+tEDORSSRS.GetText(j,3)
                             +"' and costcenter='"+costcenterterm
                             +"' and StartDate='"+StartDate+"' and EndData='"+EndDate+"' with ur";
            System.out.println(result1);
           LRComRiskResultDB tLRComRiskResultDB = new LRComRiskResultDB();
           LRComRiskResultSet ttLRComRiskResultSet = tLRComRiskResultDB.executeQuery(result1);
           if(ttLRComRiskResultSet.size()==1){
               //该条记录已经存在，则把它的保全分保数据更新。
              ttLRComRiskResultSet.get(1).setEdorBackFee(Double.parseDouble(tEDORSSRS.GetText(j,5)));
              ttLRComRiskResultSet.get(1).setEdorProcFee(Double.parseDouble(tEDORSSRS.GetText(j,6)));
              ttLRComRiskResultSet.get(1).setSumPrem(ttLRComRiskResultSet.get(1).getSumPrem()+Double.parseDouble(tEDORSSRS.GetText(j,5)));//分保保费合计
              ttLRComRiskResultSet.get(1).setSumFee(ttLRComRiskResultSet.get(1).getSumFee()+Double.parseDouble(tEDORSSRS.GetText(j,6)));//手续费合计
//              tLRComRiskResultSet.add(ttLRComRiskResultSet);
              ttLRComRiskResultSet.get(1).setStandbyFlag1(tLRAccountsSchema.getActuGetNo());//存储账单号码
              LRComRiskResultDB ttLRComRiskResultDB=new LRComRiskResultDB(con);
              ttLRComRiskResultDB.setSchema(ttLRComRiskResultSet.get(1));
              ttLRComRiskResultDB.delete();
              ttLRComRiskResultDB.insert();

           }else if(ttLRComRiskResultSet.size()<=0){
               //该条记录不存在，则创建新的记录，记录它的保全分保数据。
            LRComRiskResultSchema ttLRComRiskResultSchema = new LRComRiskResultSchema();
            ttLRComRiskResultSchema.setManageCom(tEDORSSRS.GetText(j,1));
            ttLRComRiskResultSchema.setRiskCode(tEDORSSRS.GetText(j,2));
            ttLRComRiskResultSchema.setRecontCode(tEDORSSRS.GetText(j,3));
            if(tEDORSSRS.GetText(j,4)!= null&&!tEDORSSRS.GetText(j,4).equals("")){
                ttLRComRiskResultSchema.setCostCenter(tEDORSSRS.GetText(j, 4));
            }else{
                ttLRComRiskResultSchema.setCostCenter("aa");
            }

            ttLRComRiskResultSchema.setEdorBackFee(Double.parseDouble(tEDORSSRS.GetText(j,5)));// 保全摊回分保保费
            ttLRComRiskResultSchema.setEdorProcFee(Double.parseDouble(tEDORSSRS.GetText(j,6)));//保全摊回分保手续费
            ttLRComRiskResultSchema.setSumPrem(Double.parseDouble(tEDORSSRS.GetText(j,5)));//分保保费合计
            ttLRComRiskResultSchema.setSumFee(Double.parseDouble(tEDORSSRS.GetText(j,6)));//手续费合计
            ttLRComRiskResultSchema.setStartDate(StartDate);
            ttLRComRiskResultSchema.setEndData(EndDate);
            ttLRComRiskResultSchema.setStandbyFlag1(tLRAccountsSchema.getActuGetNo());//存储账单号码
            ttLRComRiskResultSchema.setOperator(this.globalInput.Operator);
            ttLRComRiskResultSchema.setMakeDate(PubFun.getCurrentDate());
            ttLRComRiskResultSchema.setMakeTime(PubFun.getCurrentTime());
            ttLRComRiskResultSchema.setModifyDate(PubFun.getCurrentDate());
            ttLRComRiskResultSchema.setModifyTime(PubFun.getCurrentTime());
            LRComRiskResultDB ttLRComRiskResultDB=new LRComRiskResultDB(con);
            ttLRComRiskResultDB.setSchema(ttLRComRiskResultSchema);
            ttLRComRiskResultDB.delete();
            ttLRComRiskResultDB.insert();
           }
        }
       //处理理赔数据，查询出所有理赔赔款数据。
       String  claimfee = "select a.managecom,a.riskcode,a.recontcode,a.costcenter,sum(e.ClaimBackFee) from LRPolClm a, LRPolClmResult e"
                         +" where  a.PolNo = e.Polno and a.RecontCode = e.RecontCode and a.ClmNo = e.ClmNo"
                         + terms
                         +" group by a.managecom,a.riskcode,a.recontcode,a.costcenter"
                         +" with ur";
      System.out.println(claimfee);//
       SSRS tCLAIMSSRS =  new ExeSQL().execSQL(claimfee);
       int countclaim = tCLAIMSSRS.getMaxRow();
//       tLRComRiskResultSet.clear(); //清空set中保全分何保费有关的值。
       for(int k=1;k<=countclaim;k++){
           String costcenterterm = null;//如果成本中心字段为空，则置统一值;若成本中心有值，则将成本中心值赋给该字段。兼容之前的成本中心为空的记录。
            if(tCLAIMSSRS.GetText(k,4)==null ||tCLAIMSSRS.GetText(k,4).equals("")){
                costcenterterm = "aa";
            }else{
                costcenterterm = tCLAIMSSRS.GetText(k,4);
            }

           String result1 = "select * from LRComRiskResult where managecom='"+tCLAIMSSRS.GetText(k,1)
                            +"' and RiskCode='"+tCLAIMSSRS.GetText(k,2)
                            +"' and recontcode='"+tCLAIMSSRS.GetText(k,3)
                            +"' and costcenter='"+costcenterterm
                            +"' and StartDate='"+StartDate+"' and EndData='"+EndDate+"' with ur";
           System.out.println(result1);//
          LRComRiskResultDB tLRComRiskResultDB = new LRComRiskResultDB();
          LRComRiskResultSet tttLRComRiskResultSet = tLRComRiskResultDB.executeQuery(result1);
          if(tttLRComRiskResultSet.size()==1){
              //该条记录已经存在，则把它的保全分保数据更新。
             tttLRComRiskResultSet.get(1).setClaimBackFee(Double.parseDouble(tCLAIMSSRS.GetText(k,5)));
//             tLRComRiskResultSet.add(tttLRComRiskResultSet);
             tttLRComRiskResultSet.get(1).setStandbyFlag1(tLRAccountsSchema.getActuGetNo());//存储账单号码
             LRComRiskResultDB ttLRComRiskResultDB=new LRComRiskResultDB(con);
             ttLRComRiskResultDB.setSchema(tttLRComRiskResultSet.get(1));
             ttLRComRiskResultDB.delete();
             ttLRComRiskResultDB.insert();

          }else if(tttLRComRiskResultSet.size()<=0){
              //该条记录不存在，则创建新的记录，记录它的保全分保数据。
           LRComRiskResultSchema tttLRComRiskResultSchema = new LRComRiskResultSchema();
           tttLRComRiskResultSchema.setManageCom(tCLAIMSSRS.GetText(k,1));
           tttLRComRiskResultSchema.setRiskCode(tCLAIMSSRS.GetText(k,2));
           tttLRComRiskResultSchema.setRecontCode(tCLAIMSSRS.GetText(k,3));
           if(tCLAIMSSRS.GetText(k,4)!=null && !tCLAIMSSRS.GetText(k,4).equals("")){
               tttLRComRiskResultSchema.setCostCenter(tCLAIMSSRS.GetText(k,4));
           }else{
               tttLRComRiskResultSchema.setCostCenter("aa");
           }
           tttLRComRiskResultSchema.setClaimBackFee(Double.parseDouble(tCLAIMSSRS.GetText(k,5)));// 分保保费
           tttLRComRiskResultSchema.setStartDate(StartDate);
           tttLRComRiskResultSchema.setEndData(EndDate);
           tttLRComRiskResultSchema.setStandbyFlag1(tLRAccountsSchema.getActuGetNo());//存储账单号码
           tttLRComRiskResultSchema.setOperator(this.globalInput.Operator);
           tttLRComRiskResultSchema.setMakeDate(PubFun.getCurrentDate());
           tttLRComRiskResultSchema.setMakeTime(PubFun.getCurrentTime());
           tttLRComRiskResultSchema.setModifyDate(PubFun.getCurrentDate());
           tttLRComRiskResultSchema.setModifyTime(PubFun.getCurrentTime());
           LRComRiskResultDB ttLRComRiskResultDB=new LRComRiskResultDB(con);
           ttLRComRiskResultDB.setSchema(tttLRComRiskResultSchema);
           ttLRComRiskResultDB.delete();
           ttLRComRiskResultDB.insert();
          }
       }
       return true;
    }
}
