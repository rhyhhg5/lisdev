package com.sinosoft.lis.reinsure;

/**
 * Copyright (c) 2008 sinosoft  Co. Ltd.
 * All right reserved.
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LRAccountsSet;
import com.sinosoft.lis.db.LRAccountsDB;
import com.sinosoft.lis.f1print.PrintService;

/*
 * <p>ClassName: ReNewAccountXMLBL </p>
 * <p>Description: ReNewAccountXMLBL类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: Sun yu
 * @CreateDate：2008-10-11
 */
public class ReNewAccountXMLBL implements PrintService {


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";

    private TransferData mTransferData = new TransferData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private MMap mMap = new MMap();

    //业务处理相关变量
    /** 全局数据 */

    public ReNewAccountXMLBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
       //准备往后台的数据
//        if (!prepareOutputData()) return false;
//        System.out.println("---End prepareOutputData---");
//
//        System.out.println("Start PubSubmit BLS Submit...");
//        PubSubmit tPubSubmit = new PubSubmit();
//        if (!tPubSubmit.submitData(mInputData, strOperate)) {
//          // @@错误处理
//          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//          return false;
//        }
//        System.out.println("End PubSubmit BLS Submit...");

        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
    }

    public VData getResult()
    {
        return this.mResult;
    }
    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
            String tLimit = PubFun.getNoLimit(this.globalInput.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            mLOPRTManagerSchema.setOtherNo(mLOPRTManagerSchema.getOtherNo());
            mLOPRTManagerSchema.setOtherNoType("19");
            mLOPRTManagerSchema.setCode("zbzddy01");
            mLOPRTManagerSchema.setManageCom(this.globalInput.ManageCom);
            mLOPRTManagerSchema.setReqCom(this.globalInput.ManageCom);
            mLOPRTManagerSchema.setReqOperator(this.globalInput.Operator);
            mLOPRTManagerSchema.setPrtType("0");
            mLOPRTManagerSchema.setStateFlag("0");
            mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

//        System.out.println("Come to prepareOutputData()...........");
//        try {
//            this.mInputData.clear();
//            this.mInputData.add(mMap);
//
//        } catch (Exception ex) {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "LDComBL";
//            tError.functionName = "prepareData";
//            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ReNewAccount.vts", "printer"); //最好紧接着就初始化xml文档
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("JetFormType", "zbzddy01");
//      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                          this.globalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
         if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", this.globalInput.ClientIP.replaceAll("\\.","_"));
 //       texttag.add("userIP","10_252_130_163");
        texttag.add("previewflag", "1");
        String tActuGetNo=mLOPRTManagerSchema.getOtherNo();
        LRAccountsDB tLRAccountsDB=new LRAccountsDB();
        tLRAccountsDB.setActuGetNo(tActuGetNo);
        LRAccountsSet tLRAccountsSet=tLRAccountsDB.query();
        if(tLRAccountsSet==null || tLRAccountsSet.size()<=0){
            return false;
        }
        LRAccountsSchema tLRAccountsSchema=tLRAccountsSet.get(1);

        texttag.add("ActuGetNo", tActuGetNo);
        texttag.add("CreateDate", PubFun.getCurrentDate());
        texttag.add("Year", tLRAccountsSchema.getStartDate().split("-")[0]);
        texttag.add("StartDate", tLRAccountsSchema.getStartDate());
        texttag.add("EndDate", tLRAccountsSchema.getEndData());
        texttag.add("ReComCode", tLRAccountsSchema.getReComCode());
        texttag.add("ReComName", tLRAccountsSchema.getReComName());
        texttag.add("RecontCode", tLRAccountsSchema.getRecontCode());
        texttag.add("RecontName", tLRAccountsSchema.getRecontName());
        texttag.add("CessPrem", tLRAccountsSchema.getCessPrem());
        texttag.add("ReProcFee", tLRAccountsSchema.getReProcFee());
        texttag.add("ClaimBackFee", tLRAccountsSchema.getClaimBackFee());
        texttag.add("BalanceLend", tLRAccountsSchema.getBalanceLend());
        texttag.add("PayDate", mLOPRTManagerSchema.getStandbyFlag1());
        double tLead=tLRAccountsSchema.getReProcFee()+tLRAccountsSchema.getBalanceLend();
        texttag.add("SumLead", (String)(new DecimalFormat("0.00").format(new Double(tLead))));
        texttag.add("SumLoan", tLRAccountsSchema.getCessPrem());

        xmlexport.addTextTag(texttag);

//        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        String printNum="0";
        if(StrTool.cTrim(tLRAccountsSchema.getStandby3()).equals("")){
            printNum="0";
        }else{
            printNum=tLRAccountsSchema.getStandby3();
        }
        tLRAccountsSchema.setStandby3(String.valueOf(Integer.parseInt(printNum)+1));
        tLRAccountsSchema.setModifyDate(PubFun.getCurrentDate2());
        tLRAccountsSchema.setModifyTime(PubFun.getCurrentTime());
        tLRAccountsSchema.setOperator(mLOPRTManagerSchema.getReqOperator());
        LRAccountsDB ttLRAccountsDB=new LRAccountsDB();
        ttLRAccountsDB.setSchema(tLRAccountsSchema);
        ttLRAccountsDB.update();//更新打印次数字段

        prepareOutputData();
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
//        mTransferData = (TransferData) cInputData.getObjectByObjectName(
//                "TransferData", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }


}


