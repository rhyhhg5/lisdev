/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.reinsure;

import java.util.Hashtable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LRCalCessDataBL </p>
 * <p>Description: LRCalCessDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: liuli
 * @CreateDate：2008-11-20
 */
public class LRGNewCalCessDataBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";
    private String mStartDate = "";
   

    private TransferData mGetCessData = new TransferData();

    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    private double mAccQuot; //接受份额
    private double mManageFeeRate; //分保管理费费率
    //用于毛保费成数分保
    private double mCessionRate; //分保比例
    private double mReProcFeeRate; //再保手续费费率
    private double mSpeCemmRate; //特殊佣金费率
    //用于溢额分保
    private double mCessionAmount; //分保保额
    private double mCessPremRate; //分保费率
    private double mChoiRebaFeeRate; //折扣比例
    private double mCessAmnt;
    private Hashtable m_hashLMCheckField = new Hashtable();
    private String mReContCode = "";
    //业务处理相关变量
    DealRiskAmntBL tDealRiskAmntBL = new DealRiskAmntBL();
    /** 全局数据 */

    public LRGNewCalCessDataBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

   
    
    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitDataT(String cOperate,String mToDay) {
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputDataT(mToDay)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
     

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
        LRNewCalCessDataBL tLRNewCalCessDataBL = new LRNewCalCessDataBL();
        try {
            vData.add(globalInput);
            tTransferData.setNameAndValue("StartDate", "2009-2-16");
            tTransferData.setNameAndValue("EndDate", "2009-2-16");
            vData.add(tTransferData);
            tLRNewCalCessDataBL.submitData(vData, "CESSDATA");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
//            tError.moduleName = "LRCalCessDataBL";
//            tError.functionName = "prepareData";
//            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
//            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 分类计算分保数据
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //提取分保数据
        System.out.println("Come to calCessData()...............");
        
        this.mMap = new MMap();

        if (!calCessData1()) {
            buildError("calCessData1", "再保计算成数分保失败!");
        }

        return true;

    }
    public LRCalFactorValueSchema findCheckFieldByRiskCode(String strRiskCode, String tReContCode,String tFactorCode) {

        Hashtable hash = m_hashLMCheckField;
        String strPK = strRiskCode.trim() + tReContCode.trim()+tFactorCode.trim();
        Object obj = hash.get(strPK);

        if( obj != null ) {
          if( obj instanceof String ) {
            return null;
          } else {
            return (LRCalFactorValueSchema)obj;
          }
        } else {
        	LRCalFactorValueDB tLRCalFactorValueDB = new LRCalFactorValueDB();

        	tLRCalFactorValueDB.setRiskCode(strRiskCode);
        	tLRCalFactorValueDB.setReContCode(tReContCode);
        	tLRCalFactorValueDB.setFactorCode(tFactorCode);
        	LRCalFactorValueSet tLRCalFactorValueSet=tLRCalFactorValueDB.query();
        	if(tLRCalFactorValueSet.size()<=0){
        		hash.put(strPK, "NOFOUND");
        		return null;
        		
        	}
          LRCalFactorValueSchema tLRCalFactorValueSchema = tLRCalFactorValueSet.get(1);

          if( tLRCalFactorValueDB.mErrors.needDealError() ) {
            mErrors.copyAllErrors(tLRCalFactorValueDB.mErrors);
            hash.put(strPK, "NOFOUND");
            return null;
          }

          hash.put(strPK, tLRCalFactorValueSchema);
          return tLRCalFactorValueSchema;
        }
      }
    /**
     * 成数分保计算
     * @return boolean
     */
    private boolean calCessData1() {
        //先处理提数区间内承保的医疗险,根据提数规则,可能出现财务已经缴费但是尚未提取到LRPol中的情况
        //因此先对处于提数期间的LRPol表中的医疗险,LRPolDetail表中截止日期前的缴费进行计算
        String strSQL =
                "select * from LRPol a where " +
                "  a.GetDataDate = '" +
                mStartDate + "'  and a.TempCessFlag = 'N' and ReinsureItem='G'"  ;
      
        	
        	strSQL+=" and exists (select 1 from LRContInfo where RecontCode = " +
        			"a.RecontCode and CessionMode = '1')  ";
       
        strSQL+=" with ur ";
        LRPolSet tLRPolSet = new LRPolSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLRPolSet, strSQL);

        try {
            do {
                rswrapper.getData();
                mMap = new MMap();
                for (int i = 1; i <= tLRPolSet.size(); i++) {
                    LRPolSchema tLRPolSchema = tLRPolSet.get(i);
                    this.mCessionRate = tLRPolSchema.getDeadAddFeeRate(); //成数分保比例
                    this.mAccQuot = getAccQuot(tLRPolSchema); //接受份额
                    this.mReProcFeeRate = getReProcFeeRate(tLRPolSchema); //再保手续费比例
                    //09/04/30 5503的手续费费率按保费的折扣比例不同而不同
                    if(tLRPolSchema.getRiskCode().equals("5503")&& tLRPolSchema.getStandPrem()>0)
                    {
                       double FloatRateFee = tLRPolSchema.getPrem()/tLRPolSchema.getStandPrem();
                       if(FloatRateFee>=0.2 && FloatRateFee <1){
                           this.mReProcFeeRate = 0.21+FloatRateFee*0.5;
                       }else if (FloatRateFee >=1){
                           this.mReProcFeeRate = 0.71;
                       }
                    }
                    this.mSpeCemmRate = this.getSpeCemmRate(tLRPolSchema); //特殊佣金比例
                    this.mManageFeeRate = this.getManageFeeRate(tLRPolSchema); //管理费比例
                    
                    
                        LRPolResultSchema tLRPolResultSchema = new
                                LRPolResultSchema();
                        this.preLRPolResultC(tLRPolSchema,
                        			               tLRPolResultSchema);
                        this.mMap.put(tLRPolResultSchema.getSchema(),
                                      "DELETE&INSERT");

                }
                if (!prepareOutputData()) {
                    return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    if (tPubSubmit.mErrors.needDealError()) {
                        buildError("submitData", "保存再保计算结果出错!");
                    }
                    return false;
                }
            } while (tLRPolSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
        return true;
    }


 

    

    
   
  

    /**
     *成数分保准备数据
     * @param aLRPolSchema LRPolSchema
     * @param aLRPolDetailSchema LRPolDetailSchema
     * @param aLRPolResultSchema LRPolResultSchema
     * @return boolean
     */
    private boolean preLRPolResultC(LRPolSchema aLRPolSchema,
    								              LRPolResultSchema aLRPolResultSchema) {
    	aLRPolResultSchema.setPolNo(aLRPolSchema.getPolNo());
        aLRPolResultSchema.setReContCode(aLRPolSchema.getReContCode());
        aLRPolResultSchema.setReinsureItem(aLRPolSchema.getReinsureItem());
        aLRPolResultSchema.setTempCessFlag(aLRPolSchema.getTempCessFlag());
        aLRPolResultSchema.setReNewCount(aLRPolSchema.getReNewCount());
        aLRPolResultSchema.setPayCount(aLRPolSchema.getReNewCount());
        aLRPolResultSchema.setRiskCalSort(aLRPolSchema.getRiskCalSort());
        aLRPolResultSchema.setRiskCode(aLRPolSchema.getRiskCode());
        aLRPolResultSchema.setRiskVersion(aLRPolSchema.getRiskVersion());
        aLRPolResultSchema.setCessStartDate(aLRPolSchema.getGetDataDate());
        aLRPolResultSchema.setCessEndDate(aLRPolSchema.getGetDataDate());

        aLRPolResultSchema.setCurrentPrem(aLRPolSchema.getPrem()); //当期保费
        int tPayIntv = aLRPolSchema.getPayIntv();
        
            aLRPolResultSchema.setShouldCessPrem(aLRPolSchema.getPrem());
            aLRPolResultSchema.setShouldCount(1);
            aLRPolResultSchema.setRemainCount(0);
        
        aLRPolResultSchema.setCessionRate(this.mCessionRate); //分保比例
        double cessPrem = aLRPolResultSchema.getShouldCessPrem() *
                          mCessionRate * mAccQuot;
//        if(aLRPolSchema.getReinsureItem().equals("F")){//如果是反冲数据，则置分保保费为负数
//            cessPrem=cessPrem;
//        }

        	aLRPolResultSchema.setCessPrem(cessPrem); //分保保费

        aLRPolResultSchema.setSpeCemmRate(this.mSpeCemmRate); //特殊佣金比例
        aLRPolResultSchema.setReProcFeeRate(this.mReProcFeeRate); //再保手续费比例
        aLRPolResultSchema.setReProcFee(cessPrem *
                                        (mSpeCemmRate + mReProcFeeRate)); //再保手续费
        aLRPolResultSchema.setManageFeeRate(this.mManageFeeRate); //再保管理费比例
        aLRPolResultSchema.setManageFee(cessPrem * this.mManageFeeRate); //再保管理费
        aLRPolResultSchema.setPayNo(aLRPolSchema.getPayNo());
        aLRPolResultSchema.setManageCom(aLRPolSchema.getManageCom());
        aLRPolResultSchema.setOperator("Server");
        aLRPolResultSchema.setMakeDate(mPubFun.getCurrentDate());
        aLRPolResultSchema.setMakeTime(mPubFun.getCurrentTime());
        aLRPolResultSchema.setModifyDate(mPubFun.getCurrentDate());
        aLRPolResultSchema.setModifyTime(mPubFun.getCurrentTime());
        return true;
    }

   

    /**
     * 得到再保手续费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getReProcFeeRate(LRPolSchema aLRPolSchema) {
       
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"ReinProcFeeRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    /**
     * 得到特殊佣金比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getSpeCemmRate(LRPolSchema aLRPolSchema) {
       
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"SpeComRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    

   


    /**
     * 得到管理费比例
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getManageFeeRate(LRPolSchema aLRPolSchema) {
        
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"ReinManFeeRate");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

    
    
   


   
    
   

    /**
     * 获取再保合同的接受份额
     * @param aLRPolSchema LRPolSchema
     * @return String
     */
    private double getAccQuot(LRPolSchema aLRPolSchema) {
        
        LRCalFactorValueSchema tLRCalFactorValueSchema
    	=findCheckFieldByRiskCode(aLRPolSchema.getRiskCode(),aLRPolSchema.getReContCode(),"AccQuot");
        
        if (tLRCalFactorValueSchema==null) {
            return 0;
        }
        if (tLRCalFactorValueSchema.getFactorValue() == null||tLRCalFactorValueSchema.getFactorValue().equals("null")||tLRCalFactorValueSchema.getFactorValue().equals("")) {
            return 0;
        }
        String t=tLRCalFactorValueSchema.getFactorValue();
        double tresult=Double.parseDouble(t);
        return tresult;
    }

   

 
    
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputDataT(String mToDay) {
        
        mStartDate = mToDay;
       
        
        return true;
    }

    public String getResult() {
        return "没有传数据";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
//        cError.moduleName = "LRCalCessDataBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
}
