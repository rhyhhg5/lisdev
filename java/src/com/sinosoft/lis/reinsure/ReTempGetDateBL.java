package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.sql.PreparedStatement;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.util.Date;

/*
 * <p>ClassName: ReTempCessBL </p>
 * <p>Description: 提取再保临时分保数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Liu Li
 * @CreateDate：2008-12-29
 */
public class ReTempGetDateBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private PubFun mPubFun = new PubFun();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    //临分合同信息
  

    private MMap mMap = null;
    private String mStartDate="";
    private String mEndDate="";
    private String mTReContCode="";
    public ReTempGetDateBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串,为空串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRGetCessDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //处理已经合同分保的保单。
        if (!verifyContCess()) {
            return false;
        }
        if (!saveData()) {//保存数据
            return false;
        }
       
       
        return true;
    }
    /**
     * 保存数据
     * @return boolean
     */
    private boolean saveData(){
        if (!prepareOutputData()) {
           return false;
       }
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(mInputData, "")) {
           if (tPubSubmit.mErrors.needDealError()) {
               buildError("submitData", "处理临分时已经合同分保数据,出现错误!");
           }
           return false;
       }
           return true;
    }

    /**
     * verifyContCess
     * 处理已经合同提数或者合同分保的数据。
     * @return boolean
     */
    private boolean verifyContCess() {
    	FDate chgdate = new FDate();
    	Date startdate = chgdate.getDate(this.mStartDate); //录入的起始日期，用FDate处理
    	Date dedate = chgdate.getDate(this.mEndDate); //录入的终止日期，用FDate处理
    	Date dbdate = startdate;
    	while (dbdate.compareTo(dedate) <= 0){
    		
            LRTempCessContDB tLRTempCessContDB = new LRTempCessContDB();
            if(mTReContCode!=null&&!"".equals(mTReContCode))
            tLRTempCessContDB.setTempContCode(mTReContCode);
            else
            tLRTempCessContDB.setReContState("01");
            LRTempCessContSet tLRTempCessContSet = new LRTempCessContSet();
            tLRTempCessContSet = tLRTempCessContDB.query();
            if (tLRTempCessContSet.size() > 0) {
            	int count=tLRTempCessContSet.size();
            	for(int i=1;i<=count;i++){
            		LRTempCessContSchema tLRTempCessContSchema=new LRTempCessContSchema();
            		tLRTempCessContSchema = tLRTempCessContSet.get(i); //获取临分合同信息
            		String sqlinter =
            			"select 1 from lcpol a,lrinterface b,LRTempCessCont c,LRTempCessContInfo d " +
            			" where a.polno=b.polno  " +
                        " and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then a.contno else a.grpcontno end ) =c.contno and c.TempContCode=d.TempContCode" +
                        " and a.riskcode=d.riskcode" +
                      //因为个单没有保障计划且数据库不支持，null空=空，所以默认为:'ZZ'
                        " and (case when '1' = '1' then  '1'  else a.contplancode end) = d.contplancode"+
//                        " and a.Contplancode=d.contplancode" +
//                        " and  a.conttype='2' " +
                        " and c.tempcontcode='"+tLRTempCessContSchema.getTempContCode()+"'" +              
                        "  and b.renewalflag='N' " +
                        "and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then a.contno else a.grpcontno end )='"+tLRTempCessContSchema.getContno()+"' fetch first 1 rows only with ur";
            		SSRS tSSRS = null;
            		tSSRS = new ExeSQL().execSQL(sqlinter);
            		if (tSSRS==null||tSSRS.getMaxRow() <= 0) {
                        buildError("verifyContCess", "保单查询出错！");
                        continue;
                    }
            		String lrpolsql="select 1 from lrpol where " +
            				"recontcode='"+tLRTempCessContSchema.getTempContCode()+"' and " +
    						" (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then contno else grpcontno end )='"+tLRTempCessContSchema.getContno()+"' " +
							" fetch first 1 rows only with ur";
            		SSRS tpSSRS = null;
            		tpSSRS = new ExeSQL().execSQL(lrpolsql);
            		if (tpSSRS==null||tpSSRS.getMaxRow() <= 0) {
                    
            		ExeSQL tExeSQL = new ExeSQL();
            		tExeSQL.execUpdateSQL("update lrinterface a set state='2' ,polstat='3'," +
            				"contplancode=(select contplancode from lcpol where polno=a.polno) where 1=1 " +
            				"and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then a.contno else a.grpcontno end )='" +
            				tLRTempCessContSchema.getContno() +
            				"' and a.polno in  (select b.polno from lcpol b,LRTempCessCont c,LRTempCessContInfo d" +
            				" where (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then b.contno else b.grpcontno end )=c.contno and c.TempContCode=d.TempContCode and " +
                        "  b.riskcode=d.riskcode " +
                      //因为个单没有保障计划个单默认存储时为1，所以默认为:'1'
                      " and (case when '1' = '1' then  '1'  else b.contplancode end) = d.contplancode "+
                        " and   (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then b.contno else b.grpcontno end )='" +
                        tLRTempCessContSchema.getContno() +
                        "' and  c.TempContCode='" +
                        tLRTempCessContSchema.getTempContCode() +
                        "' ) with ur "
                		);
            		}
            		//为了兼容一个保单对应多个临时分保合同，更新当前日期lrinterface表中state=‘2’
            		//比如保单0200，对应两个临时分保A、B,
            		//进行提数时，是根据lrinterface表中 state=‘2’来 查询的
            		//先对A进行区配，然后会更新lrinterface中state='1',再循环B时，就查询不到数据了
            		else
            		{
            			ExeSQL tExeSQL = new ExeSQL();
                		tExeSQL.execUpdateSQL("update lrinterface a set state='2' ,polstat='3'," +
                				"contplancode=(select contplancode from lcpol where polno=a.polno) where dutydate = '"+chgdate.getString(dbdate)+"' and 1=1 " +
                				"and (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then a.contno else a.grpcontno end )='" +
                				tLRTempCessContSchema.getContno() +
                				"' and a.polno in  (select b.polno from lcpol b,LRTempCessCont c,LRTempCessContInfo d" +
                				" where (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then b.contno else b.grpcontno end )=c.contno and c.TempContCode=d.TempContCode and " +
                            "  b.riskcode=d.riskcode " +
                          //因为个单没有保障计划个单默认存储时为1，所以默认为:'1'
                          " and (case when '1' = '1' then  '1'  else b.contplancode end) = d.contplancode "+
                            " and   (case when '"+tLRTempCessContSchema.getContType().trim()+"' ='1' then b.contno else b.grpcontno end )='" +
                            tLRTempCessContSchema.getContno() +
                            "' and  c.TempContCode='" +
                            tLRTempCessContSchema.getTempContCode() +
                            "' ) with ur "
                    		);
            		}
//            		LRInterfaceSet tLRInterfaceSet=new LRInterfaceSet();
//                    RSWrapper rswrapper = new RSWrapper();
//                    rswrapper.prepareData(tLRInterfaceSet, sqlinter);
                    try {
//                        do {
//                            rswrapper.getData();
//                            for(int t=1;t<=tLRInterfaceSet.size();t++){
//                                LRInterfaceSchema tLRInterfaceSchema=tLRInterfaceSet.get(t);
                                LRNewTempCessDataBL tLRNewTempCessDataBL=new LRNewTempCessDataBL();
                                tLRNewTempCessDataBL.getCessData(tLRTempCessContSchema,dbdate);
                                LRTempGetEdorDataBL tLRTempGetEdorDataBL=new LRTempGetEdorDataBL();
                                tLRTempGetEdorDataBL.getEdorData(tLRTempCessContSchema,dbdate);
                                LRTempGetClmDataBL tLRTempGetClmDataBL=new LRTempGetClmDataBL();
                                tLRTempGetClmDataBL.insertClaimData(tLRTempCessContSchema,dbdate);
                                //}
                        //}
//                        while (tLRInterfaceSet.size() > 0);
//                        rswrapper.close();
                        }
                     catch (Exception ex) {
                        ex.printStackTrace();
                        
                    }
            	}
                
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null);
    	}
        
        

       

        return true;
    }

  
   
   

 

    /**
     * 记录错误日志
     * @return boolean
     */
    private boolean errorLog(String aPolNo, String errorInf) {
        LRErrorLogSchema tLRErrorLogSchema = new LRErrorLogSchema();
        String serialNo = PubFun1.CreateMaxNo("SERIALNOERR", 16);
        tLRErrorLogSchema.setSerialNo(serialNo);
        tLRErrorLogSchema.setPolNo(aPolNo);
        tLRErrorLogSchema.setLogType("2");
        tLRErrorLogSchema.setErrorInfo(errorInf);

        tLRErrorLogSchema.setOperator(globalInput.Operator);
        tLRErrorLogSchema.setManageCom(globalInput.ManageCom);
        tLRErrorLogSchema.setMakeDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setMakeTime(mPubFun.getCurrentTime());
        tLRErrorLogSchema.setModifyDate(mPubFun.getCurrentDate());
        tLRErrorLogSchema.setModifyTime(mPubFun.getCurrentTime());

        this.mMap.put(tLRErrorLogSchema, "INSERT");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        TransferData tTransfer = new TransferData();
        tTransfer = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = (String) tTransfer.getValueByName("StartDate");
        mEndDate = (String) tTransfer.getValueByName("EndDate");
        mTReContCode = (String) tTransfer.getValueByName("ReContCode");

        
        return true;
    }

 

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LRGetCessDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {
        ReTempCessBL tReTempCessBL = new ReTempCessBL();
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "rm0002";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("TempCessCode", "");

        VData tVData = new VData();
        tVData.addElement(globalInput);
        tVData.addElement(tTransferData);

        tReTempCessBL.submitData(tVData, "");
    }
}
