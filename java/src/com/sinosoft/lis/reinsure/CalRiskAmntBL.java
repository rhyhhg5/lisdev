/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Zhang Bin
 * @CreateDate：2006-08-28
 */
package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bq.PolicyAbateDealBL;
import java.util.Date;
import com.sinosoft.lis.bq.CommonBL;

public class CalRiskAmntBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private String mCurrentDate = new PubFun().getCurrentDate();

    /** 数据操作字符串 */
    private String strOperate;

    private MMap mMap = new MMap();

    //业务处理相关变量
    /** 全局数据 */

    public CalRiskAmntBL() {
    }

    //新增构造函数,用来限制保单
    public CalRiskAmntBL(String aCurrentDate) {
        this.mCurrentDate = aCurrentDate;
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.strOperate = cOperate;
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        CalRiskAmntBL tCalRiskAmntBL = new CalRiskAmntBL();
        LCPolDB aLCPolDB = new LCPolDB();
        aLCPolDB.setPolNo("21000220950");
        LCPolSchema aLCPolSchema = new LCPolSchema();
        if (aLCPolDB.getInfo()) {
            aLCPolSchema = aLCPolDB.getSchema();
        }
//        boolean isTemp = tCalRiskAmntBL.isTempCess(aLCPolSchema);
//        System.out.println("保额： " + aLCPolSchema.getAmnt() + " 是否临分 : " +
//                           isTemp);
         tCalRiskAmntBL.calRiskAmnt(aLCPolSchema);
//          double rate=tCalRiskAmntBL.calCessFeeRate("Re2601",aLCPolSchema,0,0,"");
//          System.out.println("rate: "+rate);
//          String a=tCalRiskAmntBL.getRateDate(aLCPolSchema);
//          System.out.println("使用费率表日期:"+a);

//          TransferData tT=tCalRiskAmntBL.getCalFactor(aLCPolSchema);
//
//          System.out.println("ManageCom: "+tT.getValueByName("ManageCom"));
//          System.out.println("StartDate:"+tT.getValueByName("StartDate"));
//          System.out.println("Sex: "+tT.getValueByName("Sex"));
//          System.out.println("Age: "+tT.getValueByName("Age"));
//

//          System.out.println("年龄: "+PubFun.calInterval("1969-11-20",PubFun.getCurrentDate(),"Y"));
//          String readMoney = tCalRiskAmntBL.reserveMoney("11000038906");
//          System.out.println("reserveMoney:"+readMoney);
//          VData vData = new VData();
//          float riskAmnt = tCalRiskAmntBL.calRiskAmnt("11000038906");
//          System.out.println("riskAmnt : "+riskAmnt);

//          double accRiskAmnt = tCalRiskAmntBL.accRiskAmnt("11000038906");
//          System.out.println("accRiskAmnt : "+accRiskAmnt);

        //String calCode=tCalRiskAmntBL.getCalCode("AB0001","2401","C");
        //System.out.println("calCode : "+calCode);
//          if(tCalRiskAmntBL.isTempCess("11000038906"))
//          {
//              System.out.println("11000038906 是临分保单。");
//          }
//          try
//          {
//              //tCalRiskAmntBL.submitData(vData,"INSERT");
//          }
//          catch (Exception ex)
//          {
//              ex.printStackTrace();
//          }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //进行插入数据
        if (this.strOperate.equals("INSERT")) {
        }
        return true;
    }

    /**
     * add by  zhangbin
     * 对个单进行临分自检，判断累计风险保额是否大于自动接受限额
     * @param aPolNo String
     * @return boolean 如果累计风险保额小于等于自动接受限额返回false,如果累计风险保额大于自动接受限额返回true
     */
    public boolean isTempCess(LCPolSchema aLCPolSchema) {
        LCPolSchema tLCPolSchema = aLCPolSchema;

        String strSQL = "select * from LRCalFactorValue where RiskCode = '" +
                        tLCPolSchema.getRiskCode() +
                        "' and RiskSort in ('2','3') with ur ";
        LRCalFactorValueSet tLRCalFactorValueSet = new LRCalFactorValueDB().
                executeQuery(strSQL);

        double accRiskAmnt = 0; //累计风险保额
        double autoLimit = 0;
        String recontCode = "";
        if (tLRCalFactorValueSet.size() <= 0) { //如果不是再保险种
            return false;
        } else { //如果是再保险种
            //得到自动接受限额, 临分得到最近一份再保合同的自动接受限额
            strSQL = "select recontcode,factorvalue from LRCalFactorValue a where a.factorcode='AutoLimit' and recontcode="
                     + " (select recontcode from LRContrInf where recontcode in (select distinct recontcode from LRCalFactorValue where riskcode='" +
                     tLCPolSchema.getRiskCode() + "') "
                     + " and Rvalidate=(select max(Rvalidate) from LRContrInf where recontcode in (select distinct recontcode from LRCalFactorValue where riskcode='" +
                     tLCPolSchema.getRiskCode() + "') and Rvalidate<='" +
                     PubFun.getCurrentDate() + "')"
                     + " ) and riskcode='" + tLCPolSchema.getRiskCode() +
                     "' with ur "
                     ;
            SSRS COUNT;
            ExeSQL exesql = new ExeSQL();
            COUNT = exesql.execSQL(strSQL);
            String tempdata1[][];
            if (COUNT != null && COUNT.getMaxRow() > 0) { //如果有自动接受限额
                tempdata1 = COUNT.getAllData();
                recontCode = tempdata1[0][0];
                autoLimit = Double.parseDouble(tempdata1[0][1]);
            } else {
                return false;
            }

            //计算累计风险保额
            accRiskAmnt = accRiskAmnt(tLCPolSchema, recontCode);
        }
        System.out.println("累积风险保额：" + accRiskAmnt + " 自动接受限额：" + autoLimit);
        //比较累计风险保额与自动接受限额
        if (accRiskAmnt <= autoLimit) {
            return false;
        }
        return true;
    }

    /**
     * 计算累计风险保额，按风险保额方式计算,用于批处理
     * add by zhangbin
     */
    public double accRiskAmnt(LCPolSchema aPolNoSchema, String recontCode) {
        LCPolSchema tLCPolSchema = aPolNoSchema;

        //得到累计计算风险保额分组名,根据累计计算险种定义，此处每一个险种只能位于一个分组中
        String strSQL =
                "select GroupName from LRAmntRelRisk where recontcode = '" +
                recontCode + "' and RiskCode = '" + tLCPolSchema.getRiskCode() +
                "'";
        SSRS COUNT = new ExeSQL().execSQL(strSQL);

        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return calRiskAmnt(tLCPolSchema); //不需要累计计算
        }
        double accRiskAmnt = 0;
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();

        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
        tLRContInfoDB.setReContCode(recontCode);
        if (!tLRContInfoDB.getInfo()) {
            System.out.println("没有此合同！");
            return 0;
        }
        LRContInfoSchema tLRContInfoSchema = tLRContInfoDB.getSchema();

        //如果是成数分保
        if (tLRContInfoSchema.getCessionMode().equals("1")) {
            return 0;
        }

        strSQL = "select * from LCPol where InsuredNo = '" +
                 tLCPolSchema.getInsuredNo() +
                 "' and appflag = '1' and  RiskCode in ( select RiskCode from LRAmntRelRisk where GroupName = '" +
                 COUNT.GetText(1, 1) + "' and recontcode='" + recontCode +
                 "') and EndDate > '" + mCurrentDate + "' and ((SignDate < '" +
                 mCurrentDate + "' and Cvalidate < '" + mCurrentDate +
                 "') or (SignDate <= Cvalidate and CvaliDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLCPolSchema.getMakeTime() +
                 "') or (SignDate > Cvalidate and SignDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLCPolSchema.getMakeTime() + "')) with ur ";

        LCPolSet aLCPolSet = new LCPolDB().executeQuery(strSQL); //得到该被保险人的所有累计险种保单
        for (int i = 1; i <= aLCPolSet.size(); i++) { //对该被保险人所有保单进行循环
            if (aLCPolSet.get(i).getContType().equals("1")) { //计算个单累计风险保额
                if (!tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Available", "1") &&
                    !tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Terminate", "1")) {
                    accRiskAmnt += calRiskAmnt(aLCPolSet.get(i));
                }
            } else { //计算团单累计风险保额
                if (!tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getGrpContNo(), "Pause", "1") &&
                    !tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getGrpContNo(), "Terminate", "1")) {
                    accRiskAmnt += calRiskAmnt(aLCPolSet.get(i));
                }
            }
        }
        System.out.println("累计风险保额：" + accRiskAmnt);
        return accRiskAmnt;
    }

    /**
     * 计算累计风险保额，按风险保额方式计算，用于再保计算
     * add by zhangbin
     */
    public double accRiskAmnt(LRPolSchema aLRPolSchema) {
        LRPolSchema tLRPolSchema = aLRPolSchema;

        //得到累计计算风险保额分组名,根据累计计算险种定义，此处每一个险种只能位于一个分组中
        String strSQL =
                "select GroupName from LRAmntRelRisk where recontcode = '" +
                tLRPolSchema.getReContCode() + "' and RiskCode = '" +
                tLRPolSchema.getRiskCode() +
                "'";
        SSRS COUNT = new ExeSQL().execSQL(strSQL);

        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return calRiskAmnt(tLRPolSchema); //不需要累计计算
        }
        double accRiskAmnt = 0;
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();

        strSQL = "select * from LCPol where InsuredNo = '" +
                 tLRPolSchema.getInsuredNo() +
                 "' and RiskCode in ( select RiskCode from LRAmntRelRisk where GroupName = '" +
                 COUNT.GetText(1, 1) + "' and recontcode='" +
                 tLRPolSchema.getReContCode() +
                 "') and PolNo<>'" + tLRPolSchema.getPolNo() + "' with ur";
        LCPolSet aLCPolSet = new LCPolDB().executeQuery(strSQL); //得到该被保险人的所有累计险种保单
        for (int i = 1; i <= aLCPolSet.size(); i++) { //对该被保险人所有保单进行循环
            if (aLCPolSet.get(i).getContType().equals("1")) { //计算个单累计风险保额
                if (!tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Available", "1") &&
                    !tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Terminate", "1")) {
                    accRiskAmnt += calRiskAmnt(aLCPolSet.get(i));
                }
            } else { //计算团单累计风险保额
                if (!tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getContNo(), "Available", "1") &&
                    !tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Terminate", "1")) {
                    accRiskAmnt += calRiskAmnt(aLCPolSet.get(i));
                }
            }
        }
        System.out.println("累计风险保额：" + accRiskAmnt);
        return accRiskAmnt + calRiskAmnt(tLRPolSchema);
    }


    /**
     * 计算累计保额，按保额方式计算，用于批处理
     * add by zhangbin
     */
    public double accAmnt(LCPolSchema aPolNoSchema, String recontCode) {
        LCPolSchema tLCPolSchema = aPolNoSchema;

        //得到累计计算保额分组名,根据累计计算险种定义，此处每一个险种只能位于一个分组中
        String strSQL =
                "select GroupName from LRAmntRelRisk where recontcode = '" +
                recontCode + "' and RiskCode = '" + tLCPolSchema.getRiskCode() +
                "'";
        SSRS COUNT = new ExeSQL().execSQL(strSQL);

        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return tLCPolSchema.getAmnt(); //不需要累计计算
        }
        double accAmnt = 0;
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();

        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
        tLRContInfoDB.setReContCode(recontCode);
        if (!tLRContInfoDB.getInfo()) {
            System.out.println("没有此合同！");
            return 0;
        }
        LRContInfoSchema tLRContInfoSchema = tLRContInfoDB.getSchema();

        //如果是成数分保
        if (tLRContInfoSchema.getCessionMode().equals("1")) {
            return 0;
        }

        strSQL = "select * from LCPol where InsuredNo = '" +
                 tLCPolSchema.getInsuredNo() +
                 "' and appflag = '1' and RiskCode in ( select RiskCode from LRAmntRelRisk where GroupName = '" +
                 COUNT.GetText(1, 1) + "' and recontcode='" + recontCode +
                 "') and EndDate > '" + mCurrentDate + "' and ((SignDate < '" +
                 mCurrentDate + "' and Cvalidate < '" + mCurrentDate +
                 "') or (SignDate <= Cvalidate and CvaliDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLCPolSchema.getMakeTime() +
                 "') or (SignDate > Cvalidate and SignDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLCPolSchema.getMakeTime() + "')) with ur ";
        LCPolSet aLCPolSet = new LCPolDB().executeQuery(strSQL); //得到该被保险人的所有累计险种保单
        for (int i = 1; i <= aLCPolSet.size(); i++) { //对该被保险人所有保单进行循环
            if (aLCPolSet.get(i).getContType().equals("1")) { //计算个单累计风险保额
                if (!tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Available", "1") &&
                    !tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Terminate", "1")) {
                    accAmnt += aLCPolSet.get(i).getAmnt();
                }
            } else { //计算团单累计风险保额
                if (!tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getGrpContNo(), "Pause", "1") &&
                    !tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getGrpContNo(), "Terminate", "1")) {
                    accAmnt += aLCPolSet.get(i).getAmnt();
                }
            }
        }
        System.out.println("累计风险保额：" + accAmnt);
        return accAmnt;
    }

    /**
     * 计算累计保额，按保额方式计算，用于再保计算
     * add by zhangbin
     */
    public double accAmnt(LRPolSchema aLRPolSchema) {
        LRPolSchema tLRPolSchema = aLRPolSchema;

        //得到累计计算保额分组名,根据累计计算险种定义，此处每一个险种只能位于一个分组中
        String strSQL =
                "select GroupName from LRAmntRelRisk where recontcode = '" +
                tLRPolSchema.getReContCode() + "' and RiskCode = '" +
                tLRPolSchema.getRiskCode() +
                "'";
        SSRS COUNT = new ExeSQL().execSQL(strSQL);

        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return tLRPolSchema.getAmnt(); //不需要累计计算
        }
        double accAmnt = 0;
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();

        strSQL = "select * from LCPol where InsuredNo = '" +
                 tLRPolSchema.getInsuredNo() +
                 "' and RiskCode in ( select RiskCode from LRAmntRelRisk where GroupName = '" +
                 COUNT.GetText(1, 1) + "' and recontcode='" +
                 tLRPolSchema.getReContCode() +
                 "') and PolNo<>'" + tLRPolSchema.getPolNo() + "' with ur";
        LCPolSet aLCPolSet = new LCPolDB().executeQuery(strSQL); //得到该被保险人的所有累计险种保单
        for (int i = 1; i <= aLCPolSet.size(); i++) { //对该被保险人所有保单进行循环
            if (aLCPolSet.get(i).getContType().equals("1")) { //计算个单累计风险保额
                if (!tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Available", "1") &&
                    !tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Terminate", "1")) {
                    accAmnt += aLCPolSet.get(i).getAmnt();
                }
            } else { //计算团单累计风险保额
                if (!tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                        getContNo(), "Available", "1") &&
                    !tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).getPolNo(),
                        "Terminate", "1")) {
                    accAmnt += aLCPolSet.get(i).getAmnt();
                }
            }
        }
        System.out.println("累计风险保额：" + accAmnt);
        return accAmnt + tLRPolSchema.getAmnt(); //这里为了保证当前保单已经退保时的算费正确
    }

    /**
     * 计算累计分保保额，用于批处理
     * add by zhangbin
     */
    public double sumCessAmnt(LCPolSchema aLCPolSchema, String aRecontCode) {
        System.out.println("开始计算累计分保保额......");
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();
        LCPolSchema tLCPolSchema = aLCPolSchema;
        double sumCessAmnt = 0;

        //得到累计计算保额分组名
        ExeSQL exesql = new ExeSQL();
        String strSQL = "select distinct GroupName from LRAmntRelRisk where "
                        + " Recontcode='" + aRecontCode + "' and RiskCode='" +
                        tLCPolSchema.getRiskCode() + "' with ur";
        SSRS COUNT = exesql.execSQL(strSQL);
        //得到该组中的所有险种
        if (COUNT == null || COUNT.getMaxRow() <= 0) { //如果没有GroupName
            return 0;
        }
        String tempdata1[][];
        tempdata1 = COUNT.getAllData(); //得到GroupName
        String groupName = tempdata1[0][0];
        strSQL = "select RiskCode from LRAmntRelRisk where GroupName = '" +
                 groupName + "' and recontcode='" + aRecontCode + "' with ur"
                 ;
        COUNT = exesql.execSQL(strSQL);
        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return 0;
        }
        //得到被保险人所有累积险种的保单  !!不包括当前保单
        //这里还需要保证同一天的保单，应该分在较后的保单上，也就是保证统计先前的保单时，不统计比自己晚的。modified by huxl
        strSQL = "select * from LCPol where InsuredNo='" +
                 tLCPolSchema.getInsuredNo() +
                 "' and appflag = '1' and  RiskCode in ("
                 + " select RiskCode from LRAmntRelRisk where GroupName = '" +
                 groupName + "' and recontcode='" + aRecontCode +
                 "') and PolNo<>'" + tLCPolSchema.getPolNo() +
                 "' and EndDate > '" + mCurrentDate + "' and((SignDate <'" +
                 mCurrentDate + "' and Cvalidate < '" + mCurrentDate +
                 "') or (SignDate <= Cvalidate and CvaliDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLCPolSchema.getMakeTime() +
                 "') or (SignDate > Cvalidate and SignDate = '" +
                 mCurrentDate + "' and MakeTime <= '" +
                 tLCPolSchema.getMakeTime() + "')) with ur ";
        LCPolDB aLCPolDB = new LCPolDB();
        LCPolSet aLCPolSet = new LCPolSet();
        aLCPolSet = aLCPolDB.executeQuery(strSQL); //得到该被保险人的所有险种保单
        if (aLCPolSet.size() > 0) {
            for (int i = 1; i <= aLCPolSet.size(); i++) { //对该被保险人所有保单进行循环
                //判断保单是否失效
                if (aLCPolSet.get(i).getGrpContNo().equals(
                        "00000000000000000000")) { //如果是个单
                    if (tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).
                            getPolNo(),
                            "Available", "1")) { //判断保单失效状态
                        continue;
                    }
                    System.out.println("个单未失效。。。。");
                    if (tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).
                            getPolNo(),
                            "Terminate", "1")) { //判断保单终止状态
                        continue;
                    }
                    System.out.println("个单未终止。。。。");
                } else { //如果是团单
                    if (tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                            getGrpContNo(), "Pause", "1")) { //判断保单失效状态
                        continue;
                    }
                    if (tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                            getGrpContNo(), "Terminate", "1")) { //判断保单终止状态
                        continue;
                    }
                }
                System.out.println("得到分保保额 " + aLCPolSet.get(i).getCessAmnt());
                //得到累加分保保额,!!分保保额是历史数据
                sumCessAmnt = sumCessAmnt + aLCPolSet.get(i).getCessAmnt();
            }
        }
        return sumCessAmnt;
    }

    /**
     * 计算累计分保保额，用于再保计算
     * add by huxl
     */
    public double sumCessAmnt(LRPolSchema aLRPolSchema) {
        System.out.println("开始计算累计分保保额......");
        PolicyAbateDealBL tPolicyAbateDealBL = new PolicyAbateDealBL();
        LRPolSchema tLRPolSchema = aLRPolSchema;
        double sumCessAmnt = 0;

        //得到累计计算保额分组名
        ExeSQL exesql = new ExeSQL();
        String strSQL = "select distinct GroupName from LRAmntRelRisk where "
                        + " Recontcode='" + tLRPolSchema.getReContCode() +
                        "' and RiskCode='" +
                        tLRPolSchema.getRiskCode() + "' with ur";
        SSRS COUNT = exesql.execSQL(strSQL);
        //得到该组中的所有险种
        if (COUNT == null || COUNT.getMaxRow() <= 0) { //如果没有GroupName
            return 0;
        }
        String tempdata1[][];
        tempdata1 = COUNT.getAllData(); //得到GroupName
        String groupName = tempdata1[0][0];
        strSQL = "select RiskCode from LRAmntRelRisk where GroupName = '" +
                 groupName + "' and recontcode='" + tLRPolSchema.getReContCode() +
                 "' with ur"
                 ;
        COUNT = exesql.execSQL(strSQL);
        if (COUNT == null || COUNT.getMaxRow() <= 0) {
            return 0;
        }
        //得到被保险人所有累积险种的保单  !!不包括当前保单
        strSQL = "select * from LCPol where InsuredNo='" +
                 tLRPolSchema.getInsuredNo() + "' and RiskCode in ("
                 + " select RiskCode from LRAmntRelRisk where GroupName = '" +
                 groupName + "' and recontcode='" + tLRPolSchema.getReContCode() +
                 "') and PolNo<>'" +
                 tLRPolSchema.getPolNo() + "' with ur";
        LCPolDB aLCPolDB = new LCPolDB();
        LCPolSet aLCPolSet = new LCPolSet();
        aLCPolSet = aLCPolDB.executeQuery(strSQL); //得到该被保险人的所有险种保单
        if (aLCPolSet.size() > 0) {
            for (int i = 1; i <= aLCPolSet.size(); i++) { //对该被保险人所有保单进行循环
                //判断保单是否失效
                if (aLCPolSet.get(i).getGrpContNo().equals(
                        "00000000000000000000")) { //如果是个单
                    if (tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).
                            getPolNo(),
                            "Available", "1")) { //判断保单失效状态
                        continue;
                    }
                    System.out.println("个单未失效。。。。");
                    if (tPolicyAbateDealBL.booPolState(aLCPolSet.get(i).
                            getPolNo(),
                            "Terminate", "1")) { //判断保单终止状态
                        continue;
                    }
                    System.out.println("个单未终止。。。。");
                } else { //如果是团单
                    if (tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                            getGrpContNo(), "Available", "1")) { //判断保单失效状态
                        continue;
                    }
                    if (tPolicyAbateDealBL.booGrpPolicyState(aLCPolSet.get(i).
                            getGrpContNo(), "Terminate", "1")) { //判断保单终止状态
                        continue;
                    }
                }
                System.out.println("得到分保保额 " + aLCPolSet.get(i).getCessAmnt());
                //得到累加分保保额,!!分保保额是历史数据
                sumCessAmnt = sumCessAmnt + aLCPolSet.get(i).getCessAmnt();
            }
        }
        return sumCessAmnt;
    }

    /**
     * 计算累计(风险)保额
     * @param aLRPolSchema LRPolSchema
     * @return double
     */
    public double sumAmnt(LRPolSchema aLRPolSchema) {
        LRContInfoDB tLRContInfoDB = new LRContInfoDB();
        tLRContInfoDB.setReContCode(aLRPolSchema.getReContCode());
        if (!tLRContInfoDB.getInfo()) {
            System.out.println("没有有效的再保合同");
            return 0;
        }
        if (tLRContInfoDB.getCessionFeeMode().equals("1")) {
            return this.accRiskAmnt(aLRPolSchema);
        } else if (tLRContInfoDB.getCessionFeeMode().equals("2")) {
            return this.accAmnt(aLRPolSchema);
        }
        return 0;
    }

    /**
     * 计算风险保额，用于批处理
     */
    private double calRiskAmnt(LCPolSchema aLCPolSchema) {
        double amnt = aLCPolSchema.getAmnt();
        String reservMoney = reserveMoney(aLCPolSchema);
        System.out.println("准备金： " + reservMoney);
        if (reservMoney == null || reservMoney.equals("")) {
            reservMoney = "0";
        }
        double riskAmnt = amnt - Float.parseFloat(reservMoney);
        return riskAmnt;
    }

    /**
     * 计算风险保额,用于再保计算
     */
    private double calRiskAmnt(LRPolSchema aLRPolSchema) {
        double amnt = aLRPolSchema.getAmnt();
        String reservMoney = reserveMoney(aLRPolSchema);
        System.out.println("准备金： " + reservMoney);
        if (reservMoney == null || reservMoney.equals("")) {
            reservMoney = "0";
        }
        double riskAmnt = amnt - Float.parseFloat(reservMoney);
        return riskAmnt;
    }


    /**
     * 只有长期险,需要计算准备金和保单年度,此处以Cvalidate为准
     * 计算险种保单的期初准备金，用于批处理
     */
    private String reserveMoney(LCPolSchema tLCPolSchema) {
        String riskCode = tLCPolSchema.getRiskCode();
        double amnt = tLCPolSchema.getAmnt();
        String validate = tLCPolSchema.getCValiDate();
        int currYear = PubFun.calInterval(validate, mCurrentDate, "Y") + 1; //计算保单年度,第一年的保单年度为1

        String strSQL = "";
        ExeSQL exesql = new ExeSQL();
        SSRS COUNT;
        String[][] tempdata;
        String reserveMoney = "0";
        if (riskCode.equals("2301")) {
            int payEndYear = tLCPolSchema.getPayEndYear(); //终交年龄年期
            int insuredSex = Integer.parseInt(tLCPolSchema.getInsuredSex());
            int insuredYear = tLCPolSchema.getInsuYear(); //保险年龄年期,保险期间
            int insuredAppAge = tLCPolSchema.getInsuredAppAge();

            strSQL = "select " + amnt + "*reserve/10000 from reserve204 "
                     + " where InsuYear =" + insuredYear + " and PayEndYear =" +
                     payEndYear + " and Sex =" + insuredSex + " and Age =" +
                     insuredAppAge + " and CurrYear =" + currYear;
            System.out.println("2301准备金SQL: " + strSQL);
            COUNT = exesql.execSQL(strSQL);
            if (COUNT != null && COUNT.getMaxRow() > 0) {
                tempdata = COUNT.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
                reserveMoney = tempdata[0][0];
            }
        } else if (riskCode.equals("2401")) {
            int payEndYear = tLCPolSchema.getPayEndYear(); //终交年龄年期
            int insuredSex = Integer.parseInt(tLCPolSchema.getInsuredSex());
            int insuredAppAge = tLCPolSchema.getInsuredAppAge();

            strSQL = "select " + amnt + "*reserve/10000 from reserve205 "
                     + " where PayEndYear =" + payEndYear + " and Sex =" +
                     insuredSex + " and Age =" + insuredAppAge
                     + " and CurrYear = " + currYear;
            System.out.println("2401准备金SQL: " + strSQL);
            COUNT = exesql.execSQL(strSQL);
            if (COUNT != null && COUNT.getMaxRow() > 0) {
                tempdata = COUNT.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
                reserveMoney = tempdata[0][0];
            }
        } else if (riskCode.equals("330106")) {
//          暂时未开发
        } else if (riskCode.equals("230201")){ // 06/30号新增
            int payEndYear = tLCPolSchema.getPayEndYear(); //终交年龄年期
             int insuredSex = Integer.parseInt(tLCPolSchema.getInsuredSex());
             int insuredYear = tLCPolSchema.getInsuYear(); //保险年龄年期,保险期间
             int insuredAppAge = tLCPolSchema.getInsuredAppAge();

             strSQL = "select " + amnt + "*reserve/10000 from reserve230201 "
                      + " where InsuYear =" + insuredYear + " and PayEndYear =" +
                      payEndYear + " and Sex =" + insuredSex + " and Age =" +
                      insuredAppAge + " and CurrYear =" + currYear;
             System.out.println("230201准备金SQL: " + strSQL);
             COUNT = exesql.execSQL(strSQL);
             if (COUNT != null && COUNT.getMaxRow() > 0) {
                 tempdata = COUNT.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
                 reserveMoney = tempdata[0][0];
             }

        } else if (riskCode.equals("240201")){ //  06/30号新增
            int payEndYear = tLCPolSchema.getPayEndYear(); //终交年龄年期
              int insuredSex = Integer.parseInt(tLCPolSchema.getInsuredSex());
              int insuredAppAge = tLCPolSchema.getInsuredAppAge();

              strSQL = "select " + amnt + "*reserve/10000 from reserve240201 "
                       + " where PayEndYear =" + payEndYear + " and Sex =" +
                       insuredSex + " and Age =" + insuredAppAge
                       + " and CurrYear = " + currYear;
              System.out.println("240201准备金SQL: " + strSQL);
              COUNT = exesql.execSQL(strSQL);
              if (COUNT != null && COUNT.getMaxRow() > 0) {
                  tempdata = COUNT.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
                  reserveMoney = tempdata[0][0];
              }
        }else {
            reserveMoney = "0";
        }
        return reserveMoney;
    }

    /**
     * 计算险种保单的期初准备金,用于再保计算
     */
    private String reserveMoney(LRPolSchema tLRPolSchema) {
        String riskCode = tLRPolSchema.getRiskCode();
        double amnt = tLRPolSchema.getAmnt();
        int payEndYear = tLRPolSchema.getPayEndYear(); //终交年龄年期
        int insuredSex = Integer.parseInt(tLRPolSchema.getInsuredSex());
        int insuredYear = tLRPolSchema.getInsuYear(); //保险年龄年期,保险期间
        int insuredAppAge = tLRPolSchema.getInsuredAppAge();

        String validate = tLRPolSchema.getCValiDate();
        int currYear = PubFun.calInterval(validate, mCurrentDate, "Y") + 1; //计算保单年度,第一年的保单年度为1

        String strSQL = "";
        ExeSQL exesql = new ExeSQL();
        SSRS COUNT;
        String[][] tempdata;
        String reserveMoney = "0";
        if (riskCode.equals("2301")) {
            strSQL = "select " + amnt + "*reserve/10000 from reserve204 "
                     + " where InsuYear =" + insuredYear + " and PayEndYear =" +
                     payEndYear + " and Sex =" + insuredSex + " and Age =" +
                     insuredAppAge + " and CurrYear =" + currYear;
            System.out.println("2301准备金SQL: " + strSQL);
            COUNT = exesql.execSQL(strSQL);
            if (COUNT != null && COUNT.getMaxRow() > 0) {
                tempdata = COUNT.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
                reserveMoney = tempdata[0][0];
            }
        } else if (riskCode.equals("2401")) {
            strSQL = "select " + amnt + "*reserve/10000 from reserve205 "
                     + " where PayEndYear =" + payEndYear + " and Sex =" +
                     insuredSex + " and Age =" + insuredAppAge
                     + " and CurrYear = " + currYear;
            System.out.println("2401准备金SQL: " + strSQL);
            COUNT = exesql.execSQL(strSQL);
            if (COUNT != null && COUNT.getMaxRow() > 0) {
                tempdata = COUNT.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
                reserveMoney = tempdata[0][0];
            }
        } else if (riskCode.equals("330106")) {
        } else {
            reserveMoney = "0";
        }
        return reserveMoney;
    }

    /**
     * 计算临分分保保费率 (用于个单)
     * 参数1： CalCode
     * 参数2： LCPolSchema
     * 参数3： 重疾加费评点
     * 参数4： 死亡加费评点
     * @return double
     */
    public double calCessFeeRate(String calCode, LRPolSchema aLRPolSchema,
                                 double aDiseaseAddFee, double aDeadAddFee) {
        double cessFeeRateNum = 0;
        double diseaseAddFee = aDiseaseAddFee;
        double deadAddFee = aDeadAddFee;

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();

        Calculator tCalculator1 = new Calculator();
        Calculator tCalculator2 = new Calculator();
        tCalculator1.setCalCode(calCode);
        tCalculator2.setCalCode(calCode);
        String occType = aLRPolSchema.getOccupationType();
        if (occType == null || occType.equals("")) {
            buildError("insertData",
                       aLRPolSchema.getInsuredName() + "(客户号：" +
                       aLRPolSchema.getInsuredNo() + ")该被保险人职业类别为空!");
            return -1;
        }
        String sex = aLRPolSchema.getInsuredSex(); //被保险人性别
        int age = PubFun.getInsuredAppAge(currentDate,
                                          aLRPolSchema.getInsuredBirthday()); //被保险人年龄
        String strSQL = "select riskcode from lmcalmode where calcode='" +
                        calCode + "'";
        System.out.println("SQL: " + strSQL);
        SSRS tSSRS;
        ExeSQL exesql = new ExeSQL();
        tSSRS = exesql.execSQL(strSQL);
        String tempdata[][];
        if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
            System.out.println("没有算法代码");
            buildError("insertData", "没有算法代码!");
            return -1;
        }
        tempdata = tSSRS.getAllData();
        String tRiskCode = tempdata[0][0];
        //增加基本要素
        if (tRiskCode.equals("5201")) { //如果是5201
            tCalculator1.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Occtype", occType);
            if (tCalculator1.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            cessFeeRateNum = Double.parseDouble(tCalculator1.calculate());
            System.out.println("ManageCom: " + aLRPolSchema.getManageCom() +
                               " StartDate： " + " 2005-1-1" + "Occtype： " +
                               occType + " calculate: " +
                               tCalculator1.calculate());
        } else if (tRiskCode.equals("5601")) { //如果是5601, 临分的5601险默认使用2006-1-1的费率表
            tCalculator1.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2006-1-1");
            tCalculator1.addBasicFactor("Occtype", occType);
            if (tCalculator1.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            cessFeeRateNum = Double.parseDouble(tCalculator1.calculate());
            System.out.println("ManageCom: " + aLRPolSchema.getManageCom() +
                               " StartDate： " + " 2005-1-1" + "Occtype： " +
                               occType + " calculate: " +
                               tCalculator1.calculate());
        } else if (tRiskCode.equals("2301")) { //如果是2301
            tCalculator1.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator2.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator2.addBasicFactor("StartDate", "2005-1-1");
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRateNum = feeRate1 * (1 + diseaseAddFee) +
                             feeRate2 * (1 + deadAddFee);
            System.out.println("费率1: " + feeRate1 + "重疾加费：" + diseaseAddFee);
            System.out.println("费率2: " + feeRate2 + "死亡加费：" + deadAddFee);

        } else if (tRiskCode.equals("2401")) { //如果是2401
            tCalculator1.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator2.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator2.addBasicFactor("StartDate", "2005-1-1");
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");

            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRateNum = feeRate1 * (1 + diseaseAddFee) +
                             feeRate2 * (1 + deadAddFee);
            System.out.println("费率1: " + feeRate1 + "重疾加费：" + diseaseAddFee);
            System.out.println("费率2: " + feeRate2 + "死亡加费：" + deadAddFee);

        } else if (tRiskCode.equals("2601")) { //如果是2601
            tCalculator1.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRateNum = feeRate1 * (1 + diseaseAddFee);
            System.out.println("费率1: " + feeRate1 + "重疾加费：" + diseaseAddFee);

        } else if (tRiskCode.equals("2602")) { //如果是2602
            tCalculator1.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");

            tCalculator2.addBasicFactor("ManageCom", aLRPolSchema.getManageCom());
            tCalculator2.addBasicFactor("StartDate", "2005-1-1");
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRateNum = feeRate1 * (1 + diseaseAddFee) +
                             feeRate2 * (1 + deadAddFee);
            System.out.println("费率1: " + feeRate1 + "重疾加费：" + diseaseAddFee);
            System.out.println("费率2: " + feeRate2 + "死亡加费：" + deadAddFee);

        }
        return cessFeeRateNum;
    }

    /**
     * 计算临分分保保费率,(用于团单)
     * 参数1： CalCode
     * 参数2： LCPolSchema
     * @return double
     */
    public double calCessFeeRate(String calCode, LCPolSchema aLCPolSchema) {
        System.out.println("CalCode: " + calCode);
        String cessFeeRate = "";
        double cessFeeRateNum = 0;

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        Calculator tCalculator1 = new Calculator();
        Calculator tCalculator2 = new Calculator();
        tCalculator1.setCalCode(calCode);
        tCalculator2.setCalCode(calCode);
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonDB.setCustomerNo(aLCPolSchema.getInsuredNo());
        if (!tLDPersonDB.getInfo()) {
            System.out.println("该被保险人不存在!");
            buildError("insertData",
                       "客户号为：" + aLCPolSchema.getInsuredNo() + "的被保险人不存在!");
            return 0;
        }
        tLDPersonSchema = tLDPersonDB.getSchema();
        String occType = tLDPersonSchema.getOccupationType(); //被保险人职业类别
        if (occType == null || occType.equals("")) {
            buildError("insertData",
                       tLDPersonSchema.getName() + "(客户号：" +
                       tLDPersonSchema.getCustomerNo() + ")该被保险人职业类别为空!");
            return 0;
        }
        String sex = tLDPersonSchema.getSex(); //被保险人性别
        System.out.println("被保险人出生日期：" + tLDPersonSchema.getBirthday());
        int age = PubFun.calInterval2(tLDPersonSchema.getBirthday(),
                                      currentDate, "Y"); //被保险人年龄
        String strSQL = "select riskcode from lmcalmode where calcode='" +
                        calCode + "'";
        SSRS tSSRS;
        ExeSQL exesql = new ExeSQL();
        tSSRS = exesql.execSQL(strSQL);
        String tempdata[][];
        if (tSSRS == null || tSSRS.getMaxRow() <= 0) { //如果有自动接受限额
            System.out.println("没有算法代码");
            buildError("insertData", "没有算法代码!");
            return -1;
        }
        tempdata = tSSRS.getAllData();
        String tRiskCode = tempdata[0][0];
        //增加基本要素
        if (tRiskCode.equals("5201")) { //如果是5201
            tCalculator1.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Occtype", occType);
            if (tCalculator1.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            cessFeeRateNum = Double.parseDouble(tCalculator1.calculate());
            System.out.println("ManageCom: " + aLCPolSchema.getManageCom() +
                               " StartDate： " + " 2005-1-1" + "Occtype： " +
                               occType + " calculate: " +
                               tCalculator1.calculate());
        } else if (tRiskCode.equals("5601")) { //如果是5601
            tCalculator1.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Occtype", occType);
            if (tCalculator1.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            cessFeeRateNum = Double.parseDouble(tCalculator1.calculate());
            System.out.println("ManageCom: " + aLCPolSchema.getManageCom() +
                               " StartDate： " + " 2005-1-1" + "Occtype： " +
                               occType + " calculate: " +
                               tCalculator1.calculate());
        } else if (tRiskCode.equals("2301")) { //如果是2301
            tCalculator1.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator2.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator2.addBasicFactor("StartDate", "2005-1-1");
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRateNum = feeRate1 + feeRate2;
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);

        } else if (tRiskCode.equals("2401")) { //如果是2401
            tCalculator1.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator2.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator2.addBasicFactor("StartDate", "2005-1-1");
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRateNum = feeRate1 * (1) + feeRate2 * (1);
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);

        } else if (tRiskCode.equals("2601")) { //如果是2601
            tCalculator1.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRateNum = feeRate1;
            System.out.println("费率1: " + feeRate1);

        } else if (tRiskCode.equals("2602")) { //如果是2602
            tCalculator1.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator1.addBasicFactor("StartDate", "2005-1-1");
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");

            tCalculator2.addBasicFactor("ManageCom", aLCPolSchema.getManageCom());
            tCalculator2.addBasicFactor("StartDate", "2005-1-1");
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null) {
                buildError("insertData", "再保费率不存在!");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRateNum = feeRate1 + feeRate2;
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);
        }
        return cessFeeRateNum;
    }

    /**
     * 计算合同分保保费率
     * 参数1： CalCode
     * 参数2： LRPolSchema
     * @return double
     */
    public double calCessFeeRate(String calCode, LRPolSchema aLRPolSchema) {
        double cessFeeRate = 0;
        double diseaseAddFee = aLRPolSchema.getDiseaseAddFeeRate();
        double deadAddFee = aLRPolSchema.getDeadAddFeeRate();

        TransferData tT = new TransferData();
        tT = getCalFactor(aLRPolSchema);
        if (tT == null) {
            buildError("calCessFeeRate", "没有适用此保单的费率");
            return -1;
        }
        String tRiskCode = aLRPolSchema.getRiskCode();
        String manageCom = (String) tT.getValueByName("ManageCom");
        String startDate = (String) tT.getValueByName("StartDate");
        String occType = (String) tT.getValueByName("OccType");
        String sex = (String) tT.getValueByName("Sex");
        String age = (String) tT.getValueByName("Age");

        Calculator tCalculator1 = new Calculator();
        Calculator tCalculator2 = new Calculator();
        tCalculator1.setCalCode(calCode);
        tCalculator2.setCalCode(calCode);

        //增加基本要素
        if (tRiskCode.equals("5201")||tRiskCode.equals("550706")||tRiskCode.equals("550806")) { //如果是5201
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Occtype", occType);
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的费率");
                return -1;
            }
            cessFeeRate = Double.parseDouble(tCalculator1.calculate());
            System.out.println(tRiskCode+"ManageCom: " + aLRPolSchema.getManageCom() +
                               " StartDate： " + startDate + "Occtype： " +
                               occType + " calculate: " +
                               tCalculator1.calculate());
        } else if (tRiskCode.equals("5601")) { //如果是5601, 临分的5601险默认使用2006-1-1的费率表
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Occtype", occType);
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的费率");
                return -1;
            }
            cessFeeRate = Double.parseDouble(tCalculator1.calculate());
            System.out.println("ManageCom: " + aLRPolSchema.getManageCom() +
                               " StartDate： " + " 2005-1-1" + "Occtype： " +
                               occType + " calculate: " +
                               tCalculator1.calculate());
        } else if (tRiskCode.equals("2301") || tRiskCode.equals("230201") || tRiskCode.equals("240201")) { //如果是2301
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null ||
                tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")) {
                buildError("calCessFeeRate", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRate = feeRate1 * (1 + diseaseAddFee) +
                          feeRate2 * (1 + deadAddFee);
            System.out.println("费率1: " + feeRate1 + "重疾加费：" + diseaseAddFee);
            System.out.println("费率2: " + feeRate2 + "死亡加费：" + deadAddFee);

        } else if (tRiskCode.equals("2401")) { //如果是2401
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null
                || tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
            cessFeeRate = feeRate1 * (1 + diseaseAddFee) +
                          feeRate2 * (1 + deadAddFee);
            System.out.println("费率1: " + feeRate1 + "重疾加费：" + diseaseAddFee);
            System.out.println("费率2: " + feeRate2 + "死亡加费：" + deadAddFee);

        } else if (tRiskCode.equals("2601") || tRiskCode.equals("260401")||tRiskCode.equals("560201")) { //如果是2601
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null ||
                tCalculator1.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            cessFeeRate = feeRate1;
            System.out.println("费率1: " + feeRate1);
        } else if (tRiskCode.equals("2602") ||tRiskCode.equals("260301")) { //如果是2602
            tCalculator1.addBasicFactor("ManageCom", manageCom);
            tCalculator1.addBasicFactor("StartDate", startDate);
            tCalculator1.addBasicFactor("Type", "1");
            tCalculator1.addBasicFactor("Sex", sex);
            tCalculator1.addBasicFactor("Age", age + "");

            tCalculator2.addBasicFactor("ManageCom", manageCom);
            tCalculator2.addBasicFactor("StartDate", startDate);
            tCalculator2.addBasicFactor("Type", "2");
            tCalculator2.addBasicFactor("Sex", sex);
            tCalculator2.addBasicFactor("Age", age + "");
            if (tCalculator1.calculate() == null || tCalculator2.calculate() == null ||
                tCalculator1.calculate().equals("") ||
                tCalculator2.calculate().equals("")) {
                buildError("insertData", "没有适用此保单的费率");
                return -1;
            }
            double feeRate1 = Double.parseDouble(tCalculator1.calculate());
            double feeRate2 = Double.parseDouble(tCalculator2.calculate());
//            cessFeeRate = feeRate1 + feeRate2;
            //2009/4/30当险种260301承保的是死亡责任保险金时，取团险死亡费率。此考中方法只适用于现有人两个责任时，如果有新增责任，需要重新审查。
            String sqla = "select getdutycode from lcget a where getdutycode='634202'"
                        +" and polno='"+aLRPolSchema.getPolNo()+"' "+
                        " union all "+
                        "select getdutycode from lbget a where getdutycode='634202'"
                        +" and polno='"+aLRPolSchema.getPolNo()+"' "+
                        " with ur ";
            SSRS aSSRS = new ExeSQL().execSQL(sqla);
            if(aSSRS !=null && aSSRS.getMaxRow()>0 ){
                if(aSSRS.GetText(1,1).equals("634202")){
                    cessFeeRate = feeRate2;
                }
            }else{
               cessFeeRate = feeRate1 + feeRate2;
            }
            System.out.println("费率1: " + feeRate1);
            System.out.println("费率2: " + feeRate2);
        }
        return CommonBL.carry(cessFeeRate);
    }

    /**
     * 得到合同再保费率表要素
     * 参数: LCPolSchema对象
     * @return boolean
     */
    private TransferData getCalFactor(LRPolSchema aLRPolSchema) {
        TransferData tTransferData = new TransferData();
        String tRiskCode = aLRPolSchema.getRiskCode();

        /*简化机构配置问题
         *查询配置表中有没有配置该机构的费率,如果没有,则将机构号赋值为'8600'
         */
       String rateTable = "";
       if (aLRPolSchema.getRiskCode().equals("5201")) {
           rateTable = "Rate5201";
       } else if (aLRPolSchema.getRiskCode().equals("5601")) {
           rateTable = "Rate5601";
       } else if (aLRPolSchema.getRiskCode().equals("2301") ||
                  aLRPolSchema.getRiskCode().equals("2401") ||
                  aLRPolSchema.getRiskCode().equals("230201")||
                  aLRPolSchema.getRiskCode().equals("240201")) {
           rateTable = "Rate2301";
       } else if (aLRPolSchema.getRiskCode().equals("2601") ||
               aLRPolSchema.getRiskCode().equals("260401")) {
           rateTable = "Rate2601";
       } else if (aLRPolSchema.getRiskCode().equals("2602")||
               aLRPolSchema.getRiskCode().equals("260301")) {
           rateTable = "Rate2602";
       } else if(aLRPolSchema.getRiskCode().equals("550706")){//090627新增
           rateTable = "Rate550706";
       } else if(aLRPolSchema.getRiskCode().equals("550806")){//090627新增
           rateTable = "Rate550806";
       }else if(aLRPolSchema.getRiskCode().equals("560201")){//090627新增
           rateTable = "Rate560201";
       }

        if(rateTable==null || rateTable.equals("")){
            return null;
        }
        String msql = "select Managecom from "+ rateTable +" where Managecom='"+aLRPolSchema.getManageCom().substring(0,4) + "'" ;
        SSRS mSSRS = new ExeSQL().execSQL(msql);
        if(mSSRS.MaxRow==0){
            aLRPolSchema.setManageCom("8600");
        }else{
            aLRPolSchema.setManageCom(mSSRS.GetText(1,1));
        }

        if (tRiskCode.equals("5201") || tRiskCode.equals("5601")||
            tRiskCode.equals("550706")||tRiskCode.equals("550806")) {
            if (getRateDate(rateTable,aLRPolSchema) == null) {
                return null;
            }
            String startDate = getRateDate(rateTable,aLRPolSchema);
            String occType = aLRPolSchema.getOccupationType();
//            由于系统中存在职业类别为空的客户，经过与再保讨论，这种情况下默认为职业类别1
            if (occType == null || occType.equals("")) {
                occType = "1";
            }
            String manageCom = aLRPolSchema.getManageCom();

            tTransferData.setNameAndValue("ManageCom", manageCom);
            tTransferData.setNameAndValue("StartDate", startDate);
            tTransferData.setNameAndValue("OccType", occType);
            tTransferData.setNameAndValue("Sex", null);
            tTransferData.setNameAndValue("Age", null);
        } else if (tRiskCode.equals("2301") || tRiskCode.equals("2401") ||
                   tRiskCode.equals("2601") || tRiskCode.equals("2602") ||
                  tRiskCode.equals("230201")||tRiskCode.equals("240201") ||
                  tRiskCode.equals("260301")||tRiskCode.equals("260401")||
                  tRiskCode.equals("560201") ) {
            if (getRateDate(rateTable,aLRPolSchema) == null) {
                System.out.println("获取费率表日期参数失败！");
                return null;
            }
            String manageCom = aLRPolSchema.getManageCom();
            String startDate = getRateDate(rateTable,aLRPolSchema);
//            String startDate = "2008-1-1";
            String sex = aLRPolSchema.getInsuredSex();
            String age = PubFun.calInterval(aLRPolSchema.getInsuredBirthday(),
                                            aLRPolSchema.getGetDataDate(), "Y") +
                         "";//这里考虑到提数日期的延迟性，采用应分保日期来计算被保人年龄。modified by huxl @ 2007-12-25
            tTransferData.setNameAndValue("ManageCom", manageCom);
            tTransferData.setNameAndValue("StartDate", startDate);
            tTransferData.setNameAndValue("Sex", sex);
            tTransferData.setNameAndValue("Age", age);
            tTransferData.setNameAndValue("OccType", null);
        } else {
            return null;
        }
        return tTransferData;
    }


    /**
     * 得到保单适用的费率表
     * 参数: LCPolSchema对象
     * @return boolean
     */
    private String getRateDate(String calCode,LRPolSchema aLRPolSchema) {
        String rateTable = calCode;
        if(rateTable==null ||rateTable.equals("")){
            System.out.print("getRateDate 中参数错误");
            return null;
        }
//        if (aLRPolSchema.getRiskCode().equals("5201")) {
//            rateTable = "Rate5201";
//        } else if (aLRPolSchema.getRiskCode().equals("5601")) {
//            rateTable = "Rate5601";
//        } else if (aLRPolSchema.getRiskCode().equals("2301") ||
//                   aLRPolSchema.getRiskCode().equals("2401") ||
//                   aLRPolSchema.getRiskCode().equals("230201")||
//                   aLRPolSchema.getRiskCode().equals("240201")) {
//            rateTable = "Rate2301";
//        } else if (aLRPolSchema.getRiskCode().equals("2601")||
//                aLRPolSchema.getRiskCode().equals("260401")) {
//            rateTable = "Rate2601";
//        } else if (aLRPolSchema.getRiskCode().equals("2602") ||
//                aLRPolSchema.getRiskCode().equals("260301")) {
//            rateTable = "Rate2602";
//        }

        String tSql = "select distinct StartDate From " + rateTable +
                      " where Managecom='" + aLRPolSchema.getManageCom() + "'"
                      + " order by StartDate desc with ur "; //起始日期从大到小排列
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL(); //用于sql查询
        tSSRS = tExeSQL.execSQL(tSql);
        if (tSSRS.getMaxRow() <= 0) {
            System.out.println("保单：" + aLRPolSchema.getPolNo() +
                               " 没有对应再保合同生效日期！");
            return null;
        }
        String[][] tempRDate = tSSRS.getAllData(); //所有再保合同生效日期
        String contDate = null;
        //根据保单提数日期和费率表的生效日期，得到该保单适用的再保费率的起始日期
        for (int j = 0; j < tempRDate.length; j++) {
            FDate chgdate = new FDate();
            Date rateDate = chgdate.getDate(tempRDate[j][0]);
            Date getDataDate = chgdate.getDate(aLRPolSchema.getGetDataDate());
            if (rateDate.compareTo(getDataDate) <= 0) {
                contDate = tempRDate[j][0];
                break;
            }
        }

        return contDate;
    }


    /**
     * 临分协议得到CalCode
     * 参数1： 再保公司代码
     * 参数2： 险种代码
     * @return String
     */
//    public String getCalCode(LRTempCessContInfoSchema aLRTempCessContInfoSchema,
//                             LRPolSchema aLRPolSchema) {
//        //得到该险种的算法
//        ExeSQL tExeSQL = new ExeSQL();
//        String strSQL;
//        StringBuffer strSQLBuf = new StringBuffer();
//        strSQLBuf.append(
//                "select RecontCode from LRCalMode where RecontCode in ");
//        strSQLBuf.append("(select RecontCode from LRContInfo where ReComCode='" +
//                         aLRTempCessContInfoSchema.getReComCode() +
//                         "') and riskcode='" +
//                         aLRTempCessContInfoSchema.getRiskCode() + "' ");
//        strSQL = strSQLBuf.toString();
//        SSRS allCom = tExeSQL.execSQL(strSQL);
//        if (allCom == null || allCom.getMaxRow() <= 0) {
//            buildError("getCalCode",
//                       "没有针对该险种(" + aLRTempCessContInfoSchema.getRiskCode() +
//                       ")的再保合同，请确认选择了正确再保公司，或该险种是溢额分保的险种!");
//            return null;
//        }
//
//        //得到小于等于当前日期的最大合同生效日期
//        strSQLBuf = new StringBuffer(
//                "select ReContCode From LRContInfo where REComCode ='" +
//                aLRTempCessContInfoSchema.getReComCode() +
//                "' and ((Rinvalidate is not null and '" +
//                aLRPolSchema.getGetDataDate() +
//                "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
//                aLRPolSchema.getGetDataDate() + "' >= Rvalidate)) " +
//                "order by Rvalidate desc");
//        strSQL = strSQLBuf.toString();
//        SSRS allRInvalidate = tExeSQL.execSQL(strSQL);
//        if (allRInvalidate == null || allRInvalidate.getMaxRow() <= 0) {
//            buildError("getCalCode",
//                       "没有针对该险种(" + aLRTempCessContInfoSchema.getRiskCode() +
//                       ")的再保合同!");
//            return null;
//        }
//
//        strSQLBuf = new StringBuffer(
//                "select CessPremMode from LRCalMode where RecontCode = '" +
//                allRInvalidate.GetText(1, 1) + "') and riskcode='" +
//                aLRTempCessContInfoSchema.getRiskCode() +
//                "'");
//        strSQL = strSQLBuf.toString();
//        SSRS CalCode = tExeSQL.execSQL(strSQL);
//        if (CalCode == null || CalCode.getMaxRow() <= 0) {
//            buildError("insertData",
//                       "没有针对该险种(" + aLRTempCessContInfoSchema.getRiskCode() +
//                       ")的再保合同!");
//            return null;
//        }
//        return CalCode.GetText(1, 1);
//    }

    /**
     * 合同分保得到CalCode
     * @param aLRPolSchema LRPolSchema
     * @param aCalCodeType String
     * @return String
     */
    public String getCalCode(LRPolSchema aLRPolSchema, String aCalCodeType) {
        LRPolSchema tLRPolSchema = aLRPolSchema;
        String strSQL = "";
        SSRS calSortSSRS = new SSRS();
        if (aCalCodeType.equals("CessPremMode")) {
            strSQL = "select CessPremMode from LRCalMode where RecontCode ='" +
                     tLRPolSchema.getReContCode() + "' and RiskCode='" +
                     tLRPolSchema.getRiskCode()
                     + "' and ReinsurItem='" + tLRPolSchema.getReinsureItem() +
                     "' and RiskSort='" +
                     tLRPolSchema.getRiskCalSort() + "'";
            calSortSSRS = new ExeSQL().execSQL(strSQL);
        } else if (aCalCodeType.equals("RiskAmntCalCode")) {
            strSQL =
                    "select RiskAmntCalCode from LRCalMode where RecontCode ='" +
                    tLRPolSchema.getReContCode() + "' and RiskCode='" +
                    tLRPolSchema.getRiskCode()
                    + "' and ReinsurItem='" + tLRPolSchema.getReinsureItem() +
                    "' and RiskSort='" +
                    tLRPolSchema.getRiskCalSort() + "'";
            calSortSSRS = new ExeSQL().execSQL(strSQL);
        }
        if (calSortSSRS.getMaxRow() <= 0) {
            System.out.println("获取再保LMCalMode算法代码失败!");
            return null;
        }
        return calSortSSRS.GetText(1, 1);
    }


    public String getResult() {
        return "";
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "CalRiskAmntBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
