package com.sinosoft.lis.reinsure;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LRAccountSchema;
import com.sinosoft.lis.vschema.LRAccountSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LRAdujstDataUploadListBL {
	 public CErrors mErrors = new CErrors();

	    /** 往前面传输数据的容器 */
	    private VData mResult = new VData();

	    private GlobalInput mGlobalInput = null;

	    private TransferData mTransferData = null;

	    /**传输到后台处理的map*/
	    private MMap mMap = new MMap();

	    /** 数据操作字符串 */
	    private String mOperate = "";

	    private String mFilePath = null;

	    private String mFileName = null;

	    private LRAccountSet mLRAccountSet;

	    private String[] mSheetName = { "LRAccount", "Ver" };

	    private String mConfigName = "LRAccountImport.xml";

	    public LRAdujstDataUploadListBL()
	    {
	    }

	    public boolean submitData(VData cInputData, String cOperate)
	    {
	        if (getSubmitMap(cInputData, cOperate) == null)
	        {
	            return false;
	        }

	        if (!submit())
	        {
	            return false;
	        }

	        

	        return true;
	    }

	    public MMap getSubmitMap(VData cInputData, String cOperate)
	    {
	        if (!getInputData(cInputData, cOperate))
	        {
	            return null;
	        }

	        if (!checkData())
	        {
	            return null;
	        }

	        if (!dealData())
	        {
	            return null;
	        }

	        return mMap;
	    }

	    private boolean getInputData(VData cInputData, String cOperate)
	    {
	        mOperate = cOperate;

	        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
	                "GlobalInput", 0);
	        if (mGlobalInput == null)
	        {
	            buildError("getInputData", "处理超时，请重新登录。");
	            return false;
	        }

	        mTransferData = (TransferData) cInputData.getObjectByObjectName(
	                "TransferData", 0);
	        if (mTransferData == null)
	        {
	            buildError("getInputData", "所需参数不完整。");
	            return false;
	        }

	       

	       

	       
	       

	        return true;
	    }

	    private boolean checkData()
	    {
	        return true;
	    }

	    private boolean dealData()
	    {
	        MMap tTmpMap = null;

	        if ("Import".equals(mOperate))
	        {
	            tTmpMap = null;
	            tTmpMap = importCertifyList();
	            if (tTmpMap == null)
	            {
	                return false;
	            }
	            mMap.add(tTmpMap);
	            tTmpMap = null;
	        }

//	        if ("ImportConfirm".equals(mOperate))
//	        {
//	            tTmpMap = null;
//	            tTmpMap = confirmImportList();
//	            if (tTmpMap == null)
//	            {
//	                return false;
//	            }
//	            mMap.add(tTmpMap);
//	            tTmpMap = null;
//	        }
	//
//	        if ("ImportDelete".equals(mOperate))
//	        {
//	            tTmpMap = null;
//	            tTmpMap = deleteImportList();
//	            if (tTmpMap == null)
//	            {
//	                return false;
//	            }
//	            mMap.add(tTmpMap);
//	            tTmpMap = null;
//	        }
	//
//	        if ("ActiveImport".equals(mOperate))
//	        {
//	            tTmpMap = null;
//	            tTmpMap = activeImportList();
//	            if (tTmpMap == null)
//	            {
//	                return false;
//	            }
//	            mMap.add(tTmpMap);
//	            tTmpMap = null;
//	        }

	        return true;
	    }

	    private MMap importCertifyList()
	    {
	        MMap tMMap = new MMap();

	        MMap tTmpMap = null;

	        // 准备导入所需数据。
	        if (!prepareBeforeImportListDatas())
	        {
	            return null;
	        }
	        // ----------------------------

	        // 获取导入文件数据。
	        if (!parseDiskFile())
	        {
	            return null;
	        }
	        // ----------------------------

	        // 创建卡单信息。
	        tTmpMap = null;
	        tTmpMap = createLRAccountList();
	        if (tTmpMap == null)
	        {
	            return null;
	        }
	        tMMap.add(tTmpMap);
	        tTmpMap = null;
	        // ----------------------------

	        return tMMap;
	    }

	    /**
	     * 获取磁盘导入文件相关数据。
	     * <br />如不存在数据信息，则置 size为0的set集合。
	     * @return true：获取数据信息成功；false：获取数据信息失败
	     */
	    private boolean parseDiskFile()
	    {
	        String tUIRoot = CommonBL.getUIRoot();
//	        String tUIRoot="D:/picc/ui/";
//	        String tUIRoot = "E:/dev/ui/";
	        if (tUIRoot == null)
	        {
	            mErrors.addOneError("没有查找到应用目录");
	            return false;
	        }

	        String tConfigPath = new ExeSQL()
	                .getOneValue("select SysVarValue from LDSysVar where SysVar = 'XmlPath_lr'");

	        LRAdujstDataUploadImportBL importFile = new LRAdujstDataUploadImportBL(mFilePath
	                + mFileName, tUIRoot + tConfigPath + mConfigName, mSheetName);

	        if (!importFile.doImport())
	        {
	            this.mErrors.copyAllErrors(importFile.mErrors);
	            return false;
	        }

	        mLRAccountSet = importFile.getLRAccountSet();
	        if (mLRAccountSet == null)
	        {
	        	mLRAccountSet = new LRAccountSet();
	        }
	        //验证数据
	        SSRS aSSRS =  new SSRS();
	        ExeSQL aExeSQL = new ExeSQL();
	        for (int i = 1;i<=mLRAccountSet.size();i++){
	        	LRAccountSchema	mLRAccountSchema = mLRAccountSet.get(i);
	        	String tYear = mLRAccountSchema.getBelongYear();
	        	if(null==tYear||"".equals(tYear))
	        	{
	        		String er = "第"+i+"条数据的年份不能为空,录入错误"; 
					buildError("dealData",er);
					return false;
	        	}
	        	if(Integer.parseInt(tYear)<2000 || Integer.parseInt(tYear)>2100)
	        	{
	        		String er = "第"+i+"条数据的年份为："+tYear+",录入错误"; 
					buildError("dealData",er);
					return false;
	        	}
	        	String tMonth = mLRAccountSchema.getBelongMonth();
	        	if(null==tMonth||"".equals(tMonth))
	        	{
	        		String er = "第"+i+"条数据的月份不能为空,录入错误"; 
					buildError("dealData",er);
					return false;
	        	}
	        	if(2!=tMonth.length())
	        	{
	        		String er = "第"+i+"条数据的月份格式录入不对，应为两位数字,录入错误"; 
					buildError("dealData",er);
					return false;
	        	}
	        	if(Integer.parseInt(tMonth)<1 || Integer.parseInt(tMonth)>12)
	        	{
	        		String er = "第"+i+"条数据的月份为："+tMonth+"，不在01-12月份,录入错误"; 
	        		buildError("dealData",er);
	        		return false;
	        	}
	        	if(null==mLRAccountSchema.getManageCom()||"".equals(mLRAccountSchema.getManageCom()))
	        	{
		        	String er = "第"+i+"条数据的管理机构不能为空,录入错误"; 
		        	buildError("dealData",er);
		        	return false;
	        	}
	        	if(null==mLRAccountSchema.getRiskCode()||"".equals(mLRAccountSchema.getRiskCode()))
	        	{
		        	String er = "第"+i+"条数据的险种不能为空,录入错误"; 
		        	buildError("dealData",er);
		        	return false;
	        	}
	        	if(null==mLRAccountSchema.getReContCode()||"".equals(mLRAccountSchema.getReContCode()))
	        	{
		        	String er = "第"+i+"条数据的再保合同号不能为空,录入错误"; 
		        	buildError("dealData",er);
		        	return false;
	        	}
	        	if(null==mLRAccountSchema.getCostCenter()||"".equals(mLRAccountSchema.getCostCenter()))
	        	{
		        	String er = "第"+i+"条数据的成本中心不能为空,录入错误"; 
		        	buildError("dealData",er);
		        	return false;
	        	}
	        	if(null==mLRAccountSchema.getGrpContNo()||"".equals(mLRAccountSchema.getGrpContNo()))
	        	{
		        	String er = "第"+i+"条数据的保单号不能为空,录入错误"; 
		        	buildError("dealData",er);
		        	return false;
	        	}
	        	if(mLRAccountSchema.getCostCenter().substring(4,6).equals("93")){
	        		if(mLRAccountSchema.getGrpContNo()==null||mLRAccountSchema.getGrpContNo().equals("")||mLRAccountSchema.getGrpContNo().equals("00000000000000000000")){
	        			String er = "第"+i+"条数据为社保数据，保单号不能为空"; 
	        			buildError("dealData",er);
	        			return false;
	        		}
	        	}else{
	        		mLRAccountSchema.setGrpContNo("00000000000000000000");
	        	}
	        	String tTempCessFlag = mLRAccountSchema.getTempCessFlag();
	        	if("".equals(tTempCessFlag)||null==tTempCessFlag)
	        	{
	        		String er = "第"+i+"条数据的合同类型不能为空,录入错误"; 
					buildError("dealData",er);
					return false;
	        	}
	        	if(!"C".equals(tTempCessFlag)&&!"T".equals(tTempCessFlag))
	        	{
	        		String er = "第"+i+"条数据的合同类型不为C-合同分保或T-临时分保,录入错误"; 
					buildError("dealData",er);
					return false;
	        	}
	        	String tDataType = mLRAccountSchema.getDataType();
	        	if("".equals(tDataType)||null==tDataType)
	        	{
	        		String er = "第"+i+"条数据的数据类型不能为空,录入错误"; 
					buildError("dealData",er);
					return false;
	        	}
	        	if(!"01".equals(tDataType)&&!"02".equals(tDataType))
	        	{
	        		String er = "第"+i+"条数据的数据类型不为01-每月调整或02-其它调整,录入错误"; 
					buildError("dealData",er);
					return false;
	        	}
//	        String	chkSql = "select max(cesspremstate),max(claimbackfeestate) from db2inst1.LRAccount where  BelongMonth = '"+mLRAccountSchema.getBelongMonth()+"' and BelongYear = '"+mLRAccountSchema.getBelongYear()+"'  and datatype ='00' with ur";	
//	        aSSRS = aExeSQL.execSQL(chkSql);
//	        String chkData = "本月数据已流转到财务，不能上传调整";
//	        String chkData1 = "本月数据已结算，不能上传调整";
//	        if(aSSRS.getMaxRow()>0)
//	        {
//    			for(int j = 1;j<aSSRS.getMaxRow();j++){
//    			String mCessState = aSSRS.GetText(j, 1);
//    			String mClaimState = aSSRS.GetText(j, 2);  	
//    			String mCostCenter = aSSRS.GetText(j, 3);
//    			String mGrpContNo = aSSRS.GetText(j, 4);
//    			String mCc = mCostCenter.substring(4, 6);
//    			//System.out.println(mCessState+"--"+mClaimState+"--"+mGrpContNo+"--"+mCostCenter);
//    			if(mCessState.equals("02")&&mClaimState.equals("02")){
//    				buildError("dealData",chkData);
//    				return false;
//    			}
//    			if(mCessState.equals("03")&&mClaimState.equals("03")){
//    				buildError("dealData",chkData1);
//    				return false;
//    			}
//    			
//    			}
//			}
	        /**
	         * 手工导入时，默认为86000000机构
	         */
//	        if(!"86000000".equals(mLRAccountSchema.getManageCom()))
//	        {
//	        	String tCheckRecontSQL = "select * from ";
//	        	chkSql = "select cesspremstate,claimbackfeestate,costcenter,grpcontno from LRAccount where ReContCode = '"+mLRAccountSchema.getReContCode()+"'"
//	        	+" and riskcode ='"+mLRAccountSchema.getRiskCode()+"' and grpcontno ='"+mLRAccountSchema.getGrpContNo()+"' with ur";	
//	        	aSSRS = aExeSQL.execSQL(chkSql);
//	        	if(aSSRS.getMaxRow()<=0)
//	        	{
//	        		buildError("dealData","第"+i+"条数据有错误，账单表中没有该合同对应的保单险种信息，请确认上传数据是否正确！");
//	        		return false;
//	        	}
//	        }
//	        else
//	        {
	        //再保合同号
	        String tReContCode = mLRAccountSchema.getReContCode();
        	String recontcodeSql ="select recomcode from lrcontinfo where recontcode ='"+tReContCode+"' union select comcode from lrtempcesscont where tempcontcode ='"+tReContCode+"' with ur";
        	aSSRS = new SSRS();
        	aSSRS = aExeSQL.execSQL(recontcodeSql);
        	if(aSSRS.getMaxRow()<=0)
        	{
        		buildError("dealData","第"+i+"条数据有错误，导入的合同号"+tReContCode+"在再保系统中不存在！");
        		return false;
        	}
        	//再保公司代码
        	String tReComCode = aSSRS.GetText(1, 1);
        	recontcodeSql ="select recomname,sapappntno From lrrecominfo  where recomcode ='"+tReComCode+"' with ur";
        	aSSRS = new SSRS();
        	aSSRS = aExeSQL.execSQL(recontcodeSql);
        	if("".equals(aSSRS.GetText(1, 2))||null == aSSRS.GetText(1, 2))
        	{
        		buildError("dealData","第"+i+"条数据有错误，导入的再保合同对应的再保公司没有录入SAP客户号！");
        		return false;
        	}
        	//导入数据的险种
        	String tRiskCode =mLRAccountSchema.getRiskCode();
        	recontcodeSql ="select '1' from lrcalfactorvalue where recontcode ='"+tReContCode+"' and riskcode = '"+tRiskCode+"' union select '1' from lrtempcesscontinfo where tempcontcode ='"+tReContCode+"'  and riskcode = '"+tRiskCode+"' with ur";
        	aSSRS = new SSRS();
        	aSSRS = aExeSQL.execSQL(recontcodeSql);
        	if(aSSRS.getMaxRow()<=0)
        	{
        		buildError("dealData","第"+i+"条数据有错误，导入的合同号"+tReContCode+"与险种"+tRiskCode+"不匹配，请核实！");
        		return false;
        	}
        	recontcodeSql="select codealias from licodetrans  where codetype='ManageCom' and code='"+mLRAccountSchema.getManageCom()+"'";
        	aSSRS = new SSRS();
        	aSSRS = aExeSQL.execSQL(recontcodeSql);
        	if(aSSRS.getMaxRow()<=0)
        	{
        		buildError("dealData","第"+i+"条数据有错误，系统中不存在此管理机构对应的SAP四位机构代码，请核实导入的管理机构是否正确！");
        		return false;
        	}
        	if(!aSSRS.GetText(1, 1).equals(mLRAccountSchema.getCostCenter().substring(0, 4)))
        	{
        		buildError("dealData","第"+i+"条数据有错误，导入的管理机构与成本中心不匹配！");
        		return false;
        	}
//	        }
		}
	        

//	        LICertifyInsuredSet tLICertifyInsuredSet = importFile
//	                .getLICertifyInsuredSet();
//	        if (tLICertifyInsuredSet == null)
//	        {
//	            tLICertifyInsuredSet = new LICertifyInsuredSet();
//	        }
	//
//	        mCertifyInfoList = new CertifyInfoColl();
//	        if (!mCertifyInfoList.loadCertifyInfo(tLICertifySet,
//	                tLICertifyInsuredSet))
//	        {
//	            String tStrErr = "获取单证导入数据失败。";
//	            buildError("parseDiskFile", tStrErr);
//	            return false;
//	        }
	//
//	        if (mCertifyInfoList.size() == 0)
//	        {
//	            String tStrErr = "清单数据信息为空。请核实后重新导入。";
//	            buildError("parseDiskFile", tStrErr);
//	            return false;
//	        }

	        return true;
	    }

	    /**
	     * 创建单证清单。
	     * @return
	     */
	    private MMap createLRAccountList()
	    {	
	    	SSRS mSSRS = new SSRS();
	    	ExeSQL ExeSQL = new ExeSQL();
	        MMap tTmpMap = new MMap();
	        String tStrSql = null;
	        String sql = "";
	        String tSQL = "";
//	        String incomeType = "";
//	        String aTempCessFlag = "";
	        //数据类型
//	         String aDataType = (String)mTransferData.getValueByName("DataType");       
	        //获得流转日期
	        String CurDate = PubFun.getCurrentDate();
	        //获得年份
//	        String aBelongYear = PubFun.getCurrentDate().substring(0, 4);
	        String aTypeNo = "LR"+CurDate.substring(0,4)+CurDate.substring(5,7)+CurDate.substring(8,10);

	        for (int i = 1; i <= mLRAccountSet.size(); i++)
	        {	
	        	LRAccountSchema tLRAccountSchema = null;
	        	tLRAccountSchema = mLRAccountSet.get(i);
	            if (tLRAccountSchema == null)
	            {
	                continue;
	            }
	            //获得季度
	            int mon = Integer.parseInt(tLRAccountSchema.getBelongMonth());
	            String aBelongQuarter = "";
	         	if(mon<=3){
	         		aBelongQuarter = "01";
	        	}else if(mon>3&&mon<=6){
	        		aBelongQuarter = "02";;
	        	}else if (mon>6&&mon<=9){
	        		aBelongQuarter = "03";
	        	}else{
	        		aBelongQuarter ="04";
	        	}
	         	//判断 合同类型
//	         	if(tLRAccountSchema.getTempCessFlag().equals("合同分保")){
//	         		aTempCessFlag = "C";
//	         	}else{
//	         		aTempCessFlag = "T";
//	         	}
	         	tLRAccountSchema.setContNo("00000000000000000000");
	         	String aFinAccountDate = PubFun.calFLDate(tLRAccountSchema.getBelongYear()+"-"+tLRAccountSchema.getBelongMonth()+"-01")[1];
	         	//判断个单还是团单	         	
//	            tSQL = "select incometype from ljapay where incomeno = '"+tLRAccountSchema.getContNo()+"'";
//	            incomeType = ExeSQL.getOneValue(tSQL);
//            	if(incomeType.equals("1")){
//            		tLRAccountSchema.setGrpContNo(tLRAccountSchema.getContNo());
//            		incomeType = "";
//            	}
//            	if(incomeType.equals("2")){
//            		tLRAccountSchema.setGrpContNo("00000000000000000000");
//            		incomeType = "";
//            	}
            	//再保序列流水号
            	String aLRSerialNo =aTypeNo + PubFun1.CreateMaxNo(aTypeNo, 7);
            	//先set下边查询再取用
//            	tLRAccountSchema.setDataType(aDataType);
//            	tLRAccountSchema.setBelongYear(aBelongYear);
            	           	            	           		         	
	         	
	            //判断数据库中是否已存在该条记录 存在取引入次数最大值
		    	sql = "select recontcode,riskcode,grpcontno,contno,datatype,belongyear,belongmonth,MAX(importcount) from db2inst1.LRAccount where ReContCode = '"
		    	+tLRAccountSchema.getReContCode()
		    	+"' and RiskCode = '"+tLRAccountSchema.getRiskCode()
		    	+"' and GrpContNo = '"+tLRAccountSchema.getGrpContNo()
		    	+"' and ContNo = '"+tLRAccountSchema.getContNo()
		    	+"' and DataType = '"+tLRAccountSchema.getDataType()
		    	+"' and BelongYear = '"+tLRAccountSchema.getBelongYear()
		    	+"' and BelongMonth = '"+tLRAccountSchema.getBelongMonth()+"'"
		    	+" group by recontcode,riskcode,grpcontno,contno,datatype,belongyear,belongmonth";
		    	System.out.println(sql);
		    	mSSRS = ExeSQL.execSQL(sql);
		    	if(mSSRS.getMaxRow()>0){
		    		int mResultCount = Integer.parseInt(mSSRS.GetText(1, 8));
		    		sql = "";
		    		//如果存在更新引入次数
		    			int importCount = mResultCount+1;
		    			tLRAccountSchema.setImportCount(importCount);		    				    			   		
		    	}else{
		    		tLRAccountSchema.setImportCount(1);
		    	}	            	
	            	tLRAccountSchema.setBelongQuarter(aBelongQuarter);
	            	tLRAccountSchema.setMarketType(tLRAccountSchema.getMarketType().trim());
	            	tLRAccountSchema.setCessPremState("00");
	            	tLRAccountSchema.setClaimBackFeeState("00");
	            	tLRAccountSchema.setReProcFeeState("00");
//	            	tLRAccountSchema.setTempCessFlag(aTempCessFlag);
	            	tLRAccountSchema.setOprater(mGlobalInput.Operator);
	            	tLRAccountSchema.setMakeDate(PubFun.getCurrentDate());
	            	tLRAccountSchema.setMakeTime(PubFun.getCurrentTime());
	            	tLRAccountSchema.setModifyDate(PubFun.getCurrentDate());
	            	tLRAccountSchema.setModifyTime(PubFun.getCurrentTime());
	            	tLRAccountSchema.setFinAccountDate(aFinAccountDate);
	            	tLRAccountSchema.setLRSerialNo(aLRSerialNo);
	            	tTmpMap.put(tLRAccountSchema, "INSERT");
	            	            
	        }

//	        if (tAllSuccFlag && tTmpMap != null)
//	        {
//	            // 终止导入日志。
//	            MMap tTmpLogEnd = null;
//	            
//	            if (tTmpLogEnd == null)
//	            {
//	                String tStrErr = "终止导入日志动作失败。";
//	                buildError("createCertifyList", tStrErr);
//	                System.out.println(tStrErr);
//	                return null;
//	            }
//	            tTmpMap.add(tTmpLogEnd);
//	            tTmpLogEnd = null;
//	            // ---------------------------
//	        }

	        return tTmpMap;
	    }


	  

	    

	  

	   

	  

	    /**
	     * 准备导入前所需相关数据。
	     * @return
	     */
	    private boolean prepareBeforeImportListDatas()
	    {
	        mFileName = (String) mTransferData.getValueByName("FileName");
	        if (mFileName == null || mFileName.equals(""))
	        {
	            buildError("getInputData", "参数[导入文件名（FileName）]不存在。");
	            return false;
	        }

	        mFilePath = (String) mTransferData.getValueByName("FilePath");
	        if (mFilePath == null || mFilePath.equals(""))
	        {
	            buildError("getInputData", "参数[导入文件路径（FilePath）]不存在。");
	            return false;
	        }

	        return true;
	    }



	    

	    
	   

	  

	    /**
	     * 根据卡号从数据库加载卡信息。
	     * @param cCardNo
	     * @return
	     */
	    private LICertifySchema loadCertifyInfo(String cCardNo)
	    {
	        LICertifyDB tLICertifyDB = new LICertifyDB();
	        tLICertifyDB.setCardNo(cCardNo);
	        if (!tLICertifyDB.getInfo())
	        {
	            return null;
	        }

	        return tLICertifyDB.getSchema();
	    }

	   

	  

	  

	    

	    /**
	     * 创建错误日志。
	     * @param szFunc
	     * @param szErrMsg
	     */
	    private void buildError(String szFunc, String szErrMsg)
	    {
	        CError cError = new CError();
	        cError.moduleName = "LRAdujstDataUploadListBL";
	        cError.functionName = szFunc;
	        cError.errorMessage = szErrMsg;
	        this.mErrors.addOneError(cError);
	    }

	    /**
	     * 调用PubSubmit将map中数据进行提交
	     * @return boolean
	     */
	    private boolean submit()
	    {
	        VData data = new VData();
	        data.add(mMap);

	        PubSubmit p = new PubSubmit();
	        if (!p.submitData(data, ""))
	        {
	            System.out.println("提交数据失败");
	            buildError("submitData", "提交数据失败");
	            return false;
	        }

	        return true;
	    }

	    public VData getResult()
	    {
	        return mResult;
	    }
	    public static void main(String args[])
	    {
	    	System.out.println("3404930100".substring(4,6));
	    }
}
