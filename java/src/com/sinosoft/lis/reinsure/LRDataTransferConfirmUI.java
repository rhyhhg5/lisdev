package com.sinosoft.lis.reinsure;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LRDataTransferConfirmUI {
	public  CErrors mErrors=new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData =new VData();
	private String mOperate;


	public boolean submitData(VData cInputData,String cOperate)
	{
	  //将操作数据拷贝到本类中
	  this.mOperate =cOperate;
	  LRDataTransferConfirmBL tLRDataTransferConfirmBL=new LRDataTransferConfirmBL();
	  try{
		  if(!tLRDataTransferConfirmBL.submitData(cInputData,mOperate)){
			  if (tLRDataTransferConfirmBL.mErrors .needDealError() )
			  {	
				  // @@错误处理
				  this.mErrors.copyAllErrors(tLRDataTransferConfirmBL.mErrors);
				  CError tError = new CError();
				  tError.moduleName = "UI";
				  tError.functionName = "submitData";
				  tError.errorMessage = "数据提交失败!";
				  this.mErrors .addOneError(tError) ;
				  return false;
			  }
		  }
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return true;
  }
}