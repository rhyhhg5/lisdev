package com.sinosoft.sign;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.log.LogInfoDeal;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.WFTransLogSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-1-29 下午04:18:32 Description:卡单激活，远程出单通用方法
 * @version
 */

public class SignVerifyTool {
	private static String LogFilePath;

	private static String LogFileName;

	private static LogInfoDeal t;

	public OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}




	// serialNo
	public String getSerialNo() {

		// 得到流水号（当前时间加五位随机码）
		String serialNo = getCurrentDateTime("yyyyMMddHHmmss");
		for (int k = 0; k < 5; k++) {
			serialNo = serialNo + (int) (Math.random() * 10);
		}

		return serialNo;
	}

	/**
	 * 得到当前系统时间日期，自定义格式
	 * 
	 * @return 当前日期,参考格式为"yyyy-MM-dd HH:mm:ss E 本月第F个星期"
	 */

	public static String getCurrentDateTime(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		Date today = new Date();
		String tString = df.format(today);
		return tString;
	}


	// 生成日志1
	public static void writeEnterLog(String IP, OMElement element) {

		try {
			t = new LogInfoDeal("jianan", "02");
			String logTxt = "[IP：" + IP + "]" + "开始调用服务类ServerInterface."
					+ System.getProperty("line.separator") + "发送报文信息："
					+ element.toString() + System.getProperty("line.separator");
			LogFilePath = t.getLogFilePath();
			LogFileName = t.getLogFileName();
			t.WriteLogTxt(logTxt);

		} catch (Exception ex) {
			System.out.print("11" + ex.getMessage());
		}

	}


	// 生成日志4
	public static void writeLog(OMElement responseOME, String batchNo,
			String messageType, String sendDate, String sendTime,
			String batchFlag, String opertator, String errCode, String errInfo) {
		String logTxt = "返回报文信息：" + responseOME.toString()
				+ System.getProperty("line.separator") + "[批次号：" + batchNo
				+ "][批次交易类型：" + messageType + "]" + "[交易日期" + sendDate
				+ "][交易时间：" + sendTime + "][批次状态：" + batchFlag + "]"
				+ "[操作员：" + opertator + "]"
				+ System.getProperty("line.separator") + "			[错误编码：" + errCode
				+ "][错误信息：" + errInfo + "]"
				+ System.getProperty("line.separator");
		try {
			t.WriteLogTxt(LogFilePath, LogFileName, logTxt);
		} catch (Exception ex) {
			System.out.print("11" + ex.getMessage());
		}

		MMap map = new MMap();
		VData tVData = new VData();
		// 错误处理类
		CErrors mErrors = new CErrors();
		WFTransLogSchema trransLogSchema = new WFTransLogSchema();
		trransLogSchema.setBatchNo(batchNo);
		trransLogSchema.setMessageType(messageType);
		trransLogSchema.setSendDate(sendDate);
		trransLogSchema.setSendTime(sendTime);
		trransLogSchema.setBatchFlag(batchFlag);
		trransLogSchema.setopertator(opertator);
		trransLogSchema.setErrCode(errCode);
		trransLogSchema.setErrInfo(errInfo);

		map.put(trransLogSchema, "DELETE&INSERT");

		// 入库
		PubSubmit tPubSubmit = new PubSubmit();
		tVData.add(map);
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			// @@错误处理
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LoginVerifyTool";
			tError.functionName = "writeLog";
			tError.errorMessage = "数据提交失败!";
			mErrors.addOneError(tError);

		}
	}

	// 将发送报文中的条件字段封装为Map
	public Map getConditionMap(OMElement Oms) {
		Map conditionMap = new HashMap();
		Iterator iter1 = Oms.getChildren();
		while (iter1.hasNext()) {
			OMElement Om = (OMElement) iter1.next();

			Iterator child = Om.getChildren();

			while (child.hasNext()) {
				OMElement child_element = (OMElement) child.next();
				conditionMap.put(child_element.getLocalName(), child_element
						.getText());
			}

		}
		return conditionMap;

	}
	public static void main(String[] args) {
	}

}
