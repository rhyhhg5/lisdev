package com.sinosoft.sign;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;

import com.sinosoft.utility.ExeSQL;

public class SignXml {

	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

		try {
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return mByteArrayOutputStream.toByteArray();
	}

	public OMElement getSendXml(String tpath) {
		OMElement tomElement=null;
		String mInXmlStr = null;
		try {
			SignXml tSignXml = new SignXml();
			InputStream mIs = new FileInputStream(tpath);
			byte[] mInXmlBytes = tSignXml.InputStreamToBytes(mIs);
			mInXmlStr = new String(mInXmlBytes, "GBK");			
			System.out.println(mInXmlStr);
			tomElement = new StAXOMBuilder(new ByteArrayInputStream(  
					mInXmlStr.getBytes("UTF-8"))).getDocumentElement(); 
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tomElement;
	}

	public static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}

	public static OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}

	public  OMElement testCardQueryData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement GRPCONTSIGNWRAP = factory.createOMElement("GRPCONTSIGNWRAP", null);

		addOm(GRPCONTSIGNWRAP, "BATCHNO", "2016091217305500001");
		addOm(GRPCONTSIGNWRAP, "SENDDATE", "2016-09-12");
		addOm(GRPCONTSIGNWRAP, "SENDTIME", "17:30:55");
		addOm(GRPCONTSIGNWRAP, "MESSAGETYPE", "04");
	
		
		OMElement GRPCONTLIST = factory.createOMElement("GRPCONTLIST", null);
		GRPCONTSIGNWRAP.addChild(GRPCONTLIST);
		
		OMElement ITEM = factory.createOMElement("ITEM", null);
		
		addOm(ITEM, "PRTNO", "1E000003283");
		addOm(ITEM, "PREM", "30360.00");
		GRPCONTLIST.addChild(ITEM);
		

		root.addChild(GRPCONTSIGNWRAP);
		
	
		return root;
		// boolean rtv = TestClient.queryMCardData(root);
	}
 


	public static void main(String args[]) {
//		String mInFilePath = "F:/send.xml";
		SignXml txml = new SignXml();
//		OMElement Oms=txml.getSendXml(mInFilePath);
		OMElement Oms = txml.testCardQueryData();
		DealGrpContSignWrap tDealGrpContSignWrap = new DealGrpContSignWrap();
		tDealGrpContSignWrap.queryData(Oms);
		
//
//		Iterator iter1 = Oms.getChildren();
//		while(iter1.hasNext()){
//			OMElement Om = (OMElement) iter1.next();
//			System.out.println("aaaaa");
//			if(Om.getLocalName().equals("GRPCONTSIGNWRAP")){
//				Iterator child = Om.getChildren();
//				while(child.hasNext()){
//					OMElement child_element = (OMElement) child.next();
//					if(child_element.getLocalName().equals("BATCHNO")){
//						String batchno=child_element.getText();
//						System.out.println("batchno:"+batchno);
//						continue;
//					}
//					if(child_element.getLocalName().equals("SENDDATE")){
//						String sendDate=child_element.getText();
//						System.out.println(sendDate);
//						continue;
//					}
//					if(child_element.getLocalName().equals("SENDTIME")){
//						String sendTime= child_element.getText();
//						System.out.println(sendTime);
//						continue;
//					}
//					if(child_element.getLocalName().equals("MESSAGETYPE")){
//						String messageType=child_element.getText();
//						System.out.println(messageType);
//						continue;
//					}
//					if(child_element.getLocalName().equals("GRPCONTLIST")){
//						Iterator childItem = child_element.getChildren();
//						while(childItem.hasNext()){
//							OMElement childOME = (OMElement) childItem.next();
//							if(childOME.getLocalName().equals("ITEM")){
//								Iterator child1OME = childOME.getChildren();
//								while(child1OME.hasNext()){
//									OMElement childleaf = (OMElement) child1OME.next();
//									if(childleaf.getLocalName().equals("PRTNO")){
//										String mPrtNo=childleaf.getText();
//										System.out.println(mPrtNo);
//										continue;
//									}
//									if(childleaf.getLocalName().equals("PREM")){
//										String mPrem = childleaf.getText();
//										System.out.println(mPrem);
//										continue;
//									}
//								}
//							}
//						}
//					}
//				}
//			}else{
//				System.out.println("报文类型不匹配");
//			}
//		}
//	
	}
}
