package com.sinosoft.sign;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.sinosoft.lis.finfee.TempFeeBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.tb.GrpSignAfterPrintBL;
import com.sinosoft.lis.tb.GrpSignAfterPrintUI;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class DealGrpContSignWrap {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
	
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	
	private String messageType = "";// 报文类型
	
	private OMElement responseOME = null;
	
	private String mPrtNo = null;
	
	private String mPrem = null;
	
	private MMap tMMap = new MMap();
	
	private String theCurrentDate = PubFun.getCurrentDate();
	
	private LJTempFeeSchema tLJTempFeeSchema = null;
	private  LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
	private LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
	private LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
	
	Reflections tReflections = new Reflections();
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildResponseOME("01","01", "加载团单数据失败!");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			return responseOME;
		}
		if(!deal()){
			return responseOME;
		}
		if(!sign()){
			return responseOME;
		}
		if(!save()){
			return responseOME;
		}		
		responseOME = buildResponseOME("00","01", "");
		
		return responseOME;
	}
	
	//传来xml信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				System.out.println("aaaaa");
				if(Om.getLocalName().equals("GRPCONTSIGNWRAP")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDDATE")){
							this.sendDate=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDTIME")){
							this.sendTime= child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("MESSAGETYPE")){
							this.messageType=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("GRPCONTLIST")){
							Iterator childItem = child_element.getChildren();
							while(childItem.hasNext()){
								OMElement childOME = (OMElement) childItem.next();
								if(childOME.getLocalName().equals("ITEM")){
									Iterator child1OME = childOME.getChildren();
									while(child1OME.hasNext()){
										OMElement childleaf = (OMElement) child1OME.next();
										if(childleaf.getLocalName().equals("PRTNO")){
											this.mPrtNo=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("PREM")){
											this.mPrem = childleaf.getText();
											continue;
										}
									}
								}
							}
						}
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验
	 * @return
	 */
	private boolean check(){
		if(!checkInfo()){
			System.out.println("校验基本信息失败");
			return false;
		}
		return true;
	}
	
	/**
	 * 处理
	 * @return
	 */ 
	private boolean deal(){
		
		tLJTempFeeSet = new LJTempFeeSet();
		tLJTempFeeClassSet = new LJTempFeeClassSet();
		//校验保单是否核保通过
		String tSQL1 = "select 1 from lcgrpcont where prtno='" + this.mPrtNo
					 + "' and uwflag in ('9', '4') with ur";
		String result1 = new ExeSQL().getOneValue(tSQL1);
		if("".equals(result1)||result1==null){
			responseOME = buildResponseOME("01","01", "保单核保未通过!");
			System.out.println("responseOME:" + responseOME);
			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "保单核保未通过!");
			return false;
		}
		
		String tSQL2 = " select tempfeeno "
					 + " from ljtempfee "
					 + " where otherno='" + this.mPrtNo + "' "
					 + " and confdate is  null and tempfeeno in (select tempfeeno from ljtempfeeclass where tempfeeno=ljtempfee.tempfeeno and paymode in ('1','11','10','13','3','2','12','6','5'))";
		String result2 = new ExeSQL().getOneValue(tSQL2);
		if(!"".equals(result2)){
			responseOME = buildResponseOME("01","01", "已经收过费，缴费凭证号是："+result2);
			System.out.println("responseOME:" + responseOME);
			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "已经收过费，缴费凭证号是："+result2);
			return false;
		}
		
    	String tSQL3= "select riskcode,agentgroup,agentcode,prem,managecom from lcgrppol "
    				  +	"where prtno='"+this.mPrtNo+"'";
    	SSRS result3 = new ExeSQL().execSQL(tSQL3);
    	
    	if(result3.getMaxRow()>0){
    		for(int i=1;i<=result3.getMaxRow();i++){ 
    			tLJTempFeeSchema = new LJTempFeeSchema();
    			tLJTempFeeSchema.setTempFeeNo(this.mPrtNo+"801");
    			tLJTempFeeSchema.setTempFeeType("1");
    			tLJTempFeeSchema.setRiskCode(result3.GetText(i, 1));
    			tLJTempFeeSchema.setAgentGroup(result3.GetText(i, 2));
    		    tLJTempFeeSchema.setAgentCode(result3.GetText(i, 3));
    		    tLJTempFeeSchema.setPayDate(theCurrentDate);
    		    tLJTempFeeSchema.setEnterAccDate(theCurrentDate); //到帐日期
    		    tLJTempFeeSchema.setPayMoney(result3.GetText(i, 4));
    		    tLJTempFeeSchema.setManageCom(result3.GetText(i, 5));
    		    tLJTempFeeSchema.setOtherNo(this.mPrtNo);
    		    tLJTempFeeSchema.setOtherNoType("5");
    		    tLJTempFeeSchema.setOperator("EC_WX");
    		    tLJTempFeeSchema.setCashier("");
    		    tLJTempFeeSet.add(tLJTempFeeSchema);
    		}
    	}	
    	
    	String tSQL4= "select sum(prem),managecom from lcgrppol "
    				+ "where prtno='"+this.mPrtNo+"' "
    				+ "group by managecom ";
    	SSRS result4 = new ExeSQL().execSQL(tSQL4);
    	if(result4.getMaxRow()>0){
	    	tLJTempFeeClassSchema = new LJTempFeeClassSchema();
	    	tLJTempFeeClassSchema.setTempFeeNo(this.mPrtNo+"801");
	        tLJTempFeeClassSchema.setPayMode("1");
	        tLJTempFeeClassSchema.setPayDate(theCurrentDate);
	        tLJTempFeeClassSchema.setPayMoney(result4.GetText(1, 1));
	        tLJTempFeeClassSchema.setChequeNo("");
	        tLJTempFeeClassSchema.setChequeDate("");
	        tLJTempFeeClassSchema.setEnterAccDate(theCurrentDate);      
	        tLJTempFeeClassSchema.setManageCom(result4.GetText(1, 2));
	        tLJTempFeeClassSchema.setBankCode("");
	        tLJTempFeeClassSchema.setBankAccNo("");
	        tLJTempFeeClassSchema.setAccName("");
	        tLJTempFeeClassSchema.setInsBankCode("");
	        tLJTempFeeClassSchema.setInsBankAccNo("");
	        tLJTempFeeClassSchema.setOperator("EC_WX");
	        tLJTempFeeClassSchema.setCashier("");
	        tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
    	}	
    	
    	GlobalInput tGI = new GlobalInput();
    	tGI.Operator = "EC_WX";
    	tGI.ManageCom = "";
    	tGI.ComCode = "86";
    	tGI.ClientIP = "";
    	tGI.ServerIP = "";
    	tGI.AgentCom = "";
    	
        VData tVData = new VData();
        tVData.addElement(tLJTempFeeSet);
        tVData.addElement(tLJTempFeeClassSet);
        tVData.addElement(tGI);
        
        // 数据传输
        TempFeeBL tTempFeeBL = new TempFeeBL();
        tTempFeeBL.submitData(tVData, "INSERT");
        
        //如果有需要处理的错误，则返回
		if (tTempFeeBL.mErrors.needDealError()) {
			this.mErrors.copyAllErrors(tTempFeeBL.mErrors);
			System.out.println("error num=" + mErrors.getErrorCount());
		    String[] strResult = tTempFeeBL.getResult();
		    String errorss = strResult[0];
			responseOME = buildResponseOME("01","01", errorss);
			System.out.println("responseOME:" + responseOME);
			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", errorss);
			return false;
		}
           return true;
	}
	
	
	public static void main(String args[]) {
		String tSQL1= "select grpcontno from lcgrpcont "
				+ "where prtno='1E000003280' ";
		SSRS result1 = new ExeSQL().execSQL(tSQL1);
		GlobalInput tGI = new GlobalInput();
		tGI.Operator = "EC_WX";
		tGI.ManageCom = "";
		tGI.ComCode = "86";
		tGI.ClientIP = "";
		tGI.ServerIP = "";
		tGI.AgentCom = "";
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		tLCGrpContSchema.setGrpContNo(result1.GetText(1, 1));    	
		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(tGI);
		GrpSignAfterPrintBL tGrpSignAfertPrintBL = new GrpSignAfterPrintBL();
	    tGrpSignAfertPrintBL.submitData(tVData, "INSERT||MAIN");
	    //如果有需要处理的错误，则返回
//	    if (tGrpSignAfertPrintBL.mErrors.needDealError()) {
//	    // @@错误处理
//	    	String errorss = tGrpSignAfertPrintBL.mErrors.toString();
//	    	responseOME = buildResponseOME("01","01", errorss);
//			System.out.println("responseOME:" + responseOME);
//			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", errorss);
//			return false;
//	   }
//		boolean bl = tGrpSignAfterPrintUI.submitData(tVData,"");
//		System.out.println("预打签单是否成功："+bl);
	}
	
	private boolean sign(){
		
		String tSQL1= "select grpcontno,managecom from lcgrpcont "
					+ "where prtno='"+this.mPrtNo+"' ";
		SSRS result1 = new ExeSQL().execSQL(tSQL1);
		GlobalInput tGI = new GlobalInput();
    	tGI.Operator = "EC_WX";
    	tGI.ManageCom = result1.GetText(1, 2);
    	tGI.ComCode = result1.GetText(1, 2);
    	tGI.ClientIP = "";
    	tGI.ServerIP = "";
    	tGI.AgentCom = "";
    	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		tLCGrpContSchema.setGrpContNo(result1.GetText(1, 1));    	
		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(tGI);
		GrpSignAfterPrintBL tGrpSignAfertPrintBL = new GrpSignAfterPrintBL();
	    tGrpSignAfertPrintBL.submitData(tVData, "INSERT||MAIN");
	    //如果有需要处理的错误，则返回
	    if (tGrpSignAfertPrintBL.mErrors.needDealError()) {
	    // @@错误处理
	    	String errorss = tGrpSignAfertPrintBL.mErrors.toString();
	    	responseOME = buildResponseOME("01","01", errorss);
			System.out.println("responseOME:" + responseOME);
			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", errorss);
			return false;
	   }
	    String errorss = "预打签单成功";
    	responseOME = buildResponseOME("00","00", errorss);
		System.out.println("responseOME:" + responseOME);
		SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "00","00", "WX", errorss);
		return true;
	}
	
	
	
	/**
	 *校验基本信息 
	 * @return
	 */
	private boolean checkInfo(){
		if(this.batchno ==null || this.batchno.equals("")){
			responseOME = buildResponseOME("01","01", "没有得到批次号!");
			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "获取批次号失败!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if(this.mPrtNo ==null || this.mPrtNo.equals("")){
			responseOME = buildResponseOME("01","01", "获取保单印刷号码失败!");
			System.out.println("responseOME:" + responseOME);
			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "获取印刷号码失败!");
			return false;
		}
		String tSQL = "select 1 from lcgrpcont where prtno = '"+this.mPrtNo+"' ";
		String isHave = new ExeSQL().getOneValue(tSQL);
		if("".equals(isHave)||isHave==null){
			responseOME = buildResponseOME("01","01", "不存在印刷号对应的保单，请核查!");
			System.out.println("responseOME:" + responseOME);
			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "不存在印刷号对应的保单，请核查!");
			return false;
		}
		//保费校验
		String tSQL1 = "select sum(prem) from lcgrppol where prtno = '"+this.mPrtNo+"' ";
		String result1 = new ExeSQL().getOneValue(tSQL1);
		if(!result1.equals(this.mPrem)){
			responseOME = buildResponseOME("01","01", "所传保费，与汇总保费不一致！");
			System.out.println("responseOME:" + responseOME);
			SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "所传保费，与汇总保费不一致！");
			return false;
		}
		return true;
	}

	//提交卡激活信息
	private boolean save(){
		try{
			VData v = new VData();
			v.add(tMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				System.out.println("数据提交失败************************");
				responseOME = buildResponseOME("01","01", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				SignVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","99", "WX", "数据提交失败!");
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME("01","01", "数据提交失败!");
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	private OMElement buildResponseOME(String state, String errCode,String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("GRPCONTSIGN", null);
		SignVerifyTool tool = new SignVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "PrtNo", mPrtNo);
		String tSQL = "select grpcontno from lcgrpcont where prtno = '"+this.mPrtNo+"' ";
		String tGrpContNo = new ExeSQL().getOneValue(tSQL);
		tool.addOm(DATASET, "GrpContNo", tGrpContNo);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		System.out.println("反馈报文："+DATASET);
		return DATASET;
	}
}