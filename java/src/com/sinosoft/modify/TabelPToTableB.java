package modify;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class TabelPToTableB 
{
	private MMap mMap = new MMap();
	
	private VData mResult = new VData();
	
	private LPPolSet mLPPolSet = new LPPolSet();
	
	private LPDutySet mLPDutySet = new LPDutySet();
	
	private LPContSet mLPContSet = new LPContSet();
	
	private LPPremSet mLPPremSet = new LPPremSet();
	
	private LPGetSet mLPGetSet = new LPGetSet();
	
	private LPInsuredSet mLPInsuredSet = new LPInsuredSet();
	
	private String fileName="e://build.txt";
	
	private String currentDate=PubFun.getCurrentDate();
	
	private String currentTime=PubFun.getCurrentTime2();
	
	
	public boolean submitData()
	{
		//处理数据
		if (!dealData())
        {
            return false;
        }
        return true;
	}
	private boolean dealData()
	{
		
		RSWrapper rswrapper = new RSWrapper();
		String sql="select * from temp_texu_tishu";
		rswrapper.prepareData(null, sql);
        SSRS tSSRS = null;
        do
        {
            tSSRS = rswrapper.getSSRS();

	        for (int i=1;i<tSSRS.getMaxRow()+1;i++)
	        {
	        	String contNo=tSSRS.GetText(i,1);
	//        	校验团单表是否存在
	        	if(i==1)
	        	{
	        		 fileName="f://bak//"+PubFun.getCurrentDate()+"_"+PubFun.getCurrentTime2()+"_"+contNo+".txt";
	        	}
				if(!getGRPContNoByContno(contNo,"LPCont"))
				{
					String co=contNo+"Not"+"\n";
					method1(fileName,co);
					String deleteSql="delete from temp_texu_tishu where grpcontno='"+contNo+"'";
					mMap.put(deleteSql, "DELETE");
					continue;
				}
				if(!dealPol(contNo))
				{
					System.out.println("处理险种表出错");
					return false;
				}
				if(!dealDuty(contNo))
				{
					System.out.println("处理责任表出错");
					return false;
				}
				if(!dealCont(contNo))
				{
					System.out.println("处理保单表出错");
					return false;
				}
				if(!dealPrem(contNo))
				{
					System.out.println("处理保费表出错");
					return false;
				}
				if(!dealGet(contNo))
				{
					System.out.println("处理给付表出错");
					return false;
				}
				if(!dealInsured(contNo))
				{
					System.out.println("处理被保人表出错");
					return false;
				}
				
				//将已跑的保单好删除
				String deleteSql="delete from temp_texu_tishu where grpcontno='"+contNo+"'";
				mMap.put(deleteSql, "DELETE");
				
				//将保单好存到txt文件中
				String co=contNo+"\n";
				method1(fileName,co);	
	        }
			 mResult.add(mMap);
			//提交数据
			if(!submitMap())
			{
				return false;
			}
			mResult.clear();
        }
        while(tSSRS.getMaxRow()>0);
  
        return true;
	}

    private boolean submitMap() 
    {        
         PubSubmit tPubsubmit = new PubSubmit();
         if(!tPubsubmit.submitData(mResult, SysConst.INSERT))
         {
         	System.out.println("提交数据失败！"); 
         	return false;
         }
         this.mMap = new MMap();
         System.out.println("提交数据成功！");
         return true;
     }  
       
     
	//处理被保人表
	private boolean dealInsured( String contno)
	{
		String polSql="select distinct contno,insuredno from lpinsured where contno='"+contno+"' and edorno like '20%' ";
		SSRS mssrspolSql =new ExeSQL().execSQL(polSql); 
		if(mssrspolSql!= null && mssrspolSql.getMaxRow() > 0 )
		{
			for(int ii=1;ii< mssrspolSql.getMaxRow()+1;ii++)
			{
				
			
				LCInsuredSet aLCInsuredSet = new LCInsuredSet();
//				String tSql="select * from LPInsured where contno in ('"+contno+"')";
				String tSql="select * from LPInsured where contno in ('"
				           +mssrspolSql.GetText(ii, 1)
				           +"') and insuredno='"
				           +mssrspolSql.GetText(ii, 2)
				           +"' order by makedate desc,maketime desc fetch first 1 row only ";
				Reflections tRef = new Reflections();
				LPInsuredDB tLPInsuredDB = new LPInsuredDB();
				mLPInsuredSet = tLPInsuredDB.executeQuery(tSql);
//				for (int i = 1; i <= mLPInsuredSet.size(); i++)
				{
		//			校验C表中原来存在的数据
					boolean flag=false;
					String checkInsured="select 1 from LCInsured where contno='"+mLPInsuredSet.get(1).getContNo()
					                +"' and insuredno='"+mLPInsuredSet.get(1).getInsuredNo()+"'" ;
					SSRS mssrs1 =new ExeSQL().execSQL(checkInsured); 
				  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
				  	{
				  		flag=true;
				  		System.out.println("C（LCInsured）表中该被保人信息:"+mLPInsuredSet.get(1).getInsuredNo()+"已存在");
				  	}
//					if(quchongLPInsuredSet.size()==0)
//					{
//						quchongLPInsuredSet.add(mLPInsuredSet.get(i));
//					}
//					else
//					{
//						for (int j = 1; j <= quchongLPInsuredSet.size(); j++)
//						{
//							if((!mLPInsuredSet.get(i).getContNo().equals(quchongLPInsuredSet.get(j).getContNo()))
//							 ||(!mLPInsuredSet.get(i).getInsuredNo().equals(quchongLPInsuredSet.get(j).getInsuredNo())))
//							{
//								quchongLPInsuredSet.add(mLPInsuredSet.get(i));
//							}
//							else
//							{
//								flag=true;
//							}
//						}
//					}
					
					if(flag)
					{
						continue;
					}
				    //将P表中数据导入C表
					LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
					tLCInsuredSchema=(LCInsuredSchema)tRef.transFields(tLCInsuredSchema, mLPInsuredSet.get(1));
					aLCInsuredSet.add(tLCInsuredSchema);

				}
				mMap.put(aLCInsuredSet, "INSERT");
				//mMap.put(aLPInsuredSet, "UPDATE");
			}
		}
		return true;
	}
	
//	处理给付表
	private boolean dealGet(String contno)
	{
		String polSql="select distinct polno,dutycode,getdutycode from lpget where contno='"+contno+"' ";
		SSRS mssrspolSql =new ExeSQL().execSQL(polSql); 
		if(mssrspolSql!= null && mssrspolSql.getMaxRow() > 0 )
		{
			for(int ii=1;ii< mssrspolSql.getMaxRow()+1;ii++)
			{
				
			
				LCGetSet aLCGetSet = new LCGetSet();
//				String tSql="select * from LPGet where contno in ('"+contno+"')";
				String tSql="select * from LPGet where contno in ('"+contno+"')"
		                   +"and polno in ('"
				           +mssrspolSql.GetText(ii, 1)
				           +"') and dutycode='"
				           +mssrspolSql.GetText(ii, 2)
				           +"' and getdutycode='"
				           +mssrspolSql.GetText(ii, 3)
				           +"' order by makedate desc,maketime desc fetch first 1 row only ";
				Reflections tRef = new Reflections();
				LPGetDB tLPGetDB = new LPGetDB();
				mLPGetSet = tLPGetDB.executeQuery(tSql);
//				for (int i = 1; i <= mLPGetSet.size(); i++)
				{
		//			校验C表中原来存在的数据
					boolean flag=false;
					String checkGet="select 1 from LCGet where contno='"+mLPGetSet.get(1).getContNo()
					                +"' and polno='"+mLPGetSet.get(1).getPolNo()
					                +"' and dutycode='"+mLPGetSet.get(1).getDutyCode()
					                +"' and getdutycode='"+mLPGetSet.get(1).getGetDutyCode()+"'" ;
					SSRS mssrs1 =new ExeSQL().execSQL(checkGet); 
				  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
				  	{
				  		flag=true;
				  		System.out.println("C（GET）表中该给付信息:"+mLPGetSet.get(1).getDutyCode()+"已存在");
				  	}
//					if(quchongLPGetSet.size()==0)
//					{
//						quchongLPGetSet.add(mLPGetSet.get(i));
//					}
//					else
//					{
//						for (int j = 1; j <= quchongLPGetSet.size(); j++)
//						{
//							if((!mLPGetSet.get(i).getPolNo().equals(quchongLPGetSet.get(j).getPolNo()))
//							 ||(!mLPGetSet.get(i).getDutyCode().equals(quchongLPGetSet.get(j).getDutyCode()))
//							 ||(!mLPGetSet.get(i).getGetDutyCode().equals(quchongLPGetSet.get(j).getGetDutyCode()))
//								)
//							{
//								quchongLPGetSet.add(mLPGetSet.get(i));
//							}
//							else
//							{
//								flag=true;
//							}
//						}
//					}
					
					if(flag)
					{
						continue;
					}
				    //将P表中数据导入C表
					LCGetSchema tLCGetSchema = new LCGetSchema();
					tLCGetSchema=(LCGetSchema)tRef.transFields(tLCGetSchema, mLPGetSet.get(1));
					aLCGetSet.add(tLCGetSchema);
				}
				mMap.put(aLCGetSet, "INSERT");
				//mMap.put(aLPGetSet, "UPDATE");
			}
		}
		return true;
	}
	
//	处理保费表
	private boolean dealPrem(String contno)
	{
		String polSql="select distinct polno,dutycode,payplancode from lpprem where contno='"+contno+"' ";
		SSRS mssrspolSql =new ExeSQL().execSQL(polSql); 
		if(mssrspolSql!= null && mssrspolSql.getMaxRow() > 0 )
		{
			for(int ii=1;ii< mssrspolSql.getMaxRow()+1;ii++)
			{
				
			
				LCPremSet aLCPremSet = new LCPremSet();
//				LPPremSet aLPPremSet = new LPPremSet();
//				String tSql="select * from LPPrem where contno in ('"+contno+"')";
				String tSql="select * from LPPrem where contno in ('"+contno+"')"
		                   +"and polno in ('"
				           +mssrspolSql.GetText(ii, 1)
				           +"') and dutycode='"
				           +mssrspolSql.GetText(ii, 2)
				           +"' and payplancode='"
				           +mssrspolSql.GetText(ii, 3)
				           +"' order by makedate desc,maketime desc fetch first 1 row only ";
				Reflections tRef = new Reflections();		
				LPPremDB tLPPremDB = new LPPremDB();
				mLPPremSet = tLPPremDB.executeQuery(tSql);
		//		防止插入是主键重复
				LPPremSet quchongLPPremSet = new LPPremSet();
//				for (int i = 1; i <= mLPPremSet.size(); i++)
				{
		//			校验C表中原来存在的数据
					boolean flag=false;
					String checkPrem="select 1 from LCPrem where contno='"+mLPPremSet.get(1).getContNo()
					                +"' and polno='"+mLPPremSet.get(1).getPolNo()
					                +"' and dutycode='"+mLPPremSet.get(1).getDutyCode()
					                +"' and payplancode='"+mLPPremSet.get(1).getPayPlanCode()+"'" ;
					SSRS mssrs1 =new ExeSQL().execSQL(checkPrem); 
				  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
				  	{
				  		flag=true;
				  		System.out.println("C（Prem）表中该保费信息:"+mLPPremSet.get(1).getDutyCode()+"已存在");
				  	}
//					if(quchongLPPremSet.size()==0)
//					{
//						quchongLPPremSet.add(mLPPremSet.get(i));
//					}
//					else
//					{
//						for (int j = 1; j <= quchongLPPremSet.size(); j++)
//						{
//							if((!mLPPremSet.get(i).getPolNo().equals(quchongLPPremSet.get(j).getPolNo()))
//							 ||(!mLPPremSet.get(i).getDutyCode().equals(quchongLPPremSet.get(j).getDutyCode()))
//							 ||(!mLPPremSet.get(i).getPayPlanCode().equals(quchongLPPremSet.get(j).getPayPlanCode()))
//									)
//							{
//								quchongLPPremSet.add(mLPPremSet.get(i));
//							}
//							else
//							{
//								flag=true;
//							}
//						}
//					}
					
					if(flag)
					{
						continue;
					}
					
				    //将P表中数据导入C表
					LCPremSchema tLCPremSchema = new LCPremSchema();
					tLCPremSchema=(LCPremSchema)tRef.transFields(tLCPremSchema, mLPPremSet.get(1));
					aLCPremSet.add(tLCPremSchema);

				}
				mMap.put(aLCPremSet, "INSERT");
				//mMap.put(aLPPremSet, "UPDATE");
			}
		}
		return true;
	}
	
//	处理保单表
	private boolean dealCont(String contno)
	{
		String polSql="select distinct contno from lpcont where contno='"+contno+"'  group by edorno,contno,edortype";
		SSRS mssrspolSql =new ExeSQL().execSQL(polSql); 
		System.out.println(mssrspolSql.getMaxRow());
		if(mssrspolSql!= null && mssrspolSql.getMaxRow() > 0 )
		{
			for(int ii=1;ii< mssrspolSql.getMaxRow()+1;ii++)
			{
			
				LCContSet aLCContSet = new LCContSet();
//				LPContSet aLPContSet = new LPContSet();
		//		String tSql="select * from LPCont where contno in ('"+contno+"')";
				String tSql="select * from LPCont where contno in ('"+mssrspolSql.GetText(ii, 1)+"')"
		        +" order by makedate desc,maketime desc fetch first 1 row only ";
		
				Reflections tRef = new Reflections();
				LPContDB tLPContDB = new LPContDB();
				mLPContSet = tLPContDB.executeQuery(tSql);
		//		防止插入是主键重复
				LPContSet quchongLPContSet = new LPContSet();
//				for (int i = 1; i <= mLPContSet.size(); i++)
				{
		//			校验C表中原来存在的数据
					boolean flag=false;
					String checkCont="select 1 from LCCont where contno='"+mLPContSet.get(1).getContNo()+"' " ;
					SSRS mssrs1 =new ExeSQL().execSQL(checkCont); 
				  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
				  	{
				  		flag=true;
				  		System.out.println("C（Cont）表中该保单信息:"+mLPContSet.get(1).getContNo()+"已存在");
				  	}
//					
//					if(quchongLPContSet.size()==0)
//					{
//						quchongLPContSet.add(mLPContSet.get(i));
//					}
//					else
//					{
//						for (int j = 1; j <= quchongLPContSet.size(); j++)
//						{
//							if(!mLPContSet.get(i).getContNo().equals(quchongLPContSet.get(j).getContNo()))
//							{
//								quchongLPContSet.add(mLPContSet.get(i));
//							}
//							else
//							{
//								flag=true;
//							}
//						}
//					}
					
					if(flag)
					{
						continue;
					}
				    //将P表中数据导入C表
					LCContSchema tLCContSchema = new LCContSchema();
					tLCContSchema=(LCContSchema)tRef.transFields(tLCContSchema, mLPContSet.get(1));
					aLCContSet.add(tLCContSchema);

				}
				mMap.put(aLCContSet, "INSERT");
			}
		}
		return true;
	}	

   //	处理责任表
	private boolean dealDuty(String contno)
	{
		String polSql="select distinct polno,dutycode from lpduty where contno='"+contno+"' ";
		SSRS mssrspolSql =new ExeSQL().execSQL(polSql); 
//		System.out.println(mssrspolSql.getMaxRow());
		if(mssrspolSql!= null && mssrspolSql.getMaxRow() > 0 )
		{
			for(int ii=1;ii< mssrspolSql.getMaxRow()+1;ii++)
			{
				
				LCDutySet aLCDutySet = new LCDutySet();
//				LPDutySet aLPDutySet = new LPDutySet();
//				String tSql="select * from LPDuty where contno in ('"+contno+"')";
				String tSql="select * from LPDuty where contno in ('"+contno+"')"
		                   +"and polno in ('"
				           +mssrspolSql.GetText(ii, 1)
				           +"') and dutycode='"
				           +mssrspolSql.GetText(ii, 2)+"' order by makedate desc,maketime desc fetch first 1 row only ";
				Reflections tRef = new Reflections();
				LPDutyDB tLPDutyDB = new LPDutyDB();
				mLPDutySet = tLPDutyDB.executeQuery(tSql);
				
		//		防止插入是主键重复
//				LPDutySet quchongLPDutySet = new LPDutySet();
//				for (int i = 1; i <= mLPDutySet.size(); i++)
				
		//			校验C表中原来存在的数据
					boolean flag=false;
					String checkDuty="select 1 from LCDuty where contno='"+mLPDutySet.get(1).getContNo()+"' " +
							"and polno='"+mLPDutySet.get(1).getPolNo()+"' and dutycode='"+mLPDutySet.get(1).getDutyCode()+"'";
					SSRS mssrs1 =new ExeSQL().execSQL(checkDuty); 
				  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
				  	{
				  		flag=true;
				  		System.out.println("C（Duty）表中该责任信息:"+mLPDutySet.get(1).getDutyCode()+"已存在");
				  	}
//					
//					if(quchongLPDutySet.size()==0)
//					{
//						quchongLPDutySet.add(mLPDutySet.get(i));
//					}
//					else
//					{
//						for (int j = 1; j <= quchongLPDutySet.size(); j++)
//						{
//							if((!mLPDutySet.get(i).getPolNo().equals(quchongLPDutySet.get(j).getPolNo()))
//							   ||(!mLPDutySet.get(i).getDutyCode().equals(quchongLPDutySet.get(j).getDutyCode()))
//									)
//							{
//								quchongLPDutySet.add(mLPDutySet.get(i));
//							}
//							else
//							{
//								flag=true;
//							}
//						}
//					}
					
					if(flag)
					{
						continue;
					}
					
					
				    //将P表中数据导入C表
					LCDutySchema tLCDutySchema = new LCDutySchema();
					tLCDutySchema=(LCDutySchema)tRef.transFields(tLCDutySchema, mLPDutySet.get(1));
					aLCDutySet.add(tLCDutySchema);
				
				mMap.put(aLCDutySet, "INSERT");
			}
		}
		//mMap.put(aLPDutySet, "UPDATE");
		return true;
	}
	
	
	//处理险种表
	private boolean dealPol(String contno)
	{
		String polSql="select distinct polno from lppol where contno='"+contno+"' group by edorno,polno,edortype";
		//得到保单下的险种
		SSRS mssrspolSql =new ExeSQL().execSQL(polSql); 
//		System.out.println(mssrspolSql.getMaxRow());
		if(mssrspolSql!= null && mssrspolSql.getMaxRow() > 0 )
		{
			for(int ii=1;ii< mssrspolSql.getMaxRow()+1;ii++)
			{
				System.out.println(ii+"ii");
				LCPolSet aLCPolSet = new LCPolSet();
				//得到该险种号最近的一条记录
				String tSql="select * from LPPol where contno in ('"+contno+"')"
				           +"and polno='"
				           +mssrspolSql.GetText(ii, 1)
				           +"' order by makedate desc,maketime desc fetch first 1 row only ";
				Reflections tRef = new Reflections();
				LPPolDB tLPPolDB = new LPPolDB();
				mLPPolSet = tLPPolDB.executeQuery(tSql);
				

//				for (int i = 1; i <= mLPPolSet.size(); i++)
//				{
					//校验C表中原来存在的数据
					boolean flag=false;
					String checkPol="select 1 from LCPol where contno='"+mLPPolSet.get(1).getContNo()+"' and polno='"+mLPPolSet.get(1).getPolNo()+"'";
					SSRS mssrs1 =new ExeSQL().execSQL(checkPol); 
				  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
				  	{
				  		flag=true;
				  		System.out.println("C（Pol）表中该险种信息:"+mLPPolSet.get(1).getPolNo()+"已存在");
				  	}
			
					if(flag)
					{
						continue;
					}
					
				    //将P表中数据导入C表
					LCPolSchema tLCPolSchema = new LCPolSchema();
					tLCPolSchema=(LCPolSchema)tRef.transFields(tLCPolSchema, mLPPolSet.get(1));
					aLCPolSet.add(tLCPolSchema);

//				}
		
				mMap.put(aLCPolSet, "INSERT");
				
		
			}
		}
		
		//mMap.put(aLPPolSet, "UPDATE");
		return true;
	}
	// 通过保单号得到团单号
	private boolean getGRPContNoByContno(String contNo,String tableName)
	{
		String sqlGRPContNo="select grpContNo from "+tableName+" where contNo='"+contNo+"'";
		String gRPContNo =new ExeSQL().getOneValue(sqlGRPContNo);

	        if(!checkGRPCunZai(gRPContNo))
	        {
	        	return false;
	        }
	        return true;
	}
	//校验grpContno的团单号是否在团单表中有记录
	private boolean checkGRPCunZai(String grpContno)
	{
		if(grpContno.equals("00000000000000000000"))
		{
			return false;
		}
		else
		{
			String checkGRPCunZai="select 1 from lcgrpcont where grpcontno='"+grpContno+"'";
			SSRS mssrs1 =new ExeSQL().execSQL(checkGRPCunZai); 
		  	if(mssrs1!= null&& mssrs1.getMaxRow() == 0)
		  	{
		  		System.out.println("C（GRP）表中该团单信息:"+grpContno+"不存在");
		  		return false;
		  	}
		}
		return true;
	}
	
	public static void method1(String file, String conent) 
	{
		   BufferedWriter out = null;
		   try 
		   {
		      out = new BufferedWriter(new OutputStreamWriter(
		      new FileOutputStream(file, true)));
		      out.write(conent);
		   } 
		   catch (Exception e) 
		   {
		      e.printStackTrace();
		   } 
		   finally 
		   {
		    try 
		    {
		       out.close();
		    }	
		    catch (IOException e) 
			   {
			      e.printStackTrace();
			   } 
		   }
	}
	
	
	public static void main(String[] args) {
		
		TabelPToTableB bl =new TabelPToTableB();
		bl.submitData();
	}
	
}