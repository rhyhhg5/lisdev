package com.sinosoft.task;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.Connection;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 电话回访转交BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Yang Yalin
 * @version 1.0
 * @date 2005-03-31
 */

public class PhoneAnwserBL
{
    /**错误保存容器*/
    public CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 输出数据的容器
    private VData mResult = new VData();

    // 数据操作字符串
    private String mOperate;

    private LGPhoneAnwserSet mLGPhoneAnwserSet = null;

    private MMap map = new MMap();

    // 全局参数
    private GlobalInput mGlobalInput = null;

    //统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    //控制访问数据库变量
    private Connection conn = DBConnPool.getConnection();
    VData v = new VData();

    public PhoneAnwserBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;

        //进行业务处理
        if (!dealData())
        {
            this.mErrors.addOneError("问卷录入失败");
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            //获得信箱节点
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            String nodeNo = "";
            LGTraceNodeOpSchema mLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
            TaskTraceNodeOpBL tTraceNodeOpBL = new TaskTraceNodeOpBL();

            String s = "select nodeNo "
                       + "from LGWork "
                       + "where workNo='"
                       + mLGPhoneAnwserSet.get(1).getPhoneWorkNo() + "'";
            try
            {
                tSSRS = tExeSQL.execSQL(s);
            }
            catch (Exception ex)
            {
                try
                {
                    this.conn.rollback();
                    this.conn.close();
                }
                catch (Exception e)
                {
                    System.out.println("获得信箱节点号时出错且数据库操作回滚失败.");
                }
            }
            if (tSSRS.getMaxRow() == 1)
            {
                nodeNo = tSSRS.GetText(1, 1);
            }

            PubSubmit tPubSubmit = new PubSubmit();
            tPubSubmit.setCommitFlag(false);
            tPubSubmit.setConnection(conn);

            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLDDiseaseBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);

                //数据库操作回滚
                try
                {
                    conn.rollback();
                    conn.close();
                }
                catch (Exception ex)
                {
                    System.out.println("提交PhoneAnwser数据时出错且数据库回滚失败: " + ex.toString());
                }

                return false;
            }

            if(mOperate.equals("UPDATE||MAIN"))
            {
                //经办,需生成历史轨迹信息
                mLGTraceNodeOpSchema.setWorkNo(mLGPhoneAnwserSet.get(1).
                                               getPhoneWorkNo());
                mLGTraceNodeOpSchema.setNodeNo(nodeNo);
                mLGTraceNodeOpSchema.setOperatorType("7");
                mLGTraceNodeOpSchema.setRemark("自动批注:电话回访经办");
                mLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
                mLGTraceNodeOpSchema.setFinishTime(mCurrentTime);

                v.add(mGlobalInput);
                v.add(mLGTraceNodeOpSchema);
                v.add(Boolean.valueOf(true));
                v.add(conn);

                if (!tTraceNodeOpBL.submitData(v, "INSERT||MAIN"))
                {
                    System.out.println("生成历史节点时出错");
                    CError.buildErr(this, "生成历史节点时出错: ");

                    try
                    {
                        conn.rollback();
                        conn.close();
                    }
                    catch (Exception e)
                    {
                        System.out.println("关闭数据库失败: " + e.toString());
                    }

                    return false;
                }
            }//if(mOperate.equals("UPDATE||MAIN"))
        }

        try
        {
            conn.commit();
            conn.close();
            System.out.println("数据提交成功");
        }
        catch (Exception e)
        {
            System.out.println("数据库提交失败: " + e.toString());
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception ex)
            {
                System.out.println("数据库回滚失败: " + ex.toString());
            }
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLGPhoneAnwserSet = (LGPhoneAnwserSet) cInputData.getObject(0);
        this.mGlobalInput = ((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        System.out.println("Number of answers input is " +
                           mLGPhoneAnwserSet.size());

        return true;
    }

    private boolean dealData()
    {
        if (mOperate.equals("UPDATE||MAIN"))
        {
            LGPhoneAnwserDB tLGPhoneAnswerDB = new LGPhoneAnwserDB();
            tLGPhoneAnswerDB.setPhoneWorkNo(mLGPhoneAnwserSet.get(1)
                                            .getPhoneWorkNo());
            LGPhoneAnwserSet tLGPhoneAnwserSet = tLGPhoneAnswerDB.query();
            if(tLGPhoneAnwserSet.size() == 0)
            {
                mErrors.addOneError("没有查询到原来的电话回访记录。");
                return false;
            }
            for (int i = 1; i < mLGPhoneAnwserSet.size() + 1; i++)
            {
                mLGPhoneAnwserSet.get(i).setPhoneTypeNo(tLGPhoneAnwserSet.get(1)
                    .getPhoneTypeNo());
                mLGPhoneAnwserSet.get(i).setOperator(mGlobalInput.Operator);
                mLGPhoneAnwserSet.get(i).setMakeDate(mCurrentDate);
                mLGPhoneAnwserSet.get(i).setMakeTime(mCurrentTime);
                mLGPhoneAnwserSet.get(i).setModifyDate(mCurrentDate);
                mLGPhoneAnwserSet.get(i).setModifyTime(mCurrentTime);
            } //for
            map.put("delete from LGPhoneAnwser " +
                    "where phoneWorkNo='" +
                    mLGPhoneAnwserSet.get(1).getPhoneWorkNo() + "'  ",
                    "DELETE");
            map.put(mLGPhoneAnwserSet, "INSERT");

            String strUpdate =
                "update LGWork " +
                "set StatusNo='3', " +
                "modifyDate='" + mCurrentDate + "', " +
                "modifyTime='" + mCurrentTime + "', " +
                "Operator = '" + mGlobalInput.Operator + "' "
                + "where workNo='"
                + mLGPhoneAnwserSet.get(1).getPhoneWorkNo() + "'  ";
            map.put(strUpdate, "UPDATE");
        }
        else if (mOperate.equals("INSERT||MAIN"))
        {
            for (int i = 1; i < mLGPhoneAnwserSet.size() + 1; i++)
            {
                mLGPhoneAnwserSet.get(i).setOperator(mGlobalInput.Operator);
                mLGPhoneAnwserSet.get(i).setMakeDate(mCurrentDate);
                mLGPhoneAnwserSet.get(i).setMakeTime(mCurrentTime);
                mLGPhoneAnwserSet.get(i).setModifyDate(mCurrentDate);
                mLGPhoneAnwserSet.get(i).setModifyTime(mCurrentTime);
                map.put(mLGPhoneAnwserSet.get(i), "INSERT"); //插入
            }
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLGPhoneAnwserSet);
            this.mInputData.add(this.map);
            mResult.clear();
            mResult.add(this.mLGPhoneAnwserSet);
        }
        catch (Exception ex)
        {
            this.mErrors.addOneError("准备数据出错。");
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LGPhoneAnwserDBSet tLGPhoneAnwserDBSet = new LGPhoneAnwserDBSet();
        for (int i = 0; i < mLGPhoneAnwserSet.size(); i++)
        {
            tLGPhoneAnwserDBSet.get(i).setSchema(this.mLGPhoneAnwserSet.get(i));
            //如果有需要处理的错误，则返回
            if (tLGPhoneAnwserDBSet.get(i).mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLGPhoneAnwserDBSet.get(i).mErrors);
                CError tError = new CError();
                tError.moduleName = "LGWorkBoxBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    public static void main(String s[])
    {
        PhoneAnwserBL aPhoneAnwserBL = new PhoneAnwserBL();
        LGPhoneAnwserSet set = new LGPhoneAnwserSet();
        GlobalInput tG = new GlobalInput();
        VData tVData = new VData();

        tG.Operator = "001";

        System.out.println("In the main() method");
        for (int i = 1; i < 4; i++)
        {
            LGPhoneAnwserSchema schema = new LGPhoneAnwserSchema();
            schema.setPhoneWorkNo("20050620000077");
            schema.setPhoneRemark("////////////////////////");
            schema.setPhoneItemNo("" + i);
            schema.setPhoneAnwer("" + i % 2);
            set.add(schema);

            //验证数据是否存到LGPhoneAnwserSet中
            System.out.print(schema.getPhoneWorkNo() + "  ");
            System.out.print(schema.getPhoneItemNo() + "  ");
            System.out.print(schema.getPhoneAnwer() + "  ");
            System.out.print(schema.getPhoneRemark() + "  ");
            System.out.print(schema.getPhoneTypeNo() + "  ");
            System.out.print(schema.getOperator() + "  ");
            System.out.print(schema.getMakeDate() + "  ");
            System.out.print(schema.getMakeTime() + "  ");
            System.out.print(schema.getModifyDate() + "  ");
            System.out.println(schema.getModifyTime() + "  ");
        }

        tVData.add(set);
        tVData.add(tG);
        System.out.println("The size of set is " + set.size());

        aPhoneAnwserBL.submitData(tVData, "UPDATE||MAIN");
        System.out.println("After tVdata.add(tg)");
    }

}
