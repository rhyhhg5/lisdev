package com.sinosoft.task;

import java.util.Map;

import com.cbsws.obj.EdorGRPLQChangeInfo;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BqConfirmUI;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.FeeNoticeGrpVtsUI;
import com.sinosoft.lis.bq.FeeNoticeVtsUI;
import com.sinosoft.lis.bq.FinanceDataBL;
import com.sinosoft.lis.bq.GrpEdorItemUI;
import com.sinosoft.lis.bq.GrpEdorLQDetailBL;
import com.sinosoft.lis.bq.PEdorAppAccConfirmBL;
import com.sinosoft.lis.bq.PEdorConfirmBL;
import com.sinosoft.lis.bq.PGrpEdorAppConfirmUI;
import com.sinosoft.lis.bq.PGrpEdorConfirmBL;
import com.sinosoft.lis.bq.PrtAppEndorsementBL;
import com.sinosoft.lis.bq.PrtGrpEndorsementBL;
import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LPGrpEdorMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorMainSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BQGrpLQEdorBL {

	/** 错误保存容器 */
	public CErrors mErrors = new CErrors();

	/** 全局变量 */
	private GlobalInput mGlobalInput = null;

	private LGWorkSchema mLGWorkSchema = null;

	private EdorGRPLQChangeInfo mEdorGRPLQChangeInfo = null;

	private MMap mMMap_AddItem = new MMap();

	private String mTypeFlag = null;

	private String mGUFlag = null;

	private String mEdorAcceptNo = null;

	private String mContNo = null;

	private String mEdorType = BQ.EDORTYPE_LQ;

	private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	
	private LJFIGetSchema mLJFIGetSchema = new LJFIGetSchema();
	
	private LJAGetSchema mLJAGetSchema = new LJAGetSchema();

	private String mCustomerno = "";

	boolean isAppntFlag = false;

	/** 当前日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 当前时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	//private LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 得到生成的工单号
	 * 
	 * @return String
	 */
	public String getEdorAcceptNo() {
		return mEdorAcceptNo;
	}

	/**
	 * 得到输入数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {

		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
				.getObjectByObjectName("LPGrpEdorItemSchema", 0);

		mEdorGRPLQChangeInfo = (EdorGRPLQChangeInfo) cInputData
				.getObjectByObjectName("EdorGRPLQChangeInfo", 0);

		mLGWorkSchema = (LGWorkSchema) cInputData.getObjectByObjectName(
				"LGWorkSchema", 0);
		mCustomerno = mLGWorkSchema.getCustomerNo();
		mContNo = mLGWorkSchema.getContNo();

		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		// 添加工单受理
		mEdorAcceptNo = createTask();
		if ((mEdorAcceptNo == null) || (mEdorAcceptNo.equals(""))) {
			return false;
		}
		mLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
		if (!addEdorItem()) {
			return false;
		}
		if (!saveDetail()) {
			return false;
		}
		// 保全理算
		if (!appConfirm()) {
			return false;
		}
		// creatPrintVts();
		// 保全确认
		if (!edorConfirm()) {
			return false;
		}
		
//		if (!endEdor()) {
//			return false;
//		}
		return true;
	}

	/**
	 * 创建工单，添加工单受理
	 * 
	 * @return boolean
	 */
	private String createTask() {
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(mLGWorkSchema);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			mErrors.addOneError("工单数据生成失败！");
			return null;
		}
		return tTaskInputBL.getWorkNo();
	}

	/**
	 * 添加保全项目
	 * 
	 * @return boolean
	 */
	private boolean addEdorItem() {

		if (!checkEdorItem()) {
			mErrors.addOneError("一卡通添加保全项目校验时出错！！！");
			return false;
		}

		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
		tLPGrpEdorMainSchema.setEdorAppDate(mCurrentDate);
		tLPGrpEdorMainSchema.setEdorValiDate(mEdorGRPLQChangeInfo
				.getEdorCValiDate());
		tLPGrpEdorMainSchema.setEdorState("1");
		tLPGrpEdorMainSchema.setUWState("0");
		tLPGrpEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
		tLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);
		tLPGrpEdorMainSchema.setMakeDate(PubFun.getCurrentDate());
		tLPGrpEdorMainSchema.setMakeTime(PubFun.getCurrentTime());
		tLPGrpEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
	      tLPGrpEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
		

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
		tLPGrpEdorItemSchema.setEdorType(mEdorType);
		tLPGrpEdorItemSchema.setEdorState("3");
		tLPGrpEdorItemSchema.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
		tLPGrpEdorItemSchema.setEdorAppDate(mCurrentDate);
		tLPGrpEdorItemSchema.setEdorValiDate(mEdorGRPLQChangeInfo
				.getEdorCValiDate());
		tLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
		tLPGrpEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
		tLPGrpEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		tLPGrpEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
		tLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		tLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
		//mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);

	//	System.out.println(mLPGrpEdorItemSet.size());
		
		MMap  tmap = new MMap();
		tmap.put(tLPGrpEdorMainSchema, "INSERT");
		tmap.put(tLPGrpEdorItemSchema, "INSERT");
	    if (!submit(tmap))
	   {
	       return false;
	   }
	
//		GrpEdorItemUI tGrpEdorItemUI = new GrpEdorItemUI();
//		if (tGrpEdorItemUI.submitData(tVData, "INSERT||GRPEDORITEM")) {
//			tVData.clear();
//			tVData = tGrpEdorItemUI.getResult();
//			mMMap_AddItem.put("VData_AddItem", tVData);
//		} else {
//			// String sqlError = "";
//			// // "select errorinfo from lcgrpimportlog where grpcontno = '" +
//			// mEdorGRPLQChangeInfo.getGrpContNo() + "' and batchno = '" +
//			// mBatchNo + "' and errorstate = '1' and makedate = '" +
//			// mCurrentDate + "' and maketime >= '" + mCurrentTime +
//			// "' with ur";
//			// ExeSQL tExeSQL = new ExeSQL();
//			// SSRS tSSRS = new SSRS();
//			// tSSRS = tExeSQL.execSQL(sqlError);
//			// String errorInfo = "一卡通团险部分领取保全项目出错！！！";
//			// if(null != tSSRS && tSSRS.getMaxRow() >= 1) {
//			// errorInfo = "importlog : " + tSSRS.GetText(tSSRS.getMaxRow(), 1);
//			// }
//
//			mErrors.copyAllErrors(tGrpEdorItemUI.mErrors);
//			return false;
//		}

		return true;

		//	
		// if (!tPEdorAppItemUI.submitData(tVData,"INSERT||EDORITEM"))
		// {
		// mErrors.addOneError("生成保全工单失败" +
		// tPEdorAppItemUI.mErrors.getFirstError());
		// return false;
		// }
		// return true;
	}

	/**
	 * 保存明保全细
	 * 
	 * @return boolean
	 */
	private boolean saveDetail() {
		//
		
		String edorSQL = " select grppolno from lcpol where grpcontno='"
			+ mEdorGRPLQChangeInfo.getGrpContNo()
			+ "' and contno='"+ mEdorGRPLQChangeInfo.getContNo()+"' " ;
				ExeSQL tExeSQL = new ExeSQL();
	    String tgrpPolNo = tExeSQL.getOneValue(edorSQL);
			 
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(
				mEdorAcceptNo, mEdorType);
		
		tSpecialData.setGrpPolNo(tgrpPolNo);
		tSpecialData.add("GrpMoney", mEdorGRPLQChangeInfo.getGetMoney());
		tSpecialData.add("GrpFixMoney", "0"); 


		GrpEdorLQDetailBL tGrpEdorLQDetailBL = new GrpEdorLQDetailBL(
				mGlobalInput, mEdorAcceptNo, mEdorGRPLQChangeInfo
						.getGrpContNo(), tSpecialData);
		if (!tGrpEdorLQDetailBL.submitData()) {
			mErrors.copyAllErrors(tGrpEdorLQDetailBL.mErrors);
			return false;
		}
		return true;
	}

	
	private boolean endEdor() {
		String mPayMode = mEdorGRPLQChangeInfo.getPaymode();
		String mAccNo = mEdorGRPLQChangeInfo.getAccNo();
		String mAccName = mEdorGRPLQChangeInfo.getAccName();
		String mBankCode = mEdorGRPLQChangeInfo.getBankCode();
		String mBankAccNo = mEdorGRPLQChangeInfo.getAccNo();
		String mPayDate = mCurrentDate;
		//若选择的付费方式为银行转账
//		if("4".equals(mLCContSchema.getPayMode())){
//			mAccNo=mLCContSchema.getBankAccNo();
//			mBankCode=mLCContSchema.getBankCode();
//			mBankAccNo=mLCContSchema.getBankAccNo();
//			if(null==mAccNo || null==mBankCode || null==mBankAccNo){
//				System.out.println("坑。。保单账户信息有空。。。");
//			}
//		}
		
		System.out.println("交退费通知书" + mEdorAcceptNo);
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("payMode", mPayMode);
		tTransferData.setNameAndValue("endDate", "");
		tTransferData.setNameAndValue("payDate", mPayDate);
		tTransferData.setNameAndValue("bank", mBankCode);
		tTransferData.setNameAndValue("bankAccno", mBankAccNo);
		tTransferData.setNameAndValue("accName", mAccName);
		tTransferData.setNameAndValue("chkYesNo", "no");

		// 生成交退费通知书
		FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI(mEdorAcceptNo);
		if (!tFeeNoticeVtsUI.submitData(tTransferData)) {
			mErrors.addOneError("生成批单失败！原因是：" + tFeeNoticeVtsUI.getError());
		}

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tTransferData);
		SetPayInfo spi = new SetPayInfo(mEdorAcceptNo);
		if (!spi.submitDate(data, "0")) {
			System.out.println("设置收退费方式失败！");
			mErrors.addOneError("设置收退费方式失败！原因是：" + spi.mErrors.getFirstError());
		}

		return true;
	}
	/**
	 * 保全理算，更新核保标志等同于保全理算
	 * 
	 * @return boolean
	 */
	private boolean appConfirm() {

//		LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
//		MMap map = new MMap();
//		tLPDiskImportSchema.setEdorNo(mEdorAcceptNo);
//		tLPDiskImportSchema.setEdorType(mEdorType);
//		tLPDiskImportSchema.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
//		tLPDiskImportSchema.setSerialNo("1");
//		tLPDiskImportSchema.setState("1");
//		tLPDiskImportSchema.setInsuredNo(mEdorGRPLQChangeInfo.getInsuredNo());
//	//	tLPDiskImportSchema.setGetMoney(mEdorGRPLQChangeInfo.getGetMoney());
//	//	tLPDiskImportSchema.setMoney(mEdorGRPLQChangeInfo.getGetMoney());
//		tLPDiskImportSchema.setBankAccNo(mEdorGRPLQChangeInfo.getAccNo());
//		tLPDiskImportSchema.setBankCode(mEdorGRPLQChangeInfo.getBankCode());
//		tLPDiskImportSchema.setAccName(mEdorGRPLQChangeInfo.getAccName());
//		// tLPDiskImportSchema.setInsuredName("");
//		// tLPDiskImportSchema.setContPlanCode("");
//		// tLPDiskImportSchema.setRetire("");
//		// tLPDiskImportSchema.setEmployeeName("");
//		// tLPDiskImportSchema.setRelation("");
//		// tLPDiskImportSchema.setSex("");
//		// tLPDiskImportSchema.setBirthday("");
//		// tLPDiskImportSchema.setIDType("");
//		// tLPDiskImportSchema.setIDNo("");
//		// tLPDiskImportSchema.setOccupationType("");
//		// tLPDiskImportSchema.setImportFileName("");
//		tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
//		tLPDiskImportSchema.setMakeDate(mCurrentDate);
//		tLPDiskImportSchema.setMakeTime(mCurrentTime);
//		tLPDiskImportSchema.setModifyDate(mCurrentDate);
//		tLPDiskImportSchema.setModifyTime(mCurrentTime);
//		map.put(tLPDiskImportSchema, "INSERT");
//		submit(map);

		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
		tLPGrpEdorMainSchema.setEdorValiDate(mEdorGRPLQChangeInfo
				.getEdorCValiDate());
		tLPGrpEdorMainSchema.setEdorAppDate(mEdorGRPLQChangeInfo
				.getEdorCValiDate());

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLPGrpEdorMainSchema);
		PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
		if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM")) {
			mErrors.copyAllErrors(tPGrpEdorAppConfirmUI.getError());
			return false;
		}
		// MMap map = new MMap();
		// String sql;
		// sql = "update LPEdorApp set EdorState = '2', UWState = '9' " +
		// "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
		// map.put(sql, "UPDATE");
		// sql = "update LPEdorMain set EdorState = '2', UWState = '9' " +
		// "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
		// map.put(sql, "UPDATE");
		// sql = "update LPEdorItem set EdorState = '2' " +
		// "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
		// map.put(sql, "UPDATE");
		// if (!submit(map))
		// {
		// return false;
		// }
		return true;
	}

	/**
	 * 产生打印数据
	 * 
	 * @return boolean
	 */
	private boolean creatPrintVts() {
		// 生成打印数据
		VData data = new VData();
		data.add(mGlobalInput);
		PrtGrpEndorsementBL tPrtAppEndorsementBL = new PrtGrpEndorsementBL(
				mEdorAcceptNo);
		if (!tPrtAppEndorsementBL.submitData(data, "")) {
			mErrors.addOneError("数据保存成功！但没有生成保全服务批单！");
			return false;
		}
		return true;
	}

	private boolean checkEdorItem() {
		String mGrpContNo = mEdorGRPLQChangeInfo.getGrpContNo();
		// 正在操作保全的无法添加保全
		String edorSQL = " select edoracceptno from lpgrpedoritem where grpcontno='"
				+ mGrpContNo
				+ "' and edoracceptno!='"
				+ mEdorAcceptNo
				+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0') with ur  ";
		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			mErrors.addOneError("保单" + mGrpContNo + "正在操作保全，工单号为：" + edorFlag
					+ "无法添加保全！");

			return false;
		}
		
		// 正在操作理赔的无法添加保全
		String edorSQL2 = "select contno from lccont where grpcontno = '"+mGrpContNo+"'";
		ExeSQL tExeSQL2 = new ExeSQL();
		SSRS tSSRS2 = tExeSQL2.execSQL(edorSQL2);
	  	  if(tSSRS2.getMaxRow()>0){
	  		  for(int i = 1;i<tSSRS2.getMaxRow();i++){
	  			  String tContNo = tSSRS2.GetText(i, 1);
	  			  if(tContNo!=null && !tContNo.equals("")){
	  				  //校验保单下被保人如果正在理赔不能续期抽档
	  				  String sql ="SELECT a.CusTomerName FROM LLCase a,LCPol b "
	  					  +"WHERE a.CusTomerNo = b.InsuredNo "
	  					  +"AND a.RGTState NOT IN ('11','12','14') AND b.contno='" + tContNo + "' ";
	  				  System.out.println(sql);
	  				  
	  				  SSRS tSSRS = tExeSQL2.execSQL(sql);
	  				  if(tSSRS.getMaxRow()>0){  
	  					  this.mErrors.addOneError("被保人"+tSSRS.GetText(1, 1)+"有未结案的理赔。");
	  					  return false;
	  				  }
	  			  }
	  		  }
	  	  }

		return true;
	}

	/**
	 * 保全确认
	 * 
	 * @param edorAcceptNo
	 *            String
	 * @return boolean
	 */
	private boolean edorConfirm() {
		// MMap map = new MMap();
		// VData tdata = new VData();
		// tdata.add(mGlobalInput);
		// PGrpEdorConfirmBL tPEdorConfirmBL =
		// new PGrpEdorConfirmBL();
		// MMap edorMap = tPEdorConfirmBL.getSubmitData(tdata,
		// "INSERT||GRPEDORCONFIRM");
		// if (edorMap == null)
		// {
		// mErrors.copyAllErrors(tPEdorConfirmBL.mErrors);
		// return false;
		// }
		// map.add(edorMap);
		// //工单结案
		// LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		// tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
		// tLGWorkSchema.setTypeNo("03"); //结案状态
		//
		// VData data = new VData();
		// data.add(mGlobalInput);
		// data.add(tLGWorkSchema);
		// TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		// MMap taskMap = tTaskAutoFinishBL.getSubmitData(data, "");
		// if (taskMap == null)
		// {
		// mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
		// return false;
		// }
		// map.add(taskMap);
		// if (!submit(map))
		// {
		// return false;
		// }
		// return true;

		String flag = "";
		String edorAcceptNo = mEdorAcceptNo;
		String fmtransact = "0";
		String payMode = "1";
		String balanceMethodValue = "1";
		String customerNo = mCustomerno;
		String accType = "1";
		String destSource = "11";
		String contType = null;
		String fmtransact2 = "NOTUSEACC";
		GlobalInput gi = mGlobalInput;

		String failType = null;

		String checkZT = "select sum(getmoney) from LPGrpEdorItem " + "where EdorNo = '"
				+ edorAcceptNo + "' " + "and EdorType = 'LQ' ";
		SSRS mssrs1 = new ExeSQL().execSQL(checkZT);
		if (mssrs1 != null && mssrs1.getMaxRow() > 0) {
//			String sql3 = "select sum(getmoney) from lpedoritem "
//					+ "where EdorNo = '" + edorAcceptNo + "' "
//					+ "and EdorType = 'LQ' ";
			SSRS tSSRS3 = new ExeSQL().execSQL(checkZT);
			String itemmoneyLQ = tSSRS3.GetText(1, 1);
			String sql4 = "select case when sum(getmoney) is null then 0 else sum(getmoney) end from ljsgetendorse "
					+ " where endorsementno = '"
					+ edorAcceptNo
					+ "' "
					+ " and feeoperationtype='LQ'  with ur";
			SSRS tSSRS4 = new ExeSQL().execSQL(sql4);
			String endorsemoneyLQ = tSSRS4.GetText(1, 1);

			if (!itemmoneyLQ.equals(endorsemoneyLQ)) {
				flag = "Fail";
			}
		}

		if (!flag.equals("Fail")) {
			EdorItemSpecialData tSpecialData = new EdorItemSpecialData(
					edorAcceptNo, "YE");
			tSpecialData.add("CustomerNo", customerNo);
			tSpecialData.add("AccType", accType);
			tSpecialData.add("OtherType", "3");
			tSpecialData.add("OtherNo", edorAcceptNo);
			tSpecialData.add("DestSource", destSource);
			tSpecialData.add("ContType", contType);
			tSpecialData.add("PayMode", mEdorGRPLQChangeInfo.getPaymode());
			tSpecialData.add("Fmtransact2", fmtransact2);
			tSpecialData.add("Bank", mEdorGRPLQChangeInfo.getBankCode());
			tSpecialData.add("BankAccNo", mEdorGRPLQChangeInfo.getAccNo());
			tSpecialData.add("AccName", mEdorGRPLQChangeInfo.getAccName());
			
			LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
			tLCAppAccTraceSchema.setCustomerNo(customerNo);
			tLCAppAccTraceSchema.setAccType(accType);
			tLCAppAccTraceSchema.setOtherType("3");// 团单
			tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
			tLCAppAccTraceSchema.setDestSource(destSource);
			tLCAppAccTraceSchema.setOperator(gi.Operator);

			VData tVData = new VData();
			tVData.add(tLCAppAccTraceSchema);
			tVData.add(tSpecialData);
			tVData.add(gi);
			PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
			if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param")) {
				flag = "Fail";
				System.out.println("处理帐户余额失败！");
			} else {
				
				// 生成财务数据
				FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput,
						mEdorAcceptNo, BQ.NOTICETYPE_G, "");
				MMap tmap = new MMap();
				if (!tFinanceDataBL.submitData()) {
					mErrors.addOneError("生成财务数据错误" + tFinanceDataBL.mErrors);
					return false;
				}

//				BqConfirmUI tBqConfirmUI = new BqConfirmUI(gi, edorAcceptNo,
//						BQ.CONTTYPE_G, balanceMethodValue);
//				if (!tBqConfirmUI.submitData()) {
//					flag = "Fail";
//					System.out.println(tBqConfirmUI.getError());
//					mErrors.addOneError(tBqConfirmUI.getError());
				String strTemplatePath = "";
				LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
				tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
				tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
				tLPGrpEdorMainSchema.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
				VData tdata = new VData();
				tdata.add(strTemplatePath);
				tdata.add(tLPGrpEdorMainSchema);
				tdata.add(mGlobalInput);
				
				PGrpEdorConfirmBL tPEdorConfirmBL = new PGrpEdorConfirmBL();
				tmap = tPEdorConfirmBL.getSubmitData(tdata,
						"INSERT||GRPEDORCONFIRM");
				if (tmap == null) {
					mErrors.addOneError("保全结案发生错误" + tFinanceDataBL.mErrors);
					return false;
				}

				// 工单结案
				LGWorkSchema tLGWorkSchema = new LGWorkSchema();
				tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
				tLGWorkSchema.setTypeNo("03"); // 结案状态

				VData data = new VData();
				data.add(mGlobalInput);
				data.add(tLGWorkSchema);
				TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
				MMap map = tTaskAutoFinishBL.getSubmitData(data, "");
				if (map == null) {
					mErrors.addOneError("工单结案失败" + tFinanceDataBL.mErrors);
					return false;
				}
				tmap.add(map);
				if (!submit(tmap)) {
					return false;
				}
				
				
//				} 
				{
					System.out.println("交退费通知书" + edorAcceptNo);
					TransferData tTransferData = new TransferData();
					tTransferData.setNameAndValue("payMode", mEdorGRPLQChangeInfo.getPaymode());
					tTransferData.setNameAndValue("endDate", mCurrentDate);
					tTransferData.setNameAndValue("payDate", mCurrentDate);
					tTransferData.setNameAndValue("bank", mEdorGRPLQChangeInfo
							.getBankCode());
					tTransferData.setNameAndValue("bankAccno",
							mEdorGRPLQChangeInfo.getAccNo());
					tTransferData.setNameAndValue("accName",
							mEdorGRPLQChangeInfo.getAccName());

					// 生成交退费通知书
					FeeNoticeGrpVtsUI tFeeNoticeGrpVtsUI = new FeeNoticeGrpVtsUI(
							edorAcceptNo);
					if (!tFeeNoticeGrpVtsUI.submitData(tTransferData)) {
						flag = "Fail";
						System.out.println("生成批单失败！原因是："
								+ tFeeNoticeGrpVtsUI.getError());
						mErrors.addOneError(tFeeNoticeGrpVtsUI.getError());
					}

					VData data2 = new VData();
					data2.add(mGlobalInput);
					data2.add(tTransferData);
					SetPayInfo spi = new SetPayInfo(edorAcceptNo);
					if (!spi.submitDate(data2, "0")) {
						System.out.println("设置转帐信息失败！");
						flag = "Fail";
						System.out.println("设置收退费方式失败！原因是："
								+ spi.mErrors.getFirstError());
						mErrors.addOneError(spi.mErrors.getFirstError());

					}
					flag = "Succ";
					//生成ljfiget表
					
					String edorSQL = " select * from ljaget where otherno='"
							+ mEdorAcceptNo
							+ "' with ur  ";
					LJAGetDB mLJAGetDB = new LJAGetDB();
					LJAGetSet mLJAGetSet = mLJAGetDB.executeQuery(edorSQL);
					if(mLJAGetSet != null){
						mLJAGetSchema.setSchema(mLJAGetSet.get(1));
					}else{
						mErrors.addOneError("ljaget查不到工单"+ mEdorAcceptNo + tFinanceDataBL.mErrors);
						return false;
					}
					mLJFIGetSchema.setActuGetNo(mLJAGetSchema.getActuGetNo());
					mLJFIGetSchema.setPayMode(mEdorGRPLQChangeInfo.getPaymode());
					mLJFIGetSchema.setOtherNoType(mLJAGetSchema.getOtherNoType());
					mLJFIGetSchema.setOtherNo(mLJAGetSchema.getOtherNo());
					mLJFIGetSchema.setAgentCode(mLJAGetSchema.getAgentCode());
					mLJFIGetSchema.setManageCom(mLJAGetSchema.getManageCom());
					mLJFIGetSchema.setPolicyCom(mLJAGetSchema.getManageCom());
					mLJFIGetSchema.setBankAccNo(mEdorGRPLQChangeInfo.getAccNo());
					mLJFIGetSchema.setGetMoney(mLJAGetSchema.getSumGetMoney());
					mLJFIGetSchema.setEnterAccDate(PubFun.getCurrentDate());
					mLJFIGetSchema.setAccName(mEdorGRPLQChangeInfo.getAccName());
					mLJFIGetSchema.setConfDate(PubFun.getCurrentDate());
					mLJFIGetSchema.setConfMakeTime(PubFun.getCurrentTime());
					mLJFIGetSchema.setShouldDate(PubFun.getCurrentDate());
					mLJFIGetSchema.setOperator(mGlobalInput.Operator);
					mLJFIGetSchema.setMakeDate(PubFun.getCurrentDate());
					mLJFIGetSchema.setMakeTime(PubFun.getCurrentTime());
					mLJFIGetSchema.setModifyDate(PubFun.getCurrentDate());
					mLJFIGetSchema.setModifyTime(PubFun.getCurrentTime());
					mLJFIGetSchema.setBankCode(mLJAGetSchema.getBankCode());
					mLJFIGetSchema.setConfMakeDate(PubFun.getCurrentDate());
					String tSerialNo = PubFun1.CreateMaxNo("ReturnFromBank", 20);
					mLJFIGetSchema.setSerialNo(tSerialNo);
					mLJFIGetSchema.setSaleChnl(mLJAGetSchema.getSaleChnl());
					tmap.put(mLJFIGetSchema, "INSERT");
					
					mLJAGetSchema.setConfDate(PubFun.getCurrentDate());
					mLJAGetSchema.setEnterAccDate(PubFun.getCurrentDate());
					tmap.put(mLJAGetSchema, "UPDATE");
				    if (!submit(tmap))
				   {
				       return false;
				   }
					
					
					System.out.println("保全确认成功！");
//					String message = tBqConfirmUI.getMessage();
//					if ((message != null) && (!message.equals(""))) {
//						System.out.println("message : " + message);
//					}
				}
			}
		}
	
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

}
