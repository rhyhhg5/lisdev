package com.sinosoft.task;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *    响应对函件的操作
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskLetterBackUWBL
{
    /**
     * 错误的容器，若数据提交失败，则可从里面得到失败的原因
     * */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mResult = new VData();
    private VData mInputData = new VData();
    private String mOperate;
    private MMap mMap = new MMap();
    private MMap map = new MMap();
    private String mEdorNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    private LGLetterSchema mLGLetterSchema = new LGLetterSchema();
    private LPUWMasterSet mLPUWMasterSet = new LPUWMasterSet();

    public TaskLetterBackUWBL()
    {
    }

    /**
     * 外部调用的接口，提交数据用
     * 参数： VData
     * 返回值：成功，true； 失败，false
     * */
    public boolean submitData(VData inputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData) inputData.clone();
        System.out.println("In TaskLetterOperateBL now");

        if (!getInputData(mInputData))
        {
            return false;
        }

        //暂存需要保留的数据
        String operatorType = ""; //作业历史信息生成方式

        if (!checkData())
        {
            return false;
        }

        String commonSqlPart =
                "operator='" + mGlobalInput.Operator + "', "
                + "modifyDate='" + mCurrentDate + "', "
                + "modifyTime='" + mCurrentTime + "' "
                + "where edorAcceptNo='"
                + mLGLetterSchema.getEdorAcceptNo() + "' "
                + "    and serialNumber='"
                + mLGLetterSchema.getSerialNumber() + "' ";

        //回销
        if (mOperate.equals("FeedBack") || mOperate.equals("ForceFeedBack"))
        {
            String state;
            if (mOperate.equals("FeedBack"))
            {
                operatorType = "4";
                state = "3";
            }
            else
            {
                operatorType = "16";
                state = "4";
            }

            String sql = "update LGLetter "
                    + "set state='" + state + "', "
                    + "realBackDate='" + mCurrentDate + "', "
                    + "FeedBackInfo='"
                    + mLGLetterSchema.getFeedBackInfo() + "', "
                    + commonSqlPart;
            map.put(sql, "UPDATE");

            for (int i = 1; i <= mLPUWMasterSet.size(); i++)
            {
                LPUWMasterSchema tLPUWMasterSchema = mLPUWMasterSet.get(i);
                LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
                tLPUWMasterDB.setEdorNo(tLPUWMasterSchema.getEdorNo());
                tLPUWMasterDB.setProposalNo(tLPUWMasterSchema.getPolNo());
                tLPUWMasterDB.setEdorType(tLPUWMasterSchema.getEdorType());

                if (!tLPUWMasterDB.getInfo())
                {
                    mErrors.addOneError("没有查询到数据。");
                    System.out.println("没有查询到数据: " +
                            tLPUWMasterDB.mErrors.getErrContent());
                    return false;
                }
                System.out.println("\n\n\n\ntLPUWMasterDB=");
                LPUWMasterSchema mLPUWMasterSchema = tLPUWMasterDB.getSchema();
                mLPUWMasterSchema.setCustomerReply(tLPUWMasterSchema.
                        getCustomerReply());
                mLPUWMasterSchema.setOperator(mGlobalInput.Operator);
                mLPUWMasterSchema.setModifyDate(mCurrentDate);
                mLPUWMasterSchema.setModifyTime(mCurrentTime);
                map.put(mLPUWMasterSchema, "UPDATE");
                String passFlag = tLPUWMasterDB.getPassFlag();
                String customerReply = tLPUWMasterSchema.getCustomerReply();
                if ((passFlag != null) &&
                        (passFlag.equals(BQ.PASSFLAG_CONDITION) ||
                        passFlag.equals(BQ.PASSFLAG_CANCEL)) &&
                        (customerReply != null) && (customerReply.equals("2"))) //客户不同意
                {
                    String edorType = tLPUWMasterSchema.getEdorType();
                    String polNo = tLPUWMasterSchema.getPolNo();
                    sql = "update LPPol " +
                            "set EdorType = 'DL' " +
                            "where EdorNo = '" + mEdorNo + "' " +
                            "and EdorType = '" + edorType + "' " +
                            "and PolNo = '" + polNo + "'";
                    map.put(sql, "UPDATE");
                    sql = "update LPDuty " +
                            "set EdorType = 'DL' " +
                            "where EdorNo = '" + mEdorNo + "' " +
                            "and EdorType = '" + edorType + "' " +
                            "and PolNo = '" + polNo + "'";
                    map.put(sql, "UPDATE");
                    sql = "update LPPrem " +
                            "set EdorType = 'DL' " +
                            "where EdorNo = '" + mEdorNo + "' " +
                            "and EdorType = '" + edorType + "' " +
                            "and PolNo = '" + polNo + "'";
                    map.put(sql, "UPDATE");
                    sql = "update LPGet " +
                            "set EdorType = 'DL' " +
                            "where EdorNo = '" + mEdorNo + "' " +
                            "and EdorType = '" + edorType + "' " +
                            "and PolNo = '" + polNo + "'";
                    map.put(sql, "UPDATE");
                    //删除批改补退费表
                    sql = "delete from LJSGetEndorse " +
                            "where EndorseMentNo = '" + mEdorNo + "' " +
                            "and FeeOperationType = '" + edorType + "' " +
                            "and PolNo = '" + polNo + "' ";
                    map.put(sql, "DELETE");
                    
                    //续保核保同时通过原PolNo删除批改补退费表
                    if(edorType.equals("XB"))
                    {
                    	sql = "delete from LJSGetEndorse where EndorseMentNo = '" + mEdorNo + "' " 
                    	    + "and FeeOperationType = '" + edorType + "' " 
                    	    + "and PolNo in (select PolNo from LCRNewStateLog where NewPolNo = '" + polNo + "') ";
                    	map.put(sql, "DELETE");
                    }
                }
            }
            delEdorItem();
            calPrem();
            if (!changeLPEdorAppState("7")) //保全状态为函件回销
            {
                return false;
            }
        }
        else if (mOperate.equals("Delete"))
        {
            operatorType = "0";
            map.put("delete from LGLetter "
                    + "where edorAcceptNo = '"
                    + mLGLetterSchema.getEdorAcceptNo() + "' "
                    + "    and serialNumber = '"
                    + mLGLetterSchema.getSerialNumber() + "' ", "DELETE");
        }

        if (!operatorType.equals("0"))
        {
            LGTraceNodeOpSchema schema = getTraceNodeOp(operatorType);
            if (schema == null)
            {
                return false;
            }
            else
            {
                map.put(schema, "INSERT");
            }
        }

        mInputData.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
        {
            mErrors.addOneError("提交数据失败");

            return false;
        }

        //重复理算保全受理
        if (mOperate.equals("FeedBack") || mOperate.equals("ForceFeedBack"))
        {
            if (!reCalculate())
            {
                return false;
            }
        }

        if (!reNewAppState())
        {
            return false;
        }

        return true;
    }

    private void delEdorItem()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mEdorNo);
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++)
        {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i);
            String edorType = tLPEdorItemSchema.getEdorType();
            String insuredNo = tLPEdorItemSchema.getInsuredNo();
            String sql;
            if ((edorType != null) && (edorType.equals(BQ.EDORTYPE_CM))) //这里先写死，以后改掉
            {
                sql = "select * from LPUWMaster " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and InsuredNo = '" + insuredNo + "' ";
            }
            else
            {
                sql = "select * from LPUWMaster " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' ";
            }
            LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
            LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.executeQuery(sql);
            if (tLPUWMasterSet.size() > 0)
            {
                boolean stopAll = true;
                for (int j = 1; j <= tLPUWMasterSet.size(); j++)
                {
                    LPUWMasterSchema tLPUWMasterSchema = tLPUWMasterSet.get(
                            j);
                    String passflag = tLPUWMasterSchema.getPassFlag();
                    if ((passflag != null) &&
                            (!passflag.equals(BQ.PASSFLAG_STOP)))
                    {
                        stopAll = false;
                    }
                }
                if (stopAll == true)
                {
                    map.put(tLPEdorItemSchema, "DELETE");
                    LOBEdorItemSchema tLOBEdorItemSchema = new
                            LOBEdorItemSchema();
                    Reflections ref = new Reflections();
                    ref.transFields(tLOBEdorItemSchema, tLPEdorItemSchema);
                    tLOBEdorItemSchema.setReason("核保终止");
                    map.put(tLOBEdorItemSchema, "DELETE&INSERT");
                }
            }
        }
    }

    /**
     * 根据加费计算期交保费
     * @param tLPUWMasterSchema LPUWMasterSchema
     * @return boolean
     */
    private boolean calPrem()
    {
        String sql = "update LPDuty a " +
                "set (Prem, SumPrem) = (select sum(Prem), sum(SumPrem) from LPPrem " +
                "    where EdorNo = '" + mEdorNo + "' " +
                "    and EdorType = a.EdorType " +
                "    and ContNo = a.ContNo " +
                "    and PolNo = a.PolNo " +
                "    and DutyCode = a.DutyCode) " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);
            String polNo = tLPPolSchema.getPolNo();
            String edorType = tLPPolSchema.getEdorType();
            LPPremDB tLPPremDB = new LPPremDB();
            tLPPremDB.setEdorNo(mEdorNo);
            tLPPremDB.setEdorType(edorType);
            tLPPremDB.setPolNo(polNo);
            if (tLPPremDB.query().size() > 0)
            {
                sql = "update LPPol a " +
                        "set (Prem, SumPrem) = (select sum(Prem), sum(SumPrem) from LPPrem " +
                        "    where EdorNo = '" + mEdorNo + "' " +
                        "    and EdorType = a.EdorType " +
                        "    and ContNo = a.ContNo " +
                        "    and PolNo = a.PolNo) " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and PolNo = '" + polNo + "'";
                mMap.put(sql, "UPDATE");
            }
        }
        //计算LPCont的保费要同时计算LPol和LCPol，LPol没有才取LCPol的Prem
        sql = "update LPCont a " +
                "set Prem = (select nvl((select sum(Prem) from LCPol b " +
                "     where ContNo = a.ContNo and not exists " +
                "           (select * from LPPol where EdorNo = '" + mEdorNo +
                "' " +
                "            and EdorType = a.EdorType and PolNo = b.PolNo)),0) + " +
                "    nvl((select sum(Prem) from LPPol where EdorNo = '" + mEdorNo +
                "' " +
                "     and EdorType = a.EdorType),0) from dual), " +
                "    SumPrem = (select nvl((select sum(SumPrem) from LCPol c " +
                "     where ContNo = a.ContNo and not exists " +
                "           (select * from LPPol where EdorNo = '" + mEdorNo +
                "' " +
                "            and EdorType = a.EdorType and PolNo = c.PolNo)),0) + " +
                "    nvl((select sum(SumPrem) from LPPol where EdorNo = '" +
                mEdorNo + "' " +
                "     and EdorType = a.EdorType),0) from dual) " +
                "where EdorNo = '" + mEdorNo + "' ";
        System.out.println(sql);
        mMap.put(sql, "UPDATE");

        return true;
    }

    /**
     * 更新Item表的保费，分为客户和保单两种情况
     */
    private boolean updateItemMoney()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mEdorNo);
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++)
        {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i);
            String edorType = tLPEdorItemSchema.getEdorType();
            String contNo = tLPEdorItemSchema.getContNo();
            String insuredNo = tLPEdorItemSchema.getInsuredNo();
            String sql;
            if (insuredNo.equals(BQ.FILLDATA))
            {
                sql = "update LPEdorItem a " +
                        "set GetMoney = (select case when sum(GetMoney) is null  " +
                       "     then 0 else sum(GetMoney) end " +
                        "    from LJSGetEndorse " +
                        "    where EndorseMentNo = '" + mEdorNo + "' " +
                        "    and FeeOperationType = '" + edorType + "' " +
                        "    and ContNo = a.ContNo) " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and ContNo = '" + contNo + "' ";
            }
            else
            {
                sql = "update LPEdorItem a " +
                        "set GetMoney = (select case when sum(GetMoney) is null " +
                        "    then 0 else sum(GetMoney) end from LJSGetEndorse " +
                        "    where EndorseMentNo = '" + mEdorNo + "' " +
                        "    and FeeOperationType = '" + edorType + "' " +
                        "    and InsuredNo = '" + insuredNo + "' " +
                        "    and ContNo = a.ContNo) " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and InsuredNo = '" + insuredNo + "' " +
                        "and ContNo = '" + contNo + "' ";
            }
            System.out.println(sql);
            mMap.put(sql, "UPDATE");
        }
        return true;
    }

    /**
     * 统计Item表中的保费变化,同步更新Main,表其中Main表按ContNo和Item表对应
     */
    private void updateMainMoney()
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mEdorNo);
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
        for (int i = 1; i <= tLPEdorMainSet.size(); i++)
        {
            String contNo = tLPEdorMainSet.get(i).getContNo();
            StringBuffer sql = new StringBuffer("update LPEdorMain ");
            sql.append("set (ChgPrem, ChgAmnt, GetMoney, GetInterest) = ")
                    .append(
                    "(select sum(ChgPrem), sum(ChgAmnt), sum(GetMoney), ")
                    .append("sum(GetInterest) from LPEdorItem ")
                    .append("where EdorAcceptNo = '").append(mEdorNo).
                    append("' ")
                    .append("and ContNo = '").append(contNo).append("') ")
                    .append("where EdorAcceptNo = '").append(mEdorNo).
                    append("' ")
                    .append("and ContNo = '").append(contNo).append("'");
            mMap.put(sql.toString(), "UPDATE");
        }
    }

    /**
     * 统计Main表中的保费变化,同步更新App表
     */
    private void updateAppMoney()
    {
        String sql = "update LPEdorApp " +
                "set (ChgPrem, ChgAmnt, GetMoney, GetInterest) = " +
                "    (select sum(ChgPrem), sum(ChgAmnt), sum(GetMoney), " +
                "    sum(GetInterest) from LPEdorMain " +
                "    where EdorAcceptNo = '" + mEdorNo + "') " +
                "where EdorAcceptNo = '" + mEdorNo + "' ";
        System.out.println(sql);
        mMap.put(sql, "UPDATE");
    }

    /**
     * 重新生成批单
     * @return boolean
     */
    private boolean rebuildVts()
    {
        updateItemMoney();
        updateMainMoney();
        updateAppMoney();
        VData tVData = new VData();
        tVData.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, "INSERT||MAIN"))
        {
            mErrors.addOneError("提交数据失败");

            return false;
        }

        //生成打印数据
        //生成打印数据
        VData data = new VData();
        data.add(mGlobalInput);
        PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
                mEdorNo);
        if (!tPrtAppEndorsementBL.submitData(data, ""))
        {
            mErrors.addOneError("重新生成保全批单错误！" +
                    tPrtAppEndorsementBL.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private String getLPEdorAppState()
    {
        /*
                 LPEdorEspecialDataDB db = new LPEdorEspecialDataDB();
                 db.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
                 db.setEdorNo(mLGLetterSchema.getSerialNumber());
                 db.setEdorType("BQ");
                 db.setDetailType(BQ.DETAILTYPE_EDORSTATE);
                 db.getInfo();
         */
        EdorItemSpecialData tEdorItemSpecialData =
                new EdorItemSpecialData(mLGLetterSchema.getEdorAcceptNo(), "BQ");
        if (!tEdorItemSpecialData.query())
        {
            return null;
        }

        LPEdorEspecialDataSet set = tEdorItemSpecialData.getSpecialDataSet();
        for (int i = 1; i <= set.size(); i++)
        {
            if (set.get(i).getDetailType().equals(BQ.DETAILTYPE_EDORSTATE))
            {
                return set.get(i).getEdorValue();
            }
        }
        return null;

        //return db.getEdorValue();
    }

    /**
     * 函件回销和强制回销后需要重复理算
     * @return boolean
     */
    private boolean reCalculate()
    {
        String operate;
        LGWorkDB db = new LGWorkDB();
        db.setWorkNo(mLGLetterSchema.getEdorAcceptNo());
        db.getInfo();
        if (db.getCustomerNo().length() == 8) //团单
        {
            GEdorReCalBL tGEdorReCalBL =
                    new GEdorReCalBL(mLGLetterSchema.getEdorAcceptNo());
            if (!tGEdorReCalBL.submitData())
            {
                mErrors.copyAllErrors(tGEdorReCalBL.mErrors);
                return false;
            }
        }
        else //个单
        {
//            PEdorReCalBL tPEdorReCalBL =
//                    new PEdorReCalBL(mLGLetterSchema.getEdorAcceptNo());
//            if (!tPEdorReCalBL.submitData())
//            {
//                mErrors.copyAllErrors(tPEdorReCalBL.mErrors);
//                return false;
//            }
        }

        return true;
    }

    /**
     * 还原保全状态        核保状态？？
     * @return boolean
     */
    private boolean reNewAppState()
    {
        MMap tMMap = new MMap();

        //还原保全状态
        if (mOperate.equals("SendOut"))
        {
            //若下发的函件不需要回销
            if (mLGLetterSchema.getBackFlag().equals("0"))
            {
                //还原保全受理状态
                tMMap.put("update LPEdorApp set edorState = '"
                        + getLPEdorAppState() + "' "
                        + "where edorAcceptNo = '"
                        + mLGLetterSchema.getEdorAcceptNo() + "' ", "UPDATE");
            }
        }
        else if (mOperate.equals("Delete"))
        {
            //还原保全受理状态
            tMMap.put("update LPEdorApp set edorState = '"
                    + getLPEdorAppState() + "' "
                    + "where edorAcceptNo = '"
                    + mLGLetterSchema.getEdorAcceptNo() + "' ", "UPDATE");
        }
        VData v = new VData();
        v.add(tMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(v, "UPDATE||MAIN"))
        {
            mErrors.addOneError("提交数据失败");

            return false;
        }

        //删除保全状态记录
        if (!deleteEspecialData())
        {
            return false;
        }
        rebuildVts(); //重新生成批单
        return true;
    }

    /**
     * 删除保全状态记录
     * @return boolean
     */
    private boolean deleteEspecialData()
    {
        MMap tMMap = new MMap();

        tMMap.put("delete from LPEdorEspecialData where edorAcceptNo = '"
                + mLGLetterSchema.getEdorAcceptNo() + "' and edorNo = '"
                + mLGLetterSchema.getSerialNumber() + "' and detailType = '"
                + BQ.DETAILTYPE_EDORSTATE + "' ", "DELETE");

        VData v = new VData();
        v.add(tMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(v, "DELETE||MAIN"))
        {
            mErrors.addOneError("提交数据失败");

            return false;
        }

        return true;
    }

    //将前台传入的数据分解到当前类变量
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.
                    getObjectByObjectName("GlobalInput", 0);
            mLGLetterSchema = (LGLetterSchema) data.
                    getObjectByObjectName("LGLetterSchema", 0);
            mLPUWMasterSet = (LPUWMasterSet) data.
                    getObjectByObjectName("LPUWMasterSet", 0);
            mEdorNo = mLGLetterSchema.getEdorAcceptNo();
        }
        catch (Exception e)
        {
            mErrors.addOneError("传入后台的数据不完整");

            return false;
        }

        return true;
    }

    //检查传入数据的合法性
    private boolean checkData()
    {
        LGLetterSchema schema = new LGLetterSchema();

        LGLetterDB db = new LGLetterDB();
        db.setSchema(mLGLetterSchema);
        db.getInfo();
        if (db == null)
        {
            mErrors.addOneError("没有查到所选择的函件信息");

            return false;
        }
        else
        {
            schema = db.getSchema();
        }

        if (mOperate.equals("SendOut"))
        {
            boolean flag = true;
            String errMsg = "";

            //若没有录入回销与否，则查表判断函件是否是非核保函件，
            //非核保函件在新建时可能录入了回销与否与回销时间
            LGLetterDB tLGLetterDB = new LGLetterDB();

            if (mLGLetterSchema.getBackFlag() == null
                    || mLGLetterSchema.getBackFlag().equals(""))
            {
                tLGLetterDB.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
                tLGLetterDB.setSerialNumber(mLGLetterSchema.getSerialNumber());
                if (tLGLetterDB.getInfo())
                {
                    //若是非核保函件，则准备校验所需的数据
                    if (tLGLetterDB.getLetterType().equals("1"))
                    {
                        mLGLetterSchema.setBackFlag(tLGLetterDB.getBackFlag());
                    }
                }
            }

            if (!schema.getState().equals("0"))
            {
                flag = false;
                errMsg += "函件状态不是待下发； ";
            }
            else if (mLGLetterSchema.getBackFlag() == null
                    || mLGLetterSchema.getBackFlag().equals(""))
            {
                flag = false;
                errMsg += "是否回销不能为空； ";
            }
            else if (mLGLetterSchema.getBackFlag().equals("1")
                    && (mLGLetterSchema.getBackDate() == null
                    || mLGLetterSchema.getBackDate().equals("")))
            {
                //若没有录入回销时间,则查表判断函件是否是非核保函件，
                //非核保函件在新建时可能录入了回销时间
                if (tLGLetterDB.getBackDate() == null
                        || tLGLetterDB.getBackDate().equals(""))
                {
                    //为录入回销时间
                    flag = false;
                    errMsg += "函件需回销，但您没有录入回销时间； ";
                }
                else
                {
                    //录入了回销时间
                    // mLGLetterSchema.setBackDate(tLGLetterDB.getBackDate());
                }
            }
            else if (mLGLetterSchema.getBackFlag().equals("1")
                    && mLGLetterSchema.getBackDate() != null
                    && !mLGLetterSchema.getBackDate().equals(""))
            {
                String sql = "select userCode "
                        + "from LDUser "
                        + "where days('" + mCurrentDate + "') > days('"
                        + mLGLetterSchema.getBackDate() + "') ";
                ExeSQL e = new ExeSQL();
                String exist = e.getOneValue(sql);
                if (exist != null && !exist.equals(""))
                {
                    flag = false;
                    errMsg += "回销日期不能早于今天";
                }
            }

            if (!flag)
            {
                mErrors.addOneError("您不能下发函件，因为发生了如下错误：" + errMsg);
                return false;
            }
        }
        //回销
        if (mOperate.equals("FeedBack"))
        {
            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setSchema(mLGLetterSchema);
            db.getInfo();

            if (db.getState() != null && db.getState().equals("0"))
            {
                mErrors.addOneError("对不起，函件状态是待下发，您不能进行回销操作");
                return false;
            }
            if (db.getState() != null
                    && db.getState().equals("6"))
            {
                mErrors.addOneError("函件已超时，请进行强制回销操作");
                return false;
            }
            
            //少儿险续保核保，客户反馈要吗两个险种都同意，要吗两个险种都不同意。
            String tEdorType = null;
            for(int i=1;i<=mLPUWMasterSet.size();i++)
            {
            	tEdorType = mLPUWMasterSet.get(i).getEdorType();
            }
            if(tEdorType.equals(BQ.EDORTYPE_XB))
            {
            	int no = 0;
            	for(int i=1;i<=mLPUWMasterSet.size();i++)
                {
                	if(mLPUWMasterSet.get(i).getCustomerReply().equals("2"))
                		no++;
                }
            	if(no!=0 && mLPUWMasterSet.size()!=no)
            	{
            		mErrors.addOneError("少儿险只能两个险种同时续保或同时终止。");
                    return false;
            	}
            		
            }
        }
        //强制回销
        if (mOperate.equals("ForceFeedBack"))
        {
            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setSchema(mLGLetterSchema);
            db.getInfo();
            if (db.getState() != null && db.getState().equals("0"))
            {
                mErrors.addOneError("对不起，函件状态是待下发，您不能进行强制回销操作");
                return false;
            }
            if (db.getState() != null &&
                    !db.getState().equals("6"))
            {
                mErrors.addOneError("函件未超时，您不能进行强制回销操作");
                return false;
            }
            if (db.getBackFlag().equals("0"))
            {
                mErrors.addOneError("对不起，函件不需要回销，您不能进行强制回销。");
                return false;
            }
//          少儿险续保核保，客户反馈要吗两个险种都同意，要吗两个险种都不同意。
            String tEdorType = null;
            for(int i=1;i<=mLPUWMasterSet.size();i++)
            {
            	tEdorType = mLPUWMasterSet.get(i).getEdorType();
            }
            if(tEdorType.equals(BQ.EDORTYPE_XB))
            {
            	int no = 0;
            	for(int i=1;i<=mLPUWMasterSet.size();i++)
                {
                	if(mLPUWMasterSet.get(i).getCustomerReply().equals("2"))
                		no++;
                }
            	if(no!=0 && mLPUWMasterSet.size()!=no)
            	{
            		mErrors.addOneError("少儿险只能两个险种同时续保或同时终止。");
                    return false;
            	}
            		
            }
        }

        if (mOperate.equals("Delete"))
        {
            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setSchema(mLGLetterSchema);
            db.getInfo();
            if (db.getState() != null && !db.getState().equals("0"))
            {
                mErrors.addOneError("您只能删除状态为待下发的函件");
                return false;
            }
        }

        return true;
    }

    //查询保全申请信息
    private boolean changeLPEdorAppState(String state)
    {
        LPEdorAppDB db = new LPEdorAppDB();
        db.setEdorAcceptNo(this.mLGLetterSchema.getEdorAcceptNo());
        LPEdorAppSet set = db.query();

        if (set == null || set.size() == 0)
        {
            return false;
        }

        LPEdorAppSchema tLPEdorAppSchema = set.get(1);
        if (tLPEdorAppSchema == null)
        {
            return false;
        }

        tLPEdorAppSchema.setEdorState(state);
        tLPEdorAppSchema.setOperator(mGlobalInput.Operator);
        tLPEdorAppSchema.setModifyDate(mCurrentDate);
        tLPEdorAppSchema.setModifyTime(mCurrentTime);
        map.put(tLPEdorAppSchema, "DELETE&INSERT");

        return true;
    }

    //生成作业历史信息
    private LGTraceNodeOpSchema getTraceNodeOp(String operatorType)
    {

        LGTraceNodeOpSchema schema = new LGTraceNodeOpSchema();
        VData v = new VData();

        schema.setWorkNo(this.mLGLetterSchema.getEdorAcceptNo());
        schema.setOperatorType(operatorType);
        schema.setFinishDate(mCurrentDate);
        schema.setFinishTime(mCurrentTime);
        schema.setRemark(mLGLetterSchema.getFeedBackInfo());
        v.add(schema);
        v.add(mGlobalInput);

        TaskTraceNodeOpBL bl = new TaskTraceNodeOpBL();
        LGTraceNodeOpSchema s = bl.getSchema(v, "INSERT||MAIN");
        if (s == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            mErrors.addOneError("生成历史信息出错");

            return null;
        }
        else
        {
            return s;
        }
    }
}
