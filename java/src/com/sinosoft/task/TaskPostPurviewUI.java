package com.sinosoft.task;

import com.sinosoft.lis.schema.LGProjectPurviewSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LGProjectPurviewSet;
import com.sinosoft.task.GetMemberInfo;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskPostPurviewUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LGProjectPurviewSet mLGProjectPurviewSet = new LGProjectPurviewSet();

    public TaskPostPurviewUI()
    {
    }

    /**
     传输数据的公共方法，对外的接口
     参数 VData， String
     返回值：boolean, 执行成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = (VData)cInputData.clone();

        if(!getInputData())
        {
            return false;
        }

        CheckData tCheckData = new CheckData(CheckData.PURVIEW_MANAGE);
        if(!tCheckData.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(tCheckData.mErrors);
            return false;
        }

        TaskPostPurviewBL tTaskPostPurviewBL = new TaskPostPurviewBL();

        System.out.println("---UI BEGIN---" + mOperate);
        if (tTaskPostPurviewBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tTaskPostPurviewBL.mErrors);
            mResult.clear();

            return false;
        }
        else
        {
            mResult = tTaskPostPurviewBL.getResult();

            return true;
        }
    }

    /*
     获得页面传入的数据
     */
    private boolean getInputData()
    {
        try
        {
            mGlobalInput.setSchema(
                    (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
            mLGProjectPurviewSet =
                    (LGProjectPurviewSet) mInputData.
                    getObjectByObjectName("LGProjectPurviewSet", 0);
            for(int i = 0; i < mLGProjectPurviewSet.size(); i++)
            {
                LGProjectPurviewSchema s = mLGProjectPurviewSet.get(i + 1);
                System.out.print("\ngetProjectPurviewNo " + s.getProjectPurviewNo());
                System.out.print(" ProjectNo " + s.getProjectNo());
                System.out.print(" ProjectType " + s.getProjectType());
                System.out.print(" DealOrganization " + s.getDealOrganization());
                System.out.print(" PostNo " + s.getPostNo());
                System.out.print(" MoneyPurview " + s.getMoneyPurview());
                System.out.print(" ExcessPurview " + s.getExcessPurview());
                System.out.print(" TimePurview " + s.getTimePurview());
                System.out.println(" MemberNo " + s.getMemberNo());
            }

        }
        catch(Exception e)
        {
            this.mErrors.addOneError(new CError("页面传入的数据不完整，请确认。",
                                                "TaskPostPurviewUI", "getInputData"));
            System.out.println("页面传入的数据不完整，请确认" + e.toString());

            return false;
        }

        return true;
    }

    /**
     * 返回数据后台处理后的数据
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String s[])
    {
        GlobalInput gi = new GlobalInput();
        LGProjectPurviewSet tLGProjectPurviewSet = new LGProjectPurviewSet();

        VData v = new VData();

        gi.Operator = "endor1";
        gi.ComCode = "86";
        LGProjectPurviewSchema tLGProjectPurviewSchema;
        for(int i = 0; i < 1; i++)
        {
            tLGProjectPurviewSchema = new LGProjectPurviewSchema();

            //tLGProjectPurviewSchema.setProjectPurviewNo("384");
            tLGProjectPurviewSchema.setProjectNo("XX");
            tLGProjectPurviewSchema.setProjectType("0");
            tLGProjectPurviewSchema.setDealOrganization("0");
            tLGProjectPurviewSchema.setPostNo("PA04");
            tLGProjectPurviewSchema.setMoneyPurview("1500");
            tLGProjectPurviewSchema.setExcessPurview("0");
            tLGProjectPurviewSchema.setTimePurview("0");
            //tLGProjectPurviewSchema.setMemberNo("endor5");
            tLGProjectPurviewSchema.setConfirmFlag("2");
            tLGProjectPurviewSchema.setNeedOtherAudit("0");

            tLGProjectPurviewSet.add(tLGProjectPurviewSchema);
        }

        v.add(gi);
        v.add(tLGProjectPurviewSet);
        System.out.println(tLGProjectPurviewSet.size());

        TaskPostPurviewUI p = new TaskPostPurviewUI();
        if(!p.submitData(v, "INSERT||MAIN"))
        {
            System.out.println(p.mErrors.getErrContent());
        }
        else
        {
            System.out.println("成功了");
        }
    }
}


