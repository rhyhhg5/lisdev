package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskLetterUWUI
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    /**构造函数*/
    public TaskLetterUWUI()
    {
    }

    /**
     * 数据提交方法， 对外的接口
     * param：VDate， 需包含LGLetterSchema, GlobalInput,
     *        其中LGLetterSchema只需edorAcceptNo即可
     * String：操作方式标志
     * return：提交成功返回true， 否则返回false
     * */
    public boolean submitData(VData cInputData, String operate)
    {
        TaskLetterUWBL bl = new TaskLetterUWBL();
        if(!bl.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(bl.mErrors);

            return false;
        }

        return true;
    }
}
