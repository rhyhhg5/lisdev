package com.sinosoft.task;

import com.sinosoft.httpclientybk.deal.YbkXBTB;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.PEdorAppItemBL;
import com.sinosoft.lis.bq.PEdorAppItemUI;
import com.sinosoft.lis.bq.PEdorCCDetailBL;
import com.sinosoft.lis.bq.PEdorConfirmBL;
import com.sinosoft.lis.bq.PrtAppEndorsementBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.operfee.IndiCancelAndDueFeeBL;
import com.sinosoft.lis.operfee.IndiDueFeeBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BQCCEdorBL {

	  /** 错误保存容器 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput = null;

    private LGWorkSchema mLGWorkSchema = null;

    private LPContSchema mLPContSchema = null;

    private String mEdorAcceptNo = null;
    
    private String mContNo = null;

    private String mEdorType = BQ.EDORTYPE_CC;

    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private String mCustomerno =null;
    

    
    boolean isAppntFlag=false;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到生成的工单号
     * @return String
     */
    public String getEdorAcceptNo()
    {
        return mEdorAcceptNo;
    }

    /**
     * 得到输入数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
    	mCustomerno = (String) cInputData.get(0);
        mGlobalInput = (GlobalInput) cInputData.
                getObjectByObjectName("GlobalInput", 0);
        mLGWorkSchema = (LGWorkSchema) cInputData.
                getObjectByObjectName("LGWorkSchema", 0);

        mLPContSchema = (LPContSchema) cInputData.
                getObjectByObjectName("LPContSchema", 0);
        mContNo = mLPContSchema.getContNo();
            	
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        //添加工单受理
        mEdorAcceptNo = createTask();
        if ((mEdorAcceptNo == null) || (mEdorAcceptNo.equals("")))
        {
            return false;
        }
        mLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
        
        if (!addEdorItem())
        {
            return false;
        }
        if (!saveDetail())
        {
            return false;
        }
        //保全理算
        if (!appConfirm())
        {
            return false;
        }
        creatPrintVts();
        //保全确认
        if (!edorConfirm())
        {
            return false;
        }
        return true;
    }

    /**
     * 创建工单，添加工单受理
     * @return boolean
     */
    private String createTask()
    {
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(mLGWorkSchema);
        TaskInputBL tTaskInputBL = new TaskInputBL();
        if (!tTaskInputBL.submitData(data, ""))
        {
            mErrors.addOneError("工单数据生成失败！");
            return null;
        }
        return tTaskInputBL.getWorkNo();
    }

    /**
     * 添加保全项目
     * @return boolean
     */
    private boolean addEdorItem()
    {
    	
    	//正在操作保全的无法添加保全
		String edorSQL = " select edoracceptno from lpedoritem where contno='"
				+ mContNo
				+ "' and edoracceptno!='"+ mEdorAcceptNo+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";
		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			mErrors.addOneError("保单" + mContNo
					+ "正在操作保全，工单号为：" + edorFlag + "无法做交费资料变更");
			return false;
		}
		
    	//添加冻结校验
  	  String sql = "select 1 from LCContHangUpState where Contno = '" + mContNo + "' "
        + "and State = '1' with ur" ;
  	  ExeSQL tExeSQL2 = new ExeSQL();
  		String edorFlag2 = tExeSQL2.getOneValue(sql);
  		if (edorFlag2 != null && !"".equals(edorFlag2)) {
  			mErrors.addOneError("保单" + mContNo
  					+ "已经被挂起，不能做保全缴费方式变更操作");
  			return false;
  		}

        MMap map = new MMap();
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorAppNo(mEdorAcceptNo);
        tLPEdorMainSchema.setContNo(mContNo);
        tLPEdorMainSchema.setEdorAppDate(mCurrentDate);
        tLPEdorMainSchema.setEdorValiDate(mCurrentDate);
        tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INIT);
        tLPEdorMainSchema.setUWState(BQ.UWFLAG_INIT);
        tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorMainSchema.setMakeDate(mCurrentDate);
        tLPEdorMainSchema.setMakeTime(mCurrentTime);
        tLPEdorMainSchema.setModifyDate(mCurrentDate);
        tLPEdorMainSchema.setModifyTime(mCurrentTime);
        map.put(tLPEdorMainSchema, "INSERT");

      if (!submit(map))
      {
          return false;
      }

        

        mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
  //      mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
        mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
        mLPEdorItemSchema.setDisplayType("1");
        mLPEdorItemSchema.setEdorType(mEdorType);
        mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPEdorItemSchema.setContNo(mContNo);
        if(mCustomerno != null ){
        mLPEdorItemSchema.setInsuredNo(mCustomerno);
        }
        mLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
        mLPEdorItemSchema.setEdorValiDate(mCurrentDate);
        mLPEdorItemSchema.setEdorAppDate(mCurrentDate);
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setMakeDate(mCurrentDate);
        mLPEdorItemSchema.setMakeTime(mCurrentTime);
        mLPEdorItemSchema.setModifyDate(mCurrentDate);
        mLPEdorItemSchema.setModifyTime(mCurrentTime);

        
		PEdorAppItemUI tPEdorAppItemUI=new PEdorAppItemUI();
		LPEdorItemSet mLPEdorItemSet=new LPEdorItemSet();
		mLPEdorItemSet.add(mLPEdorItemSchema);
		
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("DisplayType","1");
		VData tVData = new VData();
        
        tVData.add(mLPEdorItemSet);
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

		PEdorAppItemBL pEdorAppItemBL = new PEdorAppItemBL();
		if (!pEdorAppItemBL.submitData(tVData, "INSERT||EDORITEM")) {
			mErrors.addOneError("生成保全工单失败" + pEdorAppItemBL.mErrors.getFirstError());
			return false;
		}

		return true;
    }

    /**
     * 保存明保全细
     * @return boolean
     */
    private boolean saveDetail()
    {
        //准备数据
    	mLPContSchema.setEdorNo(mEdorAcceptNo);
    
        VData data = new VData();
    	data.add(mLPContSchema); 
        data.add(mGlobalInput);
      //  data.add(mLPEdorItemSchema);
 
        PEdorCCDetailBL tPEdorCCDetailBL = new PEdorCCDetailBL();
        if (!tPEdorCCDetailBL.submitData(data))
        {
            mErrors.copyAllErrors(tPEdorCCDetailBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 保全理算，更新核保标志等同于保全理算
     * @return boolean
     */
    private boolean appConfirm()
    {
        MMap map = new MMap();
        String sql;
        sql = "update LPEdorApp set EdorState = '2', UWState = '9' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LPEdorMain set EdorState = '2', UWState = '9' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LPEdorItem set EdorState = '2' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");
        if (!submit(map))
        {
            return false;
        }
        return true;
    }

    /**
     * 产生打印数据
     * @return boolean
     */
    private boolean creatPrintVts()
    {
        //生成打印数据
        VData data = new VData();
        data.add(mGlobalInput);
        PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
                mEdorAcceptNo);
        if (!tPrtAppEndorsementBL.submitData(data, ""))
        {
            mErrors.addOneError("数据保存成功！但没有生成保全服务批单！");
            return false;
        }
        return true;
    }

    /**
     * 保全确认
     * @param edorAcceptNo String
     * @return boolean
     */
    private boolean edorConfirm()
    {
        MMap map = new MMap();
        PEdorConfirmBL tPEdorConfirmBL =
                new PEdorConfirmBL(mGlobalInput, mEdorAcceptNo);
        MMap edorMap = tPEdorConfirmBL.getSubmitData();
        if (edorMap == null)
        {
            mErrors.copyAllErrors(tPEdorConfirmBL.mErrors);
            return false;
        }
        map.add(edorMap);
        //工单结案
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
        tLGWorkSchema.setTypeNo("03"); //结案状态

        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGWorkSchema);
        TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
        MMap taskMap = tTaskAutoFinishBL.getSubmitData(data, "");
        if (taskMap == null)
        {
            mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
            return false;
        }
        map.add(taskMap);
        
        //作废续期应收数据

        IndiCancelAndDueFeeBL mIndiCancelAndDueFeeBL = new IndiCancelAndDueFeeBL(this.mGlobalInput);
        MMap tMMap = mIndiCancelAndDueFeeBL.getCancelData(this.mEdorAcceptNo);
        map.add(tMMap);

        if (mIndiCancelAndDueFeeBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mIndiCancelAndDueFeeBL.mErrors);
            return false;
        }
        if (!submit(map))
        {
            return false;
        } 
        
        
        //处理续期数据的重抽档，抽档失败也返回true，表示保全确认成功
        String xbsql = " select contno from lpedoritem where edorno = '"+mEdorAcceptNo+"' and edortype = 'EW' with ur ";
    	ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(xbsql);
        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
    		VData tVData = new VData();
    		TransferData tTransferData=new TransferData();
    		tTransferData.setNameAndValue("StartDate","2005-01-01");
    		tTransferData.setNameAndValue("EndDate","3000-12-31");
    		tTransferData.setNameAndValue("ContNo",tSSRS.GetText(1, 1));
    		tTransferData.setNameAndValue("ManageCom","86310000");
    		tTransferData.setNameAndValue("BusinessFlag","2"); 
    		tTransferData.setNameAndValue("flag",true);
    		tVData.add(mGlobalInput);
    		tVData.add(tTransferData);
    		YbkXBTB tYbkXBTB = new YbkXBTB();
    		tYbkXBTB.submitData(tVData, "INSERT");
    	}else{
    		if (!mIndiCancelAndDueFeeBL.getPRnewDueFeeMap(mEdorAcceptNo))
    		{
    			System.out.println(mIndiCancelAndDueFeeBL.getMessage());
    		}
    	}
        
        
        
        //万能续期抽档
        boolean isXUQI = false;
        //查询复效生效日
    	/*复效（FX）及免息复效（MF）*/
    	String MFSql = "select 1 from lpedoritem where edorno='"+mEdorAcceptNo+"' and edortype='MF'";
    	String MFResult = new ExeSQL().getOneValue(MFSql);
    	String EdorType = "";
    	if(MFResult != null && !"".equals(MFResult) &&"1".equals(MFResult)){
    		EdorType = "MF";
    	} else {
    		EdorType = "FX";
    	}
    	EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(mEdorAcceptNo, EdorType);
        String StartDate="";
        System.out.println("==========================22222");
        if(!tEdorItemSpecialData.query())
        {
        	System.out.println("111");
      //  	return false;
        }
        if("FX".equals(EdorType)){
        	if(tEdorItemSpecialData.getEdorValue("FX_XUQI")!=null&&"1".equals(tEdorItemSpecialData.getEdorValue("FX_XUQI"))){
            	isXUQI = true;
            }
            if(tEdorItemSpecialData.getEdorValue("FX_D")!=null&&!"".equals(tEdorItemSpecialData.getEdorValue("FX_D"))){
            	 StartDate = "1900-01-01";
            }else{
            	CError tError = new CError();
            	tError.moduleName = "BqConfirmBL";
            	tError.functionName = "updatePrint";
            	tError.errorMessage = "调用万能续期查询复效日期失败";
            	mErrors.addOneError(tError);
            	System.out.println(tError.errorMessage);
        //    	return false;
            }
        } else if ("MF".equals(EdorType)){
        	if(tEdorItemSpecialData.getEdorValue("MF_XUQI")!=null&&"1".equals(tEdorItemSpecialData.getEdorValue("MF_XUQI"))){
            	isXUQI = true;
            }
            if(tEdorItemSpecialData.getEdorValue("MF_D")!=null&&!"".equals(tEdorItemSpecialData.getEdorValue("MF_D"))){
            	 StartDate = "1900-01-01";
            }else{
            	CError tError = new CError();
            	tError.moduleName = "BqConfirmBL";
            	tError.functionName = "updatePrint";
            	tError.errorMessage = "调用万能续期查询免息复效日期失败";
            	mErrors.addOneError(tError);
            	System.out.println(tError.errorMessage);
          //  	return false;
            }
        }
        /*复效（FX）及免息复效（MF）*/
        
        if(isXUQI){
        	//查询续期保单
        	String queryContSQL= "select * from lccont where contno=(select contno from lpedoritem where edoracceptno='"+mEdorAcceptNo+"')";
        	LCContDB tLCContDB = new LCContDB();
        	LCContSchema tLCContSchema = new LCContSchema();
        	LCContSet tLCContSet = tLCContDB.executeQuery(queryContSQL);
        	if(tLCContSet!=null&&tLCContSet.size()>0){
        		tLCContSchema = tLCContSet.get(1).getSchema();
        	}else{
        		CError tError = new CError();
        		tError.moduleName = "BqConfirmBL";
        		tError.functionName = "updatePrint";
        		tError.errorMessage = "调用万能续期查询保单号失败";
       // 		mErrors.addOneError(tError);
        		System.out.println(tError.errorMessage);
      //  		return false;
        	}
        	VData tVData = new VData();
        	tVData.add(tLCContSchema);
        	tVData.add(mGlobalInput);
        	
            TransferData mTransferData=new TransferData();
        	mTransferData.setNameAndValue("StartDate",StartDate);
        	mTransferData.setNameAndValue("EndDate",PubFun.getCurrentDate());
        	
//        	mTransferData.setNameAndValue(FeeConst.GETNOTICENO, getGetNoticeNo());
        	tVData.add(mTransferData);
        	String queryType = "2";
        	tVData.add(queryType);
        	
        	//若没有需要续期抽档的险种，可能会有需要续保抽档的险种，正常退出
        	IndiDueFeeBL tIndiDueFeeBL = new IndiDueFeeBL();
        	MMap sMMap = tIndiDueFeeBL.getSubmitMMap(tVData, "INSERT");
        	if((sMMap == null || sMMap.size() == 0) && tIndiDueFeeBL.mErrors.needDealError())
        	{
        		if(mEdorAcceptNo != null && !tIndiDueFeeBL.getHasePolNeedFee())
        		{
        			
        			CError tError = new CError();
        			tError.moduleName = "BqConfirmBL";
        			tError.functionName = "updatePrint";
        			tError.errorMessage = "万能续期失败";
        			mErrors.addOneError(tError);
        			System.out.println(tError.errorMessage);
    //    			return false;
        			
        		}
        		
        		mErrors.copyAllErrors(tIndiDueFeeBL.mErrors);
    //    		return false;
        	}
        	
        	VData d = new VData();
        	d.add(tMMap);
        	
        	PubSubmit p = new PubSubmit();
        	if (!p.submitData(d, ""))
        	{
        		CError tError = new CError();
        		tError.moduleName = "BqConfirmBL";
        		tError.functionName = "updatePrint";
        		tError.errorMessage = "调用万能续期时更新续期表出错";
        		mErrors.addOneError(tError);
        		System.out.println(tError.errorMessage);
     //   		return false;
        	}        
        }

        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
