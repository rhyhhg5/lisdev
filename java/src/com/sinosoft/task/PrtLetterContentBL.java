package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LGLetterSchema;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.db.LGLetterDB;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.bq.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PrtLetterContentBL {
    /**
     * 错误的容器，若数据提交失败，则可从里面得到失败的原因
     */
    public CErrors mErrors = new CErrors();

    // 全局数据, 保存用户登录信息
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mResult = new VData();
    private LGLetterSchema mLGLetterSchema = new LGLetterSchema();

    //模板类型， "Edit": 编辑生成的模板, "Fixed": 固定格式模板
    private TextTag textTag = new TextTag(); //新建一个TextTag的实例
    private XmlExport xmlexport;

    public PrtLetterContentBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            mErrors.addOneError("不支持的操作字符串: " + cOperate + ", 请传入\"PRINT\"");
            return false;
        }

        //得到传入的数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //得到显示在图片上的数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    //得到传入的数据，并分配单前类的变量
    private boolean getInputData(VData cInputData) {
        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLGLetterSchema.setSchema((LGLetterSchema) cInputData.
                                      getObjectByObjectName("LGLetterSchema", 0));
            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
            tLGLetterDB.setSerialNumber(mLGLetterSchema.getSerialNumber());
            LGLetterSet tLGLetterSet = tLGLetterDB.query();
            if (tLGLetterSet.size() > 0) {
                mLGLetterSchema = tLGLetterSet.get(1);
            }
        } catch (Exception e) {
            mErrors.addOneError("传入后台的数据不完整");
            return false;
        }

        return true;
    }

    //得到显示在图片上的数据
    private boolean getPrintData() {
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        //生成打印的数据
        String letterType = mLGLetterSchema.getLetterType();
        String letterSubType = mLGLetterSchema.getLetterSubType();
        System.out.println("letterType" + letterType);
        System.out.println("letterSubType" + letterSubType);
        if (letterType.equals("1")) {
            xmlexport.createDocument("PrtLetterPreview.vts", "printer");
            if ((letterSubType != null) && letterSubType.equals("8")) { //内部流转
                if (mLGLetterSchema.getRemark() != null) {
                    if (mLGLetterSchema.getRemark().equals("迁出销售")) {
                        xmlexport.addDisplayControl(
                                "displayPROutSel");
                        if (!getDetailPROutSel()) {
                            return false;
                        }

                    }
                    if (mLGLetterSchema.getRemark().equals("迁入销售")) {
                        xmlexport.addDisplayControl(
                                "displayPRInSel");
                        if (!getDetailPRInSel()) {
                            return false;
                        }
                    }
                    if (mLGLetterSchema.getRemark().equals("迁入健管")) {
                        xmlexport.addDisplayControl(
                                "displayPRInHel");
                        if (!getDetailPRInHel()) {
                            return false;
                        }
                    }
                } else {
                    xmlexport.addDisplayControl(
                            "displaySend");
                    if (!getDetailSend()) {
                        return false;
                    }
                }
            } else {
                if((letterSubType != null) && letterSubType.equals("9"))//个单
                {
                    xmlexport.addDisplayControl("displayPRCustomer");
                    if (!getDetailPRCustomer()) {
                        return false;
                    }
                }
                else
                {
                    xmlexport.addDisplayControl("displayEdit");
                    if (!getDetailEdit()) {
                        return false;
                    }
                }
            }
        } else if (letterType.equals("0")) {
            ListTable tListTable;

            String sql = "select edorType "
                         + "from LPEdorItem "
                         + "where edorType = '" + BQ.EDORTYPE_XB + "' "
                         + "   and edorNo = '"
                         + mLGLetterSchema.getEdorAcceptNo() + "' ";
            String edorType = new ExeSQL().getOneValue(sql);
            if (!edorType.equals("") && !edorType.equals("null")) {
                xmlexport.createDocument("PrtLetterPreviewXB.vts", "printer");
                GetUWInfoXBBL g = new GetUWInfoXBBL(mLGLetterSchema.
                        getEdorAcceptNo());
                tListTable = g.getListTable();
                String[] title = new String[10];
                title[0] = "序号";
                title[1] = "被保险人";
                title[2] = "险种名称";
                title[3] = "代码";
                title[4] = "申请项目";
                title[5] = "核保意见";
                title[6] = "生效日期";
                title[7] = "具体说明";
                title[8] = "您的意见";
                title[9] = "不同意的处理";

                xmlexport.addDisplayControl("displayFixed");
                xmlexport.addListTable(tListTable, title);
            } else {
                xmlexport.createDocument("PrtLetterPreview.vts", "printer");
                GetUWInfoUI g = new GetUWInfoUI(mLGLetterSchema.getEdorAcceptNo());
                tListTable = g.getListTable();
                String[] title = new String[11];
                title[0] = "序号";
                title[1] = "保单号";
                title[2] = "被保险人";
                title[3] = "险种名称";
                title[4] = "代码";
                title[5] = "申请项目";
                title[6] = "核保意见";
                title[7] = "生效日期";
                title[8] = "具体说明";
                title[9] = "您的意见";
                title[10] = "不同意的处理";

                xmlexport.addDisplayControl("displayFixed");
                xmlexport.addListTable(tListTable, title);
            }
        }
        xmlexport.addTextTag(textTag);
        //输出xml文档到文件
        xmlexport.outputDocumentToFile("C:/", "PrtLetterPreview");
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private boolean getDetailPROutSel() {
    			if(!getDetailPR())
    			return false;
        return getDetailEdit();
    }

    private boolean getDetailPRInSel() {
    	  if(!getDetailPR())
    			return false;
        return getDetailEdit();
    }

    private boolean getDetailPRInHel() {
    		if(!getDetailPR())
    			return false;
        return getDetailEdit();
    }

    private boolean getDetailPRCustomer(){
        String edorNo = mLGLetterSchema.getEdorAcceptNo();
        String contNo = "";
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(edorNo);
        if (!tLGWorkDB.getInfo()) {
            mErrors.addOneError("未找到受理号" + edorNo + "的工单信息！");
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(edorNo);
        tLPEdorItemDB.setEdorType("PR");
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet.size() > 0) {
            contNo = tLPEdorItemSet.get(1).getContNo();
        }
        LPAddressDB tLPAddressDB = new LPAddressDB();
        tLPAddressDB.setEdorNo(edorNo);
        tLPAddressDB.setEdorType("PR");
        tLPAddressDB.setCustomerNo(tLGWorkDB.getCustomerNo());
        LPAddressSet tLPAddressSet = tLPAddressDB.query();
        textTag.add("ContNo", contNo);
        textTag.add("Address", tLPAddressSet.get(1).getPostalAddress());
        textTag.add("ZipCode", tLPAddressSet.get(1).getZipCode());
        textTag.add("Phone", tLPAddressSet.get(1).getPhone());
        //提示内容
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tLPEdorItemSet.get(1));
        tEdorItemSpecialData.query();
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tEdorItemSpecialData.getEdorValue("incom"));
        if (!tLDComDB.getInfo()) {
            mErrors.addOneError("查询转移机构失败");
            return false;
        }
        textTag.add("incom", tLDComDB.getLetterServiceName());
        String notice = "迁入公司地址："+tLDComDB.getServicePostAddress()+"、联系电话："+tLDComDB.getPhone()+"";
        textTag.add("notice",notice);

        return true;
    }
    public boolean getDetailEdit() {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Edit");
        LGLetterDB db = new LGLetterDB();
        db.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
        db.setSerialNumber(mLGLetterSchema.getSerialNumber());
        if (!db.getInfo()) {
            mErrors.addOneError("没有查到受理号序号分别为："
                                + mLGLetterSchema.getEdorAcceptNo() + " "
                                + mLGLetterSchema.getSerialNumber() + "的函件");
            return false;
        }
        String[] strArr = new String[1];
        strArr[0] = db.getLetterInfo();
        tlistTable.add(strArr);

        String[] printTitle = new String[1];
        printTitle[0] = "通知书正文";
        xmlexport.addListTable(tlistTable, printTitle);
        return true;
    }

    private boolean getDetailSend() {
        String edorNo = mLGLetterSchema.getEdorAcceptNo();
        String contNo = "";
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(edorNo);
        if (!tLGWorkDB.getInfo()) {
            mErrors.addOneError("未找到受理号" + edorNo + "的工单信息！");
            return false;
        }
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        tLPGrpEdorItemDB.setEdorType("NI");
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() > 0) {
            contNo = tLPGrpEdorItemSet.get(1).getGrpContNo();
        }

        textTag.add("EdorNo", edorNo);
        textTag.add("ContNo", contNo);
        textTag.add("CustomerNo", tLGWorkDB.getCustomerNo());
        textTag.add("CustomerName", tLGWorkDB.getCustomerName());

//        String content = "健管反馈信息\n根据本次变更，170101险种需（○退费○补费）共＿＿元。\n" +
//                "健管负责人签字：             日期：           ";
//        ListTable tlistTable = new ListTable();
//        tlistTable.setName("Send");
//        String[] strArr = new String[1];
//        strArr[0] = content;
//        tlistTable.add(strArr);
//        xmlexport.addListTable(tlistTable, new String[1]);
        return true;
    }

    //返回处理后的结果
    public VData getResult() {
        return mResult;
    }
    public boolean getDetailPR() {
        String edorNo = mLGLetterSchema.getEdorAcceptNo();
        String contNo = "";
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(edorNo);
        if (!tLGWorkDB.getInfo()) {
            mErrors.addOneError("未找到受理号" + edorNo + "的工单信息！");
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(edorNo);
        tLPEdorItemDB.setEdorType("PR");
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet.size() > 0) {
            contNo = tLPEdorItemSet.get(1).getContNo();
        }

        textTag.add("EdorNo", edorNo);
        textTag.add("ContNo", contNo);
        textTag.add("CustomerNo", tLGWorkDB.getCustomerNo());
        textTag.add("CustomerName", tLGWorkDB.getCustomerName());
        return true;
    }
}
