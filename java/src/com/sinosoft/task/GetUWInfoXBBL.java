package com.sinosoft.task;

import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.db.LPUWMasterDB;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class GetUWInfoXBBL
{
    private String mEdorAcceptNo = null;

    public GetUWInfoXBBL()
    {
    }

    public GetUWInfoXBBL(String no)
    {
        mEdorAcceptNo = no;
    }

    /**
     * 得到人工核保结论
     */
    public ListTable getListTable()
    {
        ListTable tUWResultListTable = new ListTable();
        tUWResultListTable.setName("RiskInfo"); //对应模版投保信息部分的行对象名

        //查询人工核保结论
        String sql = "  select * "
                + "from LPUWMaster "
                + "where edorNo = '" + mEdorAcceptNo + "' "
//                + "    and autoUWFlag = '2' "
                + "   and edorType = '" + BQ.EDORTYPE_XB + "' "
                + "   order by edorType ";
        LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
        LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.executeQuery(sql);
        System.out.print(tLPUWMasterSet.size());
        if (tLPUWMasterSet.size() > 0)
        {
            for (int i = 1, n = tLPUWMasterSet.size(); i <= n; i++)
            {
                LPUWMasterSchema schema = tLPUWMasterSet.get(i);
                String[] uwResult = new String[10];

                uwResult[0] = String.valueOf(i); //序号
                uwResult[1] = com.sinosoft.lis.bq.CommonBL.
                        getInsuredName(schema.getInsuredNo());
                uwResult[3] = getRiskCode(schema);
                uwResult[2] = com.sinosoft.lis.bq.CommonBL
                              .getRiskName(uwResult[3]);  //得到险种名
                uwResult[4] = getEdorName(schema.getEdorType());  //得到申请的项目名
                uwResult[5] = getPassFlagName(schema.getPassFlag()); //核保意见
                uwResult[6] = StrTool.cTrim(schema.getSugUWIdea()); //具体说明
                uwResult[7] = StrTool.cTrim(getPolCValiDate(schema)); //生效日期


                String CustomerReply = "";
                if (schema.getPassFlag().equals(BQ.PASSFLAG_CONDITION))
                {
                    CustomerReply = "□同意  □不同意";
                }
                else
                {
                    CustomerReply = "-";
                }
                uwResult[8] = CustomerReply;

                if ((schema.getDisagreeDeal() == null)
                    || (schema.getDisagreeDeal().equals("")))
                {
                    uwResult[9] = "-";
                }
                else
                {
                    uwResult[9] = com.sinosoft.lis.bq.ChangeCodeBL.
                            getCodeName("disagreedeal", schema.getDisagreeDeal());
                }
                tUWResultListTable.add(uwResult);
            }
        }
        return tUWResultListTable;
    }

    /**
     * 的到续保后险种生效日期
     * @param tLPUWMasterSchema LPUWMasterSchema
     * @return String
     */
    private String getPolCValiDate(LPUWMasterSchema tLPUWMasterSchema)
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(tLPUWMasterSchema.getEdorNo());
        tLPPolDB.setEdorType(tLPUWMasterSchema.getEdorType());
        tLPPolDB.setPolNo(tLPUWMasterSchema.getPolNo());
        if(tLPPolDB.getInfo())
        {
            return tLPPolDB.getCValiDate();
        }
        return "";
    }


    /**
     * 得到核保结论
     * @param passFlag String
     * @return String
     */
    private String getPassFlagName(String passFlag)
    {
        if(passFlag.equals("1"))
        {
            return "批准申请";
        }

        if(passFlag.equals("4"))
        {
            return "附加条件";
        }

        if(passFlag.equals("5"))
        {
            return "终止续保";
        }

        return "";
    }

    /**
     * 得到项目名称
     * @param edorType String
     * @return String
     */
    private String getEdorName(String edorType)
    {
        LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
        tLMEdorItemDB.setEdorCode(edorType);
        LMEdorItemSet set = tLMEdorItemDB.query();
        if(set.size() > 0)
        {
            return StrTool.cTrim(set.get(1).getEdorName());
        }
        else
        {
            return "";
        }

    }

    /**
     * 得到险种代码
     * @return String
     */
    private String getRiskCode(LPUWMasterSchema schema)
    {
        //若核保结论为终止续保，会删除LC表数据
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(schema.getEdorNo());
        tLPPolDB.setEdorType(BQ.EDORTYPE_XB);
        tLPPolDB.setPolNo(schema.getPolNo());
        LPPolSet set = tLPPolDB.query();
        if(set.size() > 0)
        {
            return StrTool.cTrim(set.get(1).getRiskCode());
        }
        else
        {
            return "";
        }
    }

    public static void main(String[] args)
    {
        GetUWInfoXBBL g = new GetUWInfoXBBL("20050921000011");
        g.getListTable();
    }

}
