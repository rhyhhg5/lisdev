package com.sinosoft.task;

import com.sinosoft.utility.ListTable;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 处理人工核保结论的信息
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class GetUWInfoUI
{
    private String edorAcceptNo = null;

    public GetUWInfoUI()
    {
    }

    public GetUWInfoUI(String no)
    {
        edorAcceptNo = no;
    }

    /**
     * 得到人工核保结论
     */
    public ListTable getListTable()
    {
        GetUWInfoBL bl = new GetUWInfoBL(edorAcceptNo);

        return bl.getListTable();
    }
    public static void main(String[] args) {




  }

}
