package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LGPhoneHastenSchema;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.db.LGPhoneHastenDB;
import com.sinosoft.utility.TransferData;



/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 改变工单状态BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-20
 */

public class TaskChangeHastenStatusBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private LGPhoneHastenSchema mLGPhoneHastenSchema = new LGPhoneHastenSchema();
    private TransferData tTransferData;

    public TaskChangeHastenStatusBL()
    {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in TaskChangeHastenStatusBL submit");

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
        {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false)
        {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskDeliverBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        mGlobalInput.setSchema((GlobalInput) mInputData.
                               getObjectByObjectName("GlobalInput", 0));
        mLGPhoneHastenSchema.setSchema((LGPhoneHastenSchema) mInputData.
                                       getObjectByObjectName(
                                               "LGPhoneHastenSchema", 0));
        tTransferData = (TransferData) mInputData.
                        getObjectByObjectName("TransferData", 0);
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        String sql;

        LGPhoneHastenDB db = new LGPhoneHastenDB();
        db.setWorkNo(mLGPhoneHastenSchema.getWorkNo());
        db.getInfo();

        String remark;
        if(this.mOperate.equals("Success"))
        {
            String payMode = "";
            payMode = (String) tTransferData.getValueByName("payMode");
            remark = db.getRemark() + " 催缴结果：客户愿意缴费，缴费方式为"
                     + (payMode.equals("1") ? "现金" : "银行" )
                     + ", 缴费期限为"
                     + (String) tTransferData.getValueByName("payDate");
        }
        else
        {
            remark = db.getRemark() + " 催缴结果： 用户撤销保全申请。";
        }
        sql = "Update LGPhoneHasten set " +
              "FinishType = '" + mLGPhoneHastenSchema.getFinishType() + "', " +
              "Operator = '" + mGlobalInput.Operator + "', " +
              "ModifyDate = '" + mCurrentDate + "', " +
              "ModifyTime = '" + mCurrentTime + "' " +
              "Where  WorkNo = '" + mLGPhoneHastenSchema.getWorkNo() + "' ";
        String updateLGwork = "update LGWork "
                              + "set Remark='" + remark + "', " +
                              "ModifyDate = '" + mCurrentDate + "', " +
                              "ModifyTime = '" + mCurrentTime + "' " +
                              "Where  WorkNo = '"
                              + mLGPhoneHastenSchema.getWorkNo() + "' ";

        map.put(sql, "UPDATE");  //修改
        map.put(updateLGwork, "UPDATE");

        //改变工单状态为结案
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        VData v = new VData();
        TaskChangeStatusBL tTaskChangeStatusBL = new TaskChangeStatusBL();

        tLGWorkSchema.setWorkNo(mLGPhoneHastenSchema.getWorkNo());
        tLGWorkSchema.setStatusNo("5");
        tLGWorkSchema.setCurrDoing("1");

        v.add(tLGWorkSchema);
        v.add(this.mGlobalInput);

        if(!tTaskChangeStatusBL.submitData(v, "UPDATE||MAIN"))
        {
            this.mErrors.copyAllErrors(tTaskChangeStatusBL.mErrors);
            System.out.println("调用TaskChangeStatusBL时出错："
                               + tTaskChangeStatusBL.mErrors.getErrContent());

            return false;
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        //mResult.clear();
        //mResult.add();
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }
}
