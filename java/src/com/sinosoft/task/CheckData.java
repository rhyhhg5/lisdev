package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LGAutoDeliverRuleSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 校验规则的合法性
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class CheckData
{
    public static final String NO_TYPE = "0";  //无类型
    public static final String SCAN_RULE = "1";  // 扫描规则
    public static final String HASTEN_RULE = "2";  //电话催缴规则
    public static final String NEXTFEE_HASTEN_RULE = "3";  //续期催缴
    public static final String MJ_RULE = "4";  //满期结算
    public static final String PURVIEW_MANAGE = "5";  //自动审批权限管理
    public static final String BALTIME_RULE = "6";  //定期结算转发规则
    public static final String XBP = "7";  //个险续保核保受理
    public static final String NEXTFEE_HASTEN_RULEP = "8";  //个险续期续保电话催缴

    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();
    public String mRuleType = null;

    public CheckData()
    {
    }

    public CheckData(String type)
    {
        mRuleType = type;
    }

    /**
     * description 提交数据的公共方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(mRuleType == null)
        {}
        else
        {
            CheckRule tCheckRule = null;

            //扫描转交规则
            if(mRuleType.equals(SCAN_RULE))
            {
                tCheckRule = new CheckScanRule();
            }
            //电话催缴任务分配规则，续期收费催缴
            else if(mRuleType.equals(this.HASTEN_RULE)
                || mRuleType.equals(this.NEXTFEE_HASTEN_RULE)
                || mRuleType.equals(this.MJ_RULE)
                ||mRuleType.equals(this.BALTIME_RULE))
            {
                tCheckRule = new CheckHastenRule();
            }
            //自动审批权限管理
            else if(mRuleType.equals(this.PURVIEW_MANAGE))
            {
                tCheckRule = new CheckPurviewManage();
            }

            if (tCheckRule != null && !tCheckRule.submitData(cInputData, cOperate))
            {
                mErrors.copyAllErrors(tCheckRule.mErrors);
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args)
    {
        CheckData checkData = new CheckData(CheckData.SCAN_RULE);

        LGAutoDeliverRuleSchema s = new LGAutoDeliverRuleSchema();
        s.setRuleContent("asdf");
        s.setSourceComCode("86");
        s.setGoalType("2");
        s.setTarget("2");

        VData v = new VData();
        v.add(s);

        if(!checkData.submitData(v, "UPDATE||MAIN"))
        {
            System.out.println(checkData.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
