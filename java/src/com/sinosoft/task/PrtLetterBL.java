package com.sinosoft.task;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *    处理函件信息，
 *    函件分为核保函件（mTemplate.equals("Fixed")）
 *    和非核保函件（mTemplate.equals("Fixed")）</p>
 *
 * <p>Copyright:  (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class PrtLetterBL
{
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();
    private VData mInputData = new VData();
    private String mOperate = "";
    private String mCustomerNo = null;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    private LGLetterSchema mLGLetterSchema = new LGLetterSchema();

    //模板类型， "Edit": 编辑生成的模板, "Fixed": 固定格式模板
    private String mTemplateType = "";
    private String mSubType = "";
    private TextTag texttag = new TextTag(); //新建一个TextTag的实例
    private XmlExport xmlexport;

    public PrtLetterBL()
    {
    }


    /**
     对外的接口，生成函件信息
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData)cInputData.clone();

        if (!cOperate.equals("PRINT"))
        {
            mErrors.addOneError("不支持的操作字符串: " + cOperate + ", 请传入\"PRINT\"");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        getCustomerNo();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        System.out.println("End of submitData");

        return true;
    }

    private void getCustomerNo()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLGLetterSchema.getEdorAcceptNo());
        tLGWorkDB.getInfo();
        mCustomerNo = tLGWorkDB.getCustomerNo();
    }

    /**
     * 对外的接口,保存函件信息
     */
    public boolean commitData(String isSave)
    {
        System.out.println("Beginning of commitData");
        CheckLetterData check = new CheckLetterData();
        if(!check.submitData (mInputData, "INSERT||MAIN"))
        {
            mErrors.copyAllErrors(check.mErrors);

            return false;
        }
        System.out.println("End of checkLetterData");

        if(!prepareData(xmlexport))
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, ""))
        {
            mErrors.addOneError("保存函件信息出错");
            return false;
        }
        System.out.println("End of commitData");

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLGLetterSchema = (LGLetterSchema) cInputData.
                              getObjectByObjectName("LGLetterSchema", 0);
            mTemplateType = mLGLetterSchema.getLetterType();
            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
            tLGLetterDB.setSerialNumber(mLGLetterSchema.getSerialNumber());
            LGLetterSet tLGLetterSet = tLGLetterDB.query();
            if (tLGLetterSet.size() > 0)
            {
                mSubType = tLGLetterSet.get(1).getLetterSubType();
                mLGLetterSchema.setRemark(tLGLetterSet.get(1).getRemark());
            }
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入后台的数据不完整");

            return false;
        }

        return true;
    }

    //准备打印数据
    private boolean getPrintData()
    {
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        ListTable tListTable = null;

        //非核保函件
        if (mTemplateType.equals("1"))
        {
            System.out.println("mSubType" + mSubType);
            if ((mSubType != null) && (mSubType.equals("8")))
            {
                xmlexport.createDocument("PrtLetterSend.vts", "printer");
                                if (mLGLetterSchema.getRemark() != null) {
                    if (mLGLetterSchema.getRemark().equals("迁出销售")) {
                        xmlexport.addDisplayControl(
                                "displayPROutSel");
                        if (!getDetailPROutSel()) {
                            return false;
                        }

                    }
                    if (mLGLetterSchema.getRemark().equals("迁入销售")) {
                        xmlexport.addDisplayControl(
                                "displayPRInSel");
                        if (!getDetailPRInSel()) {
                            return false;
                        }
                    }
                    if (mLGLetterSchema.getRemark().equals("迁入健管")) {
                        xmlexport.addDisplayControl(
                                "displayPRInHel");
                        if (!getDetailPRInHel()) {
                            return false;
                        }
                    }
                } else {
                    xmlexport.addDisplayControl(
                            "displaySend");
                    if (!getDetailSend()) {
                        return false;
                    }
                }
            }
            else
            {
                xmlexport.createDocument("PrtLetterEdit.vts", "printer");
                if((mSubType != null) && (mSubType.equals("9")))
                {
                    xmlexport.addDisplayControl("displayPRCustomer");
                    if (!getDetailPRCustomer()) {
                        return false;
                    }
                }else
                {
                    xmlexport.addDisplayControl("displayEdit");
                    if (!getDetailEdit()) {
                        return false;
                    }
                }
            }
        }
        //核保函件
        else if (mTemplateType.equals("0"))
        {
            String sql = "select max(int(serialNumber)) "
                         + "from LGLetter "
                         + "where EdorAcceptNo = '"
                         + mLGLetterSchema.getEdorAcceptNo() + "' ";
            String maxSerialNumber = new ExeSQL().getOneValue(sql);

            sql = "select edorType "
                  + "from LPEdorItem "
                  + "where edorType = '" + BQ.EDORTYPE_XB + "' "
                  + "   and edorNo = '"
                  + mLGLetterSchema.getEdorAcceptNo() + "' ";
            String edorType = new ExeSQL().getOneValue(sql);
            if(!edorType.equals("") && !edorType.equals("null"))
            {
                texttag.add("Title", "续保核保通知书");
                xmlexport.createDocument("PrtLetterXB.vts", "printer");

                GetUWInfoXBBL g = new GetUWInfoXBBL(mLGLetterSchema.getEdorAcceptNo());
                tListTable = g.getListTable();
                String[] title = new String[10];
                title[0] = "序号";
                title[1] = "被保险人";
                title[2] = "险种名称";
                title[3] = "险种代码";
                title[4] = "申请项目";
                title[5] = "核保意见";
                title[6] = "生效日期";
                title[7] = "具体说明";
                title[8] = "您的意见";
                title[9] = "不同意的处理";
                xmlexport.addDisplayControl("displayFixed");
                xmlexport.addListTable(tListTable, title);
            }
            else
            {
            	String[] title = new String[11];
                texttag.add("Title", "核保通知书");
                xmlexport.createDocument("PrtLetter.vts", "printer");

                GetUWInfoUI g = new GetUWInfoUI(mLGLetterSchema.getEdorAcceptNo());
                tListTable = g.getListTable();

                title[0] = "序号";
                title[1] = "保单号";
                title[2] = "被保险人";
                title[3] = "险种名称";
                title[4] = "险种代码";
                title[5] = "申请项目";
                title[6] = "核保意见";
                title[7] = "具体说明";
                title[8] = "您的意见";
                title[9] = "不同意的处理";
                title[10] = "";
                xmlexport.addDisplayControl("displayFixed");
                xmlexport.addListTable(tListTable, title);
            }
        }


        //生成答应的数据
        String[] array = mCurrentDate.split("-");
        String sendOutDate = array[0] + "年"
                             + array[1] + "月"
                             + array[2] + "日";
        xmlexport.addDisplayControl("displayHead");
        texttag.add("SendOutDate", sendOutDate);

        if(!getPICCHInfo())
        {
            return false;
        }

        texttag.add("BarCode1", mLGLetterSchema.getEdorAcceptNo()); //受理号
        texttag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        //客户号
        texttag.add("OperatorName", GetMemberInfo.getMemberName(mGlobalInput));
        texttag.add("CustomerNo", mCustomerNo);

        //申请日期
        String[] AppDateArray = mCurrentDate.split("-");
        String AppDate = AppDateArray[0] + "年"
                         + AppDateArray[1] + "月"
                         + AppDateArray[2] + "日";
        texttag.add("AppDate", AppDate);

        //结束日期
        String sql = "select date('" + mCurrentDate + "') + 5 days "
                     + "from LDUser ";
        ExeSQL tExeSQL = new ExeSQL();
        String[] EndDateArray = tExeSQL.getOneValue(sql).split("-");
        String EndDate = EndDateArray[0] + "年"
                         + EndDateArray[1] + "月"
                         + EndDateArray[2] + "日";
        texttag.add("EndDate", EndDate);

        //个体客户信息
        if (mCustomerNo.length() == 9)
        {
            if(!setPersonInfo())
            {
                return false;
            }
        }
        //团体客户名
        else
        {
            if (!setGrpInfo())
            {
                return false;
            }
        }


        //设置注意事项
        if(!setNotice())
        {
            return false;
        }

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        xmlexport.outputDocumentToFile("C:\\", "PrtLetter"); //输出xml文档到文件

        mResult.clear();
        mResult.addElement(xmlexport);
        System.out.println("End of getPrintData");


        return true;
    }

    /**
     * 获取当前登录PICCH（子）公司的相关信息
     * @return boolean, 执行通过，true
     */
    private boolean getPICCHInfo()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mGlobalInput.ComCode); ;
        tLDComDB.getInfo();
        texttag.add("ServicePhone", tLDComDB.getServicePhone());
        texttag.add("ServiceFax", tLDComDB.getFax());
        texttag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        texttag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        texttag.add("ComName", tLDComDB.getLetterServiceName());
        texttag.add("PrintDate", com.sinosoft.lis.bq.CommonBL.decodeDate(mCurrentDate));
        return true;
    }

    private boolean setPersonInfo()
    {
        try
        {
            //得到个体客户信息
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(mCustomerNo);
            if(!tLDPersonDB.getInfo())
            {
                mErrors.addOneError("查询客户信息出错");
                return false;
            }

            texttag.add("AppntName", tLDPersonDB.getName());
            texttag.add("EdorAppName", tLDPersonDB.getName());
            texttag.add("EdorAppSex",
                        com.sinosoft.lis.bq.CommonBL.decodeSex(
                                tLDPersonDB.getSex()));

            //地址信息
            LCAddressDB tLCAddressDB = new LCAddressDB();
            tLCAddressDB.setCustomerNo(tLDPersonDB.getCustomerNo());
            tLCAddressDB.setAddressNo(mLGLetterSchema.getAddressNo());
            tLCAddressDB.getInfo();
            texttag.add("EdorAppZipCode", tLCAddressDB.getZipCode());
            texttag.add("EdorAppAddress", tLCAddressDB.getPostalAddress());
        }
        catch(Exception e)
        {
            mErrors.addOneError("设置通知书个人信息时出错" + e.toString());

            return false;
        }
        System.out.println("End of setPersonInfo");

        return true;
    }

    public boolean setGrpInfo()
    {
        //通过LPGrpEdorMain的合同号
        try
        {
            //得到团体客户信息
            LDGrpDB tLDgrpDB = new LDGrpDB();
            tLDgrpDB.setCustomerNo(mCustomerNo);
            if(!tLDgrpDB.getInfo())
            {
                mErrors.addOneError("查询投保团体出错");
                return false;
            }

            texttag.add("AppntName", tLDgrpDB.getGrpName());

            //地址及联系人信息
            LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
            tLCGrpAddressDB.setCustomerNo(tLDgrpDB.getCustomerNo());
            tLCGrpAddressDB.setAddressNo(mLGLetterSchema.getAddressNo());
            tLCGrpAddressDB.getInfo();
            texttag.add("EdorAppName", tLCGrpAddressDB.getLinkMan1());
            texttag.add("EdorAppSex", "先生/女士");
            texttag.add("EdorAppZipCode", tLCGrpAddressDB.getGrpZipCode());
            texttag.add("EdorAppAddress", tLCGrpAddressDB.getGrpAddress());
        }
        catch(Exception e)
        {
            mErrors.addOneError("设置通知书公司信息时出错" + e.toString());

            return false;
        }

        return true;
    }

    private boolean getDetailEdit()
    {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Edit");

        String[] strArr = new String[1];
        strArr[0] = mLGLetterSchema.getLetterInfo();
        tlistTable.add(strArr);

        String[] printTitle = new String[1];
        printTitle[0] = "通知书正文";

        xmlexport.addListTable(tlistTable, printTitle);

        return true;
    }

    //获得当前函件的序号
    private String getSerialNumber()
    {
        //得到序号
        String sql = "select * "
                     + "from LGLetter "
                     + "where EdorAcceptNo='"
                     + mLGLetterSchema.getEdorAcceptNo() + "' "
                     + "order by int(SerialNumber) desc ";
        try
        {
            LGLetterDB db = new LGLetterDB();
            LGLetterSet set = db.executeQuery(sql);
            if (set == null || set.size() == 0)
            {
                return "1";
            }
            else
            {
                int sn = Integer.parseInt(set.get(1).getSerialNumber());
                if(mTemplateType.equals("0"))
                {
                  return String.valueOf(sn);
                }else{
                    return String.valueOf(sn + 1);
                }
            }
        }
        catch (Exception e)
        {
            mErrors.addOneError("得到函件序号时出错");
            return null;
        }

    }

    /**
     * 设置注意事项
     * 若函件需回销则函件上需提示客户回销注意事项
     * @return boolean
     */
    private boolean setNotice()
    {
        //需回销
        if(mLGLetterSchema.getBackFlag().equals("1")
           && mLGLetterSchema.getBackDate() != null)
        {
            String[] date = mLGLetterSchema.getBackDate().split("-");
            System.out.println("date.length " + date.length);
            if (date.length != 3)
            {
                return true;
            }

            StringBuffer notice = new StringBuffer();
            notice.append("请您于")
                    .append(date[0])
                    .append("年")
                    .append(date[1])
                    .append("月")
                    .append(date[2])
                    .append("日前将本函件及本函件要求材料送交我公司，")
                    .append("若我公司在上述日期内未收到您的回复，")
                    .append("本次保单服务申请将无效。\n")
                    .append("客户确认：\n")
                    .append("（请您根据本通知书的内容，在此处做出书面确认，并签名）\n\n\n\n\n\n");
            texttag.add("notice", notice.toString());
            texttag.add("customerSign", "客户签名：");
        }

        return true;
    }

    //准备写入磁盘的函件信息
    private boolean prepareData(XmlExport xml)
    {
        String sn = getSerialNumber();
        if(sn == null)
        {
            return false;
        }
        mLGLetterSchema.setSerialNumber(sn);
        mLGLetterSchema.setState("0");
        mLGLetterSchema.setSendDate(mCurrentDate);
        mLGLetterSchema.setSendTime(mCurrentTime);
        mLGLetterSchema.setOperator(mGlobalInput.Operator);
        mLGLetterSchema.setMakeDate(mCurrentDate);
        mLGLetterSchema.setMakeTime(mCurrentTime);
        mLGLetterSchema.setModifyDate(mCurrentDate);
        mLGLetterSchema.setModifyTime(mCurrentTime);

        if(mLGLetterSchema.getLetterInfo() == null
           || mLGLetterSchema.getLetterInfo().equals(""))
        {
            mLGLetterSchema.setLetterInfo("您还没有输入函件信息");
        }

        MMap map = new MMap();

        LPEdorEspecialDataSchema especialData = setLPEdorAppState();
        map.put(especialData, "DELETE&INSERT");


        String sql = "update LPEdorApp "
                     + "set edorState = '4', "
                     + "operator = '" + mGlobalInput.Operator + "', "
                     + "modifyDate = '" + mCurrentDate + "', "
                     + "modifyTime = '" + mCurrentTime + "' "
                     + "where edorAcceptNo = '"
                     + mLGLetterSchema.getEdorAcceptNo() + "' ";
        map.put(sql, "UPDATE");

        if(mTemplateType.equals("0"))
        {
            map.put(mLGLetterSchema, "UPDATE");
         }else
        {
             map.put(mLGLetterSchema, "INSERT");
         }


        mInputData.clear();
        mInputData.addElement(map);

        return true;
    }

    private LPEdorEspecialDataSchema setLPEdorAppState()
    {
        LPEdorAppDB db = new LPEdorAppDB();
        db.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
        db.getInfo();

        EdorItemSpecialData tEdorItemSpecialData =
            new EdorItemSpecialData(db.getEdorAcceptNo(), "BQ");
        tEdorItemSpecialData.add(BQ.DETAILTYPE_EDORSTATE, db.getEdorState());

        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema =
            tEdorItemSpecialData.getSpecialDataSet().get(1);

        return tLPEdorEspecialDataSchema;
    }

    private boolean getDetailPROutSel() {
    			if(!getDetailPR())
    			return false;
        return getDetailEdit();
    }

    private boolean getDetailPRInSel() {
    	  if(!getDetailPR())
    			return false;
        return getDetailEdit();
    }
    private boolean getDetailPRCustomer(){
        String edorNo = mLGLetterSchema.getEdorAcceptNo();
        String contNo = "";
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(edorNo);
        if (!tLGWorkDB.getInfo()) {
            mErrors.addOneError("未找到受理号" + edorNo + "的工单信息！");
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(edorNo);
        tLPEdorItemDB.setEdorType("PR");
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet.size() > 0) {
            contNo = tLPEdorItemSet.get(1).getContNo();
        }
        LPAddressDB tLPAddressDB = new LPAddressDB();
        tLPAddressDB.setEdorNo(edorNo);
        tLPAddressDB.setEdorType("PR");
        tLPAddressDB.setCustomerNo(tLGWorkDB.getCustomerNo());
        LPAddressSet tLPAddressSet = tLPAddressDB.query();
        texttag.add("ContNo", contNo);
        texttag.add("Address", tLPAddressSet.get(1).getPostalAddress());
        texttag.add("ZipCode", tLPAddressSet.get(1).getZipCode());
        texttag.add("Phone", tLPAddressSet.get(1).getPhone());
        //提示内容
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tLPEdorItemSet.get(1));
        tEdorItemSpecialData.query();
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tEdorItemSpecialData.getEdorValue("incom"));
        if (!tLDComDB.getInfo()) {
            mErrors.addOneError("查询转移机构失败");
            return false;
        }
        texttag.add("incom", tLDComDB.getLetterServiceName());
        String notice = "迁入公司地址："+tLDComDB.getServicePostAddress()+"、联系电话："+tLDComDB.getPhone()+"";
        texttag.add("PRnotice",notice);

        return true;
    }
    private boolean getDetailPRInHel() {
    		if(!getDetailPR())
    			return false;
        return getDetailEdit();
    }
        public boolean getDetailPR() {
        String edorNo = mLGLetterSchema.getEdorAcceptNo();
        String contNo = "";
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(edorNo);
        if (!tLGWorkDB.getInfo()) {
            mErrors.addOneError("未找到受理号" + edorNo + "的工单信息！");
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(edorNo);
        tLPEdorItemDB.setEdorType("PR");
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet.size() > 0) {
            contNo = tLPEdorItemSet.get(1).getContNo();
        }

        texttag.add("EdorNo", edorNo);
        texttag.add("ContNo", contNo);
        texttag.add("CustomerNo", tLGWorkDB.getCustomerNo());
        texttag.add("CustomerName", tLGWorkDB.getCustomerName());
        return true;
    }



    private boolean getDetailSend()
    {
        String edorNo = mLGLetterSchema.getEdorAcceptNo();
        String contNo = "";
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(edorNo);
        if (!tLGWorkDB.getInfo())
        {
            mErrors.addOneError("未找到受理号" + edorNo + "的工单信息！");
            return false;
        }
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        tLPGrpEdorItemDB.setEdorType("NI");
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() > 0)
        {
            contNo = tLPGrpEdorItemSet.get(1).getGrpContNo();
        }

        texttag.add("EdorNo", edorNo);
        texttag.add("ContNo", contNo);
        texttag.add("CustomerNo", tLGWorkDB.getCustomerNo());
        texttag.add("CustomerName", tLGWorkDB.getCustomerName());
        return true;
    }


    public VData getResult()
    {
        return mResult;
    }

    public static void main(String s[])
    {
        GlobalInput tG = new GlobalInput();
        LGLetterSchema schema = new LGLetterSchema();

        tG.Operator = "endor0";
        tG.ComCode = "86";
        schema.setEdorAcceptNo("20060903000004");
        schema.setLetterType("0");
        schema.setBackFlag("1");
        schema.setLetterInfo("jjjjjj");


        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(schema);

        PrtLetterBL tPrtLetterBL = new PrtLetterBL();
        XmlExport txmlExport = new XmlExport();
        if (!tPrtLetterBL.submitData(tVData, "PRINT"))
        {
            System.out.println(tPrtLetterBL.mErrors.getErrContent());
        }
        else
        {
            if(!tPrtLetterBL.commitData("Save"))
            {
                System.out.println(tPrtLetterBL.mErrors.getErrContent());
                return;
            }
            VData v = tPrtLetterBL.getResult();
            txmlExport=(XmlExport) v.getObjectByObjectName("XmlExport",0);
            System.out.println(txmlExport);

            System.out.println("Success");
        }

    }

}
