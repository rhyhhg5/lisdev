package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LGLetterSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LPEdorAppDB;
import com.sinosoft.lis.db.LGLetterDB;
import com.sinosoft.lis.vschema.LGLetterSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskLetterUWBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    //公共变量
    private LGLetterSchema mLGLetterSchema = null;
    private GlobalInput mGlobalInput = null;

    /**构造函数*/
    public TaskLetterUWBL()
    {
    }

    /**
     * 数据提交方法， 对外的接口
     * param：VDate， 需包含LGLetterSchema, GlobalInput,
     *        其中LGLetterSchema只需edorAcceptNo即可
     * String：操作方式标志
     * return：提交成功返回true， 否则返回false
     */
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        MMap map = new MMap();
        VData data = new VData();
        PubSubmit tPubSubmit = new PubSubmit();

        map.put(mLGLetterSchema, "INSERT");
        data.add(map);

        if(!tPubSubmit.submitData(data, "INSERT"))
        {
            mErrors.addOneError("保存数据出错.");
            System.out.println("保存数据出错->"
                               + tPubSubmit.mErrors.getErrContent());
            return false;
        }

        return true;
    }

    //将UI类传入的数据分配给本类的变量
    public boolean getInputData(VData cInputData)
    {
        try
        {
            mLGLetterSchema = (LGLetterSchema) cInputData.
                              getObjectByObjectName("LGLetterSchema", 0);
            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入后台的数据不完整");
            System.out.println("传入后台的数据不完整" + e.toString());

            return false;
        }

        return true;
    }

    //校验数据的合法性
    private boolean checkData()
    {
        String sql = "select * "
                     + "from LGLetter "
                     + "where edorAcceptNo = '"
                     + mLGLetterSchema.getEdorAcceptNo() + "' "
                     + "    and state not in ('3', '4', '5') " //回销、强制回销、已完成
                     + "order by int(serialNumber) ";

        LGLetterDB db = new LGLetterDB();
        LGLetterSet set = db.executeQuery(sql);
        if(set.size() > 0)
        {
            String errMsg = "";
            for(int i = 1, n = set.size(); i <= n; i++)
            {
                errMsg += set.get(i).getSerialNumber() + " ";
            }

            mErrors.addOneError("本次保全还有序号为 " + errMsg
                                + "的函件未回销, 请先回销该函件");
            return false;
        }

        return true;
    }

    //处理传入的数据
    private boolean dealData()
    {
        LPEdorAppDB db = new LPEdorAppDB();
        db.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
        if (!db.getInfo())
        {
            mErrors.addOneError("没有查询到受理号" + mLGLetterSchema.getEdorAcceptNo()
                                + "相应的保全受理");
            return false;
        }

        //组织函件信息
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        mLGLetterSchema.setSerialNumber(getNextSerialNumber(mLGLetterSchema));
        mLGLetterSchema.setLetterType("0");
        mLGLetterSchema.setSendObj("0");
        mLGLetterSchema.setState("0");
        mLGLetterSchema.setLetterInfo(".");
        mLGLetterSchema.setBackFlag("1");
        mLGLetterSchema.setOperator(mGlobalInput.Operator);
        mLGLetterSchema.setMakeDate(currentDate);
        mLGLetterSchema.setMakeTime(currentTime);
        mLGLetterSchema.setModifyDate(currentDate);
        mLGLetterSchema.setModifyTime(currentTime);

        return true;
    }

    //得到本次保全受理的下一个函件号
    private String getNextSerialNumber(LGLetterSchema schema)
    {
        LGLetterDB db = new LGLetterDB();
        db.setEdorAcceptNo(schema.getEdorAcceptNo());

        LGLetterSet set = db.query();
        if(set.size() == 0)
        {
            return "1";
        }
        else
        {
            return String.valueOf(set.size() + 1);
        }
    }

    public static void main(String args[])
    {
        LGLetterSchema s = new LGLetterSchema();
        s.setEdorAcceptNo("20050716000034");

        VData v = new VData();
        v.add(s);

        GlobalInput g = new GlobalInput();
        g.Operator = "endor5";
        v.add(g);

        TaskLetterUWBL bl = new TaskLetterUWBL();
        if(!bl.submitData(v, "Save"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
