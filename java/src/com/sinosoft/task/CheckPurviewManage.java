package com.sinosoft.task;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LGProjectPurviewSchema;
import com.sinosoft.lis.db.LDUserDB;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class CheckPurviewManage extends CheckRule
{
    private LGProjectPurviewSet mLGProjectPurviewSet = null;
    private GlobalInput mGlobalInput = null;

    public CheckPurviewManage()
    {
    }

    /**
     * description 提交数据的公共方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        //校验操作员的权限
        if(!checkManagerPurview())
        {
            return false;
        }

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkManagerPurview()
    {
        String postNo = GetMemberInfo.getPostNo(mGlobalInput); //操作员的岗位
        if (postNo.equals("-1"))
        {
            mErrors.addOneError(new CError("查询您的岗位级别时出错，如您还没有岗位级别，请与您的上级联系",
                                                "TaskPostPurviewUI", "checkLogic"));
            System.out.println("TaskPostPurviewUI的checkLogic方法中: 查询岗位级别时出错");

            return false;
        }

        //只有pa001, pa002, pa004可以进行个人权限的调整
        if (!postNo.toLowerCase().equals("pa01")
            && !postNo.toLowerCase().equals("pa02")
            && !postNo.toLowerCase().equals("pa04"))
        {
            this.mErrors.addOneError(new CError("对不起，您没有进行项目权限调整的权限",
                                                "TaskPostPurviewUI", "checkLogic"));
            System.out.println("TaskPostPurviewUI的checkLogic出错：操作员没有进行项目权限调整的权限");

            return false;
        }

        //只有pa001能调整岗位级别权限
        //较低级别人成员不能给同级和较高级别的成员调整权限
        if(!(postNo.toLowerCase().equals("pa01")))
        {
            for (int i = 0; i < this.mLGProjectPurviewSet.size(); i++)
            {
                String memberNo = this.mLGProjectPurviewSet.get(i + 1).
                                  getMemberNo();

                //如果页面没有传入成员编码（岗位）， 则是对岗位级别权限的调整
                if (memberNo == null || memberNo.equals(""))
                {
                    this.mErrors.addOneError(new CError(
                            "只有总公司保单管理部经理有对岗位级别进行权限调整的权限",
                            "TaskPostPurviewUI", "checkLogic"));
                    System.out.println("只有总公司保单管理部经理有对岗位级别进行权限调整的权限, "
                                       + "请检查输入数据的合法性");

                    return false;
                }

                //较低级别人成员不能给同级和较高级别的成员调整权限
                String postNoChanged = mLGProjectPurviewSet.get(i + 1).
                                       getPostNo(); //被修改者的岗位级别
                if (postNo.compareTo(postNoChanged) >= 0)
                {
                    String postName = "";
                    if (postNoChanged.equals("PA02"))
                    {
                        postName += "总公司保单管理主管";
                    }
                    if (postNoChanged.equals("PA03"))
                    {
                        postName += "总公司保单服务岗";
                    }
                    if (postNoChanged.equals("PA04"))
                    {
                        postName += "分公司保单管理主";
                    }

                    this.mErrors.addOneError(new CError(
                            "对不起，您没有给岗位级别为" + postName + "的成员调整项目权限的权限",
                            "TaskPostPurviewUI", "checkLogic"));
                    System.out.println("TaskPostPurviewUI的checkLogic出错："
                                       + postNo + "级别人员没有给"
                                       + postNoChanged + "级别人员调整权限的权限");

                    return false;
                }

                //只能给本机构及以下机构的成员调整权限
                LDUserDB db = new LDUserDB();
                db.setUserCode(memberNo);
                if(db.getInfo())
                {
                    //权限调整者的机构级别低于被调整权限者的机构级别
                    if(db.getComCode().indexOf(mGlobalInput.ComCode) < 0)
                    {
                        mErrors.addOneError("您不能给其他机构的人调整权限");
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * description 获得传入的数据
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (
                    (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            mLGProjectPurviewSet =
                    (LGProjectPurviewSet) cInputData.
                    getObjectByObjectName("LGProjectPurviewSet", 0);

            if(mGlobalInput == null || mLGProjectPurviewSet == null)
            {
                throw new Exception();
            }
        }
        catch (Exception e)
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("CheckPurviewManage->传入的数据不完整。" + e.toString());
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        CheckPurviewManage checkpurviewmanage = new CheckPurviewManage();

        GlobalInput gi = new GlobalInput();
        LGProjectPurviewSet tLGProjectPurviewSet = new LGProjectPurviewSet();

        VData v = new VData();

        gi.Operator = "endor4";
        gi.ComCode = "8611";
        LGProjectPurviewSchema tLGProjectPurviewSchema;
        for(int i = 0; i < 1; i++)
        {
            tLGProjectPurviewSchema = new LGProjectPurviewSchema();

            tLGProjectPurviewSchema.setProjectPurviewNo("384");
            tLGProjectPurviewSchema.setProjectNo("BC");
            tLGProjectPurviewSchema.setProjectType("0");
            tLGProjectPurviewSchema.setDealOrganization("1");
            tLGProjectPurviewSchema.setPostNo("PA02");
            tLGProjectPurviewSchema.setMoneyPurview("2222");
            tLGProjectPurviewSchema.setExcessPurview("0");
            tLGProjectPurviewSchema.setTimePurview("0");
            tLGProjectPurviewSchema.setMemberNo("endor2");
            tLGProjectPurviewSchema.setConfirmFlag("2");
            tLGProjectPurviewSchema.setNeedOtherAudit("0");

            tLGProjectPurviewSet.add(tLGProjectPurviewSchema);
        }

        v.add(gi);
        v.add(tLGProjectPurviewSet);
        System.out.println(tLGProjectPurviewSet.size());

        if(!checkpurviewmanage.submitData(v, "INSERT||MAIN"))
        {
            System.out.println(checkpurviewmanage.mErrors.getErrContent());
        }
        else
        {
            System.out.println("成功了");
        }

    }
}
