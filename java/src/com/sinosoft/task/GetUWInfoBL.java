package com.sinosoft.task;

import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.db.LPUWMasterDB;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GetUWInfoBL
{
    private String mEdorAcceptNo = null;

    public GetUWInfoBL()
    {
    }

    public GetUWInfoBL(String no)
    {
        mEdorAcceptNo = no;
    }

    /**
     * 得到人工核保结论
     */
    public ListTable getListTable()
    {
        ListTable tUWResultListTable = new ListTable();
        tUWResultListTable.setName("RiskInfo"); //对应模版投保信息部分的行对象名

        //查询人工核保结论
        String sql = "  select * "
                + "from LPUWMaster "
                + "where edorNo = '" + mEdorAcceptNo + "' "
//                + "    and autoUWFlag = '2' "
                + "    order by edorType ";
        LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
        LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.executeQuery(sql);
        System.out.print(tLPUWMasterSet.size());
        if (tLPUWMasterSet.size() > 0)
        {
            for (int i = 1, n = tLPUWMasterSet.size(); i <= n; i++)
            {
                ExeSQL tExeSQL = new ExeSQL();
                LPUWMasterSchema schema = tLPUWMasterSet.get(i);
                String[] uwResult = new String[11];

                uwResult[0] = String.valueOf(i); //序号
                uwResult[1] = com.sinosoft.lis.bq.CommonBL.
                        getInsuredName(schema.getInsuredNo());

                //险种代码
                LCPolDB db = new LCPolDB();
                db.setPolNo(schema.getPolNo());
                if (db.getInfo())
                {
                    uwResult[3] = StrTool.cTrim(db.getRiskCode());
                }
                else
                {
                    //若核保结论为终止续保，会删除LC表数据
                    LPPolDB tLPPolDB = new LPPolDB();
                    tLPPolDB.setEdorNo(schema.getEdorNo());
                    //tLPPolDB.setEdorType(BQ.EDORTYPE_XB);
                    tLPPolDB.setPolNo(schema.getPolNo());
                    LPPolSet set = tLPPolDB.query();
                    if (set.size() > 0)
                    {
                        uwResult[3] = StrTool.cTrim(set.get(1).getRiskCode());
                    }
                    else
                    {
                        uwResult[3] = "";
                    }
                }

                //得到险种名
                LMRiskDB tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(uwResult[3]);
                if (tLMRiskDB.getInfo())
                {
                    uwResult[2] = StrTool.cTrim(tLMRiskDB.getRiskName());
                }
                else
                {
                    uwResult[2] = "";
                }

                //得到申请的项目名
                LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
                tLMEdorItemDB.setEdorCode(schema.getEdorType());
                LMEdorItemSet set = tLMEdorItemDB.query();
                if (set.size() > 0)
                {
                    uwResult[4] = StrTool.cTrim(set.get(1).getEdorName());
                }
                else
                {
                    uwResult[4] = "";
                }

                String sugWUIdea = StrTool.cTrim(schema.getSugUWIdea());
//                LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
//                tLJSGetEndorseDB.setEndorsementNo(schema.getEdorNo());
//                tLJSGetEndorseDB.setFeeOperationType(schema.getEdorType());
//                tLJSGetEndorseDB.setPolNo(schema.getPolNo());
//                LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();
//                if (tLJSGetEndorseSet.size() > 0)
//                {
//                    double getMoney = tLJSGetEndorseSet.get(1).getGetMoney();
//                    if (getMoney > 0)
//                    {
//                        sugWUIdea += "本险种本次补费" + String.valueOf(getMoney) + "元";
//                    }
//                    else if (getMoney < 0)
//                    {
//                        sugWUIdea += "本险种本次退费" +
//                                String.valueOf(Math.abs(getMoney)) + "元";
//                    }
//                }
                uwResult[5] = StrTool.cTrim(sugWUIdea); //核保意见
                uwResult[6] = StrTool.cTrim(schema.getUWIdea()); //具体说明
                String CustomerReply = "";
                //ok by－严超
                String passFlag = schema.getPassFlag();
                if (passFlag.equals(BQ.PASSFLAG_CONDITION))
                {
                    CustomerReply = "□同意□不同意";
                }
                else
                {
                    CustomerReply = "-";
                }
                uwResult[7] = CustomerReply;
                String disagreeDeal = schema.getDisagreeDeal();
                if ((disagreeDeal == null) || (disagreeDeal.equals("")))
                {
                    uwResult[8] = "-";
                }
                else
                {
                    uwResult[8] = com.sinosoft.lis.bq.ChangeCodeBL.
                            getCodeName("disagreedeal", disagreeDeal);
                }
                uwResult[9] = schema.getContNo();
                tUWResultListTable.add(uwResult);
            }
        }
        return tUWResultListTable;
    }

    public static void main(String[] args)
    {
        GetUWInfoBL g = new GetUWInfoBL("20050921000011");
        g.getListTable();
    }

}
