package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LGPhoneHastenSchema;



/**
 * <p>Title: </p>
 *
 * <p>Description: 改变电话催缴任务的状态，在操作员手动撤销保全申请时使用</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author: Yang Yalin
 * @version 1.0
 */
public class TaskChangeHastenStatusUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public TaskChangeHastenStatusUI()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        TaskChangeHastenStatusBL tTaskChangeHastenStatusBL = new
                TaskChangeHastenStatusBL();
        if (tTaskChangeHastenStatusBL.submitData(mInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tTaskChangeHastenStatusBL.mErrors);
            mResult.clear();
            System.out.println("调用TaskChangeHastenStatusBL时出错：" +
                               tTaskChangeHastenStatusBL.mErrors.getErrContent());

            return false;
        }

        mResult = tTaskChangeHastenStatusBL.getResult();

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
        tLGPhoneHastenSchema.setWorkNo("20050804000015");
        tLGPhoneHastenSchema.setFinishType("0");


        tG.Operator = "endor";
        tG.ComCode = "86";
        tG.ManageCom = "";
        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tLGPhoneHastenSchema);

        TaskChangeHastenStatusUI tTaskChangeHastenStatusUI = new TaskChangeHastenStatusUI();
        if (!tTaskChangeHastenStatusUI.submitData(tVData, "CreateHistory"))
        {
            System.out.println("失败");
        }
    }

}
