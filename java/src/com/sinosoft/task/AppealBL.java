package com.sinosoft.task;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 投诉管理BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Yang Yalin
 * @version 1.0
 * @date 2005-04-09
 */
public class AppealBL
{
    /**错误保存容器*/
    public CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 输出数据的容器
    private VData mResult = new VData();

    // 数据操作字符串
    private String mOperate;

    private LGAppealSchema tLGAppealSchema;

    private MMap map = new MMap();

    // 全局参数
    private GlobalInput mGlobalInput = new GlobalInput();

    //统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    public AppealBL()
    {
    }

    /**
   * Appeal对象提交数据函数
   * 输入：数据集合AData， 操作方式String
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
    public boolean submitData(VData cInputData, String cOperate)
    {
    // 将传入的数据拷贝到本类中
    this.mOperate =cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;

    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLGWorkBoxBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLGWorkBoxBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          System.out.println("This is in if(!dealData())");
          return false;
    }

    //准备往后台的数据
    if (!prepareOutputData())
    {
      return false;
    }
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      //this.submitquery();
    }
    else
    {
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLDDiseaseBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
    }
    mInputData=null;
    return true;

  }


    //将输入的数据复制到本地
    private boolean getInputData(VData cInputData)
    {
        this.tLGAppealSchema = (LGAppealSchema) cInputData.getObject(0);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    //处理数据
    private boolean dealData()
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        int dealNo = 0;
        String s = "select max(to_number(DealNo)) " +
                   "from LGAppeal " +
                   "where AppealNo='" + tLGAppealSchema.getAppealNo() + "' ";
        try
        {
            tSSRS = tExeSQL.execSQL(s);
            if(tSSRS.getMaxRow() > 0)
            {
                //若能查到
                dealNo = Integer.parseInt(tSSRS.GetText(1, 1)) + 1;
            }
            else
            {
                dealNo = 0;
            }
        }
        catch(Exception ex)
        {
        }

        tLGAppealSchema.setDealNo(Integer.toString(dealNo));
        tLGAppealSchema.setOperator(mGlobalInput.Operator);
        tLGAppealSchema.setMakeDate(mCurrentDate);
        tLGAppealSchema.setMakeTime(mCurrentTime);
        tLGAppealSchema.setModifyDate(mCurrentDate);
        tLGAppealSchema.setModifyTime(mCurrentTime);

        if(mOperate.equals("INSERT||MAIN"))//新建
        {
            map.put(tLGAppealSchema, "INSERT");
        }
        else if(mOperate.equals("UPDATE||MAIN"))//经办
        {
            //为了清晰的纪录每次经办的情况，不删除上次纪录，仅仅执行插入操作
            map.put(tLGAppealSchema, "INSERT");

            //同时生成一条历史轨迹
            LGWorkTraceSchema wt = new LGWorkTraceSchema();
            String workBoxNo;
            int nodeNo;

            //查登陆用户的信箱号
            String sq = "select workBoxNo " +
                        "from LGWorkBox " +
                        "where ownerNo='" + mGlobalInput.Operator + "' ";
            //查询最大历史节点号
            String sql = "select max(to_number(NodeNo)) " +
                   "from LGWorkTrace " +
                   "where workNo='" + tLGAppealSchema.getAppealNo() + "' ";

            try
            {
                tSSRS = tExeSQL.execSQL(sq);
                workBoxNo = tSSRS.GetText(1, 1);

                tSSRS = tExeSQL.execSQL(sql);
                nodeNo = Integer.parseInt(tSSRS.GetText(1, 1));
                nodeNo++;

                System.out.println("Max nodeNo: " + nodeNo);
            }
            catch(Exception e)
            {
                workBoxNo = "";
                CError tError = new CError();
                tError.moduleName = "AppealBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询信箱号或历史节点时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }

            wt.setWorkNo(tLGAppealSchema.getAppealNo());
            wt.setWorkBoxNo(workBoxNo);
            wt.setNodeNo(Integer.toString(nodeNo));
            wt.setInMethodNo("5"); //经办
            wt.setInDate(mCurrentDate);
            wt.setInTime(mCurrentTime);
            wt.setSendComNo("");
            wt.setSendPersonNo("");
            wt.setModifyDate(mCurrentDate);
            wt.setModifyTime(mCurrentTime);
            wt.setOperator(mGlobalInput.Operator);
            wt.setMakeDate(mCurrentDate);
            wt.setMakeTime(mCurrentTime);
            map.put(wt, "INSERT"); //插入
        }
        return true;
    }

    /*
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.tLGAppealSchema);
            this.mInputData.add(this.map);
            mResult.clear();
            mResult.add(this.tLGAppealSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGAppealBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-------end of prepareOutputData()--------");

        return true;
    }

    public static void main(String args[])
    {
        AppealBL aAppealBL = new AppealBL();
        GlobalInput tG = new GlobalInput();
        VData tVData = new VData();
        LGAppealSchema schema = new LGAppealSchema();

        tG.Operator = "001";
        schema.setAppealNo("20050512000005");
        schema.setAcceptorNo("112414");
        schema.setAcceptorName("acceptorNameAAA");
        schema.setOperator("001");
        schema.setMakeDate(PubFun.getCurrentDate());
        schema.setMakeTime(PubFun.getCurrentTime());
        schema.setModifyDate(PubFun.getCurrentDate());
        schema.setModifyTime(PubFun.getCurrentTime());

        tVData.add(schema);
        tVData.add(tG);
        aAppealBL.submitData(tVData, "UPDATE||MAIN");
    }

}
