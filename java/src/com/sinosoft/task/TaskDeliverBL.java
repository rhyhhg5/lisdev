package com.sinosoft.task;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单转交BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-20
 */

public class TaskDeliverBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LGWorkSchema mLGWorkSchema = null;

    private LGWorkTraceSchema mLGWorkTraceSchema = null;

    private LGTraceNodeOpSchema mLGTraceNodeOpSchema = null;


    private MMap map = new MMap();

    private String[] mWorkNo = null;

    private String mDeliverType;

    private String mCopyFlag;

    /** 全局参数 */
    private GlobalInput mGlobalInput = null;

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    public TaskDeliverBL()
    {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in TaskDeliverBL submit");
        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
        {
            return false;
        }
        System.out.println("---getInputData---");

        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false)
        {
            return false;
        }
        System.out.println("---dealDate---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskDeliverBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        System.out.println("Now in getInputData");
        try
        {
            mGlobalInput = ((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mWorkNo = (String[]) mInputData.getObject(1);
            mCopyFlag = (String) mInputData.getObject(2);
            mDeliverType = (String) mInputData.getObject(3);
            mLGWorkSchema = ((LGWorkSchema) mInputData.
                                    getObjectByObjectName("LGWorkSchema", 0));
            mLGWorkTraceSchema = ((LGWorkTraceSchema) mInputData.
                                         getObjectByObjectName(
                    "LGWorkTraceSchema", 0));
            mLGTraceNodeOpSchema = ((LGTraceNodeOpSchema) mInputData.
                                           getObjectByObjectName(
                    "LGTraceNodeOpSchema", 0));
        }
        catch(Exception e)
        {
            System.out.println("Catch Exception in method getInputData");
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        String sql;
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        String tWorkNo;
        String tNodeNo;
        String tRemarkNo;
        String tTypeNo;
        String tWorkBoxNo;
        tTypeNo = mLGWorkSchema.getTypeNo();
        tWorkBoxNo = mLGWorkTraceSchema.getWorkBoxNo();

        mLGTraceNodeOpSchema.setRemark(
            StrTool.unicodeToGBK(mLGTraceNodeOpSchema.getRemark()));

        for (int i = 0; i < mWorkNo.length; i++)
        {
            String autoRemark = getAutoRemark(mWorkNo[i], tWorkBoxNo);
            tWorkNo = mWorkNo[i];
            LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();



            if ((mCopyFlag != null) && (mCopyFlag.equals("Y"))) //复制
            {
                sql = "Select * From LGWork " +
                      "Where WorkNo = '" + tWorkNo + "' ";
                LGWorkDB tLGWorkDB = new LGWorkDB();
                tLGWorkDB.setWorkNo(tWorkNo);
                tLGWorkDB.getInfo();

                if (tTypeNo == null)
                {
                    tTypeNo = tLGWorkDB.getTypeNo();
                }
                LGWorkSchema tLGWorkSchema = new LGWorkSchema();
                tLGWorkSchema = new LGWorkSchema();
                tLGWorkSchema.setCustomerNo(tLGWorkDB.getCustomerNo());
                tLGWorkSchema.setCustomerName(tLGWorkDB.getCustomerName());
                tLGWorkSchema.setTypeNo(tTypeNo);
                tLGWorkSchema.setDateLimit("");
                tLGWorkSchema.setApplyTypeNo(tLGWorkDB.getApplyTypeNo());
                tLGWorkSchema.setApplyName(tLGWorkDB.getApplyName());
                tLGWorkSchema.setPriorityNo(tLGWorkDB.getPriorityNo());
                tLGWorkSchema.setAcceptWayNo("9"); //受理途径：复制转交
                tLGWorkSchema.setAcceptDate(PubFun.getCurrentDate());
                tLGWorkSchema.setAcceptCom(tLGWorkDB.getAcceptCom());
                tLGWorkSchema.setAcceptorNo(tLGWorkDB.getAcceptNo());
                tLGWorkSchema.setOperator(getOperator(tWorkBoxNo));
                tLGWorkSchema.setRemark(autoRemark
                                        + mLGTraceNodeOpSchema.getRemark());

                VData tVData = new VData();
                tVData.add(mGlobalInput);
                tVData.add(tLGWorkSchema);
                tVData.add(tWorkBoxNo);
                TaskInputBL tTaskInputBL = new TaskInputBL();
                MMap tMap = tTaskInputBL.getSubmitData(tVData, "INSERT||MAIN");
                if (tMap == null)
                {
                    mErrors.copyAllErrors(tTaskInputBL.mErrors);
                    return false;
                }
                ((LGTraceNodeOpSchema) tMap
                 .getObjectByObjectName("LGTraceNodeOpSchema", 0))
                    .setOperatorType(Task.HISTORY_TYPE_CDELIVER);
                map.add(tMap);

                //生成旧工单的历史信息
                tVData.clear();
                String newWorkNo = ((LGWorkSchema) tMap
                                 .getObjectByObjectName("LGWorkSchema", 0))
                                .getWorkNo();
                autoRemark = autoRemark.replaceAll("复制转交", "复制转交(生成了新工单"
                                      + newWorkNo + ")");

                LGTraceNodeOpSchema traceNodeOpSchema = new LGTraceNodeOpSchema();
                traceNodeOpSchema.setWorkNo(tWorkNo);
                traceNodeOpSchema.setRemark(autoRemark
                                            + mLGTraceNodeOpSchema.getRemark());
                traceNodeOpSchema.setOperatorType(Task.HISTORY_TYPE_CDELIVER);
                tVData.add(traceNodeOpSchema);
                tVData.add(mGlobalInput);

                TaskTraceNodeOpBL tTaskTraceNodeOpBL = new TaskTraceNodeOpBL();
                traceNodeOpSchema = tTaskTraceNodeOpBL.getSchema(tVData, "");
                if(traceNodeOpSchema == null)
                {
                    mErrors.copyAllErrors(tTaskTraceNodeOpBL.mErrors);
                    return false;
                }
                map.put(traceNodeOpSchema, "INSERT");
            }
            else //不复制
            {
                LGTraceNodeOpSchema traceNodeOpSchema = new LGTraceNodeOpSchema();
                traceNodeOpSchema.setOperatorNo("0");
                traceNodeOpSchema.setRemark(autoRemark
                        + mLGTraceNodeOpSchema.getRemark());
                traceNodeOpSchema.setOperatorType(mLGTraceNodeOpSchema.
                        getOperatorType());
                traceNodeOpSchema.setFinishDate(mCurrentDate);
                traceNodeOpSchema.setFinishTime(mCurrentTime);
                traceNodeOpSchema.setOperator(mGlobalInput.Operator);
                traceNodeOpSchema.setMakeDate(mCurrentDate);
                traceNodeOpSchema.setMakeTime(mCurrentTime);
                traceNodeOpSchema.setModifyDate(mCurrentDate);
                traceNodeOpSchema.setModifyTime(mCurrentTime);

                //得到新的作业结点
                sql = "Select Case When max(to_number(NodeNo)) Is Null " +
                      "       Then 0 Else max(to_number(NodeNo))+1 End " +
                      "From   LGWorkTrace " +
                      "Where  WorkNo = '" + tWorkNo + "' ";
                tSSRS = tExeSQL.execSQL(sql);
                tNodeNo = tSSRS.GetText(1, 1);
                String operator = getOperator(tWorkBoxNo);
                if(operator.equals(""))
                {
                    operator = mGlobalInput.Operator;
                }

                //设置数据
                if ((mDeliverType != null) && (mDeliverType.equals("1")))
                {
                    //送审批，历史信息完成时间为审批结束时
                    sql = "Update LGWork set " +
                          "NodeNo = '" + tNodeNo + "', " +
                          "currDoing='0', " +
                          "StatusNo = '4', " + //设为审核状态
                          "Operator = '" + operator + "', " +
                          "ModifyDate = '" + mCurrentDate + "', " +
                          "ModifyTime = '" + mCurrentTime + "' " +
                          "Where  WorkNo = '" + tWorkNo + "' ";
                }
                else
                {
                    //一般转交，历史信息完成时间为转交时
                    traceNodeOpSchema.setFinishDate(mCurrentDate);
                    traceNodeOpSchema.setFinishTime(mCurrentTime);

                    sql = "Update LGWork set " +
                          "NodeNo = '" + tNodeNo + "', " +
                          "currDoing='0', " +
                          "Operator = '" + operator + "', " +
                          "ModifyDate = '" + mCurrentDate + "', " +
                          "ModifyTime = '" + mCurrentTime + "' " +
                          "Where  WorkNo = '" + tWorkNo + "' ";

                }
                map.put(sql, "UPDATE"); //修改

                //生成作业历史轨迹
                tLGWorkTraceSchema.setWorkNo(tWorkNo);
                tLGWorkTraceSchema.setNodeNo(tNodeNo);
                tLGWorkTraceSchema.setWorkBoxNo(tWorkBoxNo);
                tLGWorkTraceSchema.setInMethodNo("2"); //接收
                tLGWorkTraceSchema.setInDate(mCurrentDate);
                tLGWorkTraceSchema.setInTime(mCurrentTime);
                tLGWorkTraceSchema.setSendComNo(mGlobalInput.ManageCom);
                tLGWorkTraceSchema.setSendPersonNo(mGlobalInput.Operator);
                tLGWorkTraceSchema.setOperator(mGlobalInput.Operator);
                tLGWorkTraceSchema.setMakeDate(mCurrentDate);
                tLGWorkTraceSchema.setMakeTime(mCurrentTime);
                tLGWorkTraceSchema.setModifyDate(mCurrentDate);
                tLGWorkTraceSchema.setModifyTime(mCurrentTime);
                map.put(tLGWorkTraceSchema, "INSERT"); //插入

                //生成历史当前历史节点的具体信息
                traceNodeOpSchema.setWorkNo(tWorkNo);
                traceNodeOpSchema.setNodeNo(tNodeNo);
                map.put(traceNodeOpSchema, "INSERT"); //插入
            }
        }
        return true;
    }

    private String getOperator(String workBoxNo)
    {
        LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
        tLGWorkBoxDB.setWorkBoxNo(workBoxNo);
        tLGWorkBoxDB.getInfo();
        //个单
        if(tLGWorkBoxDB.getOwnerTypeNo().equals("2"))
        {
            return tLGWorkBoxDB.getOwnerNo();
        }
        else
        {
            return "";
        }
    }

    private String getAutoRemark(String tWorkNo, String tWorkBoxNo)
    {
        String autoRemark = "工单" + tWorkNo + "从";

        //旧轨迹信息
        String sql = "  select * from LGWorkTrace "
                     + "where workNo = '" + tWorkNo + "' "
                     + "order by int(nodeNo) desc ";
        System.out.println(sql);
        LGWorkTraceDB tLGWorkTraceDB = new LGWorkTraceDB();
        LGWorkTraceSchema oldLGWorkTraceSchema =
                tLGWorkTraceDB.executeQuery(sql).get(1);

        LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
        tLGWorkBoxDB.setWorkBoxNo(oldLGWorkTraceSchema.getWorkBoxNo());
        tLGWorkBoxDB.getInfo();

        //旧小组信箱
        String delieverType ;
        if ((mCopyFlag != null) && (mCopyFlag.equals("Y")))
        {
            delieverType = "复制转交给 ";
        }
        else
        {
            delieverType = "转交给 ";
        }
        if (tLGWorkBoxDB.getOwnerTypeNo().equals("1"))
        {
            LGGroupSchema schema = getLGGroupInfo(tLGWorkBoxDB.getOwnerNo());
            autoRemark += schema.getGroupName()
                    + "(" + schema.getGroupNo() + ")" + delieverType;
        }
        else
        {
            LDUserSchema tLDUserSchema = getUserInfo(tLGWorkBoxDB.getOwnerNo());
            autoRemark += tLDUserSchema.getUserName()
                    + "(" + tLDUserSchema.getUserCode() + ")" + delieverType;
        }

        //新轨迹信息
        tLGWorkBoxDB.setWorkBoxNo(tWorkBoxNo);
        tLGWorkBoxDB.getInfo();
        if (tLGWorkBoxDB.getOwnerTypeNo().equals("1"))
        {
            LGGroupSchema schema = getLGGroupInfo(tLGWorkBoxDB.getOwnerNo());
            autoRemark += schema.getGroupName()
                    + "(" + schema.getGroupNo() + ")。";
        }
        else
        {
            LDUserSchema tLDUserSchema = getUserInfo(tLGWorkBoxDB.getOwnerNo());
            autoRemark += tLDUserSchema.getUserName()
                    + "(" + tLDUserSchema.getUserCode() + ")。";
        }
        return autoRemark;
    }

    private LGGroupSchema getLGGroupInfo(String ownerNo)
    {
        LGGroupDB tLGGroupDB = new LGGroupDB();
        tLGGroupDB.setGroupNo(ownerNo);
        tLGGroupDB.getInfo();

        return tLGGroupDB.getSchema();
    }

    private LDUserSchema getUserInfo(String userCode)
    {
        LDUserDB db = new LDUserDB();
        db.setUserCode(userCode);
        db.getInfo();
        return db.getSchema();
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(mLGWorkSchema);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        String tCopyFlag = "0";
        String tDeliverType = "1";

        GlobalInput tGI = new GlobalInput();
        String[] tWorkNo = {"20051108000016", "20051108000015"};

        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        if (tCopyFlag.equals("Y"))
        {
            tLGWorkSchema.setTypeNo("111");
            tLGTraceNodeOpSchema.setOperatorType("2");
        }
        else
        {
            tLGTraceNodeOpSchema.setOperatorType("1");
        }
        tLGWorkTraceSchema.setWorkBoxNo("000027");
        tLGTraceNodeOpSchema.setRemark("adfsasdf");

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tWorkNo);
        tVData.add(tCopyFlag);
        tVData.add(tDeliverType);
        tVData.add(tLGWorkSchema);
        tVData.add(tLGWorkTraceSchema);
        tVData.add(tLGTraceNodeOpSchema);

        TaskDeliverBL bl = new TaskDeliverBL();
        if(bl.submitData(tVData, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
