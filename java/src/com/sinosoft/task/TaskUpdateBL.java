package com.sinosoft.task;

import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单录入BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-17
 */

public class TaskUpdateBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LGWorkSchema mLGWorkSchema = new LGWorkSchema();

    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    public TaskUpdateBL()
    {
    }

    /**
     * 外部操作的提交方法，得到录入的数据，进行业务逻辑处理
     * @param: ：对象，需要：
     a)	LGWorkSchema对象：存储页面录入的工单信息。
     b)	GlobalInput对象：操作员信息。
     * @param String：操作方式，此可为“”
     * @return: boolean:成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false)
        {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start TaskInputBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskInputBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        mGlobalInput.setSchema((GlobalInput) mInputData.
                getObjectByObjectName("GlobalInput", 0));
        mLGWorkSchema.setSchema((LGWorkSchema) mInputData.
                getObjectByObjectName("LGWorkSchema", 0));
        return true;
    }

    /**
     * 校验是否可以进行工单信息录入操作：录入的客户必须有已签单保单。
     * @return boolean:成功true，否则false
     */
    private boolean checkData()
    {
        //没有保单不能进行保全操作
        StringBuffer sql = new StringBuffer();
        //个单
        if(mLGWorkSchema.getCustomerNo().length() == 9)
        {
            sql.append("select distinct a.appntNo ").
                    append("from LCAppnt a, LCcont b ").
                    append("where a.contno = b.contno ").
                    append(" and b.appflag = '1' ").
                    append(" and a.appntNo = '").
                    append(mLGWorkSchema.getCustomerNo() + "' ");
        }
        //团单
        else if(mLGWorkSchema.getCustomerNo().length() == 8)
        {
            sql.append("select distinct a.customerNo, a.GrpName, a.GrpNature ").
                    append("from LDGrp a, LCGrpCont b ").
                    append(" where a.customerNo=b.appntNo ").
                    append("and AppFlag = '1' ").
                    append("and a.customerNo = '").
                    append(mLGWorkSchema.getCustomerNo() + "' ");
        }
        else
        {
            mErrors.addOneError("客户号必须为8位或9位。");
            return false;
        }

        ExeSQL e = new ExeSQL();
        SSRS s = e.execSQL(new String(sql));
        if(s == null || s.getMaxRow() == 0)
        {
            mErrors.addOneError("客户没有有效保单，不能申请保全操作。");
            System.out.println("TaskUpdateBL->客户没有有效保单，不能申请保全操作。");
            return false;
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        //生成工单信息
        if(!setLGWorkInfo())
        {
            return false;
        }

        //生成历史信息
        if(!setNodeOpInfo())
        {
            return false;
        }

        //只有保全工单才执行以下操作
        if(mLGWorkSchema.getTypeNo().indexOf("03") < 0)
        {
            return true;
        }

        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mLGWorkSchema.getWorkNo());
        LPEdorAppSet tLPEdorAppSet = tLPEdorAppDB.query();
        //生成保全申请主表信息
        if(tLPEdorAppSet == null || tLPEdorAppSet.size() == 0)
        {
            if(!addEdorApp())
            {
                return false;
            }
        }
        //受理后修改，不需要生成保全主表信息
        else
        {
            if(!compareCustomer(tLPEdorAppSet.get(1).getOtherNo()))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 校验工单是否有受理机构信息
     * @return boolean
     */
    private boolean emptyAcceptCom()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLGWorkSchema.getWorkNo());
        tLGWorkDB.getInfo();
        if(tLGWorkDB.getAcceptCom() == null
           || tLGWorkDB.getAcceptCom().equals(""))
        {
            return true;
        }

        return false;
    }

    /**
     * 生成工单信息
     * @return boolean
     */
    private boolean setLGWorkInfo()
    {
        if (mLGWorkSchema.getAcceptDate() == null
            || mLGWorkSchema.getAcceptDate().equals(""))
        {
            mLGWorkSchema.setAcceptDate(mCurrentDate);
        }

        //若原工单没有受理机构
        if(emptyAcceptCom())
        {
            LDUserDB tLDUserDB = new LDUserDB();
            tLDUserDB.setUserCode(mGlobalInput.Operator);
            tLDUserDB.getInfo();

            mLGWorkSchema.setAcceptCom(tLDUserDB.getComCode());
            mLGWorkSchema.setAcceptorNo(mGlobalInput.Operator);
        }

        String customerName = CommonBL.getCustomerName(
                mLGWorkSchema.getCustomerNo());
        if (customerName.equals("+-1"))
        {
            mErrors.addOneError("没有客户号为" + mLGWorkSchema.getCustomerNo()
                                + "的客户");
            return false;
        }
        //修改工单信息
        String sqlUpdate =
                "update LGWork " +
                "set detailWorkNo='" + mLGWorkSchema.getWorkNo() + "', " +
                "PriorityNo='" + mLGWorkSchema.getPriorityNo() + "', " +
                "TypeNo='" + mLGWorkSchema.getTypeNo() + "', " +
                "CustomerNo='" + mLGWorkSchema.getCustomerNo() + "', " +
                "customerName='" + customerName + "', " +
                "DateLimit='" + mLGWorkSchema.getDateLimit() + "', " +
                "ApplyTypeNo='" + mLGWorkSchema.getApplyTypeNo() + "', " +
                "ApplyName='" + mLGWorkSchema.getApplyName() + "', " +
                "AcceptWayNo='" + mLGWorkSchema.getAcceptWayNo() + "', " +
                "AcceptDate='" + mLGWorkSchema.getAcceptDate() + "', " +
                "AcceptCom='" + mLGWorkSchema.getAcceptCom() + "', " +
                "AcceptorNo='" + mLGWorkSchema.getAcceptorNo() + "', " +
                "Remark=Remark || '" + mLGWorkSchema.getRemark() + "', " +
                "Operator='" + mGlobalInput.Operator + "', " +
                "ModifyDate='" + mCurrentDate + "', " +
                "ModifyTime='" + mCurrentTime + "' " +
                "where workNo='" + mLGWorkSchema.getWorkNo() + "' ";
        map.put(sqlUpdate, "UPDATE"); //修改

        return true;
    }

    /**
     * 生成历史信息
     * @return boolean
     */
    private boolean setNodeOpInfo()
    {
        //生成历史信息
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        tLGTraceNodeOpSchema.setWorkNo(mLGWorkSchema.getWorkNo());
        tLGTraceNodeOpSchema.setOperatorType(Task.HISTORY_TYPE_EDIT);
        tLGTraceNodeOpSchema.setRemark(mLGWorkSchema.getRemark());

        VData data = new VData();
        data.add(tLGTraceNodeOpSchema);
        data.add(mGlobalInput);

        TaskTraceNodeOpBL bl = new TaskTraceNodeOpBL();
        LGTraceNodeOpSchema schema = bl.getSchema(data, "");
        if (schema == null)
        {
            mErrors.addOneError("生成历史信息出错。");
            return false;
        }

        map.put(schema, "INSERT");
        return true;
    }

    /**
     * 生成保全申请主表信息
     * @return boolean
     */
    public boolean addEdorApp()
    {
        if (mLGWorkSchema.getCustomerNo().length() == 9)
        { //个单
            mLPEdorAppSchema.setEdorAcceptNo(mLGWorkSchema.getWorkNo()); //工单号为保全申请号
            mLPEdorAppSchema.setOtherNo(mLGWorkSchema.getCustomerNo()); //申请号码
            mLPEdorAppSchema.setOtherNoType("1"); //申请号码类型 1-个人客户号
            mLPEdorAppSchema.setEdorAppName(mLGWorkSchema.getApplyName()); //申请人名称
            mLPEdorAppSchema.setAppType(mLGWorkSchema.getAcceptWayNo()); //申请方式
            mLPEdorAppSchema.setEdorAppDate(mLGWorkSchema.getAcceptDate()); //申请日期

            VData tVData = new VData();
            tVData.add(mLPEdorAppSchema);
            tVData.add(mGlobalInput);
            PEdorAppMainBL tPEdorAppMainBL = new PEdorAppMainBL();
            if (!tPEdorAppMainBL.submitData(tVData, "INSERT||EDORAPP"))
            {
                CError tError = new CError();
                tError.moduleName = "TaskInputBL";
                tError.functionName = "dealData";
                tError.errorMessage = "保全数据提交失败!";
                this.mErrors.addOneError(tError);
                System.out.println("个单保全数据提交失败");
                return false;
            }
            VData tResult = tPEdorAppMainBL.getResult();
            LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
            tLPEdorAppSchema.setSchema((LPEdorAppSchema)
                                       tResult.getObjectByObjectName(
                                               "LPEdorAppSchema", 0));
        }
        else if (mLGWorkSchema.getCustomerNo().length() == 8)
        { //团单
            System.out.println(mLGWorkSchema.getCustomerNo());

            //团单用保单号
            mLPEdorAppSchema.setEdorAcceptNo(mLGWorkSchema.getWorkNo());
            mLPEdorAppSchema.setOtherNo(mLGWorkSchema.getCustomerNo()); //申请号码
            mLPEdorAppSchema.setOtherNoType("2"); //申请号码类型 2-团体客户号
            mLPEdorAppSchema.setEdorAppName(mLGWorkSchema.getApplyName()); //申请人名称
            mLPEdorAppSchema.setAppType(mLGWorkSchema.getAcceptWayNo()); //申请方式
            mLPEdorAppSchema.setEdorAppDate(mLGWorkSchema.getAcceptDate()); //申请日期
            mLPEdorAppSchema.setManageCom(mGlobalInput.ManageCom); //管理机构
            mLPEdorAppSchema.setEdorState("1");
            mLPEdorAppSchema.setOperator(mGlobalInput.Operator);
            mLPEdorAppSchema.setMakeDate(mCurrentDate);
            mLPEdorAppSchema.setMakeTime(mCurrentTime);
            mLPEdorAppSchema.setModifyDate(mCurrentDate);
            mLPEdorAppSchema.setModifyTime(mCurrentTime);
            map.put(mLPEdorAppSchema, "INSERT");
        }
        //得到保全号受理号
        mLGWorkSchema.setDetailWorkNo(mLPEdorAppSchema.getEdorAcceptNo());

        return true;
    }

    private boolean compareCustomer(String customerInApp)
    {
        if(!customerInApp.equals(this.mLGWorkSchema.getCustomerNo()))
        {
            mErrors.addOneError("客户号与原受理客户号不同，不能进行保全操作");
            System.out.println("TaskUpdateBL->客户号与原受理客户号不同，不能进行保全操作");
            return false;
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
    }



    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        VData mResult = new VData();
        mResult.add(this.mLGWorkSchema);
        return mResult;
    }

    public static void main (String arg[])
    {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";

    //输入参数
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setWorkNo("20050915000013");
        tLGWorkSchema.setCustomerNo("00000059");
        tLGWorkSchema.setTypeNo("03");
        tLGWorkSchema.setDateLimit("6");
        tLGWorkSchema.setApplyTypeNo("3");
        tLGWorkSchema.setApplyName("yang");
        tLGWorkSchema.setPriorityNo("3");
        tLGWorkSchema.setAcceptWayNo("6");
        tLGWorkSchema.setAcceptDate("2006-3-6");
        tLGWorkSchema.setAcceptCom("86");
        tLGWorkSchema.setAcceptorNo("001");
        tLGWorkSchema.setRemark("aaaaaaaaaaaaaaaaaa");

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(tGI);

        TaskUpdateBL tTaskUpdateBL = new TaskUpdateBL();
        if (tTaskUpdateBL.submitData(tVData, "UPDATE||MAIN") == false) {
            System.out.println("false");
        }
    }
}
