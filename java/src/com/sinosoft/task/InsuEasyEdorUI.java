package com.sinosoft.task;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;


public class InsuEasyEdorUI
{
    private InsuEasyEdorBL mInsuEasyEdorBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public InsuEasyEdorUI()
    {
        mInsuEasyEdorBL = new InsuEasyEdorBL();
    }

    /**
     * 调用业务逻辑类
     * @param operator String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!mInsuEasyEdorBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public String getError()
    {
        return mInsuEasyEdorBL.mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getEdorAcceptNo()
    {
        return mInsuEasyEdorBL.getEdorAcceptNo();
    }

    public static void main(String args[])
     {
         GlobalInput tGlobalInput = new GlobalInput();
         tGlobalInput.Operator = "endor";
         tGlobalInput.ManageCom = "86";

         LGWorkSchema tLGWorkSchema = new LGWorkSchema();
         tLGWorkSchema.setCustomerNo("000019160");
         tLGWorkSchema.setTypeNo("03");

         //得到客户投保信息
         LPAppntSchema tLPAppntSchema = new LPAppntSchema();
         tLPAppntSchema.setContNo("00001916001");
         tLPAppntSchema.setAppntNo("000019160");

         //得到客户保单联系地址
         LPAddressSchema tLPAddressSchema = new LPAddressSchema();
         tLPAddressSchema.setCustomerNo("000019160");
         tLPAddressSchema.setHomeAddress("海淀");
         tLPAddressSchema.setHomeZipCode("102206");
         tLPAddressSchema.setHomePhone("88888888");

         VData tVData = new VData();
         tVData.add(tGlobalInput);
         tVData.add(tLGWorkSchema);
         tVData.add(tLPAppntSchema);
         tVData.add(tLPAddressSchema);
         InsuEasyEdorBL tInsuEasyEdorBL = new InsuEasyEdorBL();
         tInsuEasyEdorBL.submitData(tVData);


     }

}
