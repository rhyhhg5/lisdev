package com.sinosoft.task;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *    响应对函件的操作
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskLetterOperateBL
{
    /**
     * 错误的容器，若数据提交失败，则可从里面得到失败的原因
     * */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mResult = new VData();
    private VData mInputData = new VData();
    private String mOperate;
    private MMap map = new MMap();

    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    private LGLetterSchema mLGLetterSchema = new LGLetterSchema();

    public TaskLetterOperateBL()
    {
    }

    /**
     * 外部调用的接口，提交数据用
     * 参数： VData
     * 返回值：成功，true； 失败，false
     * */
    public boolean submitData(VData inputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData)inputData.clone();
        System.out.println("In TaskLetterOperateBL now");

        if(!getInputData(mInputData))
        {
            return false;
        }


        //暂存需要保留的数据
        String operatorType = "";  //作业历史信息生成方式

        if(!checkData())
        {
            return false;
        }

        String commonSqlPart =
                "operator='" + mGlobalInput.Operator + "', "
                + "modifyDate='" + mCurrentDate + "', "
                + "modifyTime='" + mCurrentTime + "' "
                + "where edorAcceptNo='"
                + mLGLetterSchema.getEdorAcceptNo() + "' "
                + "    and serialNumber='"
                + mLGLetterSchema.getSerialNumber() + "' ";

        //下发函件操作
        if(mOperate.equals("SendOut"))
        {
            String state = "";   //函件状态

            //若下发的函件不需要回销
            String backFlag = mLGLetterSchema.getBackFlag();
            if (backFlag.equals("0"))
            {
                state = "5";
            }
            else
            {
                state = "1";
            }
            operatorType = "3";

            String sql = "update LGLetter "
                         + "set state='" + state + "', "
                         + "backFlag='" + mLGLetterSchema.getBackFlag() + "', ";
            //若函件需要回销,则修改回销时间
            if (backFlag.equals("1"))
            {
                sql += "backDate='" + mLGLetterSchema.getBackDate() + "', ";
            }
            sql += commonSqlPart;
            map.put(sql, "UPDATE");

            if(backFlag.equals("1") && !changeLPEdorAppState("5"))   //保全状态为函件待回销
            {
                return false;
            }
        }
        //置函件状态为函件待处理
        else if(mOperate.equals("WaitDeal"))
        {
            if(!changeLPEdorAppState("6"))   //保全状态为回销待处理
            {
                return false;
            }

            String sql = "update LGLetter "
                         + "set state='2'"
                         + commonSqlPart;
            map.put(sql, "UPDATE");
        }
        //回销
        else if(mOperate.equals("FeedBack") || mOperate.equals("ForceFeedBack"))
        {
            String state;
            if(mOperate.equals("FeedBack"))
            {
                operatorType = "4";
                state = "3";
            }
            else
            {
                operatorType = "16";
                state = "4";
            }

            String sql = "update LGLetter "
                         + "set state='" + state + "', "
                         + "realBackDate='" + mCurrentDate + "', "
                         + "FeedBackInfo='"
                         + mLGLetterSchema.getFeedBackInfo() + "', "
                         + commonSqlPart;
            map.put(sql, "UPDATE");

            //20090205 zhanggm 如果是个单迁移，函件回销后修改保全状态为未明细录入
            if(isRP())
            {
            	sql = "update LPEdorItem "
                    + "set EdorState = '3', "
                    + "ModifyDate ='" + mCurrentDate + "', "
                    + "ModifyTime ='" + mCurrentTime + "', "
                    + "Operator = '" + mGlobalInput.Operator + "' "
                    + "where edorno = '" + this.mLGLetterSchema.getEdorAcceptNo() + "' "
                    + "and EdorType = 'PR' ";
            	map.put(sql, "UPDATE");
            	if(!changeLPEdorAppState("1"))
            	{
            		return false;
            	}
            }
            else   //保全状态为函件回销
            {
            	if(!changeLPEdorAppState("7"))
            	{
            		return false;
            	}
            }
        }
        else if(mOperate.equals("Delete"))
        {
            operatorType = "0";
            map.put("delete from LGLetter "
                    + "where edorAcceptNo = '"
                    + mLGLetterSchema.getEdorAcceptNo() + "' "
                    + "    and serialNumber = '"
                    + mLGLetterSchema.getSerialNumber() + "' ", "DELETE");
        }

        if(!operatorType.equals("0"))
        {
            LGTraceNodeOpSchema schema = getTraceNodeOp(operatorType);
            if (schema == null)
            {
                return false;
            }
            else
            {
                map.put(schema, "INSERT");
            }
        }

        mInputData.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData (mInputData, "INSERT||MAIN"))
        {
            mErrors.addOneError("提交数据失败");

            return false;
        }

        //重复理算保全受理
        if (mOperate.equals("FeedBack") || mOperate.equals("ForceFeedBack"))
        {
       		if (!isRP()&&!reCalculate())
            {
                return false;
            }
        }

        if(!reNewAppState())
        {
            return false;
        }

        return true;
    }

    private String getLPEdorAppState()
    {
        /*
        LPEdorEspecialDataDB db = new LPEdorEspecialDataDB();
        db.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
        db.setEdorNo(mLGLetterSchema.getSerialNumber());
        db.setEdorType("BQ");
        db.setDetailType(BQ.DETAILTYPE_EDORSTATE);
        db.getInfo();
     */
        EdorItemSpecialData tEdorItemSpecialData =
            new EdorItemSpecialData(mLGLetterSchema.getEdorAcceptNo(), "BQ");
        if(!tEdorItemSpecialData.query())
        {
            return null;
        }

        LPEdorEspecialDataSet set = tEdorItemSpecialData.getSpecialDataSet();
        for(int i = 1; i <= set.size(); i++)
        {
            if(set.get(i).getDetailType().equals(BQ.DETAILTYPE_EDORSTATE))
            {
                return set.get(i).getEdorValue();
            }
        }
        return null;

        //return db.getEdorValue();
    }

    /**
     * 函件回销和强制回销后需要重复理算
     * @return boolean
     */
    private boolean reCalculate()
    {
        String operate;
        LGWorkDB db = new LGWorkDB();
        db.setWorkNo(mLGLetterSchema.getEdorAcceptNo());
        db.getInfo();
        if(db.getCustomerNo().length() == 8)
        {
            operate = "GRP";
            GEdorReCalUI tGEdorReCalUI = new GEdorReCalUI(db.getWorkNo());
            if(!tGEdorReCalUI.submitData())
            {
                mErrors.addOneError(tGEdorReCalUI.getError());
                return false;
            }
        }
        else
        {
            operate = "P";
            PEdorReCalUI tPEdorReCalUI = new PEdorReCalUI(db.getWorkNo());
            if(!tPEdorReCalUI.submitData())
            {
                mErrors.addOneError(tPEdorReCalUI.getError());
                return false;
            }
        }

//        PrepareReCalBL tPreReCalBL = new PrepareReCalBL(
//            mLGLetterSchema.getEdorAcceptNo(), operate);
//        if(!tPreReCalBL.submitData())
//        {
//            mErrors.copyAllErrors(tPreReCalBL.mErrors);
//            return false;
//        }

        return true;
    }

    /**
     * 还原保全状态
     * @return boolean
     */
    private boolean reNewAppState()
    {
        MMap tMMap = new MMap();

        //还原保全状态
        if(mOperate.equals("SendOut"))
        {
            //若下发的函件不需要回销
            if (mLGLetterSchema.getBackFlag().equals("0"))
            {
                //还原保全受理状态
                String edorstate = getLPEdorAppState();
                if(null!=edorstate)
                {
                    tMMap.put("update LPEdorApp set edorState = '"
                              + edorstate + "' "
                              + "where edorAcceptNo = '"
                              + mLGLetterSchema.getEdorAcceptNo() + "' ",
                              "UPDATE");
                }
            }
        }
        else if(mOperate.equals("Delete"))
        {
            //还原保全受理状态
            String edorstate = getLPEdorAppState();
                if(null!=edorstate)
                {
                    tMMap.put("update LPEdorApp set edorState = '"
                              + edorstate + "' "
                              + "where edorAcceptNo = '"
                              + mLGLetterSchema.getEdorAcceptNo() + "' ",
                              "UPDATE");
                }
        }

        VData v = new VData();
        v.add(tMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData (v, "UPDATE||MAIN"))
        {
            mErrors.addOneError("提交数据失败");

            return false;
        }

        //删除保全状态记录
//        if(!deleteEspecialData())
//        {
//            return false;
//        }

        return true;
    }

    /**
     * 删除保全状态记录
     * @return boolean
     */
    private boolean deleteEspecialData()
    {
        MMap tMMap = new MMap();

        tMMap.put("delete from LPEdorEspecialData where edorAcceptNo = '"
                + mLGLetterSchema.getEdorAcceptNo() + "' and edorNo = '"
                + mLGLetterSchema.getSerialNumber() + "' and detailType = '"
                + BQ.DETAILTYPE_EDORSTATE + "' ", "DELETE");

        VData v = new VData();
        v.add(tMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(v, "DELETE||MAIN"))
        {
            mErrors.addOneError("提交数据失败");

            return false;
        }

        return true;
    }

    //将前台传入的数据分解到当前类变量
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.
                           getObjectByObjectName("GlobalInput", 0);
            mLGLetterSchema = (LGLetterSchema) data.
                              getObjectByObjectName("LGLetterSchema", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入后台的数据不完整");

            return false;
        }

        return true;
    }

    //检查传入数据的合法性
    private boolean checkData()
    {
        LGLetterSchema schema = new LGLetterSchema();

        LGLetterDB db = new LGLetterDB();
        db.setSchema(mLGLetterSchema);
        db.getInfo();
        if(db == null)
        {
            mErrors.addOneError("没有查到所选择的函件信息");

            return false;
        }
        else
        {
            schema = db.getSchema();
        }

        if(mOperate.equals("SendOut"))
        {
            boolean flag = true;
            String errMsg = "";

            //若没有录入回销与否，则查表判断函件是否是非核保函件，
            //非核保函件在新建时可能录入了回销与否与回销时间
            LGLetterDB tLGLetterDB = new LGLetterDB();

            if (mLGLetterSchema.getBackFlag() == null
                || mLGLetterSchema.getBackFlag().equals(""))
            {
                tLGLetterDB.setEdorAcceptNo(mLGLetterSchema.getEdorAcceptNo());
                tLGLetterDB.setSerialNumber(mLGLetterSchema.getSerialNumber());
                if(tLGLetterDB.getInfo())
                {
                    //若是非核保函件，则准备校验所需的数据
                    if(tLGLetterDB.getLetterType().equals("1"))
                    {
                        mLGLetterSchema.setBackFlag(tLGLetterDB.getBackFlag());
                    }
                }
            }

            if(!schema.getState().equals("0"))
            {
                flag = false;
                errMsg += "函件状态不是待下发； ";
            }
            else if(mLGLetterSchema.getBackFlag() == null
               || mLGLetterSchema.getBackFlag().equals(""))
            {
                flag = false;
                errMsg += "是否回销不能为空； ";
            }
            else if (mLGLetterSchema.getBackFlag().equals("1")
                     && (mLGLetterSchema.getBackDate() == null
                         || mLGLetterSchema.getBackDate().equals("")))
            {
                //若没有录入回销时间,则查表判断函件是否是非核保函件，
                //非核保函件在新建时可能录入了回销时间
                if (tLGLetterDB.getBackDate() == null
                    || tLGLetterDB.getBackDate().equals(""))
                {
                    //为录入回销时间
                    flag = false;
                    errMsg += "函件需回销，但您没有录入回销时间； ";
                }
                else
                {
                    //录入了回销时间
                   // mLGLetterSchema.setBackDate(tLGLetterDB.getBackDate());
                }
            }
            else if (mLGLetterSchema.getBackFlag().equals("1")
                     && mLGLetterSchema.getBackDate() != null
                     && !mLGLetterSchema.getBackDate().equals(""))
            {
                String sql = "select userCode "
                             + "from LDUser "
                             + "where days('" + mCurrentDate + "') > days('"
                             + mLGLetterSchema.getBackDate() + "') ";
                ExeSQL e = new ExeSQL();
                String exist = e.getOneValue(sql);
                if(exist != null && !exist.equals(""))
                {
                    flag = false;
                    errMsg += "回销日期不能早于今天";
                }
            }

            if(!flag)
            {
                mErrors.addOneError("您不能下发函件，因为发生了如下错误：" + errMsg);
                return false;
            }
        }
        //回销
        if(mOperate.equals("FeedBack"))
        {
            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setSchema(mLGLetterSchema);
            db.getInfo();

            if (db.getState() != null && db.getState().equals("0"))
            {
                mErrors.addOneError("对不起，函件状态是待下发，您不能进行回销操作");
                return false;
            }
            if (db.getState() != null
                    && db.getState().equals("6"))
            {
                mErrors.addOneError("函件已超时，请进行强制回销操作");
                return false;
            }
        }
        //强制回销
        if(mOperate.equals("ForceFeedBack"))
        {
            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setSchema(mLGLetterSchema);
            db.getInfo();
            if (db.getState() != null && db.getState().equals("0"))
            {
                mErrors.addOneError("对不起，函件状态是待下发，您不能进行强制回销操作");
                return false;
            }
            if (db.getState() != null &&
                    !db.getState().equals("6"))
            {
                mErrors.addOneError("函件未超时，您不能进行强制回销操作");
                return false;
            }
/*
            if(db.getBackFlag().equals("0"))
            {
                mErrors.addOneError("对不起，函件不需要回销，您不能进行强制回销。");
                return false;
            }
            */
        }

        if(mOperate.equals("Delete"))
        {
            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setSchema(mLGLetterSchema);
            db.getInfo();
            if(db.getState() != null && !db.getState().equals("0"))
            {
                mErrors.addOneError("您只能删除状态为待下发的函件");
                return false;
            }
        }

        return true;
    }

    //查询保全申请信息
    private boolean changeLPEdorAppState(String state)
    {
        LPEdorAppDB db = new LPEdorAppDB();
        db.setEdorAcceptNo(this.mLGLetterSchema.getEdorAcceptNo());
        LPEdorAppSet set = db.query();

        if(set == null || set.size() == 0)
        {
            return false;
        }

        LPEdorAppSchema tLPEdorAppSchema = set.get(1);
        if (tLPEdorAppSchema == null)
        {
            return false;
        }

        tLPEdorAppSchema.setEdorState(state);
        tLPEdorAppSchema.setOperator(mGlobalInput.Operator);
        tLPEdorAppSchema.setModifyDate(mCurrentDate);
        tLPEdorAppSchema.setModifyTime(mCurrentTime);
        map.put(tLPEdorAppSchema, "DELETE&INSERT");

        return true;
    }

    //生成作业历史信息
    private LGTraceNodeOpSchema getTraceNodeOp(String operatorType)
    {

        LGTraceNodeOpSchema schema = new LGTraceNodeOpSchema();
        VData v = new VData();

        schema.setWorkNo(this.mLGLetterSchema.getEdorAcceptNo());
        schema.setOperatorType(operatorType);
        schema.setFinishDate(mCurrentDate);
        schema.setFinishTime(mCurrentTime);
        schema.setRemark(mLGLetterSchema.getFeedBackInfo());
        v.add(schema);
        v.add(mGlobalInput);

        TaskTraceNodeOpBL bl = new TaskTraceNodeOpBL();
        LGTraceNodeOpSchema s = bl.getSchema(v, "INSERT||MAIN");
        if(s == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            mErrors.addOneError("生成历史信息出错");

            return null;
        }
        else
        {
            return s;
        }
    }
    
    //20081225 zhanggm 个单保单迁移PR，不重新理算
    private boolean isRP()
    {
    	String sql = "select EdorType from LPEdoritem where EdorAcceptNo = '" 
    		       + mLGLetterSchema.getEdorAcceptNo() + "' with ur";
    	String tEdorType = new ExeSQL().getOneValue(sql);
    	if(tEdorType!=null && !"".equals(tEdorType) && "PR".equals(tEdorType))
    	{
    		return true;
    	}
    	return false;
    }
}




