package com.sinosoft.task;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import java.sql.Connection;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成作业历史信息
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class TaskTraceNodeOpBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = null;

    /** 输出数据的容器 */
    private VData mResult = null;

    /** 数据操作字符串 */
    private String mOperate;
    private LGTraceNodeOpSchema mLGTraceNodeOpSchema = null;
    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = null;

    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    //连接数据库变量
    private Connection conn = null;
    private boolean submitFlag = true;

    public TaskTraceNodeOpBL()
    {
    }

    public MMap getSubmitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        if (this.getInputData() == false)
        {
            return null;
        }

        if (this.dealData() == false)
        {
            return null;
        }
        prepareOutputData();
        return map;
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        if (this.getInputData() == false)
        {
            return false;
        }

        if (this.dealData() == false)
        {
            return false;
        }

        prepareOutputData();

        //公共提交方法
        PubSubmit tPubSubmit = new PubSubmit();
        if(!submitFlag)
        {
            tPubSubmit.setCommitFlag(false);
            tPubSubmit.setConnection(conn);
        }
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("数据提交失败");
            return false;
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        mGlobalInput =
            (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        mLGTraceNodeOpSchema =
            (LGTraceNodeOpSchema) mInputData.getObjectByObjectName(
                "LGTraceNodeOpSchema", 0);
        try
        {
            conn = (com.sinosoft.utility.DBConn)
                   mInputData.getObjectByObjectName("DBConn",0);
            submitFlag = ((Boolean) mInputData.
                          getObjectByObjectName("Boolean",0)).booleanValue();
        }
        catch(Exception e)
        {
            //System.out.println("在askTraceNodeOpBL中"
            //                   + "没有取到数据库连接conn: " + e.toString()
            //                   + "或立即提交标志submitFlag: " + e.toString()
            //                   + "但不影响程序的正常执行");
        }

        return true;
    }

    /**
     * 得到处理后的数据
     */
     public LGTraceNodeOpSchema getSchema(VData cInputData, String cOperate)
     {
         mInputData = (VData) cInputData.clone();
         this.mOperate = cOperate;

         if(!getInputData() || !dealData())
         {
             return null;
         }
         this.prepareOutputData();

         return mLGTraceNodeOpSchema;
     }

    /**
         * 根据业务逻辑对数据进行处理
         * @param: 无
         * @return: boolean
     */
    private boolean dealData()
    {
        if(mOperate.equals("UPDATE||MAIN"))
        {
            updateNodeOp();
        }
        else
        {
            createNodeOp();
        }

        return true;
    }

    /**
     * 维护历史信息
     * @return boolean
     */
    private void updateNodeOp()
    {
        map.put(mLGTraceNodeOpSchema, "DELETE&INSERT");
    }

    /**
     * 生成历史信息
     * @return boolean
     */
    private boolean createNodeOp()
    {
        String workNo = mLGTraceNodeOpSchema.getWorkNo();
        String nodeNo = mLGTraceNodeOpSchema.getNodeNo();
        if(nodeNo == null || nodeNo.equals(""))
        {
            nodeNo = getNodeNo(workNo);
        }
        //获得最大节点号并加1
        String operatorNo = getNextOperatorNo(workNo, nodeNo);
        setLGTraceNodeOpSchema(nodeNo, operatorNo);

        return true;
    }

    /**
     * 得到历史节点号
     * @param workNo String
     * @return String
     */
    private String getNodeNo(String workNo)
    {
        String nodeNo;
        String queryMaxNode = "select char(max(int(nodeNo))) "
                              + "from LGTraceNodeOp "
                              + "where workNo='" + workNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(queryMaxNode);
        if (tSSRS.getMaxRow() > 0)
        {
            nodeNo = tSSRS.GetText(1, 1);
        }
        else
        {
            nodeNo = "0";
        }
        if (nodeNo.equals(""))
        {
            nodeNo = "0";
        }
        return nodeNo;
    }

    /**
     * 该历史节点的下一操作号
     * @param workNo String
     * @param nodeNo String
     * @return String
     */
    private String getNextOperatorNo(String workNo, String nodeNo)
    {
        String sql = "select char(max(int(OperatorNo) + 1)) "
                     + "from LGTraceNodeOp "
                     + "where workNo='" + workNo + "' "
                     + "      and nodeNo='" + nodeNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);

        if (tSSRS.getMaxRow() > 0 && !tSSRS.GetText(1, 1).equals(""))
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            return "0";
        }
    }

    private boolean setLGTraceNodeOpSchema(String nodeNo, String operatorNo)
    {
        mLGTraceNodeOpSchema.setNodeNo(nodeNo);
        mLGTraceNodeOpSchema.setOperatorNo(operatorNo);
        mLGTraceNodeOpSchema.setOperator(mGlobalInput.Operator);
        mLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
        mLGTraceNodeOpSchema.setFinishTime(mCurrentTime);
        mLGTraceNodeOpSchema.setMakeDate(mCurrentDate);
        mLGTraceNodeOpSchema.setMakeTime(mCurrentTime);
        mLGTraceNodeOpSchema.setModifyDate(mCurrentDate);
        mLGTraceNodeOpSchema.setModifyTime(mCurrentTime);
        map.put(mLGTraceNodeOpSchema, "INSERT");

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult = new VData();
        mResult.add(mLGTraceNodeOpSchema);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String a[])
    {
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        tLGTraceNodeOpSchema.setWorkNo("20051109000002");
        //tLGTraceNodeOpSchema.setNodeNo("0");
        tLGTraceNodeOpSchema.setOperatorType("7");

        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";


        VData v = new VData();
        v.add(gi);
        v.add(tLGTraceNodeOpSchema);

        TaskTraceNodeOpBL t = new TaskTraceNodeOpBL();
        if(!t.submitData(v, ""))
        {
            System.out.println("Oh, my God, it's failed.");
        }
    }
}




