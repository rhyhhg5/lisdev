package com.sinosoft.task;

import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.schema.LCPolSchema;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LJAPayPersonDB;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 维护续期信息
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.3
 */
public class DealFeeAndBQ
{
    public DealFeeAndBQ()
    {
    }

    /**
     * 根据contNo生成续期个人实收信息
     * 循环每个contNo下的每个险种进行处理，合计到团险险种收费
     * @param contNo String
     */
    private void createLJAPayPerson(String getNoticeNo, String[] contNo)
    {
//        String payNo = "32000273219";
        HashMap grpPolPremHashMap = new HashMap();
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();

        String sql = "select * from LJAPayPerson "
                     + "where getNoticeNo = '" + getNoticeNo + "' "
                     + "fetch first 1 rows only ";
        LJAPayPersonSet set = new LJAPayPersonDB().executeQuery(sql);
        LJAPayPersonSchema styleSchema = set.get(1);

        //循环每个个单
        for(int t = 0; t < contNo.length; t++)
        {
            //得到个单的所有险种
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setContNo(contNo[t]);
            LCPolSet tLCPolSet = tLCPolDB.query();

            //循环个单下的每个险种
            double sumPolMoney = 0;  //险种总缴费
            for(int j = 1; j <= tLCPolSet.size(); j++)
            {
                LCPolSchema tLCPolSchema = tLCPolSet.get(j);

                LCPremDB tLCPremDB = new LCPremDB();
                tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
                LCPremSet tLCPremSet = tLCPremDB.query();

                for(int i = 1; i <= tLCPremSet.size(); i++)
                {
                    LCPremSchema s = tLCPremSet.get(i);

                    LJAPayPersonSchema schema = new LJAPayPersonSchema();
                    schema.setPolNo(s.getPolNo());

                    schema.setPayCount(s.getPayTimes() + 1); //第几次交费
                    schema.setGrpPolNo(tLCPolSchema.getGrpPolNo()); //集体保单险种号码
                    schema.setGrpContNo(tLCPolSchema.getGrpContNo()); //集体保单号码
                    schema.setContNo(s.getContNo()); //集体保单号码
                    schema.setAppntNo(s.getAppntNo()); //投保人客户号码
                    schema.setPayNo(styleSchema.getPayNo()); //交费收据号码
                    schema.setPayAimClass(styleSchema.getPayAimClass()); //交费目的分类
                    schema.setDutyCode(s.getDutyCode()); //责任编码
                    schema.setPayPlanCode(s.getPayPlanCode()); //交费计划编码
                    schema.setSumDuePayMoney(s.getPrem()); //总应交金额
                    schema.setSumActuPayMoney(s.getPrem()); //总实交金额
                    schema.setPayIntv(s.getPayIntv()); //交费间隔
                    schema.setPayDate(styleSchema.getPayDate()); //交费日期
                    schema.setPayType("ZC"); //交费类型
                    schema.setEnterAccDate(styleSchema.getEnterAccDate()); //到帐日期
                    schema.setConfDate(styleSchema.getConfDate()); //确认日期
                    schema.setLastPayToDate(styleSchema.getLastPayToDate()); //原交至日期
                    schema.setCurPayToDate(styleSchema.getCurPayToDate()); //现交至日期
                    schema.setInInsuAccState(styleSchema.getInInsuAccState()); //转入保险帐户状态
                    schema.setApproveCode(styleSchema.getApproveCode()); //复核人编码
                    schema.setApproveDate(styleSchema.getApproveDate()); //复核日期
                    schema.setSerialNo(styleSchema.getSerialNo()); //流水号
                    schema.setOperator(styleSchema.getOperator()); //操作员
                    schema.setMakeDate(styleSchema.getMakeDate()); //入机日期
                    schema.setMakeTime(styleSchema.getMakeTime()); //入机时间
                    schema.setGetNoticeNo(styleSchema.getGetNoticeNo()); //通知书号码
                    schema.setModifyDate(styleSchema.getMakeDate()); //最后一次修改日期
                    schema.setModifyTime(styleSchema.getModifyTime()); //最后一次修改时间
                    schema.setManageCom(s.getManageCom()); //管理机构
                    schema.setAgentCom(styleSchema.getAgentCom()); //代理机构
                    schema.setAgentType(styleSchema.getAgentType()); //代理机构内部分类
                    schema.setRiskCode(tLCPolSchema.getRiskCode()); //险种编码
                    schema.setAgentCode(tLCPolSchema.getAgentCode());
                    schema.setAgentGroup(tLCPolSchema.getAgentGroup());

                    tLJAPayPersonSet.add(schema);

                    sumPolMoney += schema.getSumActuPayMoney();
                }

                //将险种总缴费累计到对应的risk上
                if(grpPolPremHashMap.containsKey(tLCPolSchema.getRiskCode()))
                {
                    String moneyStr = (String) grpPolPremHashMap
                                      .get(tLCPolSchema.getRiskCode());
                    double money = Double.parseDouble(moneyStr) + sumPolMoney;
                    grpPolPremHashMap.put(tLCPolSchema.getRiskCode(),
                                          String.valueOf(money));
                }
                else
                {
                    grpPolPremHashMap.put(tLCPolSchema.getRiskCode(),
                                          String.valueOf(sumPolMoney));
                }
            }
        }

        //需要处理LJAPay,LJAPayGrp,LJSPayB,LJSPayGrpB,LOPRTManagerSub的总缴费
        MMap map = new MMap();
        map.put(tLJAPayPersonSet, "INSERT");

//        double sumMoney = 0;
//
//        Set set = grpPolPremHashMap.keySet();
//        for(Iterator it = set.iterator(); it.hasNext();)
//        {
//            String riskCode = (String) it.next();
//            String value = (String) grpPolPremHashMap.get(riskCode);
//            double money = Double.parseDouble(value);
//            sumMoney += money;
//
//            String[] table = {"LJAPayGrp", "LJSPayGrpB"};
//            for(int i = 0; i < table.length; i++)
//            {
//                String sql = "update " + table[i]
//                             + " set SumDuePayMoney=SumDuePayMoney + " + money
//                             + ",  SumActuPayMoney = SumActuPayMoney + " + money
//                             + " where getNoticeNo = '" + getNoticeNo + "' "
//                             + "   and riskCode = '" + riskCode + "' ";
//                map.put(sql, "update");
//            }
//
//            String sql = "update LOPRTManagerSub "
//                         + " set duePayMoney=duePayMoney + " + money
//                         + " where getNoticeNo = '" + getNoticeNo + "' "
//                         + "   and riskCode = '" + riskCode + "' ";
//            map.put(sql, "update");
//        }
//
//        String[] table =
//                         {"LJAPay", "LJSPayB"};
//        for(int i = 0; i < table.length; i++)
//        {
//            String sql = "update " + table[i]
//                         + " set SumActuPayMoney=SumActuPayMoney + " + sumMoney
//                         + " where getNoticeNo = '" + getNoticeNo + "' ";
//            map.put(sql, "update");
//        }

        VData d = new VData();
        d.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
        }
    }

    public static void main(String[] args)
    {
        String[] contNo = {"2300262194", "2300262195", "2300262196"};
        DealFeeAndBQ b = new DealFeeAndBQ();
        b.createLJAPayPerson("31000000415", contNo);
    }
}
