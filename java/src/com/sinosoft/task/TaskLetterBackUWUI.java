package com.sinosoft.task;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LGLetterSchema;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *    响应对函件的操作
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskLetterBackUWUI {
    /**
     * 错误的容器
     * */
    public CErrors mErrors = new CErrors();

    public TaskLetterBackUWUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        TaskLetterBackUWBL tTaskLetterBackUWBL = new TaskLetterBackUWBL();

        if(!tTaskLetterBackUWBL.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(tTaskLetterBackUWBL.mErrors);

            return false;
        }

        return true;
    }

    public static void main(String args[])
    {
        LGLetterSchema s = new LGLetterSchema();
        GlobalInput g = new GlobalInput();

        g.Operator = "endor";
        g.ManageCom = "86";

        s.setEdorAcceptNo("20060720000018");
        s.setSerialNumber("1");
        s.setBackFlag("1");
        s.setBackDate("2006-10-31");
        s.setFeedBackInfo("ForceFeedBack");

        LPUWMasterSet tLPUWMasterSet = new LPUWMasterSet();
        LPUWMasterSchema tLPUWMasterSchema = new LPUWMasterSchema();
        tLPUWMasterSchema.setEdorNo("20060720000018");
        tLPUWMasterSchema.setPolNo("21000065899");
        tLPUWMasterSchema.setEdorType("BF");
        tLPUWMasterSchema.setCustomerReply("2");
        tLPUWMasterSet.add(tLPUWMasterSchema);

        VData v = new VData();
        v.add(g);
        v.add(s);
        v.add(tLPUWMasterSet);
        TaskLetterBackUWUI ui = new TaskLetterBackUWUI();
        if(!ui.submitData(v, "ForceFeedBack"))
        {
            System.out.println(ui.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
