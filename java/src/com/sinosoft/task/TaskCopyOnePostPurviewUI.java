package com.sinosoft.task;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class TaskCopyOnePostPurviewUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    public TaskCopyOnePostPurviewUI()
    {
    }

    /**
     * 提交数据的方法
     * @param cInputData VData,需包含LGProjectPurviewSet 和 GlobalInput
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        TaskCopyOnePostPurviewBL bl = new TaskCopyOnePostPurviewBL();

        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    /**
      * 返回处理后的map
      * @param cInputData VData
      * @param cOperate String
      * @return MMap
      */
    public MMap getDealtMap(VData cInputData, String cOperate)
    {
        TaskCopyOnePostPurviewBL bl = new TaskCopyOnePostPurviewBL();
        return bl.getDealtMap(cInputData, cOperate);
    }

    public static void main(String[] args)
    {
        TaskCopyOnePostPurviewUI taskcopyonepostpurviewui = new
                TaskCopyOnePostPurviewUI();
    }
}
