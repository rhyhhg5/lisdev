package com.sinosoft.task;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LGProjectPurviewSchema;
import com.sinosoft.lis.vschema.LGProjectPurviewSet;
import com.sinosoft.task.BuildError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LGProjectPurviewDB;
import com.sinosoft.lis.db.LGGroupMemberDB;
import com.sinosoft.lis.vschema.LGGroupMemberSet;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Yang Yalin
 * @version 1.0
 */
public class TaskPostPurviewBL
{
    /**错误保存容器*/
    public CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 输出数据的容器
    private VData mResult = new VData();

    // 数据操作字符串
    private String mOperate;

    private MMap map = new MMap();

    // 全局参数
    private GlobalInput mGlobalInput = new GlobalInput();

    //统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    LGProjectPurviewSet mLGProjectPurviewSet = new LGProjectPurviewSet();

    public TaskPostPurviewBL()
    {
    }

    /**
  * Appeal对象提交数据函数
  * 输入：数据集合VData， 操作方式String
  * 输出：如果准备数据时发生错误则返回false,否则返回true
  */
   public boolean submitData(VData cInputData, String cOperate)
   {
       // 将传入的数据拷贝到本类中
       this.mOperate = cOperate;

       if(!getInputData(cInputData))
       {
           return false;
       }

       if(!prepareData())
       {
           return false;
       }

       CheckPurviewData checkor = new CheckPurviewData();
       if(!checkor.submitData(cInputData, cOperate))
       {
           mErrors.copyAllErrors(checkor.mErrors);
           return false;
       }

       if(!dealData())
       {
           return false;
       }

       if(!prepareOutputData())
       {
           return false;
       }

       //如果是新建权限,则给相应岗位的成员分配分配权限
       if(mOperate.equals("INSERT||MAIN"))
       {
           TaskCopyOnePostPurviewUI ui = new TaskCopyOnePostPurviewUI();
           MMap m = ui.getDealtMap(this.mResult, "INSERT||MAIN");
           if (m == null)
           {
               mErrors.copyAllErrors(ui.mErrors);
               return false;
           }

           map.add(m);
       }

       PubSubmit tPubsubmit = new PubSubmit();
       if(!tPubsubmit.submitData(mInputData, mOperate))
       {
           this.mErrors.copyAllErrors(tPubsubmit.mErrors);

           return false;
       }

       return true;
   }

   //获得从jsp页面传入的数据
   public boolean getInputData(VData inputData)
   {
       try
       {
           mGlobalInput.setSchema((GlobalInput) inputData.
                                  getObjectByObjectName("GlobalInput", 0));
           mLGProjectPurviewSet = (LGProjectPurviewSet) inputData.
                                  getObjectByObjectName(
                                          "LGProjectPurviewSet", 0);
       }
       catch(Exception e)
       {
           mErrors.addOneError(BuildError.create("TaskAddPostPurviewBL",
                   "getInputData", "没有获得从Save.jsp传入的数据-->" + e.toString()));

           return false;
       }

       return true;
   }

   /**
    * 为处理数据准备数据
    * @return boolean
    */
   private boolean prepareData()
   {
       if (this.mOperate.equals("INSERT||MAIN"))
       {
           for (int i = 1; i <= mLGProjectPurviewSet.size(); i++)
           {
               //若为录入客户号则为岗位级别上的权限处理
               String memberNo = mLGProjectPurviewSet.get(i).getMemberNo();
               if(memberNo == null || memberNo.equals("")
                  || memberNo.equals("00000000000000000000"))
               {
                   mLGProjectPurviewSet.get(i).setMemberNo("00000000000000000000");

                   String postNo = mLGProjectPurviewSet.get(i).getPostNo();
                   if(postNo == null || postNo.equals(""))
                   {
                       mErrors.addOneError("请录入岗位级别");
                       return false;
                   }
               }
               else
               {
                   //给权限加上处理机构类型
                   LGGroupMemberDB db = new LGGroupMemberDB();
                   db.setMemberNo(memberNo);
                   LGGroupMemberSet set = db.query();
                   if(set == null || set.size() == 0)
                   {
                       mErrors.addOneError("成员还未分配给任何小组");
                       return false;
                   }
                   String postNo = set.get(1).getpostNo();
                   if(postNo == null || postNo.equals(""))
                   {
                       mErrors.addOneError("成员还没有分配岗位,先分配岗位");
                       return false;
                   }
                   mLGProjectPurviewSet.get(i).setPostNo(postNo);
               }

               if (mLGProjectPurviewSet.get(i).getPostNo().compareTo("PA03") <= 0)
               {
                   mLGProjectPurviewSet.get(i).setDealOrganization("0"); //总公司
               }
               else
               {
                   mLGProjectPurviewSet.get(i).setDealOrganization("1"); //分公司
               }
           }
       }
       else if (mOperate.equals("UPDATE||MAIN"))
       {
           LGProjectPurviewDB db = new LGProjectPurviewDB();
           db.setProjectPurviewNo(mLGProjectPurviewSet.get(1).getProjectPurviewNo());
           if (db.getInfo())
           {
               mLGProjectPurviewSet.get(1).setProjectNo(db.getProjectNo());
               mLGProjectPurviewSet.get(1).setProjectType(db.getProjectType());
               mLGProjectPurviewSet.get(1).setDealOrganization(db.
                       getDealOrganization());
               mLGProjectPurviewSet.get(1).setPostNo(db.getPostNo());
               mLGProjectPurviewSet.get(1).setMemberNo(db.getMemberNo());
           }
       }

       return true;
   }

   //处理数据,为入库作准备
   public boolean dealData()
   {
//       SSRS tSSRS = new SSRS();
//       ExeSQL tExeSQL = new ExeSQL();
       String sql;
//       int maxProjectPurviewNo;
//
//       //查询最大项目岗位权限编号
//       sql = "select max(int(ProjectPurviewNo)) "
//             + "from LGProjectPurview ";
//       tSSRS = tExeSQL.execSQL(sql);
//
//       if(tSSRS.getMaxRow() == 1)
//       {
//           maxProjectPurviewNo = Integer.parseInt(tSSRS.GetText(1, 1));
//       }
//       else
//       {
//           mErrors.addOneError (BuildError.create("TaskAddPostPurViewBL",
//                   "dealData", "查询最大项目权限编号时出错"));
//           System.out.println("dealData中查询最大项目权限编号时出错");
//
//           return false;
//       }

       //新建
       if(mOperate.equals("INSERT||MAIN"))
       {
           for (int i = 0; i < mLGProjectPurviewSet.size(); i++)
           {

//               mLGProjectPurviewSet.get(i + 1).setProjectPurviewNo(
//                       new String(Integer.toString(maxProjectPurviewNo + 1 + i)));
               mLGProjectPurviewSet.get(i + 1).setProjectPurviewNo(PubFun1.CreateMaxNo("TASKPURVIEW", null));
               mLGProjectPurviewSet.get(i + 1).setStateFlag("1");
               mLGProjectPurviewSet.get(i + 1).setOperator(mGlobalInput.Operator);
               mLGProjectPurviewSet.get(i + 1).setMakeDate(mCurrentDate);
               mLGProjectPurviewSet.get(i + 1).setMakeTime(mCurrentTime);
               mLGProjectPurviewSet.get(i + 1).setModifyDate(mCurrentDate);
               mLGProjectPurviewSet.get(i + 1).setModifyTime(mCurrentTime);
           }
           map.put(mLGProjectPurviewSet, "INSERT"); //插入
       }
       else if(mOperate.equals("UPDATE||MAIN"))
       {
           String condition;
           if (mLGProjectPurviewSet.get(1).getMemberNo()
               .equals("00000000000000000000"))
           {
               LGProjectPurviewDB db = new LGProjectPurviewDB();
               db.setProjectPurviewNo(mLGProjectPurviewSet.get(1)
                                      .getProjectPurviewNo());
               if (!db.getInfo())
               {
                   mErrors.addOneError("查询权限信息失败。");
                   return false;
               }
               condition = "where projectNo = '" + db.getProjectNo()
                           + "'  and projectType ='" + db.getProjectType()
                           + "'  and postNo = '" + db.getPostNo() + "' "
                           + "   and (stateflag!='2' or stateflag is null)  ";
               if(db.getRiskCode()==null||"".equals(db.getRiskCode())){
            	   condition += " and (riskcode is null or riskcode='' )";
               }else{
            	   condition += " and riskcode = '"+db.getRiskCode()+"' ";
               }
           }
           else
           {
               condition = "where ProjectPurviewNo='"
                           + mLGProjectPurviewSet.get(1).getProjectPurviewNo()
                           + "' ";
           }
           sql = "update LGProjectPurview "
                 + "set MoneyPurview="
                 + mLGProjectPurviewSet.get(1).getMoneyPurview() + ", "
                 + "ExcessPurview="
                 + mLGProjectPurviewSet.get(1).getExcessPurview() + ", "
                 + "TimePurview='"
                 + mLGProjectPurviewSet.get(1).getTimePurview() + "', "
                 + "NeedOtherAudit='"
                 + mLGProjectPurviewSet.get(1).getNeedOtherAudit() + "', "
                 + "Remark='" + mLGProjectPurviewSet.get(1).getRemark() + "', "
                 + "ConfirmFlag='"
                 + mLGProjectPurviewSet.get(1).getConfirmFlag() + "', "
                 + "Operator = '" + mGlobalInput.Operator + "', "
                 + "modifyDate = '" + this.mCurrentDate + "', "
                 + "modifyTime = '" + this.mCurrentTime + "' "
                 + condition;
           map.put(sql, "UPDATE");
       }
       else if(mOperate.equals("DELETE||MAIN"))
       {
           for (int i = 1; i <= mLGProjectPurviewSet.size(); i++)
           {
               LGProjectPurviewDB db = new LGProjectPurviewDB();
               db.setProjectPurviewNo(mLGProjectPurviewSet.get(1)
                                      .getProjectPurviewNo());
               if (!db.getInfo())
               {
                   mErrors.addOneError("查询权限信息失败。");
                   return false;
               }
               String condition;
               if("00000000000000000000".equals(db.getMemberNo())){
                   condition = "where projectNo = '" + db.getProjectNo()
                   + "'  and projectType ='" + db.getProjectType()
                   + "'  and postNo = '" + db.getPostNo() + "' "
                   + "    and (stateflag!='2' or stateflag is null)   ";
			       if(db.getRiskCode()==null||"".equals(db.getRiskCode())){
			    	   condition += " and (riskcode is null or riskcode='' )";
			       }else{
			    	   condition += " and riskcode = '"+db.getRiskCode()+"' ";
			       }
			     }
			     else
			     {
			         condition = "where ProjectPurviewNo='"
			                     + mLGProjectPurviewSet.get(1).getProjectPurviewNo()
			                     + "' ";
			     }
        	   String deleteSQL  = " update LGProjectPurview set stateflag='2', " 
        		   + "Operator = '" + mGlobalInput.Operator + "', "
        		   + "modifyDate = '" + this.mCurrentDate + "', "
        		   + "modifyTime = '" + this.mCurrentTime + "' " 
//        		   + "where ProjectPurviewNo =  '"+mLGProjectPurviewSet.get(i).getProjectPurviewNo()+"' ";
        		   + condition;
        	   map.put(deleteSQL, "UPDATE");
           }
       }

       return true;
   }

   //准备交给后台处理的数据
   public boolean prepareOutputData()
   {
       try
        {
            //给后台的数据
            mInputData.clear();
            mInputData.add(this.map);

            //反馈给jsp页面的信息
            mResult.clear();
            mResult.add(mGlobalInput);
            mResult.add(this.mLGProjectPurviewSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            mErrors.addOneError (BuildError.create("TaskAddPostPurViewBL",
                    "prepareOutputData", "准备输往后台数据时出错" + ex.toString()));
            System.out.println("...prepareOutputData中准备输往后台数据时出错");

            return false;
        }

        return true;

   }

   public VData getResult()
   {
       return mResult;
   }

   public static void main(String a[])
   {
       GlobalInput tG = new GlobalInput();
       tG.Operator = "endor";

       LGProjectPurviewSchema tLGProjectPurviewSchema;
       LGProjectPurviewSet set = new LGProjectPurviewSet();

       tLGProjectPurviewSchema = new LGProjectPurviewSchema();

       tLGProjectPurviewSchema.setProjectPurviewNo("1");
       tLGProjectPurviewSchema.setProjectNo("WT");
       tLGProjectPurviewSchema.setProjectType("0");
       tLGProjectPurviewSchema.setPostNo("PA02");
       tLGProjectPurviewSchema.setMoneyPurview("100000");
       tLGProjectPurviewSchema.setExcessPurview("10000");
       tLGProjectPurviewSchema.setTimePurview("100");
       tLGProjectPurviewSchema.setRemark("可以删除");
       tLGProjectPurviewSchema.setNeedOtherAudit("0");
       tLGProjectPurviewSchema.setConfirmFlag("2");
       tLGProjectPurviewSchema.setMemberNo("");

       set.add(tLGProjectPurviewSchema);

       // 准备传输数据 VData
       VData tVData = new VData();
       tVData.add(set);
       tVData.add(tG);

       TaskPostPurviewBL tTaskPostPurviewBL = new TaskPostPurviewBL();
       if (tTaskPostPurviewBL.submitData(tVData, "UPDATE||MAIN") == false)
       {
           System.out.println(tTaskPostPurviewBL.mErrors.getErrContent());
       }

   }

}



