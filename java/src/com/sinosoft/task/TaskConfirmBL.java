package com.sinosoft.task;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class TaskConfirmBL
{
    /**错误保存容器*/
    public CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 输出数据的容器
    private VData mResult = new VData();

    // 数据操作字符串
    private String mOperate;

    private MMap map = new MMap();

    // 全局参数
    private GlobalInput mGlobalInput = new GlobalInput();

    //统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    private LGWorkSchema mLGWorkSchema = new LGWorkSchema();
    private LGTraceNodeOpSchema mLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
    
    private String mConfirmFlag = "99";
    
    private String mRemark = null;

    public TaskConfirmBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;

        if (!getInputData(cInputData) || !dealData() || !prepareOutputData())
       {
           return false;
       }

       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(mInputData, mOperate))
       {
           this.mErrors.addOneError(
                   new CError("数据保存失败", "TaskConfirmBL", "submitData"));

           return false;
       }

       return true;

    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLGWorkSchema = (LGWorkSchema) cInputData.
                             getObjectByObjectName("LGWorkSchema", 0);
        this.mLGTraceNodeOpSchema = (LGTraceNodeOpSchema) cInputData.
                               getObjectByObjectName("LGTraceNodeOpSchema", 0);
        TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        mConfirmFlag = (String)tTransferData.getValueByName("ConfirmFlag");
        if(mConfirmFlag.equals("99"))
        {
        	mConfirmFlag = Task.WORKSTATUS_CONFIRMPASS;
        	mRemark = "同意。";
        }
        else
        {
        	mConfirmFlag = Task.WORKSTATUS_CONFIRMFAIL;
        	mRemark = "不同意。";
        }

        return true;
    }


    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        String sql;
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("remark", mLGTraceNodeOpSchema.getRemark());

        VData v = new VData();
        TaskAutoExaminationBL mTaskAutoExaminationBL = new
                TaskAutoExaminationBL();

        v.add(this.mLGWorkSchema);
        v.add(this.mGlobalInput);
        v.add(tTransferData);

        //首先判断是否有权限进行审批，若无则自动送审批
        if (mConfirmFlag.equals("99")&&!mTaskAutoExaminationBL.submitData(v, ""))
        {
            //送给上级审批
            if(mTaskAutoExaminationBL.sendConfirmSuccess())
            {
                this.mErrors.copyAllErrors(
                    mTaskAutoExaminationBL.getSendConfirmReason());
            }
            //审批程序运行失败
            else
            {
                this.mErrors.copyAllErrors(mTaskAutoExaminationBL.mErrors);
            }
            return false;
        }

        //若审批通过，则置工单状态为审批通过：99、若审批不通过，则置工单状态为：88
        sql = "update LGWork "
              + "set statusNo='" + mConfirmFlag + "', "
              + "   operator = '" + mGlobalInput.Operator + "', "
              + "   modifyDate = '" + this.mCurrentDate + "', "
              + "   modifyTime = '" + this.mCurrentTime + "' "
              + "where workNo='" + this.mLGWorkSchema.getWorkNo() + "' ";
        map.put(sql, "UPDATE");

        //回送工单到最初的送审者
        String []tWorkNo = new String[1];  //当前工单号
        tWorkNo[0] = mLGWorkSchema.getWorkNo();

        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        String s = "select workBoxNo "
                   + "from LGWorkTrace "
                   + "where workNo='" + this.mLGWorkSchema.getWorkNo() + "' "
                   + "    and nodeNo= "
                   + "        (select char(min(int(nodeNo)) - 1) "
                   + "        from LGTraceNodeOp "
                   + "        where workNo='" + this.mLGWorkSchema.getWorkNo() + "' "
                   + "            and operatorType='15') ";
        tSSRS = tExeSQL.execSQL(s);

        if (tSSRS.getMaxRow() == 0)
        {
            this.mErrors.addOneError(
                    new CError("查询最初送审批者失败", "TaskConFirmBL", "dealData"));
            System.out.println("TaskConFirmBL的dealData方法中查询最初送审批者失败");

            return false;
        }

        tLGWorkTraceSchema.setWorkBoxNo(tSSRS.GetText(1, 1));

        tLGTraceNodeOpSchema.setWorkNo(mLGWorkSchema.getWorkNo());
        tLGTraceNodeOpSchema.setOperatorType("6");
        tLGTraceNodeOpSchema.setRemark(mRemark+this.mLGTraceNodeOpSchema.getRemark());
        tLGTraceNodeOpSchema.setFinishDate(this.mCurrentDate);
        tLGTraceNodeOpSchema.setFinishTime(this.mCurrentTime);

        VData tVData = new VData();
        tVData.add(this.mGlobalInput);
        tVData.add(tWorkNo);
        tVData.add("N");
        tVData.add("0");
        tVData.add(tLGWorkSchema);
        tVData.add(tLGWorkTraceSchema);
        tVData.add(tLGTraceNodeOpSchema);

        //调用工单转交程序
        TaskDeliverBL tTaskDeliverBL = new TaskDeliverBL();
        if (tTaskDeliverBL.submitData(tVData, "ok") == false)
        {
            this.mErrors.copyAllErrors(tTaskDeliverBL.mErrors);
            System.out.println("回送工单到最初的送审者时出错");

            return false;
        }

        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.map);
        }
        catch (Exception ex)
        {
            this.mErrors.addOneError(BuildError.create("TaskConfirmBL",
                    "prepareOutputData", "准备输往后台的数据时出错"));

            return false;
        }

        return true;
    }

    public static void main(String args[])
    {
        GlobalInput gi = new GlobalInput();
        LGWorkSchema w = new LGWorkSchema();
        VData v = new VData();
        TaskConfirmBL c = new TaskConfirmBL();
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        gi.Operator = "endor1";
        gi.ComCode = "86";
        v.add(gi);

        w.setWorkNo("20050725000070");
        v.add(w);

        tLGTraceNodeOpSchema.setRemark("审批通过");
        v.add(tLGTraceNodeOpSchema);

        if(!c.submitData(v, ""))
        {
            if(c.mErrors.needDealError())
            {
                System.out.println("在" + c.mErrors.getError(0).moduleName
                                   + "的方法" + c.mErrors.getError(0).functionName
                                   + "中发生错误: " +
                                   c.mErrors.getError(0).errorMessage);
            }
            else
            {
                System.out.println("审批未通过");
            }
        }

        else
        {
            System.out.println("审批通过");
        }

    }
}






