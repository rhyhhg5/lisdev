package com.sinosoft.task;

import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

public class TaskAllocationBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LGWorkTraceSchema mLGWorkTraceSchema = new LGWorkTraceSchema();

    /**需要分配的工单号*/
    private String[] workNoList;

    /**分配到的小组成员编号*/
    private String[] memberNoList;

    private TransferData mTransferData;

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    //作业坐在当前信箱号
    private String tWorkBoxNo = "";

    //分配结果
    private String[][] tAllocationResult;

    SSRS tSSRS = new SSRS();
    SSRS iSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();

    public TaskAllocationBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate){
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in TaskAllocationBL submit");
        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
        {
            return false;
        }
        System.out.println("---getInputData---");

        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false)
        {
            return false;
        }
        System.out.println("---dealDate---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start TaskAllocationBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskAllocationBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        this.mGlobalInput.setSchema((GlobalInput) mInputData.
                getObjectByObjectName("GlobalInput", 0));
        this.mTransferData = (TransferData)mInputData.
                        getObjectByObjectName("TransferData",0);
        this.workNoList = (String[])mTransferData.getValueByName("workNoList");
        this.memberNoList = (String[])mTransferData.getValueByName("memberNoList");
        this.tAllocationResult = new String[workNoList.length][2];
        for(int i = 0; i<workNoList.length; i++) {
             System.out.println(workNoList[i]);
        }

        for(int i = 0; i<memberNoList.length; i++) {
            System.out.println(memberNoList[i]);
        }
        return true;
    }

    private boolean dealData() {
        //当工单数与人数相同或者工单数小于人数时使用方法operateA()
        if(workNoList.length == memberNoList.length || workNoList.length < memberNoList.length) {
            System.out.println("使用方法operateA()");
            this.operateA();
        }
        //当工单数大于人数时使用方法operateB()
        if(workNoList.length > memberNoList.length) {
            System.out.println("使用方法operateB()");
            this.operateB();
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }

    private void operateA() {
        for(int i = 0; i < this.workNoList.length; i++) {
            this.allocationWork(this.memberNoList[i],this.workNoList[i]);
            this.tAllocationResult[i][0] = this.memberNoList[i];
            this.tAllocationResult[i][1] = this.workNoList[i];
            System.out.println("工单"+this.tAllocationResult[i][1]+"分配给组员"+this.tAllocationResult[i][0]);
        }
    }

    private void operateB() {
        for(int i = 0; i < this.workNoList.length; i++) {
            int j = i%memberNoList.length;
            this.allocationWork(this.memberNoList[j],this.workNoList[i]);
            this.tAllocationResult[i][0] = this.memberNoList[j];
            this.tAllocationResult[i][1] = this.workNoList[i];
            System.out.println("工单"+this.tAllocationResult[i][1]+"分配给组员"+this.tAllocationResult[i][0]);
        }
    }

    private String queryWorkBoxNo(String memberNo) {
        String sqlTemp;
        sqlTemp =
            "Select WorkBoxNo " +
            "From   LGWorkBox " +
            "Where  OwnerTypeNo = '2' " + //个人信箱
            "And    OwnerNo = '" + memberNo + "' ";
        tSSRS = tExeSQL.execSQL(sqlTemp);
        if(tSSRS.getMaxRow() == 1)
        {
            tWorkBoxNo = tSSRS.GetText(1, 1);
        }
        else
        {
            System.out.println("TaskALLocationBL--deasData中查询信箱出错。");
            return null;
        }

        return tWorkBoxNo;
    }


    private boolean allocationWork(String memberNo,String workNo) {
        System.out.println("组员号："+memberNo);
        System.out.println("工单号"+workNo);
        String workBoxNo = this.queryWorkBoxNo(memberNo);
        if(workBoxNo == null) return false;
        String nodeSQL = "Select Case When max(to_number(NodeNo)) Is Null " +
                  "       Then 0 Else max(to_number(NodeNo))+1 End " +
                  "From   LGWorkTrace " +
                  "Where  WorkNo = '" + workNo + "'";
        iSSRS = tExeSQL.execSQL(nodeSQL);
        String nodeNo = iSSRS.GetText(1, 1);

        LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();
            tLGWorkTraceSchema.setWorkNo(workNo);
            tLGWorkTraceSchema.setNodeNo(nodeNo);
            tLGWorkTraceSchema.setWorkBoxNo(workBoxNo);
            tLGWorkTraceSchema.setInMethodNo("5"); //组长分配
            tLGWorkTraceSchema.setInDate(mCurrentDate);
            tLGWorkTraceSchema.setInTime(mCurrentTime);
            tLGWorkTraceSchema.setSendComNo(mGlobalInput.ManageCom);
            tLGWorkTraceSchema.setSendPersonNo(mGlobalInput.Operator);
            tLGWorkTraceSchema.setOperator(mGlobalInput.Operator);
            tLGWorkTraceSchema.setMakeDate(mCurrentDate);
            tLGWorkTraceSchema.setMakeTime(mCurrentTime);
            tLGWorkTraceSchema.setModifyDate(mCurrentDate);
            tLGWorkTraceSchema.setModifyTime(mCurrentTime);
            map.put(tLGWorkTraceSchema, "INSERT"); //插入

            //设置数据
            String lgWorkSQl = "Update LGWork set " +
                  "NodeNo = '" + nodeNo + "', " +
                  "Operator = '" + mGlobalInput.Operator + "', " +
                  "ModifyDate = '" + mCurrentDate + "', " +
                  "ModifyTime = '" + mCurrentTime + "' " +
                  "Where  WorkNo = '" + workNo + "' ";
            map.put(lgWorkSQl, "UPDATE");

        return true;
    }

    public String[][] getTAllocationResult() {
        return this.tAllocationResult;
    }
}





























