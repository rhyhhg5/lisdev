package com.sinosoft.task;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 一些常用的方法
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class CommonBL
{
    /**
     *  错误的容器
     */
    public CErrors mErrors = new CErrors();

    public CommonBL()
    {
    }


    /**
     * 查询tableName表的fieldName字段的最大值
     * @param tableName String
     * @param fieldName String
     * @return String
     */
    public String getMaxNo(String tableName, String fieldName)
    {
        String maxNo = "0";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        //查询最大项目岗位权限编号
        String sql = "select max(int(" + fieldName + ")) "
                     + "from " + tableName;
        try
        {
            tSSRS = tExeSQL.execSQL(sql);

            if (tSSRS.getMaxRow() == 1)
            {
                maxNo = tSSRS.GetText(1, 1);
            }
            else
            {
                maxNo = "1";
            }
        }
        catch (Exception e)
        {
            mErrors.addOneError(e.toString());
            e.printStackTrace();
        }

        return maxNo;
    }

    /**
     * 查询tableName表的fieldName字段的最大值
     * @param tableName String
     * @param fieldName String
     * @return long
     */
    public long getMaxNoLong(String tableName, String fieldName)
    {
        long maxNo = 0;
        try
        {
            maxNo = Long.parseLong(getMaxNo(tableName, fieldName));
        }
        catch(Exception e)
        {
            mErrors.addOneError("没有查询到相应的数据");
            e.printStackTrace();
        }

        return maxNo;
    }

    /**
     * 查询tableName表的fieldName字段的 最大值+1
     * @param tableName String
     * @param fieldName String
     * @return String
     */
    public String getNextNo(String tableName, String fieldName)
    {
        return String.valueOf(getNextNoLong(tableName, fieldName));
    }

    /**
     * 查询tableName表的fieldName字段的 最大值+1
     * @param tableName String
     * @param fieldName String
     * @return long
     */
    public long getNextNoLong(String tableName, String fieldName)
    {
        return getMaxNoLong(tableName, fieldName) + 1;
    }

    public static String getCustomerName(String customerNo)
    {
        if(customerNo.length() == 9)
        {
            LDPersonDB db = new LDPersonDB();
            db.setCustomerNo(customerNo);
            if (!db.getInfo())
            {
                return "+-1";
            }
            return db.getName();
        }
        else if(customerNo.length() == 8)
        {
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(customerNo);
            if(!tLDGrpDB.getInfo())
            {
                return "+-1";
            }
            return tLDGrpDB.getGrpName();
        }
        else
        {
            return "+-1";
        }
    }

    /**
     * 得到错误信息，不包含错误编码
     * @param tCErrors CErrors
     * @return String
     */
    public static final String getErrContent(CErrors tCErrors)
    {
        StringBuffer errContent = new StringBuffer("发生以下错误：\n");
        for(int i = 0; i < tCErrors.getErrorCount(); i++)
        {
            errContent.append("   " + (i + 1) + "："
                              + tCErrors.getError(i).errorMessage + "\n");
        }
        return errContent.toString();
    }

    /**
     * 生成工单号
     * @return String
     */
    public static final String createWorkNo()
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        String tWorkNo = tDate + PubFun1.CreateMaxNo("TASK" + tDate, 6);
        return tWorkNo;
    }

    /**
     * 根据工单号得到工单信息
     * @param workNo String
     * @return LGWorkSchema
     */
    public static final LGWorkSchema getLGWorkInfo(String workNo)
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(workNo);
        tLGWorkDB.getInfo();
        return tLGWorkDB.getSchema();
    }

    /**
     * 得到自动转交信箱
     * 保单管理机构所对应的8位机构编码分配规则，若无信箱则取b规则。
     * 保单管理机构所对应的4位机构编码分配规则，若无信箱则取c规则。
     * 保单管理机构所对应2位机构编码分配规则，若无信箱则返回null。
     * @param manageCom String：8位机构代码
     * @param goalType String：信箱类型
     * @param ruleContent String：规则内容，若非扫描则可为空
     * @return String：信箱，若没有查询到信箱返回null
     */

    public static final String getAutodeliverTarget(String manageCom,
        String goalType,
        String ruleContent)
    {
        if(manageCom == null || manageCom.equals("")
           || goalType == null || goalType.equals(""))
        {
            return null;
        }

        String manageCom8 = manageCom;
        String manageCom4 = manageCom8.substring(0, 4);
        String manageCom2 = manageCom8.substring(0, 2);
        ExeSQL tExeSQL = new ExeSQL();

        if(goalType.equals("1"))
        {
            ruleContent = "   and ruleContent = '" + ruleContent + "' ";
        }
        else
        {
            ruleContent = " ";
        }

        //查8位规则
        String sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom8 + "' "
                     + "   and goalType = '" + goalType + "' "
                     + "   and defaultFlag = '1' "
                     + ruleContent;
        String target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        //查4位规则
        sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom4 + "' "
                     + "   and goalType = '" + goalType + "' "
                     + "   and defaultFlag = '1' "
                     + ruleContent;
        target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        ////查2位规则
        sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom2 + "' "
                     + "   and goalType = '" + goalType + "' "
                     + "   and defaultFlag = '1' "
                     + ruleContent;
        target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        return null;
    }


    public static void main(String[] args)
    {
        CommonBL bl = new CommonBL();
        String no = bl.getMaxNo ("LGProjectPurview", "ProjectPurviewNo");
        if(bl.mErrors.needDealError())
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println(no);
        }
    }
}
