package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LGLetterSchema;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LGLetterDB;
import com.sinosoft.lis.vschema.LGLetterSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: 校验函件信息的正确性</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CheckLetterData
{
    public CErrors mErrors = new CErrors();
    private LGLetterSchema mLGLetterSchema = new LGLetterSchema();

    private GlobalInput mGlobalInput = new GlobalInput();
    private String mOperate = "";

    public CheckLetterData()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Beginning of submitData in CheckLetterData");
        mOperate = cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }

        //校验本次生成的数据是否符合逻辑
        if(mOperate.equals("INSERT||MAIN") && !checkSaveData())
        {
            return false;
        }

        //校验本次生成的数据和数据库中的数据是否冲突
        if(!checkDBData())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData v)
    {
        try
        {
            this.mLGLetterSchema = (LGLetterSchema) v.
                                   getObjectByObjectName("LGLetterSchema", 0);
            this.mGlobalInput.setSchema((GlobalInput) v.
                                        getObjectByObjectName("GlobalInput", 0));
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入后台的数据不完整" + e.toString());

            return false;
        }

        return true;
    }

    //校验保存函件时生成的数据是否符合逻辑
    private boolean checkSaveData()
    {
        System.out.println("Beginning fo checkInputData");
        boolean flag = true;

        if (mLGLetterSchema.getSendObj() == null
                || mLGLetterSchema.getSendObj().equals(""))
        {
            mErrors.addOneError("下发对象不能为空");
            flag = false;
        }
        else if (mLGLetterSchema.getSendObj().equals("0")
                 && (mLGLetterSchema.getAddressNo() == null
                     || mLGLetterSchema.getAddressNo().equals("")))
        {
            mErrors.addOneError("下发对象为投保人，请选择地址");
            flag = false;
        }
        else if(mLGLetterSchema.getLetterType() != null
                && mLGLetterSchema.getLetterType().equals("1")
                && (mLGLetterSchema.getLetterSubType() == null
                    || mLGLetterSchema.getLetterSubType().equals("")))
        {
            mErrors.addOneError("请为新建的函件选择函件类型");
            flag = false;
        }
        //else if(mLGLetterSchema.getLetterInfo() == null
        //        || mLGLetterSchema.getLetterInfo().equals(""))
       // {
        //    mErrors.addOneError("您还没有输入函件信息");
       //     flag = false;
       // }

        //若无回销与否标记,暂存1:需回销
        if(mLGLetterSchema.getBackFlag() == null
           || mLGLetterSchema.getBackFlag().equals(""))
        {
            mLGLetterSchema.setBackFlag("1");
        }

        //若录入回销时间，则不得早于今天
        if(mLGLetterSchema.getBackFlag().equals("1")
           && mLGLetterSchema.getBackDate() != null
           && !mLGLetterSchema.getAddressNo().equals(""))
         {
             String sql = "select userCode "
                          + "from LDUser "
                          + "where days('" + PubFun.getCurrentDate() + "') - days('"
                          + mLGLetterSchema.getBackDate() + "') > 0 ";
             ExeSQL e = new ExeSQL();
             String temp = e.getOneValue(sql);
             if(temp != null && !temp.equals(""))
             {
                 mErrors.addOneError("函件回销日期不能早于今天，请确认。");
                 return false;
             }
         }

        return flag;
    }

    //校验本次生成的数据和数据库中的数据是否冲突
    private boolean checkDBData()
    {
        if(mLGLetterSchema.getLetterType().equals("0"))
         {
             return true;
         }

        LGLetterDB db = new LGLetterDB();

        String sql = "select * "
                     + "from LGLetter "
                     + "where edorAcceptNo='"
                     + mLGLetterSchema.getEdorAcceptNo() + "' "
                     + "    and state not in ('3', '4', '5') " //回销、强制回销、已完成
                     + "order by int(serialNumber) ";
        LGLetterSet set = db.executeQuery(sql);

        if(set != null && set.size() != 0)
        {
            String errMsg = "";
            for(int i = 1; i <=set.size(); i++)
            {
                errMsg += set.get(i).getSerialNumber() + " ";
            }
            mErrors.addOneError("还有序号为 " + errMsg + "的函件未处理完毕，不能新建函件");

            return false;
        }



        return true;
    }

    public static void main(String args[])
    {
        VData v = new VData();

        LGLetterSchema s = new LGLetterSchema();
        s.setEdorAcceptNo("20050825000002");
        s.setSendObj("1");
        s.setLetterType("1");
        s.setLetterSubType("1");
        s.setAddressNo("1");
        s.setBackFlag("1");
        s.setBackDate("2005-08-30");
        v.add(s);

        GlobalInput g = new GlobalInput();
        v.add(g);

        CheckLetterData cld = new CheckLetterData();
        if(!cld.submitData(v, ""))
        {
            System.out.println(cld.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }

    }
}
