package com.sinosoft.task;

import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 作业合并BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-03-09
 */

public class TaskUniteBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private String[] mWorkNo = null;

    private String mAcceptNo = null;

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LGWorkSchema tLGWorkSchema = new LGWorkSchema();

    private String remark = "";

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    public TaskUniteBL()
    {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in TaskUniteBL submit");
        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
        {
            return false;
        }
        System.out.println("---getInputData---");

        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false)
        {
            return false;
        }
        System.out.println("---dealDate---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskUniteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            String nodeNo = "";
            String operatorNo = "";
            LGTraceNodeOpSchema mLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
            TaskTraceNodeOpBL tTraceNodeOpBL = new TaskTraceNodeOpBL();

            //获得信箱节点
            String s = "select nodeNo "
                       + "from LGWork "
                       + "where workNo='" + tLGWorkSchema.getWorkNo() + "'";
            try
            {
                tSSRS = tExeSQL.execSQL(s);
                if(tSSRS.getMaxRow() == 1)
                {
                    nodeNo = tSSRS.GetText(1, 1);
                }
            }
            catch(Exception e)
            {
                System.out.println("查询作业信箱节点时出现错误：" + e.toString());
            }

            mLGTraceNodeOpSchema.setWorkNo(tLGWorkSchema.getWorkNo());
            mLGTraceNodeOpSchema.setNodeNo(nodeNo);
            mLGTraceNodeOpSchema.setOperatorType("11");
            mLGTraceNodeOpSchema.setRemark("自动批注：受理件合并");
            mLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
            mLGTraceNodeOpSchema.setFinishTime(mCurrentTime);

            VData v = new VData();
            v.add(mGlobalInput);
            v.add(mLGTraceNodeOpSchema);
            if(!tTraceNodeOpBL.submitData(v, "INSERT||MAIN"))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "TaskUniteBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        tLGWorkSchema.setSchema((LGWorkSchema) mInputData.getObjectByObjectName("LGWorkSchema", 0));
        mWorkNo = (String[]) mInputData.getObject(2);
        remark = (String) mInputData.getObject(3);
        //mWorkNo = (String[]) mInputData.getObject(1);
        //mAcceptNo = (String) mInputData.getObject(2);
        System.out.println("---------In getInputData() mWorkNo=" + mWorkNo[0] +
                           " " + mWorkNo[1] + "---------");
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        String WorkNoUnited = "";
        String sql1 = "";
        String sql2 = "";
        String sql3 = "";

        if(mOperate.equals("UPDATE||MAIN"))
        {
            //得到要设置为被合并状态的工单号
            if(mWorkNo[0].equals(tLGWorkSchema.getWorkNo()))
            {
                WorkNoUnited = mWorkNo[1];;
            }
            else
            {
                WorkNoUnited = mWorkNo[0];
            }

            //处理被合并工单纪录
            sql1 = "update LGWork " +
                   "set uniteTo='" + tLGWorkSchema.getWorkNo() + "', " +
                   "statusNo='7', " +
                   "operator='" + mGlobalInput.Operator + "', " +
                   "modifyDate='" + mCurrentDate + "', " +
                   "modifyTime='" + mCurrentTime + "' " +
                   "where workNo='" + WorkNoUnited + "' ";
            map.put(sql1, "UPDATE");
            //处理合并后工单纪录
            sql2 = "update LGWork " +
                   "set uniteTo='" + WorkNoUnited + "', " +
                   "Uniting='1', " +
                   "operator='" + mGlobalInput.Operator + "', " +
                   "modifyDate='" + mCurrentDate + "', " +
                   "modifyTime='" + mCurrentTime + "' " +
                   "where WorkNo='" + tLGWorkSchema.getWorkNo() + "' ";
            map.put(sql2, "UPDATE");

            SSRS tSSRS = new SSRS();
            SSRS tSSRS2 = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            ExeSQL tExeSQL2 = new ExeSQL();

            //得到最大节点号
            String s1 = "select max(to_number(nodeNo)) " +
                       "from LGWorkTrace " +
                       "where workNo='" + tLGWorkSchema.getWorkNo() + "' ";
           tSSRS = tExeSQL.execSQL(s1);
           System.out.println("&&&&&&&&&&&&&&maxNo is: " + tSSRS.GetText(1, 1));

            //得到当前操作员信箱号
            String s2 = "select workBoxNo " +
                        "from LGWorkBox " +
                        "where OwnerNo='" + mGlobalInput.Operator + "' ";
            tSSRS2 = tExeSQL2.execSQL(s2);
            System.out.println("&&&&&&&&&&&&&&workBoxNo is: " + tSSRS2.GetText(1, 1));

            //生成被合并工单的历史纪录
            String s = " insert into LGWorkTrace " +
                       "values('" + tLGWorkSchema.getWorkNo() + "', '" + (Integer.parseInt(tSSRS.GetText(1, 1)) + 1) + "', '" +
                       tSSRS2.GetText(1, 1) + "', '9', '" + mCurrentDate + "', '" +
                       mCurrentTime + "', '', '', '" + mGlobalInput.Operator + "', '" +
                       mCurrentDate + "', '" + mCurrentTime + "', '" +
                       mCurrentDate + "', '" + mCurrentTime + "', '" + remark + "' ) ";
            map.put(s, "INSERT");
        }
        else if(mOperate.equals("DELETE||MAIN"))
        {

        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(mAcceptNo);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String s[])
    {
        LGWorkSchema schema = new LGWorkSchema();

        schema.setWorkNo("20050425000021");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        String[] strs = {"20050428000001", "20050425000021"};

        VData v = new VData();
        v.add(tG);
        v.add(schema);
        v.add(strs);
        v.add(" 作业合并");

        new TaskUniteBL().submitData(v, "UPDATE||MAIN");
    }
}






