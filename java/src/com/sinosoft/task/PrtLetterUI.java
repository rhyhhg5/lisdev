package com.sinosoft.task;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LGLetterSchema;
import com.sinosoft.utility.XmlExport;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PrtLetterUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */

    /** 全局数据 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = new GlobalInput();
    private String mWorkNo;
    private VData mResult = new VData();
    private String isSave = "";  //数据提交标志: true, 保存函件; false, 预览函件

    public PrtLetterUI()
    {
    }

    public PrtLetterUI(String flag)
    {
        isSave = flag;
    }


    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            mErrors.addOneError("不支持的操作字符串: " + cOperate);
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!checkData())
        {
            return false;
        }

        PrtLetterBL tPrtLetterBL = new PrtLetterBL();

        if(!tPrtLetterBL.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(tPrtLetterBL.mErrors);
            mErrors.addOneError("生成函件失败");
        }
        else
        {
            if(isSave.equals("Save"))
            {
                System.out.println("Befer commitData");
                if(!tPrtLetterBL.commitData("Save"))
                {
                    mErrors.copyAllErrors(tPrtLetterBL.mErrors);

                    return false;
                }
            }
            this.mResult = tPrtLetterBL.getResult();
        }

        return true;
    }


    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName("GlobalInput", 0));

        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean checkData()
    {
        if (mGlobalInput.Operator == null || mGlobalInput.Operator.equals("")
            || mGlobalInput.ComCode == null || mGlobalInput.ComCode.equals(""))
        {
            mErrors.addOneError("传入的操作员信息不完整");
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String s[])
     {
         GlobalInput tG = new GlobalInput();
         LGLetterSchema schema = new LGLetterSchema();

         tG.Operator = "endor";
         tG.ComCode = "86";

         schema.setEdorAcceptNo("20050827000011");
         schema.setLetterType("1");
         schema.setLetterSubType("0");
         schema.setSendObj("0");
         schema.setAddressNo("2");
         schema.setLetterInfo("asdfasfd");
         schema.setLetterInfo("asdfsadf");


         VData tVData = new VData();
         VData mResult = new VData();
         CErrors mErrors = new CErrors();

         tVData.add(tG);
         tVData.add(schema);

         PrtLetterUI tPrtLetterUI = new PrtLetterUI("Save");
         XmlExport txmlExport = new XmlExport();
         if (!tPrtLetterUI.submitData(tVData, "PRINT"))
         {
             System.out.println(tPrtLetterUI.mErrors.getFirstError().toString());
         }
         else
         {
             mResult = tPrtLetterUI.getResult();
             txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);

             if (txmlExport == null)
             {
                 System.out.println("txmlExport == null");
             }
         }


     }



}
