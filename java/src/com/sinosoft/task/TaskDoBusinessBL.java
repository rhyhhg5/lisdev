package com.sinosoft.task;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 经办BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @dat e 2005-02-25
 */

public class TaskDoBusinessBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private MMap map = new MMap();

    private String mWorkNo;

    private String mUrl = "";

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //供返回用的工单状态
    private String workStatus = "";

    public TaskDoBusinessBL()
    {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in TaskDoBusinessBL submit");
        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
        {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false)
        {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskDoBusinessBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        mGlobalInput.setSchema((GlobalInput) mInputData.
                               getObjectByObjectName("GlobalInput", 0));
        mWorkNo = (String) mInputData.
                               getObjectByObjectName("String", 0);
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        String ScanBeforeInput = "";
        String sql;
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        sql = "Select TypeNo, StatusNo, DetailWorkNo, CustomerNo, " +
              "       ApplyName, AcceptWayNo, AcceptDate, AcceptNo " +
              "From   LGWork " +
              "Where  WorkNo= '" + mWorkNo + "' ";
        tSSRS = tExeSQL.execSQL(sql);
        String tTypeNo = tSSRS.GetText(1, 1);
        String tStatusNo = tSSRS.GetText(1, 2);
        String tDetailWorkNo = tSSRS.GetText(1, 3);
        String tCustomerNo = tSSRS.GetText(1, 4);
        String tAcceptWayNo = tSSRS.GetText(1, 6);
        String tAcceptDate = tSSRS.GetText(1, 7);
        String tTopTypeNo = tTypeNo.substring(0, 2);
        String tStateNo = "";
        String AcceptNo = tSSRS.GetText(1, 8);
        workStatus = tSSRS.GetText(1, 2);
        if(tDetailWorkNo.equals(""))
        {
            tDetailWorkNo = mWorkNo;
        }

        if ((tStatusNo != null) && (tTopTypeNo != null))
        {
            //续期续保工单走保全流程,其它走电话工单流程
            if(tTypeNo.equals("070001")||tTypeNo.equals("070002"))
            {
                tTopTypeNo = "03";
            }else if(tTopTypeNo.equals("07"))
            {
                tTopTypeNo = "05";
            }
            //待办件和存档件不能经办
            if ((!tStatusNo.equals("0")) && (!tStatusNo.equals("1")) && (!tStatusNo.equals("6")))
            {
                //咨询
                if (tTopTypeNo.equals("01"))
                {
                    tStateNo = "0";
                }

                //投诉
                else if (tTopTypeNo.equals("02"))
                {
                    try
                    {
                        String sqlTemp = "Select StatusNo " +
                                         "From   LGWork " +
                                         "Where  workNo='" + mWorkNo + "' ";
                        tSSRS = tExeSQL.execSQL(sqlTemp);
                        String state = tSSRS.GetText(1, 1);
                        if(state.equals("5") || state.equals("6") || state.equals("7"))
                        {
                            tStateNo = "000";
                        }
                        else
                        {
                            tStateNo = "001";
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println(e.toString());

                        mUrl = "None";
                    }
                }

                //保全
                else if (tTopTypeNo.equals("03"))
                {
                    //团单
                    if(tCustomerNo.length() == 8)
                    {
                        tTopTypeNo = tTopTypeNo + "1";
                    }
                    else
                    {
                        tTopTypeNo = tTopTypeNo + "0";
                    }

                    LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
                    tLPEdorAppDB.setEdorAcceptNo(tDetailWorkNo);
                    if(!tLPEdorAppDB.getInfo())
                    {
                        if(isScanBeforeInput(mWorkNo))
                        {
                            ScanBeforeInput = "1";
                            tStateNo = "00";
                        }
                    }
                    else
                    {
                        tStateNo = tLPEdorAppDB.getEdorState();
                    }
                }
                //理赔
                else if (tTopTypeNo.equals("04"))
                {
                }

                //电话回访
                else if(tTopTypeNo.equals("05"))
                {
                    //查询工单状态
                    String state = queryStateNo(mWorkNo);
                    if(state.equals(""))
                    {
                        return false;
                    }

                    //公用：根据工单状设置查找经办路径是的状态编号
                    if (state.equals("5") || state.equals("6") ||
                        state.equals("7"))
                    {
                        //工单不可被修改
                        tStateNo = "000";
                    }
                    else
                    {
                        tStateNo = "001";
                    }
                    tTopTypeNo += tTypeNo.substring(tTypeNo.length() - 2);
                }
                else if(tTopTypeNo.equals("06"))
                {
                    tTopTypeNo = tTopTypeNo + "01";
                    tStateNo="3";
                }
                //得到URL
                if (!tStateNo.equals(""))
                {
                    sql = "Select UrlInfo " +
                          "From   LGBusinessUrl " +
                          "Where  TypeNo = '" + tTopTypeNo + "' " +
                          "and    StatusNo = '" + tStateNo + "' ";
                    tSSRS = tExeSQL.execSQL(sql);
                    System.out.println(sql);
                    mUrl += tSSRS.GetText(1, 1) +
                        "DetailWorkNo=" + tDetailWorkNo +
                        "&CustomerNo=" + tCustomerNo +
                        "&AcceptWayNo=" + tAcceptWayNo +
                        "&AcceptDate=" + tAcceptDate +
                        "&AcceptNo=" + AcceptNo +
                        "&TypeNo=" + tTypeNo +
                        "&ScanBeforeInput=" + ScanBeforeInput;
                }
            }
        }
        return true;
    }

    /**
     * 校验工单是否是先扫描后录入，调用本方法的前提是没有保全受理
     * @param tWorkNo String
     * @return boolean
     */
    private boolean isScanBeforeInput(String tWorkNo)
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(tWorkNo);
        if(!tLGWorkDB.getInfo())
        {
            return false;
        }

        //LGWork表中没有客户信息，但是在扫描表中有数据，并且没有保全受理（进入本方法的前提）
        if(tLGWorkDB.getTypeNo().indexOf("03") == 0
           && (tLGWorkDB.getCustomerNo() == null
               || tLGWorkDB.getCustomerNo().equals("")))
        {
            StringBuffer sql = new StringBuffer();
            sql.append("select b.* ")
                .append("from ES_DOC_RELATION a, ES_DOC_MAIN b ")
                .append("where a.docID = b.docID ")
                .append("   and a.bussNoType = '91' ")
                .append("   and bussNo = '").append(tWorkNo).append("' ");
            System.out.println(sql.toString());
            ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
            if(tES_DOC_MAINDB.executeQuery(sql.toString()).size() > 0)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(mUrl);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 输入参数：无
     * 返回值：工单状态
     */
    public String getStatus()
    {
        return workStatus;
    }

    //查询工单状态
    private String queryStateNo(String aWorkNo)
    {
        try
        {
            String sqlTemp = "Select StatusNo " +
                             "From   LGWork " +
                             "Where  workNo='" + mWorkNo + "' ";
            SSRS aSSRS = new SSRS();
            ExeSQL aExeSQL = new ExeSQL();

            aSSRS = aExeSQL.execSQL(sqlTemp);
            return (aSSRS.GetText(1, 1));
        }
        catch (Exception e)
        {
            this.mErrors.addOneError(
                    new CError("查询工单状态时出错",
                               "TaskDoBusinessBL", "queryStateNo"));
            System.out.print("------查询工单状态时出错");

            return "";
        }
    }

    public static void main(String a[])
    {
        String workNo = "20070111001156";
        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa0001";

        VData v = new VData();
        v.add(gi);
        v.add(workNo);

        TaskDoBusinessBL doB = new TaskDoBusinessBL();
        doB.submitData(v, "");
        System.out.println(doB.getResult().getObjectByObjectName("String", 0));
    }

}
