package com.sinosoft.task;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LGProjectPurviewSchema;
import com.sinosoft.lis.db.LGGroupMemberDB;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class TaskCopyOnePostPurviewBL
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();
    private LGProjectPurviewSet mLGProjectPurviewSet = null;
    private GlobalInput mGlobalInput = null;
    private MMap map = new MMap();;

    public TaskCopyOnePostPurviewBL()
    {
    }

    /**
     * 提交数据的公共方法
     * @param data VData 需包含 LGProjectPurviewSet 和 GlobalInput
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(this.getDealtMap(cInputData, cOperate) != null)
        {
            VData v = new VData();
            v.add(map);
            PubSubmit p = new PubSubmit();
            if (!p.submitData(v, "INSERT"))
            {
                mErrors.addOneError("提交数据失败。");
                System.out.println("TaskCopyOnePostPurviewBL->提交数据失败。");
                p.mErrors.getErrContent();
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    /**
     * 返回处理后的map
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getDealtMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }
        return this.map;
    }

    /**
     * description
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mLGProjectPurviewSet =
                    (LGProjectPurviewSet) data.getObjectByObjectName(
                            "LGProjectPurviewSet", 0);
            mGlobalInput =
                    (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        }
        catch (Exception e)
        {
            mErrors.addOneError("传入的数据不完整.");
            System.out.println("TaskCopyOnePostPurviewUI->传入的数据不完整.");
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        LGProjectPurviewSet set = new LGProjectPurviewSet();
//        long projectPurviewNo = 0;
        for (int i = 1; i <= mLGProjectPurviewSet.size(); i++)
        {
            //查询该岗位上的所有成员
            LGGroupMemberDB db = new LGGroupMemberDB();
            db.setpostNo(mLGProjectPurviewSet.get(i).getPostNo());
            LGGroupMemberSet tLGGroupMemberSet = new LGGroupMemberSet();
            tLGGroupMemberSet = db.query();

            //对每个此岗位上的成员复制一份权限
            if(tLGGroupMemberSet != null && tLGGroupMemberSet.size() > 0)
            {
                for(int j = 1; j <= tLGGroupMemberSet.size(); j++)
                {
//                    projectPurviewNo = GetMaxNo.getLong("LGProjectPurview",
//                                                        "ProjectPurviewNo")
//                                       + mLGProjectPurviewSet.size()
//                                       + set.size();
                    LGProjectPurviewSchema schema = new LGProjectPurviewSchema();
                    schema.setSchema(mLGProjectPurviewSet.get(i));
                    schema.setProjectPurviewNo(PubFun1.CreateMaxNo("TASKPURVIEW", null));
                    schema.setMemberNo(tLGGroupMemberSet.get(j).getMemberNo());
                    schema.setOperator(mGlobalInput.Operator);
                    PubFun.fillDefaultField(schema);
                    set.add(schema);

                    String sql = "delete from LGProjectPurview "
                                 + "where memberNo = '"
                                 + tLGGroupMemberSet.get(j).getMemberNo() + "' "
                                 + "    and projectNo = '"
                                 + schema.getProjectNo() + "' "
                                 + "    and projectType = '"
                                 + schema.getProjectType() + "' "
                                 + "    and DealOrganization = '"
                                 + schema.getDealOrganization() + "' "
                                 + "	and (stateflag!='2' or stateflag is null) ";
                    if(mLGProjectPurviewSet.get(i).getRiskCode()!=null&&!"".equals(mLGProjectPurviewSet.get(i).getRiskCode())){
                    	sql +=  "and riskcode = '"+mLGProjectPurviewSet.get(i).getRiskCode()+"'";
                    }else{
                    	sql += " and (riskcode is null or riskcode = '')";
                    }

                    map.put(sql, "DELETE");
                }
            }
        }

        if(set.size() > 0)
        {
            map.put(set, "INSERT");
        }

        return true;
    }



    public static void main(String[] args)
    {
        TaskCopyOnePostPurviewBL taskcopyonepostpurviewbl = new
                TaskCopyOnePostPurviewBL();
    }
}
