package com.sinosoft.task;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 简易保全逻辑处理类</p>
 * <p>Description: 被保人保单联系地址变更 </p>
 * <p>Copyright: Copyright (c) 2013 </p>
 * <p>Company: Sinosoft </p>
 * @author Li CaiYan
 * @version 1.0
 * @date 2013-4-8
 */

public class InsuEasyEdorBL
{
    /** 错误保存容器 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput = null;

    private LGWorkSchema mLGWorkSchema = null;

    private LCAddressSchema mLCAddressSchema = null;

    private LPInsuredSchema mLPInsuredSchema = null;

    private LPContSet mLPContSet = null;

    private String mEdorAcceptNo = null;

    private String mEdorType = BQ.EDORTYPE_AD;

    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到生成的工单号
     * @return String
     */
    public String getEdorAcceptNo()
    {
        return mEdorAcceptNo;
    }

    /**
     * 得到输入数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.
                getObjectByObjectName("GlobalInput", 0);
        mLGWorkSchema = (LGWorkSchema) cInputData.
                getObjectByObjectName("LGWorkSchema", 0);
        mLCAddressSchema = (LCAddressSchema) cInputData.
                getObjectByObjectName("LCAddressSchema", 0);
        mLPInsuredSchema = (LPInsuredSchema) cInputData.
                getObjectByObjectName("LPInsuredSchema", 0);
        mLPContSet = (LPContSet) cInputData.
                getObjectByObjectName("LPContSet", 0);
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        //添加工单受理
        mEdorAcceptNo = createTask();
        if ((mEdorAcceptNo == null) || (mEdorAcceptNo.equals("")))
        {
            return false;
        }
        mLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
        if (!addEdorItem())
        {
            return false;
        }
        if (!saveDetail())
        {
            return false;
        }
        //保全理算
        if (!appConfirm())///---改成保全理算后状态
        {
            return false;
        }
        creatPrintVts();
        //保全确认
        if (!edorConfirm())
        {
            return false;
        }
        return true;
    }

    /**
     * 创建工单，添加工单受理
     * @return boolean
     */
    private String createTask()
    {
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(mLGWorkSchema);
        TaskInputBL tTaskInputBL = new TaskInputBL();
        if (!tTaskInputBL.submitData(data, ""))
        {
            mErrors.addOneError("工单数据生成失败！");
            return null;
        }
        return tTaskInputBL.getWorkNo();
    }

    /**
     * 添加保全项目
     * @return boolean
     */
    private boolean addEdorItem()
    {
        MMap map = new MMap();
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorAppNo(mEdorAcceptNo);
        tLPEdorMainSchema.setContNo(mLPInsuredSchema.getContNo());
        tLPEdorMainSchema.setEdorAppDate(mCurrentDate);
        tLPEdorMainSchema.setEdorValiDate(mCurrentDate);
        tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INIT);//保全初始状态
        tLPEdorMainSchema.setUWState(BQ.UWFLAG_INIT);//未经核保
        tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorMainSchema.setMakeDate(mCurrentDate);
        tLPEdorMainSchema.setMakeTime(mCurrentTime);
        tLPEdorMainSchema.setModifyDate(mCurrentDate);
        tLPEdorMainSchema.setModifyTime(mCurrentTime);
        map.put(tLPEdorMainSchema, "INSERT");

        mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
        mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
        mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
        mLPEdorItemSchema.setDisplayType("1");
        mLPEdorItemSchema.setEdorType(mEdorType);//联系方式变更
        mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPEdorItemSchema.setContNo(mLPInsuredSchema.getContNo());
        mLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
        mLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
        mLPEdorItemSchema.setEdorValiDate(mCurrentDate);
        mLPEdorItemSchema.setEdorAppDate(mCurrentDate);
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setMakeDate(mCurrentDate);
        mLPEdorItemSchema.setMakeTime(mCurrentTime);
        mLPEdorItemSchema.setModifyDate(mCurrentDate);
        mLPEdorItemSchema.setModifyTime(mCurrentTime);
        map.put(mLPEdorItemSchema, "INSERT");
        if (!submit(map))
        {
            return false;
        }
        return true;
    }

    /**
     * 保存明保全细
     * @return boolean
     */
    private boolean saveDetail()
    {
        //准备数据
        LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
        tLPInsuredSchema.setEdorNo(mEdorAcceptNo);
        tLPInsuredSchema.setEdorType(mEdorType);
        tLPInsuredSchema.setGrpContNo(BQ.GRPFILLDATA);
        tLPInsuredSchema.setContNo(mLPInsuredSchema.getContNo());
        tLPInsuredSchema.setInsuredNo(mLPInsuredSchema.getInsuredNo());

        LPContSchema tLPContSchema = new LPContSchema();
        tLPContSchema.setEdorNo(mEdorAcceptNo);
        tLPContSchema.setEdorType(mEdorType);
        tLPContSchema.setContNo(mLPInsuredSchema.getContNo());
        tLPContSchema.setGrpContNo(BQ.GRPFILLDATA);

        //做地址变更
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLPInsuredSchema);
        data.add(mLCAddressSchema);
        data.add(mLPEdorItemSchema);
        data.add(mLPContSet);
        PEdorADDetailBL tPEdorADDetailBL = new PEdorADDetailBL();
        if (!tPEdorADDetailBL.submitInsuData(data, ""))
        {
            mErrors.copyAllErrors(tPEdorADDetailBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 保全理算，更新核保标志等同于保全理算
     * @return boolean
     */
    private boolean appConfirm()
    {
        MMap map = new MMap();
        String sql;
        sql = "update LPEdorApp set EdorState = '2', UWState = '9' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LPEdorMain set EdorState = '2', UWState = '9' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LPEdorItem set EdorState = '2' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");//---“2”--保全理算后状态
        if (!submit(map))
        {
            return false;
        }
        return true;
    }

    /**
     * 产生打印数据
     * @return boolean
     */
    private boolean creatPrintVts()
    {
        //生成打印数据
        VData data = new VData();
        data.add(mGlobalInput);
        PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
                mEdorAcceptNo);
        if (!tPrtAppEndorsementBL.submitData(data, ""))
        {
            mErrors.addOneError("数据保存成功！但没有生成保全服务批单！");
            return false;
        }
        return true;
    }

    /**
     * 保全确认
     * @param edorAcceptNo String
     * @return boolean
     */
    private boolean edorConfirm()
    {
        MMap map = new MMap();
        PInsuEdorConfirmBL tPEdorConfirmBL =
                new PInsuEdorConfirmBL(mGlobalInput, mEdorAcceptNo);
        MMap edorMap = tPEdorConfirmBL.getSubmitData();
        if (edorMap == null)
        {
            mErrors.copyAllErrors(tPEdorConfirmBL.mErrors);
            return false;
        }
        map.add(edorMap);
        //工单结案
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
        tLGWorkSchema.setTypeNo("03"); //结案状态

        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGWorkSchema);
        TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
        MMap taskMap = tTaskAutoFinishBL.getSubmitData(data, "");
        if (taskMap == null)
        {
            mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
            return false;
        }
        map.add(taskMap);
        if (!submit(map))
        {
            return false;
        }
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
