package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: 接口：校验规则的合法性
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public abstract class CheckRule
{
    public CErrors mErrors = new CErrors();

    /**
     * description 数据提交的公共nog方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String operate)
    {
        return true;
    }
}
