package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LGProjectPurviewSet;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 快速新建岗位级别的确认权
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class TaskPostPurviewConfirmUI
{
    /**
     * 错误容器
     */
    public CErrors mErrors = new CErrors();

    public TaskPostPurviewConfirmUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        TaskPostPurviewConfirmBL bl = new TaskPostPurviewConfirmBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        TaskPostPurviewConfirmUI taskpostpurviewconfirmui = new
                TaskPostPurviewConfirmUI();
    }
}
