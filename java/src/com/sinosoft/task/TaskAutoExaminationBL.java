package com.sinosoft.task;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LGProjectPurviewDB;
import com.sinosoft.lis.db.LGTraceNodeOpDB;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.db.LPGrpEdorMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LGTraceNodeOpSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LGWorkTraceSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LGProjectPurviewSet;
import com.sinosoft.lis.vschema.LGTraceNodeOpSet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description:
 * 审批时间点为核保已通过并进行经办确认时由系统自动进行。</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author: Yang Yalin
 * description:
 * @version 1.0
 */
public class TaskAutoExaminationBL
{
    /**错误保存容器*/
    public CErrors mErrors = new CErrors();
    private CErrors confirmReason = null;

    // 输出数据的容器
    private VData mResult = new VData();

    // 全局参数
    private GlobalInput mGlobalInput = new GlobalInput();

    //统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    //权限全局变量
    private LGWorkSchema mLGWorkSchema = new LGWorkSchema();

    private String postNo = "";  //业务员的岗位
    private String workNo = "";
    private String superManager = "";  //业务员的业务上级编号
    private String superManagerName = "";  //业务员的业务上级名
    private String myName;  //业务员的姓名
    private String startDate ="2000-1-1";
    private String endDate = "1950-1-1";

    private String customerNo;  //确认项目是个险还是团险
    private TransferData mTransferData = null;

    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();

    public TaskAutoExaminationBL()
    {
    }

    /**
     * LGAutoExaminationBL对象提交数据函数
     * 输入：数据集合VData， 操作方式String
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println(".......Now in TaskAutoExaminationBL.......");

        if (!getInputData(cInputData) )
        {
            return false;
        }
        //若工单状态为审批通过99,则不需要再进行自动审批
        if(isConfirmed())
        {
            return true;
        }
        if(!dealData() || !prepareOutputData())
        {
            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        return true;
    }

    //将输入的数据分解到本地
    private boolean getInputData(VData mInputData)
    {
        try
        {
            mLGWorkSchema = (LGWorkSchema) mInputData.getObjectByObjectName(
                    "LGWorkSchema", 0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) mInputData.
                            getObjectByObjectName("TransferData", 0);
            LGWorkDB db = new LGWorkDB();
            db.setWorkNo(mLGWorkSchema.getWorkNo());
            db.getInfo();

            mLGWorkSchema.setSchema(db.getSchema());
        }
        catch(Exception e)
        {
            this.mErrors.addOneError(
                    new CError("传入到自动审批程序TaskAutoExaminationBL的数据不完整",
                               "TaskAutoExaminationBL", "getInputData"));
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 输入：none
     * 输出：如果处理数据时发生错误则返回false,否则返回true
     */
    public boolean dealData()
    {
        String sql;
        double moneyPurview;  //保存项目产生的金额差
        double excessPurview;  //保存项目产生的超额
        String timePurview;  //保存项目的时间限
        boolean checkFlag = true; //是否有权限标志，true：有权限；false：无权限
        boolean otherFlag = true; //其它权限，与个项目相关。 true：有权限；false：无权限

        String confirmReasonMes = "";

        confirmReason = new CErrors();

        workNo = mLGWorkSchema.getWorkNo();

        if (!queryCustomerNo() || !queryPoseAndSuper())
        {
            return false;
        }

        superManagerName = querySuperManagerName();
        myName = queryMyName();

        //如果当前用户是总公司保单管理部经理，则通过自动审批
        String postNoTemp = postNo.toLowerCase();
        if(postNoTemp.equals("pa01"))
        {
            return true;
        }

        //将要查询的item表
        String itemTable = getItemTable();

        //查询保全项目
        if(itemTable.equals("LPEdorItem"))  //个险保全
        {
            sql = "select edorAcceptNo, edorNo, edorType, ContNo, InsuredNo, PolNo "
                  + "from " + itemTable
                  + " where edorAcceptNo='" + mLGWorkSchema.getWorkNo() + "' ";
        }
        else  //团险保全
        {
            sql="select edorAcceptNo, edorNo, edorType, GrpContNo, edorvalidate "
                + "from " + itemTable
                + " where edorAcceptNo='" + mLGWorkSchema.getWorkNo() + "' ";
        }

        int edorCount = 0;  //当前工单的保全项目个数
        SSRS tSSRSEdorInfo = tExeSQL.execSQL(sql);  //保存当前工单的所有项目信息
        if (tSSRSEdorInfo.getMaxRow() > 0)
        {
            edorCount = tSSRSEdorInfo.getMaxRow();
        }

        for(int i = 1; i <= edorCount; i++)
        {

            try
            {
                int projectType = getProjectType();
                //2006-10-23 qulq添加校验规则
                //短期险复效需送上级审批
                //只处理个险 0是个险
                if(projectType == 0)
                {
                	//分红险满期领取
                	if(tSSRSEdorInfo.GetText(i, 3).equals("HA")){
                       String sql2 = "select distinct a.appntno,a.insuredno,c.edorvalue "
                            + " from LCPol a, LPEdorItem b,LPEdorEspecialData c " 
                            + "where a.contno =b.contno and b.edorno=c.edorno" 
                            + " and b.edorAcceptNo='" + mLGWorkSchema.getWorkNo() + "' "
                            + " and b.edortype='HA' and c.detailtype='HLGET' with ur" ;
                        SSRS tSSRS = tExeSQL.execSQL(sql2);  //保存当前工单的所有项目信息
                        System.out.println(tSSRS.GetText(1, 1));
                        System.out.println(tSSRS.GetText(1, 2));
                        System.out.println(tSSRS.GetText(1, 3));
                        System.out.println(tSSRS.GetText(1, 1)!= tSSRS.GetText(1, 2) && tSSRS.GetText(1, 3).equals("1"));
                        if (tSSRS.GetText(1, 1)!= tSSRS.GetText(1, 2) && tSSRS.GetText(1, 3).equals("1"))
                        {
                        	//给付对象为投保人是需要送审，判断当前业务岗位
                            String sql3 = " select 1 "
                                + " from LGGroupMember "
                                + " where memberNo='" + mGlobalInput.Operator + "' and postno in ('PA01','PA02','PA03') ";
                            tSSRS = tExeSQL.execSQL(sql3);
                            if(tSSRS.getMaxRow()==0){
//                            	String memberno=mGlobalInput.Operator ;
                            	if(getMemBerNo(mGlobalInput.Operator)){
                                    //送审批
                            		String errMsg = "";
                                    if (!deliver(errMsg+confirmReasonMes, true) && superManager != null &&
                                            !superManager.equals(""))
                                    { //送审批失败
                                        mErrors.addOneError(
                                                BuildError.create("TaskAutoExaminationBL",
                                                "dealData", errMsg+confirmReasonMes + "，但是送审批失败"));
                                        return false;
                                    }
                                    confirmReason.addOneError(errMsg+confirmReasonMes); //保存送审批信息

//                                    return true;
                            	}
                            }
                        }
                	}
                	
//                	减少保额不送审，增加保额走确认权
                	if(tSSRSEdorInfo.GetText(i, 3).equals("BA"))
                	{
                		boolean examFlag = true; //所有险种都减保，则examFlag = true，不送审。
                		String tContNo = tSSRSEdorInfo.GetText(i, 4);
                		String tEdorNo = tSSRSEdorInfo.GetText(i, 1);
                		
                		String sqlPol = "select (select Amnt from LCPol where Polno = a.Polno),amnt from LPPol a where ContNo = '"
                				+ tContNo + "' and EdorNo = '" + tEdorNo + "' ";
                		
                		SSRS tSSRS = new SSRS();
                		tSSRS = new ExeSQL().execSQL(sqlPol);
                		
                		for(int p=1 ;p<=tSSRS.getMaxRow(); p++)
                		{
                			double preAmnt = 0.0;
                			double afterAmnt = 0.0;
                			if(tSSRS.GetText(p, 1)!=null && !tSSRS.GetText(p, 1).equals("") && !tSSRS.GetText(p, 1).equals("null"))
                			{
                				preAmnt = Double.parseDouble(tSSRS.GetText(p, 1));
                			}
                			if(tSSRS.GetText(p, 2)!=null && !tSSRS.GetText(p, 2).equals("") && !tSSRS.GetText(p, 2).equals("null"))
                			{
                				afterAmnt = Double.parseDouble(tSSRS.GetText(p, 2));
                			}
                			//循环所有险种，只要有1个加保则判断确认全。如果所有险种都减保，则不需要送审。
                			if(preAmnt<afterAmnt) //加保
                        	{
                				examFlag = false;
                        	}
                		}
                		if(examFlag)
                		{
                			return true;
                		}
                	}
                	else if(tSSRSEdorInfo.GetText(i, 3).equals("FX"))
                    {
                        StringBuffer tSQL = new StringBuffer(128);
                        tSQL.append("select distinct RiskPeriod from lmriskapp where riskCode in (select b.riskcode from lppol b where b.contno = '" )
                                .append(tSSRSEdorInfo.GetText(i, 4))
                                .append("' and b.edorno = '")
                                .append(tSSRSEdorInfo.GetText(i, 1))
                                .append("')")
                                ;
                        SSRS tRiskInfo = tExeSQL.execSQL(tSQL.toString());
                        //短期交上级
                         for (int j = 1; j <= tRiskInfo.getMaxRow(); j++)
                         {
                           if (!tRiskInfo.GetText(j, 1).equals("L"))
                           {
                             //只送审一次，如果已经审批过则跳过
                             LGTraceNodeOpDB tLGTraceNodeOpDB= new LGTraceNodeOpDB();
                             tLGTraceNodeOpDB.setWorkNo(this.workNo);
                             tLGTraceNodeOpDB.setOperatorType(Task.HISTORY_TYPE_SDEXAMIN);
                             LGTraceNodeOpSet tLGTraceNodeOpSet = tLGTraceNodeOpDB.query();
                             if (tLGTraceNodeOpSet.size() >0)
                             {
                                 continue;
                             }
                             confirmReasonMes += myName + "的" + tSSRSEdorInfo.GetText(i, 3)
                                                + "项目记录是短期险需要送给" + this.superManagerName + "审批 ";
                             //设定送审批标记
                             otherFlag = false;
                            }
                            else if (tRiskInfo.GetText(j, 1).equals("L"))
                            {
                                sql =  "select min(startdate) from lccontstate where StateType = 'Available'"
                                       +" and State = '1' and enddate is null and contno = '"
                                       + tSSRSEdorInfo.GetText(i, 4)
                                   //    + "' and polno='"
                                   //    + tSSRSEdorInfo.GetText(i, 6)
                                       + "'"
                                       ;
                                SSRS tStartDate = tExeSQL.execSQL(sql);
                                if (tStartDate.getMaxRow()==0 ||tStartDate.GetText(1, 1).equals(""))
                                {
                                    mErrors.addOneError(BuildError.create("TaskAutoExaminationBL","dealData", "查询失效日期失败"));
                                    return false;
                                }
                                startDate = tStartDate.GetText(1, 1);
                                sql = "select EdorValue from LPedorEspecialData where EdorAcceptNo='"
                                      + mLGWorkSchema.getWorkNo()
                                      + "' and EdorType = 'FX' and DetailType='FX_D'";
                                SSRS tEndDate = tExeSQL.execSQL(sql);
                                if (tEndDate == null ||tEndDate.GetText(1, 1).equals(""))
                                {
                                    mErrors.addOneError(BuildError.create("TaskAutoExaminationBL","dealData", "查询复效日期失败"));
                                    return false;
                                }
                                endDate = tEndDate.GetText(1, 1);
                            }
                        }
                    }
                    //2009-5-6 zhanggm 银保万能协议退保分公司没有确认权限
                    else if(tSSRSEdorInfo.GetText(i, 3).equals("XT"))
                    {
                    	if(com.sinosoft.lis.bq.CommonBL.hasULIRisk(tSSRSEdorInfo.GetText(i, 4)))
                    	{
                    		StringBuffer tSQL = new StringBuffer(128);
                            tSQL.append("select salechnl from lccont where contno = '")
                                    .append(tSSRSEdorInfo.GetText(i, 4)).append("' with ur");
                            String tSalechnl = tExeSQL.getOneValue(tSQL.toString());
                            if(!"".equals(tSalechnl) && ("04".equals(tSalechnl) || "13".equals(tSalechnl)))
                            {
                            	//add by lzy 20151109保全新增支公司岗位级别
                            	if(postNoTemp.equals("pa07")||postNoTemp.equals("pa08")||postNoTemp.equals("pa09")
                            		||postNoTemp.equals("pa06")||postNoTemp.equals("pa05")||postNoTemp.equals("pa04"))
                            	{
                            		confirmReasonMes += myName + "的" + tSSRSEdorInfo.GetText(i, 3)
                                    + "项目记录是银保万能协议退保，需要送给" + this.superManagerName + "审批 ";
                            		otherFlag = false;
                            	}
                            }
                    	}
                    }
                }
                //qulq 061121 添加团单特需退保
               if(projectType == 1)
               {
                   if(tSSRSEdorInfo.GetText(i, 3).equals(BQ.EDORTYPE_CT))
                   {
                       EdorItemSpecialData tEdorItemSpecialData =
                               new EdorItemSpecialData(tSSRSEdorInfo.GetText(i,1),
                              tSSRSEdorInfo.GetText(i,2),
                               tSSRSEdorInfo.GetText(i,3));
                       if(tEdorItemSpecialData.query())
                       {
                           LPEdorEspecialDataSet tLPEdorEspecialDataSet =
                                   tEdorItemSpecialData.getSpecialDataSet();

                           if (getEdorValue("RATEMONEY",tLPEdorEspecialDataSet) != null) {
                               //定额利息加入
                               confirmReasonMes += myName + "的" + tSSRSEdorInfo.GetText(i, 3)
                                       + "项目记录是录入利息金额，需要送给" + this.superManagerName
                                       + "审批 ";

                               //设置送审批标记
                               otherFlag = false;
                          }
                          //如保险期间未满一年，选择非无息需送上级审批。
                          //查找保单
                          LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                          tLCGrpContDB.setGrpContNo(tSSRSEdorInfo.GetText(i,4));
                          LCGrpContSet grpContSet = tLCGrpContDB.query();
                          if(grpContSet==null||grpContSet.size()<1)
                          {
                              this.mErrors.addOneError("没有找到保单");
                              return false;
                          }
                          LCGrpContSchema tLCGrpContSchema = grpContSet.get(1);
                          int year = PubFun.calInterval(tLCGrpContSchema.getCValiDate(),
                                           tSSRSEdorInfo.GetText(i, 5),"Y");
                          if(year==0)
                          {
                              if(!getEdorValue("PERSONALRATETYPE",tLPEdorEspecialDataSet).equals("1")||
                                 !getEdorValue("GROUPRATETYPE",tLPEdorEspecialDataSet).equals("1")||
                                 !getEdorValue("FIXEDRATETYPE",tLPEdorEspecialDataSet).equals("1"))
                              {
                                  confirmReasonMes += myName + "的" +tSSRSEdorInfo.GetText(i, 3)
                                          + "项目记录是保险期间未满一年，选择非无息，需要送给"
                                          + this.superManagerName +"审批" ;

                                  //送审批
                                  otherFlag = false;
                              }
                          }
                          //只送审一次，如果已经审批过则跳过
                          LGTraceNodeOpDB tLGTraceNodeOpDB = new LGTraceNodeOpDB();
                          tLGTraceNodeOpDB.setWorkNo(this.workNo);
                          tLGTraceNodeOpDB.setOperatorType(Task.
                                  HISTORY_TYPE_SDEXAMIN);
                          LGTraceNodeOpSet tLGTraceNodeOpSet =
                                  tLGTraceNodeOpDB.query();
                          if (tLGTraceNodeOpSet.size() > 0) {
                              otherFlag = true;
                          }
                      }
                  }
              }
	              //如果是金额权
                if (isMoneyPurview(tSSRSEdorInfo.GetText(i, 3)))
                {
                    //查询保全费用
                    SSRS tSSRSGetMoney = getGetMoney(
                            itemTable, tSSRSEdorInfo.getRowData(i));

                    if (tSSRSGetMoney.getMaxRow() == 1)
                    {
                        //得到金额权限
                        moneyPurview = Double.parseDouble(tSSRSGetMoney.GetText(
                                1, 1));
                        System.out.println("getMoney: " + Math.abs(moneyPurview));
                    }
                    else
                    {
                        mErrors.addOneError("查询收/退费差额时出错");
                        return false;
                    }
                    //qulq 061130 计算超额 传入保全项目
                    excessPurview = getExcess(tSSRSEdorInfo.GetText(i, 3),moneyPurview);
                    timePurview = getTime(tSSRSEdorInfo.getRowData(i));

                    //查询该用户保全处理权限
                    String sqlGetPurview =
                            "select MoneyPurview, ExcessPurview, TimePurview, NeedOtherAudit, MemberNo "
                            + "from LGProjectPurview "
                            + "where ProjectNo='" + tSSRSEdorInfo.GetText(i,
                            3) + "' "
                            + "   and ProjectType='" + projectType + "' "
                            + "   and DealOrganization='" + getDealOrganization() +
                            "' "
                            + "   and PostNo='" + postNo + "' ";

                    sql = sqlGetPurview
                            + "    and MemberNo='" + mGlobalInput.Operator +
                            "' "
                            + "union "
                            + sqlGetPurview
                            + "    and MemberNo='00000000000000000000' "
                            + "order by MemberNo desc ";

                    //用户的权限
                    SSRS tSSRSPurview = getMemberPerview(itemTable,tSSRSEdorInfo.
                            getRowData(i));

                    //权限表中有记录
                    if (tSSRSPurview.getMaxRow() >= 1)
                    {
                        String errMsg = "";
                        checkFlag = checkFlag
                                && (Double.parseDouble(tSSRSPurview.GetText(1,
                                1))
                                >= Math.abs(moneyPurview)) ;//金额权限

                        if (!checkFlag)
                        {
                            //没有金额权限，需送审
                            errMsg += myName + "的" + tSSRSEdorInfo.GetText(i, 3)
                                    + "项目金额权限" + tSSRSPurview.GetText(1, 1)
                                    + "元不够，";
                        }
                        //061201 qulq
                        boolean excPurFlag =(Double.parseDouble(tSSRSPurview.GetText(1,
                                2))
                                >= -excessPurview); //超额权限
                        if(!excPurFlag)
                        {
                            //需送审
                            errMsg += myName + "的" + tSSRSEdorInfo.GetText(i, 3)
                                    + "项目超额权限" + tSSRSPurview.GetText(1, 2)
                                    + "元不够，";

                        }

                        boolean timeFlag = Long.parseLong(tSSRSPurview.GetText(
                                1, 3))
                                >= Long.parseLong(timePurview);
                        if (!timeFlag)
                        {
                            //没有时间权限,需送审
                            errMsg += tSSRSEdorInfo.GetText(i, 3)
                                    + "项目超期权限" + tSSRSPurview.GetText(1, 3)
                                    + "天不够，送给" + this.superManagerName + "审批";
                        }
                        checkFlag = checkFlag && timeFlag && excPurFlag&&otherFlag;

                        if (!checkFlag && superManager != null
                                && !superManager.equals(""))
                        {
                            if (!deliver(errMsg+confirmReasonMes, true))
                            {
                                mErrors.addOneError(errMsg+confirmReasonMes + ", 但是送c审批失败。"); //保存程序错误信息
                                return false;
                            }
                            confirmReason.addOneError(errMsg+confirmReasonMes); //保存送审批信息
                            return false;
                        }
                        else if (!checkFlag)
                        {
                            mErrors.addOneError(
                                    "对不起,您的不够,需送业务上级审批，但没查到您业务上级，请检查用户设置合法性");
                            return false;
                        }
                        //有权限但需互核, 扩展用
                        if (checkFlag && (tSSRSPurview.GetText(1, 4).equals("1")))
                        {
                            //需求取消,此处备扩展
                        }
                    }
                    else
                    {
                        //在权限表中没有记录
                        mResult.add("0");

                        //生成历史信息的备注
                        String errMsg =
                                "权限表里没有" + myName + "的" +
                                tSSRSEdorInfo.GetText(i, 3)
                                + "项目记录，送给" + this.superManagerName + "审批";

                        //送审批
                        if (!deliver(errMsg+confirmReasonMes, true) && superManager != null &&
                                !superManager.equals(""))
                        { //送审批失败
                            mErrors.addOneError(
                                    BuildError.create("TaskAutoExaminationBL",
                                    "dealData", errMsg+confirmReasonMes + "，但是送审批失败"));
                            return false;
                        }
                        confirmReason.addOneError(errMsg+confirmReasonMes); //保存送审批信息

                        return false;
                    }
                } //if(isMoneyPurview())
                else
                {
                    //若项目只需检验确认权
                    if (!checkConfirmPurview(tSSRSEdorInfo.GetText(i, 3),
                            projectType))
                    {
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

        //系统定义，无须设置的只送一级的审批
        if (!dealEdorItem())
        {
            return false;
        }

        if(!dealMJ())
        {
            return false;
        }

        return true;
    }

    /**
     * 校验满期结算的权限
     * @return boolean
     */
    private boolean dealMJ()
    {
        String sql = "select EdorValue "
                     + "from LPEdorEspecialData "
                     + "where EdorNo = '" + this.workNo + "' "
                     + "   and EdorType = '" + BQ.EDORTYPE_MJ + "' "
                     + "   and DetailType = '"
                     + BQ.DETAILTYPE_ENDTIME_RATETYPE + "' "
                     + "   and EdorValue in('" + BQ.DEALTYPE_INTERESTINPUT
                     + "', '" + BQ.DEALTYPE_RATEINPUT + "') ";
        String edorValue = new ExeSQL().getOneValue(sql);
        if(edorValue.equals(""))
        {
            //选取的是默认利率，不需要审批
            return true;
        }

        LGProjectPurviewDB tLGProjectPurviewDB = new LGProjectPurviewDB();
        tLGProjectPurviewDB.setProjectNo(BQ.EDORTYPE_MJ);
        tLGProjectPurviewDB.setMemberNo(mGlobalInput.Operator);
        LGProjectPurviewSet set = tLGProjectPurviewDB.query();
        if(set.size() != 0 && set.get(1).getConfirmFlag().equals("1"))
        {
            //有确认权
            return true;
        }

        sql = "select a.UserName "
              + "from LDUser a, LGGroupMember b "
              + "where a.UserCode = b.SuperManager "
              + "   and b.MemberNo = '" + mGlobalInput.Operator + "' ";
        String superManager = new ExeSQL().getOneValue(sql);
        if(superManager.equals("") || superManager.equals("null"))
        {
            CError tError = new CError();
            tError.moduleName = "TaskAutoExaminationBL";
            tError.functionName = "dealMJ";
            tError.errorMessage = "需要送审批，但没有查询到业务上级。";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        String errMsg = queryMyName() + "没有确认权限，送" + superManager + "审批";

        if(!deliver(errMsg, true))
        {
            this.mErrors.addOneError(errMsg + "，但送审批失败。");
            return false;
        }

        CError tError = new CError();
        tError.moduleName = "TaskAutoExaminationBL";
        tError.functionName = "dealMJ";
        tError.errorMessage = errMsg;
        confirmReason.addOneError(tError);
        System.out.println(tError.errorMessage);

        return false;
    }

    private boolean dealEdorItem()
    {
        //只送审一次，如果已经审批过则跳过
        LGTraceNodeOpDB tLGTraceNodeOpDB = new LGTraceNodeOpDB();
        tLGTraceNodeOpDB.setWorkNo(this.workNo);
        tLGTraceNodeOpDB.setOperatorType(Task.HISTORY_TYPE_SDEXAMIN);
        LGTraceNodeOpSet tLGTraceNodeOpSet = tLGTraceNodeOpDB.query();
        if (tLGTraceNodeOpSet.size() > 0)
        {
            return true;
        }

        String edorNo = mLGWorkSchema.getWorkNo();
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++)
        {
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(i);
            String edorType = tLPGrpEdorItemSchema.getEdorType();
            if (edorType.equals(BQ.EDORTYPE_NI))
            {
                String sql = "select count(*) from LCInsuredList " +
                        "where EdorNo = '" + edorNo + "' " +
                        "and EdorPrem is not null ";
                String count = (new ExeSQL()).getOneValue(sql);
                System.out.println(sql);
                if (Integer.parseInt(count) > 0)
                {
                    String errMsg = "增人项目导入保费金额送" + this.superManagerName + "审批";
                    if (!deliver(errMsg, true))
                    {
                        mErrors.addOneError(errMsg + ", 但是送审批失败。"); //保存程序错误信息
                        return false;
                    }
                    confirmReason.addOneError(errMsg);  //保存送审批信息
                    return false;
                }
            }
            else if (edorType.equals(BQ.EDORTYPE_ZT))
            {
                String sql = "select count(*) from LPDiskImport " +
                        "where EdorNo = '" + edorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and Money2 is not null ";
                String count = (new ExeSQL()).getOneValue(sql);
                System.out.println(sql);
                if (Integer.parseInt(count) > 0)
                {
                    String errMsg = "减人项目导入退费金额送" + this.superManagerName +
                            "审批";
                    if (!deliver(errMsg, true))
                    {
                        mErrors.addOneError(errMsg + ", 但是送审批失败。"); //保存程序错误信息
                        return false;
                    }
                    confirmReason.addOneError(errMsg); //保存送审批信息
                    return false;
                }

                EdorItemSpecialData mSpecialData = new EdorItemSpecialData(edorNo, edorType);
                if (mSpecialData.query())
                {
                    String feeRate = mSpecialData.getEdorValue("FeeRate");
                    if (Double.parseDouble(feeRate) != BQ.ZT_FEERATE)
                    {
                        String errMsg = "减人项目修改默认手续费费率送" + this.superManagerName +
                                "审批，手续费比例为" + feeRate;
                        if (!deliver(errMsg, true))
                        {
                            mErrors.addOneError(errMsg + ", 但是送审批失败。"); //保存程序错误信息
                            return false;
                        }
                        confirmReason.addOneError(errMsg); //保存送审批信息
                        return false;
                    }
                }
            }
            else if (edorType.equals(BQ.EDORTYPE_DJ))
            {
                //定期结算工单，进行延期结算，直接上级审批。
                String errMsg = this.myName + "没有" + edorType + "项目的确认权, 送业务上级"
                     + this.superManagerName + "审批";
                 if (!deliver(errMsg, true))
                 {
                     this.mErrors.addOneError(errMsg + "，但送审批失败。");
                     return false;
                 }
             confirmReason.addOneError(errMsg); //保存送审批信息
             return false;
         }
        }
        return true;
    }

    /**
     * 查询项目类型
     * @param customerNo String，客户号
     * @return String， 1：团险，0个险
     */
    private int getProjectType()
    {
        if (customerNo.length() == 9)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

    /**超额数目,目前暂设为0*/
    //qulq 061130 XT 项目超额  超额 = 协议解约金额 - 解约退费
    private double getExcess(String edorType,double money)
    {
        //0 是个单
        double temp = 0.0;
        if(getProjectType()==0)
        {
            //得到总解约保费
            String sql =" select sum(abs(getMoney)) from LPBudgetResult "
                        +" where edortype='"
                        +edorType
                        +"' and edorno ='"
                        +this.mLGWorkSchema.getAcceptNo()
                        +"'"
                        ;
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sql);
            if(tSSRS==null||tSSRS.GetText(1,1).equals("null"))
            {
               return 0.0;
            }
            try
            {
              temp = Double.parseDouble(tSSRS.GetText(1,1));
            }catch(Exception e)
            {
                mErrors.addOneError(e.getMessage());
            }
            money =Math.abs(money);
            return temp-money;
        }
        else
        {
            return 0.0;
        }

    }

    /**
     * 查询保全费用
     * @return SSRS
     */
    private SSRS getGetMoney(String itemTable, String[] tSSRSEdorInfo)
    {
        //查询当前保全项目产生的金额差
        String sql;
        if (itemTable.equals("LPEdorItem"))
        {
            //个险保全
            sql = "select getMoney "
                  + "from " + itemTable + " "
                  + "where edorAcceptNo='" + tSSRSEdorInfo[0]+ "' "
                  + "    and edorNo='" + tSSRSEdorInfo[1] + "' "
                  + "    and edorType='" + tSSRSEdorInfo[2] + "' "
                  + "    and ContNo='" + tSSRSEdorInfo[3] + "' "
                  + "    and InsuredNo='" + tSSRSEdorInfo[4] + "' "
                  + "    and PolNo='" + tSSRSEdorInfo[5] + "' ";
        }
        else
        {
            //团险保全
            sql = "select getMoney "
                  + "from " + itemTable + " "
                  + "where edorAcceptNo='" + tSSRSEdorInfo[0] + "' "
                  + "    and edorNo='" + tSSRSEdorInfo[1] + "' "
                  + "    and edorType='" + tSSRSEdorInfo[2] + "' "
                  + "    and GrpContNo='" + tSSRSEdorInfo[3] + "' ";
        }

        ExeSQL e = new ExeSQL();
        return e.execSQL(sql);
    }

    /**
     * 查询用户保全处理权限
     * @param tSSRSEdorInfo String[]
     * @return SSRS
     */
    private SSRS getMemberPerview(String itemTable,String[] tSSRSEdorInfo)
    {
    	SSRS tSSRS = new SSRS();
    	ExeSQL e = new ExeSQL();
    	if("LPEdorItem".equals(itemTable)&&"WT".equals(tSSRSEdorInfo[2])){
        	String riskcodeSQL = "select count(distinct riskcode) from lcpol where contno ='"+tSSRSEdorInfo[3]+"' " +
			" and riskcode in (select riskcode from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M')";
			String sqlMember =
				"select count(distinct riskcode) "
				+ "from LGProjectPurview "
				+ "where ProjectNo='" + tSSRSEdorInfo[2] + "' "
				+ "   and ProjectType='" + getProjectType() + "' "
				+ "   and DealOrganization='" + getDealOrganization() + "' "
				+ "   and PostNo='" + postNo + "' "
				+ "   and (stateflag!='2' or stateflag is null) "
				+ "   and riskcode  in  (select distinct riskcode from lcpol where contno ='"+tSSRSEdorInfo[3]+"' "
				+ "    and riskcode in (select riskcode from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M') )"
				+ "    and MemberNo='" + mGlobalInput.Operator + "' ";
        	String riskcodeFlag=e.getOneValue(riskcodeSQL);
        	String riskmemberFlag=e.getOneValue(sqlMember);
        	if(riskcodeFlag!=null&&!"1".equals(riskcodeFlag)&&"1".equals(riskmemberFlag)){
    			String sqlGetPurview =
    				"select MoneyPurview,  ExcessPurview, TimePurview, NeedOtherAudit, MemberNo "
    				+ "from LGProjectPurview "
    				+ "where ProjectNo='" + tSSRSEdorInfo[2] + "' "
    				+ "   and ProjectType='" + getProjectType() + "' "
    				+ "   and DealOrganization='" + getDealOrganization() + "' "
    				+ "   and PostNo='" + postNo + "' "
    				+ "   and (stateflag!='2' or stateflag is null) "
    				+ "   and riskcode  in  (select distinct riskcode from lcpol where contno ='"+tSSRSEdorInfo[3]+"' "
    				+ "    and riskcode in (select riskcode from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M') )";
    			String sqlGetPurview1 =
    				"select MoneyPurview, ExcessPurview, TimePurview, NeedOtherAudit, MemberNo "
    				+ "from LGProjectPurview "
    				+ "where ProjectNo='" + tSSRSEdorInfo[2] + "' "
    				+ "   and ProjectType='" + getProjectType() + "' "
    				+ "   and DealOrganization='" + getDealOrganization() + "' "
    				+ "   and PostNo='" + postNo + "' "
    				+ "   and (stateflag!='2' or stateflag is null) "
    				+ "   and (riskcode is null or riskcode='') ";

    			String sql = sqlGetPurview
    			+ "    and MemberNo='" + mGlobalInput.Operator + "' "
    			+ "union "
    			+ sqlGetPurview
    			+ "    and MemberNo='00000000000000000000' "
    			+ "union "
    			+ sqlGetPurview1
    			+ "    and MemberNo='" + mGlobalInput.Operator + "' "
    			+ "union "
    			+ sqlGetPurview1
    			+ "    and MemberNo='00000000000000000000' "
    			+ "order by timepurview,memberno desc   ";
    			
    			tSSRS = e.execSQL(sql);
        	}else
        	{
    			String sqlGetPurview =
    				"select MoneyPurview,  ExcessPurview, TimePurview, NeedOtherAudit, MemberNo "
    				+ "from LGProjectPurview "
    				+ "where ProjectNo='" + tSSRSEdorInfo[2] + "' "
    				+ "   and ProjectType='" + getProjectType() + "' "
    				+ "   and DealOrganization='" + getDealOrganization() + "' "
    				+ "   and PostNo='" + postNo + "' "
    				+ "   and (stateflag!='2' or stateflag is null) "
    				+ "   and riskcode  in  (select distinct riskcode from lcpol where contno ='"+tSSRSEdorInfo[3]+"' "
    				+ "    and riskcode in (select riskcode from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M') )";
    			
    			String sql = sqlGetPurview
    			+ "    and MemberNo='" + mGlobalInput.Operator + "' "
    			+ "union "
    			+ sqlGetPurview
    			+ "    and MemberNo='00000000000000000000' "
    			+ "order by timepurview,memberno desc    ";
    			
    			tSSRS = e.execSQL(sql);
        	}


    	}
    	//判断是否为犹豫期退保或者解约的个险保全项目
    	if(tSSRS.getMaxRow()<1){
        	if("LPEdorItem".equals(itemTable)&&("CT".equals(tSSRSEdorInfo[2])||"WT".equals(tSSRSEdorInfo[2]))){
        		//判断用户保单是否只有一个主险
        		
        	String riskcodeSQL = "select count(distinct riskcode) from lcpol where contno ='"+tSSRSEdorInfo[3]+"' " +
        			" and riskcode in (select riskcode from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M')";
        		String riskcodeFlag=e.getOneValue(riskcodeSQL);
        		if(riskcodeFlag!=null&&"1".equals(riskcodeFlag)){
        			String sqlGetPurview =
        				"select MoneyPurview,  ExcessPurview, TimePurview, NeedOtherAudit, MemberNo "
        				+ "from LGProjectPurview "
        				+ "where ProjectNo='" + tSSRSEdorInfo[2] + "' "
        				+ "   and ProjectType='" + getProjectType() + "' "
        				+ "   and DealOrganization='" + getDealOrganization() + "' "
        				+ "   and PostNo='" + postNo + "' "
        				+ "   and (stateflag!='2' or stateflag is null) "
        				+ "   and riskcode = (select distinct riskcode from lcpol where contno ='"+tSSRSEdorInfo[3]+"' "
        				+ "    and riskcode in (select riskcode from lmriskapp where riskcode=lcpol.riskcode and subriskflag='M') )";
        			
        			String sql = sqlGetPurview
        			+ "    and MemberNo='" + mGlobalInput.Operator + "' "
        			+ "union "
        			+ sqlGetPurview
        			+ "    and MemberNo='00000000000000000000' "
        			+ "order by MemberNo desc ";
        			
        			tSSRS = e.execSQL(sql);
        		}
        	}
    	}

    	if(tSSRS.getMaxRow()<1){
			String sqlGetPurview =
				"select MoneyPurview, ExcessPurview, TimePurview, NeedOtherAudit, MemberNo "
				+ "from LGProjectPurview "
				+ "where ProjectNo='" + tSSRSEdorInfo[2] + "' "
				+ "   and ProjectType='" + getProjectType() + "' "
				+ "   and DealOrganization='" + getDealOrganization() + "' "
				+ "   and PostNo='" + postNo + "' "
				+ "   and (stateflag!='2' or stateflag is null) "
				+ "   and (riskcode is null or riskcode='') ";
			
			String sql = sqlGetPurview
			+ "    and MemberNo='" + mGlobalInput.Operator + "' "
			+ "union "
			+ sqlGetPurview
			+ "    and MemberNo='00000000000000000000' "
			+ "order by MemberNo desc ";
			
			tSSRS = e.execSQL(sql);
    	}
    	
        return tSSRS;
    }

    private String querySuperManagerName()
    {
        String sql = "select userName "
                     + "from LDUser "
                     + "where userCode='" + this.superManager + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1) == null || tSSRS.GetText(1, 1).equals(""))
        {
            return "";
        }
        else
        {
            return tSSRS.GetText(1, 1);
        }
    }

    private String queryMyName()
    {
        String sql = "select userName "
                     + "from LDUser "
                     + "where userCode='" + this.mGlobalInput.Operator + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1) == null || tSSRS.GetText(1, 1).equals(""))
        {
            return "";
        }
        else
        {
            return tSSRS.GetText(1, 1);
        }

    }

    //判断当前工单状态是否为审核通过99
    private boolean isConfirmed()
    {
        String sql;
        sql = "select statusNo "
              + "from LGWork "
              + "where workNo='" + mLGWorkSchema.getWorkNo() + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            if(tSSRS.GetText(1, 1).equals("99"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    //查询当前业务人员的岗位和业务上级
    private boolean queryPoseAndSuper()
    {
        String sql = "select postNo, superManager "
                     + "from LGGroupMember "
                     + "where memberNo='" + mGlobalInput.Operator + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            postNo = tSSRS.GetText(1, 1).trim().toUpperCase();
            superManager = tSSRS.GetText(1, 2).trim();

            return true;
        }
        else
        {
            mErrors.addOneError("没有查到该用户的岗位代码");

            return false;
        }
    }

    private boolean queryCustomerNo()
    {
        //查询客户号
        String sql;
        sql = "select customerNo "
              + "from LGWork "
              + "where workNo='" + mLGWorkSchema.getWorkNo() + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            customerNo = tSSRS.GetText(1, 1);

            return true;
        }
        else
        {
            this.mErrors.addOneError(
                    BuildError.create("askAutoExaminationBL", "dealData",
                                      "查询客户号时出错"));
            return false;
        }

    }

    /**
     * 获取保全期限差
     * @param tSSRSEdorInfo String[]
     * @return String
     */
    private String getTime(String[] tSSRSEdorInfo)
    {
        String inteval = "0";
        //犹豫期退保
        if (tSSRSEdorInfo[2].equals("WT"))
        {
            //个单
//            if (tSSRSEdorInfo[3].length() == 11) //保单号的长度已经看不出是团个单
            int flag = getProjectType();
            if(flag==0) //个单
            {
                inteval = getContIntV(tSSRSEdorInfo);
            }
            else //团单
            {
                inteval = getGrpContIntV(tSSRSEdorInfo);
            }
        }
        if (tSSRSEdorInfo[2].equals("FX"))
        {
            //个单
            String sql = "select date('"+endDate+"') - 2 years from dual";
            SSRS newDate = tExeSQL.execSQL(sql);
            if (tSSRSEdorInfo[3].length() == 11)
              inteval = String.valueOf(PubFun.calInterval(startDate,newDate.GetText(1,1),"D"));
        }
        return inteval;
    }

    private String getContIntV(String[] tSSRSEdorInfo)
    {
        //保单信息
        System.out.println("tSSRSEdorInfo[3]:" + tSSRSEdorInfo[3]);
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tSSRSEdorInfo[3]);
        if (!tLCContDB.getInfo())
        {
            return null;
        }

        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(tSSRSEdorInfo[1]);
        tLPEdorMainDB.setEdorNo(tSSRSEdorInfo[1]);
        tLPEdorMainDB.setContNo(tLCContDB.getContNo());
        tLPEdorMainDB.getInfo();

        ExeSQL e = new ExeSQL();

        //如果没有回执时间
        if (tLCContDB.getCustomGetPolDate() == null
            || "".equals(tLCContDB.getCustomGetPolDate()))
        {
        	//没有回执回销日期不校验超期权限
        	return "-1";
//            String sql = "select days('" + tLPEdorMainDB.getEdorValiDate()
//                         + "') - days('" + tLCContDB.getSignDate()
//                         + "') from LDUser ";
//            String intVal = e.getOneValue(sql);
//
//            return String.valueOf(Integer.parseInt(intVal) - 18);
        }
        else
        {
            String sql = "select days('" + tLPEdorMainDB.getEdorValiDate()
                         + "') - days('" + tLCContDB.getCustomGetPolDate()
                         + "') from LDUser ";
            String intVal = e.getOneValue(sql);

//            int WTPeriod = CommonBL.getWTPeriod(tLCContDB.getContNo());
            
//            return String.valueOf(Integer.parseInt(intVal) - WTPeriod +1);
            return String.valueOf(Integer.parseInt(intVal) - 14);            
        }
    }

    private String getGrpContIntV(String[] tSSRSEdorInfo)
    {
        //保单信息
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tSSRSEdorInfo[3]);
        tLCGrpContDB.getInfo();

        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(tSSRSEdorInfo[1]);
        tLPGrpEdorMainDB.setEdorNo(tSSRSEdorInfo[1]);
        tLPGrpEdorMainDB.getInfo();

        ExeSQL e = new ExeSQL();
        String intVal = null;

        //如果没有回执时间
        if (tLCGrpContDB.getCustomGetPolDate() == null
            || "".equals(tLCGrpContDB.getCustomGetPolDate()))
        {
            String sql = "select days('" + tLPGrpEdorMainDB.getEdorValiDate()
                         + "') - days('" + tLCGrpContDB.getSignDate()
                         + "') from LDUser ";
            intVal = e.getOneValue(sql);

            return String.valueOf(Integer.parseInt(intVal) - 18);
        }
        else
        {
            String sql = "select days('" + tLPGrpEdorMainDB.getEdorValiDate()
                         + "') - days('" + tLCGrpContDB.getCustomGetPolDate()
                         + "') from LDUser ";
            intVal = e.getOneValue(sql);

            return String.valueOf(Integer.parseInt(intVal) - 13);
        }
    }

    //校验当前项目是否需要自动审批，若需要返回true，否则放回false
    private boolean isMoneyPurview(String projectNo)
    {
        String sql = "select distinct projectNo "
                     + "from LGProjectPurview "
                     + "where projectNo='" + projectNo + "' "
                     + "    and ConfirmFlag='2'"
                     + "    and (stateflag!='2' or stateflag is null)   ";

        tSSRS = tExeSQL.execSQL(sql);
        if(tSSRS.getMaxRow() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //检验确认权
    private boolean checkConfirmPurview(String projectNo, int projectType)
    {
        String sql = "select ConfirmFlag, memberNo "
                      + "from LGProjectPurview "
                      + "where ProjectNo='" + projectNo + "' "
                      + "   and ProjectType='" + projectType + "' "
                      + "   and DealOrganization='" + getDealOrganization() + "' "
                      + "   and PostNo='" + postNo + "' "
                      + "   and (stateflag!='2' or stateflag is null) ";

         String sqlConfirm = sql
                             + "  and memberNo='" + mGlobalInput.Operator + "' "
                             + "union "
                             + sql
                             + "  and memberNo='00000000000000000000' "
                             + "order by MemberNo desc ";
         ExeSQL tExeSQL = new ExeSQL();
         SSRS tSSRS = tExeSQL.execSQL(sqlConfirm);
         if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
         {
             String errMsg =
                     this.myName + "没有" + projectNo + "项目的确认权, 送业务上级"
                     + this.superManagerName + "审批";

             if (!deliver(errMsg, true))
             {
                 this.mErrors.addOneError(errMsg + "，但送审批失败。");
                 return false;
             }
             confirmReason.addOneError(errMsg); //保存送审批信息
             return false;
         }

         return true;
    }

    //根据客户号的位数确定是哪一个保全项目表
    private String getItemTable()
    {
        String table = "";
        if(customerNo.length() == 8)
        {
            //团体客户
            table = "LPGrpEdorItem";
            return table;
        }
        else if(customerNo.length() == 9)
        {
            //个体客户
            table = "LPEdorItem";
            return table;
        }
        else
        {
            this.mErrors.addOneError(
                    BuildError.create("TaskAutoExaminationBL",
                                      "getItemTable", "客户号位数不对"));
            return "";
        }
    }

    //得到机构类别
    private String getDealOrganization()
    {
        if((mGlobalInput.ComCode).equals("86"))
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }
    
    //得到岗位级别
    private boolean getMemBerNo(String memberNo){
        String sql3 = " select postno,supermanager,memberNo "
            + " from LGGroupMember "
            + " where memberNo='" + memberNo + "' with ur ";
        tSSRS = tExeSQL.execSQL(sql3);
        
        if(!tSSRS.GetText(1, 1).equals("PA01") && !tSSRS.GetText(1, 1).equals("PA02") && !tSSRS.GetText(1, 1).equals("PA03")){
        	return getMemBerNo(tSSRS.GetText(1, 2));
        }else {
        	superManager=tSSRS.GetText(1, 3);
        }
    	return true;
    }

    /**
     * 实现审核时的工单转交
     * superFlag: 送审或互核标志，true: 送审核，false: 送互核
     */
    private boolean deliver(String errorMsg, boolean superFlag)
    {
        String tCopyFlag = "N";   //不复制转交
        String tDeliverType = "1";   //送审批
        String []mWorkNo = new String[1];  //当前工单号
        mWorkNo[0] = workNo;
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        if(mTransferData != null)
        {
            String remark = (String) mTransferData.getValueByName("remark");
            if (remark != null)
            {
                errorMsg += "(" + StrTool.unicodeToGBK(remark) + ")";
            }
        }
        //处理作业历史
        LGWorkTraceSchema tLGWorkTraceSchema = null;
        LGTraceNodeOpSchema mLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        //送审核
        if(superFlag)
        {
            //查询送审批目标的信箱
            String sql = "select workBoxNo "
                         + "from LGWorkBox "
                         + "where ownerNo='" + superManager + "' ";
            tSSRS = tExeSQL.execSQL(sql);
            if (tSSRS.getMaxRow() == 1)
            {
                //生成作业轨迹
                tLGWorkTraceSchema = new LGWorkTraceSchema();
                tLGWorkTraceSchema.setWorkBoxNo(tSSRS.GetText(1, 1));

                //生成作业历史信息
                mLGTraceNodeOpSchema.setRemark(errorMsg);
            }
            else
            {
                mErrors.addOneError("没查到送审批目标的信箱");
                return false;
            }
        }
        else  //送互核
        {
            //需求取消,此处备扩展
        }
        //生成作业历史信息
        mLGTraceNodeOpSchema.setWorkNo(workNo);
        mLGTraceNodeOpSchema.setNodeNo("0");
        mLGTraceNodeOpSchema.setOperatorType("15");
        mLGTraceNodeOpSchema.setFinishDate(this.mCurrentDate);
        mLGTraceNodeOpSchema.setFinishTime(this.mCurrentTime);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(mWorkNo);
        tVData.add(tCopyFlag);
        tVData.add(tDeliverType);
        tVData.add(new LGWorkSchema());
        tVData.add(tLGWorkTraceSchema);
        tVData.add(mLGTraceNodeOpSchema);
        //如果是个单且需要送审，则先验证下是否有扫描件
        String getSQL = "select 1 from Lpedoritem a where edorno = '" + workNo + "' and exists(select 1 from lccont where contno=a.contno and conttype='1')  with ur" ;
		String saoMiaoSQL = (new ExeSQL()).getOneValue(getSQL);
    	if((saoMiaoSQL != null) && !saoMiaoSQL.equals(""))
    	{
        	String sql = "select 1 from es_doc_main where doccode = '" + workNo + "' and subtype='BQ01'  with ur" ;
			String saoMiaoJian = (new ExeSQL()).getOneValue(sql);
			if ((saoMiaoJian == null) || saoMiaoJian.equals(""))
			{
			   mErrors.addOneError("该工单需要送审，但未上传保全申请书的扫描件，请上传保全申请书的扫描件后再进行操作！");
			   return false;
			}
    	}
        //调用工单转交类送审核
        TaskDeliverBL tTaskDeliverBL = new TaskDeliverBL();
        if (tTaskDeliverBL.submitData(tVData, "ok") == false)
        {
            mErrors.addOneError("送审批失败");

            return false;
        }
        else
        {
        }
        return true;
    }

    /**
     * 判断送是否成功送审批，在调用submitData后调用
     * @return boolean
     */
    public boolean sendConfirmSuccess()
    {
        if(confirmReason.needDealError())
        {
            return true;
        }
        return false;
    }

    /**
     * 得到送审批原因，在调用submitData后调用
     * @return boolean
     */
    public CErrors getSendConfirmReason()
    {
        return confirmReason;
    }

    /**
     * 得到处理信息
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }
    private String getEdorValue(String detailType,LPEdorEspecialDataSet mSpecialDataSet)
    {
        for (int i = 1; i <= mSpecialDataSet.size(); i++)
        {
            LPEdorEspecialDataSchema tEspecialSchema = mSpecialDataSet.get(i);
            String dt = tEspecialSchema.getDetailType();
            if ((dt != null) && (dt.equalsIgnoreCase(detailType)))
            {
                return tEspecialSchema.getEdorValue();
            }
        }
        return null;
    }
    public static void main(String args[])
    {
        String mWorkNo = args[0];
        GlobalInput gi = new GlobalInput();
        LGWorkSchema mLGWorkSchema = new LGWorkSchema();
        VData mVData = new VData();

        gi.Operator = "qulq";
        gi.ComCode = "86";
        mVData.add(gi);

        mLGWorkSchema.setWorkNo(mWorkNo);
        mVData.add(mLGWorkSchema);

        TaskAutoExaminationBL a = new TaskAutoExaminationBL();
        if(!a.submitData(mVData, ""))
        {
            VData v = a.getResult();
            String message = (String)v.getObjectByObjectName("String", 0);
            if(a.mErrors.needDealError())
            {
                System.out.println(a.mErrors.getErrContent());
            }
            else
            {
                System.out.println("失败 ");
            }
        }
        else
        {
            System.out.println("审批通过");
        }
    }
}
