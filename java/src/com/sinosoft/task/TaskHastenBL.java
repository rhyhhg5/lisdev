package com.sinosoft.task;

import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.finfee.PausePayFeeUI;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LGTraceNodeOpSchema;
import com.sinosoft.lis.schema.LGPhoneHastenSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */


public class TaskHastenBL
{
/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private SSRS tSSRS = new SSRS();
    private ExeSQL tExeSQL = new ExeSQL();

    public TaskHastenBL()
    {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in TaskHastenBL submit");

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
        {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false)
        {
            return false;
        }
        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskBackBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) mInputData.
                            getObjectByObjectName("TransferData", 0);
        }
        catch(Exception e)
        {
            this.mErrors.addOneError(new CError(
                    "传入入后台的数据不完整", "TaskHastenBL", "getInputData"));

            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        String sql;

        if (this.mOperate.equals("1"))
        {
            //催缴成功
            String sqlStr =
                    "select a.BankCode, a.BankAccno, a.AccName "
                    + "from LCCont a, LPEdorMain b "
                    + "where a.ContNo = b.ContNo "
                    + "and   b.EdorAcceptNo = '"
                    + (String)mTransferData.getValueByName("edorAcceptNo")+"' ";

            tSSRS = tExeSQL.execSQL(sqlStr);
            if (tSSRS.getMaxRow() > 0)
            {
                mTransferData.setNameAndValue("bank", tSSRS.GetText(1, 1));
                mTransferData.setNameAndValue("bankAccno", tSSRS.GetText(1, 2));
                mTransferData.setNameAndValue("accName", tSSRS.GetText(1, 3));
                String payDate = (String) mTransferData.getValueByName(
                        "payDate");
//              deleted by huxl @ 2006-12-20 银行转账目前已经不再往后延2个月
//                ExeSQL e = new ExeSQL();
//                payDate = e.getOneValue("select date('"
//                                        + payDate + "') - 2 month "
//                                        + "from LDUser ");
                mTransferData.removeByName("payDate");
                mTransferData.setNameAndValue("payDate", payDate);
                System.out.println(payDate);
            }
            SetPayInfo tSetPayInfo = new SetPayInfo(
                    (String) mTransferData.getValueByName("edorAcceptNo"));

            VData data = new VData();
            data.add(this.mGlobalInput);
            data.add(mTransferData);

            //交费
            if (!tSetPayInfo.submitDate(data, "1"))
            {
                this.mErrors.copyAllErrors(tSetPayInfo.mErrors);

                return false;
            }

            //工单结案，同时改变催缴任务状态
            LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
            tLGPhoneHastenSchema.setWorkNo((String)mTransferData.getValueByName("workNo"));
            tLGPhoneHastenSchema.setFinishType("1");

            VData tVData = new VData();
            tVData.add(this.mGlobalInput);
            tVData.add(mTransferData);
            tVData.add(tLGPhoneHastenSchema);

            TaskChangeHastenStatusUI tTaskChangeHastenStatusUI = new TaskChangeHastenStatusUI();
            if (!tTaskChangeHastenStatusUI.submitData(tVData, "Success"))
            {
                this.mErrors.copyAllErrors(tTaskChangeHastenStatusUI.mErrors);

                return false;
            }

            //取消财务收费暂停
            LJSPaySchema tLJSPaySchema = new LJSPaySchema();
            VData v = new VData();

            String s = "select GetNoticeNo from LJSPay where otherNo='"
                       + (String) mTransferData.getValueByName("edorAcceptNo") + "' ";
            tSSRS = tExeSQL.execSQL(s);
            if (tSSRS.getMaxRow() == 0)
            {
                this.mErrors.addOneError(
                        new CError("查询保全受理号"
                                   + (String) mTransferData.
                                   getValueByName("edorAcceptNo")
                                   + "对应的退费通知号出错",
                                   "TaskHastenBL", "dealData"));
                return false;
            }

            tLJSPaySchema.setGetNoticeNo(tSSRS.GetText(1, 1));
            tLJSPaySchema.setOtherNoType("3"); //保全
            tLJSPaySchema.setBankOnTheWayFlag("2");
            v.add(tLJSPaySchema);

            PausePayFeeUI tPausePayFeeUI = new PausePayFeeUI();
            if(!tPausePayFeeUI.submitData(v, "Cancel"))
            {
                this.mErrors.copyAllErrors(tPausePayFeeUI.mErrors);

                return false;
            }

        }

        //若电话未通， 则只生成历史信息
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        tLGTraceNodeOpSchema.setWorkNo(
                (String) mTransferData.getValueByName("workNo"));
        tLGTraceNodeOpSchema.setNodeNo(
                queryNodeNo(tLGTraceNodeOpSchema.getWorkNo()));
        tLGTraceNodeOpSchema.setOperatorType("7");
        tLGTraceNodeOpSchema.setFinishDate(this.mCurrentDate);
        tLGTraceNodeOpSchema.setFinishTime(this.mCurrentTime);
        tLGTraceNodeOpSchema.setRemark(
                (String) mTransferData.getValueByName("remark"));

        VData inputData = new VData();
        TaskTraceNodeOpBL creatNode = new TaskTraceNodeOpBL();

        inputData.add(tLGTraceNodeOpSchema);
        inputData.add(this.mGlobalInput);
        if (!creatNode.submitData(inputData, "INSERT||MAIN"))
        {
            this.mErrors.addOneError(new CError("生成作业历史轨迹节点时出错",
                                                "TaskHastenBL", "dealData"));

            return false;
        }

        if (mOperate.equals("CreateHistory"))
        {
            String strSql =
                    "update LGWork "
                    + "set operator = '" + mGlobalInput.Operator + "', "
                    + "    modifyDate = '" + this.mCurrentDate + "', "
                    + "    modifyTime = '" + this.mCurrentTime + "' "
                    + "where workNo = '"
                    + (String) mTransferData.getValueByName("workNo") + "' ";
            map.put(strSql, "UPDATE");
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
       // mResult.add();
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

    private String queryNodeNo(String workNo)
    {
        String sql = "select max(nodeNo) "
                     + "from LGTraceNodeOp "
                     + "where workNo='" + workNo + "' ";
        tSSRS = tExeSQL.execSQL(sql);
        if(tSSRS.getMaxRow() > 0)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            return "0";
        }
    }
}
