package com.sinosoft.task;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.LGPhoneHastenSet;
import com.sinosoft.lis.db.LGPhoneHastenDB;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 催缴工单结案
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class TaskPhoneFinishBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private LGPhoneHastenSchema mLGPhoneHastenSchema = null;  //电话催缴信息
    private GlobalInput mGlobalInput = null;  //操作员信息
    private MMap map = new MMap();

    public TaskPhoneFinishBL()
    {
    }

    /**
     * 数据提交的公共方法，进行逻辑处理并提交数据库，
     * 提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象：包括：
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LGPhoneHastenSchema对象，催缴工单信息
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }
        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(tPubSubmit.submitData(data, ""))
        {
            mErrors.addOneError("提交数据出错");
            return false;
        }
        return true;
    }

    /**
     * 数据提交的公共方法，进行业务逻辑处理后返回处理后的数据机和MMp
     * 提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象：包括：
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LGPhoneHastenSchema对象，催缴工单信息
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        //进行业务处理
        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: VData，getSubmitData中传入的参数
     * @return: boolean：操作成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mLGPhoneHastenSchema = (LGPhoneHastenSchema) data
                               .getObjectByObjectName("LGPhoneHastenSchema", 0);
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        if(mGlobalInput == null || mLGPhoneHastenSchema == null)
        {
            this.mErrors.addOneError("输入数据有误");
            return false;
        }
        System.out.println("Time " + PubFun.getCurrentDate() + " "
                           + PubFun.getCurrentTime() + ": "
                           + mGlobalInput.Operator + " ");

        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        String remark = mLGPhoneHastenSchema.getRemark();
        String finishType = mLGPhoneHastenSchema.getFinishType();

        LGPhoneHastenDB db = mLGPhoneHastenSchema.getDB();
        if(!db.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "TaskPhoneFinishBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有查询到电话催缴信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mLGPhoneHastenSchema = db.getSchema();
        mLGPhoneHastenSchema
            .setRemark(StrTool.cTrim(mLGPhoneHastenSchema.getRemark()) + remark);
        mLGPhoneHastenSchema.setFinishType(finishType);

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * 若正催缴，根据GetNoticeNo从LGPhoneHasten表得到电话催缴工单的WorkNo
     调用TaskFinishBL类使电话催缴工单工单结案
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        VData data = new VData();
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        LGWorkRemarkSchema tLGWorkRemarkSchema = new LGWorkRemarkSchema();
        tLGWorkSchema.setWorkNo(mLGPhoneHastenSchema.getWorkNo());
        tLGWorkRemarkSchema.setRemarkContent(mLGPhoneHastenSchema.getRemark());

        data.add(tLGWorkSchema);
        data.add(tLGWorkRemarkSchema);
        data.add(mGlobalInput);

        TaskFinishBL tTaskFinishBL = new TaskFinishBL();
        MMap tMMap = tTaskFinishBL.getSubmitData(data, "");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tTaskFinishBL.mErrors);
            return false;
        }
        map.add(tMMap);

        map.put(mLGPhoneHastenSchema, SysConst.UPDATE);

        return true;
    }


    public static void main(String[] args)
    {
        TaskPhoneFinishBL taskphonefinishbl = new TaskPhoneFinishBL();
    }
}
