package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LGLetterSchema;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description: 函件预览借口类</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PrtLetterContentUI
{
    /**
     * 错误的容器，若数据提交失败，则可从里面得到失败的原因
     */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    public PrtLetterContentUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        PrtLetterContentBL bl = new PrtLetterContentBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        else
        {
            this.mResult = bl.getResult();
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String args[])
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "";

        LGLetterSchema schema = new LGLetterSchema();
        schema.setEdorAcceptNo("20060903000004");
        schema.setSerialNumber("2");
        schema.setLetterType("0");

        VData tVData = new VData();
        VData mResult = new VData();

        tVData.add(tG);
        tVData.add(schema);

        PrtLetterContentUI tPrtLetterContentUI = new PrtLetterContentUI();
        XmlExport txmlExport = new XmlExport();
        if (!tPrtLetterContentUI.submitData(tVData, "PRINT"))
        {
            System.out.println(tPrtLetterContentUI.mErrors.getFirstError().toString());
        }
        else
        {
            mResult = tPrtLetterContentUI.getResult();
            txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);

            if (txmlExport == null)
            {
                System.out.println("没有得到要显示的数据文件");
            }

            System.out.println("OK");
        }

    }
}
