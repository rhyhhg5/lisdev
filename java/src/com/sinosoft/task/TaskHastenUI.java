package com.sinosoft.task;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskHastenUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public TaskHastenUI()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        if(!checkData())
        {
            return false;
        }

        TaskHastenBL tTaskHastenBL = new TaskHastenBL();
        if (tTaskHastenBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tTaskHastenBL.mErrors);
            mResult.clear();
            System.out.println("调用TaskHastenBL时出错：" +
                               tTaskHastenBL.mErrors.getErrContent());

            return false;
        }

        mResult = tTaskHastenBL.getResult();

        return true;
    }

    private boolean checkData()
    {
        TransferData tTransferData = (TransferData) mInputData.
                                     getObjectByObjectName("TransferData", 0);
        String payDate = (String) tTransferData.getValueByName("payDate");
        ExeSQL tExeSQL = new ExeSQL();

        if(this.mOperate.equals("1"))
        {
            String sql =
                    "select * "
                    + "from LDUser "
                    + "where (days('" + payDate + "') - days('"
                    + PubFun.getCurrentDate() + "') > 5)";
            String result = tExeSQL.getOneValue(sql);

            if ((result != null) && (!result.equals("")))
            {
                this.mErrors.addOneError(
                        new CError("输入新缴费日期须在从明天起的5日之内，请重新输入",
                                   "TaskHastenUI", "checkData"));
                System.out.println("TaskHastenUI中checkData方法中报错："
                                   + "输入的新缴费日期在今天的5日之外，请重新输入");

                return false;
            }
        }

        String sql = "select * "
                     + "from LJSPay "
                     + "where otherNo='"
                     + (String)tTransferData.getValueByName("edorAcceptNo")+"'";
        String result = tExeSQL.getOneValue(sql);
        System.out.println(result);
        if (result == null || result.equals(""))
        {
            String errMsg = "受理号为"
                            + (String) tTransferData.
                            getValueByName("edorAcceptNo")
                            + "的保全受理可能已被撤销, 电话催缴任务无法进行下去,请手动结案";

            mErrors.addOneError(new CError(errMsg, "TaskHastenUI", "checkData"));
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        TransferData tTransferData = new TransferData();

        tG.Operator = "endor";
        tG.ComCode = "86";
        tG.ManageCom = "";

        tTransferData.setNameAndValue("workNo", "20050923000005");
        tTransferData.setNameAndValue("payMode", "1");
        tTransferData.setNameAndValue("payDate", "2005-09-24");
        tTransferData.setNameAndValue("edorAcceptNo", "20050923000001");
        tTransferData.setNameAndValue("remark", "在BL中测试");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(tG);
        System.out.println(tG.Operator + " " + tG.ComCode + " " + tG.ManageCom);

        TaskHastenUI tTaskHastenUI = new TaskHastenUI();
        if (!tTaskHastenUI.submitData(tVData, "1"))
        {
            System.out.println("失败: " + tTaskHastenUI.mErrors.getErrContent());
        }
        else
        {
            System.out.println("成功");
        }
    }

}
