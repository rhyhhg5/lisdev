package com.sinosoft.task;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CreateSql
{
    public CreateSql()
    {
    }

    /**
     * description 需要缴费的保全项目超期未交费的，系统生成催缴记录，
     * 分为日处理程序触发和人工触发两种：
     *     日处理程序触发：对所有符合催缴条件的缴费记录生成催缴任务
     *     人工触发：只对符合催缴条件并且在操作员机构及以下机构范围内的缴费记录生成催缴任务
     *
     * @param type boolean
     *     true: 第一次到期, 应生成催缴记录
     *     false: 第二次到期, 应撤销保全申请
     * @param mGlobalInput GlobalInput
     *     mGlobalInput == null: 日处理程序触发
     *     mGlobalInput != null: 人工触发
     * @return String
     */
    public static String getHastenSql(boolean type, GlobalInput mGlobalInput)
    {
        String sql;
        String sql2;
        String condition;

        if(type == true)
        {
            //第一次到期，应催缴
            condition = "    and p.otherNo not in "
                        + "        (select edorAcceptNo "
                        + "        from LGPhoneHasten "
                        + "        )";
        }
        else
        {
            //第二次到期，应撤销
            condition = "    and p.otherNo in "
                        + "        (select edorAcceptNo "
                        + "        from LGPhoneHasten a, LGWork b "
                        + "        where a.workNo=b.workNo "
                        + "            and b.StatusNo='5' "
                        + "            and a.FinishType='1' "
                        + "        )";
        }

        //日处理程序自动运行
        if(mGlobalInput == null)
        {
            //现金缴费方式
            sql =
                    "select e.EdorAcceptNo, e.otherNo, p.BankCode, p.BankAccNo, PayDate, p.GetNoticeNo "
                    + "from LJSPay p, LPEdorApp e "
                    + "where p.OtherNo = e.EdorAcceptNo "
                    + "    and p.OtherNoType = '10' " //应收表中标识保全个单缴费
                    + "    and e.othernotype = '1' " //保全申请表中标识个单申请
                    + "    and p.PayDate < '" + PubFun.getCurrentDate() + "' "
                    + "    and p.BankAccNo is null " //以帐号空标识现金转帐
                    + condition;

            //银行转账
            sql2 =
                    "select e.EdorAcceptNo, e.otherNo, p.BankCode, p.BankAccNo, PayDate, p.GetNoticeNo "
                    + "from LJSPay p, LPEdorApp e "
                    + "where p.OtherNo = e.EdorAcceptNo "
                    + "    and p.OtherNoType = '10' "
                    + "    and e.othernotype = '1' "
                    + "    and p.BankAccNo is not null " //以帐号非空标识银行转帐
                    + "    and (p.PayDate < '"
                    + PubFun.getCurrentDate() + "' or SendBankCount > 1)" //送银行次数为2
                    + "    and (BankSuccFlag='0'  or BankSuccFlag is null) " //转帐不成功
                    + "    and (BankOnTheWayFlag='0' or BankOnTheWayFlag is null) " //不在送银行的途中
                    + condition;
        }
        //只能操纵本机构及本机构的下级机构的催缴
        else
        {
            //现金缴费方式
           sql =
                   "select e.EdorAcceptNo, e.otherNo, p.BankCode, p.BankAccNo, PayDate, p.GetNoticeNo "
                   + "from LJSPay p, LPEdorApp e, LDUser u "
                   + "where p.OtherNo = e.EdorAcceptNo "
                   + "    and p.operator = u.userCode "
                   + "    and u.comCode like '" + mGlobalInput.ComCode + "%'"
                   + "    and p.OtherNoType = '10' " //应收表中标识保全个单缴费
                   + "    and e.othernotype = '1' " //保全申请表中标识个单申请
                   + "    and p.PayDate < '" + PubFun.getCurrentDate() + "' "
                   + "    and p.BankAccNo is null " //以帐号空标识现金转帐
                   + condition;

           //银行转账
           sql2 =
                   "select e.EdorAcceptNo, e.otherNo, p.BankCode, p.BankAccNo, PayDate, p.GetNoticeNo "
                   + "from LJSPay p, LPEdorApp e, LDUser u "
                   + "where p.OtherNo = e.EdorAcceptNo "
                   + "    and p.operator = u.userCode "
                   + "    and u.comCode like '" + mGlobalInput.ComCode + "%'"
                   + "    and p.OtherNoType = '10' "
                   + "    and e.othernotype = '1' "
                   + "    and p.BankAccNo is not null " //以帐号非空标识银行转帐
                   + "    and (p.PayDate < '"
                   + PubFun.getCurrentDate() + "' or SendBankCount > 1)"//送银行次数为2
                   + "    and (BankSuccFlag='0'  or BankSuccFlag is null) " //转帐不成功
                   + "    and (BankOnTheWayFlag='0' or BankOnTheWayFlag is null) " //不在送银行的途中
                   + condition;
        }

        return sql + " union " + sql2;
    }

    public static void main(String[] args)
    {
        CreateSql createsql = new CreateSql();
    }
}
