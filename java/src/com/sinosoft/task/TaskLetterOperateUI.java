package com.sinosoft.task;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LGLetterSchema;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *    响应对函件的操作
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskLetterOperateUI
{
    /**
     * 错误的容器
     * */
    public CErrors mErrors = new CErrors();

    public TaskLetterOperateUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        TaskLetterOperateBL tTaskLetterOperateBL = new TaskLetterOperateBL();

        if(!tTaskLetterOperateBL.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(tTaskLetterOperateBL.mErrors);

            return false;
        }

        return true;
    }

    public static void main(String args[])
    {
        LGLetterSchema s = new LGLetterSchema();
        GlobalInput g = new GlobalInput();

        g.Operator = "endor";
        g.ComCode = "86";

        s.setEdorAcceptNo("20051031000002");
        s.setSerialNumber("1");
        s.setBackFlag("1");
        s.setBackDate("2005-10-31");
        s.setFeedBackInfo("asdfsdf");

        VData v = new VData();
        v.add(g);
        v.add(s);

        TaskLetterOperateUI ui = new TaskLetterOperateUI();
        if(!ui.submitData(v, "FeedBack"))
        {
            System.out.println(ui.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
