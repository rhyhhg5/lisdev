package com.sinosoft.task;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import java.util.*;
import com.sinosoft.lis.pubfun.*;
import java.io.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.tb.*;
import java.sql.*;
import org.jdom.Element;
import java.util.zip.*;
import com.sinosoft.lis.pubfun.ftp.*;
import java.net.*;
import org.apache.commons.net.ftp.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class Untitled1
{
    public static final long a = new Long(100).intValue();

    private void printObjectNames(MMap tMMap)
    {
        for (int j = 0; j < tMMap.keySet().size(); j++)
        {
            Object o = tMMap.getOrder().get(String.valueOf(j + 1));
            System.out.println(o);
        }
        System.out.println();
    }

    private static void testMMap()
    {
        Untitled1 u = new Untitled1();

        MMap map = new MMap();

        LCAddressSet set = new LCAddressSet();
        map.put(set, SysConst.DELETE);
        u.printObjectNames(map);

        String sql = "delete from Dual where 1 = 0 ";
        map.put(sql, SysConst.DELETE);
        u.printObjectNames(map);

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo("xxx");
        map.put(tLCContSchema, SysConst.DELETE);
        u.printObjectNames(map);

        map.put(sql, SysConst.DELETE);
        u.printObjectNames(map);

        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonSchema.setCustomerNo("xxx");
        map.put(tLDPersonSchema, SysConst.DELETE);
        u.printObjectNames(map);

        //再次放入对象schema，这样也会导致无法读取其他对象，见本次的输出
        map.put(set, SysConst.DELETE);
        u.printObjectNames(map);
    }

    public static void main(String args[]) throws SocketException, IOException
    {
        testMMap();
    }

    public void run()
    {}
}
