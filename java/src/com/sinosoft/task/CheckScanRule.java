package com.sinosoft.task;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LGAutoDeliverRuleDB;
import com.sinosoft.lis.vschema.LGAutoDeliverRuleSet;
import com.sinosoft.lis.db.LGWorkBoxDB;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class CheckScanRule extends CheckRule
{
    /**
     * 错误的容器
     */
    private String mOperate = null;
    private LGAutoDeliverRuleSchema mLGAutoDeliverRuleSchema = null;

    public CheckScanRule()
    {
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }

        if(!check(cOperate))
        {
            return false;
        }

        return true;
    }

    /**
     * descirption 将传入的数据分配给本类的变量
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLGAutoDeliverRuleSchema =
                (LGAutoDeliverRuleSchema) cInputData.
                getObjectByObjectName("LGAutoDeliverRuleSchema", 0);
        if (mLGAutoDeliverRuleSchema == null)
        {
            mErrors.addOneError("传入的数据不完整");
            System.out.println("传入到CheckScanRule的数据不完整 ");
            return false;
        }

        return true;
    }

    //校验数据合法性
    private boolean check(String cOperate)
    {
        //校验新建规则时的数据合法性
        if (cOperate.equals("INSERT||MAIN"))
        {
            if (!checkInsert())
            {
                return false;
            }
        }
        //校验修改规则时的数据合法性
        else if (cOperate.equals("UPDATE||MAIN"))
        {
            if (!checkInsert())
            {
                return false;
            }

            if(!checkUpdate())
            {
                return false;
            }
        }

        return true;
    }

    //校验新建的规则的合法性
    public boolean checkInsert()
    {
        if (mLGAutoDeliverRuleSchema.getRuleContent() == null
            ||mLGAutoDeliverRuleSchema.getRuleContent().equals("")
            || mLGAutoDeliverRuleSchema.getSourceComCode() == null
            || mLGAutoDeliverRuleSchema.getSourceComCode().equals("")
            || mLGAutoDeliverRuleSchema.getGoalType() == null
            || mLGAutoDeliverRuleSchema.getGoalType().equals("")
            || mLGAutoDeliverRuleSchema.getTarget() == null
            || mLGAutoDeliverRuleSchema.getTarget().equals(""))
        {
            mErrors.addOneError("规则内容、机构、规则类型、目标均不能为空。");
            System.out.println("CheckScanRule->规则内容、机构、规则类型、目标均不能为空。");
            return false;
        }

        LGAutoDeliverRuleDB db = new LGAutoDeliverRuleDB();
        db.setRuleContent(mLGAutoDeliverRuleSchema.getRuleContent());
        db.setSourceComCode(mLGAutoDeliverRuleSchema.getSourceComCode());
        db.setGoalType(mLGAutoDeliverRuleSchema.getGoalType());
        db.setTarget(mLGAutoDeliverRuleSchema.getTarget());
        LGAutoDeliverRuleSet set = db.query();
        if (mOperate.equals("INSERT||MAIN") && set.size() > 0)
        {
            mErrors.addOneError("已经存在相同的规则，您不能再录入。");
            System.out.println("CheckScanRule->已经存在相同的规则，您不能再录入。");
            return false;
        }

        LGWorkBoxDB boxDB = new LGWorkBoxDB();
        boxDB.setWorkBoxNo(mLGAutoDeliverRuleSchema.getTarget());
        if(!boxDB.getInfo())
        {
            mErrors.addOneError("目标信箱不存在，请确认");
            System.out.println("CheckScanRule->目标信箱不存在，请确认");
            return false;
        }

        return true;
    }

    //修改扫描规则时的校验
    private boolean checkUpdate()
    {
        if (mLGAutoDeliverRuleSchema.getRuleNo() == null)
        {
            mErrors.addOneError("页面发生错误，没有取到规则编号，给您带来不便，请见谅。");
            System.out.println("CheckScanRule->页面发生错误，没有取到规则编号，给您带来不便，请见谅。");
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        CheckScanRule checkscanrule = new CheckScanRule();
    }
}
