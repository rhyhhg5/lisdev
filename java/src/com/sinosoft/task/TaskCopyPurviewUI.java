package com.sinosoft.task;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LGProjectPurviewSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: 与页面交互的类，仅做数据的校验，不做任何业务上的处理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class TaskCopyPurviewUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public TaskCopyPurviewUI()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        TaskCopyPurviewBL tTaskCopyPurviewBL = new TaskCopyPurviewBL();
        if (tTaskCopyPurviewBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tTaskCopyPurviewBL.mErrors);
            mResult.clear();
            System.out.println("调用TaskHastenBL时出错：" +
                               tTaskCopyPurviewBL.mErrors.getErrContent());

            return false;
        }

        mResult = tTaskCopyPurviewBL.getResult();

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        LGProjectPurviewSchema schema = new LGProjectPurviewSchema();

        tG.Operator = "endor";
        tG.ComCode = "86";
        tG.ManageCom = "";

        schema.setPostNo("PA06");
        schema.setMemberNo("endor8");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(schema);
        tVData.add(tG);
        System.out.println(tG.Operator + " " + tG.ComCode + " " + tG.ManageCom);

        TaskCopyPurviewUI tTaskCopyPurviewUI = new TaskCopyPurviewUI();
        if (!tTaskCopyPurviewUI.submitData(tVData, "INSERT||MAIN"))
        {
            System.out.println("失败");
        }
    }

}
