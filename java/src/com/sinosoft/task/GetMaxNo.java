package com.sinosoft.task;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 *
 * <p>Description: 获得给定表的给定字段的下一升序编号,反回其字符串</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GetMaxNo
{
    public GetMaxNo()
    {
    }

    //获得给定表的给定字段的下一升序编号,反回其字符串
    public static String getString(String tableName, String fieldName)
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        //查询最大项目岗位权限编号
        String sql = "select max(int(" + fieldName + ")) "
                     + "from " + tableName;
        try
        {
            tSSRS = tExeSQL.execSQL(sql);

            if (tSSRS.getMaxRow() == 1)
            {
                long maxNo = Long.parseLong(tSSRS.GetText(1, 1)) + 1;

                return Long.toString(maxNo);
            }
            else
            {
                return "1";
            }
        }
        catch (Exception e)
        {
            return "-1";
        }
    }

    //获得给定表的给定字段的下一升序编号,反回其long
    public static long getLong(String tableName, String fieldName)
    {
        String maxNo = getString(tableName, fieldName);

        return Long.parseLong(maxNo);
    }
}
