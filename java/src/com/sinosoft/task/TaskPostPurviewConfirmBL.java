package com.sinosoft.task;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LGProjectPurviewSchema;
import com.sinosoft.lis.vschema.LGProjectPurviewSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 快速新建岗位级别的确认权
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class TaskPostPurviewConfirmBL
{
    /**
     * 错误容器
     */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private LGProjectPurviewSet mLGProjectPurviewSet = null;

    public TaskPostPurviewConfirmBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        CheckPurviewManage tCheckPurviewManage = new CheckPurviewManage();
        if(!tCheckPurviewManage.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(tCheckPurviewManage.mErrors);
            return false;
        }

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!prepareData())
        {
            return false;
        }

        MMap map = new MMap();
        map.put(this.mLGProjectPurviewSet, "INSERT");
        VData data = new VData();
        data.add(map);

        //如果是新建权限,则给相应岗位的成员分配分配权限
        MMap m = null;
        if (operate.equals("INSERT||MAIN"))
        {
            VData mResult = new VData();
            mResult.add(mGlobalInput);
            mResult.add(this.mLGProjectPurviewSet);

            TaskCopyOnePostPurviewUI ui = new TaskCopyOnePostPurviewUI();
            m = ui.getDealtMap(mResult, "INSERT||MAIN");
            if (m == null)
            {
                mErrors.copyAllErrors(ui.mErrors);
                return false;
            }
            map.add(m);
        }


        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, operate))
        {
            mErrors.copyAllErrors(p.mErrors);
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mLGProjectPurviewSet = (LGProjectPurviewSet) cInputData.
                                   getObjectByObjectName("LGProjectPurviewSet", 0);
            if(mGlobalInput == null || mLGProjectPurviewSet == null)
            {
                throw new Exception();
            }
        }
        catch(Exception e)
        {
            mErrors.addOneError("对不起，传入的数据不完整。");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 校验录入数据的合法性
     * @return boolean， true：合法
     */
    private boolean checkData()
    {
        for(int i = 1; i < this.mLGProjectPurviewSet.size(); i++)
        {
            if(mLGProjectPurviewSet.get(i).getConfirmFlag().
               compareTo(mLGProjectPurviewSet.get(i + 1).getConfirmFlag()) < 0)
            {
                mErrors.addOneError(
                        "岗位级别为 " + GetMemberInfo.
                        getPostName(mLGProjectPurviewSet.get(i).getPostNo())
                        + " 的确认权不能低于岗位级别为 "
                        + GetMemberInfo.
                        getPostName(mLGProjectPurviewSet.get(i + 1).getPostNo())
                        + " 的确认权");
                return false;
            }
        }

        if(!checkDBData())
        {
            return false;
        }

        return true;
    }

    /**
     * 检验库中是否有相应的数据
     * @return boolean， true：有
     */
    private boolean checkDBData()
    {
        boolean failFlag = false;  //是否有错误发生的标志
        String errMsg = "";   //错误信息
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = null;

        for(int i = 1; i <= this.mLGProjectPurviewSet.size(); i++)
        {
            String sql = "select projectPurviewNo "
                         + "from LGProjectPurview "
                         + "where projectNo = '"
                         + mLGProjectPurviewSet.get(i).getProjectNo() + "' "
                         + "    and projectType = '"
                         + mLGProjectPurviewSet.get(i).getProjectType() + "' "
                         + "    and postNo = '"
                         + mLGProjectPurviewSet.get(i).getPostNo() + "' "
                         + "    and memberNo = '00000000000000000000'";
            tSSRS = tExeSQL.execSQL(sql);
            if(tSSRS != null && tSSRS.getMaxRow() > 0)
            {
                failFlag = true;
                errMsg += GetMemberInfo.getPostName(
                        mLGProjectPurviewSet.get(i).getPostNo()) + " ";
            }
        }
        if (failFlag)
        {
            mErrors.addOneError("本次录入权限中的 " + errMsg
                                + "已存在，您不能进行批次录入，请针对每个岗位进行权限设置");
            return false;
        }

        return true;
    }

    /**
     * 准备数据备入库
     * @return boolean, true：成功准备
     */
    private boolean prepareData()
    {

//        CommonBL bl = new CommonBL();
//        long maxNo = bl.getMaxNoLong("LGProjectPurview", "ProjectPurviewNo");
//        if(bl.mErrors.needDealError())
//        {
//            mErrors.addOneError("没有查询到相应的数据");
//            return false;
//        }

        for (int i = 1; i <= this.mLGProjectPurviewSet.size(); i++)
        {
//            mLGProjectPurviewSet.get(i).setProjectPurviewNo(
//                    String.valueOf(maxNo + i));
        	mLGProjectPurviewSet.get(i).setProjectPurviewNo(PubFun1.CreateMaxNo("TASKPURVIEW", null));
            if (i <= 3)
            {
                mLGProjectPurviewSet.get(i).setDealOrganization("0"); //总公司
            }
            else
            {
                mLGProjectPurviewSet.get(i).setDealOrganization("1");  //分公司
            }

            mLGProjectPurviewSet.get(i).setMemberNo("00000000000000000000");
            mLGProjectPurviewSet.get(i).setOperator(mGlobalInput.Operator);
            mLGProjectPurviewSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLGProjectPurviewSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLGProjectPurviewSet.get(i).setMakeDate(PubFun.getCurrentDate());
            mLGProjectPurviewSet.get(i).setMakeTime(PubFun.getCurrentTime());
        }


        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.ComCode = "86";
        g.Operator = "endor";

        LGProjectPurviewSet set = new LGProjectPurviewSet();
        for(int i = 1; i <= 6; i++)
        {
            LGProjectPurviewSchema schema = new LGProjectPurviewSchema();
            schema.setProjectNo("XX");
            schema.setProjectType("0");
            schema.setPostNo("PA0" + i);
            schema.setMoneyPurview("0");
            schema.setExcessPurview(0);
            schema.setTimePurview("0");
            schema.setRemark("可以删除");
            schema.setNeedOtherAudit("0");
            schema.setConfirmFlag("0");
            schema.setMemberNo("");

            set.add(schema);
        }
        VData v = new VData();
        v.add(g);
        v.add(set);

        TaskPostPurviewConfirmBL bl = new
                TaskPostPurviewConfirmBL();
        if(!bl.submitData(v, "INSERT||MAIN"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }

    }
}
