package com.sinosoft.task;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.PEdorADDetailBL;
import com.sinosoft.lis.bq.PEdorAppItemBL;
import com.sinosoft.lis.bq.PEdorAppItemUI;
import com.sinosoft.lis.bq.PEdorCMDetailBL;
import com.sinosoft.lis.bq.PEdorConfirmBL;
import com.sinosoft.lis.bq.PrtAppEndorsementBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPContSubSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BQCMEdorBL  {

/** 错误保存容器 */
public CErrors mErrors = new CErrors();

/** 全局变量 */
private GlobalInput mGlobalInput = null;

private LGWorkSchema mLGWorkSchema = null;

private LCInsuredSchema mLCInsuredSchema = null;


private String mTypeFlag = null;

private String mGUFlag = null;


private String mEdorAcceptNo = null;

private String mContNo = null;

private String mEdorType = BQ.EDORTYPE_CM;

private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
private LPContSubSchema mLPContSubSchema = new LPContSubSchema();
private String mCustomerno="";

boolean isAppntFlag=false;

/** 当前日期 */
private String mCurrentDate = PubFun.getCurrentDate();

/** 当前时间 */
private String mCurrentTime = PubFun.getCurrentTime();

/**
 * 提交数据
 * @param cInputData VData
 * @param cOperate String
 * @return boolean
 */
public boolean submitData(VData cInputData)
{
    if (!getInputData(cInputData))
    {
        return false;
    }

    if (!dealData())
    {
        return false;
    }
    return true;
}

/**
 * 得到生成的工单号
 * @return String
 */
public String getEdorAcceptNo()
{
    return mEdorAcceptNo;
}

/**
 * 得到输入数据
 * @param cInputData VData
 * @return boolean
 */
private boolean getInputData(VData cInputData)
{
    mTypeFlag = (String) cInputData.get(0);
    mGUFlag = (String) cInputData.get(1);
    mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0);
    mLPEdorItemSchema = (LPEdorItemSchema)
            cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
    mLDPersonSchema = (LDPersonSchema)
            cInputData.getObjectByObjectName("LDPersonSchema", 0);
    
    mCustomerno = mLDPersonSchema.getCustomerNo();
    mLCInsuredSchema = (LCInsuredSchema)
            cInputData.getObjectByObjectName("LCInsuredSchema", 0);
    mLGWorkSchema = (LGWorkSchema)
    cInputData.getObjectByObjectName("LGWorkSchema", 0);
    mContNo= mLGWorkSchema.getContNo();
    mLPContSubSchema = (LPContSubSchema)
    cInputData.getObjectByObjectName("LPContSubSchema", 0);
    
    return true;
}

/**
 * 处理业务数据
 * @return boolean
 */
private boolean dealData()
{
    //添加工单受理
    mEdorAcceptNo = createTask();
    if ((mEdorAcceptNo == null) || (mEdorAcceptNo.equals("")))
    {
        return false;
    }
    mLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
    if (!addEdorItem())
    {
        return false;
    }
    if (!saveDetail())
    {
        return false;
    }
    //保全理算
    if (!appConfirm())
    {
        return false;
    }
    creatPrintVts();
    //保全确认
    if (!edorConfirm())
    {
        return false;
    }
    return true;
}

/**
 * 创建工单，添加工单受理
 * @return boolean
 */
private String createTask()
{
    VData data = new VData();
    data.add(mGlobalInput);
    data.add(mLGWorkSchema);
    TaskInputBL tTaskInputBL = new TaskInputBL();
    if (!tTaskInputBL.submitData(data, ""))
    {
        mErrors.addOneError("工单数据生成失败！");
        return null;
    }
    return tTaskInputBL.getWorkNo();
}

/**
 * 添加保全项目
 * @return boolean
 */
private boolean addEdorItem()
{		
	//正在操作保全的无法添加保全
	String edorSQL = " select edoracceptno from lpedoritem where contno='"
			+ mContNo
			+ "' and edoracceptno!='"+ mEdorAcceptNo+ "' "
			+ " and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";
	ExeSQL tExeSQL = new ExeSQL();
	String edorFlag = tExeSQL.getOneValue(edorSQL);
	if (edorFlag != null && !"".equals(edorFlag)) {
		mErrors.addOneError("保单" + mContNo
				+ "正在操作保全，工单号为：" + edorFlag + "无法做客户基本资料变更");
		return false;
	}
	
	//添加冻结校验
	  String sql = "select 1 from LCContHangUpState where Contno = '" + mContNo + "' "
    + "and State = '1' with ur" ;
	  ExeSQL tExeSQL2 = new ExeSQL();
		String edorFlag2 = tExeSQL2.getOneValue(sql);
		if (edorFlag2 != null && !"".equals(edorFlag2)) {
  			mErrors.addOneError("保单" + mContNo
  					+ "已经被挂起，不能做保全客户资料变更操作");
			return false;
		}

    MMap map = new MMap();
    LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
    tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
    tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
    tLPEdorMainSchema.setEdorAppNo(mEdorAcceptNo);
    tLPEdorMainSchema.setContNo(mContNo);
    tLPEdorMainSchema.setEdorAppDate(mCurrentDate);
    tLPEdorMainSchema.setEdorValiDate(mCurrentDate);
    tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INIT);
    tLPEdorMainSchema.setUWState(BQ.UWFLAG_INIT);
    tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
    tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
    tLPEdorMainSchema.setMakeDate(mCurrentDate);
    tLPEdorMainSchema.setMakeTime(mCurrentTime);
    tLPEdorMainSchema.setModifyDate(mCurrentDate);
    tLPEdorMainSchema.setModifyTime(mCurrentTime);
    map.put(tLPEdorMainSchema, "INSERT");
 
   if (!submit(map))
  {
      return false;
  }

    

    mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
//  mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
    mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
    mLPEdorItemSchema.setDisplayType("1");
    mLPEdorItemSchema.setEdorType(mEdorType);
    mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
    mLPEdorItemSchema.setContNo(mContNo);
    mLPEdorItemSchema.setInsuredNo(mCustomerno);
    mLPEdorItemSchema.setPolNo(BQ.FILLDATA);
    mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
    mLPEdorItemSchema.setEdorValiDate(mCurrentDate);
    mLPEdorItemSchema.setEdorAppDate(mCurrentDate);
    mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
    mLPEdorItemSchema.setMakeDate(mCurrentDate);
    mLPEdorItemSchema.setMakeTime(mCurrentTime);
    mLPEdorItemSchema.setModifyDate(mCurrentDate);
    mLPEdorItemSchema.setModifyTime(mCurrentTime);

    


	PEdorAppItemUI tPEdorAppItemUI=new PEdorAppItemUI();
	LPEdorItemSet mLPEdorItemSet=new LPEdorItemSet();
	mLPEdorItemSet.add(mLPEdorItemSchema);
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("DisplayType","1");
	VData tVData = new VData();
    
    tVData.add(mLPEdorItemSet);
    tVData.add(tTransferData);
    tVData.add(mGlobalInput);
    
	if (!tPEdorAppItemUI.submitData(tVData,"INSERT||EDORITEM"))
    {
		mErrors.addOneError("生成保全工单失败" + tPEdorAppItemUI.mErrors.getFirstError());
		return false;
    }
	return true;
}

/**
 * 保存明保全细
 * @return boolean
 */
private boolean saveDetail()
{
    //
    VData data = new VData();
    data.add(mTypeFlag);//团单为 G
    data.add(mGUFlag); //团险万能 为 Y
    data.add(mGlobalInput);
    data.add(mLPEdorItemSchema);
    data.add(mLDPersonSchema);
    data.add(mLCInsuredSchema);
  //  data.add(tLCGetSchema);
  //  data.add(tLCInsuredListSchema);
    data.add(mLPContSubSchema); 
    PEdorCMDetailBL tPEdorCMDetailBL = new PEdorCMDetailBL();
    if (!tPEdorCMDetailBL.submitData(data))
    {
        mErrors.copyAllErrors(tPEdorCMDetailBL.mErrors);
        return false;
    }
    return true;
}

/**
 * 保全理算，更新核保标志等同于保全理算
 * @return boolean
 */
private boolean appConfirm()
{
    MMap map = new MMap();
    String sql;
    sql = "update LPEdorApp set EdorState = '2', UWState = '9' " +
            "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
    map.put(sql, "UPDATE");
    sql = "update LPEdorMain set EdorState = '2', UWState = '9' " +
            "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
    map.put(sql, "UPDATE");
    sql = "update LPEdorItem set EdorState = '2' " +
            "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
    map.put(sql, "UPDATE");
    if (!submit(map))
    {
        return false;
    }
    return true;
}

/**
 * 产生打印数据
 * @return boolean
 */
private boolean creatPrintVts()
{
    //生成打印数据
    VData data = new VData();
    data.add(mGlobalInput);
    PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
            mEdorAcceptNo);
    if (!tPrtAppEndorsementBL.submitData(data, ""))
    {
        mErrors.addOneError("数据保存成功！但没有生成保全服务批单！");
        return false;
    }
    return true;
}

/**
 * 保全确认
 * @param edorAcceptNo String
 * @return boolean
 */
private boolean edorConfirm()
{
    MMap map = new MMap();
    PEdorConfirmBL tPEdorConfirmBL =
            new PEdorConfirmBL(mGlobalInput, mEdorAcceptNo);
    MMap edorMap = tPEdorConfirmBL.getSubmitData();
    if (edorMap == null)
    {
        mErrors.copyAllErrors(tPEdorConfirmBL.mErrors);
        return false;
    }
    map.add(edorMap);
    //工单结案
    LGWorkSchema tLGWorkSchema = new LGWorkSchema();
    tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
    tLGWorkSchema.setTypeNo("03"); //结案状态

    VData data = new VData();
    data.add(mGlobalInput);
    data.add(tLGWorkSchema);
    TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
    MMap taskMap = tTaskAutoFinishBL.getSubmitData(data, "");
    if (taskMap == null)
    {
        mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
        return false;
    }
    map.add(taskMap);
    if (!submit(map))
    {
        return false;
    }
    return true;
}

/**
 * 提交数据到数据库
 * @return boolean
 */
private boolean submit(MMap map)
{
    VData data = new VData();
    data.add(map);
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(data, ""))
    {
        mErrors.copyAllErrors(tPubSubmit.mErrors);
        return false;
    }
    return true;
}
}
