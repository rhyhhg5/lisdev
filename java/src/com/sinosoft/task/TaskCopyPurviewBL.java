package com.sinosoft.task;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.schema.LGProjectPurviewSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.LGProjectPurviewDB;
import com.sinosoft.lis.vschema.LGProjectPurviewSet;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaskCopyPurviewBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private SSRS tSSRS = new SSRS();
    private ExeSQL tExeSQL = new ExeSQL();
    private LGProjectPurviewSchema mLGProjectPurviewSchema;
    private LGProjectPurviewSet mLGProjectPurviewSet;

    public TaskCopyPurviewBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mInputData = (VData)cInputData.clone();
        this.mOperate = cOperate;

        if(!getInputData())
        {
            return false;
        }

        if (mLGProjectPurviewSchema.getPostNo() == null
            || mLGProjectPurviewSchema.getPostNo().equals(""))
        {
            return true;
        }

        if(!checkLogic() || !dealData() || !prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(this.mInputData, this.mOperate))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            System.out.println(this.mErrors.getContent());

            return false;
        }

        return true;
    }

    //将传入的数据分解到本类
    public boolean getInputData()
    {
        try
        {
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            this.mLGProjectPurviewSchema =
                    (LGProjectPurviewSchema) mInputData.getObjectByObjectName(
                            "LGProjectPurviewSchema", 0);
        }
        catch (Exception e)
        {
            this.mErrors.addOneError(new CError("转入到后台的数据不完整" + e,
                                                "TaskCopyPurviewBL",
                                                "getInputData"));
            System.out.println("转入到后台的数据不完整" + e);

            return false;
        }

        return true;
    }

    /*
     检查操作逻辑的正确性
     备扩展
     */
    private boolean checkLogic()
    {
        return true;
    }

    //先删除该用户原有权限,再复制所选岗位级别的权限
    private boolean dealData()
    {
        //删除该用户原有权
        String sql = "delete "
                     + "from LGProjectPurview "
                     + "where memberNo='"
                     + this.mLGProjectPurviewSchema.getMemberNo() + "' ";
        map.put(sql, "DELETE");

        //复制所选岗位级别的权限
        LGProjectPurviewDB tLGProjectPurviewDB = new LGProjectPurviewDB();
        tLGProjectPurviewDB.setPostNo(mLGProjectPurviewSchema.getPostNo());
        tLGProjectPurviewDB.setMemberNo("00000000000000000000");  //岗位级别的权限
        LGProjectPurviewSet set = tLGProjectPurviewDB.query();

//        long maxNoAddOne = GetMaxNo.getLong("LGProjectPurview",
//                                                "ProjectPurviewNo");
        for (int i = 0; i < set.size(); i++)
        {
            //设置各人权限记录的编号
//            if (maxNoAddOne == -1)
//            {
//                this.mErrors.addOneError(
//                        new CError("获得最大编号时出错,可能是您输入的表明或字段名有误",
//                                   "TaskCoopyPurviewBL", "dealData"));
//                System.out.println("TaskCoopyPurviewBL的dealData方法中获得最大编号时出错,"
//                                   + "可能是您输入的表明或字段名有误");
//
//                return false;
//            }
//            set.get(i + 1).setProjectPurviewNo(Long.toString(maxNoAddOne + i));
            set.get(i + 1).setProjectPurviewNo(PubFun1.CreateMaxNo("TASKPURVIEW", null));
            set.get(i + 1).setMemberNo(mLGProjectPurviewSchema.getMemberNo());
            set.get(i + 1).setOperator(mGlobalInput.Operator);
            set.get(i + 1).setMakeDate(this.mCurrentDate);
            set.get(i + 1).setMakeTime(this.mCurrentTime);
            set.get(i + 1).setModifyDate(this.mCurrentDate);
            set.get(i + 1).setModifyTime(this.mCurrentTime);
        }
        map.put(set, "INSERT");
        mLGProjectPurviewSet = new LGProjectPurviewSet();
        mLGProjectPurviewSet.add(set);

        return true;
    }

    //准备交给后台处理的数据
   public boolean prepareOutputData()
   {
       try
        {
            //给后台的数据
            mInputData.clear();
            mInputData.add(this.map);

            //反馈给jsp页面的信息
            mResult.clear();
            mResult.add(mLGProjectPurviewSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            mErrors.addOneError (BuildError.create("TaskAddPostPurViewBL",
                    "prepareOutputData", "准备输往后台数据时出错" + ex.toString()));
            System.out.println("...prepareOutputData中准备输往后台数据时出错");

            return false;
        }

        return true;
   }

   public VData getResult()
   {
       return this.mResult;
   }

}
