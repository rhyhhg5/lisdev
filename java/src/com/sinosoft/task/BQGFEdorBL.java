package com.sinosoft.task;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * Title: 简易保全逻辑处理类
 * </p>
 * <p>
 * Description: 客户保单联系地址变更
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Yang Yalin, QiuYang
 * @version 1.0
 * @date 2005-04-12
 */

public class BQGFEdorBL {
	/** 错误保存容器 */
	public CErrors mErrors = new CErrors();

	/** 全局变量 */
	private GlobalInput mGlobalInput = null;

	private LPContSchema mLPContSchema = null;

	private String mEdorAcceptNo = null;

	private String mContNo = null;

	private LGWorkSchema mLGWorkSchema = new LGWorkSchema();

	private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

	private MMap map = new MMap();
	private Reflections ref = new Reflections();

	String mCustomerno = "";

	String mWorkNo = "";

	boolean isAppntFlag = false;

	/** 当前日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 当前时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 得到生成的工单号
	 * 
	 * @return String
	 */
	public String getEdorAcceptNo() {
		return mEdorAcceptNo;
	}

	/**
	 * 得到输入数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mLPContSchema = (LPContSchema) cInputData.getObjectByObjectName(
				"LPContSchema", 0);

		mLGWorkSchema = (LGWorkSchema) cInputData.getObjectByObjectName(
				"LGWorkSchema", 0);
		mContNo = mLPContSchema.getContNo();
		// mLPEdorItemSchema = (LPEdorItemSchema) cInputData
		// .getObjectByObjectName("LPEdorItemSchema", 0);

		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		// 生成工单信息
		mEdorAcceptNo = createTask();
		if ((mEdorAcceptNo == null) || (mEdorAcceptNo.equals(""))) {
			return false;
		}
		mLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);

		// 添加保全项目
		if (!addEdorItem()) {
			return false;
		}

		// 保全项目明细
		if (!saveDetail()) {
			return false;
		}
		// 保全理算
		if (!appConfirm()) {
			return false;
		}

		// 生成打印信息
		creatPrintVts();

		// 保全确认
		if (!edorConfirm()) {
			return false;
		}
		return true;
	}

	/**
	 * 创建工单，添加工单受理
	 * 
	 * @return boolean
	 */
	private String createTask() {
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(mLGWorkSchema);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			mErrors.addOneError("工单数据生成失败！");
			return null;
		}
		return tTaskInputBL.getWorkNo();
	}

	/**
	 * 添加保全项目
	 * 
	 * @return boolean
	 */
	private boolean addEdorItem() {
		//正在操作保全的无法添加保全
		String edorSQL = " select edoracceptno from lpedoritem where contno='"
				+ mContNo
				+ "' and edoracceptno!='"+ mEdorAcceptNo+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";
		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			mErrors.addOneError("保单" + mContNo
					+ "正在操作保全，工单号为：" + edorFlag + "无法做保全");
			return false;
		}
		
		MMap map = new MMap();
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorAppNo(mEdorAcceptNo);
        tLPEdorMainSchema.setContNo(mContNo);
        tLPEdorMainSchema.setEdorAppDate(mCurrentDate);
        tLPEdorMainSchema.setEdorValiDate(mCurrentDate);
        tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INIT);
        tLPEdorMainSchema.setUWState(BQ.UWFLAG_INIT);
        tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorMainSchema.setMakeDate(mCurrentDate);
        tLPEdorMainSchema.setMakeTime(mCurrentTime);
        tLPEdorMainSchema.setModifyDate(mCurrentDate);
        tLPEdorMainSchema.setModifyTime(mCurrentTime);
        map.put(tLPEdorMainSchema, "INSERT");

      if (!submit(map))
      {
          return false;
      }


		 LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

		mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
//		mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
		mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
		mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
		mLPEdorItemSchema.setDisplayType("1");
		// 测试，暂时固定
		mLPEdorItemSchema.setEdorType("GF");
		mLPEdorItemSchema.setContNo(mLPContSchema.getContNo());
		mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
		mLPEdorItemSchema.setContNo(mContNo);
		mLPEdorItemSchema.setEdorState("3");
		mLPEdorItemSchema.setInsuredNo(mLGWorkSchema.getCustomerNo());
		mLPEdorItemSchema.setPolNo(BQ.FILLDATA);
		mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
		mLPEdorItemSchema.setEdorValiDate(mCurrentDate);
		mLPEdorItemSchema.setEdorAppDate(mCurrentDate);
	//	mLPEdorItemSchema.setReasonCode(mEdorItemInfo.getReason());
		mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
		mLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
		mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());

		tLPEdorItemSet.add(mLPEdorItemSchema);
		VData tVData = new VData();

		tVData.add(tLPEdorItemSet);
		tVData.add(mGlobalInput);

		PEdorAppItemBL pEdorAppItemBL = new PEdorAppItemBL();
		if (!pEdorAppItemBL.submitData(tVData, "INSERT||EDORITEM")) {
			mErrors.addOneError("生成保全工单失败" + pEdorAppItemBL.mErrors.getFirstError());
			return false;
		}

		return true;
	}

	/**
	 * 保存保全明细
	 * 
	 * @return boolean
	 */
	private boolean saveDetail() {

		// 对需要传入的数据进行简单校验
		if (mLPEdorItemSchema == null || ("").equals(mLPEdorItemSchema)) {
			CError tError = new CError();
			tError.moduleName = "PEdorFCDetailBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询保全项目信息失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 将需要变更的数据传入后台进行处理
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(mLPEdorItemSchema);
		PEdorGFDetailBL tPEdorGFDetailBL = new PEdorGFDetailBL();
		if (tPEdorGFDetailBL.submitData(data, "") == false) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPEdorGFDetailBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorGFDetailUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据查询失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 保全理算，更新核保标志等同于保全理算
	 * 
	 * @return boolean
	 */
	private boolean appConfirm() {

		
		PEdorAppConfirmUI tPEdorAppConfirmUI = new PEdorAppConfirmUI(mGlobalInput, mEdorAcceptNo);
	    if (!tPEdorAppConfirmUI.submitData())
	    {
	    	CError tError = new CError();
			tError.moduleName = "PEdorFCDetailBL";
			tError.functionName = "dealData";
			tError.errorMessage = "保全理算失败！原因是：" + tPEdorAppConfirmUI.getError();
			this.mErrors.addOneError(tError);
	    }
	    else
			{
		    	//生成打印数据
		    	VData tVData = new VData();
				tVData.add(mGlobalInput);
				PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
						mEdorAcceptNo);
				if (!tPrtAppEndorsementBL.submitData(tVData, ""))
				{
					mErrors.addOneError("保全理算成功。但生成理算结果预览失败！");
					return false;
				}
			}
		
		
		/*MMap map = new MMap();
		String sql;
		sql = "update LPEdorApp set EdorState = '2', UWState = '9' "
				+ "where EdorAcceptNo = '"
				+ mLPEdorItemSchema.getEdorAcceptNo() + "' ";
		map.put(sql, "UPDATE");
		sql = "update LPEdorMain set EdorState = '2', UWState = '9' "
				+ "where EdorAcceptNo = '"
				+ mLPEdorItemSchema.getEdorAcceptNo() + "' ";
		map.put(sql, "UPDATE");
		sql = "update LPEdorItem set EdorState = '2' "
				+ "where EdorAcceptNo = '"
				+ mLPEdorItemSchema.getEdorAcceptNo() + "' ";
		map.put(sql, "UPDATE");

		if (!submit(map)) {
			return false;
		}*/
		return true;
	}

	/**
	 * 产生打印数据
	 * 
	 * @return boolean
	 */
	private boolean creatPrintVts() {
		// 生成打印数据
		VData data = new VData();
		data.add(mGlobalInput);
		PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
				mLPEdorItemSchema.getEdorAcceptNo());
		if (!tPrtAppEndorsementBL.submitData(data, "")) {
			mErrors.addOneError("数据保存成功！但没有生成保全服务批单！");
			return false;
		}
		return true;
	}

	/**
	 * 保全确认
	 * 
	 * @param edorAcceptNo
	 *            String
	 * @return boolean
	 */
	private boolean edorConfirm() {
		MMap map = new MMap();
		PEdorConfirmBL tPEdorConfirmBL = new PEdorConfirmBL(mGlobalInput,
				mLPEdorItemSchema.getEdorAcceptNo());
		MMap edorMap = tPEdorConfirmBL.getSubmitData();
		if (edorMap == null) {
			mErrors.copyAllErrors(tPEdorConfirmBL.mErrors);
			return false;
		}
		map.add(edorMap);
		// 工单结案
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		// tLGWorkSchema.setDetailWorkNo(mLGWorkSchema.getWorkNo());
		tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
		tLGWorkSchema.setTypeNo("03"); // 结案状态

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		MMap taskMap = tTaskAutoFinishBL.getSubmitData(data, "");
		if (taskMap == null) {
			mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
			return false;
		}
		map.add(taskMap);
		if (!submit(map)) {
			return false;
		}
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
}
