package com.sinosoft.task;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 校验维护权限时权限数据的合法性
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class CheckPurviewData extends CheckData
{
    /**
     * 错误容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private LGProjectPurviewSet mLGProjectPurviewSet = null;
    private String mOperate = null;

    public CheckPurviewData()
    {
    }

    /**
     * 公共数据提交方法，
     * @param cInputData VData
     * @param cOperate String
     * @return boolean，校验成功：true， 校验失败：false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        //获取传入数据
        if(!getInputData(cInputData))
        {
            return false;
        }

        if(cOperate.equals("INSERT||MAIN"))
        {
            if(!checkInsert())
            {
                return false;
            }
        }
        else if(cOperate.equals("UPDATE||MAIN"))
        {
            if(!checkUpdate())
            {
                return false;
            }
        }else if(mOperate.equals("DELETE||MAIN")){
        	if(!checkDelete()){
        		return false;
        	}
        }

        return true;
    }

    /**
     * getInputData
     * 获取传入的数据
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            //为避免校验数据时修改了原录入数据,特通过传值的方式取得数据
            mGlobalInput = new GlobalInput();
            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);

            LGProjectPurviewSet set =
                    (LGProjectPurviewSet) cInputData.
                    getObjectByObjectName("LGProjectPurviewSet", 0);
            mLGProjectPurviewSet = new LGProjectPurviewSet();
            for(int i = 1; i <= set.size(); i++)
            {
                LGProjectPurviewSchema schema = new LGProjectPurviewSchema();
                schema.setSchema(set.get(i));
                mLGProjectPurviewSet.add(schema);
            }
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 校验新建权限时的操作合法性
     * @return boolean
     */
    private boolean checkInsert()
    {
        for (int i = 1; i <= mLGProjectPurviewSet.size(); i++)
        {
            LGProjectPurviewSchema tLGProjectPurviewSchema =
                    mLGProjectPurviewSet.get(i);

            if(this.mOperate.equals("INSERT||MAIN")
               && !checkDBData(tLGProjectPurviewSchema))
            {
                return false;
            }
            
            //总公司保单管理部经理全权
            if(tLGProjectPurviewSchema.getPostNo().equals("PA01"))
            {
                continue;
            }

            //校验岗位级别的权限合法性
            if (tLGProjectPurviewSchema.getMemberNo() == null
                || tLGProjectPurviewSchema.getMemberNo().equals("")
                || tLGProjectPurviewSchema.getMemberNo().
                equals("00000000000000000000"))
            {
                if(!checkPostPurview(tLGProjectPurviewSchema))
                {
                    return false;
                }
            }
            //校验成员权限的合法性
            else
            {
                if(!checkMemberPurview(tLGProjectPurviewSchema))
                {
                    return false;
                }
            }
        }

        return true;
    }

    //校验库中是否已有相同的数据记录
   private boolean checkDBData(LGProjectPurviewSchema schema)
   {
       LGProjectPurviewDB db = new LGProjectPurviewDB();
//       db.setProjectNo(schema.getProjectNo());
//       db.setProjectType(schema.getProjectType());
//       db.setPostNo(schema.getPostNo());
//       if(schema.getRiskCode()!=null&&!"".equals(schema.getRiskCode())){
//    	   db.setRiskCode(schema.getRiskCode()); 
//       }
//       db.setMemberNo(schema.getMemberNo());
//       if(db.getMemberNo() == null || db.getMemberNo().equals(""))
//       {
//           db.setMemberNo("00000000000000000000");
//       }
       String sql = " select * from LGProjectPurview where ProjectNo = '"+schema.getProjectNo()+"' " +
       		" and ProjectType = '"+schema.getProjectType()+"' and PostNo = '"+schema.getPostNo()+"' " +
       		" and (stateflag!='2' or stateflag is null) ";
	     if(schema.getRiskCode()!=null&&!"".equals(schema.getRiskCode())){
	    	 		sql += " and RiskCode = '"+schema.getRiskCode()+"' ";
	   }else{
		   sql +=" and (riskcode is null or riskcode='') ";
	   }
	     
       if(db.getMemberNo() == null || db.getMemberNo().equals(""))
       {
           sql += " and MemberNo='00000000000000000000' ";
       }
	     

       LGProjectPurviewSet set = db.executeQuery(sql);

       //不能向库中插入相同的记录
       if (set.size() > 0)
       {
           mErrors.addOneError("已有相应的权限记录,不可重复录入");
           return false;
       }

       return true;
   }

   private boolean checkDeleteData(LGProjectPurviewSchema schema){
	   
       LGProjectPurviewDB db = new LGProjectPurviewDB();
       String sql = " select * from LGProjectPurview where ProjectNo = '"+schema.getProjectNo()+"' " +
  		" and ProjectType = '"+schema.getProjectType()+"'" +
  		" and PostNo = '"+getLowerPostNoOfPostNo(schema.getPostNo()) +"'" +
  		" and MemberNo='00000000000000000000'" +
  		"  and (stateflag!='2' or stateflag is null) ";
    if(schema.getRiskCode()!=null&&!"".equals(schema.getRiskCode())){
   	 		sql += " and RiskCode = '"+schema.getRiskCode()+"' ";
	  }else{
		   sql +=" and (riskcode is null or riskcode='') ";
	  }
	
	  LGProjectPurviewSet set = db.executeQuery(sql);
      if (set.size() > 0)
      {
          mErrors.addOneError("查询到"
                              + schema.getPostNo()
                              + "的有下级岗位，请先删除下级岗位的权限!");
          return false;
      }

	  
	   return true;
   }

    /**
     * 校验新建权限时岗位级别权限操作合法性
     * @return boolean
     */
    private boolean checkPostPurview(LGProjectPurviewSchema
                                     purviewSchema)
    {
        //查询上级岗位的权限
        LGProjectPurviewDB tLGProjectPurviewDB = new LGProjectPurviewDB();
//        tLGProjectPurviewDB.setProjectNo(purviewSchema.getProjectNo());
//        tLGProjectPurviewDB.setProjectType(purviewSchema.getProjectType());
//        tLGProjectPurviewDB.setPostNo(getSurperPostNoOfPostNo(
//                purviewSchema.getPostNo()));
//        tLGProjectPurviewDB.setMemberNo("00000000000000000000");
//        if(purviewSchema.getRiskCode()!=null&&!"".equals(purviewSchema.getRiskCode())){
//        	tLGProjectPurviewDB.setRiskCode(purviewSchema.getRiskCode());
//        }
//        LGProjectPurviewSet set = tLGProjectPurviewDB.query();
        String sql = " select * from LGProjectPurview where ProjectNo = '"+purviewSchema.getProjectNo()+"' " +
   		" and ProjectType = '"+purviewSchema.getProjectType()+"'" +
   		" and PostNo = '"+getSurperPostNoOfPostNo(purviewSchema.getPostNo()) +"'" +
   		" and MemberNo='00000000000000000000'" +
   		"  and (stateflag!='2' or stateflag is null) ";
     if(purviewSchema.getRiskCode()!=null&&!"".equals(purviewSchema.getRiskCode())){
    	 		sql += " and RiskCode = '"+purviewSchema.getRiskCode()+"' ";
   }else{
	   sql +=" and (riskcode is null or riskcode='') ";
   }

   LGProjectPurviewSet set = tLGProjectPurviewDB.executeQuery(sql);
        if (set == null || set.size() == 0)
        {
            mErrors.addOneError("没有查询到"
                                + purviewSchema.getPostNo()
                                + "的上级岗位，请先新建上级的权限");
            return false;
        }

        if (set.get(1).getConfirmFlag().equals("2")
            && !purviewSchema.getConfirmFlag().equals("2")
            || !set.get(1).getConfirmFlag().equals("2")
            && purviewSchema.getConfirmFlag().equals("2"))
        {
            mErrors.addOneError("权限类别与上级不同,请确认.");
            return false;
        }

        //确认权
        if (set.get(1).getConfirmFlag().
            compareTo(purviewSchema.getConfirmFlag()) < 0)
        {
            String errMsg =
                    "岗位级别为"
                    + GetMemberInfo.getPostName(purviewSchema.getPostNo())
                    + "的确认权不能高于上级岗位的权限";
            mErrors.addOneError(errMsg);
            return false;
        }

        //金额权
        if (!compareMoneyPurview(purviewSchema, set.get(1)))
        {
            return false;
        }

        return true;
    }

    /**
     * 得到岗位级别的上一岗位级别
     * @param postNo String
     * @return String
     */
    private String getSurperPostNoOfPostNo(String postNo)
    {
        String sn = postNo.substring(2);
        return "PA0" + (Integer.parseInt(sn) - 1);
    }
    
    /**
     * 得到岗位级别的下一岗位级别
     * @param postNo String
     * @return String
     */
    private String getLowerPostNoOfPostNo(String postNo)
    {
        String sn = postNo.substring(2);
        return "PA0" + (Integer.parseInt(sn) + 1);
    }

    /**
     * 修改后的权限和其上级相比较,不能高于上级权限
     * @param purview LGProjectPurviewSchema
     * @param superPurview LGProjectPurviewSchema
     * @return boolean 符合规则: true, 否则: false
     */
    private boolean compareMoneyPurview(LGProjectPurviewSchema purview,
                                        LGProjectPurviewSchema superPurview)
    {
        long time = 0;
        long superTime = 0;
        try
        {
            time = Long.parseLong(purview.getTimePurview());
            superTime = Long.parseLong(superPurview.getTimePurview());
        }
        catch (Exception e)
        {
            mErrors.addOneError("权限记录为" + superPurview.getProjectNo() + ", "
                                + ((superPurview.getProjectType().equals("0")) ?
                                   "个险" : "团险") + ", "
                                + "权限数据有误,请确认");
            e.printStackTrace();
            return false;
        }

        boolean flag = true; //出错标志: true: 未出错
        String errMsg = ""; //错误信息
        //业务上级的名字
        String superName = GetMemberInfo.getMemberName(superPurview.getMemberNo());
        if(superName.equals("-1"))
        {
            superName = "";
        }

        if (purview.getMoneyPurview() != 0
            && purview.getMoneyPurview() > superPurview.getMoneyPurview())
        {
            flag = false;
            errMsg += "金额权应低于上级" + superName + "的金额权"
                    + superPurview.getMoneyPurview() + " ";
        }
        if (purview.getExcessPurview() != 0
            && purview.getExcessPurview() > superPurview.getExcessPurview())
        {
            flag = false;
            errMsg += "超额权应低于上级" + superName + "的超额权"
                    + superPurview.getExcessPurview() + " ";
        }
        if(time > superTime)
        {
            flag = false;
            errMsg += "时间权限应低于上级" + superName + "的时间权限"
                    + superPurview.getTimePurview() + " ";
        }

        if(!flag)
        {
            mErrors.addOneError(errMsg);
        }

        return flag;
    }

    private boolean checkMemberPurview(LGProjectPurviewSchema purviewSchema)
    {
        if (!comparePostAndMemberPurview(purviewSchema))
        {
            return false;
        }

        //查询业务上级的权限
        LGProjectPurviewSchema superPurviewSchema =
                querySuperMemberPurview(purviewSchema);
        if (superPurviewSchema == null)
        {
            return false;
        }

        if (superPurviewSchema.getConfirmFlag().equals("2")
            && !purviewSchema.getConfirmFlag().equals("2")
            || !superPurviewSchema.getConfirmFlag().equals("2")
            && purviewSchema.getConfirmFlag().equals("2"))
        {
            mErrors.addOneError("权限类别与上级不同,请确认.");
            return false;
        }

        if(superPurviewSchema.getConfirmFlag().
           compareTo(purviewSchema.getConfirmFlag()) < 0)
        {
            String errMsg = "确认权不能高于业务上级"
                                + GetMemberInfo.getMemberName(
                                        superPurviewSchema.getMemberNo())
                                + "的确认权";
            mErrors.addOneError(errMsg);
            return false;
        }

        //比较修改后的权限及其业务上级的金额权
        if(!compareMoneyPurview(purviewSchema, superPurviewSchema))
        {
            return false;
        }

        return true;
    }

    //查询成员业务上级的权限
    private LGProjectPurviewSchema querySuperMemberPurview(
            LGProjectPurviewSchema purviewSchema)
    {
        LGProjectPurviewDB superDB = new LGProjectPurviewDB();
//        superDB.setProjectNo(purviewSchema.getProjectNo());
//        superDB.setProjectType(purviewSchema.getProjectType());
//        superDB.setMemberNo(GetMemberInfo.getSuperNo(purviewSchema.getMemberNo()))
//        superDB.setStateFlag(aStateFlag);
        String supperSQL =  " select * from LGProjectPurview where ProjectNo = '"+purviewSchema.getProjectNo()+"' " +
   		" and ProjectType = '"+purviewSchema.getProjectType()+"'" +
//   		" and PostNo = '"+getSurperPostNoOfPostNo(purviewSchema.getPostNo()) +"'" +
   		" and MemberNo='"+GetMemberInfo.getSuperNo(purviewSchema.getMemberNo())+"'" +
   		"  and (stateflag!='2' or stateflag is null) ";
        if(purviewSchema.getRiskCode()!=null&&!"".equals(purviewSchema.getRiskCode())){
        	supperSQL += " and RiskCode = '"+purviewSchema.getRiskCode()+"' ";
		}else{
			supperSQL +=" and (riskcode is null or riskcode='') ";
		}        

        LGProjectPurviewSet set = superDB.executeQuery(supperSQL);
        if (set == null || set.size() == 0)
        {
            String superManagerName = GetMemberInfo.getMemberName(
                    superDB.getMemberNo());
            mErrors.addOneError("业务上级" + superManagerName
                                + "没有该类别的权限，或没有业务上级。");
            return null;
        }

        return set.get(1);
    }

    /*
         PA01级别的成员的权限类型须和岗位级别的相符
         return: true, 相符; false, 不相符
     */
    private boolean comparePostAndMemberPurview(LGProjectPurviewSchema schema)
    {
        //查询相应的岗位级别的权限
        LGProjectPurviewDB postSchemaDB = new LGProjectPurviewDB();
//        postSchemaDB.setProjectNo(schema.getProjectNo());
//        postSchemaDB.setProjectType(schema.getProjectType());
//        postSchemaDB.setPostNo(schema.getPostNo());

        String sql = " select * from LGProjectPurview where ProjectNo = '"+schema.getProjectNo()+"' " +
   		" and ProjectType = '"+schema.getProjectType()+"' and PostNo = '"+schema.getPostNo()+"' " +
   		" and (stateflag!='2' or stateflag is null) ";
        if(schema.getRiskCode()!=null&&!"".equals(schema.getRiskCode())){
	 		sql += " and RiskCode = '"+schema.getRiskCode()+"' ";
		}else{
		   sql +=" and (riskcode is null or riskcode='') ";
		}
        
        LGProjectPurviewSet set = postSchemaDB.executeQuery(sql);

        if (set == null || set.size() == 0)
        {
            mErrors.addOneError(new CError(
                    "您所录入的权限条目没有相应的级别岗位上的权限，请先新建岗位级别上的权限。",
                    "TaskPostPurviewBL", "comparePostMember"));
            return false;
        }

        //同是金额权或确认权
        if (set.get(1).getConfirmFlag().equals("2")
            && schema.getConfirmFlag().equals("2")
            || set.get(1).getConfirmFlag().compareTo("2") < 0
            && schema.getConfirmFlag().compareTo("2") < 0)
        {
            return true;
        }

        String type = "";
        if (set.get(1).getConfirmFlag().equals("2"))
        {
            type = "金额权";
        }
        else
        {
            type = "确认权";
        }
        mErrors.addOneError(new CError("您所选择的权限类型与岗位级别的权限'"
                                       + type + "'不相符"));
        return false;
    }

    /**
     * 校验修改权限时的操作合法性
     * @return boolean
     */
    private boolean checkUpdate()
    {
        if(!checkInsert())
        {
            return false;
        }

        return true;
    }
    
    private boolean checkDelete(){
        if(this.mOperate.equals("DELETE||MAIN")){
            for (int i = 1; i <= mLGProjectPurviewSet.size(); i++)
            {
                LGProjectPurviewSchema tLGProjectPurviewSchema =
                        mLGProjectPurviewSet.get(i);
        	//查询本级信息
        	LGProjectPurviewDB tdb = new LGProjectPurviewDB();
        	tdb.setProjectPurviewNo(tLGProjectPurviewSchema.getProjectPurviewNo());
        	if(!tdb.getInfo()){
        		 mErrors.addOneError("需要删除的权限不存在");
        		return false;
        	}
        	tLGProjectPurviewSchema = tdb.getSchema();
        	//查询下级权限是否存在，若存在则不能删除本级权限
        	if("PA06".equals(tLGProjectPurviewSchema)){
        		continue;
        	}
        	if(!checkDeleteData(tLGProjectPurviewSchema)){
        		return false;
        	}
            }
        }

        return true;
    }

    public static void main(String[] args)
    {
        CheckPurviewData checkpurviewdata = new CheckPurviewData();
    }
}












