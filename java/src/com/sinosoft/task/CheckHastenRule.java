package com.sinosoft.task;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LGAutoDeliverRuleSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LGAutoDeliverRuleSet;
import com.sinosoft.lis.db.LGAutoDeliverRuleDB;
import com.sinosoft.lis.db.LGWorkBoxDB;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class CheckHastenRule extends CheckRule
{
    /**
     * 错误的容器
     */
    private LGAutoDeliverRuleSchema mLGAutoDeliverRuleSchema = null;


    public CheckHastenRule()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if(!check(cOperate))
        {
            return false;
        }

        return true;
    }

    /**
     * descirption 将传入的数据分配给本类的变量
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mLGAutoDeliverRuleSchema = (
                    (LGAutoDeliverRuleSchema) cInputData.
                    getObjectByObjectName("LGAutoDeliverRuleSchema", 0));
            if (mLGAutoDeliverRuleSchema == null)
            {
                throw new Exception();
            }
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整");
           System.out.println("传入到CheckScanRule的数据不完整 " + e.toString());
           return false;

        }

        return true;
    }

    private boolean check(String cOperate)
    {
        //校验新建规则时的数据合法性
        if (cOperate.equals("INSERT||MAIN"))
        {
            if (!checkInsert())
            {
                return false;
            }
        }
        //校验修改规则时的数据合法性
        else if (cOperate.equals("UPDATE||MAIN"))
        {
            if(!checkUpdate())
            {
                return false;
            }
        }


        return true;
    }

    /**
     * 新建权限是的操作校验
     * @return boolean
     */
    private boolean checkInsert()
    {
        if(!checkIncomeData())
        {
            return false;
        }

        //校验库中是否存在相同的数据
        if(dataExistInDB())
        {
            return false;
        }

        //校验信箱是否存在
        if(!checkWorkBox(mLGAutoDeliverRuleSchema.getTarget()))
        {
            return false;
        }

        return true;
    }

    /**
     * 校验页面传入数据的完整性
     * @return boolean
     */
    private boolean checkIncomeData()
    {
        if (mLGAutoDeliverRuleSchema.getSourceComCode() == null
            || mLGAutoDeliverRuleSchema.getSourceComCode().equals("")
            || mLGAutoDeliverRuleSchema.getGoalType() == null
            || mLGAutoDeliverRuleSchema.getGoalType().equals("")
            || mLGAutoDeliverRuleSchema.getTarget() == null
            || mLGAutoDeliverRuleSchema.getTarget().equals(""))
        {
            mErrors.addOneError("机构、规则类型、目标均不能为空");
            System.out.println("CheckHastenRule->机构、规则类型、目标均不能为空");
            return false;
        }

        return true;
    }

    /**
     * 校验库中是否存在相同的数据
     * @return boolean
     */
    private boolean dataExistInDB()
    {
        LGAutoDeliverRuleDB db = new LGAutoDeliverRuleDB();
        db.setSourceComCode(mLGAutoDeliverRuleSchema.getSourceComCode());
        db.setGoalType(mLGAutoDeliverRuleSchema.getGoalType());
        db.setTarget(mLGAutoDeliverRuleSchema.getTarget());
        LGAutoDeliverRuleSet set = db.query();
        if (set.size() > 0)
        {
            mErrors.addOneError("已经存在相同的规则。");
            System.out.println("CheckHastenRule->已经存在相同的规则。");
            return true;
        }

        return false;
    }

    /**
     * 校验信箱是否存在
     * @return boolean
     */
    private boolean checkWorkBox(String workBoxNo)
    {
        LGWorkBoxDB boxDB = new LGWorkBoxDB();
        boxDB.setWorkBoxNo(workBoxNo);
        if(!boxDB.getInfo())
        {
            mErrors.addOneError("目标信箱不存在，请确认");
            System.out.println("CheckHastenRule->目标信箱不存在，请确认");
            return false;
        }

        return true;
    }

    /**
     * 修改操作时的操作校验
     * @return boolean
     */
    private boolean checkUpdate()
    {
        if(!checkIncomeData())
        {
            return false;
        }

        if(mLGAutoDeliverRuleSchema.getRuleNo() == null)
        {
            mErrors.addOneError("没有取到项目编号。");
            System.out.println("CheckHastenRule->checkUpdate->没有取到项目编号。");
            return false;
        }

        //若规则目标被修改，则校验修改后的规则是否已存在
        LGAutoDeliverRuleSchema tmLGAutoDeliverRuleSchema = getRule();
        if(tmLGAutoDeliverRuleSchema == null)
        {
            return false;
        }
        if (!tmLGAutoDeliverRuleSchema.getTarget()
            .equals(mLGAutoDeliverRuleSchema.getTarget()))
        {
            if(dataExistInDB())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 从库中得到当前修改的规则
     * @return LGAutoDeliverRuleSchema
     */
    private LGAutoDeliverRuleSchema getRule()
    {
        LGAutoDeliverRuleDB db = new LGAutoDeliverRuleDB();
        db.setRuleNo(mLGAutoDeliverRuleSchema.getRuleNo());

        if(!db.getInfo())
        {
            mErrors.addOneError("没有查询到本规则的信息。");
            return null;
        }

        return db.getSchema();
    }

    public static void main(String[] args)
    {
        CheckHastenRule checkhastenrule = new CheckHastenRule();

        LGAutoDeliverRuleSchema s = new LGAutoDeliverRuleSchema();
        s.setRuleContent("asdf");
        s.setSourceComCode("86");
        s.setGoalType("2");
        s.setTarget("2");

        VData v = new VData();
        v.add(s);

        if(!checkhastenrule.submitData(v, "s"))
        {
            System.out.println(checkhastenrule.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }

    }
}
