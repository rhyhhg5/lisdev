package com.sinosoft.task;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class TaskInputNextFeeG implements TaskInput
{
    //需催缴的续期收费记录
    private TransferData nextFeeInfoTF = null;
    private GlobalInput mGlobalInput = null;
    private MMap map = new MMap();

    public TaskInputNextFeeG()
    {
    }

    /**
     * submitData
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     * @todo Implement this com.sinosoft.task.TaskInput method
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getSubmitMap(cInputData, cOperate);

        VData v = new VData();
        v.add(map);

        PubSubmit p = new PubSubmit();
        if(p.submitData(v, ""))
        {
            mErrors.copyAllErrors(p.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 业务处理类
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 接受传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        nextFeeInfoTF = (TransferData) cInputData
                        .getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData
                       .getObjectByObjectName("GlobalInput" , 0);
        if (nextFeeInfoTF == null || mGlobalInput == null)
        {
            mErrors.addOneError("传入的数据有误，不能生成团单续期收费催缴");
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑，生成所需的数据
     * @return boolean
     */
    private boolean dealData()
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(
                (String) nextFeeInfoTF.getValueByName("grpContNo"));
        tLCGrpContDB.getInfo();

        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        String tWorkNo = tDate + PubFun1.CreateMaxNo("TASK" + tDate, 6);

        createTaskInfo(tWorkNo);

        if(!createTraceInfo(tWorkNo, tLCGrpContDB.getSchema()))
        {
            return false;
        }
        createOpInfo(tWorkNo);
        createHastenInfo(tWorkNo);

        return true;
    }

    /**
     * 生成LGWork中的数据
     */
    private void createTaskInfo(String tWorkNo)
    {
        LDGrpDB tLDGrpDG = new LDGrpDB();
        tLDGrpDG.setCustomerNo((String) nextFeeInfoTF
                               .getValueByName("customerNo"));
        tLDGrpDG.getInfo();

        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setWorkNo(tWorkNo);
        tLGWorkSchema.setAcceptNo(tWorkNo);
        tLGWorkSchema.setNodeNo("0");
        tLGWorkSchema.setTypeNo(Task.WORK_TYPE_NEXTFEEG);
        tLGWorkSchema.setStatusNo(Task.WORK_STATE_INIT);
        tLGWorkSchema.setDetailWorkNo(tWorkNo);
        tLGWorkSchema.setCustomerNo(tLDGrpDG.getCustomerNo());
        tLGWorkSchema.setCustomerName(tLDGrpDG.getGrpName());
        tLGWorkSchema.setAcceptCom(mGlobalInput.ComCode);
        tLGWorkSchema.setAcceptDate(PubFun.getCurrentDate());
        tLGWorkSchema.setRemark("自动批注：生成新的工单");
        tLGWorkSchema.setOperator(mGlobalInput.Operator);
        tLGWorkSchema.setMakeDate(PubFun.getCurrentDate());
        tLGWorkSchema.setMakeTime(PubFun.getCurrentTime());
        tLGWorkSchema.setModifyDate(PubFun.getCurrentDate());
        tLGWorkSchema.setModifyTime(PubFun.getCurrentTime());
        tLGWorkSchema.setUniting("0");
        tLGWorkSchema.setCurrDoing("0");
        tLGWorkSchema.setScanFlag("2");

        map.put(tLGWorkSchema, "INSERT");
    }

    /**
     * 生成历史轨迹
     * @param tWorkNo String
     */
    private boolean createTraceInfo(String tWorkNo,
                                 LCGrpContSchema tLCGrpContSchema)
    {
        String workBoxNo = getWorkBoxNo(tLCGrpContSchema);
        if(workBoxNo == null)
        {
            return false;
        }

        LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();

        tLGWorkTraceSchema.setWorkNo(tWorkNo);
        tLGWorkTraceSchema.setNodeNo("0");
        tLGWorkTraceSchema.setWorkBoxNo(workBoxNo); //信箱编号
        tLGWorkTraceSchema.setInMethodNo(Task.HISTORY_TYPE_INIT); //新单录入
        tLGWorkTraceSchema.setInDate(PubFun.getCurrentDate());
        tLGWorkTraceSchema.setInTime(PubFun.getCurrentTime());
        tLGWorkTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLGWorkTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLGWorkTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLGWorkTraceSchema.setModifyTime(PubFun.getCurrentTime());
        tLGWorkTraceSchema.setOperator(mGlobalInput.Operator);

        map.put(tLGWorkTraceSchema, "INSERT");

        return true;
    }

    /**
     * 生成历史轨迹操作信息
     * @param tWorkNo String
     */
    private void createOpInfo(String tWorkNo)
    {
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        tLGTraceNodeOpSchema.setWorkNo(tWorkNo);
        tLGTraceNodeOpSchema.setNodeNo("0");
        tLGTraceNodeOpSchema.setOperatorNo("0");
        tLGTraceNodeOpSchema.setOperatorType(Task.HISTORY_TYPE_INIT);
        tLGTraceNodeOpSchema.setRemark("自动批注：由日处理程序生成催缴任务。");
        tLGTraceNodeOpSchema.setFinishDate(PubFun.getCurrentDate());
        tLGTraceNodeOpSchema.setFinishTime(PubFun.getCurrentTime());
        tLGTraceNodeOpSchema.setOperator(mGlobalInput.Operator);
        tLGTraceNodeOpSchema.setMakeDate(PubFun.getCurrentDate());
        tLGTraceNodeOpSchema.setMakeTime(PubFun.getCurrentTime());
        tLGTraceNodeOpSchema.setModifyDate(PubFun.getCurrentDate());
        tLGTraceNodeOpSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLGTraceNodeOpSchema, "INSERT");
    }

    /**
     * 生成催缴记录
     * @param workNo String
     */
    private void createHastenInfo(String workNo)
    {
        String getNoticeNo =
            (String) nextFeeInfoTF.getValueByName("getNoticeNo");

        LJSPaySchema tLJSPaySchema = getLJSPayInfo(getNoticeNo);

        LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
        tLGPhoneHastenSchema.setWorkNo(workNo);
        tLGPhoneHastenSchema.setTimesNo("0");
        //对于续期催缴，此处存保单号
        tLGPhoneHastenSchema.setEdorAcceptNo(tLJSPaySchema.getOtherNo());
        tLGPhoneHastenSchema.setCustomerNo(tLJSPaySchema.getAppntNo());
        tLGPhoneHastenSchema.setPayReason("续期催缴");
        tLGPhoneHastenSchema.setOldPayDate(tLJSPaySchema.getPayDate());

        //现金
        if(tLJSPaySchema.getBankAccNo() == null
            || tLJSPaySchema.getBankAccNo().equals(""))
         {
             tLGPhoneHastenSchema.setOldPayMode("1");
         }
         //银行转帐
         else
         {
             tLGPhoneHastenSchema.setOldPayMode("4");
             tLGPhoneHastenSchema.setOldBankCode(tLJSPaySchema.getBankCode());
             tLGPhoneHastenSchema.setOldBackAccNo(tLJSPaySchema.getBankAccNo());
         }

        tLGPhoneHastenSchema.setRemark("自动批注：续期催缴。");
        tLGPhoneHastenSchema.setOperator(mGlobalInput.Operator);
        tLGPhoneHastenSchema.setModifyDate(PubFun.getCurrentDate());
        tLGPhoneHastenSchema.setModifyTime(PubFun.getCurrentTime());
        tLGPhoneHastenSchema.setMakeDate(PubFun.getCurrentDate());
        tLGPhoneHastenSchema.setMakeTime(PubFun.getCurrentTime());
        tLGPhoneHastenSchema.setHastenType(Task.HASTENTYPE_NEXTFEEG);
        tLGPhoneHastenSchema.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());

        map.put(tLGPhoneHastenSchema, "INSERT");
    }

    /**
     * 查询生成本次催收的缴费信息
     * @param getNoticeNo String
     * @return LJSPaySchema
     */
    private LJSPaySchema getLJSPayInfo(String getNoticeNo)
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(getNoticeNo);
        if(!tLJSPayDB.getInfo())
        {
            mErrors.addOneError("没有收费记录号为" + getNoticeNo + "的收费记录。");
            return null;
        }

        return tLJSPayDB.getSchema();
    }


    /**
     * 查询当前用户的信箱号
     * @return String
     */
    private String getWorkBoxNo(LCGrpContSchema tLCGrpContSchema)
    {
        String workBoxNo = CommonBL.getAutodeliverTarget(tLCGrpContSchema.getManageCom(),
            CheckData.NEXTFEE_HASTEN_RULE, null);
        if( workBoxNo == null)
        {
            String errMsg = "保单" + tLCGrpContSchema.getGrpContNo()
                            + "生成催收任务失败：没有查询到机构"
                            + tLCGrpContSchema.getManageCom()
                            + "的续期催缴信箱。";
            mErrors.addOneError(errMsg);
            return null;
        }

        return workBoxNo;
    }

    public static void main(String[] args)
    {
    }
}
