package com.sinosoft.task;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LGAutoDeliverRuleSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单转交BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-20
 * */

public class TaskAutoDeliverRuleBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /**提交数据的容器*/
    private MMap map = new MMap();

    /**公共输入信息*/
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    //统一更新日期
   private String mCurrentDate = PubFun.getCurrentDate();

   // 统一更新时间
   private String mCurrentTime = PubFun.getCurrentTime();


    /**传入的规则*/
    private LGAutoDeliverRuleSchema mLGAutoDeliverRuleSchema =
            new LGAutoDeliverRuleSchema();

    public TaskAutoDeliverRuleBL()
    {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        //校验规则的合法性
        String goalType = ((LGAutoDeliverRuleSchema) cInputData.
                           getObjectByObjectName("LGAutoDeliverRuleSchema", 0)).
                          getGoalType();
        CheckData tCheckData = new CheckData(goalType);
        if(!tCheckData.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(tCheckData.mErrors);
            return false;
        }

        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            buildError("submitData", "提交数据时出错");
            return false;
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mLGAutoDeliverRuleSchema =
                    (LGAutoDeliverRuleSchema) cInputData.
                    getObjectByObjectName("LGAutoDeliverRuleSchema", 0);
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
        }
        catch(Exception e)
        {
            buildError("getInputData", "传入的数据不完整" + e);
            System.out.println("Catch Exception in getInputData");
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        try
        {
            //获得最大节点号
            String ruleNo = "0";
            String getMaxNo = "select (max(to_number(ruleNo))) " +
                              "from LGAutoDeliverRule";
            tSSRS = tExeSQL.execSQL(getMaxNo);

            if (tSSRS != null)
            {
                ruleNo = tSSRS.GetText(1, 1);
            }

            mLGAutoDeliverRuleSchema.setModifyDate(mCurrentDate);
            mLGAutoDeliverRuleSchema.setModifyTime(mCurrentTime);

            if (mOperate.equals("INSERT||MAIN"))
            {
                //设置schema信息, 新建的规则默认使用
                //若是电话催缴，则
                if (mLGAutoDeliverRuleSchema.getRuleContent() != null
                    && mLGAutoDeliverRuleSchema.getRuleContent().equals(""))
                {
                    mLGAutoDeliverRuleSchema.setRuleContent("　");
                }
                int temp = Integer.parseInt(ruleNo) + 1;
                mLGAutoDeliverRuleSchema.setRuleNo(Integer.toString(temp));
                mLGAutoDeliverRuleSchema.setDefaultFlag("1");
                mLGAutoDeliverRuleSchema.setMakeDate(mCurrentDate);
                mLGAutoDeliverRuleSchema.setOperator(mGlobalInput.Operator);
                mLGAutoDeliverRuleSchema.setMakeTime(mCurrentTime);
                map.put(mLGAutoDeliverRuleSchema, "INSERT");

                //将本机构的本规则内容对应的其他退则的默认标志置为不使用: 0

                String clrDefault = "";
                //扫描
                if(mLGAutoDeliverRuleSchema.getGoalType()
                   .equals(CheckData.SCAN_RULE))
                {
                    clrDefault =
                            "update LGAutoDeliverRule "
                            + "set defaultFlag = '0' "
                            + "where RuleContent = '"
                            + mLGAutoDeliverRuleSchema.getRuleContent() + "' "
                            + "    and sourceComCode = '"
                            + mLGAutoDeliverRuleSchema.getSourceComCode() + "' "
                            + "    and goalType = '"
                            + mLGAutoDeliverRuleSchema.getGoalType() + "' "
                            + "    and defaultFlag = '1' "
                            + "    and ruleNo != '" + Integer.toString(temp) + "' ";
                }
                //电话催缴
                else if (mLGAutoDeliverRuleSchema.getGoalType()
                         .equals(CheckData.HASTEN_RULE)
                         || mLGAutoDeliverRuleSchema.getGoalType()
                         .equals(CheckData.NEXTFEE_HASTEN_RULE)
                         || mLGAutoDeliverRuleSchema.getGoalType()
                         .equals(CheckData.MJ_RULE))
                {
                    clrDefault =
                            "update LGAutoDeliverRule "
                            + "set defaultFlag = '0' "
                            + "where sourceComCode = '"
                            + mLGAutoDeliverRuleSchema.getSourceComCode() + "' "
                            + "    and goalType = '"
                            + mLGAutoDeliverRuleSchema.getGoalType() + "' "
                            + "    and defaultFlag = '1' "
                            + "    and ruleNo != '" + Integer.toString(temp) + "' ";
                }
                if(!clrDefault.equals(""))
                {
                    map.put(clrDefault, "UPDATE");
                }
            }
            else if(mOperate.equals("UPDATE||MAIN"))
            {
                String content = mLGAutoDeliverRuleSchema.getRuleContent();
                String target = mLGAutoDeliverRuleSchema.getTarget();
                String sql =
                        "update LGAutoDeliverRule " +
                         "set target='" + target + "', " +
                         "RuleDescription='" + mLGAutoDeliverRuleSchema.getRuleDescription() + "', " +
                         "operator='" + mGlobalInput.Operator + "', " +
                         "ModifyDate='" + mCurrentDate + "', " +
                         "ModifyTime='" + mCurrentTime + "' " +
                         "where ruleNo='" + mLGAutoDeliverRuleSchema.getRuleNo() + "' ";
                 map.put(sql, "UPDATE");
                 System.out.println("UPDATE||MAIN in BL");
            }
            else if(mOperate.equals("SETDEFAULT||MAIN"))
            {
                setDefaultBox();
            }
            else if(mOperate.equals("DELETE||MAIN"))
            {
                deleteRule();
            }
        }
        catch (Exception e)
        {
            buildError("submitData", "处理数据出错" + e);
            System.out.println("Catch Exception in submitData");
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch(Exception e)
        {
            buildError("prepareOutputData", "准备输往后台数据时出错");
            System.out.println("prepareOutputData, 准备输往后台数据时出错");
        }

        return true;
    }


    /**生成错误信息*/
    private void buildError(String funcName, String errMes)
    {
        CError tError = new CError();

        tError.moduleName = "LGAutoDeliver";
        tError.functionName = funcName;
        tError.errorMessage = errMes;

        this.mErrors.addOneError(tError);
    }

    /**设置默认使用的信箱*/
    private boolean setDefaultBox()
    {
        //先将原默认使用信箱设为非默认，再设置当前默认信箱
        String sql = "";
        if (mLGAutoDeliverRuleSchema.getGoalType()
            .equals(CheckData.SCAN_RULE))
        {
            sql =
                "  update LGAutoDeliverRule "
                + "set defaultFlag='0', "
                + "operator='" + mGlobalInput.Operator + "', "
                + "ModifyDate='" + mCurrentDate + "', "
                + "ModifyTime='" + mCurrentTime + "' "
                + "where ruleContent='" + mLGAutoDeliverRuleSchema.getRuleContent()
                + "'  and sourceComCode = '" + mGlobalInput.ComCode
                + "'  and goalType = '" + mLGAutoDeliverRuleSchema.getGoalType()
                + "'  and defaultFlag='1' ";
        }
        else
        {
            sql =
                "update LGAutoDeliverRule " +
                "set defaultFlag='0', " +
                "operator='" + mGlobalInput.Operator + "', " +
                "ModifyDate='" + mCurrentDate + "', " +
                "ModifyTime='" + mCurrentTime + "' " +
                "where sourceComCode = '" + mGlobalInput.ComCode
                + "'  and goalType = '" + mLGAutoDeliverRuleSchema.getGoalType()
                + "'   and defaultFlag='1' ";
        }

        String sql2 =
            "update LGAutoDeliverRule " +
            "set defaultFlag='1', " +
            "operator='" + mGlobalInput.Operator + "', " +
            "ModifyDate='" + mCurrentDate + "', " +
            "ModifyTime='" + mCurrentTime + "' " +
            "where ruleNo='" + mLGAutoDeliverRuleSchema.getRuleNo() + "' ";

        map.put(sql, "UPDATE");
        map.put(sql2, "UPDATE");

        return true;
    }

    private boolean deleteRule()
    {
        String sql =
            "delete from LGAutoDeliverRule " +
            "where ruleNo='" +  mLGAutoDeliverRuleSchema.getRuleNo() + "' ";

        map.put(sql, "DELETE");

        return true;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String arg[])
    {
        LGAutoDeliverRuleSchema dr = new LGAutoDeliverRuleSchema();
        GlobalInput gi = new GlobalInput();

        dr.setRuleNo("26");
        dr.setRuleContent("");
        dr.setSourceComCode("86");
        dr.setGoalType("4");
        dr.setTarget("000054");

        gi.Operator = "endor0";
        gi.ManageCom = "86";

        VData v = new VData();
        v.add(dr);
        v.add(gi);

        TaskAutoDeliverRuleBL ad = new TaskAutoDeliverRuleBL();
        if(!ad.submitData(v, "SETDEFAULT||MAIN"))
        {
            System.out.println(ad.mErrors.getErrContent());
        }
    }

    private void jbInit() throws Exception {
    }

}


