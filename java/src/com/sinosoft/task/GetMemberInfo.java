package com.sinosoft.task;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: 在系统中常常会查询操作员的信息，此类集中处理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GetMemberInfo
{
    public GetMemberInfo()
    {
    }

    public static String getPostNo(GlobalInput a)
    {
        return getPostNo(a.Operator);
    }

    //查询的岗位，如果没有查到则，返回'-1'
    public static String getPostNo(String memberNo)
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        String sql = "select postNo "
                     + "from LGGroupMember "
                     + "where memberNo='" + memberNo + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            System.out.println("没有查到该用户的岗位代码");

            return "-1";
        }
    }

    /**
     * 得到成员的岗位名
     * @param operator String
     * @return String
     */
    public static String getPostName(String postNo)
    {
        LDCodeDB db = new LDCodeDB();
        db.setCodeType("position");
        db.setCode(postNo);
        if(db.getInfo())
        {
            return db.getCodeName();
        }
        else
        {
            return "+-1";
        }
    }

    public static String getMemberName(String memberNo)
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        String sql = "select userName "
                     + "from LDUser "
                     + "where userCode='" + memberNo + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            System.out.println("没有查到该用户的名字");

            return "-1";
        }
    }

    public static String getMemberName(GlobalInput a)
    {
        return getMemberName(a.Operator);
    }

    public static VData getSuperInfo(String memberNo)
    {
        StringBuffer info = new StringBuffer("");
        VData v = new VData();

        //成员在小组内的相关信息
        String sql = "select * "
                     + "from LGGroupMember "
                     + "where memberNo="
                     + "    (select SuperManager "
                     + "     from LGGroupMember "
                     + "     where memberNo='" + memberNo + "') ";

        LGGroupMemberDB db = new LGGroupMemberDB();
        LGGroupMemberSet set = db.executeQuery(sql);
        if (set.size() == 1)
        {
            v.add(set.get(1));
        }

        //成员的系统帐号信息
        sql = "select * "
              + "from LDUser "
              + "where userCode='" + getSuperNo(memberNo) + "' ";

        LDUserDB userDB = new LDUserDB();
        LDUserSet userSet = userDB.executeQuery(sql);
        if (userSet.size() == 1)
        {
            v.add(userSet.get(1));
        }


        return v;
    }

    //查询业务上级的岗位
    public static String getSuperPostNo(String memberNo)
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        String sql = "select postNo "
                     + "from LGGroupMember "
                     + "where memberNo="
                     + "    (select SuperManager "
                     + "     from LGGroupMember "
                     + "     where memberNo='" + memberNo + "') ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            System.out.println("没有查到用户" + memberNo + "的业务上级的岗位代码");

            return "-1";
        }
    }

    public static String getPostSuperPostNo(String postNo)
    {
        if(postNo.substring(2).equals("01"))
        {
            return "PA01";
        }
        else
        {
            int temp = Integer.parseInt(postNo.substring(2));
            if(temp > 10)
            {
                return postNo.substring(0, 2);
            }
            else if(temp == 10)
            {
                return "PA09";
            }
            else
            {
                return postNo.substring(0, 3) + (temp - 1);
            }
        }
    }

    //查询业务上级的岗位
    public static String getSuperPostNo(GlobalInput a)
    {
        return getSuperPostNo(a.Operator);
    }

    //查询业务上级的编码
    public static String getSuperNo(String memberNo)
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        String sql = "select SuperManager "
                     + "from LGGroupMember "
                     + "where memberNo='" + memberNo + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            System.out.println("没有查到该用户的业务上级的编码");

            return "-1";
        }
    }

    //查询业务上级的编码
    public static String getSuperNo(GlobalInput a)
    {
        return getSuperNo(a.Operator);
    }

    //查询的所属机构编码
    public static String getComCode(String userCode)
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        String sql = "select ComCode "
                     + "from LDUser "
                     + "where userCode='" + userCode + "' ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            System.out.println("没有查到该用户的所属机构编码");

            return "-1";
        }
    }

    public static String getComCode(GlobalInput a)
    {
        return getComCode(a.Operator);
    }

    //得到机构名称
    public static String getComNameByComCode(GlobalInput g)
    {
        return getComNameByComCode(g.ComCode);
    }

    //得到机构名称
    public static String getComNameByComCode(String comCode)
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(comCode);
        tLDComDB.getInfo();

        if(tLDComDB != null)
        {
            return tLDComDB.getName();
        }
        else
        {
            System.out.println("没有查到相应的机构名");
            return "-1";
        }
    }

    //查询员的所属机构名
    public static String getComName(String userCode)
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        String sql = "select Name "
                     + "from LDCom "
                     + "where ComCode="
                     + "        (select ComCode "
                     + "         from LDUser "
                     + "         where userCode='" + userCode + "') ";

        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 1)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            System.out.println("没有查到该用户的所属机构名");

            return "-1";
        }
    }

    /**查询用户的信箱*/
    public static final String getWorkBoxNo(String member)
    {
        LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
        tLGWorkBoxDB.setOwnerNo(member);
        return tLGWorkBoxDB.query().get(1).getWorkBoxNo();
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa0001";
        gi.ComCode = "86";
        gi.ManageCom = gi.ComCode;

        VData data = new VData();
        data.add(gi);

        GetMemberInfo bl = new GetMemberInfo();
        System.out.println(bl.getComNameByComCode(gi));
        System.out.println(bl.getComNameByComCode(gi.ComCode));
        System.out.println(bl.getComName(gi.Operator));
    }
}
