package com.sinosoft.task;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public final class Task
{
    /**作业状态：未经办*/
    public static final String WORK_STATE_INIT = "2"; //未经办
    public static final String WORKSTATUS_UNDO = "2"; //未经办
    public static final String WORKSTATUS_DOING = "3"; //正经办
    public static final String WORKSTATUS_WAITCONFIRM = "4"; //待审批
    public static final String WORKSTATUS_DONE = "5"; //结案
    public static final String WORKSTATUS_ARCHIVE = "6"; //存档
    public static final String WORKSTATUS_UNITED = "7"; //被合并
    public static final String WORKSTATUS_CANCEL = "8"; //已撤销
    public static final String WORKSTATUS_CONFIRMPASS = "99"; //审批通过
    public static final String WORKSTATUS_CONFIRMFAIL = "88"; //审批不通过

    //作业历史类型
    public static final String HISTORY_TYPE_INIT = "0"; //受理
    public static final String HISTORY_TYPE_DELIVER = "1"; //转发
    public static final String HISTORY_TYPE_CDELIVER = "2"; //复制转发
    public static final String HISTORY_TYPE_SDLETTER = "3"; //下发函件
    public static final String HISTORY_TYPE_BKLETTER = "4"; //函件回销
    public static final String HISTORY_TYPE_UW = "5"; //人工核保
    public static final String HISTORY_TYPE_EXAMIN = "6"; //审批
    public static final String HISTORY_TYPE_CONFIRM = "7"; //保全确认
    public static final String HISTORY_TYPE_CHARGE = "8"; //收费
    public static final String HISTORY_TYPE_FINISH = "9"; //结案
    public static final String HISTORY_TYPE_SCAN = "10"; //申请材料扫描
    public static final String HISTORY_TYPE_CONBINE = "11"; //受理件合并
    public static final String HISTORY_TYPE_CANCEL = "12"; //受理件撤销
    public static final String HISTORY_TYPE_EASY_EDOR = "13"; //简易保全
    public static final String HISTORY_TYPE_REMARK = "14"; //批注
    public static final String HISTORY_TYPE_SDEXAMIN = "15"; //送审批
    public static final String HISTORY_TYPE_FRCBKLETTER = "16"; //强制回销
    public static final String HISTORY_TYPE_EDIT = "17"; //编辑工单信息
    public static final String HISTORY_TYPE_GETBACK = "18"; //工单收回
    public static final String HISTORY_TYPE_DELETEEDOR = "19"; //保全撤销
    public static final String HISTORY_TYPE_BANKFAILDEAL = "19"; //保全撤销

    //工单类型
    public static final String WORK_TYPE_BQ = "050013";  //团单续期催缴
    public static final String WORK_TYPE_NEXTFEEG = "070014";  //团单续期催缴
    public static final String WORK_TYPE_MJ = "070015";  //续保续期—特需险满期结
    public static final String WORK_TYPE_NEXTFEEP = "070016";  //团单续期催缴

    public static final String WORK_TYPE_XQXB = "07";  //续期续保
    public static final String WORK_TYPE_XQXBP = "070001";  //续期续保-个险
    public static final String WORK_TYPE_LPXB = "070002";  //理賠續保覈保

    //续期催缴记录类型
    public static final String HASTENTYPE_ENDOR = "1";  //保全未收费催缴
    public static final String HASTENTYPE_NEXTFEEG = "2";  //团单续期催缴
    public static final String HASTENTYPE_NEXTFEEP = "3";  //个单续期催缴

    public static final String DEFAULT_YES = "1"; //默认
    public static final String DEFAULT_NO = "1"; //默认

    //受理途径
    public static final String ACCEPTWAY_COUNTER = "0"; //柜台
    public static final String ACCEPTWAY_OPTR = "1";  //业务员代办
    public static final String ACCEPTWAY_LETTER = "2";  //信函
    public static final String ACCEPTWAY_PHONE = "3";  //电话申请
    public static final String ACCEPTWAY_EMAIL = "4";  //email
    public static final String ACCEPTWAY_FAX = "5";  //传真
    public static final String ACCEPTWAY_OTHER = "6";  //其他
    public static final String ACCEPTWAY_SERVER = "7";  //日处理程序生成
    public static final String ACCEPTWAY_INNER = "8";  //内部生成
    public static final String ACCEPTWAY_CDELIVER = "9";  //复制转交
    public static final String ACCEPTWAY_BATCH_AUTO = "12";  //批处理


    //申请人类型
    public static final String APPLYTYPE_APPNT = "0";  //
    public static final String APPLYTYPE_INSURED = "1";  //
    public static final String APPLYTYPE_OPT = "2";  //
    public static final String APPLYTYPE_INNER = "3";  //
}
