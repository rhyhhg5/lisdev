package com.sinosoft.aaa;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;



public class AgentcodeUI {
	
	 public CErrors mErrors = new CErrors();
	    
	    private AgentcodeBL mAgentcodeBL=null;
	   
	    public AgentcodeUI()
	    {
	    	mAgentcodeBL=new AgentcodeBL();
	    }

	    public boolean submitData(VData cInputData, String cOperator)
	    {
	    	AgentcodeBL b1=new AgentcodeBL();
	    	
	        if(!b1.submitData(cInputData, cOperator))
	        {
	            mErrors.copyAllErrors(b1.mErrors);
	            return false;
	        }

	        return true;
	    }
	
	

}
