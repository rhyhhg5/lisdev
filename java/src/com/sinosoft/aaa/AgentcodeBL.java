package com.sinosoft.aaa;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class AgentcodeBL {
	
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String Agentcom = "";
    private String Name = "";
    private String Managecom = "";
    private String Upagentcom = "";
    private String Bankcode = "";
    private String mOperate = "";
    
    private String mCurDate = PubFun.getCurrentDate();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public AgentcodeBL()
    {
    }

    public boolean submitData(VData cInputData, String mOperate) 
    {
    	this.mOperate = mOperate;
    	
        if (!getInputData(cInputData)){
            return false;
        }
     
        if (!checkData()){
        	return false;
        }
     
        if (!dealData()){	
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    
    private boolean checkData()
    {
        return true;
    }

   
    public boolean dealData()
    {
        System.out.println("==============dealData()================"); 
    	
        MMap tmap = new MMap();
        VData tInputData = new VData();
        
        if(mOperate.equals("UPDATE")){
        	
        	if (Agentcom != null && !Agentcom.equals("") && Bankcode != null && !Bankcode.equals("") && Managecom !=null && !Managecom.equals("") ) {
            	tmap.put("update lacom set Bankcode = '"+Bankcode+"',Managecom= '"+Managecom+"' where Agentcom = '"+Agentcom+"'", "UPDATE");
            } 
        	
        }else if(mOperate.equals("DELETE")){
        	
        	tmap.put("delete lacom where Agentcom = '"+Agentcom+"'", "DELETE");
        	
        }else if(mOperate.equals("INSERT")){
        	
        	//String sql="insert into lacom (Agentcom,NAme,Managecom,Bankcode) values('"+Agentcom+"','"+Name+"','"+Managecom+"','"+Bankcode+"')";
String sql="INSERT INTO lacom VALUES ('"+Agentcom+"','"+Managecom+"','A','A','PY002123','"+Agentcom+"',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'"+Bankcode+"',NULL,NULL,NULL,'01','Y','qzyb05',{d '2016-11-28' },'16:36:02',{d '2017-10-24' },'15:24:56','02',NULL,'911000001000013428',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,{d '2015-08-20' },{d '2018-08-20' },'3','01',0,0,0,0,NULL,'N',NULL,'00',NULL,NULL,NULL,NULL)";        	
        	
        	/*String sql="insert into lacom (Agentcom,Name,Managecom,Bankcode) "
        			+ "values('"+Agentcom+"','"+Name+"','"+Managecom+"','"+Bankcode+"')";
     */   	
        	tmap.put(sql, "INSERT");

        }
        
        tInputData.add(tmap);    
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buiError( "gatherData", "数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            tmap=null;
            tInputData=null;
            return false;
        }
        
    	tmap=null;
        tInputData=null;
        return true;
  }
    

    
	private boolean createFile(){
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI+"===================");

        if (mGI == null || tf == null)
        {
        	buiError("getInputData","出错啦！！！");
            return false;
        }
        Agentcom = (String) tf.getValueByName("Agentcom");
        Name = (String) tf.getValueByName("Name");
        Managecom = (String) tf.getValueByName("Managecom");
        Upagentcom = (String) tf.getValueByName("Upagentcom");
        Bankcode = (String) tf.getValueByName("Bankcode");
        mOperate = (String) tf.getValueByName("mOperate");
        
        System.out.println("Agentcom="+Agentcom);
        System.out.println("Name="+Name);
        System.out.println("Managecom="+Managecom);
        System.out.println("Upagnetcom="+Agentcom);
        System.out.println("Bankcode="+Bankcode);
        System.out.println("mOperate="+mOperate);

        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "InterfacetableMaintainBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    

}
