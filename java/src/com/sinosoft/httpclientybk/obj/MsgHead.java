package com.sinosoft.httpclientybk.obj;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;



public class MsgHead extends BaseXmlSch{
	private static final long serialVersionUID = 7108853584002630043L;
	private String USER;
	private String PASSWORD;
	private String REQUEST_TYPE;
	private String TASK_NO;
	public String getUSER() {
		return USER;
	}
	public String getTASK_NO() {
		return TASK_NO;
	}
	public void setTASK_NO(String tASK_NO) {
		TASK_NO = tASK_NO;
	}
	public void setUSER(String uSER) {
		USER = uSER;
	}
	public String getPASSWORD() {
		return PASSWORD;
	}
	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUESTTYPE) {
		REQUEST_TYPE = rEQUESTTYPE;
	}
	
}
