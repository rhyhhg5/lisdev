package com.sinosoft.httpclientybk.obj;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;



public class MsgResHead extends BaseXmlSch
{
    private static final long serialVersionUID = -4202748903354843378L;
    private String REQUEST_TYPE;
    private String TASK_NO;
    private String RESPONSE_CODE;
    private String ERROR_MESSAGE;
    private String TRANS_NO;
    
    public String getTRANS_NO() {
		return TRANS_NO;
	}
	public void setTRANS_NO(String tRANS_NO) {
		TRANS_NO = tRANS_NO;
	}
	public String getResultStatus() {
		return ResultStatus;
	}
	public void setResultStatus(String resultStatus) {
		ResultStatus = resultStatus;
	}
	private String ResultStatus;
    
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUESTTYPE) {
		REQUEST_TYPE = rEQUESTTYPE;
	}
	public String getTASK_NO() {
		return TASK_NO;
	}
	public void setTASK_NO(String tASK_NO) {
		TASK_NO = tASK_NO;
	}
	public String getRESPONSE_CODE() {
		return RESPONSE_CODE;
	}
	public void setRESPONSE_CODE(String rESPONSECODE) {
		RESPONSE_CODE = rESPONSECODE;
	}
	public String getERROR_MESSAGE() {
		return ERROR_MESSAGE;
	}
	public void setERROR_MESSAGE(String eRRORMESSAGE) {
		ERROR_MESSAGE = eRRORMESSAGE;
	}
    
}
