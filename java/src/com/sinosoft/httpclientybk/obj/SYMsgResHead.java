package com.sinosoft.httpclientybk.obj;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class SYMsgResHead extends BaseXmlSch{
	private static final long serialVersionUID = -4819065085485770949L;
	private String REQUEST_TYPE;
	private String TASK_NO;
	    
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUESTTYPE) {
		REQUEST_TYPE = rEQUESTTYPE;
	}
	public String getTASK_NO() {
		return TASK_NO;
	}
	public void setTASK_NO(String tASKNO) {
		TASK_NO = tASKNO;
	}
	    
}
