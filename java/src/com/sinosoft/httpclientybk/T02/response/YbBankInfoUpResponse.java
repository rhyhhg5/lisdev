package com.sinosoft.httpclientybk.T02.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {
	    "head",
	    "responseNodes"
	})

	@XmlRootElement(name = "Package")
	public class YbBankInfoUpResponse {

	    @XmlElement(name = "Head", required = true)
	    protected Head head;
	    @XmlElement(name = "ResponseNodes", required = true)
	    protected ResponseNodes responseNodes;

	    /**
	     * Gets the value of the head property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link Head }
	     *     
	     */
	    public Head getHead() {
	        return head;
	    }

	    /**
	     * Sets the value of the head property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link Head }
	     *     
	     */
	    public void setHead(Head value) {
	        this.head = value;
	    }

	    /**
	     * Gets the value of the responseNodes property.
	     * 
	     * @return
	     *     possible object is
	     *     {@link ResponseNodes }
	     *     
	     */
	    public ResponseNodes getResponseNodes() {
	        return responseNodes;
	    }

	    /**
	     * Sets the value of the responseNodes property.
	     * 
	     * @param value
	     *     allowed object is
	     *     {@link ResponseNodes }
	     *     
	     */
	    public void setResponseNodes(ResponseNodes value) {
	        this.responseNodes = value;
	    }

	}

