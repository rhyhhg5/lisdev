package com.sinosoft.httpclientybk.T02.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

// TODO Auto-generated constructor stub
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "head", "requestNodes" })
@XmlRootElement(name = "XmlBankInfoUPRequest")
public class YbBankInfoUpRequest {

	@XmlElement(name = "Head", required = true)
	protected Head head;
	@XmlElement(name = "RequestNodes", required = true)
	protected RequestNodes requestNodes;

	public YbBankInfoUpRequest() {
		head = new Head();
		requestNodes = new RequestNodes();
	}

	/**
	 * 提供公共的set和get方法
	 */
	public Head getHead() {
		return head;
	}

	public void setHead(Head value) {
		this.head = value;
	}

	public RequestNodes getRequestNodes() {
		return requestNodes;
	}

	public void setRequestNodes(RequestNodes value) {
		this.requestNodes = value;
	}

}
