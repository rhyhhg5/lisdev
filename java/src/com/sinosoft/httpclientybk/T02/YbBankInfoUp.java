package com.sinosoft.httpclientybk.T02;


import com.sinosoft.httpclientybk.T02.request.RequestNode;
import com.sinosoft.httpclientybk.T02.request.YbBankInfoUpRequest;
import com.sinosoft.httpclientybk.T02.response.YbBankInfoUpResponse;
import com.sinosoft.httpclientybk.util.CommunicateServiceImpl;
import com.sinosoft.httpclientybk.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbBankInfoUp {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /**用于存储传入的案件号*/
	  private String mPrtNo;
	  /**用于存储续保次数*/
	  private String mRenewCount = "";
	  /**当前日期*/
	  private String tMakeDate = PubFun.getCurrentDate();
	  /**当前时间*/
	  private String tMakeTime = PubFun.getCurrentTime();
	  
	  private String mSuccessInfo;
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  /**
	   * 提供无参的构造方法
	   */
	  public YbBankInfoUp() {}
	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }

 		if(!dealData()){
			return false;
		}
 		if(!prepareData()){
 			return false;
 		}
		return true;
	}
	/**
	 * 获取传入的方法
	 */
	public boolean getInputData(){
		/**
  	 * 接收传入的数据
 		 */
 		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
 		mPrtNo = (String) tempTransferData.getValueByName("PrtNo");
 		System.out.println(mPrtNo);
 		if(mPrtNo==null&&mPrtNo.equals("")){
 			CError tCError = new CError();
 			tCError.moduleName = "ConnectZBXServiceImpl";
 			tCError.functionName = "getMessage";
 			tCError.errorMessage = "传入的印刷号为空！";
 			this.mErrors.addOneError(tCError);
 			return false;
		}
 		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData(){
		/**
 		 * 封装请求报文
 		 * 上传保单号，从中保信获取税优识别码
 		 */
     	YbBankInfoUpRequest tYbInfoUpRequest = new YbBankInfoUpRequest();
     	RequestNode tRequestNode = new RequestNode();
     	tRequestNode.setPrtNo(mPrtNo);
     	tYbInfoUpRequest.getRequestNodes().getRequestNode().add(tRequestNode);
     	try {
			String responseXml = CommunicateServiceImpl.post(tYbInfoUpRequest,mOperate);
			/**获取客户校验平台的平台客户编码 */
			YbBankInfoUpResponse tYbInfoUpResponse = (YbBankInfoUpResponse)XmlParseUtil.xmlToDto(responseXml, YbBankInfoUpResponse.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tYbInfoUpResponse.getHead().getResponseCode();
			String resultStatus = tYbInfoUpResponse.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tYbInfoUpResponse.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("1")){
					System.out.println("系统正常,业务成功处理!");
					/**
					 * 上传 平台案件号 从信息验证平台上获取返回信息
					 */
					
					mErrorInfo = "系统正常,业务成功处理!";
					saveErrorList(tYbInfoUpResponse,mErrorInfo);
					saveResultInfo(tYbInfoUpResponse);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&(resultStatus.equals("4")||resultStatus.equals("5"))){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tYbInfoUpResponse,mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&(resultStatus.equals("0")||resultStatus.equals("6"))){
					System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYbInfoUpResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("2")){
					System.out.println("系统正常，输入数据部分成功!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据部分成功!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYbInfoUpResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("3")){
					System.out.println("系统正常，输入数据对象已存在!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据对象已存在!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYbInfoUpResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYbInfoUpResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYbInfoUpResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
//			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
//				resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
//				/**
//				 * 上传 平台客户号和保单号码 从信息验证平台上获取税优识别码
//				 */
//				mTaxCode = tNBU001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPolicyOfSingleList().getPolicyOfSingle().get(0).getTaxCode();
//				
////				String updLCCont = "update lccont set taxcode = '"+mTaxCode+"' where a.contno '"+mContNo+"'";
////				boolean updLCContF=tExeSQL.execUpdateSQL(updLCCont);
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "正确处理，等待异步返回!";
//				this.mErrors.addOneError(tCError);
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1001")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "安全验证失败!";
//				this.mErrors.addOneError(tCError);
//				return false;
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1002")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "报文为空!";
//				this.mErrors.addOneError(tCError);
//				return false;
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1003")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "报文解析失败!";
//				this.mErrors.addOneError(tCError);
//				return false;
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1004")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "系统异常!";
//				this.mErrors.addOneError(tCError);
//				return false;
//			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			CError tCError = new CError();
			tCError.moduleName = "GetTaxCode";
			tCError.functionName = "getTaxCode";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	public boolean prepareData(){
		/**
		 * 将返回信息放入到结果集中
		 */
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("SuccessInfo;", mSuccessInfo);

		mResult.add(tTransferData);
		return true;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(YbBankInfoUpResponse tYBtestResponse,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20);
		String TransType = tYBtestResponse.getHead().getTransType();
		String TransNo = tYBtestResponse.getHead().getTransNo();
		String responseCode = tYBtestResponse.getHead().getResponseCode();
		String resultStatus = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String insertSQL = "insert into YBKErrorList values ('"+ID+"','"+TransType+"','"+TransNo+"','"+mPrtNo+"','"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"','"+tMakeDate+"','"+tMakeTime+"','"+tMakeDate+"','"+tMakeTime+"')";
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
	}
	public void saveResultInfo(YbBankInfoUpResponse tYBtestResponse)
	{
		String TransType = tYBtestResponse.getHead().getTransType();
		String TransNo = tYBtestResponse.getHead().getTransNo();
		String responseCode = (tYBtestResponse.getHead().getResponseCode()).substring(0,2);
		String otherno = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getResult().getPrtNo();
		String feeorpaymensequenceno = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPAYMENT_RESULT_SEQUENCE_NO();
		String tRenewCount = "select renewcount from lcpol where prtno = '"+mPrtNo+"' with ur";
		SSRS tSSRS = new ExeSQL().execSQL(tRenewCount);
		if("".equals(tSSRS)||null==tSSRS){
			System.out.println("续保次数查询失败");
			mRenewCount = "0";
		}else{
			mRenewCount = tSSRS.GetText(1, 1);
		}
		String insertSQL = "insert into YBK_T_BankPremInfoResult values "
		 		+ "('"+tYBtestResponse.getHead().getDataBatchNo()+"','"+tYBtestResponse.getHead().getDataSerialNo()+"',"
		 		+ "'"+otherno+"','"+TransType+"','"+TransNo+"','"+responseCode+"','"+mErrorInfo+"','"+feeorpaymensequenceno+"',current date,current time,current date,current time,'"+mRenewCount+"')";
		System.out.println("T02插入YBK_T_BankPremInfoResult表sql:"+insertSQL);
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("失败");
			}
	}
	/**
	 * 获取结果集
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", "160018380872");
		tVData.add(tTransferData);
		YbBankInfoUp tUploadClaim= new YbBankInfoUp();
		tUploadClaim.submitData(tVData, "YBKPT");
		
		
	}
}
