package com.sinosoft.httpclientybk.inf;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.sinosoft.httpclientybk.dto.c12.request.C12Request;
import com.sinosoft.httpclientybk.dto.c12.request.RequestNode;
import com.sinosoft.httpclientybk.dto.c12.response.C12Response;
import com.sinosoft.httpclientybk.util.CommunicateServiceImpl;
import com.sinosoft.httpclientybk.util.XmlParseUtil;
import com.sinosoft.lis.db.YBK_C12_LIS_ResponseInfoDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>ClassName:  C12</p>
 * <p>Description: C12类文件</p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2017-2-6 下午05:54:35
 * @author Yu ZhiWei
 */
public class C12 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 携带数据的类 */
	private TransferData tempTransferData = new TransferData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 用于存储传入的案件号 */
	private String mPrtNo;

	/** 用于存储核心理赔号 */
	private String tClaimNo;
	/** 用于存储平台理赔号 */
	private String mClaimCode;
	/** 用于存储异步理预约码 */
	private String mSequenceNo;
	/** 存储交互错误信息 */
	private String mErrorInfo;
	
	/** 个险理赔注销编码 */
	String mClaimCancellationSequenceNo = "";
	
	private String dataBatchNo;
	private String dataSerialNo;
	private String caseNo;
	private String requestType;
	private String taskNo;
	private String responseCode;
	private String errorMessage;
	private YBK_C12_LIS_ResponseInfoDB db = new YBK_C12_LIS_ResponseInfoDB();

	/**
	 * 提供无参的构造方法
	 */
	public C12() {
	}

	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		if (!getInputData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		if (!prepareData()) {
			return false;
		}
		return true;
	}

	/**
	 * 获取传入的方法
	 */
	public boolean getInputData() {
		/**
		 * 接收传入的数据
		 */
		tempTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mPrtNo = (String) tempTransferData.getValueByName("PrtNo");
		System.out.println(mPrtNo);
		if (mPrtNo == null && mPrtNo.equals("")) {
			CError tCError = new CError();
			tCError.moduleName = "ConnectZBXServiceImpl";
			tCError.functionName = "getMessage";
			tCError.errorMessage = "传入的印刷号为空！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData() {
		/**
		 * 封装请求报文dto 上传保单号，从中保信获取税优识别码
		 */
		C12Request tC12Request = new C12Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setPrtNo(mPrtNo);
		tC12Request.getRequestNodes().getRequestNode().add(tRequestNode);
		try {
			String responseXml = CommunicateServiceImpl.post(tC12Request,
					mOperate);
			/** 获取客户校验平台的平台客户编码 */
			C12Response tC12Response = (C12Response) XmlParseUtil.xmlToDto(
					responseXml, C12Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tC12Response.getHead().getResponseCode();
			String resultStatus = tC12Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tC12Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultInfoDesc();

			if (responseCode != null && !responseCode.equals("")
					&& responseCode.equals("0000")) {
				if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("1")) {
					System.out.println("系统正常，业务成功处理！");
					mErrorInfo = "系统正常，业务成功处理!";
					saveErrorList(tC12Response, mErrorInfo);
					saveReturnSuccInfo(tC12Response, mErrorInfo, responseXml);
				} else if (resultStatus != null
						&& !resultStatus.equals("")
						&& (resultStatus.equals("4") || resultStatus
								.equals("5"))) {
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tC12Response, mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				} else if (resultStatus != null
						&& !resultStatus.equals("")
						&& (resultStatus.equals("0") || resultStatus
								.equals("6"))) {
					System.out.println("系统正常，数据校验不通过!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为：" + resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tC12Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("2")) {
					System.out.println("系统正常，输入数据部分成功!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据部分成功!原因为："
							+ resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tC12Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("3")) {
					System.out.println("系统正常，输入数据对象已存在!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据对象已存在!原因为："
							+ resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tC12Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("03")) {
					System.out.println("系统正常，业务处理失败!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，业务处理失败!原因为：" + resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tC12Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("99")) {
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tC12Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
			// /**
			// * 上传 平台客户号和保单号码 从信息验证平台上获取税优识别码
			// */
			// mTaxCode =
			// tNBU001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPolicyOfSingleList().getPolicyOfSingle().get(0).getTaxCode();
			//
			// // String updLCCont =
			// "update lccont set taxcode = '"+mTaxCode+"' where a.contno '"+mContNo+"'";
			// // boolean updLCContF=tExeSQL.execUpdateSQL(updLCCont);
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "正确处理，等待异步返回!";
			// this.mErrors.addOneError(tCError);
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1001")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "安全验证失败!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1002")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "报文为空!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1003")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "报文解析失败!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1004")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "系统异常!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			CError tCError = new CError();
			e.printStackTrace();
			tCError.moduleName = "GetTaxCode";
			tCError.functionName = "getTaxCode";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	public static String generateRandomStr(int len) {
		String generateSource = "0123456789abcdefghigklmnopqrstuvwxyz";
		String rtnStr = "";
		for (int i = 0; i < len; i++) {
			String nowStr = String.valueOf(generateSource.charAt((int) Math
					.floor(Math.random() * generateSource.length())));
			rtnStr += nowStr;
			generateSource = generateSource.replaceAll(nowStr, "");
		}
		return rtnStr;
	}
	
	public boolean prepareData() {
		/**
		 * 将返回信息放入到结果集中
		 */
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ClaimNo", tClaimNo);
		tTransferData.setNameAndValue("ClaimCode", mClaimCode);
		tTransferData.setNameAndValue("SequenceNo", mSequenceNo);
		System.out.println(tClaimNo);
		System.out.println(mClaimCode);
		System.out.println(mSequenceNo);
		mResult.add(tTransferData);
		return true;
	}

	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param tC12Response
	 * @param mErrorInfo
	 */
	public void saveErrorList(C12Response tC12Response, String mErrorInfo) {
		String ID = PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20);
		String TransType = tC12Response.getHead().getTransType();
		String TransNo = tC12Response.getHead().getTransNo();
		String responseCode = tC12Response.getHead().getResponseCode();
		String resultStatus = tC12Response.getResponseNodes().getResponseNode()
				.get(0).getResult().getResultStatus();
		String insertSQL = "insert into YBKErrorList values ('" + ID + "','"
				+ TransType + "','" + TransNo + "','" + mPrtNo + "','"
				+ responseCode + "','" + resultStatus + "','" + mErrorInfo
				+ "','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"')";
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(insertSQL)) {
			System.out.println("保存返回信息失败--" + TransType + "--" + resultStatus);
		}
		System.out.println("保存返回信息成功--" + TransType + "--" + resultStatus);
	}

	/**
	 * 存储平台返回的成功信息
	 * @param tC12Response
	 * @param mErrorInfo
	 * @param responseXml
	 */
	private void saveReturnSuccInfo(C12Response tC12Response, String mErrorInfo,
			String responseXml){
		dataBatchNo = tC12Response.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getDataBatchNo();
		dataSerialNo = PubFun1.CreateMaxNo("SEQ_YBK_C12BATC", 20);
		caseNo = tC12Response.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getPrtNo();
		requestType = tC12Response.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getRequestType();
		taskNo = tC12Response.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getTaskNo();
		responseCode = tC12Response.getResponseNodes().getResponseNode().get(0)
				.getResult().getResultStatus();
		errorMessage = tC12Response.getResponseNodes().getResponseNode().get(0)
				.getResult().getResultInfoDesc();
		//个险理赔注销编码
		mClaimCancellationSequenceNo = tC12Response.getResponseNodes().getResponseNode()
				.get(0).getSuccessInfo().getClaimCancellationSequenceNo();
		
		db.setDataBatchNo(dataBatchNo);
		db.setDataSerialNo(dataSerialNo);
		db.setCaseNo(caseNo);
		db.setRequestType(requestType);
		db.setTaskNo(taskNo);
		db.setResponseCode(responseCode);
		db.setErrorMessage(errorMessage);
		db.setClaimCancellationSequenceNo(mClaimCancellationSequenceNo);
		
		db.setOperator("sys");
		db.setMakeDate(new SimpleDateFormat("yyyy-MM-dd")
				.format(new Date()));
		db.setMakeTime(new SimpleDateFormat("HH:mm:ss")
				.format(new Date()));
		db.setModifyDate(new SimpleDateFormat("yyyy-MM-dd")
				.format(new Date()));
		db.setModifyTime(new SimpleDateFormat("HH:mm:ss")
				.format(new Date()));
		//补充----案件注销类型---20170615
		String c12PrtNo="";
		String sqlPrtNo = "select rgtstate from LLCaseOpTime where caseno=(select caseno from LLCaseExt where consultNo='"+mPrtNo+"') order by startdate,starttime desc fetch first 1 rows only" ;
		SSRS tSSRSPrt = new ExeSQL().execSQL(sqlPrtNo);
		if (tSSRSPrt == null || tSSRSPrt.getMaxRow() <= 0) {
			
		}else {
			String rgtstate = tSSRSPrt.GetText(1, 1);
			if("04".equals(rgtstate)){
				c12PrtNo = "02";
			}else{
				c12PrtNo = "01";
			}
		}
		db.setPrtNo(c12PrtNo);
		
		if (!db.insert()) {
			System.out.println("保存返回成功信息失败--" + requestType + "--" + errorMessage);
		}
		System.out.println("保存返回成功信息成功--" + requestType + "--" + errorMessage);
		
		
//		StringReader tInXmlReader;
//		SAXBuilder tSAXBuilder;
//		Document document;
//		try {
//			tInXmlReader = new StringReader(responseXml);
//			tSAXBuilder = new SAXBuilder();
//			document = tSAXBuilder.build(tInXmlReader);
//			Element rootElement = document.getRootElement();
//
//			Element successInfo = rootElement.getChild("ResponseNodes")
//					.getChild("ResponseNode").getChild("SUCCESS_INFO");
//
//
//
//		} catch (JDOMException e) {
//			throw new RuntimeException(e.getMessage());
//		} catch (Exception e) {
//			throw new RuntimeException(e.getMessage());
//		}
	}

	/**
	 * 获取结果集
	 * 
	 * @param args
	 */
	public VData getResult() {
		return mResult;
	}

	/**
	 * 测试用
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		// 0201609010302
		// 0139699670000
		// 162016053101
		// PD0000009013 测试
		//工单号 20141224000003
//		tTransferData.setNameAndValue("PrtNo", "20140326000010");
		tTransferData.setNameAndValue("PrtNo", "C0000170213000001");
		

		tVData.add(tTransferData);
		C12 tUploadClaim = new C12();
		tUploadClaim.submitData(tVData, "C12");

		System.out.println("******LP C12 返回核心******");
	}
}
