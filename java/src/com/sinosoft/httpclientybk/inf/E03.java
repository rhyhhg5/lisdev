package com.sinosoft.httpclientybk.inf;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.httpclientybk.dto.e03.request.E03Request;
import com.sinosoft.httpclientybk.dto.e03.request.RequestNode;
import com.sinosoft.httpclientybk.dto.e03.response.E03Response;
import com.sinosoft.httpclientybk.util.CommunicateServiceImpl;
import com.sinosoft.httpclientybk.util.XmlParseUtil;
import com.sinosoft.lis.db.YBK_E03_LIS_ResponseInfoDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>ClassName:  E01</p>
 * <p>Description: E01类文件</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2017-1-12 上午09:59:22
 * @author Yu ZhiWei
 */
public class E03 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 携带数据的类 */
	private TransferData tempTransferData = new TransferData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 用于存储传入的案件号 */
	private String mPrtNo;
	/**当前日期*/
	private String tMakeDate = PubFun.getCurrentDate();
	/**当前时间*/
	private String tMakeTime = PubFun.getCurrentTime();

	/** 用于存储核心理赔号 */
	private String tClaimNo;
	/** 用于存储平台理赔号 */
	private String mClaimCode;
	/** 用于存储异步理预约码 */
	private String mSequenceNo;
	/** 存储交互错误信息 */
	private String mErrorInfo;
	
	/**保全退款流水号 */
	String mEndRefundNo = "";
	/**投保流水号 */
	String mPolicyFormerNo = "";
	private String dataBatchNo;
	private String dataSerialNo;
	private String prtno;//lijia 做RenewCount
	private String edorNo;
	private String requestType;
	private String taskNo;
	private String responseCode;
	private String errorMessage;
	private YBK_E03_LIS_ResponseInfoDB db = new YBK_E03_LIS_ResponseInfoDB();

	/**
	 * 提供无参的构造方法
	 */
	public E03() {
	}

	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		if (!getInputData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		if (!prepareData()) {
			return false;
		}
		return true;
	}

	/**
	 * 获取传入的方法
	 */
	public boolean getInputData() {
		/**
		 * 接收传入的数据
		 */
		tempTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mPrtNo = (String) tempTransferData.getValueByName("PrtNo");
		System.out.println(mPrtNo);
		if (mPrtNo == null && mPrtNo.equals("")) {
			CError tCError = new CError();
			tCError.moduleName = "ConnectZBXServiceImpl";
			tCError.functionName = "getMessage";
			tCError.errorMessage = "传入的印刷号为空！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData() {
		/**
		 * 封装请求报文dto 上传保单号，从中保信获取税优识别码
		 */
		E03Request tE03Request = new E03Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setPrtNo(mPrtNo);
		tE03Request.getRequestNodes().getRequestNode().add(tRequestNode);
		try {
			String responseXml = CommunicateServiceImpl.post(tE03Request,
					mOperate);
			/** 获取客户校验平台的平台客户编码 */
			E03Response tE03Response = (E03Response) XmlParseUtil.xmlToDto(
					responseXml, E03Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tE03Response.getHead().getResponseCode();
			String resultStatus = tE03Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tE03Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultInfoDesc();

			if (responseCode != null && !responseCode.equals("")
					&& responseCode.equals("0000")) {
				if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("1")) {
					System.out.println("系统正常，业务成功处理！");
					mErrorInfo = "系统正常，业务成功处理!";
					saveErrorList(tE03Response, mErrorInfo);
					saveReturnSuccInfo(tE03Response, mErrorInfo, responseXml);
				} else if (resultStatus != null
						&& !resultStatus.equals("")
						&& (resultStatus.equals("4") || resultStatus
								.equals("5"))) {
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tE03Response, mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				} else if (resultStatus != null
						&& !resultStatus.equals("")
						&& (resultStatus.equals("0") || resultStatus
								.equals("6"))) {
					System.out.println("系统正常，数据校验不通过!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为：" + resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tE03Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("2")) {
					System.out.println("系统正常，输入数据部分成功!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据部分成功!原因为："
							+ resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tE03Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("3")) {
					System.out.println("系统正常，输入数据对象已存在!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据对象已存在!原因为："
							+ resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tE03Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("03")) {
					System.out.println("系统正常，业务处理失败!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，业务处理失败!原因为：" + resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tE03Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("99")) {
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tE03Response, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
			// /**
			// * 上传 平台客户号和保单号码 从信息验证平台上获取税优识别码
			// */
			// mTaxCode =
			// tNBU001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPolicyOfSingleList().getPolicyOfSingle().get(0).getTaxCode();
			//
			// // String updLCCont =
			// "update lccont set taxcode = '"+mTaxCode+"' where a.contno '"+mContNo+"'";
			// // boolean updLCContF=tExeSQL.execUpdateSQL(updLCCont);
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "正确处理，等待异步返回!";
			// this.mErrors.addOneError(tCError);
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1001")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "安全验证失败!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1002")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "报文为空!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1003")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "报文解析失败!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1004")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "系统异常!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			CError tCError = new CError();
			e.printStackTrace();
			tCError.moduleName = "GetTaxCode";
			tCError.functionName = "getTaxCode";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	public static String generateRandomStr(int len) {
		String generateSource = "0123456789abcdefghigklmnopqrstuvwxyz";
		String rtnStr = "";
		for (int i = 0; i < len; i++) {
			String nowStr = String.valueOf(generateSource.charAt((int) Math
					.floor(Math.random() * generateSource.length())));
			rtnStr += nowStr;
			generateSource = generateSource.replaceAll(nowStr, "");
		}
		return rtnStr;
	}
	
	public boolean prepareData() {
		/**
		 * 将返回信息放入到结果集中
		 */
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ClaimNo", tClaimNo);
		tTransferData.setNameAndValue("ClaimCode", mClaimCode);
		tTransferData.setNameAndValue("SequenceNo", mSequenceNo);
		System.out.println(tClaimNo);
		System.out.println(mClaimCode);
		System.out.println(mSequenceNo);
		mResult.add(tTransferData);
		return true;
	}

	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param tE03Response
	 * @param mErrorInfo
	 */
	public void saveErrorList(E03Response tE03Response, String mErrorInfo) {
		String ID = PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20);
		String TransType = tE03Response.getHead().getTransType();
		String TransNo = tE03Response.getHead().getTransNo();
		String responseCode = tE03Response.getHead().getResponseCode();
		String resultStatus = tE03Response.getResponseNodes().getResponseNode()
				.get(0).getResult().getResultStatus();
		String insertSQL = "insert into YBKErrorList values ('" + ID + "','"
				+ TransType + "','" + TransNo + "','" + mPrtNo + "','"
				+ responseCode + "','" + resultStatus + "','" + mErrorInfo
				+ "','"+tMakeDate+"','"+tMakeTime+"','"+tMakeDate+"','"+tMakeTime+"')";
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(insertSQL)) {
			System.out.println("保存返回信息失败--" + TransType + "--" + resultStatus);
		}
		System.out.println("保存返回信息成功--" + TransType + "--" + resultStatus);
	}

	/**
	 * 存储平台返回的成功信息
	 * @param tE03Response
	 * @param mErrorInfo
	 * @param responseXml
	 */
	private void saveReturnSuccInfo(E03Response tE03Response, String mErrorInfo,
			String responseXml){
		dataBatchNo = tE03Response.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getDataBatchNo();
		dataSerialNo = PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20);
		edorNo = tE03Response.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getPrtNo();
		requestType = tE03Response.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getRequestType();
		taskNo = tE03Response.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getTaskNo();
		responseCode = tE03Response.getResponseNodes().getResponseNode().get(0)
				.getResult().getResultStatus();
		errorMessage = tE03Response.getResponseNodes().getResponseNode().get(0)
				.getResult().getResultInfoDesc();
		//保全退款流水号
		mEndRefundNo = tE03Response.getResponseNodes().getResponseNode()
				.get(0).getSuccessInfo().getEndRefundNo();
		if (mEndRefundNo != null && !"".equals(mEndRefundNo)) {
			// 判断是否为银行卡付款 LJAGet BankCode 付费
			BCPay(edorNo);
		}
		//投保流水号
		mPolicyFormerNo = tE03Response.getResponseNodes().getResponseNode()
				.get(0).getSuccessInfo().getPolicyFormerNo();
		//lijia 
		if((mEndRefundNo != null || !"".equals(mEndRefundNo)) && (mPolicyFormerNo ==null || "".equals(mPolicyFormerNo))){
			//ybk_e03_lis_responseinfo表的prtno暂时存renewcount
			String sql1="select renewcount from lbpol where edorno='"+edorNo+"'";
			ExeSQL exeSQL = new ExeSQL();
			SSRS tSSRS = exeSQL.execSQL(sql1);
			prtno=tSSRS.GetText(1, 1);
			System.out.println(prtno);
		}else {
			String sql2="select renewcount from lcpol where prtno in (select distinct prtno from lccont where contno='"+edorNo+"' ) order by makedate desc fetch first 1 rows only";
			ExeSQL exeSQL = new ExeSQL();
			SSRS tSSRS = exeSQL.execSQL(sql2);
			prtno=tSSRS.GetText(1, 1);
			System.out.println(prtno);
		}
		
		db.setDataBatchNo(dataBatchNo);
		db.setDataSerialNo(dataSerialNo);
		db.setPrtNo(prtno);
		db.setEdorNo(edorNo);
		db.setRequestType(requestType);
		db.setTaskNo(taskNo);
		db.setResponseCode(responseCode);
		db.setErrorMessage(errorMessage);
		db.setEndRefundNo(mEndRefundNo);
		db.setPolicyFormerNo(mPolicyFormerNo);
		
		
		db.setOperator("sys");
		db.setMakeDate(new SimpleDateFormat("yyyy-MM-dd")
				.format(new Date()));
		db.setMakeTime(new SimpleDateFormat("HH:mm:ss")
				.format(new Date()));
		db.setModifyDate(new SimpleDateFormat("yyyy-MM-dd")
				.format(new Date()));
		db.setModifyTime(new SimpleDateFormat("HH:mm:ss")
				.format(new Date()));
		if (!db.insert()) {
			System.out.println("保存返回成功信息失败--" + requestType + "--" + responseCode);
		}
		System.out.println("保存返回成功信息成功--" + requestType + "--" + responseCode);
		//lijia
		//1.是银行卡扣款的bankcode不置为空 2.医保扣款的和医保优先银行卡辅助的要置为空
      PubSubmit tPubSubmit = new PubSubmit();
      String flagSQL = "select 1 from lccontsub where prtno = (select prtno from lccont where contno = '"+edorNo+"') and renemalpaymethod = '3' with ur";
      String flag = new ExeSQL().getOneValue(flagSQL);
      System.out.println(flag);//lijia
      if(!"".equals(flag)){
      	String sql="update ljspay set bankcode = (select bankcode from lccont where contno='"+edorNo+"' and stateflag='1' and appflag='1' and conttype='1'),modifydate = current date,modifytime = current time where otherno = '"+edorNo+"'";
      	System.out.println(sql);
      	MMap tMap = new MMap();
      	VData mInputData = new VData();
      	tMap.put(sql, "UPDATE");
      	mInputData.clear();
      	mInputData.add(tMap);
      	if (!tPubSubmit.submitData(mInputData, "")) {
      		System.out.println("BankCode置值错误!" + tPubSubmit.mErrors.getFirstError());
      	} else {
      		System.out.println("BankCode置值完成!");
      	}
      }
		
//		StringReader tInXmlReader;
//		SAXBuilder tSAXBuilder;
//		Document document;
//		try {
//			tInXmlReader = new StringReader(responseXml);
//			tSAXBuilder = new SAXBuilder();
//			document = tSAXBuilder.build(tInXmlReader);
//			Element rootElement = document.getRootElement();
//
//			Element successInfo = rootElement.getChild("ResponseNodes")
//					.getChild("ResponseNode").getChild("SUCCESS_INFO");
//
//
//
//		} catch (JDOMException e) {
//			throw new RuntimeException(e.getMessage());
//		} catch (Exception e) {
//			throw new RuntimeException(e.getMessage());
//		}
	}

	/**
	 * 
	 * @param edorNo
	 */
	private static void BCPay(String edorNo) {
		String tEdorNo = edorNo;
		ExeSQL exeSQL = new ExeSQL();
		String getPayModeSQL = "select lc.paymode from ljtempfee lp, ljtempfeeclass lc where lp.tempfeeno = lc.tempfeeno " + 
			"and otherNo in(select contno from lpedoritem where edorno = '"+ tEdorNo +"')";
		String payMode = exeSQL.getOneValue(getPayModeSQL);
//		System.out.println("payMode = " + payMode);
		if ("4".equals(payMode)) {
			//置表 ljaget bankcode
			String getActuGetNoSQL = "select ActuGetNo from ljaget where otherno='" + tEdorNo + "' and enteraccdate is null";
			SSRS actuGetNoRes = exeSQL.execSQL(getActuGetNoSQL);
			int maxRow = actuGetNoRes.getMaxRow();
//			System.out.println("maxRow = " + maxRow);
			if (maxRow == 1) {
				String tUpdateSQL = "update ljaget set PayMode = '4', bankcode = (select bankcode from lbcont where edorno = '"+ tEdorNo +"')" + 
					", ModifyDate = current date, ModifyTime = current time where ActuGetNo = '"+ actuGetNoRes.GetText(1, 1) +"'";
				if (!exeSQL.execUpdateSQL(tUpdateSQL)) {
					System.out.println("ljaget中bankcode设置失败,edorNo=" + tEdorNo + ",tUpdateSQL:" + tUpdateSQL); 
				}
			}
		}
	}

	/**
	 * 获取结果集
	 * 
	 * @param args
	 */
	public VData getResult() {
		return mResult;
	}

	/**
	 * 测试用
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
//		VData tVData = new VData();
//		TransferData tTransferData = new TransferData();
		// 0201609010302
		// 0139699670000
		// 162016053101
		// PD0000009013 测试
		//工单号 20141224000003
//		tTransferData.setNameAndValue("PrtNo", "20140326000010");
//		tTransferData.setNameAndValue("PrtNo", "20170119000036");
//		
//
//		// tTransferData.setNameAndValue("PrtNo", "PD0000009013");
//		tVData.add(tTransferData);
//		E03 tUploadClaim = new E03();
//		tUploadClaim.submitData(tVData, "E03");
//
//		System.out.println("******XTUP E03 返回核心******");
		
		BCPay("20170223000009");
		
//		String sql = "select edorno from YBK_E03_LIS_ResponseInfo where edorno = '20170119000036'";
//		String sql = "select ActuGetNo from ljaget where otherno='20170223000009' and enteraccdate is null";
//		ExeSQL exeSQL = new ExeSQL();
//		SSRS execSQL = exeSQL.execSQL(sql);
//		int maxRow = execSQL.getMaxRow();
//		System.out.println(maxRow);
		
	}
}
