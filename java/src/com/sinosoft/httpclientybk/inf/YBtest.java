package com.sinosoft.httpclientybk.inf;


import com.sinosoft.httpclientybk.dto.ybktest.request.Head;
import com.sinosoft.httpclientybk.dto.ybktest.request.RequestNode;
import com.sinosoft.httpclientybk.dto.ybktest.request.YBtestRequest;
import com.sinosoft.httpclientybk.dto.ybktest.response.YBtestResponse;
import com.sinosoft.httpclientybk.util.CommunicateServiceImpl;
import com.sinosoft.httpclientybk.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:  医保卡平台样例</p>
 * <p>Description:医保卡平台样例上传样例</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Sinosoft</p>
 * @author : Liuyc
 * @version 1.0
 */
public class YBtest {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /**用于存储传入的案件号*/
	  private String mPrtNo;

	  /**用于存储核心理赔号*/
	  private String tClaimNo;
	  /**用于存储平台理赔号*/
	  private String mClaimCode;
	  /**用于存储异步理预约码*/
	  private String mSequenceNo;
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  /**
	   * 提供无参的构造方法
	   */
	  public YBtest() {}
	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }

   		if(!dealData()){
			return false;
		}
   		if(!prepareData()){
   			return false;
   		}
		return true;
	}
	/**
	 * 获取传入的方法
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
   		 */
   		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
   		mPrtNo = (String) tempTransferData.getValueByName("PrtNo");
   		System.out.println(mPrtNo);
   		if(mPrtNo==null&&mPrtNo.equals("")){
   			CError tCError = new CError();
   			tCError.moduleName = "ConnectZBXServiceImpl";
   			tCError.functionName = "getMessage";
   			tCError.errorMessage = "传入的印刷号为空！";
   			this.mErrors.addOneError(tCError);
   			return false;
		}
   		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 上传保单号，从中保信获取税优识别码
   		 */
       	YBtestRequest tYBtestRequest = new YBtestRequest();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setPrtNo(mPrtNo);
       	tYBtestRequest.getRequestNodes().getRequestNode().add(tRequestNode);
       	try {
			String responseXml = CommunicateServiceImpl.post(tYBtestRequest,mOperate);
			/**获取客户校验平台的平台客户编码 */
			YBtestResponse tYBtestResponse = (YBtestResponse)XmlParseUtil.xmlToDto(responseXml, YBtestResponse.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tYBtestResponse.getHead().getResponseCode();
			String resultStatus = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("1")){
					System.out.println("系统正常，业务成功处理！");
					/**
					 * 上传 平台案件号 从信息验证平台上获取返回信息
					 */
					tClaimNo = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getBusinessObject().getClaimNo();
					mClaimCode = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getBusinessObject().getClaimCodeP();
					mSequenceNo = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getBusinessObject().getBookingSequenceNo();
					mErrorInfo = "系统正常，业务成功处理!";
					saveErrorList(tYBtestResponse,mErrorInfo);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&(resultStatus.equals("4")||resultStatus.equals("5"))){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tYBtestResponse,mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&(resultStatus.equals("0")||resultStatus.equals("6"))){
					System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYBtestResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("2")){
					System.out.println("系统正常，输入数据部分成功!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据部分成功!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYBtestResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("3")){
					System.out.println("系统正常，输入数据对象已存在!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据对象已存在!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYBtestResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："+resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYBtestResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tYBtestResponse,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
//			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
//				resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
//				/**
//				 * 上传 平台客户号和保单号码 从信息验证平台上获取税优识别码
//				 */
//				mTaxCode = tNBU001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPolicyOfSingleList().getPolicyOfSingle().get(0).getTaxCode();
//				
////				String updLCCont = "update lccont set taxcode = '"+mTaxCode+"' where a.contno '"+mContNo+"'";
////				boolean updLCContF=tExeSQL.execUpdateSQL(updLCCont);
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "正确处理，等待异步返回!";
//				this.mErrors.addOneError(tCError);
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1001")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "安全验证失败!";
//				this.mErrors.addOneError(tCError);
//				return false;
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1002")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "报文为空!";
//				this.mErrors.addOneError(tCError);
//				return false;
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1003")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "报文解析失败!";
//				this.mErrors.addOneError(tCError);
//				return false;
//			}else if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1004")&&
//					resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
//				CError tCError = new CError();
//				tCError.moduleName = "GetTaxCode";
//				tCError.functionName = "getTaxCode";
//				tCError.errorMessage = "系统异常!";
//				this.mErrors.addOneError(tCError);
//				return false;
//			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			CError tCError = new CError();
			tCError.moduleName = "GetTaxCode";
			tCError.functionName = "getTaxCode";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	public boolean prepareData(){
		/**
		 * 将返回信息放入到结果集中
		 */
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ClaimNo", tClaimNo);
		tTransferData.setNameAndValue("ClaimCode", mClaimCode);
		tTransferData.setNameAndValue("SequenceNo", mSequenceNo);
		System.out.println(tClaimNo);
		System.out.println(mClaimCode);
		System.out.println(mSequenceNo);
		mResult.add(tTransferData);
		return true;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(YBtestResponse tYBtestResponse,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_TPH_BATCHNO", 20);
		String TransType = tYBtestResponse.getHead().getTransType();
		String TransNo = tYBtestResponse.getHead().getTransNo();
		String responseCode = tYBtestResponse.getHead().getResponseCode();
		String resultStatus = tYBtestResponse.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"','"+mPrtNo+"','"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
	}
	/**
	 * 获取结果集
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", "162016053101");
		tVData.add(tTransferData);
		YBtest tUploadClaim= new YBtest();
		tUploadClaim.submitData(tVData, "YBKPT");
		
		
	}
}
