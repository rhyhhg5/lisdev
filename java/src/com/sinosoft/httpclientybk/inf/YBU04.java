package com.sinosoft.httpclientybk.inf;

import com.sinosoft.httpclientybk.dto.ybkU04.request.RequestNode;
import com.sinosoft.httpclientybk.dto.ybkU04.request.YBU04Request;
import com.sinosoft.httpclientybk.dto.ybkU04.response.SUCCESS_INFO;
import com.sinosoft.httpclientybk.dto.ybkU04.response.YBU04Response;
import com.sinosoft.httpclientybk.util.CommunicateServiceImpl;
import com.sinosoft.httpclientybk.util.XmlParseUtil;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.YBK_LIS_U04_ResponseResultSchema;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 医保卡平台样例
 * </p>
 * <p>
 * Description:医保卡平台样例上传样例
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author : Liuyc
 * @version 1.0
 */
public class YBU04 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 携带数据的类 */
	private TransferData tempTransferData = new TransferData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 用于存储传入的案件号 */
	private String mPrtNo;
	/**当前日期*/
	private String tMakeDate = PubFun.getCurrentDate();
	/**当前时间*/
	private String tMakeTime = PubFun.getCurrentTime();

	/** 用于存储核心理赔号 */
	private String tClaimNo;
	/** 用于存储平台理赔号 */
	private String mClaimCode;
	/** 用于存储异步理预约码 */
	private String mSequenceNo;
	/** 存储交互错误信息 */
	private String mErrorInfo;

	private String REQUEST_TYPE=null;
	private String TASK_NO=null ;
	private String RESPONSE_CODE=null ;
	private String ERROR_MESSAGE =null;
	
	private String DataBatchNo=null;
	private String DataSerialNo=null ;
	private String PrtNo=null ;
	private String CustomerSequenceNo=null;
	private String pfn=null;
	private String sur=null;
	private String surd=null;
	private static final String surd_cftb01 = "客户已投保该产品，不可重复投保。";
	private static final String surd_cftb02 = "客户已有该产品保单，不可重复投保。";
	
	
	/**
	 * 提供无参的构造方法
	 */
	public YBU04() {
	}

	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		if (!getInputData()) {
			return false;
		}

		dealData();
		
		System.out.println("往集合放数据");
		if (!prepareData()) {
			return false;
		}
		return true;
	}

	/**
	 * 获取传入的方法
	 */
	public boolean getInputData() {
		/**
		 * 接收传入的数据
		 */
		tempTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mPrtNo = (String) tempTransferData.getValueByName("PrtNo");
		System.out.println(mPrtNo);
		if (mPrtNo == null && mPrtNo.equals("")) {
			CError tCError = new CError();
			tCError.moduleName = "ConnectZBXServiceImpl";
			tCError.functionName = "getMessage";
			tCError.errorMessage = "传入的印刷号为空！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public void dealData(){
		/**
   		 * 封装请求报文dto
   		 * 上传保单号，从中保信获取税优识别码
   		 */
       	YBU04Request tYBU04Request = new YBU04Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setPrtNo(mPrtNo);
       	tYBU04Request.getRequestNodes().getRequestNode().add(tRequestNode);
       	try {
			String responseXml = CommunicateServiceImpl.post(tYBU04Request,mOperate);
//       		String responseXml="<?xml version=\"1.0\" encoding=\"gbk\" standalone=\"yes\"?> <XmlYBU04Response>     <Head>         <TransTime>2016-12-17 16:32:28</TransTime>         <TransNo>fcce4fb9-d763-43ba-be3c-ebd2da0dfb58</TransNo>         <TransType>U04</TransType>         <ResponseCode>0000</ResponseCode>     </Head>     <ResponseNodes>         <ResponseNode>             <Result>                 <ResultStatus>0</ResultStatus>                 <ResultInfoDesc>处理流程完成</ResultInfoDesc>                 <DataBatchNo>110000002728</DataBatchNo>                 <DataSerialNo>02</DataSerialNo>                 <PrtNo>YBK0000000108</PrtNo>                 <CUSTOMER_SEQUENCE_NO>CUS201612170021613688F</CUSTOMER_SEQUENCE_NO>                 <REQUEST_TYPE>U04</REQUEST_TYPE>                 <TASK_NO>145427512</TASK_NO>                 <RESPONSE_CODE>0</RESPONSE_CODE>                 <ERROR_MESSAGE>Upload Fail!调用保险公司自核失败！Read timed out</ERROR_MESSAGE>                 <SUCCESS_INFO/>             </Result>             <BusinessObject>                 <PrtNo>YBK0000000108</PrtNo>             </BusinessObject>         </ResponseNode>     </ResponseNodes> </XmlYBU04Response> ";
			/**获取客户校验平台的平台客户编码 */
			YBU04Response tYBU04Response = (YBU04Response)XmlParseUtil.xmlToDto(responseXml, YBU04Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tYBU04Response.getHead().getResponseCode();
			//内部头
			String resultStatus = tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			String resultInfoDesc = tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			//平台返回头
			 REQUEST_TYPE = tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getREQUEST_TYPE();
			 TASK_NO = tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getTASK_NO();
			 RESPONSE_CODE = tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getRESPONSE_CODE();
			 ERROR_MESSAGE = tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getERROR_MESSAGE();
			
			//批次号
			 DataBatchNo=tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getDataBatchNo();
				//流水号
			 DataSerialNo=tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getDataSerialNo();
				
				//印刷号
			 PrtNo=tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getPrtNo();
			 CustomerSequenceNo=tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getCUSTOMER_SEQUENCE_NO();
			 
			 
			 
			if("1".equals(resultStatus)){ //有body体
				SUCCESS_INFO success=tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getSUCCESS_INFO();
				 pfn=success.getPOLICY_FORMER_NO();
				 sur=success.getSELF_UNDERWRITING_RESULT();
				 surd=success.getSELF_UNDERWRITING_RESULT_DES();
			}
			
			//99 可能是前置机提数异常返回的
			if("99".equals(resultStatus)){
				System.out.println(resultInfoDesc);
				mErrorInfo = resultInfoDesc;
				saveErrorList(tYBU04Response,mErrorInfo);
			}else{
				saveYBKLISU04ResponseResult();
			}
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
			if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("1")){
				System.out.println("输入数据正确，返回正常值！");
				mErrorInfo = "输入数据正确，返回正常值!";
				saveErrorList(tYBU04Response,mErrorInfo);
				System.out.println("核心获取返回报文后的处理。。。");
			}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("0")){
				System.out.println("输入数据有误，无法返回正常值!原因为："+resultInfoDesc);	
				CError tCError = new CError();
				tCError.moduleName = "XqPremUpLoad";
				tCError.functionName = "XqPremUpLoad";
				tCError.errorMessage = resultInfoDesc;
				mErrorInfo = tCError.errorMessage;
				saveErrorList(tYBU04Response,mErrorInfo);
				this.mErrors.addOneError(tCError);
			}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("2")){
				System.out.println("输入数据部分成功!");
				CError tCError = new CError();
				tCError.moduleName = "XqPremUpLoad";
				tCError.functionName = "XqPremUpLoad";
				tCError.errorMessage = resultInfoDesc;
				mErrorInfo = tCError.errorMessage;
				saveErrorList(tYBU04Response,mErrorInfo);
				this.mErrors.addOneError(tCError);
			}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("3")){
				System.out.println("输入数据对象已存在!");
				CError tCError = new CError();
				tCError.moduleName = "XqPremUpLoad";
				tCError.functionName = "XqPremUpLoad";
				tCError.errorMessage = resultInfoDesc;
				mErrorInfo = tCError.errorMessage;
				saveErrorList(tYBU04Response,mErrorInfo);
				this.mErrors.addOneError(tCError);
			}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("6")){
				System.out.println("报文数据超过/低于限制!");
				CError tCError = new CError();
				tCError.moduleName = "XqPremUpLoad";
				tCError.functionName = "XqPremUpLoad";
				tCError.errorMessage = resultInfoDesc;
				mErrorInfo = tCError.errorMessage;
				saveErrorList(tYBU04Response,mErrorInfo);
				this.mErrors.addOneError(tCError);
			}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("E")){
				System.out.println("系统未知错误！");
				CError tCError = new CError();
				tCError.moduleName = "XqPremUpLoad";
				tCError.functionName = "XqPremUpLoad";
				tCError.errorMessage = "系统未知错误!";
				mErrorInfo = tCError.errorMessage;
					saveErrorList(tYBU04Response,mErrorInfo);
				this.mErrors.addOneError(tCError);
			}
			}
       	}catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		CError tCError = new CError();
		tCError.moduleName = "GetTaxCode";
		tCError.functionName = "getTaxCode";
		tCError.errorMessage = "处理异常!";
		this.mErrors.addOneError(tCError);
	}
	
	}
	/**
	 * 将返回报文节点值存入表中
	 */
	private void saveYBKLISU04ResponseResult() {
		// TODO Auto-generated method stub
		
		YBK_LIS_U04_ResponseResultSchema ybk_LIS_U04_ResponseResultSchema= new YBK_LIS_U04_ResponseResultSchema();
		try {
		    String  Date = PubFun.getCurrentDate();
		    String  Time = PubFun.getCurrentTime();
			MMap map = new MMap();
			VData tVData1 = new VData();
			// 错误处理类
			CErrors mErrors = new CErrors();
			ybk_LIS_U04_ResponseResultSchema.setDataBatchNo(DataBatchNo);//批次号                       
			ybk_LIS_U04_ResponseResultSchema.setDataSerialNo(DataSerialNo);//流水号                     
			ybk_LIS_U04_ResponseResultSchema.setPrtNo(PrtNo);//印刷号                                   
			ybk_LIS_U04_ResponseResultSchema.setPolicySequenceNo(pfn);//投保流水号                      
			ybk_LIS_U04_ResponseResultSchema.setCustomerSequenceNo(CustomerSequenceNo);//客户编码       
			ybk_LIS_U04_ResponseResultSchema.setRequestType(REQUEST_TYPE);//请求类型                    
			ybk_LIS_U04_ResponseResultSchema.setTaskNo(TASK_NO);//任务编号                              
			ybk_LIS_U04_ResponseResultSchema.setResponseCode(RESPONSE_CODE); //响应编码                 
			ybk_LIS_U04_ResponseResultSchema.setResultMessage(ERROR_MESSAGE);//错误描述                 
			ybk_LIS_U04_ResponseResultSchema.setMessage(sur);//自核结论                                 
			ybk_LIS_U04_ResponseResultSchema.setDealResullt(surd);//自核结论描述                        
			ybk_LIS_U04_ResponseResultSchema.setOtherSign("");//其他标识     
			ybk_LIS_U04_ResponseResultSchema.setMakeDate(Date);                                         
			ybk_LIS_U04_ResponseResultSchema.setMakeTime(Time);                                         
			ybk_LIS_U04_ResponseResultSchema.setModifyDate(Date);                                       
			ybk_LIS_U04_ResponseResultSchema.setModifyTime(Time);                                       
			map.put(ybk_LIS_U04_ResponseResultSchema, "DELETE&INSERT");
			// 入库
			PubSubmit tPubSubmit = new PubSubmit();
			tVData1.add(map);
			if (!tPubSubmit.submitData(tVData1, "INSERT")) {
				// @@错误处理
				mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "LoginVerifyTool";
				tError.functionName = "writeLog";
				tError.errorMessage = "数据提交失败!insert——>YBK_LIS_U04_ResponseResult";
				mErrors.addOneError(tError);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public boolean prepareData() {
		/**
		 * 将返回信息放入到结果集中
		 */
		if("".equals(REQUEST_TYPE)||REQUEST_TYPE==null){
			return false;
		}
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("REQUEST_TYPE", REQUEST_TYPE);
		tTransferData.setNameAndValue("TASK_NO", TASK_NO);
		tTransferData.setNameAndValue("RESPONSE_CODE", RESPONSE_CODE);
		tTransferData.setNameAndValue("ERROR_MESSAGE", ERROR_MESSAGE);
		if("1".equals(RESPONSE_CODE)){
			tTransferData.setNameAndValue("POLICY_FORMER_NO", pfn);
			tTransferData.setNameAndValue("SELF_UNDERWRITING_RESULT", sur);
			tTransferData.setNameAndValue("SELF_UNDERWRITING_RESULT_DES", surd);
		}
		System.out.println(REQUEST_TYPE);
		System.out.println(TASK_NO);
		System.out.println(RESPONSE_CODE);
		System.out.println(ERROR_MESSAGE);
		System.out.println(pfn);
		System.out.println(sur);
		System.out.println(surd);
		mResult.add(tTransferData);
		return true;
	}

	/**
	 * 将核心与平台交互返回的信息进行存储
	 * 
	 * @param args
	 */
	public void saveErrorList(YBU04Response tYBU04Response, String mErrorInfo) {
		String ID = PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20);
		String TransType = tYBU04Response.getHead().getTransType();
		String TransNo = tYBU04Response.getHead().getTransNo();
		String responseCode = tYBU04Response.getHead().getResponseCode();
		
		
		
		String resultStatus = tYBU04Response.getResponseNodes()
				.getResponseNode().get(0).getResult().getResultStatus();
		///投保流水号
		String YbkSerialno = tYBU04Response.getResponseNodes().getResponseNode().get(0).getResult().getSUCCESS_INFO().getPOLICY_FORMER_NO();
		//印刷号
		String PrtNo = tYBU04Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPrtNo();
		
		String insertSQL = "insert into ybkerrorlist values ('" + ID + "','"
				+ TransType + "','" + TransNo + "','" + mPrtNo + "','"
				+ responseCode + "','" + resultStatus + "','" + mErrorInfo
				+ "','"+tMakeDate+"','"+tMakeTime+"','"+tMakeDate+"','"+tMakeTime+"')";
		//String ins="update lccontsub set YbkSerialno='"+YbkSerialno+"' where 1=1 and prtno='"+PrtNo+"' ";
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(insertSQL)) {
			System.out.println("保存返回信息失败--" + TransType + "--" + resultStatus);
		}else {
			System.out.println("保存返回信息成功--" + TransType + "--" + resultStatus);
			
			  if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("1")){								
				if(surd!=null&&!surd.equals("")&&(surd.equals(surd_cftb01)||surd.equals(surd_cftb02))){					
					GlobalInput tG = new GlobalInput();
					TransferData tTransferData = new TransferData();				
					String tDeleteReason = surd;
										
					LCContDB tLCContDB = new LCContDB();
					LCContSet tLCContSet = new LCContSet();
					LCContSchema tLCContSchema = new LCContSchema();
					if(PrtNo != null && !PrtNo.equals("")){
						tLCContDB.setPrtNo(PrtNo);
						tLCContSet = tLCContDB.query();
					}
					
					if(tLCContSet != null && tLCContSet.size() > 0){
						tLCContSchema = tLCContSet.get(1);					
						tG.Operator = "YBK";
						tG.ManageCom = tLCContSchema.getManageCom();
						tTransferData.setNameAndValue("DeleteReason",tDeleteReason);
						try {
							VData tVData = new VData();
							tVData.add(tLCContSchema);
							tVData.add(tTransferData);
							tVData.add(tG);
							
							ContDeleteUI tContDeleteUI = new ContDeleteUI();
							if(tContDeleteUI.submitData(tVData, "DELETE")){
								System.out.println("实时删单成功，删单原因："+surd);
							} else {
								this.mErrors.copyAllErrors(tContDeleteUI.mErrors);
								System.out.println("实时删单失败："+this.mErrors.getFirstError()+"印刷号："+PrtNo);
							}
						} catch (Exception e) {
							System.out.println("U04实时删单异常");
							e.printStackTrace();
						}
					}					
				} 
				/*else {
					if(!tExeSQL.execUpdateSQL(ins)){
						System.out.println("保存投保流水号失败--" + TransType + "--" + resultStatus);
					} else {
						System.out.println("保存投保流水号成功--" + TransType + "--" + resultStatus);
					}
				}*/
			}			
			
		}
     }

	/**
	 * 获取结果集
	 * 
	 * @param args
	 */
	public VData getResult() {
		return mResult;
	}

	/**
	 * 测试用
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", "YBK0000000108");
		tVData.add(tTransferData);
		YBU04 tUploadClaim = new YBU04();
		tUploadClaim.submitData(tVData, "YBKPT");

	}
}
