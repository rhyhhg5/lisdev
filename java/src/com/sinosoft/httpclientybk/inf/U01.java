package com.sinosoft.httpclientybk.inf;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.sinosoft.httpclientybk.dto.u01.request.HBRequest;
import com.sinosoft.httpclientybk.dto.u01.request.RequestNode;
import com.sinosoft.httpclientybk.dto.u01.response.HBResponse;
import com.sinosoft.httpclientybk.util.CommunicateServiceImpl;
import com.sinosoft.httpclientybk.util.XmlParseUtil;
import com.sinosoft.lis.db.YBK_U01_ResultsDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 医保卡平台样例
 * </p>
 * <p>
 * Description:医保卡平台样例上传样例
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author : Liuyc
 * @version 1.0
 */
public class U01 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 携带数据的类 */
	private TransferData tempTransferData = new TransferData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 用于存储传入的案件号 */
	private String mPrtNo;

	/** 用于存储核心理赔号 */
	private String tClaimNo;
	/** 用于存储平台理赔号 */
	private String mClaimCode;
	/** 用于存储异步理预约码 */
	private String mSequenceNo;
	/** 存储交互错误信息 */
	private String mErrorInfo;
	/**当前日期*/
	private String tMakeDate = PubFun.getCurrentDate();
	/**当前时间*/
	private String tMakeTime = PubFun.getCurrentTime();

	private List customerList = null;
	private List riskInfoList = null;
	private Element tRiskList;
	private Element customerInfoElement;
	private Element riskInfoElement;
	String mcutomerSequenceNo = "";
	String mUnderwritSequenceNo = "";
	private String startTime;
	private String endTime;
	private String allLifeSumInsured;
	private String allAccidentSumInsured;
	private String allHealthSumInsured;
	private String allOldSumInsured;
	private String allOtherSumInsured;
	private String allHospitalAllowance;
	private String allClaimAmount;
	private String dataBatchNo;
	private String dataSerialNo;
	private String prtNo;
	private String requestType;
	private String taskNo;
	private String responseCode;
	private String errorMessage;
	private YBK_U01_ResultsDB db;

	/**
	 * 提供无参的构造方法
	 */
	public U01() {
	}

	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		if (!getInputData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		if (!prepareData()) {
			return false;
		}
		return true;
	}

	/**
	 * 获取传入的方法
	 */
	public boolean getInputData() {
		/**
		 * 接收传入的数据
		 */
		tempTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mPrtNo = (String) tempTransferData.getValueByName("PrtNo");
		System.out.println(mPrtNo);
		if (mPrtNo == null && mPrtNo.equals("")) {
			CError tCError = new CError();
			tCError.moduleName = "ConnectZBXServiceImpl";
			tCError.functionName = "getMessage";
			tCError.errorMessage = "传入的印刷号为空！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData() {
		/**
		 * 封装请求报文dto 上传保单号，从中保信获取税优识别码
		 */
		HBRequest tHBRequest = new HBRequest();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setPrtNo(mPrtNo);
		tHBRequest.getRequestNodes().getRequestNode().add(tRequestNode);
		try {
			String responseXml = CommunicateServiceImpl.post(tHBRequest,
					mOperate);
			/** 获取客户校验平台的平台客户编码 */
			HBResponse tHBResponse = (HBResponse) XmlParseUtil.xmlToDto(
					responseXml, HBResponse.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tHBResponse.getHead().getResponseCode();
			String resultStatus = tHBResponse.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tHBResponse.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultInfoDesc();

			if (responseCode != null && !responseCode.equals("")
					&& responseCode.equals("0000")) {
				if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("1")) {
					System.out.println("系统正常，业务成功处理！");
					mErrorInfo = "系统正常，业务成功处理!";
					saveErrorList(tHBResponse, mErrorInfo);
					saveReturnSuccInfo(tHBResponse, mErrorInfo, responseXml);
				} else if (resultStatus != null
						&& !resultStatus.equals("")
						&& (resultStatus.equals("4") || resultStatus
								.equals("5"))) {
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tHBResponse, mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				} else if (resultStatus != null
						&& !resultStatus.equals("")
						&& (resultStatus.equals("0") || resultStatus
								.equals("6"))) {
					System.out.println("系统正常，数据校验不通过!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为：" + resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tHBResponse, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("2")) {
					System.out.println("系统正常，输入数据部分成功!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据部分成功!原因为："
							+ resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tHBResponse, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("3")) {
					System.out.println("系统正常，输入数据对象已存在!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，输入数据对象已存在!原因为："
							+ resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tHBResponse, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("03")) {
					System.out.println("系统正常，业务处理失败!原因为：" + resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统正常，业务处理失败!原因为：" + resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tHBResponse, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("99")) {
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetTaxCode";
					tCError.functionName = "getTaxCode";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tHBResponse, mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
			// /**
			// * 上传 平台客户号和保单号码 从信息验证平台上获取税优识别码
			// */
			// mTaxCode =
			// tNBU001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject().getPolicyOfSingleList().getPolicyOfSingle().get(0).getTaxCode();
			//
			// // String updLCCont =
			// "update lccont set taxcode = '"+mTaxCode+"' where a.contno '"+mContNo+"'";
			// // boolean updLCContF=tExeSQL.execUpdateSQL(updLCCont);
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "正确处理，等待异步返回!";
			// this.mErrors.addOneError(tCError);
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1001")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "安全验证失败!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1002")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "报文为空!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1003")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "报文解析失败!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }else
			// if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("1004")&&
			// resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
			// CError tCError = new CError();
			// tCError.moduleName = "GetTaxCode";
			// tCError.functionName = "getTaxCode";
			// tCError.errorMessage = "系统异常!";
			// this.mErrors.addOneError(tCError);
			// return false;
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			CError tCError = new CError();
			e.printStackTrace();
			tCError.moduleName = "GetTaxCode";
			tCError.functionName = "getTaxCode";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	public static String generateRandomStr(int len) {
		String generateSource = "0123456789abcdefghigklmnopqrstuvwxyz";
		String rtnStr = "";
		for (int i = 0; i < len; i++) {
			String nowStr = String.valueOf(generateSource.charAt((int) Math
					.floor(Math.random() * generateSource.length())));
			rtnStr += nowStr;
			generateSource = generateSource.replaceAll(nowStr, "");
		}
		return rtnStr;
	}
	
	public boolean prepareData() {
		/**
		 * 将返回信息放入到结果集中
		 */
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ClaimNo", tClaimNo);
		tTransferData.setNameAndValue("ClaimCode", mClaimCode);
		tTransferData.setNameAndValue("SequenceNo", mSequenceNo);
		System.out.println(tClaimNo);
		System.out.println(mClaimCode);
		System.out.println(mSequenceNo);
		mResult.add(tTransferData);
		return true;
	}

	/**
	 * 将核心与平台交互返回的信息进行存储
	 * 
	 * @param args
	 */
	public void saveErrorList(HBResponse tHBResponse, String mErrorInfo) {
		String ID = PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20);
		String TransType = tHBResponse.getHead().getTransType();
		String TransNo = tHBResponse.getHead().getTransNo();
		String responseCode = tHBResponse.getHead().getResponseCode();
		String resultStatus = tHBResponse.getResponseNodes().getResponseNode()
				.get(0).getResult().getResultStatus();
		String insertSQL = "insert into YBKErrorList values ('" + ID + "','"
				+ TransType + "','" + TransNo + "','" + mPrtNo + "','"
				+ responseCode + "','" + resultStatus + "','" + mErrorInfo
				+ "','"+tMakeDate+"','"+tMakeTime+"','"+tMakeDate+"','"+tMakeTime+"')";
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(insertSQL)) {
			System.out.println("保存返回信息失败--" + TransType + "--" + resultStatus);
		}
		System.out.println("保存返回信息成功--" + TransType + "--" + resultStatus);
	}

	/**
	 * 存储平台返回的成功信息
	 * 
	 * @param tHBResponse
	 * @param mErrorInfo2
	 * @throws JDOMException
	 * @throws ParseException
	 */
	private void saveReturnSuccInfo(HBResponse tHBResponse, String mErrorInfo,
			String responseXml){
		db = new YBK_U01_ResultsDB();
		dataBatchNo = tHBResponse.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getDataBatchNo();
		prtNo = tHBResponse.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getPrtNo();
		requestType = tHBResponse.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getRequestType();
		taskNo = tHBResponse.getResponseNodes().getResponseNode().get(0)
				.getBusinessObject().getTaskNo();
		responseCode = tHBResponse.getResponseNodes().getResponseNode().get(0)
				.getResult().getResultStatus();
		errorMessage = tHBResponse.getResponseNodes().getResponseNode().get(0)
				.getResult().getResultInfoDesc();
		mUnderwritSequenceNo = tHBResponse.getResponseNodes().getResponseNode()
				.get(0).getSuccessInfo().getUnderwrit_sequence_no();

		StringReader tInXmlReader;
		SAXBuilder tSAXBuilder;
		Document document;
		try {
			tInXmlReader = new StringReader(responseXml);
			tSAXBuilder = new SAXBuilder();
			document = tSAXBuilder.build(tInXmlReader);
			Element rootElement = document.getRootElement();

			Element insuredList = rootElement.getChild("ResponseNodes")
					.getChild("ResponseNode").getChild("SUCCESS_INFO")
					.getChild("INSURED_LIST");

			customerList = insuredList.getChildren("CUSTOMER_INFO");
			riskInfoList = null;

			for (Iterator iter = customerList.iterator(); iter.hasNext();) {
				customerInfoElement = (Element) iter.next();
				/** 客户编码 */
				mcutomerSequenceNo = customerInfoElement
						.getChildTextTrim("CUTOMER_SEQUENCE_NO");

				tRiskList = customerInfoElement.getChild("RISK_LIST");
				riskInfoList = tRiskList.getChildren("RISK_INFO");

				for (Iterator iter2 = riskInfoList.iterator(); iter2.hasNext();) {
					riskInfoElement = (Element) iter2.next();

//					dataSerialNo = generateRandomStr(new Random().nextInt(10));
					dataSerialNo = PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20);
					startTime = riskInfoElement.getChildTextTrim("START_TIME");
					endTime = riskInfoElement.getChildTextTrim("END_TIME");
					allLifeSumInsured = riskInfoElement
							.getChildTextTrim("ALL_LIFE_SUM_INSURED");
					allAccidentSumInsured = riskInfoElement
							.getChildTextTrim("ALL_ACCIDENT_SUM_INSURED");
					allHealthSumInsured = riskInfoElement
							.getChildTextTrim("ALL_HEALTH_SUM_INSURED");
					allOldSumInsured = riskInfoElement
							.getChildTextTrim("ALL_OLD_SUM_INSURED");
					allOtherSumInsured = riskInfoElement
							.getChildTextTrim("ALL_OTHER_SUM_INSURED");
					allHospitalAllowance = riskInfoElement
							.getChildTextTrim("ALL_HOSPITAL_ALLOWANCE");
					allClaimAmount = riskInfoElement
							.getChildTextTrim("ALL_CLAIM_AMOUNT");

					db.setDataBatchNo(dataBatchNo);
					db.setDataSerialNo(dataSerialNo);
					db.setPrtNo(prtNo);
					db.setRequestType(requestType);
					db.setTaskNo(taskNo);
					db.setResponseCode(responseCode);
					db.setErrorMessage(errorMessage);
					db.setUnderwritSequenceNo(mUnderwritSequenceNo);
					db.setCutomerSequenceNo(mcutomerSequenceNo);
					db.setStartTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(startTime));
					db.setEndTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(endTime));
					db.setAllLifeSumInsured(Double
							.parseDouble(allLifeSumInsured));
					db.setAllAccidentSumInsured(Double
							.parseDouble(allAccidentSumInsured));
					db.setAllHealthSumInsured(Double
							.parseDouble(allHealthSumInsured));
					db.setAllOldSumInsured(Double.parseDouble(allOldSumInsured));
					db.setAllOtherSumInsured(Double
							.parseDouble(allOtherSumInsured));
					db.setAllHospitalAllowance(Double
							.parseDouble(allHospitalAllowance));
					db.setAllClaimAmount(Double.parseDouble(allClaimAmount));
					db.setOperator("sys");
					db.setMakeDate(new SimpleDateFormat("yyyy-MM-dd")
							.format(new Date()));
					db.setMakeTime(new SimpleDateFormat("HHmmss")
							.format(new Date()));
					db.setModifyDate(new SimpleDateFormat("yyyy-MM-dd")
							.format(new Date()));
					db.setModifyTime(new SimpleDateFormat("HHmmss")
							.format(new Date()));
					db.insert();
				}
			}

		} catch (JDOMException e) {
			throw new RuntimeException(e.getMessage());
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * 获取结果集
	 * 
	 * @param args
	 */
	public VData getResult() {
		return mResult;
	}

	/**
	 * 测试用
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		// 0201609010302
		// 0139699670000
		// 162016053101
		// PD0000009013 测试	YWX0000000229	YWX0000000230
		tTransferData.setNameAndValue("PrtNo", "YWX0000000231");

		// tTransferData.setNameAndValue("PrtNo", "PD0000009013");
		tVData.add(tTransferData);
		U01 tUploadClaim = new U01();
		tUploadClaim.submitData(tVData, "U01");

		System.out.println("******HBU01返回核心******");
	
	}
}
