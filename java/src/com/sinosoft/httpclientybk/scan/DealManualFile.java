package com.sinosoft.httpclientybk.scan;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;

import com.sinosoft.httpclientybk.util.AnimatedGifEncoder;
import com.sinosoft.httpclientybk.util.IndexMap;
import com.sinosoft.httpclientybk.util.YbkDeCompress;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.TIFFEncodeParam;

public class DealManualFile {

	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mPolicyFormerNo = "";

	private String Prtno = "";

	private TransferData mTransferData;

	private String mLocalPath;

	private String mFileName;

	private String operator = "ybk";

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private VData mResult = new VData();

	private static final long serialVersionUID = 1L;

	private ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();

	private ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();

	private ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();

	private MMap map = new MMap();

	private IndexMap iMap = new IndexMap();

	private String HostName = "";

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		try {
			if (!deal(mLocalPath, mFileName)) {
				return false;
			}
		} catch (Exception e) {
			System.out.println("DeallManualFile执行==提交失败");
			return false;
		}

		return true;
	}

	private boolean getInputData(VData inputData) {
		mGI = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) inputData.getObjectByObjectName(
				"TransferData", 0);
		mPolicyFormerNo = (String) mTransferData
				.getValueByName("PolicyFormerNo");
		mLocalPath = (String) mTransferData.getValueByName("LocalPath");
		mFileName = (String) mTransferData.getValueByName("FileName");
		System.out.println("上传扫描件印刷号为：" + mPolicyFormerNo);
		return true;
	}

	private boolean deal(String path, String file) throws Exception {

		System.out.println("path:" + path + " ,file:" + file + ",prtno:"
				+ mPolicyFormerNo);
		String dir = path + "/" + file;// 原来路径

		boolean flag = true;

		// 1、解压zip包
		String unRar = "select codename,codealias from ldcode where codetype='ybkscan' and code='UnRarPath'";
		SSRS tPathSSRS = new ExeSQL().execSQL(unRar);
		if (tPathSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "ManualGetScanFileBL";
			tError.functionName = "deal";
			tError.errorMessage = "未设置文件解压路径";
			mErrors.addOneError(tError);
			return false;
		}
		String unpath = tPathSSRS.GetText(1, 1);
		unpath = unpath + mCurrentDate.replaceAll("-", "") + "/"
				+ mPolicyFormerNo;
		HostName = tPathSSRS.GetText(1, 2);
		// =================本地测试使用=====================
/*		File unFile = new File(unpath.toString());
		if (unFile.exists()&&unFile.list().length > 0)
			deletefile(unpath.toString());*/
		// ===============================================
		if (!newFolder(unpath)) {
			mErrors.addOneError("新建保存扫描件目录失败");
			return false;
		}
		flag = unZip(dir, unpath + "/");
		if (!flag) {
			CError tError = new CError();
			tError.moduleName = "DealManualFile";
			tError.functionName = "deal";
			tError.errorMessage = "文件解压失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("**************** ③解压zip包完毕 ****************");

		// 2、解析xml文件 生成扫描一套表

		if (!parseXml(mPolicyFormerNo, unpath)) {
			deletefile(unpath);
			deleteFiles(path, file);
			return false;
		}
		String tPolicyFormerNo = (String) iMap.get("POLICY_FORMER_NO");
		String tManageCom = (String) iMap.get("MANAGECOM");
		if (tPolicyFormerNo.equals("")
				|| !tPolicyFormerNo.equals(mPolicyFormerNo)) {
			CError tError = new CError();
			tError.moduleName = "DealManualFile";
			tError.functionName = "deal";
			tError.errorMessage = "解析xml文件失败 或 扫描件与投保单号不匹配";
			this.mErrors.addOneError(tError);
			deletefile(unpath);
			deleteFiles(path, file);
			return false;
		}
		System.out.println("**************** ④解析xml完毕 ****************");

/*		// 3.1 将jpg图片转换为tif 文件 删除jpg文件
		flag = convertImage(unpath);
		if (!flag) {
			CError tError = new CError();
			tError.moduleName = "DealManualFile";
			tError.functionName = "deal";
			tError.errorMessage = "图片转换失败";
			this.mErrors.addOneError(tError);
			deletefile(unpath);
			deleteFiles(path, file);
			return false;
		}
		System.out.println("**************** ⑤图片转换完毕 ****************");*/

		// 4、处理扫描一套表
		flag = sendtoCore(Prtno, tManageCom);
		if (!flag) {
			CError tError = new CError();
			tError.moduleName = "DealManualFile";
			tError.functionName = "deal";
			tError.errorMessage = "扫描数据上传核心错误";
			this.mErrors.addOneError(tError);
			deletefile(unpath);
			deleteFiles(path, file);
			return false;
		}
		System.out.println("**************** ⑥保单核心处理完毕 ****************");
		return true;

	}

	/**
	 * 解压zip包
	 */
	public boolean unZip(String file, String unpath) {
		YbkDeCompress dec = new YbkDeCompress();
		if (!dec.DeCompress(file, unpath)) {
			System.out.println("YbkDeCompress解压失败");
			return false;
		}
		return true;
	}

	/**
	 * 解析xml报文
	 * 
	 * @throws IOException
	 */
	public boolean parseXml(String mPrtno, String unpath) throws IOException {
		String tLccont = "select a.Agentcode,a.Managecom,a.Prtno from lccont a,lccontsub b where a.Prtno= b.Prtno and b.Ybkserialno = '"
				+ mPrtno + "'";
		SSRS tLcSSRS = new ExeSQL().execSQL(tLccont);
		String agentcode = tLcSSRS.GetText(1, 1);
		String managecom = tLcSSRS.GetText(1, 2);
		Prtno = tLcSSRS.GetText(1, 3);
		mES_DOC_MAINSchema.setDocCode(Prtno);
		mES_DOC_MAINSchema.setInputStartDate(mCurrentDate);
		mES_DOC_MAINSchema.setInputEndDate(mCurrentDate);
		mES_DOC_MAINSchema.setSubType("TB30");
		mES_DOC_MAINSchema.setBussType("TB");
		mES_DOC_MAINSchema.setManageCom(managecom);
		mES_DOC_MAINSchema.setVersion("01");
		mES_DOC_MAINSchema.setScanNo("0");
		mES_DOC_MAINSchema.setState("01");
		mES_DOC_MAINSchema.setDocFlag("1");
		mES_DOC_MAINSchema.setOperator(operator);
		mES_DOC_MAINSchema.setScanOperator(operator);
		mES_DOC_MAINSchema.setArchiveNo(createArchiveNo(managecom));

		iMap.put("AGENTCODE", agentcode);
		iMap.put("MANAGECOM", managecom);
		iMap.put("DATE", mCurrentDate);
		iMap.put("POLICY_FORMER_NO", mPrtno);
		List pageList = new ArrayList();
		IndexMap pageImap = null;
		File file1 = new File(unpath);
		String[] pages = file1.list();

		String pithpath = "select codename,codealias from ldcode where codetype='ybkscan' and code='ServerP/Picpath'";
		String aPicPathFTP = new ExeSQL().execSQL(pithpath).GetText(1, 1)
				+ "/ybkunrar/" + mCurrentDate.replaceAll("-", "") + "/"
				+ mPolicyFormerNo + "/";
		String aPicPath = new ExeSQL().execSQL(pithpath).GetText(1, 2)
				+ "ybkunrar/" + mCurrentDate.replaceAll("-", "") + "/"
				+ mPolicyFormerNo + "/";

		if (pages != null) {
			int j = 0;
			for (int i = 0; i < pages.length; i++) {
				ES_DOC_PAGESSchema tES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
				pageImap = new IndexMap();
				String page = pages[i].trim();
//				BufferedImage bi = ImageIO.read(new File(page));
//				if (bi != null) {
					pageImap.put("PAGE", page);
					pageList.add(pageImap);
					tES_DOC_PAGESSchema.setHostName(HostName);
					tES_DOC_PAGESSchema.setPageCode(++j);
					tES_DOC_PAGESSchema.setPageName(page.substring(0, page
							.lastIndexOf('.')));
					tES_DOC_PAGESSchema.setPageSuffix(page.substring(page.lastIndexOf('.')));
					tES_DOC_PAGESSchema.setPageFlag("1");
					tES_DOC_PAGESSchema.setPageType("0");
					tES_DOC_PAGESSchema.setPicPath(aPicPath);
					tES_DOC_PAGESSchema.setPicPathFTP(aPicPathFTP);
					tES_DOC_PAGESSchema.setManageCom(managecom);
					tES_DOC_PAGESSchema.setOperator(operator);
					mES_DOC_PAGESSet.add(tES_DOC_PAGESSchema);
//				}

			}
			mES_DOC_MAINSchema.setNumPages(j);
		}
		iMap.put("pageList", pageList);
		return true;
	}

	public boolean sendtoCore(String prtno, String manageCom) {

		String strDocID = getMaxNo("DocID");
		mES_DOC_MAINSchema.setDocID(strDocID);
		mES_DOC_MAINSchema.setMakeDate(mCurrentDate);
		mES_DOC_MAINSchema.setModifyDate(mCurrentDate);
		mES_DOC_MAINSchema.setMakeTime(mCurrentTime);
		mES_DOC_MAINSchema.setModifyTime(mCurrentTime);

		for (int i = 0; i < mES_DOC_PAGESSet.size(); i++) {
			String strPageID = getMaxNo("PageID");
			mES_DOC_PAGESSet.get(i + 1).setPageID(strPageID);
			mES_DOC_PAGESSet.get(i + 1).setDocID(strDocID);
			mES_DOC_PAGESSet.get(i + 1).setMakeDate(mCurrentDate);
			mES_DOC_PAGESSet.get(i + 1).setModifyDate(mCurrentDate);
			mES_DOC_PAGESSet.get(i + 1).setMakeTime(mCurrentTime);
			mES_DOC_PAGESSet.get(i + 1).setModifyTime(mCurrentTime);
		}
		mES_DOC_RELATIONSchema.setBussNo(prtno);
		mES_DOC_RELATIONSchema.setDocCode(prtno);
		mES_DOC_RELATIONSchema.setBussNoType("11");
		mES_DOC_RELATIONSchema.setDocID(strDocID);
		mES_DOC_RELATIONSchema.setBussType(mES_DOC_MAINSchema.getBussType());
		mES_DOC_RELATIONSchema.setSubType(mES_DOC_MAINSchema.getSubType());
		mES_DOC_RELATIONSchema.setRelaFlag("0");

		map.put(mES_DOC_RELATIONSchema, "INSERT");
		map.put(mES_DOC_MAINSchema, "INSERT");
		map.put(mES_DOC_PAGESSet, "INSERT");
		this.mResult.add(map);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(this.mResult, "INSERT")) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}

		return true;
	}

	public boolean convertImage(String path) {
		System.out.println(PubFun.getCurrentTime3());
		try {
			File root = new File(path);
			File[] tfile = root.listFiles();
			for (int i = 0; i < tfile.length; i++) {
				File a = tfile[i];
				if (a.getName().endsWith(".jpg")) {
					// 转换成tif
					String input2 = a.toString();
					String output2 = path
							+ "/"
							+ a.getName().substring(0,
									a.getName().lastIndexOf(".")) + ".tif";

					RenderedOp src2 = JAI.create("fileload", input2);
					OutputStream os2 = new FileOutputStream(output2);

					TIFFEncodeParam param3 = new TIFFEncodeParam();

					param3
							.setCompression(TIFFEncodeParam.COMPRESSION_JPEG_TTN2);

					ImageEncoder enc2 = ImageCodec.createImageEncoder("TIFF",
							os2, param3);

					enc2.encode(src2);
					os2.flush();
					os2.close();
					// 转换成gif
					String input3 = a.toString();
					String output3 = path
							+ "/"
							+ a.getName().substring(0,
									a.getName().lastIndexOf(".")) + ".gif";

					File PNFFile = new File(input3);
					BufferedImage src1 = ImageIO.read(PNFFile);
					AnimatedGifEncoder e = new AnimatedGifEncoder();
					e.setRepeat(0);
					e.start(output3);
					e.setDelay(100);
					e.addFrame(src1);
					e.finish();
					// ImageIO.write(src1, "gif", new File(output3));
				}
			}
			System.out.println(PubFun.getCurrentTime3());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	// 删除指定文件
	/*
	 * 各处理过程操作失败都需要删除本地缓存文件
	 */
	public boolean deleteFiles(String path, String file) {

		String folder = path + "/" + file;

		File zipFile = new File(folder);

		if (zipFile.exists()) {
			if (!zipFile.delete()) {
				System.out.println("删除本地缓存文件失败！");
				return false;
			}
		}

		return true;
	}

	// 删除目录下所有文件
	public boolean deletefile(String delpath) throws Exception {
		try {

			File file = new File(delpath);
			// 当且仅当此抽象路径名表示的文件存在且 是一个目录时，返回 true
			if (!file.isDirectory()) {
				file.delete();
			} else if (file.isDirectory()) {
				String[] filelist = file.list();
				for (int i = 0; i < filelist.length; i++) {
					File delfile = new File(delpath + "/" + filelist[i]);
					if (!delfile.isDirectory()) {
						delfile.delete();
						System.out
								.println(delfile.getAbsolutePath() + "删除文件成功");
					} else if (delfile.isDirectory()) {
						deletefile(delpath + "/" + filelist[i]);
					}
				}
				System.out.println(file.getAbsolutePath() + "删除成功");
				file.delete();
			}

		} catch (FileNotFoundException e) {
			System.out.println("deletefile() Exception:" + e.getMessage());
		}
		return true;
	}

	// 生成流水号，包含错误处理
	private String getMaxNo(String cNoType) {
		String strNo = PubFun1.CreateMaxNo(cNoType, 1);

		if (strNo.equals("") || strNo.equals("0")) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UploadPrepareBL";
			tError.functionName = "getReturnData";
			tError.errorNo = "-90";
			tError.errorMessage = "生成流水号失败!";
			this.mErrors.addOneError(tError);
			strNo = "";
		}
		return strNo;
	}

	public static boolean newFolder(String folderPath) {
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try {
			if (myFilePath.isDirectory()) {
				System.out.println("目录已存在");
				return true;
			} else {
				myFilePath.mkdirs();
				System.out.println("新建目录成功");
				return true;
			}
		} catch (Exception e) {
			System.out.println("新建目录失败");
			e.printStackTrace();
			return false;
		}
	}

	public String createArchiveNo(String tManageCom) {
		String tDate = PubFun.getCurrentDate();
		tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
				+ tDate.substring(8, 10);

		String comPart = "";
		int length = tManageCom.length();
		if (length == 2) {
			CError tError = new CError();
			tError.moduleName = "DocTBGrpWorkFlowService";
			tError.functionName = "createGrpArchiveNo";
			tError.errorMessage = "两位机构代码不能进行投保单扫描";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return null;
		} else if (length == 4) {
			comPart = tManageCom.substring(2, 4) + "00";
		} else {
			comPart = tManageCom.substring(2, 6);
		}

		String tComtop = "G" + comPart + tDate;
		String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
		return tArchiveNo;
	}

	public static void main(String[] args) {
		DealManualFile a = new DealManualFile();
		String path = "E:\\padscan\\PD00000001653";

		String file = "E:\\ybkscan\\test.zip";
		try {
			// a.convertImage(path);
			// 解压zip文件
			// a.unZip(file, path);
			a.deletefile("C:/Users/admin/Documents/Tencent Files/806992733/FileRecv/201612010");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}