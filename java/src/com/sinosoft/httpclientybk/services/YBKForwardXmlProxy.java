package com.sinosoft.httpclientybk.services;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.httpclientybk.xml.ctrl.YBKForwardXmlApi;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.yibaotong.JdomUtil;

public class YBKForwardXmlProxy {
	
	   private final static Logger mLogger = Logger.getLogger(YBKForwardXmlProxy.class);
	    public YBKForwardXmlProxy()
	    {
	        mLogger.info("init:" + YBKForwardXmlProxy.class.toString());
	    }

	    public String service(String cInXmlStr)
	    {
	        mLogger.info("Start Service");
	        System.out.println("===YBKForwardXmlProxy===进入时间："+PubFun.getCurrentTime());

	        // 记录服务启动时间
	        long mStartMillis = System.currentTimeMillis();
	        mLogger.debug(cInXmlStr);
	        // --------------------

	        String mOutXmlStr = null;
	        try
	        {
	            // 解析报文字符串流
	            StringReader tInXmlReader = new StringReader(cInXmlStr);
	            SAXBuilder tSAXBuilder = new SAXBuilder();
	            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);
	            // --------------------

	            // 调用核心后台处理
	            YBKForwardXmlApi tYBKForwardXmlApi = new YBKForwardXmlApi();
	            Document tOutXmlDoc = tYBKForwardXmlApi.deal(tInXmlDoc);
	            // --------------------
//	            //modofy by lxs 
//            	Element tEleRoot = (Element) tOutXmlDoc.getRootElement().getChildren().get(0);
//            	System.out.println("tEleRoot"+tEleRoot);
//            	System.out.println("tEleRoot.getChildren()"+tEleRoot.getChildren());
//            	Element tFieldList = (Element) tEleRoot.getChildren().get(0);
////               	System.out.println("tXSNode"+tFieldList.getChildren());
//               	Element mFieldList =(Element) tFieldList.getChildren().get(5);
//              	System.out.println(	"mFieldList.getText()"+mFieldList.getText());
//   
//
//	            StringWriter tStringWriter = new StringWriter();
//	            XMLOutputter tXMLOutputter = new XMLOutputter();
//	            tXMLOutputter.output(tOutXmlDoc, tStringWriter);
//	            mOutXmlStr = tStringWriter.toString();
	            mOutXmlStr=JdomUtil.outputToString(tOutXmlDoc);
//	            if("PADPRINT".equals(mFieldList.getText())){
	            mOutXmlStr=mOutXmlStr.replaceAll("&lt;", "<");
	            mOutXmlStr=mOutXmlStr.replaceAll("&gt;", ">");
//          	}
	            System.out.println("===YBKForwardXmlProxy===返回xml==："+mOutXmlStr);
	        }
	        catch (Exception ex)
	        {
	            mLogger.error("交易出错！", ex);
	            mOutXmlStr = getError(ex.toString());
	        }
	        mLogger.debug(mOutXmlStr);

	        // 记录服务结束时间
	        long mDealMillis = System.currentTimeMillis() - mStartMillis;
	        mLogger.debug("处理总耗时：" + mDealMillis / 1000.0 + "s");
	        // --------------------
	        mLogger.info("End Service");
	        
	        System.out.println("===YBKForwardXmlProxy===返回时间："+PubFun.getCurrentTime());

	        return mOutXmlStr;
	    }

	    //非预期异常处理
	    public String getError(String pErrorMsg)
	    {
	        mLogger.info("Into CrsPubService.getError()...");

	        mLogger.info("Out CrsPubService.getError()!");
	        return null;
	    }
	    
	    public static byte[] InputStreamToBytes(InputStream pIns) {
			ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
			
			try {
				byte[] tBytes = new byte[8*1024];
				for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
					mByteArrayOutputStream.write(tBytes, 0, tReadSize);
				}
			} catch (IOException ex) {
				ex.printStackTrace();
				return null;
			} finally {
				try {
					pIns.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
			
			return mByteArrayOutputStream.toByteArray();
		}      
	    
	    
	    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException
	    {
	    	YBKForwardXmlProxy tYBKForwardXmlProxy = new YBKForwardXmlProxy();
	    	String mInFilePath = "F:/1.xml";
	    	InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = tYBKForwardXmlProxy.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "GBK");
			YBKForwardXmlProxy t = new YBKForwardXmlProxy();
	        t.service(mInXmlStr);
	    }
}