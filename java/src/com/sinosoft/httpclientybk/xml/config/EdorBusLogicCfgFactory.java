/**
 * 
 * webservice接口，核心对外（前置机）
 * @author 
 * @date 2014-06-09
 *
 */
package com.sinosoft.httpclientybk.xml.config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;


import com.sinosoft.httpclientybk.dto.G05.ENDORSEMENT_INFO;
import com.sinosoft.httpclientybk.dto.G05.SUCCESS_INFO;
import com.sinosoft.httpclientybk.obj.MsgResHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.IEdorBusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public final class EdorBusLogicCfgFactory extends ABusLogic {
	private final static Logger mLogger = Logger
			.getLogger(EdorBusLogicCfgFactory.class);
	private String Msg = "";
	public CErrors mErrors = new CErrors();

	public EdorBusLogicCfgFactory() {
	}

	public List<IEdorBusLogic> getNewInstanceBusLogic(MsgCollection cMsgInfos) {

		List<String> EdorTypes = new ArrayList<String>();
		List<IEdorBusLogic> mIEdorBusLogic = new ArrayList<IEdorBusLogic>();
		EdorTypes = addEdorTypes(cMsgInfos);
		System.out.println("保全类型为："+EdorTypes);
		if(EdorTypes.size()>1){
			String edornameSql = "select codealias from ldcode where codetype = 'msgtypeclassYbkBQ' and code = '"+EdorTypes.get(0)+"' with ur";
			String EdorName = new ExeSQL().execSQL(edornameSql).GetText(1, 1);
			Msg = "一次保全申请只能一个保全项目，本次只处理第一个保全项目:"+EdorName+",本次保全处理结果是:";
		}
		for (int i = 0; i < 1; i++) {
			String tClassSQL = "select codename from ldcode where codetype = 'msgtypeclassYbkBQ' and code = '"
					+ EdorTypes.get(i) + "' ";
			String tBusLogicSeriveClass = new ExeSQL().getOneValue(tClassSQL);
			System.out.println("tBusLogicSeriveClass=" + tBusLogicSeriveClass);

			if (tBusLogicSeriveClass == null) {
				System.out.println("tBusLogicSeriveClass="
						+ tBusLogicSeriveClass);
				return null;
			}
			IEdorBusLogic tEdorBusLogic = null;
			try {
				tEdorBusLogic = (IEdorBusLogic) Class.forName(
						tBusLogicSeriveClass).newInstance();
				mIEdorBusLogic.add(tEdorBusLogic);
			} catch (Exception ex) {
				System.out.println("ex");
				ex.printStackTrace();
				return null;
			}
		}

		return mIEdorBusLogic;
	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		MsgCollection tInMsgInfos = cMsgInfos;
		MsgCollection[] tOutMsgInfos = null ;
		try {
			List<IEdorBusLogic> tEdorBusLogic = getNewInstanceBusLogic(tInMsgInfos);
			tOutMsgInfos = new MsgCollection[tEdorBusLogic.size()];
			if (tEdorBusLogic == null) {
				System.out.println("程序异常，加载业务处理类失败。");
				return false;
			}
			System.out.println("实例化tBusLogic完成:"
					+ tInMsgInfos.getMsgHead().getREQUEST_TYPE());
			for (int j = 0; j < 1; j++){
				tOutMsgInfos[j] = tEdorBusLogic.get(j).service(tInMsgInfos);
			MsgResHead tResMsgHead = tOutMsgInfos[j].getMsgResHead();
			errLog(Msg+tResMsgHead.getERROR_MESSAGE(), tResMsgHead
					.getRESPONSE_CODE());
			List<SUCCESS_INFO> list = tOutMsgInfos[j].getBodyByFlag("SUCCESS_INFO");
			putResult("SUCCESS_INFO", list.get(0));
			}
		} catch (Exception e) {
			System.out.println("异常了");
			System.out.println(e.getStackTrace());
			e.printStackTrace();
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return false;
	}

	public List<String> addEdorTypes(MsgCollection cMsgInfos) {
		// 获取保单信息
		List<ENDORSEMENT_INFO> tEdorInfoList = cMsgInfos.getBodyByFlag("BODY");
		ENDORSEMENT_INFO tEdorInfo = tEdorInfoList.get(0);
		String mPolicyFormerNo = tEdorInfo.getPOLICY_SEQUENCE_NO();
		List<String> EdorTypes = new ArrayList<String>();
		String sql = "select distinct prtno  from YBK_N01_LIS_ResponseInfo where PolicySequenceNo = '"
				+ mPolicyFormerNo + "' and responsecode = '1'  with ur";
		mPrtNo = new ExeSQL().getOneValue(sql);
		String mIfCancel = tEdorInfo.getIF_CANCEL();
		SetPrtNo(mPrtNo);
		// =============EW保全项判断==============
		String EWsql = "select ifautopay,renemalpaymethod,medicalcode from lccontsub where prtno = '"
				+ mPrtNo + "' with ur";
		SSRS ewSSRS = new ExeSQL().execSQL(EWsql);
		if("0".equals(mIfCancel)){
			if(!(tEdorInfo.getIF_AUTO_RENEWAL().equals(ewSSRS.GetText(1, 1))&& tEdorInfo.getRENEWAL_DEDUCT_METHOD().equals(ewSSRS.GetText(1, 2)) && tEdorInfo.getMEDICAL_NO().equals(ewSSRS.GetText(1, 3)))){
				EdorTypes.add("EW");
			}
			// =============AD保全项判断==============
			String ADsql = "select lcad.postaladdress from lcaddress lcad,lccont lcc,Lcappnt lcap where lcad.customerno = lcc.appntno and lcap.addressno = lcad.addressno and lcap.contno = lcc.contno and lcc.prtno = '"
					+ mPrtNo + "'with ur";
			SSRS adSSRS = new ExeSQL().execSQL(ADsql);
			if (!tEdorInfo.getADDRESS().equals(adSSRS.GetText(1, 1))){
				EdorTypes.add("AD");
			}
			// =============CC保全项判断==============
			String CCsql = "select ldc.code,lcc.bankaccno from lccont lcc,ldcode1 ldc where lcc.bankcode = ldc.code1 and ldc.codetype = 'YbkBankCode' and lcc.prtno = '"
				+ mPrtNo + "'with ur";
			SSRS ccSSRS = new ExeSQL().execSQL(CCsql);
			if (!(tEdorInfo.getBANK_NAME().equals(ccSSRS.GetText(1, 1))&& tEdorInfo.getBANK_NO().equals(ccSSRS.GetText(1, 2)))){
				EdorTypes.add("CC");
			}
		}else if("1".equals(mIfCancel)){
			//2017-3-10	新增  于坤
			try{
				//保单失效判断
				String tCInValiDatesql = "select 1 From lccont lc,lcpol lp where "
						+" lc.contno = lp.contno "
						+" and lp.renewcount = (select distinct renewcount from ybk_n01_lis_responseinfo where prtno = lc.prtno) "
						+" and CInValiDate < current date "
						+" and lc.prtno = '"+mPrtNo+"' with ur ";
				String flag = new ExeSQL().getOneValue(tCInValiDatesql);
				if(""!=flag||!("").equals(flag)){
					System.out.println("保单已经失效不能做CT和WT!");
					this.mErrors.addOneError("保单已经失效不能做CT和WT!");
				}else{
					//查询出回执回销日期
					String Datesql=" select (case when customgetpoldate is not null "
							+" then customgetpoldate "
							+" else signdate "
							+" end) as date  from lccont where prtno='"+mPrtNo+"' with ur";
					SSRS tSSRS = new ExeSQL().execSQL(Datesql);
					String signdate = tSSRS.GetText(1,1);
					if("".equals(signdate)){
						System.out.println("保单还没有打印");
						this.mErrors.addOneError("没有找到回执回销日期");
						errLog("没有找到回执回销日期");
					}
					//签单日期次日零时
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
					Date date = (Date) df.parse(signdate);	
					Calendar cal = Calendar.getInstance();
					//计算起始日期
					String stratDate = df1.format(date);
					System.out.println("犹豫期退保起始日期date:"+stratDate);
					//计算终止日期
					cal.setTime(date);
					cal.add(Calendar.DATE, 10);
					Date date1=cal.getTime();
					String endDate = df1.format(date1);
					System.out.println("犹豫期退保失效日期date1:"+endDate);	
					//退保申请日期转换为核心日期格式
					SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMdd");
					Date date2 = (Date) df2.parse(tEdorInfo.getENDORSEMENT_APPLICATION_DATE());
					String tbDate = df.format(date2);
					System.out.println("退保申请日期date2:"+tbDate);
					if ((date2.getTime() >= date.getTime()) && (date2.getTime() <= date1.getTime())){
						EdorTypes.add("WT");
					}else{
						EdorTypes.add("CT");
					}
				}
			}catch (ParseException e) {
				e.printStackTrace();
			}
		}
			
		return EdorTypes;
	}
}