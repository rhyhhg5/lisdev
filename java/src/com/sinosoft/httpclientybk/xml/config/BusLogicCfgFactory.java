/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.httpclientybk.xml.config;

import org.apache.log4j.Logger;

import com.sinosoft.httpclientybk.dto.G01.INSURE_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.httpclientybk.xml.ctrl.IBusLogic;


public final class BusLogicCfgFactory
{
    private final static Logger mLogger = Logger.getLogger(BusLogicCfgFactory.class);

    private BusLogicCfgFactory()
    {
    }

    public static IBusLogic getNewInstanceBusLogic(MsgCollection cMsgInfos)
    {
    	MsgHead tMsgHead = cMsgInfos.getMsgHead();
        System.out.println("tMsgHead="+tMsgHead);
        if (tMsgHead == null)
        {
            mLogger.error("获取报头信息失败。");
            return null;
        }

        String tBusLogicFlag = tMsgHead.getREQUEST_TYPE();
        System.out.println("tBusLogicFlag="+tBusLogicFlag);
        String tClassSQL;
        if("G01".equals(tBusLogicFlag)){
        	INSURE_INFO mINSURE_INFO = (INSURE_INFO) cMsgInfos.getBodyByFlag("BODY").get(0);
        	System.out.println("投保渠道：" + mINSURE_INFO.getINSURE_CHANNEL());
        	if( mINSURE_INFO.getINSURE_CHANNEL().equals(null) || "".equals( mINSURE_INFO.getINSURE_CHANNEL())){
        		tClassSQL = "select codename from ldcode1 where codetype = 'msgtypeclassYbk' and code = '"+tBusLogicFlag+"' and code1='02'";
        	}else{
        		 tClassSQL = "select codename from ldcode1 where codetype = 'msgtypeclassYbk' and code = '"+tBusLogicFlag+"' and code1='"+mINSURE_INFO.getINSURE_CHANNEL()+"'";
        	}
        }else{
        	  tClassSQL = "select codename from ldcode where codetype = 'msgtypeclassYbk' and code = '"+tBusLogicFlag+"' ";
        }
        String tBusLogicSeriveClass = new ExeSQL().getOneValue(tClassSQL);
        System.out.println("tBusLogicSeriveClass="+tBusLogicSeriveClass);
        if (tBusLogicSeriveClass == null)
        {
        	System.out.println("tBusLogicSeriveClass="+tBusLogicSeriveClass);
            return null;
        }

        IBusLogic tBusLogic = null;
        try
        {
            tBusLogic = (IBusLogic) Class.forName(tBusLogicSeriveClass).newInstance();
        }
        catch (Exception ex)
        {
        	System.out.println("ex");
            ex.printStackTrace();
            return null;
        }

        return tBusLogic;
    }
}