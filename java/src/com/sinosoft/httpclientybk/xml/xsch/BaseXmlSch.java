/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.httpclientybk.xml.xsch;

import java.io.Serializable;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.jdom.Element;

public abstract class BaseXmlSch implements Serializable {
	protected Logger mLogger = Logger.getLogger(getClass().getName());

	public final boolean setField(String cFieldName, String cFieldValue) {
		try {
			Class[] paramType = new Class[1];
			paramType[0] = String.class;
			Method method = (this.getClass()).getMethod(("set" + cFieldName),paramType);

			Object[] args = new Object[1];
			args[0] = cFieldValue;
			method.invoke(this, args);
		} catch (Exception e) {
			mLogger.error("对Xsch对象字段赋值时出现异常。");
			return false;
		}
		return true;
	}

	public final boolean swapX2sch(Element cXmlNode) {
		return true;
	}

	public final Element swapSch2X() {
		return null;
	}
}
