/**
 * 
 * webservice接口，核心对外（前置机）
 *返回报文头返回状态  模糊状态
 *
 */
package com.sinosoft.httpclientybk.xml.ctrl;

import org.apache.log4j.Logger;

import com.sinosoft.httpclientybk.obj.MsgResHead;
import com.sinosoft.httpclientybk.util.MsgUtil;
import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public abstract class AEdorBusLogic implements IEdorBusLogic {
	protected CErrors mErrors = new CErrors();
	protected Logger mLogger = Logger.getLogger(getClass().getName());
	protected String mCurrentDate = PubFun.getCurrentDate();
	protected String mCurrentTime = PubFun.getCurrentTime();
	protected MMap map = new MMap();
	protected VData mLogMsg = new VData();
	protected String mPrtNo;
	

	protected MsgCollection mResMsgCols = null;

	public AEdorBusLogic() {
		mLogger.info("start:" + getClass().getName());
	}

	public final MsgCollection service(MsgCollection cMsgInfos) {
		mResMsgCols = MsgUtil.getResMsgCol(cMsgInfos);
		MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
		System.out.println(cMsgInfos);
		if (!deal(cMsgInfos)) {
			if (tResMsgHead.getERROR_MESSAGE() == null) {
				errLog("逻辑处理中出现未知异常","E");
			}
		}
		return getResult();
	}

	protected final void errLog(String cErrInfo) {
		mLogger.error("ErrInfo:" + cErrInfo);

		String tErrCode = "99999999";
		MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
		tResMsgHead.setRESPONSE_CODE("0");
		tResMsgHead.setERROR_MESSAGE(tErrCode);
	}

	protected final void errLog(String cErrInfo, String cErrCode) {
		mLogger.error("ErrInfo:" + cErrInfo);

		MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
		tResMsgHead.setRESPONSE_CODE(cErrCode);
		tResMsgHead.setERROR_MESSAGE(cErrInfo);
	}
	protected final void SetPrtNo(String PrtNo){
		this.mPrtNo = PrtNo;
	}
	
	protected MsgCollection getResult() {
		return mResMsgCols;
	}

	protected final void putResult(String cNodeFlag, BaseXmlSch cBxsch) {
		mResMsgCols.setBodyByFlag(cNodeFlag, cBxsch);
	}

	protected abstract boolean deal(MsgCollection cMsgInfos);

}
