/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.httpclientybk.xml.ctrl.pack;

import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.util.XschUtil;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class MsgXmlParse {
	private final static Logger mLogger = Logger.getLogger(MsgXmlParse.class);
	// 请求类型
	private String mMsgType = null;

	// private Document mXmlDoc = null;

	public MsgCollection deal(Document cInXmlDoc) {
		MsgCollection tMsgCols = new MsgCollection();

		Document tXmlDoc = (Document) cInXmlDoc.clone();
		Element tEleRoot = tXmlDoc.getRootElement();

		// 处理报头
		if (!loadMsgHead(tEleRoot, tMsgCols)) {
			return null;
		}
		// 处理消息体，不做阻断，找不到类属于正常情况，仅输出进行提示。
		if (!loadBodyNotes(tEleRoot, tMsgCols)) {
			mLogger.info("部分节点不存在对应对象信息，或Xsch自动加载失败。");
		}

		// --------------------

		return tMsgCols;
	}

	/**
	 * 自动加载报头信息。 <br />
	 * 报头信息有且仅有一个。 <br />
	 * 注：报头加载成功后，自动删除报头，用于后续报文主体信息的相关加载。
	 * 
	 * @param cEleRoot
	 * @param cMsgInfo
	 * @return
	 */
	private boolean loadMsgHead(Element cEleRoot, MsgCollection cMsgInfo) {
		String MsgHead_Name = "HEAD";
		Element[] tEleMsgHead = getHeadNodeByName(cEleRoot, MsgHead_Name);
		if (tEleMsgHead == null || tEleMsgHead.length != 1) {
			mLogger.error("报文头信息不存在或与约定不符，报头信息应该有且仅有一个。");
			return false;
		}

		for (int i = 0; i < tEleMsgHead.length; i++) {
			Element tEleNode = tEleMsgHead[i];
			MsgHead tBXsch = (MsgHead) XschUtil.swapX2Sch(tEleNode, "MsgHead");
			if (tBXsch == null) {
				return false;
			}
			mMsgType = tBXsch.getREQUEST_TYPE();
			cMsgInfo.setMsgHead(tBXsch);
		}

		cEleRoot.removeChild(MsgHead_Name);

		return true;
	}

	private boolean loadBodyNotes(Element cEleRoot, MsgCollection cMsgInfo) {
		boolean tResFlag = true;

		String tPackage_Name = "";
		tPackage_Name = "com.sinosoft.httpclientybk.dto." + mMsgType;
		List tFieldList = cEleRoot.getChildren();
		for (int idx = 0; idx < tFieldList.size(); idx++) {
			Object tXSNode = tFieldList.get(idx);
			if (!(tXSNode instanceof Element))
				continue;
			if ((((Element) tXSNode).getChildren()).size() == 0)
				continue;

			Element tEleSubNote = (Element) tXSNode;
			String tNoteName = tEleSubNote.getName();

			Element[] tEleMsgNode = getBodyNodeByName(cEleRoot, tNoteName);
			if (tEleMsgNode == null) {
				tResFlag = false;
				mLogger.error("报文数据节点[" + tNoteName + "]未找到相关xml数据信息。");
				continue;
			}

			for (int i = 0; i < tEleMsgNode.length; i++) {
				Element tEleNode = tEleMsgNode[i];
				/**
				 * tBodyChildNoteName为报文body体根节点标签即BODY后面根据前面的package_name+
				 * BODY获取对应的类
				 */
				String tBodyChildNoteName = tEleNode.getName();
				BaseXmlSch tBXsch = (BaseXmlSch) XschUtil.swapX2Sch(tEleNode,
						tBodyChildNoteName, tPackage_Name);
				if (tBXsch == null) {
					tResFlag = false;
					mLogger.error("报文数据节点[" + tNoteName
							+ "]未找到对应Xsch对象，或装载时失败。");
					break;
				}
				cMsgInfo.setBodyByFlag(tNoteName, tBXsch);
			}
		}

		return tResFlag;
	}

	private Element[] getHeadNodeByName(Element cEleNodeList, String cNodeName) {
		Element[] tResNode = null;
		List tCurNodeList = cEleNodeList.getChildren(cNodeName);
		if (tCurNodeList == null || tCurNodeList.size() == 0) {
			mLogger.error("获取节点内容（HEAD）清单信息失败。");
			return null;
		}

		tResNode = new Element[tCurNodeList.size()];
		for (int i = 0; i < tCurNodeList.size(); i++) {
			tResNode[i] = (Element) tCurNodeList.get(i);
		}

		return tResNode;
	}

	//
	private Element[] getBodyNodeByName(Element cEleNodeList, String cNodeName) {

		Element[] tResNode = null;

		Element tEleCurNode = cEleNodeList.getChild(cNodeName);
		if (tEleCurNode == null) {
			mLogger.error("获取节点[" + cNodeName + "]信息失败。");
			return null;
		}

		List tCurNodeList = tEleCurNode.getChildren();
		if (tCurNodeList == null || tCurNodeList.size() == 0) {
			mLogger.error("获取BODY下子节点清单信息失败。");
			return null;
		}

		tResNode = new Element[tCurNodeList.size()];
		for (int i = 0; i < tCurNodeList.size(); i++) {
			tResNode[i] = (Element) tCurNodeList.get(i);
		}

		return tResNode;
	}

}
