/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.httpclientybk.xml.ctrl.pack;

import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.httpclientybk.obj.MsgResHead;
import com.sinosoft.httpclientybk.obj.SYMsgResHead;
import com.sinosoft.httpclientybk.util.XschUtil;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class MsgXschParse {
	private final static Logger mLogger = Logger.getLogger(MsgXmlParse.class);
	String tRequestType = null;

	public Document deal(MsgCollection cMsgInfos,String restype) {
		MsgCollection tMsgCols = cMsgInfos;
		Document tMsgXmlDoc = null;
		tRequestType = restype;
		Element tResHead = null;

		Element tEleRoot = new Element("PACKET");
		tEleRoot.addAttribute("type", "RESPONSE");
		tEleRoot.addAttribute("version", "1.0");
		
		if("SY05".equals(restype)||"SY03".equals(restype)||"SYTB01".equals(restype) || "SYTB02".equals(restype)){
			System.out.println("走沈阳医保");
			// 处理返回报文头
			tResHead = createSYResMsgHead(tMsgCols);
		}else{
			System.out.println("走上海医保");
			// 处理返回报文头
			tResHead = createResMsgHead(tMsgCols);
		}
		if (tResHead == null) {
			return null;
		}
		tEleRoot.addContent(tResHead);
		// --------------------
		// 处理消息体，不做阻断，找不到类属于正常情况，仅输出进行提示。
		//如果返回报文没有报文体 则跳过
		if (!cMsgInfos.getBodyIdxList().isEmpty())
			if (!createBodyNotes(tEleRoot, tMsgCols)) {
				mLogger.info("部分节点不存在对应字段信息，或者Xsch自动转换失败。");
			}
		// --------------------
		tMsgXmlDoc = new Document(tEleRoot);
		return tMsgXmlDoc;
	}
	//沈阳医保修改
	//上海医保调用
	private Element createResMsgHead(MsgCollection cMsgInfos) {
		String tSubNodeFlag = "HEAD";
		MsgResHead tMsgResHead = cMsgInfos.getMsgResHead();
		if (tMsgResHead == null) {
			mLogger.error("返回报头信息读取失败。");
			return null;
		}
		Element tSubNode = XschUtil.swapSch2X(tMsgResHead, tSubNodeFlag);
		return tSubNode;
	}
	//沈阳医保调用
	private Element createSYResMsgHead(MsgCollection cMsgInfos) {
		String tSubNodeFlag = "HEAD";
		Element tSubNode = null;
		if("SYTB01".equals(tRequestType)){
			MsgResHead tSYMsgResHead = cMsgInfos.getMsgResHead();
			if (tSYMsgResHead == null) {
				mLogger.error("返回报头信息读取失败。");
				return null;
			}
			tSubNode = XschUtil.swapSch2X(tSYMsgResHead, tSubNodeFlag);
		}else {
			SYMsgResHead tSYMsgResHead = cMsgInfos.getmSYMsgResHead();
			if (tSYMsgResHead == null) {
				mLogger.error("返回报头信息读取失败。");
				return null;
			}
			tSubNode = XschUtil.swapSch2X(tSYMsgResHead, tSubNodeFlag);
		}
		return tSubNode;
	}

	private boolean createBodyNotes(Element cEleRoot, MsgCollection cMsgInfos) {
		String tSubNodeFlag = "BODY";
		Element tEleRoot = cEleRoot;

		List tBodyList = cMsgInfos.getBodyIdxList();
		if (tBodyList == null || tBodyList.size() == 0) {
			mLogger.error("返回报头信息读取失败。");
			return false;
		}
		Element tSubBody = new Element(tSubNodeFlag);
		for (int idx = 0; idx < tBodyList.size(); idx++) {
			String tNodeName = (String) tBodyList.get(idx);
			Element tSubNode = new Element(tNodeName);
			List tSNList = cMsgInfos.getBodyByFlag(tNodeName);
			Element tSINode = null;
			for (int i = 0; i < tSNList.size(); i++) {
				BaseXmlSch tBxsch = (BaseXmlSch) tSNList.get(i);
				tSINode = XschUtil.swapSch2X(tBxsch, "SUCCESS_INFO");

			}

			tSubBody.addContent(tSINode);

		}

		tEleRoot.addContent(tSubBody);

		return true;
	}
}
