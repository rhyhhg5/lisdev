package com.sinosoft.httpclientybk.xml.ctrl;

import org.jdom.Document;


import com.sinosoft.httpclientybk.xml.config.BusLogicCfgFactory;
import com.sinosoft.httpclientybk.xml.ctrl.pack.MsgXmlParse;
import com.sinosoft.httpclientybk.xml.ctrl.pack.MsgXschParse;

import com.sinosoft.httpclientybk.xml.ctrl.IBusLogic;



public class YBKForwardXmlApi {
	public Document deal(Document cInXmlDoc)
    {
        MsgCollection tInMsgInfos = null;
        MsgCollection tOutMsgInfos = null;

        // 将报文UnPack，并解析xml数据，自动封装报文对象集 MsgCollection
        MsgXmlParse tMsgXParse = new MsgXmlParse();
        System.out.println("执行YBKForwardXmlApi的tMsgXParse.deal方法");
        tInMsgInfos = tMsgXParse.deal(cInXmlDoc);
        // --------------------

        // 业务处理
        System.out.println("执行YBKForwardXmlApi方法");
        System.out.println("报文格式如下：");
        System.out.println(tInMsgInfos.toString());
        try
        {
            IBusLogic tBusLogic = BusLogicCfgFactory.getNewInstanceBusLogic(tInMsgInfos);
            if (tBusLogic == null)
            {
                System.out.println("程序异常，加载业务处理类失败。");
                return null;
            }
            System.out.println("实例化tBusLogic完成:"+tInMsgInfos.getMsgHead().getREQUEST_TYPE());
            tOutMsgInfos = tBusLogic.service(tInMsgInfos);
        
            
        }
        catch (Exception e)
        {
            System.out.println("异常了");
            System.out.println(e.getStackTrace());
            e.printStackTrace();
            System.out.println(e.getMessage());
            return null;
        }


        // 将返回报文对象集Pack，并转换返回xml数据。
        Document tOutDoc = null;
        MsgXschParse tMsgXschParse = new MsgXschParse();
        if(tOutMsgInfos==null){
        	//沈阳医保不返回报文
        	return tOutDoc;
        }else{
        	String restype = tInMsgInfos.getMsgHead().getREQUEST_TYPE();
        	tOutDoc = tMsgXschParse.deal(tOutMsgInfos,restype);
            // --------------------
            return tOutDoc;
        }
    }
}
