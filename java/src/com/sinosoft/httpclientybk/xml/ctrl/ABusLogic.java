/**
 * 
 * webservice接口，核心对外（前置机）
 *返回报文头返回状态  模糊状态
 *
 */
package com.sinosoft.httpclientybk.xml.ctrl;

import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.SYTB01.INSURE_INFO;
import com.sinosoft.httpclientybk.obj.MsgResHead;
import com.sinosoft.httpclientybk.obj.SYMsgResHead;
import com.sinosoft.httpclientybk.util.MsgUtil;
import com.sinosoft.httpclientybk.xml.config.EdorBusLogicCfgFactory;
import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.InterfaceDealLogInfoSchema;
import com.sinosoft.lis.schema.YBKErrorListSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public abstract class ABusLogic implements IBusLogic {
	protected CErrors mErrors = new CErrors();
	protected Logger mLogger = Logger.getLogger(getClass().getName());
	protected String mCurrentDate = PubFun.getCurrentDate();
	protected String mCurrentTime = PubFun.getCurrentTime();
	protected MMap map = new MMap();
	protected VData mLogMsg = new VData();
	protected String mPrtNo;
	/*沈阳医保新增responsecode*/
	private String mResponseCode = "";
	/*沈阳医保新增errorinfo*/
	private String mErrorInfo = "";

	protected MsgCollection mResMsgCols = null;
	//沈阳医保返回报文
	protected MsgCollection mSYResMsgCols = null;
	//任務編碼
	String TaskNo = null;
	private String APPSerialno = "";

	public ABusLogic() {
		mLogger.info("start:" + getClass().getName());
	}

	public final MsgCollection service(MsgCollection cMsgInfos) {
		mResMsgCols = MsgUtil.getResMsgCol(cMsgInfos);
		MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
		//沈阳医保返回报文头
		mSYResMsgCols = MsgUtil.getSYResMsgCol(cMsgInfos);
		SYMsgResHead tSYMsgResHead = mSYResMsgCols.getmSYMsgResHead();
		
		if (!deal(cMsgInfos)) {
			if (tResMsgHead.getERROR_MESSAGE() == null) {
//				errLog("逻辑处理中出现未知异常","E");
				EdorBusLogicCfgFactory tEdorBusLogicCfgFactory = new EdorBusLogicCfgFactory();
				this.mErrors.copyAllErrors(tEdorBusLogicCfgFactory.mErrors);
				System.out.println(tEdorBusLogicCfgFactory.mErrors.getFirstError());
				errLog(this.mErrors.getFirstError(),"E");
			}
		}
		//沈阳医保修改
		String sql = "select 1 from ldcode where codetype = 'msgtypeclassYbk' and code like 'SY%' and code = '"+tResMsgHead.getREQUEST_TYPE()+"'";
		String flag = new ExeSQL().getOneValue(sql);
		if(!"".equals(flag)&&flag!=null){
			System.out.println("ABusLogic类中的REQUEST_TYPE():"+tResMsgHead.getREQUEST_TYPE());
			//需要返回报文的类在这里添加判断
			if("SYTB01".equals(tResMsgHead.getREQUEST_TYPE())){
				List sINSURE_INFO_List = cMsgInfos.getBodyByFlag("BODY");
				INSURE_INFO sINSURE_INFO = (INSURE_INFO) sINSURE_INFO_List.get(0);
				APPSerialno = sINSURE_INFO.getPOLICY_FORMER_NO();
				
				TaskNo = PubFun1.CreateMaxNo("SEQ_SYYBK_BATCHNO", 20);
				tResMsgHead.setTASK_NO(APPSerialno);
				tResMsgHead.setRESPONSE_CODE(mResponseCode);
				tResMsgHead.setERROR_MESSAGE(mErrorInfo);
				//保存日志到表
				saveLogMsgSYTB(tResMsgHead);
				int a = 0 ;
				return getResult();
			
			}
			if("SY02".equals(tResMsgHead.getREQUEST_TYPE())||"SY03".equals(tResMsgHead.getREQUEST_TYPE())||"SY05".equals(tResMsgHead.getREQUEST_TYPE())){
				tSYMsgResHead.setTASK_NO(cMsgInfos.getMsgHead().getTASK_NO());
			}else if ("SYTB02".equals(tResMsgHead.getREQUEST_TYPE())){
				//保存日志到沈阳医保
				saveLogMsgSY(tSYMsgResHead);
				tResMsgHead.setRESPONSE_CODE(mResponseCode);
				tResMsgHead.setERROR_MESSAGE(mErrorInfo);
				tSYMsgResHead.setTASK_NO(cMsgInfos.getMsgHead().getTASK_NO());
				return getSYResult();
			}else {
				MsgCollection tMsgCollection = new MsgCollection();
				tMsgCollection = null;
				tSYMsgResHead.setTASK_NO(cMsgInfos.getMsgHead().getTASK_NO());
				//保存日志到沈阳医保
				saveLogMsgSY(tSYMsgResHead);
				return tMsgCollection;
			}
			//保存日志到沈阳医保
			saveLogMsgSY(tSYMsgResHead);
			return getSYResult();
		}else{
			tResMsgHead.setTASK_NO(PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20));
			//保存日志到表
			saveLogMsg(tResMsgHead);
			return getResult();
		}
	}

	protected final void errLog(String cErrInfo) {
		mLogger.error("ErrInfo:" + cErrInfo);

		String tErrCode = "0";
		MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
		tResMsgHead.setRESPONSE_CODE(tErrCode);
		tResMsgHead.setERROR_MESSAGE(cErrInfo);
	}
	//沈阳医保错误信息
	protected final void errLogSY(String cErrInfo, String cErrCode) {
		mErrorInfo = cErrInfo;
		mResponseCode = cErrCode;
	}
	
	protected final void errLog1(String cErrInfo, String cErrCode, String Resultstatus,String RenewCount) {
		mLogger.error("ErrInfo:" + cErrInfo);
		MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
		tResMsgHead.setRESPONSE_CODE(cErrCode);
		tResMsgHead.setERROR_MESSAGE(cErrInfo);
		tResMsgHead.setResultStatus(Resultstatus);
		tResMsgHead.setTRANS_NO(RenewCount);
	}

	protected final void errLog(String cErrInfo, String cErrCode) {
		mLogger.error("ErrInfo:" + cErrInfo);

		MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
		tResMsgHead.setRESPONSE_CODE(cErrCode);
		tResMsgHead.setERROR_MESSAGE(cErrInfo);
	}
	@SuppressWarnings("unchecked")
	protected final boolean saveLogMsg(MsgResHead cResMsgHead){
		YBKErrorListSchema tYBKErrorListSchema = new YBKErrorListSchema();
		tYBKErrorListSchema.setSerialNo(cResMsgHead.getTASK_NO());
		tYBKErrorListSchema.setTransType(cResMsgHead.getREQUEST_TYPE());
		//此处暂时存续保次数
		tYBKErrorListSchema.setTransNo(cResMsgHead.getTRANS_NO());
		tYBKErrorListSchema.setBusinessNo(mPrtNo);
		tYBKErrorListSchema.setResponseCode(cResMsgHead.getRESPONSE_CODE());
		tYBKErrorListSchema.setErrorInfo(cResMsgHead.getERROR_MESSAGE());
		tYBKErrorListSchema.setMakeDate(mCurrentDate);
		tYBKErrorListSchema.setMakeTime(mCurrentTime);
		tYBKErrorListSchema.setModifyDate(mCurrentDate);
		tYBKErrorListSchema.setModifyTime(mCurrentTime);
		map.put(tYBKErrorListSchema, "INSERT");
		mLogMsg.add(map);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mLogMsg, "INSERT")) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}
	//沈阳医保新增日志保存
	@SuppressWarnings("unchecked")
	protected final boolean saveLogMsgSY(SYMsgResHead cSYResMsgHead){
		InterfaceDealLogInfoSchema tInterfaceDealLogInfoSchema = new InterfaceDealLogInfoSchema();
		tInterfaceDealLogInfoSchema.setSerialNo(PubFun1.CreateMaxNo("SEQ_SYYBK_BATCHNO", 20));
		tInterfaceDealLogInfoSchema.setTransNo(cSYResMsgHead.getTASK_NO());
		tInterfaceDealLogInfoSchema.setTransType(cSYResMsgHead.getREQUEST_TYPE());
		tInterfaceDealLogInfoSchema.setBusinessNo(mPrtNo);
		tInterfaceDealLogInfoSchema.setResponseCode(mResponseCode);
		tInterfaceDealLogInfoSchema.setErrorInfo(mErrorInfo);
		tInterfaceDealLogInfoSchema.setMakeDate(mCurrentDate);
		tInterfaceDealLogInfoSchema.setMakeTime(mCurrentTime);
		tInterfaceDealLogInfoSchema.setModifyDate(mCurrentDate);
		tInterfaceDealLogInfoSchema.setModifyTime(mCurrentTime);
		map.put(tInterfaceDealLogInfoSchema, "INSERT");
		mLogMsg.add(map);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mLogMsg, "INSERT")) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}
	
	//沈阳医保投保接口新增日志保存
		@SuppressWarnings("unchecked")
		protected final boolean saveLogMsgSYTB(MsgResHead cResMsgHead){
			InterfaceDealLogInfoSchema tInterfaceDealLogInfoSchema = new InterfaceDealLogInfoSchema();
			tInterfaceDealLogInfoSchema.setSerialNo(TaskNo);
			tInterfaceDealLogInfoSchema.setTransNo(APPSerialno);
			tInterfaceDealLogInfoSchema.setTransType(cResMsgHead.getREQUEST_TYPE());
			tInterfaceDealLogInfoSchema.setBusinessNo(mPrtNo);
			tInterfaceDealLogInfoSchema.setResponseCode(mResponseCode);
			tInterfaceDealLogInfoSchema.setErrorInfo(mErrorInfo);
			tInterfaceDealLogInfoSchema.setMakeDate(mCurrentDate);
			tInterfaceDealLogInfoSchema.setMakeTime(mCurrentTime);
			tInterfaceDealLogInfoSchema.setModifyDate(mCurrentDate);
			tInterfaceDealLogInfoSchema.setModifyTime(mCurrentTime);
			map.put(tInterfaceDealLogInfoSchema, "INSERT");
			mLogMsg.add(map);
			PubSubmit ps = new PubSubmit();
			if (!ps.submitData(mLogMsg, "INSERT")) {
				this.mErrors.copyAllErrors(ps.mErrors);
				return false;
			}
			return true;
		}
	
	protected final void SetPrtNo(String PrtNo){
		this.mPrtNo = PrtNo;
	}
	
	protected MsgCollection getResult() {
		return mResMsgCols;
	}
	//沈阳医保封装返回报文
	protected MsgCollection getSYResult() {
		return mSYResMsgCols;
	}

	protected final void putResult(String cNodeFlag, BaseXmlSch cBxsch) {
		mResMsgCols.setBodyByFlag(cNodeFlag, cBxsch);
	}
	//沈阳医保返回报文
	protected final void putSYResult(String cNodeFlag, BaseXmlSch cBxsch) {
		mSYResMsgCols.setBodyByFlag(cNodeFlag, cBxsch);
	}

	public final MsgCollection service(MsgCollection cMsgInfos,
			Document cInXmlDoc) {
		mResMsgCols = MsgUtil.getResMsgCol(cMsgInfos);
		if (!deal(cMsgInfos, cInXmlDoc)) {
			MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
			if (tResMsgHead.getERROR_MESSAGE() == null) {
				errLog("逻辑处理中出现，未知异常");
			}
		} else {
			MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
			tResMsgHead.setRESPONSE_CODE("1");
		}
		return getResult();
	}

	protected abstract boolean deal(MsgCollection cMsgInfos);

	protected abstract boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc);
}
