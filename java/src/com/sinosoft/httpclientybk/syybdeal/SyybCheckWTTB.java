/*
 * SY05沈阳医保犹豫期退保查询接口
 * yukun 2017-07-26新增
 * 
 * */
package com.sinosoft.httpclientybk.syybdeal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import com.sinosoft.httpclientybk.dto.SY05.ENDORSEMENT_INFO;
import com.sinosoft.httpclientybk.dto.SY05.SUCCESS_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class SyybCheckWTTB extends ABusLogic {

	/*报文类型*/
	public String MsgType = "";
	/*流水号*/
	private String TASK_NO = "";
	/*合同号*/
	private String tContNo = "";
	/*印刷号*/
	private String tPrtNo = "";
	/*返回报文类型*/
	private String REQUEST_TYPE = "";
	/*报报文体*/
	private ENDORSEMENT_INFO tENDORSEMENT_INFO = new ENDORSEMENT_INFO();
	private SUCCESS_INFO tSUCCESS_INFO  = new SUCCESS_INFO();
	/*公共信息*/
	public GlobalInput tGI = new GlobalInput();
	/*处理过程中的错误信息*/
	public String mError = "";
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return false;
	}
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("======开始处理沈阳医保犹豫期退保查询过程======");
		tGI.ManageCom = "86210100";
		tGI.ComCode = "86210100";
		tGI.Operator = "SYYB";
		
		//解析XML
		if (!parseXML(cMsgInfos)) {
			System.out.println("报文解析失败---");
			tSUCCESS_INFO.setIF_CAN_CANCEL("");
			tSUCCESS_INFO.setERROR_REASON("报文解析失败---");
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			//保存到日志表
			SetPrtNo(tPrtNo);
			errLogSY("报文解析失败!", "0");
			return false;
		}
		//数据校验
		if(!checkData()){
			System.out.println("传入数据校验失败---");
			//保存到日志表
			SetPrtNo(tPrtNo);
			errLogSY("传入数据校验失败!", "0");
			//封装返回报文
			tSUCCESS_INFO.setIF_CAN_CANCEL("");
			tSUCCESS_INFO.setERROR_REASON("传入数据校验失败---");
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			return false;
		}
		//是否可以做犹豫期退保查询
		if(!deal()){
			System.out.println("犹豫期退保查询失败---");
			//保存到日志表
			SetPrtNo(tPrtNo);
			errLogSY("犹豫期退保查询失败!", "0");
			//封装返回报文
			tSUCCESS_INFO.setIF_CAN_CANCEL("");
			tSUCCESS_INFO.setERROR_REASON("犹豫期退保查询失败！");
			getXmlResult();
			return false;
		}
		return true;
	}		
	/*
	 * 报文数据解析
	 * */
	private boolean parseXML(MsgCollection cMsgInfos) {
		System.out.println("开始解析XML");
		// 获取保单信息
		List<ENDORSEMENT_INFO> tEdorInfoList = cMsgInfos.getBodyByFlag("BODY");
		if (tEdorInfoList == null || tEdorInfoList.size() != 1) {
			errLogSY("请求报文信息获取失败!", "0");
			return false;
		}
		tENDORSEMENT_INFO = tEdorInfoList.get(0);
		//获取头信息
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		TASK_NO = tMsgHead.getTASK_NO();
		if("".equals(TASK_NO)||null == TASK_NO||"null".equals(TASK_NO)){
			errLogSY("task_no不能为空!", "0");
			return false;
		}
		REQUEST_TYPE = tMsgHead.getREQUEST_TYPE();
		System.out.println("TASK_NO = " + TASK_NO);
		System.out.println("REQUEST_TYPE = " + REQUEST_TYPE);
		//获取contno
		String tSYMedicalTempfeeno = tENDORSEMENT_INFO.getPAYMENTNO();
		String contnoSQL = "select prtno from lccontsub where SYMedicalTempfeeno = '"+tSYMedicalTempfeeno+"' with ur";
		SSRS tSSRS1 = new ExeSQL().execSQL(contnoSQL);
		if (tSSRS1 == null || "".equals(tSSRS1)) {
			errLogSY("Prtno获取失败!", "0");
			return false;
		}
		this.tPrtNo = tSSRS1.GetText(1, 1);
		System.out.println("tPrtNo=" + tPrtNo);
		//查询保单信息
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setPrtNo(tPrtNo);
		if (!tLCContDB.getInfo()) {
			errLogSY("传入的缴费单号错误，核心系统不存在保单.保单:“" + tPrtNo + "”", "0");
			return false;
		}
		String sql = "select contno from lccont where prtno = '"+tPrtNo+"' with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		this.tContNo = tSSRS.GetText(1, 1);
		System.out.println("tContNo="+tContNo);
		SetPrtNo(tPrtNo);
		return true;
	}
	/*
	 * 报文数据检测
	 * */
	private boolean checkData() {
		/** 对保全申请信息的校验 */
		if (null == tENDORSEMENT_INFO.getPAYMENTNO()
				|| "".equals(tENDORSEMENT_INFO.getPAYMENTNO())) {
			errLogSY("缴费单号不能为空", "0");
			return false;
		}
		if (null == tENDORSEMENT_INFO.getMEDICALNO()
				|| "".equals(tENDORSEMENT_INFO.getMEDICALNO())) {
			errLogSY("医保个人编号不能为空", "0");
			return false;
		}
		return true;
	}
	/*
	 * 是否可以做犹豫期退保查询
	 * */
	private boolean deal() {	
		//查询出回执回销日期		
		String Sql = "select customgetpoldate from LCCont where prtno = '" + tPrtNo + "' with ur" ;
		SSRS tSSRS = new ExeSQL().execSQL(Sql);
		if("".equals(tSSRS)||null==tSSRS||tSSRS.getMaxRow()<=0){
			System.out.println("保单"+tPrtNo+"尚未回执回销,不能进行犹豫期退保查询！");
			//保存到日志表
			SetPrtNo(tPrtNo);
			errLogSY("保单"+tPrtNo+"尚未回执回销,不能进行犹豫期退保查询！", "0");
			//封装返回报文
			tSUCCESS_INFO.setIF_CAN_CANCEL("");
			tSUCCESS_INFO.setERROR_REASON("犹豫期退保查询处理失败，保单尚未回执回销！");
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			return false;
			}
		String tCustomerPolDate = tSSRS.GetText(1, 1);
		//犹豫期15天校验
		try{
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date date = (Date) df.parse(tCustomerPolDate);	
			Calendar cal = Calendar.getInstance();
			//计算起始日期
			String stratDate = df.format(date);
			System.out.println("犹豫期退保起始日期date:"+stratDate);
			//计算终止日期
			cal.setTime(date);
			cal.add(Calendar.DATE, 15);
			Date date1=cal.getTime();
			String endDate = df.format(date1);
			System.out.println("犹豫期退保失效日期date1:"+endDate);	
			//退保申请日期转换为核心日期格式
			Date date2 = (Date) df.parse(PubFun.getCurrentDate());
			String tbDate = df.format(date2);
			System.out.println("退保申请日期date2:"+tbDate);
			if ((date2.getTime() >= date.getTime()) && (date2.getTime() <= date1.getTime())){
				//校验保单是否做过收退费保全
				String sql = "select 1 from ljagetendorse where contno = '"+tContNo+"' with ur";
				String flag = new ExeSQL().getOneValue(sql);
				if("".equals(flag)||null==flag){
					System.out.println("可以进行犹豫期退保！");
					tSUCCESS_INFO.setIF_CAN_CANCEL("1");
					tSUCCESS_INFO.setERROR_REASON("可以申请犹豫期退保！");
					putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
					//保存到日志表
					SetPrtNo(tPrtNo);
					errLogSY("可以申请犹豫期退保!", "1");
					return true;
					}else{
						System.out.println("保单"+tContNo+"做过收退费保全，无法申请犹豫期退保！");
						tSUCCESS_INFO.setIF_CAN_CANCEL("0");
						tSUCCESS_INFO.setERROR_REASON("保单做过收退费保全，无法申请犹豫期退保！");
						putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
						//保存到日志表
						SetPrtNo(tPrtNo);
						errLogSY("保单"+tContNo+"做过收退费保全，无法申请犹豫期退保！","0");	
						return true;
						}
				}else{
					System.out.println("保单已经过犹豫期，无法申请犹豫期退保！");
					tSUCCESS_INFO.setIF_CAN_CANCEL("0");
					tSUCCESS_INFO.setERROR_REASON("保单已经过犹豫期，无法申请犹豫期退！");
					putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
					//保存到日志表
					SetPrtNo(tPrtNo);
					errLogSY("保单已经过犹豫期，无法申请犹豫期退保！","0");	
					return true;
					}
				}catch (ParseException e) {
					e.printStackTrace();
					return false;
					}
	}
	/*
	 * 返回信息报文
	 * */
	public void getXmlResult() {
		// 返回数据节点
		putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
	}
}
