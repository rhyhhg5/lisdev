package com.sinosoft.httpclientybk.syybdeal;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.SY04.CANCLE_APPLY_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.cbcheck.ApplyRecallPolUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCApplyRecallPolSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SYPolicyCancle extends ABusLogic{
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 其余变量*/
    private TransferData tTransferData = new TransferData();
    public String TaskNo = "";	    // 批次号
	public String MsgType = "";		// 报文类型
	public String Operator = "";	// 操作者
	
	public GlobalInput tG = new GlobalInput();// 公共信息
	/* 接收报文 */
	private CANCLE_APPLY_INFO cancleApplyInfo;
	//合同号
    private String ContNo ="";
    
    @Override
	protected boolean deal(MsgCollection cMsgInfos) {
		// TODO Auto-generated method stub
		System.out.println("---------------start--支付前撤单交易--解析报文--------------");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		
		TaskNo = tMsgHead.getTASK_NO();
		MsgType = tMsgHead.getREQUEST_TYPE();
		System.out.println("TaskNo----------"+TaskNo);
		System.out.println("MsgType----------"+MsgType);
		
		try {
			List tManualList = cMsgInfos.getBodyByFlag("BODY");
			if (tManualList == null || tManualList.size() != 1) {
				errLogSY("获取撤单申请xml信息失败", "0");
				return false;
			}
			/* 获取xml信息 */
			cancleApplyInfo = (CANCLE_APPLY_INFO) tManualList.get(0);  //获取支付前撤单申请信息
			if(cancleApplyInfo == null || "".equals(cancleApplyInfo)){
				errLogSY("获取支付前撤单申请信息失败!", "0");
				return false;
			}
			ContNo = cancleApplyInfo.getCONTNO();
			if(ContNo == null || "".equals(ContNo)){
				errLogSY("获取保单号失败!", "0");
				return false;
			}
			
			/* 业务处理 */
	    	try{
	    		if(!dealCancle()){
	    			return false;
	    		}
	    	}catch (Exception ex) {
	    		errLogSY("业务处理失败!", "0");
				return false;
				}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			errLogSY(e.toString(), "E");
			return false;
		}
		
		return true;
	}
	
	
	/**
     * 业务处理
     * @return boolean
     */
    private boolean dealCancle() throws Exception {
        System.out.println("----------------dealCancle()--------------");
        if(!ifCancle()){
        	errLogSY("不符合撤单条件!", "0");
        	return  false;
        }else{
        	//进行撤单操作
        	String querySql = "select prtno,cardflag,managecom,Operator from lccont where prtno='"+ContNo+"' ";
        	SSRS tSSRS=new SSRS();
	    	ExeSQL tExeSQL=new ExeSQL();
	    	tSSRS=tExeSQL.execSQL(querySql);
	    	if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
	    		errLogSY("保单印刷号获取失败!", "0");
				return false;
	    	}else{
	    		String tPrtNo = tSSRS.GetText(1, 1);
	    		String tCardFlag = tSSRS.GetText(1, 2);
	    		String tMagCom = tSSRS.GetText(1, 3);
	    		String Operator = tSSRS.GetText(1, 4);
	    		String tErrorInfo = "沈阳智慧医保支付前撤单交易";
	    		tG.ManageCom = tMagCom;
				tG.Operator = "001";
				tTransferData.setNameAndValue("CardFlag",tCardFlag);                                               
				LCApplyRecallPolSchema tLCApplyRecallPolSchema = new LCApplyRecallPolSchema();
				VData tVData = new VData();
    			//补充附加险表			    				
    			tLCApplyRecallPolSchema.setRemark(tErrorInfo);
    			tLCApplyRecallPolSchema.setPrtNo(tPrtNo);	
    			tLCApplyRecallPolSchema.setApplyType("3");
    			tLCApplyRecallPolSchema.setManageCom(tMagCom);
    			tLCApplyRecallPolSchema.setOperator(Operator);
    			// 准备传输数据 VData
    			tVData.add(tLCApplyRecallPolSchema);
    			tVData.add(tTransferData);
    			tVData.add(tG);
    	    	try {
    	    		ApplyRecallPolUI applyRecallPolUI = new ApplyRecallPolUI();
    	    		if(!applyRecallPolUI.submitData(tVData, "")){
    	    			this.mErrors.copyAllErrors(applyRecallPolUI.mErrors);
    	    			System.out.println("沈阳智慧医保支付前撤单交易撤单失败："+mErrors.getFirstError());
    	    			SetPrtNo(tPrtNo);
    	    			errLogSY("撤单失败!", "0");
    	    			return false; 
    	    		}
				} catch (Exception e) {
					System.out.println("沈阳智慧医保支付前撤单交易撤单异常");
					e.printStackTrace();
				}
    	    	SetPrtNo(tPrtNo);
	    	}
        }
        
        errLogSY("撤单成功!", "1");
        return true;
    }

    private boolean ifCancle() throws Exception {
   
    	String ifCancleSql = "select prtno from lccont lcc"
    			           +" where lcc.appflag in ('0','9') "
    			           +" and lcc.conttype = '1' "
    			           +" and (lcc.UWFlag is null or lcc.UWFlag not in ('a','1','8')) "
    			           +" and lcc.prtno='"+ContNo+"'"
    			           +" and lcc.prtno like 'SYW%' "
    			           +" and not exists (select 1 from InterfaceDealLogInfo where businessno=lcc.prtno and transtype='SY03' and responsecode='1') "
    			           +" and not exists (select 1 from LCApplyRecallPol where prtno=lcc.prtno) "
    			           +" with ur";
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(ifCancleSql);
    	if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
    		System.out.println("不符合撤单条件，或该保单已经撤单---Prtno---"+ContNo);
			return false;
    	}else{
    		System.out.println("可以撤单，投保单号---ContNo---"+ContNo);
    		return true;
    	}
    }
    
	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return true;
	}

}
