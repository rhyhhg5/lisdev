package com.sinosoft.httpclientybk.syybdeal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.BPOLCAppntSchema;
import com.sinosoft.lis.schema.BPOLCImpartSchema;
import com.sinosoft.lis.schema.BPOLCInsuredSchema;
import com.sinosoft.lis.schema.BPOLCPolSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LCCustomerImpartSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.SYYBKWXLCInsuredListSchema;
import com.sinosoft.lis.tb.BPOImpartBL;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.tb.SYYBKWXLCParseGuideIn;
import com.sinosoft.lis.vschema.BPOLCAppntSet;
import com.sinosoft.lis.vschema.BPOLCImpartSet;
import com.sinosoft.lis.vschema.BPOLCInsuredSet;
import com.sinosoft.lis.vschema.BPOLCPolSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SyybYBKWXCalCulateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	private TransferData mTransferData = new TransferData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate2();

	private SYYBKWXLCInsuredListSchema mYBKWXLCInsuredListSchema ;
	private LCPolSchema mLCPolSchema ;
	private LCContSubSchema mLCContSubSchema;
	private String MedicalCode="";
	
	private String IfAutoPay="";
	
	private String RenemalPayMethod="";
	
	private String PayMode="";
	
	//private String CustomerNo ="";
	private String YBKSerialno ="";
	private String PrtnoTT ="";
	private String WrapCode="";
	private String PayEndYear="";
	private String tPrtNo="";
	private String Postaladdress = "";
	private String Mobile = "";
	private String Email = "";
	private String Addressno="";
	private ExeSQL mExeSQL = new ExeSQL();
	private String mAppntNo = "";
	private String mInsuredNo  = "";
	private String strProposalContNo="";

	private MMap mMap = new MMap();
	private MMap subMap = new MMap();
	private MsgHead tMsgHead;
	

	public SyybYBKWXCalCulateBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			return false;
		}

		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		
		mYBKWXLCInsuredListSchema = (SYYBKWXLCInsuredListSchema)tTransferData.getValueByName("mYBKWXLCInsuredListSchema");
		Postaladdress = mYBKWXLCInsuredListSchema.getPostalAddress();
		Mobile = mYBKWXLCInsuredListSchema.getMobile();
		Email = mYBKWXLCInsuredListSchema.getEmail();
		mLCPolSchema = (LCPolSchema)tTransferData.getValueByName("mLCPolSchema");
		//特殊字段需要更新到目的表中
		RenemalPayMethod = (String) tTransferData.getValueByName("RenemalPayMethod");
		IfAutoPay = (String) tTransferData.getValueByName("IfAutoPay");
		MedicalCode = (String) tTransferData.getValueByName("MedicalCode");
		PayMode = (String) tTransferData.getValueByName("PayMode");
		YBKSerialno = (String) tTransferData.getValueByName("YBKSerialno");
		WrapCode = (String) tTransferData.getValueByName("WrapCode");
		tMsgHead = (MsgHead)tTransferData.getValueByName("MsgHead");
		PayEndYear =(String) tTransferData.getValueByName("PayEndYear");
		tPrtNo = (String)tTransferData.getValueByName("tPrtNo");
		mAppntNo = (String)tTransferData.getValueByName("mAppntNo");
		mInsuredNo = (String)tTransferData.getValueByName("mInsuredNo");
		strProposalContNo = (String)tTransferData.getValueByName("strProposalContNo");
		
		
		if (mYBKWXLCInsuredListSchema == null || mLCPolSchema == null ){
			CError tError = new CError();
			tError.moduleName = "YBKWXCalCulateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取XML转换成的DBSchema错误，无法处理！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	private boolean dealData(){
		
		if(!CreateCont(mYBKWXLCInsuredListSchema,mLCPolSchema)){
			CError tError = new CError();
			tError.moduleName = "YBKWXCalCulateBL";
			tError.functionName = "dealData";
			tError.errorMessage = "存在算费失败的被保人，详情请下载失败清单！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBKWXCalCulateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}
	
	
	/**
     * 生成一个保单的xml信息
     * @param tContTableE Element：根节点CONTTABLE
     * @param tBPOLCPolSet BPOLCPolSet：险种信息
     * @return boolean
     */
    private boolean createOneCont(Element tContTableE, SYYBKWXLCInsuredListSchema aYBKWXLCInsuredListSchema,LCPolSchema aLCPolSchema)
    {
        //为每个保单定义一个ROW节点
        Element tRowE = new Element("ROW");

        //添加CONTID节点
        Element tContIDE = new Element("CONTID");
        tContIDE.setText(aYBKWXLCInsuredListSchema.getContNo());
        tRowE.addContent(tContIDE);

        //添加投保人节点
        Element tAPPNTTABLEE = new Element("APPNTTABLE");
        tRowE.addContent(tAPPNTTABLEE);
        BPOLCAppntSet tBPOLCAppntSet = getBPOLCAppntSet(aYBKWXLCInsuredListSchema);
        if(!addRow(tAPPNTTABLEE, tBPOLCAppntSet))
        {
        	CError tError = new CError();
			tError.moduleName = "YBKWXCalCulateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备投保人数据时出错。";
			this.mErrors.addOneError(tError);
        	return false;
        }

        //添加被保人节点
        Element tINSUREDTABLEE = new Element("INSUREDTABLE");
        tRowE.addContent(tINSUREDTABLEE);
        BPOLCInsuredSet tBPOLCInsuredSet = getBPOLCInsuredSet(aYBKWXLCInsuredListSchema);
        if(!addRow(tINSUREDTABLEE, tBPOLCInsuredSet))
        {
        	CError tError = new CError();
			tError.moduleName = "YBKWXCalCulateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备被保人数据时出错。";
			this.mErrors.addOneError(tError);
        	return false;
        }
        
        System.out.println(tPrtNo);
        
        /*String riskCode =  new ExeSQL().getEncodedResult(sql);
        System.out.println(riskCode);
        String[] riskCodes = riskCode.split("/^");*/
     
        //添加险种节点
//        String flag  = aLCPolSchema.getRiskCode();
//        String sql  = "select riskcode from LDRiskWrap where RiskWrapCode='"+ WrapCode +"'";
//        SSRS mSSRS = new ExeSQL().execSQL(sql);
//        if(mSSRS.getMaxRow()>0){
//        	for(int i=1;i<=mSSRS.getMaxRow();i++){
//        		String riskCode = mSSRS.GetText(i, 1);
//        		if(!riskCode.equals(flag)){
//        			aLCPolSchema.setRiskCode(riskCode);
//        			tBPOLCPolSet.add(getBPOLCPolSet(aYBKWXLCInsuredListSchema,aLCPolSchema,tPrtNo).get(1));
//        		}
//        		
//        	}
//        }
        BPOLCPolSet tBPOLCPolSet  = getBPOLCPolSet(aYBKWXLCInsuredListSchema,aLCPolSchema,tPrtNo);
        if(tBPOLCPolSet.size() > 0)
        {
        	
            Element tLCPOLTABLEE = new Element("LCPOLTABLE");
            tRowE.addContent(tLCPOLTABLEE);
            if(!addRow(tLCPOLTABLEE, tBPOLCPolSet))
            {
            	CError tError = new CError();
    			tError.moduleName = "YBKWXCalCulateBL";
    			tError.functionName = "prepareData";
    			tError.errorMessage = "在准备险种数据时出错。";
    			this.mErrors.addOneError(tError);
            	return false;
            }
        }
        
      //添加健康告知节点
        Element tIMPARTTABLE = new Element("IMPARTTABLE");
        tRowE.addContent(tIMPARTTABLE);
        BPOLCImpartSet tBPOLCImpartSet = getBPOIMPARTSet(aYBKWXLCInsuredListSchema,aLCPolSchema);
        if(!addRow(tIMPARTTABLE, tBPOLCImpartSet))
        {
        	CError tError = new CError();
			tError.moduleName = "YBKWXCalCulateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备健康告知数据时出错。";
			this.mErrors.addOneError(tError);
        	return false;
        }
        
        tContTableE.addContent(tRowE);

        return true;
    }
    
    /**
     * 生成xml
     * @param cContIDs String[]：合同ID
     * @return String：生成的xml文件路径及名字
     */
    private boolean CreateCont(SYYBKWXLCInsuredListSchema tYBKWXLCInsuredListSchema,LCPolSchema tLCPolSchema)
    {

        boolean tAllFlag = true;
        	String tSQL = "select prtno from lccontsub "
        				+ "where YbkSerialno = '" + YBKSerialno +"'";
        	String tPrtno = mExeSQL.getOneValue(tSQL);
        	if(tPrtno != null && !"".equals(tPrtno)){
        		tAllFlag = false;
        		CError tError = new CError();
    			tError.moduleName = "YBKWXCalCulateBL";
    			tError.functionName = "prepareData";
    			tError.errorMessage = "该投保App订单号：" + YBKSerialno +"已经存在，对应的印刷号为：" + tPrtno;
    			this.mErrors.addOneError(tError);
            	return false;
        	}
        	
            //所有保单信息的父节点
            Element tContTableE = new Element("CONTTABLE");

            if(!createOneCont(tContTableE, tYBKWXLCInsuredListSchema,tLCPolSchema))
            {
            	tAllFlag = false;
            }

            //根节点
            Element root = new Element("DATASET");
            Document doc = new Document(root);

            Element tBatchIDE = new Element("BATCHID");
            //TODO 
            //批次号变成了投保流水号
            tBatchIDE.setText(YBKSerialno);
            root.addContent(tBatchIDE);

            //保单信息父节点跟在原外包批次节点后
            root.addContent(tContTableE);

            //输出到磁盘
            XMLOutputter xo = new XMLOutputter("  ", true, "utf-8");
            try
            {
//            	String tUIRoot = CommonBL.getUIRoot();
            	String fileFolder = "F:/temp_his/" + PubFun.getCurrentDate();
            	if(!newFolder(fileFolder)){
                	return false;
                }
            	String XmlFileName = fileFolder+ "/" + YBKSerialno + ".xml";
                xo.output(doc, new FileOutputStream(XmlFileName));
                mTransferData.setNameAndValue("FilePath", XmlFileName);
                mTransferData.setNameAndValue("FileName", tYBKWXLCInsuredListSchema.getSerNo());
                mTransferData.setNameAndValue("WrapCode",WrapCode);
                mTransferData.setNameAndValue("MsgType", tMsgHead.getREQUEST_TYPE());
                mTransferData.setNameAndValue("PayEndYear", PayEndYear);
                mTransferData.setNameAndValue("mYBKWXLCInsuredListSchema",mYBKWXLCInsuredListSchema);
                mTransferData.setNameAndValue("mLCPolSchema",tLCPolSchema);
                mTransferData.setNameAndValue("RenemalPayMethod",RenemalPayMethod);
                mTransferData.setNameAndValue("IfAutoPay",IfAutoPay);
                mTransferData.setNameAndValue("MedicalCode",MedicalCode);
                mTransferData.setNameAndValue("PayMode",PayMode);
                mTransferData.setNameAndValue("YBKSerialno",YBKSerialno);
                mTransferData.setNameAndValue("tMsgHead",tMsgHead);
                mTransferData.setNameAndValue("tPrtNo",tPrtNo);
                mTransferData.setNameAndValue("mAppntNo",mAppntNo);
                mTransferData.setNameAndValue("mInsuredNo",mInsuredNo);
                mTransferData.setNameAndValue("strProposalContNo",strProposalContNo);
                
                mInputData.add(mGlobalInput);
                mInputData.add(mTransferData);
                SYYBKWXLCParseGuideIn tYBKWXLCParseGuideIn = new SYYBKWXLCParseGuideIn();
        		boolean tFlag = tYBKWXLCParseGuideIn.submitData(mInputData, "YBK");
        		System.out.println(tYBKWXLCParseGuideIn.logErrors);
        		String CustomerNo = "";
        		if(tYBKWXLCParseGuideIn.logErrors.getErrorCount()>0){
        			CError tError = new CError();
                    tError.moduleName = "YBKWXCalCulateBL";
                    tError.functionName = "createXML";
                    tError.errorMessage = tYBKWXLCParseGuideIn.logErrors.getLastError();
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
        			tAllFlag = false;
        		}else{
        			VData temVData = tYBKWXLCParseGuideIn.getResult();
        		   PrtnoTT = tPrtNo;
        			//String a  = (String) temVData.getObjectByObjectName("mPrtNo", 0);
        			System.out.println("算费成功生成保单数据了！");
        		    //查找并更新保单附属表
    				LCContSubSchema tLCContSubSchema = new LCContSubSchema();
    				LCContSubDB tLCContSubDB = new LCContSubDB();
    				tLCContSubDB.setPrtNo(PrtnoTT);
    				boolean exists = tLCContSubDB.getInfo();
    				if (exists) {
    					//从投保报文里增加的几个字段 
    					tLCContSubSchema.setMedicalCode(MedicalCode);
    					tLCContSubSchema.setIfAutoPay(IfAutoPay);
    					tLCContSubSchema.setRenemalPayMethod(RenemalPayMethod);
    					tLCContSubSchema.setPayMode(PayMode);
    					tLCContSubSchema.setYbkSerialno(YBKSerialno);
    					tLCContSubSchema.setModifyDate(PubFun.getCurrentDate());
    					tLCContSubSchema.setModifyTime(PubFun.getCurrentTime());
    					subMap.put(tLCContSubSchema, "UPDATE");
    				} else {

    					tLCContSubSchema.setPrtNo(PrtnoTT);
    					tLCContSubSchema.setManageCom("86310000");
    					tLCContSubSchema.setOperator(mGlobalInput.Operator);
    					tLCContSubSchema.setMakeDate(PubFun.getCurrentDate());
    					tLCContSubSchema.setMakeTime(PubFun.getCurrentTime());
    					tLCContSubSchema.setModifyDate(PubFun.getCurrentDate());
    					tLCContSubSchema.setModifyTime(PubFun.getCurrentTime());
    					//从投保报文里增加的几个字段 
    					tLCContSubSchema.setMedicalCode(MedicalCode);
    					tLCContSubSchema.setIfAutoPay(IfAutoPay);
    					tLCContSubSchema.setRenemalPayMethod(RenemalPayMethod);
    					tLCContSubSchema.setPayMode(PayMode);
    					tLCContSubSchema.setYbkSerialno(YBKSerialno);
    					subMap.put(tLCContSubSchema, "INSERT");
    					
    			}
    				
    				//查找并更新LDperson 
    				LDPersonSchema tLDPersonSchema = new LDPersonSchema();
    				LDPersonDB tLDPersonDB = new LDPersonDB();
    				String sqlcustomerno ="select appntno from lccont where prtno ='" + PrtnoTT + "'";
    				SSRS mSSRS = new ExeSQL().execSQL(sqlcustomerno);
    				CustomerNo = mSSRS.GetText(1, 1);
    				/*tLDPersonDB.setCustomerNo(mSSRS.GetText(1, 1));
    				boolean b = tLDPersonDB.getInfo();
    				if(b){
    					String sqlUPdate ="update ldperson set Ybkcustomerno ='" + CustomerNo+ "' where CustomerNo='" + mSSRS.GetText(1, 1)+ "'";
    					subMap.put(sqlUPdate, "UPDATE");
    				}
    				*/
    				//更新LCAddress
    				String sql = "select addressno from LCAddress where Customerno ='"+CustomerNo+"' ";
    				mSSRS = new ExeSQL().execSQL(sql);
    				Addressno = mSSRS.GetText(1, 1); 
    				System.out.println(mSSRS.getMaxRow());
    				String	 Postalprovince="";
	            	String  Postalcity ="";
	            	String  Postalcounty="";
	            	String  Postalstreet ="";
	            	String  Postycommunity="";
    				if(mSSRS.getMaxRow()>0){
    					Addressno = mSSRS.GetText(1, 1); 
    					String[] tPostalAddress=Postaladdress.split("[$]",-1);
    					  if(tPostalAddress.length==5){
    						  	 Postalprovince = tPostalAddress[0];
    			            	 Postalcity = tPostalAddress[1];
    			            	 Postalcounty = tPostalAddress[2];
    			            	 Postalstreet = tPostalAddress[3];
    			            	 Postycommunity = tPostalAddress[4];
    			            }else{
    			            	 Postalprovince = Postaladdress;
    			            	 Postalcity = Postaladdress;
    			            	 Postalcounty = Postaladdress;
    			            	 Postalstreet = Postaladdress;
    			            	 Postycommunity = Postaladdress;
    			            }
    					String addresssql = "update LCAddress set postaladdress = '"+Postaladdress+"',mobile = '"+Mobile+"',email = '"+Email+"' ,"
    							+ " Postalprovince='"+Postalprovince+"',Postalcity='"+Postalcity+"',Postalcounty='"+Postalcounty+"'"
    									+ ",Postalstreet='"+Postalstreet+"',Postalcommunity='"+Postycommunity+"'  where Customerno ='"+CustomerNo+"' ";
    					subMap.put(addresssql, "UPDATE");
    				}else{
    					Addressno="1";
    				}
    				/*LCAppntSchema tLCAppntSchema = new LCAppntSchema();
    				tLCAppntSchema.setGrpContNo("00000000000000000000");
    				tLCAppntSchema.setContNo(strProposalContNo);
    				tLCAppntSchema.setPrtNo(tPrtNo);
    				tLCAppntSchema.setAppntNo(mAppntNo);
    				tLCAppntSchema.setAppntName(tYBKWXLCInsuredListSchema.getName());
    				tLCAppntSchema.setAppntSex(tYBKWXLCInsuredListSchema.getSex());
    				tLCAppntSchema.setAppntBirthday(tYBKWXLCInsuredListSchema.getBirthDay());
    				tLCAppntSchema.setAddressNo(Addressno);
    				tLCAppntSchema.setIDType(tYBKWXLCInsuredListSchema.getIDType());
    				tLCAppntSchema.setIDNo(tYBKWXLCInsuredListSchema.getIDNo());
    				tLCAppntSchema.setRgtAddress(tYBKWXLCInsuredListSchema.getPostalAddress());
    				tLCAppntSchema.setBankCode(tYBKWXLCInsuredListSchema.getBankCode());
    				tLCAppntSchema.setBankAccNo(tYBKWXLCInsuredListSchema.getBankAccNo());
    				tLCAppntSchema.setAccName(tYBKWXLCInsuredListSchema.getName());
    				tLCAppntSchema.setOccupationCode(tYBKWXLCInsuredListSchema.getOccupationCode());
    				tLCAppntSchema.setOccupationType(tYBKWXLCInsuredListSchema.getOccupationType());
    				tLCAppntSchema.setManageCom(tLCPolSchema.getManageCom());
    				tLCAppntSchema.setOperator("YBK");
    				tLCAppntSchema.setMakeDate(PubFun.getCurrentDate());
    				tLCAppntSchema.setMakeTime(PubFun.getCurrentTime());
    				tLCAppntSchema.setModifyDate(PubFun.getCurrentDate());
    				tLCAppntSchema.setModifyTime(PubFun.getCurrentTime());
    				subMap.put(tLCAppntSchema, "INSERT");*/
        		}
            }
            catch(IOException ex)
            {
                ex.printStackTrace();
                CError tError = new CError();
                tError.moduleName = "YBKWXCalCulateBL";
                tError.functionName = "createXML";
                tError.errorMessage = "无法输出xml文件";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
            }

            batchSave(subMap);
        return tAllFlag;
    }
    
    
    /**
     * FTTP生成文件目录
     * */
    
    public static boolean newFolder(String folderPath) {
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try {
			if (myFilePath.isDirectory()) {
				System.out.println("目录已存在");
				return true;
			} else {
				myFilePath.mkdirs();
				System.out.println("新建目录成功");
				return true;
			}
		} catch (Exception e) {
			System.out.println("新建目录失败");
			e.printStackTrace();
			return false;
		}
	}
    
    private boolean batchSave(MMap map) {
		PubSubmit pubSubmit = new PubSubmit();
		VData sData = new VData();
		sData.add(map);
		boolean tr = pubSubmit.submitData(sData, "");

		if (!tr) {
			if (pubSubmit.mErrors.getErrorCount() > 0) {
				// 错误回退
				// mErrors.copyAllErrors(pubSubmit.mErrors);
				pubSubmit.mErrors.clearErrors();
			} else {
				CError.buildErr(this, "保存数据库的时候失败！");
			}
			return false;
		}
		return true;
	}
    /**
     * addAppnt
     *
     * @return boolean
     */
    private boolean addRow(Element tElement, SchemaSet cSet)
    {
    	for(int i = 1; i <= cSet.size(); i++)
        {
            Element tRowE = new Element("ROW");
            tElement.addContent(tRowE);

            Schema schema = (Schema) cSet.getObj(i);

            for(int j = 1; j < schema.getFieldCount(); j++)
            {
                String tFieldName = schema.getFieldName(j);
                String tFileValue = schema.getV(tFieldName);
                tFileValue = (tFileValue == null || tFileValue.equals("null")
                              ? "" : tFileValue);

                Element tFieldNodeE = new Element(tFieldName);
                tFieldNodeE.setText(StrTool.cTrim(tFileValue));
                tRowE.addContent(tFieldNodeE);
            }
        }

        return true;
    }
    
    private BPOLCAppntSet getBPOLCAppntSet(SYYBKWXLCInsuredListSchema aHISLCInsuredListSchema){
    	BPOLCAppntSet aBPOLCAppntSet = new BPOLCAppntSet();
    	BPOLCAppntSchema aBPOLCAppntSchema = new BPOLCAppntSchema();
    	aBPOLCAppntSchema.setAppntID(aHISLCInsuredListSchema.getContNo());
    	aBPOLCAppntSchema.setContID(aHISLCInsuredListSchema.getContNo());
    	aBPOLCAppntSchema.setName(aHISLCInsuredListSchema.getName());
    	aBPOLCAppntSchema.setSex(aHISLCInsuredListSchema.getSex());
    	aBPOLCAppntSchema.setBirthday(aHISLCInsuredListSchema.getBirthDay());
    	aBPOLCAppntSchema.setIDType(aHISLCInsuredListSchema.getIDType());
    	aBPOLCAppntSchema.setIDNo(aHISLCInsuredListSchema.getIDNo());
    	aBPOLCAppntSchema.setNativePlace(aHISLCInsuredListSchema.getNativePlace());
    	aBPOLCAppntSchema.setRgtAddress(aHISLCInsuredListSchema.getPostalAddress());
    	aBPOLCAppntSchema.setMarriage(aHISLCInsuredListSchema.getmarriage());
    	//aBPOLCAppntSchema.setNationality("31");
    	aBPOLCAppntSchema.setOccupationCode(aHISLCInsuredListSchema.getOccupationCode());
    	aBPOLCAppntSchema.setOccupationType(aHISLCInsuredListSchema.getOccupationType());
    	aBPOLCAppntSchema.setWorkType("");
    	aBPOLCAppntSchema.setPluralityType("");
    	aBPOLCAppntSchema.setBankCode(aHISLCInsuredListSchema.getBankCode());
    	aBPOLCAppntSchema.setBankAccNo(aHISLCInsuredListSchema.getBankAccNo());
    	aBPOLCAppntSchema.setAccName(aHISLCInsuredListSchema.getAccName());
    	aBPOLCAppntSchema.setSmokeFlag("");
    	aBPOLCAppntSchema.setPostalAddress(aHISLCInsuredListSchema.getPostalAddress());
    	aBPOLCAppntSchema.setZipCode(aHISLCInsuredListSchema.getZipCode());
    	aBPOLCAppntSchema.setPhone(aHISLCInsuredListSchema.getPHONE());
    	aBPOLCAppntSchema.setFax(aHISLCInsuredListSchema.getFax());
    	aBPOLCAppntSchema.setMobile(aHISLCInsuredListSchema.getMobile());
    	aBPOLCAppntSchema.setEMail(aHISLCInsuredListSchema.getEmail());
    	aBPOLCAppntSchema.setIDStartDate(aHISLCInsuredListSchema.getIDStartDate());
    	aBPOLCAppntSchema.setIDEndDate(aHISLCInsuredListSchema.getIDEndDate());
    	
    	aBPOLCAppntSet.add(aBPOLCAppntSchema);
    	return aBPOLCAppntSet;
    }
    
    private BPOLCInsuredSet getBPOLCInsuredSet(SYYBKWXLCInsuredListSchema aHISLCInsuredListSchema){
    	BPOLCInsuredSet aBPOLCInsuredSet = new BPOLCInsuredSet();
    	BPOLCInsuredSchema aBPOLCInsuredSchema = new BPOLCInsuredSchema();
    	aBPOLCInsuredSchema.setInsuredID(aHISLCInsuredListSchema.getContNo());
    	aBPOLCInsuredSchema.setContID(aHISLCInsuredListSchema.getContNo());
    	aBPOLCInsuredSchema.setName(aHISLCInsuredListSchema.getInsuredName());
    	aBPOLCInsuredSchema.setSex(aHISLCInsuredListSchema.getInsureSex());
    	aBPOLCInsuredSchema.setBirthday(aHISLCInsuredListSchema.getInsureBirthday());
    	aBPOLCInsuredSchema.setIDType(aHISLCInsuredListSchema.getInsureIDType());
    	aBPOLCInsuredSchema.setIDNo(aHISLCInsuredListSchema.getInsureIDNo());
    	aBPOLCInsuredSchema.setNativePlace(aHISLCInsuredListSchema.getNativePlace());
    	aBPOLCInsuredSchema.setRgtAddress(aHISLCInsuredListSchema.getPostalAddress());
    	aBPOLCInsuredSchema.setMarriage(aHISLCInsuredListSchema.getmarriage());
    	//aBPOLCInsuredSchema.setNationality(aHISLCInsuredListSchema.getNativePlace());
    	aBPOLCInsuredSchema.setOccupationCode(aHISLCInsuredListSchema.getOccupationCode());
    	aBPOLCInsuredSchema.setOccupationType(aHISLCInsuredListSchema.getOccupationType());
    	aBPOLCInsuredSchema.setWorkType("");
    	aBPOLCInsuredSchema.setPluralityType("");
    	aBPOLCInsuredSchema.setSmokeFlag("");
    	aBPOLCInsuredSchema.setPostalAddress(aHISLCInsuredListSchema.getPostalAddress());
    	aBPOLCInsuredSchema.setZipCode(aHISLCInsuredListSchema.getZipCode());
    	aBPOLCInsuredSchema.setPhone(aHISLCInsuredListSchema.getPHONE());
    	aBPOLCInsuredSchema.setFax(aHISLCInsuredListSchema.getFax());
    	aBPOLCInsuredSchema.setMobile(aHISLCInsuredListSchema.getInsureMobile());
    	aBPOLCInsuredSchema.setEMail(aHISLCInsuredListSchema.getEmail());
    	aBPOLCInsuredSchema.setIDStartDate(aHISLCInsuredListSchema.getIDStartDate());
    	aBPOLCInsuredSchema.setIDEndDate(aHISLCInsuredListSchema.getIDEndDate());
    	aBPOLCInsuredSchema.setPosition(aHISLCInsuredListSchema.getposition());
    	aBPOLCInsuredSchema.setStature(aHISLCInsuredListSchema.getstature());
    	aBPOLCInsuredSchema.setAvoirdupois(aHISLCInsuredListSchema.getavoirdupois());
    	aBPOLCInsuredSet.add(aBPOLCInsuredSchema);
    	return aBPOLCInsuredSet;
    }
    
    private BPOLCPolSet getBPOLCPolSet(SYYBKWXLCInsuredListSchema aYBKWXLCInsuredListSchema,LCPolSchema aLCPolSchema,String aPrtNo){
    	BPOLCPolSet aBPOLCPolSet = new BPOLCPolSet();
    	
    		BPOLCPolSchema aBPOLCPolSchema = new BPOLCPolSchema();
    		aBPOLCPolSchema.setPolID("1");
    		aBPOLCPolSchema.setContID("1");
    		aBPOLCPolSchema.setPrtNo(aPrtNo);
    		aBPOLCPolSchema.setRiskCode(aLCPolSchema.getRiskCode());
    		aBPOLCPolSchema.setFamilyType("0");
    		aBPOLCPolSchema.setMainPolID("");
    		aBPOLCPolSchema.setInsuredID(aYBKWXLCInsuredListSchema.getInsureIDNo());
    		aBPOLCPolSchema.setRelationToAppnt(aYBKWXLCInsuredListSchema.getRelation());
    		aBPOLCPolSchema.setRelationToMainInsured("00");
    		aBPOLCPolSchema.setAppntID(aYBKWXLCInsuredListSchema.getIDNo());
    		aBPOLCPolSchema.setRelaId("");
    		aBPOLCPolSchema.setPolApplyDate(aYBKWXLCInsuredListSchema.getApplyDate());
    		//TODO
    		aBPOLCPolSchema.setCValiDate(aYBKWXLCInsuredListSchema.getEFFECTIVE_DATE());
    		aBPOLCPolSchema.setFirstTrialOperator("YBK");
    		aBPOLCPolSchema.setReceiveDate(CurrentDate);
    		aBPOLCPolSchema.setPayMode(aLCPolSchema.getPayMode());
    		aBPOLCPolSchema.setPayIntv(aYBKWXLCInsuredListSchema.getPayIntv());
    		aBPOLCPolSchema.setPrem(""+aLCPolSchema.getPrem());
    		aBPOLCPolSchema.setAmnt(""+aLCPolSchema.getAmnt());
    		aBPOLCPolSchema.setMult(""+aLCPolSchema.getMult());
    		aBPOLCPolSchema.setManageCom(aLCPolSchema.getManageCom());
    		aBPOLCPolSchema.setSaleChnl(aYBKWXLCInsuredListSchema.getSaleChnl());
    		aBPOLCPolSchema.setAgentCode(aYBKWXLCInsuredListSchema.getAgentCode());
    		aBPOLCPolSchema.setPayEndYear(aYBKWXLCInsuredListSchema.getPayEndYear());
    		aBPOLCPolSchema.setPayEndYearFlag(aYBKWXLCInsuredListSchema.getPayEndYearFlag());
    		aBPOLCPolSchema.setInsuYear(aYBKWXLCInsuredListSchema.getInsuYear());
    		aBPOLCPolSchema.setInsuYearFlag(aYBKWXLCInsuredListSchema.getInsuYearFlag());
    		aBPOLCPolSchema.setExPayMode(aLCPolSchema.getExPayMode());
    		aBPOLCPolSchema.setMult(aYBKWXLCInsuredListSchema.getMult());
        	aBPOLCPolSet.add(aBPOLCPolSchema);
    	
    	return aBPOLCPolSet;
    }
    
    
    //健康告知
    private BPOLCImpartSet getBPOIMPARTSet(SYYBKWXLCInsuredListSchema aYBKWXLCInsuredListSchema,LCPolSchema aLCPolSchema){
    	
    	//身高体重
    	String sqlImport= "select ImpartVer,ImpartCode,ImpartContent from LDImpart where impartcode ='010' and impartver ='001'";
    	SSRS mSSRS = new ExeSQL().execSQL(sqlImport);
    	BPOLCImpartSet aBPOLCImpartSet = new BPOLCImpartSet();
    	
    	BPOLCImpartSchema tBPOLCImpartSchema = new BPOLCImpartSchema();
		
    	tBPOLCImpartSchema.setContID(aYBKWXLCInsuredListSchema.getContNo());
    	tBPOLCImpartSchema.setCustomerNoType("I");
    	tBPOLCImpartSchema.setCustomerNo(aYBKWXLCInsuredListSchema.getContNo());
    	tBPOLCImpartSchema.setImpartVer(mSSRS.GetText(1, 1));
    	tBPOLCImpartSchema.setImpartCode(mSSRS.GetText(1, 2));
    	tBPOLCImpartSchema.setImpartParamModle(aYBKWXLCInsuredListSchema.getstature()+',' + aYBKWXLCInsuredListSchema.getavoirdupois());
    	tBPOLCImpartSchema.setDiseaseContent(aYBKWXLCInsuredListSchema.getstature()+',' + aYBKWXLCInsuredListSchema.getavoirdupois());
    	aBPOLCImpartSet.add(tBPOLCImpartSchema);
    
    	System.out.println("告知开始了险种"+aLCPolSchema.getRiskCode());
    	//TODO
    	//添加健康告知。
        	
    	return aBPOLCImpartSet;
    }
    
    public String getPrtnoTT(){
    	return this.PrtnoTT;
    }
    public static void main(String[] args) throws IOException
    {
    	/*GlobalInput tG = new GlobalInput();
    	tG.Operator = "sys";
    	tG.ManageCom = "86110000";
    	// 准备向后台传输数据 VData
    	TransferData transferData = new TransferData();
    	transferData.setNameAndValue("BatchNo", "P2015081520560401");
    	VData tVData = new VData();
    	tVData.add(tG);
    	tVData.add(transferData);
    	
    	SyybYBKWXCalCulateBL tYBKWXCalCulateBL = new SyybYBKWXCalCulateBL();
    	tYBKWXCalCulateBL.submitData(tVData, "");*/
    	String a ="0|3^122001^511001^520601";
    	String b [] = a.split("\\^");
    	System.out.println(b.toString());
    }
    
}
