/** 
 * 沈阳医保犹豫期退保BL类
 * @author yukun
 * @date 2017-07-27
 */
package com.sinosoft.httpclientybk.syybdeal;


import com.sinosoft.httpclientybk.dto.SY06.ENDORSEMENT_INFO;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.FeeNoticeVtsUI;
import com.sinosoft.lis.bq.FinanceDataBL;
import com.sinosoft.lis.bq.PEdorAppConfirmUI;
import com.sinosoft.lis.bq.PEdorConfirmBL;
import com.sinosoft.lis.bq.PEdorWTDetailUI;
import com.sinosoft.lis.bq.PGrpEdorCancelUI;
import com.sinosoft.lis.bq.PrtAppEndorsementBL;
import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SyybWTBL {

	//公共信息
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mEdorAcceptNo="";

	private  ENDORSEMENT_INFO mEdorSementInfo;

	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
	private LCContSchema mLCContSchema;
	public CErrors mErrors = new CErrors();
	
	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();
	
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mEdorSementInfo = (ENDORSEMENT_INFO) cInputData.getObjectByObjectName("ENDORSEMENT_INFO", 0);
		mLCContSchema = (LCContSchema) cInputData.getObjectByObjectName("LCContSchema", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("请传入参数信息错误");
			return false;
		}
		return true;
	}

	public boolean submit(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!deal()) {
			return false;
		}
		
		return true;
	}

	//业务处理
	private boolean deal() {		
		try {
			// 申请工单
			if (!createWorkNo()) {
				return false;
			}
			// 添加保全
			if (!addEdorItem()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORAPP")) {
					return false;
				}
				return false;
			}
			// 录入明细
			if (!saveDetail()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
			// 理算确认
			if (!appConfirm()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
			//生成批单
			if(!creatPrintVts()){
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
			// 保全确认
			if (!edorConfirm()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
		} catch (Exception e) {
			System.out.println("解约接口程序处理错误：撤销工单+"+e.getMessage());
			mErrors.addOneError("保全处理异常，请求失败。");
			cancelEdorItem("1", "I&EDORMAIN");
			cancelEdorItem("1", "I&EDORAPP");
			return false;
		}
		// 修改批单以及财务数据
		if (!endEdor()) {
			return false;
		}
		return true;
	}

	public LPEdorItemSchema getEdorItem() {
		return mLPEdorItemSchema;
	}
	
	//工单撤销
	private boolean cancelEdorItem(String edorstate, String transact) {
		PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		tVData.addElement(mGlobalInput);

		if ("I&EDORAPP".equals(transact)) {
			tLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorAppSchema.setEdorState(edorstate);

			String delReason = "";
			String reasonCode = "002";

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorAppSchema);
			// 准备传输数据 VData
			tVData.addElement(tTransferData);
		} else if ("I&EDORMAIN".equals(transact)) {
			tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
			tLPEdorMainSchema.setEdorState(edorstate);
			tLPEdorMainSchema.setContNo(mLCContSchema.getContNo());
			String delReason = "";
			String reasonCode = "002";
			System.out.println(delReason);

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorMainSchema);
			tVData.addElement(tTransferData);
		}
		try {
			// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			tPGrpEdorCancelUI.submitData(tVData, transact);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("撤销工单失败!");
			mErrors.addOneError("撤销工单失败" + e);
			return false;
		}
		return true;
	}

	//创建工单
	private boolean createWorkNo() {
		System.out.println(mLCContSchema.getAppntName());
		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(mLCContSchema.getAppntNo());
		//工单类型 03保全
		tLGWorkSchema.setTypeNo("03");
		tLGWorkSchema.setContNo(mLCContSchema.getContNo());
		//tLGWorkSchema.setCustomerName(mLCContSchema.getAppntName());
		//工单申请人类型0为投保人
		tLGWorkSchema.setApplyTypeNo("0");
		//受理途径 微信WeChat
		tLGWorkSchema.setAcceptWayNo("WX");
		tLGWorkSchema.setAcceptDate(mCurrDate);
		tLGWorkSchema.setRemark("沈阳医保线上WT十日退保接口生成");
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			System.out.println("生成保全工单失败!");
			mErrors.addOneError("生成保全工单失败" + tTaskInputBL.mErrors);
			return false;
		}
		mEdorAcceptNo = tTaskInputBL.getWorkNo();
		System.out.println("mEdorAcceptNo======"+mEdorAcceptNo);
		return true;
	}
	
	//添加保全项目
	private boolean addEdorItem() {
		// 校验能否添加保全项目
		if (!checkEdorItem()) {
			return false;
		}
		MMap map = new MMap();
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorAppNo(mEdorAcceptNo);
        tLPEdorMainSchema.setContNo(mLCContSchema.getContNo());
        tLPEdorMainSchema.setEdorAppDate(mCurrDate);
        tLPEdorMainSchema.setEdorValiDate(mCurrDate);
        tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INIT);
        tLPEdorMainSchema.setUWState(BQ.UWFLAG_INIT);
        tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorMainSchema.setMakeDate(PubFun.getCurrentDate());
        tLPEdorMainSchema.setMakeTime(PubFun.getCurrentTime());
        tLPEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
        tLPEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLPEdorMainSchema, "INSERT");

        mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
        mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
        mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
        mLPEdorItemSchema.setDisplayType("1");
        mLPEdorItemSchema.setEdorType(BQ.EDORTYPE_WT);
        mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPEdorItemSchema.setContNo(mLCContSchema.getContNo());
        mLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
        mLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
        mLPEdorItemSchema.setEdorValiDate(mCurrDate);
        mLPEdorItemSchema.setEdorAppDate(mCurrDate);
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
		mLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
		mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLPEdorItemSchema, "INSERT");
        if (!submit(map))
        {
            return false;
        }
        return true;
	}

	//明细录入
	private boolean saveDetail() {	
	    LPPolSet mLPPolSet= new LPPolSet();
	    String polnosql="select polno from lcpol where contno='"+mLCContSchema.getContNo()+"'";
	    SSRS tSSRS = new ExeSQL().execSQL(polnosql);
    	String polno=tSSRS.GetText(1,1);
    	System.out.println("polno======="+polno);
        LPPolSchema tLPPolSchema=new LPPolSchema();
        tLPPolSchema.setEdorNo(mEdorAcceptNo);
        tLPPolSchema.setEdorType(BQ.EDORTYPE_WT);
        tLPPolSchema.setContNo(mLCContSchema.getContNo());
        tLPPolSchema.setInsuredNo(mLCContSchema.getInsuredNo());
        tLPPolSchema.setPolNo(polno);
        mLPPolSet.add(tLPPolSchema);
		
		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		tLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
		tLPEdorItemSchema.setContNo(mLCContSchema.getContNo());
		tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_WT);
		tLPEdorItemSchema.setReasonCode("070");
		
		VData tVData = new VData();
		tVData.add(mGlobalInput);
		tVData.add(tLPEdorItemSchema);
		tVData.add(mLPPolSet);
		
		//生成批单
	    PEdorWTDetailUI tPEdorWTDetailUI   = new PEdorWTDetailUI();  
	    if (!tPEdorWTDetailUI.submitData(tVData, "")){
	    	System.out.println("生成保全服务批单失败！");
			mErrors.addOneError("生成保全服务批单失败！");
			return false;
		}
		return true;
	}

	/**
     * 产生打印数据
     * @return boolean
     */
    private boolean creatPrintVts()
    {
        //生成打印数据
        VData data = new VData();
        data.add(mGlobalInput);
        PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(mEdorAcceptNo);
        if (!tPrtAppEndorsementBL.submitData(data, ""))
        {
        	System.out.println("数据保存成功！但没有生成保全服务批单！");
            mErrors.addOneError("数据保存成功！但没有生成保全服务批单！");
            return false;
        }
        return true;
    }

	//理算确认
	private boolean appConfirm() {
		//保全理算
	    PEdorAppConfirmUI tPEdorAppConfirmUI = new PEdorAppConfirmUI(mGlobalInput, mEdorAcceptNo);
	    if (!tPEdorAppConfirmUI.submitData())
	    {
	    	System.out.println("保全理算失败！");
	    	mErrors.addOneError("保全理算失败！");
	    	return false;
	    }
	    System.out.println("保全理算成功！");
		return true;
	}
	
	//犹豫期退保校验
	private boolean checkEdorItem() {
		String mContNo=mLCContSchema.getContNo();
		//正在操作保全的无法添加犹豫期退保
		String edorSQL = " select edoracceptno from lpedoritem where contno='"
				+ mContNo
				+ "' and edoracceptno!='"+ mEdorAcceptNo+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";
		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			System.out.println("保单正在做保全,无法在做"+edorFlag+"保全!");
			mErrors.addOneError("保单" + mContNo + "正在操作保全，工单号为：" + edorFlag + "无法做WT");
			return false;
		}
		// 只有承保有效状态下的保单才能犹豫期退保
		if (!"1".equals(mLCContSchema.getAppFlag())) {
			System.out.println("保单未签单不能申请退保!");
			mErrors.addOneError("保单未签单不能申请退保!");
			return false;
		}
		String cardFlag = mLCContSchema.getCardFlag();
		String saleChnl = mLCContSchema.getSaleChnl();
		if(null==cardFlag || "".equals(cardFlag)){
			System.out.println("保单类型CARDFLAG为空!");
			mErrors.addOneError("保单类型CARDFLAG为空!");
			return false;
		}		
		return true;
	}

	/**
	 * 保全确认
	 * 
	 */
	private boolean edorConfirm() {
		MMap map = new MMap();

		// 生成财务数据
		FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput,
				mEdorAcceptNo, BQ.NOTICETYPE_P, "");
		if (!tFinanceDataBL.submitData()) {
			mErrors.addOneError("生成财务数据错误" + tFinanceDataBL.mErrors);
			return false;
		}

		// 个人保全结案
		PEdorConfirmBL tPEdorConfirmBL = new PEdorConfirmBL(mGlobalInput,
				mEdorAcceptNo);
		map = tPEdorConfirmBL.getSubmitData();
		if (map == null) {
			mErrors.addOneError("保全结案发生错误" + tFinanceDataBL.mErrors);
			return false;
		}

		// 工单结案
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
		tLGWorkSchema.setTypeNo("03"); // 结案状态

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		MMap tmap = tTaskAutoFinishBL.getSubmitData(data, "");
		if (tmap == null) {
			mErrors.addOneError("工单结案失败" + tFinanceDataBL.mErrors);
			return false;
		}
		map.add(tmap);
		if (!submit(map)) {
			return false;
		}
		return true;	
	}
	
	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			System.out.println("提交数据库发生错误！");
			mErrors.addOneError("提交数据库发生错误" + tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

	//缴费方式校验
	private boolean endEdor() {
		String mPayMode = mLCContSchema.getPayMode();
		String mAccNo = mLCContSchema.getBankAccNo();
		String mAccName = mLCContSchema.getAccName();
		String mBankCode = mLCContSchema.getBankCode();
		String mBankAccNo = mLCContSchema.getBankAccNo();
		System.out.println("mPayMode="+mPayMode);
		System.out.println("mAccNo="+mAccNo);
		System.out.println("mAccName="+mAccName);
		System.out.println("mBankCode="+mBankCode);
		System.out.println("mBankAccNo="+mBankAccNo);
		String mPayDate = mCurrDate;
		//若选择的付费方式为银行转账
		if("4".equals(mLCContSchema.getPayMode())){
			mAccNo=mLCContSchema.getBankAccNo();
			mBankCode=mLCContSchema.getBankCode();
			mBankAccNo=mLCContSchema.getBankAccNo();
			if(null==mAccNo || null==mBankCode || null==mBankAccNo){
				System.out.println("保单账户信息有空!");
			}
		}
		
		System.out.println("交退费通知书" + mEdorAcceptNo);
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("payMode", mPayMode);
		tTransferData.setNameAndValue("endDate", "");
		tTransferData.setNameAndValue("payDate", mPayDate);
		tTransferData.setNameAndValue("bank", mBankCode);
		tTransferData.setNameAndValue("bankAccno", mBankAccNo);
		tTransferData.setNameAndValue("accName", mAccName);
		tTransferData.setNameAndValue("chkYesNo", "no");

		// 生成交退费通知书
		FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI(mEdorAcceptNo);
		if (!tFeeNoticeVtsUI.submitData(tTransferData)) {
			System.out.println("生成批单失败！");
			mErrors.addOneError("生成批单失败！原因是：" + tFeeNoticeVtsUI.getError());
		}

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tTransferData);
		SetPayInfo spi = new SetPayInfo(mEdorAcceptNo);
		if (!spi.submitDate(data, "0")) {
			System.out.println("设置收退费方式失败！");
			mErrors.addOneError("设置收退费方式失败！原因是：" + spi.mErrors.getFirstError());
		}

		return true;
	}
	
	public static void main(String[] args){
		GlobalInput tGI = new GlobalInput();
		tGI.ManageCom = "86";
		tGI.Operator = "001";
		
		
		String sql="select contno from lccont where prtno='162017030777'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);
		String contno = tSSRS.GetText(1, 1);
		System.out.println(contno);
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(contno);
		LCContSchema mLCContSchema = new LCContSchema();
		LCContSet tLCContSet = tLCContDB.query();
		mLCContSchema=tLCContSet.get(1);
		
		VData tVDate = new VData();
		tVDate.add(tGI);
		tVDate.add(mLCContSchema);
		System.out.println("--------------"+mLCContSchema.getAppntName());
		SyybWTBL tYbkWTBL = new SyybWTBL();
		tYbkWTBL.submit(tVDate);
	}

}
