package com.sinosoft.httpclientybk.syybdeal;

import com.sinosoft.lis.cbcheck.ApplyRecallPolUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCApplyRecallPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SyybPolicyCancle implements Runnable
{
	CError mErrors = new CError();
	//撤单功能
		public boolean policyCancle() {
			String SQL = "select lc.prtno,lc.cardflag,lc.managecom  from lccont lc,lcpol lp,ljtempfee lj "
					+ " where lc.prtno=lp.prtno and lj.otherno=lc.prtno and lc.prtno like 'SYW%'  and lc.appflag !='1' and lc.uwflag not in ('1','5','a') and lc.makedate<=current date and lc.managecom='86210100' "
					+ " and not exists(select 1 from LCApplyRecallPol where prtno=lc.prtno) and lj.enteraccdate is null and lj.confmakedate is null  with ur";
			GlobalInput tG = new GlobalInput();
			LCApplyRecallPolSchema tLCApplyRecallPolSchema = new LCApplyRecallPolSchema();
			TransferData tTransferData = new TransferData();
			VData tVData = new VData();
			String tPrtNo;
			String tCardFlag;
			String tErrorInfo;
			String tMagCom;
			SSRS tSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			tSSRS = tExeSQL.execSQL(SQL);

			if (tSSRS != null && tSSRS.MaxRow > 0) {
				tPrtNo = tSSRS.GetText(1, 1);
				tCardFlag = tSSRS.GetText(1, 2);
				tMagCom = tSSRS.GetText(1, 3);
				tG.ManageCom = tMagCom;
				tG.Operator = "YBK";
				tTransferData.setNameAndValue("CardFlag", tCardFlag);
				// 补充附加险表
				tLCApplyRecallPolSchema.setRemark("沈阳医保平台由于当天未进行付款缴费、撤单操作");
				tLCApplyRecallPolSchema.setPrtNo(tPrtNo);
				tLCApplyRecallPolSchema.setApplyType("3");
				tLCApplyRecallPolSchema.setManageCom(tMagCom);
				tLCApplyRecallPolSchema.setOperator("YBK");
				// 准备传输数据 VData
				tVData.add(tLCApplyRecallPolSchema);
				tVData.add(tTransferData);
				tVData.add(tG);
				try {
					ApplyRecallPolUI applyRecallPolUI = new ApplyRecallPolUI();
					if (applyRecallPolUI.submitData(tVData, "")) {
						System.out.println("沈阳实时撤单成功~~~");
					} else {
						CErrors  mCErrors = applyRecallPolUI.mErrors;
						System.out.println("实时撤单失败：" + mCErrors.getFirstError() + "---印刷号---" + tPrtNo);
					}
				} catch (Exception e) {
					System.out.println("沈阳批量撤单失败异常");
					e.printStackTrace();
				}
				return true;
			} else {
				System.out.println("未查询到可以撤单的保单!!!");
				return false;
			}
		}
	public void run()
	{
		// TODO Auto-generated method stub
		new SyybPolicyCancle().policyCancle();
		
	}
	public static void main(String[] args){
		new SyybPolicyCancle().policyCancle();
	}
}
