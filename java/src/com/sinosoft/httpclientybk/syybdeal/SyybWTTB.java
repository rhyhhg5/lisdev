/*
 * SY06沈阳医保犹豫期退保接口
 * yukun 2017-07-26新增
 * 
 * */
package com.sinosoft.httpclientybk.syybdeal;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.SY06.ENDORSEMENT_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SyybWTTB extends ABusLogic {
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	/*报文类型*/
	public String MsgType = "";
	/*核心保单号 */
	private String tContNo = "";
	/*印刷号 */
	private String tPrtNo = "";
	/*任务号 */
	private String TASK_NO = "";
	/*返回报文类型 */
	private String REQUEST_TYPE = "";
	/*工单号*/
	private String mEdorAcceptNo = "";
	/*缴费方式*/
	private String tPayMode = "";
	
	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();
	
	/*报文体*/
	private ENDORSEMENT_INFO mEdorSementInfo = new ENDORSEMENT_INFO();
	
	private LCContSchema mLCContSchema = new LCContSchema();
	VData tVData = new VData();
	TransferData tTransferData = new TransferData();

	public SyybWTTB() {
	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return false;
	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理沈阳医保犹豫期退保保全业务");
		mGlobalInput.ManageCom = "86210100";
		mGlobalInput.ComCode = "86210100";
		mGlobalInput.Operator = "pa0001";
		//解析XML
		if (!parseXML(cMsgInfos)) {
			return false;
		}
		//数据校验
		if(!checkData()){
			System.out.println("数据校验失败---");
			return false;
		}
		//数据封装
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(mEdorSementInfo);
		data.add(mLCContSchema);
		//业务处理
		SyybWTBL tSyybWTBL = new SyybWTBL();
		if (!tSyybWTBL.submit(data)) {
			System.out.println("保全操作错误");
			String errorMsg = tSyybWTBL.mErrors.getFirstError();
			errLogSY(errorMsg, "E");	
        	return false;
		} else {
        	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        	tLPEdorItemSchema =tSyybWTBL.getEdorItem(); 
        	this.mEdorAcceptNo = tLPEdorItemSchema.getEdorAcceptNo();
        	errLogSY("保全处理成功","1");
		}
		// 更新实付表
		if(!updateDate()){
			System.out.println("实收表更新操作错误");
			String errorMsg = tSyybWTBL.mErrors.getFirstError();
			errLogSY(errorMsg, "E");	
			return false;
		}else{
			System.out.println("实收表更新操作错误");
			errLogSY("实收表更新操作成功","1");
			return true;
		}
	}
	/*
	 * 报文数据解析
	 * */
	private boolean parseXML(MsgCollection cMsgInfos) {
		System.out.println("开始解析XML");
		// 获取保单信息
		List<ENDORSEMENT_INFO> tEdorInfoList = cMsgInfos.getBodyByFlag("BODY");
		if (tEdorInfoList == null || tEdorInfoList.size() != 1) {
			errLogSY("请求报文信息获取失败!", "0");
			return false;
		}
		mEdorSementInfo = tEdorInfoList.get(0); // 接口一次只接受一个保单的犹豫期退保
		// 获取头信息
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		TASK_NO = tMsgHead.getTASK_NO();
		if("".equals(TASK_NO)||null == TASK_NO||"null".equals(TASK_NO)){
			errLogSY("task_no不能为空!", "0");
			return false;
		}
		REQUEST_TYPE = tMsgHead.getREQUEST_TYPE();
		System.out.println("TASK_NO = " + TASK_NO);
		System.out.println("REQUEST_TYPE = " + REQUEST_TYPE);
		//获取contno
		String tSYMedicalTempfeeno = mEdorSementInfo.getPAYMENTNO();
		String contnoSQL = "select prtno from lccontsub where SYMedicalTempfeeno = '"+tSYMedicalTempfeeno+"' with ur";
		SSRS tSSRS1 = new ExeSQL().execSQL(contnoSQL);
		if (tSSRS1 == null || "".equals(tSSRS1)) {
			errLogSY("ContNo获取失败!", "0");
			return false;
		}
		this.tPrtNo = tSSRS1.GetText(1, 1);
		System.out.println("tPrtNo=" + tPrtNo);
		// 查询保单信息
		LCContDB tLCContDB = new LCContDB();
		if (tPrtNo == null || "".equals(tPrtNo)) {
			errLogSY("tPrtNo获取失败!", "0");
			return false;
		}
		tLCContDB.setPrtNo(tPrtNo);
		if (!tLCContDB.getInfo()) {
			errLogSY("传入的缴费单号错误，核心系统不存在保单.保单:“" + tContNo + "”", "0");
			return false;
		}
		mLCContSchema=tLCContDB.getSchema();
//		LCContSet tLCContSet = tLCContDB.query();
//		mLCContSchema=tLCContSet.get(1);
		//增加并发控制，同一个保单只能请求一次
    	MMap tCekMap = null;
    	tCekMap = lockLGWORK(mLCContSchema);
    	if (tCekMap == null)
    	{
    		errLogSY("保单"+mLCContSchema.getContNo()+"正在进行解约，请不要重复请求","3");	
    		return false;
    	}
    	if(!submit(tCekMap)){
    		errLogSY("保单"+mLCContSchema.getContNo()+"正在进行解约，请不要重复请求","3");	
    		return false;
    	}
		//根据prtno向日志表添加数据
    	String sql = "select contno from lccont where prtno = '"+tPrtNo+"' with ur";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	this.tContNo = tSSRS.GetText(1, 1);
		System.out.println("tContNo=" + tContNo);
		SetPrtNo(tPrtNo);
		return true;
	}
	/*
	 * 报文数据检测及犹豫期判断
	 * */
	private boolean checkData() {
		/** 对保全申请信息的校验 */
		if (null == mEdorSementInfo.getMEDICALNO()
				|| "".equals(mEdorSementInfo.getMEDICALNO())) {
			errLogSY("缴费单号不能为空", "0");
			return false;
		}
		if (null == mEdorSementInfo.getPAYMENTNO()
				|| "".equals(mEdorSementInfo.getPAYMENTNO())) {
			errLogSY("医保个人编号不能为空", "0");
			return false;
		}
		return true;
	}
	
	//退费更新实收表
	private boolean updateDate(){
		//医保扣款已经成功,更新实收表
		String sqlLjaget = "";
		String sqlLjfiget1 = "";
		String sqlljagetendorse1 = "";
		String tBankcode = "";
		String tActuGetNo = "";
		String tGetMoney = "";
		MMap tmap = new MMap();
		//获取tActuGetNo
		String sqltemp = "select ActuGetNo from ljaget where otherno='" + mEdorAcceptNo + "' and enteraccdate is null";
		SSRS ActuGetNoSSRS = new ExeSQL().execSQL(sqltemp);	
		if (ActuGetNoSSRS==null || "".equals(mEdorAcceptNo)) {
			System.out.println("ActuGetNo获取失败!");
			errLogSY("ActuGetNo获取失败!","0");
			return false;
		} else {
			tActuGetNo = ActuGetNoSSRS.GetText(1, 1);
			System.out.println("======tActuGetNo========" + tActuGetNo);
		}	
		//获取退保金
		String sqlGetMoney = "select getmoney from LJAGetEndorse where ActuGetNo = '"+tActuGetNo+"' and FeeOperationType = 'WT' and Endorsementno = '"+mEdorAcceptNo+"' with ur";
		SSRS GetMoneySSRS = new ExeSQL().execSQL(sqlGetMoney);
		if (GetMoneySSRS==null || "".equals(GetMoneySSRS)) {
			System.out.println("退保金获取失败!");
			errLogSY("退保金获取失败!","0");
			return false;
		} else {
			tGetMoney = GetMoneySSRS.GetText(1, 1);
			System.out.println("======tGetMoney========" + tGetMoney);
		}	
		//获取银行编码及扣费方式
		String bankcodeSQL = "select bankcode,paymode from lbcont where edorno = '" + mEdorAcceptNo + "'";
		SSRS tBankcodeSSRS = new ExeSQL().execSQL(bankcodeSQL);	
		if (tBankcodeSSRS == null || tBankcodeSSRS.getMaxRow() <= 0) {
			System.out.println("银行编码获取失败！");
			mErrors.addOneError("银行编码获取失败！");
			return false;
		} else {
			tBankcode = tBankcodeSSRS.GetText(1, 1);
			tPayMode = tBankcodeSSRS.GetText(1, 2);
			System.out.println("======tBankcode========" + tBankcode);
			System.out.println("======tPayMode========" + tPayMode);
		}
		if("8".equals(tPayMode)){
			//1,更新ljaget表的数据
			sqlLjaget = " update ljaget " 
					+" set PayMode='8',EnterAccDate ='"+ mCurrDate 
					+"' ,ConfDate ='"+ mCurrDate 
					+ "',bankcode = '"+ tBankcode + "'"
					+",ModifyDate= current date,ModifyTime=current time where Actugetno='" + tActuGetNo + "'";
		
			//2,向ljfiget表插入数据
			String OtherNoSql = " select OtherNo,OtherNoType,AgentType," 
					+" AgentCom,AgentGroup,AgentCode,drawer,drawerid,shoulddate" 
					+" ,bankcode,salechnl,accname,bankaccno,managecom " 
					+" from ljaget " 
					+" where ActuGetNo = '" + tActuGetNo + "'"; 
			SSRS result =  new ExeSQL().execSQL(OtherNoSql);

			sqlLjfiget1 = "insert into ljfiget" 
					+" (ActuGetNo,PayMode,PolicyCom,OtherNo,OtherNoType,GetMoney,EnterAccDate," 
					+" Operator,MakeTime,MakeDate,ModifyDate,ModifyTime,AgentType,AgentCom,AgentGroup," 
					+" AgentCode,drawer,drawerid,shoulddate,bankcode,confmaketime,confmakedate,confdate," 
					+" salechnl,bankaccno,accname,managecom) " 
					+" values ('" + tActuGetNo + "','8','" + result.GetText(1, 14) + "','" + result.GetText(1, 1) + "','" + result.GetText(1, 2) 
					+ "','" + tGetMoney + "','" + mCurrDate 
					+ "','" + mGlobalInput.Operator + "','" + mCurrTime + "','" + mCurrDate + "','" + mCurrDate 
					+ "','" + mCurrTime + "','" + result.GetText(1, 3) + "','" + result.GetText(1, 4) + "','" + result.GetText(1, 5)
					+ "','" + result.GetText(1, 6) + "','" + result.GetText(1, 7) + "','" + result.GetText(1, 8) + "','" + mCurrDate + "','" + result.GetText(1, 9)
					+ "','" + PubFun.getCurrentTime() + "','" + mCurrDate + "','" + mCurrDate + "','" + result.GetText(1, 11) + "','" + result.GetText(1, 13) + "','" + result.GetText(1, 12) + "','" + result.GetText(1, 14) + "')";
			
			//3,更新LJAGetEndorse表数据
			sqlljagetendorse1 = "update LJAGetEndorse set GetConfirmDate = '" + mCurrDate + "',EnterAccDate = '" + mCurrDate + "',ModifyDate = current date,ModifyTime = current time where Actugetno='" + tActuGetNo + "'";
			tmap.put(sqlLjaget, "UPDATE");
			tmap.put(sqlLjfiget1, "INSERT");
			tmap.put(sqlljagetendorse1, "UPDATE");
		}else if("4".equals(tPayMode)){
			sqlLjaget = "update ljaget set PayMode='4',bankcode = '" + tBankcode + "',ModifyDate= current date,ModifyTime=current time where ActuGetNo = '"+ tActuGetNo +"'";
			tmap.put(sqlLjaget, "UPDATE");
			System.out.println("医保卡退费失败,等待核心银行卡支付");
			errLogSY("等待核心银行卡支付","1");
		}else{
			errLogSY("扣款方式出错","0");
	        return false;
		}
		VData mInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		mInputData.clear();
		mInputData.add(tmap);
		if(!tPubSubmit.submitData(mInputData, "")){
			System.out.println("保存数据出错");
			CError.buildErr(this, "数据保存失败");
			System.out.println("系统接收失败");
			errLogSY("系统接收失败","0");
	        return false;
		}else{
			System.out.println("退款已经成功");
			errLogSY("退款已经成功","1");
			return true;
		}
	}
	
	/*
     * 锁定动作
     * */
    private MMap lockLGWORK(LCContSchema cLCContSchema)
    {
    	//犹豫期、解约接口都用WT来校验锁，以免保单同时申请WT和CT
        MMap tMMap = null;
        /**犹豫期退保锁定标志"WT"*/
        String tLockNoType = "WT";
        /**锁定时间*/
        String tAIS = "0";//10分钟的锁
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            return null;
        }
        return tMMap;
    }  
    /*
     * 提交数据到数据库
     * */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			errLogSY("提交数据库发生错误" + tPubSubmit.mErrors, "E");
			return false;
		}
		return true;
	}
}
