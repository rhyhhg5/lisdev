package com.sinosoft.httpclientybk.syybdeal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.SYTB01.INSURE_INFO;
import com.sinosoft.httpclientybk.dto.SYTB01.SUCCESS_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.SYYBKWXLCInsuredListSchema;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vdb.LCPolDBSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

public class SyybTB extends ABusLogic {

	private static final String STATIC_ActivityID = "0000001001";

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public VData mVData = new VData();
	private PubSubmit tPubSubmit = new PubSubmit();
	public TransferData mTransferData = new TransferData();
	public String mError = "";// 处理过程中的错误信息
	public String MsgType;

	public String AppntNo = "";
	public String InsuredNo = "";
	public String MedicalCode = "";
	public String MedicalMoney = "";
	public String PayMode = "";
	public String WrapCode = "";
	public String WrapName = "";
	public String WrapType = "";


	private String PayIntv = "";
	private String IfAutoPay = "";
	private String RenemalPayMethod = "";
	private String YBKSerialno = "";
	private String tPrtNo = "";
	String ContNo = "";

	public List INSURE_INFO_List;
	public String mMissionid;
	public INSURE_INFO mINSURE_INFO = new INSURE_INFO();
	
	SYYBKCalCulateFee mSYYBKCalCulateFee = new SYYBKCalCulateFee();

	private SUCCESS_INFO mSUCCESS_INFO = new SUCCESS_INFO();
	// 被保人信息等
	SYYBKWXLCInsuredListSchema mYBKWXLCInsuredListSchema = new SYYBKWXLCInsuredListSchema();
	// 险种信息
	LCPolSchema mLCPolSchema = new LCPolSchema();
	// 转换的SQL
	String Sql = "";
	SSRS mSSRS = null;
	String flagState = "";
	protected boolean  flag =false;
	Date aNow = null;
	Date Birthday = null;
	String errors = "";
	

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理沈阳YBK微信出单标准平台投保过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		MsgType = tMsgHead.getREQUEST_TYPE();
		mGlobalInput.Operator = "YBK";
		mGlobalInput.ComCode = "86";
		mGlobalInput.ManageCom = "86";
		try {
			System.out.println("进来了");
			INSURE_INFO_List = cMsgInfos.getBodyByFlag("BODY");
			mINSURE_INFO = (INSURE_INFO) INSURE_INFO_List.get(0);
			if (INSURE_INFO_List == null || INSURE_INFO_List.size() != 1) {
				errLogSY("获取沈阳医保投保报文信息失败", "0");
				return false;
			}

			// 将XML装换成DBSchema
			boolean chFlag = XmlToSchemaInfo();
			if (!chFlag) {
				errLogSY(errors, "0");
				return false;
			}
		
			String nolimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
			String strProposalContNo = PubFun1.CreateMaxNo("ProposalContNo",nolimit);
			
			mTransferData.setNameAndValue("mAppntNo",AppntNo);
			mTransferData.setNameAndValue("mInsuredNo",InsuredNo);
		    mTransferData.setNameAndValue("strProposalContNo",strProposalContNo);
		    mTransferData.setNameAndValue("tPrtNo",tPrtNo);
			mTransferData.setNameAndValue("mLCPolSchema", mLCPolSchema);// 险种
			mTransferData.setNameAndValue("mYBKWXLCInsuredListSchema",mYBKWXLCInsuredListSchema);// 被保人
			mTransferData.setNameAndValue("RenemalPayMethod", RenemalPayMethod);// 续保扣款方式
			mTransferData.setNameAndValue("IfAutoPay", IfAutoPay);// 是否自动续保
			mTransferData.setNameAndValue("MedicalCode", MedicalCode);// 医保卡号
			mTransferData.setNameAndValue("PayMode", PayMode);// 扣款方式
			mTransferData.setNameAndValue("MedicalMoney", MedicalMoney);// 医保账户余额
			mTransferData.setNameAndValue("YBKSerialno", YBKSerialno);// 流水号
			mTransferData.setNameAndValue("WrapCode", WrapCode);// 套餐产品编码-->
			mTransferData.setNameAndValue("MsgHead", tMsgHead);
			mTransferData.setNameAndValue("PayEndYear", mINSURE_INFO.getPAY_END_YEAR());
			mVData.add(mGlobalInput);
			mVData.add(mTransferData);
			SyybYBKWXCalCulateBL mYBKWXCalCulateBL = new SyybYBKWXCalCulateBL();
			boolean flag = mYBKWXCalCulateBL.submitData(mVData, "YBK");

			if (flag) {
				if (!PrepareInfo()) {
					errLogSY(mError, "0");
					SetPrtNo(tPrtNo);
					return false;
				}
				getXmlResult();
				SetPrtNo(tPrtNo);

			} else {
				// 生成数据异常
				errLogSY(mYBKWXCalCulateBL.mErrors.getFirstError(), "0");
				DeleteCont();
			}

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
	public void DeleteCont(){
		System.out.println("删除新单开始，Prtno为：" + tPrtNo);
		if(!"".equals(tPrtNo)||null==tPrtNo){
			VData tVData = new VData();
			String tDeleteReason = "pad出单新单复核失败需要进行新单删除";
			TransferData tTransferData = new TransferData();
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setPrtNo(tPrtNo);
			LCContSchema tLCContSchema = new LCContSchema();
			LCContSet tLCContSet = tLCContDB.query();
			if (tLCContSet.size() > 0 && !"1".equals(tLCContSet.get(1).getAppFlag())) {
				tLCContSchema = tLCContSet.get(1);
				tTransferData
						.setNameAndValue("DeleteReason", tDeleteReason);
				tVData.add(tLCContSchema);
				tVData.add(tTransferData);
				tVData.add(mGlobalInput);
				ContDeleteUI tContDeleteUI = new ContDeleteUI();
				tContDeleteUI.submitData(tVData, "DELETE");
				System.out.println("删除新单结束");
			}
		}	
	} 

	public boolean XmlToSchemaInfo() {
		
		 
		 //1、APP投保订单号
		YBKSerialno = mINSURE_INFO.getPOLICY_FORMER_NO();
		mYBKWXLCInsuredListSchema.setSerNo(YBKSerialno);
		
		// 2、投保人姓名
		mYBKWXLCInsuredListSchema.setName(mINSURE_INFO.getNAME());
		mYBKWXLCInsuredListSchema.setAccName(mINSURE_INFO.getNAME());
		//3、投保人性别
		mYBKWXLCInsuredListSchema.setSex(mINSURE_INFO.getGENDER());
		//4、投保人出生日期
		mYBKWXLCInsuredListSchema.setBirthDay(mINSURE_INFO.getBIRTHDAY());
		//5、投保人证件类型
		mYBKWXLCInsuredListSchema.setIDType(mINSURE_INFO.getCERTIFICATE_TYPE());// 身份证
		//6、投保人证件号码
		mYBKWXLCInsuredListSchema.setIDNo(mINSURE_INFO.getCERTIFICATE_NO());
		//7、投保人手机号
		mYBKWXLCInsuredListSchema.setMobile(mINSURE_INFO.getMOBILE());
		//医保卡号
		MedicalCode = mINSURE_INFO.getMEDICAL_NO();
		mYBKWXLCInsuredListSchema.setMedicalNo(mINSURE_INFO.getMEDICAL_NO());
		//银行卡号
		mYBKWXLCInsuredListSchema.setBankAccNo(mINSURE_INFO.getBANK_NO());
		//所属银行 所属银行没有存
		//TODO
		String BankName = mINSURE_INFO.getBANK_NAME();
		//银行编码
		mYBKWXLCInsuredListSchema.setBankCode(mINSURE_INFO.getBANK_CODE());
		//投保人详细地址
		mYBKWXLCInsuredListSchema.setPostalAddress(mINSURE_INFO.getADDRESS());
		//投保人邮箱
		mYBKWXLCInsuredListSchema.setEmail(mINSURE_INFO.getEMAIL());
		//与被保人关系
		mYBKWXLCInsuredListSchema.setRelation(mINSURE_INFO.getRELATION_INSURED());
		//被保人姓名
		mYBKWXLCInsuredListSchema.setInsuredName(mINSURE_INFO.getINSUED_NAME());
		//被保人性别
		mYBKWXLCInsuredListSchema.setInsureSex(mINSURE_INFO.getINSUED_GENDER());
		//被保人出生日期
		mYBKWXLCInsuredListSchema.setInsureBirthday(mINSURE_INFO.getINSUED_BIRTHDAY());
		//被保人证件类型
		mYBKWXLCInsuredListSchema.setInsureIDType(mINSURE_INFO.getINSUED_CERTIFICATE_TYPE());
		//被保人证件号码
		mYBKWXLCInsuredListSchema.setInsureIDNo(mINSURE_INFO.getINSUED_CERTIFICATE_NO());
		//被保人手机号
		mYBKWXLCInsuredListSchema.setInsureMobile(mINSURE_INFO.getINSUED_MOBILE());
		//险种编码
		mLCPolSchema.setRiskCode(mINSURE_INFO.getRISK_CODE());
		//套餐编码
		WrapCode = mINSURE_INFO.getWRAP_CODE();
		//套餐名称
		WrapName = mINSURE_INFO.getWRAP_NAME();
		//产品款别
		WrapType = mINSURE_INFO.getWRAP_TYPE();
		//档次
		mYBKWXLCInsuredListSchema.setMult("");
		//保障年限
		mYBKWXLCInsuredListSchema.setInsuYear(mINSURE_INFO.getINSUREYEAR_FLAG());
		//缴费年限
		mYBKWXLCInsuredListSchema.setPayEndYear(mINSURE_INFO.getPAY_END_YEAR());
		//缴费频次
		PayIntv = mINSURE_INFO.getPAY_INTV();
		mYBKWXLCInsuredListSchema.setPayIntv(mINSURE_INFO.getPAY_INTV());
		//扣款方式
		PayMode = mINSURE_INFO.getDEDUCT_METHOD();
		mLCPolSchema.setPayMode(mINSURE_INFO.getDEDUCT_METHOD());
		//医保账户余额
		MedicalMoney = mINSURE_INFO.getMEDICAL_MONEY();
		mYBKWXLCInsuredListSchema.setMedicalMoney(mINSURE_INFO.getMEDICAL_MONEY());
		//投保申请日期
		mYBKWXLCInsuredListSchema.setApplyDate(mINSURE_INFO.getAPPLICATION_DATE());
		mLCPolSchema.setPolApplyDate(mINSURE_INFO.getAPPLICATION_DATE());
		//保险生效日期
		mYBKWXLCInsuredListSchema.setEFFECTIVE_DATE(mINSURE_INFO.getEFFECTIVE_DATE());
		//保险终止日期
		mYBKWXLCInsuredListSchema.setEXPIRE_DATE(mINSURE_INFO.getEXPIRE_DATE());
		//是否自动续保
		mLCPolSchema.setRnewFlag(mINSURE_INFO.getIF_AUTO_RENEWAL());// 0否1是
		IfAutoPay = mINSURE_INFO.getIF_AUTO_RENEWAL();
		//续保扣款方式
		mLCPolSchema.setExPayMode(mINSURE_INFO.getRENEWAL_DEDUCT_METHOD());
        RenemalPayMethod = mINSURE_INFO.getRENEWAL_DEDUCT_METHOD();
		//保额
        mLCPolSchema.setAmnt(mINSURE_INFO.getSUM_INSURED());
        if(WrapCode.equals("WR0310") && Double.toString(100000).equals(String.valueOf(mINSURE_INFO.getSUM_INSURED()))){
        	mLCPolSchema.setMult("1");
        }else if(WrapCode.equals("WR0310") && Double.toString(200000).equals(String.valueOf(mINSURE_INFO.getSUM_INSURED()))){
        	mLCPolSchema.setMult("2");
        }
		//保费
        mLCPolSchema.setPrem(mINSURE_INFO.getSUM_PREM());
		//销售人员编码
        mYBKWXLCInsuredListSchema.setAgentCode(mINSURE_INFO.getSALES_CODE());
		//健康告知
        mYBKWXLCInsuredListSchema.setHealthPromiseInfo(mINSURE_INFO.getHEALTH_PROMISE_INFO());
		//身高
		mYBKWXLCInsuredListSchema.setstature(mINSURE_INFO.getHEIGHT());
		//体重
		mYBKWXLCInsuredListSchema.setavoirdupois(mINSURE_INFO.getWEIGHT());
		
		//默认
		// 1、销售渠道
		mYBKWXLCInsuredListSchema.setSaleChnl("01");// 默认个险直销
		// 2、销售机构
		mYBKWXLCInsuredListSchema.setAgentCom("86210100");
		mLCPolSchema.setManageCom("86210100");
	    //保单号
		mYBKWXLCInsuredListSchema.setContNo("1");
		mLCPolSchema.setContType("1");
		//职业代码
		mYBKWXLCInsuredListSchema.setOccupationCode(mINSURE_INFO.getOCCUPACTIONCODE());
		String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + mINSURE_INFO.getOCCUPACTIONCODE() + "' with ur";
		String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
		if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
			mYBKWXLCInsuredListSchema.setOccupationType("1");  //职业类别为空的话，将职业类别置为1
		}else{
			mYBKWXLCInsuredListSchema.setOccupationType(mOccupationTypeStr);	//职业类别;
		}
		//新增校验
		 if(!checkData()){
		 return false;
		 }
		return true;
	}

	/*
	 * 投保规则校验
	 */
	public boolean checkData() {
		//判断身高体重
		Double BMI = Double.parseDouble(mINSURE_INFO.getWEIGHT())/Double.parseDouble(mINSURE_INFO.getHEIGHT())* Double.parseDouble(mINSURE_INFO.getHEIGHT());
		if(BMI>32 || BMI<15){
			this.mErrors.addOneError("体重指数超标，请通过公司柜面投保");
			 errors = this.mErrors.getFirstError();
			return false;
		}
		//健康告知是否正常
		if("1".equals(mINSURE_INFO.getHEALTH_PROMISE_INFO())){
			this.mErrors.addOneError("决绝投保：健康告知不符合投保规定");
			 errors = this.mErrors.getFirstError();
			return false;
		}
		// 同一时间一个用户只能有一份待处理保单
		String strbirthday = mINSURE_INFO.getBIRTHDAY();
		String year = strbirthday.substring(0, 4);
		String month = strbirthday.substring(4, 6);
		String day = strbirthday.substring(6, 8);
		String birthday = year+"-"+month+"-"+day;
		String name = mINSURE_INFO.getNAME();
		String sex = mINSURE_INFO.getGENDER();
		String idtype= mINSURE_INFO.getCERTIFICATE_TYPE();
		String idno = mINSURE_INFO.getCERTIFICATE_NO();
		String lcsql = "SELECT 1 FROM lccont WHERE appntname='"+name+"' and "
			+ "appntsex='"+sex+"'  and appntidtype='"+idtype+"' and appntidno='"+idno+"' and appntbirthday ='"+birthday+"' and appflag='0' and conttype='1' and uwflag  in ('5','9','Z')";
		SSRS ssSSRS = new ExeSQL().execSQL(lcsql);
		if(ssSSRS.getMaxRow()>0){
			this.mErrors.addOneError("决绝投保：同一时间一个用户只能有一份待处理保单");
			 errors = this.mErrors.getFirstError();
			return false;
		}
		//生成印刷号
		 tPrtNo = "SYW"+PubFun1.CreateMaxNo("SYYBK", 10);
		 System.out.println("生成的印刷号为："+tPrtNo);
		
		 String Insustrbirthday = mINSURE_INFO.getINSUED_BIRTHDAY();
			String Insuredyear = Insustrbirthday.substring(0, 4);
			String Insuredmonth = Insustrbirthday.substring(4, 6);
			String Insuredday = Insustrbirthday.substring(6, 8);
			String Insuredbirthday = Insuredyear+"-"+Insuredmonth+"-"+Insuredday;
			String Insuredname = mINSURE_INFO.getINSUED_NAME();
			String Insuredsex = mINSURE_INFO.getINSUED_GENDER();
			String Insuredidtype= mINSURE_INFO.getINSUED_CERTIFICATE_TYPE();
			String Insuredidno = mINSURE_INFO.getINSUED_CERTIFICATE_NO();
		 //生成投保人客户号
			if(mINSURE_INFO.getRELATION_INSURED().equals("00")){
				 //生成客户号
			    String SqlPerson = "SELECT customerno FROM ldperson WHERE name='"+name+"' and "
						+ "sex='"+sex+"'  and idtype='"+idtype+"' and idno='"+idno+"' and birthday ='"+birthday+"'";
					SSRS SSRS1 = new ExeSQL().execSQL(SqlPerson);
					if(SSRS1.getMaxRow()>0){//客户已经存在
						AppntNo = SSRS1.GetText(1, 1);
						System.out.println("投保人和被保人是同一个已存在原客户号为："+AppntNo);
					}else{//客户未存在，需要生成客户号
						AppntNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
						System.out.println("新生成客户号为："+AppntNo);
					}
					InsuredNo = AppntNo;
			}else {

				 //生成投保人客户号
			    String SqlPerson = "SELECT customerno FROM ldperson WHERE name='"+name+"' and "
						+ "sex='"+sex+"'  and idtype='"+idtype+"' and idno='"+idno+"' and birthday ='"+birthday+"'";
					SSRS SSRS1 = new ExeSQL().execSQL(SqlPerson);
					if(SSRS1.getMaxRow()>0){//客户已经存在
						AppntNo = SSRS1.GetText(1, 1);
						System.out.println("投保人原客户号为："+AppntNo);
					}else{//客户未存在，需要生成客户号
						AppntNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
						System.out.println("新生成投保人客户号为："+AppntNo);
					}
			   //生成的被保人客户号
					 String SqlPersonInsured = "SELECT customerno FROM ldperson WHERE name='"+Insuredname+"' and "
								+ "sex='"+Insuredsex+"'  and idtype='"+Insuredidtype+"' and idno='"+Insuredidno+"' and birthday ='"+Insuredbirthday+"'";
					 SSRS SSRS2 = new ExeSQL().execSQL(SqlPersonInsured);
						if(SSRS2.getMaxRow()>0){//客户已经存在
							InsuredNo = SSRS2.GetText(1, 1);
							System.out.println("被保人原客户号为："+InsuredNo);
						}else{//客户未存在，需要生成客户号
							InsuredNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
							System.out.println("新生成被保人客户号为："+InsuredNo);
						}
			}
		 
		 
		//各个套餐的校验
		if("WR0340".equals(mINSURE_INFO.getWRAP_CODE())){
			//投保年龄
			int aget = calAge(mINSURE_INFO.getINSUED_BIRTHDAY());
			 if(aget==0){
				 aget = (int) ((aNow.getTime() - Birthday.getTime())/(1000 * 60 * 60 * 24)); 
				 if(aget<28){
					 this.mErrors.addOneError("拒绝投保：投保年龄需满足出生满28天(含)以上");
					 errors = this.mErrors.getFirstError();
					return false;
				 }
			 }
			 if(aget>=45){
				 this.mErrors.addOneError("拒绝投保：投保年龄需满足四十五周岁以下");
				 errors = this.mErrors.getFirstError();
				return false;
			 }
			 //保险期间
			 if(!"25".equals(mYBKWXLCInsuredListSchema.getInsuYear())){
				 this.mErrors.addOneError("拒绝投保：保险期间需25年");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 //缴费方式
			 if("-1".equals(mYBKWXLCInsuredListSchema.getPayIntv())){
				 this.mErrors.addOneError("拒绝投保：缴费方式应为期缴");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 //缴费年期
			 if(!"5".equals(mYBKWXLCInsuredListSchema.getPayEndYear())){
				 this.mErrors.addOneError("拒绝投保：缴费期间应为5年");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 //投保保额 投保保额最低为1万元
			 if(mLCPolSchema.getAmnt()< 10000){
				 this.mErrors.addOneError("拒绝投保：投保保额最低为1万元");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 if(mLCPolSchema.getAmnt()>= 200000){
				 this.mErrors.addOneError("拒绝投保：投保保额上线为1万元");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 if(mLCPolSchema.getAmnt()%10000!=0){
				 this.mErrors.addOneError("拒绝投保：投保保额须为万元的整数倍");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			// 累计投保保额上限
			 String SqlAmnt = " select sum(amnt) from lcpol where insuredno in (SELECT customerno FROM ldperson WHERE name='"+Insuredname+"' and "
						+ "sex='"+Insuredsex+"'  and idtype='"+Insuredidtype+"' and idno='"+Insuredidno+"' and birthday ='"+Insuredbirthday+"') and prtno like 'SYW%' and riskcode in ('232401','335201') and  uwflag in ('0','4','5','9') and stateflag in ('0','1')";

			 SSRS SSRS2 = new ExeSQL().execSQL(SqlAmnt);
				if(!"null".equals(SSRS2.GetText(1, 1)) && !"".equals(SSRS2.GetText(1, 1)) && SSRS2.getMaxRow()>1){//客户已经存在
					if(Integer.parseInt(SSRS2.GetText(1, 1)) + mLCPolSchema.getAmnt() >200000){
						 this.mErrors.addOneError("拒绝投保：累计投保保额已到上限20万");
						 errors = this.mErrors.getFirstError();
						 return false;
					}
				}
		}
		if("WR0290".equals(mINSURE_INFO.getWRAP_CODE())){
			//投保年龄
			int aget = calAge(mYBKWXLCInsuredListSchema.getInsureBirthday());
			 if(aget==0){
				 aget = (int) ((aNow.getTime() - Birthday.getTime())/(1000 * 60 * 60 * 24)); 
				 if(aget<28){
					 this.mErrors.addOneError("拒绝投保：投保年龄需满足出生满28天(含)以上");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
			 }
			 if(aget>=45){
				 this.mErrors.addOneError("拒绝投保：投保年龄需满足四十五周岁以下");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 
			 //缴费方式
			 if(!"0".equals(mYBKWXLCInsuredListSchema.getPayIntv())){
				 this.mErrors.addOneError("拒绝投保：缴费方式应为趸缴");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 //缴费年期
			 if(!"10".equals(mYBKWXLCInsuredListSchema.getPayEndYear()) && !"15".equals(mYBKWXLCInsuredListSchema.getPayEndYear()) && !"20".equals(mYBKWXLCInsuredListSchema.getPayEndYear()) && !"30".equals(mYBKWXLCInsuredListSchema.getPayEndYear())){
				 this.mErrors.addOneError("拒绝投保：缴费期间应为10、15、20、30年");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 //投保保额 投保保额最低为1万元
			 if(mLCPolSchema.getAmnt()< 10000){
				 this.mErrors.addOneError("拒绝投保：投保保额最低为1万元");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 if(mLCPolSchema.getAmnt()%10000!=0){
				 this.mErrors.addOneError("拒绝投保：投保保额须为万元的整数倍");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 if(mLCPolSchema.getAmnt()>200000){
				 this.mErrors.addOneError("拒绝投保：投保保额上限为20万");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			// 累计投保保额上限
			 String SqlAmnt = " select sum(amnt) from lcpol where insuredno in (SELECT customerno FROM ldperson WHERE name='"+Insuredname+"' and "
						+ "sex='"+Insuredsex+"'  and idtype='"+Insuredidtype+"' and idno='"+Insuredidno+"' and birthday ='"+Insuredbirthday+"') and prtno like 'SYW%' and riskcode in ('231701','333801') and  uwflag in ('0','4','5','9') and stateflag in ('0','1')";

			 SSRS SSRS2 = new ExeSQL().execSQL(SqlAmnt);
				if(!"null".equals(SSRS2.GetText(1, 1)) && !"".equals(SSRS2.GetText(1, 1))){//客户已经存在
					if(Integer.parseInt(SSRS2.GetText(1, 1)) + mLCPolSchema.getAmnt() >200000){
						 this.mErrors.addOneError("拒绝投保：累计投保保额已到上限20万");
						 errors = this.mErrors.getFirstError();
						 return false;
					}
				}
		}
		if("WR0310".equals(mINSURE_INFO.getWRAP_CODE())){
			//投保年龄
			int aget = calAge(mYBKWXLCInsuredListSchema.getInsureBirthday());
			 if(aget==0){
				 aget = (int) ((aNow.getTime() - Birthday.getTime())/(1000 * 60 * 60 * 24)); 
				 if(aget<=28){
					 this.mErrors.addOneError("拒绝投保：投保年龄需满足出生满28天以上");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
			 }
			 if(aget>=15){
				 this.mErrors.addOneError("拒绝投保：投保年龄需满足十五周岁以下");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 //保险期间
			 if(!"20".equals(mYBKWXLCInsuredListSchema.getInsuYear())){
				 this.mErrors.addOneError("拒绝投保：保险期间只能是20年");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 //缴费方式
			 if(!"0".equals(mYBKWXLCInsuredListSchema.getPayIntv())){
				 this.mErrors.addOneError("拒绝投保：缴费方式应为趸缴");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 //投保保额 投保保额最低为10万元
			 if(mLCPolSchema.getAmnt()< 100000){
				 this.mErrors.addOneError("拒绝投保：投保保额最低为10万元");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			//投保保额 投保保额最低为20万元
			 if(mLCPolSchema.getAmnt()> 200000){
				 this.mErrors.addOneError("拒绝投保：投保保额最高为20万元");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			 if(mLCPolSchema.getAmnt()%10000!=0){
				 this.mErrors.addOneError("拒绝投保：投保保额须为万元的整数倍");
				 errors = this.mErrors.getFirstError();
				 return false;
			 }
			// 累计投保保额上限
			 String SqlAmnt = " select sum(amnt) from lcpol where insuredno in (SELECT customerno FROM ldperson WHERE name='"+Insuredname+"' and "
						+ "sex='"+Insuredsex+"'  and idtype='"+Insuredidtype+"' and idno='"+Insuredidno+"' and birthday ='"+Insuredbirthday+"') and prtno like 'SYW%' and riskcode='230801' and  uwflag in ('0','4','5','9') and stateflag in ('0','1')";

			 SSRS SSRS2 = new ExeSQL().execSQL(SqlAmnt);
				if(!"null".equals(SSRS2.GetText(1, 1)) && !"".equals(SSRS2.GetText(1, 1)) && SSRS2.getMaxRow()>1){//客户已经存在
					if(Integer.parseInt(SSRS2.GetText(1, 1)) + mLCPolSchema.getAmnt() > 200000){
						 this.mErrors.addOneError("拒绝投保：累计投保保额已到上限20万");
						 errors = this.mErrors.getFirstError();
						 return false;
					}
				}
		}
		
		if("Hwr102".equals(mINSURE_INFO.getWRAP_CODE())){//守护安康白金卡
			//投保年龄
			int aget = calAge(mYBKWXLCInsuredListSchema.getInsureBirthday());
			 if(aget>=17 && aget<=3){
				 String SqlAmnt = " select distinct(prtno) from lcpol where insuredno in (SELECT customerno FROM ldperson WHERE name='"+Insuredname+"' and "
							+ "sex='"+Insuredsex+"'  and idtype='"+Insuredidtype+"' and idno='"+Insuredidno+"' and birthday ='"+Insuredbirthday+"') and prtno like 'SYW%' and riskcode in  ('122001’,'520601') and  uwflag in ('0','4','5','9') and stateflag in ('0','1')";

				 SSRS SSRS2 = new ExeSQL().execSQL(SqlAmnt);
					if(SSRS2.getMaxRow()>=2){
							 this.mErrors.addOneError("拒绝投保：3-17周岁限购2份");
							 errors = this.mErrors.getFirstError();
							 return false;
					}
			 }
			 if(aget>=18 && aget<=65){
				 String SqlAmnt = " select distinct(prtno) from lcpol where insuredno in (SELECT customerno FROM ldperson WHERE name='"+Insuredname+"' and "
							+ "sex='"+Insuredsex+"'  and idtype='"+Insuredidtype+"' and idno='"+Insuredidno+"' and birthday ='"+Insuredbirthday+"') and prtno like 'SYW%' and riskcode in  ('122001’,'520601') and  uwflag in ('0','4','5','9') and stateflag in ('0','1')";

				 SSRS SSRS2 = new ExeSQL().execSQL(SqlAmnt);
					if(SSRS2.getMaxRow()>=3){
							 this.mErrors.addOneError("拒绝投保：18-65周岁限购3份");
							 errors = this.mErrors.getFirstError();
							 return false;
					}
			 }
		}
		
		if("Hwr104".equals(mINSURE_INFO.getWRAP_CODE())){//守护安康白金卡
			//投保年龄
			int aget = calAge(mYBKWXLCInsuredListSchema.getInsureBirthday());
			 if(aget>=18 && aget<=65){
				 String SqlAmnt = " select distinct(prtno) from lcpol where insuredno in (SELECT customerno FROM ldperson WHERE name='"+Insuredname+"' and "
							+ "sex='"+Insuredsex+"'  and idtype='"+Insuredidtype+"' and idno='"+Insuredidno+"' and birthday ='"+Insuredbirthday+"') and prtno like 'SYW%' and riskcode='511001' and  uwflag in ('0','4','5','9') and stateflag in ('0','1')";
				 SSRS SSRS2 = new ExeSQL().execSQL(SqlAmnt);
					if(SSRS2.getMaxRow()>=2){
							 this.mErrors.addOneError("拒绝投保：18-65周岁限购2份");
							 errors = this.mErrors.getFirstError();
							 return false;
					}
			 }
		}
		return true;

	}

	/**
	 * @param 保险期间
	 * @return
	 */
	public int getQj() {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date dateQ = null;
		Date dateM = null;
		try {
			// 起保日期
			dateQ = sdf.parse(mINSURE_INFO.getEFFECTIVE_DATE());
			// 满期日期
			dateM = sdf.parse(mINSURE_INFO.getEXPIRE_DATE());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(dateM);
		calendar.add(calendar.DATE, 1);// 把日期往后增加一天.整数往后推,负数往前移动
		dateM = calendar.getTime(); // 这个时间就是日期往后推一天的结果
		return dateM.getYear() - dateQ.getYear();
	}

	/**
	 * 返回信息报文
	 * 
	 */
	public void getXmlResult() {

		// 返回数据节点
		putResult("SUCCESS_INFO", mSUCCESS_INFO);

	}

	/**
	 * 获取保单信息
	 * 
	 */
	public boolean PrepareInfo() {
		String payMoney="";
		String sql = "select contno,amnt,prem,prtno,agentcode,agentgroup,appntname,bankcode from lccont where prtno = '"
				+ tPrtNo + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		System.out.println("噢");
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败";
			return false;
		} else {
			ContNo = tSSRS.GetText(1, 1);
			String serNo = PubFun1.CreateMaxNo("SERIALNO", "86310000");
			payMoney = tSSRS.GetText(1, 3);
			System.out.println("保单号：" + ContNo);
			mSUCCESS_INFO.setPOLICY_PRT_NO(tPrtNo);
			mSUCCESS_INFO.setPOLICY_CONT_NO(YBKSerialno);
			String SqlPol ="select * from lcpol where prtno ='"+tPrtNo+"'";
			LCPolDB mLCPolDB = new LCPolDB();
			LCPolSet mLCPolSet = new LCPolSet();
			mLCPolDB.setPrtNo(tPrtNo);
			 mLCPolSet = mLCPolDB.query();
			 LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
			 String aTempFeeNo = tPrtNo+"801";
			 System.out.println("险种条数"+mLCPolSet.size());
			 String Bankcode="";
			 if(payMoney.equals("4")){
				 Bankcode=tSSRS.GetText(1, 8);
			 }else{
				 
			 }
			for(int i =1;i<=mLCPolSet.size();i++){
				LCPolSchema mlcpolSchema = new LCPolSchema();
				mlcpolSchema = mLCPolSet.get(i);
				
				LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();
				mLJTempFeeSchema.setTempFeeNo(aTempFeeNo);
				mLJTempFeeSchema.setTempFeeType("1");
				mLJTempFeeSchema.setRiskCode(mlcpolSchema.getRiskCode());
				mLJTempFeeSchema.setPayIntv(PayIntv);
				mLJTempFeeSchema.setOtherNo(tPrtNo);
				mLJTempFeeSchema.setOtherNoType("4");
				mLJTempFeeSchema.setPayMoney(mlcpolSchema.getPrem());
				mLJTempFeeSchema.setPayDate(PubFun.getCurrentDate());
				mLJTempFeeSchema.setManageCom("86310000");
				mLJTempFeeSchema.setPolicyCom("86310000");
				mLJTempFeeSchema.setConfFlag("0");
				mLJTempFeeSchema.setSerialNo(serNo);
				mLJTempFeeSchema.setOperator("YBK");
				mLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
				mLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
				mLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
				mLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
				mLJTempFeeSchema.setAgentCode(tSSRS.GetText(1, 5));
				mLJTempFeeSchema.setAgentGroup(tSSRS.GetText(1, 6));
				mLJTempFeeSchema.setAPPntName(tSSRS.GetText(1, 7));
				mLJTempFeeSet.add(mLJTempFeeSchema);
				
			}
			
			LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
			LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
			mLJTempFeeClassSchema.setAppntName(mINSURE_INFO.getNAME());
			mLJTempFeeClassSchema.setTempFeeNo(aTempFeeNo);
			mLJTempFeeClassSchema.setPayMode(PayMode);
			mLJTempFeeClassSchema.setPayMoney(payMoney);
			mLJTempFeeClassSchema.setPayDate(PubFun.getCurrentDate());
			mLJTempFeeClassSchema.setConfFlag("0");
			mLJTempFeeClassSchema.setSerialNo(serNo);
			mLJTempFeeClassSchema.setManageCom("86310000");
			mLJTempFeeClassSchema.setPolicyCom("86310000");
			mLJTempFeeClassSchema.setBankAccNo(mINSURE_INFO.getBANK_NO());
			mLJTempFeeClassSchema.setAccName(mINSURE_INFO.getNAME());
			mLJTempFeeClassSchema.setBankCode(Bankcode);
			mLJTempFeeClassSchema.setOperator("YBK");
			mLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
			mLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
			mLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
			mLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
			mLJTempFeeClassSet.add(mLJTempFeeClassSchema);
//			 提交数据
			MMap tmap = new MMap();
			tmap.put(mLJTempFeeClassSet, "INSERT");
			tmap.put(mLJTempFeeSet,"INSERT");
			VData mInputData = new VData();
			mInputData.clear();
			mInputData.add(tmap);
			boolean flag = tPubSubmit.submitData(mInputData, "");
			if(flag){
				mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU01");
				mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("通过" + mError);// 正常承保状态
				errLogSY("通过", "1");
				System.out.println("通过");
			}else{
				mError = "生成暂收分类表失败";
				System.out.println("生成暂收分类表失败");
				return false;
			}
			
		}
		if(Double.parseDouble(MedicalMoney) < Double.parseDouble(payMoney)){
			mError = "医保卡余额不足";
			mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
			mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES(mError);
			System.out.println("医保卡余额不足");
			return false;
		}

		return true;
	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}
	 public int calAge(String insuredbirthday) {
        FDate fDate = new FDate();
         Birthday = fDate.getDate(insuredbirthday);
        String strNow = PubFun.getCurrentDate();
         aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。"+CustomerAge);
        return CustomerAge;
    }
	public static void main (String args[]){
		SyybTB a = new SyybTB();
		System.out.println(Integer.toString(a.calAge("2016-11-9")));
	}
}
