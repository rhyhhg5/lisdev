package com.sinosoft.httpclientybk.syybdeal;

/**
 * 录入被保人和险种信息！
 */

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.cbsws.obj.*;
import com.cbsws.core.obj.MsgHead;
import com.sinosoft.lis.brieftb.BriefSingleContInputBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDiseaseResultSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SyybYBKContInsuredIntlBL {
	private final static Logger cLogger = Logger.getLogger(SyybYBKContInsuredIntlBL.class);
	
	private MsgHead cMsgHead;
	private LCContTable cLCContTable;//保单信息
	private LCAppntTable cLCAppntTable;//投保人
	private List cLCInsuredList;//被保人
	private WrapTable cWrapTable;//套餐
	private List cLCBnfList;//受益人
	private List cWrapParamList; //算费参数
	private List cDutyCodeList;//本次算费的所有责任
	private String Errors = "";//错误信息
	
	private LCContSchema LCContSchema = null;
	private LDPersonSchema LDPersonSchema = null;
	private String mPrtno = "";
	private String mMsgType = "";
	public double mPrem=0;
	public double mAmnt=0;
	public LCDutySet cLCDutySet = new LCDutySet();
	public LCPolSet cLCPolSet = new LCPolSet();
	public GlobalInput cGlobalInput = new GlobalInput();
	public LCRiskDutyWrapSet cLCRiskDutyWrapSet = new  LCRiskDutyWrapSet();
	
	public SyybYBKContInsuredIntlBL(GlobalInput mGlobalInput,MsgHead tMsgHead,LCContTable tLCContTable, LCAppntTable tLAppntTable,List tLCInsuredList,WrapTable tWrapTable,List tLCBnfList,List tWrapParamList,LCContSchema mlCContSchema,LDPersonSchema mLDPersonSchema) {
		cMsgHead = tMsgHead;
		cLCContTable = tLCContTable;
		cLCAppntTable = tLAppntTable;
		cLCInsuredList = tLCInsuredList;
		cWrapTable = tWrapTable;
		cLCBnfList = tLCBnfList;
		cWrapParamList = tWrapParamList;
		cGlobalInput = mGlobalInput;
		LCContSchema = mlCContSchema;
		LDPersonSchema = mLDPersonSchema;
	}
	
	public String deal() {
		cLogger.info("Into WxContInsuredIntlBL.deal()...");
		
		mMsgType = cMsgHead.getMsgType();
		mPrtno = cLCContTable.getPrtNo();
		getDutyCodeList();
		
		//LCContSchema = getLCContSchema();
		for (int i = 0; i < cLCInsuredList.size(); i++) {
			LCInsuredTable tLCInsuredTable = (LCInsuredTable) cLCInsuredList.get(i);
			
			VData tContInsuredIntlBLVData = getContInsuredIntlBLVData(tLCInsuredTable);
			if(tContInsuredIntlBLVData == null){
				return Errors;
			}
			ContInsuredIntlBL tContInsuredIntlBL   = new ContInsuredIntlBL();
			cLogger.info("Start call ContInsuredIntlBL.submitData()...");
			long tStartMillism = System.currentTimeMillis();
			
		        if (!tContInsuredIntlBL.submitData(tContInsuredIntlBLVData, "INSERT||CONTINSURED")) {
					return tContInsuredIntlBL.mErrors.getFirstError();
				}
		        
		        
		        
			
			cLogger.info("ContInsuredIntlBL耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s"
					+ "；报文类型：" + mMsgType);
			cLogger.info("End call ContInsuredIntlBL.submitData()!");
		}
		
		cLogger.info("Out WxContInsuredIntlBL.deal()!");
		return Errors;
	}
	
	private VData getContInsuredIntlBLVData(LCInsuredTable pLCInsured) {
		cLogger.info("Into WxContInsuredIntlBL.getContInsuredIntlBLVData()...");
		//投保人信息
		LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
		mLCInsuredSchema.setInsuredNo(pLCInsured.getInsuredNo());
		mLCInsuredSchema.setName(pLCInsured.getName());
		mLCInsuredSchema.setSex(pLCInsured.getSex());
		mLCInsuredSchema.setBirthday(pLCInsured.getBirthday());
		mLCInsuredSchema.setIDType(pLCInsured.getIDType());
		mLCInsuredSchema.setIDNo(pLCInsured.getIDNo());
		mLCInsuredSchema.setOccupationCode(pLCInsured.getOccupationCode());
		if("SYTB01".equals(mMsgType)){
			String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + pLCInsured.getOccupationCode() + "' with ur";
			String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
			if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
				mLCInsuredSchema.setOccupationType("1");  //职业类别为空的话，将职业类别置为1
			}else{
				mLCInsuredSchema.setOccupationType(mOccupationTypeStr);	//职业类别;
			}
		}else{
			mLCInsuredSchema.setOccupationType("1");	//职业类别; 按责任计算，试算时给默认值，对应责任默认的算费可能会用到。
		}
		mLCInsuredSchema.setPrtNo(LCContSchema.getPrtNo());
		mLCInsuredSchema.setContNo(LCContSchema.getContNo());
		mLCInsuredSchema.setRelationToMainInsured(pLCInsured.getRelaToMain());
		mLCInsuredSchema.setRelationToAppnt(pLCInsured.getRelaToAppnt());
		mLCInsuredSchema.setInsuredNo(pLCInsured.getInsuredNo());

		//被保人地址
		LCAddressSchema mInsuredAddress = new LCAddressSchema();
		mInsuredAddress.setHomeAddress(pLCInsured.getHomeAddress());
		mInsuredAddress.setPostalAddress(pLCInsured.getMailAddress());
		mInsuredAddress.setZipCode(pLCInsured.getMailZipCode());
		mInsuredAddress.setPhone(pLCInsured.getHomePhone());
//		mInsuredAddress.setHomeZipCode(pLCInsured.geth);
		mInsuredAddress.setHomePhone(pLCInsured.getHomePhone());
//		mInsuredAddress.setCompanyPhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setMobile(pLCInsured.getInsuredMobile());
		mInsuredAddress.setEMail(pLCInsured.getEmail());

		//被保人个人信息
		LDPersonSchema mInsuredPerson = new LDPersonSchema();
		mInsuredPerson.setCustomerNo("");	//注意此处必须设置！picch核心判断CustomerNo为""时才进行被保人五要素判断，CustomerNo为null时会直接生成新客户号
		mInsuredPerson.setName(mLCInsuredSchema.getName());
		mInsuredPerson.setSex(mLCInsuredSchema.getSex());
		mInsuredPerson.setIDNo(mLCInsuredSchema.getIDNo());
		mInsuredPerson.setIDType(mLCInsuredSchema.getIDType());
		mInsuredPerson.setBirthday(mLCInsuredSchema.getBirthday());
		mInsuredPerson.setOccupationCode(mLCInsuredSchema.getOccupationCode());
		mInsuredPerson.setOccupationType(mLCInsuredSchema.getOccupationType());	//职业类别

		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setSchema(mLCInsuredSchema);

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("FamilyType", "0");
		mTransferData.setNameAndValue("PolTypeFlag", "0");
		mTransferData.setNameAndValue("SequenceNo", pLCInsured.getInsuredNo());
		mTransferData.setNameAndValue("ContType", "1");
		mTransferData.setNameAndValue("SavePolType", "0");
		mTransferData.setNameAndValue("mMsgType", mMsgType);

		//套餐信息
		LDRiskDutyWrapDB mLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
		mLDRiskDutyWrapDB.setRiskWrapCode(cWrapTable.getRiskWrapCode());
		LDRiskDutyWrapSet mLDRiskDutyWrapSet = mLDRiskDutyWrapDB.query();
		LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
		for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++) {
			LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet.get(i);
			if(cDutyCodeList.contains(tLDRiskDutyWrapSchema.getDutyCode())){
				LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
				tLCRiskDutyWrapSchema.setRiskWrapCode(tLDRiskDutyWrapSchema.getRiskWrapCode());
				tLCRiskDutyWrapSchema.setRiskCode(tLDRiskDutyWrapSchema.getRiskCode());
				tLCRiskDutyWrapSchema.setDutyCode(tLDRiskDutyWrapSchema.getDutyCode());
				tLCRiskDutyWrapSchema.setCalFactor(tLDRiskDutyWrapSchema.getCalFactor());
				tLCRiskDutyWrapSchema.setCalFactorType(tLDRiskDutyWrapSchema.getCalFactorType());
				for(int j = 1;j <= cWrapParamList.size();j++){
					WrapParamTable tWrapParamTable = (WrapParamTable)cWrapParamList.get(j-1);
					if(tLDRiskDutyWrapSchema.getRiskWrapCode().equals(tWrapParamTable.getRiskWrapCode()) 
							&& tLDRiskDutyWrapSchema.getRiskCode().equals(tWrapParamTable.getRiskCode())
							&& tLDRiskDutyWrapSchema.getDutyCode().equals(tWrapParamTable.getDutyCode()) 
							&& tLDRiskDutyWrapSchema.getCalFactor().equals(tWrapParamTable.getCalfactor())){
						if("1".equals(tLDRiskDutyWrapSchema.getCalFactorType()) 
								&& (!"".equals(tWrapParamTable.getCalfactorValue()))
								&& (!tLDRiskDutyWrapSchema.getCalFactorValue().equals(tWrapParamTable.getCalfactorValue()))){
							Errors = "算费要素与产品描述不符，请核实！";
							return null;
							
						}
						tLCRiskDutyWrapSchema.setCalFactorValue(tWrapParamTable.getCalfactorValue());
					}
				}
				tLCRiskDutyWrapSchema.setInsuredNo(pLCInsured.getInsuredNo());//虚拟客户号码，避免后面生成客户号码。
				mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
			}
		}
		System.out.println("需要算费的算费参数："+mLCRiskDutyWrapSet.size());
		//受益人
		LCBnfSet mLCBnfSet = new LCBnfSet();
		if(cLCBnfList != null && cLCBnfList.size()>0){
			for (int i = 0; i < cLCBnfList.size(); i++) {
				LCBnfTable tLCBnfTable = (LCBnfTable) cLCBnfList.get(i);
				if(pLCInsured.getInsuredNo().equals(tLCBnfTable.getInsuredNo())){
					LCBnfSchema tLCBnfSchema = new LCBnfSchema();
					tLCBnfSchema.setContNo(LCContSchema.getContNo());
					tLCBnfSchema.setBnfType(tLCBnfTable.getBnfType());
					tLCBnfSchema.setBnfGrade(tLCBnfTable.getBnfGrade());
					tLCBnfSchema.setName(tLCBnfTable.getName());
					tLCBnfSchema.setBirthday(tLCBnfTable.getBirthday());
					tLCBnfSchema.setSex(tLCBnfTable.getSex());
					tLCBnfSchema.setIDType(tLCBnfTable.getIDType());
					tLCBnfSchema.setIDNo(tLCBnfTable.getIDNo());
					tLCBnfSchema.setRelationToInsured(tLCBnfTable.getRelationToInsured());
					tLCBnfSchema.setBnfLot(tLCBnfTable.getBnfLot());
					/*
					 * 核心要求的受益比例在0.0-1.0之间，而标准报文的受益比例为1-100，所以需要在此做转换
					 */
					tLCBnfSchema.setBnfLot(tLCBnfSchema.getBnfLot()/100.0);
					mLCBnfSet.add(tLCBnfSchema);
				}
			}
		}

		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(LCContSchema);
		mVData.add(cLCAppntTable);
		mVData.add(mLCInsuredSchema);
		mVData.add(mLCInsuredDB);
		mVData.add(mInsuredAddress);
		mVData.add(mInsuredPerson);
		mVData.add(mTransferData);
		mVData.add(mLCRiskDutyWrapSet);
		mVData.add(mLCBnfSet);
		mVData.add(new LCDiseaseResultSet());	//告知信息
		mVData.add(new LCNationSet());	//抵达国家

		cLogger.info("Out WxContInsuredIntlBL.getContInsuredIntlBLVData()!");
		return mVData;
	}
	
	/*private LCContSchema getLCContSchema(){
		cLogger.info("Into WXContBL.getLCContSchema()...");
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		
		LCContSchema.setContType("1");	//保单类别(个单-1，团单-2)
		LCContSchema.setAgentGroup("");
		LCContSchema.setPayLocation("0");	
		LCContSchema.setPrintCount(0);
//		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent));
//		GregorianCalendar mNowCalendar = new GregorianCalendar();
		//保单生效日，如果复杂网销传生效日期，就取传的；如果不传，就取当前日期的下一天
		String tCValidate = cLCContTable.getCValiDate();
		if(tCValidate==null || tCValidate.equals("") || tCValidate.equals("null"))
		{
			mNowCalendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
			mLCContSchema.setCValiDate(mNowCalendar.getTime());	//保单生效日期(当前日期的下一天)
		}
		else
		{
			mLCContSchema.setCValiDate(tCValidate);
		}
		LCContSchema.setPolApplyDate(mCurrentDate);	//投保日期
		LCContSchema.setProposalType("01");	//投保书类型：01-普通个单投保书
		
		return LCContSchema;
	}*/
	
	public double getPrem(){
		return mPrem;
	}
	public double getAmnt(){
		return mAmnt;
	}
	public LCDutySet getLCDutySet(){
		return cLCDutySet;
	}
	public LCPolSet getLCPolSet(){
		return cLCPolSet;
	}
	public LCRiskDutyWrapSet getLCRiskDutyWrapSet(){
		return cLCRiskDutyWrapSet;
	}
	
	private void getDutyCodeList(){
		cDutyCodeList = new ArrayList();
		for(int i = 0;i<cWrapParamList.size();i++){
			WrapParamTable tWrapParamTable = (WrapParamTable)cWrapParamList.get(i);
			String DutyCode = tWrapParamTable.getDutyCode();
			if(!cDutyCodeList.contains(DutyCode)){
				cDutyCodeList.add(DutyCode);
			}
		}
	}
	
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
//		String mInFile = "D:/request/ICBC_std/UW.xml";
		
//		FileInputStream mFis = new FileInputStream(mInFile);
//		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
//		Document mXmlDoc = new SAXBuilder().build(mIsr);
//		
//		Element mTranData = mXmlDoc.getRootElement();
//		Element mBaseInfo = mTranData.getChild("BaseInfo");
//		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
//		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
//		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
//		mLCCont.getChild("PrtNo").setText(mProposalNo);
//		mLCCont.getChild("ProposalContNo").setText(mPrtNo);
//		
//		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
//				mBaseInfo.getChildText("BankCode"),
//				mBaseInfo.getChildText("ZoneNo"),
//				mBaseInfo.getChildText("BrNo"));
//		
//		new WxContInsuredIntlBL(mXmlDoc, mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}

