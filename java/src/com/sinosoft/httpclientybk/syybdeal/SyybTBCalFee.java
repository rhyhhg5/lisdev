package com.sinosoft.httpclientybk.syybdeal;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.SYTB02.INSURE_INFO;
import com.sinosoft.httpclientybk.dto.SYTB02.SUCCESS_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.SYYBKWXLCInsuredListSchema;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;



public class SyybTBCalFee extends ABusLogic 
{
	public String MsgType;
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public List INSURE_INFO_List;
	public INSURE_INFO mINSURE_INFO = new INSURE_INFO();
	SYYBKCalCulateFee mSYYBKCalCulateFee = new SYYBKCalCulateFee();
	private SUCCESS_INFO mSUCCESS_INFO = new SUCCESS_INFO();
	Date aNow = null;
	Date Birthday = null;
	String errors = "";
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理沈阳YBK微信出单标准平台保费试算");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		MsgType = tMsgHead.getREQUEST_TYPE();
		mGlobalInput.Operator = "YBK";
		mGlobalInput.ComCode = "86";
		mGlobalInput.ManageCom = "86";
		try {
			System.out.println("进来了保费试算");
			INSURE_INFO_List = cMsgInfos.getBodyByFlag("BODY");
			mINSURE_INFO = (INSURE_INFO) INSURE_INFO_List.get(0);
			if (INSURE_INFO_List == null || INSURE_INFO_List.size() != 1) {
				errLogSY("获取沈阳医保保费试算信息失败", "0");
				return false;
			}
			//算费报文
				String WrapCode=mINSURE_INFO.getWRAP_CODE();//套擦还能编码
				String InsuredSex = mINSURE_INFO.getINSUED_GENDER();//被保人性别
				String Amnt = mINSURE_INFO.getSUM_INSURED();//保额
				String PayEndYear = mINSURE_INFO.getPAY_END_YEAR();//缴费年限、缴费期间
				String PayIntv = mINSURE_INFO.getPAY_INTV();//缴费频次 0 趸缴; 1年缴
				String CValiDate = mINSURE_INFO.getAPPLICATION_DATE();//投保申请日期
				String insureYearFlag =  mINSURE_INFO.getINSUREYEAR_FLAG();//保险年期、保险期间
				String InsuredBirthday = mINSURE_INFO.getINSUED_BIRTHDAY();//被保人出生日期
				String cvalidate = mINSURE_INFO.getEFFECTIVE_DATE();//生效日期
				 boolean flag = Check();
				if(!flag){
					errLogSY(errors,"0");
					mSUCCESS_INFO.setErrorCode("0");
					mSUCCESS_INFO.setErrorMsg(errors);
					// 返回数据节点
				    getXmlResult();
					return false;
				}
		    	String sql = "select DutyCode,RiskCode,Calfactor,Calfactorvalue from LDRiskDutyWrap where  RiskWrapCode='"
						+ WrapCode + "'";
				SSRS mSSRS = new ExeSQL().execSQL(sql);
				LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
				for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
					LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
					tLCRiskDutyWrapSchema.setDutyCode(mSSRS.GetText(i, 1));
					tLCRiskDutyWrapSchema.setRiskCode(mSSRS.GetText(i, 2));
					tLCRiskDutyWrapSchema.setRiskWrapCode(WrapCode);
					if("Amnt".equals(mSSRS.GetText(i, 3))&&("".equals(mSSRS.GetText(i, 4))||mSSRS.GetText(i, 4)==null)){
						tLCRiskDutyWrapSchema.setCalFactor("Amnt");
						tLCRiskDutyWrapSchema.setCalFactorValue(Amnt);
					}else if("PayEndYear".equals(mSSRS.GetText(i, 3))&&("".equals(mSSRS.GetText(i, 4))||mSSRS.GetText(i, 4)==null)){
						tLCRiskDutyWrapSchema.setCalFactorValue(PayEndYear);
						tLCRiskDutyWrapSchema.setCalFactor("PayEndYear");
					}else if("Mult".equals(mSSRS.GetText(i, 3))&&("".equals(mSSRS.GetText(i, 4))||mSSRS.GetText(i, 4)==null)){
						if(WrapCode.equals("WR0310") && Double.parseDouble(Amnt)==100000.00){
							tLCRiskDutyWrapSchema.setCalFactor("Mult");
							tLCRiskDutyWrapSchema.setCalFactorValue("1");
						}else if(WrapCode.equals("WR0310") && Double.parseDouble(Amnt)==200000.00){
							tLCRiskDutyWrapSchema.setCalFactor("Mult");
							tLCRiskDutyWrapSchema.setCalFactorValue("2");
						}
					
					}else{
						tLCRiskDutyWrapSchema.setCalFactor(mSSRS.GetText(i, 3));
						tLCRiskDutyWrapSchema.setCalFactorValue(mSSRS.GetText(i, 4));
					}
					mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
				}
				LCContSchema mLCContSchema = new LCContSchema();
				mLCContSchema.setCValiDate(CValiDate);
				mLCContSchema.setInsuredBirthday(InsuredBirthday);
				mLCContSchema.setInsuredSex(InsuredSex);
				mLCContSchema.setPayIntv(PayIntv);
				VData cInputData = new VData();
				cInputData.add(mLCRiskDutyWrapSet);
				cInputData.add(mLCContSchema);
				mSYYBKCalCulateFee = new SYYBKCalCulateFee();
				Map map = mSYYBKCalCulateFee.getSubmitRiskMap(cInputData,"INSERT");
				if(!map.isEmpty()){
					Double sumPrem = (Double) map.get("sumPrem");
					Double sumAmnt = (Double) map.get("sumAmnt");
					mSUCCESS_INFO.setPrem(Double.toString(sumPrem));
					mSUCCESS_INFO.setErrorCode("1");
					mSUCCESS_INFO.setErrorMsg("算费成功");
					// 返回数据节点
				    getXmlResult();
					return true;
				}else{
					errLog(mSYYBKCalCulateFee.mErrors.getFirstError(), "0");
					return false;
				}
				
		} catch ( Exception e){
			e.printStackTrace();
			}
		return true;
	}
	
	public int calAge(String insuredbirthday) {
        FDate fDate = new FDate();
         Birthday = fDate.getDate(insuredbirthday);
        String strNow = PubFun.getCurrentDate();
         aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。"+CustomerAge);
        return CustomerAge;
    } 
      public boolean Check(){
	//各个套餐的校验
			if("WR0340".equals(mINSURE_INFO.getWRAP_CODE())){
				//投保年龄
				int aget = calAge(mINSURE_INFO.getINSUED_BIRTHDAY());
				 if(aget==0){
					 aget = (int) ((aNow.getTime() - Birthday.getTime())/(1000 * 60 * 60 * 24)); 
					 if(aget<28){
						 this.mErrors.addOneError("拒绝投保：投保年龄需满足出生满28天(含)以上");
						 errors = this.mErrors.getFirstError();
						return false;
					 }
				 }
				 if(aget>=45){
					 this.mErrors.addOneError("拒绝投保：投保年龄需满足四十五周岁以下");
					 errors = this.mErrors.getFirstError();
					return false;
				 }
				 //保险期间
				 if(!"25".equals(mINSURE_INFO.getINSUREYEAR_FLAG())){
					 this.mErrors.addOneError("拒绝投保：保险期间需25年");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 //缴费年期
				 if(!"5".equals(mINSURE_INFO.getPAY_END_YEAR())){
					 this.mErrors.addOneError("拒绝投保：缴费期间应为5年");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 //投保保额 投保保额最低为1万元
				 if(Double.parseDouble(mINSURE_INFO.getSUM_INSURED())< 10000){
					 this.mErrors.addOneError("拒绝投保：投保保额最低为1万元");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 if(Double.parseDouble(mINSURE_INFO.getSUM_INSURED())%10000!=0){
					 this.mErrors.addOneError("拒绝投保：投保保额须为万元的整数倍");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
			}
			if("WR0290".equals(mINSURE_INFO.getWRAP_CODE())){
				//投保年龄
				int aget = calAge(mINSURE_INFO.getINSUED_BIRTHDAY());
				 if(aget==0){
					 aget = (int) ((aNow.getTime() - Birthday.getTime())/(1000 * 60 * 60 * 24)); 
					 if(aget<28){
						 this.mErrors.addOneError("拒绝投保：投保年龄需满足出生满28天(含)以上");
						 errors = this.mErrors.getFirstError();
						 return false;
					 }
				 }
				 if(aget>=45){
					 this.mErrors.addOneError("拒绝投保：投保年龄需满足四十五周岁以下");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 /*//保险期间
				 if(aget>80){
					 this.mErrors.addOneError("拒绝投保：保险期间需80周岁");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }*/
				 //缴费年期
				 if(!"10".equals(mINSURE_INFO.getPAY_END_YEAR()) && !"15".equals(mINSURE_INFO.getPAY_END_YEAR()) && !"20".equals(mINSURE_INFO.getPAY_END_YEAR()) && !"30".equals(mINSURE_INFO.getPAY_END_YEAR())){
					 this.mErrors.addOneError("拒绝投保：缴费期间应为10、15、20、30年");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 //投保保额 投保保额最低为1万元
				 if(Double.parseDouble(mINSURE_INFO.getSUM_INSURED())< 10000){
					 this.mErrors.addOneError("拒绝投保：投保保额最低为1万元");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 if(Double.parseDouble(mINSURE_INFO.getSUM_INSURED())%10000!=0){
					 this.mErrors.addOneError("拒绝投保：投保保额须为万元的整数倍");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 if(Double.parseDouble(mINSURE_INFO.getSUM_INSURED())>200000){
					 this.mErrors.addOneError("拒绝投保：投保保额上限为20万");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
			}
			if("WR0310".equals(mINSURE_INFO.getWRAP_CODE())){
				//投保年龄
				int aget = calAge(mINSURE_INFO.getINSUED_BIRTHDAY());
				 if(aget==0){
					 aget = (int) ((aNow.getTime() - Birthday.getTime())/(1000 * 60 * 60 * 24)); 
					 if(aget<=28){
						 this.mErrors.addOneError("拒绝投保：投保年龄需满足出生满28天以上");
						 errors = this.mErrors.getFirstError();
						 return false;
					 }
				 }
				 if(aget>=15){
					 this.mErrors.addOneError("拒绝投保：投保年龄需满足十五周岁以下");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 //保险期间
				 if(!"20".equals(mINSURE_INFO.getINSUREYEAR_FLAG())){
					 this.mErrors.addOneError("拒绝投保：保险期间只能是20年");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 //投保保额 投保保额最低为10万元
				 if(Double.parseDouble(mINSURE_INFO.getSUM_INSURED())< 100000){
					 this.mErrors.addOneError("拒绝投保：投保保额最低为10万元");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				//投保保额 投保保额最低为20万元
				 if(Double.parseDouble(mINSURE_INFO.getSUM_INSURED())> 200000){
					 this.mErrors.addOneError("拒绝投保：投保保额最高为20万元");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
				 if(Double.parseDouble(mINSURE_INFO.getSUM_INSURED())%10000!=0){
					 this.mErrors.addOneError("拒绝投保：投保保额须为万元的整数倍");
					 errors = this.mErrors.getFirstError();
					 return false;
				 }
			}
			return true;
}
	
	/**
	 * 返回信息报文
	 * 
	 */
	public void getXmlResult() {

		// 返回数据节点
		putSYResult("SUCCESS_INFO", mSUCCESS_INFO);

	}

	
	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
