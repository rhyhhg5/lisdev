/*
 * SY03沈阳医保保费接口
 * yukun 2017-07-26新增
 * 
 * */
package com.sinosoft.httpclientybk.syybdeal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.SY03.MEDICAL_INFO;
import com.sinosoft.httpclientybk.dto.SY03.SUCCESS_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.schema.LCContReceiveSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCContSubSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

public class SyybSignTB extends ABusLogic {
	/* 公共信息 */
	public GlobalInput tGI = new GlobalInput();
	/* 报文头信息 */
	private String TASK_NO;
	private String REQUEST_TYPE;
	/* 报文体信息  */
	private MEDICAL_INFO tMEDICAL_INFO = new MEDICAL_INFO();
	private SUCCESS_INFO tSUCCESS_INFO = new SUCCESS_INFO();
	/* 签单前合同号 */
	private String tContNo = "";
	/* 签单后合同号 */
	private String nContNo = "";
	/* 印刷号 */
	private String tPrtNo = "";
	/* 缴费单号*/
	private String tPaymentNo = "";
	// 处理过程中的错误信息
	public String mError = "";
	
	PubSubmit tPubSubmit = new PubSubmit();
	LCContSubSchema mLCContSubSchema = new LCContSubSchema();
	LCContSchema mLCContSchema = new LCContSchema();

	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理SY03沈阳医保扣款确认-签单-保单打印-回执回销过程");
		tGI.ManageCom = "86210100";
		tGI.ComCode = "86210100";
		tGI.Operator = "SYYB";		
		
		//解析XML
		if (!parseXML(cMsgInfos)) {
			System.out.println("报文解析失败---");
			tSUCCESS_INFO.setPAYMENTNO(tPaymentNo);
			tSUCCESS_INFO.setINSURESTATEMENT(mError);
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			return false;
		}
		//数据校验
		if(!checkData()){
			System.out.println("传入数据校验失败---");
			tSUCCESS_INFO.setPAYMENTNO(tPaymentNo);
			tSUCCESS_INFO.setINSURESTATEMENT(mError);
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			return false;
		}
		//收费确认
		if(!checkMoney()){
			System.out.println("收费确认失败---");
			tSUCCESS_INFO.setPAYMENTNO(tPaymentNo);
			tSUCCESS_INFO.setINSURESTATEMENT(mError);
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			return false;
		}
		//签单处理
		if(!SignPolicy()){
			System.out.println("签单失败---");
			tSUCCESS_INFO.setPAYMENTNO(tPaymentNo);
			tSUCCESS_INFO.setINSURESTATEMENT(mError);
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			//保存日志
			errLogSY(mError, "0");
			return false;
		}
		//保单打印
		if(!printPolicy()){
			System.out.println("保单打印失败---");
			tSUCCESS_INFO.setPAYMENTNO(tPaymentNo);
			tSUCCESS_INFO.setINSURESTATEMENT("保单打印失败");
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			//保存日志
			errLogSY("保单打印失败", "0");
			return false;
		}
		//回执回销处理
		if(!returnReceipt()){
			System.out.println("回执回销失败---");
			tSUCCESS_INFO.setPAYMENTNO(tPaymentNo);
			tSUCCESS_INFO.setINSURESTATEMENT("回执回销失败！");
			putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
			//保存日志
			errLogSY("回执回销失败！", "0");
			return false;
		}
		//保存日志
		SetPrtNo(tPrtNo);
		errLogSY("SY03处理成功!", "1");
		//封装返回报文
		tSUCCESS_INFO.setPAYMENTNO(tPaymentNo);
		tSUCCESS_INFO.setINSURESTATEMENT("SY03处理成功！");	
		getXmlResult();
		return true;
	}
	/*
	 * 报文数据解析
	 * */
	private boolean parseXML(MsgCollection cMsgInfos) {
		System.out.println("开始解析XML");
		// 获取保单信息
		List<MEDICAL_INFO> tEdorInfoList = cMsgInfos.getBodyByFlag("BODY");
		if (tEdorInfoList == null || tEdorInfoList.size() != 1) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError ="请求报文信息获取失败!";
			errLogSY(mError, "0");
			return false;
		}
		tMEDICAL_INFO = tEdorInfoList.get(0);
		// 获取头信息
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		TASK_NO = tMsgHead.getTASK_NO();
		if("".equals(TASK_NO)||null == TASK_NO||"null".equals(TASK_NO)){
			//保存日志
			SetPrtNo(tPrtNo);
			mError ="task_no不能为空!";
			errLogSY(mError, "0");
			return false;
		}
		REQUEST_TYPE = tMsgHead.getREQUEST_TYPE();
		System.out.println("TASK_NO = " + TASK_NO);
		System.out.println("REQUEST_TYPE = " + REQUEST_TYPE);
		// 获取contno
		tPrtNo = tMEDICAL_INFO.getCOUNTNO();
		if (tPrtNo == null || "".equals(tPrtNo)) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError ="Prtno获取失败!";
			errLogSY(mError, "0");
			return false;
		}
		// 查询保单信息
		LCContDB tLCContDB = new LCContDB();
		//根据prtno向日志表添加数据
	    String sql = "select * from lccont where prtno = '"+tPrtNo+"' with ur";
		LCContSet mLCContSet = tLCContDB.executeQuery(sql);
		if (mLCContSet.size() == 0 || mLCContSet == null) {
			SetPrtNo(tPrtNo);
			mError =tPrtNo+ "保单不存在";
			errLogSY(mError, "0");
			return false;
		} else {
			mLCContSchema = mLCContSet.get(1);
			this.tContNo = mLCContSchema.getContNo();
		}
		System.out.println("tPrtNo=" + tPrtNo);
		//更新lccontsub表的缴费单号
		tPaymentNo = tMEDICAL_INFO.getPAYMENTNO();
		String updateSQL = "update lccontsub set SYMedicalTempfeeno = '"+tPaymentNo+"' where prtno = '"+tPrtNo+"'";
		// 提交数据		
        VData tInputData = new VData();
        MMap mmap = new MMap();
        tInputData.clear();
		mmap.put(updateSQL, "UPDATE");
		tInputData.add(mmap);
		if(!tPubSubmit.submitData(tInputData, "UPDATE")){
			System.out.println("lccontsub表的缴费单号保存失败！");
			//保存日志
			SetPrtNo(tPrtNo);
			mError ="ccontsub表的缴费单号保存失败！";
			errLogSY(mError, "0");
			return false;
		}
		return true;
	}
	/*
	 * 报文数据检测
	 * */
	private boolean checkData() {
		//合同号校验
		if (null == tMEDICAL_INFO.getCOUNTNO()
				|| "".equals(tMEDICAL_INFO.getCOUNTNO())) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="投保单号不能为空";
			errLogSY(mError, "0");
			return false;
		}
		//缴费单号校验
		if (null == tMEDICAL_INFO.getPAYMENTNO()
				|| "".equals(tMEDICAL_INFO.getPAYMENTNO())) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="缴费单号不能为空";
			errLogSY(mError, "0");
			return false;
		}
		//医保个人编号校验
		if (null == tMEDICAL_INFO.getMEDICALNO()
				|| "".equals(tMEDICAL_INFO.getMEDICALNO())) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="医保个人编号不能为空";
			errLogSY(mError, "0");
			return false;
		}
		//缴费方式校验
		if (null == tMEDICAL_INFO.getPAYMODE()
				|| "".equals(tMEDICAL_INFO.getPAYMODE())) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="缴费方式不能为空";
			errLogSY(mError, "0");
			return false;
		}
		//缴费金额校验
		if (null == tMEDICAL_INFO.getSUMMONEY()
				|| "".equals(tMEDICAL_INFO.getSUMMONEY())) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="缴费总金额不能为空";
			errLogSY(mError, "0");
			return false;
		}
		//支付时间校验
		if (null == tMEDICAL_INFO.getPAYDATE()
				|| "".equals(tMEDICAL_INFO.getPAYDATE())) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="支付时间不能为空";
			errLogSY(mError, "0");
			return false;
		}
		//是否扣费成功
		if (null == tMEDICAL_INFO.getIFSUCCESS()
				|| "".equals(tMEDICAL_INFO.getIFSUCCESS())) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="支付时间不能为空";
			errLogSY(mError, "0");
			return false;
		}
		return true;
	}
	/*
	 * 收费确认处理
	 * */
	private boolean checkMoney(){
		//沈阳医保扣款确认部分
		System.out.println("=============开始处理沈阳医保扣款确认=================");
		//从lccontsub里边拿到医保卡号和真正的扣款方式
		String sqlsub = "select * from lccontsub where prtno = '"+ tPrtNo + "' ";
		LCContSubDB tLCContSubDB =  new LCContSubDB();
		LCContSubSet mLCContSubSet = tLCContSubDB.executeQuery(sqlsub);
		if ( mLCContSubSet.size() == 0 || mLCContSubSet ==null) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="保单附属信息不存在！";
			errLogSY(mError,"0");
			return false;
		} else {
			mLCContSubSchema = mLCContSubSet.get(1);
		}
        //校验有无待收费信息
		String sqltemp = "select * from ljtempfee where otherno='"+ tPrtNo + "' and enteraccdate is null";
		LJTempFeeDB mLJTempFeeDB = new LJTempFeeDB();
		LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
		LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();
		mLJTempFeeSet = mLJTempFeeDB.executeQuery(sqltemp);
		if (null == mLJTempFeeSet.get(1)) {
			//保存日志
			SetPrtNo(tPrtNo);
			mError="保单无待收费信息(ljtempfee为空)";
			errLogSY(mError,"0");
			return false;
		} else {
			mLJTempFeeSchema =mLJTempFeeSet.get(1);
		}
		//更新暂收分类表
		String sqltempclass = "select * from ljtempfeeclass where tempfeeno='"+ mLJTempFeeSchema.getTempFeeNo() + "'";
		LJTempFeeClassDB mLJTempFeeClassDB = new LJTempFeeClassDB();
		LJTempFeeClassSchema mLJTempFeeclassSchema = new LJTempFeeClassSchema();
		LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
		mLJTempFeeClassSet = mLJTempFeeClassDB.executeQuery(sqltempclass);
		if(null == mLJTempFeeClassSet.get(1)){
			//保存日志
			SetPrtNo(tPrtNo);
			mError="保单无待收费信息(ljtempfeeclass为空)";
			errLogSY(mError,"0");
			return false;
		}else{
			mLJTempFeeclassSchema = mLJTempFeeClassSet.get(1);
		}
		String sqlTempfeclass = "";
		String sqlTempe="";
		String sqlCont = "";
		String sqlLcpol = "";		
		MMap tmap = new MMap();
			
		if(mLJTempFeeClassSet.size()!=0){		
			//医保扣款失败
			if("1".equals(tMEDICAL_INFO.getIFSUCCESS())){
				//医保扣款失败，判断是否为银行卡扣款
				if("2".equals(tMEDICAL_INFO.getPAYMODE())){
					/*String dateString = StringPattern(tMEDICAL_INFO.getPAYDATE(),"yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss");
					String date=dateString.substring(0, 10);
					String time=dateString.substring(11, 19);*/
					//更新ljtempfeeclass
					sqlTempfeclass ="update LJTempFeeClass set PayMode='4',BankCode='"+mLCContSchema.getBankCode()
								+"' ,bankaccno ='"+ mLCContSubSchema.getMedicalCode() 
								/*+"' ,EnterAccDate ='"+ date 
								+"' ,ConfMakeDate ='"+ date 
								+"' ,ConfMakeTime ='"+ time*/
								+"',ModifyDate= current date,ModifyTime=current time where TempFeeNo='" + mLJTempFeeclassSchema.getTempFeeNo();
					//更新ljtempfee表
					/*sqlTempe ="update LJTempFee set "
								+"' ,ConfMakeDate ='"+ date 
								+"' ,ConfMakeTime ='"+ time
								+" ModifyDate= current date,ModifyTime=current time where TempFeeNo='" + mLJTempFeeSchema.getTempFeeNo()+ "'";*/
					//更新lccont表
					sqlCont = "update lccont set paymode='4',expaymode='4' where prtno ='" + mLCContSchema.getPrtNo()+ "'";
					//更新lcpol表
					sqlLcpol="update lcpol set paymode='4',expaymode='4' where prtno ='" + mLCContSchema.getPrtNo()+ "'";
					
					tmap.put(sqlTempfeclass, "UPDATE");
					tmap.put(sqlTempe, "UPDATE");
					tmap.put(sqlCont, "UPDATE");
					tmap.put(sqlLcpol, "UPDATE");
					//保存日志
					SetPrtNo(tPrtNo);
					errLogSY("银行卡扣款已经成功","1");
				}else if("1".equals(tMEDICAL_INFO.getPAYMODE())){
					//保存日志
					SetPrtNo(tPrtNo);	
					mError="传入参数为1医保扣款失败！";
					errLogSY(mError,"0");
					return false;
				}else{
					//保存日志
					SetPrtNo(tPrtNo);	
					mError="传入参数错误扣款失败！";
					errLogSY(mError,"0");
					return false;
				}
			//elseif医保扣款返回成功
			} else if ("0".equals(tMEDICAL_INFO.getIFSUCCESS())){
				//医保扣款成功，但是只是医保扣款的情况
				if("1".equals(tMEDICAL_INFO.getPAYMODE())){
					String dateString = StringPattern(tMEDICAL_INFO.getPAYDATE(),"yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss");
					String date=dateString.substring(0, 10);
					String time=dateString.substring(11, 19);
					//更新ljtempfeeclass
					sqlTempfeclass ="update LJTempFeeClass set PayMode='8',BankCode='"+mLCContSchema.getBankCode()
								+"' ,bankaccno ='"+ mLCContSubSchema.getMedicalCode() 
								+"' ,EnterAccDate ='"+ date 
								+"' ,ConfMakeDate ='"+ date 
								+"' ,ConfMakeTime ='"+ time
								+"',ModifyDate= current date,ModifyTime=current time where TempFeeNo='" + mLJTempFeeclassSchema.getTempFeeNo()+"' and paymode='8'";
					//更新ljtempfee表	
					sqlTempe ="update LJTempFee set EnterAccDate ='"+ date 
								+"' ,ConfMakeDate ='"+ date 
								+"' ,ConfMakeTime ='"+ time
								+"',ModifyDate= current date,ModifyTime=current time where TempFeeNo='" + mLJTempFeeSchema.getTempFeeNo()+ "'";
					//更新lccont表	
					sqlCont = "update lccont set paymode='8',expaymode='8' where prtno ='" + mLCContSchema.getPrtNo()+ "'";
					//更新lcpol表	
					sqlLcpol="update lcpol set paymode='8',expaymode='8' where prtno ='" + mLCContSchema.getPrtNo()+ "'";
				
				//医保扣款成功，仅为银行卡扣款的情况
				}
				/*else if("2".equals(tMEDICAL_INFO.getPAYMODE())){
					String dateString = StringPattern(tMEDICAL_INFO.getPAYDATE(),"yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss");
					String date=dateString.substring(0, 10);
					String time=dateString.substring(11, 19);
					//更新ljtempfeeclass
					sqlTempfeclass ="update LJTempFeeClass set PayMode='4',BankCode='"+mLCContSchema.getBankCode()
								+"' ,bankaccno ='"+ mLCContSubSchema.getMedicalCode() 
								+"' ,EnterAccDate ='"+ date 
								+"' ,ConfMakeDate ='"+ date 
								+"' ,ConfMakeTime ='"+ time
								+"',ModifyDate= current date,ModifyTime=current time where TempFeeNo='" + mLJTempFeeclassSchema.getTempFeeNo()+"' and paymode='4'";
					//更新ljtempfee表
					sqlTempe ="update LJTempFee set EnterAccDate ='"+ date 
								+"' ,ConfMakeDate ='"+ date 
								+"' ,ConfMakeTime ='"+ time
								+"',ModifyDate= current date,ModifyTime=current time where TempFeeNo='" + mLJTempFeeSchema.getTempFeeNo()+ "'";
					//更新lccont表
					sqlCont = "update lccont set paymode='4',expaymode='4' where prtno ='" + mLCContSchema.getPrtNo()+ "'";
					//更新lcpol表
					sqlLcpol="update lcpol set paymode='4',expaymode='4' where prtno ='" + mLCContSchema.getPrtNo()+ "'";
				
				}	*/	
				tmap.put(sqlTempfeclass, "UPDATE");
				tmap.put(sqlTempe, "UPDATE");
				tmap.put(sqlCont, "UPDATE");
				tmap.put(sqlLcpol, "UPDATE");
				//保存日志
				SetPrtNo(tPrtNo);
				errLogSY("医保扣款已经成功","1");
			
			// 提交数据		
            VData mInputData = new VData();
			mInputData.clear();
			mInputData.add(tmap);
			if(!tPubSubmit.submitData(mInputData, "")){
				System.out.println("医保扣款数据保存出错！");
				CError.buildErr(this, "医保扣款数据保存出错！");
				//保存日志
				SetPrtNo(tPrtNo);
				mError="医保扣款数据保存出错！";
				errLogSY(mError,"0");
	            return false;
			}
			System.out.println("医保扣款确认处理成功！");
			//保存日志
			SetPrtNo(tPrtNo);
			errLogSY("医保扣款确认成功！","1");
			}
		}
		return true;
	}
	/*
	 * 签单逻辑处理
	 * */
	private boolean SignPolicy(){
		try {
			//沈阳医保签单部分	
			System.out.println("=============开始处理沈阳医保签单=================");
			LCContSet tLCContSet = new LCContSet();
			TransferData tTransferData = new TransferData();
			LCContSchema tLCContSchema = new LCContSchema ();
			//查询保单的工作流节点ID
	        String strSql = "select distinct MissionId from ("
	                + "select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
	                + "and MissionProp1 = '"
	                + tPrtNo
	                + "' and ActivityId in "
	                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp1') "
	                + "union select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
	                + "and MissionProp2 = '"
	                + tPrtNo
	                + "' and ActivityId in "
	                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp2') "
	                + "union select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
	                + "and MissionProp5 = '"
	                + tPrtNo
	                + "' and ActivityId in "
	                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp5') "
	                + ") as X";
	        System.out.println(strSql);
	        SSRS tSSRS = new ExeSQL().execSQL(strSql);
	        if (tSSRS.getMaxRow() != 1)
	        {
	            CError tError = new CError();
	            tError.moduleName = "ApplyRecallPolBL";
	            tError.functionName = "getBaseData";
	            tError.errorMessage = "保单工作流任务ID查询失败！印刷号：" + tPrtNo;
	            mErrors.addOneError(tError);
	            return false;
	        }
	        String mMissionID = tSSRS.GetText(1, 1);	
			//封装Schema
			tLCContSchema.setContNo(tContNo);
		 	tLCContSchema.setPrtNo(tPrtNo);
   			tLCContSet.add( tLCContSchema );
			//封装TransferData
   			tTransferData.setNameAndValue("MissionID",mMissionID);
 			tTransferData.setNameAndValue("SubMissionID","1");			
 			//封装VData
 			VData tVData = new VData();
			tVData.add( tGI );
			tVData.add( tLCContSet );
			tVData.add( tTransferData );		
			TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
			if (!tTbWorkFlowUI.submitData( tVData, "0000001150")){
				System.out.println("签单失败！");			
				//保存日志
				this.mErrors.copyAllErrors(tTbWorkFlowUI.mErrors);
				SetPrtNo(tPrtNo);
				mError ="签单失败！"+this.mErrors.getFirstError();
				errLogSY(mError,"0");
				return false;
				}
			}catch (Exception ex) {
				System.out.println("签单逻辑异常！");
				//保存日志
				SetPrtNo(tPrtNo);
				mError ="签单失败！"+this.mErrors.getFirstError();
				errLogSY(mError,"0");
				}
		System.out.println("签单成功！");
		//保存日志
		SetPrtNo(tPrtNo);
		errLogSY("签单成功！", "1");
		//签单完成后重新查询新的ContNo
		String sql = "select contno from lccont where prtno = '"+tPrtNo+"' with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		nContNo = tSSRS.GetText(1, 1);
		return true;
	}
	/*
	 * 保单打印处理
	 * */
	private boolean printPolicy() {
		System.out.println("=============开始处理沈阳医保保单打印=================");
		//更新lccont表的printcount字段
		String sql = "update lccont set printcount = '1' where prtno = '"+tPrtNo+"' ";
		//更新lccontgetpol表
		String sql1 = "select la.name,lc.agentcode,lc.prem,lc.appntname,lc.appntno,lc.signdate,lc.cvalidate,lc.printcount "
				+ "from laagent la , lccont lc where la.agentcode=lc.agentcode and lc.prtno = '"+tPrtNo+"' with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql1);
		//更新LCContGetPol表
		LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
		tLCContGetPolSchema.setContNo(nContNo);
		tLCContGetPolSchema.setPrtNo(tPrtNo);
		tLCContGetPolSchema.setContType("1");
		tLCContGetPolSchema.setCValiDate(tSSRS.GetText(1, 7));
		tLCContGetPolSchema.setAppntNo(tSSRS.GetText(1, 5));
		tLCContGetPolSchema.setAppntName(tSSRS.GetText(1, 4));
		tLCContGetPolSchema.setPrem(tSSRS.GetText(1, 3));
		tLCContGetPolSchema.setAgentCode(tSSRS.GetText(1, 2));
		tLCContGetPolSchema.setAgentName(tSSRS.GetText(1, 1));
		tLCContGetPolSchema.setSignDate(tSSRS.GetText(1, 6));
		tLCContGetPolSchema.setManageCom(tGI.ManageCom);
		tLCContGetPolSchema.setReceiveManageCom(tGI.ManageCom);
		tLCContGetPolSchema.setReceiveOperator(tGI.Operator);
		tLCContGetPolSchema.setReceiveDate(PubFun.getCurrentDate());
		tLCContGetPolSchema.setReceiveTime(PubFun.getCurrentTime());
		tLCContGetPolSchema.setGetpolState("0");
		tLCContGetPolSchema.setGetpolDate(PubFun.getCurrentDate());
		tLCContGetPolSchema.setGetpolMan(tSSRS.GetText(1, 4));
		tLCContGetPolSchema.setSendPolMan(tSSRS.GetText(1, 1));
		tLCContGetPolSchema.setGetPolManageCom(tGI.ManageCom);
		tLCContGetPolSchema.setGetPolOperator(tGI.Operator);
		tLCContGetPolSchema.setGetpolMakeDate(PubFun.getCurrentDate());
		tLCContGetPolSchema.setGetpolMakeTime(PubFun.getCurrentTime());
		tLCContGetPolSchema.setMakeDate(PubFun.getCurrentDate());
		tLCContGetPolSchema.setMakeTime(PubFun.getCurrentTime());
		tLCContGetPolSchema.setModifyDate(PubFun.getCurrentDate());
		tLCContGetPolSchema.setModifyTime(PubFun.getCurrentTime());
		//更新LCContReceive表
		LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
		tLCContReceiveSchema.setReceiveID("1");
		tLCContReceiveSchema.setContNo(nContNo);
		tLCContReceiveSchema.setPrtNo(tPrtNo);
		tLCContReceiveSchema.setContType("1");
		tLCContReceiveSchema.setCValiDate(tSSRS.GetText(1, 7));
		tLCContReceiveSchema.setAppntNo(tSSRS.GetText(1, 5));
		tLCContReceiveSchema.setAppntName(tSSRS.GetText(1, 4));
		tLCContReceiveSchema.setPrem(tSSRS.GetText(1, 3));
		tLCContReceiveSchema.setAgentCode(tSSRS.GetText(1, 2));
		tLCContReceiveSchema.setAgentName(tSSRS.GetText(1, 1));
		tLCContReceiveSchema.setSignDate(tSSRS.GetText(1, 6));
		tLCContReceiveSchema.setManageCom(tGI.ManageCom);
		tLCContReceiveSchema.setPrintManageCom(tGI.ManageCom);
		tLCContReceiveSchema.setPrintOperator(tGI.Operator);
		tLCContReceiveSchema.setPrintDate(PubFun.getCurrentDate());
		tLCContReceiveSchema.setPrintTime(PubFun.getCurrentTime());
		tLCContReceiveSchema.setPrintCount(tSSRS.GetText(1, 8));
		tLCContReceiveSchema.setReceiveManageCom(tGI.ManageCom);
		tLCContReceiveSchema.setReceiveOperator(tGI.Operator);
		tLCContReceiveSchema.setReceiveDate(PubFun.getCurrentDate());
		tLCContReceiveSchema.setReceiveTime(PubFun.getCurrentTime());
		tLCContReceiveSchema.setReceiveState("1");
		tLCContReceiveSchema.setBackReasonCode("");
		tLCContReceiveSchema.setBackReason("");
		tLCContReceiveSchema.setDealState("0");
		tLCContReceiveSchema.setMakeDate(PubFun.getCurrentDate());
		tLCContReceiveSchema.setMakeTime(PubFun.getCurrentTime());
		tLCContReceiveSchema.setModifyDate(PubFun.getCurrentDate());
		tLCContReceiveSchema.setModifyTime(PubFun.getCurrentTime());
		//封装数据
		MMap tmap = new MMap();
		tmap.put(sql, "UPDATE");
		tmap.put(tLCContGetPolSchema,"INSERT");
		tmap.put(tLCContReceiveSchema, "INSERT");
		// 提交数据		
        VData mInputData = new VData();
		mInputData.clear();
		mInputData.add(tmap);
		if(!tPubSubmit.submitData(mInputData, "")){
			System.out.println("保存数据出错");
			CError.buildErr(this, "数据保存失败");
			//保存日志
			SetPrtNo(tPrtNo);
			errLogSY("保单打印失败！","0");
            return false;
		}
		System.out.println("保单打印成功！");
		//保存日志
		SetPrtNo(tPrtNo);
		errLogSY("保单打印成功！", "1");
		return true;
	}
	/*
	 * 回执回销逻辑处理
	 * */
	private boolean returnReceipt(){
		//沈阳医保回执回销部分
		System.out.println("=============开始处理沈阳医保回执回销=================");
		String sql = "select Appntname,Agentname,AgentCode from LCContGetPol where prtno = '"+tPrtNo+"' with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		String tGetpolMan = tSSRS.GetText(1, 1);
		String tSendPolMan = tSSRS.GetText(1, 2);
	    String tAgentCode = "D" + tSSRS.GetText(1, 3);
	    String tManageCom = "A" + tGI.ManageCom;	
		LCContGetPolSchema tLCContGetPolSchema=new LCContGetPolSchema();
		LCContGetPolUI tLCContGetPolUI =new LCContGetPolUI();
		tLCContGetPolSchema.setContNo(nContNo);
		tLCContGetPolSchema.setGetpolDate(PubFun.getCurrentDate());
		tLCContGetPolSchema.setGetpolMan(tGetpolMan);
		tLCContGetPolSchema.setSendPolMan(tSendPolMan);
		tLCContGetPolSchema.setGetPolOperator(tGI.Operator);
		tLCContGetPolSchema.setManageCom(tManageCom);
		tLCContGetPolSchema.setAgentCode(tAgentCode);
		tLCContGetPolSchema.setContType("1");
		VData vData = new VData();
		vData.addElement(tLCContGetPolSchema);
		vData.add(tGI);
		if( !tLCContGetPolUI.submitData(vData, "INSERT") ){
			System.out.println("回执回销失败！");
			//保存日志
			SetPrtNo(tPrtNo);
			errLogSY("回执回销失败！","0");
			return false;
		}
		System.out.println("回执回销成功！");
		//保存日志
		SetPrtNo(tPrtNo);
		errLogSY("回执回销成功！","1");
		return true;
	}
	/*
	 * 时间格式转化
	 * */
	public final String StringPattern(String date, String oldPattern, String newPattern) {   
        if (date == null || oldPattern == null || newPattern == null)   
            return "";   
        SimpleDateFormat sdf1 = new SimpleDateFormat(oldPattern) ;        // 实例化模板对象    
        SimpleDateFormat sdf2 = new SimpleDateFormat(newPattern) ;        // 实例化模板对象    
        Date d = null ;    
        try{    
            d = sdf1.parse(date) ;   // 将给定的字符串中的日期提取出来    
        }catch(Exception e){            // 如果提供的字符串格式有错误，则进行异常处理    
            e.printStackTrace() ;       // 打印异常信息    
        }    
        return sdf2.format(d);  
    }
	/*
	 * 返回报文
	 * */
	public void getXmlResult(){
		// 返回数据节点
		putSYResult("SUCCESS_INFO", tSUCCESS_INFO);
	}
}