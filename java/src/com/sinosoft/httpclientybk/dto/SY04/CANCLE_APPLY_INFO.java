package com.sinosoft.httpclientybk.dto.SY04;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class CANCLE_APPLY_INFO extends BaseXmlSch{

	private static final long serialVersionUID = 1L;
	private String CONTNO;		  //合同号
	private String YbPersonalNo;  //医保个人编号
	public String getCONTNO() {
		return CONTNO;
	}
	public void setCONTNO(String cONTNO) {
		CONTNO = cONTNO;
	}
	public String getYbPersonalNo() {
		return YbPersonalNo;
	}
	public void setYbPersonalNo(String ybPersonalNo) {
		YbPersonalNo = ybPersonalNo;
	}
	
}
