package com.sinosoft.httpclientybk.dto.SY03;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class MEDICAL_INFO extends BaseXmlSch {
	
	private static final long serialVersionUID = 2474102570085076829L;
	/* 缴费单号 */
	private String PAYMENTNO;
	/* 合同号 */
	private String COUNTNO;
	/* 医保个人编号 */
	private String MEDICALNO;
	/* 缴费时间:真正扣款时间 */
	private String PAYDATE;
	/* 经办人 */
	private String OPERATOR;
	/* 缴费方式:1:社保卡	2：银行卡 */
	private String PAYMODE;
	/* 缴费金额：实际扣款金额 */
	private String SUMMONEY;
	/* 是否缴费成功:0:缴费成功，可以签单，1：缴费失败 */
	private String IFSUCCESS;
	
	public String getPAYMENTNO() {
		return PAYMENTNO;
	}
	public void setPAYMENTNO(String pAYMENTNO) {
		PAYMENTNO = pAYMENTNO;
	}
	public String getCOUNTNO() {
		return COUNTNO;
	}
	public void setCOUNTNO(String cOUNTNO) {
		COUNTNO = cOUNTNO;
	}
	public String getMEDICALNO() {
		return MEDICALNO;
	}
	public void setMEDICALNO(String mEDICALNO) {
		MEDICALNO = mEDICALNO;
	}
	
	public String getPAYDATE() {
		return PAYDATE;
	}
	public void setPAYDATE(String pAYDATE) {
		PAYDATE = pAYDATE;
	}
	public String getOPERATOR() {
		return OPERATOR;
	}
	public void setOPERATOR(String oPERATOR) {
		OPERATOR = oPERATOR;
	}
	public String getPAYMODE() {
		return PAYMODE;
	}
	public void setPAYMODE(String pAYMODE) {
		PAYMODE = pAYMODE;
	}
	public String getSUMMONEY() {
		return SUMMONEY;
	}
	public void setSUMMONEY(String sUMMONEY) {
		SUMMONEY = sUMMONEY;
	}
	public String getIFSUCCESS() {
		return IFSUCCESS;
	}
	public void setIFSUCCESS(String iFSUCCESS) {
		IFSUCCESS = iFSUCCESS;
	}
	
}
