package com.sinosoft.httpclientybk.dto.SY03;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class SUCCESS_INFO extends BaseXmlSch{
	private static final long serialVersionUID = 4000555544179628768L;
	/* 缴费单号 */
	private String PAYMENTNO;
	/* 投保告知说明 */
	private String INSURESTATEMENT;
	
	public String getPAYMENTNO() {
		return PAYMENTNO;
	}
	public void setPAYMENTNO(String pAYMENTNO) {
		PAYMENTNO = pAYMENTNO;
	}
	public String getINSURESTATEMENT() {
		return INSURESTATEMENT;
	}
	public void setINSURESTATEMENT(String iNSURESTATEMENT) {
		INSURESTATEMENT = iNSURESTATEMENT;
	}

}
