package com.sinosoft.httpclientybk.dto.SY05;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class ENDORSEMENT_INFO extends BaseXmlSch{
	private static final long serialVersionUID = 730507845663295828L;
	/* 缴费单号 */
	private String PAYMENTNO;
	/* 医保个人编号 */
	private String MEDICALNO;
	
	public String getPAYMENTNO() {
		return PAYMENTNO;
	}
	public void setPAYMENTNO(String pAYMENTNO) {
		PAYMENTNO = pAYMENTNO;
	}
	public String getMEDICALNO() {
		return MEDICALNO;
	}
	public void setMEDICALNO(String mEDICALNO) {
		MEDICALNO = mEDICALNO;
	}
}
