package com.sinosoft.httpclientybk.dto.SY05;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class SUCCESS_INFO extends BaseXmlSch{
	private static final long serialVersionUID = -2620251358879729286L;
	/* 撤单结果：1-可以撤单 0-不能撤单 */
	private String IF_CAN_CANCEL;
	/* 撤单原因 */
	private String ERROR_REASON;
	
	public String getIF_CAN_CANCEL() {
		return IF_CAN_CANCEL;
	}
	public void setIF_CAN_CANCEL(String iFCANCANCEL) {
		IF_CAN_CANCEL = iFCANCANCEL;
	}
	public String getERROR_REASON() {
		return ERROR_REASON;
	}
	public void setERROR_REASON(String eRRORREASON) {
		ERROR_REASON = eRRORREASON;
	}
	

}
