package com.sinosoft.httpclientybk.dto.SYTB02;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class INSURE_INFO extends BaseXmlSch {

	private static final long serialVersionUID = -3808780886728766606L;
	/**被保人性别  0:男，1：女，2：其他 */
	private String INSUED_GENDER;
	/**被保人出生年月*/
	private String INSUED_BIRTHDAY;
	/**套餐产品编码*/
	private String WRAP_CODE;
	/**保障年期*/
	private String INSUREYEAR_FLAG;
	/**缴费年限*/
	private String PAY_END_YEAR;
	/**缴费频次 0 趸缴; 1年缴*/
	private String PAY_INTV;
	/**投保申请日期*/
	private String APPLICATION_DATE;
	/**保险生效时间*/
	private String EFFECTIVE_DATE;
	/**保额*/
	private String SUM_INSURED;
	/**职业类别编码*/
	private String OCCUPACTIONCODE;
	public String getINSUED_GENDER()
	{
		return INSUED_GENDER;
	}
	public void setINSUED_GENDER(String iNSUED_GENDER)
	{
		INSUED_GENDER = iNSUED_GENDER;
	}
	public String getINSUED_BIRTHDAY()
	{
		return INSUED_BIRTHDAY;
	}
	public void setINSUED_BIRTHDAY(String iNSUED_BIRTHDAY)
	{
		INSUED_BIRTHDAY = iNSUED_BIRTHDAY;
	}
	public String getWRAP_CODE()
	{
		return WRAP_CODE;
	}
	public void setWRAP_CODE(String wRAP_CODE)
	{
		WRAP_CODE = wRAP_CODE;
	}
	public String getINSUREYEAR_FLAG()
	{
		return INSUREYEAR_FLAG;
	}
	public void setINSUREYEAR_FLAG(String iNSUREYEAR_FLAG)
	{
		INSUREYEAR_FLAG = iNSUREYEAR_FLAG;
	}
	public String getPAY_END_YEAR()
	{
		return PAY_END_YEAR;
	}
	public void setPAY_END_YEAR(String pAY_END_YEAR)
	{
		PAY_END_YEAR = pAY_END_YEAR;
	}
	public String getPAY_INTV()
	{
		return PAY_INTV;
	}
	public void setPAY_INTV(String pAY_INTV)
	{
		PAY_INTV = pAY_INTV;
	}
	public String getAPPLICATION_DATE()
	{
		return APPLICATION_DATE;
	}
	public void setAPPLICATION_DATE(String aPPLICATION_DATE)
	{
		APPLICATION_DATE = aPPLICATION_DATE;
	}
	public String getEFFECTIVE_DATE()
	{
		return EFFECTIVE_DATE;
	}
	public void setEFFECTIVE_DATE(String eFFECTIVE_DATE)
	{
		EFFECTIVE_DATE = eFFECTIVE_DATE;
	}
	public String getSUM_INSURED()
	{
		return SUM_INSURED;
	}
	public void setSUM_INSURED(String sUM_INSURED)
	{
		SUM_INSURED = sUM_INSURED;
	}
	public String getOCCUPACTIONCODE()
	{
		return OCCUPACTIONCODE;
	}
	public void setOCCUPACTIONCODE(String oCCUPACTIONCODE)
	{
		OCCUPACTIONCODE = oCCUPACTIONCODE;
	}
	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}
	

	
	
	
	
}
