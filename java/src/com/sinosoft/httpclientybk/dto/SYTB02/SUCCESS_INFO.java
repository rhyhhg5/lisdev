package com.sinosoft.httpclientybk.dto.SYTB02;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;
/**
 * 
 * @author db2admin
 * 返回信息
 */
public class SUCCESS_INFO  extends BaseXmlSch{
	
   private static final long serialVersionUID = -4338806920933749550L;

   /** APP投保单号*/
   private String Prem = null;
   private String ErrorMsg  = null;
   private String ErrorCode  = null;

public String getPrem()
{
	return Prem;
}

public void setPrem(String prem)
{
	Prem = prem;
}

public String getErrorMsg()
{
	return ErrorMsg;
}

public void setErrorMsg(String errorMsg)
{
	ErrorMsg = errorMsg;
}

public String getErrorCode()
{
	return ErrorCode;
}

public void setErrorCode(String errorCode)
{
	ErrorCode = errorCode;
}

public static long getSerialversionuid()
{
	return serialVersionUID;
}
   

   
}
