//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.09.25 at 11:24:34 ���� CST 
//


package com.sinosoft.httpclientybk.dto.u01.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "underwrit_sequence_no",
    "insuredList"
})
@XmlRootElement(name = "SUCCESS_INFO")
public class SuccessInfo {
	
    @XmlElement(name = "UNDERWRIT_SEQUENCE_NO", required = true, nillable = true)
    protected String underwrit_sequence_no;
    
    @XmlElement(name = "INSURED_LIST", required = true, nillable = true)
    protected InsuredList insuredList;
    
    public SuccessInfo(){
    	insuredList = new InsuredList();
    }

	public String getUnderwrit_sequence_no() {
		return underwrit_sequence_no;
	}

	public void setUnderwrit_sequence_no(String underwrit_sequence_no) {
		this.underwrit_sequence_no = underwrit_sequence_no;
	}

	public InsuredList getInsuredList() {
		return insuredList;
	}

	public void setInsuredList(InsuredList insuredList) {
		this.insuredList = insuredList;
	}
	
	public static void main(String[] args) throws Exception {
//		SuccessInfo successInfo = new SuccessInfo();
//		successInfo.setUnderwrit_sequence_no("000000");
//		
//		CustomerInfo customerInfo = new CustomerInfo();
//		successInfo.getInsuredList().getCustomer_info().add(customerInfo);
//		
//		RiskInfo e = new RiskInfo();
//		e.setAll_health_sum_insured(666.66);
//		customerInfo.getRisk_list().getRisk_info().add(e);
//		
//		String xml = XmlParseUtil.dtoToXml(successInfo);
//		System.out.println(xml);

	}

}
