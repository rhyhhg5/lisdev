package com.sinosoft.httpclientybk.dto.G06;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class MEDICAL_INFO extends BaseXmlSch {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 保单编码 */
	private String POLICY_SEQUENCE_NO;
	
	/** 保全退款流水号 */
	private String END_REFUND_NO;
	
	/** 医保退款流水号 */
	private String YB_TK_SEQUENCE_NO;
	
	/** 医保退费是否成功 */
	private String REFUND_IF_SUCCESS;
	
	/** 退款失败原因 */
	private String REFUND_FAIL_REASON;
	
	/** 退款时间 */
	private String REFUND_TIME;
	
	/** 原保费*/
	private String ORIGINAL_PREMIUM;
	
	/** 医保实际退款金额 */
	private String ACTUAL_CANCEL_PREMIUM;
	

	public String getORIGINAL_PREMIUM() {
		return ORIGINAL_PREMIUM;
	}

	public void setORIGINAL_PREMIUM(String oRIGINALPREMIUM) {
		ORIGINAL_PREMIUM = oRIGINALPREMIUM;
	}

	public String getACTUAL_CANCEL_PREMIUM() {
		return ACTUAL_CANCEL_PREMIUM;
	}

	public void setACTUAL_CANCEL_PREMIUM(String aCTUALCANCELPREMIUM) {
		ACTUAL_CANCEL_PREMIUM = aCTUALCANCELPREMIUM;
	}

	public String getPOLICY_SEQUENCE_NO() {
		return POLICY_SEQUENCE_NO;
	}

	public void setPOLICY_SEQUENCE_NO(String pOLICYSEQUENCENO) {
		POLICY_SEQUENCE_NO = pOLICYSEQUENCENO;
	}

	public String getEND_REFUND_NO() {
		return END_REFUND_NO;
	}

	public void setEND_REFUND_NO(String eNDREFUNDNO) {
		END_REFUND_NO = eNDREFUNDNO;
	}

	public String getYB_TK_SEQUENCE_NO() {
		return YB_TK_SEQUENCE_NO;
	}

	public void setYB_TK_SEQUENCE_NO(String yBTKSEQUENCENO) {
		YB_TK_SEQUENCE_NO = yBTKSEQUENCENO;
	}

	public String getREFUND_IF_SUCCESS() {
		return REFUND_IF_SUCCESS;
	}

	public void setREFUND_IF_SUCCESS(String rEFUNDIFSUCCESS) {
		REFUND_IF_SUCCESS = rEFUNDIFSUCCESS;
	}

	public String getREFUND_FAIL_REASON() {
		return REFUND_FAIL_REASON;
	}

	public void setREFUND_FAIL_REASON(String rEFUNDFAILREASON) {
		REFUND_FAIL_REASON = rEFUNDFAILREASON;
	}

	public String getREFUND_TIME() {
		return REFUND_TIME;
	}

	public void setREFUND_TIME(String rEFUNDTIME) {
		REFUND_TIME = rEFUNDTIME;
	}
	
}
