package com.sinosoft.httpclientybk.dto.G07;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class CLAIM_APPLY_INFO extends BaseXmlSch{
	
	private static final long serialVersionUID = 1L;
	private String POLICY_SEQUENCE_NO;	//保单编码
	private String CLAIM_FORMER_NO = "";	// 理赔流水号
	private String APPOINT_NAME = "";	//委托人
	private String REPORT_NAME = "";		//报案人
	private String RELATIONSHIP = "";	//报案人与出险人关系
	private String REPORT_PHONE = "";		//联系电话
	private String REPORT_ADDR = "";		//联系地址
	private String HOSPITAL = "";		//治疗医院
	private String ADMISSION_DIAGNOSIS = "";		//入院诊断
	private String ACCIDENT_DATE = "";		//出险日期
	private String ACCIDENT_DES = "";		//事故说明
	
	public String getPOLICY_SEQUENCE_NO() {
		return POLICY_SEQUENCE_NO;
	}
	public void setPOLICY_SEQUENCE_NO(String pOLICY_SEQUENCE_NO) {
		POLICY_SEQUENCE_NO = pOLICY_SEQUENCE_NO;
	}
	public String getCLAIM_FORMER_NO() {
		return CLAIM_FORMER_NO;
	}
	public void setCLAIM_FORMER_NO(String cLAIM_FORMER_NO) {
		CLAIM_FORMER_NO = cLAIM_FORMER_NO;
	}
	public String getAPPOINT_NAME() {
		return APPOINT_NAME;
	}
	public void setAPPOINT_NAME(String aPPOINT_NAME) {
		APPOINT_NAME = aPPOINT_NAME;
	}
	public String getREPORT_NAME() {
		return REPORT_NAME;
	}
	public void setREPORT_NAME(String rEPORT_NAME) {
		REPORT_NAME = rEPORT_NAME;
	}
	public String getRELATIONSHIP() {
		return RELATIONSHIP;
	}
	public void setRELATIONSHIP(String rELATIONSHIP) {
		RELATIONSHIP = rELATIONSHIP;
	}
	public String getREPORT_PHONE() {
		return REPORT_PHONE;
	}
	public void setREPORT_PHONE(String rEPORT_PHONE) {
		REPORT_PHONE = rEPORT_PHONE;
	}
	public String getREPORT_ADDR() {
		return REPORT_ADDR;
	}
	public void setREPORT_ADDR(String rEPORT_ADDR) {
		REPORT_ADDR = rEPORT_ADDR;
	}
	public String getHOSPITAL() {
		return HOSPITAL;
	}
	public void setHOSPITAL(String hOSPITAL) {
		HOSPITAL = hOSPITAL;
	}
	public String getADMISSION_DIAGNOSIS() {
		return ADMISSION_DIAGNOSIS;
	}
	public void setADMISSION_DIAGNOSIS(String aDMISSION_DIAGNOSIS) {
		ADMISSION_DIAGNOSIS = aDMISSION_DIAGNOSIS;
	}
	public String getACCIDENT_DATE() {
		return ACCIDENT_DATE;
	}
	public void setACCIDENT_DATE(String aCCIDENT_DATE) {
		ACCIDENT_DATE = aCCIDENT_DATE;
	}
	public String getACCIDENT_DES() {
		return ACCIDENT_DES;
	}
	public void setACCIDENT_DES(String aCCIDENT_DES) {
		ACCIDENT_DES = aCCIDENT_DES;
	}

}
