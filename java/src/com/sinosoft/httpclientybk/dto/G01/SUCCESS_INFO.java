package com.sinosoft.httpclientybk.dto.G01;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;
/**
 * 
 * @author db2admin
 * 返回信息
 */
public class SUCCESS_INFO  extends BaseXmlSch{
	private static final long serialVersionUID = 6172421505530313164L;
	
	 /** 投保流水号 */
   private String POLICY_FORMER_NO = null;

   /** 自核结论*/
   private String SELF_UNDERWRITING_RESULT = null;

   /** 自核结论描述 */
   private String SELF_UNDERWRITING_RESULT_DES = null;

public String getPOLICY_FORMER_NO() {
	return POLICY_FORMER_NO;
}

public void setPOLICY_FORMER_NO(String pOLICY_FORMER_NO) {
	POLICY_FORMER_NO = pOLICY_FORMER_NO;
}

public String getSELF_UNDERWRITING_RESULT() {
	return SELF_UNDERWRITING_RESULT;
}

public void setSELF_UNDERWRITING_RESULT(String sELF_UNDERWRITING_RESULT) {
	SELF_UNDERWRITING_RESULT = sELF_UNDERWRITING_RESULT;
}

public String getSELF_UNDERWRITING_RESULT_DES() {
	return SELF_UNDERWRITING_RESULT_DES;
}

public void setSELF_UNDERWRITING_RESULT_DES(String sELF_UNDERWRITING_RESULT_DES) {
	SELF_UNDERWRITING_RESULT_DES = sELF_UNDERWRITING_RESULT_DES;
}
   
}
