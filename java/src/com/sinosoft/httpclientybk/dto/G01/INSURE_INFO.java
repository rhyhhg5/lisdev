package com.sinosoft.httpclientybk.dto.G01;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class INSURE_INFO extends BaseXmlSch{
	private static final long serialVersionUID = 4552158355708201810L;
	private String POLICY_FORMER_NO;
	private String CUSTOMER_SEQUENCE_NO;
	private String NAME;
	private String GENDER;
	private String BIRTHDAY;
	private String CERTIFICATE_TYPE;
	private String CERTIFICATE_NO;
	private String MEDICAL_NO;
	private String BANK_NO;
	private String BANK_NAME;
	private String MOBILE;
	private String VALIDATE_MODE;
	private String MEDICAL_TYPE;
	private String APPLICATION_DATE;
	private String EFFECTIVE_DATE;
	private String EXPIRE_DATE;
	private String IF_AUTO_RENEWAL;
	private String DEDUCT_METHOD;
	private String RENEWAL_DEDUCT_METHOD;
	private String ADDRESS;
	private String EMAIL;
	private String SUM_INSURED;
	private String SALES_CHANNEL;
	private String SALES_ORGANIZATION;
	private String SALES_CODE;
	private String INSURE_CHANNEL;
	private String HEIGHT;
	private String WEIGHT;
	private String QUESTION_ONE;
	private String QUESTION_TWO;
	private String QUESTION_TWO_SICK;
	private String QUESTION_THREE;
	private String QUESTION_FOUR;
	private String QUESTION_FIVE;
	private String QUESTION_SIX;
	private String QUESTION_SEVEN;
	
	
	
	public String getQUESTION_ONE() {
		return QUESTION_ONE;
	}
	public void setQUESTION_ONE(String qUESTION_ONE) {
		QUESTION_ONE = qUESTION_ONE;
	}
	public String getQUESTION_TWO() {
		return QUESTION_TWO;
	}
	public void setQUESTION_TWO(String qUESTION_TWO) {
		QUESTION_TWO = qUESTION_TWO;
	}
	public String getHEIGHT() {
		return HEIGHT;
	}
	public void setHEIGHT(String hEIGHT) {
		HEIGHT = hEIGHT;
	}
	public String getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}
	public String getPOLICY_FORMER_NO() {
		return POLICY_FORMER_NO;
	}
	public void setPOLICY_FORMER_NO(String pOLICY_FORMER_NO) {
		if(getPOLICY_FORMER_NO()==null)
		POLICY_FORMER_NO = pOLICY_FORMER_NO;
	}
	public String getCUSTOMER_SEQUENCE_NO() {
		return CUSTOMER_SEQUENCE_NO;
	}
	public void setCUSTOMER_SEQUENCE_NO(String cUSTOMER_SEQUENCE_NO) {
		if(getCUSTOMER_SEQUENCE_NO()==null)
		CUSTOMER_SEQUENCE_NO = cUSTOMER_SEQUENCE_NO;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		if(getNAME()==null)
		NAME = nAME;
	}
	public String getGENDER() {
		return GENDER;
	}
	public void setGENDER(String gENDER) {
		if(getGENDER()==null)
		GENDER = gENDER;
	}
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		if(getBIRTHDAY()==null)
		BIRTHDAY = bIRTHDAY;
	}
	public String getCERTIFICATE_TYPE() {
		return CERTIFICATE_TYPE;
	}
	public void setCERTIFICATE_TYPE(String cERTIFICATE_TYPE) {
		if(getCERTIFICATE_TYPE()==null)
		CERTIFICATE_TYPE = cERTIFICATE_TYPE;
	}
	public String getCERTIFICATE_NO() {
		return CERTIFICATE_NO;
	}
	public void setCERTIFICATE_NO(String cERTIFICATE_NO) {
		if(getCERTIFICATE_NO()==null)
		CERTIFICATE_NO = cERTIFICATE_NO;
	}
	public String getMEDICAL_NO() {
		return MEDICAL_NO;
	}
	public void setMEDICAL_NO(String mEDICAL_NO) {
		if(getMEDICAL_NO()==null)
		MEDICAL_NO = mEDICAL_NO;
	}
	public String getBANK_NO() {
		return BANK_NO;
	}
	public void setBANK_NO(String bANK_NO) {
		if(getBANK_NO()==null)
		BANK_NO = bANK_NO;
	}
	public String getBANK_NAME() {
		return BANK_NAME;
	}
	public void setBANK_NAME(String bANK_NAME) {
		if(getBANK_NAME()==null)
		BANK_NAME = bANK_NAME;
	}
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		if(getMOBILE()==null)
		MOBILE = mOBILE;
	}
	public String getVALIDATE_MODE() {
		return VALIDATE_MODE;
	}
	public void setVALIDATE_MODE(String vALIDATE_MODE) {
		if(getVALIDATE_MODE()==null)
		VALIDATE_MODE = vALIDATE_MODE;
	}
	public String getMEDICAL_TYPE() {
		return MEDICAL_TYPE;
	}
	public void setMEDICAL_TYPE(String mEDICAL_TYPE) {
		if(getMEDICAL_TYPE()==null)
		MEDICAL_TYPE = mEDICAL_TYPE;
	}
	public String getAPPLICATION_DATE() {
		return APPLICATION_DATE;
	}
	public void setAPPLICATION_DATE(String aPPLICATION_DATE) {
		if(getAPPLICATION_DATE()==null)
		APPLICATION_DATE = aPPLICATION_DATE;
	}
	public String getEFFECTIVE_DATE() {
		return EFFECTIVE_DATE;
	}
	public void setEFFECTIVE_DATE(String eFFECTIVE_DATE) {
		if(getEFFECTIVE_DATE()==null)
		EFFECTIVE_DATE = eFFECTIVE_DATE;
	}
	public String getEXPIRE_DATE() {
		return EXPIRE_DATE;
	}
	public void setEXPIRE_DATE(String eXPIRE_DATE) {
		if(getEXPIRE_DATE()==null)
		EXPIRE_DATE = eXPIRE_DATE;
	}
	public String getIF_AUTO_RENEWAL() {
		return IF_AUTO_RENEWAL;
	}
	public void setIF_AUTO_RENEWAL(String iF_AUTO_RENEWAL) {
		if(getIF_AUTO_RENEWAL()==null)
		IF_AUTO_RENEWAL = iF_AUTO_RENEWAL;
	}
	public String getDEDUCT_METHOD() {
		return DEDUCT_METHOD;
	}
	public void setDEDUCT_METHOD(String dEDUCT_METHOD) {
		if(getDEDUCT_METHOD()==null)
		DEDUCT_METHOD = dEDUCT_METHOD;
	}
	public String getRENEWAL_DEDUCT_METHOD() {
		return RENEWAL_DEDUCT_METHOD;
	}
	public void setRENEWAL_DEDUCT_METHOD(String rENEWAL_DEDUCT_METHOD) {
		if(getRENEWAL_DEDUCT_METHOD()==null)
		RENEWAL_DEDUCT_METHOD = rENEWAL_DEDUCT_METHOD;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		if(getADDRESS()==null)
		ADDRESS = aDDRESS;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		if(getEMAIL()==null)
		EMAIL = eMAIL;
	}
	public String getSUM_INSURED() {
		return SUM_INSURED;
	}
	public void setSUM_INSURED(String sUM_INSURED) {
		if(getSUM_INSURED()==null)
		SUM_INSURED = sUM_INSURED;
	}
	public String getSALES_CHANNEL() {
		return SALES_CHANNEL;
	}
	public void setSALES_CHANNEL(String sALES_CHANNEL) {
		if(getSALES_CHANNEL()==null)
		SALES_CHANNEL = sALES_CHANNEL;
	}
	public String getSALES_ORGANIZATION() {
		return SALES_ORGANIZATION;
	}
	public void setSALES_ORGANIZATION(String sALES_ORGANIZATION) {
		if(getSALES_ORGANIZATION()==null)
		SALES_ORGANIZATION = sALES_ORGANIZATION;
	}
	public String getSALES_CODE() {
		return SALES_CODE;
	}
	public void setSALES_CODE(String sALES_CODE) {
		if(getSALES_CODE()==null)
		SALES_CODE = sALES_CODE;
	}
	public String getINSURE_CHANNEL() {
		return INSURE_CHANNEL;
	}
	public void setINSURE_CHANNEL(String iNSURE_CHANNEL) {
		if(getINSURE_CHANNEL()==null)
		INSURE_CHANNEL = iNSURE_CHANNEL;
	}
	public String getQUESTION_TWO_SICK() {
		return QUESTION_TWO_SICK;
	}
	public void setQUESTION_TWO_SICK(String qUESTION_TWO_SICK) {
		QUESTION_TWO_SICK = qUESTION_TWO_SICK;
	}
	public String getQUESTION_THREE() {
		return QUESTION_THREE;
	}
	public void setQUESTION_THREE(String qUESTION_THREE) {
		QUESTION_THREE = qUESTION_THREE;
	}
	public String getQUESTION_FOUR() {
		return QUESTION_FOUR;
	}
	public void setQUESTION_FOUR(String qUESTION_FOUR) {
		QUESTION_FOUR = qUESTION_FOUR;
	}
	public String getQUESTION_FIVE() {
		return QUESTION_FIVE;
	}
	public void setQUESTION_FIVE(String qUESTION_FIVE) {
		QUESTION_FIVE = qUESTION_FIVE;
	}
	public String getQUESTION_SIX() {
		return QUESTION_SIX;
	}
	public void setQUESTION_SIX(String qUESTION_SIX) {
		QUESTION_SIX = qUESTION_SIX;
	}
	public String getQUESTION_SEVEN() {
		return QUESTION_SEVEN;
	}
	public void setQUESTION_SEVEN(String qUESTION_SEVEN) {
		QUESTION_SEVEN = qUESTION_SEVEN;
	}
	

}
