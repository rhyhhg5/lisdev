package com.sinosoft.httpclientybk.dto.G03;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class MEDICAL_INFO extends BaseXmlSch {
	private static final long serialVersionUID = 6172421505530313164L;
	private String POLICY_FORMER_NO;
	private String YB_KK_SEQUENCE_NO;
	private String MEDICAL_IF_SUCESS;
	private String DEDUCT_MONEY;
	private String FAIL_REASON;
	private String IF_SPECIAL_PERSON;
	private String ACTUAL_DEDUCT_PREMIUM;
	public String getPOLICY_FORMER_NO() {
		return POLICY_FORMER_NO;
	}
	public void setPOLICY_FORMER_NO(String pOLICYFORMERNO) {
		POLICY_FORMER_NO = pOLICYFORMERNO;
	}
	public String getYB_KK_SEQUENCE_NO() {
		return YB_KK_SEQUENCE_NO;
	}
	public void setYB_KK_SEQUENCE_NO(String yBKKSEQUENCENO) {
		YB_KK_SEQUENCE_NO = yBKKSEQUENCENO;
	}
	public String getMEDICAL_IF_SUCESS() {
		return MEDICAL_IF_SUCESS;
	}
	public void setMEDICAL_IF_SUCESS(String mEDICALIFSUCESS) {
		MEDICAL_IF_SUCESS = mEDICALIFSUCESS;
	}
	public String getDEDUCT_MONEY() {
		return DEDUCT_MONEY;
	}
	public void setDEDUCT_MONEY(String dEDUCTMONEY) {
		DEDUCT_MONEY = dEDUCTMONEY;
	}
	public String getFAIL_REASON() {
		return FAIL_REASON;
	}
	public void setFAIL_REASON(String fAILREASON) {
		FAIL_REASON = fAILREASON;
	}
	public String getIF_SPECIAL_PERSON() {
		return IF_SPECIAL_PERSON;
	}
	public void setIF_SPECIAL_PERSON(String iFSPECIALPERSON) {
		IF_SPECIAL_PERSON = iFSPECIALPERSON;
	}
	public String getACTUAL_DEDUCT_PREMIUM() {
		return ACTUAL_DEDUCT_PREMIUM;
	}
	public void setACTUAL_DEDUCT_PREMIUM(String aCTUAL_DEDUCT_PREMIUM) {
		ACTUAL_DEDUCT_PREMIUM = aCTUAL_DEDUCT_PREMIUM;
	}
	
	
}
