//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.17 at 09:25:07 上午 CST 
//
package com.sinosoft.httpclientybk.dto.e01.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>ClassName:  E01Request</p>
 * <p>Description: E01Request类文件</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2017-1-12 上午09:43:35
 * @author Yu ZhiWei
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "head",
    "requestNodes"
})

@XmlRootElement(name = "E01Request")
public class E01Request {

    @XmlElement(name = "Head", required = true)
    protected Head head;
    
    @XmlElement(name = "RequestNodes", required = true)
    protected RequestNodes requestNodes;
    
    public E01Request(){
    	head = new Head();
    	requestNodes = new RequestNodes();
    }
    /**
     *   提供公共的set和get方法  
     */
    public Head getHead() {
        return head;
    }

    public void setHead(Head value) {
        this.head = value;
    }

    public RequestNodes getRequestNodes() {
        return requestNodes;
    }

    public void setRequestNodes(RequestNodes value) {
        this.requestNodes = value;
    }

}
