package com.sinosoft.httpclientybk.dto.G05;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class SUCCESS_INFO extends BaseXmlSch{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4000555544179628768L;
	private String IF_CAN_CANCEL;
	private String CANCEL_PREMIUM;
	private String ERROR_REASON;
	public String getIF_CAN_CANCEL() {
		return IF_CAN_CANCEL;
	}
	public void setIF_CAN_CANCEL(String iFCANCANCEL) {
		IF_CAN_CANCEL = iFCANCANCEL;
	}
	public String getCANCEL_PREMIUM() {
		return CANCEL_PREMIUM;
	}
	public void setCANCEL_PREMIUM(String cANCELPREMIUM) {
		CANCEL_PREMIUM = cANCELPREMIUM;
	}
	public String getERROR_REASON() {
		return ERROR_REASON;
	}
	public void setERROR_REASON(String eRRORREASON) {
		ERROR_REASON = eRRORREASON;
	}
	

}
