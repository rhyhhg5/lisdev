package com.sinosoft.httpclientybk.dto.G05;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class ENDORSEMENT_INFO extends BaseXmlSch{
	private static final long serialVersionUID = -7340206901535895231L;
	private String POLICY_SEQUENCE_NO;
	private String ENDORSEMENT_APPLICATION_DATE;
	private String END_APPLY_NO;
	private String IF_CANCEL;
	private String CANCEL_REASON;
	private String IF_AUTO_RENEWAL;
	private String RENEWAL_DEDUCT_METHOD;
	private String MEDICAL_NO;
	private String BANK_NO;
	private String BANK_NAME;
	private String MOBILE;
	private String ADDRESS;
	public String getPOLICY_SEQUENCE_NO() {
		return POLICY_SEQUENCE_NO;
	}
	public void setPOLICY_SEQUENCE_NO(String pOLICYSEQUENCENO) {
		POLICY_SEQUENCE_NO = pOLICYSEQUENCENO;
	}
	public String getENDORSEMENT_APPLICATION_DATE() {
		return ENDORSEMENT_APPLICATION_DATE;
	}
	public void setENDORSEMENT_APPLICATION_DATE(String eNDORSEMENTAPPLICATIONDATE) {
		ENDORSEMENT_APPLICATION_DATE = eNDORSEMENTAPPLICATIONDATE;
	}
	public String getEND_APPLY_NO() {
		return END_APPLY_NO;
	}
	public void setEND_APPLY_NO(String eNDAPPLYNO) {
		END_APPLY_NO = eNDAPPLYNO;
	}
	public String getIF_CANCEL() {
		return IF_CANCEL;
	}
	public void setIF_CANCEL(String iFCANCEL) {
		IF_CANCEL = iFCANCEL;
	}
	public String getCANCEL_REASON() {
		return CANCEL_REASON;
	}
	public void setCANCEL_REASON(String cANCELREASON) {
		CANCEL_REASON = cANCELREASON;
	}
	public String getIF_AUTO_RENEWAL() {
		return IF_AUTO_RENEWAL;
	}
	public void setIF_AUTO_RENEWAL(String iFAUTORENEWAL) {
		IF_AUTO_RENEWAL = iFAUTORENEWAL;
	}
	public String getRENEWAL_DEDUCT_METHOD() {
		return RENEWAL_DEDUCT_METHOD;
	}
	public void setRENEWAL_DEDUCT_METHOD(String rENEWALDEDUCTMETHOD) {
		RENEWAL_DEDUCT_METHOD = rENEWALDEDUCTMETHOD;
	}
	public String getMEDICAL_NO() {
		return MEDICAL_NO;
	}
	public void setMEDICAL_NO(String mEDICALNO) {
		MEDICAL_NO = mEDICALNO;
	}
	public String getBANK_NO() {
		return BANK_NO;
	}
	public void setBANK_NO(String bANKNO) {
		BANK_NO = bANKNO;
	}
	
	public String getBANK_NAME() {
		return BANK_NAME;
	}
	public void setBANK_NAME(String bANKNAME) {
		BANK_NAME = bANKNAME;
	}
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
}
