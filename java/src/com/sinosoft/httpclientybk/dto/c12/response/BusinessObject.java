//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.17 at 09:25:07 ���� CST 
//


package com.sinosoft.httpclientybk.dto.c12.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClaimNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClaimCodeP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BookingSequenceNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "prtNo",
    "dataBatchNo",
    "requestType",
    "taskNo"
})
@XmlRootElement(name = "BusinessObject")
public class BusinessObject {
    
    @XmlElement(name = "PrtNo", required = true, nillable = true)
    protected String prtNo;
    @XmlElement(name = "DataBatchNo", required = true, nillable = true)
    protected String dataBatchNo;
    @XmlElement(name = "RequestType", required = true, nillable = true)
    protected String requestType;
    @XmlElement(name = "TaskNo", required = true, nillable = true)
    protected String taskNo;

	public String getDataBatchNo() {
		return dataBatchNo;
	}

	public void setDataBatchNo(String dataBatchNo) {
		this.dataBatchNo = dataBatchNo;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getTaskNo() {
		return taskNo;
	}

	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}

	public String getPrtNo() {
		return prtNo;
	}

	public void setPrtNo(String prtNo) {
		this.prtNo = prtNo;
	}

}
