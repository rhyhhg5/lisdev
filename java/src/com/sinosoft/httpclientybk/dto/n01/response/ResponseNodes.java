//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.17 at 09:25:07 ���� CST 
//


package com.sinosoft.httpclientybk.dto.n01.response;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ResponseNode" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseNode"
})
@XmlRootElement(name = "ResponseNodes")
public class ResponseNodes {

    @XmlElement(name = "ResponseNode", required = true)
    protected List<ResponseNode> responseNode;

    /**
     * Gets the value of the responseNode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the responseNode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponseNode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResponseNode }
     * 
     * 
     */
	public String getXml() throws JAXBException {
	ByteArrayOutputStream os = new ByteArrayOutputStream();
	JAXBContext jaxbContext = JAXBContext.newInstance(ResponseNodes.class);
	Marshaller marshaller = jaxbContext.createMarshaller();
	marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	marshaller.marshal(this, os);
	return os.toString();

}
    public List<ResponseNode> getResponseNode() {
        if (responseNode == null) {
            responseNode = new ArrayList<ResponseNode>();
        }
        return this.responseNode;
    }

    public static void main(String[] args) throws JAXBException {
		ResponseNodes r = new ResponseNodes();
		ResponseNode rn = new ResponseNode();
		r.getResponseNode().set(0, rn);
		System.out.println(r.getXml());
	}
    
    
}















