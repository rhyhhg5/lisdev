package com.sinosoft.httpclientybk.dto.n01.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "risk_Info"
})

@XmlRootElement(name = "RISK_LIST")
public class Risk_List {
	
	 @XmlElement(name = "RISK_INFO", required = true)
	    protected List<Risk_Info> risk_Info;
	    
	   

	public List<Risk_Info> getRisk_Info() {
		if (risk_Info == null) {
			risk_Info = new ArrayList<Risk_Info>();
        }
		return risk_Info;
	}



	public void setRisk_Info(List<Risk_Info> riskInfo) {
		risk_Info = riskInfo;
	}
	    
}
