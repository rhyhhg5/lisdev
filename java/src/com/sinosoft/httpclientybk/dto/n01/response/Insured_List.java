package com.sinosoft.httpclientybk.dto.n01.response;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customer_Info"
})

@XmlRootElement(name = "INSURED_LIST")
public class Insured_List {

	 @XmlElement(name = "Customer_Info", required = true)
	    protected List<Customer_Info> customer_Info;
	    
	 public Insured_List(){}

		public List<Customer_Info> getRisk_Info() {
			if (customer_Info == null) {
				customer_Info = new ArrayList<Customer_Info>();
	        }
			return customer_Info;
		}

	public void setCustomer_Info(List<Customer_Info> customerInfo) {
		customer_Info = customerInfo;
	}   
	 
}
