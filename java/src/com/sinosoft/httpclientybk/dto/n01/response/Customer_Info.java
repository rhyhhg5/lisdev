package com.sinosoft.httpclientybk.dto.n01.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customer_Sequence_No",
    "risk_List"
})
@XmlRootElement(name = "CUSTOMER_INFO")
public class Customer_Info {

	@XmlElement(name = "CUTOMER_SEQUENCE_NO", required = true)
	protected String customer_Sequence_No;
	@XmlElement(name = "RISK_LIST", required = true)
	protected Risk_List risk_List;
	
	public Customer_Info(){
		customer_Sequence_No = "00001";
		risk_List = new Risk_List();
	}
	
	
	public void setCustomer_Sequence_No(String customerSequenceNo) {
		customer_Sequence_No = customerSequenceNo;
	}
	public void setRisk_List(Risk_List riskList) {
		risk_List = riskList;
	}
	public String getCustomer_Sequence_No() {
		return customer_Sequence_No;
	}
	public Risk_List getRisk_List() {
		return risk_List;
	}
	
	
}










