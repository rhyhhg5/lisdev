package com.sinosoft.httpclientybk.dto.n01.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policy_Sequence_No",
    "insured_List"
})


@XmlRootElement(name = "SUCCESS_INFO")
public class Success_Info {

	@XmlElement(name = "POLICY_SEQUENCE_NO", required = true)
	protected String policy_Sequence_No;
	@XmlElement(name = "INSURED_LIST", required = true)
	protected Insured_List insured_List;
	public String getPolicy_Sequence_No() {
		return policy_Sequence_No;
	}
	public void setPolicy_Sequence_No(String policySequenceNo) {
		policy_Sequence_No = policySequenceNo;
	}
	public Insured_List getInsured_List() {
		return insured_List;
	}
	public void setInsured_List(Insured_List insuredList) {
		insured_List = insuredList;
	}
	
	
}
