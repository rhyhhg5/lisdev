package com.sinosoft.httpclientybk.dto.n01.response;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "start_Time",
    "end_Time",
    "all_Life_Sum",
    "all_Accident_Sum_Insured",
    "all_Health_Sum_Insured",
    "all_Old_Sum_Insured",
    "all_Other_Sum_Insured",
    "all_Hospital_Sum_Insured",
    "all_Claim_Amount"
})

@XmlRootElement(name="RISK_INFO")
public class Risk_Info {

    @XmlElement(name = "START_TIME", required = true, nillable = true)
    protected Date start_Time;
    @XmlElement(name = "END_TIME", required = true, nillable = true)
    protected Date end_Time;
    @XmlElement(name = "ALL_LIFE_SUM_INSURED", required = true, nillable = true)
    protected double all_Life_Sum;
    @XmlElement(name = "ALL_ACCIDENT_SUM_INSURED", required = true, nillable = true)
    protected double all_Accident_Sum_Insured;
    @XmlElement(name = "ALL_HEALTH_SUM_INSURED", required = true, nillable = true)
    protected double all_Health_Sum_Insured;
    @XmlElement(name = "ALL_OLD_SUM_INSURED", required = true, nillable = true)
    protected double all_Old_Sum_Insured;
    @XmlElement(name = "ALL_OTHER_SUM_INSURED", required = true, nillable = true)
    protected double all_Other_Sum_Insured;
    @XmlElement(name = "ALL_HOSPITAL_ALLOWANCE", required = true, nillable = true)
    protected double all_Hospital_Sum_Insured;
    @XmlElement(name = "ALL_CLAIM_AMOUNT", required = true, nillable = true)
    protected double all_Claim_Amount;
    
    public Risk_Info(){
    	
    }

	public Date getStart_Time() {
		return start_Time;
	}

	public void setStart_Time(Date startTime) {
		start_Time = startTime;
	}

	public Date getEnd_Time() {
		return end_Time;
	}

	public void setEnd_Time(Date endTime) {
		end_Time = endTime;
	}

	public Double getAll_Life_Sum() {
		return all_Life_Sum;
	}

	public void setAll_Life_Sum(Double allLifeSum) {
		all_Life_Sum = allLifeSum;
	}

	public Double getAll_Accident_Sum_Insured() {
		return all_Accident_Sum_Insured;
	}

	public void setAll_Accident_Sum_Insured(Double allAccidentSumInsured) {
		all_Accident_Sum_Insured = allAccidentSumInsured;
	}

	public Double getAll_Health_Sum_Insured() {
		return all_Health_Sum_Insured;
	}

	public void setAll_Health_Sum_Insured(Double allHealthSumInsured) {
		all_Health_Sum_Insured = allHealthSumInsured;
	}

	public Double getAll_Old_Sum_Insured() {
		return all_Old_Sum_Insured;
	}

	public void setAll_Old_Sum_Insured(Double allOldSumInsured) {
		all_Old_Sum_Insured = allOldSumInsured;
	}

	public Double getAll_Other_Sum_Insured() {
		return all_Other_Sum_Insured;
	}

	public void setAll_Other_Sum_Insured(Double allOtherSumInsured) {
		all_Other_Sum_Insured = allOtherSumInsured;
	}

	public Double getAll_Hospital_Sum_Insured() {
		return all_Hospital_Sum_Insured;
	}

	public void setAll_Hospital_Sum_Insured(Double allHospitalSumInsured) {
		all_Hospital_Sum_Insured = allHospitalSumInsured;
	}

	public Double getAll_Claim_Amount() {
		return all_Claim_Amount;
	}

	public void setAll_Claim_Amount(Double allClaimAmount) {
		all_Claim_Amount = allClaimAmount;
	}


    
    
}

















