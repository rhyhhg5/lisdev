package com.sinosoft.httpclientybk.dto.G02;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;


/**
 * 用来存放人工核保报文解析出来相关字段
 * 
 * @author zq
 * 
 */

public class MANUAL_UNDERWRITING extends BaseXmlSch {
	private static final long serialVersionUID = -5619296859143507527L;
	private String POLICY_FORMER_NO;
	private String IF_CANCEL_INSURE;
	private String UNDERWRITING_INFO;
	private String UNDERWRITING_PHOTO;
	public String getPOLICY_FORMER_NO() {
		return POLICY_FORMER_NO;
	}
	public void setPOLICY_FORMER_NO(String pOLICYFORMERNO) {
		POLICY_FORMER_NO = pOLICYFORMERNO;
	}
	public String getIF_CANCEL_INSURE() {
		return IF_CANCEL_INSURE;
	}
	public void setIF_CANCEL_INSURE(String iFCANCELINSURE) {
		IF_CANCEL_INSURE = iFCANCELINSURE;
	}
	public String getUNDERWRITING_INFO() {
		return UNDERWRITING_INFO;
	}
	public void setUNDERWRITING_INFO(String uNDERWRITINGINFO) {
		UNDERWRITING_INFO = uNDERWRITINGINFO;
	}
	public String getUNDERWRITING_PHOTO() {
		return UNDERWRITING_PHOTO;
	}
	public void setUNDERWRITING_PHOTO(String uNDERWRITINGPHOTO) {
		UNDERWRITING_PHOTO = uNDERWRITINGPHOTO;
	}
	
}
