package com.sinosoft.httpclientybk.dto.G04;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class ESTIMATE_INFO extends BaseXmlSch {
	private static final long serialVersionUID = 9120824436203855893L;
	/* 保单编码 */
	private String POLICY_SEQUENCE_NO;
	/* 保单起期 */
	private String EFFECTIVE_DATE;
	/* 退保申请日期 */
	private String SURRENDER_DATE;
	
	public String getPOLICY_SEQUENCE_NO() {
		return POLICY_SEQUENCE_NO;
	}
	public void setPOLICY_SEQUENCE_NO(String pOLICYSEQUENCENO) {
		POLICY_SEQUENCE_NO = pOLICYSEQUENCENO;
	}
	public String getEFFECTIVE_DATE() {
		return EFFECTIVE_DATE;
	}
	public void setEFFECTIVE_DATE(String eFFECTIVEDATE) {
		EFFECTIVE_DATE = eFFECTIVEDATE;
	}
	public String getSURRENDER_DATE() {
		return SURRENDER_DATE;
	}
	public void setSURRENDER_DATE(String sURRENDERDATE) {
		SURRENDER_DATE = sURRENDERDATE;
	}
	
}
