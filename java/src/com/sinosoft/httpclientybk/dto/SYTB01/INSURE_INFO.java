package com.sinosoft.httpclientybk.dto.SYTB01;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;

public class INSURE_INFO extends BaseXmlSch {

	private static final long serialVersionUID = -3808780886728766606L;
	
	
	/** App订单号 */
	private String POLICY_FORMER_NO;
	/** 投保人姓名*/
	private String NAME;
	/** 投保人性别  0:男，1：女，2：其他*/
	private String GENDER;
	/** 投保人出生年月*/
	private String BIRTHDAY;
	/**投保人证件类型*/
	private String CERTIFICATE_TYPE;
	/**投保人证件号码*/
	private String CERTIFICATE_NO;
	/**投保人手机号*/
	private String MOBILE;
	/**医保卡号*/
	private String MEDICAL_NO;
	/**银行卡号*/
	private String BANK_NO;
	/**所属银行*/
	private String BANK_NAME;
	/**银行编码*/
	private String BANK_CODE;
	/**投保人详细地址*/
	private String ADDRESS;
	/**投保人邮箱*/
	private String EMAIL;
	/**与被保人的关系  00: 本人 ,01: 妻子,02: 丈夫,03: 儿女,04: 父亲,05: 母亲,06: 岳父,07: 岳母,08: 女婿,09: 公公,
	10: 婆婆,11: 媳妇 ,12: 兄弟姐妹,13: 表兄弟姐妹,14: 叔伯,15: 姑姑,16: 舅舅 ,17: 姨,18: 侄子/女
	19: 爷爷,20: 奶奶,21: 孙子/女 ,22: 外公,23: 外婆,24: 外孙子/女,25: 其他亲戚 ,26: 同事,27: 朋友
	28: 雇主,29: 雇员,30: 其他  */
	private String RELATION_INSURED;
	/**被保人姓名*/
	private String INSUED_NAME;
	/**被保人性别  0:男，1：女，2：其他 */
	private String INSUED_GENDER;
	/**被保人出生年月*/
	private String INSUED_BIRTHDAY;
	/**被保人证件类型*/
	private String INSUED_CERTIFICATE_TYPE;
	/**被保人证件号码*/
	private String INSUED_CERTIFICATE_NO;
	/**被保人手机号*/
	private String INSUED_MOBILE;
	/**套餐产品编码*/
	private String WRAP_CODE;
	/**套餐产品名称*/
	private String WRAP_NAME;
	/**产品款别*/
	private String WRAP_TYPE;
	/**险种编码*/
	private String RISK_CODE;
	/**保障年期*/
	private String INSUREYEAR_FLAG;
	/**缴费年限*/
	private String PAY_END_YEAR;
	/**缴费频次 0 趸缴; 1年缴*/
	private String PAY_INTV;
	 /**扣款方式  4：银行卡   8：医保卡*/
	private String DEDUCT_METHOD;
	/**医保账户余额*/
	private String MEDICAL_MONEY;
	/**投保申请日期*/
	private String APPLICATION_DATE;
	/**保险生效时间*/
	private String EFFECTIVE_DATE;
	/**保险终止时间*/
	private String EXPIRE_DATE;
	/**是否自动续保 0：否  1：是*/
	private String IF_AUTO_RENEWAL;
	/**续保扣款方式*/
	private String RENEWAL_DEDUCT_METHOD;
	/**保额*/
	private String SUM_INSURED;
	/**保费*/
	private String SUM_PREM;
	/**销售人员编码*/
	private String SALES_CODE;
	/**健康告知  0标准体  1:非标准体*/
	private String HEALTH_PROMISE_INFO;
	/**身高*/
	private String HEIGHT;
	/**体重*/
	private String WEIGHT;
	/**职业类别编码*/
	private String OCCUPACTIONCODE;
	
	
	public String getOCCUPACTIONCODE()
	{
		return OCCUPACTIONCODE;
	}
	public void setOCCUPACTIONCODE(String oCCUPACTIONCODE)
	{
		OCCUPACTIONCODE = oCCUPACTIONCODE;
	}
	public String getHEALTH_PROMISE_INFO() {
		return HEALTH_PROMISE_INFO;
	}
	public void setHEALTH_PROMISE_INFO(String hEALTH_PROMISE_INFO) {
		HEALTH_PROMISE_INFO = hEALTH_PROMISE_INFO;
	}
	

	public String getPOLICY_FORMER_NO() {
		return POLICY_FORMER_NO;
	}
	public void setPOLICY_FORMER_NO(String pOLICY_FORMER_NO) {
		POLICY_FORMER_NO = pOLICY_FORMER_NO;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getGENDER() {
		return GENDER;
	}
	public void setGENDER(String gENDER) {
		GENDER = gENDER;
	}
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}
	public String getCERTIFICATE_TYPE() {
		return CERTIFICATE_TYPE;
	}
	public void setCERTIFICATE_TYPE(String cERTIFICATE_TYPE) {
		CERTIFICATE_TYPE = cERTIFICATE_TYPE;
	}
	public String getCERTIFICATE_NO() {
		return CERTIFICATE_NO;
	}
	public void setCERTIFICATE_NO(String cERTIFICATE_NO) {
		CERTIFICATE_NO = cERTIFICATE_NO;
	}
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	public String getMEDICAL_NO() {
		return MEDICAL_NO;
	}
	public void setMEDICAL_NO(String mEDICAL_NO) {
		MEDICAL_NO = mEDICAL_NO;
	}
	public String getBANK_NO() {
		return BANK_NO;
	}
	public void setBANK_NO(String bANK_NO) {
		BANK_NO = bANK_NO;
	}
	public String getBANK_NAME() {
		return BANK_NAME;
	}
	public void setBANK_NAME(String bANK_NAME) {
		BANK_NAME = bANK_NAME;
	}
	public String getBANK_CODE() {
		return BANK_CODE;
	}
	public void setBANK_CODE(String bANK_CODE) {
		BANK_CODE = bANK_CODE;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getRELATION_INSURED() {
		return RELATION_INSURED;
	}
	public void setRELATION_INSURED(String rELATION_INSURED) {
		RELATION_INSURED = rELATION_INSURED;
	}
	public String getINSUED_NAME() {
		return INSUED_NAME;
	}
	public void setINSUED_NAME(String iNSUED_NAME) {
		INSUED_NAME = iNSUED_NAME;
	}
	public String getINSUED_GENDER() {
		return INSUED_GENDER;
	}
	public void setINSUED_GENDER(String iNSUED_GENDER) {
		INSUED_GENDER = iNSUED_GENDER;
	}
	public String getINSUED_BIRTHDAY() {
		return INSUED_BIRTHDAY;
	}
	public void setINSUED_BIRTHDAY(String iNSUED_BIRTHDAY) {
		INSUED_BIRTHDAY = iNSUED_BIRTHDAY;
	}
	public String getINSUED_CERTIFICATE_TYPE() {
		return INSUED_CERTIFICATE_TYPE;
	}
	public void setINSUED_CERTIFICATE_TYPE(String iNSUED_CERTIFICATE_TYPE) {
		INSUED_CERTIFICATE_TYPE = iNSUED_CERTIFICATE_TYPE;
	}
	public String getINSUED_CERTIFICATE_NO() {
		return INSUED_CERTIFICATE_NO;
	}
	public void setINSUED_CERTIFICATE_NO(String iNSUED_CERTIFICATE_NO) {
		INSUED_CERTIFICATE_NO = iNSUED_CERTIFICATE_NO;
	}
	public String getINSUED_MOBILE() {
		return INSUED_MOBILE;
	}
	public void setINSUED_MOBILE(String iNSUED_MOBILE) {
		INSUED_MOBILE = iNSUED_MOBILE;
	}
	public String getWRAP_CODE() {
		return WRAP_CODE;
	}
	public void setWRAP_CODE(String wRAP_CODE) {
		WRAP_CODE = wRAP_CODE;
	}
	public String getWRAP_NAME() {
		return WRAP_NAME;
	}
	public void setWRAP_NAME(String wRAP_NAME) {
		WRAP_NAME = wRAP_NAME;
	}
	public String getWRAP_TYPE() {
		return WRAP_TYPE;
	}
	public void setWRAP_TYPE(String wRAP_TYPE) {
		WRAP_TYPE = wRAP_TYPE;
	}
	public String getRISK_CODE() {
		return RISK_CODE;
	}
	public void setRISK_CODE(String rISK_CODE) {
		RISK_CODE = rISK_CODE;
	}
	public String getINSUREYEAR_FLAG() {
		return INSUREYEAR_FLAG;
	}
	public void setINSUREYEAR_FLAG(String iNSUREYEAR_FLAG) {
		INSUREYEAR_FLAG = iNSUREYEAR_FLAG;
	}
	public String getPAY_END_YEAR() {
		return PAY_END_YEAR;
	}
	public void setPAY_END_YEAR(String pAY_END_YEAR) {
		PAY_END_YEAR = pAY_END_YEAR;
	}
	public String getPAY_INTV() {
		return PAY_INTV;
	}
	public void setPAY_INTV(String pAY_INTV) {
		PAY_INTV = pAY_INTV;
	}
	
	public String getDEDUCT_METHOD() {
		return DEDUCT_METHOD;
	}
	public void setDEDUCT_METHOD(String dEDUCT_METHOD) {
		DEDUCT_METHOD = dEDUCT_METHOD;
	}
	public String getMEDICAL_MONEY() {
		return MEDICAL_MONEY;
	}
	public void setMEDICAL_MONEY(String mEDICAL_MONEY) {
		MEDICAL_MONEY = mEDICAL_MONEY;
	}
	public String getAPPLICATION_DATE() {
		return APPLICATION_DATE;
	}
	public void setAPPLICATION_DATE(String aPPLICATION_DATE) {
		APPLICATION_DATE = aPPLICATION_DATE;
	}
	public String getEFFECTIVE_DATE() {
		return EFFECTIVE_DATE;
	}
	public void setEFFECTIVE_DATE(String eFFECTIVE_DATE) {
		EFFECTIVE_DATE = eFFECTIVE_DATE;
	}
	public String getEXPIRE_DATE() {
		return EXPIRE_DATE;
	}
	public void setEXPIRE_DATE(String eXPIRE_DATE) {
		EXPIRE_DATE = eXPIRE_DATE;
	}
	public String getIF_AUTO_RENEWAL() {
		return IF_AUTO_RENEWAL;
	}
	public void setIF_AUTO_RENEWAL(String iF_AUTO_RENEWAL) {
		IF_AUTO_RENEWAL = iF_AUTO_RENEWAL;
	}
	public String getRENEWAL_DEDUCT_METHOD() {
		return RENEWAL_DEDUCT_METHOD;
	}
	public void setRENEWAL_DEDUCT_METHOD(String rENEWAL_DEDUCT_METHOD) {
		RENEWAL_DEDUCT_METHOD = rENEWAL_DEDUCT_METHOD;
	}
	public String getSUM_INSURED() {
		return SUM_INSURED;
	}
	public void setSUM_INSURED(String sUM_INSURED) {
		SUM_INSURED = sUM_INSURED;
	}
	public String getSUM_PREM() {
		return SUM_PREM;
	}
	public void setSUM_PREM(String sUM_PREM) {
		SUM_PREM = sUM_PREM;
	}
	public String getSALES_CODE() {
		return SALES_CODE;
	}
	public void setSALES_CODE(String sALES_CODE) {
		SALES_CODE = sALES_CODE;
	}
	
	public String getHEIGHT() {
		return HEIGHT;
	}
	public void setHEIGHT(String hEIGHT) {
		HEIGHT = hEIGHT;
	}
	public String getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}
	
	

	
	
	
	
}
