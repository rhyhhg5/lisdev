package com.sinosoft.httpclientybk.dto.SYTB01;

import com.sinosoft.httpclientybk.xml.xsch.BaseXmlSch;
/**
 * 
 * @author db2admin
 * 返回信息
 */
public class SUCCESS_INFO  extends BaseXmlSch{
	
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = -4338806920933749550L;

   /** APP投保单号*/
   private String POLICY_CONT_NO = null;
   
	/** 流水号*/
   private String POLICY_PRT_NO = null;

   /** 自核结论*/
   private String SELF_UNDERWRITING_RESULT = null;

   /** 自核结论描述 */
   private String SELF_UNDERWRITING_RESULT_DES = null;

public String getPOLICY_PRT_NO() {
	return POLICY_PRT_NO;
}

public void setPOLICY_PRT_NO(String pOLICY_PRT_NO) {
	POLICY_PRT_NO = pOLICY_PRT_NO;
}

public String getPOLICY_CONT_NO() {
	return POLICY_CONT_NO;
}

public void setPOLICY_CONT_NO(String pOLICY_CONT_NO) {
	POLICY_CONT_NO = pOLICY_CONT_NO;
}

public String getSELF_UNDERWRITING_RESULT() {
	return SELF_UNDERWRITING_RESULT;
}

public void setSELF_UNDERWRITING_RESULT(String sELF_UNDERWRITING_RESULT) {
	SELF_UNDERWRITING_RESULT = sELF_UNDERWRITING_RESULT;
}

public String getSELF_UNDERWRITING_RESULT_DES() {
	return SELF_UNDERWRITING_RESULT_DES;
}

public void setSELF_UNDERWRITING_RESULT_DES(String sELF_UNDERWRITING_RESULT_DES) {
	SELF_UNDERWRITING_RESULT_DES = sELF_UNDERWRITING_RESULT_DES;
}
  



   
}
