package com.sinosoft.httpclientybk.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class YbkDeCompress {
	public boolean DeCompress(String tFile, String fileDir) {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		System.out.println(tFile);
		System.setProperty("sun.zip.encoding", System.getProperty("file.encoding")); //防止文件名中有中文时出错 
		System.setProperty("sun.jnu.encoding", System.getProperty("file.encoding"));
	    System.out.println(System.getProperty("sun.zip.encoding")); //ZIP编码方式  
	    System.out.println(System.getProperty("sun.jnu.encoding")); //当前文件编码方式  
	    System.out.println(System.getProperty("file.encoding")); //这个是当前文件内容编码方式  
		try {
			ZipInputStream Zin = new ZipInputStream(new FileInputStream(tFile));// 输入源zip路径
			BufferedInputStream Bin = new BufferedInputStream(Zin);
			String Parent = fileDir; // 输出路径（文件夹目录）
			File Fout = null;
			ZipEntry entry;
			try {
				while ((entry = Zin.getNextEntry()) != null
						&& !entry.isDirectory()) {
					Fout = new File(Parent, entry.getName());
					if (!Fout.exists()) {
						(new File(Fout.getParent())).mkdirs();
					}
					FileOutputStream out = new FileOutputStream(Fout);
					BufferedOutputStream Bout = new BufferedOutputStream(out);
					int b;
					while ((b = Bin.read()) != -1) {
						Bout.write(b);
					}
					Bout.close();
					out.close();
					System.out.println(Fout + "解压成功");
				}
				Bin.close();
				Zin.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("耗费时间： " + (endTime - startTime) + " ms");
		return true;
	}

}