/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.httpclientybk.util;

import org.apache.log4j.Logger;

import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.obj.MsgResHead;
import com.sinosoft.httpclientybk.obj.SYMsgResHead;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;




public final class MsgUtil
{
    private final static Logger mLogger = Logger.getLogger(MsgUtil.class);

    public static MsgCollection getResMsgCol(MsgCollection cMsgCols)
    {
        MsgCollection tResMsgCols = new MsgCollection();

        MsgHead tSrcMsgHead = cMsgCols.getMsgHead();
        if (tSrcMsgHead == null)
        {
            mLogger.error("未获取到报头信息。");
            return null;
        }

        MsgResHead tResMsgHead = new MsgResHead();
        //tResMsgHead.setTASK_NO("测试数据");
        tResMsgHead.setREQUEST_TYPE(tSrcMsgHead.getREQUEST_TYPE());
        //tResMsgHead.setERROR_MESSAGE(cMsgCols.getMsgHead());
        
        tResMsgCols.setMsgResHead(tResMsgHead);

        return tResMsgCols;
    }
    
    //沈阳医保新增
    public static MsgCollection getSYResMsgCol(MsgCollection cMsgCols)
    {
        MsgCollection tSYResMsgCols = new MsgCollection();

        MsgHead tSrcMsgHead = cMsgCols.getMsgHead();
        if (tSrcMsgHead == null)
        {
            mLogger.error("未获取到报头信息。");
            return null;
        }

        SYMsgResHead tSYMsgResHead = new SYMsgResHead();
        //tResMsgHead.setTASK_NO("测试数据");
        tSYMsgResHead.setREQUEST_TYPE(tSrcMsgHead.getREQUEST_TYPE());
        //tResMsgHead.setERROR_MESSAGE(cMsgCols.getMsgHead());
        
        tSYResMsgCols.setmSYMsgResHead(tSYMsgResHead);

        return tSYResMsgCols;
    }
}
