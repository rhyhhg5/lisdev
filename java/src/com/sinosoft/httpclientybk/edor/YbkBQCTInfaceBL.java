package com.sinosoft.httpclientybk.edor;

import com.sinosoft.httpclientybk.dto.G05.ENDORSEMENT_INFO;
import com.sinosoft.lis.bl.LJSGetEndorseBL;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.FeeNoticeVtsUI;
import com.sinosoft.lis.bq.FinanceDataBL;
import com.sinosoft.lis.bq.PEdorAppConfirmUI;
import com.sinosoft.lis.bq.PEdorAppItemUI;
import com.sinosoft.lis.bq.PEdorConfirmBL;
import com.sinosoft.lis.bq.PGrpEdorCancelUI;
import com.sinosoft.lis.bq.PrtAppEndorsementBL;
import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LJSGetEndorseDB;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPInsureAccSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 
 * @author zq
 *
 */
public class YbkBQCTInfaceBL {

	private GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	private String mEdorAcceptNo;

	private  ENDORSEMENT_INFO mEdorInfo;

	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
	private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
	private LCContSchema mLCContSchema;
	private LPPolSet mLPPolSet = new LPPolSet();
	private LPInsuredSet mLPInsuredSet =new LPInsuredSet();
	public CErrors mErrors = new CErrors();
	private LPContSchema mLPContschema = new LPContSchema();
	private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
	
	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();

	public YbkBQCTInfaceBL() {
	}

	public YbkBQCTInfaceBL(GlobalInput tGlobalInput, ENDORSEMENT_INFO tEdorInfo,
			LCContSchema tLCContSchema) {
		this.mGlobalInput = tGlobalInput;
		this.mEdorInfo = tEdorInfo;
		this.mLCContSchema = tLCContSchema;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mEdorInfo = (ENDORSEMENT_INFO) cInputData.getObjectByObjectName("ENDORSEMENT_INFO", 0);
		mLCContSchema = (LCContSchema) cInputData.getObjectByObjectName("LCContSchema", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("请传入参数信息错误");
			return false;
		}
		return true;
	}

	public boolean submit(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!deal()) {
			return false;
		}

		return true;
	}

	private boolean deal() {
		
		try {
			// 申请工单
			if (!createWorkNo()) {
				return false;
			}
			// 添加保全
			if (!addEdorItem()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORAPP")) {
					return false;
				}
				return false;
			}
	
			// 录入明细
			if (!saveDetail()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}

			// 理算确认
			if (!appConfirm()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
	
			if(!creatPrintVts()){
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
	
			// 保全确认
			if (!edorConfirm()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
		} catch (Exception e) {
			System.out.println("解约接口程序处理错误：撤销工单+"+e.getMessage());
			mErrors.addOneError("保全处理异常，请求失败。");
			cancelEdorItem("1", "I&EDORMAIN");
			cancelEdorItem("1", "I&EDORAPP");
			return false;
		}

		// 修改批单以及财务数据
		if (!endEdor()) {
			return false;
		}

		return true;
	}

	public LPEdorItemSchema getEdorItem() {
		return mLPEdorItemSchema;
	}

	private boolean cancelEdorItem(String edorstate, String transact) {
		PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		tVData.addElement(mGlobalInput);

		if ("I&EDORAPP".equals(transact)) {
			tLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorAppSchema.setEdorState(edorstate);

			String delReason = "";
			String reasonCode = "002";

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorAppSchema);
			// 准备传输数据 VData
			tVData.addElement(tTransferData);
		} else if ("I&EDORMAIN".equals(transact)) {
			tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
			tLPEdorMainSchema.setEdorState(edorstate);
			tLPEdorMainSchema.setContNo(mLCContSchema.getContNo());
			String delReason = "";
			String reasonCode = "002";
			System.out.println(delReason);

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorMainSchema);
			tVData.addElement(tTransferData);
		}
		try {
			// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			System.out.println("hello");
			tPGrpEdorCancelUI.submitData(tVData, transact);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mErrors.addOneError("撤销工单失败" + e);
			return false;
		}
		return true;
	}

	private boolean createWorkNo() {

		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(mLCContSchema.getAppntNo());
		//工单类型 03保全
		tLGWorkSchema.setTypeNo("03");
		tLGWorkSchema.setContNo(mLCContSchema.getContNo());
		//工单申请人类型
		tLGWorkSchema.setApplyTypeNo("0");
		//受理途径 微信WeChat
		tLGWorkSchema.setAcceptWayNo("11");
		tLGWorkSchema.setAcceptDate(mCurrDate);
		tLGWorkSchema.setRemark("线上解约接口生成");
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			mErrors.addOneError("生成保全工单失败" + tTaskInputBL.mErrors);
			return false;
		}
		mEdorAcceptNo = tTaskInputBL.getWorkNo();

		return true;
	}

	private boolean addEdorItem() {
		// 校验能否添加保全项目
		if (!checkEdorItem()) {
			return false;
		}
		mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
//		mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
//		mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
		mLPEdorItemSchema.setDisplayType("1");
		mLPEdorItemSchema.setEdorType("CT");
		mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
		mLPEdorItemSchema.setContNo(mLCContSchema.getContNo());
		mLPEdorItemSchema.setEdorState("3");
		mLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
		mLPEdorItemSchema.setPolNo(BQ.FILLDATA);
		mLPEdorItemSchema.setManageCom(mLCContSchema.getManageCom());
		//保全生效日期
		mLPEdorItemSchema.setEdorValiDate(mEdorInfo.getENDORSEMENT_APPLICATION_DATE());
		mLPEdorItemSchema.setEdorAppDate(mCurrDate);
		//ReasonCode可以为空 暂置为-1
		mLPEdorItemSchema.setReasonCode("-1");
		mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
		mLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
		mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
		PEdorAppItemUI tPEdorAppItemUI=new PEdorAppItemUI();
		LPEdorItemSet mLPEdorItemSet=new LPEdorItemSet();
		mLPEdorItemSet.add(mLPEdorItemSchema);
		
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("DisplayType","1");
		VData tVData = new VData();
        
        tVData.add(mLPEdorItemSet);
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);
        
		if (!tPEdorAppItemUI.submitData(tVData,"INSERT||EDORITEM"))
        {
			mErrors.addOneError("生成保全工单失败" + tPEdorAppItemUI.mErrors.getFirstError());
			return false;
        }
		return true;
	}

	private boolean saveDetail() {
		MMap map = new MMap();
		String sql = " edorno='" + mEdorAcceptNo + "' and edortype='CT'";
		map.put("delete from lppol where" + sql, "DELETE");
		map.put("delete from lpcont where" + sql, "DELETE");
		map.put("delete from lpinsured where" + sql, "DELETE");
		// 生成保全P表
		LCPolDB tLCPolDB = new LCPolDB();
		LCPolSet tLCPolSet = new LCPolSet();
		Reflections tReflections = new Reflections();
		tLCPolDB.setContNo(mLCContSchema.getContNo());
		tLCPolSet = tLCPolDB.query();

		if (tLCPolSet != null && tLCPolSet.size() > 0) {
			for (int i = 1; i <= tLCPolSet.size(); i++) {
				LPPolSchema tLPPolSchema = new LPPolSchema();
				tReflections.transFields(tLPPolSchema, tLCPolSet.get(i)
						.getSchema());

				tLPPolSchema.setEdorNo(mEdorAcceptNo);
				tLPPolSchema.setEdorType("CT");
				tLPPolSchema.setOperator(mGlobalInput.Operator);
				tLPPolSchema.setModifyDate(mCurrDate);
				tLPPolSchema.setModifyTime(mCurrTime);
				mLPPolSet.add(tLPPolSchema);
			}
		} else {
			mErrors.addOneError("查询险种信息失败");
			return false;
		}

		tReflections.transFields(mLPContschema, mLCContSchema);
		mLPContschema.setEdorNo(mEdorAcceptNo);
		mLPContschema.setEdorType("CT");
		
		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		String insuredStr="select * from lcinsured where contno='"+mLCContSchema.getContNo()+"' ";
	    LCInsuredSet tLCInsuredSet = tLCInsuredDB.executeQuery(insuredStr);
	    
	    for (int i = 1; i <= tLCInsuredSet.size(); i++) {
	        LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(i);

	        LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
	        tLPInsuredSchema.setEdorNo(mEdorAcceptNo);
	        tLPInsuredSchema.setEdorType("CT");
	        tReflections.transFields(tLPInsuredSchema, tLCInsuredSchema);
	        tLPInsuredSchema.setOperator(mGlobalInput.Operator);
	        PubFun.fillDefaultField(tLPInsuredSchema);
	        mLPInsuredSet.add(tLPInsuredSchema);
	    }

		// 修改保全状态
		mLPEdorItemSchema.setEdorState("1");
		//ReasonCode可以为空 暂置为-1
		mLPEdorItemSchema.setReasonCode("-1");
		map.put(mLPEdorItemSchema, "UPDATE");
		map.put(mLPPolSet, SysConst.DELETE_AND_INSERT);
		map.put(mLPContschema, SysConst.DELETE_AND_INSERT);
		map.put(mLPInsuredSet, SysConst.DELETE_AND_INSERT);

		// 提交数据库
		if (!submit(map)) {
			return false;
		}
		return true;
	}

	/**
	 * 产生打印数据
	 * 
	 * @return boolean
	 */
	private boolean creatPrintVts() {
		// 生成打印数据
		VData data = new VData();
		data.add(mGlobalInput);
		PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
				mEdorAcceptNo);
		if (!tPrtAppEndorsementBL.submitData(data, "")) {
			mErrors.addOneError("生成保全服务批单失败！");
			return false;
		}
		return true;
	}

	private boolean appConfirm() {
		
		PEdorAppConfirmUI tPEdorAppConfirmUI = new PEdorAppConfirmUI(mGlobalInput, mEdorAcceptNo);
	    if (!tPEdorAppConfirmUI.submitData())
	    {
	    	mErrors.addOneError("保全理算失败：" + tPEdorAppConfirmUI.getError());
			return false;
	    }
	    
	    /**校验理算金额*/
	    String str="select sum(getmoney) from ljsgetendorse where endorsementno='"+mEdorAcceptNo+"' ";
	    String money=new ExeSQL().getOneValue(str);
	    if(null == money || "".equals(money)){
	    	mErrors.addOneError("获取理算金额失败！");
	    	return false;
	    }
	    double getmoney = CommonBL.carry(money);
	    //欠费的万能保单解约存在收费的问题，保全金额大于0则阻断。
	    if(getmoney > 0.0001){
	    	mErrors.addOneError("保单解约产生收费，保全处理失败！");
	    	return false;
	    }
	    /*double budGet= CommonBL.carry(mEdorItemInfo.getBudGet());
	    double res = Math.abs(getmoney) - Math.abs(budGet);
	    //实际理算金额与试算金额差0.0001以上的终止处理
	    if(res > 0.0001 ){
	    	mErrors.addOneError("解约理算金额与提供的试算金额不一致，退保理算金额为:" + getmoney);
	    	return false;
	    }*/
	    
		return true;
	}

	/**
	 * 查询险种polNo发生的加费和
	 * 
	 * @param polNo
	 *            String
	 * @return double
	 */
	private double getSumAddFee(String polNo) {
		String sql = "select sum(SumActuPayMoney) " + "from LJAPayPerson "
				+ "where payPlanCode like '000000%' " + "    and  polNo = '"
				+ polNo + "' ";
		ExeSQL e = new ExeSQL();
		String sumActuPayMoney = e.getOneValue(sql);

		return sumActuPayMoney.equals("") ? 0 : Double
				.parseDouble(sumActuPayMoney);
	}

	private boolean checkEdorItem() {
		String mContNo=mLCContSchema.getContNo();
		//正在操作保全的无法添加犹豫期退保
		String edorSQL = " select edoracceptno from lpedoritem where contno='"
				+ mContNo
				+ "' and edoracceptno!='"+ mEdorAcceptNo+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";
		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			mErrors.addOneError("保单" + mContNo
					+ "正在操作保全，工单号为：" + edorFlag + "无法做解约");
			return false;
		}
		// 只有承保有效状态下的保单才能犹豫期退保
		if (!"1".equals(mLCContSchema.getAppFlag())) {
			mErrors.addOneError("保单未签单不能申请退保");
			return false;
		}
		String cardFlag = mLCContSchema.getCardFlag();
		String saleChnl = mLCContSchema.getSaleChnl();
		if(null==cardFlag || "".equals(cardFlag)){
			mErrors.addOneError("保单类型CARDFLAG为空");
			return false;
		}
		
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.addOneError("提交数据库发生错误" + tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * 保全确认
	 * 
	 * @param edorAcceptNo
	 *            String
	 * @return boolean
	 */
	private boolean edorConfirm() {
		MMap map = new MMap();

		// 生成财务数据
		FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput,
				mEdorAcceptNo, BQ.NOTICETYPE_P, "");
		if (!tFinanceDataBL.submitData()) {
			mErrors.addOneError("生成财务数据错误" + tFinanceDataBL.mErrors);
			return false;
		}

		// 个人保全结案
		PEdorConfirmBL tPEdorConfirmBL = new PEdorConfirmBL(mGlobalInput,
				mEdorAcceptNo);
		map = tPEdorConfirmBL.getSubmitData();
		if (map == null) {
			mErrors.addOneError("保全结案发生错误" + tFinanceDataBL.mErrors);
			return false;
		}

		// 工单结案
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
		tLGWorkSchema.setTypeNo("03"); // 结案状态

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		MMap tmap = tTaskAutoFinishBL.getSubmitData(data, "");
		if (tmap == null) {
			mErrors.addOneError("工单结案失败" + tFinanceDataBL.mErrors);
			return false;
		}
		map.add(tmap);
		if (!submit(map)) {
			return false;
		}
		return true;
	}

	private boolean endEdor() {
		String mPayMode = mLCContSchema.getPayMode();
		String mAccNo = mLCContSchema.getBankAccNo();
		String mAccName = mLCContSchema.getAccName();
		String mBankCode = mLCContSchema.getBankCode();
		String mBankAccNo = mLCContSchema.getBankAccNo();
		String mPayDate = mCurrDate;
		//若选择的付费方式为银行转账
		if("4".equals(mLCContSchema.getPayMode())){
			mAccNo=mLCContSchema.getBankAccNo();
			mBankCode=mLCContSchema.getBankCode();
			mBankAccNo=mLCContSchema.getBankAccNo();
			if(null==mAccNo || null==mBankCode || null==mBankAccNo){
				System.out.println("坑。。保单账户信息有空。。。");
			}
		}
		
		System.out.println("交退费通知书" + mEdorAcceptNo);
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("payMode", mPayMode);
		tTransferData.setNameAndValue("endDate", "");
		tTransferData.setNameAndValue("payDate", mPayDate);
		tTransferData.setNameAndValue("bank", mBankCode);
		tTransferData.setNameAndValue("bankAccno", mBankAccNo);
		tTransferData.setNameAndValue("accName", mAccName);
		tTransferData.setNameAndValue("chkYesNo", "no");

		// 生成交退费通知书
		FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI(mEdorAcceptNo);
		if (!tFeeNoticeVtsUI.submitData(tTransferData)) {
			mErrors.addOneError("生成批单失败！原因是：" + tFeeNoticeVtsUI.getError());
		}

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tTransferData);
		SetPayInfo spi = new SetPayInfo(mEdorAcceptNo);
		if (!spi.submitDate(data, "0")) {
			System.out.println("设置收退费方式失败！");
			mErrors.addOneError("设置收退费方式失败！原因是：" + spi.mErrors.getFirstError());
		}

		return true;
	}

}
