package com.sinosoft.httpclientybk.edor;

import com.sinosoft.httpclientybk.dto.G05.ENDORSEMENT_INFO;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.PEdorEWDetailUI;
import com.sinosoft.lis.bq.PGrpEdorCancelUI;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 
 * @author zq
 *
 */
public class YbkBQEWInfaceBL {
	//公共信息
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mEdorAcceptNo;

	private  ENDORSEMENT_INFO mEdorInfo;

	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
	private LCContSchema mLCContSchema;
	public CErrors mErrors = new CErrors();
	
	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();

	public YbkBQEWInfaceBL() {
	}
	
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mEdorInfo = (ENDORSEMENT_INFO) cInputData.getObjectByObjectName("ENDORSEMENT_INFO", 0);
		mLCContSchema = (LCContSchema) cInputData.getObjectByObjectName("LCContSchema", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("请传入参数信息错误");
			return false;
		}
		return true;
	}

	public boolean submit(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!deal()) {
			return false;
		}

		return true;
	}

	//业务处理
	private boolean deal() {
		
		try {
			// 申请工单
			if (!createWorkNo()) {
				return false;
			}
			// 添加保全
			if (!addEdorItem()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORAPP")) {
					return false;
				}
				return false;
			}
			// 录入明细
			if (!saveDetail()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
		} catch (Exception e) {
			System.out.println("解约接口程序处理错误：撤销工单+"+e.getMessage());
			mErrors.addOneError("保全处理异常，请求失败。");
			cancelEdorItem("1", "I&EDORMAIN");
			cancelEdorItem("1", "I&EDORAPP");
			return false;
		}
		return true;
	}

	public LPEdorItemSchema getEdorItem() {
		return mLPEdorItemSchema;
	}
	
	//工单撤销
	private boolean cancelEdorItem(String edorstate, String transact) {
		PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		tVData.addElement(mGlobalInput);

		if ("I&EDORAPP".equals(transact)) {
			tLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorAppSchema.setEdorState(edorstate);

			String delReason = "";
			String reasonCode = "002";

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorAppSchema);
			// 准备传输数据 VData
			tVData.addElement(tTransferData);
		} else if ("I&EDORMAIN".equals(transact)) {
			tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
			tLPEdorMainSchema.setEdorState(edorstate);
			tLPEdorMainSchema.setContNo(mLCContSchema.getContNo());
			String delReason = "";
			String reasonCode = "002";
			System.out.println(delReason);

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorMainSchema);
			tVData.addElement(tTransferData);
		}
		try {
			// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			tPGrpEdorCancelUI.submitData(tVData, transact);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("撤销工单失败!");
			mErrors.addOneError("撤销工单失败" + e);
			return false;
		}
		return true;
	}

	//创建工单
	private boolean createWorkNo() {
		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(mLCContSchema.getAppntNo());
		//工单类型 03保全
		tLGWorkSchema.setTypeNo("03");
		tLGWorkSchema.setContNo(mLCContSchema.getContNo());
		//工单申请人类型0为投保人
		tLGWorkSchema.setApplyTypeNo("0");
		//受理途径 微信WeChat
		tLGWorkSchema.setAcceptWayNo("WX");
		tLGWorkSchema.setAcceptDate(mCurrDate);
		tLGWorkSchema.setRemark("上海医保账户微信平台保全项目变更");
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			System.out.println("生成保全工单失败!");
			mErrors.addOneError("生成保全工单失败" + tTaskInputBL.mErrors);
			return false;
		}
		mEdorAcceptNo = tTaskInputBL.getWorkNo();
		System.out.println("mEdorAcceptNo======"+mEdorAcceptNo);
		return true;
	}
	
	//添加保全项目
	private boolean addEdorItem() {
		// 校验能否添加保全项目
		if (!checkEdorItem()) {
			return false;
		}
		MMap map = new MMap();
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorAppNo(mEdorAcceptNo);
        tLPEdorMainSchema.setContNo(mLCContSchema.getContNo());
        tLPEdorMainSchema.setEdorAppDate(mCurrDate);
        tLPEdorMainSchema.setEdorValiDate(mCurrDate);
        tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INIT);
        tLPEdorMainSchema.setUWState(BQ.UWFLAG_INIT);
        tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorMainSchema.setMakeDate(PubFun.getCurrentDate());
        tLPEdorMainSchema.setMakeTime(PubFun.getCurrentTime());
        tLPEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
        tLPEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLPEdorMainSchema, "INSERT");

        mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
        mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
        mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
        mLPEdorItemSchema.setDisplayType("1");
        mLPEdorItemSchema.setEdorType("EW");
        mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPEdorItemSchema.setContNo(mLCContSchema.getContNo());
        mLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
        mLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
        mLPEdorItemSchema.setEdorValiDate(mCurrDate);
        mLPEdorItemSchema.setEdorAppDate(mCurrDate);
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
		mLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
		mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLPEdorItemSchema, "INSERT");
        if (!submit(map))
        {
            return false;
        }
        return true;
	}

	//明细录入
	private boolean saveDetail() {	
		//提交LccontSubSchema和LPEdorItemSchema
		LCContSubDB tLCContSubDB = new LCContSubDB();
		tLCContSubDB.setPrtNo(mLCContSchema.getPrtNo());
		if (!tLCContSubDB.getInfo()) {
			return false;
		}
		LCContSubSchema tLCContSubSchema = tLCContSubDB.getSchema();
		tLCContSubSchema.setIfAutoPay(mEdorInfo.getIF_AUTO_RENEWAL());
		tLCContSubSchema.setRenemalPayMethod(mEdorInfo.getRENEWAL_DEDUCT_METHOD());
		tLCContSubSchema.setMedicalCode(mEdorInfo.getMEDICAL_NO());
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(mLPEdorItemSchema);
		data.add(tLCContSubSchema);

		PEdorEWDetailUI tPEdorEWDetailUI = new PEdorEWDetailUI();
		if (!tPEdorEWDetailUI.submitData(data,""))
		{
			System.out.println("生成保全服务批单失败！");
			mErrors.addOneError("生成保全服务批单失败！");
		  	return false;
		  	}
		return true;
	}
	
	//犹豫期退保校验
	private boolean checkEdorItem() {
		String mContNo=mLCContSchema.getContNo();
		//正在操作保全的无法添加犹豫期退保
		String edorSQL = " select edoracceptno from lpedoritem where contno='"
				+ mContNo
				+ "' and edoracceptno!='"+ mEdorAcceptNo+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";
		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			System.out.println("保单正在做保全,无法在做"+edorFlag+"保全!");
			mErrors.addOneError("保单" + mContNo + "正在操作保全，工单号为：" + edorFlag + "无法做EW");
			return false;
		}
		// 只有承保有效状态下的保单才能犹豫期退保
		if (!"1".equals(mLCContSchema.getAppFlag())) {
			System.out.println("保单未签单不能申请退保!");
			mErrors.addOneError("保单未签单不能申请退保!");
			return false;
		}
		String cardFlag = mLCContSchema.getCardFlag();
		String saleChnl = mLCContSchema.getSaleChnl();
		if(null==cardFlag || "".equals(cardFlag)){
			System.out.println("保单类型CARDFLAG为空!");
			mErrors.addOneError("保单类型CARDFLAG为空!");
			return false;
		}		
		return true;
	}
	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			System.out.println("提交数据库发生错误！");
			mErrors.addOneError("提交数据库发生错误" + tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

}
