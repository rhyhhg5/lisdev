package com.sinosoft.httpclientybk.deal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G01.INSURE_INFO;
import com.sinosoft.httpclientybk.dto.G01.SUCCESS_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.YBKWXLCInsuredListSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

public class YbkWXTB  extends ABusLogic{
	
	private static final String STATIC_ActivityID = "0000001001";
	
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public VData mVData = new VData();
	private PubSubmit tPubSubmit =  new PubSubmit();
	public TransferData mTransferData = new TransferData();
	public String mError = "";// 处理过程中的错误信息
	public String MsgType;
	
	
	public String CustomerNo = "";
	public String MedicalCode = "";
	public String PayMode = "";
	
	private String IfAutoPay="";
	
	private String RenemalPayMethod="";
	private String YBKSerialno = "";
	private String mPrtNo="";
	String ContNo="";
	
	public List INSURE_INFO_List;
	public String mMissionid;
	private String POLICY_FORMER_NO;
	public INSURE_INFO mINSURE_INFO = new INSURE_INFO();
	
	private SUCCESS_INFO mSUCCESS_INFO  = new SUCCESS_INFO();
	//被保人信息等
	YBKWXLCInsuredListSchema mYBKWXLCInsuredListSchema = new YBKWXLCInsuredListSchema();
	//险种信息
	LCPolSchema mLCPolSchema = new LCPolSchema();
	//转换的SQL
	String Sql= "";
	SSRS mSSRS=null;
	String flagState="";
	

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理YBK微信出单标准平台投保过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		MsgType = tMsgHead.getREQUEST_TYPE();

		mGlobalInput.Operator = "YBK";
		mGlobalInput.ComCode = "86";
		mGlobalInput.ManageCom = "86";
		try{
			System.out.println("进来了");
			 INSURE_INFO_List = cMsgInfos.getBodyByFlag("BODY");
				if (INSURE_INFO_List == null || INSURE_INFO_List.size() != 1) {
					errLog("获取医保自核报文信息失败","0");
					return false;
				}
			  
			//将XML装换成DBSchema
			boolean chFlag=XmlToSchemaInfo();
			if(!chFlag){
				String errors=this.mErrors.getFirstError();
				errLog(errors);
				return true;
			} 
			
			mTransferData.setNameAndValue("mLCPolSchema", mLCPolSchema);
			mTransferData.setNameAndValue("mYBKWXLCInsuredListSchema", mYBKWXLCInsuredListSchema);
			mTransferData.setNameAndValue("RenemalPayMethod", RenemalPayMethod);
			mTransferData.setNameAndValue("IfAutoPay", IfAutoPay);
			mTransferData.setNameAndValue("MedicalCode", MedicalCode);
			mTransferData.setNameAndValue("PayMode", PayMode);
			mTransferData.setNameAndValue("CustomerNo", CustomerNo);
			mTransferData.setNameAndValue("YBKSerialno", YBKSerialno);
			mVData.add(mGlobalInput);
			mVData.add(mTransferData);
			YBKWXCalCulateBL mYBKWXCalCulateBL =new YBKWXCalCulateBL();
			boolean flag = mYBKWXCalCulateBL.submitData(mVData, "YBK");
			
			if(flag){
				
				//正常生成数据
				//TODO 新单复核
				//获取投保工作流的MissionId
				mPrtNo =mYBKWXCalCulateBL.getPrtnoTT();
				String sqlMission = "select missionid from lwmission where missionprop1= '" + mPrtNo + "' fetch first 1 rows only";
				SSRS mSSRS = new ExeSQL().execSQL(sqlMission);
				if(mSSRS.getMaxRow()>0){
					mMissionid = mSSRS.GetText(1, 1);
				} else {
					errLog("核心工作流获取失败","0");
					return false;
				}
						
					// 工作流跳转到新单复核
				
					if (inputConfirm("", mPrtNo,mLCPolSchema.getRiskCode())) {
						System.out.println("核心自核正常执行");
						
						String sql = "select contno,amnt,prem,prtno,stateflag,uwflag from lccont where prtno = '"
								+ mPrtNo + "'";
						SSRS tSSRS = new ExeSQL().execSQL(sql);
						System.out.println("噢" + tSSRS.GetText(1, 1));
						if(mINSURE_INFO.getHEIGHT()!="" || mINSURE_INFO.getHEIGHT()!=null){
						String Str="select Uwrulecode from LCUWError where ContNo='" +tSSRS.GetText(1, 1) + "'";
					
						SSRS MSSRS = new ExeSQL().execSQL(Str);
	                    if(MSSRS.getMaxRow()>0){
	                    	for(int i=1 ;i<=MSSRS.getMaxCol();i++){
								if("430401".equals(MSSRS.GetText(i, 1)) || "429401".equals(MSSRS.GetText(i, 1))){
									flagState="1";
								}
							}
	                    }
						
						}
						if(!PrepareInfo()){
							errLog(mError);
							SetPrtNo(mPrtNo);
							return false;
						}
						getXmlResult();
						} else {
							errLog("核心自核失败: " + mError,"0");
							System.out.println("核心自核失败:");
							SetPrtNo(mPrtNo);
							return false;
						}
					
				SetPrtNo(mPrtNo);
				
			}else {
				//生成数据异常
				errLog(mYBKWXCalCulateBL.mErrors.getFirstError());
			}
			
			
		return true;
		}catch (Exception ex) {
			return false;
		}
	}
	
	public boolean  XmlToSchemaInfo(){

		mINSURE_INFO = (INSURE_INFO) INSURE_INFO_List.get(0);
		//新增校验
	    if(!checkData()){
	    	return false;
	    }
	    
	  //1、投保流水号
	  		YBKSerialno = mINSURE_INFO.getPOLICY_FORMER_NO();
	  		mYBKWXLCInsuredListSchema.setSerNo(mINSURE_INFO.getPOLICY_FORMER_NO());
	  		
	  		//2、后续将平台客户号存在ldperson里边
	  	    CustomerNo = mINSURE_INFO.getCUSTOMER_SEQUENCE_NO();
	  		
	  		//3、姓名
	  	    mYBKWXLCInsuredListSchema.setAccName(mINSURE_INFO.getNAME());
	  	    mYBKWXLCInsuredListSchema.setInsuredName(mINSURE_INFO.getNAME());
	  	    
	  		//4、性别
	  	    Sql="select code1 from ldcode1 where codetype='YbkSex' and code='" + mINSURE_INFO.getGENDER() + "'";
	  	    mSSRS = new ExeSQL().execSQL(Sql);
	  	    String ybkSex = "";
	  	    if (mSSRS == null || mSSRS.getMaxRow() <= 0) {
	  	    	errLog("性别不详");
				CError tError = new CError();
	            tError.moduleName = "YbkWXTB";
	            tError.functionName = "deal";
	            tError.errorMessage = "性别不详";
	            this.mErrors.addOneError(tError);
				return false;
	  		} else {
	  			mYBKWXLCInsuredListSchema.setSex(mSSRS.GetText(1, 1));
	  			ybkSex = mSSRS.GetText(1, 1);
	  		}
	  	  
	  		//5、出生年月
	  	    mYBKWXLCInsuredListSchema.setBirthDay(mINSURE_INFO.getBIRTHDAY());
	  		//6、证件类型
	  	    Sql="select code1 from ldcode1 where codetype='YbkIDType' and code='" + mINSURE_INFO.getCERTIFICATE_TYPE() + "'";
	  	    mSSRS = new ExeSQL().execSQL(Sql);
	  	    String idType ="";
	  	    if (mSSRS == null || mSSRS.getMaxRow() <= 0) {
	  	    	errLog("证件类型错误");
				CError tError = new CError();
	            tError.moduleName = "YbkWXTB";
	            tError.functionName = "deal";
	            tError.errorMessage = "证件类型错误";
	            this.mErrors.addOneError(tError);
				return false;
	  		} else {
	  			mYBKWXLCInsuredListSchema.setIDType(mSSRS.GetText(1, 1));//身份证
	  			idType = mSSRS.GetText(1, 1);
	  		}
	  	    
	  		//7、证件号码
	  	    mYBKWXLCInsuredListSchema.setIDNo(mINSURE_INFO.getCERTIFICATE_NO());
	  	    //20170821身份证校验
	  	    String result = PubFun.CheckIDNo(idType, mINSURE_INFO.getCERTIFICATE_NO(), "", ybkSex);
	  	    if(result != ""){
	  	    	System.out.println("身份证校验失败,失败原因:"+result);
	  	    	errLog(result);
				CError tError = new CError();
	            tError.moduleName = "YbkWXTB";
	            tError.functionName = "deal";
	            tError.errorMessage = result;
	            this.mErrors.addOneError(tError);
				return false;
	  	    }
	  		//8、后续将医保卡号存在lccontsub里边
	  	    MedicalCode= mINSURE_INFO.getMEDICAL_NO();
	  		//9、银行卡号
	  	    mYBKWXLCInsuredListSchema.setBankAccNo(mINSURE_INFO.getBANK_NO());
	  	    
	  		//10、所属银行
	  	    Sql="select code1 from ldcode1 where codetype='YbkBankCode' and code='" + mINSURE_INFO.getBANK_NAME() + "'";
	  	    mSSRS = new ExeSQL().execSQL(Sql);
	  	    if (mSSRS == null || mSSRS.getMaxRow() <= 0) {
	  	    	errLog("银行编码错误");
				CError tError = new CError();
	            tError.moduleName = "YbkWXTB";
	            tError.functionName = "deal";
	            tError.errorMessage = "银行编码错误";
	            this.mErrors.addOneError(tError);
				return false;
	  		} else {
	  			mYBKWXLCInsuredListSchema.setBankCode(mSSRS.GetText(1, 1));
	  		}	    
	  	    
	  		//11、手机号 20170911新增校验
	  	    if(mINSURE_INFO.getMOBILE()!=null){
	  	    	String mobile = mINSURE_INFO.getMOBILE();
	  	    	String pattern = "^1[\\d]{10}";
		  	    boolean isMatch = Pattern.matches(pattern, mobile);
		  	    if(isMatch==false){
		  	    	errLog("手机号码必须为11位数字");
					CError tError = new CError();
		            tError.moduleName = "YbkWXTB";
		            tError.functionName = "deal";
		            tError.errorMessage = "手机号码必须为11位数字";
		            this.mErrors.addOneError(tError);
		  	    	return isMatch;
		  	    }else{
		  	    	mYBKWXLCInsuredListSchema.setMobile(mINSURE_INFO.getMOBILE());
		  	    }
	  	    }
	  	    
	  		//12、验证通过方式
	  		//13、医保产品类型
	  	    Sql="select code1 from ldcode1 where codetype='YbkRiskType' and code='" + mINSURE_INFO.getMEDICAL_TYPE() + "'";
	  	    mSSRS = new ExeSQL().execSQL(Sql);
	  	    if (mSSRS == null || mSSRS.getMaxRow() <= 0) {
	  	    	errLog("医保产品类型错误");
				CError tError = new CError();
	            tError.moduleName = "YbkWXTB";
	            tError.functionName = "deal";
	            tError.errorMessage = "医保产品类型错误";
	            this.mErrors.addOneError(tError);
				return false;
	  		} else {
	  			mLCPolSchema.setRiskCode(mSSRS.GetText(1, 1));
	  			if("220602".equals(mSSRS.GetText(1, 1)) && mINSURE_INFO.getSUM_INSURED().equals("100000")){
	  				mYBKWXLCInsuredListSchema.setMult("1");
	  	    		mLCPolSchema.setMult("1");
	  			}else if("220602".equals(mSSRS.GetText(1, 1)) && mINSURE_INFO.getSUM_INSURED().equals("200000")){
	  				mYBKWXLCInsuredListSchema.setMult("2");
	  	    		mLCPolSchema.setMult("2");
	  			}
	  			
	  		}	
	  	    
	  		//14、投保申请日期
	  	    mYBKWXLCInsuredListSchema.setApplyDate(mINSURE_INFO.getAPPLICATION_DATE());
	  		//15、起保日期
	  	    mYBKWXLCInsuredListSchema.setEFFECTIVE_DATE(mINSURE_INFO.getEFFECTIVE_DATE());
	  		//17、满期日期
	  	    mYBKWXLCInsuredListSchema.setEXPIRE_DATE(mINSURE_INFO.getEXPIRE_DATE());
	  		//18、是否自动续保
	  	    mLCPolSchema.setRnewFlag(mINSURE_INFO.getIF_AUTO_RENEWAL());//0否1是
	  	    IfAutoPay = mINSURE_INFO.getIF_AUTO_RENEWAL();
	  		//19、扣款方式
	  	    PayMode = mINSURE_INFO.getDEDUCT_METHOD();//1:优先医保卡支付，银行卡支付辅助 2:仅医保卡支付 3:仅银行卡支付
	  		//20、续保扣款方式
	  	    mLCPolSchema.setExPayMode(mINSURE_INFO.getRENEWAL_DEDUCT_METHOD());//1:优先医保卡支付，银行卡支付辅助 2:仅医保卡支付 3:仅银行卡支付
	  	    RenemalPayMethod = mINSURE_INFO.getRENEWAL_DEDUCT_METHOD();
	  	    //21、联系地址
	  		mYBKWXLCInsuredListSchema.setPostalAddress(mINSURE_INFO.getADDRESS());
	  		mYBKWXLCInsuredListSchema.setPostalProvince(mINSURE_INFO.getADDRESS());
	  		mYBKWXLCInsuredListSchema.setPostalCity(mINSURE_INFO.getADDRESS());
	  		mYBKWXLCInsuredListSchema.setPostalCounty(mINSURE_INFO.getADDRESS());
	  		
	  	    	
	  		//22、邮箱
	  	    mYBKWXLCInsuredListSchema.setEmail(mINSURE_INFO.getEMAIL());
	  	    
	  		//23、期初保额
	  	    mLCPolSchema.setAmnt(mINSURE_INFO.getSUM_INSURED());
	  	    
	  		//24、销售渠道
	  	    	 mYBKWXLCInsuredListSchema.setSaleChnl("01");//默认个险直销
	  		//25、销售机构 
	  	    mYBKWXLCInsuredListSchema.setAgentCom("86310000");
	  	    mLCPolSchema.setManageCom("86310000");
	  		//销售人员工号
	  	    //外侧
	  	    mYBKWXLCInsuredListSchema.setAgentCode("2131067688");
	    	System.out.println("销售人员类型编码啊："+ mYBKWXLCInsuredListSchema.getAgentCode());
	  	    //正式
//	    	mYBKWXLCInsuredListSchema.setAgentCode("2131130086");
//  	    	System.out.println("销售人员类型编码啊："+ mYBKWXLCInsuredListSchema.getAgentCode());
	  	    	
	    // TODO 保单号
	    mYBKWXLCInsuredListSchema.setContNo("1");
	    //问题一
	    mYBKWXLCInsuredListSchema.setQUESTION_ONE(mINSURE_INFO.getQUESTION_ONE());
	  //问题二
	    mYBKWXLCInsuredListSchema.setQUESTION_TWO(mINSURE_INFO.getQUESTION_TWO());
	  //问题二复选疾病
	    mYBKWXLCInsuredListSchema.setQUESTION_TWO_SICK(mINSURE_INFO.getQUESTION_TWO_SICK());
	  //问题三
	    mYBKWXLCInsuredListSchema.setQUESTION_THREE(mINSURE_INFO.getQUESTION_THREE());
	  //问题四
	    mYBKWXLCInsuredListSchema.setQUESTION_FOUR(mINSURE_INFO.getQUESTION_FOUR());
	  //问题五
	    mYBKWXLCInsuredListSchema.setQUESTION_FIVE(mINSURE_INFO.getQUESTION_FIVE());
	  //问题六
	    mYBKWXLCInsuredListSchema.setQUESTION_SIX(mINSURE_INFO.getQUESTION_SIX());
	   //问题七
	    mYBKWXLCInsuredListSchema.setQUESTION_SEVEN(mINSURE_INFO.getQUESTION_SEVEN());
	    // TODO 
	    //保单号
	    mYBKWXLCInsuredListSchema.setContNo("1");
	    //缴费期间默认为趸缴
	    mYBKWXLCInsuredListSchema.setPayIntv("0");
	    //团单号
	    mYBKWXLCInsuredListSchema.setGrpNo("00000000000000000000");
	    //职业编码核心默认一种就可以微信端不需要
	    mYBKWXLCInsuredListSchema.setOccupationCode("06905");
	    mYBKWXLCInsuredListSchema.setOccupationType("2");
	    mYBKWXLCInsuredListSchema.setOccupationName("无业、待业人员");
	    mYBKWXLCInsuredListSchema.setInsuYear("1");
	    mYBKWXLCInsuredListSchema.setInsuYearFlag("Y");
	    mYBKWXLCInsuredListSchema.setPayEndYear("1");
	    mYBKWXLCInsuredListSchema.setPayEndYearFlag("Y");
	    mYBKWXLCInsuredListSchema.setstature(mINSURE_INFO.getHEIGHT());
	    mYBKWXLCInsuredListSchema.setavoirdupois(mINSURE_INFO.getWEIGHT());
	    mYBKWXLCInsuredListSchema.setNativePlace("ML");
	    //JIA de 
	    //mYBKWXLCInsuredListSchema.setIDStartDate("2002-01-01");
	    //mYBKWXLCInsuredListSchema.setIDEndDate("2023-01-01");
	    mLCPolSchema.setPolApplyDate(mINSURE_INFO.getAPPLICATION_DATE());
	    mLCPolSchema.setPayMode("4");
	    mYBKWXLCInsuredListSchema.setmarriage("0");
	    //mLCPolSchema.setExPayMode("2");
	    //mYBKWXLCInsuredListSchema.setZipCode("100100");
	    //mYBKWXLCInsuredListSchema.setmarriage("0");
	   // mYBKWXLCInsuredListSchema.setposition("知足下");
	    return true;
	}
	//新增校验
    public boolean checkData(){
    	
    	//保额
    	Double be=0.00;
    	if(!"".equals(mINSURE_INFO.getSUM_INSURED())&&mINSURE_INFO.getSUM_INSURED()!=null){
    		  be=Double.parseDouble(mINSURE_INFO.getSUM_INSURED());
    	}
    	//重疾--220602--获得年龄--投保年龄16-75周岁
    	//住院--123202--获得年龄--投保年龄大于等于16周岁
    	String nl=mINSURE_INFO.getBIRTHDAY();
    	int Nl=getNl(nl);//得到年龄，周岁
    	//产品类型
    	String Lx=mINSURE_INFO.getMEDICAL_TYPE();
    	if("01".equals(Lx)){//123202--住院
//    		投保份数 系统内有效险种只能投保一份 本险种不能重复投保
    		//以上涉及报文结构，不需校验
    		
    		String sql = " select 1 from lcpol lc where 1=1 and lc.InsuredNo in ( "
        		+" select CustomerNo from ldperson where 1=1 and  Ybkcustomerno ='"+mINSURE_INFO.getCUSTOMER_SEQUENCE_NO()+"' "
        		+" ) and lc.riskcode in ('123201','123202') and lc.uwflag not in ('a','1','2','8') and lc.stateflag not in ('2','3')" 
        		+" and not exists(select 1 from ybkerrorlist where businessno = lc.prtno and transtype = 'G03' and resultstatus = '08') ";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		System.out.println("噢");
		if (!"".equals(tSSRS) && tSSRS != null && tSSRS.getMaxRow() > 0) {
			errLog("本险种不能重复投保");
			CError tError = new CError();
            tError.moduleName = "YbkWXTB";
            tError.functionName = "deal";
            tError.errorMessage = "拒绝投保,该平台客户编码："+mINSURE_INFO.getCUSTOMER_SEQUENCE_NO()+",已投保上海医保账户住院自费医疗,本险种不能重复投保";
            this.mErrors.addOneError(tError);
			return false;
		}
    		
//    		123202--住院
    		if(Nl<16){
    			System.out.println("被保险人年龄超出产品定义范围");
    			//此处加入ERROR;
//    			mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
//				mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保,被保险人年龄超出产品定义范围");
//				errLog("拒绝投保,被保险人年龄超出产品定义范围","0");
//    			return false;
    			CError tError = new CError();
                tError.moduleName = "YbkWXTB";
                tError.functionName = "deal";
                tError.errorMessage = "拒绝投保,被保险人年龄超出产品定义范围";
                this.mErrors.addOneError(tError);
                return false;
                
    		}
    		if(be!=100000.00){
    			System.out.println("投保保额不符合要求");
    			//此处加入ERROR;
//    			mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
//				mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保,投保保额不符合要求");
//				errLog("拒绝投保,投保保额不符合要求","0");
//    			return false;
    			CError tError = new CError();
                tError.moduleName = "YbkWXTB";
                tError.functionName = "deal";
                tError.errorMessage = "拒绝投保,投保保额不符合要求";
                this.mErrors.addOneError(tError);
                return false;
    		}
    	}else if("02".equals(Lx)){
//    		220602--重疾
    		if(Nl<16 || Nl>75){
    			System.out.println("被保险人年龄超出产品定义范围");
    			//此处加入ERROR;
//    			mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
//				mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保,被保险人年龄超出产品定义范围");
//				errLog("拒绝投保,被保险人年龄超出产品定义范围","0");
//    			return false;
    			CError tError = new CError();
                tError.moduleName = "YbkWXTB";
                tError.functionName = "deal";
                tError.errorMessage = "拒绝投保,被保险人年龄超出产品定义范围";
                this.mErrors.addOneError(tError);
                return false;
    		}
    		//累计保额不能超过20万
//    		投保份数 系统内有效险种只能投保一份 本险种不能重复投保
    		//以上涉及报文结构，不需校验
    		
    		String sqlAmnt = " select nvl(sum(amnt),0) from lcpol lc where 1=1 and lc.InsuredNo in ( "
        		+" select CustomerNo from ldperson where 1=1 and  Ybkcustomerno ='"+mINSURE_INFO.getCUSTOMER_SEQUENCE_NO()+"' "
        		+" ) and lc.riskcode in ('220601','220602') and lc.uwflag not in ('a','1','2','8') and lc.stateflag not in ('2','3')" 
        		+" and not exists(select 1 from ybkerrorlist where businessno = lc.prtno and transtype = 'G03' and resultstatus = '08') ";
		String ttSSRS = new ExeSQL().getOneValue(sqlAmnt);
		if (!"".equals(ttSSRS) && ttSSRS != null) {
			Double sum= Double.parseDouble(ttSSRS) + be;
			System.out.println(be+"gekong"+sum);
			if(sum>200000.00){
				System.out.println("险种累计保额超出核保规定");
				CError tError = new CError();
	            tError.moduleName = "YbkWXTB";
	            tError.functionName = "deal";
	            tError.errorMessage = "拒绝投保,本险种最高累计保额20万元,累计保额超出核保规定";
	            this.mErrors.addOneError(tError);
	            return false;
			}
		}
    		if(be!=100000.00 && be!=200000.00){
    			System.out.println("投保保额不符合要求");
    			//此处加入ERROR;
//    			mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
//				mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保,投保保额不符合要求");
//				errLog("拒绝投保,投保保额不符合要求","0");
//    			return false;
    			CError tError = new CError();
                tError.moduleName = "YbkWXTB";
                tError.functionName = "deal";
                tError.errorMessage = "拒绝投保,投保保额不符合要求";
                this.mErrors.addOneError(tError);
                return false;
    		}
    	}else {
    		//非住院非重疾
    		System.out.println("不存在该产品:"+Lx);
    		//ERROR
//    		mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
//			mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保,不存在该产品:"+Lx+"!");
//			errLog("拒绝投保,不存在该产品:"+Lx+"!","0");
//    		return false;
    		CError tError = new CError();
            tError.moduleName = "YbkWXTB";
            tError.functionName = "deal";
            tError.errorMessage = "拒绝投保,不存在该产品:"+Lx+"!";
            this.mErrors.addOneError(tError);
            return false;
    	}
        //人员关系-------被保险人只有1人，且为投保人本人，通过，无需校验
    	
        //缴费频次-------验证是趸缴,已经写死是趸缴，无需校验
    	
    	//保险期间--------1年，通过
    	int Qj=getQj();
    	if(Qj!=1){
    		//添加错误信息到Error中
    		System.out.println("保险期间不符合产品定义内容");
//    		mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
//			mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保,保险期间不符合产品定义内容");
//			errLog("拒绝投保,保险期间不符合产品定义内容");
//    		return false;
    		CError tError = new CError();
            tError.moduleName = "YbkWXTB";
            tError.functionName = "deal";
            tError.errorMessage = "拒绝投保,保险期间不符合产品定义内容";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	return true;
    }
    /**
	 * @param 保险期间
	 * @return
	 */
    public int getQj(){
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd"); 
    	Date dateQ =null;
    	Date dateM =null;
    	try {
        	//起保日期
			 dateQ = sdf.parse(mINSURE_INFO.getEFFECTIVE_DATE());
			//满期日期
	    	 dateM = sdf.parse(mINSURE_INFO.getEXPIRE_DATE());
	    	
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		  
	     Calendar   calendar   =   new   GregorianCalendar(); 
	     calendar.setTime(dateM); 
	     calendar.add(calendar.DATE,1);//把日期往后增加一天.整数往后推,负数往前移动 
	     dateM=calendar.getTime();   //这个时间就是日期往后推一天的结果 
	    return dateM.getYear()-dateQ.getYear();
    }
    
	/**
	 * @param 年龄
	 * @return
	 * @throws ParseException 
	 */
    public int getNl(String Nl) {
    	String strBirthday=Nl;
    	SimpleDateFormat sdf= new SimpleDateFormat("yyyyMMdd");
    	Date date=null;
		try {
			date = sdf.parse(strBirthday);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	System.out.println(date);
            int returnAge;
            int birthYear = date.getYear();
            int birthMonth = date.getMonth();
            int  birthDay = date.getDay();
            
            Date d = new Date();
            int nowYear = d.getYear();
            int nowMonth = d.getMonth() + 1;
            int nowDay = d.getDate();
            
            if(nowYear == birthYear)
            {
                returnAge = 0;//同年 则为0岁
            }
            else
            {
                int ageDiff = nowYear - birthYear ; //年之差
                if(ageDiff > 0)
                {
                    if(nowMonth == birthMonth)
                    {
                        int dayDiff = nowDay - birthDay;//日之差
                        if(dayDiff < 0)
                        {
                            returnAge = ageDiff - 1;
                        }
                        else
                        {
                            returnAge = ageDiff ;
                        }
                    }
                    else
                    {
                        int monthDiff = nowMonth - birthMonth;//月之差
                        if(monthDiff < 0)
                        {
                            returnAge = ageDiff - 1;
                        }
                        else
                        {
                            returnAge = ageDiff ;
                        }
                    }
                }
                else
                {
                    returnAge = -1;//返回-1 表示出生日期输入错误 晚于今天
                }
            }
            
            return returnAge;//返回周岁年龄
    }
    
	/**
	 * 进行新单复核
	 * 
	*/
	private boolean inputConfirm(String cBPOBatchNo, String cPrtNo,String RiskCode) {

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setPrtNo(cPrtNo);
		LCContSet tLCContSet = tLCContDB.query();
		if (tLCContSet.size() == 0) {
			mError = "没有查询到以下印刷号的保单" + cPrtNo;
			return false;
		}

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("ContNo", tLCContSet.get(1).getContNo());
		mTransferData.setNameAndValue("PrtNo", cPrtNo);
		mTransferData
				.setNameAndValue("AppntNo", tLCContSet.get(1).getAppntNo());
		mTransferData.setNameAndValue("AppntName", tLCContSet.get(1)
				.getAppntName());
		mTransferData.setNameAndValue("AgentCode", tLCContSet.get(1)
				.getAgentCode());
		mTransferData.setNameAndValue("ManageCom", tLCContSet.get(1)
				.getManageCom());
		mTransferData.setNameAndValue("Operator", mGlobalInput.Operator);
		mTransferData.setNameAndValue("MakeDate", tLCContSet.get(1)
				.getMakeDate());
		mTransferData.setNameAndValue("MissionID", mMissionid);
		mTransferData.setNameAndValue("SubMissionID", "1");

		VData tVData = new VData();
		tVData.add(mTransferData);
		tVData.add(mGlobalInput);
		TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
		if (!tTbWorkFlowUI.submitData(tVData, STATIC_ActivityID)) {
			mError = tTbWorkFlowUI.mErrors.getError(0).errorMessage;
			return false;
		}

		return true;
	}

	
	/**
	 * 返回信息报文
	 * 
	*/
	public void getXmlResult() {

		// 返回数据节点
		putResult("SUCCESS_INFO", mSUCCESS_INFO);

	}

	
	/**
	 * 获取保单信息
	 * 
	*/
		public boolean PrepareInfo() {

			String sql = "select contno,amnt,prem,prtno,stateflag,uwflag from lccont where prtno = '"
					+ mPrtNo + "'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			System.out.println("噢");
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("获取保单信息失败","1");
				return false;
			} else {
				ContNo = tSSRS.GetText(1, 1);
				System.out.println("保单号："+ContNo);
				mSUCCESS_INFO.setPOLICY_FORMER_NO(YBKSerialno);
				if("9".equals(tSSRS.GetText(1, 6))){
					
					//自核之后，把暂收数据锁上，为了不能正常收费
					 //校验有无待收费信息
					String tempinfo = "select tempfeeno from ljtempfee where otherno='"
							+ mPrtNo + "' and enteraccdate is null";
					SSRS tSSRS1 = new ExeSQL().execSQL(tempinfo);
					if (tSSRS1 == null || tSSRS1.getMaxRow() <= 0) {
						errLog("保单无待收费信息","1");
						System.out.println("保单无待收费信息");
						return false;
					}
					//更新暂收分类表
					String sqltempclass = "select * from ljtempfeeclass where tempfeeno='"
							+ tSSRS1.GetText(1, 1) + "'";
					System.out.println("更新暂收" +sqltempclass);
					LJTempFeeClassDB mLJTempFeeClassDB = new LJTempFeeClassDB();
					LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
					mLJTempFeeClassSet = mLJTempFeeClassDB.executeQuery(sqltempclass);
					if(mLJTempFeeClassSet.size()!=0){
						
						for (int i = 0; i < mLJTempFeeClassSet.size(); i++){
				            
				            LJTempFeeClassSchema mLJTempFeeClassSchema = mLJTempFeeClassSet.get(i + 1);

				            System.out.println("医保扣款锁数据:" + mLJTempFeeClassSchema.getTempFeeNo());
				            mLJTempFeeClassSchema.setBankCode("");
				            mLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
				            mLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
				            
				            mLJTempFeeClassSet.set(i + 1, mLJTempFeeClassSchema);
				        }
					}
						// 提交数据
						MMap tmap = new MMap();
						tmap.put(mLJTempFeeClassSet, "UPDATE");
			            VData mInputData = new VData();
						mInputData.clear();
						mInputData.add(tmap);
						tPubSubmit.submitData(mInputData, "");
					
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU01");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("通过" + mError);//正常承保状态
					errLog("核心自核通过","1");
					System.out.println("自核通过");
				}else if("5".equals(tSSRS.GetText(1, 6))){
					if(flagState.equals("1")){
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保、体重指数超标" + mError);//待人工核保状态
					errLog("拒绝投保、体重指数超标","1");
					System.out.println("拒绝投保、体重指数超标");
					} else if("1".equals(mINSURE_INFO.getQUESTION_ONE())){
						mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
						mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保、健康告知第一项是：是" + mError);//待人工核保状态
						errLog("拒绝投保、健康告知第一项是：是","1");
						System.out.println("拒绝投保、健康告知第一项是：是");
					}else if("1".equals(mINSURE_INFO.getQUESTION_FOUR())){
						mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
						mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保、健康告知第四项是：是" + mError);//待人工核保状态
						errLog("拒绝投保、健康告知第四项是：是","1");
						System.out.println("拒绝投保、健康告知第四项是：是");
					} else if("1".equals(mINSURE_INFO.getQUESTION_SIX())){
						mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
						mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保、健康告知第六项是：是" + mError);//待人工核保状态
						errLog("拒绝投保、健康告知第六项是：是","1");
						System.out.println("拒绝投保、健康告知第六项是：是");
					} 
					else{
						mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU03");
						mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("待人工核保" + mError);//待人工核保状态
						errLog("待人核","1");
						System.out.println("自核未通过待人核");
					}
					
				} else if("1".equals(tSSRS.GetText(1, 6))){
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("拒绝投保" + mError);
					errLog("拒绝投保","0");
					System.out.println("拒绝投保");
				}else if("2".equals(tSSRS.GetText(1, 6))){
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("不通过、延期承保" + mError);
					errLog("延期承保","0");
					System.out.println("延期承保");
				}else if("3".equals(tSSRS.GetText(1, 6))){
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("不通过、条件承保" + mError);
					errLog("条件承保","0");
					System.out.println("条件承保");
				}else{
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("不通过" + mError);
					errLog("自核失败状态也没待人核","0");
					System.out.println("自核未通过");
				}
				
			}

			return true;
		}

	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}
}
