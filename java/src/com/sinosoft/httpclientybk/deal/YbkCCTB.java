package com.sinosoft.httpclientybk.deal;
/*
 * 程序功能:CC缴费资料变更
 * 时间:2017-02-
 * 于坤
 */

import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G05.ENDORSEMENT_INFO;
import com.sinosoft.httpclientybk.dto.G05.SUCCESS_INFO;
import com.sinosoft.httpclientybk.xml.ctrl.AEdorBusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbkCCTB extends AEdorBusLogic {
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	public ENDORSEMENT_INFO mEdorSementInfo;
	private LCContSchema mLCContSchema;
	/* 保单编码 */
	private String mPolicyFormerNo;
	/* 核心保单号 */
	private String tContNo;
	/* 银行卡号 */
	private String mBankNo;
	/* 平台银行编码 */
	private String mBankName;
	/* 核心银行编码 */
	private String mBankName1;
	/* 印刷号 */
	private String prtNo;
	
	YbkCCBL tYbkCCBL = new YbkCCBL();
	private SUCCESS_INFO mSuccInfo = new SUCCESS_INFO();
	
	/*用于银行卡号,银行账户名,银行,缴费方式的数据传递*/
	VData tVData = new VData();
	TransferData tTransferData = new TransferData();

	public YbkCCTB() {
	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return false;
	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理医保卡保全业务");
		// 解析XML
		if (!parseXML(cMsgInfos)) {
			return false;
		}
		// 先进行下必要的数据校验
		if (!checkData()) {
			System.out.println("数据校验失败---");
			// 组织返回报文
			mSuccInfo.setERROR_REASON("传入数据校验失败---");
			mSuccInfo.setIF_CAN_CANCEL("0");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		}

	    String tsql="select code from ldcode where codetype = 'ybkbquser'";
	      SSRS tSSRS1=new SSRS();
	      ExeSQL tExeSQL=new ExeSQL();
	      tSSRS1=tExeSQL.execSQL(tsql);
	    mGlobalInput.ManageCom = "86310000";
	    mGlobalInput.ComCode = "86310000";
	    mGlobalInput.Operator = tSSRS1.GetText(1, 1);
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(mEdorSementInfo);
		data.add(mLCContSchema);
		if (!tYbkCCBL.submit(data)) {
			System.out.println("保全操作错误");
			String errorMsg = tYbkCCBL.mErrors.getFirstError();
			errLog(errorMsg, "E");
			mSuccInfo.setERROR_REASON(errorMsg);
			// 组织返回报文
			mSuccInfo.setIF_CAN_CANCEL("0");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		} else {
			// 组织返回报文
			errLog("Success", "1");
			mSuccInfo.setIF_CAN_CANCEL("1");
			mSuccInfo.setCANCEL_PREMIUM("0.0");
			putResult("SUCCESS_INFO", mSuccInfo);
			return true;
		}
	}

	private boolean parseXML(MsgCollection cMsgInfos) {
		System.out.println("开始解析XML");
		// 获取保单信息
		List<ENDORSEMENT_INFO> tEdorInfoList = cMsgInfos.getBodyByFlag("BODY");

		if (tEdorInfoList == null || tEdorInfoList.size() != 1) {
			errLog("申请报文中获取工单信息失败!", "0");
			return false;
		}
		mEdorSementInfo = tEdorInfoList.get(0); // 接口一次只接受一个保单的犹豫期退保
		// 获取contno
		mPolicyFormerNo = mEdorSementInfo.getPOLICY_SEQUENCE_NO();
		// 获取bankno
		mBankNo = mEdorSementInfo.getBANK_NO();
		System.out.println("mBankNo==="+mBankNo);
		// 获取bankname
		mBankName = mEdorSementInfo.getBANK_NAME();
		System.out.println("平台银行编码==="+mBankName);
		// 将平台银行编码转换为核心银行编码
		String yhbmsql="select code1 from ldcode1 where codetype='YbkBankCode' and code = '"+ mBankName +"'";
		ExeSQL tExeSQL1 = new ExeSQL();
        SSRS tSSRS1 = new SSRS();
        tSSRS1 = tExeSQL1.execSQL(yhbmsql);
        mBankName1 = tSSRS1.GetText(1, 1);
        System.out.println("核心银行编码==="+mBankName1);
        
		// 向bl中传输数据
		tTransferData.setNameAndValue("mBankNo", mBankNo);
		tTransferData.setNameAndValue("mBankName1", mBankName1);
		tVData.add(tTransferData);
		tYbkCCBL.getInputData1(tVData);

		// 查询保单信息
		LCContDB tLCContDB = new LCContDB();
		String sql = "select contno,prtno from lccont where prtno in (select prtno from YBK_N01_LIS_ResponseInfo where PolicySequenceNo = '"
				+ mPolicyFormerNo + "' and responsecode = '1' ) with ur";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			errLog("ContNo获取失败!", "0");
			return false;
		}
		this.tContNo = tSSRS.GetText(1, 1);
		System.out.println("tContNo=" + tContNo);
		tLCContDB.setContNo(tContNo);
		if (!tLCContDB.getInfo()) {
			errLog("传入的保单号错误，核心系统不存在保单.保单编码:“" + mPolicyFormerNo + "”", "0");
			return false;
		}
		mLCContSchema = tLCContDB.getSchema();
		
		//根据prtno向日志表添加数据
		this.prtNo = tSSRS.GetText(1, 2);
		System.out.println("prtNo=" + prtNo);
		SetPrtNo(prtNo);

		return true;
	}

	private boolean checkData() {
		/** 对保全申请信息的校验 */
		if (null == mEdorSementInfo.getPOLICY_SEQUENCE_NO()
				|| "".equals(mEdorSementInfo.getPOLICY_SEQUENCE_NO())) {
			errLog("保单编码不能为空", "0");
			return false;
		}
		if (null == mEdorSementInfo.getENDORSEMENT_APPLICATION_DATE()
				|| "".equals(mEdorSementInfo.getENDORSEMENT_APPLICATION_DATE())) {
			errLog("申请日期不能为空", "0");
			return false;
		}
		if (null == mEdorSementInfo.getEND_APPLY_NO()
				|| "".equals(mEdorSementInfo.getEND_APPLY_NO())) {
			errLog("保全申请流水号不能为空", "0");
			return false;
		}
		if (null == mEdorSementInfo.getIF_CANCEL()
				|| "".equals(mEdorSementInfo.getIF_CANCEL())) {
			errLog("是否退保不能为空", "0");
			return false;
		}
		if ("1".equals(mEdorSementInfo.getIF_CANCEL()))
			if (null == mEdorSementInfo.getCANCEL_REASON()
					|| "".equals(mEdorSementInfo.getCANCEL_REASON())) {
				errLog("退保原因不能为空", "0");
				return false;
			}
		if ("0".equals(mEdorSementInfo.getIF_CANCEL())) {
			if (null == mEdorSementInfo.getIF_AUTO_RENEWAL()
					|| "".equals(mEdorSementInfo.getIF_AUTO_RENEWAL())) {
				errLog("是否自动续保不能为空", "0");
				return false;
			}
			if ("1".equals(mEdorSementInfo.getIF_AUTO_RENEWAL()))
				if (null == mEdorSementInfo.getRENEWAL_DEDUCT_METHOD()
						|| "".equals(mEdorSementInfo.getRENEWAL_DEDUCT_METHOD())) {
					errLog("续保扣款方式不能为空", "0");
					return false;
				}
//			if (null == mEdorSementInfo.getMEDICAL_NO()
//					|| "".equals(mEdorSementInfo.getMEDICAL_NO())) {
//				errLog("医保卡号不能为空", "0");
//				return false;
//			}
			if (null == mEdorSementInfo.getBANK_NO()
					|| "".equals(mEdorSementInfo.getBANK_NO())) {
				errLog("银行卡号不能为空", "0");
				return false;
			}
			if (null == mEdorSementInfo.getBANK_NAME()
					|| "".equals(mEdorSementInfo.getBANK_NAME())) {
				errLog("所属银行不能为空", "0");
				return false;
			}
			if (null == mEdorSementInfo.getMOBILE()
					|| "".equals(mEdorSementInfo.getMOBILE())) {
				errLog("手机号不能为空", "0");
				return false;
			}
			if (null == mEdorSementInfo.getADDRESS()
					|| "".equals(mEdorSementInfo.getADDRESS())) {
				errLog("联系地址不能为空", "0");
				return false;
			}

		}
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			errLog("提交数据库发生错误" + tPubSubmit.mErrors, "E");
			return false;
		}
		return true;
	}

	/**
	 * 返回信息报文
	 * 
	 */
	public void getXmlResult() {

		// 返回数据节点
		putResult("SUCCESS_INFO", mSuccInfo);
	}

}
