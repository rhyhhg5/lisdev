package com.sinosoft.httpclientybk.deal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G03.MEDICAL_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.cbcheck.ApplyRecallPolUI;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.YBK_G03_LIS_ResponseInfoDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCApplyRecallPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCContSubSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

public class YbkFeeTB extends ABusLogic {

	private MEDICAL_INFO mMEDICAL_INFO;

	private GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	PubSubmit tPubSubmit = new PubSubmit();

	LCContSchema mLCContSchema = null;

	LCContSubSchema mLCContSubSchema = null;

	private String mPrtNo = "";
	private String mContNo = "";
	private String mBankCode = "";
	private String mRiskCode = "";
	private String tRnewCount = "";
	//归集账号
	private String tInsBankcode= "";
	private String tInsBankaccno = "";
	//判断医保扣费是否成功
	private Boolean MedicalKF = true;
	YBK_G03_LIS_ResponseInfoDB db = null;
	//签单成功标志
	private String FlagStr = "Succ";

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理G03医保卡扣款确认过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		tMsgHead.getREQUEST_TYPE();

		mGlobalInput.Operator = "YBK";

		try {
			List MEDICAL_INFO_List = cMsgInfos.getBodyByFlag("BODY");
			if (MEDICAL_INFO_List == null || MEDICAL_INFO_List.size() != 1) {
				errLog("获取医保扣款信息失败。", "0");
				return false;
			}

			mMEDICAL_INFO = (MEDICAL_INFO) MEDICAL_INFO_List.get(0);
			//保存G03结果表
			saveMedicalInfo();

			// 投保流水号是客户号需要查询核心印刷号
			String sqlPrtNo = "select a.prtno,b.contno,b.bankcode,c.riskcode "
					+ " from lccontsub a,lccont b,lcpol c "
					+ " where a.prtno = b.prtno "
					+ " and b.prtno = c.prtno "
					+ " and a.YbkSerialno = '"+ mMEDICAL_INFO.getPOLICY_FORMER_NO() + "'"
					+ " union "
					+ " select lc.prtno,lc.contno,lc.bankcode,lp.riskcode "
					+ " from ybk_e03_lis_responseinfo ybk,lccont lc,lcpol lp "
					+ " where ybk.edorno = lc.contno "
					+ " and lc.contno = lp.contno "
					+ " and ybk.policyformerno = '"+mMEDICAL_INFO.getPOLICY_FORMER_NO()+"'";
			SSRS tSSRSPrt = new ExeSQL().execSQL(sqlPrtNo);
			if (tSSRSPrt == null || tSSRSPrt.getMaxRow() <= 0) {
				return false;
			} else {
				mPrtNo = tSSRSPrt.GetText(1, 1);
				mContNo = tSSRSPrt.GetText(1, 2);
				mBankCode = tSSRSPrt.GetText(1, 3);
				mRiskCode = tSSRSPrt.GetText(1, 4);
			}

			String sql = "select * from lccont where prtno = '" + mPrtNo + "' ";
			LCContDB tLCContDB = new LCContDB();
			LCContSet mLCContSet = tLCContDB.executeQuery(sql);
			if (mLCContSet.size() == 0 || mLCContSet == null) {
				errLog("保单不存在", "0");
				return false;
			} else {
				mLCContSchema = mLCContSet.get(1);
			}

			// 从lccontsub里边拿到医保卡号和真正的扣款方式
			String sqlsub = "select * from lccontsub where prtno = '" + mPrtNo + "' ";
			LCContSubDB tLCContSubDB = new LCContSubDB();
			LCContSubSet mLCContSubSet = tLCContSubDB.executeQuery(sqlsub);
			if (mLCContSubSet.size() == 0 || mLCContSubSet == null) {
				errLog("保单附属信息不存在", "0");
				return false;
			} else {
				mLCContSubSchema = mLCContSubSet.get(1);
			}
			// 续保抽档判断
			String xbsql = "select 1 from ljspay a,LJSPayPerson b,lccont c where a.otherno = b.contno and b.contno = c.contno and c.appflag = '1' and a.otherno = '" + mContNo + "' with ur";
			String ifxb = new ExeSQL().getOneValue(xbsql);
			// if true为首期
			if ("".equals(ifxb) || null == ifxb) {
				// 校验有无待收费信息
				String sqltemp = "select * from ljtempfee where otherno='" + mPrtNo + "' and enteraccdate is null";
				LJTempFeeDB mLJTempFeeDB = new LJTempFeeDB();
				LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
				LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();
				mLJTempFeeSet = mLJTempFeeDB.executeQuery(sqltemp);
				if (mLJTempFeeSet.get(1) == null) {
					errLog("保单无待收费信息", "0");
					return false;
				} else {
					mLJTempFeeSchema = mLJTempFeeSet.get(1);
				}
				// 更新暂收分类表
				String sqltempclass = "select * from ljtempfeeclass where tempfeeno='" + mLJTempFeeSchema.getTempFeeNo()
						+ "'";
				LJTempFeeClassDB mLJTempFeeClassDB = new LJTempFeeClassDB();
				LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
				mLJTempFeeClassSet = mLJTempFeeClassDB.executeQuery(sqltempclass);
				LJTempFeeClassSchema mLJTempFeeclassSchema = mLJTempFeeClassSet.get(1);
				
				String sqlTempFee = "";
				String sqlTempfeclass = "";
				String sqlTempe = "";
				String sqlCont = "";
				String sqlLcpol = "";
				MMap tmap = new MMap();
				
				//lccontsub的 4对应1,5对应3，但是暂收表的paymode要置为11
				if (mLJTempFeeClassSet.size() != 0) {
					// 医保扣款失败
					if ("0".equals(mMEDICAL_INFO.getMEDICAL_IF_SUCESS())) {
						// 扣款失败原因为08-数据异常时，为医保系统异常情况，需人工干预，要求公司这种情况下支持平台重新提交
						if ("08".equals(mMEDICAL_INFO.getFAIL_REASON())) {
							errLog1("扣款失败，失败原因为" + mMEDICAL_INFO.getFAIL_REASON() + "数据异常0", "0", "08", "");
						}
						// 1优先医保卡支付，银行卡支付辅助 或者3仅银行卡支付
						else if (mLCContSubSchema.getPayMode().equals("1")&& "06".equals(mMEDICAL_INFO.getFAIL_REASON())) {
							//更新暂收表的险种号以便于结算
							sqlTempFee = "update ljtempfee set riskcode = '"+mRiskCode+"' where tempfeeno = '"+mLJTempFeeSchema.getTempFeeNo()+"'";
							
							sqlTempfeclass = "update LJTempFeeClass set PayMode='4',BankCode='"
									+ mLCContSchema.getBankCode()
									+ "' ,ModifyDate= current date,ModifyTime=current time where TempFeeNo='"
									+ mLJTempFeeclassSchema.getTempFeeNo() + "' and paymode='"
									+ mLJTempFeeclassSchema.getPayMode() + "'";
							sqlCont = "update lccont set paymode='4',expaymode='4' where prtno ='"
									+ mLCContSchema.getPrtNo() + "'";
							
							sqlLcpol = "update lcpol set paymode='4',expaymode='4' where prtno ='"
									+ mLCContSchema.getPrtNo() + "'";
							
							tmap.put(sqlTempFee, "UPDATE");
							tmap.put(sqlTempfeclass, "UPDATE");
							tmap.put(sqlCont, "UPDATE");
							tmap.put(sqlLcpol, "UPDATE");
							errLog("等待核心银行卡支付0", "1");
							
						} else if(mLCContSubSchema.getPayMode().equals("4") && "06".equals(mMEDICAL_INFO.getFAIL_REASON())){
							MedicalKF = false;
							String sqlgj = "select code,code1 from ldcode1 where codetype = 'SHYBgjzh'";
							SSRS tSSRSGJ = new ExeSQL().execSQL(sqlgj);
							if(tSSRSGJ.getMaxRow()>0){
								tInsBankcode= tSSRSGJ.GetText(1, 1);
								tInsBankaccno = tSSRSGJ.GetText(1, 2);
							}else{
								tInsBankcode = "";
								tInsBankaccno = "";
							}
							//更新暂收表的险种号以便于结算
							sqlTempFee = "update ljtempfee set riskcode = '"+mRiskCode+"', enteraccdate = current date,confdate = current date,confmakedate = current date,confmaketime = current time where tempfeeno = '"+mLJTempFeeSchema.getTempFeeNo()+"'"
									+ "";
							
							sqlTempfeclass = "update LJTempFeeClass set insbankcode = '"+tInsBankcode+"', insbankaccno = '"+tInsBankaccno+"', PayMode='11',BankCode='"
									+ mLCContSchema.getBankCode()
									+ "' ,ModifyDate= current date,ModifyTime=current time, enteraccdate = current date,confdate = current date,confmakedate = current date,confmaketime = current time where TempFeeNo='"
									+ mLJTempFeeclassSchema.getTempFeeNo() + "' and paymode='"
									+ mLJTempFeeclassSchema.getPayMode() + "'";
							sqlCont = "update lccont set paymode='11',expaymode='11' where prtno ='"
									+ mLCContSchema.getPrtNo() + "'";
							
							sqlLcpol = "update lcpol set paymode='11',expaymode='11' where prtno ='"
									+ mLCContSchema.getPrtNo() + "'";
							
							tmap.put(sqlTempFee, "UPDATE");
							tmap.put(sqlTempfeclass, "UPDATE");
							tmap.put(sqlCont, "UPDATE");
							tmap.put(sqlLcpol, "UPDATE");
							errLog("等待核心支付宝支付0", "1");
							
							// 仅医保卡支付 核心什么也不做
						} else {
							errLog("医保扣款失败终止投保(" + mMEDICAL_INFO.getFAIL_REASON() + ")0", "1");
							policyCancle();
						}

						// 医保扣款返回成功，但是只医保卡扣款情况
					} else if ("1".equals(mMEDICAL_INFO.getMEDICAL_IF_SUCESS())) {
						if (!mMEDICAL_INFO.getDEDUCT_MONEY().equals("") && !mMEDICAL_INFO.getDEDUCT_MONEY().equals(null)
								&& Double.parseDouble(mMEDICAL_INFO.getACTUAL_DEDUCT_PREMIUM()) != 0) {
							String dateString = StringPattern(mMEDICAL_INFO.getDEDUCT_MONEY(), "yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss");
							String date = dateString.substring(0, 10);
							String time = dateString.substring(11, 19);
							
							sqlTempfeclass = "update LJTempFeeClass set PayMode='8',BankCode='"
									+ mLCContSchema.getBankCode() + "' ,bankaccno ='"
									+ mLCContSubSchema.getMedicalCode() + "' ,EnterAccDate ='" + date
									+ "' ,ConfMakeDate ='" + date + "' ,ConfMakeTime ='" + time
									+ "',ModifyDate= current date,ModifyTime=current time where TempFeeNo='"
									+ mLJTempFeeclassSchema.getTempFeeNo() + "' and paymode='"
									+ mLJTempFeeclassSchema.getPayMode() + "'";

							sqlTempe = "update LJTempFee set EnterAccDate ='" + date + "' ,ConfMakeDate ='" + date
									+ "' ,ConfMakeTime ='" + time + "',"
									+ " riskcode = '"+mRiskCode+"', "
									+ " ModifyDate= current date,ModifyTime=current time where TempFeeNo='"
									+ mLJTempFeeSchema.getTempFeeNo() + "'";

							sqlCont = "update lccont set paymode='8',expaymode='8' where prtno ='"
									+ mLCContSchema.getPrtNo() + "'";

							sqlLcpol = "update lcpol set paymode='8',expaymode='8' where prtno ='"
									+ mLCContSchema.getPrtNo() + "'";
							
							tmap.put(sqlTempe, "UPDATE");
							tmap.put(sqlCont, "UPDATE");
							tmap.put(sqlTempfeclass, "UPDATE");
							tmap.put(sqlLcpol, "UPDATE");
							errLog("医保扣款已经成功0", "1");
						} else {
							if ("08".equals(mMEDICAL_INFO.getFAIL_REASON())) {
								errLog1("扣款失败，失败原因为" + mMEDICAL_INFO.getFAIL_REASON() + "数据异常0", "0", "08", "");
							}
							// 银行卡扣款的
							else if ((mLCContSubSchema.getPayMode().equals("1") && "06".equals(mMEDICAL_INFO.getFAIL_REASON()))|| mLCContSubSchema.getPayMode().equals("3")) {
								//更新暂收表的险种号以便于结算
								sqlTempFee = "update ljtempfee set riskcode = '"+mRiskCode+"' where tempfeeno = '"+mLJTempFeeSchema.getTempFeeNo()+"'";
								
								sqlTempfeclass = "update LJTempFeeClass set PayMode='4',BankCode='"
										+ mLCContSchema.getBankCode()
										+ "' ,ModifyDate= current date,ModifyTime=current time where TempFeeNo='"
										+ mLJTempFeeclassSchema.getTempFeeNo() + "' and paymode='"
										+ mLJTempFeeclassSchema.getPayMode() + "'";
								
								sqlCont = "update lccont set paymode='4',expaymode='4' where prtno ='"
										+ mLCContSchema.getPrtNo() + "'";
								
								sqlLcpol = "update lcpol set paymode='4',expaymode='4' where prtno ='"
										+ mLCContSchema.getPrtNo() + "'";
								
								tmap.put(sqlTempFee, "UPDATE");
								tmap.put(sqlCont, "UPDATE");
								tmap.put(sqlLcpol, "UPDATE");
								tmap.put(sqlTempfeclass, "UPDATE");
								errLog("等待核心银行卡支付0", "1");
							} else if (mLCContSubSchema.getPayMode().equals("5")||(mLCContSubSchema.getPayMode().equals("4") && "06".equals(mMEDICAL_INFO.getFAIL_REASON()))){
								MedicalKF = false;
								String sqlgj = "select code,code1 from ldcode1 where codetype = 'SHYBgjzh'";
								SSRS tSSRSGJ = new ExeSQL().execSQL(sqlgj);
								if(tSSRSGJ.getMaxRow()>0){
									tInsBankcode= tSSRSGJ.GetText(1, 1);
									tInsBankaccno = tSSRSGJ.GetText(1, 2);
								}else{
									tInsBankcode = "";
									tInsBankaccno = "";
								}
								//更新暂收表的险种号以便于结算
								sqlTempFee = "update ljtempfee set riskcode = '"+mRiskCode+"' , enteraccdate = current date,confdate = current date,confmakedate = current date,confmaketime = current time where tempfeeno = '"+mLJTempFeeSchema.getTempFeeNo()+"'";
								
								sqlTempfeclass = "update LJTempFeeClass set insbankcode = '"+tInsBankcode+"', insbankaccno = '"+tInsBankaccno+"', PayMode='11',BankCode='"
										+ mLCContSchema.getBankCode()
										+ "' ,ModifyDate= current date,ModifyTime=current time , enteraccdate = current date,confdate = current date,confmakedate = current date,confmaketime = current time where TempFeeNo='"
										+ mLJTempFeeclassSchema.getTempFeeNo() + "' and paymode='"
										+ mLJTempFeeclassSchema.getPayMode() + "'";
								
								sqlCont = "update lccont set paymode='11',expaymode='11' where prtno ='"
										+ mLCContSchema.getPrtNo() + "'";
								
								sqlLcpol = "update lcpol set paymode='11',expaymode='11' where prtno ='"
										+ mLCContSchema.getPrtNo() + "'";
								
								tmap.put(sqlTempFee, "UPDATE");
								tmap.put(sqlCont, "UPDATE");
								tmap.put(sqlLcpol, "UPDATE");
								tmap.put(sqlTempfeclass, "UPDATE");
								errLog("等待核心支付宝支付0", "1");
							} else {
								errLog("医保扣款失败终止投保(" + mMEDICAL_INFO.getFAIL_REASON() + ")0", "1");
								policyCancle();
							}
						}
					}
				}
				// 提交数据
				VData mInputData = new VData();
				mInputData.clear();
				mInputData.add(tmap);
				if (!tPubSubmit.submitData(mInputData, "")) {
					System.out.println("保存数据出错");
					CError.buildErr(this, "数据保存失败");
					errLog("系统接收失败", "0");
					return false;
				}
				// 存日志
				SetPrtNo(mPrtNo);
				//调用签单处理逻辑
				if(!signPol(mPrtNo)){
					//签单失败不阻断，正常返回G03处理结果
					System.out.println("G03首期处理了成功，但是调用签单失败，请查找问题，等待签单批处理或者手动签单");
				}
			} else {
				// 处理续保逻辑
				//先查询出续保次数
//				String tRnewCount = "select renewcount from lcpol where prtno = '"+mPrtNo+"' order by makedate desc fetch first 1 rowsonly with ur";
				String tSql= "SELECT renewcount FROM (SELECT B.*, ROWNUMBER() OVER() AS TN FROM (SELECT renewcount FROM lcpol where prtno = '"+mPrtNo+"') AS B) AS A WHERE 1=1 and A.TN = 2 with ur";
				SSRS mSSRS = new ExeSQL().execSQL(tSql);
				if(mSSRS.getMaxRow()>0){
					tRnewCount = mSSRS.GetText(1, 1);
				}
				
				MMap tmap = new MMap();
				// 医保扣款失败
				if ("0".equals(mMEDICAL_INFO.getMEDICAL_IF_SUCESS())) {
					// 扣款失败原因为08-数据异常时，为医保系统异常情况，需人工干预，要求公司这种情况下支持平台重新提交
					if ("08".equals(mMEDICAL_INFO.getFAIL_REASON())) {
						errLog1("扣款失败，失败原因为" + mMEDICAL_INFO.getFAIL_REASON() + "数据异常1", "0", "08", tRnewCount);
					}
					// 1优先医保卡支付，银行卡支付辅助 或者3仅银行卡支付
					else if ((mLCContSubSchema.getRenemalPayMethod().equals("1") && "06".equals(mMEDICAL_INFO.getFAIL_REASON()))
							||(mLCContSubSchema.getRenemalPayMethod().equals("1") && "01".equals(mMEDICAL_INFO.getFAIL_REASON()))) {
						String sqlLjspay = "update LjsPay set bankcode = '"+mBankCode+"' where otherno ='"+mContNo+"'";
						
						String sqlLCCont = "update lccont set paymode='4',expaymode='4' where prtno ='"
								+ mLCContSchema.getPrtNo() + "' and signdate is null";
						
						String sqlLCPol = "update lcpol set paymode='4',expaymode='4' where prtno ='"
								+ mLCContSchema.getPrtNo() + "' and signdate is null";
						
						tmap.put(sqlLjspay, "UPDATE");
						tmap.put(sqlLCCont, "UPDATE");
						tmap.put(sqlLCPol, "UPDATE");
						errLog("等待核心银行卡支付1", "1");
						//医保扣款失败转支付宝的
					} else if((mLCContSubSchema.getRenemalPayMethod().equals("4") && "06".equals(mMEDICAL_INFO.getFAIL_REASON()))
							||(mLCContSubSchema.getRenemalPayMethod().equals("4") && "01".equals(mMEDICAL_INFO.getFAIL_REASON()))) {
						MedicalKF = false;
						if(!infoSave()){
							System.out.println("续保医保卡扣款暂收表数据插入失败！");
							errLog("续保医保卡扣款暂收表数据插入失败！","0");
							return false;
						}
						// 仅医保卡支付 核心什么也不做
					} else {
						errLog("医保扣款失败终止投保(" + mMEDICAL_INFO.getFAIL_REASON() + ")1", "1");
						policyCancle();
					}
					// 医保扣款返回成功，但是只医保卡扣款情况
				} else if ("1".equals(mMEDICAL_INFO.getMEDICAL_IF_SUCESS())) {
					if ((!mMEDICAL_INFO.getDEDUCT_MONEY().equals("") && !mMEDICAL_INFO.getDEDUCT_MONEY().equals(null)
							&& Double.parseDouble(mMEDICAL_INFO.getACTUAL_DEDUCT_PREMIUM()) != 0)&&(mLCContSubSchema.getRenemalPayMethod().equals("1")||mLCContSubSchema.getRenemalPayMethod().equals("2"))) {
						//医保卡扣款核心什么也不做
						if(!infoSave()){
							System.out.println("续保医保卡扣款暂收表数据插入失败！");
							errLog("续保医保卡扣款暂收表数据插入失败！","0");
							return false;
						}
						errLog("医保扣款已经成功1", "1");
					} else {
						if ("08".equals(mMEDICAL_INFO.getFAIL_REASON())) {
							errLog1("扣款失败，失败原因为" + mMEDICAL_INFO.getFAIL_REASON() + "数据异常1", "0", "08", tRnewCount);
						}
						// 银行卡扣款的
						else if ((mLCContSubSchema.getRenemalPayMethod().equals("1") && "06".equals(mMEDICAL_INFO.getFAIL_REASON()))|| mLCContSubSchema.getRenemalPayMethod().equals("3")) {
							String sqlLjspay = "update LjsPay set bankcode = '"+mBankCode+"' where otherno ='"+mContNo+"'";
							
							String sqlLCCont = "update lccont set paymode='4',expaymode='4' where prtno ='"
									+ mLCContSchema.getPrtNo() + "' and signdate is null";
							
							String sqlLCPol = "update lcpol set paymode='4',expaymode='4' where prtno ='"
									+ mLCContSchema.getPrtNo() + "' and signdate is null";
							
							tmap.put(sqlLjspay, "UPDATE");
							tmap.put(sqlLCCont, "UPDATE");
							tmap.put(sqlLCPol, "UPDATE");
							errLog("等待核心银行卡支付1", "1");
							//医保验证成功但是扣款失败，转支付宝
						} else if ((mLCContSubSchema.getRenemalPayMethod().equals("4") && "06".equals(mMEDICAL_INFO.getFAIL_REASON()))|| mLCContSubSchema.getRenemalPayMethod().equals("5")){
							MedicalKF = false;
							//待定
							
						} else {
							errLog("医保扣款失败终止投保(" + mMEDICAL_INFO.getFAIL_REASON() + ")1", "1");
							policyCancle();
						}
					}
				}
				// 提交数据
				VData mInputData = new VData();
				mInputData.clear();
				mInputData.add(tmap);
				if (!tPubSubmit.submitData(mInputData, "")) {
					System.out.println("保存数据出错");
					CError.buildErr(this, "数据保存失败");
					errLog("系统接收失败", "0");
					return false;
				}
				// 存日志
				SetPrtNo(mPrtNo);
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	public final String StringPattern(String date, String oldPattern, String newPattern) {
		if (date == null || oldPattern == null || newPattern == null)
			return "";
		SimpleDateFormat sdf1 = new SimpleDateFormat(oldPattern); // 实例化模板对象
		SimpleDateFormat sdf2 = new SimpleDateFormat(newPattern); // 实例化模板对象
		Date d = null;
		try {
			d = sdf1.parse(date); // 将给定的字符串中的日期提取出来
		} catch (Exception e) { // 如果提供的字符串格式有错误，则进行异常处理
			e.printStackTrace(); // 打印异常信息
		}
		return sdf2.format(d);
	}

	public void saveMedicalInfo() {
		db = new YBK_G03_LIS_ResponseInfoDB();
		db.setPolicyFormerNo(mMEDICAL_INFO.getPOLICY_FORMER_NO());
		db.setYBKKSequenceNo(mMEDICAL_INFO.getYB_KK_SEQUENCE_NO());
		db.setMedicalIfSucess(mMEDICAL_INFO.getMEDICAL_IF_SUCESS());
		db.setDecuctMoney(mMEDICAL_INFO.getDEDUCT_MONEY());
		db.setFailReason(mMEDICAL_INFO.getFAIL_REASON());
		db.setIfSpecialPerson(mMEDICAL_INFO.getIF_SPECIAL_PERSON());
		db.setActualDecuctPremium(mMEDICAL_INFO.getACTUAL_DEDUCT_PREMIUM());
		db.setOperator("sys");
		db.setMakeDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		db.setMakeTime(new SimpleDateFormat("HHmmss").format(new Date()));
		db.setModifyDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		db.setModifyTime(new SimpleDateFormat("HHmmss").format(new Date()));
		db.insert();
	}
	
	//续保医保卡扣款根据应收生成暂收数据,支付方式为：1,2,3的
	public boolean infoSave(){
		String cxsql = "select getnoticeno,accname,sumduepaymoney,managecom,agentcode,agentgroup,bankaccno from ljspay where otherno = '"+mContNo+"' with ur";
		SSRS tSSRS = new ExeSQL().execSQL(cxsql);
		String mTempFeeNo = tSSRS.GetText(1, 1);
		String mAppntName = tSSRS.GetText(1, 2);
		String mSumDuePayMoney = tSSRS.GetText(1, 3);	
		String mManageCom = tSSRS.GetText(1, 4);
		String mAgentCode = tSSRS.GetText(1, 5);
		String mAgentGroup = tSSRS.GetText(1, 6);
		String mBankAccNo = tSSRS.GetText(1, 7);
		String tLimit = PubFun.getNoLimit(mManageCom);
		String mSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
		//医保真正的扣款时间
		String dateString = StringPattern(mMEDICAL_INFO.getDEDUCT_MONEY(), "yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss");
		String mDate = dateString.substring(0, 10);
		String mTime = dateString.substring(11, 19);
		
		LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
		LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
		//更新LJTempFee表
		tLJTempFeeDB.setTempFeeNo(mTempFeeNo);
		tLJTempFeeDB.setTempFeeType("2");
		tLJTempFeeDB.setRiskCode(mRiskCode);
		tLJTempFeeDB.setPayIntv(0);
		tLJTempFeeDB.setOtherNo(mContNo);
		tLJTempFeeDB.setOtherNoType("2");
		tLJTempFeeDB.setPayMoney(mSumDuePayMoney);
		tLJTempFeeDB.setPayDate(PubFun.getCurrentDate());
		tLJTempFeeDB.setManageCom(mManageCom);
		tLJTempFeeDB.setPolicyCom(mManageCom);
		tLJTempFeeDB.setAPPntName(mAppntName);
		tLJTempFeeDB.setAgentGroup(mAgentGroup);
		tLJTempFeeDB.setAgentCode(mAgentCode);
		tLJTempFeeDB.setConfFlag("0");
		tLJTempFeeDB.setSerialNo(mSerialNo);
		tLJTempFeeDB.setOperator("YBK");
		tLJTempFeeDB.setMakeTime(PubFun.getCurrentTime());
		tLJTempFeeDB.setMakeDate(PubFun.getCurrentDate());
		tLJTempFeeDB.setModifyDate(PubFun.getCurrentDate());
		tLJTempFeeDB.setModifyTime(PubFun.getCurrentTime());
		tLJTempFeeDB.setEnterAccDate(mDate);
		tLJTempFeeDB.setConfMakeDate(mDate);
		tLJTempFeeDB.setConfMakeTime(mTime);
		tLJTempFeeDB.insert();
		//更新LJTempFeeClass
		tLJTempFeeClassDB.setEnterAccDate(mDate);
		tLJTempFeeClassDB.setConfMakeDate(mDate);
		tLJTempFeeClassDB.setConfMakeTime(mTime);
		tLJTempFeeClassDB.setTempFeeNo(mTempFeeNo);
		if(("4".equals(mLCContSubSchema.getRenemalPayMethod()) && (MedicalKF = false)) 
				|| "5".equals(mLCContSubSchema.getRenemalPayMethod())){
			tLJTempFeeClassDB.setPayMode("11");
		}else{
			tLJTempFeeClassDB.setPayMode("8");
		}
		tLJTempFeeClassDB.setPayMoney(mSumDuePayMoney);
		tLJTempFeeClassDB.setAppntName(mAppntName);
		tLJTempFeeClassDB.setPayDate(PubFun.getCurrentDate());
		tLJTempFeeClassDB.setConfFlag("0");
		tLJTempFeeClassDB.setSerialNo(mSerialNo);
		tLJTempFeeClassDB.setManageCom(mManageCom);
		tLJTempFeeClassDB.setPolicyCom(mManageCom);
		tLJTempFeeClassDB.setBankCode(mBankCode);
		tLJTempFeeClassDB.setBankAccNo(mBankAccNo);
		tLJTempFeeClassDB.setAccName(mAppntName);
		tLJTempFeeClassDB.setOperator("YBK");
		tLJTempFeeClassDB.setMakeDate(PubFun.getCurrentDate());
		tLJTempFeeClassDB.setMakeTime(PubFun.getCurrentTime());
		tLJTempFeeClassDB.setModifyDate(PubFun.getCurrentDate());
		tLJTempFeeClassDB.setModifyTime(PubFun.getCurrentTime());
		tLJTempFeeClassDB.insert();
		return true;
	}
	
	//撤单功能
	public boolean policyCancle() {
		String SQL = "select lc.prtno,lc.cardflag,lc.managecom " + " from lccont lc,lcpol lp "
				+ " where lc.prtno=lp.prtno " + " and lc.appflag not in ('1') " + " and lc.prtno='" + mPrtNo + "'"
				+ " and not exists(select 1 from LCApplyRecallPol where prtno=lc.prtno) " + " with ur";
		GlobalInput tG = new GlobalInput();
		LCApplyRecallPolSchema tLCApplyRecallPolSchema = new LCApplyRecallPolSchema();
		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		String tPrtNo;
		String tCardFlag;
		String tErrorInfo;
		String tMagCom;
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(SQL);

		if (tSSRS != null && tSSRS.MaxRow > 0) {
			tPrtNo = tSSRS.GetText(1, 1);
			tCardFlag = tSSRS.GetText(1, 2);
			tMagCom = tSSRS.GetText(1, 3);
			tG.ManageCom = tMagCom;
			tG.Operator = "YBK";
			tTransferData.setNameAndValue("CardFlag", tCardFlag);
			// 补充附加险表
			tLCApplyRecallPolSchema.setRemark("实时撤单原因：医保扣款失败终止投保(" + mMEDICAL_INFO.getFAIL_REASON() + "),医保验证:"
					+ mMEDICAL_INFO.getMEDICAL_IF_SUCESS());
			tLCApplyRecallPolSchema.setPrtNo(tPrtNo);
			tLCApplyRecallPolSchema.setApplyType("3");
			tLCApplyRecallPolSchema.setManageCom(tMagCom);
			tLCApplyRecallPolSchema.setOperator("YBK");
			// 准备传输数据 VData
			tVData.add(tLCApplyRecallPolSchema);
			tVData.add(tTransferData);
			tVData.add(tG);
			try {
				ApplyRecallPolUI applyRecallPolUI = new ApplyRecallPolUI();
				if (applyRecallPolUI.submitData(tVData, "")) {
					System.out.println("实时撤单成功~~~");
				} else {
					this.mErrors.copyAllErrors(applyRecallPolUI.mErrors);
					System.out.println("实时撤单失败：" + mErrors.getFirstError() + "---印刷号---" + tPrtNo);
				}
			} catch (Exception e) {
				System.out.println("G03实时撤单异常");
				e.printStackTrace();
			}
			return true;
		} else {
			System.out.println("未查询到可以撤单的保单!!!");
			return false;
		}
	}
	
	public boolean signPol(String mPrtNo){
		String signSQL = "select distinct lwmission.MissionProp1, "
                +" lwmission.MissionProp2, "
                +" lccont.appntname, "
                +" lwmission.MissionProp6, "
                +" lccont.managecom, "
                +" LWMission.MissionID, "
                +" LWMission.SubMissionID "
                +" from lwmission, lccont, ljtempfee "
                +" where '1548749472000' = '1548749472000' "
                +" and 2 = 2 "
                +" and LWMission.ProcessID = '0000000003' "
                +" and lwmission.MissionProp1 = lccont.proposalcontno "
                +" and lwmission.MissionProp2 = ljtempfee.otherno "
                +" and ljtempfee.othernotype = '4' "
                +" and ljtempfee.confflag = '0' "
                +" and (ljtempfee.EnterAccDate is not null) "
                +" and LWMission.ActivityID = '0000001150' "
                +" and lwmission.MissionProp2 = '"+mPrtNo+"' "
                +" and lccont.managecom like '86%' fetch first 3000 rows only with ur";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(signSQL);
		if(null == tSSRS || tSSRS.getMaxRow() < 1){
			System.out.println("保单不处于签单状态！");
			return false;
		}
		mGlobalInput.ManageCom = "86310000";
		mGlobalInput.ComCode = "86310000";
		mGlobalInput.Operator = "YBK";
		TransferData tTransferData = new TransferData();
		LCContSet tLCContSet = new LCContSet();
		LCContSchema tLCContSchema = new LCContSchema ();
		tLCContSchema.setContNo( tSSRS.GetText(1, 1) );
	 	tLCContSchema.setPrtNo( tSSRS.GetText(1, 2) );
		tLCContSet.add( tLCContSchema );
		tTransferData.setNameAndValue("MissionID",tSSRS.GetText(1, 6) );		   		
 		tTransferData.setNameAndValue("SubMissionID",tSSRS.GetText(1, 7) );
 		// 准备传输数据 VData
 		VData tVData = new VData();
 		tVData.add( mGlobalInput );
 		tVData.add( tLCContSet );
 		tVData.add( tTransferData );
		TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
		if (tTbWorkFlowUI.submitData(tVData, "0000001150") == false) {
			System.out.println(mPrtNo + "医保卡投保单签单失败");
			FlagStr = "Fail";
		}

		int n = tTbWorkFlowUI.mErrors.getErrorCount();
		System.out.println("flag===" + FlagStr);
		if (n == 0 && FlagStr.equals("Succ")) {
			System.out.println("医保卡签单成功!");
			FlagStr = "Succ";
		} else {
			String strErr = "";
			for (int t = 0; t < n; t++) {
				strErr += (t + 1) + ": " + tTbWorkFlowUI.mErrors.getError(t).errorMessage + "; ";
			}
			if (FlagStr.equals("Succ")) {
				System.out.println("部分投保单签单失败，原因是: " + strErr);
				FlagStr = "Fail";
			} else {
				System.out.println("可能有如下原因:" + strErr);
			}
			return false;
		}
		
		return true;
	}
	
	public static void main(String[] args){
		YbkFeeTB ybk = new YbkFeeTB();
		ybk.signPol("1620170512111");
	}
}
