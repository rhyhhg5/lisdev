package com.sinosoft.httpclientybk.deal;
/*
 * 
 * 2.4计算退保金接口
 * yukun 2017-01-13新增
 * 
 * */
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G04.ESTIMATE_INFO;
import com.sinosoft.httpclientybk.dto.G04.SUCCESS_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.bq.YbkRefundTFJS;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YbkCulateTBJTB extends ABusLogic {
	
	public String BatchNo = "";// 批次号
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	
	private String USER;
	private String PASSWORD;
	private String REQUEST_TYPE;

	/* 接收报文 */
	private ESTIMATE_INFO cEstimate;
	
	/* 返回报文 */
	private SUCCESS_INFO mSUCCESS_INFO  = new SUCCESS_INFO();
	
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	private String mPolicyFormerNo = ""; // 投保流水号
	private String mEffectiveDate = "";	//保单起期
	private String mSurrenderDate = "";	//退保申请日期
	private String customerGetPolDate = "";//回执回销日期
	
	/*印刷号*/
	private String prtNo="";
		
	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return false;
	}
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("======开始处理退保金过程======");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		MsgType = tMsgHead.getREQUEST_TYPE();

		mGlobalInput.Operator = "YBK";
		mGlobalInput.ComCode = "86";
		mGlobalInput.ManageCom = "86";
		
		USER = tMsgHead.getUSER();
		PASSWORD = tMsgHead.getPASSWORD();
		REQUEST_TYPE = tMsgHead.getREQUEST_TYPE();
		System.out.println("USER=="+USER);
		System.out.println("PASSWORD=="+PASSWORD);
		System.out.println("REQUEST_TYPE=="+REQUEST_TYPE);
		
		try {
			List tManualList = cMsgInfos.getBodyByFlag("BODY");// 个人理解
			if (tManualList == null || tManualList.size() != 1) {
				errLog("获取xml信息失败", "0");
				return false;
			}
			
			/* 获取xml信息 */
			cEstimate = (ESTIMATE_INFO) tManualList.get(0); // 获取保单信息，只有一个保单
			
			mPolicyFormerNo = cEstimate.getPOLICY_SEQUENCE_NO();
			if(mPolicyFormerNo == null || mPolicyFormerNo.equals("")){
				errLog("获取保单号失败!", "0");
				return false;
			}
			
			mEffectiveDate = cEstimate.getEFFECTIVE_DATE();
			if(mEffectiveDate == null || mEffectiveDate.equals("")){
				errLog("获取保单起期失败!", "0");
				return false;
			}
			
			mSurrenderDate=cEstimate.getSURRENDER_DATE();
			if(mSurrenderDate == null || mSurrenderDate.equals("")){
				errLog("获取退保申请日期失败!", "0");
				return false;
			}
			
			System.out.println("mPolicyFormerNo=="+mPolicyFormerNo);
			System.out.println("mEffectiveDate=="+mEffectiveDate);
			System.out.println("mSurrenderDate=="+mSurrenderDate);		
			
			/* 日期格式化:YYYYMMDD转换为YYYY-MM-DD */
			try{
				SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
				Date date = (Date) df.parse(mSurrenderDate);
				String nowDate = df1.format(date);
				System.out.println("nowDate="+nowDate);
			}catch (Exception ex) {
				errLog("日期格式转换错误!", "0");
				return false;
				}
			
			/* 根据保单编号转换为contno */
//			String sql="select contno,prtno from lccont where prtno in (select prtno from YBK_N01_LIS_ResponseInfo where PolicySequenceNo = '"+mPolicyFormerNo+"' and responsecode = '1' ) with ur";
			String sql = "select lc.contno,lc.prtno,lc.prem "
					+" from lcpol lc "
					+" where lc.prtno in (select prtno from YBK_N01_LIS_ResponseInfo where PolicySequenceNo = '"+mPolicyFormerNo+"' and responsecode = '1' and renewcount = lc.renewcount) "
					+" and lc.renewcount = '0' "
					+" with ur ";
			SSRS tSSRS=new SSRS();
	    	ExeSQL tExeSQL=new ExeSQL();
	    	tSSRS=tExeSQL.execSQL(sql);
	    	if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
	    		errLog("ContNo获取失败!", "0");
				return false;
	    	}
	    	String tContNo = tSSRS.GetText(1, 1);
	    	System.out.println("tContNo="+tContNo);
	    	//根据prtno存放日志
	    	String prtNo = tSSRS.GetText(1, 2);
	    	System.out.println("prtNo="+prtNo);
	    	SetPrtNo(prtNo);
	    	String prem = tSSRS.GetText(1, 3);
	    	
	    	//判断退保申请日期在不在犹豫期内，在的话直接返回保费，不在的话走以下计算逻辑
	    	//判断是否有回执回销日期，有就取，没有就返回签单日期
	    	String Datesql=" select (case when customgetpoldate is not null "
					+" then customgetpoldate "
					+" else signdate "
					+" end) as date  from lccont where prtno='"+prtNo+"' with ur";
			SSRS tSSRS1 = new ExeSQL().execSQL(Datesql);
			if(tSSRS1.getMaxRow()<1 || null == tSSRS1){
				errLog("保单尚未回执回销，无法计算退保金！", "0");
				return false;
			}else{
				customerGetPolDate = tSSRS1.GetText(1,1);
			}
			//签单日期次日零时
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
			Date date = (Date) df.parse(customerGetPolDate);//回执回销日期	
			Calendar cal = Calendar.getInstance();
			//计算起始日期
			String stratDate = df1.format(date);
			System.out.println("犹豫期退保起始日期date:"+stratDate);
			//计算终止日期
			cal.setTime(date);
			cal.add(Calendar.DATE, 10);
			Date date1=cal.getTime();//犹豫期失效日期
			String endDate = df1.format(date1);
			System.out.println("犹豫期退保失效日期date1:"+endDate);	
			//退保申请日期转换为核心日期格式
			SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMdd");
			Date date2 = (Date) df2.parse(mSurrenderDate);//退保申请日期
			String tbDate = df.format(date2);
			System.out.println("退保申请日期date2:"+tbDate);
			if ((date2.getTime() >= date.getTime()) && (date2.getTime() <= date1.getTime())){
				//犹豫期直接返回保费
				mSUCCESS_INFO.setESTIMATE_CANCEL_PREMIUM(prem);
	            getXmlResult();
			}else{
				/* 计算退保金 */
				try {
					YbkRefundTFJS mYbkRefundTFJS = new YbkRefundTFJS();
					double pSumPrem = mYbkRefundTFJS.getZTmoney(tContNo, mSurrenderDate);
					System.out.println("退保金pSumPrem=" + pSumPrem);
					String TBPrem = pSumPrem + "";
					mSUCCESS_INFO.setESTIMATE_CANCEL_PREMIUM(TBPrem);
					getXmlResult();
				} catch (Exception ex) {
					errLog("退保金计算错误!", "0");
					return false;
				}
			}
		}catch (Exception ex) {
				errLog(ex.toString(), "E");
				return false;
				}
		errLog("处理成功", "1");
		return true;
	}
	
	/**
	 * 返回信息报文
	*/
	public void getXmlResult() {

		// 返回数据节点
		putResult("SUCCESS_INFO", mSUCCESS_INFO);

	}
}
