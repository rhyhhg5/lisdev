package com.sinosoft.httpclientybk.deal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G06.MEDICAL_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class YBKRefundTB extends ABusLogic {
	
	/** 报文头信息 */
	private String USER;
	private String PASSWORD;
	private String REQUEST_TYPE;
	/** 报文体信息  */
	private  MEDICAL_INFO tMEDICAL_INFO;
	
	public String mError = "";// 处理过程中的错误信息
	PubSubmit tPubSubmit = new PubSubmit();
	
	/** 批次号 */
	private String tEdorno;
	
	private String tActuGetNo;
	
	/** 银行编码 */
	private String tBankcode;
	
	/** 插入表数据 */
	String Operator = "YBK";
	String MakeTime = PubFun.getCurrentTime();
	String MakeDate = PubFun.getCurrentDate();
	String ModifyDate = "";
	String ModifyTime = "";

	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理G06医保卡退费确认过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		USER = tMsgHead.getUSER();
		PASSWORD = tMsgHead.getPASSWORD();
		REQUEST_TYPE = tMsgHead.getREQUEST_TYPE();
		
		System.out.println("USER = " + USER);
		System.out.println("PASSWORD = " + PASSWORD);
		System.out.println("REQUEST_TYPE = " + REQUEST_TYPE);
		
		try {
			List MEDICAL_INFO_List = cMsgInfos.getBodyByFlag("BODY");
			if (MEDICAL_INFO_List == null || MEDICAL_INFO_List.size() != 1) {
				System.out.println("获取核保信息失败");
				errLog("获取核保信息失败", "0");
				return false;
			}
			
			tMEDICAL_INFO = (MEDICAL_INFO) MEDICAL_INFO_List.get(0);
			
			System.out.println("=============测试专用开始=================");
			
			//TODO 投保流水号是客户号需要查询核心印刷号
			String PrtNoSql = "select prtno " +
			"from YBK_N01_LIS_ResponseInfo " +
			"where PolicySequenceNo = '" + tMEDICAL_INFO.getPOLICY_SEQUENCE_NO() +
			"' and responsecode = '1'";
			SSRS tPrtNoSSRS = new ExeSQL().execSQL(PrtNoSql);
			
			if (tPrtNoSSRS == null || tPrtNoSSRS.getMaxRow() <= 0) {
				System.out.println("印刷号获取失败！");
				mError = "印刷号获取失败！";
				return false;
			} else {
				mPrtNo = tPrtNoSSRS.GetText(1, 1);
				System.out.println("======mPrtNo========" + mPrtNo);
			}
			
			//TODO 根据保单编码查edorno(保全业务号)
			String sqlEdorno = "select edorno from lpcont where prtno = '" + mPrtNo + "' and edortype in('CT','WT') order by makedate fetch first 1 rows only with ur";
			SSRS tEdornoSSRS = new ExeSQL().execSQL(sqlEdorno);
			
			if (tEdornoSSRS == null || tEdornoSSRS.getMaxRow() <= 0) {
				System.out.println("批次号获取失败！");
				mError = "批次号获取失败！";
				return false;
			} else {
				tEdorno = tEdornoSSRS.GetText(1, 1);
				System.out.println("======tEdorno========" + tEdorno);
			}
			
			//获取tActuGetNo
			String sqltemp = "select ActuGetNo from ljaget where otherno='" + tEdorno + "' and enteraccdate is null";
			SSRS ActuGetNoSSRS = new ExeSQL().execSQL(sqltemp);
			
			if (ActuGetNoSSRS==null || tEdornoSSRS.getMaxRow() <= 0) {
				System.out.println("ActuGetNo获取失败!");
				errLog("ActuGetNo获取失败!","0");
				return false;
			} else {
				tActuGetNo = ActuGetNoSSRS.GetText(1, 1);
				System.out.println("======tActuGetNo========" + tActuGetNo);
			}
			
			//更新实收分类表
			String sqlLjaget = "";
			String sqlLjfiget1 = "";
			String sqlljagetendorse1 = "";
			MMap tmap = new MMap();
			
			
			//判断医保卡退款是否成功
			
			System.out.println("===============判断医保卡扣款是否成功==============");
			if("0".equals(tMEDICAL_INFO.getREFUND_IF_SUCCESS()) && tMEDICAL_INFO.getYB_TK_SEQUENCE_NO().length() <= 0){
				if("05".equals(tMEDICAL_INFO.getREFUND_FAIL_REASON())){
					System.out.println("系统处理异常,请重新提交");
					errLog("系统处理异常,请重新提交","0");
					return false;
				}else{
					//医保扣款失败,更新ljaget表把PayMode改成'4'bankcode 应该至上
					//获取银行编码
					String bankcodeSQL = "select bankcode from lbcont where prtno = '" + mPrtNo + "'";
					SSRS tBankcodeSSRS = new ExeSQL().execSQL(bankcodeSQL);
					
					if (tBankcodeSSRS == null || tBankcodeSSRS.getMaxRow() <= 0) {
						System.out.println("银行编码获取失败！");
						mError = "银行编码获取失败！";
						return false;
					} else {
						tBankcode = tBankcodeSSRS.GetText(1, 1);
					}
					sqlLjaget = "update ljaget set PayMode='4',bankcode = '" + tBankcode + "',ModifyDate= current date,ModifyTime=current time where ActuGetNo = '"+ tActuGetNo +"'";
					tmap.put(sqlLjaget, "UPDATE");
					System.out.println("医保卡退费失败,等待核心银行卡支付");
					errLog("医保卡退费失败,等待核心银行卡支付","1");
				}
				
				
			} else if ("1".equals(tMEDICAL_INFO.getREFUND_IF_SUCCESS()) && tMEDICAL_INFO.getYB_TK_SEQUENCE_NO().length() > 0){
				//医保扣款已经成功
				//1,更新实收表
				//获取当前时间
				ModifyDate = PubFun.getCurrentDate();
				ModifyTime = PubFun.getCurrentTime();
				//TODO 时间是yyyymmddhhssmm改成核心yyyy-mm-dd   setPolicyCom  GetMoney ljaget的钱  ShouldDate
				String dateString = StringPattern(tMEDICAL_INFO.getREFUND_TIME(),"yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss");
				String date=dateString.substring(0, 10);
				String time=dateString.substring(11, 19);
				
				String bankcodeSQL = "select bankcode from lbcont where prtno = '" + mPrtNo + "'";
				SSRS tBankcodeSSRS = new ExeSQL().execSQL(bankcodeSQL);
				
				//获取银行编码
				if (tBankcodeSSRS == null || tBankcodeSSRS.getMaxRow() <= 0) {
					System.out.println("银行编码获取失败！");
					mError = "银行编码获取失败！";
					return false;
				} else {
					tBankcode = tBankcodeSSRS.GetText(1, 1);
				}
				
				sqlLjaget = "update ljaget " 
					+"set PayMode='8',EnterAccDate ='"+ date 
					+"' ,ConfDate ='"+ date 
					+ "',bankcode = '"+ tBankcode + "'"
					//+"' ,ConfmakeDate ='"+ date 
					+",ModifyDate= current date,ModifyTime=current time where Actugetno='" + tActuGetNo + "'";
				
				//2,向ljfiget表插入数据
				String OtherNoSql = "select OtherNo,OtherNoType,AgentType," +
						"AgentCom,AgentGroup,AgentCode,drawer,drawerid,shoulddate" +
						",bankcode,salechnl,accname,bankaccno,managecom " +
						"from ljaget " +
						"where ActuGetNo = '" + tActuGetNo + "'"; 
				SSRS result =  new ExeSQL().execSQL(OtherNoSql);

				sqlLjfiget1 = "insert into ljfiget" +
						"(ActuGetNo,PayMode,PolicyCom,OtherNo,OtherNoType,GetMoney,EnterAccDate," +
						"Operator,MakeTime,MakeDate,ModifyDate,ModifyTime,AgentType,AgentCom,AgentGroup," +
						"AgentCode,drawer,drawerid,shoulddate,bankcode,confmaketime,confmakedate,confdate," +
						"salechnl,bankaccno,accname,managecom) " +
						"values ('" + tActuGetNo + "','8','" + result.GetText(1, 14) + "','" + result.GetText(1, 1) + "','" + result.GetText(1, 2) 
						+ "','" + tMEDICAL_INFO.getACTUAL_CANCEL_PREMIUM() + "','" + date 
						+ "','" + Operator + "','" + MakeTime + "','" + MakeDate + "','" + ModifyDate 
						+ "','" + ModifyTime + "','" + result.GetText(1, 3) + "','" + result.GetText(1, 4) + "','" + result.GetText(1, 5)
						+ "','" + result.GetText(1, 6) + "','" + result.GetText(1, 7) + "','" + result.GetText(1, 8) + "','" + date + "','" + result.GetText(1, 9)
						+ "','" + PubFun.getCurrentTime() + "','" + date + "','" + date + "','" + result.GetText(1, 11) + "','" + result.GetText(1, 13) + "','" + result.GetText(1, 12) + "','" + result.GetText(1, 14) + "')";
				//3,更新LJAGetEndorse表数据
				sqlljagetendorse1 = "update LJAGetEndorse set GetConfirmDate = '" + date + "',EnterAccDate = '" + date + "',ModifyDate = current date,ModifyTime = current time where Actugetno='" + tActuGetNo + "'";
				
				tmap.put(sqlLjaget, "UPDATE");
				tmap.put(sqlLjfiget1, "INSERT");
				tmap.put(sqlljagetendorse1, "UPDATE");
				System.out.println("医保退款已经成功");
				errLog("医保退款已经成功","1");
			}
				
				// 提交数据
				
	            VData mInputData = new VData();
				mInputData.clear();
				mInputData.add(tmap);
				if(!tPubSubmit.submitData(mInputData, "")){
					System.out.println("保存数据出错");
					CError.buildErr(this, "数据保存失败");
					System.out.println("系统接收失败");
					errLog("系统接收失败","0");
		            return false;
				}
				
				SetPrtNo(mPrtNo);
		} catch (Exception ex) {
			return false;
		}
		return true;
	
	}
	
	 public final String StringPattern(String date, String oldPattern, String newPattern) {   
	        if (date == null || oldPattern == null || newPattern == null)   
	            return "";   
	        SimpleDateFormat sdf1 = new SimpleDateFormat(oldPattern) ;        // 实例化模板对象    
	        SimpleDateFormat sdf2 = new SimpleDateFormat(newPattern) ;        // 实例化模板对象    
	        Date d = null ;    
	        try{    
	            d = sdf1.parse(date) ;   // 将给定的字符串中的日期提取出来    
	        }catch(Exception e){            // 如果提供的字符串格式有错误，则进行异常处理    
	            e.printStackTrace() ;       // 打印异常信息    
	        }    
	        return sdf2.format(d);  
	    }

	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}
}
