package com.sinosoft.httpclientybk.deal;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G01.INSURE_INFO;
import com.sinosoft.httpclientybk.dto.G01.SUCCESS_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.tb.BPO;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.TbWorkFlowUI;


public class YbkCheckTB extends ABusLogic{

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	private String mBPOMissionState = "";

	private String mPrtNo = "";
	
	private String mCustomerNo = "";

	private static final String STATIC_ActivityID = "0000001001";

	private String mMissionid = "";

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	String cRiskWrapCode = "";// 套餐编码
	
	private INSURE_INFO mINSURE_INFO ;
	
	private PubSubmit tPubSubmit =  new PubSubmit();
	
	private SUCCESS_INFO mSUCCESS_INFO  = new SUCCESS_INFO();
	
	private LCContSchema mLCContSchema =  new LCContSchema();
	
	private Document mDocument ;
	
	private String POLICY_FORMER_NO;
	

	
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理YBK出单标准平台新单复核过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		MsgType = tMsgHead.getREQUEST_TYPE();

		mGlobalInput.Operator = "YBK";
		mGlobalInput.ComCode = "86";
		mGlobalInput.ManageCom = "86";
		//mDocument = cInXmlDoc;


		try {
			List INSURE_INFO_List = cMsgInfos.getBodyByFlag("BODY");
			if (INSURE_INFO_List == null || INSURE_INFO_List.size() != 1) {
				errLog("获取医保自核报文信息失败","0");
				return false;
			}
			//mList
			mINSURE_INFO = (INSURE_INFO) INSURE_INFO_List.get(0);
			//投保流水号
			POLICY_FORMER_NO = mINSURE_INFO.getPOLICY_FORMER_NO();
			
			
			
			//TODO 投保流水号是客户号需要查询核心印刷号
			String RiskCode = "";
			if(mINSURE_INFO.getMEDICAL_TYPE().equals("01")){
				RiskCode="123202";
			} else if(mINSURE_INFO.getMEDICAL_TYPE().equals("02")){
				RiskCode="220602";
			}
			//TODO **********************************假的******************************************************????
			//RiskCode="123202";
			//查看一个客户是否只有一个投保单
			String sqlLdperson = "select customerno from ldperson where Ybkcustomerno ='" + mINSURE_INFO.getCUSTOMER_SEQUENCE_NO()+ "'";
			SSRS tSSRSPer = new ExeSQL().execSQL(sqlLdperson);
			if (tSSRSPer == null || tSSRSPer.getMaxRow() <= 0) {
				mError = "该客户没有投保单！";
				return false;
			} else if(tSSRSPer.getMaxRow() > 1){
				mError = "该客户下存在多个投保单，不能通过客户号和险种判断具体的保单！";
				errLog("客户重复投保导致数据异常，客户号为："+mINSURE_INFO.getCUSTOMER_SEQUENCE_NO(),"0");
				System.out.println("ldperson表客户号有多个，Ybkcustomerno===="+mINSURE_INFO.getCUSTOMER_SEQUENCE_NO());
				return false;
			} else {
				mCustomerNo = tSSRSPer.GetText(1, 1);
			}
			//获取印刷号
			String sqlPrtNo = "select prtno from lcpol lcp  where InsuredNo='" +mCustomerNo+ "' and riskcode='"+RiskCode+"' and uwflag not in ('a','1','2','8') and  exists (select 1 from lccontsub where prtno=lcp.prtno and (ybkserialno ='null' or ybkserialno is null) )" ;
			SSRS tSSRSPrt = new ExeSQL().execSQL(sqlPrtNo);
			if (tSSRSPrt == null || tSSRSPrt.getMaxRow() <= 0) {
				mError = "印刷号获取失败！";
				return false;
			} else {
				mPrtNo = tSSRSPrt.GetText(1, 1);
			}
			String sql = "select * from lccont where prtno = '"
					+ mPrtNo + "' ";
			LCContDB tLCContDB =  new LCContDB();
			LCContSet mLCContSet = tLCContDB.executeQuery(sql);	
			if ( mLCContSet.size() == 0 || mLCContSet ==null) {
				errLog("核心系统保单信息不存在","0");
				return false;
			} else {
				//准备新单复核
				mBPOMissionState="03";
				mLCContSchema = mLCContSet.get(1);				
			}
			
			//更新lccontsub表ybkserialno
			String ins="update lccontsub set YbkSerialno='"+POLICY_FORMER_NO+"' where 1=1 and prtno='"+mPrtNo+"' ";
			ExeSQL tExeSQL = new ExeSQL();			
			if(tExeSQL.execUpdateSQL(ins)){
				System.out.println("更新投保流水号成功--" + POLICY_FORMER_NO + "--" + mPrtNo);
			} else {
				System.out.println("更新投保流水号失败--" + POLICY_FORMER_NO + "--" + mPrtNo);
			}
			
			//TODO 新单复核
			//获取投保工作流的MissionId
			String sqlMission = "select missionid from lwmission where missionprop1= '" + mPrtNo + "' fetch first 1 rows only";
			SSRS mSSRS = new ExeSQL().execSQL(sqlMission);
			if(mSSRS.getMaxRow()>0){
				mMissionid = mSSRS.GetText(1, 1);
			} else {
				errLog("核心工作流获取失败","0");
				return false;
			}
					
			if (BPO.MISSION_STATE_CREATE_CONT.equals(mBPOMissionState)) {
				// 工作流跳转到新单复核
				if (inputConfirm("", mPrtNo)) {
					System.out.println("核心自核正常执行");
					if(!PrepareInfo()){
						errLog(mError);
						SetPrtNo(mPrtNo);
						return false;
					}
					getXmlResult();
					} else {
						errLog("核心自核失败: " + mError,"0");
						System.out.println("核心自核失败:");
						SetPrtNo(mPrtNo);
						return false;
					}
				
				} 
			SetPrtNo(mPrtNo);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	
	/**
	 * 获取保单信息
	 * 
	*/
		public boolean PrepareInfo() {

			String sql = "select contno,amnt,prem,prtno,stateflag,uwflag from lccont where prtno = '"
					+ mPrtNo + "'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			System.out.println("噢");
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("获取保单信息失败","1");
				return false;
			} else {
				mSUCCESS_INFO.setPOLICY_FORMER_NO(POLICY_FORMER_NO);
				if("9".equals(tSSRS.GetText(1, 6))){
					
					//自核之后，把暂收数据锁上，为了不能正常收费
					 //校验有无待收费信息
					String tempinfo = "select tempfeeno from ljtempfee where otherno='"
							+ mPrtNo + "' and enteraccdate is null";
					SSRS tSSRS1 = new ExeSQL().execSQL(tempinfo);
					if (tSSRS1 == null || tSSRS1.getMaxRow() <= 0) {
						errLog("保单无待收费信息","1");
						System.out.println("保单无待收费信息");
						return false;
					}
					//更新暂收分类表
					String sqltempclass = "select * from ljtempfeeclass where tempfeeno='"
							+ tSSRS1.GetText(1, 1) + "'";
					System.out.println("更新暂收" +sqltempclass);
					LJTempFeeClassDB mLJTempFeeClassDB = new LJTempFeeClassDB();
					LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
					mLJTempFeeClassSet = mLJTempFeeClassDB.executeQuery(sqltempclass);
					if(mLJTempFeeClassSet.size()!=0){
						
						for (int i = 0; i < mLJTempFeeClassSet.size(); i++){
				            
				            LJTempFeeClassSchema mLJTempFeeClassSchema = mLJTempFeeClassSet.get(i + 1);

				            System.out.println("医保扣款锁数据:" + mLJTempFeeClassSchema.getTempFeeNo());
				            mLJTempFeeClassSchema.setBankCode("");
				            mLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
				            mLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
				            
				            mLJTempFeeClassSet.set(i + 1, mLJTempFeeClassSchema);
				        }
					}
						// 提交数据
						MMap tmap = new MMap();
						tmap.put(mLJTempFeeClassSet, "UPDATE");
			            VData mInputData = new VData();
						mInputData.clear();
						mInputData.add(tmap);
						tPubSubmit.submitData(mInputData, "");
					
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU01");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("通过:" + mError);//正常承保状态
					errLog("核心自核通过","1");
					System.out.println("自核通过");
				}else if("5".equals(tSSRS.GetText(1, 6))){
					System.out.println("获取投保流水号");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU03");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("待人工核保：" + mError);//待人工核保状态
					errLog("待人核","1");
					System.out.println("自核未通过待人核");
				} else {
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT("SU02");
					mSUCCESS_INFO.setSELF_UNDERWRITING_RESULT_DES("不通过:" + mError);
					errLog("自核失败状态也没待人核","0");
					System.out.println("自核未通过");
				}
				
			}

			return true;
		}

		
		/**
		 * 返回信息报文
		 * 
		*/
		public void getXmlResult() {

			// 返回数据节点
			putResult("SUCCESS_INFO", mSUCCESS_INFO);

		}
	
	
	
	
	
	/**
	 * 进行新单复核
	 * 
	*/
	private boolean inputConfirm(String cBPOBatchNo, String cPrtNo) {

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setPrtNo(cPrtNo);
		LCContSet tLCContSet = tLCContDB.query();
		if (tLCContSet.size() == 0) {
			mError = "没有查询到以下印刷号的保单" + cPrtNo;
			return false;
		}

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("ContNo", tLCContSet.get(1).getContNo());
		mTransferData.setNameAndValue("PrtNo", cPrtNo);
		mTransferData
				.setNameAndValue("AppntNo", tLCContSet.get(1).getAppntNo());
		mTransferData.setNameAndValue("AppntName", tLCContSet.get(1)
				.getAppntName());
		mTransferData.setNameAndValue("AgentCode", tLCContSet.get(1)
				.getAgentCode());
		mTransferData.setNameAndValue("ManageCom", tLCContSet.get(1)
				.getManageCom());
		mTransferData.setNameAndValue("Operator", mGlobalInput.Operator);
		mTransferData.setNameAndValue("MakeDate", tLCContSet.get(1)
				.getMakeDate());
		mTransferData.setNameAndValue("MissionID", mMissionid);
		mTransferData.setNameAndValue("SubMissionID", "1");

		VData tVData = new VData();
		tVData.add(mTransferData);
		tVData.add(mGlobalInput);
		TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
		if (!tTbWorkFlowUI.submitData(tVData, STATIC_ActivityID)) {
			mError = tTbWorkFlowUI.mErrors.getError(0).errorMessage;
			return false;
		}

		return true;
	}


	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}


	
}
