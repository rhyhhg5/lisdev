package com.sinosoft.httpclientybk.deal;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G05.ENDORSEMENT_INFO;
import com.sinosoft.httpclientybk.dto.G05.SUCCESS_INFO;
import com.sinosoft.httpclientybk.edor.YbkBQCTInfaceBL;
import com.sinosoft.httpclientybk.xml.ctrl.AEdorBusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbkEdorCTTB extends AEdorBusLogic {
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public ENDORSEMENT_INFO mEdorInfo;
	private LCContSchema mLCContSchema;
	private String mPolicyFormerNo;
	private String mcontno;
	private SUCCESS_INFO mSuccInfo = new SUCCESS_INFO();
	

	public YbkEdorCTTB() {
	}
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return false;
	}
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理医保卡退保/解约保全项目");
		//解析XML
        if(!parseXML(cMsgInfos)){
        	mSuccInfo.setERROR_REASON("传入数据校验失败---");
        	getWrapParmList(null,"fail");
        	return false;
        }
      //先进行下必要的数据校验
        if(!checkData()){
        	System.out.println("数据校验失败---");
        	//组织返回报文
        	mSuccInfo.setERROR_REASON("传入数据校验失败---");
        	getWrapParmList(null,"fail");
        	return false;
        }

        String tsql="select code from ldcode where codetype = 'ybkbquser'";
        SSRS tSSRS1=new SSRS();
        ExeSQL tExeSQL=new ExeSQL();
        tSSRS1=tExeSQL.execSQL(tsql);
      mGlobalInput.ManageCom = "86310000";
      mGlobalInput.ComCode = "86310000";
      mGlobalInput.Operator = tSSRS1.GetText(1, 1);
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(mEdorInfo);
        data.add(mLCContSchema);
        YbkBQCTInfaceBL tYbkBQCTInfaceBL = new YbkBQCTInfaceBL();
        if(!tYbkBQCTInfaceBL.submit(data)){
        	System.out.println("保全操作错误");
        	String errorMsg = tYbkBQCTInfaceBL.mErrors.getFirstError();
        	errLog(errorMsg,"E");
        	mSuccInfo.setERROR_REASON(errorMsg);
        	//组织返回报文
        	
			getWrapParmList(null,"fail");
        	return false;
        }else{
        	//生成报文返回
        	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        	tLPEdorItemSchema =tYbkBQCTInfaceBL.getEdorItem(); 
        	//组织返回报文
        	errLog("保全处理成功","1");
        	getWrapParmList(tLPEdorItemSchema,"success");
        	return true;
        }
	}
	
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解约接口XML");
		//获取保单信息
		List<ENDORSEMENT_INFO> tEdorInfoList = cMsgInfos.getBodyByFlag("BODY");
		
        if (tEdorInfoList == null || tEdorInfoList.size() != 1)
        {
            errLog("申请报文中获取信息失败。","0");
            return false;
        }
        mEdorInfo = tEdorInfoList.get(0);  //接口一次只接受一个保单的犹豫期退保
        mPolicyFormerNo = mEdorInfo.getPOLICY_SEQUENCE_NO();
        //查询保单信息
    	LCContDB tLCContDB = new LCContDB();
    	String sql = "select contno from lccont where prtno in (select prtno from YBK_N01_LIS_ResponseInfo where PolicySequenceNo = '"+mPolicyFormerNo+"' and responsecode = '1' ) with ur";
    	mcontno = new ExeSQL().getOneValue(sql);
    	if("".equals(mcontno)){
    		errLog("传入保单编码错误", "0");
    		return false;
    	}
    	tLCContDB.setContNo(mcontno);
    	if(!tLCContDB.getInfo()){
            errLog("传入的保单号错误，核心系统不存在保单.保单编码:“"+mPolicyFormerNo+"”","0");
            return false;        		
    	}
    	mLCContSchema = tLCContDB.getSchema();
        
    	//增加并发控制，同一个保单只能请求一次
    	MMap tCekMap = null;
    	tCekMap = lockLGWORK(mLCContSchema);
    	if (tCekMap == null)
    	{
    		errLog("保单"+mLCContSchema.getContNo()+"正在进行解约，请不要重复请求","3");	
    		return false;
    	}
    	if(!submit(tCekMap)){
    		errLog("保单"+mLCContSchema.getContNo()+"正在进行解约，请不要重复请求","3");	
    		return false;
    	}
        return true;
    }
	
	private void getWrapParmList(LPEdorItemSchema edoritem,String ztFlag){
    	//判断失败还是成功
    	if("success".equals(ztFlag)&&edoritem!=null){
    		//查询付费通知书号以及金额
    		LJAGetDB tLJAGetDB = new LJAGetDB();
    		LJAGetSet tLJAGetSet = new LJAGetSet();
    		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    		tLJAGetDB.setOtherNo(edoritem.getEdorAcceptNo());
    		tLJAGetSet = tLJAGetDB.query();
    		if(tLJAGetSet!=null&&tLJAGetSet.size()>0){
    			tLJAGetSchema = tLJAGetSet.get(1);
    		}
    		//把LJAGet表银行编码置为空是财务提数要求
    		tLJAGetSchema.setBankCode("");
    		MMap map = new MMap();
    		VData data = new VData();
    		map.put(tLJAGetSchema, "UPDATE");
    		data.add(map);
    		PubSubmit ps = new PubSubmit();
    		if (!ps.submitData(data, "UPDATE")) {
    			this.mErrors.copyAllErrors(ps.mErrors);
    		}
    		//===========更新完成==============
    		mSuccInfo.setCANCEL_PREMIUM(String.valueOf(Math.abs(tLJAGetSchema.getSumGetMoney())));
    		mSuccInfo.setIF_CAN_CANCEL("1");
    		putResult("SUCCESS_INFO",mSuccInfo);
    	}else if("fail".equals(ztFlag)){
    		mSuccInfo.setIF_CAN_CANCEL("0");
    		mSuccInfo.setCANCEL_PREMIUM("退保处理失败");
    		putResult("SUCCESS_INFO",mSuccInfo);
    	}
    }
	
	
	
	  private boolean checkData(){
      	/**  对保全申请信息的校验*/
      	if(null == mEdorInfo.getPOLICY_SEQUENCE_NO() || "".equals(mEdorInfo.getPOLICY_SEQUENCE_NO())){
      		errLog("保单编码不能为空","0");	
      		return false;
      	}
      	if(null == mEdorInfo.getENDORSEMENT_APPLICATION_DATE() || "".equals(mEdorInfo.getENDORSEMENT_APPLICATION_DATE())){
      		errLog("申请日期不能为空","0");	
      		return false;
      	}
      	if(null == mEdorInfo.getEND_APPLY_NO() || "".equals(mEdorInfo.getEND_APPLY_NO())){
      		errLog("保全申请流水号不能为空","0");	
      		return false;
      	}
      	if(null == mEdorInfo.getIF_CANCEL() || "".equals(mEdorInfo.getIF_CANCEL())){
      		errLog("是否退保不能为空","0");	
      		return false;
      	}
      	if("1".equals(mEdorInfo.getIF_CANCEL()))
      		if(null == mEdorInfo.getCANCEL_REASON() || "".equals(mEdorInfo.getCANCEL_REASON())){
      			errLog("退保原因不能为空","0");	
      			return false;
      	}
      	return true;
      }
    /**
     * 锁定动作
     * @param cLCContSchema
     * @return
     */
    private MMap lockLGWORK(LCContSchema cLCContSchema)
    {
    	//犹豫期、解约接口都用WT来校验锁，以免保单同时申请WT和CT
        MMap tMMap = null;
        /**犹豫期退保锁定标志"WT"*/
        String tLockNoType = "WT";
        /**锁定时间*/
        String tAIS = "600";//10分钟的锁
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            return null;
        }
        return tMMap;
    }  
	  /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
        	errLog("提交数据库发生错误"+tPubSubmit.mErrors,"E");
            return false;
        }
        return true;
    }

}
