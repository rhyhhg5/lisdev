package com.sinosoft.httpclientybk.deal;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G02.MANUAL_UNDERWRITING;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.scan.YbkGetScanFileBL;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * 人工核保上传扫描件处理类
 * 
 * @author zq
 * 
 */

public class YbkScanTB extends ABusLogic {

	private String USER;
	private String PASSWORD;
	private String REQUEST_TYPE;

	public MANUAL_UNDERWRITING cManual;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	private String mPolicyFormerNo = ""; // 投保流水号

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	@SuppressWarnings("unchecked")
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理人工核保上传扫描件过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		USER = tMsgHead.getUSER();
		PASSWORD = tMsgHead.getPASSWORD();
		REQUEST_TYPE = tMsgHead.getREQUEST_TYPE();
		try {
			List tManualList = cMsgInfos.getBodyByFlag("BODY");// 个人理解
			if (tManualList == null || tManualList.size() != 1) {
				errLog("获取核保信息失败", "0");
				return false;
			}
			cManual = (MANUAL_UNDERWRITING) tManualList.get(0); // 获取保单信息，只有一个保单
			mPolicyFormerNo = cManual.getPOLICY_FORMER_NO();
			if ("".equals(mPolicyFormerNo) || mPolicyFormerNo == null) {
				errLog("保单印刷号有误", "0");
				return false;
			}
			SetPrtNo(mPolicyFormerNo);
			if (cManual.getIF_CANCEL_INSURE().equals("1")) {
				//往LCUWError表插入数据
				cancel();
				errLog("已经取消投保", "0");
				return false;
			}
			String sql = "select 1 from es_doc_main where doccode =( select prtno from lccontsub where Ybkserialno ='"
					+ mPolicyFormerNo + "') and subtype='TB30' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS.getMaxRow() > 0) {
				errLog("该投保单已上传过扫描件", "3");
				return false;
			}
			// ManualGetScanFileBL为业务处理类
			YbkGetScanFileBL tYbkGetScanFileBL = new YbkGetScanFileBL();
			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("PolicyFormerNo", mPolicyFormerNo);
			// 把报文信息存入VData
			transferData1.setNameAndValue("Manual", cManual);
			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(mGlobalInput);

			if (!tYbkGetScanFileBL.submitData(tVData, "")) {
				errLog(tYbkGetScanFileBL.mErrors.getFirstError(), "E");
				return false;
			}
		} catch (Exception ex) {
			errLog(ex.toString(), "E");
			return false;
		}
		errLog("处理扫描件成功", "1");
		return true;
	}
	public boolean cancel(){
		String sql1 = "select lcc.GrpContno,lcc.contno,lcp.proposalcontno,lcp.polno,lcp.proposalno,lcp.appntNo,lcp.appntName,lcp.insuredno,lcc.insuredname from lccont lcc,lcpol lcp,lccontsub lccsub where lccsub.prtno = lcc.prtno and lcp.contno = lcc.contno and lccsub.Ybkserialno ='"
			+ mPolicyFormerNo + "' with ur";
		SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
		if (tSSRS1.getMaxRow() > 1) {
			errLog("保单印刷号有误", "0");
			return false;
		}
		String LCUWErrorsql = "insert into LCUWError(GrpContNo,ContNo,ProposalContNo,PolNo,ProposalNo,AppntNo,AppntName,InsuredNo,InsuredName,UWRuleCode,UWError,UWNo,SerialNo,ModifyDate,ModifyTime,Managecom) values (";
		for(int i = 1;i<=tSSRS1.getMaxCol();i++){
			LCUWErrorsql+="'"+tSSRS1.GetText(1,i)+"',";
		}
/*				String LMUW = "select Uwcode,Remark from LMUW where riskcode = (select riskcode from lcpol where polno = '"+tSSRS1.GetText(1,4)+"') with ur";
		SSRS tSSRS2 = new ExeSQL().execSQL(LMUW);*/
		String serialno = "select max(Serialno) from LCUWError where ProposalNo = '"+tSSRS1.GetText(1,5)+"'with ur";
		SSRS tSSRS3 = new ExeSQL().execSQL(serialno);
		String str = tSSRS3.GetText(1, 1);
		int j = Integer.valueOf(str);
		LCUWErrorsql+="'000000','取消投保','1','";
		LCUWErrorsql+=(j+1)+"',current_date,current_time,'86310000')";
		new ExeSQL().execUpdateSQL(LCUWErrorsql);
		return true;
	}
}
