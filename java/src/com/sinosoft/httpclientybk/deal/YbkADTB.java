package com.sinosoft.httpclientybk.deal;

import java.util.List;

import com.sinosoft.httpclientybk.dto.G05.ENDORSEMENT_INFO;
import com.sinosoft.httpclientybk.dto.G05.SUCCESS_INFO;
import com.sinosoft.httpclientybk.xml.ctrl.AEdorBusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.EasyEdorBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class YbkADTB extends AEdorBusLogic {
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	public ENDORSEMENT_INFO mEdorInfo;
	private LCContSchema mLCContSchema;
	private String mPolicyFormerNo;
	private String mcontno;
	private SUCCESS_INFO mSuccInfo = new SUCCESS_INFO();

	private MMap map = new MMap();

	protected boolean deal(MsgCollection cMsgInfos) {
		// 解析xml
		if (!parseXML(cMsgInfos)) {
			return false;
		}

		// 修改联系方式信息
		if (!changeAppntInfo()) {
			return false;
		}

		return true;
	}

	private boolean parseXML(MsgCollection cMsgInfos) {
		System.out.println("开始解析微信平台的XML文档");
		mSuccInfo.setCANCEL_PREMIUM("非解约/退保保全项目");
		// 获取保单信息
		List<ENDORSEMENT_INFO> tEdorInfoList = cMsgInfos.getBodyByFlag("BODY");

		if (tEdorInfoList == null || tEdorInfoList.size() != 1) {
			errLog("申请报文中获取信息失败。", "0");
			mSuccInfo.setIF_CAN_CANCEL("0");
			mSuccInfo.setERROR_REASON("申请报文中获取信息失败。");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		}
		mEdorInfo = tEdorInfoList.get(0); // 接口一次只接受一个保单的犹豫期退保
		mPolicyFormerNo = mEdorInfo.getPOLICY_SEQUENCE_NO();
		// 查询保单信息
		LCContDB tLCContDB = new LCContDB();
		String sql = "select contno from lccont where prtno in (select prtno from YBK_N01_LIS_ResponseInfo where PolicySequenceNo = '"
				+ mPolicyFormerNo + "' and responsecode = '1' ) with ur";
		mcontno = new ExeSQL().getOneValue(sql);
		if ("".equals(mcontno)) {
			errLog("传入保单编码错误", "0");
			mSuccInfo.setIF_CAN_CANCEL("0");
			mSuccInfo.setERROR_REASON("保单编码不存在");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		}
		tLCContDB.setContNo(mcontno);
		if (!tLCContDB.getInfo()) {
			errLog("传入的保单号错误，核心系统不存在保单.保单编码:“" + mPolicyFormerNo + "”", "0");
			mSuccInfo.setIF_CAN_CANCEL("0");
			mSuccInfo.setERROR_REASON("保单编码不存在");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		}
		mLCContSchema = tLCContDB.getSchema();

		return true;
	}

	private boolean changeAppntInfo() {
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(mcontno);
		if (!tLCContDB.getInfo()) {
			errLog("没有查询对应保单编码的保单信息" + mcontno + "。","0");
			mSuccInfo.setIF_CAN_CANCEL("0");
			mSuccInfo.setERROR_REASON("该保单编码没有对应的保单信息");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		}
		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(tLCContDB.getAppntNo());
		tLGWorkSchema.setTypeNo("03");
		tLGWorkSchema.setContNo(mcontno);
		tLGWorkSchema.setApplyTypeNo("0");
		tLGWorkSchema.setAcceptWayNo("11");// 渠道微信 内部修改
		tLGWorkSchema
				.setAcceptDate(mEdorInfo.getENDORSEMENT_APPLICATION_DATE());
		tLGWorkSchema.setRemark("微信平台调用简易保全生成");

		// 得到客户投保信息
		LPAppntSchema tLPAppntSchema = new LPAppntSchema();
		tLPAppntSchema.setContNo(mcontno);
		tLPAppntSchema.setAppntNo(tLCContDB.getAppntNo());

		// 得到客户保单联系地址
		// QULQ ADD 简易保全CRM问题
		LCAppntDB tLCAppntDB = new LCAppntDB();
		tLCAppntDB.setContNo(mcontno);
		tLCAppntDB.getInfo();
		LCAddressDB tLCAddressDB = new LCAddressDB();
		tLCAddressDB.setCustomerNo(tLCContDB.getAppntNo());
		tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo() == null ? "1"
				: tLCAppntDB.getAddressNo());
		tLCAddressDB.getInfo();
		LCAddressSchema tLCAddressSchema = new LCAddressSchema();
		tLCAddressSchema.setSchema(tLCAddressDB.getSchema());
		//手机号校验  手机号不可修改
		if (!mEdorInfo.getMOBILE().equals(tLCAddressSchema.getMobile())) {
			errLog("手机号与注册手机号不匹配" + mcontno + "。","0");
			mSuccInfo.setIF_CAN_CANCEL("0");
			mSuccInfo.setERROR_REASON("手机号不可修改");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		}
//		tLCAddressSchema.setMobile(tLCAddressSchema.getMobile());
		// tLCAddressSchema.setPhone(mLCAppntTable.getAppntPhone()==null?tLCAddressSchema.getPhone():mLCAppntTable.getAppntPhone());
		// tLCAddressSchema.setZipCode(mLCAppntTable.getMailZipCode()==null?tLCAddressSchema.getZipCode():mLCAppntTable.getMailZipCode());
		tLCAddressSchema.setPostalAddress(mEdorInfo.getADDRESS() == null ? tLCAddressSchema.getPostalAddress(): mEdorInfo.getADDRESS());

		// 得到关联保单
		String workNo = CommonBL.createWorkNo();
		LPContSet tLPContSet = new LPContSet();
		LPContSchema tLPContSchema = new LPContSchema();
		tLPContSchema.setEdorNo(workNo);
		tLPContSchema.setEdorType(BQ.EDORTYPE_AD);
		tLPContSchema.setContNo(tLCContDB.getContNo());
		tLPContSet.add(tLPContSchema);

		// 操作员信息
	    String tsql="select code from ldcode where codetype = 'ybkbquser'";
	      SSRS tSSRS1=new SSRS();
	      ExeSQL tExeSQL=new ExeSQL();
	      tSSRS1=tExeSQL.execSQL(tsql);
	    mGlobalInput.ManageCom = "86310000";
	    mGlobalInput.Operator = tSSRS1.GetText(1, 1);
		mGlobalInput.ComCode = mGlobalInput.ManageCom;

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		data.add(tLPAppntSchema);
		data.add(tLCAddressSchema);
		data.add(tLPContSet);

		EasyEdorBL tEasyEdorBL = new EasyEdorBL();
		if (!tEasyEdorBL.submitData(data)) {
			errLog("保全变更失败" + tEasyEdorBL.mErrors, "E");
			mSuccInfo.setIF_CAN_CANCEL("0");
			mSuccInfo.setERROR_REASON(tEasyEdorBL.mErrors.getFirstError());
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		}
		errLog("变更联系方式保全项目成功", "1");
		mSuccInfo.setIF_CAN_CANCEL("1");
		mSuccInfo.setERROR_REASON("保全处理成功");
		putResult("SUCCESS_INFO", mSuccInfo);
		return true;
	}
}
