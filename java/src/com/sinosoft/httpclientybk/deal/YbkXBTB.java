package com.sinosoft.httpclientybk.deal;
/*
 * 程序功能:续保
 * 时间:2017
 * 于坤
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.sinosoft.lis.db.LCUrgeVerifyLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCUrgeVerifyLogSchema;
import com.sinosoft.lis.vschema.LCUrgeVerifyLogSet;
import com.sinosoft.lis.xb.PRnewDueFeeBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbkXBTB {
	//查找范围
    private static final int STEP_DAYS=60;
	
	//错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /* 当前日期 */
    private String CurrentDate = PubFun.getCurrentDate();
    /* 当前时间 */
    private String CurrentTime = PubFun.getCurrentTime();
    /* 操作 */
    private String mOperate="";
    
	/* 开始日期 */
	private String StartDate="";
	/* 终止日期 */
	private String EndDate="";
	/* 当前时间 */
	private String mCurrentDate = PubFun.getCurrentDate();
	/* 保单类型 */
	private String mBusinessFlag="2";
	/* 批次号 */
	private String mserNo = "";
	/* 保单号*/
	private String contNo = "";

	private String tWhereSQL = "";
	/* 判断是否为页面抽档：true为页面抽档，false为批处理*/
	private boolean flag = false;
	
	private TransferData mTransferData = new TransferData();
	// 输入数据的容器
    private VData tVData = new VData();
    //置空bankcode和bankaccno
    private VData inputData = new VData();
    private MMap tMap = new MMap();
	public GlobalInput tGI = new GlobalInput();// 公共信息
	private String manageCom="";
	
	 /**
     * @param cInputData VData，包含：
     * TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        System.out.println("cOperate======"+cOperate);
        //接收传入数据
        if (!getInputData(cInputData)) {
            return false;
        }
        //如果不是页面抽档
        if(false == flag){
        	//数据校验
        	if (!checkDate()){
        		return false;
        	}
        }
        //处理数据
        if (!dealData()) {
            return false;
        }
        //加入到催收核销日志表数据
        if (!dealUrgeLog("3", "UPDATE")) {
            return false;
        }
        return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     * @param mInputData:
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
        System.out.println("mTransferData:" + mTransferData);
        this.flag =  (Boolean) mTransferData.getValueByName("flag");
        if(true==flag){
        	contNo = (String) mTransferData.getValueByName("ContNo");
        }
        String manageCom = (String) mTransferData.getValueByName("ManageCom");
        StartDate = (String) mTransferData.getValueByName("StartDate");
        EndDate = (String) mTransferData.getValueByName("EndDate");
        mBusinessFlag =(String) mTransferData.getValueByName("BusinessFlag");
        System.out.println("StartDate=="+StartDate+"\n"+"EndDate=="+EndDate+"\n"+"ManageCom=="+manageCom+"\n"+"BusinessFlag=="+mBusinessFlag);
        
        if (tGI == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeMultiBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    public boolean checkDate(){
        if(StartDate==""||StartDate==null||"".equals(StartDate)){
        	System.out.println("起止日期不能为空!");
        	CError.buildErr(this, "起止日期不能为空!");
        	return false;
        }
        if(EndDate==""||EndDate==null||"".equals(EndDate)){
        	System.out.println("终止日期不能为空!");
        	CError.buildErr(this, "终止日期不能为空!");
        	return false;
        }
        if(java.sql.Date.valueOf(StartDate).after(java.sql.Date.valueOf(EndDate))){
        	System.out.println("起止日期不能晚于终止日期!");
        	CError.buildErr(this, "起止日期不能晚于终止日期!");
        	return false;
        }
        // 对抽档给出60天提示。
        String result = mCurrentDate;
        System.out.println("result:"+result);
        if(result != "null" && result != "" && result != null){
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        		Calendar cal = Calendar.getInstance();    
				cal.setTime(sdf.parse(EndDate));
				long time1 = cal.getTimeInMillis();			
	            cal.setTime(sdf.parse(result));    
	            long time2 = cal.getTimeInMillis();          
	            long between_days=(time2-time1)/(1000*3600*24);
	            int day = Integer.parseInt(String.valueOf(between_days));
	            if(Math.abs(day)> 60 ){
	        		System.out.println("查询时间不能超过系统当前时间60天！");
	        		CError.buildErr(this, "查询时间不能超过系统当前时间60天！");
	        		return false ;
	        	}
			} catch (ParseException e) {
				e.printStackTrace();
			}
        }
    	return true;
    }
	
	public boolean dealData(){
        //批量抽档数据查询
		String querySql = (String)mTransferData.getValueByName("querySql");
		if(querySql == null || querySql.equals("")){
			System.out.println("YBKXBTB.java开始处理逻辑");
			if(true==flag){
				//接收保单号和终止日期
				tWhereSQL = " and contno = '"+contNo+"' ";
			}else{
				tWhereSQL = "";
			}
			querySql = "select ContNo,PrtNo,AppntName,CValiDate,SumPrem, "
					+" nvl((select AccGetMoney from LCAppAcc  where CustomerNo=lccont.appntno),0),prem,min(paytodate), "
					+" (select codename from ldcode where codetype='paymode' and code=PayMode), "
					+" ShowManageName(ManageCom),AgentCode,'' "
					+" from LCCont "
					+" where 1=1 "
					+" and AppFlag='1'  and ContType='1' and (StateFlag is null or StateFlag = '1') "
					+" and exists (select 'X' from LCPol where contno=lccont.contno "
					+" and riskcode in (select code from ldcode where codetype='ybkriskcode') "
					+" and exists (select 1 from lccontsub where prtno = lccont.prtno and ifautopay = '1' ) "
					+" and (PaytoDate>='"+StartDate+"' and PaytoDate<='"+EndDate+"' "
					+" and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%')) "
					+" and managecom like '86%' "
					+" and (PaytoDate<payEndDate and PayIntv>0 "
					+" and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode) "
					+" or "
					+" exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode) "
					+" and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and riskType5 = '2' ) "
					+" and not exists(select contNo from LCRnewStateLog where polno = LCpol.polno and state in('4','5')) ) "
					+" and AppFlag='1' and (StopFlag='0' or StopFlag is null) "
					+" and PolNo not in (select PolNo from LJSPayPerson a where a.contno=contno)))  "
					+" and grpcontno='00000000000000000000' "
					+" and managecom not in (select code from ldcode where codetype='qymanagecom') " //20160121临时屏销售迁移的机构
					//本地测试加上保单号
//					+" and contno in ('014060913000001','000010875000001','000007479000002','000010846000001','000010856000001','000014456000003','000014491000001','000015246000001','000000014000007') "
//					+" and contno = '039988954000001' "
					+ tWhereSQL 
					+" group by contNo,PrtNo,AppntName,CValiDate,SumPrem,appntno,prem,PayMode,ManageCom,AgentCode "
					+" order by contNo "
					+" with ur " ;
		}
        System.out.println(querySql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0) {
            CError.buildErr(this, "系统中没有符合催收时间范围的保单信息！");
            return false;
        }
        System.out.println("本次续期续保的保单数为====================="+tSSRS.getMaxRow());

        //抽档批次号
        String tLimit = PubFun.getNoLimit(tGI.ManageCom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        mserNo = serNo;
        //插入催收核销日志表
        if (!dealUrgeLog("1", "INSERT")) {
            return false;
        }

        //循环处理数据
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

            String contNo = tSSRS.GetText(i, 1);
            String prtNo = tSSRS.GetText(i, 2);
            LCContSchema tempLCContSchema = new LCContSchema();
            tempLCContSchema.setContNo(contNo);

            System.out.println("本次续期的保单号为============================="+contNo);
            VData tVData = new VData();
            tVData.add(tempLCContSchema);
            tVData.add(tGI);
            tVData.add(serNo);
            mTransferData.setNameAndValue("serNo",serNo);
            tVData.add(mTransferData);

            //针对保单进行抽档操作（例如：生成应收数据，应收抽档通知书等）
            try {
				PRnewDueFeeBL tPRnewDueFeeBL = new PRnewDueFeeBL();
				if (!tPRnewDueFeeBL.submitData(tVData, "INSERT")) {
				    CError.buildErr(this, "保单号为：" + contNo + "的保单催收失败:" + tPRnewDueFeeBL.mErrors.getFirstError());
				    continue;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
            //更新ljspay表的bankcode字段为空操作:
            //1.不管续保扣款方式是几，ljspay的bankcode都要置空 2.如果是仅银行卡扣款的把ljspayb表的bankcode置空为了发短信
            String sql="update ljspay set bankcode = '',modifydate = current date,modifytime = current time where otherno = '"+contNo+"'";
            System.out.println(sql);
            tMap.put(sql, "UPDATE");
            //置空ljspayb的bankaccno为了发短信
            String flagSQL = "select 1 from lccontsub where prtno = (select prtno from lccont where contno = '"+contNo+"') and renemalpaymethod = '3' with ur";
            String flag = new ExeSQL().getOneValue(flagSQL);
            if("".equals(flag)||null==flag){
            	String sql1="update ljspayb set bankaccno = '',modifydate = current date,modifytime = current time where getnoticeno = (select getnoticeno from ljspay where otherno='"+contNo+" ')";
            	System.out.println(sql1);
            	tMap.put(sql1, "UPDATE");
            } else {
            	//续保扣款方式为仅银行卡扣款时
            	String sqlLCCont = "update lccont set paymode='4',expaymode='4' where prtno ='"
						+ prtNo + "' and signdate is null";
            	String sqlLCPol = "update lcpol set paymode='4',expaymode='4' where prtno ='"
						+ prtNo + "' and signdate is null";
            	System.out.println(sqlLCCont);
            	System.out.println(sqlLCPol);
            	tMap.put(sqlLCCont, "UPDATE");
            	tMap.put(sqlLCPol, "UPDATE");
            }
            inputData.add(tMap);
        }
        //置空操作
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(inputData, "")) {
        	System.out.println("ljspay表bankcode或ljspayb表bankaccno置空错误!" + tPubSubmit.mErrors.getFirstError());
        	return false;
        } else {
        	System.out.println("置空完成!");
        }
        
        return true;
	}
	
	/**
    * 加入到催收核销日志表数据
    * @param
    * @return boolean
    */
    private boolean dealUrgeLog(String pmDealState, String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
        tLCUrgeVerifyLogSchema.setRiskFlag("2");
        tLCUrgeVerifyLogSchema.setStartDate(StartDate);
        tLCUrgeVerifyLogSchema.setEndDate(EndDate);

        tLCUrgeVerifyLogSchema.setOperateType("1"); //1：续期催收操作,2：续期核销操作
        tLCUrgeVerifyLogSchema.setOperateFlag("2"); //1：个案操作,2：批次操作
        tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
        tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setErrReason("");

        } else {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mserNo);
            tLCUrgeVerifyLogDB.setOperateType("1");
            tLCUrgeVerifyLogDB.setOperateFlag("2");
            tLCUrgeVerifyLogDB.setRiskFlag("2");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet ==null )&& tLCUrgeVerifyLogSet.size() > 0) {
                 tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                 tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                 tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                 tLCUrgeVerifyLogSchema.setDealState(pmDealState);
             }else{
                return false;
            }
        }
        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;
    }
	

    

}
