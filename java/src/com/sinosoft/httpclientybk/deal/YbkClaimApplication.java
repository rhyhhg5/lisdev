package com.sinosoft.httpclientybk.deal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Document;

import com.sinosoft.httpclientybk.dto.G07.CLAIM_APPLY_INFO;
import com.sinosoft.httpclientybk.obj.MsgHead;
import com.sinosoft.httpclientybk.xml.ctrl.ABusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAskRelaSchema;
import com.sinosoft.lis.schema.LLConsultSchema;
import com.sinosoft.lis.schema.LLMainAskSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.schema.YbkReportSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * 2.7 理赔申请接口
 */
public class YbkClaimApplication extends ABusLogic {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 其余变量*/
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperate = "INSERT||MAIN";
    
	//public String BatchNo = "";	// 批次号
	public String MsgType = "";		// 报文类型
	public String Operator = "";	// 操作者
	
	//private String USER;
	private String PASSWORD;
	private String REQUEST_TYPE;

	/* 接收报文 */
	private CLAIM_APPLY_INFO claimApplyInfo;
	
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";				// 处理过程中的错误信息

	private String mPolicySequenceNo;		//保单编码
	private String mClaimFormerNo = "";		//理赔流水号
	private String mAppointName = "";		//委托人
	private String mReportName = "";		//报案人
	private String mRelationship = "";		//报案人与出险人关系
	private String mReportPhone = "";		//联系电话
	private String mReportAddr = "";		//联系地址
	private String mHospital = "";			//治疗医院
	private String mAdmissionDiagnosis = "";//入院诊断
	private String mAccidentDate = "";		//出险日期
	private String mAccidentDes = "";		//事故说明
	
	
    //处理人
    private String mHandler;
    //咨询登记信息
    private LLMainAskSchema mLLMainAskSchema = new LLMainAskSchema();
    //咨询表
    private LLConsultSchema mLLConsultSchema = new LLConsultSchema();
    //医院信息
    //private LLCommendHospitalSchema tLLCommendHospitalSchema = new LLCommendHospitalSchema();
    //事件信息
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
    //事件关联
    private LLAskRelaSchema mLLAskRelaSchema = new LLAskRelaSchema();
    //报案表
    private YbkReportSchema mYbkReportSchema = new YbkReportSchema();
    private String mMakeDate = PubFun.getCurrentDate();
    private String mMakeTime = PubFun.getCurrentTime();
    
    //咨询通知号
    private String CNo ="";
    
    
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("---------------start-解析报文--------------");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		MsgType = tMsgHead.getREQUEST_TYPE();

		mGlobalInput.Operator = "YBK";
		mGlobalInput.ComCode = "86";
		mGlobalInput.ManageCom = "86310000";
		
		mHandler = tMsgHead.getUSER();
		PASSWORD = tMsgHead.getPASSWORD();
		REQUEST_TYPE = tMsgHead.getREQUEST_TYPE();
		System.out.println("mHandler-------------"+mHandler);
		System.out.println("PASSWORD---------"+PASSWORD);
		System.out.println("REQUEST_TYPE----------"+REQUEST_TYPE);
		
		try {
			List tManualList = cMsgInfos.getBodyByFlag("BODY");
			if (tManualList == null || tManualList.size() != 1) {
				errLog("获取理赔申请xml信息失败", "0");
				return false;
			}
			/* 获取xml信息 */
			claimApplyInfo = (CLAIM_APPLY_INFO) tManualList.get(0);  //获取理赔申请信息
			if(claimApplyInfo == null || "".equals(claimApplyInfo)){
				errLog("获取理赔申请信息失败!", "0");
				return false;
			}
			mPolicySequenceNo = claimApplyInfo.getPOLICY_SEQUENCE_NO();
			if(mPolicySequenceNo == null || "".equals(mPolicySequenceNo)){
				errLog("获取保单号失败!", "0");
				return false;
			}
			mClaimFormerNo = claimApplyInfo.getCLAIM_FORMER_NO();
			if(mClaimFormerNo == null || "".equals(mClaimFormerNo)){
				errLog("获取理赔流水号失败!", "0");
				return false;
			}
			mAppointName=claimApplyInfo.getAPPOINT_NAME();
			if(mAppointName == null || "".equals(mAppointName)){
				errLog("委托人信息为空!", "0");
				//return false;
			}
			mReportName=claimApplyInfo.getREPORT_NAME();
			if(mReportName == null || "".equals(mReportName)){
				errLog("报案人信息为空!", "0");
				return false;
			}
			mRelationship=claimApplyInfo.getRELATIONSHIP();
			if(mRelationship == null || "".equals(mRelationship)){
				errLog("获取报案人与出险人关系失败!", "0");
				return false;
			}
			mReportPhone=claimApplyInfo.getREPORT_PHONE();
			if(mReportPhone == null || "".equals(mReportPhone)){
				errLog("获取联系电话失败!", "0");
				return false;
			}
			mReportAddr=claimApplyInfo.getREPORT_ADDR();
			if(mReportAddr == null || "".equals(mReportAddr)){
				errLog("联系地址信息为空!", "0");
				//return false;
			}
			mHospital=claimApplyInfo.getHOSPITAL();
			if(mHospital == null || "".equals(mHospital)){
				errLog("治疗医院信息为空!", "0");
				//return false;
			}
			mAdmissionDiagnosis=claimApplyInfo.getADMISSION_DIAGNOSIS();
			if(mAdmissionDiagnosis == null || "".equals(mAdmissionDiagnosis)){
				errLog("入院诊断信息为空!", "0");
				//return false;
			}
			mAccidentDate=claimApplyInfo.getACCIDENT_DATE();
			if(mAccidentDate == null || "".equals(mAccidentDate)){
				errLog("获取出险日期失败!", "0");
				return false;
			}
			mAccidentDes=claimApplyInfo.getACCIDENT_DES();
			if(mAccidentDes == null || "".equals(mAccidentDes)){
				errLog("事故说明为空!", "0");
				//return false;
			}
			
			/* 日期格式化:YYYYMMDD转换为YYYY-MM-DD */
			try{
				SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
				Date date = (Date) df.parse(mAccidentDate);
				String formateDate = df1.format(date);
				System.out.println("formateDate----------------"+formateDate);
			}catch (Exception ex) {
				errLog("日期格式转换错误!", "0");
				return false;
			}
	    	
	    	/* 业务处理 */
	    	try{
	    		if(!dealCont()){
	    			return false;
	    		}
	    	}catch (Exception ex) {
				errLog("业务处理失败!", "0");
				return false;
				}
		}catch (Exception ex) {
				errLog(ex.toString(), "E");
				return false;
		}
		errLog("处理成功", "1");
		System.out.println("---------------end---------------");
		return true;
	}
   
	
	/**
     * 业务处理
     * @return boolean
     */
    private boolean dealCont() throws Exception {
        System.out.println("----------------dealCont()--------------");
        if(!getCaseInfo()){
        	return  false;
        }
        PubSubmit pubSubmit = new PubSubmit();
        if (!pubSubmit.submitData(this.mResult, "")) {
            CError.buildErr(this, "提交事务失败!");
            return false;
        }
        return true;
    }
	
    
    
    /**
     * 生成案件信息
     * @return boolean
     */
    private boolean getCaseInfo() throws Exception {
        System.out.println("--------------getCaseInfo()--------------");
        MMap tmpMap = new MMap();
        String op = "UPDATE";
        String evNo = "";
        if ("INSERT||MAIN".equals(this.mOperate)) {
		    String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
		    evNo = PubFun1.CreateMaxNo("SERIALNO", "");
		    System.out.println("登记号-------------"+evNo);
		    //咨询登记表
		    this.CNo = PubFun1.CreateMaxNo("NOTICENO", tLimit);
		    System.out.println("咨询通知号-------------"+this.CNo);
		    this.mLLMainAskSchema.setLogNo(evNo);
		    this.mLLMainAskSchema.setAskType("1");  					//咨询通知类型设为默认值1,通知类型
		    //this.mLLMainAskSchema.setRemark(this.mClaimFormerNo); 	//理赔流水号
		    this.mLLMainAskSchema.setMngCom(this.mGlobalInput.ManageCom);
		    this.mLLMainAskSchema.setMakeDate(mMakeDate);
		    this.mLLMainAskSchema.setMakeTime(mMakeTime);
		    this.mLLMainAskSchema.setOperator(this.mGlobalInput.Operator);
		    //this.mLLMainAskSchema.setLogName(this.mAppointName);  	 //委托人
		    //this.mLLMainAskSchema.setLogName(this.mReportName);   	 //报案人姓名 
		    //this.mLLMainAskSchema.setLogerNo(this.mRelationship); 	 //报案人与出险人关系  
		    
		    //this.mLLMainAskSchema.setLogCompNo(this.mReportPhone);   	 //报案人联系电话
		    //this.mLLMainAskSchema.setNotAvaliReason(this.mReportAddr); //报案人联系地址
		    
		    
		    String query = "select lcc.insuredname,lcad.customerno,lcad.addressno from lcaddress lcad,lccont lcc,Lcappnt lcap where lcad.customerno = lcc.appntno and lcap.addressno = lcad.addressno and lcap.contno = lcc.contno and lcc.prtno in(select prtno from YBK_N01_LIS_ResponseInfo where PolicySequenceNo = '"+mPolicySequenceNo+"' and responsecode = '1')";
			SSRS tSSRS=new SSRS();
	    	ExeSQL tExeSQL=new ExeSQL();
	    	tSSRS=tExeSQL.execSQL(query);
	    	if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
	    		errLog("出险人信息获取失败!", "0");
				return false;
	    	}
	    	String mInsuredName = tSSRS.GetText(1, 1);
	    	String mInsuredNo = tSSRS.GetText(1, 2);
	    	String maddressno = tSSRS.GetText(1, 3);
	    	
	    	/* 根据客户号查询出险人联系电话，联系地址，email，等信息 */
		    String sql="select phone,mobile,email,Postaladdress,zipcode from lcaddress where customerno='"+mInsuredNo+"' and addressno='"+maddressno+"'";
			SSRS tSSRS1=new SSRS();
	    	ExeSQL tExeSQL1=new ExeSQL();
	    	tSSRS1=tExeSQL1.execSQL(sql);
	    	if (tSSRS1 == null || tSSRS1.getMaxRow() <= 0) {
	    		errLog("出险人联系电话、地址等信息获取失败!", "0");
	    	}
	    	String phone = tSSRS1.GetText(1, 1);
	    	String mobile = tSSRS1.GetText(1, 2);
	    	String emial = tSSRS1.GetText(1, 3);
	    	String Postaladdress = tSSRS1.GetText(1, 4);
	    	String zipcode = tSSRS1.GetText(1, 5);
	    	this.mLLMainAskSchema.setPhone(phone);  			//出险人电话
	    	this.mLLMainAskSchema.setMobile(mobile);			//出险人手机
	    	this.mLLMainAskSchema.setEmail(emial);  			//出险人邮箱
	    	this.mLLMainAskSchema.setAskAddress(Postaladdress); //出险人通讯地址
	    	this.mLLMainAskSchema.setPostCode(zipcode);  		//出险人通讯地址邮编
	    	
		    this.mTransferData.setNameAndValue("logNo", evNo);
		    
		    //操作咨询表
		    this.mLLConsultSchema.setConsultNo(this.CNo);
		    this.mLLConsultSchema.setLogNo(evNo);
		    this.mLLConsultSchema.setMngCom(this.mGlobalInput.ManageCom);
		    this.mLLConsultSchema.setMakeDate(mMakeDate);
		    this.mLLConsultSchema.setMakeTime(mMakeTime);
		    this.mLLConsultSchema.setModifyDate(mMakeDate);
		    this.mLLConsultSchema.setModifyTime(mMakeTime);
		    this.mLLConsultSchema.setOperator(this.mGlobalInput.Operator);
		    //this.mLLConsultSchema.setRemark(this.mPolicySequenceNo);  	//保单编码
		    this.mLLConsultSchema.setCustomerName(mInsuredName); 			//出险人姓名
		    this.mLLConsultSchema.setCustomerNo(mInsuredNo); 				//出险人客户号
		    String content = "入院诊断:"+this.mAdmissionDiagnosis+",保单编码:"+this.mPolicySequenceNo+",理赔流水号:"+this.mClaimFormerNo
		    		+",委托人:"+this.mAppointName+",报案人:"+this.mReportName+",报案人与出险人关系:"+this.mRelationship+",联系电话:"+this.mReportPhone+",联系地址:"+this.mReportAddr;
		    
		    this.mLLConsultSchema.setCContent(content);
		    
		    
		    //推荐医院
		    /*tLLCommendHospitalSchema.setConsultNo(CNo);
		    tLLCommendHospitalSchema.setOperator(mGlobalInput.Operator);
		    tLLCommendHospitalSchema.setMngCom(mGlobalInput.ManageCom);
		    tLLCommendHospitalSchema.setMakeDate(mMakeDate);
		    tLLCommendHospitalSchema.setMakeTime(mMakeTime);
		    tLLCommendHospitalSchema.setModifyDate(mMakeDate);
		    tLLCommendHospitalSchema.setModifyTime(mMakeTime);
		    tLLCommendHospitalSchema.setHospitalName(this.mHospital); //治疗医院
		    tLLCommendHospitalSchema.setHospitalCode("0000");*/ //治疗医院代码先默认为0000
		    
		    //事件表
		    String tSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
		    System.out.println("事件号-------------"+tSubRptNo);
		    mLLSubReportSchema.setSubRptNo(tSubRptNo);
		    mLLSubReportSchema.setCustomerName(mInsuredName); 				//出险人姓名
		    mLLSubReportSchema.setCustomerNo(mInsuredNo);     				//出险人客户号  
		    //mLLSubReportSchema.setAccSubject(this.mAdmissionDiagnosis);   //入院诊断
		    mLLSubReportSchema.setAccDesc(this.mAccidentDes); 				//事故说明
		    mLLSubReportSchema.setAccDate(this.mAccidentDate);  			//出险日期
		    mLLSubReportSchema.setAccPlace(this.mHospital); 				//治疗医院
		    mLLSubReportSchema.setOperator(mGlobalInput.Operator);
		    mLLSubReportSchema.setMngCom(mGlobalInput.ManageCom);
		    mLLSubReportSchema.setMakeDate(mMakeDate);
		    mLLSubReportSchema.setMakeTime(mMakeTime);
		    mLLSubReportSchema.setModifyDate(mMakeDate);
		    mLLSubReportSchema.setModifyTime(mMakeTime);
		
		    //事件关联表
		    mLLAskRelaSchema.setConsultNo(this.CNo);
		    mLLAskRelaSchema.setSubRptNo(tSubRptNo);
		    
		    op = "DELETE&INSERT";
        }
        
        //更新时间
        this.mLLMainAskSchema.setModifyDate(mMakeDate);
        this.mLLMainAskSchema.setModifyTime(mMakeTime);
   
        //报案表信息存储
        this.mYbkReportSchema.setConsultNo(this.CNo);  //咨询通知号，用于关联
        this.mYbkReportSchema.setLogNo(evNo);  //登记号
        this.mYbkReportSchema.setPolicySequenceNo(this.mPolicySequenceNo);  	//保单编码
        this.mYbkReportSchema.setClaimFormerNo(this.mClaimFormerNo); 			//理赔流水号
        this.mYbkReportSchema.setAppointName(this.mAppointName); 				//委托人
        this.mYbkReportSchema.setReportName(this.mReportName);   				//报案人
        this.mYbkReportSchema.setRelationShip(this.mRelationship); 				//报案人与出险人关系
        this.mYbkReportSchema.setReportPhone(this.mReportPhone); 				//联系电话
        this.mYbkReportSchema.setReportAddr(this.mReportAddr); 					//联系地址
        this.mYbkReportSchema.setHospital(this.mHospital); 						//治疗医院
        this.mYbkReportSchema.setAdmissionDiagnosis(this.mAdmissionDiagnosis); 	//入院诊断
        this.mYbkReportSchema.setAccidentDate(this.mAccidentDate); 				//出险日期
        this.mYbkReportSchema.setAccidentDes(this.mAccidentDes);				//事故说明
        this.mYbkReportSchema.setMakeDate(mMakeDate);
        this.mYbkReportSchema.setMakeTime(mMakeTime);
        this.mYbkReportSchema.setModifyDate(mMakeDate);
        this.mYbkReportSchema.setModifyTime(mMakeTime);
        
        System.out.println("**************************mytest*******" + op);
        
        tmpMap.put(this.mLLMainAskSchema, op);
        tmpMap.put(this.mLLConsultSchema, op);
        //tmpMap.put(this.tLLCommendHospitalSchema, op);
        tmpMap.put(this.mLLSubReportSchema, op);
        tmpMap.put(this.mLLAskRelaSchema, op);
        tmpMap.put(this.mYbkReportSchema, op);
        this.mResult.add(tmpMap);
        this.mResult.add(mLLConsultSchema);
        this.mResult.add(mTransferData);
        return true;
    }
    
	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}
	
}
