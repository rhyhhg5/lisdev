package com.sinosoft.httpclientybk.deal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import com.sinosoft.lis.pubfun.MMap;

import com.sinosoft.httpclientybk.services.YBKForwardXmlProxy;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.YBKErrorListSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


public class SHYBhxyw {
	
	private static SHYBhxyw sy;
	
	//前置机运维功能接口地址
	private static String url ="http://10.136.5.12:8080/ybkpre/servlet/httpserverHXYW";//外测
//	private static String url ="http://10.136.4.2:8080/ybkpre/servlet/httpserverHXYW";//正式
//	private static String url ="http://localhost:8380/ui/servlet/httpserverHXYW";//本地
	
	private SHYBhxyw(){}
	//单例模式
	public static SHYBhxyw getInstance() {
        if (sy == null) {
        	sy = new SHYBhxyw();
        }
        return sy;
    }

	// 压缩
	public static void ZipMultiFile(String filepath, String zippath) {
		System.out.println("开始压缩================");
		try {
			File file = new File(filepath);// 要被压缩的文件夹
			File zipFile = new File(zippath);
			InputStream input = null;
			ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFile));
			if (file.isDirectory()) {
				File[] files = file.listFiles();
				for (int i = 0; i < files.length; ++i) {
					input = new FileInputStream(files[i]);
					zipOut.putNextEntry(new ZipEntry(file.getName() + File.separator + files[i].getName()));
					int temp = 0;
					while ((temp = input.read()) != -1) {
						zipOut.write(temp);
					}
					input.close();
				}
			}
			zipOut.close();
			System.out.println("压缩完成================");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 解压缩（压缩文件中包含多个文件）可代替上面的方法使用。 ZipInputStream类
	 * 当我们需要解压缩多个文件的时候，ZipEntry就无法使用了， 如果想操作更加复杂的压缩文件，我们就必须使用ZipInputStream类
	 */
	public static void ZipContraMultiFile(String zippath, String outzippath) {
		System.out.println("开始解压===================");
		try {
			File file = new File(zippath);
			File outFile = null;
			ZipFile zipFile = new ZipFile(file);
			ZipInputStream zipInput = new ZipInputStream(new FileInputStream(file));
			ZipEntry entry = null;
			InputStream input = null;
			OutputStream output = null;
			while ((entry = zipInput.getNextEntry()) != null) {
				System.out.println("解压缩" + entry.getName() + "文件");
				outFile = new File(outzippath + File.separator + entry.getName());
				if (!outFile.getParentFile().exists()) {
					outFile.getParentFile().mkdir();
				}
				if (!outFile.exists()) {
					outFile.createNewFile();
				}
				input = zipFile.getInputStream(entry);
				output = new FileOutputStream(outFile);
				int temp = 0;
				while ((temp = input.read()) != -1) {
					output.write(temp);
				}
				String filePath = outzippath + "/" + entry.getName();
				if(deal(filePath)){}
				input.close();
				output.close();
				//删除已经上传的文件
				File tFile = new File(filePath);
				if (tFile.exists()) {
					if (tFile.isFile()) {
						if (!tFile.delete()) {
							System.out.println("xml删除失败！");
						}
					}
				}
			}
			zipInput.close();
			
			System.out.println("解压完毕===================");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * 封装请求报文体
	 * */
	public static String beforeDobusiness(String flag,String firstList,String SecondList,String sendDate) throws Exception{
		UUID uuid = UUID.randomUUID();
		Document document = DocumentHelper.createDocument();  
        Element root = DocumentHelper.createElement("YWRequest");  
        document.setRootElement(root); 
        //报文头
        Element Head = root.addElement("Head"); 
        Element TransTime = Head.addElement("TransTime");
        Element TransNo = Head.addElement("TransNo");
        Element TransType = Head.addElement("TransType");
        //报文头赋值
        TransTime.setText(PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        TransNo.setText(uuid.toString());
        TransType.setText("HXYW");
        //报文体
        Element RequestNodes = root.addElement("RequestNodes"); 
        Element RequestNode = RequestNodes.addElement("RequestNode");
        Element Flag = RequestNode.addElement("Flag");
        Element Folder = RequestNode.addElement("Folder");
        Element SecondFolder = RequestNode.addElement("SecondFolder");
        Element Date = RequestNode.addElement("Date");
        //报文体赋值
        Flag.setText(flag);
        Folder.setText(firstList);
        SecondFolder.setText(SecondList);
        Date.setText(sendDate);
        OutputFormat format = OutputFormat.createCompactFormat();
        format.setEncoding("GBK");
        StringWriter writer = new StringWriter();
        XMLWriter output = new XMLWriter(writer, format);
        output.write(document);
        writer.close();
        output.close();
        String xml = writer.toString();
        System.out.println(xml);
        return xml;
	}
	
	/*
	 * 功能：组装运维报文，调用前置机运维接口
	 * */
	public static String doBusiness(String packagexml,String Operator) throws Exception{
		//GBK编码用于核心
		String xml = new String(packagexml.getBytes(),"GBK"); 
		// 建立连接
		PostMethod post = new PostMethod(url);
		// 传递参数
		RequestEntity requestEntity = new StringRequestEntity(xml, "application/xml", "GBK");
		
		post.setRequestEntity(requestEntity);
		HttpClient httpClient = new HttpClient();
		int statusCode = httpClient.executeMethod(post);
		if (HttpStatus.SC_OK == statusCode) {
			String responseXml = post.getResponseBodyAsString();
			System.out.println("前置机运维功能请求返回：" + responseXml);
			if ("INSERTPC".equals(Operator)) {
				VData tVData = new VData();
				MMap tMap = new MMap();
				Map<String, String> map = new HashMap<String, String>();
				// 创建批次号
				String mBatchNo = (PubFun.getCurrentDate()).replace("-", "") + "001";

				System.out.println("mBatchNo=" + mBatchNo);
				String[] xmls = responseXml.split("A");
				for (int i = 0; i < xmls.length; i++) {
					System.out.println(xmls[i]);
					String serialno = xmls[i].substring(37, 56);
					map.put(serialno, xmls[i]);
					YBKErrorListSchema tYBKErrorListSchema = new YBKErrorListSchema();
					tYBKErrorListSchema.setSerialNo(serialno);
					tYBKErrorListSchema.setTransType("HXYW-G03");
					tYBKErrorListSchema.setTransNo(String.valueOf(i + 1));
					tYBKErrorListSchema.setBusinessNo(mBatchNo);
					tYBKErrorListSchema.setErrorInfo(xmls[i]);
					tYBKErrorListSchema.setMakeDate(PubFun.getCurrentDate());
					tYBKErrorListSchema.setMakeTime(PubFun.getCurrentTime());
					tYBKErrorListSchema.setModifyDate(PubFun.getCurrentDate());
					tYBKErrorListSchema.setModifyTime(PubFun.getCurrentTime());
					tMap.put(tYBKErrorListSchema, "INSERT");
				}
				tVData.add(tMap);
				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tVData, "INSERT")) {
					System.out.println("失败！");
					tVData.clear();
					return "Fail";
				} else {
					System.out.println("成功！");
					tVData.clear();
					return "Success";
				}
				
			}else if ("ZIP".equals(Operator)){
				if("Success".equals(Operator)){
					System.out.println("ZIP压缩成功！");
					return "Success";
				}else{
					System.out.println("ZIP压缩失败！");
					return "Fail";
				}
			}else if ("G03".equals(Operator)){
				SHYBhxyw yw = SHYBhxyw.getInstance();
				YBKForwardXmlProxy tYBKForwardXmlProxy = new YBKForwardXmlProxy();
				tYBKForwardXmlProxy.service(responseXml);
			}

		} else {
			throw new Exception("通信异常，返回码为：" + statusCode);
		}
		return "Success";
	}
	
	public boolean dealG03(String BatchNo) throws Exception{
		VData tVData = new VData();
		MMap tMap = new MMap();
		
		String sql = "select errorinfo from ybkerrorlist where businessno = '"+BatchNo+"' with ur";
		ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        SHYBhxyw yw = SHYBhxyw.getInstance();
        if(tSSRS.getMaxRow()>0){
        	for(int i = 1;i<=tSSRS.getMaxRow();i++){
        		System.out.println(tSSRS.GetText(i, 1));
        		String xml = yw.beforeDobusiness("G03", tSSRS.GetText(i, 1), "", "");
        		if("Success".equals(yw.doBusiness(xml, "G03"))){
        			//删除数据库中已经上报的数据
        			String sql1 = "delete from ybkerrorlist "
        					+ " where businessno = '"+BatchNo+"' "
        					+ " and errorinfo = '"+tSSRS.GetText(i, 1)+"' "
        					+ " and serialno = '"+tSSRS.GetText(i, 1).substring(37, 56)+"' ";
        			tMap.put(sql1,"DELETE");
        			continue;
        		}
        	}
        	tVData.add(tMap);
        	PubSubmit tPubSubmit = new PubSubmit();
        	if(!tPubSubmit.submitData(tVData, "")){
        		System.out.println("数据删除失败!");
        	}else{
        		System.out.println("数据删除成功！");
        	}
        }else{
        	System.out.println("当前批次数据不存在，请检查是否已经处理完毕或者尚未获得前置机报文入库！");
        	return false;
        }
        return true;
	}
	
	/*
	 * 实际的业务处理类滴调用核心接口
	 * */
	public static Boolean deal(String filePath){
		//G03接口
		try {
			YBKForwardXmlProxy tYBKForwardXmlProxy = new YBKForwardXmlProxy();
			InputStream mIs = new FileInputStream(filePath);
			byte[] mInXmlBytes = tYBKForwardXmlProxy.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "GBK");
			YBKForwardXmlProxy t = new YBKForwardXmlProxy();
			t.service(mInXmlStr);
			mIs.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		return true;
	}

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

	}

}
