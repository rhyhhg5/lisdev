package com.sinosoft.httpclientybk.deal;
/*
 * 程序功能:EW上海医保账户微信平台保全项目变更
 * 时间:2017-02-
 * zq
 */

import java.util.List;

import com.sinosoft.httpclientybk.dto.G05.ENDORSEMENT_INFO;
import com.sinosoft.httpclientybk.dto.G05.SUCCESS_INFO;
import com.sinosoft.httpclientybk.edor.YbkBQEWInfaceBL;
import com.sinosoft.httpclientybk.xml.ctrl.AEdorBusLogic;
import com.sinosoft.httpclientybk.xml.ctrl.MsgCollection;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.operfee.IndiLJSCancelUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbkEWTB extends AEdorBusLogic {
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	public ENDORSEMENT_INFO mEdorInfo;
	private LCContSchema mLCContSchema;
	/* 保单编码 */
	private String mPolicyFormerNo;
	/* 核心保单号 */
	private String mContNo;

	private String prtNo;
	
	private SUCCESS_INFO mSuccInfo = new SUCCESS_INFO();
	
	/*用于银行卡号,银行账户名,银行,缴费方式的数据传递*/
	VData tVData = new VData();
	TransferData tTransferData = new TransferData();

	public YbkEWTB() {
	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		String tsql="select code from ldcode where codetype = 'ybkbquser'";
    	SSRS tSSRS1=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS1=tExeSQL.execSQL(tsql);
		System.out.println("开始处理医保卡保全业务");
		mGlobalInput.ManageCom = "86310000";
		mGlobalInput.ComCode = "86310000";
		mGlobalInput.Operator = tSSRS1.GetText(1, 1);
		// 解析XML
		if (!parseXML(cMsgInfos)) {
			return false;
		}
		// 先进行下必要的数据校验
		if (!checkData()) {
			System.out.println("数据校验失败---");
			// 组织返回报文
			mSuccInfo.setERROR_REASON("传入数据校验失败---");
			mSuccInfo.setIF_CAN_CANCEL("0");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		} 
		
		//应收记录作废
		//对于已经到应收日期的不能再做EW变更，未到应收日期的，可以做EW变更，但是需要应收作废
		String xbsql = " select 1 "
				+" from ljspay a,LJSPayPerson b,lccont c "
				+" where a.otherno = b.contno "
				+" and b.contno = c.contno "
				+" and c.appflag = '1' "
				+" and a.otherno = '"+mContNo+"' "
				+" with ur ";
		String ifxb = new ExeSQL().getOneValue(xbsql);
		if ("".equals(ifxb) || null == ifxb) {
			System.out.println("没有抽档记录！");		
		}else{
			String sql = " select a.getnoticeno "
					+" from ljspay a,LJSPayPerson b,lccont c "
					+" where a.otherno = b.contno "
					+" and b.contno = c.contno "
					+" and c.appflag = '1' "
					+" and a.otherno = '"+mContNo+"' "
					+" and a.startpaydate > current date "
					+" with ur ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if ("".equals(tSSRS) || null == tSSRS || tSSRS.getMaxRow()<=0) {
				System.out.println("当前日期大于应收日期，不能再做变更！");
				errLog("当前日期大于应收日期，不能再做变更！", "E");
				return false;	
			}else{			
				//应收记录作废
				String GetNoticeNo = tSSRS.GetText(1,1);
				LJSPaySchema tLJSPaySchema =new LJSPaySchema();
				tLJSPaySchema.setGetNoticeNo(GetNoticeNo);
				
				TransferData tTransferData= new TransferData();
				tTransferData.setNameAndValue("CancelMode","1");
				
				VData tVData = new VData();
				tVData.addElement(tLJSPaySchema);
				tVData.addElement(mGlobalInput);
				tVData.addElement(tTransferData);	
				IndiLJSCancelUI tIndiLJSCancelUI = new IndiLJSCancelUI();
				tIndiLJSCancelUI.submitData(tVData,"INSERT");
			}
		}

		
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(mEdorInfo);
		data.add(mLCContSchema);
		YbkBQEWInfaceBL tEWBL = new YbkBQEWInfaceBL();
		if (!tEWBL.submit(data)) {
			System.out.println("保全操作错误");
			String errorMsg = tEWBL.mErrors.getFirstError();
			errLog(errorMsg, "E");
			mSuccInfo.setERROR_REASON(errorMsg);
			// 组织返回报文
			mSuccInfo.setIF_CAN_CANCEL("0");
			putResult("SUCCESS_INFO", mSuccInfo);
			return false;
		} else {
			// 组织返回报文
			errLog("Success", "1");
			mSuccInfo.setIF_CAN_CANCEL("1");
			mSuccInfo.setCANCEL_PREMIUM("0.0");
			mSuccInfo.setERROR_REASON("保全处理成功");
			putResult("SUCCESS_INFO", mSuccInfo);
			return true;
		}
	}

	private boolean parseXML(MsgCollection cMsgInfos) {
		System.out.println("开始解析XML");
		// 获取保单信息
		List<ENDORSEMENT_INFO> tEdorInfoList = cMsgInfos.getBodyByFlag("BODY");

		if (tEdorInfoList == null || tEdorInfoList.size() != 1) {
			errLog("申请报文中获取工单信息失败!", "0");
			return false;
		}
		mEdorInfo = tEdorInfoList.get(0);
		mPolicyFormerNo = mEdorInfo.getPOLICY_SEQUENCE_NO();
		// 查询保单信息
		LCContDB tLCContDB = new LCContDB();
		String sql = "select contno,prtno from lccont where prtno in (select prtno from YBK_N01_LIS_ResponseInfo where PolicySequenceNo = '"
				+ mPolicyFormerNo + "' and responsecode = '1' ) with ur";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			errLog("ContNo获取失败!", "0");
			return false;
		}
		this.mContNo = tSSRS.GetText(1, 1);
		System.out.println("tContNo=" + mContNo);
		tLCContDB.setContNo(mContNo);
		if (!tLCContDB.getInfo()) {
			errLog("传入的保单号错误，核心系统不存在保单.保单编码:“" + mPolicyFormerNo + "”", "0");
			return false;
		}
		mLCContSchema = tLCContDB.getSchema();
		
		//根据prtno向日志表添加数据
		this.prtNo = tSSRS.GetText(1, 2);
		System.out.println("prtNo=" + prtNo);
		SetPrtNo(prtNo);

		return true;
	}

	  private boolean checkData(){
	      	/**  对保全申请信息的校验*/
	      	if(null == mEdorInfo.getPOLICY_SEQUENCE_NO() || "".equals(mEdorInfo.getPOLICY_SEQUENCE_NO())){
	      		errLog("保单编码不能为空","0");	
	      		return false;
	      	}
	      	if(null == mEdorInfo.getENDORSEMENT_APPLICATION_DATE() || "".equals(mEdorInfo.getENDORSEMENT_APPLICATION_DATE())){
	      		errLog("申请日期不能为空","0");	
	      		return false;
	      	}
	      	if(null == mEdorInfo.getEND_APPLY_NO() || "".equals(mEdorInfo.getEND_APPLY_NO())){
	      		errLog("保全申请流水号不能为空","0");	
	      		return false;
	      	}
	      	if(null == mEdorInfo.getIF_CANCEL() || "".equals(mEdorInfo.getIF_CANCEL())){
	      		errLog("是否退保不能为空","0");	
	      		return false;
	      	}
	      	if("1".equals(mEdorInfo.getIF_CANCEL()))
	      	if(null == mEdorInfo.getCANCEL_REASON() || "".equals(mEdorInfo.getCANCEL_REASON())){
	      		errLog("退保原因不能为空","0");	
	      		return false;
	      	}
	      	if("0".equals(mEdorInfo.getIF_CANCEL())){
	      		if(null == mEdorInfo.getIF_AUTO_RENEWAL() || "".equals(mEdorInfo.getIF_AUTO_RENEWAL())){
	          		errLog("是否自动续保不能为空","0");	
	          		return false;
	          	}
	      		if("1".equals(mEdorInfo.getIF_AUTO_RENEWAL()))
	      			if(null == mEdorInfo.getRENEWAL_DEDUCT_METHOD() || "".equals(mEdorInfo.getRENEWAL_DEDUCT_METHOD())){
	              		errLog("续保扣款方式不能为空","0");	
	              		return false;
	              	}
//	          	if(null == mEdorInfo.getMEDICAL_NO() || "".equals(mEdorInfo.getMEDICAL_NO())){
//	          		errLog("医保卡号不能为空","0");	
//	          		return false;
//	          	}
	          	if(null == mEdorInfo.getBANK_NO() || "".equals(mEdorInfo.getBANK_NO())){
	          		errLog("银行卡号不能为空","0");	
	          		return false;
	          	}
	          	if(null == mEdorInfo.getBANK_NAME() || "".equals(mEdorInfo.getBANK_NAME())){
	          		errLog("所属银行不能为空","0");	
	          		return false;
	          	}
	          	if(null == mEdorInfo.getMOBILE() || "".equals(mEdorInfo.getMOBILE() )){
	          		errLog("手机号不能为空","0");	
	          		return false;
	          	}
	          	if(null == mEdorInfo.getADDRESS() || "".equals(mEdorInfo.getADDRESS())){
	          		errLog("联系地址不能为空","0");	
	          		return false;
	          	}
	      		
	      	}
	      	return true;
	      }
	/**
	 * 返回信息报文
	 * 
	 */
	public void getXmlResult() {

		// 返回数据节点
		putResult("SUCCESS_INFO", mSuccInfo);
	}

}
