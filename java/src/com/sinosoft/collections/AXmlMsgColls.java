/**
 * Create 2011-8-8
 */
package com.sinosoft.collections;

import java.util.List;

import org.apache.log4j.Logger;

/**
 * @author LY
 *
 */
public abstract class AXmlMsgColls implements IXmlMsgColls
{
    protected Logger mLogger = Logger.getLogger(getClass().getName());

    abstract public boolean setBodyByFlag(String cNodeFlag, String cText);

    abstract public boolean setBodyByFlag(String cNodeFlag, IXmlMsgColls cXmlMsgColls);

    abstract public String getTextByFlag(String cNodeFlag);

    abstract public IXmlMsgColls[] getBodyByFlag(String cNodeFlag);

    abstract public List getBodyIdxList();

    abstract public void clean();
}
