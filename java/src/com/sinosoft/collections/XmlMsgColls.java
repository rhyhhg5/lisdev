/**
 * Create 2011-8-8
 */
package com.sinosoft.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LY
 *
 */
public class XmlMsgColls extends AXmlMsgColls
{
    /** 版本号，机构体每次变换该版本自增1 */
    private long mVersion = 0l;

    /** 节点信息索引 */
    private List mMsgBodyIdx = null;

    /** 节点信息清单 */
    private Map mMsgBodyMapping = null;

    public XmlMsgColls()
    {
        clean();
    }

    public void clean()
    {
        mMsgBodyIdx = new ArrayList();
        mMsgBodyMapping = new HashMap();
    }

    public boolean setBodyByFlag(String cNodeFlag, String cText)
    {
        upVersion();

        int tCurPos = -1;

        tCurPos = mMsgBodyIdx.indexOf(cNodeFlag);
        if (tCurPos == -1)
        {
            tCurPos = mMsgBodyIdx.size();
            mMsgBodyIdx.add(cNodeFlag);
            mMsgBodyMapping.put(new Integer(tCurPos), cText);
        }
        else
        {
            mLogger.error("[" + cNodeFlag + "]" + "标识的数据信息已经存在，集合中不允许重复保存同一标识的叶子节点数据信息。");
            return false;
        }

        return true;
    }

    public boolean setBodyByFlag(String cNodeFlag, IXmlMsgColls cXmlMsgColls)
    {
        upVersion();

        List tCurNode = null;
        int tCurPos = -1;

        tCurPos = mMsgBodyIdx.indexOf(cNodeFlag);
        if (tCurPos == -1)
        {
            tCurPos = mMsgBodyIdx.size();
            tCurNode = new ArrayList();
            mMsgBodyIdx.add(cNodeFlag);
            mMsgBodyMapping.put(new Integer(tCurPos), tCurNode);
        }
        else
        {
            tCurNode = (List) mMsgBodyMapping.get(new Integer(tCurPos));
        }

        if (tCurNode == null)
        {
            mLogger.error("创建子节点数据集失败。");
            return false;
        }
        tCurNode.add(cXmlMsgColls);

        return true;
    }

    public String getTextByFlag(String cNodeFlag)
    {
        String tCurNode = null;
        int tCurPos = -1;

        tCurPos = mMsgBodyIdx.indexOf(cNodeFlag);

        if (tCurPos == -1)
        {
            mLogger.info("[" + cNodeFlag + "]标识不存在。");
            return null;
        }

        Object tObj = mMsgBodyMapping.get(new Integer(tCurPos));
        if (!(tObj instanceof String))
        {
            mLogger.info("未找到[" + cNodeFlag + "]标识所对应数据。");
            return null;
        }
        tCurNode = (String) tObj;

        return tCurNode;
    }

    public IXmlMsgColls[] getBodyByFlag(String cNodeFlag)
    {
        List tCurNode = null;
        int tCurPos = -1;

        tCurPos = mMsgBodyIdx.indexOf(cNodeFlag);

        if (tCurPos == -1)
        {
            mLogger.info("[" + cNodeFlag + "]标识不存在。");
            return null;
        }

        Object tObj = mMsgBodyMapping.get(new Integer(tCurPos));
        if (!(tObj instanceof List))
        {
            mLogger.info("[" + cNodeFlag + "]标识所对应数据非数据集。");
            return null;
        }
        tCurNode = (List) tObj;

        IXmlMsgColls[] tMsgColls = new XmlMsgColls[tCurNode.size()];
        for (int i = 0; i < tCurNode.size(); i++)
        {
            tMsgColls[i] = (XmlMsgColls) tCurNode.get(i);
        }

        return tMsgColls;
    }

    public List getBodyIdxList()
    {
        return mMsgBodyIdx;
    }

    private void upVersion()
    {
        mVersion += 1;
    }
}
