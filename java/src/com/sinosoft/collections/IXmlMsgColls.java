/**
 * Create 2011-8-8
 */
package com.sinosoft.collections;

import java.util.List;

/**
 * @author LY
 *
 */
public interface IXmlMsgColls
{
    public boolean setBodyByFlag(String cNodeFlag, String cText);

    public boolean setBodyByFlag(String cNodeFlag, IXmlMsgColls cXmlMsgColls);

    public String getTextByFlag(String cNodeFlag);

    public IXmlMsgColls[] getBodyByFlag(String cNodeFlag);

    public List getBodyIdxList();

    public void clean();
}
