package com.renewalManagement;

import java.io.StringReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.operfee.GrpDueFeePlanAuditBL;
import com.sinosoft.lis.operfee.GrpDueFeePlanUI;
import com.sinosoft.lis.operfee.GrpDueFeeSplitUI;
import com.sinosoft.lis.operfee.GrpLJSPlanCancelUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpPayActuSchema;
import com.sinosoft.lis.schema.LCGrpPayPlanSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.vschema.LCGrpPayActuSet;
import com.sinosoft.lis.vschema.LCGrpPayPlanSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * 续期管理 1.约定缴费个案催收 2.约定缴费审批
 * 
 * @author 李颢
 *
 */
public class InterfaceRenewal {
	private String errorValue;// 错误信息
	GlobalInput globalInput = new GlobalInput();

	String BatchNo;
	String SendDate;
	String SendTime;
	String BranchCode;
	String SendOperator;
	String MsgType;
	TransferData tempTransferData = new TransferData();
	LCGrpPayActuSet tLCGrpPayActuSet = new LCGrpPayActuSet();
	TransferData tempTransferDataspit = new TransferData();
	LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();
	String Flag = "";// Y 需要审批
	String PrtNo;// 印刷号码
	String GrpContNo;// 集体保单号
	String GetNoticeNo;
	String LastTime;

	/**
	 * 接口类,返回报文
	 * 
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public String service(String xml) throws Exception {
		String returnXML;
		if (!deal(xml)) {
			// 操作日志
			if ("".equals(errorValue) || null == errorValue) {
				errorValue = "未知错误！";
			}
			CreatXMLRenewal creatXMLRenewal = new CreatXMLRenewal();
			returnXML = creatXMLRenewal.createXML(errorValue, xml, GrpContNo, PrtNo);
			// 错误日志
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ SendOperator
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','"
					+ MsgType + "','" + BranchCode + "','" + BatchNo + "','" + GrpContNo + "')";
			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
			String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+ SendOperator + "','" + xml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ returnXML + "','" + errorValue + "','" + MsgType + "','" + BranchCode + "','" + GrpContNo + "')";
			ExeSQL.execUpdateSQL(insertErrorInsertLogSql);
		} else {
			CreatXMLRenewal creatXMLRenewal = new CreatXMLRenewal();
			returnXML = creatXMLRenewal.createXML(errorValue, xml, GrpContNo, PrtNo);
			// 日志
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ SendOperator
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'1','"
					+ MsgType + "','" + BranchCode + "','" + BatchNo + "','" + GrpContNo + "')";
			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		}
		return returnXML;
	}

	public boolean deal(String xml) {
		try {
			if (!getAndCheakData(xml)) {
				return false;
			}
			if ("0".equals(LastTime)) {
				if (!grpDueFeeSplit()) {
					return false;
				}
			}
			if (!GrpSingle()) {
				// 删除操作数据
				deleteErrorData();
				return false;
			}
			if ("Y".equals(Flag)) {
				if (!approval()) {
					// 删除操作数据
					deleteErrorData();
					return false;
				}
			}
		} catch (Exception e) {
			errorValue = "处理过程异常，请检查报文";
			return false;
		}
		return true;
	}

	public boolean getAndCheakData(String xml) {

		try {
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			BatchNo = elehead.getChildText("BatchNo");
			SendDate = elehead.getChildText("SendDate");
			SendTime = elehead.getChildText("SendTime");
			BranchCode = elehead.getChildText("BranchCode");
			SendOperator = elehead.getChildText("SendOperator");
			MsgType = elehead.getChildText("MsgType");
			globalInput.ManageCom = BranchCode;
			globalInput.Operator = SendOperator;
			globalInput.ComCode = BranchCode;

			// 团单信息
			Element GrpDueFeePlanInfo = rootElement.getChild("GrpDueFeePlanInfo");
			Element eGrpDueFeePlanInfo = GrpDueFeePlanInfo.getChild("Item");

			PrtNo = eGrpDueFeePlanInfo.getChildText("PrtNo");// 印刷号码
			GrpContNo = eGrpDueFeePlanInfo.getChildText("GrpContNo");// 集体保单号
			LastTime = eGrpDueFeePlanInfo.getChildText("LastTime");// 判断是否调用“约定缴费最后一次拆分”
			String PayDate = eGrpDueFeePlanInfo.getChildText("PayDate");//
			if ("0".equals(LastTime) && ("".equals(PayDate) || null == PayDate)) {
				errorValue = "最后一期缴费日期不能为空!";
				return false;
			}
			if (("".equals(PrtNo) || null == PrtNo) && ("".equals(GrpContNo) || null == GrpContNo)) {
				errorValue = "没有查询到 可催收的团单信息！";
				return false;
			}
			String sqlisHave = "SELECT 1 FROM ljspayb a WHERE   a.dealstate IN ('0','4','5') AND a.otherno='"
					+ GrpContNo + "' and a.othernotype='1'";
			SSRS ssqlisHave = new ExeSQL().execSQL(sqlisHave);
			if (ssqlisHave.getMaxRow() > 0) {
				errorValue = "团单号:" + GrpContNo + "催收错误:该团单已经存在尚未缴费核销的催收!";
				return false;
			}
			List listLCGrpPayActuInfo = eGrpDueFeePlanInfo.getChildren("LCGrpPayActuInfo");

			String Cvalidate = "";//
			// 查询团单信息
			StringBuffer sqlPrtNo = new StringBuffer();
			sqlPrtNo.append(
					" select b.GrpContNo,b.PrtNo,b.GrpName,'',b.CValiDate,(select sum(sumactupaymoney) from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC'),'约定缴费',ShowManageName(b.ManageCom),db2inst1.getUniteCode(b.AgentCode),b.ProposalGrpContNo ");
			sqlPrtNo.append(" from LCGrpCont b where  1=1  ");
			if (!"".equals(GrpContNo) && null != GrpContNo) {
				sqlPrtNo.append("and GrpContNo = '" + GrpContNo + "'");
			}
			if (!"".equals(BranchCode) && null != BranchCode) {
				sqlPrtNo.append("and ManageCom like '" + BranchCode + "%'");
			}
			if (!"".equals(PrtNo) && null != PrtNo) {
				sqlPrtNo.append("and PrtNo = '" + PrtNo + "'");
			}
			// turnPage.queryModal(strSQL, GrpContGrid);
			SSRS ssqlPrtNo = new ExeSQL().execSQL(sqlPrtNo.toString());
			if (ssqlPrtNo.getMaxRow() < 1) {
				errorValue = "没有查询到 可催收的团单信息！";
				return false;
			}
			GrpContNo = ssqlPrtNo.GetText(1, 1);
			PrtNo = ssqlPrtNo.GetText(1, 2);
			String ProposalGrpContNo = ssqlPrtNo.GetText(1, 10);
			Cvalidate = ssqlPrtNo.GetText(1, 5);
			String SqlGrpPolnum = "select contplancode,(select contplanname from lccontplan where proposalgrpcontno=a.proposalgrpcontno and contplancode=a.contplancode), "
					+ " prem,(select varchar(sum(prem)) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and plancode=a.plancode and state = '2'),"
					+ " paytodate,plancode,(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2'),"
					+ " (select nvl(sum(prem),0) from lcgrppaydue due where prtno=a.prtno and confstate='02' and contplancode=a.contplancode and "
					+ " int(plancode)<=int(a.plancode) and exists (select 1 from lcgrpcont where prtno=due.prtno and cvalidate>='2013-1-1') ) "
					+ " from lcgrppayplan a " + " where prtno= '" + PrtNo + "'"
					+ " and int(plancode)=(select min(int(plancode)) from lcgrppayplan b where prtno= '" + PrtNo + "'"
					+ " and exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='2') "
					+ " and not exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='1')) "
					+ " order by contPlanCode";
			// turnPage2.queryModal(strSQL, GrpPolGrid);
			SSRS sSqlGrpPolnum = new ExeSQL().execSQL(SqlGrpPolnum);
			if (listLCGrpPayActuInfo.size() != sSqlGrpPolnum.getMaxRow()) {
				errorValue = "印刷号：" + PrtNo + " 未缴费的保障计划 个数 " + sSqlGrpPolnum.getMaxRow() + " 与节点下的保障计划 个数 "
						+ listLCGrpPayActuInfo.size() + "不一致";
				return false;
			}
			String PlanCode = "";
			for (int i = 0; i < listLCGrpPayActuInfo.size(); i++) {
				Element elist = (Element) listLCGrpPayActuInfo.get(i);
				String ContPlanCode = elist.getChildText("ContPlanCode");// 保障计划
				String Prem = elist.getChildText("Prem");// 本次缴费金额
				if ("".equals(ContPlanCode) || null == ContPlanCode) {
					errorValue = "保障计划不能为空";
					return false;
				}
				if ("".equals(Prem) || null == Prem) {
					errorValue = "本次缴费金额不能为空";
					return false;
				}
				// 查询收费信息
				String SqlGrpPol = "select contplancode,(select contplanname from lccontplan where proposalgrpcontno=a.proposalgrpcontno and contplancode=a.contplancode), "
						+ " prem,(select varchar(sum(prem)) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and plancode=a.plancode and state = '2'),"
						+ " paytodate,plancode,(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2'),"
						+ " (select nvl(sum(prem),0) from lcgrppaydue due where prtno=a.prtno and confstate='02' and contplancode=a.contplancode and "
						+ " int(plancode)<=int(a.plancode) and exists (select 1 from lcgrpcont where prtno=due.prtno and cvalidate>='2013-1-1') ) "
						+ " from lcgrppayplan a " + " where prtno= '" + PrtNo + "' " + "and contplancode = '"
						+ ContPlanCode + "' "
						+ " and int(plancode)=(select min(int(plancode)) from lcgrppayplan b where prtno= '" + PrtNo
						+ "' and contplancode = '" + ContPlanCode + "'"
						+ " and exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='2') "
						+ " and not exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='1')) "
						+ " order by contPlanCode";
				// turnPage2.queryModal(strSQL, GrpPolGrid);
				SSRS sSqlGrpPol = new ExeSQL().execSQL(SqlGrpPol);
				int maxRow = sSqlGrpPol.getMaxRow();
				if (maxRow < 1) {
					errorValue = "没有查询到 团体保障计划信息！";
					return false;
				}
				GetNoticeNo = sSqlGrpPol.GetText(1, 7);
				PlanCode = sSqlGrpPol.GetText(1, 6);// 缴费期次
				// 根据缴费期次 校验 未缴费的团单
				String sqlPlanCode = "select min(paytodate) from lcgrppayplandetail a " + " where int(plancode)<int('"
						+ PlanCode + "') " + " and prtno='" + PrtNo + "' " + " and state='2' "
						+ " and not exists (SELECT 1 FROM ljspayb a WHERE   " + " a.dealstate IN ('0','4','5') "
						+ " AND a.otherno=(select grpcontno from lcgrpcont where prtno=a.prtno fetch first 1 rows only) "
						+ " and a.othernotype='1' ) fetch first 1 rows only with ur ";
				SSRS result = new ExeSQL().execSQL(sqlPlanCode);
				if (result.getMaxRow() > 0 && !"".equals(result.GetText(1, 1)) && null != result.GetText(1, 1)) {
					errorValue = "该团单在本期之前还有尚未缴费的缴费计划，计划缴费日期为" + result.GetText(1, 1) + "!";
					return false;
				}

				// 当前是否已经抽档
				if (null != GetNoticeNo && !"".equals(GetNoticeNo)) {
					String Sqlaudit = "select 1 from ljspay a where getnoticeno='" + GetNoticeNo
							+ "' and cansendbank='Y' with ur ";
					SSRS sSqlaudit = new ExeSQL().execSQL(Sqlaudit);
					if (sSqlaudit.getMaxRow() > 0) {
						errorValue = "该次续期已经抽档,请直接打印通知书即可。保障计划为：" + ContPlanCode;
						return false;
					}
				}
				LCGrpPayActuSchema tLCGrpPayActuSchema = new LCGrpPayActuSchema();
				tLCGrpPayActuSchema.setContPlanCode(sSqlGrpPol.GetText(1, 1));// 保障计划
				tLCGrpPayActuSchema.setPrem(Prem);// 本次缴费金额
				tLCGrpPayActuSchema.setPaytoDate(sSqlGrpPol.GetText(1, 5));// 缴费时间
				tLCGrpPayActuSchema.setPlanCode(sSqlGrpPol.GetText(1, 6));// 缴费期次
				tLCGrpPayActuSet.add(tLCGrpPayActuSchema);

				LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
				tLCGrpPayPlanSchema.setPrtNo(PrtNo);
				tLCGrpPayPlanSchema.setProposalGrpContNo(ProposalGrpContNo);
				tLCGrpPayPlanSchema.setContPlanCode(sSqlGrpPol.GetText(1, 1));
				tLCGrpPayPlanSchema.setPrem(Prem);
				tLCGrpPayPlanSchema.setPaytoDate(sSqlGrpPol.GetText(1, 5));
				tLCGrpPayPlanSchema.setPlanCode(sSqlGrpPol.GetText(1, 6));
				tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
			}
			// 判断Flag的值 是否为空或者为Y
			float tThisPrem = 0;
			String shouldresult = "0";
			String acturesult = "0";
			String sqlFlag = "select 1 from lcgrppayplandetail a " + " where int(plancode)>int('" + PlanCode + "') "
					+ " and prtno='" + PrtNo + "' fetch first 1 rows only with ur ";
			SSRS sSqlFlag = new ExeSQL().execSQL(sqlFlag);
			if (sSqlFlag.getMaxRow() < 1) {
				for (int i = 0; i < listLCGrpPayActuInfo.size(); i++) {
					Element elist1 = (Element) listLCGrpPayActuInfo.get(i);
					String Prem = elist1.getChildText("Prem");// 本次缴费金额
					tThisPrem = tThisPrem + Float.valueOf(Prem);
				}
				String shouldsql = "select sum(prem) from lcgrppayplan a " + " where  prtno='" + PrtNo
						+ "' and plancode<>'1'  " + "  with ur ";
				String tactusql = "select nvl(sum(prem),0) from lcgrppayactu a " + " where  prtno='" + PrtNo
						+ "' and state='1' " + "  with ur ";
				SSRS sshouldsql = new ExeSQL().execSQL(shouldsql);
				if (sshouldsql.getMaxRow() > 0) {
					shouldresult = sshouldsql.GetText(1, 1);
				}
				SSRS stactusql = new ExeSQL().execSQL(tactusql);
				if (stactusql.getMaxRow() > 0) {
					acturesult = stactusql.GetText(1, 1);
				}
				float difresult = Float.valueOf(shouldresult) - Float.valueOf(acturesult) - tThisPrem;
				if (difresult >= 200000) {
					Flag = "Y";
				}
			}
			tempTransferData.setNameAndValue("PrtNo", PrtNo);
			tempTransferData.setNameAndValue("GrpContNo", GrpContNo);
			tempTransferData.setNameAndValue("ProposalGrpContNo", ProposalGrpContNo);
			tempTransferData.setNameAndValue("PlanCode", PlanCode);
			tempTransferData.setNameAndValue("Flag", Flag);
			// 最后一期，续期拆分
			tempTransferDataspit.setNameAndValue("PrtNo", PrtNo);
			tempTransferDataspit.setNameAndValue("GrpContNo", GrpContNo);
			tempTransferDataspit.setNameAndValue("ProposalGrpContNo", ProposalGrpContNo);
			tempTransferDataspit.setNameAndValue("PlanCode", PlanCode);
			tempTransferDataspit.setNameAndValue("PayDate", PayDate);
		} catch (Exception e) {
			errorValue = "解析报文出错";
			return false;
		}
		return true;
	}

	/**
	 * 处理个案催收 需要抽档的保障计划
	 * 
	 * @return
	 */
	public boolean GrpSingle() {
		// 输出参数
		VData tVData = new VData();
		tVData.add(globalInput);
		tVData.add(tempTransferData);
		tVData.add(tLCGrpPayActuSet);
		GrpDueFeePlanUI tGrpDueFeePlanUI = new GrpDueFeePlanUI();
		tGrpDueFeePlanUI.submitData(tVData, "");
		if (tGrpDueFeePlanUI.mErrors.needDealError()) {
			errorValue = " 失败，原因是:" + tGrpDueFeePlanUI.mErrors.getFirstError();
			return false;
		}
		return true;
	}

	/**
	 * 审批
	 * 
	 * @return
	 */
	public boolean approval() {
		String SQLcansendbank = " select b.GrpContNo,b.cvalidate,b.ManageCom,ShowManageName(b.ManageCom),b.prtno "
				+ " from LCGrpCont b where  "
				+ " 1=1 and exists (select 1 from ljspay where otherno=b.grpcontno and cansendbank='Y') and payintv=-1 "
				+ "and GrpContNo = '" + GrpContNo + "'" + "and ManageCom like '" + BranchCode + "%'" + "and PrtNo = '"
				+ PrtNo + "'";
		// turnPage.queryModal(strSQL, GrpContGrid);
		SSRS sSQLcansendbank = new ExeSQL().execSQL(SQLcansendbank);
		if (sSQLcansendbank.getMaxRow() < 1) {
			errorValue = "未查询到需要审核的保单信息";
			return false;
		}
		String SQLGetNoticeNo = "select contplancode, "
				+ " sum(prem),(select nvl(sum(prem),0) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and state = '1'),"
				+ " (select nvl(prem,0) from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2'),"
				+ " sum(prem)-(select nvl(sum(prem),0) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and state = '1')-"
				+ "(select nvl(prem,0) from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2'),"
				+ " (select GetNoticeNo from lcgrppayactu where prtno=a.prtno and  contplancode=a.contplancode and state = '2') "
				+ " from lcgrppayplan a " + " where prtno= '" + PrtNo + "'  " + " and int(plancode)<>1 "
				+ " group by contPlanCode,a.prtno order by contPlanCode";
		// turnPage1.queryModal(strSQL, GrpPayGrid);
		SSRS sSQLGetNoticeNo = new ExeSQL().execSQL(SQLGetNoticeNo);
		if (sSQLGetNoticeNo.getMaxRow() < 1) {
			errorValue = "未查询到需要审核保单的缴费信息";
			return false;
		}
		GetNoticeNo = sSQLGetNoticeNo.GetText(1, 6);
		// 保存保单号
		String PassFlag = "01";// 审批意见0|^01|通过^02|不通过
		String UWIdea = "通过接口传输的数据，审批通过";// 意见录入

		// 输出参数
		GrpDueFeePlanAuditBL tGrpDueFeePlanAuditBL = new GrpDueFeePlanAuditBL();
		TransferData tempTransferData = new TransferData();
		tempTransferData.setNameAndValue("PrtNo", PrtNo);
		tempTransferData.setNameAndValue("GrpContNo", GrpContNo);
		tempTransferData.setNameAndValue("PassFlag", PassFlag);
		tempTransferData.setNameAndValue("UWIdea", UWIdea);
		tempTransferData.setNameAndValue("GetNoticeNo", GetNoticeNo);
		if ("01".equals(PassFlag)) {
			VData tVData = new VData();
			tVData.add(globalInput);
			tVData.add(tempTransferData);

			tGrpDueFeePlanAuditBL.submitData(tVData, "");
			if (tGrpDueFeePlanAuditBL.mErrors.needDealError()) {
				errorValue = " 失败，原因是:" + tGrpDueFeePlanAuditBL.mErrors.getFirstError();
				return false;
			}
		} else if ("02".equals(PassFlag)) {
			GrpLJSPlanCancelUI tGrpLJSPlanCancelUI = new GrpLJSPlanCancelUI();

			LJSPaySchema tLJSPaySchema = new LJSPaySchema();
			tLJSPaySchema.setGetNoticeNo(GetNoticeNo);

			VData tVData = new VData();
			tVData.addElement(tLJSPaySchema);
			tVData.addElement(globalInput);

			tGrpLJSPlanCancelUI.submitData(tVData, "INSERT");
			if (tGrpLJSPlanCancelUI.mErrors.needDealError()) {
				errorValue = " 失败，原因是:" + tGrpLJSPlanCancelUI.mErrors.getFirstError();
				return false;
			}
			VData tVDataU = new VData();
			tVDataU.add(globalInput);
			tVDataU.add(tempTransferData);
			tGrpDueFeePlanAuditBL.submitData(tVDataU, "");
			if (tGrpDueFeePlanAuditBL.mErrors.needDealError()) {
				errorValue = " 失败，原因是:" + tGrpDueFeePlanAuditBL.mErrors.getFirstError();
				return false;
			}
		}
		return true;
	}

	/**
	 * 删除操作数据
	 */
	private void deleteErrorData() {
		VData Vdata = new VData();
		MMap mmap = new MMap();
		// String GrpContNo = "00111370000012";
		String SQLdelete1 = "delete LCUrgeVerifyLog where ContNo = '" + GrpContNo + "'";
		String SQLdelete2 = "delete LockTable Where  Nolimit = '" + GrpContNo + "'";
		String SQLdelete3 = "delete LCGrpPayActu where GrpContNo = '" + GrpContNo + "'";
		String SQLdelete4 = "delete LCGrpPayActuDetail where GrpContNo = '" + GrpContNo + "'";
		String SQLdelete5 = "delete LJSPayGrp where GrpContNo = '" + GrpContNo + "'";
		String SQLdelete6 = "delete LJSPay where OtherNo = '" + GrpContNo + "'";
		String SQLdelete7 = "delete LJSPayGrpB where GrpContNo = '" + GrpContNo + "'";
		String SQLdelete8 = "delete LJSPayB where OtherNo = '" + GrpContNo + "'";
		String SQLdelete9 = "delete LOPRTManagerSub  where OtherNo = '" + GrpContNo + "'";
		String SQLdelete10 = "delete LOPRTManager  where OtherNo = '" + GrpContNo + "'";
		String SQLdelete11 = "delete LPEdorEspecialData where EdorNo = '" + GrpContNo + "'";
		mmap.put(SQLdelete1, "DELETE");
		mmap.put(SQLdelete2, "DELETE");
		mmap.put(SQLdelete3, "DELETE");
		mmap.put(SQLdelete4, "DELETE");
		mmap.put(SQLdelete5, "DELETE");
		mmap.put(SQLdelete6, "DELETE");
		mmap.put(SQLdelete7, "DELETE");
		mmap.put(SQLdelete8, "DELETE");
		mmap.put(SQLdelete9, "DELETE");
		mmap.put(SQLdelete10, "DELETE");
		mmap.put(SQLdelete11, "DELETE");
		Vdata.add(mmap);
		PubSubmit pub = new PubSubmit();
		pub.submitData(Vdata, "DELETE");
	}

	/**
	 * 最后一期，续期拆分
	 * 
	 * @param sGrpContNo
	 * @param sPrtNo
	 * @param PayDate
	 * @return
	 */
	public boolean grpDueFeeSplit() {
		String strSQL = "select contplancode,(select contplanname from lccontplan where proposalgrpcontno=a.proposalgrpcontno and contplancode=a.contplancode), "
				+ " prem,(select varchar(sum(prem)) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and plancode=a.plancode and state = '2'),"
				+ " paytodate,plancode,(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2')"
				+ " from lcgrppayplan a " + " where prtno= '" + PrtNo + "'  "
				+ " and int(plancode)=(select max(int(plancode)) from lcgrppayplan b where prtno= '" + PrtNo + "'  )"
				+ " and not exists (select 1 from lcgrppayplandetail c where c.prtno=a.prtno and int(c.plancode)<int(a.plancode) and c.state='2') "
				+ " and exists (select 1 from lcgrppayplandetail c where c.prtno=a.prtno and c.plancode=a.plancode and c.state='2') "
				+ " and not exists (select 1 from ljspay c where c.getnoticeno=(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2')) "
				+ " order by contPlanCode";
		// turnPage2.queryModal(strSQL, GrpPolGrid);
		SSRS sstrSQL = new ExeSQL().execSQL(strSQL);
		if (sstrSQL.getMaxRow() < 1) {
			System.out.println("保单最后一期已经抽档或者之前有未核销的期次!*******不需要拆分");
			return true;
		}
		VData tVData = new VData();
		tVData.add(globalInput);
		tVData.add(tempTransferDataspit);
		tVData.add(tLCGrpPayPlanSet);
		GrpDueFeeSplitUI tGrpDueFeeSplitUI = new GrpDueFeeSplitUI();
		tGrpDueFeeSplitUI.submitData(tVData, "");
		if (tGrpDueFeeSplitUI.mErrors.needDealError()) {
			errorValue = " 失败，原因是:" + tGrpDueFeeSplitUI.mErrors.getFirstError();
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		InterfaceRenewal InterfaceRenewal = new InterfaceRenewal();
		InterfaceRenewal.deleteErrorData();
	}
}
