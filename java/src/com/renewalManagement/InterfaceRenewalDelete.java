package com.renewalManagement;

import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.operfee.GrpLJSPlanCancelUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 作废应收记录
 * 
 * @author 李颢
 *
 */
public class InterfaceRenewalDelete {
	String errorValue = null;
	GlobalInput globalInput = new GlobalInput();
	MessageHeads messageHeads = new MessageHeads();
	String GetNoticeNo;

	/**
	 * 
	 * @return
	 */
	public String service(String xml) {
		if (!easyQueryClick(xml)) {
			if ("".equals(errorValue) || null == errorValue) {
				errorValue = "应收记录作废失败！";
			}
		}
		return LogMessage(xml);
	}

	/**
	 * 作废应收记录,查询可以作废的信息 / 处理作废信息
	 * 
	 * @param xml
	 * @return
	 */
	public boolean easyQueryClick(String xml) {
		try {
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			// 报文头
			Element msgHead = rootElement.getChild("MsgHead");
			Element elehead = msgHead.getChild("Item");
			String BatchNo = elehead.getChildText("BatchNo");
			String SendDate = elehead.getChildText("SendDate");
			String SendTime = elehead.getChildText("SendTime");
			String BranchCode = elehead.getChildText("BranchCode");
			String SendOperator = elehead.getChildText("SendOperator");
			String MsgType = elehead.getChildText("MsgType");
			globalInput.ManageCom = BranchCode;
			globalInput.Operator = SendOperator;
			globalInput.ComCode = BranchCode;
			messageHeads.setBatchNo(BatchNo);
			messageHeads.setSendDate(SendDate);
			messageHeads.setSendTime(SendTime);
			messageHeads.setBranchCode(BranchCode);
			messageHeads.setSendOperator(SendOperator);
			messageHeads.setMsgType(MsgType);

			Element GrpDueFeePlanInfo = rootElement.getChild("GrpDueFeePlanInfo");
			Element eGrpDueFeePlanInfo = GrpDueFeePlanInfo.getChild("Item");

			String GrpContNo = eGrpDueFeePlanInfo.getChildText("GrpContNo");// 保单号
			String AppntNo = eGrpDueFeePlanInfo.getChildText("AppntNo");// 客户号
			GetNoticeNo = eGrpDueFeePlanInfo.getChildText("GetNoticeNo");// 应收记录号
			if ((GrpContNo == null || "".equals(GrpContNo)) && (AppntNo == null || "".equals(AppntNo))
					&& (GetNoticeNo == null || "".equals(GetNoticeNo))) {
				errorValue = "保单号、应收号或投保人号不能都为空!";
				return false;
			}
			// 新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
			StringBuffer SQLHaveMessage = new StringBuffer();
			SQLHaveMessage.append(
					"select distinct a.GrpContNo,a.GrpName,a.CValiDate,min(b.PayToDate),'已签单',c.SumDuePayMoney,getAgentName(a.AgentCode) from LCGrpCont a,LCgrppol b,LJSPay c where 1=1 ");

			if (!"".equals(GrpContNo) && null != GrpContNo) {
				SQLHaveMessage.append(" and a.GrpContNo = '" + GrpContNo + "'");
			}
			if (!"".equals(AppntNo) && null != AppntNo) {
				SQLHaveMessage.append(" and a.AppntNo = '" + AppntNo + "' ");
			}
			if (!"".equals(GetNoticeNo) && null != GetNoticeNo) {
				SQLHaveMessage.append(" and c.GetNoticeNo = '" + GetNoticeNo + "'");
			}
			SQLHaveMessage.append(
					" and c.OtherNoType='1' and a.payintv=-1 and a.GrpContNo=b.GrpContNo and c.OtherNo=b.GrpContNo and a.AppFlag='1' and b.AppFlag='1' ");
			SQLHaveMessage.append(" group by a.GrpContNo,a.GrpName,a.CValiDate,a.AppFlag,c.SumDuePayMoney,a.AgentCode");
			SSRS sSQLHaveMessage = new ExeSQL().execSQL(SQLHaveMessage.toString());
			if (sSQLHaveMessage.getMaxRow() < 1) {
				errorValue = "保单号：" + GrpContNo + " 客户号：" + AppntNo + " 应收记录号：" + GetNoticeNo + " 没有查询到可以作废的催收纪录";
				return false;
			}
			GrpContNo = sSQLHaveMessage.GetText(1, 1);
			String SQLGetNoticeNo = "select distinct b.GetNoticeNo,min(c.PayToDate),b.SumDuePayMoney,(select peoples2 from lcgrpcont where grpcontno=b.otherno), b.MakeDate,(select b.codeName from LJSPayB a, LDCode b where a.dealState = b.code and b.codeType = 'dealstate' and b.GetNoticeNo=a.GetNoticeNo)"
					+ " from ljspay b,lcgrppayactu c where  b.othernotype='1' and b.OtherNo='" + GrpContNo + "'"
					+ " and c.GetNoticeNo=b.GetNoticeNo and c.state='2' "
					+ " group by b.GetNoticeNo,b.SumDuePayMoney,b.MakeDate,b.otherNo";
			// turnPage4.queryModal(strSQL, GrpJisPayGrid);
			SSRS sSQLGetNoticeNo = new ExeSQL().execSQL(SQLGetNoticeNo);
			if (sSQLGetNoticeNo.getMaxRow() < 1) {
				errorValue = "没有查询到保单号：" + GrpContNo + " 下 可以作废应收记录";
				return false;
			}
			GetNoticeNo = sSQLGetNoticeNo.GetText(1, 1);
		} catch (Exception e) {
			errorValue = "解析报文出错";
			return false;
		}
		// 处理类
		GrpLJSPlanCancelUI tGrpLJSPlanCancelUI = new GrpLJSPlanCancelUI();
		try {
			// 合同号
			LJSPaySchema tLJSPaySchema = new LJSPaySchema();
			tLJSPaySchema.setGetNoticeNo(GetNoticeNo);

			VData tVData = new VData();
			tVData.addElement(tLJSPaySchema);
			tVData.addElement(globalInput);

			tGrpLJSPlanCancelUI.submitData(tVData, "INSERT");
		} catch (Exception ex) {
			CErrors tError = tGrpLJSPlanCancelUI.mErrors;
			if (tError.needDealError()) {
				errorValue = " 失败，原因是:" + tError.getFirstError();
				return false;
			}
			errorValue = "失败，原因是:" + ex.toString();
			return false;
		}
		return true;
	}

	/**
	 * 操作日志
	 * 
	 * @param xml
	 * @return
	 */
	public String LogMessage(String xml) {
		String returnXML = createXML();
		if (!"".equals(errorValue) && null != errorValue) {
			// 错误日志
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ messageHeads.getSendOperator()
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','"
					+ messageHeads.getMsgType() + "','" + messageHeads.getBranchCode() + "','"
					+ messageHeads.getBatchNo() + "','" + GetNoticeNo + "')";
			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
			String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+ messageHeads.getSendOperator() + "','" + xml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ returnXML + "','" + errorValue + "','" + messageHeads.getMsgType() + "','"
					+ messageHeads.getBranchCode() + "','" + GetNoticeNo + "')";
			ExeSQL.execUpdateSQL(insertErrorInsertLogSql);
		} else {
			// 日志
			ExeSQL ExeSQL = new ExeSQL();
			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,transactionType ,transactionCode ,transactionBatch , grpcontno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
					+ messageHeads.getSendOperator()
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'1','"
					+ messageHeads.getMsgType() + "','" + messageHeads.getBranchCode() + "','"
					+ messageHeads.getBatchNo() + "','" + GetNoticeNo + "')";
			ExeSQL.execUpdateSQL(insertClaimInsertLogSql);
		}
		return returnXML;
	}

	private String createXML() {
		String responseXml = "";
		try {

			Element root = new Element("DataSet");
			Document doc = new Document(root);
			Element eMsgHead = new Element("MsgHead");
			Element eHeadnode = new Element("Item");
			Element eHeadBatchNo = new Element("BatchNo");
			eHeadBatchNo.setText(messageHeads.getBatchNo());
			eHeadnode.addContent(eHeadBatchNo);
			Element eHeadSendDate = new Element("SendDate");
			eHeadSendDate.setText(messageHeads.getSendDate());
			eHeadnode.addContent(eHeadSendDate);
			Element eHeadSendTime = new Element("SendTime");
			eHeadSendTime.setText(messageHeads.getSendTime());
			eHeadnode.addContent(eHeadSendTime);
			Element eHeadBranchCode = new Element("BranchCode");
			eHeadBranchCode.setText(messageHeads.getBranchCode());
			eHeadnode.addContent(eHeadBranchCode);
			Element eHeadSendOperator = new Element("SendOperator");
			eHeadSendOperator.setText(messageHeads.getSendOperator());
			eHeadnode.addContent(eHeadSendOperator);
			Element eHeadMsgType = new Element("MsgType");
			eHeadMsgType.setText(messageHeads.getMsgType());
			eHeadnode.addContent(eHeadMsgType);
			if (!"".equals(errorValue) && null != errorValue) {
				Element eHeadState = new Element("State");
				eHeadState.setText("01");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText(errorValue);
				eHeadnode.addContent(eHeadErrInfo);
			} else {
				Element eHeadState = new Element("State");
				eHeadState.setText("00");
				eHeadnode.addContent(eHeadState);
				Element eHeadErrInfo = new Element("ErrInfo");
				eHeadErrInfo.setText("操作成功！");
				eHeadnode.addContent(eHeadErrInfo);
			}
			eMsgHead.addContent(eHeadnode);

			root.addContent(eMsgHead);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(doc);
		} catch (Exception e) {
			System.out.println("生成返回报文错误：" + e.toString());
			responseXml = "生成返回报文错误：" + e.toString();
		}
		return responseXml;
	}
}
