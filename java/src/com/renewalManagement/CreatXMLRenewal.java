package com.renewalManagement;

import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CreatXMLRenewal {

	/**
	 * 
	 * 生成返回报文，errorValue为收集的处理信息
	 * 
	 * @param data
	 *            报文数据
	 * @param errorValue
	 *            处理时的信息
	 * @return
	 * @throws Exception
	 */
	public static String createXML(String errorValue, String xml, String GrpContNo, String PrtNo) throws Exception {

		String SQLmax = "select contplancode,plancode, prem,paytodate,(select nvl(sum(prem),0) from lcgrppaydue due where prtno=a.prtno and confstate='02' and contplancode=a.contplancode and  int(plancode)<=int(a.plancode)  )  from lcgrppayplan a  where prtno= '"+PrtNo+"'   and int(plancode)=(select max(int(plancode)) from lcgrppayplan b where prtno= '"+PrtNo+"'  and exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='2')  and not exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='1')) order by contPlanCode";
		SSRS sSQLmax = new ExeSQL().execSQL(SQLmax);
		String SQLmin = "select contplancode,plancode, prem,paytodate,(select nvl(sum(prem),0) from lcgrppaydue due where prtno=a.prtno and confstate='02' and contplancode=a.contplancode and  int(plancode)<=int(a.plancode)  )  from lcgrppayplan a  where prtno= '"+PrtNo+"'   and int(plancode)=(select min(int(plancode)) from lcgrppayplan b where prtno= '"+PrtNo+"'  and exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='2')  and not exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='1')) order by contPlanCode"; 
		SSRS sSQLmin = new ExeSQL().execSQL(SQLmin);
		//应收记录号
		String SQLGetNoticeNo = "select distinct b.GetNoticeNo from  ljspay b,lcgrppayactu c where  b.othernotype='1'   and b.OtherNo='"+GrpContNo+"' and c.GetNoticeNo=b.GetNoticeNo and c.state='2'";
		SSRS sSQLGetNoticeNo = new ExeSQL().execSQL(SQLGetNoticeNo);
		String GetNoticeNo = "";
		if(sSQLGetNoticeNo.getMaxRow()>0){
			GetNoticeNo = sSQLGetNoticeNo.GetText(1, 1);
		}
		StringReader reader = new StringReader(xml);
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document readdoc = tSAXBuilder.build(reader);

		Element rootElement = readdoc.getRootElement();
		// 报文头
		Element msgHead = rootElement.getChild("MsgHead");
		Element elehead = msgHead.getChild("Item");
		String BatchNo = elehead.getChildText("BatchNo");
		String SendDate = elehead.getChildText("SendDate");
		String SendTime = elehead.getChildText("SendTime");
		String BranchCode = elehead.getChildText("BranchCode");
		String SendOperator = elehead.getChildText("SendOperator");
		String MsgType = elehead.getChildText("MsgType");

		Element root = new Element("DataSet");
		Document doc = new Document(root);
		Element eMsgHead = new Element("MsgHead");
		Element eHeadnode = new Element("Item");
		Element eHeadBatchNo = new Element("BatchNo");
		eHeadBatchNo.setText(BatchNo);
		eHeadnode.addContent(eHeadBatchNo);
		Element eHeadSendDate = new Element("SendDate");
		eHeadSendDate.setText(SendDate);
		eHeadnode.addContent(eHeadSendDate);
		Element eHeadSendTime = new Element("SendTime");
		eHeadSendTime.setText(SendTime);
		eHeadnode.addContent(eHeadSendTime);
		Element eHeadBranchCode = new Element("BranchCode");
		eHeadBranchCode.setText(BranchCode);
		eHeadnode.addContent(eHeadBranchCode);
		Element eHeadSendOperator = new Element("SendOperator");
		eHeadSendOperator.setText(SendOperator);
		eHeadnode.addContent(eHeadSendOperator);
		Element eHeadMsgType = new Element("MsgType");
		eHeadMsgType.setText(MsgType);
		eHeadnode.addContent(eHeadMsgType);
		if (!"".equals(errorValue) && null != errorValue) {
			Element eHeadState = new Element("State");
			eHeadState.setText("01");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrInfo = new Element("ErrInfo");
			eHeadErrInfo.setText(errorValue);
			eHeadnode.addContent(eHeadErrInfo);
		} else {
			Element eHeadState = new Element("State");
			eHeadState.setText("00");
			eHeadnode.addContent(eHeadState);
			Element eHeadErrInfo = new Element("ErrInfo");
			eHeadErrInfo.setText("操作成功！");
			eHeadnode.addContent(eHeadErrInfo);
		}
		eMsgHead.addContent(eHeadnode);

		Element eMsgInfo = new Element("ReturnInfo");
		Element eInfodnode = new Element("Item");
		Element eGrpContNo = new Element("GrpContNo");
		eGrpContNo.setText(GrpContNo);
		eInfodnode.addContent(eGrpContNo);
		Element ePrtNo = new Element("PrtNo");
		ePrtNo.setText(PrtNo);
		eInfodnode.addContent(ePrtNo);
		
		if(sSQLmax.getMaxRow()>0 && sSQLmin.getMaxRow()>0){
			String PlanCodemax = sSQLmax.GetText(1, 2);
			String PlanCodemin = sSQLmin.GetText(1, 2);
			if(PlanCodemax.equals(PlanCodemin)){
				String PlanCodeNum = "0";//剩余缴费次数
				Element ePlanCodeNum = new Element("PlanCodeNum");
				ePlanCodeNum.setText(PlanCodeNum);
				eInfodnode.addContent(ePlanCodeNum);
				Element eGetNoticeNo = new Element("GetNoticeNo");
				eGetNoticeNo.setText(GetNoticeNo);
				eInfodnode.addContent(eGetNoticeNo);
			}else{
				String PlanCodeNum = String.valueOf(Integer.valueOf(PlanCodemax) - Integer.valueOf(PlanCodemin));
				Element ePlanCodeNum = new Element("PlanCodeNum");
				ePlanCodeNum.setText(PlanCodeNum);
				eInfodnode.addContent(ePlanCodeNum);
				Element eGetNoticeNo = new Element("GetNoticeNo");
				eGetNoticeNo.setText(sSQLGetNoticeNo.GetText(1, 1));
				eInfodnode.addContent(eGetNoticeNo);
				Element eLCGrpPayActuInfo = new Element("LCGrpPayActuInfo");
				for(int i = 1;i<=sSQLmax.getMaxRow();i++){
					String ContplanCode = sSQLmax.GetText(i, 1);//保障计划
					String PlanCodeMoney = sSQLmax.GetText(i, 3);//剩余缴费金额
					String PayToDate = sSQLmax.GetText(i, 4);//剩余缴费金额
					Element eContPlanCode = new Element("ContPlanCode");
					eContPlanCode.setText(ContplanCode);
					eLCGrpPayActuInfo.addContent(eContPlanCode);
					Element ePlanCodeMoney = new Element("PlanCodeMoney");
					ePlanCodeMoney.setText(PlanCodeMoney);
					eLCGrpPayActuInfo.addContent(ePlanCodeMoney);
					Element ePaytodate = new Element("PayToDate");
					ePaytodate.setText(PayToDate);
					eLCGrpPayActuInfo.addContent(ePaytodate);
				}
				eInfodnode.addContent(eLCGrpPayActuInfo);
			}
		}
		eMsgInfo.addContent(eInfodnode);
		
		root.addContent(eMsgHead);
		root.addContent(eMsgInfo);
		XMLOutputter out = new XMLOutputter();
		String responseXml = out.outputString(doc);
		return responseXml;
	}
}
