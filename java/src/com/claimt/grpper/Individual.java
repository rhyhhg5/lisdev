package com.claimt.grpper;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.claimt.approve.ValidataXML;
import com.preservation.util.StringUtil;
import com.sinosoft.lis.llcase.AllCaseInfoUI;
import com.sinosoft.lis.llcase.CaseCheckUI;
import com.sinosoft.lis.llcase.CasePolicyUI;
import com.sinosoft.lis.llcase.ClaimCalBL;
import com.sinosoft.lis.llcase.ClaimCalUI;
import com.sinosoft.lis.llcase.ClaimSaveUI;
import com.sinosoft.lis.llcase.ClaimUnderwriteUI;
import com.sinosoft.lis.llcase.EventSaveBL;
import com.sinosoft.lis.llcase.LLCaseReturnUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCalPaySchema;
import com.sinosoft.lis.schema.LLCaseBackSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseInfoSchema;
import com.sinosoft.lis.schema.LLCasePolicySchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimPolicySchema;
import com.sinosoft.lis.schema.LLClaimUWMainSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLAccidentSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseInfoSet;
import com.sinosoft.lis.vschema.LLCasePolicySet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.lis.vschema.LLDisabilitySet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.lis.vschema.LLOperationSet;
import com.sinosoft.lis.vschema.LLOtherFactorSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 回退案件操作理赔流程
 * @ClassName: Individual
 * @author liuzehong
 * @date 2018年12月24日 上午9:48:03
 * @version V1.0
 **/
public class Individual {
	
	static final String FORMAT_DATE = "yyyy-MM-dd";

	static final String FORMAT_TIME = "HH:mm:ss";
	
	private SimpleDateFormat simDate = new SimpleDateFormat(FORMAT_DATE);

	private SimpleDateFormat simTime = new SimpleDateFormat(FORMAT_TIME);

	private String SuccessFlag ="0";
	private static Log log = LogFactory.getLog(Individual.class);
	private FeeInput feeInput;
	private VData mResult;
	private ExeSQL mExeSQL;
	private LLCalPaySchema tLLCalPaySchema = null;// 封装理算部分信息
	private MMap mMap;
	private GlobalInput tG; // 全局变量
	private String content = "";
	private String tBatchNo = "";
	private String tSendDate = "";
	private String tSendTime = "";
	private String tBranchCode = "";
	private String tSendOperator = "";
	private String tMsgType = "";
	private String tcheckDecision = "";
	private String tRemark1 = "";
	private String tRemark2 = "";
	private LLClaimPolicySchema tLLClaimPolicySchema;
	private LLSubReportSet mLLSubReportSet;// 客户事件信息
	private String tManageCom="";
	private String tCaseNo="";
	private LLAppealAcceptSchema mLLAppealAcceptSchema;// 申诉受理信息
	private LLCaseCureSet mLLCaseCureSet=null;
	private String responseXml="";
	private String errorV="";

	public String getResponseXml() {
		return responseXml;
	}

	public Individual() {
		this.mLLSubReportSet = new LLSubReportSet();
		feeInput = new FeeInput();
		this.tG = new GlobalInput();
		this.mExeSQL = new ExeSQL();
		this.mLLAppealAcceptSchema = new LLAppealAcceptSchema();
		this.mMap = new MMap();
		this.mResult = new VData();
		mLLCaseCureSet=new LLCaseCureSet();
	}

	public boolean deal(String strXml) {
		try {
			if (!StringUtil.isNull(strXml)) {
				this.content = "请求报文不能为空";
				log.info("========================" + content + "===============================");
				return false;
			}
			// 解析报文
			if (!getData(strXml)) {
				log.info("========================" + content + "===============================");
				return false;
			}
			// 业务处理
			if (!dealData()) {
				log.info("========================" + content + "===============================");
				return false;
			}
			this.SuccessFlag="1";
		} finally {
			rerutnXml();
		}
		return true;
	}
	/**
	 * @return
	 */
	public boolean dealData() {
		log.info("========================开始账单录入===============================");
		if (!feeInput.submitDate(mResult, tCaseNo)) {
			this.content = feeInput.getContent();
			log.info("-------------------"+content+"--------------------------------");
			log.info("-------------------账单录入失败--------------------------------");
			return false;
		}
		log.info("========================开始检录===============================");
		if (!checkInput(tCaseNo)) {
			log.info("-------------------"+content+"--------------------------------");
			log.info("---------------------检录失败--------------------------------");
			caseBack(tCaseNo);
			return false;
		}
		String sqlUpdate = "update llcase set handler='lipeib' where CaseNo = '" +tCaseNo+ "'";
		if (!mExeSQL.execUpdateSQL(sqlUpdate)) {
			content = "更新llcase表handler失败";
			return false;
		}
		log.info("========================开始理算===============================");
		if (!adjustmen(this.tCaseNo)) {
			caseBack(tCaseNo);
			this.content+=errorV;
			log.info("-------------------"+content+"--------------------------------");
			log.info("---------------------理算失败 --------------------------------");
			return false;
		}
		log.info("========================" + content + "===============================");
		return true;
	}

	/**
	 * @param strXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean getData(String strXml) {
		log.info("***********开始解析报文*********");
		Document doc;
		try {
			
			ValidataXML validataXML=new ValidataXML();
			if(!validataXML.validate(strXml, getxsd())){
				content = validataXML.getContent();
				System.out.println(content);
				return false;
			}
			doc = DocumentHelper.parseText(strXml);
			Element root = doc.getRootElement();
			// 解析报文头
			Element thead = root.element("head");
			this.tBatchNo = thead.elementText("BatchNo");
			this.tSendDate = thead.elementText("SendDate");
			this.tSendTime = thead.elementText("SendTime");
			this.tBranchCode = thead.elementText("BranchCode");
			this.tSendOperator = thead.elementText("SendOperator");
			this.tMsgType = thead.elementText("MsgType");
			// 解析报文体
			Element tbody = root.element("body");
			this.tManageCom = tbody.elementText("ManageCom");
			this.tCaseNo = tbody.elementText("CaseNo");
            Element tcorrection = tbody.element("correction");
			mLLAppealAcceptSchema.setManageCom(tManageCom);
			mLLAppealAcceptSchema.setrgtno(tCaseNo);
			mLLAppealAcceptSchema.setInsuredStat(tcorrection.elementText("InsuredStat"));// 参保人员类别
			mLLAppealAcceptSchema.setSecurityNo(tcorrection.elementText("SecurityNo"));// 客户社保号
			mLLAppealAcceptSchema.setCompSecuNo(tcorrection.elementText("CompSecuNo"));// 单位社保登记号
			mLLAppealAcceptSchema.setHospitalName(tcorrection.elementText("HospitalName"));// 医院名称
			mLLAppealAcceptSchema.setHospitalCode(tcorrection.elementText("HospitalCode"));// 医院代码
			// 医院属性？FixFlag
			// 医院级别？HosGrade
			mLLAppealAcceptSchema.setFeeAtti(tcorrection.elementText("FeeAtti"));// 账单属性
			mLLAppealAcceptSchema.setFeeType(tcorrection.elementText("FeeType"));// 账单种类
			mLLAppealAcceptSchema.setFeeAffixType(tcorrection.elementText("FeeAffixType"));// 账单类型
			mLLAppealAcceptSchema.setReceiptType(tcorrection.elementText("ReceiptType"));// 收据类型
			mLLAppealAcceptSchema.setReceiptNo(tcorrection.elementText("ReceiptNo"));// 账单号码
			mLLAppealAcceptSchema.setinpatientNo(tcorrection.elementText("inpatientNo"));// 住院号
			// 城区属性？UrbanFlag
			mLLAppealAcceptSchema.setFeeDate(tcorrection.elementText("FeeDate"));// 结算日期
			mLLAppealAcceptSchema.setHospStartDate(tcorrection.elementText("HospStartDate"));// 入院日期
			mLLAppealAcceptSchema.setHospEndDate(tcorrection.elementText("HospEndDate"));// 出院日期
			// 治疗天数?RealHospDate
			mLLAppealAcceptSchema.setIssueUnit(tcorrection.elementText("IssueUnit"));// 分割单出具单位
			mLLAppealAcceptSchema.setContRemark(tcorrection.elementText("ContRemark"));// 客户特约信息
			// 医保类型？MedicareType
			// String MedicareType=tcorrection.elementText("MedicareType");
			// 费用明细
			List<Element> tFeeItem = tcorrection.selectNodes("FeeItem");// 费用明细
			for (int i = 0; i < tFeeItem.size(); i++) {
				mMap.put(tFeeItem.get(i).elementText("FeeType"), tFeeItem.get(i).elementText("Fee"));
				System.out.println("费用明细---> 费用类型：" + tFeeItem.get(i).elementText("FeeType") + ", 费用金额："
						+ tFeeItem.get(i).elementText("Fee"));
			}
			// 客户事件信息
			List<Element> tItemList = tcorrection.selectNodes("item");
			for (int i = 0; i < tItemList.size(); i++) {
				if ("".equals(tItemList.get(i).elementText("SubDate"))
						|| tItemList.get(i).elementText("SubDate") == null) {
					System.out.println("没有事件信息！！！");
					this.content="请检查事件信息是否完整";
					return false;
				}
				LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
				tLLSubReportSchema.setAccProvinceCode(tItemList.get(i).elementText("AccProvinceCode"));// 发生地点(省)
				tLLSubReportSchema.setAccCityCode(tItemList.get(i).elementText("AccCityCode"));// 发生地点(市)
				tLLSubReportSchema.setAccCountyCode(tItemList.get(i).elementText("AccCountyCode"));// 发生地点(县)
				tLLSubReportSchema.setAccDate(tItemList.get(i).elementText("SubDate"));// 发生日期
				tLLSubReportSchema.setAccPlace(tItemList.get(i).elementText("SubAddress"));// 发生地点
				tLLSubReportSchema.setInHospitalDate(tItemList.get(i).elementText("InHospitalDate"));// 入院日期
				tLLSubReportSchema.setOutHospitalDate(tItemList.get(i).elementText("OutHospitalDate"));// 出院日期
				tLLSubReportSchema.setAccDesc(tItemList.get(i).elementText("SubMsg"));// 事件信息
				tLLSubReportSchema.setAccidentType(tItemList.get(i).elementText("AccType"));// 事件类型
				String sql = "select subrptno  from llcaserela where CaseNo='"+tCaseNo+"'";
				String subrptno = mExeSQL.getOneValue(sql);
				if (!StringUtil.isNull(subrptno)) {
					this.content="未查询到案件对应的事件号。请检查："+tCaseNo;
					return false;
				}
				tLLSubReportSchema.setSubRptNo(subrptno);
				mLLSubReportSet.add(tLLSubReportSchema);
			}
			mResult.add(strXml);
			mResult.add(mLLAppealAcceptSchema);
			mResult.add(mMap);// 费用明细
			mResult.add(mLLSubReportSet);
			
			//疾病信息
			List<Element> tDisItemList = tcorrection.selectNodes("DisItem");
			for (int i = 0; i < tDisItemList.size(); i++) {
				LLCaseCureSchema mLLCaseCureSchema = new LLCaseCureSchema();
				mLLCaseCureSchema.setDiseaseName(tDisItemList.get(i).elementText("DiseaseName"));//疾病名称
				mLLCaseCureSchema.setDiseaseCode(tDisItemList.get(i).elementText("DiseaseCode"));//疾病代码
				mLLCaseCureSchema.setLDCODE(tDisItemList.get(i).elementText("LDCODE"));//肿瘤形态学代码
				mLLCaseCureSchema.setLDNAME(tDisItemList.get(i).elementText("LDNAME"));//肿瘤形态学名称
				mLLCaseCureSchema.setHospitalCode(tDisItemList.get(i).elementText("HospitalCode"));//诊治医院
				mLLCaseCureSchema.setDoctorName(tDisItemList.get(i).elementText("DoctorName"));//治疗医生
				mLLCaseCureSchema.setOperationFlag(tDisItemList.get(i).elementText("OperationFlag"));//手术
				mLLCaseCureSet.add(mLLCaseCureSchema);
			}
			// 封装理算部分信息
			Element tCalPay = tbody.element("CalPay");
			Element titem = tCalPay.element("item");
			this.tLLCalPaySchema = new LLCalPaySchema();
			//LLClaimDeclineSchema mLLClaimDeclineSchema = new LLClaimDeclineSchema();
			tLLCalPaySchema.setriskcode(titem.elementText("Riskcode"));// 险种代码
			tLLCalPaySchema.setgetdutycode(titem.elementText("Getdutycode"));// 给付责任代码
			tLLCalPaySchema.setGetDutyKind(titem.elementText("GetDutyKind"));// 给付责任类型
			tLLCalPaySchema.setGiveType(titem.elementText("GiveType"));// 赔付结论代码
			tLLCalPaySchema.setGiveReason(titem.elementText("GiveReason"));// 赔付结论依据代码
			tLLCalPaySchema.setGiveTypeDesc(titem.elementText("GiveTypeDesc"));// 赔付结论
			tLLCalPaySchema.setGiveReasonDesc(titem.elementText("GiveReasonDesc"));// 赔付结论依据
			tLLCalPaySchema.setIsRate(titem.elementText("IsRate"));// 比例金额
			tLLCalPaySchema.setDeclineAmnt(titem.elementText("DeclineAmnt"));// 拒付金额
			tLLCalPaySchema.setApproveAmnt(titem.elementText("ApproveAmnt"));// 通融/协议给付比例
			tLLCalPaySchema.setEasyCase(titem.elementText("EasyCase"));// 是否为简易案件
			//mLLClaimDeclineSchema.setReason(titem.elementText("DeclineReason"));
		} catch (DocumentException e) {
			e.printStackTrace();
			this.content = "解析报文出错:" + e.toString();
			return false;
		}
		return true;
	}
	/**
	 * 理算
	 */
	public boolean adjustmen(String caseNo) {
		// 计算实赔金额
		if (!calPay(caseNo)) {
			log.info("========================理算失败================================");
			return false;
		}
		String FlagStr = "";
		CErrors tError = null;
		String transact = "SAVE";
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		tLLCaseSchema.setCaseNo(caseNo);

		if (StringUtil.isNull(tLLCalPaySchema.getEasyCase())) {
			tLLCaseSchema.setCaseProp("09");
		} else {
			tLLCaseSchema.setCaseProp("08");
		}
		String sql = "SELECT contno, RiskCode, b.getdutyname, a.TabFeeMoney, a.DeclineAmnt\r\n"
				+ "	, a.pregiveAmnt, a.ApproveAmnt, OutDutyAmnt, OverAmnt, a.GiveTypeDesc\r\n"
				+ "	, a.GiveReasonDesc, a.ClaimMoney, a.StandPay, a.RealPay, a.polno\r\n"
				+ "	, a.clmno, a.getdutycode, a.getdutykind, a.dutycode, a.GiveType\r\n"
				+ "	, a.GiveReason, a.caserelano, a.DeclineNo, a.rgtno\r\n" + "FROM LLClaimdetail a, LMDutyGetClm b\r\n"
				+ "WHERE a.caseno = '" + caseNo + "'\r\n" + "	AND b.getdutycode = a.getdutycode\r\n"
				+ "	AND b.getdutykind = a.getdutykind";
		SSRS sqlSSRS = mExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() < 1) {
			this.content = "未查询到理算数据";
			log.info("========================" + content + "===============================");
			return false;
		}
		LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
		for (int i = 1; i <= sqlSSRS.getMaxRow(); i++) {
			LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
			tLLClaimDetailSchema.setRgtNo(sqlSSRS.GetText(i, 24));
			tLLClaimDetailSchema.setContNo(sqlSSRS.GetText(i, 1));
			tLLClaimDetailSchema.setGetDutyCode(sqlSSRS.GetText(i, 17));
			tLLClaimDetailSchema.setGetDutyKind(sqlSSRS.GetText(i, 18));
			tLLClaimDetailSchema.setStandPay(sqlSSRS.GetText(i, 13));
			tLLClaimDetailSchema.setRealPay(sqlSSRS.GetText(i, 14));
			tLLClaimDetailSchema.setPolNo(sqlSSRS.GetText(i, 15));
			tLLClaimDetailSchema.setClmNo(sqlSSRS.GetText(i, 16));
			tLLClaimDetailSchema.setDutyCode(sqlSSRS.GetText(i, 19));
			tLLClaimDetailSchema.setGiveType(tLLCalPaySchema.getGiveType());
			tLLClaimDetailSchema.setGiveTypeDesc(tLLCalPaySchema.getGiveTypeDesc());
			tLLClaimDetailSchema.setGiveReason(tLLCalPaySchema.getGiveReason());
			tLLClaimDetailSchema.setGiveReasonDesc(tLLCalPaySchema.getGiveReasonDesc());
			tLLClaimDetailSchema.setCaseRelaNo(sqlSSRS.GetText(i, 22));
			tLLClaimDetailSchema.setCaseNo(caseNo);
			tLLClaimDetailSchema.setDeclineNo(tLLCalPaySchema.getIsRate());
			tLLClaimDetailSchema.setDeclineAmnt(tLLCalPaySchema.getDeclineAmnt());
			tLLClaimDetailSchema.setApproveAmnt(tLLCalPaySchema.getApproveAmnt());
			tLLClaimDetailSet.add(tLLClaimDetailSchema);
			
		}
		ClaimSaveUI tClaimSaveUI = new ClaimSaveUI();
		boolean cs = true;
		// 准备传输数据 VData
		VData tVData = new VData();
		tG.Operator = "lipeib";
		tG.ManageCom = this.tManageCom;
		try {
			tVData.addElement(tG);
			tVData.addElement(tLLClaimDetailSet);
			tVData.addElement(tLLCaseSchema);
			cs = tClaimSaveUI.submitData(tVData, transact);
		} catch (Exception ex) {
			content = transact + "失败，原因是:" + ex.toString();
			return false;
		}
		if (FlagStr == "") {
			tError = tClaimSaveUI.mErrors;
			if (cs || !tError.needDealError()) {
				content = "保存成功";
				log.info("========================" + content + "===============================");
				FlagStr = "Succ";
			} else {
				content = " 保存失败，原因是:" + tError.getFirstError();
				log.info("========================" + content + "===============================");
				return false;
			}
		}
		return true;
	}

	/**
	 * 理算计算
	 */
	public boolean calPay(String caseNo) {
		//判断对象是否实例化
		if (tLLCalPaySchema == null) {
			this.content = "理算失败，原因：获取理算信息失败";
			return false;
		}
		if (!StringUtil.isNull(tLLCalPaySchema.getriskcode(), tLLCalPaySchema.getGetDutyKind())) {
			this.content = "理算失败，原因：险种编码，给付责任编码不能为空";
			return false;
		}
		// 新增效验，理赔二核时不可理赔计算
		String rgtstateSql = "select rgtstate from llcase where caseno='" + caseNo + "' with ur";
		String rgtstate = mExeSQL.getOneValue(rgtstateSql);
		if (!StringUtil.isNull(rgtstate)) {
			if ("16".equals(rgtstate)) {
				this.content = "理赔二核中不能处理，必须等待二核结束！";
				return false;
			}
		}
		if ("162501".equals(tLLCalPaySchema.getriskcode())) {
			String tDeathSQL = "select deathdate from llcase where caseno='" + caseNo + "' with ur";
			String tDeathDate = mExeSQL.getOneValue(tDeathSQL);
			if (!StringUtil.isNull(tDeathDate) && tLLCalPaySchema.getGetDutyKind() != "501") {
				this.content = tLLCalPaySchema.getriskcode() + "业务u案件，一个案件下不能同时理赔医疗险责任、身故责任。";
				return false;
			} else if (!StringUtil.isNull(tDeathDate) && tLLCalPaySchema.getGetDutyKind() == "501") {
				this.content = tLLCalPaySchema.getriskcode() + "业务案件，一个案件下不能同时理赔医疗险责任、身故责任。";
				return false;
			}
		}
		String sql1="select * from LLClaimDetail where GetDutyCode='"+tLLCalPaySchema.getgetdutycode()+"' and GetDutyKind='"+tLLCalPaySchema.getGetDutyKind()+"'  and caseno='"+caseNo+"'";
		SSRS sqlSSRS1 = mExeSQL.execSQL(sql1);
		if (sqlSSRS1.getMaxRow()<1) {
			this.content = "请核实给付责任类型和给付责任编码是否正确";
			return false;
		}
		SSRS sqlSSRS;
		String sql = "select rgtno,contno,polno from LLClaimdetail where   caseno='" + caseNo + "'";
		sqlSSRS = mExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() < 1) {
			content = "未找到案件信息,请核实案件：" + caseNo;
			return false;
		}
		String polno = sqlSSRS.GetText(1, 3);
		String contno = sqlSSRS.GetText(1, 2);
		String rgtno = sqlSSRS.GetText(1, 1);
		String transact = "Cal";
		String FlagStr = "";
		CErrors tError = null;
		TransferData recalFlag = new TransferData();
		recalFlag.setNameAndValue("RecalFlag", "N");
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		tLLCaseSchema.setCaseNo(caseNo);
		LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();

		if (StringUtil.isNull(polno, contno)) {
			this.tLLClaimPolicySchema = new LLClaimPolicySchema();
			tLLClaimPolicySchema.setRgtNo(rgtno);
			tLLClaimPolicySchema.setContNo(contno);
			tLLClaimPolicySchema.setRiskCode(tLLCalPaySchema.getriskcode());
			tLLClaimPolicySchema.setCaseNo(caseNo);
			tLLClaimPolicySchema.setGetDutyKind(tLLCalPaySchema.getGetDutyKind());
			tLLClaimPolicySchema.setPolNo(polno);
			tLLClaimPolicySet.add(tLLClaimPolicySchema);
		} else {
			this.content = "获取保单号失败";
			return false;
		}
		ClaimCalUI tClaimCalUI = new ClaimCalUI();
		// 准备传输数据 VData
		VData tVData = new VData();
		tG.ManageCom = this.tManageCom;
		tG.Operator = "lipeib";
		try {
			tVData.addElement(tG);
			tVData.addElement(tLLCaseSchema);
			tVData.addElement(tLLClaimPolicySet);
			tVData.addElement(recalFlag);
			tClaimCalUI.submitData(tVData, transact);
		} catch (Exception ex) {
			content = transact + "失败，原因是:" + ex.toString();
			return false;

		}
		if (FlagStr == "") {
			tError = tClaimCalUI.mErrors;
			if (!tError.needDealError()) {
				content = " 计算成功!" + tClaimCalUI.getBackMsg();
				FlagStr = "Succ";
			} else {
				content = " 计算失败，原因是:" + tError.getFirstError();
				return false;
			}
		}
		return true;
	}

	/**
	 * 审批审定(确认)暂时先留着
	 */
	@Deprecated
	public boolean claim_SPSD(String tCaseNo) {
		log.info("========================审批审定确认===============================");
		if (!StringUtil.isNull(tCaseNo)) {
			this.content = "审批审定失败，原因：案件号不能为空";
			return false;
		}
		String rgtstateSql = "select rgtstate from llcase  where caseno='" + tCaseNo + "'";
		String rgtstate = mExeSQL.getOneValue(rgtstateSql);
		if (!StringUtil.isNull(rgtstate)) {
			this.content = "查询案件状态失败，请检查案件号：" + tCaseNo;
			return false;
		}
		String operate = "";
		if ("04".equals(rgtstate) || "06".equals(rgtstate) || "10".equals(rgtstate) || "08".equals(rgtstate)) {// 需要复审
			if (!StringUtil.isNull(tRemark1)) {
				content = "请输入审批意见";
				return false;
			}
			operate = "APPROVE|SP";
		} else if ("05".equals(rgtstate)) { // 审批状态
			if (!StringUtil.isNull(tcheckDecision)) {
				content = "请输入审定结论";
				return false;
			}
			if ("2".equals(rgtstate)) { // 不同意审批结论
				if (!StringUtil.isNull(tRemark2)) {
					content = "请输入审定意见";
					return false;
				}
			}
			operate = "APPROVE|SD";
		} else if ("16".equals(rgtstate)) {
			content = "理赔二核中不可审批审定，必须等待二核结束！";
			return false;
		} else {
			content = "案件在当前状态下不能做审批审定";
			return false;
		}
		String LowerSQL = "select 1 from LLCASEPROBLEM where 1=1 and caseno = '" + tCaseNo + "' and state='1' with ur";
		SSRS sqlSSRS = mExeSQL.execSQL(LowerSQL);
		if (sqlSSRS.getMaxRow() > 0) {
			this.content = "问题件在未回复时，案件不能进行审批审定";
			return false;
		}
		String LLClaimDetailSql = "select RgtNo,CaseNo,ClmNo,grpcontno ,RealPay from LLClaimDetail where caseno='"
				+ tCaseNo + "'";
		SSRS LLClaimDetailSqlSSRS = mExeSQL.execSQL(LLClaimDetailSql);
		if (LLClaimDetailSqlSSRS.getMaxRow() < 1) {
			this.content = "未获取到赔付信息，请检查案件号";
			return false;
		}

		CErrors tError = null;
		String FlagStr = "";
		String strRgtNo = LLClaimDetailSqlSSRS.GetText(1, 1); // 立案号
		String strCaseNo = LLClaimDetailSqlSSRS.GetText(1, 2); // 分案号
		String strClmNo = LLClaimDetailSqlSSRS.GetText(1, 3); // 赔案号
		String grpcontno = LLClaimDetailSqlSSRS.GetText(1, 4); // 团体保单号
		String realPay = LLClaimDetailSqlSSRS.GetText(1, 5); // 实赔金额
		String bjSql = "select 1 from lpgrpedoritem where grpcontno='" + grpcontno + "'    and edortype = 'BJ'";
		SSRS bjSSRS = mExeSQL.execSQL(bjSql);
		if (bjSSRS.getMaxRow() > 0) {
			content = "该保单" + grpcontno + "正在进行结余返还操作或者已完成结余返还项目，不能理算确认！";
			System.out.println(content);
			return false;
		}
		String tAccSql =" select distinct accdate from llcaserela a,llsubreport b where a.subrptno=b.subrptno and a.caseno='"
				+ tCaseNo + "'";
		String tAccDate = mExeSQL.getOneValue(tAccSql);
		if (tAccDate == null || tAccDate == "") {
			this.content = "出险日期查询失败";
			return false;
		}
		
		String strDecisionSP = "1"; // 审批结论暂时去掉，没有意义
		String contdealflagSql = "select contdealflag from llcase where caseno='C3605181222000021'";
		String ContDealFlag = mExeSQL.getOneValue(contdealflagSql);
		if (!StringUtil.isNull(ContDealFlag)) {
			ContDealFlag = "0";
		}
		System.out.println("ContDealFlag=========" + ContDealFlag);
		if (!StringUtil.isNull(strRgtNo)) {
			content = "查询立案号为空!";
			return false;
		}
		if (!StringUtil.isNull(strCaseNo)) {
			content = "查询分案号为空!";
			return false;
		}
		if (!StringUtil.isNull(strClmNo)) {
			content = "查询赔案号为空!";
			return false;
		}
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		tLLCaseSchema.setContDealFlag(ContDealFlag);

		LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();

		tLLClaimUWMainSchema.setRgtNo(strRgtNo);
		tLLClaimUWMainSchema.setCaseNo(strCaseNo);
		tLLClaimUWMainSchema.setClmNo(strClmNo);

		tLLClaimUWMainSchema.setcheckDecision1(strDecisionSP);
		tLLClaimUWMainSchema.setRemark1(tRemark1);
		tLLClaimUWMainSchema.setcheckDecision2(tcheckDecision);
		tLLClaimUWMainSchema.setRemark2(tRemark2);

		ClaimUnderwriteUI tClaimUnderwriteUI = new ClaimUnderwriteUI();
		VData tVData = new VData();
		tG.ManageCom = this.tManageCom;
		tG.Operator = "lipeib";
		try {
			tVData.add(tG);
			tVData.add(tLLClaimUWMainSchema);
			tVData.add(tLLCaseSchema);
			tClaimUnderwriteUI.submitData(tVData, operate);
		} catch (Exception ex) {
			content = " 执行失败，原因是:" + ex.toString();
			log.info("========================" + content + "===============================");
			FlagStr = "Fail";
			return false;
		}
		if (FlagStr.equals("")) {
			tError = tClaimUnderwriteUI.mErrors;
			if (!tError.needDealError()) {
				VData tResultData = tClaimUnderwriteUI.getResult();
				String strResult = (String) tResultData.getObjectByObjectName("String", 0);
				content = strResult;
				FlagStr = "Succ";
			} else {
				content = " 执行失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				return false;
			}
		}
		return true;
	}

	/**
	 * 检录确认
	 */
	public boolean checkInput(String caseNo) {
		//保存事件,这块儿暂时不用，可以直接用关联事件号代替保存事件功能
		/*if (!eventInsert(caseNo)) 
			return false;*/
		
		//账单关联保存
		if(!checkSave(caseNo))
			return false;
		CaseCheckUI tCaseCheckUI = new CaseCheckUI();
		CErrors tError = null;
		String transact = "CHECKFINISH";
		String FlagStr = "";
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		tLLCaseSchema.setCaseNo(caseNo);

		VData tVData = new VData();
		VData tVData2 = new VData();
		tVData.addElement(tG);
		tVData.addElement(tLLCaseSchema);
		try {
			if (!tCaseCheckUI.submitData(tVData, transact)) {
				content = tCaseCheckUI.mErrors.getErrContent();
				FlagStr = "Fail";
				System.out.println(content);
				return false;
			}
		} catch (Exception ex) {
			content = transact + "失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			System.out.println(content);
			return false;
		}
		if (FlagStr == "") {
			tError = tCaseCheckUI.mErrors;
			if (!tError.needDealError()) {
				content = "保存成功";
				FlagStr = "Succ";
				//保单关联
				LLCasePolicySet tLLCasePolicySet = new LLCasePolicySet();
				CasePolicyUI tCasePolicyUI = new CasePolicyUI();
				tError = null;
				String transact1 = "INSERT";
				String sql = "select a.RgtNo,a.CustomerNo,a.CustomerName,(case a.CustomerSex when '0' then '男' when '1' then '女' when '2' then '不详' end),a.AccdentDesc,a.handler,b.codename,c.apppeoples from llcase a,ldcode b,llregister c where a.CaseNo='"
						+caseNo+ "' and c.rgtno=a.rgtno and b.codetype='llrgtstate' and b.code=a.rgtstate";
				SSRS sqlSSRS = mExeSQL.execSQL(sql);
				//读取立案明细信息
				String strCustomerNo = sqlSSRS.GetText(1, 2);

				sql = "select riskcode from llcase where caseno ='" + caseNo + "'";
				SSRS sqlSSRS3 = mExeSQL.execSQL(sql);

				String strSQL0 = "select contno,InsuredName,riskcode,amnt,cvalidate,polno from ";
				String wherePart = " where insuredno='" + strCustomerNo + "' and appflag='1' and stateflag<>'4' ";
				if ("1605".equals(sqlSSRS3.GetText(1, 1))) {
					wherePart += " and RiskCode='" + sqlSSRS3.GetText(1, 1) + "'";
				}
				String strSQL = strSQL0 + " lcpol " + wherePart + " union " + strSQL0 + " lbpol " + wherePart;
				SSRS aSSRS = mExeSQL.execSQL(strSQL);

				String strRgtNo = sqlSSRS.GetText(1, 1);
				sql = "select distinct a.ReceiptNo,a.HospitalName,a.FeeDate,a.CaseRelaNo,a.MainFeeNo,a.sumfee,a.HospitalCode from llfeemain a  where a.caseno='"
						+ caseNo + "'";
				SSRS sqlSSRS2 = mExeSQL.execSQL(sql);
				String CaseRelaNo = sqlSSRS2.GetText(1, 4);
				int intLength = aSSRS.MaxRow;
				System.out.println("保存LCPOL的信息");
				System.out.println(intLength);
				for (int i = 0; i < intLength; i++) {
					LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
					tLLCasePolicySchema.setCaseNo(caseNo);
					tLLCasePolicySchema.setRgtNo(strRgtNo);
					System.out.println(aSSRS.GetText(i + 1, 6));
					tLLCasePolicySchema.setPolNo(aSSRS.GetText(i + 1, 6));
					tLLCasePolicySchema.setCaseRelaNo(CaseRelaNo);
					tLLCasePolicySet.add(tLLCasePolicySchema);
				}
				try {
					VData tVData1 = new VData();
					tVData1.addElement(strCustomerNo);
					tVData1.addElement(caseNo);
					tVData1.addElement(tLLCasePolicySet);
					tVData1.addElement(tG);
					if (tCasePolicyUI.submitData(tVData1, transact1)) {
						ClaimCalBL tClaimCalBL = new ClaimCalBL();
						try {
							tVData2.addElement(tLLCaseSchema);
							tVData2.addElement(tG);
							System.out.println("......tClaimCalBL");
							if (!tClaimCalBL.submitData(tVData2, "autocal")) {
								content = "ClaimCalBL===出错:" + tClaimCalBL.mErrors.getErrContent();
								System.out.println(content);
								return false;
							}
						} catch (Exception ex) {
							content = transact1 + "失败，原因是:" + ex.toString();
							FlagStr = "Fail";
							System.out.println(content);
							return false;
						}
					} else {
						content = tCasePolicyUI.mErrors.getContent();
						System.out.println(content);
						return false;
					}
				} catch (Exception ex) {
					content = transact1 + "失败，原因是:" + ex.toString();
					FlagStr = "Fail";
					System.out.println(content);
					return false;
				}
				if (FlagStr == "") {
					tError = tCasePolicyUI.mErrors;
					if (!tError.needDealError()) {
						content = " 保存成功";
						FlagStr = "Succ";
						System.out.println(content);
					} else {
						content = " 保存失败，原因是:" + tError.getFirstError();
						FlagStr = "Fail";
						System.out.println(content);
						return false;
					}
				}
			} else {
				content = "保存失败，原因是:" + tError.getFirstError();
				System.out.println(content);
				FlagStr = "Fail";
				return false;
			}
		}
		return true;
	}

	/**
	 * 账单关联保存
	 */
	public boolean checkSave(String caseNo) {
		
		AllCaseInfoUI tAllCaseInfoUI = new AllCaseInfoUI();
		CErrors tError = null;
		String FlagStr = "";
		String transact = "INSERT";
		String sql = "select customerNo,rgtno,caseRelaNo,MainFeeNo,CustomerName from llfeemain where caseno='"
	     + caseNo + "'";
		SSRS sqlSSRS = mExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() < 1) {
			this.content = "未查询到账单信息，请确认案件：" + caseNo;
			return false;
		}
		String CustomerNo = sqlSSRS.GetText(1, 1);
		String CaseRelaNo = sqlSSRS.GetText(1, 3);
		String RgtNo = sqlSSRS.GetText(1, 2);
		String feeNo = sqlSSRS.GetText(1, 4);
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
		LLCaseInfoSet tLLCaseInfoSet = new LLCaseInfoSet();
		LLOperationSet tLLOperationSet = new LLOperationSet();
		LLOtherFactorSet tLLOtherFactorSet = new LLOtherFactorSet();
		LLAccidentSet tLLAccidentSet = new LLAccidentSet();
		LLDisabilitySet tLLDisabilitySet = new LLDisabilitySet();
		tLLCaseSchema.setCaseNo(caseNo);
		tLLCaseSchema.setRgtNo(RgtNo);
		tG.Operator = "lipeia";
		tG.ManageCom = this.tManageCom;
		VData tVData = new VData();
		tVData.addElement(tG);
		mLLCaseInfoSchema.setCaseNo(caseNo);
		String LLSubReportSql = " SELECT a.SubRptNo, AccDate, AccPlace, InHospitalDate, OutHospitalDate\r\n"
				+ ", AccDesc\r\n" + "FROM LLSubReport a, (\r\n" + "		SELECT subrptno\r\n"
				+ "	FROM llcaserela\r\n" + " WHERE CaseNo = '" + caseNo + "'\r\n" + " ) b\r\n"
				+ "WHERE a.customerno = '" + CustomerNo + "'\r\n" + " AND a.SubRptNo = b.subrptno";
		SSRS LLSubReportSQL = mExeSQL.execSQL(LLSubReportSql);
		if (LLSubReportSQL.MaxRow < 1) {
			this.content = "查询事件信息失败";
			return false;
		}
		try {
			tVData.addElement(mLLSubReportSet);
			tVData.addElement(CaseRelaNo);
			tVData.addElement(tLLCaseSchema);
			tVData.addElement(mLLCaseCureSet);
			tVData.addElement(tLLCaseInfoSet);
			tVData.addElement(tLLOtherFactorSet);
			tVData.addElement(tLLOperationSet);
			tVData.addElement(tLLAccidentSet);
			tVData.addElement(tLLDisabilitySet);
			LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
			LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
			tLLFeeMainSchema.setCaseNo(caseNo);
			tLLFeeMainSchema.setMainFeeNo(feeNo);
			tLLFeeMainSchema.setCaseRelaNo(CaseRelaNo);
			tLLFeeMainSet.add(tLLFeeMainSchema);
			tVData.addElement(tLLFeeMainSet);
			tAllCaseInfoUI.submitData(tVData, transact);
		} catch (Exception ex) {
			content = transact + "失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			System.out.println(content);
			return false;
		}
		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tAllCaseInfoUI.mErrors;
			if (!tError.needDealError()) {
				content = "保存成功";
				System.out.println(content);
				FlagStr = "Succ";
			} else {
				content = "保存失败，原因是:" + tError.getFirstError();
				System.out.println(content);
				FlagStr = "Fail";
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 拼接返回报文
	 */
	@SuppressWarnings("unused")
	public void rerutnXml() {
		StringWriter stringWriter = new StringWriter();
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("MedicalAdjustment_Response");
		document.setRootElement(tDateSet);
		// 返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Date date = new Date();
		tSendDate.addText(simDate.format(date));
		Element tSendTime = tMsgResHead.addElement("SendTime");
		tSendTime.addText(simTime.format(date));
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		tSendOperator.addText(this.tSendOperator);
		Element tMsgType = tMsgResHead.addElement("MsgType");
		Element tRownum = tMsgResHead.addElement("Rownum");
		// 返回报文体样式
		Element tBody = tDateSet.addElement("Body");
		Element tManageCom = tBody.addElement("ManageCom");
		tManageCom.addText(this.tManageCom);
		Element tCaseNo = tBody.addElement("CaseNo");
		tCaseNo .addText(this.tCaseNo);
		Element tContent = tBody.addElement("Content");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		tContent.addText(this.content);
		tSuccessFlag.addText(this.SuccessFlag);
		// 创建写文件方法
		XMLWriter xmlWriter = new XMLWriter(stringWriter);
		// 写入文件
		try {
			xmlWriter.write(document);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		// 关闭
		try {
			xmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		 this.responseXml = stringWriter.toString();
		System.out.println(responseXml);
	}
	
	/**
	 * 保存事件
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private boolean eventInsert(String strCaseNo) {
		if (mLLSubReportSet == null) {
			return false;
		}
		LLSubReportSchema mLLSub = mLLSubReportSet.get(1);
		if (StringUtil.isNull(mLLSub.getAccProvinceCode(), mLLSub.getAccCityCode(), mLLSub.getAccCountyCode(),
				mLLSub.getAccDate(), mLLSub.getInHospitalDate(), mLLSub.getOutHospitalDate())) {
			content = "请检查事件信息是否填写完整";
			return false;
		}
		LLSubReportSet tLLSubReporSet = new LLSubReportSet();
		// 声明VData
		VData tVData = new VData();
		// 声明后台传送对象
		EventSaveBL tEventSaveBL = new EventSaveBL();
		// 输出参数
		String sql = "select customerNo,rgtno,caseRelaNo ,MainFeeNo ,CustomerName from  llfeemain where caseno='"
				+tCaseNo+ "'";
		SSRS sqlSSRS = mExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() < 1) {
			this.content = "未查询到账单信息，请确认案件：" + tCaseNo;
			return false;
		}
		String CustomerNo = sqlSSRS.GetText(1, 1);
		
		CErrors tError = null;
		String FlagStr = "";
		String CaseNo = strCaseNo;
		String CustomerName = sqlSSRS.GetText(1, 5);
		mLLSub.setSubRptNo("");
		mLLSub.setCustomerNo(CustomerNo);
		mLLSub.setCustomerName(CustomerName);

		tLLSubReporSet.add(mLLSub);
		
		TransferData CN = new TransferData();
		CN.setNameAndValue("CaseNo", CaseNo);
		try {
			// 准备传输数据 VData
			// VData传送
			System.out.println("<--Star Submit VData-->");
			tVData.add(tLLSubReporSet);
			tVData.add(CN);
			tG.Operator = "lipeia";
			tG.ManageCom = this.tManageCom;
			tVData.add(tG);
			System.out.println("<--Into tEventSaveBL-->");
			tEventSaveBL.submitData(tVData, "INSERT");
		} catch (Exception ex) {
			content = "保存失败，原因是: " + ex.toString();
			FlagStr = "Fail";
			return false;
		}
		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if ("".equals(FlagStr)) {
			tError = tEventSaveBL.mErrors;
			if (!tError.needDealError()) {
				content = " 保存成功! ";
				FlagStr = "Success";
				tVData.clear();
			} else {
				content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * 个案回退
	 */
	private boolean caseBack(String caseNo) {
		if (!StringUtil.isNull(caseNo)) {
			return false;
		}
		String rgtstateSql = "select rgtstate from  llcase where caseno='" + caseNo + "'";
		String rgtstate = mExeSQL.getOneValue(rgtstateSql);
		if (!StringUtil.isNull(rgtstate) || "11".equals(rgtstate) || "12".equals(rgtstate) || "13".equals(rgtstate)
				|| "14".equals(rgtstate) || "07".equals(rgtstate) || "16".equals(rgtstate)) {
			return false;
		}
		String lltjSql = "select 1 from llhospcase where apptranno like 'TJ%' and hospitcode ='00000000' and caseno ='"
				+ caseNo + "'";
		SSRS SSRS = mExeSQL.execSQL(lltjSql);
		if (SSRS.MaxRow > 0) {
			errorV="天津居民意外案件不能进行回退！";
			return false;
		}
		String sqlUpdate = "update llcase set handler='lipeia' where CaseNo = '" + caseNo + "'";
		if (!mExeSQL.execUpdateSQL(sqlUpdate)) {
			errorV="回退更新数据失败";
			return false;
		}
		String backState = "01";// 回退状态01：受理 04:理算 05：审批
		String llusercode = "lipeia";// 回退理赔员
		String returnReason = "1";// 回退原因
									// 1.受理登陆错误2.账单录入错误，3：疾病录入错误，4理赔结果错误，5：补充调查。6：其他
		tG.Operator = "lipeia";
		 tG.ManageCom=this.tManageCom;
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
		LLCaseReturnUI tLLCaseReturnUI = new LLCaseReturnUI();
		// 输出参数
		CErrors tError = null;
		String FlagStr = "";
		String Content = "";
		String transact = "INSERT||MAIN";
		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录

		tLLCaseSchema.setCaseNo(caseNo);
		tLLCaseSchema.setHandler(llusercode);
		tLLCaseSchema.setRgtState(backState);

		tLLCaseBackSchema.setCaseNo(caseNo);
		tLLCaseBackSchema.setBeforState(rgtstate);
		tLLCaseBackSchema.setAfterState(backState);
		tLLCaseBackSchema.setNHandler(llusercode);
		tLLCaseBackSchema.setReason(returnReason);
		tLLCaseBackSchema.setRemark("");
		tLLCaseBackSchema.setMngCom(this.tManageCom);
		try {
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add(tLLCaseSchema);
			tVData.add(tLLCaseBackSchema);
			tVData.add(tG);
			tLLCaseReturnUI.submitData(tVData, transact);
		} catch (Exception ex) {
			errorV= "回退案件失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			return false;
		}
		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tLLCaseReturnUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				errorV= "";
				FlagStr = "Success";
			} else {
				Content = " 回退案件败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				errorV=" 回退案件失败，原因是:" + tError.getFirstError();
				return false;
			}
		}
		System.out.println("===========================" + Content + "===============================");
		return true;
	}
	
	/**
	 * 获取XMLSchema字符串
	 */
	private String getxsd() {
		String  strxsd="<?xml version=\"1.0\" encoding=\"GBK\"?>\r\n" + 
				"\r\n" + 
				"<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">  \r\n" + 
				"  <xs:element name=\"MedicalAdjustment_Request\"> \r\n" + 
				"    <xs:complexType> \r\n" + 
				"      <xs:sequence> \r\n" + 
				"        <xs:element name=\"head\"> \r\n" + 
				"          <xs:complexType> \r\n" + 
				"            <xs:sequence> \r\n" + 
				"              <xs:element name=\"BatchNo\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendDate\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendTime\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"BranchCode\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendOperator\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"MsgType\" type=\"xs:string\"/> \r\n" + 
				"            </xs:sequence> \r\n" + 
				"          </xs:complexType> \r\n" + 
				"        </xs:element>  \r\n" + 
				"        <xs:element name=\"body\"> \r\n" + 
				"          <xs:complexType> \r\n" + 
				"            <xs:sequence> \r\n" + 
				"              <xs:element name=\"ManageCom\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"CaseNo\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"correction\"> \r\n" + 
				"                <xs:complexType> \r\n" + 
				"                  <xs:sequence> \r\n" + 
				"                    <xs:element name=\"InsuredStat\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"SecurityNo\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"CompSecuNo\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"HospitalName\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"HospitalCode\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"FeeAtti\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"FeeType\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"FeeAffixType\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"ReceiptType\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"ReceiptNo\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"inpatientNo\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"FeeDate\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"HospStartDate\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"HospEndDate\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"IssueUnit\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"MedicareType\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"FeeItem\" minOccurs=\"1\" maxOccurs=\"50\"> \r\n" + 
				"                      <xs:complexType> \r\n" + 
				"                        <xs:sequence> \r\n" + 
				"                          <xs:element name=\"FeeType\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"Fee\" type=\"xs:string\"/> \r\n" + 
				"                        </xs:sequence> \r\n" + 
				"                      </xs:complexType> \r\n" + 
				"                    </xs:element>  \r\n" + 
				"                    <xs:element name=\"ContRemark\" type=\"xs:string\"/>  \r\n" + 
				"                    <xs:element name=\"item\"> \r\n" + 
				"                      <xs:complexType> \r\n" + 
				"                        <xs:sequence> \r\n" + 
				"                          <xs:element name=\"AccProvinceCode\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"AccCityCode\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"AccCountyCode\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"SubDate\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"SubAddress\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"InHospitalDate\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"OutHospitalDate\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"SubMsg\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"AccType\" type=\"xs:string\"/> \r\n" + 
				"                        </xs:sequence> \r\n" + 
				"                      </xs:complexType> \r\n" + 
				"                    </xs:element>  \r\n" + 
				"                    <xs:element name=\"DisItem\"> \r\n" + 
				"                      <xs:complexType> \r\n" + 
				"                        <xs:sequence> \r\n" + 
				"                          <xs:element name=\"DiseaseName\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"DiseaseCode\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"LDCODE\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"LDNAME\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"HospitalCode\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"DoctorName\" type=\"xs:string\"/>  \r\n" + 
				"                          <xs:element name=\"OperationFlag\" type=\"xs:string\"/> \r\n" + 
				"                        </xs:sequence> \r\n" + 
				"                      </xs:complexType> \r\n" + 
				"                    </xs:element> \r\n" + 
				"                  </xs:sequence> \r\n" + 
				"                </xs:complexType> \r\n" + 
				"              </xs:element>  \r\n" + 
				"              <xs:element name=\"CalPay\" minOccurs=\"1\" maxOccurs=\"5000\"> \r\n" + 
				"                <xs:complexType> \r\n" + 
				"                  <xs:sequence> \r\n" + 
				"                    <xs:element name=\"item\" minOccurs=\"1\" maxOccurs=\"5000\"> \r\n" + 
				"                      <xs:complexType> \r\n" + 
				"                        <xs:sequence> \r\n" + 
				"                          <xs:element name=\"Riskcode\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"Getdutycode\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"GetDutyKind\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"GiveType\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"GiveReason\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"GiveTypeDesc\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"GiveReasonDesc\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"IsRate\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"DeclineAmnt\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"ApproveAmnt\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"EasyCase\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"DeclineReason\" type=\"xs:string\" minOccurs=\"0\"/> \r\n" + 
				"                        </xs:sequence> \r\n" + 
				"                      </xs:complexType> \r\n" + 
				"                    </xs:element> \r\n" + 
				"                  </xs:sequence> \r\n" + 
				"                </xs:complexType> \r\n" + 
				"              </xs:element> \r\n" + 
				"            </xs:sequence> \r\n" + 
				"          </xs:complexType> \r\n" + 
				"        </xs:element> \r\n" + 
				"      </xs:sequence> \r\n" + 
				"    </xs:complexType> \r\n" + 
				"  </xs:element> \r\n" + 
				"</xs:schema>\r\n" + 
				"";
		return strxsd;
	}
}