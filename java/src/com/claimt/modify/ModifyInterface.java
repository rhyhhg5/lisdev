package com.claimt.modify;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

/**
 * 付费信息修改/团体付费信息修改调用
 * 
 * @author 李非凡 2018/11/12
 */
public class ModifyInterface {
	/**
	 * 调用此方法进行解析请求报文并返回响应报文
	 * 
	 * @param xml
	 * @return
	 * @throws DocumentException
	 */
	public String service(String xml) throws DocumentException {

		Document doc = DocumentHelper.parseText(xml);

		Element root = doc.getRootElement();

		Element tBody = root.element("body");

		String tPayMethod = tBody.elementText("PayMethod");

		if ("".equals(tPayMethod)) {//给付方式不能为空！
			System.out.println("给付方式不可为空！");
			return null;
		}

		if ("1".equals(tPayMethod)) {//如果给付方式为1-个人给付，则走付费信息修改DealAccModify
			DealAccModify dealAccModify = new DealAccModify();
			dealAccModify.parseXml(xml);
			return dealAccModify.getResponseXml();
		} else if ("2".equals(tPayMethod) || "3".equals(tPayMethod) || "4".equals(tPayMethod)) {
			//如果给付方式为2,3,4，则走团体付费信息修改DealGrpAccModify
			DealGrpAccModify dealGrpAccModify = new DealGrpAccModify();
			dealGrpAccModify.parseXml(xml);
			return dealGrpAccModify.getResponseXml();
		} else {//给付方式只有1,2,3,4这四种
			System.out.println("给付方式只有1，2，3，4四种类型！");
			return null;
		}
	}
}
