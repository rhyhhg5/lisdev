package com.claimt.modify;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.llcase.LLAccModifyUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLCaseExtSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSendMsgSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 理赔案件-理赔处理-付费信息修改
 * 
 * @author 李非凡
 * @version 1.0 2018-11-02
 */
public class DealAccModify {
	private String content;// 错误信息
	private String responseXml;// 返回报文
	
	// 报文头字段
	private String batchNo; // 批次号
	private String sendDate; // 发送日期
	private String sendTime; // 发送时间
	private String branchCode; // 交易编码
	private String sendOperator; // 交易人员
	private String msgType; // 固定值

	// 报文体字段
	private String manageCom;
	private String handler;
	private String caseNo;// polNo与caseNo均为理赔号，otherNo代表理赔号

	private String actuGetNo;
	private String getMoney;

	private String drawerIDType;
	private String payMode;
	private String drawer;
	private String drawerID;
	private String bankCode;
	private String bankAccNo;
	private String accName;
	
	private String bankOnTheWayFlag;//银行在途标志
	private String cansendbank;//允许发送银行标记，0允许发盘，1回盘失败，2客户信息待调整
	private String cansendflag;//支持银行接口标记，0不支持，1支持
	
	private LLSendMsgSchema tLLSendMsgSchema = new LLSendMsgSchema();
	private LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	private LJAGetSchema tLJAGetSchema = new LJAGetSchema();
	private LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
	private CErrors tError = null;
	private LLAccModifyUI tLLAccModifyUI = new LLAccModifyUI();
	private GlobalInput tG = new GlobalInput();
	private MMap tMap = new MMap();
	private String FlagStr = "";
	private VData tVData = new VData();
	
	//解析请求报文,将报文信息保存到本类
	public boolean getinputdate(String xmlMsg){
		if (!StringUtil.StringNull(xmlMsg)) {
			content = "请求报文不能为空！";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}

		SAXReader reader = null;
		Document doc = null;
		File f = null;
		try {
			if (".xml".equals(xmlMsg.substring(xmlMsg.length() - 4))) {
				System.out.println("...................xmlMsg为报文路径.......................");
				reader = new SAXReader();
				f = new File(xmlMsg);
				reader.setEncoding("GBK");
				doc = reader.read(f);
			} else {
				System.out.println(".........................xmlMsg为报文内容....................................");
				doc = DocumentHelper.parseText(xmlMsg);
				tMap.put("requestXML", xmlMsg.trim());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		//解析报文
		Element root = doc.getRootElement();
		//解析报文头
		Element tHead = root.element("head");
		batchNo = tHead.elementText("BatchNo");
		sendDate = tHead.elementText("SendDate");
		sendTime = tHead.elementText("SendTime");
		branchCode = tHead.elementText("BranchCode");
		sendOperator = tHead.elementText("SendOperator");
		msgType = tHead.elementText("MsgType");
		
		tLLSendMsgSchema.setBatchNo(batchNo);
		tLLSendMsgSchema.setSendDate(sendDate);
		tLLSendMsgSchema.setSendTime(sendTime);
		tLLSendMsgSchema.setBranchCode(branchCode);
		tLLSendMsgSchema.setSendOperator(sendOperator);
		tLLSendMsgSchema.setMsgType(msgType);
		System.out.println("批次号：" + batchNo + ",发送日期:" + sendDate);
		
		//解析报文体
		Element tBody = root.element("body");
		manageCom = tBody.elementText("ManageCom");
		handler = tBody.elementText("Handler");
//		handler = "lipeia";//操作用户默认为：lipeia
		caseNo = tBody.elementText("CaseNo");
		
		Element tPerson = tBody.element("Person");
		drawerIDType = tPerson.elementText("DrawerIDType");
		payMode = tPerson.elementText("PayMode");
		drawer = tPerson.elementText("Drawer");
		drawerID = tPerson.elementText("DrawerID");
		bankCode = tPerson.elementText("BankCode");
		bankAccNo = tPerson.elementText("BankAccNo");
		accName = tPerson.elementText("AccName");
		
		return true;
	}
	
	//对已经保存到本类的信息进行校验，校验通过后查询一些必须一起传递到后台的数据
    //校验数据，获取页面自动填写数据
	public boolean checkdate(){
		String sql;
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		
		if ("".equals(caseNo)) {
			content = "理赔号不能为空！";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		if ("".equals(drawer)) {
			content = "请录入领取人！";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}

		if (payMode != "1" && payMode != "2") {

			if ("".equals(bankCode)) {
				content = "请选择对方开户银行！";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}

			if ("".equals(bankAccNo)) {
				content = "请录入对方银行帐号！";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}

			if ("".equals(accName)) {
				content = "请录入对方银行帐户名！";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}

		}
		//传递的银行有可能是数据库没有的新银行，所以校验支付接口不合理。
		try{
			sql = "select cansendflag from ldbank where bankcode='"+bankCode+"'";
			tSSRS = tExeSQL.execSQL(sql);	
			cansendflag = tSSRS.GetText(1, 1);
			if(!"1".equals(cansendflag)){
				content = "该银行不支持转账接口，请更换银行";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
			
		}catch(Exception e) {
			content="查询失败，原因是:"+e.toString();
			System.out.println("没有对应银行信息信息");
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		
		
		 //根据理赔案件号查询给付凭证号，给付金额
		 
		try {
			sql = "select managecom,actugetno,sumgetmoney,paymode,bankonthewayflag,cansendbank from ljaget where otherno = '"
		+caseNo+"'and paymode='4' and othernotype='5' and confdate is null order by managecom with ur";
			tSSRS = tExeSQL.execSQL(sql);
			System.out.println(sql);
			//根据caseno理赔号查询出managecom系统机构actugetno给付凭证号跟getmoney支付金额
			System.out.println("查询出数据条数："+tSSRS.getMaxRow());
			if(tSSRS.getMaxRow()<1){
				content = "没有该案件号信息";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
			String mmanageCom = tSSRS.GetText(1, 1);
			actuGetNo = tSSRS.GetText(1, 2);
			getMoney = tSSRS.GetText(1, 3);
			//payMode = tSSRS.GetText(1, 4);
			bankOnTheWayFlag = tSSRS.GetText(1, 5);
			cansendbank = tSSRS.GetText(1, 6);
		//	System.out.println("------"+cansendbank);
			if (!manageCom.equals(mmanageCom)){
				content = "传递机构与本案件机构不符合";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
			
			if ("".equals(actuGetNo)) {
				content = "给付凭证号不能为空！";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
			if ("1".equals(bankOnTheWayFlag)) {
				content = "处于在途状态，无法进行修改";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
				
		} catch (Exception e) {
			content="查询失败，原因是:"+e.toString();
			System.out.println("案件号没有付费信息");
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		return true;
	}

	//修改：对应页面operate动作的modify值
	public boolean modify(){
		handler = "lipeia";//操作用户默认为：lipeia
	//	tLLCaseSchema.setRigister(handler);
		tG.Operator = handler;
		String strOperate = "MODIFY";
		// 动作为修改。
				tLJAGetSchema.setActuGetNo(actuGetNo);
				tLJAGetSchema.setOtherNo(caseNo);
				tLJAGetSchema.setPayMode(payMode);
				tLJAGetSchema.setSumGetMoney(getMoney);
				tLJAGetSchema.setDrawer(drawer);
				tLJAGetSchema.setDrawerID(drawerID);
				tLJAGetSchema.setBankCode(bankCode);
				tLJAGetSchema.setBankAccNo(bankAccNo);
				tLJAGetSchema.setAccName(accName);
				//对应页面save中修改操作将cansendbank改为M，此时案件状态变更为待确认状态
				tLJAGetSchema.setCanSendBank("M");
				//对应页面save中确认操作，在bl中确认炒作将值变成0，此时案件状态变更为待提盘状态
			 //	tLJAGetSchema.setCanSendBank("0");
				System.out.println("ljaget表set完毕");
				tLLCaseSchema.setCaseNo(caseNo);
				tLLCaseSchema.setGetMode(payMode);
				tLLCaseSchema.setCaseGetMode(payMode);
				tLLCaseSchema.setBankCode(bankCode);
				tLLCaseSchema.setBankAccNo(bankAccNo);
				tLLCaseSchema.setAccName(accName);
				System.out.println("llcase表set完毕");
				// #3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
				tLLCaseExtSchema.setDrawerIDType(drawerIDType);
				tLLCaseExtSchema.setDrawerID(drawerID);
				// #3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
				System.out.println("llcaseext表set完毕");
				try {
					tVData.add(tLLCaseSchema);
					tVData.add(tLLCaseExtSchema);
					tVData.add(tLJAGetSchema);
					tVData.add(tG);
					tLLAccModifyUI.submitData(tVData, strOperate);
					System.out.println("ui层传输数据完毕");
				} catch (Exception ex) {
					content = "保存失败，原因是:" + ex.toString();
					System.out.println("aaaa" + ex.toString());
					FlagStr = "Fail";
					System.out.println(content + FlagStr);
					returnXml("0", tLLSendMsgSchema);
					return false;
				}
				if (FlagStr == "") {
					tError = tLLAccModifyUI.mErrors;
					if (!tError.needDealError()) {
						content = "保存成功！";
						FlagStr = "Succ";
						System.out.println(content + FlagStr);
						tVData.clear();
						tVData = tLLAccModifyUI.getResult();
					} else {
						content = "保存失败，原因是:" + tError.getLastError();
						FlagStr = "Fail";
						System.out.println(content + FlagStr);
						returnXml("0", tLLSendMsgSchema);
						return false;
					}
				}
		
		System.out.println("修改成功");
		return true;
	}
	
	//确认：对应页面operate动作为confirm值
	public boolean confirm(){
		

		//创建一个时间处理，给出time为当前时间前三分钟
				SimpleDateFormat Date=new SimpleDateFormat("HH:mm:ss");
				System.out.println(Date.format(new Date()));
				Calendar oldtime = Calendar.getInstance();
				oldtime.add(Calendar.MINUTE,-3);
				Date beforetime = oldtime.getTime();
				String time = Date.format(beforetime);
				System.out.println(time);
		//执行将锁表的上一次操作时间提前三分钟，执行将lipeia上级设置为lipeia		
				try{
				String sql = new String();
				ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = new SSRS();
				sql= "update locktable set maketime='"+time+"' where nolimit='"+actuGetNo+"'";
				//tSSRS = tExeSQL.execSQL(sql);
				tExeSQL.execUpdateSQL(sql);
				System.out.println(sql);
				sql="update llsocialclaimuser set upusercode='lipeia' where  usercode ='lipeia'";
				//tSSRS = tExeSQL.execSQL(sql);
				tExeSQL.execUpdateSQL(sql);
				System.out.println(sql);
				}catch (Exception e) {
					content="查询失败，原因是:"+e.toString();
					System.out.println(content);
					returnXml("0", tLLSendMsgSchema);
					return false;
				}
		
		
		
		handler = "lipeia";//操作用户默认为：lipeia
		//tLLCaseSchema.setRigister(handler);
		tG.Operator = handler;
		String strOperate = "CONFIRM";
		tLJAGetSchema.setActuGetNo(actuGetNo);
	  	tLJAGetSchema.setCanSendBank("0");
	  	//页面问题，save页面中将查询到的理赔号polno传给了salechn1销售渠道,然后在bl中又将salechen1传递给了批次号Rgtno
	  	//页面查询的理赔号polno对应报文中caseno
	  	tLJAGetSchema.setSaleChnl(caseNo);
	  	//tLJAGetSchema.setOtherNo(caseNo);
	  	//对应页面save中从前天取得grpflag判断是团体还是个人，本接口由报文中paymethod为1判断为个人给付，运行的此类，团体不运行此类
	  	//此serialno数据传递到bl中并未使用
	  	tLJAGetSchema.setSerialNo("Ge");
	    try
		   {
			   tVData.add(tLJAGetSchema);		   
			   tVData.add(tG);		   
			   tLLAccModifyUI.submitData(tVData,strOperate);
		
		   }
		 catch(Exception ex)
		    {
			 content = "保存失败，原因是:" + ex.toString();
				System.out.println("aaaa" + ex.toString());
				FlagStr = "Fail";
				System.out.println(content + FlagStr);
				returnXml("0", tLLSendMsgSchema);
				return false;
		    }
	    if (FlagStr=="")
	    {
		    tError = tLLAccModifyUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		      content ="确认成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tLLAccModifyUI.getResult();
		    }
		    else                                                                           
		    {
		    	content = "确认失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
	     }	      
		
	//	System.out.println("确认成功");
		return true;
	}
	
	public boolean parseXml(String xmlMsg) {
	System.out.println("版本号2018-12-19 13：30");
		//解析请求报文，将数据保存到本类中,
		if(!getinputdate(xmlMsg)){
			content="保存到本类数据失败:"+content;
			System.out.println("解析报文，保存数据到本类失败");
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		//校验数据
		if(!checkdate()){
			content="校验数据失败，传入的数据不符合要求:"+content;
			System.out.println("校验数据失败，传入的数据不符合要求");
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		//修改处理MODIFY,包括写死hardler操作
		if(!modify()){
			content="修改失败:"+content;
			System.out.println("修改失败");
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		//验证是否修改成功
//		String sql = "select cansendbank from ljaget where otherno ='"+caseNo+"'";
//		ExeSQL tExeSQL = new ExeSQL();
//		SSRS tSSRS = new SSRS();
//		tSSRS = tExeSQL.execSQL(sql);	
//		String tcansenbank = tSSRS.GetText(1, 1);
//		System.out.println("----------"+tcansenbank);
		
		//确定处理CONFIRM,包括写死hardler操作
		if(!confirm()){
			content="确认失败:"+content;
			System.out.println("确认失败");
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		 
	    //验证是否修改成功
//		tSSRS = tExeSQL.execSQL(sql);	
//	    tcansenbank = tSSRS.GetText(1, 1);
//		System.out.println("----------"+tcansenbank);
		
		
		System.out.println("===================成功=================");
		returnXml("1", tLLSendMsgSchema);
		return true;
	}

	/**
	 * 返回响应报文
	 * 
	 * @param str
	 * @param tLLSendMsgSchema
	 * @return String
	 */
	private String returnXml(String str, LLSendMsgSchema tLLSendMsgSchema) {
		StringWriter stringWriter = new StringWriter();
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("AccModify_Response");
		document.setRootElement(tDateSet);

		SimpleDateFormat simDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat simTime = new SimpleDateFormat("HH:mm:ss");

		// 返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Date date = new Date();
		tSendDate.addText(simDate.format(date));
		Element tSendTime = tMsgResHead.addElement("SendTime");
		tSendTime.addText(simTime.format(date));
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		tSendOperator.addText(this.handler);
		Element tMsgType = tMsgResHead.addElement("MsgType");
		Element tRownum = tMsgResHead.addElement("Rownum");
		// 返回报文体样式
		Element tBody = tDateSet.addElement("Body");
		Element tRgtNo = tBody.addElement("RgtNo");
		Element tContent = tBody.addElement("Content");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		tSuccessFlag.addText(str);
		if ("1".equals(str)) {
			tContent.addText("successful操作成功");
		} else {
			tContent.addText(content);
		}
		// 创建写文件方法
		XMLWriter xmlWriter = new XMLWriter(stringWriter);
		// 写入文件
		try {
			xmlWriter.write(document);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		// 关闭
		try {
			xmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		this.responseXml = stringWriter.toString();

		return responseXml;
	}

	/**
	 * 获取返回报文
	 * 
	 * @return
	 */
	public String getResponseXml() {

		return responseXml;
	}

}
