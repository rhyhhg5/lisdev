package com.claimt.modify;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.llcase.LLGrpAccModifyUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSendMsgSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 理赔案件-团体理赔处理-团体付费信息修改
 * 
 * @author 李非凡
 * @version 1.0 2018-10-10
 */
public class DealGrpAccModify {

	private String content;// 错误信息
	private String responseXml;// 返回报文
	// 报文头字段
	private String batchNo; // 批次号
	private String sendDate; // 发送日期
	private String sendTime; // 发送时间
	private String branchCode; // 交易编码
	private String sendOperator; // 交易人员
	private String msgType; // 固定值
	// 报文体字段
	private String manageCom;
	private String handler;
	private String rgtNo; // 批次号
	
	private String actuGetNo; // 给付凭证号
	private String getMoney; // 给付金额
	
	private String payMode;// 付费方式
	private String drawer;// 领取人
	private String drawerID;// 领取人身份证号
	private String bankCode;// 对方开户银行
	private String bankAccNo;// 对方银行账号
	private String accName;// 对方银行账户名
	
	private LLSendMsgSchema tLLSendMsgSchema = new LLSendMsgSchema();
	private MMap mMap = new MMap();

	private LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
	private LJAGetSchema tLJAGetSchema = new LJAGetSchema();
	private CErrors tError = null;
	private LLGrpAccModifyUI tLLGrpAccModifyUI = new LLGrpAccModifyUI();
	private GlobalInput tG = new GlobalInput();
	private VData tVData = new VData();

	//解析报文，将数据保存到本类
	public boolean getinputdate(String xmlMsg){
		
		if (!StringUtil.StringNull(xmlMsg)) {
			content = "请求报文不能为空！";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		SAXReader reader = null;
		Document doc =null;
		File f = null;
		try {
			//
			if (".xml".equals(xmlMsg.substring(xmlMsg.length() - 4))) {
				System.out.println("...................xmlMsg为报文路径.......................");
				reader = new SAXReader();
				f = new File(xmlMsg);
				reader.setEncoding("GBK");
				doc = reader.read(f);
			} else {
				System.out.println(".........................xmlMsg为报文内容....................................");
				doc = DocumentHelper.parseText(xmlMsg);
				mMap.put("requestXML", xmlMsg.trim());
			}

			Element root = doc.getRootElement();
			/*
			 * dom4j获取报文头，解析报文头 注：所有请求报文头是一样的
			 */
			Element tHead = root.element("head");
			batchNo = tHead.elementText("BatchNo");
			sendDate = tHead.elementText("SendDate");
			sendTime = tHead.elementText("SendTime");
			branchCode = tHead.elementText("BranchCode");
			sendOperator = tHead.elementText("SendOperator");
			msgType = tHead.elementText("MsgType");
			/*
			 * 报文头不能有空字段，否则会解析失败
			 */
			if (!(StringUtil.StringNull(batchNo) && StringUtil.StringNull(sendDate) && StringUtil.StringNull(sendTime)
					&& StringUtil.StringNull(branchCode) && StringUtil.StringNull(sendOperator)
					&& StringUtil.StringNull(msgType))) {
				content = "报文解析失败，报文头信息缺失！";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
			tLLSendMsgSchema.setBatchNo(batchNo);
			tLLSendMsgSchema.setSendDate(sendDate);
			tLLSendMsgSchema.setSendTime(sendTime);
			tLLSendMsgSchema.setBranchCode(branchCode);
			tLLSendMsgSchema.setSendOperator(sendOperator);
			tLLSendMsgSchema.setMsgType(msgType);
			System.out.println("批次号：" + batchNo + ",发送日期:" + sendDate);

			/*
			 * 解析报文体
			 */
			Element tBody = root.element("body");
			manageCom = tBody.elementText("ManageCom");
			handler = tBody.elementText("Handler");
			rgtNo = tBody.elementText("RgtNo");
		
			Element tGroup = tBody.element("Group");
			payMode = tGroup.elementText("PayMode");
			drawer = tGroup.elementText("Drawer");
			drawerID = tGroup.elementText("DrawerID");			
			bankCode = tGroup.elementText("BankCode");
			bankAccNo = tGroup.elementText("BankAccNo");
			accName = tGroup.elementText("AccName");
		
		}catch (Exception e) {
			content="解析失败，原因是:"+e.toString();
			System.out.println("输入信息节点不符合规则，校验不通过");
			returnXml("0", tLLSendMsgSchema);
			return false;
	    }
		System.out.println("...........................报文解析成功！...............................");
		return true;
		
	}
	
	//对已经保存到本类的信息进行校验，校验通过后查询一些必须一起传递到后台的数据
    //校验数据，获取页面自动填写数据
	public boolean checkdate(){
	    ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		String sql;
		
		if("".equals(rgtNo)) {
			content = "批次号不能为空！";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		String first = rgtNo.substring(0, 1);
		if(!"P".equals(first)){
			content = "批次号不是以P开头的团体批次号";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		String mpayMode = new String();
		String mmanageCom = new String();
		String mothernotype = new String();
		try {
		
			sql = "select managecom,actugetno,sumgetmoney,paymode,othernotype from ljaget where otherno = '"
					+rgtNo+"' order by managecom with ur";
			tSSRS = tExeSQL.execSQL(sql);
			System.out.println("查询出数据条数："+tSSRS.getMaxRow());
			if(tSSRS.getMaxRow()<1){
				content = "没有该批次号信息";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
			mmanageCom = tSSRS.GetText(1, 1);
			actuGetNo = tSSRS.GetText(1, 2);
			getMoney = tSSRS.GetText(1, 3);
			mpayMode = tSSRS.GetText(1, 4);
			mothernotype = tSSRS.GetText(1, 5);
			
	    } catch (Exception e) {
			content="查询失败，原因是:"+e.toString();
			System.out.println("案件号没有付费信息");
			returnXml("0", tLLSendMsgSchema);
			return false;
	    }
//	    System.out.println("数据库内机构"+mmanageCom);
//	    System.out.println("报文内机构"+manageCom);
//	    System.out.println(manageCom.equals(mmanageCom));
	    
		if(!manageCom.equals(mmanageCom)){
			content = "报文传递机构与团体批次所属机构不符";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		if(!"4".equals(mpayMode)){
			content = "该案件系统内记录不是银行转账，无法修改";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		if(!"5".equals(mothernotype)){
			content = "该案件系统内记录othernotype不符合，无法修改";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		if ("".equals(payMode)) {
			content = "付费方式不能为空！";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}
		
		
		if ("4".equals(payMode)) {
			if ("".equals(bankCode)) {
				content = "对方开户银行不能为空！";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}

			if ("".equals(bankAccNo)) {
				content = "对方银行帐号不能为空！";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}

			if ("".equals(accName)) {
				content = "对方银行帐户名不能为空！";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
		} else{
			content = "该案件不是银行转账，无法修改";
			System.out.println(content);
			returnXml("0", tLLSendMsgSchema);
			return false;
		}	
		
		
//		TogetherFlag 3 4
		String mTogetherFlag = new String();
		try {
			sql = sql="select TogetherFlag  from llregister where rgtno ='"+rgtNo+"'";
			tSSRS = tExeSQL.execSQL(sql);
			mTogetherFlag = tSSRS.GetText(1, 1);
	    } catch (Exception e) {
			content="查询失败，原因是:"+e.toString();
			System.out.println("统一给付标记查询出错");
			returnXml("0", tLLSendMsgSchema);
			return false;
	    }
		if(!"3".equals(mTogetherFlag)){
			if(!"4".equals(mTogetherFlag)){
				content = "此批次案件统一给付标记状态不是3或4不允许修改";
				System.out.println(content);
				returnXml("0", tLLSendMsgSchema);
				return false;
			}
		}
		
		
		return true;
	}
	
	//修改确认
	public boolean modifyconfirm(){
	//	System.out.println("当前操作员："+tG.Operator);
		handler="lipeia";
		tG.Operator = handler;
	//	System.out.println("当前操作员："+tG.Operator);
		// 输出参数
		String FlagStr = "";
		String strOperate = "MODIFY";
		System.out.println("==== strOperate == " + strOperate);

		// 动作为修改。
		tLJAGetSchema.setActuGetNo(actuGetNo);
		tLJAGetSchema.setSumGetMoney(getMoney);
		// tLJAGetSchema.setOtherNo(request.getParameter("OtherNo2"));
		tLJAGetSchema.setPayMode(payMode);
		tLJAGetSchema.setDrawer(drawer);
		tLJAGetSchema.setDrawerID(drawerID);
		tLJAGetSchema.setBankCode(bankCode);
		tLJAGetSchema.setBankAccNo(bankAccNo);
		tLJAGetSchema.setAccName(accName);
		tLJAGetSchema.setCanSendBank("0");
        System.out.println("ljaget表数据装填完成");
		// 团体给付
		tLLRegisterSchema.setRgtNo(rgtNo);
		tLLRegisterSchema.setCaseGetMode(payMode);
		tLLRegisterSchema.setBankCode(bankCode);
		tLLRegisterSchema.setBankAccNo(bankAccNo);
		tLLRegisterSchema.setAccName(accName);

				try {
					tVData.add(tLLRegisterSchema);
					tVData.add(tLJAGetSchema);
					tVData.add(tG);
					tLLGrpAccModifyUI.submitData(tVData, strOperate);
				} catch (Exception ex) {
					content = "保存失败，原因是:" + ex.toString();
					System.out.println("保存失败，原因是:" + ex.toString());
					FlagStr = "Fail";
					returnXml("0", tLLSendMsgSchema);
					return false;
				}
				if (FlagStr == "") {
					tError = tLLGrpAccModifyUI.mErrors;
					if (!tError.needDealError()) {
						content = "修改确认成功！";
						FlagStr = "Succ";
						System.out.println(content + FlagStr);
						tVData.clear();
						tVData = tLLGrpAccModifyUI.getResult();
						System.out.println("=================向后台传入数据成功================");
					} else {
						content = "保存失败，原因是:" + tError.getLastError();
						FlagStr = "Fail";
						System.out.println(content + FlagStr);
						returnXml("0", tLLSendMsgSchema);
						return false;
					}
				}
		
		
		
		return true;
	}
	
	
	public boolean parseXml(String xmlMsg) {
		System.out.println("版本号2018-12-19 13：30");
		//解析请求报文，将数据保存到本类中
		if(!getinputdate(xmlMsg)){
			content="保存到本类数据失败";
			System.out.println("解析报文，保存数据到本类失败");
			returnXml("0", tLLSendMsgSchema);
			return false;
			}
				
		//校验数据
		if(!checkdate()){
		content="校验数据失败，传入的数据不符合要求:"+content;
		System.out.println(content);
		returnXml("0", tLLSendMsgSchema);
		return false;
				}
				
		//修改处理MODIFY包括写死hardler操作
		if(!modifyconfirm()){
		content="修改确认失败:"+content;
		System.out.println(content);
		returnXml("0", tLLSendMsgSchema);
		return false;
		}
		
//		String tcansendbank = new String();
//		try{
//			ExeSQL tExeSQL = new ExeSQL();
//			SSRS tSSRS = new SSRS();
//		String sql ="select cansendbank from ljaget where otherno ='"+rgtNo+"';";
//		tSSRS = tExeSQL.execSQL(sql);
//		tcansendbank = tSSRS.GetText(1, 1);
//		System.out.println(tcansendbank);
//		}catch (Exception e) {
//			content="查询失败，原因是:"+e.toString();
//			System.out.println(content);
//			returnXml("0", tLLSendMsgSchema);
//			return false;
//	    }
		
//		if("0".equals(tcansendbank)){
		
		System.out.println("===================成功=================");
		returnXml("1", tLLSendMsgSchema);
		return true;
//		}
//		return false;
	}

	/**
	 * 拼接返回报文
	 * 
	 * @return
	 */
	private String returnXml(String str, LLSendMsgSchema mLLSendMsgSchema) {
		StringWriter stringWriter = new StringWriter();
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("AccModify_Response");
		document.setRootElement(tDateSet);
		
		SimpleDateFormat simDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat simTime = new SimpleDateFormat("HH:mm:ss");
		
		// 返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Date date = new Date();
		tSendDate.addText(simDate.format(date));
		Element tSendTime = tMsgResHead.addElement("SendTime");
		tSendTime.addText(simTime.format(date));
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		tSendOperator.addText(this.handler);
		Element tMsgType = tMsgResHead.addElement("MsgType");
		Element tRownum = tMsgResHead.addElement("Rownum");
		// 返回报文体样式
		Element tBody = tDateSet.addElement("Body");
		Element tRgtNo = tBody.addElement("RgtNo");
		Element tContent = tBody.addElement("Content");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		tSuccessFlag.addText(str);
		if ("1".equals(str)) {
			tContent.addText("successful");
		} else {
			tContent.addText(content);
		}
		// 创建写文件方法
		XMLWriter xmlWriter = new XMLWriter(stringWriter);
		// 写入文件
		try {
			xmlWriter.write(document);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		// 关闭
		try {
			xmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		this.responseXml = stringWriter.toString();
		return responseXml;
	}

	/**
	 * 获取返回报文
	 * 
	 * @return
	 */
	public String getResponseXml() {
//		return StringUtil.StringNull(responseXml)?responseXml:null;
//		return responseXml;
		return returnXml("0", tLLSendMsgSchema);
	}

}
