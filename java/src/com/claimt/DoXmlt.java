package com.claimt;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 建立线程 调用返回报文
 * @author 樊庆
 *
 */
public class DoXmlt  {
	private VData mResult = new VData();
	private String responseXml;
	public boolean runin() {
		System.out.println("进入DoXmlt方法 ———— start");
		DealClaimt tt = new DealClaimt();
		//调用理赔流程
	    boolean submitDate2 = tt.submitDate2(mResult);
	    //调用接口发送报文
	    System.out.println("=== 调用接口发送最终结果报文 ===");
	    responseXml = tt.getResponseXml();
//	    service(responseXml); //由于理算成功返回两次一样的报文，此处省略一次调用
	    System.out.println("进入DoXmlt方法 ———— end");
	    if (submitDate2) {
			return true;
		}
	    return false;
	}
	
	
	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

		try {
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return mByteArrayOutputStream.toByteArray();
	}

	@SuppressWarnings("rawtypes")
	public void service(String aInXmlStr) {
		// webservice 地址
		String sql = "select TransDetail,TransDetail2,remark from WFTransInfo where transtype = 'AnalysisInterface' and transcode = '1'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sql1SSRS = tExeSQL.execSQL(sql);
		
		String tStrTargetEendPoint = sql1SSRS.GetText(1, 2);
		String tStrNamespace = sql1SSRS.GetText(1, 1);
//		tStrTargetEendPoint = "http://10.253.33.166:8080/Interface/services/ReceiveXML/getMassageNoreturn";
//		tStrNamespace = "http://impl.business.hisinterface.si.sl.neusoft.com";
		System.out.println("StrTargetEendPoint==== " + tStrTargetEendPoint);
		System.out.println("StrNamespace==== " + tStrNamespace);
		System.out.println("方法名==== " + sql1SSRS.GetText(1, 3));
//		tStrTargetEendPoint = "http://10.253.33.136:8080/framework/services/AnalysisInterface";
		try {
			RPCServiceClient client = new RPCServiceClient();
			EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
			Options option = client.getOptions();
			option.setTo(erf);
//			option.setAction("analysisXml");// 调用方法名
			QName name = new QName(tStrNamespace, sql1SSRS.GetText(1, 3));// 调用方法名
			Object[] object = new Object[] { aInXmlStr };
			Class[] returnTypes = new Class[] { String.class };
			Object[] response = client.invokeBlocking(name, object, returnTypes);
			String result = (String) response[0];
			System.out.println("UI return:" + result);
			System.out.println("接口调用完毕！");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		try {
			DoXmlt test = new DoXmlt();

			String mInFilePath = "C:/Users/Administrator/Desktop/13.xml";
			InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = test.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "UTF-8");
			System.out.println(mInXmlStr);
			test.service(mInXmlStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public VData getmResult() {
		return mResult;
	}
	public void setmResult(VData mResult) {
		this.mResult = mResult;
	}

	public String getResponseXml() {
		return responseXml;
	}

	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}

	
}