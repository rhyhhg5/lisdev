package com.claimt.approve;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.preservation.util.StringUtil;
import com.sinosoft.lis.llcase.UnlockRecordBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLLockSchema;
import com.sinosoft.lis.vschema.LLLockSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
  * @ClassName: GrpContLock
  * @Description: TODO
  * @author liuzehong
  * @date 2019年1月2日 下午5:49:38
  * @version V1.0
 **/
public class GrpContLockInterface {
	
	static final String FORMAT_DATE = "yyyy-MM-dd";

	static final String FORMAT_TIME = "HH:mm:ss";
	
	private SimpleDateFormat simDate = new SimpleDateFormat(FORMAT_DATE);

	private SimpleDateFormat simTime = new SimpleDateFormat(FORMAT_TIME);
	
	private GlobalInput tG ;
	private String content="";
	private String SuccessFlag="";
	private String tSendDate="";
	private String tSendTime="";
	private String tBranchCode="";
	private String tSendOperator="";
	private String tMsgType="";
	private String tBatchNo="";
	private String tManageCom="";
	
	private Map<String ,String> grpMap;
	private String responseXml=""; 
	
	public String getResponseXml() {
		return responseXml;
	}

	private static Log log = LogFactory.getLog(GrpContLockInterface.class);

	public GrpContLockInterface() {
		this.tG = new GlobalInput();;
	}

	public boolean deal(String strXml) {
		try {
			if (!StringUtil.isNull(strXml)) {
				this.content = "请求报文不能为空";
				this.SuccessFlag="0";
				log.info("========================" + content + "===============================");
				return false;
			}
			// 解析报文
			if (!getdata(strXml)) {
				this.SuccessFlag="0";
				log.info("========================" + content + "===============================");
				return false;
			}
			// 业务处理
			if (!dealData(this.grpMap)) {
				this.SuccessFlag="0";
				log.info("========================" + content + "===============================");
				return false;
			}
			this.SuccessFlag="1";
		} finally {
			rerutnXml();
		}
		return true;
	}
	/**
	 * 
	 * 拼接返回报文
	 *
	 */
	private void rerutnXml() {
		StringWriter stringWriter = new StringWriter();
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("Claim_Response");
		document.setRootElement(tDateSet);
		// 返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Date date = new Date();
		tSendDate.addText(simDate.format(date));
		Element tSendTime = tMsgResHead.addElement("SendTime");
		tSendTime.addText(simTime.format(date));
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		tBranchCode.addText(this.tBranchCode);
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		tSendOperator.addText(this.tSendOperator);
		Element tMsgType = tMsgResHead.addElement("MsgType");
		tMsgType.addText(this.tMsgType);
		Element tRownum = tMsgResHead.addElement("Rownum");
		// 返回报文体样式
		Element tBody = tDateSet.addElement("Body");
		Element tContent = tBody.addElement("Content");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		tContent.addText(this.content);
		tSuccessFlag.addText(this.SuccessFlag);
		// 创建写文件方法
		XMLWriter xmlWriter = new XMLWriter(stringWriter);
		// 写入文件
		try {
			xmlWriter.write(document);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		// 关闭
		try {
			xmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		this.responseXml = stringWriter.toString();
	}

	/**
	 * @return
	 */
	private boolean dealData(Map<String,String> grpMap) {
		LLLockSet tLLLockSet = new LLLockSet();
		ExeSQL tExeSQL = new ExeSQL();
		if (grpMap==null||grpMap.isEmpty()) {
			this.content="GrpCont节点内容为空";
			return false;
		}
		String grpContNo="";
		String rgtno="";
		//遍历map集合,校验数据
		for (Entry<String, String> entry : grpMap.entrySet()) {
			rgtno = entry.getKey();
			grpContNo= entry.getValue();
			 String lllockSql="select projectname,Grpcontno,rgtno,onepayfee,lockdate  from lllock where grpcontno='"+grpContNo+"' and rgtno='"+rgtno+"' and state='1'  fetch first 3000 rows only with ur";
			 SSRS lllockSSRS = tExeSQL.execSQL(lllockSql);
			 if (lllockSSRS.getMaxRow()<1) {
					this.content="保单"+grpContNo+"下批次"+rgtno+"未锁定，请检查";
					return false;
		     }
			 LLLockSchema tLLLockSchema =new LLLockSchema();
		      System.out.println(grpContNo);
		      tLLLockSchema.setGrpContNo(grpContNo);
		      System.out.println(rgtno);
		      tLLLockSchema.setRgtno(rgtno);
		      tLLLockSet.add(tLLLockSchema);
		}
		CErrors tError = null;
		tG.Operator="lipeia";
		tG.ManageCom=tManageCom;
		//tG.ManageCom=this.tGEManageCom;
		VData tVData = new VData();
		UnlockRecordBL tUnlockRecordBL = new UnlockRecordBL();
		//输出参数
		String FlagStr = "";
		String strOperate ="update";
		System.out.println("strOperate=" + strOperate);
		  try{
		    tVData.add(tLLLockSet);
		    tVData.add(tG);
		    tUnlockRecordBL.submitData(tVData, strOperate);
		  }
		  catch(Exception ex){
		    content = "确认失败，原因是:" + ex.toString();
		    System.out.println(content);
		    FlagStr = "Fail";
		    return false;
		  }
		//如果catch中发现异常，则不从错误类中提取错误信息
		if(FlagStr == "") {
			tError = tUnlockRecordBL.mErrors;
			if(!tError.needDealError()) {
				content = "保存成功";
				FlagStr = "Success";
			}else {
				content = "保存失败,原因是" + tError.toString();
				FlagStr = "Fail";
				return false;
			}
		}
		return true;
	}

	/**
	 * @param strXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean getdata(String strXml) {
		log.info("***********开始解析报文*********");
		Document doc;
		try {
			
			 ValidataXML  validataXML=new ValidataXML();
				if (!validataXML.validate(strXml,getXsd())) {
					content = validataXML.getContent();
					System.out.println(content);
					return false;
				}
			doc = DocumentHelper.parseText(strXml);
			Element root = doc.getRootElement();
			// 解析报文头
			Element thead = root.element("head");
			this.tBatchNo = thead.elementText("BatchNo");
			this.tSendDate = thead.elementText("SendDate");
			this.tSendTime = thead.elementText("SendTime");
			this.tBranchCode = thead.elementText("BranchCode");
			this.tSendOperator = thead.elementText("SendOperator");
			this.tMsgType = thead.elementText("MsgType");
			//解析报文体
			Element tbody = root.element("body");
			this.tManageCom = tbody.elementText("ManageCom");
			Element tItem = tbody.element("Item");
			List<Element> elements = tItem.element("GrpCont").elements();
			if (elements != null && elements.size() > 0) {
				List<Element> grpContList = tItem.selectNodes("GrpCont");
				grpMap=new HashMap<String,String>(grpContList.size());
				for (int i = 0; i < grpContList.size(); i++) {
					grpMap.put(grpContList.get(i).elementText("RgtNo"), grpContList.get(i).elementText("GrpContNo"));
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
			this.content = "解析报文出错:" + e.toString();
			return false;
		}
		return true;
	}
	
	private String getXsd() {
		String str="<?xml version=\"1.0\" encoding=\"GBK\"?>\r\n" + 
				"<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">  \r\n" + 
				"  <xs:element name=\"MedicalAdjustment_Request\"> \r\n" + 
				"    <xs:complexType> \r\n" + 
				"      <xs:sequence> \r\n" + 
				"        <xs:element name=\"head\"> \r\n" + 
				"          <xs:complexType> \r\n" + 
				"            <xs:sequence> \r\n" + 
				"              <xs:element name=\"BatchNo\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendDate\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendTime\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"BranchCode\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendOperator\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"MsgType\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"Rownum\" type=\"xs:string\"/> \r\n" + 
				"            </xs:sequence> \r\n" + 
				"          </xs:complexType> \r\n" + 
				"        </xs:element>  \r\n" + 
				"        <xs:element name=\"body\"> \r\n" + 
				"          <xs:complexType> \r\n" + 
				"            <xs:sequence> \r\n" + 
				"              <xs:element name=\"ManageCom\" type=\"xs:string\"/>\r\n" + 
				"              <xs:element name=\"Item\" > \r\n" + 
				"                <xs:complexType> \r\n" + 
				"                  <xs:sequence> \r\n" + 
				"                    <xs:element name=\"GrpCont\"  maxOccurs=\"999999999\" >  \r\n" + 
				"                      <xs:complexType> \r\n" + 
				"                        <xs:sequence> \r\n" + 
				"                          <xs:element name=\"GrpContNo\" type=\"xs:string\" />  \r\n" + 
				"						   <xs:element name=\"RgtNo\" type=\"xs:string\" />\r\n" + 
				"                        </xs:sequence> \r\n" + 
				"                      </xs:complexType> \r\n" + 
				"                    </xs:element> \r\n" + 
				"                  </xs:sequence> \r\n" + 
				"                </xs:complexType> \r\n" + 
				"              </xs:element>  \r\n" + 
				"            </xs:sequence> \r\n" + 
				"          </xs:complexType> \r\n" + 
				"        </xs:element> \r\n" + 
				"      </xs:sequence> \r\n" + 
				"    </xs:complexType> \r\n" + 
				"  </xs:element> \r\n" + 
				"</xs:schema>";
		return str;
	}
}
