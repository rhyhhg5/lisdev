package com.claimt.approve;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

/**
 * @ClassName: ValidataXMLTest
 * @Description: XMLSchema
 * @author liuzehong
 * @date 2019年1月28日 上午11:47:00
 * @version V1.0
 */
public class ValidataXML {

	private String content = "";

	public String getContent() {
		return content;
	}

	/**
	 * 根据Schema xsd文件验证 xml文件
	 * @param strxml   xml文件字符串
	 * @param strxsd   xsd文件字符串
	 * @return
	 */
	public boolean validate(String strxml, String strxsd) {
		boolean flag = false;
		try {
			// 查找支持指定模式语言的 SchemaFactory 的实现并返回它
			SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			// 解析作为模式的指定 File 并以 Schema 的形式返回它
			// 此对象表示可以根据 XML 文档检查/实施的约束集
			// File file = new File(xsdPath);
			StreamSource sourcexsd = new StreamSource();
			InputStream stringStreamxsd = getStringStream(strxsd);
			sourcexsd.setInputStream(stringStreamxsd);
			Schema schema = factory.newSchema(sourcexsd);
			// Schema schema = factory.newSchema(file);
			// 验证器实施/检查此对象表示的约束集。Validator -> 根据 Schema检查 XML 文档的处理器。
			Validator validator = schema.newValidator();
			// 验证指定的输入。 Source -> 实现此接口的对象包含充当源输入（XML 源或转换指令）所需的信息
			// Source source = new StreamSource(xmlPath);
			StreamSource source = new StreamSource();
			InputStream stringStream = getStringStream(strxml);
			source.setInputStream(stringStream);
			validator.validate(source);
			flag = true;
		} catch (SAXException e) {
			e.printStackTrace();
			content = e.toString();
		} catch (IOException e) {
			e.printStackTrace();
			content = e.toString();
		}
		return flag;

	}

	/**
	 * 将一个字符串转化为输入流
	 */
	public static InputStream getStringStream(String sInputString) {
		if (sInputString != null && !sInputString.trim().equals("")) {
			try {
				ByteArrayInputStream tInputStringStream = new ByteArrayInputStream(sInputString.getBytes());
				return tInputStringStream;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

}
