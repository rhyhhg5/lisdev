package com.claimt.approve;

import com.preservation.util.StringUtil;
import com.sinosoft.lis.llcase.LLCaseReturnUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCaseBackSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
  * @ClassName: CaseBack
  * @Description: 个案回退
  * @author liuzehong
  * @date 2019年1月14日 上午10:40:07
 **/
public class CaseBack {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors;

	private ExeSQL mExeSQL;
	
	private GlobalInput tG=new GlobalInput();
	
	private LLAppealAcceptSchema mLLAppealAcceptSchema;

	private String tCaseNo="";
	
	private String managecom="";

	public void setManagecom(String managecom) {
		this.managecom = managecom;
	}

	public CaseBack() {
		mExeSQL=new ExeSQL();
		mErrors=new CErrors();
	}

	public CErrors getmErrors() {
		return mErrors;
	}

	public boolean submitData(VData tVData) {
        //获取数据
		if (!getData(tVData)) {
			return false;
		}
		// 业务处理
		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * @param tVData
	 * @return
	 */
	private boolean getData(VData tVData) {
		boolean executable = false;
		if (tVData != null) {
			this.mLLAppealAcceptSchema = (LLAppealAcceptSchema)tVData
					.getObjectByObjectName("mLLAppealAcceptSchema", 0);
			this.tG = (GlobalInput) tVData
					.getObjectByObjectName("GlobalInput", 0);
			this.tCaseNo = (String) tVData
					.getObjectByObjectName("tCaseNo", 0);
			if (null != mLLAppealAcceptSchema&&null!= tG&&StringUtil.isNull(tCaseNo)) {
				executable = true;
			}
			mErrors.addOneError("获取审批审定数据失败");
		}
		return executable;
	}

	/**
	 * @return
	 */
	private boolean dealData() {
		String caseNo ="";
		String returnReason ="";
		String backState ="";
		caseBack(caseNo, returnReason, backState);
		return true;
		
	}
	
	/**
	 * 个案回退
	 */
	public boolean caseBack(String caseNo,String returnReason,String backState) {
		if (!StringUtil.isNull(caseNo)) {
			mErrors.addOneError("案件号不能为空");
			return false;
		}
		String rgtstateSql = "select rgtstate from  llcase where caseno='" + caseNo + "'";
		String rgtstate = mExeSQL.getOneValue(rgtstateSql);
		if (!StringUtil.isNull(rgtstate) || "11".equals(rgtstate) || "12".equals(rgtstate) || "13".equals(rgtstate)
				|| "14".equals(rgtstate) || "07".equals(rgtstate) || "16".equals(rgtstate)) {
			mErrors.addOneError("此案件状态不能回退");
			return false;
		}
		String lltjSql = "select 1 from llhospcase where apptranno like 'TJ%' and hospitcode ='00000000' and caseno ='"
				+ caseNo + "'";
		SSRS SSRS = mExeSQL.execSQL(lltjSql);
		String errorV="";
		if (SSRS.MaxRow > 0) {
			mErrors.addOneError("天津居民意外案件不能进行回退！");
			return false;
		}
		String sqlUpdate = "update llcase set handler='lipeia' where CaseNo = '" + caseNo + "'";
		String sqlUpdate2 = "update llcase set Rigister='lipeia' where CaseNo = '" + caseNo + "'";
		if (!mExeSQL.execUpdateSQL(sqlUpdate)) {
			mErrors.addOneError("回退更新数据失败");
			return false;
		}
		if (!mExeSQL.execUpdateSQL(sqlUpdate2)) {
			mErrors.addOneError("回退更新数据失败");
			return false;
		}
		//String backState = "05";// 回退状态01：受理 04:理算 05：审批
		String llusercode = "lipeia";// 回退理赔员
		//String returnReason = "6";// 回退原因
									// 1.受理登陆错误2.账单录入错误，3：疾病录入错误，4理赔结果错误，5：补充调查。6：其他
		tG.Operator = "lipeia";
		tG.ManageCom=managecom;
		if (!StringUtil.isNull(managecom)) {
			mErrors.addOneError("机构编码不能为空");
			return false;
		}
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
		LLCaseReturnUI tLLCaseReturnUI = new LLCaseReturnUI();
		// 输出参数
		CErrors tError = null;
		String FlagStr = "";
		String Content = "";
		String transact = "INSERT||MAIN";

		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		tLLCaseSchema.setCaseNo(caseNo);
		tLLCaseSchema.setHandler(llusercode);
		tLLCaseSchema.setRgtState(backState);

		tLLCaseBackSchema.setCaseNo(caseNo);
		tLLCaseBackSchema.setBeforState(rgtstate);
		tLLCaseBackSchema.setAfterState(backState);
		tLLCaseBackSchema.setNHandler(llusercode);
		tLLCaseBackSchema.setReason(returnReason);
		tLLCaseBackSchema.setRemark("");
		tLLCaseBackSchema.setMngCom(tG.ManageCom);
		try {
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add(tLLCaseSchema);
			tVData.add(tLLCaseBackSchema);
			tVData.add(tG);
			tLLCaseReturnUI.submitData(tVData, transact);
		} catch (Exception ex) {
			errorV= "回退案件失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			return false;
		}
		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tLLCaseReturnUI.mErrors;
			if (tError.needDealError()) {
				mErrors.addOneError("回退案件败，原因是:" +tError.getFirstError());
				FlagStr = "Fail";
				errorV=" 回退案件失败，原因是:" + tError.getFirstError();
				return false;
			}
		}
		System.out.println("===========================" + Content + "===============================");
		return true;
	}
	
	public static void main(String[] args) {
		CaseBack caseback=new CaseBack();
		caseback.caseBack("R3605190116000008","01","06");
		
	}

}
