package com.claimt.approve;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.llcase.GrpGiveEnsureBL;
import com.sinosoft.lis.llcase.GrpRegisterBL;
import com.sinosoft.lis.llcase.LJAGetInsertBL;
import com.sinosoft.lis.llcase.LLContAutoDealBL;
import com.sinosoft.lis.llcase.LPSecondUWBL;
import com.sinosoft.lis.llcase.QDGrpGiveEnsureBL;
import com.sinosoft.lis.llcase.RegisterBL;
import com.sinosoft.lis.llcase.RgtSurveyUI;
import com.sinosoft.lis.llcase.SimpleClaimAuditBL;
import com.sinosoft.lis.llcase.SimpleClaimAuditUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLPersonMsgSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSurveySchema;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLInqCertificateSet;
import com.sinosoft.lis.vschema.LLPersonMsgSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * @ClassName:Approve
 * @Description: 理赔四步<1.审定，2.通知给付，3.撤件，4.回退>
 * @author liuzehong
 * @date 2018年10月17日 下午4:24:10
 * @version V1.0
 *
 */
public class Approve {

	static final String FORMAT_DATE = "yyyy-MM-dd";

	static final String FORMAT_TIME = "HH:mm:ss";

	static final String UW_MAIN = "UW||MAIN"; // 批量审定

	static final String UPDATE_RgtPayMode = "UPDATE||RgtPayMode";

	private GlobalInput tG ;

	private String content = ""; // 返回的具体信息

	private String desprition=""; // 描述
	
	private  ExeSQL mExeSQL;

	private String tRgtNo=""; // 批次号

	private String tPolNo=""; // 团体保单号

	private String tOperator="";// 操作员

	private String tGEManageCom="";// 机构

	private List<String> personCases = null;

	private String payMethod = ""; // 给付方式

	private String apppeoples = "";// 人数

	private String FlagStr = "";

	private String tOperate = "";
	
	private String lockState="0";

	private CErrors tError = null;

	private Map<String, String> map = null;

	private LLPersonMsgSet mLLPersonMsgSet = null;// 存储个人给付人员信息

	private List<String> caseNoes = null; // 存储案件集合

	private LLSurveySchema tLLSurveySchema = null;

	private Map<String, String> mapRC;

	private String responseXml = "";

	private String SuccessFlag = "";

	private List<String> successCases = null;

	private SimpleDateFormat simDate;

	private SimpleDateFormat simTime; 

	CErrors mErrors = new CErrors();
	public Approve() {
		this.mExeSQL =new ExeSQL();
		this.simDate = new SimpleDateFormat(FORMAT_DATE);
		this.simTime = new SimpleDateFormat(FORMAT_TIME);
		tG = new GlobalInput();
	}

	public void settPolNo(String tPolNo) {
		this.tPolNo = tPolNo;
	}

	public void settOperator(String tOperator) {
		this.tOperator = tOperator;
	}

	public void settGEManageCom(String tGEManageCom) {
		this.tGEManageCom = tGEManageCom;
	}

	public void settRgtNo(String tRgtNo) {
		this.tRgtNo = tRgtNo;
	}

	public String getResponseXml() {
		return responseXml;
	}

	/**
	 * 解析调用
	 * 
	 * @param strXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean parseXml(String strXml) {

		System.out.println("************************begin*****************************");

		if (!stringNull(strXml)) {
			content = "请求报文不能为空";
			SuccessFlag = "0";
			desprition = content;
			System.out.println(content);
			return false;
		}
		// 开始解析xml
		Document doc;
		try {
           ValidataXML  validataXML=new ValidataXML();
			if (!validataXML.validate(strXml,getXsd())) {
				content = validataXML.getContent();
				SuccessFlag = "0";
				desprition = content;
				System.out.println(content);
				return false;
			}
			doc = DocumentHelper.parseText(strXml);
			Element root = doc.getRootElement();
			// 获取报文体
			Element tBody = root.element("body");
			this.tPolNo = tBody.elementText("PolNo");// 团体保单号，不能为空
			this.tRgtNo = tBody.elementText("RgtNo");// 批次号，不能为空
			this.tOperator = tBody.elementText("Operator");// 操作员，不能为空
			this.tGEManageCom = tBody.elementText("GEManageCom");// 机构，不能为空
			this.tOperate = tBody.elementText("Operate");// 操作，不能为空

			if (!stringNull(tPolNo)) {
				content = "团体保单号，不能为空";
				SuccessFlag = "0";
				desprition = content;
				System.out.println(content);
				return false;
			}
			if (!stringNull(tRgtNo)) {
				content = "批次号，不能为空";
				SuccessFlag = "0";
				desprition = content;
				System.out.println(content);
				return false;
			}
			if (!stringNull(tOperate)) {
				content = "操作不能为空";
				SuccessFlag = "0";
				desprition = content;
				System.out.println(content);
				return false;
			}
			if (!stringNull(tOperator)) {
				content = "操作员不能为空";
				SuccessFlag = "0";
				desprition = content;
				System.out.println(content);
				return false;
			}
			if (!stringNull(tGEManageCom)) {
				content = "机构编码不能为空";
				desprition = content;
				System.out.println(content);
				return false;
			}
			String apppeoplesdSql = "select apppeoples from LLRegister  where rgtno='" + tRgtNo + "'";
			SSRS apppeoplesSSRS = mExeSQL.execSQL(apppeoplesdSql);
			if (apppeoplesSSRS.getMaxNumber() > 0) {
				this.apppeoples = apppeoplesSSRS.GetText(1, 1);
			}
			List<Element> list = tBody.element("caseList").elements();
			// 根据是否传案件号判断操作整个批次还是单独案件。
			if (list != null && list.size() > 0) {
				Element tCaseList = tBody.element("caseList");
				List<Element> casenoList = tCaseList.selectNodes("caseno");
				caseNoes = new ArrayList<String>();
				for (int i = 0; i < casenoList.size(); i++) {
					caseNoes.add(casenoList.get(i).getText());
				}
				List<Element> tcases = tCaseList.element("case").elements();
				if ("01".equals(tOperate) || "02".equals(tOperate) || "03".equals(tOperate)) {
					if (tcases != null && tcases.size() > 0) { // 案件回退
						List<Element> caseList = tCaseList.selectNodes("case");
						caseNoes = new ArrayList<String>();
						String caseNo = "";
						for (int i = 0; i < caseList.size(); i++) {
							caseNo = caseList.get(i).elementText("caseno");
							caseNoes.add(caseNo);
						}
					}
				} else if ("04".equals(tOperate) || "07".equals(tOperate)) {
					
					if ("04".equals(tOperate)) {
						if (tcases != null && tcases.size() > 0) { // 案件回退
							List<Element> caseList = tCaseList.selectNodes("case");
							mapRC = new HashMap<String, String>(caseList.size());
							caseNoes = new ArrayList<String>();
							String caseNo = "";
							String reason = "";
							for (int i = 0; i < caseList.size(); i++) {
								caseNo = caseList.get(i).elementText("caseno");
								reason = caseList.get(i).elementText("reason");
								if (!isNull(reason, caseNo)) {
									content = "回退案件回退原因不能为空";
									SuccessFlag = "0";
									desprition = content;
									return false;
								}
								mapRC.put(caseNo, reason);
								caseNoes.add(caseNo);
							}
						}
					}else {
						if (tcases != null && tcases.size() > 0) { // 案件回退
							List<Element> caseList = tCaseList.selectNodes("case");
							mapRC = new HashMap<String, String>(caseList.size());
							caseNoes = new ArrayList<String>();
							String caseNo = "";
							String reason = "";
								caseNo = caseList.get(0).elementText("caseno");
								reason = caseList.get(0).elementText("reason");
								if (!isNull(reason, caseNo)) {
									content = "回退案件回退原因不能为空";
									SuccessFlag = "0";
									desprition = content;
									return false;
								}
								mapRC.put(caseNo, reason);
								caseNoes.add(caseNo);
						}
					}
				}
			}

			if ("06".equals(tOperate)) {
				tLLSurveySchema = new LLSurveySchema();
				Element tRgtSurvey = tBody.element("RgtSurvey");
				tLLSurveySchema.setSurveyClass(tRgtSurvey.elementText("SurvType"));
				tLLSurveySchema.setSurveySite(     tRgtSurvey.elementText("CasePay"));
				tLLSurveySchema.setContent(   tRgtSurvey.elementText("SurvContent"));
				tLLSurveySchema.setSurveyFlag("0");/** 调查报告状态 */
				tLLSurveySchema.setSurveyType("1");
				RgtSurvey("C3605181129000029");
			}
			List<Element> elements = tBody.element("Person").elements();
			if (elements != null && elements.size() > 0) {
				List<Element> personList = tBody.selectNodes("Person");
				mLLPersonMsgSet = new LLPersonMsgSet();
				personCases = new ArrayList<String>();
				for (int i = 0; i < personList.size(); i++) {
					LLPersonMsgSchema tLLPersonMsgSchema = new LLPersonMsgSchema();
					tLLPersonMsgSchema.setPhoneNumber(personList.get(i).elementText("PhoneNumber"));
					tLLPersonMsgSchema.setEMail(personList.get(i).elementText("E-Mail"));
					tLLPersonMsgSchema.setReceiveMethod(personList.get(i).elementText("ReceiveMethod"));
					tLLPersonMsgSchema.setBankCode(personList.get(i).elementText("BankCode"));
					tLLPersonMsgSchema.setSignBank(personList.get(i).elementText("SignBank"));
					tLLPersonMsgSchema.setAccount(personList.get(i).elementText("Account"));
					tLLPersonMsgSchema.setAccountName(personList.get(i).elementText("AccountName"));
					tLLPersonMsgSchema.setDeclareAmount(personList.get(i).elementText("DeclareAmount"));
					tLLPersonMsgSchema.setAdvanceFlag(personList.get(i).elementText("AdvanceFlag"));
					tLLPersonMsgSchema.setName(personList.get(i).elementText("Name"));
					tLLPersonMsgSchema.setID(personList.get(i).elementText("ID"));
					tLLPersonMsgSchema.setDrawer(personList.get(i).elementText("Drawer"));
					tLLPersonMsgSchema.setDrawerID(personList.get(i).elementText("DrawerID"));
					tLLPersonMsgSchema.setReceiptNo(personList.get(i).elementText("ReceiptNo"));
					mLLPersonMsgSet.add(tLLPersonMsgSchema);
					personCases.add(personList.get(i).elementText("caseno"));
					System.out.println("个人给付第" + (i + 1) + "条");
				}
			}
			if ("01".equals(tOperate)) { // 申请——>审定
				if (!batchAuthorize(tRgtNo)) {
					boolean locked = isLocked(tRgtNo);
					if (locked) {
						lockState="1";
					}
					SuccessFlag = "0";
					desprition = "操作审定失败";
					return false;
				}
				boolean locked = isLocked(tRgtNo);
				if (locked) {
					lockState="1";
				}
				desprition = "操作审定成功";
				return true;
			} else if ("02".equals(tOperate)) {// 审定结案——>通知给付
				// 查询给付方式
				String payMethodSql = "select TogetherFlag from LLRegister  where rgtno='" + tRgtNo + "'";
				SSRS payMethodSSRS = mExeSQL.execSQL(payMethodSql);
				if (payMethodSSRS.getMaxNumber() > 0) {
					this.payMethod = payMethodSSRS.GetText(1, 1);
				}
				if ("1".equals(payMethod)||"".equals(payMethod)) { // 如果是个人给付
					if (!giveCon()) {
						SuccessFlag = "0";
						desprition = "系统出错";
						return false;
					}else {
						desprition = "通知给付成功";
						ClaimInsertLog();
					}
				} else { // 否则团体给付
					if (!GiveEnsure()) {
						SuccessFlag = "0";
						desprition = "操作给付失败";
						System.out.println("团体给付失败,原因是：" + content);
						return false;
					}else {
						desprition = "通知给付成功";
						ClaimInsertLog();
					}
					map = new HashMap<String, String>();
					String actuGetNo = getActuGetNo(this.tRgtNo, mExeSQL);
					if (stringNull(actuGetNo)) {
						map.put(this.tRgtNo, actuGetNo);
					}
				}
				VData adata = new VData();
				MMap amap = new MMap();
				String insertReconcileInfoSql = "INSERT INTO reconcileInfo SELECT lpad(to_char((nextval for SEQ_RECOINFOID)), 20, '0'), actugetno, paymode, otherno, othernotype,'','0', '', '', '1', '成功', (select EndCaseDate from llregister where rgtno='"
						+ this.tRgtNo
						+ "'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, '"
						+ this.tOperator + "', '" + this.tGEManageCom + "', '" + this.tRgtNo
						+ "', 'lipei', 'get', '', '' FROM ljaget WHERE actugetno IN ( SELECT actugetno FROM ljagetclaim WHERE otherno IN ( SELECT caseno FROM llcase  WHERE RgtNo ='"
						+ this.tRgtNo + "')) ";
				amap.put(insertReconcileInfoSql, "INSERT");
				adata.add(amap);
				PubSubmit puba = new PubSubmit();
				puba.submitData(adata, "UPDATE");
				desprition = "操作给付成功";
				return true;
			} else if ("03".equals(tOperate)) {// 申请撤件
				if (!DealCancel()) {
					SuccessFlag = "0";
					desprition = "申请撤件失败";
					return false;
				}
				desprition = "操作完成";
				return true;
			} else if ("04".equals(tOperate)) {// 回退
				/*
				 * 回退原因 必填 1：受理登陆错误 2：账单录入错误 3：疾病录入错误 4：理赔结果错误 5：补充调查 6：其他
				 * 
				 */
				if (mapRC == null || mapRC.size() < 1) {
					content = "案件信息不能为空";
					SuccessFlag = "0";
					desprition = content;
					System.out.println(content);
					return false;
				}
				if (!CaseBack()) {
					SuccessFlag = "0";
					desprition = "结案回退失败";
					return false;
				}
				desprition = "结案回退成功";
				return true;
			} else if ("05".equals(tOperate)) { // 统一给付给付方式修改
				if (!changePayMode(tBody)) {
					SuccessFlag = "0";
					desprition = "给付方式修改失败";
					return false;
				}
				desprition = "给付方式修改成功";
				return true;
			}else if ("07".equals(tOperate)) { // 申诉纠错个案回退
				if (mapRC == null || mapRC.size() < 1) {
					content = "案件信息不能为空";
					SuccessFlag = "0";
					desprition = content;
					System.out.println(content);
					return false;
				}
				if(!caseBack(mapRC)) {
					SuccessFlag = "0";
					desprition = content;
					System.out.println(content);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			content = "解析报文出错，请核对报文：" + e.toString();
			System.out.println(content);
			SuccessFlag = "0";
			desprition = "解析报文失败";
			return false;
		} finally {
			if ("".equals(SuccessFlag)) {
				SuccessFlag = "1";
			}
			responseXml();
			System.out.println("********content=" + content + "*************");
		}
		return true;
	}

	/**
	 * batchAuthorize(审定)
	 * 
	 * @param rgtNo
	 * @return
	 */
	private boolean batchAuthorize(String rgtNo) {

		System.out.println("*********************sept in batchAuthorize()*********操作审定*****************");
		// 更改操作员 阶段性写死
		tG.Operator = "lipeib";
		// 设置机构编码
		tG.ManageCom = tGEManageCom;
		// 提前更改操作员
		updateOperator();
		VData tVData = new VData();
		SimpleClaimAuditBL tSimpleClaimAuditBL = new SimpleClaimAuditBL();
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		LLCaseSet tLLCaseSet = new LLCaseSet();
		String strSqlss = "";
		tLLRegisterSchema.setRgtNo(rgtNo);
		// 查询当前批次下处于待审定状态下的所有案件。
		String strSql = "select distinct c.caseno " + " FROM LLCASE C " + " WHERE C.RGTNO = '" + rgtNo + "' "
				+ " and not exists(select 1 from llclaimdetail a,lcpol b,lmriskapp cl,llcaserela dl,llsubreport e "
				+ " where a.polno=b.polno and a.riskcode=cl.riskcode and a.caserelano=dl.caserelano "
				+ " and dl.subrptno=e.subrptno "
				+ " and cl.riskprop='G' and ((e.accdate >= b.paytodate) and (a.riskcode not in (select riskcode from lmriskapp where risktype3='7' and risktype <>'M'))) and a.caseno=c.caseno union "
				+ " select 1 from llclaimdetail a,lbpol b,lmriskapp cl,llcaserela dl,llsubreport e "
				+ " where a.polno=b.polno and a.riskcode=cl.riskcode and a.caserelano=dl.caserelano "
				+ " and dl.subrptno=e.subrptno "
				+ " and cl.riskprop='G' and e.accdate >= b.paytodate and a.caseno=c.caseno) ";
		
		strSqlss = " and c.rgtstate in ('03','04','08') order by c.caseno fetch first 1000 rows only ";
		strSql += strSqlss;
		SSRS tSSRS = new SSRS();
		tSSRS = mExeSQL.execSQL(strSql);
		if (!(tSSRS.getMaxNumber() > 0)) {
			content = "确认失败，原因是:该批次下没有查询到符合登录用户" + this.tOperator + "审定条件的案件";
			System.out.println(content);
			return false;
		}
		// 存储待审定案件号
		List<String> caseList = new ArrayList<String>();
		// 存储该批次下的所有案件号
		List<String> strList = new ArrayList<String>();
		for (int i = 0; i < tSSRS.getMaxRow(); i++) {
			strList.add(tSSRS.GetText(i + 1, 1));
		}
		if (caseNoes != null && caseNoes.size() > 0) {
			for (int i = 0; i < caseNoes.size(); i++) {
				if (strList.contains(caseNoes.get(i))) { // 判断案件号是否在此批次下
					LLCaseSchema tLLCaseSchema = new LLCaseSchema();
					tLLCaseSchema.setCaseNo(caseNoes.get(i));
					tLLCaseSet.add(tLLCaseSchema);
					caseList.add(caseNoes.get(i));
				} else {
					content = rgtNo + "该" + caseNoes.get(i) + "笔案件不是待审定状态";
					return false;
				}
			}
		} else {
			for (int i = 0; i < strList.size(); i++) {
				LLCaseSchema tLLCaseSchema = new LLCaseSchema();
				tLLCaseSchema.setCaseNo(strList.get(i));
				tLLCaseSet.add(tLLCaseSchema);
				caseList.add(strList.get(i));
			}
		}
		try {
			System.out.println();
			tVData.add(tLLRegisterSchema);
			tVData.add(tLLCaseSet);
			tVData.add(tG);
			tSimpleClaimAuditBL.submitData(tVData, UW_MAIN);
		} catch (Exception ex) {
			content = "确认失败，原因是:" + ex.toString();
			return false;
		}
		tError = tSimpleClaimAuditBL.mErrors;
		if (!tError.needDealError()) {
			content = "操作完成！";
			tVData.clear();
			tVData = tSimpleClaimAuditBL.getResult();
			
		} else {
			content = "确认失败，原因是:" + tError.getFirstError();
			return false;
		}
		String rgtstate = "";
		String rgtstateSql = "";
		SSRS rgtstateSSRS;
		successCases = new ArrayList<String>();
		for (int i = 0; i < caseList.size(); i++) {
			rgtstateSql = "select rgtstate from  llcase where caseno='" + caseList.get(i) + "'";
			rgtstateSSRS = mExeSQL.execSQL(rgtstateSql);
			if (rgtstateSSRS.getMaxRow() > 0) {
				rgtstate = rgtstateSSRS.GetText(1, 1);
				if (!"09".equals(rgtstate)) {
					this.SuccessFlag = "0";
					CError tError = new CError();
					tError.functionName = caseList.get(i);// 函数名称，这里用来存储案件号。
					if ("01".equals(rgtstate)) {
						tError.errorMessage = "没有理算成功,不能审定，系统自动回退到受理状态。";
						this.mErrors.addOneError(tError);
					}else {
						tError.errorMessage = "案件结案失败";
						this.mErrors.addOneError(tError);
					}
				} else {
					successCases.add(caseList.get(i));
				}
			}
		}
		
		
		return true;
	}

	/**
	 * 
	 * @return
	 */
	private boolean giveCon() {

		System.out.println("******************sept in giveCon()*****************");
		if (!giveConfirm()) // 给付确认（给付方式是个人给付） 前台校验
			return false;
		FlagStr = "";
		tG.ManageCom = tGEManageCom;
		tG.Operator = "lipeia";
		VData tVData = new VData();
		String transact = "INSERT||MAIN";
		LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
		// 如果个人给付受理申请传了三个，到给付接口只传来一个案件，接口会校验住
		// ---BY-gengye-2018/09/17 增加个人给付账户判断
		if (personCases == null || personCases.size() != mLLPersonMsgSet.size()) {
			content = "person节点下 案件号不能为空";
			System.out.println(content);
			return false;
		}
		successCases = new ArrayList<String>(mLLPersonMsgSet.size());
		content = "";
		map = new HashMap<String, String>(mLLPersonMsgSet.size());
		for (int i = 1; i <= mLLPersonMsgSet.size(); i++) {
			String CaseNo = personCases.get(i - 1);
			String RgtNo = tRgtNo;
			String PayMode = mLLPersonMsgSet.get(i).getReceiveMethod();
			String AccName = mLLPersonMsgSet.get(i).getAccountName();
			String BankAccNo = mLLPersonMsgSet.get(i).getAccount();
			String BankCode = mLLPersonMsgSet.get(i).getBankCode();
			String Drawer = mLLPersonMsgSet.get(i).getDrawer();
			String DrawerID = mLLPersonMsgSet.get(i).getDrawerID();
			String tPrePaidFlag = mLLPersonMsgSet.get(i).getAdvanceFlag();
			tVData = new VData();
			tLJAGetInsertBL = new LJAGetInsertBL();
			tLLCaseSchema = new LLCaseSchema();
			tLJAGetSchema = new LJAGetSchema();
			tLLCaseSchema.setCaseNo(CaseNo);
			tLLCaseSchema.setRgtNo(RgtNo);
			tLLCaseSchema.setPrePaidFlag(tPrePaidFlag);
			tLJAGetSchema.setOtherNo(CaseNo);
			tLJAGetSchema.setOtherNoType("5");
			tLJAGetSchema.setAccName(AccName);
			tLJAGetSchema.setBankAccNo(BankAccNo);
			tLJAGetSchema.setPayMode(PayMode);
			tLJAGetSchema.setBankCode(BankCode);
			if (Drawer.equals("") || Drawer == null) {
				Drawer = AccName;
			}
			tLJAGetSchema.setDrawer(Drawer);
			tLJAGetSchema.setDrawerID(DrawerID);
			tVData.add(tLLCaseSchema);
			tVData.add(tLJAGetSchema);
			tVData.add(tG);
			if (!tLJAGetInsertBL.submitData(tVData, transact)) {
				this.SuccessFlag = "0";
				tError = tLJAGetInsertBL.mErrors;
				CError cError = new CError();
				cError.functionName = CaseNo;// 函数名称，这里用来存储案件号。
				cError.errorMessage = "通知给付操作失败:" + tError.getFirstError();
				content += "案件" + CaseNo + ",通知给付操作失败 " + tError.getFirstError() + "\r\t";
				FlagStr = "Fail";
				this.mErrors.addOneError(cError);
				continue;
			} else {
				successCases.add(CaseNo);
				String actuGetNo = getActuGetNo(CaseNo, mExeSQL);
				if (stringNull(actuGetNo)) {
					map.put(CaseNo, actuGetNo);
				}
				content += "案件" + CaseNo + "通知给付成功" + tLJAGetInsertBL.getBackMsg() + "\r\t";
				try {
					String tsql = "select 1 from llclaimdetail where caseno = '" + CaseNo
							+ "' and riskcode in (select code from ldcode where codetype='lxb' ) and exists(select 1 from lcpol where polno=llclaimdetail.polno and polstate!='03070001')  with ur ";
					SSRS tSSRS = mExeSQL.execSQL(tsql);
					if (tSSRS.MaxRow > 0) {
						LPSecondUWBL tLPSecondUWBL = new LPSecondUWBL();
						tLPSecondUWBL.submitData(tVData, "");
					} else {
						LLContAutoDealBL tLLContAutoDealBL = new LLContAutoDealBL();
						tLLContAutoDealBL.submitData(tVData, "");
					}
				} catch (Exception tex) {
					content += "合同终止保存失败，原因是:" + tex.toString() + "，请到理赔合同处理中进行操作!";
					continue;
				}
			}
			System.out.println("循环次数:" + i);
		}
		System.out.println("*********************sept out giveCon()******************************8");

		return true;
	}

	public boolean giveConfirm() {
		System.out.println("--------------------------sept in giveConfirm()----------------------------");

		String querySql = "select c.caseno,c.customername, c.customerno, m.realpay,  (select codename from ldcode where '1480323889000'='1480323889000' and  codetype='paymode' and code=c.casegetmode),  c.CustomerName,c.IDNo,c.bankcode,c.bankaccno,c.accname,c.casegetmode,c.rgtno,  case when c.prepaidflag='1' then '是' else '否' end , 0,(case when c.rgtstate in('11','12')  then m.realpay else 0 end) ,case when c.prepaidflag='1' then '1' else '0' end from llcase c, llclaim m,llregister r  where  c.caseno=m.caseno and c.rgtstate='09' and c.rgtno=r.rgtno and (r.togetherflag is null or r.togetherflag not in ('3','4'))  and c.mngcom like '"
				+ tGEManageCom + "%' and c.rgtno='" + tRgtNo
				+ "' order by c.caseno fetch first 3000 rows only with ur ";
		SSRS querySqlSSRS = mExeSQL.execSQL(querySql);

		if (querySqlSSRS.getMaxNumber() <= 0) {
			content = "该批次次下不存在待给付的案件!";
			return false;
		}
		if (!stringNull(tGEManageCom)) {
			content = "给付操作机构为空！";
			return false;
		}
		if (!stringNull(tOperator)) {
			content = "给付操作员为空！";
			System.out.println(content);
			return false;
		}
		int num = 0;
		if (mLLPersonMsgSet.size() >= 1) {
			for (int i = 1; i <= mLLPersonMsgSet.size(); i++) {
				String name = mLLPersonMsgSet.get(i).getName();
				String id = mLLPersonMsgSet.get(i).getID();
				String sqlStr = "select c.caseno,c.customername, c.customerno, m.realpay,  (select codename from ldcode where '1480054601000'='1480054601000' and  codetype='paymode' and code=c.casegetmode),  c.CustomerName,c.IDNo,c.bankcode,c.bankaccno,c.accname,c.casegetmode,c.rgtno,  case when c.prepaidflag='1' then '是' else '否' end , 0,(case when c.rgtstate in('11','12')  then m.realpay else 0 end) ,case when c.prepaidflag='1' then '1' else '0' end from llcase c, llclaim m,llregister r  where  c.caseno=m.caseno and c.rgtstate='09' and c.rgtno=r.rgtno and (r.togetherflag is null or r.togetherflag not in ('3','4'))  and c.mngcom like '"
						+ tGEManageCom + "%' and c.rgtno='" + tRgtNo + "'and c.customername='" + name
						+ "' and  c.IDNo='" + id + "'  order by c.caseno fetch first 3000 rows only with ur ";
				
				SSRS sqlStrSSRS = mExeSQL.execSQL(sqlStr);
				if (sqlStrSSRS.getMaxNumber() > 0) {
					num = num + 1;
					if ("".equals(mLLPersonMsgSet.get(i).getReceiveMethod())
							|| null == mLLPersonMsgSet.get(i).getReceiveMethod()) {
						content = "给付方式不能为空!";
						System.out.println(content);
						return false;
					}

					if ("1".equals(mLLPersonMsgSet.get(i).getReceiveMethod())
							|| "2".equals(mLLPersonMsgSet.get(i).getReceiveMethod())) {
						if ("".equals(mLLPersonMsgSet.get(i).getDrawer())
								|| null == mLLPersonMsgSet.get(i).getDrawer()) {
							content = "现金支付，领款人不能为空!";
							System.out.println(content);
							return false;
						}
						if ("".equals(mLLPersonMsgSet.get(i).getDrawerID())
								|| null == mLLPersonMsgSet.get(i).getDrawerID()) {
							content = "现金支付，领款人身份证不能为空!";
							System.out.println(content);
							return false;
						}
					} else {
						if ("".equals(mLLPersonMsgSet.get(i).getBankCode())
								|| null == mLLPersonMsgSet.get(i).getBankCode()) {
							content = "银行支付，银行编码不能为空!";
							System.out.println(content);
							return false;
						}
						if ("".equals(mLLPersonMsgSet.get(i).getAccount())
								|| null == mLLPersonMsgSet.get(i).getAccount()) {
							content = "银行支付，银行账号不能为空!";
							System.out.println(content);
							return false;
						}
						if ("".equals(mLLPersonMsgSet.get(i).getAccountName())
								|| null == mLLPersonMsgSet.get(i).getAccountName()) {
							content = "银行支付，账户名不能为空!";
							System.out.println(content);
							return false;
						}
					}
					// ---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---111111
					String PrepaidBalaSql = "select PrepaidBala from LLPrepaidGrpCont a "
							+ " where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and caseno='"
							+ sqlStrSSRS.GetText(1, 1) + "')";
					SSRS PrepaidBalaSqlSSRS = mExeSQL.execSQL(PrepaidBalaSql);
					String tPrepaidBala = "";
					if (PrepaidBalaSqlSSRS.getMaxNumber() > 0) {
						tPrepaidBala = PrepaidBalaSqlSSRS.GetText(1, 1);
					}
					if (("".equals(tPrepaidBala) || null == tPrepaidBala)
							&& "1".equals(mLLPersonMsgSet.get(i).getAdvanceFlag())) {
						content = "案件" + sqlStrSSRS.GetText(1, 1) + "的被保险人投保的保单不存在预付未回销赔款!";
						return false;
					}
					// 细化#2407 理赔模块黑名单控制调整==》#2684关于理赔环节黑名单监测规则修改的需求
					String checkName1 = sqlStrSSRS.GetText(1, 2);
					String checkcaseno1 = sqlStrSSRS.GetText(1, 1);
					String checkId1 = "";
					String strblack = " select IDNo from llcase where caseno= '" + checkcaseno1 + "' with ur";
					SSRS strblackSSRS = mExeSQL.execSQL(strblack);
					if (strblackSSRS.getMaxNumber() > 0) {
						checkId1 = strblackSSRS.GetText(1, 1).trim();
					}
					if (!checkBlacklist(checkName1, checkId1)) {
						return false;
					}
					String checkName2 = mLLPersonMsgSet.get(i).getDrawer();
					String checkId2 = mLLPersonMsgSet.get(i).getDrawerID();
					if (checkName1 != checkName2) {
						if (!checkBlacklist(checkName2, checkId2)) {
							return false;
						}
					}
					// 新增效验，理赔二核时不可给付确认
					String sqlUW = "select rgtstate from llcase where caseno='" + sqlStrSSRS.GetText(1, 1)
							+ "' with ur";
					SSRS sqlUWSSRS = mExeSQL.execSQL(sqlUW);
					if (sqlUWSSRS.getMaxNumber() > 0) {
						if (!"".endsWith(sqlUWSSRS.GetText(1, 1)) || null != sqlUWSSRS.GetText(1, 1)) {
							if ("16".equals(sqlUWSSRS.GetText(1, 1))) {
								content = "理赔二核中不可给付确认，必须等待二核结束！";
								return false;
							}
						}
					}
				}
			}
			if (num == 0) {
				content = "报文中人员的姓名和ID与上载的excel中人员的姓名和ID不符，请检查";
				return false;
			}
		} else {
			content = "没有待给付确定的案件";
			System.out.println(content);
			return false;
		}
		System.out.println("----------------------------sept out giveConfirm()-----------------------------------");
		return true;
	}

	/**
	 * 2684关于理赔环节黑名单监测规则修改的需求
	 * 
	 * @param checkName
	 * @param checkId
	 * @return
	 */
	public boolean checkBlacklist(String checkName, String checkId) {
		System.out.println("--------------------------------sept in checkBlacklist()---------");
		// 需要验证的证件号与人名都为空
		if (("".equals(checkId) || null == checkId) && ("".equals(checkName) || null == checkName)) {
			return true;
		}
		// 1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
		String strblack = " select name,idno from lcblacklist where trim(name)in('" + checkName + "')"
				+ " or trim(idno)in('" + checkId + "') with ur";
		SSRS strblackSSRS = mExeSQL.execSQL(strblack);
		if (strblackSSRS.getMaxNumber() > 0) {
			String strblack1 = " select 1 from lcblacklist where trim(name)in('" + checkName + "') and trim(idno)in('"
					+ checkId + "') with ur";
			SSRS strblack1SSRS = mExeSQL.execSQL(strblack1);
			if (strblack1SSRS.getMaxNumber() > 0) {
				content = "客户或者申请人" + checkName + "为黑名单客户";
				System.out.println(content);
				return false;
			}
		}
		System.out.println("----------------sept out checkBlacklist()-------------------------------");
		return true;
	}

	/**
	 * 团体理赔处理--》团体给付--》团体给付确认（给付方式不是个人给付）
	 * 
	 * @return
	 */
	public boolean GiveEnsure() {
		if (!RgtGiveEnsure()) {// 团体给付确认（给付方式不是个人给付） 前台校验
			return false;
		}
		FlagStr = "";
		tG.ManageCom = this.tGEManageCom;
		tG.Operator = "lipeic";
		VData tVData = new VData();
		boolean flag = false;
		String strOperate = "GIVE|ENSURE";
		if (strOperate.equals("GIVE|ENSURE")) {
			GrpGiveEnsureBL tGrpGiveEnsureBL = new GrpGiveEnsureBL();
			LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
			tLLRegisterSchema.setRgtNo(tRgtNo);

			if ("1".equals(this.payMethod)) {
				tLLRegisterSchema.setPrePaidFlag("1"); // 0或null 不使用预付回销 1-预付回销
			} else {
				tLLRegisterSchema.setPrePaidFlag(""); // 个案标记llcase
			}
			try {
				tVData.add(tLLRegisterSchema);
				tVData.add(tG);
				tGrpGiveEnsureBL.submitData(tVData, strOperate);
			} catch (Exception ex) {
				flag = false;
				content = "确认失败，原因是:" + ex.toString();
				System.out.println(content);
				FlagStr = "Fail";
				return flag;
			}
			if (FlagStr == "") {
				tError = tGrpGiveEnsureBL.mErrors;
				if (!tError.needDealError()) {
					flag = true;
					content = "通知给付成功！" + tGrpGiveEnsureBL.getBackMsg();
					FlagStr = "Succ";
					tVData.clear();
					tVData = tGrpGiveEnsureBL.getResult();
				} else {
					flag = false;
					content = "通知给付失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return flag;
				}
			}
		}
		if (strOperate.equals("QDGIVE|ENSURE")) {
			QDGrpGiveEnsureBL tQDGrpGiveEnsureBL = new QDGrpGiveEnsureBL();
			LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
			tLLRegisterSchema.setRgtNo(tRgtNo);
			if ("1".equals(this.payMethod)) {
				tLLRegisterSchema.setPrePaidFlag("1"); // 0或null 不使用预付回销 1-预付回销
			} else {
				tLLRegisterSchema.setPrePaidFlag(""); // 个案标记llcase
			}
			try {
				tVData.add(tLLRegisterSchema);
				tVData.add(tG);
				tQDGrpGiveEnsureBL.submitData(tVData, strOperate);
			} catch (Exception ex) {
				flag = false;
				content = "通知给付确认失败，原因是:" + ex.toString();
				System.out.println(content);
				FlagStr = "Fail";
				return flag;
			}
			if (FlagStr == "") {
				tError = tQDGrpGiveEnsureBL.mErrors;
				if (!tError.needDealError()) {
					flag = true;
					content = "给付成功！";
					FlagStr = "Succ";
					tVData.clear();
				} else {
					flag = false;
					content = "给付失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return flag;
				}
			}
		}
		return flag;
	}

	/**
	 * 团体理赔处理--》团体给付--》团体给付确认（给付方式不是个人给付） 前台校验
	 */
	public boolean RgtGiveEnsure() {

		System.out.println("-----------------sept in RgtGiveEnsure()------------------");

		String prtnoSql = "SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO, R.RGTANTNAME, R.RGTDATE, (select count(caseno) from llcase where '1479797569000'='1479797569000' and  rgtno=R.rgtno), b.codename,R.rgtstate, coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where rgtno=r.rgtno  ),0), (select codename from ldcode where r.TogetherFlag=code and codetype='lltogetherflag' ) , (select codename from ldcode where r.CaseGetMode=code and codetype='paymode' ),r.RgtantName, r.BankCode,r.AccName,r.BankAccNo, (CASE WHEN r.rgtstate in ('04','05') THEN  (SELECT NVL(SUM(-pay),0) FROM ljagetclaim m WHERE m.otherno=r.rgtno AND m.othernotype='Y' )  ELSE 0 END),  (CASE WHEN r.rgtstate in ('04','05') THEN  (SELECT NVL(SUM(sumgetmoney),0) FROM ljaget m where m.othernotype in ('Y','5') AND m.otherno=r.rgtno)  ELSE 0 END)  FROM LLREGISTER R , ldcode b  WHERE  R.RGTOBJ = '0' AND R.RGTCLASS = '1' AND R.declineflag is null  and b.codetype='llgrprgtstate' and b.code = r.rgtstate  and r.MngCom like '"
				+ tGEManageCom + "%' and  r.rgtno ='" + tRgtNo + "' ";
		SSRS prtnoSqlSSRS = mExeSQL.execSQL(prtnoSql);
		if (prtnoSqlSSRS.MaxNumber > 0) {
			String trgtstate = prtnoSqlSSRS.GetText(1, 9);
			if ("04".equals(trgtstate) || "05".equals(trgtstate)) {
				content = "该团体案件已做完给付确认！";
				return false;
			}
			if ("01".equals(trgtstate)) {
				content = "当前状态下不能做该团体案件的给付确认！";
				return false;
			}
			// 细化#2407 理赔模块黑名单控制调整==》#2684关于理赔环节黑名单监测规则修改的需求
			String rgtno1 = prtnoSqlSSRS.GetText(1, 1);
			String strblack = " select IDNo from LLRegister where rgtno= '" + rgtno1 + "' with ur";
			SSRS arrblack = mExeSQL.execSQL(strblack);
			String checkId1 = "";
			if (arrblack.getMaxNumber() > 0) {
				checkId1 = arrblack.GetText(1, 1).trim();
			}
			String tRgtantName = prtnoSqlSSRS.GetText(1, 13);
			if (!checkBlacklist(tRgtantName, checkId1)) {
				return false;
			}
			String strblack2 = "select customername,IDNO from llcase where rgtno='" + tRgtNo + "'";
			SSRS arrblack2SSRS = mExeSQL.execSQL(strblack2);
			String checkName2 = "";
			String checkId2 = "";
			if (arrblack2SSRS.getMaxNumber() > 0) {
				for (int i = 0; i < arrblack2SSRS.getMaxRow(); i++) {
					checkName2 = arrblack2SSRS.GetText(i + 1, 1).trim();
					checkId2 = arrblack2SSRS.GetText(i + 1, 2).trim();
					if (!checkBlacklist(checkName2, checkId2)) {
						return false;
					}
				}
			}
			// -----------增加个人给付校验--Begin--
			String tTogetherFlag = prtnoSqlSSRS.GetText(1, 11); // 给付方式
			if ("个人给付".equals(tTogetherFlag)) {
				content = "团体案件的给付方式为个人给付!团体批次号为：" + tRgtNo;
				System.out.println(content);
				return false;
			}
			// --------避免给付方式领取方式选择错误，增加给付方式领取方式等信息提示----------End---------
			// --------校验青岛无名单是否已经结案------------Begin------------
			String tGrpContNo = prtnoSqlSSRS.GetText(1, 4); // 合同号
			String QDRgt = "select count(1) from llregister where rgtno='" + tRgtNo + "' and GrpName='青岛市医疗保险管理中心'";
			SSRS QDRgtSSRS = mExeSQL.execSQL(QDRgt);
			if (Integer.valueOf(QDRgtSSRS.GetText(1, 1)) >= 1) {
				String sql1 = "select count(1) from ljsgetclaim where otherno ='" + tRgtNo + "'";
				SSRS sql1SSRS = mExeSQL.execSQL(sql1);
				if (Integer.valueOf(sql1SSRS.GetText(1, 1)) <= 0) {
					content = "请确认团体审批审定后是否进行了【团体结案】操作!" + "团体批次号：" + tRgtNo;
					System.out.println(content);
					// fm.RgtGiveEnsure1.disabled=false;
					return false;
				}

			}
			// ---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
			String PrepaidBalaSql = "select PrepaidBala from LLPrepaidGrpCont a "
					+ " where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and grpcontno='"
					+ tGrpContNo + "')";
			SSRS PrepaidBalaSqlSSRS = mExeSQL.execSQL(PrepaidBalaSql);
			String tPrepaidBala = "";
			if (PrepaidBalaSqlSSRS.getMaxNumber() > 0) {
				tPrepaidBala = PrepaidBalaSqlSSRS.GetText(1, 1);
				if (Integer.valueOf(tPrepaidBala) <= 0) {
					tPrepaidBala = "";
				} else {
					tPrepaidBala = PrepaidBalaSqlSSRS.GetText(1, 1);
				}
			}
			if ((null == tPrepaidBala || "".equals(tPrepaidBala)) && "1".equals(this.payMethod)) {
				content = "被保险人投保的保单不存在预付未回销赔款!";
				System.out.println(content);
				// fm.RgtGiveEnsure1.disabled=false;
				return false;
			}
		} else {
			content = "批次号：" + tRgtNo + "没有满足给付条件的数据！";
			System.out.println(content);
			return false;
		}
		System.out.println("-----------------sept out RgtGiveEnsure()-----------------------");
		return true;
	}

	/**
	 * 
	 * DealCancel(撤件)
	 */
	public boolean DealCancel() {
		String rgtstate = "";
		// 查询批次状态
		String rgtstateSql = "select rgtstate from llregister where rgtno='" + tRgtNo + "' with ur";
		try {
			SSRS rgtstateSSRS = mExeSQL.execSQL(rgtstateSql);
			if (rgtstateSSRS.getMaxNumber() > 0) {
				rgtstate = rgtstateSSRS.GetText(1, 1);
			} else {
				content = "请核对批次号：" + tRgtNo;
				return false;
			}
		} catch (Exception e) {
			content = "系统出错：" + e.toString();
			return false;
		}
		if ("06".equals(rgtstate)) {
			content = "批次号:" + tRgtNo + "还在处理中，请稍候再处理该批次的案件!";
			return false;
		}
		// 表示对案件进行撤件处理
		if (caseNoes != null && caseNoes.size() > 0) {
			boolean dealCancelCase = DealCancelCase(caseNoes);
			if (dealCancelCase) {
				content = "操作完成";
			} else {
				content = "系统异常";
			}
			return dealCancelCase;
		}
		RegisterBL tRegisterBL = new RegisterBL();
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		VData tVData = new VData();
		// 输出参数
		CErrors tError = null;
		String FlagStr = "";
		tLLCaseSchema.setRgtNo(tRgtNo);
		tLLCaseSchema.setCancleReason("5");
		tLLCaseSchema.setCaseNo("");
		tG.ManageCom = this.tGEManageCom;
		tG.Operator = "lipeia";
		String strOperate = "RGTCANCEL";
		try {
			tVData.addElement(tLLCaseSchema);
			tVData.addElement(tG);
			if (tRegisterBL.submitData(tVData, strOperate)) {
				content = "案件撤销成功";
				FlagStr = "Succ";
			} else {
				FlagStr = "Fail";
			}
		} catch (Exception ex) {
			content = "案件撤销失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		// System.out.println("返回的标志是" + FlagStr);
		if (FlagStr == "Fail") {
			tError = tRegisterBL.mErrors;
			if (tError.needDealError()) {
				content = " 撤销失败,原因是" + tError.getFirstError();
				tVData.clear();
				this.SuccessFlag = "0";
				return false;
			}
		} else {
			content = "撤销成功！";
		}
		return true;
	}

	/**
	 * 撤件(操作案件)
	 */
	private boolean DealCancelCase(List<String> caseNoes) {
		if (caseNoes != null && caseNoes.size() > 0) {
			String rgtstate = "";
			String rgtstateSql = "";
			SSRS rgtstateSSRS;
			successCases = new ArrayList<String>();
			for (int i = 0; i < caseNoes.size(); i++) {
				rgtstateSql = "select rgtstate from llcase where caseno='" + caseNoes.get(i) + "' with ur";
				rgtstateSSRS = mExeSQL.execSQL(rgtstateSql);
				if (rgtstateSSRS.getMaxRow() > 0) {
					rgtstate = rgtstateSSRS.GetText(1, 1);
					if (stringNull(rgtstate)) {
						if ("07".equals(rgtstate)) {
							this.SuccessFlag = "0";
							content = "调查状态不能撤件，必须先进行调查回复";
							CError error = new CError();
							error.functionName = caseNoes.get(i);// 将函数名称字段拿来存储案件号
							error.errorMessage = "案件撤件失败,原因是" + content;
							this.mErrors.addOneError(error);
							continue;
						}
						if ("16".equals(rgtstate)) {
							content = "理赔二核中不能撤件，必须等待二核结束！";
							CError error = new CError();
							error.functionName = caseNoes.get(i);// 将函数名称字段拿来存储案件号
							error.errorMessage = "案件撤件失败,原因是" + content;
							this.mErrors.addOneError(error);
							this.SuccessFlag = "0";
							continue;
						}
					}
				}
				SSRS hospcaseSSRS;
				String hospcase = null;
				String sql = "select 1 from LLHospcase where caseno in(select caseno from llcase where rgtno ='"
						+ caseNoes.get(i) + "' and rgttype != '8' ) and claimno is not null ";
				hospcaseSSRS = mExeSQL.execSQL(sql);
				if (hospcaseSSRS.getMaxRow() > 0) {
					hospcase = hospcaseSSRS.GetText(1, 1);
					if (stringNull(hospcase)) {
						content = "医保通案件不允许撤件";
						CError error = new CError();
						error.functionName = caseNoes.get(i);// 将函数名称字段拿来存储案件号
						error.errorMessage = "案件撤件失败,原因是" + content;
						this.mErrors.addOneError(error);
						this.SuccessFlag = "0";
						continue;
					}
				}
				RegisterBL tRegisterBL = new RegisterBL();
				LLCaseSchema tLLCaseSchema = new LLCaseSchema();
				VData tVData = new VData();
				// 输出参数
				CErrors tError = null;
				String FlagStr = "";
				tLLCaseSchema.setRgtNo(tRgtNo);
				tLLCaseSchema.setCancleReason("5");
				tLLCaseSchema.setCaseNo(caseNoes.get(i));
				tG.ManageCom = this.tGEManageCom;
				tG.Operator = "lipeia";
				String strOperate = "CASECANCEL";
				try {
					tVData.addElement(tLLCaseSchema);
					tVData.addElement(tG);
					if (tRegisterBL.submitData(tVData, strOperate)) {
						content = "案件撤销成功";
						successCases.add(caseNoes.get(i));
						FlagStr = "Succ";
					} else {
						FlagStr = "Fail";
					}
				} catch (Exception ex) {
					content = "案件撤销失败，原因是:" + ex.toString();
					CError error = new CError();
					error.functionName = caseNoes.get(i);// 将函数名称字段拿来存储案件号
					error.errorMessage = "案件撤件失败,原因是" + content;
					this.mErrors.addOneError(error);
					this.SuccessFlag = "0";
					continue;
				}
				if (FlagStr == "Fail") {
					tError = tRegisterBL.mErrors;
					if (tError.needDealError()) {
						content = " 撤销失败,原因是" + tError.getFirstError();
						tError = tRegisterBL.mErrors;
						CError error = new CError();
						error.functionName = caseNoes.get(i);// 将函数名称字段拿来存储案件号
						error.errorMessage = "案件撤件失败,原因是" + tError.getFirstError();
						this.mErrors.addOneError(error);
						tVData.clear();
						this.SuccessFlag = "0";
						continue;
					}
				} else {
					content = "操作完成!";
				}
			}
		}
		return true;
	}

	/**
	 * 
	 * CaseBack(案件回退)
	 */
	private boolean CaseBack() {
		String strSql = "select c.caseno FROM LLCASE C, llfeemain d  WHERE C.RGTNO = '" + tRgtNo
				+ "' and d.caseno= c.caseno  and c.rgtstate in ('03','04','08','09') order by c.caseno";
		SSRS tSSRS = new SSRS();
		tSSRS = mExeSQL.execSQL(strSql);
		if (!(tSSRS.getMaxNumber() > 0)) {
			content = "该批次" + tRgtNo + "下没有查询到符合条件的案件信息,请核对";
			return false;
		}
		// 存储该批次下的所有案件号
		List<String> strList = new ArrayList<String>();
		for (int i = 0; i < tSSRS.getMaxRow(); i++) {
			strList.add(tSSRS.GetText(i + 1, 1));
		}
		String tCaseNo[] = new String[strList.size()];
		if (caseNoes != null && caseNoes.size() > 0) {
			for (int i = 0; i < caseNoes.size(); i++) {
				if (strList.contains(caseNoes.get(i))) { // 判断案件号是否在此批次下
					tCaseNo[i] = caseNoes.get(i);
				} else {
					content = tRgtNo + "该批次号下没有" + caseNoes.get(i) + "此案件";
					return false;
				}
			}
		} /*
			 * else { for (int i = 0; i < strList.size(); i++) { tCaseNo[i] =
			 * strList.get(i); } }
			 */
		LLCaseSet tLLCaseSet = new LLCaseSet();
		VData tVData = new VData();
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		tLLRegisterSchema.setRgtNo(tRgtNo);
		SimpleClaimAuditUI tSimpleClaimAuditUI = new SimpleClaimAuditUI();
		if (tCaseNo != null) {
			for (int i = 0; i < tCaseNo.length; i++) {
				if (!stringNull(tCaseNo[i])) {
					break;
				}
				LLCaseSchema tLLCaseSchema = new LLCaseSchema();
				tLLCaseSchema.setCaseNo(tCaseNo[i]);
				tLLCaseSchema.setRgtState("02");
				tLLCaseSchema.setCancleReason(mapRC.get(tCaseNo[i]));
				tLLCaseSet.add(tLLCaseSchema);
			}
		}
		try {
			// tG.Operator="sic001";
			tG.Operator = "lipeib";
			tG.ManageCom = this.tGEManageCom;
			// 准备传输数据 VData
			tVData.add(tLLRegisterSchema);
			tVData.add(tLLCaseSet);
			tVData.add(tG);
			tSimpleClaimAuditUI.submitData(tVData, "BACK||MAIN");
		} catch (Exception ex) {
			content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		// 如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "") {
			tError = tSimpleClaimAuditUI.mErrors;
			if (!tError.needDealError()) {
				content = "回退成功! ";
				FlagStr = "Success";
				tVData.clear();
			} else {
				content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				return false;
			}
		}
		return true;
	}

	/**
	 * upReport(审定上报)
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	@Deprecated
	private boolean upReport(LLCaseSet tLLCaseSet, LLRegisterSchema tLLRegisterSchema) {
		System.out.println("--------sept in upReport()----------操作上报---------------");
		VData tVData = new VData();
		SimpleClaimAuditBL tSimpleClaimAuditBL = new SimpleClaimAuditBL();
		// 输出参数
		// String FlagStr = "";
		String strOperate = "SIGN||MAIN"; // 上报
		String strSqlss = "";
		try {
			tVData.add(tLLRegisterSchema);
			tVData.add(tLLCaseSet);
			tVData.add(tG);
			tSimpleClaimAuditBL.submitData(tVData, strOperate);
		} catch (Exception ex) {
			content = "确认失败，原因是:" + ex.toString();
			return false;
		}
		tError = tSimpleClaimAuditBL.mErrors;
		if (!tError.needDealError()) {
			content = "审定成功！";
			tVData.clear();
			tVData = tSimpleClaimAuditBL.getResult();
		} else {
			content = "确认失败，原因是:" + tError.getFirstError();
			return false;
		}
		System.out.println("------------sept out upReport()------------------------");
		return true;
	}

	public static boolean stringNull(String str) {
		if (!"".equals(str) && null != str) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * responseXml(拼接返回报文)
	 */
	public void responseXml() {
		StringWriter stringWriter = new StringWriter();
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("Claim_Response");
		document.setRootElement(tDateSet);
		// 返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Date date = new Date();
		tSendDate.addText(simDate.format(date));
		Element tSendTime = tMsgResHead.addElement("SendTime");
		tSendTime.addText(simTime.format(date));
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		tSendOperator.addText(this.tOperator);
		Element tMsgType = tMsgResHead.addElement("MsgType");
		Element tRownum = tMsgResHead.addElement("Rownum");
		// 返回报文体样式
		Element tBody = tDateSet.addElement("Body");
		Element tRgtNo = tBody.addElement("RgtNo");
		Element tLockState = tBody.addElement("LockState");
		tLockState.addText(lockState);
		Element tContent = tBody.addElement("Content");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		Element tErrorList = tBody.addElement("ErrorList");
		Element tSuccCases = tBody.addElement("SuccCases");
		if (mErrors != null) {
			for (int i = 0; i < mErrors.getErrorCount(); i++) {
				Element tErrorCase = tErrorList.addElement("ErrorCase");
				Element tcaseno = tErrorCase.addElement("caseno");
				Element tError = tErrorCase.addElement("Error");
				CError cError = mErrors.getError(i);
				tcaseno.addText(cError.functionName);
				tError.addText(cError.errorMessage);
			}
		}
		if (successCases != null) {
			for (int i = 0; i < successCases.size(); i++) {
				Element tCaseNo = tSuccCases.addElement("CaseNo");
				tCaseNo.addText(successCases.get(i));
			}
		}
		if (this.map != null) {
			Element tActuGetNos = tBody.addElement("ActuGetNos");
			for (Entry<String, String> entry : map.entrySet()) {
				Element tItem = tActuGetNos.addElement("Item");
				if ("1".equals(payMethod)) {
					Element tCaseNo = tItem.addElement("CaseNo");
					tCaseNo.addText(entry.getKey());
				} else {
					Element tRgtNO = tItem.addElement("RgtNo");
					tRgtNO.addText(entry.getKey());
				}
				Element tActuGetNo = tItem.addElement("ActuGetNo");
				tActuGetNo.addText(entry.getValue());
			}

		}
		tContent.addText(this.content);
		tSuccessFlag.addText(this.SuccessFlag);
		tRgtNo.addText(this.tRgtNo);
		// 创建写文件方法
		XMLWriter xmlWriter = new XMLWriter(stringWriter);
		// 写入文件
		try {
			xmlWriter.write(document);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		// 关闭
		try {
			xmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		this.responseXml = stringWriter.toString();
	}

	/**
	 * 记录日志表
	 */
	private void ClaimInsertLog() {
		/*VData vdata = new VData();
		MMap mmap = new MMap();
		String sqlLog = "update ClaimInsertLog  set  startDate=to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'),\r\n"
				+ "Starttime= to_char(sysdate, 'HH24:mi:ss'),\r\n"
				+ "endDate=to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'),\r\n"
				+ "endtime= to_char(sysdate, 'HH24:mi:ss'),\r\n" + "tradingState='" + this.SuccessFlag
				+ "',transactionDescription='" + this.desprition + "',transactionType='AG', " + "Caller ='"
				+ this.tOperator + "' where  rgtno='" + this.tRgtNo + "'";
		mmap.put(sqlLog, "INSERT");
		vdata.add(mmap);
		PubSubmit pub = new PubSubmit();
		pub.submitData(vdata, "INSERT");*/
		VData vdata = new VData();
		MMap mmap = new MMap();
		String sqlLog="insert into ClaimInsertLog (Id,Caller,startDate ,Starttime ,endDate ,endtime ,"
				+ "tradingState  ,transactionType ,transactionCode, transactionDescription, grpcontno, rgtno )  "
				+ "values(lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"+this.tOperator
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'),"
				+ "to_char(sysdate, 'HH24:mi:ss'),"
				+ "to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), "
				+ "to_char(sysdate, 'HH24:mi:ss'),'"+this.SuccessFlag+"','AG','"
				+this.tGEManageCom
				+ "','"
				+ this.desprition
				+ "','"
				+this.tPolNo
				+ "','"
				+this.tRgtNo+"')";
		mmap.put(sqlLog, "INSERT");
		vdata.add(mmap);
		PubSubmit pub = new PubSubmit();
		pub.submitData(vdata, "INSERT");
	}

	public String getActuGetNo(String caseNo, ExeSQL exeSQL) {
		String actuGetNoSql = "";
		if (exeSQL != null && stringNull(caseNo)) {
			actuGetNoSql = "select * from  LJAGet where otherno='" + caseNo + "'";
			return exeSQL.getOneValue(actuGetNoSql);
		}
		return null;

	}

	/**
	 * updateOperator(更改操作用户)
	 */
	private void updateOperator() {
		VData adata = new VData();
		MMap mmap = new MMap();
		String updateSql = "update llcase set  handler='" + tG.Operator + "' where rgtno = '" + this.tRgtNo
				+ "' with ur";
		mmap.put(updateSql, "UPDATE");
		adata.add(mmap);
		PubSubmit pubSubmit = new PubSubmit();
		pubSubmit.submitData(adata, "UPDATE");
	}

	/**
	 * @param tBody
	 * @return
	 */
	private boolean changePayMode(Element tBody) {
		if (tBody == null) {
			this.content = "获取银行信息失败";
			return false;
		}
		try {
			Element tPayMode = tBody.element("PayMode");
			String tTogetherFlag = tPayMode.elementText("TogetherFlag");// 给付方式 必填
			String tCaseGetMode = tPayMode.elementText("CaseGetMode");// 领取方式 必填
			if (!stringNull(tTogetherFlag)) {
				this.content = "给付方式不能为空";
				return false;
			}
			String tBankCode = "";
			String tBankAccNo = "";
			String tAccName = "";
			if (!"1".equals(tTogetherFlag)) {
				if (!"1".equals(tCaseGetMode) && !"2".equals(tCaseGetMode)) {
					Element tBankInFo = tPayMode.element("BankInFo");// 银行信息
					tBankCode = tBankInFo.elementText("BankCode");// 银行编码
					tBankAccNo = tBankInFo.elementText("BankAccNo");// 银行账户
					tAccName = tBankInFo.elementText("AccName");// 账户名
					if (!isNull(tBankCode, tBankAccNo, tAccName)) {
						this.content = "银行编码,银行账户,账户名不能为空";
						return false;
					}
				}
			}
			// 走核心流程
			GrpRegisterBL tGrpRegisterBL = new GrpRegisterBL();
			LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
			VData tVData = new VData();
			tG.ManageCom = this.tGEManageCom;
			tG.Operator = "lipeia";
			// 输出参数
			CErrors tError = null;
			String FlagStr = "";
			tLLRegisterSchema.setRgtNo(this.tRgtNo);
			tLLRegisterSchema.setTogetherFlag(tTogetherFlag);
			tLLRegisterSchema.setCaseGetMode(tCaseGetMode);
			tLLRegisterSchema.setBankCode(tBankCode);
			tLLRegisterSchema.setBankAccNo(tBankAccNo);
			tLLRegisterSchema.setAccName(tAccName);
			try {
				tVData.addElement(tLLRegisterSchema);
				tVData.addElement(tG);
				if (tGrpRegisterBL.submitData(tVData, UPDATE_RgtPayMode)) {
					content = "给付方式修改成功";
					FlagStr = "Succ";
				} else {
					FlagStr = "Fail";
				}
			} catch (Exception ex) {
				content = "给付方式修改失败，原因是:" + ex.toString();
				FlagStr = "Fail";
			}
			System.out.println("返回的标志是" + FlagStr);
			if (FlagStr == "Fail") {
				tError = tGrpRegisterBL.mErrors;
				if (tError.needDealError()) {
					content = " 修改失败,原因是" + tError.getFirstError();
					FlagStr = "Fail";
					tVData.clear();
					return false;
				}
			} else {
				content = "修改成功！";
				FlagStr = "Succ";
			}

		} catch (Exception e) {
			content = "系统异常：" + e.toString();
			return false;
		}
		return true;
	}

	/**
	 * 提调
	 */
	private boolean RgtSurvey(String caseno) {
		if (tLLSurveySchema == null) {
			this.content = "获取提调数据失败";
			return false;
		}
		String llfeemainSql = "  select customerno,customername from  llfeemain  where caseno='" + caseno + "'";
		SSRS llfeemainSSRS = mExeSQL.execSQL(llfeemainSql);
		if (llfeemainSSRS.getMaxRow() < 1) {
			this.content = "未查询到案件信息，请检查：" + caseno;
			return false;
		}
		String customerno = llfeemainSSRS.GetText(1, 1);
		String customerName = llfeemainSSRS.GetText(1, 2);
		if (!isNull(customerno)) {
			this.content = "客户号码不能为空!";
			return false;
		}
		if (!isNull(tLLSurveySchema.getContent())) {
			this.content = "调查内容不能为空!";
			return false;
		}

		LLInqCertificateSet tLLInqCertificateSet = new LLInqCertificateSet();
		RgtSurveyUI tRgtSurveyUI = new RgtSurveyUI();
		LLCaseDB tLLCaseDB = new LLCaseDB();
		String tRgtState = "";
		// String tSurveyState = "";
		// 输出参数
		CErrors tError = null;
		String FlagStr = "";
		String transact = "";
		tG.Operator = "lipeia";
		tG.ManageCom = this.tGEManageCom;
		tG.ComCode=this.tGEManageCom;

		// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录

		tLLSurveySchema.setOtherNo(caseno);
		tLLSurveySchema.setSubRptNo("0000");
		tLLSurveySchema.setStartPhase("0");

		tLLCaseDB.setCaseNo(caseno);
		tLLCaseDB.getInfo();
		tRgtState = tLLCaseDB.getRgtState();
		// 案件提调时是什么状态，在调查表中就存为该状态，且在查讫时，统一将案件处理人修改为提调人
		// add by Houyd #1738 社保调查
		// if
		// ("01".equals(tRgtState)||"02".equals(tRgtState)||"07".equals(tRgtState)||"08".equals(tRgtState)){
		// tSurveyState = "0";
		// }else{
		// tSurveyState = "1";
		// }
		tLLSurveySchema.setRgtState(tRgtState);
		tLLSurveySchema.setCustomerNo(customerno);
		tLLSurveySchema.setCustomerName(customerName);
		tLLSurveySchema.setSurveyFlag("0");/** 调查报告状态 */

		try {
			VData tVData = new VData();
			tVData.add(tLLSurveySchema);
			tVData.add(tLLInqCertificateSet);
			tVData.add(tG);
			tRgtSurveyUI.submitData(tVData, "INSERT||MAIN");
		} catch (Exception ex) {
			content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			return false;
		}

		if (FlagStr == "") {
			tError = tRgtSurveyUI.mErrors;
			if (!tError.needDealError()) {
				content = " 保存成功! ";
				FlagStr = "Success";
			} else {
				content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				return false;
			}
		}
		return true;
	}
	/**
	 * 存在字符串为null或者空字符串，则返回null
	 * @param str
	 * @return
	 */
	public static boolean isNull(String... str) {
		if (str == null)
			return false;
		for (int i = 0; i < str.length; i++)
			if (!stringNull(str[i]))
				return false;
		return true;
	}
	
	/**
	 * 申诉纠错案件回退
	 * @param caseNo  案件号
	 * @param returnReason  回退原因
	 * @return
	 */
	public boolean caseBack(Map<String, String> mapRC) {
		if (caseNoes == null || caseNoes.size() <1) {
			content="回退案件号不能为空";
			return false;
		}
		String caseNo=caseNoes.get(0);
		String returnReason=mapRC.get(caseNo);
		if (!isNull(caseNo)) {
			content="回退案件号不能为空";
			return false;
		}
		if (!isNull(returnReason)) {
			content="回退原因不能为空";
			return false;
		}
		CaseBack caseback=new CaseBack();
		caseback.setManagecom(tGEManageCom);
		boolean isSucc = caseback.caseBack(caseNo,returnReason,"04");
		if (!isSucc) {
			content=caseback.getmErrors().getFirstError();
			return false;
		}
		return true;
	}
	
	
	/**
	 * 判断批次是否被锁 true:被锁  false：正常
	 */
	private boolean isLocked(String rgtno) {
		 String lllockSql= "select 1  from lllock where rgtno='"+rgtno+"' and state='1' "
		 		+ " fetch first 3000 rows only with ur";
		 SSRS lllockSSRS = mExeSQL.execSQL(lllockSql);
		 return lllockSSRS.getMaxRow()>0;
	}
	
	
	private String getXsd() {
		String str="<?xml version=\"1.0\" encoding=\"GBK\"?>\r\n" + 
				"\r\n" + 
				"<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">  \r\n" + 
				"  <xs:element name=\"MedicalAdjustment_Request\"> \r\n" + 
				"    <xs:complexType> \r\n" + 
				"      <xs:sequence> \r\n" + 
				"        <xs:element name=\"head\"> \r\n" + 
				"          <xs:annotation> \r\n" + 
				"            <xs:documentation/> \r\n" + 
				"          </xs:annotation>  \r\n" + 
				"          <xs:complexType> \r\n" + 
				"            <xs:sequence> \r\n" + 
				"              <xs:element name=\"BatchNo\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendDate\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendTime\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"BranchCode\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"SendOperator\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"MsgType\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"Rownum\" type=\"xs:string\"/> \r\n" + 
				"            </xs:sequence> \r\n" + 
				"          </xs:complexType> \r\n" + 
				"        </xs:element>  \r\n" + 
				"        <xs:element name=\"body\"> \r\n" + 
				"          <xs:complexType> \r\n" + 
				"            <xs:sequence> \r\n" + 
				"              <xs:element name=\"PolNo\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"RgtNo\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"Operate\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"Operator\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"GEManageCom\" type=\"xs:string\"/>  \r\n" + 
				"              <xs:element name=\"caseList\"> \r\n" + 
				"                <xs:complexType> \r\n" + 
				"                  <xs:sequence> \r\n" + 
				"                    <xs:element name=\"case\" minOccurs=\"0\" maxOccurs=\"999999999\"> \r\n" + 
				"                      <xs:complexType> \r\n" + 
				"                        <xs:sequence> \r\n" + 
				"                          <xs:element name=\"caseno\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"backstate\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"reason\" type=\"xs:string\" minOccurs=\"0\"/> \r\n" + 
				"                        </xs:sequence> \r\n" + 
				"                      </xs:complexType> \r\n" + 
				"                    </xs:element> \r\n" + 
				"                  </xs:sequence> \r\n" + 
				"                </xs:complexType> \r\n" + 
				"              </xs:element>  \r\n" + 
				"              <xs:element name=\"Person\" minOccurs=\"1\" maxOccurs=\"5000\"> \r\n" + 
				"                <xs:complexType> \r\n" + 
				"                  <xs:sequence> \r\n" + 
				"                    <xs:element name=\"caseno\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"ReceiptNo\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"PhoneNumber\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"ReceiveMethod\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"BankCode\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"SignBank\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"Account\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"AccountName\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"DeclareAmount\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"AdvanceFlag\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"Name\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"ID\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"Drawer\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"DrawerID\" type=\"xs:string\" minOccurs=\"0\"/> \r\n" + 
				"                  </xs:sequence> \r\n" + 
				"                </xs:complexType> \r\n" + 
				"              </xs:element>  \r\n" + 
				"              <xs:element name=\"PayMode\" minOccurs=\"0\" maxOccurs=\"1\"> \r\n" + 
				"                <xs:complexType> \r\n" + 
				"                  <xs:sequence> \r\n" + 
				"                    <xs:element name=\"TogetherFlag\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"CaseGetMode\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                    <xs:element name=\"BankInFo\" minOccurs=\"1\" maxOccurs=\"1\"> \r\n" + 
				"                      <xs:complexType> \r\n" + 
				"                        <xs:sequence> \r\n" + 
				"                          <xs:element name=\"BankCode\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"BankAccNo\" type=\"xs:string\" minOccurs=\"0\"/>  \r\n" + 
				"                          <xs:element name=\"AccName\" type=\"xs:string\" minOccurs=\"0\"/> \r\n" + 
				"                        </xs:sequence> \r\n" + 
				"                      </xs:complexType> \r\n" + 
				"                    </xs:element> \r\n" + 
				"                  </xs:sequence> \r\n" + 
				"                </xs:complexType> \r\n" + 
				"              </xs:element> \r\n" + 
				"            </xs:sequence> \r\n" + 
				"          </xs:complexType> \r\n" + 
				"        </xs:element> \r\n" + 
				"      </xs:sequence> \r\n" + 
				"    </xs:complexType> \r\n" + 
				"  </xs:element> \r\n" + 
				"</xs:schema>\r\n" + 
				"";
		return str;
	}
	
}
