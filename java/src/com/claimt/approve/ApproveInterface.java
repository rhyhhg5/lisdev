/**
k * @Title: Approve.java
 * @Package com.claimt
 */
package com.claimt.approve;

import java.io.FileInputStream;
import java.io.InputStream;



/**
  * 理赔审定
  * @ClassName: ApproveInterface
  * @author liuzehong
  * @date 2018年9月17日 上午9:56:48
  * @version V1.0
 *
 */
public class ApproveInterface {
	
	private Approve approve=null;
     
	public String service(String xml) {
		approve=new Approve();
		approve.parseXml(xml);  //解析报文， 进行相关业务操作·
		return approve.getResponseXml();	
	}
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
	}
	
}
