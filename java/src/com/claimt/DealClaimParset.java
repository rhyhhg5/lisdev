package com.claimt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LLPersonMsgSchema;
import com.sinosoft.lis.schema.LLSendMsgSchema;
import com.sinosoft.lis.vschema.LLPersonMsgSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/**
 * 报文解析，团体理赔
 * @author 樊庆
 * 2016-12-23
 */
public class DealClaimParset {

//	public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private MMap mMap=new MMap();
    private String responseXml = "";
	public String getResponseXml() {
		return responseXml;
	}
	private LLSendMsgSchema tLLSendMsgSchema = new LLSendMsgSchema();
	private LLPersonMsgSet mLLPersonMsgSet = new LLPersonMsgSet();
	String Content = "";
	
	/**
	 * 解析接收到的报文信息
	 * @param path
	 * 2016.11.18
	 */
	public boolean parseXml(String xmlMsg)
	{
		if(null == xmlMsg || "".equals(xmlMsg)){
			Content = "发送的报文为空！";
			System.out.println(Content);
			vDateToXml("0",tLLSendMsgSchema);
			return false;
		}
		System.out.println("接收到报文：\n"+xmlMsg);
		try
		{
			SAXReader reader = new SAXReader();
			Document doc;
			if(".xml".equals(xmlMsg.substring(xmlMsg.length()-4))){
				System.out.println(xmlMsg.substring(xmlMsg.length()-4));
				//文件形式 xmlMsg 为路径
				File f = new File(xmlMsg);
				reader.setEncoding("GBK");
				doc = reader.read(f);
				System.out.println("文件============");
			}else{
				//字符串形式xmlMsg 为报文内容
				doc = DocumentHelper.parseText(xmlMsg); 
				mMap.put("requestXML", xmlMsg.trim());
				System.out.println("字符串============");
//				System.out.println(mResult.getObjectByObjectName("String", 0));
			}
			
			
			Element root = doc.getRootElement();
			//获取报文头
			Element tHead = root.element("head");

			String tBatchNo = tHead.elementText("BatchNo"); 
			String tSendDate = tHead.elementText("SendDate");
			String tSendTime = tHead.elementText("SendTime");
			String tBranchCode = tHead.elementText("BranchCode");
			String tSendOperator = tHead.elementText("SendOperator");
			String tMsgType = tHead.elementText("MsgType");
			String tRownum = tHead.elementText("Rownum");

			if("".equals(tBatchNo) || "".equals(tSendDate) || "".equals(tSendTime) || 
					"".equals(tBranchCode) || "".equals(tSendOperator) || "".equals(tMsgType)){
				Content = "报文解析失败，报文头信息缺失！";
				System.out.println(Content);
				vDateToXml("0",tLLSendMsgSchema);
				return false;
			}
			tLLSendMsgSchema.setBatchNo(tBatchNo);
			tLLSendMsgSchema.setSendDate(tSendDate);
			tLLSendMsgSchema.setSendTime(tSendTime);
			tLLSendMsgSchema.setBranchCode(tBranchCode);
			tLLSendMsgSchema.setSendOperator(tSendOperator);
			tLLSendMsgSchema.setMsgType(tMsgType);
			tLLSendMsgSchema.setRownum(tRownum);
			System.out.println(tBatchNo + "=" + tSendDate);
			//获取报文体
			Element tBody = root.element("body");
			tLLSendMsgSchema.setApplno(tBody.elementText("Applno"));
			tLLSendMsgSchema.setExcelAddress(tBody.elementText("ExcelAddress"));
			
			String [] s = tLLSendMsgSchema.getExcelAddress().split("/");
			String ePath = "";
			for(int i = 0;i<s.length-1;i++){
				ePath += s[i] + "/";
			}
//			System.out.println("文件路径： " + ePath.substring(0, ePath.length()-1));
//			System.out.println("文件名： " + s[s.length-1]);
			
			tLLSendMsgSchema.setAcceptMethod(tBody.elementText("AcceptMethod"));
			tLLSendMsgSchema.setPayMethod(tBody.elementText("PayMethod"));
			System.out.println("tttt: "+tBody.elementText("Applno") + "=" + tBody.elementText("ExcelAddress"));
			tLLSendMsgSchema.setPhoneNumber(tBody.elementText("PhoneNumber"));
			tLLSendMsgSchema.setEMail(tBody.elementText("E-Mail"));
			tLLSendMsgSchema.setReceiveMethod(tBody.elementText("ReceiveMethod"));
			tLLSendMsgSchema.setBankCode(tBody.elementText("BankCode"));
			tLLSendMsgSchema.setSignBank(tBody.elementText("SignBank"));
			tLLSendMsgSchema.setAccount(tBody.elementText("Account"));
			tLLSendMsgSchema.setAccountName(tBody.elementText("AccountName"));
			tLLSendMsgSchema.setDeclareAmount(tBody.elementText("DeclareAmount"));
			tLLSendMsgSchema.setAdvanceFlag(tBody.elementText("AdvanceFlag"));
//			System.out.println("团体给付： " + tBody.elementText("PhoneNumber") +
//					"=" + tBody.elementText("E-Mail"));
			tLLSendMsgSchema.setManageCom(tBody.elementText("ManageCom"));
			tLLSendMsgSchema.setOperate(tBody.elementText("Operate"));
			tLLSendMsgSchema.setAppPeoples(tBody.elementText("AppPeoples"));
			tLLSendMsgSchema.setPostCode(tBody.elementText("PostCode"));
			tLLSendMsgSchema.setIDType(tBody.elementText("IDType"));
			tLLSendMsgSchema.setIDNo(tBody.elementText("IDNo"));
			tLLSendMsgSchema.setRemark(tBody.elementText("Remark"));
			tLLSendMsgSchema.setGrpRemark(tBody.elementText("GrpRemark"));
			tLLSendMsgSchema.setGEManageCom(tBody.elementText("GEManageCom"));
			tLLSendMsgSchema.setGEOperate(tBody.elementText("GEOperate"));
			
			if("1".equals(tBody.elementText("PayMethod"))){
				//个人给付
				List<Element> PersonList = tBody.selectNodes("Person");
//				Element tBodyPerson;
				for (int i = 0; i < PersonList.size(); i++) 
				{
					LLPersonMsgSchema tLLPersonMsgSchema = new LLPersonMsgSchema();
					tLLPersonMsgSchema.setPhoneNumber(PersonList.get(i).elementText("PhoneNumber"));
					tLLPersonMsgSchema.setEMail(PersonList.get(i).elementText("E-Mail"));
					tLLPersonMsgSchema.setReceiveMethod(PersonList.get(i).elementText("ReceiveMethod"));
					tLLPersonMsgSchema.setBankCode(PersonList.get(i).elementText("BankCode"));
					tLLPersonMsgSchema.setSignBank(PersonList.get(i).elementText("SignBank"));
					tLLPersonMsgSchema.setAccount(PersonList.get(i).elementText("Account"));
					tLLPersonMsgSchema.setAccountName(PersonList.get(i).elementText("AccountName"));
					tLLPersonMsgSchema.setDeclareAmount(PersonList.get(i).elementText("DeclareAmount"));
					tLLPersonMsgSchema.setAdvanceFlag(PersonList.get(i).elementText("AdvanceFlag"));
					tLLPersonMsgSchema.setName(PersonList.get(i).elementText("Name"));
					tLLPersonMsgSchema.setID(PersonList.get(i).elementText("ID"));
					tLLPersonMsgSchema.setDrawer(PersonList.get(i).elementText("Drawer"));
					tLLPersonMsgSchema.setDrawerID(PersonList.get(i).elementText("DrawerID"));
					tLLPersonMsgSchema.setReceiptNo(PersonList.get(i).elementText("ReceiptNo"));
					
					System.out.println("个人给付： " + PersonList.get(i).elementText("PhoneNumber") +
							"=" + PersonList.get(i).elementText("E-Mail"));
					mLLPersonMsgSet.add(tLLPersonMsgSchema);
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("报文解析成功！");
		tLLSendMsgSchema.setOperate("cm0002");
		mResult.add(xmlMsg);
		mResult.add(tLLSendMsgSchema);
		mResult.add(mLLPersonMsgSet);
		//调用到可导入人员校验
		DealClaimt tt = new DealClaimt();
		//调用理赔流程
	    boolean submitDate = tt.submitDate(mResult,"INSERT||MAIN");	
	    if (!submitDate) {
	    	 responseXml = tt.getResponseXml();
	    	 return false;
		}
		//创建线程调用业务处理类        
	    DoXmlt tDoXml = new DoXmlt();
		VData m = new VData();
		responseXml = tt.getResponseXml();
		m = tt.getmResult();
		m.add(tLLSendMsgSchema);
//		m.add(mMap);
		m.add(xmlMsg);
		tDoXml.setmResult(m);
//		Thread t =  new Thread(tDoXml);
//		t.start();
		boolean runin = tDoXml.runin();
		responseXml = tDoXml.getResponseXml();
		return runin;
	}

	
	
	/**
	 * 返回报文信息
	 * @param mLLSendMsgSchema
	 * 2016.11.21
	 */
	 private String vDateToXml(String str,LLSendMsgSchema mLLSendMsgSchema) {
		StringWriter stringWriter = new StringWriter();  
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("MedicalAdjustment_Request");
		document.setRootElement(tDateSet);
		//返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Element tSendTime = tMsgResHead.addElement("SendTime");
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		Element tMsgType = tMsgResHead.addElement("MsgType");
		Element tRownum = tMsgResHead.addElement("Rownum");
		//返回报文体样式
		Element tBody = tDateSet.addElement("ApplyNo");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		Element tDescription = tBody.addElement("Description");
		Element tReserveField1 = tBody.addElement("ReserveField1");
		Element tReserveField2 = tBody.addElement("ReserveField2");
		Element tReserveField3 = tBody.addElement("ReserveField3");
		
		//报文内容填充
		try{
			if(mLLSendMsgSchema.getBatchNo() != null && !"".equals(mLLSendMsgSchema.getBatchNo())){
				tBatchNo.addText(mLLSendMsgSchema.getBatchNo());
			}
			tSendDate.addText(PubFun.getCurrentDate());
			tSendTime.addText(PubFun.getCurrentTime());
			if(!"".equals(mLLSendMsgSchema.getBranchCode()) && mLLSendMsgSchema.getBranchCode() != null){
				tBranchCode.addText(mLLSendMsgSchema.getBranchCode());
			}
			tSendOperator.addText(mLLSendMsgSchema.getOperate());
			if(!"".equals(mLLSendMsgSchema.getMsgType()) && mLLSendMsgSchema.getMsgType() != null){
				tMsgType.addText(mLLSendMsgSchema.getMsgType());
			}
			tRownum.addText(Integer.toString(mLLSendMsgSchema.getRownum()));
			
			tSuccessFlag.addText(str);
			if("1".equals(str)){
				tDescription.addText("successful");
			}else{
				tDescription.addText(Content);
			}
			
			
			OutputFormat format = new OutputFormat("    ",true);
			// 设置编码
			format.setEncoding("GBK");
			// 设置换行 
			format.setNewlines(true); 
	        // 生成缩进 
			format.setIndent(true); 
			//创建写文件方法  
	        XMLWriter xmlWriter = new XMLWriter(stringWriter,format);  
	        //写入文件  
	        xmlWriter.write(document);  
	        //关闭  
	        xmlWriter.close(); 
	        // 输出xml 
	        System.out.println(stringWriter.toString());
	        
		}catch(Exception e){
			e.printStackTrace();
		}
		responseXml = stringWriter.toString();
		return stringWriter.toString();
	}
	 
	 public boolean checkXml(LLSendMsgSchema mLLSendMsgSchema){
			System.out.println("===========  This is AskSave() Start  =============");
			if("".equals(mLLSendMsgSchema.getOperate())||null==mLLSendMsgSchema.getOperate()){
				Content = "操作者属的机构不能为空";
				System.out.println("操作者属的机构不能为空");
			    return false;
		     }
		    if("".equals(mLLSendMsgSchema.getManageCom())||null==mLLSendMsgSchema.getManageCom()){
		    	Content = "操作者属的机构不能为空";
				System.out.println("操作者属的机构不能为空");
				return false;
			} 
			if("".equals(mLLSendMsgSchema.getApplno())||null==mLLSendMsgSchema.getApplno()){
				Content = "保单号码不能为空";
				System.out.println("保单号码不能为空");
				return  false;
			}
			String sql1="select  a.customerno, a.name, g.grpcontno,g.Peoples2, a.claimbankcode, a.claimbankaccno, a.claimaccname, a.AddressNo,'', b.riskcode from lcgrpcont g, LCGrpAppnt a,lcgrppol b where '1480490147000'='1480490147000' and  a.grpcontno = g.grpcontno and g.appflag = '1' and b.grpcontno=g.grpcontno  and g.GrpContNo='"+mLLSendMsgSchema.getApplno()+"'  fetch first 3000 rows only with ur ";
			ExeSQL ttExeSQL=new ExeSQL();
			SSRS sql1SSRS=ttExeSQL.execSQL(sql1);
			//团体号 
			String CustomerNo="";
			//投保人数
			String PeopleNo="";
			//单位名称
			String GrpName=""; 
			String addressno="";
			if(sql1SSRS.getMaxNumber()>0){
				CustomerNo=sql1SSRS.GetText(1, 1);
				PeopleNo=sql1SSRS.GetText(1, 4);
				GrpName=sql1SSRS.GetText(1, 2);
				addressno=sql1SSRS.GetText(1, 8);
			}else{
				Content = "保单号码输入有误";
				System.out.println("保单号码输入有误");
				return false;
			}
			String sql2="select linkman1,phone1,GrpAddress from LCGrpAddress where '1480490147000'='1480490147000' and  customerno = '"+CustomerNo+"' and addressno = '"+addressno+"' fetch first 3000 rows only with ur ";
			SSRS sql2SSRS=ttExeSQL.execSQL(sql2);
			String linkman1="";
			String phone1="";
			if(sql2SSRS.getMaxNumber()>0){
				linkman1=sql2SSRS.GetText(1, 1);
				phone1=sql2SSRS.GetText(1, 2);
			}else{
				Content = "通过团体号查询团体信息失败";
				System.out.println("通过团体号查询团体信息失败");
				return false;
			}
			if("".equals(CustomerNo)||null==CustomerNo){
				Content = "团体号不能为空";
				System.out.println("团体号不能为空");
				return  false;
			}
			if("".equals(PeopleNo)||null==PeopleNo){
				System.out.println("投保人数不能为空");
				return  false;
			}
			if("".equals(mLLSendMsgSchema.getAcceptMethod())||null==mLLSendMsgSchema.getAcceptMethod()){
				Content = "受理方式不能为空";
				System.out.println("受理方式不能为空");
				return false;
			}
			if("".equals(mLLSendMsgSchema.getAppPeoples())){
				Content = "申请人数不能为空";
				System.out.println("申请人数不能为空");
				return false;
			}
			if("".equals(mLLSendMsgSchema.getPayMethod())||null==mLLSendMsgSchema.getPayMethod()){
				Content = "给付方式不能为空";
				System.out.println("给付方式不能为空");
				return false;
			}
			if("".equals(mLLSendMsgSchema.getDeclareAmount())||null==mLLSendMsgSchema.getDeclareAmount()){
				Content = "申报金额不能为空";
				System.out.println("申报金额不能为空");
				return false;
			}
			if(!"1".equals(mLLSendMsgSchema.getReceiveMethod()) && !"2".equals(mLLSendMsgSchema.getReceiveMethod()) && !"3".equals(mLLSendMsgSchema.getReceiveMethod()) && 
					!"4".equals(mLLSendMsgSchema.getReceiveMethod()) && !"11".equals(mLLSendMsgSchema.getReceiveMethod())){
				Content = "赔款领取方式输入有误";
				System.out.println("赔款领取方式输入有误");
				return false;
			}
			//校验团体保单特约信息 长度
			System.out.println(mLLSendMsgSchema.getGrpRemark().length());
			if(mLLSendMsgSchema.getGrpRemark().length()>1600)
			{
				Content = "团体保单特约信息超过1600个汉字,无法保存!";
				System.out.println("团体保单特约信息超过1600个汉字,无法保存!");
				return false;
			}
			//校验是否存在预付未回销赔款
			String  PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a where grpcontno='"+ mLLSendMsgSchema.getApplno()+"'";      			
			ExeSQL tExeSQL = new ExeSQL();
			String tPrepaidBala = tExeSQL.getOneValue(PrepaidBalaSql);
			System.out.println(PrepaidBalaSql);
			 if((null==tPrepaidBala  || "".equals(tPrepaidBala) )&&"true".equals(mLLSendMsgSchema.getAdvanceFlag()) )
			   {
				 	Content = "保单不存在预付未回销赔款!";
				 	System.out.println("保单不存在预付未回销赔款!");
			   		return false ;
			   }
			 //校验有没有该团体客户信息
			 if((null!=CustomerNo && !"".equals(CustomerNo))||(null !=mLLSendMsgSchema.getApplno() && !"".equals(mLLSendMsgSchema.getApplno()))||(null !=GrpName && !"".equals(GrpName))){
				   String tSQL = "select  a.customerno, a.name, g.grpcontno,g.Peoples2,"
				   		+ " a.claimbankcode, a.claimbankaccno, a.claimaccname, "
				   		+ "a.AddressNo,'', b.riskcode from lcgrpcont g, LCGrpAppnt "
				   		+ "a,lcgrppol b where '1479367799000'='1479367799000' and "
				   		+ " a.grpcontno = g.grpcontno and g.appflag = '1' and "
				   		+ "b.grpcontno=g.grpcontno  ";
				   if(null!=CustomerNo && !"".equals(CustomerNo)){
					   tSQL=tSQL+" and g.AppntNo='"+CustomerNo+"'" ;
				   }
				   if(null !=GrpName && !"".equals(GrpName)){
					   tSQL=tSQL+" and a.Name='"+GrpName+"'" ;
				   }
				   if(null !=mLLSendMsgSchema.getApplno() && !"".equals(mLLSendMsgSchema.getApplno())){
					   tSQL=tSQL+" and g.GrpContNo='"+mLLSendMsgSchema.getApplno()+"'" ;
				   }
				   tSQL=tSQL+" fetch first 3000 rows only with ur";
			       // ExeSQL tExeSQL = new ExeSQL();
			        SSRS tSSRS = tExeSQL.execSQL(tSQL); 
			        System.out.println(tSQL);
			        if(tSSRS.getMaxNumber()>0){
				    }
				    else
				    {
				    	Content = "没有该团体客户信息！";
				    	System.out.println("没有该团体客户信息！");
						return false;
				    }
			 }
		/*	校验各种保全	 */
			//提示阻断
				String tIngSql1 = " select b.grpcontno,b.edortype,b.edorno,"
						 + " (select edorname from lmedoritem where edorcode=b.edortype fetch first 1 rows only) "
						 + " from lpedorapp a , lpgrpedoritem b "
						 + " where a.edoracceptno= b.edoracceptno "
						 + " and a.edorstate !='0' " //正在进行中的保全项目
						 + " and b.grpcontno ='"+mLLSendMsgSchema.getApplno()+"'"
						//+ " and b.grpcontno ='0000002101'"
						 + " and b.edortype in ('WT','XT','GA','TQ','TA','SG','CT','TF','LQ','ZB')"
						 + " group by b.grpcontno,b.edortype,b.edorno ";
				 SSRS tIngSql1SSRS = tExeSQL.execSQL(tIngSql1); 
				 System.out.println(tIngSql1);
				if(tIngSql1SSRS.getMaxNumber()>0){
					for(int i=0;i<tIngSql1SSRS.MaxRow;i++){
						Content = "该团单正在进行"+tIngSql1SSRS.GetText(i+1, 4)+"保全操作,工单号为："+tIngSql1SSRS.GetText(i+1, 3)+"";
						System.out.println("该团单正在进行"+tIngSql1SSRS.GetText(i+1, 4)+"保全操作,工单号为："+tIngSql1SSRS.GetText(i+1, 3)+"");
						return false;
					}
				}
				//满期处理
				String tMJSql = " select temp.GrpContNo,temp.edorName,temp.edorstate "
				           + " from "
				           + " ( "
				           + " select a.grpcontno GrpContNo,'团单满期给付' edorName," //团单满期给付
						   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate"
						   + " from ljsgetdraw a where feefinatype='TF' and riskcode='170206' "
						   + " and grpcontno='"+mLLSendMsgSchema.getApplno()+"' "
						   + " ) as temp "
						   + " group by temp.GrpContNo,temp.edorName,temp.edorstate "
						   + " with ur "; 
				   SSRS tMJSqlSSRS = tExeSQL.execSQL(tMJSql); 
				   System.out.println(tMJSql);
					if(tMJSqlSSRS.getMaxNumber()>0){
						for(int i=0;i<tMJSqlSSRS.MaxRow;i++){
							if(tMJSqlSSRS.GetText(i+1, 3)!="0"){
								Content = "该团单正在进行"+tMJSqlSSRS.GetText(i+1, 2)+"保全操作";
								System.out.println("该团单正在进行"+tMJSqlSSRS.GetText(i+1, 2)+"保全操作");
								return false;
							}
						}
					}
					//所在团单正在定期结算
					String tDJSql = " select distinct a.contno "
							   + " from lgwork a "
							   + " where a.contno = '"+mLLSendMsgSchema.getApplno()+"' "
							   + " and a.typeno like '06%' and a.statusno not in ('5','8') ";
					SSRS tDJSqlSSRS = tExeSQL.execSQL(tDJSql); 
					System.out.println(tDJSql);
					if(tDJSqlSSRS.MaxNumber>0){
						for(int i=0;i<tDJSqlSSRS.getMaxRow();i++){
							Content = "该团单正在进行定期结算保全操作";
							System.out.println("该团单正在进行定期结算保全操作");
							return false;
						}
					}
					if(mLLSendMsgSchema.getReceiveMethod() != "1" &&  mLLSendMsgSchema.getReceiveMethod() != "2"&&( mLLSendMsgSchema.getPayMethod()=="3"|| mLLSendMsgSchema.getPayMethod()=="2"))
					{
						if("".equals(mLLSendMsgSchema.getBankCode())||null==mLLSendMsgSchema.getBankCode()){
							Content = "请您录入开户银行的信息！！！";
							System.out.println("请您录入开户银行的信息！！！");
							return false ;
				    }
						if("".equals(mLLSendMsgSchema.getAccountName())||null==mLLSendMsgSchema.getAccountName()){
							Content = "请您录入户名信息！！！";
							System.out.println("请您录入户名信息！！！");
							return false ;
				    }
						if("".equals(mLLSendMsgSchema.getAccount())||null==mLLSendMsgSchema.getAccount()){
							Content = "请您录入账户信息！！！";
							System.out.println("请您录入账户信息！！！");
							return false ;
				    }
						if("4".endsWith(mLLSendMsgSchema.getReceiveMethod())){
					    	String tBankSQL="SELECT * FROM ldbank WHERE bankcode='"+mLLSendMsgSchema.getBankCode()+"' AND cansendflag='1'";
					    	SSRS tBankSQLSSRS = tExeSQL.execSQL(tBankSQL); 
					    	if (tBankSQLSSRS.getMaxNumber()<=0){
					    		Content = "该银行不支持银行转帐，请确认是否修改为银行汇款或修改银行编码";
					    		System.out.println("该银行不支持银行转帐，请确认是否修改为银行汇款或修改银行编码");
					    		return false ;
					    	}
					    }
					}
					String rgtN1 = linkman1;
					String rgtTel1 = phone1;  
				    int len = 0,len1 = 0;  
				    if(null!=rgtN1){ 
				    for (int i=0; i<rgtN1.length(); i++) {   
				        if (rgtN1.charAt(i)>127 || rgtN1.charAt(i)==94) {  
				            len += 3;   
				        } else {  
				            len ++;   
				        }   
				    } 
					 if(len>60){
						 Content = "联系人名字过长。";
						 System.out.println("联系人名字过长。");
						 return false;
						 }
						}
					
				    if(null!=rgtTel1){
				    	for (int i=0; i<rgtTel1.length(); i++) {   
				            if (rgtTel1.charAt(i)>127 || rgtTel1.charAt(i)==94) {   
				                len1 += 3;   
				            } else {   
				                len1 ++;   
				            }   
				        }   
				    	 if(rgtTel1.length()>18){
				    		 Content = "联系电话长度过长。";
				    		 System.out.println("联系电话长度过长。");
				    	     return false;
				    	 }
				    	}
					//556反洗钱黑名单客户的理赔监测功能
				     String  strblack=" select 1 from lcblacklist where trim(name)='"+GrpName+"' with ur";
				 	 SSRS strblackSSRS = tExeSQL.execSQL(strblack); 
				 	System.out.println(strblack);
				      if(strblackSSRS.getMaxNumber()>0){
				    	  Content = "该单位为黑名单客户，须请示上级处理。";
				    	  System.out.println("该单位为黑名单客户，须请示上级处理。");
				    	  return false ;
				      }
			System.out.println("===========  This is AskSave() End  =============");
			return true;
		}
	 
}
