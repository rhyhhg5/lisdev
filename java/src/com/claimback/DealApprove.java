package com.claimback;

import com.sinosoft.lis.llcase.ClaimUnderwriteUI;
import com.sinosoft.lis.llcase.LJAGetInsertBL;
import com.sinosoft.lis.llcase.LLContAutoDealBL;
import com.sinosoft.lis.llcase.LPSecondUWBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimUWMainSchema;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/**
 * 审批审定 中的 审定确认功能 和给付确认功能
 * @author Administrator
 *
 */
public class DealApprove {
	//不需要报文中传的参数
	private String RgtNo="";
	//案件状态
	private String RgtState="";
	//赔案号
	private String ClmNo="";
	//隐藏的复选框 选中是0 没选中为空
	private String ContDealFlag="";
	private String  cOperate="";
	private String Case_RgtState="";
	//报文中要解析节点信息 后续要赋值 xu
	private LLAppealAcceptSchema mLLAppealAcceptSchema = new LLAppealAcceptSchema();//申诉受理信息

	//审定确认报文中传的参数 xu
	//审定结论 
	private String DecisionSD="1";
	//审批意见
	private String RemarkSP="审批意见";
	//审定意见
	private String RemarkSD="审定意见";
	//给付确认报文中传的参数 xu
	//回销预付赔款 【1-预付回销 其他不是预付回销】
	private String PrePaidFlag="";
	//领取方式
	private String paymode="";
	//银行编码
	private String BankCode="";
	//银行账户
	private String BankAccNo="";
	//银行账户名
	private String AccName="";
	//领取人
	private String Drawer="";
	//领取人身份证号
	private String DrawerID="";
	//更改账户原因
	private String ModifyReason="";
	
	
	
	//给付确认中不需要报文中传入的参数
	private String fmtransact="";
	
	
	//错误信息
	private String Content="";
	//理赔号 方法中传入的参数 xu
	private String CaseNo="";
	CErrors tError = null;
	public boolean submitDate(VData cInputData,String caseNo){
		//获取数据
		if (!getInputData(cInputData,caseNo))
        {
            return false;
        }
		if(!approve()){
			return false;
		}
		
		return true;
	}
	public boolean approve(){
		if("".equals(CaseNo)||null==CaseNo){
			Content = "审批审定确认失败：理赔号不能为空!";
			System.out.println(Content);
			return false;
		}
		//通过理赔号 查询带出来的信息
		String sql = "select a.RgtNo,a.CustomerNo,a.CustomerName,(case a.CustomerSex when '0' then '男' when '1' then '女' when '2' then '不详' end),a.AccdentDesc,"
				+"a.handler,b.codename,c.apppeoples from llcase a,ldcode b,llregister c where a.CaseNo='" 
				+CaseNo+"' and c.rgtno=a.rgtno and b.codetype='llrgtstate' and b.code=a.rgtstate";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS sqlSSRSS=tExeSQL.execSQL(sql);
		if (sqlSSRSS.getMaxNumber()>0){
			RgtNo=sqlSSRSS.GetText(1, 1);
			RgtState=sqlSSRSS.GetText(1, 7); 
		}else{
			RgtNo="";
			RgtState="";
		}
		if ("".equals(RgtState)||null==RgtState)
			{
			//保单责任明细 ClaimPolGrid
			 String strSql = " select contno,RiskCode,b.getdutyname,a.TabFeeMoney,a.DeclineAmnt,a.OtherAmnt,"
					   + "a.ApproveAmnt,OutDutyAmnt,OverAmnt,a.GiveTypeDesc,a.GiveReasonDesc,a.ClaimMoney,a.StandPay,a.RealPay,"
					   +"a.polno,a.clmno,a.getdutycode,a.getdutykind,a.dutycode,a.GiveType,a.GiveReason "
					   +" from LLClaimdetail a, LMDutyGetClm b where  "
					   +" ClmNo in ( select clmno from llclaimpolicy where caseno='"+CaseNo+"')"
					   +" and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind";
					SSRS strSqlSSRS = tExeSQL.execSQL(strSql);
					if (strSqlSSRS.getMaxNumber()>0){
						ClmNo=strSqlSSRS.GetText(1, 16); 
					}
			//赔付保单明细ClaimPayGrid
			 strSql = " select contno,b.riskname,a.GetDutyKind,''," +
			            " a.StandPay,a.RealPay,a.polno,a.clmno," +
			            " a.GiveTypeDesc,a.GiveType,a.riskcode"+
			            " from LLClaimpolicy a, lmrisk b " +
			            " where caseno='"+CaseNo+"'" +
			            " and a.riskcode = b.riskcode " +
			            " order by a.polno";
				DecisionSD = "";
				RemarkSP = "";
				RemarkSD = "";		
				Case_RgtState = "";
					
			}else
			{
				//保单责任明细 ClaimPolGrid
				 String strSql = " select contno,RiskCode,b.getdutyname,a.TabFeeMoney,a.DeclineAmnt,a.OtherAmnt,"
						   + "a.ApproveAmnt,OutDutyAmnt,OverAmnt,a.GiveTypeDesc,a.GiveReasonDesc,a.ClaimMoney,a.StandPay,a.RealPay,"
						   +"a.polno,a.clmno,a.getdutycode,a.getdutykind,a.dutycode,a.GiveType,a.GiveReason "
						   +" from LLClaimdetail a, LMDutyGetClm b where  "
						   +" ClmNo in ( select clmno from llclaimpolicy where caseno='"+CaseNo+"')"
						   +" and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind";
						SSRS strSqlSSRS = tExeSQL.execSQL(strSql);
						if (strSqlSSRS.getMaxNumber()>0){
							ClmNo=strSqlSSRS.GetText(1, 16); 
						}
					String	strSQL4="select rgtstate,contdealflag from llcase where caseno='"+CaseNo+"'";
						SSRS mrr =tExeSQL.execSQL(strSQL4);
						if (mrr.getMaxNumber()>0){
							if("".equals(mrr.GetText(1, 1))||null==mrr.GetText(1, 1)){
								Case_RgtState="0";
							}else{
								Case_RgtState=mrr.GetText(1, 1);
							}
							if("0".equals(mrr.GetText(1, 1))||"1".equals(mrr.GetText(1, 1))){
								ContDealFlag = "0";
					    }
						}
						else{
							Case_RgtState = "";
						}
						
			}
		//调用审批前台校验
		if(!approveCheck()){
			return false;
		}
		//调用后台处理处理审批xu
		if(!dealApprove()){
			return false;
		}
		//调用给付确认的前台校验
		if(!giveEnsure()){
			return false;
		}
		//调用给付确认的后台
		if(!dealGiveEnsure()){
			return false;
		}
		return true;
	}
	private boolean getInputData(VData cInputData,String caseNo) {
		// TODO Auto-generated method stub
		mLLAppealAcceptSchema = null;
		CaseNo = caseNo;
		mLLAppealAcceptSchema= (LLAppealAcceptSchema) cInputData.getObjectByObjectName("LLAppealAcceptSchema", 0);
		DecisionSD=mLLAppealAcceptSchema.getcheckDecision2();
		RemarkSP=mLLAppealAcceptSchema.getRemark1();
		RemarkSD=mLLAppealAcceptSchema.getRemark2();
		PrePaidFlag=mLLAppealAcceptSchema.getPrePaidFlag();
		paymode=mLLAppealAcceptSchema.getpaymode();
		BankCode=mLLAppealAcceptSchema.getBankCode();
		BankAccNo=mLLAppealAcceptSchema.getBankAccNo();
		AccName=mLLAppealAcceptSchema.getAccName();
		Drawer=mLLAppealAcceptSchema.getDrawer();
		DrawerID=mLLAppealAcceptSchema.getDrawerID();
		ModifyReason=mLLAppealAcceptSchema.getModifyReason();
		return true;
	}
	/**
	 * 审批审定确前台校验
	 * @return
	 */
	public boolean approveCheck(){
		String rgtstatesql= "select rgtstate from llcase where caseno='"+CaseNo+"' with ur";
		ExeSQL tExeSQL=new ExeSQL();
		SSRS rgtstateUW=tExeSQL.execSQL(rgtstatesql);
		String tRgtState = rgtstateUW.GetText(1, 1);
		//查讫状态的案件，可以进行审批审定	add by Houyd
		//查讫状态的案件，可以进行审批审定	add by Houyd
		System.out.println("=================================审批审定状态："+tRgtState);
		if ( "04".equals(tRgtState) ||"06".equals(tRgtState)  ||"10".equals(tRgtState)  || "08".equals(tRgtState) ){
			if ("".equals(RemarkSP)||null==RemarkSP) {
				System.out.println("请输入审批意见");
				Content = "审批审定确认失败：请输入审批意见";
				return false;
			}
			cOperate="APPROVE|SP";		
		}	else if ("05".equals(tRgtState) ) {
			if ( null==DecisionSD || "".equals(DecisionSD)){
				System.out.println("审批审定确认失败：请输入审定结论");
				Content = "审批审定确认失败：请输入审定结论";
				return false;
			}
			if ("2".equals(DecisionSD)){
			  if ( null==RemarkSD ||"".equals(RemarkSD)) {
					Content = "审批审定确认失败：请输入审定意见";
					System.out.println(Content);
					return false;
			  }
		  }
			cOperate="APPROVE|SD";	
		}else if(  "16".equals(tRgtState)){
			Content = "审批审定确认失败：理赔二核中不可审批审定，必须等待二核结束！";
			System.out.println(Content);
			return false;
		}
		else{
			Content = "审批审定确认失败：案件在当前状态下不能做审批审定";
			System.out.println(Content);
			return false;
		}
		return true;
	}
	/**
	 * 审批审定确前调用后台
	 * @return
	 */
	public boolean dealApprove(){
		String FlagStr = "";
		String strRgtNo = RgtNo;	//立案号
		String strCaseNo = CaseNo;	//分案号
		String strDecisionSP = "1"; //审批结论暂时去掉，没有意义
		String strDecisionSD =DecisionSD;
		String strRemarkSP = RemarkSP;
		String strRemarkSD = RemarkSD;
		String cOperateCN = "";
		String ContDealFlag1 = "";
		if (ContDealFlag!= null&&!"".equals(ContDealFlag)){
		 	ContDealFlag1 = "0";
		}
		System.out.println("ContDealFlag========="+ContDealFlag1);
		if (strRgtNo == null || strRgtNo.equals("")){
			Content = "审批审定确认失败：立案号不能为空!";
			FlagStr = "Fail";
		}
		if (strCaseNo == null || strCaseNo.equals("")){
			Content = "审批审定确认失败：分案号不能为空!";
			FlagStr = "Fail";
		}
		if (ClmNo == null || "".equals(ClmNo)) {
			Content = "审批审定确认失败：赔案号不能为空!";
			FlagStr = "Fail";
		}
		if (cOperate.equals("APPROVE|SP")){
			cOperateCN = "审批";
			if (strRemarkSP == null || strRemarkSP.equals("")) {
				Content = "审批审定确认失败：审批意见不能为空!";
				FlagStr = "Fail";
			}
		}
		if (cOperate.equals("APPROVE|SD")){
			cOperateCN = "审定";
			if (strDecisionSD == null || strDecisionSD.equals("")){
				Content = "审批审定确认失败：审定结论不能为空!";
				FlagStr = "Fail";
			}
//			if (strRemarkSD == null || strRemarkSD.equals("")){
//				Content = "审定意见不能为空!";
//				FlagStr = "Fail";
//			}
		}
		if (!FlagStr.equals("Fail")){
			GlobalInput tG = new GlobalInput();
			//报文中的 xu
//			tG.Operator="ss3601";
//			tG.ManageCom="8636";
			tG.Operator="lipeib";
			tG.ManageCom = mLLAppealAcceptSchema.getManageCom().substring(0, 4);
			//tGlobalInput = (GlobalInput)session.getValue("GI");
			LLCaseSchema tLLCaseSchema = new LLCaseSchema();
			tLLCaseSchema.setContDealFlag(ContDealFlag1);

			LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();

			tLLClaimUWMainSchema.setRgtNo(strRgtNo);
			tLLClaimUWMainSchema.setCaseNo(strCaseNo);
			tLLClaimUWMainSchema.setClmNo(ClmNo);

			tLLClaimUWMainSchema.setcheckDecision1(strDecisionSP);
			tLLClaimUWMainSchema.setRemark1(strRemarkSP);
			tLLClaimUWMainSchema.setcheckDecision2(strDecisionSD);
			tLLClaimUWMainSchema.setRemark2(strRemarkSD);

			ClaimUnderwriteUI tClaimUnderwriteUI = new ClaimUnderwriteUI();
			VData tVData = new VData();
			try{
				tVData.add(tG);
				tVData.add(tLLClaimUWMainSchema);
				tVData.add(tLLCaseSchema);
				tClaimUnderwriteUI.submitData(tVData, cOperate);
			}
			catch(Exception ex){
				Content = "审批审定确认失败： 执行失败，原因是" + ex.toString();
				FlagStr = "Fail";
			}

			if (FlagStr.equals("")){
				tError = tClaimUnderwriteUI.mErrors;
				if (!tError.needDealError()){
					VData tResultData = tClaimUnderwriteUI.getResult();
					String strResult = (String) tResultData.getObjectByObjectName("String", 0);
					Content = strResult;
					FlagStr = "Succ";
				}
				else{
					Content = " 审批审定确认失败：执行失败，原因是" + tError.getFirstError();
					FlagStr = "Fail";
				}
			}
			if("Fail".endsWith(FlagStr)){
				System.out.println(Content);
				return false;
			}
		}else{
			System.out.println(Content);
			return false;
		}
		return true;
	}
	/**
	 * 给付确认的前台校验
	 * @return
	 */
	public boolean giveEnsure(){
		//初始化赋值
		  String strSQL1 = "select a.AccName,a.BankCode,a.BankAccNo,a.CaseGetMode,a.CustomerName,(select bankname from LDBank where bankcode=a.bankcode),PrePaidFlag from LLCase a "
			      +"where a.CaseNo = '" +CaseNo +"'";
		  ExeSQL tExeSQL=new ExeSQL();
		  SSRS arrResult1 =tExeSQL.execSQL(strSQL1);
		  String strSQL2 = "select PrePaidFlag from LLCase a "
			      +"where a.caseno = '" +CaseNo +"'";
		  SSRS arrResult2 =tExeSQL.execSQL(strSQL2);
		   if(arrResult1.MaxNumber>0){
			   if("".equals(mLLAppealAcceptSchema.getAccName())||null==mLLAppealAcceptSchema.getAccName()){
				   AccName= arrResult1.GetText(1, 1);
			   }else{
				   AccName= mLLAppealAcceptSchema.getAccName();
			   }
			   if("".equals(mLLAppealAcceptSchema.getBankCode())||null==mLLAppealAcceptSchema.getBankCode()){
				   BankCode= arrResult1.GetText(1, 2);
			   }else{
				   BankCode= mLLAppealAcceptSchema.getBankCode();
			   }
			   if("".equals(mLLAppealAcceptSchema.getBankAccNo())||null==mLLAppealAcceptSchema.getBankAccNo()){
				   BankAccNo= arrResult1.GetText(1, 3);
			   }else{
				   BankAccNo= mLLAppealAcceptSchema.getBankAccNo();
			   }
			   if("".equals(mLLAppealAcceptSchema.getpaymode())||null==mLLAppealAcceptSchema.getpaymode()){
				   paymode= arrResult1.GetText(1, 4);
			   }else{
				   paymode= mLLAppealAcceptSchema.getpaymode();
			   }
	        }else{
	        	 Content = "给付确认失败原因是："+CaseNo+"案件查询失败!";
		    	 System.out.println(Content);
		    	 return false;
	        }
		   if("".equals(mLLAppealAcceptSchema.getPrePaidFlag())||null==mLLAppealAcceptSchema.getPrePaidFlag()){
			   if(!"".equals(arrResult2.GetText(1, 1)) && null!=arrResult2.GetText(1, 1) && "1".equals(arrResult2.GetText(1, 1)))
		          {
		        	PrePaidFlag= arrResult2.GetText(1, 1);
		          }else{
		        	  PrePaidFlag = "";
		          }
		   }else{
			   PrePaidFlag=mLLAppealAcceptSchema.getPrePaidFlag();
		   }
		   String strSQLc = "select Drawer,DrawerID from LJAGet where actugetno=("
					+ " select  max(actugetno) from LJAGet where otherno='"
					+ CaseNo +"' and othernotype='5' )";
		   SSRS strSQLcSSRS=tExeSQL.execSQL(strSQLc);
		   if(strSQLcSSRS.getMaxNumber()>0){
			   if("".equals(mLLAppealAcceptSchema.getDrawer())||null==mLLAppealAcceptSchema.getDrawer()){
				   Drawer= strSQLcSSRS.GetText(1, 1);
			   }else{
				   Drawer=mLLAppealAcceptSchema.getDrawer();
			   }
			   if("".equals(mLLAppealAcceptSchema.getDrawerID())||null==mLLAppealAcceptSchema.getDrawerID()){
				   DrawerID= strSQLcSSRS.GetText(1, 2);
			   }else{
				   DrawerID=mLLAppealAcceptSchema.getDrawerID();
			   }
		        }else{
		        	String cusStrSQL = "select CustomerName,IDNo from LLCase "
							+"where CaseNo = '" +CaseNo+"'";
					SSRS cusStrSQLSSRS =tExeSQL.execSQL(cusStrSQL);
					if(cusStrSQLSSRS.getMaxNumber()>0){
						   if("".equals(mLLAppealAcceptSchema.getDrawer())||null==mLLAppealAcceptSchema.getDrawer()){
							   Drawer= cusStrSQLSSRS.GetText(1, 1);
						   }else{
							   Drawer=mLLAppealAcceptSchema.getDrawer();
						   }
						   if("".equals(mLLAppealAcceptSchema.getDrawerID())||null==mLLAppealAcceptSchema.getDrawerID()){
							   DrawerID= cusStrSQLSSRS.GetText(1, 2);
						   }else{
							   DrawerID=mLLAppealAcceptSchema.getDrawerID();
						   }
					        }else{
					        	 Content = "给付确认失败原因是："+CaseNo+"案件的领款人信息查询失败!";
						    	 System.out.println(Content);
						    	 return false;
					        }
		        }
		//前台校验
		//TODO:新增效验，理赔二核时不可给付确认
		String sqlUW = "select rgtstate from llcase where caseno='"+CaseNo+"' with ur";
		SSRS rgtstateUW=tExeSQL.execSQL(sqlUW);
		RgtState=rgtstateUW.GetText(1, 1);
		if (rgtstateUW.getMaxNumber()>0){
		     if( "16".equals(rgtstateUW.GetText(1, 1))){
		    	 Content = "给付确认失败原因是：理赔二核中不可给付确认，必须等待二核结束！";
		    	 System.out.println(Content);
		    	 return false;
		     }
		}
		String  strSQL= "select RgtClass,TogetherFlag from LLRegister "
			    +"where rgtNo='"+RgtNo+"'"; 
		SSRS	 arrResult =tExeSQL.execSQL(strSQL);
			    if(arrResult.getMaxNumber()>0)
			    {
			    if("1".equals(arrResult.GetText(1, 1)) && "3".equals(arrResult.GetText(1, 2)))
			    {
			    	 Content = "给付确认失败原因是：团单给付方式为统一给付，这里不能做给付确认！";
			    	 System.out.println(Content);
			    	 return false;
			    }
			    }
				if ( !"09".equals(RgtState) )
				{
					 Content = "给付确认失败原因是：案件在当前状态下不能做给付确认！";
			    	 System.out.println(Content);
			    	 return false;
				}
				//---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
				   String PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a "
				   			+" where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and caseno='"+CaseNo+"')";      			
				   SSRS tPrepaidBala=tExeSQL.execSQL(PrepaidBalaSql);
				   if(tPrepaidBala.getMaxNumber()>0){
					   if((null==tPrepaidBala.GetText(1, 1) ||  "".equals(tPrepaidBala.GetText(1, 1)) )&& "1".equals(PrePaidFlag))
					   {
						   	Content = "给付确认失败原因是：被保险人投保的保单不存在预付未回销赔款!";
					    	System.out.println(Content);
					    	return false;
					   } 
				   }
				    //--------对于全部统一给付和全部统计给付（医疗机构）的批次案件，在审批审定页面给付确认时提示阻断---------//
				    //GY add by 2012-12-20 //
					if (!"".equals(CaseNo)&&null!=CaseNo) {
						String sql4 = "select caseno from llcase where caseno='"+CaseNo+"'" +
								" and rgtno in (select rgtno from llregister where togetherflag ='3'"
								+ " or togetherflag='4')";
						SSRS sql4SSRS=tExeSQL.execSQL(sql4);
						if (sql4SSRS.getMaxNumber()>0) {
								Content = "给付确认失败原因是：请在【团体结案】页面进行给付确认。";
								System.out.println(Content);
								return false;
						}
					}
					if("".equals(paymode)||null==paymode)
					{
						Content = "给付确认失败原因是：必须选择领取方式！";
						System.out.println(Content);
						return false;
					}
					if( !"1".equals(paymode) && !"2".equals(paymode))
					{
						if("".equals(BankCode)||null==BankCode){
							Content = "给付确认失败原因是：银行编码不能为空";
							System.out.println(Content);
							return false;
						}else if("".equals(BankAccNo)||null==BankAccNo){
							Content = "给付确认失败原因是：银行账户不能为空";
							System.out.println(Content);
							return false;
						}else if("".equals(AccName)||null==AccName){
							Content = "给付确认失败原因是：银行账户名不能为空";
							System.out.println(Content);
							return false;
						}
					}
					if( !"1".equals(paymode) && !"2".equals(paymode)){
						  if(!"".equals(AccName) &&("".endsWith(Drawer)||null==Drawer)){
				  			Drawer=AccName; 
				  			} 	
						}
				  fmtransact= "INSERT";
		return true;
	}
	/**
	 * 调用给付确认后台
	 * @return
	 */
	public boolean dealGiveEnsure(){
		//接收信息，并作校验处理。
		//输入参数
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LJAGetSchema tLJAGetSchema   = new LJAGetSchema();
		LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
		//输出参数
		String FlagStr = "";
		GlobalInput tGI = new GlobalInput();
		//报文中的 xu
//		tGI.Operator ="sc3612";
//		tGI.ManageCom="86360500";
		tGI.Operator ="lipeic";
		tGI.ManageCom=mLLAppealAcceptSchema.getManageCom();
		CErrors tError = null;
		String tBmCert = "";
		String transact=fmtransact;
		 //后面要执行的动作：添加，修改，删除
		  if (!"".equals(PrePaidFlag)&&null!=PrePaidFlag) {
			  tLLCaseSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
		  }else{
			  tLLCaseSchema.setPrePaidFlag("");  // 个案存llcase 批次案件llregister
		  }	
		  tLLCaseSchema.setCaseNo(CaseNo);
		  tLLCaseSchema.setAccModifyReason(ModifyReason);
		  tLJAGetSchema.setOtherNo(CaseNo);
		  tLJAGetSchema.setOtherNoType("5");
		  tLJAGetSchema.setAccName(AccName);
		  tLJAGetSchema.setBankAccNo(BankAccNo);
		  tLJAGetSchema.setPayMode(paymode);
		  System.out.println("给付方式：：：：：：：：：：："+tLJAGetSchema.getPayMode());
		  tLJAGetSchema.setBankCode(BankCode);
		  tLJAGetSchema.setDrawer(Drawer);
		  tLJAGetSchema.setDrawerID(DrawerID); 
	
		  try{
			    // 准备传输数据 VData
			    VData tVData = new VData();
			    tVData.addElement(tLLCaseSchema);
			    tVData.addElement(tLJAGetSchema);
			    tVData.addElement(tGI);
			    System.out.println("submit======================:");
			    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			    if (tLJAGetInsertBL.submitData(tVData,transact))
			    {    
			    	tError = tLJAGetInsertBL.mErrors;
			   	    if (!tError.needDealError())
			   	    {	   	   
				   	    Content = "给付确认保存成功" + tLJAGetInsertBL.getBackMsg();
				   	    FlagStr = "Succ";
			   	      	   	    
				  	    System.out.println("LLContAutoDealBL============Begin==========:");
				  	    try{				
							String tsql = "select 1 from llclaimdetail where caseno = '"+CaseNo+"' and riskcode in (select code from ldcode where codetype='lxb' ) with ur ";
					    	SSRS tSSRS = new ExeSQL().execSQL(tsql);   	
					    	
					    	
					  	  	  if(tSSRS.MaxRow>0){
					  	  	  	LPSecondUWBL tLPSecondUWBL  = new LPSecondUWBL();
					  	  	  	tLPSecondUWBL.submitData(tVData,"");
					  	  	  }else{
								LLContAutoDealBL tLLContAutoDealBL = new LLContAutoDealBL();
								tLLContAutoDealBL.submitData(tVData,"");
								System.out.println(tLLContAutoDealBL.getBackMsg());
								Content += tLLContAutoDealBL.getBackMsg();
					  	  	  }
					  	  	  System.out.println("Content=="+Content);
					  		
				  	    }catch(Exception tex) {
				  	    	Content += "合同终止保存失败，原因是:" + tex.toString()+"，请到理赔合同处理中进行操作!";
				  	    	}
				  	 }else{
			   	      Content = "给付确认保存失败，原因是:" + tError.getFirstError();
			   	      FlagStr = "Fail";
			   	     }   	       		    	    	
				}else{
			    	  tError = tLJAGetInsertBL.mErrors;
			     	  Content = "给付确认保存失败，原因是:" + tError.getFirstError();
			     	  FlagStr = "Fail";
			    }  
			  }
			  catch(Exception ex)
			  {
			    Content = "给付确认保存失败，原因是:" + ex.toString();
			    FlagStr = "Fail";
			  }
		  if("Fail".equals(FlagStr)){
			  System.out.println(Content);
			  return false; 
		  }
		return true;
	}
	/**
	 * 删除审批数据
	 * @return
	 */
	public boolean deleteData(){
		 MMap map = new MMap();
		 String sql="select clmno from LLClaimdetail where caseno='"+CaseNo+"'";
		 ExeSQL tExeSQL=new ExeSQL();
		 SSRS sqlSSRS=tExeSQL.execSQL(sql);
		 String Clmno=sqlSSRS.GetText(1, 1);
		 map.put("delete from LLClaimUnderwrite where ClmNo='" +
				 Clmno+ "'and caseno='"+CaseNo+"' ", "DELETE");
		 map.put("delete from LLClaimUWDetail where ClmNo='" +
				 Clmno+ "'and caseno='"+CaseNo+"' ", "DELETE");
		 map.put("delete from LLClaimUWMDetail where ClmNo='" +
				 Clmno+ "'", "DELETE");
		 map.put("delete from LLClaimUWMain where ClmNo='" +
				 Clmno+ "'", "DELETE");
		 map.put("delete from LLCaseOpTime where ClmNo='" +
				 Clmno+ "'and caseno='"+CaseNo+"' ", "DELETE");
		 VData mInputData = new VData();
  	    PubSubmit tPubSubmit = new PubSubmit();
  	    mInputData.add(map);
  	    return tPubSubmit.submitData(mInputData, null);
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public static void main(String[] args) {
		DealApprove a =new DealApprove();
		a.approve();
	}
}
