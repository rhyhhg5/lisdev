package com.claimback.serviceTest;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import com.claimback.ClaimBackInterface;


public class TsetSentXML {

	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

		try {
			byte[] tBytes = new byte[8 * 1024];
			for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return mByteArrayOutputStream.toByteArray();
	}
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		
		try {
			ClaimBackInterface test = new ClaimBackInterface();
			String mInFilePath = "C:/Users/Administrator/Desktop/jiucuo.xml";
			InputStream mIs;
			mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = TsetSentXML.InputStreamToBytes(mIs);
			String mInXmlStr;
			mInXmlStr = new String(mInXmlBytes, "gbk");
			System.out.println(mInXmlStr);
			test.service(mInXmlStr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	
}
