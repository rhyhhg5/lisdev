package com.claimback.serviceTest.claimback;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.claimback.DealClaimBack;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCalPaySchema;
import com.sinosoft.lis.schema.LLClaimDeclineSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLCalPaySet;
import com.sinosoft.lis.vschema.LLClaimDeclineSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


/**
 * 理赔申诉/纠错接口报文解析
 * @author 樊庆
 * 2017.01.10
 */
public class ClaimBackParse {
	 private VData mResult = new VData();
	 private MMap mMap=new MMap();
	 String Content = "";
	 private String responseXml = "";
	 
	 public String getResponseXml() {
		return responseXml;
	}

	private LLAppealAcceptSchema mLLAppealAcceptSchema = new LLAppealAcceptSchema();//申诉受理信息
	 private LLSubReportSet mLLSubReportSet = new LLSubReportSet();//客户事件信息
	 private LLCalPaySet mLLCalPaySet = new LLCalPaySet();//理算信息
	 private LLClaimDeclineSchema mLLClaimDeclineSchema = new LLClaimDeclineSchema();//拒赔
	public boolean parseXml(String xmlMsg){
		if(null == xmlMsg || "".equals(xmlMsg)){
			Content = "发送的报文为空！";
			System.out.println(Content);
			
			return false;
		}
		System.out.println("接收到报文：\n"+xmlMsg);
		try
		{
			SAXReader reader = new SAXReader();
			Document doc;
			if(".xml".equals(xmlMsg.substring(xmlMsg.length()-4))){
				System.out.println(xmlMsg.substring(xmlMsg.length()-4));
				//文件形式 xmlMsg 为路径
				File f = new File(xmlMsg);
				reader.setEncoding("GBK");
				doc = reader.read(f);
				System.out.println("文件============");
			}else{
				//字符串形式xmlMsg 为报文内容
				doc = DocumentHelper.parseText(xmlMsg); 
//				mMap.put("requestXML", xmlMsg.trim());
				System.out.println("字符串============");
			}
			
			Element root = doc.getRootElement();
			//获取报文头
			Element tHead = root.element("head");

			mLLAppealAcceptSchema.setBatchNo(tHead.elementText("BatchNo"));//批次号
			mLLAppealAcceptSchema.setSendDate(tHead.elementText("SendDate"));//发送日期
			mLLAppealAcceptSchema.setSendTime(tHead.elementText("SendTime"));//发送时间
			mLLAppealAcceptSchema.setBranchCode(tHead.elementText("BranchCode"));//交易编码
			mLLAppealAcceptSchema.setSendOperator(tHead.elementText("SendOperator"));//交易人员
			mLLAppealAcceptSchema.setMsgType(tHead.elementText("MsgType"));//固定值
			
			//获取报文体
			Element tBody = root.element("body");
			
			Element tcorrection = tBody.element("correction");
			System.out.println(tcorrection.elementText("ManageCom"));
			mLLAppealAcceptSchema.setManageCom(tcorrection.elementText("ManageCom"));//登录机构
			ExeSQL tExeSQL = new ExeSQL();
			String sql = "select caseno from llcase  where rgtno = '"+tcorrection.elementText("rgtno")+
						"' and idno = '"+tcorrection.elementText("tIDNo")+
						"' and customername = '"+tcorrection.elementText("CustomerName")+"'";
			SSRS ss = tExeSQL.execSQL(sql);
			System.out.println(tcorrection.elementText("rgtno")+"===="+ss.GetText(1, 1));
			mLLAppealAcceptSchema.setrgtno(ss.GetText(1, 1));//理赔号
			mLLAppealAcceptSchema.setAppealType(tcorrection.elementText("AppealType"));//申诉/错误类型
			mLLAppealAcceptSchema.setAppeanRCode(tcorrection.elementText("AppeanRCode"));//申诉/错误原因
			mLLAppealAcceptSchema.setAppealRDesc(tcorrection.elementText("AppealRDesc"));//备注
			mLLAppealAcceptSchema.setCustomerName(tcorrection.elementText("CustomerName"));//客户姓名
//			mLLAppealAcceptSchema.setCustomerNo(tcorrection.elementText("CustomerNo"));//客户号码
			mLLAppealAcceptSchema.settIDNo(tcorrection.elementText("tIDNo"));//证件号码
			mLLAppealAcceptSchema.settIDType(tcorrection.elementText("tIDType"));//证件类型
			mLLAppealAcceptSchema.setCBirthday(tcorrection.elementText("CBirthday"));//出生日期
			mLLAppealAcceptSchema.setOtherIDNo(tcorrection.elementText("OtherIDNo"));//社保号码
			mLLAppealAcceptSchema.setMobilePhone(tcorrection.elementText("MobilePhone"));//手机号码
			mLLAppealAcceptSchema.setRelation(tcorrection.elementText("Relation"));//申请人与被保人关系
			mLLAppealAcceptSchema.setRgtType(tcorrection.elementText("RgtType"));//受理方式
			mLLAppealAcceptSchema.setReturnMode(tcorrection.elementText("ReturnMode"));//回执发送方式
			mLLAppealAcceptSchema.setRgtantName(tcorrection.elementText("RgtantName"));//申请人姓名
			mLLAppealAcceptSchema.setIDNo(tcorrection.elementText("IDNo"));//申请人证件号码
			mLLAppealAcceptSchema.setIDType(tcorrection.elementText("IDType"));//申请人证件类型
			mLLAppealAcceptSchema.setRgtantPhone(tcorrection.elementText("RgtantPhone"));//申请人电话
			mLLAppealAcceptSchema.setMobile(tcorrection.elementText("Mobile"));//申请人手机
			mLLAppealAcceptSchema.setEmail(tcorrection.elementText("Email"));//申请人电子邮箱
			mLLAppealAcceptSchema.setRgtantAddress(tcorrection.elementText("RgtantAddress"));//申请人地址
			mLLAppealAcceptSchema.setPostCode(tcorrection.elementText("PostCode"));//邮政编码
			mLLAppealAcceptSchema.setpaymode(tcorrection.elementText("paymode"));//受益金领取方式
			if( "3".equals(tcorrection.elementText("paymode")) || "4".equals(tcorrection.elementText("paymode")) || 
					"11".equals(tcorrection.elementText("paymode")) ){//11-银行汇款,3-转账支票,4-银行转账
				System.out.println("银行汇款/转账支票/银行转账");
				mLLAppealAcceptSchema.setBankCode(tcorrection.elementText("BankCode"));//银行编码
				mLLAppealAcceptSchema.setSendFlag(tcorrection.elementText("SendFlag"));//签约银行
				mLLAppealAcceptSchema.setBankAccNo(tcorrection.elementText("BankAccNo"));//银行账号
				mLLAppealAcceptSchema.setAccName(tcorrection.elementText("AccName"));//银行账户名
			}
			mLLAppealAcceptSchema.setCustStatus(tcorrection.elementText("CustStatus"));//事故者现状
			mLLAppealAcceptSchema.setDeathDate(tcorrection.elementText("DeathDate"));//死亡日期
			mLLAppealAcceptSchema.setappReasonCode(tcorrection.elementText("appReasonCode"));//申请原因
			mLLAppealAcceptSchema.setInsuredStat(tcorrection.elementText("InsuredStat"));//参保人员类别
			mLLAppealAcceptSchema.setSecurityNo(tcorrection.elementText("SecurityNo"));//客户社保号
			mLLAppealAcceptSchema.setCompSecuNo(tcorrection.elementText("CompSecuNo"));//单位社保登记号
			mLLAppealAcceptSchema.setHospitalName(tcorrection.elementText("HospitalName"));//医院名称
			mLLAppealAcceptSchema.setHospitalCode(tcorrection.elementText("HospitalCode"));//医院代码
			mLLAppealAcceptSchema.setFeeAtti(tcorrection.elementText("FeeAtti"));//账单属性
			mLLAppealAcceptSchema.setFeeType(tcorrection.elementText("FeeType"));//账单种类
			mLLAppealAcceptSchema.setFeeAffixType(tcorrection.elementText("FeeAffixType"));//账单类型
			mLLAppealAcceptSchema.setReceiptType(tcorrection.elementText("ReceiptType"));//收据类型
			mLLAppealAcceptSchema.setReceiptNo(tcorrection.elementText("ReceiptNo"));//账单号码
			mLLAppealAcceptSchema.setinpatientNo(tcorrection.elementText("inpatientNo"));//住院号
			mLLAppealAcceptSchema.setFeeDate(tcorrection.elementText("inpatientNo"));//结算日期
			mLLAppealAcceptSchema.setHospStartDate(tcorrection.elementText("HospStartDate"));//入院日期
			mLLAppealAcceptSchema.setHospEndDate(tcorrection.elementText("HospEndDate"));//出院日期
			mLLAppealAcceptSchema.setIssueUnit(tcorrection.elementText("IssueUnit"));//分割单出具单位
			mLLAppealAcceptSchema.setSimpleCase(tcorrection.elementText("SimpleCase"));//机构处理
			mLLAppealAcceptSchema.setEasyCase(tcorrection.elementText("EasyCase"));//简易案件
			mLLAppealAcceptSchema.setHeadCase(tcorrection.elementText("HeadCase"));//上报总公司
			mLLAppealAcceptSchema.setFenCase(tcorrection.elementText("FenCase"));//上报分公司
			mLLAppealAcceptSchema.setContRemark(tcorrection.elementText("ContRemark"));//客户特约信息
			mLLAppealAcceptSchema.setPrePaidFlag(tcorrection.elementText("PrePaidFlag"));//回销预付赔款
			mLLAppealAcceptSchema.setFenCase(tcorrection.elementText("FenCase"));//上报分公司 【简易案件】
			mLLAppealAcceptSchema.setContRemark(tcorrection.elementText("ContRemark"));//客户特约信息
			//客户事件信息
			List<Element> tItemList = tcorrection.selectNodes("item");
			for (int i = 0; i < tItemList.size(); i++) {
				if("".equals(tItemList.get(i).elementText("SubDate")) || tItemList.get(i).elementText("SubDate") == null){
					System.out.println("没有事件信息！！！");
					continue;
				}
				LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
				tLLSubReportSchema.setAccDate(tItemList.get(i).elementText("SubDate"));//发生日期
				tLLSubReportSchema.setAccPlace(tItemList.get(i).elementText("SubAddress"));//发生地点
				tLLSubReportSchema.setInHospitalDate(tItemList.get(i).elementText("InHospitalDate"));//入院日期
				tLLSubReportSchema.setOutHospitalDate(tItemList.get(i).elementText("OutHospitalDate"));//出院日期
				tLLSubReportSchema.setAccDesc(tItemList.get(i).elementText("SubMsg"));//事件信息
				tLLSubReportSchema.setAccidentType(tItemList.get(i).elementText("AccType"));//事件类型
				
				mLLSubReportSet.add(tLLSubReportSchema);
			}
			//费用明细
			List<Element> tFeeItem = tcorrection.selectNodes("FeeItem");//费用明细
			for (int i = 0; i < tFeeItem.size(); i++) {
				mMap.put(tFeeItem.get(i).elementText("FeeType"), tFeeItem.get(i).elementText("Fee"));
				System.out.println("费用明细===> 费用类型："+tFeeItem.get(i).elementText("FeeType")
						+" ===== 费用金额："+tFeeItem.get(i).elementText("Fee"));
			}
//			Set tSet = mMap.keySet();
//			Object[] ss = tSet.toArray();
//			for (int i = 0; i < ss.length; i++) {
//				System.out.println("ss=========="+ss[i].toString());
//				System.out.println("ss=========="+ss[i].toString().substring(0, 1));
//			}
			//理算
			Element tCalPay = tBody.element("CalPay");
			List<Element> titem = tCalPay.selectNodes("item");//理算
			for (int i = 0; i < titem.size(); i++) {
				LLCalPaySchema tLLCalPaySchema = new LLCalPaySchema();
				tLLCalPaySchema.setriskcode(titem.get(i).elementText("riskcode"));//险种代码
				tLLCalPaySchema.setgetdutycode(titem.get(i).elementText("getdutycode"));//给付责任代码
				tLLCalPaySchema.setGetDutyKind(titem.get(i).elementText("GetDutyKind"));//给付责任类型
				tLLCalPaySchema.setGiveType(titem.get(i).elementText("GiveType"));//赔付结论代码
				tLLCalPaySchema.setGiveReason(titem.get(i).elementText("GiveReason"));//赔付结论依据代码
				tLLCalPaySchema.setGiveTypeDesc(titem.get(i).elementText("GiveTypeDesc"));//赔付结论
				tLLCalPaySchema.setGiveReasonDesc(titem.get(i).elementText("GiveReasonDesc"));//赔付结论依据
				tLLCalPaySchema.setIsRate(titem.get(i).elementText("IsRate"));//比例金额
				tLLCalPaySchema.setDeclineAmnt(titem.get(i).elementText("DeclineAmnt"));//拒付金额
				tLLCalPaySchema.setApproveAmnt(titem.get(i).elementText("ApproveAmnt"));//通融/协议给付比例
				tLLCalPaySchema.setEasyCase(titem.get(i).elementText("EasyCase"));//是否为简易案件
				mLLClaimDeclineSchema.setReason(titem.get(i).elementText("DeclineReason"));
				mLLCalPaySet.add(tLLCalPaySchema);
			}
			
			//审批审定
			Element tClaim_SPSD = tBody.element("Claim_SPSD");
			mLLAppealAcceptSchema.setcheckDecision2(tClaim_SPSD.elementText("checkDecision2"));//审定结论
			mLLAppealAcceptSchema.setRemark1(tClaim_SPSD.elementText("Remark1"));//审批意见
			mLLAppealAcceptSchema.setRemark2(tClaim_SPSD.elementText("Remark2"));//审定意见
			System.out.println("审批审定: "+mLLAppealAcceptSchema.getcheckDecision2()+mLLAppealAcceptSchema.getRemark1()+mLLAppealAcceptSchema.getRemark2());
			
			//给付确认
			Element tgiveConfirm = tBody.element("giveConfirm");
			mLLAppealAcceptSchema.setGAccName(tgiveConfirm.elementText("AccName"));//给付账户名
			mLLAppealAcceptSchema.setGBankAccNo(tgiveConfirm.elementText("BankAccNo"));//给付账号
			mLLAppealAcceptSchema.setGBankCode(tgiveConfirm.elementText("BankCode"));//给付银行编码
			mLLAppealAcceptSchema.setGpaymode(tgiveConfirm.elementText("paymode"));//给付领取方式
			mLLAppealAcceptSchema.setGPrePaidFlag(tgiveConfirm.elementText("PrePaidFlag"));//给付回销预付赔款
			mLLAppealAcceptSchema.setDrawer(tgiveConfirm.elementText("Drawer"));//领取人
			mLLAppealAcceptSchema.setDrawerID(tgiveConfirm.elementText("DrawerID"));//领取人身份证号
			mLLAppealAcceptSchema.setModifyReason(tgiveConfirm.elementText("ModifyReason"));//更改账户原因
			
			mResult.add(xmlMsg);
			mResult.add(mLLAppealAcceptSchema);
			mResult.add(mMap);//费用明细
			mResult.add(mLLSubReportSet);//客户事件信息
			mResult.add(mLLCalPaySet);//理算信息
			mResult.add(mLLClaimDeclineSchema);//全额拒付信息
			//调用业务处理类
			DealClaimBack tDealClaimBack = new DealClaimBack();
			tDealClaimBack.submitDate(mResult);
			responseXml = tDealClaimBack.getResponseXml();
		}catch (Exception e)
			{
				e.printStackTrace();
			}
		return true;
	}
}
