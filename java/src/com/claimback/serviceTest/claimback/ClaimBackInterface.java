package com.claimback.serviceTest.claimback;

import java.io.InputStream;


/**
 * 接收接口发送的报文及返回
 * @author 樊庆
 * 2017.1.11
 *
 */
public class ClaimBackInterface {

	/**
	 * 
	 * 接口方法，接收报文，返回报文
	 * 
	 * @param xml
	 * @return
	 */
	public String service(String xml){
		//解析报文
		ClaimBackParse tClaimBackParse = new ClaimBackParse();
		tClaimBackParse.parseXml(xml);
		
		//返回报文
		System.out.println("返回报文信息===\t" + tClaimBackParse.getResponseXml());
		return tClaimBackParse.getResponseXml();
			
	}

}
