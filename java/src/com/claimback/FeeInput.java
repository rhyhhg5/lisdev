package com.claimback;

import java.util.Set;

import com.sinosoft.lis.llcase.ICaseCureUI;
import com.sinosoft.lis.llcase.RegisterBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCaseReceiptSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLFeeOtherItemSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.schema.LLSendMsgSchema;
import com.sinosoft.lis.vschema.LLCaseReceiptSet;
import com.sinosoft.lis.vschema.LLFeeOtherItemSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class FeeInput {
	private LLAppealAcceptSchema mLLAppealAcceptSchema = new LLAppealAcceptSchema();//申诉受理信息
	private MMap mMap=new MMap();
	String Content = "";
	public String getContent() {
		return Content;
	}

	String caseNo = "";//理赔号 
	String FlagStr = "";
	
	
	
	public boolean submitDate(VData cInputData,String caseNo){
		//获取数据
		if (!getInputData(cInputData,caseNo))
        {
            return false;
        }
		if(!feeInput()){
			return false;
		}
		if(!feeInput2()){
			return false;
		}
		if(!feeInput3()){
			return false;
		}
		
		return true;
	}
	
	private boolean getInputData(VData cInputData,String caseNo) {
	// TODO Auto-generated method stub
		mLLAppealAcceptSchema = null;
		mMap = null;
		this.caseNo = caseNo;
		mLLAppealAcceptSchema = (LLAppealAcceptSchema) cInputData.getObjectByObjectName("LLAppealAcceptSchema", 0);
		mMap = (MMap) cInputData.getObjectByObjectName("MMap", 0);
		return true;
	}


	/**
	 * 账单录入  前台校验  确认按钮
	 * ----------ICaseCureInput.jsp
	 * @return
	 */
	public boolean feeInput(){
		System.out.println("=====【账单录入】====【确认按钮】====【前台校验】====");
		String sFeeAtti = mLLAppealAcceptSchema.getFeeAtti();//账单属性 n
		String sFeeType = mLLAppealAcceptSchema.getFeeType();//账单种类 n
		String sHospitalName = mLLAppealAcceptSchema.getHospitalName();//医院名称 n
		String sHospitalCode = mLLAppealAcceptSchema.getHospitalCode();//医院代码 n
		String sReceiptNo = mLLAppealAcceptSchema.getReceiptNo();//账单号码 n
//		String caseNo = "R3600170113000001";//理赔号 
		String sFeeDate = mLLAppealAcceptSchema.getFeeDate();//结算日期
		
		String sFeeStart = "";//发生费用时间
		String sCustomerNo = "";//客户号码
		String managecom = "";
		String sFeeAffixType = "";//账单类型
		String sFeeEnd = "";//申报时间
		String sHospStartDate = "";//住院时间
		String sHospEndDate = "";//出院时间
		
		ExeSQL tExeSQL = new ExeSQL();
		
		if("".equals(sFeeAtti)){
			Content = "账单属性不能为空！";
			System.out.println("账单属性不能为空！");
		    return false;
		}
		if(!"4".equals(sFeeAtti)){
			Content = "账单属性非简易社保结算！";
			System.out.println("账单属性非简易社保结算！");
		    return false;
		}
		if("".equals(sFeeType)){
			Content = "账单种类不能为空！";
			System.out.println("账单种类不能为空！");
		    return false;
		}
		if("".equals(sHospitalName)){
			Content = "医院名称不能为空！";
			System.out.println("医院名称不能为空！");
		    return false;
		}
		if("".equals(sHospitalCode)){
			Content = "医院代码不能为空！";
			System.out.println("医院代码不能为空！");
		    return false;
		}
		if("".equals(sReceiptNo)){
			Content = "账单号码不能为空！";
			System.out.println("账单号码不能为空！");
		    return false;
		}
		
		if(!"2".equals(sFeeAtti)&&"".equals(sFeeDate)){
		    Content = "结算日期不能为空！";
		    System.out.println("结算日期不能为空！");
		    return false;
		}

			//TODO:新增效验，理赔二核时不可账单保存
		String sql2 = "select rgtstate from llcase where caseno='"+caseNo+"' with ur";
		SSRS sqlSSRS = tExeSQL.execSQL(sql2);

		if (sqlSSRS.MaxRow > 0){
		     if("16".equals(sqlSSRS.GetText(1,1))){
		       Content = "理赔二核中不可账单保存，必须等待二核结束！";
		       System.out.println("理赔二核中不可账单保存，必须等待二核结束！");
			   return false;
		     }
		}
		
	  
  		return true;
	}
	
	/**
	 * 账单录入  后台  确认按钮
	 * @return
	 */
	public boolean feeInput2(){
		System.out.println("========【账单录入】====【确认按钮】====【提交后台】");
		String tsOperate = "UPDATE";//INSERT UPDATE DELETE
		String tsOperateCN = "修改";//保存 修改 删除
//		String mngCom = mLLAppealAcceptSchema.getManageCom();
		String mngCom ="86360500";
//		String caseNo = "R3600170113000001";//理赔号 
		ExeSQL tExeSQL = new ExeSQL();
		
		//获取理赔账单信息-账单号码
		String sql = "select mainfeeno,feeaffixtype,firstinhos,hospstartdate,hospenddate,inhosno,compsecuno," +
				"securityno,InsuredStat,hosgrade,hosdistrict,ReceiptType,IssueUnit " +
				"from llfeemain where caseno='"+caseNo+"'";
		SSRS sqlSSRS2 = tExeSQL.execSQL(sql);
		
		String tsMainFeeNo = sqlSSRS2.GetText(1, 1);
		String tsCustomerName = mLLAppealAcceptSchema.getCustomerName();						//客户姓名
		String tsFeeAtti = mLLAppealAcceptSchema.getFeeAtti();									//账单属性
		String tsFirstInHos = sqlSSRS2.GetText(1, 3);											//第一次住院标志
		String tsMngCom = mngCom;
		//初始化
		LLFeeMainSchema mFeeMainSchema = new LLFeeMainSchema();
		LLSecurityReceiptSchema mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
		LLCaseReceiptSet mReceiptSet = new LLCaseReceiptSet();
		LLCaseReceiptSchema mReceiptSchema = null;
		LLFeeOtherItemSet mFeeOtherItemSet = new LLFeeOtherItemSet();
		LLFeeOtherItemSchema mFeeOtherItemSchema = null;
		
		//账单主信息
		mFeeMainSchema.setMainFeeNo(tsMainFeeNo);
		mFeeMainSchema.setCaseNo(caseNo);
		sql = "select rgtno,CUSTOMERNO,CUSTOMERNAME,CUSTOMERSEX,CUSTOMERAGE from llcase where caseno='"+caseNo+"'";
		SSRS sqlSSRS = tExeSQL.execSQL(sql);
		
		mFeeMainSchema.setRgtNo(sqlSSRS.GetText(1, 1));
		mFeeMainSchema.setCustomerNo(sqlSSRS.GetText(1, 2));
		mFeeMainSchema.setCustomerName(sqlSSRS.GetText(1, 3));
		mFeeMainSchema.setCustomerSex(sqlSSRS.GetText(1, 4));
		mFeeMainSchema.setAge(sqlSSRS.GetText(1, 5));
		
		mFeeMainSchema.setHospitalCode(mLLAppealAcceptSchema.getHospitalCode());
		mFeeMainSchema.setHospitalName(mLLAppealAcceptSchema.getHospitalName());
		mFeeMainSchema.setReceiptNo(mLLAppealAcceptSchema.getReceiptNo());
		mFeeMainSchema.setFeeType(mLLAppealAcceptSchema.getFeeType());
		mFeeMainSchema.setFeeAtti(mLLAppealAcceptSchema.getFeeAtti());
		if(null==mLLAppealAcceptSchema.getFeeAffixType() || "".equals(mLLAppealAcceptSchema.getFeeAffixType())){
			mFeeMainSchema.setFeeAffixType(sqlSSRS2.GetText(1, 2));//fanqing
		}else{
			mFeeMainSchema.setFeeAffixType(mLLAppealAcceptSchema.getFeeAffixType());
		}
		mFeeMainSchema.setFeeDate(mLLAppealAcceptSchema.getFeeDate());//结算日期
		//住院日期
		if("".equals(mLLAppealAcceptSchema.getHospStartDate()) || null==mLLAppealAcceptSchema.getHospStartDate()){
			mFeeMainSchema.setHospStartDate(sqlSSRS2.GetText(1, 4));
		}else{
			mFeeMainSchema.setHospStartDate(mLLAppealAcceptSchema.getHospStartDate());
		}
		//出院日期
		if("".equals(mLLAppealAcceptSchema.getHospEndDate()) || null==mLLAppealAcceptSchema.getHospEndDate()){
			mFeeMainSchema.setHospEndDate(sqlSSRS2.GetText(1, 5));
		}else{
			mFeeMainSchema.setHospEndDate(mLLAppealAcceptSchema.getHospEndDate());
		}
		System.out.println(mFeeMainSchema.getHospEndDate()+"----"+mFeeMainSchema.getHospStartDate());
		sql = "select to_char(to_date('"+mFeeMainSchema.getHospEndDate()+"') - to_date('"+mFeeMainSchema.getHospStartDate()+"')) from dual where 1=1";
		SSRS sqlSSRS3 = tExeSQL.execSQL(sql);
		mFeeMainSchema.setRealHospDate(sqlSSRS3.GetText(1, 1));//住院天数
//		mFeeMainSchema.setSeriousWard(request.getParameter("SeriousWard"));//重症监护天数
		//住院号
		if(null==mLLAppealAcceptSchema.getinpatientNo() || "".equals(mLLAppealAcceptSchema.getinpatientNo())){
			mFeeMainSchema.setInHosNo(sqlSSRS2.GetText(1, 6));
		}else{
			mFeeMainSchema.setInHosNo(mLLAppealAcceptSchema.getinpatientNo());
		}
		//单位社保登记号
		if(null==mLLAppealAcceptSchema.getCompSecuNo() || "".equals(mLLAppealAcceptSchema.getCompSecuNo())){
			mFeeMainSchema.setCompSecuNo(sqlSSRS2.GetText(1, 7));
		}else{
			mFeeMainSchema.setCompSecuNo(mLLAppealAcceptSchema.getCompSecuNo());
		}
		//客户社保号
		if(null==mLLAppealAcceptSchema.getSecurityNo() || "".equals(mLLAppealAcceptSchema.getSecurityNo())){
			mFeeMainSchema.setSecurityNo(sqlSSRS2.GetText(1, 8));
		}else{
			mFeeMainSchema.setSecurityNo(mLLAppealAcceptSchema.getSecurityNo());
		}
		//参保人员类别
		if(null==mLLAppealAcceptSchema.getInsuredStat() || "".equals(mLLAppealAcceptSchema.getInsuredStat())){
			mFeeMainSchema.setInsuredStat(sqlSSRS2.GetText(1, 9));
		}else{
			mFeeMainSchema.setInsuredStat(mLLAppealAcceptSchema.getInsuredStat());
		}
		mFeeMainSchema.setHosGrade(sqlSSRS2.GetText(1, 10));//医院级别
		mFeeMainSchema.setHosDistrict(sqlSSRS2.GetText(1, 11));//城区属性
		mFeeMainSchema.setMngCom(tsMngCom);
		//收据类型
		if(null==mLLAppealAcceptSchema.getReceiptType() || "".equals(mLLAppealAcceptSchema.getReceiptType())){
			mFeeMainSchema.setReceiptType(sqlSSRS2.GetText(1, 12));
		}else{
			mFeeMainSchema.setReceiptType(mLLAppealAcceptSchema.getReceiptType());
		}
		//分割单出具单位
		if(null==mLLAppealAcceptSchema.getIssueUnit() || "".equals(mLLAppealAcceptSchema.getIssueUnit())){
			mFeeMainSchema.setIssueUnit(sqlSSRS2.GetText(1, 13));
		}else{
			mFeeMainSchema.setIssueUnit(mLLAppealAcceptSchema.getIssueUnit());
		}

		if (tsFeeAtti.equals("4"))
		{
			  mLLSecurityReceiptSchema.setRgtNo(sqlSSRS.GetText(1, 1));
			  mLLSecurityReceiptSchema.setCaseNo(caseNo);
			  Set tSet = mMap.keySet();
			  Object[] ss = tSet.toArray();
			  //GY by 2018 07 10
			  System.out.println("==============费用项目明细=====："+ss.length);
			if (ss.length>0) {
				for (int i = 0; i < ss.length; i++) {
					System.out.println("ss==========" + ss[i] + "==="
							+ mMap.get(ss[i]));
					if ("2".equals(ss[i].toString().substring(0, 1))) {
						String tsFeeItemCode = ss[i].toString();
						sql = "SELECT codename,code FROM  ldcode WHERE codetype='llfeeitemtype' AND code LIKE '2%' and code = '"
								+ tsFeeItemCode + "'";
						SSRS sqlSSRS4 = tExeSQL.execSQL(sql);
						String tsFeeItemName = sqlSSRS4.GetText(1, 1);
						String tsFee = (String) mMap.get(ss[i]);
						mReceiptSchema = new LLCaseReceiptSchema();
						mReceiptSchema.setFeeItemCode(tsFeeItemCode);
						mReceiptSchema.setFeeItemName(tsFeeItemName);
						mReceiptSchema.setFee(tsFee);
						mReceiptSchema.setAvaliFlag("0");
						mReceiptSet.add(mReceiptSchema);
					} else if ("3".equals(ss[i].toString().substring(0, 1))) { 
						String tsSecuFeeCode = ss[i].toString();
						sql = "select codename,code,codealias from ldcode where  codetype='llsecufeeitem' and comcode='86' and code = '"
								+ tsSecuFeeCode + "'";
						SSRS sqlSSRS4 = tExeSQL.execSQL(sql);
						String tsSecuFeeName = sqlSSRS4.GetText(1, 1);
						String tsSecuFeeReason = sqlSSRS4.GetText(1, 3);
						String tsSecuFee = (String) mMap.get(ss[i]);
						mFeeOtherItemSchema = new LLFeeOtherItemSchema();
						mFeeOtherItemSchema.setItemCode(tsSecuFeeCode);
						mFeeOtherItemSchema.setDrugName(tsSecuFeeName);
						mFeeOtherItemSchema.setAvliReason(tsSecuFeeReason);
						mFeeOtherItemSchema.setFeeMoney(tsSecuFee);
						mFeeOtherItemSchema.setAvliFlag("0");
						mFeeOtherItemSet.add(mFeeOtherItemSchema);
					}
				}
			}
			System.out.println("UI will begin ");
	
			ICaseCureUI tICaseCureUI = new ICaseCureUI();
			VData tVData = new VData();
			try
			{
			  // 准备传输数据 VData
			GlobalInput tG = new GlobalInput();
			String sqloper = "select rigister from llcase where caseno = '"+caseNo+"'";
			tG.Operator = tExeSQL.execSQL(sqloper).GetText(1, 1);//根据llcase表中rigister字段决定
//			tG.ManageCom ="86360500";
			tG.ManageCom = mLLAppealAcceptSchema.getManageCom();
			 
			  tVData.add(tG);
			  tVData.add(mFeeMainSchema);
			  tVData.add(mReceiptSet);
			  tVData.add(mFeeOtherItemSet);
			  tVData.add(mLLSecurityReceiptSchema);
			  if(!tICaseCureUI.submitData(tVData,tsOperate)){
				  Content = "保存失败，原因是:" + tICaseCureUI.mErrors.getErrorCount();
				  FlagStr = "Fail";
				  return false;
			  }
			}
			catch(Exception ex)
			{
			  Content = "保存失败，原因是:" + ex.toString();
			  FlagStr = "Fail";
			  return false;
			}
		}
		
		return true;
	}
	/**
	 * 账单录入
	 * 录入完毕
	 * 前后台  ICaseCureSaveAll.jsp
	 * @return
	 */
	public boolean feeInput3(){
		System.out.println("【账单录入】====【录入完毕按钮】====【前台校验】");
		ExeSQL tExeSQL = new ExeSQL();
//		String caseNo = "R3600170113000001";//理赔号 
		
		//TODO:新增效验，理赔二核时不可确认账单录入
		String sql = "select rgtstate from llcase where caseno='"+caseNo+"' with ur";
		SSRS sqlSSRS = tExeSQL.execSQL(sql);
		if (sqlSSRS.MaxRow>0){
		     if("16".equals(sqlSSRS.GetText(1,1))){
		    	 Content = "理赔二核中不可确认账单录入，必须等待二核结束！";
		    	 System.out.println("理赔二核中不可确认账单录入，必须等待二核结束！");
		    	 return false;
		     }
		}
		System.out.println("【账单录入】====【录入完毕按钮】====【前台】===End......");
		
		System.out.println("【账单录入】====【录入完毕按钮】====【后台】===Strat.....");
		
		RegisterBL tRegisterBL   = new RegisterBL();
		  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		  VData tVData = new VData();
		  //输出参数
		  CErrors tError = null;
		  GlobalInput tG = new GlobalInput(); 
//		  tG.Operator = "sc3672";
//		  tG.ManageCom = "86360500";
		  tG.Operator = "lipeia";
		  tG.ManageCom = mLLAppealAcceptSchema.getManageCom();
		  tLLCaseSchema.setCaseNo(caseNo);
		  
		  try
		  {
		    tVData.addElement(tLLCaseSchema);
		    tVData.addElement(tG);
		    if(tRegisterBL.submitData(tVData,"RECEIPT||UPDATE"))
		    {
		      Content = "保存成功";
		      System.out.println("保存成功1");
		      FlagStr = "Succ";
		    }
		    else
		    {
		      FlagStr = "Fail";
		    }
		  }
		  catch(Exception ex)
		  {
		    Content = "保存失败，原因是:" + ex.toString();
		    System.out.println("保存失败，原因是:" + ex.toString());
		    FlagStr = "Fail";
		  }
		  if (FlagStr=="Fail")
		  {
		    tError = tRegisterBL.mErrors;
		    if (tError.needDealError())
		    {
		      Content = " 保存失败，原因是："+tError.getFirstError();
		      FlagStr = "Fail";
		      System.out.println(" 保存失败，原因是："+tError.getFirstError());
		      tVData.clear();
		      return false;
		    }
		  }
		  else
		  {
		    Content = "保存成功！";
		    System.out.println("保存成功2");
		    FlagStr = "Succ";
		  }
		
	
		
		return true;
	}
	
	
	public static void main(String[] args) {
		VData vdata = new VData();
		LLAppealAcceptSchema tLLAppealAcceptSchema = new LLAppealAcceptSchema();
		MMap mmap=new MMap();
		tLLAppealAcceptSchema.setFeeAtti("4");
		tLLAppealAcceptSchema.setFeeType("2");
		tLLAppealAcceptSchema.setHospitalName("内蒙古医院");
		tLLAppealAcceptSchema.setHospitalCode("1501001");
		tLLAppealAcceptSchema.setReceiptNo("23456789");
		tLLAppealAcceptSchema.setFeeDate("2017-01-13");
		
		
		
		vdata.add(mmap);
		vdata.add(tLLAppealAcceptSchema);
		FeeInput tFeeInput = new FeeInput();
//		tFeeInput.submitDate(vdata);
	}
}