package com.claimback;

import com.sinosoft.lis.llcase.ClaimCalUI;
import com.sinosoft.lis.llcase.ClaimDeclineUI;
import com.sinosoft.lis.llcase.ClaimSaveUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCalPaySchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDeclineSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimPolicySchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.vschema.LLCalPaySet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealAdjustment {
	// 错误标志
	private String FlagStr = "";
	// 错误信息
	private String Content = "";
	public String getContent() {
		return Content;
	}

	//
	private String RgtNo = "";
	// 案件号
	private String CaseNo = "";
	// 案件状态
	private String RgtState = "";
	// 客户号
	private String CustomerNo = "";
	// 客户姓名
	private String CustomerName = "";
	//
	private String ContRemark = "";
	// 客户性别
	private String Sex = "";
	// 操作人
	private String Handler = "";
	// 申请人数
	private String AppNum = "";
	// 原理赔号
	private String Orig_CaseNo = "";
	//
	private String AppealType = "";
	//
	private String AppeanRCode = "";
	//
	private String AppealReason = "";
	//
	private String AppealRDesc = "";
	//
	private String AppealTypeName = "";
	// 个人保单号
	private String contno = "";
	// 险种编码
	private String RiskCode = "";
	// 给付责任类型
	private String GetDutyKind = "";
	// 给付责任编码
	private String getdutycode = "";
	//
	private String polno = "";

	private LLCalPaySet mLLCalPaySet = new LLCalPaySet();// 理算信息
	LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
	LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
	LLClaimDeclineSchema mLLClaimDeclineSchema = new LLClaimDeclineSchema();
	// 报文中要解析第一个节点信息 后续要赋值
	private LLAppealAcceptSchema mLLAppea = new LLAppealAcceptSchema();
	ClaimDeclineUI tClaimDeclineUI   = new ClaimDeclineUI();
	public static void main(String[] args) {
		DealAdjustment dealAdjustment = new DealAdjustment();
		LLCalPaySchema tLLCalPaySchema = new LLCalPaySchema();
		// mLLClaimDetailSchema.setRgtNo("R3600170119000001");
		// mLLClaimDetailSchema.setContNo("2320929259");
		tLLCalPaySchema.setgetdutycode("670203");
		tLLCalPaySchema.setGetDutyKind("000");
		// mLLClaimDetailSchema.setStandPay(strSQLSSRS.GetText(i, 13));
		// mLLClaimDetailSchema.setRealPay(strSQLSSRS.GetText(i, 14));
		// mLLClaimDetailSchema.setPolNo("21042755020");
		// mLLClaimDetailSchema.setClmNo("52003641079");
		// mLLClaimDetailSchema.setDutyCode(strSQLSSRS.GetText(i, 19));
		// mLLClaimDetailSchema.setCaseRelaNo(strSQLSSRS.GetText(i, 22));
		// mLLClaimDetailSchema.setCaseNo(CaseNo);
		tLLCalPaySchema.setriskcode("690201");
		tLLCalPaySchema.setGiveType("1");
		dealAdjustment.mLLCalPaySet.add(tLLCalPaySchema);
		dealAdjustment.AdjustmentQuery();
	}

	public boolean submitDate(VData cInputData,String caseNo){
		System.out.println("理赔案件--》理赔处理--》理算开始");
		//获取数据
		if (!getInputData(cInputData,caseNo))
        {
            return false;
        }
		if(!AdjustmentQuery()){
			return false;
		}
		System.out.println("理赔案件--》理赔处理--》理算结束");
		return true;
	}
	
	
	private boolean getInputData(VData cInputData, String caseNo2) {
		// TODO Auto-generated method stub
		mLLCalPaySet = null;
		CaseNo = caseNo2;
		mLLCalPaySet = (LLCalPaySet) cInputData.getObjectByObjectName("LLCalPaySet", 1);
		mLLAppea = (LLAppealAcceptSchema) cInputData.getObjectByObjectName("LLAppealAcceptSchema", 1);
		mLLClaimDeclineSchema = (LLClaimDeclineSchema) cInputData.getObjectByObjectName("LLClaimDeclineSchema", 1);
		mLLClaimDeclineSchema.setCaseNo(CaseNo);
		mLLClaimDeclineSchema.setRgtNo(CaseNo);
		return true;
	}

	/**
	 * 理赔案件--》理赔处理--》理算 【取数据】
	 * 
	 * @return by hy 2017-01-17
	 */
	public boolean AdjustmentQuery() {
		ExeSQL aExeSQL = new ExeSQL();

		// 案件基本信息
		if (CaseNo != "") {
			String rgtBasicSql = "select a.RgtNo,a.CustomerNo,a.CustomerName,(case a.CustomerSex when '0' then '男' when '1' then '女' when '2' then '不详' end),a.AccdentDesc,"
					+ "a.handler,b.codename,c.apppeoples,a.rgtstate from llcase a,ldcode b,llregister c where a.CaseNo='"
					+ CaseNo
					+ "' and c.rgtno=a.rgtno and b.codetype='llrgtstate' and b.code=a.rgtstate";
			SSRS rgtBasicSqlSSRS = aExeSQL.execSQL(rgtBasicSql);
			if (rgtBasicSqlSSRS.getMaxRow() > 0) {
				RgtNo = rgtBasicSqlSSRS.GetText(1, 1);
				System.out.println("RgtNo========" + RgtNo);
				CustomerNo = rgtBasicSqlSSRS.GetText(1, 1);
				System.out.println("CustomerNo========" + CustomerNo);
				CustomerName = rgtBasicSqlSSRS.GetText(1, 3);
				System.out.println("CustomerName========" + CustomerName);
				ContRemark = rgtBasicSqlSSRS.GetText(1, 5);
				System.out.println("ContRemark========" + ContRemark);
				Handler = rgtBasicSqlSSRS.GetText(1, 6);
				System.out.println("Handler========" + Handler);
				RgtState = rgtBasicSqlSSRS.GetText(1, 9);
				System.out.println("RgtState========" + RgtState);
				AppNum = rgtBasicSqlSSRS.GetText(1, 8);
				System.out.println("AppNum========" + AppNum);
				Sex = rgtBasicSqlSSRS.GetText(1, 4);
				System.out.println("Sex========" + Sex);
			} else {
				RgtNo = "";
				System.out.println("RgtNo========" + RgtNo);
				CustomerNo = "";
				System.out.println("CustomerNo========" + CustomerNo);
				CustomerName = "";
				System.out.println("CustomerName========" + CustomerName);
				Sex = "";
				System.out.println("Sex========" + Sex);
				ContRemark = "";
				System.out.println("ContRemark========" + ContRemark);
				RgtState = "";
				System.out.println("RgtState========" + RgtState);
				Handler = "";
				System.out.println("Handler========" + Handler);
				AppNum = "";
				System.out.println("AppNum========" + AppNum);
			}
			GlobalInput tG = new GlobalInput();
			// tG=(GlobalInput)session.getValue("GI");
			System.out.println("CaseNo.substring(0, 1)====="
					+ CaseNo.substring(0, 1));
			if ("S".equals(CaseNo.substring(0, 1))
					|| "R".equals(CaseNo.substring(0, 1))) {
				String sql3 = "select CaseNo,appealtype,AppeanRCode,appealreason,appealrdesc,case appealtype when '0' then '申诉类' when '1' then '纠错类' end  from llappeal where appealno='"
						+ CaseNo + "'";
				SSRS sql3SSRS = aExeSQL.execSQL(sql3);
				Orig_CaseNo = sql3SSRS.GetText(1, 1);
				System.out.println("Orig_CaseNo========" + Orig_CaseNo);
				if (null != AppealType && null != AppeanRCode
						&& null != AppealReason && null != AppealRDesc
						&& null != AppealTypeName)// 只有在申诉纠错页面时符合
				{
					AppealType = sql3SSRS.GetText(1, 2);
					System.out.println("AppealType========" + AppealType);
					AppeanRCode = sql3SSRS.GetText(1, 3);
					System.out.println("AppeanRCode========" + AppeanRCode);
					AppealReason = sql3SSRS.GetText(1, 4);
					System.out.println("AppealReason========" + AppealReason);
					AppealRDesc = sql3SSRS.GetText(1, 5);
					System.out.println("AppealRDesc========" + AppealRDesc);
					AppealTypeName = sql3SSRS.GetText(1, 6);
					System.out.println("AppealTypeName========"
							+ AppealTypeName);
				}
			}
		}

		// 赔付保单明细
		for (int i = 1; i < mLLCalPaySet.size(); i++) {
			RiskCode += "'" + mLLCalPaySet.get(i).getriskcode() + "',";
			GetDutyKind += "'" + mLLCalPaySet.get(i).getGetDutyKind() + "',";
			getdutycode += "'" + mLLCalPaySet.get(i).getgetdutycode() + "',";
		}
//		getdutycode += "'"
//				+ mLLCalPaySet.get(mLLCalPaySet.size()).getgetdutycode() + "'";
//		RiskCode += "'" + mLLCalPaySet.get(mLLCalPaySet.size()).getriskcode()
//				+ "'";
//		GetDutyKind += "'"
//				+ mLLCalPaySet.get(mLLCalPaySet.size()).getGetDutyKind() + "'";
		if(1==mLLCalPaySet.size()){
			RiskCode = "'" + mLLCalPaySet.get(mLLCalPaySet.size()).getriskcode()
					+ "'";
			GetDutyKind = "'"
					+ mLLCalPaySet.get(mLLCalPaySet.size()).getGetDutyKind() + "'";
			getdutycode = "'"
					+ mLLCalPaySet.get(mLLCalPaySet.size()).getgetdutycode() + "'";
		}
		System.out.println("RiskCode========" + RiskCode);
		System.out.println("GetDutyKind========" + GetDutyKind);
		System.out.println("getdutycode========" + getdutycode);
		
//		StringBuffer payDetailSql = new StringBuffer();
//		payDetailSql
//				.append(" select distinct contno,b.riskname,a.GetDutyKind,'',");
//		payDetailSql.append(" a.StandPay,a.RealPay,a.polno,a.clmno,");
//		payDetailSql
//				.append(" a.GiveTypeDesc,a.GiveType,a.riskcode,(select amnt from lcpol where polno=a.polno union select  amnt from lbpol where polno=a.polno) ");
//		payDetailSql.append(" from LLClaimpolicy a, lmrisk b where caseno='"
//				+ CaseNo + "'");
//		payDetailSql.append(" and a.riskcode = b.riskcode ");
//		System.out.println("RiskCode========" + RiskCode);
//		payDetailSql.append(" and a.RiskCode in (" + RiskCode + ")");// 将报文中传值放入限定条件中
//		System.out.println("GetDutyKind========" + GetDutyKind);
//		payDetailSql.append(" and a.getdutykind in (" + GetDutyKind + ")");// 将报文中传值放入限定条件中
//		payDetailSql.append(" order by a.polno");
		String payDetailSql = "select a.rgtno,a.contno,a.riskcode,a.getdutykind,a.polno from llclaimdetail a, llappeal b where a.caseno=b.caseno and b.appealno='"+CaseNo+"'";
		
		SSRS payDetailSqlSSRS = aExeSQL.execSQL(payDetailSql.toString());
		LLClaimPolicySchema tLLClaimPolicySchema = null;
		for(int i=1;i<=payDetailSqlSSRS.getMaxRow();i++){
			if(!"".equals(payDetailSqlSSRS.GetText(i, 1))&& null!=payDetailSqlSSRS.GetText(i, 1)&&!"".equals(payDetailSqlSSRS.GetText(i, 5))&& null!=payDetailSqlSSRS.GetText(i, 5)){
				
				tLLClaimPolicySchema = new LLClaimPolicySchema();
				tLLClaimPolicySchema.setRgtNo(RgtNo);
				tLLClaimPolicySchema.setContNo(payDetailSqlSSRS.GetText(i, 2));
				tLLClaimPolicySchema.setRiskCode(payDetailSqlSSRS.GetText(i, 3));
				tLLClaimPolicySchema.setCaseNo(CaseNo);
				tLLClaimPolicySchema.setGetDutyKind(payDetailSqlSSRS.GetText(i, 4));
				tLLClaimPolicySchema.setPolNo(payDetailSqlSSRS.GetText(i, 5));
				tLLClaimPolicySet.add(tLLClaimPolicySchema);
			}
		}


		
		if (!calculateCheck()) {
			return false;
		}
		if (!confirmCheck()) {
			return false;
		}
		return true;
	}

	/**
	 * 理赔案件--》理赔处理--》理算 【计算校验】
	 * 
	 * @return by hy 2017-01-16
	 */
	public boolean calculateCheck() {
		System.out.println("==========【理算】===【计算按钮】==前台校验");
		ExeSQL sql = new ExeSQL();

		StringBuffer payDetailSql = new StringBuffer();
		payDetailSql
				.append(" select distinct contno,b.riskname,a.GetDutyKind,'',");
		payDetailSql.append(" a.StandPay,a.RealPay,a.polno,a.clmno,");
		payDetailSql
				.append(" a.GiveTypeDesc,a.GiveType,a.riskcode,(select amnt from lcpol where polno=a.polno union select  amnt from lbpol where polno=a.polno) ");
		payDetailSql.append(" from LLClaimpolicy a, lmrisk b where caseno='"
				+ CaseNo + "'");
		payDetailSql.append(" and a.riskcode = b.riskcode ");
		System.out.println("RiskCode========" + RiskCode);
		payDetailSql.append(" and a.RiskCode in (" + RiskCode + ")");// 将报文中传值放入限定条件中
		System.out.println("GetDutyKind========" + GetDutyKind);
		payDetailSql.append(" and a.getdutykind in (" + GetDutyKind + ")");// 将报文中传值放入限定条件中
		payDetailSql.append(" order by a.polno");

		System.out.println("payDetailSql=======" + payDetailSql.toString());

		SSRS strSQLSSRS = sql.execSQL(payDetailSql.toString());

		if (strSQLSSRS.getMaxRow() <= 0) {
			Content = "赔付保单明细不能为空，请检查！";
			System.out.println("赔付保单明细不能为空，请检查！");
			return false;
		}

		for (int i = 1; i <= strSQLSSRS.getMaxRow(); i++) {
			String cn = strSQLSSRS.GetText(i, 1);
			System.out.println("////////赔付明细--保单号========：" + cn);
			String rn = strSQLSSRS.GetText(i, 2);
			System.out.println("////////赔付明细--险种名称========" + rn);
			String dc = strSQLSSRS.GetText(i, 3);
			System.out.println("////////赔付明细--给付责任类型========" + dc);

			if ("".equals(cn.trim()) || null == cn) {
				Content = "请填写相应的保单号，保单号不能为空！";
				System.out.println("请填写相应的保单号，保单号不能为空！");
				return false;
			}

			if ("".equals(rn.trim()) || null == rn) {
				Content = "请填写保单" + cn + "对应的险种名称！";
				System.out.println("请填写保单" + cn + "对应的险种名称！");
				return false;
			}

			if ("".equals(dc.trim()) || null == dc) {
				Content = "请填写保单" + cn + "下的" + rn + "险种对应的给付责任类型！";
				System.out.println("请填写保单" + cn + "下的" + rn + "险种对应的给付责任类型！");
				return false;
			}

			// TODO：普通案件，不可添加社保保单
			String tSocial = RgtNo;
			if (!checkSocial(cn, tSocial, "normal")) {
				return false;
			}

			// TODO:新增效验，理赔二核时不可理赔计算
			String sqlUW = "select rgtstate from llcase where caseno='"
					+ CaseNo + "' with ur";
			SSRS rgtstateUW = sql.execSQL(sqlUW);
			if (rgtstateUW.getMaxRow() > 0) {
				if (rgtstateUW.GetText(1, 1) == "16") {
					Content = "理赔二核中不能处理，必须等待二核结束！";
					System.out.println("理赔二核中不能处理，必须等待二核结束！");
					return false;
				}
			}
		}
		if (!claimCalSave()) {
			return false;
		}
		return true;

	}

	/**
	 * 
	 * @param Contno
	 * @param CaseNo
	 * @param Type
	 * @return
	 */
	public boolean checkSocial(String Contno, String CaseNo, String Type) {
		ExeSQL aSql = new ExeSQL();
		String sql;
		String tContno = Contno;
		String tCaseNo = CaseNo;

		if (null != tContno && !"".equals(tContno) && "".equals(Type)) {
			sql = "select CHECKGRPCONT(grpcontno) from lccont where contno = '"
					+ Contno + "' with ur";
			SSRS aSqlSSRS = aSql.execSQL(sql);
			if (aSqlSSRS.getMaxRow() > 0) {
				if (aSqlSSRS.GetText(1, 1) == "Y") {
					Content = "社保案件暂不支持该处理！";
					System.out.println("社保案件暂不支持该处理！");
					return false;
				}
			} else {

			}

		}
		if (null != tCaseNo && !"".equals(tCaseNo) && "".equals(Type)) {
			// 此处判断需要支持社保的申诉纠错案件
			if (CaseNo.substring(0, 1) == "R" || CaseNo.substring(0, 1) == "S") {
				String SQLCaseNo = "select caseno from llcase where caseno=(select distinct caseno from LLAppeal where appealno ='"
						+ tCaseNo + "')";
				SSRS SQLCaseNoSSRS = aSql.execSQL(SQLCaseNo);
				CaseNo = SQLCaseNoSSRS.GetText(1, 1);
			}
			String sql1 = "Select CHECKGRPCONT(rgtobjno) from llregister where rgtno = (select rgtno from llcase where caseno = '"
					+ CaseNo + "') with ur";
			SSRS sql1SSRS = aSql.execSQL(sql1);
			if (sql1SSRS.getMaxRow() > 0) {// 社保案件
				if (sql1SSRS.GetText(1, 1) == "Y") {
					Content = "社保案件暂不支持该处理！";
					System.out.println("社保案件暂不支持该处理！");
					return false;
				}
			}
		}
		if (null != tContno && !"".equals(tContno) && "insuredno".equals(Type)) {
			String tSql = "select distinct grpcontno from lcinsured where insuredno='"
					+ tContno + "'";
			SSRS tSqlSSRS = aSql.execSQL(tSql);
			if (tSqlSSRS.getMaxRow() > 0) {
				int n = tSqlSSRS.getMaxRow();
				if (n == 1) {
					sql = "select CHECKGRPCONT('" + tSqlSSRS.GetText(1, 1)
							+ "') from dual with ur";
					SSRS sqlSSRS = aSql.execSQL(sql);
					if (sqlSSRS.getMaxRow() > 0) {
						if (sqlSSRS.GetText(1, 1) == "Y") {
							Content = "社保案件暂不支持该处理！";
							System.out.println("社保案件暂不支持该处理！");
							return false;
						}
					} else {

					}
				}
			}
		}
		// 社保案件不许添加非社保保单、非社保案件不许添加社保保单
		if (null != tContno && !"".equals(tContno) && null != tCaseNo
				&& !"".equals(tCaseNo) && "normal".equals(Type)) {
			// 此处判断需要支持社保的申诉纠错案件
			if (CaseNo.substring(0, 1) == "R" || CaseNo.substring(0, 1) == "S") {
				String SQLCaseNo = "select rgtno from llcase where caseno=(select distinct caseno from LLAppeal where appealno ='"
						+ tCaseNo + "')";
				SSRS SQLCaseNoSSRS = aSql.execSQL(SQLCaseNo);
				CaseNo = SQLCaseNoSSRS.GetText(1, 1);
			}
			String sql1 = "Select CHECKGRPCONT(rgtobjno) from llregister where rgtno = '"
					+ CaseNo + "'";
			SSRS sql1SSRS = aSql.execSQL(sql1);
			if (sql1SSRS.getMaxRow() > 0) {// 社保案件
				if (sql1SSRS.GetText(1, 1) == "Y") {
					String sql2 = "select CHECKGRPCONT(grpcontno) from lccont where contno = '"
							+ Contno + "' with ur";
					SSRS sql2SSRS = aSql.execSQL(sql2);
					if (sql2SSRS.getMaxRow() > 0) {
						if (sql2SSRS.GetText(1, 1) != "Y") {
							Content = "社保案件暂不支持普通保单处理！";
							System.out.println("社保案件暂不支持普通保单处理！");
							return false;
						}
					} else {

					}
				} else {// 非社保案件
					String sql3 = "select CHECKGRPCONT(grpcontno) from lccont where contno = '"
							+ Contno + "' with ur";
					SSRS sql3SSRS = aSql.execSQL(sql3);
					if (sql3SSRS.getMaxRow() > 0) {
						if (sql3SSRS.GetText(1, 1) == "Y") {
							Content = "普通案件暂不支持社保保单处理！";
							System.out.println("普通案件暂不支持社保保单处理！");
							return false;
						}
					} else {

					}
				}
			} else {// 非社保案件
				String sql4 = "select CHECKGRPCONT(grpcontno) from lccont where contno = '"
						+ Contno + "' with ur";
				SSRS sql4SSRS = aSql.execSQL(sql4);
				if (sql4SSRS.getMaxRow() > 0) {
					if (sql4SSRS.GetText(1, 1) == "Y") {
						Content = "普通案件暂不支持社保保单处理！";
						System.out.println("普通案件暂不支持社保保单处理！");
						return false;
					}
				} else {

				}
			}

		}
		return true;
	}

	/**
	 * 理赔案件--》理赔处理--》理算 【确认校验】
	 * 
	 * @return by hy 2017-01-17
	 */
	public boolean confirmCheck() {
		ExeSQL sql = new ExeSQL();
		String tAccSql = "select distinct accdate from llcaserela a,llsubreport b where a.subrptno=b.subrptno and a.caseno='"
				+ CaseNo + "'";
		SSRS tAccSqlSSRS = sql.execSQL(tAccSql);
		if (tAccSqlSSRS.getMaxRow() <= 0) {
			Content = "出险日期查询失败！";
			System.out.println("出险日期查询失败！");
			return false;
		}
		System.out.println("出险日期: " + tAccSqlSSRS.GetText(1,1));
		String HangUpNo = "";


		//=理算计算过的 保单明细
		String strSQL = "select contno,RiskCode,b.getdutyname,a.TabFeeMoney,a.DeclineAmnt,a.pregiveAmnt,"
				+ "a.ApproveAmnt,OutDutyAmnt,OverAmnt,a.GiveTypeDesc,a.GiveReasonDesc,a.ClaimMoney,a.StandPay,a.RealPay,"
				+ "a.polno,a.clmno,a.getdutycode,a.getdutykind,a.dutycode,a.GiveType,a.GiveReason,a.caserelano "
				+ " from LLClaimdetail a, LMDutyGetClm b where  a.caseno='"
				+ CaseNo
				+ "'"
				+ " and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind"
				+ " AND a.getdutycode IN("
				+ getdutycode
				+ ") AND a.getdutykind in ("
				+ GetDutyKind
				+ ") AND a.RiskCode in(" + RiskCode + ")";
		SSRS strSQLSSRS = sql.execSQL(strSQL);
		System.out.println("+++++++++++++++++++++赔付保单明细+++++++："+strSQLSSRS.getMaxRow());
		for (int i = 1; i <= strSQLSSRS.getMaxRow(); i++) {
			LLClaimDetailSchema mLLClaimDetailSchema = new LLClaimDetailSchema();
			mLLClaimDetailSchema.setContNo(strSQLSSRS.GetText(i, 1));
			mLLClaimDetailSchema.setGetDutyCode(strSQLSSRS.GetText(i, 17));
			mLLClaimDetailSchema.setGetDutyKind(strSQLSSRS.GetText(i, 18));
			mLLClaimDetailSchema.setStandPay(strSQLSSRS.GetText(i, 13));
			mLLClaimDetailSchema.setRealPay(strSQLSSRS.GetText(i, 14));
			mLLClaimDetailSchema.setPolNo(strSQLSSRS.GetText(i, 15));
			mLLClaimDetailSchema.setClmNo(strSQLSSRS.GetText(i, 16));
			mLLClaimDetailSchema.setDutyCode(strSQLSSRS.GetText(i, 19));
			
			
			mLLClaimDetailSchema.setCaseRelaNo(strSQLSSRS.GetText(i, 22));
			mLLClaimDetailSchema.setRgtNo(RgtNo);
			mLLClaimDetailSchema.setCaseNo(CaseNo);
			for (int j = 1; j <= mLLCalPaySet.size(); j++) {
				System.out.println(strSQLSSRS.GetText(i, 2) + "-----");
				if (strSQLSSRS.GetText(i, 2).equals(mLLCalPaySet.get(j).getriskcode())&& strSQLSSRS.GetText(i, 17).equals(mLLCalPaySet.get(j).getgetdutycode())
						&& strSQLSSRS.GetText(i, 18).equals(
								mLLCalPaySet.get(j).getGetDutyKind())
						) {
					mLLClaimDetailSchema.setGiveReason(mLLCalPaySet.get(j)
							.getGiveReason());
					mLLClaimDetailSchema.setGiveType(mLLCalPaySet.get(j)
							.getGiveType());
					mLLClaimDetailSchema.setGiveTypeDesc(mLLCalPaySet.get(j)
							.getGiveTypeDesc());
					mLLClaimDetailSchema.setGiveReasonDesc(mLLCalPaySet.get(j)
							.getGiveReasonDesc());
					mLLClaimDetailSchema.setDeclineNo(mLLCalPaySet.get(j)
							.getIsRate());
					if ("".equals(mLLCalPaySet.get(j).getApproveAmnt())) {
						mLLClaimDetailSchema.setDeclineAmnt(strSQLSSRS.GetText(
								i, 5));
					} else {
						mLLClaimDetailSchema.setDeclineAmnt(mLLCalPaySet.get(j)
								.getDeclineAmnt());
					}
					if ("".equals(mLLCalPaySet.get(j).getApproveAmnt())) {
						mLLClaimDetailSchema.setApproveAmnt(strSQLSSRS.GetText(
								i, 7));
					} else {
						mLLClaimDetailSchema.setApproveAmnt(mLLCalPaySet.get(j)
								.getApproveAmnt());
					}
				}
			}
			mLLClaimDetailSet.add(mLLClaimDetailSchema);
		}
	
		for (int rowNum = 1; rowNum <= strSQLSSRS.getMaxRow(); rowNum++) {
			String tContNo = strSQLSSRS.GetText(rowNum, 2);
			String strsql2 = "select a.contno from LCContHangUpState a where a.contno='"
					+ tContNo + "' and a.HangUpType='2' and a.state<>'0' ";
			SSRS strsql2SSRS = sql.execSQL(strsql2);
			if (strsql2SSRS.getMaxRow() > 0) {
				HangUpNo = HangUpNo + " " + strsql2SSRS.GetText(1, 1);
			}
		}
		if (null == HangUpNo && "".equals(HangUpNo)) {
			Content = "该保单" + HangUpNo.substring(10) + "被保全挂起！";
			System.out.println("该保单" + HangUpNo.substring(10) + "被保全挂起！");
			return false;
		}
		// 新增校验，对于赔付正在进行结余返还操作以及已完成结余返还项目的保单，在理算确认时校验并阻断
		// add by GY 2013-1-8
		// 1.先根据页面上的保单号查询该保单号对应的grpcontno
		// var
		// GrpContSQL="select grpcontno from lccont where contno='"+tContNo+"' and grpcontno not in ('00149574000001','00092763000002','00149574000002')";
		// add by lyc 2014-6-11 改为配置
		String GrpContSQL = "select distinct grpcontno from llclaimdetail where rgtno='"
				+ RgtNo
				+ "' and grpcontno not in (select code from ldcode where codetype='lp_jyfh_pass')";
		SSRS GrpContSQLSSRS = sql.execSQL(GrpContSQL);
		// 2.根据grpcontno查询该单是否正在进行结余返回操作
		String CheckBQSQL = "select 1 from lpgrpedoritem where grpcontno='"
				+ GrpContSQLSSRS.GetText(1, 1) + "' and edortype = 'BJ'";
		SSRS CheckBQSQLSSRS = sql.execSQL(CheckBQSQL);
		// 3.判断上述sql是否存在数据，是阻断否通过
		if (CheckBQSQLSSRS.getMaxRow() > 0) {
			Content = "该保单" + GrpContSQLSSRS.GetText(1, 1)
					+ "正在进行结余返还操作或者已完成结余返还项目，不能理算确认！";
			System.out.println("该保单" + GrpContSQLSSRS.GetText(1, 1)
					+ "正在进行结余返还操作或者已完成结余返还项目，不能理算确认！");
			return false;
		}

		// 对于欠缴保费，保单处于中止状态,提示并阻止进行理算
		// TODO: 该校验只针对#48 《康利人生两全保险（分红型）》
		String contSQL = " select distinct contno,b.riskname,a.GetDutyKind,'',"
				+ " a.StandPay,a.RealPay,a.polno,a.clmno,"
				+ " a.GiveTypeDesc,a.GiveType,a.riskcode,(select amnt from lcpol where polno=a.polno union select  amnt from lbpol where polno=a.polno) "
				+ " from LLClaimpolicy a, lmrisk b " + " where caseno='"
				+ CaseNo + "'" + " and a.riskcode = b.riskcode "
				+ " order by a.polno";
		SSRS contSQLSSRS = sql.execSQL(contSQL);
		for (int rowNum = 1; rowNum <= contSQLSSRS.getMaxRow(); rowNum++) {
			String contno = contSQLSSRS.GetText(rowNum, 1);
			String riskcode = contSQLSSRS.GetText(rowNum, 11);// 险种代码
			String polno = contSQLSSRS.GetText(rowNum, 7); // 保单险种号

			// TODO：普通案件，不可添加社保保单
			String tSocial = RgtNo;
			if (!checkSocial(contno, tSocial, "normal")) {
				return false;
			}

			if (riskcode == "730101" || riskcode == "332401") {
				// String sql =
				// "select 1 from lccont where stateflag='2' and contno='" +
				// contno +"'";
				// var result = easyExecSql(sql);
				// alert(result);

				String sql2 = "select 1 from lcpol where polno= '"
						+ polno
						+ "' "
						+ "and (stateflag in ('1','2') or StateFlag is null) and AppFlag = '1' and  paytodate<payenddate "
						+ "and paytodate<=(select accdate from LLSubReport where subrptno in( "
						+ "select subrptno from llcaserela where caseno='"
						+ CaseNo + "'))";
				SSRS sql2SSRS = sql.execSQL(sql2);
				if (sql2SSRS.getMaxRow() <= 0) {
					Content = "保单" + contno + "有欠缴保费不能进行理赔！";
					System.out.println("保单" + contno + "有欠缴保费不能进行理赔！");
					return false;
				}
			}
		}
		String str1SQL = "select contno,RiskCode,b.getdutyname,a.TabFeeMoney,a.DeclineAmnt,a.pregiveAmnt,"
				+ "a.ApproveAmnt,OutDutyAmnt,OverAmnt,a.GiveTypeDesc,a.GiveReasonDesc,a.ClaimMoney,a.StandPay,a.RealPay,"
				+ "a.polno,a.clmno,a.getdutycode,a.getdutykind,a.dutycode,a.GiveType,a.GiveReason,a.caserelano "
				+ " from LLClaimdetail a, LMDutyGetClm b where  a.caseno='"
				+ CaseNo
				+ "'"
				+ " and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind";
		SSRS str1SQLSSRS = sql.execSQL(str1SQL);
		if (str1SQLSSRS.getMaxRow() == 0) {
			Content = "请作赔付明细计算";
			System.out.println("请作赔付明细计算");
			return false;
		}

		// TODO:新增效验，理赔二核时不可理算确认
		String rgtsSql = "select rgtstate from llcase where caseno='" + CaseNo
				+ "' with ur";
		SSRS rgtsSqlSSRS = sql.execSQL(rgtsSql);
		if (rgtsSqlSSRS.getMaxRow() > 0) {
			if (rgtsSqlSSRS.GetText(1, 1) == "16") {
				Content = "理赔二核中不可理算确认，必须等待二核结束！";
				System.out.println("理赔二核中不可理算确认，必须等待二核结束！");
				return false;
			}
		}

		// var i = 0;
		// var tRealPay = 0;
		// var tCalPay=0;
		// var tTempPolPay=0;
		// mSaveType="savepay" ;
		// fm.SubCalculate.disabled=true;
		if (!claimSubSave()) {
			return false;
		}
		return true;
	}

	/*
	 * 理算 计算按钮后台
	 */
	public boolean claimCalSave() {
		System.out.println("=========【理算】====【计算后台】===");
		
		LLClaimSchema tLLClaimSchema = new LLClaimSchema();
		LLClaimSet tLLClaimSet = new LLClaimSet();
		LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		
		GlobalInput tG = new GlobalInput();
		// tG=(GlobalInput)session.getValue("GI");
		System.out.println("=========================cs================");
//		tG.Operator = "sc3612";
//		tG.ManageCom = "86360500";
		tG.Operator = "lipeic";
		tG.ManageCom = mLLAppea.getManageCom();
		TransferData RecalFlag = new TransferData();

		String transact = "";
		String FlagStr = "";
		CErrors tError = null;
		System.out.println("CaseNo" + CaseNo);

			transact = "Cal";
			RecalFlag.setNameAndValue("RecalFlag", "N");
			tLLCaseSchema.setCaseNo(CaseNo);
			ClaimCalUI tClaimCalUI = new ClaimCalUI();
			// 准备传输数据 VData
			VData tVData = new VData();
			try {
				tVData.addElement(tG);
				tVData.addElement(tLLCaseSchema);
				tVData.addElement(tLLClaimPolicySet);
				tVData.addElement(RecalFlag);
				tClaimCalUI.submitData(tVData, transact);
			} catch (Exception ex) {
				Content = transact + "失败，原因是:" + ex.toString();
				FlagStr = "Fail";
				return false;
			}
			if (FlagStr == "") {
				tError = tClaimCalUI.mErrors;
				if (!tError.needDealError()) {
					Content = " 计算成功!" + tClaimCalUI.getBackMsg();
					FlagStr = "Succ";
				} else {
					Content = " 计算失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return false;
				}
			}
		return true;
	}

	public boolean claimSubSave() {
		// 调用后台
		LLClaimSchema tLLClaimSchema = new LLClaimSchema();
		LLClaimSet tLLClaimSet = new LLClaimSet();
		LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
		mLLClaimDeclineSchema.setClmNo(tLLClaimSchema.getClmNo());
		LLClaimPolicySchema tLLClaimPolicySchema = null;
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
		GlobalInput tG = new GlobalInput();
//		tG.Operator = "ss3601";
//		tG.ManageCom = "8636";
		tG.Operator = "lipeib";
//		tG.ComCode="8636";
		tG.ManageCom = mLLAppea.getManageCom().substring(0, 4);
		tG.ComCode=tG.ManageCom;
		String transact = "";
		String FlagStr = "";
		CErrors tError = null;
		// tG=(GlobalInput)session.getValue("GI");
		transact = "SAVE";
		tLLCaseSchema.setCaseNo(CaseNo);
		if ("1".equals(mLLCalPaySet.get(1).getEasyCase())) {
			tLLCaseSchema.setCaseProp("09");
		} else {
			tLLCaseSchema.setCaseProp("08");
		}
		ClaimSaveUI tClaimSaveUI = new ClaimSaveUI();
		boolean cs = true;
		// 准备传输数据 VData
		VData tVData = new VData();
		try {
			tVData.addElement(tG);
			tVData.addElement(mLLClaimDetailSet);
			tVData.addElement(tLLCaseSchema);
			//全额拒付原因保存
			if("3".equals(mLLClaimDetailSet.get(1).getGiveType())){
				mLLClaimDeclineSchema.setClmNo(mLLClaimDetailSet.get(1).getClmNo());
				CErrors eError = null;
				 try
				  {
				  // 准备传输数据 VData
				  	VData eVData = new VData();
						eVData.add(mLLClaimDeclineSchema);
						eVData.add(tG);
				    tClaimDeclineUI.submitData(eVData,"INSERT");
				  }
				  catch(Exception ex)
				  {
				    Content = "保存失败，原因是:" + ex.toString();
				    FlagStr = "Fail";
				  }
				  
				//如果在Catch中发现异常，则不从错误类中提取错误信息
				  if (FlagStr=="")
				  {
				    eError = tClaimDeclineUI.mErrors;
				    if (!eError.needDealError())
				    {                          
				    	Content = " 保存成功! ";
				    	FlagStr = "Success";
				    }
				    else                                                                           
				    {
				    	Content = " 保存失败，原因是:" + eError.getFirstError();
				    	FlagStr = "Fail";
				    }
				  }
				} 
			//全额拒付原因END
			cs = tClaimSaveUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = transact + "理算确认失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}

		if (FlagStr == "") {
			tError = tClaimSaveUI.mErrors;
			if (cs || !tError.needDealError()) {
				Content = "保存成功" + tClaimSaveUI.getBackMsg();
				FlagStr = "Succ";
			} else {
				Content = "理算确认保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
		if ("Fail".equals(FlagStr)) {
			System.out.println(Content);
			return false;
		}
		
		return true;
	}
}
