package com.claimback.appealCorrection;

import com.sinosoft.lis.llcase.ClientRegisterBackBL;
import com.sinosoft.lis.llcase.LLFirstDutyFilterBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLAppealSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealAppealCorrect {
	private String CaseNo = "";
	// 报文中获取对应的案件号 P
	private String RgtNo = "";
	// 申诉/错误类型【0-申诉类，1-纠错类】
	private String AppealType = "";
	// 申诉/错误原因编码【1-业务员原因，2-条款理解原因，3-核保处理原因，4-合同管理原因，5-健康管理原因，6-理赔处理原因，7-服务态度原因，8-社保政策原因，9-其他】
	private String AppeanRCode = "";
	// 申诉/错误原因
	private String AppealReason = "";
	// 备注
	private String AppealRDesc = "";
	// 申请人与被保人关系【00-本人,01-配偶,02-子女,03-父母,04-亲属,05-其他】
	private String relation = "1";
	// 受理方式——》此栏内容可以由sql查询得到值【1-电话，2-电子邮件，3-传真，4-上门，5-信函，6-医保通，7-意外险平台，8-社保平台，9-其他】
	private String RgtType = "";
	// 回执发送方式【1-上门，2-信函，3-电子邮件，4-短信，5-电话，6-传真，9-其他】
	private String returnMode = "";
	// 申请人姓名
	private String rgtantName = "";
	// 申请人证件号码
	private String idno = "";
	// 申请人证件类型【0-身份证,1-护照,2-军官证,3-工作证,4-其它,5-户口本,6-出生证,7-回乡证/台胞证】
	private String idtype = "";
	// 申请人电话
	private String rgtantphone = "";
	// 申请人手机
	private String rgtantMobile = "";
	// 申请人电子邮箱
	private String Email = "";
	// 申请人地址
	private String rgtantAddress = "";
	// 邮政编码
	private String postCode = "";
	// 受益金领取方式编码【1-现金，11-银行汇款，2-现金支票，3-转账支票，4-银行转账】
	private String paymode = "";
	//受益金领取方式名称【1-现金，11-银行汇款，2-现金支票，3-转账支票，4-银行转账】
	private String paymodeName = "";
	// 银行编码
	private String bankCode = "";
	// 签约银行
	private String bankName = "";
	// 银行账号
	private String bankAccNo = "";
	// 银行账户名
	private String accName = "";
	// 客户姓名
	private String CustomerName = "";
	// 客户号码
	private String CustomerNo = "";
	// 证件号码
	private String custIdno = "";
	// 证件类型【0-身份证,1-护照,2-军官证,3-工作证,4-其它,5-户口本,6-出生证,7-回乡证/台胞证】
	private String custIdtype = "";
	// 出生日期
	private String custBirthday = "";
	// 性别【页面中隐藏字段】
	private String custSex = "";
	// 社保号码
	private String OtherIDNo = "";
	// 手机号码——》没找到
	private String phone = "";
	// 事故者现状【01-治疗前，02-门诊，03-住院，04-出院痊愈，05-出院好转，06-出院无效，07-死亡，09-其他】
	private String CustStatus = "";
	// 死亡日期
	private String DeathDate = "";
	// 申请原因编码【01-门诊费用，02-住院费用，03-医疗津贴，04-重大疾病，05-身故，06-护理，07-失能，08-伤残，09-特需，10-门诊大额】
	private String appReasonCode = "";
	// 预估申请金额
	private String appAmnt = "";
	// 回销预付赔款
	private String PrePaidFlag = "";
	//错误标志
	private String FlagStr = "";
	//错误信息
	String Content = "";
	
	public String getContent() {
		return Content;
	}

	// 报文中要解析第一个节点信息 后续要赋值
	private LLAppealAcceptSchema mLLAppea = new LLAppealAcceptSchema();
	private LLSubReportSet tEventSet = new LLSubReportSet();// 客户事件信息
	// 如果报文中没有传入事件信息 查询的事件信息
	private LLSubReportSet tEvent2Set = new LLSubReportSet();
	
	CErrors tError = null;
	
	public static void main(String[] args) {
		DealAppealCorrect dealAppealCorrect = new DealAppealCorrect();
		dealAppealCorrect.correctionQuery();
	}
	
	public boolean submitDate(VData cInputData){
		//获取数据
		if (!getInputData(cInputData))
        {
            return false;
        }
		if(!correctionQuery()){
			return false;
		}
		return true;
	}
	
	private boolean getInputData(VData cInputData) {
		mLLAppea = null;
		tEventSet = null;
//		mMap = null;
		mLLAppea = (LLAppealAcceptSchema) cInputData.getObjectByObjectName("LLAppealAcceptSchema", 0);
		tEventSet = (LLSubReportSet) cInputData.getObjectByObjectName("LLSubReportSet", 0);
//		mMap = (MMap) cInputData.getObjectByObjectName("MMap", 0);
		return true;
	}

	/**
	 * 理赔案件--》理赔处理--》申诉/纠错受理 【申请确认校验】
	 * 
	 * @return by hy 2017-01-11
	 */
	public boolean correctionCheck() {
		
		String sql = "select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='"
				+ CustomerNo
				+ "') and a.edorstate !='0'  and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
		String sql2 = "select contno From lpedoritem a,lpedorapp b where  contno in(select contno from lccont where insuredno='"
				+ CustomerNo
				+ "') and a.edoracceptno = b.edoracceptno and b.edorstate !='0'";
		ExeSQL aExeSQL = new ExeSQL();
		SSRS sqlSSRS = aExeSQL.execSQL(sql);
		if (sqlSSRS.getMaxRow() > 0) {
			Content = "该客户正在进行保全减人操作！";
			System.out.println("该客户正在进行保全减人操作！");
			return false;
		}
		if (null == paymode || "".equals(paymode)) {// 此处校验报文中未传值，查询也为空所以会返回错误信息
			Content = "受益金领取方式不能为空！";
			System.out.println("受益金领取方式不能为空！");
			return false;
		}
		if (null == relation || "".equals(relation)) {// 此处校验报文中未传值，查询也为空所以会返回错误信息
			Content = "申请人与被保人关系不能为空!";
			System.out.println("申请人与被保人关系不能为空!");
			return false;
		}
		if (null == RgtType || "".equals(RgtType)) {// 此处校验报文中未传值，查询也为空所以会返回错误信息
			Content = "受理方式不能为空！";
			System.out.println("受理方式不能为空！");
			return false;
		}
		if (null == rgtantName || "".equals(rgtantName)) {// 此处校验报文中未传值，查询也为空所以会返回错误信息
			Content = "申请人姓名不能为空！";
			System.out.println("申请人姓名不能为空！");
			return false;
		}
		if (paymode != "1" && paymode != "2") {
			if (null == bankCode || "".equals(bankCode)) {// 此处校验报文中未传值，查询也为空所以会返回错误信息
				Content = "银行编码不能为空！";
				System.out.println("银行编码不能为空！");
				return false;
			}
			if (null == bankAccNo || "".equals(bankAccNo)) {
				Content = "银行账号不能为空！";
				System.out.println("银行账号不能为空！");
				return false;
			}
			if (null == accName || "".equals(accName)) {
				Content = "银行账户名不能为空！";
				System.out.println("银行账户名不能为空！");
				return false;
			}
			if (paymode == "4") {
				String tBankSQL = "SELECT * FROM ldbank WHERE bankcode='"
						+ bankCode + "' AND cansendflag='1'";
				SSRS tBankSQLSSRS = aExeSQL.execSQL(tBankSQL);
				if (tBankSQLSSRS.getMaxRow() <= 0) {
					paymode = "11";
					System.out.println("该银行不支持银行转账，给付方式修改为银行汇款");
				}
			}
		}
		if (returnMode == "3" && (null == Email || "".equals(Email))) {
			Content = "回执发送方式为电子邮件，电子邮箱地址不能为空！";
			System.out.println("回执发送方式为电子邮件，电子邮箱地址不能为空！");
			return false;
		}
		if (returnMode == "4" && (null == Email || "".equals(Email))) {
			Content = "回执发送方式为短信，手机号码不能为空！";
			System.out.println("回执发送方式为短信，手机号码不能为空！");
			return false;
		}
		if (!checkID1(custIdno)) {
			return false;
		}

		if (null == AppeanRCode || "".equals(AppeanRCode)) {
			Content = "申诉原因不能为空！";
			System.out.println("申诉原因不能为空！");
			return false;
		}
		// TODO:新增效验，理赔二核时不可申诉纠错
		String sqlUW = "select rgtstate from llcase where caseno='" + CaseNo
				+ "' with ur";
		SSRS sqlUWSSRS = aExeSQL.execSQL(sqlUW);
		if (!"".equals(sqlUWSSRS.getMaxRow())) {
			if (sqlUWSSRS.GetText(1, 1) == "16") {
				Content = "理赔二核中不可申诉纠错，必须等待二核结束！";
				System.out.println("理赔二核中不可申诉纠错，必须等待二核结束！");
				return false;
			}
		}
		//调用后台处理类
		if(!correctionCommit()){
			return false;
		}

		return true;
	}

	public boolean checkID1(String vIDNO) {
		if (null == vIDNO || "".equals(vIDNO) || null == custIdtype
				|| "".equals(custIdtype)) {
			Content = "证件号码和证件类型不能为空！";
			System.out.println("证件号码和证件类型不能为空！");
			return false;
		}
		if (null != CustomerNo && "" != CustomerNo) {
			String strSql = "select idtype,idno from lcinsured where insuredno='"
					+ CustomerNo
					+ "' and idno = '"
					+ vIDNO
					+ "' and idtype = '"
					+ custIdtype
					+ "' union all select idtype,idno from lbinsured where insuredno='"
					+ CustomerNo
					+ "' and idno = '"
					+ vIDNO
					+ "' and idtype = '" + custIdtype + "' with ur";
			ExeSQL aExeSQL = new ExeSQL();
			SSRS sqlSSRS = aExeSQL.execSQL(strSql);
			if (sqlSSRS.getMaxRow() > 0) {

			} else {
				Content = "录入的证件号码【" + vIDNO + "】或者证件类型【" + custIdtype + "】有误！";
				System.out.println("录入的证件号码【" + vIDNO + "】或者证件类型【" + custIdtype
						+ "】有误！");
				return false;
			}
		}
		return true;
	}

	/**
	 * 理赔案件--》理赔处理--》申诉/纠错受理【从报文赋值】 根据理赔号查询出其他信息sql
	 * 
	 * @return by hy 2017-01-06
	 */
	public boolean correctionQuery() {
		ExeSQL aExeSQL = new ExeSQL();
		CaseNo = mLLAppea.getrgtno();//"C3600170113000009";// 报文解析的时候就换过来了
		System.out.println("CaseNo==============="+CaseNo);
		String sqlRgtno = "select a.RgtNo,a.CustomerNo,a.CustomerName,(case a.CustomerSex when '0' then '男' when '1' then '女' when '2' then '不详' end),a.AccdentDesc,"
				+ "a.handler,b.codename,c.apppeoples from llcase a,ldcode b,llregister c where a.CaseNo='"
				+ CaseNo
				+ "' and c.rgtno=a.rgtno and b.codetype='llrgtstate' and b.code=a.rgtstate";
		SSRS sqlRgtnoSSRS = aExeSQL.execSQL(sqlRgtno);
		if(sqlRgtnoSSRS.getMaxNumber()<=0){
			Content="查询案件信息失败，请确认案件号是否正确";
			System.out.println("查询案件信息失败，请确认案件号是否正确");
			return false;
		}
		RgtNo = sqlRgtnoSSRS.GetText(1, 1);// 团体批次号
		System.out.println("RgtNo===============" + RgtNo);

		// 申诉信息
		AppealType = "1";// 申诉/错误类型【0-申诉类，1-纠错类】
		AppeanRCode = "1";// 申诉/错误原因【1-业务员原因，2-条款理解原因，3-核保处理原因，4-合同管理原因，5-健康管理原因，6-理赔处理原因，7-服务态度原因，8-社保政策原因，9-其他】
		String appealReasonSql = "select Code, CodeName from ldcode where 1 = 1 and codetype = 'llappeanreason' and code = '"
				+ AppeanRCode + "'";
		SSRS appealReasonSqlSSRS = aExeSQL.execSQL(appealReasonSql);
		AppealReason = appealReasonSqlSSRS.GetText(1, 2);
		AppealRDesc = "这条申诉信息是接口数据";// 备注

		if (RgtNo != null && RgtNo.length() > 0) {
			if (CaseNo != null && CaseNo.length() > 0) {
				String sqlCustomer = "select customerno,customername,customersex,custbirthday,"
						+ "(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llcase.IDType),"
						+ "idno,idtype,otheridno,otheridtype,prepaidflag from llcase where caseno='"
						+ CaseNo + "'";
				SSRS sqlCustomerSSRS = aExeSQL.execSQL(sqlCustomer);
				// 客户查询
//				CustomerName = "";// 客户姓名
				if (null == mLLAppea.getCustomerName() || "".equals(mLLAppea.getCustomerName())) {// 此处CustomerName为报文中传值，如果为空则取sqlCustomer中查询得到的值
					CustomerName = sqlCustomerSSRS.GetText(1, 2);
				} else {
					CustomerName = mLLAppea.getCustomerName();// 此处为报文中获取的值
				}
				System.out.println("CustomerName=============" + CustomerName);
//				CustomerNo = "";// 客户号码
				if (null == mLLAppea.getCustomerNo() || "".equals(mLLAppea.getCustomerNo())) {// 此处CustomerNo为报文中传值，如果为空则取sqlCustomer中查询得到的值
					CustomerNo = sqlCustomerSSRS.GetText(1, 1);
				} else {
					CustomerNo = mLLAppea.getCustomerNo();// 此处为报文中获取的值
				}
				System.out.println("CustomerNo=============" + CustomerNo);

//				custIdno = "";// 证件号码
				if (null == mLLAppea.gettIDNo() || "".equals(mLLAppea.gettIDNo())) {// 此处customerIdno为报文中传值，如果为空则取sqlCustomer中查询得到的值
					custIdno = sqlCustomerSSRS.GetText(1, 6);
				} else {
					custIdno = mLLAppea.gettIDNo();// 此处为报文中获取的值
				}
				System.out.println("customerIdno============" + custIdno);
				String sqlCustomerIdtype = "";
//				custIdtype = "";// 证件类型【0-身份证,1-护照,2-军官证,3-工作证,4-其它,5-户口本,6-出生证,7-回乡证/台胞证】
				if (null == mLLAppea.gettIDType() || "".equals(mLLAppea.gettIDType())) {// 此处customerIdtype为报文中传值
																	// ，如果为空则取sqlCustomer中查询得到的值
					custIdtype = sqlCustomerSSRS.GetText(1, 7);
				} else {
					custIdtype = mLLAppea.gettIDType();// 此处为报文中获取的值
				}
				System.out.println("customerIdtype============" + custIdtype);
//				custBirthday = "";// 出生日期
				if (null == mLLAppea.getCBirthday() || "".equals(mLLAppea.getCBirthday())) {// 此处custBirthday为报文中传值，如果为空则取sqlCustomer中查询得到的值
					custBirthday = sqlCustomerSSRS.GetText(1, 4);
				} else {
					custBirthday = mLLAppea.getCBirthday();// 此处为报文中获取的值
				}
				System.out.println("custBirthday==============" + custBirthday);
//				custSex = "";// 性别不需要报文传值
				custSex = sqlCustomerSSRS.GetText(1, 3);
				System.out.println("custSex=============" + custSex);
//				OtherIDNo = "";// 社保号码
				if (null == mLLAppea.getOtherIDNo() || "".equals(mLLAppea.getOtherIDNo())) {// 此处OtherIDNo为报文中传值，如果为空则取sqlCustomer中查询得到的值
					OtherIDNo = sqlCustomerSSRS.GetText(1, 8);
				} else {
					OtherIDNo = mLLAppea.getOtherIDNo();// 此处为报文中获取的值
				}
				System.out.println("OtherIDNo============" + OtherIDNo);
//				PrePaidFlag = "";// 1为回销预付赔款
				if (null == mLLAppea.getPrePaidFlag() || "".equals(mLLAppea.getPrePaidFlag())) {// 此处PrePaidFlag为报文中传值，如果为空则取sqlCustomer中查询得到的值
					if("1".equals(sqlCustomerSSRS.GetText(1, 10))){
						PrePaidFlag="1";
					}else{
						PrePaidFlag="";
					}
				} else {
					PrePaidFlag = mLLAppea.getPrePaidFlag();// 此处为报文中获取的值
				}
				phone = mLLAppea.getMobilePhone();// 手机号码——》没找到报文中获取

				// 申请原因
				CustStatus = mLLAppea.getCustStatus();// 事故者现状报文中获取【01-治疗前，02-门诊，03-住院，04-出院痊愈，05-出院好转，06-出院无效，07-死亡，09-其他】
				System.out.println("CustStatus==================" + CustStatus);
				DeathDate = mLLAppea.getDeathDate();// 死亡日期报文中获取
				System.out.println("DeathDate====================" + DeathDate);
				appReasonCode = mLLAppea.getappReasonCode();// 申请原因编码【1-门诊费用，2-住院费用，3-医疗津贴，4-重大疾病，5-身故，6-护理，7-失能，8-伤残，9-特需，10-门诊大额】
				System.out.println("appReasonCode=================="
						+ appReasonCode);
				/* 接到的报文是1,2,3 报文传值就直接用报文的值，报文不传值就用查询得到的结果 */
				String sqlAppReasonCode = "select ReasonCode,Reason from LLAppClaimReason where 1=1 "
						+ "and rgtno='"
						+ RgtNo
						+ "' "
						+ " and caseno='"
						+ CaseNo + "'";
				SSRS sqlAppReasonCodeSSRS = aExeSQL.execSQL(sqlAppReasonCode);
				if (null == appReasonCode || "".equals(appReasonCode)) {
					for (int i = 1; i < sqlAppReasonCodeSSRS.getMaxRow(); i++) {
						appReasonCode += sqlAppReasonCodeSSRS.GetText(i, 1)
								+ ",";
					}
					appReasonCode += sqlAppReasonCodeSSRS.GetText(
							sqlAppReasonCodeSSRS.getMaxRow(), 1);
				} else {
					appReasonCode = mLLAppea.getappReasonCode();// 此处为报文中传的值
				}
				System.out.println("appReasonCode==============="
						+ appReasonCode);
				// 客户事件信息

				// 如果报文中没有传入事件信息 用默认查出的事件信息
				if (tEventSet.size() <= 0) {
					// 判断报文中的 客户事件信息 的节点集合是否为空 为空用下面的处理
					String strSQL = "select SubRptNo,AccDate,AccPlace,AccidentType,AccSubject,HospitalName,InHospitalDate,OutHospitalDate,AccDesc,"
							+ "case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end,"
							+ "AccidentType from LLSubReport where CustomerNo='"
							+ CustomerNo + "' AND SubRptNo IN  (select subrptno  from llcaserela where CaseNo='"+CaseNo+"') order by AccDate desc";
					SSRS strSQLSSRS = aExeSQL.execSQL(strSQL);
					if (strSQLSSRS.getMaxNumber() > 0) {
						for (int i = 1; i <= strSQLSSRS.getMaxRow(); i++) {
							LLSubReportSchema tEvent2Schema = new LLSubReportSchema();
							tEvent2Schema.setSubRptNo(strSQLSSRS.GetText(i, 1));
							tEvent2Schema.setAccDate(strSQLSSRS.GetText(i, 2));
							tEvent2Schema.setAccPlace(strSQLSSRS.GetText(i, 3));
							tEvent2Schema.setInHospitalDate(strSQLSSRS.GetText(
									i, 7));
							tEvent2Schema.setOutHospitalDate(strSQLSSRS
									.GetText(i, 8));
							tEvent2Schema.setAccDesc(strSQLSSRS.GetText(i, 9));
							tEvent2Schema.setAccidentType(strSQLSSRS.GetText(i,
									11));
							tEvent2Set.add(tEvent2Schema);
						}
					}
				}

			}
			
			String sqlInformation = "select "
					+ "(select codename from ldcode where ldcode.codetype='llaskmode' and ldcode.code=llregister.RgtType),"
					+ "AppAmnt,"
					+ "(select codename from ldcode where ldcode.codetype='llreturnmode' and ldcode.code=llregister.ReturnMode),"
					+ "RgtantName,"
					+ "(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llregister.IDType),"
					+ "IDNo,"
					+ "(select codename from ldcode where ldcode.codetype='llrelation' and ldcode.code=llregister.relation),"
					+ "RgtantPhone,RgtantAddress,PostCode,"
					+ "(select codename from ldcode where ldcode.codetype='llgetmode' and ldcode.code=llregister.getmode), "
					+ "IDType,bankcode,bankaccno,accname,RgtType,ReturnMode,relation,getmode, "
					+ "(select bankname from ldbank where ldbank.bankcode=llregister.bankcode), "
					+ "RgtantMobile,email " + "from LLRegister where rgtno='"
					+ RgtNo + "'";
			SSRS sqlInformationSSRS = aExeSQL.execSQL(sqlInformation);

			// 个人申请信息
			relation = "00";// 申请人与被保人关系【00-本人,01-配偶,02-子女,03-父母,04-亲属,05-其他】
			if (null == mLLAppea.getRelation() || "".equals(mLLAppea.getRelation())) {// 此处relation为报文中传值，如果为空则取sqlInformation中查询得到的值
				relation = sqlInformationSSRS.GetText(1, 18);
			} else {
				relation = mLLAppea.getRelation();// 此处为报文中获取的值
			}
			System.out.println("relation==========" + relation);
			if(!"00".equals(relation)){
//				rgtantName = "";// 申请人姓名
				if (null == mLLAppea.getRgtantName() || "".equals(mLLAppea.getRgtantName())) {// 此处rgtantName为报文中传值，如果为空则取sqlInformation中查询得到的值
					rgtantName = sqlInformationSSRS.GetText(1, 4);
				} else {
					rgtantName = mLLAppea.getRgtantName();// 此处为报文中获取的值
				}
				System.out.println("rgtantName===========" + rgtantName);
//				idno = "";// 申请人证件号码
				if (null == mLLAppea.getIDNo() || "".equals(mLLAppea.getIDNo())) {// 此处idno为报文中传值，如果为空则取sqlInformation中查询得到的值
					idno = sqlInformationSSRS.GetText(1, 6);
				} else {
					idno = mLLAppea.getIDNo();// 此处为报文中获取的值
				}
				System.out.println("idno=====" + idno);
//				idtype = "";// 申请人证件类型【0-身份证,1-护照,2-军官证,3-工作证,4-其它,5-户口本,6-出生证,7-回乡证/台胞证】
				if (null == mLLAppea.getIDType() || "".equals(mLLAppea.getIDType())) {// 此处idtype为报文中传值，如果为空则取sqlInformation中查询得到的值
					idtype = sqlInformationSSRS.GetText(1, 12);
				} else {
					idtype = mLLAppea.getIDType();// 此处为报文中获取的值
				}
				System.out.println("idtype============" + idtype);
//				rgtantphone = "";// 申请人电话
				if (null == mLLAppea.getRgtantPhone() || "".equals(mLLAppea.getRgtantPhone())) {// 此处rgtantphone为报文中传值，如果为空则取sqlInformation中查询得到的值
					rgtantphone = sqlInformationSSRS.GetText(1, 8);
				} else {
					rgtantphone = mLLAppea.getRgtantPhone();// 此处为报文中获取的值
				}
				System.out.println("rgtantphone==========" + rgtantphone);
//				rgtantMobile = "";// 申请人手机
				if (null == mLLAppea.getMobile() || "".equals(mLLAppea.getMobile())) {// 此处rgtantMobile为报文中传值，如果为空则取sqlInformation中查询得到的值
					rgtantMobile = sqlInformationSSRS.GetText(1, 21);
				} else {
					rgtantMobile = mLLAppea.getMobile();// 此处为报文中获取的值
				}
				System.out.println("rgtantmMbile===========" + rgtantMobile);
//				Email = "";// 申请人电子邮箱
				if (null == mLLAppea.getEmail() || "".equals(mLLAppea.getEmail())) {// 此处Email为报文中传值，如果为空则取sqlInformation中查询得到的值
					Email = sqlInformationSSRS.GetText(1, 22);
				} else {
					Email = mLLAppea.getEmail();// 此处为报文中获取的值
				}
				System.out.println("Email=========" + Email);
//				rgtantAddress = "";// 申请人地址
				if (null == mLLAppea.getRgtantAddress() || "".equals(mLLAppea.getRgtantAddress())) {// 此处rgtantAddress为报文中传值，如果为空则取sqlInformation中查询得到的值
					rgtantAddress = sqlInformationSSRS.GetText(1, 9);
				} else {
					rgtantAddress = mLLAppea.getRgtantAddress();// 此处为报文中获取的值
				}
				System.out.println("rgtantAddress===========" + rgtantAddress);
//				postCode = "";// 邮政编码
				if (null == mLLAppea.getPostCode() || "".equals(mLLAppea.getPostCode())) {// 此处postCode为报文中传值，如果为空则取sqlInformation中查询得到的值
					postCode = sqlInformationSSRS.GetText(1, 10);
				} else {
					postCode = mLLAppea.getPostCode();// 此处为报文中获取的值
				}
				System.out.println("postCode===========" + postCode);
				
			}else{
//				rgtantName = "";// 申请人姓名
				if (null == mLLAppea.getRgtantName() || "".equals(mLLAppea.getRgtantName())) {// 此处rgtantName为报文中传值，如果为空则取sqlInformation中查询得到的值
					rgtantName = CustomerName;
				} else {
					rgtantName = mLLAppea.getRgtantName();// 此处为报文中获取的值
				}
				System.out.println("rgtantName===========" + rgtantName);
//				idno = "";// 申请人证件号码
				if (null == mLLAppea.getIDNo() || "".equals(mLLAppea.getIDNo())) {// 此处idno为报文中传值，如果为空则取sqlInformation中查询得到的值
					idno = CustomerNo;
				} else {
					idno = mLLAppea.getIDNo();// 此处为报文中获取的值
				}
				System.out.println("idno=====" + idno);
//				idtype = "";// 申请人证件类型【0-身份证,1-护照,2-军官证,3-工作证,4-其它,5-户口本,6-出生证,7-回乡证/台胞证】
				if (null == mLLAppea.getIDType() || "".equals(mLLAppea.getIDType())) {// 此处idtype为报文中传值，如果为空则取sqlInformation中查询得到的值
					idtype = custIdtype;
				} else {
					idtype = mLLAppea.getIDType();// 此处为报文中获取的值
				}
				System.out.println("idtype============" + idtype);
				String sqlRelation = "select b.PostalAddress,b.zipcode,b.phone,b.mobile,b.Email from lcinsured a,lcaddress b where b.AddressNo=a.AddressNo and b.CustomerNo=a.InsuredNo and a.InsuredNo='"+CustomerNo+"'";
				SSRS sqlRelationSSRS = aExeSQL.execSQL(sqlRelation);
				if(sqlRelationSSRS.getMaxRow()>0){
//					rgtantphone = "";// 申请人电话
					if (null == mLLAppea.getRgtantPhone() || "".equals(mLLAppea.getRgtantPhone())) {// 此处rgtantphone为报文中传值，如果为空则取sqlRelationSSRS中查询得到的值
						rgtantphone = sqlRelationSSRS.GetText(1, 3);
					} else {
						rgtantphone = mLLAppea.getRgtantPhone();// 此处为报文中获取的值
					}
					System.out.println("rgtantphone==========" + rgtantphone);
//					rgtantMobile = "";// 申请人手机
					if (null == mLLAppea.getMobile() || "".equals(mLLAppea.getMobile())) {// 此处rgtantMobile为报文中传值，如果为空则取sqlRelationSSRS中查询得到的值
						rgtantMobile = sqlRelationSSRS.GetText(1, 4);
					} else {
						rgtantMobile = mLLAppea.getMobile();// 此处为报文中获取的值
					}
					System.out.println("rgtantmMbile===========" + rgtantMobile);
//					Email = "";// 申请人电子邮箱
					if (null == mLLAppea.getEmail() || "".equals(mLLAppea.getEmail())) {// 此处Email为报文中传值，如果为空则取sqlRelationSSRS中查询得到的值
						Email = sqlRelationSSRS.GetText(1, 5);
					} else {
						Email = mLLAppea.getEmail();// 此处为报文中获取的值
					}
					System.out.println("Email=========" + Email);
//					rgtantAddress = "";// 申请人地址
					if (null == mLLAppea.getRgtantAddress() || "".equals(mLLAppea.getRgtantAddress())) {// 此处rgtantAddress为报文中传值，如果为空则取sqlRelationSSRS中查询得到的值
						rgtantAddress = sqlRelationSSRS.GetText(1, 1);
					} else {
						rgtantAddress = mLLAppea.getRgtantAddress();// 此处为报文中获取的值
					}
					System.out.println("rgtantAddress===========" + rgtantAddress);
//					postCode = "";// 邮政编码
					if (null == mLLAppea.getPostCode() || "".equals(mLLAppea.getPostCode())) {// 此处postCode为报文中传值，如果为空则取sqlRelationSSRS中查询得到的值
						postCode = sqlRelationSSRS.GetText(1, 2);
					} else {
						postCode = mLLAppea.getPostCode();// 此处为报文中获取的值
					}
					System.out.println("postCode===========" + postCode);
				}else{
//					rgtantphone = "";// 申请人电话
					if (null == mLLAppea.getRgtantPhone() || "".equals(mLLAppea.getRgtantPhone())) {// 此处rgtantphone为报文中传值，如果为空则取sqlInformation中查询得到的值
						rgtantphone = sqlInformationSSRS.GetText(1, 8);
					} else {
						rgtantphone = mLLAppea.getRgtantPhone();// 此处为报文中获取的值
					}
					System.out.println("rgtantphone==========" + rgtantphone);
//					rgtantMobile = "";// 申请人手机
					if (null == mLLAppea.getMobile() || "".equals(mLLAppea.getMobile())) {// 此处rgtantMobile为报文中传值，如果为空则取sqlInformation中查询得到的值
						rgtantMobile = sqlInformationSSRS.GetText(1, 21);
					} else {
						rgtantMobile = mLLAppea.getMobile();// 此处为报文中获取的值
					}
					System.out.println("rgtantmMbile===========" + rgtantMobile);
//					Email = "";// 申请人电子邮箱
					if (null == mLLAppea.getEmail() || "".equals(mLLAppea.getEmail())) {// 此处Email为报文中传值，如果为空则取sqlInformation中查询得到的值
						Email = sqlInformationSSRS.GetText(1, 22);
					} else {
						Email = mLLAppea.getEmail();// 此处为报文中获取的值
					}
					System.out.println("Email=========" + Email);
//					rgtantAddress = "";// 申请人地址
					if (null == mLLAppea.getRgtantAddress() || "".equals(mLLAppea.getRgtantAddress())) {// 此处rgtantAddress为报文中传值，如果为空则取sqlInformation中查询得到的值
						rgtantAddress = sqlInformationSSRS.GetText(1, 9);
					} else {
						rgtantAddress = mLLAppea.getRgtantAddress();// 此处为报文中获取的值
					}
					System.out.println("rgtantAddress===========" + rgtantAddress);
//					postCode = "";// 邮政编码
					if (null == mLLAppea.getPostCode() || "".equals(mLLAppea.getPostCode())) {// 此处postCode为报文中传值，如果为空则取sqlInformation中查询得到的值
						postCode = sqlInformationSSRS.GetText(1, 10);
					} else {
						postCode = mLLAppea.getPostCode();// 此处为报文中获取的值
					}
					System.out.println("postCode===========" + postCode);
				}
			}
			RgtType = "";// 受理方式——》此栏内容可以由sql查询得到值【1-电话，2-电子邮件，3-传真，4-上门，5-信函，6-医保通，7-意外险平台，8-社保平台，9-其他】
			if (null == mLLAppea.getRgtType() || "".equals(mLLAppea.getRgtType())) {// 此处RgtType为报文中传值，如果为空则取sqlInformation中查询得到的值
				RgtType = sqlInformationSSRS.GetText(1, 16);
			} else {
				RgtType = mLLAppea.getRgtType();// 此处为报文中获取的值
			}
			System.out.println("RgtType=============" + RgtType);
//			returnMode = "";// 回执发送方式【1-上门，2-信函，3-电子邮件，4-短信，5-电话，6-传真，9-其他】
			if (null == mLLAppea.getReturnMode() || "".equals(mLLAppea.getReturnMode())) {// 此处returnMode为报文中传值，如果为空则取sqlInformation中查询得到的值
				returnMode = sqlInformationSSRS.GetText(1, 17);
			} else {
				returnMode = mLLAppea.getReturnMode();// 此处为报文中获取的值
			}
			System.out.println("returnMode==========" + returnMode);
			paymode = "1";// 受益金领取方式【1-现金，11-银行汇款，2-现金支票，3-转账支票，4-银行转账】
			if (null == mLLAppea.getpaymode() || "".equals(mLLAppea.getpaymode())) {// 此处paymodeName为报文中传值，如果为空则取sqlInformation中查询得到的值
				paymode = sqlInformationSSRS.GetText(1, 19);
				paymodeName = sqlInformationSSRS.GetText(1, 11);
				System.out.println("paymode===========" + paymode);
				System.out.println("paymodeName===========" + paymodeName);
				if (paymode.equals("11") || paymode.equals("3")
						|| paymode.equals("4")) {
//					bankCode = "";// 银行编码
					if (null == mLLAppea.getBankCode() || "".equals(mLLAppea.getBankCode())) {// 此处bankCode为报文中传值，如果为空则取sqlInformation中查询得到的值
						bankCode = sqlInformationSSRS.GetText(1, 13);
					} else {
						bankCode = mLLAppea.getBankCode();// 此处为报文中获取的值
					}
					System.out.println("BankCode=============" + bankCode);
//					bankName = "";// 签约银行
					if (null == mLLAppea.getSendFlag() || "".equals(mLLAppea.getSendFlag())) {// 此处bankName为报文中传值，如果为空则取sqlInformation中查询得到的值
						bankName = sqlInformationSSRS.GetText(1, 20);
					} else {
						bankName = mLLAppea.getSendFlag();// 此处为报文中获取的值
					}
					System.out.println("bankName==========" + bankName);
//					bankAccNo = "";// 银行账号
					if (null == mLLAppea.getBankAccNo() || "".equals(mLLAppea.getBankAccNo())) {// 此处bankAccNo为报文中传值，如果为空则取sqlInformation中查询得到的值
						bankAccNo = sqlInformationSSRS.GetText(1, 14);
					} else {
						bankAccNo = mLLAppea.getBankAccNo();// 此处为报文中获取的值
					}
					System.out.println("bankAccNo===========" + bankAccNo);
//					accName = "";// 银行账户名
					if (null == mLLAppea.getAccName() || "".equals(mLLAppea.getAccName())) {// 此处accName为报文中传值，如果为空则取sqlInformation中查询得到的值
						accName = sqlInformationSSRS.GetText(1, 15);
					} else {
						accName = mLLAppea.getAccName();// 此处为报文中获取的值
					}
					System.out.println("accName===========" + accName);
				} else {
//					paymode = "";// 此处为报文中获取的值
					System.out.println("paymodeName===========" + paymode);
					if (paymode.equals("11") || paymode.equals("3")
							|| paymode.equals("4")) {
//						bankCode = "";// 银行编码
						if (null == mLLAppea.getBankCode() || "".equals(mLLAppea.getBankCode())) {// 此处bankCode为报文中传值，如果为空则取sqlInformation中查询得到的值
							bankCode = sqlInformationSSRS.GetText(1, 13);
						} else {
							bankCode = mLLAppea.getBankCode();// 此处为报文中获取的值
						}
						System.out.println("BankCode=============" + bankCode);
//						bankName = "";// 签约银行
						if (null == mLLAppea.getSendFlag() || "".equals(mLLAppea.getSendFlag())) {// 此处bankName为报文中传值，如果为空则取sqlInformation中查询得到的值
							bankName = sqlInformationSSRS.GetText(1, 20);
						} else {
							bankName = mLLAppea.getSendFlag();// 此处为报文中获取的值
						}
						System.out.println("bankName==========" + bankName);
//						bankAccNo = "";// 银行账号
						if (null == mLLAppea.getBankAccNo() || "".equals(mLLAppea.getBankAccNo())) {// 此处bankAccNo为报文中传值，如果为空则取sqlInformation中查询得到的值
							bankAccNo = sqlInformationSSRS.GetText(1, 14);
						} else {
							bankAccNo = mLLAppea.getBankAccNo();// 此处为报文中获取的值
						}
						System.out.println("bankAccNo===========" + bankAccNo);
//						accName = "";// 银行账户名
						if (null == mLLAppea.getAccName() || "".equals(mLLAppea.getAccName())) {// 此处accName为报文中传值，如果为空则取sqlInformation中查询得到的值
							accName = sqlInformationSSRS.GetText(1, 15);
						} else {
							accName = mLLAppea.getAccName();// 此处为报文中获取的值
						}
						System.out.println("accName===========" + accName);
					}
				}
			}

			

		}
		// 前台校验

		if (!correctionCheck()) {
			return false;
		}

		return true;
	}

	/**
	 * 理赔案件--》理赔处理--》申诉/纠错受理 【向后台提交】
	 * 
	 * @return by hy 2017-01-11
	 */
	public boolean correctionCommit() {
		
		
		String strOperate = "APPEALINSERT||MAIN";// 区分是纠错还是申诉的标记
		System.out.println("==== strOperate == " + strOperate);
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LLAppealSchema tLLAppealSchema = new LLAppealSchema();
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		LLSubReportSet tLLSubReportSet = new LLSubReportSet();
		LLAppClaimReasonSet tLLAppClaimReasonSet = new LLAppClaimReasonSet();
		LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
		VData tVData = new VData();
		ClientRegisterBackBL tClientRegisterBackBL = new ClientRegisterBackBL();

		// 输出参数
		CErrors tError = null;
		String tRela = "";
		String FlagStr = "";
		
		String EventFlag = ""; // 事件申请标记 为1 有客户存在事件，为0不存在事件
		GlobalInput tG = new GlobalInput();
//		tG.Operator = "cm0002";
//		tG.Operator = "sc3672";
//		tG.ManageCom = "86360500";
		tG.Operator = "lipeia";
		tG.ManageCom = mLLAppea.getManageCom();
		
		/* 将申请信息填充 */
		System.out.println("<--submit LLRegisterSchema-->");

		if (strOperate.equals("APPEALINSERT||MAIN")) {
			tLLRegisterSchema.setRgtState("01");// 案件状态
			tLLRegisterSchema.setRgtObj("2"); // 号码类型 0总单 1分单 2个单 3客户
			tLLRegisterSchema.setRgtObjNo(CustomerNo);
			System.out.println("RgtObjNo:" + tLLRegisterSchema.getRgtObjNo());
			tLLRegisterSchema.setRgtClass("0");
			tLLRegisterSchema.setCustomerNo(CustomerNo);
			tLLRegisterSchema.setRgtType(RgtType); // 受理方式
			tLLRegisterSchema.setRgtantName(rgtantName); // 申请人姓名
			tLLRegisterSchema.setRelation(relation); // 与被保险人关系
			tLLRegisterSchema.setRgtantAddress(rgtantAddress); // 申请人地址
			tLLRegisterSchema.setRgtantPhone(rgtantphone); // 申请人电话
			tLLRegisterSchema.setPostCode(postCode); // 邮编
			String tAppAmnt = appAmnt;
			if ("".equals(tAppAmnt)|| null==tAppAmnt) {
				tAppAmnt = "0";
			}
			tLLRegisterSchema.setAppAmnt(tAppAmnt); // 预估申请金额
			tLLRegisterSchema.setGetMode(paymode);
			tLLRegisterSchema.setBankCode(bankCode);
			tLLRegisterSchema.setBankAccNo(bankAccNo);
			tLLRegisterSchema.setAccName(accName);
			tLLRegisterSchema.setReturnMode(returnMode); // 回执发送方式
			tLLRegisterSchema.setIDType(idtype); // 申请人证件类型
			tLLRegisterSchema.setIDNo(idno); // 申请人证件号码
			tLLRegisterSchema.setRgtObj("1"); // 个人客户

			// 申请原因
			String tNum[] = appReasonCode.split(",");
			String appResonName[] = { "门诊费用", "住院费用", "医疗津贴", "重大疾病", "身 故",
					"护 理", "失能", "伤残" };

			int AppReasonNum = 0;
			if (tNum != null) {
				AppReasonNum = tNum.length;
			}
			for (int i = 0; i < AppReasonNum; i++) {
				LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
				tLLAppClaimReasonSchema.setReasonCode(tNum[i]); // 原因代码
				tLLAppClaimReasonSchema.setCustomerNo(CustomerNo);
				tLLAppClaimReasonSchema.setReasonType("0");

				tLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);
			}

			if (AppealType.equals("0")) {
				tLLCaseSchema.setRgtType("4");
				System.out.println("申诉");
			} else
				tLLCaseSchema.setRgtType("5");
			tLLCaseSchema.setCustomerName(CustomerName);
			tLLCaseSchema.setCustomerNo(CustomerNo);
			tLLCaseSchema.setPostalAddress(rgtantAddress);
			tLLCaseSchema.setPhone(rgtantphone);
			tLLCaseSchema.setDeathDate(DeathDate);
			tLLCaseSchema.setCustBirthday(custBirthday);
			tLLCaseSchema.setCustomerSex(custSex);
			tLLCaseSchema.setIDType(custIdtype);
			tLLCaseSchema.setIDNo(custIdno);
			tLLCaseSchema.setCaseGetMode(paymode);
			tLLCaseSchema.setBankCode(bankCode);
			tLLCaseSchema.setBankAccNo(bankAccNo);
			tLLCaseSchema.setAccName(accName);
			tLLCaseSchema.setCustState(CustStatus);
			tLLCaseSchema.setCaseNo(CaseNo);
			if (!"".equals(PrePaidFlag) && PrePaidFlag != null) {
				tLLCaseSchema.setPrePaidFlag("1"); // 0或null 不使用预付回销 1-预付回销
			} else {
				tLLCaseSchema.setPrePaidFlag(""); // 个案标记llcase 批次案件标记llregister
			}
			tLLAppealSchema.setCaseNo(CaseNo);
			tLLCaseSchema.setReceiptFlag("1");
			System.out.println(CaseNo);
			tLLAppealSchema.setAppealType(AppealType);
			tLLAppealSchema.setAppeanRCode(AppeanRCode);
			tLLAppealSchema.setAppealReason(AppealReason);
			tLLAppealSchema.setAppealRDesc(AppealRDesc);

			System.out.println("<--submit mulline-->");
			// 事件信息
			if (tEventSet.size() > 0) {
				for (int i = 1; i <= tEventSet.size(); i++) {
					LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
					tLLSubReportSchema.setSubRptNo("");
					tLLSubReportSchema.setCustomerNo(CustomerNo);
					tLLSubReportSchema.setCustomerName(CustomerName);
					tLLSubReportSchema.setAccDate(tEventSet.get(i).getAccDate());
					tLLSubReportSchema.setAccDesc(tEventSet.get(i).getAccDesc());
					tLLSubReportSchema.setAccPlace(tEventSet.get(i).getAccPlace());
					tLLSubReportSchema.setInHospitalDate(tEventSet.get(i).getInHospitalDate());
					tLLSubReportSchema.setOutHospitalDate(tEventSet.get(i).getOutHospitalDate());
					tLLSubReportSchema.setAccidentType(tEventSet.get(i).getAccidentType());
					tLLSubReportSet.add(tLLSubReportSchema);
				}
			}else{
				for(int i=1;i<=tEvent2Set.size();i++ ){
		  			LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
			        tLLSubReportSchema.setSubRptNo(tEvent2Set.get(i).getSubRptNo());
				    tLLSubReportSchema.setCustomerNo(CustomerNo);
				    tLLSubReportSchema.setCustomerName(CustomerName);      
				    tLLSubReportSchema.setAccDate(tEvent2Set.get(i).getAccDate());
				    tLLSubReportSchema.setAccDesc(tEvent2Set.get(i).getAccDesc());   
				    tLLSubReportSchema.setAccPlace(tEvent2Set.get(i).getAccPlace());  
				    tLLSubReportSchema.setInHospitalDate( tEvent2Set.get(i).getInHospitalDate());
				    tLLSubReportSchema.setOutHospitalDate( tEvent2Set.get(i).getOutHospitalDate()); 
				    tLLSubReportSchema.setAccidentType(tEvent2Set.get(i).getAccidentType());
					tLLSubReportSet.add(tLLSubReportSchema);
			 }
			}
			tLLCaseOpTimeSchema.setRgtState("01");
			String OpStartDate = PubFun.getCurrentDate();
			String OpStartTime = PubFun.getCurrentTime();
			tLLCaseOpTimeSchema.setStartDate(OpStartDate);
			tLLCaseOpTimeSchema.setStartTime(OpStartTime);
			try {
				// 准备传输数据 VData
				// VData传送
				System.out.println("<--Star Submit VData-->");
				tVData.add(tLLAppClaimReasonSet);
				tVData.add(tLLCaseSchema);
				tVData.add(tLLAppealSchema);
				tVData.add(tLLRegisterSchema);
				tVData.add(tLLSubReportSet);
				tVData.add(tLLCaseOpTimeSchema);
				tVData.add(tG);
				System.out.println("<--Into tClientRegisterBackBL-->");
				tClientRegisterBackBL.submitData(tVData, strOperate);
			} catch (Exception ex) {
				Content = "保存失败，原因是: " + ex.toString();
				FlagStr = "Fail";
				return false;
			}
			// 如果在Catch中发现异常，则不从错误类中提取错误信息
			if ("".equals(FlagStr)) {
				tError = tClientRegisterBackBL.mErrors;
				if (!tError.needDealError()) {
					Content = " 保存成功! ";
					FlagStr = "Success";
					tVData.clear();
					tVData = tClientRegisterBackBL.getResult();
				} else {
					Content = " 保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return false;
				}
			}

			// 显示信息

			System.out.println("AferSubmit");
			LLCaseSchema mLLCaseSchema = new LLCaseSchema();

			//System.out.println("tVData" + tVData);

			mLLCaseSchema.setSchema((LLCaseSchema) tVData
					.getObjectByObjectName("LLCaseSchema", 0));

			System.out.println("================================");
			System.out.println("CaseNo" + mLLCaseSchema.getCaseNo());
			System.out.println("RgtNo" + mLLCaseSchema.getRgtNo());
			System.out.println("================================");

			LLFirstDutyFilterBL tFirstDutyFilterBL = new LLFirstDutyFilterBL();
			TransferData CaseNo = new TransferData();
			CaseNo.setNameAndValue("CaseNo", mLLCaseSchema.getCaseNo());
			VData tVData1 = new VData();
			try {
				tVData1.add(CaseNo);
				tVData1.add(tG);
				System.out.println("tG" + mLLCaseSchema.getCaseNo());
				System.out.println("=============================本宝宝");
				tFirstDutyFilterBL.submitData(tVData1, "INSERT");
			} catch (Exception ex) {
				Content = "保存失败，原因是: " + ex.toString();
				FlagStr = "Fail";
			}
		}
		return true;
	}
	public boolean appMaterAffixSave(){
		return true;
	}
}
