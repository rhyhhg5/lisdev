package com.claimback.appealCorrection;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.preservation.util.StringUtil;
import com.sinosoft.lis.llcase.ClaimUnderwriteUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimUWMainSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
  * @ClassName: ClaimSPSDBL
  * @Description: 审批审定
  * @author liuzehong
  * @date 2019年1月14日 上午9:54:58
  **/
public class ClaimSPSDBL {
	
	static final String FORMAT_DATE = "yyyy-MM-dd";

	static final String FORMAT_TIME = "HH:mm:ss";

	private String SuccessFlag = "0";
	
	private SimpleDateFormat simDate = new SimpleDateFormat(FORMAT_DATE);

	private SimpleDateFormat simTime = new SimpleDateFormat(FORMAT_TIME);
	
	private static Log log = LogFactory.getLog(ClaimSPSDBL.class);
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors;

	private ExeSQL mExeSQL;
	
	private GlobalInput tG;
	
	private LLAppealAcceptSchema mLLAppealAcceptSchema;

	private String tCaseNo="";

	private String tSendOperator="";

	private String responseXml="";

	public ClaimSPSDBL() {
		this.mErrors = new CErrors();
		this.mExeSQL = new ExeSQL();
		this.mLLAppealAcceptSchema=new LLAppealAcceptSchema();
		this.tG=new  GlobalInput();
	}

	public CErrors getmErrors() {
		return mErrors;
	}
	
	public String getResponseXml() {
		return responseXml;
	}

	public boolean parseXml(String strXml) {
		if (!StringUtil.isNull(strXml)) {
			mErrors.addOneError("请求报文不能为空");
			return false;
		}
		Document doc;
		try {
			doc = DocumentHelper.parseText(strXml);
			Element root = doc.getRootElement();
			// 获取报文体
			Element tBody = root.element("Body");
			Element tClaim_SPSD = tBody.element("Claim_SPSD");
			String tManageCom = tClaim_SPSD.elementText("ManageCom");
			if (!StringUtil.isNull(tManageCom)) {
				mErrors.addOneError("机构编码不能为空");
				return false;
			}
			System.out.println();
			this. tCaseNo = tClaim_SPSD.elementText("CaseNo");
			if (!StringUtil.isNull(tCaseNo)) {
				mErrors.addOneError("案件号不能为空");
				return false;
			}
			String tCheckDecision = tClaim_SPSD.elementText("CheckDecision");
			String tRemark1 = tClaim_SPSD.elementText("Remark1");
			String tRemark2 = tClaim_SPSD.elementText("Remark2");
			
			mLLAppealAcceptSchema.setRemark1(tRemark1);
			mLLAppealAcceptSchema.setRemark1(tRemark2);
			mLLAppealAcceptSchema.setcheckDecision2(tCheckDecision);;
			
			tG.ManageCom=tManageCom;
			if (!dealData()) {
				return false;
			}
			SuccessFlag="1";
		}catch (Exception e) {
			mErrors.addOneError("解析报文出错："+e.toString());
			return false;
		}finally {
			rerutnXml();	
		}
		return true;
	}

	/**
	 * @return
	 */
	private boolean dealData() {
		log.info("。。。。。。。。。。。。。审批审定业务处理。。。。。。。。。。。。。。。。");
		String remark1 = mLLAppealAcceptSchema.getRemark1();
		String remark2 = mLLAppealAcceptSchema.getRemark2();
		String tcheckDecision = mLLAppealAcceptSchema.getcheckDecision2();
		return claim_SPSD(tCaseNo, remark1, remark2, tcheckDecision);
		
	}
	/**
	 * 审批审定(确认)
	 */
	public boolean claim_SPSD(String tCaseNo,String tRemark1,String tRemark2,String tcheckDecision ) {
		if (!StringUtil.isNull(tCaseNo)) {
			mErrors.addOneError("审批审定失败，原因：案件号不能为空");
			return false;
		}
		String rgtstateSql = "select rgtstate from llcase  where caseno='" + tCaseNo + "'";
		String rgtstate = mExeSQL.getOneValue(rgtstateSql);
		if (!StringUtil.isNull(rgtstate)) {
			mErrors.addOneError("查询案件状态失败，请检查案件号：" + tCaseNo);
			return false;
		}
		String operate = "";
		if ("04".equals(rgtstate) || "06".equals(rgtstate) || "10".equals(rgtstate) || "08".equals(rgtstate)) {// 需要复审
			if (!StringUtil.isNull(tRemark1)) {
				mErrors.addOneError("请输入审批意见");
				return false;
			}
			operate = "APPROVE|SP";
		} else if ("05".equals(rgtstate)) { // 审批状态
			if (!StringUtil.isNull(tcheckDecision)) {
				mErrors.addOneError("请输入审定结论");
				return false;
			}
			if ("2".equals(rgtstate)) { // 不同意审批结论
				if (!StringUtil.isNull(tRemark2)) {
					mErrors.addOneError("请输入审定意见");
					return false;
				}
			}
			operate = "APPROVE|SD";
		} else if ("16".equals(rgtstate)) {
			mErrors.addOneError("理赔二核中不可审批审定，必须等待二核结束！");
			return false;
		} else {
			mErrors.addOneError("案件在当前状态下不能做审批审定");
			return false;
		}
		String LowerSQL = "select 1 from LLCASEPROBLEM where 1=1 and caseno = '" + tCaseNo + "' and state='1' with ur";
		SSRS sqlSSRS = mExeSQL.execSQL(LowerSQL);
		if (sqlSSRS.getMaxRow() > 0) {
			mErrors.addOneError("问题件在未回复时，案件不能进行审批审定");
			return false;
		}
		String LLClaimDetailSql = "select RgtNo,CaseNo,ClmNo,grpcontno ,RealPay from LLClaimDetail where caseno='"
				+ tCaseNo + "'";
		SSRS LLClaimDetailSqlSSRS = mExeSQL.execSQL(LLClaimDetailSql);
		if (LLClaimDetailSqlSSRS.getMaxRow() < 1) {
			mErrors.addOneError("未获取到赔付信息，请检查案件号");
			return false;
		}

		CErrors tError = null;
		String FlagStr = "";
		String strRgtNo = LLClaimDetailSqlSSRS.GetText(1, 1); // 立案号
		String strCaseNo = LLClaimDetailSqlSSRS.GetText(1, 2); // 分案号
		String strClmNo = LLClaimDetailSqlSSRS.GetText(1, 3); // 赔案号
		String grpcontno = LLClaimDetailSqlSSRS.GetText(1, 4); // 团体保单号
		String bjSql = "select 1 from lpgrpedoritem where grpcontno='" + grpcontno + "' and edortype = 'BJ'";
		SSRS bjSSRS = mExeSQL.execSQL(bjSql);
		if (bjSSRS.getMaxRow() > 0) {
			mErrors.addOneError("该保单" + grpcontno + "正在进行结余返还操作或者已完成结余返还项目，不能理算确认!");
			return false;
		}
		String tAccSql = "select distinct accdate from llcaserela a,llsubreport b where a.subrptno=b.subrptno and a.caseno='"
				+ tCaseNo + "'";
		String tAccDate = mExeSQL.getOneValue(tAccSql);
		if (!StringUtil.isNull(tAccDate)) {
			mErrors.addOneError("出险日期查询失败");
			return false;
		}
		String strDecisionSP = "1"; // 审批结论暂时去掉，没有意义
		String contdealflagSql = "select contdealflag from llcase where caseno='"+tCaseNo+"'";
		String ContDealFlag = mExeSQL.getOneValue(contdealflagSql);
		if (!StringUtil.isNull(ContDealFlag)) {
			ContDealFlag = "0";
		}
		System.out.println("ContDealFlag=========" + ContDealFlag);
		if (!StringUtil.isNull(strRgtNo)) {
			mErrors.addOneError("查询立案号为空!");
			return false;
		}
		if (!StringUtil.isNull(strCaseNo)) {
			mErrors.addOneError("查询分案号为空!");
			return false;
		}
		if (!StringUtil.isNull(strClmNo)) {
			mErrors.addOneError("查询赔案号为空!");
			return false;
		}
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		tLLCaseSchema.setContDealFlag(ContDealFlag);

		LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();

		tLLClaimUWMainSchema.setRgtNo(strRgtNo);
		tLLClaimUWMainSchema.setCaseNo(strCaseNo);
		tLLClaimUWMainSchema.setClmNo(strClmNo);

		tLLClaimUWMainSchema.setcheckDecision1(strDecisionSP);
		tLLClaimUWMainSchema.setRemark1(tRemark1);
		tLLClaimUWMainSchema.setcheckDecision2(tcheckDecision);
		tLLClaimUWMainSchema.setRemark2(tRemark2);

		ClaimUnderwriteUI tClaimUnderwriteUI = new ClaimUnderwriteUI();
		VData tVData = new VData();
		tG.Operator = "lipeib";
		try {
			tVData.add(tG);
			tVData.add(tLLClaimUWMainSchema);
			tVData.add(tLLCaseSchema);
			tClaimUnderwriteUI.submitData(tVData, operate);
		} catch (Exception ex) {
			mErrors.addOneError("执行失败，原因是:" + ex.toString());
			FlagStr = "Fail";
			return false;
		}
		if (FlagStr.equals("")) {
			tError = tClaimUnderwriteUI.mErrors;
			if (tError.needDealError()) {
				mErrors.addOneError("执行失败，原因是:" + tError.getFirstError());
				FlagStr = "Fail";
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * 拼接返回报文
	 */
	@SuppressWarnings("unused")
	public void rerutnXml() {
		StringWriter stringWriter = new StringWriter();
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("MedicalAdjustment_Response");
		document.setRootElement(tDateSet);
		// 返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Date date = new Date();
		tSendDate.addText(simDate.format(date));
		Element tSendTime = tMsgResHead.addElement("SendTime");
		tSendTime.addText(simTime.format(date));
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		tSendOperator.addText(this.tSendOperator);
		Element tMsgType = tMsgResHead.addElement("MsgType");
		Element tRownum = tMsgResHead.addElement("Rownum");
		// 返回报文体样式
		Element tBody = tDateSet.addElement("Body");
		Element tManageCom = tBody.addElement("ManageCom");
		tManageCom.addText(this.tG.ManageCom);
		Element tCaseNo = tBody.addElement("CaseNo");
		tCaseNo .addText(this.tCaseNo);
		Element tContent = tBody.addElement("Content");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		if (!mErrors.needDealError()) {
			tContent.addText("保存成功");
		}else {
			tContent.addText(mErrors.getFirstError());
		}
		
		tSuccessFlag.addText(this.SuccessFlag);
		// 创建写文件方法
		XMLWriter xmlWriter = new XMLWriter(stringWriter);
		// 写入文件
		try {
			xmlWriter.write(document);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		// 关闭
		try {
			xmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		 this.responseXml = stringWriter.toString();
		 System.out.println(responseXml);
	}

}
