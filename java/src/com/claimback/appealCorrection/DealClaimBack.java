package com.claimback.appealCorrection;

import java.io.StringWriter;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.sinosoft.lis.llcase.RegisterBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDeclineSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class DealClaimBack {

	private String responseXml;
	LLAppealAcceptSchema mLLAppealAcceptSchema = new LLAppealAcceptSchema();
	private LLClaimDeclineSchema mLLClaimDeclineSchema = new LLClaimDeclineSchema();// 拒赔
	String Content = "";
	String caseNo = "";

	public String getResponseXml() {
		return responseXml;
	}
	public String getContent() {
		return Content;
	}
	public boolean submitDate(VData cInputData) {
		mLLAppealAcceptSchema = (LLAppealAcceptSchema) cInputData.getObjectByObjectName("LLAppealAcceptSchema", 0);
		String requestXml = "";
		try {
			// 获取请求报文
			requestXml = (String) cInputData.getObjectByObjectName("String", 0);
			// 理赔轨迹日志表
			VData vdata = new VData();
			MMap mmap = new MMap();
			String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,betweenness ,transactionType ,transactionCode ,transactionBatch ,  transactionDescription,   transactionAddress, grpcontno, rgtno,redField1 ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'lipeia',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','中间状态','"
					+ mLLAppealAcceptSchema.getMsgType() + "','" + mLLAppealAcceptSchema.getBranchCode() + "','"
					+ mLLAppealAcceptSchema.getBatchNo() + "','交易描述','交易地址','','','" + mLLAppealAcceptSchema.getrgtno()
					+ "')";
			mmap.put(insertClaimInsertLogSql, "INSERT");
			vdata.add(mmap);
			PubSubmit pub = new PubSubmit();
			pub.submitData(vdata, "INSERT");

			// 申诉纠错受理
			DealAppealCorrect tDealAppealCorrect = new DealAppealCorrect();
			if (!tDealAppealCorrect.submitDate(cInputData)) {
				Content = tDealAppealCorrect.getContent();
				System.out.println("DealClaimBack-DealAppealCorrect " + Content);
				VData tdata = new VData();
				MMap map = new MMap();
				String resultXml = returnXml(false, Content);
				String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'lipeia','"
						+ requestXml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
						+ resultXml + "','中间状态','" + Content + "','" + mLLAppealAcceptSchema.getMsgType() + "','"
						+ mLLAppealAcceptSchema.getBranchCode() + "','" + mLLAppealAcceptSchema.getBatchNo()
						+ "','交易描述','交易地址')";
				map.put(insertErrorInsertLogSql, "INSERT");
				tdata.add(map);
				PubSubmit pubt = new PubSubmit();
				pubt.submitData(tdata, "INSERT");
				CASECANCEL(caseNo);
				return false;
			}

			//mLLAppealAcceptSchema = (LLAppealAcceptSchema) cInputData.getObjectByObjectName("LLAppealAcceptSchema", 0);
			ExeSQL tExeSQL = new ExeSQL();
			String sql = "SELECT a.appealno FROM llappeal a,llcase b WHERE a.appealno=b.caseno AND b.rgtstate not in ('11','12','14') AND a.caseno='"
					+ mLLAppealAcceptSchema.getrgtno() + "'";
			SSRS ts = tExeSQL.execSQL(sql);

			 caseNo = ts.GetText(1, 1);
			System.out.println("申诉纠错受理end");
			System.out.println("==============" + caseNo+"==============");

			// 账单录入
			FeeInput tFeeInput = new FeeInput();
			String sqlUpdate = "update llcase set claimer='lipeia' where CaseNo = '" + caseNo + "'";
			if (!tExeSQL.execUpdateSQL(sqlUpdate)) {
				Content = "更新llcase表claimer失败";
				System.out.println("更新llcase表claimer失败");
				return false;
			}
			if (!tFeeInput.submitDate(cInputData, caseNo)) {
				Content = tFeeInput.getContent();
				System.out.println("DealClaimBack-FeeInput " + Content);
				VData tdata = new VData();
				MMap map = new MMap();
				String resultXml = returnXml(false, Content);
				String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'lipeia','"
						+ requestXml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
						+ resultXml + "','中间状态','" + Content + "','" + mLLAppealAcceptSchema.getMsgType() + "','"
						+ mLLAppealAcceptSchema.getBranchCode() + "','" + mLLAppealAcceptSchema.getBatchNo()
						+ "','交易描述','交易地址')";
				map.put(insertErrorInsertLogSql, "INSERT");
				tdata.add(map);
				PubSubmit pubt = new PubSubmit();
				pubt.submitData(tdata, "INSERT");
				CASECANCEL(caseNo);
				return false;
			}
			System.out.println("账单录入end");

			// 检录
			CheckInput tCheckInput = new CheckInput();
			if (!tCheckInput.submitDate(cInputData,caseNo)) {
				Content = tCheckInput.getContent();
				System.out.println("DealClaimBack-CheckInput " + Content);
				VData tdata = new VData();
				MMap map = new MMap();
				String resultXml = returnXml(false, Content);
				String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'lipeia','"
						+ requestXml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
						+ resultXml + "','中间状态','" + Content + "','" + mLLAppealAcceptSchema.getMsgType() + "','"
						+ mLLAppealAcceptSchema.getBranchCode() + "','" + mLLAppealAcceptSchema.getBatchNo()
						+ "','交易描述','交易地址')";
				map.put(insertErrorInsertLogSql, "INSERT");
				tdata.add(map);
				PubSubmit pubt = new PubSubmit();
				pubt.submitData(tdata, "INSERT");
				CASECANCEL(caseNo);
				return false;
			}
			System.out.println("检录end");
			// 理算
			DealAdjustment tDealAdjustment = new DealAdjustment();
			if (!tDealAdjustment.submitDate(cInputData, caseNo)) {
				Content = tDealAdjustment.getContent();
				System.out.println("DealClaimBack-DealAdjustment " + Content);
				VData tdata = new VData();
				MMap map = new MMap();
				String resultXml = returnXml(false, Content);
				String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'lipeia','"
						+ requestXml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
						+ resultXml + "','中间状态','" + Content + "','" + mLLAppealAcceptSchema.getMsgType() + "','"
						+ mLLAppealAcceptSchema.getBranchCode() + "','" + mLLAppealAcceptSchema.getBatchNo()
						+ "','交易描述','交易地址')";
				map.put(insertErrorInsertLogSql, "INSERT");
				tdata.add(map);
				PubSubmit pubt = new PubSubmit();
				pubt.submitData(tdata, "INSERT");
				CASECANCEL(caseNo);
				return false;
			}
			System.out.println("理算end");


			VData adata = new VData();
			MMap amap = new MMap();
			String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"
					+ caseNo + "' WHERE transactionCode = '" + mLLAppealAcceptSchema.getBranchCode()
					+ "' and transactionBatch='" + mLLAppealAcceptSchema.getBatchNo() + "' ";
			String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
			// 管理机构与操作人从报文中获取
			String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT lpad(to_char((nextval for SEQ_RECOINFOID)), 20, '0'), actugetno, paymode, otherno, othernotype,'','0', '', '', '1', '成功', (select EndCaseDate from llregister where rgtno='"
					+ caseNo
					+ "'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, 'lipeia', '"
					+ mLLAppealAcceptSchema.getManageCom() + "', '" + mLLAppealAcceptSchema.getrgtno()
					+ "', 'DealClaimBack', 'get', '', '' FROM ljaget WHERE actugetno IN ( SELECT actugetno FROM ljagetclaim WHERE otherno IN ( SELECT caseno FROM llcase  WHERE RgtNo ='"
					+ caseNo + "')) ";
			amap.put(updateClaimInsertLogSql, "UPDATE");
			amap.put(insertReconcileInfoSql, "INSERT");
			adata.add(amap);
			PubSubmit puba = new PubSubmit();
			puba.submitData(adata, "UPDATE");
			Content = "successful";
			returnXml(true, Content);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * 数据回滚
	 * @param CaseNo
	 * @return
	 */
	//	public boolean CASECANCEL(String CaseNo) {
//		MMap map = new MMap();
//		map.put("update LLCase set RgtState='14' where CaseNo='" + CaseNo + "'", "UPDATE");
//		map.put("update LLRegister set RgtState='01' where rgtno='" + CaseNo + "'", "UPDATE");
//		map.put("delete from LLCasePolicy where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLCaseCure where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLAppClaimReason where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLToClaimDuty where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLCaseRela where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLClaim where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLFeeMain where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLClaimPolicy where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLClaimDetail where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LCInsureAccTrace where OtherNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LCInsureAccFeeTrace where OtherNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLClaimUWmain where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLClaimUWDetail where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from ljsgetclaim where otherno='" + CaseNo + "'", "DELETE");
//		map.put("delete from ljsget where otherno='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLClaimUnderwrite where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LLClaimUWMDetail where CaseNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LJAGet where OtherNo='" + CaseNo + "'", "DELETE");
//		map.put("delete from LJAGetClaim where OtherNo='" + CaseNo + "'", "DELETE");
//
//		VData mInputData = new VData();
//		PubSubmit tPubSubmit = new PubSubmit();
//		mInputData.add(map);
//		return tPubSubmit.submitData(mInputData, null);
//	}
	//调用核心撤件
	public boolean CASECANCEL(String CaseNo) {
		ExeSQL tExeSQL = new ExeSQL();
		String rgtstate = "";
		// 查询批次状态
		String rgtstateSql = "select rgtstate from llcase where caseno='"+CaseNo+"' with ur";
		try {
			SSRS rgtstateSSRS = tExeSQL.execSQL(rgtstateSql);
			if (rgtstateSSRS.getMaxNumber() > 0) {
				rgtstate = rgtstateSSRS.GetText(1, 1);
			} else {
				Content = "请核对批次号：" + CaseNo;
				return false;
			}
		} catch (Exception e) {
			Content = "系统出错：" + e.toString();
			return false;
		}
		if(rgtstate.equals("07")){
			Content="调查状态不能撤件，必须先进行调查回复！";
			return false;
		} 
		if(rgtstate.equals("16")){
			Content="理赔二核中不能撤件，必须等待二核结束！";
			return false;
		}
		if ("06".equals(rgtstate)) {
			Content = "批次号:" + CaseNo + "还在处理中，请稍候再处理该批次的案件!";
			return false;
		}
		ExeSQL hcExeSQL = new ExeSQL();
		SSRS hospcaseSSRS;
		String hospcase = null;
		String hcsql = "select 1 from LLHospcase where caseno in(select caseno from llcase where rgtno ='"
				+ CaseNo + "' and rgttype != '8' ) and claimno is not null ";
		hospcaseSSRS = hcExeSQL.execSQL(hcsql);
		if (hospcaseSSRS.getMaxRow() > 0) {
			hospcase = hospcaseSSRS.GetText(1, 1);
			if (!"".equals(hospcase) && null != hospcase) {
				Content = "医保通案件不允许撤件";	 
				return false;
			}
		}
		RegisterBL tRegisterBL = new RegisterBL();
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		VData tVData = new VData();
		// 输出参数
		CErrors tError = null;
		String FlagStr = "";
		tLLCaseSchema.setRgtNo(CaseNo);
		tLLCaseSchema.setCancleReason("5");
		tLLCaseSchema.setCaseNo(CaseNo);
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = mLLAppealAcceptSchema.getManageCom();
		tG.Operator = "lipeia";
		String strOperate = "CASECANCEL";
		
		try {
			tVData.addElement(tLLCaseSchema);
			tVData.addElement(tG);
			if (tRegisterBL.submitData(tVData, strOperate)) {
				Content = "案件撤销成功";
				FlagStr = "Succ";
			} else {
				FlagStr = "Fail";
			}
		} catch (Exception ex) {
			Content = "案件撤销失败，原因是:" + ex.toString();
			return false;
		}
		if (FlagStr == "Fail") {
			tError = tRegisterBL.mErrors;
			if (tError.needDealError()) {
				tError = tRegisterBL.mErrors;
				Content = " 撤销失败,原因是" + tError.getFirstError();
				tVData.clear();
				return false;
			}
		}
		Content = "撤件操作完成!";
		return true;
	}

	public static void main(String[] args) {
		DealClaimBack dc = new DealClaimBack();
		dc.CASECANCEL("C3605180815000006");
	}

	/**
	 * 返回报文生成
	 * @param content
	 * @return
	 */
	public String returnXml(boolean flag, String content) {

		StringWriter stringWriter = new StringWriter();
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper.createElement("MedicalAdjustment_Request");
		document.setRootElement(tDateSet);
		// 返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Element tSendTime = tMsgResHead.addElement("SendTime");
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		Element tMsgType = tMsgResHead.addElement("MsgType");

		Element tBody = tDateSet.addElement("ApplyNo");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		Element tDescription = tBody.addElement("Description");
		Element tReserveField1 = tBody.addElement("ReserveField1");
		Element tReserveField2 = tBody.addElement("ReserveField2");
		Element tReserveField3 = tBody.addElement("ReserveField3");

		// 报文内容填充
		try {
			// tBatchNo.addText(mLLSendMsgSchema.getBatchNo());
			String sql = "select VARCHAR(Responsexml) from ErrorInsertLog where transactionBatch = '"
					+ mLLAppealAcceptSchema.getBatchNo() + "' and transactionCode = '"
					+ mLLAppealAcceptSchema.getBranchCode() + "'";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS sql1SSRS = tExeSQL.execSQL(sql);
			if (sql1SSRS.getMaxRow() > 0) {
				if (sql1SSRS.GetText(1, 1) != null && !"".equals(sql1SSRS.GetText(1, 1))) {
					tBatchNo.addText(caseNo);
				}
			} else {
				tBatchNo.addText(caseNo);
			}
			tSendDate.addText(PubFun.getCurrentDate());
			tSendTime.addText(PubFun.getCurrentTime());
			tBranchCode.addText(mLLAppealAcceptSchema.getBranchCode());
//			tSendOperator.addText("sc3672");
			tSendOperator.addText("lipeia");
			tMsgType.addText(mLLAppealAcceptSchema.getMsgType());
			// tRownum.addText(Integer.toString(mLLAppealAcceptSchema.getRownum()));
			if (flag) {
				tSuccessFlag.addText("1");
			} else {
				tSuccessFlag.addText("0");
			}
			tDescription.addText(content);

			OutputFormat format = new OutputFormat("    ", true);
			// 设置编码
			format.setEncoding("GBK");
			// 设置换行
			format.setNewlines(true);
			// 生成缩进
			format.setIndent(true);
			// 创建写文件方法
			XMLWriter xmlWriter = new XMLWriter(stringWriter, format);
			// 写入文件
			xmlWriter.write(document);
			// 关闭
			xmlWriter.close();
			// 输出xml
			System.out.println(stringWriter.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		responseXml = stringWriter.toString();
		return stringWriter.toString();

	}
}
