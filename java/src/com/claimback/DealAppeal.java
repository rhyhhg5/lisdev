package com.claimback;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LLCaseRelaDB;
import com.sinosoft.lis.llcase.AutoClaimDutyMap;
import com.sinosoft.lis.llcase.ClientRegisterBL;
import com.sinosoft.lis.llcase.LLFirstDutyFilterBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLAppealSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.lis.vschema.LLToClaimDutySet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealAppeal {
	/**
	 * 受理申请——申请确认
	 */
	//错误信息
	private String Content="";
	//可查询出来的信息
	//申请人电话
	private String RgtantPhone="";
	//申请人手机
	private String Mobile="";
	//客户特约信息 
	private String ContRemark="";
	//客户姓名
	private String CustomerName="";
	//事故类型
	private String appReasonCode="";
	//受理方式(编码)
	private String RgtType="";
	//申请人姓名
	private String RgtantName="";
	//申请人证件号码
	private String IDNo="";
	//申请人与被保人关系(编码)
	private String Relation="";
	//申请人地址
	private String RgtantAddress="";
	//邮政编码 
	private String PostCode="";
	//受益金领取方式(编码)
	private String paymode="";
	//申请人证件类型
	private String IDType="";
	//银行编码(编码)
	private String BankCode="";
	//银行账号
	private String BankAccNo="";
	//银行账户名
	private String AccName=""; 
	//回执发送方式（编码）
	private String ReturnMode="";
	//申请人电子邮箱
	private String Email="";
	//证件号码
	private String tIDNo="";
	//证件类型(编码) 客户信息
	private String tIDType="";
	//社保号码  客户信息
	private String OtherIDNo="";
	//手机号码
	private String MobilePhone="";
	//事件类型 01 机构处理
	private String SimpleCase="";
	//事件类型 01 简易案件
	private String EasyCase="";
	//上报总公司
	private String HeadCase="";
	//上报分公司
	private String FenCase="";
	// 发生日期 事件信息
	private String AccDate="";
	// 发生地点      事件信息
	private String AccPlace="";
	//入院日期 事件信息
	private String InHospitalDate="";
	//出院日期     事件信息
	private String OutHospitalDate="";
	// 事件信息 事件信息
	private String AccDesc="";
	// 事件类型 事件信息
	private String AccidentType="";
	//事故者现状(编码)
	private String CustStatus="";
	
	//必须传
	//回销预付赔款
	private String PrePaidFlag="";
	//死亡日期
	private String DeathDate="";
	//理赔号 方法中传入的参数 xu
	private String CaseNo="";
	
	
	
	
	//不需要传的
	//客户号码
	private String CustomerNo="";
	//案件状态
	private String RgtState="";
	//出生日期 客户信息
	private String CBirthday="";
	
	
	//页面中隐藏参数 报文中不需要传的
	private String operate="";
	private String rgtflag="";
	private String CaseRule="";
	private String AppAmnt="";
	private String OtherIDType="";
	private String Sex="";
	//LLRegisterInput.jsp 默认值为0 0为纠错 1 为申诉
	private String AppealFlag="0";
	//事件号 事件信息
	private String SubRptNo="";
	private String RgtNo="";
	


	//报文中要解析第一个节点信息 后续要赋值 xu
	private LLAppealAcceptSchema mLLAppea = new LLAppealAcceptSchema();
	private LLSubReportSet tEventSet = new LLSubReportSet();//客户事件信息
	//如果报文中没有出入事件信息 查询的事件信息
	private LLSubReportSet tEvent2Set = new LLSubReportSet();
	//报文中登陆机构 xu
	private String tComcode="";
	GlobalInput tG = new GlobalInput();
	String FlagStr = "";
	
	public boolean submitDate(VData cInputData,String caseNo){
		//获取数据
		if (!getInputData(cInputData,caseNo))
        {
            return false;
        }
		if(!applyConfirm()){
			return false;
		}
		
		return true;
	}
	
	private boolean getInputData(VData cInputData,String caseNo) {
		// TODO Auto-generated method stub
		mLLAppea = null;
		tEventSet = null;
		CaseNo = caseNo;
		mLLAppea = (LLAppealAcceptSchema) cInputData.getObjectByObjectName("LLAppealAcceptSchema", 0);
		tEventSet = (LLSubReportSet) cInputData.getObjectByObjectName("LLSubReportSet", 0);
		tComcode = mLLAppea.getManageCom();
//		mMap = (MMap) cInputData.getObjectByObjectName("MMap", 0);
		return true;
	}
	
	public boolean applyConfirm(){
		//CaseNo = mLLAppea.getrgtno();
		if("".equals(CaseNo)||null==CaseNo){
			Content = "受理申请确认失败原因是：理赔号不能为空!";
			return false;
		}
		ExeSQL tExeSQL=new ExeSQL();
		String sql = "select a.RgtNo,a.CustomerNo,a.CustomerName,(case a.CustomerSex when '0' then '男' when '1' then '女' when '2' then '不详' end),a.AccdentDesc,"
				+"a.handler,b.codename,c.apppeoples from llcase a,ldcode b,llregister c where a.CaseNo='" 
				+ CaseNo+"' and c.rgtno=a.rgtno and b.codetype='llrgtstate' and b.code=a.rgtstate";
		SSRS sqlSSRS=tExeSQL.execSQL(sql);
		if(sqlSSRS.getMaxNumber()>0){
			RgtNo=sqlSSRS.GetText(1, 1)	;
		//	if("".equals(CustomerNo)||null==CustomerNo){
				CustomerNo=sqlSSRS.GetText(1, 2);
	//		}
			if("".equals(mLLAppea.getCustomerName())||null==mLLAppea.getCustomerName()){
				CustomerName=sqlSSRS.GetText(1, 3);
			}else{
				CustomerName=mLLAppea.getCustomerName();
			}
			if("".equals(mLLAppea.getContRemark())||null==mLLAppea.getContRemark()){
				ContRemark=sqlSSRS.GetText(1, 5);
			}else{
				ContRemark=mLLAppea.getContRemark();
			}
			RgtState=sqlSSRS.GetText(1, 7);
			Sex=sqlSSRS.GetText(1, 4);
		}else{
			System.out.println("理赔号有误!");
			Content = "受理申请确认失败原因是：理赔号有误!";
			return false;
		}
		 String initSQL="select "
				    +"(select codename from ldcode where ldcode.codetype='llaskmode' and ldcode.code=llregister.RgtType),"
				    +"AppAmnt,"
				    +"(select codename from ldcode where ldcode.codetype='llreturnmode' and ldcode.code=llregister.ReturnMode),"
				    +"RgtantName,"
				    +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llregister.IDType),"
				    +"IDNo,"
				    +"(select codename from ldcode where ldcode.codetype='llrelation' and ldcode.code=llregister.relation),"
				    +"RgtantPhone,RgtantAddress,PostCode,"
				    +"(select codename from ldcode where ldcode.codetype='llgetmode' and ldcode.code=llregister.getmode), "
				    +"IDType,bankcode,bankaccno,accname,RgtType,ReturnMode,relation,getmode, "
				    +"(select bankname from ldbank where ldbank.bankcode=llregister.bankcode), "
				    +"RgtantMobile,email "
				    +"from LLRegister where rgtno='"+RgtNo+"'";
		 SSRS initSQLSSRS=tExeSQL.execSQL(initSQL);
		 if(initSQLSSRS.getMaxNumber()>0){
			 AppAmnt=initSQLSSRS.GetText(1, 2); 
			 if("".equals(mLLAppea.getRgtantName())||null==mLLAppea.getRgtantName()){
				RgtantName=initSQLSSRS.GetText(1, 4); 
			}else{
				RgtantName=mLLAppea.getRgtantName();
			}
			if("".equals(mLLAppea.getIDNo())||null==mLLAppea.getIDNo()){
				IDNo=initSQLSSRS.GetText(1, 6); 
			}else{
				IDNo=mLLAppea.getIDNo();
			}
			if("".equals(mLLAppea.getRgtantPhone())||null==mLLAppea.getRgtantPhone()){
				RgtantPhone=initSQLSSRS.GetText(1, 8); 
			}else{
				RgtantPhone=mLLAppea.getRgtantPhone();
			}
			if("".equals(mLLAppea.getRgtantAddress())||null==mLLAppea.getRgtantAddress()){
				RgtantAddress=initSQLSSRS.GetText(1, 9); 
			}else{
				RgtantAddress=mLLAppea.getRgtantAddress();
			}
			if("".equals(mLLAppea.getPostCode())||null==mLLAppea.getPostCode()){
				PostCode=initSQLSSRS.GetText(1, 10); 
			}else{
				PostCode=mLLAppea.getPostCode();
			}
			if("".equals(mLLAppea.getIDType())||null==mLLAppea.getIDType()){
				IDType=initSQLSSRS.GetText(1, 12); 
			}else{
				IDType=mLLAppea.getIDType();
			}
			if("".equals(mLLAppea.getBankCode())||null==mLLAppea.getBankCode()){
				BankCode=initSQLSSRS.GetText(1, 13); 
			}else{
				BankCode=mLLAppea.getBankCode();
			}
			if("".equals(mLLAppea.getBankAccNo())||null==mLLAppea.getBankAccNo()){
				BankAccNo=initSQLSSRS.GetText(1, 14); 
			}else{
				BankAccNo=mLLAppea.getBankAccNo();
			}
			if("".equals(mLLAppea.getAccName())||null==mLLAppea.getAccName()){
				AccName=initSQLSSRS.GetText(1, 15); 
			}else{
				AccName=mLLAppea.getAccName();
			}
			if("".equals(mLLAppea.getRgtType())||null==mLLAppea.getRgtType()){
				RgtType=initSQLSSRS.GetText(1, 16); 
			}else{
				RgtType=mLLAppea.getRgtType();
			}
			if("".equals(mLLAppea.getReturnMode())||null==mLLAppea.getReturnMode()){
				ReturnMode=initSQLSSRS.GetText(1, 17); 
			}else{
				ReturnMode=mLLAppea.getReturnMode();
			}
			if("".equals(mLLAppea.getRelation())||null==mLLAppea.getRelation()){
				Relation=initSQLSSRS.GetText(1, 18); 
			}else{
				Relation=mLLAppea.getRelation();
			}
			if("".equals(mLLAppea.getpaymode())||null==mLLAppea.getpaymode()){
				paymode=initSQLSSRS.GetText(1, 19); 
			}else{
				paymode=mLLAppea.getpaymode();
			}
			if("".equals(mLLAppea.getMobile())||null==mLLAppea.getMobile()){
				Mobile=initSQLSSRS.GetText(1, 21); 
			}else{
				Mobile=mLLAppea.getMobile();
			}
			if("".equals(mLLAppea.getEmail())||null==mLLAppea.getEmail()){
				Email=initSQLSSRS.GetText(1, 22); 
			}else{
				Email=mLLAppea.getEmail();
			}
			 String test1 = "select togetherflag from llregister where rgtno='"+RgtNo+"'";
		      String test1RR=tExeSQL.getOneValue(test1);
		      if("3".equals(test1RR)){
		          if("1".equals(paymode) || "2".equals(paymode)){
		          AccName="";
		          BankCode="";
		          BankAccNo="";
		          }
		          paymode=initSQLSSRS.GetText(1, 19);
		          AccName=initSQLSSRS.GetText(1, 15); 
		          BankCode=initSQLSSRS.GetText(1, 13); 
		          BankAccNo=initSQLSSRS.GetText(1, 14); 
		      } else{
		          paymode="";
		          BankCode="";
		          AccName="";
		          BankAccNo="";
		          BankAccNo= "";
		     }
		 }else{
				System.out.println("受理申请确认失败原因是：案件查询失败!");
				Content = "受理申请确认失败原因是：案件查询失败!";
				return false;
			}
		//个人案件
		  String personSQL="select customerno,customername,customersex,custbirthday,"
			      +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llcase.IDType),"
			      +"idno,idtype,otheridno,otheridtype,mobilephone from llcase where caseno='"+CaseNo+"'"; 
		 SSRS personSQLSSRS=tExeSQL.execSQL(personSQL);
		 if(personSQLSSRS.getMaxNumber()>0){
			    CustomerNo=personSQLSSRS.GetText(1, 1);
			    if("".equals(mLLAppea.getCustomerName())||null==mLLAppea.getCustomerName()){
			    	CustomerName=personSQLSSRS.GetText(1, 2);
			    }else{
			    	CustomerName=mLLAppea.getCustomerName();
			    }
			    Sex=personSQLSSRS.GetText(1, 3);
			    CBirthday=personSQLSSRS.GetText(1, 4);
			    if("".equals(mLLAppea.gettIDNo())||null==mLLAppea.gettIDNo()){
			    	tIDNo=personSQLSSRS.GetText(1, 6);
			    }else{
			    	tIDNo=mLLAppea.gettIDNo();
			    }
			    if("".equals(mLLAppea.gettIDType())||null==mLLAppea.gettIDType()){
			    	tIDType=personSQLSSRS.GetText(1, 7);
			    }else{
			    	tIDType=mLLAppea.gettIDType();
			    }
			    if("".equals(mLLAppea.getOtherIDNo())||null==mLLAppea.getOtherIDNo()){
			    	OtherIDNo=personSQLSSRS.GetText(1, 8);
			    }else{
			    	OtherIDNo=mLLAppea.getOtherIDNo();
			    }
			    OtherIDType=personSQLSSRS.GetText(1, 9);
			    if("".equals(mLLAppea.getMobilePhone())||null==mLLAppea.getMobilePhone()){
			    	MobilePhone=personSQLSSRS.GetText(1, 10);
			    }else{
			    	MobilePhone=mLLAppea.getMobilePhone();
			    }
		 }else{
				System.out.println("受理申请确认失败原因是：个人案件查询失败!");
				Content = "受理申请确认失败原因是：个人案件查询失败!";
				return false;
			}
		String  strSQL1="select AccStartDate,AccidentDate,DeathDate,AccidentSite,"
			      +"AccdentDesc,AffixGetDate,"
			      +"(select codename from ldcode where ldcode.codetype='llcuststatus' and ldcode.code=llcase.custstate),custstate"
			      +" from llcase where caseno='"+CaseNo+"'";
		SSRS strSQL1SSRS=tExeSQL.execSQL(strSQL1);
		   if( strSQL1SSRS.getMaxNumber()>0 ){
			   if("".equals(mLLAppea.getDeathDate())||null==mLLAppea.getDeathDate()){
				   DeathDate=strSQL1SSRS.GetText(1, 3);
			    }else{
			    	 DeathDate=mLLAppea.getDeathDate();
			    }
			   if("".equals(mLLAppea.getCustStatus())||null==mLLAppea.getCustStatus()){
				   CustStatus=strSQL1SSRS.GetText(1, 8);
			    }else{
			    	CustStatus=mLLAppea.getCustStatus();
			    }
		      }else{
					System.out.println("受理申请确认失败原因是：个人案件事件查询失败!");
					Content = "受理申请确认失败原因是：个人案件事件查询失败!";
					return false;
				}
		String  strSQL2 = "select casegetmode,BankCode,BankAccNo,AccName, "+
				      "(select b.bankname from ldbank b where b.bankcode=a.bankcode), "+
				      "(select c.codename from ldcode c where c.codetype='llgetmode' and c.code=a.casegetmode)"+
				      " from llcase a where a.caseno='"+CaseNo+"'";
		SSRS strSQL2SSRS=tExeSQL.execSQL(strSQL2);
		if(strSQL2SSRS.getMaxNumber()>0){
			if("".equals(mLLAppea.getpaymode())||null==mLLAppea.getpaymode()){
					paymode=strSQL2SSRS.GetText(1, 1);
			    }else{
			    	paymode=mLLAppea.getpaymode();
			    }
			if("".equals(mLLAppea.getBankCode())||null==mLLAppea.getBankCode()){
				BankCode=strSQL2SSRS.GetText(1, 2);
		    }else{
		    	BankCode=mLLAppea.getBankCode();
		    }
			if("".equals(mLLAppea.getBankAccNo())||null==mLLAppea.getBankAccNo()){
				BankAccNo=strSQL2SSRS.GetText(1, 3);
		    }else{
		    	BankAccNo=mLLAppea.getBankAccNo();
		    }
			if("".equals(mLLAppea.getBankAccNo())||null==mLLAppea.getBankAccNo()){
				BankAccNo=strSQL2SSRS.GetText(1, 3);
		    }else{
		    	BankAccNo=mLLAppea.getBankAccNo();
		    }
			if("".equals(mLLAppea.getAccName())||null==mLLAppea.getAccName()){
				AccName=strSQL2SSRS.GetText(1, 4);
		    }else{
		    	AccName=mLLAppea.getAccName();
		    }
	      }else{
				System.out.println("受理申请确认失败原因是：案件的银行信息查询失败!");
				Content = "受理申请确认失败原因是：案件的银行信息查询失败!";
				return false;
			}
	      String strSQL4="select RgtState,rigister,ModifyDate,caseprop,codename,PrePaidflag from llcase,ldcode where caseno='"+CaseNo+"' and code=RgtState and codetype='llrgtstate'";
	      SSRS strSQL4SSRS=tExeSQL.execSQL(strSQL4);
	      if(strSQL4SSRS.getMaxNumber()>0){
	         // fm.all("ModifyDate").value = arrResult[0][2];
	    	  RgtState=strSQL4SSRS.GetText(1, 5);
	    	  if("".equals(mLLAppea.getPrePaidFlag())||null==mLLAppea.getPrePaidFlag()){
	    		  if("1".equals(strSQL4SSRS.GetText(1, 6)))
		          {
	    			  PrePaidFlag="1";
		          }else{
		        	  PrePaidFlag = "";
		          }
			    }else{
			    	PrePaidFlag=mLLAppea.getPrePaidFlag();
			    }
	    	  if("".equals(mLLAppea.getSimpleCase())||null==mLLAppea.getSimpleCase()){
	    		  if("06".equals(strSQL4SSRS.GetText(1, 4))){
	    			//机构处理
	    			  SimpleCase = "01";
	  	          }
	  	        }else{
	  	        	SimpleCase = mLLAppea.getSimpleCase();
	  	        }
	    	  if("".equals(mLLAppea.getEasyCase())||null==mLLAppea.getEasyCase()){
		    	  if("09".equals(strSQL4SSRS.GetText(1, 4))){
		    			//机构处理
		    		  EasyCase = "01";
		  	          }
	    	  }else{
	    		  EasyCase = mLLAppea.getEasyCase();
	    	  }
	        }else{
				System.out.println("受理申请确认失败原因是：案件的案件类型查询失败!");
				Content = "受理申请确认失败原因是：案件的案件类型查询失败!";
				return false;
			}
	      if("".equals(mLLAppea.getContRemark())||null==mLLAppea.getContRemark()){
	      String temp="";
	      String str1SQL="select grpcontno from lccont where "+
	    		  " insuredno='"+CustomerNo+"' "+
	    		  " and grpcontno in (select grpcontno from lcgrpcont where appflag='1')"+
	    		  " and appflag='1'";
	      SSRS grpResult=tExeSQL.execSQL(str1SQL);
	      if(grpResult.getMaxNumber()>0){
	    	    for (int i = 1; i <=grpResult.getMaxRow() ;i++){
	    	      String mstrSQL1="select '保单'||grpcontno||'约定:'||remark from lcgrpcont "+
	    	      "where grpcontno='"+grpResult.GetText(i, 1)+"'";
	    	      SSRS arrResult=tExeSQL.execSQL(mstrSQL1);

	    	      String polSQL="Select '保障计划'||CONTPLANCODE||'下险种'||RISKCODE||coalesce((select codename from ldcode where codetype='CalTitle' and code=c.calfactor),'')||'约定:'||remark FROM LCCONTPLANDUTYPARAM c "+
	    	      " where grpcontno='"+grpResult.GetText(i, 1)+"' AND CONTPLANCODE!='11' and remark !='' "+
	    	      "GROUP BY CONTPLANCODE,RISKCODE,remark,calfactor";
	    	      SSRS polResult=tExeSQL.execSQL(polSQL);
	    	      if(arrResult.getMaxNumber()>0 ){
	    	        try{
	    	          temp += arrResult.GetText(1, 1)+"\n";
	    	        }
	    	        catch(Exception ex){
	    	        	System.out.println(ex.getMessage());
	    	        	Content=ex.getMessage();
	    			  	return false;
	    	        }
	    	      }
	    	      if(polResult.getMaxNumber()>0){
	    	        try{
	    	          temp += polResult.GetText(1, 1)+"\n";
	    	        }
	    	        catch(Exception ex){
	    	        	System.out.println(ex.getMessage());
	    	        	Content=ex.getMessage();
	    			  	return false;
	    	        }
	    	      }
	    	    }
	    	  }
	      String str2SQL="select contno from lccont where "+
	    		  " insuredno='"+CustomerNo+"' "+
	    		  " and appflag='1'" ;
	      SSRS perResult=tExeSQL.execSQL(str2SQL);
	    		  if(perResult.getMaxNumber()>0){
	    		    for ( int i = 1; i <= perResult.getMaxRow() ;i++){
	    		      String str3SQL="select '保单'||contno||'下险种'||(select riskcode from lcpol p where p.polno=c.polno)||'约定:'||speccontent from lcpolspecrela c "+
	    		      " where contno ='"+perResult.GetText(i, 1)+"'";
	    		      SSRS arrResult=tExeSQL.execSQL(str3SQL);
	    		      String lptbInfoSql="select Impart from lptbinfo  where contno='"+perResult.GetText(i, 1)+"'";
	    		      SSRS lptbInfoResult=tExeSQL.execSQL(lptbInfoSql);
	    		      if(arrResult.getMaxNumber()>0){
	    		        try{
	    		          temp +=	arrResult.GetText(1, 1)+"\n";
	    		        }
	    		        catch(Exception ex){
		    	        	System.out.println(ex.getMessage());
		    	        	Content=ex.getMessage();
		    			  	return false;
		    	        }
	    		      }
	    		      else{
	    		      }
	    		        if(lptbInfoResult.getMaxNumber()>0){
	    		        try{
	    		         temp +=	lptbInfoResult.GetText(1, 1)+"\n";
	    		        }
	    		        catch(Exception ex){
		    	        	System.out.println(ex.getMessage());
		    	        	Content="受理申请确认失败原因是："+ex.getMessage();
		    			  	return false;
		    	        }
	    		      }
	    		      else{
	    		      }
	    		    }
	    		  }
	    		  System.out.println(temp);
	    		  if("".equals(ContRemark)||null==ContRemark){
	  				ContRemark=temp.replaceAll(",","\n");
	  			}
	    		  if("".equals(temp)){
	    			  ContRemark="无特别约定，请依照条款处理";
	    			  }
	      }else{
	    	  ContRemark=mLLAppea.getContRemark(); 
	      }
	    // 根据客户号查询事件信息
	    // 如果通过案件查询，标记关联事件
	  	if (null==CustomerNo ||"".equals(CustomerNo)) {
	  		Content="受理申请确认失败原因是：客户号 查询失败";
		   	return false;
		}
	  	//如果报文中没有出入事件信息 用默认查出的事件信息
	  	System.out.println("事件信息条数======="+tEventSet.size());
	  	if(tEventSet.size()<=0){
	  	//判断报文中的 客户事件信息 的节点集合是否为空    为空用下面的处理   
		  	String strSQLCV="select SubRptNo,AccDate,AccPlace,AccidentType,AccSubject,HospitalName,InHospitalDate,OutHospitalDate,AccDesc,"
		  			+ "case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end," +"AccidentType from LLSubReport where CustomerNo='"
		  			+ CustomerNo+"' AND SubRptNo IN  (select subrptno  from llcaserela where CaseNo='"+CaseNo+"') order by AccDate desc";	
		  	SSRS strSQLCVSSRS=tExeSQL.execSQL(strSQLCV);
		  	if(strSQLCVSSRS.getMaxNumber()>0){
		  		for(int i=1;i<=strSQLCVSSRS.getMaxRow();i++){
		  			LLSubReportSchema tEvent2Schema = new LLSubReportSchema();
		  			tEvent2Schema.setSubRptNo(strSQLCVSSRS.GetText(i, 1));
		  			tEvent2Schema.setAccDate(strSQLCVSSRS.GetText(i, 2));
		  			tEvent2Schema.setAccPlace(strSQLCVSSRS.GetText(i, 3));
		  			tEvent2Schema.setInHospitalDate(strSQLCVSSRS.GetText(i, 7));
		  			tEvent2Schema.setOutHospitalDate(strSQLCVSSRS.GetText(i, 8));
		  			tEvent2Schema.setAccDesc(strSQLCVSSRS.GetText(i, 9));
		  			tEvent2Schema.setAccidentType(strSQLCVSSRS.GetText(i, 11));
		  			tEvent2Set.add(tEvent2Schema);
		  		}
		  	}else{
				System.out.println("受理申请确认失败原因是：案件对应的事件查询失败!");
				Content = "受理申请确认失败原因是：案件对应的事件查询失败!";
				return false;
			}
	  	}
	  	System.out.println(tComcode);
	    String tstrSQL1=" select code1,othersign from ldcode1 where codetype='LLRegStyle'  and code='"+tComcode.substring(0, 4)+"'   and othersign is not null with ur";
	    if(!"86".equals(tComcode)){
	    	SSRS tstrSQL1SSRS=tExeSQL.execSQL(tstrSQL1);
	    	if(tstrSQL1SSRS.getMaxNumber()>0){
	    		CaseRule=tstrSQL1SSRS.GetText(1, 2);
	    		  if("1".equals(CaseRule)){
	  		    	if(null==mLLAppea.getEasyCase() ||"".equals(mLLAppea.getEasyCase())){
	  		    		EasyCase="01";
	  		    	}else{
	  		    		EasyCase=mLLAppea.getEasyCase();
	  		    	}
	  	  	       }else if("2".equals(CaseRule)){
	  	  	    	 if(null==mLLAppea.getSimpleCase() ||"".equals(mLLAppea.getSimpleCase())){
	  	  	    		SimpleCase="01";
	  	 	    	}else{
	  	 	    		SimpleCase=mLLAppea.getSimpleCase();
	  	 	    	}
	  	  	       }else if("3".equals(CaseRule)){
	  	  	    	 if(null==mLLAppea.getFenCase() ||"".equals(mLLAppea.getFenCase() )){
	  	  	    		FenCase="01";
	  		 	    	}else{
	  		 	    	FenCase=mLLAppea.getFenCase() ;
	  		 	    	}
	  	  	       }else if("4".equals(CaseRule)){
	  	  	    	 if(null==mLLAppea.getHeadCase() ||"".equals(mLLAppea.getHeadCase() )){
	  	  	    		 HeadCase="01";
	  		 	    	}else{
	  		 	    		HeadCase=mLLAppea.getHeadCase() ;
	  		 	    	}
	  	  	       }
	    	}
	    }
	  
	    if(null==appReasonCode ||"".equals(appReasonCode)){
			   //身故必录身故日期和事件类型
				  String  strSQL="select ReasonCode,Reason from LLAppClaimReason where 1=1 "
						      +"and rgtno='"+RgtNo+"' "
						      +" and caseno='"+CaseNo+"'";
				  SSRS strSQLSSRS=tExeSQL.execSQL(strSQL);
				  if(strSQLSSRS.getMaxNumber()>0){
					  for(int i=1;i<strSQLSSRS.getMaxRow();i++){
						  appReasonCode+=strSQLSSRS.GetText(i, 1)+",";  
					  }
					  appReasonCode+=strSQLSSRS.GetText(strSQLSSRS.getMaxRow(), 1);
					  System.out.println("appReasonCode的值为："+appReasonCode);
				  }else{
						System.out.println("受理申请确认失败原因是：案件的事故类型查询失败!");
						Content = "受理申请确认失败原因是：案件的事故类型查询失败!";
						return false;
					} 
		   }
	    if( "00".equals(Relation)){
	        RgtantName = CustomerName;
/*	        IDTypeName=tIDTypeName;
*/	        IDType = tIDType;
	        IDNo=tIDNo;

	      String  addSQL = "select b.PostalAddress,b.zipcode,b.phone,b.mobile,b.Email from lcinsured a,lcaddress b where b.AddressNo=a.AddressNo and b.CustomerNo=a.InsuredNo and a.InsuredNo='"+CustomerNo+"'";
	       SSRS crr =tExeSQL.execSQL(addSQL) ;
	        if (crr.getMaxRow()>0){
	          RgtantAddress=crr.GetText(1, 1);
	          PostCode =crr.GetText(1, 2);
	          RgtantPhone =crr.GetText(1, 3); 
	          Mobile=crr.GetText(1, 4); 
	          Email =crr.GetText(1, 5);
	        }
	      }
	    checkRgtFlag();
		//申请确认的前台校验
	  	if(!applyConfirmCheck()){
	  		return false;
	  	}
	  	
	
		return true;
	}
	/**
	 * 申请确认 前台校验
	 * @return
	 */
	public boolean applyConfirmCheck(){
		ExeSQL tExeSQL=new ExeSQL();
		//#2329 （需求编号：2014-229）客户基本信息要素录入需求
		if(("".equals(RgtantPhone)||null==RgtantPhone) && ("".equals(Mobile)||null==Mobile)){
			System.out.println("受理申请确认失败原因是：申请人电话和申请人手机必须录入其中一项！");
			Content = "受理申请确认失败原因是：申请人电话和申请人手机必须录入其中一项！";
			return false;
		}
		if("".endsWith(CBirthday)||null==CBirthday)
		{
			System.out.println("受理申请确认失败原因是：出生日期为空！");
			Content = "受理申请确认失败原因是：出生日期为空！";
			return false;
		}
		if(ContRemark.length()>1600)
		{
		  	System.out.println("受理申请确认失败原因是：保单客户特约信息录入超过1600个汉字,无法保存!");
		  	Content="受理申请确认失败原因是：保单客户特约信息录入超过1600个汉字,无法保存!";
		  	return false;
	   }
		  //---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
		   String PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a where exists (select 1 from lccont where grpcontno=a.grpcontno and insuredno='"+ CustomerNo +"'"
		   			+" union select 1 from lbcont where grpcontno=a.grpcontno and insuredno='"+ CustomerNo +"')";      			
		   String tPrepaidBala = tExeSQL.getOneValue(PrepaidBalaSql);
		   if((null==tPrepaidBala  || "".equals(tPrepaidBala) || Integer.valueOf(tPrepaidBala) <=0)&& "1".equals(PrePaidFlag))
		   {
		   		System.out.println("受理申请确认失败原因是：被保险人投保的保单不存在预付未回销赔款!");
		   		Content="受理申请确认失败原因是：被保险人投保的保单不存在预付未回销赔款!";
		   		return false;
		   }
		/*   if(null==appReasonCode ||"".equals(appReasonCode)){
			   //身故必录身故日期和事件类型
				  String  strSQL="select ReasonCode,Reason from LLAppClaimReason where 1=1 "
						      +"and rgtno='"+RgtNo+"' "
						      +" and caseno='"+CaseNo+"'";
				  SSRS strSQLSSRS=tExeSQL.execSQL(strSQL);
				  if(strSQLSSRS.getMaxNumber()>0){
					  for(int i=1;i<strSQLSSRS.getMaxRow();i++){
						  appReasonCode+=strSQLSSRS.GetText(i, 1)+",";  
					  }
					  appReasonCode+=strSQLSSRS.GetText(strSQLSSRS.getMaxRow(), 1);
					  System.out.println("appReasonCode的值为："+appReasonCode);
				  } 
		   }*/
		   if ("05".equals(appReasonCode) || "07".equals(CustStatus)) {
		        if ("".equals(DeathDate) || null==DeathDate) {
		            System.out.println("受理申请确认失败原因是：申请原因为身故或事故者现状为死亡时，请录入死亡日期");
		            Content="受理申请确认失败原因是：申请原因为身故或事故者现状为死亡时，请录入死亡日期";
			   		return false;
		        }
		        String currentDate = PubFun.getCurrentDate();
		        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		        Date tDeathDate;
		        long diff = 0;
				try {
					tDeathDate =  df.parse(DeathDate);
			        Date tcurrentDate = df.parse(currentDate);
			         diff = tcurrentDate.getTime() - tDeathDate.getTime();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Content=e.getMessage();
				   	return false;
				}
		        if(diff<0){
		        	System.out.println("受理申请确认失败原因是：死亡日期不能晚于当前日期");
		        	Content="受理申请确认失败原因是：死亡日期不能晚于当前日期";
				   	return false;
		        }
		   }
		 //校验各种保全项目
		   if(!checkBQ()){
		   		return false;
		   }
		   if(!"1".equals(paymode)&&!"2".equals(paymode)){
			    if("".equals(BankCode)){
			    	Content="受理申请确认失败原因是：请录入银行编码！";
			    	System.out.println(Content);
				   	return false;
			    }
			    if("".equals(BankAccNo)){
			    	Content="受理申请确认失败原因是：请录入银行账号！";
			    	System.out.println(Content);
				   	return false;
			    }
			    if("".equals(AccName)){
			    	Content="受理申请确认失败原因是：请录入银行账户名！";
			    	System.out.println(Content);
				   	return false;
			    }
			    if( "4".equals(paymode)){
			    	String tBankSQL="SELECT * FROM ldbank WHERE bankcode='"+BankCode+"' AND cansendflag='1'";
			    	if (tExeSQL.execSQL(tBankSQL).getMaxNumber()<=0){
			    		Content="受理申请确认失败原因是："+BankCode+"该银行不支持银行转帐";
				    	System.out.println(Content);
					   	return false;
			    	}
			    }
			  }else{
			  	BankCode = "";
			  	BankAccNo = "";
			  	AccName ="";
			  }
		   if("".equals(paymode)){
			    Content="受理申请确认失败原因是：请录入受益金领取方式！";
		    	System.out.println(Content);
			   	return false;
			    }
		   if ("P".equals(RgtNo.substring(0,1))&&!"1".equals(AppealFlag)){
			      String strSQLRGT = " select rgtstate from LLRegister where rgtno = '" + RgtNo + "' and rgtclass = '1'";
						SSRS arrResultRGT =tExeSQL.execSQL(strSQLRGT) ;
							if(arrResultRGT.getMaxNumber()>0){
							  if (!"01".equals(arrResultRGT.GetText(1, 1))){
								  Content="受理申请确认失败原因是："+RgtNo+"该批次已受理申请完毕";
							      System.out.println(Content);
							      return false;
							  }
							}
						}  
		   if(!checkID1(tIDNo)){
			    return false;
			  }
		   if("3".equals(ReturnMode)&&"".equals(Email)){
			   Content="受理申请确认失败原因是：您选择的回复方式为电子邮件，请录入电子邮箱地址！";
			   System.out.println(Content);
			   return false;
			  }
		   if("4".equals(ReturnMode)&&"".equals(Mobile)){
			   Content="受理申请确认失败原因是：您选择的回复方式为短信，请录入手机号码！";
			   System.out.println(Content);
			   return false;
			  }
		   if("0".equals(AppealFlag)){
			   if(tEventSet.size()>0){
				   for(int i=1;i<=tEventSet.size();i++ ){
					      String happenDate=tEventSet.get(i).getAccDate();
					      String currentDate = PubFun.getCurrentDate();
					        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					        Date tAccDate;
					        long diff = 0;
							try {
								tAccDate =  df.parse(happenDate);
						        Date tcurrentDate = df.parse(currentDate);
						         diff = tcurrentDate.getTime() - tAccDate.getTime();
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								Content=e.getMessage();
							   	return false;
							}
					        if(diff<0){
					        	System.out.println("死亡日期不能晚于当前日期");
					        	Content="受理申请确认失败原因是：您在事件信息中的第"+i+"个输入的发生日期不应晚于当前时间";
							   	return false;
					        }
					        String inDate=tEventSet.get(i).getInHospitalDate();
					        String outDate=tEventSet.get(i).getOutHospitalDate();
					        DateFormat tdf = new SimpleDateFormat("yyyy-MM-dd");
					        Date tinDate;
					        Date toutDate;
					        long tdiff = 0;
							try {
								tinDate =  tdf.parse(inDate);
								toutDate = tdf.parse(outDate);
						         tdiff = toutDate.getTime() - tinDate.getTime();
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								Content=e.getMessage();
							   	return false;
							}
					        if(tdiff<0){
					        	System.out.println("死亡日期不能晚于当前日期");
					        	Content="受理申请确认失败原因是：您在事件信息中的第"+i+"个输入的出院日期不应早于住院时间";
							   	return false;
					        }
					    }
			   }
			   //简易案件，机构处理，上报总公司校验
			      if(("01".equals(SimpleCase) &&"01".equals(EasyCase) && "01".equals(HeadCase))||("01".equals(SimpleCase) &&"01".equals(EasyCase))||("01".equals(EasyCase)&&"01".equals(HeadCase))||("01".equals(SimpleCase)&& "01".equals(HeadCase))){  
			    	  Content="受理申请确认失败原因是：【简易案件】，【上报总公司】和【机构处理】只能勾选其中一个，请重试";
			    	  System.out.println( Content);
					  return false;
			      }
			      //上报分公司校验
			      if(("01".equals(SimpleCase) && "01".equals(EasyCase)  && "01".equals(HeadCase) && "01".equals(FenCase) )||( "01".equals(SimpleCase) && "01".equals(EasyCase)&& "01".equals(FenCase))||("01".equals(EasyCase) &&"01".equals(HeadCase) &&"01".equals(FenCase) )||("01".equals(SimpleCase) &&"01".equals(HeadCase)  && "01".equals(FenCase))||("01".equals(HeadCase) &&"01".equals(FenCase) )||("01".equals(EasyCase)  &&"01".equals(FenCase) )||("01".equals(SimpleCase) &&"01".equals(FenCase) )){  
			    	  Content="受理申请确认失败原因是：【简易案件】，【上报总公司】和【机构处理】只能勾选其中一个，请重试";
			    	  System.out.println( Content);
					  return false;
			      }
			      if(!"01".equals(SimpleCase) && !"01".equals(EasyCase) && !"01".equals(HeadCase)&& !"01".equals(FenCase)){
			          if("".equals(CaseRule)||null==CaseRule){
			           String strCaseSQL = "select othersign from ldcode1 where codetype='LLRegStyle' and code1='5'   and code='"+tComcode.substring(0, 4)+"' with ur";
			           SSRS strCaseSQLSSRS=tExeSQL.execSQL(strCaseSQL);
			           if(strCaseSQLSSRS.getMaxNumber()>0){
			        	     	CaseRule= strCaseSQLSSRS.GetText(1, 1);
			        	      if("1".equals(CaseRule)){
						             EasyCase= "01";
						          }
						          if("2".equals(CaseRule)){
						             SimpleCase="01";
						          }
						          if("3".equals(CaseRule)){
						             FenCase= "01";
						          }
						          if("4".equals(CaseRule)){
						             HeadCase = "01";
						          }   
				          }
			           }
			         } 
			      //客户尚有未结案的个险案件校验add lyc 2014-07-16 #2034 #2256
			      String strSQL1 = "select caseno from llcase where rgtstate not in ('14','12','11') and customerno='"
			      +CustomerNo+"' and caseno <> '"+CaseNo+"' and " +
			      		"exists(select 1 from llclaimuser where usercode=llcase.handler)" 
			      +"and exists(select 1 from lcpol where lcpol.insuredno='"+CustomerNo+"' and conttype='1')";
			      SSRS strSQL1SSRS=tExeSQL.execSQL(strSQL1);
			      if(strSQL1SSRS.getMaxNumber()>0){
				    	  Content="受理申请确认失败原因是："+CustomerNo+"该客户存在案件号为"+strSQL1SSRS.GetText(1, 1)+"的案件还处理完毕，请处理完毕后再申请，谢谢！";
				    	  System.out.println( Content);
						  return false;
			      }
			      //校验客户所有保全项
			      String strSQL2 = "select a.edoracceptno ,(select edorname from lmedoritem where edorcode=b.edortype fetch first 1 rows only) "
			      +"from lpedorapp a,lpedoritem b where a.edoracceptno=b.edoracceptno " 
			      +"and a.edorstate!='0' and b.contno in ( "
			      +"select contno from lcinsured where grpcontno='00000000000000000000' " 
			      +"and insuredno='"+CustomerNo+"') ";
			      SSRS strSQL2SSRS=tExeSQL.execSQL(strSQL2);
			          if(strSQL2SSRS.getMaxNumber()>0){
			          	for(int i=1;i<=strSQL2SSRS.getMaxRow();i++){
			          		Content+=CustomerNo+"该客户存在工单号为"+strSQL2SSRS.GetText(i, 1)+"的"+strSQL2SSRS.GetText(i, 2)+"保全项 还未结案，请处理完毕后再申请，谢谢！";
					    	System.out.println( Content);
			          	}
			          	Content+="受理申请确认失败原因是："+Content;
			          	return false;
			          }
			          if(null==CustomerNo||"".equals(CustomerNo)){
			        	  Content="受理申请确认失败原因是：客户号查询";
				    	  System.out.println( Content);
						  return false;
			          }else{
			        	  if ("1".equals(rgtflag)){
			        		  operate = "UPDATE||MAIN";
				              String strSQL = " select AppPeoples,customerno,grpname from LLRegister where rgtno = '" +RgtNo+ "' and rgtclass = '1'";
				              SSRS arrResult =tExeSQL.execSQL(strSQL); 
				              if(arrResult.getMaxNumber()<=0){
				            	  Content="受理申请确认失败原因是：团体案件信息查询失败！";
						    	  System.out.println( Content);
								  return false;
				              }
			        		  
			        	  }else{
			        	        operate= "INSERT||MAIN";
			        	  }
			        	//调用纠错的后台方法ClientReasonSave.jsp 
			        	  if(!dealApplyConfirm()){
			        		return false;  
			        	  }
			          }
			          
			          
		   }else{
			   //调用申诉的后台方法AppealSave.jsp xu  
			   /*if(null==AppeanRCode||"".equals(AppeanRCode)){
				      alert("请选择申诉原因！");
				      return false;
				    }
				    fm.all('operate').value = "APPEALINSERT||MAIN";*/
		   }
					
		return true;
	}
	/**
	 *  校验客户证件号
	 * @param vIDNO
	 * @return
	 */
	public boolean checkID1 (String vIDNO) {
		ExeSQL tExeSQL=new ExeSQL();
	    if("".equals(vIDNO)||null==vIDNO||"".equals(tIDType)||null==tIDType){
	    	Content="受理申请确认失败原因是：证件号码和证件类型不能为空，请确认是否录入,申请失败！";
		    System.out.println(Content);
		    return false;
	    }
	    if(!"".equals(CustomerNo) && null!=CustomerNo)
		{  
			String  strSql = "select idtype,idno from lcinsured where insuredno='"+CustomerNo+"' and idno = '"+vIDNO+ "' and idtype = '"+tIDType+"' union all select idtype,idno from lbinsured where insuredno='"+CustomerNo+"' and idno = '" +vIDNO+  "' and idtype = '"+tIDType+"' with ur";
	        SSRS arrResult = tExeSQL.execSQL(strSql);
	    if (arrResult.getMaxNumber()<=0){
	    	Content="受理申请确认失败原因是：录入的证件号码【"+vIDNO+"】或者证件类型【"+tIDType+"】有误,申请失败！";
		    System.out.println(Content);
		    return false;
	    } 
	  }
	  return true;
	}
	/**
	 * 校验各种保全项目
	 * @return
	 */
	public boolean checkBQ(){
		ExeSQL tExeSQL=new ExeSQL();
		//正在进行中，提示且阻断
		String tIngSql1 = "select * from ( "
					+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
	    			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
	    			+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
	    			+ " where a.grpcontno=b.grpcontno "
	    			+ " and a.insuredno='"+CustomerNo+"' "
	    			+ " and a.grpcontno!='00000000000000000000' "
	    			+ " and c.edoracceptno=b.edorno"
	    			+ " and c.edorstate != '0' "
	    			+ " and b.edortype in ('WT','XT','GA','TQ','TA','SG','CT','TF','LQ','ZB') "//团单保全
	    			+ " union all "
	    			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
	    			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
	    			+ " from lcinsured a,lpedoritem b,lpedorapp c "
	    			+ " where a.contno=b.contno "
	    			+ " and a.insuredno='"+CustomerNo+"' "
	    			+ " and a.grpcontno='00000000000000000000' "
	    			+ " and c.edoracceptno=b.edorno"
	    			+ " and c.edorstate != '0' "
	    			+ " and b.edortype in ('PR','BP','BA','FC','FX','WT','GF','RF','LN','BF','XT','BC','CM','TB','NS','TF','CT','WX','ZB','LQ','RB')"//个单保全
	    			+ " ) as temp "
	    			+ " group by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname "
	    			+ " order by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname ";
		SSRS tIngArr1=tExeSQL.execSQL(tIngSql1);
		if(tIngArr1.getMaxNumber()>0){
			for(int i=1;i<=tIngArr1.getMaxRow();i++){
				if(null!=tIngArr1.GetText(i, 1)  && !"".endsWith(tIngArr1.GetText(i, 1)) &&  !"00000000000000000000".equals(tIngArr1.GetText(i, 1))){
					System.out.println(CustomerNo+"该客户所在团单(保单号码："+tIngArr1.GetText(i, 1) +"正在进行"+tIngArr1.GetText(i, 5)+"保全操作,工单号为："+tIngArr1.GetText(i, 4)+"。 \n");
		        	Content="受理申请确认失败原因是："+CustomerNo+"该客户所在团单(保单号码："+tIngArr1.GetText(i, 1) +"正在进行"+tIngArr1.GetText(i, 5)+"保全操作,工单号为："+tIngArr1.GetText(i, 4)+"。 \n";
				   	return false;
				}else{
					System.out.println(CustomerNo+"该客户所在个单(保单号码："+tIngArr1.GetText(i, 1) +"正在进行"+tIngArr1.GetText(i, 5)+"保全操作,工单号为："+tIngArr1.GetText(i, 4)+"。 \n" );
		        	Content="受理申请确认失败原因是："+CustomerNo+"该客户所在个单(保单号码："+tIngArr1.GetText(i, 1) +"正在进行"+tIngArr1.GetText(i, 5)+"保全操作,工单号为："+tIngArr1.GetText(i, 4)+"。 \n";
				   	return false;
				}
			}
		}
		//该被保人所在分单的保全项目
		String tIngSql2 = " select a.grpcontno,a.contno,b.edortype,b.edorno, "
					+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
					+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
					+ " where a.grpcontno=b.grpcontno "
					+ " and a.insuredno='"+CustomerNo+"' "
					+ " and a.grpcontno!='00000000000000000000' "
					+ " and c.edoracceptno=b.edorno"
					+ " and c.edorstate != '0' "
					+ " and b.edortype in ('LP','ZT','BC','CM','WD','JM')"
					+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
					+ " order by a.grpcontno,a.contno,b.edortype,b.edorno ";
	    SSRS tIngArr2 =tExeSQL.execSQL(tIngSql2) ;
	    if(tIngArr2.getMaxNumber()>0){
	    	for(int i=1;i<=tIngArr2.getMaxRow();i++){
	    		String tTorG = "团单(保单号码："+tIngArr2.GetText(i, 1)+")";
	    		String tempSql = " select 1 from lpinsured a "
						 + " where a.grpcontno='"+tIngArr2.GetText(i, 1)+"' "
						 + " and a.contno = '"+tIngArr2.GetText(i, 2)+"' "
						 + " and a.edortype = '"+tIngArr2.GetText(i, 3)+"' "
						 + " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate<>'0') "
						 + " union all "
						 + " select 1 from lpbnf a "
						 + " where 1=1 "
						 + " and a.contno = '"+tIngArr2.GetText(i, 2)+"' "
						 + " and a.edortype = '"+tIngArr2.GetText(i,3)+"' "
						 + " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate<>'0') "
						 + " with ur ";
				SSRS tempArr =tExeSQL.execSQL(tempSql);
				if(tempArr.getMaxNumber()>0){
					if("LP".endsWith(tIngArr2.GetText(i, 3))){
					}else{
						System.out.println("该客户正在"+tTorG+"中进行"+tIngArr2.GetText(1, 5)+"保全操作,工单号为："+tIngArr2.GetText(i, 4)+"。 \n" );
			        	Content="受理申请确认失败原因是："+CustomerNo+"该客户正在"+tTorG+"中进行"+tIngArr2.GetText(1, 5)+"保全操作,工单号为："+tIngArr2.GetText(i, 4)+"。 \n" ;
					   	return false;
					}
				}
	    	}
	    }
		//没有保全号的保全项目(各种满期给付各种特殊处理)。。。。//0 给付完成；1 正在给付。
		//常无忧B满期给付
		String tCWMJSql = " select temp.ContNo,temp.edorstate "
				   + " from "
				   + " ( "
				   + " select a.contno," 
				   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate "
				   + " from ljsgetdraw a where insuredno='"+CustomerNo+"' and feefinatype='TF' and riskcode='330501' "
				   + " ) as temp"
				   + " group by temp.ContNo,temp.edorstate "
				   + " order by temp.ContNo,temp.edorstate ";
		SSRS tCWMJArr =tExeSQL.execSQL(tCWMJSql);
		if(tCWMJArr.getMaxNumber()>0){
			for(int i=1;i<=tCWMJArr.getMaxRow();i++){
				String tTorG = "个单(保单号码："+tCWMJArr.GetText(i, 1)+")";
				if("1".equals(tCWMJArr.GetText(i, 2))){
					System.out.println("该客户所在"+tTorG+"正在进行常无忧B满期给付保全操作。 \n" );
		        	Content="受理申请确认失败原因是："+CustomerNo+"该客户所在"+tTorG+"正在进行常无忧B满期给付保全操作。 \n";
				   	return false;
				}
			}
		}
		//个单满期给付
		String tGDMJSql = " select temp.ContNo,temp.edorstate "
				   + " from "
				   + " ( "
				   + " select a.contno," 
				   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate "
				   + " from ljsgetdraw a where insuredno='"+CustomerNo+"' and feefinatype='TF' and riskcode not in ('170206','330501') "
				   + " ) as temp"
				   + " group by temp.ContNo,temp.edorstate "
				   + " order by temp.ContNo,temp.edorstate ";
		SSRS tGDMJArr =tExeSQL.execSQL(tGDMJSql);
		if(tGDMJArr.getMaxNumber()>0){
			for(int i=1;i<=tGDMJArr.getMaxRow();i++){
				String tTorG = "个单(保单号码："+tGDMJArr.GetText(i, 1)+")";
				if("0".equals(tGDMJArr.GetText(i, 2))){//需求调整，做完保全的仅提示不阻断
				}else{
					System.out.println("该客户所在"+tTorG+"正在进行个单满期给付保全操作。 \n");
		        	Content="受理申请确认失败原因是："+CustomerNo+"该客户所在"+tTorG+"正在进行个单满期给付保全操作。 \n";
				   	return false;
				}
			}
		}  
		String tMJSql = " select temp.GrpContNo,temp.edorName,temp.edorstate "
		           + " from "
		           + " ( "
		           + " select a.grpcontno GrpContNo,'团单满期给付' edorName," //团单满期给付
				   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate"
				   + " from ljsgetdraw a,lcinsured b "
				   + " where a.grpcontno = b.grpcontno and b.grpcontno != '00000000000000000000' "
				   + " and a.feefinatype='TF' and a.riskcode='170206' "
				   + " and b.insuredno='"+CustomerNo+"' "
				   + " ) as temp "
				   + " group by temp.GrpContNo,temp.edorName,temp.edorstate "
				   + " order by temp.GrpContNo,temp.edorName,temp.edorstate "
				   + " with ur "; 
		SSRS tMJArr =tExeSQL.execSQL(tMJSql) ;
		if(tMJArr.getMaxNumber()>0){
			for(int i=1;i<=tMJArr.getMaxRow();i++){
				String tTorG = "团单(保单号码："+tMJArr.GetText(i, 1)+")";
				if("0".equals(tMJArr.GetText(i, 3))){//需求调整，做完保全的，仅提示不阻断
				}else{
					System.out.println("该客户所在"+tTorG+"正在进行"+tMJArr.GetText(i, 2)+"保全操作。 \n");
		        	Content="受理申请确认失败原因是："+CustomerNo+"该客户所在"+tTorG+"正在进行"+tMJArr.GetText(i, 2)+"保全操作。 \n";
				   	return false;
				}
			}
		}
		//所在团单正在定期结算
		String tDJSql = " select distinct a.contno "
				   + " from lgwork a,lcinsured b "
				   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
				   + " and a.typeno like '06%' and a.statusno not in ('5','8') "
				   + " and b.insuredno='"+CustomerNo+"' ";
		SSRS tDJArr =tExeSQL.execSQL(tDJSql);
		if(tDJArr.getMaxNumber()>0){
			for(int i=1;i<=tDJArr.getMaxRow();i++){
				String tTorG = "团单(保单号码："+tDJArr.GetText(i, 1)+")";
				System.out.println("该客户所在"+tTorG+"正在进行定期结算保全操作。 \n");
	        	Content="受理申请确认失败原因是："+CustomerNo+"该客户所在"+tTorG+"正在进行定期结算保全操作。 \n";
			   	return false;
			}
		}
		return true;
	}
	public void checkRgtFlag(){
		String strSQL=" select rgtclass, apppeoples from LLRegister where rgtno = '" +RgtNo+ "'";
  		ExeSQL tExeSQL=new ExeSQL();
		SSRS arrResult=tExeSQL.execSQL(strSQL);
		rgtflag="0";
		 if(arrResult.getMaxNumber()>0){
		      String RgtClass;
		      RgtClass=arrResult.GetText(1, 1);
		      String strSQL1 = " select count(*) from llcase where rgtno = '" +RgtNo+ "'";
		      SSRS arrResult1 =tExeSQL.execSQL(strSQL1); 
		      if ("1".equals(RgtClass))
		      {
		        rgtflag= "1";
		      }
		    }
	}
	public boolean dealApplyConfirm(){
		//报文中的 xu
//		tG.Operator ="sc3672";
		tG.ManageCom=mLLAppea.getManageCom();
		tG.Operator ="lipeia";
//		tG.ManageCom = "86360500";
		String strOperate =operate;
		LLAppClaimReasonSet tLLAppClaimReasonSet   = new LLAppClaimReasonSet();
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LLAppealSchema tLLAppealSchema = new LLAppealSchema();
		LLSubReportSet tLLSubReporSet = new LLSubReportSet();
		LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
		//声明VData
		VData tVData = new VData();
		//声明后台传送对象
		ClientRegisterBL tClientRegisterBL   = new ClientRegisterBL();
		//输出参数
		//输出参数
		CErrors tError = null;
		String tRela  = "";
		String EventFlag="";	//事件申请标记 为1 有客户存在事件，为0不存在事件
		/*将申请信息填充*/
		if (strOperate.equals("INSERT||MAIN")) 	
		{
		    tLLRegisterSchema.setRgtState("13");//案件状态
		    tLLRegisterSchema.setRgtObj("2");						//号码类型 0总单 1分单 2个单 3客户
		    tLLRegisterSchema.setRgtObjNo(CustomerNo);
		    System.out.println("RgtObjNo:"+tLLRegisterSchema.getRgtObjNo());
		    tLLRegisterSchema.setRgtClass("0");
		    tLLRegisterSchema.setCustomerNo(CustomerNo)	;
		    tLLRegisterSchema.setRgtType(RgtType);         		//受理方式
		    tLLRegisterSchema.setRgtantName(RgtantName);     	//申请人姓名
		    tLLRegisterSchema.setRelation(Relation);        		//与被保险人关系
		    tLLRegisterSchema.setRgtantAddress(RgtantAddress);	//申请人地址
		    tLLRegisterSchema.setRgtantPhone(RgtantPhone);     	//申请人电话
		    tLLRegisterSchema.setRgtantMobile(Mobile);    //申请人手机
		    tLLRegisterSchema.setEmail(Email);           //E-mail
		    tLLRegisterSchema.setPostCode(PostCode);        		//邮编
		    tLLRegisterSchema.setAppAmnt(AppAmnt);       		//预估申请金额  
		    System.out.println("银行信息");
		    tLLRegisterSchema.setGetMode(paymode);
		    tLLRegisterSchema.setBankCode(BankCode);
		    tLLRegisterSchema.setBankAccNo(BankAccNo);
		    tLLRegisterSchema.setAccName(AccName);
		    tLLRegisterSchema.setReturnMode(ReturnMode);    		//回执发送方式  
		    tLLRegisterSchema.setIDType(IDType);        			//申请人证件类型
		    tLLRegisterSchema.setIDNo(IDNo);          			//申请人证件号码
		    tLLRegisterSchema.setRgtObj("1"); //个人客户                              
		    tLLCaseSchema.setRgtType("1");
		}
		if (strOperate.equals("UPDATE||MAIN"))
		{
		  tLLRegisterSchema.setRelation("05");
		  tLLCaseSchema.setRgtType("1");
		  tLLRegisterSchema.setRgtNo(RgtNo);
		} 
		  /******************************************************/
	     //申请原因
		String tNum[] =appReasonCode.split(",");
		int AppReasonNum = 0;
		if (tNum != null) 
		{
			AppReasonNum = tNum.length;
		}
		if ("01".equals(SimpleCase)){
	  	tLLCaseSchema.setCaseProp("06");
		}else{
		  tLLCaseSchema.setCaseProp("08");
		}
		if ("01".equals(EasyCase)) {
			  tLLCaseSchema.setCaseProp("09");
			}
			if ("01".equals(FenCase)) {
			  tLLCaseSchema.setCaseProp("11");
			}
			if ("01".equals(HeadCase)) {
			  tLLCaseSchema.setCaseProp("08");
			}
			System.out.println("888888888888888888888888888受理申请选择的上报方式:"+tLLCaseSchema.getCaseProp());
			if ("1".equals(PrePaidFlag)) {
				  tLLCaseSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
			}else{
				tLLCaseSchema.setPrePaidFlag("");  // 个案标记llcase 批次案件标记llregister
			}
		/*将原因信息填充*/
		for (int i = 0; i < AppReasonNum; i++)	
		{  
	    LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();                              
	 		tLLAppClaimReasonSchema.setReasonCode(tNum[i]);        //原因代码                                                         
			//tLLAppClaimReasonSchema.setReason(appResonName[i]);                //申请原因                                             
			tLLAppClaimReasonSchema.setCustomerNo(CustomerNo); 
			tLLAppClaimReasonSchema.setReasonType("0");
			
			tLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);
		}
		//事件信息
		if(tEventSet.size()>0){
			 for(int i=1;i<=tEventSet.size();i++ ){
		  			LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
			        tLLSubReportSchema.setSubRptNo(tEventSet.get(i).getSubRptNo());
				    tLLSubReportSchema.setCustomerNo(CustomerNo);
				    tLLSubReportSchema.setCustomerName(CustomerName);      
				    tLLSubReportSchema.setAccDate(tEventSet.get(i).getAccDate());
				    tLLSubReportSchema.setAccDesc(tEventSet.get(i).getAccDesc());   
				    tLLSubReportSchema.setAccPlace(tEventSet.get(i).getAccPlace());  
				    tLLSubReportSchema.setInHospitalDate( tEventSet.get(i).getInHospitalDate());
				    tLLSubReportSchema.setOutHospitalDate( tEventSet.get(i).getOutHospitalDate()); 
				    tLLSubReportSchema.setAccidentType(tEventSet.get(i).getAccidentType());
					tLLSubReporSet.add(tLLSubReportSchema);
			 }
		}else{
			 for(int i=1;i<=tEvent2Set.size();i++ ){
		  			LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
			        tLLSubReportSchema.setSubRptNo(tEvent2Set.get(i).getSubRptNo());
				    tLLSubReportSchema.setCustomerNo(CustomerNo);
				    tLLSubReportSchema.setCustomerName(CustomerName);      
				    tLLSubReportSchema.setAccDate(tEvent2Set.get(i).getAccDate());
				    tLLSubReportSchema.setAccDesc(tEvent2Set.get(i).getAccDesc());   
				    tLLSubReportSchema.setAccPlace(tEvent2Set.get(i).getAccPlace());  
				    tLLSubReportSchema.setInHospitalDate( tEvent2Set.get(i).getInHospitalDate());
				    tLLSubReportSchema.setOutHospitalDate( tEvent2Set.get(i).getOutHospitalDate()); 
				    tLLSubReportSchema.setAccidentType(tEvent2Set.get(i).getAccidentType());
					tLLSubReporSet.add(tLLSubReportSchema);
			 }
		}
		/**************************************************/ 
		System.out.println("lllllllllll"+tLLCaseSchema.getRgtType());
	    tLLCaseSchema.setRgtState("13");//案件状态
	    tLLCaseSchema.setCustomerName(CustomerName);
	    tLLCaseSchema.setCustomerNo(CustomerNo);
	    tLLCaseSchema.setPostalAddress(RgtantAddress);
	    tLLCaseSchema.setMobilePhone(MobilePhone);
	    tLLCaseSchema.setDeathDate(DeathDate);
	    tLLCaseSchema.setCustBirthday(CBirthday);
	    tLLCaseSchema.setCustomerSex(Sex);
	    //tLLCaseSchema.setAccidentType(request.getParameter("CaseOrder"));
	    tLLCaseSchema.setIDType(tIDType); 
	    tLLCaseSchema.setIDNo(tIDNo);
	    tLLCaseSchema.setCustState(CustStatus);
	    tLLCaseSchema.setCaseGetMode(paymode);
	    tLLCaseSchema.setBankCode(BankCode);
	    tLLCaseSchema.setBankAccNo(BankAccNo);
	    tLLCaseSchema.setAccName(AccName);
	    tLLCaseSchema.setCaseNo(CaseNo);
	    tLLCaseSchema.setSurveyFlag("0");
	    tLLCaseSchema.setAccdentDesc(ContRemark);
	    tLLCaseSchema.setOtherIDNo(OtherIDNo);
	    tLLCaseSchema.setOtherIDType("5");
	    
	   /* if("1605".equals(RiskCode))
		    tLLCaseSchema.setRiskCode(request.getParameter("RiskCode"));*/
		String CurrentDate= PubFun.getCurrentDate();   
	    String CurrentTime= PubFun.getCurrentTime();
			//保存案件时效
			tLLCaseOpTimeSchema.setRgtState("01");
			tLLCaseOpTimeSchema.setStartDate(CurrentDate);
			tLLCaseOpTimeSchema.setStartTime(CurrentTime);
			  try                                 
			  {                                   
			  // 准备传输数据 VData               
				//VData传送
					System.out.println("<--Star Submit VData-->");
					tVData.add(tLLAppClaimReasonSet);
					tVData.add(tLLCaseSchema);
					tVData.add(tLLRegisterSchema);
					tVData.add(tLLAppealSchema);
					tVData.add( tLLSubReporSet );
					tVData.add(tLLCaseOpTimeSchema);

			  	tVData.add(tG);
			  	System.out.println("<--Into tClientRegisterBL-->");
			    tClientRegisterBL.submitData(tVData,strOperate);
			  } 
			  catch(Exception ex)
			  { 
			    Content = "受理申请确认失败原因是：保存失败  " + ex.toString();
			    FlagStr = "Fail";
			  }
			    
			//如果在Catch中发现异常，则不从错误类中提取错误信息
			  if ("".equals(FlagStr))
			  { 
			    tError = tClientRegisterBL.mErrors;
			    if (!tError.needDealError())
			    {                          
			    	Content = " 保存成功! ";
			    	FlagStr = "Success";
			    	tVData.clear();
			    	tVData=tClientRegisterBL.getResult();              
			    }
			    else                                                                           
			    {
			    	Content = " 受理申请确认失败原因是：保存失败 " + tError.getFirstError();
			    	FlagStr = "Fail";
			    }
			  }
			  if("Fail".equals(FlagStr)){
				 System.out.println(Content); 
				 return false;
			  }
			  //添加各种预处理
			  LLCaseSchema mLLCaseSchema = new LLCaseSchema(); 
			  
			  mLLCaseSchema.setSchema(( LLCaseSchema )tVData.getObjectByObjectName( "LLCaseSchema", 0 ));
			      LLFirstDutyFilterBL tFirstDutyFilterBL = new LLFirstDutyFilterBL();
			      TransferData CaseNo = new TransferData();
			      CaseNo.setNameAndValue("CaseNo",mLLCaseSchema.getCaseNo());
			      VData tVData1 = new VData();
					try{
						tVData1.add(CaseNo);
						tVData1.add(tG);
						System.out.println("tG"+mLLCaseSchema.getCaseNo());
						tFirstDutyFilterBL.submitData(tVData1, "INSERT");
					}
					catch (Exception ex){
						Content = "受理申请确认失败原因是：保存失败 " + ex.toString();
						FlagStr = "Fail";
					}
					 if("Fail".equals(FlagStr)){
						 System.out.println(Content); 
						 return false;
					  }
					  tVData1.clear();
					  System.out.println("结束了");
					  //作废续期应收
					   TransferData tTransfer = new TransferData();
					   tTransfer = (TransferData) tVData.getObjectByObjectName("TransferData", 0);
					   if (tTransfer != null) {
					 	  String tIndiInfo = (String)tTransfer.getValueByName("IndiInfo");
					 	  System.out.println(tIndiInfo);
					 	  System.out.println(Content);
					 	  Content = Content + tIndiInfo;
					 	  System.out.println(Content);
					   }
		return true;
	}
	/**
	 * 删除受理申请的数据
	 * @return
	 */
	public boolean deleteDate(){
		  MMap map = new MMap();
		  
		  LLToClaimDutySet llToClaimDutySet = getToClaimDutySet(CaseNo);
		  map.put(llToClaimDutySet,"DELETE");
		  map.put("delete from llcaserela where caseno='" +
				  CaseNo+ "'", "DELETE");
		  String delsql = "delete from llappclaimreason where caseno='"
                  + CaseNo+ "'";
          map.put(delsql, "DELETE");
          VData mInputData = new VData();
  	    PubSubmit tPubSubmit = new PubSubmit();
  	    mInputData.add(map);
  	    return tPubSubmit.submitData(mInputData, null);
	}
	  public LLToClaimDutySet getToClaimDutySet(String caseNo)
	  {
		CErrors mErrors = new CErrors();
	    LLToClaimDutySet result = new LLToClaimDutySet();

	    LLCaseRelaDB relaDB = new LLCaseRelaDB();
	    relaDB.setCaseNo(caseNo);
	    LLCaseRelaSet relaSet = relaDB.query();
	    AutoClaimDutyMap autoClaimDutyMap = null;

	    try
	    {
	      autoClaimDutyMap = (AutoClaimDutyMap) Class.forName("com.sinosoft.lis.llcase."+SysConst.AUTOCHOOSEDUTY).newInstance();
	    }
	    catch (Exception ex)
	    {
	      ex.printStackTrace();
	      mErrors.addOneError("加载AutoClaimDutyMap实现类时错误:Msg="+ex.getMessage());
	      return null;
	    }

	    System.out.println(".........."+relaSet.size());

	    for(int i=1;i<=relaSet.size();i++)
	    {
	      LLToClaimDutySet oneRela = autoClaimDutyMap.autoChooseGetDuty(caseNo,relaSet.get(i).getCaseRelaNo());
	      result.add(oneRela);
	    }

	    System.out.println(".........."+result.size());

	    return result;
	  }
	public String getContent() {
		return Content;
	}
	public static void main(String[] args) {
		DealAppeal a =new DealAppeal();
		//a.deleteDate();
		a.applyConfirm();
		//a.dealApplyConfirm();
	}
}
