package com.claimback;

import com.sinosoft.lis.llcase.AllCaseInfoUI;
import com.sinosoft.lis.llcase.CaseCheckUI;
import com.sinosoft.lis.llcase.CasePolicyUI;
import com.sinosoft.lis.llcase.ClaimCalBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLAppealAcceptSchema;
import com.sinosoft.lis.schema.LLCaseInfoSchema;
import com.sinosoft.lis.schema.LLCasePolicySchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLAccidentSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseInfoSet;
import com.sinosoft.lis.vschema.LLCasePolicySet;
import com.sinosoft.lis.vschema.LLDisabilitySet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.lis.vschema.LLOperationSet;
import com.sinosoft.lis.vschema.LLOtherFactorSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class CheckInput {
	String Content = "";
	
	private LLAppealAcceptSchema mLLAppea = new LLAppealAcceptSchema();
	public String getContent() {
		return Content;
	}

	String FlagStr = "";
	String strCaseNo="";
	
	public boolean submitDate(VData cInputData,String caseNo){
		//获取数据
		if (!getInputData(cInputData,caseNo))
        {
            return false;
        }
		if(!save()){
			return false;
		}
		if(!formCheck()){
			return false;
		}
		return true;
	}
	
	
	
	private boolean getInputData(VData cInputData,String caseNo) {
		// TODO Auto-generated method stub
		mLLAppea = (LLAppealAcceptSchema) cInputData.getObjectByObjectName("LLAppealAcceptSchema", 1);
		strCaseNo = caseNo;
		return true;
	}



	/**
	 * 检录   保存按钮
	 * 后台
	 * @return
	 */
	public boolean save(){
		
		  AllCaseInfoUI tAllCaseInfoUI = new AllCaseInfoUI();
		  
		  CErrors tError = null;
		  String tRela  = "";
		  String transact = "INSERT";
		  
		  ExeSQL tExeSQL = new ExeSQL();
		  String sql = "select a.RgtNo,a.CustomerNo,a.CustomerName,(case a.CustomerSex when '0' then '男' when '1' then '女' when '2' then '不详' end),a.AccdentDesc,a.handler,b.codename,c.apppeoples from llcase a,ldcode b,llregister c where a.CaseNo='"+strCaseNo+"' and c.rgtno=a.rgtno and b.codetype='llrgtstate' and b.code=a.rgtstate";
		  SSRS sqlSSRS = tExeSQL.execSQL(sql);
		  String CustomerNo = sqlSSRS.GetText(1,2);//客户号
		  sql = "select distinct a.ReceiptNo,a.HospitalName,a.FeeDate,a.CaseRelaNo,a.MainFeeNo,a.sumfee,a.HospitalCode from llfeemain a  where a.caseno='"+strCaseNo+"'";
		  SSRS sqlSSRS2 = tExeSQL.execSQL(sql);
		  String CaseRelaNo = sqlSSRS2.GetText(1,4);
		  String RgtNo = sqlSSRS.GetText(1,1);
		  System.out.println(strCaseNo +"aaa"+CaseRelaNo+"bbbb"+RgtNo);
		  LLCaseSchema tLLCaseSchema  = new LLCaseSchema();
		  tLLCaseSchema.setCaseNo(strCaseNo);
		  tLLCaseSchema.setRgtNo(RgtNo);
		  GlobalInput tG = new GlobalInput();
//		  tG.Operator = "sc3672";
//		  tG.ManageCom = "86360500";
		  tG.Operator = "lipeia";
		  tG.ManageCom = mLLAppea.getManageCom();
		  VData tVData = new VData();
			tVData.addElement(tG);
		  
		 /////////////////////////////////////////////////////////////////////////////////////
		 	LLCaseInfoSet tLLCaseInfoSet=new LLCaseInfoSet();
		  LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
		  LLOperationSet tLLOperationSet = new LLOperationSet();
		  LLOtherFactorSet tLLOtherFactorSet = new LLOtherFactorSet();
		  LLAccidentSet tLLAccidentSet = new LLAccidentSet();
		  LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
		  LLSubReportSet tLLSubReportSet = new LLSubReportSet();
		  LLDisabilitySet tLLDisabilitySet = new LLDisabilitySet();
		  mLLCaseInfoSchema.setCaseNo(strCaseNo);
		  
		  //事件信息
//			String tRNum[] = request.getParameterValues("InpInsuredEventGridSel");
//			String eventno[] = request.getParameterValues("InsuredEventGrid1");
//			String accdate[] = request.getParameterValues("InsuredEventGrid2");
//			String accplace[] = request.getParameterValues("InsuredEventGrid3");
//			String accindate[] = request.getParameterValues("InsuredEventGrid4");
//			String accoutdate[] = request.getParameterValues("InsuredEventGrid5");
//			String accdesc[] = request.getParameterValues("InsuredEventGrid6");
			
			System.out.println("<--submit mulline-->");
			//sql = "select subrptno  from llcaserela where CaseNo='R3600170104000001'";strCaseNo
			sql = "select subrptno  from llcaserela where CaseNo='"+strCaseNo+"'";
			
			SSRS sqlSSRS3 = tExeSQL.execSQL(sql);
			sql = "select a.SubRptNo,AccDate,AccPlace,InHospitalDate,OutHospitalDate,AccDesc from LLSubReport a where a.customerno='"+CustomerNo+"' and a.SubRptNo = '"+sqlSSRS3.GetText(1,1)+"'";
			SSRS sqlSSRS4 = tExeSQL.execSQL(sql);
			System.out.println("<-eventno-->"+ sqlSSRS3.GetText(1,1)+"pppp");
			LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
			tLLSubReportSchema.setSubRptNo(sqlSSRS3.GetText(1,1));
			tLLSubReportSchema.setCustomerNo(CustomerNo);
			tLLSubReportSchema.setCustomerName(sqlSSRS.GetText(1,3));
			tLLSubReportSchema.setAccDate(sqlSSRS4.GetText(1, 2));
			tLLSubReportSchema.setAccDesc(sqlSSRS4.GetText(1, 6));   
			tLLSubReportSchema.setAccPlace(sqlSSRS4.GetText(1, 3));  
			tLLSubReportSchema.setInHospitalDate(sqlSSRS4.GetText(1, 4));
			tLLSubReportSchema.setOutHospitalDate(sqlSSRS4.GetText(1, 5)); 
							
			tLLSubReportSet.add(tLLSubReportSchema);

		 
		  
		  try
		  {
		    //VData tVData = new VData();
		    tVData.addElement(tLLSubReportSet);
		    tVData.addElement(CaseRelaNo);
		    tVData.addElement(tLLCaseSchema);
		    tVData.addElement(tLLCaseCureSet);
		    tVData.addElement(tLLCaseInfoSet);
		    tVData.addElement(tLLOtherFactorSet);
		    tVData.addElement(tLLOperationSet);
		    tVData.addElement(tLLAccidentSet);
		    tVData.addElement(tLLDisabilitySet);
		   // tCaseInfoUI.submitData(tVData,transact);
		  
		 /////////////////////////////////////////////////////////////////////////////////////

		  //账单关联保存
		  
		    String strFeeNo=sqlSSRS2.GetText(1, 5);
		    LLFeeMainSet tLLFeeMainSet= new LLFeeMainSet();
				 
			LLFeeMainSchema tLLFeeMainSchema=new LLFeeMainSchema();
			tLLFeeMainSchema.setCaseNo(strCaseNo);
			tLLFeeMainSchema.setMainFeeNo(strFeeNo);
			tLLFeeMainSchema.setCaseRelaNo(CaseRelaNo);
			tLLFeeMainSet.add(tLLFeeMainSchema);


		    tVData.addElement(tLLFeeMainSet);
		  // }
		    tAllCaseInfoUI.submitData(tVData,transact);
		  }
		  catch(Exception ex)
		  {
		    Content = transact+"失败，原因是:" + ex.toString();
		    FlagStr = "Fail";
		    System.out.println(Content);
		    return false;
		  }
		  //如果在Catch中发现异常，则不从错误类中提取错误信息
		  if (FlagStr=="")
		  {
		    tError = tAllCaseInfoUI.mErrors;
		    if (!tError.needDealError())
		    {
		      Content = "保存成功";
		      System.out.println(Content);
		      FlagStr = "Succ";
		    }
		    else
		    {
		      Content = "保存失败，原因是:" + tError.getFirstError();
		      System.out.println(Content);
		      FlagStr = "Fail";
		      return false;
		    }
		  }
		
		return true;
	}
	
	
	/**
	 * 检录  确认按钮
	 * 后台
	 * @return
	 */
	public boolean formCheck(){
		ExeSQL tExeSQL = new ExeSQL();
		  CaseCheckUI tCaseCheckUI   = new CaseCheckUI();
		  
		  CErrors tError = null;
		  String transact = "CHECKFINISH";
		  String tRela  = "";
		  String FlagStr = "";
//		  String Content = "";
		  
//		  String strCaseNo="R3600170104000001";
		  GlobalInput tG = new GlobalInput();
//		  tG.Operator = "sc3672";
//		  tG.ManageCom = "86360500";
		  tG.Operator = "lipeia";
		  tG.ManageCom = mLLAppea.getManageCom();
		  LLCaseSchema tLLCaseSchema  = new LLCaseSchema();
		  tLLCaseSchema.setCaseNo(strCaseNo);
		  
		  VData tVData = new VData();
		  VData tVData2 = new VData();
			tVData.addElement(tG);
			tVData.addElement(tLLCaseSchema);

		  try
		  {
		  //账单关联保存
		  	if ( "FEERELASAVE".equals(transact))
		  	{/*
		  	  	String CaseRelaNo = request.getParameter("CaseRelaNo");
		  	  	String[] tChk = request.getParameterValues("InpICaseCureGridChk");
		  	  	String[] strPolNo=request.getParameterValues("ICaseCureGrid5");
		  	  	LLFeeMainSet tLLFeeMainSet= new LLFeeMainSet();
				  	if ( tChk!=null )
				  	{
						  int intLength=tChk.length;
						  for(int i=0;i<intLength;i++)
						  {
						    if(tChk[i].equals("0")) //未选
						      continue;
						
						    LLFeeMainSchema tLLFeeMainSchema=new LLFeeMainSchema();
						    tLLFeeMainSchema.setCaseNo(strCaseNo);
						    tLLFeeMainSchema.setMainFeeNo( strPolNo[i]);
						    tLLFeeMainSchema.setCaseRelaNo(CaseRelaNo);
						    tLLFeeMainSet.add(tLLFeeMainSchema);
						  }
				  	}
		  	  	tVData.addElement(tLLFeeMainSet);
		   	*/}
			  if(!tCaseCheckUI.submitData(tVData,transact)){
				  Content = tCaseCheckUI.mErrors.getErrContent();
				  FlagStr = "Fail";
				  System.out.println(Content);
			      return false;
			  }
			  System.out.println("transact:::::::::"+transact);
		  }
		  catch(Exception ex)
		  {
		    Content = transact+"失败，原因是:" + ex.toString();
		    FlagStr = "Fail";
		    System.out.println(Content);
	        return false;
		  }
		  //如果在Catch中发现异常，则不从错误类中提取错误信息
		  if (FlagStr=="")
		  {
		    tError = tCaseCheckUI.mErrors;
		    if (!tError.needDealError())
		    {
		      Content = "保存成功";
		      FlagStr = "Succ";
		/**
		保单关联
		*/
		  LLCasePolicySet tLLCasePolicySet=new LLCasePolicySet();
		  CasePolicyUI tCasePolicyUI   = new CasePolicyUI();

		  tError = null;
		  String transact1 = "INSERT";
		  
		  String sql = "select a.RgtNo,a.CustomerNo,a.CustomerName,(case a.CustomerSex when '0' then '男' when '1' then '女' when '2' then '不详' end),a.AccdentDesc,a.handler,b.codename,c.apppeoples from llcase a,ldcode b,llregister c where a.CaseNo='"+strCaseNo+"' and c.rgtno=a.rgtno and b.codetype='llrgtstate' and b.code=a.rgtstate";
		  SSRS sqlSSRS = tExeSQL.execSQL(sql);
			//读取立案明细信息
		  String strCustomerNo = sqlSSRS.GetText(1,2);
		  
		  sql = "select riskcode from llcase where caseno ='"+strCaseNo+"'";
		  SSRS sqlSSRS3 = tExeSQL.execSQL(sql);
		  
		  String strSQL0 = "select contno,InsuredName,riskcode,amnt,cvalidate,polno from ";
		  String wherePart = " where insuredno='"+ strCustomerNo +"' and appflag='1' and stateflag<>'4' ";
		  if("1605".equals(sqlSSRS3.GetText(1,1)))
		  {
		  	wherePart+=" and RiskCode='"+sqlSSRS3.GetText(1,1)+"'";
		  }
		  String strSQL = strSQL0+" lcpol "+wherePart+" union "+strSQL0+" lbpol "+wherePart;
		  SSRS aSSRS = tExeSQL.execSQL(strSQL) ; 
		  
		  String strRgtNo  = sqlSSRS.GetText(1,1);
		  sql = "select distinct a.ReceiptNo,a.HospitalName,a.FeeDate,a.CaseRelaNo,a.MainFeeNo,a.sumfee,a.HospitalCode from llfeemain a  where a.caseno='"+strCaseNo+"'";
		  SSRS sqlSSRS2 = tExeSQL.execSQL(sql);
		//  String[] strPolNo=request.getParameterValues("CasePolicyGrid6");
		  String CaseRelaNo = sqlSSRS2.GetText(1, 4);

		  int intLength=aSSRS.MaxRow ;
		  System.out.println("保存LCPOL的信息");
		  System.out.println(intLength);
		  for(int i=0;i<intLength;i++)
		  {
		    LLCasePolicySchema tLLCasePolicySchema=new LLCasePolicySchema();
		    tLLCasePolicySchema.setCaseNo(strCaseNo);
		    tLLCasePolicySchema.setRgtNo(strRgtNo);
		    System.out.println(aSSRS.GetText(i+1,6));
		    tLLCasePolicySchema.setPolNo(aSSRS.GetText(i+1,6));
		    tLLCasePolicySchema.setCaseRelaNo(CaseRelaNo);
		    tLLCasePolicySet.add(tLLCasePolicySchema);
		  }

		    GlobalInput tG1 = new GlobalInput();
//		  	tG1=(GlobalInput)session.getValue("GI");
//		  	tG1.Operator = "ss3601";
//			tG1.ManageCom = "8636";
		  	tG1.Operator = "lipeib";
			tG1.ManageCom = mLLAppea.getManageCom().substring(0, 4);
			System.out.println("yuyuyu"+tG.ManageCom);
		    try
		    {
		      VData tVData1 = new VData();
		      tVData1.addElement(strCustomerNo);
		      tVData1.addElement(strCaseNo);
		      tVData1.addElement(tLLCasePolicySet);
		      tVData1.addElement(tG1);
		      if(tCasePolicyUI.submitData(tVData1,transact1)){
		        ClaimCalBL tClaimCalBL=new ClaimCalBL();
		        try
		        {
				    	tVData2.addElement(tLLCaseSchema);
		          tVData2.addElement(tG1);
		          System.out.println("......tClaimCalBL");
		          if(!tClaimCalBL.submitData(tVData2,"autocal")){
		        	  Content = "ClaimCalBL===出错:" + tClaimCalBL.mErrors.getErrContent();
			    	  System.out.println(Content);
			    	  return false;
		          }
		        }catch(Exception ex)
		        {
		          Content = transact1+"失败，原因是:" + ex.toString();
		          FlagStr = "Fail";
		          System.out.println(Content);
			        return false;
		        }
		      }else{
		    	  Content = "CasePolicyUI===出错:" + tCasePolicyUI.mErrors.getContent();
		    	  System.out.println(Content);
		    	  return false;
		      }
		    }
		    catch(Exception ex)
		    {
		      Content = transact1+"失败，原因是:" + ex.toString();
		      FlagStr = "Fail";
		      System.out.println(Content);
		        return false;
		    }
		    if (FlagStr=="")
		    {
		      tError = tCasePolicyUI.mErrors;
		      if (!tError.needDealError())
		      {
		        Content = " 保存成功";
		        FlagStr = "Succ";
		        System.out.println(Content);
		      }
		      else
		      {
		        Content = " 保存失败，原因是:" + tError.getFirstError();
		        FlagStr = "Fail";
		        System.out.println(Content);
		        return false;
		      }
		    }  
		    }
		    else
		    {
		      Content = "保存失败，原因是:" + tError.getFirstError();
		      System.out.println(Content);
		      FlagStr = "Fail";
		      return false;
		    }
		  }
		
		return true;
	}
	
	public static void main(String[] args) {
		CheckInput test = new CheckInput();
		test.save();
		test.formCheck();
	}
}
