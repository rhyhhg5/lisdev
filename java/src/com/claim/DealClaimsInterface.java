package com.claim;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.claimback.ClaimBackParse;
import com.claimback.appealCorrection.ClaimSPSDBL;
import com.claimt.DealClaimParset;
import com.claimt.approve.ApproveInterface;
import com.claimt.approve.GrpContLockInterface;
import com.claimt.grpper.Individual;
import com.claimt.modify.ModifyInterface;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubFun;
/**
 * 接收接口发送的报文及返回
 * @author 樊庆
 * 2016.11.18
 *
 */
public class DealClaimsInterface {
	/**
	 * 
	 * 接口方法，接收报文，返回报文
	 * 
	 * @param xml
	 * @return
	 */
	String mUrl = "";
	String aOutXmlStr = "";
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String fileName = sdf.format(date);
	public String service(String xml) throws Exception{
		//解析报文头
		try {
			
			Document doc = DocumentHelper.parseText(xml);
			
			Element root = doc.getRootElement();
			
			Element tHead = root.element("head");
			
			String tMsgType = tHead.elementText("MsgType");
			
			//保存请求报文
			 getUrlName("1");
			 String InFilePath = mUrl+fileName+tMsgType+".xml";
			 FileWriter fw=new FileWriter(InFilePath);
			 fw.write(xml);         
			 fw.flush();          
			 fw.close(); 
			 
			//理赔纠错接口   樊庆    2017-02-14
			if ("DealClaimBack".equals(tMsgType)) {
				// 解析报文
				ClaimBackParse tClaimBackParse = new ClaimBackParse();
				tClaimBackParse.parseXml(xml);
				// 返回报文
				aOutXmlStr=tClaimBackParse.getResponseXml();
				// 保存返回 报文  
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return tClaimBackParse.getResponseXml();
			} else if ("11".equals(tMsgType)) {// 团体理赔接口
				
				//全流程理赔（同步）
				DealClaimParset dealClaimParset = new DealClaimParset();
				dealClaimParset.parseXml(xml);
				// 返回报文
				aOutXmlStr = dealClaimParset.getResponseXml();
				// 保存返回 报文
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
				
//				DealClaimParse tDealClaimParse = new DealClaimParse();
//				tDealClaimParse.parseXml(xml);
//				// 返回报文
//				aOutXmlStr = tDealClaimParse.getResponseXml();
//				System.out.println("返回报文信息===\t" + aOutXmlStr);
//				// 保存返回 报文
//				getUrlName("2");
//				Date date = new Date();
//				String fileName = sdf.format(date);
//				String OutFilePath = mUrl + fileName + tMsgType + ".xml";
//				FileWriter fw1 = new FileWriter(OutFilePath);
//				fw1.write(aOutXmlStr);
//				fw1.flush();
//				fw1.close();
//				return aOutXmlStr;
			}else if("ClaimGrpAppAcc".equals(tMsgType)){	//理赔:团体受理申请
				System.out.println("进入新理赔流程（团体受理申请）------郭泽峰");
				com.claimt.appeal.DealClaimParset dealClaimParset = new com.claimt.appeal.DealClaimParset();
				dealClaimParset.parseXml(xml);
				// 返回报文 
				aOutXmlStr = dealClaimParset.getResponseXml();
				System.out.println("返回报文信息===\t" + aOutXmlStr);
				// 保存返回 报文
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
			}else if("AG".equals(tMsgType)) {
				ApproveInterface approveInterface=new ApproveInterface();
				aOutXmlStr= approveInterface.service(xml);
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
			}else if("11t".equals(tMsgType)){
				//全流程理赔（同步）
				DealClaimParset dealClaimParset = new DealClaimParset();
				dealClaimParset.parseXml(xml);
				// 返回报文
				aOutXmlStr = dealClaimParset.getResponseXml();
				// 保存返回 报文
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
			}else if ("modify".equals(tMsgType)) {//理赔案件-团体理赔处理-团体付费信息修改
				System.out.println("===================开始解析====================");
				ModifyInterface modifyInterface = new ModifyInterface();
				System.out.println("=============调用解析报文方法并获取返回报文============");
				aOutXmlStr = modifyInterface.service(xml);
				//保存返回报文
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
			}else if("GRPBC".equals(tMsgType)) {
				Individual individual=new Individual();
				individual.deal(xml);
				aOutXmlStr= individual.getResponseXml();
				// 保存返回 报文
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
			
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
			}else if("GRPLOCK".equals(tMsgType)) {
				GrpContLockInterface grpContLockInterface=new GrpContLockInterface();
				grpContLockInterface.deal(xml);
				aOutXmlStr= grpContLockInterface.getResponseXml();
				// 保存返回 报文
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
			
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
			}else if("ClaimBackSplit".equals(tMsgType)) {
				com.claimback.appealCorrection.ClaimBackParse claimBackParse=new com.claimback.appealCorrection.ClaimBackParse();
				claimBackParse.parseXml(xml);
				aOutXmlStr= claimBackParse.getResponseXml();
				// 保存返回 报文
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";						
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
			}else if("RECOVERY".equals(tMsgType)) {
				ClaimSPSDBL claimSPSDBL=new ClaimSPSDBL();
				claimSPSDBL.parseXml(xml);
				aOutXmlStr= claimSPSDBL.getResponseXml();
				// 保存返回 报文
				getUrlName("2");
				Date date = new Date();
				String fileName = sdf.format(date);
				String OutFilePath = mUrl  + fileName + tMsgType + ".xml";
			
				FileWriter fw1 = new FileWriter(OutFilePath);
				fw1.write(aOutXmlStr);
				fw1.flush();
				fw1.close();
				return aOutXmlStr;
			}else{
				return null;
			}
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		DealClaimParse tDealClaimParse = new DealClaimParse();
		tDealClaimParse.parseXml(xml);
		//返回报文
		System.out.println("返回报文信息===\t"+tDealClaimParse.getResponseXml());
		return "操作失败，请检查报文！";
	}
	
	/**
     * getUrlName
     * 获取文件存放路径
     * @return boolean
     */
    private  boolean getUrlName(String type){
    	
    	 String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='SYYBPath'";//发盘报文的存放路径
         String fileFolder = new ExeSQL().getOneValue(sqlurl);
         if (fileFolder == null || fileFolder.equals("")){
        	 aOutXmlStr = "getFileUrlName,获取文件存放路径出错";
             return false;
         }
          if(type.equals("1")){
        	 mUrl =fileFolder + "LPRequest/"+PubFun.getCurrentDate2()+"/";
         }else{
        	 mUrl =fileFolder + "LPRsponse/"+PubFun.getCurrentDate2()+"/";
         }
         if (!newFolder(mUrl)){
        	 return false;
         }
         return true;
         
    }
    
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath){
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try{
            if (myFilePath.isDirectory()){
                System.out.println("目录已存在,文件路径为:"+myFilePath);
                return true;
            }else{
                myFilePath.mkdirs();
                System.out.println("新建目录成功,文件路径为:"+myFilePath);
                return true;
            }
        }catch (Exception e){
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
}

