package com.claim;

//import com.claims.DealClaim;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.SocketException;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.sinosoft.lis.llcase.BatchClaimCalBL;
import com.sinosoft.lis.llcase.GrpContInvalidateUI;
import com.sinosoft.lis.llcase.GrpGiveEnsureBL;
import com.sinosoft.lis.llcase.GrpRegisterBL;
import com.sinosoft.lis.llcase.LJAGetInsertBL;
import com.sinosoft.lis.llcase.LLContAutoDealBL;
import com.sinosoft.lis.llcase.LLImportCaseInfoNew;
import com.sinosoft.lis.llcase.LPSecondUWBL;
import com.sinosoft.lis.llcase.QDGrpGiveEnsureBL;
import com.sinosoft.lis.llcase.SimpleClaimAuditBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLPersonMsgSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSendMsgSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLPersonMsgSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealClaim {
	private LLSendMsgSchema mLLSendMsgSchema = new LLSendMsgSchema();
	private LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
	private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
	LLPersonMsgSet mLLPersonMsgSet = new LLPersonMsgSet();
	LLPersonMsgSchema mLLPersonMsgSchema = new LLPersonMsgSchema();
	private VData mResult = new VData();

	// 单例模式
	// private DealClaim(){}
	// private static final DealClaim tDealClaim = new DealClaim();
	// public static synchronized DealClaim getInstance() {
	// return tDealClaim;
	// }

	public void setmResult(VData mResult) {
		this.mResult = mResult;
	}

	public VData getmResult() {
		return mResult;
	}

	private MMap mMap = new MMap();
	private String responseXml = "";// 返回报文

	public String getResponseXml() {
		return responseXml;
	}

	GlobalInput tG = new GlobalInput();

	String FlagStr = "";
	String Content = "";
	CErrors tError = null;

	public boolean submitDate(VData cInputData, String cOperate) {
		String requestXml = "";
		try {
			// 获取请求报文
			// MMap tmap = (MMap) cInputData.getObjectByObjectName("MMap", 0);
			// requestXml = (String) tmap.get("requestXML");
			requestXml = (String) cInputData.getObjectByObjectName("String", 0);
			// mMap.put("requestXML", requestXml);
			System.out.println("请求报文： \n" + requestXml);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 获取数据
		if (!getInputData(cInputData)) {
			return false;
		}

		VData vdata = new VData();
		MMap mmap = new MMap();
		String insertClaimInsertLogSql = "  insert into ClaimInsertLog (Id ,Caller ,startDate ,Starttime ,endDate ,endtime ,tradingState ,betweenness ,transactionType ,transactionCode ,transactionBatch ,  transactionDescription,   transactionAddress, grpcontno, rgtno ) values (lpad(SEQ_CLAIM_BATCHLOG.Nextval, 20, '0'),'"
				+ mLLSendMsgSchema.getOperate()
				+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'0','中间状态','"
				+ mLLSendMsgSchema.getMsgType()
				+ "','"
				+ mLLSendMsgSchema.getBranchCode()
				+ "','"
				+ mLLSendMsgSchema.getBatchNo()
				+ "','交易描述','交易地址','"
				+ mLLSendMsgSchema.getApplno() + "','')";
		mmap.put(insertClaimInsertLogSql, "INSERT");
		vdata.add(mmap);
		PubSubmit pub = new PubSubmit();
		pub.submitData(vdata, "INSERT");

		if (!saveGrpContNo(cOperate))// 可导入数据列表维护 校验保存
		{
			VData tdata = new VData();
			MMap map = new MMap();
			String resultXml = getXmlResult(false, Content);
			String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+ mLLSendMsgSchema.getOperate()
					+ "','"
					+ requestXml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ resultXml
					+ "','中间状态','"
					+ Content
					+ "','"
					+ mLLSendMsgSchema.getMsgType()
					+ "','"
					+ mLLSendMsgSchema.getBranchCode()
					+ "','"
					+ mLLSendMsgSchema.getBatchNo() + "','交易描述','交易地址')";
			map.put(insertErrorInsertLogSql, "INSERT");
			tdata.add(map);
			PubSubmit pubt = new PubSubmit();
			pubt.submitData(tdata, "INSERT");
			applyDelete();// 数据回滚
			return false;
		}

		if (!"成功导入".equals(ftpDownload()))// ftp下载
		{
			VData tdata = new VData();
			MMap map = new MMap();
			String resultXml = getXmlResult(false, Content);
			String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+ mLLSendMsgSchema.getOperate()
					+ "','"
					+ requestXml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ resultXml
					+ "','中间状态','"
					+ Content
					+ "','"
					+ mLLSendMsgSchema.getMsgType()
					+ "','"
					+ mLLSendMsgSchema.getBranchCode()
					+ "','"
					+ mLLSendMsgSchema.getBatchNo() + "','交易描述','交易地址')";
			map.put(insertErrorInsertLogSql, "INSERT");
			tdata.add(map);
			PubSubmit pubt = new PubSubmit();
			pubt.submitData(tdata, "INSERT");
			// importCustomerDelete();
			applyDelete();// 数据回滚
			return false;
		} else {
			if (!importPersonnel()) {// 上传人员
				VData tdata = new VData();
				MMap map = new MMap();
				String resultXml = getXmlResult(false, Content);
				String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
						+ mLLSendMsgSchema.getOperate()
						+ "','"
						+ requestXml
						+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
						+ resultXml
						+ "','中间状态','"
						+ Content
						+ "','"
						+ mLLSendMsgSchema.getMsgType()
						+ "','"
						+ mLLSendMsgSchema.getBranchCode()
						+ "','"
						+ mLLSendMsgSchema.getBatchNo() + "','交易描述','交易地址')";
				map.put(insertErrorInsertLogSql, "INSERT");
				tdata.add(map);
				PubSubmit pubt = new PubSubmit();
				pubt.submitData(tdata, "INSERT");
				importCustomerDelete();
				applyDelete();// 数据回滚
				return false;
			}
		}

		setmResult(cInputData);
		// mResult.add(mLLSendMsgSchema);
		mResult.add(mLLPersonMsgSet);
		// mResult.add(mMap);
		mResult.add(mLLRegisterSchema);
		mResult.add(tLLAppClaimReasonSchema);

		/*
		 * VData adata = new VData(); MMap amap = new MMap(); String
		 * updateClaimInsertLogSql =
		 * "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='' WHERE transactionCode = '"
		 * +mLLSendMsgSchema.getBranchCode()+"' and transactionBatch='"+
		 * mLLSendMsgSchema.getBatchNo()+"' "; amap.put(updateClaimInsertLogSql,
		 * "UPDATE"); adata.add(amap); PubSubmit puba = new PubSubmit();
		 * puba.submitData(adata, "UPDATE");
		 */
		Content = "导入人员信息成功！";
		getXmlResult(true, Content);
		return true;
	}

	/**
	 * 多线程调用，导入人员之后的操作
	 * 
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitDate2(VData cInputData) {
		String requestXml = "";
		try {
			// 获取请求报文
			// MMap tmap = (MMap) cInputData.getObjectByObjectName("MMap", 0);
			// requestXml = (String) tmap.getObjectByObjectName("requestXML",
			// 0);
			requestXml = (String) cInputData.getObjectByObjectName("String", 0);
			System.out.println("请求报文： \n" + requestXml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 获取数据
		if (!getInputData2(cInputData)) {
			return false;
		}

		String sql = "select VARCHAR(Responsexml) from ErrorInsertLog where transactionBatch = '"
				+ mLLSendMsgSchema.getBatchNo()
				+ "' and transactionCode = '"
				+ mLLSendMsgSchema.getBranchCode() + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sql1SSRS = tExeSQL.execSQL(sql);
		if (sql1SSRS.getMaxRow() > 0) {
			if (sql1SSRS.GetText(1, 1) != null
					&& !"".equals(sql1SSRS.GetText(1, 1))) {
				System.out.println("导入人员失败，不进行后续操作！");
				Content = "导入人员失败，不进行后续操作！";
				String resultXml = getXmlResult(false, Content);
				importCustomerDelete();
				applyDelete();// 数据回滚
				return false;
			}
		}

		if (!giveBatch())// 理算到给付确认的校验及数据存储
		{
			VData tdata = new VData();
			MMap map = new MMap();
			String resultXml = getXmlResult(false, Content);
			String insertErrorInsertLogSql = "  insert into ErrorInsertLog (Id ,Caller ,Requestxml ,startDate ,Starttime ,endDate ,endtime ,Responsexml ,betweenness ,ErrorReason ,transactionType ,transactionCode ,transactionBatch ,transactionDescription ,transactionAddress ) values (lpad(SEQ_CLAIM_ERRORLOG.Nextval, 20, '0'),'"
					+ mLLSendMsgSchema.getOperate()
					+ "','"
					+ requestXml
					+ "',to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'), to_char(sysdate, 'HH24:mi:ss'),'"
					+ resultXml
					+ "','中间状态','"
					+ Content
					+ "','"
					+ mLLSendMsgSchema.getMsgType()
					+ "','"
					+ mLLSendMsgSchema.getBranchCode()
					+ "','"
					+ mLLSendMsgSchema.getBatchNo() + "','交易描述','交易地址')";
			map.put(insertErrorInsertLogSql, "INSERT");
			tdata.add(map);
			PubSubmit pubt = new PubSubmit();
			pubt.submitData(tdata, "INSERT");
			deleteData();
			deleteCal();
			importCustomerDelete();
			applyDelete();// 数据回滚
			return false;// 给付确认 后台
		}

		VData adata = new VData();
		MMap amap = new MMap();
		String updateClaimInsertLogSql = "  UPDATE ClaimInsertLog SET endDate = to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd') , endtime=to_char(sysdate, 'HH24:mi:ss') , tradingState='1' , betweenness='',rgtno='"
				+ mLLRegisterSchema.getRgtNo()
				+ "' WHERE transactionCode = '"
				+ mLLSendMsgSchema.getBranchCode()
				+ "' and transactionBatch='"
				+ mLLSendMsgSchema.getBatchNo() + "' ";
		String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
		String insertReconcileInfoSql = " INSERT INTO reconcileInfo SELECT lpad(to_char((nextval for SEQ_RECOINFOID)), 20, '0'), actugetno, paymode, otherno, othernotype,'','0', '', '', '1', '成功', (select EndCaseDate from llregister where rgtno='"
				+ mLLRegisterSchema.getRgtNo()
				+ "'), '00:00:00', NULL, NULL, TO_CHAR(sysdate, 'yyyy-mm-dd'), TO_CHAR(sysdate, 'HH24:mi:ss'), NULL, NULL, '"
				+ mLLSendMsgSchema.getOperate()
				+ "', '"
				+ mLLSendMsgSchema.getManageCom()
				+ "', '"
				+ mLLRegisterSchema.getRgtNo()
				+ "', 'lipei', 'get', '', '' FROM ljaget WHERE actugetno IN ( SELECT actugetno FROM ljagetclaim WHERE otherno IN ( SELECT caseno FROM llcase  WHERE RgtNo ='"
				+ mLLRegisterSchema.getRgtNo() + "')) ";
		amap.put(updateClaimInsertLogSql, "UPDATE");
		amap.put(insertReconcileInfoSql, "INSERT");
		adata.add(amap);
		PubSubmit puba = new PubSubmit();
		puba.submitData(adata, "UPDATE");
		Content = "successful";
		getXmlResult(true, Content);

		// String sql =
		// "update LLSocialClaimUser set HandleFlag = '0' where  UserCode='cm0002'";
		// ExeSQL tExeSQL = new ExeSQL();
		// tExeSQL.execUpdateSQL(sql);

		return true;
	}

	/**
	 * 获取数据到实体类
	 * 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		// TODO Auto-generated method stub
		mLLSendMsgSchema = null;
		mLLPersonMsgSet = null;
		System.out
				.println("===========  This is getInputData() Start  ==============");
		mLLSendMsgSchema = (LLSendMsgSchema) cInputData.getObjectByObjectName(
				"LLSendMsgSchema", 0);
		mLLPersonMsgSet = (LLPersonMsgSet) cInputData.getObjectByObjectName(
				"LLPersonMsgSet", 0);
		System.out.println(mLLPersonMsgSet.size());
		System.out
				.println("===========  This is getInputData() End  ===============");
		return true;
	}

	/**
	 * 获取数据到实体类
	 * 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData2(VData cInputData) {
		// TODO Auto-generated method stub
		System.out
				.println("===========  This is getInputData2() Start  ==============");
		mLLSendMsgSchema = null;
		mLLPersonMsgSet = null;
		mLLRegisterSchema = null;
		tLLAppClaimReasonSchema = null;
		mLLSendMsgSchema = (LLSendMsgSchema) cInputData.getObjectByObjectName(
				"LLSendMsgSchema", 0);
		// mLLPersonMsgSet =
		// (LLPersonMsgSet)cInputData.getObjectByObjectName("LLPersonMsgSet",
		// 0);
		mLLRegisterSchema = (LLRegisterSchema) cInputData
				.getObjectByObjectName("LLRegisterSchema", 0);
		mLLPersonMsgSet = (LLPersonMsgSet) cInputData.getObjectByObjectName(
				"LLPersonMsgSet", 0);
		tLLAppClaimReasonSchema = (LLAppClaimReasonSchema) cInputData
				.getObjectByObjectName("LLAppClaimReasonSchema", 0);
		System.out.println("人数： " + mLLPersonMsgSet.size());
		System.out
				.println("===========  This is getInputData2() End  ===============");
		return true;
	}

	/**
	 * 团体理赔处理--》可导入数据列表维护--》保存 前台校验
	 */
	public boolean saveGrpContNo(String cOperate) {
		System.out
				.println("===========  This is saveGrpContNo() Start  ================");
		if ("".equals(mLLSendMsgSchema.getApplno())
				|| null == mLLSendMsgSchema.getApplno()) {
			Content = "请输入团体保单号";
			System.out.println("请输入团体保单号");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getOperate())
				|| null == mLLSendMsgSchema.getOperate()) {
			Content = "操作者所属的机构不能为空";
			System.out.println("操作者所属的机构不能为空");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getManageCom())
				|| null == mLLSendMsgSchema.getManageCom()) {
			Content = "操作者所属的机构不能为空";
			System.out.println("操作者所属的机构不能为空");
			return false;
		}
		ExeSQL tExeSQL = new ExeSQL();
		String sql1 = "select * from lcgrpcont where grpcontno='"
				+ mLLSendMsgSchema.getApplno() + "' and managecom like '"
				+ mLLSendMsgSchema.getManageCom() + "%' ";
		// String
		// sql2="select * from lbgrpcont where grpcontno='"+mLLSendMsgSchema.getApplno()+"' and managecom like '"+mLLSendMsgSchema.getManageCom()+"%' ";
		SSRS sql1SSRS = tExeSQL.execSQL(sql1);
		System.out.println(sql1);
		// SSRS sql2SSRS = tExeSQL.execSQL(sql2);
		// System.out.println(sql2);
		if (sql1SSRS.getMaxNumber() <= 0) {
			String sql3 = "select * from lccont where contno='"
					+ mLLSendMsgSchema.getApplno() + "' ";
			SSRS sql3SSRS = tExeSQL.execSQL(sql3);
			if (sql3SSRS.getMaxRow() > 0) {
				Content = mLLSendMsgSchema.getApplno() + "该保单号为个单保单号";
				System.out.println(mLLSendMsgSchema.getApplno() + "该保单号为个单保单号");
				return false;
			} else {
				Content = mLLSendMsgSchema.getManageCom() + "该机构下不存在保单号："
						+ mLLSendMsgSchema.getApplno();
				System.out.println("该机构下不存在保单号 :"
						+ mLLSendMsgSchema.getApplno());
				return false;
			}
		}

		if (!"SUCCESS".equals(claimInfo(cOperate))) {// 保存
			return false;
		}
		Content = "成功";
		System.out
				.println("===========  This is saveGrpContNo() End  ================");
		return true;
	}

	/**
	 * 可导列表维护以及团体受理申请后台调用
	 * 
	 * @param cOperate
	 * @return
	 */
	public String claimInfo(String cOperate) {
		// 1、可导列表数据的校验，首先先查询是否存在，不存在执行if语句里面的步骤
		System.out
				.println("===========  This is claimInfo() Start  ===============");
		String strSql = " SELECT RGTNO,MngCom,operator, makedate,maketime,modifydate, modifytime"
				+ " FROM LLAppClaimReason l  "
				+ "WHERE reasontype='9' and reasoncode='99' "
				+ " and l.mngcom like '"
				+ mLLSendMsgSchema.getManageCom()
				+ "%' and l.RGTNO='" + mLLSendMsgSchema.getApplno() + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(strSql);
		System.out.println(strSql);
		// tG.Operator = mLLSendMsgSchema.getOperate();
		// tG.ComCode = "8615";
		tG.ManageCom = mLLSendMsgSchema.getManageCom();
		tG.ManageCom = mLLSendMsgSchema.getManageCom();
		if ("86360500".equals(tG.ManageCom)) {
			tG.Operator = "sc3672";
		}
		if (tSSRS.getMaxNumber() <= 0) {
			// 可导列表不存在，进行保存的操作

			VData tVData = new VData();
			tVData.add(tG);

			tLLAppClaimReasonSchema.setRgtNo(mLLSendMsgSchema.getApplno());
			tLLAppClaimReasonSchema.setOperator(mLLSendMsgSchema.getOperate());
			tLLAppClaimReasonSchema.setCaseNo("保单验证");// 跟甲方沟通
			tLLAppClaimReasonSchema.setReasonCode("99");
			tLLAppClaimReasonSchema.setReasonType("9");
			tLLAppClaimReasonSchema.setCustomerNo("88888888");
			tVData.add(tLLAppClaimReasonSchema);

			String strOperate = "insert";// 校验传入后台要走哪个流程的标识
			GrpContInvalidateUI tGrpContInvalidateUI = new GrpContInvalidateUI();
			try {
				tGrpContInvalidateUI.submitData(tVData, strOperate);
			} catch (Exception ex) {
				Content = "可导入数据列表操作失败，原因是:" + ex.toString();
				System.out.println(Content);
				FlagStr = "Fail";
				return Content;
			}
			if (!FlagStr.equals("Fail")) {
				tError = tGrpContInvalidateUI.mErrors;
				if (!tError.needDealError()) {
					Content = " 可导入数据列表操作成功! ";
					System.out.println(Content);
					FlagStr = "Succ";
				} else {
					Content = " 可导数据列表维护步骤保存失败，请检查数据是否正确";
					FlagStr = "Fail";
					return Content;
				}
			}
		}
		// 2、可导列表已经存在，则进行受理申请确认的操作，此时可以系统自动生成团体批次号
		if (!AskSave())// 团体理赔处理--》团体理赔处理--》确认 前台 校验
		{
			return "";
		}
		VData tVData = new VData();
		GrpRegisterBL tGrpRegisterBL = new GrpRegisterBL();
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();

		// String strOperate = mLLSendMsgSchema.getApplyConfirm();
		// 根据保单号码查询出相应的几个信息
		String sql1 = "select  a.customerno, a.name, g.grpcontno,g.Peoples2, a.claimbankcode, a.claimbankaccno, a.claimaccname, a.AddressNo,'', b.riskcode from lcgrpcont g, LCGrpAppnt a,lcgrppol b where '1480490147000'='1480490147000' and  a.grpcontno = g.grpcontno and g.appflag = '1' and b.grpcontno=g.grpcontno  and g.GrpContNo='"
				+ mLLSendMsgSchema.getApplno()
				+ "'  fetch first 3000 rows only with ur ";
		ExeSQL ttExeSQL = new ExeSQL();
		SSRS sql1SSRS = ttExeSQL.execSQL(sql1);
		// 团体号
		String CustomerNo = "";
		// 单位名称
		String GrpName = "";
		// 投保人数
		String PeopleNo = "";
		String addressno = "";
		if (sql1SSRS.getMaxNumber() > 0) {
			CustomerNo = sql1SSRS.GetText(1, 1);
			GrpName = sql1SSRS.GetText(1, 2);
			PeopleNo = sql1SSRS.GetText(1, 4);
			addressno = sql1SSRS.GetText(1, 8);
		} else {
			Content = "保单号码输入有误";
			System.out.println("保单号码输入有误");
			return Content;
		}
		String sql2 = "select linkman1,phone1,GrpAddress from LCGrpAddress where '1480490147000'='1480490147000' and  customerno = '"
				+ CustomerNo
				+ "' and addressno = '"
				+ addressno
				+ "' fetch first 3000 rows only with ur ";
		SSRS sql2SSRS = ttExeSQL.execSQL(sql2);
		// 联系人
		String linkman1 = "";
		// 联系电话
		String phone1 = "";
		// 联系地址
		String grpAddress = "";
		if (sql2SSRS.getMaxNumber() > 0) {
			linkman1 = sql2SSRS.GetText(1, 1);
			phone1 = sql2SSRS.GetText(1, 2);
			grpAddress = sql2SSRS.GetText(1, 3);
		} else {
			Content = "通过团体号查询团体信息失败";
			System.out.println("通过团体号查询团体信息失败");
			return Content;
		}
		// String PrePaidFlag = "1";
		if (cOperate.equals("INSERT||MAIN")) {
			tLLRegisterSchema.setRgtObj("0"); // 号码类型
			tLLRegisterSchema.setRgtNo(""); //
			tLLRegisterSchema.setRgtType(mLLSendMsgSchema.getAcceptMethod()); // 申请类型
			tLLRegisterSchema.setRgtObjNo(mLLSendMsgSchema.getApplno()); // 团单合同号
			tLLRegisterSchema.setCustomerNo(CustomerNo); // 团体客户号
			tLLRegisterSchema.setGrpName(GrpName); // 单位名称
			tLLRegisterSchema.setAppAmnt(mLLSendMsgSchema.getDeclareAmount()); // 预计申请金额
			tLLRegisterSchema.setAppPeoples(mLLSendMsgSchema.getAppPeoples()); // 申请人数
			tLLRegisterSchema.setInputPeoples(mLLSendMsgSchema.getAppPeoples()); // 录入申请人数
			tLLRegisterSchema
					.setAccidentCourse(mLLSendMsgSchema.getGrpRemark());
			tLLRegisterSchema.setTogetherFlag(mLLSendMsgSchema.getPayMethod()); // 统一给付标志
			tLLRegisterSchema.setRgtClass("1");
			tLLRegisterSchema.setPostCode(mLLSendMsgSchema.getPostCode()); // 邮政编码，对应表中立案人/申请人邮政编码“申请人邮政编码”
			tLLRegisterSchema.setRgtantAddress(grpAddress); // 联系地址，对应表中“申请人地址”
			tLLRegisterSchema.setRgtantPhone(phone1); // 联系电话，对应表中是“申请人电话”
			tLLRegisterSchema.setRgtantName(linkman1); // 联系人
			/*
			 * tLLRegisterSchema.setBankCode(mLLSendMsgSchema.getBankCode());//银行编码
			 * tLLRegisterSchema
			 * .setBankAccNo(mLLSendMsgSchema.getAccount());//银行账户
			 * tLLRegisterSchema
			 * .setAccName(mLLSendMsgSchema.getAccountName());//账户名
			 */tLLRegisterSchema.setIDType(mLLSendMsgSchema.getIDType());// 证件类型
			tLLRegisterSchema.setIDNo(mLLSendMsgSchema.getIDNo());// 证件号码
			tLLRegisterSchema.setGetMode(mLLSendMsgSchema.getReceiveMethod());// 赔付金领取方式
			tLLRegisterSchema.setCaseGetMode(mLLSendMsgSchema
					.getReceiveMethod());// 赔款领取方式
			if ("1".equals(mLLSendMsgSchema.getReceiveMethod())
					|| "2".equals(mLLSendMsgSchema.getReceiveMethod())) {
				tLLRegisterSchema.setBankCode("");// 银行编码
													// 当赔款领取方式为现金的时候界面没有此框所以存空
				tLLRegisterSchema.setBankAccNo("");// 银行账户
													// 当赔款领取方式为现金的时候界面没有此框所以存空
				tLLRegisterSchema.setAccName("");// 账户名 当赔款领取方式为现金的时候界面没有此框所以存空
			} else {
				tLLRegisterSchema.setBankCode(mLLSendMsgSchema.getBankCode());// 银行编码
				tLLRegisterSchema.setBankAccNo(mLLSendMsgSchema.getAccount());// 银行账户
				tLLRegisterSchema.setAccName(mLLSendMsgSchema.getAccountName());// 账户名
			}

			tLLRegisterSchema.setRemark(mLLSendMsgSchema.getRemark());// 备注
			tLLRegisterSchema.setApplyerType("5");// 申请人身份

			tLLRegisterSchema
					.setRgtantMobile(mLLSendMsgSchema.getPhoneNumber());// 立案/申请人手机
			tLLRegisterSchema.setEmail(mLLSendMsgSchema.getEMail());// 申请人电邮
			System.out.println("==== " + tLLRegisterSchema.getRgtObjNo());
			if ("1".equals(mLLSendMsgSchema.getAdvanceFlag().trim())) {
				tLLRegisterSchema.setPrePaidFlag("1"); // 0或null 不使用预付回销 1-预付回销
			} else {
				tLLRegisterSchema.setPrePaidFlag(""); // 0或null 不使用预付回销 1-预付回销
			}
			try {
				tVData.add(tLLRegisterSchema);
				tVData.add(tG);
				boolean submitData = tGrpRegisterBL
						.submitData(tVData, cOperate);
				if (submitData) {
					mLLRegisterSchema = (LLRegisterSchema) tGrpRegisterBL
							.getResult().get(0);
					mLLRegisterSchema.getRgtNo();
				}

				// if(b==false)
				// {
				// //如果有问题的话，要回滚，把之前已经插入数据表的数据删除
				// String rr;
				// }
			} catch (Exception ex) {
				Content = "团体受理申请步骤保存失败，原因是:" + ex.toString();
				System.out.println("aaaa" + ex.toString());
				ex.printStackTrace();
				FlagStr = "Fail";
				return Content;
			}
			if (FlagStr == "") {
				tError = tGrpRegisterBL.mErrors;
				if (!tError.needDealError()) {
					Content = "团体受理申请步骤保存成功！";
					FlagStr = "Succ";
					tVData.clear();
					tVData = tGrpRegisterBL.getResult();
				} else {
					Content = "团体受理申请步骤保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return Content;
				}
			}
			// mLLRegisterSchema.setSchema((LLRegisterSchema) tVData
			// .getObjectByObjectName("LLRegisterSchema", 0));
			System.out.println("RgtNo==== " + mLLRegisterSchema.getRgtNo());
		}

		System.out
				.println("===========  This is claimInfo() End  =================");
		return "SUCCESS";
	}

	/**
	 * 团体理赔处理--》团体理赔处理--》确认 前台校验
	 */
	public boolean AskSave() {
		System.out
				.println("===========  This is AskSave() Start  =============");
		if ("".equals(mLLSendMsgSchema.getOperate())
				|| null == mLLSendMsgSchema.getOperate()) {
			Content = "操作者所属的机构不能为空";
			System.out.println("操作者所属的机构不能为空");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getManageCom())
				|| null == mLLSendMsgSchema.getManageCom()) {
			Content = "操作者所属的机构不能为空";
			System.out.println("操作者所属的机构不能为空");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getApplno())
				|| null == mLLSendMsgSchema.getApplno()) {
			Content = "保单号码不能为空";
			System.out.println("保单号码不能为空");
			return false;
		}
		String sql1 = "select  a.customerno, a.name, g.grpcontno,g.Peoples2, a.claimbankcode, a.claimbankaccno, a.claimaccname, a.AddressNo,'', b.riskcode from lcgrpcont g, LCGrpAppnt a,lcgrppol b where '1480490147000'='1480490147000' and  a.grpcontno = g.grpcontno and g.appflag = '1' and b.grpcontno=g.grpcontno  and g.GrpContNo='"
				+ mLLSendMsgSchema.getApplno()
				+ "'  fetch first 3000 rows only with ur ";
		ExeSQL ttExeSQL = new ExeSQL();
		SSRS sql1SSRS = ttExeSQL.execSQL(sql1);
		// 团体号
		String CustomerNo = "";
		// 投保人数
		String PeopleNo = "";
		// 单位名称
		String GrpName = "";
		String addressno = "";
		if (sql1SSRS.getMaxNumber() > 0) {
			CustomerNo = sql1SSRS.GetText(1, 1);
			PeopleNo = sql1SSRS.GetText(1, 4);
			GrpName = sql1SSRS.GetText(1, 2);
			addressno = sql1SSRS.GetText(1, 8);
		} else {
			Content = "保单号码：" + mLLSendMsgSchema.getApplno() + "输入有误";
			System.out.println(mLLSendMsgSchema.getApplno() + "保单号码输入有误");
			return false;
		}
		String sql2 = "select linkman1,phone1,GrpAddress from LCGrpAddress where '1480490147000'='1480490147000' and  customerno = '"
				+ CustomerNo
				+ "' and addressno = '"
				+ addressno
				+ "' fetch first 3000 rows only with ur ";
		SSRS sql2SSRS = ttExeSQL.execSQL(sql2);
		String linkman1 = "";
		String phone1 = "";
		if (sql2SSRS.getMaxNumber() > 0) {
			linkman1 = sql2SSRS.GetText(1, 1);
			phone1 = sql2SSRS.GetText(1, 2);
		} else {
			Content = "通过团体号查询团体信息失败";
			System.out.println("通过团体号查询团体信息失败");
			return false;
		}
		if ("".equals(CustomerNo) || null == CustomerNo) {
			Content = "团体号不能为空";
			System.out.println("团体号不能为空");
			return false;
		}
		if ("".equals(PeopleNo) || null == PeopleNo) {
			Content = "投保人数不能为空";
			System.out.println("投保人数不能为空");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getAcceptMethod())
				|| null == mLLSendMsgSchema.getAcceptMethod()) {
			Content = "受理方式不能为空";
			System.out.println("受理方式不能为空");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getAppPeoples())) {
			Content = "申请人数不能为空";
			System.out.println("申请人数不能为空");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getPayMethod())
				|| null == mLLSendMsgSchema.getPayMethod()) {
			Content = "给付方式不能为空";
			System.out.println("给付方式不能为空");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getDeclareAmount())
				|| null == mLLSendMsgSchema.getDeclareAmount()) {
			Content = "申报金额不能为空";
			System.out.println("申报金额不能为空");
			return false;
		}
		// 校验团体保单特约信息 长度
		System.out.println(mLLSendMsgSchema.getGrpRemark().length());
		if (mLLSendMsgSchema.getGrpRemark().length() > 1600) {
			Content = "团体保单特约信息超过1600个汉字,无法保存!";
			System.out.println("团体保单特约信息超过1600个汉字,无法保存!");
			return false;
		}
		// 校验是否存在预付未回销赔款
		String PrepaidBalaSql = "select PrepaidBala from LLPrepaidGrpCont a where grpcontno='"
				+ mLLSendMsgSchema.getApplno() + "'";
		ExeSQL tExeSQL = new ExeSQL();
		String tPrepaidBala = tExeSQL.getOneValue(PrepaidBalaSql);
		System.out.println(PrepaidBalaSql);
		if ((null == tPrepaidBala || "".equals(tPrepaidBala))
				&& "1".equals(mLLSendMsgSchema.getAdvanceFlag())) {
			Content = mLLSendMsgSchema.getApplno() + "保单不存在预付未回销赔款!";
			System.out.println("保单不存在预付未回销赔款!");
			return false;
		}
		// 校验有没有该团体客户信息
		if ((null != CustomerNo && !"".equals(CustomerNo))
				|| (null != mLLSendMsgSchema.getApplno() && !""
						.equals(mLLSendMsgSchema.getApplno()))
				|| (null != GrpName && !"".equals(GrpName))) {
			String tSQL = "select  a.customerno, a.name, g.grpcontno,g.Peoples2,"
					+ " a.claimbankcode, a.claimbankaccno, a.claimaccname, "
					+ "a.AddressNo,'', b.riskcode from lcgrpcont g, LCGrpAppnt "
					+ "a,lcgrppol b where '1479367799000'='1479367799000' and "
					+ " a.grpcontno = g.grpcontno and g.appflag = '1' and "
					+ "b.grpcontno=g.grpcontno  ";
			if (null != CustomerNo && !"".equals(CustomerNo)) {
				tSQL = tSQL + " and g.AppntNo='" + CustomerNo + "'";
			}
			if (null != GrpName && !"".equals(GrpName)) {
				tSQL = tSQL + " and a.Name='" + GrpName + "'";
			}
			if (null != mLLSendMsgSchema.getApplno()
					&& !"".equals(mLLSendMsgSchema.getApplno())) {
				tSQL = tSQL + " and g.GrpContNo='"
						+ mLLSendMsgSchema.getApplno() + "'";
			}
			tSQL = tSQL + " fetch first 3000 rows only with ur";
			// ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(tSQL);
			System.out.println(tSQL);
			if (tSSRS.getMaxNumber() > 0) {
			} else {
				Content = "经保单号" + mLLSendMsgSchema.getApplno()
						+ "查询没有该团体客户信息！";
				System.out.println("没有该团体客户信息！");
				return false;
			}
		}
		/* 校验各种保全 */
		// 提示阻断
		String tIngSql1 = " select b.grpcontno,b.edortype,b.edorno,"
				+ " (select edorname from lmedoritem where edorcode=b.edortype fetch first 1 rows only) "
				+ " from lpedorapp a , lpgrpedoritem b "
				+ " where a.edoracceptno= b.edoracceptno "
				+ " and a.edorstate !='0' " // 正在进行中的保全项目
				+ " and b.grpcontno ='"
				+ mLLSendMsgSchema.getApplno()
				+ "'"
				// + " and b.grpcontno ='0000002101'"
				+ " and b.edortype in ('WT','XT','GA','TQ','TA','SG','CT','TF','LQ','ZB')"
				+ " group by b.grpcontno,b.edortype,b.edorno ";
		SSRS tIngSql1SSRS = tExeSQL.execSQL(tIngSql1);
		System.out.println(tIngSql1);
		if (tIngSql1SSRS.getMaxNumber() > 0) {
			for (int i = 0; i < tIngSql1SSRS.MaxRow; i++) {
				Content = mLLSendMsgSchema.getApplno() + "该团单正在进行"
						+ tIngSql1SSRS.GetText(i + 1, 4) + "保全操作,工单号为："
						+ tIngSql1SSRS.GetText(i + 1, 3) + "";
				System.out.println(mLLSendMsgSchema.getApplno() + "该团单正在进行"
						+ tIngSql1SSRS.GetText(i + 1, 4) + "保全操作,工单号为："
						+ tIngSql1SSRS.GetText(i + 1, 3) + "");
				return false;
			}
		}
		// 满期处理
		String tMJSql = " select temp.GrpContNo,temp.edorName,temp.edorstate "
				+ " from "
				+ " ( "
				+ " select a.grpcontno GrpContNo,'团单满期给付' edorName," // 团单满期给付
				+ " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate"
				+ " from ljsgetdraw a where feefinatype='TF' and riskcode='170206' "
				+ " and grpcontno='" + mLLSendMsgSchema.getApplno() + "' "
				+ " ) as temp "
				+ " group by temp.GrpContNo,temp.edorName,temp.edorstate "
				+ " with ur ";
		SSRS tMJSqlSSRS = tExeSQL.execSQL(tMJSql);
		System.out.println(tMJSql);
		if (tMJSqlSSRS.getMaxNumber() > 0) {
			for (int i = 0; i < tMJSqlSSRS.MaxRow; i++) {
				if (tMJSqlSSRS.GetText(i + 1, 3) != "0") {
					Content = mLLSendMsgSchema.getApplno() + "该团单正在进行"
							+ tMJSqlSSRS.GetText(i + 1, 2) + "保全操作";
					System.out.println(mLLSendMsgSchema.getApplno() + "该团单正在进行"
							+ tMJSqlSSRS.GetText(i + 1, 2) + "保全操作");
					return false;
				}
			}
		}
		// 所在团单正在定期结算
		String tDJSql = " select distinct a.contno " + " from lgwork a "
				+ " where a.contno = '" + mLLSendMsgSchema.getApplno() + "' "
				+ " and a.typeno like '06%' and a.statusno not in ('5','8') ";
		SSRS tDJSqlSSRS = tExeSQL.execSQL(tDJSql);
		System.out.println(tDJSql);
		if (tDJSqlSSRS.MaxNumber > 0) {
			for (int i = 0; i < tDJSqlSSRS.getMaxRow(); i++) {
				Content = "团单：" + mLLSendMsgSchema.getApplno() + "正在进行定期结算保全操作";
				System.out.println(mLLSendMsgSchema.getApplno()
						+ "该团单正在进行定期结算保全操作");
				return false;
			}
		}
		if (mLLSendMsgSchema.getReceiveMethod() != "1"
				&& mLLSendMsgSchema.getReceiveMethod() != "2"
				&& (mLLSendMsgSchema.getPayMethod() == "3" || mLLSendMsgSchema
						.getPayMethod() == "2")) {
			if ("".equals(mLLSendMsgSchema.getBankCode())
					|| null == mLLSendMsgSchema.getBankCode()) {
				Content = "请您录入开户银行的信息！！！";
				System.out.println("请您录入开户银行的信息！！！");
				return false;
			}
			if ("".equals(mLLSendMsgSchema.getAccountName())
					|| null == mLLSendMsgSchema.getAccountName()) {
				Content = "请您录入户名信息！！！";
				System.out.println("请您录入户名信息！！！");
				return false;
			}
			if ("".equals(mLLSendMsgSchema.getAccount())
					|| null == mLLSendMsgSchema.getAccount()) {
				Content = "请您录入账户信息！！！";
				System.out.println("请您录入账户信息！！！");
				return false;
			}
			if ("4".endsWith(mLLSendMsgSchema.getReceiveMethod())) {
				String tBankSQL = "SELECT * FROM ldbank WHERE bankcode='"
						+ mLLSendMsgSchema.getBankCode()
						+ "' AND cansendflag='1'";
				SSRS tBankSQLSSRS = tExeSQL.execSQL(tBankSQL);
				if (tBankSQLSSRS.getMaxNumber() <= 0) {
					Content = "该银行" + mLLSendMsgSchema.getBankCode()
							+ "不支持银行转帐，请确认是否修改为银行汇款或修改银行编码";
					System.out.println(mLLSendMsgSchema.getBankCode()
							+ "该银行不支持银行转帐，请确认是否修改为银行汇款或修改银行编码");
					return false;
				}
			}
		}
		// 556反洗钱黑名单客户的理赔监测功能
		String strblack = " select 1 from lcblacklist where trim(name)='"
				+ GrpName + "' with ur";
		SSRS strblackSSRS = tExeSQL.execSQL(strblack);
		System.out.println(strblack);
		if (strblackSSRS.getMaxNumber() > 0) {
			Content = "该单位" + GrpName + "为黑名单客户，须请示上级处理。";
			System.out.println(GrpName + "该单位为黑名单客户，须请示上级处理。");
			return false;
		}
		System.out.println("===========  This is AskSave() End  =============");
		return true;
	}

	/**
	 * 团体理赔处理--》团体理赔处理--》批量导入 前台校验
	 */
	public boolean DiskImport() {
		System.out
				.println("===========  This is DiskImport() Start  =============");
		System.out.println("mLLRegisterSchema.getRgtNo(): "
				+ mLLRegisterSchema.getRgtNo());
		if ("".equals(mLLRegisterSchema.getRgtNo())
				|| null == mLLRegisterSchema.getRgtNo()) {
			Content = "没有团体申请信息！";
			System.out.println("没有团体申请信息！");
			return false;
		}
		// #2169 批次导入前后台数据同步问题方案
		ExeSQL tExeSQL = new ExeSQL();
		String strSQL = "select rgtstate from llregister where rgtno='"
				+ mLLRegisterSchema.getRgtNo() + "' with ur";
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		System.out.println(strSQL);
		if (strSQLSSRS.getMaxNumber() > 0) {
			String tRgtState = strSQLSSRS.GetText(1, 1);
			if ("06".equals(tRgtState)) {
				Content = "批次号:" + mLLRegisterSchema.getRgtNo()
						+ "还在处理中，请稍候再处理该批次的案件!";
				System.out.println("批次号:" + mLLRegisterSchema.getRgtNo()
						+ "还在处理中，请稍候再处理该批次的案件!");
				return false;
			}
		}
		String tsql = "select CHECKGRPCONT('" + mLLSendMsgSchema.getApplno()
				+ "') from dual where 1=1";
		SSRS tsqlSSRS = tExeSQL.execSQL(tsql);
		System.out.println(tsql);
		if (!"Y".equals(tsqlSSRS.GetText(1, 1))) {
			String sql = "select * from LLAppClaimReason where rgtno='"
					+ mLLSendMsgSchema.getApplno() + "'";
			SSRS sqlSSRS = tExeSQL.execSQL(sql);
			System.out.println(sql);
			if (sqlSSRS.getMaxNumber() <= 0) {
				Content = "该保单" + mLLSendMsgSchema.getApplno() + "不在可导入数据列表!";
				System.out.println(mLLSendMsgSchema.getApplno()
						+ "该保单不在可导入数据列表!");
				return false;
			}
		}
		System.out
				.println("===========  This is DiskImport() End  =============");
		return true;
	}

	/**
	 * 团体理赔处理--》团体理赔处理--》批量导入--》上载理赔信息 前台校验
	 */
	public boolean newUploadFile() {
		System.out
				.println("===========  This is newUploadFile() Start  =============");
		String strSQL = "select rgtstate from llregister where rgtno='"
				+ mLLRegisterSchema.getRgtNo() + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		System.out.println(strSQL);
		if (strSQLSSRS.MaxNumber > 0) {
			if ("06".equals(strSQLSSRS.GetText(1, 1))) {
				Content = "批次号:" + mLLRegisterSchema.getRgtNo()
						+ "还在处理中，请稍候再处理该批次的案件!";
				System.out.println("批次号:" + mLLRegisterSchema.getRgtNo()
						+ "还在处理中，请稍候再处理该批次的案件!");
				return false;
			}
			if ((!"01".equals(strSQLSSRS.GetText(1, 1)))
					&& (!"07".equals(strSQLSSRS.GetText(1, 1)))) {
				Content = "该批次已经申请完毕";
				System.out.println("该批次已经申请完毕");
				return false;
			}
		}
		// int i = 0;
		// getImportPath();
		// String importFile = "1105178.xls";// excel的名字
		System.out
				.println("===========  This is newUploadFile() End  =============");
		return true;

	}

	/**
	 * 下载ftp下载的Excel
	 * 
	 * @return 成功导入
	 */
	public String ftpDownload() {
		System.out
				.println("============== This is ftpDownload() start =============");
		if (!DiskImport()) {
			return "";
		}
		String sql = "select description,code from ldconfig where codetype = 'ClaimInterface'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sql1SSRS = tExeSQL.execSQL(sql);
		String ip = "";
		String acc = "";
		String pwd = "";
		String port = "";
		for (int i = 1; i <= sql1SSRS.MaxRow; i++) {
			if ("IP".equals(sql1SSRS.GetText(i, 1))) {
				ip = sql1SSRS.GetText(i, 2);
			} else if ("AccName".equals(sql1SSRS.GetText(i, 1))) {
				acc = sql1SSRS.GetText(i, 2);
			} else if ("PassWord".equals(sql1SSRS.GetText(i, 1))) {
				pwd = sql1SSRS.GetText(i, 2);
			} else if ("Port".equals(sql1SSRS.GetText(i, 1))) {
				port = sql1SSRS.GetText(i, 2);
			}
		}
		System.out.println("ftp地址： " + ip + "账号： " + acc + "密码： " + pwd
				+ "端口： " + port);
		// FTPTool tFTPTool = new FTPTool("10.253.33.168", "xubo",
		// "xubo", 21);
		FTPTool tFTPTool = new FTPTool(ip, acc, pwd, Integer.parseInt(port));
		// FTPTool tFTPTool = new FTPTool("10.253.33.66", "huangying",
		// "huangying", 21);

		try {
			String[] s = mLLSendMsgSchema.getExcelAddress().split("/");
			System.out.println("excel地址：" + mLLSendMsgSchema.getExcelAddress());
			System.out.println("文件名： " + s[s.length - 1]);
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
				Content = "ftp下载失败，原因是:" + tFTPTool.getErrContent(1);
				System.out.println("Content==========" + Content);
				return Content;
			} else {
				// 获取路径
				String rootPath = getClass().getResource("/").getFile()
						.toString();
				System.out.println(rootPath);
				String str = rootPath.replaceAll("WEB-INF/classes/", "");
				str += "temp_lp/";

				System.out.println("相对路径：" + str);
				// 服务器路径
				String ftpPath = "";
				for (int i = 0; i < s.length - 1; i++) {
					ftpPath += s[i] + "/";
				}
				System.out.println("服务器路径ftp======= " + ftpPath);

				// E:/workspace2/picc1102/ui/temp_lp/560.xml
				// wasfs/wasappserv/profiles/AppSrv01/installedApps/p55a1Node01Cell/lis_war.ear/taobaoPort.war/temp_lp//LL11CaseImport.xml
				/*
				 * System.out.println("===================================");
				 * if(!tFTPTool.upload(ftpPath,
				 * "/wasfs/wasappserv/profiles/AppSrv01/installedApps/p55a1Node01Cell/lis_war.ear/taobaoPort.war/temp_lp/LL11CaseImport.xml"
				 * )){ Content = "ftp上载失败，原因是:" + tFTPTool.getErrContent(1);
				 * System.out.println(Content); return Content; }
				 * System.out.println("===================================");
				 */
				/*
				 * String[] ss = tFTPTool.downloadAllDirectory(ftpPath, str);
				 * System.out.println("所有文件：");
				 * if(!"".equals(tFTPTool.getErrContent(1))){ Content =
				 * "ftp文件夹下载失败，原因是:" + tFTPTool.getErrContent(1); return
				 * Content; } for (int i = 0; i < ss.length; i++) {
				 * System.out.println(ss[i]); // return Content; }
				 */

				if (!tFTPTool.downloadFile(ftpPath, str, s[s.length - 1])) {
					Content = "ftp下载失败，原因是:" + tFTPTool.getErrContent(1);
					System.out.println("Content==========" + Content);
					return Content;
				}
			}

		} catch (SocketException ex) {
			ex.printStackTrace();
			Content = "ftp下载失败，原因是:" + ex.toString();
			System.out.println(Content);
			FlagStr = "Fail";
			return Content;
		} catch (IOException ex) {
			ex.printStackTrace();
			Content = "ftp下载失败，原因是:" + ex.toString();
			System.out.println(Content);
			FlagStr = "Fail";
			return Content;
		}
		System.out
				.println("============== This is ftpDownload() end =============");
		return "成功导入";
	}

	/**
	 * 批量导入人员
	 * 
	 * @return
	 */
	public boolean importPersonnel() {
		System.out
				.println("============== This is importPersonnel() start =============");
		FlagStr = "Fail";
		Content = "";
		String[] s = mLLSendMsgSchema.getExcelAddress().split("/");
		String FileName = s[s.length - 1];
		System.out.println("文件名： " + FileName);
		int count = 0;
		String RgtNo = mLLRegisterSchema.getRgtNo();

		// 得到批次导入的版本号 add by Houyd 20140122
		String tVersionType = "1";
		// 获取路径
		String rootPath = getClass().getResource("/").getFile().toString();
		System.out.println(rootPath);
		String str = rootPath.replaceAll("WEB-INF/classes/", "");
		str += "temp_lp/";
		System.out.println("绝对路径：" + str);
		// 归档之前，记录解析配置文件xml的路径
		String XmlPath = str;
		String importPath = str;

		System.out.println("VersionType: " + tVersionType);

		if (!newUploadFile()) {
			return false;
		}
		// 得到excel文件的保存路径
		// 输出参数
		String mRgtNo = RgtNo;
		System.out.println("RgtNo:" + mRgtNo);
		GlobalInput tG = new GlobalInput();
		// tG.Operator = mLLSendMsgSchema.getOperate();
		// tG.ComCode = "8615";
		tG.ManageCom = mLLSendMsgSchema.getManageCom();
		if ("86360500".equals(tG.ManageCom)) {
			tG.Operator = "sc3672";
		}

		TransferData tTransferData = new TransferData();
		boolean res = true;

		LLImportCaseInfoNew tLLImportCaseInfoNew = new LLImportCaseInfoNew();

		// 准备传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tTransferData.setNameAndValue("FileName", FileName);
		tTransferData.setNameAndValue("FilePath", importPath);
		tTransferData.setNameAndValue("RgtNo", mRgtNo);
		tTransferData.setNameAndValue("VersionType", tVersionType);
		tTransferData.setNameAndValue("XmlPath", XmlPath);

		tVData.add(tTransferData);
		tVData.add(tG);
		try {
			if (!tLLImportCaseInfoNew.submitData(tVData, "")) { // 提取案件信息
				Content = "批量导入人员保存失败，原因是:"
						+ tLLImportCaseInfoNew.mErrors.getFirstError();
				FlagStr = "Fail";
				res = false;
				return res;
			}
		} catch (Exception ex) {
			Content = "批量导入人员保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			res = false;
			return res;
		}
		String sql = "select caseno FROM llcase where rgtno='"
				+ mLLRegisterSchema.getRgtNo() + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sqlLSSRS = tExeSQL.execSQL(sql);
		System.out.println(sql);
		if (sqlLSSRS.getMaxNumber() <= 0) {
			Content = "批量导入人员保存失败，原因是llcase数据未生成";
			// NewSCParser newSCParser = new NewSCParser();
			// Content = NewSCParser.getErrMessage();
			FlagStr = "Fail";
			res = false;
			return res;
		}
		System.out.println("submitData Finished");
		System.out
				.println("============== This is importPersonnel() end =============");
		String filePath = str + FileName;
		deleteFile(filePath);
		return res;
	}

	// 删除excel
	public boolean deleteFile(String sPath) {
		Boolean flag = false;
		File file = new File(sPath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists()) {
			file.delete();
			flag = true;
		}
		return flag;
	}

	/**
	 * 团体理赔处理--》批量导入--》理算 前台校验
	 */
	public boolean CalClaim() {
		System.out
				.println("===========  This is CalClaim() Start  =============");
		String strSQL = "select rgtstate from llregister where rgtno='"
				+ mLLRegisterSchema.getRgtNo() + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		System.out.println(strSQL);
		if ("06".equals(strSQLSSRS.GetText(1, 1))) {
			Content = "批次号:" + mLLRegisterSchema.getRgtNo()
					+ "还在处理中，请稍候再处理该批次的案件!";
			System.out.println("批次号:" + mLLRegisterSchema.getRgtNo()
					+ "还在处理中，请稍候再处理该批次的案件!");
			return false;
		} else if ((!"01".equals(strSQLSSRS.GetText(1, 1)))
				&& (!"07".equals(strSQLSSRS.GetText(1, 1)))) {
			Content = mLLRegisterSchema.getRgtNo() + "该批次已经申请完毕";
			System.out.println(mLLRegisterSchema.getRgtNo() + "该批次已经申请完毕");
			return false;
		}
		System.out
				.println("===========  This is CalClaim() End  =============");
		return true;
	}

	/**
	 * 团体理赔处理--》团体理赔处理--》批量导入--》团体申请完毕 前台校验
	 */
	public boolean RgtFinish() {
		System.out
				.println("===========  This is RgtFinish() Start  =============");
		String strSQL = " select AppPeoples,rgtstate from LLRegister where rgtno = '"
				+ mLLRegisterSchema.getRgtNo() + "' and rgtclass = '1'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		System.out.println(strSQL);
		String strSQL1 = "select * from llcase d where rgtno='"
				+ mLLRegisterSchema.getRgtNo()
				+ "' and rgtstate !='14' and  not exists (select 1 from llclaimdetail where caseno=d.caseno) fetch first 10 rows only with ur";
		SSRS strSQL1SSRS = tExeSQL.execSQL(strSQL1);
		System.out.println(strSQL1);
		if (strSQLSSRS.getMaxNumber() > 0) {
			if ("06".equals(strSQLSSRS.GetText(1, 2))) {
				Content = "批次号:" + mLLRegisterSchema.getRgtNo()
						+ "还在处理中，请稍候再处理该批次的案件!";
				System.out.println("批次号:" + mLLRegisterSchema.getRgtNo()
						+ "还在处理中，请稍候再处理该批次的案件!");
				return false;
			}
			if ((!"01".equals(strSQLSSRS.GetText(1, 2)))
					&& (!"07".equals(strSQLSSRS.GetText(1, 2)))) {
				Content = "团体案件" + mLLRegisterSchema.getRgtNo() + "状态不允许该操作！";
				System.out.println(mLLRegisterSchema.getRgtNo()
						+ "团体案件状态不允许该操作！");
				return false;
			}
			String AppPeoples;
			try {
				AppPeoples = strSQLSSRS.GetText(1, 1);
			} catch (Exception ex) {
				Content = ex.getMessage() + "AppPeoples";
				System.out.println(ex.getMessage() + "AppPeoples");
				return false;
			}
		} else {
			Content = "团体案件" + mLLRegisterSchema.getRgtNo() + "信息查询失败！";
			System.out.println(mLLRegisterSchema.getRgtNo() + "团体案件信息查询失败！");
			return false;
		}
		if (strSQL1SSRS.getMaxNumber() > 0) {
			Content = mLLRegisterSchema.getRgtNo() + "该批次下存在未理算案件";
			System.out.println(mLLRegisterSchema.getRgtNo() + "该批次下存在未理算案件");
			return false;
		}
		String strSQL2 = " select count(*) from llcase where rgtno = '"
				+ mLLRegisterSchema.getRgtNo() + "'";
		SSRS strSQL2SSRS = tExeSQL.execSQL(strSQL2);
		System.out.println(strSQL2);
		String countStr = strSQL2SSRS.GetText(1, 1).toString();
		int countnum = Integer.valueOf(countStr);
		if (countnum > 0) {
			String RealPeoples;
			try {
				RealPeoples = strSQL2SSRS.GetText(1, 1);
			} catch (Exception ex) {
				System.out.println(ex.getMessage() + "RealPeoples");
				return false;
			}
		}
		String strSQL3 = " select customername from llcase where rgtno = '"
				+ mLLRegisterSchema.getRgtNo() + "'";
		SSRS strSQL3SSRS = tExeSQL.execSQL(strSQL3);
		System.out.println(strSQL3);
		if (strSQL3SSRS.getMaxNumber() > 0) {
			int count = strSQL3SSRS.getMaxRow();
			for (int i = 0; i < count; i++) {
				// 556反洗钱黑名单客户的理赔监测功能
				String strblack = " select 1 from lcblacklist where trim(name)='"
						+ strSQL3SSRS.GetText(i + 1, 1) + "' with ur";
				SSRS crrblack = tExeSQL.execSQL(strblack);
				System.out.println(strblack);
				if (crrblack.getMaxNumber() > 0) {
					Content = "客户" + strSQL3SSRS.GetText(i + 1, 1)
							+ "为黑名单客户，须请示上级处理。";
					System.out.println(strSQL3SSRS.GetText(i + 1, 1)
							+ "客户为黑名单客户，须请示上级处理。");
					return false;
				}
			}
		}
		// 对于赔付正在进行结余返还操作以及已完成结余返还项目的保单，在理算确认时校验并阻断
		// 1.先根据页面上的批次号查询该批次号对应的grpcontno
		// var
		// GrpContSQL="select distinct grpcontno from llclaimdetail where rgtno='"+fm.all('RgtNo').value+"'
		// and grpcontno not in
		// ('00149574000001','00092763000002','00149574000002')";
		// 改为配置
		String GrpContSQL = "select distinct grpcontno from llclaimdetail where rgtno='"
				+ mLLRegisterSchema.getRgtNo()
				+ "' and grpcontno not in (select code from ldcode where codetype='lp_jyfh_pass')";
		SSRS GrpContSQLSSRS = tExeSQL.execSQL(GrpContSQL);
		System.out.println(GrpContSQL);
		if (GrpContSQLSSRS.getMaxNumber() > 0) {
			String mGrpContNo = GrpContSQLSSRS.GetText(1, 1);
			// 2.根据grpcontno查询该单是否正在进行结余返回操作
			String CheckBQSQL = "select 1 from lpgrpedoritem where grpcontno='"
					+ mGrpContNo + "' and edortype = 'BJ'";
			SSRS CheckBQSQLSSRS = tExeSQL.execSQL(CheckBQSQL);
			System.out.println(CheckBQSQL);
			// 3.判断上述sql是否存在数据，是阻断否通过
			if (CheckBQSQLSSRS.getMaxNumber() > 0) {
				Content = "该保单" + mGrpContNo + "正在进行结余返还操作或者已完成结余返还项目，不能理算确认";
				System.out.println("该保单" + mGrpContNo
						+ "正在进行结余返还操作或者已完成结余返还项目，不能理算确认");
				return false;
			}
		}

		System.out
				.println("===========  This is RgtFinish() End  =============");
		return true;
	}

	/**
	 * 团体理赔处理--》团体审定--》批量审定 前台校验
	 */
	public boolean RGTEnsure() {
		System.out
				.println("===========  This is RGTEnsure() Start  =============");
		if ("".equals(mLLRegisterSchema.getRgtNo())
				|| null == mLLRegisterSchema.getRgtNo()) {
			Content = "团体批次号为空！";
			System.out.println("团体批次号为空！");
			return false;
		}
		System.out
				.println("===========  This is RGTEnsure() End  =============");
		return true;
	}

	/**
	 * 团体理赔处理--》团体给付--》团体给付确认（给付方式不是个人给付） 前台校验
	 */
	public boolean RgtGiveEnsure() {
		System.out
				.println("===========  This is RgtGiveEnsure() Start  =============");
		if ("".equals(mLLSendMsgSchema.getGEManageCom())
				|| null == mLLSendMsgSchema.getGEManageCom()) {
			Content = "给付操作机构为空！";
			System.out.println("给付操作机构为空！");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getGEOperate())
				|| null == mLLSendMsgSchema.getGEOperate()) {
			Content = "给付操作员为空！";
			System.out.println("给付操作员为空！");
			return false;
		}
		String prtnoSql = "SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO, R.RGTANTNAME, R.RGTDATE, (select count(caseno) from llcase where '1479797569000'='1479797569000' and  rgtno=R.rgtno), b.codename,R.rgtstate, coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where rgtno=r.rgtno  ),0), (select codename from ldcode where r.TogetherFlag=code and codetype='lltogetherflag' ) , (select codename from ldcode where r.CaseGetMode=code and codetype='paymode' ),r.RgtantName, r.BankCode,r.AccName,r.BankAccNo, (CASE WHEN r.rgtstate in ('04','05') THEN  (SELECT NVL(SUM(-pay),0) FROM ljagetclaim m WHERE m.otherno=r.rgtno AND m.othernotype='Y' )  ELSE 0 END),  (CASE WHEN r.rgtstate in ('04','05') THEN  (SELECT NVL(SUM(sumgetmoney),0) FROM ljaget m where m.othernotype in ('Y','5') AND m.otherno=r.rgtno)  ELSE 0 END)  FROM LLREGISTER R , ldcode b  WHERE  R.RGTOBJ = '0' AND R.RGTCLASS = '1' AND R.declineflag is null  and b.codetype='llgrprgtstate' and b.code = r.rgtstate  and r.MngCom like '"
				+ mLLSendMsgSchema.getGEManageCom()
				+ "%' and  r.rgtno ='"
				+ mLLRegisterSchema.getRgtNo() + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS prtnoSqlSSRS = tExeSQL.execSQL(prtnoSql);
		System.out.println(prtnoSql);
		if (prtnoSqlSSRS.MaxNumber > 0) {
			String trgtstate = prtnoSqlSSRS.GetText(1, 9);
			if ("04".equals(trgtstate) || "05".equals(trgtstate)) {
				Content = "该团体案件已做完给付确认！";
				System.out.println("该团体案件已做完给付确认！");
				return false;
			}
			if ("01".equals(trgtstate)) {
				Content = "当前状态下不能做该团体案件的给付确认！";
				System.out.println("当前状态下不能做该团体案件的给付确认！");
				return false;
			}
			// 细化#2407 理赔模块黑名单控制调整==》#2684关于理赔环节黑名单监测规则修改的需求
			String rgtno1 = prtnoSqlSSRS.GetText(1, 1);
			String strblack = " select IDNo from LLRegister where rgtno= '"
					+ rgtno1 + "' with ur";
			SSRS arrblack = tExeSQL.execSQL(strblack);
			System.out.println(strblack);
			String checkId1 = "";
			if (arrblack.getMaxNumber() > 0) {
				checkId1 = arrblack.GetText(1, 1).trim();
			}
			String tRgtantName = prtnoSqlSSRS.GetText(1, 13);
			if (!checkBlacklist(tRgtantName, checkId1)) {
				return false;
			}
			String strblack2 = "select customername,IDNO from llcase where rgtno='"
					+ mLLRegisterSchema.getRgtNo() + "'";
			SSRS arrblack2SSRS = tExeSQL.execSQL(strblack2);
			System.out.println(strblack2);
			String checkName2 = "";
			String checkId2 = "";
			if (arrblack2SSRS.getMaxNumber() > 0) {
				for (int i = 0; i < arrblack2SSRS.getMaxRow(); i++) {
					checkName2 = arrblack2SSRS.GetText(i + 1, 1).trim();
					checkId2 = arrblack2SSRS.GetText(i + 1, 2).trim();
					if (!checkBlacklist(checkName2, checkId2)) {
						return false;
					}
				}
			}
			// -----------增加个人给付校验--Begin--
			String tTogetherFlag = prtnoSqlSSRS.GetText(1, 11); // 给付方式
			if ("个人给付".equals(tTogetherFlag)) {
				Content = "团体案件的给付方式为个人给付!团体批次号为："
						+ mLLRegisterSchema.getRgtNo();
				System.out.println("团体案件的给付方式为个人给付!团体批次号为："
						+ mLLRegisterSchema.getRgtNo());
				return false;
			}
			// --------避免给付方式领取方式选择错误，增加给付方式领取方式等信息提示----------End---------
			// --------校验青岛无名单是否已经结案------------Begin------------
			String tGrpContNo = prtnoSqlSSRS.GetText(1, 4); // 合同号
			String QDRgt = "select count(1) from llregister where rgtno='"
					+ mLLRegisterSchema.getRgtNo()
					+ "' and GrpName='青岛市医疗保险管理中心'";
			SSRS QDRgtSSRS = tExeSQL.execSQL(QDRgt);
			System.out.println(QDRgt);
			if (Integer.valueOf(QDRgtSSRS.GetText(1, 1)) >= 1) {
				String sql1 = "select count(1) from ljsgetclaim where otherno ='"
						+ mLLRegisterSchema.getRgtNo() + "'";
				SSRS sql1SSRS = tExeSQL.execSQL(sql1);
				System.out.println(sql1);
				if (Integer.valueOf(sql1SSRS.GetText(1, 1)) <= 0) {
					Content = "请确认团体审批审定后是否进行了【团体结案】操作!" + "团体批次号："
							+ mLLRegisterSchema.getRgtNo();
					System.out.println("请确认团体审批审定后是否进行了【团体结案】操作!" + "团体批次号："
							+ mLLRegisterSchema.getRgtNo());
					// fm.RgtGiveEnsure1.disabled=false;
					return false;
				}

			}
			// ---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
			String PrepaidBalaSql = "select PrepaidBala from LLPrepaidGrpCont a "
					+ " where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and grpcontno='"
					+ tGrpContNo + "')";
			SSRS PrepaidBalaSqlSSRS = tExeSQL.execSQL(PrepaidBalaSql);
			System.out.println(PrepaidBalaSql);
			String tPrepaidBala = "";
			if (PrepaidBalaSqlSSRS.getMaxNumber() > 0) {
				tPrepaidBala = PrepaidBalaSqlSSRS.GetText(1, 1);
				if (Integer.valueOf(tPrepaidBala) <= 0) {
					tPrepaidBala = "";
				} else {
					tPrepaidBala = PrepaidBalaSqlSSRS.GetText(1, 1);
				}
			}
			if ((null == tPrepaidBala || "".equals(tPrepaidBala))
					&& "1".equals(mLLSendMsgSchema.getAdvanceFlag())) {
				Content = "被保险人投保的保单不存在预付未回销赔款!";
				System.out.println("被保险人投保的保单不存在预付未回销赔款!");
				// fm.RgtGiveEnsure1.disabled=false;
				return false;
			}
		} else {
			Content = "批次号：" + mLLRegisterSchema.getRgtNo() + "没有满足给付的数据！";
			System.out.println("批次号：" + mLLRegisterSchema.getRgtNo()
					+ "没有满足给付的数据！");
			return false;
		}
		System.out
				.println("===========  This is RgtGiveEnsure() End  =============");
		return true;
	}

	/**
	 * 理赔处理--》通知给付--》给付确认（给付方式是个人给付） 前台校验
	 */
	public boolean giveConfirm() {
		System.out
				.println("===========  This is giveConfirm() Start  =============");
		String querySql = "select c.caseno,c.customername, c.customerno, m.realpay,  (select codename from ldcode where '1480323889000'='1480323889000' and  codetype='paymode' and code=c.casegetmode),  c.CustomerName,c.IDNo,c.bankcode,c.bankaccno,c.accname,c.casegetmode,c.rgtno,  case when c.prepaidflag='1' then '是' else '否' end , 0,(case when c.rgtstate in('11','12')  then m.realpay else 0 end) ,case when c.prepaidflag='1' then '1' else '0' end from llcase c, llclaim m,llregister r  where  c.caseno=m.caseno and c.rgtstate='09' and c.rgtno=r.rgtno and (r.togetherflag is null or r.togetherflag not in ('3','4'))  and c.mngcom like '"
				+ mLLSendMsgSchema.getManageCom()
				+ "%' and c.rgtno='"
				+ mLLRegisterSchema.getRgtNo()
				+ "' order by c.caseno fetch first 3000 rows only with ur ";
		ExeSQL ttExeSQL = new ExeSQL();
		SSRS querySqlSSRS = ttExeSQL.execSQL(querySql);
		// 1111111111
		if (querySqlSSRS.getMaxNumber() <= 0) {
			Content = "该批次次下不存在待给付的案件!";
			System.out.println("该批次次下不存在待给付的案件!");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getGEManageCom())
				|| null == mLLSendMsgSchema.getGEManageCom()) {
			Content = "给付操作机构为空！";
			System.out.println("给付操作机构为空！");
			return false;
		}
		if ("".equals(mLLSendMsgSchema.getGEOperate())
				|| null == mLLSendMsgSchema.getGEOperate()) {
			Content = "给付操作员为空！";
			System.out.println("给付操作员为空！");
			return false;
		}

		int num = 0;
		if (mLLPersonMsgSet.size() >= 1) {
			for (int i = 1; i <= mLLPersonMsgSet.size(); i++) {
				// LLPersonMsgSchema tLLPersonMsgSchema = new
				// LLPersonMsgSchema();
				// tLLPersonMsgSchema.setSchema(mLLPersonMsgSet.get(i));
				String name = mLLPersonMsgSet.get(i).getName();
				String id = mLLPersonMsgSet.get(i).getID();
				String sqlStr = "select c.caseno,c.customername, c.customerno, m.realpay,  (select codename from ldcode where '1480054601000'='1480054601000' and  codetype='paymode' and code=c.casegetmode),  c.CustomerName,c.IDNo,c.bankcode,c.bankaccno,c.accname,c.casegetmode,c.rgtno,  case when c.prepaidflag='1' then '是' else '否' end , 0,(case when c.rgtstate in('11','12')  then m.realpay else 0 end) ,case when c.prepaidflag='1' then '1' else '0' end from llcase c, llclaim m,llregister r  where  c.caseno=m.caseno and c.rgtstate='09' and c.rgtno=r.rgtno and (r.togetherflag is null or r.togetherflag not in ('3','4'))  and c.mngcom like '"
						+ mLLSendMsgSchema.getManageCom()
						+ "%' and c.rgtno='"
						+ mLLRegisterSchema.getRgtNo()
						+ "'and c.customername='"
						+ name
						+ "' and  c.IDNo='"
						+ id
						+ "'  order by c.caseno fetch first 3000 rows only with ur ";
				ExeSQL tExeSQL = new ExeSQL();
				SSRS sqlStrSSRS = tExeSQL.execSQL(sqlStr);
				if (sqlStrSSRS.getMaxNumber() > 0) {
					num = num + 1;
					if ("".equals(mLLPersonMsgSet.get(i).getReceiveMethod())
							|| null == mLLPersonMsgSet.get(i)
									.getReceiveMethod()) {
						Content = "给付方式不能为空!";
						System.out.println("给付方式不能为空!");
						return false;
					}
					if ("1".equals(mLLPersonMsgSet.get(i).getReceiveMethod())
							|| "2".equals(mLLPersonMsgSet.get(i)
									.getReceiveMethod())) {
						if ("".equals(mLLPersonMsgSet.get(i).getDrawer())
								|| null == mLLPersonMsgSet.get(i).getDrawer()) {
							Content = "现金支付，领款人不能为空!";
							System.out.println("现金支付，领款人不能为空!");
							return false;
						}

						if ("".equals(mLLPersonMsgSet.get(i).getDrawerID())
								|| null == mLLPersonMsgSet.get(i).getDrawerID()) {
							Content = "现金支付，领款人身份证不能为空!";
							System.out.println("现金支付，领款人身份证不能为空!");
							return false;
						}
					} else {
						if ("".equals(mLLPersonMsgSet.get(i).getBankCode())
								|| null == mLLPersonMsgSet.get(i).getBankCode()) {
							Content = "银行支付，银行编码不能为空!";
							System.out.println("银行支付，银行编码不能为空!");
							return false;
						}
						if ("".equals(mLLPersonMsgSet.get(i).getAccount())
								|| null == mLLPersonMsgSet.get(i).getAccount()) {
							Content = "银行支付，银行账号不能为空!";
							System.out.println("银行支付，银行账号不能为空!");
							return false;
						}
						if ("".equals(mLLPersonMsgSet.get(i).getAccountName())
								|| null == mLLPersonMsgSet.get(i)
										.getAccountName()) {
							Content = "银行支付，账户名不能为空!";
							System.out.println("银行支付，账户名不能为空!");
							return false;
						}
					}
					// ---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---111111
					String PrepaidBalaSql = "select PrepaidBala from LLPrepaidGrpCont a "
							+ " where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and caseno='"
							+ sqlStrSSRS.GetText(1, 1) + "')";
					SSRS PrepaidBalaSqlSSRS = tExeSQL.execSQL(PrepaidBalaSql);
					System.out.println(PrepaidBalaSql);
					String tPrepaidBala = "";
					if (PrepaidBalaSqlSSRS.getMaxNumber() > 0) {
						tPrepaidBala = PrepaidBalaSqlSSRS.GetText(1, 1);
					}
					if (("".equals(tPrepaidBala) || null == tPrepaidBala)
							&& "1".equals(mLLPersonMsgSet.get(i)
									.getAdvanceFlag())) {
						Content = "案件" + sqlStrSSRS.GetText(1, 1)
								+ "的被保险人投保的保单不存在预付未回销赔款!";
						System.out.println("案件" + sqlStrSSRS.GetText(1, 1)
								+ "的被保险人投保的保单不存在预付未回销赔款!");
						return false;
					}
					// 细化#2407 理赔模块黑名单控制调整==》#2684关于理赔环节黑名单监测规则修改的需求
					String checkName1 = sqlStrSSRS.GetText(1, 2);
					String checkcaseno1 = sqlStrSSRS.GetText(1, 1);
					String checkId1 = "";
					String strblack = " select IDNo from llcase where caseno= '"
							+ checkcaseno1 + "' with ur";
					SSRS strblackSSRS = tExeSQL.execSQL(strblack);
					if (strblackSSRS.getMaxNumber() > 0) {
						checkId1 = strblackSSRS.GetText(1, 1).trim();
					}
					if (!checkBlacklist(checkName1, checkId1)) {
						return false;
					}
					String checkName2 = mLLPersonMsgSet.get(i).getDrawer();
					String checkId2 = mLLPersonMsgSet.get(i).getDrawerID();
					if (checkName1 != checkName2) {
						if (!checkBlacklist(checkName2, checkId2)) {
							return false;
						}
					}
					// TODO:新增效验，理赔二核时不可给付确认
					String sqlUW = "select rgtstate from llcase where caseno='"
							+ sqlStrSSRS.GetText(1, 1) + "' with ur";
					SSRS sqlUWSSRS = tExeSQL.execSQL(sqlUW);
					System.out.println(sqlUW);
					if (sqlUWSSRS.getMaxNumber() > 0) {
						if (!"".endsWith(sqlUWSSRS.GetText(1, 1))
								|| null != sqlUWSSRS.GetText(1, 1)) {
							if ("16".equals(sqlUWSSRS.GetText(1, 1))) {
								Content = "理赔二核中不可给付确认，必须等待二核结束！";
								System.out.println("理赔二核中不可给付确认，必须等待二核结束！");
								return false;
							}
						}
					}
				}
			}
			// 1111111
			if (num == 0) {
				Content = "报文中人员的姓名和ID与上载的excel中人员的姓名和ID不符，请检查";
				System.out.println("报文中人员的姓名和ID与上载的excel中人员的姓名和ID不符，请检查");
				return false;
			}
		} else {
			Content = "没有待给付确定的案件";
			System.out.println("没有待给付确定的案件");
			return false;
		}
		System.out
				.println("===========  This is giveConfirm() End  =============");
		return true;
	}

	/**
	 * #2684关于理赔环节黑名单监测规则修改的需求
	 */
	public boolean checkBlacklist(String checkName, String checkId) {
		System.out
				.println("===========  This is checkBlacklist() Start  =============");
		// 需要验证的证件号与人名都为空
		if (("".equals(checkId) || null == checkId)
				&& ("".equals(checkName) || null == checkName)) {
			return true;
		}
		// 1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
		String strblack = " select name,idno from lcblacklist where trim(name)in('"
				+ checkName + "') or trim(idno)in('" + checkId + "') with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS strblackSSRS = tExeSQL.execSQL(strblack);
		System.out.println(strblack);
		if (strblackSSRS.getMaxNumber() > 0) {
			String strblack1 = " select 1 from lcblacklist where trim(name)in('"
					+ checkName
					+ "') and trim(idno)in('"
					+ checkId
					+ "') with ur";
			SSRS strblack1SSRS = tExeSQL.execSQL(strblack1);
			System.out.println(strblack1);
			if (strblack1SSRS.getMaxNumber() > 0) {
				Content = "客户或者申请人" + checkName + "为黑名单客户";
				System.out.println("客户或者申请人" + checkName + "为黑名单客户");
				return false;
			}
		}
		System.out
				.println("===========  This is checkBlacklist() End  =============");
		return true;
	}

	/**
	 * 团体理赔处理--》团体审定--》批量审定
	 */
	public boolean batchAuthorize() {
		if (!RGTEnsure())// 批量审定 前台校验
		{
			return false;
		}
		FlagStr = "";
		// tG.Operator = mLLSendMsgSchema.getOperate();
		// tG.ComCode = "8615";
		// tG.ManageCom=mLLSendMsgSchema.getManageCom();
		tG.ManageCom = mLLSendMsgSchema.getManageCom().substring(0, 4);
		if ("8636".equals(tG.ManageCom)) {
			tG.Operator = "ss3601";
		}
		VData tVData = new VData();
		boolean flag = false;
		// 团体理赔处理--》团体审定--》批量审定
		SimpleClaimAuditBL tSimpleClaimAuditBL = new SimpleClaimAuditBL();
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		LLCaseSet tLLCaseSet = new LLCaseSet();
		String strOperate = "";
		String strSqlss = "";
		strOperate = "UW||MAIN";

		tLLRegisterSchema.setRgtNo(mLLRegisterSchema.getRgtNo());
		String sql0 = " and (c.handler='" + mLLSendMsgSchema.getOperate()
				+ "' or c.operator='" + mLLSendMsgSchema.getOperate()
				+ "' or c.uwer='" + mLLSendMsgSchema.getOperate()
				+ "' or c.signer='" + mLLSendMsgSchema.getOperate()
				+ "' or c.dealer='" + mLLSendMsgSchema.getOperate() + "') ";
		// 11111111111111
		String strSql = "select distinct c.caseno "
				+ " FROM LLCASE C "
				+ " WHERE C.RGTNO = '"
				+ mLLRegisterSchema.getRgtNo()
				+ "' "
				+ " and not exists(select 1 from llclaimdetail a,lcpol b,lmriskapp cl,llcaserela dl,llsubreport e "
				+ " where a.polno=b.polno and a.riskcode=cl.riskcode and a.caserelano=dl.caserelano "
				+ " and dl.subrptno=e.subrptno "
				+ " and cl.riskprop='G' and ((e.accdate >= b.paytodate) and (a.riskcode not in (select riskcode from lmriskapp where risktype3='7' and risktype <>'M'))) and a.caseno=c.caseno union "
				+ " select 1 from llclaimdetail a,lbpol b,lmriskapp cl,llcaserela dl,llsubreport e "
				+ " where a.polno=b.polno and a.riskcode=cl.riskcode and a.caserelano=dl.caserelano "
				+ " and dl.subrptno=e.subrptno "
				+ " and cl.riskprop='G' and e.accdate >= b.paytodate and a.caseno=c.caseno) ";
		strSqlss = " and c.rgtstate in ('03','04','08') order by c.caseno fetch first 1000 rows only ";
		// String strSql =
		// "select distinct c.caseno FROM LLCASE C WHERE C.RGTNO = '" +
		// mLLRegisterSchema.getRgtNo()+ "'";
		strSql += strSqlss;
		System.out.println("==== strSql == " + strSql);
		SSRS tSSRS = new SSRS();
		ExeSQL mExeSQL = new ExeSQL();
		tSSRS = mExeSQL.execSQL(strSql);
		System.out.println("==== tSSRS.getMaxNumber() == "
				+ tSSRS.getMaxNumber());
		if (tSSRS.getMaxNumber() < 0) {
			flag = false;
			Content = "确认失败，原因是:该批次下没有查询到符合登录用户"
					+ mLLSendMsgSchema.getOperate() + "审定条件的案件";
			System.out.println("aaaa：该批次下没有可审定的案件");
			FlagStr = "Fail";
			return false;
		} else {
			for (int i = 0; i < tSSRS.getMaxRow(); i++) {
				System.out.println("<-tCaseNo[i]-->" + tSSRS.GetText(i + 1, 1));
				LLCaseSchema tLLCaseSchema = new LLCaseSchema();
				tLLCaseSchema.setCaseNo(tSSRS.GetText(i + 1, 1));
				tLLCaseSet.add(tLLCaseSchema);

			}

			try {
				// 1111111111111
				// tG.Operator = "social";
				System.out.println("socialsocialsocial");
				// String sql =
				// "update LLSocialClaimUser set HandleFlag = '1' where  UserCode='cm0002'";
				// ExeSQL tExeSQL = new ExeSQL();
				// tExeSQL.execUpdateSQL(sql);

				tVData.add(tLLRegisterSchema);
				tVData.add(tLLCaseSet);
				tVData.add(tG);
				tSimpleClaimAuditBL.submitData(tVData, strOperate);
			} catch (Exception ex) {
				flag = false;
				Content = "确认失败，原因是:" + ex.toString();
				System.out.println("aaaa" + ex.toString());
				FlagStr = "Fail";
				return flag;
			}
			if (FlagStr == "") {
				tError = tSimpleClaimAuditBL.mErrors;
				if (!tError.needDealError()) {
					flag = true;
					Content = "确认成功！";
					FlagStr = "Succ";
					tVData.clear();
					tVData = tSimpleClaimAuditBL.getResult();
					String backmsg = (String) tVData.getObjectByObjectName(
							"String", 0);
					// if(!backmsg.trim().equals("")){
					// Content = backmsg;
					// }
				} else {
					flag = false;
					Content = "确认失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return flag;
				}
			}
		}

		return flag;
	}

	/**
	 * 团体理赔处理--》团体给付--》团体给付确认（给付方式不是个人给付）
	 */
	public boolean GiveEnsure() {
		if (!RgtGiveEnsure())// 团体给付确认（给付方式不是个人给付） 前台校验
		{
			return false;
		}
		// 1111111111111
		FlagStr = "";
		// tG.Operator = "gf";
		// tG.ComCode = "8615";
		tG.ManageCom = (String) mLLSendMsgSchema.getGEManageCom();
		if ("86360500".equals(tG.ManageCom)) {
			tG.Operator = "sc3612";
		}
		VData tVData = new VData();
		boolean flag = false;
		String strOperate = "GIVE|ENSURE";
		if (strOperate.equals("GIVE|ENSURE")) {
			GrpGiveEnsureBL tGrpGiveEnsureBL = new GrpGiveEnsureBL();
			LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
			tLLRegisterSchema.setRgtNo(mLLRegisterSchema.getRgtNo());

			// if
			// (!"".equals(mLLSendMsgSchema.getAdvanceFlag())||null!=mLLSendMsgSchema.getAdvanceFlag()
			// ) {
			if ("1".equals(mLLSendMsgSchema.getAdvanceFlag().trim())) {
				tLLRegisterSchema.setPrePaidFlag("1"); // 0或null 不使用预付回销 1-预付回销
			} else {
				tLLRegisterSchema.setPrePaidFlag(""); // 个案标记llcase
														// 批次案件标记llregister
			}
			try {
				tVData.add(tLLRegisterSchema);
				tVData.add(tG);
				tGrpGiveEnsureBL.submitData(tVData, strOperate);
			} catch (Exception ex) {
				flag = false;
				Content = "确认失败，原因是:" + ex.toString();
				System.out.println("aaaa" + ex.toString());
				FlagStr = "Fail";
				return flag;
			}
			if (FlagStr == "") {
				tError = tGrpGiveEnsureBL.mErrors;
				if (!tError.needDealError()) {
					flag = true;
					Content = "确认成功！" + tGrpGiveEnsureBL.getBackMsg();
					FlagStr = "Succ";
					tVData.clear();
					tVData = tGrpGiveEnsureBL.getResult();
				} else {
					flag = false;
					Content = "确认失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return flag;
				}
			}
		}
		if (strOperate.equals("QDGIVE|ENSURE")) {
			QDGrpGiveEnsureBL tQDGrpGiveEnsureBL = new QDGrpGiveEnsureBL();
			LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
			tLLRegisterSchema.setRgtNo(mLLRegisterSchema.getRgtNo());
			// if
			// (!"".equals(mLLSendMsgSchema.getAdvanceFlag())&&null!=mLLSendMsgSchema.getAdvanceFlag()
			// ) {
			if ("1".equals(mLLSendMsgSchema.getAdvanceFlag().trim())) {
				tLLRegisterSchema.setPrePaidFlag("1"); // 0或null 不使用预付回销 1-预付回销
			} else {
				tLLRegisterSchema.setPrePaidFlag(""); // 个案标记llcase
														// 批次案件标记llregister
			}
			try {
				tVData.add(tLLRegisterSchema);
				tVData.add(tG);
				tQDGrpGiveEnsureBL.submitData(tVData, strOperate);
			} catch (Exception ex) {
				flag = false;
				Content = "确认失败，原因是:" + ex.toString();
				System.out.println("aaaa" + ex.toString());
				FlagStr = "Fail";
				return flag;
			}
			if (FlagStr == "") {
				tError = tQDGrpGiveEnsureBL.mErrors;
				if (!tError.needDealError()) {
					flag = true;
					Content = "确认成功！";
					FlagStr = "Succ";
					tVData.clear();
					tVData = tQDGrpGiveEnsureBL.getResult();
				} else {
					flag = false;
					Content = "确认失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return flag;
				}
			}
		}

		return flag;
	}

	/**
	 * 理赔处理--》通知给付--》给付确认（给付方式是个人给付）
	 */
	public boolean giveCon() {
		if (!giveConfirm())// 给付确认（给付方式是个人给付） 前台校验
		{
			return false;
		}
		FlagStr = "";
		// tG.Operator = mLLSendMsgSchema.getGEOperate();
		// tG.ComCode = "8615";
		tG.ManageCom = mLLSendMsgSchema.getGEManageCom();
		// 更改操作员
		if ("86360500".equals(tG.ManageCom)) {
			tG.Operator = "sc3672";
		}
		VData tVData = new VData();
		boolean flag = false;
		String transact = "";
		LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		LLCaseSet tLLCaseSet = new LLCaseSet();
		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
		LJAGetSet tLJAGetSet = new LJAGetSet();
		transact = "INSERT||MAIN";
		// LLPersonListSchemaUntil mLLPersonListSchemaUntil=new
		// LLPersonListSchemaUntil();

		for (int i = 1; i <= mLLPersonMsgSet.size(); i++) {
			// LLPersonMsgSchema tLLPersonMsgSchema = new LLPersonMsgSchema();
			// tLLPersonMsgSchema.setSchema(mLLPersonMsgSet.get(i));
			String name = mLLPersonMsgSet.get(i).getName();
			String id = mLLPersonMsgSet.get(i).getID();
			// 1111111111
			String sqlStr = "select c.caseno,c.customername, c.customerno, m.realpay,  (select codename from ldcode where '1480054601000'='1480054601000' and  codetype='paymode' and code=c.casegetmode),  c.CustomerName,c.IDNo,c.bankcode,c.bankaccno,c.accname,c.casegetmode,c.rgtno,  case when c.prepaidflag='1' then '是' else '否' end , 0,(case when c.rgtstate in('11','12')  then m.realpay else 0 end) ,case when c.prepaidflag='1' then '1' else '0' end from llcase c, llclaim m,llregister r  where  c.caseno=m.caseno and c.rgtstate='09' and c.rgtno=r.rgtno and (r.togetherflag is null or r.togetherflag not in ('3','4'))  and c.mngcom like '"
					+ mLLSendMsgSchema.getManageCom()
					+ "%' and c.rgtno='"
					+ mLLRegisterSchema.getRgtNo()
					+ "'and c.customername='"
					+ name
					+ "' and  c.IDNo='"
					+ id
					+ "'  order by c.caseno fetch first 3000 rows only with ur ";
			// String
			// sqlStr="select c.caseno from llcase c where rgtno = '"+mLLRegisterSchema.getRgtNo()+"'";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS sqlStrSSRS = tExeSQL.execSQL(sqlStr);

			if (sqlStrSSRS.getMaxNumber() > 0) {
				String CaseNo = sqlStrSSRS.GetText(1, 1);
				String RgtNo = mLLRegisterSchema.getRgtNo();
				String PayMode = mLLPersonMsgSet.get(i).getReceiveMethod();
				String AccName = mLLPersonMsgSet.get(i).getAccountName();
				String BankAccNo = mLLPersonMsgSet.get(i).getAccount();
				String BankCode = mLLPersonMsgSet.get(i).getBankCode();
				String Drawer = mLLPersonMsgSet.get(i).getDrawer();
				String DrawerID = mLLPersonMsgSet.get(i).getDrawerID();
				String tPrePaidFlag = mLLPersonMsgSet.get(i).getAdvanceFlag();
				tVData = new VData();
				tLJAGetInsertBL = new LJAGetInsertBL();
				tLLCaseSchema = new LLCaseSchema();
				tLJAGetSchema = new LJAGetSchema();
				tLLCaseSchema.setCaseNo(CaseNo);
				tLLCaseSchema.setRgtNo(RgtNo);
				System.out.println("tPrePaidFlag[i]=======" + tPrePaidFlag);
				tLLCaseSchema.setPrePaidFlag(tPrePaidFlag);
				tLJAGetSchema.setOtherNo(CaseNo);
				tLJAGetSchema.setOtherNoType("5");
				tLJAGetSchema.setAccName(AccName);
				tLJAGetSchema.setBankAccNo(BankAccNo);
				tLJAGetSchema.setPayMode(PayMode);
				System.out.println("2222222222222:" + PayMode);
				tLJAGetSchema.setBankCode(BankCode);
				if (Drawer.equals("") || Drawer == null) {
					Drawer = AccName;
				}
				tLJAGetSchema.setDrawer(Drawer);
				tLJAGetSchema.setDrawerID(DrawerID);

				// 111111111110909
				// tLLCaseSchema.setRgtState("09");
				// String sql =
				// "update LLCase set rgtstate = '09' where CaseNo = '"+tLLCaseSchema.getCaseNo()+"'";
				// ExeSQL vExeSQL = new ExeSQL();
				// vExeSQL.execUpdateSQL(sql);
				// tG.Operator = "gf";

				tVData.add(tLLCaseSchema);
				tVData.add(tLJAGetSchema);
				tVData.add(tG);
				if (!tLJAGetInsertBL.submitData(tVData, transact)) {
					flag = false;
					tError = tLJAGetInsertBL.mErrors;
					Content += "<br>案件" + CaseNo + ",操作失败 "
							+ tError.getFirstError();
					FlagStr = "Fail";
					return flag;
				} else {
					flag = true;
					Content += "<br>案件" + CaseNo + "操作成功"
							+ tLJAGetInsertBL.getBackMsg();
					System.out
							.println("LLContAutoDealBL============Begin==========:");
					try {
						String tsql = "select 1 from llclaimdetail where caseno = '"
								+ CaseNo
								+ "' and riskcode in (select code from ldcode where codetype='lxb' ) and exists(select 1 from lcpol where polno=llclaimdetail.polno and polstate!='03070001')  with ur ";
						SSRS tSSRS = new ExeSQL().execSQL(tsql);

						if (tSSRS.MaxRow > 0) {
							LPSecondUWBL tLPSecondUWBL = new LPSecondUWBL();
							tLPSecondUWBL.submitData(tVData, "");
						} else {
							LLContAutoDealBL tLLContAutoDealBL = new LLContAutoDealBL();
							tLLContAutoDealBL.submitData(tVData, "");
							System.out.println(tLLContAutoDealBL.getBackMsg());
							Content += tLLContAutoDealBL.getBackMsg();
						}
						System.out.println("Content==" + Content);

					} catch (Exception tex) {
						flag = false;
						Content += "合同终止保存失败，原因是:" + tex.toString()
								+ "，请到理赔合同处理中进行操作!";
						return flag;
					}
				}
				System.out.println("循环次数" + i);
			}
		}
		return flag;

	}

	/**
	 * 团体理赔处理--》团体理赔处理--》批量导入--》团体申请完毕
	 */
	public boolean approveFinish() {
		if (!RgtFinish())// 团体申请完毕 前台校验
		{
			return false;
		}
		FlagStr = "";
		// tG.Operator = mLLSendMsgSchema.getOperate();
		// tG.ComCode = "8615";
		tG.ManageCom = mLLSendMsgSchema.getManageCom();
		if ("86360500".equals(tG.ManageCom)) {
			tG.Operator = "sc3672";
		}
		boolean flag = false;
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		CErrors tError = null;
		GrpRegisterBL tGrpRegisterBL = new GrpRegisterBL();
		VData tVData = new VData();
		String strOperate = "UPDATE||MAIN";
		String strSQL = " select count(*) from llcase where rgtno = '"
				+ mLLRegisterSchema.getRgtNo() + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS strSQLSSRS = tExeSQL.execSQL(strSQL);
		String realpeoples = "0";
		if (Integer.valueOf(strSQLSSRS.GetText(1, 1)) > 0) {
			realpeoples = strSQLSSRS.GetText(1, 1);
		}
		if (strOperate.equals("UPDATE||MAIN")) {
			tLLRegisterSchema.setRgtNo(mLLRegisterSchema.getRgtNo());
			tLLRegisterSchema.setAppPeoples(realpeoples);
			System.out.println("团体申请人数：" + realpeoples);
			tLLRegisterSchema.setRgtState("02");

			try {
				tVData.add(tLLRegisterSchema);
				tVData.add(tG);
				tGrpRegisterBL.submitData(tVData, strOperate);
				flag = true;
			} catch (Exception ex) {
				flag = false;
				Content = "保存失败，原因是:" + ex.toString();
				System.out.println("aaaa" + ex.toString());
				FlagStr = "Fail";
				return flag;
			}
			if (FlagStr == "") {
				tError = tGrpRegisterBL.mErrors;
				if (!tError.needDealError()) {
					flag = true;
					Content = "保存成功！";
					FlagStr = "Succ";
					tVData.clear();
					tVData = tGrpRegisterBL.getResult();
				} else {
					flag = false;
					Content = "保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
					return flag;
				}
			}
		}

		return flag;
	}

	/**
	 * 团体理赔处理--》团体理赔处理--》批量导入--》理算
	 */

	public boolean calClaimConfirm() {
		if (!CalClaim()) {
			return false;
		}
		// 111111115555
		// String sql =
		// "update llfeemain set realhospdate = 5 where caseno = (select caseno from llcase where rgtno = '"+mLLRegisterSchema.getRgtNo()+"')";
		// ExeSQL tExeSQL = new ExeSQL();
		// System.out.println("1111111");
		// System.out.println(tExeSQL.execUpdateSQL(sql));

		FlagStr = "";
		// tG.Operator = mLLSendMsgSchema.getOperate();
		// tG.ComCode = "8615";
		tG.ManageCom = mLLSendMsgSchema.getManageCom();
		if ("86360500".equals(tG.ManageCom)) {
			tG.Operator = "sc3672";
		}
		boolean flag = false;
		VData tVData = new VData();
		BatchClaimCalBL tBatchClaimCalBL = new BatchClaimCalBL();
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		tLLRegisterSchema.setRgtNo(mLLRegisterSchema.getRgtNo());
		try {
			tVData.add(tLLRegisterSchema);
			tVData.add(tG);
			tBatchClaimCalBL.submitData(tVData, "cal");
		} catch (Exception ex) {
			flag = false;
			Content = "理算确认失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			return flag;
		}
		if (FlagStr == "") {
			tError = tBatchClaimCalBL.mErrors;
			if (!tError.needDealError()) {
				flag = true;
				Content = "理算确认成功！";
				FlagStr = "Succ";
				tVData.clear();
				tVData = tBatchClaimCalBL.getResult();
			} else {
				flag = false;
				Content = "理算确认失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
				return flag;
			}
		}
		return flag;
	}

	/**
	 * 给付确认 后台
	 * 
	 * @return
	 */
	public boolean giveBatch() {
		if (!calClaimConfirm()) {
			System.out.println("------------操作 理算 -----------");
			return false;
		}
		if (!approveFinish()) {
			System.out.println("------------操作团体申请完毕失败----------原因：" + Content);
			return false;
		}
		if (!batchAuthorize()) {
			System.out.println("------------操作批量审定----------原因：" + Content);
			return false;
		}
		if ("1".equals(mLLSendMsgSchema.getPayMethod())) {
			boolean giveConfalg = giveCon();
			if (!giveConfalg) {
				System.out
						.println("----------给付确认（给付方式是个人给付)---------------原因："
								+ Content);
			}
			return giveConfalg;
		} else {
			boolean GiveEnsurefalg = GiveEnsure();
			if (!GiveEnsurefalg) {
				System.out
						.println("----------团体给付确认（给付方式不是个人给付）------------原因："
								+ Content);
			}
			return GiveEnsurefalg;
		}
	}

	public static void main(String[] args) {
		// DealClaim a =new DealClaim();
		// a.ftpDownload() ;
		// DealClaim a =new DealClaim();

		// LLPersonMsgSchema m1LLPersonMsgSchema=new LLPersonMsgSchema();
		// m1LLPersonMsgSchema.setReceiveMethod("1");
		// m1LLPersonMsgSchema.setAccountName("哈哈");
		// m1LLPersonMsgSchema.setAccount("6552001");
		// m1LLPersonMsgSchema.setBankCode("0013");
		// m1LLPersonMsgSchema.setDrawer("检验11");
		// m1LLPersonMsgSchema.setDrawerID("170184880105321");
		// m1LLPersonMsgSchema.setAdvanceFlag("0");
		// m1LLPersonMsgSchema.setName("赵六");
		// m1LLPersonMsgSchema.setID("170184880517529");
		// a.mLLPersonMsgSet.add(m1LLPersonMsgSchema);
		// a.batchAuthorize();
		// a.a();

		/*
		 * if(!a.calClaimConfirm()){ System.out.println("理算操作失败"); }else{
		 * a.approveFinish(); System.out.println(Content); }
		 */

		/*
		 * mLLRegisterSchema.setRgtNo(""); a.StateRadio="1";
		 * a.RgtNo="P1500161124000002"; a.Operate="cm0002";
		 * a.GEOperate="cm1102"; a.ManageCom="86"; a.GEManageCom="86";
		 * a.TogetherFlag="1"; String falg=a.batchAuthorize();
		 * System.out.println(falg); a.PrePaidFlag="1";
		 */

	}

	/**
	 * 理算删除数据
	 */
	public boolean deleteCal() {
		MMap map = new MMap();
		String sql = "";
		sql = "select caseno from llcase where rgtno = '"
				+ mLLRegisterSchema.getRgtNo() + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sqlSSRS = tExeSQL.execSQL(sql);
		for (int i = 0; i < sqlSSRS.getMaxRow(); i++) {
			String sql2 = "delete from LLCaseOpTime where caseno = '"
					+ sqlSSRS.GetText(i + 1, 1) + "'";
			map.put(sql2, "DELETE");
		}
		String sql3 = "delete from LLClaimDetail where rgtno = '"
				+ mLLRegisterSchema.getRgtNo() + "'";
		map.put(sql3, "DELETE");
		String sql4 = "delete from LLClaim where rgtno = '"
				+ mLLRegisterSchema.getRgtNo() + "'";
		map.put(sql4, "DELETE");
		String sql5 = "delete from LLClaimPolicy where rgtno = '"
				+ mLLRegisterSchema.getRgtNo() + "'";
		map.put(sql5, "DELETE");
		VData mInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		mInputData.add(map);
		return tPubSubmit.submitData(mInputData, null);
	}

	/**
	 * 理赔审定
	 * 
	 * @return
	 */
	public boolean deleteData() {
		MMap map = new MMap();
		// MMap tMap = new MMap();
		String mRgtno = mLLRegisterSchema.getRgtNo();
		// mRgtno="P1500161205000003";
		String sql0 = "select caseno from LLCase where rgtno='" + mRgtno + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sql0SSRS = tExeSQL.execSQL(sql0);
		// 审定
		if (sql0SSRS.getMaxNumber() > 0) {
			for (int i = 0; i < sql0SSRS.getMaxRow(); i++) {
				String mcaseno = sql0SSRS.GetText(i + 1, 1);
				String ClmNoSql = "select ClmNo from LLClaim where CaseNo='"
						+ mcaseno + "'";
				SSRS ClmNoSqlSSRS = tExeSQL.execSQL(ClmNoSql);
				if (ClmNoSqlSSRS.getMaxNumber() > 0) {
					String mClmNo = ClmNoSqlSSRS.GetText(1, 1);
					String LLClaimPolicysql = "select * from LLClaimPolicy where ClmNo='"
							+ mClmNo + "'";
					SSRS LLClaimPolicysqlSSRS = tExeSQL
							.execSQL(LLClaimPolicysql);
					if (LLClaimPolicysqlSSRS.getMaxNumber() > 0) {
						for (int j = 0; j < LLClaimPolicysqlSSRS.getMaxRow(); j++) {
							String delLLClaimUnderwrite = "delete from LLClaimUnderwrite where ClmNo='"
									+ mClmNo + "' and CaseNo='" + mcaseno + "'";
							String delLLClaimUWDetail = "delete from LLClaimUWDetail where ClmNo='"
									+ mClmNo + "' and CaseNo='" + mcaseno + "'";
							map.put(delLLClaimUnderwrite, "DELETE");
							map.put(delLLClaimUWDetail, "DELETE");
							String delLLClaimUWMDetail = "delete from LLClaimUWMDetail where ClmNo='"
									+ mClmNo + "' and CaseNo='" + mcaseno + "'";
							map.put(delLLClaimUWMDetail, "DELETE");
							String delLLClaimUWMain = "delete from LLClaimUWMain where  RgtNo='"
									+ mRgtno
									+ "' and CaseNo='"
									+ mcaseno
									+ "' and ClmNo='" + mClmNo + "'";
							map.put(delLLClaimUWMain, "DELETE");
						}
					}
				}
				String delLLCaseOpTime = "delete from LLCaseOpTime  where CaseNo='"
						+ mcaseno + "'";
				map.put(delLLCaseOpTime, "DELETE");
				String delsql1 = "delete from llclaimpolicy where caseno='"
						+ mcaseno + "'";
				String delsql2 = "delete from llclaimdetail where caseno='"
						+ mcaseno + "'";
				String delsql3 = "delete from ljsgetclaim where otherno='"
						+ mcaseno + "'";
				String delsql4 = "delete from ljsget where otherno='" + mcaseno
						+ "'";
				map.put(delsql1, "DELETE");
				map.put(delsql2, "DELETE");
				map.put(delsql3, "DELETE");
				map.put(delsql4, "DELETE");
			}
		}
		VData mInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		mInputData.add(map);
		return tPubSubmit.submitData(mInputData, null);
	}

	/**
	 * 导入数据列表维护删除
	 */
	public boolean applyDelete() {
		MMap map = new MMap();
		String delsql1 = " delete from llregister where rgtno= '"
				+ mLLRegisterSchema.getRgtNo() + "' ";
		// String
		// delsql2=" delete from ldstoreno where rgtno= '"+mLLRegisterSchema.getRgtNo()+"' ";
		// //这个表在后台没找到
		map.put(delsql1, "DELETE");
		VData mInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		mInputData.add(map);

		return tPubSubmit.submitData(mInputData, null);
	}

	/**
	 * 批量导入删除
	 * 
	 * @return
	 */
	public boolean importCustomerDelete() {
		MMap map = new MMap();

		String rgtNo = mLLRegisterSchema.getRgtNo();
		String exesql0 = " select caseno from LLCase where rgtno='" + rgtNo
				+ "' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sql0SSRS = tExeSQL.execSQL(exesql0);
		// 批量导入
		if (sql0SSRS.getMaxNumber() > 0) {
			for (int i = 0; i < sql0SSRS.getMaxRow(); i++) {
				String delsqlLLCaseRela = " DELETE FROM LLCASERELA WHERE CASENO='"
						+ sql0SSRS.GetText(i + 1, 1) + "' ";
				String delsqlLLCaseCure = " DELETE FROM LLCASECURE WHERE CASENO='"
						+ sql0SSRS.GetText(i + 1, 1) + "' ";
				String delsqlLLToClaimDuty = " DELETE FROM LLTOCLAIMDUTY WHERE CASENO='"
						+ sql0SSRS.GetText(i + 1, 1) + "' ";
				String exesql1 = " select customername,accidenttype,accdate from LLSubReport where subrptno in(select subrptno from llcaserela where caseno='"
						+ sql0SSRS.GetText(i + 1, 1) + "') ";
				SSRS sql1SSRS = tExeSQL.execSQL(exesql1);
				String customername = sql1SSRS.GetText(1, 1);
				String accidenttype = sql1SSRS.GetText(1, 2);
				String accdate = sql1SSRS.GetText(1, 3);
				String delsqlLLSubReport = " DELETE FROM LLSUBREPORT WHERE CUSTOMERNAME='"
						+ customername
						+ "' AND ACCIDENTTYPE='"
						+ accidenttype
						+ "' AND ACCDATE='" + accdate + "' ";
				map.put(delsqlLLCaseRela, "DELETE");
				map.put(delsqlLLCaseCure, "DELETE");
				map.put(delsqlLLToClaimDuty, "DELETE");
				map.put(delsqlLLSubReport, "DELETE");
			}

		}
		String delsqlLLCase = " DELETE FROM LLCASE WHERE RGTNO='" + rgtNo
				+ "' ";
		String delsqlLLFeeMain = " DELETE FROM LLFEEMAIN WHERE RGTNO='" + rgtNo
				+ "' ";
		String delsqlLLSecurityReceipt = " DELETE FROM LLSECURITYRECEIPT WHERE RGTNO='"
				+ rgtNo + "' ";
		String delsqlLLAppClaimReason = " DELETE FROM LLAPPCLAIMREASON WHERE RGTNO='"
				+ rgtNo + "' ";
		String delsqlLLCaseReceipt = " DELETE FROM LLCASERECEIPT WHERE RGTNO='"
				+ rgtNo + "' ";
		String delsqlLLFeeOtherItem1 = " DELETE FROM LLFeeOtherItem WHERE RGTNO='"
				+ rgtNo + "' ";
		// String delsqlLLFeeOtherItem2 = "  ";
		// String delsqlLLFeeOtherItem3 = "  ";
		// String delsqlLLFeeOtherItem4 = "  ";
		// String delsqlLLFeeOtherItem5 = "  ";
		// String delsqlLLCaseOpTime = "  ";//这张表不需要删除【案件操作停留时效】
		String delsqlLLCasePolicy = " DELETE FROM LLCASEPOLICY WHERE RGTNO='"
				+ rgtNo + "' ";
		map.put(delsqlLLCase, "DELETE");
		map.put(delsqlLLFeeMain, "DELETE");
		map.put(delsqlLLSecurityReceipt, "DELETE");
		map.put(delsqlLLAppClaimReason, "DELETE");
		map.put(delsqlLLCaseReceipt, "DELETE");
		map.put(delsqlLLFeeOtherItem1, "DELETE");
		map.put(delsqlLLCasePolicy, "DELETE");
		VData mInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		mInputData.add(map);

		return tPubSubmit.submitData(mInputData, null);
	}

	/**
	 * 返回报文
	 * 
	 * @return String
	 */
	public String getXmlResult(boolean flag, String desc) {
		StringWriter stringWriter = new StringWriter();
		Document document = DocumentHelper.createDocument();
		Element tDateSet = DocumentHelper
				.createElement("MedicalAdjustment_Request");
		document.setRootElement(tDateSet);
		// 返回报文头样式
		Element tMsgResHead = tDateSet.addElement("head");
		Element tBatchNo = tMsgResHead.addElement("BatchNo");
		Element tSendDate = tMsgResHead.addElement("SendDate");
		Element tSendTime = tMsgResHead.addElement("SendTime");
		Element tBranchCode = tMsgResHead.addElement("BranchCode");
		Element tSendOperator = tMsgResHead.addElement("SendOperator");
		Element tMsgType = tMsgResHead.addElement("MsgType");
		Element tRownum = tMsgResHead.addElement("Rownum");
		// 返回报文体样式
		Element tBody = tDateSet.addElement("ApplyNo");
		Element tSuccessFlag = tBody.addElement("SuccessFlag");
		Element tDescription = tBody.addElement("Description");
		String actuGetNo = null;
		// 报文内容填充
		try {
			// tBatchNo.addText(mLLSendMsgSchema.getBatchNo());
			String sql = "select VARCHAR(Responsexml) from ErrorInsertLog where transactionBatch = '"
					+ mLLSendMsgSchema.getBatchNo()
					+ "' and transactionCode = '"
					+ mLLSendMsgSchema.getBranchCode() + "'";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS sql1SSRS = tExeSQL.execSQL(sql);
			System.out.println("开始拼装报文*************");
			if (sql1SSRS.getMaxRow() > 0) {
				if (sql1SSRS.GetText(1, 1) != null
						&& !"".equals(sql1SSRS.GetText(1, 1))) {
					tBatchNo.addText("");
				}
			} else {
				if ("".equals(mLLRegisterSchema.getRgtNo())
						|| null == mLLRegisterSchema.getRgtNo()) {
					tBatchNo.addText(mLLSendMsgSchema.getBatchNo());
				} else {
					tBatchNo.addText(mLLRegisterSchema.getRgtNo());
				}
				// 返回实付号（批次号）
				String sql3 = "select ActuGetNo from LJAGet where otherno='"
						+ tBatchNo.getText() + "'";
				SSRS sql3SSRS = tExeSQL.execSQL(sql3);
				if (sql3SSRS.getMaxRow() > 0) {
					actuGetNo = sql3SSRS.GetText(1, 1);
				}
				// 返回每个出险人账单号和案件号
				String sql2 = "select caseno,ReceiptNo from LLFeeMain where rgtno='"
						+ mLLRegisterSchema.getRgtNo() + "'";
				SSRS sql2SSRS = tExeSQL.execSQL(sql2);
				if (sql2SSRS.getMaxRow() > 0) {
					for (int i = 1; i <= sql2SSRS.getMaxRow(); i++) {
						String caseno = sql2SSRS.GetText(i, 1);
						String receiptNo = sql2SSRS.GetText(i, 2);
						Element detail = tBody.addElement("detail");
						Element Caseno = detail.addElement("Caseno");
						Element ReceiptNo = detail.addElement("ReceiptNo");
						Element tActuGetNo = detail.addElement("ActuGetNo");
						Caseno.addText(caseno);
						ReceiptNo.addText(receiptNo);
						// 返回实付号（案件号）
						String sql4 = "select ActuGetNo from LJAGet where otherno='"
								+ Caseno + "'";
						SSRS sql4SSRS = tExeSQL.execSQL(sql4);
						if (sql4SSRS.getMaxRow() > 0) {
							actuGetNo = sql4SSRS.GetText(1, 1);
							tActuGetNo.addText(actuGetNo);
						}else {
							tActuGetNo.addText(actuGetNo);
						}
						
					}
				}
			}

			tSendDate.addText(PubFun.getCurrentDate());
			tSendTime.addText(PubFun.getCurrentTime());
			tBranchCode.addText(mLLSendMsgSchema.getBranchCode());
			tSendOperator.addText(mLLSendMsgSchema.getOperate());
			tMsgType.addText(mLLSendMsgSchema.getMsgType());
			tRownum.addText(Integer.toString(mLLSendMsgSchema.getRownum()));
			if (flag) {
				tSuccessFlag.addText("1");
			} else {
				tSuccessFlag.addText("0");
			}
			tDescription.addText(desc);
			OutputFormat format = new OutputFormat("    ", true);
			// 设置编码
			format.setEncoding("GBK");
			// 设置换行
			format.setNewlines(true);
			// 生成缩进
			format.setIndent(true);
			// 创建写文件方法
			XMLWriter xmlWriter = new XMLWriter(stringWriter, format);
			// 写入文件
			xmlWriter.write(document);
			// 关闭
			xmlWriter.close();
			// 输出xml
			System.out.println(stringWriter.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		responseXml = stringWriter.toString();
		return stringWriter.toString();
	}

}
