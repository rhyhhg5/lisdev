package com.finfee;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.contract.obj.MessageHead;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class FinFeePay {

	private Logger log=Logger.getLogger(TempFeeSave.class);
	private VData vdata = new VData();
	public ExeSQL exeSQL=new ExeSQL();
	
	SimpleDateFormat sdf1 =   new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf2 =   new SimpleDateFormat("HH:mm:ss");
	Date   curDate   =   new   Date(System.currentTimeMillis());//获取当前时间  
	
	public String parse(String strxml) throws JDOMException, IOException{
		log.info(strxml);
		StringReader reader = new StringReader(strxml);
		
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document doc = tSAXBuilder.build(reader);
		Element rootElement = doc.getRootElement();
		// 解析报文头
		Element msgHead = rootElement.getChild("MsgHead");
		Element elehead = msgHead.getChild("Item");
		String BatchNo = elehead.getChildText("BatchNo");
		String SendDate = elehead.getChildText("SendDate");
		String SendTime = elehead.getChildText("SendTime");
		String BranchCode = elehead.getChildText("BranchCode");
		String SendOperator = elehead.getChildText("SendOperator");
		String MsgType = elehead.getChildText("MsgType");

		MessageHead head = new MessageHead();
		head.setBatchNo(BatchNo);
		head.setSendDate(SendDate);
		head.setSendTime(SendTime);
		head.setBranchCode(BranchCode);
		head.setSendOperator(SendOperator);
		head.setMsgType(MsgType);
		vdata.add(head);
		if("LP".equals(MsgType)){
			boolean b1=false;
			boolean b2=false;
			try{
				 b1=new getLiaget().getLiaget(rootElement);
				 b2=new getLJAGetClaim().getLJAGetClaim(rootElement);
				 if(b1&&b2){
					 return returnxml("true","");
				 }else{
					 return returnxml("false","");
				 }
			}catch (Exception e) {
				// TODO: handle exception
				 return returnxml("false","");
			}finally{
				if(b1&b2){
					
				}else{
					String ActuGetNo=rootElement.getChild("MsgBody").getChild("LJAGET").getChildText("ActuGetNo");
					String ActuGetNo1=rootElement.getChild("MsgBody").getChild("LJAGetClaim").getChildText("ActuGetNo");
					String dsql="delete from ljaget where ActuGetNo='"+ActuGetNo+"'";
					String dsql1="delete from LJAGetClaim where ActuGetNo='"+ActuGetNo1+"'";
					exeSQL.execUpdateSQL(dsql);
					exeSQL.execUpdateSQL(dsql1);
				}
			}
		}else if("BQ".equals(MsgType)){
			boolean b1=false;
			boolean b2=false;
			try{
			 b1=new getLiaget().getLiaget(rootElement);
			 b2=new getLJSGETENDORSE().setBQ(rootElement);
			 if(b1&&b2){
				 return returnxml("true",""); 
			 }else{
				 return returnxml("false","");
			 }
			}catch (Exception e) {
				return returnxml("false","");
			}finally{
				if(b1&&b2){
				}else{
					String ActuGetNo=rootElement.getChild("MsgBody").getChild("LJAGET").getChildText("ActuGetNo");
					String ActuGetNo1=rootElement.getChild("MsgBody").getChild("LJAGETENDORSE").getChildText("ActuGetNo");
					String dsq="delete from ljaget where ActuGetNo='"+ActuGetNo+"'";
					String dsq1="delete from LJAGETENDORSE where ActuGetNo='"+ActuGetNo1+"'";
					exeSQL.execUpdateSQL(dsq);
					exeSQL.execUpdateSQL(dsq1);
				}
			}
			 
		}
		return returnxml("false","");
		
		
		
		
	}
	class getLiaget{
		public  getLiaget(){}
		public boolean getLiaget(Element rootElement){
			Element LJAGET=rootElement.getChild("MsgBody").getChild("LJAGET");
			
			String ActuGetNo=LJAGET.getChildText("ActuGetNo");
			String OtherNo=LJAGET.getChildText("OtherNo");
			String OtherNoType=LJAGET.getChildText("OtherNoType");
			String PayMode=LJAGET.getChildText("PayMode");
			String BankOnTheWayFlag=LJAGET.getChildText("BankOnTheWayFlag");
			String BankSuccFlag=LJAGET.getChildText("BankSuccFlag");
			String SendBankCount=LJAGET.getChildText("SendBankCount");
			String ManageCom=LJAGET.getChildText("ManageCom");
			String AgentCom=LJAGET.getChildText("AgentCom");
			String AgentCode=LJAGET.getChildText("AgentCode");
			String AgentType=LJAGET.getChildText("AgentType");
			String AgentGroup=LJAGET.getChildText("AgentGroup");
			String AccName=LJAGET.getChildText("AccName");
			String StartGetDate=LJAGET.getChildText("StartGetDate");
			String AppntNo=LJAGET.getChildText("AppntNo");
			String SumGetMoney=LJAGET.getChildText("SumGetMoney");
			String SaleChnl=LJAGET.getChildText("SaleChnl");
			String ShouldDate=LJAGET.getChildText("ShouldDate");
			String EnterAccDate=null;//LJAGET.getChildText("EnterAccDate");
			String ConfDate= null;//LJAGET.getChildText("ConfDate");
			String ApproveCode=LJAGET.getChildText("ApproveCode");
			String ApproveDate=LJAGET.getChildText("ApproveDate");
			String GetNoticeNo=LJAGET.getChildText("GetNoticeNo");
			String BankCode=LJAGET.getChildText("BankCode");
			String BankAccNo=LJAGET.getChildText("BankAccNo");
			String Drawer=LJAGET.getChildText("Drawer");
			String DrawerID=LJAGET.getChildText("DrawerID");
			String SerialNo=LJAGET.getChildText("SerialNo");
			String Operator=LJAGET.getChildText("Operator");
			String MakeDate=sdf1.format(curDate);
			String MakeTime=sdf2.format(curDate);
			String ModifyDate=LJAGET.getChildText("ModifyDate");
			String ModifyTime=LJAGET.getChildText("ModifyTime");
			String InsBankCode=LJAGET.getChildText("InsBankCode");
			String InsBankAccNo=LJAGET.getChildText("InsBankAccNo");
			String ChequeNo=LJAGET.getChildText("ChequeNo");
			String CanSendBank=LJAGET.getChildText("CanSendBank");
			String FinState=LJAGET.getChildText("FinState");
			
			String sql="insert into ljaget values" +
					"('"+ActuGetNo+"','"+OtherNo+"','"+OtherNoType+"','"+PayMode+"','"+BankOnTheWayFlag+"','"+BankSuccFlag+"','"+SendBankCount+"'," +
					"'"+ManageCom+"','"+AgentCom+"','"+AgentType+"','"+AgentCode+"','"+AgentGroup+"','"+AccName+"','"+StartGetDate+"','"+AppntNo+"'," +
					"'"+SumGetMoney+"','"+SaleChnl+"','"+ShouldDate+"',"+EnterAccDate+","+ConfDate+",'"+ApproveCode+"','"+ApproveDate+"','"+GetNoticeNo+"'," +
					"'"+BankCode+"','"+BankAccNo+"','"+Drawer+"','"+DrawerID+"','"+SerialNo+"','"+Operator+"','"+MakeDate+"','"+MakeTime+"','"+ModifyDate+"'," +
					"'"+ModifyTime+"','"+InsBankCode+"','"+InsBankAccNo+"','"+ChequeNo+"','"+CanSendBank+"','"+FinState+"')";
			boolean b=false;
				b=exeSQL.execUpdateSQL(sql);
				return b;
			
			
		}
	}
	class getLJAGetClaim{
		public getLJAGetClaim(){}
		public  boolean getLJAGetClaim(Element rootElement){
			Element LJAGetClaim=rootElement.getChild("MsgBody").getChild("LJAGetClaim");
			
			String ActuGetNo=LJAGetClaim.getChildText("ActuGetNo");
			String FeeFinaType=LJAGetClaim.getChildText("FeeFinaType");
			String FeeOperationType=LJAGetClaim.getChildText("FeeOperationType");
			String OtherNo=LJAGetClaim.getChildText("OtherNo");
			String OtherNoType=LJAGetClaim.getChildText("OtherNoType");
			String GetDutyCode=LJAGetClaim.getChildText("GetDutyCode");
			String GetDutyKind=LJAGetClaim.getChildText("GetDutyKind");
			String GrpContNo=LJAGetClaim.getChildText("GrpContNo");
			String ContNo=LJAGetClaim.getChildText("ContNo");
			String GrpPolNo=LJAGetClaim.getChildText("GrpPolNo");
			String PolNo=LJAGetClaim.getChildText("PolNo");
			String KindCode=LJAGetClaim.getChildText("KindCode");
			String RiskCode=LJAGetClaim.getChildText("RiskCode");
			String RiskVersion=LJAGetClaim.getChildText("RiskVersion");
			String SaleChnl=LJAGetClaim.getChildText("SaleChnl");
			String AgentCode=LJAGetClaim.getChildText("AgentCode");
			String AgentGroup=LJAGetClaim.getChildText("AgentGroup");
			
			String GetDate=LJAGetClaim.getChildText("GetDate");
			String EnterAccDate=LJAGetClaim.getChildText("EnterAccDate");
			String ConfDate=LJAGetClaim.getChildText("ConfDate");
			String Pay=LJAGetClaim.getChildText("Pay");
			String ManageCom=LJAGetClaim.getChildText("ManageCom");
			String AgentCom=LJAGetClaim.getChildText("AgentCom");
			String AgentType=LJAGetClaim.getChildText("AgentType");
			String GetNoticeNo=LJAGetClaim.getChildText("GetNoticeNo");
			String OPConfirmCode=LJAGetClaim.getChildText("OPConfirmCode");
			String OPConfirmDate=LJAGetClaim.getChildText("OPConfirmDate");
			String OPConfirmTime=LJAGetClaim.getChildText("OPConfirmTime");
			String SerialNo=LJAGetClaim.getChildText("SerialNo");
			String Operator=LJAGetClaim.getChildText("Operator");
			String MakeDate=sdf1.format(curDate);
			String MakeTime=sdf2.format(curDate);
			String ModifyDate=LJAGetClaim.getChildText("ModifyDate");
			String ModifyTime=LJAGetClaim.getChildText("ModifyTime");
			String sql="insert into LJAGetClaim values" +
					"('"+ActuGetNo+"','"+FeeFinaType+"','"+FeeOperationType+"','"+OtherNo+"','"+OtherNoType+"','"+GetDutyCode+"','"+GetDutyKind+"','"+GrpContNo+"'," +
					"'"+ContNo+"','"+GrpPolNo+"','"+PolNo+"','"+KindCode+"','"+RiskCode+"','"+RiskVersion+"','"+SaleChnl+"','"+AgentCode+"','"+AgentGroup+"','"+GetDate+"','"+EnterAccDate+"','"+ConfDate+"'," +
					"'"+Pay+"','"+ManageCom+"','"+AgentCom+"','"+AgentType+"','"+GetNoticeNo+"','"+OPConfirmCode+"','"+OPConfirmDate+"','"+OPConfirmTime+"','"+SerialNo+"','"+Operator+"'," +
					"'"+MakeDate+"','"+MakeTime+"','"+ModifyDate+"','"+ModifyTime+"')";
			
			boolean b=false;
				b=exeSQL.execUpdateSQL(sql);
				return b;
			
		}
		
	}
	/**
	 * @author dongjl
	 *
	 * 保全付费子表数据插入
	 */
	class getLJSGETENDORSE{
		public boolean setBQ(Element rootElement){
			Element LJAGETENDORSE = rootElement.getChild("MsgBody").getChild("LJAGETENDORSE");
			String ActuGetNo=LJAGETENDORSE.getChildText("ActuGetNo");
			String EndorsementNo=LJAGETENDORSE.getChildText("EndorsementNo");
			String FeeOperationType=LJAGETENDORSE.getChildText("FeeOperationType");
			String FeeFinaType	=LJAGETENDORSE.getChildText("FeeFinaType");
			String GrpContNo=LJAGETENDORSE.getChildText("GrpContNo");
			String ContNo=LJAGETENDORSE.getChildText("ContNo");
			String GrpPolNo=LJAGETENDORSE.getChildText("GrpPolNo");
			String PolNo=LJAGETENDORSE.getChildText("PolNo");
			String OtherNo=LJAGETENDORSE.getChildText("OtherNo");
			String OtherNoType=LJAGETENDORSE.getChildText("OtherNoType");
			String DutyCode=LJAGETENDORSE.getChildText("DutyCode");
			String PayPlanCode=LJAGETENDORSE.getChildText("PayPlanCode");
			String AppntNo=LJAGETENDORSE.getChildText("AppntNo");
			String InsuredNo=LJAGETENDORSE.getChildText("InsuredNo");
			String GetNoticeNo=LJAGETENDORSE.getChildText("GetNoticeNo");
			String GetDate=LJAGETENDORSE.getChildText("GetDate");
			String EnterAccDate=LJAGETENDORSE.getChildText("EnterAccDate");
			String GetConfirmDate=LJAGETENDORSE.getChildText("GetConfirmDate");
			String GetMoney=LJAGETENDORSE.getChildText("GetMoney");
			String KindCode=LJAGETENDORSE.getChildText("KindCode");
			String RiskCode=LJAGETENDORSE.getChildText("RiskCode");
			String RiskVersion=LJAGETENDORSE.getChildText("RiskVersion");
			String ManageCom=LJAGETENDORSE.getChildText("ManageCom");
			String AgentCom=LJAGETENDORSE.getChildText("AgentCom");
			String AgentType=LJAGETENDORSE.getChildText("AgentType");
			String AgentCode=LJAGETENDORSE.getChildText("AgentCode");
			String AgentGroup=LJAGETENDORSE.getChildText("AgentGroup");
			String GrpName=LJAGETENDORSE.getChildText("GrpName");
			String Handler=LJAGETENDORSE.getChildText("Handler");
			String ApproveDate=LJAGETENDORSE.getChildText("ApproveDate");
			String ApproveTime=LJAGETENDORSE.getChildText("ApproveTime");
			String PolType=LJAGETENDORSE.getChildText("PolType");
			String ApproveCode=LJAGETENDORSE.getChildText("ApproveCode");
			String Operator=LJAGETENDORSE.getChildText("Operator");
			String SerialNo=LJAGETENDORSE.getChildText("SerialNo");
			String ModifyDate=LJAGETENDORSE.getChildText("ModifyDate");
			String MakeDate=sdf1.format(curDate);
			String MakeTime=sdf2.format(curDate);
			String GetFlag=LJAGETENDORSE.getChildText("GetFlag");
			String ModifyTime=LJAGETENDORSE.getChildText("ModifyTime");
			String FinState=LJAGETENDORSE.getChildText("FinState");
			String MONEYNOTAX=LJAGETENDORSE.getChildText("MONEYNOTAX");
			String MONEYTAX=LJAGETENDORSE.getChildText("MONEYTAX");
			String BUSTTYPE=LJAGETENDORSE.getChildText("BUSTTYPE");
			String TAXTATE=LJAGETENDORSE.getChildText("TAXTATE");
			String sql="insert into LJAGETENDORSE values('"+ActuGetNo+"','"+EndorsementNo+"','"+FeeOperationType+"','"+FeeFinaType+"','"+GrpContNo+"','"+ContNo+"','"+GrpPolNo+"','"+PolNo+"','"+OtherNo+"','"+OtherNoType+"'," +
					"'"+DutyCode+"','"+PayPlanCode+"','"+AppntNo+"','"+InsuredNo+"','"+GetNoticeNo+"','"+GetDate+"','"+EnterAccDate+"','"+GetConfirmDate+"','"+GetMoney+"','"+KindCode+"'," +
					"'"+RiskCode+"','"+RiskVersion+"','"+ManageCom+"','"+AgentCom+"','"+AgentType+"','"+AgentCode+"','"+AgentGroup+"','"+GrpName+"','"+Handler+"','"+ApproveDate+"'," +
					"'"+ApproveTime+"','"+PolType+"','"+ApproveCode+"','"+Operator+"','"+SerialNo+"','"+ModifyDate+"','"+MakeDate+"','"+MakeTime+"','"+GetFlag+"','"+ModifyTime+"','"+FinState+"'," +
					"'"+MONEYNOTAX+"','"+MONEYTAX+"','"+BUSTTYPE+"','"+TAXTATE+"')";
			
			return exeSQL.execUpdateSQL(sql);
		}
	}
	

	public static void main(String[] str) throws IOException, JDOMException {

		InputStream pIns = new FileInputStream("C:/Users/dongjl/Desktop/ff.xml");
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		byte[] tBytes = new byte[8 * 1024];
		for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
			mByteArrayOutputStream.write(tBytes, 0, tReadSize);
		}
		String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "utf-8");
		FinFeePay ffp=new FinFeePay();
		String s=ffp.parse(mInXmlStr);
		System.out.println(s);
	}
	
	
	private String returnxml(String state,String Content) throws IOException{
		Element root = new Element("DataSet");
		Document doc = new Document(root);
		Element eMsgHead = new Element("MsgHead");
		Element estate = new Element("state");
		estate.addContent(state);
		Element eContent = new Element("Content");
		eContent.addContent(Content);
		eMsgHead.addContent(estate);
		eMsgHead.addContent(eContent);
		root.addContent(eMsgHead);
		XMLOutputter out = new XMLOutputter();
		String responseXml = out.outputString(doc);
		return responseXml;
	}
}
