package com.finfee;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element; 
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.grouppol.GroupPolSave;
import com.preservation.utilty.StringUtil;
import com.sinosoft.lis.operfee.GrpLjsPlanConfirmUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.taskservice.BqFinishTask;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author dongjiali
 *
 *查询新单收费信息接口
 *
 */
public class TempFeeQuery {
	private Logger log=Logger.getLogger(TempFeeQuery.class);
	SimpleDateFormat ymd = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
	SimpleDateFormat hms = new SimpleDateFormat("HH:mm:ss");//设置日期格式
	public String TempInfoQuery(String xml) throws JDOMException, IOException{
		String responseXml="";
		//组装返回报文bady节点
		Element bady_Response=new Element("body");
		Element Result=new Element("Result");// 处理结果 0-成功 1-失败
		Element ResultRemark=new Element("ResultRemark");//失败原因说明
		//创建返回报文
		Element Tempfee_Response=new Element("Tempfee_Response");
		try{
			log.info(xml+"+++++++++++++++++====");
			ExeSQL exeSQL=new ExeSQL();
			StringReader reader = new StringReader(xml);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document doc = tSAXBuilder.build(reader);
			Element rootElement = doc.getRootElement();
			Element head=rootElement.getChild("head");
			Element body=rootElement.getChild("body");
			
			//组装返回报文bead节点
			Element head_Response=new Element("head");
			Element BatchNo=new Element("BatchNo");
			if(!StringUtil.StringNull(head.getChildText("BatchNo"))&&
					!StringUtil.StringNull(head.getChildText("SendDate"))&&
					!StringUtil.StringNull(head.getChildText("SendTime"))&&
					!StringUtil.StringNull(head.getChildText("BranchCode"))&&
					!StringUtil.StringNull(head.getChildText("SendOperator"))&&
					!StringUtil.StringNull(head.getChildText("MsgType"))){
				
				Result.setText("1");
				ResultRemark.setText("没有查到相关数据");
				bady_Response.addContent(ResultRemark);
				bady_Response.addContent(Result);
				XMLOutputter out = new XMLOutputter();
				 responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
				return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
			}
			BatchNo.setText(head.getChildText("BatchNo"));//批次号
			head_Response.addContent(BatchNo);
			
			Element SendDate=new Element("SendDate");
			SendDate.setText(head.getChildText("SendDate"));//发送日期
			head_Response.addContent(SendDate);
			
			Element SendTime=new Element("SendTime");
			SendTime.setText(head.getChildText("SendTime"));//发送时间
			head_Response.addContent(SendTime);
			
			Element BranchCode=new Element("BranchCode");
			BranchCode.setText(head.getChildText("BranchCode"));//交易编码
			head_Response.addContent(BranchCode);
			
			Element SendOperator=new Element("SendOperator");
			SendOperator.setText(head.getChildText("SendOperator"));//交易人员
			head_Response.addContent(SendOperator);
			
			Element MsgType=new Element("MsgType");
			MsgType.setText(head.getChildText("MsgType"));//固定值
			head_Response.addContent(MsgType);
			
			Tempfee_Response.addContent(head_Response);
			
			Tempfee_Response.addContent(bady_Response);
			List<Element> list=body.getChildren("detail");
			for(int i=0;i<list.size();i++){
				String BussType=list.get(i).getChildTextTrim("BussType");//业务类型 1-新单收费 2-保全收费 3-续期收费
				String BussNo=list.get(i).getChildTextTrim("BussNo");// 业务号码 印刷号、保全受理号、保单号 
				String BussDealFlag=list.get(i).getChildTextTrim("BussDealFlag");//后续业务是否处理标志 1-继续处理 0-暂不处理 业务类型为1时，后续业务为签发保单；业务类型为2时，后续业务为保全生效；业务类型为3时，后续业务为续期核销
					//拼接sql
					String TempFeeSql="select * from LJTempFee where otherno='"+BussNo+"' and TempFeeType='"+BussType+"'";
					SSRS ssrs1=exeSQL.execSQL(TempFeeSql);//查询结果返回TempFee对象
					if(ssrs1.getMaxRow()<=0){
						Result.setText("1");
						ResultRemark.setText("没有查到相关数据");
						bady_Response.addContent(ResultRemark);
					}else{
						Result.setText("0");
						ResultRemark.setText("");
						String TempFeeClassSql="select * from LJTempFeeClass where TempFeeNo='"+ssrs1.GetText(1, 1)+"'";
						SSRS ssrs2=exeSQL.execSQL(TempFeeClassSql);//查询结果返回TempFeeClass对象
						String recoinfoId = PubFun1.CreateMaxNo("RECOINFOLID", 20);
						
						//处理数据 ssrs2.GetText(1, 15)为空的时候拼接sql语句失败，所以做数据处理
						String inputbankAccNo=ssrs2.GetText(1, 15);
						String EnterAccDate=ssrs2.GetText(1, 9);
						String EnterAccTime = ssrs2.GetText(1, 18);
						if("".endsWith(inputbankAccNo) || null==inputbankAccNo){
							inputbankAccNo="null";
						}
						//判断对账表是否已经存在
						String sql = "select 1 from reconcileInfo where otherno ='"+ BussNo +"'";
						if (exeSQL.execSQL(sql).getMaxRow() <= 0) {
							String reconcileInfosql="insert into reconcileInfo(recoinfoId,paymentOrFees,payOrFeesType,otherno,othertype,bankAccNo,managecom,processingmark,financeMark,finaSuccFailDes,finarrivalDate,finarrivalTime,businessFailMark,businessFailDes,makedate,maketime,effectiveDate,effectiveTime,redfield2,redfield3) values" +
							"('"+recoinfoId+"','"+ssrs1.GetText(1, 1)+"','"+ssrs2.GetText(1, 2)+"','"+BussNo+"','1','"+inputbankAccNo+"','86','0','1','成功','"+ EnterAccDate +"','"+ EnterAccTime +"','1','成功',TO_CHAR(sysdate, 'yyyy-mm-dd'),to_char(sysdate, 'HH24:mi:ss'),TO_CHAR(sysdate, 'yyyy-mm-dd'),to_char(sysdate, 'HH24:mi:ss'),'ShouDan','pay')";
							ExeSQL esql=new ExeSQL();
							log.info(reconcileInfosql);
							esql.execUpdateSQL(reconcileInfosql);
						}
						
						Element detail=returnElement(ssrs1,ssrs2,BussType,BussNo,head.getChildText("SendOperator"),head.getChildText("BranchCode"), BussDealFlag,head.getChildText("MsgType"),head.getChildText("BatchNo"));
						bady_Response.addContent(detail);
					}
			}
			bady_Response.addContent(Result);
			XMLOutputter out = new XMLOutputter();
			responseXml = out.outputString(Tempfee_Response);
			log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
			return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
		}catch (Exception e) {
			Result.setText("1");
			ResultRemark.setText("流程异常或者数据错误，请检测报文");
			bady_Response.addContent(ResultRemark);
			bady_Response.addContent(Result);
			XMLOutputter out = new XMLOutputter();
			 responseXml = out.outputString(Tempfee_Response);
			log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
			return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
		}finally{
			if(!StringUtil.StringNull(responseXml)){
				Result.setText("1");
				ResultRemark.setText("发生阻断异常,返回报文为空");
				bady_Response.addContent(ResultRemark);
				bady_Response.addContent(Result);
				XMLOutputter out = new XMLOutputter();
				 responseXml = out.outputString(Tempfee_Response);
				log.info("收费查询接口返回报文<?xml version='1.0' encoding='GBK'?>"+responseXml);
				return "<?xml version='1.0' encoding='GBK'?>"+responseXml;
			}
		}
	}

	
	/**
	 * @param ssrs1 LJTempFee 数据库对象
	 * @param ssrs2 LJTempFeeClass 数据库对象
	 * @param bussType 业务类型 1-新单收费 2-保全收费 3-续期收费
	 * @param bussNo 保单号
	 * @param Operator 交易人
	 * @param BranchCode 交易编码
	 * @param BussDealFlagqu 后续业务是否处理标志 1-继续处理 0-暂不处理
	 */
	public Element returnElement(SSRS ssrs1,SSRS ssrs2,String bussType,String bussNo,String Operator,String BranchCode,String BussDealFlagqu,String msgType,String batchNo) throws JDOMException, IOException{
		Element detail=new Element("detail");
		Element TempfeeFlag = new Element("TempfeeFlag");//是否收费标志 1-已收费 0-未收费 
		TempfeeFlag.setText("1");
		detail.addContent(TempfeeFlag);
		Element TempfeeNo = new Element("TempfeeNo");//暂收号
		TempfeeNo.setText(ssrs2.GetText(1, 1));
		detail.addContent(TempfeeNo);
		Element BussType = new Element("BussType");//业务类型 1-新单收费 2-保全收费 3-续期收费
		BussType.setText(bussType);
		detail.addContent(BussType);
		
		Element SaleChnl = new Element("SaleChnl");//渠道
		SaleChnl.setText(ssrs1.GetText(1, 13));
		detail.addContent(SaleChnl);
		Element PayMoney = new Element("PayMoney");//交费金额
		PayMoney.setText(ssrs2.GetText(1, 4));
		detail.addContent(PayMoney);
		Element PayDate = new Element("PayDate");//交费日期
		PayDate.setText(ssrs2.GetText(1, 6));
		detail.addContent(PayDate);
		Element EnterAccDate = new Element("EnterAccDate");//财务到账日期
		EnterAccDate.setText(ssrs1.GetText(1, 9));
		detail.addContent(EnterAccDate);
		Element ManageCom = new Element("ManageCom");//收费机构
		ManageCom.setText(ssrs2.GetText(1, 12));
		detail.addContent(ManageCom);
		Element PolicyCom = new Element("PolicyCom");//管理机构
		PolicyCom.setText(ssrs2.GetText(1, 13));
		detail.addContent(PolicyCom);
		Element APPntName = new Element("APPntName");//投保人姓名 
		APPntName.setText(ssrs2.GetText(1, 5));
		detail.addContent(APPntName);
		//Element PayDate = new Element("PayDate");//交费日期
		Element BankCode = new Element("BankCode");//银行编码
		BankCode.setText(ssrs2.GetText(1, 14));
		detail.addContent(BankCode);
		Element BankAccNo = new Element("BankAccNo");//银行账号
		BankAccNo.setText(ssrs2.GetText(1, 15));
		detail.addContent(BankAccNo);
		Element AccName = new Element("AccName");//账户名称
		AccName.setText(ssrs2.GetText(1, 16));
		detail.addContent(AccName);
		Element InsBankCode = new Element("InsBankCode");//保险公司帐号开户行
		InsBankCode.setText(ssrs2.GetText(1, 25));
		detail.addContent(InsBankCode);
		Element InsBankAccNo = new Element("InsBankAccNo");//保险公司帐号
		InsBankAccNo.setText(ssrs2.GetText(1, 26));
		detail.addContent(InsBankAccNo);
		Element Cashier = new Element("Cashier");//收费确认人员
		Cashier.setText(ssrs2.GetText(1, 20));
		detail.addContent(Cashier);
		Element BussDealFlag = new Element("BussDealFlag");
		detail.addContent(BussDealFlag);
		Element BussDealReamrk = new Element("BussDealReamrk");
		detail.addContent(BussDealReamrk);
		if("1".equals(BussDealFlagqu)){// 后续业务是否处理标志 1-继续处理 0-暂不处理
			//查询成功之后处理后续业务
			if("1".equals(bussType)){// 业务类型 1-新单收费 2-保全收费 3-续期收费
				//签单业务
				GroupPolSave gp=new GroupPolSave();
				String returnstr= gp.groupsave(Operator, BranchCode, bussNo,bussType,msgType,batchNo);
				log.info(returnstr);
//				String Bussnosql="select * from LCGrpCont where prtno='"+bussNo+"'";
				String Bussnosql="select a.grpcontno, b.customerno from lcgrpcont a, lcgrpappnt b where a.prtno=b.prtno and a.prtno = '"+ bussNo +"'";
				ExeSQL exeSQL = new ExeSQL();
				SSRS ssrs = exeSQL.execSQL(Bussnosql);
				bussNo=ssrs.GetText(1, 1);
				String customerNo = ssrs.GetText(1, 2);
				Element BussNo = new Element("BussNo");//业务号码 印刷号、保全受理号、保单号
				Element CustomerNo = new Element("CustomerNo");
				BussNo.setText(bussNo);
				CustomerNo.setText(customerNo);
				detail.addContent(BussNo);
				detail.addContent(CustomerNo);
				BussDealFlag.setText(returnstr.split("\\|")[0]);
				BussDealReamrk.setText(returnstr.split("\\|")[1]);
			}else if("2".equals(bussType)){// 业务类型 1-新单收费 2-保全收费 3-续期收费
				BqFinishTask bqfinish=new BqFinishTask();
				bqfinish.run();
			}else if("3".equals(bussType)){
				String GetNoticeNo =bussNo;
				 TransferData tempTransferData=new TransferData();
				    tempTransferData.setNameAndValue("GetNoticeNo",GetNoticeNo);
				    VData tVData = new VData(); 
				    GlobalInput GI = new GlobalInput(); 
				    GI.Operator = "BAOQA";
					GI.ComCode = BranchCode;
					GI.ManageCom = BranchCode;
				    tVData.add(GI);
				    tVData.add(tempTransferData);
				    GrpLjsPlanConfirmUI tGrpLjsPlanConfirmUI = new GrpLjsPlanConfirmUI(); 
				    tGrpLjsPlanConfirmUI.submitData(tVData,"");
				    if (!tGrpLjsPlanConfirmUI.mErrors.needDealError())
				     {
				    	Element BussNo = new Element("BussNo");////业务号码 印刷号、保全受理号、保单号
				    	BussNo.setText(bussNo);
						detail.addContent(BussNo);
						BussDealFlag.setText("true");
						BussDealReamrk.setText("处理成功");
				     }
				     else                                                                           
				     {
				    	 Element BussNo = new Element("BussNo");////业务号码 印刷号、保全受理号、保单号
					    	BussNo.setText(bussNo);
							detail.addContent(BussNo);
							BussDealFlag.setText("false");
							BussDealReamrk.setText(" 失败，原因是:" + tGrpLjsPlanConfirmUI.mErrors.getFirstError());
				       
				     }
			}
			
		}
		return detail;
	}
}

