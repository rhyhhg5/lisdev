package com.finfee;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import com.contract.obj.MessageHead;
import com.finfee.obj.tLJTempFeeClassSchemaList;
import com.finfee.obj.tLJTempFeeSchemaList;
import com.sinosoft.lis.finfee.TempFeeUI;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJSPayGrpSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.taskservice.BqFinishTask;
import com.sinosoft.lis.taskservice.LLClaimFinishTask;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * 收费接口

 * @author dongjiali
 */
public class TempFeeSave {
	private Logger log=Logger.getLogger(TempFeeSave.class);
	private VData vdata = new VData();
	public ExeSQL exeSQL=new ExeSQL();
	
	SimpleDateFormat sdf1 =   new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf2 =   new SimpleDateFormat("HH:mm:ss");
	Date   curDate   =   new   Date(System.currentTimeMillis());//获取当前时间  
	/**
	 * @param strxml
	 * @return
	 * @throws JDOMException
	 * @throws IOException 
	 */
	public String parse(String strxml) throws JDOMException, IOException{
		
		log.info(strxml);
		StringReader reader = new StringReader(strxml);
		
		SAXBuilder tSAXBuilder = new SAXBuilder();
		Document doc = tSAXBuilder.build(reader);
		Element rootElement = doc.getRootElement();
		// 解析报文头
		Element msgHead = rootElement.getChild("MsgHead");
		Element elehead = msgHead.getChild("Item");
		String BatchNo = elehead.getChildText("BatchNo");
		String SendDate = elehead.getChildText("SendDate");
		String SendTime = elehead.getChildText("SendTime");
		String BranchCode = elehead.getChildText("BranchCode");
		String SendOperator = elehead.getChildText("SendOperator");
		String MsgType = elehead.getChildText("MsgType");
		
		MessageHead head = new MessageHead();
		head.setBatchNo(BatchNo);
		head.setSendDate(SendDate);
		head.setSendTime(SendTime);
		head.setBranchCode(BranchCode);
		head.setSendOperator(SendOperator);
		head.setMsgType(MsgType);
		vdata.add(head);
		if("XQ".equals(MsgType)){
			parseXQXml(rootElement);
		}else if("BQ".equals(MsgType)){
			parseBQml(rootElement);
		}
		
		return "";
	}
	/**
	 * @author dongjl
	 *
	 * 获取LJSPay节点内部类
	 */
	class getLJSPay{
		
		public  boolean getLJSPaySchema(Element rootElement) throws ParseException{
			Element MsgBody=rootElement.getChild("MsgBody").getChild("LJSPay");
			String GetNoticeNo=MsgBody.getChildText("GetNoticeNo");
			String OtherNo=MsgBody.getChildText("OtherNo");
			String OtherNoType=MsgBody.getChildText("OtherNoType");
			String AppntNo=MsgBody.getChildText("AppntNo");
			String SumDuePayMoney=MsgBody.getChildText("SumDuePayMoney");
			String PayDate=MsgBody.getChildText("PayDate");
			String BankOnTheWayFlag=MsgBody.getChildText("BankOnTheWayFlag");
			String BankSuccFlag=MsgBody.getChildText("BankSuccFlag");
			String SendBankCount=MsgBody.getChildText("SendBankCount");
			String ApproveCode=MsgBody.getChildText("ApproveCode");
			String ApproveDate=MsgBody.getChildText("ApproveDate");
			String SerialNo=MsgBody.getChildText("SerialNo");
			String Operator=MsgBody.getChildText("Operator");
			String MakeDate=sdf1.format(curDate);
			String MakeTime=sdf2.format(curDate);
			String ModifyDate=MsgBody.getChildText("ModifyDate");
			String ModifyTime=MsgBody.getChildText("ModifyTime");
			String ManageCom=MsgBody.getChildText("ManageCom");
			String AgentCom=MsgBody.getChildText("AgentCom");
			String AgentType=MsgBody.getChildText("AgentType");
			String BankCode=MsgBody.getChildText("BankCode");
			String BankAccNo=MsgBody.getChildText("BankAccNo");
			String RiskCode=MsgBody.getChildText("RiskCode");
			String AgentCode=MsgBody.getChildText("AgentCode");
			String AgentGroup=MsgBody.getChildText("AgentGroup");
			String AccName=MsgBody.getChildText("AccName");
			String StartPayDate=MsgBody.getChildText("StartPayDate");
			String PayTypeFlag=MsgBody.getChildText("PayTypeFlag");
			String CanSendBank=MsgBody.getChildText("CanSendBank");
			String MarketType=MsgBody.getChildText("MarketType");
			String SaleChnl=MsgBody.getChildText("SaleChnl");
			
			String sqlLJSPay="insert into LJSPay values " +
					"('"+GetNoticeNo+"','"+OtherNo+"','"+OtherNoType+"','"+AppntNo+"'," +
					"'"+SumDuePayMoney+"','"+PayDate+"','"+BankOnTheWayFlag+"','"+BankSuccFlag+"'," +
					"'"+SendBankCount+"','"+ApproveCode+" ','"+ApproveDate+"','"+SerialNo+"','"+Operator+"'," +
					"'"+MakeDate+"','"+MakeTime+"','"+ModifyDate+"','"+ModifyTime+"','"+ManageCom+"'," +
					"'"+AgentCom+"','"+AgentType+"','"+BankCode+"','"+BankAccNo+"','"+RiskCode+"','"+AgentCode+"'," +
					"'"+AgentGroup+"','"+AccName+"','"+StartPayDate+"','"+PayTypeFlag+"','"+CanSendBank+"','"+MarketType+"','"+SaleChnl+"')";
			
			return exeSQL.execUpdateSQL(sqlLJSPay);
		}
	}
	
	/**
	 * @author dongjl
	 *
	 * 获取LJSPayGrp节点内部类
	 */
	class getLJSPayGrp{
		
		public  boolean getLJSPayGrpSchema(Element rootElement){
			Element MsgBody=rootElement.getChild("MsgBody").getChild("LJSPayGrp");
			String GrpPolNo=MsgBody.getChildText("GrpPolNo");
			String PayCount=MsgBody.getChildText("PayCount");
			String GrpContNo=MsgBody.getChildText("GrpContNo");
			String ManageCom=MsgBody.getChildText("ManageCom");
			String AgentCom=MsgBody.getChildText("AgentCom");
			String AgentType=MsgBody.getChildText("AgentType");
			String RiskCode=MsgBody.getChildText("RiskCode");
			String AgentCode=MsgBody.getChildText("AgentCode");
			String AgentGroup=MsgBody.getChildText("AgentGroup");
			String PayTypeFlag=MsgBody.getChildText("PayTypeFlag");
			String AppntNo=MsgBody.getChildText("AppntNo");
			String GetNoticeNo=MsgBody.getChildText("GetNoticeNo");
			String SumDuePayMoney=MsgBody.getChildText("SumDuePayMoney");
			String SumActuPayMoney=MsgBody.getChildText("SumActuPayMoney");
			String PayIntv=MsgBody.getChildText("PayIntv");
			String PayDate=MsgBody.getChildText("PayDate");
			String PayType=MsgBody.getChildText("PayType");
			String LastPayToDate=MsgBody.getChildText("LastPayToDate");
			String CurPayToDate=MsgBody.getChildText("CurPayToDate");
			String ApproveCode=MsgBody.getChildText("ApproveCode");
			String ApproveDate=MsgBody.getChildText("ApproveDate");
			String ApproveTime=MsgBody.getChildText("ApproveTime");
			String SerialNo=MsgBody.getChildText("SerialNo");
			String InputFlag=MsgBody.getChildText("InputFlag");
			String Operator=MsgBody.getChildText("Operator");
			String MakeDate=sdf1.format(curDate);
			String MakeTime=sdf2.format(curDate);
			String ModifyDate=MsgBody.getChildText("ModifyDate");
			String ModifyTime=MsgBody.getChildText("ModifyTime");
			String sqlLJSPayGrp="insert into LJSPayGrp values" +
					"('"+GrpPolNo+"','"+PayCount+"','"+GrpContNo+"','"+ManageCom+" ','"+AgentCom+"','"+AgentType+"','"+RiskCode+"'," +
					"'"+AgentCode+"','"+AgentGroup+"','"+PayTypeFlag+"','"+AppntNo+"','"+GetNoticeNo+"','"+SumDuePayMoney+"','"+SumActuPayMoney+"'," +
					"'"+PayIntv+"','"+PayDate+"','"+PayType+"','"+LastPayToDate+"','"+CurPayToDate+"','"+ApproveCode+"','"+ApproveDate+"','"+ApproveTime+"'," +
					"'"+SerialNo+"','"+InputFlag+"','"+Operator+"','"+MakeDate+"','"+MakeTime+"','"+ModifyDate+"','"+ModifyTime+"')";
			return exeSQL.execUpdateSQL(sqlLJSPayGrp);
		}
	}
	
	
	/**
	 * @author dongjl
	 *	
	 *	收费保全子表数据插入
	 */
	class getLJSGETENDORSE{
		public boolean setBQ(Element rootElement){
			Element LJSGETENDORSE=rootElement.getChild("MsgBody").getChild("LJSGETENDORSE");
			
			String GetNoticeNo=LJSGETENDORSE.getChildText("GetNoticeNo");
			String EndorsementNo=LJSGETENDORSE.getChildText("EndorsementNo");
			String FeeOperationType=LJSGETENDORSE.getChildText("FeeOperationType");
			String FeeFinaType=LJSGETENDORSE.getChildText("FeeFinaType");
			String GrpContNo=LJSGETENDORSE.getChildText("GrpContNo");
			String ContNo=LJSGETENDORSE.getChildText("ContNo");
			String GrpPolNo=LJSGETENDORSE.getChildText("GrpPolNo");
			String PolNo=LJSGETENDORSE.getChildText("PolNo");
			String OtherNo=LJSGETENDORSE.getChildText("OtherNo");
			String OtherNoType=LJSGETENDORSE.getChildText("OtherNoType");
			String DutyCode=LJSGETENDORSE.getChildText("DutyCode");
			String PayPlanCode=LJSGETENDORSE.getChildText("PayPlanCode");
			String AppntNo=LJSGETENDORSE.getChildText("AppntNo");
			String InsuredNo=LJSGETENDORSE.getChildText("InsuredNo");
			String GetDate=LJSGETENDORSE.getChildText("GetDate");
			String GetMoney=LJSGETENDORSE.getChildText("GetMoney");
			String KindCode=LJSGETENDORSE.getChildText("KindCode");
			String RiskCode=LJSGETENDORSE.getChildText("RiskCode");
			String RiskVersion=LJSGETENDORSE.getChildText("RiskVersion");
			String ManageCom=LJSGETENDORSE.getChildText("ManageCom");
			String AgentCom=LJSGETENDORSE.getChildText("AgentCom");
			String AgentType=LJSGETENDORSE.getChildText("AgentType");
			String AgentCode=LJSGETENDORSE.getChildText("AgentCode");
			String AgentGroup=LJSGETENDORSE.getChildText("AgentGroup");
			String GrpName=LJSGETENDORSE.getChildText("GrpName");
			String Handler=LJSGETENDORSE.getChildText("Handler");
			String PolType=LJSGETENDORSE.getChildText("PolType");
			String ApproveCode=LJSGETENDORSE.getChildText("ApproveCode");
			String ApproveDate=LJSGETENDORSE.getChildText("ApproveDate");
			String ApproveTime=LJSGETENDORSE.getChildText("ApproveTime");
			String GetFlag=LJSGETENDORSE.getChildText("GetFlag");
			String SerialNo=LJSGETENDORSE.getChildText("SerialNo");
			String Operator=LJSGETENDORSE.getChildText("Operator");
			String MakeDate=sdf1.format(curDate);
			String MakeTime=sdf2.format(curDate);
			String ModifyDate=LJSGETENDORSE.getChildText("ModifyDate");
			String ModifyTime=LJSGETENDORSE.getChildText("ModifyTime");
			String sql="insert into LJSGETENDORSE values ('"+GetNoticeNo+"','"+EndorsementNo+"','"+FeeOperationType+"','"+FeeFinaType+"','"+GrpContNo+"','"+ContNo+"','"+GrpPolNo+"','"+PolNo+"','"+OtherNo+"','"+OtherNoType+"'," +
					"'"+DutyCode+"','"+PayPlanCode+"','"+AppntNo+"','"+InsuredNo+"','"+GetDate+"','"+GetMoney+"','"+KindCode+"','"+RiskCode+"','"+RiskVersion+"','"+ManageCom+"'," +
					"'"+AgentCom+"','"+AgentType+"','"+AgentCode+"','"+AgentGroup+"','"+GrpName+"','"+Handler+"','"+PolType+"','"+ApproveCode+"','"+ApproveDate+"','"+ApproveTime+"'," +
					"'"+GetFlag+"','"+SerialNo+"','"+Operator+"','"+MakeDate+"','"+MakeTime+"','"+ModifyDate+"','"+ModifyTime+"')";
			
			return exeSQL.execUpdateSQL(sql);
		}
	}
	
	class setLJSPayB{
		public boolean putLJSPayB(Element rootElement){
			Element LJSPayB=rootElement.getChild("MsgBody").getChild("LJSPayB");
			
			String GetNoticeNo=LJSPayB.getChildText("GetNoticeNo");
			String OtherNo=LJSPayB.getChildText("OtherNo");
			String OtherNoType=LJSPayB.getChildText("OtherNoType");
			String AppntNo=LJSPayB.getChildText("AppntNo");
			String SumDuePayMoney=LJSPayB.getChildText("SumDuePayMoney");
			String PayDate=LJSPayB.getChildText("PayDate");
			String BankOnTheWayFlag=LJSPayB.getChildText("BankOnTheWayFlag");
			String BankSuccFlag=LJSPayB.getChildText("BankSuccFlag");
			String SendBankCount=LJSPayB.getChildText("SendBankCount");
			String ApproveCode=LJSPayB.getChildText("ApproveCode");
			String ApproveDate=LJSPayB.getChildText("ApproveDate");
			String SerialNo=LJSPayB.getChildText("SerialNo");
			String Operator=LJSPayB.getChildText("Operator");
			String MakeDate=LJSPayB.getChildText("MakeDate");
			String MakeTime=LJSPayB.getChildText("MakeTime");
			String ModifyDate=LJSPayB.getChildText("ModifyDate");
			String ModifyTime=LJSPayB.getChildText("ModifyTime");
			String ManageCom=LJSPayB.getChildText("ManageCom");
			String AgentCom=LJSPayB.getChildText("AgentCom");
			String AgentType=LJSPayB.getChildText("AgentType");
			String BankCode=LJSPayB.getChildText("BankCode");
			String BankAccNo=LJSPayB.getChildText("BankAccNo");
			String RiskCode=LJSPayB.getChildText("RiskCode");
			String AgentCode=LJSPayB.getChildText("AgentCode");
			String AgentGroup=LJSPayB.getChildText("AgentGroup");
			String AccName=LJSPayB.getChildText("AccName");
			String StartPayDate=LJSPayB.getChildText("StartPayDate");
			String PayTypeFlag=LJSPayB.getChildText("PayTypeFlag");
			String DealState=LJSPayB.getChildText("DealState");
			String CancelReason=LJSPayB.getChildText("CancelReason");
			String ConfFlag=LJSPayB.getChildText("ConfFlag");
			String MarketType=LJSPayB.getChildText("MarketType");
			String SaleChnl=LJSPayB.getChildText("SaleChnl");
			
			String sql="insert into LJSPayB values ('"+GetNoticeNo+"','"+OtherNo+"','"+OtherNoType+"','"+AppntNo+"','"+SumDuePayMoney+"','"+PayDate+"','"+BankOnTheWayFlag+"','"+BankSuccFlag+"','"+SendBankCount+"','"+ApproveCode+"'," +
					"'"+ApproveDate+"','"+SerialNo+"','"+Operator+"','"+MakeDate+"','"+MakeTime+"','"+ModifyDate+"','"+ModifyTime+"','"+ManageCom+"','"+AgentCom+"','"+AgentType+"'," +
					"'"+BankCode+"','"+BankAccNo+"','"+RiskCode+"','"+AgentCode+"','"+AgentGroup+"','"+AccName+"','"+StartPayDate+"','"+PayTypeFlag+"','"+DealState+"','"+CancelReason+"'," +
					"'"+ConfFlag+"','"+MarketType+"','"+SaleChnl+"')";
			return exeSQL.execUpdateSQL(sql);
		}
	}
	
	/**
	 *
	 *续期收费接口
	 * @throws IOException 
	 * 
	 */
	public String parseXQXml(Element rootElement) throws IOException {
		
		boolean b1 = true;
		boolean b2 = true;
		boolean b3 = true;
		try{
			 b1=new getLJSPay().getLJSPaySchema(rootElement);
			 b2=new getLJSPayGrp().getLJSPayGrpSchema(rootElement);
			 b3=new setLJSPayB().putLJSPayB(rootElement);
			 if(!b1||!b2||!b3){
				 return returnxml("false","");
			 }else{
				 return returnxml("true","");
			 }
		}catch (Exception e) {
			if(!b1||!b2||!b3){
				String GetNoticeNo=rootElement.getChild("MsgBody").getChild("LJSPay").getChildText("GetNoticeNo");
				String GetNoticeNo1=rootElement.getChild("MsgBody").getChild("LJSPayGrp").getChildText("GetNoticeNo");
				String GetNoticeNo2=rootElement.getChild("MsgBody").getChild("LJSPayB").getChildText("GetNoticeNo");
				String sqldelete1="delete from LJSPayGrp where GetNoticeNo='"+GetNoticeNo+"'";
				String sqldelete2="delete from LJSPay where GetNoticeNo='"+GetNoticeNo1+"'";
				String sqldelete3="delete from LJSPay where GetNoticeNo='"+GetNoticeNo2+"'";
				exeSQL.execUpdateSQL(sqldelete1);
				exeSQL.execUpdateSQL(sqldelete2);
				exeSQL.execUpdateSQL(sqldelete3);
			}
			return returnxml("false","");
		}
		
	}
	
	/**
	 * 
	 * 保全收费接口
	 * @param strxml 返回报文
	 * @throws IOException 
	 * 
	 */
	public String parseBQml(Element rootElement) throws IOException{
		boolean b1 = true;
		boolean b2 = true;
		try{
			b1=new getLJSPay().getLJSPaySchema(rootElement);
			b2=new getLJSGETENDORSE().setBQ(rootElement);
			if(b1&&b2){
				return returnxml("true","");
			}else{
				returnxml("false","");
			}
		}catch(Exception e) {
			log.info(e);
			String GetNoticeNo=rootElement.getChild("MsgBody").getChild("LJSPay").getChildText("GetNoticeNo");
			String GetNoticeNo1=rootElement.getChild("MsgBody").getChild("LJSGETENDORSE").getChildText("GetNoticeNo");
			String sqldelete1="delete from LJSPayGrp where GetNoticeNo='"+GetNoticeNo+"'";
			String sqldelete2="delete from LJSGETENDORSE where GetNoticeNo='"+GetNoticeNo1+"'";
			exeSQL.execUpdateSQL(sqldelete1);
			exeSQL.execUpdateSQL(sqldelete2);
			return returnxml("false","");
		}finally{
			if(!b1&&!b2){
				String GetNoticeNo=rootElement.getChild("MsgBody").getChild("LJSPay").getChildText("GetNoticeNo");
				String GetNoticeNo1=rootElement.getChild("MsgBody").getChild("LJSGETENDORSE").getChildText("GetNoticeNo");
				String sqldelete1="delete from LJSPayGrp where GetNoticeNo='"+GetNoticeNo+"'";
				String sqldelete2="delete from LJSGETENDORSE where GetNoticeNo='"+GetNoticeNo1+"'";
				exeSQL.execUpdateSQL(sqldelete1);
				exeSQL.execUpdateSQL(sqldelete2);
			}
		}
		 
		return returnxml("false","");
	}
	
	
	public static void main(String[] str) throws IOException, JDOMException {

		InputStream pIns = new FileInputStream("C:/Users/dongjl/Desktop/sf.xml");
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		byte[] tBytes = new byte[8 * 1024];
		for (int tReadSize; -1 != (tReadSize = pIns.read(tBytes));) {
			mByteArrayOutputStream.write(tBytes, 0, tReadSize);
		}
		String mInXmlStr = new String(mByteArrayOutputStream.toByteArray(), "GBK");
		TempFeeSave tfs=new TempFeeSave();
		String s=tfs.parse(mInXmlStr);
		System.out.println("==========="+s);
	}
	

	private String returnxml(String state,String Content) throws IOException{
		Element root = new Element("DataSet");
		Document doc = new Document(root);
		Element eMsgHead = new Element("MsgHead");
		Element estate = new Element("state");
		estate.addContent(state);
		Element eContent = new Element("Content");
		eContent.addContent(Content);
		eMsgHead.addContent(estate);
		eMsgHead.addContent(eContent);
		root.addContent(eMsgHead);
		XMLOutputter out = new XMLOutputter();
		String responseXml = out.outputString(doc);
		return responseXml;
	}
}


