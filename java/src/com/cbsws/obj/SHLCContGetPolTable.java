package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;


public class SHLCContGetPolTable  extends BaseXmlSch{

    /** 保单号*/
    private String ContNo = null;

    
    private String GetPolDate = null;
	
    private String GetPolTime = null;
    /** 签收日期*/
    private String CustomGetPolDate = null;
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getGetPolDate() {
		return GetPolDate;
	}
	public void setGetPolDate(String getPolDate) {
		GetPolDate = getPolDate;
	}
	public String getGetPolTime() {
		return GetPolTime;
	}
	public void setGetPolTime(String getPolTime) {
		GetPolTime = getPolTime;
	}
	public String getCustomGetPolDate() {
		return CustomGetPolDate;
	}
	public void setCustomGetPolDate(String customGetPolDate) {
		CustomGetPolDate = customGetPolDate;
	}

    
    
	
    
}
