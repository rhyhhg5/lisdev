/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 网销复杂产品出单受益人信息
 * 
 * @author GZH
 *
 */
public class LCContINFO extends BaseXmlSch
{

	private static final long serialVersionUID = -4794467791337930966L;
	/**
	 * 印刷号
	 */
	private String PrtNo;
	/**
	 * 组织机构代码
	 */
	private String OranComCode;
	/**
	 * 社会信用统一代码
	 */
	private String UNIfiedSocialCreditNO;
	/**
	 * 签单公司
	 */
	private String GRPMakeName;
	/**
	 * 地址
	 */
	private String Address;
	/**
	 * 邮编
	 */
	private String ZipCode;
	/**
	 * 电话
	 */
	private String Phone;
	/**
	 * 传真
	 */
	private String Fax;
	/**
	 * 联系电话
	 */
	private String TelePhone;
	/**
	 * 制单人
	 */
	private String MakeContName;
	/**
	 * 第三方平台销售人员姓名
	 */
	private String EjbAgent;
	/**
	 * 第三方平台销售中介出单机构
	 */
	private String EjbManage;
	/**
	 * 是否打印被保人清单
	 */
	private String PrintFlag;
	public String getOranComCode() {
		return OranComCode;
	}
	public void setOranComCode(String oranComCode) {
		OranComCode = oranComCode;
	}
	public String getUNIfiedSocialCreditNO() {
		return UNIfiedSocialCreditNO;
	}
	public void setUNIfiedSocialCreditNO(String uNIfiedSocialCreditNO) {
		UNIfiedSocialCreditNO = uNIfiedSocialCreditNO;
	}
	public String getGRPMakeName() {
		return GRPMakeName;
	}
	public void setGRPMakeName(String gRPMakeName) {
		GRPMakeName = gRPMakeName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getTelePhone() {
		return TelePhone;
	}
	public void setTelePhone(String telePhone) {
		TelePhone = telePhone;
	}
	public String getMakeContName() {
		return MakeContName;
	}
	public void setMakeContName(String makeContName) {
		MakeContName = makeContName;
	}
	public String getEjbAgent() {
		return EjbAgent;
	}
	public void setEjbAgent(String ejbAgent) {
		EjbAgent = ejbAgent;
	}
	public String getEjbManage() {
		return EjbManage;
	}
	public void setEjbManage(String ejbManage) {
		EjbManage = ejbManage;
	}
	public String getPrintFlag() {
		return PrintFlag;
	}
	public void setPrintFlag(String printFlag) {
		PrintFlag = printFlag;
	}
	public String getPrtNo() {
		return PrtNo;
	}
	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}
	
    
}
