/**
 * 2016-10-10
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保全增减人被保人信息
 * 
 * @author LC
 *
 */
public class EdorInsuredInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    
    /** 合同ID */
    private String ContID = null;
    
    /** 被保人序号 */
    private String InsuredID = null;
    
    /** 被保人状态 */
    private String InsuredState = null;
    
    /** 员工姓名 */
    private String EmployeeName = null;
    
    /** 被保人姓名 */
    private String Name = null;
    
    /** 与员工关系 */
    private String RelationToMainInsured = null;
    
    /** 性别 */
    private String Sex = null;
    
    /** 出生日期 */
    private String Birthday = null;
    
    /** 证件类型 */
    private String IDType = null;
    
    /** 证件号码 */
    private String IDNo = null;
    
    /** 保险计划 */
    private String ContPlanCode = null;
    
    /** 职业类别 */
    private String OccupationType = null;
    
    /** 理赔金转帐银行 */
    private String BankCode = null;
    
    /** 户名 */
    private String AccName = null;
    
    /** 账号 */
    private String BankAccno = null;
    
    /** 期交保费 */
    private String Prem = null;
    
    /** 退补费 */
    private String GetMoney = null;
    
    /** 生效日期 */
    private String CValidate = null;
    
    /** 联系电话 */
    private String Phone = null;


	public String getContID() {
		return ContID;
	}

	public void setContID(String contID) {
		ContID = contID;
	}

	public String getInsuredID() {
		return InsuredID;
	}

	public void setInsuredID(String insuredID) {
		InsuredID = insuredID;
	}

	public String getInsuredState() {
		return InsuredState;
	}

	public void setInsuredState(String insuredState) {
		InsuredState = insuredState;
	}

	public String getEmployeeName() {
		return EmployeeName;
	}

	public void setEmployeeName(String employeeName) {
		EmployeeName = employeeName;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getRelationToMainInsured() {
		return RelationToMainInsured;
	}

	public void setRelationToMainInsured(String relationToMainInsured) {
		RelationToMainInsured = relationToMainInsured;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getBirthday() {
		return Birthday;
	}

	public void setBirthday(String birthday) {
		Birthday = birthday;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getIDNo() {
		return IDNo;
	}

	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}

	public String getContPlanCode() {
		return ContPlanCode;
	}

	public void setContPlanCode(String contPlanCode) {
		ContPlanCode = contPlanCode;
	}

	public String getOccupationType() {
		return OccupationType;
	}

	public void setOccupationType(String occupationType) {
		OccupationType = occupationType;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getAccName() {
		return AccName;
	}

	public void setAccName(String accName) {
		AccName = accName;
	}

	public String getBankAccno() {
		return BankAccno;
	}

	public void setBankAccno(String bankAccno) {
		BankAccno = bankAccno;
	}

	public String getPrem() {
		return Prem;
	}

	public void setPrem(String prem) {
		Prem = prem;
	}

	public String getGetMoney() {
		return GetMoney;
	}

	public void setGetMoney(String getMoney) {
		GetMoney = getMoney;
	}

	public String getCValidate() {
		return CValidate;
	}

	public void setCValidate(String cValidate) {
		CValidate = cValidate;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}
	
}
