package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class ECPH_QUERY_TABLE extends BaseXmlSch {
	private static final long serialVersionUID = -4794467791337930966L;

	private String BatchNoInfo = "";
	private String BranchCodeInfo = "";
	private String TableName = "";
	private String CertifyName = "";
	private String CertifyCode = "";
	public String getBatchNoInfo() {
		return BatchNoInfo;
	}
	public void setBatchNoInfo(String batchNoInfo) {
		BatchNoInfo = batchNoInfo;
	}
	public String getBranchCodeInfo() {
		return BranchCodeInfo;
	}
	public void setBranchCodeInfo(String branchCodeInfo) {
		BranchCodeInfo = branchCodeInfo;
	}
	public String getTableName() {
		return TableName;
	}
	public void setTableName(String tableName) {
		TableName = tableName;
	}
	public String getCertifyName() {
		return CertifyName;
	}
	public void setCertifyName(String certifyName) {
		CertifyName = certifyName;
	}
	public String getCertifyCode() {
		return CertifyCode;
	}
	public void setCertifyCode(String certifyCode) {
		CertifyCode = certifyCode;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
