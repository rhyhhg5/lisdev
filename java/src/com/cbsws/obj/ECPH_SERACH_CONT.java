package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 电商查询的保单信息
 * @author wujun
 *
 */
public class ECPH_SERACH_CONT extends BaseXmlSch {
	
    private static final long serialVersionUID = -4794467791337930966L;
	
	/**
	 * 电商查询保单信息
	 */
	
	private String BATCHNOINFO = "";
	private String BRANCHCODEINFO = "";
	private String MSGTYPEINFO = "";
	private String PRTNO = "";
	private String CONTNO = "";
	private String EMAIL = "";
	private String PHONE = "";
	private String OPERATORINFO = "";
	private String MAKEDATEINFO = "";
	private String MAKETIMEINFO = "";

	public String getBATCHNOINFO() {
		return BATCHNOINFO;
	}
	public void setBATCHNOINFO(String batchnoinfo) {
		BATCHNOINFO = batchnoinfo;
	}
	public String getBRANCHCODEINFO() {
		return BRANCHCODEINFO;
	}
	public void setBRANCHCODEINFO(String branchcodeinfo) {
		BRANCHCODEINFO = branchcodeinfo;
	}
	public String getCONTNO() {
		return CONTNO;
	}
	public void setCONTNO(String contno) {
		CONTNO = contno;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String email) {
		EMAIL = email;
	}
	public String getMAKEDATEINFO() {
		return MAKEDATEINFO;
	}
	public void setMAKEDATEINFO(String makedateinfo) {
		MAKEDATEINFO = makedateinfo;
	}
	public String getMAKETIMEINFO() {
		return MAKETIMEINFO;
	}
	public void setMAKETIMEINFO(String maketimeinfo) {
		MAKETIMEINFO = maketimeinfo;
	}
	public String getMSGTYPEINFO() {
		return MSGTYPEINFO;
	}
	public void setMSGTYPEINFO(String msgtypeinfo) {
		MSGTYPEINFO = msgtypeinfo;
	}
	public String getOPERATORINFO() {
		return OPERATORINFO;
	}
	public void setOPERATORINFO(String operatorinfo) {
		OPERATORINFO = operatorinfo;
	}
	public String getPHONE() {
		return PHONE;
	}
	public void setPHONE(String phone) {
		PHONE = phone;
	}
	public String getPRTNO() {
		return PRTNO;
	}
	public void setPRTNO(String prtno) {
		PRTNO = prtno;
	}


	
}
