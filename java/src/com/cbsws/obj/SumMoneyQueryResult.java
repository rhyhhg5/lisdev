package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SumMoneyQueryResult extends BaseXmlSch{

	private static final long serialVersionUID = 471687977943276320L;	

	/**
	 * 发生时间
	 */
	private String PayDate;
	/**
	 * 业务类型
	 */
	private String MoneyType;
	/**
	 * 增加金额
	 */
	private String IncreaseMoney;
	/**
	 * 减少金额
	 */
	private String ReduceMoney;
	/**
	 * 最终金额
	 */
	private String FinalMoney;
	public String getPayDate() {
		return PayDate;
	}
	public void setPayDate(String payDate) {
		PayDate = payDate;
	}
	public String getMoneyType() {
		return MoneyType;
	}
	public void setMoneyType(String moneyType) {
		MoneyType = moneyType;
	}
	public String getIncreaseMoney() {
		return IncreaseMoney;
	}
	public void setIncreaseMoney(String increaseMoney) {
		IncreaseMoney = increaseMoney;
	}
	public String getReduceMoney() {
		return ReduceMoney;
	}
	public void setReduceMoney(String reduceMoney) {
		ReduceMoney = reduceMoney;
	}
	public String getFinalMoney() {
		return FinalMoney;
	}
	public void setFinalMoney(String finalMoney) {
		FinalMoney = finalMoney;
	}
	
	
}
