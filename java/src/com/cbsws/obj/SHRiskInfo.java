/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 *
 */
public class SHRiskInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String RiskCode;
    private String riskName;
    private String Amnt;
    private String Prem;
    private String PaySep;
    private String payTerm;
    private String payDate;
	public String getRiskCode() {
		return RiskCode;
	}
	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}
	public String getRiskName() {
		return riskName;
	}
	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}
	public String getAmnt() {
		return Amnt;
	}
	public void setAmnt(String amnt) {
		Amnt = amnt;
	}
	public String getPrem() {
		return Prem;
	}
	public void setPrem(String prem) {
		Prem = prem;
	}
	public String getPaySep() {
		return PaySep;
	}
	public void setPaySep(String paySep) {
		PaySep = paySep;
	}
	public String getPayTerm() {
		return payTerm;
	}
	public void setPayTerm(String payTerm) {
		this.payTerm = payTerm;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
   
    
    
    
    
    
    
	
    
    
    
}
