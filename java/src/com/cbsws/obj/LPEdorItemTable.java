package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public  class LPEdorItemTable extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5651652384695302756L;
	/**
	 * 保全工单号
	 */
	private String EdorAcceptNo = "";
	/**
	 * 保全项目
	 */
	private String EdorType = "";
	/**
	 * 退保原因
	 */
	private String ReasonCode = "";
	
	/**
	 * 保全生效日期
	 */
	private String EdorValidate = "";

	public String getEdorAcceptNo() {
		return EdorAcceptNo;
	}

	public void setEdorAcceptNo(String edorAcceptNo) {
		EdorAcceptNo = edorAcceptNo;
	}

	public String getEdorType() {
		return EdorType;
	}

	public void setEdorType(String edorType) {
		EdorType = edorType;
	}

	public String getReasonCode() {
		return ReasonCode;
	}

	public void setReasonCode(String reasonCode) {
		ReasonCode = reasonCode;
	}

	public String getEdorValidate() {
		return EdorValidate;
	}

	public void setEdorValidate(String edorValidate) {
		EdorValidate = edorValidate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
