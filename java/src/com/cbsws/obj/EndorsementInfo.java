/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保全项目信息
 * 
 * @author lzy
 *
 */
public class EndorsementInfo extends BaseXmlSch
{
	
	private static final long serialVersionUID = -6507772631037260086L;

	/** 工单号*/
    private String EdorNo = null;

    /** 保全类型*/
    private String EdorType = null;

    /** 投保人名称 */
    private String AppntName = null;

    /** 保单号 */
    private String ContNo = null;
    
    /** 保全生效日期*/
    private String EdorValidate = null;

    /** 保全申请日期 */
    private String EdorApplyDate = null;
    
    /** 保全 结案日期 */
    private String EdorConfdate = null;
    
    /** 保全退费金额 */
    private String SumGetMoney = null;

	public String getEdorNo() {
		return EdorNo;
	}

	public void setEdorNo(String edorNo) {
		EdorNo = edorNo;
	}

	public String getEdorType() {
		return EdorType;
	}

	public void setEdorType(String edorType) {
		EdorType = edorType;
	}

	public String getAppntName() {
		return AppntName;
	}

	public void setAppntName(String appntName) {
		AppntName = appntName;
	}

	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public String getEdorValidate() {
		return EdorValidate;
	}

	public void setEdorValidate(String edorValidate) {
		EdorValidate = edorValidate;
	}

	public String getEdorApplyDate() {
		return EdorApplyDate;
	}

	public void setEdorApplyDate(String edorApplyDate) {
		EdorApplyDate = edorApplyDate;
	}

	public String getEdorConfdate() {
		return EdorConfdate;
	}

	public void setEdorConfdate(String edorConfdate) {
		EdorConfdate = edorConfdate;
	}

	public String getSumGetMoney() {
		return SumGetMoney;
	}

	public void setSumGetMoney(String sumGetMoney) {
		SumGetMoney = sumGetMoney;
	}
    
    
    
}
