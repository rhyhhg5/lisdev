package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class EdorCMLCInsuredInfo extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8259251758509790195L;
	/** 银行编码 */
	private String BankCode;
	/** 银行账号 */
	private String BankAccNo;
	/** 银行账户名 */
	private String AccName;
	/**被保人号 */
	private String InsuredNo;  
	/** 与主被保人关系 */
	private String RelationToMainInsured;
	/** 与投保人关系 */
	private String RelationToAppnt;
	/** 被保人状态 */
	private String InsuredStat;
	/** 国籍 */
	private String NativePlace;
	/** 职位 */
	private String Position;
	/** 工资 */
	private String Salary;
	/** 身份证文件生效日期 */
	private String IDStartDate;
	/** 身份证文件失效日期 */
	private String IDEndDate;
	/** 团体被保人电话 */
	private String GrpInsuredPhone;
	/** 服务年数起始日 */
	private String JoinCompanyDate ;
	/** 级别 */
	private String PositionU;
	public String getJoinCompanyDate() {
		return JoinCompanyDate;
	}

	public void setJoinCompanyDate(String joinCompanyDate) {
		JoinCompanyDate = joinCompanyDate;
	}

	public String getPositionU() {
		return PositionU;
	}

	public void setPositionU(String positionU) {
		PositionU = positionU;
	}

	public String getGetDutyKind() {
		return GetDutyKind;
	}

	public void setGetDutyKind(String getDutyKind) {
		GetDutyKind = getDutyKind;
	}

	/** 老年护理金领取方式 */
	private String GetDutyKind;

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getBankAccNo() {
		return BankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}

	public String getAccName() {
		return AccName;
	}

	public void setAccName(String accName) {
		AccName = accName;
	}

	public String getInsuredNo() {
		return InsuredNo;
	}

	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}

	public String getRelationToMainInsured() {
		return RelationToMainInsured;
	}

	public void setRelationToMainInsured(String relationToMainInsured) {
		RelationToMainInsured = relationToMainInsured;
	}

	public String getRelationToAppnt() {
		return RelationToAppnt;
	}

	public void setRelationToAppnt(String relationToAppnt) {
		RelationToAppnt = relationToAppnt;
	}

	public String getInsuredStat() {
		return InsuredStat;
	}

	public void setInsuredStat(String insuredStat) {
		InsuredStat = insuredStat;
	}

	public String getNativePlace() {
		return NativePlace;
	}

	public void setNativePlace(String nativePlace) {
		NativePlace = nativePlace;
	}

	public String getPosition() {
		return Position;
	}

	public void setPosition(String position) {
		Position = position;
	}

	public String getSalary() {
		return Salary;
	}

	public void setSalary(String salary) {
		Salary = salary;
	}

	public String getIDStartDate() {
		return IDStartDate;
	}

	public void setIDStartDate(String iDStartDate) {
		IDStartDate = iDStartDate;
	}

	public String getIDEndDate() {
		return IDEndDate;
	}

	public void setIDEndDate(String iDEndDate) {
		IDEndDate = iDEndDate;
	}

	public String getGrpInsuredPhone() {
		return GrpInsuredPhone;
	}

	public void setGrpInsuredPhone(String grpInsuredPhone) {
		GrpInsuredPhone = grpInsuredPhone;
	}

}
