/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 网销复杂产品出单受益人信息
 * 
 * @author GZH
 *
 */
public class SHSYContTable extends BaseXmlSch
{

	private static final long serialVersionUID = -4794467791337930966L;
	private String ContNo;
	private String Sign;
	private String OrderNo;
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getSign() {
		return Sign;
	}
	public void setSign(String sign) {
		Sign = sign;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	
    
}
