/**
 * 2010-8-23
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 单证同步，获取可用单证
 * 
 * @author LY
 *
 */
public class CertSyncTable extends BaseXmlSch
{
    private static final long serialVersionUID = -9169488976853166570L;

    /** 管理机构 */
    private String ManageCom = null;

    /** 销售渠道 */
    private String SaleChnl = null;

    /** 销售渠道名称 */
    private String SaleChnlName = null;

    /** 中介机构 */
    private String AgentCom = null;

    /** 业务员代码 */
    private String AgentCode = null;

    /** 业务员姓名 */
    private String AgentName = null;

    /** 保险卡类型 */
    private String CertifyCode = null;

    /** 保险卡名称 */
    private String CertifyName = null;

    /** 套餐代码 */
    private String WrapCode = null;

    /** 套餐名称 */
    private String WrapName = null;

    /** 同步数量（需获取单证的数量） */
    private String SyncCount = null;

    /** 说明（备注） */
    private String Remark = null;

    /** 备用字段 */
    private String Bak1 = null;

    /** 备用字段 */
    private String Bak2 = null;

    /** 备用字段 */
    private String Bak3 = null;

    /** 备用字段 */
    private String Bak4 = null;

    /** 备用字段 */
    private String Bak5 = null;

    /** 备用字段 */
    private String Bak6 = null;

    /** 备用字段 */
    private String Bak7 = null;

    /** 备用字段 */
    private String BAK8 = null;

    /** 备用字段 */
    private String Bak9 = null;

    /** 备用字段 */
    private String Bak10 = null;

    /**
     * @return agentCode
     */
    public String getAgentCode()
    {
        return AgentCode;
    }

    /**
     * @param agentCode 要设置的 agentCode
     */
    public void setAgentCode(final String agentCode)
    {
        AgentCode = agentCode;
    }

    /**
     * @return agentCom
     */
    public String getAgentCom()
    {
        return AgentCom;
    }

    /**
     * @param agentCom 要设置的 agentCom
     */
    public void setAgentCom(final String agentCom)
    {
        AgentCom = agentCom;
    }

    /**
     * @return agentName
     */
    public String getAgentName()
    {
        return AgentName;
    }

    /**
     * @param agentName 要设置的 agentName
     */
    public void setAgentName(final String agentName)
    {
        AgentName = agentName;
    }

    /**
     * @return bak1
     */
    public String getBak1()
    {
        return Bak1;
    }

    /**
     * @param bak1 要设置的 bak1
     */
    public void setBak1(final String bak1)
    {
        Bak1 = bak1;
    }

    /**
     * @return bak10
     */
    public String getBak10()
    {
        return Bak10;
    }

    /**
     * @param bak10 要设置的 bak10
     */
    public void setBak10(final String bak10)
    {
        Bak10 = bak10;
    }

    /**
     * @return bak2
     */
    public String getBak2()
    {
        return Bak2;
    }

    /**
     * @param bak2 要设置的 bak2
     */
    public void setBak2(final String bak2)
    {
        Bak2 = bak2;
    }

    /**
     * @return bak3
     */
    public String getBak3()
    {
        return Bak3;
    }

    /**
     * @param bak3 要设置的 bak3
     */
    public void setBak3(final String bak3)
    {
        Bak3 = bak3;
    }

    /**
     * @return bak4
     */
    public String getBak4()
    {
        return Bak4;
    }

    /**
     * @param bak4 要设置的 bak4
     */
    public void setBak4(final String bak4)
    {
        Bak4 = bak4;
    }

    /**
     * @return bak5
     */
    public String getBak5()
    {
        return Bak5;
    }

    /**
     * @param bak5 要设置的 bak5
     */
    public void setBak5(final String bak5)
    {
        Bak5 = bak5;
    }

    /**
     * @return bak6
     */
    public String getBak6()
    {
        return Bak6;
    }

    /**
     * @param bak6 要设置的 bak6
     */
    public void setBak6(final String bak6)
    {
        Bak6 = bak6;
    }

    /**
     * @return bak7
     */
    public String getBak7()
    {
        return Bak7;
    }

    /**
     * @param bak7 要设置的 bak7
     */
    public void setBak7(final String bak7)
    {
        Bak7 = bak7;
    }

    /**
     * @return bAK8
     */
    public String getBAK8()
    {
        return BAK8;
    }

    /**
     * @param bak8 要设置的 bAK8
     */
    public void setBAK8(final String bak8)
    {
        BAK8 = bak8;
    }

    /**
     * @return bak9
     */
    public String getBak9()
    {
        return Bak9;
    }

    /**
     * @param bak9 要设置的 bak9
     */
    public void setBak9(final String bak9)
    {
        Bak9 = bak9;
    }

    /**
     * @return certifyCode
     */
    public String getCertifyCode()
    {
        return CertifyCode;
    }

    /**
     * @param certifyCode 要设置的 certifyCode
     */
    public void setCertifyCode(final String certifyCode)
    {
        CertifyCode = certifyCode;
    }

    /**
     * @return certifyName
     */
    public String getCertifyName()
    {
        return CertifyName;
    }

    /**
     * @param certifyName 要设置的 certifyName
     */
    public void setCertifyName(final String certifyName)
    {
        CertifyName = certifyName;
    }

    /**
     * @return manageCom
     */
    public String getManageCom()
    {
        return ManageCom;
    }

    /**
     * @param manageCom 要设置的 manageCom
     */
    public void setManageCom(final String manageCom)
    {
        ManageCom = manageCom;
    }

    /**
     * @return remark
     */
    public String getRemark()
    {
        return Remark;
    }

    /**
     * @param remark 要设置的 remark
     */
    public void setRemark(final String remark)
    {
        Remark = remark;
    }

    /**
     * @return saleChnl
     */
    public String getSaleChnl()
    {
        return SaleChnl;
    }

    /**
     * @param saleChnl 要设置的 saleChnl
     */
    public void setSaleChnl(final String saleChnl)
    {
        SaleChnl = saleChnl;
    }

    /**
     * @return saleChnlName
     */
    public String getSaleChnlName()
    {
        return SaleChnlName;
    }

    /**
     * @param saleChnlName 要设置的 saleChnlName
     */
    public void setSaleChnlName(final String saleChnlName)
    {
        SaleChnlName = saleChnlName;
    }

    /**
     * @return syncCount
     */
    public String getSyncCount()
    {
        return SyncCount;
    }

    /**
     * @param syncCount 要设置的 syncCount
     */
    public void setSyncCount(final String syncCount)
    {
        SyncCount = syncCount;
    }

    /**
     * @return wrapCode
     */
    public String getWrapCode()
    {
        return WrapCode;
    }

    /**
     * @param wrapCode 要设置的 wrapCode
     */
    public void setWrapCode(final String wrapCode)
    {
        WrapCode = wrapCode;
    }

    /**
     * @return wrapName
     */
    public String getWrapName()
    {
        return WrapName;
    }

    /**
     * @param wrapName 要设置的 wrapName
     */
    public void setWrapName(final String wrapName)
    {
        WrapName = wrapName;
    }
}
