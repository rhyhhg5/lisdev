/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保单信息
 * 
 * @author 
 *
 */
public class EdorRiskInfo extends BaseXmlSch
{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 8668888502010068730L;


    
    /** 被保人客户号*/
    private String InsuredNo = null;
    
    /** 险种名称 */
    private String RiskCode = null;

    /** 险种名称 */
    private String RiskName = null;

    
    /** 保费 */
    private String Prem = null;

    /** 保额 */
    private String Amnt  = null;

    /** 生效日期 */
    private String CValiDate = null;

    /** 终止日期 */
    private String EndDate = null;
    
	/** 缴费频次 */
    private String PayIntv = null;

    /** 交至日期 */
    private String PayToDate = null;



	public String getInsuredNo() {
		return InsuredNo;
	}

	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}

	public String getRiskCode() {
		return RiskCode;
	}

	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}

	public String getRiskName() {
		return RiskName;
	}

	public void setRiskName(String riskName) {
		RiskName = riskName;
	}



	public String getPrem() {
		return Prem;
	}

	public void setPrem(String prem) {
		Prem = prem;
	}

	public String getAmnt() {
		return Amnt;
	}

	public void setAmnt(String amnt) {
		Amnt = amnt;
	}

	public String getCValiDate() {
		return CValiDate;
	}

	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public String getPayIntv() {
		return PayIntv;
	}

	public void setPayIntv(String payIntv) {
		PayIntv = payIntv;
	}

	public String getPayToDate() {
		return PayToDate;
	}

	public void setPayToDate(String payToDate) {
		PayToDate = payToDate;
	}



}
