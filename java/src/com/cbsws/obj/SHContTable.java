/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class SHContTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String ContNo;
    private String State;
    private String PolApplyDate;
    private String Prem;
    private String Amnt;
    private String GetPolState;
    private String InsuredName;
	public String getGetPolState() {
		return GetPolState;
	}
	public void setGetPolState(String getPolState) {
		GetPolState = getPolState;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getPolApplyDate() {
		return PolApplyDate;
	}
	public void setPolApplyDate(String polApplyDate) {
		PolApplyDate = polApplyDate;
	}
	public String getPrem() {
		return Prem;
	}
	public void setPrem(String prem) {
		Prem = prem;
	}
	public String getAmnt() {
		return Amnt;
	}
	public void setAmnt(String amnt) {
		Amnt = amnt;
	}
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
    
    
}
