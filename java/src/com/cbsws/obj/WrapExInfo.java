/**
 * 2011-9-19
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public class WrapExInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -2687604397773932431L;

    /** 保险卡类型*/
    private String CertifyCode = null;

    /** 产品套餐名称*/
    private String ExFactorCode = null;

    /** 产品套餐保费*/
    private String ExFactorName = null;

    /** 产品套餐保额*/
    private String ExFactorValue = null;

    /**
     * @return certifyCode
     */
    public String getCertifyCode()
    {
        return CertifyCode;
    }

    /**
     * @param certifyCode 要设置的 certifyCode
     */
    public void setCertifyCode(String certifyCode)
    {
        CertifyCode = certifyCode;
    }

    /**
     * @return exFactorCode
     */
    public String getExFactorCode()
    {
        return ExFactorCode;
    }

    /**
     * @param exFactorCode 要设置的 exFactorCode
     */
    public void setExFactorCode(String exFactorCode)
    {
        ExFactorCode = exFactorCode;
    }

    /**
     * @return exFactorName
     */
    public String getExFactorName()
    {
        return ExFactorName;
    }

    /**
     * @param exFactorName 要设置的 exFactorName
     */
    public void setExFactorName(String exFactorName)
    {
        ExFactorName = exFactorName;
    }

    /**
     * @return exFactorValue
     */
    public String getExFactorValue()
    {
        return ExFactorValue;
    }

    /**
     * @param exFactorValue 要设置的 exFactorValue
     */
    public void setExFactorValue(String exFactorValue)
    {
        ExFactorValue = exFactorValue;
    }

}
