/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class LCAppntTable extends BaseXmlSch
{

	private static final long serialVersionUID = -4794467791337930966L;
	/** 保单号*/
    private String ContNo = null;

    /** 投保人序号 */
    private String AppntNo = null;

    /** 投保人姓名 */
    private String AppntName = null;

    /** 性别 */
    private String AppntSex = null;

    /** 出生日期 */
    private String AppntBirthday = null;

    /** 证件类型 */
    private String AppntIDType = null;
    
    /** 证件号码 */
    private String AppntIDNo = null;
    
    /** 移动电话 */
    private String AppntMobile = null;

    /** 家庭电话 */
    private String AppntPhone = null;

    /**  职业编码 */
    private String OccupationCode = null;
    
    /** 职业类型 */ 
    private String OccupationType = null;

    /** 电子邮箱 */
    private String Email = null;

    /** 通信地址 */
    private String MailAddress = null;

    /** 联系地址 */
    private String HomeAddress = null;
    
    /** 邮政编码 */
    private String MailZipCode = null;
    
    /** 投保人号码 反馈给电子商务*/
    private String ReAppntNo = null;
    
    /** 授权使用客户信息 */
	private String Authorization = null;
	
	/**共享标识 */
	private String SharedMark;
	/**特殊限定标识 */
	private String SpecialLimitMark;
	/**业务环节*/
	private String BusinessLink;
	/**业务触面*/
	private String CustomerContact;
	/**授权合约类型*/
	private String AuthType;
	

	/**
	 * @return the reAppntNo
	 */
	public String getReAppntNo() {
		return ReAppntNo;
	}

	/**
	 * @param reAppntNo the reAppntNo to set
	 */
	public void setReAppntNo(String reAppntNo) {
		ReAppntNo = reAppntNo;
	}

	/**
	 * @return the appntBirthday
	 */
	public String getAppntBirthday() {
		return AppntBirthday;
	}

	/**
	 * @param appntBirthday the appntBirthday to set
	 */
	public void setAppntBirthday(String appntBirthday) {
		AppntBirthday = appntBirthday;
	}

	/**
	 * @return the appntIDNo
	 */
	public String getAppntIDNo() {
		return AppntIDNo;
	}

	/**
	 * @param appntIDNo the appntIDNo to set
	 */
	public void setAppntIDNo(String appntIDNo) {
		AppntIDNo = appntIDNo;
	}

	/**
	 * @return the appntIDType
	 */
	public String getAppntIDType() {
		return AppntIDType;
	}

	/**
	 * @param appntIDType the appntIDType to set
	 */
	public void setAppntIDType(String appntIDType) {
		AppntIDType = appntIDType;
	}

	/**
	 * @return the appntMobile
	 */
	public String getAppntMobile() {
		return AppntMobile;
	}

	/**
	 * @param appntMobile the appntMobile to set
	 */
	public void setAppntMobile(String appntMobile) {
		AppntMobile = appntMobile;
	}

	/**
	 * @return the appntName
	 */
	public String getAppntName() {
		return AppntName;
	}

	/**
	 * @param appntName the appntName to set
	 */
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}

	/**
	 * @return the appntNo
	 */
	public String getAppntNo() {
		return AppntNo;
	}

	/**
	 * @param appntNo the appntNo to set
	 */
	public void setAppntNo(String appntNo) {
		AppntNo = appntNo;
	}

	/**
	 * @return the appntPhone
	 */
	public String getAppntPhone() {
		return AppntPhone;
	}

	/**
	 * @param appntPhone the appntPhone to set
	 */
	public void setAppntPhone(String appntPhone) {
		AppntPhone = appntPhone;
	}

	/**
	 * @return the appntSex
	 */
	public String getAppntSex() {
		return AppntSex;
	}

	/**
	 * @param appntSex the appntSex to set
	 */
	public void setAppntSex(String appntSex) {
		AppntSex = appntSex;
	}

	/**
	 * @return the contNo
	 */
	public String getContNo() {
		return ContNo;
	}

	/**
	 * @param contNo the contNo to set
	 */
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return Email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		Email = email;
	}

	/**
	 * @return the homeAddress
	 */
	public String getHomeAddress() {
		return HomeAddress;
	}

	/**
	 * @param homeAddress the homeAddress to set
	 */
	public void setHomeAddress(String homeAddress) {
		HomeAddress = homeAddress;
	}

	/**
	 * @return the occupationCode
	 */
	public String getOccupationCode() {
		return OccupationCode;
	}

	/**
	 * @param occupationCode the occupationCode to set
	 */
	public void setOccupationCode(String occupationCode) {
		OccupationCode = occupationCode;
	}

	/**
	 * @return the mailAddress
	 */
	public String getMailAddress() {
		return MailAddress;
	}

	/**
	 * @param mailAddress the mailAddress to set
	 */
	public void setMailAddress(String mailAddress) {
		MailAddress = mailAddress;
	}

	/**
	 * @return the mailZipCode
	 */
	public String getMailZipCode() {
		return MailZipCode;
	}

	/**
	 * @param mailZipCode the mailZipCode to set
	 */
	public void setMailZipCode(String mailZipCode) {
		MailZipCode = mailZipCode;
	}

	/**
	 * @return the occupationType
	 */
	public String getOccupationType() {
		return OccupationType;
	}

	/**
	 * @param occupationType the occupationType to set
	 */
	public void setOccupationType(String occupationType) {
		OccupationType = occupationType;
	}

	/**
	 * @return the authorization
	 */
	public String getAuthorization() {
		return Authorization;
	}

	/**
	 * @param authorization the authorization to set
	 */
	public void setAuthorization(String authorization) {
		Authorization = authorization;
	}

	public String getSharedMark() {
		return SharedMark;
	}

	public void setSharedMark(String sharedMark) {
		SharedMark = sharedMark;
	}

	public String getSpecialLimitMark() {
		return SpecialLimitMark;
	}

	public void setSpecialLimitMark(String specialLimitMark) {
		SpecialLimitMark = specialLimitMark;
	}

	public String getBusinessLink() {
		return BusinessLink;
	}

	public void setBusinessLink(String businessLink) {
		BusinessLink = businessLink;
	}

	public String getCustomerContact() {
		return CustomerContact;
	}

	public void setCustomerContact(String customerContact) {
		CustomerContact = customerContact;
	}

	public String getAuthType() {
		return AuthType;
	}

	public void setAuthType(String authType) {
		AuthType = authType;
	}
	
}
