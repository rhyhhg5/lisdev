/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class RYInsuredRiskInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String IRInsuredName;
    private String IRInsuredIdNo;
    private String IRRiskName;
    private String IRTerm;
    private String IREndDate;
    private String IRPaySep;
    private String IRPayTerm;
    private String IRAmnt;
    private String IRPrem;
    private String IRCvaliDate;
    private String IRPaytoDate;
    private String IRRiskState;
	public String getIRInsuredName() {
		return IRInsuredName;
	}
	public void setIRInsuredName(String iRInsuredName) {
		IRInsuredName = iRInsuredName;
	}
	public String getIRInsuredIdNo() {
		return IRInsuredIdNo;
	}
	public void setIRInsuredIdNo(String iRInsuredIdNo) {
		IRInsuredIdNo = iRInsuredIdNo;
	}
	public String getIRRiskName() {
		return IRRiskName;
	}
	public void setIRRiskName(String iRRiskName) {
		IRRiskName = iRRiskName;
	}
	public String getIRTerm() {
		return IRTerm;
	}
	public void setIRTerm(String iRTerm) {
		IRTerm = iRTerm;
	}
	public String getIREndDate() {
		return IREndDate;
	}
	public void setIREndDate(String iREndDate) {
		IREndDate = iREndDate;
	}
	public String getIRPaySep() {
		return IRPaySep;
	}
	public void setIRPaySep(String iRPaySep) {
		IRPaySep = iRPaySep;
	}
	public String getIRPayTerm() {
		return IRPayTerm;
	}
	public void setIRPayTerm(String iRPayTerm) {
		IRPayTerm = iRPayTerm;
	}
	public String getIRAmnt() {
		return IRAmnt;
	}
	public void setIRAmnt(String iRAmnt) {
		IRAmnt = iRAmnt;
	}
	public String getIRPrem() {
		return IRPrem;
	}
	public void setIRPrem(String iRPrem) {
		IRPrem = iRPrem;
	}
	public String getIRCvaliDate() {
		return IRCvaliDate;
	}
	public void setIRCvaliDate(String iRCvaliDate) {
		IRCvaliDate = iRCvaliDate;
	}
	public String getIRPaytoDate() {
		return IRPaytoDate;
	}
	public void setIRPaytoDate(String iRPaytoDate) {
		IRPaytoDate = iRPaytoDate;
	}
	public String getIRRiskState() {
		return IRRiskState;
	}
	public void setIRRiskState(String iRRiskState) {
		IRRiskState = iRRiskState;
	}
    
	
    
    
    
    
    
    
    
    
	
    
    
    
}
