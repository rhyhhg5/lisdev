/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class SYOtherTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 户口所在地*/
    private String RgtAddress = null;

    /** 婚姻状况 */
    private String Marriage = null;

    /** 国籍 */
    private String NativePlace = null;

    /** 证件生效日期 */
    private String IDStartdate = null;

    /** 证件终止日期  */
    private String IDEnddate = null;
    
    /** 学历 */
    private String Degree=null;
    
    private String Salary=null;
    
    private String NativeCity=null;
    
    private String GrpName=null;
    
    private String GrpNo=null;
    
	/** 初审人 */
	private String FirstTrialOperator=null;
	
	public String getFirstTrialOperator() {
		return FirstTrialOperator;
	}

	public void setFirstTrialOperator(String firstTrialOperator) {
		FirstTrialOperator = firstTrialOperator;
	}


	public String getGrpNo() {
		return GrpNo;
	}

	public void setGrpNo(String grpNo) {
		GrpNo = grpNo;
	}

	public String getSalary() {
		return Salary;
	}

	public void setSalary(String salary) {
		Salary = salary;
	}

	public String getNativeCity() {
		return NativeCity;
	}

	public String getGrpName() {
		return GrpName;
	}

	public void setGrpName(String grpName) {
		GrpName = grpName;
	}

	public void setNativeCity(String nativeCity) {
		NativeCity = nativeCity;
	}

	public String getRgtAddress() {
		return RgtAddress;
	}

	public void setRgtAddress(String rgtAddress) {
		RgtAddress = rgtAddress;
	}

	public String getMarriage() {
		return Marriage;
	}

	public void setMarriage(String marriage) {
		Marriage = marriage;
	}

	public String getNativePlace() {
		return NativePlace;
	}

	public void setNativePlace(String nativePlace) {
		NativePlace = nativePlace;
	}

	public String getIDStartdate() {
		return IDStartdate;
	}

	public void setIDStartdate(String iDStartdate) {
		IDStartdate = iDStartdate;
	}

	public String getIDEnddate() {
		return IDEnddate;
	}

	public void setIDEnddate(String iDEnddate) {
		IDEnddate = iDEnddate;
	}

	public String getDegree() {
		return Degree;
	}

	public void setDegree(String degree) {
		Degree = degree;
	}
    
   
	
}
