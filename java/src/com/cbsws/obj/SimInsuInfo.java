/**
 * 2011-9-19
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public class SimInsuInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -5109911076912200240L;

    /** 被保人编号*/
    private String InsuNo = null;

    /** 与主被保人关系*/
    private String ToMainInsuRela = null;

    /** 与投保人关系*/
    private String ToAppntRela = null;

    /** 被保人姓名*/
    private String Name = null;

    /** 性别*/
    private String Sex = null;

    /** 出生日期*/
    private String Birthday = null;

    /** 证件类型*/
    private String IDType = null;

    /** 证件号码*/
    private String IdNo = null;

    /** 职业类别*/
    private String OccupationType = null;

    /** 职业代码*/
    private String OccupationCode = null;

    /** 联系地址*/
    private String PostalAddress = null;

    /** 联系地址邮编*/
    private String ZipCode = null;

    /** 联系电话*/
    private String Phone = null;

    /** 手机*/
    private String Mobile = null;

    /** 电子邮箱*/
    private String Email = null;

    /** 单位名称*/
    private String GrpName = null;

    /** 单位电话*/
    private String CompayPhone = null;

    /** 单位地址*/
    private String CompAddr = null;

    /** 单位地址邮编*/
    private String CompZipCode = null;

    /** 英文姓名*/
    private String EnglishName = null;

    /**
     * @return birthday
     */
    public String getBirthday()
    {
        return Birthday;
    }

    /**
     * @param birthday 要设置的 birthday
     */
    public void setBirthday(String birthday)
    {
        Birthday = birthday;
    }

    /**
     * @return compAddr
     */
    public String getCompAddr()
    {
        return CompAddr;
    }

    /**
     * @param compAddr 要设置的 compAddr
     */
    public void setCompAddr(String compAddr)
    {
        CompAddr = compAddr;
    }

    /**
     * @return compayPhone
     */
    public String getCompayPhone()
    {
        return CompayPhone;
    }

    /**
     * @param compayPhone 要设置的 compayPhone
     */
    public void setCompayPhone(String compayPhone)
    {
        CompayPhone = compayPhone;
    }

    /**
     * @return compZipCode
     */
    public String getCompZipCode()
    {
        return CompZipCode;
    }

    /**
     * @param compZipCode 要设置的 compZipCode
     */
    public void setCompZipCode(String compZipCode)
    {
        CompZipCode = compZipCode;
    }

    /**
     * @return email
     */
    public String getEmail()
    {
        return Email;
    }

    /**
     * @param email 要设置的 email
     */
    public void setEmail(String email)
    {
        Email = email;
    }

    /**
     * @return englishName
     */
    public String getEnglishName()
    {
        return EnglishName;
    }

    /**
     * @param englishName 要设置的 englishName
     */
    public void setEnglishName(String englishName)
    {
        EnglishName = englishName;
    }

    /**
     * @return grpName
     */
    public String getGrpName()
    {
        return GrpName;
    }

    /**
     * @param grpName 要设置的 grpName
     */
    public void setGrpName(String grpName)
    {
        GrpName = grpName;
    }

    /**
     * @return idNo
     */
    public String getIdNo()
    {
        return IdNo;
    }

    /**
     * @param idNo 要设置的 idNo
     */
    public void setIdNo(String idNo)
    {
        IdNo = idNo;
    }

    /**
     * @return iDType
     */
    public String getIDType()
    {
        return IDType;
    }

    /**
     * @param type 要设置的 iDType
     */
    public void setIDType(String type)
    {
        IDType = type;
    }

    /**
     * @return insuNo
     */
    public String getInsuNo()
    {
        return InsuNo;
    }

    /**
     * @param insuNo 要设置的 insuNo
     */
    public void setInsuNo(String insuNo)
    {
        InsuNo = insuNo;
    }

    /**
     * @return mobile
     */
    public String getMobile()
    {
        return Mobile;
    }

    /**
     * @param mobile 要设置的 mobile
     */
    public void setMobile(String mobile)
    {
        Mobile = mobile;
    }

    /**
     * @return name
     */
    public String getName()
    {
        return Name;
    }

    /**
     * @param name 要设置的 name
     */
    public void setName(String name)
    {
        Name = name;
    }

    /**
     * @return occupationCode
     */
    public String getOccupationCode()
    {
        return OccupationCode;
    }

    /**
     * @param occupationCode 要设置的 occupationCode
     */
    public void setOccupationCode(String occupationCode)
    {
        OccupationCode = occupationCode;
    }

    /**
     * @return occupationType
     */
    public String getOccupationType()
    {
        return OccupationType;
    }

    /**
     * @param occupationType 要设置的 occupationType
     */
    public void setOccupationType(String occupationType)
    {
        OccupationType = occupationType;
    }

    /**
     * @return phone
     */
    public String getPhone()
    {
        return Phone;
    }

    /**
     * @param phone 要设置的 phone
     */
    public void setPhone(String phone)
    {
        Phone = phone;
    }

    /**
     * @return postalAddress
     */
    public String getPostalAddress()
    {
        return PostalAddress;
    }

    /**
     * @param postalAddress 要设置的 postalAddress
     */
    public void setPostalAddress(String postalAddress)
    {
        PostalAddress = postalAddress;
    }

    /**
     * @return sex
     */
    public String getSex()
    {
        return Sex;
    }

    /**
     * @param sex 要设置的 sex
     */
    public void setSex(String sex)
    {
        Sex = sex;
    }

    /**
     * @return toAppntRela
     */
    public String getToAppntRela()
    {
        return ToAppntRela;
    }

    /**
     * @param toAppntRela 要设置的 toAppntRela
     */
    public void setToAppntRela(String toAppntRela)
    {
        ToAppntRela = toAppntRela;
    }

    /**
     * @return toMainInsuRela
     */
    public String getToMainInsuRela()
    {
        return ToMainInsuRela;
    }

    /**
     * @param toMainInsuRela 要设置的 toMainInsuRela
     */
    public void setToMainInsuRela(String toMainInsuRela)
    {
        ToMainInsuRela = toMainInsuRela;
    }

    /**
     * @return zipCode
     */
    public String getZipCode()
    {
        return ZipCode;
    }

    /**
     * @param zipCode 要设置的 zipCode
     */
    public void setZipCode(String zipCode)
    {
        ZipCode = zipCode;
    }

}
