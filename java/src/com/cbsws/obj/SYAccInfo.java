package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SYAccInfo extends BaseXmlSch{


	private static final long serialVersionUID = -1533051897224538269L;
	
	/**
	 * 保单号
	 */
	private String ContNo;
	
	/**
	 * 保单首次生效日期
	 */
	private String Cvalidate;
	
	/**
	 * 万能账户余额
	 */
	private String Insuaccbala;
	
	/**
	 * 初始金额
	 */
	private String InitMoney;
	
	/**
	 * 累计缴费金额
	 */
	private String SumInitMoney;
	
	/**
	 * 利息积累
	 */
	private String SumLX;
	
	/**
	 * 累计差额返还
	 */
	private String SumBR;
	
	/**
	 * 上月利息
	 */
	private String LastLX;
	
	/**
	 * 上年差额返还金额
	 */
	private String LastBR;
	
	/**
	 * 实时结算利率
	 */
	private String Rate;

	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public String getInsuaccbala() {
		return Insuaccbala;
	}

	public void setInsuaccbala(String insuaccbala) {
		Insuaccbala = insuaccbala;
	}

	public String getInitMoney() {
		return InitMoney;
	}

	public void setInitMoney(String initMoney) {
		InitMoney = initMoney;
	}

	public String getSumLX() {
		return SumLX;
	}

	public void setSumLX(String sumLX) {
		SumLX = sumLX;
	}

	public String getSumBR() {
		return SumBR;
	}

	public void setSumBR(String sumBR) {
		SumBR = sumBR;
	}

	public String getLastLX() {
		return LastLX;
	}

	public void setLastLX(String lastLX) {
		LastLX = lastLX;
	}

	public String getLastBR() {
		return LastBR;
	}

	public void setLastBR(String lastBR) {
		LastBR = lastBR;
	}

	public String getCvalidate() {
		return Cvalidate;
	}

	public void setCvalidate(String cvalidate) {
		Cvalidate = cvalidate;
	}

	public String getSumInitMoney() {
		return SumInitMoney;
	}

	public void setSumInitMoney(String sumInitMoney) {
		SumInitMoney = sumInitMoney;
	}

	public String getRate() {
		return Rate;
	}

	public void setRate(String rate) {
		Rate = rate;
	}

}
