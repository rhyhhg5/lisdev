/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保全项目信息
 * 
 * @author lzy
 *
 */
public class EdorItemInfo extends BaseXmlSch
{
	
	private static final long serialVersionUID = 6724525694985587861L;

	/** 印刷号 */
    private String PrtNo = null;

    /** 保单号*/
    private String ContNo = null;

    /** 管理机构 */
    private String ManageCom = null;

    /** 销售渠道 */
    private String Salechnl = null;
    
    /** 保单类型*/
    private String CardFlag = null;

    /** 保险生效日期 */
    private String CValiDate = null;
    
    /** 保险失效日期 */
    private String CInValiDate = null;
    
    /** 保全项目代码 */
    private String EdorType = null;
    
    /** 退保原因 */
    private String Reason = null;
    
    /** 工本费 */
    private String Fee = null;
    
    /** 保全生效日期 */
    private String EdorValidate =  null;
    
    /** 退保试算金额 */
    private String BudGet =  null;

	public String getPrtNo() {
		return PrtNo;
	}

	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getSalechnl() {
		return Salechnl;
	}

	public void setSalechnl(String saleChnl) {
		Salechnl = saleChnl;
	}

	public String getCardFlag() {
		return CardFlag;
	}

	public void setCardFlag(String cardFlag) {
		CardFlag = cardFlag;
	}

	public String getCValiDate() {
		return CValiDate;
	}

	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}

	public String getCInValiDate() {
		return CInValiDate;
	}

	public void setCInValiDate(String cInValiDate) {
		CInValiDate = cInValiDate;
	}

	public String getEdorType() {
		return EdorType;
	}

	public void setEdorType(String edorType) {
		EdorType = edorType;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}

	public String getFee() {
		return Fee;
	}

	public void setFee(String fee) {
		Fee = fee;
	}

	public String getEdorValidate() {
		return EdorValidate;
	}

	public void setEdorValidate(String edorValidate) {
		EdorValidate = edorValidate;
	}

	public String getBudGet() {
		return BudGet;
	}

	public void setBudGet(String budGet) {
		BudGet = budGet;
	}
	
	
    
}
