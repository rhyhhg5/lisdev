/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class RYCustomerImpartTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    
    private String ImpartCode;
    private String ImpartVer;
    private String impartParamModle;
    
	public String getImpartCode() {
		return ImpartCode;
	}
	public void setImpartCode(String impartCode) {
		ImpartCode = impartCode;
	}
	public String getImpartVer() {
		return ImpartVer;
	}
	public void setImpartVer(String impartVer) {
		ImpartVer = impartVer;
	}
	public String getImpartParamModle() {
		return impartParamModle;
	}
	public void setImpartParamModle(String impartParamModle) {
		this.impartParamModle = impartParamModle;
	}
    
    
    
  
	
}
