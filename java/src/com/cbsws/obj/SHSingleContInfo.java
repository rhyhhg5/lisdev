/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class SHSingleContInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
   
    private String ContNo;

	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
 

    
}
