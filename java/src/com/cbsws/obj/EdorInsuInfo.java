/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保单信息
 * 
 * @author 
 *
 */
public class EdorInsuInfo extends BaseXmlSch
{
    

    /**
	 * 
	 */
	private static final long serialVersionUID = 5806319654406968303L;

	/** 被保人姓名 */
	private String InsuredName;
	/** 被保人客户号码 */
	private String InsuredNo;
//	/** 被保人证件类型 */
//	private String InsuredIdType;
	/** 被保人证件号码 */
	private String InsuredIdNo;
	/** 被保人家庭地址 */
	private String InsuredHomeAddress;
	/** 被保人家庭邮编 */
	private String InsuredHomeZipCode;
	/** 被保人家庭电话 */
	private String InsuredHomePhone;
	/** 被保人家庭传真 */
	private String InsuredHomeFax;
	/** 被保人单位地址 */
	private String InsuredCompanyAddress;
	/** 被保人单位邮编 */
	private String InsuredCompanyZipCode;
	/** 被保人单位电话 */
	private String InsuredCompanyPhone;
	/** 被保人单位传真 */
	private String InsuredCompanyFax;
	/** 被保人通讯地址 */
	private String InsuredPostalAddress;
	/** 被保人通讯邮编 */
	private String InsuredZipCode;
	/** 被保人通讯电话 */
	private String InsuredPhone;
	/** 被保人通讯传真 */
	private String InsuredFax;
	/** 被保人手机 */
	private String InsuredMobile;
	/** 被保人E_mail */
	private String InsuredEMail;
	/** 被保人出生日期 */
	private String InsuredBirthday;
	/** 被保人性别 */
	private String InsuredSex;


//	/** 与主被保险人关系 */
//	private String RelationToAppnt;
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
	public String getInsuredNo() {
		return InsuredNo;
	}
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}
	public String getInsuredIdNo() {
		return InsuredIdNo;
	}
	public void setInsuredIdNo(String insuredIdNo) {
		InsuredIdNo = insuredIdNo;
	}
	public String getInsuredHomeAddress() {
		return InsuredHomeAddress;
	}
	public void setInsuredHomeAddress(String insuredHomeAddress) {
		InsuredHomeAddress = insuredHomeAddress;
	}
	public String getInsuredHomeZipCode() {
		return InsuredHomeZipCode;
	}
	public void setInsuredHomeZipCode(String insuredHomeZipCode) {
		InsuredHomeZipCode = insuredHomeZipCode;
	}
	public String getInsuredHomePhone() {
		return InsuredHomePhone;
	}
	public void setInsuredHomePhone(String insuredHomePhone) {
		InsuredHomePhone = insuredHomePhone;
	}
	public String getInsuredHomeFax() {
		return InsuredHomeFax;
	}
	public void setInsuredHomeFax(String insuredHomeFax) {
		InsuredHomeFax = insuredHomeFax;
	}
	public String getInsuredCompanyAddress() {
		return InsuredCompanyAddress;
	}
	public void setInsuredCompanyAddress(String insuredCompanyAddress) {
		InsuredCompanyAddress = insuredCompanyAddress;
	}
	public String getInsuredCompanyZipCode() {
		return InsuredCompanyZipCode;
	}
	public void setInsuredCompanyZipCode(String insuredCompanyZipCode) {
		InsuredCompanyZipCode = insuredCompanyZipCode;
	}
	public String getInsuredCompanyPhone() {
		return InsuredCompanyPhone;
	}
	public void setInsuredCompanyPhone(String insuredCompanyPhone) {
		InsuredCompanyPhone = insuredCompanyPhone;
	}
	public String getInsuredCompanyFax() {
		return InsuredCompanyFax;
	}
	public void setInsuredCompanyFax(String insuredCompanyFax) {
		InsuredCompanyFax = insuredCompanyFax;
	}
	public String getInsuredPostalAddress() {
		return InsuredPostalAddress;
	}
	public void setInsuredPostalAddress(String insuredPostalAddress) {
		InsuredPostalAddress = insuredPostalAddress;
	}
	public String getInsuredZipCode() {
		return InsuredZipCode;
	}
	public void setInsuredZipCode(String insuredZipCode) {
		InsuredZipCode = insuredZipCode;
	}
	public String getInsuredPhone() {
		return InsuredPhone;
	}
	public void setInsuredPhone(String insuredPhone) {
		InsuredPhone = insuredPhone;
	}
	public String getInsuredFax() {
		return InsuredFax;
	}
	public void setInsuredFax(String insuredFax) {
		InsuredFax = insuredFax;
	}
	public String getInsuredMobile() {
		return InsuredMobile;
	}
	public void setInsuredMobile(String insuredMobile) {
		InsuredMobile = insuredMobile;
	}
	public String getInsuredEMail() {
		return InsuredEMail;
	}
	public void setInsuredEMail(String insuredEMail) {
		InsuredEMail = insuredEMail;
	}
	public String getInsuredSex() {
		return InsuredSex;
	}
	public void setInsuredSex(String insuredSex) {
		InsuredSex = insuredSex;
	}
	public String getInsuredBirthday() {
		return InsuredBirthday;
	}
	public void setInsuredBirthday(String insuredBirthday) {
		InsuredBirthday = insuredBirthday;
	}

}
