/**
 * 2011-9-19
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public class SimPolicyInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -8847816865626207189L;

    /** 保单印刷号 */
    private String PrtNo = null;

    /** 管理机构 */
    private String ManageCom = null;

    /** 销售渠道 */
    private String SaleChnl = null;

    /** 中介公司代码 */
    private String AgentCom = null;

    /** 业务员代码 */
    private String AgentCode = null;

    /** 业务员姓名 */
    private String AgentName = null;

    /** 投保单申请日期 */
    private String PolApplyDate = null;

    /** 保险责任生效日期 */
    private String CValiDate = null;

    /** 保险责任生效时间 */
    private String CValiTime = null;

    /** 保险责任终止日期 */
    private String CInValiDate = null;

    /** 保险责任终止时间 */
    private String CInValiTime = null;

    /** 保障年期 */
    private String InsuYear = null;

    /** 保障年期单位 */
    private String InsuYearFlag = null;

    /** 保费合计 */
    private String PremScope = null;

    /** 缴费频次 */
    private String PayIntv = null;

    /** 缴费年期 */
    private String PayYear = null;

    /** 缴费年期标志 */
    private String PayYearFlag = null;

    /** 缴费方式 */
    private String PayMode = null;

    /** 银行代码 */
    private String BankCode = null;

    /** 帐户号码 */
    private String BankAccNo = null;

    /** 帐户名称 */
    private String AccName = null;

    /** 特别约定 */
    private String Remark = null;

    /**
     * @return accName
     */
    public String getAccName()
    {
        return AccName;
    }

    /**
     * @param accName 要设置的 accName
     */
    public void setAccName(String accName)
    {
        AccName = accName;
    }

    /**
     * @return agentCode
     */
    public String getAgentCode()
    {
        return AgentCode;
    }

    /**
     * @param agentCode 要设置的 agentCode
     */
    public void setAgentCode(String agentCode)
    {
        AgentCode = agentCode;
    }

    /**
     * @return agentCom
     */
    public String getAgentCom()
    {
        return AgentCom;
    }

    /**
     * @param agentCom 要设置的 agentCom
     */
    public void setAgentCom(String agentCom)
    {
        AgentCom = agentCom;
    }

    /**
     * @return agentName
     */
    public String getAgentName()
    {
        return AgentName;
    }

    /**
     * @param agentName 要设置的 agentName
     */
    public void setAgentName(String agentName)
    {
        AgentName = agentName;
    }

    /**
     * @return bankAccNo
     */
    public String getBankAccNo()
    {
        return BankAccNo;
    }

    /**
     * @param bankAccNo 要设置的 bankAccNo
     */
    public void setBankAccNo(String bankAccNo)
    {
        BankAccNo = bankAccNo;
    }

    /**
     * @return bankCode
     */
    public String getBankCode()
    {
        return BankCode;
    }

    /**
     * @param bankCode 要设置的 bankCode
     */
    public void setBankCode(String bankCode)
    {
        BankCode = bankCode;
    }

    /**
     * @return cInValiDate
     */
    public String getCInValiDate()
    {
        return CInValiDate;
    }

    /**
     * @param inValiDate 要设置的 cInValiDate
     */
    public void setCInValiDate(String inValiDate)
    {
        CInValiDate = inValiDate;
    }

    /**
     * @return cInValiTime
     */
    public String getCInValiTime()
    {
        return CInValiTime;
    }

    /**
     * @param inValiTime 要设置的 cInValiTime
     */
    public void setCInValiTime(String inValiTime)
    {
        CInValiTime = inValiTime;
    }

    /**
     * @return cValiDate
     */
    public String getCValiDate()
    {
        return CValiDate;
    }

    /**
     * @param valiDate 要设置的 cValiDate
     */
    public void setCValiDate(String valiDate)
    {
        CValiDate = valiDate;
    }

    /**
     * @return cValiTime
     */
    public String getCValiTime()
    {
        return CValiTime;
    }

    /**
     * @param valiTime 要设置的 cValiTime
     */
    public void setCValiTime(String valiTime)
    {
        CValiTime = valiTime;
    }

    /**
     * @return insuYear
     */
    public String getInsuYear()
    {
        return InsuYear;
    }

    /**
     * @param insuYear 要设置的 insuYear
     */
    public void setInsuYear(String insuYear)
    {
        InsuYear = insuYear;
    }

    /**
     * @return insuYearFlag
     */
    public String getInsuYearFlag()
    {
        return InsuYearFlag;
    }

    /**
     * @param insuYearFlag 要设置的 insuYearFlag
     */
    public void setInsuYearFlag(String insuYearFlag)
    {
        InsuYearFlag = insuYearFlag;
    }

    /**
     * @return manageCom
     */
    public String getManageCom()
    {
        return ManageCom;
    }

    /**
     * @param manageCom 要设置的 manageCom
     */
    public void setManageCom(String manageCom)
    {
        ManageCom = manageCom;
    }

    /**
     * @return payIntv
     */
    public String getPayIntv()
    {
        return PayIntv;
    }

    /**
     * @param payIntv 要设置的 payIntv
     */
    public void setPayIntv(String payIntv)
    {
        PayIntv = payIntv;
    }

    /**
     * @return payMode
     */
    public String getPayMode()
    {
        return PayMode;
    }

    /**
     * @param payMode 要设置的 payMode
     */
    public void setPayMode(String payMode)
    {
        PayMode = payMode;
    }

    /**
     * @return payYear
     */
    public String getPayYear()
    {
        return PayYear;
    }

    /**
     * @param payYear 要设置的 payYear
     */
    public void setPayYear(String payYear)
    {
        PayYear = payYear;
    }

    /**
     * @return payYearFlag
     */
    public String getPayYearFlag()
    {
        return PayYearFlag;
    }

    /**
     * @param payYearFlag 要设置的 payYearFlag
     */
    public void setPayYearFlag(String payYearFlag)
    {
        PayYearFlag = payYearFlag;
    }

    /**
     * @return polApplyDate
     */
    public String getPolApplyDate()
    {
        return PolApplyDate;
    }

    /**
     * @param polApplyDate 要设置的 polApplyDate
     */
    public void setPolApplyDate(String polApplyDate)
    {
        PolApplyDate = polApplyDate;
    }

    /**
     * @return premScope
     */
    public String getPremScope()
    {
        return PremScope;
    }

    /**
     * @param premScope 要设置的 premScope
     */
    public void setPremScope(String premScope)
    {
        PremScope = premScope;
    }

    /**
     * @return prtNo
     */
    public String getPrtNo()
    {
        return PrtNo;
    }

    /**
     * @param prtNo 要设置的 prtNo
     */
    public void setPrtNo(String prtNo)
    {
        PrtNo = prtNo;
    }

    /**
     * @return remark
     */
    public String getRemark()
    {
        return Remark;
    }

    /**
     * @param remark 要设置的 remark
     */
    public void setRemark(String remark)
    {
        Remark = remark;
    }

    /**
     * @return saleChnl
     */
    public String getSaleChnl()
    {
        return SaleChnl;
    }

    /**
     * @param saleChnl 要设置的 saleChnl
     */
    public void setSaleChnl(String saleChnl)
    {
        SaleChnl = saleChnl;
    }
}
