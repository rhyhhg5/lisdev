/**
 * 2010-8-25
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public class ECCertSalesInfoTable extends BaseXmlSch
{
    private static final long serialVersionUID = 4986684520997782635L;

    private String CardNo = null;

    private String Password = null;

    private String ManageCom = null;

    private String SaleChnl = null;

    private String SaleChnlName = null;

    private String AgentCom = null;

    private String AgentCode = null;

    private String AgentName = null;

    private String CertifyCode = null;

    private String CertifyName = null;

    private String WrapCode = null;

    private String WrapName = null;

    private String ReBate = null;

    private String StandPrem = null;

    private String Prem = null;

    private String Amnt = null;

    private String Copys = null;

    private String Mult = null;

    private String Bak1 = null;

    private String Bak2 = null;

    private String Bak3 = null;

    private String Bak4 = null;

    private String Bak5 = null;

    private String Bak6 = null;

    private String Bak7 = null;

    private String Bak8 = null;

    private String Bak9 = null;

    private String Bak10 = null;

    /**
     * @return agentCode
     */
    public String getAgentCode()
    {
        return AgentCode;
    }

    /**
     * @param agentCode 要设置的 agentCode
     */
    public void setAgentCode(final String agentCode)
    {
        AgentCode = agentCode;
    }

    /**
     * @return agentCom
     */
    public String getAgentCom()
    {
        return AgentCom;
    }

    /**
     * @param agentCom 要设置的 agentCom
     */
    public void setAgentCom(final String agentCom)
    {
        AgentCom = agentCom;
    }

    /**
     * @return agentName
     */
    public String getAgentName()
    {
        return AgentName;
    }

    /**
     * @param agentName 要设置的 agentName
     */
    public void setAgentName(final String agentName)
    {
        AgentName = agentName;
    }

    /**
     * @return amnt
     */
    public String getAmnt()
    {
        return Amnt;
    }

    /**
     * @param amnt 要设置的 amnt
     */
    public void setAmnt(final String amnt)
    {
        Amnt = amnt;
    }

    /**
     * @return bak1
     */
    public String getBak1()
    {
        return Bak1;
    }

    /**
     * @param bak1 要设置的 bak1
     */
    public void setBak1(final String bak1)
    {
        Bak1 = bak1;
    }

    /**
     * @return bak10
     */
    public String getBak10()
    {
        return Bak10;
    }

    /**
     * @param bak10 要设置的 bak10
     */
    public void setBak10(final String bak10)
    {
        Bak10 = bak10;
    }

    /**
     * @return bak2
     */
    public String getBak2()
    {
        return Bak2;
    }

    /**
     * @param bak2 要设置的 bak2
     */
    public void setBak2(final String bak2)
    {
        Bak2 = bak2;
    }

    /**
     * @return bak3
     */
    public String getBak3()
    {
        return Bak3;
    }

    /**
     * @param bak3 要设置的 bak3
     */
    public void setBak3(final String bak3)
    {
        Bak3 = bak3;
    }

    /**
     * @return bak4
     */
    public String getBak4()
    {
        return Bak4;
    }

    /**
     * @param bak4 要设置的 bak4
     */
    public void setBak4(final String bak4)
    {
        Bak4 = bak4;
    }

    /**
     * @return bak5
     */
    public String getBak5()
    {
        return Bak5;
    }

    /**
     * @param bak5 要设置的 bak5
     */
    public void setBak5(final String bak5)
    {
        Bak5 = bak5;
    }

    /**
     * @return bak6
     */
    public String getBak6()
    {
        return Bak6;
    }

    /**
     * @param bak6 要设置的 bak6
     */
    public void setBak6(final String bak6)
    {
        Bak6 = bak6;
    }

    /**
     * @return bak7
     */
    public String getBak7()
    {
        return Bak7;
    }

    /**
     * @param bak7 要设置的 bak7
     */
    public void setBak7(final String bak7)
    {
        Bak7 = bak7;
    }

    /**
     * @return bak8
     */
    public String getBak8()
    {
        return Bak8;
    }

    /**
     * @param bak8 要设置的 bak8
     */
    public void setBak8(final String bak8)
    {
        Bak8 = bak8;
    }

    /**
     * @return bak9
     */
    public String getBak9()
    {
        return Bak9;
    }

    /**
     * @param bak9 要设置的 bak9
     */
    public void setBak9(final String bak9)
    {
        Bak9 = bak9;
    }

    /**
     * @return cardNo
     */
    public String getCardNo()
    {
        return CardNo;
    }

    /**
     * @param cardNo 要设置的 cardNo
     */
    public void setCardNo(final String cardNo)
    {
        CardNo = cardNo;
    }

    /**
     * @return certifyCode
     */
    public String getCertifyCode()
    {
        return CertifyCode;
    }

    /**
     * @param certifyCode 要设置的 certifyCode
     */
    public void setCertifyCode(final String certifyCode)
    {
        CertifyCode = certifyCode;
    }

    /**
     * @return certifyName
     */
    public String getCertifyName()
    {
        return CertifyName;
    }

    /**
     * @param certifyName 要设置的 certifyName
     */
    public void setCertifyName(final String certifyName)
    {
        CertifyName = certifyName;
    }

    /**
     * @return copys
     */
    public String getCopys()
    {
        return Copys;
    }

    /**
     * @param copys 要设置的 copys
     */
    public void setCopys(final String copys)
    {
        Copys = copys;
    }

    /**
     * @return manageCom
     */
    public String getManageCom()
    {
        return ManageCom;
    }

    /**
     * @param manageCom 要设置的 manageCom
     */
    public void setManageCom(final String manageCom)
    {
        ManageCom = manageCom;
    }

    /**
     * @return mult
     */
    public String getMult()
    {
        return Mult;
    }

    /**
     * @param mult 要设置的 mult
     */
    public void setMult(final String mult)
    {
        Mult = mult;
    }

    /**
     * @return password
     */
    public String getPassword()
    {
        return Password;
    }

    /**
     * @param password 要设置的 password
     */
    public void setPassword(final String password)
    {
        Password = password;
    }

    /**
     * @return prem
     */
    public String getPrem()
    {
        return Prem;
    }

    /**
     * @param prem 要设置的 prem
     */
    public void setPrem(final String prem)
    {
        Prem = prem;
    }

    /**
     * @return reBate
     */
    public String getReBate()
    {
        return ReBate;
    }

    /**
     * @param reBate 要设置的 reBate
     */
    public void setReBate(final String reBate)
    {
        ReBate = reBate;
    }

    /**
     * @return saleChnl
     */
    public String getSaleChnl()
    {
        return SaleChnl;
    }

    /**
     * @param saleChnl 要设置的 saleChnl
     */
    public void setSaleChnl(final String saleChnl)
    {
        SaleChnl = saleChnl;
    }

    /**
     * @return saleChnlName
     */
    public String getSaleChnlName()
    {
        return SaleChnlName;
    }

    /**
     * @param saleChnlName 要设置的 saleChnlName
     */
    public void setSaleChnlName(final String saleChnlName)
    {
        SaleChnlName = saleChnlName;
    }

    /**
     * @return standPrem
     */
    public String getStandPrem()
    {
        return StandPrem;
    }

    /**
     * @param standPrem 要设置的 standPrem
     */
    public void setStandPrem(final String standPrem)
    {
        StandPrem = standPrem;
    }

    /**
     * @return wrapCode
     */
    public String getWrapCode()
    {
        return WrapCode;
    }

    /**
     * @param wrapCode 要设置的 wrapCode
     */
    public void setWrapCode(final String wrapCode)
    {
        WrapCode = wrapCode;
    }

    /**
     * @return wrapName
     */
    public String getWrapName()
    {
        return WrapName;
    }

    /**
     * @param wrapName 要设置的 wrapName
     */
    public void setWrapName(final String wrapName)
    {
        WrapName = wrapName;
    }
}
