/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保单信息
 * 
 * @author 
 *
 */
public class EdorContInfo extends BaseXmlSch
{
    
    
	/**
	 * 
	 */
	private static final long serialVersionUID = -739695806534515453L;



    /** 保单号*/
    private String ContNo = null;

    /** 保单状态*/
    private String StateFlag = null;

    /** 缴费方式 */
    private String PayMode = null;
    
    /** 开户银行 */
    private String BankCode = null;
    
    /** 银行账号 */
    private String BankAccNo = null;
    
    /** 开户名 */
    private String AccName = null;
    
    
    /** 管理机构名称 */
    private String ManageComName = null;
    
    /** 管理机构传真 */
    private String ServiceFax = null;
     
    /** 客户服务电话 */
    private String ServicePhone = null;
    
    /** 信函服务地址 */
    private String ServicePostAddress = null;
    
    /** 信函服务地址邮编 */
    private String ServicePostZipcode = null;
    

    

    public String getStateFlag() {
		return StateFlag;
	}

	public void setStateFlag(String stateFlag) {
		StateFlag = stateFlag;
	}

	public String getPayMode() {
		return PayMode;
	}

	public void setPayMode(String payMode) {
		PayMode = payMode;
	}


	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getBankAccNo() {
		return BankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}

	public String getAccName() {
		return AccName;
	}

	public void setAccName(String accName) {
		AccName = accName;
	}

	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	
	public String getManageComName() {
		return ManageComName;
	}

	public void setManageComName(String manageComName) {
		ManageComName = manageComName;
	}

	public String getServiceFax() {
		return ServiceFax;
	}

	public void setServiceFax(String serviceFax) {
		ServiceFax = serviceFax;
	}

	public String getServicePhone() {
		return ServicePhone;
	}

	public void setServicePhone(String servicePhone) {
		ServicePhone = servicePhone;
	}

	public String getServicePostAddress() {
		return ServicePostAddress;
	}

	public void setServicePostAddress(String servicePostAddress) {
		ServicePostAddress = servicePostAddress;
	}

	public String getServicePostZipcode() {
		return ServicePostZipcode;
	}

	public void setServicePostZipcode(String servicePostZipcode) {
		ServicePostZipcode = servicePostZipcode;
	}

    

}
