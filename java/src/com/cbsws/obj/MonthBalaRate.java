package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class MonthBalaRate extends BaseXmlSch{
	

	private static final long serialVersionUID = 8570844532770371388L;
	/**
	 * 结算月份
	 */
	private String RateMonth;
	/**
	 * 年化结算利率
	 */
	private String Rate;
	
	public String getRateMonth() {
		return RateMonth;
	}
	public void setRateMonth(String rateMonth) {
		RateMonth = rateMonth;
	}
	public String getRate() {
		return Rate;
	}
	public void setRate(String rate) {
		Rate = rate;
	}
	
}
