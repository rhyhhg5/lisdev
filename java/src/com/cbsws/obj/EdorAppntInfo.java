/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保单信息
 * 
 * @author 
 *
 */
public class EdorAppntInfo extends BaseXmlSch
{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -2621826252873984871L;

	/** 投保人姓名 */
	private String AppntName;
	/** 投保人客户号码 */
	private String AppntNo;
//	/** 投保人证件类型 */
//	private String IdType;
	/** 投保人证件号码 */
	private String IdNo;
	/** 家庭地址 */
	private String HomeAddress;
	/** 家庭邮编 */
	private String HomeZipCode;
	/** 家庭电话 */
	private String HomePhone;
	/** 家庭传真 */
	private String HomeFax;
	/** 单位地址 */
	private String CompanyAddress;
	/** 单位邮编 */
	private String CompanyZipCode;
	/** 单位电话 */
	private String CompanyPhone;
	/** 单位传真 */
	private String CompanyFax;
	/** 通讯地址 */
	private String PostalAddress;
	/** 通讯邮编 */
	private String ZipCode;
	/** 通讯电话 */
	private String Phone;
	/** 通讯传真 */
	private String Fax;
	/** 手机 */
	private String Mobile;
	/** E_mail */
	private String EMail;
	/** 投保人出生日期 */
	private String AppntBirthday;
	/** 投保人性别 */
	private String Sex;


	
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getAppntNo() {
		return AppntNo;
	}
	public void setAppntNo(String appntNo) {
		AppntNo = appntNo;
	}
	public String getIdNo() {
		return IdNo;
	}
	public void setIdNo(String idNo) {
		IdNo = idNo;
	}
	public String getHomeAddress() {
		return HomeAddress;
	}
	public void setHomeAddress(String homeAddress) {
		HomeAddress = homeAddress;
	}
	public String getHomeZipCode() {
		return HomeZipCode;
	}
	public void setHomeZipCode(String homeZipCode) {
		HomeZipCode = homeZipCode;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
	public String getHomeFax() {
		return HomeFax;
	}
	public void setHomeFax(String homeFax) {
		HomeFax = homeFax;
	}
	public String getCompanyAddress() {
		return CompanyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		CompanyAddress = companyAddress;
	}
	public String getCompanyZipCode() {
		return CompanyZipCode;
	}
	public void setCompanyZipCode(String companyZipCode) {
		CompanyZipCode = companyZipCode;
	}
	public String getCompanyPhone() {
		return CompanyPhone;
	}
	public void setCompanyPhone(String companyPhone) {
		CompanyPhone = companyPhone;
	}
	public String getCompanyFax() {
		return CompanyFax;
	}
	public void setCompanyFax(String companyFax) {
		CompanyFax = companyFax;
	}
	public String getPostalAddress() {
		return PostalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		PostalAddress = postalAddress;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getEMail() {
		return EMail;
	}
	public void setEMail(String eMail) {
		EMail = eMail;
	}

	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getAppntBirthday() {
		return AppntBirthday;
	}
	public void setAppntBirthday(String appntBirthday) {
		AppntBirthday = appntBirthday;
	}

}
