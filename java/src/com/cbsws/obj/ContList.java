package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class ContList extends BaseXmlSch{

	private static final long serialVersionUID = -3317261089161067393L;
	
	private String ComCode = null;
	private String ComName = null;
	private String SalChannel = null;
	private String ContNo = null;
	private String RiskCode = null;
	private String RiskName = null;
	private String Amnt = null;
	private String Prem = null;
	private String CValiDate = null;
	private String AppntNo = null;
	private String AppntName = null;
	private String AppntSex = null;
	private String AppntBirthday = null;
	private String AppntIDType = null;
	private String AppntIDNo = null;
	private String AppntMobile = null;
	private String InsuredNo = null;
	private String InsuredNname = null;
	private String InsuredSex = null;
	private String InsuredBirthday = null;
	private String InsuredIDType = null;
	private String InsuredIDNo = null;
	private String InsuredMobile = null;
	private String AgentCode = null;
	private String AgentName = null;
	private String AgentSex = null;
	private String AgentBirthday = null;
	private String AgentIDType = null;
	private String AgentIDNo = null;
	private String AgentMobile = null;
	
	public String getComCode() {
		return ComCode;
	}
	public void setComCode(String comCode) {
		ComCode = comCode;
	}
	public String getComName() {
		return ComName;
	}
	public void setComName(String comName) {
		ComName = comName;
	}
	public String getSalChannel() {
		return SalChannel;
	}
	public void setSalChannel(String salChannel) {
		SalChannel = salChannel;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getRiskCode() {
		return RiskCode;
	}
	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}
	public String getRiskName() {
		return RiskName;
	}
	public void setRiskName(String riskName) {
		RiskName = riskName;
	}
	public String getAmnt() {
		return Amnt;
	}
	public void setAmnt(String amnt) {
		Amnt = amnt;
	}
	public String getPrem() {
		return Prem;
	}
	public void setPrem(String prem) {
		Prem = prem;
	}
	public String getCValiDate() {
		return CValiDate;
	}
	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}
	public String getAppntNo() {
		return AppntNo;
	}
	public void setAppntNo(String appntNo) {
		AppntNo = appntNo;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getAppntSex() {
		return AppntSex;
	}
	public void setAppntSex(String appntSex) {
		AppntSex = appntSex;
	}
	public String getAppntBirthday() {
		return AppntBirthday;
	}
	public void setAppntBirthday(String appntBirthday) {
		AppntBirthday = appntBirthday;
	}
	public String getAppntIDType() {
		return AppntIDType;
	}
	public void setAppntIDType(String appntIDType) {
		AppntIDType = appntIDType;
	}
	public String getAppntIDNo() {
		return AppntIDNo;
	}
	public void setAppntIDNo(String appntIDNo) {
		AppntIDNo = appntIDNo;
	}
	public String getAppntMobile() {
		return AppntMobile;
	}
	public void setAppntMobile(String appntMobile) {
		AppntMobile = appntMobile;
	}
	public String getInsuredNo() {
		return InsuredNo;
	}
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}
	public String getInsuredNname() {
		return InsuredNname;
	}
	public void setInsuredNname(String insuredNname) {
		InsuredNname = insuredNname;
	}
	public String getInsuredSex() {
		return InsuredSex;
	}
	public void setInsuredSex(String insuredSex) {
		InsuredSex = insuredSex;
	}
	public String getInsuredBirthday() {
		return InsuredBirthday;
	}
	public void setInsuredBirthday(String insuredBirthday) {
		InsuredBirthday = insuredBirthday;
	}
	public String getInsuredIDType() {
		return InsuredIDType;
	}
	public void setInsuredIDType(String insuredIDType) {
		InsuredIDType = insuredIDType;
	}
	public String getInsuredIDNo() {
		return InsuredIDNo;
	}
	public void setInsuredIDNo(String insuredIDNo) {
		InsuredIDNo = insuredIDNo;
	}
	public String getInsuredMobile() {
		return InsuredMobile;
	}
	public void setInsuredMobile(String insuredMobile) {
		InsuredMobile = insuredMobile;
	}
	public String getAgentCode() {
		return AgentCode;
	}
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}
	public String getAgentName() {
		return AgentName;
	}
	public void setAgentName(String agentName) {
		AgentName = agentName;
	}
	public String getAgentSex() {
		return AgentSex;
	}
	public void setAgentSex(String agentSex) {
		AgentSex = agentSex;
	}
	public String getAgentBirthday() {
		return AgentBirthday;
	}
	public void setAgentBirthday(String agentBirthday) {
		AgentBirthday = agentBirthday;
	}
	public String getAgentIDType() {
		return AgentIDType;
	}
	public void setAgentIDType(String agentIDType) {
		AgentIDType = agentIDType;
	}
	public String getAgentIDNo() {
		return AgentIDNo;
	}
	public void setAgentIDNo(String agentIDNo) {
		AgentIDNo = agentIDNo;
	}
	public String getAgentMobile() {
		return AgentMobile;
	}
	public void setAgentMobile(String agentMobile) {
		AgentMobile = agentMobile;
	}
}
