package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class ToCancelBudgetMsgBody extends BaseXmlSch{

	private static final long serialVersionUID = 4662594843921435860L;

	/**
	 * 保单号
	 */
	private String ContNo;

	/**
	 * 保全生效日
	 */
	private String EdorValiDate;


	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public String getEdorValiDate() {
		return EdorValiDate;
	}
	public void setEdorValiDate(String edorValiDate) {
		EdorValiDate = edorValiDate;
	}

}
