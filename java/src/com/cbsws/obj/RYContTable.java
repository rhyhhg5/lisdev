/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class RYContTable extends BaseXmlSch
{
	//产品名称、缴费频次、风险保费、万能账户、生效日期、税优识别码、工作单位、团体代码
    private static final long serialVersionUID = -4794467791337930966L;
    private String ContNo;
    private String State;
    private String PolApplyDate;
    private String Prem;
    private String Amnt;
    private String GetPolState;
    private String InsuredName;
    //zxs
    private String RiskName;
    private String Payintv;
    private String Fee;
    private String InsuAccBala;
    private String Cvalidate;
    private String Taxcode;
    private String Grpno;
    private String Grpname;
   
	public String getInsuAccBala() {
		return InsuAccBala;
	}
	public void setInsuAccBala(String insuAccBala) {
		InsuAccBala = insuAccBala;
	}
	
	public String getRiskName() {
		return RiskName;
	}
	public void setRiskName(String riskName) {
		RiskName = riskName;
	}
	public String getPayintv() {
		return Payintv;
	}
	public void setPayintv(String payintv) {
		Payintv = payintv;
	}
	public String getFee() {
		return Fee;
	}
	public void setFee(String fee) {
		Fee = fee;
	}
	public String getCvalidate() {
		return Cvalidate;
	}
	public void setCvalidate(String cvalidate) {
		Cvalidate = cvalidate;
	}
	public String getTaxcode() {
		return Taxcode;
	}
	public void setTaxcode(String taxcode) {
		Taxcode = taxcode;
	}
	public String getGrpno() {
		return Grpno;
	}
	public void setGrpno(String grpno) {
		Grpno = grpno;
	}
	public String getGrpname() {
		return Grpname;
	}
	public void setGrpname(String grpname) {
		Grpname = grpname;
	}
	public String getGetPolState() {
		return GetPolState;
	}
	public void setGetPolState(String getPolState) {
		GetPolState = getPolState;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getPolApplyDate() {
		return PolApplyDate;
	}
	public void setPolApplyDate(String polApplyDate) {
		PolApplyDate = polApplyDate;
	}
	public String getPrem() {
		return Prem;
	}
	public void setPrem(String prem) {
		Prem = prem;
	}
	public String getAmnt() {
		return Amnt;
	}
	public void setAmnt(String amnt) {
		Amnt = amnt;
	}
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
    
    
}
