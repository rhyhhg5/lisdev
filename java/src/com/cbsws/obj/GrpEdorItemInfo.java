/**
 * 2016-10-10
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保全项目信息
 * 
 * @author LC
 *
 */
public class GrpEdorItemInfo extends BaseXmlSch
{
	
	private static final long serialVersionUID = 6724525694985587861L;

	/** 团单号 */
    private String GrpContNo = null;
    
    /** 保全申请日期 */
    private String AcceptDate = null;

    /** 保全项目代码 */
    private String EdorType = null;
    
    /** 保全生效日期 */
    private String EdorValidate =  null;

	public String getGrpContNo() {
		return GrpContNo;
	}

	public void setGrpContNo(String grpContNo) {
		GrpContNo = grpContNo;
	}

	public String getAcceptDate() {
		return AcceptDate;
	}

	public void setAcceptDate(String acceptDate) {
		AcceptDate = acceptDate;
	}

	public String getEdorType() {
		return EdorType;
	}

	public void setEdorType(String edorType) {
		EdorType = edorType;
	}

	public String getEdorValidate() {
		return EdorValidate;
	}

	public void setEdorValidate(String edorValidate) {
		EdorValidate = edorValidate;
	}  

}