package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class WxOtherInfo extends BaseXmlSch{
	private static final long serialVersionUID = -4794467791337930966L;
	
    /** 续保标识 */
    private String XbContFlag = null;
    
    /** 旧保单号 */
    private String XbOldContno = null;

	/**
	 * @return the xbContFlag
	 */
	public String getXbContFlag() {
		return XbContFlag;
	}

	/**
	 * @param xbContFlag the xbContFlag to set
	 */
	public void setXbContFlag(String xbContFlag) {
		XbContFlag = xbContFlag;
	}

	/**
	 * @return the xbOldContno
	 */
	public String getXbOldContno() {
		return XbOldContno;
	}

	/**
	 * @param xbOldContno the xbOldContno to set
	 */
	public void setXbOldContno(String xbOldContno) {
		XbOldContno = xbOldContno;
	}
    
}
