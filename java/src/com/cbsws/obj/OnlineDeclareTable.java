/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class OnlineDeclareTable extends BaseXmlSch
{

	private static final long serialVersionUID = -4794467791337930966L;
    /** 填报内容标签*/
    private String ContentTag = null;

    /** 填报内容 */
    private String ReportedContent = null;

	/**
	 * @return the contentTag
	 */
	public String getContentTag() {
		return ContentTag;
	}

	/**
	 * @param contentTag the contentTag to set
	 */
	public void setContentTag(String contentTag) {
		ContentTag = contentTag;
	}

	/**
	 * @return the reportedContent
	 */
	public String getReportedContent() {
		return ReportedContent;
	}

	/**
	 * @param reportedContent the reportedContent to set
	 */
	public void setReportedContent(String reportedContent) {
		ReportedContent = reportedContent;
	}

    
}
