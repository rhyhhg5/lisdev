package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class EdorGRPLQAmountInfo extends BaseXmlSch{

	/**
	 * 
	 */
	private static final long serialVersionUID = -749506929505680704L;
	
	private String EdorAcceptNo;
	private String Bankcardno;
	private String Bankname;
	private String Status;
	private String Failcause;
	private String Amount;
	
	public String getEdorAcceptNo() {
		return EdorAcceptNo;
	}
	public void setEdorAcceptNo(String edorAcceptNo) {
		EdorAcceptNo = edorAcceptNo;
	}
	public String getBankcardno() {
		return Bankcardno;
	}
	public void setBankcardno(String bankcardno) {
		Bankcardno = bankcardno;
	}
	public String getBankname() {
		return Bankname;
	}
	public void setBankname(String bankname) {
		Bankname = bankname;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getFailcause() {
		return Failcause;
	}
	public void setFailcause(String failcause) {
		Failcause = failcause;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	
}
