package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class RspUniSalescod extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2668686373036424470L;
	
	private String APPID; //系统ID
	private String SID; //序列号
	private String TIMESTAMP ; //请求时间
	private String MESSAGETYPE; //消息类型
	private String ERRCODE;//错误代码
	private String ERRDESC;//错误代码
	private String DATA;//接收成功标志
	private String SALES_NAM;//姓名
	private String MAN_ORG_COD;//管理机构代码
	private String MAN_ORG_NAM;//管理机构名称
	private String IDTYP_COD;//证件类型代码
	private String IDTYP_NAM;//证件类型名称
	private String ID_NO;//证件号码
	private String UNI_SALES_COD;//统一编码
	private String APPROV_DESC;//审批意见
	public String getAPPID() {
		return APPID;
	}
	public void setAPPID(String aPPID) {
		APPID = aPPID;
	}
	public String getSID() {
		return SID;
	}
	public void setSID(String sID) {
		SID = sID;
	}
	public String getTIMESTAMP() {
		return TIMESTAMP;
	}
	public void setTIMESTAMP(String tIMESTAMP) {
		TIMESTAMP = tIMESTAMP;
	}
	public String getMESSAGETYPE() {
		return MESSAGETYPE;
	}
	public void setMESSAGETYPE(String mESSAGETYPE) {
		MESSAGETYPE = mESSAGETYPE;
	}
	public String getERRCODE() {
		return ERRCODE;
	}
	public void setERRCODE(String eRRCODE) {
		ERRCODE = eRRCODE;
	}
	public String getERRDESC() {
		return ERRDESC;
	}
	public void setERRDESC(String eRRDESC) {
		ERRDESC = eRRDESC;
	}
	public String isDATA() {
		return DATA;
	}
	public void setDATA(String dATA) {
		DATA = dATA;
	}
	public String getAPPROV_DESC() {
		return APPROV_DESC;
	}
	public void setAPPROV_DESC(String approv_desc) {
		APPROV_DESC = approv_desc;
	}
	public String getID_NO() {
		return ID_NO;
	}
	public void setID_NO(String id_no) {
		ID_NO = id_no;
	}
	public String getIDTYP_COD() {
		return IDTYP_COD;
	}
	public void setIDTYP_COD(String idtyp_cod) {
		IDTYP_COD = idtyp_cod;
	}
	public String getIDTYP_NAM() {
		return IDTYP_NAM;
	}
	public void setIDTYP_NAM(String idtyp_nam) {
		IDTYP_NAM = idtyp_nam;
	}
	public String getMAN_ORG_COD() {
		return MAN_ORG_COD;
	}
	public void setMAN_ORG_COD(String man_org_cod) {
		MAN_ORG_COD = man_org_cod;
	}
	public String getMAN_ORG_NAM() {
		return MAN_ORG_NAM;
	}
	public void setMAN_ORG_NAM(String man_org_nam) {
		MAN_ORG_NAM = man_org_nam;
	}
	public String getSALES_NAM() {
		return SALES_NAM;
	}
	public void setSALES_NAM(String sales_nam) {
		SALES_NAM = sales_nam;
	}
	public String getUNI_SALES_COD() {
		return UNI_SALES_COD;
	}
	public void setUNI_SALES_COD(String uni_sales_cod) {
		UNI_SALES_COD = uni_sales_cod;
	}
	

}
