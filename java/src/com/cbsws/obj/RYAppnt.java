/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class RYAppnt extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String AppntName;
    private String AppntSex;
    private String AppntIdType;
    private String AppntIdNo;
    private String AppntBirthday;
    private String AppntMarryState;
    private String AppntNativePlace;
    private String AppntOccuPation;
    private String AppntMobile;
    private String AppntCompanyPhone;
    private String AppntHomePhone;
    private String AppntAddress;
    private String AppntZipCode;
    private String AppntEmail;
    private String AppntPhone;
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getAppntSex() {
		return AppntSex;
	}
	public void setAppntSex(String appntSex) {
		AppntSex = appntSex;
	}
	public String getAppntIdType() {
		return AppntIdType;
	}
	public void setAppntIdType(String appntIdType) {
		AppntIdType = appntIdType;
	}
	public String getAppntIdNo() {
		return AppntIdNo;
	}
	public void setAppntIdNo(String appntIdNo) {
		AppntIdNo = appntIdNo;
	}
	public String getAppntBirthday() {
		return AppntBirthday;
	}
	public void setAppntBirthday(String appntBirthday) {
		AppntBirthday = appntBirthday;
	}
	public String getAppntMarryState() {
		return AppntMarryState;
	}
	public void setAppntMarryState(String appntMarryState) {
		AppntMarryState = appntMarryState;
	}
	public String getAppntNativePlace() {
		return AppntNativePlace;
	}
	public void setAppntNativePlace(String appntNativePlace) {
		AppntNativePlace = appntNativePlace;
	}
	public String getAppntOccuPation() {
		return AppntOccuPation;
	}
	public void setAppntOccuPation(String appntOccuPation) {
		AppntOccuPation = appntOccuPation;
	}
	public String getAppntMobile() {
		return AppntMobile;
	}
	public void setAppntMobile(String appntMobile) {
		AppntMobile = appntMobile;
	}
	public String getAppntCompanyPhone() {
		return AppntCompanyPhone;
	}
	public void setAppntCompanyPhone(String appntCompanyPhone) {
		AppntCompanyPhone = appntCompanyPhone;
	}
	public String getAppntHomePhone() {
		return AppntHomePhone;
	}
	public void setAppntHomePhone(String appntHomePhone) {
		AppntHomePhone = appntHomePhone;
	}
	public String getAppntAddress() {
		return AppntAddress;
	}
	public void setAppntAddress(String appntAddress) {
		AppntAddress = appntAddress;
	}
	public String getAppntZipCode() {
		return AppntZipCode;
	}
	public void setAppntZipCode(String appntZipCode) {
		AppntZipCode = appntZipCode;
	}
	public String getAppntEmail() {
		return AppntEmail;
	}
	public void setAppntEmail(String appntEmail) {
		AppntEmail = appntEmail;
	}
	public String getAppntPhone() {
		return AppntPhone;
	}
	public void setAppntPhone(String appntPhone) {
		AppntPhone = appntPhone;
	}
    
    
    
    
    
    
    
    
	
    
    
    
}
