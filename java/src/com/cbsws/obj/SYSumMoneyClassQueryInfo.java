package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SYSumMoneyClassQueryInfo extends BaseXmlSch{

	private static final long serialVersionUID = -7972694669848699386L;
	
	/**
	 * 查询类型（1为累计缴费金额，2为累计结算利息，3为累计差额返还）
	 */
	private String QueryType;
	/**
	 * 保单号
	 */
	private String ContNo;
	public String getQueryType() {
		return QueryType;
	}
	public void setQueryType(String queryType) {
		QueryType = queryType;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

}
