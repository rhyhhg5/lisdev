/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class LCPolTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 管理机构 */
    private String ManageCom = null;

    /** 保险生效日期 */
    private String CValiDate = null;
    
    /** 保险类型 */
    private String ContType = null;
    
    /** 险种编码 */
    private String RiskCode = null;
    
    /** 保额 */
    private String Amnt = null;
    
    /** 保费 */
    private String Prem = null;
    
    /** 被保人年龄 */
    private String InsuredAppAge = null;
    
    /** 被保人性别 */
    private String InsuredSex = null;
    
    /** 保障期间 */
    private String InsuYear = null;
    
    /** 保障期间标识 */
    private String InsuYearFlag = null;
    
    /** 缴费期间 */
    private String PayEndYear = null;
    
    /** 缴费期间标识 */
    private String PayEndYearFlag = null;
    
    /** 缴费频次 */
    private String PayIntv = null;
    
    /** 份数 */
    private String Copys = null;
    
    /** 档次 */
    private String Mult = null;

	public String getAmnt() {
		return Amnt;
	}

	public void setAmnt(String amnt) {
		Amnt = amnt;
	}

	public String getContType() {
		return ContType;
	}

	public void setContType(String contType) {
		ContType = contType;
	}

	public String getCopys() {
		return Copys;
	}

	public void setCopys(String copys) {
		Copys = copys;
	}

	public String getCValiDate() {
		return CValiDate;
	}

	public void setCValiDate(String valiDate) {
		CValiDate = valiDate;
	}

	public String getInsuredAppAge() {
		return InsuredAppAge;
	}

	public void setInsuredAppAge(String insuredAppAge) {
		InsuredAppAge = insuredAppAge;
	}

	public String getInsuredSex() {
		return InsuredSex;
	}

	public void setInsuredSex(String insuredSex) {
		InsuredSex = insuredSex;
	}

	public String getInsuYear() {
		return InsuYear;
	}

	public void setInsuYear(String insuYear) {
		InsuYear = insuYear;
	}

	public String getInsuYearFlag() {
		return InsuYearFlag;
	}

	public void setInsuYearFlag(String insuYearFlag) {
		InsuYearFlag = insuYearFlag;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getMult() {
		return Mult;
	}

	public void setMult(String mult) {
		Mult = mult;
	}

	public String getPayEndYear() {
		return PayEndYear;
	}

	public void setPayEndYear(String payEndYear) {
		PayEndYear = payEndYear;
	}

	public String getPayEndYearFlag() {
		return PayEndYearFlag;
	}

	public void setPayEndYearFlag(String payEndYearFlag) {
		PayEndYearFlag = payEndYearFlag;
	}

	public String getPayIntv() {
		return PayIntv;
	}

	public void setPayIntv(String payIntv) {
		PayIntv = payIntv;
	}

	public String getPrem() {
		return Prem;
	}

	public void setPrem(String prem) {
		Prem = prem;
	}

	public String getRiskCode() {
		return RiskCode;
	}

	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}
    
}
