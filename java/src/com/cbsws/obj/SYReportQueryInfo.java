package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SYReportQueryInfo extends BaseXmlSch {


	private static final long serialVersionUID = 1073716441995111080L;
	
	/**
	 * 报告类型（0为季报，1为年报）
	 */
	private String ReportType;
	/**
	 * 保单号
	 */
	private String ContNo;
	/**
	 * 保单年度（年报时必填）
	 */
	private String PolYear;
	/**
	 * 结算年度
	 */
	private String BalaYear;
	/**
	 * 结算季度
	 */
	private String BalaQuarter;
	
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getBalaYear() {
		return BalaYear;
	}
	public void setBalaYear(String balaYear) {
		BalaYear = balaYear;
	}
	public String getBalaQuarter() {
		return BalaQuarter;
	}
	public void setBalaQuarter(String balaQuarter) {
		BalaQuarter = balaQuarter;
	}
	public String getReportType() {
		return ReportType;
	}
	public void setReportType(String reportType) {
		ReportType = reportType;
	}
	public String getPolYear() {
		return PolYear;
	}
	public void setPolYear(String polYear) {
		PolYear = polYear;
	}

}
