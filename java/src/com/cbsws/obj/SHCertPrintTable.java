package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SHCertPrintTable extends BaseXmlSch{

	private static final long serialVersionUID = -7305678040867973031L;

	/**  卡号 */
	private String CardNo=null;
	
	/** 管理机构 */
	private String ManageCom=null;
	
	/** 保险卡类型 */
	private String CertifyCode=null;
	
	/** 保险卡名称 */
	private String CertifyName=null;
	
	/** 套餐代码 */
	private String WrapCode =null;
	
	/** 套餐名称 */
	private String WrapName=null;
	
	/** 打印模板 */
	private String PrtTemplet=null;
	
	private String SpecialClause=null;
	
	private String Sign=null;

	public String getCardNo() {
		return CardNo;
	}

	public void setCardNo(final String cardNo) {
		CardNo = cardNo;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(final String manageCom) {
		ManageCom = manageCom;
	}

	public String getCertifyCode() {
		return CertifyCode;
	}

	public void setCertifyCode(final String certifyCode) {
		CertifyCode = certifyCode;
	}

	public String getCertifyName() {
		return CertifyName;
	}

	public void setCertifyName(final String certifyName) {
		CertifyName = certifyName;
	}

	public String getWrapCode() {
		return WrapCode;
	}

	public void setWrapCode(final String wrapCode) {
		WrapCode = wrapCode;
	}

	public String getWrapName() {
		return WrapName;
	}

	public void setWrapName(final String wrapName) {
		WrapName = wrapName;
	}

	public String getPrtTemplet() {
		return PrtTemplet;
	}

	public void setPrtTemplet(final String prtTemplet) {
		PrtTemplet = prtTemplet;
	}

	public String getSpecialClause() {
		return SpecialClause;
	}

	public void setSpecialClause(String specialClause) {
		SpecialClause = specialClause;
	}
	
	public String getSign() {
		return Sign;
	}

	public void setSign(String sign) {
		Sign = sign;
	}

}