package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class BackParamInfo extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String BackMoney = "";
	
	private String ActuGetno = "";

	public String getBackMoney() {
		return BackMoney;
	}

	public void setBackMoney(String backMoney) {
		BackMoney = backMoney;
	}

	public String getActuGetno() {
		return ActuGetno;
	}

	public void setActuGetno(String actuGetno) {
		ActuGetno = actuGetno;
	}
	
	
}
