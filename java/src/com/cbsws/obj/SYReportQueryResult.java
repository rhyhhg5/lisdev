package com.cbsws.obj;


import java.util.List;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SYReportQueryResult extends BaseXmlSch {


	private static final long serialVersionUID = -4855764880887284617L;
	
	/**
	 * 报告类型（0为季报，1为年报）
	 */
	private String ReportType;
	/**
	 * 投保人姓名
	 */
	private String AppntName;
	/**
	 * 邮政编码
	 */
	private String ZipCode;
	/**
	 * 通讯地址
	 */
	private String Address;
	/**
	 * 报告起始日期
	 */
	private String StartDate;
	/**
	 * 投保人姓名
	 */
	private String EndDate;
	/**
	 * 客户姓名
	 */
	private String CustomerName;
	/**
	 * 产品名称
	 */
	private String RiskCodeName;
	/**
	 * 保单号
	 */
	private String ContNo;
	/**
	 * 保单状态
	 */
	private String ContState;
	/**
	 * 被保人姓名
	 */
	private String InsuredName;
	/**
	 * 保单生效日期
	 */
	private String CValiDate;
	/**
	 * 已缴费次数
	 */
	private String TotalCount;
	/**
	 * 已缴费金额
	 */
	private String TotalPrem;
	/**
	 * 公司偿付能力截止年度
	 */
	private String Year1;
	/**
	 * 公司偿付能力截止月度
	 */
	private String Quarter1;
	/**
	 * 公司偿付能力充足率
	 */
	private String Solvency;
	/**
	 * 是否达到监管要求
	 */
	private String Flag;
	/**
	 * 公司风险综合评估截止年度
	 */
	private String Year2;
	/**
	 * 公司风险综合评估截止月度
	 */
	private String Quarter2;
	/**
	 * 公司风险综合评级
	 */
	private String RiskRate;

	public String getReportType() {
		return ReportType;
	}
	public void setReportType(String reportType) {
		ReportType = reportType;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getEndDate() {
		return EndDate;
	}
	public void setEndDate(String endDate) {
		EndDate = endDate;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getRiskCodeName() {
		return RiskCodeName;
	}
	public void setRiskCodeName(String riskCodeName) {
		RiskCodeName = riskCodeName;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getContState() {
		return ContState;
	}
	public void setContState(String contState) {
		ContState = contState;
	}
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
	public String getCValiDate() {
		return CValiDate;
	}
	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}
	public String getTotalCount() {
		return TotalCount;
	}
	public void setTotalCount(String totalCount) {
		TotalCount = totalCount;
	}
	public String getTotalPrem() {
		return TotalPrem;
	}
	public void setTotalPrem(String totalPrem) {
		TotalPrem = totalPrem;
	}
	public String getYear1() {
		return Year1;
	}
	public void setYear1(String year1) {
		Year1 = year1;
	}
	public String getQuarter1() {
		return Quarter1;
	}
	public void setQuarter1(String quarter1) {
		Quarter1 = quarter1;
	}
	public String getSolvency() {
		return Solvency;
	}
	public void setSolvency(String solvency) {
		Solvency = solvency;
	}
	public String getFlag() {
		return Flag;
	}
	public void setFlag(String flag) {
		Flag = flag;
	}
	public String getYear2() {
		return Year2;
	}
	public void setYear2(String year2) {
		Year2 = year2;
	}
	public String getQuarter2() {
		return Quarter2;
	}
	public void setQuarter2(String quarter2) {
		Quarter2 = quarter2;
	}
	public String getRiskRate() {
		return RiskRate;
	}
	public void setRiskRate(String riskRate) {
		RiskRate = riskRate;
	}


	
}
