package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class ContInfo extends BaseXmlSch{
private static final long serialVersionUID = -6800213917931221037L;
	
	/** 管理机构 */
	private String ManageCom = null;
	/** 生效日期起期 */
	private String CvaliDateStart = null;
	/** 生效日期止期 */
	private String CvaliDateEnd = null;
	/** 险种编码 */
	private String RiskCode = null;
	
	public String getManageCom() {
		return ManageCom;
	}
	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}
	public String getCvaliDateStart() {
		return CvaliDateStart;
	}
	public void setCvaliDateStart(String cvaliDateStart) {
		CvaliDateStart = cvaliDateStart;
	}
	public String getCvaliDateEnd() {
		return CvaliDateEnd;
	}
	public void setCvaliDateEnd(String cvaliDateEnd) {
		CvaliDateEnd = cvaliDateEnd;
	}
	public String getRiskCode() {
		return RiskCode;
	}
	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}
}
