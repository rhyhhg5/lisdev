package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class LPCashPrintTable extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3341771580427631424L;
	/**
	 * �ֺ��ӡ�����ֶ�
	 */
	private String ManageComLength4;
	private String Operator;
	private String ZipCode;
	private String Address;
	private String AppntName;
	private String Year1;
	private String Quarter1;
	private String Solvency;
	private String Flag;
	private String Year2;
	private String Quarter2;
	private String RiksRate;
	private String ContNo;
	private String RiskName;
	private String CValiDate;
	private String InsuredName;
	private String PayMode;
	private String SumPolMoney;
	private String ShouldDate;
	private String PolYear;
	private String ActuDate;
	private String BonusMoney;
	private String BeforeBonusMoney;
	private String AfterBonusMoney;
	private String HLYear;
	private String HLAllSum;
	private String HLAppntSum;
	private String InterestRate;
	private String AgentName;
	private String AgentCode;
	private String AgentPhone;
	private String AgentGroup;
	private String ServicePhone;
	private String letterservicename;
	private String ServiceAddress;
	private String ServiceZip;
	private String ComName;
	private String PrintDate;
	private String Fax;
	
	public String getManageComLength4() {
		return ManageComLength4;
	}
	public void setManageComLength4(String manageComLength4) {
		ManageComLength4 = manageComLength4;
	}
	public String getOperator() {
		return Operator;
	}
	public void setOperator(String operator) {
		Operator = operator;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getYear1() {
		return Year1;
	}
	public void setYear1(String year1) {
		Year1 = year1;
	}
	public String getQuarter1() {
		return Quarter1;
	}
	public void setQuarter1(String quarter1) {
		Quarter1 = quarter1;
	}
	public String getSolvency() {
		return Solvency;
	}
	public void setSolvency(String solvency) {
		Solvency = solvency;
	}
	public String getFlag() {
		return Flag;
	}
	public void setFlag(String flag) {
		Flag = flag;
	}
	public String getYear2() {
		return Year2;
	}
	public void setYear2(String year2) {
		Year2 = year2;
	}
	public String getQuarter2() {
		return Quarter2;
	}
	public void setQuarter2(String quarter2) {
		Quarter2 = quarter2;
	}
	public String getRiksRate() {
		return RiksRate;
	}
	public void setRiksRate(String riksRate) {
		RiksRate = riksRate;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getRiskName() {
		return RiskName;
	}
	public void setRiskName(String riskName) {
		RiskName = riskName;
	}
	public String getCValiDate() {
		return CValiDate;
	}
	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
	public String getPayMode() {
		return PayMode;
	}
	public void setPayMode(String payMode) {
		PayMode = payMode;
	}
	public String getSumPolMoney() {
		return SumPolMoney;
	}
	public void setSumPolMoney(String sumPolMoney) {
		SumPolMoney = sumPolMoney;
	}
	public String getShouldDate() {
		return ShouldDate;
	}
	public void setShouldDate(String shouldDate) {
		ShouldDate = shouldDate;
	}
	public String getPolYear() {
		return PolYear;
	}
	public void setPolYear(String polYear) {
		PolYear = polYear;
	}
	public String getActuDate() {
		return ActuDate;
	}
	public void setActuDate(String actuDate) {
		ActuDate = actuDate;
	}
	public String getBeforeBonusMoney() {
		return BeforeBonusMoney;
	}
	public void setBeforeBonusMoney(String beforeBonusMoney) {
		BeforeBonusMoney = beforeBonusMoney;
	}
	public String getAfterBonusMoney() {
		return AfterBonusMoney;
	}
	public void setAfterBonusMoney(String afterBonusMoney) {
		AfterBonusMoney = afterBonusMoney;
	}
	public String getHLYear() {
		return HLYear;
	}
	public void setHLYear(String hLYear) {
		HLYear = hLYear;
	}
	public String getHLAllSum() {
		return HLAllSum;
	}
	public void setHLAllSum(String hLAllSum) {
		HLAllSum = hLAllSum;
	}
	public String getHLAppntSum() {
		return HLAppntSum;
	}
	public void setHLAppntSum(String hLAppntSum) {
		HLAppntSum = hLAppntSum;
	}
	public String getInterestRate() {
		return InterestRate;
	}
	public void setInterestRate(String interestRate) {
		InterestRate = interestRate;
	}
	public String getAgentName() {
		return AgentName;
	}
	public void setAgentName(String agentName) {
		AgentName = agentName;
	}
	public String getAgentCode() {
		return AgentCode;
	}
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}
	public String getAgentPhone() {
		return AgentPhone;
	}
	public void setAgentPhone(String agentPhone) {
		AgentPhone = agentPhone;
	}
	public String getAgentGroup() {
		return AgentGroup;
	}
	public void setAgentGroup(String agentGroup) {
		AgentGroup = agentGroup;
	}
	public String getServicePhone() {
		return ServicePhone;
	}
	public void setServicePhone(String servicePhone) {
		ServicePhone = servicePhone;
	}
	public String getLetterservicename() {
		return letterservicename;
	}
	public void setLetterservicename(String letterservicename) {
		this.letterservicename = letterservicename;
	}
	public String getServiceAddress() {
		return ServiceAddress;
	}
	public void setServiceAddress(String serviceAddress) {
		ServiceAddress = serviceAddress;
	}
	public String getServiceZip() {
		return ServiceZip;
	}
	public void setServiceZip(String serviceZip) {
		ServiceZip = serviceZip;
	}
	public String getComName() {
		return ComName;
	}
	public void setComName(String comName) {
		ComName = comName;
	}
	public String getPrintDate() {
		return PrintDate;
	}
	public void setPrintDate(String printDate) {
		PrintDate = printDate;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	/**
	 * @return the bonusMoney
	 */
	public String getBonusMoney() {
		return BonusMoney;
	}
	/**
	 * @param bonusMoney the bonusMoney to set
	 */
	public void setBonusMoney(String bonusMoney) {
		BonusMoney = bonusMoney;
	}
	
	
}
