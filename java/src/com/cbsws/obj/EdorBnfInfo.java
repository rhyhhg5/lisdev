/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保单信息
 * 
 * @author 
 *
 */
public class EdorBnfInfo extends BaseXmlSch
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7786159652198389276L;
	
	
	
	/** 受益人人姓名*/
    private String BnfName = null;
    
	/** 所属被保人客户号*/
    private String InsuredNo = null;
    
	/** 险种代码*/
    private String RiskCode = null;

	public String getBnfName() {
		return BnfName;
	}
	public void setBnfName(String bnfName) {
		BnfName = bnfName;
	}
	public String getInsuredNo() {
		return InsuredNo;
	}

	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}

	public String getRiskCode() {
		return RiskCode;
	}

	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}




}
