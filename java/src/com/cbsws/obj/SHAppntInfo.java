package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SHAppntInfo extends BaseXmlSch{
	
	private static final long serialVersionUID = -4794467791337930966L;
	private String AppntName;
	private String IDType;
	private String IDNo;
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getIDType() {
		return IDType;
	}
	public void setIDType(String iDType) {
		IDType = iDType;
	}
	public String getIDNo() {
		return IDNo;
	}
	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}
	
	
	
	
	
	
	
}
