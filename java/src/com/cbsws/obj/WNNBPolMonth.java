package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;


public class WNNBPolMonth extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7581368399319409654L;
	  private String Month;
	  private String StartMoney;
	  private String BQAddPrem;
	  private String BQAddPremFee;
//	  private String NewPrem;
//	  private String NewPremFee;
	  private String LXMoney;
	  private String RPMoney;
	  private String MFMoney;
	  private String LQMoney;
	  private String EndMoney;
	public String getMonth() {
		return Month;
	}
	public void setMonth(String month) {
		Month = month;
	}
	public String getStartMoney() {
		return StartMoney;
	}
	public void setStartMoney(String startMoney) {
		StartMoney = startMoney;
	}
	public String getBQAddPrem() {
		return BQAddPrem;
	}
	public void setBQAddPrem(String bQAddPrem) {
		BQAddPrem = bQAddPrem;
	}
	public String getBQAddPremFee() {
		return BQAddPremFee;
	}
	public void setBQAddPremFee(String bQAddPremFee) {
		BQAddPremFee = bQAddPremFee;
	}
//	public String getNewPrem() {
//		return NewPrem;
//	}
//	public void setNewPrem(String newPrem) {
//		NewPrem = newPrem;
//	}
//	public String getNewPremFee() {
//		return NewPremFee;
//	}
//	public void setNewPremFee(String newPremFee) {
//		NewPremFee = newPremFee;
//	}
	public String getLXMoney() {
		return LXMoney;
	}
	public void setLXMoney(String lXMoney) {
		LXMoney = lXMoney;
	}
	public String getRPMoney() {
		return RPMoney;
	}
	public void setRPMoney(String rPMoney) {
		RPMoney = rPMoney;
	}
	public String getMFMoney() {
		return MFMoney;
	}
	public void setMFMoney(String mFMoney) {
		MFMoney = mFMoney;
	}
	public String getLQMoney() {
		return LQMoney;
	}
	public void setLQMoney(String lQMoney) {
		LQMoney = lQMoney;
	}
	public String getEndMoney() {
		return EndMoney;
	}
	public void setEndMoney(String endMoney) {
		EndMoney = endMoney;
	}

}
