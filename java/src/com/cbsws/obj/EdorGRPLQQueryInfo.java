package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class EdorGRPLQQueryInfo extends BaseXmlSch{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4237166904350803197L;
	
	private String Edoracceptno;

	public String getEdoracceptno() {
		return Edoracceptno;
	}

	public void setEdoracceptno(String edoracceptno) {
		Edoracceptno = edoracceptno;
	}

	
}
