package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class PayInfo extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3550592198669839408L;

	/** 付费方式*/
	private String PayMode = "";
	
	/** 转账银行*/
	private String BankCode = "";
	
	/** 银行账号*/
	private String AccNo = "";
	
	/** 账户名*/
	private String AccName = "";
	
	/** 付费日期*/
	private String PayDate = "";
	
	/** 付费通知书号*/
	private String ActuGetno = "";

	public String getPayMode() {
		return PayMode;
	}

	public void setPayMode(String payMode) {
		PayMode = payMode;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getAccNo() {
		return AccNo;
	}

	public void setAccNo(String accNo) {
		AccNo = accNo;
	}

	public String getAccName() {
		return AccName;
	}

	public void setAccName(String accName) {
		AccName = accName;
	}

	public String getPayDate() {
		return PayDate;
	}

	public void setPayDate(String payDate) {
		PayDate = payDate;
	}

	public String getActuGetno() {
		return ActuGetno;
	}

	public void setActuGetno(String actuGetno) {
		ActuGetno = actuGetno;
	}
	
}
