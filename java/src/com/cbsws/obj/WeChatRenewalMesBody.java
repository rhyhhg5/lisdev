package com.cbsws.obj;

import java.io.Serializable;

public class WeChatRenewalMesBody implements Serializable{

	private static final long serialVersionUID = -5484888699811000805L;
	private String name;          
	private String sex;           
	private String idType;        
	private String idNo;          
	private String birthday;      
	private String phone;         
	private String contno;        
	private String payToDate;     
	private String messageContent;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getContno() {
		return contno;
	}
	public void setContno(String contno) {
		this.contno = contno;
	}
	public String getPayToDate() {
		return payToDate;
	}
	public void setPayToDate(String payToDate) {
		this.payToDate = payToDate;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
}
