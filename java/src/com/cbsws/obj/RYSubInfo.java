/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class RYSubInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String TaxFlag; //税优投保标识     1个人 2团体
    private String TaxNo;	//个人税务登记号
    private String transFlag;//转移保单标识  0新单 1转入
    private String TaxPayerType;	//个税征收方式 01代扣代缴 02自行申报
    private String TaxCode;	//税优识别码
    private String ErrorInfo; //错误信息
    private String GrpNo;	//团体编号
    private String CreditCode;	//个人社会信用代码
    private String GTaxNo;		//单位税务登记证代码
    private String GOrgancomCode ;	//单位社会信用代码 
    private String PremMult;  //被保险人风险保险费档次 1 未参加补充医疗保险，2.已参加补充医疗保险
	public String getTaxFlag() {
		return TaxFlag;
	}
	public String getTransFlag() {
		return transFlag;
	}
	public void setTransFlag(String transFlag) {
		this.transFlag = transFlag;
	}
	public void setTaxFlag(String taxFlag) {
		TaxFlag = taxFlag;
	}
	public String getTaxNo() {
		return TaxNo;
	}
	public void setTaxNo(String taxNo) {
		TaxNo = taxNo;
	}
	public String getTaxPayerType() {
		return TaxPayerType;
	}
	public void setTaxPayerType(String taxPayerType) {
		TaxPayerType = taxPayerType;
	}
	public String getTaxCode() {
		return TaxCode;
	}
	public void setTaxCode(String taxCode) {
		TaxCode = taxCode;
	}
	public String getErrorInfo() {
		return ErrorInfo;
	}
	public void setErrorInfo(String errorInfo) {
		ErrorInfo = errorInfo;
	}
	public String getGrpNo() {
		return GrpNo;
	}
	public void setGrpNo(String grpNo) {
		GrpNo = grpNo;
	}
	public String getCreditCode() {
		return CreditCode;
	}
	public void setCreditCode(String creditCode) {
		CreditCode = creditCode;
	}
	public String getGTaxNo() {
		return GTaxNo;
	}
	public void setGTaxNo(String gTaxNo) {
		GTaxNo = gTaxNo;
	}
	public String getGOrgancomCode() {
		return GOrgancomCode;
	}
	public void setGOrgancomCode(String gOrgancomCode) {
		GOrgancomCode = gOrgancomCode;
	}
	public String getPremMult() {
		return PremMult;
	}
	public void setPremMult(String premMult) {
		PremMult = premMult;
	}
	
    
    
    
}
