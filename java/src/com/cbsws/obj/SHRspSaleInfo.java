package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SHRspSaleInfo extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4122190489085763023L;

	/**
	 * 系统ID
	 */
	private String APPID;
	/**
	 * 序列号
	 */
	private String SID;
	/**
	 * 反馈时间
	 */
	private String TIMESTAMP;
	/**
	 * 消息类型
	 */
	private String MESSAGETYPE;
	/**
	 * 统一编码
	 */
	private String UNI_SALES_COD;
	/**
	 * 姓名
	 */
	private String SALES_NAM;
	/**
	 * 管理机构代码
	 */
	private String MAN_ORG_COD;
	/**
	 * 管理机构名称
	 */
	private String MAN_ORG_NAM;
	/**
	 * 证件类型代码
	 */
	private String IDTYP_COD;
	/**
	 * 证件类型名称
	 */
	private String IDTYP_NAM;
	/**
	 * 证件号码
	 */
	private String ID_NO;
	/**
	 * 错误代码
	 */
	private String ERRCODE;
	/**
	 * 错误描述
	 */
	private String ERRDESC;
	
	public String getAPPID() {
		return APPID;
	}
	public void setAPPID(String aPPID) {
		APPID = aPPID;
	}
	public String getSID() {
		return SID;
	}
	public void setSID(String sID) {
		SID = sID;
	}
	public String getTIMESTAMP() {
		return TIMESTAMP;
	}
	public void setTIMESTAMP(String tIMESTAMP) {
		TIMESTAMP = tIMESTAMP;
	}
	public String getMESSAGETYPE() {
		return MESSAGETYPE;
	}
	public void setMESSAGETYPE(String mESSAGETYPE) {
		MESSAGETYPE = mESSAGETYPE;
	}
	public String getERRCODE() {
		return ERRCODE;
	}
	public void setERRCODE(String eRRCODE) {
		ERRCODE = eRRCODE;
	}
	public String getERRDESC() {
		return ERRDESC;
	}
	public void setERRDESC(String eRRDESC) {
		ERRDESC = eRRDESC;
	}
	public String getUNI_SALES_COD() {
		return UNI_SALES_COD;
	}
	public void setUNI_SALES_COD(String uNISALESCOD) {
		UNI_SALES_COD = uNISALESCOD;
	}
	public String getSALES_NAM() {
		return SALES_NAM;
	}
	public void setSALES_NAM(String sALESNAM) {
		SALES_NAM = sALESNAM;
	}
	public String getMAN_ORG_COD() {
		return MAN_ORG_COD;
	}
	public void setMAN_ORG_COD(String mANORGCOD) {
		MAN_ORG_COD = mANORGCOD;
	}
	public String getMAN_ORG_NAM() {
		return MAN_ORG_NAM;
	}
	public void setMAN_ORG_NAM(String mANORGNAM) {
		MAN_ORG_NAM = mANORGNAM;
	}
	public String getIDTYP_COD() {
		return IDTYP_COD;
	}
	public void setIDTYP_COD(String iDTYPCOD) {
		IDTYP_COD = iDTYPCOD;
	}
	public String getIDTYP_NAM() {
		return IDTYP_NAM;
	}
	public void setIDTYP_NAM(String iDTYPNAM) {
		IDTYP_NAM = iDTYPNAM;
	}
	public String getID_NO() {
		return ID_NO;
	}
	public void setID_NO(String iDNO) {
		ID_NO = iDNO;
	}
	
}
