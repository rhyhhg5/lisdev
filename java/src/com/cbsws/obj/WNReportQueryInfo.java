package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class WNReportQueryInfo extends BaseXmlSch  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6893713255180143199L;

	/* 保单号 */
	private String ContNo;
	/* 保单年度（年报时必填） */
	private String PolYear;
	/* 结算年度*/
	private String BalaYear;
	
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getPolYear() {
		return PolYear;
	}
	public void setPolYear(String polYear) {
		PolYear = polYear;
	}
	public String getBalaYear() {
		return BalaYear;
	}
	public void setBalaYear(String balaYear) {
		BalaYear = balaYear;
	}

}
