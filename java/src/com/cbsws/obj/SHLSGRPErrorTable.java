package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SHLSGRPErrorTable extends BaseXmlSch{
	
	 private static final long serialVersionUID = -4794467791337930966L;
	 
	 /** ������ */
		private String GrpNo;
		/** ״̬ */
		private String State;
		/** ������� */
		private String ErrCode;
		/** ������Ϣ */
		private String ErrInfo;
		
		public String getGrpNo() {
			return GrpNo;
		}
		public void setGrpNo(String grpNo) {
			GrpNo = grpNo;
		}
		public String getState() {
			return State;
		}
		public void setState(String state) {
			State = state;
		}
		public String getErrCode() {
			return ErrCode;
		}
		public void setErrCode(String errCode) {
			ErrCode = errCode;
		}
		public String getErrInfo() {
			return ErrInfo;
		}
		public void setErrInfo(String errInfo) {
			ErrInfo = errInfo;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
	 
}
