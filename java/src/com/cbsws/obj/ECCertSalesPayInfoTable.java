/**
 * 2010-8-25
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public class ECCertSalesPayInfoTable extends BaseXmlSch
{
    private static final long serialVersionUID = 5764758790906961252L;

    /** 交易号 */
    private String PayTradeSeq = null;

    /** 管理机构 */
    private String ManageCom = null;

    /** 销售渠道 */
    private String SaleChnl = null;

    /** 销售渠道名称 */
    private String SaleChnlName = null;

    /** 中介机构 */
    private String AgentCom = null;

    /** 业务员代码 */
    private String AgentCode = null;

    /** 业务员姓名 */
    private String AgentName = null;

    /** 保险卡类型 */
    private String CertifyCode = null;

    /** 保险卡名称 */
    private String CertifyName = null;

    /** 套餐代码 */
    private String WrapCode = null;

    /** 套餐名称 */
    private String WrapName = null;

    /** 销售数量 */
    private String SalesCount = null;

    /** 销售时间 */
    private String SalesDate = null;

    /** 缴费方式 */
    private String PayMode = null;

    /** 缴费支付平台类型 */
    private String PayChnlType = null;

    /** 缴费支付平台名称 */
    private String PayChnlName = null;

    /** 缴费总金额 */
    private String PayMoney = null;

    /** 支付时间 */
    private String PayDate = null;

    /** 到帐时间 */
    private String PayEntAccDate = null;

    /** 到帐确认时间 */
    private String PayConfAccDate = null;

    /** 收费银行编码 */
    private String DescBankCode = null;

    /** 收费银行名称 */
    private String DescBankName = null;

    /** 收费帐户编码 */
    private String DescBankAccCode = null;

    /** 收费帐户名称 */
    private String DescBankAccName = null;

    /** 说明（备注） */
    private String Remark = null;
    
    /** 收费机构 */
    private String TempManageCom = null;

    /** 保险公司开户账号 */
    private String InsBankAccNo = null;
    
    /** 备用字段 */
    private String Bak1 = null;

    /** 备用字段 */
    private String Bak2 = null;

    /** 备用字段 */
    private String Bak3 = null;

    /** 备用字段 */
    private String Bak4 = null;

    /** 备用字段 */
    private String Bak5 = null;

    /** 备用字段 */
    private String Bak6 = null;

    /** 备用字段 */
    private String Bak7 = null;

    /** 备用字段 */
    private String Bak8 = null;

    /** 备用字段 */
    private String Bak9 = null;

    /** 备用字段 */
    private String Bak10 = null;
    
    /**
     * @return InsBankAccNo
     */
    public String getInsBankAccNo()
    {
        return InsBankAccNo;
    }

    /**
     * @param InsBankAccNo 要设置的 InsBankAccNo
     */
    public void setInsBankAccNo(final String insBankAccNo)
    {
    	InsBankAccNo = insBankAccNo;
    }

    
    /**
     * @return TempManageCom
     */
    public String getTempManageCom()
    {
        return TempManageCom;
    }

    /**
     * @param TempManageCom 要设置的 TempManageCom
     */
    public void setTempManageCom(final String tempManageCom)
    {
    	TempManageCom = tempManageCom;
    }


    /**
     * @return agentCode
     */
    public String getAgentCode()
    {
        return AgentCode;
    }

    /**
     * @param agentCode 要设置的 agentCode
     */
    public void setAgentCode(final String agentCode)
    {
        AgentCode = agentCode;
    }

    /**
     * @return agentCom
     */
    public String getAgentCom()
    {
        return AgentCom;
    }

    /**
     * @param agentCom 要设置的 agentCom
     */
    public void setAgentCom(final String agentCom)
    {
        AgentCom = agentCom;
    }

    /**
     * @return agentName
     */
    public String getAgentName()
    {
        return AgentName;
    }

    /**
     * @param agentName 要设置的 agentName
     */
    public void setAgentName(final String agentName)
    {
        AgentName = agentName;
    }

    /**
     * @return bak1
     */
    public String getBak1()
    {
        return Bak1;
    }

    /**
     * @param bak1 要设置的 bak1
     */
    public void setBak1(final String bak1)
    {
        Bak1 = bak1;
    }

    /**
     * @return bak10
     */
    public String getBak10()
    {
        return Bak10;
    }

    /**
     * @param bak10 要设置的 bak10
     */
    public void setBak10(final String bak10)
    {
        Bak10 = bak10;
    }

    /**
     * @return bak2
     */
    public String getBak2()
    {
        return Bak2;
    }

    /**
     * @param bak2 要设置的 bak2
     */
    public void setBak2(final String bak2)
    {
        Bak2 = bak2;
    }

    /**
     * @return bak3
     */
    public String getBak3()
    {
        return Bak3;
    }

    /**
     * @param bak3 要设置的 bak3
     */
    public void setBak3(final String bak3)
    {
        Bak3 = bak3;
    }

    /**
     * @return bak4
     */
    public String getBak4()
    {
        return Bak4;
    }

    /**
     * @param bak4 要设置的 bak4
     */
    public void setBak4(final String bak4)
    {
        Bak4 = bak4;
    }

    /**
     * @return bak5
     */
    public String getBak5()
    {
        return Bak5;
    }

    /**
     * @param bak5 要设置的 bak5
     */
    public void setBak5(final String bak5)
    {
        Bak5 = bak5;
    }

    /**
     * @return bak6
     */
    public String getBak6()
    {
        return Bak6;
    }

    /**
     * @param bak6 要设置的 bak6
     */
    public void setBak6(final String bak6)
    {
        Bak6 = bak6;
    }

    /**
     * @return bak7
     */
    public String getBak7()
    {
        return Bak7;
    }

    /**
     * @param bak7 要设置的 bak7
     */
    public void setBak7(final String bak7)
    {
        Bak7 = bak7;
    }

    /**
     * @return bak8
     */
    public String getBak8()
    {
        return Bak8;
    }

    /**
     * @param bak8 要设置的 bak8
     */
    public void setBak8(final String bak8)
    {
        Bak8 = bak8;
    }

    /**
     * @return bak9
     */
    public String getBak9()
    {
        return Bak9;
    }

    /**
     * @param bak9 要设置的 bak9
     */
    public void setBak9(final String bak9)
    {
        Bak9 = bak9;
    }

    /**
     * @return certifyCode
     */
    public String getCertifyCode()
    {
        return CertifyCode;
    }

    /**
     * @param certifyCode 要设置的 certifyCode
     */
    public void setCertifyCode(final String certifyCode)
    {
        CertifyCode = certifyCode;
    }

    /**
     * @return certifyName
     */
    public String getCertifyName()
    {
        return CertifyName;
    }

    /**
     * @param certifyName 要设置的 certifyName
     */
    public void setCertifyName(final String certifyName)
    {
        CertifyName = certifyName;
    }

    /**
     * @return descBankAccCode
     */
    public String getDescBankAccCode()
    {
        return DescBankAccCode;
    }

    /**
     * @param descBankAccCode 要设置的 descBankAccCode
     */
    public void setDescBankAccCode(final String descBankAccCode)
    {
        DescBankAccCode = descBankAccCode;
    }

    /**
     * @return descBankAccName
     */
    public String getDescBankAccName()
    {
        return DescBankAccName;
    }

    /**
     * @param descBankAccName 要设置的 descBankAccName
     */
    public void setDescBankAccName(final String descBankAccName)
    {
        DescBankAccName = descBankAccName;
    }

    /**
     * @return descBankCode
     */
    public String getDescBankCode()
    {
        return DescBankCode;
    }

    /**
     * @param descBankCode 要设置的 descBankCode
     */
    public void setDescBankCode(final String descBankCode)
    {
        DescBankCode = descBankCode;
    }

    /**
     * @return descBankName
     */
    public String getDescBankName()
    {
        return DescBankName;
    }

    /**
     * @param descBankName 要设置的 descBankName
     */
    public void setDescBankName(final String descBankName)
    {
        DescBankName = descBankName;
    }

    /**
     * @return manageCom
     */
    public String getManageCom()
    {
        return ManageCom;
    }

    /**
     * @param manageCom 要设置的 manageCom
     */
    public void setManageCom(final String manageCom)
    {
        ManageCom = manageCom;
    }

    /**
     * @return payChnlName
     */
    public String getPayChnlName()
    {
        return PayChnlName;
    }

    /**
     * @param payChnlName 要设置的 payChnlName
     */
    public void setPayChnlName(final String payChnlName)
    {
        PayChnlName = payChnlName;
    }

    /**
     * @return payChnlType
     */
    public String getPayChnlType()
    {
        return PayChnlType;
    }

    /**
     * @param payChnlType 要设置的 payChnlType
     */
    public void setPayChnlType(final String payChnlType)
    {
        PayChnlType = payChnlType;
    }

    /**
     * @return payConfAccDate
     */
    public String getPayConfAccDate()
    {
        return PayConfAccDate;
    }

    /**
     * @param payConfAccDate 要设置的 payConfAccDate
     */
    public void setPayConfAccDate(final String payConfAccDate)
    {
        PayConfAccDate = payConfAccDate;
    }

    /**
     * @return payDate
     */
    public String getPayDate()
    {
        return PayDate;
    }

    /**
     * @param payDate 要设置的 payDate
     */
    public void setPayDate(final String payDate)
    {
        PayDate = payDate;
    }

    /**
     * @return payEntAccDate
     */
    public String getPayEntAccDate()
    {
        return PayEntAccDate;
    }

    /**
     * @param payEntAccDate 要设置的 payEntAccDate
     */
    public void setPayEntAccDate(final String payEntAccDate)
    {
        PayEntAccDate = payEntAccDate;
    }

    /**
     * @return payMode
     */
    public String getPayMode()
    {
        return PayMode;
    }

    /**
     * @param payMode 要设置的 payMode
     */
    public void setPayMode(final String payMode)
    {
        PayMode = payMode;
    }

    /**
     * @return payMoney
     */
    public String getPayMoney()
    {
        return PayMoney;
    }

    /**
     * @param payMoney 要设置的 payMoney
     */
    public void setPayMoney(final String payMoney)
    {
        PayMoney = payMoney;
    }

    /**
     * @return payTradeSeq
     */
    public String getPayTradeSeq()
    {
        return PayTradeSeq;
    }

    /**
     * @param payTradeSeq 要设置的 payTradeSeq
     */
    public void setPayTradeSeq(final String payTradeSeq)
    {
        PayTradeSeq = payTradeSeq;
    }

    /**
     * @return remark
     */
    public String getRemark()
    {
        return Remark;
    }

    /**
     * @param remark 要设置的 remark
     */
    public void setRemark(final String remark)
    {
        Remark = remark;
    }

    /**
     * @return saleChnl
     */
    public String getSaleChnl()
    {
        return SaleChnl;
    }

    /**
     * @param saleChnl 要设置的 saleChnl
     */
    public void setSaleChnl(final String saleChnl)
    {
        SaleChnl = saleChnl;
    }

    /**
     * @return saleChnlName
     */
    public String getSaleChnlName()
    {
        return SaleChnlName;
    }

    /**
     * @param saleChnlName 要设置的 saleChnlName
     */
    public void setSaleChnlName(final String saleChnlName)
    {
        SaleChnlName = saleChnlName;
    }

    /**
     * @return salesCount
     */
    public String getSalesCount()
    {
        return SalesCount;
    }

    /**
     * @param salesCount 要设置的 salesCount
     */
    public void setSalesCount(final String salesCount)
    {
        SalesCount = salesCount;
    }

    /**
     * @return salesDate
     */
    public String getSalesDate()
    {
        return SalesDate;
    }

    /**
     * @param salesDate 要设置的 salesDate
     */
    public void setSalesDate(final String salesDate)
    {
        SalesDate = salesDate;
    }

    /**
     * @return wrapCode
     */
    public String getWrapCode()
    {
        return WrapCode;
    }

    /**
     * @param wrapCode 要设置的 wrapCode
     */
    public void setWrapCode(final String wrapCode)
    {
        WrapCode = wrapCode;
    }

    /**
     * @return wrapName
     */
    public String getWrapName()
    {
        return WrapName;
    }

    /**
     * @param wrapName 要设置的 wrapName
     */
    public void setWrapName(final String wrapName)
    {
        WrapName = wrapName;
    }
}
