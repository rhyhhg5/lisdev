package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class FHReportQueryInfo extends  BaseXmlSch{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1568463422119514018L;
	/**
	 * 
	 */
	/*保单号*/
	private String ContNo;
	/*红利领取年度*/
	private String PolYear;
	
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getPolYear() {
		return PolYear;
	}
	public void setPolYear(String polYear) {
		PolYear = polYear;
	}
}
