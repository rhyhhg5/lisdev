/**
 * 2010-8-23
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 单证同步信息清单
 * 
 * @author LY
 *
 */
public class CertSyncSetTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 保险卡号 */
    private String CardNo = null;

    /** 保险卡密码 */
    private String Password = null;

    /** 单证类型代码 */
    private String CertifyCode = null;

    /** 单证类型名称 */
    private String CertifyName = null;

    /** 套餐代码 */
    private String WrapCode = null;

    /** 套餐名称 */
    private String WrapName = null;

    /** 激活有效期 */
    private String ActValiDate = null;

    /**
     * @return actValiDate
     */
    public String getActValiDate()
    {
        return ActValiDate;
    }

    /**
     * @param actValiDate 要设置的 actValiDate
     */
    public void setActValiDate(final String actValiDate)
    {
        ActValiDate = actValiDate;
    }

    /**
     * @return cardNo
     */
    public String getCardNo()
    {
        return CardNo;
    }

    /**
     * @param cardNo 要设置的 cardNo
     */
    public void setCardNo(final String cardNo)
    {
        CardNo = cardNo;
    }

    /**
     * @return certifyCode
     */
    public String getCertifyCode()
    {
        return CertifyCode;
    }

    /**
     * @param certifyCode 要设置的 certifyCode
     */
    public void setCertifyCode(final String certifyCode)
    {
        CertifyCode = certifyCode;
    }

    /**
     * @return certifyName
     */
    public String getCertifyName()
    {
        return CertifyName;
    }

    /**
     * @param certifyName 要设置的 certifyName
     */
    public void setCertifyName(final String certifyName)
    {
        CertifyName = certifyName;
    }

    /**
     * @return password
     */
    public String getPassword()
    {
        return Password;
    }

    /**
     * @param password 要设置的 password
     */
    public void setPassword(final String password)
    {
        Password = password;
    }

    /**
     * @return wrapCode
     */
    public String getWrapCode()
    {
        return WrapCode;
    }

    /**
     * @param wrapCode 要设置的 wrapCode
     */
    public void setWrapCode(final String wrapCode)
    {
        WrapCode = wrapCode;
    }

    /**
     * @return wrapName
     */
    public String getWrapName()
    {
        return WrapName;
    }

    /**
     * @param wrapName 要设置的 wrapName
     */
    public void setWrapName(final String wrapName)
    {
        WrapName = wrapName;
    }
}
