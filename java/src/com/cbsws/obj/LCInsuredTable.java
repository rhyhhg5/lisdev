/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class LCInsuredTable extends BaseXmlSch
{

	private static final long serialVersionUID = -4794467791337930966L;
    /** 保单号*/
    private String ContNo = null;

    /** 被保人序号 */
    private String InsuredNo = null;

    /** 被保人姓名 */
    private String Name = null;

    /** 性别 */
    private String Sex = null;

    /** 出生日期 */
    private String Birthday = null;

    /** 证件类型 */
    private String IDType = null;
    
    /** 证件号码 */
    private String IDNo = null;
    
    /** 电子邮箱 */
    private String Email = null;
    
    /**  职业编码 */
    private String OccupationCode = null;
    
    /**  职业类型 */
    private String OccupationType = null;
    
    /** 联系地址 */
    private String HomeAddress = null;

    /** 通信地址 */
    private String MailAddress = null;
    
    /** 移动电话 */
    private String InsuredMobile = null;

    /** 家庭电话 */
    private String HomePhone = null;
    
    /** 邮政编码 */
    private String MailZipCode = null;
    
    /** 与主被保险人关系 */
    private String RelaToMain = null;
    
    /** 与投保人关系 */
    private String RelaToAppnt = null;
    
    private String ReInsuredNo = null;
    
    /** 身高 ~cm*/
    private String Stature=null;

    /** 体重 ~kg*/
    private String Avoirdupois=null;
    
    /** 授权使用客户信息 */
	private String Authorization=null;
	
	/**共享标识 */
	private String SharedMark;
	/**特殊限定标识 */
	private String SpecialLimitMark;
	/**业务环节*/
	private String BusinessLink;
	/**业务触面*/
	private String CustomerContact;
	/**授权合约类型*/
	private String AuthType;
	/**
	 * @return the reInsuredNo
	 */
	public String getReInsuredNo() {
		return ReInsuredNo;
	}

	/**
	 * @param reInsuredNo the reInsuredNo to set
	 */
	public void setReInsuredNo(String reInsuredNo) {
		ReInsuredNo = reInsuredNo;
	}

	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return Birthday;
	}

	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(String birthday) {
		Birthday = birthday;
	}

	/**
	 * @return the contNo
	 */
	public String getContNo() {
		return ContNo;
	}

	/**
	 * @param contNo the contNo to set
	 */
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return Email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		Email = email;
	}

	/**
	 * @return the homeAddress
	 */
	public String getHomeAddress() {
		return HomeAddress;
	}

	/**
	 * @param homeAddress the homeAddress to set
	 */
	public void setHomeAddress(String homeAddress) {
		HomeAddress = homeAddress;
	}

	/**
	 * @return the homePhone
	 */
	public String getHomePhone() {
		return HomePhone;
	}

	/**
	 * @param homePhone the homePhone to set
	 */
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}

	/**
	 * @return the iDNo
	 */
	public String getIDNo() {
		return IDNo;
	}

	/**
	 * @param no the iDNo to set
	 */
	public void setIDNo(String no) {
		IDNo = no;
	}

	/**
	 * @return the iDType
	 */
	public String getIDType() {
		return IDType;
	}

	/**
	 * @param type the iDType to set
	 */
	public void setIDType(String type) {
		IDType = type;
	}

	/**
	 * @return the insuredMobile
	 */
	public String getInsuredMobile() {
		return InsuredMobile;
	}

	/**
	 * @param insuredMobile the insuredMobile to set
	 */
	public void setInsuredMobile(String insuredMobile) {
		InsuredMobile = insuredMobile;
	}

	/**
	 * @return the insuredNo
	 */
	public String getInsuredNo() {
		return InsuredNo;
	}

	/**
	 * @param insuredNo the insuredNo to set
	 */
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}

	/**
	 * @return the occupationCode
	 */
	public String getOccupationCode() {
		return OccupationCode;
	}

	/**
	 * @param occupationCode the occupationCode to set
	 */
	public void setOccupationCode(String occupationCode) {
		OccupationCode = occupationCode;
	}

	/**
	 * @return the mailAddress
	 */
	public String getMailAddress() {
		return MailAddress;
	}

	/**
	 * @param mailAddress the mailAddress to set
	 */
	public void setMailAddress(String mailAddress) {
		MailAddress = mailAddress;
	}

	/**
	 * @return the mailZipCode
	 */
	public String getMailZipCode() {
		return MailZipCode;
	}

	/**
	 * @param mailZipCode the mailZipCode to set
	 */
	public void setMailZipCode(String mailZipCode) {
		MailZipCode = mailZipCode;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the relaToAppnt
	 */
	public String getRelaToAppnt() {
		return RelaToAppnt;
	}

	/**
	 * @param relaToAppnt the relaToAppnt to set
	 */
	public void setRelaToAppnt(String relaToAppnt) {
		RelaToAppnt = relaToAppnt;
	}

	/**
	 * @return the relaToMain
	 */
	public String getRelaToMain() {
		return RelaToMain;
	}

	/**
	 * @param relaToMain the relaToMain to set
	 */
	public void setRelaToMain(String relaToMain) {
		RelaToMain = relaToMain;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return Sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		Sex = sex;
	}

	/**
	 * @return the occupationType
	 */
	public String getOccupationType() {
		return OccupationType;
	}

	/**
	 * @param occupationType the occupationType to set
	 */
	public void setOccupationType(String occupationType) {
		OccupationType = occupationType;
	}

	public String getStature() {
		return Stature;
	}

	public void setStature(String stature) {
		Stature = stature;
	}

	public String getAvoirdupois() {
		return Avoirdupois;
	}

	public void setAvoirdupois(String avoirdupois) {
		Avoirdupois = avoirdupois;
	}

	/**
	 * @return the authorization
	 */
	public String getAuthorization() {
		return Authorization;
	}

	/**
	 * @param authorization the authorization to set
	 */
	public void setAuthorization(String authorization) {
		Authorization = authorization;
	}

	public String getSharedMark() {
		return SharedMark;
	}

	public void setSharedMark(String sharedMark) {
		SharedMark = sharedMark;
	}

	public String getSpecialLimitMark() {
		return SpecialLimitMark;
	}

	public void setSpecialLimitMark(String specialLimitMark) {
		SpecialLimitMark = specialLimitMark;
	}

	public String getBusinessLink() {
		return BusinessLink;
	}

	public void setBusinessLink(String businessLink) {
		BusinessLink = businessLink;
	}

	public String getCustomerContact() {
		return CustomerContact;
	}

	public void setCustomerContact(String customerContact) {
		CustomerContact = customerContact;
	}

	public String getAuthType() {
		return AuthType;
	}

	public void setAuthType(String authType) {
		AuthType = authType;
	}
	
	
	
	
}
