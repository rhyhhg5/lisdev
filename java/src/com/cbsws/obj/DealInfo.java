/**
 * 2016-10-25
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保全项目信息
 * 
 * @author LC
 *
 */
public class DealInfo extends BaseXmlSch
{
	
	private static final long serialVersionUID = -6507772631037260086L;

	/** 处理结果*/
    private String DealResult = null;

    /** 工单号*/
    private String EdorNo = null;

    /** 备注 */
    private String Remark = null;

    
	public String getDealResult() {
		return DealResult;
	}

	public void setDealResult(String dealResult) {
		DealResult = dealResult;
	}

	public String getEdorNo() {
		return EdorNo;
	}

	public void setEdorNo(String edorNo) {
		EdorNo = edorNo;
	}

	public String getRemark() {
		return Remark;
	}

	public void setRemark(String remark) {
		Remark = remark;
	}
    
}
