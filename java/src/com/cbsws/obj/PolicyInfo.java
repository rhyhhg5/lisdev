/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保全返回险种信息
 * 
 * @author lzy
 *
 */
public class PolicyInfo extends BaseXmlSch
{
	
	private static final long serialVersionUID = -5939388491857267343L;

	/** 险种编码*/
    private String PolNo = null;

    private String RiskCode = null;

    private String RiskName = null;

    private String InsuredName = null;
    
    /** 险种退费金额*/
    private String GetMoney = null;

	public String getPolNo() {
		return PolNo;
	}

	public void setPolNo(String polNo) {
		PolNo = polNo;
	}

	public String getRiskCode() {
		return RiskCode;
	}

	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}

	public String getRiskName() {
		return RiskName;
	}

	public void setRiskName(String riskName) {
		RiskName = riskName;
	}

	public String getInsuredName() {
		return InsuredName;
	}

	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}

	public String getGetMoney() {
		return GetMoney;
	}

	public void setGetMoney(String getMoney) {
		GetMoney = getMoney;
	}


    
    
}
