package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class EdorGRPLQChangeInfo extends  BaseXmlSch{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1967123225108135150L;
	
	/*团单号*/
	private String GrpContNo;
	/*分单保单号*/
	private String ContNo;
	/* 被保险人客户号  */
	private String  InsuredNo;
	/*部分领取金额 */
	private String GetMoney;
	/*保全生效日期 */
	private String EdorCValiDate;
	/* 领取方式*/
	private String Paymode;
	/* 银行编码*/
	private String BankCode;
	/* 银行账户*/
	private String AccNo;
	/* 账户名*/
	private String AccName;
	public String getGrpContNo() {
		return GrpContNo;
	}
	public void setGrpContNo(String grpContNo) {
		GrpContNo = grpContNo;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getInsuredNo() {
		return InsuredNo;
	}
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}
	public String getGetMoney() {
		return GetMoney;
	}
	public void setGetMoney(String getMoney) {
		GetMoney = getMoney;
	}
	public String getEdorCValiDate() {
		return EdorCValiDate;
	}
	public void setEdorCValiDate(String edorCValiDate) {
		EdorCValiDate = edorCValiDate;
	}
	public String getPaymode() {
		return Paymode;
	}
	public void setPaymode(String paymode) {
		Paymode = paymode;
	}
	public String getBankCode() {
		return BankCode;
	}
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}
	public String getAccNo() {
		return AccNo;
	}
	public void setAccNo(String accNo) {
		AccNo = accNo;
	}
	public String getAccName() {
		return AccName;
	}
	public void setAccName(String accName) {
		AccName = accName;
	}
	
}
