/**
 * 2011-04-11
 */
package com.cbsws.obj;

import java.io.Serializable;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保单信息
 * 
 * @author 
 *
 */
public class ContQueryInfo extends BaseXmlSch
{
    

    /**
	 * 
	 */
	private static final long serialVersionUID = 6941247632829088961L;

	/** 保单号*/
    private String ContNo = null;

    /** 印刷号*/
    private String PrtNo = null;

    /** 客户姓名 */
    private String CustomerName = null;
    
    /** 客户证件号码*/
    private String idno = null;
    
    /** 业务员代码 */
    private String AgentCode = null;

    
    
	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public String getPrtNo() {
		return PrtNo;
	}

	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public String getAgentCode() {
		return AgentCode;
	}

	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

    

}
