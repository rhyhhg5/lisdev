package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class UniSalesAppayInfo extends BaseXmlSch{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5324404810839038461L;
	
	private String APPID; //系统ID
	private String SID; //序列号
	private String TIMESTAMP ; //请求时间
	private String MESSAGETYPE; //消息类型
	private String uni_sales_cod;//统一编码
	private String comp_cod; //子公司代码
	private String comp_nam; //子公司名称
	private String PROV_ORGCOD;//省份代码
	private String PROV_ORGNAME;//省份名称
	private String sales_nam; //姓名
	private String date_birthd; //出生日期
	private String sex_cod; //性别代码
	private String man_org_cod;//管理机构代码
	private String man_org_nam;//管理机构名称
	private String idtyp_cod; //证件类型代码
	private String idtyp_nam;//证件类型名称
	private String id_no;//证件号码
	private String 	sales_tel;//联系电话
	private String 	sales_mob;//联系手机
	private String sales_mail;//电子邮件
	private String sales_addr;//邮寄地址
	private String education_cod ; //学历代码
	private String education_nam; //学历名称
	private String qualifi_no;//资格证书编号
	private String certifi_no;//执业证书编号
	private String contract_no;//委托代理合同编号
	private String 	status_cod;//在职状态代码
	private String status;//在职状态名称
	private String 	sales_typ_cod;//销售人员类型代码
	private String sales_typ;//销售人员类型名称
	private String is_crosssale;//是否允许开展交叉销售业务
	private String channel_cod;//销售渠道代码
	private String salecha_nam;//销售渠道名称
	private String entrant_date;//入职时间
	private String dimission_date;//准离司时间/离司时间
	private String demission_reason;//离职原因
	private String date_send;//报送时间
	public String getAPPID() {
		return APPID;
	}
	public void setAPPID(String aPPID) {
		APPID = aPPID;
	}
	public String getSID() {
		return SID;
	}
	public void setSID(String sID) {
		SID = sID;
	}
	public String getTIMESTAMP() {
		return TIMESTAMP;
	}
	public void setTIMESTAMP(String tIMESTAMP) {
		TIMESTAMP = tIMESTAMP;
	}
	public String getMESSAGETYPE() {
		return MESSAGETYPE;
	}
	public void setMESSAGETYPE(String mESSAGETYPE) {
		MESSAGETYPE = mESSAGETYPE;
	}
	public String getUni_sales_cod() {
		return uni_sales_cod;
	}
	public void setUni_sales_cod(String uniSalesCod) {
		uni_sales_cod = uniSalesCod;
	}
	public String getComp_cod() {
		return comp_cod;
	}
	public void setComp_cod(String compCod) {
		comp_cod = compCod;
	}
	public String getComp_nam() {
		return comp_nam;
	}
	public void setComp_nam(String compNam) {
		comp_nam = compNam;
	}
	public String getSales_nam() {
		return sales_nam;
	}
	public void setSales_nam(String salesNam) {
		sales_nam = salesNam;
	}
	public String getDate_birthd() {
		return date_birthd;
	}
	public void setDate_birthd(String dateBirthd) {
		date_birthd = dateBirthd;
	}
	public String getSex_cod() {
		return sex_cod;
	}
	public void setSex_cod(String sexCod) {
		sex_cod = sexCod;
	}
	public String getMan_org_cod() {
		return man_org_cod;
	}
	public void setMan_org_cod(String manOrgCod) {
		man_org_cod = manOrgCod;
	}
	public String getMan_org_nam() {
		return man_org_nam;
	}
	public void setMan_org_nam(String manOrgNam) {
		man_org_nam = manOrgNam;
	}
	public String getIdtyp_cod() {
		return idtyp_cod;
	}
	public void setIdtyp_cod(String idtypCod) {
		idtyp_cod = idtypCod;
	}
	public String getIdtyp_nam() {
		return idtyp_nam;
	}
	public void setIdtyp_nam(String idtypNam) {
		idtyp_nam = idtypNam;
	}
	public String getId_no() {
		return id_no;
	}
	public void setId_no(String idNo) {
		id_no = idNo;
	}
	public String getSales_tel() {
		return sales_tel;
	}
	public void setSales_tel(String salesTel) {
		sales_tel = salesTel;
	}
	public String getSales_mob() {
		return sales_mob;
	}
	public void setSales_mob(String salesMob) {
		sales_mob = salesMob;
	}
	public String getSales_mail() {
		return sales_mail;
	}
	public void setSales_mail(String salesMail) {
		sales_mail = salesMail;
	}
	public String getSales_addr() {
		return sales_addr;
	}
	public void setSales_addr(String salesAddr) {
		sales_addr = salesAddr;
	}
	public String getEducation_cod() {
		return education_cod;
	}
	public void setEducation_cod(String educationCod) {
		education_cod = educationCod;
	}
	public String getEducation_nam() {
		return education_nam;
	}
	public void setEducation_nam(String educationNam) {
		education_nam = educationNam;
	}
	public String getQualifi_no() {
		return qualifi_no;
	}
	public void setQualifi_no(String qualifiNo) {
		qualifi_no = qualifiNo;
	}
	public String getCertifi_no() {
		return certifi_no;
	}
	public void setCertifi_no(String certifiNo) {
		certifi_no = certifiNo;
	}
	public String getContract_no() {
		return contract_no;
	}
	public void setContract_no(String contractNo) {
		contract_no = contractNo;
	}
	public String getStatus_cod() {
		return status_cod;
	}
	public void setStatus_cod(String statusCod) {
		status_cod = statusCod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSales_typ_cod() {
		return sales_typ_cod;
	}
	public void setSales_typ_cod(String salesTypCod) {
		sales_typ_cod = salesTypCod;
	}
	public String getSales_typ() {
		return sales_typ;
	}
	public void setSales_typ(String salesTyp) {
		sales_typ = salesTyp;
	}
	public String getIs_crosssale() {
		return is_crosssale;
	}
	public void setIs_crosssale(String isCrosssale) {
		is_crosssale = isCrosssale;
	}
	public String getChannel_cod() {
		return channel_cod;
	}
	public void setChannel_cod(String channelCod) {
		channel_cod = channelCod;
	}
	public String getSalecha_nam() {
		return salecha_nam;
	}
	public void setSalecha_nam(String salechaNam) {
		salecha_nam = salechaNam;
	}
	public String getEntrant_date() {
		return entrant_date;
	}
	public void setEntrant_date(String entrantDate) {
		entrant_date = entrantDate;
	}
	public String getDimission_date() {
		return dimission_date;
	}
	public void setDimission_date(String dimissionDate) {
		dimission_date = dimissionDate;
	}
	public String getDemission_reason() {
		return demission_reason;
	}
	public void setDemission_reason(String demissionReason) {
		demission_reason = demissionReason;
	}
	public String getDate_send() {
		return date_send;
	}
	public void setDate_send(String dateSend) {
		date_send = dateSend;
	}
	public String getPROV_ORGCOD() {
		return PROV_ORGCOD;
	}
	public void setPROV_ORGCOD(String prov_orgcod) {
		PROV_ORGCOD = prov_orgcod;
	}
	public String getPROV_ORGNAME() {
		return PROV_ORGNAME;
	}
	public void setPROV_ORGNAME(String prov_orgname) {
		PROV_ORGNAME = prov_orgname;
	}
	
	
}
