/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class SHRYCustomerImpartDetailTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    
    private String ImpartCode; 
    private String ImpartVer;
    private String ImpartParamModle;
    private String DiseaseContent; //疾病内容
    private String StartDate;
    private String EndDate;
    private String Prover; //证明人
    private String CurrCondition;//目前状况
    private String IsProved; //能否证明
    
	public String getDiseaseContent() {
		return DiseaseContent;
	}
	public void setDiseaseContent(String diseaseContent) {
		DiseaseContent = diseaseContent;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getEndDate() {
		return EndDate;
	}
	public void setEndDate(String endDate) {
		EndDate = endDate;
	}
	public String getProver() {
		return Prover;
	}
	public void setProver(String prover) {
		Prover = prover;
	}
	public String getCurrCondition() {
		return CurrCondition;
	}
	public void setCurrCondition(String currCondition) {
		CurrCondition = currCondition;
	}
	public String getIsProved() {
		return IsProved;
	}
	public void setIsProved(String isProved) {
		IsProved = isProved;
	}
	public String getImpartCode() {
		return ImpartCode;
	}
	public void setImpartCode(String impartCode) {
		ImpartCode = impartCode;
	}
	public String getImpartVer() {
		return ImpartVer;
	}
	public void setImpartVer(String impartVer) {
		ImpartVer = impartVer;
	}
	public String getImpartParamModle() {
		return ImpartParamModle;
	}
	public void setImpartParamModle(String impartParamModle) {
		ImpartParamModle = impartParamModle;
	}
	
    
    
  
	
}
