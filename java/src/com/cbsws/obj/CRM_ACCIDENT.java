package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 理赔通知案件信息
 * @author wujun
 *
 */
public class CRM_ACCIDENT extends BaseXmlSch {
	
    private static final long serialVersionUID = -4794467791337930966L;
	
	/**
	 * 来电号码
	 */
	private String CALLNUMBER = "";
	
	/**
	 * 报案日期
	 */
	private String RPTDATE = "";
	
	/**
	 * 报案时间
	 */
	private String RPTTIME = "";
	
	/**
	 * 工单来源
	 */
	private String SOURCE = "";
	
	/**
	 * 被保险人客户姓名
	 */
	private String INSUREDNAME = "";
	
	/**
	 * 被保险人客户号
	 */
	private String INSUREDNO = "";

	/**
	 * 被保险人证件号码
	 */
	private String IDNO = "";
	
	/**
	 * 联系电话
	 */
	private String PHONE = "";
	
	/**
	 * 出险时间
	 */
	private String ACCIDENTDATE = "";
	
	/**
	 * 被保险人联系电话
	 */
	private String INSUREDPHONE = "";
	
	/**
	 * 客户现状
	 */
	private String STATION = "";
	
	/**
	 * 报案人与被保险人关系
	 */
	private String RELATION = "";
	
	/**
	 * 出险地
	 */
	private String ACCIDENTADDRESS = "";
	
	/**
	 * 报案人姓名
	 */
	private String RPTORNAME = "";
	
	/**
	 * 报案人联系电话
	 */
	private String RPTORPHONE = "";
	
	/**
	 * 报案内容
	 */
	private String RPTINFO = "";
	
	/**
	 * 是否需要回复（Y为是；N为否）
	 */
	private String REPLY = "";
	
	/**
	 * 回复期限
	 */
	private String REPLYDATE = "";
	
	/**
	 * 备注说明
	 */
	private String REMARK = "";
	
	/**
	 * 保险卡号
	 */
	private String CARDNO = "";
	
	/**
	 * 发生地点(省)
	 * @return
	 */
	private String PROVINCECODE ="";
	/**
	 * 发生地点(市)
	 * @return
	 */
	private String CITYCODE ="";
	/**
	 * 发生地点(县)
	 * @return
	 */
	private String AREACODE ="";
	
	public String getACCIDENTADDRESS() {
		return ACCIDENTADDRESS;
	}

	public void setACCIDENTADDRESS(String accidentaddress) {
		ACCIDENTADDRESS = accidentaddress;
	}

	public String getACCIDENTDATE() {
		return ACCIDENTDATE;
	}

	public void setACCIDENTDATE(String accidentdate) {
		ACCIDENTDATE = accidentdate;
	}

	public String getCALLNUMBER() {
		return CALLNUMBER;
	}

	public void setCALLNUMBER(String callnumber) {
		CALLNUMBER = callnumber;
	}

	public String getIDNO() {
		return IDNO;
	}

	public void setIDNO(String idno) {
		IDNO = idno;
	}

	public String getINSUREDNAME() {
		return INSUREDNAME;
	}

	public void setINSUREDNAME(String insuredname) {
		INSUREDNAME = insuredname;
	}

	public String getINSUREDNO() {
		return INSUREDNO;
	}

	public void setINSUREDNO(String insuredno) {
		INSUREDNO = insuredno;
	}

	public String getINSUREDPHONE() {
		return INSUREDPHONE;
	}

	public void setINSUREDPHONE(String insuredphone) {
		INSUREDPHONE = insuredphone;
	}

	public String getPHONE() {
		return PHONE;
	}

	public void setPHONE(String phone) {
		PHONE = phone;
	}

	public String getRELATION() {
		return RELATION;
	}

	public void setRELATION(String relation) {
		RELATION = relation;
	}

	public String getREMARK() {
		return REMARK;
	}

	public void setREMARK(String remark) {
		REMARK = remark;
	}

	public String getREPLY() {
		return REPLY;
	}

	public void setREPLY(String reply) {
		REPLY = reply;
	}

	public String getREPLYDATE() {
		return REPLYDATE;
	}

	public void setREPLYDATE(String replydate) {
		REPLYDATE = replydate;
	}

	public String getRPTDATE() {
		return RPTDATE;
	}

	public void setRPTDATE(String rptdate) {
		RPTDATE = rptdate;
	}

	public String getRPTINFO() {
		return RPTINFO;
	}

	public void setRPTINFO(String rptinfo) {
		RPTINFO = rptinfo;
	}

	public String getRPTORNAME() {
		return RPTORNAME;
	}

	public void setRPTORNAME(String rptorname) {
		RPTORNAME = rptorname;
	}

	public String getRPTORPHONE() {
		return RPTORPHONE;
	}

	public void setRPTORPHONE(String rptorphone) {
		RPTORPHONE = rptorphone;
	}

	public String getRPTTIME() {
		return RPTTIME;
	}

	public void setRPTTIME(String rpttime) {
		RPTTIME = rpttime;
	}

	public String getSOURCE() {
		return SOURCE;
	}

	public void setSOURCE(String source) {
		SOURCE = source;
	}

	public String getSTATION() {
		return STATION;
	}

	public void setSTATION(String station) {
		STATION = station;
	}

	public String getCARDNO() {
		return CARDNO;
	}

	public void setCARDNO(String cardno) {
		CARDNO = cardno;
	}

	public String getPROVINCECODE() {
		return PROVINCECODE;
	}

	public void setPROVINCECODE(String pROVINCECODE) {
		PROVINCECODE = pROVINCECODE;
	}

	public String getCITYCODE() {
		return CITYCODE;
	}

	public void setCITYCODE(String cITYCODE) {
		CITYCODE = cITYCODE;
	}

	public String getAREACODE() {
		return AREACODE;
	}

	public void setAREACODE(String aREACODE) {
		AREACODE = aREACODE;
	}

}
