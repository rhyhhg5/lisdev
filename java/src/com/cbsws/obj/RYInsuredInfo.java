/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class RYInsuredInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String InsuNo;
    private String ToAppntRela;
    private String RelaTionCode;
    private String RelationToInsured;
    private String ReInsuNo;
    private String InsuredOrder;
    private String InsuredName;
    private String InsuredSex;
    private String InsuredIdType;
    private String InsuredIdNo;
    private String InsuredBirthday;
    private String InsuredMarryState;
    private String InsuredNativePlace;
    private String InsuredOccupaTion;
    private String InsuredMobile;
    private String InsuredCompanyPhone;
    private String InsuredHomePhone;
    private String InsuredAddress;
    private String InsuredZipcode;
    private String InsuredEmail;
    private String InsuredPhone;
	public String getInsuNo() {
		return InsuNo;
	}
	public void setInsuNo(String insuNo) {
		InsuNo = insuNo;
	}
	public String getToAppntRela() {
		return ToAppntRela;
	}
	public void setToAppntRela(String toAppntRela) {
		ToAppntRela = toAppntRela;
	}
	public String getRelaTionCode() {
		return RelaTionCode;
	}
	public void setRelaTionCode(String relaTionCode) {
		RelaTionCode = relaTionCode;
	}
	public String getRelationToInsured() {
		return RelationToInsured;
	}
	public void setRelationToInsured(String relationToInsured) {
		RelationToInsured = relationToInsured;
	}
	public String getReInsuNo() {
		return ReInsuNo;
	}
	public void setReInsuNo(String reInsuNo) {
		ReInsuNo = reInsuNo;
	}
	public String getInsuredOrder() {
		return InsuredOrder;
	}
	public void setInsuredOrder(String insuredOrder) {
		InsuredOrder = insuredOrder;
	}
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
	public String getInsuredSex() {
		return InsuredSex;
	}
	public void setInsuredSex(String insuredSex) {
		InsuredSex = insuredSex;
	}
	public String getInsuredIdType() {
		return InsuredIdType;
	}
	public void setInsuredIdType(String insuredIdType) {
		InsuredIdType = insuredIdType;
	}
	public String getInsuredIdNo() {
		return InsuredIdNo;
	}
	public void setInsuredIdNo(String insuredIdNo) {
		InsuredIdNo = insuredIdNo;
	}
	public String getInsuredBirthday() {
		return InsuredBirthday;
	}
	public void setInsuredBirthday(String insuredBirthday) {
		InsuredBirthday = insuredBirthday;
	}
	public String getInsuredMarryState() {
		return InsuredMarryState;
	}
	public void setInsuredMarryState(String insuredMarryState) {
		InsuredMarryState = insuredMarryState;
	}
	public String getInsuredNativePlace() {
		return InsuredNativePlace;
	}
	public void setInsuredNativePlace(String insuredNativePlace) {
		InsuredNativePlace = insuredNativePlace;
	}
	public String getInsuredOccupaTion() {
		return InsuredOccupaTion;
	}
	public void setInsuredOccupaTion(String insuredOccupaTion) {
		InsuredOccupaTion = insuredOccupaTion;
	}
	public String getInsuredMobile() {
		return InsuredMobile;
	}
	public void setInsuredMobile(String insuredMobile) {
		InsuredMobile = insuredMobile;
	}
	public String getInsuredCompanyPhone() {
		return InsuredCompanyPhone;
	}
	public void setInsuredCompanyPhone(String insuredCompanyPhone) {
		InsuredCompanyPhone = insuredCompanyPhone;
	}
	public String getInsuredHomePhone() {
		return InsuredHomePhone;
	}
	public void setInsuredHomePhone(String insuredHomePhone) {
		InsuredHomePhone = insuredHomePhone;
	}
	public String getInsuredAddress() {
		return InsuredAddress;
	}
	public void setInsuredAddress(String insuredAddress) {
		InsuredAddress = insuredAddress;
	}
	public String getInsuredZipcode() {
		return InsuredZipcode;
	}
	public void setInsuredZipcode(String insuredZipcode) {
		InsuredZipcode = insuredZipcode;
	}
	public String getInsuredEmail() {
		return InsuredEmail;
	}
	public void setInsuredEmail(String insuredEmail) {
		InsuredEmail = insuredEmail;
	}
	public String getInsuredPhone() {
		return InsuredPhone;
	}
	public void setInsuredPhone(String insuredPhone) {
		InsuredPhone = insuredPhone;
	}
    
    
    
    
    
	
    
    
    
}
