/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class SHWrapTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 保单号*/
    private String ContNo = null;

    /** 被保人序号 */
    private String InsuredNo = null;

    /** 套餐编码 */
    private String RiskWrapCode = null;

    /** 主险种编码 */
    private String MainRiskCode = null;

    /** 保额 */
    private String Amnt = null;

    /** 保费 */
    private String Prem = null;
    
    /** 档次 */
    private String Mult = null;
    
    /** 交费期间单位 */
    private String PayEndYearFlag = null;

    /** 交费期间 */
    private String PayEndYear = null;

    /** 保障期间标识 */
    private String InsuYearFlag = null;
    
    /** 保障年期 */
    private String InsuYear = null;
    
    /** 折扣 */
    private String FeeRate = null;

	/**
	 * @return the feeRate
	 */
	public String getFeeRate() {
		return FeeRate;
	}

	/**
	 * @param feeRate the feeRate to set
	 */
	public void setFeeRate(String feeRate) {
		FeeRate = feeRate;
	}

	/**
	 * @return the amnt
	 */
	public String getAmnt() {
		return Amnt;
	}

	/**
	 * @param amnt the amnt to set
	 */
	public void setAmnt(String amnt) {
		Amnt = amnt;
	}

	/**
	 * @return the contNo
	 */
	public String getContNo() {
		return ContNo;
	}

	/**
	 * @param contNo the contNo to set
	 */
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	/**
	 * @return the insuredNo
	 */
	public String getInsuredNo() {
		return InsuredNo;
	}

	/**
	 * @param insuredNo the insuredNo to set
	 */
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}

	/**
	 * @return the insuYear
	 */
	public String getInsuYear() {
		return InsuYear;
	}

	/**
	 * @param insuYear the insuYear to set
	 */
	public void setInsuYear(String insuYear) {
		InsuYear = insuYear;
	}

	/**
	 * @return the insuYearFlag
	 */
	public String getInsuYearFlag() {
		return InsuYearFlag;
	}

	/**
	 * @param insuYearFlag the insuYearFlag to set
	 */
	public void setInsuYearFlag(String insuYearFlag) {
		InsuYearFlag = insuYearFlag;
	}

	/**
	 * @return the mainRiskCode
	 */
	public String getMainRiskCode() {
		return MainRiskCode;
	}

	/**
	 * @param mainRiskCode the mainRiskCode to set
	 */
	public void setMainRiskCode(String mainRiskCode) {
		MainRiskCode = mainRiskCode;
	}

	/**
	 * @return the mult
	 */
	public String getMult() {
		return Mult;
	}

	/**
	 * @param mult the mult to set
	 */
	public void setMult(String mult) {
		Mult = mult;
	}

	/**
	 * @return the payEndYear
	 */
	public String getPayEndYear() {
		return PayEndYear;
	}

	/**
	 * @param payEndYear the payEndYear to set
	 */
	public void setPayEndYear(String payEndYear) {
		PayEndYear = payEndYear;
	}

	/**
	 * @return the payEndYearFlag
	 */
	public String getPayEndYearFlag() {
		return PayEndYearFlag;
	}

	/**
	 * @param payEndYearFlag the payEndYearFlag to set
	 */
	public void setPayEndYearFlag(String payEndYearFlag) {
		PayEndYearFlag = payEndYearFlag;
	}

	/**
	 * @return the prem
	 */
	public String getPrem() {
		return Prem;
	}

	/**
	 * @param prem the prem to set
	 */
	public void setPrem(String prem) {
		Prem = prem;
	}

	/**
	 * @return the riskWrapCode
	 */
	public String getRiskWrapCode() {
		return RiskWrapCode;
	}

	/**
	 * @param riskWrapCode the riskWrapCode to set
	 */
	public void setRiskWrapCode(String riskWrapCode) {
		RiskWrapCode = riskWrapCode;
	}
    
    

}
