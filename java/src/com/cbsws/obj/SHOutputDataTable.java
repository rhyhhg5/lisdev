package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;




public class SHOutputDataTable extends BaseXmlSch{

	private static final long serialVersionUID = 1L;

	/** 保单号*/
    private String ContNo = null;
    
	 /** 印刷号*/
    private String PrtNo = null;

    /** 保额 */
    private String Amnt = null;

    /** 保费 */
    private String Prem = null;
    
    /** 保单状态 */
    private String StateFlag = null;
    
    /** 新单复核失败原因 */
    private String ErrorInfo = null;

	public String getAmnt() {
		return Amnt;
	}

	public void setAmnt(String amnt) {
		Amnt = amnt;
	}

	public String getPrem() {
		return Prem;
	}

	public void setPrem(String prem) {
		Prem = prem;
	}

	public String getPrtNo() {
		return PrtNo;
	}

	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public void setStateFlag(String stateFlag) {
		StateFlag = stateFlag;
	}

	public String getStateFlag() {
		return StateFlag;
	}

	public String getErrorInfo() {
		return ErrorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		ErrorInfo = errorInfo;
	}


}
