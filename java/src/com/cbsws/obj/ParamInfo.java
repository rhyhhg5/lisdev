package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class ParamInfo extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1001158455728817431L;
	
	/**
	 * 处理状态
	 */
	private String State = "";
	/**
	 * 工本费
	 */
	private String GBFee = "";
	/**
	 * 操作人员
	 */
	private String Operator = "";
	/**
	 * 操作机构
	 */
	private String ManageCom = "";
	/**
	 * 错误信息
	 */
	private String ErrInfo = "";
	/**
	 * 应退保费
	 */
	private String BackMoney = "";
	/**
	 * 付费通知书号
	 */
	private String ActuGetno = "";
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getGBFee() {
		return GBFee;
	}
	public void setGBFee(String gBFee) {
		GBFee = gBFee;
	}
	public String getOperator() {
		return Operator;
	}
	public void setOperator(String operator) {
		Operator = operator;
	}
	public String getManageCom() {
		return ManageCom;
	}
	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}
	public String getErrInfo() {
		return ErrInfo;
	}
	public void setErrInfo(String errInfo) {
		ErrInfo = errInfo;
	}
	public String getBackMoney() {
		return BackMoney;
	}
	public void setBackMoney(String backMoney) {
		BackMoney = backMoney;
	}
	public String getActuGetno() {
		return ActuGetno;
	}
	public void setActuGetno(String actuGetno) {
		ActuGetno = actuGetno;
	}
	
}
