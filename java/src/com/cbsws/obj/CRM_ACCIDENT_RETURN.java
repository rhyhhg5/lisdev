package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 理赔通知案件信息
 * @author wujun
 *
 */
public class CRM_ACCIDENT_RETURN extends BaseXmlSch {
	
    private static final long serialVersionUID = -4794467791337930966L;
	
	/**
	 * 理赔通知类案件号
	 */
	private String ACCIDENTNO = "";

	public String getACCIDENTNO() {
		return ACCIDENTNO;
	}

	public void setACCIDENTNO(String accidentno) {
		ACCIDENTNO = accidentno;
	}
	
	

	
	
}
