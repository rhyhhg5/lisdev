package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class PolMonthBalaInfo extends BaseXmlSch{
	

	private static final long serialVersionUID = -2324738652359671082L;
	/**
	 * 结算月份
	 */
	private String Month;
	/**
	 * 期初保单账户价值
	 */
	private String StartMoney;
	/**
	 * 追加保费
	 */
	private String BQAddPrem;
	/**
	 * 差额返还
	 */
	private String BRAddPrem;
	/**
	 * 利息
	 */
	private String LXMoney;
	/**
	 * 风险保费
	 */
	private String RiskPrem;
	/**
	 * 期末保单账户价值
	 */
	private String EndMoney;
	
	public String getMonth() {
		return Month;
	}
	public void setMonth(String month) {
		Month = month;
	}
	public String getStartMoney() {
		return StartMoney;
	}
	public void setStartMoney(String startMoney) {
		StartMoney = startMoney;
	}
	public String getBQAddPrem() {
		return BQAddPrem;
	}
	public void setBQAddPrem(String bQAddPrem) {
		BQAddPrem = bQAddPrem;
	}
	public String getBRAddPrem() {
		return BRAddPrem;
	}
	public void setBRAddPrem(String bRAddPrem) {
		BRAddPrem = bRAddPrem;
	}
	public String getLXMoney() {
		return LXMoney;
	}
	public void setLXMoney(String lXMoney) {
		LXMoney = lXMoney;
	}
	public String getRiskPrem() {
		return RiskPrem;
	}
	public void setRiskPrem(String riskPrem) {
		RiskPrem = riskPrem;
	}
	public String getEndMoney() {
		return EndMoney;
	}
	public void setEndMoney(String endMoney) {
		EndMoney = endMoney;
	}
	
	
}
