package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class LjagetTable extends BaseXmlSch{
	
	private static final long serialVersionUID = -4794467791337930965L;

	/**  付费号 */
	private String ActugetNo=null;
	
	/** 付费机构 */
	private String ManageCom=null;
	
	/** 付费方式 */
	private String PayMode=null;
	
	/** 领取人 */
	private String Drawer=null;
	
	/** 领取人ID */
	private String DrawerID=null;
	

	public String getActugetNo() {
		return ActugetNo;
	}

	public String getDrawer() {
		return Drawer;
	}

	public void setDrawer(String drawer) {
		Drawer = drawer;
	}

	public String getDrawerID() {
		return DrawerID;
	}

	public void setDrawerID(String drawerID) {
		DrawerID = drawerID;
	}

	public void setActugetNo(String actugetNo) {
		ActugetNo = actugetNo;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getPayMode() {
		return PayMode;
	}

	public void setPayMode(String payMode) {
		PayMode = payMode;
	}


}
