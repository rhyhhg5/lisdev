/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 网销复杂产品出单产品描述信息
 * 
 * @author GZH
 *
 */
public class WrapParamTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 保单号*/
    private String ContNo = null;

    /** 套餐编码 */
    private String RiskWrapCode = null;

    /** 险种编码 */
    private String RiskCode = null;

    /** 责任编码 */
    private String DutyCode = null;

    /** 计划要素名称 */
    private String Calfactor = null;

    /** 计划要素值 */
    private String CalfactorValue = null;

	/**
	 * @return the calfactor
	 */
	public String getCalfactor() {
		return Calfactor;
	}

	/**
	 * @param calfactor the calfactor to set
	 */
	public void setCalfactor(String calfactor) {
		Calfactor = calfactor;
	}

	/**
	 * @return the calfactorValue
	 */
	public String getCalfactorValue() {
		return CalfactorValue;
	}

	/**
	 * @param calfactorValue the calfactorValue to set
	 */
	public void setCalfactorValue(String calfactorValue) {
		CalfactorValue = calfactorValue;
	}

	/**
	 * @return the contNo
	 */
	public String getContNo() {
		return ContNo;
	}

	/**
	 * @param contNo the contNo to set
	 */
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	/**
	 * @return the dutyCode
	 */
	public String getDutyCode() {
		return DutyCode;
	}

	/**
	 * @param dutyCode the dutyCode to set
	 */
	public void setDutyCode(String dutyCode) {
		DutyCode = dutyCode;
	}

	/**
	 * @return the riskCode
	 */
	public String getRiskCode() {
		return RiskCode;
	}

	/**
	 * @param riskCode the riskCode to set
	 */
	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}

	/**
	 * @return the riskWrapCode
	 */
	public String getRiskWrapCode() {
		return RiskWrapCode;
	}

	/**
	 * @param riskWrapCode the riskWrapCode to set
	 */
	public void setRiskWrapCode(String riskWrapCode) {
		RiskWrapCode = riskWrapCode;
	}
    
    
}
