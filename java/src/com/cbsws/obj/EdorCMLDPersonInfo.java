package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class EdorCMLDPersonInfo extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5133454458333026967L;
	/** 保单号 */
	private String ContNo;
	/** 客户号码 */
	private String CustomerNo;
	/** 客户姓名 */
	private String Name;
	/** 客户性别 */
	private String Sex;
	/** 客户出生日期 */
	private String Birthday;
	/** 客户证件类型 */
	private String IDType;
	/** 客户证件号码 */
	private String IDNo;
	/** 职业类别 */
	private String OccupationCode;
	/** 职业代码 */
	private String OccupationType;
	/** 婚姻状况 */
	private String Marriage;
	/** 国籍 */
	private String NativePlace;
	/** 工资 */
	private String Salary;
	/** 职位 */
	private String Position;
	

	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public String getCustomerNo() {
		return CustomerNo;
	}

	public void setCustomerNo(String customerNo) {
		CustomerNo = customerNo;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getBirthday() {
		return Birthday;
	}

	public void setBirthday(String birthday) {
		Birthday = birthday;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getIDNo() {
		return IDNo;
	}

	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}

	public String getOccupationCode() {
		return OccupationCode;
	}

	public void setOccupationCode(String occupationCode) {
		OccupationCode = occupationCode;
	}

	public String getOccupationType() {
		return OccupationType;
	}

	public void setOccupationType(String occupationType) {
		OccupationType = occupationType;
	}

	public String getMarriage() {
		return Marriage;
	}

	public void setMarriage(String marriage) {
		Marriage = marriage;
	}

	public String getNativePlace() {
		return NativePlace;
	}

	public void setNativePlace(String nativePlace) {
		NativePlace = nativePlace;
	}

	public String getSalary() {
		return Salary;
	}

	public void setSalary(String salary) {
		Salary = salary;
	}

	public String getPosition() {
		return Position;
	}

	public void setPosition(String position) {
		Position = position;
	}

}
