package com.cbsws.obj;

import java.util.Date;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class LSGRPTable extends BaseXmlSch{
	
	 private static final long serialVersionUID = -4794467791337930966L;
	 
	 /** 团体编号 */
		private String GrpNo;
		/** 单位名称 */
		private String GrpName;
		/** 税务登记证号码 */
		private String TaxRegistration;
		/** 组织机构代码 */
		private String OrgancomCode;
		/** 单位地址 */
		private String GrpAddress;
		/** 邮政编码 */
		private String GrpZipcode;
		/** 单位性质 */
		private String GrpNature;
		/** 行业类别 */
		private String BusinessType;
		/** 在职员工总人数 */
		private int Peoples;
		/** 主营业务 */
		private String MainBussiness;
		/** 单位法人代表 */
		private String LeaglePerson;
		/** 单位电话号码 */
		private String Phone;
		/** 业务经办人姓名 */
		private String HandlerName;
		/** 所在部门 */
		private String Department;
		/** 联系电话 */
		private String HandlerPhone;
		/** 开户银行 */
		private String BankCode;
		/** 银行帐户名 */
		private String BankAccName;
		/** 银行账号 */
		private String BankAccNo;
		/** 状态 */
		private String State;
		/** 备注 */
		private String Remark;
		/** 操作员 */
		private String Operator;
		/** 处理日期 */
		private Date MakeDate;
		/** 处理时间 */
		private String MakeTime;
		/** 修改日期 */
		private Date ModifyDate;
		/** 修改时间 */
		private String ModifyTime;
		
		public String getGrpNo() {
			return GrpNo;
		}
		public void setGrpNo(String grpNo) {
			GrpNo = grpNo;
		}
		public String getGrpName() {
			return GrpName;
		}
		public void setGrpName(String grpName) {
			GrpName = grpName;
		}
		public String getTaxRegistration() {
			return TaxRegistration;
		}
		public void setTaxRegistration(String taxRegistration) {
			TaxRegistration = taxRegistration;
		}
		public String getOrgancomCode() {
			return OrgancomCode;
		}
		public void setOrgancomCode(String organcomCode) {
			OrgancomCode = organcomCode;
		}
		public String getGrpAddress() {
			return GrpAddress;
		}
		public void setGrpAddress(String grpAddress) {
			GrpAddress = grpAddress;
		}
		public String getGrpZipcode() {
			return GrpZipcode;
		}
		public void setGrpZipcode(String grpZipcode) {
			GrpZipcode = grpZipcode;
		}
		public String getGrpNature() {
			return GrpNature;
		}
		public void setGrpNature(String grpNature) {
			GrpNature = grpNature;
		}
		public String getBusinessType() {
			return BusinessType;
		}
		public void setBusinessType(String businessType) {
			BusinessType = businessType;
		}
		public int getPeoples() {
			return Peoples;
		}
		public void setPeoples(int peoples) {
			Peoples = peoples;
		}
		public String getMainBussiness() {
			return MainBussiness;
		}
		public void setMainBussiness(String mainBussiness) {
			MainBussiness = mainBussiness;
		}
		public String getLeaglePerson() {
			return LeaglePerson;
		}
		public void setLeaglePerson(String leaglePerson) {
			LeaglePerson = leaglePerson;
		}
		public String getPhone() {
			return Phone;
		}
		public void setPhone(String phone) {
			Phone = phone;
		}
		public String getHandlerName() {
			return HandlerName;
		}
		public void setHandlerName(String handlerName) {
			HandlerName = handlerName;
		}
		public String getDepartment() {
			return Department;
		}
		public void setDepartment(String department) {
			Department = department;
		}
		public String getHandlerPhone() {
			return HandlerPhone;
		}
		public void setHandlerPhone(String handlerPhone) {
			HandlerPhone = handlerPhone;
		}
		public String getBankCode() {
			return BankCode;
		}
		public void setBankCode(String bankCode) {
			BankCode = bankCode;
		}
		public String getBankAccName() {
			return BankAccName;
		}
		public void setBankAccName(String bankAccName) {
			BankAccName = bankAccName;
		}
		public String getBankAccNo() {
			return BankAccNo;
		}
		public void setBankAccNo(String bankAccNo) {
			BankAccNo = bankAccNo;
		}
		public String getState() {
			return State;
		}
		public void setState(String state) {
			State = state;
		}
		public String getRemark() {
			return Remark;
		}
		public void setRemark(String remark) {
			Remark = remark;
		}
		public String getOperator() {
			return Operator;
		}
		public void setOperator(String operator) {
			Operator = operator;
		}
		public Date getMakeDate() {
			return MakeDate;
		}
		public void setMakeDate(Date makeDate) {
			MakeDate = makeDate;
		}
		public String getMakeTime() {
			return MakeTime;
		}
		public void setMakeTime(String makeTime) {
			MakeTime = makeTime;
		}
		public Date getModifyDate() {
			return ModifyDate;
		}
		public void setModifyDate(Date modifyDate) {
			ModifyDate = modifyDate;
		}
		public String getModifyTime() {
			return ModifyTime;
		}
		public void setModifyTime(String modifyTime) {
			ModifyTime = modifyTime;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}

}
