package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class CTList extends BaseXmlSch{

	private static final long serialVersionUID = 6391311180929829389L;
	
	private String ContNo = null;
	private String CTDate = null;
	
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getCTDate() {
		return CTDate;
	}
	public void setCTDate(String cTDate) {
		CTDate = cTDate;
	}
}
