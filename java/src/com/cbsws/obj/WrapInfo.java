/**
 * 2011-9-19
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public class WrapInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -1373501807710120719L;

    /** 保险卡号*/
    private String CardNo = null;

    /** 卡激活密码*/
    private String Password = null;

    /** 套餐代码*/
    private String WrapCode = null;

    /** 套餐名称*/
    private String WrapName = null;

    /** 保险卡类型*/
    private String CertifyCode = null;

    /** 保险卡名称*/
    private String CertifyName = null;

    /** 激活日期*/
    private String ActiveDate = null;

    /** 保险卡保费*/
    private String Prem = null;

    /** 保险卡保额*/
    private String Amnt = null;

    /** 保险卡险种档次*/
    private String Mult = null;

    /** 份数*/
    private String Copys = null;

    /** 说明*/
    private String ReMark = null;

    /**
     * @return activeDate
     */
    public String getActiveDate()
    {
        return ActiveDate;
    }

    /**
     * @param activeDate 要设置的 activeDate
     */
    public void setActiveDate(String activeDate)
    {
        ActiveDate = activeDate;
    }

    /**
     * @return amnt
     */
    public String getAmnt()
    {
        return Amnt;
    }

    /**
     * @param amnt 要设置的 amnt
     */
    public void setAmnt(String amnt)
    {
        Amnt = amnt;
    }

    /**
     * @return cardNo
     */
    public String getCardNo()
    {
        return CardNo;
    }

    /**
     * @param cardNo 要设置的 cardNo
     */
    public void setCardNo(String cardNo)
    {
        CardNo = cardNo;
    }

    /**
     * @return certifyCode
     */
    public String getCertifyCode()
    {
        return CertifyCode;
    }

    /**
     * @param certifyCode 要设置的 certifyCode
     */
    public void setCertifyCode(String certifyCode)
    {
        CertifyCode = certifyCode;
    }

    /**
     * @return certifyName
     */
    public String getCertifyName()
    {
        return CertifyName;
    }

    /**
     * @param certifyName 要设置的 certifyName
     */
    public void setCertifyName(String certifyName)
    {
        CertifyName = certifyName;
    }

    /**
     * @return copys
     */
    public String getCopys()
    {
        return Copys;
    }

    /**
     * @param copys 要设置的 copys
     */
    public void setCopys(String copys)
    {
        Copys = copys;
    }

    /**
     * @return mult
     */
    public String getMult()
    {
        return Mult;
    }

    /**
     * @param mult 要设置的 mult
     */
    public void setMult(String mult)
    {
        Mult = mult;
    }

    /**
     * @return password
     */
    public String getPassword()
    {
        return Password;
    }

    /**
     * @param password 要设置的 password
     */
    public void setPassword(String password)
    {
        Password = password;
    }

    /**
     * @return prem
     */
    public String getPrem()
    {
        return Prem;
    }

    /**
     * @param prem 要设置的 prem
     */
    public void setPrem(String prem)
    {
        Prem = prem;
    }

    /**
     * @return reMark
     */
    public String getReMark()
    {
        return ReMark;
    }

    /**
     * @param reMark 要设置的 reMark
     */
    public void setReMark(String reMark)
    {
        ReMark = reMark;
    }

    /**
     * @return wrapCode
     */
    public String getWrapCode()
    {
        return WrapCode;
    }

    /**
     * @param wrapCode 要设置的 wrapCode
     */
    public void setWrapCode(String wrapCode)
    {
        WrapCode = wrapCode;
    }

    /**
     * @return wrapName
     */
    public String getWrapName()
    {
        return WrapName;
    }

    /**
     * @param wrapName 要设置的 wrapName
     */
    public void setWrapName(String wrapName)
    {
        WrapName = wrapName;
    }

}
