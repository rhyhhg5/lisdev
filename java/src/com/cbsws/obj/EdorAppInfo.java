package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 保全工单信息
 * @author wujun
 *
 */
public class EdorAppInfo extends BaseXmlSch {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5266460721977795702L;

	/**
	 * 保全工单号
	 */
	private String EdorNo = "";
	
	/**
	 * 工单的申请人类型
	 */
	private String ApplyTypeNo = "";
	
	/**
	 * 工单的申请人姓名
	 */
	private String ApplyName = "";
	
	/**
	 * 工单的申请客户号
	 */
	private String CustomerNo = "";
	
	private String WorkType = "";
	
	/**
	 * 工单子业务类型
	 */
	private String TypeNo = "";
	
	/**
	 * 受理途径
	 */
	private String AcceptWayNo = "";
	
	/**
	 * 工单申请时间
	 */
	private String AcceptDate = "";

	public String getEdorNo() {
		return EdorNo;
	}

	public void setEdorNo(String edorno) {
		EdorNo = edorno;
	}

	public String getApplyTypeNo() {
		return ApplyTypeNo;
	}

	public void setApplyTypeNo(String applyTypeNo) {
		ApplyTypeNo = applyTypeNo;
	}

	public String getApplyName() {
		return ApplyName;
	}

	public void setApplyName(String applyName) {
		ApplyName = applyName;
	}

	public String getCustomerNo() {
		return CustomerNo;
	}

	public void setCustomerNo(String customerNo) {
		CustomerNo = customerNo;
	}

	public String getWorkType() {
		return WorkType;
	}

	public void setWorkType(String workType) {
		WorkType = workType;
	}

	public String getTypeNo() {
		return TypeNo;
	}

	public void setTypeNo(String typeNo) {
		TypeNo = typeNo;
	}

	public String getAcceptWayNo() {
		return AcceptWayNo;
	}

	public void setAcceptWayNo(String acceptWayNo) {
		AcceptWayNo = acceptWayNo;
	}

	public String getAcceptDate() {
		return AcceptDate;
	}

	public void setAcceptDate(String acceptDate) {
		AcceptDate = acceptDate;
	}

	

}
