package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SHGrpInfo extends BaseXmlSch{
	
	private static final long serialVersionUID = -4794467791337930966L;
	private String GrpNo;
	private String StartDate;
	private String endDate;
	
	public String getGrpNo() {
		return GrpNo;
	}
	public void setGrpNo(String grpNo) {
		GrpNo = grpNo;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
	
	
	
}
