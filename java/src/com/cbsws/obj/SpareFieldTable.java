/**
 * 2018-03-21
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;
/**
 * 复杂产品保单信息备用字段
 * 
 * @author GMH
 *
 */
public class SpareFieldTable extends BaseXmlSch{
	
	private static final long serialVersionUID = -4794467791337930966L;
	/** 投保人联系省份*/
	private String appntPostalProvince = null;
	
	/** 投保人联系市*/
	private String appntPostalCity = null;
	
	/** 投保人联系县*/
	private String appntPostalCounty = null;
	
	/** 投保人联系乡镇*/
	private String appntPostalStreet = null;
	
	/** 投保人联系村*/
	private String appntPostalCommunity = null;
	
	/** 被保人联系省*/
	private String insuredPostalProvince = null;
	
	/** 被保人联系市*/
	private String insuredPostalCity = null;
	
	/** 被保人联系县*/
	private String insuredPostalCounty = null;
	
	/** 被保人联系乡镇*/
	private String insuredPostalStreet = null;
	
	/** 被保人联系村*/
	private String insuredPostalCommunity = null;

	public String getAppntPostalProvince() {
		return appntPostalProvince;
	}

	public void setAppntPostalProvince(String appntPostalProvince) {
		this.appntPostalProvince = appntPostalProvince;
	}

	public String getAppntPostalCity() {
		return appntPostalCity;
	}

	public void setAppntPostalCity(String appntPostalCity) {
		this.appntPostalCity = appntPostalCity;
	}

	public String getAppntPostalCounty() {
		return appntPostalCounty;
	}

	public void setAppntPostalCounty(String appntPostalCounty) {
		this.appntPostalCounty = appntPostalCounty;
	}

	public String getAppntPostalStreet() {
		return appntPostalStreet;
	}

	public void setAppntPostalStreet(String appntPostalStreet) {
		this.appntPostalStreet = appntPostalStreet;
	}

	public String getAppntPostalCommunity() {
		return appntPostalCommunity;
	}

	public void setAppntPostalCommunity(String appntPostalCommunity) {
		this.appntPostalCommunity = appntPostalCommunity;
	}

	public String getInsuredPostalProvince() {
		return insuredPostalProvince;
	}

	public void setInsuredPostalProvince(String insuredPostalProvince) {
		this.insuredPostalProvince = insuredPostalProvince;
	}

	public String getInsuredPostalCity() {
		return insuredPostalCity;
	}

	public void setInsuredPostalCity(String insuredPostalCity) {
		this.insuredPostalCity = insuredPostalCity;
	}

	public String getInsuredPostalCounty() {
		return insuredPostalCounty;
	}

	public void setInsuredPostalCounty(String insuredPostalCounty) {
		this.insuredPostalCounty = insuredPostalCounty;
	}

	public String getInsuredPostalStreet() {
		return insuredPostalStreet;
	}

	public void setInsuredPostalStreet(String insuredPostalStreet) {
		this.insuredPostalStreet = insuredPostalStreet;
	}

	public String getInsuredPostalCommunity() {
		return insuredPostalCommunity;
	}

	public void setInsuredPostalCommunity(String insuredPostalCommunity) {
		this.insuredPostalCommunity = insuredPostalCommunity;
	}

}
