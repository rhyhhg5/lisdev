package com.cbsws.picchPub.obj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class ResponseHead  extends PiccBaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3329878354455579438L;
	
	  private String  Request_Type;
	  private String Uuid;
	  private String Response_Code;
	  private String Error_Message;
	  private String TimeStamp;
	public String getRequest_Type() {
		return Request_Type;
	}
	public void setRequest_Type(String requestType) {
		Request_Type = requestType;
	}
	public String getUuid() {
		return Uuid;
	}
	public void setUuid(String uuid) {
		Uuid = uuid;
	}
	public String getResponse_Code() {
		return Response_Code;
	}
	public void setResponse_Code(String responseCode) {
		Response_Code = responseCode;
	}
	public String getError_Message() {
		return Error_Message;
	}
	public void setError_Message(String errorMessage) {
		Error_Message = errorMessage;
	}
	public String getTimeStamp() {
		return TimeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		TimeStamp = timeStamp;
	}
	
}
