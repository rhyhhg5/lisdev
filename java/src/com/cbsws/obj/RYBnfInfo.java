/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class RYBnfInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String ToInsuNo;
    private String RName;
    private String ToInsuRela;
    private String BnfGrade;
    private String BnfRate;
    private String BnfName;
    private String BnfIdtype;
    private String BnfIdno;
	public String getToInsuNo() {
		return ToInsuNo;
	}
	public void setToInsuNo(String toInsuNo) {
		ToInsuNo = toInsuNo;
	}
	public String getRName() {
		return RName;
	}
	public void setRName(String rName) {
		RName = rName;
	}
	public String getToInsuRela() {
		return ToInsuRela;
	}
	public void setToInsuRela(String toInsuRela) {
		ToInsuRela = toInsuRela;
	}
	public String getBnfGrade() {
		return BnfGrade;
	}
	public void setBnfGrade(String bnfGrade) {
		BnfGrade = bnfGrade;
	}
	public String getBnfRate() {
		return BnfRate;
	}
	public void setBnfRate(String bnfRate) {
		BnfRate = bnfRate;
	}
	public String getBnfName() {
		return BnfName;
	}
	public void setBnfName(String bnfName) {
		BnfName = bnfName;
	}
	public String getBnfIdtype() {
		return BnfIdtype;
	}
	public void setBnfIdtype(String bnfIdtype) {
		BnfIdtype = bnfIdtype;
	}
	public String getBnfIdno() {
		return BnfIdno;
	}
	public void setBnfIdno(String bnfIdno) {
		BnfIdno = bnfIdno;
	}
   
    
    
	
    
    
    
}
