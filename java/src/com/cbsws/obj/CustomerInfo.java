package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class CustomerInfo extends BaseXmlSch{


	private static final long serialVersionUID = -1906188671483055795L;
	
	/**
	 * 姓名
	 */
	private String Name;
	
	/**
	 * 性别
	 */
	private String Sex;
	
	/**
	 * 出生日期
	 */
	private String Birthday;
	
	/**
	 * 证件类型
	 */
	private String IDType;
	
	/**
	 * 证件号码
	 */
	private String IDNo;

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String Sex) {
		this.Sex = Sex;
	}

	public String getBirthday() {
		return Birthday;
	}

	public void setBirthday(String Birthday) {
		this.Birthday = Birthday;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getIDNo() {
		return IDNo;
	}

	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}
	
}
