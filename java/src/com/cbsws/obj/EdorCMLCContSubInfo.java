package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class EdorCMLCContSubInfo extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9193081637825646888L;
	/** 印刷号 */
	private String PrtNo;
	/** 个税征收方式 */
	private String TaxPayerType;
	/** 个人税务登记证代码 */
	private String TaxNo;
	/** 个人社会信用代码 */
	private String CreditCode;
	/** 单位税务登记证代码 */
	private String GTaxNo;
	/** 单位社会信用代码 */
	private String GOrgancomCode;

	public String getPrtNo() {
		return PrtNo;
	}

	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	public String getTaxPayerType() {
		return TaxPayerType;
	}

	public void setTaxPayerType(String taxPayerType) {
		TaxPayerType = taxPayerType;
	}

	public String getTaxNo() {
		return TaxNo;
	}

	public void setTaxNo(String taxNo) {
		TaxNo = taxNo;
	}

	public String getCreditCode() {
		return CreditCode;
	}

	public void setCreditCode(String creditCode) {
		this.CreditCode = creditCode;
	}

	public String getGTaxNo() {
		return GTaxNo;
	}

	public void setGTaxNo(String gTaxNo) {
		GTaxNo = gTaxNo;
	}

	public String getGOrgancomCode() {
		return GOrgancomCode;
	}

	public void setGOrgancomCode(String gOrgancomCode) {
		GOrgancomCode = gOrgancomCode;
	}

}
