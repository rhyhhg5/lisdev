/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class SHOtherInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String ReturnVistInfo;
    private String ContNo;
    private String ManageCom;
    private String SaleComName;
    private String SaleComAddress;
    private String AgentName;
    private String AgentTelePhone;
    private String CvaliDate;
    private String SignDate;
    private String CustomGetPolDateString;
	private String GetPolDate;
	private String contState;
	private String payLocation;
	private String isAutoPay;
	public String getReturnVistInfo() {
		return ReturnVistInfo;
	}
	public void setReturnVistInfo(String returnVistInfo) {
		ReturnVistInfo = returnVistInfo;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getManageCom() {
		return ManageCom;
	}
	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}
	public String getSaleComName() {
		return SaleComName;
	}
	public void setSaleComName(String saleComName) {
		SaleComName = saleComName;
	}
	public String getSaleComAddress() {
		return SaleComAddress;
	}
	public void setSaleComAddress(String saleComAddress) {
		SaleComAddress = saleComAddress;
	}
	public String getAgentName() {
		return AgentName;
	}
	public void setAgentName(String agentName) {
		AgentName = agentName;
	}
	public String getAgentTelePhone() {
		return AgentTelePhone;
	}
	public void setAgentTelePhone(String agentTelePhone) {
		AgentTelePhone = agentTelePhone;
	}
	public String getCvaliDate() {
		return CvaliDate;
	}
	public void setCvaliDate(String cvaliDate) {
		CvaliDate = cvaliDate;
	}
	public String getSignDate() {
		return SignDate;
	}
	public void setSignDate(String signDate) {
		SignDate = signDate;
	}
	public String getCustomGetPolDateString() {
		return CustomGetPolDateString;
	}
	public void setCustomGetPolDateString(String customGetPolDateString) {
		CustomGetPolDateString = customGetPolDateString;
	}
	public String getGetPolDate() {
		return GetPolDate;
	}
	public void setGetPolDate(String getPolDate) {
		GetPolDate = getPolDate;
	}
	public String getContState() {
		return contState;
	}
	public void setContState(String contState) {
		this.contState = contState;
	}
	public String getPayLocation() {
		return payLocation;
	}
	public void setPayLocation(String payLocation) {
		this.payLocation = payLocation;
	}
	public String getIsAutoPay() {
		return isAutoPay;
	}
	public void setIsAutoPay(String isAutoPay) {
		this.isAutoPay = isAutoPay;
	}
	
    
    
    
}
