package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class ResultInsuredInfo extends BaseXmlSch {
	/*分单号*/
	private String ContNo;
	/*被保人号*/
	private String InsuredNo;
	/*被保人姓名*/
	private String Name;
	/*证件类型*/
	private String IDType;
	/*证件号*/
	private String IDNo;
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getInsuredNo() {
		return InsuredNo;
	}
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getIDType() {
		return IDType;
	}
	public void setIDType(String iDType) {
		IDType = iDType;
	}
	public String getIDNo() {
		return IDNo;
	}
	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}
}
