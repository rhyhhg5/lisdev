package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class CrsSaleInfo extends BaseXmlSch{
	private static final long serialVersionUID = -4794467791337930966L;
	/** 交叉销售渠道 */
    private String Crs_SaleChnl = null;
    /** 销售业务类型 */
    private String Crs_BussType = null;
    /** 代理机构 */
    private String GrpAgentCom = null;
    /** 代理人编码 */
    private String GrpAgentCode = null;
    /** 代理人姓名 */
    private String GrpAgentName = null;
    /** 代理人身份证  */
    private String GrpAgentIDNo = null;
    /** 渠道来源  */
    private String OriginChnl = null;
    
	/**
	 * @return the crs_SaleChnl
	 */
	public String getCrs_SaleChnl() {
		return Crs_SaleChnl;
	}
	/**
	 * @param crs_SaleChnl the crs_SaleChnl to set
	 */
	public void setCrs_SaleChnl(String crs_SaleChnl) {
		Crs_SaleChnl = crs_SaleChnl;
	}
	/**
	 * @return the crs_BussType
	 */
	public String getCrs_BussType() {
		return Crs_BussType;
	}
	/**
	 * @param crs_BussType the crs_BussType to set
	 */
	public void setCrs_BussType(String crs_BussType) {
		Crs_BussType = crs_BussType;
	}
	/**
	 * @return the grpAgentCom
	 */
	public String getGrpAgentCom() {
		return GrpAgentCom;
	}
	/**
	 * @param grpAgentCom the grpAgentCom to set
	 */
	public void setGrpAgentCom(String grpAgentCom) {
		GrpAgentCom = grpAgentCom;
	}
	/**
	 * @return the grpAgentCode
	 */
	public String getGrpAgentCode() {
		return GrpAgentCode;
	}
	/**
	 * @param grpAgentCode the grpAgentCode to set
	 */
	public void setGrpAgentCode(String grpAgentCode) {
		GrpAgentCode = grpAgentCode;
	}
	/**
	 * @return the grpAgentName
	 */
	public String getGrpAgentName() {
		return GrpAgentName;
	}
	/**
	 * @param grpAgentName the grpAgentName to set
	 */
	public void setGrpAgentName(String grpAgentName) {
		GrpAgentName = grpAgentName;
	}
	/**
	 * @return the grpAgentIDNo
	 */
	public String getGrpAgentIDNo() {
		return GrpAgentIDNo;
	}
	/**
	 * @param grpAgentIDNo the grpAgentIDNo to set
	 */
	public void setGrpAgentIDNo(String grpAgentIDNo) {
		GrpAgentIDNo = grpAgentIDNo;
	}
	/**
	 * @return the originChnl
	 */
	public String getOriginChnl() {
		return OriginChnl;
	}
	/**
	 * @param originChnl the originChnl to set
	 */
	public void setOriginChnl(String originChnl) {
		OriginChnl = originChnl;
	}
}
