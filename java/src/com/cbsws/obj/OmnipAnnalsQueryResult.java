package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class OmnipAnnalsQueryResult extends BaseXmlSch{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6490200765875785936L;
	
	private String AgentName;
	private String AgentCode;
	private String AgentPhone;
	private String ManageComLength4;
	private String AgentGroup;
	private String AppntName;
	private String ServicePhone;
	private String letterservicename;
	private String ServiceAddress;
	private String ServiceZip;
	private String ComName;
	private String PrintDate;
	private String Fax;
	private String GrpZipCode;
	private String GrpAddress;
	private String ContNo;
	private String InsuredName;
	private String startDate;
	private String endDate;
	private String RiskCodeName;
	private String CValiDate;
	private String Amnt;
	private String Prem;
	private String PayPrem;
	private String OtherPrem;
	private String DunPrem;
	private String AddPrem;
	private String TotalPrem;
	private String Fee;
	private String BMoney;
	private String Year1;
	private String Quarter1;
	private String Solvency;
	private String Flag;
	private String Year2;
	private String Quarter2;
	private String RiksRate;
	
	public String getYear1() {
		return Year1;
	}
	public void setYear1(String year1) {
		Year1 = year1;
	}
	public String getQuarter1() {
		return Quarter1;
	}
	public void setQuarter1(String quarter1) {
		Quarter1 = quarter1;
	}
	public String getSolvency() {
		return Solvency;
	}
	public void setSolvency(String solvency) {
		Solvency = solvency;
	}
	public String getFlag() {
		return Flag;
	}
	public void setFlag(String flag) {
		Flag = flag;
	}
	public String getYear2() {
		return Year2;
	}
	public void setYear2(String year2) {
		Year2 = year2;
	}
	public String getQuarter2() {
		return Quarter2;
	}
	public void setQuarter2(String quarter2) {
		Quarter2 = quarter2;
	}
	public String getRiksRate() {
		return RiksRate;
	}
	public void setRiksRate(String riksRate) {
		RiksRate = riksRate;
	}
	public String getAgentName() {
		return AgentName;
	}
	public void setAgentName(String agentName) {
		AgentName = agentName;
	}
	public String getAgentCode() {
		return AgentCode;
	}
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}
	public String getAgentPhone() {
		return AgentPhone;
	}
	public void setAgentPhone(String agentPhone) {
		AgentPhone = agentPhone;
	}

	public String getManageComLength4() {
		return ManageComLength4;
	}
	public void setManageComLength4(String manageComLength4) {
		ManageComLength4 = manageComLength4;
	}
	public String getAgentGroup() {
		return AgentGroup;
	}
	public void setAgentGroup(String agentGroup) {
		AgentGroup = agentGroup;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getServicePhone() {
		return ServicePhone;
	}
	public void setServicePhone(String servicePhone) {
		ServicePhone = servicePhone;
	}
	public String getLetterservicename() {
		return letterservicename;
	}
	public void setLetterservicename(String letterservicename) {
		this.letterservicename = letterservicename;
	}
	public String getServiceAddress() {
		return ServiceAddress;
	}
	public void setServiceAddress(String serviceAddress) {
		ServiceAddress = serviceAddress;
	}
	public String getServiceZip() {
		return ServiceZip;
	}
	public void setServiceZip(String serviceZip) {
		ServiceZip = serviceZip;
	}
	public String getComName() {
		return ComName;
	}
	public void setComName(String comName) {
		ComName = comName;
	}
	public String getPrintDate() {
		return PrintDate;
	}
	public void setPrintDate(String printDate) {
		PrintDate = printDate;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getGrpZipCode() {
		return GrpZipCode;
	}
	public void setGrpZipCode(String grpZipCode) {
		GrpZipCode = grpZipCode;
	}
	public String getGrpAddress() {
		return GrpAddress;
	}
	public void setGrpAddress(String grpAddress) {
		GrpAddress = grpAddress;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getRiskCodeName() {
		return RiskCodeName;
	}
	public void setRiskCodeName(String riskCodeName) {
		RiskCodeName = riskCodeName;
	}
	public String getCValiDate() {
		return CValiDate;
	}
	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}
	public String getAmnt() {
		return Amnt;
	}
	public void setAmnt(String amnt) {
		Amnt = amnt;
	}
	public String getPrem() {
		return Prem;
	}
	public void setPrem(String prem) {
		Prem = prem;
	}
	public String getPayPrem() {
		return PayPrem;
	}
	public void setPayPrem(String payPrem) {
		PayPrem = payPrem;
	}
	public String getOtherPrem() {
		return OtherPrem;
	}
	public void setOtherPrem(String otherPrem) {
		OtherPrem = otherPrem;
	}
	public String getDunPrem() {
		return DunPrem;
	}
	public void setDunPrem(String dunPrem) {
		DunPrem = dunPrem;
	}
	public String getAddPrem() {
		return AddPrem;
	}
	public void setAddPrem(String addPrem) {
		AddPrem = addPrem;
	}
	public String getTotalPrem() {
		return TotalPrem;
	}
	public void setTotalPrem(String totalPrem) {
		TotalPrem = totalPrem;
	}
	public String getFee() {
		return Fee;
	}
	public void setFee(String fee) {
		Fee = fee;
	}
	public String getBMoney() {
		return BMoney;
	}
	public void setBMoney(String bMoney) {
		BMoney = bMoney;
	}
	
    
}
