package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class SHLCCustomerImpartTable extends BaseXmlSch {
	
	private static final long serialVersionUID = -7305678040867973031L;

	/** 被保人客户号码 */
    private String InsuredNo = null;

    /** 告知内容*/
    private String ImpartContent = null;

    /** 告知结果 */
    private String ImpartParamModle = null;

	/**
	 * @return the impartContent
	 */
	public String getImpartContent() {
		return ImpartContent;
	}

	/**
	 * @param impartContent the impartContent to set
	 */
	public void setImpartContent(String impartContent) {
		ImpartContent = impartContent;
	}

	/**
	 * @return the impartParamModle
	 */
	public String getImpartParamModle() {
		return ImpartParamModle;
	}

	/**
	 * @param impartParamModle the impartParamModle to set
	 */
	public void setImpartParamModle(String impartParamModle) {
		ImpartParamModle = impartParamModle;
	}

	/**
	 * @return the insuredNo
	 */
	public String getInsuredNo() {
		return InsuredNo;
	}

	/**
	 * @param insuredNo the insuredNo to set
	 */
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}
    
    
}
