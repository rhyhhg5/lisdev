/**
 * 2011-9-19
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public class SimBnfInfo extends BaseXmlSch
{
    private static final long serialVersionUID = 7493159269391739053L;

    /** 受益人所属被保人编号*/
    private String ToInsuNo = null;

    /** 受益人类别：0 - 生存受益人；1 - 死亡受益人 */
    private String BnfType = null;

    /** 受益人级别：1 - 第一类受益人；2 - 第二类受益人；以此类推 */
    private String BnfGrade = null;

    /** 受益比例，范围在0-1之间：0.1 - 10%；0.2 - 20%；以此类推，直到 1 - 100% */
    private String BnfRate = null;

    /** 受益人姓名*/
    private String Name = null;

    /** 性别*/
    private String Sex = null;

    /** 出生日期*/
    private String Birthday = null;

    /** 证件类型*/
    private String IDType = null;

    /** 证件号码*/
    private String IdNo = null;

    /**
     * @return birthday
     */
    public String getBirthday()
    {
        return Birthday;
    }

    /**
     * @param birthday 要设置的 birthday
     */
    public void setBirthday(String birthday)
    {
        Birthday = birthday;
    }

    /**
     * @return bnfGrade
     */
    public String getBnfGrade()
    {
        return BnfGrade;
    }

    /**
     * @param bnfGrade 要设置的 bnfGrade
     */
    public void setBnfGrade(String bnfGrade)
    {
        BnfGrade = bnfGrade;
    }

    /**
     * @return bnfRate
     */
    public String getBnfRate()
    {
        return BnfRate;
    }

    /**
     * @param bnfRate 要设置的 bnfRate
     */
    public void setBnfRate(String bnfRate)
    {
        BnfRate = bnfRate;
    }

    /**
     * @return bnfType
     */
    public String getBnfType()
    {
        return BnfType;
    }

    /**
     * @param bnfType 要设置的 bnfType
     */
    public void setBnfType(String bnfType)
    {
        BnfType = bnfType;
    }

    /**
     * @return idNo
     */
    public String getIdNo()
    {
        return IdNo;
    }

    /**
     * @param idNo 要设置的 idNo
     */
    public void setIdNo(String idNo)
    {
        IdNo = idNo;
    }

    /**
     * @return iDType
     */
    public String getIDType()
    {
        return IDType;
    }

    /**
     * @param type 要设置的 iDType
     */
    public void setIDType(String type)
    {
        IDType = type;
    }

    /**
     * @return name
     */
    public String getName()
    {
        return Name;
    }

    /**
     * @param name 要设置的 name
     */
    public void setName(String name)
    {
        Name = name;
    }

    /**
     * @return sex
     */
    public String getSex()
    {
        return Sex;
    }

    /**
     * @param sex 要设置的 sex
     */
    public void setSex(String sex)
    {
        Sex = sex;
    }

    /**
     * @return toInsuNo
     */
    public String getToInsuNo()
    {
        return ToInsuNo;
    }

    /**
     * @param toInsuNo 要设置的 toInsuNo
     */
    public void setToInsuNo(String toInsuNo)
    {
        ToInsuNo = toInsuNo;
    }

}
