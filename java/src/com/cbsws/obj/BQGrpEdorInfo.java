package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/*
 *create by lihuaiyv in 2018-07 
 */
public class BQGrpEdorInfo extends BaseXmlSch  {
	/*保单号*/
	private String GrpContNo = "";
	/*保全申请日期*/
	private String AcceptDate = "";
	/*保全项目类型，增人：NI，减人：ZT*/
	private String EdorType = "";
	/*保全生效日期*/
	private String EdorValidate = "";
	/*工单受理途径*/
	private String AcceptWayNo = "";
	
	public BQGrpEdorInfo() {}
	
	public String getGrpContNo() {
		return GrpContNo;
	}
	public void setGrpContNo(String grpContNo) {
		GrpContNo = grpContNo;
	}
	public String getAcceptDate() {
		return AcceptDate;
	}
	public void setAcceptDate(String acceptDate) {
		AcceptDate = acceptDate;
	}
	public String getEdorType() {
		return EdorType;
	}
	public void setEdorType(String edorType) {
		EdorType = edorType;
	}
	public String getEdorValidate() {
		return EdorValidate;
	}
	public void setEdorValidate(String edorValidate) {
		EdorValidate = edorValidate;
	}
	public String getAcceptWayNo() {
		return AcceptWayNo;
	}
	public void setAcceptWayNo(String acceptWayNo) {
		AcceptWayNo = acceptWayNo;
	}
}
