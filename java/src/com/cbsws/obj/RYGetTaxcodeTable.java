/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author zxs
 *
 */
public class RYGetTaxcodeTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;
    private String ContNo;
    private String Taxcode;
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getTaxcode() {
		return Taxcode;
	}
	public void setTaxcode(String taxcode) {
		Taxcode = taxcode;
	}
}
