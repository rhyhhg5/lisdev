/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class LCGetTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 给付责任编码 */
    private String GetDutyCode = null;

    /** 给付责任名称 */
    private String GetDutyName = null;
    
    /** 实际给付金额 */
    private String ActuGet = null;
    
    /** 险种保费 */
    private String RiskPrem = null;

	public String getActuGet() {
		return ActuGet;
	}

	public void setActuGet(String actuGet) {
		ActuGet = actuGet;
	}

	public String getGetDutyCode() {
		return GetDutyCode;
	}

	public void setGetDutyCode(String getDutyCode) {
		GetDutyCode = getDutyCode;
	}

	public String getGetDutyName() {
		return GetDutyName;
	}

	public void setGetDutyName(String getDutyName) {
		GetDutyName = getDutyName;
	}

	public String getRiskPrem() {
		return RiskPrem;
	}

	public void setRiskPrem(String riskPrem) {
		RiskPrem = riskPrem;
	}
    

    
}
