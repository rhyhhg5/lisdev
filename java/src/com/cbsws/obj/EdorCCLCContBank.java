package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class EdorCCLCContBank  extends BaseXmlSch{

	private static final long serialVersionUID = -5123454458995026967L;
	

    /** 保单号*/
    private String ContNo = null;
    /** 缴费方式 */
    private String PayMode = null;
    
    /** 银行帐号 */
    private String AppntBankAccNo = null;

    /** 银行帐户名 */
    private String AppntAccName = null;

    /** 银行名称 */
    private String AppntBankCode = null;

	public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public String getPayMode() {
		return PayMode;
	}

	public void setPayMode(String payMode) {
		PayMode = payMode;
	}

	public String getAppntBankAccNo() {
		return AppntBankAccNo;
	}

	public void setAppntBankAccNo(String appntBankAccNo) {
		AppntBankAccNo = appntBankAccNo;
	}

	public String getAppntAccName() {
		return AppntAccName;
	}

	public void setAppntAccName(String appntAccName) {
		AppntAccName = appntAccName;
	}

	public String getAppntBankCode() {
		return AppntBankCode;
	}

	public void setAppntBankCode(String appntBankCode) {
		AppntBankCode = appntBankCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
}
