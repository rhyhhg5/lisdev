/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class NewLCContTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 印刷号 */
    private String PrtNo = null;

    /** 保单号*/
    private String ContNo = null;

    /** 管理机构 */
    private String ManageCom = null;

    /** 销售渠道 */
    private String SaleChnl = null;

    /** 销售机构 */
    private String AgentCom = null;

    /** 销售人员 */
    private String AgentCode = null;

    /** 保险生效日期 */
    private String CValiDate = null;
    
    /** 保险生效时间 */
    private String CValiTime = null;
    
    /** 保险失效日期 */
    private String CInValiDate = null;
    
    /** 保险失效时间 */
    private String CInValiTime = null;
    
    /** 缴费频次（趸缴、年缴、季缴、月缴） */
    private String PayIntv = null;
    
    /** 缴费方式 */
    private String PayMode = null;
    /** 投保单申请日期 */
    private String PolApplyDate = null;

	
	public String getPolApplyDate() {
		return PolApplyDate;
	}

	/**
	 * @param polApplyDate the polApplyDate to set
	 */
	public void setPolApplyDate(String polApplyDate) {
		PolApplyDate = polApplyDate;
	}

	/**
	 * @return the payMode
	 */
	public String getPayMode() {
		return PayMode;
	}

	/**
	 * @param payMode the payMode to set
	 */
	public void setPayMode(String payMode) {
		PayMode = payMode;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return AgentCode;
	}

	/**
	 * @param agentCode the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	/**
	 * @return the agentCom
	 */
	public String getAgentCom() {
		return AgentCom;
	}

	/**
	 * @param agentCom the agentCom to set
	 */
	public void setAgentCom(String agentCom) {
		AgentCom = agentCom;
	}

	/**
	 * @return the contNo
	 */
	public String getContNo() {
		return ContNo;
	}

	/**
	 * @param contNo the contNo to set
	 */
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	/**
	 * @return the cValiDate
	 */
	public String getCValiDate() {
		return CValiDate;
	}

	/**
	 * @param valiDate the cValiDate to set
	 */
	public void setCValiDate(String valiDate) {
		CValiDate = valiDate;
	}

	/**
	 * @return the mangeCom
	 */
	public String getManageCom() {
		return ManageCom;
	}

	/**
	 * @param mangeCom the mangeCom to set
	 */
	public void setManageCom(String mangeCom) {
		ManageCom = mangeCom;
	}

	/**
	 * @return the payIntv
	 */
	public String getPayIntv() {
		return PayIntv;
	}

	/**
	 * @param payIntv the payIntv to set
	 */
	public void setPayIntv(String payIntv) {
		PayIntv = payIntv;
	}

	/**
	 * @return the prtNo
	 */
	public String getPrtNo() {
		return PrtNo;
	}

	/**
	 * @param prtNo the prtNo to set
	 */
	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	/**
	 * @return the saleChnl
	 */
	public String getSaleChnl() {
		return SaleChnl;
	}

	/**
	 * @param saleChnl the saleChnl to set
	 */
	public void setSaleChnl(String saleChnl) {
		SaleChnl = saleChnl;
	}

	/**
	 * @return the cInValiDate
	 */
	public String getCInValiDate() {
		return CInValiDate;
	}

	/**
	 * @param inValiDate the cInValiDate to set
	 */
	public void setCInValiDate(String inValiDate) {
		CInValiDate = inValiDate;
	}

	/**
	 * @return the cInValiTime
	 */
	public String getCInValiTime() {
		return CInValiTime;
	}

	/**
	 * @param inValiTime the cInValiTime to set
	 */
	public void setCInValiTime(String inValiTime) {
		CInValiTime = inValiTime;
	}

	/**
	 * @return the cValiTime
	 */
	public String getCValiTime() {
		return CValiTime;
	}

	/**
	 * @param valiTime the cValiTime to set
	 */
	public void setCValiTime(String valiTime) {
		CValiTime = valiTime;
	}
}
