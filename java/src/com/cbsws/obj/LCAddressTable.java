/**
 * 2011-04-11
 */
package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author GZH
 *
 */
public class LCAddressTable extends BaseXmlSch
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5972094458995026967L;
	/** 保单号*/
    private String ContNo = null;
    /** 投保人序号 */
    private String AppntNo = null;
    /** 投保人姓名 */
    private String AppntName = null;
    /** 性别 */
    private String AppntSex = null;
    /** 出生日期 */
    private String AppntBirthday = null;
    /** 证件类型 */
    private String AppntIDType = null;
    /** 证件号码 */
    private String AppntIDNo = null;   
	/** 家庭地址 */
	private String HomeAddress;
	/** 家庭邮编 */
	private String HomeZipCode;
	/** 家庭电话 */
	private String HomePhone;
	/** 家庭传真 */
	private String HomeFax;
	/** 单位地址 */
	private String CompanyAddress;
	/** 单位邮编 */
	private String CompanyZipCode;
	/** 单位电话 */
	private String CompanyPhone;
	/** 单位传真 */
	private String CompanyFax;
	/** 通讯地址 */
	private String PostalAddress;
	/** 通讯邮编 */
	private String ZipCode;
	/** 通讯电话 */
	private String Phone;
	/** 通讯传真 */
	private String Fax;
	/** 联系省份 */
	private String PostalProvince;
	/** 联系市 */
	private String PostalCity;
	/** 联系县 */
	private String PostalCounty;
	/** 联系乡镇 */
	private String PostalStreet;
	/** 联系村（社区） */
	private String PostalCommunity;
	/** 手机 */
	private String Mobile;
	/** E_mail */
	private String EMail;
	
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getAppntNo() {
		return AppntNo;
	}
	public void setAppntNo(String appntNo) {
		AppntNo = appntNo;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getAppntSex() {
		return AppntSex;
	}
	public void setAppntSex(String appntSex) {
		AppntSex = appntSex;
	}
	public String getAppntBirthday() {
		return AppntBirthday;
	}
	public void setAppntBirthday(String appntBirthday) {
		AppntBirthday = appntBirthday;
	}
	public String getAppntIDType() {
		return AppntIDType;
	}
	public void setAppntIDType(String appntIDType) {
		AppntIDType = appntIDType;
	}
	public String getAppntIDNo() {
		return AppntIDNo;
	}
	public void setAppntIDNo(String appntIDNo) {
		AppntIDNo = appntIDNo;
	}
	public String getHomeAddress() {
		return HomeAddress;
	}
	public void setHomeAddress(String homeAddress) {
		HomeAddress = homeAddress;
	}
	public String getHomeZipCode() {
		return HomeZipCode;
	}
	public void setHomeZipCode(String homeZipCode) {
		HomeZipCode = homeZipCode;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
	public String getHomeFax() {
		return HomeFax;
	}
	public void setHomeFax(String homeFax) {
		HomeFax = homeFax;
	}
	public String getCompanyAddress() {
		return CompanyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		CompanyAddress = companyAddress;
	}
	public String getCompanyZipCode() {
		return CompanyZipCode;
	}
	public void setCompanyZipCode(String companyZipCode) {
		CompanyZipCode = companyZipCode;
	}
	public String getCompanyPhone() {
		return CompanyPhone;
	}
	public void setCompanyPhone(String companyPhone) {
		CompanyPhone = companyPhone;
	}
	public String getCompanyFax() {
		return CompanyFax;
	}
	public void setCompanyFax(String companyFax) {
		CompanyFax = companyFax;
	}
	public String getPostalAddress() {
		return PostalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		PostalAddress = postalAddress;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getPostalProvince() {
		return PostalProvince;
	}
	public void setPostalProvince(String postalProvince) {
		PostalProvince = postalProvince;
	}
	public String getPostalCity() {
		return PostalCity;
	}
	public void setPostalCity(String postalCity) {
		PostalCity = postalCity;
	}
	public String getPostalCounty() {
		return PostalCounty;
	}
	public void setPostalCounty(String postalCounty) {
		PostalCounty = postalCounty;
	}
	public String getPostalStreet() {
		return PostalStreet;
	}
	public void setPostalStreet(String postalStreet) {
		PostalStreet = postalStreet;
	}
	public String getPostalCommunity() {
		return PostalCommunity;
	}
	public void setPostalCommunity(String postalCommunity) {
		PostalCommunity = postalCommunity;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getEMail() {
		return EMail;
	}
	public void setEMail(String eMail) {
		EMail = eMail;
	}




}
