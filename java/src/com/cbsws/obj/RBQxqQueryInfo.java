package com.cbsws.obj;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class RBQxqQueryInfo extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	/**保单号*/
	private String Contno;
	/**投保人号*/
	private String Appntname;
	/**移动电话*/
	private String Mobile;
	/**联系电话*/
	private String Phone;
	/**联系地址*/
	private String Address;
	/**险种编码*/
	private String Riskcode;
	/**险种名称*/
	private String Riskname;
	/**应交保费*/
	private String Paymoney;
	/**应交日期*/
	private String Paytodate;
	/**实交保费*/
	private String Tpaymoney;
	/**实交日期*/
	private String Tpaydate;
	/**交费方式*/
	private String Paymode;
	/**催收状态*/
	private String Falg;
	
	
	

	public String getContno() {
		return Contno;
	}
	public void setContno(String contno) {
		Contno = contno;
	}
	public String getAppntname() {
		return Appntname;
	}
	public void setAppntname(String appntname) {
		Appntname = appntname;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getRiskcode() {
		return Riskcode;
	}
	public void setRiskcode(String riskcode) {
		Riskcode = riskcode;
	}
	public String getRiskname() {
		return Riskname;
	}
	public void setRiskname(String riskname) {
		Riskname = riskname;
	}
	public String getPaymoney() {
		return Paymoney;
	}
	public void setPaymoney(String paymoney) {
		Paymoney = paymoney;
	}
	public String getPaytodate() {
		return Paytodate;
	}
	public void setPaytodate(String paytodate) {
		Paytodate = paytodate;
	}
	public String getTpaymoney() {
		return Tpaymoney;
	}
	public void setTpaymoney(String tpaymoney) {
		Tpaymoney = tpaymoney;
	}
	public String getTpaydate() {
		return Tpaydate;
	}
	public void setTpaydate(String tpaydate) {
		Tpaydate = tpaydate;
	}
	public String getPaymode() {
		return Paymode;
	}
	public void setPaymode(String paymode) {
		Paymode = paymode;
	}
	public String getFalg() {
		return Falg;
	}
	public void setFalg(String falg) {
		this.Falg = falg;
	}
	
	
	
}
