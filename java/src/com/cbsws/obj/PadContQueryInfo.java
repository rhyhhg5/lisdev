package com.cbsws.obj;
import com.cbsws.core.xml.xsch.BaseXmlSch;


public class PadContQueryInfo extends BaseXmlSch{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** 开始日期*/
	private String StartDate;
	/** 终止日期*/
	private String EndDate;
	/**应交日期*/
	private String PaytoDate;
	/**业务员编码*/
	private String AgentCode;
	
	
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getEndDate() {
		return EndDate;
	}
	public void setEndDate(String endDate) {
		EndDate = endDate;
	}
	public String getPaytoDate() {
		return PaytoDate;
	}
	public void setPaytoDate(String paytoDate) {
		PaytoDate = paytoDate;
	}
	public String getAgentCode() {
		return AgentCode;
	}
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	
	
	
}
