package com.cbsws.picchobj;


import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class CaseNoInfo extends PiccBaseXmlSch{
  private static final long serialVersionUID = -1210790423707897845L;
	
	private String	CUSTOMERNAME;//被保险人姓名
	private String	IDTYPE;//证件类型	
	private String IDNO;//被保险人证件号码
	private String ISPROBLEM; //是否问题件
	private String STATE;  //问题件的状态
	public String getISPROBLEM() {
		return ISPROBLEM;
	}
	public void setISPROBLEM(String iSPROBLEM) {
		ISPROBLEM = iSPROBLEM;
	}
	public String getSTATE() {
		return STATE;
	}
	public void setSTATE(String sTATE) {
		STATE = sTATE;
	}
	public String getCUSTOMERNAME() {
		return CUSTOMERNAME;
	}
	public void setCUSTOMERNAME(String cUSTOMERNAME) {
		CUSTOMERNAME = cUSTOMERNAME;
	}
	public String getIDTYPE() {
		return IDTYPE;
	}
	public void setIDTYPE(String iDTYPE) {
		IDTYPE = iDTYPE;
	}
	public String getIDNO() {
		return IDNO;
	}
	public void setIDNO(String iDNO) {
		IDNO = iDNO;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
