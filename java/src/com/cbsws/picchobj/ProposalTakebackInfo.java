package com.cbsws.picchobj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class ProposalTakebackInfo extends  PiccBaseXmlSch{
	private static final long serialVersionUID = -4786465933806025992L;
	/** 保单号 **/
	private String ContNo = "";
	/** 险种名称 **/
	private String RiskName = "";
	/** 生效日期 **/
	private String CValidate = "";
	/** 投保人姓名 **/
	private String AppntName = "";
	/** 投保人手机号码 **/
	private String Mobile = "";
	/** 保单打印日期 **/
	private String Printdate="";
	
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getRiskName() {
		return RiskName;
	}
	public void setRiskName(String riskName) {
		RiskName = riskName;
	}
	public String getCValidate() {
		return CValidate;
	}
	public void setCValidate(String cValidate) {
		CValidate = cValidate;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getPrintdate() {
		return Printdate;
	}
	public void setPrintdate(String printdate) {
		Printdate = printdate;
	}
	
}
