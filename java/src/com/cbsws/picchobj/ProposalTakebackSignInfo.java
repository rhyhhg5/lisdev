package com.cbsws.picchobj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class ProposalTakebackSignInfo extends  PiccBaseXmlSch{
	private static final long serialVersionUID = -4786465933806025992L;
	/** 保单号 **/
	private String ContNo= "";
	/** 投保人姓名 */
    private String AppntName= "";
    /** 生效日期 **/
	private String CValidate = "";
    /** 签收日期 */
    private String GetpolDate= "";
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getCValidate() {
		return CValidate;
	}
	public void setCValidate(String cValidate) {
		CValidate = cValidate;
	}
	public String getGetpolDate() {
		return GetpolDate;
	}
	public void setGetpolDate(String getpolDate) {
		GetpolDate = getpolDate;
	}
}
