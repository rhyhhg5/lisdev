package com.cbsws.picchobj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class ContPolicyInfo extends  PiccBaseXmlSch{

	private static final long serialVersionUID = -4786465933806025992L;
	/** 保单号 **/
	private String ContNo = "";
	/** 姓名 **/
	private String InsuredName = "";
	/** 险种名称 **/
	private String RiskName = "";
	/** 生效日期 **/
	private String CValidate="";
	/** 保单状态 **/
	private String ContState = "";
	
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
	public String getRiskName() {
		return RiskName;
	}
	public void setRiskName(String riskName) {
		RiskName = riskName;
	}
	public String getCValidate() {
		return CValidate;
	}
	public void setCValidate(String cValidate) {
		CValidate = cValidate;
	}
	public String getContState() {
		return ContState;
	}
	public void setContState(String contState) {
		ContState = contState;
	}
	
}
