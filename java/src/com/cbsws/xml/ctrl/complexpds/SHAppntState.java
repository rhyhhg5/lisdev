package com.cbsws.xml.ctrl.complexpds;

import java.util.List;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.SHAppntInfo;
import com.cbsws.obj.SHContTable;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class SHAppntState extends ABusLogic {
	
	public String AppntName="";
	public String SendDate="";//报文发送日期
	public String SendTime="";//报文发送时间
	public String BatchNo="";
	public String IDType;
	public String IDNo;
	public SHAppntInfo cSHAppntInfo;
	
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始登录验证");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		SendDate = tMsgHead.getSendDate();
		SendTime = tMsgHead.getSendTime();
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("SHAppntInfo");			
			cSHAppntInfo = (SHAppntInfo) tLCContList.get(0); //获取保单信息，只有一个保单
	        AppntName=cSHAppntInfo.getAppntName();
	        IDType=cSHAppntInfo.getIDType();
	        IDNo=cSHAppntInfo.getIDNo();

	        SSRS ss1=new SSRS();
	        ExeSQL exeSQL=new ExeSQL();
	        String sql="select customerno from ldperson where name='"+AppntName+"' and idtype='"
	        		+IDType+"' and idno='"+IDNo+"'";
	        ss1=exeSQL.execSQL(sql);
	        if(ss1.getMaxRow()==0){
	        	errLog("不存在于所传投保人信息相同的保单");
	        	return false;
	        }else{
	        	String customerno=ss1.GetText(1, 1);
	        	ss1.Clear();
	        	sql="select lc.contno, (case when lc.appflag ='0' and lc.uwflag='5' then '1'  when lc.appflag ='1' then '2' when lc.appflag ='0' then '0' else '' end),"
	 	        		+"lc.PolApplyDate,lc.prem,lc.amnt,lc.getpoldate"
	 	        		 +" from lccont lc,lcpol lp where lc.appntno='"+customerno+"' and lc.contno=lp.contno and "
	 	        		 		+ " exists (select 1 from lmriskapp where taxoptimal='Y' and riskcode=lp.riskcode) order by lc.PolApplyDate";
	 	        		 
	        	ss1=exeSQL.execSQL(sql);
	        	if(ss1.getMaxRow()==0){
	        		errLog("该投保人下不存在税优保单");
	        		return false;
	        	}
	        	for(int i=1;i<=ss1.getMaxRow();i++){
	        		SHContTable shContTable=new SHContTable();
	        		shContTable.setContNo(ss1.GetText(i, 1));
	        		shContTable.setState(ss1.GetText(i, 2));
	        		shContTable.setPolApplyDate(ss1.GetText(i, 3));	 
	        		shContTable.setPrem(ss1.GetText(i, 4));
	        		shContTable.setAmnt(ss1.GetText(i, 5));
	        		if("".equals(ss1.GetText(i, 6))||ss1.GetText(i, 6)==null){
	        			shContTable.setGetPolState("0");
	        		}else{
	        			shContTable.setGetPolState("1");
	        		}
	        		putResult("SHContTable", shContTable);
	        	}
	        }
	        //getXmlResult();
		}catch (Exception e) {
			System.out.println(e);
			errLog("未知错误，请检查报文格式");
        	return false;
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
	}

	public static void main(String[] args) {
    	String s1="[A-Za-z0-9]{10}";
    	String s2="1b3456789A";
    	System.out.println(s2.matches(s1));
    	
	}
    
}
