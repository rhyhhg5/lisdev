package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.MonthBalaRate;
import com.cbsws.obj.PolMonthBalaInfo;
import com.cbsws.obj.SYReportQueryInfo;
import com.cbsws.obj.SYReportQueryResult;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class SYReportQueryInterfaceBL extends ABusLogic{
	
	private String BatchNo;//批次号
	private String MsgType;//报文类型
	private String Operator;//操作者
	private GlobalInput mGlobalInput = new GlobalInput();//公共信息
	private SYReportQueryInfo SYReportQueryInfo;
	private SSRS tSSRS1 = new SSRS();
	private ExeSQL tExeSQL = new ExeSQL();
	private SYReportQueryResult mSYReportQueryResult = new SYReportQueryResult();;
	private List<PolMonthBalaInfo> polMonthBalaInfos = new ArrayList<PolMonthBalaInfo>();
	private List<MonthBalaRate> monthBalaRates = new ArrayList<MonthBalaRate>();
	private LCPolSchema mLCPolSchema;
	private LCContSchema mLCContSchema;
	private String reportType;
	private String contNo;
	private String balaQuarter;
	private String balaYear;
	private String polYear;
	private String mStartDate;
	private String mEndDate;
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		if(!parseXML(cMsgInfos)){
			return false;
		}
		
		if(!checkdata()){
			return false;
		}
		
		if(!getPolInfo()){
			return false;
		}
		
		if(!creatReport()){
			return false;
		}
		
		return true;
	}
	
	/**
	 * 解析xml
	 * @param cMsgInfos
	 * @return
	 */
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List<SYReportQueryInfo> SYReportQueryInfos = cMsgInfos.getBodyByFlag("SYReportQueryInfo");
        if (SYReportQueryInfos == null || SYReportQueryInfos.size() != 1)
        {
            errLog("申请报文中获取报文体失败。");
            return false;
        }
        SYReportQueryInfo = (SYReportQueryInfo)SYReportQueryInfos.get(0);
        
        return true;
	}
	
	/**
	 * 校验数据
	 * @return
	 */
	private boolean checkdata(){
		reportType = SYReportQueryInfo.getReportType();
		if(reportType == null || "".equals(reportType)){
			errLog("报告类型不能为空！");
			return false;
		}
		if(!"0".equals(reportType) && !"1".equals(reportType)){
			errLog("报告类型值不正确，只能为“0”或“1”！");
			return false;
			
		}
		contNo = SYReportQueryInfo.getContNo();
		if(contNo == null || "".equals(contNo)){
			errLog("保单号不能为空！");
			return false;
		}
		String queryCont = "select 1 from lccont where contno = '"+contNo+"' and appflag = '1' and exists (select 1 from lcpol where lcpol.contno = lccont.contno and lcpol.riskcode in (select riskcode from lmriskapp where risktype4 = '4' and taxoptimal = 'Y'))"; 
		String haveFlag = tExeSQL.getOneValue(queryCont);
		if(!"1".equals(haveFlag)){
			errLog("该保单没有查询到有效的税优保单！");
			return false;
		}
		if("0".equals(reportType)){
			balaYear = SYReportQueryInfo.getBalaYear();
			if(balaYear == null || "".equals(balaYear)){
				errLog("查询季度报告时，结算年度不能为空！");
				return false;
			}
			balaQuarter = SYReportQueryInfo.getBalaQuarter();
			if(balaQuarter == null || "".equals(balaQuarter)){
				errLog("查询季度报告时，结算季度不能为空！");
				return false;
			}			
		}
		if("1".equals(reportType)){
			polYear = SYReportQueryInfo.getPolYear();
			if(polYear == null || "".equals(polYear)){
				errLog("查询年度报告时，保单年度不能为空！");
				return false;
			}			
			String checkPolYearSQL = "select 1 from lcinsureaccbalance where contno = '"+contNo+"' and polyear = "+polYear+" and polmonth = 12";
			if(!"1".equals(tExeSQL.getOneValue(checkPolYearSQL))){
				errLog("该税优保单承保不足"+polYear+"年或结算不足"+polYear+"年，无法生成第"+polYear+"年度的年报！");
				return false;
			}
		}

		return true;
	}
	
	private boolean getPolInfo(){
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(contNo);
		if (!tLCContDB.getInfo()) {
			errLog("获得保单信息失败！");
			return false;
		}
		mLCContSchema = tLCContDB.getSchema();
		
		LCPolDB tLCPolDB = new LCPolDB();
		mLCPolSchema = tLCPolDB.executeQuery("select * from lcpol where contno = '"+contNo+"' ").get(1);
		if (mLCPolSchema == null) {
			errLog("获得险种信息失败！");
			return false;
		}
		return true;
	}
	
	
	/**
	 * 生成季报与年报返回报文
	 * @return
	 */
	private boolean creatReport(){
		// 获取投保人地址
		LCAppntDB tLCAppntDB = new LCAppntDB();
		tLCAppntDB.setContNo(contNo);
		if (!tLCAppntDB.getInfo()) {
			CError.buildErr(this, "投保人表中缺少数据");
			errLog("投保人表中缺少数据！");
			return false;
		}
		String sql = " select * from LCAddress where CustomerNo='"
			+ tLCAppntDB.getAppntNo()
			+ "' AND AddressNo = '"
			+ tLCAppntDB.getAddressNo() + "'";
		System.out.println("sql=" + sql);
		LCAddressDB tLCAddressDB = new LCAddressDB();
		LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
		LCAddressSchema mLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).getSchema();
		mSYReportQueryResult.setAddress(mLCAddressSchema.getPostalAddress());
		mSYReportQueryResult.setCustomerName(mLCPolSchema.getAppntName());
		mSYReportQueryResult.setZipCode(mLCAddressSchema.getZipCode());
		

		String sql1 = "select riskname from lmrisk where riskcode = '"+mLCPolSchema.getRiskCode()+"' with ur";

		
		tSSRS1 = tExeSQL.execSQL(sql1);
		
		String contstate = "";
		if ("1".equals(mLCPolSchema.getStateFlag())) {
			contstate = "有效";
		} else {
			contstate = "无效";
		}
		mSYReportQueryResult.setRiskCodeName(tSSRS1.GetText(1, 1));//产品名称
		mSYReportQueryResult.setContNo(contNo);//保险单编号
		mSYReportQueryResult.setContState(contstate);//保单状态（有效/无效）
		mSYReportQueryResult.setAppntName(tLCAppntDB.getAppntName());//投保人姓名
		mSYReportQueryResult.setInsuredName(mLCPolSchema.getInsuredName());//被保险人姓名
		
		// 首期承保生效日
		String CValiDateSQL = "select min(temp.cvalidate) "
				+ "from (select cvalidate cvalidate,cinvalidate cinvalidate "
				+ "from lccont "
				+ "where conttype='1' and appflag='1' and contno='"
				+ contNo
				+ "' "
				+ "union all select cvalidate cvalidate,cinvalidate cinvalidate "
				+ "from lbcont where conttype='1' and appflag='1' and edorno like 'xb%' and contno "
				+ "in (select newcontno from lcrnewstatelog where contno='"
				+ contNo + "' and state='6')) temp "
				+ "with ur";
		String CValiDate = tExeSQL.getOneValue(CValiDateSQL);
		if (null == CValiDate || "".equals(CValiDate)) {
			CValiDate = mLCContSchema.getCValiDate();
		}

		mSYReportQueryResult.setCValiDate(CValiDate);
		
		String solvencySQL = "select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
		SSRS tSSRS2 = new SSRS();
		tSSRS2 = tExeSQL.execSQL(solvencySQL);
		mSYReportQueryResult.setYear1(tSSRS2.GetText(1, 1));
		mSYReportQueryResult.setQuarter1(tSSRS2.GetText(1, 2));
		mSYReportQueryResult.setSolvency(tSSRS2.GetText(1, 3));
		mSYReportQueryResult.setFlag(tSSRS2.GetText(1, 4));
		
		String riskSQL = "select Year,quarter,riskrate  from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
		SSRS tSSRS3 = new SSRS();
		tSSRS3 = tExeSQL.execSQL(riskSQL);
		mSYReportQueryResult.setYear2(tSSRS3.GetText(1, 1));
		mSYReportQueryResult.setQuarter2(tSSRS3.GetText(1, 2));
		mSYReportQueryResult.setRiskRate(tSSRS3.GetText(1, 3));
		
		//已缴费金额
		String premSQL = "select nvl(sum(SumActuPayMoney),0) from ljapayperson where contno = '"
				+ contNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and paytype='ZC' with ur";
		String totalPrem = tExeSQL.getOneValue(premSQL);
		mSYReportQueryResult.setTotalPrem(totalPrem);

		//缴费次数
        String countSQL="select count(distinct curpaytodate) from ljapayperson where contno='"+contNo+"' and paytype='ZC' ";
        String totalCount=tExeSQL.getOneValue(countSQL);
        if(null == totalCount || "".equals(totalCount)){
        	totalCount="0";
        }
        mSYReportQueryResult.setTotalCount(totalCount);
		
		if("0".equals(reportType)){
			if(!creatQuarterReportInfo()){
				return false;
			}
		}
				
		if("1".equals(reportType)){
			if(!creatAnnualReportInfo()){
				return false;
			}
		}				
		
		getWrapParm(mSYReportQueryResult);
		getWrapParmList1(monthBalaRates);
		getWrapParmList2(polMonthBalaInfos);
		getXmlResult();
		
		return true;
	}
	
	/**
	 * 生成季报相关信息
	 * @return
	 */
	private boolean creatQuarterReportInfo(){
		System.out.println("开始生成季报相关信息。。。。。。。");
		System.out.println("保单的结算年度：" + balaYear + "年第" + balaQuarter + "季度");
		mSYReportQueryResult.setReportType("0");
		// 根据报告的季度设置起止日期
		if ("1".equals(balaQuarter)) {
			mStartDate = balaYear + "-01-01";
			mEndDate = balaYear + "-03-31";
		} else if ("2".equals(balaQuarter)) {
			mStartDate = balaYear + "-04-01";
			mEndDate = balaYear + "-06-30";
		} else if ("3".equals(balaQuarter)) {
			mStartDate = balaYear + "-07-01";
			mEndDate = balaYear + "-09-30";
		} else if ("4".equals(balaQuarter)) {
			mStartDate = balaYear + "-10-01";
			mEndDate = balaYear + "-12-31";
		}

		if (CommonBL.stringToDate(mEndDate).before(CommonBL.stringToDate(mSYReportQueryResult.getCValiDate())))
		{
			errLog("所查季度的截止日期:"+mEndDate+"不能早于保单生效日:"+mSYReportQueryResult.getCValiDate()+"！");
			return false;
		}
		mSYReportQueryResult.setStartDate(mStartDate);
		mSYReportQueryResult.setEndDate(mEndDate);
		// 保单年度
		polYear = new ExeSQL()
		.getOneValue("select min(polYear) From lcinsureaccbalance where  year(DueBalaDate - 1 days) ="
				+ balaYear
				+ " and DueBalaDate <= '"
				+ mEndDate
				+ "' "
				+ " and DueBalaDate >= '"
				+ mStartDate
				+ "' "
				+ " and contno ='"
				+ contNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' with ur ");
		
		monthBalaRates = new ArrayList<MonthBalaRate>();
		
		// 生成结算利息列表,报告期内各月年化结算利率及结算信息
		for (int m = 1; m <= 3; m++) {
			String tmpStartDate = "";
			if (m == 1) {
				tmpStartDate = mStartDate;
			} else {
				tmpStartDate = PubFun.calDate(mStartDate, m - 1, "M", null);
			}
			String ratesql = "select rate From LMInsuAccRate where riskcode = '"
				+ mLCPolSchema.getRiskCode()
				+ "' and StartBalaDate = '"
				+ tmpStartDate
				+ "' with ur";
			String rate = tExeSQL.getOneValue(ratesql);
			if(rate == null || "".equals(rate)){
				errLog( "获取保单" + contNo + "第"+ polYear + "年" + tmpStartDate + "的年化利率失败！");
				return false;
			}
			
			MonthBalaRate monthBalaRate = new MonthBalaRate();
			monthBalaRate.setRate(rate);
			monthBalaRate.setRateMonth(tmpStartDate);
			monthBalaRates.add(monthBalaRate);			
		}
		
		// 生成各月结算信息
		for (int m = 1; m <= 3; m++) {
			String tPolMonth = String.valueOf(m);
			String tBalaDate = "";
			if (m == 1) {
				tBalaDate = mStartDate;
			} else {
				tBalaDate = PubFun.calDate(mStartDate, m - 1, "M", null);
			}
			if(!setQuarterMonthList(tBalaDate)){
				errLog("获取保单" + contNo + "第"+ polYear + "年" + tPolMonth + "月结算信息明细失败。");
				return false;
				
			}
		}
		System.out.println("季报相关信息生成完毕。。。。。。");
		
		return true;
	}
		
	
	// 生成季报各月结算信息
	private boolean setQuarterMonthList(String tStartDate) {
		String sql = "select max(duebaladate),(max(duebaladate) - 1 day),PolYear,PolMonth From lcinsureaccbalance where "
				+ " contno ='"
				+ contNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and (duebaladate - 1 month)='"
				+ tStartDate + "' and length(sequenceno) <> 14 "
				+ " group by PolYear,PolMonth with ur ";
		SSRS tSSRS = tExeSQL.execSQL(sql);
		PolMonthBalaInfo polMonthBalaInfo = new PolMonthBalaInfo();
		if(null == tSSRS || tSSRS.getMaxRow()<=0){
			//没有查询到月结记录则应为保单投保首月所在的季度,填充默认数据
			polMonthBalaInfo.setMonth(decodeDate(tStartDate));
			polMonthBalaInfo.setStartMoney("0.00");
			polMonthBalaInfo.setBQAddPrem("0.00");
			polMonthBalaInfo.setBRAddPrem("0.00");
			polMonthBalaInfo.setLXMoney("0.00");
			polMonthBalaInfo.setRiskPrem("0.00");
			polMonthBalaInfo.setEndMoney("0.00");
			polMonthBalaInfos.add(polMonthBalaInfo);
			return true;
		}
		String tDueBalaDate = tSSRS.GetText(1, 1); // 应结算日期
		String tEndDate = tSSRS.GetText(1, 2); // 本月结束时间
		polMonthBalaInfo.setMonth(decodeDate(tEndDate));
		String tempPolYear = tSSRS.GetText(1, 3);//当月的保单年度
		String tempPolMonth = tSSRS.GetText(1, 4);//月度
		String tStartMoney = "";
		String tEndMoney = "";
		if("1".equals(tempPolYear) && "1".equals(tempPolMonth))
        {
    		sql = "select nvl(sum(money),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ contNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
        else
        {
        	sql = "select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ contNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
    	
        
		tStartMoney = tExeSQL.getOneValue(sql); //本月期初帐户价值
		if(null==tStartMoney || "".equals(tStartMoney)){
			errLog("获取月初账户账户价值失败！");
			return false;
		}
		polMonthBalaInfo.setStartMoney(tStartMoney);// 本月期初帐户价值

		sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '"
				+ contNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and paydate between '"
				+ tStartDate
				+ "' and '"
				+ tEndDate
				+ "' and othertype = '"
				+ BQ.NOTICETYPE_P
				+ "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
		String tBQAddPrem = tExeSQL.getOneValue(sql);
		if(null==tBQAddPrem || "".equals(tBQAddPrem)){
			errLog("获取本月追加保费失败！");
			return false;
		}
		polMonthBalaInfo.setBQAddPrem(tBQAddPrem);

		sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype ='BR' and contno = '"
				+ contNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and paydate between '"
				+ tStartDate
				+ "' and '"
				+ tEndDate
				+ "' with ur";
		String tBRAddPrem = tExeSQL.getOneValue(sql);
		if(null==tBRAddPrem || "".equals(tBRAddPrem)){
			errLog("获取本月差额返还失败！");
			return false;
		}
		polMonthBalaInfo.setBRAddPrem(tBRAddPrem);// 本月差额返还


		sql = "select nvl(sum(case when moneytype = 'LX' then money end),0), "
				+ "nvl(sum(case when moneytype = 'RP' then -money end),0) "
				+ "from lcinsureacctrace a where contno = '"
				+ contNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and otherno in (select sequenceno from lcinsureaccbalance where " 
				+ " DueBalaDate = '"+ tDueBalaDate +"') with ur ";
		SSRS tSSRS2 = tExeSQL.execSQL(sql);
		polMonthBalaInfo.setLXMoney(tSSRS2.GetText(1, 1));// 保单帐户结算收益
		polMonthBalaInfo.setRiskPrem(tSSRS2.GetText(1, 2));// 风险保险费

		sql = "select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) "
        	+ "From lcinsureacctrace where contno = '" 
        	+ contNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tDueBalaDate + "' with ur ";
        	
        tEndMoney =  tExeSQL.getOneValue(sql); //本月期末帐户价值
        if(null==tEndMoney || "".equals(tEndMoney)){
			errLog("获取月末账户价值失败！");
			return false;
		}
        polMonthBalaInfo.setEndMoney(tEndMoney);// 本月期末帐户价值
        polMonthBalaInfos.add(polMonthBalaInfo);
        
        return true;
	}
	
	
	
	
	/**
	 * 把型如"2009-01-01"格式的日期转为"2009年01月"格式
	 * 
	 * @param date
	 *            String
	 * @return String
	 */
	private String decodeDate(String date) {
		if ((date == null) || (date.equals(""))) {
			System.out.println("日期格式不正确！");
			return "";
		}
		String data[] = date.split("-");
		if (data.length != 3) {
			System.out.println("日期格式不正确！");
			return "";
		}
		return (data[0] + "年" + data[1] + "月");
	}
	
	
	
	/**
	 * 生成年报相关信息
	 * @return
	 */
	private boolean creatAnnualReportInfo(){
		System.out.println("开始生成年报相关信息。。。。。。");
		mSYReportQueryResult.setReportType("1");
		//报告期间
        if("1".equals(polYear))
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select min(DueBalaDate) From lcinsureaccbalance where polyear =" +
                    polYear +
                    " and polmonth = 1 and contno ='" +
                    contNo +
                    "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        }
        else
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select max(DueBalaDate)-1 month From lcinsureaccbalance where polyear =" +
                    polYear +
                    " and polmonth = 1 and contno ='" +
                    contNo +
                    "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        }
        String baladate = "select max(DueBalaDate)-1 day,max(DueBalaDate) From lcinsureaccbalance where  PolYear = " 
     	   + polYear + " and polmonth = 12 and contno = '" + contNo 
     	   + "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ";
		SSRS dateSSRS = tExeSQL.execSQL(baladate);
		mEndDate = dateSSRS.GetText(1, 1);
		mSYReportQueryResult.setStartDate(mStartDate);
		mSYReportQueryResult.setEndDate(mEndDate);
		
        
//      生成结算利息列表,报告期内各月年化结算利率及结算信息
        String tRiskCode = mLCPolSchema.getRiskCode();             
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthRate(tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
        		errLog( "获取保单" + contNo + "第"+ polYear + "年" + tPolMonth + "的年化利率失败！");
                return false;
            }
        }
		
//      生成各月结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setAnnualMonthList(tPolMonth);
        	}
        	catch (Exception ex)
            {
        		errLog("获取保单" + contNo + "第"+ polYear + "年" + tPolMonth + "月结算信息明细失败。");
                return false;
            }
        }
		System.out.println("年报相关信息生成完毕。。。。。。");
		return true;
	}
	
	/**
	 * 生成年报结算利息列表,报告期内各月年化结算利率及结算信息
	 * @param aRiskCode
	 * @param aPolMonth
	 */
    private void setMonthRate(String aRiskCode,String aPolMonth)
    {
    	String sql = "";
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance where polYear = " 
			   + polYear + " and polmonth = " + aPolMonth + " and contno ='" + contNo 
			   + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = tExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	
    	sql = "select rate From LMInsuAccRate where riskcode = '" + aRiskCode 
    		+ "' and baladate = '" + tDueBalaDate + "' with ur";
     	String rate = tExeSQL.getOneValue(sql);
     	MonthBalaRate monthBalaRate = new MonthBalaRate();
		monthBalaRate.setRate(rate);
		monthBalaRate.setRateMonth(decodeDate(tEndDate));
		monthBalaRates.add(monthBalaRate);	
    }
	
    /**
     * 生成年报各月结算信息
     * @param aPolMonth
     */
    private void setAnnualMonthList(String aPolMonth)
    {
    	String sql = "";
    	if("1".equals(polYear) && "1".equals(aPolMonth))
        {
        	sql = "select min(DueBalaDate) From lcinsureaccbalance where polyear = 1 " 
        		+ "and polmonth = 1 and contno = '" + contNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
        }
        else
        {
        	sql = "select max(DueBalaDate)-1 month From lcinsureaccbalance where polyear = " 
        		+ polYear + " and polmonth = " + aPolMonth + " and contno = '" + contNo + "'"
        		+ " and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
        }
        String tStartDate = tExeSQL.getOneValue(sql); //本月开始时间
        
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance where polYear = " 
			   + polYear + " and polmonth = " + aPolMonth + " and contno ='" + contNo 
			   + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = tExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	PolMonthBalaInfo polMonthBalaInfo = new PolMonthBalaInfo();
		polMonthBalaInfo.setMonth(decodeDate(tEndDate));
     	
//     	本月期初帐户价值
    	
    	if("1".equals(polYear) && "1".equals(aPolMonth))
        {
    		sql = "select nvl(sum(money),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
        else
        {
        	sql = "select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
        polMonthBalaInfo.setStartMoney(tExeSQL.getOneValue(sql));//本月期初帐户价值
        
        sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '" 
        	+ contNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + tStartDate + "' and '"
        	+ tEndDate + "' and othertype = '"
        	+ BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
        polMonthBalaInfo.setBQAddPrem(tExeSQL.getOneValue(sql));//本月追加保费
        
        
    	
    	
    	sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype ='BR' and contno = '" 
          	 + contNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + tStartDate + "' and '"
          	 + tEndDate + "' with ur";
    	polMonthBalaInfo.setBRAddPrem(tExeSQL.getOneValue(sql));//本月差额返还     	

       	    	
    	sql = "select nvl(sum(case when moneytype = 'LX' then money end),0), "
          	 + "nvl(sum(case when moneytype = 'RP' then -money end),0) " 
          	 + "from lcinsureacctrace a where contno = '" 
          	 + contNo + "' and polno ='"+mLCPolSchema.getPolNo()
          	 +"' and otherno in (select sequenceno from lcinsureaccbalance where polYear = " 
         	 + polYear + " and polmonth = " + aPolMonth + " and contno = a.contno) with ur ";
    	SSRS tSSRS2 = tExeSQL.execSQL(sql);
    	polMonthBalaInfo.setLXMoney(tSSRS2.GetText(1, 1));//保单帐户结算收益
    	polMonthBalaInfo.setRiskPrem(tSSRS2.GetText(1, 2));//风险保险费


        
//      本月期末帐户价值
        sql = "select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) "
        	+ "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tDueBalaDate + "' with ur ";
        polMonthBalaInfo.setEndMoney(tExeSQL.getOneValue(sql));//本月期末帐户价值

        polMonthBalaInfos.add(polMonthBalaInfo);
    }
	
	 private void getWrapParm(SYReportQueryResult tSYReportQueryResult){
		 mSYReportQueryResult = tSYReportQueryResult;

	 }
	 private void getWrapParmList1(List<MonthBalaRate> monthBalaRates1){
		 monthBalaRates = monthBalaRates1;
		 
	 }
	 private void getWrapParmList2(List<PolMonthBalaInfo> polMonthBalaInfos1){
		 polMonthBalaInfos = polMonthBalaInfos1;
		 
	 }
	    
	//返回报文
	public void getXmlResult(){
		
		putResult("SYReportQueryResult", mSYReportQueryResult);

		for(int i=0;i<monthBalaRates.size();i++){
			putResult("MonthBalaRate", (MonthBalaRate)monthBalaRates.get(i));
		}
		
		for(int i=0;i<polMonthBalaInfos.size();i++){
			putResult("PolMonthBalaInfo", (PolMonthBalaInfo)polMonthBalaInfos.get(i));
		}

	}	

}
