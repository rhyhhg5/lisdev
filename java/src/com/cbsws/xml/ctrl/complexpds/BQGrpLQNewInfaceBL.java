package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.EdorGRPLQChangeInfo;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPLQEdorMainTale;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;

import com.sinosoft.task.BQGrpLQEdorBL;
import com.sinosoft.task.CommonBL;
import com.sinosoft.utility.VData;

public class BQGrpLQNewInfaceBL extends ABusLogic{
	
	private LGWorkTable mLGWorkTable;
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	private EdorGRPLQChangeInfo mEdorGRPLQChangeInfo;
	private String mCustomeNO;
    private String mManageCom;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	private List mLPEdorMainList;

	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkdata()){
        	return false;
        } 
        
        
        if(!dealDate()){
        	return false;
        }
		return true;
	}

	
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

	    List tEdorGRPLQChangeInfo = cMsgInfos.getBodyByFlag("EdorGRPLQChangeInfo");
	      if (tEdorGRPLQChangeInfo == null || tEdorGRPLQChangeInfo.size() != 1)
	        {
	            errLog("申请报文中获取被保人信息失败。");
	            return false;
	        }
	      mEdorGRPLQChangeInfo = (EdorGRPLQChangeInfo)tEdorGRPLQChangeInfo.get(0);
	        
       
        List tLGWorkList = cMsgInfos.getBodyByFlag("LGWorkTable");
        if(tLGWorkList == null ||tLGWorkList.size()!=1)
        {
            errLog("申请报文中获取工单信息失败。");
            return false;
        }
        mLGWorkTable=(LGWorkTable)tLGWorkList.get(0);
        
        return true;
	}
	
	
	
	private boolean checkdata(){
   
	   if(null == mEdorGRPLQChangeInfo.getGrpContNo() || mEdorGRPLQChangeInfo.getGrpContNo().equals("")){
	      errLog("团体保单号不能为空。");
	      return false;
	   }
	      
//	   if(null == mEdorGRPLQChangeInfo.getContNo() || mEdorGRPLQChangeInfo.getContNo().equals("")){
//		      errLog("团体分单号不能为空。");
//		      return false;
//		   }
	   if(null == mEdorGRPLQChangeInfo.getPaymode() || mEdorGRPLQChangeInfo.getPaymode().equals("")){
		      errLog("领取方式不能为空。");
		      return false;
		   }
//	   if(mEdorGRPLQChangeInfo.getPaymode().equals("4")){
//	   if(null == mEdorGRPLQChangeInfo.getBankCode() || mEdorGRPLQChangeInfo.getBankCode().equals("")){
//		      errLog("银行编码不能为空。");
//		      return false;
//		   }
//	   if(null == mEdorGRPLQChangeInfo.getAccNo() || mEdorGRPLQChangeInfo.getAccNo().equals("")){
//		      errLog("银行账户不能为空。");
//		      return false;
//		   }
//	   if(null == mEdorGRPLQChangeInfo.getAccName() || mEdorGRPLQChangeInfo.getAccName().equals("")){
//		      errLog("银行账户名不能为空。");
//		      return false;
//		   }
//
//	   }
	   if(null == mEdorGRPLQChangeInfo.getGetMoney() || mEdorGRPLQChangeInfo.getGetMoney().equals("")){
		      errLog("领取金额不能为空。");
		      return false;
		   }
	   if(null == mEdorGRPLQChangeInfo.getEdorCValiDate() || mEdorGRPLQChangeInfo.getEdorCValiDate().equals("")){
		      errLog("保全生效日期不能为空。");
		      return false;
		   }

		return true;
	}
	
	
	private  boolean dealDate(){
		
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		 tLCGrpContDB.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
		 if(!tLCGrpContDB.getInfo())
		  {
		     errLog("没有查询到团体保单信息"+mEdorGRPLQChangeInfo.getGrpContNo()+"。");
		     return false;
		   }
	        mCustomeNO = tLCGrpContDB.getAppntNo();
	        mManageCom = tLCGrpContDB.getManageCom();
	        //得到工单信息
	        String workNo = CommonBL.createWorkNo();
	        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	        tLGWorkSchema.setCustomerNo(mCustomeNO);
	        tLGWorkSchema.setTypeNo(mLGWorkTable.getTypeNo());
	        tLGWorkSchema.setWorkNo(workNo);
	        tLGWorkSchema.setContNo(mEdorGRPLQChangeInfo.getContNo());
	        tLGWorkSchema.setApplyTypeNo(mLGWorkTable.getApplyTypeNo());
	        tLGWorkSchema.setApplyName(mLGWorkTable.getApplyName());
	        tLGWorkSchema.setAcceptWayNo(mLGWorkTable.getAcceptWayNo());
	        tLGWorkSchema.setAcceptDate(mLGWorkTable.getAcceptDate());
	        tLGWorkSchema.setRemark("一卡通线上团体部分领取接口生成");
	        
	        //操作员信息
	        mGlobalInput.Operator = Operator;
	        mGlobalInput.ManageCom = mManageCom;
	        mGlobalInput.ComCode = mGlobalInput.ManageCom;
	        
	        LPGrpEdorItemSchema tLPEdorItemSchema = new LPGrpEdorItemSchema();
	        tLPEdorItemSchema.setEdorNo(workNo);
	        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_LQ);
	        tLPEdorItemSchema.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());

	  
	        VData data = new VData();
	        data.add(mGlobalInput);
	        data.add(tLPEdorItemSchema);
	        data.add(mEdorGRPLQChangeInfo);
	        data.add(tLGWorkSchema);
   
	        BQGrpLQEdorBL tBQGrpLQEdorBL = new BQGrpLQEdorBL();
	        if(!tBQGrpLQEdorBL.submitData(data))
	        {
	            errLog("保全变更失败"+tBQGrpLQEdorBL.mErrors.getFirstError());
	            return false;
	        }
	        
	        LPLQEdorMainTale tLPLQEdorMainTale = new LPLQEdorMainTale();
	        tLPLQEdorMainTale.setEdorAcceptNo(workNo);
	        tLPLQEdorMainTale.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
//	        返回报文
//	    	组织返回报文
	    	getWrapParmList(tLPLQEdorMainTale);
	    	getXmlResult();
		
		return true;
	}
	
	 private void getWrapParmList(LPLQEdorMainTale tLPEdorMainTable){
	    	mLPEdorMainList = new ArrayList();
	    
	    	mLPEdorMainList.add(tLPEdorMainTable);
	    }
	    
		//返回报文
		public void getXmlResult(){
			//返回退费信息
			for(int i=0;i<mLPEdorMainList.size();i++){
				putResult("LPEdorMainTable", (LPLQEdorMainTale)mLPEdorMainList.get(i));
			}

		}
 
}
