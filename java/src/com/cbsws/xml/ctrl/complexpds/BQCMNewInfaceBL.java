package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.EdorCMLCContSubInfo;
import com.cbsws.obj.EdorCMLCInsuredInfo;
import com.cbsws.obj.EdorCMLDPersonInfo;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPEdorMainTable;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPContSubSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.task.BQCMEdorBL;
import com.sinosoft.task.CommonBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

public class BQCMNewInfaceBL extends ABusLogic {
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	private EdorCMLCInsuredInfo mEdorLCInsuredInfo;
	private LGWorkTable mLGWorkTable;
	private EdorCMLDPersonInfo mEdorLDPersonInfo;
	//private EdorLCInsuredListInfo mEdorLCInsuredListInfo;
	private EdorCMLCContSubInfo mEdorLCContSubInfo;
	private boolean isSyou = false;
    private String mManageCom;
    private String mAppntno;
	private List mLPEdorMainList;
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkdata()){
        	return false;
        }     
        
        if(!changePolicyInfo()){
        	return false;
        }        
		return true;
	}
	
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

	    List tEdorLDPersonInfo = cMsgInfos.getBodyByFlag("EdorCMLDPersonInfo");
	      if (tEdorLDPersonInfo == null || tEdorLDPersonInfo.size() != 1)
	        {
	            errLog("申请报文中获取客户信息失败。");
	            return false;
	        }
	    mEdorLDPersonInfo = (EdorCMLDPersonInfo)tEdorLDPersonInfo.get(0);
	        
        
        List tedorLCInsuredInfo = cMsgInfos.getBodyByFlag("EdorCMLCInsuredInfo");
        if (tedorLCInsuredInfo == null || tedorLCInsuredInfo.size() != 1)
          {
            errLog("申请报文中获取理赔金账户信息失败。");
            return false;
          }
        mEdorLCInsuredInfo = (EdorCMLCInsuredInfo)tedorLCInsuredInfo.get(0);
        
//        List tEdorLCInsuredListInfo = cMsgInfos.getBodyByFlag("EdorLCInsuredListInfo");
//        if (tEdorLCInsuredListInfo == null || tEdorLCInsuredListInfo.size() != 1)
//          {
//            errLog("申请报文中获取学平险汇交件保单信息失败。");
//            return false;
//          }
//        mEdorLCInsuredListInfo = (EdorLCInsuredListInfo)tEdorLCInsuredListInfo.get(0);
          
        List tEdorLCContSubInfo = cMsgInfos.getBodyByFlag("EdorCMLCContSubInfo");
        if (tEdorLCContSubInfo == null || tEdorLCContSubInfo.size() != 1)
          {
            errLog("申请报文中获取税优保单信息失败。");
            return false;
          }
        mEdorLCContSubInfo = (EdorCMLCContSubInfo)tEdorLCContSubInfo.get(0);
        
        List tLGWorkList = cMsgInfos.getBodyByFlag("LGWorkTable");
        if(tLGWorkList == null ||tLGWorkList.size()!=1)
        {
            errLog("申请报文中获取工单信息失败。");
            return false;
        }
        mLGWorkTable=(LGWorkTable)tLGWorkList.get(0);
        
        return true;
	}
	
	private boolean checkdata(){
    //校验
    LCContDB tLCContDB = new LCContDB();
    tLCContDB.setContNo(mEdorLDPersonInfo.getContNo());
    if(!tLCContDB.getInfo())
	  {
	     errLog("没有查询到保单信息"+mEdorLDPersonInfo.getContNo()+"。");
	     return false;
	  }	
    mManageCom = tLCContDB.getManageCom();
    mAppntno = tLCContDB.getAppntNo();
      SSRS tSSRS = new SSRS();
      String sql = " select 1 "
	             + " from LGWork a "
	             + " where  contno= '" + mEdorLDPersonInfo.getContNo() + "' "
	             + " and TypeNo = '070001' and StatusNo<>'5' " 
	             + " and exists (select 1 from LPUWMaster where contno=a.contno and edorno=a.workno) and InnerSource='070002'";
  	  ExeSQL tExeSQL = new ExeSQL();
  	  tSSRS = tExeSQL.execSQL(sql);
      if(tSSRS.getMaxRow()>0 ){
          errLog("保单尚处在理赔续保核保阶段，并且核保结论已经保存，不能做对保单保费或保额有修改的保全项目");
    	  return false;
	  }

//      String idsql = "select idstartdate,idenddate,current date from lcappnt where contno='"+mEdorLDPersonInfo.getContNo()+"' union " 
//                   + " select idstartdate,idenddate,current date from lbappnt where contno='" + mEdorLDPersonInfo.getContNo()+"'";
//  	         tSSRS = tExeSQL.execSQL(idsql);
	       
//  	  if(tSSRS.getMaxRow()>0){
//		    String  startdate = tSSRS.GetText(1, 1);
//		    String  enddate = tSSRS.GetText(1, 2);
//		    String  today  = tSSRS.GetText(1, 3);
//	     
//		if((startdate != null && !"".equals(startdate)) && (enddate == null || "".equals(enddate) )){
//			if( PubFun.getBeforeDate(today, startdate).equals(today)){
//				errLog("客户证件已过有效期，请进行客户资料变更！");
//				return false;
//			}
//		}else if((startdate==null || "".equals(startdate)) && (enddate!=null && !"".equals(enddate) )){
//			if(PubFun.getBeforeDate(enddate, today).equals(enddate)){
//				errLog("客户证件已过有效期，请进行客户资料变更！");
//				return false;
//			}
//		}else if((startdate!=null && !"".equals(startdate) ) && (enddate !=null  && !"".equals(enddate) )){
//			if(PubFun.getBeforeDate(startdate, today).equals(today)|| PubFun.getBeforeDate(enddate, today).equals(enddate)){
//				errLog("客户证件已过有效期，请进行客户资料变更！");
//				return false;
//			}	
//		}
//	   }
  	  
     	if(mEdorLDPersonInfo.getName()!= null && !mEdorLDPersonInfo.getName().equals("") && mEdorLDPersonInfo.getName().length() < 2)
        {
     	  errLog("客户姓名长度不能小于2!");
    	  return false;
        }
       
	    //税优出单
	   	String checkTaxRiskcode = "select 1 from lmriskapp where riskType4 = '4' and TaxOptimal = 'Y' and riskcode in (select riskcode from lcpol where contno = '"+mEdorLDPersonInfo.getContNo()+"')";
	   	tSSRS = tExeSQL.execSQL(checkTaxRiskcode);
	    if(tSSRS.getMaxRow()>0){    
	     isSyou = true;
		if(mEdorLCContSubInfo.getTaxPayerType().equals("01") ){
			String gTaxNo = mEdorLCContSubInfo.getGTaxNo().trim();
			String gOrgancomCode = mEdorLCContSubInfo.getGOrgancomCode().trim();
			if((gTaxNo==null||"".equals(gTaxNo))&&(gOrgancomCode==null||"".equals(gOrgancomCode))){
				errLog("个税征收方式选择为“代扣代缴”时,则“单位税务登记证代码”和“单位社会信用代码”不能同时为空！");
				return false;
			}
			if(!gTaxNo.equals("")&&gTaxNo!=null&&gTaxNo.length()!=15&&gTaxNo.length()!=18&&gTaxNo.length()!=20){
	            errLog("单位税务登记证代码长度不符合要求！");
	            return false;
	        }
			if(!"".equals(gOrgancomCode)&&gOrgancomCode!=null&&gOrgancomCode.length()!=18){
	            errLog("单位社会信用代码长度不符合要求！");
	            return false;
	        } 
		}
		if(mEdorLCContSubInfo.getTaxPayerType().equals("02") ){
			String taxno = mEdorLCContSubInfo.getGTaxNo().trim();
			String creditcode = mEdorLCContSubInfo.getCreditCode().trim();
			if((taxno==null||"".equals(taxno))&&(creditcode==null||"".equals(creditcode))){
				errLog("个税征收方式选择为“自行申报”时,则“个人税务登记证代码”和“个人社会信用代码”不能同时为空！");
				return false;
			}
			if(!"".equals(taxno)&&taxno!=null&&taxno.length()!=15&&taxno.length()!=18&&taxno.length()!=20){
				 errLog("个人税务登记证代码长度不符合要求！");
	             return false;
			}
			if(!"".equals(creditcode) && creditcode!=null && creditcode.length()!=18){
	            errLog("个人社会信用代码长度不符合要求！");
	            return false;
	          }
		}
     
	    }
	    
	        if(isInsured()&&needChangePrem()){
	        	errLog ("投被保人为同一人发生年龄性别职业变更暂时不允许操作！");
	        	return false;
	          }
	    
//		if(mLGWorkTable.getAcceptWayNo()!=null && !"7".equals(mLGWorkTable.getAcceptWayNo())){
//			  errLog("工单受理途径必须为7（pad）！");
//			  return false;	  
//		}

	    // 这个校验需要放在最后
		//校验变更的客户是否为万能险的被保人(是：继续校验；否：校验通过)
  		String twnSql = "select count(1) "
  				 + "  from lcpol "
  				 + " where insuredno = '" + mEdorLDPersonInfo.getCustomerNo() + "'"
  				 + "   and stateflag = '1' "
  				 + "   and riskcode in ('330801','331801','332001')";
  		tSSRS = tExeSQL.execSQL(twnSql);
	    if(tSSRS.getMaxRow()>0){    
  		if(tSSRS.GetText(1, 1).equals("0"))
  		{
  			return true;
  		}
  		//校验变更后信息是否与其他被保人客户信息一致(是：继续校验；否：校验通过)
  		String tlduser = "select a.insuredno "
 			 + "  from lcinsured a where 1=1  ";
  		if(mEdorLDPersonInfo.getName()!= null && !mEdorLDPersonInfo.getName().equals("")){
  			tlduser = tlduser + " and a.name = '" + mEdorLDPersonInfo.getName() + "' "; 			
  		}
		if(mEdorLDPersonInfo.getSex()!= null && !mEdorLDPersonInfo.getSex().equals("")){
  			tlduser = tlduser + " and a.Sex = '" + mEdorLDPersonInfo.getSex() + "' "; 			
  		}
		
		if(mEdorLDPersonInfo.getBirthday()!= null && !mEdorLDPersonInfo.getBirthday().equals("")){
  			tlduser = tlduser + " and a.Birthday = '" + mEdorLDPersonInfo.getBirthday() + "' "; 			
  		}
		if(mEdorLDPersonInfo.getIDType()!= null && !mEdorLDPersonInfo.getIDType().equals("")){
  			tlduser = tlduser + " and a.IDType = '" + mEdorLDPersonInfo.getIDType() + "' "; 			
  		}
		if(mEdorLDPersonInfo.getIDNo()!= null && !mEdorLDPersonInfo.getIDNo().equals("")){
  			tlduser = tlduser + " and a.IDNo = '" + mEdorLDPersonInfo.getIDNo() + "' "; 			
  		}
		tlduser = tlduser  + "   and exists (select 'Y' from lccont b where b.contno=a.contno and b.stateflag='1')"
  			 + "   and a.insuredno <> '" + mEdorLDPersonInfo.getCustomerNo() + "'";
		tSSRS = tExeSQL.execSQL(tlduser);
	    if(tSSRS.getMaxRow()<=0){   	
  			return true;
  		}
  		//校验其他被保人所属险种是否为万能险(是：校验失败，提示信息；否：校验通过)

  		for(int i=0;i<tSSRS.getMaxRow();i++)
  		{
  			String totherWnSql = "select 'Y' "
  				 + "  from lcpol "
  				 + " where insuredno = '" + tSSRS.GetText(1, i+1) + "'"
  				 + "   and stateflag = '1' "
  				 + "   and riskcode in ('330801','331801','332001')";
  			SSRS tSSRS2 = tExeSQL.execSQL(totherWnSql);
  			if(tSSRS2.getMaxRow()>0)
  			  {
  				errLog("变更后的被保险人有正在生效的万能险保单，不能进行变更操作！");
  				return false;
  			  }
  	     } 
		
  	     return true;
	   }
	    return true;
	} 
	private boolean changePolicyInfo(){
		
		  //得到工单信息
	    String workNo = CommonBL.createWorkNo();
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(mEdorLDPersonInfo.getCustomerNo());
        tLGWorkSchema.setTypeNo(mLGWorkTable.getTypeNo());
        tLGWorkSchema.setWorkNo(workNo);
        tLGWorkSchema.setContNo(mEdorLDPersonInfo.getContNo());
        tLGWorkSchema.setApplyTypeNo(mLGWorkTable.getApplyTypeNo());
        tLGWorkSchema.setAcceptWayNo(mLGWorkTable.getAcceptWayNo());
        tLGWorkSchema.setAcceptDate(mLGWorkTable.getAcceptDate());
        tLGWorkSchema.setApplyName(mLGWorkTable.getApplyName());
        tLGWorkSchema.setRemark("线上客户资料变更接口生成");

        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(mEdorLDPersonInfo.getCustomerNo());
        tLDPersonDB.getInfo();
        LDPersonSchema tLDPersonSchema = new  LDPersonSchema();
        tLDPersonSchema.setSchema(tLDPersonDB.getSchema());
        
        tLDPersonSchema.setCustomerNo((mEdorLDPersonInfo.getCustomerNo()==null || "".equals(mEdorLDPersonInfo.getCustomerNo()))?tLDPersonSchema.getCustomerNo():mEdorLDPersonInfo.getCustomerNo());
        tLDPersonSchema.setName((mEdorLDPersonInfo.getName()==null || "".equals(mEdorLDPersonInfo.getName()))?tLDPersonSchema.getName():mEdorLDPersonInfo.getName());
        tLDPersonSchema.setSex((mEdorLDPersonInfo.getSex()==null || "".equals(mEdorLDPersonInfo.getSex()))?tLDPersonSchema.getSex():mEdorLDPersonInfo.getSex());
        tLDPersonSchema.setBirthday((mEdorLDPersonInfo.getBirthday()==null || "".equals(mEdorLDPersonInfo.getBirthday()))?tLDPersonSchema.getBirthday():mEdorLDPersonInfo.getBirthday());
        tLDPersonSchema.setIDType((mEdorLDPersonInfo.getIDType()==null || "".equals(mEdorLDPersonInfo.getIDType()))?tLDPersonSchema.getIDType():mEdorLDPersonInfo.getIDType());
        tLDPersonSchema.setIDNo((mEdorLDPersonInfo.getIDNo()==null || "".equals(mEdorLDPersonInfo.getIDNo()))?tLDPersonSchema.getIDNo():mEdorLDPersonInfo.getIDNo());
        tLDPersonSchema.setOccupationCode((mEdorLDPersonInfo.getOccupationCode()==null || "".equals(mEdorLDPersonInfo.getOccupationCode()))?tLDPersonSchema.getOccupationCode():mEdorLDPersonInfo.getOccupationCode());
        tLDPersonSchema.setOccupationType((mEdorLDPersonInfo.getOccupationType()==null || "".equals(mEdorLDPersonInfo.getOccupationType()))?tLDPersonSchema.getOccupationType():mEdorLDPersonInfo.getOccupationType());
        tLDPersonSchema.setMarriage((mEdorLDPersonInfo.getMarriage()==null || "".equals(mEdorLDPersonInfo.getMarriage()))?tLDPersonSchema.getMarriage():mEdorLDPersonInfo.getMarriage());
        tLDPersonSchema.setNativePlace((mEdorLDPersonInfo.getNativePlace()==null || "".equals(mEdorLDPersonInfo.getNativePlace()))?tLDPersonSchema.getNativePlace():mEdorLDPersonInfo.getNativePlace());
        tLDPersonSchema.setSalary((mEdorLDPersonInfo.getSalary()==null || "".equals(mEdorLDPersonInfo.getSalary()))?tLDPersonSchema.getSalary():Double.valueOf(mEdorLDPersonInfo.getSalary()));
        tLDPersonSchema.setPosition((mEdorLDPersonInfo.getPosition()==null || "".equals(mEdorLDPersonInfo.getPosition()))?tLDPersonSchema.getPosition():mEdorLDPersonInfo.getPosition());
        
        
        LCInsuredSchema tLCInsuredSchema = new  LCInsuredSchema(); 
        if(null != mEdorLCInsuredInfo.getInsuredNo() && !mEdorLCInsuredInfo.getInsuredNo().equals("")){
        	
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            String sql = "select * from LCInsured a where AppntNo = '" + mEdorLCInsuredInfo.getInsuredNo() + "' "
                       + " and AppntNo = insuredno and exists (select 1 from LCCont where ContNo = a.ContNo and (StateFlag is null or StateFlag not in ('0')))";
  
            LCInsuredSet tLCInsuredSet = tLCInsuredDB.executeQuery(sql);

            if(tLCInsuredSet != null ){
             tLCInsuredSchema.setSchema(tLCInsuredSet.get(1));	 
            }
            else {
              LCInsuredDB sLCInsuredDB = new LCInsuredDB();
              sLCInsuredDB.setContNo(mEdorLDPersonInfo.getContNo());
              sLCInsuredDB.setInsuredNo(mEdorLCInsuredInfo.getInsuredNo());
           
              sLCInsuredDB.getInfo();
              
              tLCInsuredSchema.setSchema(sLCInsuredDB.getSchema());  	
            }
        
        tLCInsuredSchema.setBankCode((mEdorLCInsuredInfo.getBankCode()==null || "".equals(mEdorLCInsuredInfo.getBankCode()))?tLCInsuredSchema.getBankCode():mEdorLCInsuredInfo.getBankCode());
        tLCInsuredSchema.setBankAccNo((mEdorLCInsuredInfo.getBankAccNo()==null || "".equals(mEdorLCInsuredInfo.getBankAccNo()))?tLCInsuredSchema.getBankAccNo():mEdorLCInsuredInfo.getBankAccNo());                
        tLCInsuredSchema.setAccName((mEdorLCInsuredInfo.getAccName()==null || "".equals(mEdorLCInsuredInfo.getAccName()))?tLCInsuredSchema.getAccName():mEdorLCInsuredInfo.getAccName());                	
        tLCInsuredSchema.setRelationToMainInsured((mEdorLCInsuredInfo.getRelationToMainInsured()==null || "".equals(mEdorLCInsuredInfo.getRelationToMainInsured()))?tLCInsuredSchema.getRelationToMainInsured():mEdorLCInsuredInfo.getRelationToMainInsured());                	
        tLCInsuredSchema.setRelationToAppnt((mEdorLCInsuredInfo.getRelationToAppnt()==null || "".equals(mEdorLCInsuredInfo.getRelationToAppnt()))?tLCInsuredSchema.getRelationToAppnt():mEdorLCInsuredInfo.getRelationToAppnt());                	
        tLCInsuredSchema.setInsuredStat((mEdorLCInsuredInfo.getInsuredStat()==null || "".equals(mEdorLCInsuredInfo.getInsuredStat()))?tLCInsuredSchema.getInsuredStat():mEdorLCInsuredInfo.getInsuredStat());                	
        tLCInsuredSchema.setNativePlace((mEdorLCInsuredInfo.getNativePlace()==null || "".equals(mEdorLCInsuredInfo.getNativePlace()))?tLCInsuredSchema.getNativePlace():mEdorLCInsuredInfo.getNativePlace());                	 // 国籍
        if( null != tLCInsuredSchema.getAppntNo()){
        	 tLCInsuredSchema.setPosition((mEdorLCInsuredInfo.getPosition()==null || "".equals(mEdorLCInsuredInfo.getPosition()))?tLCInsuredSchema.getPosition():mEdorLCInsuredInfo.getPosition());              // 岗位职务
             tLCInsuredSchema.setSalary((mEdorLCInsuredInfo.getSalary()==null || "".equals(mEdorLCInsuredInfo.getSalary()))?tLCInsuredSchema.getSalary():Double.valueOf(mEdorLCInsuredInfo.getSalary()));              // 年薪            
             tLCInsuredSchema.setIDStartDate((mEdorLCInsuredInfo.getIDStartDate()==null || "".equals(mEdorLCInsuredInfo.getIDStartDate()))?tLCInsuredSchema.getIDStartDate():mEdorLCInsuredInfo.getIDStartDate());  // 证件生效日期
             tLCInsuredSchema.setIDEndDate((mEdorLCInsuredInfo.getIDEndDate()==null || "".equals(mEdorLCInsuredInfo.getIDEndDate()))?tLCInsuredSchema.getIDEndDate():mEdorLCInsuredInfo.getIDEndDate()); // 证件失效日期
           	
        }else{
        	  LCAppntSchema tLCAppntSchema = new  LCAppntSchema(); 
              if(null != mEdorLCInsuredInfo.getInsuredNo() && !mEdorLCInsuredInfo.getInsuredNo().equals("")){
              LCAppntDB tLCAppntDB = new LCAppntDB();
              tLCAppntDB.setContNo(mEdorLDPersonInfo.getContNo());
              tLCAppntDB.setAppntNo(mEdorLCInsuredInfo.getInsuredNo());
           
              tLCAppntDB.getInfo();
              
             tLCAppntSchema.setSchema(tLCAppntDB.getSchema());
        	 tLCInsuredSchema.setPosition((mEdorLCInsuredInfo.getPosition()==null || "".equals(mEdorLCInsuredInfo.getPosition()))?tLCAppntSchema.getPosition():mEdorLCInsuredInfo.getPosition());              // 岗位职务
             tLCInsuredSchema.setSalary((mEdorLCInsuredInfo.getSalary()==null || "".equals(mEdorLCInsuredInfo.getSalary()))?tLCAppntSchema.getSalary():Double.valueOf(mEdorLCInsuredInfo.getSalary()));              // 年薪            
             tLCInsuredSchema.setIDStartDate((mEdorLCInsuredInfo.getIDStartDate()==null || "".equals(mEdorLCInsuredInfo.getIDStartDate()))?tLCAppntSchema.getIDStartDate():mEdorLCInsuredInfo.getIDStartDate());  // 证件生效日期
             tLCInsuredSchema.setIDEndDate((mEdorLCInsuredInfo.getIDEndDate()==null || "".equals(mEdorLCInsuredInfo.getIDEndDate()))?tLCAppntSchema.getIDEndDate():mEdorLCInsuredInfo.getIDEndDate()); // 证件失效日期
            }
        }
  
        tLCInsuredSchema.setGrpInsuredPhone((mEdorLCInsuredInfo.getGrpInsuredPhone()==null || "".equals(mEdorLCInsuredInfo.getGrpInsuredPhone()))?tLCInsuredSchema.getGrpInsuredPhone():mEdorLCInsuredInfo.getGrpInsuredPhone());
        
        //针对万能团险
//      tLCInsuredSchema.setJoinCompanyDate((mEdorLCInsuredInfo.getJoinCompanyDate()==null || "".equals(mEdorLCInsuredInfo.getJoinCompanyDate()))?tLCInsuredSchema.getJoinCompanyDate():mEdorLCInsuredInfo.getJoinCompanyDate()); // 服务年数起始日
//		tLCInsuredSchema.setPosition((mEdorLCInsuredInfo.getPosition()==null || "".equals(mEdorLCInsuredInfo.getJoinCompanyDate()))?tLCInsuredSchema.getJoinCompanyDate():mEdorLCInsuredInfo.getJoinCompanyDate()); // 服务年数起始日 // 级别
//        
        }
        
//       学平险 针对团单
//        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
//        tLCInsuredListDB.setAppntIdNo(mEdorLCInsuredListInfo.getAppntIdNo());
//        tLCInsuredListDB.setAppntIdType(mEdorLCInsuredListInfo.getAppntIdType());
//        tLCInsuredListDB.getInfo();
//        LCInsuredListSchema tLCInsuredListSchema = new  LCInsuredListSchema();
//        tLCInsuredListSchema.setSchema(tLCInsuredListDB.getSchema());
        

//    	tLCInsuredListSchema.setAppntName(request.getParameter("AppntName"));
//    	tLCInsuredListSchema.setAppntSex(request.getParameter("AppntSex"));
//    	tLCInsuredListSchema.setAppntIdNo(request.getParameter("AppntIDNo"));
//    	tLCInsuredListSchema.setAppntIdType(request.getParameter("AppntIDType"));
//    	tLCInsuredListSchema.setAppntBirthday(request.getParameter("AppntBirthday")); 
//       
          LPContSubSchema tLPContSubSchema = new LPContSubSchema();
        if(isSyou){      
        	tLPContSubSchema.setPrtNo(mEdorLCContSubInfo.getPrtNo());
        	tLPContSubSchema.setTaxPayerType(mEdorLCContSubInfo.getTaxPayerType());
        	tLPContSubSchema.setTaxNo(mEdorLCContSubInfo.getTaxNo());
        	tLPContSubSchema.setCreditCode(mEdorLCContSubInfo.getCreditCode());
        	tLPContSubSchema.setGTaxNo(mEdorLCContSubInfo.getGTaxNo());
        	tLPContSubSchema.setGOrgancomCode(mEdorLCContSubInfo.getGOrgancomCode());
    	 
        }   
   
        
        //操作员信息
        mGlobalInput.Operator = Operator;
        mGlobalInput.ManageCom = mManageCom;
        mGlobalInput.ComCode = mGlobalInput.ManageCom;
        
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo(workNo);
        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_CM);
        tLPEdorItemSchema.setContNo(mEdorLDPersonInfo.getContNo());
        tLPEdorItemSchema.setInsuredNo(mAppntno);
  
        VData data = new VData();
        data.add("I");//团单为 G
        data.add("N"); //团险万能 为 Y
        data.add(mGlobalInput);
        data.add(tLPEdorItemSchema);
        data.add(tLDPersonSchema);
        data.add(tLCInsuredSchema);
        data.add(tLGWorkSchema);
      //  data.add(tLCGetSchema);
      //  data.add(tLCInsuredListSchema);
        data.add(tLPContSubSchema);  
        BQCMEdorBL tBQCMEdorBL = new BQCMEdorBL();
        if(!tBQCMEdorBL.submitData(data))
        {
            errLog("保全变更失败"+tBQCMEdorBL.mErrors.getFirstError());
            return false;
        }
        LPEdorMainTable tLPEdorMainTable = new LPEdorMainTable();
        tLPEdorMainTable.setEdorAcceptNo(workNo);
//        返回报文
//    	组织返回报文
    	getWrapParmList(tLPEdorMainTable);
    	getXmlResult();
	
        
		return true;
	}
	 private void getWrapParmList(LPEdorMainTable tLPEdorMainTable){
	    	mLPEdorMainList = new ArrayList();
	    
	    	mLPEdorMainList.add(tLPEdorMainTable);
	    }
	    
		//返回报文
		public void getXmlResult(){
			//返回退费信息
			for(int i=0;i<mLPEdorMainList.size();i++){
				putResult("LPEdorMainTable", (LPEdorMainTable)mLPEdorMainList.get(i));
			}

		}
    
    private boolean isInsured()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mEdorLDPersonInfo.getContNo());
        tLCInsuredDB.setInsuredNo(mEdorLDPersonInfo.getCustomerNo());
        if (tLCInsuredDB.query().size() == 0)
        {
            return false;
        }
        return true;
    }
    private boolean needChangePrem()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mEdorLDPersonInfo.getContNo());
        tLCInsuredDB.setInsuredNo(mEdorLDPersonInfo.getCustomerNo());
        if (!tLCInsuredDB.getInfo())
        {
        	errLog("找不到被保人信息！");
            return false;
        }
        String tOccupationType="";
        if(tLCInsuredDB.getOccupationType()==null||"".equals(tLCInsuredDB.getOccupationType().trim())){
        	tOccupationType = getOccupationType(tLCInsuredDB.getOccupationCode());
        }else{
        	
        	tOccupationType =tLCInsuredDB.getOccupationType();
        }
       // 如果性别和年龄改变则重算保额保费
        if ((StrTool.compareString(tLCInsuredDB.getSex(),
        		mEdorLDPersonInfo.getSex()) && (mEdorLDPersonInfo.getSex()!= null && !mEdorLDPersonInfo.getSex().equals(""))) ||
            (StrTool.compareString(tLCInsuredDB.getBirthday(),
            		mEdorLDPersonInfo.getBirthday()) && (mEdorLDPersonInfo.getBirthday()!= null && !mEdorLDPersonInfo.getBirthday().equals("")))
                                  ||
            (StrTool.compareString(tOccupationType,
            		mEdorLDPersonInfo.getOccupationType())
                                   && (mEdorLDPersonInfo.getOccupationType()!= null && !mEdorLDPersonInfo.getOccupationType().equals(""))
                                   )
                                   )
        {
            return true;
        }
        return false;
    }
    
    private String getOccupationType(String cOccupationCode){
    	String sql="select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"+cOccupationCode+"'  fetch first 2000 rows only ";
    	ExeSQL tExeSQL = new ExeSQL();     
    	String tOccupationType= tExeSQL.getOneValue(sql);
    	return tOccupationType;
    }

}
