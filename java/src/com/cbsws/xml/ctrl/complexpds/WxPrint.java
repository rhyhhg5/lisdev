package com.cbsws.xml.ctrl.complexpds;


import java.util.List;





import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.LCContINFO;
import com.cbsws.xml.ctrl.LCGrpContF1PBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDatasets;



public class WxPrint extends ABusLogic {
	
	public String PrtNo="";//印刷号
	public String SendDate="";//报文发送日期
	public String SendTime="";//报文发送时间
	public String BatchNo="";
	
	
	public LCContINFO cLCCONTINFO;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始处理网销复杂产品核保业务");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		SendDate = tMsgHead.getSendDate();
		SendTime = tMsgHead.getSendTime();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("LCContINFO");	       
	        cLCCONTINFO = (LCContINFO) tLCContList.get(0); //获取保单信息，只有一个保单
	        
	        PrtNo=cLCCONTINFO.getPrtNo();
	        if("".equals(PrtNo)||PrtNo==null){
	        	errLog("请确定传入印刷号，且格式为<PrtNo></PrtNo>");
	        	return false;
	        }
	        System.out.println(cLCCONTINFO.getAddress());
	        System.out.println(cLCCONTINFO.getUNIfiedSocialCreditNO());
	        System.out.println(PrtNo);
	        
	        SSRS ss1=new SSRS();
	        ExeSQL exeSQL=new ExeSQL();
	        ss1=exeSQL.execSQL("select * from ldsysvar where sysvar='ServerRoot'");
	        
	        String path=ss1.GetText(1, 1);
	        path="E:\\lisdev\\ui\\";
	        
	        String szTemplatePath = path+"/f1print/template/";	//模板路径
//	        String sOutXmlPath = path;	//xml文件输出路径
	        String sOutXmlPath="F://";
	        ss1.Clear();
	        String sql="SELECT GrpContNo,PrintCount,Operator,managecom FROM LCGrpCont A"
	        		+ " WHERE AppFlag in ('1','9') and ( PrintCount < 1 OR PrintCount IS NULL ) and (prtno like '18%%' or prtno like '20%%' or prtno like '91%%' or prtno like '99%%' or prtno like 'PR%' or prtno like 'SX%' or prtno like '05%' or prtno like '1E%' or prtno like '106%') "
	        		+ " and exists (select 1 from LDCode1 where CodeType = 'printchannel' and Code = substr(ManageCom, 1, 4) and Code1 = '2') "
	        		+ " and not exists (select 1 from lcgrppol where grpcontno = A.grpcontno and riskcode = '162001') "
	        		+ " and ContPrintType <> '5' " 
	        		+ " and prtno='"+PrtNo+"'";

	        ss1=exeSQL.execSQL(sql);
	        if(ss1.getMaxRow()==0){
	        	errLog("未查询到存在可打印的该印刷号的保单。");
	            return false;
	        }
	        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	    	tLCGrpContSchema.setGrpContNo(ss1.GetText(1, 1));
	        LCGrpContF1PBL bl = new LCGrpContF1PBL();
	        bl.mLcContINFO=cLCCONTINFO;
	        GlobalInput globalInput=new GlobalInput();
	        globalInput.ManageCom=ss1.GetText(1, 4);
	        globalInput.ComCode=ss1.GetText(1, 4);
	        globalInput.Operator=ss1.GetText(1, 3);
	        String flag=cLCCONTINFO.getPrintFlag();
	        if(!("Y".equals(flag)||"N".equals(flag))){
	        	errLog("PrintFlag填写错误,printflag只能为Y或N。");
	            return false;
	        }
	       
	        String printInsureDetail=cLCCONTINFO.getPrintFlag();
	        if("Y".equals(printInsureDetail.toUpperCase())){
	        	printInsureDetail="1";
	        	
	        }else{
	        	printInsureDetail="0";
	        }
	        String contPrintFlag="1";
	        VData vData = new VData();
	    	vData.add(globalInput);
	    	vData.addElement(tLCGrpContSchema);
	    	vData.add(szTemplatePath);
	    	vData.add(sOutXmlPath);
	    	vData.add(printInsureDetail);
	    	vData.add(contPrintFlag);
	        

	        if (!bl.submitData(vData, "PRINT"))
	        {
	            System.out.println(bl.mErrors.getErrContent());
	            errLog(bl.mErrors.getErrContent());
	            return false;
	        }
	        //getXmlResult();
		}catch (Exception e) {
			
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
		
		putResult("LCContTable", cLCCONTINFO);
		
		
	}
	

	
	

    


    public static void main(String[] args) {
    	
	}
    
}
