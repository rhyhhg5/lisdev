package com.cbsws.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.obj.LCInsuredTable;
import com.cbsws.obj.WrapParamTable;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;

public class NewWxCommon {

	private String cRiskWrapCode = "";
	
	
	public NewWxCommon(String tRiskWrapCode){
		cRiskWrapCode = tRiskWrapCode;
	}
	//  校验被保人年龄
    public boolean checkInsuredAge(List cWrapParamList){
//      校验年龄
    	if(cWrapParamList != null && cWrapParamList.size()>0){
    		for(int i=0;i<cWrapParamList.size();i++){
    			WrapParamTable tWrapParamTable = (WrapParamTable)cWrapParamList.get(i);
    			if("AppAge".equals(tWrapParamTable.getCalfactor())){
    				String tInsurrdAge = tWrapParamTable.getCalfactorValue();
    				String ageWrapSql = "select case when maxinsuredage is null then -1 else maxinsuredage end," +
            		" case when mininsuredage is null then -1 else mininsuredage end " +
            		" from ldriskwrap where riskwrapcode = '"+cRiskWrapCode+"' ";
        	        SSRS ageSSRS = new ExeSQL().execSQL(ageWrapSql);
        	        if(ageSSRS != null && ageSSRS.MaxRow>0){
        	        if (!"".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))) {
							if (Integer.parseInt(ageSSRS.GetText(1, 1)) < Integer.parseInt(tInsurrdAge)) {
								return false;
							}
						}
						if (!"".equals(StrTool.cTrim(ageSSRS.GetText(1, 2)))) {
							if (Integer.parseInt(ageSSRS.GetText(1, 2)) > Integer.parseInt(tInsurrdAge)) {
								return false;
							}
						}
        	        }
    			}
    		}
    	}
        
        return true;
    }
//  校验职业类别
    public boolean checkOccupationType(List cLCInsuredList){
    	String occutypeWrapSql = "select maxoccutype from ldwrap where riskwrapcode = '"+cRiskWrapCode+"' ";
        SSRS occutypeSSRS = new ExeSQL().execSQL(occutypeWrapSql);
        if(occutypeSSRS != null && occutypeSSRS.MaxRow>0){
        	if(!"".equals(StrTool.cTrim(occutypeSSRS.GetText(1, 1)))){
        		for(int i = 0;i<cLCInsuredList.size();i++){
        			LCInsuredTable tLCInsuredTable = (LCInsuredTable)cLCInsuredList.get(i);
        			String OccupationType = tLCInsuredTable.getOccupationType();
        			if(Integer.parseInt(occutypeSSRS.GetText(1, 1))<Integer.parseInt(OccupationType)
            				|| Integer.parseInt(OccupationType)<1){
            			return false;
            		}
        		}
        	}
        }
        return true;
    }
//    校验套餐与被保人只能是一个
    public boolean checkTheOne(List aList){
    	if(aList.size()>1){
    		return false;
    	}
    	return true;
    }
    /***
     * @author gzh
     * @date 20110615
     * @todo 校验产品是否已经停售
     * */
    public String checkWrapStop(String aPolApplyDate){
    	if(!PubFun.validateDate(aPolApplyDate)){
    		return "保单申请日期不是正确的日期格式。";
    	}
    	String sql = "select startdate,enddate from ldriskwrap where riskwrapcode = '"+cRiskWrapCode+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS != null && tSSRS.MaxRow>0){
    		if("".equals(tSSRS.GetText(1, 1)) || tSSRS.GetText(1, 1) == null){ //没有开始时间，则该产品还未开始销售
    			return "该产品为定义开始时间，不允许销售！";  
    		}else if("".equals(tSSRS.GetText(1, 2)) || tSSRS.GetText(1, 2) == null){//没有结束时间，则该产品未停止销售
    			return "";
    		}else{
    			FDate tFDate = new FDate();
    			if(tFDate.getDate(aPolApplyDate).after(tFDate.getDate(tSSRS.GetText(1, 2)))){
    				return "该产品已停售，不可购买！";
    			}
    		}
    	}else{
    		return "未获取到产品信息！";
    	}
    	return "";
    }
    /**
     * 校验身高体重
     * 规则如下：
     *	被保险人身高计算，超出范围提示身高超过年龄范围。
     *	小于或等于1周岁：【H＝50cm＋月龄*2.4cm】－30％<H<30％为OK
     *	2－15岁:【H＝年龄×7＋70cm】－30％<H<30％为OK
	 *	大于15岁:130cm<H<200cm为OK
	 *
	 *	被保险人体重计算，超出范围提示体重超过年龄范围。
	 *	出生体重W0初设为男 3.5kg，女3.0kg
	 *	小于180天：【W＝出生体重W0＋月龄*0.7Kg】－30％<W<30％为OK
	 *	181天－1岁：【W＝出生体重W0＋6*0.7Kg＋（月龄－6）*0.4Kg】－30％<W<30％为OK
	 *	2－15岁：【W＝年龄*2Kg＋8Kg】－30％<W<30％为OK
	 *	大于15岁：30Kg<W<100Kg为OK
     * @author fromyzf
     * @param List cLCInsuredList
     * @return String   "3"：年龄超过正常范围0-15岁；"2":体重超过正常范围； "1":身高超过正常范围;   "0"：身高和体重符合正常范围内
     * 
     */
    public String checkStatureAndAvoirdupois(LCInsuredTable cLCInsuredTable){
    	
    	//由参数cLCInsuredList获得所用信息
    	
    	String stature=cLCInsuredTable.getStature();//身高
    	String avoirdupois=cLCInsuredTable.getAvoirdupois();//体重
    	String birthday=cLCInsuredTable.getBirthday();
    	String sex=cLCInsuredTable.getSex();//0男1女
    	String currentDate=PubFun.getCurrentDate();
    	
    	//年龄、月龄、日龄
    	int ages=PubFun.calInterval(birthday,currentDate,"Y");
    	int months=PubFun.calInterval(birthday,currentDate,"M");
    	int days=PubFun.calInterval(birthday,currentDate,"D");
    	
    	//出生重量
    	String birthWeight=null;
    	if(sex.equals("0")){
    		birthWeight="3.5";
    	}else if(sex.equals("1")){
    		birthWeight="3";
    	}
    	
    	//范围最值：最小值-mins；最大值-maxs
    	double mins=0;
    	double maxs=0;
    	
    	//年龄校验0<=age<=15
    	if(months<0 || months>15*12){
    		System.out.println("年龄超出0-15岁的范围，请核查！");
    		return "3";
    	}
    	
    	//身高校验
    	if(months<=12){
    		mins = (50+months*2.4)*(1-0.3);
    		maxs = (50+months*2.4)*(1+0.3);
    	}else if(months>12 && months<24){
    		ages++;
    		mins = (ages*7+70)*(1-0.3);
    		maxs = (ages*7+70)*(1+0.3);
    	}else if(months>=12*2 && months<=15*12){
    		mins = (ages*7+70)*(1-0.3);
    		maxs = (ages*7+70)*(1+0.3);
    	}
    	if(Double.parseDouble(stature)<mins || Double.parseDouble(stature)>maxs){
    		System.out.println("身高超过正常范围");
    		return "1";
    	}
    	
    	//体重校验
    	if(months<=12){
    		if(days<=180){
    			mins = (Double.parseDouble(birthWeight)+(months*0.7))*(1-0.3);
    			maxs = (Double.parseDouble(birthWeight)+(months*0.7))*(1+0.3);
    		}else if(days>181){
    			mins = (Double.parseDouble(birthWeight)+(6*0.7)+(months-6)*0.4)*(1-0.3);
    			maxs = (Double.parseDouble(birthWeight)+(6*0.7)+(months-6)*0.4)*(1+0.3);
    		}
    	}else if(months>12 && months<24){
    		ages++;
    		mins = (ages*2+8)*(1-0.3);
    		maxs = (ages*2+8)*(1+0.3);
    	}else if(months>=12*2 && months<=15*12){
    		mins = (ages*2+8)*(1-0.3);
    		maxs = (ages*2+8)*(1+0.3);
    	}
    	if(Double.parseDouble(avoirdupois)<mins || Double.parseDouble(avoirdupois)>maxs){
    		System.out.println("体重超过正常范围");
    		return "2";
    	}
    	return "0";
    }

    public static void main(String[] args) {
//		int ss=PubFun.calInterval("2013-1-17", "2014-12-17", "M");
//		System.out.println("我的年龄是："+ss);
    	
//    	new NewWxCommon().checkStature(cLCInsuredList);
	}
    

}
