package com.cbsws.xml.ctrl.complexpds;


import java.util.ArrayList;
import java.util.List;

import org.apache.poi.util.SystemOutLogger;
import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.MonthBalaRate;
import com.cbsws.obj.WNNBPolMonth;
import com.cbsws.obj.WNNBPolMonth12;
import com.cbsws.obj.OmnipAnnalsQueryResult;
import com.cbsws.obj.WNReportQueryInfo;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.certify.SysOperatorNoticeBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BQWNYEARSPrintNewInface extends ABusLogic {
	private String BatchNo;//批次号
	private String MsgType;//报文类型
	private String Operator;//操作者
	private GlobalInput mGlobalInput = new GlobalInput();//公共信息
	private WNReportQueryInfo mWNReQuestInfo = new WNReportQueryInfo() ;
	private List<WNNBPolMonth> mWNNBPolMonthlist = new ArrayList<WNNBPolMonth>();
	private WNNBPolMonth12 mWNNBPolMonth12 = new WNNBPolMonth12();
	private List<MonthBalaRate> monthBalaRates = new ArrayList<MonthBalaRate>();
	private OmnipAnnalsQueryResult mWNNBPrinInfo = new OmnipAnnalsQueryResult();
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
    private ExeSQL mExeSQL = new ExeSQL();
    
    private String mPolYear = "";
    private String mPolMonth ="";
    private String mRunDateStart = ""; //批处理起期
    private String mRunDateEnd = "" ;   //
    SSRS mSSRS = new SSRS();

    SSRS tSSRS = new SSRS();
    private String mTempPolYear ="";
    private String mStartDate ="";
    private String mEndDate ="";
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		if(!parseXML(cMsgInfos)){
			return false;
		}
		
		if(!checkdata()){
			return false;
		}
//		
//		if(!getPolInfo()){
//			return false;
//		}
		
//		if(!creatReport()){
//			return false;
//		}
		
		return true;
	}
	
	 private boolean checkdata()
	 {
		 
      	 LCContDB tLCContDB = new LCContDB();
         tLCContDB.setContNo(mWNReQuestInfo.getContNo());
        if(!tLCContDB.getInfo())
        {
           errLog("没有查询到保单号为" + mWNReQuestInfo.getContNo() +"的保单信息。");
            return false;
        }
        mLCContSchema = tLCContDB.getDB().getSchema();
        
       String strsql =" select  p.polno from lmriskapp l,lcpol p where p.contno='"+tLCContDB.getContNo()+"' and  l.riskcode=p.riskcode and l.risktype4='4' and (TaxOptimal is null or TaxOptimal<>'Y')"
    	      + " union select pb.polno from lmriskapp l, lbpol pb where pb.contno ='"+tLCContDB.getContNo()+"' and l.riskcode = pb.riskcode and l.risktype4 = '4' and (TaxOptimal is null or TaxOptimal<>'Y')";

   	SSRS tSSRS = new ExeSQL().execSQL(strsql);
	if (tSSRS != null && tSSRS.getMaxRow() > 0) {
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
   	        LCPolDB tLCPolDB = new LCPolDB();
   	        tLCPolDB.setPolNo(tSSRS.GetText(i, 1));
   	        if(!tLCPolDB.getInfo())
            {
              errLog("没有查询到保单号为" + mWNReQuestInfo.getContNo() +"的险种信息。");
              return false;
            }
   	      
   	          mLCPolSchema = tLCPolDB.getSchema();

   	 		    if(!creatReport()){
   				 return false;
   			  }
   	        
		  }
	    }
	   else{
		      errLog("没有查询到保单号为" + mWNReQuestInfo.getContNo() +"的万能险种信息。");
              return false;
	}
       return true;
	 }
	 	 
    private boolean creatReport()
    {
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        mWNNBPrinInfo.setAgentName(tLaAgentDB.getName());
        mWNNBPrinInfo.setAgentCode(tLaAgentDB.getAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }

        mWNNBPrinInfo.setAgentPhone(agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                           +
                           " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                           +
                           " (select agentgroup from laagent where agentcode ='"
                           + tLaAgentDB.getAgentCode() + "'))"
                           ;
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL);
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            errLog("查询业务员机构失败");
            return false;
        }
        mWNNBPrinInfo.setAgentGroup(tLABranchGroupSet.get(1).getName());

        //获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLCContSchema.getContNo());

        if (!tLCAppntDB.getInfo()) {
            errLog("投保人表中缺少数据");
            return false;
        }

        String sql =" select * from LCAddress where CustomerNo=(select appntno from LCCont where contno='"
                   +  mLCContSchema.getContNo() + "') AND AddressNo = '"
                   +  tLCAppntDB.getAddressNo() + "'"
                   ;
        System.out.println("sql=" + sql);
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
        mLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).getSchema();
        mWNNBPrinInfo.setAppntName(tLCAppntDB.getAppntName()); //投保人姓名

        setFixedInfo();
        mWNNBPrinInfo.setGrpZipCode(mLCAddressSchema.getZipCode());
        System.out.println("GrpZipCode=" + mLCAddressSchema.getZipCode());
        mWNNBPrinInfo.setGrpAddress(mLCAddressSchema.getPostalAddress());
        String appntPhoneStr = " ";
        if (mLCAddressSchema.getPhone() != null &&
            !mLCAddressSchema.getPhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getPhone() + "、";
        }
        if (mLCAddressSchema.getHomePhone() != null &&
            !mLCAddressSchema.getHomePhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getHomePhone() + "、";
        }
        if (mLCAddressSchema.getCompanyPhone() != null &&
            !mLCAddressSchema.getCompanyPhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getCompanyPhone() + "、";
        }
        if (mLCAddressSchema.getMobile() != null &&
            !mLCAddressSchema.getMobile().equals("")) {
            appntPhoneStr += mLCAddressSchema.getMobile() + "、";
        }

        mWNNBPrinInfo.setContNo(mLCContSchema.getContNo());

        //被保人信息
        mWNNBPrinInfo.setInsuredName(mLCPolSchema.getInsuredName());

        //结算年度：
        mTempPolYear = new ExeSQL().getOneValue(
                "select polYear From lcinsureaccbalance where  year(DueBalaDate - 1 days) =" +
                mPolYear +
                " and polmonth = 12 and contno ='" +
                mLCContSchema.getContNo() +
                "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        //报告期间
        if (null !=mTempPolYear && !mTempPolYear.equals("")){
        if("1".equals(mTempPolYear))
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select min(DueBalaDate) From lcinsureaccbalance where polyear =" +
                    mTempPolYear +
                    " and polmonth = 1 and contno ='" +
                    mLCContSchema.getContNo() +
                    "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        }
        else
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select max(DueBalaDate)-1 month From lcinsureaccbalance where polyear =" +
                    mTempPolYear +
                    " and polmonth = 1 and contno ='" +
                    mLCContSchema.getContNo() +
                    "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        }
        }else {
        	errLog(mPolYear+"结算年度查询不到");
        	return false;
        }
        String baladate = "select max(DueBalaDate)-1 day,max(DueBalaDate) From lcinsureaccbalance where  PolYear = " 
        	   + mTempPolYear + " and polmonth = 12 and contno = '" + mLCContSchema.getContNo() 
        	   + "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ";
        SSRS dateSSRS = mExeSQL.execSQL(baladate);
        mEndDate = dateSSRS.GetText(1, 1);
 //       String tBalaDate = dateSSRS.GetText(1, 2);//结算日期

        mWNNBPrinInfo.setStartDate(CommonBL.decodeDate(mStartDate));//报告期间起
        mWNNBPrinInfo.setEndDate(CommonBL.decodeDate(mEndDate));//报告期间止

        if(!addNewInfo())
        {
        	return false;
        }

        getXmlResult();
        
        return true;
    }
    
    
    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCContSchema.getManageCom());
        tLDComDB.getInfo();
        mWNNBPrinInfo.setServicePhone(tLDComDB.getServicePhone());
        mWNNBPrinInfo.setLetterservicename(tLDComDB.getLetterServiceName());
        mWNNBPrinInfo.setServiceAddress(tLDComDB.getServicePostAddress());
        mWNNBPrinInfo.setServiceZip(tLDComDB.getLetterServicePostZipcode());
        mWNNBPrinInfo.setComName(tLDComDB.getLetterServiceName());
        mWNNBPrinInfo.setPrintDate(CommonBL.decodeDate(PubFun.getCurrentDate()));
        mWNNBPrinInfo.setFax(tLDComDB.getFax());
    }

//  20091118 req00000573 新增内容 zhanggm    
    private boolean addNewInfo()
    {
    	System.out.println("20091118 req00000573 新增内容新增内容开始————————————————————————");
        String tContNo = mLCContSchema.getContNo();
        String sql1 = "select (select riskname from lmrisk where riskcode = a.riskcode),cvalidate, "
        	        + "a.riskcode,prem,payintv,amnt,polno from lcpol a "
        	        + "where contno = '" + tContNo + "' and exists (select 1 from lmriskapp "
        	        + "where riskcode = a.riskcode and risktype4 = '" + BQ.RISKTYPE1_ULI + "')"
        	        + " and polno ='"+mLCPolSchema.getPolNo()+"' with ur";
        SSRS tSSRS1 = new SSRS();
        tSSRS1 = mExeSQL.execSQL(sql1);
        mWNNBPrinInfo.setRiskCodeName(tSSRS1.GetText(1, 1)); //产品名称
        mWNNBPrinInfo.setCValiDate(CommonBL.decodeDate(tSSRS1.GetText(1, 2))); //保单生效日
        
//    	首期缴纳进账户的保险费
    	sql1 = "select nvl(sum(money),0) from lcinsureacctrace where moneytype = 'BF' and contno = '" 
    		+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
    	double firstPrem1 = Double.parseDouble(mExeSQL.getOneValue(sql1));
    	
//    	首期缴纳保险费扣费
    	sql1 = "select nvl(sum(fee),0) from lcinsureaccfeetrace where moneytype = 'GL' and contno = '" 
    		+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
    	double firstFee = Double.parseDouble(mExeSQL.getOneValue(sql1));
        
//    	首期缴纳进账户的保险费
    	double firstPrem = firstPrem1 + firstFee;
    	
        String tRiskCode = tSSRS1.GetText(1, 3);
        sql1 = "select cpayflag from lmrisk where riskcode = '" + tRiskCode + "' with ur"; //cpayflag='Y'可续期
        String tCPayFlag = mExeSQL.getOneValue(sql1);
        
//      报告期间保险金额
        String tAmnt = null;
        tAmnt = tSSRS1.GetText(1, 6); 
        //新加保额算投保时的保额
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setAmnt(tSSRS1.GetText(1, 6));
        tLCPolSchema.setContNo(tContNo);
        tLCPolSchema.setPolNo(tSSRS1.GetText(1, 7));        
        double qianAmnt=CommonBL.getTBAmnt(tLCPolSchema);
        
        
        mWNNBPrinInfo.setAmnt(PubFun.setPrecision2(tAmnt)); //报告期间保险金额
        
//    	期缴保险费
    	double tPrem = 0.0;
//    	基本保险费
    	double payPrem = 0.0;
//    	额外保险费
    	double otherPrem = 0.0;
//    	趸缴保险费
    	double dunPrem = 0.0;
    	
        if(!"".equals(tCPayFlag) && "Y".equals(tCPayFlag))
        {
            if("1".equals(mTempPolYear))
            {
            	tPrem = firstPrem;
            }
            else
            {
//            	续期缴纳保险费
            	sql1 = "select nvl(sum(money),0) From lcinsureacctrace a where contno = '" + tContNo 
        			+ "' and polno ='"+mLCPolSchema.getPolNo()+"' and moneytype = 'BF' and othertype = '2' and exists (select 1 from ljapayperson where contno = "  
        			+ "a.contno and year(curpaytodate- 1 day) = " + mPolYear + " and riskcode = a.riskcode and getnoticeno = a.otherno) with ur";
            	tPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));
            }
            dunPrem = 0.0;
            String sqlCode="select 1 from ldcode where codetype='uliprint' and code='"+tRiskCode+"' and codealias='2'";
            String rs = new ExeSQL().getOneValue(sqlCode);
        	if(!rs.equals("") && !rs.equals("null"))
        	{
        		System.out.println("健康宝(330801，331501,331801)的基本保费为3者中最小值：1、期交保费，2、6000 元，3、本结算年度的基本保险金额（如果基本保险金额本年度有调整，以调整后的为准）除以20。");
        		String tPayIntv = tSSRS1.GetText(1, 5);
        		double prem1 = tPrem; //期交保费
        		double prem2 = 6000 * Double.parseDouble(tPayIntv) / 12; //6000 元
        		double prem3 = qianAmnt * Double.parseDouble(tPayIntv) / (20*12); //本结算年度的基本保险金额除以20
        		payPrem = prem1<=prem2?(prem1<=prem3?prem1:prem3):(prem2<=prem3?prem2:prem3);//取3者中最小值
        		otherPrem = prem1-payPrem; //额外保险费=期缴-基本
        		System.out.println("基本保险费="+CommonBL.carry(payPrem)+",额外保险费="+CommonBL.carry(otherPrem));//基本保险费
        	}
        	else
        	{
        		payPrem = tPrem;
        		otherPrem = 0.0;
        	}
        }
        else
        {
        	tPrem = 0.0;
        	payPrem = 0.0;
        	otherPrem = 0.0;
        	dunPrem = firstPrem;
        }
        
        mWNNBPrinInfo.setPrem(PubFun.setPrecision2(tPrem)); //期缴保险费
        mWNNBPrinInfo.setPayPrem(PubFun.setPrecision2(payPrem)); //基本保险费
        mWNNBPrinInfo.setOtherPrem(PubFun.setPrecision2(otherPrem)); //额外保险费
        mWNNBPrinInfo.setDunPrem(PubFun.setPrecision2(dunPrem)); //趸缴保险费
        
        sql1 = "select nvl(sum(money),0) from lcinsureacctrace where moneytype = 'ZF' and contno = '" 
        	+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
        double tbAddPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));//首期追加保险费
 		
        sql1 = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '" 
        	 + tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
        double bqAddPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));//保全追加保费
        
        double addPrem = 0.0; 
        if("1".equals(mTempPolYear))
        {
        	addPrem = tbAddPrem + bqAddPrem;
        }
        else
        {
        	addPrem = bqAddPrem;
        }
        mWNNBPrinInfo.setAddPrem(PubFun.setPrecision2(addPrem)); //追加保险费：本结算年度的追加保费之和  
        
        double totalPrem = tPrem + dunPrem + addPrem;
        mWNNBPrinInfo.setTotalPrem(PubFun.setPrecision2(totalPrem)); //累计保险费：期缴+趸缴+追加
        
        if("1".equals(mTempPolYear))
        {
        	sql1 = "select sum(fee) from lcinsureaccfeetrace where contno = '" + tContNo 
        		+ "' and polno ='"+mLCPolSchema.getPolNo()+"' and moneytype in ('GL','KF') and othertype = '1' with ur";
        }
        else
        {
        	sql1 = "select nvl(sum(fee),0) From lcinsureaccfeetrace a where contno = '" + tContNo 
    			+ "' and polno ='"+mLCPolSchema.getPolNo()+"' and moneytype = 'KF' and othertype = '2' and exists (select 1 from ljapayperson where contno = "  
    			+ "a.contno and year(curpaytodate-1 day) = " + mPolYear + " and riskcode = a.riskcode and getnoticeno = a.otherno) with ur";
        }
        double fee1 = Double.parseDouble(mExeSQL.getOneValue(sql1));//续期or首期费用
        
        sql1 = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype = 'KF' and contno = '" 
         	 + tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
        double fee2 = Double.parseDouble(mExeSQL.getOneValue(sql1));//保全追加保费扣费
        
        double fee = fee1 + fee2;
        mWNNBPrinInfo.setFee(PubFun.setPrecision2(fee));//初始费用（扣除项）：本结算年度的初始费用之和
        
        sql1 = "select nvl(sum(b.money),0) "
        	+ "from LCInsureAccbalance a , lcinsureacctrace b "
        	+ "where a.contno = '" + tContNo + "' and a.polno ='"+mLCPolSchema.getPolNo()+"' and a.polyear = " + mTempPolYear 
        	+ " and b.moneytype = 'B' and a.sequenceno = b.otherno with ur ";
        mWNNBPrinInfo.setBMoney(PubFun.setPrecision2(mExeSQL.getOneValue(sql1)));//持续奖金：本结算年度的持续奖金之和
        
        
//      生成结算利息列表,报告期内各月年化结算利率及结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthRate(tContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
        		errLog("打印终止，获取保单"+tContNo+"第"+mTempPolYear+"年"+tPolMonth+"月年化利率失败。");
                return false;
            }
        	
        }
   
        
		ExeSQL tExeSQL = new ExeSQL();
        String sql="select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql);
        mWNNBPrinInfo.setYear1(tSSRS.GetText(1, 1));
        mWNNBPrinInfo.setQuarter1(tSSRS.GetText(1, 2));
        mWNNBPrinInfo.setSolvency(tSSRS.GetText(1, 3));
        mWNNBPrinInfo.setFlag(tSSRS.GetText(1, 4));
        
        String sql2="select Year,quarter,riskrate  from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql2);
        mWNNBPrinInfo.setYear2(tSSRS.GetText(1, 1));
        mWNNBPrinInfo.setQuarter2(tSSRS.GetText(1, 2));
        mWNNBPrinInfo.setRiksRate(tSSRS.GetText(1, 3));
        
//      生成各月结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthList(tContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
        		errLog("打印终止，获取保单"+tContNo+"第"+mTempPolYear+"年"+tPolMonth+"月结算信息明细失败。");
                return false;
            }
        }
        System.out.println("20091118 req00000573 新增内容新增内容结束————————————————————————");
    	return true;
    }
    
    
//  生成结算利息列表,报告期内各月年化结算利率及结算信息
    private void setMonthRate(String aContNo,String aRiskCode,String aPolMonth)
    {
        MonthBalaRate tonthBalaRate = new MonthBalaRate();
    	String sql = "";
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance where polYear = " 
			   + mTempPolYear + " and polmonth = " + aPolMonth + " and contno ='" + aContNo 
			   + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	tonthBalaRate.setRateMonth( decodeDate(tEndDate));
    	
    	sql = "select rate From LMInsuAccRate where riskcode = '" + aRiskCode 
    		+ "' and baladate = '" + tDueBalaDate + "' with ur";
     	String rate = mExeSQL.getOneValue(sql);
     	tonthBalaRate.setRate(String.valueOf(Double.valueOf(rate))); //年化结算利率
     	monthBalaRates.add(tonthBalaRate);
     	
    }
    private void setMonthList(String aContNo,String aRiskCode,String aPolMonth)
    {

    //	WNNBPolMonth12 mWNNBPolMonth12 = null;
    	WNNBPolMonth mWNNBPolMonth = null;
    	if("12".equals(aPolMonth)){
    		mWNNBPolMonth12 =  new WNNBPolMonth12();
    	}
    	else {
    		mWNNBPolMonth =  new WNNBPolMonth();
    	}
    	
    	String sql = "";
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
        	sql = "select min(DueBalaDate) From lcinsureaccbalance where polyear = 1 " 
        		+ "and polmonth = 1 and contno = '" + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
        }
        else
        {
        	sql = "select max(DueBalaDate)-1 month From lcinsureaccbalance where polyear = " 
        		+ mTempPolYear + " and polmonth = " + aPolMonth + " and contno = '" + aContNo + "'"
        		+ " and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
        }
        String tStartDate = mExeSQL.getOneValue(sql); //本月开始时间
        
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance where polYear = " 
			   + mTempPolYear + " and polmonth = " + aPolMonth + " and contno ='" + aContNo 
			   + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    
       	if("12".equals(aPolMonth)){
       		mWNNBPolMonth12.setMonth(decodeDate(tEndDate));
    	}else {
    		mWNNBPolMonth.setMonth(decodeDate(tEndDate));
    	}
    	
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
    		sql = "select nvl(sum(money),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
        else
        {
        	sql = "select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
    	
       	if("12".equals(aPolMonth)){
        	mWNNBPolMonth12.setStartMoney(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期初帐户价值
        	   
    	}else {
        	mWNNBPolMonth.setStartMoney(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期初帐户价值       	   
    	}
     
    	sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '" 
       	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + tStartDate + "' and '"
       	 + tEndDate + "' and othertype = '"
       	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
     	if("12".equals(aPolMonth)){
        	mWNNBPolMonth12.setBQAddPrem(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费
        	         	   
    	}else {
        	mWNNBPolMonth.setBQAddPrem(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费
        	  	}
    	sql = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype ='KF' and contno = '" 
          	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + tStartDate + "' and '"
          	 + tEndDate + "' and othertype = '"
          	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
     	if("12".equals(aPolMonth)){
        	mWNNBPolMonth12.setBQAddPremFee(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费扣费
        	           	   
    	}else {
        	mWNNBPolMonth.setBQAddPremFee(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费扣费  
    	   }	
       	if("12".equals(aPolMonth))
       	{
       		sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype = 'BF' and contno = '" 
             	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" 
             	 + tStartDate + "' and '"+ tEndDate + "' and othertype = '2' with ur";
       		mWNNBPolMonth12.setNewPrem(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //下期期缴保费
          
          	sql = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype = 'GL' and contno = '" 
            	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" 
            	 + tStartDate + "' and '"+ tEndDate + "' and othertype = '2' with ur";
         	mWNNBPolMonth12.setNewPremFee(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //下期期缴保费初始费用
       	}
       	    	
    	sql = "select nvl(sum(case when moneytype = 'LX' then money end),0), "
          	 + "nvl(sum(case when moneytype = 'RP' then -money end),0), " 
          	 + "nvl(sum(case when moneytype = 'MF' then -money end),0) " 
          	 + "from lcinsureacctrace a where contno = '" 
          	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()
          	 +"' and otherno in (select sequenceno from lcinsureaccbalance where polYear = " 
         	 + mTempPolYear + " and polmonth = " + aPolMonth + " and contno = a.contno) with ur ";
    	SSRS tSSRS2 = mExeSQL.execSQL(sql);
     	if("12".equals(aPolMonth)){
   
        	mWNNBPolMonth12.setLXMoney(PubFun.setPrecision2(tSSRS2.GetText(1, 1))); //保单帐户结算收益
         	mWNNBPolMonth12.setRPMoney(PubFun.setPrecision2(tSSRS2.GetText(1, 2))); //风险保险费
    	    mWNNBPolMonth12.setMFMoney(PubFun.setPrecision2(tSSRS2.GetText(1, 3))); //保单管理费
      	           	   
    	}else {
        	mWNNBPolMonth.setLXMoney(PubFun.setPrecision2(tSSRS2.GetText(1, 1))); //保单帐户结算收益
        	mWNNBPolMonth.setRPMoney(PubFun.setPrecision2(tSSRS2.GetText(1, 2))); //风险保险费
        	mWNNBPolMonth.setMFMoney(PubFun.setPrecision2(tSSRS2.GetText(1, 3))); //保单管理费
           } 
        sql = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype = 'LQ' and othertype = '"
     	     + BQ.NOTICETYPE_P + "' and contno = '" + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate >= '"
     	     + tStartDate + "' and paydate <= '" + tEndDate + "' with ur";
       if("12".equals(aPolMonth)){
    	      mWNNBPolMonth12.setLQMoney(PubFun.setPrecision2(mExeSQL.getOneValue(sql)));//部分领取
    	     	           	   
    	}else {
    	      mWNNBPolMonth.setLQMoney(PubFun.setPrecision2(mExeSQL.getOneValue(sql)));//部分领取
    	      }
//      本月期末帐户价值
        sql = "select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) "
        	+ "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tDueBalaDate + "' with ur ";
       if("12".equals(aPolMonth)){
           mWNNBPolMonth12.setEndMoney(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期末帐户价值
            	           	   
     	}else {
            mWNNBPolMonth.setEndMoney(PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期末帐户价值
               }
       if(!"12".equals(aPolMonth)){
        mWNNBPolMonthlist.add(mWNNBPolMonth);
       }
        
    }
    
    
	//返回报文
	public void getXmlResult(){
		
		putResult("OmnipAnnalsQueryResult", mWNNBPrinInfo);

		for(int i=0;i<monthBalaRates.size();i++){
			putResult("MonthBalaRate", (MonthBalaRate)monthBalaRates.get(i));
		}
		
		for(int j=0;j<mWNNBPolMonthlist.size();j++){
			if (j==10){
				putResult("PolMonthBalaInfo", (WNNBPolMonth)mWNNBPolMonthlist.get(j));
				putResult("PolMonthBalaInfo", mWNNBPolMonth12);
			}else {
			putResult("PolMonthBalaInfo", (WNNBPolMonth)mWNNBPolMonthlist.get(j));
			}
		}

	}	
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List<WNReportQueryInfo> tWNReQuestInfo = cMsgInfos.getBodyByFlag("WNReportQueryInfo");
		
        if (tWNReQuestInfo == null || tWNReQuestInfo.size() != 1)
        {
            errLog("申请报文中获取报文体失败。");
            return false;
        }
        mWNReQuestInfo = (WNReportQueryInfo)tWNReQuestInfo.get(0);
        mPolYear = mWNReQuestInfo.getPolYear();
        return true;
	}
    /**
     * 把型如"2009-01-01"格式的日期转为"2009年01月"格式
     * @param date String
     * @return String
     */
    private String decodeDate(String date)
    {
        if ((date == null) || (date.equals("")))
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        String data[] = date.split("-");
        if (data.length != 3)
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        return (data[0] + "年" + data[1] + "月");
    }
}
