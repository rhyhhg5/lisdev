package com.cbsws.xml.ctrl.complexpds;


import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CrsSaleInfo;
import com.cbsws.obj.RYAppnt;
import com.cbsws.obj.RYBnfInfo;
import com.cbsws.obj.RYInsuredInfo;
import com.cbsws.obj.RYInsuredRiskInfo;
import com.cbsws.obj.RYOtherInfo;
import com.cbsws.obj.RYRiskInfo;
import com.cbsws.obj.RYSingleContInfo;
import com.cbsws.obj.RYSubInfo;
import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;



public class RYPrem extends ABusLogic {
	
	public String GrpNo="";//印刷号
	public String SendDate="";//报文发送日期
	public String SendTime="";//报文发送时间
	public String BatchNo="";
	public String PassWord;
	public String mContNo; //合同号
	
	public RYSingleContInfo cLoginINFO;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始登录验证");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		SendDate = tMsgHead.getSendDate();
		SendTime = tMsgHead.getSendTime();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("RYSingleContInfo");		
			for(int i=0;i<tLCContList.size();i++){
				cLoginINFO=new RYSingleContInfo();
				cLoginINFO = (RYSingleContInfo) tLCContList.get(i); //获取保单信息，只有一个保单
		        String contno=cLoginINFO.getContNo();
		        System.out.println("contno:"+contno);
		        if(contno==null||"".equals(contno)){
		        	errLog("请确认是否传入ContNo");
		        	return false;
		        }else {
		        	mContNo=contno;
				}
		        
				prepareData();
			}
			
			
			
	        
	        
	       
	        //getXmlResult();
		}catch (Exception e) {
			System.out.println(e);
			errLog("未知错误，请检查报文格式");
        	return false;
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
		
		
		
		
	}
	public boolean prepareData() {
		QueryTool qTool = new QueryTool();		
		ExeSQL mExeSQL=new ExeSQL();
		// 查询业务数据并封装
		
		//险种信息
		String sql1 = " select b.riskcode,(select riskname from lmrisk where riskcode=b.riskcode),'' 代码简称,a.amnt,b.enddate , a.prem,b.payintv ,"
				+ "b.payyears ,b.years ,b.PayEndYear ,b.PayEndYearFlag,b.insuyear ,b.insuyearFlag,b.mult  " +
						"from lcduty a ,lcpol b where a.polno = b.polno and b.contno = '"
				+ mContNo + "' with ur";
				
					
		SSRS riskSSRS = mExeSQL.execSQL(sql1);
		
		for (int i = 1; i <= riskSSRS.MaxRow; i++) {
			RYRiskInfo ryRiskInfo=new RYRiskInfo();
			ryRiskInfo.setRiskCode(riskSSRS.GetText(i, 1));
			ryRiskInfo.setRiskName(riskSSRS.GetText(i, 2));
			if(riskSSRS.GetText(i, 1).equals("121001")){				
				if(riskSSRS.GetText(i, 14).equals("1"))ryRiskInfo.setAmnt("一档");
				else if(riskSSRS.GetText(i, 14).equals("2"))ryRiskInfo.setAmnt("二档");
				else if(riskSSRS.GetText(i, 14).equals("3"))ryRiskInfo.setAmnt("三档");
				else ryRiskInfo.setAmnt(riskSSRS.GetText(i, 4));
			}else{
				ryRiskInfo.setAmnt(riskSSRS.GetText(i, 4));
			}
			LoginVerifyTool commonTool = new LoginVerifyTool();

			
			ryRiskInfo.setPrem(riskSSRS.GetText(i, 6));
			//转换缴费间隔
			String payIntvName = qTool.decodePayIntv(riskSSRS.GetText(i, 7));			
			
			ryRiskInfo.setPaySep(payIntvName);// 缴费间隔
			//缴费年期（终交年龄年期） 拼接
			String payEndYearFlag = riskSSRS.GetText(i, 11);
			
			String payEndYearFlagStr = commonTool.switchInsuYearFlag(payEndYearFlag);
			ryRiskInfo.setPayTerm(riskSSRS.GetText(i, 10)+payEndYearFlagStr);;
			ryRiskInfo.setPayDate("");
			putResult("RYRiskInfo", ryRiskInfo);
			
		}
		
		// 投保人信息
		String appntInfoSql = "select a.appntname,codename('sex',a.appntsex),a.appntbirthday,codename('idtype',a.idtype),"
				+ "a.idno,codename('marriage',a.marriage),codename('nativeplace',a.nativeplace),a.occupationcode ,"
				+ "b.mobile ,b.companyphone  办公电话,b.homephone  住宅电话,b.postaladdress ,b.homezipcode ,b.email "

				+ ",b.phone  联系电话  "
				+ "from Lcappnt a,lcaddress b where a.contno='"
				+ mContNo
				+ "' and b.customerno=a.appntno and b.addressno=a.addressno fetch first 1 rows only";
		SSRS appntInfoSSRS = mExeSQL.execSQL(appntInfoSql);

		
		for (int i = 1; i <= appntInfoSSRS.MaxRow; i++) {
			RYAppnt ryAppnt=new RYAppnt();
			
			ryAppnt.setAppntName(appntInfoSSRS.GetText(i, 1));
			ryAppnt.setAppntSex(appntInfoSSRS.GetText(i, 2));
			ryAppnt.setAppntIdType(appntInfoSSRS.GetText(i, 4));
			ryAppnt.setAppntIdNo(appntInfoSSRS.GetText(i, 5));
			ryAppnt.setAppntBirthday(appntInfoSSRS.GetText(i, 3));
			ryAppnt.setAppntMarryState(appntInfoSSRS.GetText(i, 6));
			ryAppnt.setAppntNativePlace(appntInfoSSRS.GetText(i, 7));
			ryAppnt.setAppntOccuPation(qTool.decodeOccupationcode(appntInfoSSRS.GetText(i, 8)) );
			ryAppnt.setAppntMobile(appntInfoSSRS.GetText(i, 9));
			ryAppnt.setAppntCompanyPhone(appntInfoSSRS.GetText(i, 10));
			ryAppnt.setAppntHomePhone(appntInfoSSRS.GetText(i, 11));
			ryAppnt.setAppntAddress(appntInfoSSRS.GetText(i, 12));
			ryAppnt.setAppntZipCode(appntInfoSSRS.GetText(i, 13));
			ryAppnt.setAppntEmail(appntInfoSSRS.GetText(i, 14));
			ryAppnt.setAppntPhone(appntInfoSSRS.GetText(i, 15));
			putResult("RYAppnt", ryAppnt);			

			
		}
		if(appntInfoSSRS.getMaxRow() == 0){  //团单
			String sql = "select a.appntname "
					+ "from lccont a where a.contno='" + mContNo + "'";
			SSRS rs = mExeSQL.execSQL(sql);
			if(rs.getMaxRow() > 0){
				RYAppnt ryAppnt=new RYAppnt();				
				ryAppnt.setAppntName(rs.GetText(1, 1));
				putResult("RYAppnt", ryAppnt);	
			}
		}
		
		
	
		
		
		//*******************************************
		
//		 被保人险种信息 add by fuxin 
		String riskString = "select insuredname "
							+",(select idno from lcinsured where a.contno= contno and a.insuredno = insuredno) "
							+",(select riskname from lmriskapp where a.riskcode = riskcode) "
							+",insuyear ,enddate,payintv,payyears,amnt "							
							+",prem,CValiDate,paytodate,codename('stateflag',stateflag)"
							+" from lcpol a where contno ='"+mContNo+"'"
							+" group by a.contno,insuredname,a.insuredno,a.riskcode,insuyear,enddate,payintv,payyears,amnt,prem,CValiDate,paytodate,STATEFLAG,a.riskseqno "
							+" order by a.riskseqno"; 
		SSRS riskSSRS1 = new ExeSQL().execSQL(riskString);
		
		

			for (int i = 1 ; i<=riskSSRS1.getMaxRow(); i++ )
			{
				RYInsuredRiskInfo ryInsuredRiskInfo=new RYInsuredRiskInfo();
				ryInsuredRiskInfo.setIRInsuredName(riskSSRS1.GetText(i, 1));
				ryInsuredRiskInfo.setIRInsuredIdNo(riskSSRS1.GetText(i, 2));
				ryInsuredRiskInfo.setIRRiskName(riskSSRS1.GetText(i, 3));
				ryInsuredRiskInfo.setIRTerm(riskSSRS1.GetText(i, 4)+"年");
				ryInsuredRiskInfo.setIREndDate(riskSSRS1.GetText(i, 5));
				ryInsuredRiskInfo.setIRPaySep(qTool.decodePayIntv(riskSSRS1.GetText(i, 6)));
				ryInsuredRiskInfo.setIRPayTerm(riskSSRS1.GetText(i, 7)+"年");
				ryInsuredRiskInfo.setIRAmnt(riskSSRS1.GetText(i, 8));
				ryInsuredRiskInfo.setIRPrem(riskSSRS1.GetText(i, 9));
				ryInsuredRiskInfo.setIRCvaliDate(riskSSRS1.GetText(i, 10));
				ryInsuredRiskInfo.setIRPaytoDate(riskSSRS1.GetText(i, 11));
				ryInsuredRiskInfo.setIRRiskState(riskSSRS1.GetText(i, 12));
				
				putResult("RYInsuredRiskInfo", ryInsuredRiskInfo);
				
			}
			
		
		//*******************************************

		// 被保人信息
		String insuredInfoSql = "select a.insuredno 被保人编号,(select codename from ldcode where codetype = 'relation' and code = a.relationtoappnt ) 与投保人关系,"
				+ "(case  relationtomaininsured when '00' then '主被保人' else '连带' end) 当前被保人连带与否 ,"
				+ " (select codename from ldcode where codetype = 'relation' and code = a.relationtomaininsured) 与主被保人关系,"
				+ "'' 主被保人编号,a.sequenceno 被保人顺序,a.name 被保人姓名,codename('sex',a.sex) 被保人性别,"
				+ "codename('idtype',a.idtype),a.idno,a.birthday,codename('marriage',a.marriage),codename('nativeplace',a.nativeplace) 国籍,"
				+ "a.OccupationCode 职业,a.addressno from Lcinsured a  where a.contno  = '"
				+ mContNo + " '";
		SSRS insuredInfoSSRS = mExeSQL.execSQL(insuredInfoSql);

		
		for (int i = 1; i <= insuredInfoSSRS.MaxRow; i++) {
			RYInsuredInfo ryInsuredInfo=new RYInsuredInfo();
			
			ryInsuredInfo.setInsuNo(insuredInfoSSRS.GetText(i, 1));
			ryInsuredInfo.setToAppntRela(insuredInfoSSRS.GetText(i, 2));
			ryInsuredInfo.setRelaTionCode(insuredInfoSSRS.GetText(i, 3));
			ryInsuredInfo.setRelationToInsured(insuredInfoSSRS.GetText(i,
					4));
			ryInsuredInfo.setReInsuNo(insuredInfoSSRS.GetText(i, 5));
			ryInsuredInfo.setInsuredOrder(insuredInfoSSRS.GetText(i, 6));
			ryInsuredInfo.setInsuredName(insuredInfoSSRS.GetText(i, 7));
			ryInsuredInfo.setInsuredSex(insuredInfoSSRS.GetText(i, 8));
			ryInsuredInfo.setInsuredIdType(insuredInfoSSRS.GetText(i, 9));
			ryInsuredInfo.setInsuredIdNo(insuredInfoSSRS.GetText(i, 10));
			ryInsuredInfo.setInsuredBirthday(insuredInfoSSRS
					.GetText(i, 11));
			ryInsuredInfo.setInsuredMarryState(insuredInfoSSRS.GetText(i,
					12));
			ryInsuredInfo.setInsuredNativePlace(insuredInfoSSRS.GetText(i,
					13));			
			
			//decode 职业编码
			String occupation =  qTool.decodeOccupationcode(insuredInfoSSRS.GetText(i,
					14));			
			
			ryInsuredInfo.setInsuredOccupaTion(occupation);
			String insuNo = insuredInfoSSRS.GetText(i, 1);
			String addressno = insuredInfoSSRS.GetText(i, 15);
			String addressInfoSql = "select b.mobile  移动电话,b.companyphone  办公电话,b.homephone  住宅电话,b.postaladdress  联系地址,"
					+ "b.zipcode,b.email  "
					//modify by zhangyige at 2014-08-19 微信项目查询增加 联系电话
					+ ",b.phone  联系电话  "
					+ "from lcaddress b where b.customerno='"
					+ insuNo + "' and addressno = '"+addressno+"' fetch first 1 rows only";
			SSRS addressInfoSSRS = mExeSQL.execSQL(addressInfoSql);
			ryInsuredInfo.setInsuredMobile(addressInfoSSRS.GetText(1, 1));
			ryInsuredInfo.setInsuredCompanyPhone(addressInfoSSRS.GetText(
					1, 2));
			ryInsuredInfo.setInsuredHomePhone(addressInfoSSRS
					.GetText(1, 3));
			ryInsuredInfo.setInsuredAddress(addressInfoSSRS.GetText(1, 4));
			ryInsuredInfo.setInsuredZipcode(addressInfoSSRS.GetText(1, 5));
			ryInsuredInfo.setInsuredEmail(addressInfoSSRS.GetText(1, 6));
			ryInsuredInfo.setInsuredPhone(addressInfoSSRS.GetText(1, 7));
			putResult("RYInsuredInfo", ryInsuredInfo);
			
		}
		

		// 受益人信息
		// modify by fuxin 2010-10-14 查询受益人信息后需要分组 不然会有重复记录。
		String bnfInfoSql = " select a.name,codename('idtype',a.idtype),a.idno,a.bnfgrade  收益顺位,a.bnflot*100 收益比例 ,a.InsuredNo 被保人客户号,"
				+ "c.riskname 险种名称 ,(select codename from ldcode where codetype = 'relation' and code = a.RelationToInsured ) 所属被保人关系"
				+ " from lcbnf a,lcpol b,Lmriskapp c where a.contno  = '"
				+ mContNo
				+ " ' and a.contno = b.contno and b.riskcode = c.riskcode group by a.name,a.idtype,a.idno,a.bnfgrade,a.bnflot,a.InsuredNo,c.riskname,a.RelationToInsured  ";
		SSRS bnfInfoSSRS = mExeSQL.execSQL(bnfInfoSql);

		
		for (int i = 1; i <= bnfInfoSSRS.MaxRow; i++) {
			RYBnfInfo ryBnfInfo=new RYBnfInfo();
			ryBnfInfo.setToInsuNo(bnfInfoSSRS.GetText(i, 6));			
			ryBnfInfo.setRName(bnfInfoSSRS.GetText(i, 7));
			ryBnfInfo.setToInsuRela(bnfInfoSSRS.GetText(i, 8));
			ryBnfInfo.setBnfGrade( bnfInfoSSRS.GetText(i, 4));
			ryBnfInfo.setBnfRate(bnfInfoSSRS.GetText(i, 5));
			ryBnfInfo.setBnfName(bnfInfoSSRS.GetText(i, 1));
			ryBnfInfo.setBnfIdtype(bnfInfoSSRS.GetText(i, 2));
			ryBnfInfo.setBnfIdno(bnfInfoSSRS.GetText(i, 3));
			
			putResult("BnfInfo", ryBnfInfo);
		}
		
		
		//****************************************
		//查询新契约回访结果 BY LI CAIYAN 2012-11-1
		
		String returnVisitSQL = "select (case returnvisitflag when '1' then '健康件' when '2' then '问题件'  when '3' then '拒访件' when '4' then '书面回访件'  when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable  where policyno = '"+mContNo+"' ";	
		SSRS returnVisitSSRS = mExeSQL.execSQL(returnVisitSQL);
		RYOtherInfo ryOtherInfo=new RYOtherInfo();
		if (returnVisitSSRS.MaxRow > 0) {			
			ryOtherInfo.setReturnVistInfo(returnVisitSSRS.GetText(1, 1));

		}
		
      //**********************************************
		String sql2 = "select a.contno,a.ManageCom, d.name 销售机构 ,b.name 服务人员姓名,b.phone 服务人员电话,Cvalidate,signdate,"
				+ "CustomGetPolDate,Getpoldate 回单录入日期 ," +
						"codename('stateflag', stateflag)  保单状态,d.branchaddress, " +
						"c.name 销售机构名称 ,c.address 销售机构地址 "+    //BY GZH 增加销售机构本级名称和地址
						"from Lccont a,laagent b ,ldcom c ,labranchgroup d "
				+ " where contno='"
				+ mContNo
				+ "' and a.agentcode = b.agentcode and a.Managecom=c.comcode and a.agentgroup=d.agentgroup ";
//		 缴费方式
		String payModeSql = "select bankaccno from ljspay where  otherno='"
				+ mContNo + "'";
		String payModeCode = mExeSQL.getOneValue(payModeSql);
		String payMode = qTool.decodePayMode(payModeCode);

		SSRS contSSRS = mExeSQL.execSQL(sql2);
		if (contSSRS.MaxRow > 0) {
			
			ryOtherInfo.setContNo(contSSRS.GetText(1, 1));
			ryOtherInfo.setManageCom(contSSRS.GetText(1, 2));
			ryOtherInfo.setSaleComName(contSSRS.GetText(1, 12));
			ryOtherInfo.setSaleComAddress(contSSRS.GetText(1, 13));
			ryOtherInfo.setAgentName(contSSRS.GetText(1, 4));
			ryOtherInfo.setAgentTelePhone(contSSRS.GetText(1, 5));
			ryOtherInfo.setCvaliDate(contSSRS.GetText(1, 6));
			ryOtherInfo.setSignDate(contSSRS.GetText(1, 7));
			ryOtherInfo.setCustomGetPolDateString(contSSRS.GetText(1, 8));
			ryOtherInfo.setGetPolDate(contSSRS.GetText(1, 9));
			ryOtherInfo.setContState(contSSRS.GetText(1, 10));
			ryOtherInfo.setPayLocation(payMode);
			
			ryOtherInfo.setIsAutoPay("");
			putResult("RYOtherInfo", ryOtherInfo);

		}
		String prtno=mExeSQL.getOneValue("select prtno from lccont where contno='"+mContNo+"'");
		
		String Subsql="select (case when taxflag='1' then '个人' when taxflag='2' then '团体' end),"
				+ "TaxNo, (case when transflag='0' then '新单' when transflag='1' then '转入' end),"
				+ "(case when TaxPayerType='01' then '代扣代缴' when taxpayertype='02' then '自行申报' end),"
				+ "TaxCode,ErrorInfo,GrpNo,CreditCode,gtaxno,gorgancomcode,premmult from lccontsub where prtno='"+prtno+"'";
		SSRS subSsrs=mExeSQL.execSQL(Subsql);
		if(subSsrs.getMaxRow()==0){
			
		}else{
			for(int i=1;i<=subSsrs.getMaxRow();i++){
				RYSubInfo rySubInfo=new RYSubInfo();
				rySubInfo.setTaxFlag(subSsrs.GetText(i, 1));
				rySubInfo.setTaxNo(subSsrs.GetText(i, 2));
				rySubInfo.setTransFlag(subSsrs.GetText(i, 3));
				rySubInfo.setTaxPayerType(subSsrs.GetText(i, 4));
				rySubInfo.setTaxCode(subSsrs.GetText(i, 5));
				rySubInfo.setErrorInfo(subSsrs.GetText(i, 6));
				rySubInfo.setGrpNo(subSsrs.GetText(i, 7));
				rySubInfo.setCreditCode(subSsrs.GetText(i, 8));
				rySubInfo.setGTaxNo(subSsrs.GetText(i, 9));
				rySubInfo.setGOrgancomCode(subSsrs.GetText(i, 10));
				rySubInfo.setPremMult(subSsrs.GetText(i, 11));
				
				putResult("RYSubInfo", rySubInfo);
			}
			
			
			
			
		}
		
		String CrsSaleSql="select Crs_SaleChnl,Crs_BussType,GrpAgentCom,GrpAgentCode,GrpAgentName,GrpAgentIDNo from lccont"
				+ " where contno='"+mContNo+"'";	
		SSRS CrsSaleSsrs=mExeSQL.execSQL(CrsSaleSql);
		if(CrsSaleSsrs.getMaxRow()==0){
			
		}else{
			for(int i=1;i<=CrsSaleSsrs.getMaxRow();i++){
				CrsSaleInfo tCrsSaleInfo = new CrsSaleInfo();
				tCrsSaleInfo.setCrs_SaleChnl(CrsSaleSsrs.GetText(i, 1));
				tCrsSaleInfo.setCrs_BussType(CrsSaleSsrs.GetText(i, 2));
				tCrsSaleInfo.setGrpAgentCom(CrsSaleSsrs.GetText(i, 3));
				tCrsSaleInfo.setGrpAgentCode(CrsSaleSsrs.GetText(i, 4));
				tCrsSaleInfo.setGrpAgentName(CrsSaleSsrs.GetText(i, 5));
				tCrsSaleInfo.setGrpAgentIDNo(CrsSaleSsrs.GetText(i, 6));
				tCrsSaleInfo.setOriginChnl("");
				putResult("CrsSaleInfo", tCrsSaleInfo);
			}
		}
			
		return true;
	}

	
	

    


    public static void main(String[] args) {
    	
    	
	}
    
}
