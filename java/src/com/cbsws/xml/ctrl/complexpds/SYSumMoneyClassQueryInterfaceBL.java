package com.cbsws.xml.ctrl.complexpds;


import java.util.ArrayList;
import java.util.List;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.SYSumMoneyClassQueryInfo;
import com.cbsws.obj.SumMoneyQueryResult;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;




public class SYSumMoneyClassQueryInterfaceBL extends ABusLogic {

	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	private String QueryType = "";
	private String ContNo = "";
	private SYSumMoneyClassQueryInfo SYSumMoneyClassQueryInfo = new SYSumMoneyClassQueryInfo();
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
	List<SumMoneyQueryResult> SumMoneyQueryResults = new ArrayList<SumMoneyQueryResult>();
	protected boolean deal(MsgCollection cMsgInfos) {
		long startTime = System.currentTimeMillis();
		System.out.println("接口处理开始时间:"+startTime);
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkdata()){
        	return false;
        }
        //
        if(!queryInfo()){
        	return false;
        }
        
		long endTime = System.currentTimeMillis();
		System.out.println("接口处理结束时间:"+endTime);
		long totalTime = endTime-startTime;
		System.out.println("接口处理总用时："+totalTime+"ms");
		return true;
	}
	/**
	 * 解析xml
	 * @param cMsgInfos
	 * @return
	 */
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List SYSumMoneyClassQueryInfos = cMsgInfos.getBodyByFlag("SYSumMoneyClassQueryInfo");
        if (SYSumMoneyClassQueryInfos == null || SYSumMoneyClassQueryInfos.size() != 1)
        {
            errLog("申请报文中获取报文体失败。");
            return false;
        }
        SYSumMoneyClassQueryInfo = (SYSumMoneyClassQueryInfo)SYSumMoneyClassQueryInfos.get(0);
        
        return true;
	}
	/**
	 * 校验数据
	 * @return
	 */
	private boolean checkdata(){
		QueryType = SYSumMoneyClassQueryInfo.getQueryType();
		if(QueryType == null || "".equals(QueryType)){
			errLog("查询类型不能为空！");
			return false;
		}
		ContNo = SYSumMoneyClassQueryInfo.getContNo();
		if(ContNo == null || "".equals(ContNo)){
			errLog("保单号不能为空！");
			return false;
		}
		return true;
	}
	/**
	 * 查询保单及账户信息
	 * @return
	 */
	private boolean queryInfo(){
		List<SumMoneyQueryResult> tSumMoneyQueryResults = new ArrayList<SumMoneyQueryResult>();
		String subSQL = "";
		if("1".equals(QueryType)){
			subSQL = " and MoneyType in ('BF','GL','RP')";
		}
		if("2".equals(QueryType)){
			subSQL = " and MoneyType = 'LX'";
		}
		if("3".equals(QueryType)){
			subSQL = " and MoneyType = 'BR'";
		}
		String QuerySQL 	= " select"
					+ " a.PayDate,"
					+ " (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only),"
					+ " nvl((case when sum(a.Money) >= 0 then sum(a.Money) end),0), "
					+ " nvl((case when sum(a.Money) <= 0 then abs(sum(a.Money)) end),0),"
					+ " nvl((select sum(money) from LCInsureAccTrace where (PayDate < a.PayDate or (paydate=a.paydate and  moneytype<a.moneytype)) and a.PolNo = PolNo " + subSQL + "),0) + sum(a.money) "
					+ " from LCInsureAccTrace a "
					+ " where a.contno = '" + ContNo + "'"
					+ subSQL
					+ " group by a.PayDate, a.MoneyType,a.PolNo"
					+ " order by a.PayDate, a.MoneyType";
		
		SSRS tSSRS = tExeSQL.execSQL(QuerySQL);
		if(tSSRS.getMaxRow()<=0){
			SumMoneyQueryResult tSumMoneyQueryResult = new SumMoneyQueryResult();
			tSumMoneyQueryResult.setPayDate("");
			tSumMoneyQueryResult.setMoneyType("");
			tSumMoneyQueryResult.setIncreaseMoney("");
			tSumMoneyQueryResult.setReduceMoney("");
			tSumMoneyQueryResult.setFinalMoney("");
		}
		for(int i = 1;i <= tSSRS.getMaxRow();i++){
			String PayDate = tSSRS.GetText(i, 1);
			String MoneyType = tSSRS.GetText(i, 2);
			String IncreaseMoney = tSSRS.GetText(i, 3);
			String ReduceMoney = tSSRS.GetText(i, 4);
			String FinalMoney = tSSRS.GetText(i, 5);
			
			SumMoneyQueryResult tSumMoneyQueryResult = new SumMoneyQueryResult();
			tSumMoneyQueryResult.setPayDate(PayDate);
			tSumMoneyQueryResult.setMoneyType(MoneyType);
			tSumMoneyQueryResult.setIncreaseMoney(IncreaseMoney);
			tSumMoneyQueryResult.setReduceMoney(ReduceMoney);
			tSumMoneyQueryResult.setFinalMoney(FinalMoney);
			
			tSumMoneyQueryResults.add(tSumMoneyQueryResult);
		}
		
		getWrapParmList(tSumMoneyQueryResults);
		getXmlResult();
		
		return true;
	}	
	private void getWrapParmList(List<SumMoneyQueryResult> tSumMoneyQueryResults){
		 SumMoneyQueryResults = tSumMoneyQueryResults;	    
	}
	    
	//返回报文
	public void getXmlResult(){
		//返回账户信息
		for(int i=0;i<SumMoneyQueryResults.size();i++){
			putResult("SumMoneyQueryResult", (SumMoneyQueryResult)SumMoneyQueryResults.get(i));
		}

	}	
    
}
