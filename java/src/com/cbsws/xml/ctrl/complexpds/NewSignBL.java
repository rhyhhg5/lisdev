package com.cbsws.xml.ctrl.complexpds;

import org.apache.log4j.Logger;

import com.cbsws.obj.LCContTable;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.VData;

public class NewSignBL {
private final Logger cLogger = Logger.getLogger(getClass());
	
	private LCContSchema cLCContSchema;
	public LCContTable cLCContTable;
	private String cPrtno = "";
	private MMap map= new MMap();;
	
	public NewSignBL(String tPrtno,LCContTable tLCContTable){
		cPrtno=tPrtno;
		cLCContTable=tLCContTable;
	}
	public String deal() throws Exception{
		cLogger.info("Into WxTempFeeBL.deal()...");
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(cPrtno);
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			return "查询保单合同数据失败！";
		}
		cLCContSchema = mLCContSet.get(1);
		String ModifyDate=PubFun.getCurrentDate();
		String ModifyTime=PubFun.getCurrentTime();
		String Sql1="update LCRnewStateLog set "
				+ " state='6',"
				+ " paytodate='"+cLCContSchema.getPaytoDate()+"',"
				+ " modifydate='"+ModifyDate+"',"
				+ " modifytime='"+ModifyTime+"',"
				+ " newcontno='"+cLCContSchema.getContNo()+"',"
				+ " operator='"+cLCContSchema.getOperator()+"',Remark='xbsign' "
				+ " where prtno='"+cLCContSchema.getPrtNo()+"'";
		map.put(Sql1, "UPDATE");
		/*String Sql2="update lcpol set "
				+ " signdate='"+cLCContSchema.getCValiDate()+"',"
				+ " signtime='"+cLCContTable.getCValiTime()+"' "
				+ " where contno='"+cLCContSchema.getContNo()+"'";
		map.put(Sql2, "UPDATE");
		String Sql3="update lccont set "
				+ " signdate='"+cLCContSchema.getCValiDate()+"',"
				+ " signtime='"+cLCContTable.getCValiTime()+"' "
				+ " where contno='"+cLCContSchema.getContNo()+"'";
		map.put(Sql3, "UPDATE");*/
		VData tVData=new VData();
		tVData.add(map);
		cLogger.info("开始提交续保签单更新数据...");
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(tVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			return "提交续保签单更新数据失败！";
		}
		cLogger.info("Out NewSignBL.deal()!");
		return "";
	}
}
