package com.cbsws.xml.ctrl.complexpds;


import java.util.List;













import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;

import com.cbsws.obj.RYAppntInfo;
import com.cbsws.obj.RYContTable;

import com.sinosoft.lis.pubfun.GlobalInput;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;






public class RYAppntState extends ABusLogic {
	
	public String AppntName="";
	public String SendDate="";//报文发送日期
	public String SendTime="";//报文发送时间
	public String BatchNo="";
	public String IDType;
	public String IDNo;
	public RYAppntInfo cRYAppntInfo;
	
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始登录验证");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		SendDate = tMsgHead.getSendDate();
		SendTime = tMsgHead.getSendTime();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("RYAppntInfo");			
			cRYAppntInfo = (RYAppntInfo) tLCContList.get(0); //获取保单信息，只有一个保单
	        AppntName=cRYAppntInfo.getAppntName();
	        IDType=cRYAppntInfo.getIDType();
	        IDNo=cRYAppntInfo.getIDNo();
	        
	        
				
			
	        
	        SSRS ss1=new SSRS();
	        ExeSQL exeSQL=new ExeSQL();
	        String sql="select customerno from ldperson where name='"+AppntName+"' and idtype='"
	        		+IDType+"' and idno='"+IDNo+"'";
	        ss1=exeSQL.execSQL(sql);
	        if(ss1.getMaxRow()==0){
	        	errLog("不存在于所传投保人信息相同的保单");
	        	return false;
	        }else{
	        	String customerno=ss1.GetText(1, 1);
	        	ss1.Clear();
	        	sql="select lc.contno, (case when lc.appflag ='0' and lc.uwflag='5' then '人工核保'  when lc.appflag ='1' then codename('appflag',lc.appflag) when lc.appflag ='0' then codename('appflag',lc.appflag) else '' end),"
	 	        		+"lc.PolApplyDate,lc.prem,lc.amnt,lc.getpoldate,lc.InsuredName, "
	 	        		 +" (select  riskname from lmriskapp where riskcode in (select riskcode from lcpol where prtno=lc.prtno fetch first 1 rows only)) riskname,"//产品名称
		        		 + "(select codename from ldcode where codetype = 'payintv' and code=lc.payintv) payintv,"//缴费频次
		        		 + "(select fee from lcinsureaccfeetrace where contno=lc.contno and othertype='1' and moneytype = 'RP') fee ,"//风险保费
	        		 + " ((select sumactupaymoney from ljapay where incomeno=lc.contno and duefeetype='0')  -(select fee from lcinsureaccfeetrace where contno=lc.contno and othertype='1' and moneytype = 'RP')),"
		        		 + "lc.CValiDate,ls.taxcode,ls.grpno,(select grpname from lsgrp where grpno = ls.grpno) grpname "
	 	        		 +" from lccont lc,lcpol lp,lccontsub ls where lc.appntno='"+customerno+"' and lc.contno=lp.contno and lc.prtno=ls.prtno and "
	 	        		 		+ " exists (select 1 from lmriskapp where taxoptimal='Y' and riskcode=lp.riskcode) order by lc.PolApplyDate";
	 	        		 
	        	ss1=exeSQL.execSQL(sql);
	        	if(ss1.getMaxRow()==0){
	        		errLog("该投保人下不存在税优保单");
	        		return false;
	        	}
	        	for(int i=1;i<=ss1.getMaxRow();i++){
	        		RYContTable ryContTable=new RYContTable();
	        		ryContTable.setContNo(ss1.GetText(i, 1));
	        		ryContTable.setState(ss1.GetText(i, 2));
	        		ryContTable.setPolApplyDate(ss1.GetText(i, 3));	 
	        		ryContTable.setPrem(ss1.GetText(i, 4));
	        		ryContTable.setAmnt(ss1.GetText(i, 5));
	        		if("".equals(ss1.GetText(i, 6))||ss1.GetText(i, 6)==null){
	        			ryContTable.setGetPolState("未回执回销");
	        			
	        			
	        		}else{
	        			ryContTable.setGetPolState("已回执回销");
	        		}
	        		ryContTable.setInsuredName(ss1.GetText(i, 7));
	        		//zxs
	        		ryContTable.setRiskName(ss1.GetText(i, 8));
	        		ryContTable.setPayintv(ss1.GetText(i, 9));
	        		ryContTable.setFee(ss1.GetText(i, 10));
	        		ryContTable.setInsuAccBala(ss1.GetText(i, 11));
	        		ryContTable.setCvalidate(ss1.GetText(i, 12));
	        		ryContTable.setTaxcode(ss1.GetText(i, 13));
	        		ryContTable.setGrpno(ss1.GetText(i, 14));
	        		ryContTable.setGrpname(ss1.GetText(i, 15));
	        		
	        		putResult("RYContTable", ryContTable);
	        		
	        		
	        	}
	        	
	        	
	        	
	        	
	        }
	        
	        
	       
	        //getXmlResult();
		}catch (Exception e) {
			System.out.println(e);
			errLog("未知错误，请检查报文格式");
        	return false;
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
		
		
		
		
	}
	

	
	

    


    public static void main(String[] args) {
    	String s1="[A-Za-z0-9]{10}";
    	String s2="1b3456789A";
    	System.out.println(s2.matches(s1));
    	
	}
    
}
