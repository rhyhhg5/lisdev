/**
 * 签单+单证核销+回执回销
 */
package com.cbsws.xml.ctrl.complexpds;

import org.apache.log4j.Logger;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.obj.ECCertSalesPayInfoTable;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LICertSalesPayInfoDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LICertSalesPayInfoSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.tb.ContReceiveUI;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.lis.tb.LCContSignBL;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LICertSalesPayInfoSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

import examples.newsgroups;

public class WxLCContSignBL {
	private final Logger cLogger = Logger.getLogger(getClass());
	
	private final GlobalInput cGlobalInput;
	private LCContSchema cLCContSchema;
	private String cError = "";
	private ECCertSalesPayInfoTable cECCertSalesPayInfo;
	private MsgHead cMsgHead ;
	private String cPrtno = "";
	
	public WxLCContSignBL(LCContSchema pLCContSchema, GlobalInput pGlobalInput) {
		cGlobalInput = pGlobalInput;
		cLCContSchema = pLCContSchema;
	}
	
	/**
	 * 查询保单合同数据失败，将会抛出MidplatException
	 */
	public WxLCContSignBL(String pPrtNo, GlobalInput pGlobalInput,ECCertSalesPayInfoTable pECCertSalesPayInfo,MsgHead tMsgHead) throws MidplatException {
		cGlobalInput = pGlobalInput;
		cMsgHead = tMsgHead;
		cPrtno = pPrtNo;
		cECCertSalesPayInfo = pECCertSalesPayInfo;
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(pPrtNo);
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			throw new MidplatException("查询保单合同数据失败！");
		}
		cLCContSchema = mLCContSet.get(1);
	}
	
	public String deal(){
		cLogger.info("Into WxLCContSignBL.deal()...");
		
		//签单
		LCContSet mLCContSet = new LCContSet();
		mLCContSet.add(cLCContSchema);

		VData mLCContSignBLVData =  new VData();
		mLCContSignBLVData.add(cGlobalInput);
		mLCContSignBLVData.add(mLCContSet);

		LCContSignBL mLCContSignBL = new LCContSignBL();
		cLogger.info("Start call LCContSignBL.submitData()...");
		long mStartMillis = System.currentTimeMillis();
		if (!mLCContSignBL.submitData(mLCContSignBLVData, "")) {
			cError = mLCContSignBL.mErrors.getFirstError();
			return cError;
		}
		if (mLCContSignBL.mErrors.needDealError()) {
			cError = mLCContSignBL.mErrors.getFirstError();
			return cError;
		}
		cLogger.info("LCContSignBL耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
		cLogger.info("End call LCContSignBL.submitData()!");

		//提交签单数据入库
		VData mSignResult = mLCContSignBL.getResult();
		cLCContSchema = (LCContSchema) mSignResult.getObjectByObjectName("LCContSchema", 0);
		MMap mSubmitMMap = (MMap) mSignResult.getObjectByObjectName("MMap", 0);
		String sql = "select "
				+ "1"
				+ " from lcpol lp where lp.prtno='"
				+ cLCContSchema.getPrtNo()
				+ "' and "
				+ " exists (select 1 from lmriskapp where taxoptimal='Y' and riskcode=lp.riskcode) ";
		SSRS s1=new ExeSQL().execSQL(sql);
		if(s1.getMaxRow()>0){
			cLCContSchema.setPrintCount(0);
		}else{
			cLCContSchema.setPrintCount(1);
			cLCContSchema.setGetPolDate(DateUtil.getCurDate("yyyy-MM-dd"));
			cLCContSchema.setGetPolTime(DateUtil.getCurDate("HH:mm:ss"));
			cLCContSchema.setCustomGetPolDate(DateUtil.getCurDate("yyyy-MM-dd"));
		}		
		
		
		//保存交易信息
		LICertSalesPayInfoSchema tLICertSalesPayInfoSchema = new LICertSalesPayInfoSchema();
		LICertSalesPayInfoDB tLICertSalesPayInfoDB = new LICertSalesPayInfoDB();
		tLICertSalesPayInfoDB.setBatchNo(cMsgHead.getBatchNo());
		LICertSalesPayInfoSet tLICertSalesPayInfoSet  = tLICertSalesPayInfoDB.query();
		if(tLICertSalesPayInfoSet == null || tLICertSalesPayInfoSet.size() != 1){
			cError = "签单时获取交易信息失败";
			return cError;
		}
		tLICertSalesPayInfoSchema = tLICertSalesPayInfoSet.get(1);
		tLICertSalesPayInfoSchema.setPayState("03");
		
		mSubmitMMap.put(tLICertSalesPayInfoSchema, "UPDATE");
		
		/**
		 * 针对健管产品，增加生成健管工作流节点的处理
		 */
		String mMissionSQL = "select 1 from lmriskapp a where a.risktype2='5' and exists (select 1 from lcpol where contno='"+ cLCContSchema.getContNo() +"' and riskcode=a.riskcode) with ur";
		String mMissionStr = new ExeSQL().getOneValue(mMissionSQL);
		if (mMissionStr.equals("1")) {
			LWMissionSchema tLWMissionSchema = createMission(cLCContSchema);
			mSubmitMMap.put(tLWMissionSchema, "INSERT");
		}
		
//		回执回销！放到打印报文中更改数据
//		cError = autoReceive(cLCContSchema.getContNo());

		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		cLogger.info("开始提交签单数据...");
		PubSubmit mPubSubmit = new PubSubmit();
		mStartMillis = System.currentTimeMillis();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			return "提交签单数据失败！";
		}
		
		cLogger.info("提交签单数据耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
		cLogger.info("提交签单数据成功！");
		cLogger.info("Out WxLCContSignBL.deal()!");
		
		return cError;
	}
	
	/**
	 * 生成工作流节点
	 */
	private LWMissionSchema createMission(LCContSchema pLCContSchema) {
		LWMissionSchema mLWMissionSchema = new LWMissionDB();
		String mMissionID = PubFun1.CreateMaxNo("MISSIONID", 20);
		mLWMissionSchema.setMissionID(mMissionID);
		mLWMissionSchema.setSubMissionID("1");
		mLWMissionSchema.setProcessID("0000000003");
		mLWMissionSchema.setActivityID("0000001190");
		mLWMissionSchema.setActivityStatus("1");
		mLWMissionSchema.setMissionProp1(pLCContSchema.getContNo());
		mLWMissionSchema.setMissionProp2(pLCContSchema.getPrtNo());
		mLWMissionSchema.setMissionProp3(pLCContSchema.getAgentCode());
		mLWMissionSchema.setMissionProp4(pLCContSchema.getAppntNo());
		mLWMissionSchema.setMissionProp5(pLCContSchema.getAppntName());
		mLWMissionSchema.setMissionProp6(pLCContSchema.getUWDate());
		mLWMissionSchema.setMissionProp7(pLCContSchema.getManageCom());
		mLWMissionSchema.setLastOperator(pLCContSchema.getOperator());
		mLWMissionSchema.setCreateOperator(pLCContSchema.getOperator());
		mLWMissionSchema.setMakeDate(DateUtil.getCurDate("yyyy-MM-dd"));
		mLWMissionSchema.setMakeTime(DateUtil.getCurDate("HH:mm:ss"));
		mLWMissionSchema.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
		mLWMissionSchema.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
		
		return mLWMissionSchema;
	}
	
	private String autoReceive(String pContNo) {
		TransferData mTmpTransferData = new TransferData();
		mTmpTransferData.setNameAndValue("ContNo", pContNo);

		VData mContReceiveVData = new VData();
		mContReceiveVData.add(cGlobalInput);
		mContReceiveVData.add(mTmpTransferData);

		cLogger.info("开始回执…");
		ContReceiveUI mContReceiveUI = new ContReceiveUI();
		if (!mContReceiveUI.submitData(mContReceiveVData, "CreateReceive")) {
			return mContReceiveUI.mErrors.getFirstError();
		}		
		//如果成功生成接收轨迹，目前银保险种需要自动进行合同接收。
		if (!mContReceiveUI.submitData(mContReceiveVData, "ReceiveCont")) {
			return mContReceiveUI.mErrors.getFirstError();
		}
		cLogger.info("回执成功！");

		cLogger.info("开始回销…");
		LCContGetPolSchema mLCContGetPolSchema = new LCContGetPolSchema();
		mLCContGetPolSchema.setContNo(pContNo);
		mLCContGetPolSchema.setGetpolDate(DateUtil.getCurDate("yyyy-MM-dd"));
		mLCContGetPolSchema.setGetpolMan(cGlobalInput.Operator);
		mLCContGetPolSchema.setSendPolMan(cGlobalInput.Operator);
		mLCContGetPolSchema.setGetPolOperator(cGlobalInput.Operator);
		mLCContGetPolSchema.setManageCom(cGlobalInput.ManageCom);

		VData mContGetPolVData = new VData();
		mContGetPolVData.addElement(mLCContGetPolSchema);
		mContGetPolVData.add(cGlobalInput);

		LCContGetPolUI mLCContGetPolUI = new LCContGetPolUI();
		if (!mLCContGetPolUI.submitData(mContGetPolVData, "UPDATE")) {
			return mLCContGetPolUI.mErrors.getFirstError();
		}
		cLogger.info("回销成功！");
		return "";
	}
	
	/**
	 * 返回签单后的保单合同号。注意：仅当签单成功时，此调用才有效。
	 */
	public String getContNo() {
		return cLCContSchema.getContNo();
	}
}

