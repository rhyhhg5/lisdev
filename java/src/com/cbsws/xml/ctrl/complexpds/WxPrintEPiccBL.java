package com.cbsws.xml.ctrl.complexpds;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.LCCustomerImpartTable;
import com.cbsws.obj.OnlineDeclareTable;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class WxPrintEPiccBL extends ABusLogic{
	
	/** 报文信息集合*/
	private MsgCollection mcollection = new MsgCollection();
	
	/** 报文头信息*/
	private MsgHead mMsgHead = new MsgHead();
	
	/** 业务信息集合*/
	private List bodyList = null;
	
	/** 告知信息*/
	private List LCCustomerImpartsList = null;
	
	/**在线填报动态个人信息*/
	private List OnlineDeclareList = null;
	
	/** 业务信息*/
	private CertPrintTable mCertPrintTable = new CertPrintTable();
	
	/** 卡号*/
	private String cardNo = null;
//	private String certifycode =null;
//	
//	/** 产品名称*/
//	private String certifyname = null;
	
	/** 产品编码*/
	private String wrapcode = null;
	
	/** 产品名称*/
	private String wrapname = null;
	
	/** 生效起期*/
	private String cvalidate=null;
	
	/** 终止日期*/
	private String cinvalidate =null;
	
	/** 打印条形码*/
	private String barCode="";
	
	/** 保单号码*/
	private String contNo = null;
	
	/** 保费*/
	private String prem = null;
	
	/** 保额*/
	private String amnt = null;
	
	/** 管理机构*/
	private String managecom = null;
	
	/** 特别约定*/
	private String special="";
	
	private String insusex = "";
	/** 主险代码*/
	private String[] mainRiskCode = null;

	private XmlExport dXmlExport = new XmlExport();
	
	/** 报文信息集合类型*/
	private final String appntInfo ="LCAPPNT";
	private final String insuInfo ="LCINSU";
	private final String bnfInfo ="LCBNF";
	private final String polInfo ="LCPOL";
	private final String termInfo ="TERMS";
	private final String hospitalInfo = "HOSPITALS";//指定医院
	private final String recommendHospitals = "Recommend_HOSPITALS";//推荐医院
	private final String cvalue = "CVALUE";//现金价值
	private final String cvalueend = "CVALUEEND";//现金价值结束节点
//	private final String healthNotic = "Health_Notic";//客户服务指南节点
	private final String OnlineDeclare = "OnlineDeclare";//在线填报动态个人信息节点
	
	private String contNoBar="";
	private String insuyearBar="";
	private String insuIdnoBar="";
	private String bnfIdnoBar="";
	private String dutyBar="";
	private StringBuffer dutyremark = new StringBuffer();
	
	/**被保人insuredno集合*/
	List insurednoList = new ArrayList();
	VData data = new VData();
	protected boolean deal(MsgCollection cMsgInfos){
		mcollection = cMsgInfos;
		if(!load()){
			System.out.println("没有得到要打印的信息");
 	        errLog("没有得到要打印的信息");
			return false;
		}
		
		if(!check()){
			System.out.println("校验失败");
 	        errLog("校验失败");
			return false;
		}
		
		if(!subDeal()){
			System.out.println("业务处理异常");
 	        errLog("业务处理异常");
			return false;
		}
		dealPrintMag();
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, ""))
        {
            System.out.println("将打印信息保存到LOPRTManager表失败");
        }
		return true;
	}
	
	private boolean load(){
		mMsgHead = mcollection.getMsgHead();
		
		bodyList=mcollection.getBodyByFlag("CertPrintTable");
		if(bodyList ==null || bodyList.size()==0){
			System.out.println("没有得到要打印的信息");
  	         errLog("没有得到要打印的信息");
  	         return false;
		}
		LCCustomerImpartsList = mcollection.getBodyByFlag("LCCustomerImpartTable");
		OnlineDeclareList = mcollection.getBodyByFlag("OnlineDeclareTable");
		return true ;
	}
	
	private boolean check(){
		if(!"WXP0001".equals(mMsgHead.getMsgType())){
			errLog("报文类型不匹配！");
 	        return false;
		}
		return true;
	}
	
	private boolean subDeal(){
		
		for (int i=0;i<bodyList.size();i++){
			this.mCertPrintTable = (CertPrintTable)bodyList.get(i);
			this.cardNo =  this.mCertPrintTable.getCardNo();
			this.special=  this.mCertPrintTable.getSpecialClause();
			if(this.cardNo!=null && !this.cardNo.equals("")){
				
				//保单中的单证编号和名称和产品代码
//				StringBuffer certifySQL = new StringBuffer();
//				certifySQL.append("select lmcd.certifycode,lmcd.certifyname,lmcr.riskcode from LZCardNumber lzcn " +
//									"inner join lmcertifydes lmcd on lmcd.subcode = lzcn.cardtype " +
//									"inner join LMCardRisk lmcr on lmcr.certifycode = lmcd.certifycode " +
//									"where 1 = 1 and lzcn.CardNo = '").append(this.cardNo).append("' ");
//				SSRS certifySSRS =  new ExeSQL().execSQL(certifySQL.toString());
//				if(certifySSRS == null || certifySSRS.MaxRow!=1){
//					System.out.println("没有得到保单信息");
//		  	         errLog("没有得到保单信息");
//		  	         return false;
//				}
//				this.certifycode = certifySSRS.GetText(1, 1);
//				this.certifyname = certifySSRS.GetText(1, 2);
				String riskWrapCodeSql = "select riskwrapcode from lcriskdutywrap where prtno = '"+this.cardNo+"' ";
				SSRS riskWrapCodeSSRS = new ExeSQL().execSQL(riskWrapCodeSql);
				if(riskWrapCodeSSRS == null || riskWrapCodeSSRS.MaxRow<=0){
					System.out.println("没有得到产品信息！");
		  	         errLog("没有得到产品信息！");
		  	         return false;
				}
				this.wrapcode =  riskWrapCodeSSRS.GetText(1, 1);
				String wrapnameSQL = "select wrapname from ldwrap where riskwrapcode ='"+this.wrapcode+"'";
				this.wrapname = new ExeSQL().getOneValue(wrapnameSQL);
				//this.wrapcode ="WR0001";
				
				String specialSQL="select remark from ldwrap where riskwrapcode='"+this.wrapcode+"'";
				String speContent = new ExeSQL().getOneValue(specialSQL);
				if(!speContent.equals("") && speContent !=null){
					this.special=speContent;
				}
				//获取保单信息
				String contSQL = "select contno,prem,cvalidate,cinvalidate,managecom,amnt,insuredsex from lccont where prtno ='"+this.cardNo+"'";
				//modify by licaiyan 2014-03-12 增加保额查询
				SSRS contSSRS =  new ExeSQL().execSQL(contSQL);
				if(contSSRS == null || contSSRS.MaxRow!=1){
					System.out.println("没有得到保单信息！");
		  	         errLog("没有得到保单信息！");
		  	         return false;
				}
				this.contNo = contSSRS.GetText(1, 1);
				this.prem = contSSRS.GetText(1, 2)+"元";
				this.cvalidate = contSSRS.GetText(1, 3);
				this.cinvalidate = contSSRS.GetText(1, 4);
				this.cinvalidate = getPreviousDate(new FDate().getDate(cinvalidate)); // by zhangyige 获取前一天日期
				this.managecom = contSSRS.GetText(1, 5);
				this.amnt = contSSRS.GetText(1, 6);
				this.insusex = contSSRS.GetText(1, 7);
				
				//投保人信息
				StringBuffer appntSQL = new StringBuffer();
				appntSQL.append("select a.appntname,a.appntsex,(select codename from ldcode where codetype = 'sex' and code =a.appntsex)," +
						"a.idtype,(select codename from ldcode where codetype = 'idtype' and code =a.idtype),a.idno," +
						"a.appntbirthday,b.phone,b.homeaddress,b.email " +
						"from lcappnt a left join lcaddress b on a.appntno = b.customerno and a.addressno = b.addressno " +
						"where prtno = '"+this.cardNo+"' ");
				SSRS appntSSRS = new ExeSQL().execSQL(appntSQL.toString());
				if(appntSSRS==null || appntSSRS.MaxRow==0){
					System.out.println("没有得到投保人信息");
		  	         errLog("没有得到投保人信息");
		  	         return false;
				}
											
				//被保人信息
				StringBuffer insuSQL = new StringBuffer();
				insuSQL.append("select insuredno,(select codename from ldcode where codetype = 'relation' and code =a.relationtoappnt)," +
						"a.name,a.sex,(select codename from ldcode where codetype = 'sex' and code =a.sex)," +
						"a.idtype,(select codename from ldcode where codetype = 'idtype' and code =a.idtype),a.idno," +
						"a.birthday,b.phone,b.homeaddress,b.email,a.occupationtype," +
						"(select trim(OccupationName)||'-'||workname from LDOccupation where occupationcode=a.occupationcode) " +
						"from lcinsured a left join lcaddress b on a.insuredno = b.customerno and a.addressno = b.addressno " +
						"where prtno = '").append(this.cardNo).append("' order by insuredno ");
				SSRS insuSSRS = new ExeSQL().execSQL(insuSQL.toString());
				if(insuSSRS==null || insuSSRS.MaxRow==0){
					System.out.println("没有得到被保人信息");
		  	         errLog("没有得到被保人信息");
		  	         return false;
				}
				
				//受益人信息
				StringBuffer bnfSQL = new StringBuffer();
				bnfSQL.append("select insuredno,name,sex,(select codename from ldcode where codetype = 'sex' and code = sex)," +
						"idtype,(select codename from ldcode where codetype = 'idtype' and code =idtype),idno," +
						"birthday,relationtoinsured,(select codename from ldcode where codetype = 'relation' and code = relationtoinsured)," +
						"Integer(BnfLot*100)||'%',BnfGrade from lcbnf  where contno = '"+this.contNo+"' " +
						"group by insuredno,name,sex,idtype,idno,birthday,relationtoinsured,BnfLot,BnfGrade " +
						"order by insuredno ");
				SSRS bnfSSRS = new ExeSQL().execSQL(bnfSQL.toString());
				StringBuffer nobnfSQL = new StringBuffer();
				nobnfSQL.append("select '','法定受益人','','','','','','','','','','' from dual where 1=1 ");
				//法定受益人也要传受益人的各个节点 moidify by zhangyige
				//保险责任
				StringBuffer dutySQL = new StringBuffer();
				dutySQL.append("select ld.riskcode,(select riskname from lmrisk where riskcode = ld.riskcode)," +
								"lm.dutycode,lm.dutyname,lm.outdutyname,lc.amnt,lc.years,'', " +
								"case when lcc.payintv = 0 then '趸缴' " +
								"when lcc.payintv = 12 then '年缴' " +
								"when lcc.payintv = 6 then '半年缴' " +
								"when lcc.payintv = 3 then '季缴'  when lcc.payintv = 1 then '月缴' else '' end," +
								"case when codename('insuyearflag',lc.insuyearflag) ='岁'"+
								"then '年' else codename('insuyearflag',lc.insuyearflag) end ," +
								"lc.payyears,lcc.payintv ,lc.insuyear,lc.insuyearflag,lc.payendyearflag "+  //增加缴费年期 、缴费方式、保险期间、保险期间标识 
								"from lmduty lm  " +
								"inner join lcduty lc  on lm.dutycode = lc.dutycode " +
								"inner join ldriskdutywrap ld on ld.dutycode = lc.dutycode " +
								"inner join lccont lcc on lc.contno = lcc.contno "+
								"where 1=1 and lcc.contno = '"+this.contNo+"' " +
							    "and ld.riskwrapcode = '"+this.wrapcode+"' and ld.calfactor = 'Amnt' ");
				SSRS dutySSRS = new ExeSQL().execSQL(dutySQL.toString());
				
				
				//现金价值表    2014-3-12 modify by licaiyan-------------------
					//根据保单查询主险
				List ssrsList = new ArrayList();
				String riskcodeSql = "select distinct a.riskcode from lcpol a  " +
						"where a.contno = '"+this.contNo+"' " +
						"and exists (select 1 from lmriskapp where riskcode=a.riskcode  and subriskflag='M')";
				
				SSRS riskcodeSSRS = new ExeSQL().execSQL(riskcodeSql);
				if(riskcodeSSRS != null && riskcodeSSRS.MaxRow > 0){//可能有多个主险
					mainRiskCode = new String[riskcodeSSRS.MaxRow];
					for(int m = 1;m <= riskcodeSSRS.MaxRow;m++){
						mainRiskCode[m - 1] = riskcodeSSRS.GetText(m, 1);
						//根据主险代码从lmcalmode中查询现金价值算法
						String cashvSql = "select calsql from lmcalmode where type='X' and riskcode='"+mainRiskCode[m - 1]+"' ";
						SSRS cashvSqlSSRS = new ExeSQL().execSQL(cashvSql);
						String riskAmntsql = "select amnt from lcpol where contno='"+this.contNo+"' and riskcode='"+mainRiskCode[m - 1]+"'";
						SSRS riskAmntSSRS = new ExeSQL().execSQL(riskAmntsql);
						if(cashvSqlSSRS != null && cashvSqlSSRS.MaxRow > 0){
							String cashValueCalSql = cashvSqlSSRS.GetText(1, 1);
							cashValueCalSql = cashValueCalSql.replaceAll("\\?CValiDate\\?", this.cvalidate);
							cashValueCalSql = cashValueCalSql.replaceAll("\\?Amnt\\?", riskAmntSSRS.GetText(1, 1));
							cashValueCalSql = cashValueCalSql.replaceAll("\\?PayEndYear\\?", dutySSRS.GetText(1,11 ));//缴费年期
							cashValueCalSql = cashValueCalSql.replaceAll("\\?PayIntv\\?", dutySSRS.GetText(1,12 ));
							cashValueCalSql = cashValueCalSql.replaceAll("\\?InsuYear\\?", dutySSRS.GetText(1, 13));
							cashValueCalSql = cashValueCalSql.replaceAll("\\?InsuYearFlag\\?", dutySSRS.GetText(1, 14));
							cashValueCalSql = cashValueCalSql.replaceAll("\\?PayEndYearFlag\\?", dutySSRS.GetText(1, 15));
							String appbirth = insuSSRS.GetText(1, 9);
							String appage = PubFun.getInsuredAppAge(this.cvalidate, appbirth) + "";//计算保单生效时被保人年龄
							cashValueCalSql = cashValueCalSql.replaceAll("\\?InsuredAppAge\\?", appage);
							cashValueCalSql = cashValueCalSql.replaceAll("\\?InsuredSex\\?", this.insusex);//2014-5-29 licaiyan 现金价值计算性别相关
							
							
							
							
							
							System.out.println("--------打印现金价值表SQL-------------" + cashValueCalSql);
							
							SSRS cvalueSSRS = new ExeSQL().execSQL(cashValueCalSql);
							
							ssrsList.add(cvalueSSRS);
						}
						
					}
				}
				
				
				if(dutySSRS==null || dutySSRS.MaxRow==0){
					System.out.println("没有得到保险责任信息");
		  	         errLog("没有得到保险责任信息");
		  	         return false;
				}

				
				for(int j=1;j<=insuSSRS.MaxRow;j++){
					String insuIdno =insuSSRS.GetText(j, 8);
					this.insuIdnoBar +=insuIdno;
					insurednoList.add(insuSSRS.GetText(j, 1));
				}
				
				if(bnfSSRS != null && bnfSSRS.MaxRow>0){
					for(int j=1;j<=bnfSSRS.MaxRow;j++){
						String bnfIdno =bnfSSRS.GetText(j, 7);
						this.bnfIdnoBar +=bnfIdno;
					}
				}else{
					bnfSSRS = new ExeSQL().execSQL(nobnfSQL.toString());
				}
				
				for(int j=1;j<=dutySSRS.MaxRow;j++){
					dutyremark.append(dutySSRS.GetText(j, 4) + dutySSRS.GetText(j, 6));
				}
				
				if(this.dutyremark.toString().equals("") || this.dutyremark.toString() ==null){
					System.out.println("没有得到适用条款信息");
		  	         errLog("没有得到适用条款信息");
		  	         return false;
				}
				this.contNoBar = this.cardNo;	
				this.insuyearBar = this.cvalidate+this.cinvalidate;
				this.dutyBar=this.dutyremark.toString();
				
				this.barCode=this.contNoBar+this.insuyearBar+this.insuIdnoBar+this.bnfIdnoBar+this.dutyBar;

				System.out.println("barcode=="+this.barCode);
				
				//分公司地址和邮编
				StringBuffer comSQL = new StringBuffer();
				comSQL.append("select address,zipcode from ldcom "+
						" where comcode = '"+this.managecom+"' ");
				SSRS comSSRS = new ExeSQL().execSQL(comSQL.toString());
				if(comSSRS==null || comSSRS.MaxRow==0||comSSRS.MaxRow>1){
					System.out.println("没有得到分公司地址和邮编信息");
		  	         errLog("没有得到分公司地址和邮编信息");
		  	         return false;
				}
//				条款
				SSRS termSSRS = getTerms();
				
				dXmlExport.createDocument("indigo", "");
				TextTag tTextTag = new TextTag();
				tTextTag.add("JETFORMTYPE", "WXP_002");
				tTextTag.add("MANAGECOMLENGTH4", "DZSW");
				tTextTag.add("USERIP", "127_0_0_1");
				tTextTag.add("PREVIEWFLAG", "1");
				tTextTag.add("CVALIDATE", cvalidate);
				tTextTag.add("CINVALIDATE", cinvalidate);
				tTextTag.add("CERTIFYNAME", "未成年人重大疾病保障计划");
				tTextTag.add("CARDNO", this.contNo);
				tTextTag.add("CONTNO", this.contNo);
				tTextTag.add("WRAPCODE", this.wrapcode);
				tTextTag.add("APPNTNAME", appntSSRS.GetText(1, 1));
				tTextTag.add("APPNTSEX", appntSSRS.GetText(1, 3));
				tTextTag.add("APPNTIDTYPE", appntSSRS.GetText(1, 5));
				tTextTag.add("APPNTIDNO", appntSSRS.GetText(1, 6));
				tTextTag.add("APPNTBIRTHDAY", appntSSRS.GetText(1, 7));
				tTextTag.add("APPNTPHONE", appntSSRS.GetText(1, 8));
				tTextTag.add("APPNTHOMEADDRESS", appntSSRS.GetText(1, 9));
				tTextTag.add("APPNTEMAIL", appntSSRS.GetText(1, 10));
				tTextTag.add("PREM",this.prem);
				tTextTag.add("BARCODE", this.barCode);
				tTextTag.add("LCSPEC", this.special);
				tTextTag.add("MAILADDR", comSSRS.GetText(1, 1));
				tTextTag.add("MAILZIPCODE", comSSRS.GetText(1, 2));
				tTextTag.add("SERVICEADDR", comSSRS.GetText(1, 1));
				tTextTag.add("SERVICEZIPCODE", comSSRS.GetText(1, 2));
				dXmlExport.addTextTag(tTextTag);
//				dealCardListInfo(dXmlExport,appntSSRS,this.appntInfo);
				dealCardListInfo(dXmlExport,insuSSRS,this.insuInfo);//被保人
				if(bnfSSRS==null || bnfSSRS.MaxRow==0){
					System.out.println("没有得到受益人信息,受益人为法定");
				}else{
					dealCardListInfo(dXmlExport,bnfSSRS,this.bnfInfo);//受益人
				}
				dealCardListInfo(dXmlExport,dutySSRS,this.polInfo);//险种责任
				
				
			   //现价价值表
				if(ssrsList == null || ssrsList.size() == 0){
					System.out.println("没有得到获取现金价值表的算法,则不打印");
//		  	        errLog("没有得到获取现金价值表的算法,则不打印");
					
				}else{
					for(int n=0;n<ssrsList.size();n++){
						SSRS data = (SSRS)ssrsList.get(n);
						String rnsql = "select a.dutyName from lmduty a ,lmriskduty b where a.dutycode = b.dutyCode and b.riskcode='"+mainRiskCode[n]+"'";
						SSRS rnSSRS = new ExeSQL().execSQL(rnsql);
						String[] tCertifyListInfoTitle = new String[2];
						tCertifyListInfoTitle[0] = "";
						tCertifyListInfoTitle[1] = n + "";
						if(rnSSRS != null && rnSSRS.MaxRow > 0){
							tCertifyListInfoTitle[0] = rnSSRS.GetText(1, 1);//存险种名称
						}
						
				        ListTable tListTable = new ListTable();
				        tListTable.setName(this.cvalue);
				
				        String[] oneCardInfo = null;
				
				        for(int k = 1; k <= data.MaxRow; k++){
				        	 oneCardInfo = new String[2];
				             oneCardInfo[0] = data.GetText(k, 2);//
				             oneCardInfo[1] = PubFun.setPrecision1(Double.parseDouble(data.GetText(k, 3)), "0.00");//保留两位小数
				             tListTable.add(oneCardInfo);
				        }
			            //如果节点个数为奇数，则凑成偶数
				        if(data.MaxRow % 2 == 1){//
				        	 oneCardInfo = new String[2];
				             oneCardInfo[0] = "--";//
				             oneCardInfo[1] = "--";
				             tListTable.add(oneCardInfo);
				        }
			            dXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
			            
			            //添加CVALUEEND节点
						dealCardListInfo(dXmlExport,null,this.cvalueend);//条款
					}
					 
				}
				
				dealCardListInfo(dXmlExport,termSSRS,this.termInfo);//条款
				//String xmlDoc =dXmlExport.outputString();
				//System.out.println("doc=="+xmlDoc);
			}else{
				 System.out.println("没有得到卡号");
	  	         errLog("没有得到卡号");
	  	         return false;
			}			
		}
//		处理个人信息
		dealOnlineDeclareListInfo(dXmlExport,OnlineDeclareList,this.OnlineDeclare);//
		dealNullNode(dXmlExport,"OnlineDeclare_End","OnlineDeclare_COL");
//		处理告知内容
		if(LCCustomerImpartsList != null && LCCustomerImpartsList.size()>0){
			dealLCCustomerImpartsInfo(dXmlExport,LCCustomerImpartsList,"LCCustomerImparts");
		}
//		healthNotic 客户服务指南节点，col节点中无需给值
		dealNullNode(dXmlExport,"Health_Notic","Health_Notic_COL");
//		医疗机构指定医院
		SSRS hospitalSSRS = getHospital();
//		推荐医院 
		SSRS reHospitalsSSRS = getReHospitals();
//		处理指定医院
		if(hospitalSSRS == null || hospitalSSRS.MaxRow == 0){
			System.out.println("没有指定医院信息");
		}else{
			dealCardListInfo(dXmlExport,hospitalSSRS,this.hospitalInfo);
		}
//		处理推荐医院
		if(reHospitalsSSRS == null || reHospitalsSSRS.MaxRow == 0){
			System.out.println("没有推荐医院信息");
		}else{
			dealCardListInfo(dXmlExport,reHospitalsSSRS,this.recommendHospitals);
		}
		if(!FTPSendFile()){
			System.out.println("上传FTP失败");
  	         errLog("上传FTP失败");
  	         return false;
		}
		return true;
	}
	
	/**
	 * 生成XML
	 * @param cXmlExport
	 * @param data
	 * @param type
	 * @return
	 */
	private boolean dealCardListInfo(XmlExport cXmlExport,SSRS data,String type)
    {
		if(type.equals(this.appntInfo)){//投保人信息
			String[] tCertifyListInfoTitle = new String[8];
			tCertifyListInfoTitle[0] = "APPNTNAME";//姓名
	        tCertifyListInfoTitle[1] = "APPNTSEX";//性别
	        tCertifyListInfoTitle[2] = "IDTYPE";//证件类型
	        tCertifyListInfoTitle[3] = "IDNO";//证件号码
	        tCertifyListInfoTitle[4] = "APPNTBIRTHDAY";//出生日期
	        tCertifyListInfoTitle[5] = "PHONE";//联系电话
	        tCertifyListInfoTitle[6] = "HOMEADDRESS";//联系地址
	        tCertifyListInfoTitle[7] = "EMAIL";//电子邮箱
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[8];
	            oneCardInfo[0] = tTmpCardInfo[0];
	            oneCardInfo[1] = tTmpCardInfo[1];
	            oneCardInfo[2] = tTmpCardInfo[2];
	            oneCardInfo[3] = tTmpCardInfo[3];
	            oneCardInfo[4] = tTmpCardInfo[4];
	            oneCardInfo[5] = tTmpCardInfo[5];
	            oneCardInfo[6] = tTmpCardInfo[6];
	            oneCardInfo[7] = tTmpCardInfo[7];
	            tListTable.add(oneCardInfo);
	        }
	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);  
		}
		if(type.equals(this.insuInfo)){
			String[] tCertifyListInfoTitle = new String[12];
			tCertifyListInfoTitle[0] = "INSUREDNO";//被保人客户号
			tCertifyListInfoTitle[1] = "RELATIONTOAPPNT";//被保人与投保人关系
	        tCertifyListInfoTitle[2] = "NAME";//姓名
	        tCertifyListInfoTitle[3] = "SEX";//性别
	        tCertifyListInfoTitle[4] = "IDTYPE";//证件类型
	        tCertifyListInfoTitle[5] = "IDNO";//证件号码
	        tCertifyListInfoTitle[6] = "BIRTHDAY";//出生日期	        
	        tCertifyListInfoTitle[7] = "PHONE";//联系电话
	        tCertifyListInfoTitle[8] = "HOMEADDRESS";//联系地址
	        tCertifyListInfoTitle[9] = "EMAIL";//电子邮件
	        tCertifyListInfoTitle[10] = "OCCUPATIONTYPE";//职业等级
	        tCertifyListInfoTitle[11] = "OCCUPATIONNAME";//职业名称
	        
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[12];
	            oneCardInfo[0] = tTmpCardInfo[0];
	            oneCardInfo[1] = tTmpCardInfo[1];
	            oneCardInfo[2] = tTmpCardInfo[2];
	            oneCardInfo[3] = tTmpCardInfo[4];
	            oneCardInfo[4] = tTmpCardInfo[6];
	            oneCardInfo[5] = tTmpCardInfo[7];
	            oneCardInfo[6] = tTmpCardInfo[8];
	            oneCardInfo[7] = tTmpCardInfo[9];
	            oneCardInfo[8] = tTmpCardInfo[10];
	            oneCardInfo[9] = tTmpCardInfo[11];
	            oneCardInfo[10] = tTmpCardInfo[12];
	            oneCardInfo[11] = tTmpCardInfo[13];
	            tListTable.add(oneCardInfo);
	        }
	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);  
		}
		if(type.equals(this.bnfInfo)){
			String[] tCertifyListInfoTitle = new String[9];
			tCertifyListInfoTitle[0] = "INSUREDNO";//被保人客户号
	        tCertifyListInfoTitle[1] = "NAME";//受益人姓名
	        tCertifyListInfoTitle[2] = "SEX";//受益人性别	        
	        tCertifyListInfoTitle[3] = "IDTYPE";//受益人证件类型
	        tCertifyListInfoTitle[4] = "IDNO";//证件号码
	        tCertifyListInfoTitle[5] = "BIRTHDAY";//受益人出生日期
	        tCertifyListInfoTitle[6] = "TOINSURELA";//与被保人关系
	        tCertifyListInfoTitle[7] = "BNFLOT";//受益比例
	        tCertifyListInfoTitle[8] = "BNFGRADE";//收益顺位
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[9];
	            oneCardInfo[0] = tTmpCardInfo[0];
	            oneCardInfo[1] = tTmpCardInfo[1];
	            oneCardInfo[2] = tTmpCardInfo[3];
	            oneCardInfo[3] = tTmpCardInfo[5];
	            oneCardInfo[4] = tTmpCardInfo[6];
	            oneCardInfo[5] = tTmpCardInfo[7];
	            oneCardInfo[6] = tTmpCardInfo[9];
	            oneCardInfo[7] = tTmpCardInfo[10];
	            oneCardInfo[8] = tTmpCardInfo[11];
	            tListTable.add(oneCardInfo);
	        }	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
		if(type.equals(this.polInfo)){
			String[] tCertifyListInfoTitle = new String[11];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "WRAPCODE";
	        tCertifyListInfoTitle[2] = "RISKCODE";
	        tCertifyListInfoTitle[3] = "RISKNAME";
	        tCertifyListInfoTitle[4] = "DUTYCODE";
	        tCertifyListInfoTitle[5] = "DUTYNAME";
	        tCertifyListInfoTitle[6] = "AMNT";
	        tCertifyListInfoTitle[7] = "POLICYTERM";
	        tCertifyListInfoTitle[8] = "PAYMENTPERIOD";
	        tCertifyListInfoTitle[9] = "PAYMENTMETHOD";
	        tCertifyListInfoTitle[10] = "PAYYEARS";
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	        
	        //update by dhc in 2014-4-2 about the duty of WX0012 and ZZJ004
	        if(this.wrapcode!=null&&(this.wrapcode.equals("WX0012")||this.wrapcode.equals("ZZJ004"))){
	        	int i=1;
	        	String[] tTmpCardInfo = data.getRowData(i);
	    		
	            oneCardInfo = new String[11];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = this.wrapcode;
	            oneCardInfo[2] = tTmpCardInfo[0];
	           
	            oneCardInfo[3]="百万安行个人综合保障计划";
	            
	            oneCardInfo[4] = tTmpCardInfo[2];
	            oneCardInfo[5] = tTmpCardInfo[1]; // 险种名称
	            oneCardInfo[6] = tTmpCardInfo[5];
	            oneCardInfo[7] = tTmpCardInfo[6]+tTmpCardInfo[9];
	            oneCardInfo[8] = tTmpCardInfo[7];
	            oneCardInfo[9] = tTmpCardInfo[8];
	            oneCardInfo[10] = tTmpCardInfo[10];
	            
	           //趸缴的话缴费年期为空
	            if(tTmpCardInfo[11] != null){
	            	 if(tTmpCardInfo[11].equals("0")){
	            		 oneCardInfo[10] = "";
	 	            }
	            }
	           
	            tListTable.add(oneCardInfo);
	        }else{
	        	for (int i = 1; i <= data.MaxRow; i++)
		        {
		            String[] tTmpCardInfo = data.getRowData(i);
		
		            oneCardInfo = new String[11];
		            oneCardInfo[0] = String.valueOf(i);
		            oneCardInfo[1] = this.wrapcode;
		            oneCardInfo[2] = tTmpCardInfo[0];
		            //modify by zhangyige indigo 取col4作为责任名称
		            if(tTmpCardInfo[4]!= null && !"".equals(tTmpCardInfo[4])){
		            	oneCardInfo[3] = tTmpCardInfo[4]; //outdutyname
		            }else{
		            	oneCardInfo[3] = tTmpCardInfo[3]; //dutyname
		            }
		            
		            //	          modify by licaiyan indigo WX0010产品显示责任名称，
		            if("WX0010".equals(this.wrapcode) || "ZZJ003".equals(this.wrapcode)){
		            	if(tTmpCardInfo[0] != null && !tTmpCardInfo[0].equals("")){
			            	if("333601".equals(tTmpCardInfo[0])){//附加险
			            		oneCardInfo[3]="少儿护理保障计划";
			            	}
			            	if("231601".equals(tTmpCardInfo[0])){//主险
			            		oneCardInfo[3]="少儿重大疾病保障计划";
			            	}
			            }
		            }
		            
		            oneCardInfo[4] = tTmpCardInfo[2];
		            oneCardInfo[5] = tTmpCardInfo[1]; // 险种名称
		            oneCardInfo[6] = tTmpCardInfo[5];
		            oneCardInfo[7] = tTmpCardInfo[6]+tTmpCardInfo[9];
		            oneCardInfo[8] = tTmpCardInfo[7];
		            oneCardInfo[9] = tTmpCardInfo[8];
		            oneCardInfo[10] = tTmpCardInfo[10];
		            
		           //趸缴的话缴费年期为空
		            if(tTmpCardInfo[11] != null){
		            	 if(tTmpCardInfo[11].equals("0")){
		            		 oneCardInfo[10] = "";
		 	            }
		            }
		           
		            tListTable.add(oneCardInfo);
		        }
	        }
	        
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
		if(type.equals(this.termInfo)){
			String[] tCertifyListInfoTitle = new String[5];
			tCertifyListInfoTitle[0] = "PRINTINDEX";//序号
	        tCertifyListInfoTitle[1] = "TERMNAME";//条款名称
	        tCertifyListInfoTitle[2] = "FILENAME";//文件名称
	        tCertifyListInfoTitle[3] = "DOCUMENTNAME";//模板名称
	        tCertifyListInfoTitle[4] = "RISKWRAPPLANNAME";//套餐保障计划说明
	        

	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	        if(data != null && data.MaxRow>0){
	        	HashMap map = new HashMap();
	        	for (int i = 1; i <= data.MaxRow; i++)
		        {
	        		if (map.get(data.GetText(i, 2)) == null)
	                {
	        		   String[] tTmpCardInfo = data.getRowData(i);
	 		           oneCardInfo = new String[5];
	 		           oneCardInfo[0] = String.valueOf(i);
	 		           oneCardInfo[1] = tTmpCardInfo[0];
	 		           oneCardInfo[2] = "0";
	 		           oneCardInfo[3] = tTmpCardInfo[1];
	 		           oneCardInfo[4] = getRiskWrapPlanName(tTmpCardInfo[2]);
	 		           tListTable.add(oneCardInfo);
	 		           map.put(data.GetText(i, 2), "1");
	                }
//		            SSRS tSSRS = getDutyTerms(data.GetText(i, 3));
//		            if(tSSRS != null && tSSRS.MaxRow>0){
//		            	for(int j=1;j<=tSSRS.MaxRow;j++){
//		            		String[] tDutyTmpCardInfo = data.getRowData(i);
//				            oneCardInfo = new String[5];
//				            oneCardInfo[0] = String.valueOf(i);
//				            oneCardInfo[1] = tDutyTmpCardInfo[0];
//				            oneCardInfo[2] = "1";
//				            oneCardInfo[3] = tDutyTmpCardInfo[1];
//				            oneCardInfo[4] = getRiskWrapPlanName(tDutyTmpCardInfo[2]);
//				            tListTable.add(oneCardInfo);
//			            }
//		            }
		        }
	        }else{
	        	data = getOtherTerms();
	        	   if (data != null && data.MaxRow > 0) {
						for (int i = 1; i <= data.MaxRow; i++) {
							String[] tTmpCardInfo = data.getRowData(i);
							oneCardInfo = new String[5];
							oneCardInfo[0] = String.valueOf(i);
							oneCardInfo[1] = tTmpCardInfo[0];
							oneCardInfo[2] = "0";
							oneCardInfo[3] = tTmpCardInfo[1];
							oneCardInfo[4] = getRiskWrapPlanName(tTmpCardInfo[2]);
							tListTable.add(oneCardInfo);
						}
					} else {
							oneCardInfo = new String[5];
							oneCardInfo[0] = " ";
							oneCardInfo[1] = " ";
							oneCardInfo[2] = " ";
							oneCardInfo[3] = " ";
							oneCardInfo[4] = " ";
							tListTable.add(oneCardInfo);
					}
		        }
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
        
		}
// 指定医院
		if(type.equals(this.hospitalInfo)){
			String[] tCertifyListInfoTitle = new String[5];
			tCertifyListInfoTitle[0] = "HOSPITCODE";//医院编码
	        tCertifyListInfoTitle[1] = "HOSPITNAME";//医院名称
	        tCertifyListInfoTitle[2] = "ADDRESS";//医院地址	        
	        tCertifyListInfoTitle[3] = "ZIPCODE";//邮编
	        tCertifyListInfoTitle[4] = "PHONE";//联系电话
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[5];
	            oneCardInfo[0] = tTmpCardInfo[0];
	            oneCardInfo[1] = tTmpCardInfo[1];
	            oneCardInfo[2] = tTmpCardInfo[2];
	            oneCardInfo[3] = tTmpCardInfo[3];
	            oneCardInfo[4] = tTmpCardInfo[4];
	            tListTable.add(oneCardInfo);
	        }	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
//		推荐医院
		if(type.equals(this.recommendHospitals)){
			String[] tCertifyListInfoTitle = new String[5];
			tCertifyListInfoTitle[0] = "HOSPITCODE";//医院编码
	        tCertifyListInfoTitle[1] = "HOSPITNAME";//医院名称
	        tCertifyListInfoTitle[2] = "ADDRESS";//医院地址	        
	        tCertifyListInfoTitle[3] = "ZIPCODE";//邮编
	        tCertifyListInfoTitle[4] = "PHONE";//联系电话
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[5];
	            oneCardInfo[0] = tTmpCardInfo[0];
	            oneCardInfo[1] = tTmpCardInfo[1];
	            oneCardInfo[2] = tTmpCardInfo[2];
	            oneCardInfo[3] = tTmpCardInfo[3];
	            oneCardInfo[4] = tTmpCardInfo[4];
	            tListTable.add(oneCardInfo);
	        }	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
		
		if(type.equals(this.cvalueend)){
			String[] tCertifyListInfoTitle = new String[2];
			tCertifyListInfoTitle[0] = "";
	        tCertifyListInfoTitle[1] = "";
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	        
	        String[] oneCardInfo = new String[2];
	        oneCardInfo[0] = "";
            oneCardInfo[1] = "";
            tListTable.add(oneCardInfo);
            cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
        return true;
    }
	
	/**
	 * 生成XML
	 * @param cXmlExport
	 * @param data
	 * @param type
	 * @return
	 */
	private boolean dealLCCustomerImpartsInfo(XmlExport cXmlExport,List aList,String type)
    {
		if(type.equals("LCCustomerImparts")){
			ListTable tListTable = new ListTable();
			String[] tLCCustomerImpartsInfoTitle = new String[3];
			tLCCustomerImpartsInfoTitle[0] = "INSUREDNO";//被保人客户号码
			tLCCustomerImpartsInfoTitle[1] = "IMPARTCONTENT";//告知内容
			tLCCustomerImpartsInfoTitle[2] = "IMPARTPARAMMODLE";//告知结果
			for(int insuNo=0;insuNo<insurednoList.size();insuNo++){
				for (int i = 0; i < aList.size(); i++)
		        {
					LCCustomerImpartTable tLCCustomerImpartTable = (LCCustomerImpartTable)aList.get(i);
					if(insurednoList.get(insuNo).equals(tLCCustomerImpartTable.getInsuredNo())){
						String[] oneCardInfo = null;
			            oneCardInfo = new String[3];
			            oneCardInfo[0] = tLCCustomerImpartTable.getInsuredNo();
			            oneCardInfo[1] = (i+1)+"."+tLCCustomerImpartTable.getImpartContent();
			            oneCardInfo[2] = tLCCustomerImpartTable.getImpartParamModle();
			            tListTable.add(oneCardInfo);
			        }
		        }
			}
	        cXmlExport.addListTableImparts("LCCUSTOMERIMPARTS","LCCUSTOMERIMPART",tListTable,tLCCustomerImpartsInfoTitle);  
		}
		return true;
    }
	/**
	 * 客户服务指南节点，col节点中无需给值 生成XML
	 * @param cXmlExport
	 * @param data
	 * @param type
	 * @return
	 */
	private boolean dealNullNode(XmlExport cXmlExport,String type,String childType)
    {
	    cXmlExport.addListTableHealthNotic(type,childType);  
		return true;
    }
	
	/**
	 * 上传XML到FTP服务器
	 * 
	 * @return
	 */
	private boolean FTPSendFile(){
	      
	    /**文件*/
	    String fileName=this.contNo;
	    String filePath="";
	    
	    try{
	    	String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PDFstrUrl'";//生成文件的存放路径
	    	filePath = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
	        System.out.println("生成文件的存放路径   "+filePath);//调试用－－－－－
	        if(filePath == null ||filePath.equals("")){
	            System.out.println("获取文件存放路径出错");
	            return false;
	        }
//	        filePath="D:\\WXP001\\";
		    String xmlDoc=this.dXmlExport.outputString();
		    
	        this.dXmlExport.outputDocumentToFile(filePath, fileName,"UTF-8");
	        System.out.println("doc=="+xmlDoc);
		    //上传文件到FTP服务器
	    	if (!sendZip(filePath+fileName+".xml")) {
	    		System.out.println("复杂产品上传文件到FTP服务器失败！");
	            return false;
	        } else {
	            //上传成功后删除
	        	System.out.println("复杂产品上传文件到FTP服务器成功！");
	        	File xmlFile = new File(filePath+fileName+".xml");
//	        	xmlFile.delete(); //删除上传完的文件
	        }		    
	    }catch(Exception e){
	    	e.printStackTrace();
	    	return false;
	    }
		return true;
	}
	/**
     * sendZip
     * 上传文件至指定FTP（清单文件和单打的数据文件在同一个目录）
     * @param string String
     * @param flag String
     * @return boolean
     */
    private boolean sendZip(String cFileUrlName) {
        //登入前置机
         String ip = "";
         String user = "";
         String pwd = "";
         int port = 21;//默认端口为21

         String sql = "select code,codename from LDCODE where codetype='printserver'";
         SSRS tSSRS = new ExeSQL().execSQL(sql);
         if(tSSRS != null){
             for(int m=1;m<=tSSRS.getMaxRow();m++){
                 if(tSSRS.GetText(m,1).equals("IP")){
                     ip = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("UserName")){
                     user = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Password")){
                     pwd = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Port")){
                     port = Integer.parseInt(tSSRS.GetText(m,2));
                 }
             }
         }
//         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port,"ActiveMode");
         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port);
//         FTPTool tFTPTool = new FTPTool("10.252.130.228", "zhangshuo ", "111111", 21);
         try
         {
             if (!tFTPTool.loginFTP())
             {
                 System.out.println(tFTPTool.getErrContent(1));
                 return false;
             }
             else
             {
                  if (!tFTPTool.upload(cFileUrlName)) {
                         CError tError = new CError();
                         tError.errorMessage = tFTPTool
                                               .getErrContent(
                                 FTPReplyCodeName.LANGUSGE_CHINESE);
                         
                         System.out.println("上载文件失败!");
                         return false;
                  }

                 tFTPTool.logoutFTP();
             }
         }
         catch (SocketException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                      .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
              
             System.out.println("上载文件失败，可能是网络异常!");
             return false;
         }
         catch (IOException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                     .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
             
             System.out.println("上载文件失败，可能是无法写入文件");
             return false;
         }

         return true;

    }
	public static void main(String args[]){
//		WxPrintBL ecp = new WxPrintBL();
//		MsgCollection tMsgCollection = new MsgCollection();
//		MsgHead tMsgHead = new MsgHead();
//		tMsgHead.setBatchNo("123456789");
//		tMsgHead.setBranchCode("001");
//		tMsgHead.setMsgType("WXP0001");
//		tMsgHead.setSendDate("2010-9-21");
//		tMsgHead.setSendTime("09:00:00");
//		tMsgHead.setSendOperator("sino");
//		tMsgCollection.setMsgHead(tMsgHead);
//		CertPrintTable tCertPrintTable = new CertPrintTable();
//		tCertPrintTable.setCardNo("15000000071");
//		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);		
//		LCCustomerImpartTable tLCCustomerImpartTable = new LCCustomerImpartTable();
//		tLCCustomerImpartTable.setInsuredNo("013927674");
//		tLCCustomerImpartTable.setImpartContent("身高(cm)");
//		tLCCustomerImpartTable.setImpartParamModle("170");
//		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable);
////		LCCustomerImpartTable tLCCustomerImpartTable1 = new LCCustomerImpartTable();
////		tLCCustomerImpartTable1.setInsuredNo("000996002");
////		tLCCustomerImpartTable1.setImpartContent("体重(kg)");
////		tLCCustomerImpartTable1.setImpartParamModle("70");
////		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable1);
////		LCCustomerImpartTable tLCCustomerImpartTable2 = new LCCustomerImpartTable();
////		tLCCustomerImpartTable2.setInsuredNo("000996002");
////		tLCCustomerImpartTable2.setImpartContent("是否吸烟");
////		tLCCustomerImpartTable2.setImpartParamModle("否");
////		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable2);
////		OnlineDeclareTable tOnlineDeclareTable = new OnlineDeclareTable();
////		tOnlineDeclareTable.setContentTag("燃气地址");
////		tOnlineDeclareTable.setReportedContent("地址信息");
////		tMsgCollection.setBodyByFlag("OnlineDeclareTable", tOnlineDeclareTable);
////		OnlineDeclareTable tOnlineDeclareTable1 = new OnlineDeclareTable();
////		tOnlineDeclareTable1.setContentTag("联系电话");
////		tOnlineDeclareTable1.setReportedContent("1301010101");
////		tMsgCollection.setBodyByFlag("OnlineDeclareTable", tOnlineDeclareTable1);
//		ecp.deal(tMsgCollection);	
		WxPrintEPiccBL ecp = new WxPrintEPiccBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo("WX0002EC_WX2015010620354855136");
		tMsgHead.setBranchCode("001");
		tMsgHead.setMsgType("WXP0001");
		tMsgHead.setSendDate("2010-9-21");
		tMsgHead.setSendTime("09:00:00");
		tMsgHead.setSendOperator("sino");
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo("15000002725");
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);		
		LCCustomerImpartTable tLCCustomerImpartTable = new LCCustomerImpartTable();
//		tLCCustomerImpartTable.setInsuredNo("013927674");
//		tLCCustomerImpartTable.setImpartContent("身高(cm)");
//		tLCCustomerImpartTable.setImpartParamModle("170");
		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable);
//		LCCustomerImpartTable tLCCustomerImpartTable1 = new LCCustomerImpartTable();
//		tLCCustomerImpartTable1.setInsuredNo("000996002");
//		tLCCustomerImpartTable1.setImpartContent("体重(kg)");
//		tLCCustomerImpartTable1.setImpartParamModle("70");
//		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable1);
//		LCCustomerImpartTable tLCCustomerImpartTable2 = new LCCustomerImpartTable();
//		tLCCustomerImpartTable2.setInsuredNo("000996002");
//		tLCCustomerImpartTable2.setImpartContent("是否吸烟");
//		tLCCustomerImpartTable2.setImpartParamModle("否");
//		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable2);
//		OnlineDeclareTable tOnlineDeclareTable = new OnlineDeclareTable();
//		tOnlineDeclareTable.setContentTag("燃气地址");
//		tOnlineDeclareTable.setReportedContent("地址信息");
//		tMsgCollection.setBodyByFlag("OnlineDeclareTable", tOnlineDeclareTable);
//		OnlineDeclareTable tOnlineDeclareTable1 = new OnlineDeclareTable();
//		tOnlineDeclareTable1.setContentTag("联系电话");
//		tOnlineDeclareTable1.setReportedContent("1301010101");
//		tMsgCollection.setBodyByFlag("OnlineDeclareTable", tOnlineDeclareTable1);
		ecp.deal(tMsgCollection);	
		
	}
    /**
     * 计算条形码长度
     * @param barStr
     * @return
     */
    public int CalculateBarCodeLen(String barStr){
    	  	
    	int strLen=0;
    	char [] barArr = barStr.toCharArray();
    	for (int i=0;i<barArr.length;i++){
    		String tempStr = String.valueOf(barArr[i]);
    		if(tempStr.getBytes().length ==2){
    			strLen+=7;
    		}else{
    			strLen+=1;
    		}
    	}
    	return strLen;
    }
    
    /**
     * 去掉年份中月和日前面的零
     * @return
     */
    public String DealDateZero(String dateStr){
    	String result=null;
    	String year =null;
    	String month =null;
    	String date =null;
    	   	
    	if(dateStr.equals("") || dateStr == null){
    		System.out.println("没有得到有效的字符类型的日期");
    		return null;
    	}
    	year = dateStr.substring(0, 4);
    	month = dateStr.substring(5, 7);
    	date = dateStr.substring(8, 10);
    	
    	if(month.substring(0,1).equals("0")){
    		month= month.substring(1);
    	}
    	if(date.substring(0,1).equals("0")){
    		date= date.substring(1);
    	}
    	result = year+"-"+month+"-"+date;
    	return result;
    }
    
    //gzh 20110118
    private boolean dealPrintMag() {
    	
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String tLimit = PubFun.getNoLimit("86000000");
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        tLOPRTManagerSchema.setOtherNo(cardNo); //存放卡号
        tLOPRTManagerSchema.setOtherNoType("02");
        tLOPRTManagerSchema.setCode("WXP0001");
        tLOPRTManagerSchema.setManageCom("86000000");
        tLOPRTManagerSchema.setAgentCode("aaaa");
        tLOPRTManagerSchema.setReqCom("86000000");
        tLOPRTManagerSchema.setReqOperator("001");
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("1");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        MMap tMMap = new MMap();
        tMMap.put(tLOPRTManagerSchema, "INSERT");
        data.add(tMMap);
        return true;
    }
//  by gzh 20110302 获取date前一天的日期
    public String getPreviousDate(Date date){ 
    	Calendar calendar=Calendar.getInstance(); 
    	FDate tFDate = new FDate();
    	calendar.setTime(date); 
    	int day=calendar.get(Calendar.DATE); 
    	calendar.set(Calendar.DATE,day-1); 
    	return tFDate.getString(calendar.getTime()); 
    }
    /**
     * 条款信息查询
     * 会出现多险种对应同意主条款
     * 排序方式为主条款，享有此主条款的险种的责任条款
     * 可采用二级循环来实现，不过比较麻烦
     * @return SSRS
     * @throws Exception
     */
    private SSRS getTerms(){
        //设置查询对象
        ExeSQL tExeSQL = new ExeSQL();
        StringBuffer tSBql = new StringBuffer();
        tSBql.append(" select * from ");
        tSBql.append(" ( ");
        tSBql.append(" select ");
        tSBql.append(" distinct ldrp.Codename, ldrp.codealias, ldrp.code, ");
        tSBql.append(" (case when (select distinct lcp.RiskCode from LCPol lcp where 1 = 1 and lcp.PolNo in (select lcp.MainPolNo from LCPol lcp where 1 = 1 and lcp.ContNo = '"
                        + this.contNo
                        + "' and lcp.RiskCode = ldrp.code)) is null then ldrp.code else (select distinct lcp.RiskCode from LCPol lcp where 1 = 1 and lcp.PolNo in (select lcp.MainPolNo from LCPol lcp where 1 = 1 and lcp.ContNo = '"
                        + this.contNo
                        + "' and lcp.RiskCode = ldrp.code)) end || trim(lmra.SubRiskFlag)) MSRiskFlag");
        tSBql.append(" from ldcode1 ldrp ");
        tSBql.append(" inner join LMRiskApp lmra on lmra.RiskCode = ldrp.code ");
        tSBql.append(" where 1 = 1 and codetype='eriskprint' ");
        tSBql.append(" and ldrp.code in (select RiskCode from LCPol where ContNo = '"
                + this.contNo + "') ");
        tSBql.append(" and code1 = '0' ");
        tSBql.append(" ) as tmp ");
        tSBql.append(" union ");
        tSBql.append(" select * from ");
        tSBql.append(" ( ");
        tSBql.append(" select ");
        tSBql.append(" distinct ldrp.ItemName, ldrp.FileName, ldrp.RiskCode, ");
        tSBql.append(" (case when (select distinct lcp.RiskCode from LCPol lcp where 1 = 1 and lcp.PolNo in (select lcp.MainPolNo from LCPol lcp where 1 = 1 and lcp.ContNo = '"
                        + this.contNo
                        + "' and lcp.RiskCode = ldrp.RiskCode)) is null then ldrp.RiskCode else (select distinct lcp.RiskCode from LCPol lcp where 1 = 1 and lcp.PolNo in (select lcp.MainPolNo from LCPol lcp where 1 = 1 and lcp.ContNo = '"
                        + this.contNo
                        + "' and lcp.RiskCode = ldrp.RiskCode)) end || trim(lmra.SubRiskFlag)) MSRiskFlag");
        tSBql.append(" from LDRiskPrint ldrp ");
        tSBql.append(" inner join LMRiskApp lmra on lmra.RiskCode = ldrp.RiskCode ");
        tSBql.append(" where 1 = 1 ");
        tSBql.append(" and ldrp.RiskCode in (select RiskCode from LCPol where ContNo = '"
                + this.contNo + "') ");
        tSBql.append(" and ItemType = 'b' ");
        tSBql.append(" ) as tmp ");

        SSRS tSSRS = new SSRS();
        System.out.println(tSBql.toString());
        tSSRS = tExeSQL.execSQL(tSBql.toString());
        return tSSRS;
    }
    /**
     * 条款信息查询
     * 查询当前主条款下的责任条款信息
     * @return SSRS
     * @throws Exception
     */
    private SSRS getDutyTerms(String aRiskCode){
        StringBuffer tSBql = new StringBuffer();
        tSBql = new StringBuffer(256);
        tSBql.append("select distinct codename,codealias from ldcode1 where codetype='eriskprint' and code in (select RiskCode from LCPol where ContNo = '");
        tSBql.append(this.contNo);
        tSBql.append("') and code1 = '1' and code in (select code from ldcode1 where code = '");
        tSBql.append(aRiskCode);
        tSBql.append("')");
        tSBql.append(" union ");
        tSBql.append("select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '");
        tSBql.append(this.contNo);
        tSBql.append("') and ItemType = '1' and RiskCode in (select RiskCode from LDRiskPrint where riskcode = '");
        tSBql.append(aRiskCode);
        tSBql.append("')");

        SSRS tSSRS = new ExeSQL().execSQL(tSBql.toString());
    	return tSSRS;
    }
    /**
     * 条款信息查询
     * @return SSRS
     * @throws Exception
     */
    private SSRS getOtherTerms(){
    	StringBuffer tSBql = new StringBuffer();
    	tSBql = new StringBuffer(256);
        tSBql.append("select distinct codename,codealias from ldcode1 where codetype='eriskprint' and code in (select RiskCode from LCPol where ContNo = '");
        tSBql.append(this.contNo);
        tSBql.append("') and code1 = '1' ");
        tSBql.append(" union ");
        tSBql.append("select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '");
        tSBql.append(this.contNo);
        tSBql.append("') and ItemType = '1' ");

        SSRS tSSRS = new ExeSQL().execSQL(tSBql.toString());
        return tSSRS;
    }   
    /**
     * getRiskWrapPlanName
     * 若险种是主附险绑定型，则返回“×××保障计划由***险种和附加***险种组成”
     * @param RiskCode String
     * @return String
     */
    private String getRiskWrapPlanName(String cRiskCode)
    {
        String sql = "select RiskWrapPlanName, " + "  (select RiskName from LMRisk where RiskCode=a.Code1),"
                + "   (select RiskName from LMRisk where RiskCode=a.Code) " + "from LDCode1 a "
                + "where CodeType = 'checkappendrisk' " + "   and (Code = '" + cRiskCode + "' or " + "       Code1 = '"
                + cRiskCode + "') ";
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            return "";
        }
        System.out.println("保险计划名称-------------" + tSSRS.GetText(1, 1) + "保障计划由" + tSSRS.GetText(1, 2) + "险种和附加" + tSSRS.GetText(1, 2) + "险种组成");
        return tSSRS.GetText(1, 1) + "保障计划由" + tSSRS.GetText(1, 2) + "险种和附加" + tSSRS.GetText(1, 2) + "险种组成";
    }
    
    /**
     * 根据个单合同号，查询指定医院明细信息
     * @param 
     * @return SSRS
     * @throws Exception
     */
    private SSRS getHospital()
    {
        //查询ldcode表，获取是否需要打印医院信息
        StringBuffer tSBql = new StringBuffer(150);
        tSBql.append("select count(1) from ldcode where codetype='printriskcode' and codealias='1' and code in (select riskcode from LCPol where ContNo='");
        tSBql.append(this.contNo);
        tSBql.append("')");

        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(tSBql.toString());
        //如果查询的结果是0，表示该合同下没有险种需要打印医院信息
        if ("0".equals(tCount))
        {
            return null;
        }
        else
        {
        	String ManageComSql = "select managecom from lccont where contno = '"+this.contNo+"'";
        	String ManageCom = new ExeSQL().getOneValue(ManageComSql);
        	if(!"".equals(ManageCom) && ManageCom.length()>=4){
        		SSRS HospitalSSRS = new SSRS();
                //添加是否有 Hospital 信息
                String hasHospitalSql = "select HospitCode,HospitName,Address,ZipCode,Phone from LDHospital where ManageCom = '"
                        + ManageCom.substring(0, 4) + "' and AssociateClass = '2' ";//2为指定医院
                HospitalSSRS = new ExeSQL().execSQL(hasHospitalSql);

                return HospitalSSRS;
        	}
        }
        return null;
    }
    /**
     * 根据个单合同号，查询推荐医院明细信息
     * @param 
     * @return SSRS
     * @throws Exception
     */
    private SSRS getReHospitals()
    {
        //查询ldcode表，获取是否需要打印医院信息
        StringBuffer tSBql = new StringBuffer(150);
        tSBql.append("select count(1) from ldcode where codetype='printriskcode' and codealias='1' and code in (select riskcode from LCPol where ContNo='");
        tSBql.append(this.contNo);
        tSBql.append("')");

        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(tSBql.toString());
        //如果查询的结果是0，表示该合同下没有险种需要打印医院信息
        if ("0".equals(tCount))
        {
            return null;
        }
        else
        {
        	String ManageComSql = "select managecom from lccont where contno = '"+this.contNo+"'";
        	String ManageCom = new ExeSQL().getOneValue(ManageComSql);
        	if(!"".equals(ManageCom) && ManageCom.length()>=4){
        		SSRS HospitalSSRS = new SSRS();
                //添加是否有 Hospital 信息
                String hasHospitalSql = "select HospitCode,HospitName,Address,ZipCode,Phone from LDHospital where ManageCom = '"
                        + ManageCom.substring(0, 4) + "' and AssociateClass = '1' "; //1为推荐医院
                HospitalSSRS = new ExeSQL().execSQL(hasHospitalSql);

                return HospitalSSRS;
        	}
        }
        return null;
    }
    /**
	 * 生成XML
	 * @param cXmlExport
	 * @param data
	 * @param type
	 * @return
	 */
	private boolean dealOnlineDeclareListInfo(XmlExport cXmlExport,List aList,String type)
    {
//		在线填报动态个人信息
		String[] tCertifyListInfoTitle = new String[2];
		tCertifyListInfoTitle[0] = "填报内容标签";
        tCertifyListInfoTitle[1] = "填报内容";
        
        ListTable tListTable = new ListTable();
        tListTable.setName(type);

        String[] oneCardInfo = null;
        if(aList != null && aList.size()>0){
        	for (int i = 0; i < aList.size(); i++)
	        {
        		OnlineDeclareTable tOnlineDeclareTable = (OnlineDeclareTable)aList.get(i);
	            oneCardInfo = new String[2];
	            oneCardInfo[0] = tOnlineDeclareTable.getContentTag();
	            oneCardInfo[1] = tOnlineDeclareTable.getReportedContent();
	            tListTable.add(oneCardInfo);
	        }	
        }
        cXmlExport.addOnlineDeclareListTable(tListTable, tCertifyListInfoTitle);
        return true;
    }
	

}
