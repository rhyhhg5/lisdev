package com.cbsws.xml.ctrl.complexpds;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.ECCertSalesPayInfoTable;
import com.cbsws.obj.LCAppntTable;
import com.cbsws.obj.LCBnfTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LCInsuredTable;
import com.cbsws.obj.NewLCAppntTable;
import com.cbsws.obj.NewLCContTable;
import com.cbsws.obj.NewLCInsuredTable;
import com.cbsws.obj.WrapParamTable;
import com.cbsws.obj.WrapTable;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPersonTraceSchema;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.schema.WXContRelaInfoSchema;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.tb.ExemptionRiskBL;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPersonTraceSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class WxUW extends ABusLogic {
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public double mPrem=0;//保费
	public double mAmnt=0;//保额
	
	public LCContTable cLCContTable;
	public LCAppntTable cLCAppntTable;
	public List cLCInsuredList;
	public WrapTable cWrapTable;
	public List cLCBnfList;
	public List cWrapParamList;//套餐参数问题
	public ECCertSalesPayInfoTable cECCertSalesPayInfo;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	public LCPersonTraceSchema appntLcPersonTrace  = new LCPersonTraceSchema();
	public LCPersonTraceSet insuredLcPersonTraceSet = new LCPersonTraceSet();
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始处理网销复杂产品核保业务");
		System.out.println("开始处理网销复杂产品核保业务979798798798789797");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
	        if (tLCContList == null || tLCContList.size() != 1)
	        {
	            errLog("获取保单信息失败。");
	            return false;
	        }
	        cLCContTable = (LCContTable) tLCContList.get(0); //获取保单信息，只有一个保单
	        mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();
			System.out.println("管理￥￥￥#￥￥￥"+cLCContTable.getManageCom());
          
//            if(cLCContTable.getPayMode() == null || "".equals(cLCContTable.getPayMode())){
//            	errLog("获取保单缴费方式失败！");
//                return false;
//            }
            System.out.println("保单来源="+cLCContTable.getPolicySource());
			List tWrapList = cMsgInfos.getBodyByFlag("WrapTable");
        	if (tWrapList == null || tWrapList.size() != 1)
            {
                errLog("试算或核保时，获取套餐编码失败。");
                return false;
            }
        	cWrapTable = (WrapTable)tWrapList.get(0);
        	cRiskWrapCode = cWrapTable.getRiskWrapCode();
        	if (cRiskWrapCode == null || "".equals(cRiskWrapCode))
            {
                errLog("在报文中获取套餐编码失败。");
                return false;
            }
        	WxCommon tWxCommon = new WxCommon(cRiskWrapCode);
        	if(!tWxCommon.checkTheOne(tWrapList)){
        		errLog("每次投保只能购买一个产品。");
                return false;
        	}
//        	根据投保单申请日期校验产品是否已停售
        	mError = tWxCommon.checkWrapStop(cLCContTable.getPolApplyDate());
        	if (!"".equals(mError))
            {
                errLog(mError);
                return false;
            }
        	List tLCAppntList = cMsgInfos.getBodyByFlag("LCAppntTable");
            if (tLCAppntList == null || tLCAppntList.size() != 1)
            {
                errLog("获取投保人信息失败。");
                return false;
            }
            cLCAppntTable = (LCAppntTable)tLCAppntList.get(0);//获取投保人信息，每个保单只有一个投保人
            //投保人授权使用客户信息不能为空
            String appntAuthorization=cLCAppntTable.getAuthorization();
            if(appntAuthorization==null || appntAuthorization.equals("")){
            	cLCAppntTable.setAuthorization("0");
            }
            appntLcPersonTrace.setAuthorization(appntAuthorization);
            appntLcPersonTrace.setAuthType(cLCAppntTable.getAuthType());
            appntLcPersonTrace.setBusinessLink(cLCAppntTable.getBusinessLink());
            appntLcPersonTrace.setCustomerContact(cLCAppntTable.getCustomerContact());
            appntLcPersonTrace.setSharedMark(cLCAppntTable.getSharedMark());
            appntLcPersonTrace.setSpecialLimitMark(cLCAppntTable.getSpecialLimitMark());
          //校验投保人身份证号信息
            if(cLCAppntTable != null ){
            	String error =PubFun.CheckIDNo(cLCAppntTable.getAppntIDType(), cLCAppntTable.getAppntIDNo(), cLCAppntTable.getAppntBirthday(), cLCAppntTable.getAppntSex());
            	if(!"".equals(error)){
            		errLog("该产品的投保人"+ error);
                    return false;
            	}
            }
            
            cLCInsuredList = cMsgInfos.getBodyByFlag("LCInsuredTable");//获取被保人

            if (cLCInsuredList == null)
            {
                errLog("获取被保人信息失败。");
                return false;
            }
            if(!tWxCommon.checkTheOne(cLCInsuredList)){
        		errLog("该产品的被保人只能为一人！");
                return false;
        	}
            if(cLCInsuredList != null && cLCInsuredList.size()>0){
            	LCInsuredTable tLCInsuredTable =(LCInsuredTable)cLCInsuredList.get(0);
        		String error = PubFun.CheckIDNo(tLCInsuredTable.getIDType(), tLCInsuredTable.getIDNo(), tLCInsuredTable.getBirthday(), tLCInsuredTable.getSex());
        		if(!"".equals(error)){
        			errLog("该产品的被保人" + error);
                    return false;
        		}
        		//被保人授权使用客户信息不能为空
                String InsuredAuthorization=tLCInsuredTable.getAuthorization();
                if(InsuredAuthorization==null || InsuredAuthorization.equals("")){
                	tLCInsuredTable.setAuthorization("0");
                }
                LCPersonTraceSchema insuredLcPersonTrace = new LCPersonTraceSchema();
                insuredLcPersonTrace.setAuthorization(InsuredAuthorization);
                insuredLcPersonTrace.setSharedMark(tLCInsuredTable.getSharedMark());
                insuredLcPersonTrace.setSpecialLimitMark(tLCInsuredTable.getSpecialLimitMark());
                insuredLcPersonTrace.setBusinessLink(tLCInsuredTable.getBusinessLink());
                insuredLcPersonTrace.setAuthType(tLCInsuredTable.getAuthType());
                insuredLcPersonTrace.setCustomerContact(tLCInsuredTable.getCustomerContact());
                insuredLcPersonTrace.setSpare1(tLCInsuredTable.getName());
                insuredLcPersonTraceSet.add(insuredLcPersonTrace);
            }
			
            cWrapParamList = cMsgInfos.getBodyByFlag("WrapParamTable");
            if (cWrapParamList == null)
            {
                errLog("获取计算保费参数信息失败。");
                return false;
            }
			
//            校验被保人年龄
            if(!tWxCommon.checkInsuredAge(cWrapParamList)){
            	errLog("被保人年龄不在投保年龄范围内!");
                return false;
            }
            
//	            校验投保人职业类型
//            if(!tWxCommon.checkOccupationType(cLCInsuredList)){
//            	errLog(mError);
//                return false;
//            }	
            cLCBnfList = cMsgInfos.getBodyByFlag("LCBnfTable");
            LCBnfTable tLCBnfTable = null;
            if (cLCBnfList != null && cLCBnfList.size() > 0)
            {
            	for(int a=0;a< cLCBnfList.size();a++){
            		tLCBnfTable=(LCBnfTable) cLCBnfList.get(a);
            		String error = PubFun.CheckIDNo(tLCBnfTable.getIDType(), tLCBnfTable.getIDNo(), tLCBnfTable.getBirthday(), tLCBnfTable.getSex());
            		if(!"".equals(error)){
            			errLog("该产品的受益人" + error);
                        return false;
            		}
                }
            }
            
            //校验身高体重#225-----fromyzfstart
//            if(cRiskWrapCode=="JX0001"){
//            	String flag=tWxCommon.checkStatureAndAvoirdupois((LCInsuredTable)cLCInsuredList.get(0));
//            	if("0".equals(flag)){
//            		System.out.println("身高和体重符合正常范围内");
//            	}else if("1".equals(flag)){
//            		errLog("身高超过正常范围");
//            		System.out.println("身高超过正常范围");
//                    return false;
//            	}else if("2".equals(flag)){
//            		errLog("体重超过正常范围");
//            		System.out.println("体重超过正常范围");
//                    return false;
//            	}else if("3".equals(flag)){
//            		errLog("年龄超过正常范围0-15岁");
//            		System.out.println("年龄超过正常范围0-15岁");
//                    return false;
//            	}else{
//            		errLog("校验身高体重未知异常");
//            		System.out.println("校验身高体重未知异常");
//                    return false;
//            	}
//            }
            //fromyzfend#225
            
//	            获取印刷号并更新卡号状态
    		if("".equals(StrTool.cTrim(cLCContTable.getPrtNo()))) {//第一次印刷号为空
//    			if(!getCardNo(cRiskWrapCode)){
//            		errLog(mError);
//	                return false;
//            	}
    			String tPrtno = "15"+PubFun1.CreateMaxNo("WXPRTNO", 9);//生成印刷号
    			cLCContTable.setPrtNo(tPrtno);
    		}else{//如果印刷号不为空，即是修改核保信息情况(不能重复购买)------fromyzfstart
    			String prtnoSql="select prtno from lccont where prtno = '"+cLCContTable.getPrtNo()+"'";
    			SSRS pSSRS=new ExeSQL().execSQL(prtnoSql);
    			if(pSSRS!=null && pSSRS.getMaxRow()>0){
    				String insuredSql="select name,sex,birthday,idtype,idno from lcinsured where prtno='"+cLCContTable.getPrtNo()+"'";
    				LCInsuredTable lcit=null;
    				lcit=(LCInsuredTable)cLCInsuredList.get(0);
    				SSRS ssrsResult=null;
    				ssrsResult=new ExeSQL().execSQL(insuredSql);
    				System.out.println(lcit.getName()+"-"+lcit.getSex()+"-"+lcit.getBirthday()+"-"+lcit.getIDType()+"-"+lcit.getIDNo()+
    						"\n"+ssrsResult.GetText(1, 1)+"-"+ssrsResult.GetText(1,2)+"-"+ssrsResult.GetText(1,3)+"-"+ssrsResult.GetText(1,4)+"-"+ssrsResult.GetText(1,5));
    				if(lcit.getName().equals(ssrsResult.GetText(1, 1)) 
    						&& lcit.getSex().equals(ssrsResult.GetText(1, 2)) 
    						&& lcit.getBirthday().equals(ssrsResult.GetText(1, 3)) 
    						&& lcit.getIDType().equals(ssrsResult.GetText(1,4)) 
    						&& lcit.getIDNo().equals(ssrsResult.GetText(1, 5))){
    					try{
	    					LCContSchema mLCContSchema = new LCContSchema();
	    					mLCContSchema.setPrtNo(cLCContTable.getPrtNo());
	    					LCContSet tLCContSet = new LCContSet();
	    					LCContDB tLCContDB = new LCContDB();
	    					tLCContSet=tLCContDB.executeQuery("select * from lccont where prtno = '"+cLCContTable.getPrtNo()+"' ");
	    					TransferData tTransferData = new TransferData();
	    					String tDeleteReason ="上一次输入保单信息有误，需重新修改";
	    					LCContSchema tLCContSchema = tLCContSet.get(1);
	    					tTransferData.setNameAndValue("DeleteReason",tDeleteReason);
    						// 准备传输数据 VData
    						VData tVData = new VData();
    						tVData.add( tLCContSchema );
    						tVData.add( tTransferData );
    						tVData.add(mGlobalInput);
    						// 数据传输
    						ContDeleteUI tContDeleteUI = new ContDeleteUI();
    						tContDeleteUI.submitData(tVData,"DELETE");
    					
	    				}
	    				catch(Exception e)
	    				{
	    					e.printStackTrace();
	    					errLog(mError);
	    					errLog("删除被保人异常！");
	    					System.out.println("删除被保人异常！");
	    					return false;
	    				}
    				}else{
    					errLog("prnNo在lccont已经存在，但当前操作的被保人与数据库lcinsured中相应的被保人不同，予以阻断");
    					return false;
    				}
    			}
    		}//-----------fromyzfend
//		         更新卡号状态结束
//					投保单录入
    		if("".equals(StrTool.cTrim(cLCContTable.getPrtNo()))) {
    			errLog("获取保单印刷号失败。");
                return false;
    		}
    		
    		   //套餐级别校验 add by licaiyan 2014-4-2
            if(!wrapCheck(cLCAppntTable,(LCInsuredTable)cLCInsuredList.get(0))){
            	
                 return false;
            }
    		
    		String sql = "select contno from lccont where prtno = '"+cLCContTable.getPrtNo()+"'";
    		SSRS tSSRS = new ExeSQL().execSQL(sql);
    		if(tSSRS ==null || tSSRS.getMaxRow()<=0){//第一次核保，保单号为空，以便后面生成保单号吗
    			cLCContTable.setContNo("");
    		}else{//多次核保，先删除保单数据
    			cLCContTable.setContNo(tSSRS.GetText(1, 1));
    			if(!deleteData()){
    				errLog(mError);
	                return false;
    			}
    		}
    		String tAgentCode = "";
    		//增加校验社保综拓业务员信息
    		if((null==cLCContTable.getAgentCode()||"".equals(cLCContTable.getAgentCode())) &&  !"18".equals(cLCContTable.getSaleChnl()) ){
				String AgentCodesql = "select agentcode from laagent where managecom = '"+cLCContTable.getManageCom()+"' " +
	//					"and agentcode like '%aaaaaa%' " +
						"and  branchtype='2' and branchtype2='01' and insideflag='0'";
	    		tAgentCode = new ExeSQL().getOneValue(AgentCodesql);
	    		if("".equals(StrTool.cTrim(tAgentCode))){
	    			mError = "根据管理机构获取销售人员信息失败！";
	    			errLog(mError);
	    			return false;
	    		}else{
	    			cLCContTable.setAgentCode(tAgentCode);
	    		}
    		}else if((null==cLCContTable.getAgentCode() || "".equals(cLCContTable.getAgentCode())) && "18".equals(cLCContTable.getSaleChnl())){
    			errLog("当销售渠道为社保综拓时，必须录入业务员编码！");
				return false;
    		}else if((null != cLCContTable.getAgentCode() || !"".equals(cLCContTable.getAgentCode())) && "18".equals(cLCContTable.getSaleChnl() )){
    			tAgentCode = cLCContTable.getAgentCode();
    			//校验业务员在职和销售渠道
    			String agentStateSql = "select 1 from LAAgent where AgentCode = '" 
    			        + tAgentCode + "' and AgentState < '06'";
    			SSRS aSSRS = new ExeSQL().execSQL(agentStateSql);
    			if(aSSRS == null || aSSRS.getMaxRow()<=0){
    				errLog("该业务员已离职！");
    				return false;
    			}
    			String agentCodeSql = "select a.GroupAgentCode,c.branchattr,a.managecom,a.name,a.idno,a.agentstate,a.Phone,a.Mobile,(select gradename from laagentgrade a1 where a1.gradecode=(select agentgrade from LATree a2 where a2.agentcode=a.agentcode))  from LAAgent a,LATree b,LABranchGroup c where 1=1 and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup and (a.AgentState is null or a.AgentState < '06')  and a.GroupAgentCode is not null and  a.GroupAgentCode<>''  and a.BranchType = '6' and a.BranchType2 = '01' and a.AgentCode like '"+tAgentCode+"%'  and a.ManageCom like '"+cLCContTable.getManageCom()+"%'" ;
    			SSRS bSSRS = new ExeSQL().execSQL(agentCodeSql);
    			if(bSSRS == null || bSSRS.getMaxRow()<=0){
    				errLog("业务员和销售渠道或管理机构不相符！");
    				return false;
    			}
    		}
//    		//-------------------fromyzfstart20141209
////    		根据管理机构获取agentcode
//    		AgentCodeTransformation a = new AgentCodeTransformation();
//    		if(null!=tAgentCode&&!"".equals(tAgentCode)){
//    			if(!a.AgentCode(cLCContTable.getAgentCode(), "Y")){
//    				System.out.println(a.getMessage());
//    				mError=a.getMessage();
//    				errLog(mError);
//    				return false;
//    			}
//    			tAgentCode = a.getResult();
//    		}
//    		
//			//----------------fromyzfend
			WxContBL ttWXContBL = new WxContBL(mGlobalInput,cLCContTable, cLCAppntTable,cLCInsuredList);
			mError = ttWXContBL.deal();
			if(!"".equals(mError)){
				errLog(mError);
                return false;
			}
        	WxContInsuredIntlBL ttWxContInsuredIntlBL = new WxContInsuredIntlBL(mGlobalInput,tMsgHead,cLCContTable,cLCAppntTable,cLCInsuredList,cWrapTable,cLCBnfList,cWrapParamList);
        	mError = ttWxContInsuredIntlBL.deal();
			if(!"".equals(mError)){
				errLog(mError);
                return false;
			}
			
			//-----------B2C#195fromyzfstart
			//封装数据
			WxContRelaInfoBL ttWXContRelaInfoBL=new WxContRelaInfoBL(tMsgHead,cLCContTable);
			mError=ttWXContRelaInfoBL.deal();
			if(!"".equals(mError)){
				errLog(mError);
				return false;
			}
//			-----------fromyzfend
			
//		    自核+复核
			WxAutoCheck ttWxAutoCheck = new WxAutoCheck(cLCContTable.getPrtNo(), mGlobalInput);
			boolean SuccFlag = createSharedMark();
			mError = ttWxAutoCheck.deal();
			if(!SuccFlag){
				mError = "生成共享标识数据失败！";	
			}
			boolean insuredFlag = createInsured();
			if(!insuredFlag){
				mError = "创建被保人失败！";	
			}
			ExemptionRiskBL mExemptionRiskBL = new ExemptionRiskBL();
			String contno = new ExeSQL().getOneValue("select contno from lccont where prtno = '"+cLCContTable.getPrtNo()+"'");
			boolean riskFlag = mExemptionRiskBL.checkContAmnt(contno);
			if(!riskFlag){
				mError = "豁免险的保额应等于被豁免险的保费之和！";
			}
			if(!"".equals(mError)){
				System.out.println("删除新单开始，Prtno为："+cLCContTable.getPrtNo());
				VData tVData = new VData();
				String tDeleteReason = "复杂产品自核失败需要进行新单删除";
				TransferData tTransferData = new TransferData();
				LCContDB tLCContDB = new LCContDB();
				tLCContDB.setPrtNo(cLCContTable.getPrtNo());
				LCContSchema tLCContSchema = new LCContSchema();
				LCContSet tLCContSet = tLCContDB.query();
				tLCContSchema = tLCContSet.get(1);
			    tTransferData.setNameAndValue("DeleteReason",tDeleteReason);
				tVData.add(tLCContSchema);
				tVData.add(tTransferData);
				tVData.add(mGlobalInput);
				ContDeleteUI tContDeleteUI = new ContDeleteUI();
				tContDeleteUI.submitData(tVData,"DELETE");
				System.out.println("删除新单结束");
				errLog(mError);
                return false;
			}
//					组织返回报文
			getWrapParmList(cRiskWrapCode);
			if(!PrepareInfo()){
				errLog(mError);
                return false;
			}
			getXmlResult();
		}catch(Exception ex){
			return false;
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
		NewLCContTable newLCContTable=replace(cLCContTable);
		NewLCAppntTable newLCAppntTable=replace(cLCAppntTable);
		putResult("LCContTable", newLCContTable);
		//投保人节点
		putResult("LCAppntTable", newLCAppntTable);
		//所有被保人节点
		for(int i=0;i<cLCInsuredList.size();i++){
			LCInsuredTable mLCInsuredTable=new LCInsuredTable();
			mLCInsuredTable=(LCInsuredTable)cLCInsuredList.get(i);
			NewLCInsuredTable newLCInsuredTable=replace(mLCInsuredTable);
			putResult("LCInsuredTable", newLCInsuredTable);
		}
		//套餐节点
		putResult("WrapTable", cWrapTable);
		//算费参数节点
		for(int i=0;i<cWrapParamList.size();i++){
			putResult("WrapParamTable", (WrapParamTable)cWrapParamList.get(i));
		}
		//受益人节点
//		for(int i=0;i<cLCBnfList.size();i++){
//			putResult("LCBnfTable", (LCBnfTable)cLCBnfList.get(i));
//		}
	}
	public NewLCContTable replace(LCContTable mLcContTable){
		NewLCContTable newlc=new NewLCContTable();
		newlc.setAgentCode(mLcContTable.getAgentCode());
		newlc.setAgentCom(mLcContTable.getAgentCom());
		newlc.setCInValiDate(mLcContTable.getCInValiDate());
		newlc.setCInValiTime(mLcContTable.getCInValiTime());
		newlc.setContNo(mLcContTable.getContNo());
		newlc.setCValiDate(mLcContTable.getCValiDate());
		newlc.setCValiTime(mLcContTable.getCValiTime());
		newlc.setManageCom(mLcContTable.getManageCom());
		newlc.setPayIntv(mLcContTable.getPayIntv());
		newlc.setPayMode(mLcContTable.getPayMode());
		newlc.setPolApplyDate(mLcContTable.getPolApplyDate());
		newlc.setPrtNo(mLcContTable.getPrtNo());
		newlc.setSaleChnl(mLcContTable.getSaleChnl());
		
		
		
		return newlc;
		
	}
	
	public NewLCAppntTable replace(LCAppntTable mLCAppntTable){
		NewLCAppntTable newlca=new NewLCAppntTable();
		newlca.setContNo(mLCAppntTable.getContNo());
		newlca.setAppntNo(mLCAppntTable.getAppntNo());
		newlca.setAppntName(mLCAppntTable.getAppntName());
		newlca.setAppntSex(mLCAppntTable.getAppntSex());
		newlca.setAppntBirthday(mLCAppntTable.getAppntBirthday());
		newlca.setAppntIDType(mLCAppntTable.getAppntIDType());
		newlca.setAppntIDNo(mLCAppntTable.getAppntIDNo());
		newlca.setAppntMobile(mLCAppntTable.getAppntMobile());
		newlca.setAppntPhone(mLCAppntTable.getAppntPhone());
		newlca.setOccupationCode(mLCAppntTable.getOccupationCode());
		newlca.setOccupationType(mLCAppntTable.getOccupationType());
		newlca.setEmail(mLCAppntTable.getEmail());
		newlca.setMailAddress(mLCAppntTable.getMailAddress());
		newlca.setHomeAddress(mLCAppntTable.getHomeAddress());
		newlca.setMailZipCode(mLCAppntTable.getMailZipCode());
		newlca.setReAppntNo(mLCAppntTable.getReAppntNo());
		return newlca;
	}
	
	public NewLCInsuredTable replace(LCInsuredTable mLCInsuredTable){
		NewLCInsuredTable newlci=new NewLCInsuredTable();
		newlci.setContNo(mLCInsuredTable.getContNo());
		newlci.setInsuredNo(mLCInsuredTable.getInsuredNo());
		newlci.setName(mLCInsuredTable.getName());
		newlci.setSex(mLCInsuredTable.getSex());
		newlci.setBirthday(mLCInsuredTable.getBirthday());
		newlci.setIDType(mLCInsuredTable.getIDType());
		newlci.setIDNo(mLCInsuredTable.getIDNo());
		newlci.setEmail(mLCInsuredTable.getEmail());
		newlci.setOccupationCode(mLCInsuredTable.getOccupationCode());
		newlci.setOccupationType(mLCInsuredTable.getOccupationType());
		newlci.setHomeAddress(mLCInsuredTable.getHomeAddress());
		newlci.setMailAddress(mLCInsuredTable.getMailAddress());
		newlci.setInsuredMobile(mLCInsuredTable.getInsuredMobile());
		newlci.setHomePhone(mLCInsuredTable.getHomePhone());
		newlci.setMailZipCode(mLCInsuredTable.getMailZipCode());
		newlci.setRelaToMain(mLCInsuredTable.getRelaToMain());
		newlci.setRelaToAppnt(mLCInsuredTable.getRelaToAppnt());
		newlci.setReInsuredNo(mLCInsuredTable.getReInsuredNo());
		newlci.setStature(mLCInsuredTable.getStature());
		newlci.setAvoirdupois(mLCInsuredTable.getAvoirdupois());
		return newlci;
	}
	
//	获取保单信息
	public boolean PrepareInfo(){
		String sql = "select contno,amnt,prem,appntno from lccont where prtno = '"+cLCContTable.getPrtNo()+"'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if(tSSRS == null || tSSRS.getMaxRow()<=0 ){
			mError = "获取保单信息失败！";
            return false;
		}else{
			cWrapTable.setAmnt(tSSRS.GetText(1, 2));
			cWrapTable.setPrem(tSSRS.GetText(1, 3));
			cLCContTable.setContNo(tSSRS.GetText(1, 1));
			cLCAppntTable.setReAppntNo(tSSRS.GetText(1, 4));			
		}
		//获取保险期间
		String InsuYearsql = "select insuyear,insuyearflag from lcpol where prtno = '"+cLCContTable.getPrtNo()+"'";
		SSRS tInsuYearSSRS = new ExeSQL().execSQL(InsuYearsql);
		if(tInsuYearSSRS == null || tInsuYearSSRS.getMaxRow()<=0 ){
			mError = "获取保险期间信息失败！";
            return false;
		}else{
			cWrapTable.setInsuYear(tInsuYearSSRS.GetText(1, 1));
			cWrapTable.setInsuYearFlag(tInsuYearSSRS.GetText(1, 2));
		}
		//获取被保人客户号码返回给电子商务
		String InsuredNOSql = "select insuredno,sequenceno from lcinsured where prtno = '"+cLCContTable.getPrtNo()+"'";
		SSRS InsuredNoSSRS = new ExeSQL().execSQL(InsuredNOSql);
		if(InsuredNoSSRS == null || InsuredNoSSRS.getMaxRow()<=0 ){
			mError = "获取被保人信息失败！";
            return false;
		}else{
			List tLCInsuredList = new ArrayList();
			for(int i = 0;i<cLCInsuredList.size();i++){
				LCInsuredTable tLCInsuredTable = (LCInsuredTable)cLCInsuredList.get(i);
				for(int j=1;j<=InsuredNoSSRS.MaxRow;j++){
					if(InsuredNoSSRS.GetText(j, 2).equals(tLCInsuredTable.getInsuredNo())){
						tLCInsuredTable.setReInsuredNo(InsuredNoSSRS.GetText(j, 1));
					}
				}
				tLCInsuredList.add(tLCInsuredTable);
			}
			cLCInsuredList = tLCInsuredList;
		}
//		获取折扣
		String FeeRateSql = "select calfactorvalue from lcriskdutywrap where prtno = '"+cLCContTable.getPrtNo()+"' and calfactor = 'FeeRate'";
    	SSRS FeeRateSSRS = new ExeSQL().execSQL(FeeRateSql);
    	if(FeeRateSSRS!=null && FeeRateSSRS.getMaxRow()>0){
    		cWrapTable.setFeeRate(tSSRS.GetText(1, 1));
    	}
		return true;
	}
	
	/**
     * deleteData
     *
     * @return boolean
     */
    private boolean deleteData()
    {
    	String wherePart_ContNo = " and ContNo = '" + cLCContTable.getContNo() + "'";
    	MMap DeleteMap = new MMap();
    	DeleteMap.put("delete from lccont where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from lcpol where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from lcduty where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from lcprem where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from lcget where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from lcappnt where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from lcinsured where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from LCCustomerImpart where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from LCCustomerImpartParams where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from LCCustomerImpartDetail where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from LCNation where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from LCBnf where 1=1 " + wherePart_ContNo, "DELETE");
    	DeleteMap.put("delete from LCRiskDutyWrap where 1=1 " + wherePart_ContNo, "DELETE");
    	
//    	　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("多次复核时，先删除原有保单信息！");
        VData mInputData = new VData();
        mInputData.add(DeleteMap);
        if (!tPubSubmit.submitData(mInputData, ""))
        {
            // @@错误处理
        	mError="多次核保时删除原有数据失败";
            return false;
        }

        return true;
    }
//    获取可用单证号并将申请单证号状态置为14
    public boolean getCardNo(String aRiskWrapCode){
//    	根据套餐编码获取单证编码
    	String getCertifycodeSql = "select * from LMCardRisk where riskcode = '"+aRiskWrapCode+"'";
    	String Certifycode = new ExeSQL().getOneValue(getCertifycodeSql);
    	if("".equals(Certifycode) || Certifycode == null){
    		mError = "获取可用单证号码失败！";
 		   return false;
    	}
    	//获取可用卡号，卡号即为印刷号
    	String searchCardnoSQL = "select lzcn.cardno,lzcn.cardpassword from LZCardNumber lzcn "
			 +"inner join LZCard lzc on lzcn.CardType = lzc.SubCode "
			                      +"and lzc.StartNo = lzcn.CardSerNo "
			                      +"and lzc.EndNo = lzcn.CardSerNo "
			 +"where  lzc.State in ('10','11') "
			   +"and lzc.certifycode = '"+Certifycode+"' fetch first 1 rows only ";
    	SSRS CardSSRS = new ExeSQL().execSQL(searchCardnoSQL);
    	if(CardSSRS == null || CardSSRS.MaxRow<=0){
    	   mError = "获取可用印刷号码失败！";
 		   return false;
    	}
       	String cardNo = CardSSRS.GetText(1, 1);
	   	if(cardNo.equals("") || cardNo ==null ){
		   mError = "获取可用印刷号码失败！";
		   return false;
		}
	   	String lzcardPKSql = " select  lzc.CertifyCode, lzc.SubCode, lzc.riskcode,lzc.RiskVersion ,lzc.StartNo,lzc.endno,lzc.state "
			+ "from LZCardNumber lzcn inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
			+ "and lzc.EndNo = lzcn.CardSerNo where 1 = 1 	and lzcn.CardNo = '"
			+ cardNo + "' with ur";
	   	ExeSQL tExeSQL = new ExeSQL();
		SSRS lzcardPKSSRS = tExeSQL.execSQL(lzcardPKSql);
		String modifyStateSql = null;
		if (lzcardPKSSRS.MaxRow > 0) {
			String CertifyCode = lzcardPKSSRS.GetText(1, 1);
			String SubCode = lzcardPKSSRS.GetText(1, 2);
			String RiskCode = lzcardPKSSRS.GetText(1, 3);
			String RiskVersion = lzcardPKSSRS.GetText(1, 4);
			String StartNo = lzcardPKSSRS.GetText(1, 5);
			String EndNo = lzcardPKSSRS.GetText(1, 6);
		    modifyStateSql = "update lzcard set state='14' where CertifyCode='"
					+ CertifyCode + "' " + "and SubCode='" + SubCode
					+ "' and RiskCode='" + RiskCode + "' and RiskVersion='"
					+ RiskVersion + "' " + "and StartNo='" + StartNo
					+ "' and EndNo='" + EndNo + "'";
		}
		MMap updateStateMap = new MMap();
		updateStateMap.put(modifyStateSql, "UPDATE");
//   	　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        VData mInputData = new VData();
        mInputData.add(updateStateMap);
        if (!tPubSubmit.submitData(mInputData, ""))
        {
            // @@错误处理
        	mError="获取印刷号后修改状态时失败！";
            return false;
        }
        cLCContTable.setPrtNo(cardNo);
       return true;
    }
    
//    获取每个责任对应的保额及保费信息
    public void getWrapParmList(String pRiskWrapCode){
    	String contnoSql = "select contno from lccont where prtno = '"+cLCContTable.getPrtNo()+"'";
    	String contno = new ExeSQL().getOneValue(contnoSql);
    	String sql = "select a.riskcode,b.dutycode, a.amnt,b.amnt,a.prem,b.prem from lcpol a left join lcduty b on a.polno = b.polno where a.contno ='"+contno+"' ";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS!=null && tSSRS.getMaxRow()>0){
    		cWrapParamList = new ArrayList();
    		for(int i=1;i<=tSSRS.getMaxRow();i++){
    			for(int j=1;j<=2;j++){
    				WrapParamTable tWrapParamTable = new WrapParamTable();
					tWrapParamTable.setContNo(cLCContTable.getContNo());
					tWrapParamTable.setRiskWrapCode(pRiskWrapCode);
					tWrapParamTable.setRiskCode(tSSRS.GetText(i, 1));
					tWrapParamTable.setDutyCode(tSSRS.GetText(i, 2));
					if(j==1){
						tWrapParamTable.setCalfactor("Amnt");
						tWrapParamTable.setCalfactorValue(String.valueOf(tSSRS.GetText(i, 4)));
					}else{
						tWrapParamTable.setCalfactor("Prem");
						tWrapParamTable.setCalfactorValue(String.valueOf(tSSRS.GetText(i, 6)));
					}
					cWrapParamList.add(tWrapParamTable);
    			}
    		}
    	}
    }
    
/**
 * 套餐级别的校验  add by licaiyan 
 * @param appntTable 投保人
 * @param insuTable 被保人
 * @param wrapParamList 算费要素
 * @return
 */
	private boolean wrapCheck(LCAppntTable appntTable,LCInsuredTable insuTable){
		boolean flag = true;
		LMCheckFieldDB tLMCheckFieldDB= new LMCheckFieldDB();
		tLMCheckFieldDB.setRiskCode(this.cRiskWrapCode);
		tLMCheckFieldDB.setFieldName("ETBINSERT");
		LMCheckFieldSet tLMCheckFieldSet = tLMCheckFieldDB.query();
		
		String amntStr = "";
		for(int k=0;k<cWrapParamList.size();k++){//获取保额
			WrapParamTable rt = (WrapParamTable)cWrapParamList.get(k);
			if("Amnt".equals(rt.getCalfactor())) amntStr = rt.getCalfactorValue();
		}
		for(int i=1;i<=tLMCheckFieldSet.size();i++){
			System.out.println("套餐校验----------------------" + i);
			LMCheckFieldSchema tLMCheckFieldSchema = tLMCheckFieldSet.get(i);
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			tLMCalModeDB.setCalCode(tLMCheckFieldSchema.getCalCode());
			LMCalModeSet tLMCalModeSet = new LMCalModeSet();
			tLMCalModeSet = tLMCalModeDB.query();
			for(int j=1;j<=tLMCalModeSet.size();j++){
				LMCalModeSchema tLMCalModeSchema = tLMCalModeSet.get(j);
	
				//目前只支持一个被保人
				//WFInsuListSchema insuListSchema = insuTable;
				int tAppntAge = PubFun.calInterval(insuTable.getBirthday(),
						this.cLCContTable.getCValiDate(), "Y");
				Calculator cal = new Calculator();
				cal.setCalCode(tLMCalModeSchema.getCalCode());
				cal.addBasicFactor("InsuredName", insuTable.getName());
				cal.addBasicFactor("Sex", insuTable.getSex());
				cal.addBasicFactor("InsuredBirthday", insuTable.getBirthday());
				cal.addBasicFactor("IDType", insuTable.getIDType());
				cal.addBasicFactor("IDNo", insuTable.getIDNo());
				cal.addBasicFactor("RiskWrapCode", this.cRiskWrapCode);
				cal.addBasicFactor("AppAge",tAppntAge+"");
				cal.addBasicFactor("OccupationType",insuTable.getOccupationType());
				cal.addBasicFactor("Cvalidate",this.cLCContTable.getCValiDate());
				
				cal.addBasicFactor("Amnt",amntStr);
				
				String result = cal.calculate();
				
				if(!result.equals("0")){
					System.out.println("校验结果为false----------------------");
					flag = false;
					errLog(tLMCheckFieldSchema.getMsg());
				}
			
				
			}
		}
		return flag;
	}
	
    public static void main(String[] args) {
    	
	}
    //创建共享标识数据
    public boolean createSharedMark(){
         String theCurrentDate = PubFun.getCurrentDate();
         String theCurrentTime = PubFun.getCurrentTime();
         MMap mMap = new MMap();
         mMap.put("delete from LCPersonTrace where  ContractNo='"+cLCContTable.getPrtNo()+"' ", "DELETE");
         VData vData = new VData();
         vData.add(mMap);
         PubSubmit pubSubmit = new PubSubmit();
         if (!pubSubmit.submitData(vData, ""))
         {
            mError = "生成共享标识数据失败！";
            return false;
         }
         mMap = new MMap();
         vData.clear();
    	LCAppntDB tlLcAppntDB = new LCAppntDB();
    	tlLcAppntDB.setPrtNo(cLCContTable.getPrtNo());
    	LCAppntSet tLcAppntSet =  tlLcAppntDB.query();
    	LCPersonTraceSchema tLcPersonTraceSchema = new LCPersonTraceSchema();
    	tLcPersonTraceSchema = appntLcPersonTrace;
    	tLcPersonTraceSchema.setCustomerNo(tLcAppntSet.get(1).getAppntNo());
    	tLcPersonTraceSchema.setContractNo(cLCContTable.getPrtNo());
    	 SSRS tSSRS2 = new SSRS();
         String sql1 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
                 +tLcPersonTraceSchema.getCustomerNo()+ "'";
         ExeSQL tExeSQL1 = new ExeSQL();
         tSSRS2 = tExeSQL1.execSQL(sql1);
         Integer firstinteger1 = Integer.valueOf(tSSRS2.GetText(1, 1));
         int tTraceNo = firstinteger1.intValue() + 1;
         Integer sTraceNo = new Integer(tTraceNo);
         String mTraceNo = sTraceNo.toString();
         tLcPersonTraceSchema.setTraceNo(mTraceNo);
         if(tLcPersonTraceSchema.getAuthorization()==null || tLcPersonTraceSchema.getAuthorization().equals("")){
        	 	tLcPersonTraceSchema.setAuthorization("0");
        	 	tLcPersonTraceSchema.setAuthType("1");
        	 	tLcPersonTraceSchema.setBusinessLink("1");
        	 	tLcPersonTraceSchema.setCustomerContact("6");
        	 	tLcPersonTraceSchema.setSharedMark("0");
        	 	tLcPersonTraceSchema.setSpecialLimitMark("0");
           }
         tLcPersonTraceSchema.setCompanySource("2");//人保健康
         tLcPersonTraceSchema.setInstitutionSource(tLcAppntSet.get(1).getManageCom());
         tLcPersonTraceSchema.setAuthVersion("1.0");
         tLcPersonTraceSchema.setSendDate(theCurrentDate);
         tLcPersonTraceSchema.setSendTime(theCurrentTime);
         tLcPersonTraceSchema.setModifyDate(theCurrentDate);
         tLcPersonTraceSchema.setModifyTime(theCurrentTime);
         tLcPersonTraceSchema.setSpare1("");
         mMap.put(tLcPersonTraceSchema, "INSERT");
         
         LCInsuredDB tLcInsuredDB = new LCInsuredDB();
         tLcInsuredDB.setPrtNo(cLCContTable.getPrtNo());
         LCInsuredSet tLcInsuredSet  = tLcInsuredDB.query();
         LCPersonTraceSchema mLcPersonTraceSchema = new LCPersonTraceSchema();
         if(tLcInsuredSet.size()>0){
        	for(int i=1;i<=tLcInsuredSet.size();i++){
        		for(int j=1;j<=insuredLcPersonTraceSet.size();j++){
        			if(tLcInsuredSet.get(i).getName().equals(insuredLcPersonTraceSet.get(j).getSpare1())&&!"00".equals(tLcInsuredSet.get(i).getRelationToAppnt())){
        				mLcPersonTraceSchema = insuredLcPersonTraceSet.get(j);
        				mLcPersonTraceSchema.setCustomerNo(tLcInsuredSet.get(i).getInsuredNo());
        				mLcPersonTraceSchema.setContractNo(cLCContTable.getPrtNo());
        		    	 SSRS tSSRS3 = new SSRS();
        		         String sql2 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
        		                 +mLcPersonTraceSchema.getCustomerNo()+ "'";
        		         ExeSQL tExeSQL2 = new ExeSQL();
        		         tSSRS3 = tExeSQL2.execSQL(sql2);
        		         Integer firstinteger2 = Integer.valueOf(tSSRS3.GetText(1, 1));
        		         int tTraceNo1 = firstinteger2.intValue() + 1;
        		         Integer sTraceNo1 = new Integer(tTraceNo1);
        		         String mTraceNo1 = sTraceNo1.toString();
        		         mLcPersonTraceSchema.setTraceNo(mTraceNo1);
        		         if(mLcPersonTraceSchema.getAuthorization()==null || mLcPersonTraceSchema.getAuthorization().equals("")){
        		        	    mLcPersonTraceSchema.setAuthorization("0");
        		        	    mLcPersonTraceSchema.setAuthType("1");
        		        	 	mLcPersonTraceSchema.setBusinessLink("1");
        		        	 	mLcPersonTraceSchema.setCustomerContact("6");
        		        	 	mLcPersonTraceSchema.setSharedMark("0");
        		        	 	mLcPersonTraceSchema.setSpecialLimitMark("0");
        		           }
        		         mLcPersonTraceSchema.setCompanySource("2");//人保健康
        		         mLcPersonTraceSchema.setInstitutionSource(tLcInsuredSet.get(i).getManageCom());
        		         mLcPersonTraceSchema.setAuthVersion("1.0");
        		         mLcPersonTraceSchema.setSendDate(theCurrentDate);
        		         mLcPersonTraceSchema.setSendTime(theCurrentTime);
        		         mLcPersonTraceSchema.setModifyDate(theCurrentDate);
        		         mLcPersonTraceSchema.setModifyTime(theCurrentTime);
        		         mLcPersonTraceSchema.setSpare1("");
        		         mMap.put(mLcPersonTraceSchema, "INSERT");
        			}
        		}
        		
        	}
         }
         
         vData.add(mMap);
         PubSubmit pubSubmit1 = new PubSubmit();
         if (!pubSubmit1.submitData(vData, ""))
         {
            mError = "生成共享标识数据失败！";
            return false;
         } 
         appntLcPersonTrace = new LCPersonTraceSchema();
         insuredLcPersonTraceSet =  new LCPersonTraceSet();
         mMap=new MMap();
         vData.clear();
    return true;	
    }
    //针对特殊套餐创建被保人
    public boolean createInsured(){
    	 Reflections ref = new Reflections();
    	 String prtno = cLCContTable.getPrtNo();
    	LCInsuredDB lcInsuredDB = new LCInsuredDB();
 		lcInsuredDB.setPrtNo(prtno);
 		LCInsuredSet tLcInsuredSet = lcInsuredDB.query();
 		String realtion = null ;
 		if(tLcInsuredSet!=null ){
 			realtion=  tLcInsuredSet.get(1).getRelationToAppnt();
 		}
 		String relationToMainInsured = null;
 		if(realtion.equals("00")){
 			 relationToMainInsured = "00";
 		}else if(realtion.equals("01")){
 			relationToMainInsured = "02";
 		}else if(realtion.equals("02")){
 			relationToMainInsured = "01";
 		}else if(realtion.equals("04")||realtion.equals("05")){
 			relationToMainInsured = "03";
 		}else if(realtion.equals("03")&&tLcInsuredSet.get(1).getSex().equals("1")){
 			relationToMainInsured = "05";
 		}else if(realtion.equals("03")&&tLcInsuredSet.get(1).getSex().equals("0")){
 			relationToMainInsured = "04";
 		}
    	 LCInsuredSchema tLcInsuredSchema = new LCInsuredSchema();
    	if(cRiskWrapCode.equals("JAP009")){
    		LCAppntDB lcAppntDB = new LCAppntDB();
    		lcAppntDB.setPrtNo(prtno);
    		LCAppntSet tLcAppntSet =lcAppntDB.query();
    		LCAppntSchema tLcAppntSchema = tLcAppntSet.get(1);
    		ref.transFields(tLcInsuredSchema,tLcAppntSchema);
    		System.out.println(tLcInsuredSchema.toString());
    		tLcInsuredSchema.setInsuredNo(tLcAppntSchema.getAppntNo());
    		tLcInsuredSchema.setExecuteCom(tLcAppntSchema.getManageCom());
    		tLcInsuredSchema.setRelationToMainInsured(relationToMainInsured);
    		tLcInsuredSchema.setRelationToAppnt("00");
    		tLcInsuredSchema.setSequenceNo("2");
    		tLcInsuredSchema.setName(tLcAppntSchema.getAppntName());
    		tLcInsuredSchema.setSex(tLcAppntSchema.getAppntSex());
    		tLcInsuredSchema.setBirthday(tLcAppntSchema.getAppntBirthday());
    		tLcInsuredSchema.setIDType(tLcAppntSchema.getIDType());
    		tLcInsuredSchema.setIDNo(tLcAppntSchema.getIDNo());
    		tLcInsuredSchema.setAddressNo(tLcAppntSchema.getAddressNo());
    		VData vData = new VData();
    		MMap mMap = new MMap();
    		mMap.put(tLcInsuredSchema, "INSERT");;
    		vData.add(mMap);
    		  PubSubmit tSubmit = new PubSubmit();

              if (!tSubmit.submitData(vData, "")) { //数据提交
                  // @@错误处理
                 return false;
              }
    	}
    	
    	return true;
    }
}
