package com.cbsws.xml.ctrl.complexpds;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.apache.log4j.Logger;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.core.xml.xsch.BaseXmlSch;
import com.cbsws.obj.EndorsementInfo;
import com.cbsws.xml.ctrl.complexpds.LCGrpContTable;
import com.cbsws.xml.ctrl.complexpds.LCInsuredTable;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BQWSSBInfaceBL; //import com.sinosoft.lis.bq.BQWTQNInfaceBL;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class BQWSSBInfaceTask {

	protected Logger mLogger = Logger.getLogger(getClass().getName());
	protected MsgCollection mResMsgCols = null;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String merr = "";
	// MsgHead信息
	public String mBatchNo = "";
	public String mSendDate = "";
	public String mSendTime = "";
	public String mBranchCode = "";
	public String mSendOperator = "";
	public String mMsgType = "";

	// 团单信息
	public String mGrpContNo = "";
	public String mGrpName = "";

	public LCGrpContTable mLCGrpContTable;
	public LCInsuredTable mLCInsuredTable;
	public MsgResHead tResMsgHead;
	//public String mliuShuiHao = "";

	private LCGrpContSchema mLCGrpContSchema;
	private EndorsementInfo mEndorsementInfo;
	private List insuredList;
	private List tLCInsuredList = new ArrayList();
	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();
	private String mCDate;
	private Document tOutXmlDoc = null;

	// 返回的被保人信息SQL
	private String mInsuredInfoSQL = "";

	// 被保人信息SSRS
	private SSRS tInsuredInfoSSRS = null;

	// 被保人数
	private int InsuredInfolen = 0;
	
	/** 应用路径 */
	private String mURL = "";

	private ExeSQL mExeSQL = new ExeSQL();

	public BQWSSBInfaceTask() {
	}

	public boolean deal() throws IOException {

		// 从FTP下载报文XML

		// 服务器存放路径
		ExeSQL tExeSql = new ExeSQL();
		
		// 设置文件路径s
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
		//mURL = "F:\\";
		

		mCDate = mCurrDate.replaceAll("-", ""); // 返回给社保的报文，去除-，例如2015-12-17返回20151217
		String tFilePathLocal = mURL + tExeSql.getOneValue("select codename from ldcode where codetype='BqWSClaim' and code='XmlPath'");//下载报文
		String tFilePathLocalCore = tFilePathLocal + "/";  //下载报文
		String tFilePathUploadCore = tFilePathLocal + "/"; //上传备份到FTP
		String tUploadPathLocalCore = mURL + tExeSql.getOneValue("select codename from ldcode where codetype='BqWSUpload' and code='XmlPath'");// 上传
		//String tUploadPathLocalCore = "E:/SBEDORUPLOAD\\"; // 从本地上传

		String tFileImportPath = "/sbftp/SBEdor"; // 从FTP上获取报文
		String tFileImportBackPath = "/sbftp/SBEdorBack"; // 核心返回FTP报文
		String tFileImportPathBackUp = "/sbftp/SBEdorBackup/" +PubFun.getCurrentDate()+"/";//存放核心保全相关备份文件，每天归档

		// 若目录不存在，则创建新目录
		File mFileDir = new File(tFilePathLocalCore);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + tFilePathLocalCore.toString()
						+ "]失败！" + mFileDir.getPath());
			}
		}
		File mUpLoadFileDir = new File(tUploadPathLocalCore);
		if (!mUpLoadFileDir.exists()) {
			if (!mUpLoadFileDir.mkdirs()) {
				System.err.println("创建目录[" + tUploadPathLocalCore.toString()
						+ "]失败！" + mUpLoadFileDir.getPath());
			}
		}
		tUploadPathLocalCore=tUploadPathLocalCore+"/";

		String getIPPort = "select codename ,codealias from ldcode where codetype='SocialClaim' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='SocialClaim' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法下载xml");
			return false;
		}

		String tIP = tIPSSRS.GetText(1, 1);
		String tPort = tIPSSRS.GetText(1, 2);
		String tUserName = tUPSSRS.GetText(1, 1);
		String tPassword = tUPSSRS.GetText(1, 2);

		FTPTool tFTPTool = new FTPTool(tIP, tUserName, tPassword, Integer
				.parseInt(tPort));

		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}

		String[] tPath = tFTPTool.downloadAllDirectory(tFileImportPath,
				tFilePathLocalCore);

		// 获取并解析报文
		System.out.println("开始获取报文");
		for (int j = 0; j < tPath.length; j++) {
			FileInputStream fis = null;
			Document tInXmlDoc;
			System.out.println("++++++++" + tFilePathLocalCore + tPath[j]);
			fis = new FileInputStream(tFilePathLocalCore + tPath[j]);
			tInXmlDoc = JdomUtil.build(fis, "GBK");
			fis.close();

			System.out.println("开始解析报文");

			Element tRootData = tInXmlDoc.getRootElement();

			// 获取MsgHead部分内容
			System.out.println("MsgHead部分");
			Element tMsgHead = tRootData.getChild("MsgHead");
			Element tItem = tMsgHead.getChild("Item");

			mBatchNo = tItem.getChildTextTrim("BatchNo");
			mSendDate = tItem.getChildTextTrim("SendDate");
			mSendTime = tItem.getChildTextTrim("SendTime");
			mBranchCode = tItem.getChildTextTrim("BranchCode");
			mSendOperator = tItem.getChildTextTrim("SendOperator");
			mMsgType = tItem.getChildTextTrim("MsgType");

			System.out.println("团单部分");

			// 获取团单信息
			// 接口一次只接受一个团单的无名单实名化
			Element tLCGrpCont = tRootData.getChild("LCGrpContTable");
			if (tLCGrpCont == null) {
				merr = "申请报文中获取团单信息失败。";
				System.out.println("申请报文中获取团单信息失败。");

				try {
					tOutXmlDoc = getWrapParmList(null, "fail");
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("============无名单实名化失败，打印传出报文============");
				JdomUtil.print(tOutXmlDoc);
				FileOutputStream fos = null;
				fos = new FileOutputStream(tUploadPathLocalCore + tPath[j]);
				JdomUtil.output(tOutXmlDoc, fos);
				fos.close();

				System.out.println("tFileImportBackPath=" + tFileImportBackPath);
				System.out.println("tUploadPathLocalCore + tPath[j]="
						+ tUploadPathLocalCore + tPath[j]);

				if (!tFTPTool.upload(tFileImportBackPath, tUploadPathLocalCore
						+ tPath[j])) {
					System.out.println("查询团单信息，上载文件失败!");
					continue;
				}

				continue;
			}
			Element tItem2 = tLCGrpCont.getChild("Item");
			mGrpContNo = tItem2.getChildTextTrim("GrpContNo");
			mGrpName = tItem2.getChildTextTrim("GrpName");
			LCGrpContTable tLCGrpContTable = new LCGrpContTable();
			tLCGrpContTable.setGrpContNo(mGrpContNo);
			tLCGrpContTable.setGrpName(mGrpName);

			// 获取被保人信息
			Element tLCInsured = tRootData.getChild("LCInsuredTable");
			if (tLCInsured == null) {
				merr = "申请报文中获取被保人信息失败。";
				System.out.println("申请报文中获取被保人信息失败。");

				try {
					tOutXmlDoc = getWrapParmList(null, "fail");
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("============无名单实名化失败，打印传出报文============");
				JdomUtil.print(tOutXmlDoc);
				FileOutputStream fos = null;
				fos = new FileOutputStream(tUploadPathLocalCore + tPath[j]);
				JdomUtil.output(tOutXmlDoc, fos);
				fos.close();

				System.out
						.println("tFileImportBackPath=" + tFileImportBackPath);
				System.out.println("tUploadPathLocalCore + tPath[j]="
						+ tUploadPathLocalCore + tPath[j]);

				if (!tFTPTool.upload(tFileImportBackPath, tUploadPathLocalCore
						+ tPath[j])) {
					System.out.println("查询团单信息，上载文件失败!");
					continue;
				}

				continue;
			}

			insuredList = tLCInsured.getChildren("Item");

			for (int i = 0; i < insuredList.size(); i++) {
				LCInsuredTable tLCInsuredTable = new LCInsuredTable();
				Element insuredItem = (Element) insuredList.get(i);
				tLCInsuredTable.setName(insuredItem.getChildTextTrim("Name"));
				tLCInsuredTable.setSex(insuredItem.getChildTextTrim("Sex"));
				tLCInsuredTable.setBirthday(insuredItem
						.getChildTextTrim("Birthday"));
				tLCInsuredTable.setIDType(insuredItem
						.getChildTextTrim("IDType"));
				tLCInsuredTable.setIDNo(insuredItem.getChildTextTrim("IDNo"));
				tLCInsuredTable.setOccupationType(insuredItem
						.getChildTextTrim("OccupationType"));
				tLCInsuredTable.setRelaToAppnt(insuredItem
						.getChildTextTrim("RelaToAppnt"));
				tLCInsuredTable.setContPlanCode(insuredItem
						.getChildTextTrim("ContPlanCode"));
				tLCInsuredList.add(tLCInsuredTable);
			}

			// 查询团单信息
			LCGrpContDB tLCGrpContDB = new LCGrpContDB();
			tLCGrpContDB.setGrpContNo(mGrpContNo);
			if (!tLCGrpContDB.getInfo()) {
				merr = "传入的团单号错误，核心系统不存在团单“" + mGrpContNo + "”";
				System.out.println("传入的团单号错误，核心系统不存在团单“" + mGrpContNo + "”");

				try {
					tOutXmlDoc = getWrapParmList(null, "fail");
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("无名单实名化失败，打印传出报文============");
				JdomUtil.print(tOutXmlDoc);
				FileOutputStream fos = null;
				fos = new FileOutputStream(tUploadPathLocalCore + tPath[j]);
				JdomUtil.output(tOutXmlDoc, fos);
				fos.close();

				System.out.println("tFileImportBackPath=" + tFileImportBackPath);
				System.out.println("tUploadPathLocalCore + tPath[j]=" + tUploadPathLocalCore + tPath[j]);

				if (!tFTPTool.upload(tFileImportBackPath, tUploadPathLocalCore
						+ tPath[j])) {
					System.out.println("查询团单信息，上载文件失败!");
					continue;
				}

				continue;
			}
			mLCGrpContSchema = tLCGrpContDB.getSchema();

			// 增加并发控制，同一个团单短时间内只能请求一次
//			MMap tCekMap = null;
//			tCekMap = lockLGWORK(mLCGrpContSchema);
//			if (tCekMap == null) {
//				merr = "团单" + mGrpContNo + "正在进行无名单实名化，请不要重复请求";
//				System.out.println("团单" + mGrpContNo + "正在进行无名单实名化，请不要重复请求");
//				continue;
//			}
//			if (!submit(tCekMap)) {
//				merr = "团单" + mGrpContNo + "正在进行无名单实名化，请不要重复请求";
//				System.out.println("团单" + mGrpContNo + "正在进行无名单实名化，请不要重复请求");
//				continue;
//			}

			// 先进行下必要的数据校验
			if (!checkData()) {
				System.out.println("数据校验失败---");
				// 组织返回报文

				try {
					tOutXmlDoc = getWrapParmList(null, "fail");
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("打印传出报文============");
				JdomUtil.print(tOutXmlDoc);
				FileOutputStream fos = null;
				fos = new FileOutputStream(tUploadPathLocalCore + tPath[j]);
				JdomUtil.output(tOutXmlDoc, fos);
				fos.close();

				System.out
						.println("tFileImportBackPath=" + tFileImportBackPath);
				System.out.println("tUploadPathLocalCore + tPath[j]="
						+ tUploadPathLocalCore + tPath[j]);

				if (!tFTPTool.upload(tFileImportBackPath, tUploadPathLocalCore
						+ tPath[j])) {
					System.out.println("数据校验失败，上载文件失败!");
					continue;
				}

			} else {

				mGlobalInput.Operator = mSendOperator;
				VData data = new VData();
				data.add(mGlobalInput);
				data.add(tLCInsuredList);
				data.add(mLCGrpContSchema);

				BQWSSBInfaceBL tBQWSSBInfaceBL = new BQWSSBInfaceBL();
				if (!tBQWSSBInfaceBL.submit(data)) {
					System.out.println("保全操作错误");
					merr = "" + tBQWSSBInfaceBL.mErrors.getFirstError();
					// 组织返回报文
					try {
						tOutXmlDoc = getWrapParmList(null, "fail");
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					// 生成报文返回
					LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
					tLPGrpEdorItemSchema = tBQWSSBInfaceBL.getEdorItem();
					// 组织返回报文
					try {
						tOutXmlDoc = getWrapParmList(tLPGrpEdorItemSchema,
								"success");
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				System.out.println("打印传出报文============");
				JdomUtil.print(tOutXmlDoc);
				FileOutputStream fos = null;
				fos = new FileOutputStream(tUploadPathLocalCore + tPath[j]);
				JdomUtil.output(tOutXmlDoc, fos);
				fos.close();

				System.out.println("tFileImportBackPath=" + tFileImportBackPath);
				System.out.println("tUploadPathLocalCore + tPath[j]="
						+ tUploadPathLocalCore + tPath[j]);

				if (!tFTPTool.upload(tFileImportBackPath, tUploadPathLocalCore
						+ tPath[j])) {
					System.out.println("上载返回报文失败!");
					continue;
				}
				
				//核心保全业务备份 
				if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
	            	System.out.println("新建目录已存在");
	            }
				System.out.println("tFilePathUploadCore + tPath[j]=" + tFilePathUploadCore + tPath[j]);
				if(!tFTPTool.upload(tFileImportPathBackUp, tFilePathUploadCore+tPath[j]))
				{
					System.out.println("上载核心备份文件失败!");
				}
				System.out.println("tFileImportPath=" + tFileImportPath);
	            if(!tFTPTool.changeWorkingDirectory(tFileImportPath))
	            {
	            	System.out.println("转换工作目录失败!");
	            }
	            if(!tFTPTool.deleteFile(tPath[j]))
	            {
	            	System.out.println("删除文件失败!");
	            }
				
			}
			tLCInsuredList = new ArrayList();
		}
		tFTPTool.logoutFTP();

		return true;

	}

	private Document getWrapParmList(LPGrpEdorItemSchema edoritem, String ztFlag)
			throws Exception {
		// 判断失败还是成功
		if ("success".equals(ztFlag) && edoritem != null) {
			mEndorsementInfo = new EndorsementInfo();
			mEndorsementInfo.setEdorNo(edoritem.getEdorNo());
			mEndorsementInfo.setEdorValidate(edoritem.getEdorValiDate());
			mEndorsementInfo.setEdorConfdate(PubFun.getCurrentDate());

			return createResultXML();

		} else if ("fail".equals(ztFlag)) {
			mEndorsementInfo = new EndorsementInfo();
			return createFalseXML();
		}
		return null;
	}

	/**
	 * 生成正常返回报文
	 * 
	 * @return Document
	 */
	private Document createResultXML() throws Exception {
		// Request部分
		Element tRootData = new Element("DataSet");

		// Head部分
		Element tMsgResHead = new Element("MsgResHead");
		Element tItem = new Element("Item");

		Element tBatchNo = new Element("BatchNo");
		tBatchNo.addContent(mBatchNo);
		tItem.addContent(tBatchNo);

		Element tSendDate = new Element("SendDate");
		tSendDate.addContent(mCurrDate);
		tItem.addContent(tSendDate);

		Element tSendTime = new Element("SendTime");
		tSendTime.addContent(mCurrTime);
		tItem.addContent(tSendTime);

		Element tBranchCode = new Element("BranchCode");
		tBranchCode.addContent(mBranchCode);
		tItem.addContent(tBranchCode);

		Element tSendOperator = new Element("SendOperator");
		tSendOperator.addContent(mSendOperator);
		tItem.addContent(tSendOperator);

		Element tMsgType = new Element("MsgType");
		tMsgType.addContent(mMsgType);
		tItem.addContent(tMsgType);

		Element tState = new Element("State");
		tState.addContent("00");
		tItem.addContent(tState);
		
		Element tErrCode = new Element("ErrCode");
		tErrCode.addContent("");
		tItem.addContent(tErrCode);

		Element tErrInfo = new Element("ErrInfo");
		tErrInfo.addContent("");
		tItem.addContent(tErrInfo);

		tMsgResHead.addContent(tItem);

		// body部分
		Element tEndorsement = new Element("EndorsementInfo");
		Element tItem2 = new Element("Item");

		Element tGrpContNo = new Element("GrpContNo");
		tGrpContNo.addContent(mGrpContNo);
		tItem2.addContent(tGrpContNo);		
		
		Element tEdorNo = new Element("EdorNo");
		tEdorNo.addContent(mEndorsementInfo.getEdorNo());
		tItem2.addContent(tEdorNo);

		Element tEdorValidate = new Element("EdorValidate");
		tEdorValidate.addContent(mCDate);
		tItem2.addContent(tEdorValidate);

		Element tEdorConfdate = new Element("EdorConfdate");
		tEdorConfdate.addContent(mCDate);
		tItem2.addContent(tEdorConfdate);

		tEndorsement.addContent(tItem2);

		// 被保人部分
		Element tInsured = new Element("LCInsuredTable");

		// 被保人信息查询SQL
		mInsuredInfoSQL = " select"
			            + " InsuredNo as CustomerNo,"
			            + " InsuredName as Name,"
			            + " Sex as Sex,"
			            + " Birthday as Birthday,"
			            + " IDtype as IDType,"
			            + " IDNo as IDNo"
			            + " from LPDiskImport"
			            + " where Grpcontno = '" + mGrpContNo + "'"
			            + " and EdorNo = '" + mEndorsementInfo.getEdorNo() + "'"
			            + " and State = '2'"
			            + " with ur";
		tInsuredInfoSSRS = mExeSQL.execSQL(mInsuredInfoSQL);
		InsuredInfolen = tInsuredInfoSSRS.getMaxRow();
		for (int j = 1; j <= InsuredInfolen; j++) {
			Element tItem3 = new Element("Item");
			tItem3.addContent(new Element("CustomerNo").setText(tInsuredInfoSSRS
					.GetText(j, 1)));
			tItem3.addContent(new Element("Name").setText(tInsuredInfoSSRS
					.GetText(j, 2)));
			tItem3.addContent(new Element("Sex").setText(tInsuredInfoSSRS
					.GetText(j, 3)));
			tItem3.addContent(new Element("Birthday").setText(tInsuredInfoSSRS
					.GetText(j, 4)));
			tItem3.addContent(new Element("IDType").setText(tInsuredInfoSSRS
					.GetText(j, 5)));
			tItem3.addContent(new Element("IDNo").setText(tInsuredInfoSSRS
					.GetText(j, 6)));
			tInsured.addContent(tItem3);
		}
		
		tRootData.addContent(tMsgResHead);
		tRootData.addContent(tEndorsement);
		tRootData.addContent(tInsured);
		Document tDocument = new Document(tRootData);

		return tDocument;
	}

	/**
	 * 生成错误信息报文
	 * 
	 * @return Document
	 */
	private Document createFalseXML() {
		// Request部分
		Element tRootData = new Element("DataSet");

		// Head部分
		Element tMsgResHead = new Element("MsgResHead");
		Element tItem = new Element("Item");

		Element tBatchNo = new Element("BatchNo");
		tBatchNo.addContent(mBatchNo);
		tItem.addContent(tBatchNo);

		Element tSendDate = new Element("SendDate");
		tSendDate.addContent(mCurrDate);
		tItem.addContent(tSendDate);

		Element tSendTime = new Element("SendTime");
		tSendTime.addContent(mCurrTime);
		tItem.addContent(tSendTime);

		Element tBranchCode = new Element("BranchCode");
		tBranchCode.addContent(mBranchCode);
		tItem.addContent(tBranchCode);

		Element tSendOperator = new Element("SendOperator");
		tSendOperator.addContent(mSendOperator);
		tItem.addContent(tSendOperator);

		Element tMsgType = new Element("MsgType");
		tMsgType.addContent(mMsgType);
		tItem.addContent(tMsgType);

		Element tState = new Element("State");
		tState.addContent("01");
		tItem.addContent(tState);

		Element tErrCode = new Element("ErrCode");
		tErrCode.addContent("99999999");
		tItem.addContent(tErrCode);

		Element tErrInfo = new Element("ErrInfo");
		tErrInfo.addContent(merr);
		tItem.addContent(tErrInfo);

		tMsgResHead.addContent(tItem);

		// body部分
		Element tEndorsement = new Element("EndorsementInfo");
		Element tInsured = new Element("LCInsuredTable");
		
		tRootData.addContent(tMsgResHead);
		tRootData.addContent(tEndorsement);
		tRootData.addContent(tInsured);
		Document tDocument = new Document(tRootData);

		return tDocument;
	}

	private boolean checkData() {
		/** 对无名单实名化申请信息的校验 */
		if (null == mGrpContNo || "".equals(mGrpContNo)) {
			merr = "团单号不能为空";
			return false;
		}
		String str = "select 1 from LCCont  where appflag='1' and polType = '1' "
				+ " and conttype='2' and grpcontno='" + mGrpContNo + "' ";
		String res = new ExeSQL().getOneValue(str);
		if (null == res || "".equals(str)) {
			merr = "非无名单保单不能申请无名单实名化";
			return false;
		}
		if (null == mGrpName || "".equals(mGrpName)) {
			merr = "团单组织名称不能为空";
			return false;
		}

		for (int i = 0; i < tLCInsuredList.size(); i++) {
			mLCInsuredTable = (LCInsuredTable) tLCInsuredList.get(i);
			if (null == mLCInsuredTable.getName()
					|| "".equals(mLCInsuredTable.getName())) {
				merr = "被保人姓名 不能为空";
				return false;
			}
			if (null == mLCInsuredTable.getSex()
					|| "".equals(mLCInsuredTable.getSex())) {
				merr = "被保人性别 不能为空";
				return false;
			}
			if (null == mLCInsuredTable.getBirthday()
					|| "".equals(mLCInsuredTable.getBirthday())) {
				merr = "被保人出生日期不能为空";
				return false;
			}
			if (null == mLCInsuredTable.getIDType()
					|| "".equals(mLCInsuredTable.getIDType())) {
				merr = "被保人证件类型不能为空";
				return false;
			}
			if (null == mLCInsuredTable.getIDNo()
					|| "".equals(mLCInsuredTable.getIDNo())) {
				merr = "被保人证件号码不能为空";
				return false;
			}
			if (null == mLCInsuredTable.getOccupationType()
					|| "".equals(mLCInsuredTable.getOccupationType())) {
				merr = "被保人职业类别不能为空";
				return false;
			}
			if (null == mLCInsuredTable.getRelaToAppnt()
					|| "".equals(mLCInsuredTable.getRelaToAppnt())) {
				merr = "被保人与投保人关系不能为空";
				return false;
			}
			if (null == mLCInsuredTable.getContPlanCode()
					|| "".equals(mLCInsuredTable.getContPlanCode())) {
				merr = "被保人保障计划不能为空";
				return false;
			}
		}

		return true;
	}

	/**
	 * 锁定动作
	 * 
	 * @param cLCContSchema
	 * @return
	 */
	private MMap lockLGWORK(LCGrpContSchema cLCGrpContSchema) {
		MMap tMMap = null;
		/** 无名单实名化标志"WS" */
		String tLockNoType = "WS";
		/** 锁定时间 */
		String tAIS = "6";// 1小时的锁，单位秒
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LockNoKey", cLCGrpContSchema
				.getGrpContNo());
		tTransferData.setNameAndValue("LockNoType", tLockNoType);
		tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

		VData tVData = new VData();
		tVData.add(mGlobalInput);
		tVData.add(tTransferData);

		LockTableActionBL tLockTableActionBL = new LockTableActionBL();
		tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
		if (tMMap == null) {
			return null;
		}
		return tMMap;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			merr = "提交数据库发生错误" + tPubSubmit.mErrors;
			return false;
		}
		return true;
	}

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		BQWSSBInfaceTask mCrmInfaceTask = new BQWSSBInfaceTask();
		mCrmInfaceTask.deal();
	}

}
