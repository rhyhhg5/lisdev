package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.FHReportQueryInfo;
import com.cbsws.obj.LPCashPrintTable;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDBonusSumDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LOBonusPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDBonusSumSchema;
import com.sinosoft.lis.schema.LOBonusPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LDBonusSumSet;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


public class BQCashPrintBL extends ABusLogic{
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	private FHReportQueryInfo mFHReportQueryInfo;
	private String mManageCom;
    private String PolYear;
    private String Contno;
    
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
    private LOBonusPolSchema mLOBonusPolSchema= new LOBonusPolSchema();
    private LPCashPrintTable mLPCashPrintTable = new LPCashPrintTable();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private List<Object> tLOPRTManagerlist = new ArrayList<Object>();
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	private ExeSQL mExeSQL=new ExeSQL();
	private List mLPCashPrintList;

	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkdata()){
        	return false;
        } 
        if(!dealDate()){
        	return false;
        }
		return true;
	}

	
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

	    List tFHReportQueryInfo = cMsgInfos.getBodyByFlag("FHReportQueryInfo");
	      if (tFHReportQueryInfo == null || tFHReportQueryInfo.size() != 1)
	        {
	            errLog("申请报文中获取信息失败。");
	            return false;
	        }
	      mFHReportQueryInfo = (FHReportQueryInfo)tFHReportQueryInfo.get(0);
//        List tLGWorkList = cMsgInfos.getBodyByFlag("LGWorkTable");
	      if(mFHReportQueryInfo.getContNo() == null || "".equals(mFHReportQueryInfo.getContNo())){
	    	  errLog("申请报文中获取保单信息失败。");
	    	  return false;
	      }
	      if(mFHReportQueryInfo.getPolYear() == null || "".equals(mFHReportQueryInfo.getPolYear())){
	    	  errLog("申请报文中获取红利领取年度信息失败。");
	    	  return false;
	      }
	      
        Contno = mFHReportQueryInfo.getContNo();
        PolYear = mFHReportQueryInfo.getPolYear();
        
      //操作员信息
        mGlobalInput.Operator = Operator;
        mGlobalInput.ManageCom = mManageCom;
//        if(tLGWorkList == null ||tLGWorkList.size()!=1)
//        {
//            errLog("申请报文中获取工单信息失败。");
//            return false;
//        }
//        mLGWorkTable=(LGWorkTable)tLGWorkList.get(0);
        
        return true;
	}
	
	
	
	private boolean checkdata(){
   
	   if(null == mFHReportQueryInfo.getContNo() || mFHReportQueryInfo.getContNo().equals("")){
	      errLog("保单号不能为空。");
	      return false;
	   }
	      
//	   if(null == mEdorGRPLQChangeInfo.getContNo() || mEdorGRPLQChangeInfo.getContNo().equals("")){
//		      errLog("团体分单号不能为空。");
//		      return false;
//		   }
	   if(null == mFHReportQueryInfo.getPolYear() || mFHReportQueryInfo.getPolYear().equals("")){
		      errLog("红利领取年度不能为空。");
		      return false;
		   }
//	   if(mEdorGRPLQChangeInfo.getPaymode().equals("4")){
//	   if(null == mEdorGRPLQChangeInfo.getBankCode() || mEdorGRPLQChangeInfo.getBankCode().equals("")){
//		      errLog("银行编码不能为空。");
//		      return false;
//		   }
//	   if(null == mEdorGRPLQChangeInfo.getAccNo() || mEdorGRPLQChangeInfo.getAccNo().equals("")){
//		      errLog("银行账户不能为空。");
//		      return false;
//		   }
//	   if(null == mEdorGRPLQChangeInfo.getAccName() || mEdorGRPLQChangeInfo.getAccName().equals("")){
//		      errLog("银行账户名不能为空。");
//		      return false;
//		   }
//
//	   }
//	   if(null == mEdorGRPLQChangeInfo.getGetMoney() || mEdorGRPLQChangeInfo.getGetMoney().equals("")){
//		      errLog("领取金额不能为空。");
//		      return false;
//		   }
//	   if(null == mEdorGRPLQChangeInfo.getEdorCValiDate() || mEdorGRPLQChangeInfo.getEdorCValiDate().equals("")){
//		      errLog("保全生效日期不能为空。");
//		      return false;
//		   }

		return true;
	}
	
	
	private  boolean dealDate(){
		String	strSQL = "select a.ContNo, "
		             + "(select managecom from lccont where contno = a.contno union select managecom from lbcont where contno = a.contno fetch first 1 row only), "
		             + "a.sgetdate, "
		             + "(case a.bonusflag when '1' then '现金' else '累积生息' end), "
		             + "a.fiscalyear, "
		             + "a.bonusmoney,a.bonusflag,a.polno "
		             + " from LOBonusPol a "
					 + " where 1=1 "
		             + "and contno = '" + Contno
		             + "' and a.fiscalyear ='" + PolYear + "' with ur "; 
					SSRS tSSRS=new SSRS();
					ExeSQL tExeSQL = new ExeSQL();
					tSSRS=tExeSQL.execSQL(strSQL);
					
//         LCGrpContDB tLCGrpContDB = new LCGrpContDB();
//		 tLCGrpContDB.setGrpContNo(mFHReportQueryInfo.getContNo());
//		 if(!tLCGrpContDB.getInfo())
//		  {
//		     errLog("没有查询到保单信息"+mFHReportQueryInfo.getContNo()+"。");
//		     return false;
//		   }
			for(int i=1;i<=tSSRS.MaxRow;i++){
				mLOPRTManagerSchema.setStandbyFlag1(tSSRS.GetText(i,5));
				mLOPRTManagerSchema.setStandbyFlag2(tSSRS.GetText(i,8));
				tLOPRTManagerlist.add(mLOPRTManagerSchema);
			}
//			tLOPRTManagerSet.add(mLOPRTManagerSchema);
			
			if (tLOPRTManagerlist.size() <= 0) {
				errLog("没有获取到打印信息！");
	            return false;
	        }
	        if(mGlobalInput.Operator==null ||mGlobalInput.Operator.equals("")){
	        	errLog("没有获取到操作员的信息！");
	            return false;
	        }
	        
	        for(int i = 0 ; i < tLOPRTManagerlist.size(); i++){
		        LOPRTManagerSchema sLOPRTManagerSchema = (LOPRTManagerSchema) tLOPRTManagerlist.get(i);
		        String tPolNo = sLOPRTManagerSchema.getStandbyFlag2();
		        String tFiscalYear = sLOPRTManagerSchema.getStandbyFlag1();
		        LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB();
		        tLOBonusPolDB.setPolNo(tPolNo);
		        tLOBonusPolDB.setFiscalYear(tFiscalYear);
		        if (!tLOBonusPolDB.getInfo()) {
		            // @@错误处理
					System.out.println("BonusCashPrintBL+getInputData++--");
					CError tError = new CError();
					tError.moduleName = "BonusCashPrintBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "数据为空--打印入口没有得到足够的信息！";
					errLog("数据为空--打印入口没有得到足够的信息！");
					mErrors.addOneError(tError);
					return false;
		        }
		        mLOBonusPolSchema.setSchema(tLOBonusPolDB.getSchema());
		        if(mLOBonusPolSchema==null){
	//	        	 @@错误处理
					System.out.println("BonusCashPrintBL+getInputData++--");
					CError tError = new CError();
					tError.moduleName = "BonusCashPrintBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "数据表为空--VTS打印入口没有得到足够的信息！";
					errLog("数据表为空--VTS打印入口没有得到足够的信息！");
					mErrors.addOneError(tError);
					return false;
		        }
		        
		        if(!getPrintData()){
		        	return false;
		        }
		        
		        
	//	        //得到工单信息
	//	        String workNo = CommonBL.createWorkNo();
	//	        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	//	        tLGWorkSchema.setCustomerNo(mCustomeNO);
	//	        tLGWorkSchema.setTypeNo(mLGWorkTable.getTypeNo());
	//	        tLGWorkSchema.setWorkNo(workNo);
	//	        tLGWorkSchema.setContNo(mFHReportQueryInfo.getContNo());
	//	        tLGWorkSchema.setApplyTypeNo(mLGWorkTable.getApplyTypeNo());
	//	        tLGWorkSchema.setApplyName(mLGWorkTable.getApplyName());
	//	        tLGWorkSchema.setAcceptWayNo(mLGWorkTable.getAcceptWayNo());
	//	        tLGWorkSchema.setAcceptDate(mLGWorkTable.getAcceptDate());
	//	        tLGWorkSchema.setRemark("一卡通线上团体部分领取接口生成");
	//	        
	//	        LPGrpEdorItemSchema tLPEdorItemSchema = new LPGrpEdorItemSchema();
	//	        tLPEdorItemSchema.setEdorNo(workNo);
	//	        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_LQ);
	//	        tLPEdorItemSchema.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
	//
	//	  
	//	        VData data = new VData();
	//	        data.add(mGlobalInput);
	//	        data.add(tLPEdorItemSchema);
	//	        data.add(mEdorGRPLQChangeInfo);
	//	        data.add(tLGWorkSchema);
	//   
	//	        BQGrpLQEdorBL tBQGrpLQEdorBL = new BQGrpLQEdorBL();
	//	        if(!tBQGrpLQEdorBL.submitData(data))
	//	        {
	//	            errLog("保全变更失败"+tBQGrpLQEdorBL.mErrors.getFirstError());
	//	            return false;
	//	        }
	//	        
	//	        LPEdorMainTable tLPEdorMainTable = new LPEdorMainTable();
	//	        tLPEdorMainTable.setEdorAcceptNo(workNo);
	//	        tLPEdorMainTable.setGrpContNo(mEdorGRPLQChangeInfo.getGrpContNo());
	//	       	 返回报文
	//	    	组织返回报文
		        getWrapParmList(mLPCashPrintTable);
	        }
	    	getXmlResult();
		
		return true;
	}
	
	 private void getWrapParmList(LPCashPrintTable tLPCashPrintTable){
	    	mLPCashPrintList = new ArrayList();
	    
	    	mLPCashPrintList.add(tLPCashPrintTable);
	    }
	    
		//返回报文
		public void getXmlResult(){
			//返回退费信息
			for(int i=0;i<mLPCashPrintList.size();i++){
				putResult("LPCashPrint", (LPCashPrintTable)mLPCashPrintList.get(i));
			}

		}
		
		private boolean getPrintData() {
	        if(!getSchema()){
	        	return false;
	        }
	        
	        LDBonusSumDB tLDBonusSumDB=new LDBonusSumDB();
	        LDBonusSumSet tLDBonusSumSet=new LDBonusSumSet();
	        LDBonusSumSchema tLDBonusSumSchema=new LDBonusSumSchema();
	        tLDBonusSumSet=tLDBonusSumDB.executeQuery("select * from LDBonusSum where riskcode='"+mLCPolSchema.getRiskCode()+"' and BonusYear='"+mLOBonusPolSchema.getFiscalYear()+"'");
	        if(tLDBonusSumSet.size()==0){
	        	// @@错误处理
				System.out.println("BonusCashPrintBL+getPrintData++--");
				CError tError = new CError();
				tError.moduleName = "BonusCashPrintBL";
				tError.functionName = "getPrintData";
				tError.errorMessage = "获取分红信息失败!";
				errLog("获取分红信息失败!");
				mErrors.addOneError(tError);
				
				return false;
	        }
	        tLDBonusSumSchema=tLDBonusSumSet.get(1);
	        
	        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
	        String sqlusercom = "select comcode from lduser where usercode='" +
	                            mGlobalInput.Operator + "' with ur";
	        String comcode = new ExeSQL().getOneValue(sqlusercom);
	        if (comcode.equals("86") || comcode.equals("8600") ||
	            comcode.equals("86000000")) {
	            comcode = "86";
	        } else if (comcode.length() >= 4) {
	            comcode = comcode.substring(0, 4);
	        } else {
	            // @@错误处理
				System.out.println("BonusCashPrintBL+getPrintData++--");
				CError tError = new CError();
				tError.moduleName = "BonusCashPrintBL";
				tError.functionName = "getPrintData";
				tError.errorMessage = "操作员机构查询出错！";
				errLog("操作员机构查询出错！");
				mErrors.addOneError(tError);
				return false;
	        }
	        String printcom =
	                "select codename from ldcode where codetype='pdfprintcom' and code='" +
	                comcode + "' with ur";
	        new ExeSQL().getOneValue(printcom);
	        String printcode = new ExeSQL().getOneValue(printcom);
	        mLPCashPrintTable.setManageComLength4(printcode);
	        mLPCashPrintTable.setOperator(Operator);

	        String appntPhoneStr = " ";
	        if (mLCAddressSchema.getPhone() != null &&
	            !mLCAddressSchema.getPhone().equals("")) {
	            appntPhoneStr += mLCAddressSchema.getPhone() + "、";
	        }
	        if (mLCAddressSchema.getHomePhone() != null &&
	            !mLCAddressSchema.getHomePhone().equals("")) {
	            appntPhoneStr += mLCAddressSchema.getHomePhone() + "、";
	        }
	        if (mLCAddressSchema.getCompanyPhone() != null &&
	            !mLCAddressSchema.getCompanyPhone().equals("")) {
	            appntPhoneStr += mLCAddressSchema.getCompanyPhone() + "、";
	        }
	        if (mLCAddressSchema.getMobile() != null &&
	            !mLCAddressSchema.getMobile().equals("")) {
	            appntPhoneStr += mLCAddressSchema.getMobile() + "、";
	        }
	        appntPhoneStr = appntPhoneStr.substring(0, appntPhoneStr.length() - 1);

//	      页眉
	        mLPCashPrintTable.setZipCode(mLCAddressSchema.getZipCode());
	        mLPCashPrintTable.setAddress(mLCAddressSchema.getPostalAddress());
	        mLPCashPrintTable.setAppntName(mLCContSchema.getAppntName());

//			正文        
//	      AppntName已有
	        SSRS tSSRS=new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
	        String sql="select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
	        tSSRS=tExeSQL.execSQL(sql);
	        mLPCashPrintTable.setYear1(tSSRS.GetText(1, 1));
	        mLPCashPrintTable.setQuarter1(tSSRS.GetText(1, 2));
	        mLPCashPrintTable.setSolvency(tSSRS.GetText(1, 3));
	        mLPCashPrintTable.setFlag(tSSRS.GetText(1, 4));
	        
	        String sql2="select Year,quarter,riskrate  from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
	        tSSRS=tExeSQL.execSQL(sql2);
	        mLPCashPrintTable.setYear2(tSSRS.GetText(1, 1));
	        mLPCashPrintTable.setQuarter2(tSSRS.GetText(1, 2));
	        mLPCashPrintTable.setRiksRate(tSSRS.GetText(1, 3));
	        
	        mLPCashPrintTable.setContNo(mLCContSchema.getContNo());
	        mLPCashPrintTable.setRiskName(mExeSQL.getOneValue("select riskname from lmrisk where riskcode='"+mLCPolSchema.getRiskCode()+"'"));
	        mLPCashPrintTable.setCValiDate(mLCContSchema.getCValiDate());
//	      AppntName已有
	        mLPCashPrintTable.setInsuredName(mLCPolSchema.getInsuredName());
	        mLPCashPrintTable.setPayMode(mExeSQL.getOneValue("select codename from ldcode where codetype='paymode' and code='"+mLCContSchema.getPayMode()+"'"));
	        mLPCashPrintTable.setSumPolMoney(mExeSQL.getOneValue("select sum(sumduepaymoney) from ljapayperson where polno='"+mLCPolSchema.getPolNo()+"' and paytype='ZC' and confdate<='"+mLOBonusPolSchema.getAGetDate()+"'"));
	        mLPCashPrintTable.setShouldDate(CommonBL.decodeDate(mLOBonusPolSchema.getSGetDate()));
	        
//	      列表信息
	        String tPolYear=String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(),mLOBonusPolSchema.getSGetDate(), "Y"));
	        mLPCashPrintTable.setPolYear(tPolYear);
//	      ShouldDate已有
	        mLPCashPrintTable.setActuDate(CommonBL.decodeDate(mLOBonusPolSchema.getAGetDate()));
	        mLPCashPrintTable.setBonusMoney(mLOBonusPolSchema.getBonusMoney()+"");
//	      add by xp #1243 分红险迟发利息 20130717
//	        mLPCashPrintTable.add("Interest", mLOBonusPolSchema.getBonusInterest());
	        mLPCashPrintTable.setBeforeBonusMoney(mExeSQL.getOneValue("select nvl(sum(BonusMoney),0) from LOBonusPol where polno='"+mLOBonusPolSchema.getPolNo()+"' and FiscalYear<"+mLOBonusPolSchema.getFiscalYear()+" with ur"));
	        mLPCashPrintTable.setAfterBonusMoney(mExeSQL.getOneValue("select nvl(sum(money),0) from lcinsureacctrace where polno='"+mLOBonusPolSchema.getPolNo()+"' and paydate<='"+mLOBonusPolSchema.getSGetDate()+"'"));
//	      提示信息
//	        String tHLYear=mExeSQL.getOneValue("select year('"+mLOBonusPolSchema.getSGetDate()+"') from dual");
	        mLPCashPrintTable.setHLYear(mLOBonusPolSchema.getFiscalYear()+"");
	        mLPCashPrintTable.setHLAllSum(tLDBonusSumSchema.getHLAllSum()+"");
	        mLPCashPrintTable.setHLAppntSum(tLDBonusSumSchema.getHLAppntSum()+"");        
	    	String trate=mExeSQL.getOneValue("select nvl(b.hlinterestrate*100,0) from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+mLOBonusPolSchema.getPolNo()+"' " +
	    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
	        mLPCashPrintTable.setInterestRate(trate);

	        //业务员信息
	        String agntPhone = "";
	        String temPhone = "";
	        LAAgentDB tLaAgentDB = new LAAgentDB();
	        tLaAgentDB.setAgentCode(mLCContSchema.getAgentCode());
	        tLaAgentDB.getInfo();
	        mLPCashPrintTable.setAgentName(tLaAgentDB.getName());
	        //mLPCashPrintTable.add("AgentCode", tLaAgentDB.getAgentCode());
	        mLPCashPrintTable.setAgentCode(tLaAgentDB.getGroupAgentCode());
	        temPhone = tLaAgentDB.getMobile();
	        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
	            agntPhone = tLaAgentDB.getPhone();
	        } else {
	            agntPhone = temPhone;
	        }
	        mLPCashPrintTable.setAgentPhone(agntPhone);
	        //机构信息
	        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
	        String branchSQL = " select * from LABranchGroup where agentgroup = "
	                           +
	                " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
	                           +
	                " (select agentgroup from laagent where agentcode ='"
	                           + tLaAgentDB.getAgentCode() + "'))"
	                           ;
	        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
	                branchSQL);
	        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
	            CError.buildErr(this, "查询业务员机构失败");
	            errLog("查询业务员机构失败!");
	            return false;
	        }
	        mLPCashPrintTable.setAgentGroup(tLABranchGroupSet.get(1).getName());
//	        mLPCashPrintTable.add("BarCode1", mLOBonusPolSchema.getContNo());
//	        mLPCashPrintTable.add("BarCodeParam1",
//	                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

	        setFixedInfo();
	        
//	        if (mLPCashPrintTable.size() > 0) {
//	            xmlexport.addmLPCashPrintTable(mLPCashPrintTable);
//	        }
//	        
//	        ListTable tEndTable = new ListTable();
//	        tEndTable.setName("END");
//	        String[] tEndTitle = new String[0];
//	        xmlexport.addListTable(tEndTable, tEndTitle);
//	        
//	        xmlexport.outputDocumentToFile("F:\\", "HL001xp");
//	        mResult.clear();
//	        mResult.addElement(xmlexport);
	        return true;
	    }
		
		private boolean getSchema() {
//	    	获取投保人地址
	        LCAppntDB tLCAppntDB = new LCAppntDB();
	        tLCAppntDB.setContNo(Contno);
	        if (!tLCAppntDB.getInfo()) {
	            CError.buildErr(this,
	                            "投保人表中缺少数据");
	            errLog("投保人表中缺少数据！");
	            return false;
	        }
	        LCAddressDB tLCAddressDB = new LCAddressDB();
	        tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
	        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
	        if(!tLCAddressDB.getInfo()){
	        	// @@错误处理
				System.out.println("BonusCashPrintBL+getPrintData++--");
				CError tError = new CError();
				tError.moduleName = "BonusCashPrintBL";
				tError.functionName = "getPrintData";
				tError.errorMessage = "地址表缺少数据!";
				mErrors.addOneError(tError);
				errLog("地址表缺少数据!");
				return false;
	        }
	        mLCAddressSchema.setSchema(tLCAddressDB.getSchema());
	        LCContDB tLCContDB = new LCContDB();
	        tLCContDB.setContNo(Contno);
	        if(!tLCContDB.getInfo()){
	        	// @@错误处理
				System.out.println("BonusCashPrintBL+getPrintData++--");
				CError tError = new CError();
				tError.moduleName = "BonusCashPrintBL";
				tError.functionName = "getPrintData";
				tError.errorMessage = "获取保单信息失败!";
				mErrors.addOneError(tError);
				errLog("获取保单信息失败!");
				return false;
	        }
	        mLCContSchema.setSchema(tLCContDB.getSchema());
	        LCPolDB tLCPolDB = new LCPolDB();
	        tLCPolDB.setPolNo(mLOBonusPolSchema.getPolNo());
	        if(!tLCPolDB.getInfo()){
	        	// @@错误处理
				System.out.println("BonusCashPrintBL+getPrintData++--");
				CError tError = new CError();
				tError.moduleName = "BonusCashPrintBL";
				tError.functionName = "getPrintData";
				tError.errorMessage = "获取险种信息失败!";
				mErrors.addOneError(tError);
				errLog("获取险种信息失败!");
				return false;
	        }
	        mLCPolSchema.setSchema(tLCPolDB.getSchema());
	        return true;
	    }
		
		private void setFixedInfo() {
	        LDComDB tLDComDB = new LDComDB();
	        tLDComDB.setComCode(mLCContSchema.getManageCom());
	        tLDComDB.getInfo();
	        mLPCashPrintTable.setServicePhone(tLDComDB.getServicePhone());
	        mLPCashPrintTable.setLetterservicename(tLDComDB.getLetterServiceName());
	        mLPCashPrintTable.setServiceAddress(tLDComDB.getServicePostAddress());
	        mLPCashPrintTable.setServiceZip(tLDComDB.getLetterServicePostZipcode());
	        mLPCashPrintTable.setComName(tLDComDB.getLetterServiceName());
	        mLPCashPrintTable.setPrintDate(CommonBL.decodeDate(PubFun.getCurrentDate()));
	        mLPCashPrintTable.setFax(tLDComDB.getFax());
	    }
		
}
