package com.cbsws.xml.ctrl.complexpds;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.BudgetResult;
import com.cbsws.obj.ToCancelBudgetMsgBody;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.EdorCTTestBL;
import com.sinosoft.lis.bq.EdorCalZTTestBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;




public class EToCancelBudgetInterfaceBL extends ABusLogic {

	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mEdorAcceptNo;
	public ToCancelBudgetMsgBody toCancelBudgetMsgBody;
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    private MMap map = new MMap();
	EdorCalZTTestBL edorCalZTTestBL = new EdorCalZTTestBL();
	EdorCTTestBL edorCTTestBL = new EdorCTTestBL();
	List<BudgetResult> budgetResults;
	protected boolean deal(MsgCollection cMsgInfos) {
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkdata()){
        	return false;
        }
        //进行退保试算
        if(!cancelBuget()){
        	return false;
        }
        
		return true;
	}
	/**
	 * 解析xml
	 * @param cMsgInfos
	 * @return
	 */
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List ToCancelBugetMsgBodies = cMsgInfos.getBodyByFlag("ToCancelBudgetMsgBody");
        if (ToCancelBugetMsgBodies == null || ToCancelBugetMsgBodies.size() != 1)
        {
            errLog("申请报文中获取报文体失败。");
            return false;
        }
        toCancelBudgetMsgBody = (ToCancelBudgetMsgBody)ToCancelBugetMsgBodies.get(0);
        
        return true;
	}
	/**
	 * 校验数据
	 * @return
	 */
	private boolean checkdata(){
		String contNo = toCancelBudgetMsgBody.getContNo();
		if(contNo == null || "".equals(contNo)){
			errLog("保单号不能为空！");
			return false;
		}
		
		LCContSchema tLCContSchema = new LCContSchema();
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(contNo);
		tLCContSchema = tLCContDB.query().get(1);
		if(tLCContSchema == null){
			errLog("没有查询到该保单号的有效保单！");
			return false;
		}
		//获取保单生效日
		String cvalidate = tLCContSchema.getCValiDate();
		
		String edorValiDate = toCancelBudgetMsgBody.getEdorValiDate();
		if(edorValiDate == null || "".equals(edorValiDate)){
			errLog("保全生效日期不能为空！");
			return false;
		}
		if(edorValiDate.length()!=10){
			errLog("保全生效日期格式不正确！");
			return false;
		}
		if(CommonBL.stringToDate(edorValiDate).before(PubFun.calDate(CommonBL.stringToDate(PubFun.getCurrentDate()), -10, "D", null))){
			errLog("保全生效日期不能早于当前日期超过10天！");
			return false;
		}
		if(CommonBL.stringToDate(edorValiDate).before(CommonBL.stringToDate(cvalidate))){
			errLog("保全生效日期不能早于保单生效日期！");
			return false;
		}
		if(CommonBL.stringToDate(PubFun.getCurrentDate()).before(CommonBL.stringToDate(edorValiDate))){
			errLog("保全生效日期不能晚于当前日期！");
			return false;
		}
		return true;
	}
	/**
	 * 退保试算	
	 * @return
	 */
	private boolean cancelBuget(){
        double sumMoney = 0;
		String contNo = toCancelBudgetMsgBody.getContNo();
		String edorValiDate = toCancelBudgetMsgBody.getEdorValiDate();
		edorCalZTTestBL.setEdorValiDate(edorValiDate);
		edorCalZTTestBL.setOperator(Operator);
		LCPolDB lcPolDB = new LCPolDB();
		lcPolDB.setContNo(contNo);
		LCPolSet lcPolSet = lcPolDB.query();
		if(lcPolSet.size()!=0){
			for(int i=1;i<=lcPolSet.size();i++){
				LCPolSchema lcPolSchema = lcPolSet.get(i);
				edorCalZTTestBL.setCurPayToDateLongPol(lcPolSet.get(i).getPaytoDate());
				LJSGetEndorseSchema lJSGetEndorseSchema= edorCalZTTestBL.budgetOnePol(lcPolSchema);
				if(edorCalZTTestBL.mErrors.needDealError()){
					errLog(edorCalZTTestBL.mErrors.getErrContent());
					return false;
				}
				double money = lJSGetEndorseSchema.getGetMoney();
				System.out.println("险种"+lcPolSchema.getRiskCode()+"退费："+money);
				sumMoney+=money;
			}
			System.out.println("合计退费："+sumMoney);
			if(sumMoney>0){
				errLog("试算失败！");
				return false;
			}
		sumMoney = Arith.round(-sumMoney,2);	
		}else{
			errLog("没有查询到该保单的个人险种表信息！");
			return false;
		}

		BudgetResult budgetResult = new BudgetResult();
		budgetResult.setSumMoney(String.valueOf(sumMoney));
		getWrapParmList(budgetResult);
    	getXmlResult();
		return true;
	}	
	 private void getWrapParmList(BudgetResult budgetResult){
		 budgetResults = new ArrayList<BudgetResult>();
	    
		 budgetResults.add(budgetResult);
	    }
	    
		//返回报文
		public void getXmlResult(){
			//返回退费信息
			for(int i=0;i<budgetResults.size();i++){
				putResult("budgetResult", (BudgetResult)budgetResults.get(i));
			}

		}	
    
}
