/**
 * 2016-01-04
 */
package com.cbsws.xml.ctrl.complexpds;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 复杂产品保单信息
 * 
 * @author LC
 *
 */
public class LCInsuredTable extends BaseXmlSch
{
	
	private static final long serialVersionUID = -4525154779716614283L;


    /** 被保人姓名 */
    private String Name = null;

    /** 性别 */
    private String Sex = null;

    /** 出生日期 */
    private String Birthday = null;

    /** 证件类型 */
    private String IDType = null;
    
    /** 证件号码 */
    private String IDNo = null;
    
    /**  职业类型 */
    private String OccupationType = null;
    
    /** 与投保人关系 */
    private String RelaToAppnt = null;
    
    /** 保障计划  */
    private String ContPlanCode = null;
    

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getBirthday() {
		return Birthday;
	}

	public void setBirthday(String birthday) {
		Birthday = birthday;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getIDNo() {
		return IDNo;
	}

	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}

	public String getOccupationType() {
		return OccupationType;
	}

	public void setOccupationType(String occupationType) {
		OccupationType = occupationType;
	}

	public String getRelaToAppnt() {
		return RelaToAppnt;
	}

	public void setRelaToAppnt(String relaToAppnt) {
		RelaToAppnt = relaToAppnt;
	}

	public String getContPlanCode() {
		return ContPlanCode;
	}

	public void setContPlanCode(String contPlanCode) {
		ContPlanCode = contPlanCode;
	}
    

	
	
	
}
