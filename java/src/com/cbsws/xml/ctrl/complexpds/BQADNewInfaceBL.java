package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.BackParamInfo;
import com.cbsws.obj.ECCertSalesInfoTable;
import com.cbsws.obj.LCAddressTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPEdorMainTable;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LPAddressSchema;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LPAddressSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.task.BQADEdorBL;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.EasyEdorBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class BQADNewInfaceBL extends ABusLogic {

	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public LCContTable cLCContTable;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mEdorAcceptNo;
	private LCAddressTable mLCAddressTable;
	private LGWorkTable mLGWorkTable;
	private List mLPEdorMainList;
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    private MMap map = new MMap();
    
    boolean appntFlag=false;
    boolean insuredFlag=false;
	
	protected boolean deal(MsgCollection cMsgInfos) {
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkdata()){
        	return false;
        }
        
        
        //修改联系方式信息
        if(!changeAppntInfo()){
        	return false;
        }
        
		return true;
	}

	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List tLCContTableList = cMsgInfos.getBodyByFlag("LCContTable");
        if (tLCContTableList == null || tLCContTableList.size() != 1)
        {
            errLog("申请报文中获取保单号信息失败。");
            return false;
        }
        cLCContTable = (LCContTable)tLCContTableList.get(0);
        
        List tmLCAddressTable = cMsgInfos.getBodyByFlag("LCAddressTable");
        if (tmLCAddressTable == null || tmLCAddressTable.size() != 1)
        {
            errLog("申请报文中获取投保人信息失败。");
            return false;
        }
        mLCAddressTable = (LCAddressTable)tmLCAddressTable.get(0);
        
        List tLGWorkList = cMsgInfos.getBodyByFlag("LGWorkTable");
        if(tLGWorkList == null ||tLGWorkList.size()!=1)
        {
            errLog("申请报文中获取工单信息失败。");
            return false;
        }
        mLGWorkTable=(LGWorkTable)tLGWorkList.get(0);
        
        return true;
	}
	private boolean checkdata(){
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(cLCContTable.getContNo());
        if(!tLCContDB.getInfo())
        {
        	errLog("没有查询到保单信息"+cLCContTable.getContNo()+"。");
            return false;
        }
        String mCustomerNo=cLCContTable.getCustomerNo1();
        String mAddressNo="";
        String mIdNo="";
        String StrSQL="select 1 from LCAppnt where contno='"+cLCContTable.getContNo()+"' " +
		" and appntno='"+mCustomerNo+"' with ur";
        String isAppnt=new ExeSQL().getOneValue(StrSQL);
        if(null==isAppnt||"".equals(isAppnt)){//非投保人即为被保人联系方式变更--BQ
            String insuredSql="select 1 from LCinsured where contno='"+cLCContTable.getContNo()+"' " +
    		" and insuredno='"+mCustomerNo+"' with ur";
            String isInsured=new ExeSQL().getOneValue(insuredSql);
            if(null==isInsured||"".equals(isInsured)){
            	errLog("没有查询到保单客户信息"+cLCContTable.getContNo()+"。");
                return false;
            }else {
            	insuredFlag=true;
                LCInsuredDB tLCInsuredDB = new LCInsuredDB();
                tLCInsuredDB.setContNo(tLCContDB.getContNo());
                tLCInsuredDB.setInsuredNo(mCustomerNo);
                tLCInsuredDB.getInfo();
                mIdNo=tLCInsuredDB.getIDNo();
                mAddressNo=tLCInsuredDB.getAddressNo();
            }
        }else{
        	appntFlag=true;
            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(tLCContDB.getContNo());
            tLCAppntDB.setAppntNo(mCustomerNo);
            tLCAppntDB.getInfo();
            mIdNo=tLCAppntDB.getIDNo();
            mAddressNo=tLCAppntDB.getAddressNo();
        }

        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(mCustomerNo);
        tLCAddressDB.setAddressNo(mAddressNo==null?"1":mAddressNo);
        tLCAddressDB.getInfo();
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        tLCAddressSchema.setSchema(tLCAddressDB.getSchema());
		
		if(mLCAddressTable.getMobile()!=null && !"".equals(mLCAddressTable.getMobile())){
			if (!Pattern.matches("^1[3|4|5|8][0-9]\\d{8}$",mLCAddressTable.getMobile())){
				 errLog("移动电话需为11位数字且必须为13、14、15、17、18开头。");
			     return false;
			 }
			//当移动电话变更时才校验
			if(mLCAddressTable.getMobile()!=tLCAddressSchema.getMobile() && appntFlag)
			{
				String mobileSql = "select count(distinct customerno) from lcaddress where mobile='" + mLCAddressTable.getMobile() + "' and Customerno<>'" +tLCContDB.getAppntNo()+ "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno) ";
				tSSRS = tExeSQL.execSQL(mobileSql);
				if(tSSRS.getMaxRow()>0){
					String  mobileCount = tSSRS.GetText(1, 1);
					int mobilenum=Integer.parseInt(mobileCount);
					if(mobilenum >= 2){
						errLog("该客户移动电话已在三个以上不同投保人的保单中出现！");
						return false;
					}
				}
			}
			if(insuredFlag){
				String tInsuSql1 = " select agentcode from laagent where idno != '" +mIdNo+ "' and mobile = '" +  mLCAddressTable.getMobile()  + "' ";
				tSSRS = tExeSQL.execSQL(tInsuSql1);
				if(tSSRS.getMaxRow()>0 ){
					errLog("被保人移动电话不能与业务人员一致！");
					return false;
				}
			}
			String  tAppSql1 = " select agentcode from laagent where idno != '" + tLCContDB.getAppntIDNo() + "' and mobile = '" + mLCAddressTable.getMobile() + "' ";
			tSSRS = tExeSQL.execSQL(tAppSql1);
			if(tSSRS.getMaxRow()>0 ){
				errLog("移动电话不能与业务人员一致！");
				return false;
			}
		}

		if(mLCAddressTable.getPhone()!=null && !"".equals(mLCAddressTable.getPhone())){
			if(mLCAddressTable.getPhone().length() < 7)
			{
				errLog("联系电话不能少于7位，请核查！");
		   		return false;
			}
//			String regPh1 ="^[|d|-|(|)]+$";
			if(!Pattern.matches("^[\\d|\\-|(|)]+$",mLCAddressTable.getPhone())){
				errLog("固定电话仅允许包含数字、小括号和“-”！");
				return  false;
			}
			//当电话变更时才校验
			if(mLCAddressTable.getPhone()!=tLCAddressSchema.getPhone() && appntFlag)
			{
				String phoneSql = "select count(distinct customerno) from lcaddress where Phone='" + mLCAddressTable.getPhone() + "' and Customerno<>'" +tLCContDB.getAppntNo()+ "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno) ";
				tSSRS = tExeSQL.execSQL(phoneSql);
				if(tSSRS.getMaxRow()>0){
					String  phoneCount = tSSRS.GetText(1, 1);
					int phoneenum=Integer.parseInt(phoneCount);
					if(phoneenum >= 2){
						errLog("该客户联系电话已在三个以上不同投保人的保单中出现！");
						return false;
					}
				}
			}
			if(insuredFlag){
				String tInsuSql1 = " select agentcode from laagent where idno != '" + mIdNo + "' and phone = '" + mLCAddressTable.getPhone() + "' ";
				tSSRS = tExeSQL.execSQL(tInsuSql1);
				if(tSSRS.getMaxRow()>0 ){
					errLog("被保人联系电话不能与业务人员一致！");
					return false;
				}
			}
			String  tAppSql1 = " select agentcode from laagent where idno != '" + tLCContDB.getAppntIDNo() + "' and Phone = '" + mLCAddressTable.getPhone() + "' ";
			tSSRS = tExeSQL.execSQL(tAppSql1);
			if(tSSRS.getMaxRow()>0 ){
				errLog("客户联系电话不能与业务人员一致！");
				return false;
			}
		}
		
		if((mLCAddressTable.getPostalCommunity() !=null && !"".equals(mLCAddressTable.getPostalCommunity())) 
				|| (mLCAddressTable.getPostalCity() !=null && !"".equals(mLCAddressTable.getPostalCity())) 
				|| (mLCAddressTable.getPostalProvince() !=null && !"".equals(mLCAddressTable.getPostalProvince()))
				|| (mLCAddressTable.getPostalCounty() !=null && !"".equals(mLCAddressTable.getPostalCounty())) 
				|| (mLCAddressTable.getPostalStreet() !=null && !"".equals(mLCAddressTable.getPostalStreet()))){
			
			String sql="select 1 from ldcode1 where codetype='province1' and code='"+mLCAddressTable.getPostalProvince()+"' "
			+" and code in (select code1 from ldcode1 where codetype='city1' and code='"+mLCAddressTable.getPostalCity()+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+mLCAddressTable.getPostalCounty()+"'))";
			tSSRS = tExeSQL.execSQL(sql);
			if(tSSRS.getMaxRow()==0){
				 errLog("联系地址级联关系不正确，请检查！");
				 return false;	  
			}
		    StringBuilder sb = new StringBuilder();
		    
			if(mLCAddressTable.getPostalProvince() ==null || "".equals(mLCAddressTable.getPostalProvince())){
				errLog("修改联系地址信息时，联系地址中的省级代码不能为空！");
				return false;
			}else{
				String province1="select codename from ldcode1 where codetype='province1' and code='"+mLCAddressTable.getPostalProvince()+"' ";
				tSSRS = tExeSQL.execSQL(province1);
				if(tSSRS.getMaxRow()==0){
					errLog("修改联系地址信息时，联系地址中的省级代码查询失败！");
					return false;
				}
				sb.append(String.valueOf(tSSRS.GetText(1, 1)));
			}
			if(mLCAddressTable.getPostalCity() ==null || "".equals(mLCAddressTable.getPostalCity())){
				errLog("修改联系地址信息时，联系地址中的市省级代码不能为空！");
				return false;
			}else{
				String city1="select codename from ldcode1 where codetype='city1' and code='"+mLCAddressTable.getPostalCity()+"'";
				tSSRS = tExeSQL.execSQL(city1);
				if(tSSRS.getMaxRow()==0){
					errLog("修改联系地址信息时，联系地址中的省级代码查询失败！");
					return false;
				}
				if("000000".equals(mLCAddressTable.getPostalCity())){
					
				}else{
					sb.append(String.valueOf(tSSRS.GetText(1, 1)));
				}

			}
			if(mLCAddressTable.getPostalCounty() ==null || "".equals(mLCAddressTable.getPostalCounty())){
				errLog("修改联系地址信息时，联系地址中的县省级代码不能为空！");
				return false;
			}else {
				String county1="select codename from ldcode1 where codetype='county1' and code='"+mLCAddressTable.getPostalCounty()+"'";
				tSSRS = tExeSQL.execSQL(county1);
				if(tSSRS.getMaxRow()==0){
					errLog("修改联系地址信息时，联系地址中的省级代码查询失败！");
					return false;
				}
				if("000000".equals(mLCAddressTable.getPostalCounty())){
					
				}else{
					sb.append(String.valueOf(tSSRS.GetText(1, 1)));
				}
			}
			if(mLCAddressTable.getPostalStreet() ==null || "".equals(mLCAddressTable.getPostalStreet())){
				errLog("修改联系地址信息时，联系地址中的乡镇（街道）不能为空！");
				return false;
			}else {
				sb.append(String.valueOf(mLCAddressTable.getPostalStreet()));
			}
			if(mLCAddressTable.getPostalCommunity() ==null || "".equals(mLCAddressTable.getPostalCommunity())){
				errLog("修改联系地址信息时，联系地址中的村（社区）不能为空！");
				return false;
			}else {
				sb.append(String.valueOf(mLCAddressTable.getPostalCommunity()));
			}


	        sb.toString();
	        String PostalAddress=sb.toString().toLowerCase();
	        mLCAddressTable.setPostalAddress(PostalAddress);
		}else{
			mLCAddressTable.setPostalAddress("");
		}
		
		if(mLCAddressTable.getEMail()!=null && !"".equals(mLCAddressTable.getEMail())){
			if(!Pattern.matches("^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$",mLCAddressTable.getEMail())){
				errLog("电子邮箱格式错误！");
				return  false;
			}
		}
		
		
//		if(mLGWorkTable.getAcceptWayNo()!=null && !"7".equals(mLGWorkTable.getAcceptWayNo())){
//			  errLog("工单受理途径必须为7（pad）！");
//			  return false;	  
//		}
		return true;
	}
	
	private boolean changeAppntInfo(){
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(cLCContTable.getContNo());
        if(!tLCContDB.getInfo())
        {
        	errLog("没有查询到保单信息"+cLCContTable.getContNo()+"。");
            return false;
        }
        //得到关联保单
        String workNo = CommonBL.createWorkNo();
        //得到工单信息
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(tLCContDB.getAppntNo());
        tLGWorkSchema.setTypeNo(mLGWorkTable.getTypeNo());
        tLGWorkSchema.setContNo(cLCContTable.getContNo());
        tLGWorkSchema.setApplyTypeNo(mLGWorkTable.getApplyTypeNo());
        tLGWorkSchema.setAcceptWayNo(mLGWorkTable.getAcceptWayNo());
        tLGWorkSchema.setAcceptDate(mLGWorkTable.getAcceptDate());
        tLGWorkSchema.setWorkNo(workNo);
        tLGWorkSchema.setRemark("调用简易保全生成");
        
        String tAddressNo="";
        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
        if(appntFlag){
            //得到投保人信息
            tLPAppntSchema.setContNo(cLCContTable.getContNo());
            tLPAppntSchema.setAppntNo(tLCContDB.getAppntNo());
            //得到客户保单联系地址
            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(cLCContTable.getContNo());
            tLCAppntDB.setAppntNo(cLCContTable.getCustomerNo1());
            tLCAppntDB.getInfo();
            tAddressNo=tLCAppntDB.getAddressNo();
        }else{
            //得到被保人信息
            tLPInsuredSchema.setContNo(cLCContTable.getContNo());
            tLPInsuredSchema.setInsuredNo(cLCContTable.getCustomerNo1());
            //得到客户保单联系地址
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(cLCContTable.getContNo());
            tLCInsuredDB.setInsuredNo(cLCContTable.getCustomerNo1());
            tLCInsuredDB.getInfo();
            tAddressNo=tLCInsuredDB.getAddressNo();
        }



        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(cLCContTable.getCustomerNo1());
        tLCAddressDB.setAddressNo(tAddressNo==null?"1":tAddressNo);
        tLCAddressDB.getInfo();
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        tLCAddressSchema.setSchema(tLCAddressDB.getSchema());
        

        tLCAddressSchema.setPostalProvince((mLCAddressTable.getPostalProvince()==null || "".equals(mLCAddressTable.getPostalProvince()))?tLCAddressSchema.getPostalProvince():mLCAddressTable.getPostalProvince());
        tLCAddressSchema.setPostalCity((mLCAddressTable.getPostalCity()==null || "".equals(mLCAddressTable.getPostalCity()))?tLCAddressSchema.getPostalCity():mLCAddressTable.getPostalCity());
        tLCAddressSchema.setPostalCounty((mLCAddressTable.getPostalCounty()==null || "".equals(mLCAddressTable.getPostalCounty()))?tLCAddressSchema.getPostalCounty():mLCAddressTable.getPostalCounty());
        tLCAddressSchema.setPostalStreet((mLCAddressTable.getPostalStreet()==null || "".equals(mLCAddressTable.getPostalStreet()))?tLCAddressSchema.getPostalStreet():mLCAddressTable.getPostalStreet());
        tLCAddressSchema.setPostalCommunity((mLCAddressTable.getPostalCommunity()==null || "".equals(mLCAddressTable.getPostalCommunity()))?tLCAddressSchema.getPostalCommunity():mLCAddressTable.getPostalCommunity());
        tLCAddressSchema.setZipCode((mLCAddressTable.getZipCode()==null || "".equals(mLCAddressTable.getZipCode()))?tLCAddressSchema.getZipCode():mLCAddressTable.getZipCode());        
        tLCAddressSchema.setPhone((mLCAddressTable.getPhone()==null || "".equals(mLCAddressTable.getPhone()))?tLCAddressSchema.getPhone():mLCAddressTable.getPhone());            
        tLCAddressSchema.setFax((mLCAddressTable.getFax()==null || "".equals(mLCAddressTable.getFax()))?tLCAddressSchema.getFax():mLCAddressTable.getFax());                
        tLCAddressSchema.setHomeAddress((mLCAddressTable.getHomeAddress()==null || "".equals(mLCAddressTable.getHomeAddress()))?tLCAddressSchema.getHomeAddress():mLCAddressTable.getHomeAddress());
        tLCAddressSchema.setHomeZipCode((mLCAddressTable.getHomeZipCode()==null || "".equals(mLCAddressTable.getHomeZipCode()))?tLCAddressSchema.getHomeZipCode():mLCAddressTable.getHomeZipCode());
        tLCAddressSchema.setHomePhone((mLCAddressTable.getHomePhone()==null || "".equals(mLCAddressTable.getHomePhone()))?tLCAddressSchema.getHomePhone():mLCAddressTable.getHomePhone());    
        tLCAddressSchema.setHomeFax((mLCAddressTable.getHomeFax()==null || "".equals(mLCAddressTable.getHomeFax()))?tLCAddressSchema.getHomeFax():mLCAddressTable.getHomeFax());        
        tLCAddressSchema.setCompanyAddress((mLCAddressTable.getCompanyAddress()==null || "".equals(mLCAddressTable.getCompanyAddress()))?tLCAddressSchema.getCompanyAddress():mLCAddressTable.getCompanyAddress());
        tLCAddressSchema.setCompanyZipCode((mLCAddressTable.getCompanyZipCode()==null || "".equals(mLCAddressTable.getCompanyZipCode()))?tLCAddressSchema.getCompanyZipCode():mLCAddressTable.getCompanyZipCode());
        tLCAddressSchema.setCompanyPhone((mLCAddressTable.getCompanyPhone()==null || "".equals(mLCAddressTable.getCompanyPhone()))?tLCAddressSchema.getCompanyPhone():mLCAddressTable.getCompanyPhone());
        tLCAddressSchema.setCompanyFax((mLCAddressTable.getCompanyFax()==null || "".equals(mLCAddressTable.getCompanyFax()))?tLCAddressSchema.getCompanyFax():mLCAddressTable.getCompanyFax());  
        tLCAddressSchema.setMobile((mLCAddressTable.getMobile()==null || "".equals(mLCAddressTable.getMobile()))?tLCAddressSchema.getMobile():mLCAddressTable.getMobile());  
        tLCAddressSchema.setEMail((mLCAddressTable.getEMail()==null || "".equals(mLCAddressTable.getEMail()))?tLCAddressSchema.getEMail():mLCAddressTable.getEMail()); 

        //拼接联系地址
        tLCAddressSchema.setPostalAddress((mLCAddressTable.getPostalAddress()==null || "".equals(mLCAddressTable.getPostalAddress()))?tLCAddressSchema.getPostalAddress():mLCAddressTable.getPostalAddress());

        LPContSet tLPContSet = new LPContSet();
        LPContSchema tLPContSchema = new LPContSchema();
        tLPContSchema.setEdorNo(workNo);
        tLPContSchema.setEdorType(BQ.EDORTYPE_AD);
        tLPContSchema.setContNo(tLCContDB.getContNo());
        tLPContSet.add(tLPContSchema);
        
        //操作员信息
        mGlobalInput.Operator = Operator;
        mGlobalInput.ManageCom = tLCContDB.getManageCom();
        mGlobalInput.ComCode = mGlobalInput.ManageCom;
        
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGWorkSchema);
        data.add(tLPAppntSchema);
        data.add(tLPInsuredSchema);
        data.add(tLCAddressSchema);
        data.add(tLPContSet);
        
        BQADEdorBL tBQADEdorBL = new BQADEdorBL();
        if(!tBQADEdorBL.submitData(data))
        {
        	 errLog("保全变更失败"+tBQADEdorBL.mErrors.getFirstError());
            return false;
        }
        LPEdorMainTable tLPEdorMainTable = new LPEdorMainTable();
        tLPEdorMainTable.setEdorAcceptNo(workNo);
        //返回报文
    	//组织返回报文
    	getWrapParmList(tLPEdorMainTable);
    	getXmlResult();
		return true;
	}

    private void getWrapParmList(LPEdorMainTable tLPEdorMainTable){
    	mLPEdorMainList = new ArrayList();
    
    	mLPEdorMainList.add(tLPEdorMainTable);
    }
    
	//返回报文
	public void getXmlResult(){
		//返回退费信息
		for(int i=0;i<mLPEdorMainList.size();i++){
			putResult("LPEdorMainTable", (LPEdorMainTable)mLPEdorMainList.get(i));
		}

	}
    
}
