package com.cbsws.xml.ctrl.complexpds;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.EdorAppInfo;
import com.cbsws.obj.EdorItemInfo;
import com.cbsws.obj.EndorsementInfo;
import com.cbsws.obj.PayInfo;
import com.cbsws.obj.PolicyInfo;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BQWTInfaceBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class XYSWTInfaceTask extends ABusLogic{
	
	public GlobalInput mGlobalInput = new  GlobalInput();//公共信息
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public String mEdorAcceptNo;
	public EdorAppInfo mEdorAppInfo;
	private EdorItemInfo mEdorItemInfo;
	private LCContSchema mLCContSchema;
	private PayInfo mPayInfo;
	private EndorsementInfo mEndorsementInfo;
	private String wrapcode = null;
	private String signdate = "";
	private String TBBatchNo = "";//投保报文批次号
	
	
	
        public XYSWTInfaceTask() {
        }
        
        
        public boolean deal(MsgCollection cMsgInfos)  {
        	//解析XML
	        if(!parseXML(cMsgInfos)){
	        	return false;
	        }
	        //先进行下必要的数据校验
	        if(!checkData()){
	        	System.out.println("数据校验失败---");
            	//组织返回报文
	        	getWrapParmList(null,"fail");
            	return false;
	        }

	        mGlobalInput.ManageCom = mEdorItemInfo.getManageCom();
	        mGlobalInput.ComCode = mEdorItemInfo.getManageCom();
	        mGlobalInput.Operator = Operator;
            VData data = new VData();
            data.add(mGlobalInput);
            data.add(mEdorAppInfo);
            data.add(mLCContSchema);
            data.add(mEdorItemInfo);
            data.add(mPayInfo);
            BQWTInfaceBL tBQWTInfaceBL = new BQWTInfaceBL();
            if(!tBQWTInfaceBL.submit(data)){
            	System.out.println("保全操作错误");
            	errLog(""+tBQWTInfaceBL.mErrors.getFirstError());
            	//组织返回报文
    			getWrapParmList(null,"fail");
            	return false;
            }else{
            	//生成报文返回
            	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
            	tLPEdorItemSchema =tBQWTInfaceBL.getEdorItem(); 
            	//组织返回报文
            	getWrapParmList(tLPEdorItemSchema,"success");
            	return true;
            }
        }
        
		private boolean parseXML(MsgCollection cMsgInfos){
        	System.out.println("开始解析保全犹豫期退保接口XML");
    		MsgHead tMsgHead = cMsgInfos.getMsgHead();
    		BatchNo = tMsgHead.getBatchNo();
    		MsgType = tMsgHead.getMsgType();
    		Operator = tMsgHead.getSendOperator();
    		//获取保单信息
			List tEdorAppList = cMsgInfos.getBodyByFlag("EdorAppInfo");
			
	        if (tEdorAppList == null || tEdorAppList.size() != 1)
	        {
	            errLog("申请报文中获取工单信息失败。");
	            return false;
	        }
	        mEdorAppInfo = (EdorAppInfo)tEdorAppList.get(0);  //接口一次只接受一个保单的犹豫期退保
	        //获取工单信息
	        List tEdorItemList = cMsgInfos.getBodyByFlag("EdorItemInfo");
	        if (tEdorItemList == null || tEdorItemList.size() != 1)
	        {
	            errLog("申请报文中获取保全项目信息失败。");
	            return false;
	        }
	        mEdorItemInfo = (EdorItemInfo)tEdorItemList.get(0);
	        
	        //获取其他参数信息
	        List tPayInfoList = cMsgInfos.getBodyByFlag("PayInfo");
	        if (tPayInfoList == null || tPayInfoList.size() != 1)
	        {
	            errLog("申请报文中获取保全项目信息失败。");
	            return false;
	        }
	        mPayInfo = (PayInfo)tPayInfoList.get(0); 
	        
        	//查询保单信息
        	LCContDB tLCContDB = new LCContDB();
        	tLCContDB.setContNo(mEdorItemInfo.getContNo());
        	if(!tLCContDB.getInfo()){
	            errLog("传入的保单号错误，核心系统不存在保单“"+mEdorItemInfo.getContNo()+"”");
	            return false;        		
        	}
        	mLCContSchema = tLCContDB.getSchema();
	        
        	//增加并发控制，同一个保单只能请求一次
        	MMap tCekMap = null;
        	tCekMap = lockLGWORK(mLCContSchema);
        	if (tCekMap == null)
        	{
        		errLog("保单"+mLCContSchema.getContNo()+"正在进行犹豫期退保，请不要重复请求");	
        		return false;
        	}
        	if(!submit(tCekMap)){
        		return false;
        	}
	        return true;
        }
        
        private void getWrapParmList(LPEdorItemSchema edoritem,String ztFlag){
        	//判断失败还是成功
        	if("success".equals(ztFlag)&&edoritem!=null){
        		mEndorsementInfo =new EndorsementInfo();
        		mEndorsementInfo.setEdorNo(edoritem.getEdorNo());
        		mEndorsementInfo.setEdorType(edoritem.getEdorType());
        		mEndorsementInfo.setContNo(edoritem.getContNo());
        		mEndorsementInfo.setAppntName("");
        		mEndorsementInfo.setEdorValidate(edoritem.getEdorValiDate());
        		mEndorsementInfo.setEdorApplyDate(edoritem.getEdorAppDate());
        		mEndorsementInfo.setEdorConfdate(PubFun.getCurrentDate());
        		mEndorsementInfo.setSumGetMoney(String.valueOf(Math.abs(edoritem.getGetMoney())));
        		
        		putResult("EndorsementInfo",mEndorsementInfo);
        		
        		String str="select polno,riskcode, "
        				 + " (select riskname from lmriskapp where riskcode=a.riskcode),"
        				 + " a.insuredname,"
        				 + " (select abs(sum(getmoney)) from ljagetendorse where endorsementno=a.edorno and polno=a.polno )"
        				 + " from lbpol a where edorno='"+edoritem.getEdorNo()+"' "
        				 + " and contno='"+edoritem.getContNo()+"' "
        				 + " with ur";
        		SSRS tSSRS=new ExeSQL().execSQL(str);
        		for(int i=1; i<=tSSRS.MaxRow; i++){
        			PolicyInfo tPolicyInfo=new PolicyInfo();
        			tPolicyInfo.setPolNo(tSSRS.GetText(i, 1));
        			tPolicyInfo.setRiskCode(tSSRS.GetText(i, 2));
        			tPolicyInfo.setRiskName(tSSRS.GetText(i, 3));
        			tPolicyInfo.setInsuredName(tSSRS.GetText(i, 4));
        			tPolicyInfo.setGetMoney(tSSRS.GetText(i, 5));
        			putResult("PolicyInfo",tPolicyInfo);
        		}
        		
        		//查询付费通知书号以及金额
        		LJAGetDB tLJAGetDB = new LJAGetDB();
        		LJAGetSet tLJAGetSet = new LJAGetSet();
        		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        		tLJAGetDB.setOtherNo(edoritem.getEdorAcceptNo());
        		tLJAGetSet = tLJAGetDB.query();
        		if(tLJAGetSet!=null&&tLJAGetSet.size()>0){
        			tLJAGetSchema = tLJAGetSet.get(1);
        		}
        		mPayInfo = new PayInfo();
        		mPayInfo.setPayMode(tLJAGetSchema.getPayMode());
        		mPayInfo.setActuGetno(tLJAGetSchema.getActuGetNo());
        		mPayInfo.setAccName(tLJAGetSchema.getAccName());
        		mPayInfo.setAccNo(tLJAGetSchema.getBankAccNo());
        		mPayInfo.setBankCode(tLJAGetSchema.getBankCode());
        		mPayInfo.setPayDate(tLJAGetSchema.getStartGetDate());
        		
        		putResult("PayInfo",mPayInfo);
        	}else if("fail".equals(ztFlag)){
        		mEndorsementInfo =new EndorsementInfo();
        		putResult("EndorsementInfo",mEndorsementInfo);
        		
        		PolicyInfo tPolicyInfo=new PolicyInfo();
        		putResult("PolicyInfo",tPolicyInfo);
        		
        		mPayInfo = new PayInfo();
        		putResult("PayInfo",mPayInfo);
        	}
        }
        
        private boolean checkData(){
        	/**  对保全申请信息的校验*/
        	if(null == mEdorAppInfo.getCustomerNo() || "".equals(mEdorAppInfo.getCustomerNo())){
        		errLog("申请人客户号不能为空");	
        		return false;
        	}
        	if(null == mEdorAppInfo.getTypeNo() || "".equals(mEdorAppInfo.getTypeNo())){
        		errLog("申请人类型不能为空");	
        		return false;
        	}
        	if(null == mEdorAppInfo.getTypeNo() || "".equals(mEdorAppInfo.getTypeNo())){
        		errLog("工单子业务类型不能为空");	
        		return false;
        	}
        	if(null == mEdorAppInfo.getAcceptWayNo() || "".equals(mEdorAppInfo.getAcceptWayNo())){
        		errLog("工单受理途径不能为空");	
        		return false;
        	}
        	/**  对保全项目信息的校验*/
        	if(null == mEdorItemInfo.getEdorType() || "".equals(mEdorItemInfo.getEdorType())){
        		errLog("保全项目代码不能为空");	
        		return false;
        	}
        	if(null == mEdorItemInfo.getContNo() || "".equals(mEdorItemInfo.getContNo())){
        		errLog("保单号不能为空");	
        		return false;
        	}
        	if(null == mEdorItemInfo.getManageCom() || "".equals(mEdorItemInfo.getManageCom())){
        		errLog("保单管理机构不能为空");	
        		return false; 
        	}
        	if(null == mEdorItemInfo.getFee() || "".equals(mEdorItemInfo.getFee())){
        		errLog("管理费不能为空");	
        		return false;
        	}
        	if(null == mEdorItemInfo.getEdorValidate() || "".equals(mEdorItemInfo.getEdorValidate())){
        		errLog("保全生效日期不能为空");	
        		return false;
        	}
        	/**  对付费信息的校验*/
        	String payMode=mPayInfo.getPayMode();
        	if(null == payMode || "".equals(payMode)){
        		errLog("付费方式不能为空");	
        		return false;
        	}
        	if("4".equals(payMode)){
	        	if(null == mPayInfo.getBankCode() || "".equals(mPayInfo.getBankCode())){
	        		errLog("付费方式为银行转账，转账银行不能为空");	
	        		return false;
	        	}
	        	if(null == mPayInfo.getAccNo() || "".equals(mPayInfo.getAccNo())){
	        		errLog("付费方式为银行转账，转账账号不能为空");	
	        		return false;
	        	}
	        	if(null == mPayInfo.getAccName() || "".equals(mPayInfo.getAccName())){
	        		errLog("付费方式为银行转账，转账户名不能为空");	
	        		return false;
	        	}
        	}
        	if(!BQ.EDORTYPE_WT.equals(mEdorItemInfo.getEdorType())){
        		errLog("申请的保全类型与请求不符");
        		return false;
        	}
        	
        	/**
        	 * 添加联合校验，保单号，投保人客户号，
        	 */
        	if(!mEdorAppInfo.getCustomerNo().equals(mLCContSchema.getAppntNo())){
        		errLog("投保人客户号与保单客户号不符");
		    	return false;
        	}
        	if(!mEdorItemInfo.getManageCom().equals(mLCContSchema.getManageCom())){
        		errLog("管理机构与保单管理机构不符");
		    	return false;
        	}
        	
        	try {
        		//获取投保批次号
        		String TBBatchNoSql = "select batchno from WXContRelaInfo where  PRTNO = (select prtno from db2inst1.lccont  where contno = '"+mEdorItemInfo.getContNo()+"') ";
        		SSRS TBBatchNoSSRS = new ExeSQL().execSQL(TBBatchNoSql);
        		if(TBBatchNoSSRS == null || TBBatchNoSSRS.MaxRow<=0){
        			System.out.println("没有得到投保批次号！");
		  	         errLog("没有得到投保批次号！");
		  	         return false;
        		}
        		this.TBBatchNo = TBBatchNoSSRS.GetText(1, 1);
        		//获取套餐编码
        		String riskWrapCodeSql = "select riskwrapcode from lcriskdutywrap where contno = '"+mLCContSchema.getContNo()+"' ";
				SSRS riskWrapCodeSSRS = new ExeSQL().execSQL(riskWrapCodeSql);
				if(riskWrapCodeSSRS == null || riskWrapCodeSSRS.MaxRow<=0){
					System.out.println("没有得到套餐编码！");
		  	         errLog("没有得到套餐编码！");
		  	         return false;
				}
				this.wrapcode =  riskWrapCodeSSRS.GetText(1, 1);
				
				if(this.TBBatchNo == null || "".equals(this.TBBatchNo)){
					System.out.println("投保批次号为空");
    		    	errLog("投保批次号为空");
    		    	return false;
				}else{
					System.out.println("投保批次号是："+this.TBBatchNo);
    		    	//添加小雨伞ZZJ225产品退保日期检验，退保日期在生效日期之前全额退保，退保日期在生效日期之后不能进行线上退保
					if(this.TBBatchNo.startsWith("XYS") && !"ZZJ225".equals(this.wrapcode)){
						System.out.println("报文中的保单号对应的产品不是小雨伞惠心无忧");
						errLog("报文中的保单号对应的产品不是小雨伞惠心无忧，不能线上退保");
						return false;
					}
					//添加中民产品退保日期检验，退保日期在生效日期之前全额退保，退保日期在生效日期之后不能进行线上退保
					if(this.TBBatchNo.startsWith("ZMB") && !"ZZJ217".equals(this.wrapcode) && !"ZZJ222".equals(this.wrapcode) && !"ZZJ224".equals(this.wrapcode)){
						System.out.println("报文中的保单号对应的产品不是中民平台的");
						errLog("报文中的保单号对应的产品不是中民平台的，不能线上退保");
						return false;
					}
					//添加慧择产品退保日期检验，退保日期在生效日期之前全额退保，退保日期在生效日期之后不能进行线上退保
					if(this.TBBatchNo.startsWith("HZW") && !"ZZJ241".equals(this.wrapcode) && !"ZZJ242".equals(this.wrapcode) && !"ZZJ224".equals(this.wrapcode)){
						System.out.println("报文中的保单号对应的产品不是慧择平台的");
						errLog("报文中的保单号对应的产品不是慧择平台的，不能线上退保");
						return false;
					}
					//添加美联产品退保日期检验，退保日期在生效日期之前全额退保，退保日期在生效日期之后不能进行线上退保
					if(this.TBBatchNo.startsWith("SZM") && !"ZZJ313".equals(this.wrapcode) && !"ZZJ314".equals(this.wrapcode)){
						System.out.println("报文中的保单号对应的产品不是美联平台的");
						errLog("报文中的保单号对应的产品不是美联平台的，不能线上退保");
						return false;
					}
					//添加保险师健康金福ZZJ214，ZZJ306产品 保险师慧心无忧ZZJ223产品退保校验，退保日期在生效日期之前全额退保
					if(this.TBBatchNo.startsWith("BXS") && !"ZZJ214".equals(this.wrapcode)&& !"ZZJ306".equals(this.wrapcode) && !"ZZJ223".equals(this.wrapcode)){
						System.out.println("报文中的保单号对应的产品不是保险师健康金福或惠心无忧");
						errLog("报文中的保单号对应的产品不是保险师健康金福或惠心无忧，不能线上退保");
						return false;
					}
					if(!this.TBBatchNo.startsWith("ZMB") && !this.TBBatchNo.startsWith("XYS")&& !this.TBBatchNo.startsWith("HZW")&& !this.TBBatchNo.startsWith("SZM")&& !this.TBBatchNo.startsWith("BXS")){
						System.out.println("报文中的保单号对应的产品未开通线上退保功能");
						errLog("报文中的保单号对应的产品未开通线上退保功能");
						return false;
					}
					
					String cvalidate = mLCContSchema.getCValiDate();
	            	String surrendDate = mEdorItemInfo.getEdorValidate();
	            	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	            	df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
	            	Date d1 = df.parse(surrendDate+" 00:00:00");
	    		    Date d2 = df.parse(cvalidate+" 00:00:00");
	    		    long diff = d2.getTime()-d1.getTime();
	    		    long days = diff/(1000 * 60 * 60 * 24);
	    		    if(days <=0){
	    		    	System.out.println("退保日期不在生效日期之前，不能进行线上退保");
	    		    	errLog("退保日期不在生效日期之前，不能进行线上退保");
	    		    	return false;
	    		    }
				}
				
			} catch (Exception e) {
				errLog("退保日期与生效日期计算失败"+e.toString());
		    	return false;
			}
        	return true;
        }
        
        /**
         * 锁定动作
         * @param cLCContSchema
         * @return
         */
        private MMap lockLGWORK(LCContSchema cLCContSchema)
        {
            MMap tMMap = null;
            /**犹豫期退保锁定标志"WT"*/
            String tLockNoType = "WT";
            /**锁定时间*/
            String tAIS = "300";//5分钟的锁
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
            tTransferData.setNameAndValue("LockNoType", tLockNoType);
            tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

            VData tVData = new VData();
            tVData.add(mGlobalInput);
            tVData.add(tTransferData);

            LockTableActionBL tLockTableActionBL = new LockTableActionBL();
            tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
            if (tMMap == null)
            {
                return null;
            }
            return tMMap;
        }    
        
        /**
         * 提交数据到数据库
         * @return boolean
         */
        private boolean submit(MMap map)
        {
            VData data = new VData();
            data.add(map);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(data, ""))
            {
            	errLog("提交数据库发生错误"+tPubSubmit.mErrors);
                return false;
            }
            return true;
        }
        
        public static void  main(String[] args) throws FileNotFoundException,
            IOException {
//            BQWTQNInfaceTask mCrmInfaceTask = new BQWTQNInfaceTask();
//            mCrmInfaceTask.DealDate();
//        	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        	Date d1 = df.parse("2018-03-25"+" 00:00:00");
//		    Date d2 = df.parse("2018-03-24"+" 00:00:00");
//		    long diff = d2.getTime()-d1.getTime();
//		    long days = diff/(1000 * 60 * 60 * 24);
//		    System.out.println(days);
		    String TBBatchNoSql = "select batchno from WXContRelaInfo where  PRTNO = (select prtno from db2inst1.lccont  where contno = '014531663000001') ";
    		SSRS TBBatchNoSSRS = new ExeSQL().execSQL(TBBatchNoSql);
    		if(TBBatchNoSSRS == null || TBBatchNoSSRS.MaxRow<=0){
    			System.out.println("没有得到投保批次号！");
    		}
    		String TBBatchNo = TBBatchNoSSRS.GetText(1, 1);
    		System.out.println("TBBatchNo:"+TBBatchNo);
    		if(TBBatchNo == null || "".equals(TBBatchNo)){
				System.out.println("投保批次号为空");
			}else{
				System.out.println("投保批次号不为空");
			}
        }

}
