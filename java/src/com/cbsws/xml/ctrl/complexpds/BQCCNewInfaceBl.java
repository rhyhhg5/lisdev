package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.EdorCCLCContBank;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPEdorMainTable;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.task.BQCCEdorBL;
import com.sinosoft.task.CommonBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class BQCCNewInfaceBl  extends ABusLogic {

	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	private EdorCCLCContBank mEdorLCContBank;
	private LGWorkTable mLGWorkTable;
	private List mLPEdorMainList;
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    private MMap map = new MMap();
    private String mCustomeNO;
	
	protected boolean deal(MsgCollection cMsgInfos) {
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkdata()){
        	return false;
        }     
        
        if(!changePolicyInfo()){
        	return false;
        }
        
		return true;
	}

	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
//		List tLCContTableList = cMsgInfos.getBodyByFlag("LCContTable");
//        if (tLCContTableList == null || tLCContTableList.size() != 1)
//        {
//            errLog("申请报文中获取保单号信息失败。");
//            return false;
//        }
//        cLCContTable = (LCContTable)tLCContTableList.get(0);
        
        List tedorLCContBank = cMsgInfos.getBodyByFlag("EdorCCLCContBank");
        if (tedorLCContBank == null || tedorLCContBank.size() != 1)
        {
            errLog("申请报文中获取保单银行信息失败。");
            return false;
        }
        mEdorLCContBank = (EdorCCLCContBank)tedorLCContBank.get(0);
        
        List tLGWorkList = cMsgInfos.getBodyByFlag("LGWorkTable");
        if(tLGWorkList == null ||tLGWorkList.size()!=1)
        {
            errLog("申请报文中获取工单信息失败。");
            return false;
        }
        mLGWorkTable=(LGWorkTable)tLGWorkList.get(0);
        
        return true;
	}
	
	private boolean checkdata(){
				
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mEdorLCContBank.getContNo());
        if(!tLCContDB.getInfo())
        {
        	errLog("没有查询到保单信息"+mEdorLCContBank.getContNo()+"。");
            return false;
        }
   
		  if ((mEdorLCContBank.getAppntBankCode()).equals("0101")) {
			    if (mEdorLCContBank.getAppntBankAccNo().length()!=19 || !isInteger(mEdorLCContBank.getAppntBankAccNo().trim())) {
			    	errLog("工商银行的账号必须是19位的数字，最后一个星号（*）不要！");
			      return false;
			    }
			  }
		 String strSQL2 = "  select appntname from lccont where contno='"+mEdorLCContBank.getContNo()+"' " ;
		 String arrResult2 = new ExeSQL().getOneValue(strSQL2);
			if(!arrResult2.equals(mEdorLCContBank.getAppntAccName())){
				  errLog("修改的银行账户名与投保人不同，请重新录入！");
			      return false;
			  }


//		if(mLGWorkTable.getAcceptWayNo()!=null && !"7".equals(mLGWorkTable.getAcceptWayNo())){
//			  errLog("工单受理途径必须为7（pad）！");
//			  return false;	  
//		}
		return true;
	}
	
    public static boolean isInteger(String str) {  
	    Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");  
	    return pattern.matcher(str).matches();  
	}
	  
	private boolean changePolicyInfo(){
		
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mEdorLCContBank.getContNo());
        if(!tLCContDB.getInfo())
        {
        	errLog("没有查询到保单信息"+mEdorLCContBank.getContNo()+"。");
            return false;
        }
        mCustomeNO = tLCContDB.getAppntNo();
        //得到工单信息
        String workNo = CommonBL.createWorkNo();
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(tLCContDB.getAppntNo());
        tLGWorkSchema.setTypeNo(mLGWorkTable.getTypeNo());
        tLGWorkSchema.setWorkNo(workNo);
        tLGWorkSchema.setContNo(mEdorLCContBank.getContNo());
        tLGWorkSchema.setApplyTypeNo(mLGWorkTable.getApplyTypeNo());
        tLGWorkSchema.setApplyName(mLGWorkTable.getApplyName());
        tLGWorkSchema.setAcceptWayNo(mLGWorkTable.getAcceptWayNo());
        tLGWorkSchema.setAcceptDate(mLGWorkTable.getAcceptDate());
        tLGWorkSchema.setRemark("线上个人保单交费方式变更接口生成");
  
          //得到关联保单
     

        LPContSchema tLPContSchema = new LPContSchema();
        tLPContSchema.setEdorNo(workNo);
        tLPContSchema.setEdorType(BQ.EDORTYPE_CC);
        tLPContSchema.setContNo(tLCContDB.getContNo());
        if(mEdorLCContBank.getAppntAccName()==null || "".equals(mEdorLCContBank.getAppntAccName()))
        {
        	 errLog("获取变更银行信息银行帐户名失败");
             return false;
        }
        else
        {
    	     tLPContSchema.setAccName(mEdorLCContBank.getAppntAccName());
        }
        
        if(mEdorLCContBank.getPayMode()==null || "".equals(mEdorLCContBank.getPayMode()))
        {
        	 errLog("获取变更银行信息缴费方式失败");
             return false;
        }
        else
        {
    	     tLPContSchema.setPayMode(mEdorLCContBank.getPayMode());
        }

        if(mEdorLCContBank.getAppntBankCode()==null || "".equals(mEdorLCContBank.getAppntBankCode()))
        {
        	 errLog("获取变更银行信息银行编码失败");
             return false;
        }
        else
        {
            tLPContSchema.setBankCode(mEdorLCContBank.getAppntBankCode());
        }
    
        if(mEdorLCContBank.getAppntBankAccNo()==null || "".equals(mEdorLCContBank.getAppntBankAccNo()))
        {
        	 errLog("获取变更银行信息银行账号失败");
             return false;
        }
        else
        {
            tLPContSchema.setBankAccNo(mEdorLCContBank.getAppntBankAccNo());
        }

      
        //操作员信息
        mGlobalInput.Operator = Operator;
        mGlobalInput.ManageCom = tLCContDB.getManageCom();
        mGlobalInput.ComCode = mGlobalInput.ManageCom;
        
        VData data = new VData();
        data.add(mCustomeNO);
        data.add(mGlobalInput);
        data.add(tLGWorkSchema);
        data.add(tLPContSchema);
        
        BQCCEdorBL tBQCCEdorBL = new BQCCEdorBL();
        if(!tBQCCEdorBL.submitData(data))
        {
            errLog("保全变更失败"+tBQCCEdorBL.mErrors.getFirstError());
            return false;
        }
        LPEdorMainTable tLPEdorMainTable = new LPEdorMainTable();
        tLPEdorMainTable.setEdorAcceptNo(workNo);
        //返回报文
    	//组织返回报文
    	getWrapParmList(tLPEdorMainTable);
    	getXmlResult();
		return true;
	}

    private void getWrapParmList(LPEdorMainTable tLPEdorMainTable){
    	mLPEdorMainList = new ArrayList();
    
    	mLPEdorMainList.add(tLPEdorMainTable);
    }
    
	//返回报文
	public void getXmlResult(){
		//返回信息
		for(int i=0;i<mLPEdorMainList.size();i++){
			putResult("LPEdorMainTable", (LPEdorMainTable)mLPEdorMainList.get(i));
		}

	}
    
}
