package com.cbsws.xml.ctrl.complexpds;


import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.jdom.Document;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.config.BusLogicCfgFactory;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.IBusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.core.xml.ctrl.pack.MsgXmlParse;
import com.cbsws.core.xml.ctrl.pack.MsgXschParse;
import com.cbsws.obj.CRM_ACCIDENT;
import com.cbsws.obj.CRM_ACCIDENT_RETURN;
import com.sinosoft.lis.certifybusiness.CertifyContConst;
import com.sinosoft.lis.certifybusiness.WSCertifyUI;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LICrmClaimSchema;
import com.sinosoft.lis.schema.LICrmLogSchema;
import com.sinosoft.lis.schema.LLAskRelaSchema;
import com.sinosoft.lis.schema.LLConsultSchema;
import com.sinosoft.lis.schema.LLMainAskSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLAskRelaSet;
import com.sinosoft.lis.vschema.LLConsultSet;
import com.sinosoft.lis.vschema.LLMainAskSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @author Crm理赔报案接口
 *
 */
public class LPMainAskBL extends ABusLogic {

	/** 批次号 */
	public String BatchNo="";
	/** 报文类型 */
	public String MsgType="";
	/** 操作者 */
	public String Operator="";
	/** 对解析xml后进行的数据封装 */
	public CRM_ACCIDENT mLPCrmAccidentTable;
	/** 公共信息 */
	public GlobalInput mGlobalInput = new GlobalInput();
	/** 通知类案件号 */
	public String mCaseNo="";
	/** 返回报文数据集合 */
	private List mLLConsultSchema;
	private VData mResult = new VData();
	
	/** 报文发送日期 */
    private String mLogDate="";
    /** 当前系统日期 */
    private String cDate = PubFun.getCurrentDate();
    /** 当前系统时间 */
    private String cTime = PubFun.getCurrentTime();
    /** 日志表流水号 */
    private String mBatchNo = PubFun1.CreateMaxNo("CRMBATCHNO", 20);
    /** 一个VData只能提交一个MMap */
    private MMap mMap = new MMap();
	
	protected boolean deal(MsgCollection cMsgInfos) {
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        
        //生成日志记录
        if(!dealCRMLog()){
        	return false;
        }
        //生成临时表数据
        if(!dealLICRMClaim()){
        	return false;
        }
        
        
        //生成理赔案件信息
        if(!dealLPMainAskInfo()){
        	return false;
        }
        
        System.out.println("Start LPMainAskBL Submit...");
        PubSubmit pubSubmit = new PubSubmit();
        if (!pubSubmit.submitData(this.mResult, "")) {
        	errLog("提交事务失败!");
            return false;
        }
        return true;
	}
	
	/**
	 * 向中间表存入数据
	 * @return
	 */
	private boolean dealLICRMClaim() {
		
		LICrmClaimSchema mLICrmClaimSchema = new LICrmClaimSchema();
        mLICrmClaimSchema.setBatchNo(mBatchNo);
        
        String tInsuredNo = "" + mLPCrmAccidentTable.getINSUREDNO();
        String tRptorName = ""+mLPCrmAccidentTable.getRPTORNAME();
        String tAskMode = ""+mLPCrmAccidentTable.getSOURCE();
        String tIdNo = ""+mLPCrmAccidentTable.getIDNO();
        String tRelation = ""+mLPCrmAccidentTable.getRELATION();
        String tInsuredName = ""+mLPCrmAccidentTable.getINSUREDNAME();
        String tRptDate = ""+mLPCrmAccidentTable.getRPTDATE();
        String tRptorPhone = ""+mLPCrmAccidentTable.getRPTORPHONE();
        String tCallNumber = ""+mLPCrmAccidentTable.getCALLNUMBER();
        String tPhone = ""+mLPCrmAccidentTable.getPHONE();
        String tRptInfo = ""+mLPCrmAccidentTable.getRPTINFO();
        String tAccidentDate = ""+mLPCrmAccidentTable.getACCIDENTDATE();
        
        System.out.println("联系电话:"+tPhone);

        mLICrmClaimSchema.setRgtType(tAskMode);
        mLICrmClaimSchema.setRptorName(tRptorName);//报案人姓名   
        mLICrmClaimSchema.setRptorSex("2");//报案人性别（CRM未传）
        mLICrmClaimSchema.setRelation(tRelation);   
        mLICrmClaimSchema.setRptorAddress("");   //报案人地址（CRM未传）   
        mLICrmClaimSchema.setRptorPhone(tRptorPhone);//报案人联系电话
        mLICrmClaimSchema.setRptorMobile(tCallNumber);//来电号码
        mLICrmClaimSchema.setEmail("");//报案人Email（CRM未传）
        mLICrmClaimSchema.setPostCode("");//报案人邮编（CRM未传）
        mLICrmClaimSchema.setRptorIDType("");//报案人证件类型（CRM未传）
        mLICrmClaimSchema.setRptorIDNo("");//报案人证件号码（CRM未传）
        mLICrmClaimSchema.setRptDate(tRptDate);
        
        if(!"".equals(tInsuredNo)){
        	mLICrmClaimSchema.setCustomerNo(tInsuredNo);
        }else{
        	mLICrmClaimSchema.setCustomerNo("000000000");
        }
       
        mLICrmClaimSchema.setCustomerName(tInsuredName);
        mLICrmClaimSchema.setCustomerSex("2");//被保人性别（CRM未传）
        mLICrmClaimSchema.setCustomerAge("");//被保人年龄（CRM未传）
        mLICrmClaimSchema.setCustomerIDType("0");//对方均传入身份证号
        mLICrmClaimSchema.setCustomerIDNo(tIdNo);
        mLICrmClaimSchema.setAccidentDate(tAccidentDate);
        mLICrmClaimSchema.setAccdentDesc(tRptInfo);
    
          
        String aSerialno = PubFun1.CreateMaxNo("CRMSERIALNO", 10);
        mLICrmClaimSchema.setSerialno(aSerialno);
        mLICrmClaimSchema.setOperator("crmNew");
        mLICrmClaimSchema.setMakeDate(cDate);
        mLICrmClaimSchema.setMakeTime(cTime);
		

        mMap.put(mLICrmClaimSchema, "INSERT");
        this.mResult.add(mMap);
		return true;
	}

	/**
	 * 生成日志记录
	 * @return
	 */
	private boolean dealCRMLog() {
        LICrmLogSchema mLICrmLogSchema = new LICrmLogSchema();
        mLICrmLogSchema.setBatchNo(mBatchNo);
        mLICrmLogSchema.setBatchDate(mLogDate);
        mLICrmLogSchema.setFileCode(MsgType);
        mLICrmLogSchema.setFileName(BatchNo);
        mLICrmLogSchema.setRecordNum("");
        mLICrmLogSchema.setMakeDate(cDate);
        mLICrmLogSchema.setMakeTime(cTime);
        mLICrmLogSchema.setOperator("crmNew");
        
        mMap.put(mLICrmLogSchema, "INSERT");
        this.mResult.add(mMap);
		return true;
	}

	/**
	 * 解析XML
	 * @param cMsgInfos
	 * @return
	 */
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("理赔：开始解析电话客服的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		mLogDate = tMsgHead.getSendDate();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		if(BatchNo==null || "".equals(BatchNo)){
            errLog("申请报文中批次号为空，请重新发送");
            return false;
		}
		String tSendDate = tMsgHead.getSendDate();
		String tSendTime = tMsgHead.getSendTime();
		if(tSendDate==null || "".equals(tSendDate)){
            errLog("申请报文中报文发送日期为空，请重新发送");
            return false;
		}
		if(tSendTime==null || "".equals(tSendTime)){
            errLog("申请报文中报文发送时间为空，请重新发送");
            return false;
		}
		
		List tCRM_ACCIDENT = cMsgInfos.getBodyByFlag("CRM_ACCIDENT");
        if (tCRM_ACCIDENT == null || tCRM_ACCIDENT.size() != 1)
        {
            errLog("申请报文中获取报案信息失败。");
            return false;
        }
        //目前只一条条处理，若需要批处理，待日后变更
        mLPCrmAccidentTable = (CRM_ACCIDENT)tCRM_ACCIDENT.get(0); 
        
        return true;
	}
	
	/**
	 * 生成理赔案件信息
	 * @return
	 */
	private boolean dealLPMainAskInfo(){

        //加入结果集
        LLMainAskSet tLLMainAskSet = new LLMainAskSet();
        LLConsultSet tLLConsultSet = new LLConsultSet();
        LLSubReportSet tLLSubReportSet = new LLSubReportSet();
        LLAskRelaSet tLLAskRelaSet = new LLAskRelaSet();

        String cMngCom = "86";
        String cOperator = "crmNew";
        
        try{
            
            LLMainAskSchema tLLMainAskSchema = new LLMainAskSchema();
            LLConsultSchema tLLConsultSchema = new LLConsultSchema();
            if(mLPCrmAccidentTable.getACCIDENTDATE()==null || mLPCrmAccidentTable.getACCIDENTDATE().equals(""))
            {
            	System.out.println("客户"+mLPCrmAccidentTable.getINSUREDNO()+"的出险日期为空！");
//            	errLog("客户"+mLPCrmAccidentTable.getINSUREDNO()+"的出险日期为空！");
//              return false;
            	
            }
            String tInsuredNo = "" + mLPCrmAccidentTable.getINSUREDNO();
            String tInsuredName = ""+mLPCrmAccidentTable.getINSUREDNAME();
            
            if("".equals(tInsuredNo) || tInsuredNo == "" || "null".equals(tInsuredNo)){
            	tInsuredNo = "000000000";
            }
            
            String tsql = "select managecom,RelationToMainInsured from lcinsured where insuredno='"
                          +tInsuredNo+"' union select managecom,RelationToMainInsured from lbinsured "
                          +"where insuredno='"+tInsuredNo+"'";
            ExeSQL texesql = new ExeSQL();
            SSRS tssrs = texesql.execSQL(tsql);
            if (tssrs.getMaxRow() <= 0) {
                System.out.println("客户信息查询失败");
//            	errLog("客户信息查询失败");
//              return false;
            } else {
                cMngCom = tssrs.GetText(1,1);
                tLLConsultSchema.setCustomerType(tssrs.GetText(1,2));
            }
            
            //#2219 客服系统接口新功能,添加卡折实名化的处理
            if(mLPCrmAccidentTable.getCARDNO()!=null && !"".equals(mLPCrmAccidentTable.getCARDNO())){
            	//进行卡折的实名化
            	String tCardNo = mLPCrmAccidentTable.getCARDNO();
                String tStrSql ="select a.State "
     			   +" from lzcard a, lzcardnumber b "
     			   +" where a.subcode = b.cardtype "
     			   +" and a.startno= b.cardserno " 
     			   +" and b.cardno = '" + tCardNo + "' " 
     			   ;
                String CardState = texesql.getOneValue(tStrSql);
		        if(CardState == "4")
		        {
		        	errLog("此卡折状态为遗失，不可进行实名化");
		            return false;
		        }
		        if(CardState == "5")
		        {
		        	errLog("此卡折状态为销毁，不可进行实名化");
		            return false;
		        }
		        if(CardState == "6")
		        {
		        	errLog("此卡折状态为作废，不可进行实名化");
		            return false;
		        }
            	
		        // 校验保险卡（折）主信息是否存在，若实名化完毕，直接跳过实名化处理
		        LICertifyDB tLICertifyDB = new LICertifyDB();
		        tLICertifyDB.setCardNo(tCardNo);
		        if (!tLICertifyDB.getInfo())
		        {
		        	errLog("保险卡（折）不存在！");
		            return false;
		        }
		        LICertifySchema mCertifyInfo = new LICertifySchema();
		        mCertifyInfo.setSchema(tLICertifyDB.getSchema());
		        //校验保险卡是否已经被实名化过
		        if (!CertifyContConst.CC_WSSTATE_UNCREATED.equals(mCertifyInfo.getWSState()))
		        {

		        }else{
		        	LICertifySchema tLICertifySchema = null;
	                tLICertifySchema = new LICertifySchema();
	                tLICertifySchema.setCardNo(tCardNo);
	                
	                VData tVData = new VData();
	                GlobalInput tGlobalInput = new GlobalInput();
	                tGlobalInput.ManageCom = "86";
	                tGlobalInput.Operator = "crmNew";
	                
	                TransferData tTransferData = new TransferData();
	                tVData.add(tLICertifySchema);
	                tVData.add(tTransferData);
	                tVData.add(tGlobalInput);
	                
	                WSCertifyUI tWSCertifyUI = new WSCertifyUI();
	                if(!tWSCertifyUI.submitData(tVData, "WSCertify"))
	                {
	                	System.out.println(" 处理失败，原因是: " + tWSCertifyUI.mErrors.getFirstError());
	                	errLog(" 处理失败，原因是: " + tWSCertifyUI.mErrors.getFirstError());
	                    return false;
	                    
	                }
		        }
		        
		        //卡折实名化成功与否，重新获取实名化后的被保险人信息
                String tCheckSql ="select a.insuredno,a.name,a.managecom "
      			   +" from lcinsured a "
      			   +" where 1=1 "
      			   +" and a.contno = '" + tCardNo + "' with ur " ;
      			   
                SSRS tAgentSSRS = texesql.execSQL(tCheckSql);
                for (int j = 1; j <= tAgentSSRS.getMaxRow(); j++) {
                	tInsuredNo = tAgentSSRS.GetText(j, 1);
                	cMngCom = tAgentSSRS.GetText(j,3);
                	System.out.println("被保险人："+tAgentSSRS.GetText(j, 2));
                	if(!StrTool.cTrim(tInsuredName).equals(tAgentSSRS.GetText(j, 2))){
    		        	errLog("此保险卡被保险人为："+tAgentSSRS.GetText(j, 2)+"，与电话报案提供的被保险人："+tInsuredName+"，不一致！");
    		            return false;
                	}else{
                		tInsuredName = tAgentSSRS.GetText(j, 2);
                	}                   	
                }          	
            }
            
            /*
             * 开始通知类案件的主表生成
             */
            String tRptorName = ""+mLPCrmAccidentTable.getRPTORNAME();
            String tAskMode = ""+mLPCrmAccidentTable.getSOURCE();
            String tIdNo = ""+mLPCrmAccidentTable.getIDNO();
            String tRelation = ""+mLPCrmAccidentTable.getRELATION();
            String tRptDate = ""+mLPCrmAccidentTable.getRPTDATE();
            String tRptTime = ""+mLPCrmAccidentTable.getRPTTIME();
            String tRptorPhone = ""+mLPCrmAccidentTable.getRPTORPHONE();
            String tCallNumber = ""+mLPCrmAccidentTable.getCALLNUMBER();
            String tPhone = ""+mLPCrmAccidentTable.getPHONE();
            String tInsuredPhone = ""+mLPCrmAccidentTable.getINSUREDPHONE();
            String tReply = ""+mLPCrmAccidentTable.getREPLY();
            String tReplyDate = ""+mLPCrmAccidentTable.getREPLYDATE();
            String tRemark = ""+mLPCrmAccidentTable.getREMARK();
            System.out.println("报案人姓名:"+tRptorName+";报案人与被保险人关系（详见代码对照表）:"
            		+tRelation+";报案人联系电话:"+tRptorPhone+";联系电话:"+tPhone+";来电号码:"+tCallNumber);
            
            if(!validateDate(tRptDate) && !"".equals(tRptDate)){
                System.out.println("报案日期格式错误，应为‘yyyy-mm-dd’");
            	errLog("报案日期格式错误，应为‘yyyy-mm-dd’");
            	return false;
            }
            if(!validateDate(tReplyDate) && !"".equals(tReplyDate)){
                System.out.println("回复期限格式错误，应为‘yyyy-mm-dd’");
            	errLog("回复期限格式错误，应为‘yyyy-mm-dd’");
            	return false;
            }
            if(!validateTime(tRptTime) && !"".equals(tRptTime)){
                System.out.println("报案时间格式错误，应为‘HH:mm:ss’");
            	errLog("报案时间格式错误，应为‘HH:mm:ss’");
            	return false;
            }
            
            String tLimit = PubFun.getNoLimit(cMngCom);
            String evNo = PubFun1.CreateMaxNo("SERIALNO", "");
            String CNo = "";
            tLLMainAskSchema.setLogNo(evNo);
            tLLMainAskSchema.setAskType("1");//0咨询:咨询和通知产生登记号;1通知;2即有咨询也有通知;3健康咨询(用于健康管理中)
            tLLMainAskSchema.setMngCom(cMngCom);
            tLLMainAskSchema.setOperator(cOperator);
            tLLMainAskSchema.setMakeDate(cDate);
            tLLMainAskSchema.setMakeTime(cTime);
            tLLMainAskSchema.setModifyDate(cDate);
            tLLMainAskSchema.setModifyTime(cTime);
            tLLMainAskSchema.setLogState("0");//0-提起询问，未进行回复
            tLLMainAskSchema.setAskMode(tAskMode);
            
            tLLMainAskSchema.setOtherNoType("0");//对方均传入身份证号
            tLLMainAskSchema.setOtherNo(tIdNo);
        	tLLMainAskSchema.setLogerNo(tInsuredNo);
        	tLLMainAskSchema.setLogName(tInsuredName);
        	
            tLLMainAskSchema.setLogDate(tRptDate);
            tLLMainAskSchema.setLogTime(tRptTime);
            tLLMainAskSchema.setPhone(tInsuredPhone);//被保险人联系电话，暂存在PostCode中
            
            if("Y".equals(tReply)){
                tLLMainAskSchema.setAnswerType("01"); //即时回复
                tLLMainAskSchema.setAnswerMode("9");//答复方式
                tLLMainAskSchema.setCNSDate(tReplyDate);//服务时间，同“回复期限”
                tLLMainAskSchema.setReplyFDate(tReplyDate);
                tLLMainAskSchema.setDealFDate(tReplyDate);
            }

            tLLMainAskSchema.setSwitchCom("LP");//转入机构，存为LP
            tLLMainAskSchema.setSwitchDate(cDate);
            tLLMainAskSchema.setSwitchTime(cTime);
            tLLMainAskSchema.setDespatcher(cOperator);
            tLLMainAskSchema.setCNSOperator(cOperator);
            tLLMainAskSchema.setAvaiFlag(tReply);//咨询有效标志，暂存“是否需要回复”
            tLLMainAskSchema.setRemark(tRemark);//备注

            /*
             * 开始通知类案件的详细生成
             */
            String tRptInfo = ""+mLPCrmAccidentTable.getRPTINFO();
            String tAccidentDate = ""+mLPCrmAccidentTable.getACCIDENTDATE();
            String tStation = ""+mLPCrmAccidentTable.getSTATION();
            
            if(!validateDate(tAccidentDate)){
                System.out.println("出险日期格式错误，应为‘yyyy-mm-dd’");
            	errLog("出险日期格式错误，应为‘yyyy-mm-dd’");
            	return false;
            }
            
            CNo = PubFun1.CreateMaxNo("NOTICENO", tLimit);
            mCaseNo = CNo;
            tLLConsultSchema.setConsultNo(CNo);
            tLLConsultSchema.setLogNo(evNo);
            tLLConsultSchema.setMngCom(cMngCom);
            tLLConsultSchema.setOperator(cOperator);
            tLLConsultSchema.setMakeDate(cDate);
            tLLConsultSchema.setMakeTime(cTime);
            tLLConsultSchema.setModifyDate(cDate);
            tLLConsultSchema.setModifyTime(cTime);
            tLLConsultSchema.setCustomerNo(tInsuredNo);
            tLLConsultSchema.setCustomerName(tInsuredName);
            tLLConsultSchema.setCSubject("");//咨询主题
            tLLConsultSchema.setCContent(tRptInfo);//咨询内容
            tLLConsultSchema.setReplyState("0");//回复状态
            tLLConsultSchema.setAskGrade("1");
            tLLConsultSchema.setInHospitalDate(tAccidentDate);//入院日期，同“出险日期”
            tLLConsultSchema.setCustStatus(tStation);//客户现状
            tLLConsultSchema.setAvaiFlag(tReply);//咨询有效标志

            /*
             * 开始通知类案件的事件生成
             */
            String tAccidentAddress = ""+mLPCrmAccidentTable.getACCIDENTADDRESS();
            /**
             * #3512 客服接口新增发生地点省、市、县信息
             * 
             */
            String tProvinceCode =""+ mLPCrmAccidentTable.getPROVINCECODE();    //发生地点省
            String tCityCode ="" +mLPCrmAccidentTable.getCITYCODE();	//发生地点市
            String tAreaCode ="" +mLPCrmAccidentTable.getAREACODE();    // 发生地点县
            //校验发生地点信息
            String tAccCode=LLCaseCommon.checkAccPlace(tProvinceCode,tCityCode,tAreaCode); 
            if(!"".equals(tAccCode)){
            	System.out.println("======事件发生地点省、市、县有错误:"+tAccCode);
            	errLog("案件信息："+tAccCode);
            	return false;
            }
            
            String SubRptNo = "";
            LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
            SubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
            tLLSubReportSchema.setSubRptNo(SubRptNo);
            tLLSubReportSchema.setMngCom(cMngCom);
            tLLSubReportSchema.setOperator(cOperator);
            tLLSubReportSchema.setMakeDate(cDate);
            tLLSubReportSchema.setMakeTime(cTime);
            tLLSubReportSchema.setModifyDate(cDate);
            tLLSubReportSchema.setModifyTime(cTime);
            tLLSubReportSchema.setCustomerNo(tLLConsultSchema.getCustomerNo());
            tLLSubReportSchema.setCustomerName(tLLConsultSchema.getCustomerName());
            tLLSubReportSchema.setCustomerType(tLLConsultSchema.getCustomerType());//关联客户类型
            tLLSubReportSchema.setAccDate(tAccidentDate);
            tLLSubReportSchema.setAccDesc(tRptInfo);//事故描述
            tLLSubReportSchema.setAccPlace(tAccidentAddress);//事故地点
            tLLSubReportSchema.setAccProvinceCode(tProvinceCode);  // 发生地点(省)
            tLLSubReportSchema.setAccCityCode(tCityCode);		//发生地点(市)
            tLLSubReportSchema.setAccCountyCode(tAreaCode);   	//发生地点(县)
            
            /*
             * 开始通知类案件的事件关联表生成
             */
            LLAskRelaSchema tLLAskRelaSchema = new LLAskRelaSchema();
            tLLAskRelaSchema.setConsultNo(CNo);
            tLLAskRelaSchema.setSubRptNo(SubRptNo);

            tLLMainAskSet.add(tLLMainAskSchema);
            tLLConsultSet.add(tLLConsultSchema);
            tLLSubReportSet.add(tLLSubReportSchema);
            tLLAskRelaSet.add(tLLAskRelaSchema);

        }catch(Exception ex){
        	errLog("解析数据出现未知异常，请重新核对数据");
            return false;
        }
    
        mMap.put(tLLMainAskSet,"INSERT");
        mMap.put(tLLConsultSet,"INSERT");
        mMap.put(tLLSubReportSet,"INSERT");
        mMap.put(tLLAskRelaSet,"INSERT");

        //更新时间
        this.mResult.add(mMap);
    
        //返回报文
    	//组织返回报文
        CRM_ACCIDENT_RETURN aLPCrmAccidentTable = new CRM_ACCIDENT_RETURN();
        aLPCrmAccidentTable.setACCIDENTNO(mCaseNo);
        
    	getWrapParmList(aLPCrmAccidentTable);
    	getXmlResult();
		return true;
	}

    private void getWrapParmList(CRM_ACCIDENT_RETURN aLPCrmAccidentTable){
    	mLLConsultSchema = new ArrayList();
    	mLLConsultSchema.add(aLPCrmAccidentTable);
    }
    
	//返回报文
	public void getXmlResult(){
		//返回退费信息
		for(int i=0;i<mLLConsultSchema.size();i++){
			putResult("CRM_ACCIDENT_RETURN", (CRM_ACCIDENT_RETURN)mLLConsultSchema.get(i));
		}

	}
	
	/**
	 * 校验日期
	 * @param strDate
	 * @return
	 */
	public boolean validateDate(String strDate)
    {
        int yyyy = 0000;
        int mm = 00;
        int dd = 00;
        // 校验是否有非法字符
        if (!PubFun.validateNumber(strDate))
        {
            return false;
        }

        if (strDate.indexOf("-") >= 0)
        {
            StringTokenizer token = new StringTokenizer(strDate, "-");
            int i = 0;
            while (token.hasMoreElements())
            {
                if (i == 0)
                {
                    yyyy = Integer.parseInt(token.nextToken());
                }
                if (i == 1)
                {
                    mm = Integer.parseInt(token.nextToken());
                }
                if (i == 2)
                {
                    dd = Integer.parseInt(token.nextToken());
                }
                i++;
            }
        }
        else
        {
            if (strDate.length() != 8)
            {
                return false;
            }
            yyyy = Integer.parseInt(strDate.substring(0, 4));
            mm = Integer.parseInt(strDate.substring(4, 6));
            dd = Integer.parseInt(strDate.substring(6, 8));
        }
        if (yyyy > 9999 || yyyy < 1800)
        {
            return false;
        }
        if (mm > 12 || mm < 1)
        {
            return false;
        }
        if (dd > 31 || dd < 1)
        {
            return false;
        }
        if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && (dd == 31))
        {
            return false;
        }
        if (mm == 2)
        {
            // 校验闰年的情况下__日期格式
            boolean leap = (yyyy % 4 == 0 && (yyyy % 100 != 0 || yyyy % 400 == 0));
            if (dd > 29 || (dd == 29 && !leap))
            {
                return false;
            }
        }

        return true;
    }
	
	/**
	 * 校验时间
	 * @param strDate
	 * @return
	 */
	public boolean validateTime(String strDate)
    {
        int hh = 00;
        int mm = 00;
        int ss = 00;
        // 校验是否有非法字符
        String TIME_LIST="0123456789:";
        String tmp = null;
        for (int i = 0; i < strDate.length(); i++)
        {
            tmp = strDate.substring(i, i + 1);
            if (TIME_LIST.indexOf(tmp) == -1)
            {
                return false;
            }
        }

        if (strDate.indexOf(":") >= 0)
        {
            StringTokenizer token = new StringTokenizer(strDate, ":");
            int i = 0;
            while (token.hasMoreElements())
            {
                if (i == 0)
                {
                	hh = Integer.parseInt(token.nextToken());
                }
                if (i == 1)
                {
                    mm = Integer.parseInt(token.nextToken());
                }
                if (i == 2)
                {
                	ss = Integer.parseInt(token.nextToken());
                }
                i++;
            }
        }
        else
        {
            if (strDate.length() != 8)
            {
                return false;
            }
            hh = Integer.parseInt(strDate.substring(0, 4));
            mm = Integer.parseInt(strDate.substring(4, 6));
            ss = Integer.parseInt(strDate.substring(6, 8));
        }
        if (hh > 23 || hh < 0)
        {
            return false;
        }
        if (mm > 59 || mm < 0)
        {
            return false;
        }
        if (ss > 59 || ss < 0)
        {
            return false;
        }

        return true;
    }

	public static void main(String[] args) throws Exception {
		
		LPMainAskBL tLPMainAskBL = new LPMainAskBL();
		MsgCollection cMsgInfos;
		MsgCollection tOutMsgInfos =null;
		MsgXmlParse tMsgXParse = new MsgXmlParse();
		Document tInXmlDoc;    
        tInXmlDoc = JdomUtil.build(new FileInputStream("E:\\理赔报文\\客服接口\\KF-01.xml"), "GBK");
        cMsgInfos=tMsgXParse.deal(tInXmlDoc);
     // 业务处理
        System.out.println("执行PubserviceApi方法");
        System.out.println("报文格式如下：");
        System.out.println(cMsgInfos.toString());
        try
        {
            IBusLogic tBusLogic = BusLogicCfgFactory.getNewInstanceBusLogic(cMsgInfos);
            if (tBusLogic == null)
            {
                System.out.println("程序异常，加载业务处理类失败。");
               
            }
            System.out.println("实例化tBusLogic完成");
            tOutMsgInfos = tBusLogic.service(cMsgInfos);
        }
        catch (Exception e)
        {
            System.out.println("异常了");
            System.out.println(e.getStackTrace());
            System.out.println(e.getMessage());
//            return null;
        }

        Document tOutDoc = null;
        MsgXschParse tMsgXschParse = new MsgXschParse();
        tOutDoc = tMsgXschParse.deal(tOutMsgInfos);
        System.out.println("打印传入报文============");
        JdomUtil.print(tInXmlDoc.getRootElement());
        System.out.println("打印传出报文============");
        JdomUtil.print(tOutDoc);
	}
    
}
