package com.cbsws.xml.ctrl.complexpds;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.ContQueryInfo; 
import com.cbsws.obj.EdorAppntInfo;
import com.cbsws.obj.EdorBnfInfo;
import com.cbsws.obj.EdorContInfo;
import com.cbsws.obj.EdorInsuInfo;
import com.cbsws.obj.EdorRiskInfo;
import com.cbsws.obj.BQxqQueryInfo;
import com.cbsws.obj.RBQxqQueryInfo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public class PadBQXQQueryInfo extends ABusLogic{

	public String mStartDate = "";
	public String mEndDate = "";
	public String mPaytoDate = "";
	public String mAgentCode = "";
	public String MsgType="";
	public String BatchNo = "";
	private BQxqQueryInfo mBqxqQueryInfo =null;
	String tsql="";


	public CErrors mErrors = new CErrors();
	
	public PadBQXQQueryInfo() {
		this.MsgType = "XQ_001";
	}
	
    public boolean deal(MsgCollection cMsgInfos)  {
    	//解析XML
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkData()){
        	System.out.println("数据校验失败---");
        	//组织返回报文
		//	getWrapParmList("fail");
        	
        	return false;
        }
        
        //组织返回报文
        try {
			getWrapParmList("success");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;

        
    }
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口XML");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		//获取查询信息
		List xqQueryList= cMsgInfos.getBodyByFlag("BQxqQueryInfo");
		 if (xqQueryList == null || xqQueryList.size() != 1)
        {
            errLog("申请报文中获取信息失败。");
            return false;
        }
		 System.out.println(xqQueryList.get(0));
        mBqxqQueryInfo =  (BQxqQueryInfo)xqQueryList.get(0);
        mStartDate=mBqxqQueryInfo.getStartDate();
        mEndDate=mBqxqQueryInfo.getEndDate();
        mAgentCode=mBqxqQueryInfo.getAgentCode();
        mPaytoDate=mBqxqQueryInfo.getPaytoDate();
		return true;
	}
	
	public boolean checkData() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = null;
		Date d2 = null;
		try {
			if(mStartDate!= null && !mStartDate.equals(""))
		    d1 = sdf.parse(mStartDate);
			if(mEndDate!= null && !mEndDate.equals(""))
			d2 =  sdf.parse(mEndDate);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		if("".equals(mAgentCode) || mAgentCode == null){
            errLog("业务员代码不能为空。");
            return false;
		}
		
		if("".equals(mPaytoDate)||mPaytoDate == null){
			
			if(!"".equals(mStartDate) && mStartDate != null && !"".equals(mEndDate) && mEndDate != null){
				tsql+="'"+mStartDate+"'<=c.lastpaytodate and c.lastpaytodate <= '"+mEndDate+"'";
			}
			else if("".equals(mStartDate)&& "".equals(mEndDate)){
				 errLog("应交日期，起始日期和终止日期不能同时为空");
		         return false;
			}
			else{
				 errLog("应交日期为空时,起始日期和终止日期不能为空");
		         return false;
			}
			int days = PubFun.calInterval(d1,d2,"D");
			if(days>92){
				errLog("起始日期和终止日期间隔不能超过3个月");
		         return false;
			}
			
			
		}else{
			tsql +="c.lastpaytodate='"+mPaytoDate+"'";
		}
		
		return true;
	}
	
	public boolean getWrapParmList(String tFlag) {
		if("success".equals(tFlag)){
			
			String contdatasql = "select  a.ContNo,a.AppntName,c.riskcode,"
					+"(select riskname from lmriskapp where riskcode = c.riskcode)," 
					+"(select mobile from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno )," 
					+"(select (case when phone is null then homephone else phone end) from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno)phone,"  
					+"(select PostalAddress from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno )," 
					+" c.SumDuePayMoney,c.lastpaytodate,"
					+" (select paymoney from ljtempfee where c.GetNoticeNo = tempfeeno and c.riskcode = riskcode),(select confdate from ljapay where GetNoticeNo = c.GetNoticeNo  order by makedate  fetch first 1 rows only),"
					+" (select codename from ldcode where codetype='paymode' and code=a.PayMode), "
					+" (select codename  from ldcode where codetype='dealstate' and code=c.dealstate)"
					+" from LCPol a ,ljspaypersonB c "
					+" where  c.agentcode=(select agentcode  from laagent ag where   ag.groupagentcode='"+mAgentCode+"' fetch first 1 rows only) and   a.ContNo =c.contno and a.riskcode = c.riskcode "
					+"   and a.conttype='1' and "
					+tsql					
					+ " order by contno,riskcode";
	


			ExeSQL tExeSQL = new ExeSQL();
			SSRS dataResult;


			dataResult = tExeSQL.execSQL(contdatasql);
		    
			if (dataResult.getMaxRow() > 0) {
				for(int i =1;i<=dataResult.getMaxRow();i++){
					
					RBQxqQueryInfo tBqxqQueryInfo=new RBQxqQueryInfo();
					
					tBqxqQueryInfo.setContno(dataResult.GetText(i, 1));
					tBqxqQueryInfo.setAppntname(dataResult.GetText(i, 2));
					tBqxqQueryInfo.setRiskcode(dataResult.GetText(i, 3));
					tBqxqQueryInfo.setRiskname(dataResult.GetText(i, 4));
					tBqxqQueryInfo.setMobile(dataResult.GetText(i, 5));
					tBqxqQueryInfo.setPhone(dataResult.GetText(i, 6));
					tBqxqQueryInfo.setAddress(dataResult.GetText(i, 7));
					tBqxqQueryInfo.setPaymoney(dataResult.GetText(i, 8));
					tBqxqQueryInfo.setPaytodate(dataResult.GetText(i, 9));
					tBqxqQueryInfo.setTpaymoney(dataResult.GetText(i, 10));
					tBqxqQueryInfo.setTpaydate(dataResult.GetText(i, 11));
					tBqxqQueryInfo.setPaymode(dataResult.GetText(i, 12));
					tBqxqQueryInfo.setFalg(dataResult.GetText(i, 13));
					putResult("RBQxqQueryInfo",tBqxqQueryInfo);
				}
			}else{
				errLog("没有查询到数据");
				return false;
			}
			
		
		}
//		else if("fail".equals(tFlag)){
//
//
//    	}
		return true;
	}
}
