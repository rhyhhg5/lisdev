package com.cbsws.xml.ctrl.complexpds;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.BackParamInfo;
import com.cbsws.obj.EdorAppInfo;
import com.cbsws.obj.EdorItemInfo;
import com.cbsws.obj.EndorsementInfo;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPEdorItemTable;
import com.cbsws.obj.ParamInfo;
import com.cbsws.obj.PayInfo;
import com.cbsws.obj.PolicyInfo;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BQCTInfaceBL;
import com.sinosoft.lis.bq.BQWTInfaceBL;
import com.sinosoft.lis.bq.BQWTQNInfaceBL;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BQCTInfaceTask extends ABusLogic{
	
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public String mEdorAcceptNo;
	public EdorAppInfo mEdorAppInfo;
	private EdorItemInfo mEdorItemInfo;
	private LCContSchema mLCContSchema;
	private PayInfo mPayInfo;
	private EndorsementInfo mEndorsementInfo;
	
	
	
        public BQCTInfaceTask() {
        }
        
        
        public boolean deal(MsgCollection cMsgInfos)  {
        	//解析XML
	        if(!parseXML(cMsgInfos)){
	        	return false;
	        }
	        //先进行下必要的数据校验
	        if(!checkData()){
	        	System.out.println("数据校验失败---");
            	//组织返回报文
	        	getWrapParmList(null,"fail");
            	return false;
	        }

	        mGlobalInput.ManageCom = mEdorItemInfo.getManageCom();
	        mGlobalInput.ComCode = mEdorItemInfo.getManageCom();
	        mGlobalInput.Operator = Operator;
            VData data = new VData();
            data.add(mGlobalInput);
            data.add(mEdorAppInfo);
            data.add(mLCContSchema);
            data.add(mEdorItemInfo);
            data.add(mPayInfo);
            BQCTInfaceBL tBQCTInfaceBL = new BQCTInfaceBL();
            if(!tBQCTInfaceBL.submit(data)){
            	System.out.println("保全操作错误");
            	errLog(""+tBQCTInfaceBL.mErrors.getFirstError());
            	//组织返回报文
    			getWrapParmList(null,"fail");
            	return false;
            }else{
            	//生成报文返回
            	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
            	tLPEdorItemSchema =tBQCTInfaceBL.getEdorItem(); 
            	//组织返回报文
            	getWrapParmList(tLPEdorItemSchema,"success");
            	return true;
            }
            
        }
        
		private boolean parseXML(MsgCollection cMsgInfos){
        	System.out.println("开始解约接口XML");
    		MsgHead tMsgHead = cMsgInfos.getMsgHead();
    		BatchNo = tMsgHead.getBatchNo();
    		MsgType = tMsgHead.getMsgType();
    		Operator = tMsgHead.getSendOperator();
    		//获取保单信息
			List tEdorAppList = cMsgInfos.getBodyByFlag("EdorAppInfo");
			
	        if (tEdorAppList == null || tEdorAppList.size() != 1)
	        {
	            errLog("申请报文中获取工单信息失败。");
	            return false;
	        }
	        mEdorAppInfo = (EdorAppInfo)tEdorAppList.get(0);  //接口一次只接受一个保单的犹豫期退保
	        //获取工单信息
	        List tEdorItemList = cMsgInfos.getBodyByFlag("EdorItemInfo");
	        if (tEdorItemList == null || tEdorItemList.size() != 1)
	        {
	            errLog("申请报文中获取保全项目信息失败。");
	            return false;
	        }
	        mEdorItemInfo = (EdorItemInfo)tEdorItemList.get(0);
	        
	        //获取其他参数信息
	        List tPayInfoList = cMsgInfos.getBodyByFlag("PayInfo");
	        if (tPayInfoList == null || tPayInfoList.size() != 1)
	        {
	            errLog("申请报文中获取保全项目信息失败。");
	            return false;
	        }
	        mPayInfo = (PayInfo)tPayInfoList.get(0); 
	        
        	//查询保单信息
        	LCContDB tLCContDB = new LCContDB();
        	tLCContDB.setContNo(mEdorItemInfo.getContNo());
        	if(!tLCContDB.getInfo()){
	            errLog("传入的保单号错误，核心系统不存在保单“"+mEdorItemInfo.getContNo()+"”");
	            return false;        		
        	}
        	mLCContSchema = tLCContDB.getSchema();
	        
        	//增加并发控制，同一个保单只能请求一次
        	MMap tCekMap = null;
        	tCekMap = lockLGWORK(mLCContSchema);
        	if (tCekMap == null)
        	{
        		errLog("保单"+mLCContSchema.getContNo()+"正在进行解约，请不要重复请求");	
        		return false;
        	}
        	if(!submit(tCekMap)){
        		errLog("保单"+mLCContSchema.getContNo()+"正在进行解约，请不要重复请求");	
        		return false;
        	}
	        return true;
        }
        
        private void getWrapParmList(LPEdorItemSchema edoritem,String ztFlag){
        	//判断失败还是成功
        	if("success".equals(ztFlag)&&edoritem!=null){
        		//查询付费通知书号以及金额
        		LJAGetDB tLJAGetDB = new LJAGetDB();
        		LJAGetSet tLJAGetSet = new LJAGetSet();
        		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        		tLJAGetDB.setOtherNo(edoritem.getEdorAcceptNo());
        		tLJAGetSet = tLJAGetDB.query();
        		if(tLJAGetSet!=null&&tLJAGetSet.size()>0){
        			tLJAGetSchema = tLJAGetSet.get(1);
        		}
        		mEndorsementInfo =new EndorsementInfo();
        		mEndorsementInfo.setEdorNo(edoritem.getEdorNo());
        		mEndorsementInfo.setEdorType(edoritem.getEdorType());
        		mEndorsementInfo.setContNo(edoritem.getContNo());
        		mEndorsementInfo.setAppntName(mLCContSchema.getAppntName());
        		mEndorsementInfo.setEdorValidate(edoritem.getEdorValiDate());
        		mEndorsementInfo.setEdorApplyDate(edoritem.getEdorAppDate());
        		mEndorsementInfo.setEdorConfdate(PubFun.getCurrentDate());
        		mEndorsementInfo.setSumGetMoney(String.valueOf(Math.abs(tLJAGetSchema.getSumGetMoney())));
        		
        		putResult("EndorsementInfo",mEndorsementInfo);
        		
        		String str="select polno,riskcode, "
        				 + " (select riskname from lmriskapp where riskcode=a.riskcode),"
        				 + " a.insuredname,"
        				 + " (select abs(sum(getmoney)) from ljagetendorse where endorsementno=a.edorno and polno=a.polno )"
        				 + " from lbpol a where edorno='"+edoritem.getEdorNo()+"' "
        				 + " and contno='"+edoritem.getContNo()+"' "
        				 + " with ur";
        		SSRS tSSRS=new ExeSQL().execSQL(str);
        		for(int i=1; i<=tSSRS.MaxRow; i++){
        			PolicyInfo tPolicyInfo=new PolicyInfo();
        			tPolicyInfo.setPolNo(tSSRS.GetText(i, 1));
        			tPolicyInfo.setRiskCode(tSSRS.GetText(i, 2));
        			tPolicyInfo.setRiskName(tSSRS.GetText(i, 3));
        			tPolicyInfo.setInsuredName(tSSRS.GetText(i, 4));
        			tPolicyInfo.setGetMoney(tSSRS.GetText(i, 5));
        			putResult("PolicyInfo",tPolicyInfo);
        		}
        		
        		mPayInfo = new PayInfo();
        		mPayInfo.setPayMode(tLJAGetSchema.getPayMode());
        		mPayInfo.setActuGetno(tLJAGetSchema.getActuGetNo());
        		mPayInfo.setAccName(tLJAGetSchema.getAccName());
        		mPayInfo.setAccNo(tLJAGetSchema.getBankAccNo());
        		mPayInfo.setBankCode(tLJAGetSchema.getBankCode());
        		mPayInfo.setPayDate(tLJAGetSchema.getStartGetDate());
        		
        		putResult("PayInfo",mPayInfo);
        	}else if("fail".equals(ztFlag)){
        		mEndorsementInfo =new EndorsementInfo();
        		putResult("EndorsementInfo",mEndorsementInfo);
        		
        		PolicyInfo tPolicyInfo=new PolicyInfo();
        		putResult("PolicyInfo",tPolicyInfo);
        		
        		mPayInfo = new PayInfo();
        		putResult("PayInfo",mPayInfo);
        	}
        }
        
        private boolean checkData(){
        	/**  对保全申请信息的校验*/
        	if(null == mEdorAppInfo.getCustomerNo() || "".equals(mEdorAppInfo.getCustomerNo())){
        		errLog("申请人客户号不能为空");	
        		return false;
        	}
        	if(null == mEdorAppInfo.getTypeNo() || "".equals(mEdorAppInfo.getTypeNo())){
        		errLog("申请人类型不能为空");	
        		return false;
        	}
        	if(null == mEdorAppInfo.getTypeNo() || "".equals(mEdorAppInfo.getTypeNo())){
        		errLog("工单子业务类型不能为空");	
        		return false;
        	}
        	if(null == mEdorAppInfo.getAcceptWayNo() || "".equals(mEdorAppInfo.getAcceptWayNo())){
        		errLog("工单受理途径不能为空");	
        		return false;
        	}
        	/**  对保全项目信息的校验*/
        	if(null == mEdorItemInfo.getEdorType() || "".equals(mEdorItemInfo.getEdorType())){
        		errLog("保全项目代码不能为空");	
        		return false;
        	}
        	if(null == mEdorItemInfo.getContNo() || "".equals(mEdorItemInfo.getContNo())){
        		errLog("保单号不能为空");	
        		return false;
        	}
    		LCContSchema tLCContSchema = new LCContSchema();
    		LCContDB tLCContDB = new LCContDB();
    		tLCContDB.setContNo(mEdorItemInfo.getContNo());
    		tLCContSchema = tLCContDB.query().get(1);
    		if(tLCContSchema == null){
    			errLog("没有查询到该保单号的有效保单！");
    			return false;
    		}
    		//获取保单生效日
    		String cvalidate = tLCContSchema.getCValiDate();
        	if(null == mEdorItemInfo.getManageCom() || "".equals(mEdorItemInfo.getManageCom())){
        		errLog("保单管理机构不能为空");	
        		return false;
        	}
        	if(null == mEdorItemInfo.getBudGet() || "".equals(mEdorItemInfo.getBudGet())){
        		errLog("试算金额不能为空");	
        		return false;
        	}
        	if(null == mEdorItemInfo.getEdorValidate() || "".equals(mEdorItemInfo.getEdorValidate())){
        		errLog("保全生效日期不能为空");	
        		return false;
        	}
//    		if(CommonBL.stringToDate(mEdorItemInfo.getEdorValidate()).before(PubFun.calDate(CommonBL.stringToDate(PubFun.getCurrentDate()), -10, "D", null))){
//    			errLog("保全生效日期不能早于当前日期超过10天！");
//    			return false;
//    		}
    		if(CommonBL.stringToDate(mEdorItemInfo.getEdorValidate()).before(CommonBL.stringToDate(cvalidate))){
    			errLog("保全生效日期不能早于保单生效日期！");
    			return false;
    		}
    		if(CommonBL.stringToDate(PubFun.getCurrentDate()).before(CommonBL.stringToDate(mEdorItemInfo.getEdorValidate()))){
    			errLog("保全生效日期不能晚于当前日期！");
    			return false;
    		}
        	/**  对付费信息的校验*/
        	String payMode=mPayInfo.getPayMode();
        	if(null == payMode || "".equals(payMode)){
        		errLog("付费方式不能为空");	
        		return false;
        	}
        	if("4".equals(payMode)){
	        	if(null == mPayInfo.getBankCode() || "".equals(mPayInfo.getBankCode())){
	        		errLog("付费方式为银行转账，转账银行不能为空");	
	        		return false;
	        	}
	        	if(null == mPayInfo.getAccNo() || "".equals(mPayInfo.getAccNo())){
	        		errLog("付费方式为银行转账，转账账号不能为空");	
	        		return false;
	        	}
	        	if(null == mPayInfo.getAccName() || "".equals(mPayInfo.getAccName())){
	        		errLog("付费方式为银行转账，转账户名不能为空");	
	        		return false;
	        	}
        	}
        	if(!BQ.EDORTYPE_CT.equals(mEdorItemInfo.getEdorType())){
        		errLog("申请的保全类型与请求不符");
        		return false;
        	}
        	return true;
        }
        
        /**
         * 锁定动作
         * @param cLCContSchema
         * @return
         */
        private MMap lockLGWORK(LCContSchema cLCContSchema)
        {
        	//犹豫期、解约接口都用WT来校验锁，以免保单同时申请WT和CT
            MMap tMMap = null;
            /**犹豫期退保锁定标志"WT"*/
            String tLockNoType = "WT";
            /**锁定时间*/
            String tAIS = "600";//10分钟的锁
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
            tTransferData.setNameAndValue("LockNoType", tLockNoType);
            tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

            VData tVData = new VData();
            tVData.add(mGlobalInput);
            tVData.add(tTransferData);

            LockTableActionBL tLockTableActionBL = new LockTableActionBL();
            tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
            if (tMMap == null)
            {
                return null;
            }
            return tMMap;
        }    
        
        /**
         * 提交数据到数据库
         * @return boolean
         */
        private boolean submit(MMap map)
        {
            VData data = new VData();
            data.add(map);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(data, ""))
            {
            	errLog("提交数据库发生错误"+tPubSubmit.mErrors);
                return false;
            }
            return true;
        }
        
        public static void  main(String[] args) throws FileNotFoundException,
            IOException {
//            BQWTQNInfaceTask mCrmInfaceTask = new BQWTQNInfaceTask();
//            mCrmInfaceTask.DealDate();
        }

}
