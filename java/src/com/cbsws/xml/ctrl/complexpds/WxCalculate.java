package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;

import com.cbsws.obj.*;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;

public class WxCalculate extends ABusLogic {
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public double mPrem=0;//保费
	public double mAmnt=0;//保额
	
	public LCContTable cLCContTable;
	public LCAppntTable cLCAppntTable;
	public List cLCInsuredList;
	public WrapTable cWrapTable;
	public List cLCBnfList;
	public List cWrapParamList;//套餐参数问题
	public ECCertSalesPayInfoTable cECCertSalesPayInfo;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	String cFeeRate = "";
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始处理网销复杂产品试算业务");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
	        if (tLCContList == null || tLCContList.size() != 1)
	        {
	            errLog("获取保单信息失败。");
	            return false;
	        }
	        cLCContTable = (LCContTable) tLCContList.get(0); //获取保单信息，只有一个保单
	        mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();;

			List tWrapList = cMsgInfos.getBodyByFlag("WrapTable");
        	if (tWrapList == null || tWrapList.size() != 1)
            {
                errLog("试算或核保时，获取套餐编码失败。");
                return false;
            }
        	cWrapTable = (WrapTable)tWrapList.get(0);
        	cRiskWrapCode = cWrapTable.getRiskWrapCode();
        	if (cRiskWrapCode == null || "".equals(cRiskWrapCode))
            {
                errLog("在报文中获取套餐编码失败。");
                return false;
            }
        	WxCommon tWxCommon = new WxCommon(cRiskWrapCode);
        	if(!tWxCommon.checkTheOne(tWrapList)){
        		errLog("每次投保只能购买一个产品。");
                return false;
        	}
//        	根据投保单申请日期校验产品是否已停售  试算时暂不需添加
//        	mError = tWxCommon.checkWrapStop(cLCContTable.getPolApplyDate());
//        	if (!"".equals(mError))
//            {
//                errLog(mError);
//                return false;
//            }
        	List tLCAppntList = cMsgInfos.getBodyByFlag("LCAppntTable");
            if (tLCAppntList == null || tLCAppntList.size() != 1)
            {
                errLog("获取投保人信息失败。");
                return false;
            }
            cLCAppntTable = (LCAppntTable)tLCAppntList.get(0);//获取投保人信息，每个保单只有一个投保人
            cLCInsuredList = cMsgInfos.getBodyByFlag("LCInsuredTable");//获取被保人
            if (cLCInsuredList == null)
            {
                errLog("获取被保人信息失败。");
                return false;
            }
            if(!tWxCommon.checkTheOne(cLCInsuredList)){
        		errLog("该产品的被保人只能为一人！");
                return false;
        	}

//	            校验投保人职业类型
//            if(!tWxCommon.checkOccupationType(cLCInsuredList)){
//            	errLog("投保人职业类别不在该产品所要的职业类别内！");
//                return false;
//            }
            cLCBnfList = cMsgInfos.getBodyByFlag("LCBnfTable");
//            if (cLCBnfList == null)
//            {
//                errLog("获取受益人信息失败。");
//                return false;
//            }
            cWrapParamList = cMsgInfos.getBodyByFlag("WrapParamTable");
            if (cWrapParamList == null)
            {
                errLog("获取计算保费参数信息失败。");
                return false;
            }
//          校验被保人年龄
            if(!tWxCommon.checkInsuredAge(cWrapParamList)){
            	errLog("被保人年龄不在投保年龄范围内!");
                return false;
            }
        	cLCContTable.setPrtNo("WX999999999");//试算印刷号，统一值，所有试算均采用该印刷号
        	WxContInsuredIntlBL ttWxContInsuredIntlBL = new WxContInsuredIntlBL(mGlobalInput,tMsgHead,cLCContTable,cLCAppntTable,cLCInsuredList,cWrapTable,cLCBnfList,cWrapParamList);
        	mError = ttWxContInsuredIntlBL.deal();
			if(!"".equals(mError)){
				errLog(mError);
                return false;
			}
			mPrem = ttWxContInsuredIntlBL.getPrem();
			mAmnt = ttWxContInsuredIntlBL.getAmnt();
			LCPolSet tLCPolSet = ttWxContInsuredIntlBL.getLCPolSet();
			LCDutySet tLCDutySet = ttWxContInsuredIntlBL.getLCDutySet();
			LCRiskDutyWrapSet tLCRiskDutyWrapSet = ttWxContInsuredIntlBL.getLCRiskDutyWrapSet();
			if(tLCPolSet != null && tLCPolSet.size()>0){
				cWrapTable.setInsuYear(String.valueOf(tLCPolSet.get(1).getInsuYear()));
				cWrapTable.setInsuYearFlag(tLCPolSet.get(1).getInsuYearFlag());
				cWrapParamList = new ArrayList();
				for(int i=1;i<=tLCPolSet.size();i++){
					for(int j=1;j<=tLCDutySet.size();j++){
						if(tLCPolSet.get(i).getPolNo().equals(tLCDutySet.get(j).getPolNo())){
							for(int k=1;k<=2;k++){
								WrapParamTable tWrapParamTable = new WrapParamTable();
								tWrapParamTable.setContNo(cLCContTable.getContNo());
								tWrapParamTable.setRiskWrapCode(cRiskWrapCode);
								tWrapParamTable.setRiskCode(tLCPolSet.get(i).getRiskCode());
								tWrapParamTable.setDutyCode(tLCDutySet.get(j).getDutyCode());
								if(k==1){
									tWrapParamTable.setCalfactor("Amnt");
									tWrapParamTable.setCalfactorValue(String.valueOf(tLCDutySet.get(j).getAmnt()));
								}else{
									tWrapParamTable.setCalfactor("Prem");
									tWrapParamTable.setCalfactorValue(String.valueOf(tLCDutySet.get(j).getPrem()));
								}
								cWrapParamList.add(tWrapParamTable);
							}
						}
					}
				}
			}
			cWrapTable.setAmnt(mAmnt+"");
			cWrapTable.setPrem(mPrem+"");
			if(tLCRiskDutyWrapSet != null && tLCRiskDutyWrapSet.size()>0){
				for(int i=1;i<=tLCRiskDutyWrapSet.size();i++){
					if("FeeRate".equals(tLCRiskDutyWrapSet.get(i).getCalFactor()) 
							&& "2".equals(tLCRiskDutyWrapSet.get(i).getCalFactorType())
							&& (!"".equals(tLCRiskDutyWrapSet.get(i).getCalFactorValue()))
							&& tLCRiskDutyWrapSet.get(i).getCalFactorValue() != null ){
						cWrapTable.setFeeRate(tLCRiskDutyWrapSet.get(i).getCalFactorValue());
					}
				}
			}
//		    组织返回报文
			getXmlResult();
		}catch(Exception ex){
			return false;
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
		NewLCContTable newLCContTable=replace(cLCContTable);
		putResult("LCContTable", newLCContTable);
		//投保人节点
//		putResult("LCAppntTable", cLCAppntTable);
		//所有被保人节点
//		for(int i=0;i<cLCInsuredList.size();i++){
//			putResult("LCInsuredTable", (LCInsuredTable)cLCInsuredList.get(i));
//		}
		//套餐节点
		putResult("WrapTable", cWrapTable);
		//算费参数节点
		for(int i=0;i<cWrapParamList.size();i++){
			putResult("WrapParamTable", (WrapParamTable)cWrapParamList.get(i));
		}
		//受益人节点
//		for(int i=0;i<cLCBnfList.size();i++){
//			putResult("LCBnfTable", (LCBnfTable)cLCBnfList.get(i));
//		}
	}
	public NewLCContTable replace(LCContTable mLcContTable){
		NewLCContTable newlc=new NewLCContTable();
		newlc.setAgentCode(mLcContTable.getAgentCode());
		newlc.setAgentCom(mLcContTable.getAgentCom());
		newlc.setCInValiDate(mLcContTable.getCInValiDate());
		newlc.setCInValiTime(mLcContTable.getCInValiTime());
		newlc.setContNo(mLcContTable.getContNo());
		newlc.setCValiDate(mLcContTable.getCInValiDate());
		newlc.setCValiTime(mLcContTable.getCInValiTime());
		newlc.setManageCom(mLcContTable.getManageCom());
		newlc.setPayIntv(mLcContTable.getPayIntv());
		newlc.setPayMode(mLcContTable.getPayMode());
		newlc.setPolApplyDate(mLcContTable.getPolApplyDate());
		newlc.setPrtNo(mLcContTable.getPrtNo());
		newlc.setSaleChnl(mLcContTable.getSaleChnl());		
		return newlc;
	}
	public static void main(String args[]){
		WxPrintBL ecp = new WxPrintBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo("123456789");
		tMsgHead.setBranchCode("001");
		tMsgHead.setMsgType("WXP0001");
		tMsgHead.setSendDate("2010-9-21");
		tMsgHead.setSendTime("09:00:00");
		tMsgHead.setSendOperator("sino");
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo("WX000000012");
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);		
		ecp.deal(tMsgCollection);	
		
	}
}
