/**
 * 保单信息录入！
 */

package com.cbsws.xml.ctrl.complexpds;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.cbsws.obj.CrsSaleInfo;
import com.cbsws.obj.LCAppntTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LCInsuredTable;
import com.cbsws.obj.SYOtherTable;
import com.cbsws.obj.SpareFieldTable;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAccountSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.tb.ContBL;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class RYContBL implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(RYContBL.class);
	
	private final LCContTable cLCContTable;
	private final LCAppntTable cLCAppntTable;
	private final CrsSaleInfo cCrsSaleInfo;
	private final GlobalInput cGlobalInput;
	private final List cLCInsuredList;
	private final LCContSubSchema cLcContSubSchema;
	private final SpareFieldTable tSpareFieldTable;
	public  SYOtherTable ryOtherInfo;
	private String cError="";
	
	public RYContBL(GlobalInput pGlobalInput,LCContTable contTable,CrsSaleInfo CrsSaleInfo, LCAppntTable appntTable,List tLCInsuredList,LCContSubSchema lcContSubSchema,SpareFieldTable cSpareFieldTable) {
		cLCContTable = contTable;
		cCrsSaleInfo=CrsSaleInfo;
		cLCAppntTable = appntTable;
		cGlobalInput = pGlobalInput;
		cLCInsuredList = tLCInsuredList;
		cLcContSubSchema=lcContSubSchema;
		tSpareFieldTable = cSpareFieldTable;
	}
	public RYContBL(GlobalInput pGlobalInput,LCContTable contTable,CrsSaleInfo CrsSaleInfo, LCAppntTable appntTable,List tLCInsuredList,LCContSubSchema lcContSubSchema,SYOtherTable ryOtherInfo,SpareFieldTable cSpareFieldTable) {
		cLCContTable = contTable;
		cCrsSaleInfo=CrsSaleInfo;
		cLCAppntTable = appntTable;
		cGlobalInput = pGlobalInput;
		cLCInsuredList = tLCInsuredList;
		cLcContSubSchema=lcContSubSchema;
		tSpareFieldTable =cSpareFieldTable;
		this.ryOtherInfo=ryOtherInfo;
	}
	
	public String deal() throws Exception {
		cLogger.info("Into WxContBL.deal()...");
		
		//校验数据
		cError = checkContBL();
		
		if("".equals(cError)){
//			录入保单合同信息
			VData mContBLData = getContBLVData();
			ContBL mContBL = new ContBL();
			cLogger.info("Start call ContBL.submitData()...");
			long mStartMillis = System.currentTimeMillis();
			if (!mContBL.submitData(mContBLData, "INSERT||CONT")) {
				return mContBL.mErrors.getFirstError();
			}
			cLogger.info("ContBL耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s"
					+ "；报文类型：WX0002" );
			cLogger.info("End call ContBL.submitData()!");
		}
		cLogger.info("Out WxContBL.deal()!");
		return cError;
	}
	
	private String checkContBL() throws MidplatException {
		cLogger.info("Into WxContBL.checkContBL()...");
		
		//校验投保单号
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(cLCContTable.getPrtNo());
		
		/**
		 * add by wangxt for checking in 2009-3-18 begin
		 */
//		if (mLCContDB.getPrtNo().length() != 12) {
//			throw new MidplatException("投保单印刷号长度必须是12位！");
//		}
//		
//		if (!mLCContDB.getPrtNo().matches("[0-9]{12}")) {
//			throw new MidplatException("投保单印刷号中不能含字符！");
//		}
		
//		int length = mLCContDB.getPrtNo().length();
//		int sum = 0;
//		for (int i = 0; i < length - 1; i++) {
//			int num = Integer.parseInt(mLCContDB.getPrtNo().substring(i,
//					i + 1));
//			sum += num;
//		}
//		if (sum % 10 != Integer.parseInt(mLCContDB.getPrtNo()
//				.substring(11))) {
//			throw new MidplatException("投保单印刷号不符合规则！");
//		}
		/**
		 * add by wangxt for checking in 2009-3-18 end
		 */
		
		LCContSet mLCContSet = mLCContDB.query();
		if (mLCContSet.size() > 0) {
			return "该投保单印刷号已使用，请更换！";
		}
		
		String mSQL = "select 1 from LDBank where 1=1 "
					+ "and BankCode = '" + cLCContTable.getAppntBankCode() + "' "
					+ "and ComCode='"+ cGlobalInput.ComCode + "' "
					+ "and CansendFlag='1' "
					+ "with ur";
		SSRS mSSRS = new ExeSQL().execSQL(mSQL);
		if(mSSRS == null || mSSRS.getMaxRow() < 0) {
			return "银行编码"+cLCContTable.getAppntBankCode()+"不存在，请更换！";
		}
			
		cLogger.info("Out WXContBL.checkContBL()!");
		return "";
	}
	
	private VData getContBLVData() throws MidplatException {
		cLogger.info("Into WXContBL.getContBLVData()...");

		//保单信息
		LCContSchema mLCContSchema = getLCContSchema();

		//投保人
		LCAppntSchema mLCAppntSchema = new LCAppntSchema();
		mLCAppntSchema.setAppntNo("");	//注意此处必须设置！picch核心判断AppntNo为""时才进行投保人五要素判断，AppntNo为null时会直接生成新客户号
		mLCAppntSchema.setPrtNo(cLCContTable.getPrtNo());
		mLCAppntSchema.setAppntName(cLCAppntTable.getAppntName());
		mLCAppntSchema.setAppntSex(cLCAppntTable.getAppntSex());
		mLCAppntSchema.setAppntBirthday(cLCAppntTable.getAppntBirthday());
		mLCAppntSchema.setIDType(cLCAppntTable.getAppntIDType());
		mLCAppntSchema.setIDNo(cLCAppntTable.getAppntIDNo());
		mLCAppntSchema.setOccupationCode(cLCAppntTable.getOccupationCode());
		mLCAppntSchema.setAuthorization(cLCAppntTable.getAuthorization());//授权编码
		mLCAppntSchema.setBankCode(mLCContSchema.getBankCode());
		mLCAppntSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLCAppntSchema.setAccName(mLCContSchema.getAccName());
		mLCAppntSchema.setMarriage(ryOtherInfo.getMarriage());
		mLCAppntSchema.setDegree(ryOtherInfo.getDegree());
		mLCAppntSchema.setRgtAddress(ryOtherInfo.getRgtAddress());
		mLCAppntSchema.setNativePlace(ryOtherInfo.getNativePlace());
		mLCAppntSchema.setIDStartDate(ryOtherInfo.getIDStartdate());
		mLCAppntSchema.setIDEndDate(ryOtherInfo.getIDEnddate());
		mLCAppntSchema.setSalary(ryOtherInfo.getSalary());
		mLCAppntSchema.setNativeCity(ryOtherInfo.getNativeCity());
		System.out.println("3099"+ryOtherInfo.getNativePlace());
//		String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + cLCAppntTable.getOccupationCode() + "' with ur";
//		String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
//		if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
//			throw new MidplatException("未查到职业类别！职业代码为：" + cLCAppntTable.getOccupationCode());
//		}
//		mLCAppntSchema.setOccupationType(mOccupationTypeStr);	//职业类别;

		//投保人地址
		LCAddressSchema mAppntAddress = new LCAddressSchema();
		mAppntAddress.setCompanyPhone("");
		mAppntAddress.setMobile(cLCAppntTable.getAppntMobile());
		mAppntAddress.setPhone(cLCAppntTable.getAppntPhone());
		mAppntAddress.setHomePhone(cLCAppntTable.getAppntPhone());
		mAppntAddress.setPostalAddress(cLCAppntTable.getMailAddress());
		mAppntAddress.setZipCode(cLCAppntTable.getMailZipCode());
		mAppntAddress.setHomeAddress(cLCAppntTable.getHomeAddress());
		mAppntAddress.setHomeZipCode(cLCAppntTable.getMailZipCode());
		mAppntAddress.setEMail(cLCAppntTable.getEmail());
		mAppntAddress.setGrpName(ryOtherInfo.getGrpName());
		mAppntAddress.setPostalProvince(tSpareFieldTable.getAppntPostalProvince());
		mAppntAddress.setPostalCity(tSpareFieldTable.getAppntPostalCity());
		mAppntAddress.setPostalCounty(tSpareFieldTable.getAppntPostalCounty());
		mAppntAddress.setPostalStreet(tSpareFieldTable.getAppntPostalStreet());
		mAppntAddress.setPostalCommunity(tSpareFieldTable.getAppntPostalCommunity());

		//投保帐户
		LCAccountSchema mLCAccountSchema = new LCAccountSchema();
		mLCAccountSchema.setBankCode(mLCContSchema.getBankCode());
		mLCAccountSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLCAccountSchema.setAccName(mLCContSchema.getAccName());
		mLCAccountSchema.setAccKind("Y");

		//投保人个人信息
		LDPersonSchema mAppntPerson = new LDPersonSchema();
		mAppntPerson.setName(mLCAppntSchema.getAppntName());
		mAppntPerson.setSex(mLCAppntSchema.getAppntSex());
		mAppntPerson.setIDNo(mLCAppntSchema.getIDNo());
		mAppntPerson.setIDType(mLCAppntSchema.getIDType());
		mAppntPerson.setBirthday(mLCAppntSchema.getAppntBirthday());
		mAppntPerson.setNativeCity(ryOtherInfo.getNativeCity());
		mAppntPerson.setRgtAddress(ryOtherInfo.getRgtAddress());
		mAppntPerson.setMarriage(ryOtherInfo.getMarriage());
		mAppntPerson.setNativePlace(ryOtherInfo.getNativePlace());
		mAppntPerson.setSalary(ryOtherInfo.getSalary());
		mAppntPerson.setGrpName(ryOtherInfo.getGrpName());
		mAppntPerson.setAuthorization(cLCAppntTable.getAuthorization());//授权标记

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("GrpNo", ryOtherInfo.getGrpNo());
		mTransferData.setNameAndValue("GrpName", ryOtherInfo.getGrpName());
		mTransferData.setNameAndValue("Authorization", cLCAppntTable.getAuthorization());//授权标记

		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(mLCContSchema);
		mVData.add(mLCAppntSchema);
		mVData.add(mAppntAddress);
		mVData.add(mLCAccountSchema);
//		mVData.add(mLCCustomerImpartSet);
		mVData.add(mAppntPerson);
		mVData.add(mTransferData);
		mVData.add(cLcContSubSchema);

		cLogger.info("Out WXContBL.getContBLVData()!");
		return mVData;
	}
	
	private LCContSchema getLCContSchema() throws MidplatException {
		cLogger.info("Into WXContBL.getLCContSchema()...");
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		
		LCContSchema mLCContSchema = new LCContSchema();
		mLCContSchema.setGrpContNo("00000000000000000000");
		mLCContSchema.setContNo(cLCContTable.getContNo());	//用保单合同印刷号录单
		mLCContSchema.setProposalContNo(cLCContTable.getContNo());
		mLCContSchema.setPrtNo(cLCContTable.getPrtNo());
		mLCContSchema.setContType("1");	//保单类别(个单-1，团单-2)
		mLCContSchema.setPolType("0");
		mLCContSchema.setCardFlag("b");	//CardFlag"b"，网销复杂产品！
		mLCContSchema.setManageCom(cGlobalInput.ManageCom);
		mLCContSchema.setAgentCom(cGlobalInput.AgentCom);
		mLCContSchema.setCrs_SaleChnl(cCrsSaleInfo.getCrs_SaleChnl());
		mLCContSchema.setCrs_BussType(cCrsSaleInfo.getCrs_BussType());
		mLCContSchema.setGrpAgentCom(cCrsSaleInfo.getGrpAgentCom());
		mLCContSchema.setGrpAgentCode(cCrsSaleInfo.getGrpAgentCode());
		mLCContSchema.setGrpAgentName(cCrsSaleInfo.getGrpAgentName());
		mLCContSchema.setGrpAgentIDNo(cCrsSaleInfo.getGrpAgentIDNo());
		
//		LAComToAgentDB mLAComToAgentDB = new LAComToAgentDB();
//		mLAComToAgentDB.setAgentCom("12345");
//		mLAComToAgentDB.setRelaType("1");
//		LAComToAgentSet mLAComToAgentSet = mLAComToAgentDB.query();
//		if (mLAComToAgentSet.mErrors.needDealError()
//				|| (null==mLAComToAgentSet) || (mLAComToAgentSet.size()<1)) {
//      	cLogger.error(mLAComToAgentDB.mErrors.getFirstError());
//      	throw new MidplatException("未查到相应银行网点的专管员信息！");
//		}
//		mLCContSchema.setAgentCode(mLAComToAgentSet.get(1).getAgentCode());
//		mLCContSchema.setAgentGroup(mLAComToAgentSet.get(1).getAgentGroup());
		mLCContSchema.setAgentCode(cLCContTable.getAgentCode());
		mLCContSchema.setAgentGroup("");
		mLCContSchema.setSaleChnl(cLCContTable.getSaleChnl());	//渠道"04"，CardFlag"9"，共同识别银保通交易！
		mLCContSchema.setPassword("");	//保单密码
		mLCContSchema.setInputOperator(cGlobalInput.Operator);
		mLCContSchema.setInputDate(PubFun.getCurrentDate());
		mLCContSchema.setInputTime(PubFun.getCurrentTime());
		//投保人
		mLCContSchema.setAppntName(cLCAppntTable.getAppntName());
		mLCContSchema.setAppntSex(cLCAppntTable.getAppntSex());
		mLCContSchema.setAppntBirthday(cLCAppntTable.getAppntBirthday());
		mLCContSchema.setAppntIDType(cLCAppntTable.getAppntIDType());
		mLCContSchema.setAppntIDNo(cLCAppntTable.getAppntIDNo());
		//zxs20190517
		mLCContSchema.setAgentSaleCode(cLCContTable.getAgentSaleCode());
		//被保人
		for(int i=0;i<cLCInsuredList.size();i++){
			LCInsuredTable tLCInsuredTable = (LCInsuredTable)cLCInsuredList.get(i);
			if("00".equals(tLCInsuredTable.getRelaToMain())){
				mLCContSchema.setInsuredName(tLCInsuredTable.getName());
				mLCContSchema.setInsuredSex(tLCInsuredTable.getSex());
				mLCContSchema.setInsuredBirthday(tLCInsuredTable.getBirthday());
				mLCContSchema.setInsuredIDType(tLCInsuredTable.getIDType());
				mLCContSchema.setInsuredIDNo(tLCInsuredTable.getIDNo());
			}
			
		}
		mLCContSchema.setPayIntv(cLCContTable.getPayIntv());	//缴费方式
		mLCContSchema.setPayMode("4");
		mLCContSchema.setPayLocation("0");	//银行转帐
		
		mLCContSchema.setBankAccNo(cLCContTable.getAppntBankAccNo());
		mLCContSchema.setBankCode(cLCContTable.getAppntBankCode());
		mLCContSchema.setAccName(cLCContTable.getAppntAccName());
		mLCContSchema.setExPayMode(cLCContTable.getExPayMode());
		
//		String mSQL = "select BankCode from LDBank where 1=1 "
//			+ "and BankCode like '" + mBaseInfo.getChildText(BankCode) + "%' "
//			+ "and ComCode='"+ cGlobalInput.ComCode + "' "
//			+ "and CansendFlag='1' "
//			+ "with ur";
//		mLCContSchema.setBankCode(new ExeSQL().getOneValue(mSQL));
//		if ((null==mLCContSchema.getBankCode()) || mLCContSchema.getBankCode().equals("")) {
//			mLCContSchema.setBankCode(mBaseInfo.getChildText(BankCode));
//		}
//		mLCContSchema.setBankAccNo(mLCCont.getChildText(BankAccNo));	//投保帐户号
//		mLCContSchema.setAccName(mLCCont.getChildText(AccName));	//投保帐户姓名
		mLCContSchema.setPrintCount(0);
//		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent));
		GregorianCalendar mNowCalendar = new GregorianCalendar();
		mNowCalendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
//		mLCContSchema.setCValiDate(mNowCalendar.getTime());	//保单生效日期(当前日期的下一天)
		mLCContSchema.setCValiDate(cLCContTable.getCValiDate());
		//保单生效日期(取自电子商务)
		if("".equals(cLCContTable.getPolApplyDate())||cLCContTable.getPolApplyDate()==null){
			mLCContSchema.setPolApplyDate(mCurrentDate);
		}else{
			mLCContSchema.setPolApplyDate(cLCContTable.getPolApplyDate());
		}
			//投保日期
		mLCContSchema.setProposalType("01");	//投保书类型：01-普通个单投保书
//		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent));	//特别约定
		mLCContSchema.setFirstTrialOperator(ryOtherInfo.getFirstTrialOperator());
		
		cLogger.info("Out YbtContBL.getLCContSchema()!");
		return mLCContSchema;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
//		String mInFile = "D:/request/ICBC_std/UW.xml";
//		
//		FileInputStream mFis = new FileInputStream(mInFile);
//		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
//		Document mXmlDoc = new SAXBuilder().build(mIsr);
//		
//		Element mTranData = mXmlDoc.getRootElement();
//		Element mBaseInfo = mTranData.getChild("BaseInfo");
//		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
//		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
//		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
//		mLCCont.getChild("PrtNo").setText(mProposalNo);
//		mLCCont.getChild("ProposalContNo").setText(mPrtNo);
//		
//		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
//				mBaseInfo.getChildText("BankCode"),
//				mBaseInfo.getChildText("ZoneNo"),
//				mBaseInfo.getChildText("BrNo"));
//		
//		new WxContBL(mXmlDoc, mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}
