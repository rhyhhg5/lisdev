package com.cbsws.xml.ctrl.complexpds;


import java.util.List;











import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.GrpInfo;

import com.cbsws.obj.RYContTable;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;






public class RYhrState extends ABusLogic {
	
	public String GrpNo="";//印刷号
	public String SendDate="";//报文发送日期
	public String SendTime="";//报文发送时间
	public String BatchNo="";
	public String StartDate;
	public String EndDate;
	public GrpInfo cGrpINFO;
	
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始登录验证");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		SendDate = tMsgHead.getSendDate();
		SendTime = tMsgHead.getSendTime();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("GrpInfo");			
			cGrpINFO = (GrpInfo) tLCContList.get(0); //获取保单信息，只有一个保单
	        
			GrpNo=cGrpINFO.getGrpNo();
			StartDate=cGrpINFO.getStartDate();
			EndDate=cGrpINFO.getEndDate();			
			
	        if("".equals(GrpNo)||GrpNo==null){
	        	errLog("团队编码不能为空");
	        	return false;
	        }
	        if(!PubFun.checkDateForm(StartDate)||!PubFun.checkDateForm(EndDate)){
	        	errLog("请确定所传日期格式为YYYY-MM-DD");
	        	return false;
	        	
	        }
	        
	       
	        //产品名称、缴费频次、风险保费、万能账户、生效日期、税优识别码、工作单位、团体代码
	        SSRS ss1=new SSRS();
	        ExeSQL exeSQL=new ExeSQL();
	        String sql="select lc.contno,(select codename from ldcode where codetype='appflag' and code=lc.appflag),"
	        		+"lc.PolApplyDate,lc.prem,lc.amnt"
	        		+ " ,nvl(lc.insuredname, (select name from lcinsured where 1=1 and contno=lc.contno and insuredno=lc.insuredno) ) insuredname, "
	        		 +" (select  riskname from lmriskapp where riskcode in (select riskcode from lcpol where prtno=lc.prtno fetch first 1 rows only)) riskname,"//产品名称
	        		 + "(select codename from ldcode where codetype = 'payintv' and code=lc.payintv) payintv,"//缴费频次
	        		 + "(select fee from lcinsureaccfeetrace where contno=lc.contno and othertype='1' and moneytype = 'RP') fee ,"//风险保费
	        		 + " ((select sumactupaymoney from ljapay where incomeno=lc.contno and duefeetype='0')  -(select fee from lcinsureaccfeetrace where contno=lc.contno and othertype='1' and moneytype = 'RP')),"
	        		 + "lc.CValiDate,ls.taxcode,ls.grpno,(select grpname from lsgrp where grpno = ls.grpno) grpname "
	        		 +" from lccont lc,lccontsub ls where  lc.prtno=ls.prtno and ls.grpno='"+GrpNo+"' and "
	        		 + "lc.polapplydate>='"+StartDate+"' and lc.polapplydate<='"+EndDate+"' order by lc.PolApplyDate";
	        ss1=exeSQL.execSQL(sql);
	        if(ss1.getMaxRow()==0){
	        	errLog("未查询到存在税优保单");
        		return false;
	        }else{
	        	for(int i=1;i<=ss1.getMaxRow();i++){
	        		RYContTable ryContTable=new RYContTable();
	        		ryContTable.setContNo(ss1.GetText(i, 1));
	        		ryContTable.setState(ss1.GetText(i, 2));
	        		ryContTable.setPolApplyDate(ss1.GetText(i, 3));
	        		ryContTable.setPrem(ss1.GetText(i, 4));
	        		ryContTable.setAmnt(ss1.GetText(i, 5));
	        		ryContTable.setInsuredName(ss1.GetText(i, 6));
	        		//zxs
	        		ryContTable.setRiskName(ss1.GetText(i, 7));
	        		ryContTable.setPayintv(ss1.GetText(i, 8));
	        		ryContTable.setFee(ss1.GetText(i, 9));
	        		ryContTable.setInsuAccBala(ss1.GetText(i, 10));
	        		ryContTable.setCvalidate(ss1.GetText(i, 11));
	        		ryContTable.setTaxcode(ss1.GetText(i, 12));
	        		ryContTable.setGrpno(ss1.GetText(i, 13));
	        		ryContTable.setGrpname(ss1.GetText(i, 14));
	        		putResult("RYContTable", ryContTable);
	        		
	        		
	        	}
	        	
	        	
	        	
	        	
	        }
	        
	        
	       
	        //getXmlResult();
		}catch (Exception e) {
			System.out.println(e);
			errLog("未知错误，请检查报文格式");
        	return false;
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
		
		
		
		
	}
	

	
	

    


    public static void main(String[] args) {
    	String s1="[A-Za-z0-9]{10}";
    	String s2="1b3456789A";
    	System.out.println(s2.matches(s1));
    	
	}
    
}
