package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;

import com.cbsws.obj.*;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.tb.CalBL;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class WxRiskCalculate extends ABusLogic {
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public double mPrem=0;//保费
	public double mAmnt=0;//保额
	
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	
	private List mLCPolList;
	private List mLCGetList = new ArrayList();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始处理网销复杂产品试算业务");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			mLCPolList = cMsgInfos.getBodyByFlag("LCPolTable");
			
			if(mLCPolList == null || mLCPolList.size() == 0){
				errLog("获取险种清单失败。");
                return false;
			}
			
			TransferData mTransferData = new TransferData();
			CalBL tCalBL;
			
			for(int i=0;i<mLCPolList.size();i++){
				LCPolTable tLCPolTable = (LCPolTable) mLCPolList.get(i);
				if(tLCPolTable == null){
					errLog("获取险种失败。");
	                return false;
				}
				LCPolBL mLCPolBL = new LCPolBL();
				LCPolSchema tLCPolSchema = new LCPolSchema();
				tLCPolSchema = getLCPolSchema(tLCPolTable);
				mLCPolBL.setSchema(tLCPolSchema);
				
				LCDutyBLSet mLCDutyBLSet = new LCDutyBLSet();
				LCDutySchema tLCDutySchema = new LCDutySchema();
				tLCDutySchema = getLCDutySchema(tLCPolTable);
				mLCDutyBLSet.add(tLCDutySchema);
				
				tCalBL = new CalBL(mLCPolBL, mLCDutyBLSet, null,mTransferData);
	        	
	        	if (tCalBL.calPol() == false){
	        		System.out.println("出错了");
	        	}
	        	
	        	mLCPolBL.setSchema(tCalBL.getLCPol().getSchema());
	        	LCPremBLSet mLCPremBLSet = tCalBL.getLCPrem(); //得到的保费项集合不包括加费的保费项，所以在后面处理
	        	LCGetBLSet mLCGetBLSet = tCalBL.getLCGet();
	            mLCDutyBLSet = tCalBL.getLCDuty();
	            double sumPrem = 0;
	            for(int j = 1;j<=mLCPremBLSet.size();j++){
	            	sumPrem += mLCPremBLSet.get(j).getPrem();
	            }
	            tLCPolTable.setPrem(sumPrem+"");
	            for(int m = 1;m<=mLCGetBLSet.size();m++){
	            	LCGetTable tLCGetTable = new LCGetTable();
	            	tLCGetTable.setGetDutyCode(mLCGetBLSet.get(m).getGetDutyCode());
	            	String tGetDutyNameSQL = "select getdutyname from lmdutyget where getdutycode = '"+mLCGetBLSet.get(m).getGetDutyCode()+"' ";
	            	String tGetDutyName = new ExeSQL().getOneValue(tGetDutyNameSQL);
	            	tLCGetTable.setGetDutyName(tGetDutyName);
	            	tLCGetTable.setActuGet(mLCGetBLSet.get(m).getActuGet()+"");
	            	tLCGetTable.setRiskPrem(sumPrem+"");
	            	mLCGetList.add(tLCGetTable);
	            }
			}
        	
//		    组织返回报文
			getXmlResult();
		}catch(Exception ex){
			return false;
		}
		return true;
	}
	
	private LCPolSchema getLCPolSchema(LCPolTable aLCPolTable){
		LCPolSchema aLCPolSchema = new LCPolSchema();
		aLCPolSchema.setPrem(aLCPolTable.getPrem());
		aLCPolSchema.setStandPrem(aLCPolTable.getPrem());
		aLCPolSchema.setRiskCode(aLCPolTable.getRiskCode());
		aLCPolSchema.setAmnt(aLCPolTable.getAmnt());
		aLCPolSchema.setInsuYear(aLCPolTable.getInsuYear());
		aLCPolSchema.setInsuYearFlag(aLCPolTable.getInsuYearFlag());
		aLCPolSchema.setPayEndYear(aLCPolTable.getPayEndYear());
		aLCPolSchema.setPayEndYearFlag(aLCPolTable.getPayEndYearFlag());
		aLCPolSchema.setInsuredAppAge(aLCPolTable.getInsuredAppAge());
		aLCPolSchema.setPayIntv(aLCPolTable.getPayIntv());
		aLCPolSchema.setCValiDate(aLCPolTable.getCValiDate());
		aLCPolSchema.setContType(aLCPolTable.getContType());
		aLCPolSchema.setManageCom(aLCPolTable.getManageCom());
		aLCPolSchema.setInsuredSex(aLCPolTable.getInsuredSex());
		String aInsuredBirthday = PubFun.calDate(mCurrentDate,-Integer.parseInt(aLCPolTable.getInsuredAppAge()),"Y",null);
		aLCPolSchema.setInsuredBirthday(aInsuredBirthday);
		return aLCPolSchema;
	}
	
	private LCDutySchema getLCDutySchema(LCPolTable aLCPolTable){
		LCDutySchema aLCDutySchema = new LCDutySchema();
		aLCDutySchema.setDutyCode("");
		aLCDutySchema.setAmnt(aLCPolTable.getAmnt());
		aLCDutySchema.setPrem(aLCPolTable.getPrem());
		aLCDutySchema.setStandPrem(aLCPolTable.getPrem());
		aLCDutySchema.setInsuYear(aLCPolTable.getInsuYear());
		aLCDutySchema.setInsuYearFlag(aLCPolTable.getInsuYearFlag());
		aLCDutySchema.setPayEndYear(aLCPolTable.getPayEndYear());
		aLCDutySchema.setPayEndYearFlag(aLCPolTable.getPayEndYearFlag());
		aLCDutySchema.setPayIntv(aLCPolTable.getPayIntv());
		return aLCDutySchema;
	}
	
	//返回报文
	public void getXmlResult(){
		//算费参数节点
		for(int i=0;i<mLCPolList.size();i++){
			putResult("LCPolTable", (LCPolTable)mLCPolList.get(i));
		}
		for(int i=0;i<mLCGetList.size();i++){
			putResult("LCGetTable", (LCGetTable)mLCGetList.get(i));
		}
	}
	public static void main(String args[]){
		LCPolBL mLCPolBL = new LCPolBL();
		LCDutyBLSet mLCDutyBLSet = new LCDutyBLSet();
		TransferData mTransferData = new TransferData();
    	
    	CalBL tCalBL;
    	LCPolSchema tLCPolSchema = new LCPolSchema();
    	tLCPolSchema.setRiskCode("340301");
    	tLCPolSchema.setAmnt(100000);
    	tLCPolSchema.setInsuYear(106);
    	tLCPolSchema.setInsuYearFlag("A");
    	tLCPolSchema.setPayEndYear("1");
    	tLCPolSchema.setPayEndYearFlag("Y");
    	tLCPolSchema.setInsuredAppAge(30);
    	tLCPolSchema.setPayIntv(0);
    	tLCPolSchema.setCValiDate("2014-12-25");
    	tLCPolSchema.setContType("1");
    	tLCPolSchema.setManageCom("86110000");
    	tLCPolSchema.setInsuredBirthday("1985-05-05");
    	tLCPolSchema.setInsuredSex("0");
    	
    	mLCPolBL.setSchema(tLCPolSchema);
    	
    	LCDutySchema tLCDutySchema = new LCDutySchema();
    	tLCDutySchema.setDutyCode("");
    	tLCDutySchema.setAmnt(100000);
    	tLCDutySchema.setInsuYear(106);
    	tLCDutySchema.setInsuYearFlag("A");
    	tLCDutySchema.setPayEndYear(1);
    	tLCDutySchema.setPayEndYearFlag("Y");
    	
    	mLCDutyBLSet.add(tLCDutySchema);
    	
    	tCalBL = new CalBL(mLCPolBL, mLCDutyBLSet, null,mTransferData);
    	
    	if (tCalBL.calPol() == false){
    		System.out.println("出错了");
    	}
    	
    	mLCPolBL.setSchema(tCalBL.getLCPol().getSchema());
    	LCPremBLSet mLCPremBLSet = tCalBL.getLCPrem(); //得到的保费项集合不包括加费的保费项，所以在后面处理
    	LCGetBLSet mLCGetBLSet = tCalBL.getLCGet();
        mLCDutyBLSet = tCalBL.getLCDuty();
		
	}
}
