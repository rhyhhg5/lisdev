package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CrsSaleInfo;
import com.cbsws.obj.ECCertSalesPayInfoTable;
import com.cbsws.obj.LCAppntTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LCInsuredTable;
import com.cbsws.obj.NewLCContTable;
import com.cbsws.obj.RYCustomerImpartDetailTable;
import com.cbsws.obj.RYCustomerImpartTable;
import com.cbsws.obj.RYSubTable;
import com.cbsws.obj.SYOtherTable;
import com.cbsws.obj.SpareFieldTable;
import com.cbsws.obj.WrapParamTable;
import com.cbsws.obj.WrapTable;
import com.certicom.security.cert.a.a.s;
import com.certicom.security.cert.a.a.t;
import com.sinosoft.httpclient.inf.GetPlatformCustomerNo;
import com.sinosoft.lis.cbcheck.TaxCheckContUI;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LCCustomerImpartDetailSchema;
import com.sinosoft.lis.schema.LCCustomerImpartSchema;
import com.sinosoft.lis.schema.LCPersonTraceSchema;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.schema.LSBatchInfoSchema;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCCustomerImpartDetailSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;



public class RYUW extends ABusLogic {

	public String BatchNo = "";// 批次号
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	public double mPrem = 0;// 保费
	public double mAmnt = 0;// 保额

	public LCContTable cLCContTable;
	public CrsSaleInfo cCrsSaleInfo;
	public LCAppntTable cLCAppntTable;
	public List cLCInsuredList;
	public WrapTable cWrapTable;
	public List cLCBnfList;
	public List cWrapParamList;// 套餐参数问题
	public RYCustomerImpartTable cRyCustomerImpartTable;
	public RYCustomerImpartDetailTable cRyCustomerImpartDetailTable;
	public List ccustomerList;
	public List cDetailList;
	public ECCertSalesPayInfoTable cECCertSalesPayInfo;
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String mError = "";// 处理过程中的错误信息
	public RYSubTable subtable;
	String cRiskWrapCode = "";// 套餐编码
	public SYOtherTable ryOtherInfo;
	
	public LCPersonTraceSchema appntLcPersonTrace  = new LCPersonTraceSchema();
	public LCPersonTraceSchema insuredLcPersonTrace = new LCPersonTraceSchema();

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理网销复杂产品核保业务");
		System.out.println("开始处理网销复杂产品核保业务979798798798789797");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			List tlcsublistList = cMsgInfos.getBodyByFlag("RYSubTable");
			List tsyotherinfoList=cMsgInfos.getBodyByFlag("SYOtherTable");
			if (tsyotherinfoList == null || tsyotherinfoList.size() != 1) {
				errLog("获取税优信息失败。");
				return false;
			}
			ryOtherInfo=(SYOtherTable) tsyotherinfoList.get(0);
			if (tlcsublistList == null || tlcsublistList.size() != 1) {
				errLog("获取税优信息失败。");
				return false;
			}
			subtable = (RYSubTable) tlcsublistList.get(0);
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();
			System.out.println("管理￥￥￥#￥￥￥" + cLCContTable.getManageCom());
			
			
			//zxs 20190809 销售渠道为互动中介时，中介机构不能为空
			if(cLCContTable.getSaleChnl().equals("15")&&(cLCContTable.getAgentCom().equals("")||cLCContTable.getAgentCom()==null)){
				errLog("销售渠道为互动中介时，中介机构代码不能为空！");
				return false;
			}

			List tWrapList = cMsgInfos.getBodyByFlag("WrapTable");
			if (tWrapList == null || tWrapList.size() != 1) {
				errLog("试算或核保时，获取套餐编码失败。");
				return false;
			}
			cWrapTable = (WrapTable) tWrapList.get(0);
			cRiskWrapCode = cWrapTable.getRiskWrapCode();
			if (cRiskWrapCode == null || "".equals(cRiskWrapCode)) {
				errLog("在报文中获取套餐编码失败。");
				return false;
			}
			WxCommon tWxCommon = new WxCommon(cRiskWrapCode);
			if (!tWxCommon.checkTheOne(tWrapList)) {
				errLog("每次投保只能购买一个产品。");
				return false;
			}
			// 根据投保单申请日期校验产品是否已停售
			mError = tWxCommon.checkWrapStop(cLCContTable.getPolApplyDate());
			if (!"".equals(mError)) {
				errLog(mError);
				return false;
			}
			//获取交叉销售信息
			List tCrsSaleInfoList=cMsgInfos.getBodyByFlag("CrsSaleInfo");
			if (tCrsSaleInfoList == null || tCrsSaleInfoList.size() != 1) {
				errLog("获取交叉销售信息失败！");
				return false;
			}
			cCrsSaleInfo = (CrsSaleInfo) tCrsSaleInfoList.get(0);
			
			List tLCAppntList = cMsgInfos.getBodyByFlag("LCAppntTable");
			if (tLCAppntList == null || tLCAppntList.size() != 1) {
				errLog("获取投保人信息失败。");
				return false;
			}
			cLCAppntTable = (LCAppntTable) tLCAppntList.get(0);// 获取投保人信息，每个保单只有一个投保人
			//投保人授权使用客户信息不能为空
            String appntAuthorization=cLCAppntTable.getAuthorization();
            if(appntAuthorization==null || appntAuthorization.equals("")){
            	errLog("投保人授权使用客户信息不能为空！");
        		return false;
            }
            appntLcPersonTrace.setAuthorization(appntAuthorization);
            appntLcPersonTrace.setAuthType(cLCAppntTable.getAuthType());
            appntLcPersonTrace.setBusinessLink(cLCAppntTable.getBusinessLink());
            appntLcPersonTrace.setCustomerContact(cLCAppntTable.getCustomerContact());
            appntLcPersonTrace.setSharedMark(cLCAppntTable.getSharedMark());
            appntLcPersonTrace.setSpecialLimitMark(cLCAppntTable.getSpecialLimitMark());
			//校验投保人身份证号信息
            if(cLCAppntTable.getAppntIDNo()!=null&& !cLCAppntTable.getAppntIDNo().equals("")){
            	if(!PubFun.CheckIDNo(cLCAppntTable.getAppntIDType(), cLCAppntTable.getAppntIDNo(), cLCAppntTable.getAppntBirthday(), cLCAppntTable.getAppntSex()).equals("")){
            		errLog("该产品的投保人身份证号信息有误！");
                    return false;
            	}
            }
			cLCInsuredList = cMsgInfos.getBodyByFlag("LCInsuredTable");// 获取被保人
			LCInsuredTable lcInsuredTable=(LCInsuredTable) cLCInsuredList.get(0);
			if(lcInsuredTable.getIDNo()!=null && !lcInsuredTable.getIDNo().equals("")){
        		if(!PubFun.CheckIDNo(lcInsuredTable.getIDType(), lcInsuredTable.getIDNo(), lcInsuredTable.getBirthday(), lcInsuredTable.getSex()).equals("")){
        			errLog("该产品的被保人身份证号信息有误！");
                    return false;
        		}
			} 
			//被保人授权使用客户信息不能为空
            String InsuredAuthorization=lcInsuredTable.getAuthorization();
            if(InsuredAuthorization==null || InsuredAuthorization.equals("")){
            	errLog("被保人授权使用客户信息不能为空！");
        		return false;
            }
            insuredLcPersonTrace.setAuthorization(InsuredAuthorization);
            insuredLcPersonTrace.setSharedMark(lcInsuredTable.getSharedMark());
            insuredLcPersonTrace.setSpecialLimitMark(lcInsuredTable.getSpecialLimitMark());
            insuredLcPersonTrace.setBusinessLink(lcInsuredTable.getBusinessLink());
            insuredLcPersonTrace.setAuthType(lcInsuredTable.getAuthType());
            insuredLcPersonTrace.setCustomerContact(lcInsuredTable.getCustomerContact());
            //增加投被保人详细地址校验
			List tSpareFieldTableList = cMsgInfos.getBodyByFlag("SpareFieldTable");
			SpareFieldTable tSpareFieldTable = (SpareFieldTable)tSpareFieldTableList.get(0);
			if(tSpareFieldTable!=null) {
				String appntProvince = tSpareFieldTable.getAppntPostalProvince();
				String appntCity = tSpareFieldTable.getAppntPostalCity();
				String appntCounty = tSpareFieldTable.getAppntPostalCounty();
				String appntStreet = tSpareFieldTable.getAppntPostalStreet();
				String appntCommunity = tSpareFieldTable.getAppntPostalCommunity();
				if(!checkAddress(1, appntProvince, appntCity, appntCounty, appntStreet, appntCommunity)) {
					return false;
				}
				String insuredProvince = tSpareFieldTable.getInsuredPostalProvince();
				String insuredCity = tSpareFieldTable.getInsuredPostalCity();
				String insuredCounty = tSpareFieldTable.getInsuredPostalCounty();
				String insuredStreet = tSpareFieldTable.getInsuredPostalStreet();
				String insuredcommunity = tSpareFieldTable.getInsuredPostalCommunity();
				if(!checkAddress(2, insuredProvince, insuredCity, insuredCounty, insuredStreet, insuredcommunity)) {
					return false;
				}
			}
			
			if (cLCInsuredList == null) {
				errLog("获取被保人信息失败。");
				return false;
			}
			if (!tWxCommon.checkTheOne(cLCInsuredList)) {
				errLog("该产品的被保人只能为一人！");
				return false;
			}
			// 校验被保人年龄
			if (!tWxCommon.checkInsuredAge(cWrapParamList)) {
				errLog("被保人年龄不在投保年龄范围内!");
				return false;
			}
			
			// 校验投保人职业类型
			// if(!tWxCommon.checkOccupationType(cLCInsuredList)){
			// errLog(mError);
			// return false;
			// }
			cLCBnfList = cMsgInfos.getBodyByFlag("LCBnfTable");
			// if (tLCBnfList == null)
			// {
			// errLog("获取受益人信息失败。");
			// return false;
			// }
			cWrapParamList = cMsgInfos.getBodyByFlag("WrapParamTable");
			if (cWrapParamList == null) {
				errLog("获取计算保费参数信息失败。");
				return false;
			}

			// 获取印刷号并更新卡号状态
			if ("".equals(StrTool.cTrim(cLCContTable.getPrtNo()))) {// 第一次印刷号为空
			//
				String tPrtno = "15" + PubFun1.CreateMaxNo("WXPRTNO", 9);// 生成印刷号
				cLCContTable.setPrtNo(tPrtno);
			} else {// 如果印刷号不为空，即是修改核保信息情况(不能重复购买)------fromyzfstart
				String prtnoSql = "select prtno from lccont where prtno = '"
						+ cLCContTable.getPrtNo() + "'";
				SSRS pSSRS = new ExeSQL().execSQL(prtnoSql);
				if (pSSRS != null && pSSRS.getMaxRow() > 0) {
					String insuredSql = "select name,sex,birthday,idtype,idno from lcinsured where prtno='"
							+ cLCContTable.getPrtNo() + "'";
					LCInsuredTable lcit = null;
					lcit = (LCInsuredTable) cLCInsuredList.get(0);
					SSRS ssrsResult = null;
					ssrsResult = new ExeSQL().execSQL(insuredSql);
					System.out.println(lcit.getName() + "-" + lcit.getSex()
							+ "-" + lcit.getBirthday() + "-" + lcit.getIDType()
							+ "-" + lcit.getIDNo() + "\n"
							+ ssrsResult.GetText(1, 1) + "-"
							+ ssrsResult.GetText(1, 2) + "-"
							+ ssrsResult.GetText(1, 3) + "-"
							+ ssrsResult.GetText(1, 4) + "-"
							+ ssrsResult.GetText(1, 5));
					if (lcit.getName().equals(ssrsResult.GetText(1, 1))
							&& lcit.getSex().equals(ssrsResult.GetText(1, 2))
							&& lcit.getBirthday().equals(
									ssrsResult.GetText(1, 3))
							&& lcit.getIDType()
									.equals(ssrsResult.GetText(1, 4))
							&& lcit.getIDNo().equals(ssrsResult.GetText(1, 5))) {
						try {
							LCContSchema mLCContSchema = new LCContSchema();
							mLCContSchema.setPrtNo(cLCContTable.getPrtNo());
							LCContSet tLCContSet = new LCContSet();
							LCContDB tLCContDB = new LCContDB();
							tLCContSet = tLCContDB
									.executeQuery("select * from lccont where prtno = '"
											+ cLCContTable.getPrtNo() + "' ");
							TransferData tTransferData = new TransferData();
							String tDeleteReason = "上一次输入保单信息有误，需重新修改";
							LCContSchema tLCContSchema = tLCContSet.get(1);
							tTransferData.setNameAndValue("DeleteReason",
									tDeleteReason);
							// 准备传输数据 VData
							VData tVData = new VData();
							tVData.add(tLCContSchema);
							tVData.add(tTransferData);
							tVData.add(mGlobalInput);
							// 数据传输
							ContDeleteUI tContDeleteUI = new ContDeleteUI();
							tContDeleteUI.submitData(tVData, "DELETE");

						} catch (Exception e) {
							e.printStackTrace();
							errLog(mError);
							errLog("删除被保人异常！");
							System.out.println("删除被保人异常！");
							return false;
						}
					} else {
						errLog("prnNo在lccont已经存在，但当前操作的被保人与数据库lcinsured中相应的被保人不同，予以阻断");
						return false;
					}
				}
			}// -----------fromyzfend
				// 更新卡号状态结束
			// 投保单录入
			if ("".equals(StrTool.cTrim(cLCContTable.getPrtNo()))) {
				errLog("获取保单印刷号失败。");
				return false;
			}
			
			// 套餐级别校验 add by licaiyan 2014-4-2
			if (!wrapCheck(cLCAppntTable,
					(LCInsuredTable) cLCInsuredList.get(0))) {

				return false;
			}
			if(!checknotnull(cLCContTable.getAppntBankAccNo())){
				errLog("银行帐号不能为空");
				return false;
			}
			if(!checknotnull(cLCContTable.getAppntAccName())){
				errLog("银行账户名不能为空");
				return false;
			}
			if(!checknotnull(cLCContTable.getAppntBankCode())){
				errLog("银行编码不能为空");
				return false;
			}
			if(!checknotnull(cLCContTable.getExPayMode())){
				errLog("续期缴费方式不能为空");
				return false;
			}
			
			
			
			
			
			String bankname=new ExeSQL().getOneValue("select bankname from ldbank where bankcode='"+cLCContTable.getAppntBankCode()+"'");
			if(bankname==null||"".equals(bankname)){
				errLog("未查询到对应的银行编码");
				return false;
			}
			if(!checknotnull(ryOtherInfo.getNativePlace())){
				errLog("国籍不能为空");
				return false;
			}
			if(!"ML".equals(ryOtherInfo.getNativePlace())){
				if(!checknotnull(ryOtherInfo.getNativePlace())){
					errLog("国籍非大陆时，国家必录");
					return false;
				}
			}
			if(!checknotnull(ryOtherInfo.getIDStartdate())||!checknotnull(ryOtherInfo.getIDEnddate())){
				errLog("证件起止日期不能为空");
				return false;
				
			}
			if(!checknotnull(ryOtherInfo.getGrpName())){
				errLog("投保税优系列产品时，工作单位不能为空！");
				return false;
				
			}
			
			if(!checknotnull(subtable.getTaxFlag())){
				errLog("税优投保标识不能为空");
				return false;
			}
			if(!checknotnull(subtable.getTaxPayerType())){
				errLog("个税征收方式不能为空");
				return false;
			}
			if("1".equals(subtable.getTaxFlag())){
				
				if("01".equals(subtable.getTaxPayerType())&&!checknotnull(subtable.getGTaxNo())&&!checknotnull(subtable.getGOrgancomCode())){
					errLog("税优标识为个人时，如个税征收方式选择为“代扣代缴”、则“单位税务登记证代码”或“单位社会信用代码”不能同时为空！");
					return false;
					
				}
				if("02".equals(subtable.getTaxPayerType())&&!checknotnull(subtable.getTaxNo())&&!checknotnull(subtable.getCreditCode())){
					errLog("税优标识为个人时，如个税征收方式选择为“自行申报”、则“个人税务登记证代码”或“个人社会信用代码”不能同时为空！");
					return false;
				}
				if(checknotnull(subtable.getGrpNo())){
					errLog("税优投保标识为“个人”时，不可录入团体编号！");
					return false;
				}
				
				
			}else{
				if(!"01".equals(subtable.getTaxPayerType())){
					errLog("税优标识为团体时，个税征收方式只能选择代扣代缴！");
					return false;
				}
				if(!checknotnull(subtable.getGrpNo())){
					errLog("税优标识为团体时，需录入团体客户信息");
					return false;
				}	
				if(!checknotnull(subtable.getGTaxNo())&&!checknotnull(subtable.getGOrgancomCode())){
					errLog("如个税征收方式选择为“代扣代缴”、则“单位税务登记证代码”或“单位社会信用代码”不能同时为空！");
					return false;
				}
				String grpname=new ExeSQL().getOneValue("select grpname from lsgrp where grpno='"+subtable.getGrpNo()+"'");
				if(grpname==null||"".equals(grpname)){
					errLog("未查询到对应的团体编码");
					return false;
				}
				if(checknotnull(subtable.getGOrgancomCode())){
					String checkGOrgancomCode = "select distinct 1 from lsgrp where OrgancomCode = '"
							+subtable.getGOrgancomCode()+"' and grpno <>'"+subtable.getGrpNo()+ "' with ur";
					String result0 = new ExeSQL().getOneValue(checkGOrgancomCode);
					if(result0!=null&&"1".equals(result0)){
						errLog("已存在拥有相同社会信用代码的团体数据，请修改!");
						return false;
					}
				}
				if(checknotnull(subtable.getGTaxNo())){
					String checkGTaxNo = "select distinct 1 from lsgrp where TaxRegistration = '"
							+subtable.getGTaxNo()+"' and grpno <>'"+subtable.getGrpNo()+ "' with ur";
					String result = new ExeSQL().getOneValue(checkGTaxNo);
					if(result!=null&&"1".equals(result)){
						errLog("已存在拥有相同税务登记号码的团体数据，请修改!");
						return false;
					}
				}
				if(!checknotnull(ryOtherInfo.getGrpNo())){
					errLog("团体编码不能为空！");
					return false;
				}
				String grpname1 = new ExeSQL().getOneValue("select grpname from lsgrp where grpno='"+ryOtherInfo.getGrpNo()+"' with ur");
				if(!grpname1.equals(ryOtherInfo.getGrpName())){
					errLog("该客户所在团体单位名称为"+grpname1+",与投保人工作单位不一致，请修改!");
					return false;
				}
				
			}
			if("01".equals(subtable.getTaxPayerType())){
				if(checknotnull(subtable.getTaxNo())||checknotnull(subtable.getCreditCode())){
					errLog("个税征收方式选择为“代扣代缴”时、不允许录入“个人税务登记证代码”或“个人社会信用代码”！");
					return false;
					
				}
				
			}
			if("02".equals(subtable.getTaxPayerType())){
				if(checknotnull(subtable.getGTaxNo())||checknotnull(subtable.getGOrgancomCode())){
					errLog("个税征收方式选择为“自行申报”时、不允许录入“单位税务登记证代码”或“单位社会信用代码”！");
					return false;
					
				}
				
			}
			
			String sql = "select contno from lccont where prtno = '"
					+ cLCContTable.getPrtNo() + "'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {// 第一次核保，保单号为空，以便后面生成保单号吗
				cLCContTable.setContNo("");
			} else {// 多次核保，先删除保单数据
				cLCContTable.setContNo(tSSRS.GetText(1, 1));
				if (!deleteData()) {
					errLog(mError);
					return false;
				}
			}
			String tAgentCode = "";
			if (null == cLCContTable.getAgentCode()
					|| "".equals(cLCContTable.getAgentCode())) {
				String AgentCodesql = "select agentcode from laagent where managecom = '"
						+ cLCContTable.getManageCom() + "' " +
						// "and agentcode like '%aaaaaa%' " +
						"and  branchtype='2' and branchtype2='01' and insideflag='0'";
				tAgentCode = new ExeSQL().getOneValue(AgentCodesql);
				if ("".equals(StrTool.cTrim(tAgentCode))) {
					mError = "根据管理机构获取销售人员信息失败！";
					errLog(mError);
					return false;
				} else {
					cLCContTable.setAgentCode(tAgentCode);
				}
			}
			//税优字段处理 by lw
			LCContSubSchema tLCContSubSchema = new LCContSubSchema();
			tLCContSubSchema.setPrtNo(cLCContTable.getPrtNo());
			tLCContSubSchema.setTaxFlag(subtable.getTaxFlag());

			tLCContSubSchema.setGrpNo(subtable.getGrpNo());
			tLCContSubSchema.setTaxNo(subtable.getTaxNo());
			tLCContSubSchema.setTaxPayerType(subtable.getTaxPayerType());

			tLCContSubSchema.setTransFlag(subtable.getTransFlag());
			tLCContSubSchema.setCreditCode(subtable.getCreditCode());
			//zxs 20190912
			tLCContSubSchema.setPolicySource(cCrsSaleInfo.getOriginChnl());
			SSRS s1 = new SSRS();
			ExeSQL e1 = new ExeSQL();
//			if ("2".equals(subtable.getTaxFlag())) {
				tLCContSubSchema.setGOrgancomCode(subtable.getGOrgancomCode());
				tLCContSubSchema.setGTaxNo(subtable.getGTaxNo());
				
//			}
			tLCContSubSchema.setAgediscountfactor(subtable
					.getAgediscountfactor());
			tLCContSubSchema.setSupdiscountfactor(subtable
					.getSupdiscountfactor());
			tLCContSubSchema.setGrpdiscountfactor(subtable
					.getGrpdiscountfactor());
			tLCContSubSchema.setTotaldiscountfactor(subtable
					.getTotaldiscountfactor());
			tLCContSubSchema.setPremMult(subtable.getPremMult());
			
			
			
			tLCContSubSchema.setBatchNo(s1.GetText(1, 6));
			tLCContSubSchema.setInsuredId(s1.GetText(1, 9));

			RYContBL ttWXContBL = new RYContBL(mGlobalInput, cLCContTable,cCrsSaleInfo,
					cLCAppntTable, cLCInsuredList, tLCContSubSchema,ryOtherInfo,tSpareFieldTable);
			mError = ttWXContBL.deal();
			if (!"".equals(mError)) {
				errLog(mError);
				return false;
			}
			//告知字段处理 bylw
			ccustomerList = cMsgInfos.getBodyByFlag("RYCustomerImpartTable");
			LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
			for (int i = 0; i < ccustomerList.size(); i++) {
				RYCustomerImpartTable ryCustomerImpartTable = (RYCustomerImpartTable) ccustomerList
						.get(i);
				String ImpartCode = ryCustomerImpartTable.getImpartCode();
				String impaetVer = ryCustomerImpartTable.getImpartVer();
				s1.Clear();
				String content;
				try {
					content = e1
							.getOneValue("SELECT impartcontent FROM LDImpart WHERE ImpartVer = '"
									+ impaetVer
									+ "' and impartcode='"
									+ ImpartCode
									+ "'");
				} catch (Exception e) {
					errLog("告知信息获取失败！");
					return false;
				}
				LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
				
				tLCCustomerImpartSchema.setCustomerNoType("I");
				
				tLCCustomerImpartSchema.setImpartCode(ImpartCode);
				if("110".equals(ImpartCode)){
					tLCCustomerImpartSchema.setImpartContent("过去一年内是否有过医院门诊检查或治疗？○是  ○否");
				}else{
					tLCCustomerImpartSchema.setImpartContent(content);
				}
				
				if(ryCustomerImpartTable.getImpartParamModle()==null){
					tLCCustomerImpartSchema
					.setImpartParamModle("");
				}else{
					tLCCustomerImpartSchema
					.setImpartParamModle(ryCustomerImpartTable
							.getImpartParamModle());
				}
				
				tLCCustomerImpartSchema.setImpartVer(impaetVer);
				tLCCustomerImpartSchema.setPrtNo(cLCContTable.getPrtNo());
				tLCCustomerImpartSchema.setMakeDate(PubFun.getCurrentDate());
				tLCCustomerImpartSchema.setMakeTime(PubFun.getCurrentTime());
				tLCCustomerImpartSchema.setModifyDate(PubFun.getCurrentDate());
				tLCCustomerImpartSchema.setModifyTime(PubFun.getCurrentTime());
				tLCCustomerImpartSchema.setOperator(mGlobalInput.Operator);
				tLCCustomerImpartSchema.setGrpContNo("000000000000000");
				tLCCustomerImpartSet.add(tLCCustomerImpartSchema);

			}
			cDetailList = cMsgInfos
					.getBodyByFlag("RYCustomerImpartDetailTable");
			LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = new LCCustomerImpartDetailSet();
			for (int i = 0; i < cDetailList.size(); i++) {
				RYCustomerImpartDetailTable ryCustomerImpartTable = (RYCustomerImpartDetailTable) cDetailList
						.get(i);
				String ImpartCode = ryCustomerImpartTable.getImpartCode();
				if(ImpartCode==null||"".equals(ImpartCode)){
					continue;
				}
				String impaetVer = ryCustomerImpartTable.getImpartVer();
				s1.Clear();
				String content;
				try {
					content = e1
							.getOneValue("SELECT impartcontent FROM LDImpart WHERE ImpartVer = '"
									+ impaetVer
									+ "' and impartcode='"
									+ ImpartCode
									+ "'");
				} catch (Exception e) {
					errLog("告知信息获取失败！");
					return false;
				}
				
				LCCustomerImpartDetailSchema tLCCustomerImpartDetailSchema = new LCCustomerImpartDetailSchema();
				
				tLCCustomerImpartDetailSchema.setPrtNo(cLCContTable.getPrtNo());
				tLCCustomerImpartDetailSchema.setCustomerNoType("I");
				tLCCustomerImpartDetailSchema.setImpartVer(impaetVer);
				tLCCustomerImpartDetailSchema.setImpartCode(ImpartCode);
				if("110".equals(ImpartCode)){
					tLCCustomerImpartDetailSchema.setImpartDetailContent("过去一年内是否有过医院门诊检查或治疗？○是  ○否");
				}else{
					tLCCustomerImpartDetailSchema.setImpartDetailContent(content);
				}
				
			
				tLCCustomerImpartDetailSchema.setSubSerialNo(""+i);
				tLCCustomerImpartDetailSchema
						.setDiseaseContent(ryCustomerImpartTable
								.getDiseaseContent());
				tLCCustomerImpartDetailSchema
						.setStartDate(ryCustomerImpartTable.getStartDate());
				tLCCustomerImpartDetailSchema.setEndDate(ryCustomerImpartTable
						.getEndDate());
				tLCCustomerImpartDetailSchema.setProver(ryCustomerImpartTable
						.getProver());
				tLCCustomerImpartDetailSchema
						.setCurrCondition(ryCustomerImpartTable
								.getCurrCondition());
				tLCCustomerImpartDetailSchema.setIsProved(ryCustomerImpartTable
						.getIsProved());
				tLCCustomerImpartDetailSchema.setMakeDate(PubFun.getCurrentDate());
				tLCCustomerImpartDetailSchema.setMakeTime(PubFun.getCurrentTime());
				tLCCustomerImpartDetailSchema.setModifyDate(PubFun.getCurrentDate());
				tLCCustomerImpartDetailSchema.setModifyTime(PubFun.getCurrentTime());
				tLCCustomerImpartDetailSchema.setOperator(mGlobalInput.Operator);
				tLCCustomerImpartDetailSchema.setGrpContNo("000000000000000");
				tLCCustomerImpartDetailSet.add(tLCCustomerImpartDetailSchema);
			}
			RYContInsuredIntlBL ttWxContInsuredIntlBL = new RYContInsuredIntlBL(
					mGlobalInput, tMsgHead, cLCContTable, cLCAppntTable,
					cLCInsuredList, cWrapTable, cLCBnfList, cWrapParamList,tLCCustomerImpartSet,tLCCustomerImpartDetailSet,tSpareFieldTable);
			ttWxContInsuredIntlBL.syOtherTable=ryOtherInfo;
			mError = ttWxContInsuredIntlBL.deal();
			if (!"".equals(mError)) {
				errLog(mError);
				return false;
			}
			
			s1.Clear();
			s1 = e1.execSQL("select contno,insuredno from lccont where prtno='"
					+ cLCContTable.getPrtNo() + "'");

			String Contno = s1.GetText(1, 1);
			String insuredno=s1.GetText(1, 2);
			
			
			
			// -----------B2C#195fromyzfstart
			// 封装数据
			WxContRelaInfoBL ttWXContRelaInfoBL = new WxContRelaInfoBL(
					tMsgHead, cLCContTable);
			mError = ttWXContRelaInfoBL.deal();
			if (!"".equals(mError)) {
				errLog(mError);
				return false;
			}
			
			
			
			WrapParamTable rt = (WrapParamTable) cWrapParamList.get(0);
			String riskcode=rt.getRiskCode();
			String tsql=new ExeSQL().getOneValue("select 1 from ldcode where codetype='sypremnult' and code='"+ riskcode + "' ");
			if("1".equals(tsql)){
				if(!checknotnull(subtable.getPremMult())){
					errLog("风险保险费档次不能为空");
					return false;
				}
				String premmult=subtable.getPremMult();
				
				String mult="";
				for(int i=0;i<cWrapParamList.size();i++){
					WrapParamTable rt1 = (WrapParamTable) cWrapParamList.get(i);
					if("Mult".equals(rt1.getCalfactor())){
						mult=rt1.getCalfactorValue();
						break;
						
					}
					
				}
				if("".equals(mult)){
					errLog("算费要素中档次不能为空");
					return false;
				}
				if(!mult.equals(premmult)){
					errLog("风险保险费档次与算费要素中档次录入不符");
					return false;					
				}
				String ttsql=new ExeSQL().getOneValue("select 1 from LCCustomerImpartParams where prtno='"+ cLCContTable.getPrtNo() + "' "
					       + "and impartcode='170' and impartparam='N' and impartparamno='5' ");
				if("1".equals(ttsql)){
					if("2".equals(premmult)){
						errLog("被保险人医疗费用支付方式与风险保险费档次不匹配！");
						return false;	
					}
					
				}
				String ttsql2=new ExeSQL().getOneValue( "select 1 "
			          	   + " from lcpol lc "
			          	   + " where lc.prtno='" + cLCContTable.getPrtNo() + "' "
			          	   + " and exists (select 1 from ldcode where codetype='sypremnult' and code=lc.riskcode) "
			          	   + " and exists (select 1 from LCCustomerImpartParams where impartcode ='260' and impartver='001' and customerno=lc.insuredno and contno=lc.contno and impartparam='Y')"
			          	   + " and lc.amnt='250000' ");
				if("1".equals(ttsql2)){
					errLog("承保为税优产品，被保人投保保额不符合要求！");
					return false;	
				}
				
			}else{
				if(checknotnull(subtable.getPremMult())){
					errLog("险种不允许录入风险保险费档次");
					return false;
				}
			}
			// -----------fromyzfend
			TransferData mTransferData = new TransferData();
			String tContNo = Contno;
			String tPrtNo = cLCContTable.getPrtNo();

			mTransferData.setNameAndValue("ContNo", tContNo);
			mTransferData.setNameAndValue("PrtNo", tPrtNo);
			VData tVData1 = new VData();
			tVData1.add(mTransferData);
			tVData1.add(mGlobalInput);
			TaxCheckContUI tTaxCheckContUI = new TaxCheckContUI();//调用处理类
			if (!tTaxCheckContUI.submitData(tVData1, "check")) {
				String Content = tTaxCheckContUI.mErrors.getError(0).errorMessage;
				errLog(Content);			
				
				return false;
			}
//			if(checknotnull(subtable.getGrpNo())){
//				String grpname=new ExeSQL().getOneValue("select grpname from lsgrp where grpno='"+subtable.getGrpNo()+"'");
//				String customerno=new ExeSQL().getOneValue("select appntno from lccont where prtno='"+cLCContTable.getPrtNo()+"'");
//				new ExeSQL().execUpdateSQL("update lcaddress set grpname='"+grpname+"' where customerno='"+customerno+"'");
//				
//			}
			if(checknotnull(ryOtherInfo.getGrpNo())){
				String customerno=new ExeSQL().getOneValue("select appntno from lccont where prtno='"+cLCContTable.getPrtNo()+"'");
				String tNoLimit = PubFun.getCurrentDate2()+PubFun.getCurrentTime2();
				String tBatchNo = "TAX" + tNoLimit + PubFun1.CreateMaxNo("T"+tNoLimit,2);
				VData tVData = new VData();
				TransferData tTransferData = new TransferData();
				tTransferData.setNameAndValue("CustomerNo",customerno);
				tTransferData.setNameAndValue("BatchNo",tBatchNo);
				tVData.add(tTransferData);
				
				GetPlatformCustomerNo tGetPlatformCustomerNo = new GetPlatformCustomerNo();
				if(!tGetPlatformCustomerNo.submitData(tVData, "ZBXPT")){
					errLog(tGetPlatformCustomerNo.mErrors.getLastError());
					return false;
				}
				
				LSBatchInfoSchema tLSBatchInfoSchema=new LSBatchInfoSchema();
				tLSBatchInfoSchema.setApplyDate(PubFun.getCurrentDate());
				tLSBatchInfoSchema.setManageCom(mGlobalInput.ComCode);
				tLSBatchInfoSchema.setOperator(mGlobalInput.Operator);
				tLSBatchInfoSchema.setBatchState("3");
				tLSBatchInfoSchema.setMakeDate(PubFun.getCurrentDate());
				tLSBatchInfoSchema.setMakeTime(PubFun.getCurrentTime());
				tLSBatchInfoSchema.setModifyDate(PubFun.getCurrentDate());
				tLSBatchInfoSchema.setModifyTime(PubFun.getCurrentTime());
				tLSBatchInfoSchema.setGrpNo(ryOtherInfo.getGrpNo());
				tLSBatchInfoSchema.setBatchNo(tBatchNo);
				MMap tMMap = new MMap();
				tMMap.put(tLSBatchInfoSchema, "INSERT");
				VData tData = new VData();

				tData.add(tMMap);

				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tData, "INSERT")) {					
					
				}
				new ExeSQL().execUpdateSQL("update lccontsub set batchno='"+tBatchNo+"' where prtno='"+cLCContTable.getPrtNo()+"'");
				
				
				
				
			}
			//税优唯一性校验
			mError=checkTax();
			if(!"".equals(mError)){
				System.out.println("删除新单开始，Prtno为：" + cLCContTable.getPrtNo());
				VData taxVData = new VData();
				String taxDeleteReason = "被保人存在生效税优保单，需要进行新单删除";
				TransferData taxTransferData = new TransferData();
				LCContDB taxLCContDB = new LCContDB();
				taxLCContDB.setPrtNo(cLCContTable.getPrtNo());
				LCContSchema taxLCContSchema = new LCContSchema();
				LCContSet taxLCContSet = taxLCContDB.query();
				taxLCContSchema = taxLCContSet.get(1);
				taxTransferData.setNameAndValue("DeleteReason", taxDeleteReason);
				taxVData.add(taxLCContSchema);
				taxVData.add(taxTransferData);
				taxVData.add(mGlobalInput);
				ContDeleteUI tContDeleteUI = new ContDeleteUI();
				tContDeleteUI.submitData(taxVData, "DELETE");
				System.out.println("删除新单结束");
//				mError="该被保人存在生效的税优保单！";
				errLog(mError);
				return false;
			}
			
			// 自核+复核
			SYAutoCheck ttWxAutoCheck = new SYAutoCheck(
					cLCContTable.getPrtNo(), mGlobalInput);
			mError = ttWxAutoCheck.deal();
			boolean SuccFlag = createSharedMark();
			if(!SuccFlag){
				mError = "生成共享数据失败！";
			}
			if (!"".equals(mError)) {
				String mSQL = "select UWFlag from LCCont where ContNo='" + Contno + "' with ur";
				ExeSQL mExeSQL = new ExeSQL();
				if (mExeSQL.getOneValue(mSQL).equals("5")) { 
					String tErrCode = "99999999";
					new ExeSQL().execUpdateSQL("update lccont set ApproveFlag='9' , approvedate=current date where prtno='"+cLCContTable.getPrtNo()+"'");
			        MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
			        tResMsgHead.setState("02");
			        tResMsgHead.setErrCode(tErrCode);
			        tResMsgHead.setErrInfo(mError);					
					// 组织返回报文
					getWrapParmList(cRiskWrapCode);
					if (!PrepareInfo()) {
						errLog(mError);
						return false;
					}
					getXmlResult();
					return false;
				}else{
					System.out.println("删除新单开始，Prtno为：" + cLCContTable.getPrtNo());
					VData tVData = new VData();
					String tDeleteReason = "复杂产品自核失败需要进行新单删除";
					TransferData tTransferData = new TransferData();
					LCContDB tLCContDB = new LCContDB();
					tLCContDB.setPrtNo(cLCContTable.getPrtNo());
					LCContSchema tLCContSchema = new LCContSchema();
					LCContSet tLCContSet = tLCContDB.query();
					tLCContSchema = tLCContSet.get(1);
					tTransferData.setNameAndValue("DeleteReason", tDeleteReason);
					tVData.add(tLCContSchema);
					tVData.add(tTransferData);
					tVData.add(mGlobalInput);
					ContDeleteUI tContDeleteUI = new ContDeleteUI();
					tContDeleteUI.submitData(tVData, "DELETE");
					System.out.println("删除新单结束");
					errLog(mError);
					return false;
				}
				
			}
			// 组织返回报文
			getWrapParmList(cRiskWrapCode);
			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {
		// 保单节点
		NewLCContTable newLCContTable = replace(cLCContTable);
		putResult("LCContTable", newLCContTable);
		//交叉销售信息节点
		putResult("CrsSaleInfo",cCrsSaleInfo);
		// 投保人节点
		putResult("LCAppntTable", cLCAppntTable);
		// 所有被保人节点
		for (int i = 0; i < cLCInsuredList.size(); i++) {
			putResult("LCInsuredTable", (LCInsuredTable) cLCInsuredList.get(i));
		}
		// 套餐节点
		putResult("WrapTable", cWrapTable);
		// 算费参数节点
		for (int i = 0; i < cWrapParamList.size(); i++) {
			putResult("WrapParamTable", (WrapParamTable) cWrapParamList.get(i));
		}
		// 受益人节点
		// for(int i=0;i<cLCBnfList.size();i++){
		// putResult("LCBnfTable", (LCBnfTable)cLCBnfList.get(i));
		// }
	}

	public NewLCContTable replace(LCContTable mLcContTable) {
		NewLCContTable newlc = new NewLCContTable();
		newlc.setAgentCode(mLcContTable.getAgentCode());
		newlc.setAgentCom(mLcContTable.getAgentCom());
		newlc.setCInValiDate(mLcContTable.getCInValiDate());
		newlc.setCInValiTime(mLcContTable.getCInValiTime());
		newlc.setContNo(mLcContTable.getContNo());
		newlc.setCValiDate(mLcContTable.getCValiDate());
		newlc.setCValiTime(mLcContTable.getCValiTime());
		newlc.setManageCom(mLcContTable.getManageCom());
		newlc.setPayIntv(mLcContTable.getPayIntv());
		newlc.setPayMode(mLcContTable.getPayMode());
		newlc.setPolApplyDate(mLcContTable.getPolApplyDate());
		newlc.setPrtNo(mLcContTable.getPrtNo());
		newlc.setSaleChnl(mLcContTable.getSaleChnl());

		return newlc;

	}

	// 获取保单信息
	public boolean PrepareInfo() {
		String sql = "select contno,amnt,prem,appntno,CValiDate,cinvalidate from lccont where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败！";
			return false;
		} else {
			cWrapTable.setAmnt(tSSRS.GetText(1, 2));
			cWrapTable.setPrem(tSSRS.GetText(1, 3));
			cLCContTable.setContNo(tSSRS.GetText(1, 1));
			cLCAppntTable.setReAppntNo(tSSRS.GetText(1, 4));
			cLCContTable.setCValiDate(tSSRS.GetText(1, 5));
			cLCContTable.setCInValiDate(tSSRS.GetText(1, 6));
		}
		// 获取保险期间
		String InsuYearsql = "select insuyear,insuyearflag from lcpol where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		SSRS tInsuYearSSRS = new ExeSQL().execSQL(InsuYearsql);
		if (tInsuYearSSRS == null || tInsuYearSSRS.getMaxRow() <= 0) {
			mError = "获取保险期间信息失败！";
			return false;
		} else {
			cWrapTable.setInsuYear(tInsuYearSSRS.GetText(1, 1));
			cWrapTable.setInsuYearFlag(tInsuYearSSRS.GetText(1, 2));
		}
		// 获取被保人客户号码返回给电子商务
		String InsuredNOSql = "select insuredno,sequenceno from lcinsured where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		SSRS InsuredNoSSRS = new ExeSQL().execSQL(InsuredNOSql);
		if (InsuredNoSSRS == null || InsuredNoSSRS.getMaxRow() <= 0) {
			mError = "获取被保人信息失败！";
			return false;
		} else {
			List tLCInsuredList = new ArrayList();
			for (int i = 0; i < cLCInsuredList.size(); i++) {
				LCInsuredTable tLCInsuredTable = (LCInsuredTable) cLCInsuredList
						.get(i);
				for (int j = 1; j <= InsuredNoSSRS.MaxRow; j++) {
					if (InsuredNoSSRS.GetText(j, 2).equals(
							tLCInsuredTable.getInsuredNo())) {
						tLCInsuredTable.setReInsuredNo(InsuredNoSSRS.GetText(j,
								1));
					}
				}
				tLCInsuredList.add(tLCInsuredTable);
			}
			cLCInsuredList = tLCInsuredList;
		}
		// 获取折扣
		String FeeRateSql = "select calfactorvalue from lcriskdutywrap where prtno = '"
				+ cLCContTable.getPrtNo() + "' and calfactor = 'FeeRate'";
		SSRS FeeRateSSRS = new ExeSQL().execSQL(FeeRateSql);
		if (FeeRateSSRS != null && FeeRateSSRS.getMaxRow() > 0) {
			cWrapTable.setFeeRate(tSSRS.GetText(1, 1));
		}
		return true;
	}

	/**
	 * deleteData
	 *
	 * @return boolean
	 */
	private boolean deleteData() {
		String wherePart_ContNo = " and ContNo = '" + cLCContTable.getContNo()
				+ "'";
		MMap DeleteMap = new MMap();
		DeleteMap.put("delete from lccont where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from lcpol where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from lcduty where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from lcprem where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from lcget where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from lcappnt where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from lcinsured where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from LCCustomerImpart where 1=1 "
				+ wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCCustomerImpartParams where 1=1 "
				+ wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCCustomerImpartDetail where 1=1 "
				+ wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCNation where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from LCBnf where 1=1 " + wherePart_ContNo,
				"DELETE");
		DeleteMap.put("delete from LCRiskDutyWrap where 1=1 "
				+ wherePart_ContNo, "DELETE");

		// 　数据提交、保存
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("多次复核时，先删除原有保单信息！");
		VData mInputData = new VData();
		mInputData.add(DeleteMap);
		if (!tPubSubmit.submitData(mInputData, "")) {
			// @@错误处理
			mError = "多次核保时删除原有数据失败";
			return false;
		}

		return true;
	}

	// 获取可用单证号并将申请单证号状态置为14
	public boolean getCardNo(String aRiskWrapCode) {
		// 根据套餐编码获取单证编码
		String getCertifycodeSql = "select * from LMCardRisk where riskcode = '"
				+ aRiskWrapCode + "'";
		String Certifycode = new ExeSQL().getOneValue(getCertifycodeSql);
		if ("".equals(Certifycode) || Certifycode == null) {
			mError = "获取可用单证号码失败！";
			return false;
		}
		// 获取可用卡号，卡号即为印刷号
		String searchCardnoSQL = "select lzcn.cardno,lzcn.cardpassword from LZCardNumber lzcn "
				+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode "
				+ "and lzc.StartNo = lzcn.CardSerNo "
				+ "and lzc.EndNo = lzcn.CardSerNo "
				+ "where  lzc.State in ('10','11') "
				+ "and lzc.certifycode = '"
				+ Certifycode
				+ "' fetch first 1 rows only ";
		SSRS CardSSRS = new ExeSQL().execSQL(searchCardnoSQL);
		if (CardSSRS == null || CardSSRS.MaxRow <= 0) {
			mError = "获取可用印刷号码失败！";
			return false;
		}
		String cardNo = CardSSRS.GetText(1, 1);
		if (cardNo.equals("") || cardNo == null) {
			mError = "获取可用印刷号码失败！";
			return false;
		}
		String lzcardPKSql = " select  lzc.CertifyCode, lzc.SubCode, lzc.riskcode,lzc.RiskVersion ,lzc.StartNo,lzc.endno,lzc.state "
				+ "from LZCardNumber lzcn inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
				+ "and lzc.EndNo = lzcn.CardSerNo where 1 = 1 	and lzcn.CardNo = '"
				+ cardNo + "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS lzcardPKSSRS = tExeSQL.execSQL(lzcardPKSql);
		String modifyStateSql = null;
		if (lzcardPKSSRS.MaxRow > 0) {
			String CertifyCode = lzcardPKSSRS.GetText(1, 1);
			String SubCode = lzcardPKSSRS.GetText(1, 2);
			String RiskCode = lzcardPKSSRS.GetText(1, 3);
			String RiskVersion = lzcardPKSSRS.GetText(1, 4);
			String StartNo = lzcardPKSSRS.GetText(1, 5);
			String EndNo = lzcardPKSSRS.GetText(1, 6);
			modifyStateSql = "update lzcard set state='14' where CertifyCode='"
					+ CertifyCode + "' " + "and SubCode='" + SubCode
					+ "' and RiskCode='" + RiskCode + "' and RiskVersion='"
					+ RiskVersion + "' " + "and StartNo='" + StartNo
					+ "' and EndNo='" + EndNo + "'";
		}
		MMap updateStateMap = new MMap();
		updateStateMap.put(modifyStateSql, "UPDATE");
		// 　数据提交、保存
		PubSubmit tPubSubmit = new PubSubmit();
		VData mInputData = new VData();
		mInputData.add(updateStateMap);
		if (!tPubSubmit.submitData(mInputData, "")) {
			// @@错误处理
			mError = "获取印刷号后修改状态时失败！";
			return false;
		}
		cLCContTable.setPrtNo(cardNo);
		return true;
	}

	// 获取每个责任对应的保额及保费信息
	public void getWrapParmList(String pRiskWrapCode) {
		String contnoSql = "select contno from lccont where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		String contno = new ExeSQL().getOneValue(contnoSql);
		String sql = "select a.riskcode,b.dutycode, a.amnt,b.amnt,a.prem,b.prem from lcpol a left join lcduty b on a.polno = b.polno where a.contno ='"
				+ contno + "' ";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS != null && tSSRS.getMaxRow() > 0) {
			cWrapParamList = new ArrayList();
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				for (int j = 1; j <= 2; j++) {
					WrapParamTable tWrapParamTable = new WrapParamTable();
					tWrapParamTable.setContNo(cLCContTable.getContNo());
					tWrapParamTable.setRiskWrapCode(pRiskWrapCode);
					tWrapParamTable.setRiskCode(tSSRS.GetText(i, 1));
					tWrapParamTable.setDutyCode(tSSRS.GetText(i, 2));
					if (j == 1) {
						tWrapParamTable.setCalfactor("Amnt");
						tWrapParamTable.setCalfactorValue(String.valueOf(tSSRS
								.GetText(i, 4)));
					} else {
						tWrapParamTable.setCalfactor("Prem");
						tWrapParamTable.setCalfactorValue(String.valueOf(tSSRS
								.GetText(i, 6)));
					}
					cWrapParamList.add(tWrapParamTable);
				}
			}
		}
	}

	/**
	 * 套餐级别的校验 add by licaiyan
	 * 
	 * @param appntTable
	 *            投保人
	 * @param insuTable
	 *            被保人
	 * @param wrapParamList
	 *            算费要素
	 * @return
	 */
	private boolean wrapCheck(LCAppntTable appntTable, LCInsuredTable insuTable) {
		boolean flag = true;
		LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
		tLMCheckFieldDB.setRiskCode(this.cRiskWrapCode);
		tLMCheckFieldDB.setFieldName("ETBINSERT");
		LMCheckFieldSet tLMCheckFieldSet = tLMCheckFieldDB.query();

		String amntStr = "";
		for (int k = 0; k < cWrapParamList.size(); k++) {// 获取保额
			WrapParamTable rt = (WrapParamTable) cWrapParamList.get(k);
			if ("Amnt".equals(rt.getCalfactor()))
				amntStr = rt.getCalfactorValue();
		}
		for (int i = 1; i <= tLMCheckFieldSet.size(); i++) {
			System.out.println("套餐校验----------------------" + i);
			LMCheckFieldSchema tLMCheckFieldSchema = tLMCheckFieldSet.get(i);
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			tLMCalModeDB.setCalCode(tLMCheckFieldSchema.getCalCode());
			LMCalModeSet tLMCalModeSet = new LMCalModeSet();
			tLMCalModeSet = tLMCalModeDB.query();
			for (int j = 1; j <= tLMCalModeSet.size(); j++) {
				LMCalModeSchema tLMCalModeSchema = tLMCalModeSet.get(j);

				// 目前只支持一个被保人
				// WFInsuListSchema insuListSchema = insuTable;
				int tAppntAge = PubFun.calInterval(insuTable.getBirthday(),
						PubFun.getCurrentDate(), "Y");
				Calculator cal = new Calculator();
				cal.setCalCode(tLMCalModeSchema.getCalCode());
				cal.addBasicFactor("InsuredName", insuTable.getName());
				cal.addBasicFactor("Sex", insuTable.getSex());
				cal.addBasicFactor("InsuredBirthday", insuTable.getBirthday());
				cal.addBasicFactor("IDType", insuTable.getIDType());
				cal.addBasicFactor("IDNo", insuTable.getIDNo());
				cal.addBasicFactor("RiskWrapCode", this.cRiskWrapCode);
				cal.addBasicFactor("AppAge", tAppntAge + "");
				cal.addBasicFactor("OccupationType",
						insuTable.getOccupationType());
				cal.addBasicFactor("Cvalidate",
						this.cLCContTable.getCValiDate());

				cal.addBasicFactor("Amnt", amntStr);

				String result = cal.calculate();

				if (!result.equals("0")) {
					System.out.println("校验结果为false----------------------");
					flag = false;
					errLog(tLMCheckFieldSchema.getMsg());
				}

			}
		}
		return flag;
	}
	private boolean checknotnull(String s1){
		if(s1==null||"".equals(s1)){
			return false;
		}else{
			return true;
		}
		
	}
	private boolean getCheckResult(String CustomerNo){
		String tNoLimit = PubFun.getCurrentDate2()+PubFun.getCurrentTime2();
		String tBatchNo = "TAX" + tNoLimit + PubFun1.CreateMaxNo("T"+tNoLimit,2);
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CustomerNo",CustomerNo);
		tTransferData.setNameAndValue("BatchNo",tBatchNo);
		tVData.add(tTransferData);
		
		GetPlatformCustomerNo tGetPlatformCustomerNo = new GetPlatformCustomerNo();
		tGetPlatformCustomerNo.submitData(tVData, "ZBXPT");
		
       	return true;
	}
	private void creatbatchinfo(){
		LSBatchInfoSchema tLSBatchInfoSchema=new LSBatchInfoSchema();
		tLSBatchInfoSchema.setApplyDate(PubFun.getCurrentDate());
		tLSBatchInfoSchema.setManageCom(mGlobalInput.ComCode);
		tLSBatchInfoSchema.setOperator(mGlobalInput.Operator);
		tLSBatchInfoSchema.setBatchState("3");
		tLSBatchInfoSchema.setMakeDate(PubFun.getCurrentDate());
		tLSBatchInfoSchema.setMakeTime(PubFun.getCurrentTime());
		tLSBatchInfoSchema.setModifyDate(PubFun.getCurrentDate());
		tLSBatchInfoSchema.setModifyTime(PubFun.getCurrentTime());
		tLSBatchInfoSchema.setGrpNo("");
		tLSBatchInfoSchema.setBatchNo("");
		
		
		
		
	}
	
	public boolean checkAddress(int flag,String province,String city,String county, String street,String community) {
		String str="";
		String province2 ;
		String city2;
		String county2;
		if(flag==1) {
			str="投保人";
		}else {
			str="被保人";
		}
		if(province==null||city==null||county==null||street==null||community==null||province.equals("")||city.equals("")||county.equals("")||street.equals("")||community.equals("")) {
			errLog(str+"联系地址填写不完整，请核查！");
			return false;
		}
		ExeSQL esql= new ExeSQL();
		SSRS pSSRS = esql.execSQL("select codename from ldcode1 where codetype='province1' and code='"+province+"' ");
		if(pSSRS==null || pSSRS.getMaxRow()==0) {
			errLog(str + "省（自治区直辖市）信息录入不正确，请录入省（自治区直辖市）编码！");
			return false;
		}else {
			province2=pSSRS.GetText(1, 1);
		}
		pSSRS.Clear();
		pSSRS =  esql.execSQL("select codename from ldcode1 where codetype='city1' and code='"+city+"' ");
	   if(pSSRS==null ||pSSRS.getMaxRow()==0){
		   errLog(str +"市信息录入不正确，请录入市编码！");
		   return false;
	   }else {
		   city2=pSSRS.GetText(1, 1);
	   }
	   pSSRS.Clear();
	   pSSRS =  esql.execSQL("select codename from ldcode1 where codetype='county1' and code='"+county+"' ");
	   if(pSSRS==null ||pSSRS.getMaxRow()==0){
		   errLog(str +"县（区）信息录入不正确，请录入县（区）编码！");
		   return false;
	   }else {
		   county2=pSSRS.GetText(1, 1);
	   }
	   pSSRS.Clear();
	   pSSRS =  esql.execSQL("select 1 from ldcode1 where codetype='province1' and code='"+province+"' "
			   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+city+"' )");
	   if(pSSRS == null || pSSRS.getMaxRow() == 0){
		   errLog(str +"省、市地址级联关系不正确，请检查！");
		   return false;
	   }
	   pSSRS.Clear();
	   pSSRS =  esql.execSQL("select code1 from ldcode1 where codetype='city1' and code='"+city+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+county+"')");
	   if(pSSRS == null || pSSRS.getMaxRow() == 0){
		   errLog(str +"市、县地址级联关系不正确，请检查！");
		   return false;
	   }
	   String homeAddress="";
	   if(city2.equals("空")) {
		   homeAddress = province2+street+community;
	   }else if(!city2.equals("空")&&county2.equals("空")) {
		   homeAddress = province2+city2+street+community;
	   }else if (!city2.equals("空")&&!county2.equals("空")) {
		   homeAddress = province2+city2+county2+street+community;
	   }
	   if(flag==1) {
		   cLCAppntTable.setHomeAddress(homeAddress);
	   }else {
			LCInsuredTable lcInsuredTable=(LCInsuredTable) cLCInsuredList.get(0);
			lcInsuredTable.setHomeAddress(homeAddress);
	   }
		return true;
	}
	public static void main(String[] args) {
		String idno="211323198005020013";
		char [] c1=idno.toCharArray();
		int [] a11= new int[]{7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2};
		String [] a2=new String[]{"1","0","X","9","8","7","6","5","4","3","2"};
		int sum=0;
		for(int i=0;i<17;i++){
//			System.out.print(c1[i]+"*"+a11[i]+"+");
			int a=Integer.parseInt(String.valueOf(c1[i]));
			sum+=a*a11[i];			
		}
		
		System.out.println(sum);
		int a=sum%11;
	
		System.out.println(a2[a]);
	
		if(a2[a].equals(String.valueOf(c1[17]))){
			System.out.println("验证通过");
		}else{
			System.out.println("最后一位验证出错");
		}
		
		
		
	}
	
	/** 
	 * 税优唯一性校验
	 * author renwei
	 * **/
	public String checkTax(){
		String error="";
		String Sql="select b.prtno from "
				+ "(select * from lcpol where prtno='"+cLCContTable.getPrtNo()+"') as a,lcpol as b "
				+ "where b.riskcode in (select riskcode from lmriskapp where  TaxOptimal='Y' and (enddate is null or enddate > current date )) "
				+ "and a.insuredno = b.insuredno "
				+ "and a.polno <> b.polno "
				+ "and b.enddate > a.cvalidate "
				+ "and b.stateflag in ('0', '1', '2')";
		SSRS tSSRS=new ExeSQL().execSQL(Sql);
		if(tSSRS!=null && tSSRS.getMaxRow()>0){
			for(int i=1;i<=tSSRS.getMaxRow();i++){
				error=error+tSSRS.GetText(i, 1)+"，";
			}
			error=error.substring(0, error.lastIndexOf("，"));
			return error="该被保人存在已投保或已生效税优保单，印刷号为："+error;
		}
		return error;
	}
	
	//创建共享标识数据
    public boolean createSharedMark(){
         String theCurrentDate = PubFun.getCurrentDate();
         String theCurrentTime = PubFun.getCurrentTime();
         MMap mMap = new MMap();
         mMap.put("delete from LCPersonTrace where  ContractNo='"+cLCContTable.getPrtNo()+"' ", "DELETE");
         VData vData = new VData();
         vData.add(mMap);
         PubSubmit pubSubmit = new PubSubmit();
         if (!pubSubmit.submitData(vData, ""))
         {
            mError = "生成共享标识数据失败！";
            return false;
         }
         mMap = new MMap();
         vData.clear();
         if(!appntLcPersonTrace.equals(insuredLcPersonTrace)){
        	mError = "投保人和被保人的共享标识数据不一样，请确认！";
        	return false; 
         }
    	LCAppntDB tlLcAppntDB = new LCAppntDB();
    	tlLcAppntDB.setPrtNo(cLCContTable.getPrtNo());
    	LCAppntSet tLcAppntSet =  tlLcAppntDB.query();
    	LCPersonTraceSchema tLcPersonTraceSchema = new LCPersonTraceSchema();
    	tLcPersonTraceSchema = appntLcPersonTrace;
    	tLcPersonTraceSchema.setCustomerNo(tLcAppntSet.get(1).getAppntNo());
    	tLcPersonTraceSchema.setContractNo(cLCContTable.getPrtNo());
    	 SSRS tSSRS2 = new SSRS();
         String sql1 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
                 +tLcPersonTraceSchema.getCustomerNo()+ "'";
         ExeSQL tExeSQL1 = new ExeSQL();
         tSSRS2 = tExeSQL1.execSQL(sql1);
         Integer firstinteger1 = Integer.valueOf(tSSRS2.GetText(1, 1));
         int tTraceNo = firstinteger1.intValue() + 1;
         Integer sTraceNo = new Integer(tTraceNo);
         String mTraceNo = sTraceNo.toString();
         tLcPersonTraceSchema.setTraceNo(mTraceNo);
         tLcPersonTraceSchema.setCompanySource("2");//人保健康
         tLcPersonTraceSchema.setInstitutionSource(tLcAppntSet.get(1).getManageCom());
         tLcPersonTraceSchema.setAuthVersion("1.0");
         tLcPersonTraceSchema.setSendDate(theCurrentDate);
         tLcPersonTraceSchema.setSendTime(theCurrentTime);
         tLcPersonTraceSchema.setModifyDate(theCurrentDate);
         tLcPersonTraceSchema.setModifyTime(theCurrentTime);
         mMap.put(tLcPersonTraceSchema, "INSERT");
         vData.add(mMap);
         PubSubmit pubSubmit1 = new PubSubmit();
         if (!pubSubmit1.submitData(vData, ""))
         {
        	mError = "生成共享标识数据失败！";
            return false;
         } 
         appntLcPersonTrace = new LCPersonTraceSchema();
         insuredLcPersonTrace = new LCPersonTraceSchema();
         mMap=new MMap();
         vData.clear();
    return true;	
    }

}
