package com.cbsws.xml.ctrl.complexpds;

import java.util.List;

import org.jdom.Document;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.LCContGetPolTable;
import com.cbsws.obj.OutputDataTable;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class RYTakebackTB extends ABusLogic {
	private MsgHead mMsgHead = null;
	public OutputDataTable cOutputDataTable;
	LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
	LCContGetPolTable tLCContGetPolTable = new LCContGetPolTable();
	VData vData = new VData();
	public CErrors mErrors = new CErrors();
	GlobalInput globalInput = new GlobalInput();

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {

		return true;

	}

	private boolean parseDatas(MsgCollection cMsgInfos) {
		// 报文头
		mMsgHead = cMsgInfos.getMsgHead();
		if (mMsgHead == null) {
			errLog("报文头信息缺失。");
			return false;
		}

		try {
			List tLCContGetPol = cMsgInfos.getBodyByFlag("LCContGetPolTable");
			if (tLCContGetPol == null || tLCContGetPol.size() == 0) {
				errLog("保单信息缺失。");
				return false;
			}

			// for (int i = 0; i < tLCContGetPol.size(); i++) {
			tLCContGetPolTable = (LCContGetPolTable) tLCContGetPol.get(0);
			String contno = tLCContGetPolTable.getContNo();
			String getpoldate = new ExeSQL()
					.getOneValue("select getpoldate from lccont where contno= '"
							+ contno + "'");
			if (!("".equals(getpoldate)||getpoldate==null)) {
				errLog("已存在回执回销日期，禁止多次修改。");
				return false;
			}
			if (tLCContGetPolTable.getGetPolDate() == ""
					|| tLCContGetPolTable.getGetPolTime() == ""
					|| tLCContGetPolTable.getCustomGetPolDate() == "") {
				errLog("签收日期为空，无法保存！");
				return false;
			}
			FDate nowdate = new FDate();

			// if
			// (nowdate.getDate(tLCContGetPolTable.getGetPolDate()).compareTo(
			// nowdate.getDate(PubFun.getCurrentDate())) > 0) {
			// errLog("客户签收日期不得晚于系统的回销日期！");
			// return false;
			// }
			// 需要确认合同打印时间是否取LCContReceive的printdate
			String printdate = new ExeSQL()
					.getOneValue("select printdate from LCContReceive where dealstate='0' and contno= '"
							+ contno + "'");
			if ("".equals(printdate)) {
				errLog("回执回销失败，原因是：该合同还未打印、或已经操作过回执回销。");
				return false;
			}
			if (nowdate.getDate(tLCContGetPolTable.getCustomGetPolDate()).compareTo(
					nowdate.getDate(printdate)) < 0) {
				errLog("客户签收日期不能早于合同打印时间！");
				return false;
			}

			//获取保单信息 modify by zxs
			String agentCode = null;
			String managecom = null;
			String appntName = null;
			String agentcode = null;
			String agentName  ;
			SSRS tSSRS = new ExeSQL().execSQL("select AgentCode,ManageCom,appntname,agentcode from lccont where contno ='"+tLCContGetPolTable.getContNo()+"'");
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("获取保单信息失败");
				return false;
			} else {
				 agentCode = tSSRS.GetText(1, 1);
				 managecom = tSSRS.GetText(1, 2);
				 appntName = tSSRS.GetText(1, 3);
				 agentcode = tSSRS.GetText(1, 4);
			}
			//进行回执回销
			 agentName = new ExeSQL().getOneValue("select name from laagent where agentcode='"+agentcode+"'");
			  LCContGetPolSchema tLCContGetPolSchema=new LCContGetPolSchema();
			  LCContGetPolUI tLCContGetPolUI =new LCContGetPolUI();
			  tLCContGetPolSchema.setContNo(tLCContGetPolTable.getContNo());
			  tLCContGetPolSchema.setGetpolDate(tLCContGetPolTable.getCustomGetPolDate());
			  tLCContGetPolSchema.setGetpolMan(appntName); //投保人
			  tLCContGetPolSchema.setSendPolMan(agentName);//agengtname
			  tLCContGetPolSchema.setGetPolOperator("EC_WX");
			  tLCContGetPolSchema.setManageCom(managecom);
			  tLCContGetPolSchema.setAgentCode(agentCode);
			  tLCContGetPolSchema.setContType("1");

				VData vData = new VData();
				globalInput.ComCode=managecom;
				globalInput.ManageCom = managecom;
				globalInput.Operator="EC_WX";
				vData.addElement(tLCContGetPolSchema);
				vData.add(globalInput);

				try {
					if( !tLCContGetPolUI.submitData(vData, "INSERT") )
					{
				   		if ( tLCContGetPolUI.mErrors.needDealError() )
				   		{
				   			errLog(tLCContGetPolUI.mErrors.getFirstError());
					  	}
					  	else
					  	{
					  		errLog("保存失败，但是没有详细的原因");
						}
					}

				}
				catch (Exception ex)
				{
					ex.printStackTrace();
					errLog(ex.getMessage());
				}
				
//			new ExeSQL()
//					.execUpdateSQL("update lccont set getpoldate='"
//							+ tLCContGetPolTable.getGetPolDate() + "' ,"
//							+ "getpoltime='"
//							+ tLCContGetPolTable.getGetPolTime()
//							+ "' , customgetpoldate='"
//							+ tLCContGetPolTable.getCustomGetPolDate()
//							+ "' , modifydate='" + PubFun.getCurrentDate()
//							+ "'  , modifytime='" + PubFun.getCurrentTime()
//							+ "' where contno='"
//							+ tLCContGetPolTable.getContNo() + "'");
			if (!PrepareInfo()) {
				errLog("查询数据失败！");
				return false;
			}

			getXmlResult();
		} catch (Exception ex) {
			return false;
			// errLog(ex.getMessage());
		}

		return true;

	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,stateflag,uwflag from lccont where contno = '"
				+ tLCContGetPolTable.getContNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			errLog("获取保单信息失败");
			return false;
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			cOutputDataTable.setStateFlag("回销成功");

		}
		return true;

	}

	protected boolean deal(MsgCollection cMsgInfos) {// 解析报文，获得报文数据
		if (!parseDatas(cMsgInfos)) {
			return false;
		}

		return true;
	}
}
