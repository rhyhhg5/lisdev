package com.cbsws.xml.ctrl.complexpds;


import java.util.List;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.RYGetTaxcodeTable;
import com.sinosoft.lis.cbcheck.GetContTaxCodeBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class RYGetTaxcode extends ABusLogic {
	
	public String ContNo="";//合同号
	public String SendDate="";//报文发送日期
	public String SendTime="";//报文发送时间
	public String BatchNo="";
	public RYGetTaxcodeTable cRYGetTaxcodeTable;
	
	public LCContTable cLCContTable;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始处理税优识别码业务");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		SendDate = tMsgHead.getSendDate();
		SendTime = tMsgHead.getSendTime();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");	       
			cLCContTable = (LCContTable) tLCContList.get(0); //获取保单信息，只有一个保单
	        
	        ContNo=cLCContTable.getContNo();
	        if("".equals(ContNo)||ContNo==null){
	        	errLog("请确定传入合同号，且格式为<ContNo></ContNo>");
	        	return false;
	        }
	        String TaxCodeSQL = "select lcc.contno,lcc.prtno,lcc.appntname,lcs.taxcode,case when lcs.succflag='00' then '成功' "
	        		+ "when lcs.succflag is null then '' else '失败' end,lcs.errorinfo from lccont lcc,lccontsub lcs "
	        		+ "where 1=1 and lcc.prtno=lcs.prtno and lcc.appflag='1' and lcc.conttype='1' and "
	        		+ "exists (select 1 from lcpol lcp,lmriskapp lm where lcp.contno=lcc.contno and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' )"
	        		+ "and lcc.contno = '"+ContNo+"'";
	        SSRS tSSRS = new ExeSQL().execSQL(TaxCodeSQL);
	        String tPrtNo = null;
	        if(tSSRS!=null&&tSSRS.getMaxRow()>0){
	        	tPrtNo = tSSRS.GetText(1, 2);
	        	if(tSSRS.GetText(1, 4)!=""&&tSSRS.GetText(1, 4)!=null){
	        		PrepareInfo(tSSRS.GetText(1, 4));
	        		getXmlResult();
	        		return true;
	        	}
	        }else{
	        	errLog("传入的保单号在核心无此保单信息！");
	        	return false;
	        }
	        TransferData mTransferData = new TransferData();
	    	mTransferData.setNameAndValue("ContNo", ContNo);
	    	mTransferData.setNameAndValue("PrtNo", tPrtNo);
	    	VData tVData = new VData();
	    	tVData.add(mTransferData);
	    	tVData.add(mGlobalInput);
	    	GetContTaxCodeBL tGetContTaxCodeBL = new GetContTaxCodeBL();//调用处理类
	    	if (!tGetContTaxCodeBL.submitData(tVData, "GET")) {
	    		errLog(tGetContTaxCodeBL.mErrors.getError(0).errorMessage);
	    		return false;
	    	}
	    	String taxcode = new ExeSQL().getOneValue("select taxcode from lccontsub where prtno ='"+tPrtNo+"'");
		    PrepareInfo(taxcode);
	    	getXmlResult();
			  
		}catch (Exception e) {
			
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单
		putResult("RYGetTaxcodeTable",cRYGetTaxcodeTable);	
		
	}
	
	// 获取保单信息
	public boolean PrepareInfo(String Taxcode) {

		 cRYGetTaxcodeTable = new RYGetTaxcodeTable();
		 cRYGetTaxcodeTable.setContNo(ContNo);
		 cRYGetTaxcodeTable.setTaxcode(Taxcode);
		return true;

	}
    public static void main(String[] args) {
    	
	}
    
}
