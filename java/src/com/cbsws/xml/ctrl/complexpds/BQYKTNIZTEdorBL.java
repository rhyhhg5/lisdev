package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;

import com.cbsws.obj.BQGrpEdorInfo;
import com.cbsws.obj.EdorInsuredInfo;
import com.cbsws.obj.ResultInsuredInfo;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BqConfirmUI;
import com.sinosoft.lis.bq.EdorItemSpecialData;
import com.sinosoft.lis.bq.FeeNoticeGrpVtsUI;
import com.sinosoft.lis.bq.FinanceDataBL;
import com.sinosoft.lis.bq.GEdorConfirmBL;
import com.sinosoft.lis.bq.GEdorZTDetailBL;
import com.sinosoft.lis.bq.GrpEdorItemUI;
import com.sinosoft.lis.bq.GrpEdorNIDetailUI;
import com.sinosoft.lis.bq.GrpEdorZTDetailUI;
import com.sinosoft.lis.bq.PEdorAppAccConfirmBL;
import com.sinosoft.lis.bq.PEdorConfirmBL;
import com.sinosoft.lis.bq.PGrpEdorAppConfirmUI;
import com.sinosoft.lis.bq.PGrpEdorCancelUI;
import com.sinosoft.lis.bq.PGrpEdorConfirmBL;
import com.sinosoft.lis.bq.SetPayInfo;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
  * create by lihuaiyv in 2018-07
 **/
public class BQYKTNIZTEdorBL {
	
	private MMap mMMap_AddItem = new MMap();
	
	public CErrors mErrors = new CErrors();
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mBatchNo = "";    //批次号
	public String mBranchCode = ""; //发报单位
	public String mEdorType = "";   //保全类型
	
	public String mResultMessage = "";   //返回信息
	
	public BQGrpEdorInfo mBQGrpEdorInfo;
	
	private TransferData mTransferData;
	
	private String mGrpContNo = "";
	private String mEdorAcceptNo = "";
	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();
	
	private List<EdorInsuredInfo> mLCInsuredList = new ArrayList<EdorInsuredInfo>();
	private List<ResultInsuredInfo> mResultInsuredInfoList = new ArrayList<ResultInsuredInfo>();
	
	/** 
	  * 全局变量 顶替数据库的作用 
	  * update = 重新 set 对象属性
	 **/
	private LCGrpContSchema mLCGrpContSchema;
	
	private LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
	
	private LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();
	
	public BQYKTNIZTEdorBL() {}
	
	public boolean submit(VData cInputData) {
		System.out.println("<----->BQYKTNIZTEdorBL.dealNIZT() start<----->");
		try {
			if (!getInputData(cInputData)) {
				System.out.println("提示：一卡通获取数据出错！！！");
				mResultMessage = "一卡通获取数据出错！！！" + mResultMessage;
				return false;
			}

			if (!createWork()) {
				System.out.println("提示：一卡通生成工单出错！！！");
				mResultMessage = "一卡通生成工单出错！！！" + mResultMessage;
				return false;
			}

			if (!addItem()) {
				if (!cancelEdorItem("2", "G&EDORMAIN")) {
					return false;
				}
				System.out.println("提示：一卡通添加保全出错！！！");
				mResultMessage = "一卡通添加保全出错！！！" + mResultMessage;
				return false;
			}
			
			if (!deatailEdor()) {
				if (!cancelEdorItem("1", "G&EDORITEM")) {
					return false;
				} else {
					if (!cancelEdorItem("2", "G&EDORMAIN")) {
						return false;
					}
				}
				System.out.println("提示：一卡通明细录入出错！！！");
				mResultMessage = "一卡通明细录入出错！！！" + mResultMessage;
				return false;
			}

			if (!calEdor()) {
				if (!cancelEdorItem("1", "G&EDORITEM")) {
					return false;
				} else {
					if (!cancelEdorItem("2", "G&EDORMAIN")) {
						return false;
					}
				}
				System.out.println("提示：一卡通保全理算出错！！！");
				mResultMessage = "一卡通保全理算出错！！！" + mResultMessage;
				return false;
			}

			if (!edorFinish()) {
				if (!cancelEdorItem("1", "G&EDORITEM")) {
					return false;
				} else {
					if (!cancelEdorItem("2", "G&EDORMAIN")) {
						return false;
					}
				}
				System.out.println("提示：一卡通保全确认出错！！！");
				mResultMessage = "一卡通保全确认出错！！！" + mResultMessage;
				return false;
			}
		}catch(Exception ex) {
			System.out.println("一卡通接口程序处理错误：撤销工单。。。"+ex.toString());
			mResultMessage = "一卡通接口程序处理错误：撤销工单。。。"+ex.toString() + mResultMessage;
			mErrors.addOneError("保全处理异常，请求失败。");
			cancelEdorItem("1", "G&EDORITEM");
			cancelEdorItem("2", "G&EDORMAIN");
			return false;
		}
		/*查询新增的分单信息*/
		for(int i = 0;i < mLCInsuredList.size();i++) {
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = new SSRS();
			String sql = "";
			if("NI".equals(mEdorType)) {
				sql = "select contno,insuredno,name,idtype,idno from lcinsured "
						+ " where grpcontno = '" + mGrpContNo + "' "
						+ " and idtype = '" + mLCInsuredList.get(i).getIDType() + "' "
						+ " and idno = '" + mLCInsuredList.get(i).getIDNo() + "' "
						+ " and name = '" + mLCInsuredList.get(i).getName() + "' "
						+ " and sex = '" + mLCInsuredList.get(i).getSex() + "' "
						+ " and birthday = '" + mLCInsuredList.get(i).getBirthday() + "' "
						+ " with ur";
			}else if ("ZT".equals(mEdorType)) {
				sql = "select contno,insuredno,name,idtype,idno from lbinsured "
						+ " where grpcontno = '" + mGrpContNo + "' "
						+ " and edorno = '" + mEdorAcceptNo + "' "
						+ " and idtype = '" + mLCInsuredList.get(i).getIDType() + "' "
						+ " and idno = '" + mLCInsuredList.get(i).getIDNo() + "' "
						+ " and name = '" + mLCInsuredList.get(i).getName() + "' "
						+ " and sex = '" + mLCInsuredList.get(i).getSex() + "' "
						+ " and birthday = '" + mLCInsuredList.get(i).getBirthday() + "' "
						+ " with ur";
			}
			tSSRS = tExeSQL.execSQL(sql);
			if(tSSRS.MaxRow != 1) {
				System.out.println("BQYKTNIZTEdorBL.java  保全结案，但查询返回分单处理信息失败！！！");
				mResultMessage = mResultMessage + "保全结案，但查询返回分单处理信息失败！！！";
				mErrors.addOneError("保全结案，但查询返回分单处理信息失败！！！");
			}
			
			ResultInsuredInfo tResultInsuredInfo = new ResultInsuredInfo();
			tResultInsuredInfo.setContNo(tSSRS.GetText(1, 1));
			tResultInsuredInfo.setInsuredNo(tSSRS.GetText(1, 2));
			tResultInsuredInfo.setName(tSSRS.GetText(1, 3));
			tResultInsuredInfo.setIDType(tSSRS.GetText(1, 4));
			tResultInsuredInfo.setIDNo(tSSRS.GetText(1, 5));
			mResultInsuredInfoList.add(tResultInsuredInfo);
			
		}
		System.out.println("<----->BQYKTNIZTEdorBL.dealNIZT()  end <----->");
		return true;
	}
	
	private boolean getInputData(VData cInputData) {
		mEdorAcceptNo = CommonBL.createWorkNo();
		if (null == mEdorAcceptNo || "".equals(mEdorAcceptNo)) {
			mResultMessage = "生成工单号出错";
			mErrors.addOneError("生成工单号出错");
			return false;
		}
		// 请求批次号
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mTransferData == null) {
			mResultMessage = "请传入请求信息";
			mErrors.addOneError("请传入请求信息");
			return false;
		}
		mBatchNo = (String) mTransferData.getValueByName("BatchNo");
		if (mBatchNo == null) {
			mResultMessage = "请传入请求批次号";
			mErrors.addOneError("请传入请求批次号");
			return false;
		}
		mBranchCode = (String)mTransferData.getValueByName("BranchCode");
		if (mBranchCode == null) {
			mResultMessage = "请传入发报单位代码";
			mErrors.addOneError("请传入发报单位代码");
			return false;
		}
		mEdorType = (String)mTransferData.getValueByName("EdorType");
		if (mEdorType == null) {
			mResultMessage = "请传入保全类型";
			mErrors.addOneError("请传入保全类型");
			return false;
		}

		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

		mBQGrpEdorInfo = (BQGrpEdorInfo) cInputData.getObjectByObjectName("BQGrpEdorInfo", 0);
		mGrpContNo = mBQGrpEdorInfo.getGrpContNo();
		
		mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0);
		
		mLCInsuredList = (List) cInputData.getObjectByObjectName("ArrayList", 0);
		for(int i = 0;i < mLCInsuredList.size();i++) {
			LCInsuredListSchema mLCInsuredListSchema = new LCInsuredListSchema();
			
			mLCInsuredListSchema.setBatchNo(mBatchNo);
			mLCInsuredListSchema.setContNo(mLCInsuredList.get(i).getContID());
			mLCInsuredListSchema.setGrpContNo(mBQGrpEdorInfo.getGrpContNo());
			mLCInsuredListSchema.setEdorNo(mEdorAcceptNo);
			mLCInsuredListSchema.setInsuredID(mLCInsuredList.get(i).getInsuredID());
			mLCInsuredListSchema.setState("0");
			mLCInsuredListSchema.setRetire(mLCInsuredList.get(i).getInsuredState());
			mLCInsuredListSchema.setEmployeeName(mLCInsuredList.get(i).getEmployeeName());
			mLCInsuredListSchema.setInsuredName(mLCInsuredList.get(i).getName());
			mLCInsuredListSchema.setRelation(mLCInsuredList.get(i).getRelationToMainInsured());
			mLCInsuredListSchema.setSex(mLCInsuredList.get(i).getSex());
			mLCInsuredListSchema.setBirthday(mLCInsuredList.get(i).getBirthday());
			mLCInsuredListSchema.setIDType(mLCInsuredList.get(i).getIDType());
			mLCInsuredListSchema.setIDNo(mLCInsuredList.get(i).getIDNo());
			mLCInsuredListSchema.setContPlanCode(mLCInsuredList.get(i).getContPlanCode());
			mLCInsuredListSchema.setOccupationType(mLCInsuredList.get(i).getOccupationType());
			mLCInsuredListSchema.setEdorValiDate(mBQGrpEdorInfo.getEdorValidate());
			mLCInsuredListSchema.setOperator(mGlobalInput.Operator);
			mLCInsuredListSchema.setMakeDate(mCurrDate);
			mLCInsuredListSchema.setMakeTime(mCurrTime);
			mLCInsuredListSchema.setModifyDate(mCurrDate);
			mLCInsuredListSchema.setModifyTime(mCurrTime);
			
			mLCInsuredListSchema.setBankCode(mLCInsuredList.get(i).getBankCode());
			mLCInsuredListSchema.setAccName(mLCInsuredList.get(i).getAccName());
			mLCInsuredListSchema.setBankAccNo(mLCInsuredList.get(i).getBankAccno());
			mLCInsuredListSchema.setPrem(mLCInsuredList.get(i).getPrem());
			mLCInsuredListSchema.setPhone(mLCInsuredList.get(i).getPhone());
			
			mLCInsuredListSet.add(mLCInsuredListSchema);
		}

		if (mGlobalInput == null) {
			mResultMessage = "传入参数信息错误";
			mErrors.addOneError("传入参数信息错误");
			return false;
		}
		
		return true;
	}
	
	/*生成工单数据*/
	private boolean createWork() {
		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setWorkNo(mEdorAcceptNo);
		tLGWorkSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
		tLGWorkSchema.setTypeNo("03");
		tLGWorkSchema.setContNo(mLCGrpContSchema.getGrpContNo());
		tLGWorkSchema.setApplyTypeNo("1");
		tLGWorkSchema.setAcceptWayNo(mBQGrpEdorInfo.getAcceptWayNo());
		tLGWorkSchema.setAcceptDate(mBQGrpEdorInfo.getAcceptDate());
		tLGWorkSchema.setRemark("一卡通工单");

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			mResultMessage = "一卡通生成保全工单失败" + tTaskInputBL.mErrors.getFirstError();
			mErrors.addOneError("一卡通生成保全工单失败" + tTaskInputBL.mErrors.getFirstError());
			return false;
		}
		return true;
	}
	
	/*添加保全项目*/
	private boolean addItem() {
		
		if(!checkEdorItem()) {
			System.out.println("一卡通添加保全项目校验时出错！！！");
			mResultMessage = mResultMessage + "一卡通添加保全项目校验时出错！！！";
			mErrors.addOneError("一卡通添加保全项目校验时出错！！！");
			return false;
		}
		
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo); 
        tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
        tLPGrpEdorMainSchema.setGrpContNo(mBQGrpEdorInfo.getGrpContNo());
        tLPGrpEdorMainSchema.setEdorAppDate(mBQGrpEdorInfo.getAcceptDate()); 
        tLPGrpEdorMainSchema.setEdorValiDate(mBQGrpEdorInfo.getEdorValidate()); 
     
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
        tLPGrpEdorItemSchema.setEdorAppNo(mEdorAcceptNo); 
        tLPGrpEdorItemSchema.setEdorType(mEdorType); 
        tLPGrpEdorItemSchema.setGrpContNo(mGrpContNo); 
        tLPGrpEdorItemSchema.setEdorAppDate(mBQGrpEdorInfo.getAcceptDate()); 
        tLPGrpEdorItemSchema.setEdorValiDate(mBQGrpEdorInfo.getEdorValidate()); 
        tLPGrpEdorItemSchema.setManageCom(mBranchCode);
        mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);
        
        System.out.println(mLPGrpEdorItemSet.size());
        
        // 准备传输数据 VData
        TransferData tTransferData = new TransferData();
        VData tVData = new VData();
        
        tVData.add(mLPGrpEdorItemSet);
        tVData.add(tLPGrpEdorMainSchema);
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);
        
        GrpEdorItemUI tGrpEdorItemUI = new GrpEdorItemUI();
        if(tGrpEdorItemUI.submitData(tVData, "INSERT||GRPEDORITEM")) {
        	tVData.clear();
        	tVData = tGrpEdorItemUI.getResult();
        	mMMap_AddItem.put("VData_AddItem", tVData);
        }else {
        	String sqlError = "select errorinfo from lcgrpimportlog where grpcontno = '" + mGrpContNo + "' and batchno = '" + mBatchNo + "' and errorstate = '1' and makedate = '" + mCurrDate + "' and maketime >= '" + mCurrTime + "' with ur";
        	ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = new SSRS();
			tSSRS = tExeSQL.execSQL(sqlError);
			String errorInfo = "一卡通增减人添加保全项目出错！！！";
			if(null != tSSRS && tSSRS.getMaxRow() >= 1) {
				errorInfo = "importlog : " + tSSRS.GetText(tSSRS.getMaxRow(), 1);
        	}
        	mResultMessage = mResultMessage + errorInfo + tGrpEdorItemUI.mErrors.getFirstError();
        	mErrors.copyAllErrors(tGrpEdorItemUI.mErrors);
        	return false;
        }
	    return true;
	}
	
	/*保全明细录入*/
	private boolean deatailEdor() {
		MMap tMMap = new MMap();
		if("NI".equals(mEdorType)) {
			
			if(mLCInsuredListSet.size()<1) {
				mResultMessage = mResultMessage + "保全明细录入，被保人信息提交数据库失败！！！";
				mErrors.addOneError("保全明细录入，被保人信息提交数据库失败！！！");
				return false;
			}
			
			tMMap.put(mLCInsuredListSet, "DELETE&INSERT");
			
			if(!pubSubmitData(tMMap)) {
				mResultMessage = mResultMessage + "保全明细录入，提交数据库失败！！！";
				mErrors.addOneError("保全明细录入，提交数据库失败！！！");
				return false;
			}
			
			LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
			tLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
			tLPGrpEdorItemSchema.setEdorType(mEdorType);
			tLPGrpEdorItemSchema.setGrpContNo(mGrpContNo);
			
			EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tLPGrpEdorItemSchema);
			tEdorItemSpecialData.add("CalType", "2");     //剩余时间比例
			tEdorItemSpecialData.add("RevertType", "2");  //维持非一年期保费
			tEdorItemSpecialData.add("CalTime", "2");     //按月计算
			tEdorItemSpecialData.add("CreateAccFlag", "1");  
			tEdorItemSpecialData.add("InputType", "2"); 
			tEdorItemSpecialData.add("Source", "2"); 
			
			VData data = new VData();
			data.add(mGlobalInput);
			data.add(tLPGrpEdorItemSchema);
			data.add(tEdorItemSpecialData);

			GrpEdorNIDetailUI tGrpEdorNIDetailUI = new GrpEdorNIDetailUI();
			if (!tGrpEdorNIDetailUI.submitData(data)) {
				String sqlError = "select errorinfo from lcgrpimportlog where grpcontno = '" + mGrpContNo + "' and batchno = '" + mBatchNo + "' and errorstate = '1' and makedate = '" + mCurrDate + "' and maketime >= '" + mCurrTime + "' with ur";
	        	ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = new SSRS();
				tSSRS = tExeSQL.execSQL(sqlError);
				String errorInfo = "一卡通增人明细录入出错！！！";
				if(null != tSSRS && tSSRS.getMaxRow() >= 1) {
					errorInfo = "importlog : " + tSSRS.GetText(tSSRS.getMaxRow(), 1);
	        	}
	        	mResultMessage = mResultMessage + errorInfo + tGrpEdorNIDetailUI.getError() + tGrpEdorNIDetailUI.getMessage();
				mErrors.addOneError(tGrpEdorNIDetailUI.getError());
				return false;
			}
		}else if("ZT".equals(mEdorType)){
			
			//个人保全
			
			LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	        tLPGrpEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
	        tLPGrpEdorItemSchema.setGrpContNo(mGrpContNo);
	        tLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
	        tLPGrpEdorItemSchema.setEdorType(mEdorType);

	        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
	        LPInsuredSet tLPInsuredSet = new LPInsuredSet();
	        LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
	        
	        for(int i = 0;i < mLCInsuredList.size();i++) {
	        	ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = new SSRS();
				
				String sql = "select contno,insuredno from lcinsured "
						+ " where grpcontno = '" + mGrpContNo + "' "
						+ " and idtype = '" + mLCInsuredList.get(i).getIDType() + "' "
						+ " and idno = '" + mLCInsuredList.get(i).getIDNo() + "' "
						+ " and name = '" + mLCInsuredList.get(i).getName() + "' "
						+ " and sex = '" + mLCInsuredList.get(i).getSex() + "' "
						+ " and birthday = '" + mLCInsuredList.get(i).getBirthday() + "' "
						+ " with ur";
				
				tSSRS = tExeSQL.execSQL(sql);
				
				if(tSSRS.MaxRow != 1) {
					System.out.println("BQYKTNIZTEdorBL.java  根据报文信息查询被保人信息出错！！！");
					mResultMessage = mResultMessage + "一卡通根据报文信息查询被保人信息出错！！！该团单下没有此被保人，无法减人！！！或请检查被保人信息是否正确！！！";
					mErrors.addOneError("一卡通根据报文信息查询被保人信息出错！！！");
					return false;
				}
				
				LCInsuredDB tLCInsuredDB = new LCInsuredDB();
				String tContNo = tSSRS.GetText(1, 1);
				String tInsuredNo = tSSRS.GetText(1, 2);
				
				LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
				
				tLPDiskImportSchema.setEdorNo(mEdorAcceptNo);
				tLPDiskImportSchema.setEdorType(mEdorType);
				tLPDiskImportSchema.setGrpContNo(mGrpContNo);
				tLPDiskImportSchema.setSerialNo(mLCInsuredList.get(i).getContID());
				tLPDiskImportSchema.setState("1");
				tLPDiskImportSchema.setInsuredNo(tInsuredNo);
				tLPDiskImportSchema.setInsuredName(mLCInsuredList.get(i).getName());
				tLPDiskImportSchema.setContPlanCode(mLCInsuredList.get(i).getContPlanCode());
				tLPDiskImportSchema.setRetire(mLCInsuredList.get(i).getInsuredState());
				tLPDiskImportSchema.setEmployeeName(mLCInsuredList.get(i).getEmployeeName());
				tLPDiskImportSchema.setRelation(mLCInsuredList.get(i).getRelationToMainInsured());
				tLPDiskImportSchema.setSex(mLCInsuredList.get(i).getSex());
				tLPDiskImportSchema.setBirthday(mLCInsuredList.get(i).getBirthday());
				tLPDiskImportSchema.setIDType(mLCInsuredList.get(i).getIDType());
				tLPDiskImportSchema.setIDNo(mLCInsuredList.get(i).getIDNo());
				tLPDiskImportSchema.setOccupationType(mLCInsuredList.get(i).getOccupationType());
				tLPDiskImportSchema.setImportFileName(mBatchNo);
				tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
				tLPDiskImportSchema.setMakeDate(mCurrDate);
				tLPDiskImportSchema.setMakeTime(mCurrTime);
				tLPDiskImportSchema.setModifyDate(mCurrDate);
				tLPDiskImportSchema.setModifyTime(mCurrTime);
				
				LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
				
				tLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
                tLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
                tLPEdorItemSchema.setContNo(tContNo);
                tLPEdorItemSchema.setInsuredNo(tInsuredNo);
                tLPEdorItemSchema.setEdorValiDate(mBQGrpEdorInfo.getEdorValidate());
				
                LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                
                tLPInsuredSchema.setContNo(tContNo);
                tLPInsuredSchema.setInsuredNo(tInsuredNo);
                
                tLPDiskImportSet.add(tLPDiskImportSchema);
                tLPEdorItemSet.add(tLPEdorItemSchema);
                tLPInsuredSet.add(tLPInsuredSchema);
                
	        }
	        
	        VData tVData = new VData();
	        tVData.add(mGlobalInput);
	        tVData.add(tLPGrpEdorItemSchema);
	        tVData.add(tLPEdorItemSet);
	        tVData.add(tLPInsuredSet);
	        tVData.add(tLPDiskImportSet);

	        GEdorZTDetailBL tGEdorZTDetailBL = new GEdorZTDetailBL();
	        if(!tGEdorZTDetailBL.submitData(tVData, "INSERT||EDOR")) {
	        	System.out.println("一卡通调用 GEdorZTDetailBL 出错！！！");
	        	String sqlError = "select errorinfo from lcgrpimportlog where grpcontno = '" + mGrpContNo + "' and batchno = '" + mBatchNo + "' and errorstate = '1' and makedate = '" + mCurrDate + "' and maketime >= '" + mCurrTime + "' with ur";
	        	ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = new SSRS();
				tSSRS = tExeSQL.execSQL(sqlError);
				String errorInfo = "一卡通减人明细录入出错！";
				if(null != tSSRS && tSSRS.getMaxRow() >= 1) {
					errorInfo = "importlog : " + tSSRS.GetText(tSSRS.getMaxRow(), 1);
	        	}
	        	mResultMessage = mResultMessage + errorInfo + tGEdorZTDetailBL.mErrors.getFirstError() + tGEdorZTDetailBL.mErrors.getErrContent();
	            mErrors.copyAllErrors(tGEdorZTDetailBL.mErrors);
	            return false;
	        }
			
			//明细录入状态更改
	        
	        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tLPGrpEdorItemSchema);
	        
	        tEdorItemSpecialData.add("CalType", "2");     //剩余时间比例
			tEdorItemSpecialData.add("RevertType", "2");  //维持非一年期保费
			tEdorItemSpecialData.add("CalTime", "2");     //按日计算
	        tEdorItemSpecialData.add("FeeRate", "0.25");
	        tEdorItemSpecialData.add(BQ.DETAILTYPE_BALAPAYTYPE, "0");   //账户余额退还方式，退还给集体账户
	        tEdorItemSpecialData.add("CreateAccFlag", "1");  
			tEdorItemSpecialData.add("InputType", "2"); 
			tEdorItemSpecialData.add("Source", "2"); 
			
	        VData data = new VData();
			data.add(mGlobalInput);
			data.add(tLPGrpEdorItemSchema);
			data.add(tEdorItemSpecialData);
			
			GrpEdorZTDetailUI tGrpEdorZTDetailUI = new GrpEdorZTDetailUI();
			if (!tGrpEdorZTDetailUI.submitData(data)) {
				System.out.println("一卡通调用 GrpEdorZTDetailUI 出错！！！");
				String sqlError = "select errorinfo from lcgrpimportlog where grpcontno = '" + mGrpContNo + "' and batchno = '" + mBatchNo + "' and errorstate = '1' and makedate = '" + mCurrDate + "' and maketime >= '" + mCurrTime + "' with ur";
	        	ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = new SSRS();
				tSSRS = tExeSQL.execSQL(sqlError);
				String errorInfo = "一卡通减人明细录入出错！！！";
				if(null != tSSRS && tSSRS.getMaxRow() >= 1) {
					errorInfo = "importlog : " + tSSRS.GetText(tSSRS.getMaxRow(), 1);
	        	}
	        	mResultMessage = mResultMessage + "一卡通调用 GrpEdorZTDetailUI 出错！！！" + errorInfo + tGrpEdorZTDetailUI.getError();
	            mErrors.addOneError(tGrpEdorZTDetailUI.getError());
	            return false;
			}
		}else {
			System.out.println("传入的保全类型有误！！！");
			mResultMessage = mResultMessage + "传入的保全类型有误！！！";
			return false;
		}
		
		return true;
	}
	
	/*保全理算*/
	private boolean calEdor() {
		LPGrpEdorMainSchema tLPGrpEdorMainSchema  = new LPGrpEdorMainSchema();
	    tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
	    tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
	    tLPGrpEdorMainSchema.setGrpContNo(mGrpContNo);
	    tLPGrpEdorMainSchema.setEdorValiDate(mBQGrpEdorInfo.getEdorValidate());
	    tLPGrpEdorMainSchema.setEdorAppDate(mBQGrpEdorInfo.getAcceptDate());
	             
	    VData data = new VData();  
	    data.add(mGlobalInput);   
	    data.add(tLPGrpEdorMainSchema);
	    PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
	    if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM")){
	    	String sqlError = "select errorinfo from lcgrpimportlog where grpcontno = '" + mGrpContNo + "' and batchno = '" + mBatchNo + "' and errorstate = '1' and makedate = '" + mCurrDate + "' and maketime >= '" + mCurrTime + "' with ur";
        	ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = new SSRS();
			tSSRS = tExeSQL.execSQL(sqlError);
			String errorInfo = "一卡通增减人理算出错！！！或请检查报文信息是否正确！！！";
			if(null != tSSRS && tSSRS.getMaxRow() >= 1) {
				errorInfo = "importlog : " + tSSRS.GetText(tSSRS.getMaxRow(), 1) + "\n";
        	}
	    	mResultMessage = mResultMessage + errorInfo + tPGrpEdorAppConfirmUI.getError();
	    	mErrors.copyAllErrors(tPGrpEdorAppConfirmUI.getError());
	    	return false;
	    }
	    return true;
	}
	
	/*保全确认*/
	private boolean edorFinish() {
		String flag = "";
		String edorAcceptNo = mEdorAcceptNo;
		String fmtransact = "0";
		String payMode = "1";
		String balanceMethodValue = "1";
		String customerNo = mLCGrpContSchema.getAppntNo();
		String accType = "1";
		String destSource = "11";
		String contType = null;
		String fmtransact2 = "NOTUSEACC";
		GlobalInput gi = mGlobalInput;

		String failType = null;

		String checkNI = "select 1 from LPGrpEdorItem " + "where EdorNo = '" + edorAcceptNo + "' "
				+ "and EdorType = 'NI' ";
		SSRS mssrs = new ExeSQL().execSQL(checkNI);
		if (mssrs != null && mssrs.getMaxRow() > 0) {
			// 增加理算后对于lcinsured是否缺失的校验 add by lhy in 2018-07
			String checkInsured = " select 1 from lccont where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"
					+ edorAcceptNo + "' fetch first 1 row only) and contno not in "
					+ "(select distinct contno from lcinsured where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"
					+ edorAcceptNo
					+ "' fetch first 1 row only) ) and contno in (select contno from lcinsuredlist where edorno='"
					+ edorAcceptNo + "') ";
			SSRS mssrs1 = new ExeSQL().execSQL(checkInsured);
			if (mssrs1 != null && mssrs1.getMaxRow() > 0) {
				flag = "Fail";
				System.out.println("保全项目增人时出现lcinsured丢失的问题,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试");
				mResultMessage += "保全项目增人时出现lcinsured丢失的问题,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
			}
			// ------------------insured校验结束-------------------

			String sql1 = "select getmoney from lpgrpedoritem " + "where EdorNo = '" + edorAcceptNo + "' "
					+ "and EdorType = 'NI' ";
			SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
			String itemmoney = tSSRS1.GetText(1, 1);
			String sql2 = "select sum(getmoney) from ljsgetendorse " + " where endorsementno = '" + edorAcceptNo + "' "
					+ " and feeoperationtype='NI' and feefinatype='BF' with ur";
			SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
			String endorsemoney = tSSRS2.GetText(1, 1);

			if (!itemmoney.equals(endorsemoney)) {
				flag = "Fail";
				System.out.println("保全项目增人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试");
				mResultMessage += "保全项目增人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
			}
			// -----------------------关于增人总数和实际的理算的人数的比较-----------------------
			String sql_check_ljagetendorse = "  select count(distinct a.contno),count(distinct b.contno) "
					+ "  from ljsgetendorse a, lcinsuredlist b " + "  where a.grpcontno = b.grpcontno "
					+ "  and a.endorsementno = '" + edorAcceptNo + "'" + "  and a.feeoperationtype = 'NI' "
					+ "  and a.endorsementno=b.edorno " + "  and b.state='1' " + "  with ur";
			// ---------------------------------------------------------
			SSRS t_check_ljagendorse_SSRS1 = new ExeSQL().execSQL(sql_check_ljagetendorse);
			// ---------------------------------------------------------
			System.out.println("sql_check_ljagetendorse:" + sql_check_ljagetendorse);
			int the_number_of_1 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1, 1));
			int the_number_of_2 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1, 2));
			System.out.println("the_number_of_1:" + the_number_of_1);
			System.out.println("the_number_of_2:" + the_number_of_2);
			if (the_number_of_1 != the_number_of_2) {
				flag = "Fail";
				System.out.println("实际理算完成的人数和模板录入的人数不一致，请点击[重复理算]回退到保全明细录入页面，并重新录入[NI]项目明细");
				mResultMessage += "实际理算完成的人数和模板录入的人数不一致，请点击[重复理算]回退到保全明细录入页面，并重新录入[NI]项目明细";
			}

		}

		// lhy 2018-07-11 增加做ZT项目时会错误的校验
		String checkZT = "select 1 from LPGrpEdorItem " + "where EdorNo = '" + edorAcceptNo + "' "
				+ "and EdorType = 'ZT' ";
		SSRS mssrs1 = new ExeSQL().execSQL(checkZT);
		if (mssrs1 != null && mssrs1.getMaxRow() > 0) {
			String sql3 = "select sum(getmoney) from lpedoritem " + "where EdorNo = '" + edorAcceptNo + "' "
					+ "and EdorType = 'ZT' ";
			SSRS tSSRS3 = new ExeSQL().execSQL(sql3);
			String itemmoneyZT = tSSRS3.GetText(1, 1);
			String sql4 = "select case when sum(getmoney) is null then 0 else sum(getmoney) end from ljsgetendorse "
					+ " where endorsementno = '" + edorAcceptNo + "' " + " and feeoperationtype='ZT'  with ur";
			SSRS tSSRS4 = new ExeSQL().execSQL(sql4);
			String endorsemoneyZT = tSSRS4.GetText(1, 1);

			if (!itemmoneyZT.equals(endorsemoneyZT)) {
				flag = "Fail";
				System.out.println("保全项目减人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[ZT]项目明细重试");
				mResultMessage += "保全项目减人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[ZT]项目明细重试";
			}
		}

		if (!flag.equals("Fail")) {
			EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorAcceptNo, "YE");
			tSpecialData.add("CustomerNo", customerNo);
			tSpecialData.add("AccType", accType);
			tSpecialData.add("OtherType", "3");
			tSpecialData.add("OtherNo", edorAcceptNo);
			tSpecialData.add("DestSource", destSource);
			tSpecialData.add("ContType", contType);
			tSpecialData.add("Fmtransact2", fmtransact2);

			LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
			tLCAppAccTraceSchema.setCustomerNo(customerNo);
			tLCAppAccTraceSchema.setAccType(accType);
			tLCAppAccTraceSchema.setOtherType("3");// 团单
			tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
			tLCAppAccTraceSchema.setDestSource(destSource);
			tLCAppAccTraceSchema.setOperator(gi.Operator);

			VData tVData = new VData();
			tVData.add(tLCAppAccTraceSchema);
			tVData.add(tSpecialData);
			tVData.add(gi);
			PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
			if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param")) {
				flag = "Fail";
				System.out.println("处理帐户余额失败！");
				mResultMessage += "处理帐户余额失败！";
			} else {
				
				if(!edorConfirm()) {
//				BqConfirmUI tBqConfirmUI = new BqConfirmUI(gi, edorAcceptNo, BQ.CONTTYPE_G, balanceMethodValue);
//				if (!tBqConfirmUI.submitData()) {
					flag = "Fail";
					System.out.println("保全确认出错！！！");
					mResultMessage += "保全确认出错！！！";
//					System.out.println(tBqConfirmUI.getError());
//					mResultMessage += "保全确认出错！！！" + tBqConfirmUI.getError();
//					mErrors.addOneError(tBqConfirmUI.getError());
					
				} else {
					System.out.println("交退费通知书" + edorAcceptNo);
					TransferData tTransferData = new TransferData();
					tTransferData.setNameAndValue("payMode", payMode);
					tTransferData.setNameAndValue("endDate", mCurrDate);
					tTransferData.setNameAndValue("payDate", mCurrDate);
					tTransferData.setNameAndValue("bank", mLCGrpContSchema.getBankCode());
					tTransferData.setNameAndValue("bankAccno", mLCGrpContSchema.getBankAccNo());
					tTransferData.setNameAndValue("accName", mLCGrpContSchema.getAccName());

					// 生成交退费通知书
					FeeNoticeGrpVtsUI tFeeNoticeGrpVtsUI = new FeeNoticeGrpVtsUI(edorAcceptNo);
					if (!tFeeNoticeGrpVtsUI.submitData(tTransferData)) {
						flag = "Fail";
						System.out.println("生成批单失败！原因是：" + tFeeNoticeGrpVtsUI.getError());
						mErrors.addOneError(tFeeNoticeGrpVtsUI.getError());
						mResultMessage += "生成批单失败！原因是：" + tFeeNoticeGrpVtsUI.getError();
					}

					VData data = new VData();
					data.add(gi);
					data.add(tTransferData);
					SetPayInfo spi = new SetPayInfo(edorAcceptNo);
					if (!spi.submitDate(data, "0")) {
						System.out.println("设置转帐信息失败！");
						flag = "Fail";
						System.out.println("设置收退费方式失败！原因是：" + spi.mErrors.getFirstError());
						mErrors.addOneError(spi.mErrors.getFirstError());
						mResultMessage += "设置转帐信息失败！原因是：" + spi.mErrors.getFirstError();
					}
					flag = "Succ";
					System.out.println("保全确认成功！");
//					String message = tBqConfirmUI.getMessage();
//					if ((message != null) && (!message.equals(""))) {
//						System.out.println("message : " + message);
//					}
				}
			}
		}
		System.out.println("BQYKTNIZTEdorBL.edorFinish() end");
		return true;
	}
	
	private boolean checkEdorItem() {
		String mGrpContNo=mLCGrpContSchema.getGrpContNo();
		//正在操作保全的无法添加保全
		String edorSQL = " select edoracceptno from lpgrpedoritem where grpcontno='"
				+ mGrpContNo
				+ "' and edoracceptno!='"+ mEdorAcceptNo+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0') with ur  ";
		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			mErrors.addOneError("保单" + mGrpContNo
					+ "正在操作保全，工单号为：" + edorFlag + "无法添加保全！");
			mResultMessage += "保单" + mGrpContNo
					+ "正在操作保全，工单号为：" + edorFlag + "无法添加保全！";
			return false;
		}
		// 只有承保有效状态下的保单才能犹豫期退保
		if (!"1".equals(mLCGrpContSchema.getAppFlag())) {
			mResultMessage += "保单未签单不能申请增减人";
			mErrors.addOneError("保单未签单不能申请增减人");
			return false;
		}
		
		return true;
	}
	
	private boolean cancelEdorItem(String DelFlag, String transact) {
		// 接收信息，并作校验处理。
		// 输入参数
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
		TransferData tTransferData = new TransferData();

		// 输出参数
		String Content = "";
		GlobalInput tGI = new GlobalInput();
		tGI = mGlobalInput;
		if (tGI == null) {
			System.out.println("没有 GlobalInput 公共信息！！！");
		} else {

			String delReason = "";
			String reasonCode = "";

			if (DelFlag.equals("1")) { // 删除项目

				tLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
				tLPGrpEdorItemSchema.setGrpContNo(mGrpContNo);
				tLPGrpEdorItemSchema.setEdorType(mEdorType);
				reasonCode = "002";
				delReason = "002";

				tTransferData.setNameAndValue("DelReason", delReason);
				tTransferData.setNameAndValue("ReasonCode", reasonCode);
				try {
					// 准备传输数据 VData
					VData tVData = new VData();
					tVData.addElement(tGI);
					tVData.addElement(tLPGrpEdorItemSchema);
					tVData.addElement(tTransferData);

					// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
					tPGrpEdorCancelUI.submitData(tVData, transact);
				} catch (Exception ex) {
					mResultMessage = "保存失败，原因是:" + ex.toString();
					return false;
				}
			}
			if (DelFlag.equals("2")) { // 删除批单

				tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
				tLPGrpEdorMainSchema.setGrpContNo(mGrpContNo);
				reasonCode = "002";
				delReason = "002";

				tTransferData.setNameAndValue("DelReason", delReason);
				tTransferData.setNameAndValue("ReasonCode", reasonCode);
				try {
					// 准备传输数据 VData
					VData tVData = new VData();
					tVData.addElement(tGI);
					tVData.addElement(tLPGrpEdorMainSchema);
					tVData.addElement(tTransferData);// 撤销申请原因
					// tVData.addElement(reasonCode);
					// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
					tPGrpEdorCancelUI.submitData(tVData, transact);
				} catch (Exception ex) {
					mResultMessage = "保存失败，原因是:" + ex.toString();
					return false;
				}
			}
		}
		return false;
	}
	
	private boolean edorConfirm() {
		MMap map = new MMap();

		// 生成财务数据
		FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput,
				mEdorAcceptNo, BQ.NOTICETYPE_G, "");
		if (!tFinanceDataBL.submitData()) {
			mErrors.addOneError("生成财务数据错误" + tFinanceDataBL.mErrors);
			return false;
		}

		// 团单保全结案
		PGrpEdorConfirmBL tPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
        tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		
        VData tVData = new VData();
        String strTemplatePath = "";
        tVData.add(strTemplatePath);
        tVData.add(tLPGrpEdorMainSchema);
        tVData.add(mGlobalInput);
        
		map = tPGrpEdorConfirmBL.getSubmitData(tVData,"INSERT||GRPEDORCONFIRM");
		if (map == null) {
			mErrors.addOneError("保全结案发生错误" + tFinanceDataBL.mErrors);
			return false;
		}

		// 工单结案
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
		tLGWorkSchema.setTypeNo("03"); // 结案状态

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		MMap tmap = tTaskAutoFinishBL.getSubmitData(data, "");
		if (tmap == null) {
			mErrors.addOneError("工单结案失败" + tFinanceDataBL.mErrors);
			return false;
		}
		map.add(tmap);
		if (!pubSubmitData(map)) {
			return false;
		}
		return true;
	}
	
	private boolean pubSubmitData(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.addOneError("提交数据库发生错误" + tPubSubmit.mErrors.getFirstError());
			mResultMessage += "提交数据库发生错误" + tPubSubmit.mErrors.getFirstError();
			return false;
		}
		return true;
	}
	
	public String getEdorAcceptNo() {
		return mEdorAcceptNo;
	}

	public List<ResultInsuredInfo> getResultInsuredInfoList() {
		return mResultInsuredInfoList;
	}
	
}
