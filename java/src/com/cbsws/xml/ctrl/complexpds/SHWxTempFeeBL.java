package com.cbsws.xml.ctrl.complexpds;

import org.apache.log4j.Logger;

import com.cbsws.obj.SHECCertSalesPayInfoTable;
import com.cbsws.obj.SHLCContTable;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LICertSalesPayInfoSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class SHWxTempFeeBL {
	private final Logger cLogger = Logger.getLogger(getClass());
	
	private final GlobalInput cGlobalInput;
	private LCContSchema cLCContSchema;
	private LICertSalesPayInfoSchema cLICertSalesPayInfoSchema = new LICertSalesPayInfoSchema();
	private SHECCertSalesPayInfoTable cECCertSalesPayInfo;
	
	
	private String cTempFeeNo = null;
	private String cPrtno = "";
	public SHLCContTable cLCContTable;
	
	public SHWxTempFeeBL(SHLCContTable cLCContTable2, GlobalInput pGlobalInput,LICertSalesPayInfoSchema pLICertSalesPayInfoSchema,SHECCertSalesPayInfoTable cECCertSalesPayInfo2){
		cGlobalInput = pGlobalInput;
		cLCContTable =cLCContTable2;
		cPrtno = cLCContTable2.getPrtNo();
		cLICertSalesPayInfoSchema = pLICertSalesPayInfoSchema;
		cECCertSalesPayInfo = cECCertSalesPayInfo2;
	}
	
	public String deal() throws Exception {
		cLogger.info("Into WxTempFeeBL.deal()...");
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(cPrtno);
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			return "查询保单合同数据失败！";
		}
		cLCContSchema = mLCContSet.get(1);
		
		//插入财务暂收费数据
		
//		VData mTempFeeBLData = getTempFeeBLData(cLCContSchema);
		MMap mSubmitMMap = getTempFeeBLData(cLCContSchema);
//		TempFeeBL mTempFeeBL = new TempFeeBL();
		mSubmitMMap.put(cLICertSalesPayInfoSchema, "INSERT");
		cLogger.info("Start call TempFeeBL.submitData()...");
		long mStartMillis = System.currentTimeMillis();
//		if (!mTempFeeBL.submitData(mTempFeeBLData, "INSERT")) {
//			return mTempFeeBL.mErrors.getFirstError();
//		}
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		cLogger.info("开始提交暂收数据...");
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			return "提交暂收数据失败！";
		}
		cLogger.info("TempFeeBL耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
		cLogger.info("End call TempFeeBL.submitData()!");
		
		//更新LCCont中的TempFeeNo
//		String mSQL = "update LCCont set TempFeeNo='" + cTempFeeNo + "' where ContNo='" + cLCContSchema.getContNo() + "' with ur";
//		ExeSQL mExeSQL = new ExeSQL();
//		if (!mExeSQL.execUpdateSQL(mSQL)) {
//			cLogger.error(mExeSQL.mErrors.getFirstError());
//			return "更新暂收费号(TempFeeNo)到LCCont失败！";
//		}

		cLogger.info("Out WxTempFeeBL.deal()!");
		return "";
	}
	
	private MMap getTempFeeBLData(LCContSchema pLCContSchema) throws MidplatException {
		cLogger.info("Into WxTempFeeBL.getTempFeeBLData()...");
		
		LCPolDB mLCPolDB = new LCPolDB();
		mLCPolDB.setContNo(pLCContSchema.getContNo());
		LCPolSet mLCPolSet = mLCPolDB.query();
		if (mLCPolSet.size() < 1) {
			throw new MidplatException("查询保单险种数据失败！");
		}

		String mSQL = "select max(tempfeeno) from ljtempfee where tempfeeno like '" + pLCContSchema.getPrtNo() + "%' with ur";
		cTempFeeNo = new ExeSQL().getOneValue(mSQL);
		if (cTempFeeNo.equals("")) {
			cTempFeeNo = pLCContSchema.getPrtNo() + "801";
		} else {
			String tSuffix = cTempFeeNo.substring(cTempFeeNo.length() - 3);
			cTempFeeNo = pLCContSchema.getPrtNo() + (Integer.parseInt(tSuffix)+1); // 暂收费号+1
		}
		pLCContSchema.setTempFeeNo(cTempFeeNo);
		String mManageCom = cGlobalInput.ManageCom;
		
//		double mSumTempFeePayMoney = 0;
		LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
//		for (int i = 1; i <= mLCPolSet.size(); i++) {
//			LCPolSchema tLCPolSchema = mLCPolSet.get(i);
//			mSumTempFeePayMoney += tLCPolSchema.getPrem();
//		}
		
		String ZBankManageCom = "";
		String ZBankAccNo = "";
//		by gzh 20110221 增加网销银行编码 20110721 调整为总公司账户收取，修改获取管理机构及账户
		if( "".equals(cECCertSalesPayInfo.getTempManageCom()) || null == cECCertSalesPayInfo.getTempManageCom() || "86000000".equals(cECCertSalesPayInfo.getTempManageCom())){
			System.out.println("微信签单获取总公司账户");
			SSRS BankInfos = getBankAccCode();
			ZBankManageCom = BankInfos.GetText(1, 1);//总公司机构
			ZBankAccNo = BankInfos.GetText(1, 2);
		}else {
			System.out.println("微信签单获取录入账户："+ZBankManageCom+"；"+ZBankAccNo);
			ZBankManageCom = cECCertSalesPayInfo.getTempManageCom();//总公司机构
			ZBankAccNo = cECCertSalesPayInfo.getInsBankAccNo();
		}
		
		LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
		tLJTempFeeSchema.setTempFeeNo(cTempFeeNo);
		tLJTempFeeSchema.setOtherNo(mLCPolSet.get(1).getPrtNo()); //投保单号
		tLJTempFeeSchema.setTempFeeType("31");//31-复杂产品网销收费
		tLJTempFeeSchema.setOtherNoType("4");	//4--印刷号
		tLJTempFeeSchema.setRiskCode("000000");//默认000000
		tLJTempFeeSchema.setPayIntv(mLCPolSet.get(1).getPayIntv());
		tLJTempFeeSchema.setPayMoney(cLICertSalesPayInfoSchema.getPayMoney());//交易报文的交费总金额
		tLJTempFeeSchema.setPayDate(pLCContSchema.getPolApplyDate());
		tLJTempFeeSchema.setEnterAccDate(pLCContSchema.getPolApplyDate());
		tLJTempFeeSchema.setSaleChnl(pLCContSchema.getSaleChnl());
		tLJTempFeeSchema.setManageCom(ZBankManageCom);	//收费机构
		tLJTempFeeSchema.setPolicyCom(pLCContSchema.getManageCom());	//管理机构
		tLJTempFeeSchema.setAPPntName(mLCPolSet.get(1).getAppntName());
		tLJTempFeeSchema.setAgentCom(pLCContSchema.getAgentCom());
		tLJTempFeeSchema.setAgentGroup(pLCContSchema.getAgentGroup());
		tLJTempFeeSchema.setAgentCode(pLCContSchema.getAgentCode());
		tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
		tLJTempFeeSchema.setConfFlag("0");	//是否核销 0：没有财务核销 1：已有财务核销
		tLJTempFeeSchema.setOperator(cGlobalInput.Operator);
		tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
		tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
		tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
		tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
		mLJTempFeeSet.add(tLJTempFeeSchema);

		LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
		mLJTempFeeClassSchema.setTempFeeNo(cTempFeeNo);
		String payMode = cLCContTable.getPayMode();
		if(payMode != null && !"".equals(payMode)){
			mLJTempFeeClassSchema.setPayMode(payMode);
		}else{
			mLJTempFeeClassSchema.setPayMode("12");
		}
		mLJTempFeeClassSchema.setPayMoney(cLICertSalesPayInfoSchema.getPayMoney());//交易报文的交费总金额
		mLJTempFeeClassSchema.setAppntName(pLCContSchema.getAppntName());
		mLJTempFeeClassSchema.setPayDate(pLCContSchema.getPolApplyDate());
		mLJTempFeeClassSchema.setEnterAccDate(pLCContSchema.getPolApplyDate());
		mLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setConfFlag("0"); //是否核销 0：没有财务核销 1：已有财务核销
		mLJTempFeeClassSchema.setManageCom(ZBankManageCom);
		mLJTempFeeClassSchema.setPolicyCom(cGlobalInput.ManageCom);
		mLJTempFeeClassSchema.setInsBankAccNo(ZBankAccNo);
    	String aBankCode = getBankCode(ZBankAccNo);//获取银行账号对应的bankcode
    	mLJTempFeeClassSchema.setBankCode(aBankCode);
		mLJTempFeeClassSchema.setBankCode(pLCContSchema.getBankCode());
		mLJTempFeeClassSchema.setBankAccNo(pLCContSchema.getBankAccNo());
		mLJTempFeeClassSchema.setAccName(pLCContSchema.getAccName());
		mLJTempFeeClassSchema.setOperator(cGlobalInput.Operator);
		mLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
		mLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
		LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
		mLJTempFeeClassSet.add(mLJTempFeeClassSchema);

		//#3313 ,关于360网销接口调整 start
		String tSQL = "select codename from ldcode where codetype = 'specwrap' and code = '"+cLICertSalesPayInfoSchema.getWrapCode()+"' with ur";
		String tcodeName = new ExeSQL().getOneValue(tSQL);
		if("185".equals(tcodeName)) {
			//tempTempFeeNo，暂收费票据号
			String tempTempFeeNo = pLCContSchema.getPrtNo() + "32801";
			//tempSerialNo,流水号
			String tempSerialNo = PubFun1.CreateMaxNo("SERIALNO", cGlobalInput.ManageCom);
			
			LJTempFeeSchema tempLJTempFeeSchema = new LJTempFeeSchema();
			tempLJTempFeeSchema.setTempFeeNo(tempTempFeeNo);
			tempLJTempFeeSchema.setOtherNo(mLCPolSet.get(1).getPrtNo()); //投保单号
			tempLJTempFeeSchema.setTempFeeType("32");
			tempLJTempFeeSchema.setOtherNoType("32");	//4--印刷号
			tempLJTempFeeSchema.setRiskCode("000000");//默认000000
			tempLJTempFeeSchema.setSerialNo(tempSerialNo);
			tempLJTempFeeSchema.setPayIntv(mLCPolSet.get(1).getPayIntv());
			tempLJTempFeeSchema.setPayMoney(tcodeName);//交易报文的交费总金额
			tempLJTempFeeSchema.setPayDate(pLCContSchema.getPolApplyDate());
			tempLJTempFeeSchema.setEnterAccDate(pLCContSchema.getPolApplyDate());
			tempLJTempFeeSchema.setSaleChnl(pLCContSchema.getSaleChnl());
			tempLJTempFeeSchema.setManageCom(ZBankManageCom);	//收费机构
			tempLJTempFeeSchema.setPolicyCom(pLCContSchema.getManageCom());	//管理机构
			tempLJTempFeeSchema.setAPPntName(mLCPolSet.get(1).getAppntName());
			tempLJTempFeeSchema.setAgentCom(pLCContSchema.getAgentCom());
			tempLJTempFeeSchema.setAgentGroup(pLCContSchema.getAgentGroup());
			tempLJTempFeeSchema.setAgentCode(pLCContSchema.getAgentCode());
			tempLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
			tempLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
			tempLJTempFeeSchema.setConfFlag("1");	//是否核销 0：没有财务核销 1：已有财务核销
			tempLJTempFeeSchema.setConfDate(PubFun.getCurrentDate());
			tempLJTempFeeSchema.setOperator(cGlobalInput.Operator);
			tempLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
			tempLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
			tempLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
			tempLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
			mLJTempFeeSet.add(tempLJTempFeeSchema);
			
			LJTempFeeClassSchema tempLJTempFeeClassSchema = new LJTempFeeClassSchema();
			tempLJTempFeeClassSchema.setTempFeeNo(tempTempFeeNo);
			String payMode2 = cLCContTable.getPayMode();
			if(payMode2 != null && !"".equals(payMode2)){
				tempLJTempFeeClassSchema.setPayMode(payMode2);
			}else{
				tempLJTempFeeClassSchema.setPayMode("12");
			}
			tempLJTempFeeClassSchema.setPayMoney(tcodeName);//交易报文的交费总金额
			tempLJTempFeeClassSchema.setAppntName(pLCContSchema.getAppntName());
			tempLJTempFeeClassSchema.setPayDate(pLCContSchema.getPolApplyDate());
			tempLJTempFeeClassSchema.setEnterAccDate(pLCContSchema.getPolApplyDate());
			tempLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
			tempLJTempFeeClassSchema.setConfFlag("1"); //是否核销 0：没有财务核销 1：已有财务核销
			tempLJTempFeeClassSchema.setConfDate(PubFun.getCurrentDate());
			tempLJTempFeeClassSchema.setSerialNo(tempSerialNo);
			tempLJTempFeeClassSchema.setManageCom(ZBankManageCom);
			tempLJTempFeeClassSchema.setPolicyCom(cGlobalInput.ManageCom);
			tempLJTempFeeClassSchema.setInsBankAccNo(ZBankAccNo);
			String tempBankCode = getBankCode(ZBankAccNo);//获取银行账号对应的bankcode
			tempLJTempFeeClassSchema.setBankCode(tempBankCode);
			tempLJTempFeeClassSchema.setBankCode(pLCContSchema.getBankCode());
			tempLJTempFeeClassSchema.setBankAccNo(pLCContSchema.getBankAccNo());
			tempLJTempFeeClassSchema.setAccName(pLCContSchema.getAccName());
			tempLJTempFeeClassSchema.setOperator(cGlobalInput.Operator);
			tempLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
			tempLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
			tempLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
			tempLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
			mLJTempFeeClassSet.add(tempLJTempFeeClassSchema);
		}
		//#3313,关于360网销接口调整 end
		
//		TransferData mTransferData = new TransferData();
//		mTransferData.setNameAndValue("CertifyFlag", "6");

//		VData mVData = new VData();
//		mVData.add(mTransferData);
//		mVData.add(cGlobalInput);
//		mVData.add(mLJTempFeeSet);
//		mVData.add(mLJTempFeeClassSet);
		
		MMap aMMap = new MMap();
		aMMap.put(mLJTempFeeClassSet, "INSERT");
		aMMap.put(mLJTempFeeSet, "INSERT");

		cLogger.info("Out WxTempFeeBL.getTempFeeBLData()!");
		return aMMap;
	}
	
	/**
	 * 返回暂收费号。注意：仅当成功生成暂收数据时，此调用才有效。
	 */
	public String getTempFeeNo() {
		return cTempFeeNo;
	}
	
	/**
     * 根据网销结算时总公司的机构编码和银行账户。
     * @param szFunc
     * @param szErrMsg
     */
	
    public SSRS getBankAccCode()
    {
    	String sql = "select code,codename from ldcode where codetype = 'InsBankAccNo' and othersign = 'WX'";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0)
    	{
        	return null;
    	}
        return tSSRS;
    }
    
    /**
     * 根据机构编码获取银行账户。
     * @param szFunc
     * @param szErrMsg
     */
    public String getBankCode(String aBankAccNo)
    {
    	String sql = "select bankcode from ldfinbank where bankaccno = '"+aBankAccNo+"' ";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0)
    	{
        	return null;
    	}
        return tSSRS.GetText(1, 1);
    }
}