package com.cbsws.xml.ctrl.complexpds;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.GrpEdorItemInfo;
import com.cbsws.obj.DealInfo;
import com.cbsws.obj.EdorInsuredInfo;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BQNIZTInfaceBL;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class BQNIZTInfaceTask extends ABusLogic {

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String BatchNo = "";// 批次号
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	public String mEdorAcceptNo;


	// New Define
	public GrpEdorItemInfo mGrpEdorItemInfo;
	private EdorInsuredInfo mEdorInsuredInfo;
	private LCGrpContSchema mLCGrpContSchema;
	private DealInfo mDealInfo;
	private List tLCInsuredList = new ArrayList();

	public BQNIZTInfaceTask() {
	}

	public boolean deal(MsgCollection cMsgInfos) {
		// 解析XML
		if (!parseXML(cMsgInfos)) {
			return false;
		}
		// 先进行下必要的数据校验
		if (!checkData()) {
			System.out.println("数据校验失败---");
			// 组织返回报文
			getWrapParmList("fail");
			return false;
		}

		mGlobalInput.Operator = Operator;
		TransferData tempTransferData = new TransferData();
		tempTransferData.setNameAndValue("BatchNo", BatchNo);

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tempTransferData);
		data.add(mGrpEdorItemInfo);
		data.add(mLCGrpContSchema);
		data.add(tLCInsuredList);
		BQNIZTInfaceBL tBQNIZTInfaceBL = new BQNIZTInfaceBL();
		if (!tBQNIZTInfaceBL.submit(data)) {
			System.out.println("被保人数据存储错误");
			errLog("" + tBQNIZTInfaceBL.mErrors.getFirstError());
			// 组织返回报文
			getWrapParmList("fail");
			return false;
		} else {
        	//生成报文返回
        	getWrapParmList("success");
        	return true;
        }
	}
        
	private boolean parseXML(MsgCollection cMsgInfos) {
		System.out.println("开始解析保全线上增减人接口请求XML");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		// 获取保单信息
		List tGrpEdorItemList = cMsgInfos.getBodyByFlag("GrpEdorItemInfo");

		if (tGrpEdorItemList == null || tGrpEdorItemList.size() != 1) {
			errLog("申请报文中获取保全项目信息失败。");
			return false;
		}
		mGrpEdorItemInfo = (GrpEdorItemInfo) tGrpEdorItemList.get(0); // 接口一次只接受一个保单的线上增减人

		// 获取被保人信息
		List tEdorInsuredList = cMsgInfos.getBodyByFlag("EdorInsuredInfo");
		if (tEdorInsuredList == null) {
			errLog("申请报文中获取被保人信息失败。");
			return false;
		}

		for (int i = 0; i < tEdorInsuredList.size(); i++) {
			EdorInsuredInfo tEdorInsuredInfo = new EdorInsuredInfo();
			tEdorInsuredInfo = (EdorInsuredInfo) tEdorInsuredList.get(i);
			tLCInsuredList.add(tEdorInsuredInfo);
		}

		// 查询保单信息
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mGrpEdorItemInfo.getGrpContNo());
		if (!tLCGrpContDB.getInfo()) {
			errLog("传入的保单号错误，核心系统不存在保单“" + mGrpEdorItemInfo.getGrpContNo()
					+ "”");
			return false;
		}
		mLCGrpContSchema = tLCGrpContDB.getSchema();

		// 增加并发控制，同一个保单只能请求一次
		// MMap tCekMap = null;
		// tCekMap = lockLGWORK(mLCGrpContSchema);
		// if (tCekMap == null)
		// {
		// errLog("保单"+mLCContSchema.getContNo()+"正在进行犹豫期退保，请不要重复请求");
		// return false;
		// }
		// if(!submit(tCekMap)){
		// return false;
		// }
		return true;
	}

	private void getWrapParmList(String ztFlag) {
		// 判断失败还是成功
		if("success".equals(ztFlag)){
			mDealInfo = new DealInfo();
			mDealInfo.setRemark("数据存储成功");
			putResult("DealInfo", mDealInfo);
		} else if ("fail".equals(ztFlag)) {
			mDealInfo = new DealInfo();
			putResult("DealInfo", mDealInfo);
		}
	}

	private boolean checkData() {
		/** 对保全项目信息的校验 */
		if (null == mGrpEdorItemInfo.getGrpContNo()
				|| "".equals(mGrpEdorItemInfo.getGrpContNo())) {
			errLog("保单号不能为空");
			return false;
		}
		if (null == mGrpEdorItemInfo.getAcceptDate()
				|| "".equals(mGrpEdorItemInfo.getAcceptDate())) {
			errLog("保全申请日期不能为空");
			return false;
		}
		if (null == mGrpEdorItemInfo.getEdorType()
				|| "".equals(mGrpEdorItemInfo.getEdorType())) {
			errLog("保全业务类型不能为空");
			return false;
		}
		if (null == mGrpEdorItemInfo.getEdorValidate()
				|| "".equals(mGrpEdorItemInfo.getEdorValidate())) {
			errLog("保全生效日期不能为空");
			return false;
		}

		/** 对被保人信息的校验 */
		for (int i = 0; i < tLCInsuredList.size(); i++) {
			mEdorInsuredInfo = (EdorInsuredInfo) tLCInsuredList.get(i);
			if (null == mEdorInsuredInfo.getInsuredID()
					|| "".equals(mEdorInsuredInfo.getInsuredID())) {
				errLog("被保人序号不能为空");
				return false;
			}
			if (BQ.EDORTYPE_NI.equals(mGrpEdorItemInfo.getEdorType())) {
				if (null == mEdorInsuredInfo.getContID()
						|| "".equals(mEdorInsuredInfo.getContID())) {
					errLog("增人申请中，合同ID不能为空");
					return false;
				}
			}
			if (null == mEdorInsuredInfo.getInsuredState()
					|| "".equals(mEdorInsuredInfo.getInsuredState())) {
				errLog("被保人状态不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getEmployeeName()
					|| "".equals(mEdorInsuredInfo.getEmployeeName())) {
				errLog("员工姓名不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getName()
					|| "".equals(mEdorInsuredInfo.getName())) {
				errLog("被保人姓名不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getRelationToMainInsured()
					|| "".equals(mEdorInsuredInfo.getRelationToMainInsured())) {
				errLog("与员工关系不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getSex()
					|| "".equals(mEdorInsuredInfo.getSex())) {
				errLog("被保人性别不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getBirthday()
					|| "".equals(mEdorInsuredInfo.getBirthday())) {
				errLog("被保人出生日期不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getIDType()
					|| "".equals(mEdorInsuredInfo.getIDType())) {
				errLog("被保人证件类型不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getIDNo()
					|| "".equals(mEdorInsuredInfo.getIDNo())) {
				errLog("被保人证件号不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getContPlanCode()
					|| "".equals(mEdorInsuredInfo.getContPlanCode())) {
				errLog("保险计划不能为空");
				return false;
			}
			if (null == mEdorInsuredInfo.getOccupationType()
					|| "".equals(mEdorInsuredInfo.getOccupationType())) {
				errLog("职业类别不能为空");
				return false;
			}

			if (!BQ.EDORTYPE_NI.equals(mGrpEdorItemInfo.getEdorType())
					&& !BQ.EDORTYPE_ZT.equals(mGrpEdorItemInfo.getEdorType())) 
			{
				errLog("申请的保全类型与请求不符");
				return false;
			}
		}		
		return true;
	}
        
//        /**
//         * 锁定动作
//         * @param cLCContSchema
//         * @return
//         */
//        private MMap lockLGWORK(LCGrpContSchema cmLCGrpContSchema)
//        {
//            MMap tMMap = null;
//            /**犹豫期退保锁定标志"WT"*/
//            String tLockNoType = "WT";
//            /**锁定时间*/
//            String tAIS = "300";//5分钟的锁
//            TransferData tTransferData = new TransferData();
//            tTransferData.setNameAndValue("LockNoKey", cmLCGrpContSchema.getGrpContNo());
//            tTransferData.setNameAndValue("LockNoType", tLockNoType);
//            tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
//
//            VData tVData = new VData();
//            tVData.add(mGlobalInput);
//            tVData.add(tTransferData);
//
//            LockTableActionBL tLockTableActionBL = new LockTableActionBL();
//            tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
//            if (tMMap == null)
//            {
//                return null;
//            }
//            return tMMap;
//        }    

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			errLog("提交数据库发生错误" + tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		// BQWTQNInfaceTask mCrmInfaceTask = new BQWTQNInfaceTask();
		// mCrmInfaceTask.DealDate();
	}

}
