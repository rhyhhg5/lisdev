/**
 * 2015-12-10
 */
package com.cbsws.xml.ctrl.complexpds;

import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * 团单信息
 * 
 * @author LC
 *
 */
public class LCGrpContTable extends BaseXmlSch {

	private static final long serialVersionUID = 6945840087131288661L;

	/** 团单号 */
	private String GrpContNo = null;

	/** 团体名称 */
	private String GrpName = null;

	/**
	 * @return the polApplyDate
	 */

	public String getGrpContNo() {
		return GrpContNo;
	}

	public void setGrpContNo(String grpContNo) {
		GrpContNo = grpContNo;
	}

	public String getGrpName() {
		return GrpName;
	}

	public void setGrpName(String grpName) {
		GrpName = grpName;
	}

}
