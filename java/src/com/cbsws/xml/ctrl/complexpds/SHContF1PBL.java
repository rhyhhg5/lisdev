/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.cbsws.xml.ctrl.complexpds;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.ES_DOC_PAGESDB;
import com.sinosoft.lis.db.ES_SERVER_INFODB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgenttempDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPolSpecRelaDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskFormDB;
import com.sinosoft.lis.f1print.FeeInvoiceF1PBL;
import com.sinosoft.lis.f1print.LCContF1PBLS;
import com.sinosoft.lis.f1print.ModifyBnfInfoForPrintError;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_SERVER_INFOSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPolSpecRelaSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LDSysVarSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMRiskSet;
import com.sinosoft.utility.CBlob;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataMine;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;
import com.sinosoft.utility.XmlExport;

import examples.newsgroups;

/**
 * <p>
 * ClassName: LCContF1PBL
 * </p>
 * <p>
 * Description: 保单xml文件生成，数据准备类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class SHContF1PBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private File mFile = null;

	// 操作流转控制变量
	private String mOperate = "";

	private int mCheckNo = 0;

	private String mPrtno = "";

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private LCContSchema mLCContSchema = new LCContSchema();

	private LCAppntSchema mLCAppntSchema = new LCAppntSchema();

	private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

	private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

	private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();

	private LDComSchema mLDComSchema = new LDComSchema();

	private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();

	// 扫描件集合
	// private VData mEasyScan = new VData();

	private String mTemplatePath = null;

	private String mOutXmlPath = null;

	private XMLDatasets mXMLDatasets = null;

	private String[][] mScanFile = null;

	private String mSQL;

	private TransferData mTransferData = null;

	private boolean isULI = false;// 个险万能标志

	private String contPrintFlag = "1";// 保单是否打印标记：0，不打印；1，打印

	private String mRemarkFlag = "";

	private String mJetFormType = "";

	private String mComArchiveName = "";

	private String mTaxCode = "";

	private String PrintType = "";

	private String testFlag = "";// 试点打印标记

	private String PadXmlFile;

	private int LostTimes;

	private String mSign;
	
	private String mOrderNo;

	LCContSet mLCContSet = new LCContSet();

	public SHContF1PBL() {
	}

	/**
	 * 主程序
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		try {
			// 判定打印类型
			if (!cOperate.equals("PRINT") && !cOperate.equals("REPRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			// 全局变量赋值
			mOperate = cOperate;
			// 得到外部传入的数据，将数据备份到本类中
			if (!getInputData(cInputData)) {
				return false;
			}
			// pad打印电子保单需求 2016-11-15 lxs
			PrintType = new ExeSQL()
					.getOneValue("select PrintType from lccontsub where prtno='"
							+ mLCContSet.get(1).getPrtNo() + "' with ur");
			if ("".equals(PrintType) || "null".equals(PrintType)
					|| PrintType == null) {
				PrintType = "0";
			}
			LostTimes = (mLCContSet.get(1).getLostTimes());
			System.out.println("LostTimes" + LostTimes);
			System.out
					.println("PrintCount" + mLCContSet.get(1).getPrintCount());
			System.out.println("PrintType" + PrintType);
			if (((LostTimes >= 1) && "1".equals(PrintType))
					|| (-1 == (mLCContSet.get(1).getPrintCount()))
					&& "1".equals(PrintType)) {
				PrintType = "0";
				String mlccontsubsql = "UPDATE lccontsub set PrintType= '"
						+ PrintType + "' where prtno='"
						+ mLCContSet.get(1).getPrtNo() + "'";
				boolean flag = new ExeSQL().execUpdateSQL(mlccontsubsql);
				if (!flag) {
					CError tError = new CError();
					tError.moduleName = "LCContF1PBLS";
					tError.functionName = "saveData";
					tError.errorMessage = "更新lccontsub表信息失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			if (LostTimes == 0) {
				if (!checkdate()) {
					return false;
				}
			}

			// 正常打印处理
			if (mOperate.equals("PRINT")) {
				// 新建一个xml对象
				mXMLDatasets = new XMLDatasets();
				mXMLDatasets.createDocument();
				// 数据准备
				if (!getPrintData()) {
					return false;
				}
			}
			// 重打处理
			if (mOperate.equals("REPRINT")) {
				if (!getRePrintData()) {
					return false;
				}
			}
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("submit", ex.getMessage());
			return false;
		}
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean 如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 获取数据的时候不需要区分打印模式
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mLCContSchema.setSchema((LCContSchema) cInputData
				.getObjectByObjectName("LCContSchema", 0));
		mTemplatePath = (String) cInputData.get(2);
		mOutXmlPath = (String) cInputData.get(3);
		try {
			contPrintFlag = (String) cInputData.get(4);

			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLCContSchema.getContNo());
			mLCContSet = tLCContDB.query();
		} catch (Exception e) {
			// do nothing
		}
		mSign = (String) cInputData.get(5);
		mOrderNo=(String)cInputData.get(6);
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

		return true;
	}

	/**
	 * 返回信息
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return this.mResult;
	}

	/**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LCContF1PBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 正常打印处理
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getPrintData() throws Exception {
		// 个单Schema
		LCContSchema tLCContSchema = null;
		// 原则上一次只打印一个个单，当然可以一次多打
		tLCContSchema = this.mLCContSchema;

		// 取得该个单下的所有投保险种信息
		LCPolSet tLCPolSet = getRiskList(tLCContSchema.getContNo());

		// 正常打印流程，目前只关心这个
		if (!getPolDataSet(tLCPolSet)) {
			return false;
		}

		String tPrtTimes = "-1"; // 为了支持万能险打印,特增加是此属性：第一次保单打印，且打印第一部分时不修改打印次数
		if (mTransferData != null) {
			String tSetPrtTimes = (String) mTransferData
					.getValueByName("SetPrtTimes");
			if (mLCContSchema.getPrintCount() == 0 && "0".equals(tSetPrtTimes)) {
				tPrtTimes = "0";
			}
		}
		// 将准备好的数据，放入mResult对象，传递给BLS进行后台操作
		this.mLCContSchema.setPrintCount(tPrtTimes);
		mResult.add(this.mLCContSchema); // 只更新这个表就好拉
		mResult.add(mXMLDatasets);
		mResult.add(mGlobalInput);
		mResult.add(mOutXmlPath);
		mResult.add(mScanFile);
		mResult.add(mTransferData);
		mResult.add(contPrintFlag);// 保单是否打印标记：0，不打印；1，打印
		mResult.add(testFlag);// 新试点打印标记:"new",打印
		mResult.add(PrintType);// 电子投保书标记

		// 后台提交
		LCContF1PBLS tLCContF1PBLS = new LCContF1PBLS();
		tLCContF1PBLS.submitData(mResult, "PRINT");
		// 判错处理
		if (tLCContF1PBLS.mErrors.needDealError()) {
			mErrors.copyAllErrors(tLCContF1PBLS.mErrors);
			buildError("saveData", "提交数据库出错！");
			return false;
		} // PAD电子投保书需求modify by lxs
		PadXmlFile = tLCContF1PBLS.getPadXmlFile();
		System.out.println("电子保单报文："+PadXmlFile);
		return true;
	}

	public String getPadXmlFile() {
		return PadXmlFile;
	}

	public void setPadXmlFile(String padXmlFile) {
		PadXmlFile = padXmlFile;
	}

	/**
	 * 正常打印流程
	 * 
	 * @param cLCPolSet
	 *            LCPolSet
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getPolDataSet(LCPolSet cLCPolSet) throws Exception {
		// xml对象
		XMLDataset tXMLDataset = mXMLDatasets.createDataset();

		// 获取打印类型，感觉无此需要
		LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
		tLMRiskFormDB.setRiskCode("000000");
		tLMRiskFormDB.setFormType("PG");
		if (!tLMRiskFormDB.getInfo()) {
			// 只有当查询出错的时候才报错，如果是空记录无所谓
			if (tLMRiskFormDB.mErrors.needDealError()) {
				buildError("getInfo", "查询打印模板信息失败！");
				return false;
			}
			// 团个单标志，1个单，2团单
			tXMLDataset.setContType("1");
			// 默认填入空
			tXMLDataset.setTemplate("");
		} else {
			// 团个单标志，1个单，2团单
			tXMLDataset.setContType("1");
			// 打印模板信息
			tXMLDataset.setTemplate(tLMRiskFormDB.getFormName());
		}

		// 查询个单合同表信息
		LCContDB tLCContDB = new LCContDB();
		// 根据合同号查询
		tLCContDB.setContNo(this.mLCContSchema.getContNo());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLCContDB.getInfo()) {
			buildError("getInfo", "查询个单合同信息失败！");
			return false;
		}
		this.mLCContSchema = tLCContDB.getSchema();

		// 校验是否为个险万能保单
		if (mLCContSchema.getContType().equals("1")
				&& (mLCContSchema.getCardFlag() == null
						|| mLCContSchema.getCardFlag().equals("0")
						|| mLCContSchema.getCardFlag().equals("8")
						|| mLCContSchema.getCardFlag().equals("9")
						|| mLCContSchema.getCardFlag().equals("c") || mLCContSchema
						.getCardFlag().equals("d"))) {
			String flag = new ExeSQL()
					.getOneValue("select count(1) from LCPol a, LMRiskApp b where a.ContNo = '"
							+ mLCContSchema.getContNo()
							+ "' and a.RiskCode = b.RiskCode and b.RiskType4 = '4' and a.riskcode not in ('332301','334801') ");// ,'340501'
			isULI = Integer.parseInt(flag) > 0;
		}

		// 判断保单是否为分公司打印
		String sql = "select count(1) from LDCode1 a, LCCont b "
				+ "where a.CodeType = 'printchannel' and a.CodeName = '1' "
				+ "and a.Code = substr(b.ManageCom, 1, 4) and a.Code1 = b.ContType "
				+ "and b.ContNo = '"
				+ mLCContSchema.getContNo()
				+ "' and b.ContType = '1' and b.CardFlag = '0' "
				+ "and not exists (select 1 from LCPol c, LMRiskApp d "
				+ "where c.ContNo = b.ContNo and c.RiskCode = d.RiskCode and d.RiskType4 = '4' and c.riskcode not in ('332301','334801') )";// ,'340501'
		System.out.println(sql);
		boolean isFilialePrint = new ExeSQL().getOneValue(sql).equals("1");

		String RiskWrapCode = "";
		if (mLCContSchema.getCardFlag().equals("9")
				|| mLCContSchema.getCardFlag().equals("c")
				|| mLCContSchema.getCardFlag().equals("d")) {
			String SQL = "select distinct riskwrapcode from lcriskdutywrap where contno = '"
					+ mLCContSchema.getContNo() + "'";
			System.out.println(SQL);
			RiskWrapCode = new ExeSQL().getOneValue(SQL);
		}
		String sqlC = "select count(1) from ldcode where codetype='wraptypeybt' and code='"
				+ RiskWrapCode + "' ";
		System.out.println(sqlC);
		boolean isCardPrint = new ExeSQL().getOneValue(sqlC).equals("1");

		sqlC = "select count(distinct codetype) from ldcode ld where codetype='J025' and exists (select 1 from lcpol where riskcode=ld.code and contno='"
				+ mLCContSchema.getContNo() + "') ";
		System.out.println(sqlC);
		boolean isJ025Print = new ExeSQL().getOneValue(sqlC).equals("1");

//		if (mLCContSchema.getCardFlag().equals("5")) {
//
//			mJetFormType = "J005";
//			testFlag = "new";
//		} else if (isJ025Print) {
//
//			mJetFormType = "J025";
//			testFlag = "new";
//		} else if (mLCContSchema.getCardFlag().equals("6")
//				|| ((mLCContSchema.getCardFlag().equals("9")
//						|| mLCContSchema.getCardFlag().equals("c") || mLCContSchema
//						.getCardFlag().equals("d")) && isCardPrint)) {
//
//			mJetFormType = "J020";
//			testFlag = "new";
//		} else if (mLCContSchema.getCardFlag().equals("8")
//				|| (mLCContSchema.getCardFlag().equals("9"))
//				|| mLCContSchema.getCardFlag().equals("c")
//				|| mLCContSchema.getCardFlag().equals("d")) {
//			// 判断是否为分红险，是分红险采用格式为：J021；否则视为万能险：J018
//			String tStrSql = " select 1 from LCPol lcp inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode where lcp.ContNo = '"
//					+ mLCContSchema.getContNo() + "' and lmra.RiskType4 = '2' ";
//			String tResult = new ExeSQL().getOneValue(tStrSql);
//			if ("1".equals(tResult)) {
//				mJetFormType = "J021";
//				testFlag = "new";
//
//			} else {
//				mJetFormType = "J018";
//				testFlag = "new";
//			}
//			// --------------------
//		} else if (isULI) {
//
//			mJetFormType = "J011";
//			testFlag = "new";
//		} else if (isFilialePrint || mLCContSchema.getCardFlag().equals("2")
//				|| mLCContSchema.getCardFlag().equals("b")) {
//
//			mJetFormType = "ST011";
//			testFlag = "new";
//		} else {
//
//			mJetFormType = "J008";
//		}
//		// modify by lxs
//		if ("PD".equals(mLCContSchema.getPrtNo().substring(0, 2))) {
//			String tStrSql = " select 1 from LCPol lcp inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode where lcp.ContNo = '"
//					+ mLCContSchema.getContNo() + "' and lmra.RiskType4 = '2' ";
//			String tResult = new ExeSQL().getOneValue(tStrSql);
//			if ("1".equals(tResult)) {
//				mJetFormType = "J021";
//				testFlag = "new";
//			}
//		}
//		if ("1".equals(PrintType)) {
//			mJetFormType = mJetFormType + "P";
//		}
		mJetFormType = "ST011A";
		testFlag = "new";
		System.out.println(mJetFormType);
		tXMLDataset.addDataObject(new XMLDataTag("JetFormType", mJetFormType));

		// 增加保单归档号节点
		String ArchiveNo = new ExeSQL()
				.getOneValue("select ArchiveNo from ES_DOC_Main where DocCode = '"
						+ mLCContSchema.getPrtNo()
						+ "' order by DocId fetch first 1 rows only");
		tXMLDataset.addDataObject(new XMLDataTag("ArchiveNo", ArchiveNo));

		// #1882 银行保险部关于特别约定修改的申请
		String tYYRemark = "";
		boolean tySaleChnl = "6".equals(mLCContSchema.getCardFlag())
				|| "8".equals(mLCContSchema.getCardFlag())
				|| "9".equals(mLCContSchema.getCardFlag())
				|| "c".equals(mLCContSchema.getCardFlag())
				|| "d".equals(mLCContSchema.getCardFlag());
		if (tySaleChnl) {
			tYYRemark = "自投保人签收保险单之日起有15个自然日的犹豫期。投保人犹豫期内撤销合同，本公司将无息（扣除不超过10元的工本费）退还投保人所交的保险费。";
		}
		// 将查询出的schema信息放入到xml对象中
		// by gzh 20120512 特殊处理万能G
		String tRemark = this.mLCContSchema.getRemark();
		String IsWNG = new ExeSQL()
				.getOneValue("select 1 from LCRiskDutyWrap where prtno = '"
						+ this.mLCContSchema.getPrtNo()
						+ "' and RiskWrapCode in ('WR0260','WR0270') ");
		if ("1".equals(IsWNG)) {
			String tInitFeeRate = new ExeSQL()
					.getOneValue("select calfactorvalue from LCRiskDutyWrap where prtno = '"
							+ this.mLCContSchema.getPrtNo()
							+ "' and RiskWrapCode in ('WR0260','WR0270') and Calfactor = 'InitFeeRate'");
			if (tInitFeeRate == null || tInitFeeRate == "") {
				tInitFeeRate = "0";
			}
			String tSql = "select DECIMAL(drawrate,4,3) from LCRiskFeeRate where prtno = '"
					+ this.mLCContSchema.getPrtNo() + "' order by feeyear ";
			SSRS tSSRS = new ExeSQL().execSQL(tSql);
			if (tSSRS == null || tSSRS.MaxRow != 4) {
				buildError("getInfo", "查询退保及部分领取比例信息失败！");
				return false;
			}
			String tWNGRemark = "\n 1.初始扣费："
					+ Float.parseFloat(tInitFeeRate)
					+ "% "
					+ "\n 2.第二个保单年度退保及部分领取扣费："
					+ Arith.round(
							Float.parseFloat((tSSRS.GetText(1, 1))) * 100, 1)
					+ "% "
					+ "\n   第三个保单年度退保及部分领取扣费："
					+ Arith.round(
							Float.parseFloat((tSSRS.GetText(2, 1))) * 100, 1)
					+ "% "
					+ "\n   第四个保单年度退保及部分领取扣费："
					+ Arith.round(
							Float.parseFloat((tSSRS.GetText(3, 1))) * 100, 1)
					+ "% "
					+ "\n   第五个保单年度退保及部分领取扣费："
					+ Arith.round(
							Float.parseFloat((tSSRS.GetText(4, 1))) * 100, 1)
					+ "%";
			if (tySaleChnl) {
				tWNGRemark = tWNGRemark + "\n 3." + tYYRemark;
			}
			if (tRemark == null || tRemark == "") {
				this.mLCContSchema.setRemark(tWNGRemark);
			} else {
				tRemark = tRemark + tWNGRemark;
				this.mLCContSchema.setRemark(tRemark);
			}
		}
		// by gzh 20130621 特殊处理万能F 惠享连年
		String IsWNF = new ExeSQL()
				.getOneValue("select 1 from LCRiskDutyWrap where prtno = '"
						+ this.mLCContSchema.getPrtNo()
						+ "' and RiskWrapCode in ('WR0238','WR0272') ");
		if ("1".equals(IsWNF)) {
			String tInitFeeRate = new ExeSQL()
					.getOneValue("select calfactorvalue from LCRiskDutyWrap where prtno = '"
							+ this.mLCContSchema.getPrtNo()
							+ "' and RiskWrapCode in ('WR0238','WR0272') and Calfactor = 'InitFeeRate'");
			if (tInitFeeRate == null || tInitFeeRate == "") {
				tInitFeeRate = "0";
			}
			String tWNGRemark = "\n 1.初始扣费：" + Float.parseFloat(tInitFeeRate)
					+ "% " + "\n 2.第二个保单年度退保及部分领取扣费：0% "
					+ "\n   第三个保单年度退保及部分领取扣费：0% " + "\n   第四个保单年度退保及部分领取扣费：0% "
					+ "\n   第五个保单年度退保及部分领取扣费：0% ";
			if (tySaleChnl) {
				tWNGRemark = tWNGRemark + "\n 3." + tYYRemark;
			}
			if (tRemark == null || tRemark == "") {
				this.mLCContSchema.setRemark(tWNGRemark);
			} else {
				tRemark = tRemark + tWNGRemark;
				this.mLCContSchema.setRemark(tRemark);
			}
		}

		if (!"1".equals(IsWNG) && !"1".equals(IsWNF)) {
			String tYYRemark1 = "";
			if (tySaleChnl) {
				tYYRemark1 = "\n 1." + tYYRemark;
				String t334801Sql = "select lc.riskcode,lc.insuredname,lcd.codealias from lcpol lc,ldcode1 lcd "
						+ "where lc.riskcode=lcd.code1 and lcd.codetype='mainsubriskrela' and lcd.code = '334801'  "
						+ "and exists (select 1 from lcpol where riskcode='334801' and contno=lc.contno) "
						+ "and lc.contno ='" + mLCContSchema.getContNo() + "' ";
				SSRS t334801SSRS = new ExeSQL().execSQL(t334801Sql);
				if (t334801SSRS != null && t334801SSRS.MaxRow == 1) {
					tYYRemark1 += "\n 2.对被保险人<" + t334801SSRS.GetText(1, 2)
							+ ">“附加个人护理保险（万能型，B款）”，双方约定如下："
							+ "\n   (1)本险种的主险为《" + t334801SSRS.GetText(1, 3)
							+ "》，主险合同效力终止，本附加险合同效力即行终止。"
							+ "\n   (2)《附加个人护理保险（万能型，B款）》的投保人为主险《"
							+ t334801SSRS.GetText(1, 3) + "》的被保险人"
							+ t334801SSRS.GetText(1, 2) + "。";
				}
			}
			if (tRemark == null || tRemark == "") {
				this.mLCContSchema.setRemark(tYYRemark1);
			} else {
				tRemark = tRemark + tYYRemark1;
				this.mLCContSchema.setRemark(tRemark);
			}
		}
		LAAgentDB tLAAgentDB = new LAAgentDB();
		tLAAgentDB.setAgentCode(this.mLCContSchema.getAgentCode());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLAAgentDB.getInfo()) {
			buildError("getInfo", "查询销售人员信息失败！");
			return false;
		}
		this.mLAAgentSchema = tLAAgentDB.getSchema();
		String gragent = mLAAgentSchema.getAgentCode();
		this.mLCContSchema.setAgentCode(mLAAgentSchema.getGroupAgentCode());
		this.mLAAgentSchema.setAgentCode(mLAAgentSchema.getGroupAgentCode());
		this.mLAAgentSchema.setGroupAgentCode(gragent);
		tXMLDataset.addSchema(this.mLCContSchema);
		this.mLCContSchema.setAgentCode(gragent);
		mRemarkFlag = this.mLCContSchema.getRemark();
		this.mLCContSchema.setRemark(tRemark);

		String tManageCom = StrTool.cTrim(mLCContSchema.getManageCom());
		tManageCom = (tManageCom).length() >= 4 ? tManageCom.substring(0, 4)
				: tManageCom;
		tXMLDataset
				.addDataObject(new XMLDataTag("ManageComLength4", "DZSW"));

		// 查询投保人信息
		LCAppntDB tLCAppntDB = new LCAppntDB();
		// 根据合同号查询
		tLCAppntDB.setContNo(tLCContDB.getContNo());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLCAppntDB.getInfo()) {
			buildError("getInfo", "查询个单投保人信息失败！");
			return false;
		}
		this.mLCAppntSchema = tLCAppntDB.getSchema();
		// 将查询出的schema信息放入到xml对象中
		tXMLDataset.addSchema(this.mLCAppntSchema);

		// 查询投保人的地址信息
		LCAddressDB tLCAddressDB = new LCAddressDB();
		// 根据投保人编码和地址编码查询
		tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
		tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLCAddressDB.getInfo()) {
			buildError("getInfo", "查询个单投保人地址信息失败！");
			return false;
		}
		this.mLCAddressSchema = tLCAddressDB.getSchema();
		// 将查询出的schema信息放入到xml对象中
		tXMLDataset.addSchema(this.mLCAddressSchema);

		// 查询管理机构信息
		LDComDB tLDComDB = new LDComDB();
		// 根据管理机构查询，管理机构的选取可根据个保险公司不同而不同
		tLDComDB.setComCode(tLCContDB.getManageCom());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLDComDB.getInfo()) {
			buildError("getInfo", "查询管理机构信息失败！");
			return false;
		}
		this.mLDComSchema = tLDComDB.getSchema();
		if ("Y".equals(mLDComSchema.getInteractFlag())) {
			tLDComDB.setComCode(tLCContDB.getManageCom().substring(0, 4));
			if (!tLDComDB.getInfo()) {
				buildError("getInfo", "查询管理机构信息失败！");
				return false;
			}
			mLDComSchema.setName(tLDComDB.getSchema().getName());
		}
		// 将查询出的schema信息放入到xml对象中
		tXMLDataset.addSchema(this.mLDComSchema);

		// 将查询出的schema信息放入到xml对象中
		tXMLDataset.addSchema(this.mLAAgentSchema);

		// 专管员代资证编码
		String tLAAgentQualIfno = new ExeSQL()
				.getOneValue("select qualifno from LAQualification where agentcode = '"
						+ gragent + "' ");
		tXMLDataset.addDataObject(new XMLDataTag("LAAgentQualIfno",
				tLAAgentQualIfno));

		// 增加LACom.Name等结点是否存在的标志性结点
		LAComDB tLAComDB = new LAComDB();
		tLAComDB.setAgentCom(mLCContSchema.getAgentCom());
		if (tLAComDB.getInfo()) {
			tXMLDataset.addDataObject(new XMLDataTag("LAComFlag", "0"));
			tXMLDataset.addSchema(tLAComDB.getSchema());
		} else {
			tXMLDataset.addDataObject(new XMLDataTag("LAComFlag", "1"));
		}

		LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
		tLABranchGroupDB.setAgentGroup(this.mLCContSchema.getAgentGroup());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLABranchGroupDB.getInfo()) {
			buildError("getInfo", "查询销售机构信息失败！");
			return false;
		}
		this.mLABranchGroupSchema = tLABranchGroupDB.getSchema();
		// 将查询出的schema信息放入到xml对象中
		tXMLDataset.addSchema(this.mLABranchGroupSchema);

		// 张成轩 2012-5-25
		// 中介机构销售人员信息
		String tAgentSaleCode = this.mLCContSchema.getAgentSaleCode();
		if (tAgentSaleCode != null && !"".equals(tAgentSaleCode)) {
			LAAgenttempDB tLAAgenttempDB = new LAAgenttempDB();
			tLAAgenttempDB.setAgentCode(tAgentSaleCode);
			// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
			if (!tLAAgenttempDB.getInfo()) {
				buildError("getInfo", "查询中介机构代理业务员信息失败！");
				return false;
			}
			this.mLAAgenttempSchema = tLAAgenttempDB.getSchema();
		}
		tXMLDataset.addSchema(this.mLAAgenttempSchema);

		// 专管员代资证编码
		String tLAAgentTempQualIfno = new ExeSQL()
				.getOneValue("select qualifno from LAQualification where agentcode = '"
						+ this.mLAAgenttempSchema.getAgentCode() + "' ");
		tXMLDataset.addDataObject(new XMLDataTag("LAAgentTempQualIfno",
				tLAAgentTempQualIfno));

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();

		StringBuffer tSBql = new StringBuffer(128);
		tSBql.append("select distinct TempFeeNo from LJTempFee where OtherNo = '");
		tSBql.append(this.mLCContSchema.getContNo());
		tSBql.append("' and OtherNoType = '2'");

		// tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSBql.toString());

		System.out.println(tSSRS.MaxNumber);
		if (tSSRS.MaxNumber == 0) {
			mCheckNo = 1;
			mPrtno = mLCContSchema.getPrtNo().substring(0,
					mLCContSchema.getPrtNo().length() - 2);
			System.out.println(mPrtno);
			String tSBql2 = "select distinct TempFeeNo from LJTempFee where OtherNo in "
					+ "(select contno from lccont where prtno='"
					+ mPrtno
					+ "' and appflag='1') and OtherNoType = '2'";
			tSSRS = tExeSQL.execSQL(tSBql2);
		}
		if (!StrTool.cTrim(this.mLCContSchema.getAppFlag()).equals("9")) {
			tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo", tSSRS
					.GetText(1, 1))); // 收据号
		}

		// 保单保费合计。
		tSBql = new StringBuffer(128);

		if (com.sinosoft.lis.bq.CommonBL.hasULIRisk(this.mLCContSchema
				.getContNo())) {
			tSBql.append(" select nvl(sum(lcp.Prem), 0) + nvl(sum(lcp.SupplementaryPrem), 0) ");
			tSBql.append(" from LCPol lcp ");
			tSBql.append(" where lcp.ContNo = '"
					+ this.mLCContSchema.getContNo() + "' ");
		} else {
			tSBql.append("select sum(prem) from lcpol where ContNo = '");
			tSBql.append(this.mLCContSchema.getContNo());
			tSBql.append("'");
		}
		// -----------------------------------

		// tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSBql.toString());
		tXMLDataset.addDataObject(new XMLDataTag("SumPremCHS", PubFun
				.getChnMoney(Double.parseDouble(tSSRS.GetText(1, 1))))); // 总保费
		tXMLDataset.addDataObject(new XMLDataTag("SumPrem", format(Double
				.parseDouble(tSSRS.GetText(1, 1))))); // 总保费

		String cDate = PubFun.getCurrentDate().split("-")[0] + "年"
				+ PubFun.getCurrentDate().split("-")[1] + "月"
				+ PubFun.getCurrentDate().split("-")[2] + "日";
		tXMLDataset.addDataObject(new XMLDataTag("Today", cDate));

		// 保险合同生效日
		String tUWDate = mLCContSchema.getUWDate();
		if (tUWDate != null && !"".equals(tUWDate)) {
			// tUWDate = PubFun.calDate(tUWDate, 1, "D", null);
			tUWDate = tUWDate.split("-")[0] + "年" + tUWDate.split("-")[1] + "月"
					+ tUWDate.split("-")[2] + "日";
		}
		tXMLDataset.addDataObject(new XMLDataTag("UWDateNext", tUWDate));
		// --------------------

		// 增加：保单打印时间/签单时间/收费时间
		String tTmpContNo = mLCContSchema.getContNo();
		if (mCheckNo == 1) {
			ExeSQL tExeSQLok = new ExeSQL();
			String SQLok = "select contno from lccont where prtno='" + mPrtno
					+ "'";
			String tCont = tExeSQLok.getOneValue(SQLok);
			tTmpContNo = tCont;
		}
		String tContPrintDate = cDate;
		String tContPrintTime = PubFun.getCurrentTime();
		tContPrintTime = tContPrintTime.split(":")[0] + "时"
				+ tContPrintTime.split(":")[1] + "分";
		tXMLDataset.addDataObject(new XMLDataTag("ContPrintDateTime",
				tContPrintDate + tContPrintTime));

		String tContSignDate = tExeSQL
				.getOneValue("select SignDate from LCCont where ContNo = '"
						+ tTmpContNo + "'");
		tContSignDate = tContSignDate.split("-")[0] + "年"
				+ tContSignDate.split("-")[1] + "月"
				+ tContSignDate.split("-")[2] + "日";
		String tContSignTime = tExeSQL
				.getOneValue("select replace(varchar(SignTime), '.', ':') from LCCont where ContNo = '"
						+ tTmpContNo + "'");
		if (tContSignTime == null || tContSignTime.equals(""))
			tContSignTime = PubFun.getCurrentTime();
		tContSignTime = tContSignTime.split(":")[0] + "时"
				+ tContSignTime.split(":")[1] + "分";
		tXMLDataset.addDataObject(new XMLDataTag("ContSignDateTime",
				tContSignDate + tContSignTime));

		String tPayConfDate = tExeSQL
				.getOneValue("select max(ConfMakeDate) from LJTempFee where OtherNo = '"
						+ tTmpContNo + "'");
		tPayConfDate = tPayConfDate.split("-")[0] + "年"
				+ tPayConfDate.split("-")[1] + "月" + tPayConfDate.split("-")[2]
				+ "日";
		String tPayConfTime = tExeSQL
				.getOneValue("select replace(varchar(max(ConfMakeTime)), '.', ':') from LJTempFee where OtherNo = '"
						+ tTmpContNo
						+ "' and ConfMakeDate = (select max(ConfMakeDate) from LJTempFee where OtherNo = '"
						+ tTmpContNo + "')");
		if (tPayConfTime == null || tPayConfTime.equals("")) {
			tPayConfTime = tExeSQL
					.getOneValue("select replace(varchar(max(MakeTime)), '.', ':') from LJTempFee where OtherNo = '"
							+ tTmpContNo
							+ "' and ConfMakeDate = (select max(ConfMakeDate) from LJTempFee where OtherNo = '"
							+ tTmpContNo + "')");
		}
		tPayConfTime = tPayConfTime.split(":")[0] + "时"
				+ tPayConfTime.split(":")[1] + "分";
		tXMLDataset.addDataObject(new XMLDataTag("PayConfDateTime",
				tPayConfDate + tPayConfTime));
		// --------------------

		sqlC = "select distinct 1 from lcpol lc,lmriskapp lm where lm.riskcode=lc.riskcode and lm.TaxOptimal='Y' and lc.contno='"
				+ mLCContSchema.getContNo() + "' ";
		boolean isSYFlag = new ExeSQL().getOneValue(sqlC).equals("1");
		if (isSYFlag) {
			LCContSubDB tLCContSubDB = new LCContSubDB();
			tLCContSubDB.setPrtNo(mLCContSchema.getPrtNo());
			if (tLCContSubDB.getInfo()) {
				mTaxCode = "税优识别码：" + tLCContSubDB.getTaxCode();
				// +"\n税优健康险平台网址：health.ciitc.com.cn";
			}
		}

		// 套餐名称
		String tWrapName = getWrapName(mLCContSchema.getContNo(), isSYFlag);
		tXMLDataset.addDataObject(new XMLDataTag("WrapName", tWrapName));
		// --------------------

		// 得到套餐名称的全称
		String strWrapName = new ExeSQL()
				.getOneValue("select codename from ldcode where codetype = 'wrapnameall' and code in(select riskwrapcode from lcriskdutywrap where prtno = '"
						+ mLCContSchema.getPrtNo()
						+ "') fetch first 1 rows only");

		if (tySaleChnl) {
			if (strWrapName == null || strWrapName == "") {
				strWrapName = tYYRemark;
				String t334801Sql = "select lc.riskcode,lc.insuredname,lcd.codealias from lcpol lc,ldcode1 lcd "
						+ "where lc.riskcode=lcd.code1 and lcd.codetype='mainsubriskrela' and lcd.code = '334801'  "
						+ "and exists (select 1 from lcpol where riskcode='334801' and contno=lc.contno) "
						+ "and lc.contno ='" + mLCContSchema.getContNo() + "' ";
				SSRS t334801SSRS = new ExeSQL().execSQL(t334801Sql);  
				if (t334801SSRS != null && t334801SSRS.MaxRow == 1) {
					strWrapName = "\n 1." + tYYRemark + "\n 2.对被保险人<"
							+ t334801SSRS.GetText(1, 2)
							+ ">“附加个人护理保险（万能型，B款）”，双方约定如下："
							+ "\n   (1)本险种的主险为《" + t334801SSRS.GetText(1, 3)
							+ "》，主险合同效力终止，本附加险合同效力即行终止。"
							+ "\n   (2)《附加个人护理保险（万能型，B款）》的投保人为主险《"
							+ t334801SSRS.GetText(1, 3) + "》的被保险人"
							+ t334801SSRS.GetText(1, 2) + "。";
				}
			} else {
				strWrapName = "\n 1." + strWrapName + "\n 2." + tYYRemark;
			}
		}
		System.out.println(strWrapName);
		tXMLDataset.addDataObject(new XMLDataTag("ComArchiveName", StrTool
				.cTrim(strWrapName)));
		mComArchiveName = StrTool.cTrim(strWrapName);

		tXMLDataset.addDataObject(new XMLDataTag("TaxCode", StrTool
				.cTrim(mTaxCode)));

		// 处理受益人信息
		if (!DealBnf(tXMLDataset)) {
			return false;
		}
		// 产生个单下明细信息，根据配置文件生成xml数据
		if (!genInsuredList(tXMLDataset)) {
			return false;
		}
		System.out.println("个单下被保人明细准备完毕。。。");

		// 获取持续奖金信息
		if (!getBankInsurance(tXMLDataset)) {
			return false;
		}

		// 获取适用条款信息
		if (!getTermSpecDoc(tXMLDataset, cLCPolSet)) {
			return false;
		}

		// 获取特约信息
		if (!getSpecDoc(tXMLDataset, cLCPolSet)) {
			return false;
		}
		
		// 获取现金价值信息，尚未定义
		if (!getCashValue(tXMLDataset, cLCPolSet)) {
			return false;
		}
		System.out.println("现金价值明细准备完毕。。。");
		if (!getProposalForm(tXMLDataset)) {
			return false;
		}
		// 获取条款信息，尚未定义
		if (!getTerm(tXMLDataset, cLCPolSet)) {
			return false;
		}

		if (!getOmnipotence(tXMLDataset, cLCPolSet)) {
			return false;
		}
		
		System.out.println("条款明细准备完毕。。。");

		// 获取EasyScan文件信息
		if (!getScanPic(tXMLDataset, mLCContSchema)) {
			return false;
		}
		System.out.println("影印件明细准备完毕。。。");

		// 获取指定医院信息
		if (!getHospital(tXMLDataset)) {
			return false;
		}
		System.out.println("指定医院明细准备完毕。。。");

		// 获取定点医院说明
		if (!getHospitalRemark(tXMLDataset)) {
			return false;
		}
		System.out.println("定点医院明细准备完毕。。。");

		// 获取推荐医院信息
		if (!getSpecHospital(tXMLDataset)) {
			return false;
		}
		System.out.println("推荐医院明细准备完毕。。。");

		// 获取特殊产品指定医院信息 by gzh 20110708
		if (!getSpecRiskHospital(tXMLDataset)) {
			return false;
		}
		System.out.println("特殊产品医疗机构明细准备完毕。。。");

		// 获取发票信息，尚未定义
		if (!getInvoice(tXMLDataset)) {
			return false;
		}
		System.out.println("发票明细准备完毕。。。");

		// 判断是否被保人中，存在未成年人。
		// <ExistsMinor />
		// true：存在；false：不存在
		if (!getMinor(tXMLDataset)) {
			return false;
		}

		// 获取收款凭证
		if (!getReceiptNote(tXMLDataset)) {
			return false;
		}

		String ttSql = "select 1 from loprtmanager2 "
				+ "where code='35' and otherno in ( "
				+ "select payno from ljapay " + "where incomeno='"
				+ mLCContSchema.getContNo() + "') ";
		SSRS ttSSRS = new ExeSQL().execSQL(ttSql);
		String tttsql1 = "select 1 from LYOutPayDetail " + "where insureno='"
				+ this.mLCContSchema.getPrtNo()
				+ "' and billprintdate is not null ";
		SSRS tttSSRS1 = new ExeSQL().execSQL(tttsql1);
		if (ttSSRS.getMaxRow() > 0 || tttSSRS1.getMaxRow() > 0) {
			tXMLDataset.addDataObject(new XMLDataTag("ReceiptNoteFlag", "0"));
		} else {

			// 保费收据字段标识 0：不打收据 1：打收据
			tXMLDataset.addDataObject(new XMLDataTag("ReceiptNoteFlag", "1"));

			// 保单号
			tXMLDataset.addDataObject(new XMLDataTag("ReceiptPolicyNo",
					mLCContSchema.getContNo()));

			// 实收号
			String tStrSql0 = "select payno from ljapay " + "where incomeno='"
					+ mLCContSchema.getContNo() + "' ";
			SSRS tSSRS0 = new ExeSQL().execSQL(tStrSql0);
			if (tSSRS0.getMaxRow() > 0) {
				tXMLDataset.addDataObject(new XMLDataTag("ReceiptPayNo", tSSRS0
						.GetText(1, 1)));

			}

			// 投保单位/投保人
			String tStrSql2 = "select appntname from lcappnt "
					+ "where contno='" + mLCContSchema.getContNo() + "' ";
			SSRS tSSRS2 = new ExeSQL().execSQL(tStrSql2);
			if (tSSRS2.getMaxRow() > 0) {
				tXMLDataset.addDataObject(new XMLDataTag("ReceiptAppName",
						tSSRS2.GetText(1, 1)));
			}

			// 交费方式
			String tStrSql3 = "select codename from ldcode "
					+ "where codetype='paymode' and code in ("
					+ "select paymode from lccont " + "where contno='"
					+ mLCContSchema.getContNo() + "')";
			SSRS tSSRS3 = new ExeSQL().execSQL(tStrSql3);
			if (tSSRS3.getMaxRow() > 0) {
				tXMLDataset.addDataObject(new XMLDataTag("ReceiptWayToPay",
						tSSRS3.GetText(1, 1)));
			}

			// 总保费金额 人民币大小写
			String tStrSql4 = "select sum(sumactupaymoney) from ljapay "
					+ "where incomeno='" + mLCContSchema.getContNo()
					+ "' and duefeetype='0'";
			String a = tExeSQL.getOneValue(tStrSql4);
			if (a != null && !a.equals("")) {
				double p = Double.parseDouble(a);
				tXMLDataset.addDataObject(new XMLDataTag("ReceiptTotalPrem",
						new java.text.DecimalFormat("#.00").format(p)));
				tXMLDataset.addDataObject(new XMLDataTag("ReceiptTotalPremC",
						PubFun.getChnMoney(p)));
			}

			// 收款方名称和地址
			String tStrSql5 = "select name,address from ldcom "
					+ "where comcode in ( " + "select managecom from lccont "
					+ "where contno='" + mLCContSchema.getContNo() + "')";
			SSRS tSSRS5 = new ExeSQL().execSQL(tStrSql5);
			if (tSSRS5.getMaxRow() > 0) {
				tXMLDataset.addDataObject(new XMLDataTag(
						"ReceiptReceivingName", tSSRS5.GetText(1, 1)));
				tXMLDataset.addDataObject(new XMLDataTag("ReceiptReceivingAdd",
						tSSRS5.GetText(1, 2)));
			}

			// 获取险种名称以及险种保费
			if (!getPremDetail(tXMLDataset)) {
				return false;
			}
		}

		// 个人健康调查问卷
		if (!getSurvey(tXMLDataset)) {
			return false;
		}

		if (!combineInvioce(tXMLDataset)) {
			return false;
		}

		System.out.println("查找未成年被保人准备完毕。。。");

		return true;
	}

	/**
	 * combineInvioce 合并发票xml
	 * 
	 * @param tXMLDataset
	 *            XMLDataset
	 * @return boolean
	 */
	public boolean combineInvioce(XMLDataset tXMLDataset) {
		// 银保分红产品，需要打印这个节点
		String tStrSql = " select 1 from LCPol lcp inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode where lcp.ContNo = '"
				+ mLCContSchema.getContNo() + "' and lmra.RiskType4 = '2' ";
		String tResult = new ExeSQL().getOneValue(tStrSql);
		if ("1".equals(tResult)) {
			Element tInvioceElement = new Element("Invoice");
			Element tSubElement = new Element("Head");
			tInvioceElement.addContent(tSubElement);
			tXMLDataset.getElementNoClone().addContent(tInvioceElement);
			return true;
		}
		// --------------------

		// 银保万能、个险万能和电话销售的保单打印界面触发的打印才需要发送新外包
		if (mTransferData == null
				|| (!StrTool.cTrim(mLCContSchema.getCardFlag()).equals("8")
						&& !StrTool.cTrim(mLCContSchema.getCardFlag()).equals(
								"9")
						&& !StrTool.cTrim(mLCContSchema.getCardFlag()).equals(
								"c")
						&& !StrTool.cTrim(mLCContSchema.getCardFlag()).equals(
								"d")
						&& !StrTool.cTrim(mLCContSchema.getCardFlag()).equals(
								"5")
						&& !StrTool.cTrim(mLCContSchema.getCardFlag()).equals(
								"6") && !isULI)) {
			return true;
		}

		if (!"1".equals(mLCContSchema.getAppFlag())) {
			System.out.println("未签单，不用打印发票");
			return true;
		}

		XmlExport tXmlExport = getInvioceXml(); // 得到发票xml
		if (tXmlExport == null) {
			return false;
		}

		Element tInvioceElement = new Element("Invoice");

		Element tElement = null;
		List tNodeList = tXmlExport.getDocument().getRootElement()
				.getChildren();
		for (int i = 0; i < tNodeList.size(); i++) {
			tElement = (Element) tNodeList.get(i);

			// 若节点是控制信息节点，则不需要合并
			if ("CONTROL".equals(tElement.getName())) {
				continue;
			}

			Element e = new Element(tElement.getName());
			e.setText(tElement.getText());
			tInvioceElement.addContent(e);
		}

		// 最后进入保单xml的节点
		tXMLDataset.getElementNoClone().addContent(tInvioceElement);

		return true;
	}

	/**
	 * 调用生成发票程序生成发票xml
	 * 
	 * @return XmlExport
	 */
	private XmlExport getInvioceXml() {
		String sql = "select * from LJApay where IncomeNo = '"
				+ mLCContSchema.getContNo()
				+ "' and IncomeType = '2' and GetNoticeNo is null ";

		LJAPayDB tLJAPayDB = new LJAPayDB();
		LJAPaySet tLJAPaySet = tLJAPayDB.executeQuery(sql);
		if (tLJAPayDB.mErrors.needDealError()) {
			mErrors.addOneError("查询实收数据出错");
			return null;
		}
		if (tLJAPaySet.size() == 0) {
			mErrors.addOneError("没有查询到财务实收，无法生成财务信息");
			return null;
		}

		// 得到险种信息
		sql = "select distinct c.AppntName, "
				+ "  (case when exists(select 1 from LDCode1 where CodeType = 'bankcheckappendrisk' and Code1 = a.RiskCode) " // 若是绑定主险，则显示套餐名
				+ "           then (select RiskWrapPlanName from LDCode1 where CodeType = 'bankcheckappendrisk' and Code1 = a.RiskCode) "
				+ "        when exists(select 1 from LDCode1 where CodeType = 'bankcheckappendrisk' and Code = a.RiskCode) " // 若是绑定主险，则显示套餐名
				+ "           then (select RiskWrapPlanName from LDCode1 where CodeType = 'bankcheckappendrisk' and Code = a.RiskCode) "
				+ "        else RiskName end), a.Riskcode "
				+ "from LCPol a, LMRiskApp b, LCCont c "
				+ "where c.ContNo = a.ContNo "
				+ "   and a.Riskcode = b.Riskcode " + "   and a.ContNo = '"
				+ mLCContSchema.getContNo() + "' "
				+ "order by a.RiskCode with ur ";
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tExeSQL.mErrors.needDealError()) {
			mErrors.addOneError("查询险种信息失败");
			return null;
		}
		if (tSSRS.getMaxRow() == 0) {
			mErrors.addOneError("没有查询到保单险种");
			return null;
		}

		String tRiskName = tSSRS.GetText(1, 2);
		if (tSSRS.getMaxRow() > 1) {
			tRiskName += "等";
		}
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("AppntName", tSSRS.GetText(1, 1));
		tTransferData.setNameAndValue("RiskName", tSSRS.GetText(1, 1));
		tTransferData.setNameAndValue("HPerson", "");
		tTransferData.setNameAndValue("CPerson", "");
		tTransferData.setNameAndValue("Remark", "");
		tTransferData.setNameAndValue("invoiceNo", "");

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLJAPaySet.get(1));
		data.add(tTransferData);

		FeeInvoiceF1PBL bl = new FeeInvoiceF1PBL();
		if (!bl.submitData(data, "CONFIRM")) {
			mErrors.copyAllErrors(bl.mErrors);
			return null;
		}

		data = bl.getResult();
		if (data == null) {
			mErrors.addOneError("生成发票信息出错");
			return null;
		}

		XmlExport tXmlExport = (XmlExport) data.getObjectByObjectName(
				"XmlExport", 0);
		if (tXmlExport == null) {
			mErrors.addOneError("生成发盘出错");
			return null;
		}

		return tXmlExport;
	}

	/**
	 * 持续奖金查询
	 * 
	 * @param cXmlDataset
	 * @return
	 * @throws Exception
	 */
	private boolean getBankInsurance(XMLDataset cXmlDataset) throws Exception {
		String sql = "select ItemName from LDRiskPrint where ItemType in ('A', 'B', 'C') "
				+ "and RiskCode in (select a.RiskCode from LCPol a, LMRiskApp b where ContNo = '"
				+ this.mLCContSchema.getContNo()
				+ "' and a.RiskCode = b.RiskCode and b.RiskType4 = '4') order by ItemType";
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.getMaxRow() != 3) {
			System.out.println("没有描述持续奖金！");
			return true;
		}

		XMLDataList xmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		xmlDataList.setDataObjectID("BankInsuranceFlag");
		xmlDataList.addColHead("Description");
		xmlDataList.addColHead("Algorithm");
		xmlDataList.addColHead("Proportion");
		xmlDataList.buildColHead();

		xmlDataList.setColValue("Description", tSSRS.GetText(1, 1));
		xmlDataList.setColValue("Algorithm", tSSRS.GetText(2, 1));
		xmlDataList.setColValue("Proportion", tSSRS.GetText(3, 1));
		xmlDataList.insertRow(0);
		cXmlDataset.addDataObject(xmlDataList);

		return true;
	}

	/**
	 * 条款信息查询
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getSpecDoc(XMLDataset cXmlDataset, LCPolSet cLCPolSet)
			throws Exception {
		XMLDataList xmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		xmlDataList.setDataObjectID("LCSpec");
		// 添加Head单元内的信息
		xmlDataList.addColHead("RowID");
		xmlDataList.addColHead("SpecContent");
		xmlDataList.addColHead("InsuredName");
		xmlDataList.addColHead("RiskName");
		xmlDataList.addColHead("RiskCode");
		xmlDataList.addColHead("InsuredIdNo");
		xmlDataList.buildColHead();
		// 查询合同下的特约信息
		LCPolSpecRelaDB tLCPolSpecRelaDB = new LCPolSpecRelaDB();
		LCPolSpecRelaSet tLCPolSpecRelaSet = new LCPolSpecRelaSet();
		String SpecID = "";
		String Content = "";
		int LCSpecFlag = 0;// 特约信息标记
		// 是否是套餐绑定销售

		if ("2".equals(mLCContSchema.getCardFlag())
				&& !"F".equals(mLCContSchema.getFamilyType())) {
			if (cLCPolSet.size() > 0) {

				String sql = "select SpecContent from LCPolSpecRela where contno='"
						+ this.mLCContSchema.getContNo()
						+ "' and polno=(select polno from LCPolSpecRela where contno='"
						+ this.mLCContSchema.getContNo()
						+ "' fetch first 1 rows only) order by specno";
				ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = tExeSQL.execSQL(sql);

				for (int j = 1; j <= tSSRS.getMaxRow(); j++, LCSpecFlag++) {
					SpecID = "注1";
					Content = "(" + j + ")" + tSSRS.GetText(j, 1);

					// 查询被保险人身份证号 2008-4-17
					LCInsuredDB cLCInsuredDB = new LCInsuredDB();
					cLCInsuredDB.setContNo(mLCContSchema.getContNo());
					cLCInsuredDB.setInsuredNo(mLCContSchema.getInsuredNo());
					LCInsuredSet cLCInsuredSet = cLCInsuredDB.query();
					String insuredIdNo = cLCInsuredSet.size() > 0 ? cLCInsuredSet
							.get(1).getIDNo() : "";

					xmlDataList.setColValue("RowID", SpecID);
					xmlDataList.setColValue("SpecContent", Content);
					xmlDataList.setColValue("InsuredName",
							mLCContSchema.getInsuredName());
					xmlDataList.setColValue("RiskName",
							getWrapName(mLCContSchema.getPrtNo(), false));
					xmlDataList.setColValue("RiskCode",
							getWrapCode(mLCContSchema.getPrtNo()));
					xmlDataList.setColValue("InsuredIdNo", insuredIdNo);
					xmlDataList.insertRow(0);
				}
			}
			cXmlDataset.addDataObject(xmlDataList);

		} else {
			for (int i = 1; i <= cLCPolSet.size(); i++) {
				// 如该险种在打印时不显示，那么该险种的特约信息也将不进行显示。
				LCPolSchema cLCPolSchema = cLCPolSet.get(i);
				String tPolNo = cLCPolSchema.getPolNo();
				String tContNO = this.mLCContSchema.getContNo();
				String tStrSql = " select 1 from lcpol "
						+ " where contno = '"
						+ tContNO
						+ "' "
						+ " and polno = '"
						+ tPolNo
						+ "' "
						+ " and RiskCode in (select Code from LDCode1 where CodeType = 'checkappendrisk' and RiskWrapPlanName is not null) "
						+ " with ur ";
				ExeSQL tExeSQL = new ExeSQL();
				String isFind = tExeSQL.getOneValue(tStrSql);

				if ("1".equals(isFind))
					continue;

				// -----------------------------------------------------

				tLCPolSpecRelaDB.setContNo(this.mLCContSchema.getContNo());
				tLCPolSpecRelaDB.setPolNo(cLCPolSchema.getPolNo());
				if ("122601".equals(cLCPolSchema.getRiskCode())
						|| "122901".equals(cLCPolSchema.getRiskCode())) {
					tLCPolSpecRelaSet = tLCPolSpecRelaDB
							.executeQuery("select * from LCPolSpecRela where polno='"
									+ cLCPolSchema.getPolNo()
									+ "'order by specno");
				} else {
					tLCPolSpecRelaSet = tLCPolSpecRelaDB.query();
				}
				for (int j = 1; j <= tLCPolSpecRelaSet.size(); j++, LCSpecFlag++) {
					SpecID = "注" + tLCPolSpecRelaSet.get(j).getSpecCode();
					Content = "(" + j + ")"
							+ tLCPolSpecRelaSet.get(j).getSpecContent();

					// 查询险种名称 2008-4-17
					LMRiskDB cLMRiskDB = new LMRiskDB();
					cLMRiskDB.setRiskCode(cLCPolSchema.getRiskCode());
					LMRiskSet cLMRiskSet = cLMRiskDB.query();
					// 险种绑定销售，提取套餐名
					String riskName = "";
					String isExistsFR = "select 1 from ldcode1 where  CodeType = 'checkappendrisk' "
							+ "and RiskWrapPlanName is not null and code1 = '"
							+ cLCPolSchema.getRiskCode()
							+ "' "
							+ "and code in (select riskcode from lcpol where  ContNo = '"
							+ tContNO + "') with ur";
					String isBindingSales = tExeSQL.getOneValue(isExistsFR);
					if ("1".equals(isBindingSales)) {
						String xRiskName = getWrapNameByRisk(tContNO,
								cLCPolSchema.getRiskCode());
						if (!"".equals(xRiskName) && xRiskName != null) {
							riskName = xRiskName;
						} else {
							riskName = cLMRiskSet.size() > 0 ? cLMRiskSet
									.get(1).getRiskName() : "";
						}
					} else {
						riskName = cLMRiskSet.size() > 0 ? cLMRiskSet.get(1)
								.getRiskName() : "";
					}
					System.out.println(riskName);

					// 查询被保险人身份证号 2008-4-17
					LCInsuredDB cLCInsuredDB = new LCInsuredDB();
					cLCInsuredDB.setContNo(mLCContSchema.getContNo());
					cLCInsuredDB.setInsuredNo(cLCPolSchema.getInsuredNo());
					LCInsuredSet cLCInsuredSet = cLCInsuredDB.query();
					String insuredIdNo = cLCInsuredSet.size() > 0 ? cLCInsuredSet
							.get(1).getIDNo() : "";

					xmlDataList.setColValue("RowID", SpecID);
					xmlDataList.setColValue("SpecContent", Content);
					xmlDataList.setColValue("InsuredName",
							cLCPolSchema.getInsuredName());
					xmlDataList.setColValue("RiskName", riskName);
					xmlDataList.setColValue("RiskCode",
							cLCPolSchema.getRiskCode());
					xmlDataList.setColValue("InsuredIdNo", insuredIdNo);
					xmlDataList.insertRow(0);
				}
			}
			for (int i = 1; i <= cLCPolSet.size(); i++, LCSpecFlag++) {
				LCPolSchema cLCPolSchema = cLCPolSet.get(i);
				String triskcode = cLCPolSchema.getRiskCode();
				double tmult = cLCPolSchema.getMult();

				// 查询险种名称 2008-4-17
				LMRiskDB cLMRiskDB = new LMRiskDB();
				cLMRiskDB.setRiskCode(cLCPolSchema.getRiskCode());
				LMRiskSet cLMRiskSet = cLMRiskDB.query();
				String riskName = cLMRiskSet.size() > 0 ? cLMRiskSet.get(1)
						.getRiskName() : "";

				String tsql = "select 1 from ldcode "
						+ "where codetype='sypremnult' and code='" + triskcode
						+ "' ";
				SSRS tSSRS = new ExeSQL().execSQL(tsql);

				String isExists1 = "（1）被保险人的医疗费用支付方式不满足条款中关于补充医疗保险的释义，按照被保险人未参加补充医疗保险对应的风险保险费投保。";
				String isExists2 = "（1）被保险人的医疗费用支付方式满足条款中关于补充医疗保险的释义，按照被保险人已参加补充医疗保险对应的风险保险费投保。";

				if (tSSRS.getMaxRow() > 0) {
					if (tmult == 1.0) {
						LCSpecFlag++;
						Content = isExists1;
						xmlDataList.setColValue("RowID", "注" + LCSpecFlag);
						xmlDataList.setColValue("SpecContent", Content);
						xmlDataList.setColValue("InsuredName",
								cLCPolSchema.getInsuredName());
						xmlDataList.setColValue("RiskName", riskName);
						xmlDataList.setColValue("RiskCode",
								cLCPolSchema.getRiskCode());
						xmlDataList.setColValue("InsuredIdNo", "");
						xmlDataList.insertRow(0);
					} else if (tmult == 2.0) {
						LCSpecFlag++;
						Content = isExists2;
						xmlDataList.setColValue("RowID", "注" + LCSpecFlag);
						xmlDataList.setColValue("SpecContent", Content);
						xmlDataList.setColValue("InsuredName",
								cLCPolSchema.getInsuredName());
						xmlDataList.setColValue("RiskName", riskName);
						xmlDataList.setColValue("RiskCode",
								cLCPolSchema.getRiskCode());
						xmlDataList.setColValue("InsuredIdNo", "");
						xmlDataList.insertRow(0);
					}
				}
			}
			if ("F".equals(mLCContSchema.getFamilyType())) {
				String WR0296Spec = "select codename||codealias from ldcode where codetype='lccontprint' and code='"
						+ getWrapCode(mLCContSchema.getPrtNo()) + "' ";
				ExeSQL tExeSQL1 = new ExeSQL();
				String isExists = tExeSQL1.getOneValue(WR0296Spec);
				if (!"".equals(isExists)) {
					LCSpecFlag++;
					Content = isExists;
					xmlDataList.setColValue("RowID", "注" + LCSpecFlag);
					xmlDataList.setColValue("SpecContent", Content);
					xmlDataList.setColValue("InsuredName", "");
					xmlDataList.setColValue("RiskName",
							getWrapName(mLCContSchema.getPrtNo(), false));
					xmlDataList.setColValue("RiskCode",
							getWrapCode(mLCContSchema.getPrtNo()));
					xmlDataList.setColValue("InsuredIdNo", "");
					xmlDataList.insertRow(0);
				}
			}

			cXmlDataset.addDataObject(xmlDataList);
		}

		// 增加特约信息标记节点
		if (LCSpecFlag > 0) {
			cXmlDataset.addDataObject(new XMLDataTag("LCSpecFlag", "1"));
		} else {
			cXmlDataset.addDataObject(new XMLDataTag("LCSpecFlag", "0"));
		}
		return true;
	}

	/**
	 * 条款信息查询 会出现多险种对应同意主条款 排序方式为主条款，享有此主条款的险种的责任条款 可采用二级循环来实现，不过比较麻烦
	 * 
	 * @param xmlDataset
	 *            XMLDataset
	 * @param aLCPolSet
	 *            LCPolSet
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getTerm(XMLDataset xmlDataset, LCPolSet aLCPolSet)
			throws Exception {
		XMLDataList tXmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		tXmlDataList.setDataObjectID("Term");
		// 添加Head单元内的信息
		tXmlDataList.addColHead("PrintIndex");
		tXmlDataList.addColHead("TermName");
		tXmlDataList.addColHead("FileName");
		tXmlDataList.addColHead("DocumentName");
		tXmlDataList.addColHead("RiskWrapPlanName");
		tXmlDataList.buildColHead();

		// 设置查询对象
		ExeSQL tExeSQL = new ExeSQL();
		// 查询保单下的险种条款，根据条款级别排序
		// String tSql =
		// "select distinct a.ItemName,a.ItemType,a.FileName from LDRiskPrint a,LCPol b where b.ContNo = '"
		// +
		// this.mLCContSchema.getContNo() +
		// "' and a.RiskCode = b.RiskCode order by a.ItemType";
		// 查询合同下的全部唯一主条款信息
		// String tSql =
		// "select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '"
		// +
		// this.mLCContSchema.getContNo() + "') and ItemType = '0'";
		StringBuffer tSBql = new StringBuffer();
		// tSBql
		// .append("select distinct ItemName,FileName,riskcode from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '");
		// tSBql.append(this.mLCContSchema.getContNo());
		// tSBql.append("') and ItemType = '0' order by FileName ");
		tSBql.append(" select * from ");
		tSBql.append(" ( ");
		tSBql.append(" select ");
		tSBql.append(" distinct ldrp.ItemName, ldrp.FileName, ldrp.RiskCode, ");
		tSBql.append(" (trim(lmra.SubRiskFlag) || case when (select distinct lcp.RiskCode from LCPol lcp where 1 = 1 and lcp.PolNo in (select lcp.MainPolNo from LCPol lcp where 1 = 1 and lcp.ContNo = '"
				+ this.mLCContSchema.getContNo()
				+ "' and lcp.RiskCode = ldrp.RiskCode) fetch first 1 rows only) is null then ldrp.RiskCode else (select distinct lcp.RiskCode from LCPol lcp where 1 = 1 and lcp.PolNo in (select lcp.MainPolNo from LCPol lcp where 1 = 1 and lcp.ContNo = '"
				+ this.mLCContSchema.getContNo()
				+ "' and lcp.RiskCode = ldrp.RiskCode) fetch first 1 rows only) end || trim(lmra.SubRiskFlag)) MSRiskFlag");
		// tSBql.append(" ,(select SequenceNo from lcinsured where contno = lcp.contno and insuredno = lcp.insuredno) SequenceNo ");
		tSBql.append(" ,lcp.RiskSeqNo RiskSeqNo ");
		tSBql.append(" from LDRiskPrint ldrp ");
		tSBql.append(" inner join LMRiskApp lmra on lmra.RiskCode = ldrp.RiskCode ");
		tSBql.append(" inner join LCPol lcp on lcp.RiskCode = ldrp.RiskCode ");
		tSBql.append(" where 1 = 1 ");
		// tSBql.append(" and ldrp.RiskCode in (select RiskCode from LCPol where ContNo = '"
		// + this.mLCContSchema.getContNo() + "') ");
		tSBql.append(" and lcp.ContNo = '" + this.mLCContSchema.getContNo()
				+ "' ");
		tSBql.append(" and ItemType = '0' ");
		tSBql.append(" ) as tmp ");
		tSBql.append(" order by int(tmp.RiskSeqNo), tmp.FileName ");

		SSRS tSSRS = new SSRS();
		SSRS tSSRS2 = new SSRS();
		System.out.println(tSBql.toString());
		tSSRS = tExeSQL.execSQL(tSBql.toString());
		if (tSSRS.getMaxRow() > 0) {
			HashMap map = new HashMap();
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				if (map.get(tSSRS.GetText(i, 2)) == null) {
					tXmlDataList.setColValue("PrintIndex", i);
					tXmlDataList.setColValue("TermName", tSSRS.GetText(i, 1));
					tXmlDataList.setColValue("FileName", "0");
					tXmlDataList.setColValue("DocumentName",
							tSSRS.GetText(i, 2));
					tXmlDataList.setColValue("RiskWrapPlanName",
							getRiskWrapPlanName(tSSRS.GetText(i, 3)));
					tXmlDataList.insertRow(0);
					map.put(tSSRS.GetText(i, 2), "1");
				}
				// 查询当前主条款下的责任条款信息
				// tSql =
				// "select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '"
				// +
				// this.mLCContSchema.getContNo() +
				// "') and ItemType = '1' and RiskCode in (select RiskCode from LDRiskPrint where FileName = '"
				// +
				// tSSRS.GetText(i, 2) + "')";
				tSBql = new StringBuffer(256);
				if ("122303".equals(tSSRS.GetText(i, 3))
						|| "122304".equals(tSSRS.GetText(i, 3))) {
					tSBql.append("select distinct ItemName,trim(FileName)||getRiskPlan(lcp.riskcode,mult) ");
					tSBql.append("from LDRiskPrint ldr,lcpol lcp ");
					tSBql.append("where ldr.riskcode=lcp.riskcode ");
					tSBql.append("and ldr.ItemType='1' ");
					tSBql.append("and lcp.ContNo = '"
							+ this.mLCContSchema.getContNo() + "' ");
					tSBql.append("and ldr.riskcode in  (select RiskCode from LDRiskPrint where riskcode = '"
							+ tSSRS.GetText(i, 3) + "') ");
				} else {
					tSBql.append("select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '");
					tSBql.append(this.mLCContSchema.getContNo());
					tSBql.append("') and ItemType = '1' and RiskCode in (select RiskCode from LDRiskPrint where riskcode = '");
					tSBql.append(tSSRS.GetText(i, 3));
					tSBql.append("')");
				}

				tSSRS2 = tExeSQL.execSQL(tSBql.toString());
				for (int j = 1; j <= tSSRS2.getMaxRow(); j++) {
					tXmlDataList.setColValue("PrintIndex", i);
					tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
					tXmlDataList.setColValue("FileName", "1");
					tXmlDataList.setColValue("DocumentName",
							tSSRS2.GetText(j, 2));
					tXmlDataList.setColValue("RiskWrapPlanName",
							getRiskWrapPlanName(tSSRS.GetText(i, 3)));
					tXmlDataList.insertRow(0);
				}
			}
		} else {
			tSBql = new StringBuffer(256);
			// tSBql
			// .append("select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '");
			// tSBql.append(this.mLCContSchema.getContNo());
			tSBql.append("select * from ( ");
			tSBql.append("select distinct ItemName,FileName, ");
			// tSBql.append("(select SequenceNo from lcinsured where contno = lcp.contno and insuredno = lcp.insuredno), ");
			tSBql.append("lcp.RiskSeqNo RiskSeqNo ");
			tSBql.append("from LDRiskPrint ldrp inner join LCPol lcp on lcp.RiskCode = ldrp.RiskCode ");
			tSBql.append("where 1=1 ");
			tSBql.append("and lcp.ContNo = '" + this.mLCContSchema.getContNo()
					+ "' ");
			tSBql.append("and ldrp.ItemType = '1' ");
			tSBql.append(") as temp ");
			tSBql.append("order by int(temp.RiskSeqNo) ");

			tSSRS2 = tExeSQL.execSQL(tSBql.toString());
			for (int j = 1; j <= tSSRS2.getMaxRow(); j++) {
				tXmlDataList.setColValue("PrintIndex", j);
				tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
				tXmlDataList.setColValue("FileName", "1");
				tXmlDataList.setColValue("DocumentName", tSSRS2.GetText(j, 2));
				tXmlDataList.setColValue("RiskWrapPlanName",
						getRiskWrapPlanName(tSSRS.GetText(j, 3)));
				tXmlDataList.insertRow(0);
			}
		}
		xmlDataset.addDataObject(tXmlDataList);
		return true;
	}

	/**
	 * 万能险条款信息查询 根据lcpol的信息生成多个数据 排序方式为主条款，享有此主条款的险种的责任条款
	 * 
	 * @param xmlDataset
	 *            XMLDataset
	 * @param aLCPolSet
	 *            LCPolSet
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getOmnipotence(XMLDataset xmlDataset, LCPolSet aLCPolSet)
			throws Exception {
		XMLDataList tXmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		tXmlDataList.setDataObjectID("Omnipotence");
		// 添加Head单元内的信息
		tXmlDataList.addColHead("PrintIndex"); // 被保人编号
		tXmlDataList.addColHead("InsuredNo"); // 被保人编号
		tXmlDataList.addColHead("InsuredName"); // 被保人姓名
		tXmlDataList.addColHead("InsuredSex"); // 被保人性别
		tXmlDataList.addColHead("Riskcode"); // 险种编码
		tXmlDataList.addColHead("IdType"); // 证件类型
		tXmlDataList.addColHead("IdNo"); // 证件号码
		tXmlDataList.addColHead("Prem"); // 保险费
		tXmlDataList.addColHead("ManageFee"); // 初始费用
		tXmlDataList.addColHead("InsuAccBala"); // 初始帐户价值
		tXmlDataList.addColHead("SupplementaryPrem"); // 首期契约追加保费
		tXmlDataList.addColHead("RiskPremiums"); // 风险保费
		tXmlDataList.buildColHead();

		if (aLCPolSet.size() > 0) {
			for (int i = 1; i <= aLCPolSet.size(); i++) {
				LCPolSchema tLCPolSchema = new LCPolSchema();
				tLCPolSchema = aLCPolSet.get(i);

				if (!isOmnipotence(tLCPolSchema.getRiskCode())) {
					continue;
				}

				String tInsuredNo = tLCPolSchema.getInsuredNo();
				String tInsuredName = tLCPolSchema.getInsuredName();

				LCInsuredDB tLCInsuredDB = new LCInsuredDB();
				tLCInsuredDB.setInsuredNo(tInsuredNo);
				tLCInsuredDB.setContNo(tLCPolSchema.getContNo());
				if (!tLCInsuredDB.getInfo()) {
					return false;
				}
				String tInsuredSex = tLCInsuredDB.getSex();
				if (tInsuredSex.trim().equals("0")) {
					tInsuredSex = "男";
				} else {
					tInsuredSex = "女";
				}

				String tIdNo = tLCInsuredDB.getIDNo();
				String tIdType = tLCInsuredDB.getIDType();
				LDCodeDB tLDCodeDB = new LDCodeDB();
				tLDCodeDB.setCodeType("idtype");
				tLDCodeDB.setCode(tIdType.trim());
				if (!tLDCodeDB.getInfo()) {
					// 添加证件类型出错提示 by zhangyang
					String tErrorString = "该被保险人" + tInsuredName + "的证件类型为："
							+ tIdType + "，不符合证件类别填写规则，请进行纠正！";
					System.out.println("Error: " + tErrorString);
					buildError("getOmnipotence", tErrorString);
					return false;
				}
				tIdType = tLDCodeDB.getCodeName();
				String tRiskcode = tLCPolSchema.getRiskCode();

				// String tStrSql =
				// " select nvl(sum(sumactupaymoney), 0) from ljapayperson "
				// + " where polno = '"
				// + tLCPolSchema.getPolNo()
				// + "' "
				// +
				// " and paycount=1 and(getnoticeno is null or getnoticeno='') "
				// + " with ur ";

				// 首期契约期缴保费。
				String tStrSql = " select nvl(sum(sumactupaymoney), 0) "
						+ " from ljapayperson ljapp "
						+ " where ljapp.paycount = 1 "
						+ " and (ljapp.getnoticeno is null or ljapp.getnoticeno = '') "
						+ " and ljapp.PayType = 'ZC' " + " and ljapp.polno = '"
						+ tLCPolSchema.getPolNo() + "' ";
				ExeSQL tExeSQL = new ExeSQL();
				String tPrem = tExeSQL.getOneValue(tStrSql);
				// -------------------------------------------

				// 首期契约追加保费。
				String tStrSql1 = " select nvl(sum(sumactupaymoney), 0) "
						+ " from ljapayperson ljapp "
						+ " where ljapp.paycount = 1 "
						+ " and (ljapp.getnoticeno is null or ljapp.getnoticeno = '') "
						+ " and ljapp.PayType = 'ZB' " + " and ljapp.polno = '"
						+ tLCPolSchema.getPolNo() + "' ";
				ExeSQL tExeSQL1 = new ExeSQL();
				String tSupplementaryPrem = tExeSQL1.getOneValue(tStrSql1);
				// -------------------------------------------

				// 首期契约帐户相关管理费用总额。--不包括风险保费
				// String tStrSq2 =
				// " select nvl(sum(fee), 0) from lcinsureaccfeetrace a "
				// + " where otherno = '1' and polno = '"
				// + tLCPolSchema.getPolNo()
				// + "' "
				// +
				// " and exists(select 1 from lmriskfee where feecode=a.feecode and feetakeplace in ('01', '06')) "
				// + " with ur ";
				String tStrSq2 = " select nvl(sum(lciaft.fee), 0) "
						+ " from LCInsureAccFeeTrace lciaft "
						+ " where lciaft.OtherType = '1' "
						+ " and exists (select 1 from LMRiskFee lmrf where lmrf.feecode = lciaft.feecode and lmrf.feetakeplace in ('01', '06')) "
						+ " and lciaft.feecode<>'000099' "
						+ " and lciaft.PolNo = '" + tLCPolSchema.getPolNo()
						+ "' ";
				ExeSQL tExeSQL2 = new ExeSQL();
				String tManageFee = tExeSQL2.getOneValue(tStrSq2);
				// -------------------------------------------
				// 风险保费
				tStrSq2 = " select nvl(sum(lciaft.fee), 0) "
						+ " from LCInsureAccFeeTrace lciaft "
						+ " where lciaft.OtherType = '1' "
						+ " and exists (select 1 from LMRiskFee lmrf where lmrf.feecode = lciaft.feecode and lmrf.feetakeplace in ('01', '06')) "
						+ " and lciaft.feecode='000099' "
						+ " and lciaft.PolNo = '" + tLCPolSchema.getPolNo()
						+ "' ";
				String tRiskFee = tExeSQL2.getOneValue(tStrSq2);

				String tInsuAccBala = com.sinosoft.lis.bq.CommonBL
						.bigDoubleToCommonString(
								Double.parseDouble(tPrem)
										+ Double.parseDouble(tSupplementaryPrem)
										- Double.parseDouble(tManageFee)
										- Double.parseDouble(tRiskFee), "0.00");
				System.out.println(tInsuredNo);
				System.out.println(tInsuredName);
				System.out.println(tInsuredSex);
				System.out.println(tRiskcode);
				System.out.println(tIdType);
				System.out.println(tIdNo);
				System.out.println(tPrem);
				System.out.println(tManageFee);
				System.out.println(tInsuAccBala);
				System.out.println(tSupplementaryPrem);
				tXmlDataList.setColValue("PrintIndex", i);
				tXmlDataList.setColValue("InsuredNo", tInsuredNo);
				tXmlDataList.setColValue("InsuredName", tInsuredName);
				tXmlDataList.setColValue("InsuredSex", tInsuredSex);
				tXmlDataList.setColValue("Riskcode", tRiskcode);
				tXmlDataList.setColValue("IdType", tIdType);
				tXmlDataList.setColValue("IdNo", tIdNo);
				tXmlDataList.setColValue("Prem", tPrem);
				tXmlDataList.setColValue("ManageFee", tManageFee);
				tXmlDataList.setColValue("InsuAccBala", tInsuAccBala);
				tXmlDataList.setColValue("SupplementaryPrem",
						format(Double.parseDouble(tSupplementaryPrem)));
				tXmlDataList.setColValue("RiskPremiums", tRiskFee);

				tXmlDataList.insertRow(0);
			}
		}

		xmlDataset.addDataObject(tXmlDataList);
		return true;
	}

	private boolean isOmnipotence(String cRiskCode) {
		LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
		tLMRiskAppDB.setRiskCode(cRiskCode);
		tLMRiskAppDB.setRiskType4("4");
		if (tLMRiskAppDB.query().size() == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * getRiskWrapPlanName 若险种是主附险绑定型，则返回“×××保障计划由***险种和附加***险种组成”
	 * 
	 * @param RiskCode
	 *            String
	 * @return String
	 */
	private String getRiskWrapPlanName(String cRiskCode) {
		String sql = "select RiskWrapPlanName, "
				+ "  (select RiskName from LMRisk where RiskCode=a.Code1),"
				+ "   (select RiskName from LMRisk where RiskCode=a.Code) "
				+ "from LDCode1 a " + "where CodeType = 'checkappendrisk' "
				+ "   and (Code = '" + cRiskCode + "' or " + "       Code1 = '"
				+ cRiskCode + "') ";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS.getMaxRow() == 0) {
			return "";
		}

		return tSSRS.GetText(1, 1) + "保障计划由" + tSSRS.GetText(1, 2) + "险种和附加"
				+ tSSRS.GetText(1, 2) + "险种组成";
	}

	/**
	 * 获取现金价值表信息
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @param cLCPolSet
	 *            LCPolSet
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getCashValue(XMLDataset cXmlDataset, LCPolSet ccLCPolSet)
			throws Exception {
		// 特殊标签
		XMLDataList tXMLDataList = new XMLDataList();
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();

		// 现价存在标志
		boolean tFlag = false;
		int j = 0;

		// 按需求对现价显示顺序进行调整
		if (ccLCPolSet == null || ccLCPolSet.size() == 0) {
			return true; // 若未发现险种，直接跳出
		}
		String tContNo = ccLCPolSet.get(1).getContNo();
		String tStrSql = " select lcp.* "
				+ " from LCPol lcp "
				+ " inner join LCInsured lci on lci.ContNo = lcp.ContNo and lci.InsuredNo = lcp.InsuredNo "
				+ " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
				+ " where 1 = 1 "
				+ " and lcp.ContNo ='"
				+ tContNo
				+ "' "
				+ " order by int(lci.SequenceNo), lmra.SubRiskFlag, int(lcp.RiskSeqNo) ";
		LCPolSet cLCPolSet = new LCPolDB().executeQuery(tStrSql);
		// --------------------

		// 循环处理险种下的现金价值信息
		for (int i = 1; i <= cLCPolSet.size(); i++) {
			String tRiskCode = cLCPolSet.get(i).getRiskCode();

			// 若是绑定型附加险，则不需要打印现金价值表
			if (CommonBL.isAppendRisk(tRiskCode)) {
				continue;
			}

			// 得到现金价值的算法描述
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			// 获取险种信息
			tLMCalModeDB.setRiskCode(tRiskCode);
			tLMCalModeDB.setType("X");
			LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

			// 解析得到的SQL语句
			String strSQL = "";
			// 如果描述记录唯一时处理
			if (tLMCalModeSet.size() == 1) {
				strSQL = tLMCalModeSet.get(1).getCalSQL();
			}
			// 这个险种不需要取现金价值的数据
			if (strSQL.equals("")) {
			} else {
				tFlag = true;
				tXMLDataList.setDataObjectID("CashValueInfo"
						+ String.valueOf(j));
				tXMLDataList.addColHead("InsuredName");
				tXMLDataList.addColHead("RiskName");
				tXMLDataList.buildColHead();

				tXMLDataList.setColValue("InsuredName", cLCPolSet.get(i)
						.getInsuredName());
				String tSql = "select riskname from lmriskapp where riskcode = '"
						+ cLCPolSet.get(i).getRiskCode() + "'";
				tSSRS = tExeSQL.execSQL(tSql);

				String riskWrapPlanName = CommonBL
						.getRiskWrapPlanName(tRiskCode); // 绑定型险种套餐名
				tXMLDataList.setColValue("RiskName",
						riskWrapPlanName == null ? tSSRS.GetText(1, 1)
								: riskWrapPlanName);
				tXMLDataList.insertRow(0);
				cXmlDataset.addDataObject(tXMLDataList);

				// 现金价值标签
				tXMLDataList = new XMLDataList();
				tXMLDataList.setDataObjectID("CashValue" + String.valueOf(j));
				tXMLDataList.addColHead("Year");
				tXMLDataList.addColHead("Date");
				tXMLDataList.addColHead("Cash");
				tXMLDataList.buildColHead();
				j = j + 1;

				Calculator calculator = new Calculator();
				// 设置基本的计算参数
				calculator.addBasicFactor("ContNo", cLCPolSet.get(i)
						.getContNo());
				calculator.addBasicFactor("InsuredNo", cLCPolSet.get(i)
						.getInsuredNo());
				calculator.addBasicFactor("InsuredSex", cLCPolSet.get(i)
						.getInsuredSex());
				calculator.addBasicFactor("InsuredAppAge",
						String.valueOf(cLCPolSet.get(i).getInsuredAppAge()));
				calculator.addBasicFactor("PayIntv",
						String.valueOf(cLCPolSet.get(i).getPayIntv()));
				calculator.addBasicFactor("PayEndYear",
						String.valueOf(cLCPolSet.get(i).getPayEndYear()));
				calculator.addBasicFactor("PayEndYearFlag",
						String.valueOf(cLCPolSet.get(i).getPayEndYearFlag()));
				calculator.addBasicFactor("PayYears",
						String.valueOf(cLCPolSet.get(i).getPayYears()));
				calculator.addBasicFactor("InsuYear",
						String.valueOf(cLCPolSet.get(i).getInsuYear()));
				calculator.addBasicFactor("Prem",
						String.valueOf(cLCPolSet.get(i).getPrem()));
				calculator.addBasicFactor("Amnt",
						String.valueOf(cLCPolSet.get(i).getAmnt()));
				calculator.addBasicFactor("FloatRate",
						String.valueOf(cLCPolSet.get(i).getFloatRate()));
				calculator.addBasicFactor("Mult",
						String.valueOf(cLCPolSet.get(i).getMult()));
				calculator.addBasicFactor("Copys",
						String.valueOf(cLCPolSet.get(i).getCopys()));
				// add by yt 2004-3-10
				calculator.addBasicFactor("InsuYearFlag",
						String.valueOf(cLCPolSet.get(i).getInsuYearFlag()));
				calculator.addBasicFactor("GetYear",
						String.valueOf(cLCPolSet.get(i).getGetYear()));
				calculator.addBasicFactor("GetYearFlag",
						String.valueOf(cLCPolSet.get(i).getGetYearFlag()));
				calculator.addBasicFactor("CValiDate",
						String.valueOf(cLCPolSet.get(i).getCValiDate()));
				calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
				strSQL = calculator.getCalSQL();

				// tXMLDataList.setColValue("Year", "0");
				// tXMLDataList.setColValue("Date", cLCPolSet.get(i)
				// .getCValiDate());
				// tXMLDataList.insertRow(0);

				System.out.println(strSQL);
				// 执行现金价值计算sql，获取现金价值列表
				Connection conn = DBConnPool.getConnection();
				if (null == conn) {
					buildError("getConnection", "连接数据库失败！");
					return false;
				}
				Statement stmt = null;
				ResultSet rs = null;
				try {
					stmt = conn.createStatement();
					rs = stmt.executeQuery(strSQL);
					int nCount = 0;
					while (rs.next()) {
						// 往xml对象中添加现金价值信息
						tXMLDataList
								.setColValue("Year", rs.getString(1).trim());
						tXMLDataList
								.setColValue("Date", rs.getString(2).trim());
						tXMLDataList.setColValue("Cash",
								format(rs.getDouble(3)));
						tXMLDataList.insertRow(0);
						nCount++;
					}
					cXmlDataset.addDataObject(tXMLDataList);
					// 加入现金价值记录的总的条数
					// cXmlDataset.addDataObject(new
					// XMLDataTag("CashValueCount",
					// nCount));
					rs.close();
					stmt.close();
					conn.close();
				} catch (Exception ex) {
					if (null != rs) {
						rs.close();
					}
					stmt.close();
					try {
						conn.close();
					} catch (Exception e) {
					}
					;
					// 出错处理
					buildError("executeQuery", "数据库查询失败！");
					return false;
				}
				if ("231301".equals(tRiskCode)) {
					cXmlDataset.createByContent("CashValueRemark",
							"RemarkValue",
							"4、本现金价值表所列为康乐人生个人重大疾病保险与附加康乐人生个人护理保险的合计金额。");
				}
			}
		}
		// 如果有现价，才显示标签
		/*
		 * if (tFlag) { //全部现金价值信息取完毕后，将xmlDataList数据添加到xmlDataset中 }
		 */
		return true;
	}

	/**
	 * 查询影印件数据
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @param cLCContSchema
	 *            LCContSchema
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getScanPic(XMLDataset cXmlDataset,
			LCContSchema cLCContSchema) throws Exception {
		XMLDataList tXmlDataList = new XMLDataList();
		// 标签对象
		tXmlDataList.setDataObjectID("PicFile");
		// 标签中的Header
		tXmlDataList.addColHead("FileUrl");
		tXmlDataList.addColHead("HttpUrlOut");
		tXmlDataList.addColHead("PageIndex");
		tXmlDataList.addColHead("HttpUrlIn");
		tXmlDataList.addColHead("FileName");
		tXmlDataList.buildColHead();

		mSQL = "select * from es_doc_main where doccode like '"
				+ cLCContSchema.getPrtNo()
				+ "%' and BussType = 'TB' and SubType not in ('TB16','TB15') order by doccode";
		System.out.println("mSQL is " + mSQL);
		ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
		ES_DOC_MAINSet tES_DOC_MAINDBSet = new ES_DOC_MAINSet();
		tES_DOC_MAINDBSet = tES_DOC_MAINDB.executeQuery(mSQL);
		int piccount = 1;
		if (tES_DOC_MAINDBSet.size() > 0) {
			// 查询内部服务URL
			LDSysVarDB tLDSysVarDB = new LDSysVarDB();
			tLDSysVarDB.setSysVar("InnerServiceURL");
			LDSysVarSet tLDSysVarSet = tLDSysVarDB.query();
			if (tLDSysVarSet.size() != 1) {
				buildError("getScanPic", "缺少内部服务URL的描述数据！");
				return false;
			}
			String InnerServiceURL = tLDSysVarSet.get(1).getSysVarValue();

			ES_SERVER_INFODB tES_SERVER_INFODB = new ES_SERVER_INFODB();
			ES_SERVER_INFOSet tES_SERVER_INFOSet = new ES_SERVER_INFOSet();
			// 因为没有查询条件，所以直接查询会报异常。修改为SQL查询
			// tES_SERVER_INFOSet = tES_SERVER_INFODB.query();
			tES_SERVER_INFOSet = tES_SERVER_INFODB
					.executeQuery("select * from ES_SERVER_INFO");
			String tHttpHeadOut = "http://"
					+ tES_SERVER_INFOSet.get(1).getServerPort() + "/";
			String tHttpHeadIn = InnerServiceURL + "/";
			// String trealpath = this.getClass().getResource("/").getPath();
			String trealpath = new ExeSQL()
					.getOneValue("select SysVarValue from LDSysVar where SysVar = 'UIRoot'");
			int k = 0;
			for (int i = 0; i < tES_DOC_MAINDBSet.size(); i++) {
				k = k + (int) tES_DOC_MAINDBSet.get(i + 1).getNumPages();
			}
			String tStrUrlOut = "";
			String tStrUrlIn = "";
			mScanFile = new String[k][6];
			for (int j = 0; j < tES_DOC_MAINDBSet.size(); j++) {
				ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
				ES_DOC_PAGESSet tES_DOC_PAGESSet = new ES_DOC_PAGESSet();
				// tES_DOC_PAGESDB.setDocID(tES_DOC_MAINDBSet.get(j +
				// 1).getDocID());
				// tES_DOC_PAGESSet = tES_DOC_PAGESDB.query();
				tES_DOC_PAGESSet = tES_DOC_PAGESDB
						.executeQuery("select * from es_doc_pages where docid="
								+ tES_DOC_MAINDBSet.get(j + 1).getDocID()
								+ " order by pagecode asc");
				if (tES_DOC_PAGESSet.size() > 0) {
					// 错误影印件明细
					String errorInf = "";
					// 错误影印件计数
					int errorCont = 0;
					// 错误标记
					boolean errorFlag = true;
					for (int m = 0; m < tES_DOC_PAGESSet.size(); m++) {
						String tpath = tES_DOC_PAGESSet.get(m + 1).getPicPath();
						// String tcheckhead =
						// trealpath.replaceAll("WEB-INF/classes/",tpath);
						String tcheckhead = trealpath + tpath;
						String tCheckUrl = tcheckhead
								+ tES_DOC_PAGESSet.get(m + 1).getPageName()
								+ ".tif";
						File cScanFile = new File(tCheckUrl);
						if (!cScanFile.exists()) {
							String tnewpath = tpath.replaceAll("EasyScan2016",
									"EasyScan2014");
							tcheckhead = trealpath + tnewpath;
							tCheckUrl = tcheckhead
									+ tES_DOC_PAGESSet.get(m + 1).getPageName()
									+ ".tif";
							cScanFile = new File(tCheckUrl);
							if (!cScanFile.exists()) {
								tnewpath = tpath.replaceAll("EasyScan2016",
										"EasyScan2012");
								tcheckhead = trealpath + tnewpath;
								tCheckUrl = tcheckhead
										+ tES_DOC_PAGESSet.get(m + 1)
												.getPageName() + ".tif";
								cScanFile = new File(tCheckUrl);
								if (!cScanFile.exists()) {
									tnewpath = tpath.replaceAll("EasyScan2016",
											"EasyScan2011");
									tcheckhead = trealpath + tnewpath;
									tCheckUrl = tcheckhead
											+ tES_DOC_PAGESSet.get(m + 1)
													.getPageName() + ".tif";
									cScanFile = new File(tCheckUrl);
									if (!cScanFile.exists()) {
										tnewpath = tpath.replaceAll(
												"EasyScan2016", "EasyScan");
										tcheckhead = trealpath + tnewpath;
										tCheckUrl = tcheckhead
												+ tES_DOC_PAGESSet.get(m + 1)
														.getPageName() + ".tif";
										cScanFile = new File(tCheckUrl);
										if (!cScanFile.exists()) {// 以EasyScan2014为基础
											tnewpath = tpath.replaceAll(
													"EasyScan2014",
													"EasyScan2012");
											tcheckhead = trealpath + tnewpath;
											tCheckUrl = tcheckhead
													+ tES_DOC_PAGESSet.get(
															m + 1)
															.getPageName()
													+ ".tif";
											cScanFile = new File(tCheckUrl);
											if (!cScanFile.exists()) {
												tnewpath = tpath.replaceAll(
														"EasyScan2014",
														"EasyScan2011");
												tcheckhead = trealpath
														+ tnewpath;
												tCheckUrl = tcheckhead
														+ tES_DOC_PAGESSet.get(
																m + 1)
																.getPageName()
														+ ".tif";
												cScanFile = new File(tCheckUrl);
												if (!cScanFile.exists()) {
													tnewpath = tpath
															.replaceAll(
																	"EasyScan2014",
																	"EasyScan");
													tcheckhead = trealpath
															+ tnewpath;
													tCheckUrl = tcheckhead
															+ tES_DOC_PAGESSet
																	.get(m + 1)
																	.getPageName()
															+ ".tif";
													cScanFile = new File(
															tCheckUrl);
													if (!cScanFile.exists()) {// 以EasyScan2012为基础
														tnewpath = tpath
																.replaceAll(
																		"EasyScan2012",
																		"EasyScan2011");
														tcheckhead = trealpath
																+ tnewpath;
														tCheckUrl = tcheckhead
																+ tES_DOC_PAGESSet
																		.get(m + 1)
																		.getPageName()
																+ ".tif";
														cScanFile = new File(
																tCheckUrl);
														if (!cScanFile.exists()) {
															tnewpath = tpath
																	.replaceAll(
																			"EasyScan2012",
																			"EasyScan");
															tcheckhead = trealpath
																	+ tnewpath;
															tCheckUrl = tcheckhead
																	+ tES_DOC_PAGESSet
																			.get(m + 1)
																			.getPageName()
																	+ ".tif";
															cScanFile = new File(
																	tCheckUrl);
															if (!cScanFile
																	.exists()) {// 以EasyScan2011为基础
																tnewpath = tpath
																		.replaceAll(
																				"EasyScan2011",
																				"EasyScan");
																tcheckhead = trealpath
																		+ tnewpath;
																tCheckUrl = tcheckhead
																		+ tES_DOC_PAGESSet
																				.get(m + 1)
																				.getPageName()
																		+ ".tif";
																cScanFile = new File(
																		tCheckUrl);
																if (!cScanFile
																		.exists()) {
																	System.out
																			.println("新老路径都不存在文件");
																	// 显示明细错误影印件明细页
																	errorInf = errorInf
																			+ (m + 1)
																			+ "、";
																	// 遇到丢失影印件errorFlag置为false
																	errorFlag = false;
																	// 错误影印件计数
																	errorCont++;
																	continue;
																} else {
																	tpath = tnewpath;
																	System.out
																			.println("2011-EasyScan新路径存在文件");
																}
															} else {
																tpath = tnewpath;
																System.out
																		.println("2012-EasyScan新路径存在文件");
															}
														} else {
															tpath = tnewpath;
															System.out
																	.println("2012-2011新路径存在文件");
														}
													} else {
														tpath = tnewpath;
														System.out
																.println("2014-EasyScan新路径存在文件");
													}
												} else {
													tpath = tnewpath;
													System.out
															.println("2014-2011新路径存在文件");
												}
											} else {
												tpath = tnewpath;
												System.out
														.println("2014-2012新路径存在文件");
											}
										} else {
											tpath = tnewpath;
											System.out
													.println("2016-EasyScan新路径存在文件");
										}
									} else {
										tpath = tnewpath;
										System.out.println("2016-2011新路径存在文件");
									}
								} else {
									tpath = tnewpath;
									System.out.println("2016-2012新路径存在文件");
								}
							} else {
								tpath = tnewpath;
								System.out.println("2016-2014新路径存在文件");
							}
						} else {
							System.out.println("未变更路径存在文件");
						}

						tStrUrlOut = tHttpHeadOut + tpath
								+ tES_DOC_PAGESSet.get(m + 1).getPageName()
								+ ".tif";
						tStrUrlIn = tHttpHeadIn + tpath
								+ tES_DOC_PAGESSet.get(m + 1).getPageName()
								+ ".tif";
						mScanFile[piccount - 1][0] = mLCContSchema.getContNo()
								+ "_" + k + "_" + piccount + ".tif";
						mScanFile[piccount - 1][1] = tStrUrlOut;
						mScanFile[piccount - 1][2] = tStrUrlIn;
						mScanFile[piccount - 1][3] = tES_DOC_PAGESSet
								.get(m + 1).getPageName() + ".tif";
						mScanFile[piccount - 1][4] = tES_DOC_MAINDBSet.get(
								j + 1).getSubType();
						mScanFile[piccount - 1][5] = m + 1 + "";
						piccount++;
					}
					if (!errorFlag) {
						if (errorCont == tES_DOC_PAGESSet.size()
								&& tES_DOC_PAGESSet.size() != 1) {
							// 去除字符串最后一个顿号
							buildError(
									"getScanPic",
									"第"
											+ errorInf.substring(0,
													errorInf.length() - 1)
											+ "张扫描件异常，请先保留第一张扫描件，并对其它扫描件进行扫描修改，修改后再对第一张扫描件进行修改。");
							return false;
						} else {
							buildError(
									"getScanPic",
									"第"
											+ errorInf.substring(0,
													errorInf.length() - 1)
											+ "张影印件异常，请对这" + errorCont
											+ "张影印件进行扫描修改！");
							return false;
						}
					}
				}
			}
		}
		System.out.println("ddddfdf");
		if (mScanFile != null) {
			System.out.println("sdsf");
			for (int i = 0; i < mScanFile.length; i++) {
				tXmlDataList.setColValue("FileUrl", mScanFile[i][0]);
				tXmlDataList.setColValue("HttpUrlOut", mScanFile[i][1]);
				tXmlDataList.setColValue("PageIndex", i + 1);
				tXmlDataList.setColValue("HttpUrlIn", mScanFile[i][2]);
				tXmlDataList.setColValue("FileName", mScanFile[i][3]);
				tXmlDataList.insertRow(0);
			}
		}
		cXmlDataset.addDataObject(tXmlDataList);
		// 添加图片标记节点
		if (mScanFile != null && mScanFile.length > 0) {
			cXmlDataset.addDataObject(new XMLDataTag("PicFileFlag", "1"));
		} else {
			cXmlDataset.addDataObject(new XMLDataTag("PicFileFlag", "0"));
		}

		return true;
	}

	/**
	 * 根据个单合同号，查询明细信息
	 * 
	 * @param tXmlDataset
	 *            XMLDataset
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getHospital(XMLDataset tXmlDataset) throws Exception {
		// 查询ldcode表，获取是否需要打印医院信息
		StringBuffer tSBql = new StringBuffer(150);
		tSBql.append("select count(1) from ldcode ld ");
		tSBql.append("where ld.codetype='printriskcode' and ld.codealias='1' ");
		tSBql.append("and ld.code in (select riskcode from LCPol where ContNo='");
		tSBql.append(this.mLCContSchema.getContNo());
		tSBql.append("') ");
		tSBql.append("and ld.code not in (select code from LDCode where codetype='printriskcode1') ");
		tSBql.append("and (ld.code not in (select code from ldcode1 where codetype = 'checkappendrisk') ");
		tSBql.append(" or (select code1 from ldcode1 where codetype = 'checkappendrisk' and code = ld.code) not in (select code from LDCode where codetype='printriskcode1') ) ");

		ExeSQL tExeSQL = new ExeSQL();
		String tCount = tExeSQL.getOneValue(tSBql.toString());
		// 如果查询的结果是0，表示该合同下没有险种需要打印医院信息
		if (tCount.equals("0")) {
			tXmlDataset.addDataObject(new XMLDataTag("LDHospitalFlag", "0"));
			// 如无需定点医院，定点医院说明描述节点该值设置为空（北分）
			// tXmlDataset.addDataObject(new XMLDataTag("LDHospitalRemark",
			// "")); //修改为统一处理
			// --------------------
			return true;
		} else {
			String tTemplateFile = "";
			// 团险默认配置文件，mszTemplatePath为模板所在的应用路径
			tTemplateFile = mTemplatePath + "Hospital.xml";
			// 校验配置文件是否存在
			mFile = new File(tTemplateFile);
			if (!mFile.exists()) {
				buildError("getHospital", "XML配置文件不存在！");
				return false;
			}
			try {
				Hashtable tHshData = new Hashtable();
				// 将变量_ContNo的值赋给xml文件
				tHshData.put("_CONTNO", this.mLCContSchema.getContNo());
				// 根据配置文件生成xml数据
				XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
						tTemplateFile), tHshData);
				tXmlDataset.addDataObject(tXmlDataMine);

				// 添加是否有 Hospital 信息
				String hasHospitalSql = "select count(1) from LDHospital b where ManageCom = '"
						+ mLCContSchema.getManageCom().substring(0, 4)
						+ "' and AssociateClass in ('1','2') And Exists (Select 1 From Ldcomhospital Where Hospitcode = b.Hospitcode And Associatclass In ('指定医院', '推荐医院') And (Startdate <= Current Date Or Startdate Is Null) And (Enddate >= Current Date Or Enddate Is Null))";
				String hasHospital = new ExeSQL().getOneValue(hasHospitalSql);

				if (hasHospital.equals("0")) {
					tXmlDataset.addDataObject(new XMLDataTag("LDHospitalFlag",
							"0"));

					// 如无需定点医院，定点医院说明描述节点该值设置为空（北分）
					// tXmlDataset.addDataObject(new
					// XMLDataTag("LDHospitalRemark", ""));
					// --------------------
				} else {
					tXmlDataset.addDataObject(new XMLDataTag("LDHospitalFlag",
							"1"));

				}
			} catch (Exception e) {
				// 出错处理
				buildError("genHospital", "根据XML文件生成报表数据失败！");
				return false;
			}

			return true;
		}
	}

	private boolean getSpecHospital(XMLDataset tXmlDataset) throws Exception {
		// 查询ldcode表，获取是否需要打印医院信息
		StringBuffer tSBql = new StringBuffer(150);
		tSBql.append("select count(1) from ldcode ld where ld.codetype='printriskcode' and ld.othersign='1' ");
		tSBql.append("and ld.code in (select riskcode from LCPol where ContNo='");
		tSBql.append(this.mLCContSchema.getContNo());
		tSBql.append("')");
		tSBql.append("and code not in (select code from LDCode where codetype='printriskcode1') ");
		tSBql.append("and (ld.code not in (select code from ldcode1 where codetype = 'checkappendrisk') ");
		tSBql.append(" or (select code1 from ldcode1 where codetype = 'checkappendrisk' and code = ld.code) not in (select code from LDCode where codetype='printriskcode1') ) ");

		ExeSQL tExeSQL = new ExeSQL();
		String tCount = tExeSQL.getOneValue(tSBql.toString());
		// 如果查询的结果是0，表示该合同下没有险种需要打印医院信息
		if (tCount.equals("0")) {
			tXmlDataset.addDataObject(new XMLDataTag("SpeLDHospitalFlag", "0"));
			return true;
		} else {
			String tTemplateFile = "";
			// 团险默认配置文件，mszTemplatePath为模板所在的应用路径
			tTemplateFile = mTemplatePath + "SpecHospital.xml";
			// 校验配置文件是否存在
			mFile = new File(tTemplateFile);
			if (!mFile.exists()) {
				buildError("getHospital", "XML配置文件不存在！");
				return false;
			}
			try {
				Hashtable tHshData = new Hashtable();
				// 将变量_ContNo的值赋给xml文件
				tHshData.put("_CONTNO", this.mLCContSchema.getContNo());
				// 根据配置文件生成xml数据
				XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
						tTemplateFile), tHshData);
				tXmlDataset.addDataObject(tXmlDataMine);

				// 添加是否有 SpecHospital 信息
				String hasSpecHospitalSql = "select count(1) from LDHospital b where ManageCom = '"
						+ mLCContSchema.getManageCom().substring(0, 4)
						+ "' and AssociateClass = '1' And Exists (Select 1 From Ldcomhospital Where Hospitcode = b.Hospitcode And Associatclass = '推荐医院' And (Startdate <= Current Date Or Startdate Is Null) And (Enddate >= Current Date Or Enddate Is Null))";
				String hasSpecHospital = new ExeSQL()
						.getOneValue(hasSpecHospitalSql);

				if (hasSpecHospital.equals("0")) {
					tXmlDataset.addDataObject(new XMLDataTag(
							"SpeLDHospitalFlag", "0"));
				} else {
					tXmlDataset.addDataObject(new XMLDataTag(
							"SpeLDHospitalFlag", "1"));
				}
			} catch (Exception e) {
				// 出错处理
				buildError("genHospital", "根据XML文件生成报表数据失败！");
				return false;
			}
			return true;
		}
	}

	/**
	 * 特殊产品，指定医院与推荐医院一起打印 根据个单合同号，查询明细信息
	 * 
	 * @param tXmlDataset
	 *            XMLDataset
	 * @date 20110708
	 * @author gzh
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getSpecRiskHospital(XMLDataset tXmlDataset)
			throws Exception {
		// 查询ldcode表，获取是否需要打印医院信息
		StringBuffer tSBql = new StringBuffer(150);
		tSBql.append("select count(1) from ldcode where codetype='printriskcode' ");
		tSBql.append("and (codealias='1' or othersign='1' ) ");
		tSBql.append("and code in (select code from LDCode where codetype='printriskcode1' ) ");
		tSBql.append("and code in (select riskcode from LCPol where ContNo='");
		tSBql.append(this.mLCContSchema.getContNo());
		tSBql.append("')");

		ExeSQL tExeSQL = new ExeSQL();
		String tCount = tExeSQL.getOneValue(tSBql.toString());
		// 如果查询的结果是0，表示该合同下没有险种需要打印医院信息
		if (tCount.equals("0")) {
			tXmlDataset.addDataObject(new XMLDataTag("SpeRiskLDHospital", "0"));
			return true;
		} else {
			String tTemplateFile = "";
			// 团险默认配置文件，mszTemplatePath为模板所在的应用路径
			tTemplateFile = mTemplatePath + "SpecRiskHospital.xml";
			// 校验配置文件是否存在
			mFile = new File(tTemplateFile);
			if (!mFile.exists()) {
				buildError("getHospital", "XML配置文件不存在！");
				return false;
			}
			try {
				Hashtable tHshData = new Hashtable();
				// 将变量_ContNo的值赋给xml文件
				tHshData.put("_CONTNO", this.mLCContSchema.getContNo());
				// 根据配置文件生成xml数据
				XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
						tTemplateFile), tHshData);
				tXmlDataset.addDataObject(tXmlDataMine);

				// 添加是否有 SpecHospital 信息
				String hasSpecHospitalSql = "select count(1) from LDHospital b where ManageCom = '"
						+ mLCContSchema.getManageCom().substring(0, 4)
						+ "' and AssociateClass in ('1','2') And Exists (Select 1 From Ldcomhospital Where Hospitcode = b.Hospitcode And Associatclass In ('指定医院', '推荐医院') And (Startdate <= Current Date Or Startdate Is Null) And (Enddate >= Current Date Or Enddate Is Null))";
				String hasSpecHospital = new ExeSQL()
						.getOneValue(hasSpecHospitalSql);

				if (hasSpecHospital.equals("0")) {
					tXmlDataset.addDataObject(new XMLDataTag(
							"SpeRiskLDHospital", "0"));
				} else {
					StringBuffer showNameSql = new StringBuffer(150);
					showNameSql
							.append("select codename from ldcode where codetype='printriskcode1' ");
					showNameSql
							.append("and code in (select riskcode from LCPol where ContNo='");
					showNameSql.append(this.mLCContSchema.getContNo());
					showNameSql.append("')");
					SSRS showNameSSRS = tExeSQL.execSQL(showNameSql.toString());
					String showName = "";
					for (int nameIndex = 1; nameIndex <= showNameSSRS.MaxRow; nameIndex++) {
						if (nameIndex < showNameSSRS.MaxRow) {
							showName += showNameSSRS.GetText(nameIndex, 1)
									+ "|";
						} else {
							showName += showNameSSRS.GetText(nameIndex, 1);
						}
					}
					tXmlDataset.addDataObject(new XMLDataTag(
							"SpeRiskLDHospital", "1"));
					tXmlDataset.addDataObject(new XMLDataTag(
							"SpeRiskLDHospitalTitle", showName + "产品指定医疗机构"));
				}
			} catch (Exception e) {
				// 出错处理
				buildError("genHospital", "根据XML文件生成报表数据失败！");
				return false;
			}
			return true;
		}
	}

	/**
	 * 根据个单合同号，查询明细信息
	 * 
	 * @param xmlDataset
	 *            XMLDataset
	 * @return boolean
	 * @throws Exception
	 */
	private boolean genInsuredList(XMLDataset xmlDataset) throws Exception {
		String szTemplateFile = "";
		// 团险默认配置文件，mszTemplatePath为模板所在的应用路径
		if ("2".equals(mLCContSchema.getCardFlag())) {
			if ("F".equals(mLCContSchema.getFamilyType())) {
				szTemplateFile = mTemplatePath + "FamilyContPrint.xml";

			} else {

				szTemplateFile = mTemplatePath + "AccContPrint.xml";
			}
		} else {
			szTemplateFile = mTemplatePath + "ContPrint.xml";
		}

		// 校验配置文件是否存在
		mFile = new File(szTemplateFile);
		if (!mFile.exists()) {
			buildError("genInsuredList", "XML配置文件不存在！");
			return false;
		}
		try {
			Hashtable hashData = new Hashtable();
			// 将变量_ContNo的值赋给xml文件
			hashData.put("_CONTNO", this.mLCContSchema.getContNo());
			// 根据配置文件生成xml数据
			XMLDataMine xmlDataMine = new XMLDataMine(new FileInputStream(
					szTemplateFile), hashData);
			xmlDataset.addDataObject(xmlDataMine);
		} catch (Exception e) {
			// 出错处理
			e.printStackTrace();
			buildError("genInsuredList", "根据XML文件生成报表数据失败！");
			return false;
		}
		return true;
	}

	/**
	 * 获取发票信息
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getInvoice(XMLDataset cXmlDataset) throws Exception {
		String szTemplateFile = "";
		// 团险默认配置文件
		szTemplateFile = mTemplatePath + "ContInvoice.xml";
		// 校验配置文件是否存在
		mFile = new File(szTemplateFile);
		if (!mFile.exists()) {
			buildError("getInvoice", "XML配置文件不存在！");
			return false;
		}
		try {
			XMLDataList tXMLDataList = new XMLDataList();
			tXMLDataList.setDataObjectID("Money");
			tXMLDataList.addColHead("ChinaMoney");
			tXMLDataList.buildColHead();

			tXMLDataList.setColValue("ChinaMoney",
					PubFun.getChnMoney(this.mLCContSchema.getPrem()));
			tXMLDataList.insertRow(0);

			cXmlDataset.addDataObject(tXMLDataList);

			Hashtable hashData = new Hashtable();
			// 将变量ContNo的值赋给xml文件
			hashData.put("_CONTNO", this.mLCContSchema.getContNo());
			// 根据配置文件生成xml数据
			XMLDataMine xmlDataMine = new XMLDataMine(new FileInputStream(
					szTemplateFile), hashData);
			cXmlDataset.addDataObject(xmlDataMine);

			// cXmlDataset.addDataObject(new XMLDataTag("ChinaMoney",
			// PubFun.getChnMoney(); //保费大写
			// 查询缴费方式信息
			// 根据合同号，查询缴费记录信息
			StringBuffer tSBql = new StringBuffer(128);
			tSBql.append("select distinct TempFeeNo from LJTempFee where OtherNo = '");
			tSBql.append(this.mLCContSchema.getContNo());
			tSBql.append("' and OtherNoType = '2'");

			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());

			if (tSSRS.MaxNumber == 0) {
				// mCheckNo=1;
				String tSBql2 = "select distinct TempFeeNo from LJTempFee where OtherNo in "
						+ "(select contno from lccont where prtno='"
						+ mPrtno
						+ "' and appflag='1') and OtherNoType = '2'";
				tSSRS = tExeSQL.execSQL(tSBql2);
			}

			StringBuffer tBWhere = new StringBuffer(128);
			tBWhere.append(" where a.TempFeeNo in ('");

			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				// 将每笔缴费的缴费号串连起来作为查询条件
				if (i == tSSRS.getMaxRow()) {
					tBWhere.append(tSSRS.GetText(i, 1));
					tBWhere.append("')");
				} else {
					tBWhere.append(tSSRS.GetText(i, 1));
					tBWhere.append("','");
				}
			}
			// cXmlDataset.addDataObject(new XMLDataTag("TempFeeNo",
			// tSSRS.GetText(1, 1))); //收据号

			// 根据上面的查询条件，查询缴费方式为现金的数据
			tSBql = new StringBuffer(128);
			tSBql.append("select a.ConfMakeDate from LJTempFeeClass a");
			tSBql.append(tBWhere);
			tSBql.append(" and a.PayMode = '1' order by a.ConfMakeDate");
			if (StrTool.cTrim(this.mLCContSchema.getAppFlag()).equals("9")) {
				return true;
			}
			tSSRS = tExeSQL.execSQL(tSBql.toString());
			if (tSSRS.getMaxRow() > 0) {
				// 添加缴费方式为现金的标签
				tXMLDataList = new XMLDataList();
				// 添加xml一个新的对象Term
				tXMLDataList.setDataObjectID("Cash");
				// 添加Head单元内的信息
				tXMLDataList.addColHead("PayMode");
				tXMLDataList.addColHead("ConfMakeDate");
				tXMLDataList.buildColHead();
				// 此缴费方式只需知道确认日期
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					tXMLDataList.setColValue("PayMode", "现金");
					tXMLDataList.setColValue("ConfMakeDate",
							tSSRS.GetText(i, 1));
					tXMLDataList.insertRow(0);
				}
				cXmlDataset.addDataObject(tXMLDataList);
			}

			// 根据上面的查询条件，查询缴费方式不是现金的数据
			tSBql = new StringBuffer(256);
			tSBql.append("select b.BankName,a.BankAccNo,a.AccName,a.ConfMakeDate from LJTempFeeClass a, LDBank b");
			tSBql.append(tBWhere);
			tSBql.append(" and a.PayMode <> '1' and a.BankCode = b.BankCode order by a.BankAccNo,a.ConfMakeDate");

			tSSRS = tExeSQL.execSQL(tSBql.toString());
			if (tSSRS.getMaxRow() > 0) {
				// 添加缴费方式为支票的标签
				tXMLDataList = new XMLDataList();
				// 添加xml一个新的对象Term
				tXMLDataList.setDataObjectID("Check");
				// 添加Head单元内的信息
				tXMLDataList.addColHead("PayMode");
				tXMLDataList.addColHead("Bank");
				tXMLDataList.addColHead("AccName");
				tXMLDataList.addColHead("BankAccNo");
				tXMLDataList.addColHead("ConfMakeDate");
				tXMLDataList.buildColHead();
				// 此标签需要获得银行、户名、帐户的信息
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					tXMLDataList.setColValue("PayMode", "银行转帐");
					tXMLDataList.setColValue("Bank", tSSRS.GetText(i, 1));
					tXMLDataList.setColValue("AccName", tSSRS.GetText(i, 2));
					tXMLDataList.setColValue("BankAccNo", tSSRS.GetText(i, 3));
					tXMLDataList.setColValue("ConfMakeDate",
							tSSRS.GetText(i, 4));
					tXMLDataList.insertRow(0);
				}
				cXmlDataset.addDataObject(tXMLDataList);
			}
			// 添加xml结束标签
			tXMLDataList = new XMLDataList();
			tXMLDataList.setDataObjectID("End");
			tXMLDataList.addColHead("Flag");
			tXMLDataList.buildColHead();

			tXMLDataList.setColValue("Flag", "1");
			tXMLDataList.insertRow(0);

			cXmlDataset.addDataObject(tXMLDataList);
		} catch (Exception e) {
			buildError("getInvoice", "根据XML文件生成报表数据失败！");
			return false;
		}
		return true;
	}

	/**
	 * 取得个单下的全部LCPol表数据
	 * 
	 * @param cContNo
	 *            String
	 * @return LCPolSet
	 * @throws Exception
	 */
	private static LCPolSet getRiskList(String cContNo) throws Exception {
		// 查询返回容器
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolSet = tLCPolDB
				.executeQuery("select a.* from LCPol a,LCInsured b where a.ContNo=b.ContNo and a.InsuredNo=b.InsuredNo and a.ContNo ='"
						+ cContNo
						+ "' order by int(b.SequenceNo),int(a.RiskSeqNo) with ur ");
		// 返回个单险种集合
		return tLCPolSet;
	}

	/**
	 * 获取补打保单的数据
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getRePrintData() throws Exception {
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = null;

		// 查询LCPolPrint表，获取要补打的保单数据
		String tSql = "SELECT PrtTimes + 1 FROM LCPolPrint WHERE MainPolNo = '"
				+ mLCContSchema.getContNo() + "'";
		System.out.println(tSql);
		tSSRS = tExeSQL.execSQL(tSql);
		// 查询出错返回，一般是大异常导致
		if (tExeSQL.mErrors.needDealError()) {
			mErrors.copyAllErrors(tExeSQL.mErrors);
			throw new Exception("获取打印数据失败");
		}
		// 查询结果为空的处理，表示没有查询到以前的打印数据
		if (tSSRS.MaxRow < 1) {
			throw new Exception("找不到原来的打印数据，可能传入的不是主险保单号！");
		}
		mResult.clear();
		mResult.add(mLCContSchema);
		// 暂时不放在一个事务里面，因为基本上读入流后很少会发生错误
		LCContF1PBLS tLCContF1PBLS = new LCContF1PBLS();
		if (!tLCContF1PBLS.submitData(mResult, "REPRINT")) {
			if (tLCContF1PBLS.mErrors.needDealError()) {
				mErrors.copyAllErrors(tLCContF1PBLS.mErrors);
			}
			throw new Exception("保存数据失败");
		}

		// 取打印数据
		Connection conn = null;

		try {
			DOMBuilder tDOMBuilder = new DOMBuilder();
			Element tRootElement = new Element("DATASETS");

			conn = DBConnPool.getConnection();

			if (conn == null) {
				throw new Exception("连接数据库失败！");
			}

			Blob tBlob = null;
			// CBlob tCBlob = new CBlob();
			// 拼写查询条件
			tSql = " and MainPolNo = '" + mLCContSchema.getContNo() + "'";
			// 根据查询条件获取原始打印数据
			tBlob = CBlob.SelectBlob("LCPolPrint", "PolInfo", tSql, conn);
			// 如果数据为空
			if (tBlob == null) {
				throw new Exception("找不到打印数据");
			}
			Element tElement = tDOMBuilder.build(tBlob.getBinaryStream())
					.getRootElement();
			tElement = new Element("DATASET").setMixedContent(tElement
					.getMixedContent());
			tRootElement.addContent(tElement);

			ByteArrayOutputStream tByteArrayOutputStream = new ByteArrayOutputStream();
			XMLOutputter tXMLOutputter = new XMLOutputter("  ", true, "UTF-8");
			tXMLOutputter.output(tRootElement, tByteArrayOutputStream);

			// mResult.clear();
			// mResult.add(new ByteArrayInputStream(tByteArrayOutputStream.
			// toByteArray()));
			ByteArrayInputStream tByteArrayInputStream = new ByteArrayInputStream(
					tByteArrayOutputStream.toByteArray());

			// 仅仅重新生成xml打印数据，影印件信息暂时不重新获取
			String FilePath = mOutXmlPath + "/printdata";
			mFile = new File(FilePath);
			if (!mFile.exists()) {
				mFile.mkdir();
			}
			// 根据合同号生成打印数据存放文件夹
			FilePath = mOutXmlPath + "/printdata" + "/"
					+ mLCContSchema.getContNo();
			mFile = new File(FilePath);
			if (!mFile.exists()) {
				mFile.mkdir();
			}

			// 根据团单合同号生成文件
			String XmlFile = FilePath + "/" + mLCContSchema.getContNo()
					+ ".xml";
			FileOutputStream fos = new FileOutputStream(XmlFile);
			// 此方法写入数据准确，但是相对效率比较低下
			int n = 0;
			while ((n = tByteArrayInputStream.read()) != -1) {
				fos.write(n);
			}
			fos.close();

			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				// do nothing
			}

			throw ex;
		}
		return true;
	}

	/**
	 * 格式化浮点型数据
	 * 
	 * @param dValue
	 *            double
	 * @return String
	 */
	private static String format(double dValue) {
		return new DecimalFormat("0.00").format(dValue);
	}

	private boolean checkdate() throws Exception {
		LCContSchema tLCContSchema = this.mLCContSchema;
		LCPolSet tLCPolSet = getRiskList(tLCContSchema.getContNo());
		LCContDB tLCContDB = new LCContDB();
		LCContSet tLCContSet = new LCContSet();
		tLCContDB.setContNo(tLCContSchema.getContNo());
		tLCContSet = tLCContDB.query();
		LCAppntDB tLCAppntDB = new LCAppntDB();
		LCAppntSet tLCAppntSet = new LCAppntSet();
		tLCAppntDB.setContNo(this.mLCContSchema.getContNo());
		tLCAppntSet = tLCAppntDB.query();
		LDComDB tLDComDB = new LDComDB();
		LDComSet tLDComSet = new LDComSet();
		System.out.println(tLCContSet.get(1).getManageCom());
		tLDComDB.setComCode(tLCContSet.get(1).getManageCom());
		tLDComSet = tLDComDB.query();
		LAAgentDB tLAAgentDB = new LAAgentDB();
		LAAgentSet tLAAgentSet = new LAAgentSet();
		tLAAgentDB.setAgentCode(tLCContSet.get(1).getAgentCode());
		tLAAgentSet = tLAAgentDB.query();
		SSRS tSSRS2 = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		double prem = 0;
		// System.out.println(this.mLCContSchema.getContNo());
		if (StrTool.cTrim(tLDComSet.get(1).getName()).equals("")) {
			buildError("checkdate", "取分公司名称失败！");
			return false;
		}
		if (StrTool.cTrim(tLDComSet.get(1).getClaimReportPhone()).equals("")) {
			buildError("checkdate", "取理赔报案电话失败！");
			return false;
		}
		if (!"1".equals(PrintType)) {
			tSSRS2 = tExeSQL
					.execSQL("SELECT count(1) FROM LCCont  where ContType = '1' and  contno='"
							+ this.mLCContSchema.getContNo()
							+ "' AND AppFlag in ( '1','9') and uwflag not in ('a','1','8') AND  PrintCount=1  ");
			System.out.println("tSSRS2.GetText(1,1)" + tSSRS2.GetText(1, 1));
			if (!StrTool.cTrim(tSSRS2.GetText(1, 1)).equals("0")
					&& !"5".equals(tLCContSet.get(1).getCardFlag())
					&& !"6".equals(tLCContSet.get(1).getCardFlag())
					&& !"8".equals(tLCContSet.get(1).getCardFlag())
					&& !"9".equals(tLCContSet.get(1).getCardFlag())
					&& !"c".equals(tLCContSet.get(1).getCardFlag())
					&& !"d".equals(tLCContSet.get(1).getCardFlag())) // 万能险和电话销售不需要本校验
			{
				buildError("checkdate", "该保单已经进行过打印!");
				return false;
			}
		}
		if (StrTool.cTrim(tLDComSet.get(1).getLetterServiceName()).equals("")) {
			buildError("checkdate", "取信函服务名称失败！");
			return false;
		}

		if (StrTool.cTrim(tLDComSet.get(1).getLetterServicePostAddress())
				.equals("")) {
			buildError("checkdate", "取地址信函服务地址失败！");
			return false;
		}

		if (StrTool.cTrim(tLDComSet.get(1).getLetterServicePostZipcode())
				.equals("")) {
			buildError("checkdate", "取地址信函服务邮编失败！");
			return false;
		}

		if (StrTool.cTrim(tLDComSet.get(1).getServicePostAddress()).equals("")) {
			buildError("checkdate", "取客户服务中心地址失败！");
			return false;
		}

		if (StrTool.cTrim(tLDComSet.get(1).getServicePostZipcode()).equals("")) {
			buildError("checkdate", "取客户服务中心邮编失败！");
			return false;
		}
		if (StrTool.cTrim(tLAAgentSet.get(1).getName()).equals("")) {
			buildError("checkdate", "取业务员名称失败！");
			return false;
		}
		if (StrTool.cTrim(tLCContSet.get(1).getAppntName()).equals("")
				|| StrTool.cTrim(tLCAppntSet.get(1).getAppntName()).equals("")
				|| !(tLCContSet.get(1).getAppntName().trim().equals(tLCAppntSet
						.get(1).getAppntName().trim()))) {
			buildError("checkdate", "取投保人名称失败！");
			return false;
		}

		if (StrTool.cTrim(tLCContSet.get(1).getAppntNo()).equals("")
				|| StrTool.cTrim(tLCAppntSet.get(1).getAppntNo()).equals("")
				|| !(tLCContSet.get(1).getAppntNo().trim().equals(tLCAppntSet
						.get(1).getAppntNo().trim()))) {
			buildError("checkdate", "取投保人代码失败！");
			return false;
		}
		String tSql = "SELECT * FROM LCPol WHERE ContNo = '"
				+ mLCContSchema.getContNo() + "' and uwflag<>'a'";
		System.out.println(this.mLCContSchema.getContNo());
		LCPolSet mLCPolSet = new LCPolSet();
		LCPolDB mLCPolDB = new LCPolDB();
		mLCPolSet = mLCPolDB.executeQuery(tSql);
		for (int i = 0; i < mLCPolSet.size(); i++) {
			if (StrTool.cTrim(tLCContSet.get(1).getCValiDate()).equals("")
					|| StrTool.cTrim(mLCPolSet.get(i + 1).getCValiDate())
							.equals("")
					|| !(tLCContSet.get(1).getCValiDate().trim()
							.equals(mLCPolSet.get(i + 1).getCValiDate().trim()))) {
				buildError("checkdate", "取生效日期失败！");
				return false;
			}
			/*
			 * if(!(tLCContSet.get(1).getPayIntv()==mLCPolSet.get(i+1).getPayIntv
			 * ())){ buildError("checkdate", "取交费间隔失败！"); return false; }
			 */
			if (StrTool.cTrim(tLCContSet.get(1).getPayMode()).equals("")
					|| StrTool.cTrim(mLCPolSet.get(i + 1).getPayMode()).equals(
							"")
					|| !(tLCContSet.get(1).getPayMode().trim().equals(mLCPolSet
							.get(i + 1).getPayMode().trim()))) {
				buildError("checkdate", "取交费方式失败！");
				return false;
			}
			if (!(mLCPolSet.get(i + 1).getPrem() == mLCPolSet.get(i + 1)
					.getSumPrem())) {
				buildError("checkdate", "取险种保费金额失败！");
				return false;
			}
			prem = prem + mLCPolSet.get(i + 1).getPrem();
		}
		System.out.println("++" + prem);
		if (StrTool.cTrim(tLCContSet.get(1).getAppFlag()).equals("1")) {
			if (!(tLCContSet.get(1).getPrem() == PubFun.setPrecision(prem,
					"0.00"))
					|| !(tLCContSet.get(1).getPrem() == tLCContSet.get(1)
							.getSumPrem())) {
				buildError("checkdate", "取保费金额失败！");
				return false;
			}
		}

		/*
		 * if(StrTool.cTrim(tLCContSet.get(1).getContPremFeeNo()).equals("")){
		 * buildError("checkdate", "取收费记录号失败！"); return false; }
		 */
		if (StrTool.cTrim(tLCContSet.get(1).getCustomerReceiptNo()).equals("")) {
			buildError("checkdate", "取合同送达回执号失败！");
			return false;
		}
		if (StrTool.cTrim(tLCContSet.get(1).getPayMode()).equals("4")) {
			if (StrTool.cTrim(tLCContSet.get(1).getAccName()).equals("")) {
				buildError("checkdate", "取银行帐户名失败！");
				return false;
			}

			if (StrTool.cTrim(tLCContSet.get(1).getBankAccNo()).equals("")) {
				buildError("checkdate", "取银行帐号失败！");
				return false;
			}

			if (StrTool.cTrim(tLCContSet.get(1).getBankCode()).equals("")) {
				buildError("checkdate", "取银行代码失败！");
				return false;
			}

		}

		return true;
	}

	private boolean DealBnf(XMLDataset xmlDataset) throws Exception {
		VData tVData = new VData();
		TransferData mTransferData = new TransferData();
		System.out.println("ContNo" + mLCContSchema.getContNo());
		mTransferData.setNameAndValue("ContNo", mLCContSchema.getContNo());
		tVData.add(mTransferData);
		ModifyBnfInfoForPrintError modifybnfinfoforprinterror = new ModifyBnfInfoForPrintError();
		if (!modifybnfinfoforprinterror.submitData(tVData, null)) {
			CError.buildErr(this, "险种特约关联信息插入失败！");
			return false;
		}

		/**
		 * 处理多受益人信息
		 */
		System.out.println("开始处理受益人信息...");
		String strSql = "select distinct Name, "
				+ " sex, "
				+ " CodeName('idtype', idtype) as idtype, birthday, idno, bnfno, "
				+ " bnfgrade, bnflot, "
				+ " CodeName('relation', relationtoinsured) as relationtoinsured "
				+ " from lcbnf " + " where contno = '"
				+ mLCContSchema.getContNo() + "' and bnftype = '1' "
				+ " order by bnfgrade, bnfno " + " with ur ";

		XMLDataList xmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		xmlDataList.setDataObjectID("LCBnf");
		// 添加Head单元内的信息
		xmlDataList.addColHead("Name");
		xmlDataList.addColHead("Sex");
		xmlDataList.addColHead("Birthday");
		xmlDataList.addColHead("IDNo");
		xmlDataList.addColHead("BnfGrade");
		xmlDataList.addColHead("BnfLot");
		xmlDataList.buildColHead();

		System.out.println(strSql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS ssrs = tExeSQL.execSQL(strSql);
		if (tExeSQL.mErrors.needDealError()) {
			String str = " 查询受益人信息失败原因是：" + tExeSQL.mErrors.getFirstError();
			buildError("dealBnf", str);
			System.out.println("Error : LCContPrintPdfBL -> dealBnf()");
			return false;
		}
		if (ssrs != null && ssrs.getMaxRow() > 0) {
			for (int i = 1; i <= ssrs.getMaxRow(); i++) {
				xmlDataList.setColValue("Name", ssrs.GetText(i, 1));
				xmlDataList.setColValue("Sex", ssrs.GetText(i, 2));
				xmlDataList.setColValue("IdType", ssrs.GetText(i, 3));
				xmlDataList.setColValue("Birthday", ssrs.GetText(i, 4));
				xmlDataList.setColValue("IDNo", ssrs.GetText(i, 5));
				xmlDataList.setColValue("BnfNo", ssrs.GetText(i, 6));
				xmlDataList.setColValue("BnfGrade", ssrs.GetText(i, 7));
				xmlDataList.setColValue("BnfLot", ssrs.GetText(i, 8));
				xmlDataList
						.setColValue("RelationToInsured", ssrs.GetText(i, 9));
				xmlDataList.insertRow(0);
			}
			xmlDataset.addDataObject(new XMLDataTag("IsBnfFlag", "1"));
		} else {
			xmlDataset.addDataObject(new XMLDataTag("IsBnfFlag", "0"));
		}
		xmlDataset.addDataObject(xmlDataList);
		return true;
	}

	private boolean getMinor(XMLDataset cXmlDataset) {
		boolean result = true;
		String tContNo = mLCContSchema.getContNo();
		String tSql = " select lcp.cvalidate, lci.birthday "
				+ " from lcinsured lci "
				+ " inner join lcpol lcp on lci.contno = lcp.contno "
				+ " where lcp.contno = '" + tContNo + "' " + " with ur ";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tSql);
		boolean bExists = false;

		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String tCvalidate = tSSRS.GetText(i, 1);
			String tBirthday = tSSRS.GetText(i, 2);

			if ("".equals(tCvalidate) || "".equals(tBirthday))
				return false;

			int tAge = PubFun.getInsuredAppAge(tCvalidate, tBirthday);
			if (tAge < 18) {
				bExists = true;
				break;
			}
		}
		cXmlDataset.addDataObject(new XMLDataTag("ExistsMinor", String
				.valueOf(bExists)));
		System.out.println("被保人中是否存在未成年人：" + bExists);

		return result;
	}

	/**
	 * 获取险种套餐名称，注：该节点只用于个险万能。
	 * 
	 * @param cContNo
	 * @return
	 */
	private String getWrapName(String cContNo, boolean SYFlag) {
		String tStrSql = "";
		String specialRiskcodesql = " select riskcode from lcpol where contno='"
				+ cContNo
				+ "' and riskcode in (select code from ldcode where codetype='tsxz' ) fetch first 1 rows only";
		String specialRiskcode = new ExeSQL().getOneValue(specialRiskcodesql);
		if (mLCContSchema.getCardFlag().equals("2")) {
			tStrSql = " select wrapname from ldwrap "
					+ "where exists (select 1 from lcriskdutywrap "
					+ "where riskwrapcode = ldwrap.riskwrapcode and prtno='"
					+ mLCContSchema.getPrtNo() + "') ";
		} else {
			if (SYFlag) {
				tStrSql = " select lmr.riskName from LCPol lcp,lmriskapp lmr "
						+ " where 1 = 1 and lcp.ContNo = '"
						+ cContNo
						+ "' and lcp.riskcode=lmr.riskcode and lmr.TaxOptimal='Y' fetch first 1 rows only ";
			} else if (specialRiskcode != "") {
				tStrSql = " select riskName from lmriskapp  "
						+ " where  riskcode='" + specialRiskcode
						+ "'  fetch first 1 rows only ";
			} else {
				tStrSql = " select ldc1.RiskWrapPlanName from LCPol lcp "
						+ " inner join LDCode1 ldc1 on ldc1.Code1 = lcp.RiskCode and ldc1.CodeType = 'checkappendrisk' "
						+ " where 1 = 1 " + " and lcp.ContNo = '" + cContNo
						+ "' " + " fetch first 1 rows only ";
			}

		}
		return new ExeSQL().getOneValue(tStrSql);
	}

	private String getWrapCode(String cPrtNo) {
		String tStrSql = " select distinct riskwrapcode from lcriskdutywrap "
				+ "where prtno='" + cPrtNo + "' ";
		return new ExeSQL().getOneValue(tStrSql);
	}

	/**
	 * 获取险种套餐名称，注：该节点只用于个险万能。
	 * 
	 * @param cContNo
	 *            保单号
	 * @param cRiskCode
	 *            主险名称
	 * @return
	 */
	private String getWrapNameByRisk(String cContNo, String cRiskCode) {
		String tStrSql = " select ldc1.RiskWrapPlanName from LCPol lcp "
				+ " inner join LDCode1 ldc1 on ldc1.Code1 = lcp.RiskCode and ldc1.CodeType = 'checkappendrisk' "
				+ " where 1 = 1 " + " and lcp.ContNo = '" + cContNo + "' "
				+ "  and ldc1.code1 = '" + cRiskCode
				+ "'  fetch first 1 rows only ";
		return new ExeSQL().getOneValue(tStrSql);
	}

	/**
	 * 获取定点医院说明信息，因增加特殊产品打印，所以需将定点医院说明单独处理（以前是在指定医院中处理的）
	 * */
	private boolean getHospitalRemark(XMLDataset tXmlDataset) {
		// 如存在定点医院，增加定点医院说明描述（北分）
		String tInputDate = mLCContSchema.getInputDate();
		String tRiskName = "";
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(mLCContSchema.getContNo());
		tLCPolSet = tLCPolDB.query();
		if (tLCPolSet != null && tLCPolSet.size() > 0) {
			HashSet RiskKeys = new HashSet();
			for (int i = 1; i <= tLCPolSet.size(); i++) {
				String tRiskcode = tLCPolSet.get(i).getRiskCode();
				if (!RiskKeys.contains(tRiskcode)) {
					String tRiskNamSql = "select RiskName from lmriskapp where riskcode = '"
							+ tLCPolSet.get(i).getRiskCode()
							+ "' "
							+ " and exists (select 1 from lddinghospital ldhpt where ldhpt.ManageCom = '"
							+ mLCContSchema.getManageCom().substring(0, 4)
							+ "' and '"
							+ tInputDate
							+ "' between ldhpt.ValiDate and ldhpt.CInValiDate"
							+ " and ldhpt.RiskCode = '"
							+ tLCPolSet.get(i).getRiskCode() + "') ";

					String tResult = new ExeSQL().getOneValue(tRiskNamSql);

					if (tResult != null && !"".equals(tResult)) {
						tRiskName = tRiskName + "《" + tResult + "》";
					}
					RiskKeys.add(tRiskcode);
				}
			}
		}

		String tLDHospitalRemarkSql = "select '带※字符的医院不是' || '"
				+ tRiskName
				+ "' || '的指定及推荐医院。' from dual where '"
				+ mLCContSchema.getManageCom().substring(0, 4)
				+ "' = '8611' "
				+ " and exists (select 1 from lddinghospital ldhpt where ldhpt.ManageCom = '"
				+ mLCContSchema.getManageCom().substring(0, 4)
				+ "' and '"
				+ tInputDate
				+ "' between ldhpt.ValiDate and ldhpt.CInValiDate"
				+ " and ldhpt.RiskCode in (select RiskCode from lcpol where ContNo = '"
				+ mLCContSchema.getContNo() + "')" + ")";

		String tLDHospitalRemark = new ExeSQL()
				.getOneValue(tLDHospitalRemarkSql);
		if (tLDHospitalRemark != null && !"".equals(tLDHospitalRemark)) {
			tXmlDataset.addDataObject(new XMLDataTag("LDHospitalRemark",
					tLDHospitalRemark));
		} else {
			tXmlDataset.addDataObject(new XMLDataTag("LDHospitalRemark", ""));
		}

		return true;
	}

	/**
	 * 适用条款信息查询
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getTermSpecDoc(XMLDataset cXmlDataset, LCPolSet cLCPolSet)
			throws Exception {
		XMLDataList xmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		xmlDataList.setDataObjectID("LCTermSpec");
		// 添加Head单元内的信息
		xmlDataList.addColHead("RowID");
		xmlDataList.addColHead("SpecContent");

		String SpecID = "";
		String Content = "";
		int LCTermSpecFlag = 0;// 使用条款标记
		String mainpol = "";
		// 是否是套餐绑定销售
		if ("2".equals(mLCContSchema.getCardFlag())
				&& !"F".equals(mLCContSchema.getFamilyType())) {
			mainpol = " select distinct lcr.riskwrapcode,(select Wrapname from ldwrap where riskwrapcode=lcr.riskwrapcode) "
					+ " from lcriskdutywrap lcr where contno='"
					+ this.mLCContSchema.getContNo() + "' ";
			SSRS tpolSSRS = new ExeSQL().execSQL(mainpol);
			if (tpolSSRS.getMaxRow() > 0) {
				for (int i = 1; i <= tpolSSRS.getMaxRow(); i++) {
					Content = "";
					String sql = " select distinct lcp.riskcode,(select riskname from lmriskapp where riskcode=lcp.riskcode),lcp.riskseqno "
							+ " from lcpol lcp ,lcriskdutywrap lcr "
							+ " where lcp.contno=lcr.contno "
							+ " and lcp.contno='"
							+ this.mLCContSchema.getContNo()
							+ "' "
							+ " and lcr.riskwrapcode ='"
							+ tpolSSRS.GetText(i, 1)
							+ "'"
							+ " and lcp.insuredno=lcr.insuredno "
							+ " and lcp.riskcode=lcr.riskcode "
							+ " order by lcp.riskseqno ";
					ExeSQL tExeSQL = new ExeSQL();
					SSRS tSSRS = tExeSQL.execSQL(sql);
					for (int j = 1; j <= tSSRS.getMaxRow(); j++, LCTermSpecFlag++) {
						if (j == 1) {
							Content = "“" + tpolSSRS.GetText(i, 2) + "（"
									+ tpolSSRS.GetText(i, 1) + "）”适用《"
									+ tSSRS.GetText(j, 2) + "条款》";
						} else {
							Content += "、《" + tSSRS.GetText(j, 2) + "条款》";
						}

					}
					SpecID = "适用条款说明" + i;
					Content += "。";
					xmlDataList.setColValue("RowID", SpecID);
					xmlDataList.setColValue("SpecContent", Content);
					xmlDataList.insertRow(0);
				}
			}
		} else if ("F".equals(mLCContSchema.getFamilyType())) {

		} else {
			// 查询主险信息
			mainpol = "select lcp.riskcode,min(lci.sequenceno),min(lcp.riskseqno) "
					+ " from lcpol lcp,lcinsured lci "
					+ " where lcp.contno='"
					+ this.mLCContSchema.getContNo()
					+ "' "
					+ " and lcp.contno=lci.contno "
					+ " and lcp.insuredno=lci.insuredno "
					+ " and exists (select 1 from ldcode1 where codetype='checkappendrisk' and code1=lcp.riskcode) "
					+ " group by lcp.riskcode "
					+ " order by min(lci.sequenceno),min(lcp.riskseqno) ";

			SSRS tpolSSRS = new ExeSQL().execSQL(mainpol);
			if (tpolSSRS.getMaxRow() > 0) {
				for (int i = 1; i <= tpolSSRS.getMaxRow(); i++) {

					Content = "";
					String sql = "select trim(code1),trim(codealias),trim(riskwrapplanname) from ldcode1 where codetype='checkappendrisk' and code1='"
							+ tpolSSRS.GetText(i, 1)
							+ "' union select trim(code),trim(codename),trim(riskwrapplanname) from ldcode1 where codetype='checkappendrisk' and code1='"
							+ tpolSSRS.GetText(i, 1) + "' ";
					ExeSQL tExeSQL = new ExeSQL();
					SSRS tSSRS = tExeSQL.execSQL(sql);

					for (int j = 1; j <= tSSRS.getMaxRow(); j++, LCTermSpecFlag++) {
						if (j == 1) {
							Content = "“" + tSSRS.GetText(j, 3) + "（"
									+ tSSRS.GetText(j, 1) + "）”适用《"
									+ tSSRS.GetText(j, 2) + "条款》";
						} else {
							Content += "、《" + tSSRS.GetText(j, 2) + "条款》";
						}

					}
					SpecID = "适用条款说明" + i;
					Content += "。";
					xmlDataList.setColValue("RowID", SpecID);
					xmlDataList.setColValue("SpecContent", Content);
					xmlDataList.insertRow(0);
				}
			}
		}
		cXmlDataset.addDataObject(xmlDataList);

		// 增加特约信息标记节点
		if (LCTermSpecFlag > 0) {
			cXmlDataset.addDataObject(new XMLDataTag("LCTermSpecFlag", "1"));
		} else {
			cXmlDataset.addDataObject(new XMLDataTag("LCTermSpecFlag", "0"));
		}

		if ("J018".equals(mJetFormType)) {
			if (LCTermSpecFlag < 1 && "".equals(mRemarkFlag)) {
				cXmlDataset.addDataObject(new XMLDataTag("remarkflag", "0"));
			} else {
				cXmlDataset.addDataObject(new XMLDataTag("remarkflag", "1"));
			}
		}

		if ("J020".equals(mJetFormType) || "J021".equals(mJetFormType)) {
			if (LCTermSpecFlag < 1 && "".equals(mComArchiveName)) {
				cXmlDataset.addDataObject(new XMLDataTag("remarkflag", "0"));
			} else {
				cXmlDataset.addDataObject(new XMLDataTag("remarkflag", "1"));
			}

		}
		return true;
	}

	/**
	 * 一个空值节点
	 * 
	 * @param cXmlDataset
	 * @return boolean
	 */
	private boolean getReceiptNote(XMLDataset tXmlDataset) {
		XMLDataList xmlDataList = new XMLDataList();
		xmlDataList.setDataObjectID("ReceiptNote");
		tXmlDataset.addDataObject(xmlDataList);
		return true;
	}

	/**
	 * 获取险种名称以及保费
	 * 
	 * @param cXmlDataset
	 * @return boolean
	 */
	private boolean getPremDetail(XMLDataset tXmlDataset) {
		XMLDataList xmlDataList = new XMLDataList();
		xmlDataList.setDataObjectID("PremDetail");
		xmlDataList.addColHead("RiskName");
		xmlDataList.addColHead("RiskPrem");
		ExeSQL tExeSQL = new ExeSQL();
		String tStrSql = "select distinct(riskcode) from lcpol "
				+ "where contno='" + this.mLCContSchema.getContNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(tStrSql);
		if (tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				String tStrSql1 = "select riskname from lmriskapp "
						+ "where riskcode='" + tSSRS.GetText(i, 1) + "'";
				String riskname = tExeSQL.getOneValue(tStrSql1);
				xmlDataList.setColValue("RiskName", riskname);

				String tStrSql2 = "select sum(sumactupaymoney) from ljapayperson "
						+ "where payno in (select payno from ljapay "
						+ "where incomeno='"
						+ this.mLCContSchema.getContNo()
						+ "' and duefeetype='0') "
						+ "and riskcode ='"
						+ tSSRS.GetText(i, 1) + "'";
				String riskprem = tExeSQL.getOneValue(tStrSql2);
				xmlDataList.setColValue("RiskPrem", riskprem);
				xmlDataList.insertRow(0);
			}
		}
		tXmlDataset.addDataObject(xmlDataList);
		return true;
	}

	/**
	 * 获取个人健康问卷。
	 * 
	 * @return
	 */
	private boolean getSurvey(XMLDataset cXMLDataset) {
		XMLDataList xmlDataList = new XMLDataList();
		xmlDataList.setDataObjectID("Survey");
		// InsuredName及InsuredNo指代保单下客户的信息（包含投保人）
		// 如投保人和3个被保人的信息均不一样，则显示4个
		// 如投保人和被保人信息一样，则显示1个
		xmlDataList.addColHead("InsuredName");
		xmlDataList.addColHead("InsuredNo");
		// String tStrSql =
		// " select distinct insuredname ,insuredno from lcpol where contno='"+this.mLCContSchema.getContNo()+"' ";
		String tStrSql = "select appntname, appntno from lcpol "
				+ "where contno='" + this.mLCContSchema.getContNo() + "' "
				+ "union " + "select insuredname, insuredno from lcpol "
				+ "where contno='" + this.mLCContSchema.getContNo() + "' ";
		SSRS tSSRS = new ExeSQL().execSQL(tStrSql);
		if (tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				xmlDataList.setColValue("InsuredName", tSSRS.GetText(i, 1));
				xmlDataList.setColValue("InsuredNo", tSSRS.GetText(i, 2));
				xmlDataList.insertRow(0);
			}

		}
		cXMLDataset.addDataObject(xmlDataList);
		return true;
	}

	private boolean getProposalForm(XMLDataset cXMLDataset) {
		Element element = cXMLDataset.getElementNoClone();
		Element elementControl = new Element("ProposalForm");
		element.addContent(elementControl);
		ExeSQL e1 = new ExeSQL();
		String managename = e1
				.getOneValue("select name from ldcom where comcode='"
						+ mLCContSchema.getManageCom() + "'");

		elementControl.addContent(new Element("MANAGECONNAME")
				.addContent(managename));
		elementControl.addContent(new Element("MANAGECON")
				.addContent(stringnotnull(mLCContSchema.getManageCom())));
		elementControl.addContent(new Element("AGENTCODE")
				.addContent(stringnotnull(mLCContSchema.getAgentCode())));
		String AGENT = e1
				.getOneValue("select name from laagent where agentcode='"
						+ mLCContSchema.getAgentCode() + "'");
		elementControl.addContent(new Element("AGENT").addContent(AGENT));
		elementControl.addContent(new Element("TRIALCODE")
				.addContent(stringnotnull(mLCContSchema.getFirstTrialOperator())));
		elementControl.addContent(new Element("ACQDATE")
				.addContent(stringnotnull(mLCContSchema.getPolApplyDate())));
		String salename = e1
				.getOneValue("select codename from ldcode where codetype='salechnl' and code='"
						+ mLCContSchema.getSaleChnl() + "'");
		elementControl.addContent(new Element("SALEMEG").addContent(salename));

		elementControl.addContent(new Element("SALECHNL")
				.addContent(stringnotnull(mLCContSchema.getSaleChnl())));
		String crssale = e1
				.getOneValue("select codename from ldcode where codetype='crs_busstype' and code='"
						+ mLCContSchema.getCrs_BussType() + "'");
		elementControl.addContent(new Element("ACROSSSALEMEG")
				.addContent(crssale));
		String crssalename = e1
				.getOneValue("select codename from ldcode where codetype='crs_salechnl' and code='"
						+ mLCContSchema.getCrs_SaleChnl() + "'");
		elementControl.addContent(new Element("ACROSSSALECHNL")
				.addContent(crssalename));
		// 税优信息
		String insumark = e1
				.getOneValue("select (select codename from ldcode where codetype='taxflag' and code=taxflag) from lccontsub where prtno='"
						+ mLCContSchema.getPrtNo() + "'");
		elementControl.addContent(new Element("INSURANCEMAK")
				.addContent(insumark));
		String INSURANCCHNL = e1
				.getOneValue("select (select codename from ldcode where codetype='transflag' and code=transflag) from lccontsub where prtno='"
						+ mLCContSchema.getPrtNo() + "'");
		elementControl.addContent(new Element("INSURANCCHNL")
				.addContent(INSURANCCHNL));
		elementControl.addContent(new Element("BFMANAGECON")
		.addContent(stringnotnull("")));
		elementControl.addContent(new Element("BFMANAGECONNAME")
		.addContent(""));
		elementControl.addContent(new Element("BFCONTNO")
		.addContent(stringnotnull("")));
//		elementControl.addContent(new Element("BFMANAGECON")
//				.addContent(stringnotnull(mLCContSchema.getManageCom())));
//		elementControl.addContent(new Element("BFMANAGECONNAME")
//				.addContent(managename));
//		elementControl.addContent(new Element("BFCONTNO")
//				.addContent(stringnotnull(mLCContSchema.getContNo())));
		// 投保人信息
		elementControl.addContent(new Element("APPNTNAME")
				.addContent(stringnotnull(mLCAppntSchema.getAppntName())));
		elementControl.addContent(new Element("APPNTBIRTHDAY")
				.addContent(stringnotnull(mLCAppntSchema.getAppntBirthday())));
		String sex = e1
				.getOneValue("select codename from ldcode where codetype='sex' and code='"
						+ mLCAppntSchema.getAppntSex() + "'");
		elementControl.addContent(new Element("APPNTSEX").addContent(sex));
		String marry = e1
				.getOneValue("select codename from ldcode where codetype ='marriage' and code='"
						+ mLCAppntSchema.getMarriage() + "'");
		elementControl.addContent(new Element("APPNTMARRIAGE")
				.addContent(marry));
		String nativeplace="";
		if ("ML".equals(mLCAppntSchema.getNativePlace())) {
			nativeplace = e1
					.getOneValue("select codename from ldcode where codetype ='nativeplace' and code='"
							+ mLCAppntSchema.getNativePlace() + "'");
			
		}else{
			nativeplace = e1
					.getOneValue("select codename from ldcode where codetype ='nativecity' and code='"
							+ mLCAppntSchema.getNativeCity() + "'");
		}
		
		elementControl.addContent(new Element("APPNTNATIONALITY")
				.addContent(nativeplace));
		String idtype = e1
				.getOneValue("select codename from ldcode where codetype ='idtype' and code='"
						+ mLCAppntSchema.getIDType() + "'");
		elementControl
				.addContent(new Element("APPNTIDTYPE").addContent(idtype));
		elementControl
		.addContent(new Element("APPNTIDCODE").addContent(stringnotnull(mLCAppntSchema.getIDNo())));
//		String idflag = "N";
//		if ("".equals(mLCAppntSchema.getIDEndDate())
//				|| mLCAppntSchema.getIDEndDate() == null) {
//			idflag = "Y";
//		}
//		elementControl
//				.addContent(new Element("APPNTIDFLAG").addContent(idflag));
		elementControl.addContent(new Element("APPNTIDCVALIDATE")
				.addContent(stringnotnull(mLCAppntSchema.getIDStartDate())));
		elementControl.addContent(new Element("APPNTIDCCINVALIDATE")
				.addContent(stringnotnull(mLCAppntSchema.getIDEndDate())));
		elementControl.addContent(new Element("APPNTCOM")
				.addContent(stringnotnull(mLCAddressSchema.getGrpName())));
		String occuname = e1
				.getOneValue("select occupationname from LDOccupation  where occupationcode='"
						+ mLCAppntSchema.getOccupationCode() + "'");
		elementControl.addContent(new Element("APPNTOCCUPATIONNAME")
				.addContent(stringnotnull(occuname)));
		elementControl.addContent(new Element("APPNTOCCUPATIONCODE")
				.addContent(stringnotnull(mLCAppntSchema.getOccupationCode())));
		elementControl.addContent(new Element("APPNTOCCUPATIONTYPE")
				.addContent(stringnotnull(mLCAppntSchema.getOccupationType())));
		elementControl.addContent(new Element("APPNTSALARY").addContent(Double
				.toString(mLCAppntSchema.getSalary())));
		LCContSubSchema lcContSchema = new LCContSubDB().executeQuery(
				"select * from lccontsub where prtno='"
						+ mLCContSchema.getPrtNo() + "'").get(1);
		elementControl.addContent(new Element("APPNTCREDITCODE")
				.addContent(stringnotnull(lcContSchema.getCreditCode())));
		elementControl.addContent(new Element("COMTAXREGISTCODE")
				.addContent(stringnotnull(lcContSchema.getGTaxNo())));
		elementControl.addContent(new Element("PERTAXREGISTCODE")
				.addContent(stringnotnull(lcContSchema.getTaxNo())));
		elementControl.addContent(new Element("PERTAXREGISTTYPE")
		.addContent(stringnotnull(numtranscn(lcContSchema.getTaxPayerType(), "taxpayertype"))));
		elementControl.addContent(new Element("APPNTHOMEADDRESS")
				.addContent(stringnotnull(mLCAddressSchema.getHomeAddress())));
		elementControl.addContent(new Element("APPNTZIPCODE")
				.addContent(stringnotnull(mLCAddressSchema.getZipCode())));
		elementControl.addContent(new Element("APPNTEMAIL")
				.addContent(stringnotnull(mLCAddressSchema.getEMail())));
		elementControl.addContent(new Element("APPNTPHONE")
				.addContent(stringnotnull(mLCAddressSchema.getHomePhone())));
		elementControl.addContent(new Element("MOBELPHONE")
				.addContent(stringnotnull(mLCAddressSchema.getPhone())));
		String rela = e1
				.getOneValue("select RelationToAppnt from lcinsured where prtno='"
						+ mLCAppntSchema.getPrtNo() + "'");
		elementControl.addContent(new Element("RELATINOTOAPPNT")
		.addContent(stringnotnull(numtranscn(rela, "relation"))));
		elementControl.addContent(new Element("PAYMODE").addContent(numtranscn(
				mLCContSchema.getPayMode(), "paymode")));
		elementControl
				.addContent(new Element("EXPAYMODE").addContent(numtranscn(
						mLCContSchema.getExPayMode(), "paymode")));
		elementControl.addContent(new Element("BANKNAME").addContent(e1
				.getOneValue("select bankname from ldbank where bankcode='"
						+ mLCContSchema.getBankCode() + "'")));
		elementControl.addContent(new Element("BANKCODE")
				.addContent(stringnotnull(mLCContSchema.getBankCode())));
		elementControl.addContent(new Element("APPNTACCNAME")
				.addContent(stringnotnull(mLCContSchema.getAccName())));
		elementControl.addContent(new Element("APPNTBANKACCNO")
				.addContent(stringnotnull(mLCContSchema.getBankAccNo())));

		elementControl.addContent(new Element("PAYREMIND").addContent(""));
		elementControl.addContent(new Element("PreviewSign").addContent(mSign));
		elementControl.addContent(new Element("APPLICANT_DATE")
				.addContent(stringnotnull(mLCContSchema.getPolApplyDate())));
		XMLDataList xmlDataList = new XMLDataList();
		xmlDataList.setDataObjectID("BZJH");

		xmlDataList.addColHead("PRODUCT_NAME");
		xmlDataList.addColHead("PRODUCT_CODE");
		xmlDataList.addColHead("PRODUCT_AMOUNT");
		xmlDataList.addColHead("PRODUCT_PERIOD_NAIN");
		xmlDataList.addColHead("PRODUCT_PAY_PERIOD");
		xmlDataList.addColHead("PRODUCT_PAY_FREQUENCY");
		xmlDataList.addColHead("PRODUCT_PREMIUM");

		String tStrSql = "select (select riskname from lmrisk where riskcode=lc.riskcode),riskcode,amnt,InsuYear,paymode,payintv,prem,PayEndYear "
				+ "from lcpol lc where prtno='"
				+ mLCContSchema.getPrtNo()
				+ "' ";

		SSRS tSSRS = new ExeSQL().execSQL(tStrSql);
		if (tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				xmlDataList.setColValue("PRODUCT_NAME", tSSRS.GetText(i, 1));
				xmlDataList.setColValue("PRODUCT_CODE", tSSRS.GetText(i, 2));
				xmlDataList.setColValue("PRODUCT_AMOUNT", tSSRS.GetText(i, 3));
				xmlDataList.setColValue("PRODUCT_PERIOD_NAIN",
						tSSRS.GetText(i, 4) + "年");
				xmlDataList.setColValue("PRODUCT_PAY_PERIOD",
						tSSRS.GetText(i, 8) + "年");
				xmlDataList.setColValue("PRODUCT_PAY_FREQUENCY",
						numtranscn(tSSRS.GetText(i, 6), "payintv"));
				xmlDataList.setColValue("PRODUCT_PREMIUM", tSSRS.GetText(i, 7));

				xmlDataList.insertRow(0);
			}

		}
		xmlDataList.addDataTo(elementControl);
		Element gzl = new Element("GZL");
		elementControl.addContent(gzl);
		String g1 = e1
				.getOneValue("select impartparammodle from lccustomerimpart where impartcode='010' and prtno='"
						+ mLCContSchema.getPrtNo() + "'");
		String[] g11 = g1.split(",");
		gzl.addContent(new Element("QU1_INSURED_H").addContent(g11[0]+"cm"));
		gzl.addContent(new Element("QU1_INSURED_W").addContent(g11[1]+"kg"));
		String g2 = e1
				.getOneValue("select impartparammodle from lccustomerimpart where impartcode='170' and prtno='"
						+ mLCContSchema.getPrtNo() + "'");
		String[] g21 = g2.split(",");
		StringBuffer s2 = new StringBuffer();
		for (int i = 0; i < g21.length; i++) {
			if (g21[i].equals("Y")) {
				s2.append((i + 1) + ",");
			}

		}
		if (s2.lastIndexOf(",") != -1) {
			s2.deleteCharAt(s2.lastIndexOf(","));
		}

		gzl.addContent(new Element("QU2_INSURED").addContent(s2.toString()));
		String g3 = e1
				.getOneValue("select impartparammodle from lccustomerimpart where impartcode='180' and prtno='"
						+ mLCContSchema.getPrtNo() + "'");
		String[] g31 = g3.split(",");
		System.out.println(g31.length);
		gzl.addContent(new Element("QU3_INSURED").addContent(g31[0]));
		gzl.addContent(new Element("QU3_INSURED_NUM").addContent(g31[1]));
		gzl.addContent(new Element("QU3_INSURED_YEAR").addContent(g31[2]));
		String g4 = e1
				.getOneValue("select impartparammodle from lccustomerimpart where impartcode='190' and prtno='"
						+ mLCContSchema.getPrtNo() + "'");
		String[] g41 = g4.split(",");
		gzl.addContent(new Element("QU4_INSURED").addContent(g41[0]));
		StringBuffer s1 = new StringBuffer();
		if (g41[1].equals("Y")) {
			s1.append("白酒 ");
		}
		if (g41[2].equals("Y")) {
			s1.append("啤酒");
		}
		if (g41[3].equals("Y")) {
			s1.append("葡萄酒 ");
		}
		if (g41[4].equals("Y")) {
			s1.append("黄酒");
		}
		gzl.addContent(new Element("QU4_INSURED_TYPE").addContent(s1.toString()));
		gzl.addContent(new Element("QU4_INSURED_KG").addContent(g41[5]));
		gzl.addContent(new Element("QU4_INSURED_YEAR").addContent(g41[6]));
		String g5 = e1
				.getOneValue("select impartparammodle from lccustomerimpart where impartcode='110' and prtno='"
						+ mLCContSchema.getPrtNo() + "'");
		gzl.addContent(new Element("QU5_INSURED").addContent(g5));
		String g6 = e1
				.getOneValue("select impartparammodle from lccustomerimpart where impartcode='240' and prtno='"
						+ mLCContSchema.getPrtNo() + "'");

		gzl.addContent(new Element("QU6_INSURED").addContent(g6));
		String g7 = e1
				.getOneValue("select impartparammodle from lccustomerimpart where impartcode='260' and prtno='"
						+ mLCContSchema.getPrtNo() + "'");

		gzl.addContent(new Element("QU7_INSURED").addContent(g7));
		String g8 = e1
				.getOneValue("select impartparammodle from lccustomerimpart where impartcode='270' and prtno='"
						+ mLCContSchema.getPrtNo() + "'");
		gzl.addContent(new Element("QU8_INSURED").addContent(g8));
		XMLDataList xmlDataList1 = new XMLDataList();
		xmlDataList1.setDataObjectID("GZMX");
		xmlDataList1.addColHead("QUESTION");
		xmlDataList1.addColHead("EXPLAIN");

		// String tStrSql =
		// " select distinct insuredname ,insuredno from lcpol where contno='"+this.mLCContSchema.getContNo()+"' ";
		String tStrSql1 = "select lc.diseasecontent,lc.impartdetailcontent,lc.impartcode from lccustomerimpartdetail lc where "
				+ "  lc.prtno='" + this.mLCContSchema.getPrtNo() + "' ";

		SSRS tSSRS1 = new ExeSQL().execSQL(tStrSql1);
		if (tSSRS1.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS1.getMaxRow(); i++) {
				xmlDataList1.setColValue("QUESTION", tSSRS1.GetText(i, 2));
				xmlDataList1.setColValue("EXPLAIN", tSSRS1.GetText(i, 1));

				xmlDataList1.insertRow(0);
			}

		}
		xmlDataList1.addDataTo(elementControl);
		Element TBTSH = new Element("TBTSH");
		elementControl.addContent(TBTSH);
		String sql = "  select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate "
				+ "where  codetype='CFNL' and state='1'  order by Year desc,quarter desc fetch first row only  with ur ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS mSSRS = tExeSQL.execSQL(sql.toString());
		TBTSH.addContent(new Element("EndYear").addContent(mSSRS.GetText(1, 1)));
		TBTSH.addContent(new Element("Quarter").addContent(mSSRS.GetText(1, 2)));
		TBTSH.addContent(new Element("Solvency").addContent(mSSRS.GetText(1, 3)));
		TBTSH.addContent(new Element("YorN").addContent(mSSRS.GetText(1, 4)));
		String sql1 = "   select Year,quarter,riskrate  from  ldriskrate  where  codetype='FXDJ' and state='1' "
				+ " order by Year desc,quarter desc fetch first row only  with ur  ";
		ExeSQL ExeSQL = new ExeSQL();
		
		SSRS mSSRS1 = ExeSQL.execSQL(sql1.toString());
		
		TBTSH.addContent(new Element("CEndYear").addContent(mSSRS1
				.GetText(1, 1)));
		TBTSH.addContent(new Element("CQuarter").addContent(mSSRS1
				.GetText(1, 2)));
		TBTSH.addContent(new Element("Riskrate").addContent(mSSRS1
				.GetText(1, 3)));
		TBTSH.addContent(new Element("Hostline1").addContent("12378"));
		TBTSH.addContent(new Element("Hostline2").addContent("95001303"));
		TBTSH.addContent(new Element("PrintingNo").addContent(mOrderNo));

	

		return true;
	}

	public String numtranscn(String num, String type) {

		return new ExeSQL()
				.getOneValue("select codename from ldcode where codetype='"
						+ type + "' and code='" + num + "'");

	}
	public String stringnotnull(String s1){
		if(s1==null||"".equals(s1)){
			return "";
		}else{
			return s1;
		}
		
	}

	public static void main(String[] args) {
		SHContF1PBL bl = new SHContF1PBL();
		bl.mGlobalInput.ComCode = "86";
		bl.mGlobalInput.ManageCom = bl.mGlobalInput.ComCode;
		bl.mGlobalInput.Operator = "group";
		bl.mTemplatePath = "E:\\lisdev\\ui\\f1print\\template\\";
		bl.mOutXmlPath = "F:\\";

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo("014110156000001");
		tLCContDB.getInfo();
		bl.mLCContSchema = tLCContDB.getSchema();

		XMLDatasets tXMLDatasets = new XMLDatasets();
		tXMLDatasets.createDocument();

		// XMLDataset tXMLDataset = tXMLDatasets.createDataset();

		// if (!bl.combineInvioce(tXMLDataset))
		// {
		// System.out.println(bl.mErrors.getErrContent());
		// }
		VData aInputData = new VData();
		aInputData.add(bl.mGlobalInput);
		aInputData.add(bl.mLCContSchema);
		aInputData.add(bl.mTemplatePath);
		aInputData.add(bl.mOutXmlPath);
		aInputData.add("1");
		aInputData.add("");
		// aInputData.add("P");
		if (!bl.submitData(aInputData, "PRINT")) {
			System.out.println(bl.mErrors.getErrContent());
		}
	}
}
