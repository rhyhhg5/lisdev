package com.cbsws.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.LjagetTable;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.finfee.BatchPayUI;
import com.sinosoft.lis.finfee.OperFinFeeGetUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


public class WxFeePay extends ABusLogic {
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	
	public LjagetTable cLjagetTable;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public CErrors mError = null;//处理过程中的错误信息
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始处理网销复杂产品付费业务");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLjagetList = cMsgInfos.getBodyByFlag("LjagetTable");
	        if (tLjagetList == null || tLjagetList.size() != 1)
	        {
	            errLog("获取付费信息失败。");
	            return false;
	        }
	        cLjagetTable = (LjagetTable) tLjagetList.get(0); //获取保单信息，只有一个保单
			mGlobalInput.ManageCom = cLjagetTable.getManageCom();//  不确定  需要修改  这个是付费机构
			mGlobalInput.ComCode = cLjagetTable.getManageCom();//  不确定  需要修改  这个是付费机构

    		LJAGetDB tLJAGetDB = new LJAGetDB();
    		LJAGetSet tLJAGetSet = new LJAGetSet();
    		tLJAGetSet=tLJAGetDB.executeQuery("select * from ljaget where actugetno='"+cLjagetTable.getActugetNo()+"' ");
			
    		if(tLJAGetSet ==null || tLJAGetSet.size()!=1){
    			errLog("付费信息查询失败！");
    			return false;
    		}
    		
    		LJAGetSchema tLJAGetSchema = tLJAGetSet.get(1);
    		String enter=tLJAGetSchema.getEnterAccDate();
    		if(!"".equals(enter)&&enter!=null){
    			errLog("付费号:"+tLJAGetSchema.getActuGetNo()+" 已付费完成！");
    			return false;
    		}
    		
    		SSRS BankInfos = getBankAccCode();
    		if(BankInfos==null){
    			errLog("获取归集帐号错误！");
    			return false;
    		}
        	String ZBankManageCom = BankInfos.GetText(1, 1);//总公司机构
        	String ZBankAccNo = BankInfos.GetText(1, 2);
        	
        	String aBankCode = getBankCode(ZBankAccNo);
    		
    		String EnterAccDate = PubFun.getCurrentDate();
    		String Drawer = cLjagetTable.getDrawer();
    		String DrawerID = cLjagetTable.getDrawerID();
    		
    		
    		tLJAGetSchema.setInsBankAccNo(ZBankAccNo);
    		tLJAGetSchema.setInsBankCode(aBankCode);
    		tLJAGetSchema.setPayMode(cLjagetTable.getPayMode());
    		tLJAGetSchema.setDrawer(Drawer);
    		tLJAGetSchema.setDrawerID(DrawerID);
    		tLJAGetSchema.setEnterAccDate(EnterAccDate);
    		
    		LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
    	    tLJFIGetSchema.setActuGetNo(tLJAGetSchema.getActuGetNo());
    	    tLJFIGetSchema.setPayMode(tLJAGetSchema.getPayMode());
    	    tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
    	    tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
    	    tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
    	    tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
    	    tLJFIGetSchema.setEnterAccDate(EnterAccDate);
    	    tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
    	    tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
    	    tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
    	    tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
    	    tLJFIGetSchema.setDrawer(Drawer);
    	    tLJFIGetSchema.setDrawerID(DrawerID);
    	    tLJFIGetSchema.setOperator(Operator);

    	    VData tVData = new VData();
    	    tVData.clear();
    	    tVData.add(tLJFIGetSchema);
    	    tVData.add(tLJAGetSchema);   
    	    tVData.add(mGlobalInput);
    	    OperFinFeeGetUI tOperFinFeeGetUI = new OperFinFeeGetUI();
    	    if(tOperFinFeeGetUI.submitData(tVData,"VERIFY")==true){
    	    		BatchPayUI  tBatchPayUI=new BatchPayUI();
    	    		LJAGetSet  mLJAGetSet = new LJAGetSet();
    	 	 	    LJFIGetSet mLJFIGetSet = new LJFIGetSet();
    	 	 	    mLJFIGetSet.add(tLJFIGetSchema);
    	 			mLJAGetSet.add(tLJAGetSchema);
    	 			tVData.add(mLJFIGetSet);
    	    		tVData.add(mLJAGetSet);   
    	    		tBatchPayUI.submitData(tVData,"CLAIM");

    	    }
    	    mError = tOperFinFeeGetUI.mErrors; 
    	    if (mError.needDealError())
    	    {                          
    	    	errLog(mError.getFirstError()) ;
    	        return false;
    	    } 


		}catch(Exception ex){
			return false;
		}
		return true;
	}
	
	/**
     * 付费的机构编码和银行账户。
     * @param szFunc
     * @param szErrMsg
     */
	
    public SSRS getBankAccCode()
    {
    	String sql = "select code,codename from ldcode where codetype = 'InsBankAccNoF' and othersign = 'WXF' and code='"+cLjagetTable.getManageCom()+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0)
    	{
        	return null;
    	}
        return tSSRS;
    }
    
    /**
     * 获取银行编码。
     * @param szFunc
     * @param szErrMsg
     */
    public String getBankCode(String aBankAccNo)
    {
    	String sql = "select bankcode from ldfinbank where bankaccno = '"+aBankAccNo+"' ";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0)
    	{
        	return null;
    	}
        return tSSRS.GetText(1, 1);
    }
	
    
}
