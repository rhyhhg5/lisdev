/**
 * 印刷号和操作号录入新表：WXContRelaInfo
 */
package com.cbsws.xml.ctrl.complexpds;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.obj.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.WXContRelaInfoSchema;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class RYContRelaInfoBL implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(WxContBL.class);
	
	private final LCContWTable cLCContTable;
	private final MsgHead cMsgHead;
	private MMap mMap = new MMap();
	private String cError="";
	
	public RYContRelaInfoBL(MsgHead tMsgHead,LCContWTable tLCContTable){
		cMsgHead=tMsgHead;
		cLCContTable=tLCContTable;
	}
	
	public String deal() throws Exception {
		cLogger.info("Into WxContBL.deal()...");
		VData mContBLData = getContBLVData();
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectInfoBL Submit...");
		if (!tPubSubmit.submitData(mContBLData, "")) {
			// @@错误处理
			return tPubSubmit.mErrors.getContent();
		}
		cLogger.info("Out WxContBL.deal()!");
		return cError;
	}
	
	private VData getContBLVData() throws MidplatException {
		cLogger.info("Into WXContBL.getContBLVData()...");

//		微信保单信息
		WXContRelaInfoSchema mWXContRelaInfoSchema=new WXContRelaInfoSchema();
		String cSerNo = PubFun1.CreateMaxNo("SerNo", 20);//自动生成流水号
		mWXContRelaInfoSchema.setSerNo(cSerNo);
		
		mWXContRelaInfoSchema.setBatchNo(cMsgHead.getBatchNo());
		mWXContRelaInfoSchema.setSendDate(cMsgHead.getSendDate());
		mWXContRelaInfoSchema.setSendTime(cMsgHead.getSendTime());
		mWXContRelaInfoSchema.setBranchCode(cMsgHead.getBranchCode());
		mWXContRelaInfoSchema.setOperator(cMsgHead.getSendOperator());
		mWXContRelaInfoSchema.setMsgType(cMsgHead.getMsgType());
		mWXContRelaInfoSchema.setPrtNo(cLCContTable.getPrtNo());
		mWXContRelaInfoSchema.setMakeDate(PubFun.getCurrentDate());
		mWXContRelaInfoSchema.setMakeTime(PubFun.getCurrentTime());
		mWXContRelaInfoSchema.setModifyDate(PubFun.getCurrentDate());
		mWXContRelaInfoSchema.setModifyTime(PubFun.getCurrentTime());

		VData myData=new VData();
		mMap.put(mWXContRelaInfoSchema, SysConst.INSERT);
		myData.add(mMap);

		cLogger.info("Out WXContRelaInfoBL.getContBLVData()!");
		return myData;
	}
	
	public static void main(String[] args) throws Exception {
//		System.out.println("程序开始…");
//		
//		String mInFile = "D:/request/ICBC_std/UW.xml";
//		
//		FileInputStream mFis = new FileInputStream(mInFile);
//		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
//		System.out.println("成功结束！");
//		RYContRelaInfoBL ttWXContRelaInfoSchema=new RYContRelaInfoBL(tMsgHead,cLCContTable);
//		String mError=ttWXContRelaInfoSchema.deal();
//		if(!"".equals(mError)){
//			System.out.println("处理失败");
//		}
	}
}
