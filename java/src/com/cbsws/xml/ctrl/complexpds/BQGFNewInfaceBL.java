package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.BackParamInfo;
import com.cbsws.obj.ECCertSalesInfoTable;
import com.cbsws.obj.LCAddressTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPEdorItemTable;
import com.cbsws.obj.LPEdorMainTable;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LPAddressSchema;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LPAddressSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.task.BQADEdorBL;
import com.sinosoft.task.BQFCEdorBL;
import com.sinosoft.task.BQGFEdorBL;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.EasyEdorBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class BQGFNewInfaceBL extends ABusLogic {

	public String BatchNo = "";// 批次号
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	public LCContTable cLCContTable;
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String mEdorAcceptNo;
	private LGWorkTable mLGWorkTable;
//	private LPEdorItemTable mLPEdorItemTable;
	private List mLPEdorMainList;
	public CErrors mErrors = new CErrors();
	SSRS tSSRS = new SSRS();
	ExeSQL tExeSQL = new ExeSQL();
	private MMap map = new MMap();

	boolean appntFlag = false;
	boolean insuredFlag = false;

	protected boolean deal(MsgCollection cMsgInfos) {
		// 解析xml
		if (!parseXML(cMsgInfos)) {
			return false;
		}
		if (!checkdata()) {
			return false;
		}

		// 准备报文中的数据
		if (!changeBQGF()) {
			return false;
		}

		return true;
	}

	private boolean parseXML(MsgCollection cMsgInfos) {
		System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		List tLCContTableList = cMsgInfos.getBodyByFlag("LCContTable");
		if (tLCContTableList == null || tLCContTableList.size() != 1) {
			errLog("申请报文中获取保单号信息失败。");
			return false;
		}
		cLCContTable = (LCContTable) tLCContTableList.get(0);

		List tLGWorkList = cMsgInfos.getBodyByFlag("LGWorkTable");
        if(tLGWorkList == null ||tLGWorkList.size()!=1)
        {
            errLog("申请报文中获取工单信息失败。");
            return false;
        }
        mLGWorkTable=(LGWorkTable)tLGWorkList.get(0);

		return true;
	}

	private boolean checkdata() {
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(cLCContTable.getContNo());
		if (!tLCContDB.getInfo()) {
			errLog("没有查询到保单信息" + cLCContTable.getContNo() + "。");
			return false;
		}

		return true;
	}

	/**
     * 生成工单号
     * @return String
     */
    public static final String createWorkNo()
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        String tWorkNo = tDate + PubFun1.CreateMaxNo("TASK" + tDate, 6);
        return tWorkNo;
    }
	
	
	private boolean changeBQGF() {
		LCContSchema tLCContSchema = new LCContSchema();
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(cLCContTable.getContNo());
		if (!tLCContDB.getInfo()) {
			errLog("没有查询到保单信息" + cLCContTable.getContNo() + "。");
			return false;
		}
//		tLCContSchema = tLCContDB.getSchema();
		
		String workNo = CommonBL.createWorkNo();
		//得到工单信息
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(tLCContDB.getAppntNo());
        tLGWorkSchema.setWorkNo(workNo);
        tLGWorkSchema.setTypeNo(mLGWorkTable.getTypeNo());
        tLGWorkSchema.setContNo(cLCContTable.getContNo());
        tLGWorkSchema.setApplyTypeNo(mLGWorkTable.getApplyTypeNo());
        tLGWorkSchema.setAcceptWayNo(mLGWorkTable.getAcceptWayNo());
        tLGWorkSchema.setAcceptDate(mLGWorkTable.getAcceptDate());
        tLGWorkSchema.setApplyName(mLGWorkTable.getApplyName());
        tLGWorkSchema.setRemark("调用线上保全解冻生成");
        
     //   LPContSet tLPContSet = new LPContSet();
        LPContSchema tLPContSchema = new LPContSchema();
        tLPContSchema.setEdorNo(workNo);
        tLPContSchema.setEdorType("GF");
        tLPContSchema.setContNo(tLCContDB.getContNo());
      //  tLPContSet.add(tLPContSchema);
        
		// 操作员信息
		mGlobalInput.Operator = Operator;
		mGlobalInput.ManageCom = tLCContDB.getManageCom();
		mGlobalInput.ComCode = mGlobalInput.ManageCom;

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLPContSchema);
		data.add(tLGWorkSchema);

		BQGFEdorBL tBQGFEdorBL = new BQGFEdorBL();
		if (!tBQGFEdorBL.submitData(data)) {
			errLog("保全变更失败" + tBQGFEdorBL.mErrors.getFirstError());
			return false;
		}
		LPEdorMainTable tLPEdorMainTable = new LPEdorMainTable();
		tLPEdorMainTable.setEdorAcceptNo(workNo);
		// 返回报文
		// 组织返回报文
		getWrapParmList(tLPEdorMainTable);
		getXmlResult();
		return true;
	}

	private void getWrapParmList(LPEdorMainTable tLPEdorMainTable) {
		mLPEdorMainList = new ArrayList();

		mLPEdorMainList.add(tLPEdorMainTable);
	}

	// 返回报文
	public void getXmlResult() {
		// 返回退费信息
		for (int i = 0; i < mLPEdorMainList.size(); i++) {
			putResult("LPEdorMainTable",
					(LPEdorMainTable) mLPEdorMainList.get(i));
		}

	}

}
