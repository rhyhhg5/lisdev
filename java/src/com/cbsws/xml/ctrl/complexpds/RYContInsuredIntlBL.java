package com.cbsws.xml.ctrl.complexpds;

/**
 * 录入被保人和险种信息！
 */

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.obj.LCAppntTable;
import com.cbsws.obj.LCBnfTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LCInsuredTable;
import com.cbsws.obj.SYOtherTable;
import com.cbsws.obj.SpareFieldTable;
import com.cbsws.obj.WrapParamTable;
import com.cbsws.obj.WrapTable;
import com.sinosoft.lis.brieftb.BriefSingleContInputBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.intltb.ContInsuredIntlBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCCustomerImpartDetailSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCDiseaseResultSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class RYContInsuredIntlBL {
	private final static Logger cLogger = Logger.getLogger(RYContInsuredIntlBL.class);
	
	private MsgHead cMsgHead;
	private LCContTable cLCContTable;//保单信息
	private LCAppntTable cLCAppntTable;//投保人
	private SpareFieldTable tSpareFieldTable;//备注信息
	private List cLCInsuredList;//被保人
	private WrapTable cWrapTable;//套餐
	private List cLCBnfList;//受益人
	private List cWrapParamList; //算费参数
	private List cDutyCodeList;//本次算费的所有责任
	private String Errors = "";//错误信息
	
	private LCContSchema cLCContSchema = null;
	private String mPrtno = "";
	private String mMsgType = "";
	public double mPrem=0;
	public double mAmnt=0;
	public LCDutySet cLCDutySet = new LCDutySet();
	public LCPolSet cLCPolSet = new LCPolSet();
	public GlobalInput cGlobalInput = new GlobalInput();
	public LCRiskDutyWrapSet cLCRiskDutyWrapSet = new  LCRiskDutyWrapSet();
	public LCCustomerImpartSet cLcCustomerImpartSet=new LCCustomerImpartSet();
	public LCCustomerImpartDetailSet cLcCustomerImpartDetailSet=new LCCustomerImpartDetailSet();
	public SYOtherTable syOtherTable=new SYOtherTable();
	
	public RYContInsuredIntlBL(GlobalInput mGlobalInput,MsgHead tMsgHead,LCContTable tLCContTable, LCAppntTable tLAppntTable,List tLCInsuredList,WrapTable tWrapTable,List tLCBnfList,List tWrapParamList,LCCustomerImpartSet lcCustomerImpartSet,LCCustomerImpartDetailSet lcCustomerImpartDetailSet,SpareFieldTable cSpareFieldTable) {
		cMsgHead = tMsgHead;
		cLCContTable = tLCContTable;
		cLCAppntTable = tLAppntTable;
		cLCInsuredList = tLCInsuredList;
		cWrapTable = tWrapTable;
		cLCBnfList = tLCBnfList;
		cWrapParamList = tWrapParamList;
		cGlobalInput = mGlobalInput;
		cLcCustomerImpartSet=lcCustomerImpartSet;
		cLcCustomerImpartDetailSet=lcCustomerImpartDetailSet;
		tSpareFieldTable = cSpareFieldTable;
	}
	
	public String deal() {
		cLogger.info("Into WxContInsuredIntlBL.deal()...");
		
		mMsgType = cMsgHead.getMsgType();
		mPrtno = cLCContTable.getPrtNo();
		getDutyCodeList();
		if("RY0004".equals(mMsgType)){
			LCContDB mLCContDB = new LCContDB();
			mLCContDB.setPrtNo(mPrtno);
			LCContSet mLCContSet =  mLCContDB.query();
			if (1 != mLCContSet.size()) {
	            return "获取保单信息失败";
			}
			cLCContSchema = mLCContSet.get(1);
			String cardflag=cLCContSchema.getCardFlag();
			if("b".equals(cardflag)){
				if(getRiskCodeList()){
					//cLCContSchema.setExiSpec("Y");
					String tsql = "update lccont set exispec='N' where prtno='"+mPrtno+"'";
					new ExeSQL().execUpdateSQL(tsql);
				}
			}
		}else{
			cLCContSchema = getLCContSchema();
		}
		for (int i = 0; i < cLCInsuredList.size(); i++) {
			LCInsuredTable tLCInsuredTable = (LCInsuredTable) cLCInsuredList.get(i);
			
			checkContInsuredIntlBL(tLCInsuredTable);
			
			VData tContInsuredIntlBLVData = getContInsuredIntlBLVData(tLCInsuredTable);
			if(tContInsuredIntlBLVData == null){
				return Errors;
			}
			ContInsuredIntlBL tContInsuredIntlBL   = new ContInsuredIntlBL();
			cLogger.info("Start call ContInsuredIntlBL.submitData()...");
			long tStartMillism = System.currentTimeMillis();
			if("RY0001".equals(mMsgType)){
				BriefSingleContInputBL bl = new BriefSingleContInputBL();
		        MMap tMMap3 = bl.getSubmitRiskMap(tContInsuredIntlBLVData, "INSERT||CONTINSURED");
		        LCContSchema tLCContSchema = (LCContSchema)tMMap3.getObjectByObjectName("LCContSchema", 0);
		        cLCPolSet = (LCPolSet)tMMap3.getObjectByObjectName("LCPolSet", 0);
		        cLCDutySet = (LCDutySet)tMMap3.getObjectByObjectName("LCDutySet", 0);
		        cLCRiskDutyWrapSet = (LCRiskDutyWrapSet)tMMap3.getObjectByObjectName("LCRiskDutyWrapSet", 0);
		        mPrem =  tLCContSchema.getPrem();
		        mAmnt = tLCContSchema.getAmnt();
		        System.out.println("WxContInsuredIntlBL中保额算保费prem="+mPrem);
			}else{
		        if (!tContInsuredIntlBL.submitData(tContInsuredIntlBLVData, "INSERT||CONTINSURED")) {
					return tContInsuredIntlBL.mErrors.getFirstError();
				}
			}
			
			cLogger.info("ContInsuredIntlBL耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s"
					+ "；报文类型：" + mMsgType);
			cLogger.info("End call ContInsuredIntlBL.submitData()!");
		}
		
		cLogger.info("Out WxContInsuredIntlBL.deal()!");
		return Errors;
	}
	
	private void checkContInsuredIntlBL(LCInsuredTable pLCInsured) {
		cLogger.info("Into WxContInsuredIntlBL.checkContInsuredIntlBL()...");
		
//		if (!pLCInsured.getRelaToMain().equals("00")) {
//			return "与主被保人关系必须为本人！";
//		}
//
//		//校验险种数据
//		List mRiskList = pLCInsured.getChild(Risks).getChildren(Risk);
//		for (int i = 0; i < mRiskList.size(); i++) {
//			Element tRisk = (Element) mRiskList.get(i);
//			
//			String tMainRiskCodeStr = tRisk.getChildText(MainRiskCode);
//			if (tMainRiskCodeStr.equals("331201")) {	//健康人生个人护理保险(万能型，B款)
//				checkRisk331201(tRisk);
//			} else if (tMainRiskCodeStr.equals("331301")) {	//健康人生个人护理保险(万能型，C款)
//				checkRisk331301(tRisk);
//			}
//			
//			//校验受益人
//			List tLCBnfList = tRisk.getChild(LCBnfs).getChildren(LCBnf);
//			for (int j = 0; j < tLCBnfList.size(); j++) {
//				Element ttLCBnf = (Element) tLCBnfList.get(j);
//				if (ttLCBnf.getChildText(RelationToInsured).equals("00")) {
//					throw new MidplatException("受益人和被保人的关系不能为本人!");
//				}
//			}
//		}
		
		cLogger.info("Out WxContInsuredIntlBL.checkContInsuredIntlBL()!");
	}
	
	private VData getContInsuredIntlBLVData(LCInsuredTable pLCInsured) {
		cLogger.info("Into WxContInsuredIntlBL.getContInsuredIntlBLVData()...");

		//被保人
		LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
		mLCInsuredSchema.setName(pLCInsured.getName());
		mLCInsuredSchema.setSex(pLCInsured.getSex());
		mLCInsuredSchema.setBirthday(pLCInsured.getBirthday());
		mLCInsuredSchema.setIDType(pLCInsured.getIDType());
		mLCInsuredSchema.setIDNo(pLCInsured.getIDNo());
		mLCInsuredSchema.setOccupationCode(pLCInsured.getOccupationCode());
		mLCInsuredSchema.setAuthorization(pLCInsured.getAuthorization());//授权标记
		if("RY0004".equals(mMsgType)){
			String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + pLCInsured.getOccupationCode() + "' with ur";
			String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
			if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
				mLCInsuredSchema.setOccupationType("1");  //职业类别为空的话，将职业类别置为1
			}else{
				mLCInsuredSchema.setOccupationType(mOccupationTypeStr);	//职业类别;
			}
		}else{
			mLCInsuredSchema.setOccupationType("1");	//职业类别; 按责任计算，试算时给默认值，对应责任默认的算费可能会用到。
		}
		mLCInsuredSchema.setPrtNo(cLCContSchema.getPrtNo());
		mLCInsuredSchema.setContNo(cLCContSchema.getContNo());
		mLCInsuredSchema.setRelationToMainInsured(pLCInsured.getRelaToMain());
		mLCInsuredSchema.setRelationToAppnt(pLCInsured.getRelaToAppnt());
		mLCInsuredSchema.setInsuredNo(pLCInsured.getInsuredNo());
		mLCInsuredSchema.setNativeCity(syOtherTable.getNativeCity());
		mLCInsuredSchema.setNativePlace(syOtherTable.getNativePlace());
		mLCInsuredSchema.setMarriage(syOtherTable.getMarriage());
		mLCInsuredSchema.setSalary(syOtherTable.getSalary());
		mLCInsuredSchema.setRgtAddress(syOtherTable.getRgtAddress());
		mLCInsuredSchema.setIDEndDate(syOtherTable.getIDEnddate());
		mLCInsuredSchema.setIDStartDate(syOtherTable.getIDStartdate());
		mLCInsuredSchema.setDegree(syOtherTable.getDegree());

		//被保人地址
		LCAddressSchema mInsuredAddress = new LCAddressSchema();
		mInsuredAddress.setHomeAddress(pLCInsured.getHomeAddress());
		mInsuredAddress.setPostalAddress(pLCInsured.getMailAddress());
		mInsuredAddress.setZipCode(pLCInsured.getMailZipCode());
		mInsuredAddress.setPhone(pLCInsured.getHomePhone());
//		mInsuredAddress.setHomeZipCode(pLCInsured.geth);
		mInsuredAddress.setHomePhone(pLCInsured.getHomePhone());
//		mInsuredAddress.setCompanyPhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setMobile(pLCInsured.getInsuredMobile());
		mInsuredAddress.setEMail(pLCInsured.getEmail());
		mInsuredAddress.setGrpName(syOtherTable.getGrpName());
		mInsuredAddress.setPostalProvince(tSpareFieldTable.getInsuredPostalProvince());
		mInsuredAddress.setPostalCity(tSpareFieldTable.getInsuredPostalCity());
		mInsuredAddress.setPostalCounty(tSpareFieldTable.getInsuredPostalCounty());
		mInsuredAddress.setPostalStreet(tSpareFieldTable.getInsuredPostalStreet());
		mInsuredAddress.setPostalCommunity(tSpareFieldTable.getInsuredPostalCommunity());

		//被保人个人信息
		LDPersonSchema mInsuredPerson = new LDPersonSchema();
		mInsuredPerson.setCustomerNo("");	//注意此处必须设置！picch核心判断CustomerNo为""时才进行被保人五要素判断，CustomerNo为null时会直接生成新客户号
		mInsuredPerson.setName(mLCInsuredSchema.getName());
		mInsuredPerson.setSex(mLCInsuredSchema.getSex());
		mInsuredPerson.setIDNo(mLCInsuredSchema.getIDNo());
		mInsuredPerson.setIDType(mLCInsuredSchema.getIDType());
		mInsuredPerson.setBirthday(mLCInsuredSchema.getBirthday());
		mInsuredPerson.setOccupationCode(mLCInsuredSchema.getOccupationCode());
		mInsuredPerson.setOccupationType(mLCInsuredSchema.getOccupationType());	//职业类别
		mInsuredPerson.setNativeCity(syOtherTable.getNativeCity());
		mInsuredPerson.setRgtAddress(syOtherTable.getRgtAddress());
		mInsuredPerson.setMarriage(syOtherTable.getMarriage());
		mInsuredPerson.setNativePlace(syOtherTable.getNativePlace());
		mInsuredPerson.setSalary(syOtherTable.getSalary());
		mInsuredPerson.setGrpName(syOtherTable.getGrpName());
		mInsuredPerson.setGrpNo(syOtherTable.getGrpNo());
		mInsuredPerson.setAuthorization(pLCInsured.getAuthorization());//授权标记
		
		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setSchema(mLCInsuredSchema);

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("FamilyType", "0");
		mTransferData.setNameAndValue("PolTypeFlag", "0");
		mTransferData.setNameAndValue("SequenceNo", pLCInsured.getInsuredNo());
		mTransferData.setNameAndValue("ContType", "1");
		mTransferData.setNameAndValue("SavePolType", "0");
		mTransferData.setNameAndValue("mMsgType", mMsgType);
		mTransferData.setNameAndValue("InsuredAuth", pLCInsured.getAuthorization());//授权标记

		//套餐信息
//		Element mRisk = pLCInsured.getChild(Risks).getChild(Risk);
//		mSQL = "select RiskWrapCode from LDRiskWrap where RiskCode='" + mRisk.getChildText(MainRiskCode) + "' with ur";
		
		LDRiskDutyWrapDB mLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
		mLDRiskDutyWrapDB.setRiskWrapCode(cWrapTable.getRiskWrapCode());
		LDRiskDutyWrapSet mLDRiskDutyWrapSet = mLDRiskDutyWrapDB.query();
		LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
		for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++) {
			LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet.get(i);
			if(cDutyCodeList.contains(tLDRiskDutyWrapSchema.getDutyCode())){
				LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
				tLCRiskDutyWrapSchema.setRiskWrapCode(tLDRiskDutyWrapSchema.getRiskWrapCode());
				tLCRiskDutyWrapSchema.setRiskCode(tLDRiskDutyWrapSchema.getRiskCode());
				tLCRiskDutyWrapSchema.setDutyCode(tLDRiskDutyWrapSchema.getDutyCode());
				tLCRiskDutyWrapSchema.setCalFactor(tLDRiskDutyWrapSchema.getCalFactor());
				tLCRiskDutyWrapSchema.setCalFactorType(tLDRiskDutyWrapSchema.getCalFactorType());
				for(int j = 1;j <= cWrapParamList.size();j++){
					WrapParamTable tWrapParamTable = (WrapParamTable)cWrapParamList.get(j-1);
					
					if(tLDRiskDutyWrapSchema.getRiskWrapCode().equals(tWrapParamTable.getRiskWrapCode()) 
							&& tLDRiskDutyWrapSchema.getRiskCode().equals(tWrapParamTable.getRiskCode())
							&& tLDRiskDutyWrapSchema.getDutyCode().equals(tWrapParamTable.getDutyCode()) 
							&& tLDRiskDutyWrapSchema.getCalFactor().equals(tWrapParamTable.getCalfactor())){
						if("1".equals(tLDRiskDutyWrapSchema.getCalFactorType()) 
								&& (!"".equals(tWrapParamTable.getCalfactorValue()))
								&& (!tLDRiskDutyWrapSchema.getCalFactorValue().equals(tWrapParamTable.getCalfactorValue()))){
							System.out.println(tWrapParamTable.getCalfactorValue());
							System.out.println(tWrapParamTable.getCalfactor());
							Errors = "算费要素与产品描述不符，请核实！";
							return null;
							
						}
						tLCRiskDutyWrapSchema.setCalFactorValue(tWrapParamTable.getCalfactorValue());
					}
				}
				tLCRiskDutyWrapSchema.setInsuredNo(pLCInsured.getInsuredNo());//虚拟客户号码，避免后面生成客户号码。
				mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
			}
		}
		System.out.println("需要算费的算费参数："+mLCRiskDutyWrapSet.size());
		//受益人
		LCBnfSet mLCBnfSet = new LCBnfSet();
		if(cLCBnfList != null && cLCBnfList.size()>0){
			for (int i = 0; i < cLCBnfList.size(); i++) {
				LCBnfTable tLCBnfTable = (LCBnfTable) cLCBnfList.get(i);
				if(pLCInsured.getInsuredNo().equals(tLCBnfTable.getInsuredNo())){
					LCBnfSchema tLCBnfSchema = new LCBnfSchema();
					tLCBnfSchema.setContNo(cLCContSchema.getContNo());
					tLCBnfSchema.setBnfType(tLCBnfTable.getBnfType());
					tLCBnfSchema.setBnfGrade(tLCBnfTable.getBnfGrade());
					tLCBnfSchema.setName(tLCBnfTable.getName());
					tLCBnfSchema.setBirthday(tLCBnfTable.getBirthday());
					tLCBnfSchema.setSex(tLCBnfTable.getSex());
					tLCBnfSchema.setIDType(tLCBnfTable.getIDType());
					tLCBnfSchema.setIDNo(tLCBnfTable.getIDNo());
					tLCBnfSchema.setRelationToInsured(tLCBnfTable.getRelationToInsured());
					tLCBnfSchema.setBnfLot(tLCBnfTable.getBnfLot());
					/*
					 * 核心要求的受益比例在0.0-1.0之间，而标准报文的受益比例为1-100，所以需要在此做转换
					 */
					tLCBnfSchema.setBnfLot(tLCBnfSchema.getBnfLot()/100.0);
					mLCBnfSet.add(tLCBnfSchema);
				}
			}
		}

		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(cLCContSchema);
		mVData.add(mLCInsuredSchema);
		mVData.add(mLCInsuredDB);
		mVData.add(mInsuredAddress);
		mVData.add(mInsuredPerson);
		mVData.add(mTransferData);
		mVData.add(mLCRiskDutyWrapSet);
		mVData.add(mLCBnfSet);
		mVData.add(new LCDiseaseResultSet());	//告知信息
		mVData.add(new LCNationSet());	//抵达国家
		mVData.add(cLcCustomerImpartSet);
		mVData.add(cLcCustomerImpartDetailSet);

		cLogger.info("Out WxContInsuredIntlBL.getContInsuredIntlBLVData()!");
		return mVData;
	}
	
	private LCContSchema getLCContSchema(){
		cLogger.info("Into WXContBL.getLCContSchema()...");
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		
		LCContSchema mLCContSchema = new LCContSchema();
		mLCContSchema.setGrpContNo("00000000000000000000");
//		mLCContSchema.setContNo(mLCCont.getChildText(ProposalContNo));	//用保单合同印刷号录单
		mLCContSchema.setContNo(cLCContTable.getContNo());	//需考虑如何生成保单号
		mLCContSchema.setProposalContNo("");
//		mLCContSchema.setPrtNo(mLCCont.getChildText(PrtNo));
		if(!"".equals(mPrtno) && mPrtno !=null){
			mLCContSchema.setPrtNo(mPrtno);
		}else{
			mLCContSchema.setPrtNo("10000000001");//以后会动态获取该号码，并一直做为该保单唯一标识
		}
		mLCContSchema.setContType("1");	//保单类别(个单-1，团单-2)
		mLCContSchema.setPolType("0");
		mLCContSchema.setCardFlag("0");	//渠道"04"，CardFlag"9"，共同识别银保通交易！
		mLCContSchema.setManageCom(cLCContTable.getManageCom());
		mLCContSchema.setAgentCom(cLCContTable.getAgentCom());
		mLCContSchema.setAgentCode(cLCContTable.getAgentCode());
		mLCContSchema.setAgentGroup("");
		mLCContSchema.setSaleChnl(cLCContTable.getSaleChnl());	//渠道"04"，CardFlag"9"，共同识别银保通交易！
		mLCContSchema.setPassword("");	//保单密码
		mLCContSchema.setInputOperator(cMsgHead.getSendOperator());
		mLCContSchema.setInputDate(PubFun.getCurrentDate());
		mLCContSchema.setInputTime(PubFun.getCurrentTime());
		//投保人
		mLCContSchema.setAppntName(cLCAppntTable.getAppntName());
		mLCContSchema.setAppntSex(cLCAppntTable.getAppntSex());
		mLCContSchema.setAppntBirthday(cLCAppntTable.getAppntBirthday());
		mLCContSchema.setAppntIDType(cLCAppntTable.getAppntIDType());
		mLCContSchema.setAppntIDNo(cLCAppntTable.getAppntIDNo());
		//被保人
//		mLCContSchema.setInsuredName(mLCInsured.getChildText(Name));
//		mLCContSchema.setInsuredSex(mLCInsured.getChildText(Sex));
//		mLCContSchema.setInsuredBirthday(mLCInsured.getChildText(Birthday));
//		mLCContSchema.setInsuredIDType(mLCInsured.getChildText(IDType));
//		mLCContSchema.setInsuredIDNo(mLCInsured.getChildText(IDNo));
		mLCContSchema.setPayIntv(cLCContTable.getPayIntv());	//缴费方式
		mLCContSchema.setPayMode("4");
		mLCContSchema.setPayLocation("0");	//银行转帐
//		if ((null==mLCContSchema.getBankCode()) || mLCContSchema.getBankCode().equals("")) {
//			mLCContSchema.setBankCode(mBaseInfo.getChildText(BankCode));
//		}
//		mLCContSchema.setBankAccNo(mLCCont.getChildText(BankAccNo));	//投保帐户号
//		mLCContSchema.setAccName(mLCCont.getChildText(AccName));	//投保帐户姓名
		mLCContSchema.setPrintCount(0);
//		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent));
		
		
		GregorianCalendar mNowCalendar = new GregorianCalendar();
		//保单生效日，如果复杂网销传生效日期，就取传的；如果不传，就取当前日期的下一天
		String tCValidate = cLCContTable.getCValiDate();
		if(tCValidate==null || tCValidate.equals("") || tCValidate.equals("null"))
		{
			mNowCalendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
			mLCContSchema.setCValiDate(mNowCalendar.getTime());	//保单生效日期(当前日期的下一天)
		}
		else
		{
			mLCContSchema.setCValiDate(tCValidate);
		}
		mLCContSchema.setPolApplyDate(mCurrentDate);	//投保日期
		mLCContSchema.setProposalType("01");	//投保书类型：01-普通个单投保书
//		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent));	//特别约定
		
		return mLCContSchema;
	}
	
	public double getPrem(){
		return mPrem;
	}
	public double getAmnt(){
		return mAmnt;
	}
	public LCDutySet getLCDutySet(){
		return cLCDutySet;
	}
	public LCPolSet getLCPolSet(){
		return cLCPolSet;
	}
	public LCRiskDutyWrapSet getLCRiskDutyWrapSet(){
		return cLCRiskDutyWrapSet;
	}
	
	private void getDutyCodeList(){
		cDutyCodeList = new ArrayList();
		for(int i = 0;i<cWrapParamList.size();i++){
			WrapParamTable tWrapParamTable = (WrapParamTable)cWrapParamList.get(i);
			String DutyCode = tWrapParamTable.getDutyCode();
			if(!cDutyCodeList.contains(DutyCode)){
				cDutyCodeList.add(DutyCode);
			}
		}
	}
	
	private boolean getRiskCodeList(){
		List cRiskCodeList = new ArrayList();
		for(int i = 0;i<cWrapParamList.size();i++){
			WrapParamTable tWrapParamTable = (WrapParamTable)cWrapParamList.get(i);
			String RiskCode = tWrapParamTable.getRiskCode();
			if(!cRiskCodeList.contains(RiskCode)){
				cRiskCodeList.add(RiskCode);
			}
		}
		boolean b = true;
		for(int j=0;j<cRiskCodeList.size();j++){
			String tsql = "select 1 from dual where '"+cRiskCodeList.get(j)+"' in (select distinct riskcode from lmriskapp where taxoptimal='Y')";
			String t1 = new ExeSQL().getOneValue(tsql);
			if ((null==t1) || t1.equals("")){
				b = false;
			}
		}
		return b;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
//		String mInFile = "D:/request/ICBC_std/UW.xml";
		
//		FileInputStream mFis = new FileInputStream(mInFile);
//		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
//		Document mXmlDoc = new SAXBuilder().build(mIsr);
//		
//		Element mTranData = mXmlDoc.getRootElement();
//		Element mBaseInfo = mTranData.getChild("BaseInfo");
//		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
//		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
//		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
//		mLCCont.getChild("PrtNo").setText(mProposalNo);
//		mLCCont.getChild("ProposalContNo").setText(mPrtNo);
//		
//		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
//				mBaseInfo.getChildText("BankCode"),
//				mBaseInfo.getChildText("ZoneNo"),
//				mBaseInfo.getChildText("BrNo"));
//		
//		new WxContInsuredIntlBL(mXmlDoc, mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}

