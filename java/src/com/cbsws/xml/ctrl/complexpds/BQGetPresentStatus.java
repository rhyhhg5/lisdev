package com.cbsws.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.obj.EdorGRPLQQueryInfo;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.EdorGRPLQAmountInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;



public class BQGetPresentStatus extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public EdorGRPLQQueryInfo mEdorGRPLQQueryInfo;

	public EdorGRPLQAmountInfo EdorGRPLQAmountInfo;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息
	
	private String mEdorAcceptNo = ""; //保全受理号
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始调用查询接口");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try {
			
			List tEdorGRPLQQueryInfoList = cMsgInfos.getBodyByFlag("EdorGRPLQQueryInfo");
			if (tEdorGRPLQQueryInfoList == null || tEdorGRPLQQueryInfoList.size() != 1) {
				errLog("获取工单信息失败。");
				return false;
			}
			mEdorGRPLQQueryInfo = (EdorGRPLQQueryInfo) tEdorGRPLQQueryInfoList.get(0); // 获取保单信息，只有一个保单
			mEdorAcceptNo = mEdorGRPLQQueryInfo.getEdoracceptno();
			if("".equals(mEdorAcceptNo)||mEdorAcceptNo == null){
				errLog("保全受理号有误，请检查");
				return false;
			}
			
			String selectNO = "select 1 from LPGrpEdorItem item , LPEdorApp app where app.edoracceptno = '"+mEdorAcceptNo+"' and app.edoracceptno = item.edoracceptno and item.edortype = 'LQ' and app.edorstate = '0'";
			String tSSRSNO = new ExeSQL().getOneValue(selectNO);
			if (!"1".equals(tSSRSNO)) {
				errLog("保全受理号无领取记录，请检查");
				return false;
			}
			
//			String GetPresentStatus = "select l.GetMoney , l.BankCode , l.Bankaccno from ljfiget l , ldbank b where OtherNo = '"+mEdorAcceptNo+"' and l.BankCode = b.BankCode";
			String GetPresentStatus = "select l.GetMoney , l.BankCode , l.Bankaccno from ljfiget l  where OtherNo = '"+mEdorAcceptNo+"'";
			SSRS ReturnPresentStatus = new ExeSQL().execSQL(GetPresentStatus);
			if (ReturnPresentStatus.getMaxRow() < 1) {
				String GetPresentStatus_ljaget = "select SumGetMoney , BankCode , BankAccNo from ljaget where otherno = '"+mEdorAcceptNo+"'";
				SSRS ReturnPresentStatus_ljaget = new ExeSQL().execSQL(GetPresentStatus_ljaget);
				if(ReturnPresentStatus_ljaget.getMaxRow() < 1){
					errLog("未查询到此保全号提现状态！");
					return false;
				}else{
					String bankcardno = ReturnPresentStatus_ljaget.GetText(1, 2);
					String bankname = ReturnPresentStatus_ljaget.GetText(1, 3);
					String status = "1";
					String amount = ReturnPresentStatus_ljaget.GetText(1, 1);
					PrepareInfo( bankcardno ,  bankname ,  status ,  amount);
				}
			}else{
				String bankcardno = ReturnPresentStatus.GetText(1, 2);
				String bankname = ReturnPresentStatus.GetText(1, 3);
				String status = "0";
				String amount = ReturnPresentStatus.GetText(1, 1);
				PrepareInfo( bankcardno ,  bankname ,  status ,  amount);
			}
			getXmlResult();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}
	
	// 返回报文
		public void getXmlResult() {

			// 返回数据节点
			putResult("EdorGRPLQAmountInfo", EdorGRPLQAmountInfo);

		}

		// 获取保单信息
		public boolean PrepareInfo(String bankcardno , String bankname , String status , String amount) {
			EdorGRPLQAmountInfo = new EdorGRPLQAmountInfo();
			EdorGRPLQAmountInfo.setEdorAcceptNo(mEdorAcceptNo);
			EdorGRPLQAmountInfo.setBankcardno(bankcardno);
			EdorGRPLQAmountInfo.setBankname(bankname);
			EdorGRPLQAmountInfo.setStatus(status);
			EdorGRPLQAmountInfo.setAmount(amount);
			return true;
		}
}
