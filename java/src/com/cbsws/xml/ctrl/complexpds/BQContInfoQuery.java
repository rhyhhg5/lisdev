package com.cbsws.xml.ctrl.complexpds;

import java.util.Iterator;
import java.util.List;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.ContQueryInfo; 
import com.cbsws.obj.EdorAppntInfo;
import com.cbsws.obj.EdorBnfInfo;
import com.cbsws.obj.EdorContInfo;
import com.cbsws.obj.EdorInsuInfo;
import com.cbsws.obj.EdorRiskInfo;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public class BQContInfoQuery extends ABusLogic{

	public String mContNo = "";
	public String mPrtNo = "";
	public String mAgentCode = "";
	public String MsgType="";
	public String BatchNo = "";
	private ContQueryInfo mContQueryInfo =null;
	private EdorContInfo mEdorContInfo =null;
	private EdorAppntInfo mEdorAppntInfo =null;
	private EdorInsuInfo mEdorInsuredInfo =null;
	private EdorRiskInfo mEdorRiskInfo =null;

	public CErrors mErrors = new CErrors();
	
	public BQContInfoQuery() {
		this.MsgType = "BQCONTINFOQUERY";
	}
	
    public boolean deal(MsgCollection cMsgInfos)  {
    	//解析XML
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkData()){
        	System.out.println("数据校验失败---");
        	//组织返回报文
			getWrapParmList("fail");
        	
        	return false;
        }
        
        //组织返回报文
        try {
			getWrapParmList("success");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;

        
    }
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口XML");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		//获取保单信息
		List mContQueryList = cMsgInfos.getBodyByFlag("ContQueryInfo");
        if (mContQueryList == null || mContQueryList.size() != 1)
        {
            errLog("申请报文中获取保单信息失败。");
            return false;
        }
        mContQueryInfo = (ContQueryInfo)mContQueryList.get(0);
        mContNo=mContQueryInfo.getContNo();
        mPrtNo=mContQueryInfo.getPrtNo();
        mAgentCode=mContQueryInfo.getAgentCode();
		return true;
	}
	
	public boolean checkData(){
		String tsql="";	
		if("".equals(mAgentCode) || mAgentCode == null){
            errLog("业务员代码不能为空。");
            return false;
		}

		if(!"".equals(mContNo) && mContNo != null){
			tsql+=" and a.contno='"+mContNo+"'";
		}
	    if (!"".equals(mPrtNo) && mPrtNo != null){
			tsql+=" and a.prtno='"+mPrtNo+"'";
	    }
		if(("".equals(mContNo) || mContNo == null)
				&& ("".equals(mPrtNo) || mPrtNo == null))
		{
			errLog("保单号和印刷号不能同时为空");
			return false;
		}
		String contsql = "select 1  from lccont a where "
			+ "	a.conttype='1' "
			+ " and (a.agentcode='"+ mAgentCode + "' or a.agentcode=(select agentcode from laagent where groupagentcode='"+mAgentCode+"')) " 
			+ tsql
			+ " with ur ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS contResult=tExeSQL.execSQL(contsql);
		if(contResult.MaxRow==0){
			errLog("保单查询失败，请检查保单号、印刷号或者业务员代码是否正确");
			return false;
		}
		return true;
	}
	
	public boolean getWrapParmList(String tFlag) {
		if("success".equals(tFlag)){
			String tsql="";
			if(!"".equals(mContNo) && mContNo != null){
				tsql+=" and a.contno='"+mContNo+"'";
			}else if (!"".equals(mPrtNo) && mPrtNo != null){
				tsql+=" and a.prtno='"+mPrtNo+"'";
			}else {
				errLog("保单号和印刷号不能同时为空");
				return false;
			}
			String contdatasql = "select a.contno as 保单号,a.stateflag as 保单状态, a.paymode as 缴费方式,"
					+ " a.bankcode 开户银行,a.BankAccNo 银行账号,a.accname 开户名,"
					+ " b.LetterServiceName 管理机构名称,b.fax 管理机构传真,b.ServicePhone 客户服务电话,"
					+ " b.ServicePostAddress 客户服务投递地址, b.ServicePostZipcode 客户服务投递邮编   "
//					+ " b.LetterServicePhone 信函服务电话,b.LetterServicePostAddress 信函服务投递地址,b.LetterServicePostZipcode 信函服务投递邮编  "
					+ " from lccont a,ldcom b where "
					+ "	a.conttype='1' "
					+ "	and a.managecom=b.comcode  "
					+ " and (a.agentcode='"+ mAgentCode + "' or a.agentcode=(select agentcode from laagent where groupagentcode='"+mAgentCode+"')) " 
					+ tsql
					+ " with ur ";

			String appntdatasql = "select b.Name as 投保人姓名,"
				+ " b.customerno as 投保人客户号,"
//				+ " b.IDType as 证件类型,"
				+ " b.IDNo as 证件号码,"
				+ " c.HomeAddress as 家庭地址," 
				+ " c.HomeZipCode as 家庭邮编," 
				+ " c.HomePhone as 家庭电话,"
				+ " c.HomeFax as 家庭传真,"
				+ " c.CompanyAddress as 单位地址,"
				+ " c.CompanyZipCode as 单位邮编,"
				+ " c.CompanyPhone as 单位联系电话,"
				+ " c.CompanyFax as 单位传真,"
				+ " c.PostalAddress as 联系住址,"
				+ " c.ZipCode as 联系邮编,"
				+ " c.Phone as 联系电话,"
				+ " c.Fax as 通讯传真,"
				+ " c.Mobile as 移动电话,"
				+ " c.EMail as 电子邮箱,"
				+ " a.AppntSex as 投保人性别,"
				+ " a.AppntBirthday as 投保人出生日期  "
				+ " from LCAppnt a,LDPerson b,LCAddress c "
				+ " where 1=1 " 
				+ tsql
				+ " and b.CustomerNo=a.AppntNo "
				+ " and b.CustomerNo = c.CustomerNo "
				+ " and a.AddressNo=c.AddressNo with ur ";
		
		
			String insureddatasql = "select b.Name as 被保人姓名,"
				+ " b.customerno as 被保人客户号,"
				+ " b.IDNo as 证件号码,"
				+ " c.HomeAddress as 家庭地址," 
				+ " c.HomeZipCode as 家庭邮编," 
				+ " c.HomePhone as 家庭电话,"
				+ " c.HomeFax as 家庭传真,"
				+ " c.CompanyAddress as 单位地址,"
				+ " c.CompanyZipCode as 单位邮编,"
				+ " c.CompanyPhone as 单位联系电话,"
				+ " c.CompanyFax as 单位传真,"
				+ " c.PostalAddress as 联系住址,"
				+ " c.ZipCode as 联系邮编,"
				+ " c.Phone as 联系电话,"
				+ " c.Fax as 通讯传真,"
				+ " c.Mobile as 移动电话,"
				+ " c.EMail as 电子邮箱,"
				+ " a.Sex as 被保人性别,"
				+ " a.Birthday as 被保人出生日期  "
				+ " from LCInsured a,LDPerson b,LCAddress c "
				+ " where 1=1 " 
				+ tsql 
				+ " and b.CustomerNo=a.insuredNo "
				+ "and b.CustomerNo = c.CustomerNo "
				+ "and a.AddressNo=c.AddressNo with ur ";

			String riskcodesql = "select a.InsuredNo as 被保人客户号,a.riskcode 险种代码 ,b.riskname as 险种名称,"
				+ "a.Prem as 保费,a.Amnt as 保额,a.CVALIDATE as 生效日期,a.EndDate as 保险止期,"
				+ "a.payintv as 缴费年期,a.paytodate as 交至日期  "
				+ " from LCPol a inner join lmrisk b on b.RiskCode=a.RiskCode where 1=1" 
				+ tsql 
				+ " with ur ";
		
			String bnflistsql = "select b.Name as 受益人姓名 ,a.InsuredNo as 被保人客户号,a.riskcode as 险种代码  "
				+ " from lcpol a,lcbnf b where  a.contno=b.contno and a.polno=b.polno " 
				+ tsql
				+ " with ur ";
		
		// 1)保单信息 CONTDATA:contdatasql
		// 2)投保人信息  APPNTDATA:appntdatasql
		// 3)被保人信息 INSUREDDATA:insureddatasql
		// 4)保单险种信息（集合）RISKLIST:riskcodesql


			ExeSQL tExeSQL = new ExeSQL();
			SSRS contdataResult;
			SSRS insureddataResult;
			SSRS appntdataResult;
			SSRS riskcodeResult;
			SSRS bnfResult;

			contdataResult = tExeSQL.execSQL(contdatasql);
			appntdataResult = tExeSQL.execSQL(appntdatasql);
			insureddataResult = tExeSQL.execSQL(insureddatasql);
			riskcodeResult = tExeSQL.execSQL(riskcodesql);
		    bnfResult = tExeSQL.execSQL(bnflistsql);

			if (contdataResult.getMaxRow() > 0) {
				EdorContInfo tEdorContInfo=new EdorContInfo();
				tEdorContInfo.setContNo(contdataResult.GetText(1, 1));
				tEdorContInfo.setStateFlag(contdataResult.GetText(1, 2));
				tEdorContInfo.setPayMode(contdataResult.GetText(1, 3));
				tEdorContInfo.setBankCode(contdataResult.GetText(1, 4));
				tEdorContInfo.setBankAccNo(contdataResult.GetText(1, 5));
				tEdorContInfo.setAccName(contdataResult.GetText(1, 6));
				tEdorContInfo.setManageComName(contdataResult.GetText(1, 7));
				tEdorContInfo.setServiceFax(contdataResult.GetText(1, 8));
				tEdorContInfo.setServicePhone(contdataResult.GetText(1, 9));
				tEdorContInfo.setServicePostAddress(contdataResult.GetText(1, 10));
				tEdorContInfo.setServicePostZipcode(contdataResult.GetText(1, 11));
				putResult("EdorContInfo",tEdorContInfo);
			}else{
				EdorContInfo tEdorContInfo=new EdorContInfo();
				tEdorContInfo.setContNo("");
				tEdorContInfo.setStateFlag("");
				tEdorContInfo.setPayMode("");
				tEdorContInfo.setBankCode("");
				tEdorContInfo.setBankAccNo("");
				tEdorContInfo.setAccName("");
				tEdorContInfo.setManageComName("");
				tEdorContInfo.setServiceFax("");
				tEdorContInfo.setServicePhone("");
				tEdorContInfo.setServicePostAddress("");
				tEdorContInfo.setServicePostZipcode("");
				putResult("EdorContInfo",tEdorContInfo);
			}
			
			if (appntdataResult.getMaxRow() > 0) {
				EdorAppntInfo tEdorAppntInfo =new EdorAppntInfo();
			
				tEdorAppntInfo.setAppntName(appntdataResult.GetText(1, 1));
				tEdorAppntInfo.setAppntNo(appntdataResult.GetText(1, 2));
				tEdorAppntInfo.setIdNo(appntdataResult.GetText(1, 3));
				tEdorAppntInfo.setHomeAddress(appntdataResult.GetText(1, 4));
				tEdorAppntInfo.setHomeZipCode(appntdataResult.GetText(1, 5));
				tEdorAppntInfo.setHomePhone(appntdataResult.GetText(1, 6));
				tEdorAppntInfo.setHomeFax(appntdataResult.GetText(1, 7));
				tEdorAppntInfo.setCompanyAddress(appntdataResult.GetText(1, 8));
				tEdorAppntInfo.setCompanyZipCode(appntdataResult.GetText(1, 9));
				tEdorAppntInfo.setCompanyPhone(appntdataResult.GetText(1, 10));
				tEdorAppntInfo.setCompanyFax(appntdataResult.GetText(1, 11));
				tEdorAppntInfo.setPostalAddress(appntdataResult.GetText(1, 12));
				tEdorAppntInfo.setZipCode(appntdataResult.GetText(1, 13));
				tEdorAppntInfo.setPhone(appntdataResult.GetText(1, 14));
				tEdorAppntInfo.setFax(appntdataResult.GetText(1, 15));
				tEdorAppntInfo.setMobile(appntdataResult.GetText(1, 16));
				tEdorAppntInfo.setEMail(appntdataResult.GetText(1, 17));
				tEdorAppntInfo.setAppntBirthday(appntdataResult.GetText(1, 19));
				tEdorAppntInfo.setSex(appntdataResult.GetText(1, 18));

			
				putResult("EdorAppntInfo",tEdorAppntInfo);
			}else{
				EdorAppntInfo tEdorAppntInfo =new EdorAppntInfo();
				
				tEdorAppntInfo.setAppntName("");
				tEdorAppntInfo.setAppntNo("");
				tEdorAppntInfo.setIdNo("");
				tEdorAppntInfo.setHomeAddress("");
				tEdorAppntInfo.setHomeZipCode("");
				tEdorAppntInfo.setHomePhone("");
				tEdorAppntInfo.setHomeFax("");
				tEdorAppntInfo.setCompanyAddress("");
				tEdorAppntInfo.setCompanyZipCode("");
				tEdorAppntInfo.setCompanyPhone("");
				tEdorAppntInfo.setCompanyFax("");
				tEdorAppntInfo.setPostalAddress("");
				tEdorAppntInfo.setZipCode("");
				tEdorAppntInfo.setPhone("");
				tEdorAppntInfo.setFax("");
				tEdorAppntInfo.setMobile("");
				tEdorAppntInfo.setEMail("");
				tEdorAppntInfo.setAppntBirthday("");
				tEdorAppntInfo.setSex("");

			
				putResult("EdorAppntInfo",tEdorAppntInfo);
			}
		
			if (insureddataResult.getMaxRow() > 0) {
				for (int i = 1; i <= insureddataResult.MaxRow; i++){
					EdorInsuInfo tEdorInsuredInfo= new EdorInsuInfo();
					
					tEdorInsuredInfo.setInsuredName(insureddataResult.GetText(i, 1));
					tEdorInsuredInfo.setInsuredNo(insureddataResult.GetText(i, 2));
					tEdorInsuredInfo.setInsuredIdNo(insureddataResult.GetText(i, 3));
					tEdorInsuredInfo.setInsuredHomeAddress(insureddataResult.GetText(i, 4));
					tEdorInsuredInfo.setInsuredHomeZipCode(insureddataResult.GetText(i, 5));
					tEdorInsuredInfo.setInsuredHomePhone(insureddataResult.GetText(i, 6));
					tEdorInsuredInfo.setInsuredHomeFax(insureddataResult.GetText(i, 7));
					tEdorInsuredInfo.setInsuredCompanyAddress(insureddataResult.GetText(i, 8));
					tEdorInsuredInfo.setInsuredCompanyZipCode(insureddataResult.GetText(i, 9));
					tEdorInsuredInfo.setInsuredCompanyPhone(insureddataResult.GetText(i, 10));
					tEdorInsuredInfo.setInsuredCompanyFax(insureddataResult.GetText(i, 11));
					tEdorInsuredInfo.setInsuredPostalAddress(insureddataResult.GetText(i, 12));
					tEdorInsuredInfo.setInsuredZipCode(insureddataResult.GetText(i, 13));
					tEdorInsuredInfo.setInsuredPhone(insureddataResult.GetText(i, 14));
					tEdorInsuredInfo.setInsuredFax(insureddataResult.GetText(i, 15));
					tEdorInsuredInfo.setInsuredMobile(insureddataResult.GetText(i, 16));
					tEdorInsuredInfo.setInsuredEMail(insureddataResult.GetText(i, 17));
					tEdorInsuredInfo.setInsuredBirthday(insureddataResult.GetText(i, 19));
					tEdorInsuredInfo.setInsuredSex(insureddataResult.GetText(i, 18));
					
				
					putResult("EdorInsuredInfo",tEdorInsuredInfo);;
				}


			}else{
				EdorInsuInfo tEdorInsuredInfo= new EdorInsuInfo();
				
				tEdorInsuredInfo.setInsuredName("");
				tEdorInsuredInfo.setInsuredNo("");
				tEdorInsuredInfo.setInsuredIdNo("");
				tEdorInsuredInfo.setInsuredHomeAddress("");
				tEdorInsuredInfo.setInsuredHomeZipCode("");
				tEdorInsuredInfo.setInsuredHomePhone("");
				tEdorInsuredInfo.setInsuredHomeFax("");
				tEdorInsuredInfo.setInsuredCompanyAddress("");
				tEdorInsuredInfo.setInsuredCompanyZipCode("");
				tEdorInsuredInfo.setInsuredCompanyPhone("");
				tEdorInsuredInfo.setInsuredCompanyFax("");
				tEdorInsuredInfo.setInsuredPostalAddress("");
				tEdorInsuredInfo.setInsuredZipCode("");
				tEdorInsuredInfo.setInsuredPhone("");
				tEdorInsuredInfo.setInsuredFax("");
				tEdorInsuredInfo.setInsuredMobile("");
				tEdorInsuredInfo.setInsuredEMail("");
				tEdorInsuredInfo.setInsuredBirthday("");
				tEdorInsuredInfo.setInsuredSex("");
				
			
				putResult("EdorInsuredInfo",tEdorInsuredInfo);;
			}

			if (riskcodeResult.getMaxRow() > 0) {
				for (int i = 1; i <= riskcodeResult.MaxRow; i++) {
					EdorRiskInfo tEdorRiskInfo = new EdorRiskInfo();
					tEdorRiskInfo.setInsuredNo(riskcodeResult.GetText(i, 1));
					tEdorRiskInfo.setRiskCode(riskcodeResult.GetText(i, 2));
					tEdorRiskInfo.setRiskName(riskcodeResult.GetText(i, 3));
					tEdorRiskInfo.setPrem(riskcodeResult.GetText(i, 4));
					tEdorRiskInfo.setAmnt(riskcodeResult.GetText(i, 5));
					tEdorRiskInfo.setCValiDate(riskcodeResult.GetText(i, 6));
					tEdorRiskInfo.setEndDate(riskcodeResult.GetText(i, 7));
					tEdorRiskInfo.setPayIntv(riskcodeResult.GetText(i, 8));
					tEdorRiskInfo.setPayToDate(riskcodeResult.GetText(i, 9));
				
					putResult("EdorRiskInfo",tEdorRiskInfo);

				}
			}else{
				EdorRiskInfo tEdorRiskInfo = new EdorRiskInfo();
				tEdorRiskInfo.setInsuredNo("");
				tEdorRiskInfo.setRiskCode("");
				tEdorRiskInfo.setRiskName("");
				tEdorRiskInfo.setPrem("");
				tEdorRiskInfo.setAmnt("");
				tEdorRiskInfo.setCValiDate("");
				tEdorRiskInfo.setEndDate("");
				tEdorRiskInfo.setPayIntv("");
				tEdorRiskInfo.setPayToDate("");
			
				putResult("EdorRiskInfo",tEdorRiskInfo);
			}
		


			if(bnfResult.getMaxRow()>0){
				for (int i = 1; i <= bnfResult.MaxRow; i++) {
					EdorBnfInfo tEdorBnfInfo = new EdorBnfInfo();
					tEdorBnfInfo.setBnfName(bnfResult.GetText(i, 1));
					tEdorBnfInfo.setInsuredNo(bnfResult.GetText(i, 2));
					tEdorBnfInfo.setRiskCode(bnfResult.GetText(i, 3));
				
					putResult("EdorBnfInfo",tEdorBnfInfo);
				}
			}else {
					EdorBnfInfo tEdorBnfInfo = new EdorBnfInfo();
					tEdorBnfInfo.setBnfName("");
					tEdorBnfInfo.setInsuredNo("");
					tEdorBnfInfo.setRiskCode("");
				
					putResult("EdorBnfInfo",tEdorBnfInfo);
			}
		}else if("fail".equals(tFlag)){
    		
//			EdorContInfo tEdorContInfo=new EdorContInfo();
//			putResult("EdorBnfInfo",tEdorContInfo);
//			
//			EdorAppntInfo tEdorAppntInfo=new EdorAppntInfo();
//			putResult("EdorBnfInfo",tEdorAppntInfo);
//			
//			EdorInsuredInfo tEdorInsuredInfo=new EdorInsuredInfo();
//			putResult("EdorBnfInfo",tEdorInsuredInfo);
//			
//			EdorRiskInfo tEdorRiskInfo=new EdorRiskInfo();
//			putResult("EdorBnfInfo",tEdorRiskInfo);
//			
//			EdorBnfInfo tEdorBnfInfo=new EdorBnfInfo();
//			putResult("EdorBnfInfo",tEdorBnfInfo);
			

    	}
		return true;
	}
}
