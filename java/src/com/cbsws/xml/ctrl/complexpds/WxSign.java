package com.cbsws.xml.ctrl.complexpds;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.ECCertSalesPayInfoTable;
import com.cbsws.obj.LCAppntTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LCCustomerImpartTable;
import com.cbsws.obj.NewLCContTable;
import com.cbsws.obj.WrapParamTable;
import com.cbsws.obj.WrapTable;
import com.cbsws.xml.ctrl.blogic.WxSimplePrintBL;
import com.ecwebservice.epicc.EpiccEmailAndMessageBL;
import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LICertSalesPayInfoSchema;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class WxSign extends ABusLogic {
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public double mPrem=0;//保费
	public double mAmnt=0;//保额
	
	public LCContTable cLCContTable;
	public LCAppntTable cLCAppntTable;
	public List cLCInsuredList;
	public WrapTable cWrapTable;
	public List cLCBnfList;
	public List cWrapParamList;//套餐参数问题
	public ECCertSalesPayInfoTable cECCertSalesPayInfo;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	String cPrtno = "";//印刷号
	private String memail = "";
	private String mcontno = "";
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始处理网销复杂产品业务");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			//考虑已经承保但是对方没有收到返回报文的情况
			String chksql = "select a.prtno,a.contno,b.wrapcode " +
				"from lccont a ,LICertSalesPayInfo b where 1=1 " +
				"and  a.prtno = b.relainfolist and b.batchno = '"+BatchNo+"'" +
				"and a.appflag = '1'";
			System.out.println("chksql校验是否是已经承保的请求----" + chksql);
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(chksql);  
			if(tSSRS != null && tSSRS.MaxRow > 0){
				String tContno = tSSRS.GetText(1, 2);
				System.out.println("-------已经正常承保-----无需再次处理---保单号为：" + tContno);
				//封装成功状态
				MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
	            tResMsgHead.setState("00");
	            errLog("批次号：" + BatchNo + "，该请求已经处理并承保，对应的保单号：" + tContno);
	            //返回保单号
	            LCContTable contttable = null;
	            List contList = cMsgInfos.getBodyByFlag("LCContTable");
	            if(contList != null && contList.size() > 0){
	            	contttable = (LCContTable) contList.get(0);
	            	contttable.setContNo(tContno);
	            }
	            putResult("LCContTable", contttable);
	            getResult();
				return true;
			}
			
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
	        if (tLCContList == null || tLCContList.size() != 1)
	        {
	            errLog("获取保单信息失败。");
	            return false;
	        }
	        cLCContTable = (LCContTable) tLCContList.get(0); //获取保单信息，只有一个保单
	        mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();
			cPrtno = cLCContTable.getPrtNo();
			if (cPrtno == null || "".equals(cPrtno))
	        {
	            errLog("获取保单印刷号失败。");
	            return false;
	        }
			String sqlString="select uwflag from lccont where prtno='"+cPrtno+"'";
			tSSRS.Clear();
			tSSRS=tExeSQL.execSQL(sqlString);
			if(tSSRS.getMaxRow()!=0){
				String uwflag=tSSRS.GetText(1, 1);
				if("5".equals(uwflag)){
					errLog("保单为人工核保状态，禁止签单。");
		            return false;
				}
			}
			
			List tECCertSalesPayInfoList = cMsgInfos.getBodyByFlag("ECCertSalesPayInfoTable");
	        if (tECCertSalesPayInfoList == null || tECCertSalesPayInfoList.size() != 1)
	        {
	            errLog("获取交易信息失败。");
	            return false;
	        }
	        cECCertSalesPayInfo = (ECCertSalesPayInfoTable)tECCertSalesPayInfoList.get(0);
			//校验收费机构与保险公司开户账号
			String tempManageCom = cECCertSalesPayInfo.getTempManageCom();
	        String insBankAccNo = cECCertSalesPayInfo.getInsBankAccNo();
	        if( !"".equals(tempManageCom) && null != tempManageCom && !"86000000".equals(tempManageCom) ){
	        	String bankAcc = new ExeSQL().getOneValue("select 1 from ldfinbank where  managecom='"+tempManageCom+"' and bankaccno ='"+insBankAccNo+"'");
	        	if(!"1".equals(bankAcc)){
	        		System.out.println("WxSign====================================================保险公司开户账号与收费管理机构不符");
	        		mError ="保险公司开户账号与收费管理机构不符，请核实！";
	        		errLog(mError);
	        		return false;
	        	}
	        }
	        LICertSalesPayInfoSchema tLICertSalesPayInfoSchema = getSalesInfo();//封装交易信息
	        cWrapTable = new WrapTable();
	        //	        财务收费 暂收
	        //	        校验是否已生成暂收记录
	        String signSql = "select appflag from lccont where prtno = '"+cPrtno+"' ";
	        String Appflag = new ExeSQL().getOneValue(signSql);
	        if(!"1".equals(Appflag)){
	        	String tempfeeSql = "select 1 from LJTempFee where othernotype = '4' and otherno = '"+cPrtno+"' ";
		        String isTempFee = new ExeSQL().getOneValue(tempfeeSql);
		        if("1".equals(isTempFee)){//考虑暂收成功，但签单失败的情况
		        	System.out.println("已生成暂收，不需再次生成！");
		        }else{
		        	WxTempFeeBL ttWxTempFeeBL = new WxTempFeeBL(cLCContTable, mGlobalInput,tLICertSalesPayInfoSchema,cECCertSalesPayInfo);
					mError = ttWxTempFeeBL.deal();
					if(!"".equals(mError)){
						errLog(mError);
		                return false;
					}
		        }
//			     签单+单证核销+回执回销
				WxLCContSignBL ttWxLCContSignBL = new WxLCContSignBL(cPrtno, mGlobalInput,cECCertSalesPayInfo,tMsgHead);
				mError = ttWxLCContSignBL.deal();
				if(!"".equals(mError)){
					errLog(mError);
	                return false;
				}
	        }
	      //更新续保关联信息
	        String Sql1="select 1 from LCRnewStateLog where prtno='"+cLCContTable.getPrtNo()+"'";
	        SSRS SqlResult= new SSRS();
	        ExeSQL tSql1 = new ExeSQL();
	        SqlResult=tSql1.execSQL(Sql1);
	        if(SqlResult.MaxRow>0){
	        	NewSignBL tNewSignBL = new NewSignBL(cPrtno,cLCContTable);
		        mError = tNewSignBL.deal();
		        if(!"".equals(mError)){
					errLog(mError);
	                return false;
				}
	        }
//				//组织返回报文
			getWrapParmList(cWrapTable.getRiskWrapCode());
			if(!PrepareInfo()){
				errLog(mError);
                return false;
			}
			getXmlResult();
			
			String dzdbSql = "select code from ldcode where codetype='wx_jybd'";
			SSRS bdpSSRS = tExeSQL.execSQL(dzdbSql);
			if(bdpSSRS != null && bdpSSRS.MaxRow > 0){
				for(int k=1;k<=bdpSSRS.MaxRow;k++){
					String code = bdpSSRS.GetText(k, 1);
					System.out.println("code="+code);
					if(code != null && !"".equals(code) ){
						if(this.BatchNo.startsWith(code)){
							if("WX0002EC".equals(code)){
								EpiccEmailAndMessageBL tepicc = new EpiccEmailAndMessageBL();
								tepicc.sendSmsAndEmail(BatchNo,cPrtno);
								makepdf(BatchNo,cPrtno);
								return true;
							}else if("QEN".equals(code)){
								//发送短信通知
								this.sendMsg();
								
								//生成电子保单
								this.makePDF(cLCContTable.getContNo());
								
								//发送邮件通知 发送邮件起线程发送，让注流程返回（由于邮件附件需要时间生成）
								//启动额外线程发送邮件，让主线程继续运行
								MailtoCustomerThread mt = new MailtoCustomerThread();
								new Thread(mt).run();
								return true;
								//this.mailtoCustomer();
							}
							
						}
					}
				}
			}
			
		}catch(Exception ex){
			
			return false;
		}
		return true;
	}
	public void makepdf(String BatchNo,String PrtNo){
		WxPrintEPiccBL ecp = new WxPrintEPiccBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo(BatchNo);
		tMsgHead.setBranchCode("001");
		tMsgHead.setMsgType("WXP0001");
		tMsgHead.setSendDate(PubFun.getCurrentDate());
		tMsgHead.setSendTime(PubFun.getCurrentTime());
		tMsgHead.setSendOperator("sino");
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo(PrtNo);
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);		
		LCCustomerImpartTable tLCCustomerImpartTable = new LCCustomerImpartTable();
		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable);
		ecp.deal(tMsgCollection);	
	}
	//返回报文
	public void getXmlResult(){
		//保单节点
		NewLCContTable newLCContTable=replace(cLCContTable);
		putResult("LCContTable", newLCContTable);
		//投保人节点
//		putResult("LCAppntTable", cLCAppntTable);
		//所有被保人节点
//		for(int i=0;i<cLCInsuredList.size();i++){
//			putResult("LCInsuredTable", (LCInsuredTable)cLCInsuredList.get(i));
//		}
		//套餐节点
		putResult("WrapTable", cWrapTable);
		//算费参数节点
		for(int i=0;i<cWrapParamList.size();i++){
			putResult("WrapParamTable", (WrapParamTable)cWrapParamList.get(i));
		}
		//受益人节点
//		for(int i=0;i<cLCBnfList.size();i++){
//			putResult("LCBnfTable", (LCBnfTable)cLCBnfList.get(i));
//		}
	}
	public NewLCContTable replace(LCContTable mLcContTable){
		NewLCContTable newlc=new NewLCContTable();
		newlc.setAgentCode(mLcContTable.getAgentCode());
		newlc.setAgentCom(mLcContTable.getAgentCom());
		newlc.setCInValiDate(mLcContTable.getCInValiDate());
		newlc.setCInValiTime(mLcContTable.getCInValiTime());
		newlc.setContNo(mLcContTable.getContNo());
		newlc.setCValiDate(mLcContTable.getCValiDate());
		newlc.setCValiTime(mLcContTable.getCValiTime());
		newlc.setManageCom(mLcContTable.getManageCom());
		newlc.setPayIntv(mLcContTable.getPayIntv());
		newlc.setPayMode(mLcContTable.getPayMode());
		newlc.setPolApplyDate(mLcContTable.getPolApplyDate());
		newlc.setPrtNo(mLcContTable.getPrtNo());
		newlc.setSaleChnl(mLcContTable.getSaleChnl());		
		return newlc;
	}	
//	获取保单信息
	public boolean PrepareInfo(){
		String sql = "select contno,amnt,prem from lccont where prtno = '"+cPrtno+"'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if(tSSRS == null || tSSRS.getMaxRow()<=0 ){
			mError = "获取保单信息失败！";
            return false;
		}else{
			cWrapTable.setAmnt(tSSRS.GetText(1, 2));
			cWrapTable.setPrem(tSSRS.GetText(1, 3));
			cLCContTable.setContNo(tSSRS.GetText(1, 1));
		}
		//获取保险期间
		String InsuYearsql = "select insuyear,insuyearflag from lcpol where prtno = '"+cPrtno+"'";
		SSRS tInsuYearSSRS = new ExeSQL().execSQL(InsuYearsql);
		if(tInsuYearSSRS == null || tInsuYearSSRS.getMaxRow()<=0 ){
			mError = "获取保险期间信息失败！";
            return false;
		}else{
			cWrapTable.setInsuYear(tInsuYearSSRS.GetText(1, 1));
			cWrapTable.setInsuYearFlag(tInsuYearSSRS.GetText(1, 2));
		}
		return true;
	}
    
//    获取每个责任对应的保额及保费信息
    public void getWrapParmList(String pRiskWrapCode){
    	String contnoSql = "select contno from lccont where prtno = '"+cPrtno+"'";
    	String contno = new ExeSQL().getOneValue(contnoSql);
    	this.mcontno = contno;
    	String sql = "select a.riskcode,b.dutycode, a.amnt,b.amnt,a.prem,b.prem from lcpol a left join lcduty b on a.polno = b.polno where a.contno ='"+contno+"' ";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS!=null && tSSRS.getMaxRow()>0){
    		cWrapParamList = new ArrayList();
    		for(int i=1;i<=tSSRS.getMaxRow();i++){
    			for(int j=1;j<=2;j++){
    				WrapParamTable tWrapParamTable = new WrapParamTable();
					tWrapParamTable.setContNo(cLCContTable.getContNo());
					tWrapParamTable.setRiskWrapCode(pRiskWrapCode);
					tWrapParamTable.setRiskCode(tSSRS.GetText(i, 1));
					tWrapParamTable.setDutyCode(tSSRS.GetText(i, 2));
					if(j==1){
						tWrapParamTable.setCalfactor("Amnt");
						tWrapParamTable.setCalfactorValue(String.valueOf(tSSRS.GetText(i, 4)));
					}else{
						tWrapParamTable.setCalfactor("Prem");
						tWrapParamTable.setCalfactorValue(String.valueOf(tSSRS.GetText(i, 6)));
					}
					cWrapParamList.add(tWrapParamTable);
    			}
    		}
    	}
    }
    
    /**
     * 封装交易信息
     *
     * @return boolean
     */
    private LICertSalesPayInfoSchema getSalesInfo()
    {
    	LICertSalesPayInfoSchema aLICertSalesPayInfoSchema = new LICertSalesPayInfoSchema();
    	aLICertSalesPayInfoSchema.setBatchNo(BatchNo);
    	aLICertSalesPayInfoSchema.setPayTradeSeq(cECCertSalesPayInfo.getPayTradeSeq());
    	aLICertSalesPayInfoSchema.setManageCom(cECCertSalesPayInfo.getManageCom());
		aLICertSalesPayInfoSchema.setSaleChnl(cECCertSalesPayInfo.getSaleChnl());
		aLICertSalesPayInfoSchema.setSaleChnlName(cECCertSalesPayInfo.getSaleChnlName());
		aLICertSalesPayInfoSchema.setAgentCom(cECCertSalesPayInfo.getAgentCom());

//		String agentcode = cECCertSalesPayInfo.getAgentCode();
//		AgentCodeTransformation a = new AgentCodeTransformation();
//		if(!a.AgentCode(agentcode, "Y")){
//			System.out.println(a.getMessage());
//            errLog(a.getMessage());
//			return null;
//		}
//		agentcode = a.getResult();
//		System.out.println(agentcode);
//		
//		aLICertSalesPayInfoSchema.setAgentCode(agentcode);

		aLICertSalesPayInfoSchema.setAgentCode(cECCertSalesPayInfo.getAgentCode());
		aLICertSalesPayInfoSchema.setAgentName(cECCertSalesPayInfo.getAgentName());
		aLICertSalesPayInfoSchema.setCertifyCode(cECCertSalesPayInfo.getCertifyCode());
		aLICertSalesPayInfoSchema.setCertifyName(cECCertSalesPayInfo.getCertifyName());
		aLICertSalesPayInfoSchema.setWrapCode(cECCertSalesPayInfo.getWrapCode());
		aLICertSalesPayInfoSchema.setWrapName(cECCertSalesPayInfo.getWrapName());
		aLICertSalesPayInfoSchema.setSalesCount(cECCertSalesPayInfo.getSalesCount());
		aLICertSalesPayInfoSchema.setSalesDate(cECCertSalesPayInfo.getSalesDate());
		aLICertSalesPayInfoSchema.setPayMode(cECCertSalesPayInfo.getPayMode());
		aLICertSalesPayInfoSchema.setPayChnlType(cECCertSalesPayInfo.getPayChnlType());
		aLICertSalesPayInfoSchema.setPayChnlName(cECCertSalesPayInfo.getPayChnlName());
		aLICertSalesPayInfoSchema.setPayMoney(cECCertSalesPayInfo.getPayMoney());
		aLICertSalesPayInfoSchema.setPayDate(cECCertSalesPayInfo.getPayDate());
		aLICertSalesPayInfoSchema.setPayEntAccDate(cECCertSalesPayInfo.getPayEntAccDate());
		aLICertSalesPayInfoSchema.setPayConfAccDate(cECCertSalesPayInfo.getPayConfAccDate());
		aLICertSalesPayInfoSchema.setDescBankCode(cECCertSalesPayInfo.getDescBankCode());
		aLICertSalesPayInfoSchema.setDescBankName(cECCertSalesPayInfo.getDescBankName());
		aLICertSalesPayInfoSchema.setDescBankAccCode(cECCertSalesPayInfo.getDescBankAccCode());
		aLICertSalesPayInfoSchema.setDescBankAccName(cECCertSalesPayInfo.getDescBankAccName());
		aLICertSalesPayInfoSchema.setRemark(cECCertSalesPayInfo.getRemark());
		aLICertSalesPayInfoSchema.setOperator(Operator);
		aLICertSalesPayInfoSchema.setMakeDate(DateUtil.getCurDate("yyyy-MM-dd"));
		aLICertSalesPayInfoSchema.setMakeTime(DateUtil.getCurDate("HH:mm:ss"));
		aLICertSalesPayInfoSchema.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
		aLICertSalesPayInfoSchema.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
		aLICertSalesPayInfoSchema.setRelaInfoList(cLCContTable.getPrtNo());
		aLICertSalesPayInfoSchema.setPayInfoType("01");
		aLICertSalesPayInfoSchema.setPayState("00");
    	
        return aLICertSalesPayInfoSchema;
    }
    
	//生成电子保单
	public void makePDF(String cardno){
		
		WxSimplePrintBL ecp = new WxSimplePrintBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo(makeBatchNO());
		tMsgHead.setBranchCode("001");
		tMsgHead.setMsgType("INDIGO_JYBD");//简易保单
		tMsgHead.setSendDate(PubFun.getCurrentDate());
		tMsgHead.setSendTime(PubFun.getCurrentTime());
		tMsgHead.setSendOperator("EC_WX");
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo(cardno);
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);
		ecp.deal(tMsgCollection);
	}
	/**
	 * 生成批次号
	 */
	private static String  makeBatchNO(){
		Calendar CD = Calendar.getInstance();
		int YY = CD.get(Calendar.YEAR);
		int MM = CD.get(Calendar.MONTH)+1;
		int DD = CD.get(Calendar.DATE);
		int HH = CD.get(Calendar.HOUR_OF_DAY);
		int NN = CD.get(Calendar.MINUTE);
		int SS = CD.get(Calendar.SECOND);
		int MI = CD.get(Calendar.MILLISECOND);
		return ""+YY+MM+DD+HH+NN+SS+MI+genRandomNum(4);
	}
	
    private static String genRandomNum(int pwd_len) {
        int i; //生成的随机数
        int count = 0; //生成的密码的长度
        char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        StringBuffer pwd = new StringBuffer("");
        while (count < pwd_len) 
        {
        	i = (int)(Math.random()*10);
            if (i >= 0 && i < str.length) 
            {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }
    
    private void sendMsg(){
    	 System.out.println("完成承保。。开始发送短信......。。。。");
         SmsServiceSoapBindingStub binding = null;
         try {
             binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().
                       getSmsService(); //创建binding对象
         } catch (javax.xml.rpc.ServiceException jre) {
             jre.printStackTrace();
         }

         binding.setTimeout(60000);
         Response value = null; 
         
         Vector vec = new Vector();
         SmsMessage tKXMsg = new SmsMessage(); 
         String contSql = "select cValidate,cinValidate from lccont where prtno='"+this.cLCContTable.getPrtNo()+"'";
         ExeSQL esl = new ExeSQL();
         SSRS conssrs = esl.execSQL(contSql);
         String cvalidate = conssrs.GetText(1, 1);
         String cinvalidate = conssrs.GetText(1, 2);
         String tMSGContents = "尊敬的客户，您好！您已成功投保"+this.cECCertSalesPayInfo.getCertifyName()+"，" +
  		 "保障期限自"+cvalidate+" 零时起 至"+cinvalidate+"零时止（北京时间），" +
     	 "感谢您的支持，祝您健康。详情请致电95591（"+PubFun.getCurrentDate()+"）。";
         String moSql = "select mobile,email from lcaddress where customerno=" +
         		"(select appntno from lccont where prtno='"+this.cLCContTable.getPrtNo()+"') " +
         		"and addressno=(select max( addressno) from lcaddress " +
         		"where customerno=(select appntno from lccont where prtno='"+this.cLCContTable.getPrtNo()+"') ) ";
         SSRS mobssrs = esl.execSQL(moSql);
         String sendtoMobile = mobssrs.GetText(1, 1);
         this.memail = mobssrs.GetText(1, 2);
         System.out.println("承保成功，短信通知对象----------------" + sendtoMobile);
        
         tKXMsg.setReceiver(sendtoMobile); 
         tKXMsg.setContents(tMSGContents); 
         vec.add(tKXMsg);
         Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数

         if(sendtoMobile != null && !"".equals(sendtoMobile)){
        	 if (!vec.isEmpty()) 
             {
             	for(int i=0;i<vec.size();i++)
                 {
             		tempVec.clear();
                 	tempVec.add(vec.get(i));
                 	
                 	SmsMessages msgs = new SmsMessages(); 
                     msgs.setOrganizationId("0"); 
                     msgs.setExtension("false"); 
                     msgs.setServiceType("xuqi"); 
                     msgs.setStartDate(PubFun.getCurrentDate()); 
                     msgs.setStartTime("11:47"); 
                     msgs.setEndDate(PubFun.getCurrentDate()); 
                     msgs.setEndTime("20:00");
                     
                 	msgs.setMessages((SmsMessage[]) tempVec.toArray(new SmsMessage[tempVec.size()]));
                     try 
                     {
                         value = binding.sendSMS("Admin", "Admin", msgs); 
                         System.out.println(value.getStatus());
                         System.out.println(value.getMessage());
                     } 
                     catch (RemoteException ex) 
                     {ex.printStackTrace(); }
                 }
             } 
         }
        
         System.out.println("承保短信已发送完毕......");
        
    }
    private void mailtoCustomer(){
    	//this.mcontno = "013969375000002";
    	//获取电子保单下载地址
    	String urlSql = "select codename from ldcode where codetype='pdfdownloadlink' ";
    	ExeSQL exes = new ExeSQL();
    	SSRS urlssrs = exes.execSQL(urlSql);
    	String urlStr = "";
    	if(urlssrs != null && urlssrs.MaxRow > 0){
    		urlStr = urlssrs.GetText(1, 1);
    		urlStr = urlStr + this.mcontno;
    	}
    	URLConnection conn = null;
    	DataInputStream ireader = null;
    	FileOutputStream fwriter = null;
    	String filePath = "";
    	File desFile = null;
    	try{
    		conn = new URL(urlStr).openConnection();
    		ireader = new DataInputStream(conn.getInputStream());
    		
    		File tempdir = new File(System.getProperty("user.dir"));
    		File desdir = new File(tempdir.getParent()+File.separator + "temp_pdf" + File.separator);
    		System.out.println(desdir);
    		if(!desdir.exists()){
    			desdir.mkdirs();
    		}
    		filePath = tempdir.getParent() +  File.separator + "temp_pdf" + File.separator + this.mcontno + ".pdf";
    		desFile = new File(filePath);
    		
    		//start--看文件是否生成，如果未生成睡20秒，如果超过10分钟，断开
    		boolean on = true;
    		int i = 0;
    		
    		fwriter =  new FileOutputStream(desFile);
    		byte[] bytearr = new byte[1024];
    		while(on){
    			i++ ;
    			while(ireader.read(bytearr) != -1){
        			fwriter.write(bytearr);
        		}
    			fwriter.flush();
    			if(desFile.exists()&&desFile.length()>50){
    				on = false;
    				Thread.sleep(2000);
    				continue;
    			}
    			Thread.sleep(20000);
    			if(i==30){
    				on = false;
    			}
    		}
    		//end --
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		try{
    			if(ireader != null){
        			ireader.close();
        		}
        		if(fwriter != null){
        			fwriter.close();
        		}
    		}catch(Exception e){
    			e.printStackTrace();
    			System.out.println("流关闭异常！！");
    		}
    		
    	}
    	
    	String maiSql = "select appntName,appntSex,insuredName,cvalidate,cinValidate from lccont where contno='"+this.mcontno+"'";
    	SSRS malssras = exes.execSQL(maiSql);
    	String appsexstr = (malssras.GetText(1, 2).equals("1"))?"女士":"男士";
    	if(malssras != null && malssras.MaxRow > 0){
    		
//    		 邮箱发送
    		MailSender tMailSender = new MailSender("eshop", "11111111","picchealth");
    		tMailSender.setSendInf("【人保健康客服中心成功投保提醒】您支付的投保单已成功出单！", 
    				"尊敬的"+malssras.GetText(1, 1)+ appsexstr + "，您好！\r\n 您投保的保单已成功生效，电子投保书详见附件：" +
    				"\r\n投保单号："+this.mcontno+"\n投保人："+malssras.GetText(1, 1)+"\n被保险人："+malssras.GetText(1, 3)+
    				"\r\n保单时间："+malssras.GetText(1, 4)+" 零时至"+malssras.GetText(1, 5)+" 零时\r\n" +
    				"温馨提醒：如对保单有任何疑问请直接咨询客服95591或http://eshop.picchealth.com进行查询。", filePath);
    		
    		tMailSender.setToAddress(this.memail,
    				null, null);
    		
    		// 发送邮件
    		if (!tMailSender.sendMail()) {
    			System.out.println(tMailSender.getErrorMessage());
    		}
    	}
    	
		//删除pdf文件
		//if(desFile.isFile() && desFile.exists()){
		//	desFile.delete();
		//}
		
    }
    /**
     * 内部类，实现runable接口的线程，为了发送邮件
     * @author dhc
     *
     */
    private class MailtoCustomerThread implements Runnable{

		public void run() {
			mailtoCustomer();
		}
    	
    }
    public static void main(String[] args) {
    	WxSign ws = new WxSign();
    	//ws.sendMsg();
    	ws.mailtoCustomer();
    	
	}
}

