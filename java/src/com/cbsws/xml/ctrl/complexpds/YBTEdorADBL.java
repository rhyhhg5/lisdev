package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.BackParamInfo;
import com.cbsws.obj.ECCertSalesInfoTable;
import com.cbsws.obj.LCAppntTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPEdorMainTable;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LPAddressSchema;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LPAddressSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.EasyEdorBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;

public class YBTEdorADBL extends ABusLogic {

	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public LCContTable cLCContTable;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mEdorAcceptNo;
	private LCAppntTable mLCAppntTable;
	private LGWorkTable mLGWorkTable;
	private List mLPEdorMainList;
	
    private MMap map = new MMap();
	
	protected boolean deal(MsgCollection cMsgInfos) {
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        
        //修改联系方式信息
        if(!changeAppntInfo()){
        	return false;
        }
        
		return true;
	}

	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析电话客服的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List tLCContTableList = cMsgInfos.getBodyByFlag("LCContTable");
        if (tLCContTableList == null || tLCContTableList.size() != 1)
        {
            errLog("申请报文中获取卡单号信息失败。");
            return false;
        }
        cLCContTable = (LCContTable)tLCContTableList.get(0);
        
        List tLCAppntList = cMsgInfos.getBodyByFlag("LCAppntTable");
        if (tLCAppntList == null || tLCAppntList.size() != 1)
        {
            errLog("申请报文中获取投保人信息失败。");
            return false;
        }
        mLCAppntTable = (LCAppntTable)tLCAppntList.get(0);
        
        List tLGWorkList = cMsgInfos.getBodyByFlag("LGWorkTable");
        if(tLGWorkList == null ||tLGWorkList.size()!=1)
        {
            errLog("申请报文中获取工单信息失败。");
            return false;
        }
        mLGWorkTable=(LGWorkTable)tLGWorkList.get(0);
        
        return true;
	}
	
	private boolean changeAppntInfo(){
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(cLCContTable.getContNo());
        if(!tLCContDB.getInfo())
        {
        	errLog("没有查询到保单信息"+cLCContTable.getContNo()+"。");
            return false;
        }
        //得到工单信息
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(tLCContDB.getAppntNo());
        tLGWorkSchema.setTypeNo(mLGWorkTable.getTypeNo());
        tLGWorkSchema.setContNo(cLCContTable.getContNo());
        tLGWorkSchema.setApplyTypeNo(mLGWorkTable.getApplyTypeNo());
        tLGWorkSchema.setAcceptWayNo(mLGWorkTable.getAcceptWayNo());
        tLGWorkSchema.setAcceptDate(mLGWorkTable.getAcceptDate());
        tLGWorkSchema.setRemark("电话客服CRM002调用简易保全生成");
        
        //得到客户投保信息
        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        tLPAppntSchema.setContNo(cLCContTable.getContNo());
        tLPAppntSchema.setAppntNo(tLCContDB.getAppntNo());

        //得到客户保单联系地址
        //QULQ ADD 简易保全CRM问题
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(cLCContTable.getContNo());
        tLCAppntDB.getInfo();
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(tLCContDB.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo()==null?"1":tLCAppntDB.getAddressNo());
        tLCAddressDB.getInfo();
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        tLCAddressSchema.setSchema(tLCAddressDB.getSchema());
        
        tLCAddressSchema.setMobile(mLCAppntTable.getAppntMobile()==null?tLCAddressSchema.getMobile():mLCAppntTable.getAppntMobile());
        tLCAddressSchema.setPhone(mLCAppntTable.getAppntPhone()==null?tLCAddressSchema.getPhone():mLCAppntTable.getAppntPhone());
        tLCAddressSchema.setZipCode(mLCAppntTable.getMailZipCode()==null?tLCAddressSchema.getZipCode():mLCAppntTable.getMailZipCode());
        tLCAddressSchema.setPostalAddress(mLCAppntTable.getHomeAddress()==null?tLCAddressSchema.getPostalAddress():mLCAppntTable.getHomeAddress());
        
        //得到关联保单
        String workNo = CommonBL.createWorkNo();
        LPContSet tLPContSet = new LPContSet();
        LPContSchema tLPContSchema = new LPContSchema();
        tLPContSchema.setEdorNo(workNo);
        tLPContSchema.setEdorType(BQ.EDORTYPE_AD);
        tLPContSchema.setContNo(tLCContDB.getContNo());
        tLPContSet.add(tLPContSchema);
        
        //CRM操作员信息
        mGlobalInput.Operator = "ybt";
        mGlobalInput.ManageCom = tLCContDB.getManageCom();
        mGlobalInput.ComCode = mGlobalInput.ManageCom;
        
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGWorkSchema);
        data.add(tLPAppntSchema);
        data.add(tLCAddressSchema);
        data.add(tLPContSet);
        
        EasyEdorBL tEasyEdorBL = new EasyEdorBL();
        if(!tEasyEdorBL.submitData(data))
        {
        	 errLog("保全变更失败"+tEasyEdorBL.mErrors);
            return false;
        }
        LPEdorMainTable tLPEdorMainTable = new LPEdorMainTable();
        tLPEdorMainTable.setEdorAcceptNo(workNo);
        //返回报文
    	//组织返回报文
    	getWrapParmList(tLPEdorMainTable);
    	getXmlResult();
		return true;
	}

    private void getWrapParmList(LPEdorMainTable tLPEdorMainTable){
    	mLPEdorMainList = new ArrayList();
    
    	mLPEdorMainList.add(tLPEdorMainTable);
    }
    
	//返回报文
	public void getXmlResult(){
		//返回退费信息
		for(int i=0;i<mLPEdorMainList.size();i++){
			putResult("LPEdorMainTable", (LPEdorMainTable)mLPEdorMainList.get(i));
		}

	}
    
}
