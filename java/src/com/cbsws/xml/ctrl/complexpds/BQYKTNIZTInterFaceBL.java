package com.cbsws.xml.ctrl.complexpds;

import java.util.ArrayList;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.BQGrpEdorInfo;
import com.cbsws.obj.DealInfo;
import com.cbsws.obj.EdorInsuredInfo;
import com.cbsws.obj.ResultInsuredInfo;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/*
 *create by lihuaiyv in 2018-07 
 */
public class BQYKTNIZTInterFaceBL extends ABusLogic  {
	
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String BatchNo = "";     //批次号
	public String MsgType = "";     //报文类型
	public String BranchCode = "";  //发报单位
	public String Operator = "";    //操作者
	public String mResultError = "";   //返回错误信息
	
	public BQGrpEdorInfo mBQGrpEdorInfo;
	private EdorInsuredInfo mEdorInsuredInfo;
	private LCGrpContSchema mLCGrpContSchema;
	private List<ResultInsuredInfo> mResultInsuredInfoList = new ArrayList<ResultInsuredInfo>();
	private DealInfo mDealInfo;
	private String mEdorAcceptNo;
	private List tLCInsuredList = new ArrayList();

	public boolean deal(MsgCollection cMsgInfos) {
		System.out.println("----->一卡通增减人 BQYKTNIZIInterFaceBL.deal start<-----");
		//解析xml
        if(!parseXML(cMsgInfos)){
        	errLog("一卡通接口解析报文出错！！！" + mResultError);
        	getWrapParmList("fail");
        	return false;
        }
        
        if(!checkdata()){
        	System.out.println("数据校验失败");
        	errLog("一卡通接口校验报文信息出错！！！" + mResultError);
			getWrapParmList("fail");
        	return false;
        }
        
        if(!dealNIZT()) {
        	System.out.println("一卡通接口处理出错");
        	getWrapParmList("fail");
			errLog("一卡通接口处理出错！！！" + mResultError);
			return false;
        }
        
        //解锁
        String openLock = "delete from LockTable where nolimit = '" + mLCGrpContSchema.getGrpContNo() + "' and notype = '" + mBQGrpEdorInfo.getEdorType() + "'";
        MMap tOpenMap = new MMap();
        tOpenMap.put(openLock, "DELETE");
        if(!submit(tOpenMap)){
    		mResultError = "保单"+mLCGrpContSchema.getGrpContNo()+"保全确认结案，但保单解锁失败！！！10分钟后自动解锁。";
    		return false;
    	}
        
        //返回报文
        getWrapParmList("success");
        System.out.println("----->一卡通增减人 BQYKTNIZIInterFaceBL.deal  end <-----");
		return true;
	}
	
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("===parseXML()===");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		BranchCode = tMsgHead.getBranchCode();
		Operator = tMsgHead.getSendOperator();
		
		//获得保单信息
		List cBQGrpEdorInfoList = cMsgInfos.getBodyByFlag("BQGrpEdorInfo");
		if (cBQGrpEdorInfoList == null || cBQGrpEdorInfoList.size() != 1)
        {
			mResultError = "申请报文中获取保单信息失败。";
            System.out.println("<----->解析获取 BQGrpEdorInfo Error<----->");
            return false;
        }
		mBQGrpEdorInfo = (BQGrpEdorInfo)cBQGrpEdorInfoList.get(0);
		
		//获得被保人清单
		List cEdorInsuredInfoList = cMsgInfos.getBodyByFlag("EdorInsuredInfo");
		if (cEdorInsuredInfoList == null || cEdorInsuredInfoList.size() == 0)
        {
			mResultError = "申请报文中获取被保人信息失败。";
            System.out.println("<----->解析获取 EdorInsuredInfo Error<----->");
            return false;
        }
		for(int i = 0;i < cEdorInsuredInfoList.size();i++) {
			EdorInsuredInfo tEdorInsuredInfo = new EdorInsuredInfo();
			tEdorInsuredInfo = (EdorInsuredInfo)cEdorInsuredInfoList.get(i);
			tLCInsuredList.add(tEdorInsuredInfo);
		}
		
		//查询保单信息
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mBQGrpEdorInfo.getGrpContNo());
		if (!tLCGrpContDB.getInfo()) {
			mResultError = "传入的保单号错误，核心系统不存在保单:" + mBQGrpEdorInfo.getGrpContNo() + "!!!";
			return false;
		}
		mLCGrpContSchema = tLCGrpContDB.getSchema();
		
		//增加并发控制，同一个保单只能请求一次
    	MMap tCekMap = null;
    	tCekMap = lockLGWORK(mLCGrpContSchema,mBQGrpEdorInfo.getEdorType());
    	if (tCekMap == null){
    		mResultError = "保单"+mLCGrpContSchema.getGrpContNo()+"正在进行保全，请不要重复请求";	
    		return false;
    	}
    	if(!submit(tCekMap)){
    		mResultError = "保单"+mLCGrpContSchema.getGrpContNo()+"正在进行保全，请不要重复请求";
    		return false;
    	}
		
		return true;
	}
	
	private boolean checkdata(){
		System.out.println("===checkdata()===");
		
		/** 对保全项目信息的校验 */
		if (null == mBQGrpEdorInfo.getGrpContNo()
				|| "".equals(mBQGrpEdorInfo.getGrpContNo())) {
			mResultError = "保单号不能为空";
			return false;
		}
		if (null == mBQGrpEdorInfo.getAcceptDate()
				|| "".equals(mBQGrpEdorInfo.getAcceptDate())) {
			mResultError = "保全申请日期不能为空";
			return false;
		}
		if (null == mBQGrpEdorInfo.getEdorType()
				|| "".equals(mBQGrpEdorInfo.getEdorType())) {
			mResultError = "保全业务类型不能为空";
			return false;
		}
		if (null == mBQGrpEdorInfo.getEdorValidate()
				|| "".equals(mBQGrpEdorInfo.getEdorValidate())) {
			mResultError = "保全生效日期不能为空";
			return false;
		}

		/** 对被保人信息的校验 */
		for (int i = 0; i < tLCInsuredList.size(); i++) {
			mEdorInsuredInfo = (EdorInsuredInfo) tLCInsuredList.get(i);
			if (null == mEdorInsuredInfo.getInsuredID()
					|| "".equals(mEdorInsuredInfo.getInsuredID())) {
				mResultError = "被保人序号不能为空";
				return false;
			}
			if (BQ.EDORTYPE_NI.equals(mBQGrpEdorInfo.getEdorType())) {
				if (null == mEdorInsuredInfo.getContID()
						|| "".equals(mEdorInsuredInfo.getContID())) {
					mResultError = "增人申请中，合同ID不能为空";
					return false;
				}
			}
			if (null == mEdorInsuredInfo.getInsuredState()
					|| "".equals(mEdorInsuredInfo.getInsuredState())) {
				mResultError = "被保人状态不能为空";
				return false;
			}
			if (null == mEdorInsuredInfo.getEmployeeName()
					|| "".equals(mEdorInsuredInfo.getEmployeeName())) {
				mResultError = "员工姓名不能为空";
				return false;
			}
			if (null == mEdorInsuredInfo.getName()
					|| "".equals(mEdorInsuredInfo.getName())) {
				mResultError = "被保人姓名不能为空";
				return false;
			}
			if (null == mEdorInsuredInfo.getRelationToMainInsured()
					|| "".equals(mEdorInsuredInfo.getRelationToMainInsured())) {
				mResultError = "与员工关系不能为空";
				return false;
			}
			if (null == mEdorInsuredInfo.getSex()
					|| "".equals(mEdorInsuredInfo.getSex())) {
				mResultError = "被保人性别不能为空";
				return false;
			}
			if (null == mEdorInsuredInfo.getBirthday()
					|| "".equals(mEdorInsuredInfo.getBirthday())) {
				mResultError = "被保人出生日期不能为空";
				return false;
			}
			if (null == mEdorInsuredInfo.getIDType()
					|| "".equals(mEdorInsuredInfo.getIDType())) {
				mResultError = "被保人证件类型不能为空";
				return false;
			}
			if (null == mEdorInsuredInfo.getIDNo()
					|| "".equals(mEdorInsuredInfo.getIDNo())) {
				mResultError = "被保人证件号不能为空";
				return false;
			}
			if (null == mEdorInsuredInfo.getContPlanCode()
					|| "".equals(mEdorInsuredInfo.getContPlanCode())) {
				mResultError = "保险计划不能为空";
				return false;
			}
			/*if (null == mEdorInsuredInfo.getOccupationType()
					|| "".equals(mEdorInsuredInfo.getOccupationType())) {
				mResultError = "职业类别不能为空";
				return false;
			}*/

			if (!BQ.EDORTYPE_NI.equals(mBQGrpEdorInfo.getEdorType())
					&& !BQ.EDORTYPE_ZT.equals(mBQGrpEdorInfo.getEdorType())) 
			{
				mResultError = "申请的保全类型与请求不符";
				return false;
			}
		}		
		return true;
	}
	
	private boolean dealNIZT(){
		System.out.println("===dealNIZT()===");
		
		mGlobalInput.Operator = Operator;
		mGlobalInput.ComCode = BranchCode;
		mGlobalInput.ManageCom = BranchCode;
		TransferData tempTransferData = new TransferData();
		tempTransferData.setNameAndValue("BatchNo", BatchNo);
		tempTransferData.setNameAndValue("BranchCode", BranchCode);
		tempTransferData.setNameAndValue("EdorType", mBQGrpEdorInfo.getEdorType());

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tempTransferData);
		data.add(mBQGrpEdorInfo);
		data.add(mLCGrpContSchema);
		data.add(tLCInsuredList);
		
		BQYKTNIZTEdorBL tBQYKTNIZTEdorBL = new BQYKTNIZTEdorBL();
		if(!tBQYKTNIZTEdorBL.submit(data)) {
			mEdorAcceptNo = tBQYKTNIZTEdorBL.getEdorAcceptNo();
			mResultError = "  \n  " + tBQYKTNIZTEdorBL.mErrors.getFirstError() + "  \n  " + tBQYKTNIZTEdorBL.mResultMessage;
			return false;
		}
		mResultInsuredInfoList = tBQYKTNIZTEdorBL.getResultInsuredInfoList();
		mEdorAcceptNo = tBQYKTNIZTEdorBL.getEdorAcceptNo();
		mResultError = tBQYKTNIZTEdorBL.mResultMessage;
		return true;
	}
	
	//组织返回报文
	private void getWrapParmList(String ztFlag) {
		//判断失败还是成功
		if("success".equals(ztFlag)){
			for(int i=0;i<mResultInsuredInfoList.size();i++) {
				ResultInsuredInfo tResultInsuredInfo = mResultInsuredInfoList.get(i);
				putResult("ResultInsuredInfo", tResultInsuredInfo);
			}
			mDealInfo = new DealInfo();
			mDealInfo.setDealResult(ztFlag);
			mDealInfo.setEdorNo(mEdorAcceptNo);
			mDealInfo.setRemark("数据存储成功");
			putResult("DealInfo", mDealInfo);
		} else if ("fail".equals(ztFlag)) {
			mDealInfo = new DealInfo();
			mDealInfo.setDealResult(ztFlag);
			mDealInfo.setEdorNo(mEdorAcceptNo);
			mDealInfo.setRemark("数据存储失败");
			putResult("DealInfo", mDealInfo);
		}
	}
	
	private MMap lockLGWORK(LCGrpContSchema cLCGrpContSchema,String cEdorType)
    {
    	
        MMap tMMap = null;
        
        String tLockNoType = cEdorType;
        /**锁定时间*/
        String tAIS = "60";   //1分钟的锁
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCGrpContSchema.getGrpContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null){
            return null;
        }
        return tMMap;
    }
	
	private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
        	mResultError = "提交数据库发生错误"+tPubSubmit.mErrors;
            return false;
        }
        return true;
    }
}
