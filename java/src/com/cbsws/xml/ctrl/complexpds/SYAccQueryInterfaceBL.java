package com.cbsws.xml.ctrl.complexpds;


import java.util.ArrayList;
import java.util.List;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CustomerInfo;
import com.cbsws.obj.SYAccInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;




public class SYAccQueryInterfaceBL extends ABusLogic {

	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public CustomerInfo customerInfo;
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
	List<SYAccInfo> SYAccInfos;
	protected boolean deal(MsgCollection cMsgInfos) {
		long startTime = System.currentTimeMillis();
		System.out.println("接口处理开始时间:"+startTime);
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkdata()){
        	return false;
        }
        //
        if(!queryInfo()){
        	return false;
        }
        
		long endTime = System.currentTimeMillis();
		System.out.println("接口处理结束时间:"+endTime);
		long totalTime = endTime-startTime;
		System.out.println("接口处理总用时："+totalTime+"ms");
		return true;
	}
	/**
	 * 解析xml
	 * @param cMsgInfos
	 * @return
	 */
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List customerInfos = cMsgInfos.getBodyByFlag("CustomerInfo");
        if (customerInfos == null || customerInfos.size() != 1)
        {
            errLog("申请报文中获取报文体失败。");
            return false;
        }
        customerInfo = (CustomerInfo)customerInfos.get(0);
        
        return true;
	}
	/**
	 * 校验数据
	 * @return
	 */
	private boolean checkdata(){
		String name = customerInfo.getName();
		if(name == null || "".equals(name)){
			errLog("客户姓名不能为空！");
			return false;
		}
		String sex = customerInfo.getSex();
		if(sex == null || "".equals(sex)){
			errLog("客户性别不能为空！");
			return false;
		}
		String birthday = customerInfo.getBirthday();
		if(birthday == null || "".equals(birthday)){
			errLog("出生日期不能为空！");
			return false;
		}
		if(birthday.length()!=10){
			errLog("出生日期格式不正确！");
			return false;
		}
		String idType = customerInfo.getIDType();
		if(idType == null || "".equals(idType)){
			errLog("证件类型不能为空！");
			return false;
		}
		String idNo = customerInfo.getIDNo();
		if(idNo == null || "".equals(idNo)){
			errLog("证件号码不能为空！");
			return false;
		}
		return true;
	}
	/**
	 * 查询保单及账户信息
	 * @return
	 */
	private boolean queryInfo(){
		String queryCont = "select contno from lccont where AppntName = '"+customerInfo.getName()+"' and AppntSex = '"+customerInfo.getSex()
						+"' and AppntBirthday = '"+customerInfo.getBirthday()
						+"' and AppntIDType = '"+customerInfo.getIDType()+"' and AppntIDNo = '"+customerInfo.getIDNo()
						+"' and appflag = '1' " 
						+"and exists (select 1 from lcpol where lcpol.contno = lccont.contno and lcpol.riskcode in (select riskcode from lmriskapp where risktype4 = '4' and taxoptimal = 'Y'))";
		String contNo = tExeSQL.getOneValue(queryCont);
		if(contNo == null || "".equals(contNo) || "null".equals(contNo)){
			errLog("没有查询到该客户的有效的税优保单！");
			return false;
		}
		
		String checkAccSQL = "select 1 from lcinsureacc where contno = '"+contNo+"'";
		if(!"1".equals(tExeSQL.getOneValue(checkAccSQL))){
			errLog("账户表数据缺失！");
			return false;
		}
		
		String checkAccTraceSQL = "select 1 from lcinsureacctrace where contno = '"+contNo+"' fetch first 1 rows only";
		if(!"1".equals(tExeSQL.getOneValue(checkAccTraceSQL))){
			errLog("账户轨迹表表数据缺失！");
			return false;
		}
		
		String accQuerySQL = "select "
			+ "(select min(lastpaytodate) from ljapayperson where contno = '"+contNo+"'),"
			+ "(select Insuaccbala from lcinsureacc where contno = '"+contNo+"'), "
			+ "(select sum(money) from lcinsureacctrace where contno = '"+contNo+"' and moneytype in ('BF','ZF') and OtherType = '1'),"
			+ "(select sum(money) from lcinsureacctrace where contno = '"+contNo+"' and moneytype in ('BF','ZF','RP')),"
			+ "(select sum(money) from lcinsureacctrace where contno = '"+contNo+"' and moneytype = 'LX' ),"
			+ "(select sum(money) from lcinsureacctrace where contno = '"+contNo+"' and moneytype = 'BR' ),"
			+ "(select money from lcinsureacctrace where contno = '"+contNo+"' and moneytype = 'LX' order by paydate desc fetch first 1 rows only),"
			+ "(select money from lcinsureacctrace where contno = '"+contNo+"' and moneytype = 'BR' order by paydate desc fetch first 1 rows only),"
			+ "(select rate From LMInsuAccRate where riskcode = (select riskcode from lcpol where contno = '"+contNo+"') order by StartBalaDate desc fetch first 1 rows only) "
			+ "from dual ";
		tSSRS = tExeSQL.execSQL(accQuerySQL);
		if(tSSRS.getMaxRow()<=0){
			errLog("账户信息查询失败！");
			return false;
		}
		
		SYAccInfo accInfo = new SYAccInfo();
		for(int i = 1;i <= tSSRS.getMaxRow();i++){

			//首次生效日期
			String Cvalidate = tSSRS.GetText(i, 1);
			if(Cvalidate == null || "".equals(Cvalidate) || "null".equals(Cvalidate)){
				errLog("查询客户保单首期生效日期失败！");
				return false;
			}
			//账户金额
			String Insuaccbala = tSSRS.GetText(i, 2);
			if(Insuaccbala == null || "".equals(Insuaccbala) || "null".equals(Insuaccbala)){
				Insuaccbala = "0.00";
			}
			//初始金额
			String InitMoney = tSSRS.GetText(i, 3);
			if(InitMoney == null || "".equals(InitMoney) || "null".equals(InitMoney)){
				InitMoney = "0.00";
			}
			//累计缴费金额
			String SumInitMoney = tSSRS.GetText(i, 4);
			if(SumInitMoney == null || "".equals(SumInitMoney) || "null".equals(SumInitMoney)){
				SumInitMoney = "0.00";
			}
			//利息积累
			String SumLX = tSSRS.GetText(i, 5);
			if(SumLX == null || "".equals(SumLX) || "null".equals(SumLX)){
				SumLX = "0.00";
			}
			//累计差额返还
			String SumBR = tSSRS.GetText(i, 6);
			if(SumBR == null || "".equals(SumBR) || "null".equals(SumBR)){
				SumBR = "0.00";
			}
			//上月利息
			String LastLX = tSSRS.GetText(i, 7);
			if(LastLX == null || "".equals(LastLX) || "null".equals(LastLX)){
				LastLX = "0.00";
			}
			//上年差额返还金额
			String LastBR = tSSRS.GetText(i, 8);
			if(LastBR == null || "".equals(LastBR) || "null".equals(LastBR)){
				LastBR = "0.00";
			}
			//实时结算利率
			String Rate = tSSRS.GetText(i, 9);
			if(Rate == null || "".equals(Rate) || "null".equals(Rate)){
				Rate = "0.00";
			}
			
			accInfo.setContNo(contNo);
			accInfo.setCvalidate(Cvalidate);
			accInfo.setInsuaccbala(Insuaccbala);
			accInfo.setInitMoney(InitMoney);
			accInfo.setSumInitMoney(SumInitMoney);
			accInfo.setSumLX(SumLX);
			accInfo.setSumBR(SumBR);
			accInfo.setLastLX(LastLX);
			accInfo.setLastBR(LastBR);
			accInfo.setRate(Rate);
			
		}
		getWrapParmList(accInfo);
		getXmlResult();
		
		return true;
	}	
	 private void getWrapParmList(SYAccInfo SYAccInfo){
		 SYAccInfos = new ArrayList<SYAccInfo>();
	    
		 SYAccInfos.add(SYAccInfo);
	    }
	    
	//返回报文
	public void getXmlResult(){
		//返回账户信息
		for(int i=0;i<SYAccInfos.size();i++){
			putResult("SYAccInfo", (SYAccInfo)SYAccInfos.get(i));
		}

	}	
    
}
