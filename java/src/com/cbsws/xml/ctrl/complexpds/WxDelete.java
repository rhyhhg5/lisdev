package com.cbsws.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.LCContTable;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 网销删单功能：
 * 1、已收费的单子不可删除
 * 2、已签单的单子不可删除
 * @author JC
 *
 */
public class WxDelete extends ABusLogic{
	
	public LCContTable conttable = null; //保单信息实体类
	public String Prtno = null; //印刷号
	public String Managecom = null; //管理机构代码
	public SSRS ssrs = null; //结果集
	public ExeSQL exeSql = new ExeSQL(); //sql执行类
	public GlobalInput mGlobalInput = new GlobalInput(); //公共信息
	public boolean flag = false; //删单标志
	
	/**
	 * 重写ABusLogic中deal处理方法：进入删单流程
	 */
	protected boolean deal(MsgCollection cMsgInfos) {
		
		System.out.println("开始进行删除处理...");
		mGlobalInput.Operator = cMsgInfos.getMsgHead().getSendOperator(); //获取操作员信息		
			
		try {
			//1、获取报文体中的印刷号 和 管理机构代码
			List contList = cMsgInfos.getBodyByFlag("LCContTable");
			if(contList == null || (contList.size() != 1)) {
				errLog("获取保单信息失败！");
				return false;
			}else if(contList != null && (contList.size() > 0)) {
				conttable = (LCContTable)contList.get(0);
				Prtno = conttable.getPrtNo();
				Managecom = conttable.getManageCom();
				mGlobalInput.ManageCom = Managecom;
				System.out.println("获取印刷号、管理机构成功! Prtno: "+Prtno+" Managecom: "+Managecom);
			}
			
			//2、进行收费校验（暂收表中的到帐日期-EnterAccDate 存在，则证明已收费）
			String feeCheckSql = "select EnterAccDate from LJTempFee where OtherNo = '"+Prtno+"' and ManageCom = '"+Managecom+"' ";
			ssrs = exeSql.execSQL(feeCheckSql);
			if(ssrs != null && (ssrs.MaxRow > 0)) {
				String enterAccDateResult = ssrs.GetText(1, 1);
				System.out.println("EnterAccDate: "+enterAccDateResult);
				if("null".equals(enterAccDateResult) || enterAccDateResult == null || "".equals(enterAccDateResult)) {
					System.out.println("保单未收费,继续进行签单校验..");
				}else {
					errLog("印刷号为："+Prtno+"的保单已经收费，不可删除！");
					return false;
				}
			}
			
			//3、进行签单校验（Appflag为1，则已签单）
			String signCheckSql = "select AppFlag from LCCont where PrtNo = '"+Prtno+"' and ManageCom = '"+Managecom+"' ";
			ssrs.Clear();
			ssrs = exeSql.execSQL(signCheckSql);
			if(ssrs != null && (ssrs.MaxRow > 0)) {
				String appFlagResult = ssrs.GetText(1, 1);
				System.out.println("AppFlag: "+appFlagResult);
				if("1".equals(appFlagResult)) {
					errLog("印刷号为："+Prtno+"的保单已经签单，不可删除！");
					return false;
				}else {
					//进行删单！
					deleteCont();
					if(!flag) {
						errLog("印刷号为："+Prtno+"的保单，删单失败！");
						return false;
					}else {
						System.out.println("删单成功！");
						System.out.println("删单处理完毕...");
						return true;
					}
				}
			} else {
				errLog("印刷号为："+Prtno+"的保单不存在，或者印刷号与管理机构不匹配！");
				return false;
			}
		}catch (Exception e) {
			return false;
		}
	}

	/**
	 * 删单方法
	 */
	public void deleteCont() {
		System.out.println("=========开始删单，Prtno为："+Prtno+"=========");
		VData tVData = new VData();
		String tDeleteReason = "网销新单删除";
		TransferData tTransferData = new TransferData();
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setPrtNo(Prtno);
		LCContSchema tLCContSchema = new LCContSchema();
		LCContSet tLCContSet = tLCContDB.query();
		tLCContSchema = tLCContSet.get(1);
	    tTransferData.setNameAndValue("DeleteReason",tDeleteReason);
		tVData.add(tLCContSchema);
		tVData.add(tTransferData);
		tVData.add(mGlobalInput);
		ContDeleteUI tContDeleteUI = new ContDeleteUI();
		flag = tContDeleteUI.submitData(tVData,"DELETE");
    	System.out.println("=========删单结束！=========");
	}
}
