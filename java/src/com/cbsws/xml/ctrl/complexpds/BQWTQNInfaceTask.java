package com.cbsws.xml.ctrl.complexpds;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.BackParamInfo;
import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPEdorItemTable;
import com.cbsws.obj.ParamInfo;
import com.sinosoft.lis.bq.BQWTQNInfaceBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BQWTQNInfaceTask extends ABusLogic{
	
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public LCContTable cLCContTable;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mEdorAcceptNo;
	private LCContSchema mLCContSchema;
	private LGWorkTable mLgWorkTable;
	private LPEdorItemTable mLPedorItemTable;
	private ParamInfo mParamInfo;
	private List mParamInfoList;
	
	
	
        public BQWTQNInfaceTask() {
        }
        
//        public MsgCollection service(MsgCollection cMsgInfos){
//        	if(!deal(cMsgInfos)){
//        		return null;
//        	}
//        	return cMsgInfos;
//        }
        
        public boolean deal(MsgCollection cMsgInfos)  {
        	//解析XML
	        if(!parseXML(cMsgInfos)){
	        	return false;
	        }

	        mGlobalInput.ManageCom=mParamInfo.getManageCom();
	        mGlobalInput.Operator = mParamInfo.getOperator();
            VData data = new VData();
            data.add(mGlobalInput);
            data.add(cLCContTable);
            data.add(mLCContSchema);
            data.add(mLgWorkTable);
            data.add(mLPedorItemTable);
            data.add(mParamInfo);
            BQWTQNInfaceBL tBQWTQNInfaceBL = new BQWTQNInfaceBL();
            if(!tBQWTQNInfaceBL.submit(data)){
            	System.out.println("保全操作错误");
            	errLog("保全操作错误"+tBQWTQNInfaceBL.mErrors);
            	//组织返回报文
    			getWrapParmList(null,"success");
    			getXmlResult();
            }else{
            	//生成报文返回
            	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
            	tLPEdorItemSchema =tBQWTQNInfaceBL.getEdorItem(); 
            	//组织返回报文
            	getWrapParmList(tLPEdorItemSchema,"success");
            	getXmlResult();
            }
            return true;
        }
        
        private boolean parseXML(MsgCollection cMsgInfos){
        	System.out.println("开始解析去哪网的XML文档");
    		MsgHead tMsgHead = cMsgInfos.getMsgHead();
    		BatchNo = tMsgHead.getBatchNo();
    		MsgType = tMsgHead.getMsgType();
    		Operator = tMsgHead.getSendOperator();
    		
    		//获取保单信息
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			
	        if (tLCContList == null || tLCContList.size() != 1)
	        {
	            errLog("申请报文中获取保单信息失败。");
	            return false;
	        }
	        cLCContTable = (LCContTable)tLCContList.get(0);  //接口一次只接受一个保单的犹豫期退保
	        //获取工单信息
	        List tLgWorkList = cMsgInfos.getBodyByFlag("LGWorkTable");
	        if (tLgWorkList == null || tLgWorkList.size() != 1)
	        {
	            errLog("申请报文中获取工单信息失败。");
	            return false;
	        }
	        mLgWorkTable = (LGWorkTable)tLgWorkList.get(0);
	        //获取保全项目信息
	        List tLPEdorItemList = cMsgInfos.getBodyByFlag("LPEdorItemTable");
	        if (tLPEdorItemList == null || tLPEdorItemList.size() != 1)
	        {
	            errLog("申请报文中获取保全项目信息失败。");
	            return false;
	        }
	        mLPedorItemTable = (LPEdorItemTable)tLPEdorItemList.get(0); 
	        //获取其他参数信息
	        List tParamInfoList = cMsgInfos.getBodyByFlag("ParamInfo");
	        if (tParamInfoList == null || tParamInfoList.size() != 1)
	        {
	            errLog("申请报文中获取保全项目信息失败。");
	            return false;
	        }
	        mParamInfo = (ParamInfo)tParamInfoList.get(0); 
	        
        	//查询保单信息
        	LCContDB tLCContDB = new LCContDB();
        	tLCContDB.setContNo(cLCContTable.getContNo());
        	if(!tLCContDB.getInfo()){
	            errLog("传入的保单号错误，核心系统不存在保单“"+cLCContTable.getContNo()+"”");
	            return false;        		
        	}
        	mLCContSchema = tLCContDB.getSchema();
	        
        	//增加并发控制，同一个保单只能请求一次
        	MMap tCekMap = null;
        	tCekMap = lockLGWORK(mLCContSchema);
        	if (tCekMap == null)
        	{
        	errLog("保单"+mLCContSchema.getContNo()+"正在进行犹豫期退保，请不要重复请求");	
        		return false;
        	}
        	if(!submit(tCekMap)){
        		return false;
        	}
	        return true;
        }
        
        private void getWrapParmList(LPEdorItemSchema tLPEdorItemSchema,String ztFlag){
        	mParamInfoList = new ArrayList();
        	//判断失败还是成功
        	if("success".equals(ztFlag)&&tLPEdorItemSchema!=null){
        		//查询付费通知书号以及金额
        		LJAGetDB tLJAGetDB = new LJAGetDB();
        		LJAGetSet tLJAGetSet = new LJAGetSet();
        		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        		tLJAGetDB.setOtherNo(tLPEdorItemSchema.getEdorAcceptNo());
        		tLJAGetSet = tLJAGetDB.query();
        		if(tLJAGetSet!=null&&tLJAGetSet.size()>0){
        			tLJAGetSchema = tLJAGetSet.get(1);
        		}
        		BackParamInfo tParamInfo = new BackParamInfo();
        		tParamInfo.setBackMoney(""+tLJAGetSchema.getSumGetMoney());
        		tParamInfo.setActuGetno(tLJAGetSchema.getActuGetNo());
        		mParamInfoList.add(tParamInfo);
        	}else{
        		BackParamInfo tParamInfo = new BackParamInfo();
        		tParamInfo.setBackMoney("");
        		tParamInfo.setActuGetno("");
        		mParamInfoList.add(tParamInfo);
        	}
        }
        
    	//返回报文
    	public void getXmlResult(){
    		//返回退费信息
    		for(int i=0;i<mParamInfoList.size();i++){
    			putResult("BackParamInfo", (BackParamInfo)mParamInfoList.get(i));
    		}

    	}
        
        /**
         * 锁定动作
         * @param cLCContSchema
         * @return
         */
        private MMap lockLGWORK(LCContSchema cLCContSchema)
        {
            MMap tMMap = null;
            /**犹豫期退保锁定标志"WT"*/
            String tLockNoType = "WT";
            /**锁定时间*/
            String tAIS = "3600";
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
            tTransferData.setNameAndValue("LockNoType", tLockNoType);
            tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

            VData tVData = new VData();
            tVData.add(mGlobalInput);
            tVData.add(tTransferData);

            LockTableActionBL tLockTableActionBL = new LockTableActionBL();
            tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
            if (tMMap == null)
            {
                return null;
            }
            return tMMap;
        }    
        
        /**
         * 提交数据到数据库
         * @return boolean
         */
        private boolean submit(MMap map)
        {
            VData data = new VData();
            data.add(map);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(data, ""))
            {
            	errLog("提交数据库发生错误"+tPubSubmit.mErrors);
                return false;
            }
            return true;
        }
        
        public static void  main(String[] args) throws FileNotFoundException,
            IOException {
//            BQWTQNInfaceTask mCrmInfaceTask = new BQWTQNInfaceTask();
//            mCrmInfaceTask.DealDate();
        }

}
