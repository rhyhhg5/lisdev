package com.cbsws.xml.ctrl.complexpds;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.ContQueryInfo; 
import com.cbsws.obj.EdorAppntInfo;
import com.cbsws.obj.EdorBnfInfo;
import com.cbsws.obj.EdorContInfo;
import com.cbsws.obj.EdorInsuInfo;
import com.cbsws.obj.EdorRiskInfo;
import com.cbsws.obj.PadContQueryInfo;
import com.cbsws.obj.RPadContQueryInfo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public class PADBQContInfoQuery extends ABusLogic{

	public String mStartDate = "";
	public String mEndDate = "";
	public String mPaytoDate = "";
	public String mAgentCode = "";
	public String MsgType="";
	public String BatchNo = "";
	private PadContQueryInfo mPadContQueryInfo =null;
	String tsql="";
//	private EdorContInfo mEdorContInfo =null;
//	private EdorAppntInfo mEdorAppntInfo =null;
//	private EdorInsuInfo mEdorInsuredInfo =null;
//	private EdorRiskInfo mEdorRiskInfo =null;

	public CErrors mErrors = new CErrors();
	
	public PADBQContInfoQuery() {
		this.MsgType = "BQCONTINFOQUERY";
	}
	
    public boolean deal(MsgCollection cMsgInfos)  {
    	//解析XML
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!checkData()){
        	System.out.println("数据校验失败---");
        	//组织返回报文
			getWrapParmList("fail");
        	
        	return false;
        }
        
        //组织返回报文
        try {
			getWrapParmList("success");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;

        
    }
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口XML");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		//获取保单信息
//		List mContQueryInfolist = cMsgInfos.getBodyByFlag("ContQueryInfo");
		List mContQueryList= cMsgInfos.getBodyByFlag("PadContQueryInfo");
		 if (mContQueryList == null || mContQueryList.size() != 1)
        {
            errLog("申请报文中获取保单信息失败。");
            return false;
        }
		 System.out.println(mContQueryList.get(0));
        mPadContQueryInfo =  (PadContQueryInfo)mContQueryList.get(0);
        mStartDate=mPadContQueryInfo.getStartDate();
        mEndDate=mPadContQueryInfo.getEndDate();
        mAgentCode=mPadContQueryInfo.getAgentCode();
        mPaytoDate=mPadContQueryInfo.getPaytoDate();
		return true;
	}
	
	public boolean checkData() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		String tsql="";	
		Date d1 = null;
		Date d2 = null;
		try {
		    d1 = sdf.parse(mStartDate);
			d2 =  sdf.parse(mEndDate);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		if("".equals(mAgentCode) || mAgentCode == null){
            errLog("业务员代码不能为空。");
            return false;
		}
		if("".equals(mPaytoDate)||mPaytoDate == null){
			
			if(!"".equals(mStartDate) && mStartDate != null && !"".equals(mEndDate) && mEndDate != null){
				tsql+=" and '"+mStartDate+"'<=c.lastpaytodate and c.lastpaytodate <= '"+mEndDate+"'";
			}
			else{
				 errLog("起始日期和终止日期不能同时为空");
		         return false;
			}
			int days = PubFun.calInterval(d1,d2,"D");
			if(days>92){
				errLog("起始日期和终止日期间隔不能超过3个月");
		         return false;
			}
			
			
		}else{
			tsql +=" and  c.lastpaytodate='"+mPaytoDate+"'";
		}
		
		String contsql = "select 1  from lccont a where "
			+ "	a.conttype='1' "
			+ " and (a.agentcode='"+ mAgentCode + "' or a.agentcode=(select agentcode from laagent where groupagentcode='"+mAgentCode+"')) " 
			+ " with ur ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS contResult=tExeSQL.execSQL(contsql);
		if(contResult.MaxRow==0){
			errLog("保单查询失败，检查业务员代码是否正确");
			return false;
		}
		return true;
	}
	
	public boolean getWrapParmList(String tFlag) {
		if("success".equals(tFlag)){
//			String tsql="";
//			if(!"".equals(mStartDate) && mStartDate != null){
//				tsql+=" and  c.CurPayToDate='"+mStartDate+"'";
//			}else if (!"".equals(mEndDate) && mEndDate != null){
//				tsql+=" and a.prtno='"+mEndDate+"'";
//			}else {
//				errLog("保单号和印刷号不能同时为空");
//				return false;
//			}
	
			
			
			String contdatasql = "select  a.ContNo,a.AppntName,c.riskcode,(select riskname from lmriskapp where riskcode = c.riskcode),"
				  	 +" (select mobile from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno ),"
				  	 +" (select (case when phone is null then homephone else phone end) from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno)phone, "
				  	+" (select PostalAddress from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno ),"
					 + " c.SumDuePayMoney,min(c.lastpaytodate),d.paymoney,d.paydate,(select codename from ldcode where codetype='paymode' and code=a.PayMode),a.AgentCode,b.GetNoticeNo,"
					 + " (case when (b.dealstate ='0' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno "
					 + " and riskcode in ('320106','120706')) is not null ) then '等待客户反馈' "
					 + " when (b.dealstate ='1' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno and riskcode in ('320106','120706')) is not null ) then '续保成功' "
					 + " when (b.dealstate ='3' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno  and riskcode in ('320106','120706')	) is not null ) then '满期给付' "
					 +" else (select codename  from ldcode where codetype='dealstate' and code=b.dealstate)  end)"
//					 +	" (case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end)" 
					
					// + " (select codename  from ldcode where codetype='dealstate' and code=b.dealstate),(case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end)"
//					 +"(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen "
					 + "from LCCont a,ljspayB b ,ljspaypersonB c ,ljtempfee d where  "
					 + " a.ContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo and b.GetNoticeNo = d.tempfeeno and c.riskcode = d.riskcode" 
					 + " and b.OtherNoType='2'"
//					 + " and  b.SerialNo in (" + tInNoticeNo + ")" 
					 +tsql
//					 + " and c.CurPayToDate = '"+mPaytoDate+"'"
					 + " and c.agentcode = '"+ mAgentCode +"' "
					 + " group by a.ContNo,a.AppntName,a.appntno,c.SumDuePayMoney,c.riskcode,d.paymoney,d.paydate,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate"
			     + " order by a.AgentCode,b.GetNoticeNo ";
	


			ExeSQL tExeSQL = new ExeSQL();
			SSRS contdataResult;
//			SSRS insureddataResult;
//			SSRS appntdataResult;
//			SSRS riskcodeResult;
//			SSRS bnfResult;

			contdataResult = tExeSQL.execSQL(contdatasql);
//			appntdataResult = tExeSQL.execSQL(appntdatasql);
//			insureddataResult = tExeSQL.execSQL(insureddatasql);
//			riskcodeResult = tExeSQL.execSQL(riskcodesql);
//		    bnfResult = tExeSQL.execSQL(bnflistsql);
		    
			if (contdataResult.getMaxRow() > 0) {
				for(int i =1;i<=contdataResult.getMaxRow();i++){
					
					RPadContQueryInfo tPadContQueryInfo=new RPadContQueryInfo();
					tPadContQueryInfo.setAgentCode(mAgentCode);
					tPadContQueryInfo.setContno(contdataResult.GetText(i, 1));
					tPadContQueryInfo.setAppntname(contdataResult.GetText(i, 2));
					tPadContQueryInfo.setRiskcode(contdataResult.GetText(i, 3));
					tPadContQueryInfo.setRiskname(contdataResult.GetText(i, 4));
					tPadContQueryInfo.setMobile(contdataResult.GetText(i, 5));
					tPadContQueryInfo.setPhone(contdataResult.GetText(i, 6));
					tPadContQueryInfo.setAddress(contdataResult.GetText(i, 7));
					tPadContQueryInfo.setPaymoney(contdataResult.GetText(i, 8));
					tPadContQueryInfo.setPaytodate(contdataResult.GetText(i, 9));
					tPadContQueryInfo.setTpaymoney(contdataResult.GetText(i, 10));
					tPadContQueryInfo.setTpaydate(contdataResult.GetText(i, 11));
					tPadContQueryInfo.setPaymode(contdataResult.GetText(i, 12));
					tPadContQueryInfo.setFalg(contdataResult.GetText(i, 15));
					putResult("RPadContQueryInfo",tPadContQueryInfo);
				}
			}else{
				errLog("没有查询到保单数据");
				return false;
			}
			
		
		
			

		


		
		}else if("fail".equals(tFlag)){
    		
//			EdorContInfo tEdorContInfo=new EdorContInfo();
//			putResult("EdorBnfInfo",tEdorContInfo);
//			
//			EdorAppntInfo tEdorAppntInfo=new EdorAppntInfo();
//			putResult("EdorBnfInfo",tEdorAppntInfo);
//			
//			EdorInsuredInfo tEdorInsuredInfo=new EdorInsuredInfo();
//			putResult("EdorBnfInfo",tEdorInsuredInfo);
//			
//			EdorRiskInfo tEdorRiskInfo=new EdorRiskInfo();
//			putResult("EdorBnfInfo",tEdorRiskInfo);
//			
//			EdorBnfInfo tEdorBnfInfo=new EdorBnfInfo();
		
			

    	}
		return true;
	}
}
