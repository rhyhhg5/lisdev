package com.cbsws.xml.ctrl.complexpds;


import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.SYContTable;
import com.sinosoft.lis.cbcheck.GetContTaxCodeBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;



public class RYPrint extends ABusLogic {
	
	public String ContNo="";//合同号
	public String SendDate="";//报文发送日期
	public String SendTime="";//报文发送时间
	public String BatchNo="";
	
	
	public SYContTable cLCCONTINFO;
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始处理网销复杂产品核保业务");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		SendDate = tMsgHead.getSendDate();
		SendTime = tMsgHead.getSendTime();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("SYContTable");	       
	        cLCCONTINFO = (SYContTable) tLCContList.get(0); //获取保单信息，只有一个保单
	        
	        ContNo=cLCCONTINFO.getContNo();
	        if("".equals(ContNo)||ContNo==null){
	        	errLog("请确定传入合同号，且格式为<ContNo></ContNo>");
	        	return false;
	        }
	        
	        
	        SSRS ss1=new SSRS();
	        ExeSQL exeSQL=new ExeSQL();
	        ss1=exeSQL.execSQL("select sysvarvalue from ldsysvar where sysvar='ServerRoot'");
	        
	        String path=ss1.GetText(1, 1);
	        
//            path="D:\\SourceCode\\lisdev\\ui\\";
//	        exeSQL.execUpdateSQL("update lccont set printcount='0' where contno='"+ContNo+"'");
	        String szTemplatePath = path+"/f1print/template/";	//模板路径
	        String sOutXmlPath = path;	//xml文件输出路径
//	        sOutXmlPath="F://";
	        ss1.Clear();
	        String sql="SELECT prtNo,PrintCount,Operator,managecom FROM LCCont A"
	        		+ " WHERE AppFlag in ('1','9') and ( PrintCount < 1 OR PrintCount IS NULL )  "
	        		+ " and exists (select 1 from LDCode1 where CodeType = 'printchannel' and Code = substr(ManageCom, 1, 4) and Code1 = '2') "
	        		+ ""
	        		+ "" 
	        		+ " and contno='"+ContNo+"'";
	        
	        ss1=exeSQL.execSQL(sql);
	        if(ss1.getMaxRow()==0){
	        	errLog("未查询到存在可打印的该合同号的保单。");
	            return false;
	        }
	       
			TransferData mTransferData = new TransferData();
			String tContNo = "";
			String tPrtNo = "";
			String TaxFlag=exeSQL.getOneValue("select Taxcode from lccontsub where prtno='"+ss1.GetText(1, 1)+"'");
			if(TaxFlag==null||"".equals(TaxFlag)){
				mTransferData.setNameAndValue("ContNo", ContNo);
				mTransferData.setNameAndValue("PrtNo", ss1.GetText(1, 1));
				VData tVData1 = new VData();
				tVData1.add(mTransferData);
				tVData1.add(mGlobalInput);
				GetContTaxCodeBL tGetContTaxCodeBL = new GetContTaxCodeBL();//调用处理类
				if (!tGetContTaxCodeBL.submitData(tVData1, "GET")) {
					
					String Content = tGetContTaxCodeBL.mErrors.getError(0).errorMessage;
					errLog(Content);
					return false;
				}
				
			}
			
			
	        LCContSchema tLCGrpContSchema = new LCContSchema();
	    	tLCGrpContSchema.setContNo(ContNo);
	    	RYContF1PBL bl = new RYContF1PBL();	     
	        GlobalInput globalInput=new GlobalInput();
	        globalInput.ManageCom=ss1.GetText(1, 4);
	        globalInput.ComCode=ss1.GetText(1, 4);
	        globalInput.Operator=ss1.GetText(1, 3);	       
	        String sign=cLCCONTINFO.getSign();  
	        String orderno=cLCCONTINFO.getOrderNo();
	        
	        
	        String contPrintFlag="1";
	        VData vData = new VData();
	    	vData.add(globalInput);
	    	vData.addElement(tLCGrpContSchema);
	    	vData.add(szTemplatePath);
	    	vData.add(sOutXmlPath);	    	
	    	vData.add(contPrintFlag);
	        vData.add(sign);
	        vData.add(orderno);
	        

	        if (!bl.submitData(vData, "PRINT"))
	        {
	            System.out.println(bl.mErrors.getErrContent());
	            errLog(bl.mErrors.getErrContent());
	            return false;
	        }
	        //getXmlResult();
		}catch (Exception e) {
			
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
		
		putResult("LCContTable", cLCCONTINFO);
		
		
	}
	

	
	

    


    public static void main(String[] args) {
    	
	}
    
}
