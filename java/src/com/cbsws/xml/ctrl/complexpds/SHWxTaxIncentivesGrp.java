package com.cbsws.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.SHLSGRPErrorTable;
import com.cbsws.obj.SHLSGRPTable;
import com.sinosoft.lis.db.LSGrpDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LSGrpSchema;
import com.sinosoft.lis.tb.SHTaxIncentivesGrpUI;
import com.sinosoft.lis.vschema.LSGrpSet;
import com.sinosoft.utility.VData;

public class SHWxTaxIncentivesGrp extends ABusLogic {
	
	public String Operator="";//操作者
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public SHLSGRPTable mLSGRPTable = new SHLSGRPTable();//
	public SHLSGRPErrorTable mLSGRPErrorTable = new SHLSGRPErrorTable();//
	private LSGrpSchema mLSGrpSchema = new LSGrpSchema();
	private MsgHead mMsgHead  = new MsgHead();
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理上海网销增加税优团体客户业务;");
		mMsgHead = cMsgInfos.getMsgHead();
		Operator = mMsgHead.getSendOperator();
		mGlobalInput.Operator = mMsgHead.getSendOperator();
		mGlobalInput.ManageCom = "86";
		try{
			List LSGRPTableList = cMsgInfos.getBodyByFlag("SHLSGRPTable"); 
			if (LSGRPTableList == null || LSGRPTableList.size() != 1)
	        {
	            errLog("获取信息失败。");
	            return false;
	        }
			mLSGRPTable = (SHLSGRPTable) LSGRPTableList.get(0);//只有一个
			String sql ="select * from lsgrp where grpname='"+mLSGRPTable.getGrpName()+"' with ur";
			LSGrpDB tLSGrpDB = new LSGrpDB();
			LSGrpSet tLSGrpSet = tLSGrpDB.executeQuery(sql);
			if(tLSGrpSet != null && tLSGrpSet.size() != 0){
				tLSGrpSet.get(1);
				mLSGRPErrorTable.setGrpNo(tLSGrpSet.get(1).getGrpNo());
				mLSGRPErrorTable.setErrInfo("此单位已存在税优团体客户信息");
				mLSGRPErrorTable.setState("00");
				mLSGRPErrorTable.setErrCode("0");
			}else{
		  		mLSGrpSchema.setGrpNo("S" + PubFun1.CreateMaxNo("L",8));
		  	  	mLSGrpSchema.setGrpName(mLSGRPTable.getGrpName());
		  	  	mLSGrpSchema.setTaxRegistration(mLSGRPTable.getTaxRegistration());
		  	  	mLSGrpSchema.setGrpAddress(mLSGRPTable.getGrpAddress());
		  	  	mLSGrpSchema.setGrpZipcode(mLSGRPTable.getGrpZipcode());
		  	  	mLSGrpSchema.setGrpNature(mLSGRPTable.getGrpNature());
		  	  	mLSGrpSchema.setPeoples(mLSGRPTable.getPeoples());
		  	  	mLSGrpSchema.setOrgancomCode(mLSGRPTable.getOrgancomCode());
		  	  	mLSGrpSchema.setBusinessType(mLSGRPTable.getBusinessType());
		  	  	mLSGrpSchema.setLeaglePerson(mLSGRPTable.getLeaglePerson());
		  	  	mLSGrpSchema.setPhone(mLSGRPTable.getPhone());
		  	  	mLSGrpSchema.setHandlerName(mLSGRPTable.getHandlerName());
		  	  	mLSGrpSchema.setDepartment(mLSGRPTable.getDepartment());
		  	  	mLSGrpSchema.setHandlerPhone(mLSGRPTable.getHandlerPhone());
		  	  	mLSGrpSchema.setBankCode(mLSGRPTable.getBankCode());
		  	  	mLSGrpSchema.setBankAccName(mLSGRPTable.getBankAccName());
		  	  	mLSGrpSchema.setBankAccNo(mLSGRPTable.getBankAccNo());
		  	  	mLSGrpSchema.setOperator(mGlobalInput.Operator);
		  	  	mLSGrpSchema.setMakeDate(PubFun.getCurrentDate());
		  		mLSGrpSchema.setMakeTime(PubFun.getCurrentTime());
		  		mLSGrpSchema.setModifyDate(PubFun.getCurrentDate());
		  	  	mLSGrpSchema.setModifyTime(PubFun.getCurrentTime());

				VData tVData = new VData();
		  		tVData.add(mGlobalInput);
		  		tVData.add(mLSGrpSchema);
		  		SHTaxIncentivesGrpUI tTaxIncentivesGrpUI = new SHTaxIncentivesGrpUI();
		  		if(!tTaxIncentivesGrpUI.submitData(tVData,"INSERT")){
		  			mLSGRPErrorTable.setErrInfo("处理失败，原因是:" + tTaxIncentivesGrpUI.mErrors.getFirstError());
		  			mLSGRPErrorTable.setState("01");
		  			mLSGRPErrorTable.setErrCode("4");
		  			
		  		}else{
					mLSGRPErrorTable.setGrpNo(mLSGrpSchema.getGrpNo());
					mLSGRPErrorTable.setState("00");
		  		}
			}
			getXmlResult();
		}catch(Exception ex){
			return false;
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		putResult("SHLSGRPErrorTable", mLSGRPErrorTable);

	}

}
