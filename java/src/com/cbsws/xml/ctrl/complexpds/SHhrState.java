package com.cbsws.xml.ctrl.complexpds;

import java.util.List;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.SHGrpInfo;
import com.cbsws.obj.SHContTable;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class SHhrState extends ABusLogic {
	
	public String GrpNo="";//印刷号
	public String SendDate="";//报文发送日期
	public String SendTime="";//报文发送时间
	public String BatchNo="";
	public String StartDate;
	public String EndDate;
	public SHGrpInfo cGrpINFO;
	
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mError="";//处理过程中的错误信息
	String cRiskWrapCode = "";//套餐编码
	
	protected boolean deal(MsgCollection cMsgInfos){
		System.out.println("开始登录验证");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		SendDate = tMsgHead.getSendDate();
		SendTime = tMsgHead.getSendTime();
		
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		
		try{
			List tLCContList = cMsgInfos.getBodyByFlag("SHGrpInfo");			
			cGrpINFO = (SHGrpInfo) tLCContList.get(0); //获取保单信息，只有一个保单
	        
			GrpNo=cGrpINFO.getGrpNo();
			StartDate=cGrpINFO.getStartDate();
			EndDate=cGrpINFO.getEndDate();			
			
	        if("".equals(GrpNo)||GrpNo==null){
	        	errLog("团队编码不能为空");
	        	return false;
	        }
	        if(!PubFun.checkDateForm(StartDate)||!PubFun.checkDateForm(EndDate)){
	        	errLog("请确定所传日期格式为YYYY-MM-DD");
	        	return false;
	        	
	        }
	        
	       
	        
	        SSRS ss1=new SSRS();
	        ExeSQL exeSQL=new ExeSQL();
	        String sql="select lc.contno,lc.appflag,"
	        		+"lc.PolApplyDate,lc.prem,lc.amnt"
	        		+ " ,nvl(lc.insuredname, (select name from lcinsured where 1=1 and contno=lc.contno and insuredno=lc.insuredno) ) insuredname "
	        		 +" from lccont lc,lccontsub ls where  lc.prtno=ls.prtno and ls.grpno='"+GrpNo+"' and "
	        		 + "lc.polapplydate>='"+StartDate+"' and lc.polapplydate<='"+EndDate+"' order by lc.PolApplyDate";
	        ss1=exeSQL.execSQL(sql);
	        if(ss1.getMaxRow()==0){
	        	errLog("未查询到存在税优保单");
        		return false;
	        }else{
	        	for(int i=1;i<=ss1.getMaxRow();i++){
	        		SHContTable shContTable=new SHContTable();
	        		shContTable.setContNo(ss1.GetText(i, 1));
	        		shContTable.setState(ss1.GetText(i, 2));
	        		shContTable.setPolApplyDate(ss1.GetText(i, 3));
	        		shContTable.setPrem(ss1.GetText(i, 4));
	        		shContTable.setAmnt(ss1.GetText(i, 5));
	        		shContTable.setInsuredName(ss1.GetText(i, 6));
	        		putResult("SHContTable", shContTable);
	        		
	        		
	        	}
	        	
	        	
	        	
	        	
	        }
	        
	        
	       
	        //getXmlResult();
		}catch (Exception e) {
			System.out.println(e);
			errLog("未知错误，请检查报文格式");
        	return false;
		}
		return true;
	}
	
	//返回报文
	public void getXmlResult(){
		//保单节点
		
		
		
		
	}
	

	
	

    


    public static void main(String[] args) {
    	String s1="[A-Za-z0-9]{10}";
    	String s2="1b3456789A";
    	System.out.println(s2.matches(s1));
    	
	}
    
}
