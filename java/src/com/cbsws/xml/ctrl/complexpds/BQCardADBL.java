package com.cbsws.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.ECCertSalesInfoTable;
import com.cbsws.obj.LCAppntTable;
import com.sinosoft.lis.certifybusiness.CertifyContConst;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.db.WFAppntListDB;
import com.sinosoft.lis.db.WFInsuListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.lis.vschema.WFAppntListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class BQCardADBL extends ABusLogic {

	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public GlobalInput mGlobalInput = new GlobalInput();//公共信息
	public String mEdorAcceptNo;
	private LCAppntTable mLCAppntTable;
	private ECCertSalesInfoTable mECCertSalesInfoTable;
    private MMap map = new MMap();
	
	public boolean deal(MsgCollection cMsgInfos) {
		// TODO Auto-generated method stub
		//解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        
        //修改联系方式信息
        if(!changeAppntInfo()){
        	return false;
        }
        
		return true;
	}

	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析电话客服的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List tECCertSalesInfoList = cMsgInfos.getBodyByFlag("ECCertSalesInfoTable");
        if (tECCertSalesInfoList == null || tECCertSalesInfoList.size() != 1)
        {
            errLog("申请报文中获取卡单号信息失败。");
            return false;
        }
        mECCertSalesInfoTable = (ECCertSalesInfoTable)tECCertSalesInfoList.get(0);
        
        List tLCAppntList = cMsgInfos.getBodyByFlag("LCAppntTable");
        if (tLCAppntList == null || tLCAppntList.size() != 1)
        {
            errLog("申请报文中获取投保人信息失败。");
            return false;
        }
        mLCAppntTable = (LCAppntTable)tLCAppntList.get(0);
        
		return true;
	}
	
	private boolean changeAppntInfo(){
	
		if(mECCertSalesInfoTable.getCardNo()==null||"".equals(mECCertSalesInfoTable.getCardNo())){
            errLog("申请报文中获取卡号信息失败。");
            return false;
		}
		 // 校验保险卡（折）主信息是否存在
        LICertifyDB tLICertifyDB = new LICertifyDB();
        tLICertifyDB.setCardNo(mECCertSalesInfoTable.getCardNo());
        LICertifySet tLICertifySet = tLICertifyDB.query();
        
        if (tLICertifySet!=null&&tLICertifySet.size()>0)
        {
        	/**
        	 *已结算的保单判断保单已激活并且未实名化查看LICertify的状态WSState='00'表示未实名化
        	 * 存在LICardActiveInfoList表表示已激活，执行修改逻辑
        	 * 被保险人与投保人是否同一客户
        	 *（WFAppntList与WFInsuList卡号相同、姓名、证件号码、证件类型相同、出生日期相同、性别相同表示同一客户）
        	 * 不相同只修改WFAppntList，相同还需要修改WFInsuList、LICardActiveInfoList
        	 */
        	LICardActiveInfoListDB tLICardActiveInfoListdb = new LICardActiveInfoListDB();
        	tLICardActiveInfoListdb.setCardNo(mECCertSalesInfoTable.getCardNo());
        	LICardActiveInfoListSet tLICardActiveInfoListSet = new LICardActiveInfoListSet();
        	tLICardActiveInfoListSet = tLICardActiveInfoListdb.query();
        	if(tLICardActiveInfoListSet!=null&&tLICardActiveInfoListSet.size()>0){
        		//已激活的卡单判断是否未实名化
        		if(!CertifyContConst.CC_WSSTATE_UNCREATED.equals(tLICertifySet.get(1).getWSState())){
        			errLog( "[" + mECCertSalesInfoTable.getCardNo() + "]保险卡已经被实名化。");
                    return false;
        		}
        		WFAppntListDB tWFAppntListDB = new WFAppntListDB();
        		tWFAppntListDB.setCardNo(mECCertSalesInfoTable.getCardNo());
        		WFAppntListSet tWFAppntListSet = tWFAppntListDB.query();
        		if(tWFAppntListSet!=null&&tWFAppntListSet.size()>0){
        			WFAppntListSchema tWFAppntListSchema = new WFAppntListSchema();
        			tWFAppntListSchema = tWFAppntListSet.get(1);
        			
        			WFInsuListDB tWFInsuListDB = new WFInsuListDB();
        			tWFInsuListDB.setCardNo(mECCertSalesInfoTable.getCardNo());
        			tWFInsuListDB.setName(tWFAppntListSet.get(1).getName());
        			tWFInsuListDB.setSex(tWFAppntListSet.get(1).getSex());
        			tWFInsuListDB.setBirthday(tWFAppntListSet.get(1).getBirthday());
        			tWFInsuListDB.setIDType(tWFAppntListSet.get(1).getIDType());
        			tWFInsuListDB.setIDNo(tWFAppntListSet.get(1).getIDNo());
        			
        			LICardActiveInfoListDB cLICardActiveInfoListDB = new LICardActiveInfoListDB();
        			cLICardActiveInfoListDB.setCardNo(mECCertSalesInfoTable.getCardNo());
        			cLICardActiveInfoListDB.setName(tWFAppntListSet.get(1).getName());
        			cLICardActiveInfoListDB.setSex(tWFAppntListSet.get(1).getSex());
        			cLICardActiveInfoListDB.setBirthday(tWFAppntListSet.get(1).getBirthday());
        			cLICardActiveInfoListDB.setIdType(tWFAppntListSet.get(1).getIDType());
        			cLICardActiveInfoListDB.setIdNo(tWFAppntListSet.get(1).getIDNo());
        			
        			
        			LICertifyInsuredDB tLICertifyInsuredDB = new LICertifyInsuredDB();
        			tLICertifyInsuredDB.setCardNo(mECCertSalesInfoTable.getCardNo());
        			tLICertifyInsuredDB.setName(tWFAppntListSet.get(1).getName());
        			tLICertifyInsuredDB.setSex(tWFAppntListSet.get(1).getSex());
        			tLICertifyInsuredDB.setBirthday(tWFAppntListSet.get(1).getBirthday());
        			tLICertifyInsuredDB.setIdType(tWFAppntListSet.get(1).getIDType());
        			tLICertifyInsuredDB.setIdNo(tWFAppntListSet.get(1).getIDNo());
        			
        			
        			//修改投保人信息
        			if(mLCAppntTable.getAppntMobile()!=null&&!"".equals(mLCAppntTable.getAppntMobile())){
        				tWFAppntListSchema.setMobile(mLCAppntTable.getAppntMobile());
        			}
        			if(mLCAppntTable.getAppntPhone()!=null&&!"".equals(mLCAppntTable.getAppntPhone())){
        				tWFAppntListSchema.setPhont(mLCAppntTable.getAppntPhone());
        			}
        			if(mLCAppntTable.getMailZipCode()!=null&&!"".equals(mLCAppntTable.getMailZipCode())){
        				tWFAppntListSchema.setZipCode(mLCAppntTable.getMailZipCode());
        			}
        			if(mLCAppntTable.getHomeAddress()!=null&&!"".equals(mLCAppntTable.getHomeAddress())){
        				tWFAppntListSchema.setPostalAddress(mLCAppntTable.getHomeAddress());
        			}
        			map.put(tWFAppntListSchema,SysConst.UPDATE);
        			
        		//若被保人与投保人相同，也需要修改被保险人信息
        			WFInsuListSet cWFInsuListSet = new WFInsuListSet();
        			LICardActiveInfoListSet cLICardActiveInfoList = new LICardActiveInfoListSet();
        			cWFInsuListSet = tWFInsuListDB.query();
        			cLICardActiveInfoList = cLICardActiveInfoListDB.query();
        			if(cWFInsuListSet!=null&&cWFInsuListSet.size()>0){
        				for(int i=1;i<=cWFInsuListSet.size();i++){
        					WFInsuListSchema cWFInsuListSchema = new WFInsuListSchema();
        					cWFInsuListSchema = cWFInsuListSet.get(i);
                			//修改信息 被保险人
                			if(mLCAppntTable.getAppntMobile()!=null&&!"".equals(mLCAppntTable.getAppntMobile())){
                				cWFInsuListSchema.setMobile(mLCAppntTable.getAppntMobile());
                			}
                			if(mLCAppntTable.getAppntPhone()!=null&&!"".equals(mLCAppntTable.getAppntPhone())){
                				cWFInsuListSchema.setPhont(mLCAppntTable.getAppntPhone());
                			}
                			if(mLCAppntTable.getMailZipCode()!=null&&!"".equals(mLCAppntTable.getMailZipCode())){
                				cWFInsuListSchema.setZipCode(mLCAppntTable.getMailZipCode());
                			}
                			if(mLCAppntTable.getHomeAddress()!=null&&!"".equals(mLCAppntTable.getHomeAddress())){
                				cWFInsuListSchema.setPostalAddress(mLCAppntTable.getHomeAddress());
                			}
                			map.put(cWFInsuListSchema,SysConst.UPDATE);
        				}
        			
        			}
        			
        			if(cLICardActiveInfoList!=null&&cLICardActiveInfoList.size()>0){
        				for(int i=1;i<=cLICardActiveInfoList.size();i++){
        					LICardActiveInfoListSchema cLICardActiveInfoListSchema = new LICardActiveInfoListSchema();
        					cLICardActiveInfoListSchema = cLICardActiveInfoList.get(i);
                			//修改信息 被保险人
                			if(mLCAppntTable.getAppntMobile()!=null&&!"".equals(mLCAppntTable.getAppntMobile())){
                				cLICardActiveInfoListSchema.setMobile(mLCAppntTable.getAppntMobile());
                			}
                			if(mLCAppntTable.getAppntPhone()!=null&&!"".equals(mLCAppntTable.getAppntPhone())){
                				cLICardActiveInfoListSchema.setPhone(mLCAppntTable.getAppntPhone());
                			}
                			if(mLCAppntTable.getMailZipCode()!=null&&!"".equals(mLCAppntTable.getMailZipCode())){
                				cLICardActiveInfoListSchema.setZipCode(mLCAppntTable.getMailZipCode());
                			}
                			if(mLCAppntTable.getHomeAddress()!=null&&!"".equals(mLCAppntTable.getHomeAddress())){
                				cLICardActiveInfoListSchema.setPostalAddress(mLCAppntTable.getHomeAddress());
                			}
                			map.put(cLICardActiveInfoListSchema,SysConst.UPDATE);
        				}
        			}
        			
        			LICertifyInsuredSet tLICertifyInsuredSet = new LICertifyInsuredSet();
        			tLICertifyInsuredSet = tLICertifyInsuredDB.query();
        			
        			if(tLICertifyInsuredSet!=null&&tLICertifyInsuredSet.size()>0){
        				for(int i=1;i<=tLICertifyInsuredSet.size();i++){
        					LICertifyInsuredSchema cLICertifyInsuredSchema = new LICertifyInsuredSchema();
        					cLICertifyInsuredSchema = tLICertifyInsuredSet.get(i);
                			//修改信息 被保险人
                			if(mLCAppntTable.getAppntMobile()!=null&&!"".equals(mLCAppntTable.getAppntMobile())){
                				cLICertifyInsuredSchema.setMobile(mLCAppntTable.getAppntMobile());
                			}
                			if(mLCAppntTable.getAppntPhone()!=null&&!"".equals(mLCAppntTable.getAppntPhone())){
                				cLICertifyInsuredSchema.setPhone(mLCAppntTable.getAppntPhone());
                			}
                			if(mLCAppntTable.getMailZipCode()!=null&&!"".equals(mLCAppntTable.getMailZipCode())){
                				cLICertifyInsuredSchema.setZipCode(mLCAppntTable.getMailZipCode());
                			}
                			if(mLCAppntTable.getHomeAddress()!=null&&!"".equals(mLCAppntTable.getHomeAddress())){
                				cLICertifyInsuredSchema.setPostalAddress(mLCAppntTable.getHomeAddress());
                			}
                			map.put(cLICertifyInsuredSchema,SysConst.UPDATE);
        				}
        			}
        			
        		}else{
        			errLog( "[" + mECCertSalesInfoTable.getCardNo() + "]投保人信息不存在。");
                    return false;
        		}
        		
        	}else{
    			errLog( "[" + mECCertSalesInfoTable.getCardNo() + "]保险卡未被激活。");
                return false;
        	}
        	
        }else{
        	/**
        	 * 未结算的已激活的卡单直接修改
        	 * 存在LICardActiveInfoList表表示已激活，若存在执行修改逻辑
        	 * 被保险人与投保人是否同一客户
        	 * （WFAppntList与WFInsuList卡号相同、姓名、证件号码、证件类型相同、出生日期相同、性别相同表示同一客户）
        	 * 不相同只修改WFAppntList，相同还需要修改WFInsuList、LICardActiveInfoList
        	 */
        	LICardActiveInfoListDB tLICardActiveInfoListdb = new LICardActiveInfoListDB();
        	tLICardActiveInfoListdb.setCardNo(mECCertSalesInfoTable.getCardNo());
        	LICardActiveInfoListSet tLICardActiveInfoListSet = new LICardActiveInfoListSet();
        	tLICardActiveInfoListSet = tLICardActiveInfoListdb.query();
        	if(tLICardActiveInfoListSet!=null&&tLICardActiveInfoListSet.size()>0){

        		WFAppntListDB tWFAppntListDB = new WFAppntListDB();
        		tWFAppntListDB.setCardNo(mECCertSalesInfoTable.getCardNo());
        		WFAppntListSet tWFAppntListSet = tWFAppntListDB.query();
        		if(tWFAppntListSet!=null&&tWFAppntListSet.size()>0){
        			WFAppntListSchema tWFAppntListSchema = new WFAppntListSchema();
        			tWFAppntListSchema = tWFAppntListSet.get(1);
        			
        			WFInsuListDB tWFInsuListDB = new WFInsuListDB();
        			tWFInsuListDB.setCardNo(mECCertSalesInfoTable.getCardNo());
        			tWFInsuListDB.setName(tWFAppntListSet.get(1).getName());
        			tWFInsuListDB.setSex(tWFAppntListSet.get(1).getSex());
        			tWFInsuListDB.setBirthday(tWFAppntListSet.get(1).getBirthday());
        			tWFInsuListDB.setIDType(tWFAppntListSet.get(1).getIDType());
        			tWFInsuListDB.setIDNo(tWFAppntListSet.get(1).getIDNo());
        			
        			LICardActiveInfoListDB cLICardActiveInfoListDB = new LICardActiveInfoListDB();
        			cLICardActiveInfoListDB.setCardNo(mECCertSalesInfoTable.getCardNo());
        			cLICardActiveInfoListDB.setName(tWFAppntListSet.get(1).getName());
        			cLICardActiveInfoListDB.setSex(tWFAppntListSet.get(1).getSex());
        			cLICardActiveInfoListDB.setBirthday(tWFAppntListSet.get(1).getBirthday());
        			cLICardActiveInfoListDB.setIdType(tWFAppntListSet.get(1).getIDType());
        			cLICardActiveInfoListDB.setIdNo(tWFAppntListSet.get(1).getIDNo());
        			
        			//修改投保人信息
        			if(mLCAppntTable.getAppntMobile()!=null&&!"".equals(mLCAppntTable.getAppntMobile())){
        				tWFAppntListSchema.setMobile(mLCAppntTable.getAppntMobile());
        			}
        			if(mLCAppntTable.getAppntPhone()!=null&&!"".equals(mLCAppntTable.getAppntPhone())){
        				tWFAppntListSchema.setPhont(mLCAppntTable.getAppntPhone());
        			}
        			if(mLCAppntTable.getMailZipCode()!=null&&!"".equals(mLCAppntTable.getMailZipCode())){
        				tWFAppntListSchema.setZipCode(mLCAppntTable.getMailZipCode());
        			}
        			if(mLCAppntTable.getHomeAddress()!=null&&!"".equals(mLCAppntTable.getHomeAddress())){
        				tWFAppntListSchema.setPostalAddress(mLCAppntTable.getHomeAddress());
        			}
        			map.put(tWFAppntListSchema,SysConst.UPDATE);
        			
        		//若被保人与投保人相同，也需要修改被保险人信息
        			WFInsuListSet cWFInsuListSet = new WFInsuListSet();
        			LICardActiveInfoListSet cLICardActiveInfoList = new LICardActiveInfoListSet();
        			cWFInsuListSet = tWFInsuListDB.query();
        			cLICardActiveInfoList = cLICardActiveInfoListDB.query();
        			if(cWFInsuListSet!=null&&cWFInsuListSet.size()>0){
        				for(int i=1;i<=cWFInsuListSet.size();i++){
        					WFInsuListSchema cWFInsuListSchema = new WFInsuListSchema();
        					cWFInsuListSchema = cWFInsuListSet.get(i);
                			//修改信息 被保险人
                			if(mLCAppntTable.getAppntMobile()!=null&&!"".equals(mLCAppntTable.getAppntMobile())){
                				cWFInsuListSchema.setMobile(mLCAppntTable.getAppntMobile());
                			}
                			if(mLCAppntTable.getAppntPhone()!=null&&!"".equals(mLCAppntTable.getAppntPhone())){
                				cWFInsuListSchema.setPhont(mLCAppntTable.getAppntPhone());
                			}
                			if(mLCAppntTable.getMailZipCode()!=null&&!"".equals(mLCAppntTable.getMailZipCode())){
                				cWFInsuListSchema.setZipCode(mLCAppntTable.getMailZipCode());
                			}
                			if(mLCAppntTable.getHomeAddress()!=null&&!"".equals(mLCAppntTable.getHomeAddress())){
                				cWFInsuListSchema.setPostalAddress(mLCAppntTable.getHomeAddress());
                			}
                			map.put(cWFInsuListSchema,SysConst.UPDATE);
        				}
        			
        			}
        			
        			if(cLICardActiveInfoList!=null&&cLICardActiveInfoList.size()>0){
        				for(int i=1;i<=cLICardActiveInfoList.size();i++){
        					LICardActiveInfoListSchema cLICardActiveInfoListSchema = new LICardActiveInfoListSchema();
        					cLICardActiveInfoListSchema = cLICardActiveInfoList.get(i);
                			//修改信息 被保险人
                			if(mLCAppntTable.getAppntMobile()!=null&&!"".equals(mLCAppntTable.getAppntMobile())){
                				cLICardActiveInfoListSchema.setMobile(mLCAppntTable.getAppntMobile());
                			}
                			if(mLCAppntTable.getAppntPhone()!=null&&!"".equals(mLCAppntTable.getAppntPhone())){
                				cLICardActiveInfoListSchema.setPhone(mLCAppntTable.getAppntPhone());
                			}
                			if(mLCAppntTable.getMailZipCode()!=null&&!"".equals(mLCAppntTable.getMailZipCode())){
                				cLICardActiveInfoListSchema.setZipCode(mLCAppntTable.getMailZipCode());
                			}
                			if(mLCAppntTable.getHomeAddress()!=null&&!"".equals(mLCAppntTable.getHomeAddress())){
                				cLICardActiveInfoListSchema.setPostalAddress(mLCAppntTable.getHomeAddress());
                			}
                			map.put(cLICardActiveInfoListSchema,SysConst.UPDATE);
        				}
        			}
        		}else{
        			errLog( "[" + mECCertSalesInfoTable.getCardNo() + "]投保人信息不存在。");
                    return false;
        		}
        		
        	}else{
    			errLog( "[" + mECCertSalesInfoTable.getCardNo() + "]保险卡未被激活。");
                return false;
        	}
        	
        }
        if(!submit(map)){
        	return false;
        }
        
		return true;
	}
	
    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
        	errLog("提交数据库发生错误"+tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
