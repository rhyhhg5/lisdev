package com.cbsws.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.core.xml.xsch.BaseXmlSch;
import com.cbsws.obj.DSXQRequestInfo;
import com.cbsws.obj.SYAccInfo;
import com.cbsws.obj.SYReportQueryInfo;
import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.db.LCAppAccDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJSPayPersonBDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccSchema;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.schema.LJSPayPersonBSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCAppAccSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSPayPersonBSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;
import com.cbsws.obj.DSXQResponseInfo;

public class DSPayFeeInterfaceBL extends ABusLogic{
	private String BatchNo;//批次号
	private String MsgType;//报文类型
	private String Operator;//操作者
	private DSXQRequestInfo DSXQRequestInfo;
	private DSXQResponseInfo DSXQResponseInfo = new DSXQResponseInfo();
	private SSRS tSSRS = new SSRS();
	private ExeSQL tExeSQL = new ExeSQL();
	private String ContNo = null;
	private LCContSchema mLCContSchema = null;
	private LJSPaySchema mLJSPaySchema = null;
	private MMap map = new MMap();
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	
	protected boolean deal(MsgCollection cMsgInfos) {
		if(!parseXML(cMsgInfos)){
			return false;
		}
		if(!checkdata()){
        	return false;
        }
		if(!dealdate()){
			return false;
		}
		if(!submit()){
			return false;
		}
		getXmlResult();
		
		return true;
	}
	
	/**
	 * 解析xml
	 * @param cMsgInfos
	 * @return
	 */
	private boolean parseXML(MsgCollection cMsgInfos){
    	System.out.println("开始解析接口的XML文档");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		
		List<DSXQRequestInfo> DSXQRequestInfos = cMsgInfos.getBodyByFlag("DSXQRequestInfo");
        if (DSXQRequestInfos == null || DSXQRequestInfos.size() != 1)
        {
            errLog("申请报文中获取报文体失败。");
            return false;
        }
        DSXQRequestInfo = (DSXQRequestInfo)DSXQRequestInfos.get(0);
        
        return true;
	}
	
	/**
	 * 校验数据
	 * @return
	 */
	private boolean checkdata(){
		ContNo = DSXQRequestInfo.getContNo();
		if(ContNo == null || "".equals(ContNo)){
			errLog("保单号不能为空！");
			return false;
		}
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(ContNo);
		if(!tLCContDB.getInfo()){
			errLog("没有查询到该保单号的保单！");
			return false;
		}
		mLCContSchema = tLCContDB.getSchema();

		if (mLCContSchema.getCardFlag().equals("b")){
			errLog("电商保单不能通过此通道付费！");
			return false;
		}
		return true;
	}
	
	private boolean dealdate(){
		//查询该保单有无催收记录
		LJSPayDB tLJSPayDB = new LJSPayDB();
		LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery("select * from ljspay where otherno = '" + ContNo + "'");
		if(tLJSPaySet.size()!=1){
			errLog("查询该保单的催收记录有误！");
			return false;
		}
		mLJSPaySchema = tLJSPaySet.get(1);
		//如果已经是银行在途，则直接将该笔金额放入投保人账户
		if("1".equals(mLJSPaySchema.getBankOnTheWayFlag())){
			if(!dealAcc()){
				errLog("处理投保人账户失败！");
			}
			return true;
		}
		//没有银行在途，则直接生成暂收
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(ContNo);
		LCPolSet tLCPolSet = tLCPolDB.query();
	    String tTempFeeType = "2";//续期续保
	    String sql3 = "select riskcode,sum(SumDuePayMoney) as prem from ljspayperson where contno='"+ContNo+"' group by riskcode";
	    SSRS ljspayList = tExeSQL.execSQL(sql3);
	    double paySum = 0.0;
	    for(int i=0;i<ljspayList.MaxRow;i++){
	    	String riskPrem = ljspayList.GetText(i+1, 2);
	    	paySum = paySum + Double.parseDouble(riskPrem);
	    }
	    String getNoticeno = mLJSPaySchema.getGetNoticeNo();
	    
		LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
		
    	SSRS BankInfos = getBankAccCode();
     	String ZBankManageCom  ="";
    	if (BankInfos.GetText(1, 1).equals("86")){
    		ZBankManageCom ="86000000";
    	}else {
    	   ZBankManageCom= BankInfos.GetText(1, 1);//总公司机构
    	}
    	String ZBankAccNo = BankInfos.GetText(1, 2);
    	for(int i=0;i<ljspayList.MaxRow;i++){
    		String riskcode = ljspayList.GetText(i+1, 1); 
    		String riskPrem = ljspayList.GetText(i+1, 2);
    		LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema(); 
    		tLJTempFeeSchema.setTempFeeNo(getNoticeno);
    		tLJTempFeeSchema.setOtherNo(ContNo); //投保单号
    		tLJTempFeeSchema.setTempFeeType(tTempFeeType);//2-续期续保
    		tLJTempFeeSchema.setOtherNoType("4");	//4--印刷号
    		tLJTempFeeSchema.setRiskCode(riskcode);//默认000000
    		tLJTempFeeSchema.setPayIntv(tLCPolSet.get(1).getPayIntv());
    		tLJTempFeeSchema.setAPPntName(tLCPolSet.get(1).getAppntName());
    		tLJTempFeeSchema.setPayMoney(riskPrem);
    		tLJTempFeeSchema.setManageCom(ZBankManageCom);	//收费机构
    		tLJTempFeeSchema.setPolicyCom(mLCContSchema.getManageCom());	//管理机构
    		tLJTempFeeSchema.setPayDate(mCurrentDate);
    		tLJTempFeeSchema.setEnterAccDate(mCurrentDate);
    		tLJTempFeeSchema.setConfMakeDate(mCurrentDate);
    		tLJTempFeeSchema.setConfMakeTime(mCurrentTime);
    		tLJTempFeeSchema.setSaleChnl(mLCContSchema.getSaleChnl());
    		tLJTempFeeSchema.setAgentCom(mLCContSchema.getAgentCom());
    		tLJTempFeeSchema.setAgentGroup(mLCContSchema.getAgentGroup());
    		tLJTempFeeSchema.setAgentCode(mLCContSchema.getAgentCode());
    		tLJTempFeeSchema.setConfFlag("0");	//是否核销 0：没有财务核销 1：已有财务核销
    		tLJTempFeeSchema.setOperator(Operator);
    		tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
    		tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
    		tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
    		tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
    		tLJTempFeeSet.add(tLJTempFeeSchema);
    	}
		LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
		
    	LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
		mLJTempFeeClassSchema.setTempFeeNo(getNoticeno);
		mLJTempFeeClassSchema.setPayMode("12");
		mLJTempFeeClassSchema.setPayMoney(paySum);
		mLJTempFeeClassSchema.setAppntName(mLCContSchema.getAppntName());
		mLJTempFeeClassSchema.setPayDate(mCurrentDate);
		mLJTempFeeClassSchema.setEnterAccDate(mCurrentDate);
		mLJTempFeeClassSchema.setConfMakeDate(mCurrentDate);
		mLJTempFeeClassSchema.setConfMakeTime(mCurrentTime);
		mLJTempFeeClassSchema.setConfFlag("0"); //是否核销 0：没有财务核销 1：已有财务核销
		mLJTempFeeClassSchema.setManageCom(ZBankManageCom);
		mLJTempFeeClassSchema.setPolicyCom(mLCContSchema.getManageCom());
		mLJTempFeeClassSchema.setInsBankAccNo(ZBankAccNo);
    	String aBankCode = getBankCode(ZBankAccNo);//获取银行账号对应的bankcode
    	mLJTempFeeClassSchema.setBankCode(aBankCode);
    	mLJTempFeeClassSchema.setBankCode(mLCContSchema.getBankCode());
		mLJTempFeeClassSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLJTempFeeClassSchema.setAccName(mLCContSchema.getAccName());
		mLJTempFeeClassSchema.setOperator(Operator);
		mLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
		mLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
		tLJTempFeeClassSet.add(mLJTempFeeClassSchema);
		
		//暂收生成之后将应收置为待核销状态
		LJSPayBDB tLJSPayBDB = new LJSPayBDB();
		tLJSPayBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
		if(!tLJSPayBDB.getInfo()){
			errLog("没有查询到该保单的应收记录备份表！");
			return false;
		}
		LJSPayBSchema tLJSPayBSchema = tLJSPayBDB.getSchema();
		tLJSPayBSchema.setDealState("4");
		
		map.put(tLJTempFeeSet,"DELETE&INSERT");
		map.put(tLJTempFeeClassSet,"DELETE&INSERT");
		map.put(tLJSPayBSchema,"UPDATE");
		return true;
	}
	
	/**
	 * 保存入库
	 */
	private boolean submit(){
		VData dd = new VData();
        dd.add(map);
        PubSubmit pub = new PubSubmit();
        if(!pub.submitData(dd, "")){
            System.out.println(pub.mErrors.getErrContent());
            errLog("生成暂收数据失败!");
            return false;
        }
		return true;
	}
	
	/**
	 * 返回报文
	 */
	public void getXmlResult(){
		DSXQResponseInfo.setContNo(ContNo);
		putResult("DSXQResponseInfo", DSXQResponseInfo);

	}	
	
    /**
     * 将有银行在途的实收保费转入帐户
     * @return boolean
     */
    private boolean dealAcc(){

    	AppAcc tAppAcc = new AppAcc();
        LCAppAccSchema tLCAppAccSchema= tAppAcc.getLCAppAcc(mLCContSchema.getAppntNo());

        if(tLCAppAccSchema==null){
            AppAcc mAppAcc = new AppAcc();

            MMap mMMap = new MMap();
            LCAppAccTraceSchema schema = new LCAppAccTraceSchema();
            schema.setCustomerNo(mLCContSchema.getAppntNo());

            schema.setOtherNo(mLJSPaySchema.getGetNoticeNo());
            schema.setOtherType("1");
            schema.setMoney(0);
            schema.setOperator(mLCContSchema.getOperator());

            mMMap.add(mAppAcc.accShiftToXQY(schema, "0"));

            VData dd = new VData();
            dd.add(mMMap);
            PubSubmit pub = new PubSubmit();
            if(!pub.submitData(dd, "")){
                System.out.println(pub.mErrors.getErrContent());
                errLog("自动创建帐户,提交失败!");
                return false;
            }
        }

        //生成一条应收记录作废，作为该笔收费进账户余额的凭证
		LJSPayBDB tLJSPayBDB = new LJSPayBDB();
		tLJSPayBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
		if(!tLJSPayBDB.getInfo()){
			errLog("没有查询到该保单的应收记录备份表！");
			return false;
		}
		LJSPayBSchema tLJSPayBSchema = tLJSPayBDB.getSchema();
		//重新生成一个应收号
		String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String tGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);

		tLJSPayBSchema.setDealState("2");
		tLJSPayBSchema.setCancelReason("1");
		tLJSPayBSchema.setGetNoticeNo(tGetNoticeNo);
		
		LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
		LJSPayPersonBSet tOldLJSPayPersonBSet = tLJSPayPersonBDB.executeQuery("select * from ljspaypersonb where getnoticeno = '" + mLJSPaySchema.getGetNoticeNo() + "'");
		LJSPayPersonBSet tNewLJSPayPersonBSet = new LJSPayPersonBSet();
		for(int i = 1;i<=tOldLJSPayPersonBSet.size();i++){
			LJSPayPersonBSchema tLJSPayPersonBSchema = tOldLJSPayPersonBSet.get(i);
			tLJSPayPersonBSchema.setGetNoticeNo(tGetNoticeNo);
			tLJSPayPersonBSchema.setDealState("2");
			tLJSPayPersonBSchema.setCancelReason("1");
			
			tNewLJSPayPersonBSet.add(tLJSPayPersonBSchema);
		}
        
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(ContNo);
		LCPolSet tLCPolSet = tLCPolDB.query();
		
	    String sql3 = "select riskcode,sum(SumDuePayMoney) as prem from ljspayperson where contno='"+ContNo+"' group by riskcode";
	    SSRS ljspayList = tExeSQL.execSQL(sql3);
	    double paySum = 0.0;
	    for(int i=0;i<ljspayList.MaxRow;i++){
	    	String riskPrem = ljspayList.GetText(i+1, 2);
	    	paySum = paySum + Double.parseDouble(riskPrem);
	    }
		LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
		
    	SSRS BankInfos = getBankAccCode();
     	String ZBankManageCom  ="";
    	if (BankInfos.GetText(1, 1).equals("86")){
    		ZBankManageCom ="86000000";
    	}else {
    	   ZBankManageCom= BankInfos.GetText(1, 1);//总公司机构
    	}
    	String ZBankAccNo = BankInfos.GetText(1, 2);
    	for(int i=0;i<ljspayList.MaxRow;i++){
    		String riskcode = ljspayList.GetText(i+1, 1); 
    		String riskPrem = ljspayList.GetText(i+1, 2);
    		LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema(); 
    		tLJTempFeeSchema.setTempFeeNo(tGetNoticeNo);
    		tLJTempFeeSchema.setOtherNo(ContNo); //投保单号
    		tLJTempFeeSchema.setTempFeeType("2");//2-续期续保
    		tLJTempFeeSchema.setOtherNoType("4");	//4--印刷号
    		tLJTempFeeSchema.setRiskCode(riskcode);//默认000000
    		tLJTempFeeSchema.setPayIntv(tLCPolSet.get(1).getPayIntv());
    		tLJTempFeeSchema.setAPPntName(tLCPolSet.get(1).getAppntName());
    		tLJTempFeeSchema.setPayMoney(riskPrem);
    		tLJTempFeeSchema.setManageCom(ZBankManageCom);	//收费机构
    		tLJTempFeeSchema.setPolicyCom(mLCContSchema.getManageCom());	//管理机构
    		tLJTempFeeSchema.setPayDate(mCurrentDate);
    		tLJTempFeeSchema.setEnterAccDate(mCurrentDate);
    		tLJTempFeeSchema.setConfMakeDate(mCurrentDate);
    		tLJTempFeeSchema.setConfMakeTime(mCurrentTime);
    		tLJTempFeeSchema.setSaleChnl(mLCContSchema.getSaleChnl());
    		tLJTempFeeSchema.setAgentCom(mLCContSchema.getAgentCom());
    		tLJTempFeeSchema.setAgentGroup(mLCContSchema.getAgentGroup());
    		tLJTempFeeSchema.setAgentCode(mLCContSchema.getAgentCode());
    		tLJTempFeeSchema.setConfFlag("2");	//是否核销 0：没有财务核销 1：已有财务核销
    		tLJTempFeeSchema.setOperator(Operator);
    		tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
    		tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
    		tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
    		tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
    		tLJTempFeeSet.add(tLJTempFeeSchema);
    	}
		LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
		
    	LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
		mLJTempFeeClassSchema.setTempFeeNo(tGetNoticeNo);
		mLJTempFeeClassSchema.setPayMode("12");
		mLJTempFeeClassSchema.setPayMoney(paySum);
		mLJTempFeeClassSchema.setAppntName(mLCContSchema.getAppntName());
		mLJTempFeeClassSchema.setPayDate(mCurrentDate);
		mLJTempFeeClassSchema.setEnterAccDate(mCurrentDate);
		mLJTempFeeClassSchema.setConfMakeDate(mCurrentDate);
		mLJTempFeeClassSchema.setConfMakeTime(mCurrentTime);
		mLJTempFeeClassSchema.setConfFlag("2"); //是否核销 0：没有财务核销 1：已有财务核销
		mLJTempFeeClassSchema.setManageCom(ZBankManageCom);
		mLJTempFeeClassSchema.setPolicyCom(mLCContSchema.getManageCom());
		mLJTempFeeClassSchema.setInsBankAccNo(ZBankAccNo);
    	String aBankCode = getBankCode(ZBankAccNo);//获取银行账号对应的bankcode
    	mLJTempFeeClassSchema.setBankCode(aBankCode);
    	mLJTempFeeClassSchema.setBankCode(mLCContSchema.getBankCode());
		mLJTempFeeClassSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLJTempFeeClassSchema.setAccName(mLCContSchema.getAccName());
		mLJTempFeeClassSchema.setOperator(Operator);
		mLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
		mLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
		tLJTempFeeClassSet.add(mLJTempFeeClassSchema);
		
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(mLCContSchema.getAppntNo());
        tLCAppAccTraceSchema.setAccType("1");
        tLCAppAccTraceSchema.setOtherNo(mLCContSchema.getContNo());
        tLCAppAccTraceSchema.setBakNo(tGetNoticeNo);
        tLCAppAccTraceSchema.setOtherType("2");
        tLCAppAccTraceSchema.setMoney(mLJSPaySchema.getSumDuePayMoney());
        tLCAppAccTraceSchema.setOperator(Operator);
        MMap tMap = tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema);
        if(tMap == null){
            errLog(tAppAcc.mErrors.getContent());
            return false;
        }
        map.add(tMap);
        map.put(tLJSPayBSchema,"DELETE&INSERT");
        map.put(tLJTempFeeSet,"DELETE&INSERT");
        map.put(mLJTempFeeClassSchema,"DELETE&INSERT");

        return true;
    }
    
    /**
     * 生成暂收数据
     * @return
     */
    private boolean dealTempFee(){
    	LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(ContNo);
		LCPolSet tLCPolSet = tLCPolDB.query();
	    String tTempFeeType = "2";//续期续保
	    String sql3 = "select riskcode,sum(SumDuePayMoney) as prem from ljspayperson where contno='"+ContNo+"' group by riskcode";
	    SSRS ljspayList = tExeSQL.execSQL(sql3);
	    double paySum = 0.0;
	    for(int i=0;i<ljspayList.MaxRow;i++){
	    	String riskPrem = ljspayList.GetText(i+1, 2);
	    	paySum = paySum + Double.parseDouble(riskPrem);
	    }
	    String getNoticeno = mLJSPaySchema.getGetNoticeNo();
	    String currentDate = PubFun.getCurrentDate();
	    
		LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
		
    	SSRS BankInfos = getBankAccCode();
    	String ZBankManageCom  ="";
    	if (BankInfos.GetText(1, 1).equals("86")){
    		ZBankManageCom ="86000000";
    	}else {
    	   ZBankManageCom= BankInfos.GetText(1, 1);//总公司机构
    	}
    	String ZBankAccNo = BankInfos.GetText(1, 2);
    	for(int i=0;i<ljspayList.MaxRow;i++){
    		String riskcode = ljspayList.GetText(i+1, 1); 
    		String riskPrem = ljspayList.GetText(i+1, 2);
    		LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema(); 
    		tLJTempFeeSchema.setTempFeeNo(getNoticeno);
    		tLJTempFeeSchema.setOtherNo(ContNo); //投保单号
    		tLJTempFeeSchema.setTempFeeType(tTempFeeType);//2-续期续保
    		tLJTempFeeSchema.setOtherNoType("4");	//4--印刷号
    		tLJTempFeeSchema.setRiskCode(riskcode);//默认000000
    		tLJTempFeeSchema.setPayIntv(tLCPolSet.get(1).getPayIntv());
    		tLJTempFeeSchema.setAPPntName(tLCPolSet.get(1).getAppntName());
    		tLJTempFeeSchema.setPayMoney(riskPrem);
    		tLJTempFeeSchema.setManageCom(ZBankManageCom);	//收费机构
    		tLJTempFeeSchema.setPolicyCom(mLCContSchema.getManageCom());	//管理机构
    		tLJTempFeeSchema.setPayDate(mCurrentDate);
    		tLJTempFeeSchema.setEnterAccDate(mCurrentDate);
    		tLJTempFeeSchema.setConfMakeDate(mCurrentDate);
    		tLJTempFeeSchema.setConfMakeTime(mCurrentTime);
    		tLJTempFeeSchema.setSaleChnl(mLCContSchema.getSaleChnl());
    		tLJTempFeeSchema.setAgentCom(mLCContSchema.getAgentCom());
    		tLJTempFeeSchema.setAgentGroup(mLCContSchema.getAgentGroup());
    		tLJTempFeeSchema.setAgentCode(mLCContSchema.getAgentCode());
    		tLJTempFeeSchema.setConfFlag("2");	//是否核销 0：没有财务核销 1：已有财务核销
    		tLJTempFeeSchema.setOperator(Operator);
    		tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
    		tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
    		tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
    		tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
    		tLJTempFeeSet.add(tLJTempFeeSchema);
    	}
		LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
		
    	LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
		mLJTempFeeClassSchema.setTempFeeNo(getNoticeno);
		mLJTempFeeClassSchema.setPayMode("12");
		mLJTempFeeClassSchema.setPayMoney(paySum);
		mLJTempFeeClassSchema.setAppntName(mLCContSchema.getAppntName());
		mLJTempFeeClassSchema.setPayDate(mCurrentDate);
		mLJTempFeeClassSchema.setEnterAccDate(mCurrentDate);
		mLJTempFeeClassSchema.setConfMakeDate(mCurrentDate);
		mLJTempFeeClassSchema.setConfMakeTime(mCurrentTime);
		mLJTempFeeClassSchema.setConfFlag("2"); //是否核销 0：没有财务核销 1：已有财务核销
		mLJTempFeeClassSchema.setManageCom(ZBankManageCom);
		mLJTempFeeClassSchema.setPolicyCom(mLCContSchema.getManageCom());
		mLJTempFeeClassSchema.setInsBankAccNo(ZBankAccNo);
    	String aBankCode = getBankCode(ZBankAccNo);//获取银行账号对应的bankcode
    	mLJTempFeeClassSchema.setBankCode(aBankCode);
    	mLJTempFeeClassSchema.setBankCode(mLCContSchema.getBankCode());
		mLJTempFeeClassSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLJTempFeeClassSchema.setAccName(mLCContSchema.getAccName());
		mLJTempFeeClassSchema.setOperator(Operator);
		mLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
		mLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
		mLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
		tLJTempFeeClassSet.add(mLJTempFeeClassSchema);
		
		//暂收生成之后将应收置为待核销状态
		LJSPayBDB tLJSPayBDB = new LJSPayBDB();
		tLJSPayBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
		if(!tLJSPayBDB.getInfo()){
			errLog("没有查询到该保单的应收记录备份表！");
			return false;
		}
		LJSPayBSchema tLJSPayBSchema = tLJSPayBDB.getSchema();
		tLJSPayBSchema.setDealState("4");
		
		map.put(tLJTempFeeSet,"DELETE&INSERT");
		map.put(tLJTempFeeClassSet,"DELETE&INSERT");
		map.put(tLJSPayBSchema,"UPDATE");
    	return true;
    }
    
    
    /**
     * 根据网销结算时总公司的机构编码和银行账户。
     * @param szFunc
     * @param szErrMsg
     */
	
    public SSRS getBankAccCode(){
    	String sql = "select code,codename from ldcode where codetype = 'InsBankAccNo' and othersign = 'DSLY'";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0)
    	{
        	return null;
    	}
        return tSSRS;
    }
    
    /**
     * 根据机构编码获取银行账户。
     * @param szFunc
     * @param szErrMsg
     */
    public String getBankCode(String aBankAccNo){
    	String sql = "select bankcode from ldfinbank where bankaccno = '"+aBankAccNo+"' ";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0)
    	{
        	return null;
    	}
        return tSSRS.GetText(1, 1);
    }
}
