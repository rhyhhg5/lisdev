package com.cbsws.xml.ctrl.complexpds;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.lis.cbcheck.UWAutoChkBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class WxAutoCheck implements XmlTag {
	private final Logger cLogger = Logger.getLogger(getClass());
	
	private final GlobalInput cGlobalInput;
	private final LCContSchema cLCContSchema;
	
	public WxAutoCheck(String pPrtNo, GlobalInput pGlobalInput) throws MidplatException {
		cGlobalInput = pGlobalInput;
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(pPrtNo);
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			throw new MidplatException("查询保单合同数据失败！");
		}
		cLCContSchema = mLCContSet.get(1);
	}
	
	public String deal() throws Exception {
		cLogger.info("Into WxAutoCheck.deal()...");

		/**
		 * 清理自核报错记录！(相同保单印刷号反复录单，存储自核报错时，存在主键冲突，在此提前清除可能存在的历史报错记录)
		 * ChenGB(陈贵菠) 2008.08.18
		 */
		MMap mMMap = new MMap();
		mMMap.put("delete from LCCUWError where ContNo='" + cLCContSchema.getContNo() + "' with ur", "DELETE"); // 保单级别自核报错
		mMMap.put("delete from LCUWError where ContNo='" + cLCContSchema.getContNo() + "' with ur", "DELETE"); // 险种级别自核报错
		VData mSubmitVData = new VData();
		mSubmitVData.add(mMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
//			throw new MidplatException("清理自核错误记录失败！");
			return "清理自核错误记录失败！";
		}
		
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("ContNo", cLCContSchema.getContNo());
		mTransferData.setNameAndValue("BankInsu", "1");
		
		VData mUWAutoChkBLData = new VData();
		mUWAutoChkBLData.add(cGlobalInput);
		mUWAutoChkBLData.add(cLCContSchema);
		//江苏中介校验
		String manageCom=cLCContSchema.getManageCom();
		if(manageCom.length()>2){
		String saleChnl=cLCContSchema.getSaleChnl();
		String manageCom2=cLCContSchema.getManageCom().substring(0, 4);
		String contNo=cLCContSchema.getContNo();
		System.out.println(contNo);
		System.out.println(manageCom2);
        System.out.println(saleChnl);
        if(manageCom2.equals("8632")){
        ExeSQL texe= new ExeSQL();
        String tsql="select  code  from ldcode  where  codetype='JSZJsalechnl'  and code='"+saleChnl+"' ";
        SSRS tssrs=texe.execSQL(tsql);
        int s=tssrs.MaxRow;
        System.out.println(s);
       // if(tssrs!=null&& s>0 ){
        	for(int i=1;i<=s;i++){
        		if(saleChnl.equals(tssrs.GetText(1, i))){
        			System.out.println("code"+tssrs.GetText(1,i));
        			ContInputAgentcomChkBL cacb=new ContInputAgentcomChkBL();
        			TransferData  tdata=new TransferData();
        			tdata.setNameAndValue("ContNo", contNo);
        			VData vdata=new VData();
        			vdata.add(tdata);           		   
        			if(!cacb.submitData(vdata, "check")){
        				String terror=cacb.mErrors.getError(0).errorMessage;
        				System.out.println(terror);
        				cLogger.error(terror);
        				return terror;
        			}
        		}
        //	}
         }
        }
		}
		  
		UWAutoChkBL mUWAutoChkBL = new UWAutoChkBL();
		cLogger.info("Start call UWAutoChkBL.submitData()...");
		long tStartMillism = System.currentTimeMillis();
		if (!mUWAutoChkBL.submitData(mUWAutoChkBLData, "submit")) {
//			throw new MidplatException(mUWAutoChkBL.mErrors.getFirstError());
			return mUWAutoChkBL.mErrors.getFirstError();
		}
		cLogger.info("UWAutoChkBL耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s");
		cLogger.info("End call UWAutoChkBL.submitData()!");
		
		
		
		/**
		 * 获取自核报错信息！(自核报错描述在数据库中，AutoUWCheckBL并不返回！) ChenGB(陈贵菠) 2008.08.18
		 */
		String mSQL = "select UWFlag from LCCont where ContNo='" + cLCContSchema.getContNo() + "' with ur";
		ExeSQL mExeSQL = new ExeSQL();
		if (!mExeSQL.getOneValue(mSQL).equals("9")) { // 自核标识(UWFlag)：9-通过；5-失败
			
			mSQL = "select UWError from LCCUWError where ContNo='" + cLCContSchema.getContNo() +"'";
			
			String tErrorMsg = "";
			SSRS s1=mExeSQL.execSQL(mSQL);
			if(s1.getMaxRow()!=0){
				for(int i=1;i<=s1.getMaxRow();i++){
					tErrorMsg+=s1.GetText(i, 1);
				}
			}
			if (!tErrorMsg.equals("")) {
//				throw new MidplatException(tErrorMsg);
				return tErrorMsg;
			}
			mSQL = "select UWError from LCUWError where ContNo='" + cLCContSchema.getContNo() + "' ";
			
//			throw new MidplatException(tErrorMsg);
			s1.Clear();
			s1=mExeSQL.execSQL(mSQL);
			if(s1.getMaxRow()!=0){
				for(int i=1;i<=s1.getMaxRow();i++){
					tErrorMsg+=s1.GetText(i, 1)+"||";
				}
			}
			return tErrorMsg;
		}
		
		//保单复核
		String mApproveDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mApproveTime = DateUtil.getCurDate("HH:mm:ss");
		mMMap = new MMap();
		mMMap.put("update lccont set ApproveFlag='9', ApproveDate='" + mApproveDate
				+ "', ApproveTime='" + mApproveTime
				+ "', ModifyTime='" + mApproveTime
				+ "' where contno='" + cLCContSchema.getContNo() + "'", "UPDATE");
		mMMap.put("update lcpol set ApproveFlag='9', ApproveDate='" + mApproveDate
				+ "', ApproveTime='" + mApproveTime
				+ "', ModifyTime='" + mApproveTime
				+ "' where contno='" + cLCContSchema.getContNo() + "'", "UPDATE");
		mSubmitVData = new VData();
		mSubmitVData.add(mMMap);
		mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
//			throw new MidplatException("保单复核失败！");
			return "保单复核失败！";
		}
		
		cLogger.info("Out WxAutoCheck.deal()!");
		return "";
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "D:/request/ICBC_std/UW.xml";
		
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mXmlDoc = new SAXBuilder().build(mIsr);
		
		Element mTranData = mXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild("BaseInfo");
		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
		mLCCont.getChild("PrtNo").setText(mProposalNo);
		mLCCont.getChild("ProposalContNo").setText(mPrtNo);
		
		GlobalInput mGlobalInput =  new GlobalInput();

		
		new WxAutoCheck(mLCCont.getChildTextTrim("PrtNo"), mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}
