package com.cbsws.xml.ctrl.blogic;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class ReceiveCardNoInfo {
	
	/** 错误处理类 */
    private CErrors mErrors = new CErrors();
    
	private LICertifySchema mLICertifySchema = new LICertifySchema();
	private VData mVData= new VData();
	/**传输到后台处理的map*/
	private MMap mMap = new MMap();
	/** 封装处理信息 */
	private String tAgentCom ="";
	private String tAgentCode ="";
	private String tAgentName ="";
	private String tManageCom ="";
	private String tOperator ="";
	private String tCertifyCode ="";
	private String tAgentFlag ="";
	private String tTradetype ="";
	private String cardNo[] =null;
	// 生成批次流水号
	private String tBatchNo = PubFun1.CreateMaxNo("LICBATCHNO","86000000",null);
	
	public boolean parseCardNoInfo(TransferData tTransferData){
		// 解析接收的卡号和相关信息进行封装
		if(tTransferData == null){
			buildError("parseCardNoInfo", " 没有传入所需的参数信息。");
			return false;
		}
		System.out.println("cardno=="+tTransferData.getValueByName("cardNo"));
		//cardNo
		if(tTransferData.getValueByName("cardNo")!=null){
			cardNo = (String []) tTransferData.getValueByName("cardNo");
		}else{
			buildError("parseCardNoInfo", " 没有传入所需的卡号信息。");
			return false;
		}
		//操作员 
		if(tTransferData.getValueByName("Operator")==null || tTransferData.getValueByName("Operator").equals("")){
			buildError("parseCardNoInfo", " 没有传入所需的操作员信息。");
			return false;
		}else{
			tOperator = (String) tTransferData.getValueByName("Operator");
		}
		int cardNolength = cardNo.length;
		CardNoInfoToLicertiry tCardNoInfoToLicertiry = new CardNoInfoToLicertiry();
		for(int i=0;i<cardNolength;i++){
			try{
				String sql = "select lzcn.CardNo, lzc.CertifyCode, lzc.SubCode, lzc.State, lzc.ReceiveCom,substr(lzc.ReceiveCom, 1, 1) deflag, "
								+"case substr(lzc.ReceiveCom, 1, 1) when 'D' then (select a.name from laagent a where agentcode = substr(lzc.ReceiveCom, 2)) else '' end agentname,"
								+"case substr(lzc.ReceiveCom, 1, 1) when 'D' then substr(lzc.ReceiveCom, 2) else '' end agentcode,"
								+"case substr(lzc.ReceiveCom, 1, 1) when 'E' then substr(lzc.ReceiveCom, 2) else '' end agentcom,"
								+"case substr(lzc.ReceiveCom, 1, 1) when 'D' then (select b.managecom from laagent b where agentcode = substr(lzc.ReceiveCom, 2))  when 'E' then (select c.managecom from lacom c where agentcom = substr(lzc.ReceiveCom, 2)) end managecom,"
								+"lmcd.operatetype "
								+" from LZCardNumber lzcn " 
								+"inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
								+"inner join lmcertifydes lmcd on lzc.certifycode=lmcd.certifycode"
								+" where 1 = 1 " 
								+"and lzcn.CardNo = '"+cardNo[i]+"'";
				System.out.println("sql=="+sql);
				SSRS tSSRS = new ExeSQL().execSQL(sql);
				if(tSSRS!=null){
					this.tAgentFlag   = tSSRS.GetText(1, 6);
					this.tCertifyCode = tSSRS.GetText(1, 2);
					this.tManageCom   = tSSRS.GetText(1, 10);
					this.tTradetype   = tSSRS.GetText(1, 11);
					if(this.tAgentFlag !=null && this.tAgentFlag.equals("E")){
						this.tAgentCom    = tSSRS.GetText(1, 9);
						SSRS eSSRS = new SSRS();
						
						/**查询中介机构对应的代理人*/
						String sql2="select lac.agentcode,laa.name from lacomtoagent lac,laagent laa where agentcom = '"+this.tAgentCom+"' and lac.agentcode=laa.agentcode order by agentcode desc fetch first 1 rows only ";
						
						eSSRS = new ExeSQL().execSQL(sql2);
						this.tAgentCode   = eSSRS.GetText(1, 1);
						this.tAgentName   = eSSRS.GetText(1, 2);
					}else{
						this.tAgentCode   = tSSRS.GetText(1, 8);
						this.tAgentName   = tSSRS.GetText(1, 7);
					}
					
				}else{
					buildError("parseCardNoInfo", " 没有取到卡号对应的信息。");
		            return false;
				}
				if(this.tAgentCode.equals("") || this.tAgentCode == null){
					System.out.println("没有得到所需的业务员代码");
					buildError("parseCardNoInfo", " 没有得到所需的业务员代码。");
					return false;
				}
				if(this.tBatchNo.equals("") || this.tBatchNo == null){
					System.out.println("生成tBatchNo失败");
					buildError("parseCardNoInfo", " 生成tBatchNo失败。");
					return false;
				}
				if(this.tCertifyCode.equals("") || this.tCertifyCode == null){
					System.out.println("没有得到tCertifyCode");
					buildError("parseCardNoInfo", " 没有得到tCertifyCode。");
					return false;
				}
				if(this.tManageCom.equals("") || this.tManageCom == null){
					System.out.println("没有得到tManageCom");
					buildError("parseCardNoInfo", " 没有得到tManageCom。");
					return false;
				}
				if(this.tTradetype.equals("") || this.tTradetype == null){
					System.out.println("没有得到tTradetype");
					buildError("parseCardNoInfo", " 没有得到tTradetype。");
					return false;
				}
				
				mLICertifySchema.setBatchNo(tBatchNo);
				mLICertifySchema.setDiskSeqNo(String.valueOf(i));
				System.out.println("第"+i+"卡号为===="+cardNo[i]);
				mLICertifySchema.setCardNo(cardNo[i]);
				mLICertifySchema.setAgentCom(tAgentCom);
				mLICertifySchema.setAgentCode(tAgentCode);
				mLICertifySchema.setAgentName(tAgentName);
				mLICertifySchema.setManageCom(tManageCom);
				mLICertifySchema.setCertifyCode(tCertifyCode);
				mLICertifySchema.setCertOperateType(tTradetype);
				mLICertifySchema.setOperator(tOperator);
				mVData.add(mLICertifySchema); 
				
				if(!tCardNoInfoToLicertiry.dealSubmitMap(mVData)){
					buildError("submitData", " 整理卡号待结算信息失败。");
		            return false;
				}
			}catch(Exception e){
				System.out.println("***************Start封装CardNo信息时出错***************");
				e.printStackTrace();
				System.out.println("***************end封装CardNo信息时出错*****************");
				return false;
			}
		}
		mMap = tCardNoInfoToLicertiry.getResult();
		if(mMap!=null){
			if (!submit())
	        {
	            return false;
	        }
		}else{
			buildError("submit", " 卡号待结算信息为空。");
            return false;
		}
		
		return true;
	}
	
	/**
	 * 保存CardNo信息
	 * @param tLICertifySchema
	 * @return
	 */
	private boolean submit(){
		mVData.add(mMap);
		PubSubmit p = new PubSubmit();		
		if (!p.submitData(mVData, null))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }
    	return true;
    	
    }
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CardNoInfoToLicertiry";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	
	public static void main (String args[]){
		TransferData cTransferData = new TransferData();
		String cardno[] = {"T6000030034"};
		cTransferData.setNameAndValue("cardNo", cardno);
		cTransferData.setNameAndValue("Operator", "myTest");
		ReceiveCardNoInfo rci= new ReceiveCardNoInfo();
		rci.parseCardNoInfo(cTransferData);
	}
}
