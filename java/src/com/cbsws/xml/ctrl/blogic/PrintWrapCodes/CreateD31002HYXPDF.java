package com.cbsws.xml.ctrl.blogic.PrintWrapCodes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

import com.cbsws.xml.ctrl.blogic.CreateHYXPDF;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

public class CreateD31002HYXPDF implements CreateHYXPDF{
	
	private Document doc;
	private static String out = null;
	private StringBuffer buffer_notice = new StringBuffer();
	private String notice = "";
	private StringBuffer buffer_discript1 = new StringBuffer();
	private String discript1 = "";
	private StringBuffer buffer_discript2 = new StringBuffer();
	private String discript2 = "";
	private StringBuffer buffer_discript3 = new StringBuffer();
	private String discript3 = "";
	private StringBuffer buffer_discript_bak = new StringBuffer();
	private String discript_bak = "";
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	
	private String cardno = "";
	private String ticketno = "";
	private String insuredname = "";
	private String insuredidno = "";
	private String amnt = "";
	private String prem = "";
	private String term = "";
	private String managecom = "";
	private String operator = "";
	
	private String currentdateAndtime = CurrentDate + " " +CurrentTime;
	public void initData(HashMap map){
		init(map);
		buffer_notice.append("1、在保险期间内乘坐的机票、火车、汽车票及本保险单是理赔的必要凭证，请妥善保管，如遗失请您立即通知出票点。\n");
		buffer_notice.append("2、本保险同一被保险人最多投保份数以叁份为限，本公司承担的保险金给付责任以叁份为限，超过部分无效，本保险不接受退保。\n");
		buffer_notice.append("3、本保险单在2013年12月31日前签发有效，本保险单无保险公司公章无效。\n");
		buffer_notice.append("4、本保险单为电脑打印，手写、涂改无效。\n");
		buffer_notice.append("5、为未成年子女投保的人身保险，因被保险人身故给付的保险金额总和不得超过国务院保险监督管理机构规定的限额，此保单的身故给付的保险金额总和约定也不得超过此规定限额。\n");
		buffer_notice.append("\n本保险单为电脑打印，手写、涂改无效\n");
		notice = buffer_notice.toString();
		
		buffer_discript1.append("神行无忧交通工具意外伤害保险简介\n");
		buffer_discript1.append("一、	保险对象：凡出生满28天至74周岁，均可向本公司投保本保险，成为本保险的被保险人。\n");
		buffer_discript1.append("二、	保险责任：本合同有效期内，被保险人以乘客身份乘坐从事合法客运的民航班机期间（自进入民航班机的舱门时起至走出民航班机的舱门止）、以乘客身份乘坐从事合法客运的火车（含轻轨、地铁）期间（自进入火车车厢起至走出火车车厢止）、被保险人以乘客身份乘坐从事合法客运的客车期间（自进入客车车厢起至走出客车车厢止）遭受的意外伤害事故并导致身故或残疾，本公司按保险单上的约定承担保险责任。\n");
		buffer_discript1.append("【意外身故保险金】被保险人自意外伤害事故发生之日起180天内因该次意外伤害事故直接导致身故的，本公司按保险单上所载的发生该类意外伤害事故所对应的保险金额给付意外身故保险金。\n");
		buffer_discript1.append("【意外伤残保险金】被保险人自意外伤害事故发生之日起180天内因该次意外伤害事故直接造成本合同所附“残疾程度与给付比例表”（见附表一）中所列伤残程度之一的，本公司按表中的给付比例" +
				"乘以发生该类意外伤害事故所对应的保险金额给付意外伤残保险金。如180天内治疗仍未结束的，本公司将按自事故发生之日起第180天的身体情况进行伤残鉴定，并据此给付意外伤残保险金。被保险人因遭受同一意外" +
				"伤害事故导致附表一中一项以上伤残时，本公司给付各项意外伤残保险金之和。但若不同伤残项目属于同一肢时，仅给付一项较严重项目的意外伤残保险金。被保险人该次意外伤害事故所致的伤残，如合并原有残疾后残" +
				"疾程度大于原有的残疾程度的，本公司按合并后残疾项目的给付比例与原有残疾项目的给付比例的差值，再乘以该类意外伤害事故的保险金额来确定给付意外伤残保险金。\n");
		buffer_discript1.append("保险期间内，本公司对每位被保险人发生同类意外伤害事故所负的给付保险金责任，以保险单载明的意外伤害事故的保险金额为限。\n");
		buffer_discript2.append("三、	责任免除\n");
		buffer_discript2.append("因下列情形之一引起的保险事故，本公司不承担给付保险金的责任：\n");
		buffer_discript2.append("1）投保人对被保险人的故意杀害、故意伤害；2）被保险人故意自伤、自杀（被保险人自杀时为无民事行为能力人的除外）、故意犯罪或者抗拒依法采取的刑事强制措施；3）被保险人流产、分娩；" +
				"4）被保险人因整容手术、药物过敏或其他医疗行为所致事故；5）被保险人未遵医嘱，服用、涂用、吸入或注射药物；6）被保险人违反承运人关于安全乘坐的规定；7）被保险人乘坐从事非法营运的交通工具；" +
				"8）被保险人在客车、轿车和火车的车厢外部，轮船的甲板之外，飞机的舱门之外所遭受的意外导致的身故或残疾；9）被保险人从事探险、特技表演、赛车等高风险职业活动；10）战争、军事冲突、暴乱或武装叛乱；" +
				"11）核爆炸、核辐射或核污染。被保险人在下列期间内发生的保险事故，本公司不承担给付保险金的责任：\n");
		buffer_discript2.append("1)醉酒、主动吸食或注射毒品；2)酒后驾驶、无合法有效驾驶证驾驶，或驾驶无有效行驶证的机动车；3）从事非法、犯罪活动期间或被依法拘留、服刑期间。\n");
		buffer_discript3.append("四、保险金的申请和给付\n");
		buffer_discript3.append("1、投保人或被保险人或受益人应在知道保险事故发生之日起10天内通知本公司，如果投保人、被保险人或受益人故意或者因重大过失未及时通知，致使保险事故的性质、原因、损失程度等难以确定的，" +
				"本公司对无法确定的部分，不承担给付保险金的责任，但本公司通过其他途径已经及时知道或者应当及时知道保险事故发生或者虽未及时通知但不影响本公司确定保险事故的性质、原因、损失程度的除外。\n");
		buffer_discript3.append("2、受益人向本公司请求给付保险金的诉讼时效期间为2年，自其知道或者应当知道保险事故发生之日起计算。\n");
		buffer_discript3.append("五、理赔须知\n");
		buffer_discript3.append("理赔时请根据申请的保险金类别准备保险金申请资料，详见下表\n");
		discript1 = buffer_discript1.toString();
		discript2 = buffer_discript2.toString();
		discript3 = buffer_discript3.toString();
		
		buffer_discript_bak.append("1．受益人的有效身份证件；2．由公安交通管理部门或承运人出具的意外事故证明；3．医院或其它合法的鉴定机构出具的被保险人身体残疾程度鉴定书；4．被保险人户籍注销证明；" +
				"5．被保险人因意外事故由人民法院宣告死亡的，还应提供法院宣告死亡判决书原件；6．国家卫生行政部门认定的医疗机构、公安部门或其他相关机构出具的被保险人的死亡证明；7．所能提供的与确认保险事故的性质、原因等有关的其他证明和资料。\n");
		buffer_discript_bak.append("本保险凭证为保险合同组成部分，适用条款为中国人民健康保险股份有限公司《畅行无忧交通工具意外伤害保险条款》为准。其它未尽事宜请参见相关保险条款，请在购买本保险前登陆公司网站www.picchealth.com,或全国客户服务热线：95591或4006695518查询。\n");
		discript_bak = buffer_discript_bak.toString();
	}
	public void Cpdf(HashMap map) {
		initData(map);
//		Rectangle pageSize = new Rectangle(289, 298);
		Rectangle pageSize = new Rectangle(289, 596);

		doc = new Document(pageSize);
		
		//Paragraph paragraph = null;

		try {

			// 定义输出位置并把文档对象装入输出对象中
			String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PDFstrUrl'";//生成文件的存放路径
	    	String filePath = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
	        System.out.println("生成文件的存放路径   "+filePath);//调试用－－－－－
	        if(filePath == null ||filePath.equals("")){
	            System.out.println("获取文件存放路径出错");
	        }
//	        filePath="E:\\";
			PdfWriter.getInstance(doc, new FileOutputStream(filePath+this.cardno+".pdf"));

			// 打开文档对象
			doc.addTitle("电子保险单");
			

			doc.open();

			
//			BaseFont bfComic = BaseFont.createFont("c:\\WINDOWS\\Fonts\\mingliu.ttc,0",  BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
			BaseFont bfComic = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", false);
			Font font = new Font(bfComic, 4);
			Font littlefont = new Font(bfComic, 4);
			Font dutyfont = new Font(bfComic, 4,Font.BOLD);
			Font fontTilte = new Font(bfComic, 5);
			
			Paragraph paragraph = new Paragraph("神行无忧交通意外伤害保险单", fontTilte);              
			paragraph.setAlignment(Element.ALIGN_CENTER);
//			Phrase p = new Phrase("");	
			doc.add(paragraph);
//			doc.add(p);
			//标志图片
			addLogo();
			// 设置 Table
			PdfPTable aTable = new PdfPTable(6);
			aTable.setWidthPercentage(100);

//			PdfPCell cell0_1 = new PdfPCell(new Phrase("单证编码：",font));
//			cell0_1.setBorder(0);
//			cell0_1.setColspan(5);
//			aTable.addCell(cell0_1);
//			
//			PdfPCell cell0_2= new PdfPCell(new Phrase("",font));
//			cell0_2.setBorder(0);
//			cell0_2.setColspan(1);
//			aTable.addCell(cell0_2);
			
			PdfPCell cell1_1 = new PdfPCell(new Phrase("保单号",font));
			cell1_1.setColspan(1);
			aTable.addCell(cell1_1);
			
			PdfPCell cell1_2 = new PdfPCell(new Phrase(this.cardno,font));
			cell1_2.setColspan(2);
			aTable.addCell(cell1_2);
			
			PdfPCell cell1_3 = new PdfPCell(new Phrase("电子客票号",font));
			cell1_3.setColspan(1);
			aTable.addCell(cell1_3);
			
			PdfPCell cell1_4 = new PdfPCell(new Phrase(this.ticketno,font));
			cell1_4.setColspan(2);
			aTable.addCell(cell1_4);
			
			PdfPCell cell2_1 = new PdfPCell(new Phrase("被保险人姓名",font));
			cell2_1.setColspan(1);
			aTable.addCell(cell2_1);
			
			PdfPCell cell2_2 = new PdfPCell(new Phrase(this.insuredname,font));
			cell2_2.setColspan(2);
			aTable.addCell(cell2_2);
			
			PdfPCell cell2_3 = new PdfPCell(new Phrase("被保险人身份证号码",font));
			cell2_3.setColspan(1);
			aTable.addCell(cell2_3);
			
			PdfPCell cell2_4 = new PdfPCell(new Phrase(this.insuredidno,font));
			cell2_4.setColspan(2);
			aTable.addCell(cell2_4);
			
			PdfPCell cell3_1 = new PdfPCell(new Phrase("保险金额",font));
			cell3_1.setColspan(1);
			aTable.addCell(cell3_1);
			
			PdfPCell cell3_2 = new PdfPCell(new Phrase(this.amnt,font));
			cell3_2.setColspan(2);
			aTable.addCell(cell3_2);
			
			PdfPCell cell4_1 = new PdfPCell(new Phrase("保险费",font));
			cell4_1.setColspan(1);
			aTable.addCell(cell4_1);
			
			PdfPCell cell4_2 = new PdfPCell(new Phrase(this.prem,font));
			cell4_2.setColspan(2);
			aTable.addCell(cell4_2);
			
			PdfPCell cell5_1 = new PdfPCell(new Phrase("保险期间",font));
			cell5_1.setColspan(1);
			aTable.addCell(cell5_1);
			
			PdfPCell cell5_2 = new PdfPCell(new Phrase(this.term,font));
			cell5_2.setColspan(5);
			aTable.addCell(cell5_2);
			
			PdfPCell cell6_1 = new PdfPCell(new Phrase("投保注意事项",font));
			cell6_1.setColspan(1);
			aTable.addCell(cell6_1);
			
			PdfPCell cell6_2 = new PdfPCell(new Phrase(this.notice,font));
			cell6_2.setColspan(5);
			aTable.addCell(cell6_2);
			
			
			PdfPCell cell7_1 = new PdfPCell(new Phrase("代理单位",font));
			cell7_1.setColspan(1);
			aTable.addCell(cell7_1);
			
			PdfPCell cell7_2 = new PdfPCell(new Phrase(this.managecom,font));
			cell7_2.setColspan(1);
			aTable.addCell(cell7_2);
			
			PdfPCell cell7_3 = new PdfPCell(new Phrase("经办人员",font));
			cell7_3.setColspan(1);
			aTable.addCell(cell7_3);
			
			PdfPCell cell7_4 = new PdfPCell(new Phrase(this.operator,font));
			cell7_4.setColspan(1);
			aTable.addCell(cell7_4);
			
			PdfPCell cell7_5 = new PdfPCell(new Phrase("出票时间",font));
			cell7_5.setColspan(1);
			aTable.addCell(cell7_5);
			
			PdfPCell cell7_6 = new PdfPCell(new Phrase(this.currentdateAndtime,font));
			cell7_6.setColspan(1);
			aTable.addCell(cell7_6);

			PdfPCell cell8_1 = new PdfPCell(new Phrase("被保险人（或法定监护人签名）",font));
			cell8_1.setColspan(1);
			aTable.addCell(cell8_1);
			
			PdfPCell cell8_2 = new PdfPCell(new Phrase("",font));
			cell8_2.setColspan(2);
			aTable.addCell(cell8_2);
			
			PdfPCell cell8_3 = new PdfPCell(new Phrase("投保人签名",font));
			cell8_3.setColspan(1);
			aTable.addCell(cell8_3);
			
			PdfPCell cell8_4 = new PdfPCell(new Phrase("",font));
			cell8_4.setColspan(2);
			aTable.addCell(cell8_4);
					
			doc.add(aTable);
			
//			加入图章
			addStamp();
//			doc.newPage();
			
			// 设置 Table
			PdfPTable tTable = new PdfPTable(1);
			tTable.setWidthPercentage(100);
			
			PdfPCell tcell = new PdfPCell(new Phrase(this.discript1,littlefont));
			tcell.setColspan(1);
			tcell.setBorder(0);
			tcell.setLeading(2, 1);
			tTable.addCell(tcell);
			
			PdfPCell tcell1 = new PdfPCell(new Phrase(this.discript2,dutyfont));
			tcell1.setColspan(1);
			tcell1.setBorder(0);
			tcell1.setLeading(2, 1);
			tTable.addCell(tcell1);
			
			PdfPCell tcell2 = new PdfPCell(new Phrase(this.discript3,littlefont));
			tcell2.setColspan(1);
			tcell2.setBorder(0);
			tcell2.setLeading(2, 1);
			tTable.addCell(tcell2);
			
			PdfPTable littleTable = new PdfPTable(2);
			PdfPCell littlecell1_1 = new PdfPCell(new Phrase("保险金类别",littlefont));
			littlecell1_1.setColspan(1);
			littleTable.addCell(littlecell1_1);
			
			PdfPCell littlecell1_2 = new PdfPCell(new Phrase("保险金申请资料",littlefont));
			littlecell1_2.setColspan(1);
			littleTable.addCell(littlecell1_2);
			
			PdfPCell littlecell2_1 = new PdfPCell(new Phrase("民航、火车（含轻轨、地铁）、合法客运的客车意外伤残保险金",littlefont));
			littlecell2_1.setColspan(1);
			littleTable.addCell(littlecell2_1);
			
			PdfPCell littlecell2_2 = new PdfPCell(new Phrase("1．2．3．7",littlefont));
			littlecell2_2.setColspan(1);
			littleTable.addCell(littlecell2_2);
			
			PdfPCell littlecell3_1 = new PdfPCell(new Phrase("民航、火车（含轻轨、地铁）、合法客运的客车意外身故保险金",littlefont));
			littlecell3_1.setColspan(1);
			littleTable.addCell(littlecell3_1);
			
			PdfPCell littlecell31_2 = new PdfPCell(new Phrase("1．2．4．5．6．7",littlefont));
			littlecell31_2.setColspan(1);
			littleTable.addCell(littlecell31_2);
			
			tTable.addCell(littleTable);
			
			PdfPCell ttcell = new PdfPCell(new Phrase(this.discript_bak,littlefont));
			ttcell.setColspan(1);
			ttcell.setBorder(0);
			ttcell.setLeading(1, 1);
			tTable.addCell(ttcell);
			
			doc.add(tTable);
			
			Phrase p2 = new Phrase("\n登陆公司网站或拨打全国客户服务热线（可即时查询保险单信息）",font);			
			doc.add(p2);

			System.out.println("create a PDF document!");
			
			// 关闭文档对象，释放资源

			doc.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (DocumentException e) {

			e.printStackTrace();

		} catch (IOException e) {
			
			e.printStackTrace();
		}

	}
	/**
	 * 添加logo
	 */
	private void addLogo(){
		String picPath = getProjectLocalPath() + "/common/images/logo_cardpolicy.jpg";
//		String picPath = "E:/logo_cardpolicy.jpg";
		Image logo;
		try {
			logo = Image.getInstance(picPath);
			logo.scalePercent(20);
			logo.setAlignment(Element.ALIGN_RIGHT);
			doc.add(logo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 加入电子章
	 */
	private void addStamp(){
		
		String picPath = getProjectLocalPath() + "/common/images/stamp.jpg";
//		String picPath = "E:/stamp.jpg";
		Image stamp;
		try {
			stamp = Image.getInstance(picPath);
			stamp.scalePercent(5);
			stamp.setAlignment(Element.ALIGN_RIGHT);
			doc.add(stamp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 获取项目所在路径
	 * @return				项目路径
	 * @throws Exception	未找到路径
	 */
	private String getProjectLocalPath() {
		String path = CreateD31002HYXPDF.class.getResource("").getFile();
		try {
			path = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		path = path.substring(0,path.lastIndexOf("/WEB-INF"));
		String temp = path.substring(0, 5);
		if("file:".equalsIgnoreCase(temp)){
			path = path.substring(5);
		}
		return path;
	}

	private void init(HashMap map){
		if(map.size()>0){
			this.cardno = (String)map.get("CARDNO");
			this.ticketno = (String)map.get("TICKETNO");
			this.insuredname = (String)map.get("INSUREDNAME");
			this.insuredidno = (String)map.get("INSUREDIDNO");
			this.amnt = (String)map.get("AMNT");
			this.prem = (String)map.get("PREM");
			this.term = (String)map.get("TERM");
			this.managecom = (String)map.get("MANAGECOM");
			this.operator = (String)map.get("OPERATOR");
		}
	}
	public static void main(String[] args) {
		HashMap map = new HashMap();
		CreateD31002HYXPDF pdf = new CreateD31002HYXPDF();
		pdf.Cpdf(map);
	}
}
