/**
 * 2010-8-25
 */
package com.cbsws.xml.ctrl.blogic;

import java.util.List;

import com.cbsws.obj.ECCertSalesInfoTable;
import com.cbsws.obj.ECCertSalesPayInfoTable;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertSalesPayInfoSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.vschema.LICertSalesPayInfoSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;
/**
 * @author LY
 *
 */
public class ECCertSalesBL extends ABusLogic
{
	/**后台传输数据对象*/
	private MMap tMMap = new MMap();
	private VData tVData = new VData();
	/**操作的数据对象*/
	private LICertSalesPayInfoSchema mLICertSalesPayInfoSchema = new LICertSalesPayInfoSchema();
    private LICertSalesPayInfoSet mLICertSalesPayInfoSet = new LICertSalesPayInfoSet();
    
    private LICertifySchema mLICertifySchema = new LICertifySchema();
    private LICertifySet mLICertifySet = new LICertifySet();
    private LICertifyDB mLICertifyDB = new LICertifyDB();
    
    private MsgHead mMsgHead = new MsgHead();
    private ECCertSalesPayInfoTable mECCertSalesPayInfoTable = new ECCertSalesPayInfoTable();
    private ECCertSalesInfoTable mECCertSalesInfoTable = new ECCertSalesInfoTable();
    
    private StringBuffer cardNoBuf = new StringBuffer();
    private String batchNo =null;
	
	protected boolean deal(MsgCollection cMsgInfos)
    {
    	try{
    		if(cMsgInfos==null){
   			 System.out.println("没有得到要保存的信息");
   	         errLog("没有得到要保存的信息");
   	         return false;
   		}
       	//处理头信息 
       	mMsgHead = cMsgInfos.getMsgHead();			
   		if(this.mMsgHead == null){
   			System.out.println("没有得到报文头信息");
   			errLog("没有得到要保存的信息");
   	        return false;
   		}
   		//得到batchNo
   		if(mMsgHead.getBatchNo() == null || mMsgHead.getBatchNo().equals("")){
   			System.out.println("没有得到batchNo");
   			errLog("没有得到batchNo");
   	        return false;
   		}
   		batchNo =mMsgHead.getBatchNo();
   		this.mLICertSalesPayInfoSchema.setBatchNo(mMsgHead.getBatchNo());
   		
   		//操作者
   		if(mMsgHead.getSendOperator() == null || mMsgHead.getSendOperator().equals("")){
   			System.out.println("没有得到SendOperator");
   			errLog("没有得到SendOperator");
   	        return false;
   		}
   		this.mLICertSalesPayInfoSchema.setOperator(mMsgHead.getSendOperator());
   		this.mLICertSalesPayInfoSchema.setMakeDate(PubFun.getCurrentDate());
   		this.mLICertSalesPayInfoSchema.setModifyDate(PubFun.getCurrentDate());
   		this.mLICertSalesPayInfoSchema.setMakeTime(PubFun.getCurrentTime());
   		this.mLICertSalesPayInfoSchema.setModifyTime(PubFun.getCurrentTime());
   		
   		//ECCertSalesPayInfoTable
   		List bodyList=cMsgInfos.getBodyByFlag("ECCertSalesPayInfoTable");
   		if(bodyList == null){
   			System.out.println("没有得到ECCertSalesPayInfoTable");
   			errLog("没有得到ECCertSalesPayInfoTable");
   	        return false;
   		}
   		if(bodyList.size()!=1){
   			System.out.println("ECCertSalesPayInfoTable信息大于一");
   			errLog("ECCertSalesPayInfoTable信息大于一");
   	        return false;
   		}
   		mECCertSalesPayInfoTable = (ECCertSalesPayInfoTable)bodyList.get(0);
   			if(this.mECCertSalesPayInfoTable.getAgentCode()==null || this.mECCertSalesPayInfoTable.getAgentCode().equals("")){
   				System.out.println("没有得到AgentCode");
   			}else{
   				AgentCodeTransformation a = new AgentCodeTransformation();
				if(!a.AgentCode(mECCertSalesPayInfoTable.getAgentCode(), "Y")){
					System.out.println(a.getMessage());
					errLog(a.getMessage());
					return false;
				}
   				this.mLICertSalesPayInfoSchema.setAgentCode(a.getResult());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getAgentCom()==null || this.mECCertSalesPayInfoTable.getAgentCom().equals("")){
   				System.out.println("没有得到AgentCom");
   			}else{
   				this.mLICertSalesPayInfoSchema.setAgentCom(mECCertSalesPayInfoTable.getAgentCom());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getAgentName()==null || this.mECCertSalesPayInfoTable.getAgentName().equals("")){
   				System.out.println("没有得到AgentName");
   			}else{
   				this.mLICertSalesPayInfoSchema.setAgentName(mECCertSalesPayInfoTable.getAgentName());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getCertifyCode()==null || this.mECCertSalesPayInfoTable.getCertifyCode().equals("")){
   				System.out.println("没有得到CertifyCode");
   			}else{
   				this.mLICertSalesPayInfoSchema.setCertifyCode(mECCertSalesPayInfoTable.getCertifyCode());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getCertifyName()==null || this.mECCertSalesPayInfoTable.getCertifyName().equals("")){
   				System.out.println("没有得到CertifyName");
   			}else{
   				this.mLICertSalesPayInfoSchema.setCertifyCode(mECCertSalesPayInfoTable.getCertifyName());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getDescBankAccCode()==null || this.mECCertSalesPayInfoTable.getDescBankAccCode().equals("")){
   				System.out.println("没有得到DescBankAccCode");
   			}else{
   				this.mLICertSalesPayInfoSchema.setDescBankAccCode(mECCertSalesPayInfoTable.getDescBankAccCode());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getDescBankAccName()==null || this.mECCertSalesPayInfoTable.getDescBankAccName().equals("")){
   				System.out.println("没有得到DescBankAccName");
   			}else{
   				this.mLICertSalesPayInfoSchema.setDescBankAccName(mECCertSalesPayInfoTable.getDescBankAccName());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getDescBankCode()==null || this.mECCertSalesPayInfoTable.getDescBankCode().equals("")){
   				System.out.println("没有得到DescBankCode");
   			}else{
   				this.mLICertSalesPayInfoSchema.setDescBankCode(mECCertSalesPayInfoTable.getDescBankCode());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getDescBankName()==null || this.mECCertSalesPayInfoTable.getDescBankName().equals("")){
   				System.out.println("没有得到DescBankName");
   			}else{
   				this.mLICertSalesPayInfoSchema.setDescBankName(mECCertSalesPayInfoTable.getDescBankName());
   			}
   			
   			//得到管理机构
   			if(this.mECCertSalesPayInfoTable.getManageCom()==null || this.mECCertSalesPayInfoTable.getManageCom().equals("")){
   				System.out.println("没有得到Managecom");
   	   			errLog("没有得到Managecom");
   	   	        return false;
   			}
   			this.mLICertSalesPayInfoSchema.setManageCom(mECCertSalesPayInfoTable.getManageCom());
   			
   			if(this.mECCertSalesPayInfoTable.getPayChnlName()==null || this.mECCertSalesPayInfoTable.getPayChnlName().equals("")){
   				System.out.println("没有得到PayChnlName");
   			}else{
   				this.mLICertSalesPayInfoSchema.setPayChnlName(mECCertSalesPayInfoTable.getPayChnlName());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getPayChnlType()==null || this.mECCertSalesPayInfoTable.getPayChnlType().equals("")){
   				System.out.println("没有得到PayChnlType");
   			}else{
   				this.mLICertSalesPayInfoSchema.setPayChnlType(mECCertSalesPayInfoTable.getPayChnlType());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getPayConfAccDate()==null || this.mECCertSalesPayInfoTable.getPayConfAccDate().equals("")){
   				System.out.println("没有得到PayConfAccDate");
   			}else{
   				this.mLICertSalesPayInfoSchema.setPayConfAccDate(mECCertSalesPayInfoTable.getPayConfAccDate());
   			}
   			
   			//得到支付时间
   			if(this.mECCertSalesPayInfoTable.getPayDate()==null || this.mECCertSalesPayInfoTable.getPayDate().equals("")){
   				System.out.println("没有得到PayDate");
   	   			errLog("没有得到PayDate");
   	   	        return false;
   			}
   			this.mLICertSalesPayInfoSchema.setPayDate(mECCertSalesPayInfoTable.getPayDate());
   			
   			if(this.mECCertSalesPayInfoTable.getPayEntAccDate()==null || this.mECCertSalesPayInfoTable.getPayEntAccDate().equals("")){
   				System.out.println("没有得到PayEntAccDate");
   			}else{
   				this.mLICertSalesPayInfoSchema.setPayEntAccDate(mECCertSalesPayInfoTable.getPayEntAccDate());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getPayMode()==null || this.mECCertSalesPayInfoTable.getPayMode().equals("")){
   				System.out.println("没有得到PayMode");
   			}else{
   				this.mLICertSalesPayInfoSchema.setPayMode(mECCertSalesPayInfoTable.getPayMode());
   			}
   			
   			//得到支付金额
   			if(this.mECCertSalesPayInfoTable.getPayMoney()==null || this.mECCertSalesPayInfoTable.getPayMoney().equals("")){
   				System.out.println("没有得到PayMoney");
   	   			errLog("没有得到PayMoney");
   	   	        return false;
   			}
   			this.mLICertSalesPayInfoSchema.setPayMoney(mECCertSalesPayInfoTable.getPayMoney());
   			
   			//交易流水
   			if(this.mECCertSalesPayInfoTable.getPayTradeSeq()==null || this.mECCertSalesPayInfoTable.getPayTradeSeq().equals("")){
   				System.out.println("没有得到PayTradeSeq");
   	   			errLog("没有得到PayTradeSeq");
   	   	        return false;
   			}
   			this.mLICertSalesPayInfoSchema.setPayTradeSeq(mECCertSalesPayInfoTable.getPayTradeSeq());
   			
   			if(this.mECCertSalesPayInfoTable.getRemark()==null || this.mECCertSalesPayInfoTable.getRemark().equals("")){
   				System.out.println("没有得到Remark");
   			}else{
   				this.mLICertSalesPayInfoSchema.setRemark(mECCertSalesPayInfoTable.getRemark());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getSaleChnl()==null || this.mECCertSalesPayInfoTable.getSaleChnl().equals("")){
   				System.out.println("没有得到SaleChnl");
   			}else{
   				this.mLICertSalesPayInfoSchema.setSaleChnl(mECCertSalesPayInfoTable.getSaleChnl());
   			}
   			
   			
   			if(this.mECCertSalesPayInfoTable.getSaleChnlName()==null || this.mECCertSalesPayInfoTable.getSaleChnlName().equals("")){
   				System.out.println("没有得到SaleChnlName");
   			}else{
   				this.mLICertSalesPayInfoSchema.setSaleChnlName(mECCertSalesPayInfoTable.getSaleChnlName());
   			}
   			
   			//得到套餐代码
   			if(this.mECCertSalesPayInfoTable.getWrapCode()==null || this.mECCertSalesPayInfoTable.getWrapCode().equals("")){
   				System.out.println("没有得到WrapCode");
   	   			errLog("没有得到WrapCode");
   	   	        return false;
   			}
   			this.mLICertSalesPayInfoSchema.setWrapCode(mECCertSalesPayInfoTable.getWrapCode());
   			
   			if(this.mECCertSalesPayInfoTable.getWrapName()==null || this.mECCertSalesPayInfoTable.getWrapName().equals("")){
   				System.out.println("没有得到WrapName");
   			}else{
   				this.mLICertSalesPayInfoSchema.setWrapName(mECCertSalesPayInfoTable.getWrapName());
   			}
   			
   			//得到购买份数
   			if(this.mECCertSalesPayInfoTable.getSalesCount()==null || this.mECCertSalesPayInfoTable.getSalesCount().equals("")){
   				System.out.println("没有得到SalesCount");
   	   			errLog("没有得到SalesCount");
   	   	        return false;
   			}
   			this.mLICertSalesPayInfoSchema.setSalesCount(mECCertSalesPayInfoTable.getSalesCount());
   			
   			//得到购买时间
   			if(this.mECCertSalesPayInfoTable.getSalesDate()==null || this.mECCertSalesPayInfoTable.getSalesDate().equals("")){
   				System.out.println("没有得到SalesDate");
   	   			errLog("没有得到SalesDate");
   	   	        return false;
   			}
   			this.mLICertSalesPayInfoSchema.setSalesDate(mECCertSalesPayInfoTable.getSalesDate());
   			
   		//ECCertSalesInfoTable
   		List bodyList2 = cMsgInfos.getBodyByFlag("ECCertSalesInfoTable");
   		if(bodyList2 == null || bodyList2.size()<1){
   			System.out.println("没有得到ECCertSalesInfoTable");
	   		errLog("没有得到ECCertSalesInfoTable");
	   	    return false;
   		}
   		
   		// 校验卡号对应的清单信息 
   		LICertifySet tCertSet = new LICertifySet();
   		
   		for(int i=0;i<bodyList2.size();i++)
   		{
   			String tCardNo = ((ECCertSalesInfoTable)bodyList2.get(i)).getCardNo();
   			if(tCardNo==null){
   				System.out.println("没有得到CardNo");
   	   			errLog("没有得到CardNo");
   	   			return false;
   			}
   				
   			LICertifyDB tCertDb = new LICertifyDB();
   			tCertDb.setCardNo(tCardNo);
   			if(!tCertDb.getInfo())
   			{
   				System.out.println("没有得到CardNo为"+tCardNo+"的清单信息");
   	   			errLog("没有得到CardNo为"+tCardNo+"的清单信息");
   	   	        return false;
   			}
   			cardNoBuf.append(tCardNo+"|");
   			LICertifySchema tCertSch = tCertDb.getSchema();
   			tCertSch.setPayTradeBatchNo(this.batchNo);
   			tCertSch.setPayTradeStatus("00");
   			tCertSch.setPayTradeMakeDate(this.mLICertSalesPayInfoSchema.getPayDate());
   			tCertSet.add(tCertSch);
   		}
   		String cardNoRela = cardNoBuf.toString();
   		cardNoRela= cardNoRela.substring(0,cardNoRela.length()-1);
   		mLICertSalesPayInfoSchema.setRelaInfoList(cardNoRela);
   		this.tMMap.put(mLICertSalesPayInfoSchema, SysConst.INSERT);
   		this.tMMap.put(tCertSet, SysConst.UPDATE);
   		this.tVData.add(this.tMMap);
   		
   		PubSubmit p = new PubSubmit();
   		if(!p.submitData(this.tVData, "")){
   			System.out.println("********提交失败********");
	   			errLog("********提交失败********");
	   	        return false;
   		}
    	}catch(Exception e){
    		e.printStackTrace();
    		return false;
    	}
		
		
    	return true;
    }
}
