package com.cbsws.xml.ctrl.blogic;

import java.io.FileInputStream;
import java.io.InputStream;

import com.cbsws.core.services.PubServiceProxy;
import com.sinosoft.midplat.channel.util.IOTrans;

public class ReceiveCardSalesInfo {
	

    public static void main(String[] args)
    {
    	try
        {                                       
            String mInFilePath = "D:/ec_proxy2.xml"; // 本地测试报文，本地报文的文件编码必须为utf-8。

            InputStream mIs = new FileInputStream(mInFilePath);
            byte[] mInXmlBytes = IOTrans.InputStreamToBytes(mIs);
            String mInXmlStr = new String(mInXmlBytes, "GBK");

            PubServiceProxy tPServProxy = new PubServiceProxy();
            String p = tPServProxy.service(mInXmlStr);

            System.out.println("return:" + p);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
