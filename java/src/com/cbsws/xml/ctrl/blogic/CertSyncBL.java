/**
 * 2010-8-23
 */
package com.cbsws.xml.ctrl.blogic;

import java.util.List;

import com.cbsws.obj.CertSyncSetTable;
import com.cbsws.obj.CertSyncTable;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.ecwebservice.services.ReceiveCardNoInfo;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;

/**
 * @author LY
 *
 */
public class CertSyncBL extends ABusLogic
{
    protected boolean deal(MsgCollection cMsgInfos)
    {
        List tCertSyncList = cMsgInfos.getBodyByFlag("CertSyncTable");
        if (tCertSyncList == null || tCertSyncList.size() != 1)
        {
            errLog("不存在[CertSyncTable]节点信息。");
            return false;
        }

        for (int i = 0; i < tCertSyncList.size(); i++)
        {
            CertSyncTable tCertSyncTable = (CertSyncTable) tCertSyncList.get(i);

            String tManagCom = tCertSyncTable.getManageCom();
            String tWrapCode = tCertSyncTable.getWrapCode();

            String tCertifyCode = getCertifyCode(tWrapCode);
            if (tCertifyCode == null)
            {
                return false;
            }

            int tSyncCount = 0;
            try
            {
                tSyncCount = Integer.parseInt(tCertSyncTable.getSyncCount());
            }
            catch (Exception e)
            {
                errLog("申请单证的数量不是整数。");
                return false;
            }

            //date 20101109 by 郭忠华 调整逻辑
            String tStrSql = " select tmp.RowId, tmp.CardNo, tmp.CardPassword   "
                    + " from  "
                    + " (  "
                    + " select row_number() over() as RowId, lzcn.CardNo, lzcn.CardPassword, lzc.CertifyCode, lzc.SubCode, lzc.State, lzc.ReceiveCom  "
                    + " from LZCardNumber lzcn  "
                    + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo  "
                    + " where 1 = 1  " 
                    + " and lzc.CertifyCode = '" + tCertifyCode + "'  "
                    + " and lzc.State in ('10', '11') " 
                    + " and (case substr(lzc.ReceiveCom, 1, 1) " 
                    + " when 'D' then (select managecom from laagent where agentcode = substr(lzc.ReceiveCom, 2))"
			        + " when 'E' then (select managecom from lacom where agentcom = substr(lzc.ReceiveCom, 2)) end)"
			        + " = '"+tManagCom+"' " 
			        + " ) as tmp  "  
                    + " where 1 = 1 "	
                    +" and tmp.RowId <= "
                    + tSyncCount + "  " + " order by CardNo ";

            SSRS tCardInfo = new ExeSQL().execSQL(tStrSql);
            if (tCardInfo == null || tCardInfo.MaxRow < tSyncCount)
            {
                errLog("可用单证数量不足申请数量【" + tSyncCount + "】张。");
                return false;
            }

            String[] tCardNoInfo = new String[tCardInfo.MaxRow];
            for (int j = 1; j <= tCardInfo.MaxRow; j++)
            {
                tCardNoInfo[j - 1] = tCardInfo.GetText(j, 2);
            }

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("cardNo", tCardNoInfo);
            MsgHead tMsgHead = cMsgInfos.getMsgHead();
            tTransferData.setNameAndValue("Operator", tMsgHead.getSendOperator());

            ReceiveCardNoInfo tReCardImportBL = new ReceiveCardNoInfo();
            if (!tReCardImportBL.parseCardNoInfo(tTransferData))
            {
                errLog("获取可用单证信息失败。");
                return false;
            }

            for (int j = 1; j <= tCardInfo.MaxRow; j++)
            {
                String[] tTmpCardInfo = tCardInfo.getRowData(j);
                CertSyncSetTable tSubCertInfo = new CertSyncSetTable();
                tSubCertInfo.setCardNo(tTmpCardInfo[1]);
                tSubCertInfo.setPassword(tTmpCardInfo[2]);
                putResult("CertSyncSetTable", tSubCertInfo);
            }
        }

        return true;
    }

    private String getCertifyCode(String cWrapCode)
    {
        String tStrSql = " select trim(lmcd.CertifyCode) from LMCardRisk lmcr "
                + " inner join LMCertifyDes lmcd on lmcd.CertifyCode = lmcr.CertifyCode "
                + " where 1 = 1 and lmcd.operatetype='4' and lmcr.RiskType = 'W' " + " and lmcr.RiskCode = '" + cWrapCode + "' ";
        SSRS tSSCertCode = new ExeSQL().execSQL(tStrSql);
        if (tSSCertCode == null || tSSCertCode.MaxRow != 1)
        {
            errLog("获取单证编码失败，或该产品可能对应多条单证信息。");
            return null;
        }
        return tSSCertCode.GetText(1, 1);
    }
}
