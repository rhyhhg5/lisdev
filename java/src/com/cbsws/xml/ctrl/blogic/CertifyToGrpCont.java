package com.cbsws.xml.ctrl.blogic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.certifybusiness.CertifyContConst;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LDRiskWrapDB;
import com.sinosoft.lis.db.LIBCertifyDB;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.f1print.PrintPDFManagerBL;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDRiskWrapSchema;
import com.sinosoft.lis.schema.LIBCertifySchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCCoInsuranceParamSet;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LDGrpSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.lis.vschema.LIBCertifySet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CertifyToGrpCont {
	
	public CertifyToGrpCont(){
		
	}
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
	
	/** 传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    
    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
    
    private FDate mFDate = new FDate();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 团单印刷号（结算单号） */
    private String mPrtNo = null;

    /** 团单机构 */
    private String mContManageCom = null;

    /** 团单产品类型（单证类型） */
    private String mCertifyCode = null;

    /** 团单销售渠道 */
    private String mSaleChnl = null;

    /** 销售业务员代码 */
    private String mAgentCode = null;

    /** 销售中介机构 */
    private String mAgentCom = null;

    /** 团单缴费方式 */
    private String mPayMode = null;

    /** 团单缴费频次 */
    private String mPayIntv = null;

    /** 团单生效日期 */
    private String mCValidate = null;

    /** 团单终止日期 */
    private String mCInValidate = null;

    /** 团体投保人信息 */
    private String mAppntName = null;
    
    /**是否为共保保单*/
    private String mFlag = null;

    private LDRiskWrapSet mRiskWrapDes = null;

    private LCGrpContSchema mGrpContInfo = new LCGrpContSchema();

    private LCGrpAppntSchema mGrpAppntInfo = new LCGrpAppntSchema();

    private LDGrpSchema mGrpInfo = null;

    private LCGrpAddressSchema mGrpAddressInfo = null;

    private LCGrpPolSet mGrpPolInfo = new LCGrpPolSet();

    private LCCoInsuranceParamSet mLCCoInsuranceParamSet = null;

    private LCContPlanDutyParamSet mGrpPlanDutyParams = new LCContPlanDutyParamSet();

    private LCContPlanSet mGrpPlanInfo = new LCContPlanSet();

    private LCContPlanRiskSet mGrpPlanRiskInfo = new LCContPlanRiskSet();

    private LCInsuredListSet mNoNameInsuredInfo = new LCInsuredListSet();
    
    private LJTempFeeSchema dLJTempFeeSchema = null;
    
    private LJTempFeeSet dLJTempFeeSet = new LJTempFeeSet();
    
    private LJTempFeeClassSchema dLJTempFeeClassSchema= new LJTempFeeClassSchema();
    
    private LJTempFeeClassSet dLJTempFeeClassSet= new LJTempFeeClassSet();

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }
    
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }
    
    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }
        
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        if (mPrtNo == null || mPrtNo.equals(""))
        {
            buildError("getInputData", "获取结算单号失败。");
            return false;
        }

        mContManageCom = (String) mTransferData.getValueByName("ManageCom");
        if (mContManageCom == null || mContManageCom.equals(""))
        {
            buildError("getInputData", "获取结算机构代码失败。");
            return false;
        }

        mAgentCode = (String) mTransferData.getValueByName("AgentCode");
        if (mAgentCode == null || mAgentCode.equals(""))
        {
            buildError("getInputData", "获取业务员代码失败。");
            return false;
        }

        mCertifyCode = (String) mTransferData.getValueByName("CertifyCode");
        if (mCertifyCode == null || mCertifyCode.equals(""))
        {
            buildError("getInputData", "获取团体合同单证编码失败。");
            return false;
        }

        mSaleChnl = (String) mTransferData.getValueByName("SaleChnl");
        if (mSaleChnl == null || mSaleChnl.equals(""))
        {
            buildError("getInputData", "获取团体合同销售渠道失败。");
            return false;
        }

        mAgentCom = (String) mTransferData.getValueByName("AgentCom");

        mAppntName = (String) mTransferData.getValueByName("AppntName");

        mPayMode = (String) mTransferData.getValueByName("PayMode");
        if (mPayMode == null || mPayMode.equals(""))
        {
            buildError("getInputData", "获取缴费方式失败。");
            return false;
        }

        mPayIntv = (String) mTransferData.getValueByName("PayIntv");
        if (mPayIntv == null || mPayIntv.equals(""))
        {
            buildError("getInputData", "获取缴费频次失败。");
            return false;
        }

        mCValidate = (String) mTransferData.getValueByName("CValidate");
        if (mCValidate == null || mCValidate.equals(""))
        {
            buildError("getInputData", "获取团体合同生效日期失败。");
            return false;
        }

        mCInValidate = (String) mTransferData.getValueByName("CInValidate");
        if (mCInValidate == null || mCInValidate.equals(""))
        {
            buildError("getInputData", "获取团体合同失效日期失败。");
            return false;
        }

//        mCrs_SaleChnl = (String) mTransferData.getValueByName("Crs_SaleChnl");
//        mCrs_BussType = (String) mTransferData.getValueByName("Crs_BussType");
//        mGrpAgentCom = (String) mTransferData.getValueByName("GrpAgentCom");
//        mGrpAgentCode = (String) mTransferData.getValueByName("GrpAgentCode");
//        mGrpAgentName = (String) mTransferData.getValueByName("GrpAgentName");
//        mGrpAgentIDNo = (String) mTransferData.getValueByName("GrpAgentIDNo");
        mFlag = (String) mTransferData.getValueByName("Flag");//是否为共保保单???
        if (!"".equals(mFlag) && null != mFlag)
        {
            if ("1".equals(mFlag) || "1" == mFlag)
            {
                mLCCoInsuranceParamSet = (LCCoInsuranceParamSet) cInputData
                        .getObjectByObjectName("LCCoInsuranceParamSet", 0);
            }
        }

        return true;
    }
    
    private boolean checkData()
    {
        String tTmpSql = " select 1 from " + " (" + " select " + " ( "
                + " select nvl(sum(lict.Prem), 0) " + " from LICertify lict "
                + " where 1 = 1 " + " and lict.PrtNo = '" + mPrtNo + "' "
                + " ) " + " - " + " (" + " select nvl(sum(libct.Prem), 0) "
                + " from LIBCertify libct" + " where 1 = 1 "
                + " and libct.PrtNo = '" + mPrtNo + "' " + " ) SubPrem "
                + " from dual " + " ) as tmp " + " where 1 = 1 "
                + " and tmp.SubPrem <= 0 ";
        String tResult = new ExeSQL().getOneValue(tTmpSql);

        if ("1".equals(tResult))
        {
            String tStrErr = "结算单的保费不能小于等于0。";
            buildError("dealSaleInfo", tStrErr);
            return false;
        }

        return true;
    }
    
    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Create".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = createGrpCont();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }
    
    /**
     * 根据结算清单创建团体无名单。
     * @return
     */
    private MMap createGrpCont()
    {
        MMap tMMap = new MMap();

        MMap tTmpMap = null;
        
        lockCont(this.mPrtNo);

        // 创建团体合同层级基本数据。
        if (!createGrpContBaseInfo())
        {
            return null;
        }
        // ---------------------

        // 处理销售信息。
        if (!dealSaleInfo())
        {
            return null;
        }
        // ---------------------

        // 处理投保单位信息。
        if (!dealGrpAppntInfo())
        {
            return null;
        }
        // ---------------------

        // 处理团体保障计划信息。
        if (!dealGrpPlanInfo())
        {
            return null;
        }
        // ---------------------

        // 生成无名单待导入数据。
        if (!dealNoNameInsuredList())
        {
            return null;
        }
        // ---------------------

        // 回置单证状态
        tTmpMap = null;
        tTmpMap = dealCertifyListState();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ---------------------

        // 生成缴费通知书，同时生成暂收信息。
        tTmpMap = null;
        tTmpMap = sendPayPrint();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        //-----------------------
        
        // 把处理后的数据装载到事务中。
        tMMap.put(mGrpContInfo, SysConst.INSERT);
        mResult.add(mGrpContInfo);

        tMMap.put(mGrpAppntInfo, SysConst.INSERT);
        mResult.add(mGrpAppntInfo);

        if (mGrpInfo != null)
        {
            tMMap.put(mGrpInfo, SysConst.INSERT);
        }
        mResult.add(mGrpInfo);

        if (mGrpAddressInfo != null)
        {
            tMMap.put(mGrpAddressInfo, SysConst.INSERT);
        }
        mResult.add(mGrpAddressInfo);

        tMMap.put(mGrpPolInfo, SysConst.INSERT);
        mResult.add(mGrpPolInfo);

        tMMap.put(mGrpPlanInfo, SysConst.INSERT);
        mResult.add(mGrpPlanInfo);

        tMMap.put(mGrpPlanRiskInfo, SysConst.INSERT);
        mResult.add(mGrpPlanRiskInfo);

        tMMap.put(mGrpPlanDutyParams, SysConst.INSERT);
        mResult.add(mGrpPlanDutyParams);

        tMMap.put(mNoNameInsuredInfo, SysConst.INSERT);
        mResult.add(mNoNameInsuredInfo);
        // ---------------------

        return tMMap;
    }
    
    /**
     * 创建保单层业务数据。
     * @return
     */
    private boolean createGrpContBaseInfo()
    {
        mGrpContInfo.setPrtNo(mPrtNo);
        mGrpContInfo.setManageCom(mContManageCom);

        mGrpContInfo.setPayMode(mPayMode);
        mGrpContInfo.setPayIntv(mPayIntv);

        mGrpContInfo.setCValiDate(mCValidate);
        mGrpContInfo.setCInValiDate(mCInValidate);

        mGrpContInfo.setSaleChnl(mSaleChnl);
        mGrpContInfo.setAgentCode(mAgentCode);
        mGrpContInfo.setAgentCom(mAgentCom);

//        mGrpContInfo.setCrs_SaleChnl(mCrs_SaleChnl);
//        mGrpContInfo.setCrs_BussType(mCrs_BussType);
//
//        mGrpContInfo.setGrpAgentCom(mGrpAgentCom);
//        mGrpContInfo.setGrpAgentCode(mGrpAgentCode);
//        mGrpContInfo.setGrpAgentName(mGrpAgentName);
//        mGrpContInfo.setGrpAgentIDNo(mGrpAgentIDNo);

        mGrpContInfo.setInputOperator(mGlobalInput.Operator);
        mGrpContInfo.setInputDate(PubFun.getCurrentDate());
        mGrpContInfo.setInputTime(PubFun.getCurrentTime());

        String tApproveFlag = "9";
        mGrpContInfo.setApproveFlag(tApproveFlag);
        mGrpContInfo.setApproveCode(mGlobalInput.Operator);//复核人编码
        mGrpContInfo.setApproveDate(PubFun.getCurrentDate());
        mGrpContInfo.setApproveTime(PubFun.getCurrentTime());

        String tUWFlag = "9";
        mGrpContInfo.setUWFlag(tUWFlag);
        mGrpContInfo.setUWOperator(mGlobalInput.Operator);//核保人
        mGrpContInfo.setUWDate(PubFun.getCurrentDate());
        mGrpContInfo.setUWTime(PubFun.getCurrentTime());
        mGrpContInfo.setCoInsuranceFlag(mFlag);//????????

        mGrpContInfo.setAppFlag("0");
        mGrpContInfo.setPolApplyDate(PubFun.getCurrentDate());

        mGrpContInfo.setStateFlag("0");

        // 卡式业务标志。
        mGrpContInfo.setCardFlag(CertifyContConst.LGW_CC_CONTFLAG);//????
        // ---------------------------

        mGrpContInfo.setOperator(mGlobalInput.Operator);
        mGrpContInfo.setMakeDate(PubFun.getCurrentDate());
        mGrpContInfo.setMakeTime(PubFun.getCurrentTime());
        mGrpContInfo.setModifyDate(PubFun.getCurrentDate());
        mGrpContInfo.setModifyTime(PubFun.getCurrentTime());
        
        //gzh  增加市场类型为1
        mGrpContInfo.setMarketType("1");

        return true;
    }
    
    /**
     * 处理保单层业务员相关信息。
     * @return
     */
    private boolean dealSaleInfo()
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mGrpContInfo.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            String tStrErr = "代理人编码输入错误。";
            buildError("dealSaleInfo", tStrErr);
            return false;
        }

        if (tLAAgentDB.getManageCom() == null)
        {
            String tStrErr = "代理人编码对应数据库中的管理机构为空！";
            buildError("dealSaleInfo", tStrErr);
            return false;
        }

//        if (!tLAAgentDB.getManageCom().equals(mGrpContInfo.getManageCom()))
//        {
//            String tStrErr = "录入的管理机构和数据库中代理人编码对应的管理机构不符合！";
//            buildError("dealSaleInfo", tStrErr);
//            return false;
//        }

        if (mGrpContInfo.getAgentCom() != null
                && !mGrpContInfo.getAgentCom().equals(""))
        {
            LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(mGrpContInfo.getAgentCom());
            if (!tLAComDB.getInfo())
            {
                String tStrErr = "代理机构编码输入错误。";
                buildError("dealSaleInfo", tStrErr);
                return false;
            }

            if (tLAComDB.getManageCom() == null)
            {
                String tStrErr = "代理机构编码对应数据库中的管理机构为空！";
                buildError("dealSaleInfo", tStrErr);
                return false;
            }

            if (!tLAComDB.getManageCom().equals(mGrpContInfo.getManageCom()))
            {
                String tStrErr = "录入的管理机构和数据库中代理机构编码对应的管理机构不符合！";
                buildError("dealSaleInfo", tStrErr);
                return false;
            }
        }

        mGrpContInfo.setAgentGroup(tLAAgentDB.getAgentGroup());
        mGrpContInfo.setSaleChnlDetail(tLAAgentDB.getBranchType2());//网销？？？01

        return true;
    }
    
    /**
     * 处理投保人信息。
     * @return
     */
    private boolean dealGrpAppntInfo()
    {
        String tGrpAppntName = null;

        if (mAppntName == null || mAppntName.equals(""))
        {
            if (mGrpContInfo.getAgentCom() != null
                    && !mGrpContInfo.getAgentCom().equals(""))
            {
                String tAgentCom = mGrpContInfo.getAgentCom();
                LAComDB tLAComDB = new LAComDB();
                tLAComDB.setAgentCom(tAgentCom);
                if (!tLAComDB.getInfo())
                {
                    String tStrErr = "代理机构信息查询失败。";
                    buildError("dealGrpAppntInfo", tStrErr);
                    return false;
                }

                tGrpAppntName = tLAComDB.getName();
            }
            else
            {
                String tAgentGroup = mGrpContInfo.getAgentGroup();
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tAgentGroup);
                if (!tLABranchGroupDB.getInfo())
                {
                    String tStrErr = "代理人组别信息查询失败。";
                    buildError("dealGrpAppntInfo", tStrErr);
                    return false;
                }

                tGrpAppntName = tLABranchGroupDB.getName();
            }
        }
        else
        {
            tGrpAppntName = mAppntName;
        }

        if (tGrpAppntName == null || tGrpAppntName.equals(""))
        {
            String tStrErr = "获取投保机构信息失败。";
            buildError("dealGrpAppntInfo", tStrErr);
            return false;
        }

        // 处理投保单位信息。
        String tStrGrpSql = "select * from LDGrp ldg where ldg.GrpName = '"
                + tGrpAppntName
                + "' order by ldg.ModifyDate desc, ldg.ModifyTime desc";
        LDGrpSet tLDGrpSet = new LDGrpDB().executeQuery(tStrGrpSql);

        if (tLDGrpSet.size() > 0)
        {
            LDGrpSchema tLDGrpSchema = tLDGrpSet.get(1);

            String tCustomerNo = tLDGrpSchema.getCustomerNo();
            String tGrpName = tLDGrpSchema.getGrpName();

            mGrpAppntInfo.setCustomerNo(tCustomerNo);
            mGrpAppntInfo.setName(tGrpName);

            mGrpContInfo.setAppntNo(tCustomerNo);
            mGrpContInfo.setGrpName(tGrpName);
        }
        else
        {
            String tLimit = "SN";
            String tCustomerNo = PubFun1.CreateMaxNo("GRPNO", tLimit);
            if (!tCustomerNo.equals(""))
            {
                mGrpInfo = new LDGrpSchema();

                mGrpAppntInfo.setCustomerNo(tCustomerNo);
                mGrpAppntInfo.setName(tGrpAppntName);

                mGrpInfo.setCustomerNo(mGrpAppntInfo.getCustomerNo());
                mGrpInfo.setGrpName(mGrpAppntInfo.getName());

                mGrpInfo.setOperator(mGlobalInput.Operator);
                mGrpInfo.setMakeDate(PubFun.getCurrentDate());
                mGrpInfo.setMakeTime(PubFun.getCurrentTime());
                mGrpInfo.setModifyDate(PubFun.getCurrentDate());
                mGrpInfo.setModifyTime(PubFun.getCurrentTime());

                mGrpContInfo.setAppntNo(tCustomerNo);
                mGrpContInfo.setGrpName(tGrpAppntName);
            }
            else
            {
                String tStrErr = "客户号码生成失败。";
                buildError("dealGrpAppntInfo", tStrErr);
                return false;
            }
        }

        // 由于团体合同号生成规则要求根据客户号进行生成，因此该处理在投保单位处理中进行处理。
        // 生成团体投保单流水号ProposalGrpContNo
        String tProposalGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo",
                mGrpAppntInfo.getCustomerNo());
        if (!tProposalGrpContNo.equals(""))
        {
            mGrpContInfo.setProposalGrpContNo(tProposalGrpContNo);
            mGrpContInfo.setGrpContNo(tProposalGrpContNo);

            mGrpAppntInfo.setGrpContNo(tProposalGrpContNo);
        }
        else
        {
            String tStrErr = "集体投保单号生成失败！";
            buildError("createGrpContBaseInfo", tStrErr);
            return false;
        }
        // --------------------------

        mGrpAppntInfo.setPrtNo(mGrpContInfo.getPrtNo());

        mGrpAppntInfo.setOperator(mGlobalInput.Operator);
        mGrpAppntInfo.setMakeDate(PubFun.getCurrentDate());
        mGrpAppntInfo.setMakeTime(PubFun.getCurrentTime());
        mGrpAppntInfo.setModifyDate(PubFun.getCurrentDate());
        mGrpAppntInfo.setModifyTime(PubFun.getCurrentTime());
        // --------------------------------

        // 处理投保单位地址信息。虚拟投保人地址没必要重复生成地址信息。如有一致的地址取数据库中记录，没有，则新建。
        String tStrGrpAddrSql = " select integer(nvl(max(integer(lgga.AddressNo)), 0)) "
                + " from LCGrpAddress lgga "
                + " where 1 = 1 "
                + " and lgga.CustomerNo = '"
                + mGrpAppntInfo.getCustomerNo()
                + "' " + " and lgga.GrpAddress = ' ' ";
        String tAddressNo = new ExeSQL().getOneValue(tStrGrpAddrSql);
        if (Integer.parseInt(tAddressNo) > 0)
        {
            mGrpAppntInfo.setAddressNo(tAddressNo);
            mGrpContInfo.setAddressNo(tAddressNo);
        }
        else
        {
            try
            {
                tStrGrpAddrSql = " select integer(nvl(max(integer(lgga.AddressNo)), 0) + 1) "
                        + " from LCGrpAddress lgga "
                        + " where 1 = 1 "
                        + " and lgga.CustomerNo = '"
                        + mGrpAppntInfo.getCustomerNo() + "' ";
                tAddressNo = new ExeSQL().getOneValue(tStrGrpAddrSql);

                mGrpAddressInfo = new LCGrpAddressSchema();

                mGrpAddressInfo.setCustomerNo(mGrpAppntInfo.getCustomerNo());
                mGrpAddressInfo.setAddressNo(tAddressNo);

                mGrpAddressInfo.setGrpAddress(" ");

                mGrpAddressInfo.setOperator(mGlobalInput.Operator);
                mGrpAddressInfo.setMakeDate(PubFun.getCurrentDate());
                mGrpAddressInfo.setMakeTime(PubFun.getCurrentTime());
                mGrpAddressInfo.setModifyDate(PubFun.getCurrentDate());
                mGrpAddressInfo.setModifyTime(PubFun.getCurrentTime());

                mGrpAppntInfo.setAddressNo(mGrpAddressInfo.getAddressNo());
                mGrpContInfo.setAddressNo(tAddressNo);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                String tStrErr = "客户地址信息生成失败。";
                buildError("dealGrpAppntInfo", tStrErr);
                return false;
            }
        }
        // --------------------------------

        return true;
    }
    
    /**
     * 处理单证关联险种信息
     * @return
     */
    private boolean dealGrpPlanInfo()
    {
        // 根据单证类别，获取对应产品险种信息。 
        if (!loadRiskInfoByCertifyCode(mCertifyCode))
        {
            return false;
        }
        // -----------------------------

        // 处理团单险种信息。
        if (!dealGrpPolInfo())
        {
            return false;
        }
        // -----------------------------

        // 处理团单保障计划。
        //if (!dealContPlanInfo())
        //{
        //    return false;
        //}
        // -----------------------------

        return true;
    }
    
    /**
     * 根据单证类型获取险种相关信息。（险种、险种要素）
     * @param cCertifyCode
     * @return
     */
    private boolean loadRiskInfoByCertifyCode(String cCertifyCode)
    {
        String tStrSql = null;

        // 根据单证类别获取套餐险种名称。
        tStrSql = null;
        tStrSql = " select RiskCode from LMCardRisk "
                + " where RiskType = 'W' " + " and CertifyCode = '"
                + cCertifyCode + "' "
                + " group by RiskCode having count(distinct RiskCode) = 1 ";
        String tWrapCode = new ExeSQL().getOneValue(tStrSql);

        tStrSql = null;
        if (tWrapCode == null || tWrapCode.equals(""))
        {
            String tStrErr = "[" + cCertifyCode + "]该单证类型未绑定套餐，或绑定了多个套餐。";
            buildError("loadRiskInfoByCertifyCode", tStrErr);
            return false;
        }
        // -----------------------------

        // 获取套餐险种信息。
        tStrSql = null;
        tStrSql = " select * from LDRiskWrap where 1 = 1 "
                + " and RiskWrapCode = '" + tWrapCode + "' "
                + " order by RiskWrapCode, RiskCode ";
        mRiskWrapDes = new LDRiskWrapDB().executeQuery(tStrSql);
        tStrSql = null;
        if (mRiskWrapDes == null || mRiskWrapDes.size() == 0)
        {
            String tStrErr = "获取套餐险种信息失败。";
            buildError("loadRiskInfoByCertifyCode", tStrErr);
            return false;
        }
        // -----------------------------

        // 获取套餐险种要素信息。
        tStrSql = null;
        //        tStrSql = " select * from LDRiskDutyWrap where 1 = 1 "
        //                + " and RiskWrapCode = '" + tWrapCode + "' "
        //                + " order by RiskWrapCode, RiskCode, DutyCode, CalFactor ";
        //        mRiskDutyWrapDes = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        //        tStrSql = null;
        //        if (mRiskDutyWrapDes == null || mRiskDutyWrapDes.size() == 0)
        //        {
        //            String tStrErr = "获取套餐险种要素信息失败。";
        //            buildError("loadRiskInfoByCertifyCode", tStrErr);
        //            return false;
        //        }
        // -----------------------------

        return true;
    }
    
    private boolean dealGrpPolInfo()
    {
        String tDefContPlanCode = "A";
        String tDefContPlanName = "卡式业务保障计划";

        LICertifySet tCertifyList = new LICertifyDB()
                .executeQuery("select * from LICertify where PrtNo = '"
                        + mGrpContInfo.getPrtNo() + "'");
        if (tCertifyList == null || tCertifyList.size() == 0)
        {
            String tStrErr = "获取团单下单证信息清单失败。";
            buildError("dealGrpPolInfo", tStrErr);
            return false;
        }

        LIBCertifySet tWTCertifyList = new LIBCertifyDB()
                .executeQuery("select * from LIBCertify where PrtNo = '"
                        + mGrpContInfo.getPrtNo() + "'");

        for (int i = 1; i <= mRiskWrapDes.size(); i++)
        {
            // 
            LDRiskWrapSchema tLDRiskWrapSchema = null;
            tLDRiskWrapSchema = mRiskWrapDes.get(i);

            // 获取险种描述信息。
            LMRiskAppSchema tLMRiskApp = getRiskAppDes(tLDRiskWrapSchema
                    .getRiskCode());
            if (tLMRiskApp == null)
            {
                String tStrErr = "获取套餐中险种描述信息失败。";
                buildError("dealGrpPolInfo", tStrErr);
                return false;
            }
            // ----------------------------

            String tLimit = null;
            String tPolSeqNo = null;

            try
            {
                tLimit = PubFun.getNoLimit(mGrpContInfo.getManageCom());
                tPolSeqNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);

                if (tPolSeqNo == null || tPolSeqNo.equals(""))
                {
                    String tStrErr = "团体险种流水号生成失败。";
                    buildError("dealGrpPolInfo", tStrErr);
                    return false;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                String tStrErr = "团体险种流水号生成失败。";
                buildError("dealGrpPolInfo", tStrErr);
                return false;
            }

            LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();

            tLCGrpPolSchema.setRiskCode(tLMRiskApp.getRiskCode());
            tLCGrpPolSchema.setComFeeRate(tLMRiskApp.getAppInterest());
            tLCGrpPolSchema.setBranchFeeRate(tLMRiskApp.getAppPremRate());

            tLCGrpPolSchema.setGrpPolNo(tPolSeqNo);
            tLCGrpPolSchema.setGrpProposalNo(tPolSeqNo);

            tLCGrpPolSchema.setGrpContNo(mGrpContInfo.getGrpContNo());
            tLCGrpPolSchema.setPrtNo(mGrpContInfo.getPrtNo());

            tLCGrpPolSchema.setManageCom(mGrpContInfo.getManageCom());
            tLCGrpPolSchema.setCValiDate(mGrpContInfo.getCValiDate());

            String tRiskWrapFlag = "W";
            tLCGrpPolSchema.setRiskWrapFlag(tRiskWrapFlag);

            tLCGrpPolSchema.setPayMode(mGrpContInfo.getPayMode());
            tLCGrpPolSchema.setPayIntv(mGrpContInfo.getPayIntv());

            tLCGrpPolSchema.setSaleChnl(mGrpContInfo.getSaleChnl());
            tLCGrpPolSchema.setAgentCom(mGrpContInfo.getAgentCom());
            tLCGrpPolSchema.setAgentType(mGrpContInfo.getAgentType());
            tLCGrpPolSchema.setAgentCode(mGrpContInfo.getAgentCode());
            tLCGrpPolSchema.setAgentGroup(mGrpContInfo.getAgentGroup());

            tLCGrpPolSchema.setCustomerNo(mGrpContInfo.getAppntNo());
            tLCGrpPolSchema.setAddressNo(mGrpContInfo.getAddressNo());
            tLCGrpPolSchema.setGrpName(mGrpContInfo.getGrpName());

            tLCGrpPolSchema.setAppFlag(mGrpContInfo.getAppFlag());
            tLCGrpPolSchema.setState(mGrpContInfo.getState());
            tLCGrpPolSchema.setStateFlag(mGrpContInfo.getStateFlag());

            tLCGrpPolSchema.setUWFlag(mGrpContInfo.getUWFlag());
            tLCGrpPolSchema.setUWOperator(mGrpContInfo.getUWOperator());
            tLCGrpPolSchema.setUWDate(mGrpContInfo.getUWDate());
            tLCGrpPolSchema.setUWTime(mGrpContInfo.getUWTime());

            tLCGrpPolSchema.setApproveFlag(mGrpContInfo.getApproveFlag());
            tLCGrpPolSchema.setApproveCode(mGrpContInfo.getApproveCode());
            tLCGrpPolSchema.setApproveDate(mGrpContInfo.getApproveDate());
            tLCGrpPolSchema.setApproveTime(mGrpContInfo.getApproveTime());

            tLCGrpPolSchema.setOperator(mGlobalInput.Operator);
            tLCGrpPolSchema.setMakeDate(PubFun.getCurrentDate());
            tLCGrpPolSchema.setMakeTime(PubFun.getCurrentTime());
            tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
            // -------------------------------

            mGrpPolInfo.add(tLCGrpPolSchema);
            // -------------------------------

            // 
            LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();

            tLCContPlanRiskSchema.setGrpContNo(mGrpContInfo.getGrpContNo());
            tLCContPlanRiskSchema.setProposalGrpContNo(mGrpContInfo
                    .getProposalGrpContNo());

            tLCContPlanRiskSchema.setRiskCode(tLMRiskApp.getRiskCode());
            tLCContPlanRiskSchema.setMainRiskCode(tLDRiskWrapSchema
                    .getMainRiskCode());
            tLCContPlanRiskSchema.setMainRiskVersion(tLMRiskApp.getRiskVer());
            tLCContPlanRiskSchema.setRiskVersion(tLMRiskApp.getRiskVer());

            tLCContPlanRiskSchema.setContPlanCode(tDefContPlanCode);
            tLCContPlanRiskSchema.setContPlanName(tDefContPlanName);

            tLCContPlanRiskSchema.setPlanType("0");

            tLCContPlanRiskSchema.setRiskWrapFlag("Y");
            tLCContPlanRiskSchema.setRiskWrapCode(tLDRiskWrapSchema
                    .getRiskWrapCode());

            tLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);
            tLCContPlanRiskSchema.setMakeDate(PubFun.getCurrentDate());
            tLCContPlanRiskSchema.setMakeTime(PubFun.getCurrentTime());
            tLCContPlanRiskSchema.setModifyDate(PubFun.getCurrentDate());
            tLCContPlanRiskSchema.setModifyTime(PubFun.getCurrentTime());

            // --------------------------------

            // 获取套餐险种要素信息。
            LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

            String tStrSql = " select * from LDRiskDutyWrap where 1 = 1 "
                    + " and RiskWrapCode = '"
                    + tLDRiskWrapSchema.getRiskWrapCode() + "' "
                    + " and RiskCode = '" + tLDRiskWrapSchema.getRiskCode()
                    + "' "
                    + " order by RiskWrapCode, RiskCode, DutyCode, CalFactor ";
            tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
            tStrSql = null;
            if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
            {
                String tStrErr = "获取套餐险种要素信息失败。";
                buildError("dealGrpPolInfo", tStrErr);
                return false;
            }

            double tSumRiskPrem = 0;

            for (int j = 1; j <= tLDRiskDutyWrapSet.size(); j++)
            {
                LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = tLDRiskDutyWrapSet
                        .get(j);

                LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();

                tLCContPlanDutyParamSchema.setGrpContNo(mGrpContInfo
                        .getGrpContNo());
                tLCContPlanDutyParamSchema.setProposalGrpContNo(mGrpContInfo
                        .getProposalGrpContNo());

                tLCContPlanDutyParamSchema.setContPlanCode(tDefContPlanCode);
                tLCContPlanDutyParamSchema.setContPlanName(tDefContPlanName);

                tLCContPlanDutyParamSchema.setRiskCode(tLDRiskDutyWrapSchema
                        .getRiskCode());
                tLCContPlanDutyParamSchema.setDutyCode(tLDRiskDutyWrapSchema
                        .getDutyCode());

                tLCContPlanDutyParamSchema.setRiskVersion(tLMRiskApp
                        .getRiskVer());

                tLCContPlanDutyParamSchema.setGrpPolNo(tPolSeqNo);

                tLCContPlanDutyParamSchema.setMainRiskCode(tLDRiskWrapSchema
                        .getMainRiskCode());
                tLCContPlanDutyParamSchema.setMainRiskVersion(tLMRiskApp
                        .getRiskVer());

                tLCContPlanDutyParamSchema.setCalFactor(tLDRiskDutyWrapSchema
                        .getCalFactor());

                String tCalFactorType = tLDRiskDutyWrapSchema
                        .getCalFactorType();
                tLCContPlanDutyParamSchema.setCalFactorType(tCalFactorType);

                //                if ("Prem".equals(tLDRiskDutyWrapSchema.getCalFactor())
                //                        || "Amnt".equals(tLDRiskDutyWrapSchema.getCalFactor()))
                //                {
                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();
                    tCal.addBasicFactor("RiskCode", tLDRiskDutyWrapSchema
                            .getRiskCode());
                    tCal.addBasicFactor("Copys", "1");
                    tCal.addBasicFactor("Mult", "1");

                    /** 计算 */
                    tCal.setCalSql(tLDRiskDutyWrapSchema.getCalSql());
                    System.out.println(tLDRiskDutyWrapSchema.getCalSql());

                    String tResult = tCal.calculate();
                    tLCContPlanDutyParamSchema.setCalFactorValue(tResult);
                    System.out.println(tResult);
                }
                else
                {
                    tLCContPlanDutyParamSchema
                            .setCalFactorValue(tLDRiskDutyWrapSchema
                                    .getCalFactorValue());
                }

                if ("Prem".equals(tLDRiskDutyWrapSchema.getCalFactor()))
                {
                    try
                    {
                        String tResult = null;

                        for (int n = 1; n <= tCertifyList.size(); n++)
                        {
                            LICertifySchema tLICertifySchema = null;
                            tLICertifySchema = tCertifyList.get(n);

                            if ("2".equals(tCalFactorType))
                            {
                                String tTmpCopys = String
                                        .valueOf(tLICertifySchema.getCopys());
                                String tTmpMult = String
                                        .valueOf(tLICertifySchema.getMult());

                                /** 准备要素 calbase */
                                PubCalculator tCal = new PubCalculator();
                                tCal.addBasicFactor("RiskCode",
                                        tLDRiskDutyWrapSchema.getRiskCode());
                                tCal.addBasicFactor("Copys", tTmpCopys);
                                tCal.addBasicFactor("Mult", tTmpMult);

                                /** 计算 */
                                tCal.setCalSql(tLDRiskDutyWrapSchema
                                        .getCalSql());
                                System.out.println(tLDRiskDutyWrapSchema
                                        .getCalSql());

                                tResult = tCal.calculate();
                                tLCContPlanDutyParamSchema
                                        .setCalFactorValue(tResult);
                                System.out.println(tResult);
                            }
                            else
                            {
                                tResult = tLDRiskDutyWrapSchema
                                        .getCalFactorValue();
                            }

                            tSumRiskPrem = Arith.add(tSumRiskPrem,
                                    new BigDecimal(tResult).doubleValue());
                        }

                        // 处理退保数据
                        tResult = null;

                        for (int n = 1; n <= tWTCertifyList.size(); n++)
                        {
                            LIBCertifySchema tWTLICertifySchema = null;
                            tWTLICertifySchema = tWTCertifyList.get(n);

                            if ("2".equals(tCalFactorType))
                            {
                                String tTmpCopys = String
                                        .valueOf(tWTLICertifySchema.getCopys());
                                String tTmpMult = String
                                        .valueOf(tWTLICertifySchema.getMult());

                                /** 准备要素 calbase */
                                PubCalculator tCal = new PubCalculator();
                                tCal.addBasicFactor("RiskCode",
                                        tLDRiskDutyWrapSchema.getRiskCode());
                                tCal.addBasicFactor("Copys", tTmpCopys);
                                tCal.addBasicFactor("Mult", tTmpMult);

                                /** 计算 */
                                tCal.setCalSql(tLDRiskDutyWrapSchema
                                        .getCalSql());
                                System.out.println(tLDRiskDutyWrapSchema
                                        .getCalSql());

                                tResult = tCal.calculate();
                                tLCContPlanDutyParamSchema
                                        .setCalFactorValue(tResult);
                                System.out.println(tResult);
                            }
                            else
                            {
                                tResult = tLDRiskDutyWrapSchema
                                        .getCalFactorValue();
                            }

                            tSumRiskPrem = Arith.sub(tSumRiskPrem,
                                    new BigDecimal(tResult).doubleValue());
                        }
                        // --------------------
                    }
                    catch (Exception e)
                    {
                        String tStrErr = "套餐要素中保费出现非数值型字符串。";
                        buildError("dealGrpPolInfo", tStrErr);
                        return false;
                    }
                }
                //                }
                //                else
                //                {
                //                    tLCContPlanDutyParamSchema
                //                            .setCalFactorValue(tLDRiskDutyWrapSchema
                //                                    .getCalFactorValue());
                //                }

                tLCContPlanDutyParamSchema.setPlanType("0");
                tLCContPlanDutyParamSchema.setPayPlanCode("000000");
                tLCContPlanDutyParamSchema.setGetDutyCode("000000");
                tLCContPlanDutyParamSchema.setInsuAccNo("000000");

                mGrpPlanDutyParams.add(tLCContPlanDutyParamSchema);

            }
            // -----------------------------

            //            String tSumCopys = new ExeSQL()
            //                    .getOneValue("select nvl(sum(Copys), 0) from licertify where prtno = '"
            //                            + mPrtNo
            //                            + "' and state = '"
            //                            + CertifyContConst.CC_CASHING_UP + "'");

            tLCContPlanRiskSchema.setRiskPrem(tSumRiskPrem);
            mGrpPlanRiskInfo.add(tLCContPlanRiskSchema);
        }

        // 创建保障计划
        LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();

        tLCContPlanSchema.setGrpContNo(mGrpContInfo.getGrpContNo());
        tLCContPlanSchema.setProposalGrpContNo(mGrpContInfo
                .getProposalGrpContNo());

        tLCContPlanSchema.setContPlanCode(tDefContPlanCode);
        tLCContPlanSchema.setContPlanName(tDefContPlanName);

        tLCContPlanSchema.setPlanType("0");

        tLCContPlanSchema.setOperator(mGlobalInput.Operator);
        tLCContPlanSchema.setMakeDate(PubFun.getCurrentDate());
        tLCContPlanSchema.setMakeTime(PubFun.getCurrentTime());
        tLCContPlanSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContPlanSchema.setModifyTime(PubFun.getCurrentTime());

        String tSettlementCertifyCount = new ExeSQL()
                .getOneValue("select Count(1) from licertify where prtno = '"
                        + mPrtNo + "' and state = '"
                        + CertifyContConst.CC_CASHING_UP + "'");
        tLCContPlanSchema.setPeoples2(tSettlementCertifyCount);
        tLCContPlanSchema.setPeoples3(tSettlementCertifyCount);

        mGrpPlanInfo.add(tLCContPlanSchema);
        // ------------------------

        return true;
    }
    
    /**
     * 获取险种产品描述。
     * @param cRiskCode
     * @return
     */
    private LMRiskAppSchema getRiskAppDes(String cRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cRiskCode);
        if (!tLMRiskAppDB.getInfo())
        {
            String tStrErr = "获取套餐中险种描述信息失败。";
            buildError("dealGrpPolInfo", tStrErr);
            return null;
        }
        return tLMRiskAppDB.getSchema();
    }
    
    private boolean dealNoNameInsuredList()
    {
        LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();

        LCContPlanSchema tLCContPlanSchema = mGrpPlanInfo.get(1);

        tLCInsuredListSchema.setNoNamePeoples(tLCContPlanSchema.getPeoples3());

        tLCInsuredListSchema.setContPlanCode(tLCContPlanSchema
                .getContPlanCode());

        tLCInsuredListSchema.setGrpContNo(mGrpContInfo.getGrpContNo());

        tLCInsuredListSchema.setInsuredID("1");

        tLCInsuredListSchema.setState("0");

        tLCInsuredListSchema.setContNo("1");

        tLCInsuredListSchema.setEmployeeName("无名单");
        tLCInsuredListSchema.setInsuredName("无名单");

        tLCInsuredListSchema.setRelation("00");
        tLCInsuredListSchema.setIDType("4");
        tLCInsuredListSchema.setOccupationType("0");

        String CurrentData = PubFun.getCurrentDate();
        tLCInsuredListSchema.setBirthday(PubFun.calDate(CurrentData, -35, "Y",
                null));
        tLCInsuredListSchema.setPublicAccType("1");

        tLCInsuredListSchema.setSex("0");

        tLCInsuredListSchema.setOperator(mGlobalInput.Operator);
        tLCInsuredListSchema.setMakeDate(PubFun.getCurrentDate());
        tLCInsuredListSchema.setMakeTime(PubFun.getCurrentTime());
        tLCInsuredListSchema.setModifyDate(PubFun.getCurrentDate());
        tLCInsuredListSchema.setModifyTime(PubFun.getCurrentTime());

        mNoNameInsuredInfo.add(tLCInsuredListSchema);

        return true;

    }
    
    /**
     * 回置单证清单结算业务数据
     * @return
     */
    private MMap dealCertifyListState()
    {
        MMap tMMap = new MMap();

        // 回置LICertify-GrpContNo/ProposalGrpContNo
        String tStrCCStateSql = " update LICertify " + " set GrpContNo = '"
                + mGrpContInfo.getGrpContNo() + "', "
                + " ProposalGrpContNo = '"
                + mGrpContInfo.getProposalGrpContNo() + "', " + " State = '"
                + CertifyContConst.CC_SETTLEMENT + "', "
                + " SettlementInputDate = '" + PubFun.getCurrentDate() + "', "
                + " SettlementInputOperator = '" + mGlobalInput.Operator
                + "', " + " ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + " ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + " where PrtNo = '" + mGrpContInfo.getPrtNo() + "' ";
        tMMap.put(tStrCCStateSql, SysConst.UPDATE);
        // ------------------------------------------

        // 回置LICertifyInsured-GrpContNo/ProposalGrpContNo
        String tStrCCIStateSql = " update LICertifyInsured "
                + " set GrpContNo = '" + mGrpContInfo.getGrpContNo() + "', "
                + " ProposalGrpContNo = '"
                + mGrpContInfo.getProposalGrpContNo() + "', "
                + " ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + " ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + " where PrtNo = '" + mGrpContInfo.getPrtNo() + "' ";
        tMMap.put(tStrCCIStateSql, SysConst.UPDATE);
        // ------------------------------------------

        // 回置LIBCertify-GrpContNo/ProposalGrpContNo
        String tStrCCWTStateSql = " update LIBCertify " + " set GrpContNo = '"
                + mGrpContInfo.getGrpContNo() + "', "
                + " ProposalGrpContNo = '"
                + mGrpContInfo.getProposalGrpContNo() + "', " + " State = '"
                + CertifyContConst.CC_SETTLEMENT + "', "
                + " SettlementInputDate = '" + PubFun.getCurrentDate() + "', "
                + " SettlementInputOperator = '" + mGlobalInput.Operator
                + "', " + " ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + " ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + " where PrtNo = '" + mGrpContInfo.getPrtNo() + "' ";
        tMMap.put(tStrCCWTStateSql, SysConst.UPDATE);
        // ------------------------------------------

        return tMMap;
    }
    
    /**
     * 生成缴费通知书待打印数据。
     * @return
     */
    private MMap sendPayPrint()
    {
        MMap tMMap = new MMap();

        LOPRTManagerSchema tPayPrintInfo = new LOPRTManagerSchema();

        // 获取结算单缴费凭证号
        String tPrtSeq = null;
        tPrtSeq = PubFun1
                .CreateMaxNo("LCBPAYNOTICENO", mGrpContInfo.getPrtNo());

        if (tPrtSeq == null || "".equals(tPrtSeq))
        {
            String tStrErr = "获取结算单缴费凭证号失败。";
            buildError("sendPayPrint", tStrErr);
            return null;
        }
        tPayPrintInfo.setPrtSeq(tPrtSeq);
        // ----------------------------------

        tPayPrintInfo.setOtherNo(mGrpContInfo.getGrpContNo());
        tPayPrintInfo.setOtherNoType(PrintPDFManagerBL.ONT_CC_GRPCONT);

        tPayPrintInfo.setCode(PrintPDFManagerBL.CODE_CC_GRP_FIRSTPAY);

        tPayPrintInfo.setAgentCode(mGrpContInfo.getAgentCode());
        tPayPrintInfo.setManageCom(mGrpContInfo.getManageCom());

        tPayPrintInfo.setReqCom(mGlobalInput.ManageCom);
        tPayPrintInfo.setReqOperator(mGlobalInput.Operator);

        tPayPrintInfo.setPrtType(PrintPDFManagerBL.PT_FRONT);

        tPayPrintInfo.setStateFlag("0");

        tPayPrintInfo.setMakeDate(PubFun.getCurrentDate());
        tPayPrintInfo.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tPayPrintInfo, SysConst.INSERT);
        //生成暂收保费信息
        
        MMap dMMap = new MMap();
        dMMap=createTempfeeInfo(tPrtSeq);
        if(dMMap == null){
        	System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>生成暂收保费信息失败<<<<<<<<<<<<<<<<<<<<<<");
        	buildError("createTempfeeInfo", "生成暂收保费信息失败");
        	return null;
        }
         tMMap.add(dMMap);
        
        return tMMap;
    }
    
    /**
     * 生成暂收保费信息
     * @return MMap
     */
    private MMap createTempfeeInfo(String tPrtSeq){
    	MMap tMMap = new MMap();
//    	String prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO", mPrtNo);
//    	if(prtSeq == null || prtSeq.equals("")){
//    		buildError("createTempfeeInfo", "生成暂交费收据号码失败");
//            return null;
//    	}
    	if(mGrpPlanRiskInfo !=null && mGrpPlanRiskInfo.size()>0){
    		String payDateSQL="select max(licsp.PayDate) from liCertSalesPayInfo licsp " +
    				"inner join licertify lic on lic.paytradebatchno = licsp.batchno " +
    				"where lic.state = '02' " +
		    				"and lic.PayTradeStatus = '00' " +
//		    				"and lic.PayTradeMakeDate = current date - 1 day " +
		    				"and lic.prtno='"+mPrtNo+"'";
    		String payDate = new ExeSQL().getOneValue(payDateSQL);
    		if(payDate==null || payDate.equals("")){
    			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>得到暂交费支付时间失败<<<<<<<<<<<<<<<<<<<<<<");
    			buildError("createTempfeeInfo", "得到暂交费支付时间失败");
                return null;
    		}
//    		by gzh 20110221 增加网销银行编码 20110721 调整为总公司账户收取，修改获取管理机构及账户
	    	SSRS BankInfos = getBankAccCode();
	    	String ZBankManageCom = BankInfos.GetText(1, 1);//总公司机构
	    	String ZBankAccNo = BankInfos.GetText(1, 2);
    		String serNo=null;
    		String prem = null;
    		List sumPrem = new ArrayList();
    		LCContPlanRiskSchema dLCContPlanRiskSchema = new LCContPlanRiskSchema();
    		for(int i=1;i<=mGrpPlanRiskInfo.size();i++){
    			serNo = PubFun1.CreateMaxNo("SERIALNO", mContManageCom);
    	    	if(serNo == null || serNo.equals("")){
    	    		buildError("createTempfeeInfo", "生成暂收保费流水号失败");
    	            return null;
    	    	}
    	    	dLCContPlanRiskSchema = mGrpPlanRiskInfo.get(i);
    	    	dLJTempFeeSchema = new LJTempFeeSchema();
    	    	dLJTempFeeSchema.setTempFeeNo(tPrtSeq);
    	    	dLJTempFeeSchema.setTempFeeType("1");
    	    	dLJTempFeeSchema.setRiskCode(dLCContPlanRiskSchema.getRiskCode());
    	    	System.out.println();
    	    	System.out.println("dLCContPlanRiskSchema.getRiskCode()=="+dLCContPlanRiskSchema.getRiskCode());
    	    	dLJTempFeeSchema.setPayIntv(this.mPayIntv);
    	    	dLJTempFeeSchema.setOtherNo(mPrtNo);
    	    	dLJTempFeeSchema.setOtherNoType("5"); 
    	    	dLJTempFeeSchema.setPayMoney(dLCContPlanRiskSchema.getRiskPrem());
    	    	
    	    	System.out.println("dLCGrpPolSchema.getPrem()===="+dLCContPlanRiskSchema.getRiskPrem());
    	    	//累加全部保费
    	    	prem = String.valueOf(dLCContPlanRiskSchema.getRiskPrem());
    	    	if(prem!=null && !prem.equals("")){sumPrem.add(prem);}
    	    	
    	    	dLJTempFeeSchema.setPayDate(mFDate.getDate(payDate)); 
    	    	dLJTempFeeSchema.setSaleChnl(this.mSaleChnl);
    	    	dLJTempFeeSchema.setManageCom(ZBankManageCom);// by gzh 20100721 收费均收取到总公司账户
    	    	dLJTempFeeSchema.setPolicyCom(this.mContManageCom);//分公司
    	    	dLJTempFeeSchema.setAgentCom(this.mAgentCom);
    	    	dLJTempFeeSchema.setAPPntName(this.mAppntName);
    	    	dLJTempFeeSchema.setAgentGroup(this.mGrpContInfo.getAgentGroup());
    	    	dLJTempFeeSchema.setAgentCode(this.mAgentCode);
    	    	dLJTempFeeSchema.setSerialNo(serNo);   	    	
    	    	dLJTempFeeSchema.setOperator(mGlobalInput.Operator);
    	    	dLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
    	    	dLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
    	    	dLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
    	    	dLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
    	    	dLJTempFeeSet.add(dLJTempFeeSchema);   	    	 	    	
    		}
    		//------------------------
    		double result = tempFeeClassPrem(sumPrem);
    		serNo = PubFun1.CreateMaxNo("SERIALNO", mContManageCom);
	    	dLJTempFeeClassSchema.setTempFeeNo(tPrtSeq);
	    	dLJTempFeeClassSchema.setPayMode(this.mPayMode);
	    	dLJTempFeeClassSchema.setPayMoney(result);
	    	dLJTempFeeClassSchema.setAppntName(mAppntName);
	    	dLJTempFeeClassSchema.setPayDate(mFDate.getDate(payDate));
	    	dLJTempFeeClassSchema.setSerialNo(serNo);
	    	dLJTempFeeClassSchema.setManageCom(ZBankManageCom);//总公司
	    	dLJTempFeeClassSchema.setPolicyCom(mContManageCom);//分公司
	    	dLJTempFeeClassSchema.setOperator(this.mGlobalInput.Operator);
	    	dLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
	    	dLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
	    	dLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
	    	dLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
	    	dLJTempFeeClassSchema.setInsBankAccNo(ZBankAccNo);
//	    	dLJTempFeeClassSchema.setPrtNo(this.mPrtNo);
	    	String aBankCode = getBankCode(ZBankAccNo);//获取银行账号对应的bankcode
	    	dLJTempFeeClassSchema.setBankCode(aBankCode);
	    	dLJTempFeeClassSet.add(dLJTempFeeClassSchema);
	    	tMMap.put(dLJTempFeeSet, SysConst.INSERT);  
	    	tMMap.put(dLJTempFeeClassSet, SysConst.INSERT);
    	}else{
    		buildError("createTempfeeInfo", "没有要生成暂收保费信息的数据，mGrpPolInfo为空");
            return null;
    	}	
    	return tMMap;
    }
    
    /**
     * 计算tempfeeclass的保费
     * @param sumPrem
     * @return
     */
    private double tempFeeClassPrem(List sumPrem){
    	double result =0;
    	String prem = "";
    	for(int i=0;i<sumPrem.size();i++){
    		prem = sumPrem.get(i).toString();
    		if(prem.equals("")){
    			result+=0;
    		}
    		result+=Double.valueOf(prem).doubleValue();
    	}
    	return result;
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
    
    public VData getResult()
    {
        return mResult;
    }
    
    /**
     * 锁定签单动作。
     * @param cLCContSchema
     * @return
     */
    private MMap lockCont(String prtNo)
    {
        MMap tMMap = null;

        // 签单锁定标志为：“SC”
        String tLockNoType = "WX";//网销锁定
        // -----------------------

        // 锁定有效时间
        String tAIS = "300";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", prtNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return tMMap;
    }
    
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CardNoInfoToLicertiry";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据网销结算时总公司的机构编码和银行账户。
     * @param szFunc
     * @param szErrMsg
     */
    public SSRS getBankAccCode()
    {
    	String sql = "select code,codename from ldcode where codetype = 'InsBankAccNo' and othersign = 'WX'";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0){
    		buildError("getBankAccCode", "获取机构对应银行账户失败");
        	return null;
    	}
        return tSSRS;
    }
    
    /**
     * 根据机构编码获取银行账户。
     * @param szFunc
     * @param szErrMsg
     */
    public String getBankCode(String aBankAccNo)
    {
    	String sql = "select bankcode from ldfinbank where bankaccno = '"+aBankAccNo+"' ";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0){
    		buildError("getBankCode", "获取银行账户对应的银行编码失败");
        	return null;
    	}
        return tSSRS.GetText(1, 1);
    }
}
