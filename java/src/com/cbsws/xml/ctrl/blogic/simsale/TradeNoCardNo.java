/**
 * 2011-9-20
 */
package com.cbsws.xml.ctrl.blogic.simsale;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.SimAppntInfo;
import com.cbsws.obj.SimBnfInfo;
import com.cbsws.obj.SimInsuInfo;
import com.cbsws.obj.SimPolicyInfo;
import com.cbsws.obj.WrapExInfo;
import com.cbsws.obj.WrapInfo;
import com.cbsws.xml.ctrl.blogic.LvmamaPrintBL;
import com.ecwebservice.lvmama.LVEmailAndMessageBL;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LIWrapExListSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.schema.WFWrapExListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LIWrapExListSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.lis.vschema.WFWrapExListSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author zhangyige
 * 
 * 简易保单试算处理通用接口
 *
 */
public class TradeNoCardNo extends ABusLogic
{
    private MsgHead mMsgHead = null;

    private SimPolicyInfo mMsgSimPolicyInfo = null;

    private SimAppntInfo mMsgSimAppntInfo = null;

    private SimInsuInfo[] mMsgSimInsuInfoList = null;

    private SimBnfInfo[] mMsgSimBnfInfoList = null;

    private WrapInfo mMsgWrapInfo = null;

    private WrapExInfo[] mMsgWrapExInfoList = null;

    private CardNoManager mCardNoManager = new CardNoManager();
    
    private static HashMap mAndeMap = new HashMap();
    
    private String BitchNo = "";
   
    private String ContNo = "";
    
    protected boolean deal(MsgCollection cMsgInfos)
    {
        try
        {
            // 加载报文中相关数据
            if (!parseDatas(cMsgInfos))
            {
                return false;
            }
            String tWrapCode = mMsgWrapInfo.getWrapCode();
            String tCertifyCode = mMsgWrapInfo.getCertifyCode();
            String tMsgType = mMsgHead.getMsgType();
            String []info = mCardNoManager.getCardNo(tWrapCode,tCertifyCode,tMsgType);
            if(info==null){
            	errLog("获取卡号过程出现异常，稍候请尝试重新交易");
                return false;
            }else{
            	if(info[2]!=null){
            		errLog(info[2]);
                    return false;
            	}else{
            		mMsgWrapInfo.setCardNo(info[0]);
                    mMsgWrapInfo.setPassword(info[1]);
                    System.out.println("套餐编码为："+tWrapCode+"试算过程中取得的卡号为："+mMsgWrapInfo.getCardNo()+
                    		"取得的密码为："+mMsgWrapInfo.getPassword());
            	}
            }
            // 可用单证信息核对
            if (!chkCertInfo())
            {
                return false;
            }
            // --------------------

            // 处理业务数据
            if (!tradeService())
            {
                return false;
            }
            // 发送短信&邮件
            sendSmsAndEmail();
            makeLvPDF(ContNo);
            // --------------------
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            errLog("交易过程出现异常，稍候请尝试重新交易");
            return false;
        }

        return true;
    }
    private void makeLvPDF(String ContNo){
    	String cardno = ContNo;
    	LvmamaPrintBL ecp = new LvmamaPrintBL();
    	MsgCollection tMsgCollection = new MsgCollection();
    	MsgHead tMsgHead = new MsgHead();
    	tMsgHead.setBatchNo(BitchNo);
    	if(mMsgHead.getBranchCode().equalsIgnoreCase("SZM")){
    		tMsgHead.setBranchCode("SZM");
    	}else{
    		tMsgHead.setBranchCode("001");
    	}
    	tMsgHead.setMsgType("INDIGO");
    	tMsgHead.setSendDate(PubFun.getCurrentDate());
    	tMsgHead.setSendTime(PubFun.getCurrentTime());
    	tMsgHead.setSendOperator("EC_WX");
    	tMsgCollection.setMsgHead(tMsgHead);
    	CertPrintTable tCertPrintTable = new CertPrintTable();
    	tCertPrintTable.setCardNo(cardno);
    	tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);
    	ecp.deal(tMsgCollection);
    }
    private boolean parseDatas(MsgCollection cMsgInfos)
    {
        // 报文头 
        mMsgHead = cMsgInfos.getMsgHead();
        if (mMsgHead == null)
        {
            errLog("报文头信息缺失。");
            return false;
        }
        // --------------------

        // 保单信息
        try
        {
            List tSimPolicyInfoList = cMsgInfos.getBodyByFlag("SimPolicyInfo");
            if (tSimPolicyInfoList == null || tSimPolicyInfoList.size() == 0)
            {
                errLog("保单信息缺失。");
                return false;
            }
            else if (tSimPolicyInfoList.size() != 1)
            {
                errLog("保单信息出现多条。");
                return false;
            }
            mMsgSimPolicyInfo = (SimPolicyInfo) tSimPolicyInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 保单信息");
            return false;
        }
        // --------------------

        // 投保人信息
        try
        {
            List tSimAppntInfoList = cMsgInfos.getBodyByFlag("SimAppntInfo");
            if (tSimAppntInfoList == null || tSimAppntInfoList.size() == 0)
            {
                errLog("投保人信息缺失。");
                return false;
            }
            else if (tSimAppntInfoList.size() != 1)
            {
                errLog("投保人信息出现多条。");
                return false;
            }
            mMsgSimAppntInfo = (SimAppntInfo) tSimAppntInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 投保人信息");
            return false;
        }
        // --------------------

        // 被保人信息
        try
        {
            List tSimInsuInfoList = cMsgInfos.getBodyByFlag("SimInsuInfo");
            if (tSimInsuInfoList == null || tSimInsuInfoList.size() == 0)
            {
                errLog("投保人信息缺失。");
                return false;
            }
            mMsgSimInsuInfoList = new SimInsuInfo[tSimInsuInfoList.size()];
            for (int i = 0; i < tSimInsuInfoList.size(); i++)
            {
                SimInsuInfo tSimInsuInfo = null;
                tSimInsuInfo = (SimInsuInfo) tSimInsuInfoList.get(i);
                mMsgSimInsuInfoList[i] = tSimInsuInfo;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 被保人信息");
            return false;
        }
        // --------------------

        // 受益人信息
        try
        {
            List tSimBnfInfoList = cMsgInfos.getBodyByFlag("SimBnfInfo");
            if (tSimBnfInfoList != null)
            {
                mMsgSimBnfInfoList = new SimBnfInfo[tSimBnfInfoList.size()];
                for (int i = 0; i < tSimBnfInfoList.size(); i++)
                {
                    SimBnfInfo tSimBnfInfo = null;
                    tSimBnfInfo = (SimBnfInfo) tSimBnfInfoList.get(i);
                    mMsgSimBnfInfoList[i] = tSimBnfInfo;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 受益人信息");
            return false;
        }
        // --------------------

        // 产品信息
        try
        {
            List tWrapInfoList = cMsgInfos.getBodyByFlag("WrapInfo");
            if (tWrapInfoList == null || tWrapInfoList.size() == 0)
            {
                errLog("产品信息缺失。");
                return false;
            }
            else if (tWrapInfoList.size() != 1)
            {
                errLog("产品信息出现多条。");
                return false;
            }
            mMsgWrapInfo = (WrapInfo) tWrapInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 产品信息");
            return false;
        }
        // --------------------

        // 产品扩展信息
        try
        {
            List tWrapExInfoList = cMsgInfos.getBodyByFlag("WrapExInfo");
            if (tWrapExInfoList != null)
            {
                mMsgWrapExInfoList = new WrapExInfo[tWrapExInfoList.size()];
                for (int i = 0; i < tWrapExInfoList.size(); i++)
                {
                    WrapExInfo tWrapExInfo = null;
                    tWrapExInfo = (WrapExInfo) tWrapExInfoList.get(i);
                    mMsgWrapExInfoList[i] = tWrapExInfo;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 产品扩展信息");
            return false;
        }
        // --------------------

        return true;
    }
    
    private boolean chkCertInfo()
    {
        StringBuffer tErrInfos = new StringBuffer();

        String tCardNo = mMsgWrapInfo.getCardNo();
        String tPassword = mMsgWrapInfo.getPassword();
        String tCertifyCode = mMsgWrapInfo.getCertifyCode();
        String tWrapCode = mMsgWrapInfo.getWrapCode();

        if (!CertInfoManager.verifyCertInfo(tErrInfos, tCardNo, tPassword, tCertifyCode, tWrapCode))
        {
            errLog(tErrInfos.toString());
            return false;
        }

        return true;
    }

    private boolean tradeService()
    {
        // 封装保单信息 
        WFContListSchema tWFContInfo = getContInfo();
        if (tWFContInfo == null)
        {
            return false;
        }
        // --------------------

        // 封装投保人信息
        WFAppntListSchema tWFAppntInfo = getAppntInfo();
        if (tWFAppntInfo == null)
        {
            return false;
        }
        // --------------------

        // 封装被保人信息
        WFInsuListSet tWFInsuInfos = getInsuInfos();
        if (tWFInsuInfos == null || tWFInsuInfos.size() == 0)
        {
            return false;
        }
        // --------------------

        // 封装受益人信息
        WFBnfListSet tWFBnfInfos = getBnfInfos();
        if (tWFBnfInfos == null)
        {
            return false;
        }
        // 封装扩展信息(报文备份）
        WFWrapExListSet tWFWrapExInfos = getWrapExInfos();
        if (tWFWrapExInfos == null)
        {
        	return false;
        }
        //备份业务表
        int length = tWFWrapExInfos.size();
        LIWrapExListSet tLIWrapExListSet = new LIWrapExListSet();
    	for(int i=1;i<length+1;i++){
    		LIWrapExListSchema tLIWrapExListSchema = new LIWrapExListSchema();
    		WFWrapExListSchema tWFWrapExListSchema = new WFWrapExListSchema();
    		tWFWrapExListSchema.setSchema(tWFWrapExInfos.get(i).getSchema());
    		Reflections ref = new Reflections();
    		ref.transFields(tLIWrapExListSchema, tWFWrapExListSchema);  			
    		tLIWrapExListSet.add(tLIWrapExListSchema);
    	}
        // 把职业类别放到保单表里面
    	if(tWFInsuInfos.size() != 0){
    		if(!"".equals(tWFInsuInfos.get(1).getOccupationType()) && tWFInsuInfos.get(1).getOccupationType() != null){
    			tWFContInfo.setBak10(tWFInsuInfos.get(1).getOccupationType());
    		}
    	}
        // 进行投保处理（试算-不提交数据库）
        String mBatchNo = mMsgHead.getBatchNo();
        String mSendDate = mMsgHead.getSendDate();
        String mSendTime = mMsgHead.getSendTime();
        String mBranchCode = mMsgHead.getBranchCode();
        String mSendOperator = mMsgHead.getSendOperator();
        String mMsgType = mMsgHead.getMsgType();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", mBatchNo);
        tTransferData.setNameAndValue("SendDate", mSendDate);
        tTransferData.setNameAndValue("SendTime", mSendTime);
        tTransferData.setNameAndValue("BranchCode", mBranchCode);
        tTransferData.setNameAndValue("SendOperator", mSendOperator);
        tTransferData.setNameAndValue("MsgType", mMsgType);

        VData tVData = new VData();
        tVData.add(tWFContInfo);
        tVData.add(tWFAppntInfo);
        tVData.add(tWFInsuInfos);
        tVData.add(tWFBnfInfos);
        tVData.add(tWFWrapExInfos);
        tVData.add(tLIWrapExListSet);
        
        tVData.add(tTransferData);

        GlobalInput tGi = new GlobalInput();
        tGi.ManageCom = "86";
        tGi.ComCode = "86";
        tGi.Operator = "001";
        tVData.add(tGi);

        SimPolicyBL tSimPolicyBL = new SimPolicyBL();
        if (!tSimPolicyBL.submitData(tVData, "SimPolicyTrade"))
        {
            String tErrInfo = tSimPolicyBL.mErrors.getFirstError();
            errLog(tErrInfo);
            return false;
        }
        // --------------------

        // 交易结果查询
        VData tResultDatas = tSimPolicyBL.getResult();
        if (!qryTradeResult(tResultDatas))
        {
            return false;
        }
        // --------------------

        return true;
    }
    
    private boolean sendSmsAndEmail(){
		if("".equals(BitchNo)||BitchNo==null){
    		System.out.println("LV获取批次号失败!");
    	}
		ExeSQL tExeSql = new ExeSQL();
		String tStrSql2 = "select wfa.name,wfa.mobile ,wfa.email "+		 
			 "from wfappntlist wfa where wfa.batchno = '" + BitchNo + "' and wfa.cardno = '"+mMsgWrapInfo.getCardNo()+"' ";
		SSRS checkSSRS2 = tExeSql.execSQL(tStrSql2);
		if(checkSSRS2 == null || checkSSRS2.MaxRow==0){
	        	System.out.println("获取发送短信和邮件信息失败!");
	        	return false;
	    }
		String tcertifyName = checkSSRS2.GetText(1, 1);;
		String tmobile = checkSSRS2.GetText(1, 2);;
		String tproposerEmail = checkSSRS2.GetText(1, 3);;
		
        String tStrSql = "select distinct lic.cardno,"+
			 			 "(select ldw.wrapname from LDWrap ldw ,lmcardrisk lmc where  ldw.riskwrapcode = lmc.riskcode and lmc.certifycode = wfc.certifycode FETCH FIRST 1 ROWS ONLY ),"+
          				 "lic.cvalidate,lic.inactivedate "+		 
          				 "from wfcontlist wfc,licardactiveinfolist lic where wfc.batchno = '" + BitchNo + "' "+
			 			 "and lic.cardno = wfc.cardno " +
          				 "and lic.cardno = '"+mMsgWrapInfo.getCardNo()+"' ";
        SSRS checkSSRS = tExeSql.execSQL(tStrSql);
        if(checkSSRS.MaxRow==0){
        	System.out.println("获取发送短信和邮件信息失败!");
        	return false;
        }
        int length = checkSSRS.MaxRow;
        for(int i=0;i<length;i++){
        	String tcardNo = "";
        	String tCValiDate = "";
        	String tCInValiDate = "";
        	tcardNo = checkSSRS.GetText(i+1, 1);
        	String tWrapName = checkSSRS.GetText(1, 2);
        	tCValiDate = checkSSRS.GetText(i+1, 3);
        	tCInValiDate = checkSSRS.GetText(i+1, 4);
        	System.out.println("tcardNo="+tcardNo);
        	System.out.println("tCValiDate="+tCValiDate);
        	System.out.println("tCInValiDate="+tCInValiDate);
        	mAndeMap.put("cardNo"+i, tcardNo);
			if(i==length-1){
				mAndeMap.put("endindex", ""+i);
			}
			mAndeMap.put("WrapName"+i, tWrapName);
			mAndeMap.put("CValiDate"+i, tCValiDate);
			mAndeMap.put("CInValiDate"+i, tCInValiDate);
        }
        mAndeMap.put("certifyName", tcertifyName);
        mAndeMap.put("mobile", tmobile);
        mAndeMap.put("orderID", BitchNo);
        mAndeMap.put("proposerEmail",tproposerEmail);
        mAndeMap.put("ProposerNeedSMS", "T");
        mAndeMap.put("ProposerName", tcertifyName);
        // 校验批次号唯一
        //计算满期日期
//        try {
//			tCInValiDate =  PubFun.calDate(df.parse(tCValiDate), Integer.parseInt(tinsuYear), tinsuYearFlag, df.parse(tCValiDate));
//		}catch (ParseException e) {
//			e.printStackTrace();
//		}
//        tCInValiDate = PubFun.calDate(tCInValiDate,-1,"D",tCInValiDate);
        
        //日期转换成string格式处理
		boolean messageFlag = true;
		boolean emailFlag = true;
		String errinfo = "";
		if(mAndeMap.get("ProposerNeedSMS").equals("T")){
			System.out.println(((String)mAndeMap.get("mobile")).length()+"----------mobile");
			//短息通知
			if(mAndeMap.get("mobile")!=null && !("").equals(mAndeMap.get("mobile"))){
				if(new LVEmailAndMessageBL().sendMsg(mAndeMap)){
					System.out.println("手机号为"+mAndeMap.get("mobile")+"的短息发送成功");
				}else{
					messageFlag = false;
					//errinfo = errinfo+"投保异常，短息发送失败  ";
					System.out.println("手机号为"+mAndeMap.get("mobile")+"的短息发送失败");
				}
			}
			
			//邮件通知
			if(mAndeMap.get("proposerEmail")!=null && !("").equals(mAndeMap.get("proposerEmail"))){
				if(LVEmailAndMessageBL.sendEmail(mAndeMap)){
//					System.out.println("邮箱号为"+mAndeMap.get("proposerEmail")+"的邮件发送成功");
				}else{
					//errinfo = errinfo+"投保异常，邮件发送失败";
					System.out.println("邮箱号为"+mAndeMap.get("proposerEmail")+"的邮件发送失败");
					emailFlag = false;
				}
				if(messageFlag&emailFlag){
					return true;
				}else{
					return false;
				}
			}
			
		}
		return true;
	}
    
    private WFContListSchema getContInfo()
    {
        WFContListSchema tWFContInfo = new WFContListSchema();

        // 基本交易数据
        tWFContInfo.setBatchNo(mMsgHead.getBatchNo());
        tWFContInfo.setSendDate(mMsgHead.getSendDate());
        tWFContInfo.setSendTime(mMsgHead.getSendTime());
        tWFContInfo.setBranchCode(mMsgHead.getBranchCode());
        tWFContInfo.setSendOperator(mMsgHead.getSendOperator());
        tWFContInfo.setMessageType(mMsgHead.getMsgType());
        // --------------------
        BitchNo = mMsgHead.getBatchNo();
        // tWFContInfo.setCardDealType("01");

        // 产品数据
        tWFContInfo.setCardNo(mMsgWrapInfo.getCardNo());
        ContNo = mMsgWrapInfo.getCardNo();
        tWFContInfo.setPassword(mMsgWrapInfo.getPassword());
        tWFContInfo.setCertifyCode(mMsgWrapInfo.getCertifyCode());
        tWFContInfo.setCertifyName(mMsgWrapInfo.getCertifyName());
        tWFContInfo.setRiskCode(mMsgWrapInfo.getWrapCode());

        if (mMsgWrapInfo.getPrem() == null || mMsgWrapInfo.getPrem().equals(""))
        {
            String tErrInfo = "承保交易中，承保保费信息不能为空。";
            errLog(tErrInfo);
            return null;
        }
        else
        {
            try
            {
                double tTmpPrem = Double.valueOf(mMsgWrapInfo.getPrem()).doubleValue();
                if (tTmpPrem == 0)
                {
                    String tErrInfo = "承保交易中，承保保费信息不能为0。";
                    errLog(tErrInfo);
                    return null;
                }
            }
            catch (Exception e)
            {
                String tErrInfo = "保费信息为非数值型数据。";
                errLog(tErrInfo);
                return null;
            }
        }

        tWFContInfo.setPrem(mMsgWrapInfo.getPrem());
        tWFContInfo.setAmnt(mMsgWrapInfo.getAmnt());
        tWFContInfo.setMult(mMsgWrapInfo.getMult());
        tWFContInfo.setCopys(mMsgWrapInfo.getCopys());
        // --------------------

        tWFContInfo.setActiveDate(mMsgWrapInfo.getActiveDate());

        // 投保单数据
        tWFContInfo.setManageCom(mMsgSimPolicyInfo.getManageCom());
        tWFContInfo.setSaleChnl(mMsgSimPolicyInfo.getSaleChnl());
        tWFContInfo.setAgentCom(mMsgSimPolicyInfo.getAgentCom());
        
        String agentcode = mMsgSimPolicyInfo.getAgentCode();
		AgentCodeTransformation a = new AgentCodeTransformation();
		if(!a.AgentCode(agentcode, "N")){
			System.out.println(a.getMessage());
		}
		agentcode = a.getResult();
		System.out.println(agentcode);
		
		tWFContInfo.setAgentCode(agentcode);
        
//        tWFContInfo.setAgentCode(mMsgSimPolicyInfo.getAgentCode());
        tWFContInfo.setAgentName(mMsgSimPolicyInfo.getAgentName());
        tWFContInfo.setPayMode(mMsgSimPolicyInfo.getPayMode());
        tWFContInfo.setPayIntv(mMsgSimPolicyInfo.getPayIntv());
        tWFContInfo.setPayYear(mMsgSimPolicyInfo.getPayYear());
        tWFContInfo.setPayYearFlag(mMsgSimPolicyInfo.getPayYearFlag());

        String tCValiDate = mMsgSimPolicyInfo.getCValiDate();
        if (tCValiDate == null || tCValiDate.equals(""))
        {
            String tErrInfo = "保单生效日期不能为空。";
            errLog(tErrInfo);
            return null;
        }
        tWFContInfo.setCvaliDate(mMsgSimPolicyInfo.getCValiDate());

        tWFContInfo.setCvaliDateTime(mMsgSimPolicyInfo.getCValiTime());
        tWFContInfo.setInsuYear(mMsgSimPolicyInfo.getInsuYear());
        tWFContInfo.setInsuYearFlag(mMsgSimPolicyInfo.getInsuYearFlag());

        tWFContInfo.setRemark(mMsgSimPolicyInfo.getRemark());
        // --------------------

        // 投保人关联数据 - 暂定用卡号
        tWFContInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        //将报文中印刷号保存起来
        if(!mMsgSimPolicyInfo.getPrtNo().equals(mMsgWrapInfo.getCardNo())){
        	tWFContInfo.setBak2(mMsgSimPolicyInfo.getPrtNo()); 
        }
        // --------------------

        // 特殊字段-客票号信息暂不存储
        tWFContInfo.setTicketNo(null);
        tWFContInfo.setTeamNo(null);
        tWFContInfo.setSeatNo(null);
        tWFContInfo.setFrom(null);
        tWFContInfo.setTo(null);
        // --------------------

        // 特殊字段 - 投保地域信息暂不存储
        tWFContInfo.setCardArea(null);
        //存放保单终止日期
        tWFContInfo.setBak3(mMsgSimPolicyInfo.getCInValiDate());
        // --------------------
        //添加批次号
        tWFContInfo.setBak4(mMsgHead.getBatchNo());

        return tWFContInfo;
    }

    private WFAppntListSchema getAppntInfo()
    {
        WFAppntListSchema tWFAppntInfo = new WFAppntListSchema();

        tWFAppntInfo.setBatchNo(mMsgHead.getBatchNo());

        // 投保人关联数据 - 暂定用卡号
        tWFAppntInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        // --------------------

        tWFAppntInfo.setCardNo(mMsgWrapInfo.getCardNo());

        // 投保人关联数据 - 暂定用卡号
        tWFAppntInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        // --------------------

        tWFAppntInfo.setName(mMsgSimAppntInfo.getName());
        tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
        tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());
        tWFAppntInfo.setIDType(mMsgSimAppntInfo.getIDType());
        tWFAppntInfo.setIDNo(mMsgSimAppntInfo.getIdNo());

        tWFAppntInfo.setOccupationType(mMsgSimAppntInfo.getOccupationType());
        tWFAppntInfo.setOccupationCode(mMsgSimAppntInfo.getOccupationCode());

        tWFAppntInfo.setPostalAddress(mMsgSimAppntInfo.getPostalAddress());
        tWFAppntInfo.setZipCode(mMsgSimAppntInfo.getZipCode());
        tWFAppntInfo.setPhont(mMsgSimAppntInfo.getPhone());
        tWFAppntInfo.setMobile(mMsgSimAppntInfo.getMobile());
        tWFAppntInfo.setEmail(mMsgSimAppntInfo.getEmail());

        tWFAppntInfo.setGrpName(mMsgSimAppntInfo.getGrpName());
        tWFAppntInfo.setCompanyPhone(mMsgSimAppntInfo.getCompayPhone());
        tWFAppntInfo.setCompAddr(mMsgSimAppntInfo.getCompAddr());
        tWFAppntInfo.setCompZipCode(mMsgSimAppntInfo.getCompZipCode());

        return tWFAppntInfo;
    }

    private WFInsuListSet getInsuInfos()
    {
        WFInsuListSet tWFInsuInfos = new WFInsuListSet();

        for (int i = 0; i < mMsgSimInsuInfoList.length; i++)
        {
            SimInsuInfo tSimInsuInfo = mMsgSimInsuInfoList[i];

            WFInsuListSchema tWFInsuInfo = new WFInsuListSchema();

            tWFInsuInfo.setBatchNo(mMsgHead.getBatchNo());

            tWFInsuInfo.setCardNo(mMsgWrapInfo.getCardNo());

            tWFInsuInfo.setInsuNo(tSimInsuInfo.getInsuNo());
            tWFInsuInfo.setToAppntRela(tSimInsuInfo.getToAppntRela());

            tWFInsuInfo.setRelationToInsured(tSimInsuInfo.getToMainInsuRela());

            tWFInsuInfo.setRelationCode(null); // 数据描述为该被保人是否连带，放到业务逻辑中进行处理
            tWFInsuInfo.setReInsuNo(null); // 数据描述为该被保人主被保人编号，放到业务逻辑中进行处理

            tWFInsuInfo.setName(tSimInsuInfo.getName());
            tWFInsuInfo.setSex(tSimInsuInfo.getSex());
            tWFInsuInfo.setBirthday(tSimInsuInfo.getBirthday());
            tWFInsuInfo.setIDType(tSimInsuInfo.getIDType());
            tWFInsuInfo.setIDNo(tSimInsuInfo.getIdNo());

            tWFInsuInfo.setOccupationType(tSimInsuInfo.getOccupationType());
            tWFInsuInfo.setOccupationCode(tSimInsuInfo.getOccupationCode());
            tWFInsuInfo.setPostalAddress(tSimInsuInfo.getPostalAddress());
            tWFInsuInfo.setZipCode(tSimInsuInfo.getZipCode());
            tWFInsuInfo.setPhont(tSimInsuInfo.getPhone());
            tWFInsuInfo.setMobile(tSimInsuInfo.getMobile());
            tWFInsuInfo.setEmail(tSimInsuInfo.getEmail());

            tWFInsuInfo.setGrpName(tSimInsuInfo.getGrpName());
            tWFInsuInfo.setCompanyPhone(tSimInsuInfo.getCompayPhone());
            tWFInsuInfo.setCompAddr(tSimInsuInfo.getCompAddr());
            tWFInsuInfo.setCompZipCode(tSimInsuInfo.getCompZipCode());

            tWFInsuInfos.add(tWFInsuInfo);
        }

        return tWFInsuInfos;
    }

    private WFBnfListSet getBnfInfos()
    {
        WFBnfListSet tWFBnfInfos = new WFBnfListSet();

        if (mMsgSimBnfInfoList != null)
        {
            for (int i = 0; i < mMsgSimBnfInfoList.length; i++)
            {
                SimBnfInfo tSimBnfInfo = mMsgSimBnfInfoList[i];

                WFBnfListSchema tWFInsuInfo = new WFBnfListSchema();

                tWFInsuInfo.setBatchNo(mMsgHead.getBatchNo());

                tWFInsuInfo.setCardNo(mMsgWrapInfo.getCardNo());

                tWFInsuInfo.setToInsuNo(tSimBnfInfo.getToInsuNo());
                tWFInsuInfo.setToInsuRela(tSimBnfInfo.getToInsuNo());

                tWFInsuInfo.setBnfType(tSimBnfInfo.getBnfType());
                tWFInsuInfo.setBnfGrade(tSimBnfInfo.getBnfGrade());
                tWFInsuInfo.setBnfRate(tSimBnfInfo.getBnfRate());

                tWFInsuInfo.setName(tSimBnfInfo.getName());
                tWFInsuInfo.setSex(tSimBnfInfo.getSex());
                tWFInsuInfo.setBirthday(tSimBnfInfo.getBirthday());
                tWFInsuInfo.setIDType(tSimBnfInfo.getIDType());
                tWFInsuInfo.setIDNo(tSimBnfInfo.getIdNo());

                tWFBnfInfos.add(tWFInsuInfo);
            }
        }

        return tWFBnfInfos;
    }

    private WFWrapExListSet getWrapExInfos()
    {
    	WFWrapExListSet tWFWrapExInfos = new WFWrapExListSet();
    	
    	if (mMsgWrapExInfoList != null)
    	{
    		for (int i = 0; i < mMsgWrapExInfoList.length; i++)
    		{
    			WrapExInfo tWrapExInfo = mMsgWrapExInfoList[i];
    			
    			WFWrapExListSchema tWFWrapExInfo = new WFWrapExListSchema();
    			
    			tWFWrapExInfo.setBatchNo(mMsgHead.getBatchNo());
    			
    			tWFWrapExInfo.setCardNo(mMsgWrapInfo.getCardNo());
    			
    			tWFWrapExInfo.setCertifyCode(tWrapExInfo.getCertifyCode());
    			
    			tWFWrapExInfo.setExFactorCode(tWrapExInfo.getExFactorCode());
    			
    			tWFWrapExInfo.setExFactorName(tWrapExInfo.getExFactorName());
    			
    			tWFWrapExInfo.setExFactorValue(tWrapExInfo.getExFactorValue());
    			
    			tWFWrapExInfo.setManageCom(mMsgSimPolicyInfo.getManageCom());
    			
    			tWFWrapExInfo.setOperator(mMsgHead.getSendOperator());
    			
    			tWFWrapExInfo.setMakeDate(mMsgHead.getSendDate());
    			
    			tWFWrapExInfo.setMakeTime(mMsgHead.getSendTime()); 
    			
    			tWFWrapExInfo.setModifyDate(mMsgHead.getSendDate());
    			
    			tWFWrapExInfo.setModifyTime(mMsgHead.getSendTime());
    			
    			tWFWrapExInfos.add(tWFWrapExInfo);
    		}
    	}
    	return tWFWrapExInfos;
    }

    private boolean qryTradeResult(VData cResultVDatas)
    {
        LICardActiveInfoListSet tResActCardInfos = (LICardActiveInfoListSet) cResultVDatas.getObjectByObjectName(
                "LICardActiveInfoListSet", 0);
        if (tResActCardInfos == null || tResActCardInfos.size() == 0)
        {
            String tErrInfo = "查询保单卡承保信息失败。";
            errLog(tErrInfo);
            return false;
        }

        WFContListSchema tResWFContInfo = (WFContListSchema) cResultVDatas.getObjectByObjectName("WFContListSchema", 0);
        if (tResWFContInfo == null)
        {
            String tErrInfo = "查询保单卡投保信息失败。";
            errLog(tErrInfo);
            return false;
        }

        WFAppntListSchema tResWFAppntInfo = (WFAppntListSchema) cResultVDatas.getObjectByObjectName(
                "WFAppntListSchema", 0);
        if (tResWFAppntInfo == null)
        {
            String tErrInfo = "查询保单卡投保人信息失败。";
            errLog(tErrInfo);
            return false;
        }

        WFInsuListSet tResWFInsuInfos = (WFInsuListSet) cResultVDatas.getObjectByObjectName("WFInsuListSet", 0);
        if (tResWFInsuInfos == null || tResWFInsuInfos.size() == 0)
        {
            String tErrInfo = "查询保单卡被保人信息失败。";
            errLog(tErrInfo);
            return false;
        }

        WFBnfListSet tResWFBnfInfos = (WFBnfListSet) cResultVDatas.getObjectByObjectName("WFBnfListSet", 0);
        if (tResWFInsuInfos == null)
        {
            String tErrInfo = "查询保单卡受益人信息失败。";
            errLog(tErrInfo);
            return false;
        }

        // 查询保单投保信息
        SimPolicyInfo tSimPolicyInfo = new SimPolicyInfo();

        tSimPolicyInfo.setPrtNo(tResWFContInfo.getCardNo());

        tSimPolicyInfo.setManageCom(tResWFContInfo.getManageCom());
        tSimPolicyInfo.setSaleChnl(tResWFContInfo.getSaleChnl());
        tSimPolicyInfo.setAgentCom(tResWFContInfo.getAgentCom());
        
        String agentcode = mMsgSimPolicyInfo.getAgentCode();
		AgentCodeTransformation a = new AgentCodeTransformation();
		if(!a.AgentCode(agentcode, "N")){
			System.out.println(a.getMessage());
		}
		agentcode = a.getResult();
		System.out.println(agentcode);
        
        tSimPolicyInfo.setAgentCode(agentcode);
        tSimPolicyInfo.setAgentName(tResWFContInfo.getAgentName());

        // tSimPolicyInfo.setPolApplyDate(null);

        tSimPolicyInfo.setCValiDate(tResWFContInfo.getCvaliDate());
        tSimPolicyInfo.setCValiTime(tResWFContInfo.getCvaliDateTime());

        tSimPolicyInfo.setCInValiDate(tResActCardInfos.get(1).getInActiveDate());
        //tSimPolicyInfo.setCInValiTime(null);

        tSimPolicyInfo.setInsuYear(String.valueOf(tResWFContInfo.getInsuYear()));
        tSimPolicyInfo.setInsuYearFlag(tResWFContInfo.getInsuYearFlag());

        tSimPolicyInfo.setPremScope(String.valueOf(tResWFContInfo.getPrem()));

        tSimPolicyInfo.setPayIntv(tResWFContInfo.getPayIntv());
        tSimPolicyInfo.setPayYear(String.valueOf(tResWFContInfo.getPayYear()));
        tSimPolicyInfo.setPayYearFlag(tResWFContInfo.getPayYearFlag());
        tSimPolicyInfo.setPayMode(tResWFContInfo.getPayMode());

        tSimPolicyInfo.setBankCode(null);
        tSimPolicyInfo.setBankAccNo(null);
        tSimPolicyInfo.setAccName(null);

        tSimPolicyInfo.setRemark(tResWFContInfo.getRemark());

        putResult("SimPolicyInfo", tSimPolicyInfo);
        // --------------------

        // 查询产品信息
        WrapInfo tWrapInfo = new WrapInfo();

        tWrapInfo.setCardNo(tResWFContInfo.getCardNo());
        tWrapInfo.setPassword(tResWFContInfo.getPassword());

        tWrapInfo.setWrapCode(tResWFContInfo.getRiskCode());
        // tWrapInfo.setWrapName(null);

        tWrapInfo.setCertifyCode(tResWFContInfo.getCertifyCode());
        tWrapInfo.setCertifyName(tResWFContInfo.getCertifyName());
        tWrapInfo.setActiveDate(tResWFContInfo.getActiveDate());
        tWrapInfo.setPrem(String.valueOf(tResWFContInfo.getPrem()));
        tWrapInfo.setAmnt(String.valueOf(tResWFContInfo.getAmnt()));
        tWrapInfo.setMult(String.valueOf(tResWFContInfo.getMult()));
        tWrapInfo.setCopys(String.valueOf(tResWFContInfo.getCopys()));

        // tWrapInfo.setReMark(null);

        putResult("WrapInfo", tWrapInfo);
        // --------------------

        // 查询产品扩展信息
        //putResult("WrapExInfo", null);
        // --------------------

        // 查询投保人信息
        SimAppntInfo tSimAppntInfo = new SimAppntInfo();

        tSimAppntInfo.setName(tResWFAppntInfo.getName());
        tSimAppntInfo.setSex(tResWFAppntInfo.getSex());
        tSimAppntInfo.setBirthday(tResWFAppntInfo.getBirthday());
        tSimAppntInfo.setIDType(tResWFAppntInfo.getIDType());
        tSimAppntInfo.setIdNo(tResWFAppntInfo.getIDNo());

        tSimAppntInfo.setOccupationType(tResWFAppntInfo.getOccupationType());
        tSimAppntInfo.setOccupationCode(tResWFAppntInfo.getOccupationCode());

        tSimAppntInfo.setPostalAddress(tResWFAppntInfo.getPostalAddress());
        tSimAppntInfo.setZipCode(tResWFAppntInfo.getZipCode());
        tSimAppntInfo.setPhone(tResWFAppntInfo.getPhont());
        tSimAppntInfo.setMobile(tResWFAppntInfo.getMobile());
        tSimAppntInfo.setEmail(tResWFAppntInfo.getEmail());

        tSimAppntInfo.setGrpName(tResWFAppntInfo.getGrpName());
        tSimAppntInfo.setCompayPhone(tResWFAppntInfo.getCompanyPhone());
        tSimAppntInfo.setCompAddr(tResWFAppntInfo.getCompAddr());
        tSimAppntInfo.setCompZipCode(tResWFAppntInfo.getCompZipCode());

        putResult("SimAppntInfo", tSimAppntInfo);
        // --------------------

        // 查询被保人信息
        for (int i = 1; i <= tResWFInsuInfos.size(); i++)
        {
            WFInsuListSchema tWFInsuInfo = tResWFInsuInfos.get(i);

            SimInsuInfo tSimInsuInfo = new SimInsuInfo();

            tSimInsuInfo.setInsuNo(tWFInsuInfo.getInsuNo());
            tSimInsuInfo.setToMainInsuRela(tWFInsuInfo.getRelationToInsured());
            tSimInsuInfo.setToAppntRela(tWFInsuInfo.getToAppntRela());

            tSimInsuInfo.setName(tWFInsuInfo.getName());
            tSimInsuInfo.setSex(tWFInsuInfo.getSex());
            tSimInsuInfo.setBirthday(tWFInsuInfo.getBirthday());
            tSimInsuInfo.setIDType(tWFInsuInfo.getIDType());
            tSimInsuInfo.setIdNo(tWFInsuInfo.getIDNo());

            tSimInsuInfo.setOccupationType(tWFInsuInfo.getOccupationType());
            tSimInsuInfo.setOccupationCode(tWFInsuInfo.getOccupationCode());

            tSimInsuInfo.setPostalAddress(tWFInsuInfo.getPostalAddress());
            tSimInsuInfo.setZipCode(tWFInsuInfo.getZipCode());
            tSimInsuInfo.setPhone(tWFInsuInfo.getPhont());
            tSimInsuInfo.setMobile(tWFInsuInfo.getMobile());
            tSimInsuInfo.setEmail(tWFInsuInfo.getEmail());
            tSimInsuInfo.setGrpName(tWFInsuInfo.getGrpName());
            tSimInsuInfo.setCompayPhone(tWFInsuInfo.getCompanyPhone());
            tSimInsuInfo.setCompAddr(tWFInsuInfo.getCompAddr());
            tSimInsuInfo.setCompZipCode(tWFInsuInfo.getCompZipCode());

            // tSimInsuInfo.setEnglishName(null);
            putResult("SimInsuInfo", tSimInsuInfo);
        }
        // --------------------

        // 查询受益人信息
        for (int i = 1; i <= tResWFBnfInfos.size(); i++)
        {
            WFBnfListSchema tWFBnfListInfo = tResWFBnfInfos.get(i);

            SimBnfInfo tSimBnfInfo = new SimBnfInfo();

            tSimBnfInfo.setToInsuNo(tWFBnfListInfo.getToInsuNo());

            tSimBnfInfo.setBnfType(tWFBnfListInfo.getBnfType());
            tSimBnfInfo.setBnfGrade(tWFBnfListInfo.getBnfGrade());
            tSimBnfInfo.setBnfRate(String.valueOf(tWFBnfListInfo.getBnfRate()));

            tSimBnfInfo.setName(tWFBnfListInfo.getName());
            tSimBnfInfo.setSex(tWFBnfListInfo.getSex());
            tSimBnfInfo.setBirthday(tWFBnfListInfo.getBirthday());
            tSimBnfInfo.setIDType(tWFBnfListInfo.getIDType());
            tSimBnfInfo.setIdNo(tWFBnfListInfo.getIDNo());

            putResult("SimBnfInfo", tSimBnfInfo);
        }
        // --------------------

        return true;
    }
}
