package com.cbsws.xml.ctrl.blogic.simsale;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.cbsws.core.xml.config.CfgBusLogicImp;
import com.cbsws.core.xml.config.CfgFactory;
import com.cbsws.core.xml.config.ICfgProp;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CardNoManager {
	private String errLog = "";

	private static InputStream tIs = null;
	public String[] getCardNo(String WrapCode, String CertifyCode,String MsgType) {

		String[] info = new String[3];
		String tWrapCode = WrapCode;
		if (tWrapCode == null || tWrapCode.equals("")) {
			info[2] = "报文没有传套餐编码";
			return info;
		}
		String tCertifyCode = "";
		if (CertifyCode != null && !CertifyCode.equals("")) {
			tCertifyCode = CertifyCode;
		} else {
			tCertifyCode = getCertifyCode(tWrapCode);
		}
		if (tCertifyCode == null || tCertifyCode.equals("")) {
			info[2] = errLog;
			return info;
		}
		if (MsgType == null || MsgType.equals("")) {
			info[2] = "报文交易类型不可以为空！";
			return info;
		}
		String tAgentCom = getAgentComs(MsgType);
		System.out.println("tAgentCom=" + tAgentCom);
		// String manageCom_Key = MsgType+"manageCom";
		// String manageCom = mProp.getProperty(manageCom_Key);
		// System.out.println("manageCom="+manageCom);
		int tSyncCount = 1;
		// date 20101109 by 郭忠华 调整逻辑
		String tStrSql = /*" select tmp.RowId, tmp.CardNo, tmp.CardPassword   "
				+ " from  "
				+ " (  "
				+ " select row_number() over() as RowId, lzcn.CardNo, lzcn.CardPassword, lzc.CertifyCode, lzc.SubCode, lzc.State, lzc.ReceiveCom  "
				+ " from LZCardNumber lzcn  "
				+ " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo  "
				+ " where 1 = 1  "
				+ " and lzc.CertifyCode = '"
				+ tCertifyCode
				+ "'  "
				+ " and lzc.State in ('10', '11') "
				+ " and lzc.ReceiveCom in ("
				+ tAgentCom
				+ ") and not exists (select 1 from licardactiveinfolist where cardno = lzcn.cardno)"
				+ " order by lzcn.CardNo ) as tmp  " + " where 1 = 1 " + " and tmp.RowId <= "
				+ tSyncCount + "  " + "  ";*/
				//by  wxd 
				" select lzcn.cardno, lzcn.CardPassword "
				  +" from LZCardNumber lzcn "
				 +" where lzcn.cardtype = (select subcode from lmcertifydes where CertifyCode = '" + tCertifyCode + "') " 
				   +" and lzcn.cardserno in (select lzc.startno "
				                           +" from lzcard lzc "
				                          +" where 1 = 1 "
				                             +" and lzc.CertifyCode = '" + tCertifyCode + "' "
				                            +" and lzc.State in ('10', '11') "
				                           +" and lzc.ReceiveCom in (" + tAgentCom + ") "
				                           +" and lzc.SUMCOUNT = 1) "
				    +" and not exists (select 1 "
				         +" from licardactiveinfolist "
				        +"  where cardno = lzcn.cardno) "
				 +" order by lzcn.CardNo fetch first  " + tSyncCount + "  rows only ";

		SSRS tCardInfo = new ExeSQL().execSQL(tStrSql);
		if (tCardInfo == null || tCardInfo.MaxRow < tSyncCount) {
			errLog = "可用单证数量不足申请数量【" + tSyncCount + "】张。";
			info[2] = errLog;
			return info;
		}

		String cardno = tCardInfo.GetText(1, 1);
		String password = tCardInfo.GetText(1, 2);
		info[0] = cardno;
		info[1] = new LisIDEA().decryptString(password);
		return info;
	}

	private String getCertifyCode(String cWrapCode) {
		String tStrSql = " select trim(lmcd.CertifyCode) from LMCardRisk lmcr "
				+ " inner join LMCertifyDes lmcd on lmcd.CertifyCode = lmcr.CertifyCode "
				+ " where 1 = 1 and lmcr.RiskType = 'W' "
				+ " and lmcr.RiskCode = '" + cWrapCode + "' ";
		SSRS tSSCertCode = new ExeSQL().execSQL(tStrSql);
		if (tSSCertCode == null || tSSCertCode.MaxRow != 1) {
			errLog = "获取单证编码失败，或该产品可能对应多条单证信息。";
			return null;
		}
		return tSSCertCode.GetText(1, 1);
	}

	private String getAgentComs(String MsgType) {
		String tStrSql = " select codename from ldcode where codetype='nocardnotrade' "
		+" and code='"+MsgType+"'";
		SSRS tAgentCom = new ExeSQL().execSQL(tStrSql);
		if (tAgentCom == null || tAgentCom.MaxRow != 1) {
			errLog = "获取"+MsgType+"类型的代理机构编码失败，获取可用卡号失败！";
			return null;
		}
		return tAgentCom.GetText(1, 1);
	}
}
