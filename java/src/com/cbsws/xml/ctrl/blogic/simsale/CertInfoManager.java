/**
 * 2011-9-28
 */
package com.cbsws.xml.ctrl.blogic.simsale;

import com.sinosoft.lis.db.LMCardRiskDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.db.LZCardNumberDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.schema.LZCardNumberSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LMCardRiskSet;
import com.sinosoft.lis.vschema.LZCardNumberSet;
import com.sinosoft.lis.vschema.LZCardSet;

/**
 * @author LY
 *
 */
public final class CertInfoManager
{
    private CertInfoManager()
    {
    }

    public static boolean verifyCertInfo(StringBuffer cErrInfos, String cCardNo, String cPassWord, String cCertifyCode,
            String cWrapCode)
    {
        String tStrSql = null;

        // 校验用户名密码
        LZCardNumberSchema tCardNumberInfo = null;
        tStrSql = " select * from LZCardNumber where CardNo = '" + cCardNo + "' ";
        LZCardNumberSet tLZCardNumberSet = new LZCardNumberDB().executeQuery(tStrSql);
        if (tLZCardNumberSet == null || tLZCardNumberSet.size() != 1)
        {
            if (tLZCardNumberSet.size() == 0)
            {
                String tErrMsg = "[" + cCardNo + "]" + "卡号不存在。";
                errLog(cErrInfos, tErrMsg);
                return false;
            }
            else
            {
                String tErrMsg = "[" + cCardNo + "]" + "卡信息异常。";
                errLog(cErrInfos, tErrMsg);
                return false;
            }
        }
        tStrSql = null;

        tCardNumberInfo = tLZCardNumberSet.get(1);

        String tPassWord = tCardNumberInfo.getCardPassword();
        tPassWord = new LisIDEA().decryptString(tPassWord);

        if (tPassWord == null || !tPassWord.equals(cPassWord))
        {
            String tErrMsg = "[" + cCardNo + "]" + "密码错误。";
            errLog(cErrInfos, tErrMsg);
            return false;
        }
        // --------------------

        // 校验单证相关
        tStrSql = " select lzc.* "
                + " from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where 1 = 1 " + " and lzcn.CardNo = '" + cCardNo + "' ";
        LZCardSet tCertInfos = new LZCardDB().executeQuery(tStrSql);
        if (tCertInfos == null || tCertInfos.size() == 0)
        {
            String tErrMsg = "[" + cCardNo + "]" + "单证信息未找到。";
            errLog(cErrInfos, tErrMsg);
            return false;
        }
        else if (tCertInfos.size() != 1)
        {
            String tErrMsg = "[" + cCardNo + "]" + "单证信息出现多条。";
            errLog(cErrInfos, tErrMsg);
            return false;
        }
        tStrSql = null;

        LZCardSchema tCertCardInfo = tCertInfos.get(1);

        if (!tCertCardInfo.getCertifyCode().equals(cCertifyCode))
        {
            String tErrMsg = "[" + cCardNo + "]" + "与[" + cCertifyCode + "]单证类型不符。";
            errLog(cErrInfos, tErrMsg);
            return false;
        }

        if ("14".equals(tCertCardInfo.getState()) || "1".equals(tCertCardInfo.getActiveFlag()))
        {
            String tErrMsg = "[" + cCardNo + "]" + "已经激活或售出。";
            errLog(cErrInfos, tErrMsg);
            return false;
        }
        else if (!"13".equals(tCertCardInfo.getState())&&!"2".equals(tCertCardInfo.getState())&&!"10".equals(tCertCardInfo.getState()) && !"11".equals(tCertCardInfo.getState()))
        {
            String tErrMsg = "[" + cCardNo + "]" + "尚未下发。";
            errLog(cErrInfos, tErrMsg);
            return false;
        }
        // --------------------

        // 校验产品信息
        tStrSql = " select 1 from LMCardRisk lzcr where lzcr.CertifyCode = '" + cCertifyCode
                + "' and lzcr.RiskType = 'W' ";
        LMCardRiskSet tCardRiskInfos = new LMCardRiskDB().executeQuery(tStrSql);
        if (tCardRiskInfos == null || tCardRiskInfos.size() != 1)
        {
            String tErrMsg = "[" + cCardNo + "]" + "与产品信息[" + cWrapCode + "]不符。";
            errLog(cErrInfos, tErrMsg);
            return false;
        }
        tStrSql = null;
        // --------------------

        return true;
    }

    private static void errLog(StringBuffer cErrInfos, String cErrMsg)
    {
        System.out.println("CertInfoManager: " + cErrMsg);
        if (cErrInfos == null)
            return;
        cErrInfos.append(cErrMsg);
    }
}
