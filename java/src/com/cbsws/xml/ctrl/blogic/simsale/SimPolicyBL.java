/**
 * 2011-10-12
 */
package com.cbsws.xml.ctrl.blogic.simsale;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.certifybusiness.CertifyDiskImportLog;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LIWrapExListSchema;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.schema.WFTransLogSchema;
import com.sinosoft.lis.schema.WFWrapExListSchema;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LIWrapExListSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.lis.vschema.WFWrapExListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class SimPolicyBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;
    
    private CertifyDiskImportLog mImportLog = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mBatchNo = null;

    private String mSendDate = null;

    private String mSendTime = null;

    private String mBranchCode = null;

    private String mSendOperator = null;

    private String mMsgType = null;
    
    private boolean familyFlag = false;
    
    String mRiskWrapCode = "";//套餐编码
	String mCardNo = "";
	String mCValidate = "";
	String mOccupationType = "";
	int mCopys = 0;
	int mMult = 0;
	String mCertifyCode = "";
	double tAmnt = 0;
	double tPrem = 0;
	double tSysWrapSumPrem = 0; //系统计算出的保费
    double tSysWrapSumAmnt = 0;//系统计算出的保额
    private String Error = "";
    private ArrayList mUwErrorList = new ArrayList();
	ExeSQL tExeSQL = new ExeSQL();

    private LICardActiveInfoListSet mActCardInfos = null;

    private WFContListSchema mWFContInfo = null;

    private WFAppntListSchema mWFAppntInfo = null;

    private WFInsuListSet mWFInsuInfos = null;

    private WFBnfListSet mWFBnfInfos = null;
    
    private WFWrapExListSet mWFWrapExListSet = null;
    
    private LIWrapExListSet mLIWrapExListSet = null;

    public SimPolicyBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if ("SimPolicyTrade".equals(cOperate))
        {
            if (!submit())
            {
                return false;
            }
            
           //生成电子保单
           // ECPrintOtherBL ecprint = new ECPrintOtherBL();
           //  String flag = ecprint.deal(mCardNo,mMsgType);
           // if(flag!=null){
           // 	buildError("ECOtherPrint", flag);
           //     return false;
           // }
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        if (!prepareResult())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mWFContInfo = (WFContListSchema) cInputData.getObjectByObjectName("WFContListSchema", 0);
        if (mWFContInfo == null)
        {
            buildError("getInputData", "未获取到产品投保信息。");
            return false;
        }

        mWFAppntInfo = (WFAppntListSchema) cInputData.getObjectByObjectName("WFAppntListSchema", 0);
        if (mWFAppntInfo == null)
        {
            buildError("getInputData", "未获取到投保人信息。");
            return false;
        }

        mWFInsuInfos = (WFInsuListSet) cInputData.getObjectByObjectName("WFInsuListSet", 0);
        if (mWFInsuInfos == null || mWFInsuInfos.size() == 0)
        {
            buildError("getInputData", "未获取到被保人信息。");
            return false;
        }

        mWFBnfInfos = (WFBnfListSet) cInputData.getObjectByObjectName("WFBnfListSet", 0);
        
        mWFWrapExListSet = (WFWrapExListSet) cInputData.getObjectByObjectName("WFWrapExListSet", 0);
        
        mLIWrapExListSet = (LIWrapExListSet) cInputData.getObjectByObjectName("LIWrapExListSet", 0);

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "获取交易批次号失败。");
            return false;
        }

        mSendDate = (String) mTransferData.getValueByName("SendDate");
        if (mSendDate == null || mSendDate.equals(""))
        {
            buildError("getInputData", "获取交易报送日期失败。");
            return false;
        }
        mSendDate = PubFun.getCurrentDate();
        mSendTime = (String) mTransferData.getValueByName("SendTime");
        if (mSendTime == null || mSendTime.equals(""))
        {
            buildError("getInputData", "获取交易报送时间失败。");
            return false;
        }
        mSendTime = PubFun.getCurrentTime();
        mBranchCode = (String) mTransferData.getValueByName("BranchCode");
        if (mBranchCode == null || mBranchCode.equals(""))
        {
            buildError("getInputData", "获取交易报送单位失败。");
            return false;
        }

        mSendOperator = (String) mTransferData.getValueByName("SendOperator");
        if (mSendOperator == null || mSendOperator.equals(""))
        {
            buildError("getInputData", "获取交易操作员失败。");
            return false;
        }

        mMsgType = (String) mTransferData.getValueByName("MsgType");
        if (mMsgType == null || mMsgType.equals(""))
        {
            buildError("getInputData", "获取交易类型失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
//    	String ca = mWFContInfo.getActiveDate();
//    	String cv = mWFContInfo.getCvaliDate();
//    	String cd = PubFun.getCurrentDate();
//    	if(!ca.equals(cd)){
//    		buildError("checkData", "激活日期不为当天。");
//    		return false;
//    	}
//    	String date1 = StrTool.cTrim(cv);
//        String date2 = StrTool.cTrim(cd);
//        FDate fd = new FDate();
//        Date tCValidate = fd.getDate(date1);
//        Date tCurrentDate = fd.getDate(date2);
//    	if(tCValidate.before(tCurrentDate)){
//    		buildError("checkData", "生效日期不能早于当天。");
//    		return false;
//    	}
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        // 处理交易业务数据
        tTmpMap = null;
        lockCardNo(mWFContInfo.getCardNo());
        tTmpMap = createCertCont();
        if (tTmpMap == null)
        {
            dealWFTransLog(false);
            return false;
        }
        mMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 处理交易日志
        dealWFTransLog(true);
        // --------------------

        return true;
    }
    /**
     * 锁定动作
     * @param polBalaCount
     * @return MMap
     */
    private boolean lockCardNo(String aCardNo)
    {
        MMap tMMap = null;
        /**承保获取可用卡号 "CN"*/
        String tLockNoType = "CN";
        /**锁定有效时间（秒）*/
        String tAIS = "10";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", aCardNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return false;
        }
        mMap.add(tMMap);
        return true;
    }
    private MMap createCertCont()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

//      支持驴妈妈家庭单
		String tempWrapCode = this.mWFContInfo.getRiskCode();
		String psql = "select code from ldcode where codetype='familycard'";
		SSRS productSet  = tExeSQL.execSQL(psql);
		List list = new ArrayList();
		if(productSet != null){
			for(int i=1;i<= productSet.MaxRow;i++){
				list.add(productSet.GetText(i, 1));
			}
		}
		if(list.contains(tempWrapCode)){
			this.familyFlag = true;
		}
		
        // 处理单证信息
        tTmpMap = null;
        tTmpMap = dealCertInfo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 处理保单信息
        if (!dealWFContInfo())
        {
            return null;
        }
        // --------------------

        // 处理投保人信息
        if (!dealWFAppntInfo())
        {
            return null;
        }
        // --------------------

        // 处理被保人信息
        if (!dealWFInsuInfo())
        {
            return null;
        }
        // --------------------

        // 处理受益人信息
        if (!dealWFBnfInfo())
        {
            return null;
        }
        // --------------------
        
        // 处理扩展信息（报文备份）
        if(mWFWrapExListSet!=null){
        	if (!dealWFWrapExInfo())
            {
            	return null;
            }
        }
        // --------------------
    
        // 处理扩展信息（业务）
        if(mLIWrapExListSet!=null){
        	if (!dealLIWrapExInfo())
            {
            	return null;
            }
        }
        // --------------------
        
        // 处理承保中间表信息
        if (!dealActCardInfo())
        {
            return null;
        }
        // --------------------
        // 校验信息
		if(!checkAll(mWFContInfo,mWFInsuInfos))
        {
        	return null;
        }
		
        // --------------------
	
		
		if(list.contains(tempWrapCode)){
			if(!setTotalPremAmnt()){
	        	return null;
	        }
		}
		
        // 封装数据库
        tMMap.put(mActCardInfos, SysConst.INSERT);
        tMMap.put(mWFContInfo, SysConst.INSERT);
        tMMap.put(mWFAppntInfo, SysConst.INSERT);
        tMMap.put(mWFInsuInfos, SysConst.INSERT);
        tMMap.put(mWFBnfInfos, SysConst.INSERT);
        tMMap.put(mWFWrapExListSet, SysConst.INSERT);
        tMMap.put(mLIWrapExListSet, SysConst.INSERT);
        // --------------------

        return tMMap;
    }
    
    /**
     * 对于驴妈妈家庭单，wfcontlist存总保费、保额
     * @return
     */
    public boolean setTotalPremAmnt(){
    	try{
    		double tprem = 0.0;
    		double tamnt = 0.0;
    		if(this.mActCardInfos != null && this.mActCardInfos.size() > 0){
    			for(int i=1;i<=this.mActCardInfos.size();i++){
    				tprem += Double.parseDouble(mActCardInfos.get(i).getPrem());
    				tamnt += Double.parseDouble(mActCardInfos.get(i).getAmnt());
    			}
    		}
    		
    		this.mWFContInfo.setPrem(tprem);
    		this.mWFContInfo.setAmnt(tamnt);
    		return true;
    	}catch(Exception e){
    		e.printStackTrace();
    		buildError("setTotalPremAmnt", "保单表WFContList汇总保费失败");
            return false;
    	}
    }
    
    public boolean checkAll(WFContListSchema contListSchema,WFInsuListSet tInsuListSet) {
    	mRiskWrapCode = contListSchema.getRiskCode();//套餐编码
    	mCardNo = contListSchema.getCardNo();
    	mCValidate = contListSchema.getCvaliDate();
    	mCopys = contListSchema.getCopys();
    	mMult = contListSchema.getMult();
    	mCertifyCode = contListSchema.getCertifyCode();
		tAmnt = contListSchema.getAmnt();
		tPrem = contListSchema.getPrem();
		mBatchNo = contListSchema.getBatchNo();
		mGlobalInput.Operator = "第三方";//该功能校验第三方传送至核心的数据，则Operator默认为该名。
    	if(mRiskWrapCode==null||mRiskWrapCode.equals("")){
    		String sql = "select riskcode from lmcardrisk where certifycode='"+mCertifyCode+"' with ur";
    		mRiskWrapCode = tExeSQL.getOneValue(sql);	
    	}
		if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "获取批次信息失败，请检查文件名是否符合批次命名规则。");
            return false;
        }
        try
        {
            mImportLog = new CertifyDiskImportLog(mGlobalInput, mBatchNo);
        }
        catch (Exception e)
        {
            String tStrErr = "创建日志失败。";
            buildError("getInputData", tStrErr);
            e.printStackTrace();
            return false;
        }
		if(mCopys == 0 ){//当第三方向核心发送份数为0时，则为未传递份数，则认为购买1份
			mCopys =1;
		}
//		校验传送年龄 职业类别 所购买份数 是否符合产品描述
		if(!checkInsuredInfos(tInsuListSet)){
			return false;
		}
//		校验特殊规定
		if(!checkSpec(tInsuListSet)){
			String UWErrors =  this.getUWErrors();
			buildError("checkSpec",UWErrors);
			if (!mImportLog.errLog(mCardNo, UWErrors))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
			return false;
		}
//		校验保费保额
		if(!dealCertifyWrapParams()){
			return false;
		}
		return true;
	}
    private boolean checkInsuredInfos(WFInsuListSet tInsuListSet){
    	
		for (int i = 1; i <= tInsuListSet.size(); i++) {
			String Name = tInsuListSet.get(i).getName();//姓名
			String Sex = tInsuListSet.get(i).getSex();//性别
			String Birthday = tInsuListSet.get(i).getBirthday();//出生日期
			String IDType = tInsuListSet.get(i).getIDType();//证件类型
			String IDNo = tInsuListSet.get(i).getIDNo();//证件号码
			String occupAtionType = tInsuListSet.get(i).getOccupationType();//职业类别
			if(Birthday.indexOf("-")==-1){
				Error = "被保人出生日期格式不正确，请输入标准日期格式如：2007-03-08!";
				buildError("checkInsuredInfos",Error);
				if (!mImportLog.errLog(mCardNo, Error))
	            {
	                mErrors.copyAllErrors(mImportLog.mErrors);
	                return false;
	            }
				return false;
			}
			//校验被保人年龄
			String ageWrapSql = "select case when maxinsuredage is null then -1 else maxinsuredage end,"
					+ " case when mininsuredage is null then -1 else mininsuredage end "
					+ " from ldriskwrap where riskwrapcode = '"
					+ mRiskWrapCode
					+ "' ";
			SSRS ageSSRS = tExeSQL.execSQL(ageWrapSql);
			if (ageSSRS != null && ageSSRS.MaxRow > 0) {
				if (!"".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"-1".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"0".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"".equals(StrTool.cTrim(ageSSRS.GetText(1, 2)))
						&& !"-1".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))) {
					int tInsurrdAge = PubFun.calInterval(Birthday, PubFun.getCurrentDate(), "Y");
					if (Integer.parseInt(ageSSRS.GetText(1, 1)) < tInsurrdAge
							|| Integer.parseInt(ageSSRS.GetText(1, 2)) > tInsurrdAge) {
						Error = "被保人年龄不在投保年龄范围内!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
				}
			}
			//校验被保人职业类别
			String occutypeWrapSql = "select maxoccutype from ldwrap where riskwrapcode = '"+ mRiskWrapCode + "' ";
			SSRS occutypeSSRS = tExeSQL.execSQL(occutypeWrapSql);
			if (occutypeSSRS != null && occutypeSSRS.MaxRow > 0) {
				if (!"".equals(StrTool.cTrim(occutypeSSRS.GetText(1, 1)))) {
					if(occupAtionType == null || "".equals(occupAtionType)){
						Error = "被保人职业类别不可以为空!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
					if(!PubFun.isNumLetter(occupAtionType)){
						Error = "被保人职业类别应为数字，请核实!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
					if (Integer.parseInt(occutypeSSRS.GetText(1, 1)) < Integer
							.parseInt(occupAtionType)) {
						Error = "被保人职业类别不在投保职业类别范围内!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
				}
			}
			//校验份数
			//准备校验SQL,校验该激活卡是否是lmcardrisk中的risktype = 'W'的类型
			String checkSql = " select lmcr.riskcode, re.CertifyCode from lmcardrisk lmcr,"
					+ "(select lzcn.CardNo, lzc.CertifyCode, lzc.State from LZCardNumber lzcn "
					+ " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and "
					+ "lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
					+ "where 1 = 1 and lzcn.CardNo = '"
					+ mCardNo
					+ "') re where lmcr.risktype = 'W' and lmcr.certifycode = re.certifycode ";
			SSRS checkSSRS = tExeSQL.execSQL(checkSql);

			String insureSql = null;
			String cardNoStr = mCardNo.toString().substring(0, 2);
			int alreadyNo = mCopys;//将本次购买份数保存到alreadyNo，然后再去加以后份数，校验总分数是否超过最大份数。
			if (checkSSRS.MaxRow != 0
					&& (mCValidate != null || mCValidate != "")) {
				//校验E安康
				System.out.println(mMsgType);
				System.out.println(mCertifyCode);
				if("EJ0002".equals(mMsgType)){
					insureSql = "select cardno,Cvalidate,InActiveDate,copys from LICardActiveInfoList where cardtype='"//--------fromYZF
	                        +mCertifyCode+"' and name='"+ Name + "' and sex='"+ Sex + "' and birthday='"+ Birthday
							+ "' and idtype='"+ IDType + "' and idno='"	+ IDNo	+ "' and InActiveDate>'"+ PubFun.getCurrentDate()+ "' "
							+"and (CardStatus = '01' or CardStatus is null)";
					System.out.println(insureSql);
					SSRS insureSSRS = tExeSQL.execSQL(insureSql);
					FDate fdate = new FDate();
					LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
					for (int j = 1; j <= insureSSRS.MaxRow; j++) {
						Date oldCvalidate = fdate.getDate(insureSSRS.GetText(j, 2)); //结果中的生效日期，用FDate处理
						Date oldInactivedate = fdate.getDate(insureSSRS.GetText(j,3)); //结果中失效日期，用FDate处理
						String copys=insureSSRS.GetText(j,4);
						Date newCvalidate = fdate.getDate(mCValidate); //当前激活卡的生效日期，用FDate处理
						String[] inactivedate = tLoginVerifyTool.getMyProductInfo(mCardNo, mCValidate,String.valueOf(mMult));
						Date newInactivedate = fdate.getDate(inactivedate[2]); //当前激活卡的失效日期，用FDate处理		        
						if ((oldCvalidate.compareTo(newCvalidate) <= 0 && oldInactivedate.compareTo(newCvalidate) > 0)
								|| (oldCvalidate.compareTo(newInactivedate) < 0 && oldInactivedate.compareTo(newInactivedate) >= 0)) {
							//compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
							alreadyNo+=Integer.parseInt(copys);
						}
					}
				}else{
					insureSql = "select cardno,Cvalidate,InActiveDate from LICardActiveInfoList where cardtype='"//--------fromYZF
                        +mRiskWrapCode+"' and name='"+ Name + "' and sex='"+ Sex + "' and birthday='"+ Birthday
						+ "' and idtype='"+ IDType + "' and idno='"	+ IDNo	+ "' and InActiveDate>'"+ PubFun.getCurrentDate()+ "' "
				        +"and (CardStatus = '01' or CardStatus is null)";
					System.out.println(insureSql);
					SSRS insureSSRS = tExeSQL.execSQL(insureSql);
					FDate fdate = new FDate();
					LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
					for (int j = 1; j <= insureSSRS.MaxRow; j++) {
						Date oldCvalidate = fdate.getDate(insureSSRS.GetText(j, 2)); //结果中的生效日期，用FDate处理
						Date oldInactivedate = fdate.getDate(insureSSRS.GetText(j,3)); //结果中失效日期，用FDate处理
						Date newCvalidate = fdate.getDate(mCValidate); //当前激活卡的生效日期，用FDate处理
						String[] inactivedate = tLoginVerifyTool.getMyProductInfo(mCardNo, mCValidate,String.valueOf(mMult));
						Date newInactivedate = fdate.getDate(inactivedate[2]); //当前激活卡的失效日期，用FDate处理		        
						if ((oldCvalidate.compareTo(newCvalidate) <= 0 && oldInactivedate.compareTo(newCvalidate) > 0)
								|| (oldCvalidate.compareTo(newInactivedate) < 0 && oldInactivedate.compareTo(newInactivedate) >= 0)) {
							//compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
							alreadyNo++;
						}
					}
				}
				String maxNoSql = null;
				//取最大份数
				maxNoSql = " select maxcopys from ldwrap where riskwrapcode='" + mRiskWrapCode + "' ";
				String temp2 = tExeSQL.execSQL(maxNoSql).GetText(1, 1);
				int maxNo = 0;
				if (temp2.matches("^\\d+$")) {
					maxNo = Integer.parseInt(temp2);
					if (maxNo < alreadyNo) {
						Error = "被保人投保份数已超过可投保的最大份数!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
				}
			} else {
				insureSql = "select count(1) from LICardActiveInfoList where cardtype='"//--------fromYZF
                        +mRiskWrapCode+"' and name='" + Name + "' and sex='" + Sex
						+ "' and birthday='" + Birthday	+ "' and idtype='"	+ IDType
						+ "' and idno='" + IDNo	+ "' ";
				String temp1 = tExeSQL.execSQL(insureSql).GetText(1, 1);
				if (temp1.matches("^\\d+$")) {
					alreadyNo += Integer.parseInt(temp1);
				}
				String maxNoSql = null;
				maxNoSql = " select maxcopys from ldwrap where riskwrapcode='"+ mRiskWrapCode + "' ";
				String temp2 = tExeSQL.execSQL(maxNoSql).GetText(1, 1);
				int maxNo = 0;
				if (temp2.matches("^\\d+$")) {
					maxNo = Integer.parseInt(temp2);
					if (maxNo < alreadyNo) {
						Error = "被保人投保份数已超过可投保的最大份数!";
						buildError("checkInsuredInfos",Error);
						return false;
					}
				}
			}
		}
		return true;
	}
	/**
     * 对产品的特殊规定进行校验。
     * @param insuListSet
     * @return
     */
	private boolean checkSpec(WFInsuListSet insuListSet){
		boolean flag = true;
		LMCheckFieldDB tLMCheckFieldDB= new LMCheckFieldDB();
		tLMCheckFieldDB.setRiskCode(mRiskWrapCode);
		tLMCheckFieldDB.setFieldName("ETBINSERT");
		LMCheckFieldSet tLMCheckFieldSet = tLMCheckFieldDB.query();
		for(int i=1;i<=tLMCheckFieldSet.size();i++){
			LMCheckFieldSchema tLMCheckFieldSchema = tLMCheckFieldSet.get(i);
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			tLMCalModeDB.setCalCode(tLMCheckFieldSchema.getCalCode());
			LMCalModeSet tLMCalModeSet = new LMCalModeSet();
			tLMCalModeSet = tLMCalModeDB.query();
			for(int j=1;j<=tLMCalModeSet.size();j++){
				LMCalModeSchema tLMCalModeSchema = tLMCalModeSet.get(j);
				for(int k=1;k<=insuListSet.size();k++){
					WFInsuListSchema insuListSchema = insuListSet.get(k);
					int tAppntAge = PubFun.calInterval(insuListSchema.getBirthday(),
							PubFun.getCurrentDate(), "Y");
					Calculator cal = new Calculator();
					cal.setCalCode(tLMCalModeSchema.getCalCode());
					cal.addBasicFactor("InsuredName", insuListSchema.getName());
					cal.addBasicFactor("Sex", insuListSchema.getSex());
					cal.addBasicFactor("InsuredBirthday", insuListSchema.getBirthday());
					cal.addBasicFactor("IDType", insuListSchema.getIDType());
					cal.addBasicFactor("IDNo", insuListSchema.getIDNo());
					cal.addBasicFactor("RiskWrapCode", mRiskWrapCode);
					cal.addBasicFactor("AppAge",tAppntAge+"");
					cal.addBasicFactor("Mult",mMult+"");
					cal.addBasicFactor("OccupationType",insuListSchema.getOccupationType());
					cal.addBasicFactor("Cvalidate",mCValidate);
					String result = cal.calculate();
					if(!result.equals("0")){
						flag = false;
						mUwErrorList.add(tLMCalModeSchema.getRemark());
					}
				}
			}
		}
		return flag;
	}
    /**
     * 处理单证关键要素（保额、保费、档次、份数等）。
     * @param contListSchema
     * @return
     */
    private boolean dealCertifyWrapParams()
    {

        String tCopysFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Copys' and lmcr.CertifyCode = '" + mCertifyCode + "' ");
        if (!"1".equals(tCopysFlag) && mCopys != 1)
        {
            String tStrErr = "单证号[" + mCardNo + "]对应套餐份数不能大于1。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);
            if (!mImportLog.errLog(mCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        // -------------------------------------------

        // 校验档次，但不对POS单证进行档次的对应校验
        String tMultFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + mCertifyCode + "' " + "union all "
                + " select 1 from LMCertifyDes lmcd where lmcd.CertifyCode = '" + mCertifyCode
                + "' and lmcd.OperateType = '3'");
        if ("1".equals(tMultFlag))
        {
        	String MultSql = "select calfactortype,calfactorvalue from LDRiskDutyWrap ldrdw inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
        		+"where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + mCertifyCode + "' ";
        	SSRS tSSRS = new ExeSQL().execSQL(MultSql);
        	if(tSSRS != null && tSSRS.MaxRow>0){
        		if("2".equals(tSSRS.GetText(1, 1)) && mMult == 0){//当需要传递档次，而档次为0时，则阻断。
                	String tStrErr = "单证号[" + mCardNo + "]对应套餐需含有档次要素。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    if (!mImportLog.errLog(mCardNo, tStrErr))
                    {
                        mErrors.copyAllErrors(mImportLog.mErrors);
                        return false;
                    }
                    return false;
        		}
        	}
        }else{
    		if(mMult != 0){//没有档次要素，当传递档次时，则阻断
            	String tStrErr = "单证号[" + mCardNo + "]对应套餐不应含有档次要素。";
                buildError("dealCertifyWrapParams", tStrErr);
                if (!mImportLog.errLog(mCardNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        // -------------------------------------------

        LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

        String tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
                + " where lmcr.CertifyCode = '"
                + mCertifyCode
                + "' "
                + " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
        tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
        {
            String tStrErr = "单证号[" + mCardNo + "]对应套餐要素信息未找到。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(mCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        // 处理要素信息。
        for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++)
        {
            LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);

            String tCalFactorType = tRiskDutyParam.getCalFactorType();
            String tCalFactor = tRiskDutyParam.getCalFactor();
            String tCalFactorValue = tRiskDutyParam.getCalFactorValue();

            if ("Prem".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(mCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(mMult));
                    tCal.addBasicFactor("OccupationType", String.valueOf(mOccupationType));
                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + mCardNo + "]保费计算失败，请核查算费参数。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        System.out.println(tStrErr);

                        if (!mImportLog.errLog(mCardNo, tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                  
                }

                try
                {
                    tSysWrapSumPrem = Arith.add(tSysWrapSumPrem, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保费出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }

            if ("Amnt".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(mCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(mMult));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + mCardNo + "]保额计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        System.out.println(tStrErr);

                        if (!mImportLog.errLog(mCardNo, tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                        // tTmpCalResult = "0";
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保额出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }
        }
        // ------------------------------------------------

        if (tPrem != 0 && tPrem != tSysWrapSumPrem)
        {
            String tStrErr = "单证号[" + mCardNo + "]系统计算出保费：" + tSysWrapSumPrem + "，与获取保费：" + tPrem + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(mCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        if (tAmnt != 0 && tAmnt != tSysWrapSumAmnt)
        {
            String tStrErr = "单证号[" + mCardNo + "]系统计算出保额：" + tSysWrapSumAmnt + "，与获取保额：" + tAmnt + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);
            if (!mImportLog.errLog(mCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            
            return false;
        }

        return true;
    }
    private MMap dealCertInfo()
    {
        MMap tMMap = new MMap();

        StringBuffer tErrInfos = new StringBuffer();

        String tCardNo = mWFContInfo.getCardNo();
        String tPassword = mWFContInfo.getPassword();
        String tCertifyCode = mWFContInfo.getCertifyCode();
        String tWrapCode = mWFContInfo.getRiskCode();

        if (!CertInfoManager.verifyCertInfo(tErrInfos, tCardNo, tPassword, tCertifyCode, tWrapCode))
        {
            buildError("dealCertInfo", tErrInfos.toString());
            return null;
        }

        String tStrSql = " select lzc.* "
                + " from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where 1 = 1 " + " and lzcn.CardNo = '" + tCardNo + "' ";
        LZCardSet tCertInfos = new LZCardDB().executeQuery(tStrSql);
        if (tCertInfos == null || tCertInfos.size() == 0)
        {
            buildError("dealCertInfo", "单证信息未找到。");
            return null;
        }
        else if (tCertInfos.size() != 1)
        {
            buildError("dealCertInfo", "单证信息出现多条。");
            return null;
        }

        LZCardSchema tCertCardInfo = tCertInfos.get(1);
        //modify by zhangyige 2012-7-2
        //state 为2-正常回销 13-已导入不能再修改单证状态了
        String state = tCertCardInfo.getState();
        if(!state.equals("2")&&!state.equals("13")){
        	tCertCardInfo.setState("14");
        }
        tCertCardInfo.setActiveFlag("1");

        tMMap.put(tCertCardInfo, SysConst.UPDATE);

        return tMMap;
    }

    private boolean dealWFAppntInfo()
    {
        mWFAppntInfo.setBatchNo(mBatchNo);

        // 如未填写姓名、性别、出生日期，则均视为“ ”来代替
        if (mWFAppntInfo.getName() == null || mWFAppntInfo.getName().equals(""))
        {
            mWFAppntInfo.setName(" ");
        }

        if (mWFAppntInfo.getSex() == null || mWFAppntInfo.getSex().equals(""))
        {
            mWFAppntInfo.setSex(" ");
        }

        if (mWFAppntInfo.getBirthday() == null || mWFAppntInfo.getBirthday().equals(""))
        {
            mWFAppntInfo.setBirthday("1900-01-01");
        }

        if (mWFAppntInfo.getIDType() == null || mWFAppntInfo.getIDType().equals(""))
        {
            mWFAppntInfo.setSex(" ");
        }

        if (mWFAppntInfo.getIDNo() == null || mWFAppntInfo.getIDNo().equals(""))
        {
            mWFAppntInfo.setBirthday(" ");
        }
        // --------------------

        return true;
    }

    private boolean dealWFWrapExInfo()
    {
        for (int i = 1; i <= mWFWrapExListSet.size(); i++)
        {
        	WFWrapExListSchema tWFWrapExListSchema = mWFWrapExListSet.get(i);

            // 批次号不能为空
            if (tWFWrapExListSchema.getBatchNo() == null || tWFWrapExListSchema.getBatchNo().equals(""))
            {
                buildError("dealWFWrapExInfo", "批次号不能为空。");
                return false;
            }
            
            // 卡号不能为空
            if (tWFWrapExListSchema.getCardNo() == null || tWFWrapExListSchema.getCardNo().equals(""))
            {
            	buildError("dealWFWrapExInfo", "卡号不能为空。");
            	return false;
            }
            
            // 保险卡类型不能为空
            if (tWFWrapExListSchema.getCertifyCode() == null || tWFWrapExListSchema.getCertifyCode().equals(""))
            {
            	buildError("dealWFWrapExInfo", "保险卡类型不能为空。");
            	return false;
            }
            
            // 扩展要素代码不能为空
            if (tWFWrapExListSchema.getExFactorCode() == null || tWFWrapExListSchema.getExFactorCode().equals(""))
            {
            	buildError("dealWFWrapExInfo", "扩展要素代码不能为空。");
            	return false;
            }
            
            // 扩展要素名称不能为空
            if (tWFWrapExListSchema.getExFactorName() == null || tWFWrapExListSchema.getExFactorName().equals(""))
            {
            	buildError("dealWFWrapExInfo", "扩展要素名称不能为空。");
            	return false;
            }
            
            // 操作人不能为空
            if (tWFWrapExListSchema.getOperator() == null || tWFWrapExListSchema.getOperator().equals(""))
            {
            	buildError("dealWFWrapExInfo", "操作人不能为空。");
            	return false;
            }
            String exSql = "select count(*) from WFWrapExList where cardNo ='"+tWFWrapExListSchema.getCardNo()
            +"' and ExFactorCode = '"+tWFWrapExListSchema.getExFactorCode()+"'";
			ExeSQL tExeSQL = new ExeSQL();
			int exCount = Integer.parseInt(tExeSQL.getOneValue(exSql));
			if(exCount > 0){
				buildError("dealWFWrapExInfo", "该扩展要素已在系统中存在！");
            	return false;
			}
            
            // --------------------
        }
        ArrayList array = new ArrayList();
        int length = mLIWrapExListSet.size();
        for(int i = 1;i <= length;i++){
        	String code = mLIWrapExListSet.get(i).getExFactorCode();
        	if(array.contains(code)){
				buildError("dealLIWrapExInfo", "该扩展要素已在系统中存在！");
            	return false;
        	}else{
        		array.add(code);
        	}
        }
        return true;
    }

    private boolean dealLIWrapExInfo()
    {
        for (int i = 1; i <= mLIWrapExListSet.size(); i++)
        {
        	LIWrapExListSchema tLIWrapExListSchema = mLIWrapExListSet.get(i);

            // 批次号不能为空
            if (tLIWrapExListSchema.getBatchNo() == null || tLIWrapExListSchema.getBatchNo().equals(""))
            {
                buildError("dealLIWrapExInfo", "批次号不能为空。");
                return false;
            }
            
            // 卡号不能为空
            if (tLIWrapExListSchema.getCardNo() == null || tLIWrapExListSchema.getCardNo().equals(""))
            {
            	buildError("dealLIWrapExInfo", "卡号不能为空。");
            	return false;
            }
            
            // 保险卡类型不能为空
            if (tLIWrapExListSchema.getCertifyCode() == null || tLIWrapExListSchema.getCertifyCode().equals(""))
            {
            	buildError("dealLIWrapExInfo", "保险卡类型不能为空。");
            	return false;
            }
            
            // 扩展要素代码不能为空
            if (tLIWrapExListSchema.getExFactorCode() == null || tLIWrapExListSchema.getExFactorCode().equals(""))
            {
            	buildError("dealLIWrapExInfo", "扩展要素代码不能为空。");
            	return false;
            }
            
            // 扩展要素名称不能为空
            if (tLIWrapExListSchema.getExFactorName() == null || tLIWrapExListSchema.getExFactorName().equals(""))
            {
            	buildError("dealLIWrapExInfo", "扩展要素名称不能为空。");
            	return false;
            }
            
            // 操作人不能为空
            if (tLIWrapExListSchema.getOperator() == null || tLIWrapExListSchema.getOperator().equals(""))
            {
            	buildError("dealLIWrapExInfo", "操作人不能为空。");
            	return false;
            }
            String exSql = "select count(*) from LIWrapExList where cardNo ='"+tLIWrapExListSchema.getCardNo()
            +"' and ExFactorCode = '"+tLIWrapExListSchema.getExFactorCode()+"'";
			ExeSQL tExeSQL = new ExeSQL();
			int exCount = Integer.parseInt(tExeSQL.getOneValue(exSql));
			if(exCount > 0){
				buildError("dealLIWrapExInfo", "该扩展要素已在系统中存在！");
            	return false;
			}
            
            // --------------------
        }
        ArrayList array = new ArrayList();
        int length = mLIWrapExListSet.size();
        for(int i = 1;i <= length;i++){
        	String code = mLIWrapExListSet.get(i).getExFactorCode();
        	if(array.contains(code)){
				buildError("dealLIWrapExInfo", "该扩展要素已在系统中存在！");
            	return false;
        	}else{
        		array.add(code);
        	}
        }
        return true;
    }
    
    private boolean dealWFInsuInfo()
    {
        for (int i = 1; i <= mWFInsuInfos.size(); i++)
        {
            WFInsuListSchema tWFInsuInfo = mWFInsuInfos.get(i);

            tWFInsuInfo.setBatchNo(mBatchNo);

            if ("00".equals(tWFInsuInfo.getRelationToInsured()))
            {
                tWFInsuInfo.setRelationCode("00"); // 数据描述为该被保人是否连带，放到业务逻辑中进行处理
                // tWFInsuInfo.setReInsuNo(null); // 数据描述为该被保人主被保人编号，放到业务逻辑中进行处理
            }
            else
            {
                tWFInsuInfo.setRelationCode("01");
            }

            // 如未填写姓名、性别、出生日期，则均视为“ ”来代替
            if (tWFInsuInfo.getName() == null || tWFInsuInfo.getName().equals(""))
            {
                tWFInsuInfo.setName(" ");
            }

            if (tWFInsuInfo.getSex() == null || tWFInsuInfo.getSex().equals(""))
            {
                tWFInsuInfo.setSex(" ");
            }

            if (tWFInsuInfo.getBirthday() == null || tWFInsuInfo.getBirthday().equals(""))
            {
                tWFInsuInfo.setBirthday("1900-01-01");
            }

            if (mWFAppntInfo.getIDType() == null || mWFAppntInfo.getIDType().equals(""))
            {
                mWFAppntInfo.setSex(" ");
            }

            if (mWFAppntInfo.getIDNo() == null || mWFAppntInfo.getIDNo().equals(""))
            {
                mWFAppntInfo.setBirthday(" ");
            }
            // --------------------
        }

        return true;
    }

    private boolean dealWFBnfInfo()
    {
        return true;
    }
    
    private boolean dealWFContInfo()
    {
        String tStrSql = null;
        String tStrResult = null;
        ExeSQL tExeSql = new ExeSQL();

        // 校验批次号唯一
        tStrSql = "select 1 from WFContList where BatchNo = '" + mBatchNo + "'";
        tStrResult = tExeSql.getOneValue(tStrSql);
        if ("1".equals(tStrResult))
        {
            buildError("dealWFContInfo", "批次信息已存在。");
            return false;
        }
        tStrSql = null;
        tStrResult = null;
        // --------------------

        // 校验卡号存在
        String tCardNo = mWFContInfo.getCardNo();

        tStrSql = "select 1 from WFContList where CardNo = '" + tCardNo + "'";
        tStrResult = tExeSql.getOneValue(tStrSql);
        if ("1".equals(tStrResult))
        {
            buildError("dealWFContInfo", "[" + tCardNo + "]已使用。");
            return false;
        }
        tStrSql = null;
        tStrResult = null;
        // --------------------

        mWFContInfo.setBatchNo(mBatchNo);
        mWFContInfo.setSendDate(mSendDate);
        mWFContInfo.setSendTime(mSendTime);
        mWFContInfo.setBranchCode(mBranchCode);
        mWFContInfo.setSendOperator(mSendOperator);
        mWFContInfo.setMessageType(mMsgType);

        mWFContInfo.setCardDealType("01"); // 代表承保

        if (!dealCertifyWrapParams(mWFContInfo))
        {
            return false;
        }

        if (mWFContInfo.getCvaliDate() == null || mWFContInfo.getCvaliDate().equals(""))
        {
            buildError("dealWFContInfo", "[" + tCardNo + "]生效日期不得为空。");
            return false;
        }

        return true;
    }

    private boolean dealActCardInfo()
    {
        mActCardInfos = new LICardActiveInfoListSet();

        for (int i = 1; i <= mWFInsuInfos.size(); i++)
        {
            WFInsuListSchema tWFInsuInfo = mWFInsuInfos.get(i);

            LICardActiveInfoListSchema tActCardInfo = new LICardActiveInfoListSchema();

            tActCardInfo.setBatchNo(mWFContInfo.getBatchNo());

            tActCardInfo.setSequenceNo(String.valueOf(i));
            tActCardInfo.setCardNo(mWFContInfo.getCardNo());

            tActCardInfo.setCardType(mWFContInfo.getCertifyCode());
            tActCardInfo.setCValidate(mWFContInfo.getCvaliDate());

            tActCardInfo.setActiveDate(mWFContInfo.getActiveDate());
            tActCardInfo.setActiveIP(null);

            tActCardInfo.setDealFlag("00");
            //tActCardInfo.setDealDate(null);

            tActCardInfo.setName(tWFInsuInfo.getName());
            tActCardInfo.setSex(tWFInsuInfo.getSex());
            tActCardInfo.setBirthday(tWFInsuInfo.getBirthday());
            tActCardInfo.setIdType(tWFInsuInfo.getIDType());
            tActCardInfo.setIdNo(tWFInsuInfo.getIDNo());

            tActCardInfo.setOccupationType(tWFInsuInfo.getOccupationType());
            tActCardInfo.setOccupationCode(tWFInsuInfo.getOccupationCode());

            tActCardInfo.setPostalAddress(tWFInsuInfo.getPostalAddress());
            tActCardInfo.setZipCode(tWFInsuInfo.getZipCode());
            tActCardInfo.setPhone(tWFInsuInfo.getPhont());
            tActCardInfo.setMobile(tWFInsuInfo.getMobile());

            tActCardInfo.setGrpName(tWFInsuInfo.getGrpName());
            tActCardInfo.setCompanyPhone(tWFInsuInfo.getCompanyPhone());
            tActCardInfo.setEMail(tWFInsuInfo.getEmail());

            tActCardInfo.setState("00");

            tActCardInfo.setOperator(mGlobalInput.Operator);
            tActCardInfo.setMakeDate(PubFun.getCurrentDate());
            tActCardInfo.setMakeTime(PubFun.getCurrentTime());
            tActCardInfo.setModifyDate(PubFun.getCurrentDate());
            tActCardInfo.setModifyTime(PubFun.getCurrentTime());

            tActCardInfo.setPrem(String.valueOf(mWFContInfo.getPrem()));
            tActCardInfo.setAmnt(String.valueOf(mWFContInfo.getAmnt()));
            tActCardInfo.setMult(String.valueOf(mWFContInfo.getMult()));
            tActCardInfo.setCopys(String.valueOf(mWFContInfo.getCopys()));

            tActCardInfo.setTicketNo(mWFContInfo.getTicketNo());
            tActCardInfo.setTeamNo(mWFContInfo.getTeamNo());
            tActCardInfo.setSeatNo(mWFContInfo.getSeatNo());
            tActCardInfo.setFrom(mWFContInfo.getFrom());
            tActCardInfo.setTo(mWFContInfo.getTo());

            tActCardInfo.setRemark(mWFContInfo.getRemark());

            tActCardInfo.setCardStatus("01");
            LoginVerifyTool tool = new LoginVerifyTool();
            String[] riskRelateCode = tool.getRiskSetCodeByCardNo(mWFContInfo.getCardNo());// 包含套餐编码及certifyCode
    		String riskSetCode = riskRelateCode[0];
    		String[] insuYearInfoArr = tool.getInsuInfoFromCore(mWFContInfo.getCardNo(), riskSetCode,String.valueOf(mWFContInfo.getMult()));
            tActCardInfo.setInsuYear(insuYearInfoArr[0]);
            tActCardInfo.setInsuYearFlag(insuYearInfoArr[1]);
            String tInActiveDate = mWFContInfo.getBak3();
            String[] aInActiveDate= tool.getMyProductInfo(mWFContInfo.getCardNo(), mWFContInfo.getCvaliDate(),String.valueOf(mWFContInfo.getMult()));
			if(aInActiveDate[2] != null||aInActiveDate[2] != ""){
				tActCardInfo.setInActiveDate(aInActiveDate[2]);
			}else{
				tActCardInfo.setInActiveDate(tInActiveDate);
			}
            mActCardInfos.add(tActCardInfo);
        }

        return true;
    }

    private void dealWFTransLog(boolean cSucFlag)
    {
        MMap tMMap = new MMap();

        WFTransLogSchema tWFTransLog = new WFTransLogSchema();

        tWFTransLog.setBatchNo(mWFContInfo.getBatchNo());
        tWFTransLog.setMessageType(mWFContInfo.getMessageType());
        tWFTransLog.setSendDate(mWFContInfo.getSendDate());
        tWFTransLog.setSendTime(mWFContInfo.getSendTime());

        if (cSucFlag)
        {
            tWFTransLog.setBatchFlag("00");

            tWFTransLog.setErrCode("0");
            tWFTransLog.setErrInfo("交易成功");
        }
        else
        {
            tWFTransLog.setBatchFlag("01");

            tWFTransLog.setErrCode("99");
            tWFTransLog.setErrInfo(mErrors != null ? mErrors.getFirstError() : "未知异常。");
        }

        tWFTransLog.setopertator(mWFContInfo.getSendOperator());

        tMMap.put(tWFTransLog, SysConst.DELETE_AND_INSERT);

        VData data = new VData();
        data.add(tMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return;
        }
    }

    private boolean prepareResult()
    {
        mResult.clear();

        if (mActCardInfos != null)
            mResult.add(mActCardInfos);

        if (mWFContInfo != null)
            mResult.add(mWFContInfo);

        if (mWFAppntInfo != null)
            mResult.add(mWFAppntInfo);

        if (mWFInsuInfos != null)
            mResult.add(mWFInsuInfos);

        if (mWFBnfInfos != null)
            mResult.add(mWFBnfInfos);
        
        if (mWFWrapExListSet != null)
        	mResult.add(mWFWrapExListSet);
        
        if (mLIWrapExListSet != null)
        	mResult.add(mLIWrapExListSet);

        return true;
    }

    /**
     * 处理单证关键要素（保额、保费、档次、份数等）。
     * @param cLICertifySchema
     * @return
     */
    private boolean dealCertifyWrapParams(WFContListSchema cWFContInfo)
    {
        String tStrSql = null;

        String tCardNo = cWFContInfo.getCardNo();

        String tCertifyCode = cWFContInfo.getCertifyCode();
        double tAmnt = cWFContInfo.getAmnt();
        double tPrem = cWFContInfo.getPrem();
        double tCopys = cWFContInfo.getCopys();
        int tMult = cWFContInfo.getMult();
        mOccupationType = cWFContInfo.getBak10();
        String tCardType = null;
        // 获取单证类型
        tStrSql = "select OperateType from LMCertifyDes where CertifyCode = '" + tCertifyCode + "'";
        tCardType = new ExeSQL().getOneValue(tStrSql);
        if (tCardType == null || tCardType.equals(""))
        {
            String tStrErr = "[" + tCardNo + "]对应单证中，单证业务类别为空。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        tStrSql = null;
        // --------------------

        // 处理Copys要素。如果套餐中无Copys要素，清单中份数要素必须为1。
        //        if (tCopys == 0)
        //        {
        //            tCopys = 1;
        //            // cWFContInfo.setCopys(String.valueOf(tCopys));
        //        }

        String tCopysFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Copys' and lmcr.CertifyCode = '" + tCertifyCode + "' ");
        if (!"1".equals(tCopysFlag) && tCopys > 1)
        {
            String tStrErr = "[" + tCardNo + "]对应套餐不含有份数要素。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        else if ("1".equals(tCopysFlag) && tCopys == 0)
        {
            String tStrErr = "[" + tCardNo + "]对应套餐份数不能为空或0。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        // -------------------------------------------

        // 校验档次
        String tMultFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + tCertifyCode + "' ");
        if (!"1".equals(tMultFlag) && tMult != 0)
        {
            String tStrErr = "[" + tCardNo + "]对应套餐不含有档次要素。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        // -------------------------------------------

        LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

        tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
                + " where lmcr.CertifyCode = '"
                + tCertifyCode
                + "' "
                + " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
        tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
        {
            String tStrErr = "[" + tCardNo + "]对应套餐要素信息未找到。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        tStrSql = null;

        double tSysWrapSumPrem = 0;
        double tSysWrapSumAmnt = 0;

        // 处理要素信息。
        for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++)
        {
            LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);
            System.out.println("处理的险种是========"+tRiskDutyParam.getRiskCode());
            String tCalFactorType = tRiskDutyParam.getCalFactorType();
            String tCalFactor = tRiskDutyParam.getCalFactor();
            String tCalFactorValue = tRiskDutyParam.getCalFactorValue();
//            if("OccupationType".equals(tCalFactor)){
//            	mOccupationType = tCalFactorValue;
//            }

            System.out.println("算费要素tCalFactor====="+tCalFactor);
            if ("Prem".equals(tCalFactor))
            {
                String tTmpCalResult = null;
                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(tMult));
                    tCal.addBasicFactor("OccupationType", String.valueOf(mOccupationType));
                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "[" + tCardNo + "]保费计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        return false;
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumPrem = Arith.add(tSysWrapSumPrem, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保费出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }

            if ("Amnt".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(tMult));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + tCardNo + "]保额计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        return false;
                        // tTmpCalResult = "0";
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保额出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }
        }
        // ------------------------------------------------
        //驴妈妈家庭单算费，需转换成一个人的保额、保费
        if(this.familyFlag){
        	int peoples = this.mWFInsuInfos.size();
        	tPrem = Arith.div(tPrem, peoples);
        	tAmnt = Arith.div(tAmnt, peoples);
        	
        }
        if (tPrem != 0 && tPrem != tSysWrapSumPrem)
        {
            String tStrErr = "[" + tCardNo + "]系统计算出保费：" + tSysWrapSumPrem + "，与填写保费：" + tPrem + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        cWFContInfo.setPrem(tSysWrapSumPrem);

        if (tAmnt != 0 && tAmnt != tSysWrapSumAmnt)
        {
            String tStrErr = "[" + tCardNo + "]系统计算出保额：" + tSysWrapSumAmnt + "，与填写保额：" + tAmnt + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        cWFContInfo.setAmnt(tSysWrapSumAmnt);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ":" + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
	/**
     * 得到卡单激活未通过保原因
     * @return String
     * @date 20101122
     */
    public String getUWErrors()
    {
        String uwErrors = "";
        Set set = new HashSet();
        set.addAll(mUwErrorList);
        ArrayList mUwErrorList1 = new ArrayList();
        mUwErrorList1.addAll(set);
        for (int i = 0; i < mUwErrorList1.size(); i++)
        {
        	uwErrors += (String) mUwErrorList1.get(i) ;
            if(uwErrors.length()>0 && i<(mUwErrorList1.size()-1)) uwErrors+= ";";
        }
        return uwErrors;
    }
    public static void main(String[] args) {
    	ArrayList array = new ArrayList();
    	String a[] = new String[3];
    	a[0] = "abc";
    	a[1] = "ab";
    	a[2] = "abcd";
    	int length = a.length;
    	for(int i= 0;i<length;i++){
    		if(array.contains(a[i])){
    			System.out.println("有重复！"+i+"  "+a[i]);
    		}else{
    			array.add(a[i]);
    		}
    	}
    	
	}
}
