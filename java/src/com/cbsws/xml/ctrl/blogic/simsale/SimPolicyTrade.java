/**
 * 2011-9-20
 */
package com.cbsws.xml.ctrl.blogic.simsale;

import java.util.Date;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.SimAppntInfo;
import com.cbsws.obj.SimBnfInfo;
import com.cbsws.obj.SimInsuInfo;
import com.cbsws.obj.SimPolicyInfo;
import com.cbsws.obj.WrapExInfo;
import com.cbsws.obj.WrapInfo;
import com.ecwebservice.subinterface.LoginVerifyTool;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LIWrapExListSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.schema.WFWrapExListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LIWrapExListSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.lis.vschema.WFWrapExListSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 * 
 * 简易保单试算处理通用接口
 *
 */
public class SimPolicyTrade extends ABusLogic
{
    private MsgHead mMsgHead = null;

    private SimPolicyInfo mMsgSimPolicyInfo = null;

    private SimAppntInfo mMsgSimAppntInfo = null;

    private SimInsuInfo[] mMsgSimInsuInfoList = null;

    private SimBnfInfo[] mMsgSimBnfInfoList = null;

    private WrapInfo mMsgWrapInfo = null;

    private WrapExInfo[] mMsgWrapExInfoList = null;

    protected boolean deal(MsgCollection cMsgInfos)
    {
        try
        {
            // 加载报文中相关数据
            if (!parseDatas(cMsgInfos))
            {
                return false;
            }

            // 可用单证信息核对
            if (!chkCertInfo())
            {
                return false;
            }
            // --------------------

            // 处理业务数据
            if (!tradeService())
            {
                return false;
            }
            // --------------------
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            errLog("交易过程出现异常，稍候请尝试重新交易");
            return false;
        }

        return true;
    }

    private boolean parseDatas(MsgCollection cMsgInfos)
    {
        // 报文头 
        mMsgHead = cMsgInfos.getMsgHead();
        if (mMsgHead == null)
        {
            errLog("报文头信息缺失。");
            return false;
        }
        // --------------------

        // 保单信息
        try
        {
            List tSimPolicyInfoList = cMsgInfos.getBodyByFlag("SimPolicyInfo");
            if (tSimPolicyInfoList == null || tSimPolicyInfoList.size() == 0)
            {
                errLog("保单信息缺失。");
                return false;
            }
            else if (tSimPolicyInfoList.size() != 1)
            {
                errLog("保单信息出现多条。");
                return false;
            }
            mMsgSimPolicyInfo = (SimPolicyInfo) tSimPolicyInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 保单信息");
            return false;
        }
        // --------------------

        // 投保人信息
        try
        {
            List tSimAppntInfoList = cMsgInfos.getBodyByFlag("SimAppntInfo");
            if (tSimAppntInfoList == null || tSimAppntInfoList.size() == 0)
            {
                errLog("投保人信息缺失。");
                return false;
            }
            else if (tSimAppntInfoList.size() != 1)
            {
                errLog("投保人信息出现多条。");
                return false;
            }
            mMsgSimAppntInfo = (SimAppntInfo) tSimAppntInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 投保人信息");
            return false;
        }
        // --------------------

        // 被保人信息
        try
        {
            List tSimInsuInfoList = cMsgInfos.getBodyByFlag("SimInsuInfo");
            if (tSimInsuInfoList == null || tSimInsuInfoList.size() == 0)
            {
                errLog("投保人信息缺失。");
                return false;
            }
            mMsgSimInsuInfoList = new SimInsuInfo[tSimInsuInfoList.size()];
            for (int i = 0; i < tSimInsuInfoList.size(); i++)
            {
                SimInsuInfo tSimInsuInfo = null;
                tSimInsuInfo = (SimInsuInfo) tSimInsuInfoList.get(i);
                mMsgSimInsuInfoList[i] = tSimInsuInfo;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 被保人信息");
            return false;
        }
        // --------------------

        // 受益人信息
        try
        {
            List tSimBnfInfoList = cMsgInfos.getBodyByFlag("SimBnfInfo");
            if (tSimBnfInfoList != null)
            {
                mMsgSimBnfInfoList = new SimBnfInfo[tSimBnfInfoList.size()];
                for (int i = 0; i < tSimBnfInfoList.size(); i++)
                {
                    SimBnfInfo tSimBnfInfo = null;
                    tSimBnfInfo = (SimBnfInfo) tSimBnfInfoList.get(i);
                    mMsgSimBnfInfoList[i] = tSimBnfInfo;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 受益人信息");
            return false;
        }
        // --------------------

        // 产品信息
        try
        {
            List tWrapInfoList = cMsgInfos.getBodyByFlag("WrapInfo");
            if (tWrapInfoList == null || tWrapInfoList.size() == 0)
            {
                errLog("产品信息缺失。");
                return false;
            }
            else if (tWrapInfoList.size() != 1)
            {
                errLog("产品信息出现多条。");
                return false;
            }
            mMsgWrapInfo = (WrapInfo) tWrapInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 产品信息");
            return false;
        }
        // --------------------

        // 产品扩展信息
        try
        {
            List tWrapExInfoList = cMsgInfos.getBodyByFlag("WrapExInfo");
            if (tWrapExInfoList != null)
            {
                mMsgWrapExInfoList = new WrapExInfo[tWrapExInfoList.size()];
                for (int i = 0; i < tWrapExInfoList.size(); i++)
                {
                    WrapExInfo tWrapExInfo = null;
                    tWrapExInfo = (WrapExInfo) tWrapExInfoList.get(i);
                    mMsgWrapExInfoList[i] = tWrapExInfo;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 产品扩展信息");
            return false;
        }
        // --------------------

        return true;
    }
    
    private boolean chkCertInfo()
    {
        StringBuffer tErrInfos = new StringBuffer();

        String tCardNo = mMsgWrapInfo.getCardNo();
        String tPassword = mMsgWrapInfo.getPassword();
        String tCertifyCode = mMsgWrapInfo.getCertifyCode();
        String tWrapCode = mMsgWrapInfo.getWrapCode();

        if (!CertInfoManager.verifyCertInfo(tErrInfos, tCardNo, tPassword, tCertifyCode, tWrapCode))
        {
        	if(mMsgHead.getMsgType().equals("AI0006")){
        		errLog1(tErrInfos.toString());
        	}else{
        		//modify by zhangyige 2014-01-07 湖南乘意险 已经激活或售出 返回成功
        		if(mMsgHead.getMsgType().equals("AI0004")&&tErrInfos.toString().contains("已经激活或售出")){
        			errLog2(tErrInfos.toString());
        		}else{
        			errLog(tErrInfos.toString());
        		}
        	}
            return false;
        }

        return true;
    }

    private boolean tradeService()
    {
        // 封装保单信息 
        WFContListSchema tWFContInfo = getContInfo();
        if (tWFContInfo == null)
        {
            return false;
        }
        // --------------------

        // 封装投保人信息
        WFAppntListSchema tWFAppntInfo = getAppntInfo();
        if (tWFAppntInfo == null)
        {
            return false;
        }
        // --------------------

        // 封装被保人信息
        WFInsuListSet tWFInsuInfos = getInsuInfos();
        if (tWFInsuInfos == null || tWFInsuInfos.size() == 0)
        {
            return false;
        }
        // --------------------

        // 封装受益人信息
        WFBnfListSet tWFBnfInfos = getBnfInfos();
        if (tWFBnfInfos == null)
        {
            return false;
        }
        // 封装扩展信息(报文备份）
        WFWrapExListSet tWFWrapExInfos = getWrapExInfos();
        if (tWFWrapExInfos == null)
        {
        	return false;
        }
        //备份业务表
        int length = tWFWrapExInfos.size();
        LIWrapExListSet tLIWrapExListSet = new LIWrapExListSet();
    	for(int i=1;i<length+1;i++){
    		LIWrapExListSchema tLIWrapExListSchema = new LIWrapExListSchema();
    		WFWrapExListSchema tWFWrapExListSchema = new WFWrapExListSchema();
    		tWFWrapExListSchema.setSchema(tWFWrapExInfos.get(i).getSchema());
    		Reflections ref = new Reflections();
    		ref.transFields(tLIWrapExListSchema, tWFWrapExListSchema);  			
    		tLIWrapExListSet.add(tLIWrapExListSchema);
    	}
        // --------------------

        // 进行投保处理（试算-不提交数据库）
        String mBatchNo = mMsgHead.getBatchNo();
        String mSendDate = mMsgHead.getSendDate();
        String mSendTime = mMsgHead.getSendTime();
        String mBranchCode = mMsgHead.getBranchCode();
        String mSendOperator = mMsgHead.getSendOperator();
        String mMsgType = mMsgHead.getMsgType();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", mBatchNo);
        tTransferData.setNameAndValue("SendDate", mSendDate);
        tTransferData.setNameAndValue("SendTime", mSendTime);
        tTransferData.setNameAndValue("BranchCode", mBranchCode);
        tTransferData.setNameAndValue("SendOperator", mSendOperator);
        tTransferData.setNameAndValue("MsgType", mMsgType);

        VData tVData = new VData();
        tVData.add(tWFContInfo);
        tVData.add(tWFAppntInfo);
        tVData.add(tWFInsuInfos);
        tVData.add(tWFBnfInfos);
        tVData.add(tWFWrapExInfos);
        tVData.add(tLIWrapExListSet);
        
        tVData.add(tTransferData);

        GlobalInput tGi = new GlobalInput();
        tGi.ManageCom = "86";
        tGi.ComCode = "86";
        tGi.Operator = "001";
        tVData.add(tGi);

        SimPolicyBL tSimPolicyBL = new SimPolicyBL();
        if (!tSimPolicyBL.submitData(tVData, "SimPolicyTrade"))
        {
            String tErrInfo = tSimPolicyBL.mErrors.getFirstError();
            errLog(tErrInfo);
            return false;
        }
        // --------------------

        // 交易结果查询
        VData tResultDatas = tSimPolicyBL.getResult();
        if (!qryTradeResult(tResultDatas))
        {
            return false;
        }
        // --------------------

        return true;
    }

    private WFContListSchema getContInfo()
    {
        WFContListSchema tWFContInfo = new WFContListSchema();

        // 基本交易数据
        tWFContInfo.setBatchNo(mMsgHead.getBatchNo());
        tWFContInfo.setSendDate(mMsgHead.getSendDate());
        tWFContInfo.setSendTime(mMsgHead.getSendTime());
        tWFContInfo.setBranchCode(mMsgHead.getBranchCode());
        tWFContInfo.setSendOperator(mMsgHead.getSendOperator());
        tWFContInfo.setMessageType(mMsgHead.getMsgType());
        // --------------------

        // tWFContInfo.setCardDealType("01");

        // 产品数据
        tWFContInfo.setCardNo(mMsgWrapInfo.getCardNo());
        tWFContInfo.setPassword(mMsgWrapInfo.getPassword());
        tWFContInfo.setCertifyCode(mMsgWrapInfo.getCertifyCode());
        tWFContInfo.setCertifyName(mMsgWrapInfo.getCertifyName());
        tWFContInfo.setRiskCode(mMsgWrapInfo.getWrapCode());

        if (mMsgWrapInfo.getPrem() == null || mMsgWrapInfo.getPrem().equals(""))
        {
            String tErrInfo = "承保交易中，承保保费信息不能为空。";
            errLog(tErrInfo);
            return null;
        }
        else
        {
            try
            {
                double tTmpPrem = Double.valueOf(mMsgWrapInfo.getPrem()).doubleValue();
                if (tTmpPrem == 0)
                {
                    String tErrInfo = "承保交易中，承保保费信息不能为0。";
                    errLog(tErrInfo);
                    return null;
                }
            }
            catch (Exception e)
            {
                String tErrInfo = "保费信息为非数值型数据。";
                errLog(tErrInfo);
                return null;
            }
        }

        tWFContInfo.setPrem(mMsgWrapInfo.getPrem());
        tWFContInfo.setAmnt(mMsgWrapInfo.getAmnt());
        tWFContInfo.setMult(mMsgWrapInfo.getMult());
        tWFContInfo.setCopys(mMsgWrapInfo.getCopys());
        // --------------------

        tWFContInfo.setActiveDate(mMsgWrapInfo.getActiveDate());

        // 投保单数据
        tWFContInfo.setManageCom(mMsgSimPolicyInfo.getManageCom());
        tWFContInfo.setSaleChnl(mMsgSimPolicyInfo.getSaleChnl());
        tWFContInfo.setAgentCom(mMsgSimPolicyInfo.getAgentCom());
        tWFContInfo.setAgentCode(mMsgSimPolicyInfo.getAgentCode());









        tWFContInfo.setAgentName(mMsgSimPolicyInfo.getAgentName());
        tWFContInfo.setPayMode(mMsgSimPolicyInfo.getPayMode());
        tWFContInfo.setPayIntv(mMsgSimPolicyInfo.getPayIntv());
        tWFContInfo.setPayYear(mMsgSimPolicyInfo.getPayYear());
        tWFContInfo.setPayYearFlag(mMsgSimPolicyInfo.getPayYearFlag());

        String tCValiDate = mMsgSimPolicyInfo.getCValiDate();
        if (tCValiDate == null || tCValiDate.equals(""))
        {
            String tErrInfo = "保单生效日期不能为空。";
            errLog(tErrInfo);
            return null;
        }
        tWFContInfo.setCvaliDate(mMsgSimPolicyInfo.getCValiDate());

        tWFContInfo.setCvaliDateTime(mMsgSimPolicyInfo.getCValiTime());
        tWFContInfo.setInsuYear(mMsgSimPolicyInfo.getInsuYear());
        tWFContInfo.setInsuYearFlag(mMsgSimPolicyInfo.getInsuYearFlag());

        tWFContInfo.setRemark(mMsgSimPolicyInfo.getRemark());
        // --------------------

        // 投保人关联数据 - 暂定用卡号
        tWFContInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        //将报文中印刷号保存起来
        if(!mMsgSimPolicyInfo.getPrtNo().equals(mMsgWrapInfo.getCardNo())){
        	tWFContInfo.setBak2(mMsgSimPolicyInfo.getPrtNo()); 
        }
        // --------------------

        // 特殊字段-客票号信息暂不存储
        tWFContInfo.setTicketNo(null);
        tWFContInfo.setTeamNo(null);
        tWFContInfo.setSeatNo(null);
        tWFContInfo.setFrom(null);
        tWFContInfo.setTo(null);
        // --------------------

        // 特殊字段 - 投保地域信息暂不存储
        tWFContInfo.setCardArea(null);
        //存放保单终止日期
        tWFContInfo.setBak3(mMsgSimPolicyInfo.getCInValiDate());
        // --------------------

        return tWFContInfo;
    }

    private WFAppntListSchema getAppntInfo()
    {
        WFAppntListSchema tWFAppntInfo = new WFAppntListSchema();

        tWFAppntInfo.setBatchNo(mMsgHead.getBatchNo());

        // 投保人关联数据 - 暂定用卡号
        tWFAppntInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        // --------------------

        tWFAppntInfo.setCardNo(mMsgWrapInfo.getCardNo());

        // 投保人关联数据 - 暂定用卡号
        tWFAppntInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        // --------------------

        tWFAppntInfo.setName(mMsgSimAppntInfo.getName());
        tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
        tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());
        tWFAppntInfo.setIDType(mMsgSimAppntInfo.getIDType());
        tWFAppntInfo.setIDNo(mMsgSimAppntInfo.getIdNo());

        tWFAppntInfo.setOccupationType(mMsgSimAppntInfo.getOccupationType());
        tWFAppntInfo.setOccupationCode(mMsgSimAppntInfo.getOccupationCode());

        tWFAppntInfo.setPostalAddress(mMsgSimAppntInfo.getPostalAddress());
        tWFAppntInfo.setZipCode(mMsgSimAppntInfo.getZipCode());
        tWFAppntInfo.setPhont(mMsgSimAppntInfo.getPhone());
        tWFAppntInfo.setMobile(mMsgSimAppntInfo.getMobile());
        tWFAppntInfo.setEmail(mMsgSimAppntInfo.getEmail());

        tWFAppntInfo.setGrpName(mMsgSimAppntInfo.getGrpName());
        tWFAppntInfo.setCompanyPhone(mMsgSimAppntInfo.getCompayPhone());
        tWFAppntInfo.setCompAddr(mMsgSimAppntInfo.getCompAddr());
        tWFAppntInfo.setCompZipCode(mMsgSimAppntInfo.getCompZipCode());

        return tWFAppntInfo;
    }

    private WFInsuListSet getInsuInfos()
    {
        WFInsuListSet tWFInsuInfos = new WFInsuListSet();

        for (int i = 0; i < mMsgSimInsuInfoList.length; i++)
        {
            SimInsuInfo tSimInsuInfo = mMsgSimInsuInfoList[i];

            WFInsuListSchema tWFInsuInfo = new WFInsuListSchema();

            tWFInsuInfo.setBatchNo(mMsgHead.getBatchNo());

            tWFInsuInfo.setCardNo(mMsgWrapInfo.getCardNo());

            tWFInsuInfo.setInsuNo(tSimInsuInfo.getInsuNo());
            tWFInsuInfo.setToAppntRela(tSimInsuInfo.getToAppntRela());

            tWFInsuInfo.setRelationToInsured(tSimInsuInfo.getToMainInsuRela());

            tWFInsuInfo.setRelationCode(null); // 数据描述为该被保人是否连带，放到业务逻辑中进行处理
            tWFInsuInfo.setReInsuNo(null); // 数据描述为该被保人主被保人编号，放到业务逻辑中进行处理

            tWFInsuInfo.setName(tSimInsuInfo.getName());
            tWFInsuInfo.setSex(tSimInsuInfo.getSex());
            tWFInsuInfo.setBirthday(tSimInsuInfo.getBirthday());
            tWFInsuInfo.setIDType(tSimInsuInfo.getIDType());
            tWFInsuInfo.setIDNo(tSimInsuInfo.getIdNo());

            tWFInsuInfo.setOccupationType(tSimInsuInfo.getOccupationType());
            tWFInsuInfo.setOccupationCode(tSimInsuInfo.getOccupationCode());

            tWFInsuInfo.setPostalAddress(tSimInsuInfo.getPostalAddress());
            tWFInsuInfo.setZipCode(tSimInsuInfo.getZipCode());
            tWFInsuInfo.setPhont(tSimInsuInfo.getPhone());
            tWFInsuInfo.setMobile(tSimInsuInfo.getMobile());
            tWFInsuInfo.setEmail(tSimInsuInfo.getEmail());

            tWFInsuInfo.setGrpName(tSimInsuInfo.getGrpName());
            tWFInsuInfo.setCompanyPhone(tSimInsuInfo.getCompayPhone());
            tWFInsuInfo.setCompAddr(tSimInsuInfo.getCompAddr());
            tWFInsuInfo.setCompZipCode(tSimInsuInfo.getCompZipCode());

            tWFInsuInfos.add(tWFInsuInfo);
        }

        return tWFInsuInfos;
    }

    private WFBnfListSet getBnfInfos()
    {
        WFBnfListSet tWFBnfInfos = new WFBnfListSet();

        if (mMsgSimBnfInfoList != null)
        {
            for (int i = 0; i < mMsgSimBnfInfoList.length; i++)
            {
                SimBnfInfo tSimBnfInfo = mMsgSimBnfInfoList[i];

                WFBnfListSchema tWFInsuInfo = new WFBnfListSchema();

                tWFInsuInfo.setBatchNo(mMsgHead.getBatchNo());

                tWFInsuInfo.setCardNo(mMsgWrapInfo.getCardNo());

                tWFInsuInfo.setToInsuNo(tSimBnfInfo.getToInsuNo());
                tWFInsuInfo.setToInsuRela(tSimBnfInfo.getToInsuNo());

                tWFInsuInfo.setBnfType(tSimBnfInfo.getBnfType());
                tWFInsuInfo.setBnfGrade(tSimBnfInfo.getBnfGrade());
                tWFInsuInfo.setBnfRate(tSimBnfInfo.getBnfRate());

                tWFInsuInfo.setName(tSimBnfInfo.getName());
                tWFInsuInfo.setSex(tSimBnfInfo.getSex());
                tWFInsuInfo.setBirthday(tSimBnfInfo.getBirthday());
                tWFInsuInfo.setIDType(tSimBnfInfo.getIDType());
                tWFInsuInfo.setIDNo(tSimBnfInfo.getIdNo());

                tWFBnfInfos.add(tWFInsuInfo);
            }
        }

        return tWFBnfInfos;
    }

    private WFWrapExListSet getWrapExInfos()
    {
    	WFWrapExListSet tWFWrapExInfos = new WFWrapExListSet();
    	
    	if (mMsgWrapExInfoList != null)
    	{
    		for (int i = 0; i < mMsgWrapExInfoList.length; i++)
    		{
    			WrapExInfo tWrapExInfo = mMsgWrapExInfoList[i];
    			
    			WFWrapExListSchema tWFWrapExInfo = new WFWrapExListSchema();
    			
    			tWFWrapExInfo.setBatchNo(mMsgHead.getBatchNo());
    			
    			tWFWrapExInfo.setCardNo(mMsgWrapInfo.getCardNo());
    			
    			tWFWrapExInfo.setCertifyCode(tWrapExInfo.getCertifyCode());
    			
    			tWFWrapExInfo.setExFactorCode(tWrapExInfo.getExFactorCode());
    			
    			tWFWrapExInfo.setExFactorName(tWrapExInfo.getExFactorName());
    			
    			tWFWrapExInfo.setExFactorValue(tWrapExInfo.getExFactorValue());
    			
    			tWFWrapExInfo.setManageCom(mMsgSimPolicyInfo.getManageCom());
    			
    			tWFWrapExInfo.setOperator(mMsgHead.getSendOperator());
    			
    			tWFWrapExInfo.setMakeDate(mMsgHead.getSendDate());
    			
    			tWFWrapExInfo.setMakeTime(mMsgHead.getSendTime()); 
    			
    			tWFWrapExInfo.setModifyDate(mMsgHead.getSendDate());
    			
    			tWFWrapExInfo.setModifyTime(mMsgHead.getSendTime());
    			
    			tWFWrapExInfos.add(tWFWrapExInfo);
    		}
    	}
    	return tWFWrapExInfos;
    }

    private boolean qryTradeResult(VData cResultVDatas)
    {
        LICardActiveInfoListSet tResActCardInfos = (LICardActiveInfoListSet) cResultVDatas.getObjectByObjectName(
                "LICardActiveInfoListSet", 0);
        if (tResActCardInfos == null || tResActCardInfos.size() == 0)
        {
            String tErrInfo = "查询保单卡承保信息失败。";
            errLog(tErrInfo);
            return false;
        }

        WFContListSchema tResWFContInfo = (WFContListSchema) cResultVDatas.getObjectByObjectName("WFContListSchema", 0);
        if (tResWFContInfo == null)
        {
            String tErrInfo = "查询保单卡投保信息失败。";
            errLog(tErrInfo);
            return false;
        }

        WFAppntListSchema tResWFAppntInfo = (WFAppntListSchema) cResultVDatas.getObjectByObjectName(
                "WFAppntListSchema", 0);
        if (tResWFAppntInfo == null)
        {
            String tErrInfo = "查询保单卡投保人信息失败。";
            errLog(tErrInfo);
            return false;
        }

        WFInsuListSet tResWFInsuInfos = (WFInsuListSet) cResultVDatas.getObjectByObjectName("WFInsuListSet", 0);
        if (tResWFInsuInfos == null || tResWFInsuInfos.size() == 0)
        {
            String tErrInfo = "查询保单卡被保人信息失败。";
            errLog(tErrInfo);
            return false;
        }

        WFBnfListSet tResWFBnfInfos = (WFBnfListSet) cResultVDatas.getObjectByObjectName("WFBnfListSet", 0);
        if (tResWFInsuInfos == null)
        {
            String tErrInfo = "查询保单卡受益人信息失败。";
            errLog(tErrInfo);
            return false;
        }

        // 查询保单投保信息
        SimPolicyInfo tSimPolicyInfo = new SimPolicyInfo();

        tSimPolicyInfo.setPrtNo(tResWFContInfo.getCardNo());

        tSimPolicyInfo.setManageCom(tResWFContInfo.getManageCom());
        tSimPolicyInfo.setSaleChnl(tResWFContInfo.getSaleChnl());
        tSimPolicyInfo.setAgentCom(tResWFContInfo.getAgentCom());
        tSimPolicyInfo.setAgentCode(tResWFContInfo.getAgentCode());













        tSimPolicyInfo.setAgentName(tResWFContInfo.getAgentName());

        // tSimPolicyInfo.setPolApplyDate(null);

        tSimPolicyInfo.setCValiDate(tResWFContInfo.getCvaliDate());
        tSimPolicyInfo.setCValiTime(tResWFContInfo.getCvaliDateTime());

        tSimPolicyInfo.setCInValiDate(tResActCardInfos.get(1).getInActiveDate());
        //tSimPolicyInfo.setCInValiTime(null);

        tSimPolicyInfo.setInsuYear(String.valueOf(tResWFContInfo.getInsuYear()));
        tSimPolicyInfo.setInsuYearFlag(tResWFContInfo.getInsuYearFlag());

        tSimPolicyInfo.setPremScope(String.valueOf(tResWFContInfo.getPrem()));

        tSimPolicyInfo.setPayIntv(tResWFContInfo.getPayIntv());
        tSimPolicyInfo.setPayYear(String.valueOf(tResWFContInfo.getPayYear()));
        tSimPolicyInfo.setPayYearFlag(tResWFContInfo.getPayYearFlag());
        tSimPolicyInfo.setPayMode(tResWFContInfo.getPayMode());

        tSimPolicyInfo.setBankCode(null);
        tSimPolicyInfo.setBankAccNo(null);
        tSimPolicyInfo.setAccName(null);

        tSimPolicyInfo.setRemark(tResWFContInfo.getRemark());

        putResult("SimPolicyInfo", tSimPolicyInfo);
        // --------------------

        // 查询产品信息
        WrapInfo tWrapInfo = new WrapInfo();

        tWrapInfo.setCardNo(tResWFContInfo.getCardNo());
        tWrapInfo.setPassword(tResWFContInfo.getPassword());

        tWrapInfo.setWrapCode(tResWFContInfo.getRiskCode());
        // tWrapInfo.setWrapName(null);

        tWrapInfo.setCertifyCode(tResWFContInfo.getCertifyCode());
        tWrapInfo.setCertifyName(tResWFContInfo.getCertifyName());
        tWrapInfo.setActiveDate(tResWFContInfo.getActiveDate());
        tWrapInfo.setPrem(String.valueOf(tResWFContInfo.getPrem()));
        tWrapInfo.setAmnt(String.valueOf(tResWFContInfo.getAmnt()));
        tWrapInfo.setMult(String.valueOf(tResWFContInfo.getMult()));
        tWrapInfo.setCopys(String.valueOf(tResWFContInfo.getCopys()));

        // tWrapInfo.setReMark(null);

        putResult("WrapInfo", tWrapInfo);
        // --------------------

        // 查询产品扩展信息
        //putResult("WrapExInfo", null);
        // --------------------

        // 查询投保人信息
        SimAppntInfo tSimAppntInfo = new SimAppntInfo();

        tSimAppntInfo.setName(tResWFAppntInfo.getName());
        tSimAppntInfo.setSex(tResWFAppntInfo.getSex());
        tSimAppntInfo.setBirthday(tResWFAppntInfo.getBirthday());
        tSimAppntInfo.setIDType(tResWFAppntInfo.getIDType());
        tSimAppntInfo.setIdNo(tResWFAppntInfo.getIDNo());

        tSimAppntInfo.setOccupationType(tResWFAppntInfo.getOccupationType());
        tSimAppntInfo.setOccupationCode(tResWFAppntInfo.getOccupationCode());

        tSimAppntInfo.setPostalAddress(tResWFAppntInfo.getPostalAddress());
        tSimAppntInfo.setZipCode(tResWFAppntInfo.getZipCode());
        tSimAppntInfo.setPhone(tResWFAppntInfo.getPhont());
        tSimAppntInfo.setMobile(tResWFAppntInfo.getMobile());
        tSimAppntInfo.setEmail(tResWFAppntInfo.getEmail());

        tSimAppntInfo.setGrpName(tResWFAppntInfo.getGrpName());
        tSimAppntInfo.setCompayPhone(tResWFAppntInfo.getCompanyPhone());
        tSimAppntInfo.setCompAddr(tResWFAppntInfo.getCompAddr());
        tSimAppntInfo.setCompZipCode(tResWFAppntInfo.getCompZipCode());

        putResult("SimAppntInfo", tSimAppntInfo);
        // --------------------

        // 查询被保人信息
        for (int i = 1; i <= tResWFInsuInfos.size(); i++)
        {
            WFInsuListSchema tWFInsuInfo = tResWFInsuInfos.get(i);

            SimInsuInfo tSimInsuInfo = new SimInsuInfo();

            tSimInsuInfo.setInsuNo(tWFInsuInfo.getInsuNo());
            tSimInsuInfo.setToMainInsuRela(tWFInsuInfo.getRelationToInsured());
            tSimInsuInfo.setToAppntRela(tWFInsuInfo.getToAppntRela());

            tSimInsuInfo.setName(tWFInsuInfo.getName());
            tSimInsuInfo.setSex(tWFInsuInfo.getSex());
            tSimInsuInfo.setBirthday(tWFInsuInfo.getBirthday());
            tSimInsuInfo.setIDType(tWFInsuInfo.getIDType());
            tSimInsuInfo.setIdNo(tWFInsuInfo.getIDNo());

            tSimInsuInfo.setOccupationType(tWFInsuInfo.getOccupationType());
            tSimInsuInfo.setOccupationCode(tWFInsuInfo.getOccupationCode());

            tSimInsuInfo.setPostalAddress(tWFInsuInfo.getPostalAddress());
            tSimInsuInfo.setZipCode(tWFInsuInfo.getZipCode());
            tSimInsuInfo.setPhone(tWFInsuInfo.getPhont());
            tSimInsuInfo.setMobile(tWFInsuInfo.getMobile());
            tSimInsuInfo.setEmail(tWFInsuInfo.getEmail());
            tSimInsuInfo.setGrpName(tWFInsuInfo.getGrpName());
            tSimInsuInfo.setCompayPhone(tWFInsuInfo.getCompanyPhone());
            tSimInsuInfo.setCompAddr(tWFInsuInfo.getCompAddr());
            tSimInsuInfo.setCompZipCode(tWFInsuInfo.getCompZipCode());

            // tSimInsuInfo.setEnglishName(null);
            putResult("SimInsuInfo", tSimInsuInfo);
        }
        // --------------------

        // 查询受益人信息
        for (int i = 1; i <= tResWFBnfInfos.size(); i++)
        {
            WFBnfListSchema tWFBnfListInfo = tResWFBnfInfos.get(i);

            SimBnfInfo tSimBnfInfo = new SimBnfInfo();

            tSimBnfInfo.setToInsuNo(tWFBnfListInfo.getToInsuNo());

            tSimBnfInfo.setBnfType(tWFBnfListInfo.getBnfType());
            tSimBnfInfo.setBnfGrade(tWFBnfListInfo.getBnfGrade());
            tSimBnfInfo.setBnfRate(String.valueOf(tWFBnfListInfo.getBnfRate()));

            tSimBnfInfo.setName(tWFBnfListInfo.getName());
            tSimBnfInfo.setSex(tWFBnfListInfo.getSex());
            tSimBnfInfo.setBirthday(tWFBnfListInfo.getBirthday());
            tSimBnfInfo.setIDType(tWFBnfListInfo.getIDType());
            tSimBnfInfo.setIdNo(tWFBnfListInfo.getIDNo());

            putResult("SimBnfInfo", tSimBnfInfo);
        }
        // --------------------

        return true;
    }
}

