/**
 * 2011-10-12
 */
package com.cbsws.xml.ctrl.blogic.simsale;

import java.util.Date;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFTransLogSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class SimPolicyWTBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mBatchNo = null;

    private String mSendDate = null;

    private String mSendTime = null;

    private String mBranchCode = null;

    private String mSendOperator = null;

    private String mMsgType = null;

    private WFContListSchema mWFContInfo = null;

    private WFAppntListSchema mWFAppntInfo = null;

    private WFInsuListSet mWFInsuInfos = null;

    public SimPolicyWTBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        if (!prepareResult())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mWFContInfo = (WFContListSchema) cInputData.getObjectByObjectName("WFContListSchema", 0);
        if (mWFContInfo == null)
        {
            buildError("getInputData", "未获取到产品投保信息。");
            return false;
        }

        mWFAppntInfo = (WFAppntListSchema) cInputData.getObjectByObjectName("WFAppntListSchema", 0);
        if (mWFAppntInfo == null)
        {
            buildError("getInputData", "未获取到投保人信息。");
            return false;
        }

        mWFInsuInfos = (WFInsuListSet) cInputData.getObjectByObjectName("WFInsuListSet", 0);
        if (mWFInsuInfos == null || mWFInsuInfos.size() == 0)
        {
            buildError("getInputData", "未获取到被保人信息。");
            return false;
        }

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "获取交易批次号失败。");
            return false;
        }

        mSendDate = (String) mTransferData.getValueByName("SendDate");
        if (mSendDate == null || mSendDate.equals(""))
        {
            buildError("getInputData", "获取交易报送日期失败。");
            return false;
        }

        mSendTime = (String) mTransferData.getValueByName("SendTime");
        if (mSendTime == null || mSendTime.equals(""))
        {
            buildError("getInputData", "获取交易报送时间失败。");
            return false;
        }

        mBranchCode = (String) mTransferData.getValueByName("BranchCode");
        if (mBranchCode == null || mBranchCode.equals(""))
        {
            buildError("getInputData", "获取交易报送单位失败。");
            return false;
        }

        mSendOperator = (String) mTransferData.getValueByName("SendOperator");
        if (mSendOperator == null || mSendOperator.equals(""))
        {
            buildError("getInputData", "获取交易操作员失败。");
            return false;
        }

        mMsgType = (String) mTransferData.getValueByName("MsgType");
        if (mMsgType == null || mMsgType.equals(""))
        {
            buildError("getInputData", "获取交易类型失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        // 处理交易业务数据
        tTmpMap = null;
        tTmpMap = dealWTCertCont();
        if (tTmpMap == null)
        {
            dealWFTransLog(false);
            return false;
        }
        mMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 处理交易日志
        dealWFTransLog(true);
        // --------------------

        return true;
    }

    private MMap dealWTCertCont()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 处理单证信息
        tTmpMap = null;
        tTmpMap = dealCertInfo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 处理保单信息
        if (!dealWFContInfo())
        {
            return null;
        }
        // --------------------

        // 处理承保中间表信息
        tTmpMap = null;
        tTmpMap = dealActCardInfo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 封装数据库
        //        tMMap.put(mActCardInfos, SysConst.INSERT);
        //        tMMap.put(mWFContInfo, SysConst.INSERT);
        //        tMMap.put(mWFAppntInfo, SysConst.INSERT);
        //        tMMap.put(mWFInsuInfos, SysConst.INSERT);
        //        tMMap.put(mWFBnfInfos, SysConst.INSERT);
        // --------------------

        return tMMap;
    }

    private MMap dealCertInfo()
    {
        MMap tMMap = new MMap();

        String tCardNo = mWFContInfo.getCardNo();

        String tStrSql = " select lzc.* "
                + " from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where 1 = 1 " + " and lzcn.CardNo = '" + tCardNo + "' ";
        LZCardSet tCertInfos = new LZCardDB().executeQuery(tStrSql);
        if (tCertInfos == null || tCertInfos.size() == 0)
        {
            buildError("dealCertInfo", "单证信息未找到。");
            return null;
        }
        else if (tCertInfos.size() != 1)
        {
            buildError("dealCertInfo", "单证信息出现多条。");
            return null;
        }

        LZCardSchema tCertCardInfo = tCertInfos.get(1);

        String tDesCertifyCode = tCertCardInfo.getCertifyCode();

        String tCertifyCode = mWFContInfo.getCertifyCode();
        if (!tDesCertifyCode.equals(tCertifyCode))
        {
            buildError("dealCertInfo", "单证类型[" + tCertifyCode + "]与保险卡[" + tCardNo + "]信息不符。");
            return null;
        }

        tCertCardInfo.setState("14");
        tCertCardInfo.setActiveFlag("1");

        tMMap.put(tCertCardInfo, SysConst.UPDATE);

        return tMMap;
    }

    private boolean dealWFContInfo()
    {
        String tStrSql = null;
        String tStrResult = null;
        ExeSQL tExeSql = new ExeSQL();

        // 校验批次号唯一
        tStrSql = "select 1 from WFContList where BatchNo = '" + mBatchNo + "'";
        tStrResult = tExeSql.getOneValue(tStrSql);
        if ("1".equals(tStrResult))
        {
            buildError("dealWFContInfo", "批次信息已存在。");
            return false;
        }
        tStrSql = null;
        tStrResult = null;
        // --------------------

        // 校验卡号存在
        String tCardNo = mWFContInfo.getCardNo();

        tStrSql = "select 1 from WFContList where CardNo = '" + tCardNo + "'";
        tStrResult = tExeSql.getOneValue(tStrSql);
        if (!"1".equals(tStrResult))
        {
            buildError("dealWFContInfo", "[" + tCardNo + "]未找到已承保数据。");
            return false;
        }
        tStrSql = null;
        tStrResult = null;
        // --------------------

        //        mWFContInfo.setBatchNo(mBatchNo);
        //        mWFContInfo.setSendDate(mSendDate);
        //        mWFContInfo.setSendTime(mSendTime);
        //        mWFContInfo.setBranchCode(mBranchCode);
        //        mWFContInfo.setSendOperator(mSendOperator);
        //        mWFContInfo.setMessageType(mMsgType);
        //
        //        mWFContInfo.setCardDealType("01"); // 代表承保
        //
        //        if (!dealCertifyWrapParams(mWFContInfo))
        //        {
        //            return false;
        //        }
        //
        //        if (mWFContInfo.getCvaliDate() == null || mWFContInfo.getCvaliDate().equals(""))
        //        {
        //            buildError("dealWFContInfo", "[" + tCardNo + "]生效日期不得为空。");
        //            return false;
        //        }

        return true;
    }

    private MMap dealActCardInfo()
    {
        MMap tMMap = new MMap();

        String tStrSql = null;
        String tStrResult = null;

        ExeSQL tExeSql = new ExeSQL();

        // 校验卡号存在
        String tCardNo = mWFContInfo.getCardNo();

        tStrSql = "select 1 from LICardActiveInfoList where CardNo = '" + tCardNo + "'";
        tStrResult = tExeSql.getOneValue(tStrSql);
        if (!"1".equals(tStrResult))
        {
            buildError("dealActCardInfo", "[" + tCardNo + "]未找到已承保销售数据。");
            return null;
        }
        tStrSql = null;
        tStrResult = null;
        // --------------------

        // 校验生效日期
        tStrSql = "select min(CValidate) from LICardActiveInfoList where CardNo = '" + tCardNo + "' ";
        tStrResult = tExeSql.getOneValue(tStrSql);

        if (tStrResult != null )
        {
            Date tDCValiDate = new FDate().getDate(tStrResult);
            if (tDCValiDate == null)
            {
                buildError("dealActCardInfo", "[" + tCardNo + "]保险卡生效日期异常。");
                return null;
            }

            String tCurDate = PubFun.getCurrentDate();
            Date tDCurDate = new FDate().getDate(tCurDate);
            if("DLG".equals(mBranchCode)){
            	if(!"PICCTB".equals(mWFContInfo.getRemark())){
            		if (tDCurDate.after(tDCValiDate)&&!mMsgType.equals("CI0003"))
            		{
            			buildError("dealActCardInfo", "[" + tCardNo + "]保险卡已指定于[" + tStrResult + "]生效，不允许撤销。");
            			return null;
            		}
            	}
            }else{
            	if(!"PICCTB".equals(mWFContInfo.getRemark())){
            		if (!tDCurDate.before(tDCValiDate)&&!mMsgType.equals("CI0003"))
            		{
            			buildError("dealActCardInfo", "[" + tCardNo + "]保险卡已指定于[" + tStrResult + "]生效，不允许撤销。");
            			return null;
            		}
            	}
            }
        }
        
        
        if(!"PICCTB".equals(mWFContInfo.getRemark())){
        tStrSql = null;
        tStrResult = null;

        // --------------------
       
        // 校验卡号存在
        if("LMM".equals(mBranchCode)){                      //驴妈妈、大连港修改为校验生效日期
        	tStrSql = "select 1 from LICardActiveInfoList where CardNo = '" + tCardNo + "' and  current date >= CValidate";
        	tStrResult = tExeSql.getOneValue(tStrSql);
        	if ("1".equals(tStrResult)&&!mMsgType.equals("CI0003"))
            {
                buildError("dealActCardInfo", "[" + tCardNo + "]保险卡当前已生效，不允许撤销。");
                return null;
            }
        	
        }else if("DLG".equals(mBranchCode)){
        	tStrSql = "select 1 from LICardActiveInfoList where CardNo = '" + tCardNo + "' and  current date > CValidate";
        	tStrResult = tExeSql.getOneValue(tStrSql);
        	if ("1".equals(tStrResult)&&!mMsgType.equals("CI0003"))
            {
                buildError("dealActCardInfo", "[" + tCardNo + "]保险卡当前已生效，不允许撤销。");
                return null;
            }
        }else{
        	tStrSql = "select 1 from LICardActiveInfoList where CardNo = '" + tCardNo + "' and ActiveDate = current date";
            tStrResult = tExeSql.getOneValue(tStrSql);
            if (!"1".equals(tStrResult)&&!mMsgType.equals("CI0003"))
            {
                buildError("dealActCardInfo", "[" + tCardNo + "]保险卡开卡激活日期不为当天，不允许撤销。");
                return null;
            }
         }
        }
        tStrSql = null;
        tStrResult = null;
        
        // --------------------

        // 校验是否结算或待结算
        tStrSql = "select 1 from LICertify where CardNo = '" + tCardNo + "' ";
        tStrResult = tExeSql.getOneValue(tStrSql);
        if ("1".equals(tStrResult))
        {
            buildError("dealActCardInfo", "[" + tCardNo + "]保险卡已被导入保险公司系统准备结算或已经结算，不允许进行撤单动作。");
            return null;
        }
        tStrSql = null;
        tStrResult = null;
        // --------------------

        String tStrUSql = " update LICardActiveInfoList set CardStatus = '03' where CardNo = '" + tCardNo + "' ";

        tMMap.put(tStrUSql, SysConst.UPDATE);

        return tMMap;
    }

    private void dealWFTransLog(boolean cSucFlag)
    {
        MMap tMMap = new MMap();

        WFTransLogSchema tWFTransLog = new WFTransLogSchema();

        tWFTransLog.setBatchNo(mWFContInfo.getBatchNo());
        tWFTransLog.setMessageType(mWFContInfo.getMessageType());
        tWFTransLog.setSendDate(mWFContInfo.getSendDate());
        tWFTransLog.setSendTime(mWFContInfo.getSendTime());

        if (cSucFlag)
        {
            tWFTransLog.setBatchFlag("00");

            tWFTransLog.setErrCode("0");
            tWFTransLog.setErrInfo("交易成功");
        }
        else
        {
            tWFTransLog.setBatchFlag("01");

            tWFTransLog.setErrCode("99");
            tWFTransLog.setErrInfo(mErrors != null ? mErrors.getFirstError() : "未知异常。");
        }

        tWFTransLog.setopertator(mWFContInfo.getSendOperator());

        tMMap.put(tWFTransLog, SysConst.DELETE_AND_INSERT);

        VData data = new VData();
        data.add(tMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return;
        }
    }

    private boolean prepareResult()
    {
        mResult.clear();

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ":" + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
//    public static void main(String[] args) {
//    	String tStrSql = "select min(CValidate) from LICardActiveInfoList where CardNo = '0Q000000759' ";
//    	ExeSQL tExeSql = new ExeSQL();
//    	String tStrResult = tExeSql.getOneValue(tStrSql);
//
//        if (tStrResult != null )
//        {
//            Date tDCValiDate = new FDate().getDate(tStrResult);
//            String tCurDate = PubFun.getCurrentDate();
//            Date tDCurDate = new FDate().getDate(tCurDate);
//            System.out.println("生效日期tDCValiDate"+tDCValiDate);
//            System.out.println("当前日期tCurDate"+tCurDate);
//            if(tDCurDate.after(tDCValiDate)){
//            	System.out.println("zhiqian ");
//            }
//        } 
//	}
}
