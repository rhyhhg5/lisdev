package com.cbsws.xml.ctrl.blogic;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LvmamaPrintBL extends ABusLogic{
	
	/** 报文信息集合*/
	private MsgCollection mcollection = new MsgCollection();
	
	/** 报文头信息*/
	private MsgHead mMsgHead = new MsgHead();
	
	/** 业务信息集合*/
	private List bodyList = null;
	private String sendDate = null;
	
	/** 业务信息*/
	private CertPrintTable mCertPrintTable = new CertPrintTable();
	
	/** 卡号*/
	private String cardNo = null;
	private String certifycode =null;
	
	/** 产品名称*/
	private String certifyname = null;
	
	/** 产品编码*/
	private String wrapcode = null;
	
	/** 产品名称*/
	private String wrapname = null;
	
	/** 保障期限*/
	private String insuyear = null;
	
	/** 保障期限标志*/
	private String insuyearflag =null;
	
	/** 生效起期*/
	private String cvalidate=null;
	
	/** 终止日期*/
	private String cinvalidate =null;
	
	/** 档次*/
	private String mult=null;
	
	/** 份数*/
	private String coyps=null;
	
	/** 保障责任和保额的对应*/
	private Map resMap=null;
	
	/** 打印条形码*/
	private String barCode="";
	
	/** 特别约定*/
	private String special="";
	private XmlExport dXmlExport = new XmlExport();
	
	/** 报文信息集合类型*/
	private final static String insuInfo ="LCINSU";
	private final static String bnfInfo ="LCBNF";
	private final static String polInfo ="LCPOL";
	private final static String termInfo ="TERM";
	
	/** 条形码约定 单位为半角数字，如'张'为7个数字*/
	private final static int contNoLength=30;
	private final static int insuyearLength=32;
	private final static int insuIdnoLength=18;
	//private final static int dutyLength=266;
	private final static int barcodeLength=200;//单位为汉字
	
	
	private String contNoBar="";
	private String insuyearBar="";
	private String insuIdnoBar="";
	private String dutyBar="";
	private StringBuffer dutyremark = new StringBuffer();
	VData data = new VData();
	public boolean deal(MsgCollection cMsgInfos){
		mcollection = cMsgInfos;
		if(!load()){
			System.out.println("没有得到要打印的信息");
 	        errLog("没有得到要打印的信息");
			return false;
		}
		
		if(!check()){
			System.out.println("校验失败");
 	        errLog("校验失败");
			return false;
		}
		
		if(!subDeal()){
			System.out.println("业务处理异常");
 	        errLog("业务处理异常");
			return false;
		}
		dealPrintMag();
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, ""))
        {
            System.out.println("将打印信息保存到LOPRTManager表失败");
        }
		return true;
	}
	
	private boolean load(){
		mMsgHead = mcollection.getMsgHead();
		
		sendDate = this.mMsgHead.getSendDate();
		bodyList=mcollection.getBodyByFlag("CertPrintTable");
		if(bodyList ==null || bodyList.size()==0){
			System.out.println("没有得到要打印的信息");
  	         errLog("没有得到要打印的信息");
  	         return false;
		}
		return true ;
	}
	
	private boolean check(){
		return true;
	}
	
	private boolean subDeal(){
		
		for (int i=0;i<bodyList.size();i++){
			this.mCertPrintTable = (CertPrintTable)bodyList.get(i);
			this.cardNo =  this.mCertPrintTable.getCardNo();
			this.special=  this.mCertPrintTable.getSpecialClause();
			if(this.cardNo!=null && !this.cardNo.equals("")){
				
				//保单中的单证编号和名称和产品代码
				StringBuffer contSQL = new StringBuffer();
				contSQL.append("select lmcd.certifycode,lmcd.certifyname,lmcr.riskcode from LZCardNumber lzcn " +
									"inner join lmcertifydes lmcd on lmcd.subcode = lzcn.cardtype " +
									"inner join LMCardRisk lmcr on lmcr.certifycode = lmcd.certifycode " +
									"where 1 = 1 and lzcn.CardNo = '").append(this.cardNo).append("' ");
				SSRS contSSRS =  new ExeSQL().execSQL(contSQL.toString());
				if(contSSRS == null || contSSRS.MaxRow!=1){
					System.out.println("没有得到保单信息");
		  	         errLog("没有得到保单信息");
		  	         return false;
				}
				this.certifycode = contSSRS.GetText(1, 1);
				this.certifyname = contSSRS.GetText(1, 2);
				this.wrapcode =  contSSRS.GetText(1, 3);
				String wrapnameSQL = "select wrapname from ldwrap where riskwrapcode ='"+this.wrapcode+"'";
				this.wrapname = new ExeSQL().getOneValue(wrapnameSQL);
				//this.wrapcode ="WR0001";
				
				String specialSQL="select remark from ldwrap where riskwrapcode='"+this.wrapcode+"'";
				String speContent = new ExeSQL().getOneValue(specialSQL);
				if(!speContent.equals("") && speContent !=null){
					this.special=speContent;
				}
				//保险卡信息
				String insuyearSQL = "select lica.insuyear,lica.InsuYearFlag,lica.CvaliDate,lica.mult,lica.copys " +
						"from licardactiveinfolist lica " +
				" where sequenceno='1' and lica.cardno ='"+this.cardNo+"'";
				SSRS cardSSRS =  new ExeSQL().execSQL(insuyearSQL);
				if(cardSSRS == null || cardSSRS.MaxRow!=1){
					System.out.println("没有得到保险卡信息");
		  	         errLog("没有得到保险卡信息");
		  	         return false;
				}
				this.insuyear = cardSSRS.GetText(1, 1);
				this.insuyearflag = cardSSRS.GetText(1, 2);
				this.cvalidate = cardSSRS.GetText(1, 3);
				this.mult = cardSSRS.GetText(1, 4);
				this.coyps = cardSSRS.GetText(1, 5);
//				by gzh 20110302 调整获取终止日期的逻辑过程，激活卡信息在licardactiveinfolist表中获取不到终止日期。
				if(insuyear==null || "".equals(insuyear) || insuyearflag==null || "".equals(insuyearflag)){
					String dutysql = "select distinct calfactor,calfactortype,calfactorvalue from ldriskdutywrap where riskwrapcode = " +
							" (select riskcode from lmcardrisk where certifycode =" +
							" (select certifycode from lmcertifydes where subcode = '"+this.cardNo.substring(0,2)+"')) " +
							" and (Calfactor ='InsuYear' or Calfactor ='InsuYearFlag')";
					SSRS DutySSRS = new ExeSQL().execSQL(dutysql);
					if(DutySSRS==null || DutySSRS.MaxRow==0){
						System.out.println("没有得到保险责任信息！");
			  	         errLog("没有得到保险责任信息！");
			  	         return false;
					}
					for(int j=1;j<=DutySSRS.getMaxRow();j++){
						if("1".equals(DutySSRS.GetText(j, 2))){
							if("InsuYear".equals(DutySSRS.GetText(j, 1))){
								this.insuyear = DutySSRS.GetText(j, 3);
							}else if("InsuYearFlag".equals(DutySSRS.GetText(j, 1))){
								this.insuyearflag = DutySSRS.GetText(j, 3);
							}
						}else if("2".equals(DutySSRS.GetText(j, 2))){
							PubCalculator tCal = new PubCalculator();

			                String tCalSql = DutySSRS.GetText(j, 4);
			                System.out.println("CalSql:" + tCalSql);
			                tCal.setCalSql(tCalSql);

			                tCal.addBasicFactor("Copys", String.valueOf(this.coyps));
			                tCal.addBasicFactor("Mult", String.valueOf(this.mult));

			                String tTmpCalResult = tCal.calculate();

			                if (tTmpCalResult == null || tTmpCalResult.equals(""))
			                {
			                	System.out.println("计算要素失败！");
					  	         errLog("计算要素失败！");
			                    return false;
			                }
							if("InsuYear".equals(DutySSRS.GetText(j, 1))){
								this.insuyear = tTmpCalResult;
							}else if("InsuYearFlag".equals(DutySSRS.GetText(j, 1))){
								this.insuyearflag = tTmpCalResult;
							}
						}
					}
				}
				if(insuyear==null || "".equals(insuyear) || insuyearflag==null || "".equals(insuyearflag)){
					System.out.println("计算终止日期时，没有得到保险责任信息！");
		  	         errLog("计算终止日期时，没有得到保险责任信息！");
		  	         return false;
				}
				//------by gzh end
				//计算终止日期
				this.cinvalidate=PubFun.calDate(this.cvalidate, Integer.parseInt(this.insuyear), 
						this.insuyearflag,null);
				this.cinvalidate = getPreviousDate(new FDate().getDate(cinvalidate)); // by  gzh 获取前一天日期
//				this.cvalidate = DealDateZero(cvalidate);
//				this.cinvalidate = DealDateZero(cinvalidate);
				
				//投保人信息
				StringBuffer appntSQL = new StringBuffer();
				appntSQL.append("select distinct wfa.name,wfa.sex,wfa.birthday,wfa.idtype,wfa.idno from licardactiveinfolist lica " +
									" inner join wfappntList wfa on lica.cardno= wfa.cardno " +
										" where lica.cardno ='").append(this.cardNo).append("' ");
				SSRS appntSSRS = new ExeSQL().execSQL(appntSQL.toString());
				if(appntSSRS==null || appntSSRS.MaxRow==0){
					System.out.println("没有得到投保人信息");
		  	         errLog("没有得到投保人信息");
		  	         return false;
				}
											
				//被保人信息
				StringBuffer insuSQL = new StringBuffer();
				insuSQL.append("select distinct wfi.name,wfi.sex,wfi.birthday,wfi.idtype,wfi.idno,(case when relationcode ='00' then db2inst1.codename('relation',wfi.Toappntrela) else db2inst1.codename('relation',wfi.Relationtoinsured) end) from licardactiveinfolist lica " +
						" inner join wfinsulist wfi on lica.cardno= wfi.cardno " +
						" where lica.cardno ='").append(this.cardNo).append("' ");
				SSRS insuSSRS = new ExeSQL().execSQL(insuSQL.toString());
				if(insuSSRS==null || insuSSRS.MaxRow==0){
					System.out.println("没有得到被保人信息");
		  	         errLog("没有得到被保人信息");
		  	         return false;
				}
				
				//受立益人信息
				StringBuffer bnfSQL = new StringBuffer();
				bnfSQL.append("select distinct wfb.name,wfb.sex,wfb.birthday,wfb.idtype,wfb.idno,wfb.ToInsuRela from licardactiveinfolist lica " +
						" inner join wfbnflist wfb on lica.cardno= wfb.cardno " +
						" where lica.cardno ='").append(this.cardNo).append("' ");
				SSRS bnfSSRS = new ExeSQL().execSQL(bnfSQL.toString());
				
				//保险责任
				this.resMap =calculateAmnt();
				String dutySQL="select lm.dutycode,lm.dutyname,lm.outdutyname from ldriskdutywrap ld"  // by gzh 20110321 增加lm.outdutyname
					+" inner join lmduty lm on lm.dutycode = ld.dutycode"
					+" where calfactor = 'Amnt' and riskwrapcode = '"+this.wrapcode+"'";
				SSRS dutySSRS = new ExeSQL().execSQL(dutySQL);
				if(dutySSRS==null || dutySSRS.MaxRow==0){
					System.out.println("没有得到保险责任信息");
		  	         errLog("没有得到保险责任信息");
		  	         return false;
				}
				//适用条款
//				String termSQL="select riskcode,riskname from lmrisk " +
//						"where riskcode in (select distinct riskcode " +
//						"from ldriskdutywrap  where riskwrapcode='"+this.wrapcode+"')";
//				SSRS termSSRS = new ExeSQL().execSQL(termSQL);
//				if(termSSRS==null || termSSRS.MaxRow==0){
//					System.out.println("没有得到适用条款信息");
//		  	         errLog("没有得到适用条款信息");
//		  	         return false;
//				}
				//createBarcode

				for(int j=1;j<=insuSSRS.MaxRow;j++){
					String insuIdno =insuSSRS.GetText(j, 5);
					this.insuIdnoBar +=insuIdno;
				}
				
				if(this.dutyremark.toString().equals("") || this.dutyremark.toString() ==null){
					System.out.println("没有得到适用条款信息");
		  	         errLog("没有得到适用条款信息");
		  	         return false;
				}
				
				String premSQL = "select prem from licardactiveinfolist where cardno ='"+this.cardNo+"'";
				String prem = new ExeSQL().getOneValue(premSQL);
//				String termSQL = "select Bak5,Bak6,Bak7,Bak8 from WFContlist where cardno ='"+this.cardNo+"'";
				String termSQL = "select cvalidate,inactivedate from licardactiveinfolist where cardno ='"+this.cardNo+"'";
				SSRS termSSRS = new ExeSQL().execSQL(termSQL.toString());
				String startDate = "",startTime = "",endDate = "",endTime = "";
				for(int k=1;k<=termSSRS.MaxRow;k++){
					startDate =termSSRS.GetText(k, 1);
					startTime ="000000";
					endDate =termSSRS.GetText(k, 2);
					endTime ="240000";
				}
//				his.cvalidate = DealDateZero(cvalidate);2014-05-30
//				this.cinvalidate = DealDateZero(cinvalidate);2014-06-03
				String term = "自"+cvalidate.substring(0,4)+"年"+cvalidate.substring(5,7)+"月"+cvalidate.substring(8,10)+"日"
				+startTime.substring(0,2)+"时"+startTime.substring(2,4)+"分起"
				+"至"+cinvalidate.substring(0,4)+"年"+cinvalidate.substring(5,7)+"月"+cinvalidate.substring(8,10)+"日"
				+endTime.substring(0,2)+"时"+endTime.substring(2,4)+"分止";
				//XmlExport dXmlExport = new XmlExport();
				//获取保险责任
//				String dutycontSQL = "select codename from LDCODE where codetype='lvmamadutyremark' and code ='"+this.wrapcode+"'";
//				SSRS dutycontSQLSSRS = new ExeSQL().execSQL(dutycontSQL.toString());
////				if(dutycontSQLSSRS==null || dutycontSQLSSRS.MaxRow==0){
////					System.out.println("没有得到保险责任信息");
////		  	        errLog("没有得到保险责任信息");
////		  	        return false;
////				}
//				ResourceBundle rb = ResourceBundle.getBundle("dutyremark");
				DutyRemark dr = new DutyRemark();
				String dutyconttent = dr.getdutyremark(this.wrapcode);
//				String dutyconttent = dutycontSQLSSRS.GetText(1, 1);;
//				String dutyconttent = "保险期间内，被保人遭受意外伤害+伤残(急性病身故)，保险金额500000元。";
				this.contNoBar = this.cardNo;	
				this.insuyearBar = DealDateZero(cvalidate)+DealDateZero(cinvalidate);
				this.dutyBar=this.dutyremark.toString();
				
				this.barCode=this.contNoBar+this.insuyearBar+this.insuIdnoBar+dutyconttent;
				
				System.out.println("barcode=="+this.barCode);
				
				dXmlExport.createDocument("indigo", "");
				TextTag tTextTag = new TextTag();
				tTextTag.add("JETFORMTYPE", "WXP_004");
				tTextTag.add("MANAGECOMLENGTH4", "DZSW");
				tTextTag.add("USERIP", "127_0_0_1");
				tTextTag.add("PREVIEWFLAG", "1");
				tTextTag.add("CVALIDATE", cvalidate);
				tTextTag.add("CINVALIDATE", cinvalidate);
				tTextTag.add("TERM", term);
				tTextTag.add("DUTYCONTENT", dutyconttent);
				tTextTag.add("PREM", prem+"元");
				if(mMsgHead.getBranchCode().equalsIgnoreCase("SZM")){
					tTextTag.add("SALECOM", "深圳分公司本级");
				}else{
					tTextTag.add("SALECOM", "上海分公司本级");
				}
				tTextTag.add("CERTIFYNAME", certifyname);
				tTextTag.add("CARDNO", this.cardNo);
				tTextTag.add("WRAPCODE", this.wrapcode);
				tTextTag.add("APPNTNAME", appntSSRS.GetText(1, 1));
				tTextTag.add("APPNTSEX", appntSSRS.GetText(1, 2));
				tTextTag.add("APPNTBIRTHDAY", appntSSRS.GetText(1, 3));
				tTextTag.add("APPNTIDTYPE", appntSSRS.GetText(1, 4));
				tTextTag.add("APPNTIDNO", appntSSRS.GetText(1, 5));
				tTextTag.add("BARCODE", this.barCode);
				tTextTag.add("LCSPEC", this.special);
				dXmlExport.addTextTag(tTextTag);
				dealCardListInfo(dXmlExport,insuSSRS,this.insuInfo);
				if(bnfSSRS==null || bnfSSRS.MaxRow==0){
					System.out.println("没有得到受立益人信息");
				}else{
					dealCardListInfo(dXmlExport,bnfSSRS,this.bnfInfo);
				}
				dealCardListInfo(dXmlExport,dutySSRS,this.polInfo);
//				dealCardListInfo(dXmlExport,termSSRS,this.termInfo);
//				String xmlDoc =dXmlExport.outputString();
//				System.out.println("doc=="+xmlDoc);
				if(!FTPSendFile()){
					System.out.println("上传FTP失败");
		  	         errLog("上传FTP失败");
		  	         return false;
				}
			}else{
				System.out.println("没有得到卡号");
	  	         errLog("没有得到卡号");
	  	         return false;
			}			
		}
		return true;
	}
	
	/**
	 * 生成XML
	 * @param cXmlExport
	 * @param data
	 * @param type
	 * @return
	 */
	private boolean dealCardListInfo(XmlExport cXmlExport,SSRS data,String type)
    {
		if(type.equals(this.insuInfo)){
			String[] tCertifyListInfoTitle = new String[7];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "NAME";
	        tCertifyListInfoTitle[2] = "SEX";
	        tCertifyListInfoTitle[3] = "BIRTHDAY";
	        tCertifyListInfoTitle[4] = "IDTYPE";
	        tCertifyListInfoTitle[5] = "IDNO";
	        tCertifyListInfoTitle[6] = "TOAPPNTRELA";
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[7];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = tTmpCardInfo[0];
	            oneCardInfo[2] = tTmpCardInfo[1];
	            oneCardInfo[3] = tTmpCardInfo[2];
	            oneCardInfo[4] = tTmpCardInfo[3];
	            oneCardInfo[5] = tTmpCardInfo[4];
	            oneCardInfo[6] = tTmpCardInfo[5];
	            tListTable.add(oneCardInfo);
	        }
	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);  
		}
		if(type.equals(this.bnfInfo)){
			String[] tCertifyListInfoTitle = new String[7];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "NAME";
	        tCertifyListInfoTitle[2] = "SEX";
	        tCertifyListInfoTitle[3] = "BIRTHDAY";
	        tCertifyListInfoTitle[4] = "IDTYPE";
	        tCertifyListInfoTitle[5] = "IDNO";
	        tCertifyListInfoTitle[6] = "TOINSURELA";
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[7];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = tTmpCardInfo[0];
	            oneCardInfo[2] = tTmpCardInfo[1];
	            oneCardInfo[3] = tTmpCardInfo[2];
	            oneCardInfo[4] = tTmpCardInfo[3];
	            oneCardInfo[5] = tTmpCardInfo[4];
	            oneCardInfo[6] = tTmpCardInfo[5];
	            tListTable.add(oneCardInfo);
	        }	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
		if(type.equals(this.polInfo)){
			String[] tCertifyListInfoTitle = new String[5];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "WRAPCODE";
	        tCertifyListInfoTitle[2] = "DUTYCODE";
	        tCertifyListInfoTitle[3] = "DUTYNAME";
	        tCertifyListInfoTitle[4] = "AMNT";
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[5];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = this.wrapcode;
	            oneCardInfo[2] = tTmpCardInfo[0];
	            //by gzh 20110321 当产品描述中存在outdutyname时，报文中发送outdutyname，否则发送dutyname，打印结果便于客户明白
	            if(tTmpCardInfo[2]!= null && !"".equals(tTmpCardInfo[2])){
	            	oneCardInfo[3] = tTmpCardInfo[2]; //outdutyname
	            }else{
	            	oneCardInfo[3] = tTmpCardInfo[1]; //dutyname
	            }
	            oneCardInfo[4] = (String)this.resMap.get(tTmpCardInfo[1]);

	            tListTable.add(oneCardInfo);
	        }
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
		if(type.equals(this.termInfo)){
			String[] tCertifyListInfoTitle = new String[3];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "RISKCODE";
	        tCertifyListInfoTitle[2] = "RISKNAME";

	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[3];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = tTmpCardInfo[0];
	            oneCardInfo[2] = tTmpCardInfo[1];
	            tListTable.add(oneCardInfo);
	        }
	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
        
		}
        return true;
    }
	
	/**
	 * 保险责任
	 * @return
	 */
	private Map calculateAmnt(){
		String tTmpCalResult = null;
		Map dutymap = new HashMap();
		
		String dutyname =null;
		String calfactortype =null;
		String calfactorvalue =null;
		String calsql =null;
		
		String dutySQL="select lm.dutyname,ld.calfactortype,ld.calfactorvalue, ld.calsql from ldriskdutywrap ld"
								+" inner join lmduty lm on lm.dutycode = ld.dutycode"
								+" where calfactor = 'Amnt' and riskwrapcode = '"+this.wrapcode+"'";
		try{
			
			SSRS dutySSRS = new ExeSQL().execSQL(dutySQL);
			if(dutySSRS==null || dutySSRS.MaxRow==0){
				System.out.println("计算保额失败");
				return null;
			}
			
			for(int i=1;i<=dutySSRS.MaxRow;i++){
				dutyname = dutySSRS.GetText(i, 1);
				calfactortype = dutySSRS.GetText(i, 2);
				calfactorvalue = dutySSRS.GetText(i, 3);
				calsql = dutySSRS.GetText(i, 4);
				if ("2".equals(calfactortype))
		        {
		            /** 准备要素 calbase */
		            PubCalculator tCal = new PubCalculator();

		            tCal.addBasicFactor("RiskCode", this.wrapcode);
		            tCal.addBasicFactor("Copys", String.valueOf(this.coyps));
		            tCal.addBasicFactor("Mult", String.valueOf(this.mult));

		            /** 计算 */
		            tCal.setCalSql(calsql);
		            System.out.println("CalSql:" + calsql);

		            tTmpCalResult = tCal.calculate();

		            if (tTmpCalResult == null || tTmpCalResult.equals(""))
		            {
		                String tStrErr = "单证号[" + this.cardNo + "]保额计算失败。";               
		                System.out.println(tStrErr);
		                return null;
		                // tTmpCalResult = "0";
		            }
		        }
		        else
		        {
		            tTmpCalResult = calfactorvalue;
		        }			
				dutymap.put(dutyname, tTmpCalResult);
				dutyremark.append(dutyname + tTmpCalResult);
			}
	        
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
        return dutymap;
	}
	
	/**
	 * 上传XML到FTP服务器
	 * 
	 * @return
	 */
	private boolean FTPSendFile(){
	      
	    /**文件*/
	    String fileName=this.cardNo;
	    String filePath="";
	    
	    try{
	    	String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PDFstrUrl'";//生成文件的存放路径
	    	filePath = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
	        System.out.println("生成文件的存放路径   "+filePath);//调试用－－－－－
	        if(filePath == null ||filePath.equals("")){
	            System.out.println("获取文件存放路径出错");
	            return false;
	        }
//	        filePath="D:\\WXP002\\";
		    String xmlDoc=this.dXmlExport.outputString();
		    System.out.println("doc=="+xmlDoc);
	        this.dXmlExport.outputDocumentToFile(filePath, fileName,"UTF-8");
		    //上传文件到FTP服务器
		    	if (!sendZip(filePath+fileName+".xml")) {
		    		System.out.println("上传文件到FTP服务器失败！");
		            return false;
		        } else {
		            //上传成功后删除
		        	System.out.println("上传文件到FTP服务器成功！");
		        	File xmlFile = new File(filePath+fileName+".xml");
		        	xmlFile.delete(); //删除上传完的文件
		        }		    
	    }catch(Exception e){
	    	e.printStackTrace();
	    	return false;
	    }
		return true;
	}
	/**
     * sendZip
     * 上传文件至指定FTP（清单文件和单打的数据文件在同一个目录）
     * @param string String
     * @param flag String
     * @return boolean
     */
    private boolean sendZip(String cFileUrlName) {
        //登入前置机
         String ip = "";
         String user = "";
         String pwd = "";
         int port = 21;//默认端口为21

         String sql = "select code,codename from LDCODE where codetype='printserver'";
         SSRS tSSRS = new ExeSQL().execSQL(sql);
         if(tSSRS != null){
             for(int m=1;m<=tSSRS.getMaxRow();m++){
                 if(tSSRS.GetText(m,1).equals("IP")){
                     ip = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("UserName")){
                     user = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Password")){
                     pwd = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Port")){
                     port = Integer.parseInt(tSSRS.GetText(m,2));
                 }
             }
         }
//         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port,"ActiveMode");
         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port);
//         FTPTool tFTPTool = new FTPTool("10.252.130.174", "JL", "jl", 21);
         try
         {
             if (!tFTPTool.loginFTP())
             {
                 System.out.println(tFTPTool.getErrContent(1));
                 return false;
             }
             else
             {
                  if (!tFTPTool.upload(cFileUrlName)) {
                         CError tError = new CError();
                         tError.errorMessage = tFTPTool
                                               .getErrContent(
                                 FTPReplyCodeName.LANGUSGE_CHINESE);
                         
                         System.out.println("上载文件失败!");
                         return false;
                  }

                 tFTPTool.logoutFTP();
             }
         }
         catch (SocketException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                      .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
              
             System.out.println("上载文件失败，可能是网络异常!");
             return false;
         }
         catch (IOException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                     .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
             
             System.out.println("上载文件失败，可能是无法写入文件");
             return false;
         }

         return true;

    }
	/**
     * 将字符串补数,将sourString的<br>后面</br>用cChar补足cLen长度的字符串,如果字符串超长，则不做处理
     * <p><b>Example: </b><p>
     * <p>RCh("Minim", "0", 10) returns "Minim00000"<p>
     * @param sourString 源字符串
     * @param cChar 补数用的字符
     * @param cLen 字符串的目标长度
     * @return 字符串
     */
    private String RCh(String sourString, String cChar, int cLen) {
        int tLen = sourString.getBytes().length;
        int i, j, iMax;
        String tReturn = "";
        if (tLen >= cLen) {
            return sourString;
        }
        iMax = cLen - tLen;
        for (i = 0; i < iMax; i++) {
            tReturn = tReturn + cChar;
        }
        tReturn = sourString + tReturn;
        return tReturn;
    }
    
    private String createBarcode(String pram,int pramlen){
    	String result=null;
    	int len =pram.getBytes().length;
    	LvmamaPrintBL ecp = new LvmamaPrintBL();
    	result=ecp.RCh(pram, " ", pramlen);
    	System.out.println("result===="+result.getBytes().length);
    	return result;
    }
    /**
     * 计算条形码长度
     * @param barStr
     * @return
     */
    public int CalculateBarCodeLen(String barStr){
    	  	
    	int strLen=0;
    	char [] barArr = barStr.toCharArray();
    	for (int i=0;i<barArr.length;i++){
    		String tempStr = String.valueOf(barArr[i]);
    		if(tempStr.getBytes().length ==2){
    			strLen+=7;
    		}else{
    			strLen+=1;
    		}
    	}
    	return strLen;
    }
    
    /**
     * 去掉年份中月和日前面的零
     * @return
     */
    public String DealDateZero(String dateStr){
    	String result=null;
    	String year =null;
    	String month =null;
    	String date =null;
    	   	
    	if(dateStr.equals("") || dateStr == null){
    		System.out.println("没有得到有效的字符类型的日期");
    		return null;
    	}
    	year = dateStr.substring(0, 4);
    	month = dateStr.substring(5, 7);
    	date = dateStr.substring(8, 10);
    	
    	if(month.substring(0,1).equals("0")){
    		month= month.substring(1);
    	}
    	if(date.substring(0,1).equals("0")){
    		date= date.substring(1);
    	}
    	result = year+"-"+month+"-"+date;
    	return result;
    }
    
    //gzh 20110118
    private boolean dealPrintMag() {
    	
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String tLimit = PubFun.getNoLimit("86000000");
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        tLOPRTManagerSchema.setOtherNo(cardNo); //存放卡号
        tLOPRTManagerSchema.setOtherNoType("02");
        tLOPRTManagerSchema.setCode("WX01");
        tLOPRTManagerSchema.setManageCom("86000000");
        tLOPRTManagerSchema.setAgentCode("aaaa");
        tLOPRTManagerSchema.setReqCom("86000000");
        tLOPRTManagerSchema.setReqOperator("001");
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("1");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        MMap tMMap = new MMap();
        tMMap.put(tLOPRTManagerSchema, "INSERT");
        data.add(tMMap);
        return true;
    }
//  by gzh 20110302 获取date前一天的日期
    public String getPreviousDate(Date date){ 
    	Calendar calendar=Calendar.getInstance(); 
    	FDate tFDate = new FDate();
    	calendar.setTime(date); 
    	int day=calendar.get(Calendar.DATE); 
    	calendar.set(Calendar.DATE,day-1); 
    	return tFDate.getString(calendar.getTime()); 
    	} 
    /**
	 * 生成批次号
	 */
	private static String  makeBatchNO(){
		//Calendar nowTime=Calendar.getInstance();
		//System.out.println(nowTime);
		//return nowTime.getTimeInMillis()+"";
		Calendar CD = Calendar.getInstance();
		int YY = CD.get(Calendar.YEAR);
		int MM = CD.get(Calendar.MONTH)+1;
		int DD = CD.get(Calendar.DATE);
		int HH = CD.get(Calendar.HOUR_OF_DAY);
		int NN = CD.get(Calendar.MINUTE);
		int SS = CD.get(Calendar.SECOND);
		int MI = CD.get(Calendar.MILLISECOND);
		return ""+YY+MM+DD+HH+NN+SS+MI+genRandomNum(4);
	}
	/**
     * 生成随即密码
     * @param pwd_len 生成的密码的总长度
     * @return  密码的字符串
     */
    private static String genRandomNum(int pwd_len) {
        //35是因为数组是从0开始的，26个字母+10个数字
//        final int maxNum = 100;
        int i; //生成的随机数
        int count = 0; //生成的密码的长度
        char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        StringBuffer pwd = new StringBuffer("");
//        Random r = new Random();
        while (count < pwd_len) 
        {
            //生成随机数，取绝对值，防止生成负数，
//            i = Math.abs(r.nextInt(maxNum)); //生成的数最大为36-1
        	i = (int)(Math.random()*10);
            if (i >= 0 && i < str.length) 
            {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }
	public static void main(String args[]){
		LvmamaPrintBL ecp = new LvmamaPrintBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		//tMsgHead.setBatchNo(makeBatchNO());
		tMsgHead.setBatchNo("SF0002LMM201111011219220527109");
		
		tMsgHead.setBranchCode("001");
		tMsgHead.setMsgType("INDIGO");
		tMsgHead.setSendDate(PubFun.getCurrentDate());
		tMsgHead.setSendTime(PubFun.getCurrentTime());
		tMsgHead.setSendOperator("EC_WX");
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo("1E000000165");
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);
		ecp.deal(tMsgCollection);
		
	}
}
