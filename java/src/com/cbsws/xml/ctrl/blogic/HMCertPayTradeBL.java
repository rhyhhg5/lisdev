/**
 * 2011-6-3
 */
package com.cbsws.xml.ctrl.blogic;

import java.security.MessageDigest;

import sun.misc.BASE64Encoder;

import com.sinosoft.lis.db.LICertSalesPayInfoDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertSalesPayInfoSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class HMCertPayTradeBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 系统处理当前日期 */
    private String mCurDate = PubFun.getCurrentDate();

    /** 系统处理当前时间 */
    private String mCurTime = PubFun.getCurrentTime();

    /** 批次号 */
    private String mBatchNo = null;

    /** 管理机构 */
    private String mManageCom = null;

    /** 销售渠道 */
    private String mSaleChnl = null;

    /** 产品代码 */
    private String mWrapCode = null;

    /** 产品名称 */
    private String mWrapName = null;

    /** 销售数量 */
    private String mSalesCount = null;

    /** 交易号 */
    private String mFinanceNum = null;

    /** 收费账号 */
    private String mChargeAccount = null;

    /** 收费金额 */
    private String mChargeAmount = null;

    /** 到账日期 */
    private String mConfMakeDate = null;

    /** 到账时间 */
    private String mConfMakeTime = null;

    /** 接口识别码 */
    private String mIdentityKey = null;

    /** 缴费交易轨迹 */
    private LICertSalesPayInfoSchema mPayTradeTrack = null;

    public HMCertPayTradeBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            mGlobalInput = new GlobalInput();
            mGlobalInput.ComCode = "86";
            mGlobalInput.ManageCom = "86";
            mGlobalInput.Operator = "HM_JYK";
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "批次号不能为空。");
            return false;
        }

        mManageCom = (String) mTransferData.getValueByName("ManageCom");
        if (mManageCom == null || mManageCom.equals(""))
        {
            buildError("getInputData", "管理机构不能为空。");
            return false;
        }

        mSaleChnl = (String) mTransferData.getValueByName("SaleChnl");

        mWrapCode = (String) mTransferData.getValueByName("WrapCode");

        mWrapName = (String) mTransferData.getValueByName("WrapName");

        mSalesCount = (String) mTransferData.getValueByName("SalesCount");

        mFinanceNum = (String) mTransferData.getValueByName("FinanceNum");
        if (mFinanceNum == null || mFinanceNum.equals(""))
        {
            buildError("getInputData", "交易号不能为空。");
            return false;
        }

        mChargeAccount = (String) mTransferData.getValueByName("ChargeAccount");
        if (mChargeAccount == null || mChargeAccount.equals(""))
        {
            buildError("getInputData", "收费账号不能为空。");
            return false;
        }

        mChargeAmount = (String) mTransferData.getValueByName("ChargeAmount");
        if (mChargeAmount == null || mChargeAmount.equals(""))
        {
            buildError("getInputData", "收费金额不能为空。");
            return false;
        }

        mConfMakeDate = (String) mTransferData.getValueByName("ConfMakeDate");
        if (mConfMakeDate == null || mConfMakeDate.equals(""))
        {
            buildError("getInputData", "到帐日期不能为空。");
            return false;
        }

        mConfMakeTime = (String) mTransferData.getValueByName("ConfMakeTime");
        if (mConfMakeTime == null || mConfMakeTime.equals(""))
        {
            buildError("getInputData", "到帐时间不能为空。");
            return false;
        }

        mIdentityKey = (String) mTransferData.getValueByName("IdentityKey");
        if (mIdentityKey == null || mIdentityKey.equals(""))
        {
            buildError("getInputData", "接口识别码不能为空。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("HMPay".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = dealHMPayTrade();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap dealHMPayTrade()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 校验身份
        if (!chkIdentity())
        {
            return null;
        }
        // --------------------

        // 处理缴费轨迹
        tTmpMap = crtPayTrack();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 生成财务暂收数据
        tTmpMap = crtTempFeeTrade();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    /**
     * 
     * @return
     */
    private boolean chkIdentity()
    {
        if (mFinanceNum == null || "".equals(mFinanceNum))
        {
            buildError("chkIdentity", "交易号不能为空。");
            return false;
        }

        String cTmpFinanceNum = mFinanceNum + "000";

        String sEncKeyMD5 = encryptMD5(cTmpFinanceNum);
        if (sEncKeyMD5 == null)
        {
            return false;
        }

        if (!sEncKeyMD5.equals(mIdentityKey))
        {
            buildError("chkIdentity", "接口识别码校验失败。");
            return false;
        }

        return true;
    }

    private MMap crtPayTrack()
    {
        MMap tMMap = new MMap();

        // 相同批次只允许处理一次
        LICertSalesPayInfoDB tPayTradeInfoDB = new LICertSalesPayInfoDB();
        tPayTradeInfoDB.setBatchNo(mBatchNo);
        if (tPayTradeInfoDB.getInfo())
        {
            buildError("crtPayTrack", "批次号已经在" + tPayTradeInfoDB.getMakeDate() + " " + tPayTradeInfoDB.getMakeTime()
                    + "处理过。");
            return null;
        }
        // --------------------

        LICertSalesPayInfoSchema tPayTradeInfo = new LICertSalesPayInfoSchema();

        tPayTradeInfo.setBatchNo(mBatchNo);

        tPayTradeInfo.setPayTradeSeq(mFinanceNum);

        tPayTradeInfo.setManageCom(mManageCom);
        tPayTradeInfo.setSaleChnl(mSaleChnl);
        tPayTradeInfo.setWrapCode(mWrapCode);
        tPayTradeInfo.setWrapName(mWrapName);

        tPayTradeInfo.setSalesCount(mSalesCount);
        tPayTradeInfo.setSalesDate(mConfMakeDate + " " + mConfMakeTime);

        try
        {
            Double.parseDouble(mChargeAmount);
        }
        catch (NumberFormatException ex)
        {
            ex.printStackTrace();
            buildError("crtPayTrack", "收费金额为非数值数据。");
            return null;
        }
        tPayTradeInfo.setPayMoney(mChargeAmount);

        tPayTradeInfo.setPayDate(mConfMakeDate + " " + mConfMakeTime);
        tPayTradeInfo.setPayEntAccDate(mConfMakeDate + " " + mConfMakeTime);
        tPayTradeInfo.setPayConfAccDate(mConfMakeDate + " " + mConfMakeTime);
        tPayTradeInfo.setDescBankAccCode(mChargeAccount);

        // 交易类型
        String tPayInfoType = "02";
        tPayTradeInfo.setPayInfoType(tPayInfoType);
        // --------------------

        tPayTradeInfo.setOperator(mGlobalInput.Operator);
        tPayTradeInfo.setMakeDate(mCurDate);
        tPayTradeInfo.setMakeTime(mCurTime);
        tPayTradeInfo.setModifyDate(mCurDate);
        tPayTradeInfo.setModifyTime(mCurTime);

        // 同步全局变量
        tMMap.put(tPayTradeInfo, SysConst.INSERT);
        mPayTradeTrack = tPayTradeInfo.getSchema(); // 创建副本
        // --------------------

        return tMMap;
    }

    private MMap crtTempFeeTrade()
    {
        MMap tMMap = new MMap();

        String tPayTradeNo = mFinanceNum;

        // 每个交易号只能处理一次        
        String tStrSql = "select 1 from LJTempfee where OtherNoType = '30' and OtherNo = '" + tPayTradeNo + "'";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            buildError("crtTempFeeTrade", "交易号已经缴费，缴费金额为：" + mChargeAmount + "；缴费批次为：" + mFinanceNum + "。");
            return null;
        }
        // --------------------
//by  gzh  20110801 增加总公司账户信息
        CertifyToGrpCont tCertifyToGrpCont = new CertifyToGrpCont();
        SSRS BankInfos = tCertifyToGrpCont.getBankAccCode();
        String ZBankManageCom = BankInfos.GetText(1, 1);//总公司机构
    	String ZBankAccNo = BankInfos.GetText(1, 2);
    	
        // 暂收明细数据
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();

        String tTempFeeNo = "HM" + tPayTradeNo;
        tLJTempFeeSchema.setTempFeeNo(tTempFeeNo);

        String tPayTempFeeType = "30";
        tLJTempFeeSchema.setTempFeeType(tPayTempFeeType);
        tLJTempFeeSchema.setRiskCode("000000");

        tLJTempFeeSchema.setOtherNo(tPayTradeNo);
        String tPayNoType = "30";
        tLJTempFeeSchema.setOtherNoType(tPayNoType);
        tLJTempFeeSchema.setManageCom(ZBankManageCom);
        tLJTempFeeSchema.setPolicyCom(mManageCom);
        
        //2014-7-19 灵活获取代理人编码
        String agSql = "select agentcode from laagent " +
        		"where managecom = '"+mManageCom+"' " +
        		"and agentcode like '%aaaaaa%' " +
        		"and  branchtype='2' and branchtype2='01' " +
        		"and insideflag='0' with ur";
        String aAgentCode = "";
        String aAentGroup = "";
        ExeSQL exe = new ExeSQL();
        SSRS agentSSRS = exe.execSQL(agSql);
        if(agentSSRS != null && agentSSRS.MaxRow > 0){
        	aAgentCode = agentSSRS.GetText(1, 1);
        }
        
        String groupSql = "select agentGroup from laagent where agentCode='"+aAgentCode+"'";
        SSRS groupSSRS = exe.execSQL(groupSql);
        if(groupSSRS != null && groupSSRS.MaxRow > 0){
        	aAentGroup = groupSSRS.GetText(1, 1);
        }
        
        tLJTempFeeSchema.setAgentCode(aAgentCode);
        tLJTempFeeSchema.setAgentGroup(aAentGroup);
        

        tLJTempFeeSchema.setPayMoney(mChargeAmount);
        tLJTempFeeSchema.setPayDate(mConfMakeDate);

        String serNo = PubFun1.CreateMaxNo("SERIALNO", mPayTradeTrack.getManageCom());
        tLJTempFeeSchema.setSerialNo(serNo);

        tLJTempFeeSchema.setConfFlag("1");
        tLJTempFeeSchema.setEnterAccDate(mConfMakeDate);
        tLJTempFeeSchema.setConfMakeDate(mConfMakeDate);
        tLJTempFeeSchema.setConfMakeTime(mConfMakeTime);
        tLJTempFeeSchema.setConfDate(mConfMakeDate);
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(mCurDate);
        tLJTempFeeSchema.setMakeTime(mCurTime);
        tLJTempFeeSchema.setModifyDate(mCurDate);
        tLJTempFeeSchema.setModifyTime(mCurTime);

        tMMap.put(tLJTempFeeSchema, SysConst.INSERT);
        // --------------------

        // 暂收汇总数据
        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();

        tLJTempFeeClassSchema.setTempFeeNo(tTempFeeNo);

        tLJTempFeeClassSchema.setPayMode("12");
        tLJTempFeeClassSchema.setPayDate(mConfMakeDate);
        tLJTempFeeClassSchema.setPayMoney(mChargeAmount);

        tLJTempFeeClassSchema.setManageCom(ZBankManageCom);
        tLJTempFeeClassSchema.setPolicyCom(mManageCom);

        tLJTempFeeClassSchema.setSerialNo(serNo);

        tLJTempFeeClassSchema.setConfFlag("1");
        tLJTempFeeClassSchema.setEnterAccDate(mConfMakeDate);
        tLJTempFeeClassSchema.setConfMakeDate(mConfMakeDate);
        tLJTempFeeClassSchema.setConfMakeTime(mConfMakeTime);
        tLJTempFeeClassSchema.setConfDate(mConfMakeDate);

        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(mCurDate);
        tLJTempFeeClassSchema.setMakeTime(mCurTime);
        tLJTempFeeClassSchema.setModifyDate(mCurDate);
        tLJTempFeeClassSchema.setModifyTime(mCurTime);
        
        tLJTempFeeClassSchema.setInsBankAccNo(ZBankAccNo);
        String aBankCode = tCertifyToGrpCont.getBankCode(ZBankAccNo);//获取银行账号对应的bankcode
        tLJTempFeeClassSchema.setBankCode(aBankCode);

        tMMap.put(tLJTempFeeClassSchema, SysConst.INSERT);
        // --------------------

        return tMMap;
    }

    /**
     * MD5加密
     * @param cStrKey
     * @return
     */
    private String encryptMD5(String cStrKey)
    {
        String tStrMd5 = null;
        try
        {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            BASE64Encoder base64en = new BASE64Encoder();
            tStrMd5 = base64en.encode(md5.digest(cStrKey.getBytes("GBK")));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("encryptMD5", "MD5生成失败。");
            return null;
        }
        return tStrMd5;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szErrMsg + " : " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
