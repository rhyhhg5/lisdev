package com.cbsws.xml.ctrl.blogic;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.SHCertPrintTable;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class SHWxSimplePrintBL extends ABusLogic{

	/** 报文信息集合*/
	private MsgCollection mcollection = new MsgCollection();
	
	/** 报文头信息*/
	private MsgHead mMsgHead = new MsgHead();
	
	/** 业务信息集合*/
	private List bodyList = null;
	
	/** 业务信息*/
	private SHCertPrintTable mCertPrintTable = new SHCertPrintTable();
	
	private String sendDate = null;
	
	/** 卡号*/
	private String cardNo = null;
	
	/** 产品编码*/
	private String wrapcode = null;
	
	/** 产品名称*/
	private String wrapname = null;

	
	/** 生效起期*/
	private String cvalidate=null;
	
	/** 终止日期*/
	private String cinvalidate =null;
	
	/** 档次*/
	private String mult=null;
	
	/** 份数*/
	private String coyps=null;
	
	
	/** 保障责任和保额的对应*/
	private Map resMap=null;
	
	/** 打印条形码*/
	private String barCode="";
	
	/** 特别约定*/
	private String special="";
	private XmlExport dXmlExport = new XmlExport();
	
	/** 报文信息集合类型*/
	private final static String insuInfo ="LCINSU";
	private final static String bnfInfo ="LCBNF";
	private final static String polInfo ="LCPOL";
	private final static String termInfo ="TERM";
	
	/** 条形码约定 单位为半角数字，如'张'为7个数字*/
	private final static int contNoLength=30;
	private final static int insuyearLength=32;
	private final static int insuIdnoLength=18;
	//private final static int dutyLength=266;
	private final static int barcodeLength=200;//单位为汉字
	
	
	private String contNoBar="";
	private String insuyearBar="";
	private String insuIdnoBar="";
	private String dutyBar="";
	private StringBuffer dutyremark = new StringBuffer();
	VData data = new VData();
	public boolean deal(MsgCollection cMsgInfos){
		mcollection = cMsgInfos;
		if(!load()){
			System.out.println("没有得到要打印的信息");
 	        errLog("没有得到要打印的信息");
			return false;
		}
		String msgType = this.mMsgHead.getMsgType();

		if(!check()){
			System.out.println("校验失败");
 	        errLog("校验失败");
			return false;
		}

		if(!jywxsubDeal()){
			System.out.println("业务处理异常");
 	        errLog("业务处理异常");
			return false;
		}
		
		dealPrintMag();
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, ""))
        {
            System.out.println("将打印信息保存到LOPRTManager表失败");
        }
		return true;
	}
	
	private boolean load(){
		mMsgHead = mcollection.getMsgHead();
		
		sendDate = this.mMsgHead.getSendDate();
		bodyList=mcollection.getBodyByFlag("SHCertPrintTable");
		if(bodyList ==null || bodyList.size()==0){
			System.out.println("没有得到要打印的信息");
  	         errLog("没有得到要打印的信息");
  	         return false;
		}
		return true ;
	}
	
	private boolean check(){
		return true;
	}
	
	/**
	 * 去哪儿网简易电子保单
	 * @return
	 */
	private boolean jywxsubDeal(){
		
		for (int i=0;i<bodyList.size();i++){
			this.mCertPrintTable = (SHCertPrintTable)bodyList.get(i);
			this.cardNo =  this.mCertPrintTable.getCardNo();
			this.special=  this.mCertPrintTable.getSpecialClause();
			if(this.cardNo!=null && !this.cardNo.equals("")){
				
				//保单中的单证编号和名称和产品代码
				String wrapSql = "select a.wrapCode from LICertSalesPayInfo a,lccont b where a.relainfolist = b.prtno and b.contno='"+this.cardNo+"'";
				SSRS contSSRS =  new ExeSQL().execSQL(wrapSql);
				if(contSSRS == null || contSSRS.MaxRow!=1){
					System.out.println("没有得到保单信息");
		  	         errLog("没有得到保单信息");
		  	         return false;
				}
				//this.certifycode = contSSRS.GetText(1, 1);
				//this.certifyname = contSSRS.GetText(1, 2);
				this.wrapcode =  contSSRS.GetText(1, 1);
				String wrapnameSQL = "select Wrapname from ldwrap where riskwrapcode='"+this.wrapcode+"'";
				this.wrapname = new ExeSQL().getOneValue(wrapnameSQL);
				
				String specialSQL="select remark from ldwrap where riskwrapcode='"+this.wrapcode+"'";
				String speContent = new ExeSQL().getOneValue(specialSQL);
				if(!speContent.equals("") && speContent !=null){
					this.special=speContent;
				}
				//保险卡信息
				String insuyearSQL = "select cvalidate,cinvalidate, amnt from lccont where contno='"+this.cardNo+"'";
				SSRS cardSSRS =  new ExeSQL().execSQL(insuyearSQL);
				if(cardSSRS == null || cardSSRS.MaxRow!=1){
					System.out.println("没有得到保险卡信息");
		  	         errLog("没有得到保险卡信息");
		  	         return false;
				}
			
				this.cvalidate = cardSSRS.GetText(1, 1);
				this.cinvalidate = cardSSRS.GetText(1, 2);	

				//投保人信息
				String appntSQL = "select appntName,AppntSex,appntBirthday,idtype,idno from lcappnt where contno='"+this.cardNo+"'";
				SSRS appntSSRS = new ExeSQL().execSQL(appntSQL.toString());
				if(appntSSRS==null || appntSSRS.MaxRow==0){
					System.out.println("没有得到投保人信息");
		  	         errLog("没有得到投保人信息");
		  	         return false;
				}
											
				//被保人信息
				String insuSql = "select  insu.name,insu.sex,insu.birthday,insu.idtype,insu.idno, " +
						"db2inst1.codename('relation',insu.relationToAppnt) from  lcinsured insu where insu.contno ='"+this.cardNo+"' ";
				SSRS insuSSRS = new ExeSQL().execSQL(insuSql);
				if(insuSSRS==null || insuSSRS.MaxRow==0){
					System.out.println("没有得到被保人信息");
		  	         errLog("没有得到被保人信息");
		  	         return false;
				}
				
				//受立益人信息
				String bnfSql = "select name ,sex,birthday,idtype,idno, " +
						"db2inst1.codename('relation',relationToInsured) " +
						" from lcbnf where contno='"+this.cardNo+"' ";
				SSRS bnfSSRS = new ExeSQL().execSQL(bnfSql);
				
				//保险责任
				this.resMap =calculateAmnt();
				String dutySQL="select lm.dutycode,lm.dutyname,lm.outdutyname " +
						"from ldriskdutywrap ld inner join lmduty lm on lm.dutycode = ld.dutycode " +
						"where calfactor = 'Amnt' and riskwrapcode ='"+this.wrapcode+"'";
				SSRS dutySSRS = new ExeSQL().execSQL(dutySQL);
				if(dutySSRS==null || dutySSRS.MaxRow==0){
					System.out.println("没有得到保险责任信息");
		  	         errLog("没有得到保险责任信息");
		  	         return false;
				}
				//适用条款
				String termSQL="select riskcode,riskname from lmrisk " +
						"where riskcode in (select distinct riskcode " +
					"from ldriskdutywrap  where riskwrapcode='"+this.wrapcode+"')";
				SSRS termSSRS = new ExeSQL().execSQL(termSQL);
				if(termSSRS==null || termSSRS.MaxRow==0){
					System.out.println("没有得到适用条款信息");
		  	         errLog("没有得到适用条款信息");
		  	         return false;
				}

				for(int j=1;j<=insuSSRS.MaxRow;j++){
					String insuIdno =insuSSRS.GetText(j, 5);
					this.insuIdnoBar +=insuIdno;
				}
				
				if(this.dutyremark.toString().equals("") || this.dutyremark.toString() ==null){
					System.out.println("没有得到适用条款信息");
		  	         errLog("没有得到适用条款信息");
		  	         return false;
				}
				this.contNoBar = this.cardNo;	
				this.insuyearBar = this.cvalidate+this.cinvalidate;
				this.dutyBar=this.dutyremark.toString();
				
				this.barCode=this.contNoBar+this.insuyearBar+this.insuIdnoBar+this.dutyBar;

				System.out.println("barcode=="+this.barCode);
				
				//XmlExport dXmlExport = new XmlExport();
				dXmlExport.createDocument("indigo", "");
				TextTag tTextTag = new TextTag();
				tTextTag.add("JETFORMTYPE", "WXP_001");
				tTextTag.add("MANAGECOMLENGTH4", "8600");
				tTextTag.add("USERIP", "127_0_0_1");
				tTextTag.add("PREVIEWFLAG", "1");
				tTextTag.add("CVALIDATE", cvalidate);
				tTextTag.add("CINVALIDATE", cinvalidate);
				tTextTag.add("CERTIFYNAME", this.wrapname);
				tTextTag.add("CARDNO", this.cardNo);
				tTextTag.add("WRAPCODE", this.wrapcode);
				tTextTag.add("APPNTNAME", appntSSRS.GetText(1, 1));
				tTextTag.add("APPNTSEX", appntSSRS.GetText(1, 2));
				tTextTag.add("APPNTBIRTHDAY", appntSSRS.GetText(1, 3));
				tTextTag.add("APPNTIDTYPE", appntSSRS.GetText(1, 4));
				tTextTag.add("APPNTIDNO", appntSSRS.GetText(1, 5));
				tTextTag.add("BARCODE", this.barCode);
				tTextTag.add("LCSPEC", this.special);
				dXmlExport.addTextTag(tTextTag);
				dealCardListInfo(dXmlExport,insuSSRS,this.insuInfo);
				if(bnfSSRS==null || bnfSSRS.MaxRow==0){
					System.out.println("没有得到受立益人信息");
				}else{
					dealCardListInfo(dXmlExport,bnfSSRS,this.bnfInfo);
				}
				dealCardListInfo(dXmlExport,dutySSRS,this.polInfo);
//				dealCardListInfo(dXmlExport,termSSRS,this.termInfo);
//				String xmlDoc =dXmlExport.outputString();
//				System.out.println("doc=="+xmlDoc);
				if(!FTPSendFile()){
					System.out.println("上传FTP失败");
		  	         errLog("上传FTP失败");
		  	         return false;
				}
			}else{
				System.out.println("没有得到卡号");
	  	         errLog("没有得到卡号");
	  	         return false;
			}			
		}
		return true;
	
	}
	/**
	 * 生成XML
	 * @param cXmlExport
	 * @param data
	 * @param type
	 * @return
	 */
	private boolean dealCardListInfo(XmlExport cXmlExport,SSRS data,String type)
    {
		if(type.equals(this.insuInfo)){
			String[] tCertifyListInfoTitle = new String[7];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "NAME";
	        tCertifyListInfoTitle[2] = "SEX";
	        tCertifyListInfoTitle[3] = "BIRTHDAY";
	        tCertifyListInfoTitle[4] = "IDTYPE";
	        tCertifyListInfoTitle[5] = "IDNO";
	        tCertifyListInfoTitle[6] = "TOAPPNTRELA";
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[7];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = tTmpCardInfo[0];
	            oneCardInfo[2] = tTmpCardInfo[1];
	            oneCardInfo[3] = tTmpCardInfo[2];
	            oneCardInfo[4] = tTmpCardInfo[3];
	            oneCardInfo[5] = tTmpCardInfo[4];
	            oneCardInfo[6] = tTmpCardInfo[5];
	            tListTable.add(oneCardInfo);
	        }
	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);  
		}
		if(type.equals(this.bnfInfo)){
			String[] tCertifyListInfoTitle = new String[7];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "NAME";
	        tCertifyListInfoTitle[2] = "SEX";
	        tCertifyListInfoTitle[3] = "BIRTHDAY";
	        tCertifyListInfoTitle[4] = "IDTYPE";
	        tCertifyListInfoTitle[5] = "IDNO";
	        tCertifyListInfoTitle[6] = "TOINSURELA";
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[7];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = tTmpCardInfo[0];
	            oneCardInfo[2] = tTmpCardInfo[1];
	            oneCardInfo[3] = tTmpCardInfo[2];
	            oneCardInfo[4] = tTmpCardInfo[3];
	            oneCardInfo[5] = tTmpCardInfo[4];
	            oneCardInfo[6] = tTmpCardInfo[5];
	            tListTable.add(oneCardInfo);
	        }	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
		if(type.equals(this.polInfo)){
			String[] tCertifyListInfoTitle = new String[5];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "WRAPCODE";
	        tCertifyListInfoTitle[2] = "DUTYCODE";
	        tCertifyListInfoTitle[3] = "DUTYNAME";
	        tCertifyListInfoTitle[4] = "AMNT";
	        
	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[5];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = this.wrapcode;
	            oneCardInfo[2] = tTmpCardInfo[0];
	            //by gzh 20110321 当产品描述中存在outdutyname时，报文中发送outdutyname，否则发送dutyname，打印结果便于客户明白
	            if(tTmpCardInfo[2]!= null && !"".equals(tTmpCardInfo[2])){
	            	oneCardInfo[3] = tTmpCardInfo[2]; //outdutyname
	            }else{
	            	oneCardInfo[3] = tTmpCardInfo[1]; //dutyname
	            }
	            oneCardInfo[4] = (String)this.resMap.get(tTmpCardInfo[1]);

	            tListTable.add(oneCardInfo);
	        }
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
		}
		if(type.equals(this.termInfo)){
			String[] tCertifyListInfoTitle = new String[3];
			tCertifyListInfoTitle[0] = "ORDER";
	        tCertifyListInfoTitle[1] = "RISKCODE";
	        tCertifyListInfoTitle[2] = "RISKNAME";

	        ListTable tListTable = new ListTable();
	        tListTable.setName(type);
	
	        String[] oneCardInfo = null;
	
	        for (int i = 1; i <= data.MaxRow; i++)
	        {
	            String[] tTmpCardInfo = data.getRowData(i);
	
	            oneCardInfo = new String[3];
	            oneCardInfo[0] = String.valueOf(i);
	            oneCardInfo[1] = tTmpCardInfo[0];
	            oneCardInfo[2] = tTmpCardInfo[1];
	            tListTable.add(oneCardInfo);
	        }
	
	        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);
        
		}
        return true;
    }
	
	/**
	 * 保险责任
	 * @return
	 */
	private Map calculateAmnt(){
		String tTmpCalResult = null;
		Map dutymap = new HashMap();
		
		String dutyname =null;
		String calfactortype =null;
		String calfactorvalue =null;
		String calsql =null;
		
		String dutySQL="select lm.dutyname,ld.calfactortype,ld.calfactorvalue, ld.calsql from ldriskdutywrap ld"
								+" inner join lmduty lm on lm.dutycode = ld.dutycode"
								+" where calfactor = 'Amnt' and riskwrapcode = '"+this.wrapcode+"'";
		
		String amntSql = "select lcc.amnt, "+ 
			" lcc.payintv, "+ 
			" lc.payendyear,lc.payendyearflag "+ 
			" from lcduty lc "+ 
			" inner join ldriskdutywrap ld on ld.dutycode = lc.dutycode "+ 
			" inner join lccont lcc on lc.contno = lcc.contno "+ 
			" where 1=1 and lcc.contno = '"+this.cardNo+"' "+ 
			" and ld.riskwrapcode = '"+this.wrapcode+"' and ld.calfactor = 'Amnt'";
		SSRS amntSSRS = new ExeSQL().execSQL(amntSql);
		
		try{
			
			SSRS dutySSRS = new ExeSQL().execSQL(dutySQL);
			if(dutySSRS==null || dutySSRS.MaxRow==0){
				System.out.println("计算保额失败");
				return null;
			}
			
			for(int i=1;i<=dutySSRS.MaxRow;i++){
				dutyname = dutySSRS.GetText(i, 1);
				calfactortype = dutySSRS.GetText(i, 2);
				calfactorvalue = dutySSRS.GetText(i, 3);
				calsql = dutySSRS.GetText(i, 4);
				if ("2".equals(calfactortype))
		        {
		            /** 准备要素 calbase */
		            PubCalculator tCal = new PubCalculator();

		            tCal.addBasicFactor("RiskCode", this.wrapcode);
		            tCal.addBasicFactor("Copys", String.valueOf(this.coyps));
		            tCal.addBasicFactor("Mult", String.valueOf(this.mult));
		            tCal.addBasicFactor("Amnt", amntSSRS.GetText(1, 1));
		            tCal.addBasicFactor("PayEndYear", amntSSRS.GetText(1, 3));
		            tCal.addBasicFactor("PayEndYearFlag", amntSSRS.GetText(1, 4));
		            tCal.addBasicFactor("PayIntv", amntSSRS.GetText(1, 2));

		            /** 计算 */
		            tCal.setCalSql(calsql);
		            System.out.println("CalSql:" + calsql);

		            tTmpCalResult = tCal.calculate();

		            if (tTmpCalResult == null || tTmpCalResult.equals(""))
		            {
		                String tStrErr = "单证号[" + this.cardNo + "]保额计算失败。";               
		                System.out.println(tStrErr);
		                return null;
		                // tTmpCalResult = "0";
		            }
		        }
		        else
		        {
		            tTmpCalResult = calfactorvalue;
		        }			
				dutymap.put(dutyname, tTmpCalResult);
				dutyremark.append(dutyname + tTmpCalResult);
			}
	        
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
        return dutymap;
	}
	
	/**
	 * 上传XML到FTP服务器
	 * 
	 * @return
	 */
	private boolean FTPSendFile(){
	      
	    /**文件*/
	    String fileName=this.cardNo;
	    String filePath="";
	    
	    try{
	    	String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PDFstrUrl'";//生成文件的存放路径
	    	filePath = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
	        System.out.println("生成文件的存放路径   "+filePath);//调试用－－－－－
	        if(filePath == null ||filePath.equals("")){
	            System.out.println("获取文件存放路径出错");
	            return false;
	        }
	        //filePath="D:\\quaer\\";
		    String xmlDoc=this.dXmlExport.outputString();
		    System.out.println("doc=="+xmlDoc);
	        this.dXmlExport.outputDocumentToFile(filePath, fileName,"UTF-8");
		    //上传文件到FTP服务器
		    	if (!sendZip(filePath+fileName+".xml")) {
		    		System.out.println("上传文件到FTP服务器失败！");
		            return false;
		        } else {
		            //上传成功后删除
		        	System.out.println("上传文件到FTP服务器成功！");
		        	File xmlFile = new File(filePath+fileName+".xml");
		        	xmlFile.delete(); //删除上传完的文件
		        }		    
	    }catch(Exception e){
	    	e.printStackTrace();
	    	return false;
	    }
		return true;
	}
	/**
     * sendZip
     * 上传文件至指定FTP（清单文件和单打的数据文件在同一个目录）
     * @param string String
     * @param flag String
     * @return boolean
     */
    private boolean sendZip(String cFileUrlName) {
        //登入前置机
         String ip = "";
         String user = "";
         String pwd = "";
         int port = 21;//默认端口为21

         String sql = "select code,codename from LDCODE where codetype='printserver'";
         SSRS tSSRS = new ExeSQL().execSQL(sql);
         if(tSSRS != null){
             for(int m=1;m<=tSSRS.getMaxRow();m++){
                 if(tSSRS.GetText(m,1).equals("IP")){
                     ip = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("UserName")){
                     user = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Password")){
                     pwd = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Port")){
                     port = Integer.parseInt(tSSRS.GetText(m,2));
                 }
             }
         }
//         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port,"ActiveMode");
         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port);
//         FTPTool tFTPTool = new FTPTool("10.252.130.228", "zhangshuo ", "111111", 21);
         try
         {
             if (!tFTPTool.loginFTP())
             {
                 System.out.println(tFTPTool.getErrContent(1));
                 return false;
             }
             else
             {
                  if (!tFTPTool.upload(cFileUrlName)) {
                         CError tError = new CError();
                         tError.errorMessage = tFTPTool
                                               .getErrContent(
                                 FTPReplyCodeName.LANGUSGE_CHINESE);
                         
                         System.out.println("上载文件失败!");
                         return false;
                  }

                 tFTPTool.logoutFTP();
             }
         }
         catch (SocketException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                      .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
              
             System.out.println("上载文件失败，可能是网络异常!");
             return false;
         }
         catch (IOException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                     .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
             
             System.out.println("上载文件失败，可能是无法写入文件");
             return false;
         }

         return true;

    }
	public static void main(String args[]){
		ECPrintBL ecp = new ECPrintBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo("123456789");
		tMsgHead.setBranchCode("001");
		tMsgHead.setMsgType("INDIGO");
		tMsgHead.setSendDate("2010-9-21");
		tMsgHead.setSendTime("09:00:00");
		tMsgHead.setSendOperator("sino");
		tMsgCollection.setMsgHead(tMsgHead);
		SHCertPrintTable tCertPrintTable = new SHCertPrintTable();
		tCertPrintTable.setCardNo("EA007206559");
		tMsgCollection.setBodyByFlag("SHCertPrintTable", tCertPrintTable);
		ecp.deal(tMsgCollection);	
		
	}
	/**
     * 将字符串补数,将sourString的<br>后面</br>用cChar补足cLen长度的字符串,如果字符串超长，则不做处理
     * <p><b>Example: </b><p>
     * <p>RCh("Minim", "0", 10) returns "Minim00000"<p>
     * @param sourString 源字符串
     * @param cChar 补数用的字符
     * @param cLen 字符串的目标长度
     * @return 字符串
     */
    private String RCh(String sourString, String cChar, int cLen) {
        int tLen = sourString.getBytes().length;
        int i, j, iMax;
        String tReturn = "";
        if (tLen >= cLen) {
            return sourString;
        }
        iMax = cLen - tLen;
        for (i = 0; i < iMax; i++) {
            tReturn = tReturn + cChar;
        }
        tReturn = sourString + tReturn;
        return tReturn;
    }
    
    private String createBarcode(String pram,int pramlen){
    	String result=null;
    	int len =pram.getBytes().length;
    	SHWxSimplePrintBL ecp = new SHWxSimplePrintBL();
    	result=ecp.RCh(pram, " ", pramlen);
    	System.out.println("result===="+result.getBytes().length);
    	return result;
    }
    /**
     * 计算条形码长度
     * @param barStr
     * @return
     */
    public int CalculateBarCodeLen(String barStr){
    	  	
    	int strLen=0;
    	char [] barArr = barStr.toCharArray();
    	for (int i=0;i<barArr.length;i++){
    		String tempStr = String.valueOf(barArr[i]);
    		if(tempStr.getBytes().length ==2){
    			strLen+=7;
    		}else{
    			strLen+=1;
    		}
    	}
    	return strLen;
    }
    
    /**
     * 去掉年份中月和日前面的零
     * @return
     */
    public String DealDateZero(String dateStr){
    	String result=null;
    	String year =null;
    	String month =null;
    	String date =null;
    	   	
    	if(dateStr.equals("") || dateStr == null){
    		System.out.println("没有得到有效的字符类型的日期");
    		return null;
    	}
    	year = dateStr.substring(0, 4);
    	month = dateStr.substring(5, 7);
    	date = dateStr.substring(8, 10);
    	
    	if(month.substring(0,1).equals("0")){
    		month= month.substring(1);
    	}
    	if(date.substring(0,1).equals("0")){
    		date= date.substring(1);
    	}
    	result = year+"-"+month+"-"+date;
    	return result;
    }
    
    //gzh 20110118
    private boolean dealPrintMag() {
    	
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String tLimit = PubFun.getNoLimit("86000000");
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        tLOPRTManagerSchema.setOtherNo(cardNo); //存放卡号
        tLOPRTManagerSchema.setOtherNoType("02");
        tLOPRTManagerSchema.setCode("WX01");
        tLOPRTManagerSchema.setManageCom("86000000");
        tLOPRTManagerSchema.setAgentCode("aaaa");
        tLOPRTManagerSchema.setReqCom("86000000");
        tLOPRTManagerSchema.setReqOperator("001");
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("1");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        MMap tMMap = new MMap();
        tMMap.put(tLOPRTManagerSchema, "INSERT");
        data.add(tMMap);
        return true;
    }
//  by gzh 20110302 获取date前一天的日期
    public String getPreviousDate(Date date){ 
    	Calendar calendar=Calendar.getInstance(); 
    	FDate tFDate = new FDate();
    	calendar.setTime(date); 
    	int day=calendar.get(Calendar.DATE); 
    	calendar.set(Calendar.DATE,day-1); 
    	return tFDate.getString(calendar.getTime()); 
    	} 

}
