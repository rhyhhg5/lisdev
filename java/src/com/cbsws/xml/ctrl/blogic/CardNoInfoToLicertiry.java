package com.cbsws.xml.ctrl.blogic;

import java.math.BigDecimal;

import com.sinosoft.lis.certifybusiness.CertifyContConst;
import com.sinosoft.lis.certifybusiness.CertifyDiskImportLog;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class CardNoInfoToLicertiry {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    /** 往前面传输数据的容器 */
	private VData mVData= new VData();
	
	/**传输到后台处理的map*/
	private MMap mMap = new MMap();
	
	/** 数据操作字符串 */
	private String tCertifyCode ="";
	private double tCopys = 0;
    private double tMult =  0;
    private String cardno=null;
	private LICertifySchema mLICertifySchema = new LICertifySchema();
	private LICertifySet mLICertifySet = new LICertifySet();
	private LZCardSchema mLZCardSchema = new LZCardSchema();
	private LZCardSet mLZCardSet =new LZCardSet();
	private CertifyDiskImportLog mImportLog = null;
	 
	 public boolean dealSubmitMap(VData cInputData)
	    {
	        if (!getInputData(cInputData))
	        {
	            return false;
	        }

	        if (!checkData())
	        {
	            return false;
	        }

	        if (!dealData())
	        {
	            return false;
	        }

	        return true;
	    }
	 
	 private boolean getInputData(VData cInputData)
	    {
	        mLICertifySchema = (LICertifySchema) cInputData.getObjectByObjectName(
	                "LICertifySchema", 0);

	        if (mLICertifySchema == null)
	        {
	            buildError("getInputData", " 没有所需参数。");
	            return false;
	        }
	        return true;
	    }

	    private boolean checkData()
	    { 
	    	//对传入的卡号进行校验
	    	if(mLICertifySchema.getCardNo()==null || mLICertifySchema.getCardNo().equals("")){
	    		buildError("checkData", " 其中一个卡号为空。");
	            return false;
	    	}else{
	    		cardno=mLICertifySchema.getCardNo();
	    		try{
	    			String cardnosql="select 1 from licertify where cardno ='"+cardno+"'";
	    			String existflag =new ExeSQL().getOneValue(cardnosql);
	    			if(existflag.equals("1")){
	    				buildError("checkData", " 卡号已经在LIcertify表中存在。"); 
	    	            return false;
	    			}
	    		}catch(Exception e){
	    			e.printStackTrace();
	    			return false;
	    		}
	    	}
	    	
	    	//对档次和份数进行校验
	    	try{ 
	    		tCertifyCode = mLICertifySchema.getCertifyCode();
		    	String sql ="select distinct calfactor," 
	    					+"case calfactortype when '1' then calfactortype else '' end calfactortype "
							+"from ldriskdutywrap " 
							+"where 1 = 1 " 
							+"and riskwrapcode = (select riskcode from lmcardrisk where certifycode='"+tCertifyCode+"')" 
							+" and calfactor in ('Mult', 'Copys') " ;
		    	SSRS eSSRS = new SSRS();
		    	eSSRS = new ExeSQL().execSQL(sql);
		    	for(int i=1;i<=eSSRS.getMaxRow();i++){
		    		if(eSSRS.GetText(i, 1).equals("Copys")&& eSSRS.GetText(i, 2).equals("")){
		    			System.out.println("卡号"+this.cardno+"对应的险种的"+eSSRS.GetText(i, 1)+"有误。");
		    			buildError("checkData", " 卡号对应的险种的"+eSSRS.GetText(i, 1)+"有误。");
			            return false;
		    		}	
		    		if(eSSRS.GetText(i, 1).equals("Mult")&& eSSRS.GetText(i, 2).equals("")){
		    			System.out.println("卡号"+this.cardno+"对应的险种的"+eSSRS.GetText(i, 1)+"有误。");
		    			buildError("checkData", " 卡号对应的险种的"+eSSRS.GetText(i, 1)+"有误。");
			            return false;
		    		}	
		    	}
		    	if(eSSRS.GetText(1, 2)!=null && !eSSRS.GetText(1, 2).equals("")){
		    		tCopys = Double.valueOf(eSSRS.GetText(1, 2).toString()).doubleValue();
		    	}
		    	if(eSSRS.GetText(2, 2)!=null && !eSSRS.GetText(2, 2).equals("")){
		    		tMult =  Double.valueOf(eSSRS.GetText(2, 2).toString()).doubleValue();
		    	}
		        
		        
		      //校验卡单状态
		    	String cardno  =mLICertifySchema.getCardNo();
		    	String sql2="select 1 from LZCardNumber lzcn "
		    				+"inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo"
		    				+" where 1 = 1 	and lzcn.CardNo = '"+cardno+"' and lzc.State in ('10' ,'11')";
		    	String val =""; 
		    	val = new ExeSQL().getOneValue(sql2);
		    	if(!val.equals("1")){
		    		buildError("checkData", " 卡号对应的状态有误，不是10或11。");
		            return false;
		    	}
		    	
		    	
	    	}catch(Exception e){
	    		e.printStackTrace();
	    		return false;
	    	}
	    	return true;
	    }

	    private boolean dealData()
	    {	        
	        	try{
		        	LICertifySchema tLICertifySchema = new LICertifySchema();
		        	
		        	// 处理激活卡要素：保额、保费、档次、份数等。（主要为了重算保额）
		            if (!dealCertifyWrapParams(mLICertifySchema))
		            {
		                return false;
		            }
		            tLICertifySchema = (LICertifySchema)mLICertifySchema.clone();
		        	tLICertifySchema.setSaleChnl("15");//销售机构定为15
		        	tLICertifySchema.setCopys(Double.valueOf("1").doubleValue());
		        	tLICertifySchema.setMult(this.tMult);
		        	tLICertifySchema.setState("01");
		        	tLICertifySchema.setActiveFlag(CertifyContConst.CC_IMPORT_FIRST);
		        	tLICertifySchema.setWSState("00");
		        	tLICertifySchema.setCertifyStatus("01");
		        	tLICertifySchema.setMakeDate(PubFun.getCurrentDate());
		        	tLICertifySchema.setMakeTime(PubFun.getCurrentTime());
		        	tLICertifySchema.setModifyDate(PubFun.getCurrentDate());
		        	tLICertifySchema.setModifyTime(PubFun.getCurrentTime());
		        	mLICertifySet.add(tLICertifySchema);		      
		        	
		        	//回置
			    	String tStrSql = " select lzc.* from LZCard lzc "
		                + " inner join LZCardNumber lzcn on lzc.SubCode = lzcn.CardType and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
		                + " and lzc.State in ('10', '11') "
		                + " and lzcn.CardNo = '" + mLICertifySchema.getCardNo()
		                + "' ";
			    	LZCardSet tLZCardSet = new LZCardDB().executeQuery(tStrSql);
			        if (tLZCardSet == null || tLZCardSet.size() != 1)
			        {
			            String tStrErr = "回置单证预售状态失败。";
			            if (!mImportLog.errLog(mLICertifySchema.getCardNo(), tStrErr))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
			        }
			        mLZCardSchema = tLZCardSet.get(1);
			        mLZCardSchema.setState("13");
			        mLZCardSet.add(mLZCardSchema);
	        	}catch(Exception e){
	        		e.printStackTrace();
	        		return false;
	        	}        

	        return true;
	    }
	

	/**
     * 处理单证关键要素（保额、保费、档次、份数等）。
     * @param cLICertifySchema
     * @return
     */
    private boolean dealCertifyWrapParams(LICertifySchema cLICertifySchema)
    {
        String tCardNo = cLICertifySchema.getCardNo();

        String tCertifyCode = cLICertifySchema.getCertifyCode();
        //double tAmnt = cLICertifySchema.getAmnt();//??
        //double tPrem = cLICertifySchema.getPrem();//??
        //double tCopys = cLICertifySchema.getCopys();
        //double tMult = cLICertifySchema.getMult();//??

        // 处理Copys要素。如果套餐中无Copys要素，清单中份数要素必须为1。
        if (cLICertifySchema.getCopys() == 0)
        {
            tCopys = 1;
            cLICertifySchema.setCopys(tCopys);
        }

        String tCopysFlag = new ExeSQL()
                .getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                        + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                        + " where ldrdw.CalFactor = 'Copys' and lmcr.CertifyCode = '"
                        + tCertifyCode + "' ");
        if (!"1".equals(tCopysFlag) && tCopys != 1)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐份数不能大于1。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        // -------------------------------------------

        // 校验档次，但不对POS单证进行档次的对应校验
        String tMultFlag = new ExeSQL()
                .getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                        + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                        + " where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '"
                        + tCertifyCode
                        + "' "
                        + "union all "
                        + " select 1 from LMCertifyDes lmcd where lmcd.CertifyCode = '"
                        + tCertifyCode + "' and lmcd.OperateType = '3'");
        if (!"1".equals(tMultFlag) && tMult != 0)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐不含有档次要素。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        // -------------------------------------------

        LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;
        //查询单证对应的套餐要素
        String tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
                + " where lmcr.CertifyCode = '"
                + tCertifyCode
                + "' "
                + " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
        tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐要素信息未找到。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        double tSysWrapSumPrem = 0;
        double tSysWrapSumAmnt = 0;

        // 处理要素信息。
        for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++)
        {
            LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);

            String tCalFactorType = tRiskDutyParam.getCalFactorType();//计划要素类型
            String tCalFactor = tRiskDutyParam.getCalFactor();//计划要素
            String tCalFactorValue = tRiskDutyParam.getCalFactorValue();//计划要素值

            if ("Prem".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam
                            .getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(tMult));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + tCardNo + "]保费计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        System.out.println(tStrErr);

                        if (!mImportLog.errLog(tCardNo, tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumPrem = Arith.add(tSysWrapSumPrem,
                            new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保费出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }

            if ("Amnt".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam
                            .getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(tMult));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + tCardNo + "]保额计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        System.out.println(tStrErr);

                        if (!mImportLog.errLog(tCardNo, tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                        // tTmpCalResult = "0";
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt,
                            new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保额出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }
        }
        // ------------------------------------------------
        //校验保费保额是否与传入值一到致
//        if (tPrem != 0 && tPrem != tSysWrapSumPrem)
//        {
//            String tStrErr = "单证号[" + tCardNo + "]系统计算出保费：" + tSysWrapSumPrem
//                    + "，与填写保费：" + tPrem + "不一致。";
//            buildError("dealCertifyWrapParams", tStrErr);
//            System.out.println(tStrErr);
//
//            if (!mImportLog.errLog(tCardNo, tStrErr))
//            {
//                mErrors.copyAllErrors(mImportLog.mErrors);
//                return false;
//            }
//            return false;
//        }
        mLICertifySchema.setPrem(tSysWrapSumPrem);

//        if (tAmnt != 0 && tAmnt != tSysWrapSumAmnt)
//        {
//            String tStrErr = "单证号[" + tCardNo + "]系统计算出保额：" + tSysWrapSumAmnt
//                    + "，与填写保额：" + tAmnt + "不一致。";
//            buildError("dealCertifyWrapParams", tStrErr);
//            System.out.println(tStrErr);
//
//            if (!mImportLog.errLog(tCardNo, tStrErr))
//            {
//                mErrors.copyAllErrors(mImportLog.mErrors);
//                return false;
//            }
//            return false;
//        }
        mLICertifySchema.setAmnt(tSysWrapSumAmnt);
        return true;
    }

    public MMap getResult(){
    	mMap.put(mLICertifySet, SysConst.INSERT);
    	mMap.put(mLZCardSet, SysConst.UPDATE);
    	return mMap;
    }
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CardNoInfoToLicertiry";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
