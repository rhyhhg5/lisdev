/**
 * 2011-6-3
 */
package com.cbsws.xml.ctrl.blogic;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class HMCertPayTradeUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public HMCertPayTradeUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            HMCertPayTradeBL tLogicBL = new HMCertPayTradeBL();
            if (!tLogicBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tLogicBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
