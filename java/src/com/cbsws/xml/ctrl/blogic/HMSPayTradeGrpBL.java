package com.cbsws.xml.ctrl.blogic;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author zhanggm 20110927
 *
 */
public class HMSPayTradeGrpBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 系统处理当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 系统处理当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    
    private Reflections ref = new Reflections();

    /** 接口识别码 */
    private String mGrpContNo = null;
    
    private String mManageCom = null;
    
    private String mAppntNo = null;
    
    private String mMoney = null;
    
    private String mAgentCode = null;
    
    private String mOperator = null;

    public HMSPayTradeGrpBL()
    {
    }

    public boolean submitData(VData cInputData, String type)
    {
//    	接受业务类型
//   	 type = 01 - 查询是否收费成功 如果没收费 则 删除应收 生成应收
//   	 type = 02 - 查询是否收费成功 如果已收费 则 删除应收
//   	 type = 03 - 查询是否收费成功 如果没收费 则 删除应收
    	if (!getInputData(cInputData, type))
        {
            return false;
        }
    	
    	if(type.equals("01"))
    	{
            if(!deal01())
        	{
            	buildError("deal01", "生成应收记录失败");
        		return false;
        	}
            else
            {
            	buildError("deal01", "生成应收记录成功");
            }
    	}
    	else if(type.equals("02"))
    	{
    		if(!deal02())
        	{
    			buildError("deal02", "尚未缴费");
        		return false;
        	}
    		else
            {
    			buildError("deal02", "收费成功");
            }
    	}
    	else if(type.equals("03"))
    	{
    		if(!deal03())
        	{
    			buildError("deal03", "删除应收失败");
        		return false;
        	}
    		else
    		{
    			buildError("deal03", "删除应收成功");
    		}
    	}

        if (!submit())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData, String type)
    {
    	if(type==null||type.equals("")||type.equals("null"))
    	{
            buildError("getInputData", "处理类型传入失败");
            return false;
    	}
    	
    	mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }
    	mGrpContNo = (String)mTransferData.getValueByName("GrpContNo");
    	mManageCom = (String)mTransferData.getValueByName("ManageCom");
    	mAppntNo = (String)mTransferData.getValueByName("AppntNo");
    	mMoney = (String)mTransferData.getValueByName("Money");
    	mAgentCode = (String)mTransferData.getValueByName("AgentCode");
    	mOperator = (String)mTransferData.getValueByName("Operator");
    	
    	if(mGrpContNo==null || mGrpContNo.equals("") || mGrpContNo.equals("null"))
    	{
    		System.out.println("没有传入团体合同号码");
    		buildError("getInputData", "没有传入团体合同号码");
            return false;
    	}
    	
    	if(type.equals("01"))
    	{
        	if(mManageCom==null || mManageCom.equals("") || mManageCom.equals("null"))
        	{
        		System.out.println("没有传入管理机构");
        		buildError("getInputData", "没有传入管理机构");
                return false;
        	}
        	if(mAppntNo==null || mAppntNo.equals("") || mAppntNo.equals("null"))
        	{
        		System.out.println("没有传入投保人号码");
        		buildError("getInputData", "没有传入投保人号码");
                return false;
        	}
        	if(mMoney==null || mMoney.equals("") || mMoney.equals("null"))
        	{
        		System.out.println("没有传入缴费金额");
        		buildError("getInputData", "没有传入缴费金额");
                return false;
        	}
//        	if(mAgentCode==null || mAgentCode.equals("") || mAgentCode.equals("null"))
//        	{
//        		System.out.println("没有传入代理人编码");
//        		buildError("getInputData", "没有传入代理人编码");
//                return false;
//        	}
    	}
    	
    	if(mOperator==null || mOperator.equals("") || mOperator.equals("null"))
    	{
    		mOperator = "HM_JYK";
    	}
    	
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            mGlobalInput = new GlobalInput();
            mGlobalInput.ComCode = mManageCom==null?"86":mManageCom;
            mGlobalInput.ManageCom = mManageCom==null?"86":mManageCom;
            mGlobalInput.Operator = mOperator;
        }
        return true;
    }

    private boolean deal01()
    {
//    	查询是否已缴费
    	if(queryTempFee())
    	{
			buildError("deal01", "已经缴费成功，不能修改合同。");
    		return false;
    	}
    	
//    	删除应收
    	if(!delleteLJSPay())
    	{
			buildError("deal01", "删除应收记录失败");
    		return false;
    	}

//    	生成新应收
    	if(!insertNewLJSPay())
    	{
			buildError("deal01", "生成应收记录失败");
    		return false;
    	}
        
        return true;
    }
    
    private boolean deal02()
    {
//    	查询是否已缴费
    	if(!queryTempFee())
    	{
			buildError("deal02", "没有缴费，不能签发合同。");
    		return false;
    	}
    	
//    	删除应收
    	if(!delleteLJSPay())
    	{
			buildError("deal02", "删除应收记录失败");
    		return false;
    	}

        return true;
    }
    
    private boolean deal03()
    {
//    	查询是否已缴费
    	if(queryTempFee())
    	{
			buildError("deal03", "已经缴费成功，不能撤销合同。");
    		return false;
    	}
    	
//    	删除应收
    	if(!delleteLJSPay())
    	{
			buildError("deal03", "删除应收记录失败");
    		return false;
    	}

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szErrMsg + " : " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
    
    /**
     * 锁定动作
     * @param polBalaCount
     * @return MMap
     */
    private boolean lockLJSPay(String aGrpContNo)
    {
        MMap tMMap = null;
        /**健管网销团体购买产生应收 "HG"*/
        String tLockNoType = "HG";
        /**锁定有效时间（秒）*/
        String tAIS = "5";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", aGrpContNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return false;
        }
        mMap.add(tMMap);
        return true;
    }
    
    /**
     * 内部使用 查询是否收费成功
     * @return true - 收费成功 ； false - 没收费
     */
    private boolean queryTempFee()
    {
    	LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
    	tLJTempFeeDB.setOtherNo(mGrpContNo);
    	tLJTempFeeDB.setOtherNoType("30");
    	tLJTempFeeDB.setTempFeeType("30");
    	LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
    	if(tLJTempFeeSet!=null && tLJTempFeeSet.size()>=1)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     * 删除应收-内部调用
     * @return true - 删除应收成功 ； false - 删除应收失败
     */
    private boolean delleteLJSPay()
    {
    	LJSPayDB tLJSPayDB = new LJSPayDB();
    	tLJSPayDB.setOtherNo(mGrpContNo);
    	tLJSPayDB.setOtherNoType("30");
    	LJSPaySet tLJSPaySet = new LJSPaySet();
    	tLJSPaySet = tLJSPayDB.query();
    	if(tLJSPaySet.size()>=1)
    	{
    		for(int i=1;i<=tLJSPaySet.size();i++)
    		{
    			LJSPaySchema delLJSPaySchema = new LJSPaySchema();
    			delLJSPaySchema.setSchema(tLJSPaySet.get(i).getSchema());
    			mMap.put(delLJSPaySchema, SysConst.DELETE);
    			
    			LJSPayBDB delLJSPayBDB = new LJSPayBDB();
    			delLJSPayBDB.setGetNoticeNo(delLJSPaySchema.getGetNoticeNo());
    			if(delLJSPayBDB.getInfo())
    			{
    				LJSPayBSchema delLJSPayBSchema = new LJSPayBSchema();
    				delLJSPayBSchema.setSchema(delLJSPayBDB.getSchema());
    				delLJSPayBSchema.setDealState("2");
    				delLJSPayBSchema.setOperator(mOperator);
    				delLJSPayBSchema.setModifyDate(mCurrentDate);
    				delLJSPayBSchema.setModifyTime(mCurrentTime);
    				mMap.put(delLJSPayBSchema, SysConst.UPDATE);
    			}
    		}
    	}
        return true;
    }
    
    /**
     * 生成新应收记录-内部调用
     * @return true - 删除应收成功 ； false - 删除应收失败
     */
    private boolean insertNewLJSPay()
    {
    	//插入新的收费信息
    	LJSPaySchema tLJSPaySchema = new LJSPaySchema();
    	String tLimit = PubFun.getNoLimit(mManageCom);
    	String aGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);
    	System.out.println("生成应收号：" + aGetNoticeNo);
    	String aPayDate = PubFun.calDate(mCurrentDate, 60, "D", null);
    	String aSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
    	String aAgentGroup = null;
    	if(mAgentCode!=null && !mAgentCode.equals("") && !mAgentCode.equals("null"))
    	{
    		LAAgentDB tLAAgentDB = new LAAgentDB();
        	tLAAgentDB.setAgentCode(mAgentCode);
        	if(tLAAgentDB.getInfo())
        	{
        		aAgentGroup = tLAAgentDB.getAgentGroup();
        	}
        	else
        	{
        		System.out.println("查询业务员机构失败，请确认业务员代码是否正确：" + mAgentCode);
        		buildError("dealData", "查询业务员机构失败，请确认业务员代码是否正确：" + mAgentCode);
        		return false;
        	}
    	}
    	
    	tLJSPaySchema.setGetNoticeNo(aGetNoticeNo);
    	tLJSPaySchema.setOtherNo(mGrpContNo);
    	tLJSPaySchema.setOtherNoType("30");
    	tLJSPaySchema.setAppntNo(mAppntNo);
    	tLJSPaySchema.setSumDuePayMoney(mMoney);
    	tLJSPaySchema.setStartPayDate(mCurrentDate);
    	tLJSPaySchema.setPayDate(aPayDate);
    	tLJSPaySchema.setApproveCode(mOperator);
    	tLJSPaySchema.setApproveDate(mCurrentDate);
    	tLJSPaySchema.setSerialNo(aSerialNo);
    	tLJSPaySchema.setManageCom(mManageCom);
    	tLJSPaySchema.setRiskCode("000000");
    	tLJSPaySchema.setAgentCode(mAgentCode);
    	tLJSPaySchema.setAgentGroup(aAgentGroup);
    	tLJSPaySchema.setMarketType("1");
    	tLJSPaySchema.setSaleChnl("01");
    	tLJSPaySchema.setOperator(mGlobalInput.Operator);
    	tLJSPaySchema.setMakeDate(mCurrentDate);
    	tLJSPaySchema.setMakeTime(mCurrentTime);
    	tLJSPaySchema.setModifyDate(mCurrentDate);
    	tLJSPaySchema.setModifyTime(mCurrentTime);
    	
    	LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
    	ref.transFields(tLJSPayBSchema, tLJSPaySchema);
    	tLJSPayBSchema.setDealState("1");
    	tLJSPayBSchema.setConfFlag("1");
    	
    	//锁定，避免重复提交
//    	if(!lockLJSPay(mGrpContNo))
//        {
//            return false;
//        }
        mMap.put(tLJSPaySchema, SysConst.INSERT);
        mMap.put(tLJSPayBSchema, SysConst.INSERT);
        return true;
    }
}
