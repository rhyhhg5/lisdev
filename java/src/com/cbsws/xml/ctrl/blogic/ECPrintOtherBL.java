package com.cbsws.xml.ctrl.blogic;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ECPrintOtherBL {
		
	/** 卡号*/
	private String cardNo = null;
	/** 被保人姓名*/
	private String insuname = null;
	/** 被保人证件号码 **/
	private String insuredidno =null;
	
	/** 保额*/
	private String amnt = null;
	/** 保费*/
	private String prem = null;
	/** 代理机构*/
	private String managecom = "";
	/** 经办人员*/
	private String operator = "";
	/** 生效日期和时间*/
	private String dateAndtime = null;
	
	/** 产品编码*/
	private String wrapcode = null;
	
	/** 产品名称*/
	private String wrapname = null;
	
	/** 保障期限*/
	private String insuyear = null;
	
	/** 保障期限标志*/
	private String insuyearflag =null;
	
	/** 生效起期*/
	private String cvalidate=null;
	
	/** 终止日期*/
	private String cinvalidate =null;
	
	/** 档次*/
	private String mult=null;
	
	/** 份数*/
	private String coyps=null;
	
	/** 保障责任和保额的对应*/
	private Map resMap=null;
	
	/** 打印条形码*/
	private String barCode="";
	
	/** 特别约定*/
	private String special="";
	
	private String error = "";
	
	private String msgType = "";
	
	private StringBuffer dutyremark = new StringBuffer();
	VData data = new VData();
	public String deal(String cardNo,String msgType){
		this.msgType = msgType;
		String sql = "select 1 from ldcode where codetype = 'ecprint' and code='"+this.msgType+"'";
		SSRS countSSRS = new ExeSQL().execSQL(sql);
		if(countSSRS.getMaxRow()!=1){
			return null;
		}
		if(!load(cardNo)){
			System.out.println("没有得到要打印的信息");
 	        error = errLog("没有得到要打印的信息");
			return error;
		}
		
		if(!check()){
			System.out.println("校验失败，对应卡号信息不正确");
 	        error = errLog("校验失败，对应卡号信息不正确");
			return error;
		}
		
		if(!subDeal()){
			System.out.println(error);
			return error;
		}
		if(!print()){
			System.out.println(error);
			return error;
		}
		dealPrintMag();
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, ""))
        {
            System.out.println("将打印信息保存到LOPRTManager表失败");
        }
		return null;
	}
	
	private boolean load(String cardNo){
		this.cardNo = cardNo;
		return true ;
	}
	
	private boolean check(){
    	String SQL = "select count(1) from licardactiveinfolist where cardno ='"+cardNo+"'";
    	String flag = new ExeSQL().getOneValue(SQL);
    	if(flag.equals("1")){
    		return true;
    	}else{
    		return false;
    	}
	}
	
	private boolean subDeal(){
		
			if(this.cardNo!=null && !this.cardNo.equals("")){
				
				//保单中的单证编号和名称和产品代码
				StringBuffer contSQL = new StringBuffer();
				contSQL.append("select lmcd.certifycode,lmcd.certifyname,lmcr.riskcode from LZCardNumber lzcn " +
									"inner join lmcertifydes lmcd on lmcd.subcode = lzcn.cardtype " +
									"inner join LMCardRisk lmcr on lmcr.certifycode = lmcd.certifycode " +
									"where 1 = 1 and lzcn.CardNo = '").append(this.cardNo).append("' ");
				SSRS contSSRS =  new ExeSQL().execSQL(contSQL.toString());
				if(contSSRS == null || contSSRS.MaxRow!=1){
					System.out.println("没有得到保单信息");
		  	         error = errLog("没有得到保单信息");
		  	         return false;
				}
				this.wrapcode =  contSSRS.GetText(1, 3);
				
				//modify by zhangyige 2012-08-13
				//校验wrapcode（套餐编码）是否可以打印电子保单
				String sql = "select codename from ldcode where codetype = 'ecprint' and code='"+this.msgType+"'";
				SSRS wrapcodeSSRS = new ExeSQL().execSQL(sql);
				String[] wrapcodes = wrapcodeSSRS.GetText(1, 1).split(",");
				boolean flag = false; //标记套餐编码是否能打印电子保单
				for(int i=0;i<wrapcodes.length;i++){
					String twrapcode = wrapcodes[i];
					if(this.wrapcode.equals(twrapcode)){
						flag = true;
						break;
					}
				}
				if(!flag){
					error = null;
		  	        return false;
				}
				String wrapnameSQL = "select wrapname from ldwrap where riskwrapcode ='"+this.wrapcode+"'";
				this.wrapname = new ExeSQL().getOneValue(wrapnameSQL);
				//this.wrapcode ="WR0001";
				
				String specialSQL="select remark from ldwrap where riskwrapcode='"+this.wrapcode+"'";
				String speContent = new ExeSQL().getOneValue(specialSQL);
				if(!speContent.equals("") && speContent !=null){
					this.special=speContent;
				}
				//保险卡信息
				String insuyearSQL = "select lica.insuyear,lica.InsuYearFlag,lica.CvaliDate,lica.mult,lica.copys,lica.amnt,lica.prem " +
						"from licardactiveinfolist lica " +
				" where lica.cardno ='"+this.cardNo+"'";
				SSRS cardSSRS =  new ExeSQL().execSQL(insuyearSQL);
				if(cardSSRS == null || cardSSRS.MaxRow!=1){
					System.out.println("没有得到保险卡信息");
		  	         error = errLog("没有得到保险卡信息");
		  	         return false;
				}
				
				this.insuyear = cardSSRS.GetText(1, 1);
				this.insuyearflag = cardSSRS.GetText(1, 2);
				this.cvalidate = cardSSRS.GetText(1, 3);
				this.mult = cardSSRS.GetText(1, 4);
				this.coyps = cardSSRS.GetText(1, 5);
				this.amnt = cardSSRS.GetText(1, 6);
				this.prem = cardSSRS.GetText(1, 7);
				
				//by gzh 20110302 调整获取终止日期的逻辑过程，激活卡信息在licardactiveinfolist表中获取不到终止日期。
				if(insuyear==null || "".equals(insuyear) || insuyearflag==null || "".equals(insuyearflag)){
					String dutysql = "select distinct calfactor,calfactortype,calfactorvalue from ldriskdutywrap where riskwrapcode = " +
							" (select riskcode from lmcardrisk where certifycode =" +
							" (select certifycode from lmcertifydes where subcode = '"+this.cardNo.substring(0,2)+"')) " +
							" and (Calfactor ='InsuYear' or Calfactor ='InsuYearFlag')";
					SSRS DutySSRS = new ExeSQL().execSQL(dutysql);
					if(DutySSRS==null || DutySSRS.MaxRow==0){
						System.out.println("没有得到保险责任信息！");
			  	         error = errLog("没有得到保险责任信息！");
			  	         return false;
					}
					for(int j=1;j<=DutySSRS.getMaxRow();j++){
						if("1".equals(DutySSRS.GetText(j, 2))){
							if("InsuYear".equals(DutySSRS.GetText(j, 1))){
								this.insuyear = DutySSRS.GetText(j, 3);
							}else if("InsuYearFlag".equals(DutySSRS.GetText(j, 1))){
								this.insuyearflag = DutySSRS.GetText(j, 3);
							}
						}else if("2".equals(DutySSRS.GetText(j, 2))){
							PubCalculator tCal = new PubCalculator();

			                String tCalSql = DutySSRS.GetText(j, 4);
			                System.out.println("CalSql:" + tCalSql);
			                tCal.setCalSql(tCalSql);

			                tCal.addBasicFactor("Copys", String.valueOf(this.coyps));
			                tCal.addBasicFactor("Mult", String.valueOf(this.mult));

			                String tTmpCalResult = tCal.calculate();

			                if (tTmpCalResult == null || tTmpCalResult.equals(""))
			                {
			                	System.out.println("计算要素失败！");
					  	         error = errLog("计算要素失败！");
			                    return false;
			                }
							if("InsuYear".equals(DutySSRS.GetText(j, 1))){
								this.insuyear = tTmpCalResult;
							}else if("InsuYearFlag".equals(DutySSRS.GetText(j, 1))){
								this.insuyearflag = tTmpCalResult;
							}
						}
					}
				}
				if(insuyear==null || "".equals(insuyear) || insuyearflag==null || "".equals(insuyearflag)){
					System.out.println("计算终止日期时，没有得到保险责任信息！");
		  	         error = errLog("计算终止日期时，没有得到保险责任信息！");
		  	         return false;
				}
				//------by gzh end
				//计算终止日期
				this.cinvalidate=PubFun.calDate(this.cvalidate, Integer.parseInt(this.insuyear), 
						this.insuyearflag,null);
				this.cinvalidate = getPreviousDate(new FDate().getDate(cinvalidate)); // by  gzh 获取前一天日期
				this.cvalidate = DealDateZero(cvalidate);
				this.cinvalidate = DealDateZero(cinvalidate);
				//生效时间
				String insutimeSQL = "select cvalidatetime from wfcontlist where cardno ='"+this.cardNo
				+ "'and branchcode = 'LKBSys' order by date(senddate) desc,time(sendtime) desc "
				+ "fetch first 1 rows only";
				SSRS timeSSRS =  new ExeSQL().execSQL(insutimeSQL);
				String cvalidatetime = "";
				if(timeSSRS.MaxRow > 0){
					cvalidatetime = timeSSRS.GetText(1, 1);
				}
				if(cvalidatetime.equals("")||cvalidatetime.equals(null)||cvalidatetime==null){
					this.dateAndtime = "自"+cvalidate + "零时 至 "+cinvalidate + " 二十四时止";
				}else{
					this.dateAndtime = "自"+cvalidate + " "+ cvalidatetime + " 至 "+cinvalidate + " "+cvalidatetime+" 止";
				}
				//投保人信息
				StringBuffer appntSQL = new StringBuffer();
				appntSQL.append("select distinct wfa.name,wfa.sex,wfa.birthday,wfa.idtype,wfa.idno from licardactiveinfolist lica " +
									" inner join wfappntList wfa on lica.cardno= wfa.cardno " +
										" where lica.cardno ='").append(this.cardNo).append("' ");
				SSRS appntSSRS = new ExeSQL().execSQL(appntSQL.toString());
				if(appntSSRS==null || appntSSRS.MaxRow==0){
					System.out.println("没有得到投保人信息");
		  	         error = errLog("没有得到投保人信息");
		  	         return false;
				}
											
				//被保人信息
				StringBuffer insuSQL = new StringBuffer();
				insuSQL.append("select distinct wfi.name,wfi.sex,wfi.birthday,wfi.idtype,wfi.idno,(case when relationcode ='00' then db2inst1.codename('relation',wfi.Toappntrela) else db2inst1.codename('relation',wfi.Relationtoinsured) end) from licardactiveinfolist lica " +
						" inner join wfinsulist wfi on lica.cardno= wfi.cardno " +
						" where lica.cardno ='").append(this.cardNo).append("' ");
				SSRS insuSSRS = new ExeSQL().execSQL(insuSQL.toString());
				if(insuSSRS==null || insuSSRS.MaxRow==0){
					System.out.println("没有得到被保人信息");
		  	         error = errLog("没有得到被保人信息");
		  	         return false;
				}
				this.insuname = insuSSRS.GetText(1, 1);
				this.insuredidno = insuSSRS.GetText(1, 5);
				//受立益人信息
				StringBuffer bnfSQL = new StringBuffer();
				bnfSQL.append("select distinct wfb.name,wfb.sex,wfb.birthday,wfb.idtype,wfb.idno,wfb.ToInsuRela from licardactiveinfolist lica " +
						" inner join wfbnflist wfb on lica.cardno= wfb.cardno " +
						" where lica.cardno ='").append(this.cardNo).append("' ");
				SSRS bnfSSRS = new ExeSQL().execSQL(bnfSQL.toString());
				
				//保险责任
				this.resMap =calculateAmnt();
				String dutySQL="select lm.dutycode,lm.dutyname from ldriskdutywrap ld"
					+" inner join lmduty lm on lm.dutycode = ld.dutycode"
					+" where calfactor = 'Amnt' and riskwrapcode = '"+this.wrapcode+"'";
				SSRS dutySSRS = new ExeSQL().execSQL(dutySQL);
				if(dutySSRS==null || dutySSRS.MaxRow==0){
					System.out.println("没有得到保险责任信息");
		  	         error = errLog("没有得到保险责任信息");
		  	         return false;
				}
				
				if(this.dutyremark.toString().equals("") || this.dutyremark.toString() ==null){
					System.out.println("没有得到适用条款信息");
		  	         error = errLog("没有得到适用条款信息");
		  	         return false;
				}		
			}else{
				System.out.println("没有得到卡号");
	  	         error = errLog("没有得到卡号");
	  	         return false;
			}			
		return true;
	}
	
	
	/**
	 * 保险责任
	 * @return
	 */
	private Map calculateAmnt(){
		String tTmpCalResult = null;
		Map dutymap = new HashMap();
		
		String dutyname =null;
		String calfactortype =null;
		String calfactorvalue =null;
		String calsql =null;
		
		String dutySQL="select lm.dutyname,ld.calfactortype,ld.calfactorvalue, ld.calsql from ldriskdutywrap ld"
								+" inner join lmduty lm on lm.dutycode = ld.dutycode"
								+" where calfactor = 'Amnt' and riskwrapcode = '"+this.wrapcode+"'";
		try{
			
			SSRS dutySSRS = new ExeSQL().execSQL(dutySQL);
			if(dutySSRS==null || dutySSRS.MaxRow==0){
				System.out.println("计算保额失败");
				return null;
			}
			
			for(int i=1;i<=dutySSRS.MaxRow;i++){
				dutyname = dutySSRS.GetText(i, 1);
				calfactortype = dutySSRS.GetText(i, 2);
				calfactorvalue = dutySSRS.GetText(i, 3);
				calsql = dutySSRS.GetText(i, 4);
				if ("2".equals(calfactortype))
		        {
		            /** 准备要素 calbase */
		            PubCalculator tCal = new PubCalculator();

		            tCal.addBasicFactor("RiskCode", this.wrapcode);
		            tCal.addBasicFactor("Copys", String.valueOf(this.coyps));
		            tCal.addBasicFactor("Mult", String.valueOf(this.mult));

		            /** 计算 */
		            tCal.setCalSql(calsql);
		            System.out.println("CalSql:" + calsql);

		            tTmpCalResult = tCal.calculate();

		            if (tTmpCalResult == null || tTmpCalResult.equals(""))
		            {
		                String tStrErr = "单证号[" + this.cardNo + "]保额计算失败。";               
		                System.out.println(tStrErr);
		                return null;
		                // tTmpCalResult = "0";
		            }
		        }
		        else
		        {
		            tTmpCalResult = calfactorvalue;
		        }			
				dutymap.put(dutyname, tTmpCalResult);
				dutyremark.append(dutyname + tTmpCalResult);
			}
	        
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
        return dutymap;
	}
	
	/**
	 * 上传XML到FTP服务器
	 * 
	 * @return
	 */
	private boolean FTPSendFile(){
	      
	    /**文件*/
	    String fileName=this.cardNo;
	    String filePath="";
	    
	    try{
	    	String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PDFstrUrl'";//生成文件的存放路径
	    	filePath = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
	        System.out.println("生成文件的存放路径   "+filePath);//调试用－－－－－
	        if(filePath == null ||filePath.equals("")){
	            System.out.println("获取文件存放路径出错");
	            return false;
	        }
//	        filePath="E:\\";
		    //上传文件到FTP服务器
		    	if (!sendZip(filePath+fileName+".pdf")) {
		    		System.out.println("上传文件到FTP服务器失败！");
		            return false;
		        } else {
		            //上传成功后删除
		        	System.out.println("上传文件到FTP服务器成功！");
		        	File xmlFile = new File(filePath+fileName+".pdf");
		        	xmlFile.delete(); //删除上传完的文件
		        }		    
	    }catch(Exception e){
	    	e.printStackTrace();
	    	return false;
	    }
		return true;
	}
	/**
     * sendZip
     * 上传文件至指定FTP（清单文件和单打的数据文件在同一个目录）
     * @param string String
     * @param flag String
     * @return boolean
     */
    private boolean sendZip(String cFileUrlName) {
        //登入前置机
         String ip = "";
         String user = "";
         String pwd = "";
         int port = 21;//默认端口为21

         String sql = "select code,codename from LDCODE where codetype='ecprintserver'";
         SSRS tSSRS = new ExeSQL().execSQL(sql);
         if(tSSRS != null){
             for(int m=1;m<=tSSRS.getMaxRow();m++){
                 if(tSSRS.GetText(m,1).equals("IP")){
                     ip = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("UserName")){
                     user = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Password")){
                     pwd = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Port")){
                     port = Integer.parseInt(tSSRS.GetText(m,2));
                 }
             }
         }
//         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port,"ActiveMode");
         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port);
//         FTPTool tFTPTool = new FTPTool("10.252.130.228", "zhangshuo ", "111111", 21);
         try
         {
             if (!tFTPTool.loginFTP())
             {
                 System.out.println(tFTPTool.getErrContent(1));
                 return false;
             }
             else
             {
                  if (!tFTPTool.upload(cFileUrlName)) {
                         CError tError = new CError();
                         tError.errorMessage = tFTPTool
                                               .getErrContent(
                                 FTPReplyCodeName.LANGUSGE_CHINESE);
                         
                         System.out.println("上载文件失败!");
                         return false;
                  }

                 tFTPTool.logoutFTP();
             }
         }
         catch (SocketException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                      .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
              
             System.out.println("上载文件失败，可能是网络异常!");
             return false;
         }
         catch (IOException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                     .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
             
             System.out.println("上载文件失败，可能是无法写入文件");
             return false;
         }

         return true;

    }
	public static void main(String args[]){
		ECPrintOtherBL ecprint = new ECPrintOtherBL();
		ecprint.deal("SU000000059","AI0011");
		
	}
   
    /**
     * 计算条形码长度
     * @param barStr
     * @return
     */
    public int CalculateBarCodeLen(String barStr){
    	  	
    	int strLen=0;
    	char [] barArr = barStr.toCharArray();
    	for (int i=0;i<barArr.length;i++){
    		String tempStr = String.valueOf(barArr[i]);
    		if(tempStr.getBytes().length ==2){
    			strLen+=7;
    		}else{
    			strLen+=1;
    		}
    	}
    	return strLen;
    }
    
    /**
     * 去掉年份中月和日前面的零
     * @return
     */
    public String DealDateZero(String dateStr){
    	String result=null;
    	String year =null;
    	String month =null;
    	String date =null;
    	   	
    	if(dateStr.equals("") || dateStr == null){
    		System.out.println("没有得到有效的字符类型的日期");
    		return null;
    	}
    	year = dateStr.substring(0, 4);
    	month = dateStr.substring(5, 7);
    	date = dateStr.substring(8, 10);
    	
    	if(month.substring(0,1).equals("0")){
    		month= month.substring(1);
    	}
    	if(date.substring(0,1).equals("0")){
    		date= date.substring(1);
    	}
    	result = year+"-"+month+"-"+date;
    	return result;
    }
    
    //gzh 20110118
    private boolean dealPrintMag() {
    	
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String tLimit = PubFun.getNoLimit("86000000");
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        tLOPRTManagerSchema.setOtherNo(cardNo); //存放卡号
        tLOPRTManagerSchema.setOtherNoType("02");
        tLOPRTManagerSchema.setCode("Other");
        tLOPRTManagerSchema.setManageCom("86000000");
        tLOPRTManagerSchema.setAgentCode("aaaa");
        tLOPRTManagerSchema.setReqCom("86000000");
        tLOPRTManagerSchema.setReqOperator("001");
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("1");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        MMap tMMap = new MMap();
        tMMap.put(tLOPRTManagerSchema, "INSERT");
        data.add(tMMap);
        return true;
    }
//  by gzh 20110302 获取date前一天的日期
    public String getPreviousDate(Date date){ 
    	Calendar calendar=Calendar.getInstance(); 
    	FDate tFDate = new FDate();
    	calendar.setTime(date); 
    	int day=calendar.get(Calendar.DATE); 
    	calendar.set(Calendar.DATE,day-1); 
    	return tFDate.getString(calendar.getTime()); 
    	}
    public String errLog(String error){
    	return error;   	
    }
    private boolean print(){
    	HashMap map = new HashMap();
		map.put("CARDNO",this.cardNo);
		map.put("TICKETNO",this.cardNo);
		map.put("INSUREDNAME",this.insuname);
		map.put("INSUREDIDNO",this.insuredidno);
		map.put("AMNT",this.amnt+"元");
		map.put("PREM",this.prem+"元");
		map.put("TERM",this.dateAndtime);
		map.put("MANAGECOM",this.managecom);
		map.put("OPERATOR",this.operator);
		CreateHYXPDF cPDF = null;
		String className = "";
        className = "com.cbsws.xml.ctrl.blogic.PrintWrapCodes.Create" +this.wrapcode+ "HYXPDF";
        try {
            cPDF = (CreateHYXPDF) Class.forName(className).newInstance();
            cPDF.Cpdf(map);
			if(!FTPSendFile()){
				System.out.println("上传FTP失败");
	  	         error = errLog("上传FTP失败");
	  	         return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
    }
}
