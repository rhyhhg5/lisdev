package com.cbsws.picchPub.obj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class RequestHead extends PiccBaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4191325335892848234L;
	//服务编码
	private String Request_Type;
	//唯一标示
	private String Uuid;
	//发送者
	private String Sender;
	//服务版本
	private String Server_Version;
	
	private String User;
	//用户密码
	private String PassWord;
	//时间戳
	private String FlowinTime;
	public String getRequest_Type() {
		return Request_Type;
	}
	public void setRequest_Type(String requestType) {
		Request_Type = requestType;
	}
	public String getUuid() {
		return Uuid;
	}
	public void setUuid(String uuid) {
		Uuid = uuid;
	}
	public String getSender() {
		return Sender;
	}
	public void setSender(String sender) {
		Sender = sender;
	}
	public String getServer_Version() {
		return Server_Version;
	}
	public void setServer_Version(String serverVersion) {
		Server_Version = serverVersion;
	}
	public String getUser() {
		return User;
	}
	public void setUser(String user) {
		User = user;
	}
	public String getPassWord() {
		return PassWord;
	}
	public void setPassWord(String passWord) {
		PassWord = passWord;
	}
	public String getFlowinTime() {
		return FlowinTime;
	}
	public void setFlowinTime(String flowinTime) {
		FlowinTime = flowinTime;
	}
	
}
