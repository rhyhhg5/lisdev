package com.cbsws.picchPub.xml.ctrl.pack;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.cbsws.picchPub.obj.RequestHead;
import com.cbsws.picchPub.util.PiccXschUtil;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;
import com.enterprisedt.util.debug.Logger;

public class PiccMsgXmlParse {


    private final static Logger mLogger = Logger.getLogger(PiccMsgXmlParse.class);

    //    private Document mXmlDoc = null;

    public PiccJOGMsgCollection deal(Document cInXmlDoc)
    {
    	PiccJOGMsgCollection tMsgCols = new PiccJOGMsgCollection();

        Document tXmlDoc = (Document) cInXmlDoc.clone();
        Element tEleRoot = tXmlDoc.getRootElement();

        // 处理报头
        if (!loadMsgHead(tEleRoot, tMsgCols))
        {
            return null;
        }
        // --------------------

        // 处理消息体，不做阻断，找不到类属于正常情况，仅输出进行提示。
        if (!loadBodyNotes(tEleRoot, tMsgCols))
        {
            mLogger.info("部分节点不存在对应对象信息，或Xsch自动加载失败。");
        }
        // --------------------

        return tMsgCols;
    }

    /**
     * 自动加载报头信息。
     * <br />报头信息有且仅有一个。
     * <br />注：报头加载成功后，自动删除报头，用于后续报文主体信息的相关加载。
     * 
     * @param cEleRoot
     * @param cMsgInfo
     * @return
     */
    private boolean loadMsgHead(Element cEleRoot, PiccJOGMsgCollection cMsgInfo)
    {
        String MsgHead_Name = "RequestHead";//   MsgHead
        Element[] tEleMsgHead = getNodeByName(cEleRoot, MsgHead_Name);
        if (tEleMsgHead == null || tEleMsgHead.length != 7)
        {
            mLogger.error("报文头信息不存在或与约定不符。");
            return false;
        }
        Element tEleCurNode = cEleRoot.getChild(MsgHead_Name);
    //    for (int i = 0; i < tEleMsgHead.length; i++)
   //     {
      //      Element tEleNode = tEleMsgHead[i];
            
            RequestHead tBXsch = (RequestHead) PiccXschUtil.swapX2Sch(tEleCurNode, "RequestHead");//  MsgHead
            if (tBXsch == null)
            {
                return false;
            }
            cMsgInfo.setMsgHead(tBXsch);
       // }

        cEleRoot.removeChild(MsgHead_Name);

        return true;
    }

    private boolean loadBodyNotes(Element cEleRoot, PiccJOGMsgCollection cMsgInfo)
    {
        boolean tResFlag = true;

        String tPackage_Name = "com.cbsws.picchobj";

        List tFieldList = cEleRoot.getChildren();
        for (int idx = 0; idx < tFieldList.size(); idx++)
        {
            Object tXSNode = tFieldList.get(idx);
            if (!(tXSNode instanceof Element))
                continue;
            if ((((Element) tXSNode).getChildren()).size() == 0)
                continue;

            Element tEleSubNote = (Element) tXSNode;
            String tNoteName = tEleSubNote.getName();
            if (null != tNoteName && tNoteName.equals("BODY")){
            	Element child = cEleRoot.getChild(tNoteName);
            	loadBodyNotes(child,cMsgInfo);
            }

            Element[] tEleMsgNode = getBodyNodeByName(cEleRoot, tNoteName);
            if (tEleMsgNode == null)
            {
                tResFlag = false;
                mLogger.error("报文数据节点[" + tNoteName + "]未找到相关xml数据信息。");
                continue;
            }

            for (int i = 0; i < tEleMsgNode.length; i++)
            {
                Element tEleNode = tEleMsgNode[i];
                PiccBaseXmlSch tBXsch = (PiccBaseXmlSch) PiccXschUtil.swapX2Sch(tEleNode, tNoteName, tPackage_Name);
                if (tBXsch == null)
                {
                    tResFlag = false;
                    mLogger.error("报文数据节点[" + tNoteName + "]未找到对应Xsch对象，或装载时失败。");
                    break;
                }
                cMsgInfo.setBodyByFlag(tNoteName, tBXsch);
            }
        }

        return tResFlag;
    }

    private Element[] getNodeByName(Element cEleNodeList, String cNodeName)
    {
    //    String tSubNodeFlag = "Item";
        Element[] tResNode = null;

        Element tEleCurNode = cEleNodeList.getChild(cNodeName);
        if (tEleCurNode == null)
        {
            mLogger.error("获取节点[" + cNodeName + "]信息失败。");
            return null;
        }
//
       List tCurNodeList = tEleCurNode.getChildren();
//        if (tCurNodeList == null || tCurNodeList.size() == 0)
//        {
//            mLogger.error("获取节点内容（"+cNodeName+"）清单信息失败。");
//            return null;
//        }

        tResNode = new Element[tCurNodeList.size()];
        for (int i = 0; i < tCurNodeList.size(); i++)
        {
            tResNode[i] = (Element) tCurNodeList.get(i);
        }

        return tResNode;
    }

    private Element[] getBodyNodeByName(Element cEleNodeList, String cNodeName)
    {
        String tSubNodeFlag = "Item";
        Element[] tResNode = null;

        Element tEleCurNode = cEleNodeList.getChild(cNodeName);
        if (tEleCurNode == null)
        {
            mLogger.error("获取节点[" + cNodeName + "]信息失败。");
            return null;
        }

        List tCurNodeList = tEleCurNode.getChildren(tSubNodeFlag);
        if (tCurNodeList == null || tCurNodeList.size() == 0)
        {
            mLogger.error("获取节点内容（Item）清单信息失败。");
            return null;
        }

        tResNode = new Element[tCurNodeList.size()];
        for (int i = 0; i < tCurNodeList.size(); i++)
        {
            tResNode[i] = (Element) tCurNodeList.get(i);
        }

        return tResNode;
    }
}
