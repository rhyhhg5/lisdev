package com.cbsws.picchPub.xml.ctrl.pack;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.cbsws.picchPub.obj.ResponseHead;
import com.cbsws.picchPub.util.PiccXschUtil;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;
import com.enterprisedt.util.debug.Logger;

public class PiccMsgXschParse {


    private final static Logger mLogger = Logger.getLogger(PiccMsgXschParse.class);

    public Document deal(PiccJOGMsgCollection cMsgInfos)
    {
    	PiccJOGMsgCollection tMsgCols = cMsgInfos;

        Document tMsgXmlDoc = null;

        Element tEleRoot = new Element("REQPacket");

        // 处理返回报文头
        Element tEleMsgResHead = createResMsgHead(tMsgCols);
        if (tEleMsgResHead == null)
        {
            return null;
        }
        tEleRoot.addContent(tEleMsgResHead);
        // --------------------

        // 处理消息体，不做阻断，找不到类属于正常情况，仅输出进行提示。
        if (!createBodyNotes(tEleRoot, tMsgCols))
        {
            mLogger.info("部分节点不存在对应对象信息，或Xsch自动转换失败。");
        }
        // --------------------

        tMsgXmlDoc = new Document(tEleRoot);

        return tMsgXmlDoc;
    }

    private Element createResMsgHead(PiccJOGMsgCollection cMsgInfos)
    {
    //    String tSubNodeFlag = "Item";
  //      Element tEleRoot = new Element("REQPacket");

        ResponseHead tMsgResHead = cMsgInfos.getMsgResHead();
        if (tMsgResHead == null)
        {
            mLogger.error("返回报头信息读取失败。");
            return null;
        }

        Element tSubNode = PiccXschUtil.swapSch2X(tMsgResHead, "ResponseHead");
      //  tEleRoot.addContent(tSubNode);

        return tSubNode;
    }

    private boolean createBodyNotes(Element cEleRoot, PiccJOGMsgCollection cMsgInfos)
    {
        String tSubNodeFlag = "Item";
        Element tEleRoot = cEleRoot;

        List tBodyList = cMsgInfos.getBodyIdxList();
        if (tBodyList == null || tBodyList.size() == 0)
        {
            mLogger.error("返回报头信息读取失败。");
            return false;
        }

        for (int idx = 0; idx < tBodyList.size(); idx++)
        {
            String tNodeName = (String) tBodyList.get(idx);
            Element tSubNode = new Element(tNodeName);

            List tSNList = cMsgInfos.getBodyByFlag(tNodeName);
            for (int i = 0; i < tSNList.size(); i++)
            {
            	PiccBaseXmlSch tBxsch = (PiccBaseXmlSch) tSNList.get(i);
                Element tSINode = PiccXschUtil.swapSch2X(tBxsch, tSubNodeFlag);
                tSubNode.addContent(tSINode);
            }
            tEleRoot.addContent(tSubNode);
        }

        return true;
    }

}
