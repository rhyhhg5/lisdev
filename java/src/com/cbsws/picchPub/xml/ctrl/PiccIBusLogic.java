package com.cbsws.picchPub.xml.ctrl;


public interface PiccIBusLogic {

    public PiccJOGMsgCollection service(PiccJOGMsgCollection cMsgInfos);
}
