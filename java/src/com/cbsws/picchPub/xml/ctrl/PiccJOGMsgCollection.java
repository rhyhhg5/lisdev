package com.cbsws.picchPub.xml.ctrl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.cbsws.picchPub.obj.RequestHead;
import com.cbsws.picchPub.obj.ResponseHead;
import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;
import com.enterprisedt.util.debug.Logger;

public class PiccJOGMsgCollection {
	

    private final static Logger mLogger = Logger.getLogger(PiccJOGMsgCollection.class);

    /** 报文头 */
    private RequestHead mMsgHead = null;

    /** 反馈报文头 */
    private ResponseHead mMsgResHead = null;

    /** 节点信息索引 */
    private List mMsgBodyIdx = null;

    /** 节点信息清单 */
    private Map mMsgBodyMapping = null;

    public PiccJOGMsgCollection()
    {
        clean();
    }

    public void clean()
    {
        mMsgHead = null;
        mMsgResHead = null;
        mMsgBodyIdx = new ArrayList();
        mMsgBodyMapping = new HashMap();
    }

    public void setMsgHead(RequestHead cMsgHead)
    {
        mMsgHead = cMsgHead;
    }

    public RequestHead getMsgHead()
    {
        return mMsgHead;
    }

    public void setMsgResHead(ResponseHead cMsgResHead)
    {
        mMsgResHead = cMsgResHead;
    }

    public ResponseHead getMsgResHead()
    {
        return mMsgResHead;
    }

    public void setBodyByFlag(String cNodeFlag, PiccBaseXmlSch cBxsch)
    {
        List tCurNode = null;
        int tCurPos = -1;

        tCurPos = mMsgBodyIdx.indexOf(cNodeFlag);
        if (tCurPos == -1)
        {
            tCurPos = mMsgBodyIdx.size();
            tCurNode = new ArrayList();
            mMsgBodyIdx.add(cNodeFlag);
            mMsgBodyMapping.put(new Integer(tCurPos), tCurNode);
        }
        else
        {
            tCurNode = (List) mMsgBodyMapping.get(new Integer(tCurPos));
        }

        if (tCurNode == null)
            mLogger.error("获取报文中节点信息数据异常。");

        tCurNode.add(cBxsch);
    }

    public List getBodyByFlag(String cNodeFlag)
    {
        List tCurNode = null;
        int tCurPos = -1;

        tCurPos = mMsgBodyIdx.indexOf(cNodeFlag);

        if (tCurPos != -1)
        {
            tCurNode = (List) mMsgBodyMapping.get(new Integer(tCurPos));
        }

        return tCurNode;
    }

    public List getBodyIdxList()
    {
        return mMsgBodyIdx;
    }


}
