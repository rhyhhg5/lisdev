package com.cbsws.picchPub.xml.ctrl;

import org.apache.log4j.Logger;

import com.cbsws.picchPub.obj.ResponseHead;
import com.cbsws.picchPub.util.PiccMsgUtil;
import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public abstract class PiccHABusLogic implements PiccIBusLogic{
	

    protected Logger mLogger = Logger.getLogger(getClass().getName());

    protected PiccJOGMsgCollection mResMsgCols = null;

    public PiccHABusLogic()
    {
        mLogger.info("start:" + getClass().getName());
    }

    public final PiccJOGMsgCollection service(PiccJOGMsgCollection cMsgInfos)
    {
        mResMsgCols = PiccMsgUtil.getResMsgCol(cMsgInfos);
        if (!deal(cMsgInfos))
        {
        	ResponseHead tResMsgHead = mResMsgCols.getMsgResHead();
            
           if (tResMsgHead.getError_Message() == null)
            {
                errLog("逻辑处理中出现，未知异常");
        	 //  tResMsgHead.setResponse_Code("00");
            }
        }
        else
        {
        	ResponseHead tResMsgHead = mResMsgCols.getMsgResHead();
          //  tResMsgHead.setState("00");
           tResMsgHead.setResponse_Code("1"); //1为成功
        }
        return getResult();
    }

    protected final void errLog(String cErrInfo)
    {
        mLogger.error("ErrInfo:" + cErrInfo);

      //  String tErrCode = "99999999";
        ResponseHead tResMsgHead = mResMsgCols.getMsgResHead();
     	 tResMsgHead.setResponse_Code("0");
         tResMsgHead.setError_Message(cErrInfo);
       // tResMsgHead.setErrInfo(cErrInfo);
    }
    protected final void errLog1(String cErrInfo)
    {
    	mLogger.error("ErrInfo:" + cErrInfo);
    	String tErrCode = "";
    	ExeSQL exeSql = new ExeSQL();
    	String sql = "select code,codename from ldcode where codetype='errorCode' with ur";
    	SSRS result = exeSql.execSQL(sql);
    	for(int i=1;i<=result.getMaxRow();i++){
    		String codename = result.GetText(i, 2);
    		if(cErrInfo.contains(codename)){
    			tErrCode = result.GetText(i, 1);
    			break;
    		}
    	}
    	if(tErrCode.equals("")||tErrCode==null){
    		tErrCode = "99999999";
    	}
    	ResponseHead tResMsgHead = mResMsgCols.getMsgResHead();
    	 tResMsgHead.setResponse_Code("0");
         tResMsgHead.setError_Message(cErrInfo);
    //	tResMsgHead.setErrInfo(cErrInfo);
    }

    protected PiccJOGMsgCollection getResult()
    {
        return mResMsgCols;
    }

    protected final void putResult(String cNodeFlag, PiccBaseXmlSch cBxsch)
    {
        mResMsgCols.setBodyByFlag(cNodeFlag, cBxsch);
    }

    protected abstract boolean deal(PiccJOGMsgCollection cMsgInfos);


}
