package com.cbsws.picchPub.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.picchPub.obj.RequestHead;
import com.cbsws.picchPub.xml.ctrl.PiccHABusLogic;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.cbsws.picchobj.ContInfo;
import com.cbsws.picchobj.ProposalTakebackInfo;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ProposalTakebackQueryCInterfaceBL extends PiccHABusLogic{
	public String mContNo="";
	private ContInfo mContInfo=null;
	public CErrors mErrors = new CErrors();
	public ProposalTakebackQueryCInterfaceBL(){
		
	}
	@Override
	protected boolean deal(PiccJOGMsgCollection cMsgInfos) {
		//解析XML
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        
        //组织返回报文
        try {
			if(!getWrapParmList("success")){
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;
	}

	private boolean parseXML(PiccJOGMsgCollection cMsgInfos){
    	System.out.println("开始解析接口XML");
    	RequestHead tRequestHead = cMsgInfos.getMsgHead();
		//获取保单信息
		List mContQueryList = cMsgInfos.getBodyByFlag("ContInfo");
        if (mContQueryList == null || mContQueryList.size() != 1)
        {
            errLog("申请报文中获取保单号失败！");
            return false;
        }
        mContInfo = (ContInfo)mContQueryList.get(0);
        mContNo = mContInfo.getContNo();
		return true;
	}
	
	public boolean getWrapParmList(String tFlag) {
		if("success".equals(tFlag)){
			String contdatasql="";
			if(mContNo==null || "".equals(mContNo)){
				errLog("保单号不能为空！");
				return false;
			}else{
				contdatasql="select a.ContNo contno,e.riskname riskname, b.CValiDate cvalidate,b.AppntName appntname,case when (d.mobile is null or d.mobile='') then d.phone else d.mobile end mobile,c.printdate "
						+ " from LCContGetPol a,LCCont b ,LCContReceive c,lcaddress d,lmriskapp e "
						+ " where a.contno=b.contno "
						+ " and c.contno=b.contno "
						+ " and a.appntno=d.customerno"
						+ " and d.addressno=(select addressno from lcappnt where contno=a.contno) "
						+ " and riskcode=(select riskcode from lcpol lcp where lcp.contno=a.contno and lcp.polno=lcp.mainpolno fetch first 1 rows only) "
						+ " and a.contno='"+mContNo+"'"
						+ " and e.subriskflag='M' "
						+ " and c.dealstate='0' "
						+ " and a.conttype='1' "
						+ " and b.conttype='1' "
						+ " and b.uwflag != 'a' "
						+ " and a.GetPolState='0' "
						+ " fetch first 3000 rows only with ur  ";
				}
			ExeSQL tExeSQL = new ExeSQL();
			SSRS contdataResult;
			contdataResult = tExeSQL.execSQL(contdatasql);
			if (contdataResult.getMaxRow() > 0) {
				for(int i=1;i<=contdataResult.getMaxRow();i++){
					ProposalTakebackInfo tProposalTakebackInfo=new ProposalTakebackInfo();
					tProposalTakebackInfo.setContNo(contdataResult.GetText(i, 1));
					tProposalTakebackInfo.setRiskName(contdataResult.GetText(i, 2));
					tProposalTakebackInfo.setCValidate(contdataResult.GetText(i, 3));
					tProposalTakebackInfo.setAppntName(contdataResult.GetText(i, 4));
					tProposalTakebackInfo.setMobile(contdataResult.GetText(i, 5));
					tProposalTakebackInfo.setPrintdate(contdataResult.GetText(i, 6));
					putResult("CONTLIST",tProposalTakebackInfo);
				}
			}else{
				errLog("未查询到保单信息！");
				return false;
			}
			
		}else if("fail".equals(tFlag)){
			
    	}
		return true;
	}

}
