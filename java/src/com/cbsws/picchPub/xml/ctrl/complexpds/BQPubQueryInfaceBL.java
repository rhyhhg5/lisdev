package com.cbsws.picchPub.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.picchPub.xml.ctrl.PiccHABusLogic;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.cbsws.picchobj.BQCustomerInfo;
import com.cbsws.picchobj.BQPolicyInfo;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BQPubQueryInfaceBL  extends PiccHABusLogic {

	@Override
	protected boolean deal(PiccJOGMsgCollection cMsgInfos) {
		// TODO Auto-generated method stub
        if(!parseandsendXML(cMsgInfos)){
        	return false;
        }
        
		return true;
	}
	
	private boolean parseandsendXML(PiccJOGMsgCollection cMsgInfos){
	    List tCustomerInfo = cMsgInfos.getBodyByFlag("BQCustomerInfo");
	      if (tCustomerInfo == null || tCustomerInfo.size() != 1)
	        {
	            errLog("申请报文中获取客户信息失败。");
	            return false;
	        }
	      BQCustomerInfo  cBQCustomerInfo = (BQCustomerInfo)tCustomerInfo.get(0);
	       
	     String tName= cBQCustomerInfo.getCustomerName();
	     String tsub ="";
	     if (null != tName && !tName.equals("")){
	    	 tsub =tsub+" and ld.Name= '"+tName+"' ";
	     }
	     
         String tIDNo = cBQCustomerInfo.getCustomerIDNo(); 
	     
	     if (null != tIDNo && !tIDNo.equals("")){
	    	 tsub =tsub+" and ld.IDNo= '"+tIDNo+"' ";
	     }
	     ExeSQL tExeSQL = new ExeSQL();
	     if(null == cBQCustomerInfo.getCustomerIDType()|| cBQCustomerInfo.getCustomerIDType().equals("")){
	          errLog("申请报文中证件类型不能为空。");
	          return false;
	     }
	   
	     String tidsql = "select othersign from ldcode where codetype='appidtype' and code='"+cBQCustomerInfo.getCustomerIDType()+"'";
	  
	     String tIDType= tExeSQL.getOneValue(tidsql);
	     
	     if (null != tIDType && !tIDType.equals("")){
	    	 tsub =tsub+" and  ld.IDType= '"+tIDType+"' ";
	     }
	     
   String str = " select (select riskname from lmriskapp where  riskcode = (select riskcode from lcpol where riskcode not in (select code  from ldcode where codetype = 'jkys')and polno =mainpolno and contno =a.contno fetch first 1 rows only )) ,a.contno ,(select codename from ldcode where codetype='stateflag' and code =a.stateflag ) ,"
             + " a.cvalidate,"
             + " a.paytodate "
             + " from lccont a where 1=1 "
             + " and a.ContType = '1' and AppFlag='1' "
             + " and a.AppntNo in ( select ld.CustomerNo from LDPerson ld where 1=1    "     
             + tsub

               + " 	  )" ;		
  
      SSRS tSSRS=new ExeSQL().execSQL(str);
      if(null!=tSSRS &&  tSSRS.MaxRow>0){
      
	   for(int i=1; i<=tSSRS.MaxRow; i++){
		BQPolicyInfo tPolicyInfo=new BQPolicyInfo();
		tPolicyInfo.setRiskName(tSSRS.GetText(i, 1));
		tPolicyInfo.setPolicyNo(tSSRS.GetText(i, 2));
		tPolicyInfo.setPolicyState(tSSRS.GetText(i, 3));
		tPolicyInfo.setCValiDate(tSSRS.GetText(i, 4));
		putResult("PolicyInfo",tPolicyInfo);
	 }
      }else{
    	  errLog("没有查询到信息");
    	  return false;
      }
		return true;
	}


	
}
