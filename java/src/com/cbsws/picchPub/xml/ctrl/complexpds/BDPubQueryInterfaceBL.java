
package com.cbsws.picchPub.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.picchPub.obj.RequestHead;
import com.cbsws.picchPub.xml.ctrl.PiccHABusLogic;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.cbsws.picchobj.ContCustomerInfo;
import com.cbsws.picchobj.ContPolicyInfo;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BDPubQueryInterfaceBL extends PiccHABusLogic{
	public String mName = "";
	public String mIDType = "";
	public String mIDNo = "";
	public String mContState = "";
	private ContCustomerInfo mContCustomerInfo =null;
	public CErrors mErrors = new CErrors();
	
	public BDPubQueryInterfaceBL() {
		//this.MsgType = "BQCONTINFOQUERY";
	}
	
    public boolean deal(PiccJOGMsgCollection cMsgInfos)  {
    	//解析XML
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        
        //组织返回报文
        try {
        	if(!getWrapParmList("success")){
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;

        
    }
	private boolean parseXML(PiccJOGMsgCollection cMsgInfos){
    	System.out.println("开始解析接口XML");
    	RequestHead tRequestHead = cMsgInfos.getMsgHead();
		/*BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();*/
		//获取保单信息
		List mContQueryList = cMsgInfos.getBodyByFlag("ContCustomerInfo");
        if (mContQueryList == null || mContQueryList.size() != 1)
        {
            errLog("申请报文中获取保单信息失败。");
            return false;
        }
        mContCustomerInfo = (ContCustomerInfo)mContQueryList.get(0);
        mName = mContCustomerInfo.getCustomerName();
        ExeSQL tExeSQL = new ExeSQL();
		SSRS IDTypesqlResult;
		//集团证件类型与核心证件类型转换
        String IDTypesql="select othersign from ldcode where codetype = 'appidtype' and code='"+mContCustomerInfo.getCustomerIDType()+"' ";
		IDTypesqlResult = tExeSQL.execSQL(IDTypesql);
		if(IDTypesqlResult.getMaxRow()<0){
			errLog("证件类型错误，请核实！");
            return false;
		}
		if("".equals(IDTypesqlResult.GetText(1, 1))||IDTypesqlResult.GetText(1, 1)==null){
			errLog("证件类型在核心系统不存在，请核实！");
            return false;
		}else{
			mIDType = IDTypesqlResult.GetText(1, 1);
		}
        mIDNo = mContCustomerInfo.getCustomerIDNo();
        mContState = mContCustomerInfo.getContState();
        /*mContNo=mContQueryInfo.getContNo();
        mPrtNo=mContQueryInfo.getPrtNo();
        mAgentCode=mContQueryInfo.getAgentCode();*/
		return true;
	}
	
	public boolean getWrapParmList(String tFlag) {
		if("success".equals(tFlag)){
			String contdatasql="";
			if("".equals(mName) || mName == null || "".equals(mIDType) || mIDType == null || "".equals(mIDNo) || mIDNo == null || "".equals(mContState) || mContState == null){
				errLog("姓名、证件类型、证件号、保单状态不能空");
				return false;
			}
			
			if(mContState.equals("00")){//全部保单
				contdatasql = "select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " (case "
						+ " when (lcp.stateflag='1' and lcp.cvalidate <= current date) then '01'"
						+ " when (lcp.stateflag='1' and lcp.signdate <= current date and lcp.cvalidate > current date) then '02' "
						+ " when lcp.stateflag='2' then '03' "
						+ " when lcp.stateflag in ('3','4') then '04' end ) ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.INSUREDNO in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"') "
						+ " and lcp.ContType = '1' "
						+ " and lcp.appflag = '1' "
						+ " union "
						+ " select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " (case "
						+ " when (lcp.stateflag='1' and lcp.cvalidate <= current date) then '01'"
						+ " when (lcp.stateflag='1' and lcp.signdate <= current date and lcp.cvalidate > current date) then '02' "
						+ " when lcp.stateflag='2' then '03' "
						+ " when lcp.stateflag in ('3','4') then '04' end ) ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.appntno in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"') "
						+ " and lcp.ContType = '1' "
						+ " and lcp.appflag = '1' "
						+ " union "
						+ " select lbc.contno ContNo,"
						+ " lbp.INSUREDNAME InsuredName,"
						+ " lbp.CVALIDATE CValidate,"
						+ " '04' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lbcont lbc left join lbpol lbp on lbc.contno = lbp.contno left join lmriskapp lm on lbp.riskcode = lm.riskcode left join LPEdorItem lp on lbc.contno=lp.contno"
						+ " where lbp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lbc.contno fetch first 1 rows only)"
						+ " and lbp.INSUREDNO in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"') "
						+ " and lbp.ContType = '1' "
						+ " and lbp.appflag = '1' "
						+ " and lp.EdorType in ('CT', 'WT', 'XT') "
						+ " union "
						+ " select lbc.contno ContNo,"
						+ " lbp.INSUREDNAME InsuredName,"
						+ " lbp.CVALIDATE CValidate,"
						+ " '04' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lbcont lbc left join lbpol lbp on lbc.contno = lbp.contno left join lmriskapp lm on lbp.riskcode = lm.riskcode left join LPEdorItem lp on lbc.contno=lp.contno"
						+ " where lbp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lbc.contno fetch first 1 rows only)"
						+ " and lbp.appntno in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"') "
						+ " and lbp.ContType = '1' "
						+ " and lbp.appflag = '1' "
						+ " and lp.EdorType in ('CT', 'WT', 'XT')"
						+ " with ur";
			}else if(mContState.equals("01")){//有效保单
				contdatasql = "select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " '01' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.INSUREDNO in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"' ) "
						+ " and lcp.cvalidate <= current date"
						+ " and lcp.AppFlag='1' "
						+ " and lcp.ContType = '1' "
						+ " and lcp.STATEFLAG = '1'  "
						+ " union "
						+ "select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " '01' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.appntno in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"' ) "
						+ " and lcp.cvalidate <= current date"
						+ " and lcp.AppFlag='1' "
						+ " and lcp.ContType = '1' "
						+ " and lcp.STATEFLAG = '1' "
						+ " with ur ";
			}else if(mContState.equals("02")){//未起保保单
				contdatasql = "select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " '02' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.INSUREDNO in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"' ) "
						+ " and lcp.signdate <= current date "
						+ "	and lcp.cvalidate > current date "
						+ " and lcp.AppFlag='1' "
						+ " and lcp.ContType = '1' "
						+ " and lcp.STATEFLAG = '1'  "
						+ " union "
						+ "select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " '02' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.appntno in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"' ) "
						+ " and lcp.signdate <= current date "
						+ "	and lcp.cvalidate > current date "
						+ " and lcp.AppFlag='1' "
						+ " and lcp.ContType = '1' "
						+ " and lcp.STATEFLAG = '1' "
						+ " with ur ";
			}else if(mContState.equals("03")){//已停效
				contdatasql = "select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " '03' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.INSUREDNO in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"' ) "
						+ " and lcp.AppFlag='1' "
						+ " and lcp.ContType = '1' "
						+ " and lcp.STATEFLAG = '2' "
						+ " union "
						+ "select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " '03' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.appntno in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"' ) "
						+ " and lcp.AppFlag='1' "
						+ " and lcp.ContType = '1' "
						+ " and lcp.STATEFLAG = '2' "
						+ " with ur ";
			}else if(mContState.equals("04")){//已终止
				contdatasql = "select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " '04' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.INSUREDNO in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"') "
						+ " and lcp.ContType = '1' "
						+ " and lcp.appflag = '1' "
						+ " and lcp.stateflag in ('3','4') "
						+ " union "
						+ " select lcc.contno ContNo,"
						+ " lcp.INSUREDNAME InsuredName,"
						+ " lcp.CVALIDATE CValidate,"
						+ " '04' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lccont lcc left join lcpol lcp on lcc.contno = lcp.contno left join lmriskapp lm on lcp.riskcode = lm.riskcode "
						+ " where lcp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lcc.contno fetch first 1 rows only)"
						+ " and lcp.appntno in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"') "
						+ " and lcp.ContType = '1' "
						+ " and lcp.appflag = '1' "
						+ " and lcp.stateflag in ('3','4') "
						+ " union "
						+ " select lbc.contno ContNo,"
						+ " lbp.INSUREDNAME InsuredName,"
						+ " lbp.CVALIDATE CValidate,"
						+ " '04' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lbcont lbc left join lbpol lbp on lbc.contno = lbp.contno left join lmriskapp lm on lbp.riskcode = lm.riskcode left join LPEdorItem lp on lbc.contno=lp.contno"
						+ " where lbp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lbc.contno fetch first 1 rows only)"
						+ " and lbp.INSUREDNO in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"') "
						+ " and lbp.ContType = '1' "
						+ " and lbp.appflag = '1' "
						+ " and lp.EdorType in ('CT', 'WT', 'XT')"
						+ " union "
						+ " select lbc.contno ContNo,"
						+ " lbp.INSUREDNAME InsuredName,"
						+ " lbp.CVALIDATE CValidate,"
						+ " '04' ContState,"
						+ " lm.RISKNAME RiskName "
						+ " from lbcont lbc left join lbpol lbp on lbc.contno = lbp.contno left join lmriskapp lm on lbp.riskcode = lm.riskcode left join LPEdorItem lp on lbc.contno=lp.contno"
						+ " where lbp.riskcode=(select riskcode from lcpol where polno=mainpolno and contno=lbc.contno fetch first 1 rows only)"
						+ " and lbp.appntno in (select distinct (CUSTOMERNO) from LDPERSON ld where ld.name = '"+mName+"' and ld.IDTYPE = '"+mIDType+"' and ld.idno = '"+mIDNo+"') "
						+ " and lbp.ContType = '1' "
						+ " and lbp.appflag = '1' "
						+ " and lp.EdorType in ('CT', 'WT', 'XT')"
						+ " with ur";
			}else{
				errLog("保单状态错误！");
				return false;
			}
			

		
		// 1)保单信息 CONTDATA:contdatasql

			ExeSQL tExeSQL = new ExeSQL();
			SSRS contdataResult;

			contdataResult = tExeSQL.execSQL(contdatasql);

			if (contdataResult.getMaxRow() > 0) {
				//ContPolicyInfo tContPolicyInfo=new ContPolicyInfo();
				for(int i=1;i<=contdataResult.getMaxRow();i++){
					ContPolicyInfo tContPolicyInfo=new ContPolicyInfo();
					tContPolicyInfo.setContNo(contdataResult.GetText(i, 1));
					tContPolicyInfo.setInsuredName(contdataResult.GetText(i, 2));
					tContPolicyInfo.setCValidate(contdataResult.GetText(i, 3));
					tContPolicyInfo.setContState(contdataResult.GetText(i, 4));
					tContPolicyInfo.setRiskName(contdataResult.GetText(i, 5));	
					putResult("CONTLIST",tContPolicyInfo);
				}
				
			}else{
				errLog("未查询到保单信息！");
				return false;
			}
			
		}else if("fail".equals(tFlag)){
			
    	}
		return true;
	}

}
