package com.cbsws.picchPub.xml.ctrl.complexpds;

import java.util.List;

import com.cbsws.picchPub.xml.ctrl.PiccHABusLogic;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.cbsws.picchobj.CaseNoInfo;
import com.cbsws.picchobj.LPCaseNoInfo;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LLAPPIdCaseInfo extends PiccHABusLogic {

	public String mCustomername = "";// 被保险人姓名
	public String mIdtype = "";// 证件类型
	public String mIdno = "";// 被保险人证件号码
	private String mISPROBLEM; // 是否问题件
	private String mSTATE; // 问题件的状态
	public CaseNoInfo mCaseNoInfo;
	public LPCaseNoInfo mLPCaseNoInfo;
	private String sqlname;
	// 发送者身份
	private String Sender = "";
	// 报文请求类型
	private String mRequestType = "";
	// 报文交互密码
	private String PassWord = "";
	// 用户代码
	private String user = "";
	// 错误容器
	public CErrors mCErrors = new CErrors();

	public LLAPPIdCaseInfo() {
	}

	public boolean deal(PiccJOGMsgCollection cMsgInfos) {
		// 解析XML
		if (!parseXML(cMsgInfos)) {
			return false;
		}
		// 先进行下必要的数据校验
		// if(!checkData()){
		// System.out.println("数据校验失败---");
		// //组织返回报文
		// putResult("LPCaseNoInfo",mLPCaseNoInfo);
		// }
		return true;
	}

	private boolean parseXML(PiccJOGMsgCollection cMsgInfos) {
		System.out.println("开始进入XML");
		List tCaseNoInfo = cMsgInfos.getBodyByFlag("CaseNoInfo");
		if (tCaseNoInfo == null || tCaseNoInfo.size() != 1) {
			errLog("报文中获取保险人信息失败。");
			return false;
		}

		mCaseNoInfo = (CaseNoInfo) tCaseNoInfo.get(0); // 获取item点
		mCustomername = mCaseNoInfo.getCUSTOMERNAME();
		mIdtype = mCaseNoInfo.getIDTYPE();
		mIdno = mCaseNoInfo.getIDNO();
		mISPROBLEM = mCaseNoInfo.getISPROBLEM();
		mSTATE = mCaseNoInfo.getSTATE();
		if (!"".equals(mCustomername) && mCustomername != null) {
			sqlname = "and a.customername='" + mCustomername + "'";
		}
	    ExeSQL tExeSQL = new ExeSQL();
		
		//先进行转码校验是是否存在。
		String idSql="select code from ldcode1 where codetype='APPIDTYPE' and code1='"+mIdtype+"' with ur";
		String aIdType =tExeSQL.getOneValue(idSql);
		if("".equals(aIdType)) {
			errLog("客户证件类型未匹配，请核实证件类型");
			return false;
		}else {
			String dealSQL = "select a.caseno,a.rgtstate,a.rgtdate from llcase a, llclaimdetail b "
					+ "where a.caseno=b.caseno and a.idno='"+mIdno +"' and a.idtype='"+aIdType+"'" 
					+ sqlname
					+ "and not exists (select 1 from ldcode where codetype='lpapprisk' and code=b.riskcode)  group by a.caseno,a.rgtstate,a.rgtdate   with ur";//app不查询险种
			SSRS tSSRS = new ExeSQL().execSQL(dealSQL);
			System.out.println(dealSQL);

			if (tSSRS != null && tSSRS.getMaxRow() > 0) {
				for (int i = 1; i <= tSSRS.MaxRow; i++) {
					mLPCaseNoInfo = new LPCaseNoInfo();
					mLPCaseNoInfo.setCASENO(tSSRS.GetText(i, 1));
					mLPCaseNoInfo.setRGTSTATE(tSSRS.GetText(i, 2));
					mLPCaseNoInfo.setRGTDATE(tSSRS.GetText(i, 3));
					ExeSQL RSSRS = new ExeSQL();
					String problemSQL = "select state from LLCASEPROBLEM where 1=1 and caseno = '"
							+ tSSRS.GetText(i, 1)
							+ "' order by makedate desc ,maketime desc with ur ";
					SSRS problemSSRS = tExeSQL.execSQL(problemSQL);
					int problemNo = problemSSRS.getMaxRow();
					if (problemNo > 0) {
						mLPCaseNoInfo.setISPROBLEM("1");
						mLPCaseNoInfo.setSTATE(problemSSRS.GetText(i, 1));
					} else {
						mLPCaseNoInfo.setISPROBLEM("0");
					}
					putResult("LPCaseNoInfo", mLPCaseNoInfo);
				}
			} else {
				errLog("该客户未发生理赔");
				return false;
			}
		}
		return true;
	}

	private boolean checkData() {
		System.out.println("LPADNewInfaceBLRegister--统一接口--checkData()");
		if (!"Sender".equals(Sender)) {
			errLog("发送者身份【Sender='" + Sender + "'】错误");
			return false;
		}
		if (!"Request_Type".equals(mRequestType)) {
			buildError("checkData()", "【请求类型】的值不存在或匹配错误");
			return false;
		}
		if (PassWord == null || PassWord == "" || PassWord.equals("null")) {
			errLog("请求密码为空了");
			return false;
		}
		if (PassWord == null || PassWord == "" || PassWord.equals("null")) {
			buildError("checkData()", "请求参数密码为空");
			return false;
		}

		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError tCError = new CError();
		tCError.moduleName = "LPADNewInfaceBLRegister";
		tCError.functionName = szFunc;
		tCError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mCErrors.addOneError(tCError);
	}

}
