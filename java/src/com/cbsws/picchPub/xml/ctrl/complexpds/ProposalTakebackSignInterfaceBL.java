package com.cbsws.picchPub.xml.ctrl.complexpds;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.cbsws.picchPub.obj.RequestHead;
import com.cbsws.picchPub.xml.ctrl.PiccHABusLogic;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.cbsws.picchobj.ContInfo;
import com.cbsws.picchobj.ProposalTakebackSignInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.lis.vschema.LCContGetPolSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ProposalTakebackSignInterfaceBL extends PiccHABusLogic{
	public String mContNo="";
	public String mName="";
	public String mSignForDate="";
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private ContInfo mContInfo=null;
	public CErrors mErrors = new CErrors();
	
	public ProposalTakebackSignInterfaceBL(){
		
	}
	
	@Override
	protected boolean deal(PiccJOGMsgCollection cMsgInfos) {
		//解析XML
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        //组织返回报文
        try {
        	if(!getWrapParmList("success")){
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;
	}
	
	private boolean parseXML(PiccJOGMsgCollection cMsgInfos){
    	System.out.println("开始解析接口XML");
    	RequestHead tRequestHead = cMsgInfos.getMsgHead();
		//获取保单信息
		List mContQueryList = cMsgInfos.getBodyByFlag("ContInfo");
        if (mContQueryList == null || mContQueryList.size() != 1)
        {
            errLog("申请报文中获取签收信息失败！");
            return false;
        }
        Date now=new Date();
        mContInfo = (ContInfo)mContQueryList.get(0);
        mContNo = mContInfo.getContNo();
        mName=mContInfo.getAppntName();
        mSignForDate=mContInfo.getGetpolDate();
		return true;
	}
	
	public boolean getWrapParmList(String tFlag) {
		if("success".equals(tFlag)){
			if(mContNo==null || "".equals(mContNo)){
				errLog("保单号不能为空！");
				return false;
			}else if(mName==null || "".equals(mName)){
				errLog("投保人姓名不能为空！");
				return false;
			}else if(mSignForDate==null || "".equals(mSignForDate)){
				errLog("签收日期不能为空！");
				return false;
			}else if(!checkbq()){
				return false;
			}else if(!checkSign()){
				return false;
			}else if(!checkSignForDate()){
				return false;
			}
			Date SignDate=new Date();
			try {
				SignDate=format.parse(mSignForDate);
			} catch (ParseException e1) {
				errLog("日期转换错误！");
				return false;
			}
			LCContGetPolSchema tLCContGetPolSchema=new LCContGetPolSchema();
			LCContGetPolSet tLCContGetPolSet=new LCContGetPolSet();
			tLCContGetPolSchema.setContNo(mContNo);
			tLCContGetPolSet.set(tLCContGetPolSchema.getDB().query());
			if(tLCContGetPolSet!=null && tLCContGetPolSet.size()>0){
				tLCContGetPolSchema.setSchema(tLCContGetPolSet.get(1));
				tLCContGetPolSchema.setContNo(mContNo);
				tLCContGetPolSchema.setGetpolDate(SignDate);
				tLCContGetPolSchema.setGetpolMan(mName);
				tLCContGetPolSchema.setSendPolMan(tLCContGetPolSchema.getSendPolMan());
				tLCContGetPolSchema.setGetPolOperator("CAP001");
				tLCContGetPolSchema.setManageCom(tLCContGetPolSchema.getManageCom());
				tLCContGetPolSchema.setAgentCode(tLCContGetPolSchema.getAgentCode());
				tLCContGetPolSchema.setContType("1");
				GlobalInput globalInput = new GlobalInput();
				VData vData = new VData();
				String mOperate="INSERT";
				globalInput.Operator = "CAPP01";
				globalInput.ComCode = "86";
				globalInput.ManageCom = "86";
				globalInput.setSchema(globalInput);
				vData.addElement(tLCContGetPolSchema);
				vData.add(globalInput);
				try{
					LCContGetPolUI tLCContGetPolUI = new LCContGetPolUI();
		            System.out.println("LCContGetPolUI BEGIN");
		            if (tLCContGetPolUI.submitData(vData, mOperate) == false) {
		            	if (tLCContGetPolUI.mErrors.needDealError()) {
							errLog(tLCContGetPolUI.mErrors.getFirstError());
							return false;
						} else {
							errLog(" 保存失败，但是没有详细的原因。");
							return false;
						}
		            }
					}catch(Exception e){
						// @@错误处理
						errLog("保单签收失败！");
						return false;
					}
			}else{
				errLog("未查询到保单信息！");
				return false;
			}
			
			ExeSQL tExeSQL = new ExeSQL();
			SSRS contdataResult;
			String contdatasql="select a.ContNo,b.appntname,b.cvalidate,a.getpoldate "
					+ " from LCContGetPol a left join LCCont b on a.contno=b.contno"
					+ " where a.contno='"+mContNo+"' "
					+ " with ur ";
			contdataResult = tExeSQL.execSQL(contdatasql);
			if (contdataResult.getMaxRow() > 0) {
					ProposalTakebackSignInfo tProposalTakebackSignInfo=new ProposalTakebackSignInfo();
					tProposalTakebackSignInfo.setContNo(contdataResult.GetText(1, 1));
					tProposalTakebackSignInfo.setAppntName(contdataResult.GetText(1, 2));
					tProposalTakebackSignInfo.setCValidate(contdataResult.GetText(1, 3));
					tProposalTakebackSignInfo.setGetpolDate(contdataResult.GetText(1, 4));
					putResult("CONTLIST",tProposalTakebackSignInfo);
			}else{
				errLog("未查询到保单信息！");
				return false;
			}
			
		}else if("fail".equals(tFlag)){
			errLog("未查询到保单信息！");
			return false;
    	}
		return true;
	}
	
	public boolean checkbq(){
		String Csql="select a.* from LPEdorApp a, LPEdorMain b where a.edorAcceptNO = b.edorAcceptNo  "
			    +" and b.contNo ='"+mContNo+"' and a.edorState != '0'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS CsqlResult;
		CsqlResult = tExeSQL.execSQL(Csql);
		if(CsqlResult.MaxRow>0){
			errLog("该单正在保全操作中，待保全确认后再进行回执回销操作！");
			return false;
		}
		return true;
	}
	public boolean checkSignForDate(){
		String SysDate=PubFun.getCurrentDate();
		String printDate="";
		String DateSql="select to_char(printdate,'yyyy-mm-dd') from LCContReceive where contno='"+mContNo+"' and dealstate='0' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS DateSqlResult=tExeSQL.execSQL(DateSql);
		if(DateSqlResult.MaxRow>0){
			printDate=DateSqlResult.GetText(1, 1);
			if(SysDate.compareTo(mSignForDate)<0){
				errLog("客户签收日期不得晚于系统的回销日期！");
				return false;
			}
			if(printDate.compareTo(mSignForDate)>0){
				errLog("客户签收日期不能早于合同打印时间！");
				return false;
			}
		}
		return true;
	}
	//判断该保单是否未签收
	private boolean checkSign() {
		String checkSign="select 1  from LCContGetPol a,LCCont b ,LCContReceive c where a.contno=b.contno and c.contno=b.contno and a.contno='"+mContNo+"' and c.dealstate='0' and a.conttype='1' and b.conttype='1' and b.uwflag != 'a' and a.GetPolState='0' fetch first 3000 rows only with ur ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS checkSignResult=tExeSQL.execSQL(checkSign);
		if(checkSignResult==null || checkSignResult.MaxRow==0){
			errLog("该保单已签收，不能再次进行签收！");
			return false;
		}
		return true;
	}
}
