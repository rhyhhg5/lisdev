package com.cbsws.picchPub.xml.config;

import org.apache.log4j.Logger;

import com.cbsws.core.xml.config.BusLogicCfgFactory;

import com.cbsws.picchPub.obj.RequestHead;
import com.cbsws.picchPub.xml.ctrl.PiccIBusLogic;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.ecwebservice.subinterface.LoginVerifyTool;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class PiccJOGSkipFactory {
	
	private final static Logger mLogger = Logger.getLogger(BusLogicCfgFactory.class);

    private PiccJOGSkipFactory()
    {
    }

    public static PiccIBusLogic getNewInstanceBusLogic(PiccJOGMsgCollection cMsgInfos)
    {
    	RequestHead tMsgHead = cMsgInfos.getMsgHead();
        System.out.println("tMsgHead="+tMsgHead);
        if (tMsgHead == null)
        {
            mLogger.error("获取报头信息失败。");
            return null;
        }

        String tBusLogicFlag = tMsgHead.getRequest_Type();
        System.out.println("tBusLogicFlag="+tBusLogicFlag);
        String transType = tBusLogicFlag;
        String sql = "select TransDetail,transName from WFTransInfo where TransType='"
			+ transType + "' and transstate = '2' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sResult;
		sResult = tExeSQL.execSQL(sql);
		String TransDetail = "";
		String transName = "";
		if (sResult.MaxRow > 0) {
			TransDetail = sResult.GetText(1, 1);
			transName = sResult.GetText(1, 2);
			// 日志2--调用某个交易的java类
			LoginVerifyTool.writeTransTypeLog(transName);
		}
		if (TransDetail == null || TransDetail.equals(""))
        {
        	System.out.println("TransDetail="+TransDetail);
            return null;
        } 
        PiccIBusLogic tBusLogic = null;
        try
        {
            tBusLogic = (PiccIBusLogic) Class.forName(TransDetail).newInstance();
        }
        catch (Exception ex)
        {
        	System.out.println("ex");
            ex.printStackTrace();
            return null;
        }

        return tBusLogic;
    }

}
