package com.cbsws.picchPub.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import org.jdom.Element;


import com.cbsws.picchPub.obj.RequestHead;
import com.cbsws.picchPub.obj.ResponseHead;
import com.cbsws.picchPub.xml.ctrl.PiccJOGMsgCollection;
import com.enterprisedt.util.debug.Logger;
import com.sinosoft.lis.pubfun.PubFun;

public  final class PiccMsgUtil {
	  private final static Logger mLogger = Logger.getLogger(PiccMsgUtil.class);


	    public static PiccJOGMsgCollection getResMsgCol(PiccJOGMsgCollection cMsgCols)
	    {
	    	PiccJOGMsgCollection tResMsgCols = new PiccJOGMsgCollection();

	        RequestHead tSrcMsgHead = cMsgCols.getMsgHead();
	        if (tSrcMsgHead == null)
	        {
	            mLogger.error("未获取到报头信息。");
	            return null;
	        }

	        ResponseHead tResMsgHead = new ResponseHead();
	        tResMsgHead.setRequest_Type(tSrcMsgHead.getRequest_Type());
	        tResMsgHead.setUuid(tSrcMsgHead.getUuid());
//	        tResMsgHead.setsender(tSrcMsgHead.getsender());
//	        tResMsgHead.setserver_version(tSrcMsgHead.getserver_version());
//	        tResMsgHead.setuser(tSrcMsgHead.getuser());
//	        tResMsgHead.setpassword(tSrcMsgHead.getpassword());	        
//	        tResMsgHead.setflowintime(tSrcMsgHead.getflowintime());
//	        tResMsgCols.setMsgHead(tResMsgHead);
	        String tCurrentDate = PubFun.getCurrentDate();
	        String tCurrentTime = PubFun.getCurrentTime();
	        tResMsgHead.setTimeStamp(tCurrentDate+" "+tCurrentTime);
	        tResMsgCols.setMsgResHead(tResMsgHead);

	        return tResMsgCols;
	    }
}