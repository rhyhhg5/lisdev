/**
 * 2010-8-20
 */
package com.cbsws.core.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.cbsws.core.xml.ctrl.PubServiceApi;
import com.sinosoft.utility.ExeSQL;

/**
 * @author LY
 *
 */
public class PubServiceProxy
{
    private final static Logger mLogger = Logger.getLogger(PubServiceProxy.class);

    public PubServiceProxy()
    {
        mLogger.info("init:" + PubServiceProxy.class.toString());
    }

    public String service(String cInXmlStr)
    {
        mLogger.info("Start Service");
        
        System.out.println("PubServiceProxy：第三方报送核心报文");
        System.out.println(cInXmlStr);

        //若报文类型在配置表中配置过，存储。
        if(whichTypeIs(cInXmlStr)){
        	saveXML(cInXmlStr,"fromCPToCore");
        }
        
        // 记录服务启动时间
        long mStartMillis = System.currentTimeMillis();
        Date datetime = new Date(mStartMillis);
        System.out.println("**********开始执行PubServiceProxy:"+datetime);
        mLogger.debug(cInXmlStr);
        // --------------------

        String mOutXmlStr = null;
        try
        {
            // 解析报文字符串流
            StringReader tInXmlReader = new StringReader(cInXmlStr);
            SAXBuilder tSAXBuilder = new SAXBuilder();
            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);
            // --------------------

            // 调用核心后台处理
            PubServiceApi tBusLogic = new PubServiceApi();
            Document tOutXmlDoc = tBusLogic.deal(tInXmlDoc);
            // --------------------

            StringWriter tStringWriter = new StringWriter();
            XMLOutputter tXMLOutputter = new XMLOutputter();
            tXMLOutputter.output(tOutXmlDoc, tStringWriter);
            mOutXmlStr = tStringWriter.toString();
            
            System.out.println("PubServiceProxy：核心返回报文："+mOutXmlStr);
            mLogger.info(mOutXmlStr);
            if(whichTypeIs(mOutXmlStr)){
            	saveXML(mOutXmlStr,"fromCoreToCP");
            }
        }
        catch (Exception ex)
        {
            mLogger.error("交易出错！", ex);
            mOutXmlStr = getError(ex.toString());
        }
        mLogger.debug(mOutXmlStr);

        // 记录服务结束时间
        long mDealMillis = System.currentTimeMillis() - mStartMillis;
        System.out.println("**********处理总耗时：" + mDealMillis / 1000.0 + "s");
        mLogger.debug("处理总耗时：" + mDealMillis / 1000.0 + "s");
        // --------------------

        mLogger.info("End Service");

        return mOutXmlStr;
    }

    //非预期异常处理
    public String getError(String pErrorMsg)
    {
        mLogger.info("Into CrsPubService.getError()...");

        mLogger.info("Out CrsPubService.getError()!");
        return null;
    }

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException
    {
    	PubServiceProxy tPubServiceProxy = new PubServiceProxy();
    	String mInFilePath = "E:/3/7/SH0004请求.xml";
		InputStream mIs = new FileInputStream(mInFilePath);
		byte[] mInXmlBytes = tPubServiceProxy.InputStreamToBytes(mIs);
		String mInXmlStr = new String(mInXmlBytes, "utf-8");
        PubServiceProxy t = new PubServiceProxy();
        t.service(mInXmlStr);
    }
    public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024]; 
			for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}
    
    //判断报文类型
    private boolean whichTypeIs(String XmlStr){
    	String MsgType = XmlStr.substring(XmlStr.indexOf("<MsgType>"),XmlStr.indexOf("</MsgType>"));
		MsgType = MsgType.substring(MsgType.indexOf(">")+1);
		String tSQL = "select distinct 1 from ldcode where codetype='alreadysent' and code='"+MsgType+"'";
		String tResult = new ExeSQL().getOneValue(tSQL);
		if(tResult!=null && "1".equals(tResult)){
			return true;
		}
		
		return false;
    }
    
    //存储报文
    private void saveXML(String cInXmlStr, String fromWhereToWhere){
    	if("fromCPToCore".equals(fromWhereToWhere)){
    		System.out.println("存储云平台报送至核心的报文...");
    	}else if("fromCoreToCP".equals(fromWhereToWhere)){
    		System.out.println("存储核心返回至云平台的报文...");
    	}
    	String BatchNo = cInXmlStr.substring(cInXmlStr.indexOf("<BatchNo>"),cInXmlStr.indexOf("</BatchNo>"));
    	BatchNo = BatchNo.substring(BatchNo.indexOf(">") + 1);
    	String theFilePath = new ExeSQL().getOneValue("select sysvarvalue from ldsysvar where sysvar='ServerRoot' ");
    	theFilePath = theFilePath + fromWhereToWhere + "/";
    	File dir = new File(theFilePath);
    	if(!dir.exists()){
    		dir.mkdir();
    	}
    	theFilePath = theFilePath + BatchNo + ".txt";
    	
    	FileWriter fw = null;
		try{
			fw = new FileWriter(new File(theFilePath));
			fw.write(cInXmlStr);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try {
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("存储完成。");
    }
}
