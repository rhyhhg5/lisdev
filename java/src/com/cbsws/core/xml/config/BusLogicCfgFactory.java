/**
 * 2010-8-23
 */
package com.cbsws.core.xml.config;

import org.apache.log4j.Logger;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.IBusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * @author LY
 *
 */
public final class BusLogicCfgFactory
{
    private final static Logger mLogger = Logger.getLogger(BusLogicCfgFactory.class);

    private BusLogicCfgFactory()
    {
    }

    public static IBusLogic getNewInstanceBusLogic(MsgCollection cMsgInfos)
    {
    	MsgHead tMsgHead = cMsgInfos.getMsgHead();
        System.out.println("tMsgHead="+tMsgHead);
        if (tMsgHead == null)
        {
            mLogger.error("获取报头信息失败。");
            return null;
        }

        String tBusLogicFlag = tMsgHead.getMsgType();
        System.out.println("tBusLogicFlag="+tBusLogicFlag);
        String transType = tBusLogicFlag;
        String sql = "select TransDetail,transName from WFTransInfo where TransType='"
			+ transType + "' and transstate = '1' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sResult;
		sResult = tExeSQL.execSQL(sql);
		String TransDetail = "";
		String transName = "";
		if (sResult.MaxRow > 0) {
			TransDetail = sResult.GetText(1, 1);
			transName = sResult.GetText(1, 2);
			// 日志2--调用某个交易的java类
			LoginVerifyTool.writeTransTypeLog(transName);
		}
		if (TransDetail == null || TransDetail.equals(""))
        {
        	System.out.println("TransDetail="+TransDetail);
            return null;
        }
        IBusLogic tBusLogic = null;
        try
        {
            tBusLogic = (IBusLogic) Class.forName(TransDetail).newInstance();
        }
        catch (Exception ex)
        {
        	System.out.println("ex");
            ex.printStackTrace();
            return null;
        }

        return tBusLogic;
    }
}
