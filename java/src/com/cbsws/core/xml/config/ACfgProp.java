/**
 * 2010-12-21
 */
package com.cbsws.core.xml.config;

import org.apache.log4j.Logger;

/**
 * @author LY
 *
 */
public abstract class ACfgProp implements ICfgProp
{
    protected Logger mLogger = Logger.getLogger(getClass().getName());

    public abstract String getProperty(String cPropKey);

    public abstract void cleanCache();
}
