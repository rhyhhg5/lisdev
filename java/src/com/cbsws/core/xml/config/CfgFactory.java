/**
 * 2010-12-22
 */
package com.cbsws.core.xml.config;

import org.apache.log4j.Logger;

/**
 * @author LY
 *
 */
public final class CfgFactory
{
    private final static Logger mLogger = Logger.getLogger(CfgFactory.class.getName());

    private CfgFactory()
    {
    }

    public static ICfgProp getConfigProperties(String cCfgType)
    {

        ICfgProp tICfgProp = null;

        if ("CfgBusLogicCode".equals(cCfgType))
        {
            tICfgProp = CfgBusLogicImp.getInstance();
            return tICfgProp;
        }

        try
        {
            if (cCfgType == null || cCfgType.equals(""))
            {
                mLogger.error("�����ļ�����Ϊ�ա�");
                return null;
            }

            String tPropName = cCfgType + ".properties";
            tICfgProp = new CfgDefaultCommImp(tPropName);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return tICfgProp;
    }
}
