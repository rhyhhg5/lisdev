/**
 * 2010-12-22
 */
package com.cbsws.core.xml.config;

/**
 * @author LY
 *
 */
public interface ICfgProp
{
    public String getProperty(String cPropKey);

    public void cleanCache();
}
