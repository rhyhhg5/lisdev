/**
 * 2010-8-23
 */
package com.cbsws.core.xml.ctrl;

/**
 * @author LY
 *
 */
public interface IBusLogic
{
    public MsgCollection service(MsgCollection cMsgInfos);
}
