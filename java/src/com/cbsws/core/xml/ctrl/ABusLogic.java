/**
 * 2010-8-23
 */
package com.cbsws.core.xml.ctrl;

import org.apache.log4j.Logger;

import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.util.MsgUtil;
import com.cbsws.core.xml.xsch.BaseXmlSch;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * @author LY
 *
 */
public abstract class ABusLogic implements IBusLogic
{
    protected Logger mLogger = Logger.getLogger(getClass().getName());

    protected MsgCollection mResMsgCols = null;

    public ABusLogic()
    {
        mLogger.info("start:" + getClass().getName());
    }

    public final MsgCollection service(MsgCollection cMsgInfos)
    {
        mResMsgCols = MsgUtil.getResMsgCol(cMsgInfos);
        if (!deal(cMsgInfos))
        {
            MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
            if (tResMsgHead.getErrCode() == null)
            {
                errLog("逻辑处理中出现，未知异常");
            }
        }
        else
        {
            MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
            tResMsgHead.setState("00");
        }
        return getResult();
    }

    protected final void errLog(String cErrInfo)
    {
        mLogger.error("ErrInfo:" + cErrInfo);

        String tErrCode = "99999999";
        MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
        tResMsgHead.setState("01");
        tResMsgHead.setErrCode(tErrCode);
        tResMsgHead.setErrInfo(cErrInfo);
    }
    protected final void errLog1(String cErrInfo)
    {
    	mLogger.error("ErrInfo:" + cErrInfo);
    	String tErrCode = "";
    	ExeSQL exeSql = new ExeSQL();
    	String sql = "select code,codename from ldcode where codetype='errorCode' with ur";
    	SSRS result = exeSql.execSQL(sql);
    	for(int i=1;i<=result.getMaxRow();i++){
    		String codename = result.GetText(i, 2);
    		if(cErrInfo.contains(codename)){
    			tErrCode = result.GetText(i, 1);
    			break;
    		}
    	}
    	if(tErrCode.equals("")||tErrCode==null){
    		tErrCode = "99999999";
    	}
    	MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
    	tResMsgHead.setState("01");
    	tResMsgHead.setErrCode(tErrCode);
    	tResMsgHead.setErrInfo(cErrInfo);
    }
    protected final void errLog2(String cErrInfo)
    {
    	mLogger.error("ErrInfo:" + cErrInfo);
    	String tErrCode = "";
    	if(tErrCode.equals("")||tErrCode==null){
    		tErrCode = "err001";
    	}
    	MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
    	tResMsgHead.setState("00");
    	tResMsgHead.setErrCode(tErrCode);
    	tResMsgHead.setErrInfo(cErrInfo);
    }

    protected MsgCollection getResult()
    {
        return mResMsgCols;
    }

    protected final void putResult(String cNodeFlag, BaseXmlSch cBxsch)
    {
        mResMsgCols.setBodyByFlag(cNodeFlag, cBxsch);
    }

    protected abstract boolean deal(MsgCollection cMsgInfos);
}
