/**
 * 2010-8-23
 */
package com.cbsws.core.xml.ctrl.pack;

import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.util.XschUtil;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public class MsgXschParse
{
    private final static Logger mLogger = Logger.getLogger(MsgXmlParse.class);

    public Document deal(MsgCollection cMsgInfos)
    {
        MsgCollection tMsgCols = cMsgInfos;

        Document tMsgXmlDoc = null;

        Element tEleRoot = new Element("DataSet");

        // 处理返回报文头
        Element tEleMsgResHead = createResMsgHead(tMsgCols);
        if (tEleMsgResHead == null)
        {
            return null;
        }
        tEleRoot.addContent(tEleMsgResHead);
        // --------------------

        // 处理消息体，不做阻断，找不到类属于正常情况，仅输出进行提示。
        if (!createBodyNotes(tEleRoot, tMsgCols))
        {
            mLogger.info("部分节点不存在对应对象信息，或Xsch自动转换失败。");
        }
        // --------------------

        tMsgXmlDoc = new Document(tEleRoot);

        return tMsgXmlDoc;
    }

    private Element createResMsgHead(MsgCollection cMsgInfos)
    {
        String tSubNodeFlag = "Item";
        Element tEleRoot = new Element("MsgResHead");

        MsgResHead tMsgResHead = cMsgInfos.getMsgResHead();
        if (tMsgResHead == null)
        {
            mLogger.error("返回报头信息读取失败。");
            return null;
        }

        Element tSubNode = XschUtil.swapSch2X(tMsgResHead, tSubNodeFlag);
        tEleRoot.addContent(tSubNode);

        return tEleRoot;
    }

    private boolean createBodyNotes(Element cEleRoot, MsgCollection cMsgInfos)
    {
        String tSubNodeFlag = "Item";
        Element tEleRoot = cEleRoot;

        List tBodyList = cMsgInfos.getBodyIdxList();
        if (tBodyList == null || tBodyList.size() == 0)
        {
            mLogger.error("返回报头信息读取失败。");
            return false;
        }

        for (int idx = 0; idx < tBodyList.size(); idx++)
        {
            String tNodeName = (String) tBodyList.get(idx);
            Element tSubNode = new Element(tNodeName);

            List tSNList = cMsgInfos.getBodyByFlag(tNodeName);
            for (int i = 0; i < tSNList.size(); i++)
            {
                BaseXmlSch tBxsch = (BaseXmlSch) tSNList.get(i);
                Element tSINode = XschUtil.swapSch2X(tBxsch, tSubNodeFlag);
                tSubNode.addContent(tSINode);
            }
            tEleRoot.addContent(tSubNode);
        }

        return true;
    }
}
