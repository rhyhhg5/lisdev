/**
 * 2010-8-25
 */
package com.cbsws.core.util;

import org.apache.log4j.Logger;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.xml.ctrl.MsgCollection;

/**
 * @author LY
 *
 */
public final class MsgUtil
{
    private final static Logger mLogger = Logger.getLogger(MsgUtil.class);

    public static MsgCollection getResMsgCol(MsgCollection cMsgCols)
    {
        MsgCollection tResMsgCols = new MsgCollection();

        MsgHead tSrcMsgHead = cMsgCols.getMsgHead();
        if (tSrcMsgHead == null)
        {
            mLogger.error("未获取到报头信息。");
            return null;
        }

        MsgResHead tResMsgHead = new MsgResHead();
        tResMsgHead.setBatchNo(tSrcMsgHead.getBatchNo());
        tResMsgHead.setSendDate(tSrcMsgHead.getSendDate());
        tResMsgHead.setSendTime(tSrcMsgHead.getSendTime());

        tResMsgHead.setBranchCode(tSrcMsgHead.getBranchCode());
        tResMsgHead.setSendOperator(tSrcMsgHead.getSendOperator());

        tResMsgHead.setMsgType(tSrcMsgHead.getMsgType());

        tResMsgCols.setMsgResHead(tResMsgHead);

        return tResMsgCols;
    }
}
