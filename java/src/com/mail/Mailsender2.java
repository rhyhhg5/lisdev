package com.mail;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import weblogic.wtc.jatmi.tsession;

import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Mailsender2 {
	private static String host;// 发送方邮箱所在的smtp主机

	private static String sender;// 发送方邮箱

	private static String password;// 密码

	private static String username;//

	private String mCurrentDate = PubFun.getCurrentDate();

	private ExeSQL mExeSQL = new ExeSQL();

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	public static void init() {
		host = "10.252.36.9";
		sender = "eshop@picchealth.com";
		username = "eshop";
		password = "11111111";
		// Properties prop=new Properties();
		// InputStream inputStream =
		// Mailsender2.class.getClassLoader().getResourceAsStream("config.properties");
		// try {
		// prop.load(inputStream);
		// host = prop.getProperty("mail_host");
		// sender = prop.getProperty("mail_sender");
		// password = prop.getProperty("mail_password");
		// username = prop.getProperty("mail_username");
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}

	/**
	 * 发送短信
	 */
	public boolean sendMsg(List contnomapList, String s) {
		System.out.println("短信通知批处理开始......。。。。");
		SmsServiceSoapBindingStub binding = null;
		try {
			binding = (SmsServiceSoapBindingStub) new SmsServiceServiceLocator()
					.getSmsService(); // 创建binding对象
		} catch (javax.xml.rpc.ServiceException jre) {
			jre.printStackTrace();
		}

		binding.setTimeout(60000);
		Response value = null; // 声明Response对象，该对象将在提交短信后包含提交的结果。

		Vector vec = new Vector();
		if (s.equals("1")) {

			vec = getMessage(contnomapList);
		} else {
			vec = getMessage2(contnomapList);
		}
		Vector tempVec = new Vector(); // 临时变量--实际发短信时传的参数
		// 拆分Vector
		System.out.println("---------vec=" + vec.size());

		if (!vec.isEmpty()) {
			for (int i = 0; i < vec.size(); i++) {
				tempVec.clear();
				tempVec.add(vec.get(i));

				SmsMessages msgs = new SmsMessages(); // 创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
				msgs.setOrganizationId("0"); // 设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
				msgs.setExtension("false"); // 设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
				msgs.setServiceType("xuqi"); // 设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
				msgs.setStartDate(mCurrentDate); // 设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
				msgs.setStartTime("9:00"); // 设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
				msgs.setEndDate(mCurrentDate); // 设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
				msgs.setEndTime("20:00"); // 设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

				msgs.setMessages((SmsMessage[]) tempVec
						.toArray(new SmsMessage[tempVec.size()])); // 设置该批短信的每一条短信，一批短信可以包含多条短信
				try {
					value = binding.sendSMS("Admin", "Admin", msgs); // 提交该批短信，UserName是短信服务平台管理员分配的用户名，
					// Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
					System.out.println(value.getStatus());
					System.out.println(value.getMessage());
				} catch (RemoteException ex) {
					ex.printStackTrace();
				}
			}
		} else {
			if (s.equals("1")) {

				System.out.print("没有符合类型1的短信！");
			}
			if (s.equals("2")) {
				System.out.println("没有符合类型2的短信！");
			}
		}
		System.out.println("客户短信通知批处理正常结束......");
		return true;
	}

	/*
	 * 发送短信格式1
	 */
	private Vector getMessage(List contnomapList) {
		System.out.println("进入获取信息 getMessage()------------------------");
		Vector tVector = new Vector();
		for (int i = 0; i < contnomapList.size(); i++) {
			String appName = (String) ((Map) contnomapList.get(i))
					.get("appName");
			String appSex = (String) ((Map) contnomapList.get(i)).get("appSex");
			String contNo = (String) ((Map) contnomapList.get(i)).get("contNo");
			String enddate = (String) ((Map) contnomapList.get(i))
					.get("enddate");
			String sumprem = (String) ((Map) contnomapList.get(i))
					.get("sumprem");
			String mobile = (String) ((Map) contnomapList.get(i)).get("mobile");

			// 短信内容
			String tMSGContents = "";
			SmsMessage tKXMsg = new SmsMessage(); // 创建SmsMessage对象，定义同上文下行短信格式中的Message元素

			tMSGContents = "尊敬的" + appName + (appSex.equals("0") ? "先生" : "女士")
					+ "：您好！您的保单号为" + contNo + "的保单将于" + changeData(enddate)
					+ "24时满期，请您尽快到公司网站缴纳续保保费" + sumprem + "元，祝您健康。客服电话：95591。";
			System.out.println(tMSGContents);
			tKXMsg.setReceiver(mobile);
			tKXMsg.setContents(tMSGContents);
			tVector.add(tKXMsg);
			// // 在LCCSpec表中插入值
			// ********************************************************
			// LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
			// tLCCSpecSchema.setGrpContNo("000"); //此字段非空
			// tLCCSpecSchema.setContNo(contNo); // 暂定为卡号(此表中该字段为联合主键)
			// tLCCSpecSchema.setProposalContNo("000"); //此字段非空
			// tLCCSpecSchema.setSerialNo(orderID);//暂定为订单号（此表中该字段为联合主键）
			// // tLCCSpecSchema.setPrtSeq(orderID);
			// // String tLimit = PubFun.getNoLimit(tManageCom);
			// // String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
			// // tLCCSpecSchema.setEndorsementNo(tGetNoticeNo);
			// tLCCSpecSchema.setOperator("001"); //暂定为001
			// tLCCSpecSchema.setSpecContent(tMSGContents+"客户手机号："+ mobile);
			// tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
			// tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
			// tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
			// tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
			// tLCCSpecSchema.getDB().insert();
		}
		System.out
				.println("都结束了----------------------------------------------------------------");
		return tVector;
	}

	/*
	 * 发送短信格式2
	 */
	private Vector getMessage2(List contnomapList) {
		System.out.println("进入获取信息 getMessage2()------------------------");
		Vector tVector = new Vector();
		for (int i = 0; i < contnomapList.size(); i++) {
			String appName = (String) ((Map) contnomapList.get(i))
					.get("appName");
			String appSex = (String) ((Map) contnomapList.get(i)).get("appSex");
			String contNo = (String) ((Map) contnomapList.get(i)).get("contNo");
			String enddate = (String) ((Map) contnomapList.get(i))
					.get("enddate");
			String sumprem = (String) ((Map) contnomapList.get(i))
					.get("sumprem");
			String mobile = (String) ((Map) contnomapList.get(i)).get("mobile");

			// 短信内容
			String tMSGContents = "";
			SmsMessage tKXMsg = new SmsMessage(); // 创建SmsMessage对象，定义同上文下行短信格式中的Message元素

			tMSGContents = "尊敬的" + appName + (appSex.equals("0") ? "先生" : "女士")
					+ "：您好！您的保单号为" + contNo + "的保单已于" + changeData(enddate)
					+ "24时满期，请您尽快到公司网站缴纳续保保费" + sumprem + "元，祝您健康。客服电话：95591。";
			System.out.println(tMSGContents);
			tKXMsg.setReceiver(mobile);
			tKXMsg.setContents(tMSGContents);
			tVector.add(tKXMsg);

		}
		System.out
				.println("都结束了----------------------------------------------------------------");
		return tVector;
	}

	/**
	 * 发送普通邮件
	 * 
	 * @param receiver
	 *            收件人
	 * @param subject
	 *            主题
	 * @param contents
	 *            内容
	 * @return true-成功 false-失败
	 */
	public static boolean sendEmail(String receiver, String subject,
			String contents) {
		init();
		try {
			String typeName = "中国人民健康保险股份有限公司";// 发送方名称
			String name = sender.substring(0, sender.indexOf('@'));
			Properties props = new Properties();
			// Setup mail server
			props.put("mail.smtp.host", host);// 设置smtp主机
			props.put("mail.smtp.auth", "true");// 使用smtp身份验证
			// Get session
			Session session = Session.getDefaultInstance(props, null);
			// Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender, typeName));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					receiver));
			message.setSubject(subject);
			message.setContent(contents, "text/html;charset=GBK");
			message.saveChanges();
			// Send message
			Transport transport = session.getTransport("smtp");
			System.out.println("******正在连接" + host);
			transport.connect(host, name, password);
			System.out.println("******正在发送给" + receiver);
			transport.sendMessage(message, message.getAllRecipients());
			System.out.println("******邮件发送成功");
			return true;
		} catch (Exception e) {
			System.out.println("发送普通邮件异常" + e);
			return false;
		}
	}

	/*
	 * 发送普通邮件格式1
	 */
	public static String getContent(String appName, String appSex,
			String totalMoney, String contNo, SSRS tssrs) {
		String managecom = new ExeSQL().getOneValue("select managecom from lccont where contno='"+contNo+"'");
		StringBuffer comSQL = new StringBuffer();
		comSQL.append("select address,zipcode from ldcom "+
				" where comcode = '"+managecom+"' ");
		SSRS comSSRS = new ExeSQL().execSQL(comSQL.toString());
		String contents = "<head>\n";
		contents = contents
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=gb2312\">\n";
		contents = contents + "</head>\n";
		contents = contents + "\n";
		contents = contents
		+ "<img src=\"http://eshop.picchealth.com/newSales/img/tlogo.png\">\n";
		contents = contents
		+ "\n";
		contents = contents
				+ "<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;个险续期续保缴费通知书</h2>\n";
		contents = contents
				+ "<table width=700 cellpadding=0 cellspacing=0 border=0 style=\"border-color:black;\">\n";
		contents = contents + "	<tr>\n";
		contents = contents + "		<td>\n";
		contents = contents + "			<table border=0  line-height:20pt'>\n";
		contents = contents + "				<tr>\n";
		contents = contents + "					<td align=left colspan=5>\n";
		contents = contents
				+ "						<table border=0 width=100% line-height:18pt'>\n";
		contents = contents + "							<tr>\n";
		contents = contents + "								<td>\n";
		contents = contents + "		 						尊敬的" + appName
				+ (appSex.equals("0") ? "先生" : "女士") + "：\n";
		contents = contents
				+ "		 							<br>&nbsp;&nbsp;&nbsp;&nbsp;您好！感谢您对我公司的支持和信赖。您在本公司投保的健康保险保单续期续保日已至，请您及时缴纳续期续保保费，本次缴费详细情况如下：";
		contents = contents + "                                  <br>保单号："
				+ contNo
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应交保费合计:"
				+ totalMoney + "\n";
		contents = contents
				+ "		 							<br>缴费方式：银行转账&nbsp;&nbsp;&nbsp;&nbsp;应缴费日期："
				+ tssrs.GetText(1, 8) + "&nbsp;&nbsp;&nbsp;&nbsp;缴费截止日期："
				+ tssrs.GetText(1, 9) + "\n";
		contents = contents + "		 						</td>\n";
		contents = contents + "		 					</tr>\n";
		contents = contents + "		 				</table>\n";
		contents = contents + "					</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "  <tr>\n";
		contents = contents + "  <td>\n";
		contents = contents
				+ "<table width=700 cellpadding=0 cellspacing=0 border=1 style=\"border-color:black;\">\n";
		contents = contents + "   <tr>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          险种序号\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          险种名称\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          被保险人\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          保险金额/档次\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          投保时间\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          本次缴费保险期间\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          缴费频次\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          险种保费\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          保费性质\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          本公司续保结论\n";
		contents = contents + "     </td>\n";
		contents = contents + "   </tr>\n";
		for (int i = 1; i <= tssrs.getMaxRow(); i++) {
			contents = contents + "   <tr>\n";
			contents = contents + "     <td>\n";
			contents = contents + i + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 2) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 3) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 4) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 5) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + "          一年\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + "          年缴\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 6) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + "          续期/续保\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + "          同意续保\n";
			contents = contents + "     </td>\n";
			contents = contents + "   </tr>\n";
		}

		contents = contents + "                     </table>\n";
		contents = contents + "                 </td>\n";
		contents = contents + "             </tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
		+ "					<td width=100% align=left style='font-size:12pt;color:#000000;\";line-height:18pt'>&nbsp;&nbsp;&nbsp;&nbsp;顺祝身体健康，万事如意！</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				\n";
		contents = contents
				+ "					<td width=100% align=right style='font-size:12pt;color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;中国人民健康保险股份有限公司</td>\n";
		contents = contents + "				</tr>\n";

		contents = contents + "			</table>\n";
		contents = contents + "	</td>\n";
		contents = contents + "</tr>\n";
		contents = contents + "<tr>\n";
		contents = contents + "<td>\n";
		contents = contents + "<br>缴费通知：\n";
		contents = contents + "<ul>\n";
		contents = contents + "<li>\n";
		contents = contents
				+ " 您的缴费方式为：银行转账。请于"+tssrs.GetText(1, 9) +"二十四时前到我公司的电子商务网站，按应缴保费合计所列金额缴纳保费，若您未在规定的期限内缴纳保费，按照保险合同条款约定，保障责任将被中止，为保证您的保障权益不受影响，请及时缴纳保险费。\n";
		contents = contents + "</li>\n";
		contents = contents + "<li>\n";
		contents = contents
				+ " 我公司收到保费后将向您的电子邮箱发送《个险续期续保保费对账单》，若您在缴费后10日内未能收到对账单，请登录我公司网上商城个人中心或致电我公司服务热线查询，客服热线：95591、4006695518，网上商城：http://eshop.picchealth.com。\n";
		contents = contents + "</li>\n";
		contents = contents + "<li>\n";
		contents = contents
				+ "以上险种列表中，标明“保证续保”或“同意续保”的为应办理续保的险种，在您缴费后原责任将自动延续一年，本公司将在对账单上对新一年的保障责任给予批注和明确；标明“终止续保”的险种到期后自行终止，不再续保。 \n";
		contents = contents + "</li>\n";
		contents = contents + "<li>\n";
		contents = contents
				+ "以上险种列表中，标明“保证续保”或“同意续保”的为应若本公司发出通知后，保单发生变更或理赔的，则本通知书所载内容以变更后情况为准，如您已缴纳新一年度保费，续保结论发生变更，公司将无息退还新一年保费，敬请留意。 \n";
		contents = contents + "</li>\n";
		contents = contents + "</ul>\n";
		contents = contents + "</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
		+ "					<td width=100% align=left style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;服务中心地址："+comSSRS.GetText(1, 1)+"</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
		+ "					<td width=100% align=left style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;邮编："+comSSRS.GetText(1, 2)+"</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				 <tr>\n";
		contents = contents
		+ "				 	<td colspan=5 align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";
		contents = contents + "				 </tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
				+ "					<td width=100% align=right style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;全国统一服务热线：95591或4006695518</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
				+ "					<td width=100% align=right style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;公司网站：www.picchealth.com</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				 <tr>\n";
		contents = contents
				+ "				 	<td colspan=5 align=right>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";
		contents = contents + "				 </tr>\n";
		contents = contents + "</table>\n";
		contents = contents + "\n";
		return contents;
	}

	/*
	 * 发送普通邮件格式2
	 */
	public static String getContent2(String appName, String appSex,
			String totalMoney, String contNo, SSRS tssrs) {
		String managecom = new ExeSQL().getOneValue("select managecom from lccont where contno='"+contNo+"'");
		StringBuffer comSQL = new StringBuffer();
		comSQL.append("select address,zipcode from ldcom "+
				" where comcode = '"+managecom+"' ");
		SSRS comSSRS = new ExeSQL().execSQL(comSQL.toString());
		String contents = "<head>\n";
		contents = contents
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=gb2312\">\n";
		contents = contents + "</head>\n";
		contents = contents + "\n";
		contents = contents
		+ "<img src=\"http://eshop.picchealth.com/newSales/img/tlogo.png\">\n";
		contents = contents
		+ "\n";
		contents = contents
				+ "<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;个险续期续保缴费通知书</h2>\n";
		contents = contents
				+ "<table width=700 cellpadding=0 cellspacing=0 border=0 style=\"border-color:black;\">\n";
		contents = contents + "	<tr>\n";
		contents = contents + "		<td>\n";
		contents = contents + "			<table border=0  line-height:20pt'>\n";
		contents = contents + "				<tr>\n";
		contents = contents + "					<td align=left colspan=5>\n";
		contents = contents
				+ "						<table border=0 width=100% line-height:18pt'>\n";
		contents = contents + "							<tr>\n";
		contents = contents + "								<td>\n";
		contents = contents + "		 						尊敬的" + appName
				+ (appSex.equals("0") ? "先生" : "女士") + "：您好！\n";
		contents = contents
				+ "		 							<br>&nbsp;&nbsp;&nbsp;&nbsp;您好！感谢您对我公司的支持和信赖。您在本公司投保的健康保险保单续期续保日将至，请您及时缴纳续期续保保费，本次缴费详细情况如下:";
		contents = contents + "                                  <br>保单号："
				+ contNo
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应交保费合计:"
				+ totalMoney + "\n";
		contents = contents
				+ "		 							<br>缴费方式：银行转账&nbsp;&nbsp;&nbsp;&nbsp;应缴费日期："
				+ tssrs.GetText(1, 8) + "&nbsp;&nbsp;&nbsp;&nbsp;缴费截止日期："
				+ tssrs.GetText(1, 9) + "\n";
		contents = contents + "		 						</td>\n";
		contents = contents + "		 					</tr>\n";
		contents = contents + "		 				</table>\n";
		contents = contents + "					</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "  <tr>\n";
		contents = contents + "  <td>\n";
		contents = contents
				+ "<table width=700 cellpadding=0 cellspacing=0 border=1 style=\"border-color:black;\">\n";
		contents = contents + "   <tr>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          险种序号\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          险种名称\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          被保险人\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          保险金额/档次\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          投保时间\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          本次缴费保险期间\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          缴费频次\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          险种保费\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          保费性质\n";
		contents = contents + "     </td>\n";
		contents = contents + "     <td>\n";
		contents = contents + "          本公司续保结论\n";
		contents = contents + "     </td>\n";
		contents = contents + "   </tr>\n";
		for (int i = 1; i <= tssrs.getMaxRow(); i++) {
			contents = contents + "   <tr>\n";
			contents = contents + "     <td>\n";
			contents = contents + i + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 2) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 3) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 4) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 5) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + "          一年\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + "          年缴\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + tssrs.GetText(i, 6) + "\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + "          续期/续保\n";
			contents = contents + "     </td>\n";
			contents = contents + "     <td>\n";
			contents = contents + "          同意续保\n";
			contents = contents + "     </td>\n";
			contents = contents + "   </tr>\n";
		}
		contents = contents + "                     </table>\n";
		contents = contents + "                 </td>\n";
		contents = contents + "             </tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
				+ "					<td width=100% align=right style='font-size:12pt;color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;中国人民健康保险股份有限公司</td>\n";
		contents = contents + "				</tr>\n";

		contents = contents + "			</table>\n";
		contents = contents + "	</td>\n";
		contents = contents + "</tr>\n";
		contents = contents + "<tr>\n";
		contents = contents + "<td>\n";
		contents = contents + "<br>缴费通知：\n";
		contents = contents + "<ul>\n";
		contents = contents + "<li>\n";
		contents = contents
				+ " 您的缴费方式为：银行转账。请于二十四时前到我公司的电子商务网站，按应缴保费合计所列金额缴纳保费，若您未在规定的期限内缴纳保费，按照保险合同条款约定，保障责任将被中止，为保证您的保障权益不受影响，请及时缴纳保险费。\n";
		contents = contents + "</li>\n";
		contents = contents + "<li>\n";
		contents = contents
				+ " 我公司收到保费后将向您的电子邮箱发送《个险续期续保保费对账单》，若您在缴费后10日内未能收到对账单，请登录我公司网上商城个人中心或致电我公司服务热线查询，客服热线：95591、4006695518，网上商城：http://eshop.picchealth.com。\n";
		contents = contents + "</li>\n";
		contents = contents + "<li>\n";
		contents = contents
				+ "以上险种列表中，标明“保证续保”或“同意续保”的为应办理续保的险种，在您缴费后原责任将自动延续一年，本公司将在对账单上对新一年的保障责任给予批注和明确；标明“终止续保”的险种到期后自行终止，不再续保。 \n";
		contents = contents + "</li>\n";
		contents = contents + "<li>\n";
		contents = contents
				+ "以上险种列表中，标明“保证续保”或“同意续保”的为应若本公司发出通知后，保单发生变更或理赔的，则本通知书所载内容以变更后情况为准，如您已缴纳新一年度保费，续保结论发生变更，公司将无息退还新一年保费，敬请留意。 \n";
		contents = contents + "</li>\n";
		contents = contents + "</ul>\n";
		contents = contents + "</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
		+ "					<td width=100% align=left style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;服务中心地址："+comSSRS.GetText(1, 1)+"</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
		+ "					<td width=100% align=left style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;邮编："+comSSRS.GetText(1, 2)+"</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				 <tr>\n";
		contents = contents
		+ "				 	<td colspan=5 align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";
		contents = contents + "				 </tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
				+ "					<td width=100% align=right style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;全国统一服务热线：95591或4006695518</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
				+ "					<td width=100% align=right style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;公司网站：www.picchealth.com</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				 <tr>\n";
		contents = contents
				+ "				 	<td colspan=5 align=right>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";
		contents = contents + "				 </tr>\n";
		contents = contents + "</table>\n";
		contents = contents + "\n";
		return contents;
	}

	public static String getContent3(String appName, String appSex,
			String sumprem, String contNo, String date, SSRS tSsrs) {
		String managecom = new ExeSQL().getOneValue("select managecom from lccont where contno='"+contNo+"'");
		StringBuffer comSQL = new StringBuffer();
		comSQL.append("select address,zipcode from ldcom "+
				" where comcode = '"+managecom+"' ");
		SSRS comSSRS = new ExeSQL().execSQL(comSQL.toString());
		String contents = "<head>\n";
		contents = contents
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=gb2312\">\n";
		contents = contents + "</head>\n";
		contents = contents + "\n";
		contents = contents
		+ "<img src=\"http://eshop.picchealth.com/newSales/img/tlogo.png\">\n";
		contents = contents
		+ "\n";
		contents = contents
				+ "<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;个险续期续保保费对账单</h2>\n";
		contents = contents
				+ "<table width=700 cellpadding=0 cellspacing=0 border=0 style=\"border-color:black;\">\n";
		contents = contents + "	<tr>\n";
		contents = contents + "		<td>\n";
		contents = contents + "			<table border=0  line-height:20pt'>\n";
		contents = contents + "				<tr>\n";
		contents = contents + "					<td align=left colspan=5>\n";
		contents = contents
				+ "						<table border=0 width=100% line-height:18pt'>\n";
		contents = contents + "							<tr>\n";
		contents = contents + "								<td>\n";
		contents = contents + "		 						尊敬的" + appName
				+ (appSex.equals("0") ? "先生" : "女士") + "：\n";
		contents = contents
				+ "		 							<br>&nbsp;&nbsp;&nbsp;&nbsp;您好！  本公司已经收到您所缴纳的保费，本次缴费情况如下：";
		contents = contents + "                                  <br>保单号："
				+ contNo
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缴费方式：银行转账\n";
		contents = contents + "		 							<br>实缴金额：" + sumprem
				+ "&nbsp;&nbsp;&nbsp;&nbsp;缴费时间：" + date
				+ "&nbsp;&nbsp;&nbsp;&nbsp;\n";
		contents = contents + "		 						</td>\n";
		contents = contents + "		 					</tr>\n";
		contents = contents + "		 				</table>\n";
		contents = contents + "					</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "  <tr>\n";
		contents = contents + "  <td>\n";
		contents = contents
				+ "<table width=700 cellpadding=0 cellspacing=0 border=1 style=\"border-color:black;\">\n";
		contents = contents + "             <tr>\n";
		contents = contents + "                   <td>\n";
		contents = contents + "                         险种序号\n";
		contents = contents + "                   </td>\n";
		contents = contents + "                   <td>\n";
		contents = contents + "                         险种名称\n";
		contents = contents + "                   </td>\n";
		contents = contents + "                   <td>\n";
		contents = contents + "                         被保险人\n";
		contents = contents + "                   </td>\n";
		contents = contents + "                   <td>\n";
		contents = contents + "                          险种金额/档次\n";
		contents = contents + "                   </td>\n";
		contents = contents + "                   <td>\n";
		contents = contents + "                          保险期间\n";
		contents = contents + "                   </td>\n";
		contents = contents + "                   <td>\n";
		contents = contents + "                          缴费频次\n";
		contents = contents + "                   </td>\n";
		contents = contents + "                   <td>\n";
		contents = contents + "                          险种保费\n";
		contents = contents + "                   </td>\n";
		contents = contents + "                   <td>\n";
		contents = contents + "                         保费性质\n";
		contents = contents + "                   </td>\n";
		contents = contents + "                </tr>\n";
		for (int i = 1; i <= tSsrs.getMaxRow(); i++) {
			contents = contents + "             <tr>\n";
			contents = contents + "                   <td>\n";
			contents = contents + i + "\n";
			contents = contents + "                   </td>\n";
			contents = contents + "                   <td>\n";
			contents = contents + tSsrs.GetText(i, 1) + "\n";
			contents = contents + "                   </td>\n";
			contents = contents + "                   <td>\n";
			contents = contents + tSsrs.GetText(i, 2) + "\n";
			contents = contents + "                   </td>\n";
			contents = contents + "                   <td>\n";
			contents = contents + tSsrs.GetText(i, 3) + "\n";
			contents = contents + "                   </td>\n";
			contents = contents + "                   <td>\n";
			contents = contents + "               一年\n";
			contents = contents + "                   </td>\n";
			contents = contents + "                   <td>\n";
			contents = contents + "               年缴\n";
			contents = contents + "                   </td>\n";
			contents = contents + "                   <td>\n";
			contents = contents + tSsrs.GetText(i, 4) + "\n";
			contents = contents + "                   </td>\n";
			contents = contents + "                   <td>\n";
			contents = contents + "                续保\n";
			contents = contents + "                   </td>\n";
			contents = contents + "                </tr>\n";
		}

		contents = contents + "               </table>\n";
		contents = contents + "	</td>\n";
		contents = contents + "</tr>\n";
		contents = contents + "<tr>\n";
		contents = contents + "  <td>\n";
		contents = contents
				+ "     <br>&nbsp;&nbsp;&nbsp;&nbsp;请您仔细核对以上信息，若与您意愿有不符之处或有其他疑问，请及时与本公司联系，服务热线：95591或4006695518。\n";
		contents = contents + "     <br>&nbsp;&nbsp;&nbsp;&nbsp;祝您身体健康，万事如意！";
		contents = contents + "  </td>\n";
		contents = contents + "</tr>\n";

		contents = contents + "				<tr>\n";
		contents = contents
				+ "					<td width=100% align=right style='font-size:12pt;color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;中国人民健康保险股份有限公司</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "               </hr>";
		contents = contents + "               </br>";
		contents = contents + "				<tr>\n";
		contents = contents
				+ "					<td width=100% align=right style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;服务中心地址："+comSSRS.GetText(1, 1)+"</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
		+ "					<td width=100% align=right style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;邮编："+comSSRS.GetText(1, 2)+"</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
		+ "					<td width=100% align=right style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;全国统一服务热线：95591或4006695518</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				<tr>\n";
		contents = contents
				+ "					<td width=100% align=right style='color:#000000;font-family:\"黑体\";line-height:18pt'>&nbsp;公司网站：www.picchealth.com</td>\n";
		contents = contents + "				</tr>\n";
		contents = contents + "				 <tr>\n";
		contents = contents
				+ "				 	<td colspan=5 align=right>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";
		contents = contents + "				 </tr>\n";
		contents = contents + "</table>\n";
		contents = contents + "\n";
		return contents;
	}

	public String changeData(String date) {
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(df1.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public boolean testSendEmail(String contno, String email) {
		SSRS tSsrs = new SSRS();
		SSRS tSsrs2 = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select riskcode,(select riskname from lmrisk where riskcode = a.riskcode ),insuredname,amnt,cvalidate,prem,Appntname,enddate+1 days,enddate+30 days from lcpol a where contno='"
				+ contno + "' with ur";
		String sql2 = "select appntName,appntsex from lcappnt where contno='"
				+ contno + "' with ur";
		String sql3 = "select sumprem from lccont where contno='" + contno
				+ "' with ur";

		tSsrs = tExeSQL.execSQL(sql);
		tSsrs2 = tExeSQL.execSQL(sql2);
		String sumprem = tExeSQL.getOneValue(sql3);

		return sendEmail(email, "个险续期续保缴费通知书", getContent(tSsrs2.GetText(1, 1),
				tSsrs2.GetText(1, 2), sumprem, contno, tSsrs));
	}

	public boolean testSendMessage(String contno, String mobile) {
		ExeSQL mexeSQL = new ExeSQL();
		List contnomapList = new ArrayList();
		try {

			SSRS tSsrs2 = new SSRS();
			String sql = "select enddate from lcpol a where contno='" + contno
					+ "' with ur";
			String sql2 = "select appntName,appntSex,appntno,addressno from lcappnt where contno='"
					+ contno + "' with ur";
			String enddate = mexeSQL.getOneValue(sql);
			tSsrs2 = mexeSQL.execSQL(sql2);
			String sql3 = "select mobile from lcaddress where customerno='"
					+ tSsrs2.GetText(1, 3) + "' and addressno='"
					+ tSsrs2.GetText(1, 4) + "' with ur";
			String sql4 = "select sumprem from lccont where  contno='" + contno
					+ "' with ur";
			// String mobile = mexeSQL.getOneValue(sql3);
			String sumprem = mexeSQL.getOneValue(sql4);
			Map contnoMap = new HashMap();
			contnoMap.put("appName", tSsrs2.GetText(1, 1));
			contnoMap.put("appSex", tSsrs2.GetText(1, 2));
			contnoMap.put("contNo", contno);
			contnoMap.put("enddate", enddate);
			contnoMap.put("sumprem", sumprem);
			contnoMap.put("mobile", mobile);
			contnomapList.add(contnoMap);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("给保单号为：" + contno + "的保单发送邮件失败");

		}
		Mailsender2 ms = new Mailsender2();
		return ms.sendMsg(contnomapList, "1");
	}

	public static void main(String[] args) {
		//016664673000001	13625296073	kingxin_007@163.com
		//016677446000001	15995753559	ixnwang@gmail.com
		Mailsender2 sender = new Mailsender2();
		System.out.println("短信结果："+sender.testSendMessage("013936587000073", "15611720048"));
		System.out.println("邮件结果："+sender.testSendEmail("013936587000073", "zhangyige@sinosoft.com.cn"));
	}
}
