package com.mail;

import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Properties;

import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.*;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


/**
 * 发送邮件
 * @author 		chiho
 * @version		1.0		2010-05-11	新建
 */

public class MailSender extends Thread{	
	private static String host;//发送方邮箱所在的smtp主机
	private static String sender;//发送方邮箱 
	private static String password;//密码
	private static String username;//
	private static String subject = "爱心1+1”公益行动《电子承保凭证》";
	private static String AppntName = "";//投保人姓名
	private static String AppntSex = "";//投保人性别
	private static String Content = "";//正文内容
	private static String CurrentDate = "";//落款日期
	private static String InsuredName = "";//被保人姓名
	private static String InsuredIDTypeName = "";//被保人证件类型名称
	private static String InsuredIDNo = "";//被保人证件号码
	private static String Cvalidate = "";//生效日期
	private static String Inactivedate = "";//终止日期
	private static String dutyContent = "";//保险责任
	private static String productName = "";//产品名称
	private static String CardNo = "";//保险卡号
	
	private static String Cfg = "";//配置文件名称
	private static String receivers = "";
	
	
	
	public MailSender(String aContent,String aCfg,String asubject){
		Content = aContent;
		Cfg = aCfg;
		subject = asubject;
	}
	
	public MailSender(String tCardNo,String tCvalidate,String tInactivedate,String tdutyContent,String tAppntName,String tAppntSex,String tInsuName,String tInsuIDTypeName,String tInsuDINO,String tCurrentDate){
		CardNo = tCardNo;
		Cvalidate = tCvalidate;
		Inactivedate = tInactivedate;
		dutyContent = tdutyContent;
		productName = "中国儿童少年基金会团体少儿重大疾病保险（明亚B款）";
		AppntName = tAppntName;
		AppntSex = tAppntSex;
		InsuredName =tInsuName;
//		InsuredSex = "0";
		InsuredIDTypeName = tInsuIDTypeName;
		InsuredIDNo = tInsuDINO;
		CurrentDate = tCurrentDate;
	}
	private static void init(){
		Properties prop=new Properties();
		InputStream inputStream = MailSender.class.getClassLoader().getResourceAsStream("config.properties"); 
//		FileInputStream inputStream = null;
//		try {
//			inputStream = new FileInputStream("config.properties");
//		} catch (FileNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		try {
			prop.load(inputStream);
			host = prop.getProperty("mail_host");
			sender = prop.getProperty("mail_sender");
			password = prop.getProperty("mail_password");
			username = prop.getProperty("mail_username");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 发送邮件(暂时不用---不发附件)
	 * @param receiver	邮件接收人
	 * @param subject	邮件主题
	 * @param contents	邮件内容
	 * @return boolean
	 */
	public static boolean send1(String receiver){
		try{
			init();
			Content = getContent(CardNo);
			String typeName = "";//发送方名称
			String name = sender.substring(0, sender.indexOf('@')); 
			Properties props = new Properties();
			//Setup mail server
			props.put("mail.smtp.host", host);//设置smtp主机
			props.put("mail.smtp.auth", "true");//使用smtp身份验证
			//Get session
			Session session = Session.getDefaultInstance(props, null);
			//Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender, typeName));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));	
			message.setSubject(subject);			
			message.setContent(Content, "text/html;charset=GBK");
			message.saveChanges();
			//Send message
			Transport transport = session.getTransport("smtp");	
			transport.connect(host, name, password);	
			transport.sendMessage(message, message.getAllRecipients());
			return true;
		}catch(Exception e){
			System.out.println(e.getMessage());
			return false;
		}	
	}
	
	/**
	 * 发送复杂邮件（发附件）
	 * @param receiver
	 * @param subject
	 * @param contents
	 * @return
	 */
	public static boolean send(String receiver){
		init();
		try{
			Content = getContent(CardNo);
			String typeName = "中国人民健康保险股份有限公司";//发送方名称
//			String name = sender.substring(0, sender.indexOf('@')); 
			Properties props = new Properties();
			//Setup mail server
			props.put("mail.smtp.host", host);//设置smtp主机
			props.put("mail.smtp.auth", "true");//使用smtp身份验证
			//Get session
			Session session = Session.getDefaultInstance(props, null);
			//Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender, typeName));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));	
			message.setSubject(subject);			
			
			//加入正文
			Multipart multipart = new MimeMultipart();//复杂邮件主结构
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(Content,"text/html;charset=\"GB2312\"");
			multipart.addBodyPart(messageBodyPart);
			
			//加入附件1
			messageBodyPart = new MimeBodyPart();
			DataSource datasource1 = new FileDataSource(getProjectLocalPath()+"/printdata/mypdf/"+CardNo+".pdf");
			System.out.println("pdf 地址-------------" + getProjectLocalPath()+"/printdata/mypdf/"+CardNo+".pdf");
			//DataSource datasource1 = new FileDataSource("D:/"+CardNo+".pdf");
			messageBodyPart.setDataHandler(new DataHandler(datasource1)); 
			messageBodyPart.setFileName(MimeUtility.encodeWord(CardNo+".pdf","gb2312", null)); 
			multipart.addBodyPart(messageBodyPart);
			
			message.setContent(multipart);
			message.saveChanges();
			//Send message
			Transport transport = session.getTransport("smtp");
			transport.connect(host, username, password);
			transport.sendMessage(message, message.getAllRecipients());
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}	
	}
	
	
	public static String getContent(String aCardNo) throws Exception{
		String contents="<head>\n";
		contents = contents +"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=gb2312\">\n";
		contents = contents +"</head>\n";
		contents = contents +"\n";
		contents = contents +"<table width='90%' cellpadding=0 cellspacing=0 border=0 style=\"border-color:black;\">\n";
		contents = contents +"	<tr>\n";
		contents = contents +"		<td>\n";
		contents = contents +"			<table width = 100% border=0 style='font-size:12pt;color:#1c5185;font-family:\"幼圆\"; line-height:20pt'>\n";
		contents = contents +"				<tr>\n";
		contents = contents +"					<td align=left colspan=5>\n";
//		contents = contents +"						<table align = 'center'>\n";
//		contents = contents +"							<tr>\n";
//		contents = contents +"								<td>\n";
//		contents = contents +"								<img src='" + getUrlPath() + "common/images/logo_cardpolicy.jpg' width='90%' border='0' />\n";
//		contents = contents +"		 						</td>\n";
//		contents = contents +"		 					</tr>\n";
//		contents = contents +"       				</table>\n";
		contents = contents +"						<table border=0 width=100% style='font-size:12pt;color:#000000;font-family:\"黑体\";line-height:18pt'>\n";
		contents = contents +"							<tr>\n";
		contents = contents +"								<td>\n";
		contents = contents +"		 						尊敬的"+AppntName+(AppntSex.equals("0")?"先生":"女士")+":\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr>\n";
		contents = contents +"								<td style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>\n";
		contents = contents +"		 							<br>&nbsp;&nbsp;&nbsp;&nbsp;您好！恭喜您成功投保“中国人民健康保险股份有限公司中国儿童少年基金会团体少儿重大疾病保险（明亚B款）”。本款产品是中国儿童少年基金会专属，不能对外单独进行商业销售的公益保险产品。\n";
		contents = contents +"		 							<br>&nbsp;&nbsp;&nbsp;&nbsp;您收获的这一份安心，源于您或您的亲友对孤儿和贫困儿童的一片爱心。请登录“爱心1+1”行动官方网站<a href = 'http://1j1.cctf.org.cn'>http://1j1.cctf.org.cn</a>了解更多详情。\n";
		contents = contents +"		 							<br>&nbsp;&nbsp;&nbsp;&nbsp;中国人民健康保险股份有限公司欢迎您成为我们的客户。您的保险卡号为"+aCardNo+"。请妥善保管本邮件作为您的投保凭证。感谢您对我们的支持与信赖。在未来的日子里，愿人保健康带给您健康快乐每一天！\n";
		contents = contents +"		 							<br>\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr>\n";
		contents = contents +"								<td align = 'right' style='font-size:12pt;color:#000000;font-family:\"黑体\";line-height:18pt'>\n";
		contents = contents +"		 						中国人民健康保险股份有限公司\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr>\n";
		contents = contents +"								<td align = 'right'>\n";
		contents = contents +"		 						"+CurrentDate+"\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"		 				</table>\n";
		contents = contents +"						<table border=0 width=100% style='font-size:12pt;color:#000000;font-family:\"黑体\";line-height:18pt'>\n";
		contents = contents +"							<tr>\n";
		contents = contents +"								<td align = 'center'>\n";
		contents = contents +"								人保健康，关注您健康每一刻!\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"       				</table>\n";
		contents = contents +"						<div style='width:100%;margin:0 auto;border:1px solid black;'>\n";
		contents = contents +"						<table align ='center' border=0 style='width:100%;font-size:12pt;color:#000000;font-family:\"黑体\";line-height:20pt'>\n";
		contents = contents +"							<tr>\n";
		contents = contents +"								<td colspan ='2' align = 'center'>\n";
		contents = contents +"								"+productName+"\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
//		contents = contents +"							<tr>\n";
//		contents = contents +"								<td colspan ='2' align = 'center' style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>\n";
//		contents = contents +"								（本卡解释权归属于中国人民健康保险股份有限公司）\n";
//		contents = contents +"		 						</td>\n";
//		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr  style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>\n";
		contents = contents +"								<td align = 'left'>\n";
		contents = contents +"								被保人姓名:\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"								<td>\n";
		contents = contents +"								"+InsuredName+"\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>\n";
		contents = contents +"								<td align = 'left'>\n";
		contents = contents +"								被保人证件类型:\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"								<td>\n";
		contents = contents +"								"+InsuredIDTypeName+"\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>\n";
		contents = contents +"								<td align = 'left'>\n";
		contents = contents +"								被保人证件号码:\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"								<td>\n";
		contents = contents +"								"+InsuredIDNo+"\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>\n";
		contents = contents +"								<td align = 'left'>\n";
		contents = contents +"								保险期限:\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"								<td>\n";
		contents = contents +"								自   "+Cvalidate+"   零时起至   "+Inactivedate+"   零时止\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>\n";
		contents = contents +"								<td align = 'left'>\n";
		contents = contents +"								保险责任:\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"								<td>\n";
		contents = contents +"								"+dutyContent+"\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"							<tr style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>\n";
		contents = contents +"								<td colspan ='2' align = 'right'>\n";
		contents = contents +"								（具体条款可登录我公司网站或“爱心1+1”官方网站查询）\n";
		contents = contents +"		 						</td>\n";
		contents = contents +"		 					</tr>\n";
		contents = contents +"       				</table>\n";
		contents = contents +"       				</div>\n";
		contents = contents +"					</td>\n";
		contents = contents +"				</tr>\n";
		contents = contents +"				<tr>\n";
		contents = contents +"					<td align=right style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>&nbsp;全国客户服务热线：95591或4006695518</td>\n";
		contents = contents +"				</tr>\n";
		contents = contents +"				<tr>\n";
		contents = contents +"					<td  align=right style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>&nbsp;公司网址：<a href = 'http://www.picchealth.com'>http://www.picchealth.com</a></td>\n";
		contents = contents +"				</tr>\n";
		contents = contents +"				<tr>\n";
		contents = contents +"					<td align=right style='font-size:12pt;color:#000000;font-family:\"宋体\";line-height:18pt'>&nbsp;服务邮箱：service@picchealth.com</td>\n";
		contents = contents +"				</tr>\n";
		contents = contents +"			</table>\n";
		contents = contents +"	</td>\n";
		contents = contents +"</tr>\n";
		contents = contents +"</table>\n";
		contents = contents +"\n";
		return contents;
	}
	
	/**
	 * 获取项目所在路径
	 * @return				项目路径
	 * @throws Exception	未找到路径
	 */
	private static String getProjectLocalPath() throws Exception{
		String path = MailSender.class.getResource("").getFile();
		path = URLDecoder.decode(path, "UTF-8");
		path = path.substring(0,path.lastIndexOf("/WEB-INF"));
		String temp = path.substring(0, 5);
		if("file:".equalsIgnoreCase(temp)){
			path = path.substring(5);
		}
		return path;
	}
	
	/**
	 * 获取项目所在路径
	 * @return				项目路径
	 * @throws Exception	未找到路径
	 */
	private static String getUrlPath() throws Exception{
		String sql = "select Sysvarvalue from ldsysvar where sysvar = 'ServerURL' ";
		String Sysvarvalue = new ExeSQL().getOneValue(sql);
		return Sysvarvalue;
	}
	
	/**
	 * 发送邮件，通用。
	 * @param receiver	邮件接收人
	 * @param subject	邮件主题
	 * @param contents	邮件内容
	 * @return boolean
	 */
	public static boolean ComSend(){
		try{
			init(Cfg);
			String[] MailReceivers = receivers.split(";");
			InternetAddress[] sendTo = new InternetAddress[MailReceivers.length]; 
			for (int i = 0; i < MailReceivers.length; i++) 
			{ 
			System.out.println("发送到:" + MailReceivers[i]); 
			sendTo[i] = new InternetAddress(MailReceivers[i]); 
			} 

			String typeName = "";//发送方名称
			String name = sender.substring(0, sender.indexOf('@')); 
			Properties props = new Properties();
			//Setup mail server
			props.put("mail.smtp.host", host);//设置smtp主机
			props.put("mail.smtp.auth", "true");//使用smtp身份验证
			//Get session
			Session session = Session.getDefaultInstance(props, null);
			//Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender, typeName));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
			message.setRecipients(Message.RecipientType.TO, sendTo);
			message.setSubject(subject);			
			message.setContent(Content, "text/html;charset=GBK");
			message.saveChanges();
			//Send message
			Transport transport = session.getTransport("smtp");	
			transport.connect(host, name, password);	
			transport.sendMessage(message, message.getAllRecipients());
			return true;
		}catch(Exception e){
			System.out.println(e.getMessage());
			return false;
		}	
	}
	//通用
	private static void init(String aCfg){
		Properties prop=new Properties();
		InputStream inputStream = MailSender.class.getClassLoader().getResourceAsStream(aCfg); 
		try {
			prop.load(inputStream);
			host = prop.getProperty("mail_host");
			sender = prop.getProperty("mail_sender");
			password = prop.getProperty("mail_password");
			username = prop.getProperty("mail_username");
			receivers = prop.getProperty("mail_receivers");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception {

		String str = getProjectLocalPath();
		System.out.println(str);
		
	}
}
