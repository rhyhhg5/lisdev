package com.mail;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.sinosoft.lis.pubfun.PubFun;

import java.awt.Color;
import java.io.*;
import java.net.URLDecoder;

public class CreatePDF {
	private Font Body;
	private Font H1;
	private Font H2;
	private Font H3;
	private Document document;
	
	private String productName = "";//产品名称
	private String productId = "";//套餐编码
	private String dutyContent = "";//保险责任
	
	private String CardNo = "";//保险卡号
	private String PrintID = "";//保单印刷号
	private String Cvalidate = "";//生效日期
	private String Inactivedate = "";//终止日期
	
	private String AppntName = "";//投保人姓名
	private String AppntSex = "";//投保人性别
	private String InsuredName = "";//被保人姓名
//	private String InsuredSex = "";//被保人性别
	private String InsuredIDTypeName = "";//被保人证件类型名称
	private String InsuredIDNo = "";//被保人证件号码
	private String CurrentDate = "";//被保人证件号码
	
	public CreatePDF(String tCardNo,String tCvalidate,String tInactivedate,String tdutyContent,String tAppntName,String tAppntSex,String tInsuName,String tInsuIDTypeName,String tInsuDINO,String tCurrentDate){
		init();//初始化格式信息
		
		//保单信息及产品规则信息
//		PrintID = "123456";
		CardNo = tCardNo;
		Cvalidate = tCvalidate;
		Inactivedate = tInactivedate;
		dutyContent = tdutyContent;
		productName = "中国儿童少年基金会团体少儿重大疾病保险（明亚B款）";
		AppntName = tAppntName;
		AppntSex = tAppntSex;
		InsuredName =tInsuName;
//		InsuredSex = "0";
		InsuredIDTypeName = tInsuIDTypeName;
		InsuredIDNo = tInsuDINO;
		CurrentDate = tCurrentDate;
		
    	
//    	InsuredSex = InsuredSex.equals("0")?"男":"女";
//		if(InsuredIDType.equals("0")){
//			InsuredIDType = "身份证";
//		}else if(InsuredIDType.equals("1")){
//			InsuredIDType = "护照";
//		}else if(InsuredIDType.equals("2")){
//			InsuredIDType = "军官证";
//		}else if(InsuredIDType.equals("8")){
//			InsuredIDType = "其他";
//		}
	}
	/**
	 * 初始化
	 * @param contList
	 * @param appntList
	 * @param insuList
	 * @throws Exception
	 */
	private void init(){
		try {
			BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
			Body = new Font(bfChinese, 6, Font.NORMAL);
	       	H1 = new Font(bfChinese, 10, Font.BOLD,new Color(01,01,54));
	       	H2 = new Font(bfChinese, 8, Font.BOLD,new Color(01,01,54));
	       	H3 = new Font(bfChinese, 7, Font.BOLD,new Color(01,01,54));
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	/**
	 * 生成PDF
	 * @param list
	 */
	public boolean creatPdf(){
		try { 
	       	document = new Document(PageSize.A5,50,50,25,25);// 建立一个Document对象
			FileOutputStream fos = new FileOutputStream(getProjectLocalPath()+"/printdata/mypdf/"+CardNo+".pdf");
//	       	FileOutputStream fos = new FileOutputStream("D:/"+policyNo+".pdf");
		    PdfWriter.getInstance(document,fos);// 建立一个PdfWriter对象
		    document.open();//打开document对象
		    
		    addLogo();//标志图片
//			addTitle();//标题,标题下面是对应单元格内的文字内容   
		   
			
			// 建立一个表格tb1
			Table tb1 = new Table(1);
		    tb1.setPadding(Float.parseFloat("0"));
		    tb1.setWidth(100);
		    tb1.setBorderWidth(Float.parseFloat("0"));//设置表格边框
			//tb1加入内容
		    CreatePDF.addRow(new String[]{"\n"}, new String[]{"1"}, tb1, Body,Element.ALIGN_LEFT, false);
		    CreatePDF.addRow(new String[]{"尊敬的"+AppntName+(AppntSex.equals("0")?"先生":"女士")+":"}, new String[]{"1"}, tb1, H3, Element.ALIGN_LEFT,false);
		    CreatePDF.addRow(new String[]{"          您好！恭喜您成功投保“中国人民健康保险股份有限公司中国儿童少年基金会团体少儿重大疾病保险（明亚B款）”。本款产品是中国儿童少年基金会专属，不能对外单独进行商业销售的公益保险产品。"}, new String[]{"1"}, tb1, Body, Element.ALIGN_LEFT,false);
		    CreatePDF.addRow(new String[]{"          您收获的这一份安心，源于您或您的亲友对孤儿和贫困儿童的一片爱心。请登录“爱心1+1”行动官方网站http://1j1.cctf.org.cn/ 了解更多详情。"}, new String[]{"1"}, tb1, Body, Element.ALIGN_LEFT,false);
		    CreatePDF.addRow(new String[]{"          中国人民健康保险股份有限公司欢迎您成为我们的客户。您的保险卡号为"+CardNo+"。请妥善保管本邮件作为您的投保凭证。感谢您对我们的支持与信赖。在未来的日子里，愿人保健康带给您健康快乐每一天！"}, new String[]{"1"}, tb1, Body, Element.ALIGN_LEFT,false);
		    CreatePDF.addRow(new String[]{"\n"}, new String[]{"1"}, tb1, Body,Element.ALIGN_LEFT, false);
		    CreatePDF.addRow(new String[]{"中国人民健康保险股份有限公司"}, new String[]{"1"}, tb1, Body,Element.ALIGN_RIGHT, false);
		    CreatePDF.addRow(new String[]{PubFun.getCurrentDate()}, new String[]{"1"}, tb1, Body,Element.ALIGN_RIGHT, false);
		    
		    // 建立一个表格tb2
			Table tb2 = new Table(1);
		    tb2.setPadding(Float.parseFloat("0"));
		    tb2.setWidth(100);
		    tb2.setBorderWidth(Float.parseFloat("0"));//设置表格边框
			//tb2加入内容
		    CreatePDF.addRow(new String[]{"人保健康，关注您健康每一刻!"}, new String[]{"1"}, tb2, H3,Element.ALIGN_CENTER, false);
		    CreatePDF.addRow(new String[]{"\n"}, new String[]{"1"}, tb2, Body,Element.ALIGN_LEFT, false);
            //建立一个表格tb3
            Table tb3 = new Table(9);
            tb3.setPadding(Float.parseFloat("2"));
            tb3.setWidth(95);
            tb3.setBorderWidth(Float.parseFloat("0.5"));//设置表格边框
            
            CreatePDF.addRow(new String[]{productName}, new String[]{"9"}, tb3, H2,Element.ALIGN_CENTER, false);
//            CreatePDF.addRow(new String[]{"（本卡解释权归属于中国人民健康保险股份有限公司）"}, new String[]{"9"}, tb3, Body,Element.ALIGN_CENTER,false);
           
	    	CreatePDF.addRow(new String[]{"被保人姓名:",InsuredName}, new String[]{"2","7"}, tb3, Body,Element.ALIGN_LEFT, false);
	        CreatePDF.addRow(new String[]{"被保人证件类型:",InsuredIDTypeName}, new String[]{"2","7"}, tb3, Body,Element.ALIGN_LEFT, false);
	        CreatePDF.addRow(new String[]{"被保人证件号码:",InsuredIDNo}, new String[]{"2","7"}, tb3, Body,Element.ALIGN_LEFT, false);
            CreatePDF.addRow(new String[]{"保险期限:","自   "+Cvalidate+"   零时起至   "+Inactivedate+"   零时止"}, new String[]{"2","7"}, tb3, Body,Element.ALIGN_LEFT, false);
            CreatePDF.addRow(new String[]{"保险责任:",dutyContent}, new String[]{"2","7"}, tb3, Body,Element.ALIGN_LEFT, false);
            CreatePDF.addRow(new String[]{"（具体条款可登录我公司网站或“爱心1+1”官方网站查询）"}, new String[]{"9"}, tb3, Body,Element.ALIGN_RIGHT, false);
           
		    // 建立一个表格tb7
			Table tb4 = new Table(1);
		    tb4.setPadding(Float.parseFloat("0"));
		    tb4.setWidth(100);
		    tb4.setBorderWidth(Float.parseFloat("0"));//设置表格边框
		    //tb7加入内容
		    CreatePDF.addRow(new String[]{"\n"}, new String[]{"1"}, tb4, Body,Element.ALIGN_CENTER, false);
		    CreatePDF.addRow(new String[]{"全国客户服务热线：95591或4006695518"}, new String[]{"1"}, tb4, Body,Element.ALIGN_RIGHT, false);
		    CreatePDF.addRow(new String[]{"公司网址：http://www.picchealth.com"}, new String[]{"1"}, tb4, Body,Element.ALIGN_RIGHT, false);
		    CreatePDF.addRow(new String[]{"服务邮箱：service@picchealth.com"}, new String[]{"1"}, tb4, Body,Element.ALIGN_RIGHT, false);
		    
		    
            document.add(tb1);
            document.add(tb2);
            document.add(tb3);
            document.add(tb4);
	   		document.close();
	   		return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 添加logo
	 */
	public void addLogo() throws Exception{
		String picPath = getProjectLocalPath() + "/common/images/logo_cardpolicy.jpg";
//		String picPath = "D:/images/logo_cardpolicy.jpg";
		Image logo = Image.getInstance(picPath);
		logo.scalePercent(40);
		logo.setAlignment(Element.ALIGN_CENTER);
		document.add(logo);
	}
	
	/**
	 * 添加标题
	 * @throws Exception
	 */
	public void addTitle() throws Exception{
		Paragraph title = new Paragraph();
		Phrase phrase1=new Phrase("保单号"+PrintID+"详细信息\n",H1);
		Phrase phrase2=new Phrase("套餐编码:  "+productId,H2);
		title.add(phrase1);
		title.add(phrase2);
		title.setAlignment(Element.ALIGN_CENTER);
		document.add(title);
	}
	
	/**
	 * 
	 * @param cells 单元格的内容
	 * @param widths 单元格的宽度
	 * @param tb 行所在的table
	 * @param type 字体的样式
	 * @param isBorder 是否有边框
	 */
	private static void addRow(String[] cells,String[] widths,Table tb ,Font type,int align,boolean isBorder) {
		for(int i = 0; i<cells.length; i++){
			Cell cell =null;
			try {
				cell = new Cell(new Paragraph(cells[i],type));
			} catch (BadElementException e) {}
			cell.setHorizontalAlignment(align); 
		   	if(isBorder==false){
			   cell.setBorder(0);
		   	}
		   	
		   	cell.setColspan(Integer.parseInt(widths[i])); 
		   	tb.addCell(cell);
		}
	}
	
	
	/**
	 * 获取项目所在路径
	 * @return				项目路径
	 * @throws Exception	未找到路径
	 */
	private static String getProjectLocalPath() throws Exception{
		String path = CreatePDF.class.getResource("").getFile();
		path = URLDecoder.decode(path, "UTF-8");
		path = path.substring(0,path.lastIndexOf("/WEB-INF"));
		String temp = path.substring(0, 5);
		if("file:".equalsIgnoreCase(temp)){
			path = path.substring(5);
		}
		return path;
	}
	
	public static void main(String[] args) { 
		CreatePDF cPDF = new CreatePDF("263","","","","","","","","",""); 
		cPDF.creatPdf(); 
	}

}
