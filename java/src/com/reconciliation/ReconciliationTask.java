package com.reconciliation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ReconciliationTask extends TaskThread {
	private String startDate;
	private String endDate;
	private String manageCom;

	public void run() {
		System.out.println("---BatchSendDataTask开始---");
		submitData();
		System.out.println("---BatchSendDataTask结束---");
	}

	private boolean submitData() {
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");
		return true;
	}

	private boolean getInputData() {
		try {
			startDate = "2012-1-1";
			endDate = PubFun.getCurrentDate();
//			manageCom = "8636";
			manageCom = "86%";
		} catch (Exception e) {

			CError.buildErr(this, "接收数据失败");
			return false;
		}
		return true;
	}

	private boolean dealData() {
		SSRS sqlArrivaldateSSRS =null;
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		StringBuffer selPaySqlBuff = new StringBuffer();
		selPaySqlBuff.append("select paymentorfees,payorfeestype,redfield2,redfield3 from reconcileInfo where processingmark = '0' ");
		if (!"".equals(manageCom) || null != manageCom) {
			selPaySqlBuff.append("and manageCom like '" + manageCom + "'");
		}
		tSSRS = tExeSQL.execSQL(selPaySqlBuff.toString());
		try {
			VData adata = new VData();
			MMap amap = new MMap();
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				System.out.println("---调用修改表reconcileInfo的方法---" + tSSRS.GetText(i, 1));
				System.out.println("---调用修改表reconcileInfo的方法---" + tSSRS.GetText(i, 2));
				System.out.println("---调用修改表reconcileInfo的方法---" + tSSRS.GetText(i, 3));
				StringBuffer tSQLBuffer = new StringBuffer();
				//保全无名单增人、保费回补、追加保费上报（收费）
				if ("WZ".equals(tSSRS.GetText(i, 3)) || "BB".equals(tSSRS.GetText(i, 3)) || "ZB".equals(tSSRS.GetText(i, 3))) {
					// 获取实付号码
					String SqlLJAGetEndorse = "select actugetno from LJaGetEndorse where otherno="
							+ "'" + tSSRS.GetText(i, 1) + "'";
					SSRS SSLJAGetEndorse = tExeSQL.execSQL(SqlLJAGetEndorse);
					String actugetno= "";
					if (SSLJAGetEndorse.getMaxRow() > 0) {
						actugetno = SSLJAGetEndorse.GetText(1, 1);
						tSQLBuffer.append("update reconcileInfo set bankAccNo = (select BankAccNo from ljapay where payno = '"
										+ actugetno + "'), ");
						if ("1".equals(tSSRS.GetText(i, 2)) || "2".equals(tSSRS.GetText(i, 2))) {
							String sqlArrivaldate = "select EnterAccDate from ljapay where payno = '"
									+ actugetno + "'";
							sqlArrivaldateSSRS = tExeSQL.execSQL(sqlArrivaldate);
							if (sqlArrivaldateSSRS.getMaxNumber() > 0) {
								if (!"".equals(sqlArrivaldateSSRS.GetText(1, 1)) && null != sqlArrivaldateSSRS.GetText(1, 1)) {
									tSQLBuffer.append("financeMark = '1', ");
									tSQLBuffer.append("finaSuccFailDes = '成功', ");
									tSQLBuffer.append("finarrivalDate = '" + sqlArrivaldateSSRS.GetText(1, 1) + "', ");
									tSQLBuffer.append("finarrivalTime = (select a.bankdealTime from lyreturnfrombankb a,ljapay b where  b.payno='"
													+ actugetno + "'  and a.paycode=b.payno ), ");
								} else {
									tSQLBuffer.append("financeMark = '0', ");
									tSQLBuffer.append("finaSuccFailDes = '没有收费', ");
								}
							} else {
								System.out.println("有问题数据保全受理号：  " + tSSRS.GetText(i, 1));
							}
						} else {
							String sqlArrivaldate = "select EnterAccDate from ljapay where payno = '"
									+ actugetno + "'";
							sqlArrivaldateSSRS = tExeSQL.execSQL(sqlArrivaldate);
							if (sqlArrivaldateSSRS.getMaxNumber() > 0) {
								if (!"".equals(sqlArrivaldateSSRS.GetText(1, 1)) && null != sqlArrivaldateSSRS.GetText(1, 1)) {
									tSQLBuffer.append("financeMark = '1', ");
									tSQLBuffer.append("finaSuccFailDes = '成功', ");
									// 如果到账日期不为空，说明收付费成功，获取到账时间
									tSQLBuffer.append("finarrivalDate = '"
											+ sqlArrivaldateSSRS.GetText(1, 1)
											+ "', ");
									tSQLBuffer.append("finarrivalTime = (select a.bankdealTime from lyreturnfrombankb a,ljapay b where  b.payno='"
													+ actugetno + "'  and a.paycode=b.payno ), ");
								} else {
									tSQLBuffer.append("financeMark = '0', ");
									tSQLBuffer.append("finaSuccFailDes = (select codename from ldcode1 where codetype = 'bankerror' and code1 = (select a.banksuccflag from lyreturnfrombankb a,ljapay b where  b.payno='"
													+ actugetno + "' and a.paycode=b.payno)), ");
								}
							} else {
								System.out.println("有问题数据保全受理号：  " + tSSRS.GetText(i, 1));
							}
						}
					}else {
						tSQLBuffer.append("update reconcileInfo set financeMark = '0', ");
						tSQLBuffer.append("finaSuccFailDes = '保全未收费或未结案', ");
					}
					tSQLBuffer.append("modifydate = TO_CHAR(sysdate, 'yyyy-mm-dd'), ");
					tSQLBuffer.append("modifytime = TO_CHAR(sysdate, 'HH24:mi:ss') ");
					tSQLBuffer.append("where 1=1 ");
					tSQLBuffer.append("and paymentorfees= '" + tSSRS.GetText(i, 1) + "' ");
					tSQLBuffer.append("and processingmark = '0' ");
					if (!"".equals(manageCom) || null != manageCom) {
						tSQLBuffer.append("and manageCom like '" + manageCom
								+ "'");
					}
					System.out.println("生成的sql为=========" + tSQLBuffer);

					amap.put(tSQLBuffer.toString(), "UPDATE");
					adata.add(amap);

				}
				
				//结余返还（付费）
				if ("BJ".equals(tSSRS.GetText(i, 3))) {

					// 获取实付号码
					String SqlLJAGetEndorse = "select actugetno from LJaGetEndorse where otherno="
							+ "'" + tSSRS.GetText(i, 1) + "'";
					SSRS SSLJAGetEndorse = tExeSQL.execSQL(SqlLJAGetEndorse);
					String actugetno="";
					if (SSLJAGetEndorse.getMaxRow() > 0) {
						actugetno = SSLJAGetEndorse.GetText(1, 1);
						tSQLBuffer.append("update reconcileInfo set bankAccNo = (select BankAccNo from ljaget where actugetno = '"
										+ actugetno + "'), ");
						if ("1".equals(tSSRS.GetText(i, 2)) || "2".equals(tSSRS.GetText(i, 2))) {
							String sqlArrivaldate = "select EnterAccDate from ljaget where actugetno = '"
									+ actugetno + "'";
							sqlArrivaldateSSRS = tExeSQL.execSQL(sqlArrivaldate);
							if (sqlArrivaldateSSRS.getMaxNumber() > 0) {
								if (!"".equals(sqlArrivaldateSSRS.GetText(1, 1)) && null != sqlArrivaldateSSRS.GetText(1, 1)) {
									tSQLBuffer.append("financeMark = '1', ");
									tSQLBuffer.append("finaSuccFailDes = '成功', ");
									tSQLBuffer.append("finarrivalDate = '" + sqlArrivaldateSSRS.GetText(1, 1) + "', ");
									tSQLBuffer.append("finarrivalTime = (select a.bankdealTime from lyreturnfrombankb a,ljaget b where  b.actugetno='"
													+ actugetno + "'  and a.paycode=b.actugetno ), ");
								} else {
									tSQLBuffer.append("financeMark = '0', ");
									tSQLBuffer.append("finaSuccFailDes = '没有付费', ");
								}
							} else {
								System.out.println("有问题数据保全受理号：  " + tSSRS.GetText(i, 1));
							}
						} else {
							String sqlArrivaldate = "select EnterAccDate from ljaget where actugetno = '"
									+ actugetno + "'";
							sqlArrivaldateSSRS = tExeSQL.execSQL(sqlArrivaldate);
							if (sqlArrivaldateSSRS.getMaxNumber() > 0) {
								if (!"".equals(sqlArrivaldateSSRS.GetText(1, 1)) && null != sqlArrivaldateSSRS.GetText(1, 1)) {
									tSQLBuffer.append("financeMark = '1', ");
									tSQLBuffer.append("finaSuccFailDes = '成功', ");
									// 如果到账日期不为空，说明收付费成功，获取到账时间
									tSQLBuffer.append("finarrivalDate = '"
											+ sqlArrivaldateSSRS.GetText(1, 1)
											+ "', ");
									tSQLBuffer.append("finarrivalTime = (select a.bankdealTime from lyreturnfrombankb a,ljaget b where  b.actugetno='"
													+ actugetno + "'  and a.paycode=b.actugetno ), ");
								} else {
									tSQLBuffer.append("financeMark = '0', ");
									tSQLBuffer.append("finaSuccFailDes = (select codename from ldcode1 where codetype = 'bankerror' and code1 = (select a.banksuccflag from lyreturnfrombankb a,ljaget b where  b.actugetno='"
													+ actugetno + "' and a.paycode=b.actugetno)), ");
								}
							} else {
								System.out.println("有问题数据保全受理号：  " + tSSRS.GetText(i, 1));
							}
						}
					}else {
						tSQLBuffer.append("update reconcileInfo set financeMark = '0', ");
						tSQLBuffer.append("finaSuccFailDes = '保全未付费或未结案', ");
					}
					tSQLBuffer.append("modifydate = TO_CHAR(sysdate, 'yyyy-mm-dd'), ");
					tSQLBuffer.append("modifytime = TO_CHAR(sysdate, 'HH24:mi:ss') ");
					tSQLBuffer.append("where 1=1 ");
					tSQLBuffer.append("and paymentorfees= '" + tSSRS.GetText(i, 1) + "' ");
					tSQLBuffer.append("and processingmark = '0' ");
					if (!"".equals(manageCom) || null != manageCom) {
						tSQLBuffer.append("and manageCom like '" + manageCom
								+ "'");
					}
					System.out.println("生成的sql为=========" + tSQLBuffer);

					amap.put(tSQLBuffer.toString(), "UPDATE");
					adata.add(amap);
				}
				
				//续期约定催收（收费）
				if ("PlanCS".equals(tSSRS.GetText(i, 3))) {
					 
					tSQLBuffer.append("update reconcileInfo set bankAccNo = (select BankAccNo from ljapay where GetNoticeNo = '"
									+ tSSRS.GetText(i, 1) + "'), ");

					if ("1".equals(tSSRS.GetText(i, 2)) || "2".equals(tSSRS.GetText(i, 2))) {
						String sqlArrivaldate = "select EnterAccDate from ljapay where GetNoticeNo = '"
								+ tSSRS.GetText(i, 1) + "'";
						sqlArrivaldateSSRS = tExeSQL.execSQL(sqlArrivaldate);
						if (sqlArrivaldateSSRS.getMaxNumber() > 0) {
							if (!"".equals(sqlArrivaldateSSRS.GetText(1, 1)) && null != sqlArrivaldateSSRS.GetText(1, 1)) {
								tSQLBuffer.append("financeMark = '1', ");
								tSQLBuffer.append("finaSuccFailDes = '成功', ");
								tSQLBuffer.append("finarrivalDate = '" + sqlArrivaldateSSRS.GetText(1, 1) + "', ");
								tSQLBuffer.append("finarrivalTime = (select a.bankdealTime from lyreturnfrombankb a,ljapay b where  b.GetNoticeNo='"
												+ tSSRS.GetText(i, 1) + "'  and a.paycode=b.payno ), ");
							} else {
								tSQLBuffer.append("financeMark = '0', ");
								tSQLBuffer.append("finaSuccFailDes = '没有收费', ");
							}
						} else {
							System.out.println("有问题数据应收记录号：  " + tSSRS.GetText(i, 1));
						}
					} else {
						String sqlArrivaldate = "select EnterAccDate from ljapay where GetNoticeNo = '"
								+ tSSRS.GetText(i, 1) + "'";
						sqlArrivaldateSSRS = tExeSQL.execSQL(sqlArrivaldate);
						if (sqlArrivaldateSSRS.getMaxNumber() > 0) {
							if (!"".equals(sqlArrivaldateSSRS.GetText(1, 1)) && null != sqlArrivaldateSSRS.GetText(1, 1)) {
								tSQLBuffer.append("financeMark = '1', ");
								tSQLBuffer.append("finaSuccFailDes = '成功', ");
								// 如果到账日期不为空，说明收付费成功，获取到账时间
								tSQLBuffer.append("finarrivalDate = '"
										+ sqlArrivaldateSSRS.GetText(1, 1)
										+ "', ");
								tSQLBuffer.append("finarrivalTime = (select a.bankdealTime from lyreturnfrombankb a,ljapay b where  b.GetNoticeNo='"
												+ tSSRS.GetText(i, 1) + "'  and a.paycode=b.payno ), ");
							} else {
								tSQLBuffer.append("financeMark = '0', ");
								tSQLBuffer.append("finaSuccFailDes = (select codename from ldcode1 where codetype = 'bankerror' and code1 = (select a.banksuccflag from lyreturnfrombankb a,ljapay b where  b.GetNoticeNo='"
												+ tSSRS.GetText(i, 1) + "' and a.paycode=b.payno)), ");
							}
						} else {
							System.out.println("有问题数据保全受理号：  " + tSSRS.GetText(i, 1));
						}
					}
					tSQLBuffer.append("modifydate = TO_CHAR(sysdate, 'yyyy-mm-dd'), ");
					tSQLBuffer.append("modifytime = TO_CHAR(sysdate, 'HH24:mi:ss') ");
					tSQLBuffer.append("where 1=1 ");
					tSQLBuffer.append("and paymentorfees= '" + tSSRS.GetText(i, 1) + "' ");
					tSQLBuffer.append("and processingmark = '0' ");
					if (!"".equals(manageCom) || null != manageCom) {
						tSQLBuffer.append("and manageCom like '" + manageCom
								+ "'");
					}
					System.out.println("生成的sql为=========" + tSQLBuffer);

					amap.put(tSQLBuffer.toString(), "UPDATE");
					adata.add(amap);
				}

				//理赔和深入纠错上报（付费）
				if ("lipei".equals(tSSRS.GetText(i, 3)) || "DealClaimBack".equals(tSSRS.GetText(i, 3))) {
					tSQLBuffer.append("update reconcileInfo set bankAccNo = (select BankAccNo from ljaget where actugetno = '"
									+ tSSRS.GetText(i, 1) + "'), ");

					if ("1".equals(tSSRS.GetText(i, 2))	|| "2".equals(tSSRS.GetText(i, 2))) {
						String sqlArrivaldate = "select EnterAccDate from ljaget where actugetno = '"
								+ tSSRS.GetText(i, 1) + "'";
						sqlArrivaldateSSRS = tExeSQL.execSQL(sqlArrivaldate);
						if (sqlArrivaldateSSRS.getMaxNumber() > 0) {
							if (!"".equals(sqlArrivaldateSSRS.GetText(1, 1)) && null != sqlArrivaldateSSRS.GetText(1, 1)) {
								tSQLBuffer.append("financeMark = '1', ");
								tSQLBuffer.append("finaSuccFailDes = '成功', ");
								tSQLBuffer.append("finarrivalDate = '" + sqlArrivaldateSSRS.GetText(1, 1)+ "', ");
								tSQLBuffer.append("finarrivalTime = (select a.bankdealTime from lyreturnfrombankb a,ljaget b where  b.actugetno='"
												+ tSSRS.GetText(i, 1) + "'  and a.paycode=b.actugetno ), ");
							} else {
								tSQLBuffer.append("financeMark = '0', ");
								tSQLBuffer.append("finaSuccFailDes = '没有付费', ");
							}
						} else {
							System.out.println("有问题数据实付号：  " + tSSRS.GetText(i, 1));
						}
					} else {
						String sqlArrivaldate = "select EnterAccDate from ljaget where actugetno = '"
								+ tSSRS.GetText(i, 1) + "'";
						sqlArrivaldateSSRS = tExeSQL.execSQL(sqlArrivaldate);
						if (sqlArrivaldateSSRS.getMaxNumber() > 0) {
							if (!"".equals(sqlArrivaldateSSRS.GetText(1, 1)) && null != sqlArrivaldateSSRS.GetText(1, 1)) {
								tSQLBuffer.append("financeMark = '1', ");
								tSQLBuffer.append("finaSuccFailDes = '成功', ");
								// 如果到账日期不为空，说明收付费成功，获取到账时间
								tSQLBuffer.append("finarrivalDate = (select EnterAccDate from ljaget where actugetno = '"
												+ tSSRS.GetText(i, 1) + "'), ");
								tSQLBuffer.append("finarrivalTime = (select a.bankdealTime from lyreturnfrombankb a,ljaget b where  b.actugetno='"
												+ tSSRS.GetText(i, 1)
												+ "'  and a.paycode=b.actugetno ), ");
							} else {
								tSQLBuffer.append("financeMark = '0', ");
//								tSQLBuffer.append("finaSuccFailDes = (select codename from ldcode1 where codetype = 'bankerror' and code1 = (select a.banksuccflag from lyreturnfrombankb a,ljaget b where  b.actugetno='"
//												+ tSSRS.GetText(i, 1)
//												+ "' and a.paycode=b.actugetno)), ");
								tSQLBuffer.append("finaSuccFailDes = '没有付费或付费失败', ");
							}
						} else {
							System.out.println("有问题数据实付号：  " + tSSRS.GetText(i, 1));
						}
					}
					tSQLBuffer.append("modifydate = TO_CHAR(sysdate, 'yyyy-mm-dd'), ");
					tSQLBuffer.append("modifytime = TO_CHAR(sysdate, 'HH24:mi:ss') ");
					tSQLBuffer.append("where 1=1 ");
					tSQLBuffer.append("and paymentorfees= '" + tSSRS.GetText(i, 1) + "' ");
					tSQLBuffer.append("and processingmark = '0' ");
					if (!"".equals(manageCom) || null != manageCom) {
						tSQLBuffer.append("and manageCom like '" + manageCom + "'");
					}
					System.out.println("生成的sql为=========" + tSQLBuffer);

					amap.put(tSQLBuffer.toString(), "UPDATE");
					adata.add(amap);
				}
				
				//首单
				if ("ShouDan".equals(tSSRS.GetText(i, 3))) {
					tSQLBuffer.append("update reconcileInfo set modifydate = TO_CHAR(sysdate, 'yyyy-mm-dd'), ");
					tSQLBuffer.append("modifytime = TO_CHAR(sysdate, 'HH24:mi:ss') ");
					tSQLBuffer.append("where 1=1 ");
					tSQLBuffer.append("and paymentorfees= '" + tSSRS.GetText(i, 1) + "' ");
					tSQLBuffer.append("and processingmark = '0' ");
					if (!"".equals(manageCom) || null != manageCom) {
						tSQLBuffer.append("and manageCom like '" + manageCom + "'");
					}
					System.out.println("生成的sql为=========" + tSQLBuffer);

					amap.put(tSQLBuffer.toString(), "UPDATE");
					adata.add(amap);
				}
			} 
			
			 PubSubmit puba = new PubSubmit();
			 if (!puba.submitData(adata, "UPDATE")) {
				 System.out.println("更新数据失败，原因是： "+puba.mErrors);
		            return false;
		        }
			
			String path = createTxt();
			String sql = "select description,code from ldconfig where codetype = 'ReconInterface'";
			SSRS sql1SSRS = tExeSQL.execSQL(sql);
			String ip = "";
			String acc = "";
			String pwd = "";
			String port = "";
			String ftpPath = "";
			for (int i = 1; i <= sql1SSRS.MaxRow; i++) {
				if ("IP".equals(sql1SSRS.GetText(i, 1))) {
					ip = sql1SSRS.GetText(i, 2);
				} else if ("AccName".equals(sql1SSRS.GetText(i, 1))) {
					acc = sql1SSRS.GetText(i, 2);
				} else if ("PassWord".equals(sql1SSRS.GetText(i, 1))) {
					pwd = sql1SSRS.GetText(i, 2);
				} else if ("Port".equals(sql1SSRS.GetText(i, 1))) {
					port = sql1SSRS.GetText(i, 2);
				} else if ("Path".equals(sql1SSRS.GetText(i, 1))) {
					ftpPath = sql1SSRS.GetText(i, 2);
				}
			}
			System.out.println("ftp地址： " + ip + "账号： " + acc + "密码： " + pwd
					+ "端口： " + port);
			FTPTool tFTPTool = new FTPTool(ip, acc, pwd, Integer.parseInt(port));
			// 上传txt
			System.out.println("FTP路径：" + ftpPath + "\n文件路径：" + path);
			if (!tFTPTool.loginFTP()) {
				System.out.println("ftp登录失败，原因是:" + tFTPTool.getErrContent(1));
				return false;
			} else {
				if (!tFTPTool.upload(ftpPath, path)) {
					System.out.println("ftp上载失败，原因是:"
							+ tFTPTool.getErrContent(1));
					return false;
				}
			}

			// 删除生成的txt
			if (!deleteFile(path)) {
				System.out.println("删除失败");
				return false;
			}

			StringBuffer sqlUpdateRecon = new StringBuffer();
			sqlUpdateRecon.append("update reconcileInfo set processingmark = '1' ");
			sqlUpdateRecon.append("where 1=1 ");
			sqlUpdateRecon.append("and processingmark = '0' ");
			if (!"".equals(manageCom) || null != manageCom) {
				sqlUpdateRecon.append("and manageCom like '" + manageCom + "'");
			}
			sqlUpdateRecon.append("and financeMark = '1' ");
			System.out.println("update reconcileInfo====" + sqlUpdateRecon.toString());
			if (!tExeSQL.execUpdateSQL(sqlUpdateRecon.toString())) {
				return false;
			}
		} catch (Exception ex) {
			System.out.println("---修改表reconcileInfo报错---TT----");
			ex.printStackTrace();
		}
		return true;
	}

	/**
	 * 生成txt文件
	 * 
	 * @return
	 */
	private String createTxt() {
		String rootPath = getClass().getResource("/").getFile().toString();
		System.out.println(rootPath);
		String filepath = rootPath.replaceAll("WEB-INF/classes/", "");
		filepath += "temp_lp/"+PubFun.getCurrentDate().substring(0, 4)+"duizhang/";
		System.out.println("相对路径：" + filepath);
		String filename = "DZ_" + PubFun.getCurrentDate() + " "
				+ PubFun.getCurrentTime().replace(":", "") + ".txt";
		//最终路径
		String finalPath = filepath + filename;
		System.out.println("最终路径："+finalPath);
		//新建文件夹
		File file1 = new File(filepath);
		ReconciliationTask.judeDirExists(file1);
		//新建文件
		File file = new File(finalPath);
		ReconciliationTask.judeFileExists(file);
		
		BufferedWriter output;
		
		try {
				output = new BufferedWriter(new FileWriter(file));
				StringBuffer sql = new StringBuffer();
				//保全收费对账
				sql.append("select distinct a.recoinfoid,a.otherno,a.othertype,b.name,b.idno,a.bankAccNo,");
				sql.append("a.financeMark,d.ConfMakeDate,d.ConfMakeTime,a.finaSuccFailDes ");
				sql.append("from reconcileInfo a,lcinsured b,lpgrpedoritem c, LJTempFee d ");
				sql.append("where 1=1 ");
				sql.append("AND a.OtherNo = c.edorno ");
				sql.append("AND b.GrpContNo = c.GrpContNo  AND a.OtherNo=d.otherno ");
				sql.append("and a.processingmark = '0' ");
				if (!"".equals(manageCom) || null != manageCom) {
					sql.append("and a.manageCom like '" + manageCom + "'");
				}
				//保全付费对账
				sql.append(" union all ");
				sql.append("select distinct a.recoinfoid,a.otherno,a.othertype,b.name,b.idno,a.bankAccNo,");
				sql.append("a.financeMark,d.EnterAccDate,d.MakeTime,a.finaSuccFailDes ");
				sql.append("from reconcileInfo a,lcinsured b,lpgrpedoritem c, LJAGet d ");
				sql.append("where 1=1 ");
				sql.append("AND a.OtherNo = c.edorno ");
				sql.append("AND b.GrpContNo = c.GrpContNo  AND a.OtherNo=d.otherno ");
				sql.append("and a.processingmark = '0' ");
				if (!"".equals(manageCom) || null != manageCom) {
					sql.append("and a.manageCom like '" + manageCom + "'");
				}
				//理赔对账
				sql.append(" union all ");
				sql.append("select distinct c.caseno,c.rgtno,a.othertype,b.name,b.idno,a.bankAccNo, ");
				sql.append("a.financeMark,d.EnterAccDate,d.ModifyTime,a.finaSuccFailDes ");
				sql.append("from reconcileInfo a,lcinsured b,llcase c,LJAGet d ");
				sql.append("where 1=1 ");
				sql.append("AND (a.OtherNo = c.caseno or a.OtherNo = c.rgtno) ");
				sql.append("AND b.insuredno = c.customerno  AND a.otherno=d.otherno ");
				sql.append("AND a.processingmark = '0'");
				if (!"".equals(manageCom) || null != manageCom) {
					sql.append("and a.manageCom like '" + manageCom + "'");
				}
				//首期对账
				sql.append(" union all ");
				sql.append("select distinct a.recoinfoid,a.otherno,a.othertype,b.name,b.idno,a.bankAccNo, ");
				sql.append("a.financeMark,d.ConfMakeDate,d.ConfMakeTime,a.finaSuccFailDes ");
				sql.append("from reconcileInfo a,lcinsured b,lcgrpcont c, LJTempFee d ");
				sql.append("where 1=1 ");
				sql.append("AND a.OtherNo = b.prtno ");
				sql.append("AND b.grpcontno=c.grpcontno AND a.OtherNo=d.otherno ");
				sql.append("AND a.processingmark = '0'");
				if (!"".equals(manageCom) || null != manageCom) {
					sql.append("and a.manageCom like '" + manageCom + "'");
				}
				//续期
				sql.append(" union all ");
				sql.append("select distinct a.recoinfoid,a.otherno,a.othertype,b.name,b.idno,a.bankAccNo, ");
				sql.append("a.financeMark,d.ConfMakeDate,d.ConfMakeTime,a.finaSuccFailDes ");
				sql.append("from reconcileInfo a,lcinsured b,LJAPayGrp c, LJTempFee d ");
				sql.append("where 1=1 ");
				sql.append("AND a.OtherNo = c.GetNoticeNo  AND a.OtherNo=d.otherno ");
				sql.append("AND b.GrpContNo = c.GrpContNo ");
				sql.append("AND a.processingmark = '0'");
				if (!"".equals(manageCom) || null != manageCom) {
					sql.append("and a.manageCom like '" + manageCom + "'");
				}
				ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = new SSRS();
				tSSRS = tExeSQL.execSQL(sql.toString());
				//String ss = "批次号|业务号|业务类型|用户名|证件号|银行账号|财务成功失败标志|财务到账日期|财务到账时间|财务成功失败描述";
				String ss="";
				output.write(ss);
				//output.newLine();
				if (tSSRS.getMaxRow() > 0) {
					for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
						ss = (tSSRS.GetText(i, 1) + "|" + tSSRS.GetText(i, 2) + "|"
								+ tSSRS.GetText(i, 3) + "|" + tSSRS.GetText(i, 4)
								+ "|" + tSSRS.GetText(i, 5) + "|"
								+ tSSRS.GetText(i, 6) + "|" + tSSRS.GetText(i, 7)
								+ "|" + tSSRS.GetText(i, 8) + "|"
								+ tSSRS.GetText(i, 9) + "|" + tSSRS.GetText(i, 10) +"$"+ "\n");
						output.write(ss);
						output.newLine();
					}
				}

				output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(filepath + filename);
		return filepath + filename;
	}
	
	/**
	 * 判断文件是否存在
	 * @param file
	 */
	public static void judeDirExists(File file){
		if (!file.exists()) {
			System.out.println("文件夹不存在，即将新建");
			file.mkdirs();
		}
	}
	
	/**
	 * 判断文件是否存在
	 * @param file
	 */
	public static void judeFileExists(File file){
		if(!file.exists()){
			try {
				System.out.println("文件不存在，即将新建");
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean deleteFile(String sPath) {
		Boolean flag = false;
		File file = new File(sPath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists()) {
			file.delete();
			flag = true;
		}
		return flag;
	}

	public static void main(String[] args) {
		ReconciliationTask tBatchSendDataTask = new ReconciliationTask();
		tBatchSendDataTask.submitData();
//		tBatchSendDataTask.createTxt();
	}
}
