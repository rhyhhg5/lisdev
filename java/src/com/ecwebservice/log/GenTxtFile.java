package com.ecwebservice.log;

import java.io.File;
import java.io.FileWriter;

public class GenTxtFile {	
	
	private String txtFilePath;
	private String txtContent;
	public boolean flag = false;
		
	public GenTxtFile() {
		super();
	}

	public GenTxtFile(String txtFilePath, String txtContent) 
	{
		this.txtFilePath = txtFilePath;
		this.txtContent = txtContent;
	}
		
	public void writeTxt() 
	{	
		try {
			File txtFile;
			txtFile = new File(txtFilePath);
			if (txtFile.exists() == false)    // 文件不存在时创建
			{
				txtFile.createNewFile();
				txtFile = new File(txtFilePath);
			}
			FileWriter fwriter = new FileWriter(txtFile,true);
			fwriter.write(txtContent);
			fwriter.flush();
			flag = true;
		} catch (Exception d) {
			System.out.println(d.getMessage());
			flag = false;	
		}
	}
	
	public void setTxtFileName(String txtFilePath){
		this.txtFilePath = txtFilePath;
	}
	
	public void setTxtContent(String txtContent) {
		this.txtContent = txtContent;
	}

}