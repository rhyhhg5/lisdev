package com.ecwebservice.singlecard;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.axiom.om.OMElement;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.SimAppntInfo;
import com.cbsws.obj.SimBnfInfo;
import com.cbsws.obj.SimInsuInfo;
import com.cbsws.obj.SimPolicyInfo;
import com.cbsws.obj.WrapExInfo;
import com.cbsws.obj.WrapInfo;

import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.db.WFAppntListDB;
import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.db.WFInsuListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.vschema.WFAppntListSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFContListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * @author grs
 * 第三方平台销售远程出单产品的保单退保接口 
 * 校验 卡号 投保人 姓名
 *
 */
public class SimPolicyWTTradeAll extends ABusLogic
{
    private MsgHead mMsgHead = null;

    private SimPolicyInfo mMsgSimPolicyInfo = null;//保单信息

    private SimAppntInfo mMsgSimAppntInfo = null;//投保人信息

    private SimInsuInfo[] mMsgSimInsuInfoList = null;//被保人信息

    private SimBnfInfo[] mMsgSimBnfInfoList = null;//受益人信息

    private WrapInfo mMsgWrapInfo = null;//产品信息

    private WrapExInfo[] mMsgWrapExInfoList = null;//产品扩展信息

    /**
     * 处理方法
     */
    protected boolean deal(MsgCollection cMsgInfos)
    {
        try
        {
            // 加载报文中相关数据
            if (!parseDatas(cMsgInfos))
            {
                return false;
            }
            // --------------------

            // 处理业务数据
            if (!tradeService())
            {
                return false;
            }
            // --------------------
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            errLog("交易过程出现异常，稍候请尝试重新交易");
            return false;
        }

        return true;
    }
    /**
     * 加载报文中相关数据
     * 获取报文中传送的：
     * 报文头信息 保单信息  投保人信息 被保人信息 受益人信息 产品信息  产品扩展信息
     * @param cMsgInfos
     * @return
     */
    private boolean parseDatas(MsgCollection cMsgInfos)
    {
        // 报文头 
        mMsgHead = cMsgInfos.getMsgHead();
        if (mMsgHead == null)
        {
            errLog("报文头信息缺失。");
            return false;
        }
       
        // 保单信息
        try
        {
            List tSimPolicyInfoList = cMsgInfos.getBodyByFlag("SimPolicyInfo");
            if (tSimPolicyInfoList == null || tSimPolicyInfoList.size() == 0)
            {
                errLog("保单信息缺失。");
                return false;
            }
            else if (tSimPolicyInfoList.size() != 1)
            {
                errLog("保单信息出现多条。");
                return false;
            }
            mMsgSimPolicyInfo = (SimPolicyInfo) tSimPolicyInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 保单信息");
            return false;
        }
        // --------------------

        // 投保人信息
        try
        {
            List tSimAppntInfoList = cMsgInfos.getBodyByFlag("SimAppntInfo");
            if (tSimAppntInfoList == null || tSimAppntInfoList.size() == 0)
            {
                errLog("投保人信息缺失。");
                return false;
            }
            else if (tSimAppntInfoList.size() != 1)
            {
                errLog("投保人信息出现多条。");
                return false;
            }
            mMsgSimAppntInfo = (SimAppntInfo) tSimAppntInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 投保人信息");
            return false;
        }
        // --------------------

        // 被保人信息
        try
        {
            List tSimInsuInfoList = cMsgInfos.getBodyByFlag("SimInsuInfo");
            if (tSimInsuInfoList == null || tSimInsuInfoList.size() == 0)
            {
                errLog("投保人信息缺失。");
                return false;
            }
            mMsgSimInsuInfoList = new SimInsuInfo[tSimInsuInfoList.size()];
            for (int i = 0; i < tSimInsuInfoList.size(); i++)
            {
                SimInsuInfo tSimInsuInfo = null;
                tSimInsuInfo = (SimInsuInfo) tSimInsuInfoList.get(i);
                mMsgSimInsuInfoList[i] = tSimInsuInfo;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 被保人信息");
            return false;
        }
        
        // 受益人信息
        try
        {
            List tSimBnfInfoList = cMsgInfos.getBodyByFlag("SimBnfInfo");
            if (tSimBnfInfoList != null)
            {
                mMsgSimBnfInfoList = new SimBnfInfo[tSimBnfInfoList.size()];
                for (int i = 0; i < tSimBnfInfoList.size(); i++)
                {
                    SimBnfInfo tSimBnfInfo = null;
                    tSimBnfInfo = (SimBnfInfo) tSimBnfInfoList.get(i);
                    mMsgSimBnfInfoList[i] = tSimBnfInfo;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 受益人信息");
            return false;
        }

        // 产品信息
        try
        {
            List tWrapInfoList = cMsgInfos.getBodyByFlag("WrapInfo");
            if (tWrapInfoList == null || tWrapInfoList.size() == 0)
            {
                errLog("产品信息缺失。");
                return false;
            }
            else if (tWrapInfoList.size() != 1)
            {
                errLog("产品信息出现多条。");
                return false;
            }
            mMsgWrapInfo = (WrapInfo) tWrapInfoList.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 产品信息");
            return false;
        }
        // 产品扩展信息
        try
        {
            List tWrapExInfoList = cMsgInfos.getBodyByFlag("WrapExInfo");
            if (tWrapExInfoList != null)
            {
                mMsgWrapExInfoList = new WrapExInfo[tWrapExInfoList.size()];
                for (int i = 0; i < tWrapExInfoList.size(); i++)
                {
                    WrapExInfo tWrapExInfo = null;
                    tWrapExInfo = (WrapExInfo) tWrapExInfoList.get(i);
                    mMsgWrapExInfoList[i] = tWrapExInfo;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("报文信息读取异常 - 产品扩展信息");
            return false;
        }
       
        /*
         * 
         * from jl
         * 
         */
        WFContListSchema tWFContListSchema = null;
        try {
        	System.out.println(mMsgWrapInfo.getCardNo());
        	String tStrSql = "select wf.* from  wfcontlist wf,licardactiveinfolist lic    where wf.cardno = lic.cardno and  "+
        			"  lic.cardstatus in ('01') and wf.cardno ='"+ mMsgWrapInfo.getCardNo()+"' ";
        	WFContListSet tWFContListSets = new  WFContListDB().executeQuery(tStrSql);
        	if (tWFContListSets == null || tWFContListSets.size() == 0)
        	{
        		errLog("投保保单信息缺失。");
        		return false;
        	}else if(tWFContListSets.size() != 1){
        		errLog("保单信息出现多条。");
        		return false;
        	}
        	tWFContListSchema = tWFContListSets.get(1);
			
		} catch (Exception e) {
			e.printStackTrace();
            errLog("报文信息读取异常 - 保单信息");
            return false;
		}
        
        LICardActiveInfoListSchema tLICardActiveInfoListSchema = null;
        try {
        	String tStrSql = "select lic.* from  wfcontlist wf,licardactiveinfolist lic    where wf.cardno = lic.cardno and  "+
        			"  lic.cardstatus in ('01') and wf.cardno ='"+ mMsgWrapInfo.getCardNo()+"' ";
        	LICardActiveInfoListSet tLICardActiveInfoListSets = new  LICardActiveInfoListDB().executeQuery(tStrSql);
        	if (tLICardActiveInfoListSets == null || tLICardActiveInfoListSets.size() == 0)
        	{
        		errLog("投保保单信息缺失。");
        		return false;
        	}else if(tLICardActiveInfoListSets.size() != 1){
        		errLog("保单信息出现多条。");
        		return false;
        	}
        	tLICardActiveInfoListSchema = tLICardActiveInfoListSets.get(1);
			
		} catch (Exception e) {
			e.printStackTrace();
            errLog("报文信息读取异常 - 保单信息");
            return false;
		}
        
        WFAppntListSchema tWFAppntListSchema = null;
        try {
        	String tStrSql = "select wf.* from  wfappntlist wf,licardactiveinfolist lic    where wf.cardno = lic.cardno and  "+
        			"  lic.cardstatus in ('01') and wf.cardno ='"+ mMsgWrapInfo.getCardNo()+"' ";
        	 WFAppntListSet tWFAppntListSet = new  WFAppntListDB().executeQuery(tStrSql);
        	if (tWFAppntListSet == null || tWFAppntListSet.size() == 0)
        	{
        		errLog("投保人信息缺失。");
        		return false;
        	}else if(tWFAppntListSet.size() != 1){
        		errLog("投保人信息出现多条。");
        		return false;
        	}
        	tWFAppntListSchema = tWFAppntListSet.get(1);
		} catch (Exception e) {
			e.printStackTrace();
            errLog("报文信息读取异常 - 投保人信息");
            return false;
		}
        
        
        WFInsuListSet tWFInsuListSet = null;
        try {
        	String tStrSql = "select wf.* from  wfinsulist wf,licardactiveinfolist lic    where wf.cardno = lic.cardno and  "+
        			"  lic.cardstatus in ('01') and wf.cardno ='"+ mMsgWrapInfo.getCardNo()+"' ";
        	tWFInsuListSet = new  WFInsuListDB().executeQuery(tStrSql);
        	if (tWFInsuListSet == null || tWFInsuListSet.size() == 0)
        	{
        		errLog("被保人信息缺失。");
        		return false;
        	}else if(tWFInsuListSet.size() != 1){
        		errLog("被保人信息出现多条。");
        		return false;
        	}
		} catch (Exception e) {
			e.printStackTrace();
            errLog("报文信息读取异常 - 被保人信息");
            return false;
		}
        
        mMsgHead.setMsgType("CI0002");
        mMsgSimPolicyInfo.setCValiDate(tLICardActiveInfoListSchema.getCValidate());
        mMsgSimPolicyInfo.setCInValiDate(tLICardActiveInfoListSchema.getInActiveDate());
        mMsgSimPolicyInfo.setPremScope(tWFContListSchema.getPrem()+"");
        mMsgSimPolicyInfo.setPayIntv("0");
        mMsgSimPolicyInfo.setRemark("PICCTB");
        System.out.println("CInValiDate"+mMsgSimPolicyInfo.getCInValiDate());

        
//        
        mMsgWrapInfo.setCardNo(mMsgWrapInfo.getCardNo());
        mMsgWrapInfo.setWrapCode(tWFContListSchema.getRiskCode());
        mMsgWrapInfo.setCertifyCode(tWFContListSchema.getCertifyCode());
        mMsgWrapInfo.setActiveDate(tWFContListSchema.getActiveDate());//出保日期
        mMsgWrapInfo.setPrem(tWFContListSchema.getPrem()+"");
        mMsgWrapInfo.setMult(tWFContListSchema.getMult()+"");
        mMsgWrapInfo.setCopys(tWFContListSchema.getCopys()+"");
        System.out.println("ActiveDate"+mMsgWrapInfo.getActiveDate());

//        
//        mMsgSimAppntInfo.setName(tWFAppntListSchema.getName());
        mMsgSimAppntInfo.setSex(tWFAppntListSchema.getSex());
        mMsgSimAppntInfo.setBirthday(tWFAppntListSchema.getBirthday());
        mMsgSimAppntInfo.setIDType(tWFAppntListSchema.getIDType());
        mMsgSimAppntInfo.setIdNo(tWFAppntListSchema.getIDNo());
        mMsgSimAppntInfo.setPhone(tWFAppntListSchema.getPhont());
        mMsgSimAppntInfo.setMobile(tWFAppntListSchema.getMobile());
        mMsgSimAppntInfo.setEmail(tWFAppntListSchema.getEmail());
        System.out.println("WFAppntList-IdNo"+mMsgSimAppntInfo.getIdNo());

        
//        
        for(int i=1;i<=tWFInsuListSet.size();i++){
        	WFInsuListSchema tWFInsuListSchema = tWFInsuListSet.get(i);
        	mMsgSimInsuInfoList[i-1].setInsuNo("1");
        	mMsgSimInsuInfoList[i-1].setToMainInsuRela("00");
        	mMsgSimInsuInfoList[i-1].setToAppntRela(tWFInsuListSchema.getToAppntRela());
        	mMsgSimInsuInfoList[i-1].setName(tWFInsuListSchema.getName());
        	mMsgSimInsuInfoList[i-1].setSex(tWFInsuListSchema.getSex());
        	mMsgSimInsuInfoList[i-1].setBirthday(tWFInsuListSchema.getBirthday());
        	mMsgSimInsuInfoList[i-1].setIDType(tWFInsuListSchema.getIDType());
        	mMsgSimInsuInfoList[i-1].setIdNo(tWFInsuListSchema.getIDNo());
        	mMsgSimInsuInfoList[i-1].setOccupationType("1");
        	mMsgSimInsuInfoList[i-1].setOccupationCode("05802");
        	mMsgSimInsuInfoList[i-1].setPhone(tWFInsuListSchema.getPhont());
        	mMsgSimInsuInfoList[i-1].setMobile(tWFInsuListSchema.getMobile());
        	mMsgSimInsuInfoList[i-1].setEmail(tWFInsuListSchema.getEmail());
        	System.out.println("WFInsuList-IdNo"+mMsgSimInsuInfoList[i-1].getIdNo());
        }
        return true;
    }

    
    
    
    private boolean tradeService()
    {
        // 封装保单信息 
        WFContListSchema tWFContInfo = getContInfo();
        if (tWFContInfo == null)
        {
            return false;
        }
        // --------------------

        // 封装投保人信息
        WFAppntListSchema tWFAppntInfo = getAppntInfo();
        if (tWFAppntInfo == null)
        {
            return false;
        }
        // --------------------

        // 封装被保人信息
        WFInsuListSet tWFInsuInfos = getInsuInfos();
        if (tWFInsuInfos == null || tWFInsuInfos.size() == 0)
        {
            return false;
        }
        // --------------------

        // 封装受益人信息
        WFBnfListSet tWFBnfInfos = getBnfInfos();
        if (tWFBnfInfos == null)
        {
            return false;
        }
        // --------------------

        // 进行投保处理（试算-不提交数据库）
        String mBatchNo = mMsgHead.getBatchNo();
        String mSendDate = mMsgHead.getSendDate();
        String mSendTime = mMsgHead.getSendTime();
        String mBranchCode = mMsgHead.getBranchCode();
        String mSendOperator = mMsgHead.getSendOperator();
        String mMsgType = mMsgHead.getMsgType();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", mBatchNo);
        tTransferData.setNameAndValue("SendDate", mSendDate);
        tTransferData.setNameAndValue("SendTime", mSendTime);
        tTransferData.setNameAndValue("BranchCode", mBranchCode);
        tTransferData.setNameAndValue("SendOperator", mSendOperator);
        tTransferData.setNameAndValue("MsgType", mMsgType);

        VData tVData = new VData();
        tVData.add(tWFContInfo);
        tVData.add(tWFAppntInfo);
        tVData.add(tWFInsuInfos);
        tVData.add(tWFBnfInfos);

        tVData.add(tTransferData);

        GlobalInput tGi = new GlobalInput();
        tGi.ManageCom = "86";
        tGi.ComCode = "86";
        tGi.Operator = "001";
        tVData.add(tGi);

        SimPolicyWTBL tSimPolicyWTBL = new SimPolicyWTBL();
        if (!tSimPolicyWTBL.submitData(tVData, "SimPolicyWTTrade"))
        {
            String tErrInfo = tSimPolicyWTBL.mErrors.getFirstError();
            errLog(tErrInfo);
            return false;
        }
        // --------------------

        return true;
    }

    private WFContListSchema getContInfo()
    {
        WFContListSchema tWFContInfo = new WFContListSchema();

        // 基本交易数据
        tWFContInfo.setBatchNo(mMsgHead.getBatchNo());
        tWFContInfo.setSendDate(mMsgHead.getSendDate());
        tWFContInfo.setSendTime(mMsgHead.getSendTime());
        tWFContInfo.setBranchCode(mMsgHead.getBranchCode());
        tWFContInfo.setSendOperator(mMsgHead.getSendOperator());
        tWFContInfo.setMessageType(mMsgHead.getMsgType());
        // --------------------

        // tWFContInfo.setCardDealType("01");

        // 产品数据
        tWFContInfo.setCardNo(mMsgWrapInfo.getCardNo());
        tWFContInfo.setPassword(mMsgWrapInfo.getPassword());
        tWFContInfo.setCertifyCode(mMsgWrapInfo.getCertifyCode());
        tWFContInfo.setCertifyName(mMsgWrapInfo.getCertifyName());
        tWFContInfo.setRiskCode(mMsgWrapInfo.getWrapCode());

        if (mMsgWrapInfo.getPrem() == null || mMsgWrapInfo.getPrem().equals(""))
        {
            String tErrInfo = "承保交易中，承保保费信息不能为空。";
            errLog(tErrInfo);
            return null;
        }
        else
        {
            try
            {
                double tTmpPrem = Double.valueOf(mMsgWrapInfo.getPrem()).doubleValue();
                if (tTmpPrem == 0)
                {
                    String tErrInfo = "承保交易中，承保保费信息不能为0。";
                    errLog(tErrInfo);
                    return null;
                }
            }
            catch (Exception e)
            {
                String tErrInfo = "保费信息为非数值型数据。";
                errLog(tErrInfo);
                return null;
            }
        }

        tWFContInfo.setPrem(mMsgWrapInfo.getPrem());
        tWFContInfo.setAmnt(mMsgWrapInfo.getAmnt());
        tWFContInfo.setMult(mMsgWrapInfo.getMult());
        tWFContInfo.setCopys(mMsgWrapInfo.getCopys());
        // --------------------

        tWFContInfo.setActiveDate(mMsgWrapInfo.getActiveDate());

        // 投保单数据
        tWFContInfo.setManageCom(mMsgSimPolicyInfo.getManageCom());
        tWFContInfo.setSaleChnl(mMsgSimPolicyInfo.getSaleChnl());
        tWFContInfo.setAgentCom(mMsgSimPolicyInfo.getAgentCom());
        tWFContInfo.setAgentCode(mMsgSimPolicyInfo.getAgentCode());











        tWFContInfo.setAgentName(mMsgSimPolicyInfo.getAgentName());
        tWFContInfo.setPayMode(mMsgSimPolicyInfo.getPayMode());
        tWFContInfo.setPayIntv(mMsgSimPolicyInfo.getPayIntv());
        tWFContInfo.setPayYear(mMsgSimPolicyInfo.getPayYear());
        tWFContInfo.setPayYearFlag(mMsgSimPolicyInfo.getPayYearFlag());
        tWFContInfo.setCvaliDate(mMsgSimPolicyInfo.getCValiDate());
        tWFContInfo.setCvaliDateTime(mMsgSimPolicyInfo.getCValiTime());
        tWFContInfo.setInsuYear(mMsgSimPolicyInfo.getInsuYear());
        tWFContInfo.setInsuYearFlag(mMsgSimPolicyInfo.getInsuYearFlag());

        tWFContInfo.setRemark(mMsgSimPolicyInfo.getRemark());
        // --------------------

        // 投保人关联数据 - 暂定用卡号
        tWFContInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        // --------------------

        // 特殊字段-客票号信息暂不存储
        tWFContInfo.setTicketNo(null);
        tWFContInfo.setTeamNo(null);
        tWFContInfo.setSeatNo(null);
        tWFContInfo.setFrom(null);
        tWFContInfo.setTo(null);
        // --------------------

        // 特殊字段 - 投保地域信息暂不存储
        tWFContInfo.setCardArea(null);
        // --------------------

        return tWFContInfo;
    }

    private WFAppntListSchema getAppntInfo()
    {
        WFAppntListSchema tWFAppntInfo = new WFAppntListSchema();

        tWFAppntInfo.setBatchNo(mMsgHead.getBatchNo());

        // 投保人关联数据 - 暂定用卡号
        tWFAppntInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        // --------------------

        tWFAppntInfo.setCardNo(mMsgWrapInfo.getCardNo());

        // 投保人关联数据 - 暂定用卡号
        tWFAppntInfo.setAppntNo(mMsgWrapInfo.getCardNo());
        // --------------------

        tWFAppntInfo.setName(mMsgSimAppntInfo.getName());
        tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
        tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());
        tWFAppntInfo.setIDType(mMsgSimAppntInfo.getIDType());
        tWFAppntInfo.setIDNo(mMsgSimAppntInfo.getIdNo());

        tWFAppntInfo.setOccupationType(mMsgSimAppntInfo.getOccupationType());
        tWFAppntInfo.setOccupationCode(mMsgSimAppntInfo.getOccupationCode());

        tWFAppntInfo.setPostalAddress(mMsgSimAppntInfo.getPostalAddress());
        tWFAppntInfo.setZipCode(mMsgSimAppntInfo.getZipCode());
        tWFAppntInfo.setPhont(mMsgSimAppntInfo.getPhone());
        tWFAppntInfo.setMobile(mMsgSimAppntInfo.getMobile());
        tWFAppntInfo.setEmail(mMsgSimAppntInfo.getEmail());

        tWFAppntInfo.setGrpName(mMsgSimAppntInfo.getGrpName());
        tWFAppntInfo.setCompanyPhone(mMsgSimAppntInfo.getCompayPhone());
        tWFAppntInfo.setCompAddr(mMsgSimAppntInfo.getCompAddr());
        tWFAppntInfo.setCompZipCode(mMsgSimAppntInfo.getCompZipCode());

        return tWFAppntInfo;
    }

    private WFInsuListSet getInsuInfos()
    {
        WFInsuListSet tWFInsuInfos = new WFInsuListSet();

        for (int i = 0; i < mMsgSimInsuInfoList.length; i++)
        {
            SimInsuInfo tSimInsuInfo = mMsgSimInsuInfoList[i];

            WFInsuListSchema tWFInsuInfo = new WFInsuListSchema();

            tWFInsuInfo.setBatchNo(mMsgHead.getBatchNo());

            tWFInsuInfo.setCardNo(mMsgWrapInfo.getCardNo());

            tWFInsuInfo.setInsuNo(tSimInsuInfo.getInsuNo());
            tWFInsuInfo.setToAppntRela(tSimInsuInfo.getToAppntRela());

            tWFInsuInfo.setRelationToInsured(tSimInsuInfo.getToMainInsuRela());

            tWFInsuInfo.setRelationCode(null); // 数据描述为该被保人是否连带，放到业务逻辑中进行处理
            tWFInsuInfo.setReInsuNo(null); // 数据描述为该被保人主被保人编号，放到业务逻辑中进行处理

            tWFInsuInfo.setName(tSimInsuInfo.getName());
            tWFInsuInfo.setSex(tSimInsuInfo.getSex());
            tWFInsuInfo.setBirthday(tSimInsuInfo.getBirthday());
            tWFInsuInfo.setIDType(tSimInsuInfo.getIDType());
            tWFInsuInfo.setIDNo(tSimInsuInfo.getIdNo());

            tWFInsuInfo.setOccupationType(tSimInsuInfo.getOccupationType());
            tWFInsuInfo.setOccupationCode(tSimInsuInfo.getOccupationCode());

            tWFInsuInfo.setPostalAddress(tSimInsuInfo.getPostalAddress());
            tWFInsuInfo.setZipCode(tSimInsuInfo.getZipCode());
            tWFInsuInfo.setPhont(tSimInsuInfo.getPhone());
            tWFInsuInfo.setMobile(tSimInsuInfo.getMobile());
            tWFInsuInfo.setEmail(tSimInsuInfo.getEmail());

            tWFInsuInfo.setGrpName(tSimInsuInfo.getGrpName());
            tWFInsuInfo.setCompanyPhone(tSimInsuInfo.getCompayPhone());
            tWFInsuInfo.setCompAddr(tSimInsuInfo.getCompAddr());
            tWFInsuInfo.setCompZipCode(tSimInsuInfo.getCompZipCode());

            tWFInsuInfos.add(tWFInsuInfo);
        }

        return tWFInsuInfos;
    }

    private WFBnfListSet getBnfInfos()
    {
        WFBnfListSet tWFBnfInfos = new WFBnfListSet();

        if (mMsgSimBnfInfoList != null)
        {
            for (int i = 0; i < mMsgSimBnfInfoList.length; i++)
            {
                SimBnfInfo tSimBnfInfo = mMsgSimBnfInfoList[i];

                WFBnfListSchema tWFInsuInfo = new WFBnfListSchema();

                tWFInsuInfo.setBatchNo(mMsgHead.getBatchNo());

                tWFInsuInfo.setCardNo(mMsgWrapInfo.getCardNo());

                tWFInsuInfo.setToInsuNo(tSimBnfInfo.getToInsuNo());
                tWFInsuInfo.setToInsuRela(tSimBnfInfo.getToInsuNo());

                tWFInsuInfo.setBnfType(tSimBnfInfo.getBnfType());
                tWFInsuInfo.setBnfGrade(tSimBnfInfo.getBnfGrade());
                tWFInsuInfo.setBnfRate(tSimBnfInfo.getBnfRate());

                tWFInsuInfo.setName(tSimBnfInfo.getName());
                tWFInsuInfo.setSex(tSimBnfInfo.getSex());
                tWFInsuInfo.setBirthday(tSimBnfInfo.getBirthday());
                tWFInsuInfo.setIDType(tSimBnfInfo.getIDType());
                tWFInsuInfo.setIDNo(tSimBnfInfo.getIdNo());

                tWFBnfInfos.add(tWFInsuInfo);
            }
        }

        return tWFBnfInfos;
    }

}