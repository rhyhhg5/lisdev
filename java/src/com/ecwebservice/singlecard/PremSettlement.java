package com.ecwebservice.singlecard;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.cbsws.core.services.PubServiceProxy;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * 远程出单 卡单出单统计
 * @author GRS
 *
 */
public class PremSettlement {
	
	
	/** 回复报文 */
	private OMElement responseOME = null;

	private OMElement mOME = null;

	/** 批次号 */
	private String batchno = "";

	/** 报文发送日期 */
	private String sendDate = "";

	/** 报文发送时间 */
	private String sendTime = "";

	/** 报文类型 */
	private String messageType = "";

	/** 投保日期起期 */
	private String startDate = null;

	/** 投保日期止期 */
	private String endDate = null;

	/** 生效日期起期 */
	private String cvalistartDate = null;

	/** 生效日期止期 */
	private String cvaliendDate = null;
	
	/** 交易日期起期 */
	private String bstartDate = null;

	/** 交易日期止期 */
	private String bendDate = null;
	
	/** 卡号题头*/
	private String cardHeading = null;
	/**出单平台*/
	private String salesplatform = null;
	
	private String account = null;
//	/** 套餐编码 */
//	private String wrapcode = null;

	/** 返回信息 */
	private String RACCOUNT = null;
	
	private String CERTIFYCODE = null;
	
	private String RISKCODE = null;
	
	private String RISKNAME = null;

	private String POLICYCONT = null;

	private String CANCELCONT = null;

	private String POLICYPREM = null;

	private String CANCELPREM = null;

	private String SUMPOLICYPREM = null;
	
	private String SUMCANCELPREM = null;
	
	private String SUMPOLICYCOUNT = null;
	
	private String SUMCANCELCOUNT = null;
	
	private final static Logger mLogger = Logger.getLogger(PremSettlement.class);

	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if (!load()) {
			mLogger.error("ErrInfo:" + "接收查询信息失败");
			responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
					null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if (!check()) {
			mLogger.error("ErrInfo:" + "校验信息失败");
			responseOME = buildCardActiveResponseOME("01", "02", "校验信息失败", null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if (!deal()) {
			mLogger.error("ErrInfo:" + "处理信息失败");
			responseOME = buildCardActiveResponseOME("01", "03", "处理信息失败", null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		return responseOME;
	}

	/**
	 * 接收报文
	 * 
	 * @return
	 */
	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("CARDPREMSETTLEMENT")) {//远程出单卡单 保费查询
					Iterator leaf_element = Om.getChildren();
					while (leaf_element.hasNext()) {
						OMElement leaf = (OMElement) leaf_element.next();
						if (leaf.getLocalName().equals("BATCHNO")) {//批次号
							this.batchno = leaf.getText();
							mLogger.info("批次号:" + this.batchno );
							System.out.println("批次号"+this.batchno);
							continue;
						}
						if (leaf.getLocalName().equals("SENDDATE")) {//发送日期
							this.sendDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("SENDTIME")) {//发送时间
							this.sendTime = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("MESSAGETYPE")) {//报文发送类型
							this.messageType = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("STARTDATE")) {//交易开始时间
							this.startDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("ENDDATE")) {//交易结束时间
							this.endDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("CARDHEADING")) {//卡号题头--单证类型码
							this.cardHeading = leaf.getText();
							continue;
						}
						
						if (leaf.getLocalName().equals("CVALISTARTDATE")) {//生效开始时间
							this.cvalistartDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("CVALIENDDATE")) {//生效结束时间
							this.cvaliendDate = leaf.getText();
							continue;
						}	
						
						if (leaf.getLocalName().equals("BSTARTDATE")) {//结算开始日期
							this.bstartDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("BENDDATE")) {//结算结束日期
							this.bendDate = leaf.getText();
							continue;
						}
						
						
						if (leaf.getLocalName().equals("SALESPLATFORM")) {//第三方平台
							this.salesplatform = leaf.getText();
							mLogger.info("第三方平台:" + this.salesplatform );
							continue;
						}
						
					}
				}
			}
		} catch (Exception e) {
			mLogger.error("ErrInfo:" + "接收报文出现异常");
			mLogger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 校验报文信息
	 * 
	 * @return
	 */
	private boolean check() {

//		日期两边都必填
//		校验投保日期
		if(startDate!=null&&!("").equals(startDate)){
			if(endDate == null || endDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				mLogger.error("ErrInfo:" + "投保结束日期未填写");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(endDate!=null && !("").equals(endDate)){
			if(startDate == null || startDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				mLogger.error("ErrInfo:" + "投保开始日期未填写");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		
//	    校验生效日期
		if(cvalistartDate!=null&&!("").equals(cvalistartDate)){
			if(cvaliendDate == null || cvaliendDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				mLogger.error("ErrInfo:" + "生效结束日期未填写");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(cvaliendDate!=null && !("").equals(cvaliendDate)){
			if(cvalistartDate == null || cvalistartDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				mLogger.error("ErrInfo:" + "生效日期未填写");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}		
//	校验结算日期
		if(bstartDate!=null&&!("").equals(bstartDate)){
			if(bendDate == null || bendDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(bendDate!=null && !("").equals(bendDate)){
			if(bstartDate == null || bstartDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		
		if((bstartDate == null||bstartDate.equals(""))&&(bendDate == null||bendDate.equals(""))&&
				(startDate == null||startDate.equals(""))&&(endDate == null||endDate.equals(""))&&
				(cvalistartDate == null||cvalistartDate.equals(""))&&(cvaliendDate == null||cvaliendDate.equals(""))){
			responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
			null);
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		
		
		return true;
	}

	/**
	 * 报文业务处理
	 * 
	 * @return
	 */
	private boolean deal() {
		try {

			
			
			String ctripno="select distinct cont.cardno from licardactiveinfolist li inner join wfcontlist cont on li.cardno=cont.cardno where  cont.branchcode='ctrip'  ";
			StringBuffer cbuf = new StringBuffer(ctripno);
			if(cardHeading!=null&&!("").equals(cardHeading)){
				String[] cardHeadings = cardHeading.split("@");
				int length = cardHeadings.length;
				cbuf.append("and (");
				for(int i=0;i<length;i++){
					if(i == length -1){
						cbuf.append("cont.cardno like '").append(cardHeadings[i]).append("%') ");
						break;
					}
					cbuf.append("cont.cardno like '").append(cardHeadings[i]).append("%' or ");
				}
				
			}
			if (this.startDate != null && !this.startDate.equals("")) {
				cbuf.append("and cont.senddate >='").append(startDate).append("' ").append(" and cont.messagetype='01' ");
			}
			
			if (this.endDate != null && !this.endDate.equals("")) {
				cbuf.append("and cont.senddate <='").append(endDate).append("' ").append(" and cont.messagetype='01' ");;
			}
			if (this.cvalistartDate != null && !this.cvalistartDate.equals("")) {
				cbuf.append("and li.cvalidate >= '").append(this.cvalistartDate)
						.append("' ");
			}
			if (this.cvaliendDate != null && !this.cvaliendDate.equals("")) {
				cbuf.append("and li.cvalidate <= '").append(this.cvaliendDate)
						.append("' ");
			}
			
			if (this.bstartDate != null && !this.bstartDate.equals("")) {
				cbuf.append("and li.cvalidate + 30 day >= '").append(this.bstartDate)
						.append("' ");
			}
			if (this.bendDate != null && !this.bendDate.equals("")) {
				cbuf.append("and li.cvalidate + 30 day <= '").append(this.bendDate)
						.append("' ");
			}
			
			
			System.out.println(cbuf+"~~~~~");
			
			String ctripSQL = " select COALESCE ( a.riskwrapcode,b.riskwrapcode) RISKCODE,COALESCE ( a.wrapname,b.wrapname) RISKNAME,COALESCE ( a.CERTIFYCODE,b.CERTIFYCODE) CERTIFYCODE,COALESCE ( a.ccount,0) POLICYCONT ,COALESCE ( a.cprem,0) POLICYPREM ,COALESCE ( b.bcount,0) CANCELCONT ,COALESCE ( b.bprem,0) CANCELPREM    "
			+ " from  ( select ld.riskwrapcode ,ld.wrapname,  count(li.prem) ccount,sum(double(li.prem)) cprem,cont.branchcode , cont.certifycode  from licardactiveinfolist li  inner join WFCONTLIST cont  on li.cardno = cont.cardno  inner join lmcardrisk lr on cont.certifycode=lr.certifycode "
            +" inner join ldwrap ld on ld.riskwrapcode=lr.riskcode where 1=1 "
			+ " and cont.branchcode='ctrip'  and li.cardstatus ='01' "
			+" and cont.cardno in ( "+cbuf+" )"
			+"  group by cont.branchcode,cont.certifycode,ld.wrapname,ld.riskwrapcode  ) a  "
			+" full join (  select   ld.riskwrapcode,ld.wrapname , "
			+"  count(li.prem) bcount, "
			+"  sum(double(li.prem)) bprem,cont.branchcode, cont.certifycode from licardactiveinfolist li inner join WFCONTLIST cont on li.cardno = cont.cardno "
			+" inner join lmcardrisk lr on cont.certifycode=lr.certifycode "
			+" inner join ldwrap ld on ld.riskwrapcode=lr.riskcode where 1=1 "
			+"  and li.cardstatus ='03' and cont.messagetype='02'   and cont.branchcode='ctrip'  and operator='ctrip' "
			+"  and cont.cardno in ( "+cbuf +" ) "
			+"   group by cont.branchcode,cont.certifycode,ld.wrapname,ld.riskwrapcode ) b "
			+"  on  a.riskwrapcode=b.riskwrapcode  ";
			
			
			
			
			
			
			String policySQL = "( select ld.riskwrapcode ,ld.wrapname,"
								+" count(1) ccount,sum(double(li.prem)) cprem,cont.branchcode , cont.certifycode "
								+" from licardactiveinfolist li "
								+" inner join WFCONTLIST cont  on li.cardno = cont.cardno "
								+" inner join lmcardrisk lr on cont.certifycode=lr.certifycode "
								+"inner join ldwrap ld on ld.riskwrapcode=lr.riskcode "	
								+"where 1=1 "
								+ "and cont.branchcode='"+this.salesplatform+"' " +"and li.cardstatus ='01'";
			
			StringBuffer buf1 = new StringBuffer(policySQL);
			if(cardHeading!=null&&!("").equals(cardHeading)){
				String[] cardHeadings = cardHeading.split("@");
				int length = cardHeadings.length;
				buf1.append("and (");
				for(int i=0;i<length;i++){
					if(i == length -1){
						buf1.append("cont.cardno like '").append(cardHeadings[i]).append("%') ");
						break;
					}
					buf1.append("cont.cardno like '").append(cardHeadings[i]).append("%' or ");
				}
				
			}
			if (this.startDate != null && !this.startDate.equals("")) {
				buf1.append("and cont.senddate >='").append(startDate).append("' ");
			}
			
			if (this.endDate != null && !this.endDate.equals("")) {
				buf1.append("and cont.senddate <='").append(endDate).append("' ");
			}
			if (this.cvalistartDate != null && !this.cvalistartDate.equals("")) {
				buf1.append("and li.cvalidate >= '").append(this.cvalistartDate)
						.append("' ");
			}
			if (this.cvaliendDate != null && !this.cvaliendDate.equals("")) {
				buf1.append("and li.cvalidate <= '").append(this.cvaliendDate)
						.append("' ");
			}
			
			if (this.bstartDate != null && !this.bstartDate.equals("")) {
				buf1.append("and li.cvalidate + 30 day >= '").append(this.bstartDate)
						.append("' ");
			}
			if (this.bendDate != null && !this.bendDate.equals("")) {
				buf1.append("and li.cvalidate + 30 day <= '").append(this.bendDate)
						.append("' ");
			}
			
			
			buf1.append(" group by cont.branchcode,cont.certifycode,ld.wrapname,ld.riskwrapcode  ) a ");
			
			SSRS tSSRS1 = null;
//			tSSRS1 = new ExeSQL().execSQL(buf1.toString());
		
			
			String cancelSQL = "( select ld.riskwrapcode,ld.wrapname , "
				+ "count(1) bcount,sum(double(li.prem)) bprem,cont.branchcode, cont.certifycode "
				+ "from licardactiveinfolist li "
				+ "inner join WFCONTLIST cont on li.cardno = cont.cardno "
				+"  inner join lmcardrisk lr on cont.certifycode=lr.certifycode "
				+" inner join ldwrap ld on ld.riskwrapcode=lr.riskcode "
				+ "where 1=1 "
				+ "and li.cardstatus ='03' and cont.branchcode='"+this.salesplatform+"' ";
			 if(this.salesplatform.equals("PI2109001791")){
				cancelSQL+=" and operator='201' ";
			}else if(this.salesplatform.equals("UnionPay")){
				cancelSQL+=" and operator='UnionPay' ";
			}else{
				cancelSQL+=" and operator='001' ";
			}
			
			StringBuffer buf2 = new StringBuffer(cancelSQL);
			if(cardHeading!=null&&!("").equals(cardHeading)){
				String[] cardHeadings = cardHeading.split("@");
				int length = cardHeadings.length;
				buf2.append("and (");
				for(int i=0;i<length;i++){
					if(i == length -1){
						buf2.append("cont.cardno like '").append(cardHeadings[i]).append("%') ");
						break;
					}
					buf2.append("cont.cardno like '").append(cardHeadings[i]).append("%' or ");
				}
				
			}
			if (this.startDate != null && !this.startDate.equals("")) {
				buf2.append("and cont.senddate >='").append(startDate).append("' ");
			}
			if (this.endDate != null && !this.endDate.equals("")) {
				buf2.append("and cont.senddate <='").append(endDate).append("' ");
			}
			
			if (this.cvalistartDate != null && !this.cvalistartDate.equals("")) {
				buf2.append("and li.cvalidate >= '").append(this.cvalistartDate)
						.append("' ");
			}
			if (this.cvaliendDate != null && !this.cvaliendDate.equals("")) {
				buf2.append("and li.cvalidate <= '").append(this.cvaliendDate)
						.append("' ");
			}
			if (this.bstartDate != null && !this.bstartDate.equals("")) {
				buf2.append("and li.cvalidate + 30 day >= '").append(this.bstartDate)
						.append("' ");
			}
			if (this.bendDate != null && !this.bendDate.equals("")) {
				buf2.append("and li.cvalidate + 30 day <= '").append(this.bendDate)
						.append("' ");
			}
			
			
			buf2.append(" group by cont.branchcode,cont.certifycode,ld.wrapname,ld.riskwrapcode ) b");
			
			StringBuffer s1 = new StringBuffer ("select COALESCE ( a.riskwrapcode,b.riskwrapcode) RISKCODE,COALESCE ( a.wrapname,b.wrapname) RISKNAME,COALESCE ( a.CERTIFYCODE,b.CERTIFYCODE) CERTIFYCODE,COALESCE ( a.ccount,0) POLICYCONT ,COALESCE ( a.cprem,0) POLICYPREM ,COALESCE ( b.bcount,0) CANCELCONT ,COALESCE ( b.bprem,0) CANCELPREM from  ");
			StringBuffer str= s1.append(buf1).append(" full join ").append(buf2).append(" on a.riskwrapcode=b.riskwrapcode  ");
			//携程出单 统计查询
			if(this.salesplatform.equals("ctrip")){
				str=new StringBuffer(ctripSQL);
			}
			
			
			SSRS tSS = null;
			tSS = new ExeSQL().execSQL(str.toString());
			SSRS tSSRS2 = null;
//			tSSRS2 = new ExeSQL().execSQL(buf2.toString());
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMElement CONTLIST = fac.createOMElement("CONTLIST", null);
			int policyCount = 0,cancelCount = 0;
			double policyPrem = 0.0,cancelPrem = 0.0;
			
			
			
			//如果出单结果为空  并且退保结果也为空 返回 没有查到结果
			if ((tSS == null || tSS.MaxRow < 1) ) {
				responseOME = buildCardActiveResponseOME("01", "01",
						"没有得到查询结果", null);
				 mLogger.info("没有查询到出单结果");
				System.out.println("出单结果为空  并且退保结果也为空 返回 没有查到结果");
				return false;
			}else {
				List list = new ArrayList();
				for (int i = 1; i <= tSS.MaxRow; i++) {
					this.RISKCODE = tSS.GetText(i, 1);
					this.RISKNAME = tSS.GetText(i, 2);
					this.CERTIFYCODE = tSS.GetText(i, 3);
					this.POLICYCONT = tSS.GetText(i, 4);
					this.POLICYPREM = tSS.GetText(i,5);
					this.CANCELCONT = tSS.GetText(i, 6);
					this.CANCELPREM = tSS.GetText(i, 7);
			
					policyPrem = policyPrem + Double.parseDouble(POLICYPREM);
					cancelPrem = cancelPrem + Double.parseDouble(CANCELPREM);
					policyCount = policyCount + Integer.parseInt(this.POLICYCONT);
					cancelCount = cancelCount + Integer.parseInt(this.CANCELCONT);
					OMElement subItem = buildCardActiveResponseItem();
					CONTLIST.addChild(subItem);
				}
				
				
			}
			
			this.SUMPOLICYPREM = String.valueOf(policyPrem);
			this.SUMCANCELPREM = String.valueOf(cancelPrem);
			this.SUMPOLICYCOUNT = String.valueOf(policyCount);
			this.SUMCANCELCOUNT = String.valueOf(cancelCount);
			this.responseOME = buildCardActiveResponseOME("00", "", "",
					CONTLIST);
		} catch (Exception e) {
			 mLogger.error("业务处理异常");
			System.out.println("业务处理异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * 创建返回报文
	 * 
	 * @param state
	 * @param errCode
	 * @param errInfo
	 * @return OMElement
	 */
	private OMElement buildCardActiveResponseOME(String state, String errCode,
			String errInfo, OMElement contList) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("CARDPREMSETTLEMENT", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", this.messageType);

		if (state.equals("01")) {
			tool.addOm(DATASET, "STATE", state);
			tool.addOm(DATASET, "ERRCODE", errCode);
			tool.addOm(DATASET, "ERRINFO", errInfo);
			tool.addOm(DATASET, "OUTPUTDATA", null);
			tool.addOm(DATASET, "STARTDATE", "");
			tool.addOm(DATASET, "ENDDATE", "");
			tool.addOm(DATASET, "ACCOUNT", null);
			tool.addOm(DATASET, "SUMPOLICYPREM", "");
			tool.addOm(DATASET, "SUMCANCELPREM", "");
			tool.addOm(DATASET, "SUMPOLICYCOUNT", "");
			tool.addOm(DATASET, "SUMCANCELCOUNT", "");
		}
//查找数据返回
		if (state.equals("00")) {
			tool.addOm(DATASET, "STATE", state);
			tool.addOm(DATASET, "ERRCODE", null);
			tool.addOm(DATASET, "ERRINFO", null);
			tool.addOm(DATASET, "STARTDATE", this.startDate);
			tool.addOm(DATASET, "ENDDATE", this.endDate);
			tool.addOm(DATASET, "CVALISTARTDATE", this.cvalistartDate);
			tool.addOm(DATASET, "CVALIENDDATE", this.cvaliendDate);
			tool.addOm(DATASET, "ACCOUNT", this.RACCOUNT);
			tool.addOm(DATASET, "SUMPOLICYPREM", this.SUMPOLICYPREM);
			tool.addOm(DATASET, "SUMCANCELPREM", this.SUMCANCELPREM);
			tool.addOm(DATASET, "SUMPOLICYCOUNT", this.SUMPOLICYCOUNT);
			tool.addOm(DATASET, "SUMCANCELCOUNT", this.SUMCANCELCOUNT);
			OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
			OUTPUTDATA.addChild(contList);
			DATASET.addChild(OUTPUTDATA);
		}
		return DATASET;
	}

	/**
	 * 创建返回结点
	 * 
	 * @param state
	 * @param errCode
	 * @param errInfo
	 * @return OMElement
	 */
	private OMElement buildCardActiveResponseItem() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tool = new LoginVerifyTool();

		OMElement ITEM = fac.createOMElement("ITEM", null);
		tool.addOm(ITEM, "ACCOUNT", this.RACCOUNT);
		tool.addOm(ITEM, "CERTIFYCODE", this.CERTIFYCODE);
		tool.addOm(ITEM, "RISKCODE", this.RISKCODE);
		tool.addOm(ITEM, "RISKNAME", this.RISKNAME);
		tool.addOm(ITEM, "POLICYCONT", this.POLICYCONT);
		tool.addOm(ITEM, "CANCELCONT", this.CANCELCONT);
		tool.addOm(ITEM, "POLICYPREM", this.POLICYPREM);
		tool.addOm(ITEM, "CANCELPREM", this.CANCELPREM);
		return ITEM;
	}
}
