package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.utils.CoreDAO;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.db.WFAppntListDB;
import com.sinosoft.lis.db.WFBnfListDB;
import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.db.WFInsuListDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFContListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:卡单查询
 * 
 * @author yuanzw 创建时间：2010-2-3 上午11:12:58 Description:
 * @version
 */

public class ZCardQuery {

	
	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 交易类型

	private String cardNo = null;// 卡单号码

	private String password = null;// 卡单密码
	
	public SSRS tSSRS = null; //获取投保人关系数据

	// 保险卡信息
	String cardDealType;

	String certifyCode;

	public OMElement queryData(OMElement Oms) {
		String errorInfo = null;
		CoreDAO tempCoreDAO = new CoreDAO();
		
		OMElement responseOME = null;
		WFContListSchema contListSchema = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("ZCARDQUERY")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);
							if (!"04".equals(messageType)) {
								responseOME = buildResponseOME(null, null,
										null, null, "", "01", "5", "报文类型不正确!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "5", "报文类型不正确!");
								return responseOME;
							}
						}

						if (child_element.getLocalName().equals("CARDNO")) {
							cardNo = child_element.getText();

							System.out.println("卡号："+cardNo);
						}
						if (child_element.getLocalName().equals("PASSWORD")) {
							password = child_element.getText();

							System.out.println("密码："+password);
						}
					}
					// 日志3
					LoginVerifyTool.writeLoginLog(cardNo, password);

				}
			}

			if (cardNo == null || "".equals(cardNo)) {
				responseOME = buildResponseOME(null, null, null, null, "",
						"01", "1", "卡单号码为空值");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "100", "1", "卡单号码为空值");
				return responseOME;
			}
			
			//校验该卡号是否被激活过
			boolean isCardNoActived = tempCoreDAO.isCardNoActived(cardNo);
			if(!isCardNoActived){
				// 提示该卡未激活过
				responseOME = buildResponseOME(null, null, null, null, "",
						"01", "1", "该卡未被激活，请进入激活页面");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "100", "1", "该卡未被激活");
				return responseOME;
				
			}
			/*
			 * update by zhangyige 2012-03-15
			 */
			//校验该卡号是否被激活过
			boolean isCardNoCanseled = tempCoreDAO.isCardNoCanseled(cardNo);
			if(isCardNoCanseled){
				// 提示该卡已为撤单状态
				responseOME = buildResponseOME(null, null, null, null, "",
						"01", "1", "该卡已为撤单状态");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "100", "1", "该卡已为撤单状态");
				return responseOME;
				
			}
			
			if (password == null || "".equals(password)) {
				responseOME = buildResponseOME(null, null, null, null, "",
						"01", "2", "卡单密码为空值");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "100", "2", "卡单密码为空值");
				return responseOME;
			}
			LisIDEA tLisIdea = new LisIDEA();
			String strpassword = tLisIdea.encryptString(password);
			// by gzh 20101115
			String SysSql = "select * from LZCardNumber where cardno = '"+cardNo+"' and cardpassword = '"+strpassword+"'";
			SSRS sResult = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			sResult = tExeSQL.execSQL(SysSql);
			//101117
			String insuredIDNoSql = "select IDNo from LICardActiveInfoList where CardNo ='"	+ cardNo + "'";
			SSRS tRemoteResult = tExeSQL.execSQL(insuredIDNoSql);
			String coreInsuredIDNo = tRemoteResult.GetText(1, 1);
			if(sResult.MaxRow>0 || password.equals(coreInsuredIDNo)){
				SSRS sResult1 = new SSRS();//根据cardno获取LICardActiveInfoList数据
				SSRS sResult2 = new SSRS();//根据cardno获取LICertify数据
				SSRS sResult3 = new SSRS();//获取senddate,sendtime,batchno
				String licaSQL = " select * from LICardActiveInfoList where cardno = '"+cardNo+"'";
//		           + " and CardStatus = '01'";
				System.out.println("查询卡号是否有效："+licaSQL);			
				String LiceSQL = " select * from LICertify where cardno = '"+cardNo+"' and ActiveFlag = '01'";
				System.out.println("查询卡号是否激活："+LiceSQL);
				sResult1 = tExeSQL.execSQL(licaSQL);
				sResult2 = tExeSQL.execSQL(LiceSQL);
				// 卡单号 密码 校验是否通过Flag
				boolean queryFlag = false;
				if ( sResult1.MaxRow > 0 || sResult2.MaxRow>0) {
					queryFlag = true;
				}else{
					errorInfo = "该卡未激活";
				}
				
				String sql = "select senddate,sendtime,batchno "
					+ "from wfcontlist where cardNo='" + cardNo
					+ "' and Password='" + password
					+ "' order by date(senddate) desc,time(sendtime) desc "
					+ "fetch first 1 rows only";
				System.out.println(sql+"======199");
				sResult3 = tExeSQL.execSQL(sql);
				if(sResult3.MaxRow<=0){
					String sql3 = "select senddate,sendtime,batchno "
						+ "from wfcontlist where cardNo='"
						+ cardNo
						+ "' order by date(senddate) desc,time(sendtime) desc "
						+ "fetch first 1 rows only";
					System.out.println(sql3+"==========207");
					sResult3 = tExeSQL.execSQL(sql3);
				}
				
				if (!queryFlag) {
					// 远程出单 使用cardNo 和 被保人的证件号进行校验
					// 进行被保人证件号校验
//					String insuredIDNoSql = "select IDNo from LICardActiveInfoList where CardNo ='"
//							+ cardNo + "'";
//					SSRS tRemoteResult = tExeSQL.execSQL(insuredIDNoSql);
					if (null != tRemoteResult && tRemoteResult.MaxRow > 0) {
//						String coreInsuredIDNo = tRemoteResult.GetText(1, 1);
						// 如果查询出的被保人证件号与发送报文中password的一致 则认为远程出单 密码校验通过
						if (password.equals(coreInsuredIDNo)) {
							queryFlag = true;
							String sql3 = "select senddate,sendtime,batchno "
									+ "from wfcontlist where cardNo='"
									+ cardNo
									+ "' order by date(senddate) desc,time(sendtime) desc "
									+ "fetch first 1 rows only";
							sResult3 = tExeSQL.execSQL(sql3);
						}else{
							errorInfo = "该卡未激活";
						}
					}
				}
//				 两种校验方式 有一种可通过 可以进行查询数据
				if (queryFlag) {
					if (sResult1.MaxRow > 0) {
//						 使用licardactiveinfolist 及投保人 受益人 信息构建返回报文
						System.out.println("使用licardactiveinfolist 及投保人 受益人 信息构建返回报文");
						// 保险卡信息查询	
						WFContListDB contListDB = new WFContListDB();
						WFAppntListDB appntListDB = new WFAppntListDB();
						WFBnfListDB bnfListDB = new WFBnfListDB();
						WFAppntListSchema appntListSchema = null;
						WFBnfListSet bnfListSet = null;
						String latestSendDate = null;
						String latestSendTime = null;
						String batchNo = null;
						if (sResult3.MaxRow > 0) {
							latestSendDate = sResult3.GetText(1, 1);
							latestSendTime = sResult3.GetText(1, 2);
							batchNo = sResult3.GetText(1, 3);
						}
//						获取WFContList投保人信息
						contListDB.setCardNo(cardNo);
						String contListSQL ="select * from WFContList where cardno='"+cardNo
						+"' and senddate='"+latestSendDate+"' and sendtime='"+latestSendTime+"'";
						System.out.println("获取WFContList投保人信息："+contListSQL);
						WFContListSet contlistSet =contListDB.executeQuery(contListSQL);
						if(contlistSet.size()>0){
//							投保人信息查询
							String appntlistSQL="select * from WFAppntList where batchNo='"+batchNo+"'";
							System.out.println("wf:获取投保人信息："+appntlistSQL);
							if (appntListDB.executeQuery(appntlistSQL).size() != 0) {
								appntListSchema = appntListDB.executeQuery(appntlistSQL).get(1);
							}
							
//							 被保人信息
							WFInsuListDB insuListDB = new WFInsuListDB();
							
							String insulistSQL="select * from wfinsulist where batchno='"+batchNo+"'";
							System.out.println("wf:获取被保人信息："+insulistSQL);
							WFInsuListSet insuListSet = null;
							if (insuListDB.executeQuery(insulistSQL).size() != 0) {
								insuListSet = insuListDB.executeQuery(insulistSQL);
							}
							// 受益人信息
							String bnflistSQL="select * from WFBnfList where batchNo='"+batchNo+"'";
							System.out.println("wf:获取受益人信息："+bnflistSQL);
							bnfListSet = bnfListDB.executeQuery(bnflistSQL);
							//卡信息获取第一条记录
							contListSchema = contlistSet.get(1);
							cardDealType = contListSchema.getCardDealType();
							certifyCode = contListSchema.getCertifyCode();

							responseOME = buildResponseOME(contListSchema, appntListSchema,
									insuListSet, bnfListSet, "", "00", "0", "成功");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno, messageType,
									sendDate, sendTime, "00", "100", "0", "成功");
						}else{
							//
							System.out.println("使用licardactiveinfolist(无wf) 及投保人 受益人 信息构建返回报文");
							LICardActiveInfoListDB tLICardActiveInfoListDB = new LICardActiveInfoListDB();
							LICardActiveInfoListSet tLICardActiveInfoListSet = tLICardActiveInfoListDB
									.executeQuery(licaSQL);
							if(tLICardActiveInfoListSet.size()>0){
								responseOME = buildResponseOMEByCore(
										tLICardActiveInfoListSet, null,
										null, "00", "0", "成功");
								return responseOME;
						    }
						}	
					}else if (sResult2.MaxRow > 0) {
//						 使用使用LICertify 及投保人 受益人 信息构建返回报文
						System.out.println("使用LICertify 信息构建返回报文");
						LICertifyDB tLICertifyDB = new LICertifyDB();
						LICertifySet tLICertifySet =tLICertifyDB.executeQuery(LiceSQL);

						//获取被保人信息
						String LiceInsuSQL="select * from licertifyinsured where cardno='"+cardNo+"' ";
						LICertifyInsuredDB tLICertifyInsuredDB = new LICertifyInsuredDB();
						LICertifyInsuredSet tLICertifyInsuredSet = tLICertifyInsuredDB.executeQuery(LiceInsuSQL);
						if(tLICertifySet.size()>0){
							responseOME = buildResponseOMEByLiCertfy(
									tLICertifySet, tLICertifyInsuredSet,null,null,"00", "0", "成功");
							return responseOME;
					    } 
					}					
				}else{
					responseOME = buildResponseOME(null, null, null, null, "",
							"01", "3", "该卡未激活！");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno, messageType,
							sendDate, sendTime, "01", "100", "5", "该卡未激活！");
					return responseOME;
				}
			}else{
				responseOME = buildResponseOME(null, null, null, null, "",
						"01", "3", "卡号或密码不正确");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "100", "3", "卡号或密码不正确");
				return responseOME;
			}

		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME(null, null, null, null, "", "01",
					"4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "100", "4", "发生异常");
			return responseOME;
		}

		return responseOME;

	}

	private OMElement buildResponseOME(WFContListSchema contListSchema,
			WFAppntListSchema appntListSchema, WFInsuListSet insuListSet,
			WFBnfListSet bnfListSet, String branchCode, String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("ZCARDQUERY", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", messageType);
		tool.addOm(DATASET, "STATE", state);

		if (null != contListSchema) {
			tool.addOm(DATASET, "CERTIFYCODE", contListSchema.getRiskCode());
		} else {
			tool.addOm(DATASET, "CERTIFYCODE", "");
		}

		tool.addOm(DATASET, "CARDDEALTYPE", cardDealType);

		// 保险卡信息
		OMElement CONTLIST = fac.createOMElement("CONTLIST", null);
		if (null != contListSchema) {
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "CARDNO", contListSchema.getCardNo());
			tool.addOm(ITEM, "PASSWORD", password);
			tool.addOm(ITEM, "CERTIFYCODE", certifyCode);

			tool.addOm(ITEM, "ACTIVEDATE", contListSchema.getActiveDate());

//			ExeSQL tExeSQL = new ExeSQL();

			String [] proInfoArr = tool.getProductInfo(cardNo);
			tool.addOm(ITEM, "CVALIDATE", proInfoArr[1]);
			tool.addOm(ITEM, "ENDDATE",proInfoArr[2]);
//			modify by zhangyige 2012-07-12
			//如果WFContList中存在生效时间，那么组件在返回报文中
			String getTimeSql = "select cvalidatetime "
				+ "from wfcontlist where cardNo='"
				+ cardNo
				+ "'and branchcode = 'LKBSys' order by date(senddate) desc,time(sendtime) desc "
				+ "fetch first 1 rows only";
			ExeSQL tExeSQL = new ExeSQL();
			String valiTime = tExeSQL.getOneValue(getTimeSql);
			if(!valiTime.equals("")&&valiTime!=null){
				tool.addOm(ITEM, "CVALIDATETIME", valiTime);
				tool.addOm(ITEM, "ENDDATETIME", valiTime);
			}
			// 查询套餐名称
//			String certifyCode = riskRelateCode[1];
//			String certifyCodeSql = "select certifyname from lmcertifydes where certifycode='"
//					+ certifyCode + "'";
//
//			String productName = tExeSQL.getOneValue(certifyCodeSql);
			tool.addOm(ITEM, "CERTIFYNAME", proInfoArr[3]);// 套餐名称
			
			// 新增 管理机构
			String manageArr = tool.getManageComName(cardNo);
			tool.addOm(ITEM, "MANAGECOM", manageArr);
			System.out.println("保费："+String.valueOf(contListSchema.getPrem())+"=====384");
			tool.addOm(ITEM, "PREM", String.valueOf(contListSchema.getPrem()));
			tool.addOm(ITEM, "INSUYEAR", proInfoArr[0]);
			//编号#399开始 查询航班号 by jialei 2015年7月16日09:58:05
			String teamnoSql = "select distinct teamno from LICARDACTIVEINFOLIST where cardno='"+contListSchema.getCardNo()+"'" ;
			String teamno = tExeSQL.getOneValue(teamnoSql);
			System.out.println("航班号："+teamno);
			tool.addOm(ITEM, "TEAMNO", teamno);
			
			String branchcodeSql = "select distinct branchcode from Wfcontlist where cardno='"+contListSchema.getCardNo()+"'" ;
			String branchcode = tExeSQL.getOneValue(branchcodeSql);
			System.out.println("发送机构："+branchcode);
			tool.addOm(ITEM, "BRANCHCODE", branchcode);
			//编号#399结束
			CONTLIST.addChild(ITEM);
		} else {
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "CARDNO", "");
			tool.addOm(ITEM, "PASSWORD", "");
			tool.addOm(ITEM, "CERTIFYCODE", "");
			tool.addOm(ITEM, "CERTIFYNAME", "");
			tool.addOm(ITEM, "ACTIVEDATE", "");
			tool.addOm(ITEM, "CVALIDATE", "");
			tool.addOm(ITEM, "SALEAGENT", "");
			tool.addOm(ITEM, "PREM", "");
			tool.addOm(ITEM, "INSUYEAR", "");
			tool.addOm(ITEM, "TEAMNO", "");
			tool.addOm(ITEM, "BRANCHCODE", "");
			CONTLIST.addChild(ITEM);
		}

		DATASET.addChild(CONTLIST);

		// 投保人信息
		OMElement APPNTLIST = fac.createOMElement("APPNTLIST", null);
		if (null != appntListSchema) {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", appntListSchema.getName());

			tool.addOm(APPNTITEM, "SEX", tool
					.getChSex(appntListSchema.getSex()));
			tool.addOm(APPNTITEM, "BIRTHDAY", appntListSchema.getBirthday());
			tool.addOm(APPNTITEM, "IDTYPE", tool.queryIDType(appntListSchema
					.getIDType()));
			tool.addOm(APPNTITEM, "IDNO", appntListSchema.getIDNo());
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", appntListSchema
					.getOccupationType());
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", appntListSchema
					.getOccupationCode());
			tool.addOm(APPNTITEM, "POSTALADDRESS", appntListSchema
					.getPostalAddress());
			tool.addOm(APPNTITEM, "ZIPCODE", appntListSchema.getZipCode());
			tool.addOm(APPNTITEM, "PHONT", appntListSchema.getPhont());
			tool.addOm(APPNTITEM, "MOBILE", appntListSchema.getMobile());
			tool.addOm(APPNTITEM, "EMAIL", appntListSchema.getEmail());
			tool.addOm(APPNTITEM, "GRPNAME", appntListSchema.getGrpName());
			tool.addOm(APPNTITEM, "COMPANYPHONE", appntListSchema
					.getCompanyPhone());
			tool.addOm(APPNTITEM, "COMPADDR", appntListSchema.getCompAddr());
			tool.addOm(APPNTITEM, "COMPZIPCODE", appntListSchema
					.getCompZipCode());

			APPNTLIST.addChild(APPNTITEM);
		} else {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", "");
			tool.addOm(APPNTITEM, "SEX", "");
			tool.addOm(APPNTITEM, "BIRTHDAY", "");
			tool.addOm(APPNTITEM, "IDTYPE", "");
			tool.addOm(APPNTITEM, "IDNO", "");
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", "");
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", "");
			tool.addOm(APPNTITEM, "POSTALADDRESS", "");
			tool.addOm(APPNTITEM, "ZIPCODE", "");
			tool.addOm(APPNTITEM, "PHONT", "");
			tool.addOm(APPNTITEM, "MOBILE", "");
			tool.addOm(APPNTITEM, "EMAIL", "");
			tool.addOm(APPNTITEM, "GRPNAME", "");
			tool.addOm(APPNTITEM, "COMPANYPHONE", "");
			tool.addOm(APPNTITEM, "COMPADDR", "");
			tool.addOm(APPNTITEM, "COMPZIPCODE", "");

			APPNTLIST.addChild(APPNTITEM);
		}

		DATASET.addChild(APPNTLIST);

		// 被保人信息
		OMElement INSULIST = fac.createOMElement("INSULIST", null);
		if (null != insuListSet) {
			WFInsuListSchema insuListSchema;
			while (null != insuListSet.get(1) || "".equals(insuListSet.get(1))) {
				insuListSchema = insuListSet.get(1);

				OMElement INSUITEM = fac.createOMElement("ITEM", null);
				tool.addOm(INSUITEM, "INSUNO", String.valueOf(insuListSchema
						.getInsuNo()));
				tool.addOm(INSUITEM, "TOAPPNTRELA", insuListSchema
						.getToAppntRela());
				tool.addOm(INSUITEM, "RELATIONCODE", insuListSchema
						.getRelationCode());
				tool.addOm(INSUITEM, "RELATIONTOINSURED", insuListSchema
						.getRelationToInsured());
				tool.addOm(INSUITEM, "REINSUNO", insuListSchema.getReInsuNo());

				tool.addOm(INSUITEM, "NAME", insuListSchema.getName());

				tool.addOm(INSUITEM, "SEX", tool.getChSex(insuListSchema
						.getSex()));
				tool.addOm(INSUITEM, "BIRTHDAY", insuListSchema.getBirthday());
				tool.addOm(INSUITEM, "IDTYPE", tool.queryIDType(insuListSchema
						.getIDType()));
				tool.addOm(INSUITEM, "IDNO", insuListSchema.getIDNo());
				tool.addOm(INSUITEM, "OCCUPATIONTYPE", insuListSchema
						.getOccupationType());
				tool.addOm(INSUITEM, "OCCUPATIONCODE", insuListSchema
						.getOccupationCode());
				tool.addOm(INSUITEM, "POSTALADDRESS", insuListSchema
						.getPostalAddress());
				tool.addOm(INSUITEM, "ZIPCODE", insuListSchema.getZipCode());
				tool.addOm(INSUITEM, "PHONT", insuListSchema.getPhont());
				tool.addOm(INSUITEM, "MOBILE", insuListSchema.getMobile());
				tool.addOm(INSUITEM, "EMAIL", insuListSchema.getEmail());
				tool.addOm(INSUITEM, "GRPNAME", insuListSchema.getGrpName());
				tool.addOm(INSUITEM, "COMPANYPHONE", insuListSchema
						.getCompanyPhone());
				tool.addOm(INSUITEM, "COMPADDR", insuListSchema.getCompAddr());
				tool.addOm(INSUITEM, "COMPZIPCODE", insuListSchema
						.getCompZipCode());

				insuListSet.remove(insuListSchema);
				INSULIST.addChild(INSUITEM);
			}
		} else {
			OMElement INSUITEM = fac.createOMElement("ITEM", null);
			tool.addOm(INSUITEM, "INSUNO", "");
			tool.addOm(INSUITEM, "TOAPPNTRELA", "");
			tool.addOm(INSUITEM, "RELATIONCODE", "");
			tool.addOm(INSUITEM, "RELATIONTOINSURED", "");
			tool.addOm(INSUITEM, "REINSUNO", "");
			tool.addOm(INSUITEM, "NAME", "");
			tool.addOm(INSUITEM, "SEX", "");
			tool.addOm(INSUITEM, "BIRTHDAY", "");
			tool.addOm(INSUITEM, "IDTYPE", "");
			tool.addOm(INSUITEM, "IDNO", "");
			tool.addOm(INSUITEM, "OCCUPATIONTYPE", "");
			tool.addOm(INSUITEM, "OCCUPATIONCODE", "");
			tool.addOm(INSUITEM, "POSTALADDRESS", "");
			tool.addOm(INSUITEM, "ZIPCODE", "");
			tool.addOm(INSUITEM, "PHONT", "");
			tool.addOm(INSUITEM, "MOBILE", "");
			tool.addOm(INSUITEM, "EMAIL", "");
			tool.addOm(INSUITEM, "GRPNAME", "");
			tool.addOm(INSUITEM, "COMPANYPHONE", "");
			tool.addOm(INSUITEM, "COMPADDR", "");
			tool.addOm(INSUITEM, "COMPZIPCODE", "");

			INSULIST.addChild(INSUITEM);
		}

		DATASET.addChild(INSULIST);
		// 受益人信息

		OMElement BNFLIST = fac.createOMElement("BNFLIST", null);
		if (null != bnfListSet) {
			WFBnfListSchema bnfListSchema;
			while (null != bnfListSet.get(1) || "".equals(bnfListSet.get(1))) {
				bnfListSchema = bnfListSet.get(1);

				OMElement BNFITEM = fac.createOMElement("ITEM", null);
				tool.addOm(BNFITEM, "TOINSUNO", String.valueOf(bnfListSchema
						.getToInsuNo()));
				tool
						.addOm(BNFITEM, "TOINSURELA", bnfListSchema
								.getToInsuRela());
				tool.addOm(BNFITEM, "BNFTYPE", bnfListSchema.getBnfType());
				tool.addOm(BNFITEM, "BNFGRADE", bnfListSchema.getBnfGrade());
				tool.addOm(BNFITEM, "BNFRATE", String.valueOf(bnfListSchema
						.getBnfRate()));
				tool.addOm(BNFITEM, "NAME", bnfListSchema.getName());
				tool.addOm(BNFITEM, "SEX", tool
						.getChSex(bnfListSchema.getSex()));
				tool.addOm(BNFITEM, "BIRTHDAY", bnfListSchema.getBirthday());
				String bnfIdType = "";
				if(bnfListSchema.getIDType()==null){
					bnfIdType = "4";
				}else{
					bnfIdType = bnfListSchema.getIDType();
				}
				tool.addOm(BNFITEM, "IDTYPE", tool.queryIDType(bnfIdType));
				tool.addOm(BNFITEM, "IDNO", bnfListSchema.getIDNo());

				bnfListSet.remove(bnfListSchema);
				BNFLIST.addChild(BNFITEM);
			}
		} else {
			OMElement BNFITEM = fac.createOMElement("ITEM", null);
			tool.addOm(BNFITEM, "TOINSUNO", "");
			tool.addOm(BNFITEM, "TOINSURELA", "");
			tool.addOm(BNFITEM, "BNFTYPE", "");
			tool.addOm(BNFITEM, "BNFGRADE", "");
			tool.addOm(BNFITEM, "BNFRATE", "");
			tool.addOm(BNFITEM, "NAME", "");
			tool.addOm(BNFITEM, "SEX", "");
			tool.addOm(BNFITEM, "BIRTHDAY", "");
			tool.addOm(BNFITEM, "IDTYPE", "");
			tool.addOm(BNFITEM, "IDNO", "");

			BNFLIST.addChild(BNFITEM);
		}

		DATASET.addChild(BNFLIST);

		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	// 根据核心中间表 licardactiveinfolist 等表构建新报文
	private OMElement buildResponseOMEByCore(
			LICardActiveInfoListSet tLICardActiveInfoListSet,
			WFAppntListSchema appntListSchema, WFBnfListSet bnfListSet,
			String state, String errCode, String errInfo) {
		LICardActiveInfoListSchema contListSchema = tLICardActiveInfoListSet.get(1);

		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("ZCARDQUERY", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", messageType);
		tool.addOm(DATASET, "STATE", state);		

		tool.addOm(DATASET, "CARDDEALTYPE", cardDealType);

		// 保险卡信息
		OMElement CONTLIST = fac.createOMElement("CONTLIST", null);
		if (null != contListSchema) {
//			tool.addOm(DATASET, "CERTIFYCODE", contListSchema.getCardType());//套餐编码
			tool.addOm(DATASET, "CERTIFYCODE", tool.getRiskSetCodeByCardNo(cardNo)[0]);//套餐编码
			
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "CARDNO", cardNo);
			tool.addOm(ITEM, "PASSWORD", password);
			tool.addOm(ITEM, "CERTIFYCODE", certifyCode);

			tool.addOm(ITEM, "ACTIVEDATE", contListSchema.getActiveDate());

			// 获取保险期间及保险期间标志 并拼接 Modify at 2010-7-12
			String [] proInfoArr = tool.getProductInfo(cardNo);
		
			tool.addOm(ITEM, "CVALIDATE", proInfoArr[1]);
			tool.addOm(ITEM, "ENDDATE", proInfoArr[2]);
			//modify by zhangyige 2012-07-12
			//如果WFContList中存在生效时间，那么组件在返回报文中
			String getTimeSql = "select cvalidatetime "
				+ "from wfcontlist where cardNo='"
				+ cardNo
				+ "'and branchcode = 'LKBSys' order by date(senddate) desc,time(sendtime) desc "
				+ "fetch first 1 rows only";
			ExeSQL tExeSQL = new ExeSQL();
			String valiTime = tExeSQL.getOneValue(getTimeSql);
			if(!valiTime.equals("")&&valiTime!=null){
				tool.addOm(ITEM, "CVALIDATETIME", valiTime);
				tool.addOm(ITEM, "ENDDATETIME", valiTime);
			}
			
			// 查询套餐名称
			tool.addOm(ITEM, "CERTIFYNAME", proInfoArr[3] );// 套餐名称
			String oldPrem = String.valueOf(contListSchema.getPrem());
			System.out.println("保费："+oldPrem+"=====627");
			if("".equals(oldPrem)||oldPrem==null){
				oldPrem = "0";
			}
			tool.addOm(ITEM, "PREM",oldPrem );
			tool.addOm(ITEM, "INSUYEAR", proInfoArr[0]);
			//管理机构
			String manageArr = tool.getManageComName(cardNo);
			tool.addOm(ITEM, "MANAGECOM", manageArr);
			
			CONTLIST.addChild(ITEM);
		} else {
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "CARDNO", "");
			tool.addOm(ITEM, "PASSWORD", "");
			tool.addOm(ITEM, "CERTIFYCODE", "");
			tool.addOm(ITEM, "CERTIFYNAME", "");
			tool.addOm(ITEM, "ACTIVEDATE", "");
			tool.addOm(ITEM, "CVALIDATE", "");
			tool.addOm(ITEM, "SALEAGENT", "");
			tool.addOm(ITEM, "PREM", "");
			tool.addOm(ITEM, "INSUYEAR", "");

			CONTLIST.addChild(ITEM);
		}

		DATASET.addChild(CONTLIST);
		LICardActiveInfoListSchema insuListSchema;
		insuListSchema = tLICardActiveInfoListSet.get(1);
		// 投保人信息
		OMElement APPNTLIST = fac.createOMElement("APPNTLIST", null);
		if (null != appntListSchema ) {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", appntListSchema.getName());

			tool.addOm(APPNTITEM, "SEX", tool
					.getChSex(appntListSchema.getSex()));
			tool.addOm(APPNTITEM, "BIRTHDAY", appntListSchema.getBirthday());
			tool.addOm(APPNTITEM, "IDTYPE", tool.queryIDType(appntListSchema
					.getIDType()));
			tool.addOm(APPNTITEM, "IDNO", appntListSchema.getIDNo());
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", appntListSchema
					.getOccupationType());
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", appntListSchema
					.getOccupationCode());
			tool.addOm(APPNTITEM, "POSTALADDRESS", appntListSchema
					.getPostalAddress());
			tool.addOm(APPNTITEM, "ZIPCODE", appntListSchema.getZipCode());
			tool.addOm(APPNTITEM, "PHONT", appntListSchema.getPhont());
			tool.addOm(APPNTITEM, "MOBILE", appntListSchema.getMobile());
			tool.addOm(APPNTITEM, "EMAIL", appntListSchema.getEmail());
			tool.addOm(APPNTITEM, "GRPNAME", appntListSchema.getGrpName());
			tool.addOm(APPNTITEM, "COMPANYPHONE", appntListSchema
					.getCompanyPhone());
			tool.addOm(APPNTITEM, "COMPADDR", appntListSchema.getCompAddr());
			tool.addOm(APPNTITEM, "COMPZIPCODE", appntListSchema
					.getCompZipCode());

			APPNTLIST.addChild(APPNTITEM);
		}else if(null != insuListSchema) {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", insuListSchema.getName());
			tool.addOm(APPNTITEM, "SEX", tool.getChSex(insuListSchema.getSex()));
			tool.addOm(APPNTITEM, "BIRTHDAY", insuListSchema.getBirthday());
			tool.addOm(APPNTITEM, "IDTYPE", tool.queryIDType(insuListSchema.getIdType()));
			tool.addOm(APPNTITEM, "IDNO", insuListSchema.getIdNo());
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", insuListSchema.getOccupationType());
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", insuListSchema.getOccupationCode());
			tool.addOm(APPNTITEM, "POSTALADDRESS", insuListSchema.getPostalAddress());
			tool.addOm(APPNTITEM, "ZIPCODE", insuListSchema.getZipCode());
			tool.addOm(APPNTITEM, "PHONT", insuListSchema.getPhone());
			tool.addOm(APPNTITEM, "MOBILE", insuListSchema.getMobile());
			tool.addOm(APPNTITEM, "EMAIL", insuListSchema.getEMail());
			tool.addOm(APPNTITEM, "GRPNAME", insuListSchema.getGrpName());
			tool.addOm(APPNTITEM, "COMPANYPHONE", insuListSchema.getCompanyPhone());
			tool.addOm(APPNTITEM, "COMPADDR", "");
			tool.addOm(APPNTITEM, "COMPZIPCODE", "");

			APPNTLIST.addChild(APPNTITEM);
		} else {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", "");
			tool.addOm(APPNTITEM, "SEX", "");
			tool.addOm(APPNTITEM, "BIRTHDAY", "");
			tool.addOm(APPNTITEM, "IDTYPE", "");
			tool.addOm(APPNTITEM, "IDNO", "");
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", "");
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", "");
			tool.addOm(APPNTITEM, "POSTALADDRESS", "");
			tool.addOm(APPNTITEM, "ZIPCODE", "");
			tool.addOm(APPNTITEM, "PHONT", "");
			tool.addOm(APPNTITEM, "MOBILE", "");
			tool.addOm(APPNTITEM, "EMAIL", "");
			tool.addOm(APPNTITEM, "GRPNAME", "");
			tool.addOm(APPNTITEM, "COMPANYPHONE", "");
			tool.addOm(APPNTITEM, "COMPADDR", "");
			tool.addOm(APPNTITEM, "COMPZIPCODE", "");

			APPNTLIST.addChild(APPNTITEM);
		}

		DATASET.addChild(APPNTLIST);

		// 被保人信息
		OMElement INSULIST = fac.createOMElement("INSULIST", null);
		if (null != tLICardActiveInfoListSet) {
			while (null != tLICardActiveInfoListSet.get(1)
					|| "".equals(tLICardActiveInfoListSet.get(1))) {
				insuListSchema = tLICardActiveInfoListSet.get(1);

				OMElement INSUITEM = fac.createOMElement("ITEM", null);
				tool.addOm(INSUITEM, "INSUNO", String.valueOf(insuListSchema
						.getSequenceNo()));
				//获取被保人关系，包括与投保人关系及连带被保人关系 by gzh
				String sql = "select RelationCode,ToAppntRela,RelationToInsured from WFInsuList where idno = '"+insuListSchema.getIdNo()+"'";
				tSSRS = new ExeSQL().execSQL(sql);
				if(tSSRS.MaxRow>0){
					tool.addOm(INSUITEM, "TOAPPNTRELA", tSSRS.GetText(1, 2));
					tool.addOm(INSUITEM, "RELATIONCODE", tSSRS.GetText(1, 1));
					tool.addOm(INSUITEM, "RELATIONTOINSURED", tSSRS.GetText(1, 3));
				}else{
					tool.addOm(INSUITEM, "TOAPPNTRELA", "");
					tool.addOm(INSUITEM, "RELATIONCODE", "");
					tool.addOm(INSUITEM, "RELATIONTOINSURED", "");
				}
				
				tool.addOm(INSUITEM, "REINSUNO", "");

				tool.addOm(INSUITEM, "NAME", insuListSchema.getName());

				tool.addOm(INSUITEM, "SEX", tool.getChSex(insuListSchema
						.getSex()));
				tool.addOm(INSUITEM, "BIRTHDAY", insuListSchema.getBirthday());
				tool.addOm(INSUITEM, "IDTYPE", tool.queryIDType(insuListSchema
						.getIdType()));
				tool.addOm(INSUITEM, "IDNO", insuListSchema.getIdNo());
				tool.addOm(INSUITEM, "OCCUPATIONTYPE", insuListSchema
						.getOccupationType());
				tool.addOm(INSUITEM, "OCCUPATIONCODE", insuListSchema
						.getOccupationCode());
				tool.addOm(INSUITEM, "POSTALADDRESS", insuListSchema
						.getPostalAddress());
				tool.addOm(INSUITEM, "ZIPCODE", insuListSchema.getZipCode());
				tool.addOm(INSUITEM, "PHONT", insuListSchema.getPhone());
				tool.addOm(INSUITEM, "MOBILE", insuListSchema.getMobile());
				tool.addOm(INSUITEM, "EMAIL", insuListSchema.getEMail());
				tool.addOm(INSUITEM, "GRPNAME", insuListSchema.getGrpName());
				tool.addOm(INSUITEM, "COMPANYPHONE", insuListSchema
						.getCompanyPhone());
				tool.addOm(INSUITEM, "COMPADDR", "");
				tool.addOm(INSUITEM, "COMPZIPCODE", "");

				tLICardActiveInfoListSet.remove(insuListSchema);
				INSULIST.addChild(INSUITEM);
			}
		} else {
			OMElement INSUITEM = fac.createOMElement("ITEM", null);
			tool.addOm(INSUITEM, "INSUNO", "");
			tool.addOm(INSUITEM, "TOAPPNTRELA", "");
			tool.addOm(INSUITEM, "RELATIONCODE", "");
			tool.addOm(INSUITEM, "RELATIONTOINSURED", "");
			tool.addOm(INSUITEM, "REINSUNO", "");
			tool.addOm(INSUITEM, "NAME", "");
			tool.addOm(INSUITEM, "SEX", "");
			tool.addOm(INSUITEM, "BIRTHDAY", "");
			tool.addOm(INSUITEM, "IDTYPE", "");
			tool.addOm(INSUITEM, "IDNO", "");
			tool.addOm(INSUITEM, "OCCUPATIONTYPE", "");
			tool.addOm(INSUITEM, "OCCUPATIONCODE", "");
			tool.addOm(INSUITEM, "POSTALADDRESS", "");
			tool.addOm(INSUITEM, "ZIPCODE", "");
			tool.addOm(INSUITEM, "PHONT", "");
			tool.addOm(INSUITEM, "MOBILE", "");
			tool.addOm(INSUITEM, "EMAIL", "");
			tool.addOm(INSUITEM, "GRPNAME", "");
			tool.addOm(INSUITEM, "COMPANYPHONE", "");
			tool.addOm(INSUITEM, "COMPADDR", "");
			tool.addOm(INSUITEM, "COMPZIPCODE", "");

			INSULIST.addChild(INSUITEM);
		}

		DATASET.addChild(INSULIST);
		// 受益人信息

		OMElement BNFLIST = fac.createOMElement("BNFLIST", null);
		if (null != bnfListSet) {
			WFBnfListSchema bnfListSchema;
			while (null != bnfListSet.get(1) || "".equals(bnfListSet.get(1))) {
				bnfListSchema = bnfListSet.get(1);

				OMElement BNFITEM = fac.createOMElement("ITEM", null);
				tool.addOm(BNFITEM, "TOINSUNO", String.valueOf(bnfListSchema
						.getToInsuNo()));
				tool
						.addOm(BNFITEM, "TOINSURELA", bnfListSchema
								.getToInsuRela());
				tool.addOm(BNFITEM, "BNFTYPE", bnfListSchema.getBnfType());
				tool.addOm(BNFITEM, "BNFGRADE", bnfListSchema.getBnfGrade());
				tool.addOm(BNFITEM, "BNFRATE", String.valueOf(bnfListSchema
						.getBnfRate()));
				tool.addOm(BNFITEM, "NAME", bnfListSchema.getName());
				tool.addOm(BNFITEM, "SEX", tool
						.getChSex(bnfListSchema.getSex()));
				tool.addOm(BNFITEM, "BIRTHDAY", bnfListSchema.getBirthday());
				tool.addOm(BNFITEM, "IDTYPE", tool.queryIDType(bnfListSchema
						.getIDType()));
				tool.addOm(BNFITEM, "IDNO", bnfListSchema.getIDNo());

				bnfListSet.remove(bnfListSchema);
				BNFLIST.addChild(BNFITEM);
			}
		} else {
			OMElement BNFITEM = fac.createOMElement("ITEM", null);
			tool.addOm(BNFITEM, "TOINSUNO", "");
			tool.addOm(BNFITEM, "TOINSURELA", "");
			tool.addOm(BNFITEM, "BNFTYPE", "");
			tool.addOm(BNFITEM, "BNFGRADE", "");
			tool.addOm(BNFITEM, "BNFRATE", "");
			tool.addOm(BNFITEM, "NAME", "");
			tool.addOm(BNFITEM, "SEX", "");
			tool.addOm(BNFITEM, "BIRTHDAY", "");
			tool.addOm(BNFITEM, "IDTYPE", "");
			tool.addOm(BNFITEM, "IDNO", "");

			BNFLIST.addChild(BNFITEM);
		}

		DATASET.addChild(BNFLIST);

		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}
	
//	 根据核心中间表 LiCertfy 等表构建新报文 20101115 by gzh
	private OMElement buildResponseOMEByLiCertfy(
			LICertifySet tLICertifySet,
			LICertifyInsuredSet tLICertifyInsuredSet,
			WFAppntListSchema appntListSchema, WFBnfListSet bnfListSet,
			String state, String errCode, String errInfo) {
		//获取卡信息
		LICertifySchema tLICertifySchema = tLICertifySet.get(1);
		LICertifyInsuredSchema tLICertifyInsuredSchema = new LICertifyInsuredSchema();
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("ZCARDQUERY", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", messageType);
		tool.addOm(DATASET, "STATE", state);		

		tool.addOm(DATASET, "CARDDEALTYPE", cardDealType);

		// 保险卡信息
		OMElement CONTLIST = fac.createOMElement("CONTLIST", null);
		if (null != tLICertifySchema) {
//			tool.addOm(DATASET, "CERTIFYCODE", contListSchema.getCardType());//套餐编码
			tool.addOm(DATASET, "CERTIFYCODE", tool.getRiskSetCodeByCardNo(cardNo)[0]);//套餐编码
			
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "CARDNO", cardNo);
			tool.addOm(ITEM, "PASSWORD", password);
			tool.addOm(ITEM, "CERTIFYCODE", certifyCode);

			tool.addOm(ITEM, "ACTIVEDATE", tLICertifySchema.getActiveDate());//获取激活日期

			// 获取保险期间及保险期间标志 并拼接 Modify at 2010-7-12
			String [] proInfoArr = tool.getProductInfo(cardNo);
		
			tool.addOm(ITEM, "CVALIDATE", proInfoArr[1]);
			tool.addOm(ITEM, "ENDDATE", proInfoArr[2]);
			
			//modify by zhangyige 2012-07-12
			//如果WFContList中存在生效时间，那么组件在返回报文中
			String getTimeSql = "select cvalidatetime "
				+ "from wfcontlist where cardNo='"
				+ cardNo
				+ "'and branchcode = 'LKBSys' order by date(senddate) desc,time(sendtime) desc "
				+ "fetch first 1 rows only";
			ExeSQL tExeSQL = new ExeSQL();
			String valiTime = tExeSQL.getOneValue(getTimeSql);
			if(!valiTime.equals("")&&valiTime!=null){
				tool.addOm(ITEM, "CVALIDATETIME", valiTime);
				tool.addOm(ITEM, "ENDDATETIME", valiTime);
			}
			
			// 查询套餐名称
			tool.addOm(ITEM, "CERTIFYNAME", proInfoArr[3] );// 套餐名称
			String oldPrem = String.valueOf(tLICertifySchema.getPrem());
			System.out.println("保费："+oldPrem+"=========901");
			if("".equals(oldPrem)||oldPrem==null){
				oldPrem = "0";
			}
			tool.addOm(ITEM, "PREM",oldPrem );
			tool.addOm(ITEM, "INSUYEAR", proInfoArr[0]);
			//管理机构
			String manageArr = tool.getManageComName(cardNo);
			tool.addOm(ITEM, "MANAGECOM", manageArr);
			
			CONTLIST.addChild(ITEM);
		} else {
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "CARDNO", "");
			tool.addOm(ITEM, "PASSWORD", "");
			tool.addOm(ITEM, "CERTIFYCODE", "");
			tool.addOm(ITEM, "CERTIFYNAME", "");
			tool.addOm(ITEM, "ACTIVEDATE", "");
			tool.addOm(ITEM, "CVALIDATE", "");
			tool.addOm(ITEM, "SALEAGENT", "");
			tool.addOm(ITEM, "PREM", "");
			tool.addOm(ITEM, "INSUYEAR", "");

			CONTLIST.addChild(ITEM);
		}

		DATASET.addChild(CONTLIST);

		// 投保人信息
		OMElement APPNTLIST = fac.createOMElement("APPNTLIST", null);
		if (null != appntListSchema) {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", appntListSchema.getName());

			tool.addOm(APPNTITEM, "SEX", tool
					.getChSex(appntListSchema.getSex()));
			tool.addOm(APPNTITEM, "BIRTHDAY", appntListSchema.getBirthday());
			tool.addOm(APPNTITEM, "IDTYPE", tool.queryIDType(appntListSchema
					.getIDType()));
			tool.addOm(APPNTITEM, "IDNO", appntListSchema.getIDNo());
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", appntListSchema
					.getOccupationType());
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", appntListSchema
					.getOccupationCode());
			tool.addOm(APPNTITEM, "POSTALADDRESS", appntListSchema
					.getPostalAddress());
			tool.addOm(APPNTITEM, "ZIPCODE", appntListSchema.getZipCode());
			tool.addOm(APPNTITEM, "PHONT", appntListSchema.getPhont());
			tool.addOm(APPNTITEM, "MOBILE", appntListSchema.getMobile());
			tool.addOm(APPNTITEM, "EMAIL", appntListSchema.getEmail());
			tool.addOm(APPNTITEM, "GRPNAME", appntListSchema.getGrpName());
			tool.addOm(APPNTITEM, "COMPANYPHONE", appntListSchema
					.getCompanyPhone());
			tool.addOm(APPNTITEM, "COMPADDR", appntListSchema.getCompAddr());
			tool.addOm(APPNTITEM, "COMPZIPCODE", appntListSchema
					.getCompZipCode());

			APPNTLIST.addChild(APPNTITEM);
		}else if(null != tLICertifyInsuredSet){
			tLICertifyInsuredSchema = tLICertifyInsuredSet.get(1);
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", tLICertifyInsuredSchema.getName());
			tool.addOm(APPNTITEM, "SEX", tool.getChSex(tLICertifyInsuredSchema.getSex()));
			tool.addOm(APPNTITEM, "BIRTHDAY", tLICertifyInsuredSchema.getBirthday());
			tool.addOm(APPNTITEM, "IDTYPE", tool.queryIDType(tLICertifyInsuredSchema.getIdType()));
			tool.addOm(APPNTITEM, "IDNO", tLICertifyInsuredSchema.getIdNo());
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", tLICertifyInsuredSchema.getOccupationType());
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", tLICertifyInsuredSchema.getOccupationCode());
			tool.addOm(APPNTITEM, "POSTALADDRESS", tLICertifyInsuredSchema.getPostalAddress());
			tool.addOm(APPNTITEM, "ZIPCODE", tLICertifyInsuredSchema.getZipCode());
			tool.addOm(APPNTITEM, "PHONT", tLICertifyInsuredSchema.getPhone());
			tool.addOm(APPNTITEM, "MOBILE", tLICertifyInsuredSchema.getMobile());
			tool.addOm(APPNTITEM, "EMAIL", tLICertifyInsuredSchema.getEMail());
			tool.addOm(APPNTITEM, "GRPNAME", tLICertifyInsuredSchema.getGrpName());
			tool.addOm(APPNTITEM, "COMPANYPHONE", tLICertifyInsuredSchema.getCompanyPhone());
			tool.addOm(APPNTITEM, "COMPADDR", "");
			tool.addOm(APPNTITEM, "COMPZIPCODE", "");

			APPNTLIST.addChild(APPNTITEM);
		} else {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", "");
			tool.addOm(APPNTITEM, "SEX", "");
			tool.addOm(APPNTITEM, "BIRTHDAY", "");
			tool.addOm(APPNTITEM, "IDTYPE", "");
			tool.addOm(APPNTITEM, "IDNO", "");
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", "");
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", "");
			tool.addOm(APPNTITEM, "POSTALADDRESS", "");
			tool.addOm(APPNTITEM, "ZIPCODE", "");
			tool.addOm(APPNTITEM, "PHONT", "");
			tool.addOm(APPNTITEM, "MOBILE", "");
			tool.addOm(APPNTITEM, "EMAIL", "");
			tool.addOm(APPNTITEM, "GRPNAME", "");
			tool.addOm(APPNTITEM, "COMPANYPHONE", "");
			tool.addOm(APPNTITEM, "COMPADDR", "");
			tool.addOm(APPNTITEM, "COMPZIPCODE", "");

			APPNTLIST.addChild(APPNTITEM);
		}

		DATASET.addChild(APPNTLIST);

		// 被保人信息
		OMElement INSULIST = fac.createOMElement("INSULIST", null);
		if (null != tLICertifyInsuredSet) {
			while (null != tLICertifyInsuredSet.get(1)
					|| "".equals(tLICertifyInsuredSet.get(1))) {
				tLICertifyInsuredSchema = tLICertifyInsuredSet.get(1);

				OMElement INSUITEM = fac.createOMElement("ITEM", null);
				tool.addOm(INSUITEM, "INSUNO", String.valueOf(tLICertifyInsuredSchema
						.getSequenceNo()));
				//获取被保人关系，包括与投保人关系及连带被保人关系 by gzh
				String sql = "select RelationCode,ToAppntRela,RelationToInsured from WFInsuList where idno = '"+tLICertifyInsuredSchema.getIdNo()+"'";
				tSSRS = new ExeSQL().execSQL(sql);
				if(tSSRS.MaxRow>0){
					tool.addOm(INSUITEM, "TOAPPNTRELA", tSSRS.GetText(1, 2));
					tool.addOm(INSUITEM, "RELATIONCODE", tSSRS.GetText(1, 1));
					tool.addOm(INSUITEM, "RELATIONTOINSURED", tSSRS.GetText(1, 3));
				}else{
					tool.addOm(INSUITEM, "TOAPPNTRELA", "");
					tool.addOm(INSUITEM, "RELATIONCODE", "");
					tool.addOm(INSUITEM, "RELATIONTOINSURED", "");
				}
				
				tool.addOm(INSUITEM, "REINSUNO", "");

				tool.addOm(INSUITEM, "NAME", tLICertifyInsuredSchema.getName());

				tool.addOm(INSUITEM, "SEX", tool.getChSex(tLICertifyInsuredSchema
						.getSex()));
				tool.addOm(INSUITEM, "BIRTHDAY", tLICertifyInsuredSchema.getBirthday());
				tool.addOm(INSUITEM, "IDTYPE", tool.queryIDType(tLICertifyInsuredSchema
						.getIdType()));
				tool.addOm(INSUITEM, "IDNO", tLICertifyInsuredSchema.getIdNo());
				tool.addOm(INSUITEM, "OCCUPATIONTYPE", tLICertifyInsuredSchema
						.getOccupationType());
				tool.addOm(INSUITEM, "OCCUPATIONCODE", tLICertifyInsuredSchema
						.getOccupationCode());
				tool.addOm(INSUITEM, "POSTALADDRESS", tLICertifyInsuredSchema
						.getPostalAddress());
				tool.addOm(INSUITEM, "ZIPCODE", tLICertifyInsuredSchema.getZipCode());
				tool.addOm(INSUITEM, "PHONT", tLICertifyInsuredSchema.getPhone());
				tool.addOm(INSUITEM, "MOBILE", tLICertifyInsuredSchema.getMobile());
				tool.addOm(INSUITEM, "EMAIL", tLICertifyInsuredSchema.getEMail());
				tool.addOm(INSUITEM, "GRPNAME", tLICertifyInsuredSchema.getGrpName());
				tool.addOm(INSUITEM, "COMPANYPHONE", tLICertifyInsuredSchema
						.getCompanyPhone());
				tool.addOm(INSUITEM, "COMPADDR", "");
				tool.addOm(INSUITEM, "COMPZIPCODE", "");

				tLICertifyInsuredSet.remove(tLICertifyInsuredSchema);
				INSULIST.addChild(INSUITEM);
			}
		} else {
			OMElement INSUITEM = fac.createOMElement("ITEM", null);
			tool.addOm(INSUITEM, "INSUNO", "");
			tool.addOm(INSUITEM, "TOAPPNTRELA", "");
			tool.addOm(INSUITEM, "RELATIONCODE", "");
			tool.addOm(INSUITEM, "RELATIONTOINSURED", "");
			tool.addOm(INSUITEM, "REINSUNO", "");
			tool.addOm(INSUITEM, "NAME", "");
			tool.addOm(INSUITEM, "SEX", "");
			tool.addOm(INSUITEM, "BIRTHDAY", "");
			tool.addOm(INSUITEM, "IDTYPE", "");
			tool.addOm(INSUITEM, "IDNO", "");
			tool.addOm(INSUITEM, "OCCUPATIONTYPE", "");
			tool.addOm(INSUITEM, "OCCUPATIONCODE", "");
			tool.addOm(INSUITEM, "POSTALADDRESS", "");
			tool.addOm(INSUITEM, "ZIPCODE", "");
			tool.addOm(INSUITEM, "PHONT", "");
			tool.addOm(INSUITEM, "MOBILE", "");
			tool.addOm(INSUITEM, "EMAIL", "");
			tool.addOm(INSUITEM, "GRPNAME", "");
			tool.addOm(INSUITEM, "COMPANYPHONE", "");
			tool.addOm(INSUITEM, "COMPADDR", "");
			tool.addOm(INSUITEM, "COMPZIPCODE", "");

			INSULIST.addChild(INSUITEM);
		}

		DATASET.addChild(INSULIST);
		// 受益人信息

		OMElement BNFLIST = fac.createOMElement("BNFLIST", null);
		if (null != bnfListSet) {
			WFBnfListSchema bnfListSchema;
			while (null != bnfListSet.get(1) || "".equals(bnfListSet.get(1))) {
				bnfListSchema = bnfListSet.get(1);

				OMElement BNFITEM = fac.createOMElement("ITEM", null);
				tool.addOm(BNFITEM, "TOINSUNO", String.valueOf(bnfListSchema
						.getToInsuNo()));
				tool
						.addOm(BNFITEM, "TOINSURELA", bnfListSchema
								.getToInsuRela());
				tool.addOm(BNFITEM, "BNFTYPE", bnfListSchema.getBnfType());
				tool.addOm(BNFITEM, "BNFGRADE", bnfListSchema.getBnfGrade());
				tool.addOm(BNFITEM, "BNFRATE", String.valueOf(bnfListSchema
						.getBnfRate()));
				tool.addOm(BNFITEM, "NAME", bnfListSchema.getName());
				tool.addOm(BNFITEM, "SEX", tool
						.getChSex(bnfListSchema.getSex()));
				tool.addOm(BNFITEM, "BIRTHDAY", bnfListSchema.getBirthday());
				tool.addOm(BNFITEM, "IDTYPE", tool.queryIDType(bnfListSchema
						.getIDType()));
				tool.addOm(BNFITEM, "IDNO", bnfListSchema.getIDNo());

				bnfListSet.remove(bnfListSchema);
				BNFLIST.addChild(BNFITEM);
			}
		} else {
			OMElement BNFITEM = fac.createOMElement("ITEM", null);
			tool.addOm(BNFITEM, "TOINSUNO", "");
			tool.addOm(BNFITEM, "TOINSURELA", "");
			tool.addOm(BNFITEM, "BNFTYPE", "");
			tool.addOm(BNFITEM, "BNFGRADE", "");
			tool.addOm(BNFITEM, "BNFRATE", "");
			tool.addOm(BNFITEM, "NAME", "");
			tool.addOm(BNFITEM, "SEX", "");
			tool.addOm(BNFITEM, "BIRTHDAY", "");
			tool.addOm(BNFITEM, "IDTYPE", "");
			tool.addOm(BNFITEM, "IDNO", "");

			BNFLIST.addChild(BNFITEM);
		}

		DATASET.addChild(BNFLIST);

		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
