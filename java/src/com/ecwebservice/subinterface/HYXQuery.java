package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;


import com.sinosoft.lis.db.WFAppntListDB;
import com.sinosoft.lis.db.WFBnfListDB;
import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.db.WFInsuListDB;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:远程出单 当日保单查询
 * 
 * @author yuanzw 创建时间：2010-2-3 下午02:21:52 Description:
 * @version
 */

public class HYXQuery {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String cardNo = null;// 单证号

	private String password = null;

	// 保险卡信息

	String certifyCode;

	String cardDealType;

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {
			// 保险卡信息
			WFContListSchema contListSchema = null;
			// 投保人信息
			WFAppntListSchema appntListSchema = null;
			WFInsuListSet insuListSet = null;
			WFBnfListSet bnfListSet = null;

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("HYXQUERY")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);
							if (!"04".equals(messageType)) {
								responseOME = buildResponseOME(null,null,null,null,"01",
										"5", "报文类型不正确!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "5", "报文类型不正确!");
								return responseOME;
							}
						}

						if (child_element.getLocalName().equals("CARDNO")) {
							cardNo = child_element.getText();

							System.out.println(cardNo);
						}

					}
					// 日志3
					LoginVerifyTool.writeLoginLog(cardNo, "无");
					// 未处理
					if (batchno == null || "".equals(batchno)) {
						responseOME = buildResponseOME(null, null, null, null,
								"01", "7", "批次号为空值");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME,batchno, messageType,
								sendDate, sendTime, "01", "100", "7", "批次号为空值");
						return responseOME;
					}
					if (cardNo == null || "".equals(cardNo)) {
						responseOME = buildResponseOME(null, null, null, null,
								"01", "1", "卡单号码为空值");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool
								.writeLog(responseOME,batchno, messageType, sendDate,
										sendTime, "01", "100", "1", "卡单号码为空值");
						return responseOME;
					}
					

				}
			}
			// 保险卡信息查询
			WFContListDB contListDB = new WFContListDB();


			String sql = "select senddate,sendtime,batchno from   "
				+ "wfcontlist where cardNo='"+cardNo+"' order by date(senddate) desc,time(sendtime) desc "
				+ "fetch first 1 rows only";

			SSRS sResult = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			sResult = tExeSQL.execSQL(sql);
			if (null != sResult && sResult.MaxRow > 0) {

				String sendDate = sResult.GetText(1, 1);
				String sendTime = sResult.GetText(1, 2);
				String aBatchNo = sResult.GetText(1, 3);

				contListDB.setCardNo(cardNo);
				contListDB.setSendDate(sendDate);
				contListDB.setSendTime(sendTime);

				if (contListDB.query().size() != 0) {
					contListSchema = contListDB.query().get(1);
					password = contListSchema.getPassword();
					certifyCode = contListSchema.getCertifyCode();
					cardDealType = contListSchema.getCardDealType();

				}

				// 投保人信息查询
				WFAppntListDB appntListDB = new WFAppntListDB();
				appntListDB.setBatchNo(aBatchNo);

				if (appntListDB.query().size() != 0) {
					appntListSchema = appntListDB.query().get(1);

				}

				// 被保人信息
				WFInsuListDB insuListDB = new WFInsuListDB();
				insuListDB.setBatchNo(aBatchNo);

				if (insuListDB.query().size() != 0) {
					insuListSet = insuListDB.query();

				}
				// 受益人信息
				WFBnfListDB bnfListDB = new WFBnfListDB();
				bnfListDB.setBatchNo(aBatchNo);

				if (bnfListDB.query().size() != 0) {
					bnfListSet = bnfListDB.query();

				}

			} else {
				responseOME = buildResponseOME(null, null, null, null, "01",
						"3", "卡号不正确");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate,
						sendTime, "01", "100", "3", "卡号不正确");
				return responseOME;
			}

			responseOME = buildResponseOME(contListSchema, appntListSchema,
					insuListSet, bnfListSet, "00", "0", "成功");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate, sendTime,
					"00", "100", "0", "成功");

		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME(null, null, null, null, "01", "4",
					"发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate, sendTime,
					"01", "100", "4", "发生异常");
			return responseOME;
		}

		return responseOME;

	}

	private OMElement buildResponseOME(WFContListSchema contListSchema,
			WFAppntListSchema appntListSchema, WFInsuListSet insuListSet,
			WFBnfListSet bnfListSet, String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement DATASET = fac.createOMElement("HYXQUERY", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "CERTIFYCODE", certifyCode);
		tool.addOm(DATASET, "CARDDEALTYPE", cardDealType);

		// 保险卡信息
		OMElement CONTLIST = fac.createOMElement("CONTLIST", null);
		if (null != contListSchema) {
			OMElement ITEM = fac.createOMElement("ITEM", null);
			
			//add at 2010-7-9 新增销售机构
			String agentcomCode = contListSchema.getBranchCode();
			String agentcomNameSql = "select (select name from ldcom where ldcom.comcode=managecom) from lacom where agentcom='"+agentcomCode+"'";
			ExeSQL tExeSQL = new ExeSQL();
			String agentComName = tExeSQL.getOneValue(agentcomNameSql);
			tool.addOm(ITEM, "SALEAGENT", agentComName);
			
			tool.addOm(ITEM, "CARDNO", contListSchema.getCardNo());
			tool.addOm(ITEM, "PASSWORD", contListSchema.getPassword());
			tool.addOm(ITEM, "CERTIFYCODE", contListSchema.getCertifyCode());
			tool.addOm(ITEM, "CERTIFYNAME", contListSchema.getCertifyName());
			tool.addOm(ITEM, "ACTIVEDATE", contListSchema.getActiveDate());
			tool.addOm(ITEM, "MANAGECOM", contListSchema.getManageCom());
			tool.addOm(ITEM, "SALECHNL", contListSchema.getSaleChnl());
			tool.addOm(ITEM, "AGENTCOM", contListSchema.getAgentCom());
			tool.addOm(ITEM, "AGENTCODE", contListSchema.getAgentCode());
			tool.addOm(ITEM, "AGENTNAME", contListSchema.getAgentName());
			tool.addOm(ITEM, "PAYMODE", contListSchema.getPayMode());
			tool.addOm(ITEM, "PAYINTV", contListSchema.getPayIntv());
			tool.addOm(ITEM, "PAYYEAR", String.valueOf(contListSchema
					.getPayYear()));
			tool.addOm(ITEM, "PAYYEARFLAG", contListSchema.getPayYearFlag());
			tool.addOm(ITEM, "CVALIDATE", contListSchema.getCvaliDate());
			tool.addOm(ITEM, "CVALIDATETIME", String.valueOf(contListSchema
					.getCvaliDateTime()));
			tool.addOm(ITEM, "INSUYEAR", String.valueOf(contListSchema
					.getInsuYear()));
			tool.addOm(ITEM, "INSUYEARFLAG", contListSchema.getInsuYearFlag());
			tool.addOm(ITEM, "PREM", String.valueOf(contListSchema.getPrem()));
			tool.addOm(ITEM, "AMNT", String.valueOf(contListSchema.getAmnt()));
			tool.addOm(ITEM, "MULT", String.valueOf(contListSchema.getMult()));
			tool
					.addOm(ITEM, "COPYS", String.valueOf(contListSchema
							.getCopys()));
			tool.addOm(ITEM, "TICKETNO", contListSchema.getTicketNo());
			tool.addOm(ITEM, "TEAMNO", contListSchema.getTeamNo());
			tool.addOm(ITEM, "SEATNO", contListSchema.getSeatNo());
			tool.addOm(ITEM, "FROM", contListSchema.getFrom());
			tool.addOm(ITEM, "TO", contListSchema.getTo());
			tool.addOm(ITEM, "REMARK", contListSchema.getRemark());

			CONTLIST.addChild(ITEM);
		} else {
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "CARDNO", "");
			tool.addOm(ITEM, "PASSWORD", "");
			tool.addOm(ITEM, "CERTIFYCODE", "");
			tool.addOm(ITEM, "CERTIFYNAME", "");
			tool.addOm(ITEM, "ACTIVEDATE", "");
			tool.addOm(ITEM, "MANAGECOM", "");
			tool.addOm(ITEM, "SALECHNL", "");
			tool.addOm(ITEM, "AGENTCOM", "");
			tool.addOm(ITEM, "AGENTCODE", "");
			tool.addOm(ITEM, "AGENTNAME", "");
			tool.addOm(ITEM, "PAYMODE", "");
			tool.addOm(ITEM, "PAYINTV", "");
			tool.addOm(ITEM, "PAYYEAR", String.valueOf(""));
			tool.addOm(ITEM, "PAYYEARFLAG", "");
			tool.addOm(ITEM, "CVALIDATE", "");
			tool.addOm(ITEM, "CVALIDATETIME", String.valueOf(""));
			tool.addOm(ITEM, "INSUYEAR", String.valueOf(""));
			tool.addOm(ITEM, "INSUYEARFLAG", "");
			tool.addOm(ITEM, "PREM", String.valueOf(""));
			tool.addOm(ITEM, "AMNT", String.valueOf(""));
			tool.addOm(ITEM, "MULT", String.valueOf(""));
			tool.addOm(ITEM, "COPYS", String.valueOf(""));
			tool.addOm(ITEM, "TICKETNO", "");
			tool.addOm(ITEM, "TEAMNO", "");
			tool.addOm(ITEM, "SEATNO", "");
			tool.addOm(ITEM, "FROM", "");
			tool.addOm(ITEM, "TO", "");
			tool.addOm(ITEM, "REMARK", "");

			CONTLIST.addChild(ITEM);
		}

		DATASET.addChild(CONTLIST);

		// 投保人信息
		OMElement APPNTLIST = fac.createOMElement("APPNTLIST", null);
		if (null != appntListSchema) {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", appntListSchema.getName());
			tool.addOm(APPNTITEM, "SEX", tool
					.getChSex(appntListSchema.getSex()));
			tool.addOm(APPNTITEM, "BIRTHDAY", appntListSchema.getBirthday());
			tool.addOm(APPNTITEM, "IDTYPE", tool.queryIDType(appntListSchema.getIDType()));
			tool.addOm(APPNTITEM, "IDNO", appntListSchema.getIDNo());
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", appntListSchema
					.getOccupationType());
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", appntListSchema
					.getOccupationCode());
			tool.addOm(APPNTITEM, "POSTALADDRESS", appntListSchema
					.getPostalAddress());
			tool.addOm(APPNTITEM, "ZIPCODE", appntListSchema.getZipCode());
			tool.addOm(APPNTITEM, "PHONT", appntListSchema.getPhont());
			tool.addOm(APPNTITEM, "MOBILE", appntListSchema.getMobile());
			tool.addOm(APPNTITEM, "EMAIL", appntListSchema.getEmail());
			tool.addOm(APPNTITEM, "GRPNAME", appntListSchema.getGrpName());
			tool.addOm(APPNTITEM, "COMPANYPHONE", appntListSchema
					.getCompanyPhone());
			tool.addOm(APPNTITEM, "COMPADDR", appntListSchema.getCompAddr());
			tool.addOm(APPNTITEM, "COMPZIPCODE", appntListSchema
					.getCompZipCode());

			APPNTLIST.addChild(APPNTITEM);
		} else {
			OMElement APPNTITEM = fac.createOMElement("ITEM", null);
			tool.addOm(APPNTITEM, "NAME", "");
			tool.addOm(APPNTITEM, "SEX", "");
			tool.addOm(APPNTITEM, "BIRTHDAY", "");
			tool.addOm(APPNTITEM, "IDTYPE", "");
			tool.addOm(APPNTITEM, "IDNO", "");
			tool.addOm(APPNTITEM, "OCCUPATIONTYPE", "");
			tool.addOm(APPNTITEM, "OCCUPATIONCODE", "");
			tool.addOm(APPNTITEM, "POSTALADDRESS", "");
			tool.addOm(APPNTITEM, "ZIPCODE", "");
			tool.addOm(APPNTITEM, "PHONT", "");
			tool.addOm(APPNTITEM, "MOBILE", "");
			tool.addOm(APPNTITEM, "EMAIL", "");
			tool.addOm(APPNTITEM, "GRPNAME", "");
			tool.addOm(APPNTITEM, "COMPANYPHONE", "");
			tool.addOm(APPNTITEM, "COMPADDR", "");
			tool.addOm(APPNTITEM, "COMPZIPCODE", "");

			APPNTLIST.addChild(APPNTITEM);
		}

		DATASET.addChild(APPNTLIST);

		// 被保人信息

		OMElement INSULIST = fac.createOMElement("INSULIST", null);
		if (null != insuListSet) {
			WFInsuListSchema insuListSchema;
			while (null != insuListSet.get(1) || "".equals(insuListSet.get(1))) {
				insuListSchema = insuListSet.get(1);
				OMElement INSUITEM = fac.createOMElement("ITEM", null);
				tool.addOm(INSUITEM, "INSUNO", String.valueOf(insuListSchema
						.getInsuNo()));
				tool.addOm(INSUITEM, "TOAPPNTRELA", insuListSchema
						.getToAppntRela());
				tool.addOm(INSUITEM, "RELATIONCODE", insuListSchema
						.getRelationCode());
				tool.addOm(INSUITEM, "RELATIONTOINSURED", insuListSchema
						.getRelationToInsured());
				tool.addOm(INSUITEM, "REINSUNO", insuListSchema.getReInsuNo());
				tool.addOm(INSUITEM, "NAME", insuListSchema.getName());
				tool.addOm(INSUITEM, "SEX", tool.getChSex(insuListSchema
						.getSex()));
				tool.addOm(INSUITEM, "BIRTHDAY", insuListSchema.getBirthday());
				tool.addOm(INSUITEM, "IDTYPE", tool.queryIDType(insuListSchema.getIDType()));
				tool.addOm(INSUITEM, "IDNO", insuListSchema.getIDNo());
				tool.addOm(INSUITEM, "OCCUPATIONTYPE", insuListSchema
						.getOccupationType());
				tool.addOm(INSUITEM, "OCCUPATIONCODE", insuListSchema
						.getOccupationCode());
				tool.addOm(INSUITEM, "POSTALADDRESS", insuListSchema
						.getPostalAddress());
				tool.addOm(INSUITEM, "ZIPCODE", insuListSchema.getZipCode());
				tool.addOm(INSUITEM, "PHONT", insuListSchema.getPhont());
				tool.addOm(INSUITEM, "MOBILE", insuListSchema.getMobile());
				tool.addOm(INSUITEM, "EMAIL", insuListSchema.getEmail());
				tool.addOm(INSUITEM, "GRPNAME", insuListSchema.getGrpName());
				tool.addOm(INSUITEM, "COMPANYPHONE", insuListSchema
						.getCompanyPhone());
				tool.addOm(INSUITEM, "COMPADDR", insuListSchema.getCompAddr());
				tool.addOm(INSUITEM, "COMPZIPCODE", insuListSchema
						.getCompZipCode());

				INSULIST.addChild(INSUITEM);
				insuListSet.remove(insuListSchema);
			}
		} else {
			OMElement INSUITEM = fac.createOMElement("ITEM", null);
			tool.addOm(INSUITEM, "INSUNO", "");
			tool.addOm(INSUITEM, "TOAPPNTRELA", "");
			tool.addOm(INSUITEM, "RELATIONCODE", "");
			tool.addOm(INSUITEM, "RELATIONTOINSURED", "");
			tool.addOm(INSUITEM, "REINSUNO", "");
			tool.addOm(INSUITEM, "NAME", "");
			tool.addOm(INSUITEM, "SEX", "");
			tool.addOm(INSUITEM, "BIRTHDAY", "");
			tool.addOm(INSUITEM, "IDTYPE", "");
			tool.addOm(INSUITEM, "IDNO", "");
			tool.addOm(INSUITEM, "OCCUPATIONTYPE", "");
			tool.addOm(INSUITEM, "OCCUPATIONCODE", "");
			tool.addOm(INSUITEM, "POSTALADDRESS", "");
			tool.addOm(INSUITEM, "ZIPCODE", "");
			tool.addOm(INSUITEM, "PHONT", "");
			tool.addOm(INSUITEM, "MOBILE", "");
			tool.addOm(INSUITEM, "EMAIL", "");
			tool.addOm(INSUITEM, "GRPNAME", "");
			tool.addOm(INSUITEM, "COMPANYPHONE", "");
			tool.addOm(INSUITEM, "COMPADDR", "");
			tool.addOm(INSUITEM, "COMPZIPCODE", "");

			INSULIST.addChild(INSUITEM);
		}

		DATASET.addChild(INSULIST);
		// 受益人信息

		OMElement BNFLIST = fac.createOMElement("BNFLIST", null);
		if (null != bnfListSet) {
			WFBnfListSchema bnfListSchema;
			while (null != bnfListSet.get(1) || "".equals(bnfListSet.get(1))) {
				bnfListSchema = bnfListSet.get(1);

				OMElement BNFITEM = fac.createOMElement("ITEM", null);
				tool.addOm(BNFITEM, "TOINSUNO", String.valueOf(bnfListSchema
						.getToInsuNo()));
				tool
						.addOm(BNFITEM, "TOINSURELA", bnfListSchema
								.getToInsuRela());
				tool.addOm(BNFITEM, "BNFTYPE", bnfListSchema.getBnfType());
				tool.addOm(BNFITEM, "BNFGRADE", bnfListSchema.getBnfGrade());
				tool.addOm(BNFITEM, "BNFRATE", String.valueOf(bnfListSchema
						.getBnfRate()));
				tool.addOm(BNFITEM, "NAME", bnfListSchema.getName());
				tool.addOm(BNFITEM, "SEX", tool
						.getChSex(bnfListSchema.getSex()));
				tool.addOm(BNFITEM, "BIRTHDAY", bnfListSchema.getBirthday());
				tool.addOm(BNFITEM, "IDTYPE", tool.queryIDType(bnfListSchema.getIDType()));
				tool.addOm(BNFITEM, "IDNO", bnfListSchema.getIDNo());

				bnfListSet.remove(bnfListSchema);
				BNFLIST.addChild(BNFITEM);
			}
		} else {
			OMElement BNFITEM = fac.createOMElement("ITEM", null);
			tool.addOm(BNFITEM, "TOINSUNO", "");
			tool.addOm(BNFITEM, "TOINSURELA", "");
			tool.addOm(BNFITEM, "BNFTYPE", "");
			tool.addOm(BNFITEM, "BNFGRADE", "");
			tool.addOm(BNFITEM, "BNFRATE", "");
			tool.addOm(BNFITEM, "NAME", "");
			tool.addOm(BNFITEM, "SEX", "");
			tool.addOm(BNFITEM, "BIRTHDAY", "");
			tool.addOm(BNFITEM, "IDTYPE", "");
			tool.addOm(BNFITEM, "IDNO", "");

			BNFLIST.addChild(BNFITEM);
		}

		DATASET.addChild(BNFLIST);

		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
