package com.ecwebservice.subinterface;

import java.util.Iterator;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 查询所有险种的最新结算利率信息。
 * @author 
 *	20140807
 */
public class QueryNewsetRate {

	private OMElement mOME = null;

	private String batchNo = null;// 批次号

	private String branchCode = "";// 报送单位

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String sendOperator = null; // 报送人员
	
	private MMap rateMap=new MMap();	//返回报文信息
	
	
	private OMElement responseOME = null;
	
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			return responseOME;
		}
		
		if(!check()){
			return responseOME;
		}
		
		if(!dealData()){
			return responseOME;
		}
		
		
		return responseOME=buildResponseOME("00",null,null);
	}
	
	
	private boolean load(){
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("LIS004")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BatchNo")) {
							this.batchNo = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendDate")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendTime")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("BranchCode")) {
							this.branchCode = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendOperator")) {
							this.sendOperator = child_element.getText();
							continue;
						}
					}

				} else {
					System.out.println("报文类型不匹配");
					responseOME = buildResponseOME("01", "01", "报文类型不匹配");
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME = buildResponseOME("01", "01", "加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean check(){
		
		return true;
	}
	
	
	
	
	private boolean dealData(){
		
		String strSQL="select a.riskcode, (select riskname from lmriskapp where riskcode=a.riskcode), " +
				" (select  left(cast((a.baladate) as char(10)),7)  from dual) as month, a.rate  " +
				" from LMInsuAccRate a  " +
				" where baladate=(select max(baladate) " +
				" from LMInsuAccRate where riskcode=a.riskcode) " +
				" order by month desc with ur";
	
		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(strSQL);
		if (null == tSSRS || tSSRS.MaxRow==0){
			responseOME=buildResponseOME("01","02","查询险种利率失败!");
			return false;
		}else{
			for(int i=1;i<=tSSRS.getMaxRow();i++){
				String[] riskRate=new String[4];
				
				riskRate[0]=tSSRS.GetText(i, 1);
				riskRate[1]=tSSRS.GetText(i, 2);
				riskRate[2]=tSSRS.GetText(i, 3);
				riskRate[3]=tSSRS.GetText(i, 4);
				rateMap.put(riskRate[1], riskRate);
			}
		}
		return true;
	}
	
	
	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement("LIS004", null);
		OMElement rateInfo = fac.createOMElement("RateInfo", null);
		
		addOm(DATASET, "BatchNo", this.batchNo);
		addOm(DATASET, "SendDate", PubFun.getCurrentDate());
		addOm(DATASET, "SendTime", PubFun.getCurrentTime());
		addOm(DATASET, "BranchCode", this.branchCode);
		addOm(DATASET, "SendOperator", this.sendOperator);
		addOm(DATASET, "State", state);
		addOm(DATASET, "ErrCode", errorCode);
		addOm(DATASET, "ErrInfo", errorInfo);
		
		Set set=rateMap.keySet();
		Object key = null;
		for(int i=0;i<set.size();i++){
			OMElement item = fac.createOMElement("ITEM", null);
			key = rateMap.getOrder().get(String.valueOf(i + 1));
			String[] temp = (String[]) rateMap.get(key);
				
				addOm(item, "RiskCode",temp[0]);
				addOm(item, "RiskName",temp[1]);
				addOm(item, "Month",temp[2]);
				addOm(item, "Rate",temp[3]);
				rateInfo.addChild(item);
		}
		DATASET.addChild(rateInfo);
		
		return DATASET;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
}
