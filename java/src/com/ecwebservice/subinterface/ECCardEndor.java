package com.ecwebservice.subinterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.WFAppntListDB;
import com.sinosoft.lis.db.WFInsuListDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.WFAppntListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * Title:卡单客户资料变更
 * 
 * @author zhangyige 创建时间：2012-08-03 下午02:43:12
 * @Description:
 * @version
 */

public class ECCardEndor {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String cardno = null;// 卡号

	private VData mVData = new VData(); // 存储实名化保全项目变更数据

	private VData mLIVData = new VData();// 存储中间表变更数据

	private String sendOperator = null;// g 非发送报文提供

	HashMap appntMap = new HashMap(); // 存储投保人联系方式信息

	List insuList = new ArrayList(); // 存储被保人联系方式信息
	
	MMap mMMap = new MMap();
	
	ECContEndor mECContEndor = new ECContEndor();

	/** 当前日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 当前时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	// 错误处理类
	public CErrors mErrors = new CErrors();

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {
			LoginVerifyTool mTool = new LoginVerifyTool();

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("ECCARDENDOR")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println("BATCHNO" + batchno);
							boolean validBatchNo = mTool
									.isValidBatchNo(batchno);
							if (!validBatchNo) {
								// 批次号不唯一
								responseOME = buildResponseOME("01", "21",
										"批次号不唯一!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "21", "批次号不唯一!");
								return responseOME;
							}
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}

						if (child_element.getLocalName().equals("CARDNO")) {
							cardno = child_element.getText();

							System.out.println(cardno);
						}
						// 投保人信息

						if (child_element.getLocalName().equals("APPNTLIST")) {

							/* 投保人信息 */
							String name = null;// 姓名

							String email = null; // 邮箱地址

							String mobile = null; // 手机号

							String phone = null; // 电话

							String zipcode = null; // 邮编

							String postalAddress = null; // 通讯地址

							String applyType = null;// 00 - 不变更 01 - 变更

							Iterator child1 = child_element.getChildren();

							while (child1.hasNext()) {
								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										if (child2OME.getLocalName().equals(
												"NAME")
												&& child2OME.getText() != null) {
											name = child2OME.getText();
											System.out.println("name:" + name);
										}
										if (child2OME.getLocalName().equals(
												"EMAIL")
												&& child2OME.getText() != null) {
											email = child2OME.getText();
											System.out
													.println("email:" + email);
										}
										if (child2OME.getLocalName().equals(
												"MOBILE")
												&& child2OME.getText() != null) {
											mobile = child2OME.getText();
											System.out.println("mobile:"
													+ mobile);
										}
										if (child2OME.getLocalName().equals(
												"PHONE")
												&& child2OME.getText() != null) {
											phone = child2OME.getText();
											System.out
													.println("phone:" + phone);
										}
										if (child2OME.getLocalName().equals(
												"ZIPCODE")
												&& child2OME.getText() != null) {
											zipcode = child2OME.getText();
											System.out.println("zipcode:"
													+ zipcode);
										}
										if (child2OME.getLocalName().equals(
												"POSTALADDRESS")
												&& child2OME.getText() != null) {
											postalAddress = child2OME.getText();
											System.out.println("postalAddress:"
													+ postalAddress);
										}
										if (child2OME.getLocalName().equals(
												"APPLYTYPE")
												&& child2OME.getText() != null) {
											applyType = child2OME.getText();
											System.out.println("applyType:"
													+ applyType);
										}
									}
								}
								if (applyType.equals("01")) {
									appntMap.put("NAME", name);
									appntMap.put("ZipCode", zipcode);
									appntMap.put("Phone", phone);
									appntMap
											.put("PostalAddress", postalAddress);
									appntMap.put("Mobile", mobile);
									appntMap.put("EMAIL", email);
								}
							}
						}
						if (child_element.getLocalName().equals("INSULIST")) {
							Iterator child1 = child_element.getChildren();

							while (child1.hasNext()) {
								/* 被保人信息 */

								String name = null; // 姓名

								String email = null; // 邮箱地址

								String mobile = null; // 手机号

								String phone = null; // 电话

								String zipcode = null; // 邮编

								String postalAddress = null; // 通讯地址

								String applyType = null;// 00 - 不变更 01 - 变更

								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										if (child2OME.getLocalName().equals(
												"NAME")
												&& child2OME.getText() != null) {
											name = child2OME.getText();
											System.out.println("name:" + name);
										}
										if (child2OME.getLocalName().equals(
												"EMAIL")
												&& child2OME.getText() != null) {
											email = child2OME.getText();
											System.out
													.println("email:" + email);
										}
										if (child2OME.getLocalName().equals(
												"MOBILE")
												&& child2OME.getText() != null) {
											mobile = child2OME.getText();
											System.out.println("mobile:"
													+ mobile);
										}
										if (child2OME.getLocalName().equals(
												"PHONE")
												&& child2OME.getText() != null) {
											phone = child2OME.getText();
											System.out
													.println("phone:" + phone);
										}
										if (child2OME.getLocalName().equals(
												"ZIPCODE")
												&& child2OME.getText() != null) {
											zipcode = child2OME.getText();
											System.out.println("zipcode:"
													+ zipcode);
										}
										if (child2OME.getLocalName().equals(
												"POSTALADDRESS")
												&& child2OME.getText() != null) {
											postalAddress = child2OME.getText();
											System.out.println("postalAddress:"
													+ postalAddress);
										}
										if (child2OME.getLocalName().equals(
												"APPLYTYPE")
												&& child2OME.getText() != null) {
											applyType = child2OME.getText();
											System.out.println("applyType:"
													+ applyType);
										}
									}
								}
								if (applyType.equals("01")) {
									HashMap tMap = new HashMap();
									tMap.put("NAME", name);
									tMap.put("ZipCode", zipcode);
									tMap.put("Phone", phone);
									tMap.put("PostalAddress", postalAddress);
									tMap.put("Mobile", mobile);
									tMap.put("EMAIL", email);
									insuList.add(tMap);
								}
							}
						}
					}
				}
				ExeSQL exeSql = new ExeSQL();
				String getType = "select 1 from lccont where contno = '"
						+ this.cardno + "' with ur";
				String c = exeSql.getOneValue(getType);
				if (insuList != null) {
					for (int i = 0; i < insuList.size(); i++) {
						HashMap insuMap = (HashMap) insuList.get(i);
						if(c.equals("1")){
							mECContEndor.setContNo(this.cardno);
							boolean flag = mECContEndor.doInsuredEndor(insuMap);
							String name = (String) insuMap.get("NAME");
							if (!flag) {
								responseOME = buildResponseOME("01", "1", "被保人" + name
										+ "联系方式修改失败！");
								System.out.println("responseOME:" + responseOME);
								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										sendOperator, "1", "被保人" + name + "联系方式修改失败！");
								return responseOME;

							}
						}
						getLIEndorData(("02"), insuMap);
					}
				}
				if (appntMap != null) {
					if (appntMap.size() > 0) {
						getLIEndorData(("01"), appntMap);
					}
				}
				mLIVData.add(mMMap);
				if(mLIVData.size()>0){
					PubSubmit tPubSubmit = new PubSubmit();
					if (!tPubSubmit.submitData(mLIVData, "INSERT")) {
						// @@错误处理
						this.mErrors.copyAllErrors(tPubSubmit.mErrors);
						CError tError = new CError();
						tError.moduleName = "CardActiveInfo";
						tError.functionName = "submitData";
						tError.errorMessage = "数据提交失败!";
						this.mErrors.addOneError(tError);
						responseOME = buildResponseOME("01", "6", "数据提交失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno, messageType,
								sendDate, sendTime, "01", sendOperator, "6",
								"数据提交失败");
						return responseOME;
					}
				}
				
				responseOME = buildResponseOME("00", "0", "成功");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "00",
						sendOperator, "0", "成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", sendOperator, "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("ECCARDENDOR", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}
	/**
	 * 封装中间表变更数据
	 * 
	 * @param type
	 * @param data
	 * @return
	 */
	private void getLIEndorData(String type, HashMap data) {
		String name = (String) data.get("NAME");
		String ZipCode = (String) data.get("ZipCode");
		String Phone = (String) data.get("Phone");
		String PostalAddress = (String) data.get("PostalAddress");
		String Mobile = (String) data.get("Mobile");
		String EMAIL = (String) data.get("EMAIL");

		if (type.equals("01")) {
			WFAppntListDB tWFAppntListDB = new WFAppntListDB();
			tWFAppntListDB.setCardNo(this.cardno);
			tWFAppntListDB.setName(name);
			WFAppntListSchema tWFAppntListSchema = new WFAppntListSchema();
			WFAppntListSet tWFAppntListSet = tWFAppntListDB.query();
			tWFAppntListSchema = tWFAppntListSet.get(1);
			tWFAppntListSchema.setZipCode(ZipCode);
			tWFAppntListSchema.setPhont(Phone);
			tWFAppntListSchema.setPostalAddress(PostalAddress);
			tWFAppntListSchema.setMobile(Mobile);
			tWFAppntListSchema.setEmail(EMAIL);
			mMMap.put(tWFAppntListSchema, "UPDATE");
		} else {
			// ------ 被保人表 -------------
			WFInsuListDB tWFInsuListDB = new WFInsuListDB();
			tWFInsuListDB.setCardNo(this.cardno);
			tWFInsuListDB.setName(name);
			WFInsuListSchema tWFInsuListSchema = new WFInsuListSchema();
			WFInsuListSet tWFInsuListSet = tWFInsuListDB.query();
			tWFInsuListSchema = tWFInsuListSet.get(1);
			tWFInsuListSchema.setZipCode(ZipCode);
			tWFInsuListSchema.setPhont(Phone);
			tWFInsuListSchema.setPostalAddress(PostalAddress);
			tWFInsuListSchema.setMobile(Mobile);
			tWFInsuListSchema.setEmail(EMAIL);
			mMMap.put(tWFInsuListSchema, "UPDATE");

			// ------ 激活中间表 -------------
			LICardActiveInfoListDB tLICardActiveInfoListDB = new LICardActiveInfoListDB();
			tLICardActiveInfoListDB.setCardNo(this.cardno);
			tLICardActiveInfoListDB.setName(name);
			tLICardActiveInfoListDB.setCardStatus("01");// 有效
			LICardActiveInfoListSchema tLICardActiveInfoListSchema = new LICardActiveInfoListSchema();
			LICardActiveInfoListSet tLICardActiveInfoListSet = tLICardActiveInfoListDB.query();
			tLICardActiveInfoListSchema = tLICardActiveInfoListSet.get(1);
			tLICardActiveInfoListSchema.setZipCode(ZipCode);
			tLICardActiveInfoListSchema.setPhone(Phone);
			tLICardActiveInfoListSchema.setPostalAddress(PostalAddress);
			tLICardActiveInfoListSchema.setMobile(Mobile);
			tLICardActiveInfoListSchema.setEMail(EMAIL);
			tLICardActiveInfoListSchema.setModifyDate(mCurrentDate);
			tLICardActiveInfoListSchema.setModifyTime(mCurrentTime);
			mMMap.put(tLICardActiveInfoListSchema, "UPDATE");

			// ---------- 提数激活表 --------------
			String liactiveUpdateSql = "update liactivecarddetail set "
					+ " postaladdress = '" + PostalAddress + "', "
					+ " zipcode = '" + ZipCode + "', " + " phone = '" + Phone
					+ "', " + " mobile = '" + Mobile + "' " + " where 1=1 "
					+ " and cardno = '" + this.cardno + "' "
					+ " and insuredname ='" + name + "' with ur";
			mMMap.put(liactiveUpdateSql, "UPDATE");
		}

	}

	public static void main(String[] args) {

	}

}
