package com.ecwebservice.subinterface;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;


import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;

import com.sinosoft.lis.pubfun.PubSubmit;

import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * Title:复杂网销订单删除（调用核心新单删除）
 * 
 * @author zhangyige 创建时间：2012-11-7 
 * @version
 */

public class DeleteCont {

    /**
     * @param args
     */
    // 保险卡信息
    private String batchno = null;// 批次号

    private String sendDate = null;// 报文发送日期

    private String sendTime = null;// 报文发送时间

    private String sendOperator = "EC_WX";// g

    private String messageType = null;// 报文类型

    MMap map = new MMap();

    VData tVData = new VData();

    // 错误处理类
    public CErrors mErrors = new CErrors();

    // 印刷号
    private String printNo = null;

    public GlobalInput mGlobalInput = new GlobalInput();//公共信息

    public OMElement queryData(OMElement Oms) {

        OMElement responseOME = null;
        boolean fieldCompleteFlag = true;// 非空字段完整性标志
        String emptyField = null;// 空值字段
        try {
            LoginVerifyTool mTool = new LoginVerifyTool();

            Iterator iter1 = Oms.getChildren();
            while (iter1.hasNext()) {
                OMElement Om = (OMElement) iter1.next();

                if (Om.getLocalName().equals("DELETECONT")) {

                    Iterator child = Om.getChildren();

                    while (child.hasNext() && fieldCompleteFlag) {
                        OMElement child_element = (OMElement) child.next();

                        if (child_element.getLocalName().equals("BATCHNO")
                                && child_element.getText() != "") {
                            batchno = child_element.getText();
                            System.out.println("BATCHNO" + batchno);
                            boolean validBatchNo = mTool
                                    .isValidBatchNo(batchno);
                            if (!validBatchNo) {
                                // 批次号不唯一
                                responseOME = buildResponseOME("01", "21", "批次号不唯一!");
                                System.out
                                        .println("responseOME:" + responseOME);

                                LoginVerifyTool.writeLog(responseOME, batchno,
                                        messageType, sendDate, sendTime, "01",
                                        "100", "21", "批次号不唯一!");
                                return responseOME;
                            }
                        }
                        if (child_element.getLocalName().equals("BATCHNO")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "BATCHNO";
                            System.out.println("执行else BATCHNO");
                            break;
                        }
                        if (child_element.getLocalName().equals("SENDDATE")
                                && child_element.getText() != "") {
                            sendDate = child_element.getText();

                            System.out.println(sendDate);
                        }
                        if (child_element.getLocalName().equals("SENDDATE")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "SENDDATE";
                            System.out.println("执行else SENDDATE");
                            break;
                        }
                        if (child_element.getLocalName().equals("SENDTIME")
                                && child_element.getText() != "") {
                            sendTime = child_element.getText();

                            System.out.println(sendTime);
                        }
                        if (child_element.getLocalName().equals("SENDTIME")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "SENDTIME";
                            break;
                        }
                        if (child_element.getLocalName().equals("PRINTNO")
                                && child_element.getText() != "") {
                            printNo = child_element.getText();

                            System.out.println(printNo);
                        }
                        if (child_element.getLocalName().equals("PRINTNO")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "PRINTNO";
                            break;
                        }
                        if (child_element.getLocalName().equals("MESSAGETYPE")
                                && child_element.getText() != "") {
                            messageType = child_element.getText();

                            System.out.println(messageType);
                            if (!"DE".equals(messageType)) {
                                responseOME = buildResponseOME("01", "5", "报文类型不正确!");
                                System.out
                                        .println("responseOME:" + responseOME);

                                LoginVerifyTool.writeLog(responseOME, batchno,
                                        messageType, sendDate, sendTime, "01",
                                        "100", "5", "报文类型不正确!");
                                return responseOME;
                            }
                        }
                        if (child_element.getLocalName().equals("MESSAGETYPE")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "MESSAGETYPE";
                            break;
                        }
                    }
                }
            }
            responseOME = checkData();
            if(responseOME!=null){
            	return responseOME;
            }
	        System.out.println("删除新单开始，Prtno为："+printNo);
			VData tVData = new VData();
			String tDeleteReason = "复杂产品订单删除需要进行新单删除";
			TransferData tTransferData = new TransferData();
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setPrtNo(printNo);
			LCContSchema tLCContSchema = new LCContSchema();
			LCContSet tLCContSet = tLCContDB.query();
			tLCContSchema = tLCContSet.get(1);
		    tTransferData.setNameAndValue("DeleteReason",tDeleteReason);
			tVData.add(tLCContSchema);
			tVData.add(tTransferData);
			tVData.add(mGlobalInput);
			ContDeleteUI tContDeleteUI = new ContDeleteUI();
			boolean flag = tContDeleteUI.submitData(tVData,"DELETE");
        	System.out.println("删除新单结束");
            if (!flag) {
    			String error = tContDeleteUI.mErrors.getFirstError();
                // @@错误处理
                
                responseOME = buildResponseOME( "01",
                        "6", error);
                System.out.println("responseOME:" + responseOME);
                LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                        sendDate, sendTime, "01", sendOperator, "6",
                        error);
                return responseOME;
            } else {
                responseOME = buildResponseOME("00", "0", "成功!");
                System.out.println("responseOME:" + responseOME);
                LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                        sendDate, sendTime, "00", sendOperator, "0", "成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
            responseOME = buildResponseOME( "01", "4",
                    "发生异常");
            System.out.println("responseOME:" + responseOME);
            LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                    sendDate, sendTime, "01", sendOperator, "4", "发生异常");
            return responseOME;
        }
        return responseOME;

    }

    private OMElement buildResponseOME( String state, String errCode, String errInfo) {
        OMFactory fac = OMAbstractFactory.getOMFactory();

        OMElement DATASET = fac.createOMElement("DELETECONT", null);
        LoginVerifyTool tool = new LoginVerifyTool();
        tool.addOm(DATASET, "BATCHNO", batchno);
        tool.addOm(DATASET, "SENDDATE", sendDate);
        tool.addOm(DATASET, "SENDTIME", sendTime);

        tool.addOm(DATASET, "STATE", state);
        tool.addOm(DATASET, "ERRCODE", errCode);
        tool.addOm(DATASET, "ERRINFO", errInfo);

        return DATASET;
    }
    private OMElement checkData(){
    	
    	mGlobalInput.ManageCom = "86";
    	mGlobalInput.Operator = "EC_WX";
    	OMElement responseOME = null;
    	ExeSQL tExeSQL = new ExeSQL();
    	String isECsql = "select cardflag from lccont where prtno = '"+printNo+"'";
    	String cardflag = tExeSQL.getOneValue(isECsql);
    	if(!cardflag.equals("b")){
    		responseOME = buildResponseOME("01", "7", "保单不是复杂网销，不能进行新单删除!");
            System.out.println("responseOME:" + responseOME);

            LoginVerifyTool.writeLog(responseOME, batchno,
                    messageType, sendDate, sendTime, "01",
                    "100", "7", "保单不是复杂网销，不能进行新单删除!");
            return responseOME;
    	}
    	String isSignsql = "select appflag from lccont where prtno = '"+printNo+"'";
    	String signflag = tExeSQL.getOneValue(isSignsql);
    	if(!signflag.equals("0")){
    		responseOME = buildResponseOME("01", "7", "保单已经签单，不能进行新单删除!");
            System.out.println("responseOME:" + responseOME);

            LoginVerifyTool.writeLog(responseOME, batchno,
                    messageType, sendDate, sendTime, "01",
                    "100", "7", "保单已经签单，不能进行新单删除!");
            return responseOME;
    	}
    	String isFeesql = "select prtno from lccont a where 1=1 and prtno = '"+printNo+"'"
    		 +" and (exists(select 1 from ljtempfee where otherno = a.contno)"
    		 +" or exists(select 1 from ljtempfee where otherno = a.prtno))";
    	String feeflag = tExeSQL.getOneValue(isFeesql);
    	if(!feeflag.equals("")){
    		responseOME = buildResponseOME("01", "7", "保单已经缴费，不能进行新单删除!");
            System.out.println("responseOME:" + responseOME);

            LoginVerifyTool.writeLog(responseOME, batchno,
                    messageType, sendDate, sendTime, "01",
                    "100", "7", "保单已经缴费，不能进行新单删除!");
            return responseOME;
    	}
    	return null;
    }

    public static void main(String[] args) {

    }

}
