package com.ecwebservice.subinterface;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/**
 * 
 * @author zhangyige 2013-08-08
 * 说明：该类是为处理北分旅游意外险出单系统请求而做
 * 主要处理四类请求：
 * 1、获取现有投保单位客户号
 * 2、没有已存在客户情况下，生成新的团单客户号
 * 3、获取代理人信息
 * 4、获取代理机构以及其下代理人信息
 *
 */
public class DealTripRequest {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	
	private String branchcode = "";// 网点
	
	private String sendoperator = "";// 操作员

	private String grpName = "";// 投保单位名称 
	
	private String businessType = "";// 行业编码 
	
	private String grpNature = "";// 企业类型编码 
	
	private String satrap = "";// 负责人姓名 
	
	private String phone = "";// 公司电话 
	
	private String agentCode = "";// 代理人编码 
	
	private String agentName = "";// 代理人名称
	
	private String agentCom = "";// 代理机构编码 
	
	private String agentComName = "";// 代理机构名称 
	
	private String messageType = "";// 报文类型 
	
	private boolean needSave = false;// 是否需要保存数据库 

	private String pageIndex = "";//起始页
	
	private String pageSize = "";//每页多少条记录
	
	private String totalCount = "";//总记录数
	
	private OMElement responseOME = null;
	
	//封装结果集
	private Set resultSet = new HashSet();
	
	
	private MMap tMMap = new MMap();
	
	private String theCurrentDate = PubFun.getCurrentDate();
	private String theCurrentTime = PubFun.getCurrentTime();
	
	private LoginVerifyTool mTool = new LoginVerifyTool();

	//投保单位信息
	private LDGrpSchema mLDGrpSchema = new LDGrpSchema();
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildResponseOME(resultSet,"01","01", "加载报文数据失败!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "加载报文数据失败!");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			return responseOME;
		}
		if(!deal()){
			return responseOME;
		}
		if(needSave){
			if(!save()){
				return responseOME;
			}
		}
		responseOME = buildResponseOME(resultSet,"00","01", "");
		
		return responseOME;
	}
	
	//加载报文信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("TRIPREQUEST")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDDATE")){
							this.sendDate=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDTIME")){
							this.sendTime= child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("MESSAGETYPE")){
							this.messageType=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("GRPNAME")){
							this.grpName=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("BUSINESSTYPE")){
							this.businessType=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("GRPNATURE")){
							this.grpNature=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SATRAP")){
							this.satrap=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("PHONE")){
							this.phone=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDOPERATOR")){
							this.sendoperator=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("AGENTCODE")){
							
							AgentCodeTransformation a = new AgentCodeTransformation();
							if(!a.AgentCode(child_element.getText(), "N")){
								System.out.println("信息"+a.getMessage());
							}
							this.agentCode=a.getResult();
							System.out.println("返回值"+a.getResult());
							continue;
						}
						if(child_element.getLocalName().equals("AGENTNAME")){
							this.agentName=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("AGENTCOM")){
							this.agentCom=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("AGENTCOMNAME")){
							this.agentComName=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("PAGEINDEX")){
							this.pageIndex=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("PAGESIZE")){
							this.pageSize=child_element.getText();
							continue;
						}
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验
	 * @return
	 */
	private boolean check(){
		if(!checkInfo()){
			System.out.println("校验基本信息失败");
			return false;
		}
		return true;
	}
	
	/**
	 * 处理
	 * @return
	 */ 
	private boolean deal(){
		try{
			if(this.messageType.equals("1")){
				 if(!queryGrpCustomerNo()){
					return false;
			     }
			}
			if(this.messageType.equals("2")){
				if(!createGrpCustomerNo()){
					return false;
				}
			}
			if(this.messageType.equals("3")){
				if(!queryAgentCode()){
		        	return false;
		        }
			}
			if(this.messageType.equals("4")){
				 if(!queryAgentCom()){
			        return false;
			     }
			}
			if(this.messageType.equals("5")){
				if(!queryAgentSales()){
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("处理旅行社接口请求异常！");
			e.printStackTrace();
			return false;
		}
									
		return true;
	}
	
	/**
	 *校验基本信息 
	 * @return
	 */
	private boolean checkInfo(){
		if(this.batchno ==null || this.batchno.equals("")){
			responseOME = buildResponseOME(resultSet,"01","02", "没有得到批次号!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到批次号!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if(this.messageType ==null || this.messageType.equals("")){
			responseOME = buildResponseOME(resultSet,"01","02", "没有得到交易类型!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到交易类型!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if(this.messageType.equals("1")){
			if(this.grpName ==null || this.grpName.equals("")){
				responseOME = buildResponseOME(resultSet,"01","02", "没有得到投保单位名称!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到投保单位名称!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(this.messageType.equals("2")){
			if(this.grpName ==null || this.grpName.equals("")){
				responseOME = buildResponseOME(resultSet,"01","02", "没有得到投保单位名称!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到投保单位名称!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(this.businessType ==null || this.businessType.equals("")){
				responseOME = buildResponseOME(resultSet,"01","02", "没有得到行业编码!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到行业编码!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(this.grpNature ==null || this.grpNature.equals("")){
				responseOME = buildResponseOME(resultSet,"01","02", "没有得到企业类型编码!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到企业类型编码!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(this.satrap ==null || this.satrap.equals("")){
				responseOME = buildResponseOME(resultSet,"01","02", "没有得到负责人姓名!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到负责人姓名!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(this.phone ==null || this.phone.equals("")){
				responseOME = buildResponseOME(resultSet,"01","02", "没有得到公司电话!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到公司电话!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(this.sendoperator ==null || this.sendoperator.equals("")){
				responseOME = buildResponseOME(resultSet,"01","02", "没有得到操作员代码!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到公司电话!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(this.messageType.equals("3")||this.messageType.equals("4")){
			if((this.agentCode ==null || this.agentCode.equals(""))&&(this.agentName ==null || this.agentName.equals(""))){
				responseOME = buildResponseOME(resultSet,"01","02", "代理人编码、代理人名称不能同时为空!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "代理人编码、代理人名称不能同时为空!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(this.messageType.equals("5")){
			if((this.agentCom ==null || this.agentCom.equals(""))){
				responseOME = buildResponseOME(resultSet,"01","02", "代理机构编码不能为空!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "代理机构编码不能为空!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		return true;
	}
	
	//提交卡激活信息
	private boolean save(){
		try{
			VData v = new VData();
			v.add(tMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				System.out.println("数据提交失败************************");
				responseOME = buildResponseOME(resultSet,"01","99", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","99", "WX", "数据提交失败!");
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME(resultSet,"01","99", "数据提交失败!");
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	private OMElement buildResponseOME(Set resultSet,String state, String errCode,String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("TRIPREQUEST", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRINFO", errInfo);
		tool.addOm(DATASET, "TOTALCOUNT", totalCount);
		OMElement CLIST = fac.createOMElement("LIST", null);
		//遍历HashSet
		Iterator resultIterator = resultSet.iterator();
		while (resultIterator.hasNext()) {
			Map resultSchemaMap = (HashMap) resultIterator.next();
			OMElement ITEM = fac.createOMElement("ITEM", null);
			if(this.messageType.equals("1")){
				tool.addOm(ITEM, "CUSTOMERNO", (String) resultSchemaMap.get("CUSTOMERNO"));
				tool.addOm(ITEM, "GRPNAME", (String) resultSchemaMap.get("GRPNAME"));
			}
			if(this.messageType.equals("2")){
				tool.addOm(ITEM, "CUSTOMERNO", (String) resultSchemaMap.get("CUSTOMERNO"));
			}
			if(this.messageType.equals("3")){
				tool.addOm(ITEM, "AGENTCODE", (String) resultSchemaMap.get("AGENTCODE"));
				tool.addOm(ITEM, "AGENTNAME", (String) resultSchemaMap.get("AGENTNAME"));
				tool.addOm(ITEM, "AGENTMANAGECOM", (String) resultSchemaMap.get("AGENTMANAGECOM"));
			}
			if(this.messageType.equals("4")){
				tool.addOm(ITEM, "AGENTCODE", (String) resultSchemaMap.get("AGENTCODE"));
				tool.addOm(ITEM, "AGENTNAME", (String) resultSchemaMap.get("AGENTNAME"));
				tool.addOm(ITEM, "AGENTCOM", (String) resultSchemaMap.get("AGENTCOM"));
				tool.addOm(ITEM, "AGENTCOMNAME", (String) resultSchemaMap.get("AGENTCOMNAME"));
				tool.addOm(ITEM, "AGENTMANAGECOM", (String) resultSchemaMap.get("AGENTMANAGECOM"));
			}
			if(this.messageType.equals("5")){
				tool.addOm(ITEM, "AGENTCODE", (String) resultSchemaMap.get("AGENTCODE"));
				tool.addOm(ITEM, "AGENTNAME", (String) resultSchemaMap.get("AGENTNAME"));
				tool.addOm(ITEM, "AGENTMANAGECOM", (String) resultSchemaMap.get("AGENTMANAGECOM"));
			}
			CLIST.addChild(ITEM);
		}
		DATASET.addChild(CLIST);


		return DATASET;
	}
	
	private boolean queryGrpCustomerNo(){
		String tSQLCount = "select count(1) from LDGrp  where GrpName like '%"+this.grpName+"%' ";
		this.totalCount = new ExeSQL().getOneValue(tSQLCount);
		String tSQL = "select customerno,grpname from LDGrp  where GrpName like '%"+this.grpName+"%' ";
        SSRS result = new ExeSQL().execSQL(tSQL, (Integer.parseInt(pageIndex)-1)*Integer.parseInt(pageSize)+1, Integer.parseInt(pageSize));
        
        int len = result.MaxRow;
		for (int i = 1; i <= len; i++) {
			Map schemaMap = new HashMap();
			schemaMap.put("CUSTOMERNO", result.GetText(i, 1));
			schemaMap.put("GRPNAME", result.GetText(i, 2));
			resultSet.add(schemaMap);
		}
		return true;
	}
	private boolean queryAgentCode(){
		String tSQLCount = "select count(1) from laagent  where 1=1 ";
		if(this.agentCode!=null&&!this.agentCode.equals("")){
			tSQLCount = tSQLCount + " and groupagentcode = '"+this.agentCode+"' ";
		}
		if(this.agentName!=null&&!this.agentName.equals("")){
			tSQLCount = tSQLCount + " and name like '%"+this.agentName+"%' ";
		}
		this.totalCount = new ExeSQL().getOneValue(tSQLCount);
		
		String tSQL = "select groupagentcode,name,managecom from laagent  where 1=1 ";
		if(this.agentCode!=null&&!this.agentCode.equals("")){
			tSQL = tSQL + " and groupagentcode = '"+this.agentCode+"' ";
		}
		if(this.agentName!=null&&!this.agentName.equals("")){
			tSQL = tSQL + " and name like '%"+this.agentName+"%' ";
		}
		SSRS result = new ExeSQL().execSQL(tSQL, (Integer.parseInt(pageIndex)-1)*Integer.parseInt(pageSize)+1, Integer.parseInt(pageSize));
		int len = result.MaxRow;
		for (int i = 1; i <= len; i++) {
			Map schemaMap = new HashMap();
			schemaMap.put("AGENTCODE", result.GetText(i, 1));
			schemaMap.put("AGENTNAME", result.GetText(i, 2));
			schemaMap.put("AGENTMANAGECOM", result.GetText(i, 3));
			resultSet.add(schemaMap);
		}
		return true;
	}
	private boolean queryAgentSales(){
		String tSQLCount = "select count(1) from laagenttemp  where 1=1 ";
		if(this.agentCode!=null&&!this.agentCode.equals("")){
			tSQLCount = tSQLCount + " and agentCode = '"+this.agentCode+"' ";
		}
		if(this.agentName!=null&&!this.agentName.equals("")){
			tSQLCount = tSQLCount + " and name like '%"+this.agentName+"%' ";
		}
		if(this.agentCom!=null&&!this.agentCom.equals("")){
			tSQLCount = tSQLCount + " and entryno = '"+this.agentCom+"' ";
		}
		this.totalCount = new ExeSQL().getOneValue(tSQLCount);
		
		String tSQL = "select agentcode,name,managecom from laagenttemp  where 1=1 ";
		if(this.agentCode!=null&&!this.agentCode.equals("")){
			tSQL = tSQL + " and agentCode = '"+this.agentCode+"' ";
		}
		if(this.agentName!=null&&!this.agentName.equals("")){
			tSQL = tSQL + " and name like '%"+this.agentName+"%' ";
		}
		if(this.agentCom!=null&&!this.agentCom.equals("")){
			tSQL = tSQL + " and entryno = '"+this.agentCom+"' ";
		}
		SSRS result = new ExeSQL().execSQL(tSQL, (Integer.parseInt(pageIndex)-1)*Integer.parseInt(pageSize)+1, Integer.parseInt(pageSize));
		int len = result.MaxRow;
		for (int i = 1; i <= len; i++) {
			Map schemaMap = new HashMap();
			schemaMap.put("AGENTCODE", result.GetText(i, 1));
			schemaMap.put("AGENTNAME", result.GetText(i, 2));
			schemaMap.put("AGENTMANAGECOM", result.GetText(i, 3));
			resultSet.add(schemaMap);
		}
		return true;
	}
	private boolean queryAgentCom(){
		String tSQLCount = "select count(1) from lacomtoagent a "
			+" inner join laagent b on a.agentcode = b.agentcode where 1=1 ";
		if(this.agentCode!=null&&!this.agentCode.equals("")){
			tSQLCount = tSQLCount + " and b.groupagentcode = '"+this.agentCode+"' ";
		}
		if(this.agentName!=null&&!this.agentName.equals("")){
			tSQLCount = tSQLCount + " and b.name like '%"+this.agentName+"%' ";
		}
		this.totalCount = new ExeSQL().getOneValue(tSQLCount);
		
		String tSQL = "select b.groupagentcode,b.name, agentcom,(select name from lacom where agentcom = a.agentcom ),b.managecom from lacomtoagent a "
			+" inner join laagent b on a.agentcode = b.agentcode where 1=1 ";
		if(this.agentCode!=null&&!this.agentCode.equals("")){
			tSQL = tSQL + " and b.groupagentcode = '"+this.agentCode+"' ";
		}
		if(this.agentName!=null&&!this.agentName.equals("")){
			tSQL = tSQL + " and b.name like '%"+this.agentName+"%' ";
		}
		SSRS result = new ExeSQL().execSQL(tSQL, (Integer.parseInt(pageIndex)-1)*Integer.parseInt(pageSize)+1, Integer.parseInt(pageSize));
		int len = result.MaxRow;
		for (int i = 1; i <= len; i++) {
			Map schemaMap = new HashMap();
			schemaMap.put("AGENTCODE", result.GetText(i, 1));
			schemaMap.put("AGENTNAME", result.GetText(i, 2));
			schemaMap.put("AGENTCOM", result.GetText(i, 3));
			schemaMap.put("AGENTCOMNAME", result.GetText(i, 4));
			schemaMap.put("AGENTMANAGECOM", result.GetText(i, 5));
			resultSet.add(schemaMap);
		}
		return true;
	}
	private boolean createGrpCustomerNo(){
		this.needSave = true;
		String newCustomerNo = PubFun1.CreateMaxNo("GRPNO", "SN");
		if("".equals(newCustomerNo) || newCustomerNo == null){
        	responseOME = buildResponseOME(resultSet,"01","11", "生成客户号失败!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","11", "WX", "生成客户号失败!");
			System.out.println("responseOME:" + responseOME);
			return false;
        }
		Map schemaMap = new HashMap();
		schemaMap.put("CUSTOMERNO", newCustomerNo);
		resultSet.add(schemaMap);
		this.mLDGrpSchema.setCustomerNo(newCustomerNo);
		this.mLDGrpSchema.setGrpName(this.grpName);
		this.mLDGrpSchema.setPhone(this.phone);
		this.mLDGrpSchema.setOrganComCode("");
		this.mLDGrpSchema.setBusinessType(this.businessType);
		this.mLDGrpSchema.setGrpNature(this.grpNature);
		this.mLDGrpSchema.setPeoples("");
		this.mLDGrpSchema.setOnWorkPeoples("");
		this.mLDGrpSchema.setOffWorkPeoples("");
		this.mLDGrpSchema.setOtherPeoples("");
		mLDGrpSchema.setMakeDate(theCurrentDate);
		mLDGrpSchema.setMakeTime(theCurrentTime);
		mLDGrpSchema.setOperator(this.sendoperator);
		mLDGrpSchema.setModifyDate(theCurrentDate);
		mLDGrpSchema.setModifyTime(theCurrentTime);
		tMMap.put(mLDGrpSchema, "INSERT");

		return true;
	}
	
}
