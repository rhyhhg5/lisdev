package com.ecwebservice.subinterface;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
/**
 * 北分旅行社出单，批处理获取已结算保单   已结算保单即已收费保单
 * This class is getting the policy which has settled through batching.
 * 'Settled' means the policy has been payed.
 * @author dhc
 *
 */
public class DealQueryGrpSt {
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private OMElement mOME = null;
	
	private OMElement responseOME = null;
	
	private List prtnos = new ArrayList();
	
	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	/**
	 * 
	 * @return 
	 */
	public OMElement queryData(OMElement Oms){
		this.mOME = Oms.cloneOMElement();
		
		//解析报文  parsing the message
		if(!parseData()){
			responseOME = buildResponseOME("false", "加载团单数据失败!");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		
		responseOME = deal();
		return responseOME;
	}
	public boolean parseData(){
		try {
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement)iter1.next();
				if(Om.getLocalName().equals("QUERYGRPST")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement childElement = (OMElement)child.next();
						if(childElement.getLocalName().equals("PRTNO")){
							prtnos.add(childElement.getText());
						}
						if(childElement.getLocalName().equals("SENDDATE")){
							this.sendDate=childElement.getText();
							continue;
						}
						if(childElement.getLocalName().equals("SENDTIME")){
							this.sendTime= childElement.getText();
							continue;
						}
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
				
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * 处理 --查询是否结算  Query the policy has settled or not according prtno
	 * 步骤：1.根据prtno查询lcgrpcont表中的appflag，若appflag为1，则已经结算
	 * 2.若lcgrpcont中的appflag不为1，根据otherno='印刷号'查询ljtempfee的enteraccdate字段,若enteraccdate is not null则已收费
	 * @return
	 * Procedure:
	 * Firstly,According to the prtno to query the appflag from table lcgrpcont,if the appflag is 1,it vertifies the policy has settled
	 * Secondly,if the appflag is not 1 in the last step,we should query the field of enteraccdate from table ljtempfee with the condition 'where otherno = [prtno]'.
	 * 			If the result of enteraccdate is not null,it means the policy hasn't settled.
	 */
	private OMElement deal(){
		ExeSQL tExeSQL = new ExeSQL();
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement("QUERYGRPST", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "ERRINFO", "success");
		int length = prtnos.size();
		for(int i=0;i<length;i++){
			OMElement item = fac.createOMElement("ITEM", null);
			String prtno = (String)prtnos.get(i);
			String flag = "false";
			String sql = "select appflag from lcgrpcont where prtno = '"+prtno+"' with ur";
			String appflag = tExeSQL.getOneValue(sql);
			if(appflag==null||("0").equals(appflag)){
				//在ljtempfee中根据otherno='[prtno]' 查询enteraccdate -- Query the field of enteraccdate from table ljtempfee with the conditon 'where otherno = prtno'
				String tempfeesql = "select enteraccdate from ljtempfee where otherno = '"+prtno+"' with ur";
				String enteraccdate = tExeSQL.getOneValue(tempfeesql);
				if(enteraccdate!=null&&!("").equals(enteraccdate)){//已结算    settled
					flag = "true";
				}
			}
			if(appflag!=null&&("1").equals(appflag)){//结算  settled
				flag = "true";
			}
			tool.addOm(item, "PRTNO", prtno);
			tool.addOm(item, "FLAG", flag);
			DATASET.addChild(item);
		}
		return DATASET;
	}
	
	private OMElement buildResponseOME(String flag, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement("QUERYGRPST", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "ERRINFO", errInfo);
		return DATASET;
	}
	
	
}
