package com.ecwebservice.subinterface;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.rpc.client.RPCServiceClient;

import com.cbsws.obj.RspSaleInfo;
import com.ecwebservice.ctrip.form.PraseXmlUtil;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.midplat.channel.util.IOTrans;

public class DealQueryAgentInfoBL {
	
	private RspSaleInfo mRspSaleInfo =  new RspSaleInfo();
	
	public RspSaleInfo queryAgentInfo(String uni_sales_cod,String managecom){
		//根据同意工号调用webservice查询业务员信息
	      try
	        {
	    	  								 
	           String tStrTargetEendPoint = "http://10.132.97.207:8080/unifyno/services/ReqUniSalescode?wsdl";
	            String tStrNamespace = "http://10.132.97.207:8080/unifyno/services/ReqUniSalescode?wsdl";

	            /*String tStrTargetEendPoint = "http://10.132.27.102:8080/Unity/ReqSaleInfo.jws";
	            String tStrNamespace = "http://10.132.27.102:8080/Unity/ReqSaleInfo.jws";*/
	            RPCServiceClient client = new RPCServiceClient();
	            String mInXmlStr = createPayLoad(uni_sales_cod,managecom);
	            System.out.println(mInXmlStr);
	            EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
	            Options option = client.getOptions();
	            option.setTo(erf);
	            option.setTimeOutInMilliSeconds(100000L);
	            option.setAction("getSalesInfo");
	            QName name = new QName(tStrNamespace, "getSalesInfo");
	            Object[] object = new Object[] { mInXmlStr };
	            Class[] returnTypes = new Class[] { String.class };

	            Object[] response = client.invokeBlocking(name, object, returnTypes);
	            String p = (String) response[0];

	            System.out.println("UI return:" + p);
	            mRspSaleInfo = (RspSaleInfo)PraseXmlUtil.praseXml("//RspSaleInfo", p, new RspSaleInfo());
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
		return mRspSaleInfo;
	}
	


	
    private String createPayLoad(String uni_sales_cod,String manageCom){   
    	String SID = "";
    	SID=createMaxNo(manageCom);
    	String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    	+"<REQSALEINFO><HEAD>"
    	+"<APPID>H00001</APPID>"
    	+"<SID>"+SID+"</SID>"
    	+"<TIMESTAMP>"+PubFun.getCurrentDate2()+" "+PubFun.getCurrentTime()+"</TIMESTAMP>"
    	+"<MESSAGETYPE>01</MESSAGETYPE>"
    	+"</HEAD><BODY>"
    	+"<uni_sales_cod>"+uni_sales_cod+"</uni_sales_cod></BODY></REQSALEINFO>";
        return xmlStr;   
    }  
    
    private String createMaxNo(String manageCom){
      	String SID = "";  //序列号
      		String procode = manageCom.substring(2, 4);
      		if("91".equals(procode)){
      			procode="98";
      		}else if("92".equals(procode)){
      			procode="95";
      		}else if("93".equals(procode)){
      			procode="97";
      		}else if("94".equals(procode)){
      			procode="96";
      		}else if("95".equals(procode)){
      			procode="99";
      		}else if("86330100".equals(manageCom)||"86330101".equals(manageCom)||"863301".equals(manageCom)){
      			procode="94";
      		}else if("86440100".equals(manageCom)||"864401".equals(manageCom)){
      			procode="93";
      		}
      		SID = "2" + procode + "0001" +PubFun.getCurrentDate2() + PubFun1.CreateMaxNo("SIDCode", 7);
      	return SID;
    }
    
    public static void main(String[] args) {
    	DealQueryAgentInfoBL tDealQueryAgentInfoBL = new DealQueryAgentInfoBL();
		System.out.println(tDealQueryAgentInfoBL.queryAgentInfo("1232100153","8611").getUNI_SALES_COD());
	}

}
