package com.ecwebservice.subinterface;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;


import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.LCContTable;
import com.cbsws.xml.ctrl.blogic.GosportPrintBL;
import com.cbsws.xml.ctrl.blogic.LvmamaPrintBL;
import com.cbsws.xml.ctrl.complexpds.WxContRelaInfoBL;
import com.ecwebservice.epicc.GosportEmailAndMessageBL;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;

import com.sinosoft.lis.pubfun.PubSubmit;

import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * Title:远程出单
 * 
 * @author yuanzw 创建时间：2010-2-3 下午03:37:28 Description:
 * @version
 */

public class YKTCont {

    /**
     * @param args
     */
    // 保险卡信息
    private String batchno = null;// 批次号

    private String sendDate = null;// 报文发送日期

    private String sendTime = null;// 报文发送时间

    private String branchCode = null;

    private String sendOperator = null;// g

    private String messageType = null;// 报文类型

    private String uAppntNo;// 投保人编号

    MMap map = new MMap();

    VData tVData = new VData();

    // 错误处理类
    public CErrors mErrors = new CErrors();

    // 保险卡信息
    private String cardNo = null;

    String password = null;

    String cardDealType = null;
    
    private String mRiskWrapCode = null;
    private String mMult = null;
    private String mCValidate = null;

    public OMElement queryData(OMElement Oms) {

        OMElement responseOME = null;
        boolean fieldCompleteFlag = true;// 非空字段完整性标志
        String emptyField = null;// 空值字段
        try {
            LoginVerifyTool mTool = new LoginVerifyTool();

            uAppntNo = mTool.getAppntNo();
            // 保险卡信息
            WFContListSchema contListSchema = new WFContListSchema();
            WFAppntListSchema appntListSchema = null;
            WFInsuListSet insuListSet = new WFInsuListSet();
            WFInsuListSchema insuListSchema = null;
            WFBnfListSet bnfListSet = new WFBnfListSet();
            String relationCode = null;
            String relationToInsured;
            String reInsuNo;

            Iterator iter1 = Oms.getChildren();
            while (iter1.hasNext()) {
                OMElement Om = (OMElement) iter1.next();

                if (Om.getLocalName().equals("YKTCONT")) {

                    Iterator child = Om.getChildren();

                    while (child.hasNext() && fieldCompleteFlag) {
                        OMElement child_element = (OMElement) child.next();

                        if (child_element.getLocalName().equals("BATCHNO")
                                && child_element.getText() != "") {
                            batchno = child_element.getText();
                            System.out.println("BATCHNO" + batchno);
                            boolean validBatchNo = mTool
                                    .isValidBatchNo(batchno);
                            if (!validBatchNo) {
                                // 批次号不唯一
                                responseOME = buildResponseOME(null, null,
                                        null, null, "01", "21", "批次号不唯一!");
                                System.out
                                        .println("responseOME:" + responseOME);

                                LoginVerifyTool.writeLog(responseOME, batchno,
                                        messageType, sendDate, sendTime, "01",
                                        "100", "21", "批次号不唯一!");
                                return responseOME;
                            }
                        }
                        if (child_element.getLocalName().equals("BATCHNO")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "BATCHNO";
                            System.out.println("执行else BATCHNO");
                            break;
                        }
                        if (child_element.getLocalName().equals("SENDDATE")
                                && child_element.getText() != "") {
                            sendDate = child_element.getText();

                            System.out.println(sendDate);
                        }
                        if (child_element.getLocalName().equals("SENDDATE")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "SENDDATE";
                            System.out.println("执行else SENDDATE");
                            break;
                        }
                        if (child_element.getLocalName().equals("SENDTIME")
                                && child_element.getText() != "") {
                            sendTime = child_element.getText();

                            System.out.println(sendTime);
                        }
                        if (child_element.getLocalName().equals("SENDTIME")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "SENDTIME";
                            break;
                        }
                        if (child_element.getLocalName().equals("BRANCHCODE")
                                && child_element.getText() != "") {
                            branchCode = child_element.getText();

                            System.out.println(branchCode);
                        }
                        if (child_element.getLocalName().equals("BRANCHCODE")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "BRANCHCODE";
                            break;
                        }
                        if (child_element.getLocalName().equals("SENDOPERATOR")
                                && child_element.getText() != "") {
                            sendOperator = child_element.getText();

                        }
                        if (child_element.getLocalName().equals("SENDOPERATOR")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "SENDOPERATOR";
                            break;
                        }
                        if (child_element.getLocalName().equals("MESSAGETYPE")
                                && child_element.getText() != "") {
                            messageType = child_element.getText();

                            System.out.println(messageType);
                            if (!"01".equals(messageType)) {
                                responseOME = buildResponseOME(null, null,
                                        null, null, "01", "5", "报文类型不正确!");
                                System.out
                                        .println("responseOME:" + responseOME);

                                LoginVerifyTool.writeLog(responseOME, batchno,
                                        messageType, sendDate, sendTime, "01",
                                        "100", "5", "报文类型不正确!");
                                return responseOME;
                            }
                        }
                        if (child_element.getLocalName().equals("MESSAGETYPE")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "MESSAGETYPE";
                            break;
                        }

                        // 保险卡信息封装

                        contListSchema.setBatchNo(batchno);

                        contListSchema.setSendDate(PubFun.getCurrentDate());
                        contListSchema.setSendTime(PubFun.getCurrentTime());
                        contListSchema.setBranchCode(branchCode);

                        contListSchema.setSendOperator("201");
                        contListSchema.setMessageType(messageType);
                        contListSchema.setAppntNo("1");
                        // 不为空假数据
                        // contListSchema.setCertifyCode("2");

                        if (child_element.getLocalName().equals("CONTLIST")) {
                            String coreRiskCode = null;
                            String certifyCode = null;
                            String tcertifyCode = null;
                            String strCValidate = null;
                            String strActiveDate = null;

                            Iterator child1 = child_element.getChildren();
                            while (child1.hasNext()) {
                                OMElement child1OME = (OMElement) child1.next();
                                if (child1OME.getLocalName().equals("ITEM")) {
                                    Iterator child2 = child1OME.getChildren();
                                    while (child2.hasNext()
                                            && fieldCompleteFlag) {
                                        OMElement child2OME = (OMElement) child2
                                                .next();
                                        if (child2OME.getLocalName().equals(
                                                "CARDNO")) {
                                            contListSchema.setCardNo(child2OME
                                                    .getText());
                                            cardNo = child2OME.getText();
                                            // 卡单号唯一性校验
                                            boolean cardNoFlag = mTool
                                                    .isValidcardNo(cardNo);
                                            if (!cardNoFlag) {
                                                // 卡单号不唯一
                                                responseOME = buildResponseOME(
                                                        null, null, null, null,
                                                        "01", "23",
                                                        "卡单号已使用，无法提交！");
                                                System.out
                                                        .println("responseOME:"
                                                                + responseOME);
                                                LoginVerifyTool.writeLog(
                                                        responseOME, batchno,
                                                        messageType, sendDate,
                                                        sendTime, "01",
                                                        sendOperator, "23",
                                                        "卡单号已使用，无法提交！");
                                                return responseOME;
                                            }

                                            // ****单证号 有效性校验 1 单证号不在退保表中 2 state
                                            // in 10 11 13
                                            String libSql = "select count(*) from libcertify where cardNo='"
                                                    + cardNo + "'";
                                            ExeSQL tExeSQL = new ExeSQL();
                                            int libCount = Integer
                                                    .parseInt(tExeSQL
                                                            .getOneValue(libSql));
                                            if (libCount > 0) {
                                                responseOME = buildResponseOME(
                                                        null, null, null, null,
                                                        "01", "3",
                                                        "单证号在退保表中 无效");
                                                System.out
                                                        .println("responseOME:"
                                                                + responseOME);
                                                LoginVerifyTool.writeLog(
                                                        responseOME, batchno,
                                                        messageType, sendDate,
                                                        sendTime, "01", "100",
                                                        "3", "单证号在退保表中 无效");
                                                return responseOME;
                                            }

                                            String stateSql = "select  lmcr.riskcode,lmcr.certifycode from lmcardrisk lmcr,"
                                                    + "(select lzcn.CardNo, lzc.CertifyCode, lzc.State from LZCardNumber lzcn "
                                                    + "inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
                                                    + " and lzc.EndNo = lzcn.CardSerNo where 1 = 1 and lzc.State in ('10','11') "
                                                    + "and lzcn.CardNo='"
                                                    + cardNo
                                                    + "' ) re where lmcr.risktype ='W' and lmcr.certifycode=re.certifycode";
                                            SSRS tSSRS = new SSRS();
                                            tSSRS = new ExeSQL().execSQL(stateSql);
                                            if(tSSRS==null || tSSRS.getMaxRow()<=0){

                                                responseOME = buildResponseOME(
                                                        null, null, null, null,
                                                        "01", "23", "单证号码无效！");
                                                System.out
                                                        .println("responseOME:"
                                                                + responseOME);
                                                LoginVerifyTool.writeLog(
                                                        responseOME, batchno,
                                                        messageType, sendDate,
                                                        sendTime, "01",
                                                        sendOperator, "23",
                                                        "单证号码无效！");
                                                return responseOME;
                                            
                                            }
                                            coreRiskCode = tSSRS.GetText(1, 1);
                                            mRiskWrapCode = tSSRS.GetText(1, 1);
                                            tcertifyCode = tSSRS.GetText(1, 2);
                                            // ****单证号state not in 10 11 13
                                            // 单证号码无效
                                            if (coreRiskCode == null
                                                    || "".equals(coreRiskCode)) {
                                                responseOME = buildResponseOME(
                                                        null, null, null, null,
                                                        "01", "23", "单证号码无效！");
                                                System.out
                                                        .println("responseOME:"
                                                                + responseOME);
                                                LoginVerifyTool.writeLog(
                                                        responseOME, batchno,
                                                        messageType, sendDate,
                                                        sendTime, "01",
                                                        sendOperator, "23",
                                                        "单证号码无效！");
                                                return responseOME;
                                            }

                                        }

                                        if (child2OME.getLocalName().equals(
                                                "CERTIFYCODE")
                                                && child2OME.getText() != "") {
                                            // 获取电子商务发送的套餐编码

                                            certifyCode = child2OME.getText();
                                            contListSchema
                                                    .setRiskCode(child2OME
                                                            .getText());
                                            // 同时设置certifyCode
                                            contListSchema
                                                    .setCertifyCode(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "CERTIFYCODE")
                                                && child2OME.getText() == "") {
                                            fieldCompleteFlag = false;
                                            // 记录为空的字段
                                            emptyField = "CERTIFYCODE";
                                            break;
                                        }
                                        if (child2OME.getLocalName().equals(
                                                "CERTIFYNAME")) {
                                            contListSchema
                                                    .setCertifyName(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "ACTIVEDATE")
                                                && child2OME.getText() != "") {
                                            strActiveDate = child2OME
                                            .getText();
                                            contListSchema
                                                    .setActiveDate(child2OME
                                                            .getText());

                                            System.out.println("激活日期"
                                                    + child2OME.getText());
                                        }
                                        if (child2OME.getLocalName().equals(
                                                "ACTIVEDATE")
                                                && child2OME.getText() == "") {
                                            fieldCompleteFlag = false;
                                            // 记录为空的字段
                                            emptyField = "ACTIVEDATE";
                                            break;
                                        }
                                        if (child2OME.getLocalName().equals(
                                                "MANAGECOM")) {
                                            contListSchema
                                                    .setManageCom(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "SALECHNL")) {
                                            contListSchema
                                                    .setSaleChnl(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "AGENTCOM")) {
                                            contListSchema
                                                    .setAgentCom(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                        "AGENTCODE")) {
		                                	AgentCodeTransformation a = new AgentCodeTransformation();
		                    				if(!a.AgentCode(child2OME
		                                            .getText(), "N")){
		                    					System.out.println(a.getMessage());
		                    				}
		                                    contListSchema
		                                            .setAgentCode(a.getResult());
		                                    
		                                }
                                        if (child2OME.getLocalName().equals(
                                                "AGENTNAME")) {
                                            contListSchema
                                                    .setAgentName(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "PAYMODE")) {
                                            contListSchema.setPayMode(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "PAYINTV")) {
                                            contListSchema.setPayIntv(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "PAYYEAR")) {
                                            contListSchema.setPayYear(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "PAYYEARFLAG")) {
                                            contListSchema
                                                    .setPayYearFlag(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "CVALIDATE")
                                                && child2OME.getText() != "") {
                                            strCValidate = child2OME
                                            .getText();
//                                          contListSchema
//                                                  .setCvaliDate(child2OME
//                                                          .getText());
                                            mCValidate = child2OME.getText();

                                        }
                                        //添加生效时间
                                        if (child2OME.getLocalName().equals(
                                        "CVALIDATETIME")
                                        && child2OME.getText() != "") {
                                    contListSchema.setCvaliDateTime(child2OME
                                                    .getText());

                                }
                                        if (child2OME.getLocalName().equals(
                                                "CVALIDATE")
                                                && child2OME.getText() == "") {
                                            fieldCompleteFlag = false;
                                            // 记录为空的字段
                                            emptyField = "CVALIDATE";
                                            break;
                                        }
                                        if (child2OME.getLocalName().equals(
                                                "CVALIDATETIME")) {
                                            contListSchema
                                                    .setCvaliDateTime(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "INSUYEAR")) {
                                            contListSchema
                                                    .setInsuYear(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "INSUYEARFLAG")) {
                                            contListSchema
                                                    .setInsuYearFlag(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "PREM")) {
                                            contListSchema.setPrem(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "AMNT")) {
                                            contListSchema.setAmnt(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "MULT")) {
                                            contListSchema.setMult(child2OME
                                                    .getText());
                                            mMult = child2OME.getText();

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "COPYS")) {
                                            contListSchema.setCopys(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "REMARK")) {
                                            contListSchema.setRemark(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "RISKCODE")) {
                                            contListSchema
                                                    .setRiskCode(child2OME
                                                            .getText());

                                        }

                                    }
                                }

                            }
                            if(tcertifyCode!=null&&!tcertifyCode.equals("")){
                            	contListSchema.setCertifyCode(tcertifyCode);
                            }
                            //生效日期校验
//                            String realCvaliDate = mTool.calCValidate(strCValidate, strActiveDate);
                            
                            contListSchema.setCvaliDate(strCValidate);
                            
                            
                            // 保险卡信息处理方式
                            if ("".equals(password) || password == null) {
                                cardDealType = "02";
                            } else {
                                cardDealType = "01";
                            }
                            contListSchema.setCardDealType(cardDealType);
                            // 校验套餐编码
                            if (!coreRiskCode.equals(certifyCode)) {
                                responseOME = buildResponseOME(null, null,
                                        null, null, "01", "3", "单证号码无效");
                                System.out
                                        .println("responseOME 套餐编码无效:" + responseOME);
                                LoginVerifyTool.writeLog(responseOME, batchno,
                                        messageType, sendDate, sendTime, "01",
                                        "100", "3", "套餐编码无效");
                                return responseOME;
                            }

                            map.put(contListSchema, "INSERT");

                        }
                        // 投保人信息

                        if (child_element.getLocalName().equals("APPNTLIST")) {

                            Iterator child1 = child_element.getChildren();

                            while (child1.hasNext()) {
                                OMElement child1OME = (OMElement) child1.next();
                                if (child1OME.getLocalName().equals("ITEM")) {
                                    appntListSchema = new WFAppntListSchema();
                                    appntListSchema.setBatchNo(batchno);
                                    appntListSchema.setCardNo(cardNo);
                                    appntListSchema.setAppntNo("1");

                                    Iterator child2 = child1OME.getChildren();
                                    while (child2.hasNext()) {
                                        OMElement child2OME = (OMElement) child2
                                                .next();
                                        if (child2OME.getLocalName().equals(
                                                "NAME")) {
                                            appntListSchema.setName(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "SEX")) {
                                            appntListSchema.setSex(mTool
                                                    .codeSex(child2OME
                                                            .getText()));

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "BIRTHDAY")) {
                                            appntListSchema
                                                    .setBirthday(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "IDTYPE")) {
                                            appntListSchema.setIDType(mTool
                                                    .codeIDType(child2OME
                                                            .getText()));

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "IDNO")) {
                                            appntListSchema.setIDNo(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "OCCUPATIONTYPE")) {
                                            appntListSchema
                                                    .setOccupationType(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "OCCUPATIONCODE")) {
                                            appntListSchema
                                                    .setOccupationCode(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "POSTALADDRESS")) {
                                            appntListSchema
                                                    .setPostalAddress(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "ZIPCODE")) {
                                            appntListSchema
                                                    .setZipCode(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "PHONT")) {
                                            appntListSchema.setPhont(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "MOBILE")) {
                                            appntListSchema.setMobile(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "EMAIL")) {
                                            appntListSchema.setEmail(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "GRPNAME")) {
                                            appntListSchema
                                                    .setGrpName(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "COMPANYPHONE")) {
                                            appntListSchema
                                                    .setCompanyPhone(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "COMPADDR")) {
                                            appntListSchema
                                                    .setCompAddr(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "COMPZIPCODE")) {
                                            appntListSchema
                                                    .setCompZipCode(child2OME
                                                            .getText());

                                        }

                                    }
                                    map.put(appntListSchema, "INSERT");
                                }

                            }

                        }

                        // 被保人信息

                        if (child_element.getLocalName().equals("INSULIST")) {
                            int insuNo = 1;
                            Iterator child1 = child_element.getChildren();
                            while (child1.hasNext()) {
                                OMElement child1OME = (OMElement) child1.next();
                                if (child1OME.getLocalName().equals("ITEM")) {
                                    insuListSchema = new WFInsuListSchema();
                                    // 不为空
                                    insuListSchema.setBatchNo(batchno);
                                    insuListSchema.setCardNo(cardNo);
                                    insuListSchema.setInsuNo(String
                                            .valueOf(insuNo));

                                    Iterator child2 = child1OME.getChildren();
                                    while (child2.hasNext()) {
                                        OMElement child2OME = (OMElement) child2
                                                .next();
                                        if (child2OME.getLocalName().equals(
                                                "INSUNO")) {

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "TOAPPNTRELA")) {

                                            insuListSchema.setToAppntRela(mTool
                                                    .codeToAppntRela(child2OME
                                                            .getText()));

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "RELATIONCODE")) {
                                            relationCode = child2OME.getText();
                                            insuListSchema
                                                    .setRelationCode(relationCode);

                                            System.out.println(relationCode);
                                        }
                                        if (child2OME.getLocalName().equals(
                                                "RELATIONTOINSURED")) {

                                            relationToInsured = child2OME
                                                    .getText();
                                            insuListSchema
                                                    .setRelationToInsured(relationToInsured);
                                            System.out
                                                    .println(relationToInsured);
                                        }
                                        if (child2OME.getLocalName().equals(
                                                "REINSUNO")) {
                                            reInsuNo = child2OME.getText();
                                            insuListSchema
                                                    .setReInsuNo(reInsuNo);
                                            System.out.println(reInsuNo);
                                        }
                                        if (child2OME.getLocalName().equals(
                                                "NAME")) {
                                            insuListSchema.setName(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "SEX")) {
                                            insuListSchema.setSex(mTool
                                                    .codeSex(child2OME
                                                            .getText()));

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "BIRTHDAY")) {
                                            insuListSchema
                                                    .setBirthday(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "IDTYPE")) {
                                            insuListSchema.setIDType(mTool
                                                    .codeIDType(child2OME
                                                            .getText()));

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "IDNO")) {
                                            insuListSchema.setIDNo(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "OCCUPATIONTYPE")) {
                                            insuListSchema
                                                    .setOccupationType(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "OCCUPATIONCODE")) {
                                            insuListSchema
                                                    .setOccupationCode(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "POSTALADDRESS")) {
                                            insuListSchema
                                                    .setPostalAddress(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "ZIPCODE")) {
                                            insuListSchema.setZipCode(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "PHONT")) {
                                            insuListSchema.setPhont(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "MOBILE")) {
                                            insuListSchema.setMobile(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "EMAIL")) {
                                            insuListSchema.setEmail(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "GRPNAME")) {
                                            insuListSchema.setGrpName(child2OME
                                                    .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "COMPANYPHONE")) {
                                            insuListSchema
                                                    .setCompanyPhone(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "COMPADDR")) {
                                            insuListSchema
                                                    .setCompAddr(child2OME
                                                            .getText());

                                        }
                                        if (child2OME.getLocalName().equals(
                                                "COMPZIPCODE")) {
                                            insuListSchema
                                                    .setCompZipCode(child2OME
                                                            .getText());

                                        }

                                    }
                                    insuListSet.add(insuListSchema);
                                    insuNo++;
                                }

                            }
                            map.put(insuListSet, "INSERT");
                        }
                        // 受益人信息
                        WFBnfListSchema bnfListSchema;
                        String toInsuNo = null;
						String toInsuRela = null;
						String bnfType = null;
						String bnfGrade = null;
						String bnfRate = null;
						String name = null;
						String sex = null;
						String birthday = null;
						String IDType = null;
						String IDNo = null;
                        if (child_element.getLocalName().equals("BNFLIST")) {

                            Iterator child1 = child_element.getChildren();
                            boolean loadFlag = true;
                            int seq = 1;
                        	int sum[] = new int[6];
                            ArrayList array = new ArrayList();
                            while (child1.hasNext()) {
                                OMElement child1OME = (OMElement) child1.next();

                                if (child1OME.getLocalName().equals("ITEM")) {
                                    Iterator child2 = child1OME.getChildren();
                                    bnfListSchema = new WFBnfListSchema();
                                    bnfListSchema.setBatchNo(batchno);
                                    bnfListSchema.setCardNo(cardNo);
                                    // item下没有节点
//                                    boolean flag = child2.hasNext();
//                                    if (!flag) {
//                                        loadFlag = false;
//                                    }
                                    int i =1;
                                    while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
										.next();
										String child2OMELocalName = child2OME
												.getLocalName();
		
										if (child2OMELocalName
												.equals("TOINSUNO")) {
											toInsuNo = child2OME.getText();
											System.out.println("toInsuNo "
													+ toInsuNo);
										}
										if (child2OMELocalName
												.equals("TOINSURELA")) {
											toInsuRela = child2OME.getText();
											System.out.println("toInsuRela "
													+ toInsuRela);
										}
										if (child2OMELocalName
												.equals("BNFTYPE")) {
											bnfType = child2OME.getText();
											System.out.println("bnfType "
													+ bnfType);
										}
										if (child2OMELocalName
												.equals("BNFGRADE")) {
											bnfGrade = child2OME.getText();
											System.out.println(bnfGrade);
										}
										if (child2OMELocalName
												.equals("BNFRATE")) {
											bnfRate = child2OME.getText();
											System.out.println(bnfRate);
										}
										if (child2OMELocalName.equals("NAME")) {
											name = child2OME.getText();
											if (name == null || "".equals(name)) {
												loadFlag = false;
											}
											System.out.println(name);
										}
										if (child2OMELocalName.equals("SEX")) {
											sex = child2OME.getText();
											if (sex == null || "".equals(sex)) {
												loadFlag = false;
											}
											System.out.println(sex);
										}
										if (child2OMELocalName
												.equals("BIRTHDAY")) {
											birthday = child2OME.getText();
											if (birthday == null
													|| "".equals(birthday)) {
												loadFlag = false;
											}
											System.out.println(birthday);
										}
										if (child2OMELocalName.equals("IDTYPE")) {
											IDType = mTool.codeIDType(child2OME
													.getText());
											if (IDType == null
													|| "".equals(IDType)) {
												//没有传受益人证件类型和证件号码，可以将受益人信息传到数据库
//												loadFlag = false;
											}
											System.out.println(IDType+":"+loadFlag);
										}
										if (child2OMELocalName.equals("IDNO")) {
											IDNo = child2OME.getText();
											if (IDNo == null || "".equals(IDNo)) {
//												loadFlag = false;
											}
											System.out.println(IDNo);
										}
		
									}
                                    bnfListSchema = new WFBnfListSchema();
    								// 不为空
    								bnfListSchema.setBatchNo(batchno);
    								bnfListSchema.setCardNo(cardNo);

    								bnfListSchema.setToInsuNo(toInsuNo);
    								bnfListSchema.setToInsuRela(toInsuRela);
    								bnfListSchema.setBnfType(bnfType);
    								bnfListSchema.setBnfGrade(bnfGrade);
    								bnfListSchema.setBnfRate(Double.parseDouble(bnfRate)/100);
    								int n = Integer.parseInt(bnfGrade);
    								if(array.contains(bnfGrade)){
                                    	seq++;
                                    	bnfListSchema.setBnfGradeNo(String.valueOf(seq));
                                    	sum[n] = (int)(sum[n] + Integer.parseInt(bnfRate));
                                	}else{
                                		bnfListSchema.setBnfGradeNo(String.valueOf(1));
                                		array.add(bnfGrade);
                                		sum[n] = (int) Integer.parseInt(bnfRate);
                                	}
    								bnfListSchema.setName(name);
    								bnfListSchema.setSex(mTool.codeSex(sex));
    								bnfListSchema.setBirthday(birthday);
    								bnfListSchema.setIDType(IDType);
    								bnfListSchema.setIDNo(IDNo);

    								bnfListSet.add(bnfListSchema);

    							}
                                
                            }
                            int length = sum.length;
							for(int i=1;i<length;i++){
								if(sum[i]==0)continue;
								if(sum[i]!=100){
									responseOME = buildResponseOME(null, null,
	                                        null, null, "01", "3", "受益顺位"+i+"的受益总和不等于100!");
	                                System.out
	                                        .println("受益顺位"+i+"的受益总和不等于100!" + responseOME);
	                                LoginVerifyTool.writeLog(responseOME, batchno,
	                                        messageType, sendDate, sendTime, "01",
	                                        "100", "3", "受益顺位"+i+"的受益总和不等于100!");
	                                return responseOME;
	                        	}
							}
                            if (loadFlag) {
                                map.put(bnfListSet, "INSERT");
                            }

                        }

                    }
                   
                    // 如果必须字段为空 中断返回
                    if (!fieldCompleteFlag == true) {
                        responseOME = buildResponseOME(null, null, null, null,
                                "01", "6", " 插入或更新值为空，但该列不能包含空值!---"
                                        + emptyField);
                        System.out.println("responseOME:" + responseOME);
                        LoginVerifyTool.writeLog(responseOME, batchno,
                                messageType, sendDate, sendTime, "01",
                                sendOperator, "6", " 插入或更新值为空，但该列不能包含空值!");
                        return responseOME;
                    }
                    // 日志3
                    LoginVerifyTool.writeBasicInfoLog(cardNo, uAppntNo,
                            insuListSchema.getInsuNo());

                }

            }
            // 保存数据
            if(!check(insuListSet,appntListSchema)){
            	responseOME = buildResponseOME(null, null,
                        null, null, "01", "5", this.mErrors.getLastError());
            	return responseOME;
            }

            // WFInsuListSchema liWFInsuListSchema = insuListSet.get(1);
            WFInsuListSet backInsuListSet = insuListSet;

            ExternalActive tExternalActive = new ExternalActive();
            LICardActiveInfoListSet exCardActiveInfoListSet = tExternalActive
                    .writeLICardActiveInfoList(contListSchema, backInsuListSet);
//			by gzh 
			if(exCardActiveInfoListSet == null ){
				System.out.println(tExternalActive.getMErrors().getErrContent());
				responseOME = buildResponseOME(null, null, null, null,"01","21",tExternalActive.getMErrors().getFirstError());
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "21", tExternalActive.getMErrors().getFirstError());
				return responseOME;
			}
            map.put(exCardActiveInfoListSet, "INSERT");
            //获取更新 lzcard state sql
            String modifyStateSql = tExternalActive.getUpdateLzcardSql(cardNo)[0];
            map.put(modifyStateSql, "UPDATE");
            
            // 入库
            PubSubmit tPubSubmit = new PubSubmit();
            tVData.add(map);
            if (!tPubSubmit.submitData(tVData, "INSERT")) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "CardActiveInfo";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                responseOME = buildResponseOME(null, null, null, null, "01",
                        "6", "数据提交接口中间表失败!");
                System.out.println("responseOME:" + responseOME);
                LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                        sendDate, sendTime, "01", sendOperator, "6",
                        "数据提交接口中间表失败");
                return responseOME;
            } else {
            	

           	 //发送邮件和短信------fromyzf
            	ExeSQL tExeSQL = new ExeSQL();
               	String dzdbSql = "select code from ldcode where codetype='hb_b2b'";
	   			SSRS bdpSSRS = tExeSQL.execSQL(dzdbSql);
	   			if(bdpSSRS != null && bdpSSRS.MaxRow > 0){
   				for(int k=1;k<=bdpSSRS.MaxRow;k++){
   					String code = bdpSSRS.GetText(k, 1);
   					System.out.println("code="+code);
   					if(code != null && !"".equals(code) ){
   						if(cardNo.startsWith(code)){
   							if("1A".equals(code)){
   								GosportEmailAndMessageBL togosport = new GosportEmailAndMessageBL();
   								togosport.sendSmsAndEmail(batchno,cardNo);
   								System.out.println("发送短信邮件成功！");
   								
//   							将承保处理成功后的信息与B2C业务区分功能相关联-----fromyzf
   					   			MsgHead myMsgHead=new MsgHead();
   					   			myMsgHead.setBatchNo(batchno);
   					   			myMsgHead.setBranchCode("HBYCCD");//EC_WX
   					   			myMsgHead.setMsgType(messageType);//"DSC001"
   					   			myMsgHead.setSendDate(sendDate);
   					   			myMsgHead.setSendOperator(sendOperator);//EC_WX
   					   			myMsgHead.setSendTime(sendTime);
   					   			LCContTable tLCContTable=new LCContTable();
   					   			tLCContTable.setPrtNo(cardNo);
   					   			WxContRelaInfoBL a=new WxContRelaInfoBL(myMsgHead,tLCContTable);
   					   			try {
   					   				String error=a.deal();
   					   				System.out.println(error);
   					   			} catch (Exception e) {
   					   				// TODO Auto-generated catch block
   					   				e.printStackTrace();
   					   			}
   					   			
   					   			//打印电子保单
   								makeLvPDF(batchno, cardNo);
   								
   							}
   						}
   					}
   				}
   			}
   			
   			
   			
               responseOME = buildResponseOME(contListSchema, appntListSchema,
                       insuListSet, bnfListSet, "00", "0", "成功!");
               System.out.println("responseOME:" + responseOME);
               LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                       sendDate, sendTime, "00", sendOperator, "0", "成功");
           
            	
            	
            	
                responseOME = buildResponseOME(contListSchema, appntListSchema,
                        insuListSet, bnfListSet, "00", "0", "成功!");
                System.out.println("responseOME:" + responseOME);
                LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                        sendDate, sendTime, "00", sendOperator, "0", "成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
            responseOME = buildResponseOME(null, null, null, null, "01", "4",
                    "发生异常");
            System.out.println("responseOME:" + responseOME);
            LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                    sendDate, sendTime, "01", sendOperator, "4", "发生异常");
            return responseOME;
        }
        return responseOME;

    }
    
    /**
     *打印远程出单版的电子保单---fromyzf
     *
     */
    private void makeLvPDF(String batchNo,String ContNo){
    	String cardno = ContNo;
    	GosportPrintBL ecp = new GosportPrintBL();
    	MsgCollection tMsgCollection = new MsgCollection();
    	MsgHead tMsgHead = new MsgHead();
    	tMsgHead.setBatchNo(batchNo);
    	tMsgHead.setBranchCode("001");
    	tMsgHead.setMsgType("INDIGO");
    	tMsgHead.setSendDate(PubFun.getCurrentDate());
    	tMsgHead.setSendTime(PubFun.getCurrentTime());
    	tMsgHead.setSendOperator("EC_WX");
    	tMsgCollection.setMsgHead(tMsgHead);
    	CertPrintTable tCertPrintTable = new CertPrintTable();
    	tCertPrintTable.setCardNo(cardno);
    	tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);
    	ecp.deal(tMsgCollection);
    }

    private OMElement buildResponseOME(WFContListSchema contListSchema,
            WFAppntListSchema appntListSchema, WFInsuListSet insuListSet,
            WFBnfListSet bnfListSet, String state, String errCode,
            String errInfo) {
        OMFactory fac = OMAbstractFactory.getOMFactory();

        OMElement DATASET = fac.createOMElement("YKTCONT", null);
        LoginVerifyTool tool = new LoginVerifyTool();
        tool.addOm(DATASET, "BATCHNO", batchno);
        tool.addOm(DATASET, "SENDDATE", sendDate);
        tool.addOm(DATASET, "SENDTIME", sendTime);

        tool.addOm(DATASET, "STATE", state);
        tool.addOm(DATASET, "POLICYNO", tool.getSerialNo());

        tool.addOm(DATASET, "ERRCODE", errCode);
        tool.addOm(DATASET, "ERRINFO", errInfo);

        return DATASET;
    }

//  20101122 卡单激活校验 by gzh
	private boolean check(WFInsuListSet insuListSet,WFAppntListSchema appntListSchema){
		boolean flag = true;
		LMCheckFieldDB tLMCheckFieldDB= new LMCheckFieldDB();
		tLMCheckFieldDB.setRiskCode(this.mRiskWrapCode);
		tLMCheckFieldDB.setFieldName("ETBINSERT");
		LMCheckFieldSet tLMCheckFieldSet = tLMCheckFieldDB.query();
		for(int i=1;i<=tLMCheckFieldSet.size();i++){
			LMCheckFieldSchema tLMCheckFieldSchema = tLMCheckFieldSet.get(i);
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			tLMCalModeDB.setCalCode(tLMCheckFieldSchema.getCalCode());
			LMCalModeSet tLMCalModeSet = new LMCalModeSet();
			tLMCalModeSet = tLMCalModeDB.query();
			for(int j=1;j<=tLMCalModeSet.size();j++){
				LMCalModeSchema tLMCalModeSchema = tLMCalModeSet.get(j);
				for(int k=1;k<=insuListSet.size();k++){
					WFInsuListSchema insuListSchema = insuListSet.get(k);
					int tAppntAge = PubFun.calInterval(insuListSchema.getBirthday(),
							PubFun.getCurrentDate(), "Y");
					Calculator cal = new Calculator();
					cal.setCalCode(tLMCalModeSchema.getCalCode());
					cal.addBasicFactor("InsuredName", insuListSchema.getName());
					cal.addBasicFactor("Sex", insuListSchema.getSex());
					cal.addBasicFactor("InsuredBirthday", insuListSchema.getBirthday());
					cal.addBasicFactor("IDType", insuListSchema.getIDType());
					cal.addBasicFactor("IDNo", insuListSchema.getIDNo());
					cal.addBasicFactor("RiskWrapCode", this.mRiskWrapCode);
					cal.addBasicFactor("AppAge",tAppntAge+"");
					cal.addBasicFactor("Mult",this.mMult);
					cal.addBasicFactor("OccupationType",insuListSchema.getOccupationType());
					cal.addBasicFactor("Cvalidate",mCValidate);
					String result = cal.calculate();
					if(!result.equals("0")){
						flag = false;
						//System.out.println("被保人"+insuListSchema.getName()+"数据有问题。");
						//setContUWError(tLMCalModeSchema.getRemark(), tLPContSchema);
		                CError tError = new CError();
		                tError.moduleName = "CardActiveInfo";
		                tError.functionName = "submitData";
		                tError.errorMessage = tLMCalModeSchema.getRemark();
		                this.mErrors.addOneError(tError);
					}
				}
			}
		}
		return flag;
	}
	
	 public static void main(String[] args) {
    	 //发送邮件和短信------fromyzf
      ExeSQL tExeSQL = new ExeSQL();
      String dzdbSql = "select code from ldcode where codetype='hb_b2b'";
		SSRS bdpSSRS = tExeSQL.execSQL(dzdbSql);
		if(bdpSSRS != null && bdpSSRS.MaxRow > 0){
			for(int k=1;k<=bdpSSRS.MaxRow;k++){
				String code = bdpSSRS.GetText(k, 1);
				System.out.println("code="+code);
				if(code != null && !"".equals(code) ){
					if("1A000000010".startsWith(code)){
						if("1A".equals(code)){
							GosportEmailAndMessageBL togosport = new GosportEmailAndMessageBL();
							togosport.sendSmsAndEmail("2142423","1A000000102");
							System.out.println("发送短信邮件成功！");
						}
					}
				}
			}
		}
		
		//测试存b2c业务区分表在WXCONTRELAINFOBL里的main方法里
		
		//测试电子保单
//		new HYXCont(). makeLvPDF("2142423", "1A000000010");
    }

}
