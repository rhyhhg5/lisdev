package com.ecwebservice.subinterface;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.cbcheck.GrpUWSendPrintUI;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.tb.ParseGuideIn;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealGrpContData {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	
	private String branchcode = "";// 网点
	
	private String sendoperator = "";// 操作员

	private String messageType = "";// 报文类型

	private OMElement responseOME = null;
	
	private String mPrtNo = null;
	private String mProposalGrpContNo = null;
	
	private MMap tMMap = new MMap();
	
	private String theCurrentDate = PubFun.getCurrentDate();
	private String theCurrentTime = PubFun.getCurrentTime();
	
	private LoginVerifyTool mTool = new LoginVerifyTool();
	//保单信息
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

	//投保单位信息
	private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();
	private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();
	private LDGrpSchema mLDGrpSchema = new LDGrpSchema();
	
	//保障计划
	LCContPlanSet mLCContPlanSet = new LCContPlanSet();
	LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
	LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
	
	//被保人
	LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();
	
	Reflections tReflections = new Reflections();
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildResponseOME("01","01", "加载团单数据失败!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "加载团单数据失败!");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			return responseOME;
		}
		if(!deal()){
			return responseOME;
		}
		if(!save()){
			return responseOME;
		}
		if(!calPrem()){
			return responseOME;
		}
		if(!creatMission()){
			return responseOME;
		}
		
		responseOME = buildResponseOME("00","01", "");
		
		return responseOME;
	}
	
	//加载卡激活信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("GRPCONTDATA")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDDATE")){
							this.sendDate=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDTIME")){
							this.sendTime= child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("MESSAGETYPE")){
							this.messageType=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("GRPCONTLIST")){
							Iterator childItem = child_element.getChildren();
							while(childItem.hasNext()){
								OMElement childOME = (OMElement) childItem.next();
								if(childOME.getLocalName().equals("ITEM")){
									Iterator child1OME = childOME.getChildren();
									while(child1OME.hasNext()){
										OMElement childleaf = (OMElement) child1OME.next();
										if(childleaf.getLocalName().equals("PrtNo")){
											this.mLCGrpContSchema.setPrtNo(childleaf.getText());
											this.mLCGrpAppntSchema.setPrtNo(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("ManageCom")){
											this.mLCGrpContSchema.setManageCom(childleaf.getText());
											mGlobalInput.ManageCom = childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("SaleChnl")){
											this.mLCGrpContSchema.setSaleChnl(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("HandlerDate")){
											this.mLCGrpContSchema.setHandlerDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentCom")){
											this.mLCGrpContSchema.setAgentCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentCode")){
											AgentCodeTransformation a = new AgentCodeTransformation();
											if(!a.AgentCode(childleaf.getText(), "N")){
												System.out.println(a.getMessage());
											}
											this.mLCGrpContSchema.setAgentCode(a.getResult());
											continue;
										}
										if(childleaf.getLocalName().equals("FirstTrialOperator")){
											this.mLCGrpContSchema.setFirstTrialOperator(childleaf.getText());
											mGlobalInput.Operator = childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("MarketType")){
											this.mLCGrpContSchema.setMarketType(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("ReceiveDate")){
											this.mLCGrpContSchema.setReceiveDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CoInsuranceFlag")){
											this.mLCGrpContSchema.setCoInsuranceFlag(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PayMode")){
											this.mLCGrpContSchema.setPayMode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("BankCode")){
											this.mLCGrpContSchema.setBankCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("BankAccNo")){
											this.mLCGrpContSchema.setBankAccNo(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AccName")){
											this.mLCGrpContSchema.setAccName(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PayIntv")){
											this.mLCGrpContSchema.setPayIntv(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CValiDate")){
											this.mLCGrpContSchema.setCValiDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CInValiDate")){
											this.mLCGrpContSchema.setCInValiDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("Remark")){
											this.mLCGrpContSchema.setRemark(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentSaleCode")){
											this.mLCGrpContSchema.setAgentSaleCode(childleaf.getText());
											continue;
										}
									}
								}
							}
						}
						
						if(child_element.getLocalName().equals("GRPAPPNTLIST")){
							Iterator child_appnt = child_element.getChildren();
							while(child_appnt.hasNext()){
								OMElement appnt_item = (OMElement) child_appnt.next();
								if(appnt_item.getLocalName().equals("ITEM")){
									Iterator appnt_element= appnt_item.getChildren();
									while(appnt_element.hasNext()){
										OMElement appntleaf = (OMElement)appnt_element.next();
										
										if(appntleaf.getLocalName().equals("GrpNo")){
											this.mLCGrpContSchema.setAppntNo(appntleaf.getText());
											this.mLCGrpAppntSchema.setCustomerNo(appntleaf.getText());
											this.mLDGrpSchema.setCustomerNo(appntleaf.getText());
											this.mLCGrpAddressSchema.setCustomerNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpName")){
											this.mLCGrpContSchema.setGrpName(appntleaf.getText());
											this.mLCGrpAppntSchema.setName(appntleaf.getText());
											this.mLDGrpSchema.setGrpName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Phone")){
											this.mLCGrpContSchema.setPhone(appntleaf.getText());
											this.mLCGrpAppntSchema.setPhone(appntleaf.getText());
											this.mLDGrpSchema.setPhone(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OrgancomCode")){
											this.mLCGrpAppntSchema.setOrganComCode(appntleaf.getText());
											this.mLDGrpSchema.setOrganComCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpAddress")){
											this.mLCGrpAppntSchema.setPostalAddress(appntleaf.getText());
											this.mLCGrpAddressSchema.setGrpAddress(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpZipCode")){
											this.mLCGrpAppntSchema.setZipCode(appntleaf.getText());
											this.mLCGrpAddressSchema.setGrpZipCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("TaxNo")){
											this.mLCGrpAppntSchema.setTaxNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonName")){
											this.mLCGrpAppntSchema.setLegalPersonName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDNo")){
											this.mLCGrpAppntSchema.setLegalPersonIDNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LinkMan1")){
											this.mLCGrpAddressSchema.setLinkMan1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Phone1")){
											this.mLCGrpAddressSchema.setPhone1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Fax1")){
											this.mLCGrpAddressSchema.setFax1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("E_Mail1")){
											this.mLCGrpAddressSchema.setE_Mail1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BusinessType")){
											this.mLCGrpContSchema.setBusinessType(appntleaf.getText());
											this.mLDGrpSchema.setBusinessType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BusinessBigType")){
											this.mLCGrpContSchema.setBusinessBigType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpNature")){
											this.mLCGrpContSchema.setGrpNature(appntleaf.getText());
											this.mLDGrpSchema.setGrpNature(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Peoples")){
											this.mLCGrpContSchema.setPeoples(appntleaf.getText());
											this.mLCGrpAppntSchema.setPeoples(appntleaf.getText());
											this.mLDGrpSchema.setPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("AppntOnWorkPeoples")){
											this.mLCGrpAppntSchema.setOnWorkPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOnWorkPeoples(appntleaf.getText());
											continue;
										}										
										if(appntleaf.getLocalName().equals("AppntOffWorkPeoples")){
											this.mLCGrpAppntSchema.setOffWorkPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOffWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("AppntOtherPeoples")){
											this.mLCGrpAppntSchema.setOtherPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOtherPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Peoples3")){
											this.mLCGrpContSchema.setPeoples3(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OnWorkPeoples")){
											this.mLCGrpContSchema.setOnWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OffWorkPeoples")){
											this.mLCGrpContSchema.setOffWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OtherPeoples")){
											this.mLCGrpContSchema.setOtherPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaPeoples")){
											this.mLCGrpContSchema.setRelaPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaMatePeoples")){
											this.mLCGrpContSchema.setRelaMatePeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaYoungPeoples")){
											this.mLCGrpContSchema.setRelaYoungPeoples(appntleaf.getText());
											continue;
										}
									}
								}
							}
						}
						//保障计划
						if(child_element.getLocalName().equals("CONTPLAN")){
							Iterator child_contplan = child_element.getChildren();
							while(child_contplan.hasNext()){
								OMElement contplan_item = (OMElement) child_contplan.next();
								if(contplan_item.getLocalName().equals("ITEM")){
									Iterator contplan_element = contplan_item.getChildren();
									LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
									LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
									while(contplan_element.hasNext()){
										OMElement contplanleaf = (OMElement)contplan_element.next();
										if(contplanleaf.getLocalName().equals("ContPlanCode")){
											tLCContPlanRiskSchema.setContPlanCode(contplanleaf.getText());
											tLCContPlanSchema.setContPlanCode(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("ContPlanName")){
											tLCContPlanRiskSchema.setContPlanName(contplanleaf.getText());
											tLCContPlanSchema.setContPlanName(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("Peoples3")){
											tLCContPlanSchema.setPeoples3(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("Peoples2")){
											tLCContPlanSchema.setPeoples2(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("RiskCode")){
											tLCContPlanRiskSchema.setRiskCode(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("MainRiskCode")){
											tLCContPlanRiskSchema.setMainRiskCode(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("RiskPrem")){
											tLCContPlanRiskSchema.setRiskPrem(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("RiskAmnt")){
											tLCContPlanRiskSchema.setRiskAmnt(contplanleaf.getText());
											continue;
										}
									}
									this.mLCContPlanSet.add(tLCContPlanSchema);
									this.mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
								}
							}
						}
						
						//保障计划要素
						if(child_element.getLocalName().equals("CONTPLANDUTYPARAM")){
							Iterator child_contplanparam = child_element.getChildren();
							while(child_contplanparam.hasNext()){
								OMElement contplanparam_item = (OMElement) child_contplanparam.next();
								LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
								if(contplanparam_item.getLocalName().equals("ITEM")){
									Iterator contplanparam_element = contplanparam_item.getChildren();
									while(contplanparam_element.hasNext()){
										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
										if(contplanparamleaf.getLocalName().equals("ContPlanCode")){
											tLCContPlanDutyParamSchema.setContPlanCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("ContPlanName")){
											tLCContPlanDutyParamSchema.setContPlanName(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("RiskCode")){
											tLCContPlanDutyParamSchema.setRiskCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("MainRiskCode")){
											tLCContPlanDutyParamSchema.setMainRiskCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("DutyCode")){
											tLCContPlanDutyParamSchema.setDutyCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactor")){
											tLCContPlanDutyParamSchema.setCalFactor(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorType")){
											tLCContPlanDutyParamSchema.setCalFactorType(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorValue")){
											tLCContPlanDutyParamSchema.setCalFactorValue(contplanparamleaf.getText());
											continue;
										}
									}
									this.mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
								}
							}
						}
						
						if(child_element.getLocalName().equals("INSULIST")){
							Iterator child_insu = child_element.getChildren();
							while(child_insu.hasNext()){
								OMElement insu_item = (OMElement) child_insu.next();
								LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
								if(insu_item.getLocalName().equals("ITEM")){
									Iterator insu_element = insu_item.getChildren();
									while(insu_element.hasNext()){
										OMElement insuleaf = (OMElement)insu_element.next();
										if(insuleaf.getLocalName().equals("InsuredID")){
											tLCInsuredListSchema.setInsuredID(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("ContID")){
											tLCInsuredListSchema.setContNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Retire")){
											tLCInsuredListSchema.setRetire(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("RelationToInsured")){
											tLCInsuredListSchema.setRelation(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("MainInsuredName")){
											tLCInsuredListSchema.setEmployeeName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("InsuredName")){
											tLCInsuredListSchema.setInsuredName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Sex")){
											tLCInsuredListSchema.setSex(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Birthday")){
											tLCInsuredListSchema.setBirthday(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDType")){
											tLCInsuredListSchema.setIDType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDNo")){
											tLCInsuredListSchema.setIDNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("ContPlanCode")){
											tLCInsuredListSchema.setContPlanCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("OccupationType")){
											tLCInsuredListSchema.setOccupationType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("OccupationCode")){
//											tLCInsuredListSchema.setocc(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("BankCode")){
											tLCInsuredListSchema.setBankCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("BankAccNo")){
											tLCInsuredListSchema.setBankAccNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("AccName")){
											tLCInsuredListSchema.setAccName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Phone")){
											tLCInsuredListSchema.setPhone(insuleaf.getText());
											continue;
										}
									}
									this.mLCInsuredListSet.add(tLCInsuredListSchema);
								}
							}
						}
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验
	 * @return
	 */
	private boolean check(){
		if(!checkInfo()){
			System.out.println("校验基本信息失败");
			return false;
		}
		return true;
	}
	
	/**
	 * 处理
	 * @return
	 */ 
	private boolean deal(){
		try{
	        mProposalGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo",mLCGrpAppntSchema.getCustomerNo());
	        if("".equals(mProposalGrpContNo) || mProposalGrpContNo == null){
	        	responseOME = buildResponseOME("01","11", "生成保单号码失败!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","11", "WX", "生成保单号码失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
	        }
	        if(!creatGrpCont()){
				return false;
	        }
	        if(!creatGrpPol()){
	        	return false;
	        }
	        if(!creatContPlan()){
	        	return false;
	        }
	        if(!creatInsured()){
	        	return false;
	        }
		}catch(Exception e){
			System.out.println("处理团单电子商务复杂产品异常！");
			e.printStackTrace();
			return false;
		}
									
		return true;
	}
	
	/**
	 *校验基本信息 
	 * @return
	 */
	private boolean checkInfo(){
		if(this.batchno ==null || this.batchno.equals("")){
			responseOME = buildResponseOME("01","02", "没有得到批次号!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "获取印刷号码失败!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if("".equals(this.mLCGrpContSchema.getPrtNo()) || this.mLCGrpContSchema.getPrtNo() == null){
			responseOME = buildResponseOME("01","03", "获取保单印刷号码失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","03", "WX", "获取印刷号码失败!");
			return false;
		}
		if("".equals(this.mLCGrpContSchema.getAppntNo()) || this.mLCGrpContSchema.getAppntNo() == null){
			responseOME = buildResponseOME("01","04", "获取投保单位客户号失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "获取投保单位客户号失败!");
			return false;
		}
		// 集团法人身份账号校验    20170926 zy
		String tLegalPersonIDNo=this.mLCGrpAppntSchema.getLegalPersonIDNo();
		if(tLegalPersonIDNo!=null && tLegalPersonIDNo!=""){
			String strChkIdNo=PubFun.CheckIDNo("0",tLegalPersonIDNo,"","");
			if(strChkIdNo !=""){
				responseOME = buildResponseOME("01","05", "法人"+strChkIdNo);
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "法人"+strChkIdNo);
				return false;
			}
		}
		// 校验被保人身份证号   20170926 zy
		if(this.mLCInsuredListSet!=null&& mLCInsuredListSet.size() != 0){
			LCInsuredListSet tmLCInsuredListSet=this.mLCInsuredListSet;
			for(int i=1;i<=tmLCInsuredListSet.size();i++){
				String name=tmLCInsuredListSet.get(i).getInsuredName();
				String idtype=tmLCInsuredListSet.get(i).getIDType();
				String idno=tmLCInsuredListSet.get(i).getIDNo();
				String sex=tmLCInsuredListSet.get(i).getSex();
				String birthday=tmLCInsuredListSet.get(i).getBirthday();
				if(idno!=null && idno!=""){
					String strChkIdNo=PubFun.CheckIDNo(idtype, idno, birthday, sex);
					if(strChkIdNo !=""){
						responseOME = buildResponseOME("01","06", "被保人:"+name+","+strChkIdNo);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","06", "WX", "被保人:"+name+","+strChkIdNo);
						return false;
					}
				}
			}
		}
		return true;
	}
	
	//提交卡激活信息
	private boolean save(){
		try{
			VData v = new VData();
			v.add(tMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				System.out.println("数据提交失败************************");
				responseOME = buildResponseOME("01","99", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","99", "WX", "数据提交失败!");
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME("01","99", "数据提交失败!");
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	private OMElement buildResponseOME(String state, String errCode,String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("GRPCONTDATA", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "PrtNo", mLCGrpContSchema.getPrtNo());

		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}
	
	private boolean creatGrpCont(){
		
        mLCGrpContSchema.setProposalGrpContNo(mProposalGrpContNo);
        mLCGrpContSchema.setGrpContNo(mProposalGrpContNo);
        mLCGrpAppntSchema.setGrpContNo(mProposalGrpContNo);
        if (mLCGrpContSchema.getAddressNo() == null || mLCGrpContSchema.getAddressNo().trim().equals(""))
        {
            try
            {
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(integer(AddressNo)) Is Null Then 0 Else max(integer(AddressNo)) End from LCGrpAddress where CustomerNo='"
                        + mLCGrpAppntSchema.getCustomerNo() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                String AddressNo = integer.toString();
                System.out.println("得到的地址码是：" + AddressNo);
                if (!"".equals(AddressNo))
                {
                    mLCGrpContSchema.setAddressNo(AddressNo);
                    mLCGrpAppntSchema.setAddressNo(AddressNo);
                    mLCGrpAddressSchema.setAddressNo(AddressNo);
                }
                else
                {
                	responseOME = buildResponseOME("01","13", "客户地址号码生成失败!");
    				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","13", "WX", "客户地址号码生成失败!");
    				System.out.println("responseOME:" + responseOME);
    				return false;
                }
            }
            catch (Exception e)
            {
            	responseOME = buildResponseOME("01","13", "地址码超长,生成号码失败,请先删除原来的超长地址码!!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","13", "WX", "地址码超长,生成号码失败,请先删除原来的超长地址码!!");
				System.out.println("responseOME:" + responseOME);
				return false;
            }
        }
        if (mLCGrpContSchema.getAppFlag() == null
                || mLCGrpContSchema.getAppFlag().equals(""))
        {
            mLCGrpContSchema.setAppFlag("0");
            mLCGrpContSchema.setStateFlag("0");
        }
        if (StrTool.cTrim(mLCGrpContSchema.getPolApplyDate()).equals(""))
        {
            mLCGrpContSchema.setPolApplyDate(theCurrentDate);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getInputDate()).equals(""))
        {
            mLCGrpContSchema.setInputDate(theCurrentDate);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getInputTime()).equals(""))
        {
            mLCGrpContSchema.setInputTime(theCurrentTime);
        }
        
        String tSQL = "select agentgroup from laagent where agentcode = '"+mLCGrpContSchema.getAgentCode()+"' ";
        String tAgentGroup = new ExeSQL().getOneValue(tSQL);
        
        mLCGrpContSchema.setContPrintType("0");
        mLCGrpContSchema.setPrintCount(1);
        mLCGrpContSchema.setCardFlag("b");
        mLCGrpContSchema.setAgentGroup(tAgentGroup);
        mLCGrpContSchema.setApproveFlag("0");
        mLCGrpContSchema.setUWFlag("0");
        mLCGrpContSchema.setSpecFlag("0");
        //记录入机日期
        mLCGrpContSchema.setMakeDate(theCurrentDate);
        mLCGrpAppntSchema.setMakeDate(theCurrentDate);
        //记录入机时间
        mLCGrpContSchema.setMakeTime(theCurrentTime);
        mLCGrpAppntSchema.setMakeTime(theCurrentTime);

        mLDGrpSchema.setMakeDate(theCurrentDate);
        mLDGrpSchema.setMakeTime(theCurrentTime);
        
        //记录入机时间
        mLCGrpAddressSchema.setMakeTime(theCurrentTime);
        mLCGrpAddressSchema.setMakeDate(theCurrentDate);
        
//      记录当前操作员
        mLCGrpContSchema.setOperator(mGlobalInput.Operator);
        mLCGrpAppntSchema.setOperator(mGlobalInput.Operator);
        mLCGrpAddressSchema.setOperator(mGlobalInput.Operator);
        mLDGrpSchema.setOperator(mGlobalInput.Operator);
        //记录最后一次修改日期
        mLCGrpContSchema.setModifyDate(theCurrentDate);
        mLCGrpAppntSchema.setModifyDate(theCurrentDate);
        mLCGrpAddressSchema.setModifyDate(theCurrentDate);
        mLDGrpSchema.setModifyDate(theCurrentDate);
        //记录最后一次修改时间
        mLCGrpContSchema.setModifyTime(theCurrentTime);
        mLCGrpAppntSchema.setModifyTime(theCurrentTime);
        mLCGrpAddressSchema.setModifyTime(theCurrentTime);
        mLDGrpSchema.setModifyTime(theCurrentTime);
        
        /** 如果页面没有传入行业性质,就从数据库里查询出来添上 */
        if (StrTool.cTrim(mLCGrpContSchema.getBusinessBigType()).equals(""))
        {
            String sql = "select CodeAlias from ldcode where codetype='businesstype'"
                    + " and code='" + mLCGrpContSchema.getBusinessType() + "'";
            String bigType = (new ExeSQL()).getOneValue(sql);
            if (StrTool.cTrim(bigType).equals("")
                    || StrTool.cTrim(bigType).equals("null"))
            {
            	responseOME = buildResponseOME("01","12", "获取行业性质失败!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","12", "WX", "获取行业性质失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
            }
            mLCGrpContSchema.setBusinessBigType(bigType);
        }
//        tMMap.put(mLDGrpSchema, "INSERT");
        tMMap.put(mLCGrpAddressSchema, "INSERT");
        tMMap.put(mLCGrpContSchema, "INSERT");
        tMMap.put(mLCGrpAppntSchema, "INSERT");
        
		return true;
	}
	
	private boolean creatGrpPol(){
		
		String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        for(int i=1;i<=mLCContPlanRiskSet.size();i++){
        	String tRiskCode = mLCContPlanRiskSet.get(i).getRiskCode();
        	LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tRiskCode);
            if (tLMRiskAppDB.getInfo() == false) {
            	responseOME = buildResponseOME("01","14", "没有查到险种信息!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","14", "WX", "没有查到险种信息!");
				System.out.println("responseOME:" + responseOME);
				return false;
            }
            String tNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);
        	LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        	tLCGrpPolSchema.setGrpPolNo(tNo); //如果是新增
            tLCGrpPolSchema.setGrpProposalNo(tNo);
            tLCGrpPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
            tLCGrpPolSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLCGrpPolSchema.setSaleChnl(mLCGrpContSchema.getSaleChnl());
            tLCGrpPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
            tLCGrpPolSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
            tLCGrpPolSchema.setAgentType(mLCGrpContSchema.getAgentType());
            tLCGrpPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
            tLCGrpPolSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
            tLCGrpPolSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
            tLCGrpPolSchema.setAddressNo(mLCGrpContSchema.getAddressNo());
            tLCGrpPolSchema.setRiskCode(tRiskCode);
            tLCGrpPolSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
            tLCGrpPolSchema.setPayIntv(mLCGrpContSchema.getPayIntv());
            tLCGrpPolSchema.setPayMode(mLCGrpContSchema.getPayMode());
            tLCGrpPolSchema.setExpPeoples(mLCGrpContSchema.getExpPeoples());
            tLCGrpPolSchema.setGrpName(mLCGrpContSchema.getGrpName());
            tLCGrpPolSchema.setComFeeRate(tLMRiskAppDB.getAppInterest());
            tLCGrpPolSchema.setBranchFeeRate(tLMRiskAppDB.getAppPremRate());
            tLCGrpPolSchema.setAppFlag("0");
            tLCGrpPolSchema.setStateFlag("0");
            tLCGrpPolSchema.setUWFlag("0");
            tLCGrpPolSchema.setApproveFlag("0");
            tLCGrpPolSchema.setModifyDate(theCurrentDate);
            tLCGrpPolSchema.setModifyTime(theCurrentTime);
            tLCGrpPolSchema.setOperator(mGlobalInput.Operator);
            tLCGrpPolSchema.setMakeDate(theCurrentDate);
            tLCGrpPolSchema.setMakeTime(theCurrentTime);
            tLCGrpPolSchema.setPayMode(mLCGrpContSchema.getPayMode());
            tLCGrpPolSchema.setRiskWrapFlag("N");
            
            mLCGrpPolSet.add(tLCGrpPolSchema);
            
        }
        
        tMMap.put(mLCGrpPolSet, "INSERT");
        
		return true;
	}
	
	private boolean creatContPlan(){
		
		String tContPlanCode = "";
		HashMap tContPlanCodeMap = new HashMap();
		LCContPlanSet tLCContPlanSet = new LCContPlanSet();
        for(int i=1;i<=mLCContPlanSet.size();i++){
        	for(int j=i+1;j<=mLCContPlanSet.size();j++){
        		if(mLCContPlanSet.get(i).getContPlanCode().equals(mLCContPlanSet.get(j).getContPlanCode())){
        			if(mLCContPlanSet.get(i).getPeoples2() != mLCContPlanSet.get(j).getPeoples2() || mLCContPlanSet.get(i).getPeoples3() != mLCContPlanSet.get(j).getPeoples3()){
        				responseOME = buildResponseOME("01","15", "相同保障计划中应保人数和实保人数不一致!");
        				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","15", "WX", "相同保障计划中应保人数和实保人数不一致!");
        				System.out.println("responseOME:" + responseOME);
        				return false;
        			}
        		}
        	}
        	tContPlanCode = mLCContPlanSet.get(i).getContPlanCode();
        	if(!tContPlanCodeMap.containsKey(tContPlanCode)){
        		tContPlanCodeMap.put(tContPlanCode, "");
        		LCContPlanSchema tLCContPlanSchema = mLCContPlanSet.get(i);
        		tLCContPlanSchema.setGrpContNo(mProposalGrpContNo);
        		tLCContPlanSchema.setProposalGrpContNo(mProposalGrpContNo);
        		tLCContPlanSchema.setPlanType("0");
        		tLCContPlanSchema.setOperator(mGlobalInput.Operator);
        		tLCContPlanSchema.setMakeDate(theCurrentDate);
        		tLCContPlanSchema.setMakeTime(theCurrentTime);
        		tLCContPlanSchema.setModifyDate(theCurrentDate);
        		tLCContPlanSchema.setModifyTime(theCurrentTime);
        		tLCContPlanSet.add(tLCContPlanSchema);
        	}
        }
        //默认保障计划
        LCContPlanSchema tempLCContPlanSchema = new LCContPlanSchema();
        tempLCContPlanSchema.setGrpContNo(mProposalGrpContNo);
        tempLCContPlanSchema.setProposalGrpContNo(mProposalGrpContNo);
        tempLCContPlanSchema.setContPlanCode("11");
        tempLCContPlanSchema.setContPlanName("默认计划");
        tempLCContPlanSchema.setPlanType("0");
        tempLCContPlanSchema.setPeoples2("0");
        tempLCContPlanSchema.setPeoples3("0");
        tempLCContPlanSchema.setOperator(mGlobalInput.Operator);
        tempLCContPlanSchema.setMakeDate(theCurrentDate);
        tempLCContPlanSchema.setMakeTime(theCurrentTime);
        tempLCContPlanSchema.setModifyDate(theCurrentDate);
        tempLCContPlanSchema.setModifyTime(theCurrentTime);
        tLCContPlanSet.add(tempLCContPlanSchema);
       
        tMMap.put(tLCContPlanSet, "INSERT");
		
        String tRiskCode = "";
		HashMap tRiskMap = new HashMap();
		LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
        for(int i=1;i<=mLCContPlanRiskSet.size();i++){
        	mLCContPlanRiskSet.get(i).setGrpContNo(mProposalGrpContNo);
        	mLCContPlanRiskSet.get(i).setProposalGrpContNo(mProposalGrpContNo);
        	mLCContPlanRiskSet.get(i).setMainRiskVersion("2002");
        	mLCContPlanRiskSet.get(i).setRiskVersion("2002");
        	mLCContPlanRiskSet.get(i).setPlanType("0");
        	mLCContPlanRiskSet.get(i).setOperator(mGlobalInput.Operator);
        	mLCContPlanRiskSet.get(i).setMakeDate(theCurrentDate);
        	mLCContPlanRiskSet.get(i).setMakeTime(theCurrentTime);
        	mLCContPlanRiskSet.get(i).setModifyDate(theCurrentDate);
        	mLCContPlanRiskSet.get(i).setModifyTime(theCurrentTime);
        	
        	//默认保障计划
        	tRiskCode = mLCContPlanRiskSet.get(i).getRiskCode();
        	if(!tRiskMap.containsKey(tRiskCode)){
        		tRiskMap.put(tRiskCode, "");
        		LCContPlanRiskSchema tempLCContPlanRiskSchema = new LCContPlanRiskSchema();
        		tReflections.transFields(tempLCContPlanRiskSchema,mLCContPlanRiskSet.get(i));
        		tempLCContPlanRiskSchema.setContPlanCode("11");
        		tempLCContPlanRiskSchema.setContPlanName("默认计划");
        		tempLCContPlanRiskSchema.setRiskPrem(0);
        		tempLCContPlanRiskSchema.setRiskAmnt(0);
        		tLCContPlanRiskSet.add(tempLCContPlanRiskSchema);
        	}
        	
        }
        
        tMMap.put(mLCContPlanRiskSet, "INSERT");
        tMMap.put(tLCContPlanRiskSet, "INSERT");
        
        String tParamRiskCode = "";
		HashMap tParamRiskMap = new HashMap();
		LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
        for(int i=1;i<=mLCContPlanDutyParamSet.size();i++){
        	tParamRiskCode = mLCContPlanDutyParamSet.get(i).getRiskCode();
        	mLCContPlanDutyParamSet.get(i).setGrpContNo(mProposalGrpContNo);
        	mLCContPlanDutyParamSet.get(i).setProposalGrpContNo(mProposalGrpContNo);
        	String tGrpPolNo = "";
        	for(int j=1;j<=mLCGrpPolSet.size();j++){
            	if(mLCContPlanDutyParamSet.get(i).getRiskCode().equals(mLCGrpPolSet.get(j).getRiskCode())){
            		tGrpPolNo = mLCGrpPolSet.get(j).getGrpPolNo();
            		break;
            	}
            }
        	mLCContPlanDutyParamSet.get(i).setGrpPolNo(tGrpPolNo);
        	mLCContPlanDutyParamSet.get(i).setMainRiskVersion("2002");
        	mLCContPlanDutyParamSet.get(i).setRiskVersion("2002");
        	mLCContPlanDutyParamSet.get(i).setPlanType("0");
        	String tSQL = "select PayPlanCode,GetDutyCode,InsuAccNo from lmriskdutyfactor where riskcode = '"+mLCContPlanDutyParamSet.get(i).getRiskCode()+"' and dutycode = '"+mLCContPlanDutyParamSet.get(i).getDutyCode()+"'";
        	SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        	if(tSSRS != null && tSSRS.MaxRow>0){
        		mLCContPlanDutyParamSet.get(i).setPayPlanCode(tSSRS.GetText(1, 1));
            	mLCContPlanDutyParamSet.get(i).setGetDutyCode(tSSRS.GetText(1, 2));
            	mLCContPlanDutyParamSet.get(i).setInsuAccNo(tSSRS.GetText(1, 3));
        	}else{
        		mLCContPlanDutyParamSet.get(i).setPayPlanCode("000000");
            	mLCContPlanDutyParamSet.get(i).setGetDutyCode("000000");
            	mLCContPlanDutyParamSet.get(i).setInsuAccNo("000000");
        	}
        	
        	if("DutyAmnt".equals(mLCContPlanDutyParamSet.get(i).getCalFactor()) && !"".equals(StrTool.cTrim(mLCContPlanDutyParamSet.get(i).getCalFactorValue()))){
        		String tSQL1 = "select 1 from lmriskdutyfactor where riskcode = '"+mLCContPlanDutyParamSet.get(i).getRiskCode()+"' and dutycode = '"+mLCContPlanDutyParamSet.get(i).getDutyCode()+"' and calfactor = 'DutyAmnt' ";
            	String tDutyAmntFlag = new ExeSQL().getOneValue(tSQL1);
            	if(!"1".equals(tDutyAmntFlag)){
            		for(int m=1;m<=mLCContPlanDutyParamSet.size();m++){
            			if("Amnt".equals(mLCContPlanDutyParamSet.get(m).getCalFactor())){
            				mLCContPlanDutyParamSet.get(m).setCalFactorValue(mLCContPlanDutyParamSet.get(i).getCalFactorValue());
            			}
            		}
            	}
        	}
        	
        	mLCContPlanDutyParamSet.get(i).setOrder(i);
        	
        	
//        	默认保障计划
        	if(!tParamRiskMap.containsKey(tParamRiskCode)){
        		tParamRiskMap.put(tParamRiskCode, "");
        		LCContPlanDutyParamSchema tempLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        		tReflections.transFields(tempLCContPlanDutyParamSchema,mLCContPlanDutyParamSet.get(i));
        		tempLCContPlanDutyParamSchema.setContPlanCode("11");
        		tempLCContPlanDutyParamSchema.setContPlanName("默认计划");
        		tempLCContPlanDutyParamSchema.setDutyCode("000000");
        		tempLCContPlanDutyParamSchema.setCalFactor("CalRule");
        		tempLCContPlanDutyParamSchema.setCalFactorType("1");
        		tempLCContPlanDutyParamSchema.setCalFactorValue("3");
        		tLCContPlanDutyParamSet.add(tempLCContPlanDutyParamSchema);
        	}
        	
        }
        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSet);
        tMMap.put(mLCContPlanDutyParamSet, "INSERT");
        
		return true;
	}
	
	private boolean creatInsured(){
		for(int i=1;i<=mLCInsuredListSet.size();i++){
			mLCInsuredListSet.get(i).setGrpContNo(mProposalGrpContNo);
			mLCInsuredListSet.get(i).setState("0");
			mLCInsuredListSet.get(i).setBatchNo(mLCGrpContSchema.getPrtNo());
			mLCInsuredListSet.get(i).setOperator(mGlobalInput.Operator);
			mLCInsuredListSet.get(i).setMakeDate(theCurrentDate);
			mLCInsuredListSet.get(i).setMakeTime(theCurrentTime);
			mLCInsuredListSet.get(i).setModifyDate(theCurrentDate);
			mLCInsuredListSet.get(i).setModifyTime(theCurrentTime);
		}
		tMMap.put(mLCInsuredListSet, SysConst.INSERT);
		
		return true;
	}
	
	private boolean calPrem(){
		ParseGuideIn tParseGuideIn = new ParseGuideIn();
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("GrpContNo", mProposalGrpContNo);
		
		tVData.add(tTransferData);
		tVData.add(mGlobalInput);
		if (tParseGuideIn.submitData(tVData, "INSERT||DATABASE") == false) {
			// @@错误处理
			responseOME = buildResponseOME("01", "15", "保费计算失败，原因为："+tParseGuideIn.mErrors.getLastError()+"!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "15", "WX","保费计算失败，原因为："+tParseGuideIn.mErrors.getLastError()+"!");
			System.out.println("responseOME:" + responseOME);
			return false;
		} 
		return true;
	}
	
	private boolean creatMission(){
		String tSQL1 = "update lcgrpcont set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
		String tSQL2 = "update lcgrppol set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
		String tSQL3 = "update lccont set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
		String tSQL4 = "update lcpol set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwcode = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
		String tMissionID = PubFun1.CreateMaxNo("MissionID", 20);
		String tSQL5 = "INSERT INTO lwmission VALUES ('"+tMissionID+"','1','0000000004','0000002006','1','"+mProposalGrpContNo+"','"+mLCGrpContSchema.getPrtNo()+"','02','"+mLCGrpContSchema.getManageCom()+"','"+mLCGrpContSchema.getAgentCode()+"','"+mLCGrpContSchema.getAgentGroup()+"','"+mLCGrpContSchema.getGrpName()+"','"+mLCGrpContSchema.getCValiDate()+"','"+mLCGrpContSchema.getAppntNo()+"','"+theCurrentDate+"','"+theCurrentTime+"',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'"+mGlobalInput.Operator+"','"+mGlobalInput.Operator+"','"+theCurrentDate+"','"+theCurrentTime+"','"+theCurrentDate+"','"+theCurrentTime+"',NULL,NULL,NULL,NULL) ";
		MMap tempMMap = new MMap();
		tempMMap.put(tSQL1, SysConst.UPDATE);
		tempMMap.put(tSQL2, SysConst.UPDATE);
		tempMMap.put(tSQL3, SysConst.UPDATE);
		tempMMap.put(tSQL4, SysConst.UPDATE);
		tempMMap.put(tSQL5, SysConst.INSERT);
		
		
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		String prtSeq = PubFun1.CreateMaxNo("GPAYNOTICENO",mLCGrpContSchema.getPrtNo());
		tLOPRTManagerSchema.setPrtSeq(prtSeq);
        tLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLOPRTManagerSchema.setOtherNoType("01");
        tLOPRTManagerSchema.setCode("57");
        tLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
        tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0");//前台打印
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setMakeDate(theCurrentDate);
        tLOPRTManagerSchema.setMakeTime(theCurrentTime);
        
        tempMMap.put(tLOPRTManagerSchema, SysConst.INSERT);
        
        
		try{
			VData v = new VData();
			v.add(tempMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				responseOME = buildResponseOME("01", "15", "生成核保数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "15", "WX","生成核保数据失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME("01", "15", "生成核保数据失败！");
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
