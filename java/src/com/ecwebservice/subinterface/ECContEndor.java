package com.ecwebservice.subinterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.bl.LPInsuredBL;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LPAddressDB;
import com.sinosoft.lis.db.LPInsuredDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LPAddressSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

/**
 * Title:个单客户资料变更
 * 
 * @author zhangyige 创建时间：2012-07-09
 * @Description:
 * @version
 */

public class ECContEndor {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String contNo = null;// 保单号
	
	private String dealChannel = "3"; //3 - 电话申请 （需要修改为网上申请）
	
	private String edorOperator = "zyg";//上线时需要修改为具有保全操作权限的核心系统用户（如EC_WX）
	
	private String mManageCom = "86";//管理机构

	private VData mVData = new VData();

	private String sendOperator = null;// g 非发送报文提供

	HashMap appntMap = new HashMap(); // 存储投保人联系方式信息

	List insuList = new ArrayList(); // 存储被保人联系方式信息

	/** 当前日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 当前时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	// 错误处理类
	public CErrors mErrors = new CErrors();

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {
			LoginVerifyTool mTool = new LoginVerifyTool();

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("ECCONTENDOR")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println("BATCHNO" + batchno);
							boolean validBatchNo = mTool
									.isValidBatchNo(batchno);
							if (!validBatchNo) {
								// 批次号不唯一
								responseOME = buildResponseOME("01", "21",
										"批次号不唯一!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "21", "批次号不唯一!");
								return responseOME;
							}
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}

						if (child_element.getLocalName().equals("CONTNO")) {
							contNo = child_element.getText();

							System.out.println(contNo);
						}
						// 投保人信息

						if (child_element.getLocalName().equals("APPNTLIST")) {

							/* 投保人信息 */
							String name = null;// 姓名

							String email = null; // 邮箱地址
							
							String phone = null; // 联系电话

							String mobile = null; // 手机号

							String homePhone = null; // 家庭电话

							String companyPhone = null; // 公司电话

							String homeZipcode = null; // 家庭邮编

							String postalAddress = null; // 通讯地址
							
							String applyType = null;// 00 - 不变更 01 - 变更

							Iterator child1 = child_element.getChildren();

							while (child1.hasNext()) {
								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										if (child2OME.getLocalName().equals(
												"NAME")
												&& child2OME.getText() != null) {
											name = child2OME.getText();
											System.out.println("name:" + name);
										}
										if (child2OME.getLocalName().equals(
												"EMAIL")
												&& child2OME.getText() != null) {
											email = child2OME.getText();
											System.out
													.println("email:" + email);
										}
										if (child2OME.getLocalName().equals(
												"Phone")
												&& child2OME.getText() != null) {
											phone = child2OME.getText();
											System.out
											.println("phone:" + phone);
										}
										if (child2OME.getLocalName().equals(
												"MOBILE")
												&& child2OME.getText() != null) {
											mobile = child2OME.getText();
											System.out.println("mobile:"
													+ mobile);
										}
										if (child2OME.getLocalName().equals(
												"HOMEPHONE")
												&& child2OME.getText() != null) {
											homePhone = child2OME.getText();
											System.out.println("homePhone:"
													+ homePhone);
										}
										if (child2OME.getLocalName().equals(
												"COMPANYPHONE")
												&& child2OME.getText() != null) {
											companyPhone = child2OME.getText();
											System.out.println("companyPhone:"
													+ companyPhone);
										}
										if (child2OME.getLocalName().equals(
												"HOMEZIPCODE")
												&& child2OME.getText() != null) {
											homeZipcode = child2OME.getText();
											System.out.println("homeZipcode:"
													+ homeZipcode);
										}
										if (child2OME.getLocalName().equals(
												"POSTALADDRESS")
												&& child2OME.getText() != null) {
											postalAddress = child2OME.getText();
											System.out.println("postalAddress:"
													+ postalAddress);
										}
										if (child2OME.getLocalName().equals(
												"APPLYTYPE")
												&& child2OME.getText() != null) {
											applyType = child2OME.getText();
											System.out.println("applyType:"
													+ applyType);
										}
									}
								}
								if (applyType.equals("01")) {
									appntMap.put("NAME", name);
									appntMap.put("HomeZipCode", homeZipcode);
									appntMap.put("HomePhone", homePhone);
									appntMap.put("CompanyPhone", companyPhone);
									appntMap
											.put("PostalAddress", postalAddress);
									appntMap.put("Mobile", mobile);
									appntMap.put("EMAIL", email);
									appntMap.put("Phone", phone);
								}
							}
						}
						if (child_element.getLocalName().equals("INSULIST")) {
							Iterator child1 = child_element.getChildren();

							while (child1.hasNext()) {
								/* 被保人信息 */

								String name = null; // 姓名
								
								String email = null; // 邮箱地址
								
								String phone=null; //通信电话  liujiandong weixin 20180122

								String mobile = null; // 手机号

								String homePhone = null; // 家庭电话

								String companyPhone = null; // 公司电话

								String homeZipcode = null; // 家庭邮编

								String postalAddress = null; // 通讯地址

								String applyType = null;// 00 - 不变更 01 - 变更

								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										if (child2OME.getLocalName().equals(
												"NAME")
												&& child2OME.getText() != null) {
											name = child2OME.getText();
											System.out.println("name:" + name);
										}
										if (child2OME.getLocalName().equals(
												"EMAIL")
												&& child2OME.getText() != null) {
											email = child2OME.getText();
											System.out
													.println("email:" + email);
										}
										if (child2OME.getLocalName().equals(//liujiandong 20180122 weixin
												"Phone")
												&& child2OME.getText() != null) {
											phone = child2OME.getText();
											System.out.println("phone:"
													+ phone);
										}
										if (child2OME.getLocalName().equals(
												"MOBILE")
												&& child2OME.getText() != null) {
											mobile = child2OME.getText();
											System.out.println("mobile:"
													+ mobile);
										}
										
										if (child2OME.getLocalName().equals(
												"HOMEPHONE")
												&& child2OME.getText() != null) {
											homePhone = child2OME.getText();
											System.out.println("homePhone:"
													+ homePhone);
										}
										if (child2OME.getLocalName().equals(
												"COMPANYPHONE")
												&& child2OME.getText() != null) {
											companyPhone = child2OME.getText();
											System.out.println("companyPhone:"
													+ companyPhone);
										}
										if (child2OME.getLocalName().equals(
												"HOMEZIPCODE")
												&& child2OME.getText() != null) {
											homeZipcode = child2OME.getText();
											System.out.println("homeZipcode:"
													+ homeZipcode);
										}
										if (child2OME.getLocalName().equals(
												"POSTALADDRESS")
												&& child2OME.getText() != null) {
											postalAddress = child2OME.getText();
											System.out.println("postalAddress:"
													+ postalAddress);
										}
										if (child2OME.getLocalName().equals(
												"APPLYTYPE")
												&& child2OME.getText() != null) {
											applyType = child2OME.getText();
											System.out.println("applyType:"
													+ applyType);
										}
									}
								}
								if (applyType.equals("01")) {
									HashMap tMap = new HashMap();
									tMap.put("NAME", name);
									tMap.put("HomeZipCode", homeZipcode);
									tMap.put("HomePhone", homePhone);
									tMap.put("CompanyPhone", companyPhone);
									tMap.put("PostalAddress", postalAddress);
									tMap.put("Mobile", mobile);
									tMap.put("EMAIL", email);
									tMap.put("Phone", phone);
									insuList.add(tMap);
								}
							}
						}
					}
				}

				for (int i = 0; i < insuList.size(); i++) {//封装被保人信息
					HashMap insuMap = (HashMap) insuList.get(i);
					if(insuMap.size() > 0){
						mVData.add(getInsuEndorData(insuMap));
					}
				}
				//	 被保人联系方式变更--调用简易保全处理程序--------------------------------------------
				InsuEasyEdorUI tInsuEasyEdorUI = new InsuEasyEdorUI();
				System.out.println("mVData.size():" + mVData.size());
				String insucontent = null;
				for (int i = 0; i < mVData.size(); i++) {
					VData iVData = (VData) mVData.get(i);
					if (!tInsuEasyEdorUI.submitData(iVData)) {
						insucontent = "被保人联系方式修改失败！" + tInsuEasyEdorUI.getError();
						responseOME = buildResponseOME("01", "1", insucontent);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "00",
								sendOperator, "0", "成功");
						return responseOME;
					} 
				}
				responseOME = buildResponseOME("00", "0", "成功");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "00", sendOperator, "0", "成功");
				
				
				mVData.clear();
				//
				// 投保人联系方式变更--调用简易保全处理程序
				// -------------------------------------------------------------
				if (appntMap.size() > 0) {
					mVData.add(getAppntEndorData(appntMap));
				}
				EasyEdorUI tEasyEdorUI = new EasyEdorUI();
				System.out.println("mVData.size():" + mVData.size());
				String content = null;
				for (int i = 0; i < mVData.size(); i++) {
					VData iVData = (VData) mVData.get(i);
					if (!tEasyEdorUI.submitData(iVData)) {
						content = "投保人联系方式修改失败！" + tEasyEdorUI.getError();
						responseOME = buildResponseOME("01", "1", content);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "00",
								sendOperator, "0", "成功");
						return responseOME;
					} 
				}
				responseOME = buildResponseOME("00", "0", "成功");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "00", sendOperator, "0", "成功");

				// -------------------------------------------------------------
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", sendOperator, "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}
	
	
	/**
	 * 进行被保人联系方式变更 生成lpinsured和lpaddress 新增一条lcaddress 修改lcinsured 的addressno
	 * 
	 * @param data
	 * @return
	 */
	protected boolean doInsuredEndor(HashMap data) {

		String name = (String) data.get("NAME");
		String HomeZipCode = (String) data.get("HomeZipCode");
		String HomePhone = (String) data.get("HomePhone");
		String CompanyPhone = (String) data.get("CompanyPhone");
		String PostalAddress = (String) data.get("PostalAddress");
		String Mobile = (String) data.get("Mobile");
		String EMAIL = (String) data.get("EMAIL");
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = mManageCom;// 总公司
		tG.Operator = edorOperator;// 电子商务

		// 获取必要的信息
		String CustomerNo = null;// 客户号
		String IDNo = null;// 客户证件号码
		ExeSQL exeSql = new ExeSQL();
		String getCustomerNoSql = "";
		getCustomerNoSql = "select insuredno,idno from lcinsured "
				+ " where  contno = '" + this.contNo + "' and name = '" + name
				+ "' with ur";
		SSRS tSSRS = exeSql.execSQL(getCustomerNoSql);

		CustomerNo = tSSRS.GetText(1, 1);
		IDNo = tSSRS.GetText(1, 2);

		LCAddressSchema tLCAddressSchema = new LCAddressSchema();
		int ttNo = 0;//最大号
		int nextNo = 0;//最大号加1
		//处理地址编号
		if (StrTool.compareString(tLCAddressSchema.getAddressNo(), "")) {
			ExeSQL tExeSQL = new ExeSQL();
			SSRS aSSRS = new SSRS();
			String sql = "  Select Case When max(int(AddressNo)) Is Null Then 1 "
					+ "Else max(int(AddressNo))  End "
					+ "from LCAddress "
					+ "where CustomerNo='" + CustomerNo + "'";

			aSSRS = tExeSQL.execSQL(sql);
			Integer firstinteger = Integer.valueOf(aSSRS.GetText(1, 1));
			ttNo = firstinteger.intValue();
			nextNo = ttNo+ 1;
			System.out.println("最大地址码加1为:" + nextNo);			
		}
		LCAddressDB tLCAddressDB = new LCAddressDB();
		tLCAddressDB.setCustomerNo(CustomerNo);
		tLCAddressDB.setAddressNo(""+ttNo);
		if(tLCAddressDB.getInfo()){
			tLCAddressSchema = tLCAddressDB.getSchema();
		}
		tLCAddressSchema.setCustomerNo(CustomerNo);
		tLCAddressSchema.setZipCode(HomeZipCode);
		tLCAddressSchema.setHomePhone(HomePhone);
		tLCAddressSchema.setCompanyPhone(CompanyPhone);
		tLCAddressSchema.setPostalAddress(PostalAddress);
		tLCAddressSchema.setMobile(Mobile);
		tLCAddressSchema.setEMail(EMAIL);
		tLCAddressSchema.setAddressNo("" + nextNo);
		
		String edorNo = new CommonBL().createWorkNo(); // 保全号
		// 生成P表数据
		boolean flag = addEndorData1(tLCAddressSchema, tG, edorNo);
		if (flag) {
			boolean t_flag = addEndorData2(edorNo);
			if (!t_flag) {
				deleteEndorData1(edorNo, CustomerNo);
				return false;
			}
		} else {
			return false;
		}

		return true;
	}
	

	/**
	 * 获取被保人保全变更数据信息
	 * 
	 * @return
	 */
	private VData getInsuEndorData(HashMap data) {

		String name = (String) data.get("NAME");
		String HomeZipCode = (String) data.get("HomeZipCode");
		String HomePhone = (String) data.get("HomePhone");
		String CompanyPhone = (String) data.get("CompanyPhone");
		String PostalAddress = (String) data.get("PostalAddress");
		String Mobile = (String) data.get("Mobile");
		String EMAIL = (String) data.get("EMAIL");
		String Phone = (String) data.get("Phone");//liujiandong  weixin 20180119

		// 获取必要的信息
		String CustomerNo = null;// 客户号
		String IDNo = null;// 客户证件号码
		String PriorityNo = null;// 优先级别编号
		String tTypeNo = "03";// 业务类型 03 - 客户联系方式变更
		String DateLimit = null;// 时限
		String ApplyTypeNo = null;// 申请人类型编号
		String ApplyName = name;// 申请人姓名
		String AcceptWayNo = dealChannel;// 受理途径编号 3-电话申请
		String AcceptDate = mCurrentDate;// 受理日期
		String Remark = null;// 备注

		ExeSQL exeSql = new ExeSQL();
		String getCustomerNoSql = null;
		getCustomerNoSql = "select InsuredNo,idno from lcinsured  "
				+ " where contno = '" + this.contNo + "' and name = '"
				+ name + "' with ur";
		SSRS tSSRS = exeSql.execSQL(getCustomerNoSql);

		CustomerNo = tSSRS.GetText(1, 1);
		IDNo = tSSRS.GetText(1, 2);

		GlobalInput tG = new GlobalInput();
		tG.ManageCom = mManageCom;// 总公司
		tG.Operator = edorOperator;// 电子商务

		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(CustomerNo);
		tLGWorkSchema.setCustomerCardNo(IDNo);
		tLGWorkSchema.setPriorityNo(PriorityNo);
		tLGWorkSchema.setTypeNo(tTypeNo);
		tLGWorkSchema.setDateLimit(DateLimit);
		tLGWorkSchema.setApplyTypeNo(ApplyTypeNo);
		tLGWorkSchema.setApplyName(ApplyName);
		tLGWorkSchema.setAcceptWayNo(AcceptWayNo);
		tLGWorkSchema.setAcceptDate(AcceptDate);
		tLGWorkSchema.setRemark(Remark);

		// 得到客户被保信息
		LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
		tLPInsuredSchema.setContNo(this.contNo);
		tLPInsuredSchema.setInsuredNo(CustomerNo);

		// 得到客户保单联系地址
		LCAddressSchema tLCAddressSchema = new LCAddressSchema();
		int ttNo = 0;//最大号
		int ttNextNo = 0;//最大号+1
		//处理地址编号
		if (StrTool.compareString(tLCAddressSchema.getAddressNo(), "")) {
			ExeSQL tExeSQL = new ExeSQL();
			SSRS aSSRS = new SSRS();
			String sql = "  Select Case When max(int(AddressNo)) Is Null Then 1 "
					+ "Else max(int(AddressNo))  End "
					+ "from LCAddress "
					+ "where CustomerNo='" + CustomerNo + "'";

			aSSRS = tExeSQL.execSQL(sql);
			Integer firstinteger = Integer.valueOf(aSSRS.GetText(1, 1));
			ttNo = firstinteger.intValue();		
			ttNextNo = ttNo+1;
		}
		System.out.println("最大地址码加1为:"+ttNextNo);
		LCAddressDB tLCAddressDB = new LCAddressDB();
		tLCAddressDB.setCustomerNo(CustomerNo);
		tLCAddressDB.setAddressNo(""+ttNo);
		if(tLCAddressDB.getInfo()){
			tLCAddressSchema = tLCAddressDB.getSchema();
		}
		tLCAddressSchema.setAddressNo(""+ttNextNo);
		tLCAddressSchema.setCustomerNo(CustomerNo);
		if(HomeZipCode!=null&&!HomeZipCode.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setZipCode(HomeZipCode);
		}
		if(HomePhone!=null&&!HomePhone.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setHomePhone(HomePhone);
		}
		if(CompanyPhone!=null&&!CompanyPhone.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setCompanyPhone(CompanyPhone);
		}
		if(PostalAddress!=null&&!PostalAddress.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setPostalAddress(PostalAddress);
		}
		if(Mobile!=null&&!Mobile.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setMobile(Mobile);

		}
		if(EMAIL!=null&&!EMAIL.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setEMail(EMAIL);

		}
		if(Phone!=null&&!Phone.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setPhone(Phone);

		}
		LPContSet tLPContSet = new LPContSet();
		LPContSchema tLPContSchema = new LPContSchema();
		tLPContSchema.setEdorNo("");
		tLPContSchema.setEdorType("");
		tLPContSchema.setContNo(this.contNo);
		tLPContSet.add(tLPContSchema);
		VData tdata = new VData();
		tdata.add(tG);
		tdata.add(tLGWorkSchema);
		tdata.add(tLPInsuredSchema);
		tdata.add(tLCAddressSchema);
		tdata.add(tLPContSet);

		return tdata;
	}

	
	private VData getAppntEndorData(HashMap data) {

		String name = (String) data.get("NAME");
		String HomeZipCode = (String) data.get("HomeZipCode");
		String HomePhone = (String) data.get("HomePhone");
		String CompanyPhone = (String) data.get("CompanyPhone");
		String PostalAddress = (String) data.get("PostalAddress");
		String Mobile = (String) data.get("Mobile");
		String EMAIL = (String) data.get("EMAIL");
		String Phone = (String) data.get("Phone");//added by zhangyige 2014-08-20 for weixin

		// 获取必要的信息
		String CustomerNo = null;// 客户号
		String IDNo = null;// 客户证件号码
		String PriorityNo = null;// 优先级别编号
		String tTypeNo = "03";// 业务类型 03 - 客户联系方式变更
		String DateLimit = null;// 时限
		String ApplyTypeNo = null;// 申请人类型编号
		String ApplyName = name;// 申请人姓名
		String AcceptWayNo = dealChannel;// 受理途径编号 3-电话申请
		String AcceptDate = mCurrentDate;// 受理日期
		String Remark = null;// 备注

		ExeSQL exeSql = new ExeSQL();
		String getCustomerNoSql = null;
		getCustomerNoSql = "select appntno,idno from lcappnt  "
				+ " where contno = '" + this.contNo + "' and appntname = '"
				+ name + "' with ur";
		SSRS tSSRS = exeSql.execSQL(getCustomerNoSql);

		CustomerNo = tSSRS.GetText(1, 1);
		IDNo = tSSRS.GetText(1, 2);

		GlobalInput tG = new GlobalInput();
		tG.ManageCom = mManageCom;// 总公司
		tG.Operator = edorOperator;// 电子商务

		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(CustomerNo);
		tLGWorkSchema.setCustomerCardNo(IDNo);
		tLGWorkSchema.setPriorityNo(PriorityNo);
		tLGWorkSchema.setTypeNo(tTypeNo);
		tLGWorkSchema.setDateLimit(DateLimit);
		tLGWorkSchema.setApplyTypeNo(ApplyTypeNo);
		tLGWorkSchema.setApplyName(ApplyName);
		tLGWorkSchema.setAcceptWayNo(AcceptWayNo);
		tLGWorkSchema.setAcceptDate(AcceptDate);
		tLGWorkSchema.setRemark(Remark);

		// 得到客户投保信息
		LPAppntSchema tLPAppntSchema = new LPAppntSchema();
		tLPAppntSchema.setContNo(this.contNo);
		tLPAppntSchema.setAppntNo(CustomerNo);

		// 得到客户保单联系地址
		LCAddressSchema tLCAddressSchema = new LCAddressSchema();
		int ttNo = 0;//最大号
		int ttNextNo = 0;//最大号+1
		//处理地址编号
		if (StrTool.compareString(tLCAddressSchema.getAddressNo(), "")) {
			ExeSQL tExeSQL = new ExeSQL();
			SSRS aSSRS = new SSRS();
			String sql = "  Select Case When max(int(AddressNo)) Is Null Then 1 "
					+ "Else max(int(AddressNo))  End "
					+ "from LCAddress "
					+ "where CustomerNo='" + CustomerNo + "'";

			aSSRS = tExeSQL.execSQL(sql);
			Integer firstinteger = Integer.valueOf(aSSRS.GetText(1, 1));
			ttNo = firstinteger.intValue();		
			ttNextNo = ttNo+1;
		}
		System.out.println("最大地址码加1为:"+ttNextNo);
		LCAddressDB tLCAddressDB = new LCAddressDB();
		tLCAddressDB.setCustomerNo(CustomerNo);
		tLCAddressDB.setAddressNo(""+ttNo);
		if(tLCAddressDB.getInfo()){
			tLCAddressSchema = tLCAddressDB.getSchema();
		}
		tLCAddressSchema.setAddressNo(""+ttNextNo);
		tLCAddressSchema.setCustomerNo(CustomerNo);
		if(HomeZipCode!=null&&!HomeZipCode.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setHomeZipCode(HomeZipCode);
		}
		if(HomePhone!=null&&!HomePhone.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setHomePhone(HomePhone);
		}
		if(CompanyPhone!=null&&!CompanyPhone.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setCompanyPhone(CompanyPhone);
		}
	
		if(PostalAddress!=null&&!PostalAddress.equals("")){
			tLCAddressSchema.setPostalAddress(PostalAddress);//added by zhangyige 2014-08-20 for weixin
		}
		if(EMAIL!=null&&!EMAIL.equals("")){//added by liujiandong 2018-01-19 for weixin
			tLCAddressSchema.setEMail(EMAIL);
		}
		
		if(Mobile!=null&&!Mobile.equals("")){
			tLCAddressSchema.setMobile(Mobile);//added by zhangyige 2014-08-20 for weixin
		}
		if(Phone!=null&&!Phone.equals("")){
			tLCAddressSchema.setPhone(Phone);//added by zhangyige 2014-08-20 for weixin
		}

		LPContSet tLPContSet = new LPContSet();
		LPContSchema tLPContSchema = new LPContSchema();
		tLPContSchema.setEdorNo("");
		tLPContSchema.setEdorType("");
		tLPContSchema.setContNo(this.contNo);
		tLPContSet.add(tLPContSchema);
		VData tdata = new VData();
		tdata.add(tG);
		tdata.add(tLGWorkSchema);
		tdata.add(tLPAppntSchema);
		tdata.add(tLCAddressSchema);
		tdata.add(tLPContSet);

		return tdata;
	}
	
	

	/**
	 * 生成P表数据
	 * 
	 * @param tLCAddressSchema
	 * @param tG
	 * @return
	 */
	private boolean addEndorData1(LCAddressSchema tLCAddressSchema,
			GlobalInput tG, String edorNo) {
		MMap aMMap = new MMap();
		String CustomerNo = tLCAddressSchema.getCustomerNo();
		String edorType = "AD";// 联系地址变更
		
		LPAddressSchema tLPAddressSchema = new LPAddressSchema();
		Reflections ref = new Reflections();
		ref.transFields(tLPAddressSchema, tLCAddressSchema);
		tLPAddressSchema.setEdorNo(edorNo);
		tLPAddressSchema.setEdorType(edorType);
		tLPAddressSchema.setOperator(tG.Operator);
		tLPAddressSchema.setMakeDate(PubFun.getCurrentDate());
		tLPAddressSchema.setMakeTime(PubFun.getCurrentTime());
		tLPAddressSchema.setModifyDate(PubFun.getCurrentDate());
		tLPAddressSchema.setModifyTime(PubFun.getCurrentTime());
		LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
		LPInsuredBL tLPInsuredBL = new LPInsuredBL();
		tLPInsuredBL.setEdorNo(edorNo);
		tLPInsuredBL.setContNo(this.contNo);
		tLPInsuredBL.setInsuredNo(CustomerNo);
		tLPInsuredBL.queryLPInsured(tLPInsuredBL.getSchema());
		if (tLPInsuredBL.mErrors.needDealError()) {
			// @@错误处理
			CError.buildErr(this, "查询投保人信息失败！");
			return false;
		} else {
			tLPInsuredSchema.setSchema(tLPInsuredBL.getSchema());
		}
		tLPInsuredSchema.setEdorNo(edorNo);
		tLPInsuredSchema.setEdorType(edorType);
		tLPInsuredSchema.setAddressNo(tLCAddressSchema.getAddressNo());
		aMMap.put(tLPInsuredSchema, "DELETE&INSERT");
		aMMap.put(tLPAddressSchema, "DELETE&INSERT");
		boolean flag = submit(aMMap);
		return flag;
	}

	/**
	 * P表和C表对换数据
	 * 
	 * @param edorNo
	 * @return
	 */
	private boolean addEndorData2(String edorNo) {

		Reflections tRef = new Reflections();
		String edorType = "AD";// 联系地址变更
		MMap mMap = new MMap();
		// 得到当前保单的被保人资料
		LCInsuredSchema aLCInsuredSchema = new LCInsuredSchema();
		LPInsuredSchema aLPInsuredSchema = new LPInsuredSchema();

		LPInsuredSet tLPInsuredSet = new LPInsuredSet();

		LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
		tLPInsuredSchema.setEdorNo(edorNo);
		tLPInsuredSchema.setEdorType(edorType);

		LPInsuredDB tLPInsuredDB = new LPInsuredDB();
		tLPInsuredDB.setSchema(tLPInsuredSchema);
		tLPInsuredSet = tLPInsuredDB.query();
		for (int j = 1; j <= tLPInsuredSet.size(); j++) {
			aLPInsuredSchema = tLPInsuredSet.get(j);
			LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
			tLPInsuredSchema = new LPInsuredSchema();

			LCInsuredDB aLCInsuredDB = new LCInsuredDB();
			aLCInsuredDB.setContNo(aLPInsuredSchema.getContNo());
			aLCInsuredDB.setInsuredNo(aLPInsuredSchema.getInsuredNo());
			if (!aLCInsuredDB.getInfo()) {
				mErrors.copyAllErrors(aLCInsuredDB.mErrors);
				mErrors.addOneError(new CError("查询投保人信息失败！"));
				return false;
			}
			aLCInsuredSchema = aLCInsuredDB.getSchema();
			tRef.transFields(tLPInsuredSchema, aLCInsuredSchema);
			tLPInsuredSchema.setEdorNo(aLPInsuredSchema.getEdorNo());
			tLPInsuredSchema.setEdorType(aLPInsuredSchema.getEdorType());

			// 转换成保单个人信息。
			tRef.transFields(tLCInsuredSchema, aLPInsuredSchema);
			tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
			tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());

			mMap.put(tLCInsuredSchema, "UPDATE");
		}

		// 得到当前保单的投保人资料
		LPAddressSchema aLPAddressSchema = new LPAddressSchema();
		LCAddressSet tLCAddressSet = new LCAddressSet();
		LPAddressSet tLPAddressSet = new LPAddressSet();

		LPAddressSchema tLPAddressSchema = new LPAddressSchema();
		tLPAddressSchema.setEdorNo(edorNo);
		tLPAddressSchema.setEdorType(edorType);
		tLPAddressSchema.setCustomerNo(tLPInsuredSchema.getInsuredNo());

		LPAddressDB tLPAddressDB = new LPAddressDB();
		tLPAddressDB.setSchema(tLPAddressSchema);
		tLPAddressSet = tLPAddressDB.query();
		for (int j = 1; j <= tLPAddressSet.size(); j++) {
			aLPAddressSchema = tLPAddressSet.get(j);
			LCAddressSchema tLCAddressSchema = new LCAddressSchema();
			tLPAddressSchema = new LPAddressSchema();

			LCAddressDB aLCAddressDB = new LCAddressDB();
			aLCAddressDB.setCustomerNo(aLPAddressSchema.getCustomerNo());
			aLCAddressDB.setAddressNo(aLPAddressSchema.getAddressNo());
			if (!aLCAddressDB.getInfo()) {
				if (aLCAddressDB.mErrors.needDealError()) {
					mErrors.copyAllErrors(aLCAddressDB.mErrors);
					mErrors.addOneError(new CError("查询投保人地址信息失败！"));
					return false;
				}
				// 转换成保单个人信息。
				tRef.transFields(tLCAddressSchema, aLPAddressSchema);
				tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
				tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());
				tLCAddressSet.add(tLCAddressSchema);
			}
		}

		mMap.put(tLPInsuredSchema, "UPDATE");
		mMap.put(tLCAddressSet, "INSERT");
		boolean flag = submit(mMap);
		return flag;
	}

	/**
	 * 删除P表数据
	 * 
	 */
	public void deleteEndorData1(String edorNo, String customerno) {
		String aEdorType = "AD";
		LPInsuredDB tLPInsuredDB = new LPInsuredDB();
		tLPInsuredDB.setEdorNo(edorNo);
		tLPInsuredDB.setEdorType(aEdorType);
		tLPInsuredDB.setContNo(this.contNo);
		LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();

		LPAddressDB tLPAddressDB = new LPAddressDB();
		tLPAddressDB.setEdorNo(edorNo);
		tLPAddressDB.setEdorType(aEdorType);
		tLPAddressDB.setCustomerNo(customerno);
		LPAddressSet tLPAddressSet = tLPAddressDB.query();

		MMap mMap = new MMap();
		mMap.put(tLPInsuredSet, "DELETE");
		mMap.put(tLPAddressSet, "DELETE");
		boolean flag = submit(mMap);
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
	
	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("ECCONTENDOR", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}
}
