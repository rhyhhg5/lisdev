package com.ecwebservice.subinterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.OMNamespaceImpl;

import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.db.LMRiskInsuAccDB;
import com.sinosoft.lis.db.LMRiskInterestDB;
import com.sinosoft.lis.db.LMRiskToAccDB;
import com.sinosoft.lis.finfee.TempFeeUI;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpFeeSchema;
import com.sinosoft.lis.schema.LCGrpInterestSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LMRiskInterestSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.tb.GroupContDeleteUI;
import com.sinosoft.lis.tb.GrpPubAccBL;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.tb.ParseGuideIn;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.vschema.LCGrpInterestSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDPromiseRateSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.lis.vschema.LMRiskInterestSet;
import com.sinosoft.lis.vschema.LMRiskToAccSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealCDGrpContData {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	
	private String branchcode = "";// 网点
	
	private String sendoperator = "";// 操作员

	private String messageType = "";// 报文类型

	private OMElement responseOME = null;
	
	private String mPrtNo = null;
	private String mProposalGrpContNo = null;
	
	private MMap tMMap = new MMap();
	
	private String theCurrentDate = PubFun.getCurrentDate();
	private String theCurrentTime = PubFun.getCurrentTime();
	
	private LoginVerifyTool mTool = new LoginVerifyTool();
	//保单信息
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

	//投保单位信息
	private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();
	private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();
	private LDGrpSchema mLDGrpSchema = new LDGrpSchema();
	
	//保障计划
	LCContPlanSet mLCContPlanSet = new LCContPlanSet();
	LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
	LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
	
	//团体账户信息
	LCContPlanDutyParamSet mLCContPlanDutyParamSet2 = new LCContPlanDutyParamSet();
	LCContPlanRiskSchema mLCContPlanRiskSchema = new LCContPlanRiskSchema();
	LCContPlanSchema mLCContPlanSchema = new LCContPlanSchema();
	LMRiskFeeSet mLMRiskFeeSet = new LMRiskFeeSet();
	LCGrpInterestSet mLCGrpInterestSet = new LCGrpInterestSet();
	LCInsuredListSet mLCInsuredListSet2 = new LCInsuredListSet();
	LCGrpFeeSet mLCGrpFeeSet = new LCGrpFeeSet();
	String ClaimNum ="";
	LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
	LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
	LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
	String payMoney = "";
	double PayMoney = 0;
	boolean ldGrpFlag = false;
	String message = "";
	String result;
	boolean customerflag = false;
	
	//被保人
	LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();
	
	Reflections tReflections = new Reflections();
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildResponseOME("01","01", "加载团单数据失败!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "加载团单数据失败!");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			return responseOME;
		}
		if(!deal()){
			return responseOME;
		}
		if(!save(tMMap)){
			return responseOME;
		}
		if(!calPrem()){
			delete();
			return responseOME;
		}
		if(!creatMission()){
			delete();
			return responseOME;
		}
		if(!createFee()){
			delete();
			return responseOME;
		}
		if(!signCount()){
			return responseOME;
		}
		if(!print()){
			return responseOME;
		}
		
		responseOME = buildResponseOME("00","01", "");
		System.out.println("Create OK");
		return responseOME;
	}
	
	//加载卡激活信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("CDGRPCONTDATA")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDDATE")){
							this.sendDate=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDTIME")){
							this.sendTime= child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDOPERATOR")){
							this.mLCGrpContSchema.setInputOperator(child_element.getText());
							mGlobalInput.Operator = child_element.getText();
							this.sendoperator = child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("MESSAGETYPE")){
							this.messageType=child_element.getText();
							this.messageType="cd";
							continue;
						}
						if(child_element.getLocalName().equals("GRPCONTLIST")){
							Iterator childItem = child_element.getChildren();
							while(childItem.hasNext()){
								OMElement childOME = (OMElement) childItem.next();
								if(childOME.getLocalName().equals("ITEM")){
									Iterator child1OME = childOME.getChildren();
									while(child1OME.hasNext()){
										OMElement childleaf = (OMElement) child1OME.next();
										if(childleaf.getLocalName().equals("PrtNo")){
											this.mLCGrpContSchema.setPrtNo(childleaf.getText());
											this.mLCGrpAppntSchema.setPrtNo(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("ManageCom")){
											this.mLCGrpContSchema.setManageCom(childleaf.getText());
											mGlobalInput.ManageCom = childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("CardFlag")){
											this.mLCGrpContSchema.setCardFlag(childleaf.getText());
										}
										if(childleaf.getLocalName().equals("SaleChnl")){
											this.mLCGrpContSchema.setSaleChnl(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("HandlerDate")){
											this.mLCGrpContSchema.setHandlerDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentCom")){
											this.mLCGrpContSchema.setAgentCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentCode")){
											if(!AgentCode(childleaf.getText(), "N")){
												System.out.println(getMessage());
											}
											this.mLCGrpContSchema.setAgentCode(getResult());
											continue;
										}
										if(childleaf.getLocalName().equals("FirstTrialOperator")){
											this.mLCGrpContSchema.setFirstTrialOperator(childleaf.getText());
											
											continue;
										}
										if(childleaf.getLocalName().equals("MarketType")){
											this.mLCGrpContSchema.setMarketType(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("ReceiveDate")){
											this.mLCGrpContSchema.setReceiveDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CoInsuranceFlag")){
											this.mLCGrpContSchema.setCoInsuranceFlag(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PayMode")){
											this.mLCGrpContSchema.setPayMode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("BankCode")){
											this.mLCGrpContSchema.setBankCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("BankAccNo")){
											this.mLCGrpContSchema.setBankAccNo(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AccName")){
											this.mLCGrpContSchema.setAccName(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PayIntv")){
											this.mLCGrpContSchema.setPayIntv(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CValiDate")){
											this.mLCGrpContSchema.setCValiDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CInValiDate")){
											this.mLCGrpContSchema.setCInValiDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("Remark")){
											this.mLCGrpContSchema.setRemark(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentSaleCode")){
											this.mLCGrpContSchema.setAgentSaleCode(childleaf.getText());
											continue;
										}
									}
								}
							}
						}
						
						if(child_element.getLocalName().equals("GRPAPPNTLIST")){
							Iterator child_appnt = child_element.getChildren();
							while(child_appnt.hasNext()){
								OMElement appnt_item = (OMElement) child_appnt.next();
								if(appnt_item.getLocalName().equals("ITEM")){
									Iterator appnt_element= appnt_item.getChildren();
									while(appnt_element.hasNext()){
										OMElement appntleaf = (OMElement)appnt_element.next();
										
										if(appntleaf.getLocalName().equals("GrpNo")){
//											this.mLCGrpContSchema.setAppntNo(appntleaf.getText());
//											this.mLCGrpAppntSchema.setCustomerNo(appntleaf.getText());
//											this.mLDGrpSchema.setCustomerNo(appntleaf.getText());
//											this.mLCGrpAddressSchema.setCustomerNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpName")){
//											String tLimit = "SN";
//									        String GrpNo = PubFun1.CreateMaxNo("GRPNO", tLimit);
//											this.mLCGrpContSchema.setAppntNo(GrpNo);
//											this.mLCGrpAppntSchema.setCustomerNo(GrpNo);
//											this.mLDGrpSchema.setCustomerNo(GrpNo);
//											this.mLCGrpAddressSchema.setCustomerNo(GrpNo);
											this.mLCGrpContSchema.setGrpName(appntleaf.getText());
											this.mLCGrpAppntSchema.setName(appntleaf.getText());
											this.mLDGrpSchema.setGrpName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Phone")){
											this.mLCGrpContSchema.setPhone(appntleaf.getText());
											this.mLCGrpAppntSchema.setPhone(appntleaf.getText());
											this.mLDGrpSchema.setPhone(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Phone1")){
											this.mLCGrpAddressSchema.setPhone1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OrgancomCode")){
											this.mLCGrpAppntSchema.setOrganComCode(appntleaf.getText());
											this.mLDGrpSchema.setOrganComCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("UnifiedSocialCreditNo")){
											this.mLDGrpSchema.setUnifiedSocialCreditNo(appntleaf.getText());
											this.mLCGrpAppntSchema.setUnifiedSocialCreditNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("PostalProvince")){
											this.mLCGrpAddressSchema.setPostalProvince(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("PostalCity")){
											this.mLCGrpAddressSchema.setPostalCity(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("PostalCounty")){
											this.mLCGrpAddressSchema.setPostalCounty(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpAddress")){
											this.mLCGrpAddressSchema.setDetailAddress(appntleaf.getText());
//											this.mLCGrpAppntSchema.setPostalAddress(appntleaf.getText());
//											this.mLCGrpAddressSchema.setGrpAddress(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpZipCode")){
											this.mLCGrpAppntSchema.setZipCode(appntleaf.getText());
											this.mLCGrpAddressSchema.setGrpZipCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("TaxNo")){
											this.mLCGrpAppntSchema.setTaxNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("InsuredProperty")){
											this.mLDGrpSchema.setInsuredProperty(appntleaf.getText());
											this.mLCGrpAppntSchema.setInsuredProperty(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LinkMan1")){
											this.mLCGrpAddressSchema.setLinkMan1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDType")){
											this.mLCGrpAppntSchema.setIDType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDNo")){
											this.mLCGrpAppntSchema.setIDNo(appntleaf.getText());
											continue;
										}
//										if(appntleaf.getLocalName().equals("IDStartDate")){
//											this.mLCGrpAppntSchema.setIDStartDate(appntleaf.getText());
//											continue;
//										}
//										if(appntleaf.getLocalName().equals("IDEndDate")){
//											this.mLCGrpAppntSchema.setIDEndDate(appntleaf.getText());
//											continue;
//										}
//										if(appntleaf.getLocalName().equals("IDLongEffFlag")){
//											this.mLCGrpAppntSchema.setIDLongEffFlag(appntleaf.getText());
//											continue;
//										}
										if(appntleaf.getLocalName().equals("Sex")){
											this.mLCGrpAddressSchema.setSex(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("NativePlace")){
											this.mLCGrpAddressSchema.setNativePlace(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OccupationCode")){
											this.mLCGrpAddressSchema.setOccupationCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OccupationtType")){
											this.mLCGrpAddressSchema.setOccupationType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderName")){
											this.mLCGrpAppntSchema.setShareholderName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderIDType")){
											this.mLCGrpAppntSchema.setShareholderIDType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderIDNo")){
											this.mLCGrpAppntSchema.setShareholderIDNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderIDStart")){
											this.mLCGrpAppntSchema.setShareholderIDStart(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderIDEnd")){
											this.mLCGrpAppntSchema.setShareholderIDEnd(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonName1")){
											this.mLCGrpAppntSchema.setLegalPersonName1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDType1")){
											this.mLCGrpAppntSchema.setLegalPersonIDType1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDNo1")){
											this.mLCGrpAppntSchema.setLegalPersonIDNo1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDStart1")){
											this.mLCGrpAppntSchema.setLegalPersonIDStart1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDEnd1")){
											this.mLCGrpAppntSchema.setLegalPersonIDEnd1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleName")){
											this.mLCGrpAppntSchema.setResponsibleName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDType")){
											this.mLCGrpAppntSchema.setResponsibleIDType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDNo")){
											this.mLCGrpAppntSchema.setResponsibleIDNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDStart")){
											this.mLCGrpAppntSchema.setResponsibleIDStart(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDEnd")){
											this.mLCGrpAppntSchema.setResponsibleIDEnd(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDLongFlag")){
											this.mLCGrpAppntSchema.setResponsibleIDLongFlag(appntleaf.getText());
											continue;
										}

										
										if(appntleaf.getLocalName().equals("Fax1")){
											this.mLCGrpAddressSchema.setFax1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("E_Mail1")){
											this.mLCGrpAddressSchema.setE_Mail1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BusinessType")){
											this.mLCGrpContSchema.setBusinessType(appntleaf.getText());
											this.mLDGrpSchema.setBusinessType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BusinessBigType")){
											this.mLCGrpContSchema.setBusinessBigType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpNature")){
											this.mLCGrpContSchema.setGrpNature(appntleaf.getText());
											this.mLDGrpSchema.setGrpNature(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Peoples")){
											this.mLCGrpContSchema.setPeoples(appntleaf.getText());
											this.mLCGrpAppntSchema.setPeoples(appntleaf.getText());
											this.mLDGrpSchema.setPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("AppntOnWorkPeoples")){
											this.mLCGrpAppntSchema.setOnWorkPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOnWorkPeoples(appntleaf.getText());
											continue;
										}										
										if(appntleaf.getLocalName().equals("AppntOffWorkPeoples")){
											this.mLCGrpAppntSchema.setOffWorkPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOffWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("AppntOtherPeoples")){
											this.mLCGrpAppntSchema.setOtherPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOtherPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Peoples3")){
											this.mLCGrpContSchema.setPeoples3(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OnWorkPeoples")){
											this.mLCGrpContSchema.setOnWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OffWorkPeoples")){
											this.mLCGrpContSchema.setOffWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OtherPeoples")){
											this.mLCGrpContSchema.setOtherPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaPeoples")){
											this.mLCGrpContSchema.setRelaPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaMatePeoples")){
											this.mLCGrpContSchema.setRelaMatePeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaYoungPeoples")){
											this.mLCGrpContSchema.setRelaYoungPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Authorization")){
											this.mLCGrpAppntSchema.setAuthorization(appntleaf.getText());
											continue;
										}
									}
								}
							}
						}
						//保障计划
						if(child_element.getLocalName().equals("CONTPLAN")){
							Iterator child_contplan = child_element.getChildren();
							while(child_contplan.hasNext()){
								OMElement contplan_item = (OMElement) child_contplan.next();
								if(contplan_item.getLocalName().equals("ITEM")){
									Iterator contplan_element = contplan_item.getChildren();
									LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
									LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
									while(contplan_element.hasNext()){
										OMElement contplanleaf = (OMElement)contplan_element.next();
										if(contplanleaf.getLocalName().equals("ContPlanCode")){
											tLCContPlanRiskSchema.setContPlanCode(contplanleaf.getText());
											tLCContPlanSchema.setContPlanCode(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("ContPlanName")){
											tLCContPlanRiskSchema.setContPlanName(contplanleaf.getText());
											tLCContPlanSchema.setContPlanName(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("Peoples3")){
											tLCContPlanSchema.setPeoples3(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("Peoples2")){
											tLCContPlanSchema.setPeoples2(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("RiskCode")){
											tLCContPlanRiskSchema.setRiskCode(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("MainRiskCode")){
											tLCContPlanRiskSchema.setMainRiskCode(contplanleaf.getText());
											continue;
										}
									}
									this.mLCContPlanSet.add(tLCContPlanSchema);
									this.mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
								}
							}
						}
						
						//保障计划要素
						if(child_element.getLocalName().equals("CONTPLANDUTYPARAM")){
							Iterator child_contplanparam = child_element.getChildren();
							while(child_contplanparam.hasNext()){
								OMElement contplanparam_item = (OMElement) child_contplanparam.next();
								LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
								if(contplanparam_item.getLocalName().equals("ITEM")){
									Iterator contplanparam_element = contplanparam_item.getChildren();
									while(contplanparam_element.hasNext()){
										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
										if(contplanparamleaf.getLocalName().equals("ContPlanCode")){
											tLCContPlanDutyParamSchema.setContPlanCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("ContPlanName")){
											tLCContPlanDutyParamSchema.setContPlanName(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("RiskCode")){
											tLCContPlanDutyParamSchema.setRiskCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("MainRiskCode")){
											tLCContPlanDutyParamSchema.setMainRiskCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("DutyCode")){
											tLCContPlanDutyParamSchema.setDutyCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactor")){
											if("AccGetRateP".equals(contplanparamleaf.getText()) || "AccGetRateG".equals(contplanparamleaf.getText())){
												tLCContPlanDutyParamSchema.setCalFactor("AccGetRate");
											}else{
												tLCContPlanDutyParamSchema.setCalFactor(contplanparamleaf.getText());
											}
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorType")){
											tLCContPlanDutyParamSchema.setCalFactorType(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorValue")){
											tLCContPlanDutyParamSchema.setCalFactorValue(contplanparamleaf.getText());
											continue;
										}
									}
									this.mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
								}
							}
						}
						
						if(child_element.getLocalName().equals("INSULIST")){
							Iterator child_insu = child_element.getChildren();
							while(child_insu.hasNext()){
								OMElement insu_item = (OMElement) child_insu.next();
								LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
								if(insu_item.getLocalName().equals("ITEM")){
									Iterator insu_element = insu_item.getChildren();
									while(insu_element.hasNext()){
										OMElement insuleaf = (OMElement)insu_element.next();
										if(insuleaf.getLocalName().equals("InsuredID")){
											tLCInsuredListSchema.setInsuredID(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("ContID")){
											tLCInsuredListSchema.setContNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Retire")){
											tLCInsuredListSchema.setRetire(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("RelationToInsured")){
											tLCInsuredListSchema.setRelation(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("MainInsuredName")){
											tLCInsuredListSchema.setEmployeeName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("InsuredName")){
											tLCInsuredListSchema.setInsuredName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Sex")){
											tLCInsuredListSchema.setSex(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Birthday")){
											tLCInsuredListSchema.setBirthday(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDType")){
											tLCInsuredListSchema.setIDType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDNo")){
											tLCInsuredListSchema.setIDNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("ContPlanCode")){
											tLCInsuredListSchema.setContPlanCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("OccupationType")){
											tLCInsuredListSchema.setOccupationType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("OccupationCode")){
//											tLCInsuredListSchema.setocc(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("BankCode")){
											tLCInsuredListSchema.setBankCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("BankAccNo")){
											tLCInsuredListSchema.setBankAccNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("AccName")){
											tLCInsuredListSchema.setAccName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Phone")){
											tLCInsuredListSchema.setPhone(insuleaf.getText());
											continue;
										}
									}
									this.mLCInsuredListSet.add(tLCInsuredListSchema);
								}
							}
						}
						//团体账户信息
						if(child_element.getLocalName().equals("GRPACCOUNT")){
							Iterator child_grpaccount = child_element.getChildren();
							while(child_grpaccount.hasNext()){
								OMElement contplanparam_item = (OMElement) child_grpaccount.next();
								LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
								if(contplanparam_item.getLocalName().equals("ITEM")){
									Iterator contplanparam_element = contplanparam_item.getChildren();
									while(contplanparam_element.hasNext()){
										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
										if(contplanparamleaf.getLocalName().equals("RiskCode")){
											tLCContPlanDutyParamSchema.setRiskCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("InsuAccNo")){
											tLCContPlanDutyParamSchema.setInsuAccNo(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactor")){
											tLCContPlanDutyParamSchema.setCalFactor(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorType")){
											tLCContPlanDutyParamSchema.setCalFactorType(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorValue")){
											tLCContPlanDutyParamSchema.setCalFactorValue(contplanparamleaf.getText());
											continue;
										}
									}
									this.mLCContPlanDutyParamSet2.add(tLCContPlanDutyParamSchema);
								}
							}
						}
						//管理费信息
						if(child_element.getLocalName().equals("ManageFee")){
							Iterator child_managefee = child_element.getChildren();
							while(child_managefee.hasNext()){
								OMElement contplanparam_item = (OMElement) child_managefee.next();
								if(contplanparam_item.getLocalName().equals("ITEM")){
									Iterator contplanparam_element = contplanparam_item.getChildren();
									while(contplanparam_element.hasNext()){
										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
										if(contplanparamleaf.getLocalName().equals("AccManageFee")){
											LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
											tLCGrpFeeSchema.setFeeValue(contplanparamleaf.getText());
											tLCGrpFeeSchema.setFeeCode("000002");
											this.mLCGrpFeeSet.add(tLCGrpFeeSchema);
//											tLCGrpFeeSchema.setInsuAccNo("1000001");
//											tLCGrpFeeSchema.setPayPlanCode("652102");
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("AccReliefFee")){
											LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
											tLCGrpFeeSchema.setFeeValue(contplanparamleaf.getText());
											tLCGrpFeeSchema.setFeeCode("000006");
											this.mLCGrpFeeSet.add(tLCGrpFeeSchema);
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("FixAccManageFee")){
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("ClaimNum")){
											ClaimNum=contplanparamleaf.getText();
											continue;
										}
									}
								}
							}
						}
						//收费信息
						if(child_element.getLocalName().equals("PayInfo")){
							Iterator child_managefee = child_element.getChildren();
							while(child_managefee.hasNext()){
								OMElement contplanparam_item = (OMElement) child_managefee.next();
								if(contplanparam_item.getLocalName().equals("ITEM")){
									Iterator contplanparam_element = contplanparam_item.getChildren();
									while(contplanparam_element.hasNext()){
										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
										if(contplanparamleaf.getLocalName().equals("PayMode")){
											mLJTempFeeClassSchema.setPayMode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("PayMoney")){
											mLJTempFeeClassSchema.setPayMoney(contplanparamleaf.getText());
											payMoney = contplanparamleaf.getText();
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("PayEntAccDate")){
											mLJTempFeeClassSchema.setEnterAccDate(contplanparamleaf.getText());
											continue;
										}
									}
								}
							}
						}
						
						
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验
	 * @return
	 */
	private boolean check(){
		if(!checkInfo()){
			System.out.println("校验基本信息失败");
			return false;
		}
		//处理团体投保客户，是否需要生成新客户
		if(!checkGrpName()){
			System.out.println("客户信息生成失败");
			return false;
		}
		
		return true;
	}
	
	/**
	 * 处理
	 * @return
	 */ 
	private boolean deal(){
		try{
	        mProposalGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo",mLCGrpAppntSchema.getCustomerNo());
	        if("".equals(mProposalGrpContNo) || mProposalGrpContNo == null){
	        	responseOME = buildResponseOME("01","11", "生成保单号码失败!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","11", "WX", "生成保单号码失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
	        }
	        if(!creatGrpCont()){
	        	delete();
				return false;
	        }
	        if(!creatGrpPol()){
	        	delete();
	        	return false;
	        }
	        if(!creatPubAccContPlan()){
	        	delete();
	        	return false;
	        }
	        if(!createManageFee()){
	        	delete();
	        	return false;
	        }
	        if(!creatContPlan()){
	        	delete();
	        	return false;
	        }
	        if(!creatInsured()){
	        	delete();
	        	return false;
	        }
		}catch(Exception e){
			System.out.println("处理团单电子商务复杂产品异常！");
			e.printStackTrace();
			return false;
		}
									
		return true;
	}
	
	/**
	 *校验基本信息 
	 * @return
	 */
	private boolean checkInfo(){
		if(this.batchno ==null || this.batchno.equals("")){
			responseOME = buildResponseOME("01","02", "没有得到批次号!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到批次号!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if("".equals(this.mLCGrpContSchema.getPrtNo()) || this.mLCGrpContSchema.getPrtNo() == null){
			responseOME = buildResponseOME("01","03", "获取保单印刷号码失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","03", "WX", "获取保单印刷号码失败!");
			return false;
		}else{
			String sql = "select 1 from lcgrpcont where prtno='"+this.mLCGrpContSchema.getPrtNo()+"'";
			String sqlRes = new ExeSQL().getOneValue(sql);
			if (!StrTool.cTrim(sqlRes).equals("") && !StrTool.cTrim(sqlRes).equals("null")){
				responseOME = buildResponseOME("01","04", "该保单印刷号码已存在!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "该保单印刷号码已存在!");
				return false;
			}
		}
		if(this.sendDate ==null || this.sendDate.equals("") || this.sendTime ==null || this.sendTime.equals("")){
			responseOME = buildResponseOME("01","02", "获取报文发送日期或时间失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取报文发送日期或时间失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.sendoperator == null || this.sendoperator.equals("")){
			responseOME = buildResponseOME("01","02", "获取操作员失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取操作员失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getManageCom() == null || this.mLCGrpContSchema.getManageCom().equals("")){
			responseOME = buildResponseOME("01","02", "获取管理机构失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取管理机构失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getCardFlag() == null || this.mLCGrpContSchema.getCardFlag().equals("")){
			responseOME = buildResponseOME("01","02", "获取一卡通标识失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取一卡通标识失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			if(!this.mLCGrpContSchema.getCardFlag().equals("cd")){
				responseOME = buildResponseOME("01","02", "获取一卡通标识失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取一卡通标识失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getSaleChnl() == null || this.mLCGrpContSchema.getSaleChnl().equals("")){
			responseOME = buildResponseOME("01","02", "获取销售渠道失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取销售渠道失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql = "select 1 from ldcode where codetype = 'unitesalechnlsgrp' and code='"+this.mLCGrpContSchema.getSaleChnl()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取销售渠道失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取销售渠道失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}else{
				String a = this.mLCGrpContSchema.getSaleChnl();
				if("03".equals(a) || "04".equals(a) || "10".equals(a) || "15".equals(a) || "20".equals(a) || "23".equals(a) ) {
					if(this.mLCGrpContSchema.getAgentCom()==null || "".equals(this.mLCGrpContSchema.getAgentCom()) || "null".equals(this.mLCGrpContSchema.getAgentCom())){
						responseOME = buildResponseOME("01","02", "销售渠道为中介，请录入中介公司代码！");
						LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "销售渠道为中介，请录入中介公司代码！");
						System.out.println("rsponseOME:" + responseOME);
						return false;
					}
				}else{
					this.mLCGrpContSchema.setAgentCom("");
				}
			}
		}
		if(this.mLCGrpContSchema.getHandlerDate() == null || this.mLCGrpContSchema.getHandlerDate().equals("")){
			responseOME = buildResponseOME("01","02", "获取投保单填写日期失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取投保单填写日期失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getFirstTrialOperator() == null || this.mLCGrpContSchema.getFirstTrialOperator().equals("")){
			responseOME = buildResponseOME("01","02", "获取初审人员代码失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取初审人员代码失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getMarketType()==null || this.mLCGrpContSchema.getMarketType().equals("")){
			responseOME = buildResponseOME("01","02", "获取市场类型失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取市场类型失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql = "select 1 from ldcode where codetype = 'markettype' and code='"+this.mLCGrpContSchema.getMarketType()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取市场类型失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取市场类型失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getReceiveDate()==null || this.mLCGrpContSchema.getReceiveDate().equals("")){
			responseOME = buildResponseOME("01","02", "获取接收日期失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取接受日期失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getCoInsuranceFlag()==null || this.mLCGrpContSchema.getCoInsuranceFlag().equals("")){
			responseOME = buildResponseOME("01","02", "获取保单属性是否为共保保单失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取保单属性共保保单失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getPayMode()==null || this.mLCGrpContSchema.getPayMode().equals("")){
			responseOME = buildResponseOME("01","02", "获取缴费方式失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取缴费方式失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql = "select 1 from dual where '"+this.mLCGrpContSchema.getPayMode()+"' in ('1','3','4','21')";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取缴费方式失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取缴费方式失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getPayIntv()!=0 && this.mLCGrpContSchema.getPayIntv()!=1 && this.mLCGrpContSchema.getPayIntv()!=3 && this.mLCGrpContSchema.getPayIntv()!=6 && this.mLCGrpContSchema.getPayIntv()!=12){
			responseOME = buildResponseOME("01","02", "获取缴费频次失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取缴费频次失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql ="select 1 from ldcode where codetype = 'grppayintv' and code='"+this.mLCGrpContSchema.getPayIntv()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取缴费频次失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取缴费频次失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getCValiDate()==null || this.mLCGrpContSchema.getCValiDate().equals("")){
			responseOME = buildResponseOME("01","05", "获取保单生效日期失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保单生效日期失败!");
			return false;
		}
		if(this.mLCGrpContSchema.getCInValiDate()==null || this.mLCGrpContSchema.getCInValiDate().equals("")){
			responseOME = buildResponseOME("01","05", "获取保单失效日期失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保单失效日期失败!");
			return false;
		}
		//保单生效日期必须小于保单失效日期
		if(!"".equals(diffdate(this.mLCGrpContSchema.getCValiDate(),this.mLCGrpContSchema.getCInValiDate()))){
			String a = diffdate(this.mLCGrpContSchema.getCValiDate(),this.mLCGrpContSchema.getCInValiDate());
			responseOME = buildResponseOME("01","05", a);
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "保单失效日期不应在保单生效日期之前!");
			return false;
		}
//		if("".equals(this.mLCGrpContSchema.getAppntNo()) || this.mLCGrpContSchema.getAppntNo() == null){
//			responseOME = buildResponseOME("01","05", "获取投保单位客户号失败!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保单位客户号失败!");
//			return false;
//		}
		if(this.mLCGrpContSchema.getGrpName()==null || this.mLCGrpContSchema.getGrpName().equals("")){
			responseOME = buildResponseOME("01","05", "获取投保单位名称失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保单位名称失败!");
			return false;
		}
		if(this.mLCGrpAppntSchema.getPhone()==null || this.mLCGrpAppntSchema.getPhone().equals("")){
			responseOME = buildResponseOME("01","05", "获取投保单位联系电话失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保单位联系电话失败!");
			return false;
		}
		if(this.mLCGrpAppntSchema.getInsuredProperty()==null || this.mLCGrpAppntSchema.getInsuredProperty().equals("")){
			responseOME = buildResponseOME("01","05", "获取投保人属性失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保人属性失败!");
			return false;
		}else{
			if(this.mLCGrpAppntSchema.getInsuredProperty().equals("1")){//法人
				if(this.mLCGrpAppntSchema.getOrganComCode()==null || this.mLCGrpAppntSchema.getOrganComCode().equals("")){
					if(this.mLCGrpAppntSchema.getUnifiedSocialCreditNo()==null || this.mLCGrpAppntSchema.getUnifiedSocialCreditNo().equals("")){
						responseOME = buildResponseOME("01","05", "投保人属性为法人时，组织机构代码、统一社会信用代码至少录入其一!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，组织机构代码、统一社会信用代码至少录入其一!");
						return false;
					}else{
						if(this.mLCGrpAppntSchema.getUnifiedSocialCreditNo().length()!=18){
							responseOME = buildResponseOME("01","05", "统一社会信用代码不符合规定的录入长度!长度需要等于18!");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "统一社会信用代码不符合规定的录入长度!长度需要等于18!");
							return false;
						}
					}
				}else{
					if(this.mLCGrpAppntSchema.getOrganComCode().length()!=10){
						responseOME = buildResponseOME("01","05", "组织机构代码不符合规定的录入长度!长度需要等于10!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "组织机构代码不符合规定的录入长度!长度需要等于10!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getUnifiedSocialCreditNo()!=null && !this.mLCGrpAppntSchema.getUnifiedSocialCreditNo().equals("")){
						if(this.mLCGrpAppntSchema.getUnifiedSocialCreditNo().length()!=18){
							responseOME = buildResponseOME("01","05", "统一社会信用代码不符合规定的录入长度!长度需要等于18!");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "统一社会信用代码不符合规定的录入长度!长度需要等于18!");
							return false;
						}
					}
				}
				if(this.mLCGrpContSchema.getBusinessType()==null || this.mLCGrpContSchema.getBusinessType().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，行业编码不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，行业编码不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldcode where codetype = 'businesstype' and code='"+this.mLCGrpContSchema.getBusinessType()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取行业编码失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取行业编码失败!");
						return false;
					}
				}
				if(this.mLCGrpContSchema.getBusinessBigType()==null || this.mLCGrpContSchema.getBusinessBigType().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，行业性质不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，行业性质不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldcode where codetype='businesstype' and code='"+this.mLCGrpContSchema.getBusinessType()+"' and codealias='"+this.mLCGrpContSchema.getBusinessBigType()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取行业性质失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取行业性质失败!");
						return false;
					}
				}
				if(this.mLCGrpContSchema.getGrpNature()==null || this.mLCGrpContSchema.getGrpNature().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，企业类型编码不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，企业类型编码不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldcode where codetype = 'grpnature' and code='"+this.mLCGrpContSchema.getGrpNature()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取企业类型编码失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取企业类型编码失败!");
						return false;
					}
				}
				if(this.mLCGrpContSchema.getPeoples()==0){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，员工总人数不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，员工总人数不能为空!");
					return false;
				}
				if(this.mLCGrpAppntSchema.getOnWorkPeoples()==0){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，在职人数不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，在职人数不能为空!");
					return false;
				}
			}else if(this.mLCGrpAppntSchema.getInsuredProperty().equals("2")){//自然人
				if(this.mLCGrpAddressSchema.getSex()==null || this.mLCGrpAddressSchema.getSex().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为自然人时，联系人性别不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人时，联系人性别不能为空!");
					return false;
				}else{
					String sql="select 1 from ldcode where codetype = 'sex' and code='"+this.mLCGrpAddressSchema.getSex()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取联系人性别失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人性别失败!");
						return false;
					}
				}
				if(this.mLCGrpAddressSchema.getNativePlace()==null || this.mLCGrpAddressSchema.getNativePlace().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为自然人时，联系人国籍不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人时，联系人国籍不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldcode where codetype = 'nativeplace' and code='"+this.mLCGrpAddressSchema.getNativePlace()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取联系人国籍失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人国籍失败!");
						return false;
					}
				}
				if(this.mLCGrpAddressSchema.getOccupationCode()==null || this.mLCGrpAddressSchema.getOccupationCode().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为自然人时，联系人职业代码不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人时，联系人职业代码不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldoccupation where occupationcode='"+this.mLCGrpAddressSchema.getOccupationCode()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取联系人职业代码失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人职业代码失败!");
						return false;
					}
				}
				if(this.mLCGrpAddressSchema.getOccupationType()==null || this.mLCGrpAddressSchema.getOccupationType().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为自然人时，联系人职业类别不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人时，联系人职业类别不能为空!");
					return false;
				}else{
					String sql = "select 1 from LDOccupation where occupationcode='"+this.mLCGrpAddressSchema.getOccupationCode()+"' and OccupationType='"+this.mLCGrpAddressSchema.getOccupationType()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取联系人职业类别失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人职业类别失败!");
						return false;
					}
				}
			}else{
				responseOME = buildResponseOME("01","05", "获取投保人属性失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保人属性失败!");
				return false;
			}
		}
		
		if("".equals(this.mLCGrpAddressSchema.getPostalProvince()) || this.mLCGrpAddressSchema.getPostalProvince() == null
				|| "".equals(this.mLCGrpAddressSchema.getPostalCity()) || this.mLCGrpAddressSchema.getPostalCity() == null
				|| "".equals(this.mLCGrpAddressSchema.getPostalCounty()) || this.mLCGrpAddressSchema.getPostalCounty() == null
				|| "".equals(this.mLCGrpAddressSchema.getDetailAddress()) || this.mLCGrpAddressSchema.getDetailAddress() == null){
			responseOME = buildResponseOME("01","05", "获取团体基本资料中的省，市，县和详细地址失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体基本资料中的省，市，县和详细地址失败!");
			return false;
		}else{
			//校验省和市是否与匹配
			String provinceID = this.mLCGrpAddressSchema.getPostalProvince();
			String cityID = this.mLCGrpAddressSchema.getPostalCity();
			String countyID = this.mLCGrpAddressSchema.getPostalCounty();
			String strSql = "select code from ldcode1 where codetype='province1' and code = '" + provinceID + "'";
			String strSql2 = "select code1 from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
			String strSql3 = "select code from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
			String strSql4 = "select code1 from ldcode1 where codetype='county1' and code = '" + countyID  + "'";
			SSRS arrResult = new ExeSQL().execSQL(strSql);
			SSRS arrResult2 = new ExeSQL().execSQL(strSql2);
			SSRS arrResult3 = new ExeSQL().execSQL(strSql3);
			SSRS arrResult4 = new ExeSQL().execSQL(strSql4);
			
			if ("710000".equals(provinceID) || "810000".equals(provinceID) || "820000".equals(provinceID)||"990000".equals(provinceID)) {
				if(!"000000".equals(cityID)){
					responseOME = buildResponseOME("01","05", "省和市不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "省和市不匹配!");
					return false;
				}
				if(!"000000".equals(countyID)){
					responseOME = buildResponseOME("01","05", "市和县不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "市和县不匹配!");
					return false;
				}
			}else if (("620000".equals(provinceID)&&"620200".equals(cityID)) || ("440000".equals(provinceID)&&("442000".equals(cityID)||"441900".equals(cityID))) ) {
				if(!"000000".equals(countyID)){
					responseOME = buildResponseOME("01","05", "市和县不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "市和县不匹配!");
					return false;
				}
			}else{
				if(arrResult!=null && arrResult.getMaxRow()>0 &&
						arrResult2!=null && arrResult2.getMaxRow()>0 &&
						arrResult3!=null && arrResult3.getMaxRow()>0 &&
						arrResult4!=null && arrResult4.getMaxRow()>0){
		 		if (!arrResult.GetText(1, 1).equals(arrResult2.GetText(1, 1))) {
		 			responseOME = buildResponseOME("01","05", "省和市不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "省和市不匹配!");
					return false;
		 		}
		 		if (!arrResult3.GetText(1, 1).equals(arrResult4.GetText(1, 1))) {
		 			responseOME = buildResponseOME("01","05", "市和县不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "市和县不匹配!");
					return false;
		 		}}else{
		 			responseOME = buildResponseOME("01","05", "获取省、市、县编码失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取省、市、县编码失败!");
					return false;
		 		}
			}
			
			String sql1 = "select CodeName from LDCode1 where codetype = 'province1' and code = '"+mLCGrpAddressSchema.getPostalProvince()+"'";
			String sql2 = "select CodeName from LDCode1 where codetype = 'city1' and Code1 = '"+mLCGrpAddressSchema.getPostalProvince()+"' and code ='"+mLCGrpAddressSchema.getPostalCity()+"' ";
			String sql3 = "select CodeName from LDCode1 where codetype = 'county1' and Code1 = '"+mLCGrpAddressSchema.getPostalCity()+"' and code='"+mLCGrpAddressSchema.getPostalCounty()+"'";
			String grpAddress = new ExeSQL().getOneValue(sql1) + new ExeSQL().getOneValue(sql2) + new ExeSQL().getOneValue(sql3) + this.mLCGrpAddressSchema.getDetailAddress();
			this.mLCGrpAppntSchema.setPostalAddress(grpAddress);
			this.mLCGrpAddressSchema.setGrpAddress(grpAddress);
		}

		if(this.mLCGrpAddressSchema.getGrpZipCode()==null || this.mLCGrpAddressSchema.getGrpZipCode().equals("")){
			responseOME = buildResponseOME("01","05", "获取邮政编码失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取邮政编码失败!");
			return false;
		}
		
		if(this.mLCGrpAddressSchema.getLinkMan1()==null || this.mLCGrpAddressSchema.getLinkMan1().equals("")){
			responseOME = buildResponseOME("01","05", "获取联系人姓名失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人姓名失败!");
			return false;
		}
		if(this.mLCGrpAddressSchema.getPhone1()==null || this.mLCGrpAddressSchema.getPhone1().equals("")){
			responseOME = buildResponseOME("01","05", "获取联系人联系电话失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人联系电话失败!");
			return false;
		}
		if(this.mLCGrpAppntSchema.getIDType()==null || this.mLCGrpAppntSchema.getIDType().equals("")){
			responseOME = buildResponseOME("01","05", "获取联系人证件类型失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件类型失败!");
			return false;
		}else{
			if(!this.mLCGrpAppntSchema.getIDType().equals("4") && !this.mLCGrpAppntSchema.getIDType().equals("0")){
				responseOME = buildResponseOME("01","05", "获取联系人证件类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件类型失败!");
				return false;
			}
		}
		if(this.mLCGrpAppntSchema.getIDNo()==null || this.mLCGrpAppntSchema.getIDNo().equals("")){
			responseOME = buildResponseOME("01","05", "获取联系人证件号码失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件号码失败!");
			return false;
		}
//		if(this.mLCGrpAppntSchema.getIDStartDate()==null || this.mLCGrpAppntSchema.getIDStartDate().equals("")){
//			responseOME = buildResponseOME("01","05", "获取联系人证件生效日期失败!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件生效日期失败!");
//			return false;
//		}
//		if(this.mLCGrpAppntSchema.getIDLongEffFlag()==null || this.mLCGrpAppntSchema.getIDLongEffFlag().equals("")){
//			if(this.mLCGrpAppntSchema.getIDEndDate()==null || this.mLCGrpAppntSchema.getIDEndDate().equals("")){
//				responseOME = buildResponseOME("01","05", "获取联系人证件失效日期失败，当证件长期有效标志为空时，证件失效日期为必录项!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件失效日期失败，当证件长期有效标志为空时，证件失效日期为必录项!");
//				return false;
//			}
//		}else{
//			if(!"Y".equals(this.mLCGrpAppntSchema.getIDLongEffFlag())){
//				responseOME = buildResponseOME("01","05", "获取联系人证件长期有效标志失败!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件长期有效标志失败!");
//				return false;
//			}
//		}
		if(this.mLCGrpContSchema.getPeoples3()==0){
			responseOME = buildResponseOME("01","05", "获取被保险人数失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人数失败!");
			return false;
		}
		if(this.mLCGrpContSchema.getOnWorkPeoples()==0){
			responseOME = buildResponseOME("01","05", "获取在职人数失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取在职人数失败!");
			return false;
		}
		if(this.mLCGrpAppntSchema.getAuthorization()==null || this.mLCGrpAppntSchema.getAuthorization().equals("")){
			responseOME = buildResponseOME("01","05", "授权使用客户信息不能为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "授权使用客户信息不能为空!");
			return false;
		}else{
			if(!this.mLCGrpAppntSchema.getAuthorization().equals("1")){
				responseOME = buildResponseOME("01","05", "授权使用客户信息只能为“是”!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "授权使用客户信息只能为“是”!");
				return false;
			}
		}
		for(int i=1;i<=this.mLCContPlanSet.size();i++){
			if(mLCContPlanSet.get(i).getContPlanCode()==null || mLCContPlanSet.get(i).getContPlanCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取保障计划代码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保障计划代码失败!");
				return false;
			}
			if(mLCContPlanSet.get(i).getContPlanName()==null || mLCContPlanSet.get(i).getContPlanName().equals("")){
				responseOME = buildResponseOME("01","05", "获取保障计划名称失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保障计划名称失败!");
				return false;
			}
			if(mLCContPlanSet.get(i).getPeoples3()==0){
				responseOME = buildResponseOME("01","05", "获取可投保人数失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取可投保人数失败!");
				return false;
			}
			if(mLCContPlanSet.get(i).getPeoples2()==0){
				responseOME = buildResponseOME("01","05", "获取参保人数失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取参保人数失败!");
				return false;
			}
			
		}
		for(int i=1;i<=this.mLCContPlanRiskSet.size();i++){
			if(mLCContPlanRiskSet.get(i).getRiskCode()==null || mLCContPlanRiskSet.get(i).getRiskCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取险种编码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取险种编码失败!");
				return false;
			}
			if(mLCContPlanRiskSet.get(i).getMainRiskCode()==null || mLCContPlanRiskSet.get(i).getMainRiskCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取主险编码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取主险编码失败!");
				return false;
			}
		}
		for(int i=1;i<=this.mLCContPlanDutyParamSet.size();i++){
			if(mLCContPlanDutyParamSet.get(i).getContPlanCode()==null || mLCContPlanDutyParamSet.get(i).getContPlanCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取保障计划代码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保障计划代码失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet.get(i).getContPlanName()==null || mLCContPlanDutyParamSet.get(i).getContPlanName().equals("")){
				responseOME = buildResponseOME("01","05", "获取保障计划名称失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保障计划名称失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet.get(i).getRiskCode()==null || mLCContPlanDutyParamSet.get(i).getRiskCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取险种编码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取险种编码失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet.get(i).getMainRiskCode()==null || mLCContPlanDutyParamSet.get(i).getMainRiskCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取主险编码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取主险编码失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet.get(i).getDutyCode()==null || mLCContPlanDutyParamSet.get(i).getDutyCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取责任编码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取责任编码失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet.get(i).getCalFactor()==null || mLCContPlanDutyParamSet.get(i).getCalFactor().equals("")){
				responseOME = buildResponseOME("01","05", "获取计划要素名称失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取计划要素名称失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet.get(i).getCalFactorType()==null || mLCContPlanDutyParamSet.get(i).getCalFactorType().equals("")){
				responseOME = buildResponseOME("01","05", "获取计划要素类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取计划要素类型失败!");
				return false;
			}
			
		}
		for(int i=1;i<=this.mLCInsuredListSet.size();i++){
			if(mLCInsuredListSet.get(i).getInsuredID()==null || mLCInsuredListSet.get(i).getInsuredID().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保人序号失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保人序号失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getContNo()==null || mLCInsuredListSet.get(i).getContNo().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保人合同序号失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保人合同序号失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getRetire()==null || mLCInsuredListSet.get(i).getRetire().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保人状态失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保人状态失败!");
				return false;
			}else{
				if(!mLCInsuredListSet.get(i).getRetire().equals("1") && !mLCInsuredListSet.get(i).equals("2")){
					responseOME = buildResponseOME("01","05", "获取被保人状态失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保人状态失败!");
					return false;
				}
			}
			if(mLCInsuredListSet.get(i).getRelation()==null || mLCInsuredListSet.get(i).getRelation().equals("")){
				responseOME = buildResponseOME("01","05", "获取员工关系失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取员工关系失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getEmployeeName()==null || mLCInsuredListSet.get(i).getEmployeeName().equals("")){
				responseOME = buildResponseOME("01","05", "获取主被保险人姓名失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取主被保险人姓名失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getInsuredName()==null || mLCInsuredListSet.get(i).getInsuredName().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人姓名失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人姓名失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getSex()==null || mLCInsuredListSet.get(i).getSex().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人性别失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人性别失败!");
				return false;
			}else{
				String sql="select 1 from ldcode where codetype = 'sex' and code='"+mLCInsuredListSet.get(i).getSex()+"'";
				SSRS tSSRS = new ExeSQL().execSQL(sql);
				if(tSSRS==null ||tSSRS.getMaxRow()<=0){
					responseOME = buildResponseOME("01","05", "获取被保险人性别失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人性别失败!");
					return false;
				}
			}
			if(mLCInsuredListSet.get(i).getBirthday()==null || mLCInsuredListSet.get(i).getBirthday().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人出生日期失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人出生日期失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getIDType()==null || mLCInsuredListSet.get(i).getIDType().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人证件类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人证件类型失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getIDNo()==null || mLCInsuredListSet.get(i).getIDNo().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人证件号码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人证件号码失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getContPlanCode()==null || mLCInsuredListSet.get(i).getContPlanCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人保障计划代码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人保障计划代码失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getContPlanCode()==null || mLCInsuredListSet.get(i).getContPlanCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人保障计划代码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人保障计划代码失败!");
				return false;
			}
		}
		for(int i=1;i<=this.mLCContPlanDutyParamSet2.size();i++){
			if(mLCContPlanDutyParamSet2.get(i).getInsuAccNo()==null || mLCContPlanDutyParamSet2.get(i).getInsuAccNo().equals("")){
				responseOME = buildResponseOME("01","05", "获取团体账户类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户类型失败!");
				return false;
			}else{
				if(!mLCContPlanDutyParamSet2.get(i).getInsuAccNo().equals("1000001") && !mLCContPlanDutyParamSet2.get(i).getInsuAccNo().equals("1000002")){
					responseOME = buildResponseOME("01","05", "获取团体账户类型失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户类型失败!");
					return false;
				}
			}
			
			if(mLCContPlanDutyParamSet2.get(i).getCalFactor()==null || mLCContPlanDutyParamSet2.get(i).getCalFactor().equals("")){
				responseOME = buildResponseOME("01","05", "获取团体账户计划要素名称失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户计划要素名称失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet2.get(i).getCalFactorType()==null || mLCContPlanDutyParamSet2.get(i).getCalFactorType().equals("")){
				responseOME = buildResponseOME("01","05", "获取团体账户计划要素类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户计划要素类型失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet2.get(i).getCalFactorValue()==null || mLCContPlanDutyParamSet2.get(i).getCalFactorValue().equals("")){
				responseOME = buildResponseOME("01","05", "获取团体账户计划要素值失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户计划要素值失败!");
				return false;
			}
		}
		if(mLCGrpFeeSet.size()!=2){
			responseOME = buildResponseOME("01","05", "获取管理费信息失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取管理费信息失败!");
			return false;
		}
		if(mLJTempFeeClassSchema.getPayMode()==null || mLJTempFeeClassSchema.getPayMode().equals("")){
			responseOME = buildResponseOME("01","05", "获取缴费方式失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取缴费方式失败!");
			return false;
		}else{
			if(!mLCGrpContSchema.getPayMode().equals(mLJTempFeeClassSchema.getPayMode())){
				responseOME = buildResponseOME("01","05", "获取缴费方式失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取缴费方式失败!");
				return false;
			}
		}
		if(mLJTempFeeClassSchema.getEnterAccDate()==null || mLJTempFeeClassSchema.getEnterAccDate().equals("")){
			responseOME = buildResponseOME("01","05", "获取到账时间失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取到账时间失败!");
			return false;
		}
		PayMoney = Double.parseDouble(payMoney);
		
		
		
		// 校验被保人身份证号   20170926 zy
		if(this.mLCInsuredListSet!=null&& mLCInsuredListSet.size() != 0){
			LCInsuredListSet tmLCInsuredListSet=this.mLCInsuredListSet;
			for(int i=1;i<=tmLCInsuredListSet.size();i++){
				String name=tmLCInsuredListSet.get(i).getInsuredName();
				String idtype=tmLCInsuredListSet.get(i).getIDType();
				String idno=tmLCInsuredListSet.get(i).getIDNo();
				String sex=tmLCInsuredListSet.get(i).getSex();
				String birthday=tmLCInsuredListSet.get(i).getBirthday();
				if(idno!=null && idno!=""){
					String strChkIdNo=PubFun.CheckIDNo(idtype, idno, birthday, sex);
					if(strChkIdNo !=""){
						responseOME = buildResponseOME("01","07", "被保人:"+name+","+strChkIdNo);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "被保人:"+name+","+strChkIdNo);
						return false;
					}
				}
			}
		}
		

		String tManageCom = this.mLCGrpContSchema.getManageCom();
		String tAgentCom = this.mLCGrpContSchema.getAgentCom();
		String tAgentCode = this.mLCGrpContSchema.getAgentCode();
		String tSaleChnl = this.mLCGrpContSchema.getSaleChnl();
		
		//校验业务员是否离职
        String tAgentState = new ExeSQL()
                    .getOneValue("SELECT agentstate FROM laagent where agentcode='"
                            + tAgentCode + "'");
        if ((tAgentState == null) || tAgentState.equals(""))
        {
            responseOME = buildResponseOME("01","07", "无法查到保单业务员[" + tAgentCode +"]的状态!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "无法查到保单业务员[" + tAgentCode +"]的状态!");
			return false;
        }
//        if (Integer.parseInt(tAgentState) >= 6)
//        {
//            responseOME = buildResponseOME("01","07", "该保单业务员[" + tAgentCode + "]已经离职!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "该保单业务员[" + tAgentCode + "]已经离职!");
//			return false;
//        }
		
		String tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
		SSRS arrCode = new ExeSQL().execSQL(tSQLCode);
		if(arrCode.getMaxRow()<1){
			responseOME = buildResponseOME("01","07", "业务员与管理机构不匹配！");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "业务员与管理机构不匹配！");
			return false;
		}
		if("02".equals(tSaleChnl) || "03".equals(tSaleChnl)){
			String agentCodeSql = " select 1 from LAAgent a where a.AgentCode = '"+ tAgentCode+ "'" 
		                 + " and a.BranchType = '2' and a.BranchType2 in ('01','02') "
		                 + " union all "
						 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
						 + " and '03' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
		    SSRS arrAgentCode = new ExeSQL().execSQL(agentCodeSql);
		    if(arrAgentCode.getMaxRow()<1){
		    	responseOME = buildResponseOME("01","07", "业务员和销售渠道不匹配！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "业务员和销售渠道不匹配！");
				return false;
		    }
		}else{
			String agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
		                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
	                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
	                     + " union all "
						 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
						 + " and '10' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
		    SSRS arrAgentCode = new ExeSQL().execSQL(agentCodeSql);
		    if(arrAgentCode.getMaxRow()<1){
		    	responseOME = buildResponseOME("01","07", "业务员和销售渠道不匹配！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "业务员和销售渠道不匹配！");
				return false;
		    }
		}
		if("03".equals(tSaleChnl) || "04".equals(tSaleChnl) || "10".equals(tSaleChnl) || "15".equals(tSaleChnl) || "20".equals(tSaleChnl) || "23".equals(tSaleChnl) ) {
			String tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
			SSRS arrCom = new ExeSQL().execSQL(tSQLCom);
			if(arrCom.getMaxRow()<1){
				responseOME = buildResponseOME("01","07", "中介机构与管理机构不匹配！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "中介机构与管理机构不匹配！");
				return false;
			}
			String tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
			SSRS arrComCode = new ExeSQL().execSQL(tSQLComCode);
			if(arrComCode.getMaxRow()<1){
				responseOME = buildResponseOME("01","07", "业务员与中介机构不匹配！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "业务员与中介机构不匹配！");
				return false;
			}
		}
		
		String tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+this.mLCGrpContSchema.getMarketType()+"' and code1 = '"+this.mLCGrpContSchema.getSaleChnl()+"' ";
		SSRS arrCheck = new ExeSQL().execSQL(tcheckSQL);
		if(arrCheck.getMaxRow()<1){
			responseOME = buildResponseOME("01","07", "市场类型和销售渠道不匹配！");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "市场类型和销售渠道不匹配！");
			return false;
		}
		
		
		
		return true;

	}
	
	//提交卡激活信息
	private boolean save(MMap mMMap){
		try{
			VData v = new VData();
			v.add(mMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				System.out.println("数据提交失败************************");
				responseOME = buildResponseOME("01","99", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","99", "WX", "数据提交失败!");
				delete();
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME("01","99", "数据提交失败!");
			System.out.println("数据提交异常");
			e.printStackTrace();
			delete();
			return false;
		}
		
		return true;
	}
	private boolean delete(){
		GlobalInput tG = new GlobalInput(); 
		tG.ComCode=mLCGrpContSchema.getManageCom();
		tG.ManageCom=mLCGrpContSchema.getManageCom();
		tG.Operator=mLCGrpContSchema.getOperator();
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		LCGrpContSet tLCGrpContSet = new LCGrpContSet();
		tLCGrpContDB.setGrpContNo(mProposalGrpContNo);
		tLCGrpContSet = tLCGrpContDB.query();
		LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);
		VData tVData = new VData();
		tVData.add( tLCGrpContSchema );
		tVData.add( tG );
		// 数据传输
		GroupContDeleteUI tGroupContDeleteUI = new GroupContDeleteUI();
		tGroupContDeleteUI.submitData(tVData,"DELETE");
		return true;
	}
	
	private OMElement buildResponseOME(String state, String errCode,String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement DATAEND = fac.createOMElement("queryDataResponse", null);
		OMNamespace ns = new OMNamespaceImpl("http://services.ecwebservice.com", "ns2");
		DATAEND.setNamespace(ns);
		OMElement DATASET = fac.createOMElement("CDGRPCONTDATA", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "PrtNo", mLCGrpContSchema.getPrtNo());
		if("00".equals(state)){
			String sql1 = "select grpcontno from lcgrpcont where prtno='"+mLCGrpContSchema.getPrtNo()+"'";
			String sqlRes1 = new ExeSQL().getOneValue(sql1);
			tool.addOm(DATASET, "GrpContNo", sqlRes1);
			tool.addOm(DATASET, "CustomerNo", this.mLDGrpSchema.getCustomerNo());
		}
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRINFO", errInfo);
			
		if("00".equals(state)){
			
//			addOm(GRPCONTITEM, "PrtNo", "18180716090");
			OMElement CONTINFO = fac.createOMElement("ContInfo", null);
			String sql2 = "select contno,insuredno,insuredname,insuredidtype,insuredidno from lccont where prtno='"+mLCGrpContSchema.getPrtNo()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql2);
			for(int i=1;i<=tSSRS.getMaxRow();i++){
				OMElement GRPCONTITEM = fac.createOMElement("ITEM", null);
				tool.addOm(GRPCONTITEM, "ContNo", tSSRS.GetText(i, 1));
				tool.addOm(GRPCONTITEM, "InsuredNo", tSSRS.GetText(i, 2));
				tool.addOm(GRPCONTITEM, "Name", tSSRS.GetText(i, 3));
				tool.addOm(GRPCONTITEM, "IDType", tSSRS.GetText(i, 4));
				tool.addOm(GRPCONTITEM, "IDNo", tSSRS.GetText(i, 5));
				CONTINFO.addChild(GRPCONTITEM);
			}
			DATASET.addChild(CONTINFO);
		}
		DATAEND.addChild(DATASET);
		return DATAEND;
	}
	
	private boolean creatGrpCont(){
		
        mLCGrpContSchema.setProposalGrpContNo(mProposalGrpContNo);
        mLCGrpContSchema.setGrpContNo(mProposalGrpContNo);
        mLCGrpAppntSchema.setGrpContNo(mProposalGrpContNo);
        if (mLCGrpContSchema.getAddressNo() == null || mLCGrpContSchema.getAddressNo().trim().equals(""))
        {
            try
            {
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(integer(AddressNo)) Is Null Then 0 Else max(integer(AddressNo)) End from LCGrpAddress where CustomerNo='"
                        + mLCGrpAppntSchema.getCustomerNo() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                String AddressNo = integer.toString();
                System.out.println("得到的地址码是：" + AddressNo);
                if (!"".equals(AddressNo))
                {
                    mLCGrpContSchema.setAddressNo(AddressNo);
                    mLCGrpAppntSchema.setAddressNo(AddressNo);
                    mLCGrpAddressSchema.setAddressNo(AddressNo);
                }
                else
                {
                	responseOME = buildResponseOME("01","13", "客户地址号码生成失败!");
    				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","13", "WX", "客户地址号码生成失败!");
    				System.out.println("responseOME:" + responseOME);
    				return false;
                }
            }
            catch (Exception e)
            {
            	responseOME = buildResponseOME("01","13", "地址码超长,生成号码失败,请先删除原来的超长地址码!!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","13", "WX", "地址码超长,生成号码失败,请先删除原来的超长地址码!!");
				System.out.println("responseOME:" + responseOME);
				return false;
            }
        }
        if (mLCGrpContSchema.getAppFlag() == null
                || mLCGrpContSchema.getAppFlag().equals(""))
        {
            mLCGrpContSchema.setAppFlag("0");
            mLCGrpContSchema.setStateFlag("0");
        }
        if (StrTool.cTrim(mLCGrpContSchema.getPolApplyDate()).equals(""))
        {
            mLCGrpContSchema.setPolApplyDate(theCurrentDate);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getInputDate()).equals(""))
        {
            mLCGrpContSchema.setInputDate(theCurrentDate);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getInputTime()).equals(""))
        {
            mLCGrpContSchema.setInputTime(theCurrentTime);
        }
        
        String tSQL = "select agentgroup from laagent where agentcode = '"+mLCGrpContSchema.getAgentCode()+"' ";
        String tAgentGroup = new ExeSQL().getOneValue(tSQL);
        
        if(tAgentGroup==null || tAgentGroup.equals("")){
        	responseOME = buildResponseOME("01","13", "获取业务员编码失败!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","13", "WX", "获取业务员编码失败!");
			System.out.println("responseOME:" + responseOME);
			return false;
        }
        
        mLCGrpContSchema.setContPrintType("2");
        mLCGrpContSchema.setPrintCount(0);
        mLCGrpContSchema.setAgentGroup(tAgentGroup);
        mLCGrpContSchema.setApproveFlag("0");
        mLCGrpContSchema.setUWFlag("0");
        mLCGrpContSchema.setSpecFlag("0");
        //记录入机日期
        mLCGrpContSchema.setMakeDate(theCurrentDate);
        mLCGrpAppntSchema.setMakeDate(theCurrentDate);
        //记录入机时间
        mLCGrpContSchema.setMakeTime(theCurrentTime);
        mLCGrpAppntSchema.setMakeTime(theCurrentTime);

        mLDGrpSchema.setMakeDate(theCurrentDate);
        mLDGrpSchema.setMakeTime(theCurrentTime);
        
        //记录入机时间
        mLCGrpAddressSchema.setMakeTime(theCurrentTime);
        mLCGrpAddressSchema.setMakeDate(theCurrentDate);
        
//      记录当前操作员
        mLCGrpContSchema.setOperator(mGlobalInput.Operator);
        mLCGrpAppntSchema.setOperator(mGlobalInput.Operator);
        mLCGrpAddressSchema.setOperator(mGlobalInput.Operator);
        mLDGrpSchema.setOperator(mGlobalInput.Operator);
        //记录最后一次修改日期
        mLCGrpContSchema.setModifyDate(theCurrentDate);
        mLCGrpAppntSchema.setModifyDate(theCurrentDate);
        mLCGrpAddressSchema.setModifyDate(theCurrentDate);
        mLDGrpSchema.setModifyDate(theCurrentDate);
        //记录最后一次修改时间
        mLCGrpContSchema.setModifyTime(theCurrentTime);
        mLCGrpAppntSchema.setModifyTime(theCurrentTime);
        mLCGrpAddressSchema.setModifyTime(theCurrentTime);
        mLDGrpSchema.setModifyTime(theCurrentTime);
        
//        /** 如果页面没有传入行业性质,就从数据库里查询出来添上 */
//        if ("1".equals(mLCGrpAppntSchema.getInsuredProperty()) && StrTool.cTrim(mLCGrpContSchema.getBusinessBigType()).equals(""))
//        {
//            String sql = "select CodeAlias from ldcode where codetype='businesstype'"
//                    + " and code='" + mLCGrpContSchema.getBusinessType() + "'";
//            String bigType = (new ExeSQL()).getOneValue(sql);
//            if (StrTool.cTrim(bigType).equals("")
//                    || StrTool.cTrim(bigType).equals("null"))
//            {
//            	responseOME = buildResponseOME("01","12", "获取行业性质失败!");
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","12", "WX", "获取行业性质失败!");
//				System.out.println("responseOME:" + responseOME);
//				return false;
//            }
//            mLCGrpContSchema.setBusinessBigType(bigType);
//        }
//        tMMap.put(mLDGrpSchema, "INSERT");
        MMap mMMap = new MMap();
        if(customerflag){
        	mMMap.put(mLDGrpSchema, "INSERT");
        }
        mMMap.put(mLCGrpAddressSchema, "INSERT");
        mMMap.put(mLCGrpContSchema, "INSERT");
        mMMap.put(mLCGrpAppntSchema, "INSERT");
        boolean b =save(mMMap);
		return b;
	}
	
	private boolean creatGrpPol(){
		
		String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        for(int i=1;i<=mLCContPlanRiskSet.size();i++){
        	String tRiskCode = mLCContPlanRiskSet.get(i).getRiskCode();
        	LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tRiskCode);
            if (tLMRiskAppDB.getInfo() == false) {
            	responseOME = buildResponseOME("01","14", "没有查到险种信息!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","14", "WX", "没有查到险种信息!");
				System.out.println("responseOME:" + responseOME);
				return false;
            }
            String tNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);
        	LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        	tLCGrpPolSchema.setGrpPolNo(tNo); //如果是新增
            tLCGrpPolSchema.setGrpProposalNo(tNo);
            tLCGrpPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
            tLCGrpPolSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLCGrpPolSchema.setSaleChnl(mLCGrpContSchema.getSaleChnl());
            tLCGrpPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
            tLCGrpPolSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
            tLCGrpPolSchema.setAgentType(mLCGrpContSchema.getAgentType());
            tLCGrpPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
            tLCGrpPolSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
            tLCGrpPolSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
            tLCGrpPolSchema.setAddressNo(mLCGrpContSchema.getAddressNo());
            tLCGrpPolSchema.setRiskCode(tRiskCode);
            tLCGrpPolSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
            tLCGrpPolSchema.setPayIntv(mLCGrpContSchema.getPayIntv());
            tLCGrpPolSchema.setPayMode(mLCGrpContSchema.getPayMode());
            tLCGrpPolSchema.setExpPeoples(mLCGrpContSchema.getExpPeoples());
            tLCGrpPolSchema.setGrpName(mLCGrpContSchema.getGrpName());
            tLCGrpPolSchema.setComFeeRate(tLMRiskAppDB.getAppInterest());
            tLCGrpPolSchema.setBranchFeeRate(tLMRiskAppDB.getAppPremRate());
            tLCGrpPolSchema.setAppFlag("0");
            tLCGrpPolSchema.setStateFlag("0");
            tLCGrpPolSchema.setUWFlag("0");
            tLCGrpPolSchema.setApproveFlag("0");
            tLCGrpPolSchema.setModifyDate(theCurrentDate);
            tLCGrpPolSchema.setModifyTime(theCurrentTime);
            tLCGrpPolSchema.setOperator(mGlobalInput.Operator);
            tLCGrpPolSchema.setMakeDate(theCurrentDate);
            tLCGrpPolSchema.setMakeTime(theCurrentTime);
            tLCGrpPolSchema.setPayMode(mLCGrpContSchema.getPayMode());
            tLCGrpPolSchema.setRiskWrapFlag("N");
            
            mLCGrpPolSet.add(tLCGrpPolSchema);
            
        }
        
        MMap mMMap = new MMap();
        mMMap.put(mLCGrpPolSet, "INSERT");
        boolean b = save(mMMap);
		return b;
	}
	
	private boolean creatContPlan(){
		
		String tContPlanCode = "";
		HashMap tContPlanCodeMap = new HashMap();
		LCContPlanSet tLCContPlanSet = new LCContPlanSet();
        for(int i=1;i<=mLCContPlanSet.size();i++){
        	for(int j=i+1;j<=mLCContPlanSet.size();j++){
        		if(mLCContPlanSet.get(i).getContPlanCode().equals(mLCContPlanSet.get(j).getContPlanCode())){
        			if(mLCContPlanSet.get(i).getPeoples2() != mLCContPlanSet.get(j).getPeoples2() || mLCContPlanSet.get(i).getPeoples3() != mLCContPlanSet.get(j).getPeoples3()){
        				responseOME = buildResponseOME("01","17", "相同保障计划中应保人数和实保人数不一致!");
        				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "相同保障计划中应保人数和实保人数不一致!");
        				System.out.println("responseOME:" + responseOME);
        				return false;
        			}
        		}
        	}
        	tContPlanCode = mLCContPlanSet.get(i).getContPlanCode();
        	if(!tContPlanCodeMap.containsKey(tContPlanCode)){
        		tContPlanCodeMap.put(tContPlanCode, "");
        		LCContPlanSchema tLCContPlanSchema = mLCContPlanSet.get(i);
        		tLCContPlanSchema.setGrpContNo(mProposalGrpContNo);
        		tLCContPlanSchema.setProposalGrpContNo(mProposalGrpContNo);
        		tLCContPlanSchema.setPlanType("0");
        		tLCContPlanSchema.setOperator(mGlobalInput.Operator);
        		tLCContPlanSchema.setMakeDate(theCurrentDate);
        		tLCContPlanSchema.setMakeTime(theCurrentTime);
        		tLCContPlanSchema.setModifyDate(theCurrentDate);
        		tLCContPlanSchema.setModifyTime(theCurrentTime);
        		tLCContPlanSet.add(tLCContPlanSchema);
        	}
        }
       
        tMMap.put(tLCContPlanSet, "INSERT");
		
        String tRiskCode = "";
		HashMap tRiskMap = new HashMap();
		LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
        for(int i=1;i<=mLCContPlanRiskSet.size();i++){
        	mLCContPlanRiskSet.get(i).setGrpContNo(mProposalGrpContNo);
        	mLCContPlanRiskSet.get(i).setProposalGrpContNo(mProposalGrpContNo);
        	mLCContPlanRiskSet.get(i).setMainRiskVersion("2002");
        	mLCContPlanRiskSet.get(i).setRiskVersion("2002");
        	mLCContPlanRiskSet.get(i).setPlanType("0");
        	mLCContPlanRiskSet.get(i).setOperator(mGlobalInput.Operator);
        	mLCContPlanRiskSet.get(i).setMakeDate(theCurrentDate);
        	mLCContPlanRiskSet.get(i).setMakeTime(theCurrentTime);
        	mLCContPlanRiskSet.get(i).setModifyDate(theCurrentDate);
        	mLCContPlanRiskSet.get(i).setModifyTime(theCurrentTime);
        	
        	
        }
        
        tMMap.put(mLCContPlanRiskSet, "INSERT");
        tMMap.put(tLCContPlanRiskSet, "INSERT");
        
        String tParamRiskCode = "";
		HashMap tParamRiskMap = new HashMap();
		LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
        for(int i=1;i<=mLCContPlanDutyParamSet.size();i++){
        	String insuAccNo = mLCContPlanDutyParamSet.get(i).getInsuAccNo();
        	if(insuAccNo!=null && insuAccNo!=""){
        		continue;
        	}
        	tParamRiskCode = mLCContPlanDutyParamSet.get(i).getRiskCode();
        	mLCContPlanDutyParamSet.get(i).setGrpContNo(mProposalGrpContNo);
        	mLCContPlanDutyParamSet.get(i).setProposalGrpContNo(mProposalGrpContNo);
        	String tGrpPolNo = "";
        	for(int j=1;j<=mLCGrpPolSet.size();j++){
            	if(mLCContPlanDutyParamSet.get(i).getRiskCode().equals(mLCGrpPolSet.get(j).getRiskCode())){
            		tGrpPolNo = mLCGrpPolSet.get(j).getGrpPolNo();
            		break;
            	}
            }
        	mLCContPlanDutyParamSet.get(i).setGrpPolNo(tGrpPolNo);
        	mLCContPlanDutyParamSet.get(i).setMainRiskVersion("2002");
        	mLCContPlanDutyParamSet.get(i).setRiskVersion("2002");
        	mLCContPlanDutyParamSet.get(i).setPlanType("0");
        	String tSQL = "select PayPlanCode,GetDutyCode,InsuAccNo from lmriskdutyfactor where riskcode = '"+mLCContPlanDutyParamSet.get(i).getRiskCode()+"' and dutycode = '"+mLCContPlanDutyParamSet.get(i).getDutyCode()+"' and chooseflag in('0','2') and factororder='"+i+"'";
        	SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        	if(tSSRS != null && tSSRS.MaxRow>0){
        		mLCContPlanDutyParamSet.get(i).setPayPlanCode(tSSRS.GetText(1, 1));
            	mLCContPlanDutyParamSet.get(i).setGetDutyCode(tSSRS.GetText(1, 2));
            	mLCContPlanDutyParamSet.get(i).setInsuAccNo(tSSRS.GetText(1, 3));
        	}else{
        		mLCContPlanDutyParamSet.get(i).setPayPlanCode("000000");
            	mLCContPlanDutyParamSet.get(i).setGetDutyCode("000000");
            	mLCContPlanDutyParamSet.get(i).setInsuAccNo("000000");
        	}
        	
        	if("DutyAmnt".equals(mLCContPlanDutyParamSet.get(i).getCalFactor()) && !"".equals(StrTool.cTrim(mLCContPlanDutyParamSet.get(i).getCalFactorValue()))){
        		String tSQL1 = "select 1 from lmriskdutyfactor where riskcode = '"+mLCContPlanDutyParamSet.get(i).getRiskCode()+"' and dutycode = '"+mLCContPlanDutyParamSet.get(i).getDutyCode()+"' and calfactor = 'DutyAmnt' ";
            	String tDutyAmntFlag = new ExeSQL().getOneValue(tSQL1);
            	if(!"1".equals(tDutyAmntFlag)){
            		for(int m=1;m<=mLCContPlanDutyParamSet.size();m++){
            			if("Amnt".equals(mLCContPlanDutyParamSet.get(m).getCalFactor())){
            				mLCContPlanDutyParamSet.get(m).setCalFactorValue(mLCContPlanDutyParamSet.get(i).getCalFactorValue());
            			}
            		}
            	}
        	}
        	
        	mLCContPlanDutyParamSet.get(i).setOrder(i-1);
        	
        	
        	//默认保障计划
        	if(!tParamRiskMap.containsKey(tParamRiskCode)){
        		tParamRiskMap.put(tParamRiskCode, "");
        		LCContPlanDutyParamSchema tempLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        		tReflections.transFields(tempLCContPlanDutyParamSchema,mLCContPlanDutyParamSet.get(i));
        		tempLCContPlanDutyParamSchema.setContPlanCode("11");
        		tempLCContPlanDutyParamSchema.setContPlanName("默认计划");
        		tempLCContPlanDutyParamSchema.setDutyCode("000000");
        		tempLCContPlanDutyParamSchema.setCalFactor("CalRule");
        		tempLCContPlanDutyParamSchema.setCalFactorType("1");
        		tempLCContPlanDutyParamSchema.setCalFactorValue("0");
        		tLCContPlanDutyParamSet.add(tempLCContPlanDutyParamSchema);
        	}
        	
        }
        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSet);
        
        
        /**
         * 将账户录入的要素信息补充到计划的责任下；
         */
        //contplancode为11 表示帐户计划
        LCContPlanDutyParamSet tLCContPlanDutyParamSet3 = new LCContPlanDutyParamSet();
        LCContPlanDutyParamSet tLCContPlanDutyParamSet4 = new LCContPlanDutyParamSet();
        for(int i=1;i<=mLCContPlanDutyParamSet.size();i++){
        	if("11".equals(mLCContPlanDutyParamSet.get(i).getContPlanCode()) 
        			&& mLCContPlanRiskSet.get(1).getRiskCode().equals(mLCContPlanDutyParamSet.get(i).getRiskCode())){
        		tLCContPlanDutyParamSet3.add(mLCContPlanDutyParamSet.get(i));
        	}else{
        		tLCContPlanDutyParamSet4.add(mLCContPlanDutyParamSet.get(i));
        	}
        }
        for(int i=1;i<=mLCContPlanDutyParamSet2.size();i++){
        	if("11".equals(mLCContPlanDutyParamSet2.get(i).getContPlanCode()) 
        			&& mLCContPlanRiskSet.get(1).getRiskCode().equals(mLCContPlanDutyParamSet2.get(i).getRiskCode())){
        		tLCContPlanDutyParamSet3.add(mLCContPlanDutyParamSet2.get(i));
        	}
        }
        String dutyCode = tLCContPlanDutyParamSet4.get(1).getDutyCode();
        int theFinal = tLCContPlanDutyParamSet4.size();
        for (int i = 1; i <= theFinal; i++)
        {
            if (i == 1
                    || !dutyCode.equals(tLCContPlanDutyParamSet4.get(i)
                            .getDutyCode()))
            {
                for (int n = 1; n <= tLCContPlanDutyParamSet3.size(); n++)
                {
                    if (tLCContPlanDutyParamSet3.get(n).getRiskCode()
                            .equals(
                            		tLCContPlanDutyParamSet4.get(i)
                                            .getRiskCode()))
                    {
                        LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = tLCContPlanDutyParamSet3
                                .get(n).getSchema();
                        tLCContPlanDutyParamSchema
                                .setDutyCode(tLCContPlanDutyParamSet4.get(i)
                                        .getDutyCode());
                        tLCContPlanDutyParamSchema
                                .setMainRiskCode(tLCContPlanDutyParamSet4
                                        .get(i).getMainRiskCode());
                        tLCContPlanDutyParamSchema
                                .setContPlanCode(tLCContPlanDutyParamSet4
                                        .get(i).getContPlanCode());
                        tLCContPlanDutyParamSchema
                                .setContPlanName(tLCContPlanDutyParamSet4
                                        .get(i).getContPlanName());
                        mLCContPlanDutyParamSet
                                .add(tLCContPlanDutyParamSchema);
                    }
                }
            }
            dutyCode = tLCContPlanDutyParamSet4.get(i).getDutyCode();
        }
        
        
        
        tMMap.put(mLCContPlanDutyParamSet, "INSERT");
		return true;
	}
	
	private boolean creatPubAccContPlan(){
		
		
		
		LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
		LCGrpInterestSet tLCGrpInterestSet = new LCGrpInterestSet();
		LDPromiseRateSet tLDPromiseRateSet = new LDPromiseRateSet();
		GlobalInput tG = new GlobalInput(); 
		tG.ComCode=mLCGrpContSchema.getManageCom();
		tG.ManageCom=mLCGrpContSchema.getManageCom();
		tG.Operator=mLCGrpContSchema.getOperator();
		
		  //实际是存储在职/退休 但是应数据被保险人状态，因此用词字段存储账户类型
	      String insuAccNo = mLCContPlanDutyParamSet2.get(1).getInsuAccNo();
		  String strSql = "select acctype from LMRiskInsuAcc where  InsuAccNo='"+insuAccNo+"'";
		  String strSqlRes = new ExeSQL().getOneValue(strSql); 
		  //团体账户名称，类型
      	String tInsuredName ;
      	String tPublicAccType ;
  		if("001".equals(strSqlRes))
  		{
  			tPublicAccType="C";
  			tInsuredName="团体理赔帐户";
  		}else if("004".equals(strSqlRes))
  		{
  			tPublicAccType="G";
  			tInsuredName="团体固定账户";
  		}else{
	    		//"没有指定帐户类型!";
	    		return false;
  		}
			//账户金额
  		String tPublicAcc = "";
  		for(int i=1;i<=mLCContPlanDutyParamSet2.size();i++){
  			if("PublicAcc".equals(mLCContPlanDutyParamSet2.get(i).getCalFactor())){
  				tPublicAcc = mLCContPlanDutyParamSet2.get(i).getCalFactorValue();
  			}
  		}
  			
			//险种代码
			String tRiskCode = mLCContPlanRiskSet.get(1).getRiskCode();
			//处理要素信息
			String tInsuAccNo = insuAccNo;

			LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
			tLCInsuredListSchema.setGrpContNo(mProposalGrpContNo);
			tLCInsuredListSchema.setInsuredName(tInsuredName);
			tLCInsuredListSchema.setEmployeeName(tInsuredName);
			tLCInsuredListSchema.setRelation("00");
			tLCInsuredListSchema.setRiskCode(tRiskCode);
			tLCInsuredListSchema.setPublicAcc(tPublicAcc);
			tLCInsuredListSchema.setPublicAccType(tPublicAccType);
			mLCInsuredListSet2.add(tLCInsuredListSchema);
			
			String tGrpPolNosql = "select grppolno from lcgrppol where grpcontno='"+mProposalGrpContNo+"'";
			SSRS tSSRS = new ExeSQL().execSQL(tGrpPolNosql);
			String tGrpPolNo1 = tSSRS.GetText(1, 1);
			for(int i=1;i<=mLCContPlanDutyParamSet2.size();i++){
				mLCContPlanDutyParamSet2.get(i).setGrpContNo(mProposalGrpContNo);
				mLCContPlanDutyParamSet2.get(i).setProposalGrpContNo(mProposalGrpContNo);
	        	mLCContPlanDutyParamSet2.get(i).setGrpPolNo(tGrpPolNo1);
	        	mLCContPlanDutyParamSet2.get(i).setMainRiskCode(tRiskCode);
	        	mLCContPlanDutyParamSet2.get(i).setRiskCode(tRiskCode);
	        	mLCContPlanDutyParamSet2.get(i).setContPlanCode("11");
	        	mLCContPlanDutyParamSet2.get(i).setDutyCode("000000");
	        	mLCContPlanDutyParamSet2.get(i).setPlanType("0");
	        	mLCContPlanDutyParamSet2.get(i).setPayPlanCode("000000");
	        	mLCContPlanDutyParamSet2.get(i).setGetDutyCode("000000");
	        	
			}
		
			//INSERT||MAIN
			// 准备传输数据 VData
			String transact = "INSERT||MAIN";
		  	VData tVData = new VData();
		  	TransferData tTransferData = new TransferData();
		  	tTransferData.setNameAndValue("GrpContNo",mProposalGrpContNo);
		  	tTransferData.setNameAndValue("RiskCode",mLCContPlanRiskSet.get(1).getRiskCode());
		  	tTransferData.setNameAndValue("GrpPolNo",tGrpPolNo1);
		  	tTransferData.setNameAndValue("InsuAccNo",mLCContPlanDutyParamSet2.get(1).getInsuAccNo());
		  	String claimnum ="";
		  	tTransferData.setNameAndValue("ClaimNum",claimnum);
		  	String tFlag = "";
		  	tTransferData.setNameAndValue("Flag",tFlag);
		  	tVData.add(tTransferData);
			tVData.add(mLCInsuredListSet2);
			tVData.add(mLCContPlanDutyParamSet2);
			tVData.add(tLCGrpFeeSet);
			tVData.add(tLCGrpInterestSet);
			tVData.add(tLDPromiseRateSet);
		  	tVData.add(tG);
		  	GrpPubAccBL tGrpPubAccBL = new GrpPubAccBL();
		  	if(!tGrpPubAccBL.submitData(tVData,transact)){
		  		responseOME = buildResponseOME("01", "15", "生成公共账户数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "15", "WX","生成公共账户数据失败!");
				System.out.println("responseOME:" + responseOME);
		    	return false;
		  	}
	        return true;
	    
		
	}
	
	private boolean createManageFee(){
		
		String tGrpPolNosql = "select grppolno from lcgrppol where grpcontno='"+mProposalGrpContNo+"'";
		SSRS tSSRS3 = new ExeSQL().execSQL(tGrpPolNosql);
		String tGrpPolNo1 = tSSRS3.GetText(1, 1);
		
		String strSql = "select distinct GrpPolNo,GrpContNo,RiskCode,"
				+"FeeCode,InsuAccNo,PayPlanCode,PayInsuAccName,"
				+"FeeCalMode,FeeCalModeType,FeeCalCode,(select FeeName from LMRiskFee where "
				+"FeeCode = LCGrpFee.FeeCode and InsuAccNo=LCGrpFee.InsuAccNo and PayPlanCode=LCGrpFee.PayPlanCode),FeeValue,"
				+"CompareValue,FeePeriod,MaxTime,case DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end,Operator,"
				+"MakeDate,MakeTime,ModifyDate,ModifyTime from LCGrpFee where grppolno='"+tGrpPolNo1+"' and InsuAccNo in (select InsuAccNo from LMRiskInsuAcc where RiskCode='"+mLCContPlanRiskSet.get(1).getRiskCode()+"' and AccType<>'002' union select '000000' from dual)";
		SSRS tSSRS = new ExeSQL().execSQL(strSql);
		for(int i=1;i<=tSSRS.getMaxRow();i++){
			for(int j=1;j<=mLCGrpFeeSet.size();j++){
				if(mLCGrpFeeSet.get(j).getFeeCode().equals(tSSRS.GetText(i, 4))){
					mLCGrpFeeSet.get(j).setGrpPolNo(tSSRS.GetText(i, 1));
					mLCGrpFeeSet.get(j).setGrpContNo(tSSRS.GetText(i, 2));
					mLCGrpFeeSet.get(j).setRiskCode(tSSRS.GetText(i, 3));
					mLCGrpFeeSet.get(j).setFeeCode(tSSRS.GetText(i, 4));
					mLCGrpFeeSet.get(j).setInsuAccNo(tSSRS.GetText(i, 5));
					mLCGrpFeeSet.get(j).setPayPlanCode(tSSRS.GetText(i, 6));
					mLCGrpFeeSet.get(j).setPayInsuAccName(tSSRS.GetText(i, 7));
					mLCGrpFeeSet.get(j).setFeeCalMode(tSSRS.GetText(i, 8));
					mLCGrpFeeSet.get(j).setFeeCalModeType(tSSRS.GetText(i, 9));
					mLCGrpFeeSet.get(j).setFeeCalCode(tSSRS.GetText(i, 10));
					mLCGrpFeeSet.get(j).setCompareValue(tSSRS.GetText(i, 13));
					mLCGrpFeeSet.get(j).setFeePeriod(tSSRS.GetText(i, 14));
					mLCGrpFeeSet.get(j).setMaxTime(tSSRS.GetText(i, 15));
					mLCGrpFeeSet.get(j).setDefaultFlag(tSSRS.GetText(i, 16));
					mLCGrpFeeSet.get(j).setOperator(tSSRS.GetText(i, 17));
					mLCGrpFeeSet.get(j).setMakeDate(tSSRS.GetText(i, 18));
					mLCGrpFeeSet.get(j).setMakeTime(tSSRS.GetText(i, 19));
					mLCGrpFeeSet.get(j).setModifyDate(tSSRS.GetText(i, 20));
					mLCGrpFeeSet.get(j).setModifyTime(tSSRS.GetText(i, 21));
					mLCGrpFeeSet.get(j).setClaimNum(ClaimNum);
				}
			}
		}
		
		String strSql2 = "select distinct GrpPolNo,GrpContNo,RiskCode,"
				+"FeeCode,InsuAccNo,PayPlanCode,PayInsuAccName,"
				+"FeeCalMode,FeeCalModeType,FeeCalCode,(select FeeName from LMRiskFee where "
				+"FeeCode = LCGrpFee.FeeCode and InsuAccNo=LCGrpFee.InsuAccNo and PayPlanCode=LCGrpFee.PayPlanCode),FeeValue,"
				+"CompareValue,FeePeriod,MaxTime,case DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end,Operator,"
				+"MakeDate,MakeTime,ModifyDate,ModifyTime from LCGrpFee where grppolno='"+tGrpPolNo1+"' and InsuAccNo in (select InsuAccNo from LMRiskInsuAcc where RiskCode='"+mLCContPlanRiskSet.get(1).getRiskCode()+"' and AccType='002')";
		SSRS tSSRS2 = new ExeSQL().execSQL(strSql2);
		for(int i=1;i<=tSSRS2.getMaxRow();i++){
			LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
			
			tLCGrpFeeSchema.setGrpPolNo(tSSRS2.GetText(i, 1));
			tLCGrpFeeSchema.setGrpContNo(tSSRS2.GetText(i, 2));
			tLCGrpFeeSchema.setRiskCode(tSSRS2.GetText(i, 3));
			tLCGrpFeeSchema.setFeeCode(tSSRS2.GetText(i, 4));
			tLCGrpFeeSchema.setInsuAccNo(tSSRS2.GetText(i, 5));
			tLCGrpFeeSchema.setPayPlanCode(tSSRS2.GetText(i, 6));
			tLCGrpFeeSchema.setPayInsuAccName(tSSRS2.GetText(i, 7));
			tLCGrpFeeSchema.setFeeCalMode(tSSRS2.GetText(i, 8));
			tLCGrpFeeSchema.setFeeCalModeType(tSSRS2.GetText(i, 9));
			tLCGrpFeeSchema.setFeeCalCode(tSSRS2.GetText(i, 10));
//			for(int j=1;j<=tSSRS.getMaxRow();j++){
//				if(tSSRS2.GetText(i, 11).equals(tSSRS.GetText(j, 11))){
//					tLCGrpFeeSchema.setFeeValue(mLCGrpFeeSet.get(j).getFeeValue());
//				}
//			}
			for(int j=1;j<=mLCGrpFeeSet.size();j++){
				if("000002".equals(mLCGrpFeeSet.get(j).getFeeCode())){
					tLCGrpFeeSchema.setFeeValue(mLCGrpFeeSet.get(j).getFeeValue());
				}
			}
			
			tLCGrpFeeSchema.setCompareValue(tSSRS2.GetText(i, 13));
			tLCGrpFeeSchema.setFeePeriod(tSSRS2.GetText(i, 14));
			tLCGrpFeeSchema.setMaxTime(tSSRS2.GetText(i, 15));
			tLCGrpFeeSchema.setDefaultFlag(tSSRS2.GetText(i, 16));
			tLCGrpFeeSchema.setOperator(tSSRS2.GetText(i, 17));
			tLCGrpFeeSchema.setMakeDate(tSSRS2.GetText(i, 18));
			tLCGrpFeeSchema.setMakeTime(tSSRS2.GetText(i, 19));
			tLCGrpFeeSchema.setModifyDate(tSSRS2.GetText(i, 20));
			tLCGrpFeeSchema.setModifyTime(tSSRS2.GetText(i, 21));
			tLCGrpFeeSchema.setClaimNum(ClaimNum);
			mLCGrpFeeSet.add(tLCGrpFeeSchema);
		}
		
		LCGrpInterestSet tLCGrpInterestSet = new LCGrpInterestSet();
		LDPromiseRateSet tLDPromiseRateSet = new LDPromiseRateSet();
		GlobalInput tG = new GlobalInput(); 
		tG.ComCode=mLCGrpContSchema.getManageCom();
		tG.ManageCom=mLCGrpContSchema.getManageCom();
		tG.Operator=mLCGrpContSchema.getOperator();
		
		  //实际是存储在职/退休 但是应数据被保险人状态，因此用词字段存储账户类型
	      String insuAccNo = mLCContPlanDutyParamSet2.get(1).getInsuAccNo();
		  String strSql3 = "select acctype from LMRiskInsuAcc where  InsuAccNo='"+insuAccNo+"'";
		  String strSqlRes = new ExeSQL().getOneValue(strSql3); 
		  //团体账户名称，类型
      	String tInsuredName ;
      	String tPublicAccType ;
  		if("001".equals(strSqlRes))
  		{
  			tPublicAccType="C";
  			tInsuredName="团体理赔帐户";
  		}else if("004".equals(strSqlRes))
  		{
  			tPublicAccType="G";
  			tInsuredName="团体固定账户";
  		}else{
	    		//"没有指定帐户类型!";
	    		return false;
  		}
			//账户金额
  		String tPublicAcc = "";
  		for(int i=1;i<=mLCContPlanDutyParamSet2.size();i++){
  			if("PublicAcc".equals(mLCContPlanDutyParamSet2.get(i).getCalFactor())){
  				tPublicAcc = mLCContPlanDutyParamSet2.get(i).getCalFactorValue();
  			}
  		}
  			
			//险种代码
			String tRiskCode = mLCContPlanRiskSet.get(1).getRiskCode();
			//处理要素信息
			String tInsuAccNo = insuAccNo;

			LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();
			LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
			tLCInsuredListSchema.setGrpContNo(mProposalGrpContNo);
			tLCInsuredListSchema.setInsuredName(tInsuredName);
			tLCInsuredListSchema.setEmployeeName(tInsuredName);
			tLCInsuredListSchema.setRelation("00");
			tLCInsuredListSchema.setRiskCode(tRiskCode);
			tLCInsuredListSchema.setPublicAcc(tPublicAcc);
			tLCInsuredListSchema.setPublicAccType(tPublicAccType);
			mLCInsuredListSet.add(tLCInsuredListSchema);
		
			LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
			//INSERT||MAIN
			// 准备传输数据 VData
			String transact = "MANAGEFEE||MAIN";
		  	VData tVData = new VData();
		  	TransferData tTransferData = new TransferData();
		  	tTransferData.setNameAndValue("GrpContNo",mProposalGrpContNo);
		  	tTransferData.setNameAndValue("RiskCode",mLCContPlanRiskSet.get(1).getRiskCode());
		  	tTransferData.setNameAndValue("GrpPolNo",tGrpPolNo1);
		  	tTransferData.setNameAndValue("InsuAccNo",mLCContPlanDutyParamSet2.get(1).getInsuAccNo());
		  	tTransferData.setNameAndValue("ClaimNum",ClaimNum);
		  	String tFlag = "";
		  	tTransferData.setNameAndValue("Flag",tFlag);
		  	tVData.add(tTransferData);
				tVData.add(mLCInsuredListSet);
				tVData.add(tLCContPlanDutyParamSet);
				tVData.add(mLCGrpFeeSet);
				tVData.add(tLCGrpInterestSet);
				tVData.add(tLDPromiseRateSet);
		  	tVData.add(tG);
		  	GrpPubAccBL tGrpPubAccBL = new GrpPubAccBL();
		  	if(!tGrpPubAccBL.submitData(tVData,transact)){
		  		responseOME = buildResponseOME("01", "16", "生成管理费数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "16", "WX","生成管理费数据失败!");
				System.out.println("responseOME:" + responseOME);
		    	return false;
		  	}
	        return true;
	        

	}
	
	private boolean createFee(){
		String tempfeeno = mLCGrpContSchema.getPrtNo() + "801";
		String strSql1="select a.riskcode,b.riskname,sum(a.prem)-(Select nvl(Sum(Mo.Paymoney),0) From Ljtempfee Mo Where Mo.Riskcode = a.Riskcode And Mo.Otherno = '"+mLCGrpContSchema.getPrtNo()+"' And Exists (Select 1 From Ljtempfeeclass Where Tempfeeno = Mo.Tempfeeno And Paymode = 'YS')) from lcgrppol a,lmrisk b where prtno='"+mLCGrpContSchema.getPrtNo()+"' and a.riskcode=b.riskcode group by a.riskcode,b.riskname";
		String strSql2="select paymode,codename,prem - (select nvl(Sum(Mo.Paymoney),0) from ljtempfeeclass mo where exists (select 1 from ljtempfee where tempfeeno=mo.tempfeeno and otherno='"+mLCGrpContSchema.getPrtNo()+"')),'','','',bankcode,bankaccno,accname from lcgrpcont,ldcode where prtno='"+mLCGrpContSchema.getPrtNo()+"' and  codetype='paymode' and paymode=code";
		SSRS ssrs1 = new ExeSQL().execSQL(strSql1);
		SSRS ssrs2 = new ExeSQL().execSQL(strSql2);
		double PayMoney2 = Double.parseDouble(ssrs2.GetText(1, 3));
		if(PayMoney2!=PayMoney){
			responseOME = buildResponseOME("01", "20", "传入的保费与计算的保费不一致！");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "20", "WX","传入的保费与计算的保费不一致!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		double CashValue = 0;
		//校验缴费方式，总金额，
		for(int i=1;i<=ssrs2.getMaxRow();i++){
			// 暂收分类表记录集
		    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
		      tLJTempFeeClassSchema.setTempFeeNo(tempfeeno);
		      tLJTempFeeClassSchema.setPayMode(ssrs2.GetText(i, 1));
		      tLJTempFeeClassSchema.setPayDate(mLJTempFeeClassSchema.getEnterAccDate());
		      tLJTempFeeClassSchema.setPayMoney(ssrs2.GetText(i, 3));
		      tLJTempFeeClassSchema.setChequeNo(ssrs2.GetText(i, 4));
		      tLJTempFeeClassSchema.setChequeDate(ssrs2.GetText(i, 5));
		      tLJTempFeeClassSchema.setEnterAccDate(mLJTempFeeClassSchema.getEnterAccDate());
		      tLJTempFeeClassSchema.setManageCom(mLCGrpContSchema.getManageCom());
		      tLJTempFeeClassSchema.setBankCode(ssrs2.GetText(i, 7));
		      tLJTempFeeClassSchema.setBankAccNo(ssrs2.GetText(i, 8));
		      tLJTempFeeClassSchema.setAccName(ssrs2.GetText(i, 9));
		      tLJTempFeeClassSchema.setInsBankCode("");
		      tLJTempFeeClassSchema.setInsBankAccNo("");
		      tLJTempFeeClassSchema.setOperator(mLCGrpContSchema.getOperator());
		      tLJTempFeeClassSchema.setCashier(mLCGrpContSchema.getOperator());
		      tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
		      CashValue = CashValue + Double.parseDouble(ssrs2.GetText(i, 3));
		}
		for(int i=1;i<=ssrs1.getMaxRow();i++){
		    // 暂收表信息记录集
			LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
		      tLJTempFeeSchema.setTempFeeNo(tempfeeno);
		      tLJTempFeeSchema.setTempFeeType("1");
		      tLJTempFeeSchema.setRiskCode(ssrs1.GetText(i, 1));
//		      String sql1 = "";
//		      tLJTempFeeSchema.setAgentGroup(tAgentGroup[i]);
		      tLJTempFeeSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
		      tLJTempFeeSchema.setPayDate(mLJTempFeeClassSchema.getEnterAccDate());
		      tLJTempFeeSchema.setEnterAccDate(mLJTempFeeClassSchema.getEnterAccDate()); //到帐日期
		      tLJTempFeeSchema.setPayMoney(ssrs1.GetText(i, 3));
		      tLJTempFeeSchema.setManageCom(mLCGrpContSchema.getManageCom());
		      tLJTempFeeSchema.setOtherNo(mLCGrpContSchema.getPrtNo());
		      tLJTempFeeSchema.setOtherNoType("4");
		      tLJTempFeeSchema.setOperator(mLCGrpContSchema.getOperator());
		      tLJTempFeeSchema.setCashier(mLCGrpContSchema.getOperator());
		      tLJTempFeeSet.add(tLJTempFeeSchema);
		}
		
		GlobalInput tG = new GlobalInput(); 
		tG.ComCode=mLCGrpContSchema.getManageCom();
		tG.ManageCom=mLCGrpContSchema.getManageCom();
		tG.Operator=mLCGrpContSchema.getOperator();
		// 准备传输数据 VData
	    VData tVData = new VData();
	    tVData.addElement(tLJTempFeeSet);
	    tVData.addElement(tLJTempFeeClassSet);
	    tVData.addElement(tG);
	    // 数据传输
	    TempFeeUI tTempFeeUI = new TempFeeUI();
	    if(!tTempFeeUI.submitData(tVData, "INSERT")){
	    	responseOME = buildResponseOME("01", "21", "生成收费数据失败！");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "21", "WX","生成收费数据失败!");
			System.out.println("responseOME:" + responseOME);
	    	return false;
	    }
		return true;
		
	}
	
	private boolean signCount(){
		VData cInputData = new VData();
        String cOperate = "";
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = mLCGrpContSchema.getOperator();
        tGlobalInput.ManageCom = mLCGrpContSchema.getManageCom();
        tGlobalInput.ComCode = mLCGrpContSchema.getManageCom();
        cInputData.add(tGlobalInput);
        String sql = "select grpcontno from lcgrpcont where prtno='"+mLCGrpContSchema.getPrtNo()+"'";
        String grpcontno = new ExeSQL().getOneValue(sql);
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContSchema tLCContSchema = new LCGrpContSchema();
        tLCContSchema.setGrpContNo(grpcontno);
        tLCGrpContSet.add(tLCContSchema);
        cInputData.add(tLCGrpContSet);
        LCGrpContSignBL lCGrpContSignBL = new LCGrpContSignBL();
        if(!lCGrpContSignBL.submitData(cInputData, cOperate)){
        	responseOME = buildResponseOME("01", "22", "生成签单数据失败！");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "22", "WX","生成签单数据失败!");
			System.out.println("responseOME:" + responseOME);
	    	return false;
        }
        return true;
	}
	
	private boolean print(){
		MMap tMMap = new MMap();
        LCGrpContDB mLCGrpContDB = new LCGrpContDB();
        LCGrpContSet mLCGrpContSet = new LCGrpContSet();
        mLCGrpContDB.setPrtNo(this.mLCGrpContSchema.getPrtNo());

        mLCGrpContSet = mLCGrpContDB.query();
        if (mLCGrpContSet.size() == 0)
        {
        	responseOME = buildResponseOME("01", "23", "查询保单信息失败！");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "23", "WX","查询保单信息失败！");
			System.out.println("responseOME:" + responseOME);
			return false;
        }
        for(int i=1;i<=mLCGrpContSet.size();i++){
        	LCGrpContSchema mLgc = mLCGrpContSet.get(i);
        	mLgc.setCustomGetPolDate(mLgc.getSignDate());
        	mLgc.setGetPolDate(mLgc.getSignDate());
        	mLgc.setPrintCount(1);
        	mLgc.setModifyDate(theCurrentDate);
        	mLgc.setModifyTime(theCurrentTime);
        	tMMap.put(mLgc, "UPDATE");
        	LCContDB mLCContDB = new LCContDB();
        	LCContSet mLCContSet = new LCContSet();
        	mLCContDB.setGrpContNo(mLgc.getGrpContNo());
        	mLCContSet = mLCContDB.query();
        	if(mLCContSet.size() == 0){
        		responseOME = buildResponseOME("01", "23", "查询保单信息失败！");
    			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
    					sendDate, sendTime, "01", "23", "WX","查询保单信息失败！");
    			System.out.println("responseOME:" + responseOME);
    			return false;
        	}
        	for(int j=1;j<=mLCContSet.size();j++){
        		LCContSchema mLc = mLCContSet.get(j);
        		mLc.setCustomGetPolDate(mLgc.getSignDate());
        		mLc.setGetPolDate(mLgc.getSignDate());
        		mLc.setModifyDate(theCurrentDate);
        		mLc.setModifyTime(theCurrentTime);
        		tMMap.put(mLc, "UPDATE");
        	}
        }
        try{
			VData v = new VData();
			v.add(tMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				responseOME = buildResponseOME("01", "19", "生成打印、回执数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "19", "WX","生成打印、回执数据失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME("01", "19", "生成打印、回执数据失败！");
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean creatInsured(){
		for(int i=1;i<=mLCInsuredListSet.size();i++){
			mLCInsuredListSet.get(i).setGrpContNo(mProposalGrpContNo);
			mLCInsuredListSet.get(i).setState("0");
			mLCInsuredListSet.get(i).setBatchNo(mLCGrpContSchema.getPrtNo());
			mLCInsuredListSet.get(i).setOperator(mGlobalInput.Operator);
			mLCInsuredListSet.get(i).setMakeDate(theCurrentDate);
			mLCInsuredListSet.get(i).setMakeTime(theCurrentTime);
			mLCInsuredListSet.get(i).setModifyDate(theCurrentDate);
			mLCInsuredListSet.get(i).setModifyTime(theCurrentTime);
		}
		tMMap.put(mLCInsuredListSet, SysConst.INSERT);
		
		return true;
	}
	
	private boolean calPrem(){
		ParseGuideIn tParseGuideIn = new ParseGuideIn();
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("GrpContNo", mProposalGrpContNo);
		
		tVData.add(tTransferData);
		tVData.add(mGlobalInput);
		if (tParseGuideIn.submitData(tVData, "INSERT||DATABASE") == false) {
			// @@错误处理
			responseOME = buildResponseOME("01", "18", "保费计算失败，原因为："+tParseGuideIn.mErrors.getLastError()+"!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "18", "WX","保费计算失败，原因为："+tParseGuideIn.mErrors.getLastError()+"!");
			System.out.println("responseOME:" + responseOME);
			return false;
		} 
		return true;
	}
	
	private boolean creatMission(){
		String tSQL1 = "update lcgrpcont set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
		String tSQL2 = "update lcgrppol set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
		String tSQL3 = "update lccont set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
		String tSQL4 = "update lcpol set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwcode = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
		MMap tempMMap = new MMap();
		tempMMap.put(tSQL1, SysConst.UPDATE);
		tempMMap.put(tSQL2, SysConst.UPDATE);
		tempMMap.put(tSQL3, SysConst.UPDATE);
		tempMMap.put(tSQL4, SysConst.UPDATE);
		
		
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		String prtSeq = PubFun1.CreateMaxNo("GPAYNOTICENO",mLCGrpContSchema.getPrtNo());
		tLOPRTManagerSchema.setPrtSeq(prtSeq);
        tLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLOPRTManagerSchema.setOtherNoType("01");
        tLOPRTManagerSchema.setCode("57");
        tLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
        tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0");//前台打印
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setMakeDate(theCurrentDate);
        tLOPRTManagerSchema.setMakeTime(theCurrentTime);
        
        tempMMap.put(tLOPRTManagerSchema, SysConst.INSERT);
        
        
		try{
			VData v = new VData();
			v.add(tempMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				responseOME = buildResponseOME("01", "19", "生成核保数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "19", "WX","生成核保数据失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME("01", "19", "生成核保数据失败！");
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean AgentCode(String AgentCode,String state) {
		System.out.println("输入的agentcode="+AgentCode);
		boolean cheak = false;

		if(AgentCode!=null&&!"".equals(AgentCode)){
			//TODO
			//String querySql = "select groupagentcode from laagent where (agentcode ='"+AgentCode+"' and groupagentcode !='') or (groupagentcode ='"+AgentCode+"' and agentcode !='')";
			String querySql = "select agentcode from laagent where (agentcode ='"+AgentCode+"' and groupagentcode !='') or (groupagentcode ='"+AgentCode+"' and agentcode !='')";
			
			SSRS resultSSRS = new ExeSQL().execSQL(querySql);
			if(resultSSRS==null || resultSSRS.MaxRow==0){
				message = "业务员号码有误！";
				if("Y".equals(state)){
					cheak = false;
				}else if("N".equals(state)){
					cheak = true;
					result = AgentCode ;
				}
				return cheak ;
			}
			result = resultSSRS.GetText(1, 1);
			System.out.println("resultSSRS不为空，result: " + result);
		}else{
			if("Y".equals(state)){
				cheak = true;
			}else if("N".equals(state)){
				cheak = true;
			}
			result = AgentCode ;
		}
		System.out.println("输出的agentcode="+result);
		return cheak ;
	}
	//返回信息
	public String getMessage(){
	    return message;
	}
	//返回业务员代码
	public String getResult(){
	    return result;
	}
	
	public String diffdate(String start,String end){
		String a = "";
		String sql = "select 1 from dual where to_Date('"+start+"', 'YYYY-MM-DD')<=to_Date('"+end+"','YYYY-MM-DD')";
		SSRS res = new ExeSQL().execSQL(sql);
		if(res == null || res.getMaxRow()<1){
			a = "保单失效日期不应在保单生效日期之前!";
			return a;
		}
		return a;
	}	
	
	public boolean checkGrpName(){
		String insuredProp = this.mLCGrpAppntSchema.getInsuredProperty();
		String grpName = this.mLCGrpContSchema.getGrpName();
		//法人
		String customerno = "";
		boolean b = false;
		if(this.mLCGrpAppntSchema.getInsuredProperty().equals("1")){
			//组织机构代码
			String organComCode = this.mLCGrpAppntSchema.getOrganComCode();
			//统一社会信用代码
			String unifiedSocialCreditNo = this.mLCGrpAppntSchema.getUnifiedSocialCreditNo();
			if(organComCode!=null && !"".equals(organComCode)){
				if(unifiedSocialCreditNo!=null && !"".equals(unifiedSocialCreditNo)){
					String sql1 = "select customerno from ldgrp where grpname='"+grpName+"' and organcomcode='"+organComCode+"'";
					SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
					if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
						String sql2 = "select customerno from ldgrp where grpname='"+grpName+"' and unifiedsocialcreditno='"+unifiedSocialCreditNo+"'";
						SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
						if(tSSRS2 == null || tSSRS2.getMaxRow()<=0){
							b = true;
						}else{
							customerno = tSSRS2.GetText(1, 1);
						}
					}else{
						customerno = tSSRS1.GetText(1, 1);
					}
					
					
				}else{
					String sql1 = "select customerno from ldgrp where grpname='"+grpName+"' and organcomcode='"+organComCode+"'";
					SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
					if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
						b = true;
					}else{
						customerno = tSSRS1.GetText(1, 1);
					}
				}
			}else{
				String sql1 = "select customerno from ldgrp where grpname='"+grpName+"' and unifiedsocialcreditno='"+unifiedSocialCreditNo+"'";
				SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
				if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
					b = true;
				}else{
					customerno = tSSRS1.GetText(1, 1);
				}
			}
		}else if(this.mLCGrpAppntSchema.getInsuredProperty().equals("2")){//自然人
			String linkManName = this.mLCGrpAddressSchema.getLinkMan1();
			if(!grpName.equals(linkManName)){
				responseOME = buildResponseOME("01","17", "投保人属性为自然人时，投保人名称和联系人姓名不同，请检查！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "投保人属性为自然人时，投保人名称和联系人姓名不同，请检查！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}else{
				String sql1 = "select customerno from ldgrp where grpname='"+grpName+"'";
				SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
				if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
					b = true;
				}else{
					boolean c = false;
					String custno = "";
					for(int i=1;i<=tSSRS1.getMaxRow();i++){
						String sql2 = "select grpcontno from lcgrpcont where appntno='"+tSSRS1.GetText(i, 1)+"'";
						SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
						if(tSSRS2 != null || tSSRS2.getMaxRow()>0){
							for(int j=1;j<=tSSRS2.getMaxRow();j++){
								String sql3 = "select a.customerno,b.linkman1,a.idtype,a.idno from lcgrpappnt a,lcgrpaddress b,lcgrpcont c where a.grpcontno=c.grpcontno and a.prtno=c.prtno and a.addressno=c.addressno and a.customerno=c.appntno and b.customerno=c.appntno and b.addressno=c.addressno and c.grpcontno ='"+tSSRS2.GetText(j, 1)+"' "
										+ "union "
										+ "select a.customerno,b.linkman1,a.idtype,a.idno from lbgrpappnt a,lcgrpaddress b,lbgrpcont c where a.grpcontno=c.grpcontno and a.prtno=c.prtno and a.addressno=c.addressno and a.customerno=c.appntno and b.customerno=c.appntno and b.addressno=c.addressno and c.grpcontno ='"+tSSRS2.GetText(j, 1)+"' with ur";
								SSRS tSSRS3 = new ExeSQL().execSQL(sql3);
								if(tSSRS3 != null || tSSRS3.getMaxRow()>0){
									for(int z=1;z<=tSSRS3.getMaxRow();z++){
										if(this.mLCGrpAddressSchema.getLinkMan1().equals(tSSRS3.GetText(z, 2)) && this.mLCGrpAppntSchema.getIDType().equals(tSSRS3.GetText(z, 3)) && this.mLCGrpAppntSchema.getIDNo().equals(tSSRS3.GetText(z, 4))){
											c = true;
											custno = tSSRS3.GetText(z, 1);
										}
									}
								}
							}
						}
					}
					if(c){
						customerno = custno;
					}else{
						b = true;
					}
				}
			}
		}
		if(b){
			customerflag = true;
			//新建客户
			String tLimit = "SN";
	        String grpNo = PubFun1.CreateMaxNo("GRPNO", tLimit);
	        if("".equals(grpNo)){
	        	responseOME = buildResponseOME("01","17", "客户号码生成失败！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "客户号码生成失败！");
				System.out.println("responseOME:" + responseOME);
				return false;
	        }
			this.mLCGrpContSchema.setAppntNo(grpNo);
			this.mLCGrpAppntSchema.setCustomerNo(grpNo);
			this.mLDGrpSchema.setCustomerNo(grpNo);
			this.mLCGrpAddressSchema.setCustomerNo(grpNo);
		}else{
			//使用历史客户
			this.mLCGrpContSchema.setAppntNo(customerno);
			this.mLCGrpAppntSchema.setCustomerNo(customerno);
			this.mLDGrpSchema.setCustomerNo(customerno);
			this.mLCGrpAddressSchema.setCustomerNo(customerno);
		}
		return true;
	}
}
