package com.ecwebservice.subinterface;

import java.util.Iterator;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 管理机构其下所有机构详细信息
 * @author abc
 *
 */
public class QueryManageComInfo {

	
	private OMElement mOME = null;

	private String batchNo = null;// 批次号

	private String branchCode = "";// 报送单位

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String sendOperator = null; // 报送人员
	
	private String manageCom="";	
	
	private MMap mMap=new MMap();	
	
	private OMElement responseOME = null;
	
	
	public OMElement queryData(OMElement Oms) {
		this.mOME=Oms.cloneOMElement();
		if(!load()){
			return responseOME;
		}
		
		if(!check()){
			return responseOME;
		}
		
		if(!deal()){
			return responseOME;
		}
		
		return responseOME=buildResponseOME("00","","");
	}
	
	private boolean deal() {
		
		String strSQL="select comcode,name,address,phone,fax,ZipCode " +
				" from ldcom where comcode like '"+this.manageCom+"%' " +
				" and comcode!='"+this.manageCom+"%' " +
				" and Sign = '1' " +
				" and length(trim(comcode))=8 " +
				" with ur";
		
		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(strSQL);
		if (null == tSSRS || tSSRS.MaxRow==0){
			this.mMap.put("", new String[]{"","","","","",""});	//保证返回报文的完整性
			responseOME=buildResponseOME("01","02","没有查询到管理机构"+this.manageCom+"的下属机构信息!");
			return false;
		}else{
			for(int i=1;i<=tSSRS.getMaxRow();i++){
				int length=tSSRS.getRowData(i).length;
				String[] mangeComInfo=new String[length];
				for(int j=0;j<length;j++){
					mangeComInfo[j]=tSSRS.GetText(i, j+1);
				}
				this.mMap.put(mangeComInfo[0], mangeComInfo);
			}
		}
		
		return true;
	}

	private boolean check() {
		if(null==this.manageCom||"".equals(this.manageCom)){
			this.mMap.put("", new String[]{"","","","","",""});
			responseOME=buildResponseOME("01","02","获取管理机构代码失败!");
			return false;
		}
		return true;
	}

	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("LIS008")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BatchNo")) {
							this.batchNo = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendDate")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendTime")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("BranchCode")) {
							this.branchCode = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendOperator")) {
							this.sendOperator = child_element.getText();
							continue;
						}
						// 业务员信息
						if (child_element.getLocalName().equals("SiteInfo")) {
							Iterator childVerify = child_element.getChildren();
							
							while (childVerify.hasNext()) {
								OMElement child_element_verify = (OMElement) childVerify
										.next();

								if (child_element_verify.getLocalName().equals("ITEM")) {
									Iterator childItem = child_element_verify.getChildren();
									while (childItem.hasNext()) {
										OMElement child_item = (OMElement) childItem.next();

										if (child_item.getLocalName().equals("ManageCom")) {
											this.manageCom = child_item.getText();
										}
									}
								}
							}
							
						}
						
					}

				} else {
					System.out.println("报文类型不匹配");
					responseOME=buildResponseOME("01","01","报文类型不匹配");
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME=buildResponseOME("01","01","加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement("LIS008", null);
		OMElement comInfo = fac.createOMElement("SiteInfo", null);
		
		addOm(DATASET, "BatchNo", this.batchNo);
		addOm(DATASET, "SendDate", PubFun.getCurrentDate());
		addOm(DATASET, "SendTime", PubFun.getCurrentTime());
		addOm(DATASET, "BranchCode", this.branchCode);
		addOm(DATASET, "SendOperator", this.sendOperator);
		addOm(DATASET, "State", state);
		addOm(DATASET, "ErrCode", errorCode);
		addOm(DATASET, "ErrInfo", errorInfo);
		addOm(DATASET, "ManageCom", this.manageCom);
		
		Set set=mMap.keySet();
		Object key = null;
		for(int i=0;i<set.size();i++){
			OMElement item = fac.createOMElement("ITEM", null);
			key = mMap.getOrder().get(String.valueOf(i + 1));
			String[] temp = (String[]) mMap.get(key);
				
				addOm(item, "ManageCom",temp[0]);
				addOm(item, "ManageName",temp[1]);
				addOm(item, "Address",temp[2]);
				addOm(item, "Phone",temp[3]);
				addOm(item, "Fax",temp[4]);
				addOm(item, "ZipCode",temp[5]);
				comInfo.addChild(item);
		}
		DATASET.addChild(comInfo);
		
		return DATASET;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}	
	
	
	
}
