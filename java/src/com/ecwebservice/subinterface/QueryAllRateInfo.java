package com.ecwebservice.subinterface;

import java.util.Iterator;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 查询指定险种的历史结算利率信息
 * @author 
 *	20140808
 */
public class QueryAllRateInfo {
	
	private OMElement mOME = null;

	private String batchNo = null;// 批次号

	private String branchCode = "";// 报送单位

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String sendOperator = null; // 报送人员
	
	private String riskCode="";
	
	private String riskName="";
	
	private MMap rateMap= new MMap();	//返回报文信息
	
	private OMElement responseOME = null;
	
	public OMElement queryData(OMElement Oms) {
		this.mOME=Oms.cloneOMElement();
		if(!load()){
			return responseOME;
		}
		
		if(!check()){
			return responseOME;
		}
		
		if(!deal()){
			return responseOME;
		}
		
		return responseOME=buildResponseOME("00","","");
	}

	private boolean deal() {
		String strSQL="select riskcode,(select riskname from lmriskapp where riskcode=a.riskcode), " +
				" (select  left(cast((baladate) as char(10)),7)  from dual) as month," +
				" a.rate " +
				" from LMInsuAccRate a where riskcode='"+this.riskCode+"' " +
				" order by month desc with ur ";

		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(strSQL);
		if (null == tSSRS || tSSRS.MaxRow==0){
			this.rateMap.put("", new String[]{"",""});	//保证返回报文的完整性
			responseOME=buildResponseOME("01","02","没有查询到险种"+this.riskCode+"的结算利率!");
			return false;
		}else{
			this.riskCode=tSSRS.GetText(1, 1);
			this.riskName=tSSRS.GetText(1, 2);
			for(int i=1;i<=tSSRS.getMaxRow();i++){
				String[] riskRate=new String[2];
				
				riskRate[0]=tSSRS.GetText(i, 3);
				riskRate[1]=tSSRS.GetText(i, 4);
				rateMap.put(String.valueOf(i), riskRate);
			}
		}
		return true;
	}

	private boolean check() {
		
		if(null==this.riskCode||"".equals(this.riskCode)){
			this.rateMap.put("", new String[]{"",""});	//保证返回报文的完整性
			responseOME=buildResponseOME("01","03","险种编码为空!");
			return false;
		}
		
		return true;
	}

	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("LIS005")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BatchNo")) {
							this.batchNo = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendDate")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendTime")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("BranchCode")) {
							this.branchCode = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendOperator")) {
							this.sendOperator = child_element.getText();
							continue;
						}
						// 业务员信息
						if (child_element.getLocalName().equals("RateInfo")) {
							Iterator childVerify = child_element.getChildren();
							
							while (childVerify.hasNext()) {
								OMElement child_element_verify = (OMElement) childVerify
										.next();

								if (child_element_verify.getLocalName().equals("ITEM")) {
									Iterator childItem = child_element_verify.getChildren();
									while (childItem.hasNext()) {
										OMElement child_item = (OMElement) childItem.next();

										if (child_item.getLocalName().equals("RiskCode")) {
											this.riskCode = child_item.getText();
										}
									}
								}
							}
							
						}
						
					}

				} else {
					System.out.println("报文类型不匹配");
					responseOME=buildResponseOME("01","01","报文类型不匹配");
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME=buildResponseOME("01","01","加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement("LIS005", null);
		OMElement rateInfo = fac.createOMElement("RateInfo", null);
		
		addOm(DATASET, "BatchNo", this.batchNo);
		addOm(DATASET, "SendDate", PubFun.getCurrentDate());
		addOm(DATASET, "SendTime", PubFun.getCurrentTime());
		addOm(DATASET, "BranchCode", this.branchCode);
		addOm(DATASET, "SendOperator", this.sendOperator);
		addOm(DATASET, "State", state);
		addOm(DATASET, "ErrCode", errorCode);
		addOm(DATASET, "ErrInfo", errorInfo);
		addOm(DATASET, "RiskCode", this.riskCode);
		addOm(DATASET, "RiskName", this.riskName);
		
		Set set=rateMap.keySet();
		Object key = null;
		for(int i=0;i<set.size();i++){
			OMElement item = fac.createOMElement("ITEM", null);
			key = rateMap.getOrder().get(String.valueOf(i + 1));
			String[] temp = (String[]) rateMap.get(key);
				
				addOm(item, "Month",temp[0]);
				addOm(item, "Rate",temp[1]);
				rateInfo.addChild(item);
		}
		DATASET.addChild(rateInfo);
		
		return DATASET;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}	
	
	
}
