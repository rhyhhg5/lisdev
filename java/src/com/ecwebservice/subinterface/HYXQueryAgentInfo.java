package com.ecwebservice.subinterface;

import java.util.HashMap;
import java.util.Map;

import com.ecwebservice.topinterface.CardTopInterface;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class HYXQueryAgentInfo extends CardTopInterface
{
    private SSRS resultSSRS;

    public HYXQueryAgentInfo()
    {
        super("HYXQUERYAGENT");

    }

    /** 
     * @desc 远程出单中代理人信息查询
     * @author Yuanzw
     * 创建时间：Jul 27, 2010 11:52:22 AM
     * @param       
     * @return      
     * @exception  
     */
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub

    }

    //	protected void getQueryFactor(Map conditionMap) {
    //		 agentCode = (String) conditionMap.get("AGENTCODE");
    //		 userType = (String) conditionMap.get("COMCODE");
    //		// TODO Auto-generated method stub
    //		
    //	}

    public Map prepareData()
    {
        Map resultMap = new HashMap();
        if (resultSSRS.MaxRow > 0)
        {
            Map agentInfoMap = new HashMap();
            agentInfoMap.put("MANAGECOM", resultSSRS.GetText(1, 1));
            agentInfoMap.put("NAME", resultSSRS.GetText(1, 2));
            agentInfoMap.put("SEX", resultSSRS.GetText(1, 3));
            agentInfoMap.put("BIRTHDAY", resultSSRS.GetText(1, 4));
            agentInfoMap.put("IDTYPE", "");
            agentInfoMap.put("IDNO", resultSSRS.GetText(1, 5));
            agentInfoMap.put("POSTALADDRESS", resultSSRS.GetText(1, 6));
            agentInfoMap.put("ZIPCODE", resultSSRS.GetText(1, 7));
            agentInfoMap.put("PHONT", resultSSRS.GetText(1, 8));
            agentInfoMap.put("MOBILE", resultSSRS.GetText(1, 9));
            agentInfoMap.put("EMAIL", resultSSRS.GetText(1, 10));
            agentInfoMap.put("GROUPAGENTCODE", resultSSRS.GetText(1, 11));
            agentInfoMap.put("AGENTCODE", resultSSRS.GetText(1, 12));
            resultMap.put("AGENTINFO", agentInfoMap);

        }

        return resultMap;
    }

    protected boolean checkData(Map conditionMap)
    {
        String agentCode = (String) conditionMap.get("AGENTCODE");
		AgentCodeTransformation a = new AgentCodeTransformation();
		if(!a.AgentCode(agentCode, "N")){
			System.out.println(a.getMessage());
		}
		agentCode = a.getResult();
        String comCode = (String) conditionMap.get("COMCODE");
        ExeSQL mExeSQL = new ExeSQL();
        //进行校验
        String agentSql = " select ManageCom,name,sex,birthday,idno,homeaddress,zipcode,phone,mobile,email,groupagentcode,agentcode from LAAgent laa "
            + "where 1 = 1 and laa.AgentCode = '"
            + agentCode
            + "' and laa.ManageCom like '"
            + comCode+ "%' and laa.AgentState <= '05' with ur";
        resultSSRS = mExeSQL.execSQL(agentSql);
        if (resultSSRS.MaxRow > 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    protected void getQueryFactor(Map queryFactor)
    {
        // TODO Auto-generated method stub

    }

}
