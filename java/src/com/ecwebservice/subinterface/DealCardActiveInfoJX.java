package com.ecwebservice.subinterface;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.utils.AgentCodeTransformation;
import com.mail.CreatePDF;
import com.mail.MailSender;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.WFAppntListSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFContListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealCardActiveInfoJX {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	
	private String branchcode = "";// 网点
	
	private String sendoperator = "";// 操作员

	private String messageType = "";// 报文类型

	private String cardDealType = null;// 处理方式
	
	private String dutyContent = "";// 保险责任
	
	private String IDTypeName = "";//证件类型名称
	
	private String bak1 =null;
	private String bak2 =null;
	private String bak3 =null;
	private String bak4 =null;
	private String bak5 =null;
	private String bak6 =null;
	private String bak7 =null;
	private String bak8 =null;
	private String bak9 =null;
	private String bak10 =null;
	
	private OMElement responseOME = null;
	
	private String cardNo = null;
	
	private MMap tMMap = new MMap();
	
	private String cardpassword = null;
	private LoginVerifyTool mTool = new LoginVerifyTool();
	// 保险卡信息
	private WFContListSchema contListSchema = new WFContListSchema();
	private WFContListSet contListSet = new WFContListSet();

	// 被保人信息
	private WFInsuListSet insuListSet = new WFInsuListSet();
//	private WFInsuListSchema insuListSchema = null;
	private WFInsuListSchema insuListSchema = new WFInsuListSchema();
	
	//投保人信息
	private WFAppntListSchema appntListSchema = new WFAppntListSchema();
	private WFAppntListSet appntListSet = new WFAppntListSet();
	
	//受益人信息
//	private WFBnfListSchema bnfListSchema = null;
//	private WFBnfListSet bnfListSet = new WFBnfListSet();
	
	int ContNum = 0;//报文中保险卡节点数量
	int AppntNum = 0;//报文中投保人数量
	int InsuNum = 0;//报文中被保人数量
	boolean BnfInfo = true;//判断是否由受益人信息 ps：根据受益人必须是被保人本人，所以受益人节点为空节点
	
	
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildCardActiveResponseOME("01","01", "加载激活信息失败");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			return responseOME;
		}
		if(!deal()){
			return responseOME;
		}
		if(!save()){
			return responseOME;
		}
		//发送邮件 by gzh 20110218
		if(!"04".equals(this.messageType)){
			System.out.println("=====开始发送邮件=====");
			if(!sendEmail()){
				return responseOME;
			}
		}
		return responseOME;
	}
	
	//加载卡激活信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("YSCERTACT")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDDATE")){
							this.sendDate=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDTIME")){
							this.sendTime= child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("BRANCHCODE")){
							this.branchcode=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDOPERATOR")){
							this.sendoperator=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("MESSAGETYPE")){
							this.messageType=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("CONTLIST")){
							Iterator childItem = child_element.getChildren();
							while(childItem.hasNext()){
								OMElement childOME = (OMElement) childItem.next();
								if(childOME.getLocalName().equals("ITEM")){
									ContNum++;
									Iterator child1OME = childOME.getChildren();
									while(child1OME.hasNext()){
										OMElement childleaf = (OMElement) child1OME.next();
										if(childleaf.getLocalName().equals("WRAPCODE")){
											this.contListSchema.setRiskCode(childleaf.getText());
											continue;
										}
//										if(childleaf.getLocalName().equals("WRAPNAME")){
//											
//										}
										if(childleaf.getLocalName().equals("CERTIFYCODE")){
											this.contListSchema.setCertifyCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CERTIFYNAME")){
											this.contListSchema.setCertifyName(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("ACTIVEDATE")){
											this.contListSchema.setActiveDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("MANAGECOM")){
											this.contListSchema.setManageCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("SALECHNL")){
											this.contListSchema.setSaleChnl(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AGENTCOM")){
											this.contListSchema.setAgentCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AGENTCODE")){
											AgentCodeTransformation a = new AgentCodeTransformation();
											if(!a.AgentCode(childleaf.getText(), "N")){
												System.out.println(a.getMessage());
											}
											this.contListSchema.setAgentCode(a.getResult());
											continue;
										}
										if(childleaf.getLocalName().equals("AGENTNAME")){
											this.contListSchema.setAgentCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PAYMODE")){
											this.contListSchema.setPayMode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PAYINTV")){
											this.contListSchema.setPayIntv(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PAYYEAR")){
											this.contListSchema.setPayYear(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PAYYEARFLAG")){
											this.contListSchema.setPayYearFlag(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CVALIDATE")){
											this.contListSchema.setCvaliDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CVALIDATETIME")){
											this.contListSchema.setCvaliDateTime(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("INSUYEAR")){
											this.contListSchema.setInsuYear(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("INSUYEARFLAG")){
											this.contListSchema.setInsuYearFlag(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PREM")){
											if(childleaf.getText().equals("")||childleaf.getText()==null){
												System.out.println("没有得到保费");
												//return false;
											}else{
												this.contListSchema.setPrem(new Double(childleaf.getText()).doubleValue());
												continue;
											}
											
										}
										if(childleaf.getLocalName().equals("AMNT")){
											if(childleaf.getText().equals("")||childleaf.getText()==null){
												System.out.println("没有得到保额");
												//return false;
											}else{
												this.contListSchema.setAmnt(Integer.parseInt(childleaf.getText()));
												continue;
											}
					
										}
										if(childleaf.getLocalName().equals("MULT")){
											if(childleaf.getText().equals("")||childleaf.getText()==null){
												System.out.println("没有得到档次");
												//return false;
											}else{
												this.contListSchema.setMult(Integer.parseInt(childleaf.getText()));
												continue;
											}
											
										}
										if(childleaf.getLocalName().equals("COPYS")){
											if(childleaf.getText().equals("")||childleaf.getText()==null){
												System.out.println("没有得到份数");
												//return false;
											}else{
												this.contListSchema.setCopys(Integer.parseInt(childleaf.getText()));
												continue;
											}
											
										}
										if(childleaf.getLocalName().equals("REMARK")){
											this.contListSchema.setRemark(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("BAK1")){
											this.bak1=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK2")){
											this.bak2=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK3")){
											this.bak3=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK4")){
											this.bak4=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK5")){
											this.bak5=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK6")){
											this.bak6=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK7")){
											this.bak7=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK8")){
											this.bak8=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK9")){
											this.bak9=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK10")){
											this.bak10=childleaf.getText();
											continue;
										}
									}
								this.contListSet.add(this.contListSchema);
								}
							}
						}
						
						if(child_element.getLocalName().equals("APPNTLIST")){
							Iterator child_appnt = child_element.getChildren();
							while(child_appnt.hasNext()){
								OMElement appnt_item = (OMElement) child_appnt.next();
								if(appnt_item.getLocalName().equals("ITEM")){
									AppntNum++;
									Iterator appnt_element= appnt_item.getChildren();
									while(appnt_element.hasNext()){
										OMElement appntleaf = (OMElement)appnt_element.next();
										
										if(appntleaf.getLocalName().equals("NAME")){
											this.appntListSchema.setName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("SEX")){
											this.appntListSchema.setSex(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BIRTHDAY")){
											this.appntListSchema.setBirthday(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDTYPE")){
											this.appntListSchema.setIDType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDNO")){
											this.appntListSchema.setIDNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OCCUPATIONTYPE")){
											this.appntListSchema.setOccupationType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OCCUPATIONCODE")){
											this.appntListSchema.setOccupationCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("POSTALADDRESS")){
											this.appntListSchema.setPostalAddress(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ZIPCODE")){
											this.appntListSchema.setZipCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("PHONT")){
											this.appntListSchema.setPhont(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("MOBILE")){
											this.appntListSchema.setMobile(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("EMAIL")){
											this.appntListSchema.setEmail(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GRPNAME")){
											this.appntListSchema.setGrpName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("COMPANYPHONE")){
											this.appntListSchema.setCompanyPhone(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("COMPADDR")){
											this.appntListSchema.setCompAddr(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("COMPZIPCODE")){
											this.appntListSchema.setCompZipCode(appntleaf.getText());
											continue;
										}
									}
									this.appntListSet.add(this.appntListSchema);
								}
							}
						}
						if(child_element.getLocalName().equals("INSULIST")){
							Iterator child_insu = child_element.getChildren();
							while(child_insu.hasNext()){
								OMElement insu_item = (OMElement) child_insu.next();
//								insuListSchema=new WFInsuListSchema();
								if(insu_item.getLocalName().equals("ITEM")){
									InsuNum++;
									Iterator insu_element = insu_item.getChildren();
									while(insu_element.hasNext()){
										OMElement insuleaf = (OMElement)insu_element.next();
										if(insuleaf.getLocalName().equals("INSUNO")){
											this.insuListSchema.setInsuNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("TOAPPNTRELA")){
											this.insuListSchema.setToAppntRela(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("NAME")){
											this.insuListSchema.setName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("SEX")){
											this.insuListSchema.setSex(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("BIRTHDAY")){
											this.insuListSchema.setBirthday(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDTYPE")){
											this.insuListSchema.setIDType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDNO")){
											this.insuListSchema.setIDNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("OCCUPATIONTYPE")){
											this.insuListSchema.setOccupationType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("OCCUPATIONCODE")){
											this.insuListSchema.setOccupationCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("POSTALADDRESS")){
											this.insuListSchema.setPostalAddress(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("ZIPCODE")){
											this.insuListSchema.setZipCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("PHONT")){
											this.insuListSchema.setPhont(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("MOBILE")){
											this.insuListSchema.setMobile(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("EMAIL")){
											this.insuListSchema.setEmail(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("GRPNAME")){
											this.insuListSchema.setGrpName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("COMPANYPHONE")){
											this.insuListSchema.setCompanyPhone(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("COMPADDR")){
											this.insuListSchema.setCompAddr(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("COMPZIPCODE")){
											this.insuListSchema.setCompZipCode(insuleaf.getText());
											continue;
										}
									}
									this.insuListSet.add(this.insuListSchema);
								}
							}
						}
						if(child_element.getLocalName().equals("BNFLIST")){
							Iterator child_bnf = child_element.getChildren();
							if(child_bnf.hasNext()) BnfInfo=false;//判断受益人节点是否为空
//							while(child_bnf.hasNext()){
//								OMElement bnf_item = (OMElement)child_bnf.next();
//								bnfListSchema=new WFBnfListSchema();
//								if(bnf_item.getLocalName().equals("ITEM")){
//									Iterator bnf_element = bnf_item.getChildren();
//									while(bnf_element.hasNext()){
//										OMElement bnfleaf = (OMElement)bnf_element.next();
//										if(bnfleaf.getLocalName().equals("TOINSUNO")){
//											this.bnfListSchema.setToInsuNo(bnfleaf.getText());
//											continue;
//										}
//										if(bnfleaf.getLocalName().equals("TOINSURELA")){
//											this.bnfListSchema.setToInsuRela(bnfleaf.getText());
//											continue;
//										}
//										if(bnfleaf.getLocalName().equals("BNFTYPE")){
//											this.bnfListSchema.setBnfType(bnfleaf.getText());
//											continue;
//										}
//										if(bnfleaf.getLocalName().equals("BNFGRADE")){
//											this.bnfListSchema.setBnfGrade(bnfleaf.getText());
//											continue;
//										}
//										if(bnfleaf.getLocalName().equals("BNFRATE")){
//											if(!bnfleaf.getText().equals("") && bnfleaf.getText()!=null){
//												this.bnfListSchema.setBnfRate(new Double(bnfleaf.getText()).doubleValue());
//												continue;
//											}else{
//												System.out.println("没有得到受益比例");
//												//return false;
//											}	
//										}
//										if(bnfleaf.getLocalName().equals("NAME")){
//											this.bnfListSchema.setName(bnfleaf.getText());
//											continue;
//										}
//										if(bnfleaf.getLocalName().equals("SEX")){
//											this.bnfListSchema.setSex(bnfleaf.getText());
//											continue;
//										}
//										if(bnfleaf.getLocalName().equals("BIRTHDAY")){
//											this.bnfListSchema.setBirthday(bnfleaf.getText());
//											continue;
//										}
//										if(bnfleaf.getLocalName().equals("IDTYPE")){
//											this.bnfListSchema.setIDType(bnfleaf.getText());
//											continue;
//										}
//										if(bnfleaf.getLocalName().equals("IDNO")){
//											this.bnfListSchema.setIDNo(bnfleaf.getText());
//											continue;
//										}
//									}
//									this.bnfListSet.add(this.bnfListSchema);
//								}
//							}
						}
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验卡激活信息
	 * @return
	 */
	private boolean check(){
		if(!checkInfo()){
			System.out.println("校验基本信息失败");
			return false;
		}
		if(!chkMingYa()){
			System.out.println("明亚校验信息失败");
			return false;
		}
		return true;
	}
	
	/**
	 * 处理卡激活信息
	 * @return
	 */ 
	private boolean deal(){
		try{
			WFContListSet tContListSet = new WFContListSet();
			WFAppntListSet tAppntList = new WFAppntListSet();
			WFInsuListSet tInsuList = new WFInsuListSet();
			WFBnfListSet tBnfList = new WFBnfListSet();
			if(this.contListSet.size()!=1){
				System.out.println("险种数目有误");
				responseOME = buildCardActiveResponseOME("01","81", "险种数目有误!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "81", "险种数目有误!");
				return false;
			}
			String insuYear = null;
			String insuYearFlag = null;
			String insuYearSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
				+ contListSchema.getRiskCode()
				+ "' and calfactor='InsuYear'   with ur";
			String insuYearFlagSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
				+  contListSchema.getRiskCode()
				+ "' and calfactor='InsuYearFlag'   with ur";
			insuYear = new ExeSQL().getOneValue(insuYearSql);
			insuYearFlag = new ExeSQL().getOneValue(insuYearFlagSql);
				this.contListSchema.setBatchNo(this.batchno);
				this.contListSchema.setSendDate(this.sendDate);
				this.contListSchema.setSendTime(this.sendTime);
				this.contListSchema.setBranchCode(this.branchcode);
				this.contListSchema.setSendOperator(this.sendoperator);
				this.contListSchema.setMessageType(this.messageType);//?????
				this.contListSchema.setAppntNo("1");
				this.contListSchema.setCardDealType("01");
				this.contListSchema.setInsuYear(Integer.parseInt(insuYear));
				this.contListSchema.setInsuYearFlag(insuYearFlag);
				tContListSet.add(this.contListSchema);
				tMMap.put(tContListSet, SysConst.INSERT);
				
				for(int i=1;i<=this.appntListSet.size();i++){
					WFAppntListSchema tWFAppntListSchema = new WFAppntListSchema();
					tWFAppntListSchema = appntListSet.get(i);
					tWFAppntListSchema.setCardNo(this.cardNo);
					tWFAppntListSchema.setBatchNo(this.batchno);
					tWFAppntListSchema.setAppntNo("1");
					tAppntList.add(tWFAppntListSchema);
				}
				tMMap.put(tAppntList, SysConst.INSERT);
				for(int i=1;i<=this.insuListSet.size();i++){
					WFInsuListSchema tWFInsuListSchema = new WFInsuListSchema();
					tWFInsuListSchema = insuListSet.get(i);
					tWFInsuListSchema.setCardNo(this.cardNo);
					tWFInsuListSchema.setBatchNo(this.batchno);
					tInsuList.add(tWFInsuListSchema);
				}
				tMMap.put(tInsuList, SysConst.INSERT);
//				for(int i=1;i<=this.bnfListSet.size();i++){
//					WFBnfListSchema tWFBnfListSchema = new WFBnfListSchema();
//					tWFBnfListSchema= bnfListSet.get(i);
//					tWFBnfListSchema.setCardNo(cardNo);
//					tWFBnfListSchema.setBatchNo(this.batchno);
//					tBnfList.add(tWFBnfListSchema);
//				}
//				tMMap.put(tBnfList, SysConst.INSERT);
				//卡单外部激活信息
				ExternalActive tExternalActive = new ExternalActive();
				LICardActiveInfoListSet exCardActiveInfoListSet = tExternalActive.writeLICardActiveInfoList(contListSchema, insuListSet);
				
				//state In Lzcard 需要修改为14.
				String[] updateArr = tExternalActive.getUpdateLzcardSql(cardNo);
				String stateInLzcard = updateArr[1];
				//判断单证号是否有效
				if("2".equals(stateInLzcard)||"10".equals(stateInLzcard)||"11".equals(stateInLzcard)||"13".equals(stateInLzcard)){
					System.out.println("单证号有效.");
				}else{
					responseOME = buildCardActiveResponseOME("01","21", "单证号无效--状态!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "21", "单证号无效!");
					return false;
				}
				if("11".equals(stateInLzcard)||"10".equals(stateInLzcard)){
					System.out.println("state In Lzcard 需要修改为14.");
					String modifyStateSql = updateArr[0];
					tMMap.put(modifyStateSql, SysConst.UPDATE);
				}
				tMMap.put(exCardActiveInfoListSet, SysConst.INSERT);
		
		}catch(Exception e){
			System.out.println("处理卡激活信息异常");
			e.printStackTrace();
			return false;
		}
									
		return true;
	}
	
	/**
	 *校验基本信息 
	 * @return
	 */
	private boolean checkInfo(){
		if(this.batchno ==null || this.batchno.equals("")){
			responseOME = buildCardActiveResponseOME("01","21", "没有得到批次号!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		boolean validBatchNo = mTool.isValidBatchNo(batchno);
		if (!validBatchNo) {
			// 批次号不唯一
			responseOME = buildCardActiveResponseOME("01","22", "批次号不唯一!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "22", "批次号不唯一!");
			return false;
		}
		if(this.sendDate==null || this.sendDate.equals("")){
			responseOME = buildCardActiveResponseOME("01","23", "没有得到发送日期!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "23", "没有得到发送日期!");
			return false;
		}
		if(this.sendTime==null || this.sendTime.equals("")){
			responseOME = buildCardActiveResponseOME("01","24", "没有得到发送时间!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "24", "没有得到发送时间!");
			return false;
		}
		if(this.branchcode==null || this.branchcode.equals("")){
			responseOME = buildCardActiveResponseOME("01","25", "没有得到网点!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "25", "没有得到网点!");
			return false;
		}
		if(this.sendoperator==null || this.sendoperator.equals("")){
			responseOME = buildCardActiveResponseOME("01","26", "没有得到发送者!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "26", "没有得到发送者!");
			return false;
		}
		if(this.contListSchema.getCertifyCode().equals("") || this.contListSchema.getCertifyCode()==null){
			responseOME = buildCardActiveResponseOME("01","27", "没有得到单证编码!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "27", "没有得到单证编码!");
			return false;	
		}
		try{
			String searchCardnoSQL = "select lzcn.cardno,lzcn.cardpassword from LZCardNumber lzcn "
				 +"inner join LZCard lzc on lzcn.CardType = lzc.SubCode "
				                      +"and lzc.StartNo = lzcn.CardSerNo "
				                      +"and lzc.EndNo = lzcn.CardSerNo "
				 +"where  lzc.State in ('10', '11') "
				   +"and lzc.certifycode = '"+this.contListSchema.getCertifyCode()+"' fetch first 1 rows only ";
			SSRS tSSRS = new SSRS();
			tSSRS = new ExeSQL().execSQL(searchCardnoSQL);
			if(tSSRS!=null){
				this.cardNo = tSSRS.GetText(1, 1);
				this.cardpassword = tSSRS.GetText(1, 2);
			}
			LisIDEA tLisIdea = new LisIDEA();
			cardpassword=tLisIdea.decryptString(cardpassword);
			if(cardNo.equals("") || cardNo ==null ||cardpassword.equals("") || cardpassword ==null){
				responseOME = buildCardActiveResponseOME("01","28", "没有得到CardNo或cardpassword!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "28", "没有得到CardNo或cardpassword!");
				return false;
			}
		}catch(Exception e){
			System.out.println("获取卡号失败");
			e.printStackTrace();
			return false;
		}
		contListSchema.setCardNo(this.cardNo);
		contListSchema.setPassword(cardpassword);
		
		if(this.contListSchema.getRiskCode().equals("") || this.contListSchema.getRiskCode()==null){
			responseOME = buildCardActiveResponseOME("01","29", "没有得到套餐编码!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "29", "没有得到套餐编码!");
			return false;	
		}
		String riskcodeSQL = "select 1 from LMCardRisk lmcr " +
				"where lmcr.certifycode='"+this.contListSchema.getCertifyCode()+"' and lmcr.riskcode='"
					+this.contListSchema.getRiskCode()+"' and risktype='W'";
		String riskcheck = new ExeSQL().getOneValue(riskcodeSQL);
		if(!riskcheck.equals("1")){
			responseOME = buildCardActiveResponseOME("01","30", "单证编码与套餐编码不匹配!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "30", "单证编码与套餐编码不匹配!");
			return false;
		}
		if(this.contListSchema.getActiveDate().equals("") || this.contListSchema.getActiveDate()==null){
			responseOME = buildCardActiveResponseOME("01","31", "没有得到激活时间!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "31", "没有得到激活时间!");
			return false;
		}
		if(this.contListSchema.getCvaliDate().equals("") || this.contListSchema.getCvaliDate()==null){
			responseOME = buildCardActiveResponseOME("01","32", "没有得到生效时间!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "32", "没有得到生效时间!");
			return false;
		}
		if(this.contListSchema.getCopys()>1){
			responseOME = buildCardActiveResponseOME("01","33", "保险卡份数不为1!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "33", "保险卡份数不为1!");
			return false;
		}
		
		if(this.appntListSchema.getName().equals("") || this.appntListSchema.getName()==null){
			responseOME = buildCardActiveResponseOME("01","34", "没有得到投保人姓名!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "34", "没有得到投保人姓名!");
			return false;
		}
		if(this.appntListSchema.getSex().equals("") || this.appntListSchema.getSex()==null){
			responseOME = buildCardActiveResponseOME("01","35", "没有得到投保人性别!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "35", "没有得到投保人性别!");
			return false;
		}
		if(this.appntListSchema.getBirthday().equals("") || this.appntListSchema.getBirthday()==null){
			responseOME = buildCardActiveResponseOME("01","36", "没有得到投保人出生日期!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "36", "没有得到投保人出生日期!");
			return false;
		}
		if(this.appntListSchema.getIDType().equals("") || this.appntListSchema.getIDType()==null){
			responseOME = buildCardActiveResponseOME("01","37", "没有得到投保人ID类型!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "37", "没有得到投保人ID类型!");
			return false;
		}
		if(this.appntListSchema.getIDNo().equals("") || this.appntListSchema.getIDNo()==null){
			responseOME = buildCardActiveResponseOME("01","38", "没有得到投保人ID号码!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "38", "没有得到投保人ID号码!");
			return false;
		}
		//被保人信息判断
		if(this.insuListSchema.getInsuNo().equals("") || this.insuListSchema.getInsuNo()==null){
			responseOME = buildCardActiveResponseOME("01","39", "没有得到被保人编号!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "39", "没有得到被保人编号!");
			return false;
		}
		if(this.insuListSchema.getName().equals("") || this.insuListSchema.getName()==null){
			responseOME = buildCardActiveResponseOME("01","40", "没有得到被保人姓名!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "40", "没有得到被保人姓名!");
			return false;
		}
		if(this.insuListSchema.getSex().equals("") || this.insuListSchema.getSex()==null){
			responseOME = buildCardActiveResponseOME("01","41", "没有得到被保人性别!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "41", "没有得到被保人性别!");
			return false;
		}
		if(this.insuListSchema.getBirthday().equals("") || this.insuListSchema.getBirthday()==null){
			responseOME = buildCardActiveResponseOME("01","42", "没有得到被保人出生日期!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "42", "没有得到被保人出生日期!");
			return false;
		}
		if(this.insuListSchema.getIDType().equals("") || this.insuListSchema.getIDType()==null){
			responseOME = buildCardActiveResponseOME("01","43", "没有得到被保人ID类型!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "43", "没有得到被保人ID类型!");
			return false;
		}
		if(this.insuListSchema.getIDNo().equals("") || this.insuListSchema.getIDNo()==null){
			responseOME = buildCardActiveResponseOME("01","44", "没有得到被保人ID号码!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "44", "没有得到被保人ID号码!");
			return false;
		}
		if(this.bak6==null || this.bak6.equals("")){
			responseOME = buildCardActiveResponseOME("01","45", "被保人国籍为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "45", "被保人国籍为空!");
			return false;
		}
		if(this.bak7==null || this.bak7.equals("")){
			responseOME = buildCardActiveResponseOME("01","46", "被保险人与投保人的关系为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "46", "被保险人与投保人的关系为空!");
			return false;
		}
		if(this.bak4==null || this.bak4.equals("")){
			responseOME = buildCardActiveResponseOME("01","47", "其余告知+同意协议标志集为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "47", "其余告知+同意协议标志集为空!");
			return false;
		}
		if(this.bak2==null || this.bak2.equals("")){
			responseOME = buildCardActiveResponseOME("01","48", "被保人身高为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "48", "被保人身高为空!");
			return false;
		}
		if(this.bak3==null || this.bak3.equals("")){
			responseOME = buildCardActiveResponseOME("01","49", "被保人体重为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "49", "被保人体重为空!");
			return false;
		}
		
		
//		if(this.bnfListSchema.getToInsuNo().equals("") || this.bnfListSchema.getToInsuNo()==null){
//			responseOME = buildCardActiveResponseOME("01","61", "没有得到所属被保人!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"100", "61", "没有得到所属被保人!");
//			return false;
//		}
//		if(this.bnfListSchema.getToInsuRela().equals("") || this.bnfListSchema.getToInsuRela()==null){
//			responseOME = buildCardActiveResponseOME("01","62", "没有得到所属被保人关系!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"100", "62", "没有得到所属被保人关系!");
//			return false;
//		}
//		if(this.bnfListSchema.getBnfType().equals("") || this.bnfListSchema.getBnfType()==null){
//			responseOME = buildCardActiveResponseOME("01","63", "没有得到受益人类型!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"100", "63", "没有得到受益人类型!");
//			return false;
//		}
//		if(this.bnfListSchema.getBnfGrade().equals("") || this.bnfListSchema.getBnfGrade()==null){
//			responseOME = buildCardActiveResponseOME("01","64", "没有得到受益顺位!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"100", "64", "没有得到受益顺位!");
//			return false;
//		}
//		if(this.bnfListSchema.getName().equals("") || this.bnfListSchema.getName()==null){
//			responseOME = buildCardActiveResponseOME("01","65", "没有得到受益人姓名!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"100", "65", "没有得到受益人姓名!");
//			return false;
//		}
		return true;
	}
	
	//提交卡激活信息
	private boolean save(){
		if(this.messageType.equals("04")){
			responseOME = buildCardActiveResponseOME("00","0", "明亚校验类型，不做激活处理。");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "00",
					"100", "0", "明亚校验类型，不做激活处理。");
			return true;
		}
		try{
			String cheCardNoSQL="select 1 from WFContList where cardno='"+cardNo+"'";
			String cheCardNoRes = new ExeSQL().getOneValue(cheCardNoSQL);
			if(cheCardNoRes.equals("1")){
				responseOME = buildCardActiveResponseOME("01","80", "cardNo不唯一!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "80", "cardNo不唯一!");
				return false;
			}
			lockCont(cardNo);
			VData v = new VData();
			v.add(tMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				System.out.println("数据提交失败************************");
				responseOME = buildCardActiveResponseOME("01","6", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "6", "数据提交失败!");
				return false;
			}
			responseOME = buildCardActiveResponse("00",this.cardNo,this.cardpassword);
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "00",
					"100", "0", "数据提交成功!");
		}catch(Exception e){
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	private OMElement buildCardActiveResponseOME(String state, String errCode,String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("YSCERTACT", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "BRANCHCODE", this.branchcode);
		tool.addOm(DATASET, "SENDOPERATOR", this.sendoperator);
		tool.addOm(DATASET, "MESSAGETYPE", this.messageType);

		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}
	
	
	private OMElement buildCardActiveResponse(String state,String cardno, String password) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("YSCERTACT", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "BRANCHCODE", this.branchcode);
		tool.addOm(DATASET, "SENDOPERATOR", this.sendoperator);

		tool.addOm(DATASET, "CARDNO", cardno);
		tool.addOm(DATASET, "PASSWORD", password);
		
		tool.addOm(DATASET, "STATE", state);


		return DATASET;
	}

    /**
     * 明亚产品校验
     * @return
     */
    private boolean chkMingYa()
    {
        // 只有明亚产品才进行校验。
//        if (!this.cardNo.equals("101"))
//        {
//            return true;
//        }
        // --------------------
    	//业务类型不可为空
    	if(this.messageType==null || this.messageType.equals("")){
			responseOME = buildCardActiveResponseOME("01","82", "没有得到业务类型!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "82", "没有得到业务类型!");
			return false;
		}
//      投保人联系电话或手机至少填写一项
		if((this.appntListSchema.getPhont()==null || this.appntListSchema.getPhont().equals(""))&& 
				(this.appntListSchema.getMobile()==null || this.appntListSchema.getMobile().equals(""))){
			responseOME = buildCardActiveResponseOME("01","50", "投保人的联系电话和手机至少填写一项!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "50", "投保人的联系电话和手机至少填写一项!");
			return false;
		}
//		if(this.appntListSchema.getPostalAddress()==null || this.appntListSchema.getPostalAddress().equals("")){
//			responseOME = buildCardActiveResponseOME("01","51", "没有得到投保人的联系地址!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"100", "51", "没有得到投保人的联系地址!");
//			return false;
//		}
//		if(this.appntListSchema.getZipCode()==null || this.appntListSchema.getZipCode().equals("")){
//			responseOME = buildCardActiveResponseOME("01","52", "没有得到投保人的邮编!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"100", "52", "没有得到投保人的邮编!");
//			return false;
//		}
//      判断投保人邮箱是否为空
		if(this.appntListSchema.getEmail()==null || this.appntListSchema.getEmail().equals("")){
		responseOME = buildCardActiveResponseOME("01","53", "没有得到投保人的电子邮箱!");
		System.out.println("responseOME:" + responseOME);
		LoginVerifyTool.writeLog(responseOME, batchno,
				messageType, sendDate, sendTime, "01",
				"100", "53", "没有得到投保人的电子邮箱!");
		return false;
	    }		
		
		//投保人身份证判断
		if(this.appntListSchema.getIDType().equals("1")){
			String IDNo=this.appntListSchema.getIDNo();//获取投保人身份证号
			if(IDNo.length()==18){//18为身份证校验，校验出生年月日
				String birthday = this.appntListSchema.getBirthday().replaceAll("-", "");//获取出生日期 格式为20101010
				String idBirthday=IDNo.substring(6, 14);//获取身份证中的出生年月日
//				birthday = getDate(birthday,"0");
//				idBirthday = getDate(idBirthday,"0");
			    if(!idBirthday.equals(birthday)){
			    	responseOME = buildCardActiveResponseOME("01","54", "投保人身份证中包含的出生日期与获取的出生日期不符!");
			    	System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "54", "投保人身份证中包含的出生日期与获取的出生日期不符!");
					return false;
			    }else{
			    	int sexid = Integer.parseInt(IDNo.substring(16, 17));
			    	if((sexid%2)!=(Integer.parseInt(this.appntListSchema.getSex())%2)){
			    		responseOME = buildCardActiveResponseOME("01","55", "投保人身份证中包含的性别信息与获取的性别不符!");
				    	System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "55", "投保人身份证中包含的性别信息与获取的性别不符!");
						return false;
			    	}
			    }
			}else if(IDNo.length()==15){//15位身份证校验，校验月日
				String birthday = this.appntListSchema.getBirthday().replaceAll("-", "").substring(2, 8);
				String idBirthday = IDNo.substring(6, 12);
				if(!idBirthday.equals(birthday)){
					responseOME = buildCardActiveResponseOME("01","56", "投保人身份证中包含的出生日期与获取的出生日期不符!");
			    	System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "56", "投保人身份证中包含的出生日期与获取的出生日期不符!");
					return false;
				}else{
					int sexid = Integer.parseInt(IDNo.substring(14, 15));
			    	if((sexid%2)!=(Integer.parseInt(this.appntListSchema.getSex())%2)){
			    		responseOME = buildCardActiveResponseOME("01","57", "投保人身份证中包含的性别信息与获取的性别不符!");
				    	System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "57", "投保人身份证中包含的性别信息与获取的性别不符!");
						return false;
			    	}
				}
			}
			
		}
        // 投保人年龄≥18周岁，校验通过
        try
        {
            int appntAge = Integer.parseInt(bak5);
            if (appntAge < 18)
            {
                responseOME = buildCardActiveResponseOME("01","58", "投保人年龄不能小于18周岁!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "58", "投保人年龄不能小于18周岁!");
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("投保人年龄传入非数字类型数据。");
            return false;
        }
        // --------------------

        
//      被保人身份证判断
		if(this.insuListSchema.getIDType().equals("1")){
			String IDNo=this.insuListSchema.getIDNo();//获取被保人身份证号
			if(IDNo.length()==18){//18位身份证校验，校验出生年月日
				String birthday = this.insuListSchema.getBirthday().replaceAll("-", "");//获取出生日期 格式为20101010
				String idBirthday=IDNo.substring(6, 14);//获取身份证中的出生年月日
//				birthday = getDate(birthday,"0");//18位身份证 标记0
//				idBirthday = getDate(idBirthday,"0");//18位身份证 标记0
			    if(!idBirthday.equals(birthday)){
			    	responseOME = buildCardActiveResponseOME("01","59", "被保人身份证中包含的出生日期与获取的出生日期不符!");
			    	System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "59", "被保人身份证中包含的出生日期与获取的出生日期不符!");
					return false;
			    }else{
			    	int sexid = Integer.parseInt(IDNo.substring(16, 17));
			    	if((sexid%2)!=(Integer.parseInt(this.insuListSchema.getSex())%2)){
			    		responseOME = buildCardActiveResponseOME("01","60", "被保人身份证中包含的性别信息与获取的性别不符!");
				    	System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "60", "被保人身份证中包含的性别信息与获取的性别不符!");
						return false;
			    	}
			    }
			}else if(IDNo.length()==15){//15位身份证校验，校验月日
				String birthday = this.insuListSchema.getBirthday().replaceAll("-", "").substring(2, 8);
				String idBirthday = IDNo.substring(6, 12);
				if(!idBirthday.equals(birthday)){
					responseOME = buildCardActiveResponseOME("01","61", "被保人身份证中包含的出生日期与获取的出生日期不符!");
			    	System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "61", "被保人身份证中包含的出生日期与获取的出生日期不符!");
					return false;
				}else{
					int sexid = Integer.parseInt(IDNo.substring(14, 15));
			    	if((sexid%2)!=(Integer.parseInt(this.insuListSchema.getSex())%2)){
			    		responseOME = buildCardActiveResponseOME("01","62", "被保人身份证中包含的性别信息与获取的性别不符!");
				    	System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "62", "被保人身份证中包含的性别信息与获取的性别不符!");
						return false;
			    	}
				}
			}
			
		}
        
        // 被保险人30天≤年龄＜20周岁，校验通过 20110318 需求由<18周岁变为<20周岁
        Date insuBrithday = new FDate().getDate(this.insuListSchema.getBirthday());
        if (insuBrithday == null)
        {
            responseOME = buildCardActiveResponseOME("01","63", "被保人生日不符合YYYY-MM-DD格式!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "63", "被保人生日不符合YYYY-MM-DD格式。!");
            return false;
        }
        Calendar tIBDate = Calendar.getInstance();
        tIBDate.setTime(insuBrithday);

        Date actDate = new FDate().getDate(this.contListSchema.getActiveDate());
        if (actDate == null)
        {
            responseOME = buildCardActiveResponseOME("01","64", "系统激活时间不符合YYYY-MM-DD格式!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "64", "系统激活时间不符合YYYY-MM-DD格式!");
            return false;
        }
        Calendar tActDate = Calendar.getInstance();
        tActDate.setTime(actDate);

        Calendar t20YBDate = (Calendar) tIBDate.clone();
        t20YBDate.add(Calendar.YEAR, 20);
        Calendar t30DBDate = (Calendar) tIBDate.clone();
        t30DBDate.add(Calendar.DATE, 29);
        if (tActDate.after(t20YBDate) || tActDate.before(t30DBDate))
        {
            responseOME = buildCardActiveResponseOME("01","65", "被保险人需：30天≤年龄＜20周岁!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "65", "被保险人需：30天≤年龄＜20周岁!");
            return false;
        }
        // --------------------

        // 填写国籍仅为中国大陆时校验通过
        if (!"ML".equals(bak6))
        {
            responseOME = buildCardActiveResponseOME("01","66", "被保人国籍仅能为中国大陆!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "66", "被保人国籍仅能为中国大陆!");
            return false;
        }
        // --------------------

        // 仅当关系录入为子女时，校验通过
        if (!"01".equals(bak7))
        {
            responseOME = buildCardActiveResponseOME("01","67", "被保人与投保人关系仅能为子女!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "67", "被保人与投保人关系仅能为子女!");
            return false;
        }
        // --------------------

        // 其余告知+同意协议标志集校验，告知必须均为否，且同意协议，校验通过
        if (!"111111111".equals(bak4))
        {
            responseOME = buildCardActiveResponseOME("01","68", "告知必须均为否，且同意协议!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "68", "告知必须均为否，且同意协议!");
            return false;
        }
		//本险种保险金受益人为被保险人本人 判断条件IDNo 
//		if(this.insuListSchema.getIDType().equals(this.bnfListSchema.getIDType())){
//			if(!this.insuListSchema.getIDNo().equals(this.bnfListSchema.getIDNo())){
//				responseOME = buildCardActiveResponseOME("01","69", "本险种保险金受益人应为被保险人本人!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,
//						messageType, sendDate, sendTime, "01",
//						"100", "69", "本险种保险金受益人应为被保险人本人!");
//				return false;
//			}
//		}
        // --------------------

        // 系统控制录入的保单生效日必须为录入日期次日至录入日期后30个自然日内
        Date tCValidate = new FDate().getDate(this.contListSchema.getCvaliDate());
        if (tCValidate == null)
        {
            responseOME = buildCardActiveResponseOME("01","70", "保单生效日期不符合YYYY-MM-DD格式!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "70", "保单生效日期不符合YYYY-MM-DD格式!");
            return false;
        }
        Calendar tCalCValidate = Calendar.getInstance();
        tCalCValidate.setTime(tCValidate);

        Calendar tInputDate = (Calendar) tActDate.clone();
        tInputDate.add(Calendar.DATE, 32);
        if (tCalCValidate.after(tInputDate))
        {
            responseOME = buildCardActiveResponseOME("01","71", "系统录入的保单生效日必须为录入日期次日至录入日期后30个自然日内!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "71", "系统录入的保单生效日必须为录入日期次日至录入日期后30个自然日内!");
            return false;
        }
        // --------------------

        // 同一被保险人该险种只能承保一份
		String tStrSql =" select count(1) from licardactiveinfolist " + " where Cardtype = '" + this.contListSchema.getRiskCode()+"'"+
	     				" and IDType = '"+insuListSchema.getIDType()+"'  and IDNO = '" + this.insuListSchema.getIDNo() + "'" +
	     				" and Name = '"+insuListSchema.getName()+"' and Sex = '"+insuListSchema.getSex()+"' and Birthday = '"+insuListSchema.getBirthday()+"'";
	    String tInsuCount = new ExeSQL().getOneValue(tStrSql);
	    if (!"0".equals(tInsuCount))
	    {
	        responseOME = buildCardActiveResponseOME("01","72", "同一被保险人该产品只能承保一份!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "72", "同一被保险人该产品只能承保一份!");
	        return false;
	    }
        

        // --------------------

        // 身高/体重/bmi校验
        try
        {
            double sg = Double.parseDouble(bak2);
            double tz = Double.parseDouble(bak3);
            String birthday = this.insuListSchema.getBirthday();
            String year = birthday.substring(0, birthday.indexOf("-"));
            String month = birthday.substring(birthday.indexOf("-") + 1, birthday.indexOf("-") + 3);
            String day = birthday.substring(birthday.lastIndexOf("-") + 1);

            String today = this.contListSchema.getActiveDate();
            String tyear = today.substring(0, today.indexOf("-"));
            String tmonth = today.substring(today.indexOf("-") + 1, today.indexOf("-") + 3);
            String tday = today.substring(today.lastIndexOf("-") + 1);

            int age = Integer.parseInt(tyear) - Integer.parseInt(year);
            if (Integer.parseInt(tmonth) == Integer.parseInt(month))
            {
                if (Integer.parseInt(tday) < Integer.parseInt(day))
                {
                    age = age - 1;
                }
            }
            else if (Integer.parseInt(tmonth) < Integer.parseInt(month))
            {
                age = age - 1;
            }
            System.out.println("被保人年龄！ ： " + age);

            if (age >= 12)
            {
                double bmi = (1.0d * tz) / (sg * 0.01 * sg * 0.01);
                if ("1".equals(this.insuListSchema.getSex()) && (bmi < 17 || bmi > 26))
                {
                    responseOME = buildCardActiveResponseOME("01","79", "男性BMI值（体重（Kg）/身高（m）^2）超出承保范围17≤BMI≤26!");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "79", "男性BMI值（体重（Kg）/身高（m）^2）超出承保范围17≤BMI≤26!");
                    return false;
                }
                else if ("2".equals(this.insuListSchema.getSex()) && (bmi < 16 || bmi > 25))
                {
                    responseOME = buildCardActiveResponseOME("01","79", "女性BMI值（体重（Kg）/身高（m）^2）超出承保范围16≤BMI≤25!");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "79", "女性BMI值（体重（Kg）/身高（m）^2）超出承保范围16≤BMI≤25!");
                    return false;

                }
                else if (!"1".equals(this.insuListSchema.getSex()) && !"2".equals(this.insuListSchema.getSex()))
                {
                    responseOME = buildCardActiveResponseOME("01","79", "被保人性别填写有误!");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "79", "被保人性别填写有误!");
                    return false;
                }
            }
            else if (age >= 7 && age < 12)
            {
                double tSsg = age * 6 + 77;
                double tStz = (age * 7 - 5) * 0.5d;

                if (sg > (tSsg * 1.3))
                {
                    responseOME = buildCardActiveResponseOME("01","79", "身高（m）超过正常范围30%。正常身高计算公式：年龄*6+77!");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "79", "身高（m）超过正常范围30%。正常身高计算公式：年龄*6+77!");
                    return false;
                }

                if (tz > (tStz * 1.3))
                {

                    responseOME = buildCardActiveResponseOME("01","79", "体重（Kg）超过正常范围30%。正常体重计算公式：（年龄*7-5）*0.5!");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "79", "体重（Kg）超过正常范围30%。正常体重计算公式：（年龄*7-5）*0.5!");
                    return false;
                }
            }
            else if (age >= 0 && age <= 6)
            {
                double tSsg = age * 6 + 77;
                double tStz = age * 2 + 8d;

                if (sg > (tSsg * 1.3))
                {
                    responseOME = buildCardActiveResponseOME("01","79", "身高（m）超过正常范围30%。正常身高计算公式：年龄*6+77!");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "79", "身高（m）超过正常范围30%。正常身高计算公式：年龄*6+77!");
                    return false;
                }

                if (tz > (tStz * 1.3))
                {
                    responseOME = buildCardActiveResponseOME("01","79", "体重（Kg）超过正常范围30%。正常体重计算公式：年龄*2+8!");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "79", "体重（Kg）超过正常范围30%。正常体重计算公式：年龄*2+8!");
                    return false;
                }
            }
        }        
        catch (Exception e)
        {
            e.printStackTrace();
            responseOME = buildCardActiveResponseOME("01","79", "身高或体重信息不为数值，或出生日期/激活日期不符合YYYY-MM-DD格式!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "79", "身高或体重信息不为数值，或出生日期/激活日期不符合YYYY-MM-DD格式!");
            return false;
        }
        //20101103  by gzh 判断
        if(ContNum>1){//判断报文中卡信息节点是否唯一
        	responseOME = buildCardActiveResponseOME("01","83", "报文中卡信息不唯一!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "83", "报文中卡信息不唯一!");
            return false;
        }
        if(AppntNum>1){//判断报文中投保人是否唯一
        	responseOME = buildCardActiveResponseOME("01","84", "报文中投保人不唯一!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "83", "报文中投保人不唯一!");
            return false;
        }
        if(InsuNum>1){//判断报文中被保人是否唯一
        	responseOME = buildCardActiveResponseOME("01","85", "报文中被保人不唯一!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "85", "报文中被保人不唯一!");
            return false;
        }
        if(!BnfInfo){//判断报文中被保人是否唯一
        	responseOME = buildCardActiveResponseOME("01","86", "报文中受益人节点不为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "86", "报文中受益人节点不为空!");
            return false;
        }
        // --------------------

        return true;
    }
	
	/**
     * 锁定激活卡号。
     * @param cLCContSchema
     * @return
     */
    private MMap lockCont(String cardno)
    {
        MMap tMMap = null;

        // 签单锁定标志为：“SC”
        String tLockNoType = "WX";//网销锁定
        // -----------------------

        // 锁定有效时间
        String tAIS = "300";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cardno);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return tMMap;
    }
    /**
     * 获取保险责任。
     * @param cLCContSchema
     * @return
     */
    private boolean getDutyContent(){
    	String getDutyCodesql = "select dutycode,calfactorvalue from ldriskdutywrap where riskwrapcode='"+contListSchema.getRiskCode()+"' and Calfactor = 'Amnt'";
    	SSRS tSSRS = new ExeSQL().execSQL(getDutyCodesql);
    	if(tSSRS == null || tSSRS.getMaxRow()<=0){
    		responseOME = buildCardActiveResponseOME("01","101", "保单已承保，但发送电子保单邮件失败，请与客服联系，补发电子保单!");
			System.out.println("responseOME:" + responseOME+" 原因：根据险种编码获取保险责任失败！");
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"101", "86", "保单已承保，但发送电子保单邮件失败!");
            return false;
    	}
    	String getDutyName = "select dutyname from lmduty where dutycode='"+tSSRS.GetText(1, 1)+"'";
    	SSRS tSSRS1 = new ExeSQL().execSQL(getDutyName);
    	if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
    		responseOME = buildCardActiveResponseOME("01","101", "保单已承保，但发送电子保单邮件失败，请与客服联系，补发电子保单!");
			System.out.println("responseOME:" + responseOME+" 原因：根据险种编码获取保险责任失败！");
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"101", "86", "保单已承保，但发送电子保单邮件失败,请与客服联系，补发电子保单!");
            return false;
    	}
    	dutyContent = tSSRS1.GetText(1, 1)+tSSRS.GetText(1, 2)+"元";
    	return true;
    }
    /**
     * 发送邮件。
     * @param cLCContSchema
     * @return
     */
    private boolean sendEmail(){
    	if(!getDutyContent()){
            return false;
		}
    	if(!getIDTypeName()){
            return false;
		}
    	LICardActiveInfoListDB tLICardActiveInfoListDB = new LICardActiveInfoListDB();
		tLICardActiveInfoListDB.setCardNo(cardNo);
		LICardActiveInfoListSet tLICardActiveInfoListSet = tLICardActiveInfoListDB.query();
		LICardActiveInfoListSchema tLICardActiveInfoListSchema = tLICardActiveInfoListSet.get(1);
		if(tLICardActiveInfoListSchema == null){
			responseOME = buildCardActiveResponseOME("01","101", "保单已承保，但发送电子保单邮件失败，请与客服联系，补发电子保单!");
			System.out.println("responseOME:" + responseOME+" 原因：获取licardactiveinfolist生效日期和失效日期失败！");
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"101", "86", "保单已承保，但发送电子保单邮件失败,请与客服联系，补发电子保单!");
            return false;
		}
		String Cvalidate = tLICardActiveInfoListSchema.getCValidate();//生效日期
		String Inactivedate = tLICardActiveInfoListSchema.getInActiveDate();//失效日期
		//明亚接口字典规定 性别1为男 2为女
		String CurrentDate = PubFun.getCurrentDate();
		CreatePDF tCreatePDF = new CreatePDF(cardNo,Cvalidate,Inactivedate,dutyContent,appntListSchema.getName(),appntListSchema.getSex(),insuListSchema.getName(),IDTypeName,insuListSchema.getIDNo(),CurrentDate);
		tCreatePDF.creatPdf();
		MailSender tMailSender = new MailSender(cardNo,Cvalidate,Inactivedate,dutyContent,appntListSchema.getName(),appntListSchema.getSex(),insuListSchema.getName(),IDTypeName,insuListSchema.getIDNo(),CurrentDate);
		tMailSender.send(appntListSchema.getEmail());
		//向明亚指定邮箱发送邮件
		tMailSender.send("1j1@mingya.com.cn");
    	return true;
    }
    /**
     * 获取证件类型名称（与明亚接口字典设置不一致），暂时不用。
     * @param cLCContSchema
     * @return
     */
//    private boolean getIDTypeName(){
//    	String getIDTypeNameSQL = "select code,codename from ldcode where codetype = 'idtype' ";
//    	SSRS tSSRS = new ExeSQL().execSQL(getIDTypeNameSQL);
//    	if(tSSRS == null || tSSRS.getMaxRow()<=0){
//    		responseOME = buildCardActiveResponseOME("01","101", "保单已承保，但发送电子保单邮件失败，请与客服联系，补发电子保单!");
//			System.out.println("responseOME:" + responseOME+" 原因：获取证件类型对应证件名称失败！");
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"101", "86", "保单已承保，但发送电子保单邮件失败!");
//            return false;
//    	}
//    	for(int i=1;i<=tSSRS.getMaxRow();i++){
//    		if(insuListSchema.getIDType().equals(tSSRS.GetText(i, 1))){
//    			IDTypeName = tSSRS.GetText(i, 2);
//    		}
//    	}
//    	return true;
//    }
    /**
     * 获取证件类型名称，根据明亚接口字典设置。
     * @param cLCContSchema
     * @return
     */
    private boolean getIDTypeName(){
    	if("1".equals(insuListSchema.getIDType())){
    		IDTypeName = "身份证";
    	}else if("2".equals(insuListSchema.getIDType())){
    		IDTypeName = "护照";
    	}else if("3".equals(insuListSchema.getIDType())){
    		IDTypeName = "其他";
    	}
    	return true;
    }
    /**
     * 统一化时间格式，避免时间格式不一致导致的校验失败。
     * @param date
     * @return
     * 如传入格式为20110201 转换为201121
     */
    private String getDate(String aDate,String flag){
    	if("0".equals(flag)){
    		if("0".equals(aDate.substring(4, 5)) && "0".equals(aDate.substring(6, 7))){
        		aDate = aDate.substring(0, 4)+aDate.substring(5,6)+aDate.substring(7);
    		}else if("0".equals(aDate.substring(4, 5))){
    			aDate = aDate.substring(0, 4)+aDate.substring(5);
    		}else if("0".equals(aDate.substring(6, 7))){
    			aDate = aDate.substring(0, 6)+aDate.substring(7);
    		}
    	}else{
    		if("0".equals(aDate.substring(2, 3)) &&  "0".equals(aDate.substring(4, 5))){
        		aDate = aDate.substring(0, 2)+aDate.substring(3,4)+aDate.substring(5);
    		}else if("0".equals(aDate.substring(2, 3))){
    			aDate = aDate.substring(0, 2)+aDate.substring(3);
    		}else if("0".equals(aDate.substring(4, 5))){
    			aDate = aDate.substring(0, 4)+aDate.substring(5);
    		}
    	}
    	return aDate;
    }
}
