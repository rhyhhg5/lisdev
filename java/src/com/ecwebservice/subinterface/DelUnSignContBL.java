package com.ecwebservice.subinterface;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.*;
import org.apache.log4j.Logger;

/**
 * <p>Title:复杂网销未承保新单删除</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class DelUnSignContBL {
	
	protected Logger mLogger = Logger.getLogger(getClass().getName());
    private GlobalInput mG = new GlobalInput();
    private ExeSQL mExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    public GlobalInput mGlobalInput = new GlobalInput();//公共信息

    private boolean getInputData(VData cInputData) 
    {
    	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) 
        {
        	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        	if(mG.Operator==null || mG.Operator.equals("") || mG.Operator.equals("null"))
        	{
        		mG.Operator="001";
        	}
        	if(mG.ManageCom==null || mG.ManageCom.equals("") || mG.ManageCom.equals("null"))
        	{
        		mG.ManageCom="86";
        	}
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("WXDEL")) {
        	doDeleteWx();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean doDeleteWx() {
        System.out.println("复杂网销未签单垃圾数据删除批处理开始......。。。。");
        mGlobalInput.ManageCom = "86";
    	mGlobalInput.Operator = "EC_WX";
        Vector vec = new Vector();
        vec = getWxUnSignCont();

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {
        		String printNo = (String)vec.get(i);
    	        System.out.println("删除新单开始，Prtno为："+printNo);
    			VData tVData = new VData();
    			String tDeleteReason = "复杂产品订单删除需要进行新单删除";
    			TransferData tTransferData = new TransferData();
    			LCContDB tLCContDB = new LCContDB();
    			tLCContDB.setPrtNo(printNo);
    			LCContSchema tLCContSchema = new LCContSchema();
    			LCContSet tLCContSet = tLCContDB.query();
    			tLCContSchema = tLCContSet.get(1);
    		    tTransferData.setNameAndValue("DeleteReason",tDeleteReason);
    			tVData.add(tLCContSchema);
    			tVData.add(tTransferData);
    			tVData.add(mGlobalInput);
    			ContDeleteUI tContDeleteUI = new ContDeleteUI();
    			boolean flag = tContDeleteUI.submitData(tVData,"DELETE");
            	System.out.println("删除新单结束");
                if (!flag) {
        			String error = tContDeleteUI.mErrors.getFirstError();
        			mLogger.info("删除失败-" + printNo + "--" + error);
                    // @@错误处理
        			System.out.print("复杂网销-删除一条垃圾数据失败！");
                  
                } else {
                	System.out.print("复杂网销-成功删除一条垃圾数据！");
                	mLogger.info("删除成功-" + printNo);
                }
            }
        } 
        else 
        {
            System.out.print("复杂网销-没有需要删除的垃圾数据！");
        }
        System.out.println("复杂网销删除垃圾数据批处理正常结束......");
        return true;
    }

    private Vector getWxUnSignCont() {
    	System.out.println("getWxUnSignCont()----------------");
        Vector tVector = new Vector();
        //查询出电子商务端生成的垃圾数据
        String tWxSql =
        	"select a.prtno from lccont a where a.cardflag='b' and a.appflag <> '1' " +
        	"and not exists (select 1 from ljtempfee where otherno = a.contno) " +
        	"and not exists (select 1 from ljtempfee where otherno = a.prtno) " +
        	"and (days(current date)-days(a.makedate)) >=30 " ; //保单号
        System.out.println("电子商务垃圾数据删除---------------"+tWxSql);
        SSRS tMsgSuccSSRS =mExeSQL.execSQL(tWxSql);
        StringBuffer sbuf = new StringBuffer();
        for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++) 
        {
        	String tprtno = tMsgSuccSSRS.GetText(i, 1);
        	sbuf.append(tprtno + "--");
            tVector.add(tprtno);
        }
        System.out.println("网销删除的新单----" + sbuf.toString());
        return tVector;
    }
    
    
    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "lcy";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        
        DelUnSignContBL tSDwMsgBL = new DelUnSignContBL();
        tSDwMsgBL.submitData(mVData, "WXDEL");
    }

}
