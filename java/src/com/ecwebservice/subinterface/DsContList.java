package com.ecwebservice.subinterface;

import java.util.Iterator;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class DsContList {
	private GlobalInput mGlobalInput = new GlobalInput();

	private OMElement mOME = null;

	private String batchNo = null;     // 批次号

	private String sendDate = null;    // 报文发送日期

	private String sendTime = null;    // 报文发送时间

	private String sendOperator = "";  // 报送人员

	private String msgType = null;     //业务类型
	
	private String state = null;       // 处理状态：00－成功；01－失败

	private String name = ""; 	   	   //客户姓名

	private String idType = "";        //证件类型
	
	private String idNo = "";        //证件号码
	
	private String identity = "";        //客户类别标识符
	
	private String contType = "";        //保单类型标识符
	
	private MMap mCont = new MMap(); // key--保单号 ,value--客户类型(0-投保人;1-被保人)

	private OMElement responseOME = null;

	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if (!load()) {
			return responseOME;
		}

		if (!check()) {
			return responseOME;
		}

		if (!dealData()) {
			responseOME = buildResponseOME("01", "03", "没有查询到客户的保单信息");
			return responseOME;
		}
		responseOME = buildResponseOME("00", "", "");
		return responseOME;
	}

	/**
	 * 加载报文
	 * 
	 * @return
	 */
	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
					OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("DataSet")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();
						if (child_element.getLocalName().equals("MsgHead")) {
							Iterator children = child_element.getChildren();
							while (children.hasNext()) {
								OMElement child_elements = (OMElement) children.next();
								Iterator childrens = child_elements.getChildren();
									while (childrens.hasNext()) {
										OMElement children_elements = (OMElement) childrens.next();
										
										if(children_elements.getLocalName().equals("BatchNo")){
											this.batchNo = children_elements.getText();
											continue;
											}
										if(children_elements.getLocalName().equals("SendDate")){
											this.sendDate = children_elements.getText();
											continue;
											}
										if(children_elements.getLocalName().equals("SendTime")){
											this.sendTime = children_elements.getText();
											continue;
											}
										if(children_elements.getLocalName().equals("SendOperator")){
											this.sendOperator = children_elements.getText();
											continue;
											}
										if(child_elements.getLocalName().equals("MsgType")){
											this.msgType = children_elements.getText();
											continue;
									}
								}
									}
							}
						if (child_element.getLocalName().equals("CONTLIST")){
							Iterator child_cl = child_element.getChildren();
							while (child_cl.hasNext()) {
								OMElement children_cl = (OMElement) child_cl.next();
								
								if(children_cl.getLocalName().equals("Name")){
									this.name = children_cl.getText();
									continue;
									}
								if(children_cl.getLocalName().equals("IDType")){
									this.idType = children_cl.getText();
									continue;
									}
								if(children_cl.getLocalName().equals("IDNo")){
									this.idNo = children_cl.getText();
									continue;
									}
								if(children_cl.getLocalName().equals("Identity")){
									this.identity = children_cl.getText();
									continue;
									}
								if(children_cl.getLocalName().equals("ContType")){
									this.contType = children_cl.getText();
									continue;
									}
								}
							}
						}
					} else {
						System.out.println("报文类型不匹配");
						responseOME = buildResponseOME("01", "01", "报文类型不匹配");
						return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME = buildResponseOME("01", "01", "加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean check() {
		if(this.name == null || this.name == ""){
			responseOME = buildResponseOME("01", "04", "客户的姓名为空！");
			return false;
		}
		if(this.idType == null || this.idType == ""){
			responseOME = buildResponseOME("01", "04", "客户的证件类型为空！");
			return false;
		}
		if(this.idNo == null || this.idNo == ""){
			responseOME = buildResponseOME("01", "04", "客户的证件号码为空！");
			return false;
		}
		
		return true;
	}


	private boolean dealData() {
		String[] contInfo = null;

		String sqlSA = "(select lc.contno," // 客户作为投保人的保单
				+ " lc.appntname,lc.insuredname,lc.cvalidate,(select codename from ldcode where codetype='stateflag' and code=lc.stateflag),lc.stateflag"
				+ " from lccont lc,lcappnt app where lc.contno=app.contno and lc.appflag='1' and lc.ContType='1'"
				+ " and app.appntname='"+ this.name + "'  "
				+ " and app.IDType='"+this.idType + "'  "
				+ " and app.IDNo='"+this.idNo + "'  "
				+ " order by lc.stateflag)";
		
		String sqlSI = "(select lc.contno," // 客户作为被保人的保单
				+ " lc.appntname,lc.insuredname,lc.cvalidate,(select codename from ldcode where codetype='stateflag' and code=lc.stateflag),lc.stateflag"
				+ " from lccont lc,lcinsured ins where lc.contno=ins.contno and lc.appflag='1' and lc.ContType='1' "
				+ " and ins.name='"+ this.name + "'  "
				+ " and ins.IDType='"+this.idType + "'  "
				+ " and ins.IDNo='"+this.idNo + "'  "
				+ " order by lc.stateflag)";
		
		String SQL = "";
		if(this.contType.equals("S")){	
			if(this.identity.equals("A")){
				SQL = sqlSA + " with ur";
			}
			if(this.identity.equals("I")){
				SQL = sqlSI + " with ur";
			}
			if(this.identity.equals("")){
				SQL = sqlSA + " union " + sqlSI + " with ur";
			}
		}
		
		if(this.contType.equals("")){
			SQL = sqlSA + " union " + sqlSI + " union "  + " with ur";
		}

		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(SQL);
		if (null != tSSRS && tSSRS.MaxRow > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				contInfo = new String[6];
				contInfo[0] = tSSRS.GetText(i, 1);
				contInfo[1] = tSSRS.GetText(i, 2);
				contInfo[2] = tSSRS.GetText(i, 3);
				contInfo[3] = tSSRS.GetText(i, 4);
				contInfo[4] = tSSRS.GetText(i, 5);
				this.mCont.put(contInfo[0], contInfo);
			}
		}

		if (this.mCont.size() == 0) {
			// 保证返回报文的完整,在没有查到客户信息时置为空
			mCont.put("", new String[] { "", "", "", "", "" });
			return false;
		}

		return true;
	}
	
	
	
	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement dataSet = factory.createOMElement("DataSet", null);
		OMElement msgResHead = factory.createOMElement("MsgResHead", null);
		OMElement item = factory.createOMElement("Item", null);
		
		dataSet.addChild(msgResHead);
		msgResHead.addChild(item);
		addOm(item, "BatchNo", this.batchNo);
		addOm(item, "SendDate", PubFun.getCurrentDate());
		addOm(item, "SendTime", PubFun.getCurrentTime());
		addOm(item, "SendOperator", this.sendOperator);
		addOm(item, "MsgType", msgType);
		addOm(item, "State", state);
		addOm(item, "ErrCode", errorCode);
		addOm(item, "ErrInfo", errorInfo);
		
		OMElement contTable = factory.createOMElement("CONTTABLE", null);
		Set set = mCont.keySet();
		Object key = null;
		for (int j = 0; j < set.size(); j++) {
			OMElement item1 = factory.createOMElement("Item", null);
			key = mCont.getOrder().get(String.valueOf(j + 1));
			String[] temp = (String[]) mCont.get(key);
			addOm(item1, "ContNo", temp[0]);
			addOm(item1, "AppntName", temp[1]);
			addOm(item1, "InsuredName", temp[2]);
			addOm(item1, "CValidate", temp[3]);
			addOm(item1, "StateFlag", temp[4]);
			contTable.addChild(item1);
		}
		dataSet.addChild(contTable);
		return dataSet;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
}
