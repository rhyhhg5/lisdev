package com.ecwebservice.subinterface;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.rpc.client.RPCServiceClient;

import com.cbsws.obj.RspSaleInfo;
import com.cbsws.obj.RspUniSalescod;
import com.cbsws.obj.UniSalesAppayInfo;
import com.ecwebservice.ctrip.form.PraseXmlUtil;
import com.sinosoft.lis.pubfun.PubFun;

public class DealUniSalescodBL {
	
	private RspUniSalescod mRspUniSalescod = new RspUniSalescod();
	
	public RspUniSalescod uniSalesCodInfo(UniSalesAppayInfo tUniSalesAppayInfo){
		//根据同意工号调用webservice查询业务员信息
	      try
	        {

	           String tStrTargetEendPoint = "http://10.132.27.102:8080/Unity/ReqUniSalescod.jws";
	            String tStrNamespace = "http://10.132.27.102:8080/Unity/ReqUniSalescod.jws";

	            RPCServiceClient client = new RPCServiceClient();
	            String mInXmlStr = createPayLoad(tUniSalesAppayInfo);
//	            String mInXmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><REQUNISALESCOD><HEAD><APPID>H00001</APPID><SID>2320001201409010000063</SID><TIMESTAMP>20140901 15:17:50</TIMESTAMP><MESSAGETYPE>01</MESSAGETYPE></HEAD><BODY><DATA>       <UNI_SALES_COD></UNI_SALES_COD> <COMP_COD>000085</COMP_COD> <COMP_NAM>中国人民健康保险股份有限公司</COMP_NAM> <SALES_NAM>田七</SALES_NAM>     <DATE_BIRTHD>19700728</DATE_BIRTHD>   <SEX_COD>1</SEX_COD>     <MAN_ORG_COD>86320500</MAN_ORG_COD><MAN_ORG_NAM>苏州中心支公司</MAN_ORG_NAM> <IDTYP_COD>11</IDTYP_COD>     <IDTYP_NAM>居民身份证</IDTYP_NAM>  <ID_NO>612524197007285473</ID_NO>      <SALES_TEL>5454545455</SALES_TEL>  <SALES_MOB>11111111111</SALES_MOB>   <SALES_MAIL>N</SALES_MAIL>  <SALES_ADDR></SALES_ADDR>   <EDUCATION_COD>99</EDUCATION_COD><EDUCATION_NAM>文盲</EDUCATION_NAM><QUALIFI_NO>20050711010090000842</QUALIFI_NO><CERTIFI_NO>000000000000</CERTIFI_NO>	<CONTRACT_NO>N</CONTRACT_NO><STATUS_COD>1</STATUS_COD>  <STATUS>在职</STATUS>    <SALES_TYP_COD>1</SALES_TYP_COD><SALES_TYP>营销人员</SALES_TYP><IS_CROSSSALE>1</IS_CROSSSALE><CHANNEL_COD>101</CHANNEL_COD><SALECHA_NAM>个人营销直销</SALECHA_NAM><ENTRANT_DATE>20140901</ENTRANT_DATE>  <DIMISSION_DATE>N</DIMISSION_DATE><DEMISSION_REASON>N</DEMISSION_REASON><DATE_SEND>20140901 15:17:50</DATE_SEND></DATA></BODY></REQUNISALESCOD>";
	            System.out.println(mInXmlStr);
	            EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
	            Options option = client.getOptions();
	            option.setTo(erf);
	            option.setTimeOutInMilliSeconds(100000L);
	            option.setAction("getCompanyXml");
	            QName name = new QName(tStrNamespace, "getCompanyXml");
	            Object[] object = new Object[] { mInXmlStr };
	            Class[] returnTypes = new Class[] { String.class };

	            Object[] response = client.invokeBlocking(name, object, returnTypes);
	            String p = (String) response[0];

	            System.out.println("UI return1:" + p);
	            mRspUniSalescod = (RspUniSalescod)PraseXmlUtil.praseXml("//RSPUNISALESCOD", p, new RspUniSalescod());
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
		return mRspUniSalescod;
	}
	
	
	
	   private String createPayLoad(UniSalesAppayInfo tUniSalesAppayInfo){    
	        String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
	        				"<REQUNISALESCOD>"+
	        				"<HEAD>"+
	        				"<APPID>H00001</APPID>"+
	        				"<SID>"+tUniSalesAppayInfo.getSID()+"</SID>"+
	        				"<TIMESTAMP>"+tUniSalesAppayInfo.getTIMESTAMP()+"</TIMESTAMP>"+
	        				"<MESSAGETYPE>"+tUniSalesAppayInfo.getMESSAGETYPE()+"</MESSAGETYPE>"+
	        				"</HEAD>"+
	        				"<BODY>"+
	        				"<DATA>       "+
	        				"<UNI_SALES_COD>"+tUniSalesAppayInfo.getUni_sales_cod()+"</UNI_SALES_COD> "+
	        				"<COMP_COD>"+tUniSalesAppayInfo.getComp_cod()+"</COMP_COD> "+        
	        				"<COMP_NAM>"+tUniSalesAppayInfo.getComp_nam()+"</COMP_NAM> "+        
	        				"<SALES_NAM>"+tUniSalesAppayInfo.getSales_nam()+"</SALES_NAM>     "+        
	        				"<DATE_BIRTHD>"+tUniSalesAppayInfo.getDate_birthd()+"</DATE_BIRTHD>   "+  
	        				"<SEX_COD>"+tUniSalesAppayInfo.getSex_cod()+"</SEX_COD>     "+        
	        				"<MAN_ORG_COD>"+tUniSalesAppayInfo.getMan_org_cod()+"</MAN_ORG_COD>"+ 
	        				"<MAN_ORG_NAM>"+tUniSalesAppayInfo.getMan_org_nam()+"</MAN_ORG_NAM> "+
	        				"<IDTYP_COD>"+tUniSalesAppayInfo.getIdtyp_cod()+"</IDTYP_COD>     "+
	        				"<IDTYP_NAM>"+tUniSalesAppayInfo.getIdtyp_nam()+"</IDTYP_NAM>  "+   
	        				"<ID_NO>"+tUniSalesAppayInfo.getId_no()+"</ID_NO>      "+           
	        				"<SALES_TEL>"+tUniSalesAppayInfo.getSales_tel()+"</SALES_TEL>  "+       
	        				"<SALES_MOB>"+tUniSalesAppayInfo.getSales_mob()+"</SALES_MOB>   "+      
	        				"<SALES_MAIL>"+tUniSalesAppayInfo.getSales_mail()+"</SALES_MAIL>  "+     
	        				"<SALES_ADDR>"+tUniSalesAppayInfo.getSales_addr()+"</SALES_ADDR>   "+    
	        				"<EDUCATION_COD>"+tUniSalesAppayInfo.getEducation_cod()+"</EDUCATION_COD>"+
	        				"<EDUCATION_NAM>"+tUniSalesAppayInfo.getEducation_nam()+"</EDUCATION_NAM>"+
	        				"<QUALIFI_NO>"+tUniSalesAppayInfo.getQualifi_no()+"</QUALIFI_NO>"+
	        				"<CERTIFI_NO>"+tUniSalesAppayInfo.getCertifi_no()+"</CERTIFI_NO>	"+
	        				"<CONTRACT_NO>"+tUniSalesAppayInfo.getContract_no()+"</CONTRACT_NO>"+
	        				"<STATUS_COD>"+tUniSalesAppayInfo.getStatus_cod()+"</STATUS_COD>  "+ 
	        				"<STATUS>"+tUniSalesAppayInfo.getStatus()+"</STATUS>    "+       
	        				"<SALES_TYP_COD>"+tUniSalesAppayInfo.getSales_typ_cod()+"</SALES_TYP_COD>"+
	        				"<SALES_TYP>"+tUniSalesAppayInfo.getSales_typ()+"</SALES_TYP>"+
	        				"<IS_CROSSSALE>"+tUniSalesAppayInfo.getIs_crosssale()+"</IS_CROSSSALE>"+
	        				"<CHANNEL_COD>"+tUniSalesAppayInfo.getChannel_cod()+"</CHANNEL_COD>"+
	        				"<SALECHA_NAM>"+tUniSalesAppayInfo.getSalecha_nam()+"</SALECHA_NAM>"+
	        				"<ENTRANT_DATE>"+tUniSalesAppayInfo.getEntrant_date()+"</ENTRANT_DATE>  "+
	        				"<DIMISSION_DATE>"+tUniSalesAppayInfo.getDimission_date()+"</DIMISSION_DATE>"+
	        				"<DEMISSION_REASON>"+tUniSalesAppayInfo.getDemission_reason()+"</DEMISSION_REASON>"+
	        				"<DATE_SEND>"+tUniSalesAppayInfo.getDate_send()+"</DATE_SEND>"+
	        				"</DATA>"+
	        				"</BODY>"+
	        				"</REQUNISALESCOD>";
	        
	        return xmlStr;   
	    }  
	   
	   public static void main(String[] args) {
		   DealUniSalescodBL tDealUniSalescodBL = new DealUniSalescodBL();
		   UniSalesAppayInfo tUniSalesAppayInfo =new UniSalesAppayInfo ();
		   RspUniSalescod mRspUniSalescod= tDealUniSalescodBL.uniSalesCodInfo(tUniSalesAppayInfo);
		   String a =mRspUniSalescod.getMESSAGETYPE();
		   if("01".equals(a)){
			   
			   System.out.print(a);
		   }
	}

}
