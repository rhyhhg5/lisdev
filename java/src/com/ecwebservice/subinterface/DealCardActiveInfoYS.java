package com.ecwebservice.subinterface;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.utils.AgentCodeTransformation;
import com.mail.CreatePDF;
import com.mail.MailSender;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.IDValidator;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.vschema.WFAppntListSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFContListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealCardActiveInfoYS {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	
	private String branchcode = "";// 网点
	
	private String sendoperator = "";// 操作员

	private String messageType = "";// 报文类型

	private String cardDealType = null;// 处理方式
	
	private String dutyContent = "";// 保险责任
	
	private String IDTypeName = "";//证件类型名称
	
	ExeSQL tExeSQL = new ExeSQL();
	private ArrayList mUwErrorList = new ArrayList();
	double tSysWrapSumPrem = 0; //系统计算出的保费
    double tSysWrapSumAmnt = 0;//系统计算出的保额
	
	private String bak1 =null;
	private String bak2 =null;
	private String bak3 =null;
	private String bak4 =null;
	private String bak5 =null;
	private String bak6 =null;
	private String bak7 =null;
	private String bak8 =null;
	private String bak9 =null;
	private String bak10 =null;
	
	private OMElement responseOME = null;
	
	private String cardNo = null;
	
	private MMap tMMap = new MMap();
	
	private String cardpassword = null;
	private LoginVerifyTool mTool = new LoginVerifyTool();
	// 保险卡信息
	private WFContListSchema contListSchema = new WFContListSchema();
	private WFContListSet contListSet = new WFContListSet();

	// 被保人信息
	private WFInsuListSet insuListSet = new WFInsuListSet();
//	private WFInsuListSchema insuListSchema = null;
	private WFInsuListSchema insuListSchema = new WFInsuListSchema();
	
	//投保人信息
	private WFAppntListSchema appntListSchema = new WFAppntListSchema();
	private WFAppntListSet appntListSet = new WFAppntListSet();
	
	//受益人信息
//	private WFBnfListSchema bnfListSchema = null;
//	private WFBnfListSet bnfListSet = new WFBnfListSet();
	
	int ContNum = 0;//报文中保险卡节点数量
	int AppntNum = 0;//报文中投保人数量
	int InsuNum = 0;//报文中被保人数量
	boolean BnfInfo = true;//判断是否由受益人信息 ps：根据受益人必须是被保人本人，所以受益人节点为空节点
	
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildCardActiveResponseOME("01","01", "加载激活信息失败");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			return responseOME;
		}
		if(!deal()){
			return responseOME;
		}
		if(!save()){
			return responseOME;
		}
		return responseOME;
	}
	
	//加载卡激活信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("YSCERTACT")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDDATE")){
							this.sendDate=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDTIME")){
							this.sendTime= child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("BRANCHCODE")){
							this.branchcode=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDOPERATOR")){
							this.sendoperator=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("MESSAGETYPE")){
							this.messageType=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("CONTLIST")){
							Iterator childItem = child_element.getChildren();
							while(childItem.hasNext()){
								OMElement childOME = (OMElement) childItem.next();
								if(childOME.getLocalName().equals("ITEM")){
									ContNum++;
									Iterator child1OME = childOME.getChildren();
									while(child1OME.hasNext()){
										OMElement childleaf = (OMElement) child1OME.next();
										if(childleaf.getLocalName().equals("WRAPCODE")){
											this.contListSchema.setRiskCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CERTIFYCODE")){
											this.contListSchema.setCertifyCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CERTIFYNAME")){
											this.contListSchema.setCertifyName(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("ACTIVEDATE")){
											this.contListSchema.setActiveDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("MANAGECOM")){
											this.contListSchema.setManageCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("SALECHNL")){
											this.contListSchema.setSaleChnl(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AGENTCOM")){
											this.contListSchema.setAgentCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AGENTCODE")){
											AgentCodeTransformation a = new AgentCodeTransformation();
											if(!a.AgentCode(childleaf.getText(), "N")){
												System.out.println(a.getMessage());
											}
											this.contListSchema.setAgentCode(a.getResult());
											continue;
										}
										if(childleaf.getLocalName().equals("AGENTNAME")){
											this.contListSchema.setAgentCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PAYMODE")){
											this.contListSchema.setPayMode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PAYINTV")){
											this.contListSchema.setPayIntv(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PAYYEAR")){
											this.contListSchema.setPayYear(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PAYYEARFLAG")){
											this.contListSchema.setPayYearFlag(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CVALIDATE")){
											this.contListSchema.setCvaliDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CVALIDATETIME")){
											this.contListSchema.setCvaliDateTime(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("INSUYEAR")){
											this.contListSchema.setInsuYear(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("INSUYEARFLAG")){
											this.contListSchema.setInsuYearFlag(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PREM")){
											if(childleaf.getText().equals("")||childleaf.getText()==null){
												System.out.println("没有得到保费");
												//return false;
											}else{
												this.contListSchema.setPrem(new Double(childleaf.getText()).doubleValue());
												continue;
											}
											
										}
										if(childleaf.getLocalName().equals("AMNT")){
											if(childleaf.getText().equals("")||childleaf.getText()==null){
												System.out.println("没有得到保额");
												//return false;
											}else{
												this.contListSchema.setAmnt(Integer.parseInt(childleaf.getText()));
												continue;
											}
					
										}
										if(childleaf.getLocalName().equals("MULT")){
											if(childleaf.getText().equals("")||childleaf.getText()==null){
												System.out.println("没有得到档次");
												//return false;
											}else{
												this.contListSchema.setMult(Integer.parseInt(childleaf.getText()));
												continue;
											}
											
										}
										if(childleaf.getLocalName().equals("COPYS")){
											if(childleaf.getText().equals("")||childleaf.getText()==null){
												System.out.println("没有得到份数");
												//return false;
											}else{
												this.contListSchema.setCopys(Integer.parseInt(childleaf.getText()));
												continue;
											}
											
										}
										if(childleaf.getLocalName().equals("REMARK")){
											this.contListSchema.setRemark(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("BAK1")){
											this.bak1=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK2")){
											this.bak2=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK3")){
											this.bak3=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK4")){
											this.bak4=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK5")){
											this.bak5=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK6")){
											this.bak6=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK7")){
											this.bak7=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK8")){
											this.bak8=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK9")){
											this.bak9=childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("BAK10")){
											this.bak10=childleaf.getText();
											continue;
										}
									}
								this.contListSet.add(this.contListSchema);
								}
							}
						}
						
						if(child_element.getLocalName().equals("APPNTLIST")){
							Iterator child_appnt = child_element.getChildren();
							while(child_appnt.hasNext()){
								OMElement appnt_item = (OMElement) child_appnt.next();
								if(appnt_item.getLocalName().equals("ITEM")){
									AppntNum++;
									Iterator appnt_element= appnt_item.getChildren();
									while(appnt_element.hasNext()){
										OMElement appntleaf = (OMElement)appnt_element.next();
										
										if(appntleaf.getLocalName().equals("NAME")){
											this.appntListSchema.setName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("SEX")){
											this.appntListSchema.setSex(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BIRTHDAY")){
											this.appntListSchema.setBirthday(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDTYPE")){
											this.appntListSchema.setIDType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDNO")){
											this.appntListSchema.setIDNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OCCUPATIONTYPE")){
											this.appntListSchema.setOccupationType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OCCUPATIONCODE")){
											this.appntListSchema.setOccupationCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("POSTALADDRESS")){
											this.appntListSchema.setPostalAddress(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ZIPCODE")){
											this.appntListSchema.setZipCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("PHONT")){
											this.appntListSchema.setPhont(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("MOBILE")){
											this.appntListSchema.setMobile(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("EMAIL")){
											this.appntListSchema.setEmail(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GRPNAME")){
											this.appntListSchema.setGrpName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("COMPANYPHONE")){
											this.appntListSchema.setCompanyPhone(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("COMPADDR")){
											this.appntListSchema.setCompAddr(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("COMPZIPCODE")){
											this.appntListSchema.setCompZipCode(appntleaf.getText());
											continue;
										}
									}
									this.appntListSet.add(this.appntListSchema);
								}
							}
						}
						if(child_element.getLocalName().equals("INSULIST")){
							Iterator child_insu = child_element.getChildren();
							while(child_insu.hasNext()){
								OMElement insu_item = (OMElement) child_insu.next();
								if(insu_item.getLocalName().equals("ITEM")){
									InsuNum++;
									Iterator insu_element = insu_item.getChildren();
									while(insu_element.hasNext()){
										OMElement insuleaf = (OMElement)insu_element.next();
										if(insuleaf.getLocalName().equals("INSUNO")){
											this.insuListSchema.setInsuNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("TOAPPNTRELA")){
											this.insuListSchema.setToAppntRela(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("NAME")){
											this.insuListSchema.setName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("SEX")){
											this.insuListSchema.setSex(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("BIRTHDAY")){
											this.insuListSchema.setBirthday(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDTYPE")){
											this.insuListSchema.setIDType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDNO")){
											this.insuListSchema.setIDNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("OCCUPATIONTYPE")){
											this.insuListSchema.setOccupationType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("OCCUPATIONCODE")){
											this.insuListSchema.setOccupationCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("POSTALADDRESS")){
											this.insuListSchema.setPostalAddress(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("ZIPCODE")){
											this.insuListSchema.setZipCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("PHONT")){
											this.insuListSchema.setPhont(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("MOBILE")){
											this.insuListSchema.setMobile(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("EMAIL")){
											this.insuListSchema.setEmail(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("GRPNAME")){
											this.insuListSchema.setGrpName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("COMPANYPHONE")){
											this.insuListSchema.setCompanyPhone(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("COMPADDR")){
											this.insuListSchema.setCompAddr(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("COMPZIPCODE")){
											this.insuListSchema.setCompZipCode(insuleaf.getText());
											continue;
										}
									}
									this.insuListSet.add(this.insuListSchema);
								}
							}
						}
						if(child_element.getLocalName().equals("BNFLIST")){
							Iterator child_bnf = child_element.getChildren();
							if(child_bnf.hasNext()) BnfInfo=false;//判断受益人节点是否为空
						}
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验卡激活信息
	 * @return
	 */
	private boolean check(){
		if(!checkInfo()){
			System.out.println("校验基本信息失败");
			return false;
		}
		if(!chkMingYa()){
			System.out.println("明亚校验信息失败");
			return false;
		}
		return true;
	}
	
	/**
	 * 处理卡激活信息
	 * @return
	 */ 
	private boolean deal(){
		try{
			WFContListSet tContListSet = new WFContListSet();
			WFAppntListSet tAppntList = new WFAppntListSet();
			WFInsuListSet tInsuList = new WFInsuListSet();
			WFBnfListSet tBnfList = new WFBnfListSet();
			if(this.contListSet.size()!=1){
				System.out.println("险种数目有误");
				responseOME = buildCardActiveResponseOME("01","81", "险种数目有误!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "81", "险种数目有误!");
				return false;
			}
			String insuYear = null;
			String insuYearFlag = null;
			String insuYearSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
				+ contListSchema.getRiskCode()
				+ "' and calfactor='InsuYear'   with ur";
			String insuYearFlagSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
				+  contListSchema.getRiskCode()
				+ "' and calfactor='InsuYearFlag'   with ur";
			insuYear = new ExeSQL().getOneValue(insuYearSql);
			insuYearFlag = new ExeSQL().getOneValue(insuYearFlagSql);
				this.contListSchema.setBatchNo(this.batchno);
				this.contListSchema.setSendDate(this.sendDate);
				this.contListSchema.setSendTime(this.sendTime);
				this.contListSchema.setBranchCode(this.branchcode);
				this.contListSchema.setSendOperator(this.sendoperator);
				this.contListSchema.setMessageType(this.messageType);//?????
				this.contListSchema.setAppntNo("1");
				this.contListSchema.setCardDealType("01");
				this.contListSchema.setInsuYear(Integer.parseInt(insuYear));
				this.contListSchema.setInsuYearFlag(insuYearFlag);
				tContListSet.add(this.contListSchema);
				tMMap.put(tContListSet, SysConst.INSERT);
				
				for(int i=1;i<=this.appntListSet.size();i++){
					WFAppntListSchema tWFAppntListSchema = new WFAppntListSchema();
					tWFAppntListSchema = appntListSet.get(i);
					tWFAppntListSchema.setCardNo(this.cardNo);
					tWFAppntListSchema.setBatchNo(this.batchno);
					tWFAppntListSchema.setAppntNo("1");
					tAppntList.add(tWFAppntListSchema);
				}
				tMMap.put(tAppntList, SysConst.INSERT);
				for(int i=1;i<=this.insuListSet.size();i++){
					WFInsuListSchema tWFInsuListSchema = new WFInsuListSchema();
					tWFInsuListSchema = insuListSet.get(i);
					tWFInsuListSchema.setCardNo(this.cardNo);
					tWFInsuListSchema.setBatchNo(this.batchno);
					tInsuList.add(tWFInsuListSchema);
				}
				tMMap.put(tInsuList, SysConst.INSERT);
				//卡单外部激活信息
				ExternalActive tExternalActive = new ExternalActive();
				LICardActiveInfoListSet exCardActiveInfoListSet = tExternalActive.writeLICardActiveInfoList(contListSchema, insuListSet);
				
				//state In Lzcard 需要修改为14.
				String[] updateArr = tExternalActive.getUpdateLzcardSql(cardNo);
				String stateInLzcard = updateArr[1];
				//判断单证号是否有效
				if("2".equals(stateInLzcard)||"10".equals(stateInLzcard)||"11".equals(stateInLzcard)||"13".equals(stateInLzcard)){
					System.out.println("单证号有效.");
				}else{
					responseOME = buildCardActiveResponseOME("01","21", "单证号无效--状态!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "21", "单证号无效!");
					return false;
				}
				if("11".equals(stateInLzcard)||"10".equals(stateInLzcard)){
					System.out.println("state In Lzcard 需要修改为14.");
					String modifyStateSql = updateArr[0];
					tMMap.put(modifyStateSql, SysConst.UPDATE);
				}
				tMMap.put(exCardActiveInfoListSet, SysConst.INSERT);
		
		}catch(Exception e){
			System.out.println("处理卡激活信息异常");
			e.printStackTrace();
			return false;
		}
									
		return true;
	}
	
	/**
	 *校验基本信息 
	 * @return
	 */
	private boolean checkInfo(){
		if(this.batchno ==null || this.batchno.equals("")){
			responseOME = buildCardActiveResponseOME("01","21", "没有得到批次号!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		boolean validBatchNo = mTool.isValidBatchNo(batchno);
		if (!validBatchNo) {
			// 批次号不唯一
			responseOME = buildCardActiveResponseOME("01","22", "批次号不唯一!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "22", "批次号不唯一!");
			return false;
		}
		if(this.sendDate==null || this.sendDate.equals("")){
			responseOME = buildCardActiveResponseOME("01","23", "没有得到发送日期!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "23", "没有得到发送日期!");
			return false;
		}
		if(this.sendTime==null || this.sendTime.equals("")){
			responseOME = buildCardActiveResponseOME("01","24", "没有得到发送时间!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "24", "没有得到发送时间!");
			return false;
		}
		if(this.branchcode==null || this.branchcode.equals("")){
			responseOME = buildCardActiveResponseOME("01","25", "没有得到网点!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "25", "没有得到网点!");
			return false;
		}
		if(this.sendoperator==null || this.sendoperator.equals("")){
			responseOME = buildCardActiveResponseOME("01","26", "没有得到发送者!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "26", "没有得到发送者!");
			return false;
		}
		if(this.contListSchema.getCertifyCode().equals("") || this.contListSchema.getCertifyCode()==null){
			responseOME = buildCardActiveResponseOME("01","27", "没有得到单证编码!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "27", "没有得到单证编码!");
			return false;	
		}
		try{
			String searchCardnoSQL = "select lzcn.cardno,lzcn.cardpassword from LZCardNumber lzcn "
				 +"inner join LZCard lzc on lzcn.CardType = lzc.SubCode "
				                      +"and lzc.StartNo = lzcn.CardSerNo "
				                      +"and lzc.EndNo = lzcn.CardSerNo "
				 +" where lzc.State in ('10', '11','2','13') "
				// +" and lzc.ReceiveCom = '"+contListSchema.getAgentCom()+"' "
				 +" and not exists (select 1 from licardactiveinfolist where cardno = lzcn.cardno) "
				   +"and lzc.certifycode = '"+this.contListSchema.getCertifyCode()+"' fetch first 1 rows only ";
			SSRS tSSRS = new SSRS();
			tSSRS = new ExeSQL().execSQL(searchCardnoSQL);
			if(tSSRS!=null){
				this.cardNo = tSSRS.GetText(1, 1);
				this.cardpassword = tSSRS.GetText(1, 2);
			}
			LisIDEA tLisIdea = new LisIDEA();
			cardpassword=tLisIdea.decryptString(cardpassword);
			if(cardNo.equals("") || cardNo ==null ||cardpassword.equals("") || cardpassword ==null){
				responseOME = buildCardActiveResponseOME("01","28", "没有得到CardNo或cardpassword!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "28", "没有得到CardNo或cardpassword!");
				return false;
			}
		}catch(Exception e){
			System.out.println("获取卡号失败");
			e.printStackTrace();
			return false;
		}
		contListSchema.setCardNo(this.cardNo);
		contListSchema.setPassword(cardpassword);
		
		if(this.contListSchema.getRiskCode().equals("") || this.contListSchema.getRiskCode()==null){
			responseOME = buildCardActiveResponseOME("01","29", "没有得到套餐编码!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "29", "没有得到套餐编码!");
			return false;	
		}
		String riskcodeSQL = "select 1 from LMCardRisk lmcr " +
				"where lmcr.certifycode='"+this.contListSchema.getCertifyCode()+"' and lmcr.riskcode='"
					+this.contListSchema.getRiskCode()+"' and risktype='W'";
		String riskcheck = new ExeSQL().getOneValue(riskcodeSQL);
		if(!riskcheck.equals("1")){
			responseOME = buildCardActiveResponseOME("01","30", "单证编码与套餐编码不匹配!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "30", "单证编码与套餐编码不匹配!");
			return false;
		}
		if(this.contListSchema.getActiveDate().equals("") || this.contListSchema.getActiveDate()==null){
			responseOME = buildCardActiveResponseOME("01","31", "没有得到激活时间!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "31", "没有得到激活时间!");
			return false;
		}
		if(this.contListSchema.getCvaliDate().equals("") || this.contListSchema.getCvaliDate()==null){
			responseOME = buildCardActiveResponseOME("01","32", "没有得到生效时间!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "32", "没有得到生效时间!");
			return false;
		}
		if(this.appntListSchema.getName().equals("") || this.appntListSchema.getName()==null){
			responseOME = buildCardActiveResponseOME("01","34", "没有得到投保人姓名!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "34", "没有得到投保人姓名!");
			return false;
		}
		if(this.appntListSchema.getSex().equals("") || this.appntListSchema.getSex()==null){
			responseOME = buildCardActiveResponseOME("01","35", "没有得到投保人性别!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "35", "没有得到投保人性别!");
			return false;
		}
		if(this.appntListSchema.getBirthday().equals("") || this.appntListSchema.getBirthday()==null){
			responseOME = buildCardActiveResponseOME("01","36", "没有得到投保人出生日期!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "36", "没有得到投保人出生日期!");
			return false;
		}
		if(this.appntListSchema.getIDType().equals("") || this.appntListSchema.getIDType()==null){
			responseOME = buildCardActiveResponseOME("01","37", "没有得到投保人ID类型!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "37", "没有得到投保人ID类型!");
			return false;
		}
		if(this.appntListSchema.getIDNo().equals("") || this.appntListSchema.getIDNo()==null){
			responseOME = buildCardActiveResponseOME("01","38", "没有得到投保人ID号码!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "38", "没有得到投保人ID号码!");
			return false;
		}
		//被保人信息判断
		if(this.insuListSchema.getInsuNo().equals("") || this.insuListSchema.getInsuNo()==null){
			responseOME = buildCardActiveResponseOME("01","39", "没有得到被保人编号!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "39", "没有得到被保人编号!");
			return false;
		}
		if(this.insuListSchema.getName().equals("") || this.insuListSchema.getName()==null){
			responseOME = buildCardActiveResponseOME("01","40", "没有得到被保人姓名!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "40", "没有得到被保人姓名!");
			return false;
		}
		if(this.insuListSchema.getSex().equals("") || this.insuListSchema.getSex()==null){
			responseOME = buildCardActiveResponseOME("01","41", "没有得到被保人性别!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "41", "没有得到被保人性别!");
			return false;
		}
		if(this.insuListSchema.getBirthday().equals("") || this.insuListSchema.getBirthday()==null){
			responseOME = buildCardActiveResponseOME("01","42", "没有得到被保人出生日期!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "42", "没有得到被保人出生日期!");
			return false;
		}
		if(this.insuListSchema.getIDType().equals("") || this.insuListSchema.getIDType()==null){
			responseOME = buildCardActiveResponseOME("01","43", "没有得到被保人ID类型!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "43", "没有得到被保人ID类型!");
			return false;
		}
		if(this.insuListSchema.getIDNo().equals("") || this.insuListSchema.getIDNo()==null){
			responseOME = buildCardActiveResponseOME("01","44", "没有得到被保人ID号码!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "44", "没有得到被保人ID号码!");
			return false;
		}
		if(this.bak6==null || this.bak6.equals("")){
			responseOME = buildCardActiveResponseOME("01","45", "被保人国籍为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "45", "被保人国籍为空!");
			return false;
		}
		if(this.bak7==null || this.bak7.equals("")){
			responseOME = buildCardActiveResponseOME("01","100", "被保险人与投保人的关系为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "46", "被保险人与投保人的关系为空!");
			return false;
		}
		if(this.bak4==null || this.bak4.equals("")){
			responseOME = buildCardActiveResponseOME("01","101", "其余告知+同意协议标志集为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "47", "其余告知+同意协议标志集为空!");
			return false;
		}		
		return true;
	}
	
	//提交卡激活信息
	private boolean save(){
		if(this.messageType.equals("04")){
			responseOME = buildCardActiveResponseOME("00","0", "明亚校验类型，不做激活处理。");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "00",
					"100", "0", "明亚校验类型，不做激活处理。");
			return true;
		}
		try{
			String cheCardNoSQL="select 1 from WFContList where cardno='"+cardNo+"'";
			String cheCardNoRes = new ExeSQL().getOneValue(cheCardNoSQL);
			if(cheCardNoRes.equals("1")){
				responseOME = buildCardActiveResponseOME("01","80", "cardNo不唯一!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "80", "cardNo不唯一!");
				return false;
			}
			lockCont(cardNo);
			VData v = new VData();
			v.add(tMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				System.out.println("数据提交失败************************");
				responseOME = buildCardActiveResponseOME("01","6", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "6", "数据提交失败!");
				return false;
			}
			responseOME = buildCardActiveResponse("00",this.cardNo,this.cardpassword);
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "00",
					"100", "0", "数据提交成功!");
		}catch(Exception e){
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	private OMElement buildCardActiveResponseOME(String state, String errCode,String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("YSCERTACT", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "BRANCHCODE", this.branchcode);
		tool.addOm(DATASET, "SENDOPERATOR", this.sendoperator);
		tool.addOm(DATASET, "MESSAGETYPE", this.messageType);

		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}
	
	
	private OMElement buildCardActiveResponse(String state,String cardno, String password) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("YSCERTACT", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "BRANCHCODE", this.branchcode);
		tool.addOm(DATASET, "SENDOPERATOR", this.sendoperator);

		tool.addOm(DATASET, "CARDNO", cardno);
		tool.addOm(DATASET, "PASSWORD", password);
		
		tool.addOm(DATASET, "STATE", state);


		return DATASET;
	}

    /**
     * 明亚产品校验
     * @return
     */
    private boolean chkMingYa()
    {
    	//业务类型不可为空
    	if(this.messageType==null || this.messageType.equals("")){
			responseOME = buildCardActiveResponseOME("01","82", "没有得到业务类型!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "82", "没有得到业务类型!");
			return false;
		}
//      投保人联系电话或手机至少填写一项
		if((this.appntListSchema.getPhont()==null || this.appntListSchema.getPhont().equals(""))&& 
				(this.appntListSchema.getMobile()==null || this.appntListSchema.getMobile().equals(""))){
			responseOME = buildCardActiveResponseOME("01","50", "投保人的联系电话和手机至少填写一项!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "50", "投保人的联系电话和手机至少填写一项!");
			return false;
		}
		if(this.appntListSchema.getEmail()==null || this.appntListSchema.getEmail().equals("")){
		responseOME = buildCardActiveResponseOME("01","53", "没有得到投保人的电子邮箱!");
		System.out.println("responseOME:" + responseOME);
		LoginVerifyTool.writeLog(responseOME, batchno,
				messageType, sendDate, sendTime, "01",
				"100", "53", "没有得到投保人的电子邮箱!");
		return false;
	    }		
		
		//投保人身份证判断
		if(this.appntListSchema.getIDType().equals("1")){
			//判断是否为身份证类型 进行身份证号校验
			boolean flag =  IDValidator.chkIDCards(this.appntListSchema.getIDNo());
			System.out.println("是否符合正确的信息----"+flag);
			if(!flag){
				responseOME = buildCardActiveResponseOME("01","150", "身份证号码最后一位不能为小写x!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "200", "身份证号码最后一位不能为小写x!");
				return false;
           }
			String IDNo=this.appntListSchema.getIDNo();//获取投保人身份证号
			if(IDNo.length()==18){//18为身份证校验，校验出生年月日
				String birthday = this.appntListSchema.getBirthday().replaceAll("-", "");//获取出生日期 格式为20101010
				String idBirthday=IDNo.substring(6, 14);//获取身份证中的出生年月日
			    if(!idBirthday.equals(birthday)){
			    	responseOME = buildCardActiveResponseOME("01","54", "投保人身份证中包含的出生日期与获取的出生日期不符!");
			    	System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "54", "投保人身份证中包含的出生日期与获取的出生日期不符!");
					return false;
			    }else{
			    	int sexid = Integer.parseInt(IDNo.substring(16, 17));
			    	if((sexid%2)!=(Integer.parseInt(this.appntListSchema.getSex())%2)){
			    		responseOME = buildCardActiveResponseOME("01","55", "投保人身份证中包含的性别信息与获取的性别不符!");
				    	System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "55", "投保人身份证中包含的性别信息与获取的性别不符!");
						return false;
			    	}
			    }
			}else if(IDNo.length()==15){//15位身份证校验，校验月日
				String birthday = this.appntListSchema.getBirthday().replaceAll("-", "").substring(2, 8);
				String idBirthday = IDNo.substring(6, 12);
				if(!idBirthday.equals(birthday)){
					responseOME = buildCardActiveResponseOME("01","56", "投保人身份证中包含的出生日期与获取的出生日期不符!");
			    	System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "56", "投保人身份证中包含的出生日期与获取的出生日期不符!");
					return false;
				}else{
					int sexid = Integer.parseInt(IDNo.substring(14, 15));
			    	if((sexid%2)!=(Integer.parseInt(this.appntListSchema.getSex())%2)){
			    		responseOME = buildCardActiveResponseOME("01","57", "投保人身份证中包含的性别信息与获取的性别不符!");
				    	System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "57", "投保人身份证中包含的性别信息与获取的性别不符!");
						return false;
			    	}
				}
			}
			
		}
        // 投保人年龄≥18周岁，校验通过
        try
        {
            int appntAge = Integer.parseInt(bak5);
            if (appntAge < 18)
            {
                responseOME = buildCardActiveResponseOME("01","58", "投保人年龄不能小于18周岁!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "58", "投保人年龄不能小于18周岁!");
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("投保人年龄传入非数字类型数据。");
            return false;
        }
        // --------------------

        
//      被保人身份证判断
		if(this.insuListSchema.getIDType().equals("1")){
			//判断是否为身份证类型 进行身份证号校验
			boolean flag =  IDValidator.chkIDCards(this.appntListSchema.getIDNo());
			System.out.println("是否符合正确的信息----"+flag);
			if(!flag){
				responseOME = buildCardActiveResponseOME("01","150", "身份证号码最后一位不能为小写x!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "200", "身份证号码最后一位不能为小写x!");
				return false;
           }
			String IDNo=this.insuListSchema.getIDNo();//获取被保人身份证号
			if(IDNo.length()==18){//18位身份证校验，校验出生年月日
				String birthday = this.insuListSchema.getBirthday().replaceAll("-", "");//获取出生日期 格式为20101010
				String idBirthday=IDNo.substring(6, 14);//获取身份证中的出生年月日
			    if(!idBirthday.equals(birthday)){
			    	responseOME = buildCardActiveResponseOME("01","59", "被保人身份证中包含的出生日期与获取的出生日期不符!");
			    	System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "59", "被保人身份证中包含的出生日期与获取的出生日期不符!");
					return false;
			    }else{
			    	int sexid = Integer.parseInt(IDNo.substring(16, 17));
			    	if((sexid%2)!=(Integer.parseInt(this.insuListSchema.getSex())%2)){
			    		responseOME = buildCardActiveResponseOME("01","60", "被保人身份证中包含的性别信息与获取的性别不符!");
				    	System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "60", "被保人身份证中包含的性别信息与获取的性别不符!");
						return false;
			    	}
			    }
			}else if(IDNo.length()==15){//15位身份证校验，校验月日
				String birthday = this.insuListSchema.getBirthday().replaceAll("-", "").substring(2, 8);
				String idBirthday = IDNo.substring(6, 12);
				if(!idBirthday.equals(birthday)){
					responseOME = buildCardActiveResponseOME("01","61", "被保人身份证中包含的出生日期与获取的出生日期不符!");
			    	System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,
							messageType, sendDate, sendTime, "01",
							"100", "61", "被保人身份证中包含的出生日期与获取的出生日期不符!");
					return false;
				}else{
					int sexid = Integer.parseInt(IDNo.substring(14, 15));
			    	if((sexid%2)!=(Integer.parseInt(this.insuListSchema.getSex())%2)){
			    		responseOME = buildCardActiveResponseOME("01","62", "被保人身份证中包含的性别信息与获取的性别不符!");
				    	System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "62", "被保人身份证中包含的性别信息与获取的性别不符!");
						return false;
			    	}
				}
			}
			
		}
		if(contListSchema.getCopys() == 0 ){//当第三方向核心发送份数为0时，则为未传递份数，则认为购买1份
			contListSchema.setCopys(1);
		}
//		校验传送年龄 职业类别 所购买份数 是否符合产品描述
		if(!checkInsuredInfos(insuListSet)){
			return false;
		}
//		校验特殊规定
		if(!checkSpec(insuListSet)){
			return false;
		}
//		校验保费保额
		if(!dealCertifyWrapParams()){
			return false;
		}
     
        //20101103  by gzh 判断
        if(ContNum>1){//判断报文中卡信息节点是否唯一
        	responseOME = buildCardActiveResponseOME("01","83", "报文中卡信息不唯一!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "83", "报文中卡信息不唯一!");
            return false;
        }
        if(AppntNum>1){//判断报文中投保人是否唯一
        	responseOME = buildCardActiveResponseOME("01","84", "报文中投保人不唯一!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "83", "报文中投保人不唯一!");
            return false;
        }
        if(InsuNum>1){//判断报文中被保人是否唯一
        	responseOME = buildCardActiveResponseOME("01","85", "报文中被保人不唯一!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "85", "报文中被保人不唯一!");
            return false;
        }
        if(!BnfInfo){//判断报文中被保人是否唯一
        	responseOME = buildCardActiveResponseOME("01","86", "报文中受益人节点不为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "86", "报文中受益人节点不为空!");
            return false;
        }
        // --------------------

        return true;
    }
	
	/**
     * 锁定激活卡号。
     * @param cLCContSchema
     * @return
     */
    private MMap lockCont(String cardno)
    {
        MMap tMMap = null;

        // 签单锁定标志为：“SC”
        String tLockNoType = "WX";//网销锁定
        // -----------------------

        // 锁定有效时间
        String tAIS = "300";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cardno);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return tMMap;
    }
    /**
     * 统一化时间格式，避免时间格式不一致导致的校验失败。
     * @param date
     * @return
     * 如传入格式为20110201 转换为201121
     */
    private String getDate(String aDate,String flag){
    	if("0".equals(flag)){
    		if("0".equals(aDate.substring(4, 5)) && "0".equals(aDate.substring(6, 7))){
        		aDate = aDate.substring(0, 4)+aDate.substring(5,6)+aDate.substring(7);
    		}else if("0".equals(aDate.substring(4, 5))){
    			aDate = aDate.substring(0, 4)+aDate.substring(5);
    		}else if("0".equals(aDate.substring(6, 7))){
    			aDate = aDate.substring(0, 6)+aDate.substring(7);
    		}
    	}else{
    		if("0".equals(aDate.substring(2, 3)) &&  "0".equals(aDate.substring(4, 5))){
        		aDate = aDate.substring(0, 2)+aDate.substring(3,4)+aDate.substring(5);
    		}else if("0".equals(aDate.substring(2, 3))){
    			aDate = aDate.substring(0, 2)+aDate.substring(3);
    		}else if("0".equals(aDate.substring(4, 5))){
    			aDate = aDate.substring(0, 4)+aDate.substring(5);
    		}
    	}
    	return aDate;
    }
    private boolean checkInsuredInfos(WFInsuListSet tInsuListSet){
    	
		for (int i = 1; i <= tInsuListSet.size(); i++) {
			String Name = tInsuListSet.get(i).getName();//姓名
			String Sex = tInsuListSet.get(i).getSex();//性别
			String Birthday = tInsuListSet.get(i).getBirthday();//出生日期
			String IDType = tInsuListSet.get(i).getIDType();//证件类型
			String IDNo = tInsuListSet.get(i).getIDNo();//证件号码
			String occupAtionType = tInsuListSet.get(i).getOccupationType();//职业类别
			if(Birthday.indexOf("-")==-1){
				responseOME = buildCardActiveResponseOME("01","46", "被保人出生日期格式不正确，请输入标准日期格式如：2007-03-08!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "46", "被保人出生日期格式不正确，请输入标准日期格式如：2007-03-08!");
				return false;
			}
			//校验被保人年龄
			String ageWrapSql = "select case when maxinsuredage is null then -1 else maxinsuredage end,"
					+ " case when mininsuredage is null then -1 else mininsuredage end "
					+ " from ldriskwrap where riskwrapcode = '"
					+ contListSchema.getRiskCode()
					+ "' ";
			SSRS ageSSRS = tExeSQL.execSQL(ageWrapSql);
			if (ageSSRS != null && ageSSRS.MaxRow > 0) {
				if (!"".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"-1".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"0".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"".equals(StrTool.cTrim(ageSSRS.GetText(1, 2)))
						&& !"-1".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))) {
					int tInsurrdAge = PubFun.calInterval(Birthday, PubFun.getCurrentDate(), "Y");
					if (Integer.parseInt(ageSSRS.GetText(1, 1)) < tInsurrdAge
							|| Integer.parseInt(ageSSRS.GetText(1, 2)) > tInsurrdAge) {
						responseOME = buildCardActiveResponseOME("01","47", "被保人年龄不在投保年龄范围内!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "46", "被保人年龄不在投保年龄范围内!");
						return false;
					}
				}
			}
			//校验被保人职业类别
			String occutypeWrapSql = "select maxoccutype from ldwrap where riskwrapcode = '"+ contListSchema.getRiskCode() + "' ";
			SSRS occutypeSSRS = tExeSQL.execSQL(occutypeWrapSql);
			if (occutypeSSRS != null && occutypeSSRS.MaxRow > 0) {
				if (!"".equals(StrTool.cTrim(occutypeSSRS.GetText(1, 1)))) {
					if(occupAtionType == null || "".equals(occupAtionType)){
						responseOME = buildCardActiveResponseOME("01","48", "被保人职业类别不可以为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "46", "被保人职业类别不可以为空!");
						return false;
					}
					if(!PubFun.isNumLetter(occupAtionType)){
						responseOME = buildCardActiveResponseOME("01","49", "被保人职业类别应为数字，请核实!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "46", "被保人职业类别应为数字，请核实!");
						return false;
					}
					if (Integer.parseInt(occutypeSSRS.GetText(1, 1)) < Integer
							.parseInt(occupAtionType)) {
						responseOME = buildCardActiveResponseOME("01","50", "被保人职业类别不在投保职业类别范围内!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "46", "被保人职业类别不在投保职业类别范围内!");
						return false;
					}
				}
			}
			//校验份数
			//准备校验SQL,校验该激活卡是否是lmcardrisk中的risktype = 'W'的类型
			String checkSql = " select lmcr.riskcode, re.CertifyCode from lmcardrisk lmcr,"
					+ "(select lzcn.CardNo, lzc.CertifyCode, lzc.State from LZCardNumber lzcn "
					+ " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and "
					+ "lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
					+ "where 1 = 1 and lzcn.CardNo = '"
					+ contListSchema.getCardNo()
					+ "') re where lmcr.risktype = 'W' and lmcr.certifycode = re.certifycode ";
			SSRS checkSSRS = tExeSQL.execSQL(checkSql);

			String insureSql = null;
			String cardNoStr = contListSchema.getCardNo().toString().substring(0, 2);
			int alreadyNo = contListSchema.getCopys();//将本次购买份数保存到alreadyNo，然后再去加以后份数，校验总分数是否超过最大份数。
			if (checkSSRS.MaxRow != 0
					&& (contListSchema.getCvaliDate() != null || contListSchema.getCvaliDate() != "")) {
				insureSql = "select cardno,Cvalidate,InActiveDate from LICardActiveInfoList where CardNo "
						+ " like '"+ cardNoStr + "%' and name='"+ Name + "' and sex='"+ Sex + "' and birthday='"+ Birthday
						+ "' and idtype='"+ IDType + "' and idno='"	+ IDNo	+ "' and InActiveDate>'"+ PubFun.getCurrentDate()+ "' "
						+"and (CardStatus = '01' or CardStatus is null)";
				SSRS insureSSRS = tExeSQL.execSQL(insureSql);
				FDate fdate = new FDate();
				LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
				for (int j = 1; j <= insureSSRS.MaxRow; j++) {
					Date oldCvalidate = fdate.getDate(insureSSRS.GetText(j, 2)); //结果中的生效日期，用FDate处理
					Date oldInactivedate = fdate.getDate(insureSSRS.GetText(j,3)); //结果中失效日期，用FDate处理
					Date newCvalidate = fdate.getDate(contListSchema.getCvaliDate()); //当前激活卡的生效日期，用FDate处理
					String[] inactivedate = tLoginVerifyTool.getMyProductInfo(contListSchema.getCardNo(), contListSchema.getCvaliDate(),String.valueOf(contListSchema.getMult()));
					Date newInactivedate = fdate.getDate(inactivedate[2]); //当前激活卡的失效日期，用FDate处理		        
					if ((oldCvalidate.compareTo(newCvalidate) <= 0 && oldInactivedate.compareTo(newCvalidate) > 0)
							|| (oldCvalidate.compareTo(newInactivedate) < 0 && oldInactivedate.compareTo(newInactivedate) >= 0)) {
						//compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
						alreadyNo++;
					}
				}
				String maxNoSql = null;
				//取最大份数
				maxNoSql = " select maxcopys from ldwrap where riskwrapcode='" + contListSchema.getRiskCode() + "' ";
				String temp2 = tExeSQL.execSQL(maxNoSql).GetText(1, 1);
				int maxNo = 0;
				if (temp2.matches("^\\d+$")) {
					maxNo = Integer.parseInt(temp2);
					if (maxNo < alreadyNo) {
						responseOME = buildCardActiveResponseOME("01","51", "被保人投保份数已超过可投保的最大份数!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "46", "被保人投保份数已超过可投保的最大份数!");
						return false;
					}
				}
			} else {
				insureSql = "select count(1) from LICardActiveInfoList where CardNo "
						+ " like '"	+ cardNoStr	+ "%' and name='" + Name + "' and sex='" + Sex
						+ "' and birthday='" + Birthday	+ "' and idtype='"	+ IDType
						+ "' and idno='" + IDNo	+ "' ";
				String temp1 = tExeSQL.execSQL(insureSql).GetText(1, 1);
				if (temp1.matches("^\\d+$")) {
					alreadyNo += Integer.parseInt(temp1);
				}
				String maxNoSql = null;
				maxNoSql = " select maxcopys from ldwrap where riskwrapcode='"+ contListSchema.getRiskCode() + "' ";
				String temp2 = tExeSQL.execSQL(maxNoSql).GetText(1, 1);
				int maxNo = 0;
				if (temp2.matches("^\\d+$")) {
					maxNo = Integer.parseInt(temp2);
					if (maxNo < alreadyNo) {
						responseOME = buildCardActiveResponseOME("01","52", "被保人投保份数已超过可投保的最大份数!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								"100", "46", "被保人投保份数已超过可投保的最大份数!");
						return false;
					}
				}
			}
		}
		return true;
	}
	/**
     * 对产品的特殊规定进行校验。
     * @param insuListSet
     * @return
     */
	private boolean checkSpec(WFInsuListSet insuListSet){
		boolean flag = true;
		LMCheckFieldDB tLMCheckFieldDB= new LMCheckFieldDB();
		tLMCheckFieldDB.setRiskCode(contListSchema.getRiskCode());
		tLMCheckFieldDB.setFieldName("ETBINSERT");
		LMCheckFieldSet tLMCheckFieldSet = tLMCheckFieldDB.query();
		for(int i=1;i<=tLMCheckFieldSet.size();i++){
			LMCheckFieldSchema tLMCheckFieldSchema = tLMCheckFieldSet.get(i);
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			tLMCalModeDB.setCalCode(tLMCheckFieldSchema.getCalCode());
			LMCalModeSet tLMCalModeSet = new LMCalModeSet();
			tLMCalModeSet = tLMCalModeDB.query();
			for(int j=1;j<=tLMCalModeSet.size();j++){
				LMCalModeSchema tLMCalModeSchema = tLMCalModeSet.get(j);
				for(int k=1;k<=insuListSet.size();k++){
					WFInsuListSchema insuListSchema = insuListSet.get(k);
					int tAppntAge = PubFun.calInterval(insuListSchema.getBirthday(),
							PubFun.getCurrentDate(), "Y");
					Calculator cal = new Calculator();
					cal.setCalCode(tLMCalModeSchema.getCalCode());
					cal.addBasicFactor("InsuredName", insuListSchema.getName());
					cal.addBasicFactor("Sex", insuListSchema.getSex());
					cal.addBasicFactor("InsuredBirthday", insuListSchema.getBirthday());
					cal.addBasicFactor("IDType", insuListSchema.getIDType());
					cal.addBasicFactor("IDNo", insuListSchema.getIDNo());
					cal.addBasicFactor("RiskWrapCode", contListSchema.getRiskCode());
					cal.addBasicFactor("AppAge",tAppntAge+"");
					cal.addBasicFactor("Mult",contListSchema.getMult()+"");
					cal.addBasicFactor("OccupationType",insuListSchema.getOccupationType());
					cal.addBasicFactor("Cvalidate",contListSchema.getCvaliDate());
					String result = cal.calculate();
					if(!result.equals("0")){
						flag = false;
						mUwErrorList.add(tLMCalModeSchema.getRemark());
					}
				}
			}
		}
		return flag;
	}
    /**
     * 处理单证关键要素（保额、保费、档次、份数等）。
     * @param contListSchema
     * @return
     */
    private boolean dealCertifyWrapParams()
    {

        String tCopysFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Copys' and lmcr.CertifyCode = '" + contListSchema.getCertifyCode() + "' ");
        if (!"1".equals(tCopysFlag) && contListSchema.getCopys() != 1)
        {
        	responseOME = buildCardActiveResponseOME("01","53", "单证号[" + contListSchema.getCardNo() + "]对应套餐份数不能大于1。");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "46", "单证号[" + contListSchema.getCardNo() + "]对应套餐份数不能大于1。");
            return false;
        }
        // -------------------------------------------

        // 校验档次，但不对POS单证进行档次的对应校验
        String tMultFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + contListSchema.getCertifyCode() + "' " + "union all "
                + " select 1 from LMCertifyDes lmcd where lmcd.CertifyCode = '" + contListSchema.getCertifyCode()
                + "' and lmcd.OperateType = '3'");
        if ("1".equals(tMultFlag))
        {
        	String MultSql = "select calfactortype,calfactorvalue from LDRiskDutyWrap ldrdw inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
        		+"where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + contListSchema.getCertifyCode() + "' ";
        	SSRS tSSRS = new ExeSQL().execSQL(MultSql);
        	if(tSSRS != null && tSSRS.MaxRow>0){
        		if("2".equals(tSSRS.GetText(1, 1)) && contListSchema.getMult() == 0){//当需要传递档次，而档次为0时，则阻断。
        			responseOME = buildCardActiveResponseOME("01","54", "单证号[" + contListSchema.getCardNo() + "]对应套餐需含有档次要素。");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "46", "单证号[" + contListSchema.getCardNo() + "]对应套餐需含有档次要素。");
                    return false;
        		}
        	}
        }else{
    		if(contListSchema.getMult() != 0){//没有档次要素，当传递档次时，则阻断
    			responseOME = buildCardActiveResponseOME("01","55", "单证号[" + contListSchema.getCardNo() + "]对应套餐不应含有档次要素。");
    			System.out.println("responseOME:" + responseOME);
    			LoginVerifyTool.writeLog(responseOME, batchno,
    					messageType, sendDate, sendTime, "01",
    					"100", "46", "单证号[" + contListSchema.getCardNo() + "]对应套餐不应含有档次要素。");
                return false;
            }
        }
        // -------------------------------------------

        LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

        String tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
                + " where lmcr.CertifyCode = '"
                + contListSchema.getCertifyCode()
                + "' "
                + " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
        tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
        {
        	responseOME = buildCardActiveResponseOME("01","56","单证号[" + contListSchema.getCardNo() + "]对应套餐要素信息未找到。");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "46", "单证号[" + contListSchema.getCardNo() + "]对应套餐要素信息未找到。");
            return false;
        }

        // 处理要素信息。
        for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++)
        {
            LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);

            String tCalFactorType = tRiskDutyParam.getCalFactorType();
            String tCalFactor = tRiskDutyParam.getCalFactor();
            String tCalFactorValue = tRiskDutyParam.getCalFactorValue();

            if ("Prem".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(contListSchema.getCopys()));
                    tCal.addBasicFactor("Mult", String.valueOf(contListSchema.getMult()));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                    	responseOME = buildCardActiveResponseOME("01","57","单证号[" + contListSchema.getCardNo() + "]保费计算失败，请核查算费参数。");
            			System.out.println("responseOME:" + responseOME);
            			LoginVerifyTool.writeLog(responseOME, batchno,
            					messageType, sendDate, sendTime, "01",
            					"100", "46", "单证号[" + contListSchema.getCardNo() + "]保费计算失败，请核查算费参数。");
                        return false;
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumPrem = Arith.add(tSysWrapSumPrem, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                	responseOME = buildCardActiveResponseOME("01","58","套餐要素中保费出现非数值型字符串。");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "46", "套餐要素中保费出现非数值型字符串。");
                    return false;
                }
            }

            if ("Amnt".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(contListSchema.getCopys()));
                    tCal.addBasicFactor("Mult", String.valueOf(contListSchema.getMult()));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                    	responseOME = buildCardActiveResponseOME("01","59","单证号[" + contListSchema.getCardNo() + "]保额计算失败。");
            			System.out.println("responseOME:" + responseOME);
            			LoginVerifyTool.writeLog(responseOME, batchno,
            					messageType, sendDate, sendTime, "01",
            					"100", "46", "单证号[" + contListSchema.getCardNo() + "]保额计算失败。");
                        return false;
                        // tTmpCalResult = "0";
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                	responseOME = buildCardActiveResponseOME("01","60","套餐要素中保额出现非数值型字符串。");
        			System.out.println("responseOME:" + responseOME);
        			LoginVerifyTool.writeLog(responseOME, batchno,
        					messageType, sendDate, sendTime, "01",
        					"100", "46", "套餐要素中保额出现非数值型字符串。");
                    return false;
                }
            }
        }
        // ------------------------------------------------

        if (contListSchema.getPrem() != 0 && contListSchema.getPrem() != tSysWrapSumPrem)
        {
        	responseOME = buildCardActiveResponseOME("01","61","单证号[" + contListSchema.getCardNo() + "]系统计算出保费：" + tSysWrapSumPrem + "，与获取保费：" + contListSchema.getPrem() + "不一致。");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "46", "单证号[" + contListSchema.getCardNo() + "]系统计算出保费：" + tSysWrapSumPrem + "，与获取保费：" + contListSchema.getPrem() + "不一致。");
            return false;
        }

        if (contListSchema.getAmnt() != 0 && contListSchema.getAmnt() != tSysWrapSumAmnt)
        {
        	responseOME = buildCardActiveResponseOME("01","62","单证号[" + contListSchema.getCardNo() + "]系统计算出保额：" + tSysWrapSumAmnt + "，与获取保额：" + contListSchema.getAmnt() + "不一致。");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,
					messageType, sendDate, sendTime, "01",
					"100", "46", "单证号[" + contListSchema.getCardNo() + "]系统计算出保额：" + tSysWrapSumAmnt + "，与获取保额：" + contListSchema.getAmnt() + "不一致。");
            return false;
        }

        return true;
    }
}
