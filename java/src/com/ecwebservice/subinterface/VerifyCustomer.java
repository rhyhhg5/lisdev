package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 根据报文中的客户信息查询是否为保险公司客户
 * 
 * @author abc
 * 
 */
public class VerifyCustomer {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = new GlobalInput();

	private OMElement mOME = null;

	private String batchNo = null;// 批次号

	private String branchCode = "";// 报送单位

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String sendOperator = ""; // 报送人员

	private String msgType = null; // 报文类型

	private String name = null; // 客户姓名

	private String idType = null; // 证件类型

	private String idNo = null; // 证件号码

	private String mobile = null; // 移动电话

	private String email = null;

	private String address = null; // 联系地址

	private String state = null; // 处理状态：00－成功；01－失败

	private OMElement responseOME = null;

	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if (!load()) {
			return responseOME;
		}

		if (!check()) {
			return responseOME;
		}

		if (!dealData()) {
			return responseOME;
		}

		return responseOME;
	}

	private boolean check() {
		if ((this.name == null || this.name == "")
				&& (this.idType == null || this.idType == "")
				&& (this.idNo == null || this.idNo == "")) {
			responseOME = buildResponseOME("01", "03", "客户的姓名、证件类型、证件号码不可以全为空！");
			return false;
		}
		
		if(!"0".equals(this.idType)&&!"1".equals(this.idType)
				&&!"2".equals(this.idType)&&!"3".equals(this.idType)
				&&!"4".equals(this.idType)){
			responseOME = buildResponseOME("01", "04", "证件类型错误!");
			return false;
		}
		
		if("".equals(this.name)&&"".equals(this.idNo)){
			responseOME = buildResponseOME("01", "03", "客户的姓名、证件号码不可以同时为空！");
			return false;
		}
		
		return true;
	}

	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				// 根节点
				if (Om.getLocalName().equals("LIS001")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BatchNo")) {
							this.batchNo = child_element.getText();
							continue;
						}

						if (child_element.getLocalName().equals("SendDate")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendTime")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("BranchCode")) {
							this.branchCode = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendOperator")) {
							this.sendOperator = child_element.getText();
							continue;
						}
						// 验证信息Verify
						if (child_element.getLocalName().equals("Verify")) {
							Iterator childVerify = child_element.getChildren();
							while (childVerify.hasNext()) {
								OMElement child_element_verify = (OMElement) childVerify.next();

								if (child_element_verify.getLocalName().equals(
										"ITEM")) {
									Iterator childItem = child_element_verify.getChildren();
									while (childItem.hasNext()) {
										OMElement child_item = (OMElement) childItem.next();

										if (child_item.getLocalName().equals("Name")) {
											this.name = child_item.getText();
										}
										if (child_item.getLocalName().equals("IDType")) {
											this.idType = child_item.getText();
										}
										if (child_item.getLocalName().equals("IDNo")) {
											this.idNo = child_item.getText();
										}
										if (child_item.getLocalName().equals("Mobile")) {
											this.mobile = child_item.getText();
										}
										if (child_item.getLocalName().equals("Email")) {
											this.email = child_item.getText();
										}
										if (child_item.getLocalName().equals("Address")) {
											this.address = child_item.getText();
										}
									}
								}
							}
						}
					}
				} else {
					System.out.println("报文类型不匹配");
					responseOME = buildResponseOME("01", "01", "报文类型不匹配");
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME = buildResponseOME("01", "01", "加载报文信息异常");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private boolean dealData() {
		try {
			LDPersonDB tLDPersonDB = new LDPersonDB();
			tLDPersonDB.setName(this.name);
			tLDPersonDB.setIDType(this.idType);
			tLDPersonDB.setIDNo(this.idNo);
	
			LDPersonSet tLDPersonSet = tLDPersonDB.query();
			if (tLDPersonSet.size() == 0) {
				//如果不是保险公司客户，返回成功
				responseOME = buildResponseOME("00", null, null);
			} else {
				SSRS tSSRS = new SSRS();
				for (int m = 1; m <= tLDPersonSet.size(); m++) {
					String pSql=" select b.mobile from lcinsured a,lcaddress b"
							+" where a.insuredno='"+tLDPersonSet.get(m).getCustomerNo()+"' and b.customerno=a.insuredno and b.addressno=a.addressno"
							+" union "
							+" select b.mobile from lbinsured a,lcaddress b "
							+" where a.insuredno='"+tLDPersonSet.get(m).getCustomerNo()+"' and b.customerno=a.insuredno and b.addressno=a.addressno and a.edorno not like 'xb%' "
							+" union "
							+" select b.mobile from lcappnt a,lcaddress b "
							+" where a.appntno='"+tLDPersonSet.get(m).getCustomerNo()+"'  and b.customerno=a.appntno and b.addressno=a.addressno "
							+" union "
							+" select b.mobile from lbappnt a,lcaddress b "
							+" where a.appntno='"+tLDPersonSet.get(m).getCustomerNo()+"' and b.customerno=a.appntno and b.addressno=a.addressno and a.edorno not like 'xb%' "
							+" with ur ";
						
					tSSRS = new ExeSQL().execSQL(pSql);
						
					if (null != tSSRS && tSSRS.MaxRow > 0){
						for(int i=1;i<=tSSRS.getMaxRow();i++){
							String lcMobile =tSSRS.GetText(i,1);
							//若客户移动号码不匹配则返回失败，若系统中的移动电话为空则通过
							if(null==lcMobile||"".equals(lcMobile)){
								responseOME = buildResponseOME("00", null, null);
								return true;
							}
							if (null != lcMobile && !"".equals(lcMobile) && lcMobile.equals(this.mobile)) {
								responseOME = buildResponseOME("00", null, null);
								return true;
							}
						}
					}else{
						responseOME = buildResponseOME("00", null, null);
						return true;
					}
				}
				responseOME = buildResponseOME("01", null, null);
			}
		} catch (Exception e) {
			responseOME = buildResponseOME("01", "03", "查询客户信息失败");
			return false;
		}
		return true;
	}

	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement LIS001 = fac.createOMElement("LIS001", null);
		addOm(LIS001, "BatchNo", this.batchNo);
		addOm(LIS001, "SendDate", PubFun.getCurrentDate());
		addOm(LIS001, "SendTime", PubFun.getCurrentTime());
		addOm(LIS001, "BranchCode",	this.branchCode);
		addOm(LIS001, "SendOperator", this.sendOperator);
		addOm(LIS001, "State", state);
		addOm(LIS001, "ErrCode", errorCode);
		addOm(LIS001, "ErrInfo", errorInfo);
		return LIS001;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
}
