package com.ecwebservice.subinterface;

import java.util.Iterator;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class QueryBonusInfo {

	private OMElement mOME = null;

	private String batchNo = null;// 批次号

	private String branchCode = "";// 报送单位

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String sendOperator = null; // 报送人员
	
	private String contNo="";	
	
	private String stateFlag="";	//保单状态
	
	private MMap mMap=new MMap();	//存放分红信息
	
	private OMElement responseOME = null;
	
	
	
	public OMElement queryData(OMElement Oms) {
		this.mOME=Oms.cloneOMElement();
		if(!load()){
			return responseOME;
		}
		
		if(!check()){
			return responseOME;
		}
		
		if(!deal()){
			return responseOME;
		}
		
		return responseOME=buildResponseOME("00","","");
	}
	
	private boolean deal() {
		
		//查询保单的状态
		String stateSQL=" select codename from ldcode where codetype='stateflag' " +
				" and code=" +
				" (select stateflag from lccont where contno='"+this.contNo+"') " +
				" with ur";
		this.stateFlag=new ExeSQL().getOneValue(stateSQL);
		
		String strSQL="select b.riskcode,(select riskname from lmriskapp where riskcode=b.riskcode)," +
				" year(a.BonusMakeDate)," +
				" (select codename from ldcode where codetype='bonusgetmode' and code=b.bonusgetmode)," +
				" a.sgetdate,a.BonusMoney," +
				" nvl((select InsuAccBala from lcinsureacc where polno=a.polno),0) " +
				" from lobonuspol a,lcpol b " +
				" where a.contno='"+this.contNo+"' " +
				" and b.polno=a.polno " +
				" with ur";
		
		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(strSQL);
		if (null == tSSRS || tSSRS.MaxRow==0){
			this.mMap.put("", new String[]{"","","","","","",""});//保证返回报文的完整性
			responseOME=buildResponseOME("01","02","没有查询到保单"+this.contNo+"的分红信息!");
			return false;
		}else{
			for(int i=1;i<=tSSRS.getMaxRow();i++){
				int length=tSSRS.getRowData(i).length;
				String[] bonusInfo=new String[length];
				for(int j=0;j<length;j++){
					bonusInfo[j]=tSSRS.GetText(i, j+1);
				}
				this.mMap.put(bonusInfo[0], bonusInfo);
			}
		}
		
		return true;
	}

	private boolean check() {
		if(null==this.contNo||"".equals(this.contNo)){
			this.mMap.put("", new String[]{"","","","","","",""});
			responseOME=buildResponseOME("01","02","获取保单号失败");
			return false;
		}
		return true;
	}

	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("LIS007")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BatchNo")) {
							this.batchNo = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendDate")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendTime")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("BranchCode")) {
							this.branchCode = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendOperator")) {
							this.sendOperator = child_element.getText();
							continue;
						}
						// 业务员信息
						if (child_element.getLocalName().equals("Bonus")) {
							Iterator childVerify = child_element.getChildren();
							
							while (childVerify.hasNext()) {
								OMElement child_element_verify = (OMElement) childVerify
										.next();

								if (child_element_verify.getLocalName().equals("ITEM")) {
									Iterator childItem = child_element_verify.getChildren();
									while (childItem.hasNext()) {
										OMElement child_item = (OMElement) childItem.next();

										if (child_item.getLocalName().equals("Contno")) {
											this.contNo = child_item.getText();
										}
									}
								}
							}
							
						}
						
					}

				} else {
					System.out.println("报文类型不匹配");
					responseOME=buildResponseOME("01","01","报文类型不匹配");
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME=buildResponseOME("01","01","加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement("LIS007", null);
		OMElement BONUS = fac.createOMElement("Bonus", null);
		
		addOm(DATASET, "BatchNo", this.batchNo);
		addOm(DATASET, "SendDate", PubFun.getCurrentDate());
		addOm(DATASET, "SendTime", PubFun.getCurrentTime());
		addOm(DATASET, "BranchCode", this.branchCode);
		addOm(DATASET, "SendOperator", this.sendOperator);
		addOm(DATASET, "State", state);
		addOm(DATASET, "ErrCode", errorCode);
		addOm(DATASET, "ErrInfo", errorInfo);
		addOm(DATASET, "Contno", this.contNo);
		addOm(DATASET, "State", this.stateFlag);
		
		Set set=mMap.keySet();
		Object key = null;
		for(int i=0;i<set.size();i++){
			OMElement item = fac.createOMElement("ITEM", null);
			key = mMap.getOrder().get(String.valueOf(i + 1));
			String[] temp = (String[]) mMap.get(key);
				
				addOm(item, "RiskCode",temp[0]);
				addOm(item, "RiskName",temp[1]);
				addOm(item, "PublishAnnual",temp[2]);
				addOm(item, "DistributionType",temp[3]);
				addOm(item, "DistributionTime",temp[4]);
				addOm(item, "DistributionAmount",temp[5]);
				addOm(item, "CurrentAmount",temp[6]);
				BONUS.addChild(item);
		}
		DATASET.addChild(BONUS);
		
		return DATASET;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
	
}
