package com.ecwebservice.subinterface;

import java.util.HashMap;
import java.util.Map;

import com.ecwebservice.topinterface.CardTopInterface;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class HYXQueryAgentComInfo extends CardTopInterface {
	
	 private SSRS resultSSRS;
	public HYXQueryAgentComInfo()
    {
        super("HYXQUERYAGENTCOM");

    }

	protected boolean checkData(Map conditionMap) {
		String agentCom = (String) conditionMap.get("AGENTCOMCODE");
        String comCode = (String) conditionMap.get("COMCODE");
        ExeSQL mExeSQL = new ExeSQL();
        //����У��
        String agentSql = "select la.agentcom,la.name,la.managecom,ld.name from lacom la " +
        		"inner join ldcom ld on la.managecom=ld.comcode " +
        		"where  la.agentcom ='"+agentCom+"' and la.managecom like '"+comCode+"%'";
        resultSSRS = mExeSQL.execSQL(agentSql);
        if (resultSSRS.MaxRow > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
	}

	protected void getQueryFactor(Map queryFactor) {
		// TODO Auto-generated method stub
		
	}

	public Map prepareData() {
		 Map resultMap = new HashMap();
	        if (resultSSRS.MaxRow > 0)
	        {
	            Map agentInfoMap = new HashMap();
	            agentInfoMap.put("AGENTCOMCODE", resultSSRS.GetText(1, 1));
	            agentInfoMap.put("AGENTCOMNAME", resultSSRS.GetText(1, 2));
	            agentInfoMap.put("COMCODE", resultSSRS.GetText(1, 3));
	            agentInfoMap.put("COMNAME", resultSSRS.GetText(1, 4));

	            resultMap.put("AGENTCOMINFO", agentInfoMap);
	        }
	        return resultMap;
	}

}
