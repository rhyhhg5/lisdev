package com.ecwebservice.subinterface;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;


import com.sinosoft.lis.db.WFAppntListDB;
import com.sinosoft.lis.db.WFBnfListDB;
import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.db.WFInsuListDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;

import com.sinosoft.lis.pubfun.PubSubmit;

import com.sinosoft.lis.schema.LICardActModifySchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * Title:远程出单 当日保单修改
 * 
 * @author yuanzw 创建时间：2010-2-3 下午02:43:12
 * @Description:
 * @version
 */

public class HYXEndor {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String cardNo = null;// 单证号

	private String sendOperator = null;// g 非发送报文提供

	MMap map = new MMap();

	VData tVData = new VData();

	// 错误处理类
	public CErrors mErrors = new CErrors();

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {
			LoginVerifyTool mTool = new LoginVerifyTool();

			WFContListSchema tContListSchema = null;
			WFAppntListSchema appntListSchema = null;
			WFInsuListSet insuListSet = new WFInsuListSet();
			WFInsuListSchema insuListSchema = null;
			WFBnfListSet bnfListSet = new WFBnfListSet();
			WFBnfListSchema bnfListSchema = null;

			String relationCode = null;
			String relationToInsured = null;
			String reInsuNo = null;

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("HYXENDOR")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println("BATCHNO" + batchno);
							boolean validBatchNo = mTool
									.isValidBatchNo(batchno);
							if (!validBatchNo) {
								// 批次号不唯一
								responseOME = buildResponseOME("01", "21",
										"批次号不唯一!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "21", "批次号不唯一!");
								return responseOME;
							}
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);
							if (!"03".equals(messageType)) {
								responseOME = buildResponseOME("01", "5",
										"报文类型不正确!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "5", "报文类型不正确!");
								return responseOME;
							}
						}

						if (child_element.getLocalName().equals("CARDNO")) {
							cardNo = child_element.getText();

							System.out.println(cardNo);

							String sql = "select senddate,sendtime,batchno from   "
									+ "wfcontlist where cardNo='"
									+ cardNo
									+ "' order by date(senddate) desc,time(sendtime) desc "
									+ "fetch first 1 rows only";

							SSRS sResult = new SSRS();
							ExeSQL tExeSQL = new ExeSQL();
							sResult = tExeSQL.execSQL(sql);
							if (null != sResult && sResult.MaxRow > 0) {

								// String sendDate = sResult.GetText(1, 1);
								// String sendTime = sResult.GetText(1, 2);
								String aBatchNo = sResult.GetText(1, 3);

								// contListDB.setCardNo(cardNo);
								// contListDB.setSendDate(sendDate);
								// contListDB.setSendTime(sendTime);
								//
								// if (contListDB.query().size() != 0) {
								// contListSchema = contListDB.query().get(1);
								// password = contListSchema.getPassword();
								// certifyCode =
								// contListSchema.getCertifyCode();
								// cardDealType =
								// contListSchema.getCardDealType();
								//
								// }

								// 保险卡信息查询

								WFContListDB contListDB = new WFContListDB();
								contListDB.setBatchNo(aBatchNo);
								if (contListDB.query().size() != 0) {
									tContListSchema = contListDB.query().get(1);
								}
								// 投保人信息查询
								WFAppntListDB appntListDB = new WFAppntListDB();
								appntListDB.setBatchNo(aBatchNo);

								if (appntListDB.query().size() != 0) {
									appntListSchema = appntListDB.query()
											.get(1);

								}

								// 被保人信息
								WFInsuListDB insuListDB = new WFInsuListDB();
								insuListDB.setBatchNo(aBatchNo);

								if (insuListDB.query().size() != 0) {
									insuListSchema = insuListDB.query().get(1);

								}
								// 受益人信息
								WFBnfListDB bnfListDB = new WFBnfListDB();
								bnfListDB.setBatchNo(aBatchNo);

								if (bnfListDB.query().size() != 0) {
									bnfListSchema = bnfListDB.query().get(1);

								}

							}
						}

						// 投保人信息

						if (child_element.getLocalName().equals("APPNTLIST")) {
							// 投保人信息
							String name = null;
							String sex = null;
							String birthday = null;
							String IDType = null;
							String IDNo = null;
							String occupAtionType = null;
							String occupAtionCode = null;
							String postalAddress = null;
							String zipCode = null;
							String phont = null;
							String mobile = null;
							String email = null;
							String grpName = null;
							String companyPhone = null;
							String compaAddr = null;
							String compZipCode = null;
							Iterator child1 = child_element.getChildren();

							while (child1.hasNext()) {
								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										if (child2OME.getLocalName().equals(
												"NAME")
												&& child2OME.getText() != null) {
											name = child2OME.getText();

											appntListSchema.setName(child2OME
													.getText());
											System.out.println("name" + name);
										}
										if (child2OME.getLocalName().equals(
												"SEX")
												&& child2OME.getText() != null) {
											sex = child2OME.getText();
											appntListSchema.setSex(mTool
													.codeSex(child2OME
															.getText()));
											System.out.println(sex);
										}
										if (child2OME.getLocalName().equals(
												"BIRTHDAY")
												&& child2OME.getText() != null) {
											birthday = child2OME.getText();
											appntListSchema
													.setBirthday(child2OME
															.getText());
											System.out.println(birthday);
										}
										if (child2OME.getLocalName().equals(
												"IDTYPE")
												&& child2OME.getText() != null) {
											IDType = mTool.codeIDType(child2OME
													.getText());
											appntListSchema.setIDType(mTool
													.codeIDType(child2OME
															.getText()));
											System.out.println(IDType);
										}
										if (child2OME.getLocalName().equals(
												"IDNO")
												&& child2OME.getText() != null) {
											IDNo = child2OME.getText();
											appntListSchema.setIDNo(child2OME
													.getText());
											System.out.println(IDNo);
										}
										if (child2OME.getLocalName().equals(
												"OCCUPATIONTYPE")
												&& child2OME.getText() != null) {
											occupAtionType = child2OME
													.getText();
											appntListSchema
													.setOccupationType(child2OME
															.getText());
											System.out.println(occupAtionType);
										}
										if (child2OME.getLocalName().equals(
												"OCCUPATIONCODE")
												&& child2OME.getText() != null) {
											occupAtionCode = child2OME
													.getText();
											appntListSchema
													.setOccupationCode(child2OME
															.getText());
											System.out.println(occupAtionCode);
										}
										if (child2OME.getLocalName().equals(
												"POSTALADDRESS")
												&& child2OME.getText() != null) {
											postalAddress = child2OME.getText();
											appntListSchema
													.setPostalAddress(child2OME
															.getText());
											System.out.println(postalAddress);
										}
										if (child2OME.getLocalName().equals(
												"ZIPCODE")
												&& child2OME.getText() != null) {
											zipCode = child2OME.getText();
											appntListSchema
													.setZipCode(child2OME
															.getText());
											System.out.println(zipCode);
										}
										if (child2OME.getLocalName().equals(
												"PHONT")
												&& child2OME.getText() != null) {
											phont = child2OME.getText();
											appntListSchema.setPhont(child2OME
													.getText());
											System.out.println(phont);
										}
										if (child2OME.getLocalName().equals(
												"MOBILE")
												&& child2OME.getText() != null) {
											mobile = child2OME.getText();
											appntListSchema.setMobile(child2OME
													.getText());
											System.out.println(mobile);
										}
										if (child2OME.getLocalName().equals(
												"EMAIL")
												&& child2OME.getText() != null) {
											email = child2OME.getText();
											appntListSchema.setEmail(child2OME
													.getText());
											System.out.println(email);
										}
										if (child2OME.getLocalName().equals(
												"GRPNAME")
												&& child2OME.getText() != null) {
											grpName = child2OME.getText();
											appntListSchema
													.setGrpName(child2OME
															.getText());
											System.out.println(grpName);
										}
										if (child2OME.getLocalName().equals(
												"COMPANYPHONE")
												&& child2OME.getText() != null) {
											companyPhone = child2OME.getText();
											appntListSchema
													.setCompanyPhone(child2OME
															.getText());
											System.out.println(companyPhone);
										}
										if (child2OME.getLocalName().equals(
												"COMPADDR")
												&& child2OME.getText() != null) {
											compaAddr = child2OME.getText();
											appntListSchema
													.setCompAddr(child2OME
															.getText());
											System.out.println(compaAddr);
										}
										if (child2OME.getLocalName().equals(
												"COMPZIPCODE")
												&& child2OME.getText() != null) {
											compZipCode = child2OME.getText();
											appntListSchema
													.setCompZipCode(child2OME
															.getText());
											System.out.println(compZipCode);
										}

									}
								}

								appntListSchema.setBatchNo(batchno);
								appntListSchema.setCardNo(cardNo);

								map.put(appntListSchema, "INSERT");
							}

						}

						// 被保人信息
						String toInsuNo = null;// 受益人信息中受益人所属的所属被保人
						if (child_element.getLocalName().equals("INSULIST")) {
							
							int insuNo = 1;// 被保人信息
							// String insuNo = null;
							String toAppntRela = null;
							String name = null;
							String sex = null;
							String birthday = null;
							String IDType = null;
							String IDNo = null;
							String occupAtionType = null;
							String occupAtionCode = null;
							String postalAddress = null;
							String zipCode = null;
							String phont = null;
							String mobile = null;
							String email = null;
							String grpName = null;
							String companyPhone = null;
							String compaAddr = null;
							String compZipCode = null;
							Iterator child1 = child_element.getChildren();
							while (child1.hasNext()) {
								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();

										if (child2OME.getLocalName().equals(
												"TOAPPNTRELA")
												&& child2OME.getText() != null) {

											// toAppntRela =
											// child2OME.getText();
											insuListSchema.setToAppntRela(mTool
													.codeToAppntRela(child2OME
															.getText()));
											System.out.println("toAppntRela"
													+ toAppntRela);
										}
										if (child2OME.getLocalName().equals(
												"RELATIONCODE")
												&& child2OME.getText() != null) {
											relationCode = child2OME.getText();
											insuListSchema
													.setRelationCode(child2OME
															.getText());
											System.out.println(relationCode);
										}
										if (child2OME.getLocalName().equals(
												"RELATIONTOINSURED")
												&& child2OME.getText() != null) {
											relationToInsured = child2OME
													.getText();
											insuListSchema
													.setRelationToInsured(child2OME
															.getText());
											System.out
													.println(relationToInsured);
										}
										if (child2OME.getLocalName().equals(
												"REINSUNO")
												&& child2OME.getText() != null) {
											reInsuNo = child2OME.getText();
											insuListSchema
													.setReInsuNo(child2OME
															.getText());
											System.out.println(reInsuNo);
										}
										if (child2OME.getLocalName().equals(
												"NAME")
												&& child2OME.getText() != null) {
											name = child2OME.getText();
											insuListSchema.setName(child2OME
													.getText());
											System.out.println("name" + name);
										}
										if (child2OME.getLocalName().equals(
												"SEX")
												&& child2OME.getText() != null) {
											sex = child2OME.getText();
											insuListSchema.setSex(mTool
													.codeSex(child2OME
															.getText()));
											System.out.println(sex);
										}
										if (child2OME.getLocalName().equals(
												"BIRTHDAY")
												&& child2OME.getText() != null) {
											birthday = child2OME.getText();
											insuListSchema
													.setBirthday(child2OME
															.getText());
											System.out.println(birthday);
										}
										if (child2OME.getLocalName().equals(
												"IDTYPE")
												&& child2OME.getText() != null) {
											IDType = mTool.codeIDType(child2OME
													.getText());
											insuListSchema.setIDType(mTool
													.codeIDType(child2OME
															.getText()));
											System.out.println(IDType);
										}
										if (child2OME.getLocalName().equals(
												"IDNO")
												&& child2OME.getText() != null) {
											IDNo = child2OME.getText();
											insuListSchema.setIDNo(child2OME
													.getText());
											System.out.println(IDNo);
										}
										if (child2OME.getLocalName().equals(
												"OCCUPATIONTYPE")
												&& child2OME.getText() != null) {
											occupAtionType = child2OME
													.getText();
											insuListSchema
													.setOccupationType(child2OME
															.getText());
											System.out.println(occupAtionType);
										}
										if (child2OME.getLocalName().equals(
												"OCCUPATIONCODE")
												&& child2OME.getText() != null) {
											occupAtionCode = child2OME
													.getText();
											insuListSchema
													.setOccupationCode(child2OME
															.getText());
											System.out.println(occupAtionCode);
										}
										if (child2OME.getLocalName().equals(
												"POSTALADDRESS")
												&& child2OME.getText() != null) {
											postalAddress = child2OME.getText();
											insuListSchema
													.setPostalAddress(child2OME
															.getText());
											System.out.println(postalAddress);
										}
										if (child2OME.getLocalName().equals(
												"ZIPCODE")
												&& child2OME.getText() != null) {
											zipCode = child2OME.getText();
											insuListSchema.setZipCode(child2OME
													.getText());
											System.out.println(zipCode);
										}
										if (child2OME.getLocalName().equals(
												"PHONT")
												&& child2OME.getText() != null) {
											phont = child2OME.getText();
											insuListSchema.setPhont(child2OME
													.getText());
											System.out.println(phont);
										}
										if (child2OME.getLocalName().equals(
												"MOBILE")
												&& child2OME.getText() != null) {
											mobile = child2OME.getText();
											insuListSchema.setMobile(child2OME
													.getText());
											System.out.println(mobile);
										}
										if (child2OME.getLocalName().equals(
												"EMAIL")
												&& child2OME.getText() != null) {
											email = child2OME.getText();
											insuListSchema.setEmail(child2OME
													.getText());
											System.out.println(email);
										}
										if (child2OME.getLocalName().equals(
												"GRPNAME")
												&& child2OME.getText() != null) {
											grpName = child2OME.getText();
											insuListSchema.setGrpName(child2OME
													.getText());
											System.out.println(grpName);
										}
										if (child2OME.getLocalName().equals(
												"COMPANYPHONE")
												&& child2OME.getText() != null) {
											companyPhone = child2OME.getText();
											insuListSchema
													.setCompanyPhone(child2OME
															.getText());
											System.out.println(companyPhone);
										}
										if (child2OME.getLocalName().equals(
												"COMPADDR")
												&& child2OME.getText() != null) {
											compaAddr = child2OME.getText();
											insuListSchema
													.setCompAddr(child2OME
															.getText());
											System.out.println(compaAddr);
										}
										if (child2OME.getLocalName().equals(
												"COMPZIPCODE")
												&& child2OME.getText() != null) {
											compZipCode = child2OME.getText();
											insuListSchema
													.setCompZipCode(child2OME
															.getText());
											System.out.println(compZipCode);
										}

									}
								}

								// 不为空
								insuListSchema.setBatchNo(batchno);
								insuListSchema.setCardNo(cardNo);

								insuListSet.add(insuListSchema);

							}
							map.put(insuListSet, "INSERT");

						}
						// 受益人信息

						if (child_element.getLocalName().equals("BNFLIST")) {
							// 受益人信息
							if (bnfListSchema == null) {
								bnfListSchema = new WFBnfListSchema();
							}
							// String toInsuNo = null;
							String toInsuRela = null;
							String bnfType = null;
							String bnfGrade = null;
							String bnfRate = null;
							String name = null;
							String sex = null;
							String birthday = null;
							String IDType = null;
							String IDNo = null;

							Iterator child1 = child_element.getChildren();
							boolean bnfLoadFlag = true;
							int seq = 1;
							int sum[] = new int[6];
                            ArrayList array = new ArrayList();
							while (child1.hasNext()) {
								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									// item下没有节点
									if (!child2.hasNext()) {
										bnfLoadFlag = false;
									}
									while (child2.hasNext()) {

										OMElement child2OME = (OMElement) child2
												.next();
										String child2OMELocalName = child2OME
												.getLocalName();

										if (child2OMELocalName
												.equals("TOINSUNO")) {
											toInsuNo = child2OME.getText();
											System.out.println("toInsuNo "
													+ toInsuNo);
										}
										if (child2OMELocalName
												.equals("TOINSURELA")) {
											toInsuRela = child2OME.getText();
											System.out.println("toInsuRela "
													+ toInsuRela);
										}
										if (child2OMELocalName
												.equals("BNFTYPE")) {
											bnfType = child2OME.getText();
											System.out.println("bnfType "
													+ bnfType);
										}
										if (child2OMELocalName
												.equals("BNFGRADE")) {
											bnfGrade = child2OME.getText();
											System.out.println(bnfGrade);
										}
										if (child2OMELocalName
												.equals("BNFRATE")) {
											bnfRate = child2OME.getText();
											System.out.println(bnfRate);
										}
										if (child2OMELocalName.equals("NAME")) {
											name = child2OME.getText();
											if (name == null || "".equals(name)) {
												bnfLoadFlag = false;
											}
											System.out.println(name);
										}
										if (child2OMELocalName.equals("SEX")) {
											sex = child2OME.getText();
											if (sex == null || "".equals(sex)) {
												bnfLoadFlag = false;
											}
											System.out.println(sex);
										}
										if (child2OMELocalName
												.equals("BIRTHDAY")) {
											birthday = child2OME.getText();
											if (birthday == null
													|| "".equals(birthday)) {
												bnfLoadFlag = false;
											}
											System.out.println(birthday);
										}
										if (child2OMELocalName.equals("IDTYPE")) {
											IDType = mTool.codeIDType(child2OME
													.getText());
											if (IDType == null
													|| "".equals(IDType)) {
												bnfLoadFlag = false;
											}
											System.out.println(IDType+":"+IDType);
										}
										if (child2OMELocalName.equals("IDNO")) {
											IDNo = child2OME.getText();
											if (IDNo == null || "".equals(IDNo)) {
												bnfLoadFlag = false;
											}
											System.out.println(IDNo);
										}

									
									}
								}

								// 不为空

								bnfListSchema.setBatchNo(batchno);
								bnfListSchema.setCardNo(cardNo);
								bnfListSchema.setToInsuNo(toInsuNo);

								bnfListSchema = new WFBnfListSchema();
								// 不为空
								bnfListSchema.setBatchNo(batchno);
								bnfListSchema.setCardNo(cardNo);

								bnfListSchema.setToInsuNo(toInsuNo);
								bnfListSchema.setToInsuRela(toInsuRela);
								bnfListSchema.setBnfType(bnfType);
								bnfListSchema.setBnfGrade(bnfGrade);
								bnfListSchema.setBnfRate(Double.parseDouble(bnfRate)/100);
								int n = Integer.parseInt(bnfGrade);
								if(array.contains(bnfGrade)){
                                	seq++;
                                	bnfListSchema.setBnfGradeNo(String.valueOf(seq));
                                	sum[n] = (int)(sum[n] + Integer.parseInt(bnfRate));
                            	}else{
                            		bnfListSchema.setBnfGradeNo(String.valueOf(seq));
                            		array.add(bnfGrade);
                            		sum[n] = (int) Integer.parseInt(bnfRate);
                            	}
								bnfListSchema.setName(name);
								bnfListSchema.setSex(mTool.codeSex(sex));
								bnfListSchema.setBirthday(birthday);
								bnfListSchema.setIDType(IDType);
								bnfListSchema.setIDNo(IDNo);

								bnfListSet.add(bnfListSchema);

							}
							int length = sum.length;
							for(int i=1;i<length;i++){
								if(sum[i]==0)continue;
								if(sum[i]!=100){
									responseOME = buildResponseOME("01", "5",
											"受益顺位"+i+"的受益总和不等于100!");
									System.out.println("responseOME:" + responseOME);
		
									LoginVerifyTool.writeLog(responseOME, batchno,
											messageType, sendDate, sendTime, "01",
											"100", "5", "受益顺位"+i+"的受益总和不等于100!");
									return responseOME;
	                        	}
							}
							if (bnfLoadFlag) {
								map.put(bnfListSet, "INSERT");
							}

							// map.put(bnfListSchema, "INSERT");
						}
					}
					// 日志3
					LoginVerifyTool.writeBasicInfoLog(cardNo, appntListSchema
							.getAppntNo(), insuListSchema.getInsuNo());
					// 保险卡信息封装

					tContListSchema.setBatchNo(batchno);

					tContListSchema.setSendDate(PubFun.getCurrentDate());
					tContListSchema.setSendTime(PubFun.getCurrentTime());

					tContListSchema.setCardNo(cardNo);

					// 不为空

					map.put(tContListSchema, "INSERT");
				}

			}

			// 保存数据
			WFContListSchema liWFContListSchema = tContListSchema;
			WFInsuListSchema liWFInsuListSchema = insuListSet.get(1);

			// 判断是否连带
			boolean externalActiveFlag = false;
			LICardActModifySchema cardActModifySchema;
			// if ("00".equals(relationCode)) {
			// 非连带

			// 被保人信息

			ExternalActive tExternalActive = new ExternalActive();
			// cardActModifySchema = tExternalActive.writeLICardActModify(
			// liWFContListSchema, liWFInsuListSchema);
			LICardActiveInfoListSet exCardActiveInfoListSet = tExternalActive
					.writeLICardActiveInfoList(liWFContListSchema,
							insuListSet);
			liWFContListSchema = null;
			liWFInsuListSchema = null;
			// } else {
			// responseOME = buildResponseOME("01", "22", "被保人为连带，无法提交！");
			// System.out.println("responseOME:" + responseOME);
			// // 日志4
			// LoginVerifyTool.writeLog(responseOME, batchno, messageType,
			// sendDate, sendTime, "01", "100", "22", "被保人为连带，无法提交！");
			// return responseOME;
			// }

			map.put(exCardActiveInfoListSet, "DELETE&INSERT");
			// 入库
			PubSubmit tPubSubmit = new PubSubmit();
			tVData.add(map);
			if (!tPubSubmit.submitData(tVData, "INSERT")) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "CardActiveInfo";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				responseOME = buildResponseOME("01", "6", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", sendOperator, "6", "数据提交失败");
				return responseOME;
			} else {
				responseOME = buildResponseOME("00", "0", "成功");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "00", sendOperator, "0", "成功");
			}

		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", sendOperator, "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("HYXENDOR", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
