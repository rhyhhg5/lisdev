package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;


import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.db.WFInsuListDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.WFContListSchema;
//import com.sinosoft.lis.schema.WFInsuListSchema;
//import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

//import com.sinosoft.lis.pubfun.FDate;

/**
 * Title:远程出单当日撤单
 * 
 * @author yuanzw 创建时间：2010-2-3 下午04:33:16 Description:
 * @version
 */

public class GJCD {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String cardNo = null;// 单证号

	private String effectDate = null;// 生效日期

	private String revocationDate = null;// 撤单日期

	private String effectOnlyDate;

	private String effectOnlyTime;

	MMap map = new MMap();

	VData tVData = new VData();

	// 错误处理类
	public CErrors mErrors = new CErrors();

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {
			LoginVerifyTool mTool = new LoginVerifyTool();
			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("GJCD")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
							boolean validBatchNo = mTool
									.isValidBatchNo(batchno);
							if (!validBatchNo) {
								// 批次号不唯一
								responseOME = buildResponseOME("01", "21",
										"批次号不唯一!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "21", "批次号不唯一!");
								return responseOME;
							}
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);
							if (!"02".equals(messageType)) {
								responseOME = buildResponseOME("01", "5",
										"报文类型不正确!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "5", "报文类型不正确!");
								return responseOME;
							}
						}

						if (child_element.getLocalName().equals("CARDNO")) {
							cardNo = child_element.getText();

							System.out.println(cardNo);
						}

						if (child_element.getLocalName().equals("EFFECTDATE")) {
							effectDate = child_element.getText();

							System.out.println(effectDate);
							System.out.println("撤单生效时间字符串长度 "
									+ effectDate.length());
							if (effectDate.length() == 19) {
								effectOnlyDate = effectDate.substring(0, 10);// 第10个不截取
								System.out.println("撤单生效日期" + effectOnlyDate);
								effectOnlyTime = effectDate.substring(11, 19);
								System.out.println("撤单生效日期和时间" + effectOnlyDate
										+ effectOnlyTime);
							}

						}
						if (child_element.getLocalName().equals(
								"REVOCATIONDATE")) {
							revocationDate = child_element.getText();

							System.out.println(revocationDate);
						}

					}
					// 日志3
					LoginVerifyTool.writeLoginLog(cardNo, "无");

					if (!"02".equals(messageType)) {
						responseOME = buildResponseOME("01", "5", "报文类型不正确");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"5", "报文类型不正确");
						return responseOME;
					}
					if (cardNo == null || "".equals(cardNo)) {
						responseOME = buildResponseOME("01", "1", "卡单号码为空值");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"1", "卡单号码为空值");
						return responseOME;
					}

					// 保险卡信息查询
					WFContListDB contListDB = new WFContListDB();
					contListDB.setCardNo(cardNo);
					WFContListSchema contListSchema;
					contListSchema = contListDB.query().get(1);
					if (null != contListSchema) {
						contListSchema.setBatchNo(batchno);

						contListSchema.setSendDate(PubFun.getCurrentDate());
						contListSchema.setSendTime(PubFun.getCurrentTime());
						contListSchema.setMessageType(messageType);
						contListSchema.setActiveDate(revocationDate);// 撤单日期

						contListSchema.setCvaliDate(effectOnlyDate);
						contListSchema.setCvaliDateTime(effectOnlyTime);

						map.put(contListSchema, "INSERT");

					} else {
						responseOME = buildResponseOME("01", "11", "撤单失败!无该卡号");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"11", "撤单失败!无该卡号");
						return responseOME;
					}
//					校验撤单时该卡已生效
					String CvalidateSql = "select cvalidate,makedate from licardactiveinfolist where cardno = '"+cardNo+"'";
					SSRS licSSRS = new ExeSQL().execSQL(CvalidateSql);
					if(licSSRS == null || licSSRS.MaxRow<=0){
						responseOME = buildResponseOME("01", "11", "该卡出单及生效日期获取失败！");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"11", "该卡出单及生效日期获取失败！");
						return responseOME;
					}
					String tCvalidate = licSSRS.GetText(1, 1);//生效日
					if(tCvalidate == null || "".equals(tCvalidate)){
						responseOME = buildResponseOME("01", "11", "该卡生效日期不正确！");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"11", "该卡生效日期不正确");
						return responseOME;
					}
					String tMakeDate = licSSRS.GetText(1, 2);//出单日
					if(tMakeDate == null || "".equals(tMakeDate)){
						responseOME = buildResponseOME("01", "11", "该卡出单日期不正确！");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"11", "该卡出单日期不正确");
						return responseOME;
					}
//					
//					FDate tFDate = new FDate();
//					if((tFDate.getDate(sendDate).compareTo(tFDate.getDate(tMakeDate)) != 0 
//							|| (tFDate.getDate(sendDate).compareTo(tFDate.getDate(tCvalidate)) != 0))
//							&& tFDate.getDate(tCvalidate).compareTo(tFDate.getDate(sendDate))<=0){
//						responseOME = buildResponseOME("01", "12", "该卡已生效，不可以撤单！");
//						System.out.println("responseOME:" + responseOME);
//						LoginVerifyTool.writeLog(responseOME, batchno,
//								messageType, sendDate, sendTime, "01", "100",
//								"12", "该卡已生效，不可以撤单！");
//						return responseOME;
//					}
					
//					校验是否已进入结算表
					String certifySql = "select * from licertify where cardno = '"+cardNo+"'";
					SSRS certifySSRS= new ExeSQL().execSQL(certifySql);
					if(certifySSRS != null && certifySSRS.MaxRow>0){
						responseOME = buildResponseOME("01", "13", "该卡已进入结算状态，不可以撤单！");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"12", "该卡已进入结算状态，不可以撤单！");
						return responseOME;
					}

					// 查询被保人信息
					WFInsuListDB insuListDB = new WFInsuListDB();
					insuListDB.setCardNo(cardNo);
//					WFInsuListSet insuListSet;
					// WFInsuListSchema insuListSchema;
//					insuListSet = insuListDB.query();

					// 判断是否连带
					// if ("00".equals(relationCode)) {
					// // 非连带
					//					
					// // 被保人信息
					// WFInsuListSchema exInsuListSchema = insuListSet.get(1);
					// ExternalActive tExternalActive = new ExternalActive();
					// tExternalActive.writeLICardActiveInfoList(contListSchema,
					// insuListSchema);
					// }

					// 保存数据
//					WFContListSchema liWFContListSchema = contListSchema;
					// WFInsuListSchema liWFInsuListSchema = insuListSchema;
					// 入库
					PubSubmit tPubSubmit = new PubSubmit();
					tVData.add(map);
					if (!tPubSubmit.submitData(tVData, "INSERT")) {
						// @@错误处理
						this.mErrors.copyAllErrors(tPubSubmit.mErrors);
						CError tError = new CError();
						tError.moduleName = "GJCDXTb";
						tError.functionName = "submitData";
						tError.errorMessage = "数据提交失败!";
						this.mErrors.addOneError(tError);
						responseOME = buildResponseOME("01", "6", "数据提交失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"6", "数据提交失败");
						return responseOME;
					}
					// 写入外部激活信息表

					ExternalActive tExternalActive = new ExternalActive();
					boolean externalActiveFlag = tExternalActive
							.cancelCont(cardNo);
//					liWFContListSchema = null;
					// liWFInsuListSchema = null;
					if (externalActiveFlag) {
						responseOME = buildResponseOME("00", "0", "成功");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "00", "100",
								"0", "成功");
					} else {
						responseOME = buildResponseOME("01", "1", "数据提交失败");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"1", "数据提交失败");
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "100", "4", "发生异常");
			return responseOME;
		}

		return responseOME;

	}

	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("GJCD", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
