package com.ecwebservice.subinterface;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LICardActModifySchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActModifySet;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-2-26 下午03:19:33
 * @Description:写入卡单外部激活信息表，卡单外部激活信息增量表
 * @version
 */

public class ExternalActive {

	/**
	 * @param args
	 */
	// MMap map = new MMap();
	//
	// VData tVData = new VData();
	// 错误处理类
	private LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
	public CErrors mErrors = new CErrors();
	private double tSysWrapSumPrem = 0; //系统计算出的保费
    private double tSysWrapSumAmnt = 0;//系统计算出的保额

	public LICardActiveInfoListSet writeLICardActiveInfoList(
			WFContListSchema contListSchema, WFInsuListSet tInsuListSet,String ipaddr) {

		// 写入卡单外部激活信息表 LICardActiveInfoList
		LICardActiveInfoListSet cardActiveInfoListSet = new LICardActiveInfoListSet();

		if (null != contListSchema && null != tInsuListSet) {
			
//			对所有第三方数据进行校验
			CommonCheck tCommonCheck = new CommonCheck(contListSchema);
			if(!tCommonCheck.checkAll(tInsuListSet)){
				System.out.println("生成中间表数据前校验出错："+tCommonCheck.getMErrors().getErrContent());
				mErrors = tCommonCheck.getMErrors();
				return null;
			}else{
				tSysWrapSumPrem = tCommonCheck.getTSysWrapSumPrem();
				tSysWrapSumAmnt = tCommonCheck.getTSysWrapSumAmnt();
			}
			String currentDate = PubFun.getCurrentDate();
			String currentTime = PubFun.getCurrentTime();

			// 判断激活还是撤单
			// "02"为撤单
			// 更新LICardActiveInfoList表中的CardStatus 02-退保
			if (contListSchema.getMessageType().equals("02")) {
				String tbCardNo = contListSchema.getCardNo();
				String sql = "update LICardActiveInfoList set CardStatus='02',modifydate='"
						+ currentDate
						+ "',modifytime='"
						+ currentTime
						+ "' where CardNo='" + tbCardNo + "'";
				ExeSQL tExeSQL = new ExeSQL();
				boolean updateFlag = tExeSQL.execUpdateSQL(sql);
				// if (updateFlag) {
				// return true;
				// } else {
				// return false;
				// }

			}

			WFInsuListSchema insuListSchema;
			for (int i = 1; i <= tInsuListSet.size(); i++) {
				LICardActiveInfoListSchema cardActiveInfoListSchema = new LICardActiveInfoListSchema();
				insuListSchema = tInsuListSet.get(i);

				cardActiveInfoListSchema.setDealFlag("00");
				cardActiveInfoListSchema.setState("00");
				cardActiveInfoListSchema.setCardStatus("01");

				cardActiveInfoListSchema.setCardNo(contListSchema.getCardNo());
				// 被保人流水号
				cardActiveInfoListSchema.setSequenceNo(insuListSchema
						.getInsuNo());
				// 卡类型
				cardActiveInfoListSchema.setCardType(contListSchema
						.getRiskCode());
				cardActiveInfoListSchema.setActiveDate(contListSchema
						.getActiveDate());
				cardActiveInfoListSchema.setCValidate(contListSchema
						.getCvaliDate());
				
//				String IP = BaseService.getClientIp();
				cardActiveInfoListSchema.setActiveIP(ipaddr);

				cardActiveInfoListSchema.setName(insuListSchema.getName());
				cardActiveInfoListSchema.setSex(insuListSchema.getSex());
				cardActiveInfoListSchema.setBirthday(insuListSchema
						.getBirthday());
				cardActiveInfoListSchema.setIdType(insuListSchema.getIDType());
				cardActiveInfoListSchema.setIdNo(insuListSchema.getIDNo());
				cardActiveInfoListSchema.setOccupationType(insuListSchema
						.getOccupationType());
				cardActiveInfoListSchema.setOccupationCode(insuListSchema
						.getOccupationCode());
				cardActiveInfoListSchema.setPostalAddress(insuListSchema
						.getPostalAddress());
				cardActiveInfoListSchema
						.setZipCode(insuListSchema.getZipCode());
				cardActiveInfoListSchema.setPhone(insuListSchema.getPhont());
				cardActiveInfoListSchema.setMobile(insuListSchema.getMobile());
				cardActiveInfoListSchema
						.setGrpName(insuListSchema.getGrpName());
				cardActiveInfoListSchema.setCompanyPhone(insuListSchema
						.getCompanyPhone());
				cardActiveInfoListSchema.setEMail(insuListSchema.getEmail());

				cardActiveInfoListSchema.setOperator(contListSchema
						.getSendOperator());// 卡单激活未提供该字段假数据"100"

				cardActiveInfoListSchema.setMakeDate(currentDate);
				cardActiveInfoListSchema.setMakeTime(currentTime);
				cardActiveInfoListSchema.setModifyDate(currentDate);
				cardActiveInfoListSchema.setModifyTime(currentTime);
				cardActiveInfoListSchema.setPrem(String.valueOf(contListSchema.getPrem()));
				cardActiveInfoListSchema.setAmnt(String.valueOf(contListSchema.getAmnt()));
//				调整为存储核心计算的保额保费
//				cardActiveInfoListSchema.setPrem(String.valueOf(tSysWrapSumPrem));
//				cardActiveInfoListSchema.setAmnt(String.valueOf(tSysWrapSumAmnt));
				cardActiveInfoListSchema.setMult(String.valueOf(contListSchema
						.getMult()));
//				if (contListSchema.getCopys() > 0) {
//					cardActiveInfoListSchema.setCopys("1");
//				}
				cardActiveInfoListSchema.setCopys(String.valueOf(contListSchema.getCopys()));
				cardActiveInfoListSchema.setTicketNo(contListSchema
						.getTicketNo());
				cardActiveInfoListSchema.setTeamNo(contListSchema.getTeamNo());
				cardActiveInfoListSchema.setSeatNo(contListSchema.getSeatNo());
				cardActiveInfoListSchema.setFrom(contListSchema.getFrom());
				cardActiveInfoListSchema.setTo(contListSchema.getTo());

				cardActiveInfoListSchema.setMult(String.valueOf(contListSchema
						.getMult()));
				// 将生效日期及生效时间 电子商务传入的remark
				// 拼串写入cardActiveInfoListSchema的remark字段

				cardActiveInfoListSchema.setRemark(contListSchema
						.getCvaliDate()
						+ " "
						+ contListSchema.getCvaliDateTime()
						+ "     "
						+ contListSchema.getRemark());
				
				String[] aInActiveDate= tLoginVerifyTool.getMyProductInfo(contListSchema.getCardNo(), contListSchema.getCvaliDate(),String.valueOf(contListSchema.getMult()));
				if(aInActiveDate[2] != null||aInActiveDate[2] != ""){
					cardActiveInfoListSchema.setInActiveDate(aInActiveDate[2]);
				}				

				cardActiveInfoListSet.add(cardActiveInfoListSchema);

				// tInsuListSet.remove(insuListSchema);
			}

			// map.put(cardActiveInfoListSet, "INSERT");

			// 入库
			// PubSubmit tPubSubmit = new PubSubmit();
			// tVData.add(map);
			// if (!tPubSubmit.submitData(tVData, "INSERT")) {
			// // @@错误处理
			// this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			// CError tError = new CError();
			// tError.moduleName = "ExternalActive";
			// tError.functionName = "writeLICardActiveInfoList";
			// tError.errorMessage = "数据提交失败!";
			// this.mErrors.addOneError(tError);
			// return false;
			// } else {
			// return true;
			// }
		}
		return cardActiveInfoListSet;
	}
	public LICardActiveInfoListSet writeLICardActiveInfoList(
			WFContListSchema contListSchema, WFInsuListSet tInsuListSet) {

		// 写入卡单外部激活信息表 LICardActiveInfoList
		LICardActiveInfoListSet cardActiveInfoListSet = new LICardActiveInfoListSet();

		if (null != contListSchema && null != tInsuListSet) {
			
//			对所有第三方数据进行校验
			
			CommonCheck tCommonCheck = new CommonCheck(contListSchema);
			System.out.println("\n\n\n\n走到ExternalActive.java Line 202\n\n\n\n");
			if(!tCommonCheck.checkAll(tInsuListSet)){
				System.out.println("生成中间表数据前校验出错："+tCommonCheck.getMErrors().getErrContent());
				mErrors = tCommonCheck.getMErrors();
				return null;
			}else{
				tSysWrapSumPrem = tCommonCheck.getTSysWrapSumPrem();
				tSysWrapSumAmnt = tCommonCheck.getTSysWrapSumAmnt();
			}
			String currentDate = PubFun.getCurrentDate();
			String currentTime = PubFun.getCurrentTime();

			// 判断激活还是撤单
			// "02"为撤单
			// 更新LICardActiveInfoList表中的CardStatus 02-退保
			if (contListSchema.getMessageType().equals("02")) {
				String tbCardNo = contListSchema.getCardNo();
				String sql = "update LICardActiveInfoList set CardStatus='02',modifydate='"
						+ currentDate
						+ "',modifytime='"
						+ currentTime
						+ "' where CardNo='" + tbCardNo + "'";
				ExeSQL tExeSQL = new ExeSQL();
				boolean updateFlag = tExeSQL.execUpdateSQL(sql);
				// if (updateFlag) {
				// return true;
				// } else {
				// return false;
				// }

			}

			WFInsuListSchema insuListSchema;
			for (int i = 1; i <= tInsuListSet.size(); i++) {
				LICardActiveInfoListSchema cardActiveInfoListSchema = new LICardActiveInfoListSchema();
				insuListSchema = tInsuListSet.get(i);

				cardActiveInfoListSchema.setDealFlag("00");
				cardActiveInfoListSchema.setState("00");
				cardActiveInfoListSchema.setCardStatus("01");

				cardActiveInfoListSchema.setCardNo(contListSchema.getCardNo());
				// 被保人流水号
				cardActiveInfoListSchema.setSequenceNo(insuListSchema
						.getInsuNo());
				// 卡类型
				cardActiveInfoListSchema.setCardType(contListSchema
						.getRiskCode());
				cardActiveInfoListSchema.setActiveDate(contListSchema
						.getActiveDate());
				cardActiveInfoListSchema.setCValidate(contListSchema
						.getCvaliDate());
				
				String IP = BaseService.getClientIp();
				cardActiveInfoListSchema.setActiveIP(IP);

				cardActiveInfoListSchema.setName(insuListSchema.getName());
				cardActiveInfoListSchema.setSex(insuListSchema.getSex());
				cardActiveInfoListSchema.setBirthday(insuListSchema
						.getBirthday());
				cardActiveInfoListSchema.setIdType(insuListSchema.getIDType());
				cardActiveInfoListSchema.setIdNo(insuListSchema.getIDNo());
				cardActiveInfoListSchema.setOccupationType(insuListSchema
						.getOccupationType());
				cardActiveInfoListSchema.setOccupationCode(insuListSchema
						.getOccupationCode());
				cardActiveInfoListSchema.setPostalAddress(insuListSchema
						.getPostalAddress());
				cardActiveInfoListSchema
						.setZipCode(insuListSchema.getZipCode());
				cardActiveInfoListSchema.setPhone(insuListSchema.getPhont());
				cardActiveInfoListSchema.setMobile(insuListSchema.getMobile());
				cardActiveInfoListSchema
						.setGrpName(insuListSchema.getGrpName());
				cardActiveInfoListSchema.setCompanyPhone(insuListSchema
						.getCompanyPhone());
				cardActiveInfoListSchema.setEMail(insuListSchema.getEmail());

				cardActiveInfoListSchema.setOperator(contListSchema
						.getSendOperator());// 卡单激活未提供该字段假数据"100"

				cardActiveInfoListSchema.setMakeDate(currentDate);
				cardActiveInfoListSchema.setMakeTime(currentTime);
				cardActiveInfoListSchema.setModifyDate(currentDate);
				cardActiveInfoListSchema.setModifyTime(currentTime);
				cardActiveInfoListSchema.setPrem(String.valueOf(contListSchema.getPrem()));
				cardActiveInfoListSchema.setAmnt(String.valueOf(contListSchema.getAmnt()));
//				调整为存储核心计算的保额保费
//				cardActiveInfoListSchema.setPrem(String.valueOf(tSysWrapSumPrem));
//				cardActiveInfoListSchema.setAmnt(String.valueOf(tSysWrapSumAmnt));
				cardActiveInfoListSchema.setMult(String.valueOf(contListSchema
						.getMult()));
//				if (contListSchema.getCopys() > 0) {
//					cardActiveInfoListSchema.setCopys("1");
//				}
				cardActiveInfoListSchema.setCopys(String.valueOf(contListSchema.getCopys()));
				cardActiveInfoListSchema.setTicketNo(contListSchema
						.getTicketNo());
				cardActiveInfoListSchema.setTeamNo(contListSchema.getTeamNo());
				cardActiveInfoListSchema.setSeatNo(contListSchema.getSeatNo());
				cardActiveInfoListSchema.setFrom(contListSchema.getFrom());
				cardActiveInfoListSchema.setTo(contListSchema.getTo());

				cardActiveInfoListSchema.setMult(String.valueOf(contListSchema
						.getMult()));
				// 将生效日期及生效时间 电子商务传入的remark
				// 拼串写入cardActiveInfoListSchema的remark字段

				cardActiveInfoListSchema.setRemark(contListSchema
						.getCvaliDate()
						+ " "
						+ contListSchema.getCvaliDateTime()
						+ "     "
						+ contListSchema.getRemark());
	            String[] riskRelateCode = tLoginVerifyTool.getRiskSetCodeByCardNo(contListSchema.getCardNo());// 包含套餐编码及certifyCode
	    		String riskSetCode = riskRelateCode[0];
	    		String[] insuYearInfoArr = tLoginVerifyTool.getInsuInfoFromCore(contListSchema.getCardNo(), riskSetCode,String.valueOf(contListSchema
						.getMult()));
	    		cardActiveInfoListSchema.setInsuYear(insuYearInfoArr[0]);
	    		cardActiveInfoListSchema.setInsuYearFlag(insuYearInfoArr[1]);
				String[] aInActiveDate= tLoginVerifyTool.getMyProductInfo(contListSchema.getCardNo(), contListSchema.getCvaliDate(),String.valueOf(contListSchema.getMult()));
				if(aInActiveDate[2] != null||aInActiveDate[2] != ""){
					cardActiveInfoListSchema.setInActiveDate(aInActiveDate[2]);
				}				

				cardActiveInfoListSet.add(cardActiveInfoListSchema);

				// tInsuListSet.remove(insuListSchema);
			}

			// map.put(cardActiveInfoListSet, "INSERT");

			// 入库
			// PubSubmit tPubSubmit = new PubSubmit();
			// tVData.add(map);
			// if (!tPubSubmit.submitData(tVData, "INSERT")) {
			// // @@错误处理
			// this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			// CError tError = new CError();
			// tError.moduleName = "ExternalActive";
			// tError.functionName = "writeLICardActiveInfoList";
			// tError.errorMessage = "数据提交失败!";
			// this.mErrors.addOneError(tError);
			// return false;
			// } else {
			// return true;
			// }
		}
		return cardActiveInfoListSet;
	}

	public LICardActModifySchema writeLICardActModify(
			WFContListSchema contListSchema, WFInsuListSchema insuListSchema) {
		LICardActModifySchema cardActModifySchema = new LICardActModifySchema();
		// 写入卡单外部激活信息增量表 LICardActModifySchema
		if (null != contListSchema && null != insuListSchema) {
			String currentDate = PubFun.getCurrentDate();
			String currentTime = PubFun.getCurrentTime();

			cardActModifySchema.setCardNo(contListSchema.getCardNo());
			// 被保人流水号
			cardActModifySchema.setSequenceNo(insuListSchema.getInsuNo());
			// 卡类型
			cardActModifySchema.setCardType(contListSchema.getRiskCode());
			cardActModifySchema.setActiveDate(contListSchema.getActiveDate());
			cardActModifySchema.setCValidate(contListSchema.getCvaliDate());
			String IP = BaseService.getClientIp();
			cardActModifySchema.setActiveIP(IP);
			cardActModifySchema.setDealFlag("00");

			cardActModifySchema.setName(insuListSchema.getName());
			cardActModifySchema.setSex(insuListSchema.getSex());
			cardActModifySchema.setBirthday(insuListSchema.getBirthday());
			cardActModifySchema.setIdType(insuListSchema.getIDType());
			cardActModifySchema.setIdNo(insuListSchema.getIDNo());
			cardActModifySchema.setOccupationType(insuListSchema
					.getOccupationType());
			cardActModifySchema.setOccupationCode(insuListSchema
					.getOccupationCode());
			cardActModifySchema.setPostalAddress(insuListSchema
					.getPostalAddress());
			cardActModifySchema.setZipCode(insuListSchema.getZipCode());
			cardActModifySchema.setPhone(insuListSchema.getPhont());
			cardActModifySchema.setMobile(insuListSchema.getMobile());
			cardActModifySchema.setGrpName(insuListSchema.getGrpName());
			cardActModifySchema.setCompanyPhone(insuListSchema
					.getCompanyPhone());
			cardActModifySchema.setEMail(insuListSchema.getEmail());

			cardActModifySchema.setState("00");
			cardActModifySchema.setOperator(contListSchema.getSendOperator());

			cardActModifySchema.setMakeDate(currentDate);
			cardActModifySchema.setMakeTime(currentTime);
			cardActModifySchema.setModifyDate(currentDate);
			cardActModifySchema.setModifyTime(currentTime);
			cardActModifySchema.setPrem(String
					.valueOf(contListSchema.getPrem()));
			cardActModifySchema.setAmnt(String
					.valueOf(contListSchema.getAmnt()));
			cardActModifySchema.setMult(String
					.valueOf(contListSchema.getMult()));
//			if (contListSchema.getCopys() > 0) {
//				cardActModifySchema.setCopys("1");
//			}
			cardActModifySchema.setCopys(String.valueOf(contListSchema.getCopys()));
			cardActModifySchema.setTicketNo(contListSchema.getTicketNo());
			cardActModifySchema.setTeamNo(contListSchema.getTeamNo());
			cardActModifySchema.setSeatNo(contListSchema.getSeatNo());
			cardActModifySchema.setFrom(contListSchema.getFrom());
			cardActModifySchema.setTo(contListSchema.getTo());
			cardActModifySchema.setRemark(contListSchema.getRemark());
			cardActModifySchema.setCardStatus("01");

			cardActModifySchema.setWebModifyDate(contListSchema.getSendDate());

		}
		return cardActModifySchema;
	}

	public boolean cancelCont(String cardNo) {
		String currentDate = PubFun.getCurrentDate();
		String currentTime = PubFun.getCurrentTime();
		String sql = "update LICardActiveInfoList set CardStatus='03',modifydate='"
				+ currentDate
				+ "',modifytime='"
				+ currentTime
				+ "' where CardNo='" + cardNo + "'";
		ExeSQL tExeSQL = new ExeSQL();
		boolean updateFlag = tExeSQL.execUpdateSQL(sql);
		if (updateFlag) {
			return true;
		} else {
			return false;
		}
	}

	// 更新lzcard 中的状态 将state 改为14
	public String[] getUpdateLzcardSql(String cardNo) {
		String[] updateArr = new String[2];
		String modifyStateSql = null;
		String currentDate = PubFun.getCurrentDate();
		String currentTime = PubFun.getCurrentTime();
		String lzcardPKSql = " select  lzc.CertifyCode, lzc.SubCode, lzc.riskcode,lzc.RiskVersion ,lzc.StartNo,lzc.endno,lzc.state "
				+ "from LZCardNumber lzcn inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
				+ "and lzc.EndNo = lzcn.CardSerNo where 1 = 1 	and lzcn.CardNo like '"
				+ cardNo + "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS lzcardPKSSRS = tExeSQL.execSQL(lzcardPKSql);
		if (lzcardPKSSRS.MaxRow > 0) {
			String CertifyCode = lzcardPKSSRS.GetText(1, 1);
			String SubCode = lzcardPKSSRS.GetText(1, 2);
			String RiskCode = lzcardPKSSRS.GetText(1, 3);
			String RiskVersion = lzcardPKSSRS.GetText(1, 4);
			String StartNo = lzcardPKSSRS.GetText(1, 5);
			String EndNo = lzcardPKSSRS.GetText(1, 6);
			modifyStateSql = "update lzcard set state='14',MODIFYDATE='" + currentDate + "',MODIFYTIME='" + currentTime + "' where CertifyCode='"
					+ CertifyCode + "' " + "and SubCode='" + SubCode
					+ "' and RiskCode='" + RiskCode + "' and RiskVersion='"
					+ RiskVersion + "' " + "and StartNo='" + StartNo
					+ "' and EndNo='" + EndNo + "'";
			updateArr[0] = modifyStateSql;
			updateArr[1] = lzcardPKSSRS.GetText(1, 7);
		}
		return updateArr;
	}

	public static void main(String[] args) {

	}
	/**
	 * @return the mErrors
	 */
	public CErrors getMErrors() {
		return mErrors;
	}
	/**
	 * @param errors the mErrors to set
	 */
	public void setMErrors(CErrors errors) {
		mErrors = errors;
	}

}
