package com.ecwebservice.subinterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;

public class CardActiveWT {

	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
	
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	
	private String branchcode = "";// 网点
	
	private String sendoperator = "";// 操作员

	private String messageType = "";// 报文类型

	private OMElement responseOME = null;
	
	private static final String cardNo 		 = 	"cardno";        
	private static final String cardpassword =  "password";  
	private static final String riskcode	 = 	"wrapcode";        
	private static final String riskname	 = 	"wrapname";        
	private static final String certifycode	 = 	"certifycode";     
	private static final String certifyname  = 	"certifyname";     
	
	private Map recmap = null;
	private List reclist = new ArrayList();
	private MMap tMMap = new MMap();
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildCardActiveResponseOME("01","", "加载激活信息失败");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			return responseOME;
		}
		if(!deal()){
			return responseOME;
		}
		if(!submit()){
			return responseOME;
		}
		return responseOME;
	}
	
	//加载卡激活信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("MYCERTACTWT")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDDATE")){
							this.sendDate=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDTIME")){
							this.sendTime= child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("BRANCHCODE")){
							this.branchcode=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDOPERATOR")){
							this.sendoperator=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("MESSAGETYPE")){
							this.messageType=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("CONTLIST")){
							Iterator childItem = child_element.getChildren();
							while(childItem.hasNext()){
								OMElement childOME = (OMElement) childItem.next();
								if(childOME.getLocalName().equals("ITEM")){
									recmap = new HashMap();
									Iterator child1OME = childOME.getChildren();
									while(child1OME.hasNext()){
										OMElement childleaf = (OMElement) child1OME.next();
										if(childleaf.getLocalName().equals("CARDNO")){
											this.recmap.put(this.cardNo, childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PASSWORD")){
											this.recmap.put(this.cardpassword, childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("WRAPCODE")){
											this.recmap.put(this.riskcode, childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("WRAPNAME")){
											this.recmap.put(this.riskname, childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CERTIFYCODE")){
											this.recmap.put(this.certifycode, childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CERTIFYNAME")){
											this.recmap.put(this.certifyname, childleaf.getText());
											continue;
										}
									}
								this.reclist.add(recmap);
								}
							}
						}
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean check(){
		if(this.reclist.size()==0){
			responseOME = buildCardActiveResponseOME("01","", "没有得到退保信息");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if(this.messageType ==null || this.messageType.equals("")|| !this.messageType.equals("02")){
			responseOME = buildCardActiveResponseOME("01","", "业务类型有误");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		for(int i=0;i<reclist.size();i++){
			recmap = new HashMap();
			recmap =(Map)reclist.get(i);
			if(recmap.get(this.cardNo).equals("") || recmap.get(this.cardNo)==null){
				responseOME = buildCardActiveResponseOME("01","", "没有得到卡号");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(recmap.get(this.cardpassword).equals("") || recmap.get(this.cardpassword)==null){
				responseOME = buildCardActiveResponseOME("01","", "没有得到卡密码");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(recmap.get(this.riskcode).equals("") || recmap.get(this.riskcode)==null){
				responseOME = buildCardActiveResponseOME("01","", "没有得到套餐代码");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(recmap.get(this.certifycode).equals("") || recmap.get(this.certifycode)==null){
				responseOME = buildCardActiveResponseOME("01","", "没有得到单证编码");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			
			try{
				String sql="select 1 from LICardActiveInfoList lica "
							  +"inner join wfcontlist wfc on lica.cardno= wfc.cardno "
							  +"inner join lzcardnumber lzcn on lzcn.cardno = lica.cardno "
							  +"inner join lmcertifydes lmcd on lzcn.cardtype =lmcd.subcode " 
							  +"inner join lmcardrisk lmcr on lmcd.certifycode = lmcr.certifycode "
							  +"where lica.cardno='"+recmap.get(this.cardNo)+"' " 
									  +"and wfc.password='"+recmap.get(this.cardpassword)+"' " 
									  +"and lmcd.certifycode='"+recmap.get(this.certifycode)+"'"
									  +"and lmcr.riskcode='"+recmap.get(this.riskcode)+"'";
				String checkRes = new ExeSQL().getOneValue(sql);
				if(!checkRes.equals("1")){
					responseOME = buildCardActiveResponseOME("01","", "校验失败，卡号"+recmap.get(this.cardNo)+"相关信息不匹配");
					System.out.println("responseOME:" + responseOME);
					return false;
				}
			}catch(Exception e){
				
				System.out.println("校验异常");
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	private boolean deal(){
		try{
			String currentDate = PubFun.getCurrentDate();
			String currentTime = PubFun.getCurrentTime();
			for(int i=0;i<reclist.size();i++){
				recmap = new HashMap();
				recmap =(Map)reclist.get(i);
				String tbCardNo = recmap.get(this.cardNo).toString();
				String sql = "update LICardActiveInfoList set CardStatus='02',modifydate='"
						+ currentDate
						+ "',modifytime='"
						+ currentTime
						+ "' where cardstatus='01' and CardNo='" + tbCardNo + "'";
				this.tMMap.put(sql, "UPDATE");
			}
		}catch(Exception e){
			responseOME = buildCardActiveResponseOME("01","", "deal异常");
			System.out.println("responseOME:" + responseOME);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean submit(){	
		VData v = new VData();
		v.add(this.tMMap);
		try{
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				responseOME = buildCardActiveResponseOME("01","", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			this.responseOME = buildCardActiveResponseOME("00","", "");
		}catch(Exception e){
			System.out.println("数据提交失败************************");
			responseOME = buildCardActiveResponseOME("01","", "数据提交失败!");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	private OMElement buildCardActiveResponseOME(String state, String errCode,String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("MYCERTACTWT", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "BRANCHCODE", this.branchcode);
		tool.addOm(DATASET, "SENDOPERATOR", this.sendoperator);
		tool.addOm(DATASET, "MESSAGETYPE", this.messageType);

		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}
}
