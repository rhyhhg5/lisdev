package com.ecwebservice.subinterface;

import java.util.Date;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.utils.CoreDAO;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:卡单登陆校验
 * 
 * @author yuanzw 创建时间：2010-1-29 下午04:17:53 Description:
 * @version
 */

public class CardLoginVerify {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String password = null;// 卡单号码

	private String cardNo = null;// 卡单密码

	// 卡单登陆校验
	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("ZCARDLOGIN")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);
							if (!"99".equals(messageType)) {
								responseOME = buildCardResponseOME("", "01",
										"5", "报文类型不正确!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "5", "报文类型不正确!");
								return responseOME;
							}

						}

						if (child_element.getLocalName().equals("CARDNO")) {
							cardNo = child_element.getText();

							System.out.println(cardNo);
						}
						if (child_element.getLocalName().equals("PASSWORD")) {
							password = child_element.getText();

							System.out.println(password);
						}
					}
					// 日志3
					LoginVerifyTool.writeLoginLog(cardNo, password);
					

					if (cardNo == null || "".equals(cardNo)) {
						responseOME = buildCardResponseOME("", "01", "1",
								"卡单号码为空值");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"1", "卡单号码为空值");
						return responseOME;
					}
					if (password == null || "".equals(password)) {
						responseOME = buildCardResponseOME("", "01", "2",
								"卡单密码为空值");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"2", "卡单密码为空值");
						return responseOME;
					}
					// 为了测试通过
//					if (password.equals("6677881000000019")) {
//						responseOME = buildCardResponseOME("453", "00", "0",
//								"成功");
//						System.out.println("responseOME:" + responseOME);
//						return responseOME;
//					}

					// ****单证号 有效性校验 1 单证号不在退保表中 2 state in ('2','10','11','13')
					String libSql = "select count(*) from libcertify where cardNo='"+cardNo+"'";
					ExeSQL tExeSQL = new ExeSQL();
					int libCount = Integer
							.parseInt(tExeSQL.getOneValue(libSql));
					if (libCount > 0) {
						responseOME = buildCardResponseOME("", "01", "3",
								"单证号在退保表中 无效");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"3", "单证号在退保表中 无效");
						return responseOME;
					}

					LisIDEA tLisIdea = new LisIDEA();
					String strpassword = tLisIdea.encryptString(password);

					
					String sql = "  select  lmcr.riskcode "
							+ "from lmcardrisk lmcr,(select lzcn.CardNo, lzc.CertifyCode, lzc.State "
							+ "from LZCardNumber lzcn "
							+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
							+ "where 1 = 1 and lzcn.CardNo='"
							+ cardNo
							// + "' and lzcn.cardpassword2='" + strpassword
							+ "' and lzcn.cardpassword='"
							+ strpassword
							+ "' and  lzc.State in ('2','10','11','13')) re where lmcr.risktype ='W' and lmcr.certifycode=re.certifycode";

					SSRS sResult = tExeSQL.execSQL(sql);
					// 查询出激活截止日期，进行控制
					// String currentDate = PubFun.getCurrentDate();
					// String currentTime = PubFun.getCurrentTime();
					
					CoreDAO tCoreDAO = new CoreDAO();
					String errInfo = null ;
					//只校验卡号和密码
					boolean NoAndPasswordFlag = tCoreDAO.cardNoAndPassWordVerify(cardNo, strpassword);
					if(!NoAndPasswordFlag){
						errInfo = "卡单号码或卡单密码不正确";
						responseOME = buildCardResponseOME("", "01", "3",
								errInfo);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"3", errInfo);
						return responseOME;
					}
					
					//如果在licardactiveinfolist已存在该卡号的记录，则可能在老网站中已经进行了激活，不允许再登录
					
					SSRS infoListSSRS = tCoreDAO.getLicardactiveinfolistSSRS(cardNo);

					//校验当前日期是否在卡的最终激活日期之前 by licaiyan ----------------------
					String codeSql = "select code from ldcode where codeType ='switch' with ur";
					SSRS sCodeResult = tExeSQL.execSQL(codeSql);
					String code = sCodeResult.GetText(1, 1);
					if(null == code){
						code = "CodeIsNull";
					}
					boolean validate = true;
					if("1".equals(code)){
						validate = beforeLastVaDate(cardNo);
					}
					
					if (sResult.MaxRow > 0 && infoListSSRS.MaxRow<=0 && validate) {
						
						// 查询成功
						
						String certifyCode = sResult.GetText(1, 1);
						responseOME = buildCardResponseOME(certifyCode, "00",
								"0", "成功");

						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "00", "100",
								"0", "成功");
					} else {
						
						if(infoListSSRS.MaxRow>0){
							errInfo = "该卡已激活";
						}else if(sResult.MaxRow<=0){
							errInfo = " 请检查单证状态";
						}else if(!validate){
							errInfo = "该卡已经过期失效";
						}
						responseOME = buildCardResponseOME("", "01", "3",
								errInfo);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"3", errInfo);
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildCardResponseOME("", "01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "100", "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}
	
	/**
	 * 如果当前日期在最终激活日期之前，则单证有效，返回true
	 * @return
	 */
	private boolean  beforeLastVaDate(String cardNo){
		System.out.println("要进行最终激活日期校验--------------");
		String tSubCodeSQL = "select cardtype from lzcardnumber where cardno = '"+cardNo+"'";
		String tSubCode = new ExeSQL().getOneValue(tSubCodeSQL);
		if("".equals(tSubCode) || tSubCode == null){
			return false;
		}
		String subcode = tSubCode;

		String numPart = cardNo.substring(tSubCode.length());
		
	    //查看是否有校验位，若有，则去掉两位校验位
		String no = "";
		ExeSQL tExeSQL = new ExeSQL();
		String checkNoStr = "select  CheckRule  from lmcertifydes where certifyclass='D' and subcode = '"+subcode+"'";//'1'--'有校验' ,'2'--'无校验',
		SSRS chkResult = tExeSQL.execSQL(checkNoStr);
		String checkRule = chkResult.GetText(1, 1);
		if(null != checkRule && "1".equals(checkRule)){//有校验位
			no = numPart.substring(0, numPart.length()-2);
		}else{
			no = numPart;
		}
		
		String  sqlStr = "select ActiveDate  from LZCardPrint where subCode='"+subcode+"'"
						 +" and startNo <='"+no+"'  and endNo >='"+no+"' with ur";
		
		SSRS result = tExeSQL.execSQL(sqlStr);
		String lastDate = result.GetText(1, 1);
		if(null == lastDate || "".equals(lastDate)){
			System.out.println("没有定义最终激活日期，不进行校验--------------");
			return true;
		}
		FDate fd = new FDate();
        Date dd1 = fd.getDate(lastDate);//最終激活日期
        Date dd2 = fd.getDate(PubFun.getCurrentDate());

		
		if(dd1.before(dd2)){
			return false;
		}
		
		return true;
	}

	// 构建卡单登陆校验返回xml
	private OMElement buildCardResponseOME(String certifyCode, String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement DATASET = fac.createOMElement("ZCARDLOGIN", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", messageType);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "CERTIFYCODE", certifyCode);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
