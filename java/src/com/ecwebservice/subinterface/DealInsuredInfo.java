package com.ecwebservice.subinterface;


import java.net.URLDecoder;
import java.util.Iterator;


import org.apache.axiom.om.OMElement;



import com.sinosoft.lis.pubfun.GlobalInput;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;

import com.sinosoft.lis.pubfun.WriteToExcel;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public class DealInsuredInfo {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号


	
	private String grpcontno = "";// 网点
	


	private String email = "";// 报文类型


	private String mOutXmlPath = null;

	
	private OMElement responseOME = null;
	
	private String cardNo = null;
	
	private MMap tMMap = new MMap();
	
	private String cardpassword = null;
	private LoginVerifyTool mTool = new LoginVerifyTool();
	
	int ContNum = 0;//报文中保险卡节点数量
	int AppntNum = 0;//报文中投保人数量
	int InsuNum = 0;//报文中被保人数量
	boolean BnfInfo = true;//判断是否由受益人信息 ps：根据受益人必须是被保人本人，所以受益人节点为空节点
	
	
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
//			responseOME = buildCardActiveResponseOME("01","01", "加载激活信息失败");
//			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
//		if(!check()){
//			return responseOME;
//		}
		if(!deal()){
			return responseOME;
		}
//		if(!save()){
//			return responseOME;
//		}
		//发送邮件 by gzh 20110218
		
			System.out.println("=====开始发送邮件=====");
			if(!sendEmail()){
				return responseOME;
			}
		
		return responseOME;
	}
	

	
	//加载卡激活信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("HYINSURED")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						
						if(child_element.getLocalName().equals("GRPCONTNO")){
							this.grpcontno=child_element.getText();
							continue;
						}
						
						if(child_element.getLocalName().equals("EMAIL")){
							this.email=child_element.getText();
							continue;
						}
						
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验卡激活信息
	 * @return
	 */
//	private boolean check(){
//		if(!checkInfo()){
//			System.out.println("校验基本信息失败");
//			return false;
//		}
//		if(!chkMingYa()){
//			System.out.println("明亚校验信息失败");
//			return false;
//		}
//		return true;
//	}
	
	/**
	 * 处理EXCEL生成
	 * @return
	 */ 
	private boolean deal(){
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        String fileName = "团体保单"+this.grpcontno + "被保人清单.xls";
        
        String path = this.getClass().getResource("").getPath();
        path = path.substring(0,path.lastIndexOf("/WEB-INF"));
		String temp = path.substring(0, 5);
		if("file:".equalsIgnoreCase(temp)){
			path = path.substring(5);
		}
    
        mOutXmlPath = path + "/" + fileName;
        String mSql="select contno,name,idno from lcinsured where grpcontno='"+this.grpcontno+"' with ur";
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 1][8];

       
        mToExcel[0][0] = "团体分单号";
        mToExcel[0][1] = "姓名";
        mToExcel[0][2] = "证件号码";
        


        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }
	

	
	//提交卡激活信息
//	private boolean save(){
//		
//		try{
//			String cheCardNoSQL="select 1 from WFContList where cardno='"+cardNo+"'";
//			String cheCardNoRes = new ExeSQL().getOneValue(cheCardNoSQL);
//			
//			lockCont(cardNo);
//			VData v = new VData();
//			v.add(tMMap);
//			PubSubmit p = new PubSubmit();
//			if(!p.submitData(v, "")){
//				System.out.println("数据提交失败************************");
//				responseOME = buildCardActiveResponseOME("01","6", "数据提交失败!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,
//						messageType, sendDate, sendTime, "01",
//						"100", "6", "数据提交失败!");
//				return false;
//			}
//			responseOME = buildCardActiveResponse("00",this.cardNo,this.cardpassword);
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "00",
//					"100", "0", "数据提交成功!");
//		}catch(Exception e){
//			System.out.println("数据提交异常");
//			e.printStackTrace();
//			return false;
//		}
//		
//		return true;
//	}
//	private OMElement buildCardActiveResponseOME(String state, String errCode,String errInfo) {
//		OMFactory fac = OMAbstractFactory.getOMFactory();
//
//		OMElement DATASET = fac.createOMElement("MYCERTACT", null);
//		LoginVerifyTool tool = new LoginVerifyTool();
//		tool.addOm(DATASET, "BATCHNO", batchno);
//		
//
//		tool.addOm(DATASET, "STATE", state);
//		tool.addOm(DATASET, "ERRCODE", errCode);
//		tool.addOm(DATASET, "ERRINFO", errInfo);
//
//		return DATASET;
//	}
	
	
//	private OMElement buildCardActiveResponse(String state,String cardno, String password) {
//		OMFactory fac = OMAbstractFactory.getOMFactory();
//
//		OMElement DATASET = fac.createOMElement("MYCERTACT", null);
//		LoginVerifyTool tool = new LoginVerifyTool();
//		tool.addOm(DATASET, "BATCHNO", batchno);
//		tool.addOm(DATASET, "SENDDATE", sendDate);
//		tool.addOm(DATASET, "SENDTIME", sendTime);
//		tool.addOm(DATASET, "BRANCHCODE", this.branchcode);
//		tool.addOm(DATASET, "SENDOPERATOR", this.sendoperator);
//
//		tool.addOm(DATASET, "CARDNO", cardno);
//		tool.addOm(DATASET, "PASSWORD", password);
//		
//		tool.addOm(DATASET, "STATE", state);
//
//
//		return DATASET;
//	}

    
	
	/**
     * 锁定激活卡号。
     * @param cLCContSchema
     * @return
     */
//    private MMap lockCont(String cardno)
//    {
//        MMap tMMap = null;
//
//        // 签单锁定标志为：“SC”
//        String tLockNoType = "WX";//网销锁定
//        // -----------------------
//
//        // 锁定有效时间
//        String tAIS = "300";
//        // -----------------------
//
//        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("LockNoKey", cardno);
//        tTransferData.setNameAndValue("LockNoType", tLockNoType);
//        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
//
//        VData tVData = new VData();
//        tVData.add(mGlobalInput);
//        tVData.add(tTransferData);
//
//        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
//        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
//        if (tMMap == null)
//        {
//            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
//            return null;
//        }
//
//        return tMMap;
//    }
  
    /**
     * 发送邮件。
     * @param cLCContSchema
     * @return
     */
    private boolean sendEmail(){
    	
    	
//    	LICardActiveInfoListDB tLICardActiveInfoListDB = new LICardActiveInfoListDB();
//		tLICardActiveInfoListDB.setCardNo(cardNo);
//		LICardActiveInfoListSet tLICardActiveInfoListSet = tLICardActiveInfoListDB.query();
//		LICardActiveInfoListSchema tLICardActiveInfoListSchema = tLICardActiveInfoListSet.get(1);
//		if(tLICardActiveInfoListSchema == null){
//			responseOME = buildCardActiveResponseOME("01","101", "保单已承保，但发送电子保单邮件失败，请与客服联系，补发电子保单!");
//			System.out.println("responseOME:" + responseOME+" 原因：获取licardactiveinfolist生效日期和失效日期失败！");
//			LoginVerifyTool.writeLog(responseOME, batchno,
//					messageType, sendDate, sendTime, "01",
//					"101", "86", "保单已承保，但发送电子保单邮件失败,请与客服联系，补发电子保单!");
//            return false;
//		}
//		String Cvalidate = tLICardActiveInfoListSchema.getCValidate();//生效日期
//		String Inactivedate = tLICardActiveInfoListSchema.getInActiveDate();//失效日期
		//明亚接口字典规定 性别1为男 2为女
		String CurrentDate = PubFun.getCurrentDate();
		cardNo=this.grpcontno;
		MailSender tMailSender = new MailSender(cardNo);
		tMailSender.send(this.email);
		//向明亚指定邮箱发送邮件
//		tMailSender.send("1j1@mingya.com.cn");
    	return true;
    }
   


    /**
     * 统一化时间格式，避免时间格式不一致导致的校验失败。
     * @param date
     * @return
     * 如传入格式为20110201 转换为201121
     */
    private String getDate(String aDate,String flag){
    	if("0".equals(flag)){
    		if("0".equals(aDate.substring(4, 5)) && "0".equals(aDate.substring(6, 7))){
        		aDate = aDate.substring(0, 4)+aDate.substring(5,6)+aDate.substring(7);
    		}else if("0".equals(aDate.substring(4, 5))){
    			aDate = aDate.substring(0, 4)+aDate.substring(5);
    		}else if("0".equals(aDate.substring(6, 7))){
    			aDate = aDate.substring(0, 6)+aDate.substring(7);
    		}
    	}else{
    		if("0".equals(aDate.substring(2, 3)) &&  "0".equals(aDate.substring(4, 5))){
        		aDate = aDate.substring(0, 2)+aDate.substring(3,4)+aDate.substring(5);
    		}else if("0".equals(aDate.substring(2, 3))){
    			aDate = aDate.substring(0, 2)+aDate.substring(3);
    		}else if("0".equals(aDate.substring(4, 5))){
    			aDate = aDate.substring(0, 4)+aDate.substring(5);
    		}
    	}
    	return aDate;
    }
}
