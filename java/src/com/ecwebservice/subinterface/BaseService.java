package com.ecwebservice.subinterface;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-3-1 下午03:07:14 Description:
 * @version
 */

// 注意：是客户端的ip，不是获取本机的ip
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis.transport.http.HTTPConstants;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.ServiceContext;

public class BaseService {
	HttpSession session;

	public static Map map = new HashMap();

	public static String getClientIp() {
		MessageContext mc = MessageContext.getCurrentMessageContext();
		// System.out.println("SOAP :" + mc.getEnvelope().getBody().toString());

		HttpServletRequest request = (HttpServletRequest) mc
				.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		String IP = request.getHeader("X-Forwarded-For");
		if(IP == null || IP.length() == 0) { 
		   IP = request.getRemoteAddr(); 
		}
		System.out.println("remote  ip:  " +IP);
		return IP;
	}

//	public void putSession(String key, String value) {
//		MessageContext mc = MessageContext.getCurrentMessageContext();
//		ServiceContext sc = mc.getServiceContext();
//		
//		sc.setProperty(key, value);
//		System.out.println("IN BS:"+getSessionValue(key));
//
////		 MessageContext mc = MessageContext.getCurrentMessageContext();
////		 //HttpSession session = mc.getp
////		 HttpServletRequest request = (HttpServletRequest) mc
////		 .getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
////		 session = request.getSession();
////		 session.setAttribute(key, value);
//		
//	}

	public String getSessionValue(String key) {
		
		MessageContext mc = MessageContext.getCurrentMessageContext();
		ServiceContext sc = mc.getServiceContext();
		return (String) sc.getProperty(key);
	}

}
