package com.ecwebservice.subinterface;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.poi.hwpf.usermodel.DateAndTime;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import com.cbsws.xml.ctrl.blogic.HMCertPayTradeUI;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.*;
import com.cbsws.xml.ctrl.blogic.*;

public class DealEcphLisFinanceData {

	/**
	 *#162-处理电商调用核心产生财务数据的方法
	 *@author fromyzf
	 *@param queryData
	 *@return String :核心处理是否成功
	 */
	
	private String tBatchNo = null;// 批次号
	String tManageCom = null;//管理机构
	String tSaleChnl = null;//
	String tWrapCode = null;//产品编码
	String tWrapName = null;//产品名称
	String tSalesCount = null;//购买数量
	String tFinanceNum = null;//交易号
	String tChargeAccount = null;//收费账号
	String tChargeAmount = null;//收费金额
	String tConfMakeDate = null;//到账日期
	String tConfMakeTime = null;//到账时间
	String tIdentityKey = null;//接口识别码
	
	public OMElement queryData(OMElement Oms){
		
		OMElement responseOME = null;
		
		System.out.println("DealEcphLisFinanceData.java start ...");

		String FlagStr = "0";
		String Content = "";
		String strResult = null;
		String state="false";//返回报文状态：true-全部处理成功；false:-出现异常或错误，查看返回的报文中ERRCODE值
		

		//解析报文
		//--------------------------------------解析xml开始：xmlString
		try {
			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("LisFinanceData")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();
						if (child_element.getLocalName().equals("BATCHNO")&& child_element.getText() != "") {
							tBatchNo = child_element.getText();
						}
						if(child_element.getLocalName().equals("MANAGECOM")){
							tManageCom=child_element.getText();
						}
						if(child_element.getLocalName().equals("SALECHNL")){
							tSaleChnl=child_element.getText();
						}
						if(child_element.getLocalName().equals("WRAPCODE")){
							tWrapCode=child_element.getText();
						}
						if(child_element.getLocalName().equals("WRAPNAME")){
							tWrapName=child_element.getText();
						}
						if(child_element.getLocalName().equals("SALESCOUNT")&& child_element.getText() != ""){
							tSalesCount=child_element.getText();
						}
						if(child_element.getLocalName().equals("FINANCE_NUM")){
							tFinanceNum=child_element.getText();
						}
						if(child_element.getLocalName().equals("CHARGE_ACCOUNT")){
							tChargeAccount=child_element.getText();
						}
						if(child_element.getLocalName().equals("CHARGE_AMOUNT")){
							tChargeAmount=child_element.getText();
						}
						if(child_element.getLocalName().equals("CONFMAKE_DATE")){
							tConfMakeDate=child_element.getText();
						}
						if(child_element.getLocalName().equals("CONFMAKE_TIME")){
							tConfMakeTime=child_element.getText();
						}
						if(child_element.getLocalName().equals("FINANCE_PASS")){
							tIdentityKey=child_element.getText();
						}
					}
				}
					System.out.println("BatchNo = " + tBatchNo);
					System.out.println("FinanceNum = " + tFinanceNum);
					
//			--------------------------------------解析xml结束
					
				try
				{
				    VData tVData = new VData();
				    TransferData tTransferData = new TransferData();
				    
				    tTransferData.setNameAndValue("BatchNo", tBatchNo);
				    tTransferData.setNameAndValue("ManageCom", tManageCom);
				    tTransferData.setNameAndValue("SaleChnl", tSaleChnl);
				    tTransferData.setNameAndValue("WrapCode", tWrapCode);
				    tTransferData.setNameAndValue("WrapName", tWrapName);
				    tTransferData.setNameAndValue("SalesCount", tSalesCount);
				    
				    tTransferData.setNameAndValue("FinanceNum", tFinanceNum);
				    tTransferData.setNameAndValue("ChargeAccount", tChargeAccount);
				    tTransferData.setNameAndValue("ChargeAmount", tChargeAmount);
				    tTransferData.setNameAndValue("ConfMakeDate", tConfMakeDate);
				    tTransferData.setNameAndValue("ConfMakeTime", tConfMakeTime);
				    tTransferData.setNameAndValue("IdentityKey", tIdentityKey);
				    
				    System.out.println("tTransferData封装好的数据"+tTransferData.getValues());
				    tVData.add(tTransferData);
				    tTransferData.getValues();
				    HMCertPayTradeUI tBL = new HMCertPayTradeUI();
				    if(!tBL.submitData(tVData, "HMPay"))
				    {
				        Content = tBL.mErrors.getFirstError();
				        FlagStr = "0";
				    }
				    else
				    {
				        Content = "处理成功";
				        FlagStr = "1";
				    }
				}
				catch(Exception ex)
				{
					System.out.println("处理异常！");
					Content = "处理异常";
				    FlagStr = "2";
				}
				strResult = FlagStr + "|" + Content;
				System.out.println("电商调用核心："+strResult);
			}
			if("1".equals(FlagStr)){
				state="true";
			}
			responseOME=buildResponseOME(tBatchNo, state,FlagStr,Content);//parameter:批次号；状态；错误代码；错误信息
			return responseOME;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("处理document时异常！");
				return null;
			}
	}	
	public static void main(String[] args) {
		DealEcphLisFinanceData defd=new DealEcphLisFinanceData();
		//LisFinanceData是响应报文的报头
		OMElement root = buildQueryData();//命名空间
		OMElement result=defd.queryData(root);
		System.out.println(result);
		
	}
	/**
	 * 封装返回的报文
	 * 
	 */
	private OMElement buildResponseOME(String tBatchNo,
            String state, String errCode,
            String errInfo) {
        OMFactory fac = OMAbstractFactory.getOMFactory();

        OMElement LisFinanceData = fac.createOMElement("LisFinanceData", null);
        LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(LisFinanceData, "BATCHNO",tBatchNo);
		tool.addOm(LisFinanceData, "SENDDATE",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		tool.addOm(LisFinanceData, "SENDTIME",new SimpleDateFormat("hh:mm:ss").format(new Date()));
		
		tool.addOm(LisFinanceData, "STATE", state);//处理状态
		tool.addOm(LisFinanceData, "POLICYNO", tool.getSerialNo());
		
		tool.addOm(LisFinanceData, "ERRCODE",errCode);
		tool.addOm(LisFinanceData, "ERRCODE", errInfo);
        
        return LisFinanceData;
    }
	
	public static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}
	public static OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}

}
