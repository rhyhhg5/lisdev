package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class ContListQueryForCS {
	
	/** 回复报文*/
	private OMElement responseOME = null;
	
	private OMElement mOME = null;
	
	/** 批次号*/
	private String batchno = "";

	/** 报文发送日期*/
	private String sendDate = "";

	/** 报文发送时间*/
	private String sendTime = ""; 

	/** 报文类型*/
	private String messageType = ""; 
	
	/** 当前页码*/
	private String pageIndex = null;
	
	/** 每页记录条数*/
	private String pageSize = null;
	
	/** 卡号*/
	private String cardNo = null;
	
	/** 产品编码*/
	private String productId =null;
	
	/** 管理机构代码*/
	private String manageCom = null;
	
	/** 被保人姓名*/
	private String insuredName=null;
	
	/** 被保人证件号码*/
	private String insuredIdno = null;
	
	/** 生效日期查询上限*/
	private String feffectDate = null;
	
	/** 生效日期查询下限*/
	private String teffectDate = null;
	
	/** 生效止期查询上限*/
	private String fendDate = null;
	
	/** 生效止期查询下限*/
	private String tendDate = null;
	/**第三方平台*/
	private String platForm = null; //add by liujiandong 2018/3/5
//	modify by zhangyige 2012-03-26  便于金农按激活日期统计
	
	/** 返回信息*/
	private String rCardNo =null;
	private String rProductId=null;
	private String rInsuredName=null;
	private String rInsuredIdno=null;
	private String rSingDate=null;
	private String rEffectDate=null;
	private String rPrem="";
	private String rpassword="";
	private String rMaxrow="";
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildCardActiveResponseOME("01","01", "接收查询信息失败",null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			responseOME = buildCardActiveResponseOME("01","02", "校验信息失败",null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!deal()){
			responseOME = buildCardActiveResponseOME("01","03", "处理信息失败",null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		return responseOME;
	}
	
	/**
	 * 接收报文
	 * @return
	 */
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("CONTLISTQUERYFORCS")){
					Iterator leaf_element = Om.getChildren();
					while(leaf_element.hasNext()){
						OMElement leaf = (OMElement)leaf_element.next();
						if(leaf.getLocalName().equals("PLAT")){ //add by liujiandong 2018/3/5
							this.platForm=leaf.getText();
							System.out.println(platForm+"platform");
							continue;
						}
						if(leaf.getLocalName().equals("BATCHNO")){
							this.batchno=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("SENDDATE")){
							this.sendDate=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("SENDTIME")){
							this.sendTime=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("MESSAGETYPE")){
							this.messageType=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("PAGEINDEX")){
							this.pageIndex=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("PAGESIZE")){
							this.pageSize=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("CARDNO")){
							this.cardNo=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("PRODUCTID")){
							this.productId=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("COMCODE")){
							this.manageCom=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("INSUREDNAME")){
							this.insuredName=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("INSUREDIDNO")){
							this.insuredIdno=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("FEFFECTDATE")){
							this.feffectDate=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("TEFFECTDATE")){
							this.teffectDate=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("FENDDATE")){
							this.fendDate=leaf.getText();
							continue;
						}
						if(leaf.getLocalName().equals("TENDDATE")){
							this.tendDate=leaf.getText();
							continue;
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验报文信息
	 * @return
	 */
	private boolean check(){
//		if((this.insuredName==null || this.insuredName.equals("")) && 
//				(this.insuredIdno==null || this.insuredIdno.equals(""))){
//			responseOME = buildCardActiveResponseOME("01","01", "接收查询信息失败",null);
//			System.out.println("responseOME:" + responseOME);
//			return false;
//		}
		return true;
	}
	
	/**
	 * 报文业务处理
	 * @return
	 */
	private boolean deal(){
		try{
			String maxNumSQL=" select count(1)"
								+" from (" 		
								   +" select lica.cardno,"
					               +"lica.name,"
					               +"lica.idno,"
					               +"lica.activedate,"
					               +"lica.cvalidate,"
					               +"lica.prem,"
					               +"lmcr.riskcode,"
					               +"lzcn.cardpassword,"
					               +"(case  when substr(lzc.ReceiveCom, 1, 1) in ('D', 'E') then lzc.ReceiveCom"
					               	+" when substr(lzc.SendOutCom, 1, 1) in ('D', 'E') then lzc.SendOutCom end) ReceiveSale "
					               +"from LICardActiveInfoList lica "
					               +"inner join lzcardnumber lzcn on lzcn.cardno = lica.cardno "
					               +"inner join lmcertifydes lmcd on lmcd.subcode = lzcn.cardtype "
					               +"inner join lmcardrisk lmcr on lmcd.certifycode =lmcr.certifycode "
					               +"inner join wfcontlist cont on lzcn.cardno = cont.cardno "
					               +"inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
					               +"where lmcr.risktype = 'W' "
					               +"and (lica.cardstatus is null or lica.cardstatus ='01') "
									 +"and lmcr.riskcode is not null ";
									StringBuffer maxbuf = new StringBuffer(maxNumSQL);
									if(this.insuredName !=null && !this.insuredName.equals("")){
										maxbuf.append("and lica.name like '").append(this.insuredName).append("%' ");
									}
									if(this.insuredIdno !=null && !this.insuredIdno.equals("")){
										maxbuf.append("and lica.idno like '").append(this.insuredIdno).append("%' ");
									}
									if(this.cardNo != null && !this.cardNo.equals("")){
										maxbuf.append("and lica.cardno like '").append(this.cardNo).append("%' ");
									}
									if(this.feffectDate!=null && !this.feffectDate.equals("")){
										maxbuf.append("and lica.cvalidate >= '").append(this.feffectDate).append("' ");
									}
									if(this.teffectDate!=null && !this.teffectDate.equals("")){
										maxbuf.append("and lica.cvalidate <= '").append(this.teffectDate).append("' ");
									}
									if(this.productId !=null && !this.productId.equals("")){
										maxbuf.append("and lmcr.riskcode = '").append(this.productId).append("' ");
									}
									if(this.platForm !=null && !this.platForm.equals("")){ //add by liujiandong 2018/3/5
										maxbuf.append("and cont.branchcode = '").append(this.platForm).append("' ");
									}
									maxbuf.append("order by lica.cardno ) rec where 1=1 ");
									if(this.manageCom !=null && !this.manageCom.equals("")){
										maxbuf.append(" and (case when substr(rec.ReceiveSale, 1, 1) = 'D' " +
										"then (select laa.ManageCom " +
												"from LAAgent laa where laa.AgentCode = " +
												"substr(rec.ReceiveSale, 2, length(rec.ReceiveSale))) " +
										"when substr(rec.ReceiveSale, 1, 1) = 'E' " +
										"then (select lac.ManageCom " +
												"from LACom lac where lac.AgentCom = " +
												"substr(rec.ReceiveSale, 2, length(rec.ReceiveSale))) end ) " +
										"like '").append(this.manageCom).append("%' ");
									}
									maxbuf.append(" with ur ");
								
			
			String actInfoSQL="select rec.cardno,"
				                        +"rec.name,"
				                        +"rec.idno,"
				                        +"rec.activedate,"
				                        +"rec.cvalidate,"
				                        +"rec.prem,"
				                        +"rec.riskcode,"
				                        +"rec.cardpassword"
				                    +" from (" 		
									   +" select lica.cardno,"
				                       +"lica.name,"
				                       +"lica.idno,"
				                       +"lica.activedate,"
				                       +"lica.cvalidate,"
				                       +"lica.prem,"
				                       +"lmcr.riskcode,"
				                       +"lzcn.cardpassword,"
				                       +"(case  when substr(lzc.ReceiveCom, 1, 1) in ('D', 'E') then lzc.ReceiveCom"
				                       	+" when substr(lzc.SendOutCom, 1, 1) in ('D', 'E') then lzc.SendOutCom end) ReceiveSale "
				                       +"from LICardActiveInfoList lica "
				                       +"inner join lzcardnumber lzcn on lzcn.cardno = lica.cardno "
				                       +"inner join lmcertifydes lmcd on lmcd.subcode = lzcn.cardtype "
				                       +"inner join lmcardrisk lmcr on lmcd.certifycode =lmcr.certifycode "
				                       +"inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
				                       +"inner join wfcontlist cont on lzcn.cardno = cont.cardno "
				                       +"where lmcr.risktype = 'W' "
				                       +"and (lica.cardstatus is null or lica.cardstatus ='01') "
										 +"and lmcr.riskcode is not null ";
					StringBuffer buf = new StringBuffer(actInfoSQL);
					if(this.insuredName !=null && !this.insuredName.equals("")){
						buf.append("and lica.name like '").append(this.insuredName).append("%' ");
					}
					if(this.insuredIdno !=null && !this.insuredIdno.equals("")){
						buf.append("and lica.idno like '").append(this.insuredIdno).append("%' ");
					}
					if(this.cardNo != null && !this.cardNo.equals("")){
						buf.append("and lica.cardno like '").append(this.cardNo).append("%' ");
					}
					if(this.feffectDate!=null && !this.feffectDate.equals("")){
						buf.append("and lica.cvalidate >= '").append(this.feffectDate).append("' ");
					}
					if(this.teffectDate!=null && !this.teffectDate.equals("")){
						buf.append("and lica.cvalidate <= '").append(this.teffectDate).append("' ");
					}
					if(this.productId !=null && !this.productId.equals("")){
						buf.append("and lmcr.riskcode = '").append(this.productId).append("' ");
					}
					if(this.platForm !=null && !this.platForm.equals("")){// add byliujiandong 2018/3/6
						buf.append("and cont.branchcode = '").append(this.platForm).append("' ");
					}
					buf.append("order by lica.cardno ) rec where 1=1 ");
					if(this.manageCom !=null && !this.manageCom.equals("")){
						buf.append(" and (case when substr(rec.ReceiveSale, 1, 1) = 'D' " +
											"then (select laa.ManageCom " +
													"from LAAgent laa where laa.AgentCode = " +
													"substr(rec.ReceiveSale, 2, length(rec.ReceiveSale))) " +
											"when substr(rec.ReceiveSale, 1, 1) = 'E' " +
											"then (select lac.ManageCom " +
													"from LACom lac where lac.AgentCom = " +
													"substr(rec.ReceiveSale, 2, length(rec.ReceiveSale))) end ) " +
											"like '").append(this.manageCom).append("%' ");
					}
					StringBuffer pagebuf = new StringBuffer();
					pagebuf.append("select * from (select rownumber() over() as rowid, aa.* from (");
					pagebuf.append(buf.toString());
					pagebuf.append(") as aa ");
					pagebuf.append(") as tmp where 1 = 1 ");
					if(this.pageIndex ==null || this.pageIndex.equals("") || this.pageSize ==null || this.pageSize.equals("")){
						System.out.println("没有得到分页信息");
						return false;
					}
					pagebuf.append("and rowid >= (").append(this.pageIndex).append("-1) *").append(this.pageSize).append(" + 1 ");
					pagebuf.append("and rowid <= ").append(this.pageIndex).append("*").append(this.pageSize).append(" with ur");
			SSRS tSSRS = null;
			tSSRS = new ExeSQL().execSQL(pagebuf.toString());
			if(tSSRS == null || tSSRS.MaxRow<1){
				responseOME = buildCardActiveResponseOME("01","01", "没有得到查询结果",null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			this.rMaxrow=new ExeSQL().getOneValue(maxbuf.toString());
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMElement CONTLIST = fac.createOMElement("CONTLIST", null);
			for(int i=1;i<=tSSRS.MaxRow;i++){
				this.rCardNo = tSSRS.GetText(i, 2);
				this.rInsuredName =tSSRS.GetText(i, 3);
				this.rInsuredIdno =tSSRS.GetText(i, 4);
				this.rSingDate = tSSRS.GetText(i, 5);
				this.rEffectDate = tSSRS.GetText(i, 6);
				if(!tSSRS.GetText(i, 7).equals("") || tSSRS.GetText(i, 7)!=null){
					this.rPrem = tSSRS.GetText(i, 7);
				}
				this.rProductId = tSSRS.GetText(i, 8);
				if(!tSSRS.GetText(i, 9).equals("") || tSSRS.GetText(i, 9)!=null){
					this.rpassword=tSSRS.GetText(i, 9);
					LisIDEA tLisIdea = new LisIDEA();
					rpassword=tLisIdea.decryptString(rpassword);	
				}
				OMElement subItem =buildCardActiveResponseItem();
				CONTLIST.addChild(subItem);
			}
			this.responseOME = buildCardActiveResponseOME("00","","",CONTLIST);
		}catch(Exception e){
			System.out.println("业务处理异常");
			e.printStackTrace();
			return false;
		}		
		return true;
	}
	
	/**
	 * 创建返回报文
	 * @param state
	 * @param errCode
	 * @param errInfo
	 * @return OMElement
	 */
	private OMElement buildCardActiveResponseOME(String state, String errCode,String errInfo,OMElement contList) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("CONTLISTQUERYFORCS", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", this.messageType);
		
		if(state.equals("01")){
			tool.addOm(DATASET, "STATE", state);
			tool.addOm(DATASET, "ERRCODE", errCode);
			tool.addOm(DATASET, "ERRINFO", errInfo);
			tool.addOm(DATASET, "TOTALROWNUM", null);
			tool.addOm(DATASET, "OUTPUTDATA", null);
		}
		
		if(state.equals("00")){
			tool.addOm(DATASET, "STATE", state);
			tool.addOm(DATASET, "ERRCODE", null);
			tool.addOm(DATASET, "ERRINFO", null);
			tool.addOm(DATASET, "TOTALROWNUM", this.rMaxrow);
			OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
			OUTPUTDATA.addChild(contList);
			DATASET.addChild(OUTPUTDATA);			
		}
		return DATASET;
	}
	
	/**
	 * 创建返回结点
	 * @param state
	 * @param errCode
	 * @param errInfo
	 * @return OMElement
	 */
	private OMElement buildCardActiveResponseItem() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tool = new LoginVerifyTool();

		OMElement ITEM = fac.createOMElement("ITEM", null);
		tool.addOm(ITEM, "CARDNO", this.rCardNo);
		tool.addOm(ITEM, "PASSWORD", this.rpassword);
		tool.addOm(ITEM, "PRODUCTID", this.rProductId);
		tool.addOm(ITEM, "INSUREDNAME", this.rInsuredName);
		tool.addOm(ITEM, "INSUREDIDNO", this.rInsuredIdno);
		tool.addOm(ITEM, "SIGNDATE", this.rSingDate);
		tool.addOm(ITEM, "EFFECTDATE", this.rEffectDate);
		tool.addOm(ITEM, "ENDDATE", null);
		tool.addOm(ITEM, "PREM", this.rPrem);			
		return ITEM;
	}
}
