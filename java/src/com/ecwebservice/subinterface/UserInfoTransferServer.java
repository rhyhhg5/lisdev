package com.ecwebservice.subinterface;



import java.util.Iterator;



import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;


import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:处理代理机构登陆发送的xml,返回xml
 * 
 * @author yuanzw 创建时间：2010-1-29 上午11:18:07
 * @version
 */

public class UserInfoTransferServer {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 交易类型

	private String sendOperator = null;// 操作员

	private String password = null;// 操作员密码
	
	private String macaddr = null ; //物理地址
	private String agentCode;
	private String userType;
	private String agentComCode;

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				
				if (Om.getLocalName().equals("HYXLOGIN")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
							continue;
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
							continue;
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
							continue;
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);
							if (!"99".equals(messageType)) {
								responseOME = buildResponseOME("","","01",
										"5", "报文类型不正确!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "5", "报文类型不正确!");
								return responseOME;
							}
							continue;
						}

						if (child_element.getLocalName().equals("SENDOPERATOR")) {
							sendOperator = child_element.getText();

							System.out.println(sendOperator);
//							BaseService bService = new BaseService();
//							bService.putSession("operator", sendOperator);
							//BaseService.map.put("operator", sendOperator);
							continue;
							
							
						}
						if (child_element.getLocalName().equals("PASSWORD")) {
							password = child_element.getText();

							System.out.println(password);
							continue;
						}
						if (child_element.getLocalName().equals("MACADDR")) {
							macaddr = child_element.getText();

							System.out.println("macaddr:"+macaddr);
							continue;
						}
					}
					//日志3
					LoginVerifyTool.writeLoginLog(sendOperator, password);
				
					
					if (sendOperator == null || "".equals(sendOperator)) {
						responseOME = buildResponseOME("", "","01", "1", "代理机构登陆-操作员为空值");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate,
								sendTime, "01", sendOperator,"1", "代理机构登陆-操作员为空值");
						return responseOME;
					}
					if (password == null || "".equals(password)) {
						responseOME = buildResponseOME("","", "01", "2",
								"操作员密码为空值");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate,
								sendTime, "01", sendOperator,"2", "操作员密码为空值");
						return responseOME;
					}
					//为了测试通过
//					if ("1".equals(sendOperator)) {
//						responseOME = buildResponseOME("PI3100000040", "00", "0",
//								"测试成功");
//						System.out.println("responseOME:" + responseOME);
//						LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate,
//								sendTime, "00", sendOperator,"0", "测试成功");
//						return responseOME;
//					}
					//encrypt
					LisIDEA tLisIdea = new LisIDEA();
			        String strpassword = tLisIdea.encryptString(password);

					String sql = "select managecom,rulestate,agentcode,usertype,agentcom,Mac_id,Mac_flag from LAComPersonal where LOGINNAME='"
							+ sendOperator
							+ "' and "
							+ "PASSWORD='"
//							+ password
							+ strpassword
							+ "'  ";
					ExeSQL tExeSQL = new ExeSQL();
					SSRS sResult = tExeSQL.execSQL(sql);

					if (null!=sResult&&sResult.MaxRow > 0) {
						// 查询成功
						// int count = Integer.parseInt(sResult.GetText(1, 1));
						String comCode = sResult.GetText(1, 1);
						//返回报文增加 代理人标志 
						String agentMark = sResult.GetText(1, 2);
						
						agentCode = sResult.GetText(1, 3);
						userType =  sResult.GetText(1, 4);
						agentComCode = sResult.GetText(1, 5);
						responseOME = buildResponseOME(comCode,agentMark, "00", "0",
								"成功");

						System.out.println("responseOME:" + responseOME);
						//日志4
						LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate,
								sendTime, "00", sendOperator,"0", "成功");
					} else {
						responseOME = buildResponseOME("","", "01", "3",
								"操作员或操作员密码不正确");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate,
								sendTime, "01", sendOperator,"3", "操作员或操作员密码不正确");
					}
					//需要校验mac地址
					if (sResult.GetText(1, 7).equals("1"))
					{
//						if(!sResult.GetText(1,6).equals(macaddr))
						if(macaddr.indexOf(","+sResult.GetText(1,6)+",")==-1)
						{
							responseOME = buildResponseOME("","", "01", "3",
							"该机器没有登录的权限");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate,
							sendTime, "01", sendOperator,"3", "该机器没有登录的权限");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("","", "01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME,batchno, messageType, sendDate,
					sendTime, "01", sendOperator,"4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	// 构建代理机构登陆返回xml

	private OMElement buildResponseOME(String comCode, String agentMark,String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("HYXLOGIN", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "COMCODE", comCode);
		tool.addOm(DATASET, "AGENTTYPE", agentMark);
		
		tool.addOm(DATASET, "AGENTCODE", agentCode);
		tool.addOm(DATASET, "USERTYPE", userType);
		
		tool.addOm(DATASET, "AGENTCOMCODE", agentComCode);
		
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
