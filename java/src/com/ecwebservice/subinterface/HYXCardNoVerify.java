package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class HYXCardNoVerify {
	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 交易类型

	private String sendOperator = null;// 操作员

	private String password = null;// 操作员密码

	private String certifyCode;

	private String comCode;

	private String printID;

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("HYXVALIDATEPRINTID")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);

						}

						// 套餐编码
						if (child_element.getLocalName().equals("CERTIFYCODE")) {
							certifyCode = child_element.getText();

							System.out.println(certifyCode);
						}
						// 代理机构
						if (child_element.getLocalName().equals("COMCODE")) {
							comCode = child_element.getText();

							System.out.println(comCode);
						}
						// 印刷号
						if (child_element.getLocalName().equals("PRINTID")) {
							printID = child_element.getText();

							System.out.println(printID);
						}

					}

					// ****单证号 有效性校验 1 单证号不在退保表中 2 state
					// in 10 11 13
					String libSql = "select count(*) from libcertify where cardNo='"
							+ printID + "'";
					ExeSQL tExeSQL = new ExeSQL();
					int libCount = Integer
							.parseInt(tExeSQL.getOneValue(libSql));
					if (libCount > 0) {
						responseOME = buildResponseOME("01", "3", "单证号无效");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01", "100",
								"3", "单证号在退保表中 无效");
						return responseOME;
					}

					// 查询套餐编码 进行校验
					String stateSql = "select  lmcr.riskcode,re.ReceiveCom ,re.CertifyCode, re.SubCode ,re.RiskCode, "
							+ "re.RiskVersion, re.StartNo, re.EndNo,re.state "
							+ "from lmcardrisk lmcr,"
							+ "(select lzcn.CardNo, lzc.CertifyCode, lzc.State,lzc.ReceiveCom ,lzc.SubCode"
							+ ",lzc.riskcode,lzc.RiskVersion ,lzc.StartNo,lzc.endno from LZCardNumber lzcn "
							+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
							+ " and lzc.EndNo = lzcn.CardSerNo where 1 = 1 and lzc.State in ('10','11') "
							+ "and lzcn.CardNo='"
							+ printID
							+ "' ) re where lmcr.risktype ='W' and lmcr.certifycode=re.certifycode";
					SSRS sResult = tExeSQL.execSQL(stateSql);
					if (sResult.MaxRow > 0) {
						String coreRiskCode = sResult.GetText(1, 1);

						// 查询单证号所属的中介机构编码 并进行校验
						String coreAgentComCode = sResult.GetText(1, 2);
						System.out.println("单证号所属的中介机构编码:" + coreAgentComCode);

						String receiveComCode = coreAgentComCode.substring(1);
						System.out.println("获取接收机构的后缀代码" + receiveComCode);

						// ****单证号state not in 10 11 13
						// 单证号码无效
						if (coreRiskCode == null || "".equals(coreRiskCode)
								|| !certifyCode.equals(coreRiskCode)
								|| !comCode.equals(receiveComCode)) {
							responseOME = buildResponseOME("01", "23",
									"单证号码无效！");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,
									messageType, sendDate, sendTime, "01",
									sendOperator, "23", "单证号码无效！");
							return responseOME;
						} else {
							responseOME = buildResponseOME("00", "0", "成功！");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,
									messageType, sendDate, sendTime, "00",
									sendOperator, "0", "成功！");
							return responseOME;
						}
					} else {
						responseOME = buildResponseOME("01", "23", "单证号码无效！");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								sendOperator, "23", "单证号码无效--！");
						return responseOME;
					}

					// 日志3
					// LoginVerifyTool.writeLoginLog(sendOperator, password);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", sendOperator, "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	// 构建代理机构登陆返回xml

	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("HYXVALIDATEPRINTID", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		// 单证号校验状态
		tool.addOm(DATASET, "STATE", state);

		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}
}
