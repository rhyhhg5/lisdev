package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

//业务员查询接口
public class QueryAgentInfo {

	private OMElement mOME = null;

	private String batchNo = null;// 批次号

	private String branchCode = "";// 报送单位

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String sendOperator = null; // 报送人员

	private String agentCode = null; // 业务员代码

	private String agentState = null; // 业务状态

	private String agentName = null; // 业务员姓名

	private String agentMobile = null; // 电话

	private String agentManageCom = null; // 管理机构

	private OMElement responseOME = null;

	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if (!load()) {
			return responseOME;
		}

		if (!check()) {
			return responseOME;
		}

		if (!dealData()) {
			return responseOME;
		}

		return responseOME=buildResponseOME("00","","");
	}

	private boolean check() {
		if(null==this.agentCode||"".equals(this.agentCode)){
			responseOME=buildResponseOME("01","03","获取业务员代码失败!");
			return false;
		}
		
		return true;
	}

	private boolean dealData() {
		String querySql = "select (select codename from ldcode where codetype = 'agentstate' and code = laagent.agentstate),"
				+ "name,mobile,"
				+ "(select name from ldcom where comcode=laagent.managecom) "
				+ "from laagent where agentcode = '" + this.agentCode + "' with ur";
		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(querySql);
		if (null == tSSRS || tSSRS.MaxRow==0 || tSSRS.MaxRow>1) {
			responseOME=buildResponseOME("01","02","查询业务员信息失败!");
			return false;
		}else if(tSSRS.getMaxRow()==1){
			this.agentState = tSSRS.GetText(1, 1);
			this.agentName = tSSRS.GetText(1, 2);
			this.agentMobile = tSSRS.GetText(1, 3);
			this.agentManageCom = tSSRS.GetText(1, 4);
		}
		
		return true;
	}

	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("LIS003")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BatchNo")) {
							this.batchNo = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendDate")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendTime")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("BranchCode")) {
							this.branchCode = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendOperator")) {
							this.sendOperator = child_element.getText();
							continue;
						}
						// 业务员信息
						if (child_element.getLocalName().equals("AgentInfo")) {
							Iterator childVerify = child_element.getChildren();
							
							while (childVerify.hasNext()) {
								OMElement child_element_verify = (OMElement) childVerify
										.next();

								if (child_element_verify.getLocalName().equals("ITEM")) {
									Iterator childItem = child_element_verify.getChildren();
									while (childItem.hasNext()) {
										OMElement child_item = (OMElement) childItem.next();

										if (child_item.getLocalName().equals("AgentCode")) {
											AgentCodeTransformation a = new AgentCodeTransformation();
											if(!a.AgentCode(child_item.getText(), "N")){
												System.out.println(a.getMessage());
											}
											this.agentCode = a.getResult();
											
										}
									}
								}
							}
							
						}
						
					}

				} else {
					System.out.println("报文类型不匹配");
					responseOME=buildResponseOME("01","01","报文类型不匹配");
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME=buildResponseOME("01","01","加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement("LIS003", null);
		OMElement agentInfo = fac.createOMElement("AgentInfo", null);
		OMElement item = fac.createOMElement("ITEM", null);
		addOm(DATASET, "BatchNo", this.batchNo);
		addOm(DATASET, "SendDate", PubFun.getCurrentDate());
		addOm(DATASET, "SendTime", PubFun.getCurrentTime());
		addOm(DATASET, "BranchCode", "WX");
		addOm(DATASET, "SendOperator", "WX");
		addOm(DATASET, "State", state);
		addOm(DATASET, "ErrCode", errorCode);
		addOm(DATASET, "ErrInfo", errorInfo);
		
		addOm(item, "AgentCode",this.agentCode);
		addOm(item, "State", this.agentState);
		addOm(item, "Name", this.agentName);
		addOm(item, "Mobile", this.agentMobile);
		addOm(item, "ManageCom", this.agentManageCom);
		
		agentInfo.addChild(item);
		DATASET.addChild(agentInfo);
		
		return DATASET;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
}
