package com.ecwebservice.subinterface;

import java.util.Iterator;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.f1j.mvc.el;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class QueryPayInfo {
	private OMElement mOME = null;

	private String batchNo = null;// 批次号

	private String branchCode = "";// 报送单位

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String sendOperator = null; // 报送人员

	private String contNo = "";

	private String AppntNo = ""; // 投保人客户号

	private String AppntName = ""; // 投保人姓名

	private String AppntIdType = ""; // 投保人证件类型

	private String AppntIdNo = ""; // 投保人证件号码
	
	private String AppntSex = ""; // 投保人性别

	private String PayMode = ""; // 交费方式

	private String BankName = ""; // 开户银行

	private String BankAccNo = ""; // 银行账户

	private String AccountName = ""; // 账户所有人姓名

	private String AccountIDType = ""; // 账户所有人证件类型

	private String AccountIDNo = ""; // 账户所有人证件号

	private String RiskPeriod = ""; // 长短期限标识

	private String RNewFlag = ""; // 续保

	private OMElement responseOME = null;

	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if (!load()) {
			return responseOME;
		}

		if (!check()) {
			return responseOME;
		}

		if (!dealData()) {
			responseOME = buildResponseOME("01", "03", "没有查询到客户的保单信息");
			return responseOME;
		}
		responseOME = buildResponseOME("00", "", "");
		return responseOME;
	}

	private boolean dealData() {
		// 查询保单信息
		String contSql = "select * from lccont where contno='" + this.contNo
				+ "' and stateflag='1'";
		SSRS cSSRS = new SSRS();
		cSSRS = new ExeSQL().execSQL(contSql);
		if (null == cSSRS || cSSRS.MaxRow == 0 || cSSRS.MaxRow > 1) {
			responseOME = buildResponseOME("01", "02", "查询保单信息失败!");
			return false;
		} else if (cSSRS.getMaxRow() == 1) {
			
			
			
			if (cSSRS.GetText(1, 20) == null || "".equals(cSSRS.GetText(1, 20))) {
				this.AppntNo = "";
			} else {
				this.AppntNo = cSSRS.GetText(1, 20).trim();
			}
			if (cSSRS.GetText(1, 21) == null || "".equals(cSSRS.GetText(1, 21))) {
				this.AppntName = "";
			} else {
				this.AppntName = cSSRS.GetText(1, 21).trim();
			}
			//投保人性别
			if (cSSRS.GetText(1, 22) == null || "".equals(cSSRS.GetText(1, 22))) {
				this.AppntSex = "";
			} else {
				this.AppntSex = cSSRS.GetText(1, 22).trim();
			}


			// 判断投保人证件类型
			if (cSSRS.GetText(1, 24) == null || "".equals(cSSRS.GetText(1, 24))) {
				System.out.println("投保人证件类型为空");
				this.AppntIdType = "";
			} else {
				String idType = cSSRS.GetText(1, 24).trim();
				String idSql = "select codename from ldcode where code ='"
						+ idType + "' and codetype='idtype' ";
				String IdType = new ExeSQL().getOneValue(idSql);
				if (IdType == null || "".equals(IdType)) {
					this.AppntIdType = "";
				} else {
					this.AppntIdType = IdType.trim();
				}
			}
			if (cSSRS.GetText(1, 25) == null || "".equals(cSSRS.GetText(1, 25))) {
				this.AppntIdNo = "";
			} else {
				this.AppntIdNo = cSSRS.GetText(1, 25).trim();
			}
			if (cSSRS.GetText(1, 43) == null || "".equals(cSSRS.GetText(1, 43))) {
				this.BankAccNo = "";
			} else {
				this.BankAccNo = cSSRS.GetText(1, 43).trim();
			}
			if (cSSRS.GetText(1, 44) == null || "".equals(cSSRS.GetText(1, 44))) {
				this.AccountName = "";
			} else {
				this.AccountName = cSSRS.GetText(1, 44).trim();
				if (AccountName.equals(cSSRS.GetText(1, 21).trim())) {
					this.AccountIDType = this.AppntIdType;
					this.AccountIDNo = cSSRS.GetText(1, 25).trim();
				} else {
					this.AccountIDType = "";
					this.AccountIDNo = "";
				}
			}
		}
		// 查询银行信息
		String BankSql = "select bankname from db2inst1.ldbank where bankcode="
				+ "(select bankcode from lccont where contno='" + this.contNo
				+ "') ";
		String bankStr = new ExeSQL().getOneValue(BankSql);
		if (bankStr == null || "".equals(bankStr)) {
			this.BankName = "";
		} else {
			this.BankName = bankStr.trim();
		}
		// 查询长短期限标识
		String rsql = "select distinct riskperiod from lmriskapp where riskcode in "
				+ "(select riskcode from lcpol where contno='"
				+ this.contNo
				+ "')";
		String riskStr = new ExeSQL().getOneValue(rsql);
		if (riskStr == null || "".equals(riskStr)) {
			this.RiskPeriod = "";
		} else {
			this.RiskPeriod = riskStr.trim();
		}
		// 查询续保
		String xusSql = "select distinct rnewflag from lmrisk  where riskcode in "
				+ "(select riskcode from lcpol where contno='"
				+ this.contNo
				+ "')";
		String xuStr = new ExeSQL().getOneValue(xusSql);
		if (xuStr == null || "".equals(xuStr)) {
			this.RNewFlag = "";
		} else {
			this.RNewFlag = xuStr.trim();
		}
		// 查询交费方式
		String ModeSql = "select codename from ldcode where code in "
				+ "(select paymode from lccont where contno='" + this.contNo
				+ "') " + "and codetype='paymode' ";
		String modeStr = new ExeSQL().getOneValue(ModeSql);
		if (modeStr == null || "".equals(modeStr)) {
			this.PayMode = "";
		} else {
			this.PayMode = modeStr.trim();
		}
		return true;
	}

	private boolean check() {

		if ((this.contNo == null || this.contNo == "")) {
			responseOME = buildResponseOME("01", "03", "获取保单号失败");
			return false;
		}
		return true;
	}

	/**
	 * 加载报文
	 * 
	 * @return
	 */
	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("PayMode")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BatchNo")) {
							this.batchNo = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendDate")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendTime")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("BranchCode")) {
							this.branchCode = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendOperator")) {
							this.sendOperator = child_element.getText();
							continue;
						}

						if (child_element.getLocalName().equals("QueryInfo")) {
							Iterator childVerify = child_element.getChildren();
							while (childVerify.hasNext()) {
								OMElement child_element_verify = (OMElement) childVerify
										.next();

								if (child_element_verify.getLocalName().equals(
										"ITEM")) {
									Iterator childItem = child_element_verify
											.getChildren();
									while (childItem.hasNext()) {
										OMElement child_item = (OMElement) childItem
												.next();

										if (child_item.getLocalName().equals(
												"ContNo")) {
											this.contNo = child_item.getText();
										}
									}
								}
							}
						}
					}

				} else {
					System.out.println("报文类型不匹配");
					responseOME = buildResponseOME("01", "01", "报文类型不匹配");
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME = buildResponseOME("01", "01", "加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement PayMode = fac.createOMElement("PayMode", null);
		addOm(PayMode, "BatchNo", this.batchNo);
		addOm(PayMode, "SendDate", PubFun.getCurrentDate());
		addOm(PayMode, "SendTime", PubFun.getCurrentTime());
		addOm(PayMode, "BranchCode", this.branchCode);
		addOm(PayMode, "SendOperator", this.sendOperator);
		addOm(PayMode, "State", state);
		addOm(PayMode, "ErrCode", errorCode);
		addOm(PayMode, "ErrInfo", errorInfo);

		OMElement QueryInfo = fac.createOMElement("QueryInfo", null);
		OMElement item = fac.createOMElement("ITEM", null);
		addOm(item, "AppntNo", this.AppntNo);
		addOm(item, "AppntName", this.AppntName);
		addOm(item, "AppntIdType", this.AppntIdType);
		addOm(item, "AppntIdNo", this.AppntIdNo);
		addOm(item, "AppntSex", this.AppntSex);
		addOm(item, "PayMode", this.PayMode);
		addOm(item, "BankName", this.BankName);
		addOm(item, "BankAccNo", this.BankAccNo);
		addOm(item, "AccountName", this.AccountName);
		addOm(item, "AccountIDType", this.AccountIDType);
		addOm(item, "AccountIDNo", this.AccountIDNo);
		addOm(item, "RiskPeriod", this.RiskPeriod);
		addOm(item, "RNewFlag", this.RNewFlag);
		QueryInfo.addChild(item);
		PayMode.addChild(QueryInfo);
		return PayMode;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
}
