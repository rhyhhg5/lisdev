package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.tb.GroupContDeleteUI;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class DealGrpContDelete {

	public CErrors mErrors = new CErrors();
	private GlobalInput gl = new GlobalInput();
	private OMElement oml = null;
	private String batchno = null;
	private String managecom = null;
	private String sendDate = null;
	private String sendTime = null;
	private String messageType = "";
	private OMElement responseOME = null;
	private String mprtno = null;
	
	private VData mresult = new VData();
	private String theCurrentdate = PubFun.getCurrentDate();
	private String theCurrenttime = PubFun.getCurrentTime();
	private LoginVerifyTool logv = new LoginVerifyTool();
	private LCGrpContSchema lcgrpSchema = new LCGrpContSchema();
    private PubSubmit  pubsubmit =new PubSubmit();
	public OMElement queryData(OMElement omes) {
		this.oml = omes.cloneOMElement();
		System.out.println("aa :  " + oml);
		if (!load()) {
			responseOME = buildResponseOME("01", "07", "加载团单数据失败");
			System.out.println("responseOME" + responseOME);
			return responseOME;
		}
		if (!check()) {
			return responseOME;
		}
		System.out.println("删除新单开始，Prtno为：" + mprtno);
		gl.Operator = "EC_WX";
		gl.ManageCom = managecom;
		System.out.println(gl.ManageCom);
		System.out.println(gl.Operator);
		ExeSQL tExeSQL = new ExeSQL();
		String ySql = "select  grpcontno  from  lcgrpcont   where  prtno='"
				+ mprtno + "'";
		SSRS tssrs = tExeSQL.execSQL(ySql);
		lcgrpSchema.setGrpContNo(tssrs.GetText(1, 1));
		System.out.println(lcgrpSchema.getGrpContNo());
		VData vdata = new VData();
		vdata.add(lcgrpSchema);
		vdata.add(gl);
		GroupContDeleteUI grpui = new GroupContDeleteUI();
		if (!grpui.submitData(vdata, "DELETE")) {
			String errors = grpui.mErrors.getFirstError();
			System.out.println(errors);
			responseOME = buildResponseOME("01", "06", "errors");
			System.out.println("response  " + responseOME);
			return responseOME;
		} else {
			responseOME = buildResponseOME("00", "09", "成功!");
			System.out.println("删除新单结束");
			System.out.println("response  " + responseOME);
			return responseOME;
		}
	}

	private boolean load() {
		try {
			Iterator iter = this.oml.getChildren();
			while (iter.hasNext()) {
				OMElement om = (OMElement) iter.next();
				System.out.println("test     :   " + om.getLocalName());
				if (om.getLocalName().equals("GRPDELETECONT")) {
					Iterator child_elementt = om.getChildren();
					while (child_elementt.hasNext()) {
						OMElement child_element = (OMElement) child_elementt
								.next();
						if (child_element.getLocalName().equals("BATCHNO")) {
							this.batchno = child_element.getText();
							System.out.println("batchno      :" + batchno);
							continue;
						}
						if (child_element.getLocalName().equals("MANAGECOM")) {
							this.managecom = child_element.getText();
							System.out.println("managecom  " + managecom);
							continue;
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							this.messageType = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("PRINTNO")) {
							this.mprtno = child_element.getText();
							// System.out.println(mprtno);
							this.lcgrpSchema.setPrtNo(child_element.getText());
							System.out.println("secane    "
									+ lcgrpSchema.getPrtNo());
							continue;
						}
					}

				} else {
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		} catch (Exception ex) {
			System.out.println("报文加载异常");
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean check() {
		if (!checkInfo()) {
			System.out.println("校验信息失败");
			return false;
		}
		return true;
	}

	private boolean checkInfo() {
		if (this.batchno == null || this.batchno == "") {
			responseOME = buildResponseOME("01", "02", "没有得到批次号");
			System.out.println("response " + responseOME);
			return false;
		}
		if (this.mprtno == null || this.mprtno == "") {
			responseOME = buildResponseOME("01", "03", "印刷号为空");
			System.out.println("response" + responseOME);
			return false;
		}
		if (!this.messageType.equals("DE0001")) {
			responseOME = buildResponseOME("01", "07", "删除类型错误");
			System.out.println("response  " + responseOME);
			return false;
		}
		ExeSQL tExeSQL = new ExeSQL();

		String sql1 = "select  1  from  lcgrpcont  where prtno='" + mprtno
				+ "' and managecom='" + managecom + "'";
		SSRS tSSRS1 = tExeSQL.execSQL(sql1);
		if (tSSRS1.getMaxRow()!=1 ) {
			responseOME = buildResponseOME("01", "04", "该保单查询失败");
			System.out.println("responserOMe" + responseOME);
			return false;
		}


		String strSql = "select  1 from lcgrpcont  where prtno='" + mprtno
				+ "' and appflag='1' ";
		SSRS tSSRS2 = tExeSQL.execSQL(strSql);
		if (tSSRS2.getMaxRow() > 0) {
			responseOME = buildResponseOME("01", "06", "已签单，不能进行新单删除");
			System.out.println("responseOME" + responseOME);
			return false;
		}
		
		String tSql = "select 1 from ljtempfee where otherno='"
				+ mprtno
				+ "' "
				+ "and tempfeeno not in (select tempfeeno from LJAGetTempFee) and enteraccdate is not null";
		SSRS tSSRS = tExeSQL.execSQL(tSql);
		if (tSSRS.getMaxRow() > 0) {
			responseOME = buildResponseOME("01", "05", "财务已缴费，不能进行新单删除");
			System.out.println("responseOME" + responseOME);
			return false;
		}

		return true;
	}

	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement("GRPDELETECONT", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", theCurrentdate);
		tool.addOm(DATASET, "SENDTIME", theCurrenttime);
		tool.addOm(DATASET, "PrtNo", lcgrpSchema.getPrtNo());
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", "1");
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {
		try {
			// XMLStreamReader parser =
			// XMLInputFactory.newInstance().createXMLStreamReader( new
			// FileInputStream("F:\\netsale.xml"));
			// StAXOMBuilder builder = new StAXOMBuilder(parser);
			// OMElement root=builder.getDocumentElement();

			OMElement root = buildQueryData();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMElement HYXLOGIN = factory.createOMElement("GRPDELETECONT", null);

			addOm(HYXLOGIN, "BATCHNO", "2015841816249574915");
			addOm(HYXLOGIN, "MANAGECOM", "86430000");
			addOm(HYXLOGIN, "SENDDATE", "2015-08-04");
			addOm(HYXLOGIN, "SENDTIME", "11:15:48");
			addOm(HYXLOGIN, "MESSAGETYPE", "DE0001");
			addOm(HYXLOGIN, "PRINTNO", "1E000001826");
			// DE0001 15000001961
			root.addChild(HYXLOGIN);
			DealGrpContDelete deald = new DealGrpContDelete();

			deald.queryData(root);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}

	public static OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		Om.buildNext();
		return Om;
	}

}