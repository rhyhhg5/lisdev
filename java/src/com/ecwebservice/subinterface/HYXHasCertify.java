package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class HYXHasCertify {
	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 交易类型

	private String sendOperator = null;// 操作员

	private String agentCode = null;  //代理人或代理机构编码

	private String productID = null;  //产品编码

	private String ffprintID = null;  //印刷号上限

	private String ttprintID = null;  //印刷号下限
	
	private String name = null;  //被保人姓名
	
	private String startDate = null;  //开始日期
	
	private String endDate = null;  //结束日期
	
	private String tprintID = null;  //组建报文印刷号
	
	private String tname = null;  //组建报文被保人姓名
	
	private String tactiveDate = null;  //组建报文激活日期

	/** 当前页码*/
	private String pageIndex = null;
	
	/** 每页记录条数*/
	private String pageSize = null;
	
	private String rMaxrow="";  //最大记录数

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("HYXHASCERTIFY")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);

						}
						// 当前页
						if (child_element.getLocalName().equals("PAGEINDEX")) {
							pageIndex = child_element.getText();
							
							System.out.println(pageIndex);
						}
						// 分页大小
						if (child_element.getLocalName().equals("PAGESIZE")) {
							pageSize = child_element.getText();
							
							System.out.println(pageSize);
						}
						// 代理机构所属管理机构
						if (child_element.getLocalName().equals("AGENTCODE")) {
							agentCode = child_element.getText();
							AgentCodeTransformation a = new AgentCodeTransformation();
							if(!a.AgentCode(agentCode, "N")){
								System.out.println(a.getMessage());
							}
							System.out.println(agentCode);
							agentCode = a.getResult();
						}
						// 产品编码
						if (child_element.getLocalName().equals("PRODUCTID")) {
							productID = child_element.getText();
							
							System.out.println(productID);
						}
						// 印刷号上限
						if (child_element.getLocalName().equals("FPRINTID")) {
							ffprintID = child_element.getText();

							System.out.println(ffprintID);
						}
						// 印刷号下限
						if (child_element.getLocalName().equals("TPRINTID")) {
							ttprintID = child_element.getText();
							
							System.out.println(ttprintID);
						}
						// 被保人姓名
						if (child_element.getLocalName().equals("NAME")) {
							name = child_element.getText();

							System.out.println(name);
						}
						// 出单开始日期
						if (child_element.getLocalName().equals("STARTDATE")) {
							startDate = child_element.getText();
							
							System.out.println(startDate);
						}
						// 出单结束日期
						if (child_element.getLocalName().equals("ENDDATE")) {
							endDate = child_element.getText();
							
							System.out.println(endDate);
						}
					}

					ExeSQL tExeSQL = new ExeSQL();
					//查询出最大记录数
					String maxSql =  "select count(1) from licertify lic inner join licardactiveinfolist licard on lic.cardno = licard.cardno " 
						+" where 1=1 and exists "
						+" (select 1 from lcgrpcont where grpcontno = lic.grpcontno and appflag='1')";
					StringBuffer maxBuffer = new StringBuffer(maxSql);
					if(agentCode!=null&&!agentCode.equals("")){
						maxBuffer.append(" and (lic.agentcode = '"+agentCode+"' or lic.agentCom = '"+agentCode+"')");
					}
					if(productID!=null&&!productID.equals("")&&!productID.equals("null")){
						String getCertifyCode = "select certifycode from lmcardrisk where riskcode = '"+productID+"'"
						+" and risktype = 'W'";
						SSRS certifyCodeSSRS = tExeSQL.execSQL(getCertifyCode);
						String certifyCode = "";
						if(certifyCodeSSRS.MaxRow==1){
							certifyCode = certifyCodeSSRS.GetText(1, 1); //单证编码
							maxBuffer.append(" and lic.certifycode = '"+certifyCode+"' ");
						}else{
							responseOME = buildResponseOME("01", "23", "对应多条单证编码！",null);
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,
									messageType, sendDate, sendTime, "01",
									sendOperator, "23", "对应多条单证编码！");
							return responseOME;
						}
					}
					if(ffprintID!=null&&!ffprintID.equals("")&&!"null".equals(ffprintID)){
						maxBuffer.append(" and licard.cardno >= '"+ffprintID+"' ");
					}
					if(ttprintID!=null&&!ttprintID.equals("")&&!"null".equals(ttprintID)){
						maxBuffer.append(" and licard.cardno <= '"+ttprintID+"' ");
					}
					if(name!=null&&!name.equals("")&&!"null".equals(name)){
						maxBuffer.append(" and licard.name like '%"+name+"%' ");
					}
					if(startDate!=null&&!startDate.equals("")&&!startDate.equals("null")){
						maxBuffer.append(" and licard.activedate >= '"+startDate+"' ");
					}
					if(endDate!=null&&!endDate.equals("")&&!"null".equals(endDate)){
						maxBuffer.append(" and licard.activedate <= '"+endDate+"' ");
					}
					this.rMaxrow=new ExeSQL().getOneValue(maxBuffer.toString());
					// 按条件查询已经结算完成的印刷号信息
					String isSignSql = "select licard.cardno,licard.name,licard.activedate from licertify lic inner join licardactiveinfolist licard on lic.cardno = licard.cardno " 
						+" where 1=1 and exists "
						+" (select 1 from lcgrpcont where grpcontno = lic.grpcontno and appflag='1')";
					StringBuffer sqlBuffer = new StringBuffer(isSignSql);
					if(agentCode!=null&&!agentCode.equals("")){
						sqlBuffer.append(" and (lic.agentcode = '"+agentCode+"' or lic.agentCom = '"+agentCode+"')");
					}
					if(productID!=null&&!productID.equals("")){
						String getCertifyCode = "select certifycode from lmcardrisk where riskcode = '"+productID+"'"
						+" and risktype = 'W'";
						SSRS certifyCodeSSRS = tExeSQL.execSQL(getCertifyCode);
						String certifyCode = "";
						if(certifyCodeSSRS.MaxRow==1){
							certifyCode = certifyCodeSSRS.GetText(1, 1); //单证编码
							sqlBuffer.append(" and lic.certifycode = '"+certifyCode+"' ");
						}else{
							responseOME = buildResponseOME("01", "23", "对应多条单证编码！",null);
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,
									messageType, sendDate, sendTime, "01",
									sendOperator, "23", "对应多条单证编码！");
							return responseOME;
						}
					}
					if(ffprintID!=null&&!ffprintID.equals("")&&!"null".equals(ffprintID)){
						sqlBuffer.append(" and licard.cardno >= '"+ffprintID+"' ");
					}
					if(ttprintID!=null&&!ttprintID.equals("")&&!"null".equals(ttprintID)){
						sqlBuffer.append(" and licard.cardno <= '"+ttprintID+"' ");
					}
					if(name!=null&&!name.equals("")&&!"null".equals(name)){
						sqlBuffer.append(" and licard.name like '%"+name+"%' ");
					}
					if(startDate!=null&&!startDate.equals("")&&!"null".equals(startDate)){
						sqlBuffer.append(" and licard.activedate >= '"+startDate+"' ");
					}
					if(endDate!=null&&!endDate.equals("")&&!"null".equals(endDate)){
						sqlBuffer.append(" and licard.activedate <= '"+endDate+"' ");
					}
					StringBuffer pagebuf = new StringBuffer();
					pagebuf.append("select * from (select rownumber() over() as rowid, aa.* from (");
					pagebuf.append(sqlBuffer.toString());
					pagebuf.append(") as aa ");
					pagebuf.append(") as tmp where 1 = 1 ");
					if(this.pageIndex ==null || this.pageIndex.equals("") || this.pageSize ==null || this.pageSize.equals("")){
						System.out.println("没有得到分页信息");
						responseOME = buildResponseOME("01", "23", "没有得到分页信息！",null);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								sendOperator, "23", "没有得到分页信息！");
						return responseOME;
					}
					pagebuf.append("and rowid >= (").append(this.pageIndex).append("-1) *").append(this.pageSize).append(" + 1 ");
					pagebuf.append("and rowid <= ").append(this.pageIndex).append("*").append(this.pageSize).append(" with ur");
					SSRS sResult = tExeSQL.execSQL(pagebuf.toString());
					OMFactory fac = OMAbstractFactory.getOMFactory();
					OMElement INFOLIST = fac.createOMElement("INFOLIST", null);
					if (sResult.MaxRow > 0) {
						for(int i=1;i<=sResult.MaxRow;i++){
							this.tprintID = sResult.GetText(i, 2);
							this.tname =sResult.GetText(i, 3);
							this.tactiveDate =sResult.GetText(i, 4);
							OMElement subItem =buildResponseItem();
							INFOLIST.addChild(subItem);
						}
						responseOME = buildResponseOME("00","","",INFOLIST);
						return responseOME;
					} else {
						responseOME = buildResponseOME("01", "23", "没有已经结算完成的印刷号！",null);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,
								messageType, sendDate, sendTime, "01",
								sendOperator, "23", "没有已经结算完成的印刷号！");
						return responseOME;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("01", "4", "发生异常",null);
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", sendOperator, "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	/**
	 * 创建返回结点
	 * @param state
	 * @param errCode
	 * @param errInfo
	 * @return OMElement
	 */
	private OMElement buildResponseItem() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tool = new LoginVerifyTool();

		OMElement ITEM = fac.createOMElement("ITEM", null);
		tool.addOm(ITEM, "PRINTID", tprintID);
		tool.addOm(ITEM, "NAME", tname);
		tool.addOm(ITEM, "ACTIVEDATE", tactiveDate);		
		return ITEM;
	}
	// 构建返回xml

	private OMElement buildResponseOME(String state, String errCode,
			String errInfo,OMElement infoList) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("HYXHASCERTIFY", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		
		if(state.equals("00")){
			tool.addOm(DATASET, "TOTALROWNUM", this.rMaxrow);
			OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
			OUTPUTDATA.addChild(infoList);
			DATASET.addChild(OUTPUTDATA);
		}
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {
		HYXHasCertify hyx = new HYXHasCertify();
//		hyx.queryData(Oms);
	}
}
