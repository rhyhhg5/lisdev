package com.ecwebservice.subinterface;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.log.LogInfoDeal;
import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFTransLogSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-1-29 下午04:18:32 Description:卡单激活，远程出单通用方法
 * @version
 */

public class LoginVerifyTool {
	private static String LogFilePath;

	private static String LogFileName;

	private static LogInfoDeal t;

	public OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}

	// 生成投保人信息表中的AppntNo
	public String getAppntNo() {

		// 得到投保人编码（当前时间加五位随机码）
		String sAppntNo = getCurrentDateTime("yyyyMMddHHmmss");
		for (int k = 0; k < 5; k++) {
			sAppntNo = sAppntNo + (int) (Math.random() * 10);
		}

		return sAppntNo;
	}

	// 生成被保人流水号 new Modify
	public String getInsuNo(String cardNo) {
		String sql = "select COALESCE(max(CAST (InsuNo AS Integer))+1,1) from wfcontlist where cardno='"
				+ cardNo + "'";
		ExeSQL tExeSQL = new ExeSQL();
		String insuNo = tExeSQL.getOneValue(sql);
		return insuNo;
	}

	// serialNo
	public String getSerialNo() {

		// 得到流水号（当前时间加五位随机码）
		String serialNo = getCurrentDateTime("yyyyMMddHHmmss");
		for (int k = 0; k < 5; k++) {
			serialNo = serialNo + (int) (Math.random() * 10);
		}

		return serialNo;
	}

	/**
	 * 得到当前系统时间日期，自定义格式
	 * 
	 * @return 当前日期,参考格式为"yyyy-MM-dd HH:mm:ss E 本月第F个星期"
	 */

	public static String getCurrentDateTime(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		Date today = new Date();
		String tString = df.format(today);
		return tString;
	}

	// 被保人编号唯一性校验
	public boolean isValidInsuNo(String insuNo) {
		String sql = "select count(*) from WFINSULIST where insuno='" + insuNo
				+ "'";
		ExeSQL tExeSQL = new ExeSQL();
		int count = Integer.parseInt(tExeSQL.getOneValue(sql));
		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}

	// 生成日志1
	public static void writeEnterLog(String IP, OMElement element) {

		try {
			t = new LogInfoDeal("jianan", "02");
			String logTxt = "[IP：" + IP + "]" + "开始调用服务类ServerInterface."
					+ System.getProperty("line.separator") + "发送报文信息："
					+ element.toString() + System.getProperty("line.separator");
			LogFilePath = t.getLogFilePath();
			LogFileName = t.getLogFileName();
			t.WriteLogTxt(logTxt);

		} catch (Exception ex) {
			System.out.print("11" + ex.getMessage());
		}

	}

	// 生成日志2
	public static void writeTransTypeLog(String transName) {

		try {

			String logTxt = "[交易类型：" + transName + "]"
					+ System.getProperty("line.separator");

			t.WriteLogTxt(LogFilePath, LogFileName, logTxt);

		} catch (Exception ex) {
			System.out.print("11" + ex.getMessage());
		}

	}

	// 生成日志3
	public static void writeLoginLog(String certainNo, String password) {

		try {

			String logTxt = "[查询条件：号码-" + certainNo + "  密码-" + password + "]"
					+ System.getProperty("line.separator");

			t.WriteLogTxt(LogFilePath, LogFileName, logTxt);

		} catch (Exception ex) {
			System.out.print("11" + ex.getMessage());
		}

	}

	// 生成日志3
	public static void writeBasicInfoLog(String cardNo, String appntNo,
			String insuNo) {

		try {

			String logTxt = "基本信息：卡单号码-" + cardNo + "  投保人号码-" + appntNo
					+ "  被保人号码-" + insuNo
					+ System.getProperty("line.separator");

			t.WriteLogTxt(LogFilePath, LogFileName, logTxt);

		} catch (Exception ex) {
			System.out.print("11" + ex.getMessage());
		}

	}

	// 生成日志4
	public static void writeLog(OMElement responseOME, String batchNo,
			String messageType, String sendDate, String sendTime,
			String batchFlag, String opertator, String errCode, String errInfo) {
		String logTxt = "返回报文信息：" + responseOME.toString()
				+ System.getProperty("line.separator") + "[批次号：" + batchNo
				+ "][批次交易类型：" + messageType + "]" + "[交易日期" + sendDate
				+ "][交易时间：" + sendTime + "][批次交易状态：" + batchFlag + "]"
				+ "[操作员：" + opertator + "]"
				+ System.getProperty("line.separator") + "			[错误编码：" + errCode
				+ "][错误信息：" + errInfo + "]"
				+ System.getProperty("line.separator");
		try {
			// LogInfoDeal t = new LogInfoDeal("jianan", "02");
			t.WriteLogTxt(LogFilePath, LogFileName, logTxt);
			// t.WriteLogTxt(logTxt);

		} catch (Exception ex) {
			System.out.print("11" + ex.getMessage());
		}

		MMap map = new MMap();
		VData tVData = new VData();
		// 错误处理类
		CErrors mErrors = new CErrors();
		WFTransLogSchema trransLogSchema = new WFTransLogSchema();
		trransLogSchema.setBatchNo(batchNo);
		trransLogSchema.setMessageType(messageType);
		trransLogSchema.setSendDate(sendDate);
		trransLogSchema.setSendTime(sendTime);
		trransLogSchema.setBatchFlag(batchFlag);
		trransLogSchema.setopertator(opertator);
		trransLogSchema.setErrCode(errCode);
		trransLogSchema.setErrInfo(errInfo);

		map.put(trransLogSchema, "DELETE&INSERT");

		// 入库
		PubSubmit tPubSubmit = new PubSubmit();
		tVData.add(map);
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			// @@错误处理
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LoginVerifyTool";
			tError.functionName = "writeLog";
			tError.errorMessage = "数据提交失败!";
			mErrors.addOneError(tError);

		}
	}

	public WFContListSchema getWFContListSchemaByCardNo(String cardNo) {

		WFContListSchema contListSchema;
		WFContListDB contListDB = new WFContListDB();
		contListDB.setCardNo(cardNo);
		contListSchema = contListDB.query().get(1);
		return contListSchema;

	}

	// 根据编码获取性别
	public String getChSex(String sexCode) {
		String sex = null;
		if (sexCode.equals("0")) {
			sex = "男";
		} else if (sexCode.equals("1")) {
			sex = "女";
		} else {
			sex = "其他";
		}
		return sex;
	}

	// 对性别进行编码
	public String codeSex(String sex) {
		if ("男".equals(sex)) {
			return "0";
		} else if ("女".equals(sex)) {
			return "1";
		} else {
			return sex;
		}
	}

	// 供入库使用
	public String codeIDType(String IDType) {
		if (IDType.equals("8")) {
			return "4";
		} else {
			return IDType;
		}
	}

	// 供查询交易使用
	public String queryIDType(String IDType) {
		if (IDType.equals("4")) {
			return "8";
		} else {
			return IDType;
		}
	}

	// batchNo唯一性校验
	public boolean isValidBatchNo(String batchNo) {
		String sql = "select count(*) from WFContList where BatchNo='"
				+ batchNo + "'";
		ExeSQL tExeSQL = new ExeSQL();
		int count = Integer.parseInt(tExeSQL.getOneValue(sql));
		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}

	// cardNo唯一性校验
	public boolean isValidcardNo(String cardNo) {
		String sql = "select count(*) from WFContList where cardNo='" + cardNo
				+ "'";
		ExeSQL tExeSQL = new ExeSQL();
		int count = Integer.parseInt(tExeSQL.getOneValue(sql));
		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}

	//
	public String codeToAppntRela(String ToAppntRela) {
		if ("Self".equals(ToAppntRela)) {
			return "00";
		}
		if ("Spouse".equals(ToAppntRela)) {
			return "01";
		}
		if ("Parent".equals(ToAppntRela)) {
			return "02";
		}
		if ("Sibling".equals(ToAppntRela)) {
			return "03";
		}
		if ("Family".equals(ToAppntRela)) {
			return "04";
		}
		if ("Child".equals(ToAppntRela)) {
			return "05";
		}
		if ("GrandParent".equals(ToAppntRela)) {
			return "06";
		}
		if ("GrandChild".equals(ToAppntRela)) {
			return "07";
		}
		if ("LegalGuard".equals(ToAppntRela)) {
			return "08";
		}
		if ("Other".equals(ToAppntRela)) {
			return "09";
		}

		return ToAppntRela;
	}

	// 转换 InsuYearFlag
	public String switchInsuYearFlag(String InsuYearFlag) {
		if ("Y".equalsIgnoreCase(InsuYearFlag)) {
			return "年";
		}
		if ("M".equalsIgnoreCase(InsuYearFlag)) {
			return "个月";
		}
		if ("D".equalsIgnoreCase(InsuYearFlag)) {
			return "天";
		}
		if ("A".equalsIgnoreCase(InsuYearFlag)) {
			return "岁";
		}
		return "未知";
	}

	// 根据卡单号 查询套餐编码
	/***************************************************************************
	 * 返回数组中 0：套餐编码 1：LZCard.CertifyCode
	 */
	public String[] getRiskSetCodeByCardNo(String cardNo) {
		String[] Code = new String[2];
		String riskSetCodeSql = "select  lmcr.riskcode,re.CertifyCode from lmcardrisk lmcr,"
				+ "(select lzcn.CardNo, lzc.CertifyCode, lzc.State from LZCardNumber lzcn "
				+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
				+ " and lzc.EndNo = lzcn.CardSerNo where 1 = 1 and lzcn.CardNo='"
				+ cardNo
				+ "' ) re "
				+ "where lmcr.risktype ='W' and lmcr.certifycode=re.certifycode";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS codeSSRS = tExeSQL.execSQL(riskSetCodeSql);
		// 套餐编码
		String riskSetCode = null;
		String certifyCode = null;
		if (codeSSRS.MaxRow > 0) {
			riskSetCode = codeSSRS.GetText(1, 1);
			certifyCode = codeSSRS.GetText(1, 2);
		}

		Code[0] = riskSetCode;
		Code[1] = certifyCode;

		return Code;

	}

	// 从核心获取保障期间信息 参数为套餐编码 copys mult 返回数据中 0：insuYear 1:insuYearFlag
	//外网查询使用（此时已生成中间表数据，不用传mult值）
	public String[] getInsuInfoFromCore(String cardNo, String riskSetCode) {
		String insuYearInfo[] = new String[2];
		String InsuYear = null;
		String InsuYearFlag = null;
		Map insuYearInfoMap = new HashMap();

		ExeSQL tExeSQL = new ExeSQL();
		String dutycodeSql = "select dutycode from LDRiskDutywrap "
				+ "where riskwrapcode = '"
				+ riskSetCode
				+ "' and calfactor in ('InsuYear', 'InsuYearFlag') fetch first 1 rows only with ur";
		String dutycode = tExeSQL.getOneValue(dutycodeSql);
		if (dutycode != null && !"".equals(dutycode)) {
			// System.out.println("责任编码为："+dutycode);
			String insuInfoSql = "select calfactor, calfactortype, calfactorvalue, calsql from LDRiskDutywrap where riskwrapcode = '"
					+ riskSetCode
					+ "' "
					+ "and calfactor in ('InsuYear', 'InsuYearFlag') and dutycode = '"
					+ dutycode + "' with ur";
			SSRS insuInfoSSRS = new SSRS();
			insuInfoSSRS = tExeSQL.execSQL(insuInfoSql);
			for (int i = 1; i <= insuInfoSSRS.MaxRow; i++) {
				String calFactor = insuInfoSSRS.GetText(i, 1);
				String calFactorValue = insuInfoSSRS.GetText(i, 3);

				String tCalFactorType = insuInfoSSRS.GetText(i, 2);
				if ("2".equalsIgnoreCase(tCalFactorType)) {
					if("D31037".equals(riskSetCode)){
						// 根据sql查询
						PubCalculator tCal = new PubCalculator();
						
						String tCalSql = insuInfoSSRS.GetText(i, 4);
						System.out.println("CalSql:" + tCalSql);
						tCal.setCalSql(tCalSql);
						
						// tCal.addBasicFactor("RiskCode", tTmpRiskCode);
						
						// tCal.addBasicFactor("Copys", String.valueOf(tCopys));
						String insuyearSql = "select insuyear from licardactiveinfolist where cardNo='"
								+ cardNo + "'";
						String insuyear = tExeSQL.getOneValue(insuyearSql);
//						tCal.addBasicFactor("Mult", mult);
						
//						String tTmpCalResult = tCal.calculate();
						String tTmpCalResult = insuyear;
						calFactorValue = tTmpCalResult;
						
						System.out.println("查询结果：" + tTmpCalResult);
						
						if (tTmpCalResult == null || tTmpCalResult.equals("")) {
							System.out.println("要素计算错误");
							
						}
					}else{
						// 根据sql查询
						PubCalculator tCal = new PubCalculator();
						
						String tCalSql = insuInfoSSRS.GetText(i, 4);
						System.out.println("CalSql:" + tCalSql);
						tCal.setCalSql(tCalSql);
						
						// tCal.addBasicFactor("RiskCode", tTmpRiskCode);
						
						// tCal.addBasicFactor("Copys", String.valueOf(tCopys));
						String multSql = "select mult from licardactiveinfolist where cardNo='"
								+ cardNo + "'";
						String mult = tExeSQL.getOneValue(multSql);
						tCal.addBasicFactor("Mult", mult);
						
						String tTmpCalResult = tCal.calculate();
						
						calFactorValue = tTmpCalResult;
						
						System.out.println("查询结果：" + tTmpCalResult);
						
						if (tTmpCalResult == null || tTmpCalResult.equals("")) {
							System.out.println("要素计算错误");
							
						}
					}

				}

				insuYearInfoMap.put(calFactor, calFactorValue);
			}
			InsuYear = (String) insuYearInfoMap.get("InsuYear");
			InsuYearFlag = (String) insuYearInfoMap.get("InsuYearFlag");
		}
		if("D31037".equals(riskSetCode)){
			InsuYear = InsuYear;
		}else{
			InsuYear = dealLeapYear(InsuYear,InsuYearFlag,cardNo);
		}
		insuYearInfo[0] = InsuYear;
		insuYearInfo[1] = InsuYearFlag;
		return insuYearInfo;

	}
	//	1、校验份数时使用 2、新单保存使用（包括激活卡、远程出单、明亚、保尔和第三方承保）
	public String[] getInsuInfoFromCore(String cardNo, String riskSetCode,String mult) {
		String insuYearInfo[] = new String[2];
		String InsuYear = null;
		String InsuYearFlag = null;
		Map insuYearInfoMap = new HashMap();

		ExeSQL tExeSQL = new ExeSQL();
		String dutycodeSql = "select dutycode from LDRiskDutywrap "
				+ "where riskwrapcode = '"
				+ riskSetCode
				+ "' and calfactor in ('InsuYear', 'InsuYearFlag') fetch first 1 rows only with ur";
		String dutycode = tExeSQL.getOneValue(dutycodeSql);
		if (dutycode != null && !"".equals(dutycode)) {
			// System.out.println("责任编码为："+dutycode);
			String insuInfoSql = "select calfactor, calfactortype, calfactorvalue, calsql from LDRiskDutywrap where riskwrapcode = '"
					+ riskSetCode
					+ "' "
					+ "and calfactor in ('InsuYear', 'InsuYearFlag') and dutycode = '"
					+ dutycode + "' with ur";
			SSRS insuInfoSSRS = new SSRS();
			insuInfoSSRS = tExeSQL.execSQL(insuInfoSql);
			for (int i = 1; i <= insuInfoSSRS.MaxRow; i++) {
				String calFactor = insuInfoSSRS.GetText(i, 1);
				String calFactorValue = insuInfoSSRS.GetText(i, 3);

				String tCalFactorType = insuInfoSSRS.GetText(i, 2);
				if ("2".equalsIgnoreCase(tCalFactorType)) {
					// 根据sql查询
					PubCalculator tCal = new PubCalculator();

					String tCalSql = insuInfoSSRS.GetText(i, 4);
					System.out.println("CalSql:" + tCalSql);
					tCal.setCalSql(tCalSql);
					tCal.addBasicFactor("Mult", mult);

					String tTmpCalResult = tCal.calculate();

					calFactorValue = tTmpCalResult;

					System.out.println("查询结果：" + tTmpCalResult);

					if (tTmpCalResult == null || tTmpCalResult.equals("")) {
						System.out.println("要素计算错误");

					}

				}

				insuYearInfoMap.put(calFactor, calFactorValue);
			}
			InsuYear = (String) insuYearInfoMap.get("InsuYear");
			InsuYearFlag = (String) insuYearInfoMap.get("InsuYearFlag");
		}
		insuYearInfo[0] = InsuYear;
		insuYearInfo[1] = InsuYearFlag;
		return insuYearInfo;

	}

	/**
	 * 保险期间闰年处理函数 <br />
	 * 根据生效日期以及两个闰年修改时间点计算出正确的保险期间
	 * made by zhangyige 2012-04-20
	 * @param InsuYear,  InsuYearFlag, cardNo
	 *        保险期间     保险期间标志    卡号
	 * @return InsuYear 保险期间
	 */
	public String dealLeapYear(String InsuYear, String InsuYearFlag,String cardNo) {
		String point1 = "2015-03-01"; //系统在该时间点将LDRiskDutywrap中所有365D改为366D
 		String point2 = "2016-02-29"; //系统在该时间点将LDRiskDutywrap中所有366D改为365D
 		ExeSQL tExeSQL = new ExeSQL();
		String getCValidate = "select CValidate from licardactiveinfolist where cardno = '"+cardNo+"' with ur";
		String aCValidate = tExeSQL.getOneValue(getCValidate);
 		try
        {
 			if(InsuYearFlag.equals("D")&&InsuYear.equals("365")){
 				String date1 = StrTool.cTrim(aCValidate);
	            String date2 = StrTool.cTrim(point1);
	            String date3 = StrTool.cTrim(point2);
	            FDate fd = new FDate();
	            Date tCValidate = fd.getDate(date1);
	            Date tpoint1 = fd.getDate(date2);
	            Date tpoint2 = fd.getDate(date3);
	            if (tCValidate.after(tpoint1)&&tCValidate.before(tpoint2))
	            {
	            	InsuYear = String.valueOf(Integer.parseInt(InsuYear)+1); 
	            	System.out.println("保险期间在闰年期间内，应加一天，InsuYear为："+InsuYear);
	            }
 			}else if(InsuYearFlag.equals("D")&&InsuYear.equals("366")){
 				String date1 = StrTool.cTrim(aCValidate);
	            String date2 = StrTool.cTrim(point1);
	            String date3 = StrTool.cTrim(point2);
	            FDate fd = new FDate();
	            Date tCValidate = fd.getDate(date1);
	            Date tpoint1 = fd.getDate(date2);
	            Date tpoint2 = fd.getDate(date3);
	            if (tCValidate.before(tpoint1)||tCValidate.after(tpoint2))
	            {
	            	InsuYear = String.valueOf(Integer.parseInt(InsuYear)-1);
	            	System.out.println("保险期间在闰年期间外，应减一天，InsuYear为："+InsuYear);
	            }
 			}
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		return InsuYear;
	}

	/**
	 * 根据激活日期计算生效日期。 <br />
	 * 激活日期次日零时，为保单生效日期。
	 * 
	 * @param cActiveDate
	 *            激活日期
	 * @return
	 */
	public String calCValidate(String strCValidate, String strActiveDate) {
		String caledCValidateStr = strCValidate;
		// Date tResCValidate = null;
		//
		// FDate tFDate = new FDate();
		// Date tCValidate = tFDate.getDate(strCValidate);
		// Date tActiveDate = tFDate.getDate(strActiveDate);
		//
		// if (tActiveDate == null)
		// {
		// return null;
		// }
		//
		// if (tCValidate != null && tCValidate.after(tActiveDate))
		// {
		// tResCValidate = tCValidate;
		// }
		// else
		// {
		// int tIntervalDays = 1;
		// Calendar tParamDate = Calendar.getInstance();
		// try
		// {
		// tParamDate.setTime(tActiveDate);
		// tParamDate.add(Calendar.DATE, tIntervalDays);
		// tResCValidate = tParamDate.getTime();
		// }
		// catch (Exception e)
		// {
		// e.printStackTrace();
		// return null;
		// }
		// }
		//        
		// DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		// caledCValidateStr=format1.format(tResCValidate);

		return caledCValidateStr;
	}

	// 获取身份证编码所对应的名称
	public String decodeIDType(String IDType) {
		ExeSQL tExeSQL = new ExeSQL();
		String IDTypeSql = "select codename('idtype','" + IDType
				+ "') from  SYSIBM.SYSDUMMY1";
		String IDTypeName = tExeSQL.getOneValue(IDTypeSql);
		return IDTypeName;

	}

	// 查询单证号下放至业务员 或 中介 的管理机构 0: 业务员的管理机构 1:中介的管理机构
	public String getManageComName(String cardNo) {
		String[] manageArr = new String[2];
		ExeSQL tExeSQL = new ExeSQL();

		String managecomSql = "  select case when substr(receivecomcode, 1,1) = 'D' "
				+ "then (select name from ldcom where comcode in (select managecom from laagent where agentcode = substr(receivecomcode, 2)))  end, "
				+ "case when substr(receivecomcode, 1,1) = 'E' "
				+ "then (select (select name from ldcom where ldcom.comcode=managecom) from lacom where agentcom = substr(receivecomcode, 2))  end "
				+ "from (select lzc.sendoutcom receivecomcode from LZCardNumber lzcn "
				+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo where 1 = 1 "
				+ "and lzcn.CardNo like '"
				+ cardNo
				+ "%' and substr(lzc.sendoutcom, 1, 1) in ('D', 'E') "
				+ "union all select lzc.receivecom receivecomcode from LZCardNumber lzcn "
				+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
				+ "where 1 = 1 and lzcn.CardNo like '"
				+ cardNo
				+ "%' and substr(lzc.receivecom, 1, 1) in ('D', 'E') ) as tmp with ur";
		SSRS managecomSSRS = tExeSQL.execSQL(managecomSql);
		if (managecomSSRS.MaxRow == 0) {
			return null;
		}
		manageArr[0] = managecomSSRS.GetText(1, 1);
		manageArr[1] = managecomSSRS.GetText(1, 2);

		if (manageArr[0] != null && !"".equals(manageArr[0])) {
			return manageArr[0];
		} else {
			return manageArr[1];
		}

	}

	// 查询保障期间 起始日期 终止日期 返回信息中 0：保障期间 1:起始日期 2:终止日期 3:产品名称
	//外网查询时使用
	public String[] getProductInfo(String cardNo) {
		String[] InsuYearInfoArr = new String[4];
		// 获取保险期间及保险期间标志 并拼接 Modify at 2010-7-12
		String[] riskRelateCode = getRiskSetCodeByCardNo(cardNo);// 包含套餐编码及certifyCode
		String riskSetCode = riskRelateCode[0];
		String[] insuYearInfoArr = getInsuInfoFromCore(cardNo, riskSetCode);

		String queryedInsuYear = insuYearInfoArr[0];
		String queryedInsuYearFlag = insuYearInfoArr[1];
		// 拼接后的insuYear 字符串
		String InsuYearFlagCN = switchInsuYearFlag(insuYearInfoArr[1]);
		String insuYearInfo = insuYearInfoArr[0] + InsuYearFlagCN;

		System.out.println("保障期间信息：" + insuYearInfo);

		ExeSQL tExeSQL = new ExeSQL();

		// 查询起始时间和终止时间
		String cValidateSql = "select cvalidate from licardactiveinfolist where cardno='"
				+ cardNo + "'";
		String riskStartDate = tExeSQL.getOneValue(cValidateSql);

		// 终止日期
		String riskEndDate = null;
		if (queryedInsuYear != null && !"".equals(queryedInsuYear)) {
			riskEndDate = PubFun.calDate(riskStartDate, Integer
					.parseInt(queryedInsuYear), queryedInsuYearFlag, "");
		}

		// 查询套餐名称
		String certifyCode = riskRelateCode[1];
		String certifyCodeSql = "select certifyname from lmcertifydes where certifycode='"
				+ certifyCode + "'";

		String productName = tExeSQL.getOneValue(certifyCodeSql);

		InsuYearInfoArr[0] = insuYearInfo;
		InsuYearInfoArr[1] = riskStartDate;
		InsuYearInfoArr[2] = riskEndDate;
		InsuYearInfoArr[3] = productName;

		return InsuYearInfoArr;

	}

	// 查询保障期间 起始日期 终止日期 返回信息中 0：保障期间 1:起始日期 2:终止日期 3:产品名称
	// 此两个参数的方法预留，不知哪个类会调用
	public String[] getMyProductInfo(String cardNo, String cValiDate) {
		String[] InsuYearInfoArr = new String[4];
		// 获取保险期间及保险期间标志 并拼接 Modify at 2010-7-12
		String[] riskRelateCode = getRiskSetCodeByCardNo(cardNo);// 包含套餐编码及certifyCode
		String riskSetCode = riskRelateCode[0];
		String[] insuYearInfoArr = getInsuInfoFromCore(cardNo, riskSetCode);

		String queryedInsuYear = insuYearInfoArr[0];
		String queryedInsuYearFlag = insuYearInfoArr[1];
		// 拼接后的insuYear 字符串
		String InsuYearFlagCN = switchInsuYearFlag(insuYearInfoArr[1]);
		String insuYearInfo = insuYearInfoArr[0] + InsuYearFlagCN;

		System.out.println("保障期间信息：" + insuYearInfo);

		ExeSQL tExeSQL = new ExeSQL();

		// 查询起始时间和终止时间
		// String cValidateSql = "select cvalidate from licardactiveinfolist
		// where
		// cardno='"
		// + cardNo + "'";
		// String riskStartDate = tExeSQL.getOneValue(cValidateSql);
		String riskStartDate = cValiDate;
		// 终止日期
		String riskEndDate = null;
		if (queryedInsuYear != null && !"".equals(queryedInsuYear)) {
			riskEndDate = PubFun.calDate(riskStartDate, Integer
					.parseInt(queryedInsuYear), queryedInsuYearFlag, "");
		}

		// 查询套餐名称
		String certifyCode = riskRelateCode[1];
		String certifyCodeSql = "select certifyname from lmcertifydes where certifycode='"
				+ certifyCode + "'";

		String productName = tExeSQL.getOneValue(certifyCodeSql);

		InsuYearInfoArr[0] = insuYearInfo;
		InsuYearInfoArr[1] = riskStartDate;
		InsuYearInfoArr[2] = riskEndDate;
		InsuYearInfoArr[3] = productName;

		return InsuYearInfoArr;

	}
	
	//1、校验份数时使用 2、新单保存使用（包括激活卡、远程出单、明亚、保尔和第三方承保）
	//made by zhangyige 2012-04-20
	public String[] getMyProductInfo(String cardNo, String cValiDate,String mult) {
		String[] InsuYearInfoArr = new String[4];
		// 获取保险期间及保险期间标志 并拼接 Modify at 2010-7-12
		String[] riskRelateCode = getRiskSetCodeByCardNo(cardNo);// 包含套餐编码及certifyCode
		String riskSetCode = riskRelateCode[0];
		String[] insuYearInfoArr = getInsuInfoFromCore(cardNo, riskSetCode,mult);

		String queryedInsuYear = insuYearInfoArr[0];
		String queryedInsuYearFlag = insuYearInfoArr[1];
		// 拼接后的insuYear 字符串
		String InsuYearFlagCN = switchInsuYearFlag(insuYearInfoArr[1]);
		String insuYearInfo = insuYearInfoArr[0] + InsuYearFlagCN;

		System.out.println("保障期间信息：" + insuYearInfo);

		ExeSQL tExeSQL = new ExeSQL();

		// 查询起始时间和终止时间
		// String cValidateSql = "select cvalidate from licardactiveinfolist
		// where
		// cardno='"
		// + cardNo + "'";
		// String riskStartDate = tExeSQL.getOneValue(cValidateSql);
		String riskStartDate = cValiDate;
		// 终止日期
		String riskEndDate = null;
		if (queryedInsuYear != null && !"".equals(queryedInsuYear)) {
			riskEndDate = PubFun.calDate(riskStartDate, Integer
					.parseInt(queryedInsuYear), queryedInsuYearFlag, "");
		}

		// 查询套餐名称
		String certifyCode = riskRelateCode[1];
		String certifyCodeSql = "select certifyname from lmcertifydes where certifycode='"
				+ certifyCode + "'";

		String productName = tExeSQL.getOneValue(certifyCodeSql);

		InsuYearInfoArr[0] = insuYearInfo;
		InsuYearInfoArr[1] = riskStartDate;
		InsuYearInfoArr[2] = riskEndDate;
		InsuYearInfoArr[3] = productName;

		return InsuYearInfoArr;

	}
	// 将发送报文中的条件字段封装为Map
	public Map getConditionMap(OMElement Oms) {
		Map conditionMap = new HashMap();
		Iterator iter1 = Oms.getChildren();
		while (iter1.hasNext()) {
			OMElement Om = (OMElement) iter1.next();

			Iterator child = Om.getChildren();

			while (child.hasNext()) {
				OMElement child_element = (OMElement) child.next();
				conditionMap.put(child_element.getLocalName(), child_element
						.getText());
			}

		}
		return conditionMap;

	}
//	public static void main(String[] args) {
//		LoginVerifyTool tool = new LoginVerifyTool();
//		String [] proInfoArr = tool.getMyProductInfo("KX000001914", "2011-9-20");
//		System.out.println("proInfoArr[2]:"+proInfoArr[2]);
//	}
	public static void main(String[] args) {
		LoginVerifyTool a = new LoginVerifyTool();
		String [] proInfoArr = a.getProductInfo("YI000000031");
		System.out.println("11");
	}

}
