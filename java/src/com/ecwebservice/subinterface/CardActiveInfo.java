package com.ecwebservice.subinterface;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

/**
 * Title:处理卡单激活报文
 * 
 * @author yuanzw 创建时间：2010-2-1 下午03:13:42 Description:
 * @version
 */

public class CardActiveInfo {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String cardDealType = null;// 处理方式

	private String cardNo = null;//

	private String password = null;// 

	// private String certifyCode = null;

	private String certifyName = null;// 

	private String cardrea = null;//

	private String effectDate = null;// EFFECTDATE

	private String activationDate = null;// ACTIVATIONDATE

	private String riskCode = null;

	private String mult = null;

	private String uAppntNo = null;// 投保人编号
	
	private String tComCode = null;//承保机构 代码

	MMap map = new MMap();

	VData tVData = new VData();
	
	private String mCValidate = null;//增加全局的cValidate
	
	
	
	 /** 用于输出的核保错误信息列表 */
    private List mUwErrorList = new ArrayList();
    
    //by gzh
    private String ipaddr = null;
    

	// 错误处理类
	public CErrors mErrors = new CErrors();

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {
			LoginVerifyTool mTool = new LoginVerifyTool();
			// 保险卡信息
			WFContListSchema contListSchema = new WFContListSchema();
			//投保人信息
			WFAppntListSchema appntListSchema = new WFAppntListSchema();
			// 被保人信息
			WFInsuListSet insuListSet = new WFInsuListSet();
			WFInsuListSchema insuListSchema = null;

			String relationCode = null;// 说明当前被保人是连带与否
			String relationToInsured = null;// 与主被保人人关系
			String reInsuNo = null;// 主被保人编号

			Iterator iter1 = Oms.getChildren();

			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("ZCARDENABLE")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();
							boolean validBatchNo = mTool
									.isValidBatchNo(batchno);
							if (!validBatchNo) {
								// 批次号不唯一
								responseOME = buildCardActiveResponseOME("01",
										"21", "批次号不唯一!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "21", "批次号不唯一!");
								return responseOME;
							}

							System.out.println("BATCHNO" + batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);
							if (!"01".equals(messageType)) {
								responseOME = buildCardActiveResponseOME("01",
										"5", "报文类型不正确!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "5", "报文类型不正确!");
								return responseOME;
							}
						}

						if (child_element.getLocalName().equals("CARDDEALTYPE")) {
							cardDealType = child_element.getText();

							System.out.println(cardDealType);
						}
						// 保险卡信息

						if (child_element.getLocalName().equals("CONTLIST")) {
							Iterator child1 = child_element.getChildren();
							while (child1.hasNext()) {
								OMElement child1OME = (OMElement) child1.next();

								// 新增字段 保费 保险期间 销售机构
								String prem = null;
								String insuYear = null;
								String insuYearFlag = null;
								String saleAgent = null;

								String certifyCode = null;

								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										String child2OMELocalName = child2OME
												.getLocalName();
										if (child2OMELocalName.equals("CARDNO")) {
											cardNo = child2OME.getText();
											System.out.println("cardNo"
													+ cardNo);
											// 单证号唯一性校验
											boolean cardNoFlag = mTool.isValidcardNo(cardNo);
											if (!cardNoFlag) {
												// 投保单号不唯一
												System.out.println("重复的CARDno:"+cardNo);
												responseOME = buildCardActiveResponseOME(
														"01", "23","单证号已使用，无法提交！");
												System.out.println("responseOME:"+ responseOME);
												LoginVerifyTool.writeLog(
														responseOME, batchno,
														messageType, sendDate,
														sendTime, "01", "100",
														"23", "单证号已使用，无法提交！！");
												return responseOME;
											}

										}
										if (child2OMELocalName
												.equals("PASSWORD")) {
											password = child2OME.getText();
											System.out.println(password);
										}
										if (child2OMELocalName
												.equals("CERTIFYCODE")) {
											certifyCode = child2OME.getText();
											System.out.println(certifyCode);
										}
										if (child2OMELocalName
												.equals("CERTIFYNAME")) {
											certifyName = child2OME.getText();
											System.out.println(certifyName);
										}
										if (child2OMELocalName
												.equals("CARDAREA")) {
											cardrea = child2OME.getText();
											System.out.println("***Wrong Code:"
													+ cardrea);
										}
										if (child2OMELocalName
												.equals("EFFECTDATE")) {
											effectDate = child2OME.getText();
											System.out.println(effectDate);
										}
										if (child2OMELocalName
												.equals("ACTIVATIONDATE")) {
											activationDate = child2OME
													.getText();
											System.out.println(activationDate);
										}
										if (child2OMELocalName
												.equals("RISKCODE")) {
											// 套餐编码
											riskCode = child2OME.getText();
											System.out.println(riskCode);

										}
										if (child2OMELocalName.equals("RLEVEL")) {
											// 档次
											mult = child2OME.getText();

										}
										
										if (child2OMELocalName.equals("IPADDR")) {
											// IP
											ipaddr = child2OME.getText();

										}

									}
								}

								// 获取套餐编码之后 根据套餐编码查询 查询保费
								// 保险期间及保险期间标志 销售机构 最后写入接口中间表

								ExeSQL tExeSQL = new ExeSQL();
								// 根据单证号 查询单证编码 查询出LZCardNumber 中的ReceiveCom
								// 供查询销售机构使用
								String certifyCodeSql = "select lzc.CertifyCode,lzc.ReceiveCom from LZCardNumber lzcn "
										+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
										+ "and lzc.EndNo = lzcn.CardSerNo where 1 = 1 and lzcn.CardNo='"
										+ cardNo + "' ";
								SSRS sResult = tExeSQL.execSQL(certifyCodeSql);

								// 管理机构 同时作为电子商务的销售机构
								String manageCom = null;
								if (sResult.MaxRow > 0) {
									//									 
									certifyCode = sResult.GetText(1, 1);
									// 下发的单证号的 接收机构（完整 其中第一位为标识位）
									String receiveCom = sResult.GetText(1, 2);
									// 获取接收机构的前缀标识
									String receiveComMark = receiveCom
											.substring(0, 1);
									String receiveComCode = receiveCom
											.substring(1);
									System.out.println("获取接收机构的前缀标识"
											+ receiveComMark);
									System.out.println("获取接收机构的后缀代码"
											+ receiveComCode);

									String manageComSql = null;
									String tComCodeSql = null;

									// 根据接收机构的前缀 分别进行处理
									// 如果单证号发放至代理机构
									if ("E".equalsIgnoreCase(receiveComMark)) {
										manageComSql = "select lac.name from LACom lac where 1 = 1 and lac.AgentCom in ('"
												+ receiveComCode + "') with ur";
										manageCom = tExeSQL
												.getOneValue(manageComSql);
										
										//通过中介代理机构 查找 承保机构代码
										tComCodeSql = " select ManageCom from LACom where AgentCom in ('"+receiveComCode+"') with ur";
										tComCode = tExeSQL.getOneValue(tComCodeSql);
										
									}
									// 如果单证号发放至业务员 代理人
									if ("D".equalsIgnoreCase(receiveComMark)) {
										manageComSql = " select name from ldcom where comcode in (select laa.ManageCom from LAAgent laa where 1 = 1 and laa.AgentCode in ('"
												+ receiveComCode
												+ "') )with ur";
										manageCom = tExeSQL
												.getOneValue(manageComSql);
										
										//通过代理人 查找 承保机构代码
										tComCodeSql = "select ManageCom from LAAgent where AgentCode in ('"+receiveComCode+"') with ur";
										tComCode = tExeSQL.getOneValue(tComCodeSql);
										
									}

									System.out.println("获取的销售机构" + manageCom);
								}

								// certifyCode = tExeSQL
								// .getOneValue(certifyCodeSql);
								// 根据单证编码查询保费
								String premSql = "select prem from lmcardrisk where CertifyCode='"
										+ certifyCode + "'";
								String insuYearSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
										+ riskCode
										+ "' and calfactor='InsuYear'   with ur";
								String insuYearFlagSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
										+ riskCode
										+ "' and calfactor='InsuYearFlag'   with ur";

								prem = tExeSQL.getOneValue(premSql);
								System.out.println("查询出的保费" + prem);
								insuYear = tExeSQL.getOneValue(insuYearSql);
								insuYearFlag = tExeSQL
										.getOneValue(insuYearFlagSql);
								// 查询销售机构 并写入接口中间表
								saleAgent = manageCom;

								contListSchema.setBatchNo(batchno);

								contListSchema.setSendDate(PubFun
										.getCurrentDate());
								contListSchema.setSendTime(PubFun
										.getCurrentTime());
								contListSchema.setMessageType(messageType);
								contListSchema.setCardDealType(cardDealType);
								// 不为空
								contListSchema.setBranchCode("860");
								contListSchema.setSendOperator("100");
								contListSchema.setActiveDate(activationDate);
								
								//增加生效日期校验
								String realCvaliDate = mTool.calCValidate(effectDate, activationDate);
								mCValidate=realCvaliDate;
								contListSchema.setCvaliDate(realCvaliDate);
								contListSchema.setRiskCode(riskCode);
								
								// uAppntNo = mTool.getAppntNo();
								contListSchema.setAppntNo("1");
								contListSchema.setCardNo(cardNo);
								contListSchema.setPassword(password);
								contListSchema.setCertifyCode(certifyCode);
								contListSchema.setCertifyName(certifyName);
								contListSchema.setCardArea(cardrea);
								// 新增 保费 保险期间 销售机构写入中间表

								contListSchema.setPrem(prem);
								contListSchema.setInsuYear(insuYear);
								contListSchema.setInsuYearFlag(insuYearFlag);
								contListSchema.setBak1(saleAgent);

								contListSchema.setCertifyCode(certifyCode);

								contListSchema.setMult(mult);

								map.put(contListSchema, "INSERT");
							}

						}

						// 投保人信息
						
						if (child_element.getLocalName().equals("APPNTLIST")) {
							// 投保人信息
							String name = null;
							String sex = null;
							String birthday = null;
							String IDType = null;
							String IDNo = null;
							String occupAtionType = null;
							String occupAtionCode = null;
							String postalAddress = null;
							String zipCode = null;
							String phont = null;
							String mobile = null;
							String email = null;
							String grpName = null;
							String companyPhone = null;
							String compaAddr = null;
							String compZipCode = null;
							Iterator child1 = child_element.getChildren();

							while (child1.hasNext()) {
								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										String child2OMELocalName = child2OME
												.getLocalName();
										if (child2OMELocalName.equals("NAME")) {
											name = child2OME.getText();
											System.out.println("name" + name);
										}
										if (child2OMELocalName.equals("SEX")) {
											sex = child2OME.getText();
											System.out.println(sex);
										}
										if (child2OMELocalName
												.equals("BIRTHDAY")) {
											birthday = child2OME.getText();
											System.out.println(birthday);
										}
										if (child2OMELocalName.equals("IDTYPE")) {
											IDType = mTool.codeIDType(child2OME
													.getText());
											System.out.println(IDType);
										}
										if (child2OMELocalName.equals("IDNO")) {

											IDNo = child2OME.getText();
											System.out.println(IDNo);
											if("0".equals(IDType)){
												System.out.println("获取的身份证号为+"+IDNo);
												boolean flag = CardActiveInfo.chkIDCards(IDNo);
												if(!flag){
													responseOME = buildCardActiveResponseOME("01", "150",
	                                 						"您的身份证号码有问题，请重新填写！");
	                                 				System.out.println("responseOME:" + responseOME);
													LoginVerifyTool.writeLog(responseOME, batchno, messageType,
															sendDate, sendTime, "01", "100", "200", "您的身份证号码有问题，请重新填写！");
													return responseOME;
												}
												System.out.println("返回是否正确"+flag);
											}
											
										}
										if (child2OMELocalName
												.equals("OCCUPATIONTYPE")) {
											occupAtionType = child2OME
													.getText();
											System.out.println(occupAtionType);
										}
										if (child2OMELocalName
												.equals("OCCUPATIONCODE")) {
											occupAtionCode = child2OME
													.getText();
											System.out.println(occupAtionCode);
										}
										if (child2OMELocalName
												.equals("POSTALADDRESS")) {
											postalAddress = child2OME.getText();
											System.out.println(postalAddress);
										}
										if (child2OMELocalName
												.equals("ZIPCODE")) {
											zipCode = child2OME.getText();
											System.out.println(zipCode);
										}
										if (child2OMELocalName.equals("PHONT")) {
											phont = child2OME.getText();
											System.out.println(phont);
										}
										if (child2OMELocalName.equals("MOBILE")) {
											mobile = child2OME.getText();
											System.out.println(mobile);
										}
										if (child2OMELocalName.equals("EMAIL")) {
											email = child2OME.getText();
											System.out.println(email);
										}
										if (child2OMELocalName
												.equals("GRPNAME")) {
											grpName = child2OME.getText();
											System.out.println(grpName);
										}
										if (child2OMELocalName
												.equals("COMPANYPHONE")) {
											companyPhone = child2OME.getText();
											System.out.println(companyPhone);
										}
										if (child2OMELocalName
												.equals("COMPADDR")) {
											compaAddr = child2OME.getText();
											System.out.println(compaAddr);
										}
										if (child2OMELocalName
												.equals("COMPZIPCODE")) {
											compZipCode = child2OME.getText();
											System.out.println(compZipCode);
										}

									}
								}
								appntListSchema = new WFAppntListSchema();
								appntListSchema.setCardNo(cardNo);

								appntListSchema.setBatchNo(batchno);
								appntListSchema.setAppntNo("1");
								appntListSchema.setName(name);
								appntListSchema.setSex(mTool.codeSex(sex));
								appntListSchema.setBirthday(birthday);
								appntListSchema.setIDType(IDType);
								appntListSchema.setIDNo(IDNo);

								appntListSchema
										.setOccupationType(occupAtionType);
								appntListSchema
										.setOccupationCode(occupAtionCode);
								appntListSchema.setPostalAddress(postalAddress);
								appntListSchema.setZipCode(zipCode);
								appntListSchema.setPhont(phont);
								appntListSchema.setMobile(mobile);
								appntListSchema.setEmail(email);
								appntListSchema.setGrpName(grpName);
								appntListSchema.setCompanyPhone(companyPhone);
								appntListSchema.setCompAddr(compaAddr);
								appntListSchema.setCompZipCode(compZipCode);

								map.put(appntListSchema, "INSERT");
							}

						}

						// 被保人信息

						if (child_element.getLocalName().equals("INSULIST")) {
							// 被保人信息
							int insuNo = 1;

							String toAppntRela = null;
							String authorization = null;
							String name = null;
							String sex = null;
							String birthday = null;
							String IDType = null;
							String IDNo = null;
							String occupAtionType = null;
							String occupAtionCode = null;
							String postalAddress = null;
							String zipCode = null;
							String phont = null;
							String mobile = null;
							String email = null;
							String grpName = null;
							String companyPhone = null;
							String compaAddr = null;
							String compZipCode = null;
							Iterator child1 = child_element.getChildren();
							while (child1.hasNext()) {
								OMElement child1OME = (OMElement) child1.next();
								IDType = "";
								IDNo = "";
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									insuListSchema = new WFInsuListSchema();
									// 不为空
									insuListSchema.setBatchNo(batchno);
									insuListSchema.setCardNo(cardNo);
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										String child2OMELocalName = child2OME
												.getLocalName();
										if (child2OMELocalName.equals("INSUNO")) {

										}
										if (child2OMELocalName
												.equals("TOAPPNTRELA")) {
											toAppntRela = child2OME.getText();
											System.out.println("toAppntRela"
													+ toAppntRela);
										}
										if (child2OMELocalName
												.equals("RELATIONCODE")) {
											relationCode = child2OME.getText();
											System.out.println(relationCode);
										}
										if (child2OMELocalName
												.equals("RELATIONTOINSURED")) {
											relationToInsured = child2OME
													.getText();
											System.out
													.println(relationToInsured);
										}
										if (child2OMELocalName
												.equals("REINSUNO")) {
											reInsuNo = child2OME.getText();
											System.out.println(reInsuNo);
										}
										if (child2OMELocalName.equals("NAME")) {
											name = child2OME.getText();
											System.out.println("name" + name);
										}
										if (child2OMELocalName.equals("SEX")) {
											String tempSex = child2OME.getText();
											if(tempSex.equals("男")){
												sex="0";
											}
											if(tempSex.equals("女")){
												sex="1";
											}
											System.out.println(sex);
										}
										if (child2OMELocalName
												.equals("BIRTHDAY")) {
											birthday = child2OME.getText();
											System.out.println(birthday);
										}
										if (child2OMELocalName.equals("IDTYPE")) {
											IDType = mTool.codeIDType(child2OME
													.getText());
											System.out.println("IDType="+IDType);
											IDType = mTool.codeIDType(child2OME.getText());
											System.out.println("IDType="+IDType);
											if("0".equals(IDType)){
												System.out.println("获取的身份证号为+"+IDNo);
												boolean flag = CardActiveInfo.chkIDCards(IDNo);
												System.out.println("身份证校验结果："+flag);
												if(!flag){
													responseOME = buildCardActiveResponseOME("01", "150",
	                                 						"您的身份证号码有问题，请重新填写！");
	                                 				System.out.println("responseOME:" + responseOME);
													LoginVerifyTool.writeLog(responseOME, batchno, messageType,
															sendDate, sendTime, "01", "100", "200", "您的身份证号码有问题，请重新填写！");
													return responseOME;
												}
												System.out.println("返回是否正确"+flag);
											}
										}
										if (child2OMELocalName.equals("IDNO")) {			
											IDNo = child2OME.getText();
											System.out.println(IDNo);
											
										}
										if (child2OMELocalName
												.equals("OCCUPATIONTYPE")) {
											occupAtionType = child2OME
													.getText();
											System.out.println(occupAtionType);
										}
										if (child2OMELocalName
												.equals("OCCUPATIONCODE")) {
											occupAtionCode = child2OME
													.getText();
											System.out.println(occupAtionCode);
										}
										if (child2OMELocalName
												.equals("POSTALADDRESS")) {
											postalAddress = child2OME.getText();
											System.out.println(postalAddress);
										}
										if (child2OMELocalName
												.equals("ZIPCODE")) {
											zipCode = child2OME.getText();
											System.out.println(zipCode);
										}
										if (child2OMELocalName.equals("PHONT")) {
											phont = child2OME.getText();
											System.out.println(phont);
										}
										if (child2OMELocalName.equals("MOBILE")) {
											mobile = child2OME.getText();
											System.out.println(mobile);
										}
										if (child2OMELocalName.equals("EMAIL")) {
											email = child2OME.getText();
											System.out.println(email);
										}
										if (child2OMELocalName
												.equals("GRPNAME")) {
											grpName = child2OME.getText();
											System.out.println(grpName);
										}
										if (child2OMELocalName
												.equals("COMPANYPHONE")) {
											companyPhone = child2OME.getText();
											System.out.println(companyPhone);
										}
										if (child2OMELocalName
												.equals("COMPADDR")) {
											compaAddr = child2OME.getText();
											System.out.println(compaAddr);
										}
										if (child2OMELocalName
												.equals("COMPZIPCODE")) {
											compZipCode = child2OME.getText();
											System.out.println(compZipCode);
										}
										if (child2OMELocalName.equals("AUTHORIZATION")) {
											authorization = child2OME.getText();
											System.out.println("授权字段AUTHORIZATION：" + authorization);
										}

									}
									
									//被保人份数校验开始
                                    ExeSQL tExeSQL = new ExeSQL();
                                    //准备校验SQL,校验该激活卡是否是lmcardrisk中的risktype = 'W'的类型
									String checkSql = " select lmcr.riskcode, re.CertifyCode from lmcardrisk lmcr," 
										        +"(select lzcn.CardNo, lzc.CertifyCode, lzc.State from LZCardNumber lzcn " 
												+" inner join LZCard lzc on lzcn.CardType = lzc.SubCode and " 
												+"lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo " 
												+"where 1 = 1 and lzcn.CardNo = '"
												+cardNo
												+"') re where lmcr.risktype = 'W' and lmcr.certifycode = re.certifycode " ;
									SSRS checkSSRS = tExeSQL.execSQL(checkSql);									
//									String checkSql2 =" select * from LICardActiveInfoList where cvalidate is not null and cardno='"+cardNo+"' ";
//									SSRS checkSSRS2 = tExeSQL.execSQL(checkSql2);	
									
                                    String insureSql = null;               
                                    String cardNoStr = cardNo.toString().substring(0, 2);
                                    if(checkSSRS.MaxRow!=0 && (mCValidate != null|| mCValidate != "")){
                                        insureSql="select cardno,Cvalidate,InActiveDate from LICardActiveInfoList where 1=1 "
//                                              +"and CardNo like '"+cardNoStr+"%' "
                                              +"and cardtype = '"+riskCode+"' "//+ " cardtype = '"+riskCode+"' "-----------fromYZF
                                              +"and name='"+name+"' and sex='"+sex
                                              +"' and birthday='"+birthday+"' and idtype='"+ IDType
                                              +"' and idno='"+IDNo+"' and InActiveDate>'"+PubFun.getCurrentDate()+"' "
                                              +"and (CardStatus = '01' or CardStatus is null)";
                                        
//                                        String temp1=tExeSQL.execSQL(insureSql).GetText(1, 1);
                                        SSRS insureSSRS = tExeSQL.execSQL(insureSql);
                                        int alreadyNo = 1;
                        		        FDate fdate = new FDate();
                        		        LoginVerifyTool tLoginVerifyTool= new LoginVerifyTool();
                        		        System.out.println("insureSSRS.MaxRow:--lcb:"+insureSSRS.MaxRow);
                                        for(int i=1;i<=insureSSRS.MaxRow;i++){
                            		        Date oldCvalidate = fdate.getDate(insureSSRS.GetText(i, 2)); //结果中的生效日期，用FDate处理
                            		        Date oldInactivedate = fdate.getDate(insureSSRS.GetText(i, 3)); //结果中失效日期，用FDate处理
                            		        Date newCvalidate = fdate.getDate(mCValidate); //当前激活卡的生效日期，用FDate处理
                            		        String[] inactivedate=tLoginVerifyTool.getMyProductInfo(cardNo, mCValidate,mult);
                            		        Date newInactivedate = fdate.getDate(inactivedate[2]); //当前激活卡的失效日期，用FDate处理		        
                                        	if((oldCvalidate.compareTo(newCvalidate) <= 0 && oldInactivedate.compareTo(newCvalidate) > 0 )||
                                        			(oldCvalidate.compareTo(newInactivedate) < 0 && oldInactivedate.compareTo(newInactivedate) >= 0) ){ 
                                        		//compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
                                        		alreadyNo++;
                                        	}
                                        	System.out.println("alreadyNo:--lcb:hasdone:"+alreadyNo);
                                        }                                          
                                        String maxNoSql=null;
                                        //取最大份数
                                        maxNoSql=" select maxcopys from ldwrap where riskwrapcode='"+riskCode+"' ";                                       
                                        String temp2=tExeSQL.execSQL(maxNoSql).GetText(1, 1);
                                        int maxNo = 0;                                    
                                        if(temp2.matches("^\\d+$")){
                                        	maxNo = Integer.parseInt(temp2);
                                            if(maxNo<alreadyNo){
//                                           	 @@错误处理被保人投保份数超过验证
//                                               this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                                            	
                                 				CError tError = new CError();
                                 				tError.moduleName = "CardActiveInfo";
                                 				tError.functionName = "submitData";
                                 				tError.errorMessage = "被保人投保份数已超过可投保的最大份数!";
                                 				this.mErrors.addOneError(tError);
                                 				responseOME = buildCardActiveResponseOME("01", "6",
                                 						"被保人投保份数已超过可投保的最大份数!");
                                 				System.out.println("responseOME:" + responseOME);

                                 				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                                 						sendDate, sendTime, "01", "100", "6", "被保人投保份数已超过可投保的最大份数!");
                                 				return responseOME;
                                             }
                                        	
                                        }
                                        System.out.println("max:"+maxNo+"  alread:"+alreadyNo);
                                    }else{
                                    	insureSql="select count(1) + 1 from LICardActiveInfoList where cardtype='"//--------fromYZF
                                            +riskCode+"' and name='"+name+"' and sex='"+sex
                                            +"' and birthday='"+birthday+"' and idtype='"+ IDType
                                            +"' and idno='"+IDNo+"' ";
                                    	
                                    	String temp1=tExeSQL.execSQL(insureSql).GetText(1, 1);   
                                    	int alreadyNo = 0;
                                        if(temp1.matches("^\\d+$")){
                                        	alreadyNo = Integer.parseInt(temp1);
                                        }                                          
                                        String maxNoSql=null;
                                        maxNoSql=" select maxcopys from ldwrap where riskwrapcode='"+riskCode+"' ";                                       
                                        String temp2=tExeSQL.execSQL(maxNoSql).GetText(1, 1);
                                        int maxNo = 0;                                    
                                        if(temp2.matches("^\\d+$")){
                                        	maxNo = Integer.parseInt(temp2);
                                            if(maxNo<alreadyNo){
//                                           	 @@错误处理被保人投保份数超过验证
//                                               this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                                            	
                                 				CError tError = new CError();
                                 				tError.moduleName = "CardActiveInfo";
                                 				tError.functionName = "submitData";
                                 				tError.errorMessage = "被保人投保份数已超过可投保的最大份数!";
                                 				this.mErrors.addOneError(tError);
                                 				responseOME = buildCardActiveResponseOME("01", "6",
                                 						"被保人投保份数已超过可投保的最大份数!");
                                 				System.out.println("responseOME:" + responseOME);

                                 				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                                 						sendDate, sendTime, "01", "100", "6", "被保人投保份数已超过可投保的最大份数!");
                                 				return responseOME;
                                             }
                                        }
                                        System.out.println("max:"+maxNo+"  alread:"+alreadyNo);
                                    }
                                    //-----------------------添加日期校验------------------------------------------------------------------
                                /* //校验截止日期
                                    String dateClose = null;
                        			String tDateCloseSql = null;
                        			String subCodeSql = null;
                        			String captureCardNo = null;
                        			subCodeSql = "select Subcode from LMCertifyDes where Certifyname = '"+certifyName+"'";
                        			SSRS cardNoExeSQL = new ExeSQL().execSQL(subCodeSql);
                        			captureCardNo = cardNo.replaceAll(cardNoExeSQL.GetText(1, 1), "");
                        			tDateCloseSql = "select Activedate from LZCardPrint where Subcode = ("+subCodeSql+") and '"+captureCardNo+"' between Startno and Endno";
                        			SSRS dateExeSQL = new ExeSQL().execSQL(tDateCloseSql);
                        			dateClose = dateExeSQL.GetText(1, 1);
                        			SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
                        			try {
                        				Date date = sdf.parse(dateClose);
                        				Date date1 = sdf.parse(activationDate);
                        				Calendar calendar = Calendar.getInstance();
                        				Calendar calendar1 = Calendar.getInstance();
                        				calendar.setTime(date);
                        				calendar1.setTime(date1);
                        				if(calendar1.get(calendar1.DAY_OF_YEAR)-calendar.get(calendar.DAY_OF_YEAR)<0){
                        					CError tError = new CError();
                                 			tError.moduleName = "CardActiveInfo";
                                 			tError.functionName = "submitData";
                                 			tError.errorMessage = "保单激活日期已过期!";
                                 			this.mErrors.addOneError(tError);
                                 			responseOME = buildCardActiveResponseOME("01", "6",
                                 						"保单激活日期"+activationDate+"超出激活日期"+dateClose+"最后期限!");
                                 			System.out.println("responseOME:" + responseOME);

                                 			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                                 						sendDate, sendTime, "01", "100", "6", "被保人年龄不在投保年龄范围内!");
                                 			return responseOME;
                        				};
                        			} catch (ParseException e) {
                        				e.printStackTrace();
                        			}
                                    */
                                    //---------------------------添加结束------------------------------------------------------------------------
//                                  校验年龄
                                    String ageWrapSql = "select case when maxinsuredage is null then -1 else maxinsuredage end," +
					                            		" case when mininsuredage is null then -1 else mininsuredage end " +
					                            		" from ldriskwrap where riskwrapcode = '"+riskCode+"' ";
                                    SSRS ageSSRS = new ExeSQL().execSQL(ageWrapSql);
                                    if(ageSSRS != null && ageSSRS.MaxRow>0){
                                    	if(!"".equals(StrTool.cTrim(ageSSRS.GetText(1, 1))) 
                                    			&& !"-1".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
                                    			&& !"0".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
                                    			&& !"".equals(StrTool.cTrim(ageSSRS.GetText(1, 2)))
                                    			&& !"-1".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))){
                                    		int tInsurrdAge = PubFun.calInterval(birthday,effectDate, "Y");
                                    		if(Integer.parseInt(ageSSRS.GetText(1, 1))<tInsurrdAge || Integer.parseInt(ageSSRS.GetText(1, 2))>tInsurrdAge){
                                    			CError tError = new CError();
                                 				tError.moduleName = "CardActiveInfo";
                                 				tError.functionName = "submitData";
                                 				tError.errorMessage = "被保人年龄不在投保年龄范围内!";
                                 				this.mErrors.addOneError(tError);
                                 				responseOME = buildCardActiveResponseOME("01", "6",
                                 						"被保人年龄"+tInsurrdAge+"不在投保年龄"+ageSSRS.GetText(1, 2)+"-"+ageSSRS.GetText(1, 1)+"范围内!");
                                 				System.out.println("responseOME:" + responseOME);

                                 				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                                 						sendDate, sendTime, "01", "100", "6", "被保人年龄不在投保年龄范围内!");
                                 				return responseOME;
                                    		}
                                    	}
                                    }
//                                  校验职业类别
                                    String occutypeWrapSql = "select maxoccutype from ldwrap where riskwrapcode = '"+riskCode+"' ";
                                    SSRS occutypeSSRS = new ExeSQL().execSQL(occutypeWrapSql);
                                    if(occutypeSSRS != null && occutypeSSRS.MaxRow>0){
                                    	if(!"".equals(StrTool.cTrim(occutypeSSRS.GetText(1, 1)))){
                                    		if(Integer.parseInt(occutypeSSRS.GetText(1, 1))<Integer.parseInt(occupAtionType)){
                                    			CError tError = new CError();
                                 				tError.moduleName = "CardActiveInfo";
                                 				tError.functionName = "submitData";
                                 				tError.errorMessage = "被保人职业类别不在投保职业类别范围内!";
                                 				this.mErrors.addOneError(tError);
                                 				responseOME = buildCardActiveResponseOME("01", "6",
                                 						"被保人职业类别"+occupAtionType+"不在投保最大职业类别"+occutypeSSRS.GetText(1, 1)+"范围内!");
                                 				System.out.println("responseOME:" + responseOME);

                                 				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                                 						sendDate, sendTime, "01", "100", "6", "被保人职业类别不在投保职业类别范围内!");
                                 				return responseOME;
                                    		}
                                    	}
                                    }

									// 输出被保人流水号
									System.out.println("被保人流水号：" + insuNo);
									insuListSchema.setInsuNo(String
											.valueOf(insuNo));
									insuListSchema.setToAppntRela(toAppntRela);
									insuListSchema
											.setRelationCode(relationCode);
									insuListSchema
											.setRelationToInsured(relationToInsured);
									insuListSchema.setReInsuNo(reInsuNo);
									insuListSchema.setName(name);
									insuListSchema.setSex(mTool.codeSex(sex));
									insuListSchema.setBirthday(birthday);
									insuListSchema.setIDType(IDType);
									insuListSchema.setIDNo(IDNo);
									insuListSchema
											.setOccupationCode(occupAtionCode);
									insuListSchema
											.setOccupationType(occupAtionType);
									insuListSchema
											.setPostalAddress(postalAddress);
									insuListSchema.setZipCode(zipCode);
									insuListSchema.setPhont(phont);
									insuListSchema.setMobile(mobile);
									insuListSchema.setEmail(email);
									insuListSchema.setGrpName(grpName);
									insuListSchema
											.setCompanyPhone(companyPhone);
									insuListSchema.setCompAddr(compaAddr);
									insuListSchema.setCompZipCode(compZipCode);
									insuListSchema.setAuthorization(authorization);

									insuListSet.add(insuListSchema);
									
									// 生成下一个被保人的流水号
									insuNo++;
								}
							}
							map.put(insuListSet, "INSERT");
						}

						// 受益人信息
						if (child_element.getLocalName().equals("BNFLIST")) {
							// 被保人信息
							String toInsuNo = null;
							String toInsuRela = null;
							String bnfType = null;
							String bnfGrade = null;
							String bnfRate = null;
							String name = null;
							String sex = null;
							String birthday = null;
							String IDType = null;
							String IDNo = null;

							Iterator child1 = child_element.getChildren();
							WFBnfListSet bnfListSet = new WFBnfListSet();
							WFBnfListSchema bnfListSchema;
							boolean loadFlag = true;
							int seq = 1;
							int sum[] = new int[6];
							ArrayList array = new ArrayList();
							while (child1.hasNext() && loadFlag) {
								OMElement child1OME = (OMElement) child1.next();
								if (child1OME.getLocalName().equals("ITEM")) {
									Iterator child2 = child1OME.getChildren();
									// item下没有节点
									if (!child2.hasNext()) {
										loadFlag = false;
									}
									while (child2.hasNext()) {
										OMElement child2OME = (OMElement) child2
												.next();
										String child2OMELocalName = child2OME
												.getLocalName();

										if (child2OMELocalName
												.equals("TOINSUNO")) {
											toInsuNo = child2OME.getText();
											System.out.println("toInsuNo "
													+ toInsuNo);
										}
										if (child2OMELocalName
												.equals("TOINSURELA")) {
											toInsuRela = child2OME.getText();
											System.out.println("toInsuRela "
													+ toInsuRela);
										}
										if (child2OMELocalName
												.equals("BNFTYPE")) {
											bnfType = child2OME.getText();
											System.out.println("bnfType "
													+ bnfType);
										}
										if (child2OMELocalName
												.equals("BNFGRADE")) {
											bnfGrade = child2OME.getText();
											System.out.println(bnfGrade);
										}
										if (child2OMELocalName
												.equals("BNFRATE")) {
											bnfRate = child2OME.getText();
											System.out.println(bnfRate);
										}
										if (child2OMELocalName.equals("NAME")) {
											name = child2OME.getText();
											if (name == null || "".equals(name)) {
												loadFlag = false;
											}
											System.out.println(name);
										}
										if (child2OMELocalName.equals("SEX")) {
											sex = child2OME.getText();
											if (sex == null || "".equals(sex)) {
												loadFlag = false;
											}
											System.out.println(sex);
										}
										if (child2OMELocalName
												.equals("BIRTHDAY")) {
											birthday = child2OME.getText();
											if (birthday == null
													|| "".equals(birthday)) {
												loadFlag = false;
											}
											System.out.println(birthday);
										}
										/*
										 * 受益人可以不填证件类型和证件号码
										 */
										if (child2OMELocalName.equals("IDTYPE")) {
											IDType = mTool.codeIDType(child2OME
													.getText());
											if (IDType == null
													|| "".equals(IDType)) {
											//	loadFlag = false;
											}
											System.out.println(IDType+":"+loadFlag);
										}
										if (child2OMELocalName.equals("IDNO")) {
											IDNo = child2OME.getText();
											if (IDNo == null || "".equals(IDNo)) {
											//	loadFlag = false;
											}
											System.out.println(IDNo);
										}

									}
								}
								bnfListSchema = new WFBnfListSchema();
								// 不为空
								bnfListSchema.setBatchNo(batchno);
								bnfListSchema.setCardNo(cardNo);

								bnfListSchema.setToInsuNo(toInsuNo);
								bnfListSchema.setToInsuRela(toInsuRela);
								bnfListSchema.setBnfType(bnfType);
								bnfListSchema.setBnfGrade(bnfGrade);
								bnfListSchema.setBnfRate(Double.parseDouble(bnfRate)/100);
								int n = Integer.parseInt(bnfGrade);
								if(array.contains(bnfGrade)){
                                	seq++;
                                	bnfListSchema.setBnfGradeNo(String.valueOf(seq));
                                	sum[n] = (int)(sum[n] + Integer.parseInt(bnfRate));
                            	}else{
                            		bnfListSchema.setBnfGradeNo(String.valueOf(1));
                            		array.add(bnfGrade);
                            		sum[n] = (int) Integer.parseInt(bnfRate);
                            	}
								bnfListSchema.setName(name);
								bnfListSchema.setSex(mTool.codeSex(sex));
								bnfListSchema.setBirthday(birthday);
								bnfListSchema.setIDType(IDType);
								bnfListSchema.setIDNo(IDNo);

								bnfListSet.add(bnfListSchema);

							}
							int length = sum.length;
							for(int i=1;i<length;i++){
								if(sum[i]==0)continue;
								if(sum[i]!=100){
	                        		CError tError = new CError();
	                 				tError.moduleName = "CardActiveInfo";
	                 				tError.functionName = "BNFLIST";
	                 				tError.errorMessage = "受益顺位"+i+"的受益总和不等于100!";
	                 				this.mErrors.addOneError(tError);
	                 				responseOME = buildCardActiveResponseOME("01", "6",
	                 						"受益顺位"+i+"的受益总和不等于100!");
	                 				System.out.println("responseOME:" + responseOME);

	                 				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
	                 						sendDate, sendTime, "01", "100", "6", "受益顺位"+i+"的受益总和不等于100!");
	                 				return responseOME;
	                        	}
							}
							if (loadFlag) {
								map.put(bnfListSet, "INSERT");
							}

						}
					}
					// 日志3
					LoginVerifyTool.writeBasicInfoLog(cardNo, uAppntNo,
							insuListSchema.getInsuNo());
				}
			}

			ExternalActive tExternalActive = new ExternalActive();
			LICardActiveInfoListSet exCardActiveInfoListSet = tExternalActive
					.writeLICardActiveInfoList(contListSchema, insuListSet,ipaddr);
//			by gzh 
			if(exCardActiveInfoListSet == null ){
				System.out.println(tExternalActive.getMErrors().getErrContent());
				responseOME = buildCardActiveResponseOME("01","21",tExternalActive.getMErrors().getFirstError());
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "21", tExternalActive.getMErrors().getFirstError());
				return responseOME;
			}
			map.put(exCardActiveInfoListSet, "INSERT");
			
			//state In Lzcard 需要修改为14.
			String[] updateArr = tExternalActive.getUpdateLzcardSql(cardNo);
			
			String stateInLzcard = updateArr[1];
			//判断单证号是否有效
			if("2".equals(stateInLzcard)||"10".equals(stateInLzcard)||"11".equals(stateInLzcard)||"13".equals(stateInLzcard)){
				System.out.println("单证号有效.");
			}else{
				responseOME = buildCardActiveResponseOME("01",
						"21", "单证号无效--状态!");
				System.out.println("responseOME:" + responseOME);

				LoginVerifyTool.writeLog(responseOME, batchno,
						messageType, sendDate, sendTime, "01",
						"100", "21", "单证号无效!");
				return responseOME;
			}
			if("11".equals(stateInLzcard)||"10".equals(stateInLzcard)){
				System.out.println("state In Lzcard 需要修改为14.");
				String modifyStateSql = updateArr[0];
				map.put(modifyStateSql, "UPDATE");
			}
			
			//校验 by gzh
			if(!check(insuListSet,appntListSchema)){
				String UWErrors =  this.getUWErrors();
				System.out.println("错误信息"+UWErrors);
				responseOME = buildCardActiveResponseOME("01",
						"100", UWErrors);
				return responseOME;
			}
			// WFInsuListSchema liWFInsuL istSchema = insuListSet.get(1);
			// WFInsuListSet externalInsuListSet = insuListSet;
			// 入库
			PubSubmit tPubSubmit = new PubSubmit();
			tVData.add(map);
			if (!tPubSubmit.submitData(tVData, "INSERT")) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "CardActiveInfo";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				responseOME = buildCardActiveResponseOME("01", "6",
						"系统繁忙请稍后!");
				System.out.println("responseOME:" + responseOME);

				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "100", "6", "数据提交失败!");
				return responseOME;
			} else {
				responseOME = buildCardActiveResponseOME("00", "0", "成功");
				System.out.println("responseOME:" + responseOME);
				// 日志4
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "00", "100", "0", "成功");
			}
			// 判断是否连带 写入卡单外部激活信息表

			// if ("00".equals(relationCode)) {
			// 非连带

			// 被保人信息

			// liWFInsuListSchema = null;

			// } else {
			// responseOME = buildCardActiveResponseOME("01", "22",
			// "被保人为连带，无法提交！");
			// System.out.println("responseOME:" + responseOME);
			// // 日志4
			// LoginVerifyTool.writeLog(responseOME, batchno, messageType,
			// sendDate, sendTime, "01", "100", "22", "被保人为连带，无法提交！");
			// return responseOME;
			// }

		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildCardActiveResponseOME("01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "100", "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	private OMElement buildCardActiveResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("ZCARDENABLE", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);

		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "MANAGECODE", tComCode);//添加 承保机构 代码
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}
	
	// 20101122 卡单激活校验 by gzh
	private boolean check(WFInsuListSet insuListSet,WFAppntListSchema appntListSchema){
		boolean flag = true;
		LMCheckFieldDB tLMCheckFieldDB= new LMCheckFieldDB();
		tLMCheckFieldDB.setRiskCode(this.riskCode);
		tLMCheckFieldDB.setFieldName("ETBINSERT");
		LMCheckFieldSet tLMCheckFieldSet = tLMCheckFieldDB.query();
		for(int i=1;i<=tLMCheckFieldSet.size();i++){
			LMCheckFieldSchema tLMCheckFieldSchema = tLMCheckFieldSet.get(i);
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			tLMCalModeDB.setCalCode(tLMCheckFieldSchema.getCalCode());
			LMCalModeSet tLMCalModeSet = new LMCalModeSet();
			tLMCalModeSet = tLMCalModeDB.query();
			for(int j=1;j<=tLMCalModeSet.size();j++){
				LMCalModeSchema tLMCalModeSchema = tLMCalModeSet.get(j);
				for(int k=1;k<=insuListSet.size();k++){
					WFInsuListSchema insuListSchema = insuListSet.get(k);
					int tAppntAge = PubFun.calInterval(insuListSchema.getBirthday(),
							PubFun.getCurrentDate(), "Y");
					Calculator cal = new Calculator();
					cal.setCalCode(tLMCalModeSchema.getCalCode());
					cal.addBasicFactor("InsuredName", insuListSchema.getName());
					cal.addBasicFactor("Sex", insuListSchema.getSex());
					cal.addBasicFactor("InsuredBirthday", insuListSchema.getBirthday());
					cal.addBasicFactor("IDType", insuListSchema.getIDType());
					cal.addBasicFactor("IDNo", insuListSchema.getIDNo());
					cal.addBasicFactor("RiskWrapCode", this.riskCode);
					cal.addBasicFactor("AppAge",tAppntAge+"");
					cal.addBasicFactor("Mult",this.mult);
					cal.addBasicFactor("OccupationType",insuListSchema.getOccupationType());
					cal.addBasicFactor("Cvalidate",mCValidate);
					String result = cal.calculate();
					if(!result.equals("0")){
						flag = false;
						//System.out.println("被保人"+insuListSchema.getName()+"数据有问题。");
						//setContUWError(tLMCalModeSchema.getRemark(), tLPContSchema);
		                mUwErrorList.add(tLMCalModeSchema.getRemark());
					}
				}
			}
		}
		return flag;
	}
    /**
     * 得到卡单激活未通过保原因
     * @return String
     * @date 20101122
     */
    public String getUWErrors()
    {
        String uwErrors = "";
        Set set = new HashSet();
        set.addAll(mUwErrorList);
        ArrayList mUwErrorList1 = new ArrayList();
        mUwErrorList1.addAll(set);
        for (int i = 0; i < mUwErrorList1.size(); i++)
        {
        	uwErrors += (String) mUwErrorList1.get(i) ;
            if(uwErrors.length()>0 && i<(mUwErrorList1.size()-1)) uwErrors+= ";";
        }
        return uwErrors;
    }
public static Boolean chkIDCards(String idcard){
		
		String M = "";
		String JYM = "";
		String ereg = "";
		String ereg1 = "";
		Integer Y = 0;
		Integer S = 0;
		
		List<String> errors = new ArrayList<String>();
		errors.add("身份证验证通过!");
		errors.add("身份证号码位数不对!");
		errors.add("身份证号码出生日期超出范围或含有非法字符!");
		errors.add("身份证号码校验错误!");
		errors.add("身份证地区非法!");
		errors.add("身份证X必须大写!");		
		
		Map<Integer,String> area = new HashMap<Integer,String>();
		area.put(11, "北京");
		area.put(12, "天津");
		area.put(13, "河北");
		area.put(14, "山西");
		area.put(15, "内蒙古");
		area.put(21, "辽宁");
		area.put(22, "吉林");
		area.put(23, "黑龙江");
		area.put(31, "上海");
		area.put(32, "江苏");
		area.put(33, "浙江");
		area.put(34, "安徽");
		area.put(35, "福建");
		area.put(36, "江西");
		area.put(37, "山东");
		area.put(41, "河南");
		area.put(42, "湖北");
		area.put(43, "湖南");
		area.put(44, "广东");
		area.put(45, "广西");
		area.put(46, "海南");
		area.put(50, "重庆");
		area.put(51, "四川");
		area.put(52, "贵州");
		area.put(53, "云南");
		area.put(54, "西藏");
		area.put(61, "陕西");
		area.put(62, "甘肃");
		area.put(63, "青海");
		area.put(64, "宁夏");
		area.put(65, "新疆");
		area.put(71, "台湾");
		area.put(81, "香港");
		area.put(82, "澳门");
		area.put(91, "国外");
		
		char[] chars = idcard.toCharArray();
		
		if(area.get(Integer.parseInt(idcard.substring(0,2)))==null){ 
			System.out.println(errors.get(4));
			return false;
		}
		switch(idcard.length()){
		case 15:
			if ( (Integer.parseInt(idcard.substring(6,8))+1900) % 4 == 0 || 
			((Integer.parseInt(idcard.substring(6,8))+1900) % 100 == 0 && 
			(Integer.parseInt(idcard.substring(6,8))+1900) % 4 == 0 )){
			    ereg="^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$";//测试出生日期的合法性
			} else {
				ereg="^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$";//测试出生日期的合法性
			}
			Pattern p = Pattern.compile(ereg);  
	        Matcher m = p.matcher(idcard); 
	        if(m.matches()){
	        	return true;
	        }else{
	        	return false;
	        }
		case 18:
			//18位身份号码检测
			//出生日期的合法性检查
			//闰年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))
			//平年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))
			if (Integer.parseInt(idcard.substring(6,10)) % 4 == 0 || 
			(Integer.parseInt(idcard.substring(6,10)) % 100 == 0 && 
			Integer.parseInt(idcard.substring(6,10)) % 4 == 0 )){
				ereg1="^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9xX]$";//闰年出生日期的合法性正则表达式
			} else {
				ereg1="^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9xX]$";//平年出生日期的合法性正则表达式
			}
			Pattern p1 = Pattern.compile(ereg1);  
	        Matcher m1 = p1.matcher(idcard); 
			if(m1.matches()){//测试出生日期的合法性
				//计算校验位
				S = (Integer.parseInt(String.valueOf(chars[0])) + Integer.parseInt(String.valueOf(chars[10]))) * 7
				+ (Integer.parseInt(String.valueOf(chars[1])) + Integer.parseInt(String.valueOf(chars[11]))) * 9
				+ (Integer.parseInt(String.valueOf(chars[2])) + Integer.parseInt(String.valueOf(chars[12]))) * 10
				+ (Integer.parseInt(String.valueOf(chars[3])) + Integer.parseInt(String.valueOf(chars[13]))) * 5
				+ (Integer.parseInt(String.valueOf(chars[4])) + Integer.parseInt(String.valueOf(chars[14]))) * 8
				+ (Integer.parseInt(String.valueOf(chars[5])) + Integer.parseInt(String.valueOf(chars[15]))) * 4
				+ (Integer.parseInt(String.valueOf(chars[6])) + Integer.parseInt(String.valueOf(chars[16]))) * 2
				+ Integer.parseInt(String.valueOf(chars[7])) * 1
				+ Integer.parseInt(String.valueOf(chars[8])) * 6
				+ Integer.parseInt(String.valueOf(chars[9])) * 3;
				Y = S % 11;
				JYM = "10X98765432";
				M = JYM.substring(Y,Y+1);//判断校验位
				if(chars[17] == 'x') {
					System.out.println(errors.get(5));
					return false;
				}
				if(M.equals(String.valueOf(chars[17]))){ 
					System.out.println(errors.get(0));
					return true; //检测ID的校验位
				}else{
					return false;
				}
			} 
			else{ 
				return false;
			}
		default:
			System.out.println(errors.get(1));
			return false;
		}
	}
    
	public static void main(String[] args) {
//		LoginVerifyTool tLoginVerifyTool= new LoginVerifyTool();
////		System.out.println(tLoginVerifyTool.getProductInfo("YM000910759")[2]);
//		int x = 1;	
//		while(true){
//	        x++;
//			ExeSQL tExeSQL = new ExeSQL();
//	        String sql=" select cardno from licardactiveinfolist where inactivedate is null and cvalidate is not null " 
//	        	      + "and cardtype in ('WR0151','Hwr011','WR0060','WR0028','WR0027','WR0048','WR0047','WR0041','Hwr020','D33001','WR0183','WR0122','WR0195','WR0049','WR0044','WR0046','WR0056','WR0066','Hwr002','Hwr004','Hwr006','Hwr008','Hwr005','Hwr001','Hwr003','Hwr007','Hwr012','Hwr010','Hwr009','WR0061','WR0062','WR0063','WR0080','WR0082','WR0108','WR0111','WR0120','WR0121','WR0125','WR0135','WR0136','WR0149','WR0167','WRYK01','WR0165','WRYK00','Hwr021','Hwr022','Hwr023','WR0169','WR0134','WR0194','WR0058','WR0054','WR0106','WR0133','WR0124','WR0067','WR0126','WR0146','WR0153','WR0160','WR0152','WR0166','WR0081','WR0185','WR0181','WR0184','WR0186','WR0187','Hwr029','Hwr030','Hwr031','WR0188','WR0189','WR0190','WR0191','WR0192','WR0193')"	        		
//	                  + "fetch first 1000 rows only ";
//			SSRS tSSRS=tExeSQL.execSQL(sql);
//			 PubSubmit tPubSubmit = new PubSubmit();
//		     MMap tmap = new MMap();
//			 VData mOutputData = new VData();
//			//LICardActiveInfoListSet mLICardActiveInfoListSet = new LICardActiveInfoListSet();			
//			for(int i = 1;i<=tSSRS.MaxRow;i++){			
//			    String aCardNo=tSSRS.GetText(1, i) ;
//			    String[] inactivedate=tLoginVerifyTool.getProductInfo(aCardNo);
//			    if(inactivedate[2]==null||inactivedate[2]=="") 
//			     {
//			    	continue ;
//			     } 
//			    String upSql =" update LICardActiveInfoList set InActiveDate = '"+inactivedate[2]+"' where cardno='"+aCardNo+"'"; 
//			    //LICardActiveInfoListDB tLICardActiveInfoListDB = new LICardActiveInfoListDB();
//			    //tLICardActiveInfoListDB.setCardNo(aCardNo);
//			    //LICardActiveInfoListSet tLICardActiveInfoListSet = tLICardActiveInfoListDB.query();
//			    //LICardActiveInfoListSchema tLICardActiveInfoListSchema = tLICardActiveInfoListSet.get(1);
//			    //tLICardActiveInfoListSchema.setActiveDate(inactivedate[2]);
//			    //tLICardActiveInfoListDB.update();	
//			    tmap.put(upSql, "UPDATE");			   
//			}
//			mOutputData.add(tmap);
//	        tPubSubmit.submitData(mOutputData,"");
//
////	        if(x==10){
////	        	break;
////	        }
////	        System.out.println("x---------------------------------------------------x    :    "+x);
//			if(tSSRS.MaxRow==0) return;			
//		} 
		LoginVerifyTool tLoginVerifyTool= new LoginVerifyTool();
        FDate fdate = new FDate();
		Date oldCvalidate = fdate.getDate("2016-12-10"); //结果中的生效日期，用FDate处理
        Date oldInactivedate = fdate.getDate("2017-12-10"); //结果中失效日期，用FDate处理
        Date newCvalidate = fdate.getDate("2015-12-10"); //当前激活卡的生效日期，用FDate处理
        String[] inactivedate=tLoginVerifyTool.getMyProductInfo("OE000000042", "2015-12-10");
        Date newInactivedate = fdate.getDate(inactivedate[2]); //当前激活卡的失效日期，用FDate处理 
        int alreadyNo=0;
        System.out.println(oldCvalidate);
        System.out.println(oldInactivedate);
        System.out.println(oldCvalidate);
        System.out.println(newInactivedate);
        System.out.println(oldCvalidate.compareTo(newCvalidate) <= 0 );
        System.out.println(oldInactivedate.compareTo(newCvalidate) > 0 );
        System.out.println(oldCvalidate.compareTo(newInactivedate) < 0);
        System.out.println(oldInactivedate.compareTo(newInactivedate) >= 0);
        
    	if((oldCvalidate.compareTo(newCvalidate) <= 0 && oldInactivedate.compareTo(newCvalidate) > 0 )||
    			(oldCvalidate.compareTo(newInactivedate) < 0 && oldInactivedate.compareTo(newInactivedate) >= 0) ){ 
    		//compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
    		alreadyNo++;
    		System.out.println(alreadyNo+"lcb--");
    	}
    	System.out.println(alreadyNo);
	}
         
    
    
    
    
    
    
    
    
    
    
    
    
    
}
