package com.ecwebservice.subinterface;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sinosoft.lis.certifybusiness.CertifyDiskImportLog;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;



public class CommonCheck {

	//	 错误处理类
	public CErrors mErrors = new CErrors();
	private CertifyDiskImportLog mImportLog = null;
	private GlobalInput mGlobalInput = new GlobalInput();
	String mRiskWrapCode = "";//套餐编码
	int mMult = 0;//档次
	int mCopys = 0;//份数
	double tAmnt = 0;//保额
    double tPrem = 0;//保费
	String mCValidate = ""; //生效日期
	String mCertifyCode = ""; //单证编码
	String mCardNo = "";//卡号
	String mBatchNo = "";
	double tSysWrapSumPrem = 0; //系统计算出的保费
    double tSysWrapSumAmnt = 0;//系统计算出的保额
	/** 用于输出的核保错误信息列表 */
    private List mUwErrorList = new ArrayList();
    private String Error = "";
    ExeSQL tExeSQL = new ExeSQL();

    public CommonCheck(WFContListSchema contListSchema){
    	mRiskWrapCode = contListSchema.getRiskCode();//套餐编码
    	mCardNo = contListSchema.getCardNo();
		mCValidate = contListSchema.getCvaliDate();
		mCopys = contListSchema.getCopys();
		mMult = contListSchema.getMult();
		mCertifyCode = contListSchema.getCertifyCode();
		System.out.println("\n\n\n\n走到CommonCheck.java Line 66 下一行开始 tAmnt = contListSchema.getAmnt() \n\n\n\n");
		tAmnt = contListSchema.getAmnt();
		System.out.println("\n\n\n\n走到CommonCheck.java Line 68 上一行是 tAmnt = contListSchema.getAmnt() \n\n\n\n");
		tPrem = contListSchema.getPrem();
		mBatchNo = contListSchema.getBatchNo();
		mGlobalInput.Operator = "第三方";//该功能校验第三方传送至核心的数据，则Operator默认为该名。
    }
	public boolean checkAll(WFInsuListSet tInsuListSet) {
		
		String tSql = "select code from ldcode where codetype =  'eccommoncheck' ";
		String tOorC = new ExeSQL().getOneValue(tSql);
		if(tOorC == null || "".equals(tOorC) || "0".equals(tOorC)){
			return true;
		}
		
		if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "获取批次信息失败，请检查文件名是否符合批次命名规则。");
            return false;
        }
        try
        {
            mImportLog = new CertifyDiskImportLog(mGlobalInput, mBatchNo);
        }
        catch (Exception e)
        {
            String tStrErr = "创建日志失败。";
            buildError("getInputData", tStrErr);
            e.printStackTrace();
            return false;
        }
		if(mCopys == 0 ){//当第三方向核心发送份数为0时，则为未传递份数，则认为购买1份
			mCopys =1;
		}
//		校验传送年龄 职业类别 所购买份数 是否符合产品描述
		if(!checkInsuredInfos(tInsuListSet)){
			return false;
		}
//		校验特殊规定
		if(!checkSpec(tInsuListSet)){
			String UWErrors =  this.getUWErrors();
			buildError("checkSpec",UWErrors);
			if (!mImportLog.errLog(mCardNo, UWErrors))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
			return false;
		}
//		校验保费保额
		if(!dealCertifyWrapParams()){
			return false;
		}
		return true;
	}
	private boolean checkInsuredInfos(WFInsuListSet tInsuListSet){
		for (int i = 1; i <= tInsuListSet.size(); i++) {
			String Name = tInsuListSet.get(i).getName();//姓名
			String Sex = tInsuListSet.get(i).getSex();//性别
			String Birthday = tInsuListSet.get(i).getBirthday();//出生日期
			String IDType = tInsuListSet.get(i).getIDType();//证件类型
			String IDNo = tInsuListSet.get(i).getIDNo();//证件号码
			String occupAtionType = tInsuListSet.get(i).getOccupationType();//职业类别
			
			//校验被保人年龄
			String ageWrapSql = "select case when maxinsuredage is null then -1 else maxinsuredage end,"
					+ " case when mininsuredage is null then -1 else mininsuredage end "
					+ " from ldriskwrap where riskwrapcode = '"
					+ mRiskWrapCode
					+ "' ";
			SSRS ageSSRS = tExeSQL.execSQL(ageWrapSql);
			if (ageSSRS != null && ageSSRS.MaxRow > 0) {
				if (!"".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"-1".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"0".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))
						&& !"".equals(StrTool.cTrim(ageSSRS.GetText(1, 2)))
						&& !"-1".equals(StrTool.cTrim(ageSSRS.GetText(1, 1)))) {
					int tInsurrdAge = PubFun.calInterval(Birthday, PubFun.getCurrentDate(), "Y");
					if (Integer.parseInt(ageSSRS.GetText(1, 1)) < tInsurrdAge
							|| Integer.parseInt(ageSSRS.GetText(1, 2)) > tInsurrdAge) {
						Error = "被保人年龄不在投保年龄范围内!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
				}
			}
			//校验被保人职业类别
			String occutypeWrapSql = "select maxoccutype from ldwrap where riskwrapcode = '"+ mRiskWrapCode + "' ";
			SSRS occutypeSSRS = tExeSQL.execSQL(occutypeWrapSql);
			if (occutypeSSRS != null && occutypeSSRS.MaxRow > 0) {
				if (!"".equals(StrTool.cTrim(occutypeSSRS.GetText(1, 1)))) {
					if(occupAtionType == null || "".equals(occupAtionType)){
						Error = "被保人职业类别不可以为空!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
					if(!PubFun.isNumLetter(occupAtionType)){
						Error = "被保人职业类别应为数字，请核实!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
					if (Integer.parseInt(occutypeSSRS.GetText(1, 1)) < Integer
							.parseInt(occupAtionType)) {
						Error = "被保人职业类别不在投保职业类别范围内!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
				}
			}
			//校验份数
			//准备校验SQL,校验该激活卡是否是lmcardrisk中的risktype = 'W'的类型
			String checkSql = " select lmcr.riskcode, re.CertifyCode from lmcardrisk lmcr,"
					+ "(select lzcn.CardNo, lzc.CertifyCode, lzc.State from LZCardNumber lzcn "
					+ " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and "
					+ "lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
					+ "where 1 = 1 and lzcn.CardNo = '"
					+ mCardNo
					+ "') re where lmcr.risktype = 'W' and lmcr.certifycode = re.certifycode ";
			SSRS checkSSRS = tExeSQL.execSQL(checkSql);

			String insureSql = null;
			String cardNoStr = mCardNo.toString().substring(0, 2);
			int alreadyNo = mCopys;//将本次购买份数保存到alreadyNo，然后再去加以后份数，校验总分数是否超过最大份数。
			if (checkSSRS.MaxRow != 0
					&& (mCValidate != null || mCValidate != "")) {
				insureSql = "select cardno,Cvalidate,InActiveDate from LICardActiveInfoList where cardtype='"//--------fromYZF
                        +mRiskWrapCode+"' and name='"+ Name + "' and sex='"+ Sex + "' and birthday='"+ Birthday
						+ "' and idtype='"+ IDType + "' and idno='"	+ IDNo	+ "' and InActiveDate>'"+ PubFun.getCurrentDate()+ "' "
						+"and (CardStatus = '01' or CardStatus is null)";
				SSRS insureSSRS = tExeSQL.execSQL(insureSql);
				FDate fdate = new FDate();
				LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
				for (int j = 1; j <= insureSSRS.MaxRow; j++) {
					Date oldCvalidate = fdate.getDate(insureSSRS.GetText(j, 2)); //结果中的生效日期，用FDate处理
					Date oldInactivedate = fdate.getDate(insureSSRS.GetText(j,3)); //结果中失效日期，用FDate处理
					Date newCvalidate = fdate.getDate(mCValidate); //当前激活卡的生效日期，用FDate处理
					String[] inactivedate = tLoginVerifyTool.getMyProductInfo(mCardNo, mCValidate,String.valueOf(mMult));
					Date newInactivedate = fdate.getDate(inactivedate[2]); //当前激活卡的失效日期，用FDate处理		        
					if ((oldCvalidate.compareTo(newCvalidate) <= 0 && oldInactivedate.compareTo(newCvalidate) > 0)
							|| (oldCvalidate.compareTo(newInactivedate) < 0 && oldInactivedate.compareTo(newInactivedate) >= 0)) {
						//compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
						alreadyNo++;
					}
				}
				String maxNoSql = null;
				//取最大份数
				maxNoSql = " select maxcopys from ldwrap where riskwrapcode='" + mRiskWrapCode + "' ";
				String temp2 = tExeSQL.execSQL(maxNoSql).GetText(1, 1);
				int maxNo = 0;
				if (temp2.matches("^\\d+$")) {
					maxNo = Integer.parseInt(temp2);
					if (maxNo < alreadyNo) {
						Error = "被保人投保份数已超过可投保的最大份数!";
						buildError("checkInsuredInfos",Error);
						if (!mImportLog.errLog(mCardNo, Error))
			            {
			                mErrors.copyAllErrors(mImportLog.mErrors);
			                return false;
			            }
						return false;
					}
				}
			} else {
				insureSql = "select count(1) from LICardActiveInfoList where cardtype='"//--------fromYZF
                        +mRiskWrapCode+"' and name='" + Name + "' and sex='" + Sex
						+ "' and birthday='" + Birthday	+ "' and idtype='"	+ IDType
						+ "' and idno='" + IDNo	+ "' ";
				String temp1 = tExeSQL.execSQL(insureSql).GetText(1, 1);
				if (temp1.matches("^\\d+$")) {
					alreadyNo += Integer.parseInt(temp1);
				}
				String maxNoSql = null;
				maxNoSql = " select maxcopys from ldwrap where riskwrapcode='"+ mRiskWrapCode + "' ";
				String temp2 = tExeSQL.execSQL(maxNoSql).GetText(1, 1);
				int maxNo = 0;
				if (temp2.matches("^\\d+$")) {
					maxNo = Integer.parseInt(temp2);
					if (maxNo < alreadyNo) {
						Error = "被保人投保份数已超过可投保的最大份数!";
						buildError("checkInsuredInfos",Error);
						return false;
					}
				}
			}
		}
		return true;
	}
	/**
     * 对产品的特殊规定进行校验。
     * @param insuListSet
     * @return
     */
	private boolean checkSpec(WFInsuListSet insuListSet){
		boolean flag = true;
		LMCheckFieldDB tLMCheckFieldDB= new LMCheckFieldDB();
		tLMCheckFieldDB.setRiskCode(mRiskWrapCode);
		tLMCheckFieldDB.setFieldName("ETBINSERT");
		LMCheckFieldSet tLMCheckFieldSet = tLMCheckFieldDB.query();
		for(int i=1;i<=tLMCheckFieldSet.size();i++){
			LMCheckFieldSchema tLMCheckFieldSchema = tLMCheckFieldSet.get(i);
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			tLMCalModeDB.setCalCode(tLMCheckFieldSchema.getCalCode());
			LMCalModeSet tLMCalModeSet = new LMCalModeSet();
			tLMCalModeSet = tLMCalModeDB.query();
			for(int j=1;j<=tLMCalModeSet.size();j++){
				LMCalModeSchema tLMCalModeSchema = tLMCalModeSet.get(j);
				for(int k=1;k<=insuListSet.size();k++){
					WFInsuListSchema insuListSchema = insuListSet.get(k);
					int tAppntAge = PubFun.calInterval(insuListSchema.getBirthday(),
							PubFun.getCurrentDate(), "Y");
					Calculator cal = new Calculator();
					cal.setCalCode(tLMCalModeSchema.getCalCode());
					cal.addBasicFactor("InsuredName", insuListSchema.getName());
					cal.addBasicFactor("Sex", insuListSchema.getSex());
					cal.addBasicFactor("InsuredBirthday", insuListSchema.getBirthday());
					cal.addBasicFactor("IDType", insuListSchema.getIDType());
					cal.addBasicFactor("IDNo", insuListSchema.getIDNo());
					cal.addBasicFactor("RiskWrapCode", mRiskWrapCode);
					cal.addBasicFactor("AppAge",tAppntAge+"");
					cal.addBasicFactor("Mult",mMult+"");
					cal.addBasicFactor("OccupationType",insuListSchema.getOccupationType());
					cal.addBasicFactor("Cvalidate",mCValidate);
					String result = cal.calculate();
					if(!result.equals("0")){
						flag = false;
						mUwErrorList.add(tLMCalModeSchema.getRemark());
					}
				}
			}
		}
		return flag;
	}
    /**
     * 处理单证关键要素（保额、保费、档次、份数等）。
     * @param contListSchema
     * @return
     */
    private boolean dealCertifyWrapParams()
    {

        String tCopysFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Copys' and lmcr.CertifyCode = '" + mCertifyCode + "' ");
        if (!"1".equals(tCopysFlag) && mCopys != 1)
        {
            String tStrErr = "单证号[" + mCardNo + "]对应套餐份数不能大于1。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);
            if (!mImportLog.errLog(mCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        // -------------------------------------------

        // 校验档次，但不对POS单证进行档次的对应校验
        String tMultFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + mCertifyCode + "' " + "union all "
                + " select 1 from LMCertifyDes lmcd where lmcd.CertifyCode = '" + mCertifyCode
                + "' and lmcd.OperateType = '3'");
        if ("1".equals(tMultFlag))
        {
        	String MultSql = "select calfactortype,calfactorvalue from LDRiskDutyWrap ldrdw inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
        		+"where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + mCertifyCode + "' ";
        	SSRS tSSRS = new ExeSQL().execSQL(MultSql);
        	if(tSSRS != null && tSSRS.MaxRow>0){
        		if("2".equals(tSSRS.GetText(1, 1)) && mMult == 0){//当需要传递档次，而档次为0时，则阻断。
                	String tStrErr = "单证号[" + mCardNo + "]对应套餐需含有档次要素。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    if (!mImportLog.errLog(mCardNo, tStrErr))
                    {
                        mErrors.copyAllErrors(mImportLog.mErrors);
                        return false;
                    }
                    return false;
        		}
        	}
        }else{
    		if(mMult != 0){//没有档次要素，当传递档次时，则阻断
            	String tStrErr = "单证号[" + mCardNo + "]对应套餐不应含有档次要素。";
                buildError("dealCertifyWrapParams", tStrErr);
                if (!mImportLog.errLog(mCardNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        // -------------------------------------------

        LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

        String tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
                + " where lmcr.CertifyCode = '"
                + mCertifyCode
                + "' "
                + " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
        tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
        {
            String tStrErr = "单证号[" + mCardNo + "]对应套餐要素信息未找到。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(mCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        // 处理要素信息。
        for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++)
        {
            LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);

            String tCalFactorType = tRiskDutyParam.getCalFactorType();
            String tCalFactor = tRiskDutyParam.getCalFactor();
            String tCalFactorValue = tRiskDutyParam.getCalFactorValue();

            if ("Prem".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(mCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(mMult));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + mCardNo + "]保费计算失败，请核查算费参数。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        System.out.println(tStrErr);

                        if (!mImportLog.errLog(mCardNo, tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumPrem = Arith.add(tSysWrapSumPrem, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保费出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }

            if ("Amnt".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(mCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(mMult));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + mCardNo + "]保额计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        System.out.println(tStrErr);

                        if (!mImportLog.errLog(mCardNo, tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                        // tTmpCalResult = "0";
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保额出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }
        }
        // ------------------------------------------------

        if (tPrem != 0 && tPrem != tSysWrapSumPrem)
        {
            String tStrErr = "单证号[" + mCardNo + "]系统计算出保费：" + tSysWrapSumPrem + "，与获取保费：" + tPrem + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(mCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        if (tAmnt != 0 && tAmnt != tSysWrapSumAmnt)
        {
            String tStrErr = "单证号[" + mCardNo + "]系统计算出保额：" + tSysWrapSumAmnt + "，与获取保额：" + tAmnt + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);
            if (!mImportLog.errLog(mCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            
            return false;
        }

        return true;
    }

	/**
     * 得到卡单激活未通过保原因
     * @return String
     * @date 20101122
     */
    public String getUWErrors()
    {
        String uwErrors = "";
        Set set = new HashSet();
        set.addAll(mUwErrorList);
        ArrayList mUwErrorList1 = new ArrayList();
        mUwErrorList1.addAll(set);
        for (int i = 0; i < mUwErrorList1.size(); i++)
        {
        	uwErrors += (String) mUwErrorList1.get(i) ;
            if(uwErrors.length()>0 && i<(mUwErrorList1.size()-1)) uwErrors+= ";";
        }
        return uwErrors;
    }
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	/**
	 * @return the tSysWrapSumAmnt
	 */
	public double getTSysWrapSumAmnt() {
		return tSysWrapSumAmnt;
	}
	/**
	 * @param sysWrapSumAmnt the tSysWrapSumAmnt to set
	 */
	public void setTSysWrapSumAmnt(double sysWrapSumAmnt) {
		tSysWrapSumAmnt = sysWrapSumAmnt;
	}
	/**
	 * @return the tSysWrapSumPrem
	 */
	public double getTSysWrapSumPrem() {
		return tSysWrapSumPrem;
	}
	/**
	 * @param sysWrapSumPrem the tSysWrapSumPrem to set
	 */
	public void setTSysWrapSumPrem(double sysWrapSumPrem) {
		tSysWrapSumPrem = sysWrapSumPrem;
	}
	/**
	 * @return the mErrors
	 */
	public CErrors getMErrors() {
		return mErrors;
	}
	/**
	 * @param errors the mErrors to set
	 */
	public void setMErrors(CErrors errors) {
		mErrors = errors;
	}
}
