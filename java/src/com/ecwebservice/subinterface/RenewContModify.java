package com.ecwebservice.subinterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;


import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;


import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.tb.RenewalContModifyUI;
import com.sinosoft.lis.vdb.LCDutyDBSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * Title:电商续保接口维护，lcduty  freerate 改为0
 * 
 * @author licaiyan 创建时间：2014-4-25 
 * @version
 */

public class RenewContModify {

    /**
     * @param args
     */
    // 保险卡信息
    private String batchno = null;// 批次号

    private String sendDate = null;// 报文发送日期

    private String sendTime = null;// 报文发送时间

    private String sendOperator = "EC_WX";// g

    private String messageType = null;// 报文类型

    MMap map = new MMap();

    VData tVData = new VData();

    // 错误处理类
    public CErrors mErrors = new CErrors();

    // 印刷号
    private String contNo = null;

    public GlobalInput mGlobalInput = new GlobalInput();//公共信息

    public OMElement queryData(OMElement Oms) {

        OMElement responseOME = null;
        boolean fieldCompleteFlag = true;// 非空字段完整性标志
        String emptyField = null;// 空值字段
        try {

            Iterator iter1 = Oms.getChildren();
            while (iter1.hasNext()) {
                OMElement Om = (OMElement) iter1.next();

                if (Om.getLocalName().equals("RENEWALMAINTEN")) {

                    Iterator child = Om.getChildren();

                    while (child.hasNext() && fieldCompleteFlag) {
                        OMElement child_element = (OMElement) child.next();

                        if (child_element.getLocalName().equals("BATCHNO")
                                && child_element.getText() != "") {
                            batchno = child_element.getText();
                            System.out.println("BATCHNO" + batchno);
                        
                        }
                        if (child_element.getLocalName().equals("BATCHNO")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "BATCHNO";
                            System.out.println("执行else BATCHNO");
                            break;
                        }
                        if (child_element.getLocalName().equals("SENDDATE")
                                && child_element.getText() != "") {
                            sendDate = child_element.getText();

                            System.out.println(sendDate);
                        }
                        if (child_element.getLocalName().equals("SENDDATE")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "SENDDATE";
                            System.out.println("执行else SENDDATE");
                            break;
                        }
                        if (child_element.getLocalName().equals("SENDTIME")
                                && child_element.getText() != "") {
                            sendTime = child_element.getText();

                            System.out.println(sendTime);
                        }
                        if (child_element.getLocalName().equals("SENDTIME")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "SENDTIME";
                            break;
                        }
                    
                        if (child_element.getLocalName().equals("MESSAGETYPE")
                                && child_element.getText() != "") {
                            messageType = child_element.getText();

                            System.out.println(messageType);
                            if (!"EX_RENEWAL".equals(messageType)) {
                                responseOME = buildResponseOME("01", "5", "报文类型不正确!");
                                System.out
                                        .println("responseOME:" + responseOME);

                                LoginVerifyTool.writeLog(responseOME, batchno,
                                        messageType, sendDate, sendTime, "01",
                                        "100", "5", "报文类型不正确!");
                                return responseOME;
                            }
                        }
                        if (child_element.getLocalName().equals("MESSAGETYPE")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "MESSAGETYPE";
                            break;
                        }
                        if (child_element.getLocalName().equals("CONTNO")
                                && child_element.getText() == "") {
                            fieldCompleteFlag = false;
                            // 记录为空的字段
                            emptyField = "MESSAGETYPE";
                            break;
                        }else{
                        	this.contNo = child_element.getText();
                        }
                    }
                }
            }
            responseOME = checkData();
            if(responseOME!=null){
            	return responseOME;
            }
	        System.out.println("为下次续保维护保单，contNo："+contNo);
	        VData tVData = new VData();
	        MMap map = new MMap();
			
			LCDutySet allLCDutySet = new LCDutySet();
			if(contNo != null && !"".equals(contNo)){
				String [] tempArr = contNo.split(",");
				
				for(int m=0;m<tempArr.length;m++){
					LCDutyDB dutydb = new LCDutyDB();
					dutydb.setContNo(tempArr[m]);
					LCDutySet tLCDutySet = dutydb.query();
					
					if(tLCDutySet != null && tLCDutySet.size() > 0){
						for(int i=1;i<=tLCDutySet.size();i++){
							LCDutySchema tLCDutySchema = tLCDutySet.get(i);
							tLCDutySchema.setFreeRate("0");
							tLCDutySchema.setModifyDate(PubFun.getCurrentDate());
							tLCDutySchema.setModifyTime(PubFun.getCurrentTime());
							allLCDutySet.add(tLCDutySchema);
							
						}
					}
					
				}
			}
			map.put(allLCDutySet,"UPDATE");
			tVData.add(map);
			
			tVData.add(mGlobalInput);
			RenewalContModifyUI tRenewalContModifyUI = new RenewalContModifyUI();
			boolean flag = tRenewalContModifyUI.submitData(tVData,"UPDATE");
        	System.out.println("为将来续保操作维护--结束");
            if (!flag) {
    			String error = tRenewalContModifyUI.mErrors.getFirstError();
                // @@错误处理
                
                responseOME = buildResponseOME( "01",
                        "6", error);
                System.out.println("responseOME:" + responseOME);
                LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                        sendDate, sendTime, "01", sendOperator, "6",
                        error);
                return responseOME;
            } else {
                responseOME = buildResponseOME("00", "0", "成功!");
                System.out.println("responseOME:" + responseOME);
                LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                        sendDate, sendTime, "00", sendOperator, "0", "成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
            responseOME = buildResponseOME( "01", "4",
                    "发生异常");
            System.out.println("responseOME:" + responseOME);
            LoginVerifyTool.writeLog(responseOME, batchno, messageType,
                    sendDate, sendTime, "01", sendOperator, "4", "发生异常");
            return responseOME;
        }
        return responseOME;

    }

    private OMElement buildResponseOME( String state, String errCode, String errInfo) {
        OMFactory fac = OMAbstractFactory.getOMFactory();

        OMElement DATASET = fac.createOMElement("DELETECONT", null);
        LoginVerifyTool tool = new LoginVerifyTool();
        tool.addOm(DATASET, "BATCHNO", batchno);
        tool.addOm(DATASET, "SENDDATE", sendDate);
        tool.addOm(DATASET, "SENDTIME", sendTime);

        tool.addOm(DATASET, "STATE", state);
        tool.addOm(DATASET, "ERRCODE", errCode);
        tool.addOm(DATASET, "ERRINFO", errInfo);

        return DATASET;
    }
    private OMElement checkData(){
    	
    	mGlobalInput.ManageCom = "86";
    	mGlobalInput.Operator = "EC_WX";
    	OMElement responseOME = null;
    	ExeSQL tExeSQL = new ExeSQL();
    	if(contNo != null && !"".equals(contNo)){
    		String tempArr [] = contNo.split(",");
    		String tempContno = "";
    		for(int m=0;m<tempArr.length;m++){
    			tempContno = tempArr[m];
    			String isECsql = "select cardflag from lccont where contno = '"+tempContno+"'";
    	    	String cardflag = tExeSQL.getOneValue(isECsql);
    	    	if(!cardflag.equals("b")){
    	    		responseOME = buildResponseOME("01", "7", "保单不是网销保单，不能进行续保维护!");
    	            System.out.println("responseOME:" + responseOME);

    	            LoginVerifyTool.writeLog(responseOME, batchno,
    	                    messageType, sendDate, sendTime, "01",
    	                    "100", "7", "保单不是网销保单，不能进行续保维护!");
    	            return responseOME;
    	    	}
    	    	String isSignsql = "select appflag from lccont where contno = '"+tempContno+"'";
    	    	String signflag = tExeSQL.getOneValue(isSignsql);
    	    	if(!signflag.equals("1")){
    	    		responseOME = buildResponseOME("01", "7", "不是有效网销保单，不能进行续保维护!");
    	            System.out.println("responseOME:" + responseOME);

    	            LoginVerifyTool.writeLog(responseOME, batchno,
    	                    messageType, sendDate, sendTime, "01",
    	                    "100", "7", "不是有效网销保单，不能进行续保维护!");
    	            return responseOME;
    	    	}
    	    	String isFeesql = "select prtno from lccont a where 1=1 and a.contno = '"+tempContno+"'"
    	    		 +" and (exists(select 1 from ljtempfee where otherno = a.contno)"
    	    		 +" or exists(select 1 from ljtempfee where otherno = a.prtno))";
    	    	String feeflag = tExeSQL.getOneValue(isFeesql);
    	    	if("".equals(feeflag)){
    	    		responseOME = buildResponseOME("01", "7", "保单没有交费，不能为下次续保进行维护!");
    	            System.out.println("responseOME:" + responseOME);

    	            LoginVerifyTool.writeLog(responseOME, batchno,
    	                    messageType, sendDate, sendTime, "01",
    	                    "100", "7", "保单没有交费，不能为下次续保进行维护!");
    	            return responseOME;
    	    	}
    		}
    	}
    	
    	return null;
    }

    public static void main(String[] args) {

    	
    	
    }

}
