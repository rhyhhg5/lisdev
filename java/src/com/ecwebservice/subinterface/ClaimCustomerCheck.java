package com.ecwebservice.subinterface;

import java.util.Iterator;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.certifybusiness.WSCertifyUI;
import com.sinosoft.lis.crminterface.LLMainAskBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LICrmClaimSchema;
import com.sinosoft.lis.vschema.LICrmClaimSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * Title:
 * 
 * @author zhangyige 创建时间：2012-4-12
 * @Description:使用5要素来进行校验
 * @version
 */

public class ClaimCustomerCheck implements CardHYX {

	private LICrmClaimSet mLICrmClaimSet = new LICrmClaimSet();
	
	private String errors = "";

	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String ReportName = null;//报案人姓名
	
	private String Relation = null;//	 报案人与被保人关系 
	   
	private String ReportSex = null;//	 报案人性别 	
	   
	private String ReportIdType = null;//	 报案人证件类型     
	   
	private String ReportIdNo = null;//	 报案人证件号码     
	   
	private String ReportBirthday = null;// 报案人出生日期   
	
	private String Address = null;//	 报案人地址
		     
	private String Phone = null;//	 报案人电话      
		      
	private String Mobile = null;//	 报案人手机    
		 
	private String Email = null;//	 报案人电邮
		     
	private String PostCode = null;//	 报案人邮政编码     
	
	private String ReportDate = null;//	 报案日期     
	   
	private String InsuName = null;//	 出险人名称  
	   
	private String Sex = null;//	 出险人性别  
	   
	private String Birthday = null;//	 出险人年龄  
	   
	private String IdType = null;//	 出险人证件类型     
	   
	private String IdNo = null;//	 出险人证件号码     
	
	private String branch = null;//	 所属分公司    
	
	private String cardno = null;//	 卡号     
	   
	private String HappenDate = null;//	 出险日期    
	   
	private String Describe = null;//	 出险描述  
	
	public OMElement queryData(OMElement Oms) {
		System.out.println("进入在线理赔客户五要素校验处理类！");
		OMElement responseOME = null;
		//获取电子商务报文，并解析
		responseOME = getData(Oms);
		if (responseOME != null) {
			return responseOME;
		}
		//对被保人进行校验
		errors = this.perCheck(Oms);
		System.out.println(errors + "=========errors");
		if (errors.indexOf("E")!=-1) {
			errors = errors.substring(1);
			responseOME = buildResponseOME("01", "*", errors);
			return responseOME;
		} else {
			//在核心系统生成报案信息
			responseOME = saveClaimInfo(errors);
			return responseOME;
		}
	}

	/*
	 * 正确 返回客户号
	 * 错误返回提示信息
	 */
	private String perCheck(OMElement Oms) {
		String ErrInfo = "";
		// 根据五要素判断是否为被保人
		ExeSQL tExeSQL = new ExeSQL();
		String ChecklcSql = "select insuredno from lcinsured where name = '"
				+ InsuName + "'" + "and sex='" + Sex
				+ "' and idtype='" + IdType + "' and idno='"
				+ IdNo + "' and birthday='" + Birthday
				+ "'";
		System.out.println("校验lcinsured中是否为被保人：" + ChecklcSql);
		String ChecklbSql = "select insuredno from lbinsured where name = '"
				+ InsuName + "'" + "and sex='" + Sex
				+ "' and idtype='" + IdType + "' and idno='"
				+ IdNo + "' and birthday='" + Birthday
				+ "'";
		System.out.println("校验lbinsured中是否为被保人：" + ChecklbSql);
		SSRS lcSSRS = tExeSQL.execSQL(ChecklcSql);
		if (lcSSRS.MaxRow > 0) {
			ErrInfo = lcSSRS.GetText(1, 1);
			return ErrInfo;
		} else {
			SSRS lbSSRS = tExeSQL.execSQL(ChecklbSql);
			if (lbSSRS.MaxRow > 0 ) {
				ErrInfo = lbSSRS.GetText(1, 1);
				return ErrInfo;
			}
		}
		if(cardno.equals("")||cardno==null){
			ErrInfo = "E无对应被保人信息，如果为卡折客户，请输入卡号！";
			return ErrInfo;
		}else{
			String CheckSql = "select 1 from licardactiveinfolist where name = '"
				+ InsuName + "'" + "and sex='" + Sex
				+ "' and idtype='" + IdType + "' and idno='"
				+ IdNo + "' and birthday='" + Birthday
				+ "' and cardno = '"+ cardno +"'";
			System.out.println("校验是否为卡折被保人："+CheckSql);
			SSRS tSSRS = tExeSQL.execSQL(CheckSql);
			if (tSSRS.MaxRow == 1) {
				//被保人为卡折客户需要先对其进行实名化，在获取客户号
				boolean flag = false;
				flag = dealWSCertify(cardno);
				if(flag){
					SSRS pSSRS = tExeSQL.execSQL(ChecklcSql);
					if (pSSRS.MaxRow > 0) {
						ErrInfo = pSSRS.GetText(1, 1);
						System.out.println("ErrInfo:"+ErrInfo);
						return ErrInfo;
					}
				}else{
					ErrInfo = "E在线报案失败，不能找到被保人信息，请您确认被保人信息是否填写正确。您也可以通过客服电话95591报案！";
					return ErrInfo;
				}
			}else{
				ErrInfo = "E在线报案失败，不能找到被保人信息，请您确认被保人信息是否填写正确。您也可以通过客服电话95591报案！";
				return ErrInfo;
			}			
		}
		return ErrInfo;
	}
	//对卡折进行实名化，调用核心程序
	public boolean dealWSCertify(String cardno){
		
		LICertifySchema tLICertifySchema = null;
	    tLICertifySchema = new LICertifySchema();
	    tLICertifySchema.setCardNo(cardno);
	    VData tVData = new VData();

	    TransferData tTransferData = new TransferData();
	    GlobalInput tGI = new GlobalInput();
	    //电子商务端实名化，由于没有登录信息
	    tGI.ManageCom = "86";  //默认管理机构
	    tGI.Operator = "ec";   //默认操作员
	    tVData.add(tLICertifySchema);
	    tVData.add(tTransferData);
	    tVData.add(tGI);
	    
	    WSCertifyUI tWSCertifyUI = new WSCertifyUI();
	    if(!tWSCertifyUI.submitData(tVData, "WSCertify"))
	    {
	    	String  Content = " 处理失败，原因是: " + tWSCertifyUI.mErrors.getFirstError();
	    	System.out.println("卡折实名化未成功的原因是："+Content);
	        return false;
	    }
	    else
	    {
	        return true;
	    }
	}
	//在核心生成报案信息
	public OMElement saveClaimInfo(String customerno) {
		OMElement responseOME = null;
		//组装中间表数据
		boolean flag = createLICrm(customerno);
		if(flag){
			VData mVData = new VData();
	        if (mLICrmClaimSet.size()>0)
	        {
	            VData tVData = new VData();
	            MMap mMMap = new MMap();
	            mMMap.put(mLICrmClaimSet,"INSERT");
	            tVData.add(mMMap);
	            if(doSaveLICrmClaim(tVData)){
	            	mVData.add(mLICrmClaimSet);
	            	LLMainAskBL tLLMainAskBL = new LLMainAskBL();
	            	//生成理赔报案一套核心表
		            tLLMainAskBL.submitData(mVData);
	            }else{
	            	responseOME = buildResponseOME("01", "*", "保存报案信息失败，提交数据失败！");
	        		return responseOME;	
	            }
	        }else{
	        	responseOME = buildResponseOME("00", "*", "保存报案信息失败，提交数据失败！");
	    		return responseOME;
	        }
		} 
		String state = "00";
		responseOME = buildResponseOME(state, "*", "在核心系统报案成功！");
		return responseOME;

	}
	//保存中间表数据
	private boolean doSaveLICrmClaim(VData data){
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start save LICrmClaim中间表数据...");
		if (!tPubSubmit.submitData(data, null))
		{
			return false;
		}
		System.out.println("End save LICrmClaim中间表数据...");
		return true;
	}
	//组装中间表数据
	private boolean createLICrm(String customerno)
    {
		String mToday = PubFun.getCurrentDate();
		String mTime = PubFun.getCurrentTime();
		String mBatchNo = PubFun1.CreateMaxNo("CRMBATCHNO", 20);
		String aSerialno = PubFun1.CreateMaxNo("CRMSERIALNO", 10);
		int customerAge = 0;
        LICrmClaimSchema mLICrmClaimSchema = new LICrmClaimSchema();
        mLICrmClaimSchema.setBatchNo(mBatchNo);
        mLICrmClaimSchema.setRgtType("1");   //1-电话报案 （实际为网上报案，与电话报案效果一样，暂时记为电话报案）
        if (ReportName != null || !ReportName.equals(""))
           mLICrmClaimSchema.setRptorName(ReportName);
        if (ReportSex != null || !ReportSex.equals(""))
        	mLICrmClaimSchema.setRptorSex(ReportSex);
        if (Relation != null || !Relation.equals(""))
           mLICrmClaimSchema.setRelation(Relation);
        if (Address != null || !Address.equals(""))
           mLICrmClaimSchema.setRptorAddress(Address);
        if (Phone != null || !Phone.equals(""))
           mLICrmClaimSchema.setRptorPhone(Phone);
        if (Mobile != null || !Mobile.equals(""))
           mLICrmClaimSchema.setRptorMobile(Mobile);
        if (Email != null || !Email.equals(""))
           mLICrmClaimSchema.setEmail(Email);
        if (PostCode != null || !PostCode.equals(""))
           mLICrmClaimSchema.setPostCode(PostCode);
        if (ReportIdType != null || !ReportIdType.equals(""))
           mLICrmClaimSchema.setRptorIDType(ReportIdType);
        if (ReportIdNo != null || !ReportIdNo.equals(""))
            mLICrmClaimSchema.setRptorIDNo(ReportIdNo);
        if (ReportDate != null || !ReportDate.equals(""))
            mLICrmClaimSchema.setRptDate(ReportDate);
        if (customerno!= null || !customerno.equals(""))
            mLICrmClaimSchema.setCustomerNo(customerno);
        if (InsuName != null || !InsuName.equals(""))
            mLICrmClaimSchema.setCustomerName(InsuName);
        if (Sex != null || !Sex.equals(""))
            mLICrmClaimSchema.setCustomerSex(Sex);
        customerAge = PubFun.getInsuredAppAge(PubFun.getCurrentDate(), Birthday);
        if (customerAge>0)
           mLICrmClaimSchema.setCustomerAge(customerAge);
        if (IdType != null || !IdType.equals(""))
           mLICrmClaimSchema.setCustomerIDType(IdType);
        if (IdNo != null || !IdNo.equals(""))
           mLICrmClaimSchema.setCustomerIDNo(IdNo);
        if (HappenDate != null || !HappenDate.equals(""))
           mLICrmClaimSchema.setAccidentDate(HappenDate);
        if (Describe != null || !Describe.equals(""))
           mLICrmClaimSchema.setAccdentDesc(Describe);
        mLICrmClaimSchema.setSerialno(aSerialno);
        mLICrmClaimSchema.setOperator("ec_claim");
        mLICrmClaimSchema.setMakeDate(mToday);
        mLICrmClaimSchema.setMakeTime(mTime);
        mLICrmClaimSet.add(mLICrmClaimSchema);
        return true;
    }
	//解析报文内容
	public OMElement getData(OMElement Oms) {
		OMElement responseOME = null;
		try {
			Iterator iter1 = Oms.getChildren();
			// 至根节点
			OMElement Om = (OMElement) iter1.next();

			Iterator inputInfoIterator = Om.getChildren();
			while (inputInfoIterator.hasNext()) {
				// 至baseInfo
				OMElement baseInfoOme = (OMElement) inputInfoIterator.next();
				if (baseInfoOme.getLocalName().equalsIgnoreCase("BASEINFO")) {
					Iterator itemIterator = baseInfoOme.getChildren();
					while (itemIterator.hasNext()) {
						// 至item
						OMElement child_element = (OMElement) itemIterator.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("MESSAGETYPE")) {
							messageType = child_element.getText();

							System.out.println(messageType);
							if (!"01".equals(messageType)) {
								responseOME = buildResponseOME("01", "5",
										"报文类型不正确!");
								System.out
										.println("responseOME:" + responseOME);

								LoginVerifyTool.writeLog(responseOME, batchno,
										messageType, sendDate, sendTime, "01",
										"100", "5", "报文类型不正确!");
								return responseOME;
							}
						}
					}
				}
				if (baseInfoOme.getLocalName().equalsIgnoreCase("INPUTDATA")) {
					Iterator itemIterator = baseInfoOme.getChildren();
					while (itemIterator.hasNext()) {
						// 至item
						OMElement child_element = (OMElement) itemIterator.next();

						if (child_element.getLocalName().equals("REPORTNAME")) {
							ReportName = child_element.getText();

							System.out.println(ReportName);
						}
						if (child_element.getLocalName().equals("RELATION")) {
							Relation = child_element.getText();
							
							System.out.println(Relation);
						}
						if (child_element.getLocalName().equals("REPORTSEX")) {
							ReportSex = child_element.getText();

							System.out.println(ReportSex);
						}
						if (child_element.getLocalName().equals("REPORTIDTYPE")) {
							ReportIdType = child_element.getText();

							System.out.println(ReportIdType);
						}
						if (child_element.getLocalName().equals("REPORTIDNO")) {
							ReportIdNo = child_element.getText();

							System.out.println(ReportIdNo);
						}
						if (child_element.getLocalName().equals("REPORTBIRTHDAY")) {
							ReportBirthday = child_element.getText();
							
							System.out.println(ReportBirthday);
						}
						if (child_element.getLocalName().equals("ADDRESS")) {
							Address = child_element.getText();
							
							System.out.println(Address);
						}
						if (child_element.getLocalName().equals("PHONE")) {
							Phone = child_element.getText();
							
							System.out.println(Phone);
						}
						if (child_element.getLocalName().equals("MOBILE")) {
							Mobile = child_element.getText();
							
							System.out.println(Mobile);
						}
						if (child_element.getLocalName().equals("EMAIL")) {
							Email = child_element.getText();
							
							System.out.println(Email);
						}
						if (child_element.getLocalName().equals("POSTCODE")) {
							PostCode = child_element.getText();
							
							System.out.println(PostCode);
						}
						if (child_element.getLocalName().equals("REPORTDATE")) {
							ReportDate = child_element.getText();
							
							System.out.println(ReportDate);
						}
						if (child_element.getLocalName().equals("INSUNAME")) {
							InsuName = child_element.getText();
							
							System.out.println(InsuName);
						}
						if (child_element.getLocalName().equals("SEX")) {
							Sex = child_element.getText();
							
							System.out.println(Sex);
						}
						if (child_element.getLocalName().equals("BIRTHDAY")) {
							Birthday = child_element.getText();
							
							System.out.println(Birthday);
						}
						if (child_element.getLocalName().equals("IDTYPE")) {
							IdType = child_element.getText();
							
							System.out.println(IdType);
						}
						if (child_element.getLocalName().equals("IDNO")) {
							IdNo = child_element.getText();
							
							System.out.println(IdNo);
						}
						if (child_element.getLocalName().equals("BRANCH")) {
							branch = child_element.getText();
							
							System.out.println("分公司："+branch);
						}
						if (child_element.getLocalName().equals("CARDNO")) {
							cardno = child_element.getText();
							
							System.out.println(cardno);
						}
						if (child_element.getLocalName().equals("HAPPENDATE")) {
							HappenDate = child_element.getText();
							
							System.out.println(HappenDate);
						}
						if (child_element.getLocalName().equals("DESCRIBE")) {
							Describe = child_element.getText();
							
							System.out.println(Describe);
						}
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("01", "4", "发生异常");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "100", "4", "发生异常");
			return responseOME;
		}
		return responseOME;
	}
	//返回报文
	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("CLAIMCUSTOMERCHECK", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
