package com.ecwebservice.subinterface;

import java.util.Iterator;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class QueryCont {

	private GlobalInput mGlobalInput = new GlobalInput();

	private OMElement mOME = null;

	private String batchNo = null;// 批次号

	private String branchCode = "";// 报送单位

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String sendOperator = ""; // 报送人员

	private String state = null; // 处理状态：00－成功；01－失败

	private String name = ""; // 客户姓名

	private String idType = ""; // 证件类型

	private String idNo = ""; // 证件号码

	private MMap mCont = new MMap(); // key--保单号 ,value--客户类型(0-投保人;1-被保险人)

	private OMElement responseOME = null;

	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if (!load()) {
			return responseOME;
		}

		if (!check()) {
			return responseOME;
		}

		if (!dealData()) {
			responseOME = buildResponseOME("01", "03", "没有查询到客户的保单信息");
			return responseOME;
		}
		responseOME = buildResponseOME("00", "", "");
		return responseOME;
	}

	private boolean check() {

		if ((this.name == null || this.name == "")
				&& (this.idType == null || this.idType == "")
				&& (this.idNo == null || this.idNo == "")) {
			responseOME = buildResponseOME("01", "04", "客户的姓名、证件类型、证件号码全为空！");
			return false;
		}

		return true;
	}

	private boolean dealData() {
		String[] contInfo = null;

		// String appSql="select a.contno," +
		// " (select codename from ldcode where codetype='stateflag' and code=b.stateflag) "
		// +
		// " from lcappnt a,lccont b where  a.contno=b.contno and b.appflag='1' "
		// +
		// " and a.appntname='"+this.name+"' " +
		// " and a.idtype='"+this.idType+"'" +
		// " and a.idno='"+this.idNo+"' " +
		// " with ur";
		// SSRS tSSRS = new SSRS();
		//
		// tSSRS = new ExeSQL().execSQL(appSql);
		// if (null != tSSRS && tSSRS.MaxRow > 0){
		// for(int i=1;i<=tSSRS.getMaxRow();i++){
		// contInfo=new String[3];
		//
		// contInfo[0]=tSSRS.GetText(i,1);
		// contInfo[1]="0";
		// contInfo[2]=tSSRS.GetText(i,2);
		// this.mCont.put(contInfo[0], contInfo);
		// }
		// }
		//
		// String insureSql="select a.contno," +
		// " (select codename from ldcode where codetype='stateflag' and code=b.stateflag) "
		// +
		// " from lcinsured a,lccont b where a.contno=b.contno and b.appflag='1' "
		// +
		// " and a.idtype='"+this.idType+"' " +
		// " and a.idno='"+this.idNo+"' " +
		// " and a.name='"+this.name+"' " +
		// " and a.insuredno<>b.appntno " +
		// " with ur";
		//
		// tSSRS = new ExeSQL().execSQL(insureSql);
		// if (null != tSSRS && tSSRS.MaxRow > 0){
		// for(int i=1;i<=tSSRS.getMaxRow();i++){
		// contInfo=new String[3];
		//
		// contInfo[0]=tSSRS.GetText(i,1);
		// contInfo[1]="1";
		// contInfo[2]=tSSRS.GetText(i,2);
		// this.mCont.put(contInfo[0], contInfo);
		// }
		// }

		String strSQL = "select a.contno,'0'," // 客户作为投保人的保单
				+ " (select codename from ldcode where codetype='stateflag' and code=b.stateflag),b.stateflag status,b.CValiDate,b.ContType, '' cardpassword"
				+ " from lcappnt a,lccont b "
				+ " where  a.contno=b.contno and b.appflag='1'  "
				+ " and a.appntname='"
				+ this.name
				+ "'  "
				+ " and a.idtype='"
				+ this.idType
				+ "' "
				+ " and a.idno='"
				+ this.idNo
				+ "' "
				+ " union "
				+ " select a.contno,'1', " // 客户作为被保人的保单
				+ " (select codename from ldcode where codetype='stateflag' and code=b.stateflag),b.stateflag status,b.CValiDate,b.ContType, '' cardpassword"
				+ " from lcinsured a,lccont b "
				+ " where a.contno=b.contno and b.appflag='1' "
				+ " and a.name='"
				+ this.name
				+ "' "
				+ " and a.idtype='"
				+ this.idType
				+ "' "
				+ " and a.idno='"
				+ this.idNo
				+ "' "
				+ " and a.insuredno<>b.appntno "
				+ " union "
				+ " select a.cardno,'0', " // 激活卡
				+ " (select codename from ldcode where codetype='cardstatus' and code= a.cardstatus),a.cardstatus status, a.CValiDate, '3', b.cardpassword cardpassword"
				+ " from licardactiveinfolist a ,lzcardnumber b, lmcertifydes c "
				+ " where  a.cardno=b.cardno and b.cardtype=c.subcode and c.operateType ='0' "
				+ " and a.name='"
				+ this.name
				+ "' "
				+ " and a.idtype='"
				+ this.idType
				+ "' "
				+ " and a.idno='"
				+ this.idNo
				+ "' "
				+ " order by status with ur";

		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(strSQL);
		if (null != tSSRS && tSSRS.MaxRow > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				contInfo = new String[6];

				contInfo[0] = tSSRS.GetText(i, 1);
				contInfo[1] = tSSRS.GetText(i, 2);
				contInfo[2] = tSSRS.GetText(i, 3);
				contInfo[3] = tSSRS.GetText(i, 5);
				contInfo[4] = tSSRS.GetText(i, 6);
				contInfo[5] = tSSRS.GetText(i, 7);
				this.mCont.put(contInfo[0], contInfo);
			}
		}

		if (this.mCont.size() == 0) {
			// 保证返回报文的完整,在没有查到客户信息时置为空
			mCont.put("", new String[] { "", "", "", "", "" });
			return false;
		}

		return true;
	}

	/**
	 * 加载报文
	 * 
	 * @return
	 */
	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("LIS002")) {
					Iterator child = Om.getChildren();
					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BatchNo")) {
							this.batchNo = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendDate")) {
							this.sendDate = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendTime")) {
							this.sendTime = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("BranchCode")) {
							this.branchCode = child_element.getText();
							continue;
						}
						if (child_element.getLocalName().equals("SendOperator")) {
							this.sendOperator = child_element.getText();
							continue;
						}

						if (child_element.getLocalName().equals("Bangding")) {
							Iterator childVerify = child_element.getChildren();
							while (childVerify.hasNext()) {
								OMElement child_element_verify = (OMElement) childVerify
										.next();

								if (child_element_verify.getLocalName().equals(
										"ITEM")) {
									Iterator childItem = child_element_verify
											.getChildren();
									while (childItem.hasNext()) {
										OMElement child_item = (OMElement) childItem
												.next();

										if (child_item.getLocalName().equals(
												"Name")) {
											this.name = child_item.getText();
										}
										if (child_item.getLocalName().equals(
												"IDType")) {
											this.idType = child_item.getText();
										}
										if (child_item.getLocalName().equals(
												"IDNo")) {
											this.idNo = child_item.getText();
										}
									}
								}
							}
						}
					}

				} else {
					System.out.println("报文类型不匹配");
					responseOME = buildResponseOME("01", "01", "报文类型不匹配");
					return false;
				}
			}
		} catch (Exception e) {
			System.out.println("加载报文信息异常");
			responseOME = buildResponseOME("01", "01", "加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private OMElement buildResponseOME(String state, String errorCode,
			String errorInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement LIS002 = fac.createOMElement("LIS002", null);
		addOm(LIS002, "BatchNo", this.batchNo);
		addOm(LIS002, "SendDate", PubFun.getCurrentDate());
		addOm(LIS002, "SendTime", PubFun.getCurrentTime());
		addOm(LIS002, "BranchCode", this.branchCode);
		addOm(LIS002, "SendOperator", this.sendOperator);
		addOm(LIS002, "State", state);
		addOm(LIS002, "ErrCode", errorCode);
		addOm(LIS002, "ErrInfo", errorInfo);

		OMElement BANGDING = fac.createOMElement("Bangding", null);
		Set set = mCont.keySet();
		Object key = null;
		for (int j = 0; j < set.size(); j++) {
			OMElement item = fac.createOMElement("ITEM", null);
			key = mCont.getOrder().get(String.valueOf(j + 1));
			String[] temp = (String[]) mCont.get(key);
			addOm(item, "Contno", temp[0]);
			addOm(item, "CustomerType", temp[1]);
			addOm(item, "ContState", temp[2]);
			addOm(item, "CValiDate", temp[3]);
			addOm(item, "ContType", temp[4]);
			addOm(item, "Password", "".equals(temp[5].trim()) ? ""
					: new LisIDEA().decryptString(temp[5].trim()));
			BANGDING.addChild(item);
		}
		LIS002.addChild(BANGDING);
		return LIS002;
	}

	private OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}

}
