/**
 * GrpContDetail.java
 * com.ecwebservice.grpquery
 *
 * Function： TODO 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2010-6-25 		caosg
 *
 * Copyright (c) 2010, TNT All Rights Reserved.
*/

package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

/**
 * ClassName:GrpContDetail
 * Function: TODO ADD FUNCTION
 * Reason:	 TODO ADD REASON
 *
 * @author   caosg
 * @version  
 * @since    Ver 1.1
 * @Date	 2010-6-25		下午03:50:37
 */
public class GrpContDetail extends StandCardHYX{

	private String mGrpContNo = "";

	public GrpContDetail() {
		
		super("GRPCONTDETAIL");
		
	}

	protected void getQueryFactor(Map queryFactor) {
		//获取条件值 
		Iterator factor = queryFactor.keySet().iterator();
		
		while(factor.hasNext())
		{
			String key = factor.next().toString();
			if("GRPCONTNO".equalsIgnoreCase(key))
			{
				mGrpContNo = (String) queryFactor.get(key);
				System.out.println("团体客户--团单详细信息查询,GrpContNo="+mGrpContNo);
			}
		}
		
	}

	public Map prepareData() {
		//查询业务数据并封装
		Map OUTPUTDATA = new HashMap();

//		StringBuffer sql1 = new StringBuffer(" select b.grpcontno ,b.grpname,(select codename from ldcode where codetype = 'businesstype' " +
//				"and code = b.businesstype),");
//		sql1.append(" (select codename from ldcode where codetype = 'grpnature' and code = b.GrpNature),'' 注册资本,'' 资产总额,Fax,");
//		sql1.append(" a.linkman1,a.linkman2,a.mobile1,a.mobile2,a.e_mail1,a.e_mail2,a.grpaddress,a.grpzipcode ,b.cvalidate,b.Peoples2 投保人数," +
//				"b.RelaPeoples,(case b.stateflag when '1' then '有效' else '失效' end),b.CInValiDate," +
//				"c.name salescom," +
//				"c.branchaddress ");
//		sql1.append(" from lcgrpaddress a,lcgrpcont b,labranchgroup c where a.customerno = b.appntno and b.agentgroup = c.agentgroup and b.grpcontno='"+mGrpContNo+"'");
		
		//modify bu fuxin 修改查询SQL ，节点没有调整，具体内容以查询SQL为准。
		String sql1 =" select "
					+" b.grpcontno 保单号码 "
					+" ,b.grpname 投保人单位名称 "
					+" ,(select codename from ldcode where codetype = 'businesstype' and code = b.businesstype) 销售渠道 "
					+" ,(select codename from ldcode where codetype = 'grpnature' and code = b.GrpNature) "
					+" ,RgtMoney 注册资本 "  
					+" ,Asset 资产总额  "
					+" ,b.Fax 修改成单位传真 "
					+" ,a.linkman1 联系人1 "
					+" ,a.linkman2 联系人2 "
					+" ,a.mobile1 联系人1手机号 "
					+" ,a.phone1  联系人固定电话 " 
					+" ,a.e_mail1 联系人1邮箱 "
					+" ,a.Fax1 联系人2邮箱 "
					+" ,a.grpaddress 单位联系地址 "
					+" ,a.grpzipcode 单位邮编 "
					+" ,b.cvalidate 保单生效日期 "
					+" ,(select count(1) from lcinsured where relationtomaininsured ='00' and grpcontno =b.grpcontno) 投保人数  "
					+" ,(select count(1) from lcinsured where relationtomaininsured !='00' and grpcontno =b.grpcontno) 连带投保人数 "
					+" ,codename('stateflag', b.stateflag)  保单状态 "
					+" ,b.CInValiDate 保单终止日期"
					+" ,c.name salescom"
					+" ,c.branchaddress"
					+" ,d.name"   //机构本级名称
					+" ,d.address" //机构本级地址
					+" from lcgrpaddress a,lcgrpcont b,labranchgroup c,ldcom d where a.customerno = b.appntno " 
					+" and b.agentgroup = c.agentgroup"
					+" and a.addressno = b.addressno "
					+" and b.managecom = d.comcode " //by gzh 关联机构表
					+" and b.grpcontno='"+mGrpContNo+"' with ur ";
		
		SSRS contSSRS = mExeSQL.execSQL(sql1);
		
		boolean queryFlag = false;
		if(contSSRS.MaxRow>0)
		{
			queryFlag = true;
			
			OUTPUTDATA.put("GRPCONTNO", contSSRS.GetText(1, 1));
			OUTPUTDATA.put("GRPCUSTNAME", contSSRS.GetText(1, 2));
			OUTPUTDATA.put("INDUSTRYTYPE", contSSRS.GetText(1, 3));
			OUTPUTDATA.put("ENTENPROPERTY", contSSRS.GetText(1, 4));
			OUTPUTDATA.put("RGLCAPITAL", contSSRS.GetText(1, 5));
			OUTPUTDATA.put("ASSET", contSSRS.GetText(1, 6));
			OUTPUTDATA.put("FAX", contSSRS.GetText(1, 7));
			OUTPUTDATA.put("LINKMAN1", contSSRS.GetText(1, 8));
			OUTPUTDATA.put("LINKMAN2", contSSRS.GetText(1, 9));
			OUTPUTDATA.put("PHONE1", contSSRS.GetText(1, 10));
			OUTPUTDATA.put("PHONE2", contSSRS.GetText(1, 11));
			OUTPUTDATA.put("EMAIL1", contSSRS.GetText(1, 12));
			OUTPUTDATA.put("EMAIL2", contSSRS.GetText(1, 13));
			OUTPUTDATA.put("GRPADDRESS", contSSRS.GetText(1, 14));
			OUTPUTDATA.put("GRPZIPCODE", contSSRS.GetText(1, 15));
			
//			OUTPUTDATA.put("SALECOMNAME", contSSRS.GetText(1, 21));
//			OUTPUTDATA.put("SALECOMADDRESS", contSSRS.GetText(1, 22));
			OUTPUTDATA.put("SALECOMNAME", contSSRS.GetText(1, 23));//BY GZH 增加销售机构本级名称
			OUTPUTDATA.put("SALECOMADDRESS", contSSRS.GetText(1, 24));//BY GZH 增加销售机构地址
			
			OUTPUTDATA.put("CVALIDATE", contSSRS.GetText(1, 16));
			
			OUTPUTDATA.put("ENDDATE", contSSRS.GetText(1, 20));
			
			OUTPUTDATA.put("PEOPLES", contSSRS.GetText(1, 17));
			OUTPUTDATA.put("RELAMATEPEOPLES", contSSRS.GetText(1, 18));
			OUTPUTDATA.put("CONTSTATE", contSSRS.GetText(1, 19));	
			
			
		}
		
		String sql2 = " select c.dutycode,(select dutyname from lmduty where dutycode=c.dutycode),'' 代码简称,sum(c.prem),sum(c.amnt) from lcpol b ,lcduty c " 
			+" where  b.polno=c.polno and b.grpcontno = '"+mGrpContNo+"' group by c.dutycode,c.prem,c.amnt" ;
		SSRS riskSSRS = mExeSQL.execSQL(sql2);
		
		//是否查询到险种信息
		
		if(queryFlag&&riskSSRS.MaxRow>0){
			String[] errInfoArr = {"00","0","成功"};
			this.setErrInfoArr(errInfoArr);
		}
		
		Map RISKINFO = new HashMap();
		List item = new ArrayList();
		Map info = null;
		for(int i=1;i<=riskSSRS.MaxRow;i++)
		{
			info = new HashMap();
			
			info.put("RISKCODE", riskSSRS.GetText(i, 1));
			info.put("RISKNAME", riskSSRS.GetText(i, 2));
			info.put("RISKSHORTCODE", riskSSRS.GetText(i, 3));
			info.put("PREM", riskSSRS.GetText(i, 4));
			info.put("AMNT", riskSSRS.GetText(i, 5));
			
			item.add(info);
		}
		RISKINFO.put("ITEM", item);
		OUTPUTDATA.put("RISKINFO", RISKINFO);
		
		return OUTPUTDATA;		
		
	}
	
	/**
	 * main:(这里用一句话描述这个方法的作用)
	 *
	 * @param  @param args    设定文件
	 * @return void    DOM对象
	 * @throws 
	 * @since  CodingExample　Ver 1.1
	 */

	public static void main(String[] args) {

		
		StringBuffer sql1 = new StringBuffer(" select b.grpcontno ,b.grpname,(select codename from ldcode where codetype = 'businesstype' and code = b.businesstype),");
		sql1.append(" (select codename from ldcode where codetype = 'grpnature' and code = b.GrpNature),'' 注册资本,'' 资产总额,Fax,");
		sql1.append(" a.linkman1,a.linkman2,a.mobile1,a.mobile2,a.e_mail1,a.e_mail2,a.grpaddress,a.grpzipcode ,b.cvalidate,'' 投保人数,b.RelaPeoples,codename('stateflag', b.stateflag)");
		sql1.append(" from lcgrpaddress a,lcgrpcont b where a.customerno = b.appntno and b.grpcontno=' 00063316000001 '");
		System.out.println(sql1.toString());
		
		String sql =" select "
			+" b.grpcontno 保单号码 "
			+" ,b.grpname 投保人单位名称 "
			+" ,(select codename from ldcode where codetype = 'businesstype' and code = b.businesstype) 销售渠道 "
			+" ,(select codename from ldcode where codetype = 'grpnature' and code = b.GrpNature) "
			+" ,RgtMoney 注册资本 "  
			+" ,Asset 资产总额  "
			+" ,Fax 修改成单位传真 "
			+" ,a.linkman1 联系人1 "
			+" ,a.linkman2 联系人2 "
			+" ,a.mobile1 联系人1手机号 "
			+" ,a.phone1  联系人固定电话 " 
			+" ,a.e_mail1 联系人1邮箱 "
			+" ,a.Fax1 联系人2邮箱 "
			+" ,a.grpaddress 单位联系地址 "
			+" ,a.grpzipcode 单位邮编 "
			+" ,b.cvalidate 保单生效日期 "
			+" ,Peoples3 投保人数  "
			+" ,b.RelaPeoples 连带投保人数 "
			+" ,codename('stateflag', b.stateflag)  保单状态 "
			+" ,b.CInValiDate 保单终止日期"
			+" ,c.name salescom"
			+" ,c.branchaddress"
			+" from lcgrpaddress a,lcgrpcont b,labranchgroup c where a.customerno = b.appntno " 
			+" and b.agentgroup = c.agentgroup"
			+" and a.addressno = b.addressno "
			+" and b.grpcontno='00063316000001' with ur ";
		System.out.println(sql);
	}
}

