package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import com.ecwebservice.querytools.QueryTool;

import com.ecwebservice.topinterface.ListInterface;
import com.sinosoft.lis.pubfun.PubFun;


public class GrpClaimList extends ListInterface {

	public void prepareCondition() {
		QueryTool qTool = new QueryTool();
		Map queryStringMap = qTool.parseQueryString(this.getOms());
		
		String rgtSql = "select distinct c.caseno,c.customername,c.idno,c.rgtdate,c.rgtstate "
				+ "from llcase c,llclaimdetail d "
				+ "where 1=1   ";
		StringBuffer querySqlBuffer = new StringBuffer();
		querySqlBuffer.append(rgtSql);
		// 查询条件
		String contNo = (String) queryStringMap.get("GRPCONTNO");//团体保单号
		if (contNo != null && !"".equals(contNo)){
			querySqlBuffer.append("and d.grpcontno='"+contNo+"' and c.caseno=d.caseno");
		}
		
		String clmNo = (String) queryStringMap.get("CLMNO");// 赔案号
		if (clmNo != null && !"".equals(clmNo)) {
			querySqlBuffer.append(" and c.caseno='" + clmNo
					+ "'  ");
		}
		
        //被保险人姓名
		String tCustomerName = (String) queryStringMap.get("INSUREDNAME");
		
		if (tCustomerName !=null && !"".equals(tCustomerName)) {
			querySqlBuffer.append(" and c.customername='"+tCustomerName+"' ");
		}
		
		// 立案起始日期
		String rgtStartDate = (String) queryStringMap.get("ACCSTARTDATE");
		// 立案结束日期 

		String rgtEndDate = (String) queryStringMap.get("ACCENDDATE");
		
		if(rgtStartDate == null||"".equals(rgtStartDate)){
//			System.out.println("无起始日期.");
			rgtStartDate = "1900-01-01";
		}
		if(rgtEndDate == null||"".equals(rgtEndDate)){
//			System.out.println("无结束日期.");
			rgtEndDate = PubFun.getCurrentDate();
		}
		
//		if(rgtStartDate != null && !"".equals(rgtStartDate)&&rgtEndDate != null && !"".equals(rgtEndDate)){
//			
//		}
		
		querySqlBuffer.append(" and date(c.rgtdate) between '"+rgtStartDate+"' and '"+rgtEndDate+"'");
		
		String countSql = "select count(distinct c.caseno) "
			+ "from llcase c,llclaimdetail d "
			+ "where 1=1   ";
		StringBuffer countSqlBuffer = new StringBuffer();
		countSqlBuffer.append(countSql);
		// 查询条件
		if (contNo != null && !"".equals(contNo)){
			countSqlBuffer.append("and d.grpcontno='"+contNo+"' and c.caseno=d.caseno");
		}
		
		if (clmNo != null && !"".equals(clmNo)) {
			countSqlBuffer.append(" and c.caseno='" + clmNo
					+ "'  ");
		}
		
		if (tCustomerName !=null && !"".equals(tCustomerName)) {
			countSqlBuffer.append(" and c.customername='"+tCustomerName+"' ");
		}
		if(rgtStartDate == null||"".equals(rgtStartDate)){
			rgtStartDate = "1900-01-01";
		}
		if(rgtEndDate == null||"".equals(rgtEndDate)){
			rgtEndDate = PubFun.getCurrentDate();
		}
		
		countSqlBuffer.append(" and date(c.rgtdate) between '"+rgtStartDate+"' and '"+rgtEndDate+"'");

		//查询该保单下各个险种的赔付案件数及赔付总额
		StringBuffer riskpayinfo = new StringBuffer();
		riskpayinfo.append("select a.riskcode," +
				"(select riskName from lmrisk where riskcode = a.riskcode) as riskname," +
				"sum(a.realpay) as realpay," +
				"count(distinct(caseno)) as casenum  " +
				"from llclaimdetail a  " +
				"where a.grpcontno = '"+contNo+"' " +
				"group by a.riskcode");
		
		this.setRootTag("GRPCLAIMLIST");
		List returnChildTag = new ArrayList();
		returnChildTag.add("CLMNO");
		returnChildTag.add("INSUREDNAME");
		returnChildTag.add("INSUREDIDNO");//证件号码
		returnChildTag.add("CLMDATE");
		returnChildTag.add("CLMSTATE");
		this.setChildTagName(returnChildTag);
	    
		List riskTag = new ArrayList();
		riskTag.add("RISKCODE");
		riskTag.add("RISKNAME");
		riskTag.add("RISKTOTALPAY");
		riskTag.add("RISKCASENUM");
		setRiskList(riskTag);
        
		setQuerySql(querySqlBuffer.toString());
		setCountSql(countSqlBuffer.toString());
		setRiskSql(riskpayinfo.toString());
		
		this.setOrderCol("c.caseno");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
