/**
 * GrpAccountHistory.java
 * com.ecwebservice.grpquery
 *
 * Function： TODO 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2010-6-25 		caosg
 *
 * Copyright (c) 2010, TNT All Rights Reserved.
*/

package com.ecwebservice.grpquery;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

/**
 * ClassName:GrpAccountHistory
 * Function: TODO ADD FUNCTION
 * Reason:	 TODO ADD REASON
 *
 * @author   caosg
 * @version  
 * @since    Ver 1.1
 * @Date	 2010-6-25		下午04:45:02
 */
public class GrpAccountHistory extends StandCardHYX{
	
	private String mGrpContNo = "";
	
	public GrpAccountHistory() {
		
		super("GRPACCOUNTHISTORY");
		// TODO Auto-generated constructor stub
	}

	protected void getQueryFactor(Map queryFactor) {
		
		//获取条件值 
		Iterator factor = queryFactor.keySet().iterator();
		
		while(factor.hasNext())
		{
			String key = factor.next().toString();
			if("GRPCONTNO".equalsIgnoreCase(key))
			{
				mGrpContNo = (String) queryFactor.get(key);
				System.out.println("团体客户--团体客户--账户历史查询,GrpContNO="+mGrpContNo);
			}
		}
		
	}

	public Map prepareData() {
		
		//查询业务数据并封装
		Map OUTPUTDATA = new HashMap();
		StringBuffer sql1 = new StringBuffer("select (select sum(getmoney) from lpdiskimport where edorno = a.otherno and insuredno = (select insuredno from lccont where contno = a.contno)),");
		sql1.append(" makedate, (select defaultrate from lcgrpinterest where grpcontno = '"+mGrpContNo+"' fetch first 1 row only ),");
		sql1.append(" (select sum(money) from lcinsureacctrace where contno = a.contno and moneytype = 'BF' and othertype = '1' and insuaccno = a.insuaccno),");
		sql1.append(" (select sum(money) from lcinsureacctrace where contno = a.contno and insuaccno = a.insuaccno and otherno <>a.otherno) from lcinsureacctrace a ");
		sql1.append(" where contno in (select contno from lccont where grpcontno = '"+mGrpContNo+"' and poltype ='2')");
		sql1.append(" and exists (select 1 from lgwork where statusno = '5' and workno = a.otherno and typeno = '070015' ) with ur");
		
		SSRS contSSRS = mExeSQL.execSQL(sql1.toString());
		
		if(contSSRS.MaxRow>0)
		{
			String[] errInfoArr = {"0","00","成功"};
			this.setErrInfoArr(errInfoArr);
			OUTPUTDATA.put("CURRENTAMOUNT", contSSRS.GetText(1, 1));
			OUTPUTDATA.put("BALANCEDATE", contSSRS.GetText(1, 2));
			OUTPUTDATA.put("BALANCERATE", contSSRS.GetText(1, 3));
			OUTPUTDATA.put("INCEPTCONTVALUE", contSSRS.GetText(1, 4));
			OUTPUTDATA.put("CURRENTCONTVALUE", contSSRS.GetText(1, 5));		
		}
		
		return OUTPUTDATA;
		
	}
	
	/**
	 * main:(这里用一句话描述这个方法的作用)
	 * @param  @param args    设定文件
	 * @return void    DOM对象
	 * @throws 
	 * @since  CodingExample　Ver 1.1
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		StringBuffer sql1 = new StringBuffer("select (select sum(getmoney) from lpdiskimport where edorno = a.otherno and insuredno = (select insuredno from lccont where contno = a.contno)),");
		sql1.append(" makedate, (select defaultrate from lcgrpinterest where grpcontno = '00026161000001' fetch first 1 row only ),");
		sql1.append(" (select sum(money) from lcinsureacctrace where contno = a.contno and moneytype = 'BF' and othertype = '1' and insuaccno = a.insuaccno),");
		sql1.append(" (select sum(money) from lcinsureacctrace where contno = a.contno and insuaccno = a.insuaccno and otherno <>a.otherno) from lcinsureacctrace a ");
		sql1.append(" where contno in (select contno from lccont where grpcontno = '  ' and poltype ='2')");
		sql1.append(" and exists (select 1 from lgwork where statusno = '5' and workno = a.otherno and typeno = '070015' ) with ur");
		
		System.out.println(sql1.toString());

	}


}

