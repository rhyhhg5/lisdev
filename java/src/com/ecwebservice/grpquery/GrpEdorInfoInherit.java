package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMElement;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.topinterface.ListInterface;

public class GrpEdorInfoInherit extends ListInterface {

	public void prepareCondition() {
		QueryTool qTool = new QueryTool();
		Map queryStringMap = qTool.parseQueryString(this.getOms());
		
		String rgtSql = "select a.workno 保全批单号,a.applyname 申请人姓名,a.acceptdate 申请时间, b.edorvalidate 生效时间," +
				"(select edorname from lmedoritem where edorcode=b.EdorType and appobj='G') 批改类型," +
				"(select codename from ldcode where codetype = 'applytypeno' and code = a.ApplyTypeNo) 申请人类型," +
				"(select codename from ldcode where codetype = 'appedorstate' and code = b.EdorState)  批改状态 " +
				" from lgwork a ,lpgrpedoritem  b where a.workno = b.edoracceptno ";
		StringBuffer querySqlBuffer = new StringBuffer();
		querySqlBuffer.append(rgtSql);
		// 查询条件
		String grpContNo = (String) queryStringMap.get("GRPCONTNO");// 赔案号
		if (grpContNo != null && !"".equals(grpContNo)) {
			querySqlBuffer.append("and b.grpcontno = '"+grpContNo+"'");
		}
	

		

		this.setRootTag("GRPEDORINFO");
		List returnChildTag = new ArrayList();
		returnChildTag.add("PRESERVENO");
		returnChildTag.add("APPLYNAME");
		returnChildTag.add("APPLYDATE");
		returnChildTag.add("CVALIDATE");
		returnChildTag.add("CAUSE");
		returnChildTag.add("APPLYTYPE");
		returnChildTag.add("EDORSTATE");
		
		this.setChildTagName(returnChildTag);
		// sql
		// String totalRecordsSql =
		// String rgtSql = "select
		// accdate,codename('llrgtstate',c.rgtstate),c.caseno "
		// + "from llsubreport a,llcaserela b,llcase c " + "where 1=1 ";

		setQuerySql(querySqlBuffer.toString());
		this.setOrderCol("a.workno");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
