package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.ListInterface;

public class GrpRecordInherit extends ListInterface{
	public void prepareCondition() {
		QueryTool qTool = new QueryTool();
		Map queryStringMap = qTool.parseQueryString(this.getOms());
		
		String rgtSql = "select (select riskname from lmrisk where riskcode = a.riskcode),"
					       +"sumduepaymoney,"
					       +"paycount,"
					       +"(select codename from ldcode where codetype = 'payintv' and code = char(a.PayIntv)),"
					       +"confdate,"
					       +"(select codename from ldcode where codetype = 'paymode' and code = " +
					       		"(select paymode from ljtempfeeclass where tempfeeno = a.getnoticeno fetch first 1 row only))"
					       +" from ljapaygrp a where 1=1 ";
		StringBuffer querySqlBuffer = new StringBuffer();
		querySqlBuffer.append(rgtSql);
		// 查询条件
		String grpContNo = (String) queryStringMap.get("GRPCONTNO");//保单号
		if (grpContNo != null && !"".equals(grpContNo)){
			querySqlBuffer.append("and  a.grpcontno = '"+grpContNo+"'");
		}
//		date 20101126 by gzh 现根据时间排序，再根据险种名称排序。
		querySqlBuffer.append(" order by confdate desc,riskname");
		

		this.setRootTag("GRPRENEWRECORD");
		List returnChildTag = new ArrayList();
		returnChildTag.add("RISKNAME");
		returnChildTag.add("AMOUNT");
		returnChildTag.add("INDEX");
		returnChildTag.add("PAYTYPE");
		returnChildTag.add("PAYDATE");
		returnChildTag.add("PAYWAY");
		
		this.setChildTagName(returnChildTag);
		
		System.out.println("团单续期记录查询SQL："+querySqlBuffer.toString());
		setQuerySql(querySqlBuffer.toString());
		this.setOrderCol("paycount");
	}

}
