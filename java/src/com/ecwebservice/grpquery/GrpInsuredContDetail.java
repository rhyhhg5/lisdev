package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

public class GrpInsuredContDetail extends StandCardHYX{
	

	private String mContNo = "";

	public GrpInsuredContDetail() {
		
		super("GRPINSUREDCONTDETAIL");
		
	}

	protected void getQueryFactor(Map queryFactor) {
		//获取条件值 
		Iterator factor = queryFactor.keySet().iterator();
		
		while(factor.hasNext())
		{
			String key = factor.next().toString();
			if("CONTNO".equalsIgnoreCase(key))
			{
				mContNo = (String) queryFactor.get(key);
				System.out.println("团体客户--被保人详细信息查询,ContNo="+mContNo);
			}
		}
		
	}

public Map prepareData() {
		
		Map OUTPUTDATA = new HashMap();
		String sql2 = "select c.contno,b.grpname,a.linkman1,a.linkman2,a.mobile1,a.mobile2,a.e_mail1,a.e_mail2,a.grpaddress,a.grpzipcode ,b.cvalidate,b.signdate,b.RelaPeoples,codename('stateflag', c.stateflag),c.CInValiDate";
		sql2 = sql2+" from lcgrpaddress a,lcgrpcont b,lccont c where a.customerno = b.appntno and b.signdate = c.signdate and b.grpcontno = c.grpcontno and c.contno = '"+mContNo+"' " ;
		
		SSRS contSSRS = mExeSQL.execSQL(sql2);
		if(contSSRS.MaxRow>0){
			String[] errInfoArr = {"00","0","成功"};
			this.setErrInfoArr(errInfoArr);
			
			OUTPUTDATA.put("CONTNO", contSSRS.GetText(1, 1));		
			OUTPUTDATA.put("CVALIDATE", contSSRS.GetText(1, 11));
			OUTPUTDATA.put("ENDDATE", contSSRS.GetText(1, 14));
			OUTPUTDATA.put("CONTSTATE", contSSRS.GetText(1, 13));
			OUTPUTDATA.put("SIGNDATE", contSSRS.GetText(1, 12));
		}
		//查询业务数据并封装
		String sql1 = " select b.riskcode,a.riskname,'' 代码简称,b.amnt from lmriskapp a ,lcpol b where a.riskcode = b.riskcode and b.contno = '"+mContNo+"'";
		SSRS riskSSRS = mExeSQL.execSQL(sql1);
		
		Map riskInfos = new HashMap();
		Map info = null;
		List item = new ArrayList();
		for (int i=1;i<=riskSSRS.MaxRow;i++)
		{
			info = new HashMap();
			info.put("RISKCODE", riskSSRS.GetText(i, 1));
			info.put("RISKNAME", riskSSRS.GetText(i, 2));
//			info.put("RISKSHORTCODE", riskSSRS.GetText(i, 3));
			info.put("AMNT", riskSSRS.GetText(i, 4));
			item.add(info);
		}
		riskInfos.put("ITEM", item);
		OUTPUTDATA.put("RISKINFO", riskInfos);
		
		//新加被保人信息
		String insurSQL="select lci.insuredno,"
					       +"lci.relationtoappnt,"
					       +"case lci.relationtomaininsured when '00' then '00' else '01' end,"
					       +"lci.relationtomaininsured,"
					       +"'',"
					       +"'',"
					       +"lci.name,"
					       +"(select codename from ldcode where codetype = 'sex' and code =lci.sex ),"
					       +"(select codename from ldcode where codetype = 'idtype' and code =lci.idtype ),"
					       +"lci.idno,"
					       +"lci.birthday,"
					       +"(select codename from ldcode where codetype = 'marriage' and code = char(lci.Marriage) ),"
					       +"(select codename from ldcode where codetype = 'nativeplace' and code =lci.NativePlace ),"
					       +"lci.WorkType,"
					       +"lca.Mobile,"
					       +"lca.CompanyPhone,"
					       +"lca.HomePhone,"
					       +"lca.PostalAddress,"
					       +"lca.ZipCode,"
					       +"lca.EMail "
						  +"from lcinsured lci "
						 +"left join lcaddress lca on lci.insuredno = lca.customerno "
                         +"and lci.addressno = lca.addressno where 1=1 and lci.contno='"+mContNo+"'";
		SSRS insurSSRS = mExeSQL.execSQL(insurSQL);//??
		Map insuredInfos = new HashMap();
		Map item_info = null;
		List item2 = new ArrayList();
		for (int i=1;i<=insurSSRS.MaxRow;i++)
		{
			item_info = new HashMap();
			item_info.put("INSUNO", insurSSRS.GetText(i, 1));
			item_info.put("TOAPPNTRELA", insurSSRS.GetText(i, 2));
			item_info.put("RELATIONCODE", insurSSRS.GetText(i, 3));
			item_info.put("RELATIONTOINSURED", insurSSRS.GetText(i, 4));
			item_info.put("REINSUNO", insurSSRS.GetText(i, 5));
			item_info.put("INSUREDORDER", insurSSRS.GetText(i, 6));
			item_info.put("INSUREDNAME", insurSSRS.GetText(i, 7));
			item_info.put("INSUREDSEX", insurSSRS.GetText(i, 8));
			item_info.put("INSUREDIDTYPE", insurSSRS.GetText(i, 9));
			item_info.put("INSUREDIDNO", insurSSRS.GetText(i, 10));
			item_info.put("INSUREDBIRTHDAY", insurSSRS.GetText(i, 11));
			
			item_info.put("INSUREDMARRYSTATE", insurSSRS.GetText(i, 12));
			item_info.put("INSUREDNATIVEPLACE", insurSSRS.GetText(i, 13));
			item_info.put("INSUREDOCCUPATION", insurSSRS.GetText(i, 14));
			item_info.put("INSUREDMOBILE", insurSSRS.GetText(i, 15));
			item_info.put("INSUREDCOMPANYPHONE", insurSSRS.GetText(i, 16));
			item_info.put("INSUREDHOMEPHONE", insurSSRS.GetText(i, 17));
			item_info.put("INSUREDADDRESS", insurSSRS.GetText(i, 18));
			item_info.put("INSUREDZIPCODE", insurSSRS.GetText(i, 19));
			item_info.put("INSUREDEMAIL", insurSSRS.GetText(i, 20));

			item2.add(item_info);
		}
		insuredInfos.put("ITEM", item2);
		OUTPUTDATA.put("INSUREDINFO", insuredInfos);

		// TODO Auto-generated method stub
		
		return OUTPUTDATA;		
	}
}
