package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.ListInterface;

public class GrpAccountRate extends ListInterface{
	public void prepareCondition() {
		QueryTool qTool = new QueryTool();
		Map queryStringMap = qTool.parseQueryString(this.getOms());
		
		String rgtSql = "select grpcontno,(select riskname from lmrisk where riskcode = a.riskcode), "
					  + "trim(char(year(startdate)))||'年'||trim(char(month(startdate)))||'月', "
					  + "(select trim(varchar(double(rate*100)))||'%' from ldpromiserate where year(startdate) = year(a.startdate)), "
					  + "trim(varchar(double(rate*100)))||'%' "
					  + "from interest000001 a where 1=1 and startdate <= current date " ;
		StringBuffer querySqlBuffer = new StringBuffer();
		querySqlBuffer.append(rgtSql);
		// 查询条件
		String grpContNo = (String) queryStringMap.get("GRPCONTNO");//保单号
		if (grpContNo != null && !"".equals(grpContNo))
		{
			querySqlBuffer.append("and  a.grpcontno = '"+grpContNo+"'");
		}
		querySqlBuffer.append(" order by startdate desc");
		

		this.setRootTag("GRPACCOUNTRATE");
		List returnChildTag = new ArrayList();
		returnChildTag.add("GRPCONTNO");
		returnChildTag.add("RISKNAME");
		returnChildTag.add("MONTH");
		returnChildTag.add("GURATRATE");
		returnChildTag.add("BALARATE");
		
		this.setChildTagName(returnChildTag);
		
		System.out.println("团体万能结算利率查询SQL："+querySqlBuffer.toString());
		setQuerySql(querySqlBuffer.toString());
		this.setOrderCol("startdate");
	}

}
