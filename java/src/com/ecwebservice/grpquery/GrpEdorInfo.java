package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

public class GrpEdorInfo extends StandCardHYX {
	private String mGrpContNo = "";

	public GrpEdorInfo() {
		// 返回报文根节点
		super("GRPEDORINFO");
	}

	protected void getQueryFactor(Map queryFactor) {

		// 获取条件值 CONTNO
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();
			if ("GrpContNo".equalsIgnoreCase(key)) {
				mGrpContNo = (String) queryFactor.get(key);
				System.out.println("团险保全信息查询,contno=" + mGrpContNo);
			}
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Map prepareData() {

		Map OUTPUTDATA = new HashMap();
		Map EDORLIST = new HashMap();
		List item = new ArrayList();
		Map info = null;
		String sql = "select a.workno 保全批单号,a.applyname 申请人姓名,a.acceptdate 申请时间, b.edorvalidate 生效时间,b.contno 保单代码," +
				"'没有' 申请人证件号码,'没有' 申请人证件类型,'没有' 保全变更原因,codename('applytypeno',a.ApplyTypeNo) 申请人类型,'什么状态?' 保单操作状态 " +
				"from lgwork a ,lpgrpedoritem b where a.workno = b.edoracceptno and b.grpcontno = '"
				+ mGrpContNo + "'";

		SSRS contSSRS = mExeSQL.execSQL(sql);

		boolean queryFlag = false;
		if (contSSRS.MaxRow > 0) {
			queryFlag = true;
		}
		if (queryFlag) {
			String[] errInfoArr = { "00", "0", "成功" };
			this.setErrInfoArr(errInfoArr);
		}
		for (int i = 1; i <= contSSRS.MaxRow; i++) {
			info = new HashMap();

			info.put("PRESERVENO", contSSRS.GetText(i, 1));
			info.put("APPLYNAME", contSSRS.GetText(i, 2));
			info.put("APPLYDATE", contSSRS.GetText(i, 3));
			info.put("CVALIDATE", contSSRS.GetText(i, 4));
			info.put("CONTNO", contSSRS.GetText(i, 5));
//			info.put("APPLYIDTYPE", contSSRS.GetText(i, 6));
//			info.put("APPLYIDNO", contSSRS.GetText(i, 7));
			info.put("CAUSE", contSSRS.GetText(i, 8));
			info.put("APPLYTYPE", contSSRS.GetText(i, 9));
			info.put("CONTSTATE", contSSRS.GetText(i, 10));

			item.add(info);
		}
		EDORLIST.put("ITEM", item);
		OUTPUTDATA.put("EDORLIST", EDORLIST);
		// 查询业务数据并封装
		return OUTPUTDATA;
	}
}
