package com.ecwebservice.grpquery;

import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;

public class GrpContToUserName implements CardHYX {
	private String[] baseInfoString;

	// baseInfo[0] = batchno;
	// baseInfo[1] = sendDate;
	// baseInfo[2] = sendTime;
	// baseInfo[3] = messageType;
	private String rootTag;

	public OMElement queryData(OMElement Oms) {
		OMElement responseOME;
		QueryTool qTool = new QueryTool();
		ExeSQL tExeSQL = new ExeSQL();

		// 获取baseInfo
		baseInfoString = qTool.parseBaseInfo(Oms);
		rootTag = qTool.getRootTag(Oms);
		
		Map queryMap = qTool.parseQueryString(Oms);
		String contNo = (String) queryMap.get("CONTNO");
		String grpCode = (String) queryMap.get("GRPCODE");
		String verifySql = "select count(*) from lcgrpappnt where grpcontno='"
				+ contNo + "' and organcomcode='" + grpCode + "' with ur";
		String count = tExeSQL.getOneValue(verifySql);
		System.out.println("校验结果：" + count);
		
		if(Integer.parseInt(count)>0){
			responseOME = buildResponseOME("0", "00", "成功");
		}else{
			responseOME = buildResponseOME("1", "*", " 绑定失败");
		}

		
		return responseOME;
	}

	// 构建返回报文
	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement(rootTag, null);

		LoginVerifyTool tool = new LoginVerifyTool();
		QueryTool qTool = new QueryTool();
		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString, state,
				errCode, errInfo);

		DATASET.addChild(BASEINFO);// 构建基础信息

		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);

		DATASET.addChild(OUTPUTDATA);

		return DATASET;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
