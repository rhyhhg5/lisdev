package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.ListInterface;

public class GrpInsuredList extends ListInterface{
	
	public void prepareCondition() {
		QueryTool qTool = new QueryTool();
		Map queryStringMap = qTool.parseQueryString(this.getOms());
		
		String rgtSql = "select a.contno,a.name,a.idno,b.mobile from lcinsured a left join lcaddress b on a.insuredno = b.customerno and a.addressno = b.addressno where 1=1 ";
		StringBuffer querySqlBuffer = new StringBuffer();
		querySqlBuffer.append(rgtSql);
		// 查询条件
		String grpContNo = (String) queryStringMap.get("GRPCONTNO");// 团单号
		String contNo = (String) queryStringMap.get("CONTNO");//分单号
		String name = (String) queryStringMap.get("NAME");//姓名
		String idNo = (String) queryStringMap.get("IDNO");//
		String insuredmobile = (String) queryStringMap.get("INSUREDMOBILE");//
		if (grpContNo != null && !"".equals(grpContNo)) {
			querySqlBuffer.append("and a.grpcontno = '"+grpContNo+"' ");
		}
		if(contNo !=null && !"".equals(contNo)){
			querySqlBuffer.append("and a.contNo = '"+contNo+"' ");
		}
		if(name !=null && !"".equals(name)){
			querySqlBuffer.append("and a.name = '"+name+"' ");
		}
		if(idNo !=null && !"".equals(idNo)){
			querySqlBuffer.append("and a.idNo = '"+idNo+"' ");
		}
		if(insuredmobile !=null && !"".equals(insuredmobile)){
			querySqlBuffer.append("and b.Mobile = '"+insuredmobile+"' ");
		}

		this.setRootTag("GRPINSUREDLIST");
		List returnChildTag = new ArrayList();
		returnChildTag.add("CONTNO");
		returnChildTag.add("NAME");
		returnChildTag.add("IDNO");
		returnChildTag.add("INSUREDMOBILE");
		
		this.setChildTagName(returnChildTag);
		// sql
		// String totalRecordsSql =
		// String rgtSql = "select
		// accdate,codename('llrgtstate',c.rgtstate),c.caseno "
		// + "from llsubreport a,llcaserela b,llcase c " + "where 1=1 ";

		setQuerySql(querySqlBuffer.toString());
		this.setOrderCol("grpcontno");
	}


}
