/**
 * GrpContList.java
 * com.ecwebservice.grpquery
 *
 * Function： TODO 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2010-6-25 		caosg
 *
 * Copyright (c) 2010, TNT All Rights Reserved.
 */

package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

/**
 * @author caosg
 * @version
 * @since Ver 1.1
 * @Date 2010-6-25 下午03:06:12
 */
public class GrpContList extends StandCardHYX {

	// 增加对多GRPcontNo 的支持 GRPcontNo放入Set中
	Set grpContNoSet = new HashSet();

	/** 团体保单号 */
	private String mGrpContNo = "";

	/** 团体客户号 */
	// private String mGrpCustomerNo = "";
	public GrpContList() {

		super("GRPCONTLIST");
	
	}

	protected void getQueryFactor(Map queryFactor) {

		// 获取条件值
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();

			mGrpContNo = (String) queryFactor.get(key);
			grpContNoSet.add(mGrpContNo);
			System.out.println("团体客户--团单列表信息查询,GrpContNO=" + mGrpContNo);

			// if("GRPCUSTOMERNO".equalsIgnoreCase(key))
			// {
			// mGrpCustomerNo = (String) queryFactor.get(key);
			// System.out.println("团体客户--团单列表信息查询,GrpCustomerNo="+mGrpCustomerNo);
			// }
		}
	}

	public Map prepareData() {
		String[] errInfoArr  = new String[3];
		errInfoArr[0] = "01";
		errInfoArr[1] = "*";
		errInfoArr[2] = "没有符合查询条件的数据";
		
		// 准备返回报文标签
		List returnTagList = new ArrayList();

		if (this.getRoot().equalsIgnoreCase("GRPCONTLIST")) {
			// 团险 保单列表
			System.out.println("构建返回报文标签");

			returnTagList.add("GRPCONTNO");
			returnTagList.add("GRPCUSTOMERNO");
			returnTagList.add("CVALIDATE");
			returnTagList.add("PEOPLES");
			returnTagList.add("CONTSTATE");
			returnTagList.add("RISKNAME");
			returnTagList.add("PREM");
			returnTagList.add("AMNT");
			returnTagList.add("RISKTYPE4");
			returnTagList.add("MANAGECOM");
		}

		if (this.getRoot().equalsIgnoreCase("CONTLIST")) {
			// 个险保单列表
			System.out.println("构建个险保单列表返回报文标签");

			returnTagList.add("CONTNO");
			returnTagList.add("CONTTYPE");
			returnTagList.add("CVALIDATE");
			returnTagList.add("CONTSTATE");
			returnTagList.add("RISKNAME");
			returnTagList.add("PREM");
			returnTagList.add("AMNT");
		}

		if (this.getRoot().equalsIgnoreCase("INSUREDCONTLIST")) {
			// 个险保单列表
			System.out.println("构建个险 团单分单保单列表返回报文标签");

			returnTagList.add("CONTNO");
			returnTagList.add("CVALIDATE");
			returnTagList.add("CONTSTATE");
			returnTagList.add("RISKNAME");

			returnTagList.add("AMNT");
			returnTagList.add("RISKCODE");
			returnTagList.add("MANAGECOM");
			
			//start -- created in 2014-7-17 by dhc ; add 账号资金分配总金额 及账户余额
			returnTagList.add("TOTALMONEY");
			returnTagList.add("INSUACCBALA");//insuaccbala
			//end
		}

		// 查询业务数据并封装
		Map OUTPUTDATA = new HashMap();
		Map CLIST = new HashMap();
		List item = new ArrayList();

		Iterator grpContNoIterator = grpContNoSet.iterator();
		while (grpContNoIterator.hasNext()) {
			Map info = null;
			String sql = null;

			mGrpContNo = (String) grpContNoIterator.next();

			if (this.getRoot().equalsIgnoreCase("GRPCONTLIST")) {
				//modify by fuxin 2010-10-15 修改投保总人数的方式。
				sql = "select a.grpcontno,b.appntno,a.cvalidate,(select count(1) From lcinsured where grpcontno = b.grpcontno) 投保总人数,codename('stateflag', b.stateflag) 保单状态,"
						+ "(select dutyname from lmduty where dutycode=c.dutycode),sum(c.prem),sum(c.amnt),(select RiskType4 from lmriskapp where riskcode = a.riskcode),a.managecom ";
				sql = sql
						+ " from lcgrppol a,lcgrpcont b,lcduty c,lcpol d where a.grpcontno = b.grpcontno and a.grpcontno=d.grpcontno and d.polno = c.polno  and b.grpcontno = '"
						+ mGrpContNo + "'" 
						+ " group by c.dutycode,a.grpcontno,b.appntno,a.cvalidate,c.prem,c.amnt,a.riskcode,b.grpcontno,b.stateflag,a.managecom";

			}
			if (this.getRoot().equalsIgnoreCase("CONTLIST")) {
				sql = "select a.contno,a.conttype 保单类型,a.Cvalidate 生效日期 ,  codename('stateflag', a.stateflag) 保单状态 "
						+ ",(select dutyname from lmduty where dutycode=b.dutycode) 责任名称,b.prem,b.amnt from Lccont  a,lcduty b ,lcpol c  where a.contno='"
						+ mGrpContNo
						+ "' a"
						+ "nd c.contno=a.contno and c.polno = b.polno";
			}
			// 团单分单
			if (this.getRoot().equalsIgnoreCase("INSUREDCONTLIST")) {
				sql = "select c.contno 团单分单号,c.cvalidate 团单分单生效日期,codename('stateflag', c.stateflag) 分单状态, "
						+ "(select dutyname from lmduty where dutycode=e.dutycode)责任名称 ,e.amnt 保额,e.dutycode,c.managecom 管理机构, "
//						start Added by dhc
						+ "(select nvl(sum(money),0) from lcinsureacctrace where moneytype='GA' and contno = '"+mGrpContNo+"') 账户资金分配总金额,"
						+ "(select nvl(sum(insuaccbala),0) from lcinsureacc where contno= '"+mGrpContNo+"') 账户余额 "
						//end
						+ "from  lcgrpcont a,lccont c,lcpol d,lcduty e  where  a.grpcontno = c.grpcontno and d.polno=e.polno and  c.contno='"
						+ mGrpContNo + "' and c.contno=d.contno "
						+"union all "
						+"select c.contno 团单分单号,c.cvalidate 团单分单生效日期,codename('stateflag', c.stateflag) 分单状态, "
						+ "(select dutyname from lmduty where dutycode=e.dutycode)责任名称 ,e.amnt 保额,e.dutycode,c.managecom 管理机构, "
//						start Added by dhc
						+ "(select nvl(sum(money),0) from lcinsureacctrace where moneytype='GA' and contno = '"+mGrpContNo+"') 账户资金分配总金额,"
						+ "(select nvl(sum(insuaccbala),0) from lcinsureacc where contno= '"+mGrpContNo+"') 账户余额 "
						//end
						+ "from  lcgrpcont a,lbcont c,lbpol d,lbduty e  where  a.grpcontno = c.grpcontno and d.polno=e.polno and  c.contno='"
						+ mGrpContNo + "' and c.contno=d.contno ";
			}
			SSRS contSSRS = mExeSQL.execSQL(sql);

			// 设置查询状态

			if (contSSRS.MaxRow > 0) {
				errInfoArr[0] = "00";
				errInfoArr[1] = "0";
				errInfoArr[2] = "成功";
//				String[] errInfoArr = { "00", "0", "成功" };
				this.setErrInfoArr(errInfoArr);
			}
			// else {
			// String[] errInfoArr = { "01", "*", "没有符合查询条件的数据" };
			// this.setErrInfoArr(errInfoArr);
			// }

			for (int i = 1; i <= contSSRS.MaxRow; i++) {
				info = new HashMap();

				for (int tagPointer = 0; tagPointer < returnTagList.size(); tagPointer++) {
					info.put((String) returnTagList.get(tagPointer), contSSRS
							.GetText(i, tagPointer + 1));
				}

				

				item.add(info);
			}

		}

		CLIST.put("ITEM", item);
		OUTPUTDATA.put("CLIST", CLIST);

		return OUTPUTDATA;
	}

	/**
	 * main:(这里用一句话描述这个方法的作用)
	 * 
	 * @param
	 * @param args
	 *            设定文件
	 * @return void DOM对象
	 */

	public static void main(String[] args) {

		// TODO Auto-generated method stub

	}
}
