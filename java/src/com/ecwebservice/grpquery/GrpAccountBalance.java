/**
 * GrpAccountBalance.java
 * com.ecwebservice.grpquery
 *
 * Function： TODO 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2010-6-25 		caosg
 *
 * Copyright (c) 2010, TNT All Rights Reserved.
*/

package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

/**
 * ClassName:GrpAccountBalance
 * Function: TODO ADD FUNCTION
 * Reason:	 TODO ADD REASON
 *
 * @author   caosg
 * @version  
 * @since    Ver 1.1
 * @Date	 2010-6-25		下午04:44:05
 */
public class GrpAccountBalance extends StandCardHYX{
	
	private String mGrpContNo = "";
	
	public GrpAccountBalance() {
		
		super("GRPACCOUNTBALANCE");
		// TODO Auto-generated constructor stub
		
	}

	protected void getQueryFactor(Map queryFactor) {
		
		//获取条件值 
		Iterator factor = queryFactor.keySet().iterator();
		
		while(factor.hasNext())
		{
			String key = factor.next().toString();
			if("GRPCONTNO".equalsIgnoreCase(key))
			{
				mGrpContNo = (String) queryFactor.get(key);
				System.out.println("团体客户--账户价值查询,GrpContNO="+mGrpContNo);
			}
		}		
		
		
	}

	public Map prepareData() {
		
		//查询业务数据并封装
		Map OUTPUTDATA = new HashMap();
		
		StringBuffer sql1 = new StringBuffer(" select (select max(insuaccname) from lmrisktoacc where insuaccno=a.insuaccno and riskcode = a.riskcode) 账户名称 ,");
		sql1.append(" appntno 团体客户号,sumpay 累计交费,sumpay 累计金额,insuaccbala 最新保单价值," +
				"(select sum(money) from lcinsureacctrace where contno = a.contno and moneytype = 'BF' and othertype = '1' and insuaccno = a.insuaccno) 起初保单价值");
		sql1.append("  from lcinsureacc a where contno in (select contno from lccont where grpcontno = '"+mGrpContNo+"' and poltype ='2')");
		
		SSRS contSSRS = mExeSQL.execSQL(sql1.toString());
		
		List item = new ArrayList();
		Map info = null;
		if(contSSRS.MaxRow>0)
		{
			String[] errInfoArr = {"0","00","成功"}; 
			this.setErrInfoArr(errInfoArr);
			for (int i=1;i<=contSSRS.MaxRow;i++){
				info = new HashMap();
				info.put("ACCOUNTNAME", contSSRS.GetText(i, 1));
				info.put("GRPCUSTOMERNO", contSSRS.GetText(i, 2));
				info.put("PAYMENTSUM", contSSRS.GetText(i, 3));
				info.put("AMOUNTSUM", contSSRS.GetText(i, 4));
				info.put("CONTNEWVALUE", contSSRS.GetText(i, 5));		
				info.put("CONTSTARTVALUE", contSSRS.GetText(i, 6));	
				item.add(info);
			}
			
		}
		OUTPUTDATA.put("ITEM", item);
		
		return OUTPUTDATA;
		
	}
	
	/**
	 * main:(这里用一句话描述这个方法的作用)
	 * @param  @param args    设定文件
	 * @return void    DOM对象
	 * @throws 
	 * @since  CodingExample　Ver 1.1
	 */

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		StringBuffer sql1 = new StringBuffer(" select (select max(insuaccname) from lmrisktoacc where insuaccno=a.insuaccno and riskcode = a.riskcode) ,");
		sql1.append(" appntno,sumpay,sumpay,insuaccbala,(select sum(money) from lcinsureacctrace where contno = a.contno and moneytype = 'BF' and othertype = '1' and insuaccno = a.insuaccno) ");
		sql1.append("  from lcinsureacc a where contno in (select contno from lccont where grpcontno = '  ' and poltype ='2')");
		
		System.out.println(sql1.toString());
	}


}

