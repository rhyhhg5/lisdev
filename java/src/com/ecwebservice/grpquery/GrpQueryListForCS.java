package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class GrpQueryListForCS extends StandCardHYX{

	private String mContNo = "";
	private String name="";
	private String grpno = "";
	private String customNo = "";
	private String pageIndex =null;
	private String pageSize=null;

	public GrpQueryListForCS() {
		// 返回报文根节点
		super("GRPQUERYLISTFORCS");
	}

	protected void getQueryFactor(Map queryFactor) {

		// 获取条件值 CONTNO
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();
			if ("CONTNO".equalsIgnoreCase(key)) {
				mContNo = (String) queryFactor.get(key);
			}
			
			
			if ("GRPNAME".equalsIgnoreCase(key)) {
				name = (String) queryFactor.get(key);
			}
			
			if ("GRPNO".equalsIgnoreCase(key)) {
				grpno = (String) queryFactor.get(key);
			}
			
			if ("CUSTOMNO".equalsIgnoreCase(key)) {
				customNo = (String) queryFactor.get(key);
			}
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Map prepareData() {
		QueryTool qTool = new QueryTool();

		Map OUTPUTDATA = new HashMap();
		Map EDORLIST = new HashMap();
		List item = new ArrayList();
		Map info = null;
		pageIndex =baseInfoString[4];
		pageSize=baseInfoString[5];
		
		String rgtSql = "select lc.grpcontno, lc.cvalidate, codename('stateflag', lc.stateflag),lca.name appname,lca.organcomcode id, "
                      + "(case when (select count(1) from lcgrppol where grpcontno = lc.grpcontno "
                      + "and exists (select 1 from lmriskapp where risktype4='4' and riskcode = lcgrppol.riskcode)) = 0 then '1' else '4' end) "
                      + "from lcgrpcont lc inner join lcgrpappnt lca on lc.grpcontno = lca.grpcontno "
                      + "where 1=1 ";
                      
		StringBuffer sql = new StringBuffer();
		StringBuffer numSQL = new StringBuffer();
		numSQL.append("select count(1) from lcgrpcont lc inner join lcgrpappnt lca on lc.grpcontno = lca.grpcontno where 1=1 ");
		sql.append(rgtSql);
		// 查询条件
		
		if (mContNo != null && !"".equals(mContNo)){
			sql.append("and  lc.grpcontno = '"+mContNo+"' ");
			numSQL.append("and  lc.grpcontno = '"+mContNo+"' ");
		}
		
		
		if (name != null && !"".equals(name)){
			sql.append(" and lca.name like '"+name+"%'");
			numSQL.append(" and lca.name like '"+name+"%'");
		}
		
		
		if (grpno != null && !"".equals(grpno)){
			sql.append(" and lca.organcomcode = '"+grpno+"'");
			numSQL.append(" and lca.organcomcode = '"+grpno+"'");
		}
		
		if (customNo != null && !"".equals(customNo)){
			sql.append(" and lca.customerno = '"+customNo+"'");
			numSQL.append(" and lca.customerno = '"+customNo+"'");
		}
		
		sql.append(" order by lc.grpcontno");
		String num = new ExeSQL().getOneValue(numSQL.toString());
		String pageSQL=qTool.buildPageSql(sql.toString(),Integer.valueOf(pageIndex).intValue(),Integer.valueOf(pageSize).intValue());
		SSRS contSSRS = mExeSQL.execSQL(pageSQL);
		//传入记录条数
		this.setRecordCount(String.valueOf(num));
		if(contSSRS.MaxRow>0){
			String[] errInfoArr = {"00","0","成功"};
			this.setErrInfoArr(errInfoArr);
		}
		for (int i = 1; i <= contSSRS.MaxRow; i++) {
			info = new HashMap();

			info.put("CONTNO", contSSRS.GetText(i, 1));
			info.put("CVALIDATE", contSSRS.GetText(i, 2));
			info.put("CONTSTATE", contSSRS.GetText(i, 3));
			info.put("GRPNAME", contSSRS.GetText(i, 4));
			info.put("GRPNO", contSSRS.GetText(i, 5));
			info.put("RISKTYPE4",contSSRS.GetText(i, 6));

			item.add(info);
		}
		EDORLIST.put("ITEM", item);
		OUTPUTDATA.put("CLIST", EDORLIST);
		// 查询业务数据并封装
		return OUTPUTDATA;
	}
}
