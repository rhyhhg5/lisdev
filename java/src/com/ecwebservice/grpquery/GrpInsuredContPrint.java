package com.ecwebservice.grpquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

public class GrpInsuredContPrint extends StandCardHYX{

	Set insuContNoSet = new HashSet();
	
	String mContNo = "";
	
	public GrpInsuredContPrint(){
		super("GRPINSUREDCONTPRINT");
	}
	
	
	protected void getQueryFactor(Map queryFactor) {
		// TODO Auto-generated method stub
		//获取查询条件
		Iterator factor = queryFactor.keySet().iterator();
		
		while(factor.hasNext()){
			String key = factor.next().toString();
			mContNo = (String)queryFactor.get(key);
			insuContNoSet.add(mContNo);
			System.out.println("团体客户--团单个单保险凭证打印信息查询,ContNo="+mContNo);
		}
	}

	
	public Map prepareData() {
		// TODO Auto-generated method stub
		
		Map OUTPUTDATA = new HashMap();
		List list = new ArrayList();
		Map CLIST = new HashMap();
		
		Iterator insuContNoIterator = insuContNoSet.iterator();
		//System.out.println("mContNo--------"+insuContNoIterator.next().toString());
		//System.out.println("true or false----"+insuContNoIterator.hasNext());
		while(insuContNoIterator.hasNext()) {
			
			mContNo = insuContNoIterator.next().toString();
			System.out.println("--------------mContNo--------"+mContNo);
			
			Map data = new HashMap();
			
			String sqla = "select d.riskcode 险种代码, " +
						  " ( select riskname from lmriskapp where riskcode = d.riskcode) 险种名称,d.amnt 保额, "  +
						  " lc.dutycode 责任代码,lc.amnt 医疗费用限额,lc.mult 档次, " + 
						  " (select calfactorvalue from LCContPlanDutyParam where grpcontno=d.grpcontno and dutycode=lc.dutycode and contplancode = d.contplancode and calfactor='GetLimit') 免赔额, " +
						  " (select calfactorvalue from LCContPlanDutyParam where grpcontno=d.grpcontno and dutycode=lc.dutycode and contplancode = d.contplancode and calfactor='GetRate' ) 给付比例, " +
						  " lc.prem 保险费, (select dutyname from lmduty where dutycode = lc.dutycode) 责任名称,lc.contno " +
						  " from lcpol d,lcduty lc " +
						  " where lc.polno = d.polno and lc.contno = d.contno and d.contno= '" + mContNo + "'";
			
			SSRS riskSSRS = mExeSQL.execSQL(sqla);
			Map riskInfos = new HashMap();
			Map info = null;
			List item = new ArrayList();
			for(int i = 1; i <= riskSSRS.MaxRow; i ++){
				info = new HashMap();
				info.put("RISKCODE", riskSSRS.GetText(i, 1));
				info.put("RISKNAME", riskSSRS.GetText(i, 2));
				info.put("RISKAMNT", riskSSRS.GetText(i, 3));
				info.put("DUTYCODE", riskSSRS.GetText(i, 4));
				info.put("LIMITAMNT", riskSSRS.GetText(i, 5));
				info.put("MULT", riskSSRS.GetText(i, 6));
				info.put("GETLIMIT", riskSSRS.GetText(i, 7));
				info.put("GETRATE", riskSSRS.GetText(i, 8));
				info.put("PREM", riskSSRS.GetText(i, 9));
				info.put("DUTYNAME", riskSSRS.GetText(i, 10));
				info.put("RCONTNO", riskSSRS.GetText(i, 11));
				item.add(info);
			}
			riskInfos.put("RISKITEM", item);
			data.put("RISKINFO", riskInfos);
			
			String sqlb = "select name,codename('idtype',idtype),idno,contno from lcbnf where contno='" + mContNo + "'";
			SSRS bnfSSRS = mExeSQL.execSQL(sqlb);
			Map bnfInfos = new HashMap();
			Map bnfInfo = null;
			List bnfItem = new ArrayList();
			for(int i = 1; i <= bnfSSRS.MaxRow; i ++){
				bnfInfo = new HashMap();
				bnfInfo.put("BNFNAME", bnfSSRS.GetText(i, 1));
				bnfInfo.put("BNFIDTYPE", bnfSSRS.GetText(i, 2));
				bnfInfo.put("BNFIDNO", bnfSSRS.GetText(i, 3));
				bnfInfo.put("BCONTNO", bnfSSRS.GetText(i, 4));
				bnfItem.add(bnfInfo);
			}
			bnfInfos.put("BNFITEM", bnfItem);
			data.put("BNFINFO", bnfInfos);
			
			String sqlc = "select name,insuredno,codename('idtype',idtype),idno,contno from lcinsured where contno='" + mContNo + "'";
			SSRS insuSSRS = mExeSQL.execSQL(sqlc);
			Map insuInfos = new HashMap();
			Map insuInfo = null;
			List insuList = new ArrayList();
			for(int i = 1; i <= insuSSRS.MaxRow; i ++){
				insuInfo = new HashMap();
				insuInfo.put("INSUNAME", insuSSRS.GetText(i, 1));
				insuInfo.put("INSUNO", insuSSRS.GetText(i, 2));
				insuInfo.put("INSUIDTYPE", insuSSRS.GetText(i, 3));
				insuInfo.put("INSUIDNO", insuSSRS.GetText(i, 4));
				insuInfo.put("ICONTNO", insuSSRS.GetText(i, 5));
				insuList.add(insuInfo);
			}
			insuInfos.put("INSUITEM", insuList);
			data.put("INSULIST", insuInfos);
			
			String sqld = "select a.grpcontno,a.cvalidate 团单生效日期,a.grpname 投保人姓名,dc.name,dc.address,dc.zipcode, " +
							" a.agentcom,(select name from lacom where agentcom = a.agentcom ) 代理机构名称, " +
							" a.agentcode,(select name from laagent where agentcode = a.agentcode) 代理人名称, " +
							" c.contno 团单分单号,c.cvalidate 团单分单生效日期,codename('paymode',c.paymode) 缴费方式,c.signdate 签单日期,c.cinvalidate 失效日期, " +
							" (select min(confmakedate) from ljtempfee where 1=1 and otherno=c.grpcontno and tempfeetype='1') 收费确认日期 " +
							" from  lcgrpcont a,lccont c,ldcom dc " +
							" where  a.grpcontno = c.grpcontno and left(a.managecom,4) = dc.comcode and c.contno='" + mContNo + "'";
			
			SSRS contSSRS = mExeSQL.execSQL(sqld);
			if(contSSRS.MaxRow>0){
				String[] errInfoArr = {"00","0","成功"};
				this.setErrInfoArr(errInfoArr);
				data.put("GRPCONTNO", contSSRS.GetText(1, 1));
				data.put("GRPCVALIDATE", contSSRS.GetText(1, 2));
				data.put("APPNAME", contSSRS.GetText(1, 3));
				data.put("MANAGECOMNAME", contSSRS.GetText(1, 4));
				data.put("MANAGECOMADDR", contSSRS.GetText(1, 5));
				data.put("ZIPCODE", contSSRS.GetText(1, 6));
				data.put("AGENTCOM", contSSRS.GetText(1, 7));
				data.put("AGENTCOMNAME", contSSRS.GetText(1, 8));
				data.put("AGENTCODE", contSSRS.GetText(1, 9));
				data.put("AGENTCODENAME", contSSRS.GetText(1, 10));
				data.put("CONTNO", contSSRS.GetText(1, 11));
				data.put("CVALIDATE", contSSRS.GetText(1, 12));
				data.put("PAYMODE", contSSRS.GetText(1, 13));
				data.put("SIGNDATE", contSSRS.GetText(1, 14));
				data.put("ENDDATE", contSSRS.GetText(1, 15));
				data.put("FIRSTPAYDATE", contSSRS.GetText(1, 16));
			}
			list.add(data);
		}
		CLIST.put("ITEM", list);
		OUTPUTDATA.put("CLIST", CLIST);
		return OUTPUTDATA;
	}
	
}