package com.ecwebservice.utils;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CoreDAO {

	/**
	 * @desc
	 * @author Yuanzw 创建时间：Jul 30, 2010 9:29:09 AM
	 * @param
	 * @return
	 * @exception
	 */
	private ExeSQL mExeSQL = new ExeSQL();

	public SSRS getLicardactiveinfolistSSRS(String cardNo) {
		String querySql = "select * from licardactiveinfolist where cardNo='"
				+ cardNo + "'";
		SSRS resultSSRS = mExeSQL.execSQL(querySql);
		return resultSSRS;

	}
	
	/** 
	* @desc 校验核心单证系统中的单证号和密码
	* @author Yuanzw
	* 创建时间：Jul 30, 2010 1:57:47 PM
	* @param       
	* @return   卡号密码是否正确   
	* @exception  
	*/
	public boolean cardNoAndPassWordVerify(String cardNo,String passWord){
		String querySql = "select '1' from LZCardNumber where cardno = '"+cardNo+"' and cardpassword = '"+passWord+"'";
		String queryFlag =  mExeSQL.getOneValue(querySql);
		if("1".equals(queryFlag)){
			return true;
		}else{
			return false;
		}
		
	}
	
	/** 
	* @desc 校验某一卡号是否已经通过新网站或老网站激活过
	* @author Yuanzw
	* 创建时间：Jul 30, 2010 2:36:42 PM
	* @param       
	* @return    true：该卡号已经通过新网站或老网站激活过  
	* @exception  
	*/
	public boolean isCardNoActived(String cardNo){
		String verifySql = "select cardNo from licardactiveinfolist  where cardNo = '"+cardNo+"' union " +
				"select cardNo from wfcontlist  where cardNo = '"+cardNo+"'";
		SSRS tempSSRS = mExeSQL.execSQL(verifySql);
		if(tempSSRS.MaxRow>0){
			//该卡号已激活过
			return true;
			
		}else{
			return false;
		}
		
		
	}
	/** 
	 * @desc 校验某一卡号是否为撤单状态
	 * @author zhangyige
	 * 创建时间：2012-3-15
	 * @param       
	 * @return    true：该卡号已经为撤单状态 
	 * @exception  
	 */
	public boolean isCardNoCanseled(String cardNo){
		String verifySql = "select 1 from licardactiveinfolist  where cardNo = '"+cardNo+"'and cardstatus = '03'";
		SSRS tempSSRS = mExeSQL.execSQL(verifySql);
		if(tempSSRS.MaxRow>0){
			//该卡号已为撤单状态
			return true;
		}else{
			return false;
		}
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
