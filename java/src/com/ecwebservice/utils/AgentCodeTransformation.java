package com.ecwebservice.utils;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class AgentCodeTransformation {

	/**
	 * 业务员代码校验，校验规则
	 */
	private String result ;
	
	private boolean cheak;
	
	private String message;
	
	private ExeSQL mExeSQL = new ExeSQL();
	
	public boolean AgentCode(String AgentCode,String state) {
		System.out.println("输入的agentcode="+AgentCode);
		if(AgentCode!=null&&!"".equals(AgentCode)){
			String querySql = "select groupagentcode from laagent where (agentcode ='"+AgentCode+"' and groupagentcode !='') or (groupagentcode ='"+AgentCode+"' and agentcode !='')";
			SSRS resultSSRS = mExeSQL.execSQL(querySql);
			if(resultSSRS==null || resultSSRS.MaxRow==0){
				message = "业务员号码有误！";
				if("Y".equals(state)){
					cheak = false;
				}else if("N".equals(state)){
					cheak = true;
					result = AgentCode ;
				}
				return cheak ;
			}
			result = resultSSRS.GetText(1, 1);
			System.out.println("resultSSRS不为空，result: " + result);
		}else{
			if("Y".equals(state)){
				cheak = true;
			}else if("N".equals(state)){
				cheak = true;
			}
			result = AgentCode ;
		}
		System.out.println("输出的agentcode="+result);
		return cheak ;
	}
	//返回业务员代码
	public String getResult(){
        return result;
    }
	//返回信息
	public String getMessage(){
        return message;
    }
	//返回状态
	public boolean getCheak(){
        return cheak;
    }

	public static void main(String[] args) {
		AgentCodeTransformation a = new AgentCodeTransformation();
		if(!a.AgentCode("2113067250", "Y")){
			System.out.println(a.getMessage());
			if(a.getCheak()){
				System.out.println("返回失败");
			}
		}
		System.out.println("---" + a.getResult());
	}

}
