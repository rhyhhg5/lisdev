package com.ecwebservice.services;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class GetActiveCardDetail 
{
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	public boolean submitData()
    {
		System.out.println("开始运行激活卡明细提取批处理*********");
		for(int j=1 ;j<=1100; j++)
    	{
    		System.out.println("第 ************************************************************次提取：" + j);
    		String sql = "select * from LICardActiveInfoList a where not exists (select 1 from LIActiveCardDetail where CardNo = a.CardNo and SequenceNo = a.SequenceNo) and makedate >= current date - 7 days fetch first 5000 rows only with ur";
    		System.out.println(sql);
    		LICardActiveInfoListDB tLICardActiveInfoListDB = new LICardActiveInfoListDB();
    		LICardActiveInfoListSet tLICardActiveInfoListSet = tLICardActiveInfoListDB.executeQuery(sql);
    	 	System.out.println("拢共条数：" + tLICardActiveInfoListSet.size());
    	 	if(tLICardActiveInfoListSet.size()==0)
    	 	{
    	 		break;
    	 	}
    	 	for(int i=1; i<= tLICardActiveInfoListSet.size(); i++)
    	 	{
    	 		LICardActiveInfoListSchema tLICardActiveInfoListSchema = tLICardActiveInfoListSet.get(i).getSchema();
    	 		String aCardNo = tLICardActiveInfoListSchema.getCardNo();
    	 		String aActiveDate = tLICardActiveInfoListSchema.getActiveDate()==null?"1981-4-2":tLICardActiveInfoListSchema.getActiveDate();
    	 		System.out.println("第*********次提取：" + j + "************正在处理第********** 条 ：" + i +"**********卡单 = " + aCardNo + "**********激活日期： " + aActiveDate);
    	 		try
    	 		{
        	 		String tCardNo = aCardNo;
        	 		String tReceiveSale= "";
        	 		String tManageCom= "";
        	 		String tCValidate= tLICardActiveInfoListSchema.getCValidate()==null?"1981-4-2":tLICardActiveInfoListSchema.getCValidate();
        	 		String tActiveDate= aActiveDate;
        	 		String tInsuredName= tLICardActiveInfoListSchema.getName()==null?"":tLICardActiveInfoListSchema.getName();
        	 		String tInsuredSex= tLICardActiveInfoListSchema.getSex()==null?"":tLICardActiveInfoListSchema.getSex();
        	 		String tInsuredBirthDay= tLICardActiveInfoListSchema.getBirthday()==null?"1981-4-2":tLICardActiveInfoListSchema.getBirthday();
        	 		String tInsuredIDType= tLICardActiveInfoListSchema.getIdType()==null?"":tLICardActiveInfoListSchema.getIdType();
        	 		String tInsuredIDNo= tLICardActiveInfoListSchema.getIdNo()==null?"":tLICardActiveInfoListSchema.getIdNo();
        	 		String tPostalAddress= tLICardActiveInfoListSchema.getPostalAddress()==null?"":tLICardActiveInfoListSchema.getPostalAddress();
        	 		String tZipCode= tLICardActiveInfoListSchema.getZipCode()==null?"":tLICardActiveInfoListSchema.getZipCode();
        	 		String tPhone= tLICardActiveInfoListSchema.getPhone()==null?"":tLICardActiveInfoListSchema.getPhone();
        	 		String tMobile= tLICardActiveInfoListSchema.getMobile()==null?"":tLICardActiveInfoListSchema.getMobile();
        	 		String tActiveOperator= tLICardActiveInfoListSchema.getOperator()==null?"":tLICardActiveInfoListSchema.getOperator();
        	 		String tPrem= tLICardActiveInfoListSchema.getPrem()==null?"":tLICardActiveInfoListSchema.getPrem();
        	 		
        	 		//查询条件 
        	 		String tOperateType= "";
        	 		String tCertifyCode= "";
        	 		String tCertifyName= "";
        	 		
        	 		//多存中间数据字段
        	 		String tReceiveCom= "";
        	 		String tSendOutCom= "";
        	 		String tReceiveSaleType= "";
        	 		
        	 		String tOPERATOR= "Server";
        	 		String tMAKEDATE= CurrentDate;
        	 		String tMAKETIME= CurrentTime;
        	 		String tMODIFYDATE= CurrentDate;
        	 		String tMODIFYTIME= CurrentTime;
        	 		
        	 		//新增主键
        	 		String tSequenceNo = tLICardActiveInfoListSchema.getSequenceNo()==null?"1":tLICardActiveInfoListSchema.getSequenceNo();
        	 		
        	 		LZCardNumberDB tLZCardNumberDB = new LZCardNumberDB();
        	 		tLZCardNumberDB.setCardNo(aCardNo);
        	 		LZCardNumberSet tLZCardNumberSet = tLZCardNumberDB.query();
        	 		if(tLZCardNumberSet.size()==0)
        	 		{
        	 			System.out.println("没找到卡单的LZCardNumber数据**********= " + aCardNo );
        	 		}
        	 		else
        	 		{
        	 			LZCardNumberSchema tLZCardNumberSchema = new LZCardNumberSchema();
            	 		tLZCardNumberSchema.setSchema(tLZCardNumberSet.get(1).getSchema());
            	 		
            	 		LZCardDB tLZCardDB = new LZCardDB();
            	 		tLZCardDB.setSubCode(tLZCardNumberSchema.getCardType());
            	 		tLZCardDB.setStartNo(tLZCardNumberSchema.getCardSerNo());
            	 		tLZCardDB.setEndNo(tLZCardNumberSchema.getCardSerNo());
            	 		LZCardSet tLZCardSet = tLZCardDB.query();
            	 		if(tLZCardSet.size()==0)
            	 		{
            	 			System.out.println("没找到卡单的LZCard数据**********= " + aCardNo );
            	 		}
            	 		else
            	 		{
            	 			LZCardSchema tLZCardSchema = new LZCardSchema();
                	 		tLZCardSchema.setSchema(tLZCardSet.get(1).getSchema());
                	 		
                	 		tCertifyCode= tLZCardSchema.getCertifyCode();
                	 		
                	 		tReceiveCom= tLZCardSchema.getReceiveCom();
                	 		tSendOutCom= tLZCardSchema.getSendOutCom();
                	 		
                	 		String R1 = tReceiveCom.substring(0, 1);
                	 		String S1 = tSendOutCom.substring(0, 1);
                	 		
                	 		String tempReceiveSale = ""; 
                	 		if(R1.equals("D") || R1.equals("E"))
                	 		{
                	 			tempReceiveSale = tReceiveCom;
                	 		}
                	 		else if(S1.equals("D") || S1.equals("E"))
                	 		{
                	 			tempReceiveSale = tSendOutCom;
                	 		}
                	 		else
                	 		{
                	 			tempReceiveSale = "fuck";
                	 		}
                	 		
                	 		tReceiveSaleType = tempReceiveSale.substring(0, 1);
                	 		tReceiveSale = tempReceiveSale.substring(1);
                	 		
                	 		String comSql = "";
                	 		if(tReceiveSaleType.equals("D"))
                	 		{
                	 			comSql = "select laa.ManageCom from LAAgent laa where laa.AgentCode = '" + tReceiveSale + "' ";
                	 		}
                	 		else if(tReceiveSaleType.equals("E"))
                	 		{
                	 			comSql = "select lac.ManageCom from LACom lac where lac.AgentCom = '" + tReceiveSale + "' ";
                	 		}
                	 		tManageCom= new ExeSQL().getOneValue(comSql);
                	 		
                	 		LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
                	 		tLMCertifyDesDB.setCertifyCode(tLZCardSchema.getCertifyCode());
                	 		if(!tLMCertifyDesDB.getInfo())
                	 		{
                	 			System.out.println("没找到卡单的LMCertifyDes数据**********= " + aCardNo );
                	 		}
                	 		else
                	 		{
                	 			LMCertifyDesSchema tLMCertifyDesSchema = new LMCertifyDesSchema();
                    	 		tLMCertifyDesSchema.setSchema(tLMCertifyDesDB.getSchema());
                    	 		tOperateType= tLMCertifyDesSchema.getOperateType();
                    	 		tCertifyName= tLMCertifyDesSchema.getCertifyName();
                	 		}
            	 		}
        	 		}
        	 		
        	 		//表结构 -- 字段按顺序存储
        	 		StringBuffer insertSql = new StringBuffer();
        	 		insertSql.append("insert into LIActiveCardDetail values ('") 
        	 			.append( tCardNo +  "', '" + tReceiveSale +  "', '" + tManageCom +  "', '" + tCValidate +  "', '" + tActiveDate +  "', '") 
        	 			.append( tInsuredName +  "', '" + tInsuredSex +  "', '" + tInsuredBirthDay +  "', '" + tInsuredIDType +  "', '" + tInsuredIDNo +  "', '") 
        	 			.append(tPostalAddress +  "', '" + tZipCode +  "', '" + tPhone +  "', '" + tMobile +  "', '" + tActiveOperator +  "', '" + tPrem +  "','") 
        	 			.append(tOperateType +  "', '" + tCertifyCode +  "', '" + tCertifyName +  "','" + tReceiveCom +  "', '" + tSendOutCom +  "', '" + tReceiveSaleType +  "', ") 
        	 			.append("null, null, null, null, null, null, null, null,'") 
        	 			.append(tOPERATOR +  "', '" + tMAKEDATE +  "', '" + tMAKETIME +  "', '" + tMODIFYDATE +  "', '" + tMODIFYTIME + "', '" + tSequenceNo + "') ");
        	 		
        	 		System.out.println("插入数据**********= " + aCardNo);
        	 		MMap map = new MMap();
        	 		map.put(insertSql.toString(), "INSERT");
        	 		if (!submit(map))
        		    {
        	 			System.out.println("数据提交失败**********= " + aCardNo );
        		    }
    	 		}
    	 		catch(Exception e)
    	 		{
    	 			System.out.println("抛异常，继续跑***************= " + aCardNo );
    	 		}
    	 	}
    	}
	    return true;
       
    }
	
	/**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            return false;
        }
        return true;
    }

	public static void main(String[] args) 
	{
        new GetActiveCardDetail().submitData();
	}
}
