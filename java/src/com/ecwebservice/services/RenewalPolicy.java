package com.ecwebservice.services;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.sinosoft.utility.ExeSQL;

/**
 * 查询可投保的保单
 * @author Administrator
 *
 */
public class RenewalPolicy {
	private OMElement responseOME = null;
	private String contnos;
	private String errInfo;
	private boolean flag;
	
	public OMElement queryData(OMElement oms){
		System.out.println("获取报文："+oms.toString());
		//解析报文
		flag = parseDate(oms);
		if(!flag){
			System.out.println(errInfo);
		}
		//查询出能够抽档的保单
		String str = allContnos(contnos);
		//返回报文
		responseOME = buildResponse(str);
		return responseOME;
	}
	//解析报文，获取所有保单号
	public boolean parseDate(OMElement oms){
		System.out.println("接收到的报文为："+oms);
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement om = (OMElement)iterator.next();
				if(om.getLocalName().equals("RENEWALPOLICY")){
					Iterator child = om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("CONTNO")){
							contnos = child_Element.getText();
							return true;
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			errInfo = "报文解析异常，无法获得保单号";
			return false;
		}
		return true;
	}
	
	//查询出能够抽档的保单
	public String allContnos(String contnos){
		String[] strs = contnos.split(",");
		boolean b;
		boolean c;
		StringBuffer sf = new StringBuffer();
		for(int i=0;i<strs.length;i++){
			b = ispickRecord(strs[i]);
			c = isOverdue(strs[i]);
			if(b&&c){
				sf.append(strs[i]);
				sf.append(",");
			}
		}
		return sf.toString();
	}
	//查询是否有抽档记录，如果有抽档记录就可以在线续保
	public boolean ispickRecord(String contNo){
		String recordSql = "select count(*) from LJSPay where otherno='"+contNo+"' with ur";
		ExeSQL exeSQL = new ExeSQL();
		String s = exeSQL.getOneValue(recordSql);
		if(s.equals("0")){
			errInfo = "该保单没有抽档";
			return false;
		}
		return true;
	}
	//查询是否已过缴至日期30天
	public boolean isOverdue(String contNo){
		String sql = "select count(*) from lcpol where contno = '"+contNo+"' and days(current date) - days(date(enddate)) <=30 with ur";
		ExeSQL exeSQL = new ExeSQL();
		String s = exeSQL.getOneValue(sql);
		if(s.equals("0")){
			errInfo = "该保单已过期";
			return false;
		}
		return true;
	}
//	返回报文
	public OMElement buildResponse(String contnos){
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement renewalPolicy = factory.createOMElement("RENEWALPOLICY",null);
		addOm(renewalPolicy,"CONTNO",contnos);
		root.addChild(renewalPolicy);
		return root;
	}
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
	private static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}
}
