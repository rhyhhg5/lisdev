package com.ecwebservice.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMElement;

import com.mail.Mailsender2;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


/**
 * 功能：续保提醒
 * @author Administrator
 *
 */
public class ReContInfo {
	private boolean flag;
	private String info;
	private String case1;
	private String case2;
	private String case3;
	private String case4;
	private ArrayList contnoslist1 = new ArrayList();
	private ArrayList contnoslist2 = new ArrayList();
	private ArrayList contnoslist3 = new ArrayList();
	private ArrayList contnoslist4 = new ArrayList();
	private String[] contnos1;
	private String[] contnos2;
	private String[] contnos3;
	private String[] contnos4;
	private ExeSQL mexeSQL = new ExeSQL();
	public OMElement queryData(OMElement oms){
		//将保单号码分类（四种情况：1.满期前30日放送电子保单。2.未交费客户于7日前，发送满期续保短信。3.满期日次日。4.满期次日起15日）
		OMElement responseOME = null;
		flag = getInfo(oms);
		if(!flag){
			System.out.println(info);
			return oms;
		}
		flag = cleanInfo(case1, case2, case3, case4);
		if(!flag){
			System.out.println(info);
			return oms;
		}
		flag = rejectCont(contnos1, contnos2, contnos3, contnos4);
		if(!flag){
			System.out.println(info);
			return oms;
		}
		flag = sendMailOrMsg();
		if(!flag){
			System.out.println(info);
			return oms;
		}
		return oms;
	}
	/*
	 * 取出报文中的四种不同情况的保单字符串
	 */
	private boolean getInfo(OMElement oms){
		System.out.println("接收的报文为："+oms);
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement Om = (OMElement)iterator.next();
				if(Om.getLocalName().equals("RECONT")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("CASE1")){
							case1 = child_Element.getText();
						}
						if(child_Element.getLocalName().equals("CASE2")){
							case2 = child_Element.getText();
						}
						if(child_Element.getLocalName().equals("CASE3")){
							case3 = child_Element.getText();
						}
						if(child_Element.getLocalName().equals("CASE4")){
							case4 = child_Element.getText();
						}
						
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			info = "获取报文中的四种保单号字符串失败";
			return false;
		}
		return true;
	}
	/*
	 * 对四种情况的保单字符串进行整理，整理成字符串数组(并且符合业务条件)
	 */
	private boolean cleanInfo(String case1,String case2,String case3,String case4){
		try {
			if(case1==null||case1.equals("")){
				contnos1 = null;
			}else{
				contnos1 = case1.trim().split(",");//即是距离满期30日的保单号集合
			}
			if(case2==null||case2.equals("")){
				contnos2 = null;
			}else{
				contnos2 = case2.trim().split(",");
			
			}
			if(case3==null||case3.equals("")){
				contnos3 = null;
			}else{
				contnos3 = case3.trim().split(",");
			}
			if(case4==null||case4.equals("")){
				contnos4 = null;
			}else{
				contnos4 = case4.trim().split(",");
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			info="将保单号字符串转换为数组失败。";
			return false;
		}
		return true;
	}
	/*
	 * 剔除已缴费的保单号
	 */
	private boolean rejectCont(String[] contnos1,String[] contnos2,String[] contnos3,String[] contnos4){
		try {
			if(contnos1!=null){
				for(int i=0;i<contnos1.length;i++){
					contnoslist1.add(contnos1[i]);
				}
			}
			if(contnos2!=null){
				for(int i=0;i<contnos2.length;i++){
					String sql = "select count(*) from LJSPAYPERSON where Contno='"+contnos2[i]+"'";
					String size = mexeSQL.getOneValue(sql);
					if(!size.equals("0")){
						contnoslist2.add(contnos2[i]);
					}
				}
			}
			if(contnos3!=null){
				for(int i=0;i<contnos3.length;i++){
					String sql = "select count(*) from LJSPAYPERSON where Contno='"+contnos3[i]+"'";
					String size = mexeSQL.getOneValue(sql);
					if(!size.equals("0")){
						contnoslist3.add(contnos3[i]);
					}
				}
			}
			if(contnos4!=null){
				for(int i=0;i<contnos4.length;i++){
					String sql = "select count(*) from LJSPAYPERSON where Contno='"+contnos4[i]+"'";
					String size = mexeSQL.getOneValue(sql);
					if(!size.equals("0")){
						contnoslist4.add(contnos4[i]);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			info = "剔除已缴费的保单号出现异常";
			return false;
		}
		return true;
	}
	/*
	 * 放送短信或邮件
	 */
	private boolean sendMailOrMsg(){//关键是接收者，主题，和内容
		//contnoslist1:获取接收者
		if(contnoslist1!=null){
			for(int i=0;i<contnoslist1.size();i++){
				boolean b = sendMail1((String)contnoslist1.get(i),"1");
				if(b==false){
					System.out.println("给保单号为："+contnoslist1.get(i)+"的客户发送邮件失败");
				}
			}
		}
		if(contnoslist2!=null){
			sendMessage1(contnoslist2,"1");
		}
		
		if(contnoslist3!=null){
			for(int i=0;i<contnoslist3.size();i++){
				boolean b = sendMail1((String)contnoslist3.get(i),"2");
				if(b==false){
					System.out.println("给保单号为："+contnoslist1.get(i)+"的客户发送失败");
				}
			}
		}
		if(contnoslist4!=null){
			
			sendMessage1(contnoslist4,"2");
		}
		return true;
	}
	/*
	 * 发送邮件1，以及发送邮件2
	 */
	private boolean sendMail1(String contno,String type){
		try {
			String contents;
			String subject = "个险续期续保缴费通知书";
			SSRS tSsrs = new SSRS();
			SSRS tSsrs2 = new SSRS();
			String sql = "select riskcode,(select riskname from lmrisk where riskcode = a.riskcode ),insuredname,amnt,cvalidate,ljspayp.prem,Appntname,enddate+1 days,enddate+30 days "
				+" from lcpol a inner join (select sum(SumDuePayMoney) as prem,polno from ljspayperson where contno='"+contno+"' group by polno) as ljspayp "
				+" on a.polno = ljspayp.polno where contno='"+contno+"' " ;
			String sql2 = "select appntName,appntsex,appntno,addressno from lcappnt where contno='"+contno+"' with ur";
			tSsrs = mexeSQL.execSQL(sql);
			tSsrs2= mexeSQL.execSQL(sql2);
			String sql3 = "select email from lcaddress where customerno='"+tSsrs2.GetText(1, 3)+"' and addressno='"+tSsrs2.GetText(1, 4)+"' with ur";
			String sql4 = "select sumduepaymoney from ljspay where otherno='"+contno+"'with ur";
			String receiver = mexeSQL.getOneValue(sql3);
			String sumprem = mexeSQL.getOneValue(sql4);
			if(receiver==null||receiver.equals("")){
				return true;
			}
			if(type.equals("1")){
				contents =  Mailsender2.getContent(tSsrs2.GetText(1, 1), tSsrs2.GetText(1, 2), sumprem, contno, tSsrs);
				
			}
			else{
				
				contents =  Mailsender2.getContent2(tSsrs2.GetText(1, 1), tSsrs2.GetText(1, 2), sumprem, contno, tSsrs);
			}
			Mailsender2.sendEmail(receiver, subject, contents);
		} catch (Exception e) {
			// TODO: handle exception
			
			return false;
		}
		return true;
	}
	/*
	 * 发送短信1,和短信2
	 */
	private boolean sendMessage1(ArrayList contnolist,String type){
		List contnomapList = new ArrayList();
		for(int i=0;i<contnolist.size();i++){
			String contno = (String)contnolist.get(i);
			try {
				
				SSRS tSsrs2 = new SSRS();
				String sql = "select enddate from lcpol a where contno='"+contno+"' with ur";
				String sql2 ="select appntName,appntSex,appntno,addressno from lcappnt where contno='"+contno+"' with ur";
				String enddate = mexeSQL.getOneValue(sql);
				tSsrs2= mexeSQL.execSQL(sql2);
				String sql3 = "select mobile from lcaddress where customerno='"+tSsrs2.GetText(1, 3)+"' and addressno='"+tSsrs2.GetText(1, 4)+"' with ur";
				String sql4 = "select sumprem from lccont where  contno='"+contno+"' with ur";
				String mobile = mexeSQL.getOneValue(sql3);
				String sumprem = mexeSQL.getOneValue(sql4);
				Map contnoMap = new HashMap();
				contnoMap.put("appName", tSsrs2.GetText(1, 1));
				contnoMap.put("appSex", tSsrs2.GetText(1, 2));
				contnoMap.put("contNo", contno);
				contnoMap.put("enddate", enddate);
				contnoMap.put("sumprem", sumprem);
				contnoMap.put("mobile", mobile);
				contnomapList.add(contnoMap);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("给保单号为："+contno+"的保单发送邮件失败");
				
			}
		}
		Mailsender2 ms = new Mailsender2();
		ms.sendMsg(contnomapList, type);
		return true;
	}
	public static void main(String[] args) {
		new ReContInfo().sendMail1("013970182000018","1");
	}
}
