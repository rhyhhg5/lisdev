package com.ecwebservice.services;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/*
 * 获取续保信息
 */
public class queryRenewalInfo {
	private ExeSQL mexeSQL = new ExeSQL();
	private String contNo;
	private String errInfo;
	private boolean flag;
	private OMElement responseOME = null;
	public OMElement queryData(OMElement oms){
		flag = parseDate(oms);
		if(!flag){
			System.out.println(errInfo);
			responseOME = buildResponse(oms, errInfo);
			return responseOME;
		}
		flag = queryRenewalInfoByContno(oms);
		if(!flag){
			System.out.println(errInfo);
			responseOME  = buildResponse(oms, errInfo);
			return responseOME;
		}
		System.out.println("返回报文信息："+responseOME);
		return responseOME;
		
	}
	//解析报文获报文中的保单号
	public boolean parseDate(OMElement oms){
		System.out.println("接收到的报文为："+oms);
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement om = (OMElement)iterator.next();
				if(om.getLocalName().equals("RENEWALQUERY")){
					Iterator child = om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("CONTNO")){
							contNo = child_Element.getText();
							return true;
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			errInfo = "报文解析异常，无法获得保单号";
			return false;
		}
		return true;
	}
	//查询保单续保信息
	public boolean queryRenewalInfoByContno(OMElement oms){
		try {
			//查看是否抽档。如果没有抽档，就没有续保信息
			String checkSql = "select count(*) from LJSPay where otherno='"+contNo+"' with ur";
			ExeSQL exeSQL = new ExeSQL();
			String s = exeSQL.getOneValue(checkSql);
			if(s.equals("0")){
				return false;
			}
			SSRS tSsrs = new SSRS();
			SSRS tSsrs2 = new SSRS();
			String sql = "select row_number()over(),(select riskname from lmrisk where riskcode = a.riskcode ),insuredname,amnt,cvalidate,ljspayp.prem,Appntname,enddate+1 days,enddate+30 days "
					+" from lcpol a inner join (select sum(SumDuePayMoney) as prem,polno from ljspayperson where contno='"+contNo+"' group by polno) as ljspayp "
					+" on a.polno = ljspayp.polno where contno='"+contNo+"' " 
					+" and days(current date) - days(date(enddate)) <=30 with ur";
			String sql2 = "select appntName,appntsex from lcappnt where contno='"+contNo+"' with ur";
			tSsrs = mexeSQL.execSQL(sql);
			tSsrs2= mexeSQL.execSQL(sql2);
			String sql3 = "select sumduepaymoney from ljspay where otherno='"+contNo+"' with ur";
			String sumprem = mexeSQL.getOneValue(sql3);
			responseOME = buildResponse(oms, tSsrs, tSsrs2,sumprem);
		} catch (Exception e) {
			// TODO: handle exception
			
			return false;
		}
		return true;
	}
	//返回报文
	public OMElement buildResponse(OMElement oms,String info){
		addOm(oms, "RsMsg", info);
		return oms;
	}
	public OMElement buildResponse(OMElement oms,SSRS tssrs1,SSRS tssrs2,String sumprem){
		OMFactory factory = OMAbstractFactory.getOMFactory();
		for(int i=1;i<=tssrs1.getMaxRow();i++){
			OMElement record = factory.createOMElement("RECORD",null);
			addOm(record, "RISKCODE", tssrs1.GetText(i, 1));
			addOm(record, "RISKNAME", tssrs1.GetText(i, 2));
			addOm(record, "INSUREDNAME", tssrs1.GetText(i, 3));
			addOm(record, "AMNT", tssrs1.GetText(i, 4));
			addOm(record, "CVALIDATE", tssrs1.GetText(i, 5));
			addOm(record, "PREM", tssrs1.GetText(i, 6));
			addOm(record, "APPNTNAME", tssrs1.GetText(i, 7));
			addOm(record, "ENDDATE", tssrs1.GetText(i, 8));
			addOm(record, "ENDDATE2", tssrs1.GetText(i, 9));
			
			oms.addChild(record);
		}
		OMElement appnt = factory.createOMElement("APPNT",null);
		addOm(appnt, "APPNTNAME", tssrs2.GetText(1, 1));
		addOm(appnt, "APPNTSEX", tssrs2.GetText(1, 2));
		addOm(appnt,"SUMPREM",sumprem);
		oms.addChild(appnt);
		addOm(oms, "RsMsg", "success");
		return oms;
	}
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
}
