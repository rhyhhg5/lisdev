package com.ecwebservice.services;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class UPCertPaytradestatus {
	
	public UPCertPaytradestatus(){}
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
	
	public boolean upCertStatus(String tGrpContNo){
		//回置清单列表
		if(tGrpContNo.equals("") || tGrpContNo==null){
			System.out.println("确认签单后,回置licertify.paytradestatus失败，没有得到老团单号！");
			buildError("upCertStatus", "确认签单后,回置licertify.paytradestatus失败,没有得到老团单号！");
	        return false;
		}
		String updateCertSQL="update licertify lic set lic.paytradestatus='01' " +
									"where lic.certoperatetype='4' and lic.state='04' "
										+" and lic.grpcontno is not null "
										+" and lic.prtno is not null "
										+" and lic.salechnl='15' "
										+" and lic.paytradestatus='00' "
										+" and lic.CertifyStatus='01' "
										+" and lic.proposalgrpcontno='"+tGrpContNo+"' "
										+" and exists(select 1 from liCertSalesPayInfo licsp where lic.PayTradeBatchNo = licsp.BatchNo)";
		
		System.out.println("updateCertSQL=="+updateCertSQL);
		MMap tMMap = new MMap();
		tMMap.put(updateCertSQL, SysConst.UPDATE);
		VData sVData = new VData();
		sVData.add(tMMap);
		PubSubmit p = new PubSubmit();
		if(!p.submitData(sVData, "")){
			System.out.println("确认签单后,回置licertify.paytradestatus失败！"+tGrpContNo);
			buildError("upCertStatus", "确认签单后,回置licertify.paytradestatus失败！");
	        return false;
		}
		return true;
	}
	
	/**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CardNoInfoToLicertiry";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String args[]){
    	UPCertPaytradestatus upstatus = new UPCertPaytradestatus();
    	upstatus.upCertStatus("1400006332");
    }
}
