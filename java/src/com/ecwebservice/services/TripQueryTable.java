package com.ecwebservice.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.ECPH_QUERY_TABLE;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class TripQueryTable extends ABusLogic {
	private ExeSQL mexeSQL = new ExeSQL();
	private ECPH_QUERY_TABLE eTable;
	private String BatchNoInfo = "";
	private String BranchCodeInfo = "";//查询出来的值
	private String TableName = "";//表名字
	private String CertifyName = "";//字段1名字
	private String CertifyCode = "";//字段1值

	/**
	 * 核心处理接口方法
	 * 
	 * @param String
	 *            电商发送给核心的参数报文
	 * @param STreing
	 *            核心返回给电商的结果报文
	 */
	protected boolean deal(MsgCollection cMsgInfos) {
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		String BatchNo = tMsgHead.getBatchNo();
		List tTable = cMsgInfos.getBodyByFlag("ECPH_QUERY_TABLE");
		if(tTable == null||tTable.size()!=1){
			errLog("获取报文信息失败");
			return false;
		}
		// 解析xml
		if (!parseXML(tTable)) {
			return false;
		}
		if (!dealLPMainAskInfo(BatchNoInfo,TableName,CertifyName,CertifyCode)) {
			return false;
		}
		return true;
	}

	private boolean parseXML(List tTable) {
		eTable = (ECPH_QUERY_TABLE)tTable.get(0);
		BatchNoInfo = eTable.getBatchNoInfo();
		BranchCodeInfo = eTable.getBranchCodeInfo();
		TableName = eTable.getTableName();
		CertifyName = eTable.getCertifyName();
		CertifyCode = eTable.getCertifyCode();
		System.out.println("开始处理报文！");
		return true;
	}

	private List mLLConsultSchema = new ArrayList();

	private boolean dealLPMainAskInfo(String BatchNoInfo,String TableName,String CertifyName ,String CertifyCode) {
		// 通过prtNo从数据库获取signDate
		if(BatchNoInfo!=null&&!"".equals(BatchNoInfo)&&TableName!=null&&!"".equals(TableName)&&CertifyName!=null&&!"".equals(CertifyName)&&CertifyCode!=null&&!"".equals(CertifyCode)){
			
			String sqlQuery = "select "+BatchNoInfo+" from "+TableName+" where "+CertifyName+" = "+CertifyCode+" fetch first 1 rows only ";
			System.out.println("sqlQuery=" + sqlQuery);
			SSRS result = mexeSQL.execSQL(sqlQuery);
			if (result == null || "".equals(result)) {
				return false;
			}
			for (int i = 1; i <= result.MaxRow; i++) {
				ECPH_QUERY_TABLE ldgrpcont = new ECPH_QUERY_TABLE();
				ldgrpcont.setBatchNoInfo(eTable.getBatchNoInfo());
				ldgrpcont.setBranchCodeInfo(result.GetText(i, 1));
				ldgrpcont.setTableName(eTable.getTableName());
				ldgrpcont.setCertifyName(eTable.getCertifyName());
				ldgrpcont.setCertifyCode(eTable.getCertifyCode());
				getWrapParmList(ldgrpcont);
			}
			getXmlResult();
			return true;
		}
		return false;
	}
	//返回报文
	private void getXmlResult() {
		for(int i=0;i<mLLConsultSchema.size();i++){
			putResult("ECPH_QUERY_TABLE",(ECPH_QUERY_TABLE)mLLConsultSchema.get(i));
		}
	}

	private void getWrapParmList(ECPH_QUERY_TABLE ldgrpcont) {
		mLLConsultSchema.add(ldgrpcont);
	}
}
