package com.ecwebservice.services;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.*;
import java.util.*;


import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.ECPH_SERACH_CONT;
import com.sinosoft.midplat.channel.util.IOTrans;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * 获取当天核心中有关湖北“去运动”的保单信息
 * @author fromyzf
 */
public class GoSportQueryCont extends ABusLogic {
	
	private ExeSQL mexeSQL = new ExeSQL();
	
	/**
	 * 核心处理接口方法
	 * @param String 电商发送给核心的参数报文
	 * @return Streing 核心返回给电商的结果报文
	 * @author fromyzf
	 */
	protected boolean deal(MsgCollection cMsgInfos) {
//		解析xml
        if(!parseXML(cMsgInfos)){
        	return false;
        }
        if(!dealLPMainAskInfo(getCurrentDateTime("yyyy-MM-dd"))){//getCurrentDateTime("yyyy-MM-dd")
        	return false;
        }
        return true;
	}
	private boolean parseXML(MsgCollection cMsgInfos){
		return true;
	}

	/**
	 * 获取当前日期和时间（自定义格式）
	 * 参考格式：yyyy年MM月dd日HH时（hh时）mm分ss秒ms毫秒E本月第F个星期
	 * 对应的值：2009年07月22日15时（03时）05分30秒530毫秒星期三本月第4个星期
	 * @param format	日期时间的格式
	 * @return			当前日期和时间
	 */
	public static String getCurrentDateTime(String format) {
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(new Date());
	}
	
	public static void main(String[] args) {
		
		//读取xml
		String xmlStr="";
		try {
			String xmlPath="D:\\gosport.xml";
			InputStream is=new FileInputStream(xmlPath);
			byte[] isXmlBytes = IOTrans.InputStreamToBytes(is);
			xmlStr=new String(isXmlBytes,"UTF-8");
			System.out.println("xmlStr:\n"+xmlStr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private List mLLConsultSchema=new ArrayList();
	private boolean dealLPMainAskInfo(String datePara) {
		// 通过prtNo从数据库获取batchNo
//		datePara="2015-1-28";

        	 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        	   Date date = new Date();
        	   try {
        	    date = sdf.parse(datePara);
        	   } catch (Exception e) {
        	    e.printStackTrace();
        	   }
        	   Calendar calendar = Calendar.getInstance(); //得到日历
        	   calendar.setTime(date);//把当前时间赋给日历
        	   calendar.add(Calendar.DAY_OF_MONTH, -1);  //设置为后一天
        	   String  firstday = sdf.format(calendar.getTime());

		String sqlQuery = "select wxc.batchno 批次号,"
				+ "wxc.branchcode 报送单位,"
				+ "wxc.MsgType 业务类型,"
				+ "wxc.prtno 印刷号,"
				+ "lcc.contno 保单号, "
				+"case when lcd.email is null or trim( lcd.email )='' then  (select lcd.email from LCAddress lcd where lca.appntno = lcd.customerno and lcd.email is not null order by int (addressno) desc fetch first 1 rows only) else lcd.email end 电邮,"
			    +"case When (Lcd.Phone Is Null Or Trim(Lcd.Phone) = '') Then  Case  When (Lcd.Mobile Is Null Or Trim(Lcd.Mobile) = '') Then (Select Case  When (Lcd.Phone Is Null Or Trim(Lcd.Phone) = '') Then  Lcd.Mobile Else   Lcd.Phone  End  From Lcaddress Lcd Where Lca.Appntno = Lcd.Customerno And ((Lcd.Phone Is Not Null and trim(Lcd.Phone)<>'') Or (Lcd.Mobile Is Not Null and trim(Lcd.Mobile)<>'') )Order By Int(Addressno) Desc Fetch First 1 Rows Only)   Else    Lcd.Mobile    End Else Lcd.Phone End  手机, "
				+ "lcc.operator 操作员,lcc.MakeDate 投保日期,lcc.MakeTime 投保时间,'个单' "
				+ "from WXContRelaInfo wxc, LCCont lcc, LCAppnt lca ,LCAddress lcd where 1=1 "
				+ "and lcc.prtno = lca.prtno "
                + "and lca.appntno=lcd.customerno "
				+ "and lca.addressno=lcd.addressno "
				+ "and lca.grpcontno = '00000000000000000000' "
				+ "and wxc.prtno = lcc.prtno "
				+ "and lcc.appflag = '1' "
				+ "and wxc.senddate between '"
				+ firstday
			    +"' and '"
				+ datePara
				+ "'  union all "
				+ "select wxc.batchno 批次号,wxc.branchcode 报送单位,wxc.MsgType 业务类型,wxc.prtno 印刷号,lcg.grpcontno 保单号,lcd.email 电邮,lcd.phone 手机,lcg.operator 操作员,lcg.MakeDate 投保日期,lcg.MakeTime 投保时间,'团单' "
				+ "from WXContRelaInfo wxc, LCGrpCont lcg, LCAppnt lca,LCAddress lcd where 1=1 "
				+ "and lcg.prtno = lca.prtno "
				+ "and lca.grpcontno != '00000000000000000000' "
				+ "and lca.addressno = lcd.addressno "
				+ "and lcg.appntno = lcd.customerno "
				+ "and lcg.appflag = '1' "
				+ "and wxc.senddate between '"
				+ firstday
			    +"' and '"
				+ datePara
				+ "' union all "
				+"select wfc.batchno 批次号,wfc.branchcode 报送单位,'cardno' 业务类型 ,wfc.cardno 印刷号,wfc.cardno 保单号, lic.email 电邮,lic.mobile 手机,lic.operator 操作员,lic.MakeDate 投保日期,lic.MakeTime 投保时间,'卡单' " 
				+"from WXContRelaInfo wxc,LICARDACTIVEINFOLIST lic,wfcontlist wfc "
				+"where wxc.prtno = lic.cardno and wxc.prtno = wfc.cardno and lic.cardstatus = '01'"			
			    + "and wxc.senddate between '"
				+ firstday
			    +"' and '"
				+ datePara
				+"' ";
		System.out.println("sqlQuery="+sqlQuery);
		SSRS result = mexeSQL.execSQL(sqlQuery);
		if(result==null || "".equals(result)){
			return false;
		}
		for (int i = 1; i <= result.MaxRow; i++) {
			ECPH_SERACH_CONT aLPCrmAccidentTable = new ECPH_SERACH_CONT();
			aLPCrmAccidentTable.setBATCHNOINFO(result.GetText(i, 1));
			aLPCrmAccidentTable.setBRANCHCODEINFO(result.GetText(i, 2));
			aLPCrmAccidentTable.setMSGTYPEINFO(result.GetText(i, 3));
			aLPCrmAccidentTable.setPRTNO(result.GetText(i, 4));
			aLPCrmAccidentTable.setCONTNO(result.GetText(i, 5));
			aLPCrmAccidentTable.setEMAIL(result.GetText(i, 6));
			aLPCrmAccidentTable.setPHONE(result.GetText(i, 7));
			aLPCrmAccidentTable.setOPERATORINFO(result.GetText(i, 8));
			aLPCrmAccidentTable.setMAKEDATEINFO(result.GetText(i, 9));
			aLPCrmAccidentTable.setMAKETIMEINFO(result.GetText(i, 10));
			getWrapParmList(aLPCrmAccidentTable);
		}
		getXmlResult();
		return true;
	}
	private void getWrapParmList(ECPH_SERACH_CONT aLPCrmAccidentTable){
    	mLLConsultSchema.add(aLPCrmAccidentTable);
    }
    
	//返回报文
	public void getXmlResult(){
		//返回退费信息
		for(int i=0;i<mLLConsultSchema.size();i++){
			putResult("ECPH_SERACH_CONT", (ECPH_SERACH_CONT)mLLConsultSchema.get(i));
		}

	}

	
}

