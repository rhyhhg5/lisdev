package com.ecwebservice.services;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class GetActiveTerm {
	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	public boolean submitData() {
		String aCardNo = "";
		System.out.println("开始运行激活卡保险期间提取批处理*********");
		try {
			for (int j = 1; j <= 2000; j++) {
				System.out
						.println("第 ************************************************************次提取："
								+ j);
				String sql = "select * from LICardActiveInfoList where insuyearflag is null or insuyearflag = '' fetch first 5000 rows only with ur";
				System.out.println(sql);
				LICardActiveInfoListDB tLICardActiveInfoListDB = new LICardActiveInfoListDB();
				LICardActiveInfoListSet tLICardActiveInfoListSet = tLICardActiveInfoListDB
						.executeQuery(sql);
				System.out.println("总共条数：" + tLICardActiveInfoListSet.size());
				if (tLICardActiveInfoListSet.size() == 0) {
					break;
				}

				for (int i = 1; i <= tLICardActiveInfoListSet.size(); i++) {
					LICardActiveInfoListSchema tLICardActiveInfoListSchema = tLICardActiveInfoListSet
							.get(i).getSchema();
					aCardNo = tLICardActiveInfoListSchema.getCardNo();
					System.out.println("卡号CardNo为：" + aCardNo);
					String aCValidate = tLICardActiveInfoListSchema
							.getCValidate();
					System.out.println("第*********次提取：" + j
							+ "************正在处理第********** 条 ：" + i
							+ "**********卡单 = " + aCardNo + "**********生效日期： "
							+ aCValidate);

					LoginVerifyTool tool = new LoginVerifyTool();
					String[] riskRelateCode = tool
							.getRiskSetCodeByCardNo(aCardNo);
					String riskSetCode = riskRelateCode[0];
					String[] insuYearInfoArr = tool.getInsuInfoFromCore(
							aCardNo, riskSetCode);
					String[] proInfoArr = tool.getProductInfo(aCardNo);
					// 获得反算出的保险期间、保险期间单位以及保险止期

					String tInsuyear = insuYearInfoArr[0];
					System.out.println("第*********次提取：" + j
							+ "************正在处理第********** 条 ：" + i
							+ "**********卡单 = " + aCardNo + "**********保险期间： "
							+ tInsuyear);

					String tInsuyearFlag = insuYearInfoArr[1];
					if (tInsuyearFlag == null)
						tInsuyearFlag = "Y";
					String tInActiveDate = proInfoArr[2];

					// 开始为数据赋值
					String aActiveDate = tLICardActiveInfoListSchema
							.getActiveDate() == null ? "1981-4-2"
							: tLICardActiveInfoListSchema.getActiveDate();
					System.out.println("第*********次提取：" + j
							+ "************正在处理第********** 条 ：" + i
							+ "**********卡单 = " + aCardNo + "**********激活日期： "
							+ aActiveDate);

					MMap map = new MMap();
					String tMODIFYDATE = CurrentDate;
					String tMODIFYTIME = CurrentTime;

					tLICardActiveInfoListSchema.setInsuYear(tInsuyear);
					tLICardActiveInfoListSchema.setInsuYearFlag(tInsuyearFlag);
					tLICardActiveInfoListSchema.setInActiveDate(tInActiveDate);
					tLICardActiveInfoListSchema.setModifyDate(tMODIFYDATE);
					tLICardActiveInfoListSchema.setModifyTime(tMODIFYTIME);
					// 获得卡号的WFContlist记录
					WFContListDB tWFContListDB = new WFContListDB();
					WFContListSet tWFContListSet = new WFContListSet();
					WFContListSet aWFContListSet = new WFContListSet();
					WFContListSchema tWFContListSchema = new WFContListSchema();
					tWFContListDB.setCardNo(aCardNo);
					tWFContListSet = tWFContListDB.query();
					if (tWFContListSet.size() > 0) {
						for (int k = 1; k <= tWFContListSet.size(); k++) {
							tWFContListSchema = tWFContListSet.get(k)
									.getSchema();
							tWFContListSchema.setInsuYear(tInsuyear);
							tWFContListSchema.setInsuYearFlag(tInsuyearFlag);
							aWFContListSet.add(tWFContListSchema);
						}
						map.put(aWFContListSet, "UPDATE");
					}

					System.out.println("修改数据**********= " + aCardNo);

					map.put(tLICardActiveInfoListSchema, "UPDATE");
					if (!submit(map)) {
						System.out.println("数据提交失败**********= " + aCardNo);
					}
				}
			}
		} catch (Exception e) {
			System.out.println("抛异常，继续跑***************= " + aCardNo);
		}
		return true;

	}

	/**
	 * 提交数据到数据库
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		new GetActiveTerm().submitData();
	}
}
