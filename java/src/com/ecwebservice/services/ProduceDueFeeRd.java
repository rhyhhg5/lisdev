package com.ecwebservice.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.xb.PRnewDueFeeUI;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ProduceDueFeeRd {
	CErrors tError = null;   
	private String[] contnos;
	private String startdate;//抽档开始日期
	private String enddate;//抽档结束日期
	
	private String info;
	private boolean flag;
	public OMElement queryData(OMElement oms){
		OMElement responseOME = null;
		flag = getInfo(oms);
		if(!flag){
			System.out.println(info);
			responseOME = buildResponse(oms,info);
			return responseOME;
		}
		flag = pdRecord();
		if(!flag){
			System.out.println(info);
			responseOME = buildResponse(oms, info);
			return responseOME;
		}
		responseOME = buildResponse(oms, info);
		return responseOME;
	}
	public boolean getInfo(OMElement oms){
		System.out.println("接收的报文为："+oms.toString());
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement om = (OMElement)iterator.next();
				if(om.getLocalName().equals("DUEFEE")){
					Iterator child = om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("CONTNOS")){
							String contnos1 = child_Element.getText();
							contnos = contnos1.split(",");
						}
						if(child_Element.getLocalName().equals("STARTDATE")){
							startdate = child_Element.getText();
						}
						if(child_Element.getLocalName().equals("ENDDATE")){
							enddate = child_Element.getText();
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			info ="解析报文信息失败";
			return false;
		}
		return true;
	}
	public boolean pdRecord(){
		boolean b=true;
		for(int i=0;i<contnos.length;i++){
			LCContSchema lContSchema = new LCContSchema();
			
			//如果已经抽档，就不用再抽档
			String reSql = "select count(*) from LJSPay where otherno='"+contnos[i]+"' with ur";
			ExeSQL exeSQL = new ExeSQL();
			String count = exeSQL.getOneValue(reSql);
			if(!count.equals("0")){
				continue;
			}
			String sql = "select * from lccont where contNo='"+contnos[i]+"' with ur";
			System.out.println("EXesql:"+sql);
			LCContDB db = new LCContDB();
			lContSchema = db.executeQuery(sql).get(1);
			lContSchema.setContNo(contnos[i]);
			/*StartDate至EndDate之间生成抽档记录*/
			TransferData tempTransferData = new TransferData();
			//要修改日期
			tempTransferData.setNameAndValue("StartDate",dateAdd("D", startdate, -50));
		    tempTransferData.setNameAndValue("EndDate",dateAdd("D", enddate, 50));
		    
		    GlobalInput g = new GlobalInput();
	        g.Operator = lContSchema.getOperator();
	        g.ComCode = lContSchema.getManageCom();
	        g.ManageCom = lContSchema.getManageCom();
	        
		    VData tVData = new VData();
		    tVData.add(lContSchema);
		    tVData.add(tempTransferData);
		    tVData.add(g);
		    
		    PRnewDueFeeUI tPRnewDueFeeUI = new PRnewDueFeeUI();
		    tPRnewDueFeeUI.submitData(tVData,"INSERT");
		    tError = tPRnewDueFeeUI.mErrors;
		    info = "";
		    if(tError == null || !tError.needDealError())
		    {
			     
			     
		    }
		    else
		    {
			     String error="保单号为："+contnos[i]+"的保单生成抽档记录失败！原因："+tError.getErrContent();
			     System.out.println(error);
			     info = contnos[i]+" ";
			     b = false;
		    }  
		}
		if(!b){
			info = "保单号为："+info+"的保单生成抽档记录失败";
			return false;
		}else{
			info = "续保生成抽档记录成功";
			return true;
		}
	}
	public OMElement buildResponse(OMElement oms,String info){
		addOm(oms, "RsMsg", info);
		return oms;
	}
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
	public  String dateAdd(String compare, String sdate,int addNumber){
		Date date = null;
		SimpleDateFormat df = null;
		
		if(sdate.indexOf("-")>0){
			df = new SimpleDateFormat("yyyy-MM-dd");
		}else{
			df = new SimpleDateFormat("yyyy年MM月dd日");
		}
		try {
			date = df.parse(sdate);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Calendar cal = null;
		cal = Calendar.getInstance();
		cal.setTime(date);
		
		if(compare.equalsIgnoreCase("d")){
			cal.add(Calendar.DATE,addNumber);
		}
		if(compare.equalsIgnoreCase("M")){
			cal.add(Calendar.MONTH, addNumber);
		}
		if(compare.equalsIgnoreCase("Y")){
			cal.add(Calendar.YEAR, addNumber);
		}
		return df.format(cal.getTime());
	}
	public static void main(String[] args) {
		
		new ProduceDueFeeRd().pdRecord();
	}
}
