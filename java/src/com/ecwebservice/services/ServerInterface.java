package com.ecwebservice.services;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis.Constants;
import org.apache.axis2.context.MessageContext;


import com.ecwebservice.subinterface.BaseService;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ServerInterface {
	private Object dbObject = null;

	
	public OMElement queryData(OMElement element) throws Exception {
		System.out.println("进入ServerInterface.：");
		 //System.out.println("SOAP :" + mc.getEnvelope().getBody().toString());
		System.out.println("发送报文信息："+element.toString());
		OMElement oElement = null;

		String IP = BaseService.getClientIp();

		// 日志1--进入服务类
		LoginVerifyTool.writeEnterLog(IP,element);
		String transType = null;
		Iterator iter1 = element.getChildren();
		while (iter1.hasNext()) {
			OMElement Om = (OMElement) iter1.next();
			transType = Om.getLocalName();
			System.out.println("节点名称：" + transType);
		}

		String sql = "select TransDetail,transName from WFTransInfo where TransType='"
				+ transType + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sResult;
		sResult = tExeSQL.execSQL(sql);
		
		if (sResult.MaxRow > 0) {
			String TransDetail = sResult.GetText(1, 1);
			String transName = sResult.GetText(1, 2);
			// 日志2--调用某个交易的java类
			LoginVerifyTool.writeTransTypeLog(transName);

			Class TransDetailClass = Class.forName(TransDetail);
			// 0223 add接口实现方式--需要相应交易的java类实现CardHYX接口
			// CardHYX tCardHYX = (CardHYX) (TransDetailClass.newInstance());
			// OMElement oElement = tCardHYX.queryData(element);

			Class[] parameterC = {};
			Constructor constructor = TransDetailClass
					.getConstructor(parameterC);
			Object[] parameterO = {};
			dbObject = constructor.newInstance(parameterO);
			// 获取相应的方法
			Method getMethod = TransDetailClass.getMethod("queryData",
					new Class[] { OMElement.class });
			OMElement[] param = new OMElement[] { element };
			//调用交易类的方法
			oElement = (OMElement) getMethod.invoke(dbObject, param);
		}

		return oElement;

	}

	// 解析xml
	private OMElement DomParse(OMElement Oms) {
		// DocumentBuilderFactory domfac = DocumentBuilderFactory.newInstance();
		try {
			// DocumentBuilder dombuilder = domfac.newDocumentBuilder();
			// InputStream is = new FileInputStream(filePath);
			// Document doc = dombuilder.parse(is);
			// Element root = doc.getDocumentElement();
			System.out.println(Oms.getLocalName());
			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				// 单独解析财务实收实付确认文件
				if (Om.getLocalName().equals("Feeback")) {
					System.out.println("Feeback is OK。");
				} else if (Om.getLocalName().equals("Fee")) {
					// 测试上传应收应付文件用
					System.out.println("Fee is OK。");
				} else if (Om.getLocalName().equals("ContNo")) {
					// 测试上传应收应付文件用
					System.out.println("ContNo is OK。");
				} else if (Om.getLocalName().equals("Product")) {
					// 测试上传应收应付文件用
					System.out.println("Product is OK。");
				} else {
					System.out.println("没有找到节点。");
				}
			}
			System.out.println("查询数据进入解析");
		} catch (Exception e) {
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMNamespace ns = fac.createOMNamespace(
					"http://example.org/filedata", "fd");
			OMElement ele = fac.createOMElement("response", ns);
			ele.setText("false");
			return ele;
		}
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement ele = fac.createOMElement("response", ns);
		ele.setText("true");
		return ele;
	}

}
