package com.ecwebservice.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

//import com.sinosoft.common.Data;

public class ActivityQueryCont{
	private ExeSQL mexeSQL = new ExeSQL();
	private String errInfo;
	private boolean flag;
	private String aname;
	private String idtype;
	private String idno;
	private String birthday;
	private String sex;
	private String sendDate;
	private String sendTime;
	private String appntno;
/*	private int num_renew;
	private int num_single;
	private int numAll;*/
	
	private OMElement responseOME = null;
	public OMElement queryData(OMElement oms){
		flag = parseDate(oms);
		if(!flag){
			System.out.println(errInfo);
			responseOME = buildResponse(oms,errInfo);
			return responseOME;
		}
		flag = queryInfoByAccount(oms);
		if(!flag){
			System.out.println(errInfo);
			responseOME  = buildResponse(oms, errInfo);
			return responseOME;
		}else{
			responseOME  = queryInfoByLcc(oms, appntno);
		}
		System.out.println("返回报文信息："+responseOME);
		return responseOME;
		
	}
	
	//解析报文获报文中的信息
	public boolean parseDate(OMElement oms){
		System.out.println("接收到的报文为："+oms);
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement om = (OMElement)iterator.next();
				if(om.getLocalName().equals("INSUREAWARD")){//INSUREAWARD ceshi :RENEWALQUERY  
					Iterator child = om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("NAME")){
							aname = child_Element.getText();
							System.out.println("aname--="+aname);
						}
						if(child_Element.getLocalName().equals("IDTYPE")){
							idtype = child_Element.getText();
							System.out.println("idtype--="+idtype);
						}
						if(child_Element.getLocalName().equals("IDNO")){
							idno = child_Element.getText();
							System.out.println("idno--="+idno);
						}
						if(child_Element.getLocalName().equals("BIRTHDAY")){
							birthday = child_Element.getText();
							System.out.println("birthday--="+birthday);
						}
						if(child_Element.getLocalName().equals("SEX")){
							sex = child_Element.getText();
							System.out.println("sex--="+sex);
						}
						if(child_Element.getLocalName().equals("SENDDATE")){
							sendDate = child_Element.getText();
							System.out.println("sendDate--="+sendDate);
						}
						if(child_Element.getLocalName().equals("SENDTIME")){
							sendTime = child_Element.getText();
							System.out.println("sendTime--="+sendTime);
						}
						
					}
					
				}
				System.out.println("over!!!!");
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			errInfo = "报文解析异常，无法获得activity个人信息";
			return false;
		}
		return true;
	}
	
	//查询投保人的客户号
	public boolean queryInfoByAccount(OMElement oms){
//	public boolean queryInfoByAccount(String aname, String idtype, String idno , String birthday, String sex){//ceshi 
		
//		String appntno ="";
		try {
			//关联微信信息对应记录
			String appntnoSql = "select distinct lcc.APPNTNO from LCCONT lcc where APPNTNAME='"+aname+"'"
					         +"AND APPNTIDTYPE='"+idtype+"'"
					         +"AND APPNTIDNO='"+idno+"'"
					         +"AND APPNTBIRTHDAY='"+birthday+"'"
					         +"AND APPNTSEX='"+sex+"' with ur";
					         
			ExeSQL exeSQL = new ExeSQL();
			/*String s = exeSQL.getOneValue(appntnoSql);
			if(s.equals("0")){
				System.out.println("queryInfoByAccount未找到相关对应数据");
				return false;
			}*/
			System.out.println("appntnoSql=" + appntnoSql);
			SSRS result = mexeSQL.execSQL(appntnoSql);
			if(result==null || "".equals(result)){
				errInfo = "没有对应信息！";
				return false;
			}
			for (int i = 1; i <= result.MaxRow; i++) {//查询一条
				appntno = result.GetText(i, 1);
				System.out.println("--appntno---"+appntno);
			}

		} catch (Exception e) {
			System.out.println("queryInfoByAccount异常Exception");
			errInfo = "queryInfoByAccount异常Exception";
			return false;
		}
//		queryInfoByLcc(appntno);//ceshi
		return true;
	}
	public OMElement queryInfoByLcc(OMElement oms ,String appntno){
//		public OMElement queryInfoByLcc(String appntno){//ceshi
		OMElement responseOME = null;
		String appntNum = "";
		String num_renew = "";
		String num_single = "";
		String information = "";
		int numAll =0;
		
		String sqlQuery1 = "select a.appntno 投保人客户号, count(distinct a.contno) 保单数量续期续保  from lccont a,lpedoritem b,lpedorapp c,lcappnt d "
				+"where a.contno=b.contno and b.edorno=c.edoracceptno and a.contno=d.contno "
				+"and a.conttype='1' and a.appflag='1' and  a.stateflag='1' "
				+"and b.edortype in ('FX','TF') and c.edorstate='0' "
				+"and c.confdate between '2016-5-18' and '2016-7-18' "
				+"and a.appntno='"+appntno+"' "
				+"group by a.appntno with ur";

		String sqlQuery2 = "Select Appntno 投保人客户号,count(distinct contno) 保单数量标准个单  "
						+"From Lcpol "
						+"Where Riskcode In (Select Riskcode "
						+"From Lmriskapp "
						+"Where Riskcode = Lcpol.Riskcode "
						+"And (Riskperiod = 'L' Or Riskcode In ('122601', '122901'))) "
						+"And Cvalidate Between '2016-05-18' And '2016-07-18' "
						+"And Conttype = '1' "
						+"And Appflag = '1' "
						+"And Stateflag = '1' "
						+"and exists (select 1 from ReturnVisitTable where policyno=Lcpol.contno and returnvisitflag is not null) "
						+"And  Not exists (Select 1 "
						+"From Lbpol "
						+"Where Polno = Lcpol.Polno "
						+"And Edorno Like 'xb%') "
						+"and Appntno='"+appntno+"' "
						+"group by appntno with ur";
		
		System.out.println("sqlQuery1=" + sqlQuery1);
		System.out.println("sqlQuery2=" + sqlQuery2);
		SSRS result1 = mexeSQL.execSQL(sqlQuery1);
		SSRS result2 = mexeSQL.execSQL(sqlQuery2);
		if((result1==null || "".equals(result1))&&(result2==null || "".equals(result2))){
			information = "没有保单信息！！！";
			responseOME  = buildResponse(oms, information);
			return responseOME;
		}
		for (int i = 1; i <= result1.MaxRow; i++) {//查询一条
			appntNum = result1.GetText(i, 1);
			num_renew = result1.GetText(i, 2);

		}
		for (int i = 1; i <= result2.MaxRow; i++) {//查询一条
			appntNum = result2.GetText(i, 1);
			num_single = result2.GetText(i, 2);

		}
		if(num_renew ==null ||("").equals(num_renew)){
			num_renew ="0";
		}
		if(num_single ==null ||("").equals(num_single)){
			num_single ="0";
		}
		numAll = Integer.parseInt(num_renew)+Integer.parseInt(num_single);
		
		responseOME = buildResponse(oms,appntNum,num_renew,num_single,numAll);
		System.out.println("appntNum===:"+appntNum);
		System.out.println("num_renew===:"+num_renew);
		System.out.println("num_single===:"+num_single);
		System.out.println("numAll===:"+numAll);
		return responseOME;
		
	}
	
	
/*	public OMElement buildResponse(OMElement oms,String errInfo){
		
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement AWARDBACK = fac.createOMElement("RENEWALQUERY", null);//INSUREAWARD  AWARDBACK
//		String date = Data.getCurrentDate();
//		String time = Data.getCurrentTime();
		
//		addOm(AWARDBACK, "SENDDATE", date);
//		addOm(AWARDBACK, "SENDTIME", time);
		addOm(AWARDBACK, "ERROINFO", errInfo);
		
		return AWARDBACK;
		
	}*/
	//返回报文
		public OMElement buildResponse(OMElement oms,String info){
			System.out.println("nononononono!!!");
			addOm(oms, "RsMsg", info);
			return oms;
		}
	
	public OMElement buildResponse(OMElement oms,String appntNum,String num_renew,String num_single,int num){
		
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement AWARDBACK = fac.createOMElement("INSUREAWARD", null);////INSUREAWARD ceshi :RENEWALQUERY  
//		String date = Data.getCurrentDate();
//		String time = Data.getCurrentTime();
		System.out.println("lets go xml!!!");
		addOm(AWARDBACK, "APPNTNO", appntNum);
		addOm(AWARDBACK, "NUMRENEW", num_renew);
		addOm(AWARDBACK, "NUMSINGLE", num_single);
		addOm(AWARDBACK, "NUMALL", num+"");
//		addOm(AWARDBACK, "SENDDATE", date);
//		addOm(AWARDBACK, "SENDTIME", time);
		
		return AWARDBACK;
		
	}
	
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
	
	public static void main(String args[]){
		ActivityQueryCont actCont = new ActivityQueryCont();
//		actCont.queryInfoByAccount("测试","4","12312312","1975-5-5","0");

		
	}
}

