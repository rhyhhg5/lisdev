package com.ecwebservice.services;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.tb.ParseGuideIn;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CardSettlement {
	
	public CardSettlement(){
		
	}	
	/** 错误处理类 */
    private CErrors mErrors = new CErrors();
    
    private FDate mFDate = new FDate();
    
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    /** 传输数据的容器 */  
    private VData mVData = new VData();
    private TransferData mTransferData = new TransferData();   
    private MMap mMMap = new MMap();
    
    /** 团单印刷号（结算单号） */
    private String mPrtNo = null;
    private List prtNo = new ArrayList();

    /** 团单机构 */
    private String tManagecom = null;

    /** 团单产品类型（单证类型） */
    private String tCertifycode = null;

    /** 团单销售渠道 */
    private String tSalechnl = null;

    /** 销售业务员代码 */
    private String tAgentcode = null;

    /** 销售中介机构 */
    private String tAgentcom = null;
    
    /**第三方支付信息提交核心时的系统时间*/
    private String tTradeMakeDate = null;
    
    /**网销操作员*/
	private static final String operator ="SYS_ECWX";
	
	/***/
	private static final String managecom="86000000";
	
	/**
	 * 产生结算的团单无名单
	 * @return boolean
	 */
	public boolean settlement(){
		
		// 在全局变量mGlobalInput中加入所用值
		this.mGlobalInput.Operator=operator;
		this.mGlobalInput.ManageCom=managecom;
		
		try{			
			// 查询核心库SQL，是否为得到条件语句
			if(!settleListCondition()){
				buildError("settleListCondition", " 没有要结算的卡单。");
	            return false;
			}
			// 查询未结算保单信息
			String settleSQL="select distinct lic.prtno,lic.managecom,lic.certifycode,lic.salechnl," +
												"lic.agentcom,lic.agentcode,lic.PayTradeMakeDate " +
												"from licertify lic " +
													"inner join liCertSalesPayInfo licsp on lic.paytradebatchno = licsp.batchno " +
														"where lic.CertOperateType = '4' and lic.state in ('02','03') and lic.prtno is not null ";
			SSRS dSSRS = new SSRS();
			dSSRS = new ExeSQL().execSQL(settleSQL); 
			System.out.println("dSSRS.getMaxRow()===="+dSSRS.getMaxRow());
			if(dSSRS !=null && dSSRS.getMaxRow()>0){
				CertifyToGrpCont tCertifyToGrpCont = new CertifyToGrpCont();
				for(int i=1;i<=dSSRS.getMaxRow();i++){
					if(dSSRS.GetText(i, 1) !=null && !dSSRS.GetText(i, 1).equals("")){
						mPrtNo = dSSRS.GetText(i, 1);
						prtNo.add(mPrtNo);
					}else{
						buildError("settlement", "没有得到结算单号！");
					    return false;
					}
					String tGrpContNo = new ExeSQL()
                						.getOneValue("select GrpContNo from LCGrpCont where PrtNo = '"
                								+ mPrtNo + "' ");
					if(tGrpContNo == null ||tGrpContNo.equals("")){
						if(dSSRS.GetText(i, 2) !=null && !dSSRS.GetText(i, 2).equals("")){
							tManagecom = dSSRS.GetText(i, 2);//??????????????
							this.mGlobalInput.ManageCom=tManagecom;
						}else{
							buildError("settlement", "没有得到管理机构！");
						    return false;
						}
						if(dSSRS.GetText(i, 3) !=null && !dSSRS.GetText(i, 3).equals("")){
							tCertifycode = dSSRS.GetText(i, 3);
						}else{
							buildError("settlement", "没有得到单证编码！");
						    return false;
						}
						if(dSSRS.GetText(i, 4) !=null && !dSSRS.GetText(i, 4).equals("")){
							tSalechnl = dSSRS.GetText(i, 4);
						}else{
							buildError("settlement", "没有得到销售渠道！");
						    return false;
						}
						if(dSSRS.GetText(i, 5) !=null && !dSSRS.GetText(i, 5).equals("")){
							tAgentcom = dSSRS.GetText(i, 5);
							this.mGlobalInput.AgentCom = tAgentcom;
						}else{
							tAgentcom="";
						}
						if(dSSRS.GetText(i, 6) !=null && !dSSRS.GetText(i, 6).equals("")){
							tAgentcode = dSSRS.GetText(i, 6);
						}else{
							buildError("settlement", "没有得到业务人员代码！");
						    return false;
						}
						if(dSSRS.GetText(i, 7) !=null && !dSSRS.GetText(i, 7).equals("")){
							this.tTradeMakeDate = dSSRS.GetText(i, 7);
						}else{
							buildError("settlement", "第三方支付信息提交核心时的系统时间为空");
					        return false;
						}
						// 在mTransferData中加入所用值
						System.out.println("mPrtNo==========="+mPrtNo);
						mTransferData.setNameAndValue("PrtNo",mPrtNo);
						mTransferData.setNameAndValue("ManageCom",tManagecom);
						mTransferData.setNameAndValue("AgentCode",tAgentcode);
						mTransferData.setNameAndValue("CertifyCode",tCertifycode);
						mTransferData.setNameAndValue("SaleChnl",tSalechnl);
						mTransferData.setNameAndValue("AgentCom",tAgentcom);
						mTransferData.setNameAndValue("AppntName","网销");
						mTransferData.setNameAndValue("PayMode","1");//暂定为1
						mTransferData.setNameAndValue("PayIntv","0");
						mTransferData.setNameAndValue("CValidate",tTradeMakeDate);//??
						mTransferData.setNameAndValue("CInValidate",PubFun.calDate(tTradeMakeDate, 1, "Y", null));//??
						mTransferData.setNameAndValue("Flag","");//共保保单不处理
						
						VData cInputData = new VData();
						cInputData.add(mGlobalInput);
						cInputData.add(mTransferData);
						System.out.println("*******************生成团体无名单*******************");
						if(!tCertifyToGrpCont.submitData(cInputData, "Create")){
							buildError("settlement", "生成团体无名单失败！");
							return false;
						}
					}
				// 计算团体无名单应收费用
				System.out.println("*******************计算团体无名单应收费用*******************");
				tGrpContNo = new ExeSQL()
                	.getOneValue("select GrpContNo from LCGrpCont where PrtNo = '"
                        + mPrtNo + "' ");
				boolean action = true;
				String actionSQL = "select state from lcinsuredlist where grpcontno='"+tGrpContNo+"'";
				SSRS pSSRS = new SSRS();
				pSSRS= new ExeSQL().execSQL(actionSQL);
				if(pSSRS!=null && pSSRS.getMaxRow()>0){
					for(int j=1;j<=pSSRS.getMaxRow();j++){
						if(pSSRS.GetText(j, 1).equals("1")){
							action=false;
							break;
						}
					}
				}
				if(action){
					VData tVData = new VData();
					TransferData tTransferData = new TransferData();
					tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
					
					tVData.add(tTransferData);
					tVData.add(this.mGlobalInput);
					
					ParseGuideIn tPGI = new ParseGuideIn();
					
					if (!tPGI.submitData(tVData, "INSERT||DATABASE"))
				    {
				        String tStrErr = " 产生无名单分单信息失败! 原因是: "
				                + tPGI.mErrors.getLastError();
				        buildError("submitData", tStrErr);
				        return false;
				    }
				}
				
				// 校验保费
				System.out.println("*******************校验保费*******************");
				if(!checkPrem(this.mPrtNo)){
					System.out.println("*******************结算单号"+mPrtNo+"，校验保费失败*******************");
					buildError("checkPrem", "结算单号"+mPrtNo+"，校验保费失败");
			        return false;
				}

				// 确认暂收
				System.out.println("*******************确认暂收*******************");
				String confirmSQL = "select 1 from ljtempfee ljt inner join ljtempfeeclass ljtc on ljt.tempfeeno = ljtc.tempfeeno "
										+" where ljt.paydate is not null and ljtc.paydate is not null and ljt.enteraccdate = ljt.paydate and ljt.confmakedate = ljt.paydate "
											+" and ljt.confflag='0' and ljtc.enteraccdate = ljtc.paydate and ljtc.confmakedate = ljtc.paydate "
												+" and ljtc.confflag='0' and ljt.otherno='"+mPrtNo+"' ";
				String conflag = new ExeSQL().getOneValue(confirmSQL);
				if(!conflag.equals("1")){
					if(!confirmTempfee(this.mPrtNo)){
						buildError("checkPrem", "结算单号"+mPrtNo+"，确认暂收失败");
				        return false;
					}
				}else{
					System.out.println("**************"+mPrtNo+"已确认暂收！***************");
				}
				
			
			    MMap map = new MMap();

			    map.put(
			            "update lccont set UWFlag='9',ApproveFlag='9' where grpcontno='"
			                    + tGrpContNo + "'", SysConst.UPDATE);
			    map.put(
			            "update lcpol set UWFlag='9', ApproveFlag='9' where grpcontno='"
			                    + tGrpContNo + "'", SysConst.UPDATE);
			    map.put(
			            "update lcgrpcont set UWFlag='9',ApproveFlag='9' where grpcontno='"
			                    + tGrpContNo + "'", SysConst.UPDATE);
			    map.put(
			            "update lcgrppol set UWFlag='9',ApproveFlag='9' where grpcontno='"
			                    + tGrpContNo + "'", SysConst.UPDATE);
			
			    VData data = new VData();
			    data.add(map);
			
			    PubSubmit p = new PubSubmit();
			    if (!p.submitData(data, ""))
			    {
			        System.out.println("提交数据失败");
			        buildError("submitData", "提交数据失败");
			        return false;
			    }
				
			    // 签单
			    System.out.println("*******************确认签单*******************");
			    tGrpContNo = new ExeSQL()
            	.getOneValue("select GrpContNo from LCGrpCont where PrtNo = '"
                    + mPrtNo + "' ");
			    if(!confirmGrpContSign(tGrpContNo)){
			    	buildError("confirmGrpContSign", "没有成功签单！");
					return false;
			    }
			    
			
				}
			}else{
				buildError("settlement", "没有要生成团单的数据");
				return false;		
			}	
			}catch(Exception e){
				e.printStackTrace();
				return false;
			}    
		
			return true;
		}
	
	/**
	 * 查出结算单数量并生成相应的结算单号
	 * @return boolean
	 */
	private boolean settleListCondition(){
		//salechnl, certifycode, agentcom, agentcode, managecom
		String conSQL="select distinct lic.managecom,lic.certifycode,lic.salechnl,lic.agentcom,lic.agentcode "
			  			+"from licertify lic "
			  				+"inner join liCertSalesPayInfo licsp on lic.PayTradeBatchNo = licsp.BatchNo "
			  					+"where lic.prtno is null "
									   +"and lic.state = '01' "
									   +"and lic.PayTradeStatus = '00' "
									   +"and lic.PayTradeMakeDate = current date - 1 day ";

		try{
			SSRS dSSRS = new SSRS();
			dSSRS = new ExeSQL().execSQL(conSQL);
			System.out.println("dSSRS.getMaxRow()===="+dSSRS.getMaxRow());
			if(dSSRS !=null && dSSRS.getMaxRow()>0){
				
				for(int i=1;i<=dSSRS.getMaxRow();i++){
					if(dSSRS.GetText(i, 1) !=null && !dSSRS.GetText(i, 1).equals("")){
						tManagecom = dSSRS.GetText(i, 1);
					}else{
						System.out.println("没有得到管理机构");
						buildError("settleListCondition", "没有得到管理机构！");
					    return false;
					}
					if(dSSRS.GetText(i, 2) !=null && !dSSRS.GetText(i, 2).equals("")){
						tCertifycode = dSSRS.GetText(i, 2);
					}else{
						System.out.println("没有得到单证编码");
						buildError("settleListCondition", "没有得到单证编码！");
					    return false;
					}
					if(dSSRS.GetText(i, 3) !=null && !dSSRS.GetText(i, 3).equals("")){
						tSalechnl = dSSRS.GetText(i, 3);
					}else{
						System.out.println("没有得到销售渠道");
						buildError("settleListCondition", "没有得到销售渠道！");
					    return false;
					}
					if(dSSRS.GetText(i, 4) !=null && !dSSRS.GetText(i, 4).equals("")){
						tAgentcom = dSSRS.GetText(i, 4);
					}
					if(dSSRS.GetText(i, 5) !=null && !dSSRS.GetText(i, 5).equals("")){
						tAgentcode = dSSRS.GetText(i, 5);
					}else{
						System.out.println("没有得到业务人员代码");
						buildError("settlement", "没有得到业务人员代码！");
					    return false;
					}
					// 生成结算单号
					mPrtNo = PubFun1.CreateMaxNo("LICPRTNO", null);
					if(mPrtNo==null){
						buildError("settleListCondition", "生成结算单号失败！");
					    return false;
					}
					String updateSQL="update licertify lic set lic.prtno='"+mPrtNo+"',lic.state='02' " +
							"where lic.managecom='"+tManagecom+"' and lic.certifycode='"+tCertifycode+"' "
							   +" and salechnl='"+this.tSalechnl+"' "
							   +"and lic.prtno is null "
							   +"and lic.state = '01' "
							   +"and lic.PayTradeStatus = '00' "
							   +"and lic.PayTradeMakeDate = current date - 1 day "
							   +"and lic.agentcode='"+this.tAgentcode+"'";
					if(tAgentcom!=null && !tAgentcom.equals("")){
						updateSQL+=" and lic.agentcom ='"+this.tAgentcom+"' ";
					}
					updateSQL+=" and exists(select 1 from liCertSalesPayInfo licsp where lic.PayTradeBatchNo = licsp.BatchNo) ";
					mMMap.put(updateSQL, SysConst.UPDATE);	
				}
				VData vVData = new VData();
				vVData.add(mMMap);
				PubSubmit p = new PubSubmit();
		        if (!p.submitData(vVData, ""))
		        {
		            System.out.println("提交数据失败");
		            buildError("settleListCondition", "插入结算单号和回置卡状态失败");
		            return false;
		        }
			}	
		}catch(Exception e){
			System.out.println(">>>>>>>>>>>>查询结算单数量时报错");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验计算后的保费是否全部一致
	 * @param prtno
	 * @return
	 */
	private boolean checkPrem(String prtno){
		try{
			String tGrpContNo="";
			List premList = new ArrayList();
			if(prtno !=null && !prtno.equals("")){
				tGrpContNo = new ExeSQL()
						.getOneValue("select GrpContNo from LCGrpCont where PrtNo = '"+ prtno + "' ");
				if(tGrpContNo==null || tGrpContNo.equals("")){
					buildError("checkPrem", "没有得到团单代码");
					return false;
				}
			}else{
				buildError("checkPrem", "没有得到结算单代码");
				return false;
			}
			//lcgrpcont
			String grpContPrem = new ExeSQL().
					getOneValue("select prem from lcgrpcont where grpcontno = '"+tGrpContNo+"'");
			System.out.println("lcgrpcont的保费计算为=="+grpContPrem);
			if(grpContPrem==null || grpContPrem.equals("")){
				buildError("checkPrem", "grpContPrem出错");
				return false;
			}
			premList.add(grpContPrem);
			//lcgrppol
			String grpPolPrem = new ExeSQL().
					getOneValue("select sum(prem) from lcgrppol where grpcontno = '"+tGrpContNo+"'");
			System.out.println("lcgrpcont的保费计算为=="+grpContPrem);
			if(grpPolPrem==null || grpPolPrem.equals("")){
				buildError("checkPrem", "grpPolPrem出错");
				return false;
			}
			premList.add(grpPolPrem);
			//lccont
			String contPrem = new ExeSQL().
					getOneValue("select sum(prem) from lccont where grpcontno = '"+tGrpContNo+"'");
			System.out.println("lccont的保费计算为=="+contPrem);
			if(contPrem==null || contPrem.equals("")){
				buildError("checkPrem", "contPrem出错");
				return false;
			}
			premList.add(contPrem);
			//lcpol
			String polPrem = new ExeSQL().
					getOneValue("select sum(prem) from lcpol where grpcontno = '"+tGrpContNo+"'");
			System.out.println("lcpol的保费计算为=="+polPrem);
			if(polPrem==null || polPrem.equals("")){
				buildError("checkPrem", "polPrem出错");
				return false;
			}
			premList.add(polPrem);
			//lcduty
			String dutyPrem = new ExeSQL().
					getOneValue("select sum(prem) from lcduty where polno in (select polno from lcpol where grpcontno = '"+tGrpContNo+"')");
			System.out.println("lcduty的保费计算为=="+dutyPrem);
			if(dutyPrem==null || dutyPrem.equals("")){
				buildError("checkPrem", "dutyPrem出错");
				return false;
			}
			premList.add(dutyPrem);
			//lcprem
			String premPrem = new ExeSQL().
					getOneValue("select sum(prem) from lcprem where polno in (select polno from lcpol where grpcontno = '"+tGrpContNo+"')");
			System.out.println("lcprem的保费计算为=="+premPrem);
			if(premPrem==null || premPrem.equals("")){
				buildError("checkPrem", "premPrem出错");
				return false;
			}
			premList.add(premPrem);
			//ljtempfee
			String tempfeePrem = new ExeSQL().
					getOneValue("select sum(paymoney) from ljtempfee where otherno in (select prtno from lcgrpcont where grpcontno = '"+tGrpContNo+"')");
			System.out.println("ljtempfee的保费计算为=="+tempfeePrem);
			if(tempfeePrem==null || tempfeePrem.equals("")){
				buildError("checkPrem", "tempfeePrem出错");
				return false;
			}
			premList.add(tempfeePrem);
			//ljtempfeeclass
			String tempfeeclassPrem = new ExeSQL().
					getOneValue("select sum(paymoney) from ljtempfeeclass where prtno in (select prtno from lcgrpcont where grpcontno = '"+tGrpContNo+"')");
			System.out.println("ljtempfeeclass的保费计算为=="+tempfeeclassPrem);
			if(tempfeeclassPrem==null || tempfeeclassPrem.equals("")){
				buildError("checkPrem", "tempfeeclassPrem出错");
				return false;
			}
			premList.add(tempfeeclassPrem);
			//licertify
			String licPrem = new ExeSQL().
					getOneValue("select sum(prem) from licertify where grpcontno = '"+tGrpContNo+"'");
			System.out.println("licertify的保费计算为=="+licPrem);
			if(licPrem==null || licPrem.equals("")){
				buildError("checkPrem", "licPrem出错");
				return false;
			}
			premList.add(licPrem);
			//比较保费是否一致
			for(int i=1;i<premList.size();i++){
				if(!premList.get(0).equals(premList.get(i))){
					buildError("checkPrem", "校验保费不一致");
					return false;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 确认暂收保费
	 * @param prtno
	 * @return
	 */
	private boolean confirmTempfee(String prtno){
		try{
			if(prtno!=null && !prtno.equals("")){
				String payDateSQL= "select distinct paydate from LJTempFee " +
									"where confflag is null and paydate is not null and otherno='"+prtno+"'";
				String payDate = new ExeSQL().getOneValue(payDateSQL);
				
				String updateTempfee= "update LJTempFee set enteraccdate='"+payDate+"',confmakedate='"+payDate+"',confflag='0' " +
						"where confflag is null and paydate is not null and otherno='"+prtno+"'"; 
				System.out.println("updateTempfeeSQL==="+updateTempfee);
				String updateTempfeeClass= "update LJTempFeeclass set enteraccdate='"+payDate+"',confmakedate='"+payDate+"',confflag='0' " +
						"where confflag is null and paydate is not null and prtno='"+prtno+"'";
				
				System.out.println("updateTempfeeClassSQL==="+updateTempfeeClass);
				MMap tMMap = new MMap();
				tMMap.put(updateTempfee, SysConst.UPDATE);
				tMMap.put(updateTempfeeClass, SysConst.UPDATE);
				VData dVData = new VData();
				dVData.add(tMMap);
				PubSubmit p = new PubSubmit();
		        if (!p.submitData(dVData, ""))
		        {
		            System.out.println("提交数据失败");
		            buildError("submitData", "提交数据失败");
		            return false;
		        }
				
			}else{
				 buildError("confirmTempfee", "没有得到确认暂收保费的条件prtno");
		         return false;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 确认签单
	 * @param tGrpContNo
	 * @return
	 */
	private boolean confirmGrpContSign(String tGrpContNo){		
		ConfirmGrpContsignBL mConfirmGrpContsignBL= new ConfirmGrpContsignBL();
		if(!mConfirmGrpContsignBL.submit(tGrpContNo, mGlobalInput)){
			buildError("confirmGrpContSign", "确认签单时失败！");
	         return false;
		}
		return true;
	}

	/**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CardNoInfoToLicertiry";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    
    public static void main (String args[]){
    	CardSettlement cs = new CardSettlement();
    	cs.settlement();
    }
}
