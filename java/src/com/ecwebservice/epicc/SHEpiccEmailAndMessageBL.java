package com.ecwebservice.epicc;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.ecwebservice.lvmama.LVEmailAndMessageBL;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCCSpecSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class SHEpiccEmailAndMessageBL {
	
	private static String host = "10.252.8.100";//发送方邮箱所在的smtp主机
	private static String sender = "eshop@picchealth.com";//发送方邮箱 
	private static String password = "pass@word1";//密码
    private String mCurrentDate = PubFun.getCurrentDate();
    private ExeSQL mExeSQL = new ExeSQL();
    private static HashMap mAndeMap = new HashMap();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public boolean sendSmsAndEmail(String BatchNo ,String prtno){
		if("".equals(prtno)||prtno==null){
    		System.out.println("获得印刷号失败!");
    	}
		ExeSQL tExeSql = new ExeSQL();
		String tStrSql2 =   "select mobile,email from lcaddress where customerno=" +
		 		"(select appntno from lccont where prtno='"+prtno+"') " +
		 		"and addressno=(select max( addressno) from lcaddress " +
		 		"where customerno=(select appntno from lccont where prtno='"+prtno+"') ) ";
		SSRS checkSSRS2 = tExeSql.execSQL(tStrSql2);
		if(checkSSRS2 == null || checkSSRS2.MaxRow==0){
	        	System.out.println("获取发送短信和邮件信息失败!");
	        	return false;
	    }
		String tcertifyName = "";
		String tmobile = checkSSRS2.GetText(1, 1);
		String tproposerEmail = checkSSRS2.GetText(1, 2);
		
        String tStrSql = "select contno,'未成年人重大疾病保障计划',cvalidate,cinvalidate,appntname from lccont where appflag='1' and prtno = '"+prtno+"' ";
        SSRS checkSSRS = tExeSql.execSQL(tStrSql);
        if(checkSSRS.MaxRow==0){
        	System.out.println("获取发送短信和邮件信息失败!");
        	return false;
        }
        int length = checkSSRS.MaxRow;
        for(int i=0;i<length;i++){
        	String tcontNo = "";
        	String tCValiDate = "";
        	String tCInValiDate = "";
        	tcontNo = checkSSRS.GetText(1, 1);
        	String tWrapName = checkSSRS.GetText(1, 2);
        	tCValiDate = checkSSRS.GetText(i+1, 3);
        	tCInValiDate = checkSSRS.GetText(1, 4);
        	tcertifyName = checkSSRS.GetText(1, 5);
        	System.out.println("tcardNo="+tcontNo);
        	System.out.println("tCValiDate="+tCValiDate);
        	System.out.println("tCInValiDate="+tCInValiDate);
        	mAndeMap.put("cardNo"+i, tcontNo);
			if(i==length-1){
				mAndeMap.put("endindex", ""+i);
			}
			mAndeMap.put("WrapName"+i, tWrapName);
			mAndeMap.put("CValiDate"+i, tCValiDate);
			mAndeMap.put("CInValiDate"+i, tCInValiDate);
        }
        mAndeMap.put("certifyName", tcertifyName);
        mAndeMap.put("mobile", tmobile);
        mAndeMap.put("orderID", BatchNo);
        mAndeMap.put("proposerEmail",tproposerEmail);
        mAndeMap.put("ProposerNeedSMS", "T");
        mAndeMap.put("ProposerName", tcertifyName);
        // 校验批次号唯一
        //计算满期日期
//        try {
//			tCInValiDate =  PubFun.calDate(df.parse(tCValiDate), Integer.parseInt(tinsuYear), tinsuYearFlag, df.parse(tCValiDate));
//		}catch (ParseException e) {
//			e.printStackTrace();
//		}
//        tCInValiDate = PubFun.calDate(tCInValiDate,-1,"D",tCInValiDate);
        
        //日期转换成string格式处理
		boolean messageFlag = true;
		boolean emailFlag = true;
		String errinfo = "";
		if(mAndeMap.get("ProposerNeedSMS").equals("T")){
			System.out.println(((String)mAndeMap.get("mobile")).length()+"----------mobile");
			//短息通知
			if(mAndeMap.get("mobile")!=null && !("").equals(mAndeMap.get("mobile"))){
				if(new SHEpiccEmailAndMessageBL().sendMsg(mAndeMap)){
					System.out.println("手机号为"+mAndeMap.get("mobile")+"的短息发送成功");
				}else{
					messageFlag = false;
					//errinfo = errinfo+"投保异常，短息发送失败  ";
					System.out.println("手机号为"+mAndeMap.get("mobile")+"的短息发送失败");
				}
			}
			System.out.println(((String)mAndeMap.get("proposerEmail")).length()+"----------Email");
			//邮件通知
			if(mAndeMap.get("proposerEmail")!=null && !("").equals(mAndeMap.get("proposerEmail"))){
				if(SHEpiccEmailAndMessageBL.sendEmail(mAndeMap)){
					System.out.println("邮箱号为"+mAndeMap.get("proposerEmail")+"的邮件发送成功");
				}else{
					//errinfo = errinfo+"投保异常，邮件发送失败";
					System.out.println("邮箱号为"+mAndeMap.get("proposerEmail")+"的邮件发送失败");
					emailFlag = false;
				}
				if(messageFlag&emailFlag){
					return true;
				}else{
					return false;
				}
			}
			
		}
		return true;
	}
    public boolean sendMsg(Map Msgmap) {
        System.out.println("短信通知批处理开始......。。。。");
        SmsServiceSoapBindingStub binding = null;
        try {
            binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().
                      getSmsService(); //创建binding对象
        } catch (javax.xml.rpc.ServiceException jre) {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        
        Vector vec = new Vector();
        vec = getMessage(Msgmap);
        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec="+vec.size());

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {
        		tempVec.clear();
            	tempVec.add(vec.get(i));
            	
            	SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
                msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                msgs.setServiceType("xuqi"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                
            	msgs.setMessages((SmsMessage[]) tempVec.toArray(new SmsMessage[tempVec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
                try 
                {
                    value = binding.sendSMS("Admin", "Admin", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                    System.out.println(value.getStatus());
                    System.out.println(value.getMessage());
                } 
                catch (RemoteException ex) 
                {
                    ex.printStackTrace();
                }
            }
        } 
        else 
        {
            System.out.print("无符合条件的短信！");
        }
        System.out.println("客户短信通知批处理正常结束......");
        return true;
    }
    /**
	 * 发送普通邮件
	 * @param receiver	收件人
	 * @param subject	主题
	 * @param contents	内容
	 * @return	true-成功 false-失败
	 */
	public static boolean sendEmail(Map Msgmap){
		String receiver = (String)Msgmap.get("proposerEmail");
		String subject = "人保健康客户通知";
	
		try{
			String index = (String)Msgmap.get("endindex");
	        int endindex = Integer.parseInt(index);
	        for(int i=0;i<=endindex;i++){
	        	String cardNo = (String)Msgmap.get("cardNo"+i);
	        	String appntName = (String)Msgmap.get("ProposerName");
	        	String wrapName = (String)Msgmap.get("WrapName"+i);
	        	String cvaliDate = (String)Msgmap.get("CValiDate"+i);
	        	String cinValiDate = (String)Msgmap.get("CInValiDate"+i);
	        	String nowDate = PubFun.getCurrentDate();
	        	if(cardNo == null || ("").equals(cardNo)){
	        		System.out.println("发送邮件时卡号为空，此条短信发送失败");
	        		continue;
	        	}
	        	//短信内容
	        	String tMSGContents=""; 
//	        	tMSGContents = "尊敬的"+appntName+"女士/先生：您的携程飞翔无忧航空意外险已生效，保单号"+cardNo+"，保险期间为自登机舱门到离机舱门之间，详询95591或登陆www.picchealth.com查询。";
	        	tMSGContents = "尊敬的客户，您好！您已成功投保"+wrapName+"，保障期限自"+cvaliDate+"零时起 至"+cinValiDate+"二十四时止（北京时间），感谢您的支持，祝您健康。详情请致电95591（"+nowDate+"）。";

				String typeName = "中国人民健康保险股份有限公司";//发送方名称
				String name = sender.substring(0, sender.indexOf('@')); 
				Properties props = new Properties();
				//Setup mail server
				props.put("mail.smtp.host", host);//设置smtp主机
				props.put("mail.smtp.auth", "true");//使用smtp身份验证
				//Get session
				Session session = Session.getDefaultInstance(props, null);
				//Define message
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(sender, typeName));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));	
				message.setSubject(subject);			
				message.setContent(tMSGContents, "text/html;charset=GBK");
				message.saveChanges();
				//Send message
				Transport transport = session.getTransport("smtp");	
				System.out.println("******正在连接" + host);
				transport.connect(host, name, password);
				System.out.println("******正在发送给" + receiver);
				transport.sendMessage(message, message.getAllRecipients());
				System.out.println("******邮件发送成功");
	        }
			return true;
		}catch(Exception e){
			System.out.println("发送普通邮件（" + receiver + "）异常"+e);
			return false;
		}	
	}
    private Vector getMessage(Map Msgmap) {
    	System.out.println("进入获取信息 getMessage()------------------------");
        Vector tVector = new Vector();
        
    
        //携程意险短信通知（产品（套餐）名称,卡号（合同号），生效日期,手机号,订单号（流水号））
        String index = (String)Msgmap.get("endindex");
        
        int endindex = Integer.parseInt(index);
        for(int i=0;i<=endindex;i++){
        	String cardNo = (String)Msgmap.get("cardNo"+i);
        	String mobile = (String)Msgmap.get("mobile");
        	String orderID = (String)Msgmap.get("orderID");
        	String appntName = (String)Msgmap.get("ProposerName");
        	String wrapName = (String)Msgmap.get("WrapName"+i);
        	String cvaliDate = (String)Msgmap.get("CValiDate"+i);
        	String cinValiDate = (String)Msgmap.get("CInValiDate"+i);
        	String nowDate = PubFun.getCurrentDate();
        	if(orderID == null || ("").equals(orderID)){
        		System.out.println("发送短信时订单号为空，此条短信发送失败");
        	}
        	if(cardNo == null || ("").equals(cardNo)){
        		System.out.println("发送短信时卡号为空，此条短信发送失败");
        		continue;
        	}
        	if(mobile == null || ("").equals(mobile)){
        		System.out.println("发送短信时电话号码为空，此条短信发送失败");
        		continue;
        	}
        	//短信内容
        	String tMSGContents=""; 
        	
        	SmsMessage tKXMsg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
        	
//        	tMSGContents = "尊敬的"+appntName+"女士/先生：您的携程飞翔无忧航空意外险已生效，保单号"+cardNo+"，保险期间为自登机舱门到离机舱门之间，详询95591或登陆www.picchealth.com查询。";
        	tMSGContents = "尊敬的客户，您好！您已成功投保"+wrapName+"，保障期限自"+cvaliDate+"零时起 至"+cinValiDate+"二十四时止（北京时间），感谢您的支持，祝您健康。详情请致电95591（"+nowDate+"）。";
        	System.out.println(tMSGContents);
        	tKXMsg.setReceiver(mobile);
        	tKXMsg.setContents(tMSGContents);
        	tVector.add(tKXMsg);
//        	 在LCCSpec表中插入值  ********************************************************  
        	LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
            tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);       //此字段非空
            tLCCSpecSchema.setContNo(cardNo);                 //    暂定为卡号(此表中该字段为联合主键)
            tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);   //此字段非空   
            tLCCSpecSchema.setSerialNo(PubFun.getCurrentDate()+PubFun.getCurrentTime()+String.valueOf(i));//暂定为订单号（此表中该字段为联合主键）
//            tLCCSpecSchema.setPrtSeq(orderID);                
//            String tLimit = PubFun.getNoLimit(tManageCom);
//	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
//            tLCCSpecSchema.setEndorsementNo(tGetNoticeNo);
            tLCCSpecSchema.setOperator("001");   //暂定为001
            tLCCSpecSchema.setSpecContent(tMSGContents+"客户手机号："+ mobile);
            tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
            System.out.println("保存数据");
            tLCCSpecSchema.getDB().insert();
        }
       
        System.out.println("都结束了----------------------------------------------------------------");
        return tVector;
    }
    public static void main(String[] args) {
//        GlobalInput mGlobalInput = new GlobalInput();
//        VData mVData = new VData();
//        mGlobalInput.Operator = "001";
//        mGlobalInput.ManageCom = "86";
//        mVData.add(mGlobalInput);
    	SHEpiccEmailAndMessageBL a = new SHEpiccEmailAndMessageBL();
    	a.sendSmsAndEmail("2142423","15000002707");
    }
}
