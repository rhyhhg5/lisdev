package com.ecwebservice.app4pad;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;

/**
 * Title:移动出单代理人修改登录密码
 * @author zhangyige
 * @version
 */

public class PadModify {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String agentcode = null;// 代理人编码

	private String password = null;// 密码

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("PADMODIFY")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}

						if (child_element.getLocalName().equals("AGENTCODE")) {
							agentcode = child_element.getText();

							System.out.println(agentcode);
						}
						if (child_element.getLocalName().equals("PASSWORD")) {
							password = child_element.getText();

							System.out.println(password);
						}
					}		

					if (agentcode == null || "".equals(agentcode)) {
						responseOME = buildResponseOME("01", "1",
								"代理人编码为空值");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					if (password == null || "".equals(password)) {
						responseOME = buildResponseOME("01", "2",
								"密码为空值");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					//将密码进行加密处理
					 LisIDEA tLisIdea = new LisIDEA();
					 password =  tLisIdea.encryptString(password);
					 LAAgentSchema tLAAgentSchema = new LAAgentSchema();
					 LAAgentDB tLAAgentDB = new LAAgentDB();
					 tLAAgentDB.setAgentCode(agentcode);
					 if(tLAAgentDB.getInfo())
					 {
						 tLAAgentSchema = tLAAgentDB.getSchema();
					 }else{
						 responseOME = buildResponseOME("01",
									"N", "不能查找到该业务员");
					 }
					 tLAAgentSchema.setPassword(password);
					 tLAAgentSchema.setModifyDate(PubFun.getCurrentDate());
					 tLAAgentSchema.setModifyTime(PubFun.getCurrentTime());
					 tLAAgentDB.setSchema(tLAAgentSchema);
					 boolean flag = tLAAgentDB.update();
					 if(flag){
						 responseOME = buildResponseOME("00",
							"Y", "成功");
					 }else{
						 responseOME = buildResponseOME("01",
									"N", "修改密码失败");
					 }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME( "01", "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	private OMElement buildResponseOME(String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement DATASET = fac.createOMElement("PADMODIFY", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", messageType);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
