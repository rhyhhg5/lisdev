package com.ecwebservice.app4pad;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.utility.ExeSQL;

/**
 * Title:移动出单代理人登陆校验
 * 校验代理人是否存在、是否离职
 * @author zhangyige
 * @version
 */

public class LoginVerify {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String agentcode = null;// 代理人编码

	private String password = null;// 密码

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("PADLOGIN")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}

						if (child_element.getLocalName().equals("AGENTCODE")) {
							agentcode = child_element.getText();
							AgentCodeTransformation a = new AgentCodeTransformation();
							if(!a.AgentCode(agentcode, "N")){
								System.out.println(a.getMessage());
							}
							System.out.println(agentcode);
							agentcode = a.getResult();
						}
						if (child_element.getLocalName().equals("PASSWORD")) {
							password = child_element.getText();

							System.out.println(password);
						}
					}		

					if (agentcode == null || "".equals(agentcode)) {
						responseOME = buildResponseOME("01", "1",
								"代理人编码为空值");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					if (password == null || "".equals(password)) {
						responseOME = buildResponseOME("01", "2",
								"密码为空值");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					//将密码进行加密处理
					 LisIDEA tLisIdea = new LisIDEA();
					 password =  tLisIdea.encryptString(password);
					
					//判断业务员是否存在
					String exSql = "select count(*) from laagent where agentcode='"+agentcode+"'";
					ExeSQL tExeSQL = new ExeSQL();
					int exCount = Integer
							.parseInt(tExeSQL.getOneValue(exSql));
					if (exCount == 0) {
						responseOME = buildResponseOME("01", "NE",
								"业务员不存在");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					//判断业务员是否离职
					String outSql = "select count(*) from laagent where agentcode='"+agentcode+"' and agentstate <> '06'";
					int outCount = Integer
					.parseInt(tExeSQL.getOneValue(outSql));
					if (outCount == 0) {
						responseOME = buildResponseOME("01", "YF",
						"业务员已经离职");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					//判断登录密码是否有误
					String loginSql = "select managecom from laagent where agentcode='"+agentcode+"' and password ='"+password+"'";
					String manageCom = tExeSQL.getOneValue(loginSql);
					if (manageCom == null||manageCom.equals("")) {
						responseOME = buildResponseOME("01", "YW",
						"该业务人员存在且尚未离职，但密码输入有误");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					
					responseOME = buildResponseOME("00",
							"Y@"+manageCom, "成功");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME( "01", "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	private OMElement buildResponseOME(String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement DATASET = fac.createOMElement("PADLOGIN", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", messageType);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
