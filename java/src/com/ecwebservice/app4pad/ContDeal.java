package com.ecwebservice.app4pad;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.tb.ESPolicyPicsBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * Title:移动出单与核心银保万能影像件处理对接
 * @author zhangyige
 * @version
 */

public class ContDeal {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String prtno = null;// 印刷号

	private String managecom = null;// 管理机构
	
	private String path = getUploadPath();//图片的存储位置

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("CONTDEAL")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}

						if (child_element.getLocalName().equals("PRTNO")) {
							prtno = child_element.getText();

							System.out.println(prtno);
						}
						if (child_element.getLocalName().equals("MANAGECOM")) {
							managecom = child_element.getText();

							System.out.println(managecom);
						}
					}		

					if (prtno == null || "".equals(prtno)) {
						responseOME = buildResponseOME("01", "1",
								"印刷号为空值");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					if (managecom == null || "".equals(managecom)) {
						responseOME = buildResponseOME("01", "2",
								"管理机构为空值");
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					String result = submitData(prtno,managecom);
					if(result.equals("success")){
						responseOME = buildResponseOME("00",
								"0", "成功");
					}else{
						responseOME = buildResponseOME("01",
								"1", result);
					}
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME( "01", "4", "发生异常");
			return responseOME;
		}
		return responseOME;

	}

	private OMElement buildResponseOME(String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement DATASET = fac.createOMElement("CONTDEAL", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", messageType);
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}
	private String submitData(String prtno,String managecom)
    {
        String result = "";
		boolean flag = false;
        String tPrtNo = prtno;
        String tScanManageCom = managecom;

        String tPicFile_TIF = path+prtno+".tif";
        String tPicFile_GIF = path+prtno+".gif";

        FileInputStream tFPicFile_TIF = null;
        FileInputStream tFPicFile_GIF = null;

        try
        {
            ArrayList tPicFiles = new ArrayList();

            HashMap tMPicFiles = new HashMap();

            tFPicFile_TIF = new FileInputStream(new File(tPicFile_TIF));
            tMPicFiles.put("TIF", tFPicFile_TIF); // "TIF"-图片类型，统一用大写；tFPicFile_TIF-文件流

            tFPicFile_GIF = new FileInputStream(new File(tPicFile_GIF));
            tMPicFiles.put("GIF", tFPicFile_GIF); // 同上

            tPicFiles.add(tMPicFiles); // 一个tMPicFiles，代表一份图片（tif，gif）；以后如果有多张顺序添加就可以了

            VData tVData = new VData();

            // 参数要素
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("PrtNo", tPrtNo);
            tTransferData.setNameAndValue("ScanManageCom", tScanManageCom);
            tTransferData.setNameAndValue("PicFiles", tPicFiles);

            tVData.add(tTransferData);

            ESPolicyPicsBL tLogic = new ESPolicyPicsBL();
            flag = tLogic.submitData(tVData, "PAD-WNTB"); // "PAD-WNTB"-约定处理标志，写死就成；处理数据完成前，不要关闭数据流，否则图片上传可能失败，finally中统一关闭文件流。
            if(flag){
            	result = "success";
            }else{
            	result = tLogic.mErrors.getFirstError();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (tFPicFile_TIF != null)
                {
                    tFPicFile_TIF.close();
                }

                if (tFPicFile_GIF != null)
                {
                    tFPicFile_GIF.close();
                }
                //不管核心处理成功与否都将缓存的图片文件进行删除
            	deleteFiles(tPicFile_TIF,tPicFile_GIF);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return result;
    }
	private void deleteFiles(String file1,String file2){
		File f1 = new File(file1);
		File f2 = new File(file2);
		f1.delete();
		f2.delete();
	}
    /**
     * 取得当前类所在的文件
     * @param clazz
     * @return
     */
    public static File getClassFile(Class clazz){
        URL path = clazz.getResource(clazz.getName().substring(
                clazz.getName().lastIndexOf(".")+1)+".classs");
        if(path == null){
            String name = clazz.getName().replaceAll("[.]", "/");
            path = clazz.getResource("/"+name+".class");
        }
        return new File(path.getFile());
    }
    /**
     * 得到当前类的路径
     * @param clazz
     * @return
     */
    public static String getClassFilePath(Class clazz){
        try{
            return java.net.URLDecoder.decode(getClassFile(clazz).getAbsolutePath(),"UTF-8");
        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return "";
        }
    }
    /**
     * 得到存储图片路径
     * @param args
     */
    public static String getUploadPath(){
    	String path = "";
    	String classPath = getClassFilePath(ContDeal.class);
    	int pos = classPath.indexOf("WEB-INF");
    	path = classPath.substring(0, pos);
    	path = path + "temp/padapp/";
    	return path;
    }

	public static void main(String[] args) {
		String path = getUploadPath();
		System.out.println(path);
//		ContDeal deal = new ContDeal();
//		String result = deal.submitData("1234567892","86110000");
//		System.out.println(result);

	}

}
