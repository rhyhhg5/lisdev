package com.ecwebservice.app4pad;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 根据工号，管理机构，查询的投保日期的开始日期，结束日期查询 印刷号、投保日期、处理状态
 * 
 * @author zhangyige
 * @version
 */

public class ContQuery {

	/**
	 * @param args
	 */
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间

	private String messageType = null;// 报文类型

	private String agentcode = null;// 代理人编码

	private String managecom = null;// 管理机构

	private String startDate = null;// 出单起期

	private String endDate = null;// 出单止期

	private String prtNo = null;// 印刷号

	private String tPrintNO = null;// 查询出的印刷号

	private String tInputDate = null;// 出单日期

	private String tState = null;// 保单状态

	/** 当前页码 */
	private String pageIndex = null;

	/** 每页记录条数 */
	private String pageSize = null;

	private String rMaxrow = ""; // 最大记录数

	public OMElement queryData(OMElement Oms) {

		OMElement responseOME = null;
		try {

			Iterator iter1 = Oms.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();

				if (Om.getLocalName().equals("CONTQUERY")) {

					Iterator child = Om.getChildren();

					while (child.hasNext()) {
						OMElement child_element = (OMElement) child.next();

						if (child_element.getLocalName().equals("BATCHNO")) {
							batchno = child_element.getText();

							System.out.println(batchno);
						}
						if (child_element.getLocalName().equals("SENDDATE")) {
							sendDate = child_element.getText();

							System.out.println(sendDate);
						}
						if (child_element.getLocalName().equals("SENDTIME")) {
							sendTime = child_element.getText();

							System.out.println(sendTime);
						}
						if (child_element.getLocalName().equals("AGENTCODE")) {
							agentcode = child_element.getText();
							AgentCodeTransformation a = new AgentCodeTransformation();
							if(!a.AgentCode(agentcode, "N")){
								System.out.println(a.getMessage());
							}
							System.out.println(agentcode);
							agentcode = a.getResult();
						}
						if (child_element.getLocalName().equals("MANAGECOM")) {
							managecom = child_element.getText();

							System.out.println(managecom);
						}
						if (child_element.getLocalName().equals("STARTDATE")) {
							startDate = child_element.getText();

							System.out.println(startDate);
						}
						if (child_element.getLocalName().equals("ENDDATE")) {
							endDate = child_element.getText();

							System.out.println(endDate);
						}
						if (child_element.getLocalName().equals("PRTNO")) {
							prtNo = child_element.getText();

							System.out.println(prtNo);
						}
						// 当前页
						if (child_element.getLocalName().equals("PAGEINDEX")) {
							pageIndex = child_element.getText();

							System.out.println(pageIndex);
						}
						// 分页大小
						if (child_element.getLocalName().equals("PAGESIZE")) {
							pageSize = child_element.getText();

							System.out.println(pageSize);
						}
					}
					ExeSQL tExeSQL = new ExeSQL();
					// 查询出最大记录数
					String maxSql = "select count(1) from lccont lc where 1=1 ";
					// +" and exists (select 1 from Es_Doc_Main where DocCode =
					// lc.prtno and scanoperator='PADSYS')";
					// String maxSql = "select count(1) from lccont lc where 1=1
					// ";
					StringBuffer maxBuffer = new StringBuffer(maxSql);
					if (agentcode != null && !agentcode.equals("")) {
						maxBuffer.append(" and (lc.agentcode = '" + agentcode
								+ "')");
					}
					if (managecom != null && !managecom.equals("")
							&& !"null".equals(managecom)) {
						maxBuffer.append(" and lc.managecom = '" + managecom
								+ "' ");
					}
					if (startDate != null && !startDate.equals("")
							&& !startDate.equals("null")) {
						maxBuffer.append(" and lc.makedate >= '" + startDate
								+ "' ");
					}
					if (endDate != null && !endDate.equals("")
							&& !"null".equals(endDate)) {
						maxBuffer.append(" and lc.makedate <= '" + endDate
								+ "' ");
					}
					if (prtNo != null && !prtNo.equals("")
							&& !"null".equals(prtNo)) {
						maxBuffer.append(" and lc.prtno = '" + prtNo + "' ");
					}

					this.rMaxrow = new ExeSQL().getOneValue(maxBuffer
							.toString());
					// 按条件查询已经印刷号、出单日期、保单状态
					String infoSql = "select lc.prtno,lc.makedate,codename('stateflag',lc.stateflag) from lccont lc where 1=1  ";
					// +" and exists (select 1 from Es_Doc_Main where DocCode =
					// lc.prtno and scanoperator='PADSYS')";
					// String infoSql = "select
					// lc.prtno,lc.inputdate,codename('stateflag',lc.stateflag)
					// from lccont lc where 1=1 ";
					StringBuffer sqlBuffer = new StringBuffer(infoSql);
					if (agentcode != null && !agentcode.equals("")) {
						sqlBuffer.append(" and (lc.agentcode = '" + agentcode
								+ "')");
					}
					if (managecom != null && !managecom.equals("")
							&& !"null".equals(managecom)) {
						sqlBuffer.append(" and lc.managecom = '" + managecom
								+ "' ");
					}
					if (startDate != null && !startDate.equals("")
							&& !startDate.equals("null")) {
						sqlBuffer.append(" and lc.makedate >= '" + startDate
								+ "' ");
					}
					if (endDate != null && !endDate.equals("")
							&& !"null".equals(endDate)) {
						sqlBuffer.append(" and lc.makedate <= '" + endDate
								+ "' ");
					}
					if (prtNo != null && !prtNo.equals("")
							&& !"null".equals(prtNo)) {
						sqlBuffer.append(" and lc.prtno = '" + prtNo + "' ");
					}
					StringBuffer pagebuf = new StringBuffer();
					pagebuf
							.append("select * from (select rownumber() over() as rowid, aa.* from (");
					pagebuf.append(sqlBuffer.toString());
					pagebuf.append(") as aa ");
					pagebuf.append(") as tmp where 1 = 1 ");
					if (this.pageIndex == null || this.pageIndex.equals("")
							|| this.pageSize == null
							|| this.pageSize.equals("")) {
						System.out.println("没有得到分页信息");
						responseOME = buildResponseOME("01", "23", "没有得到分页信息！",
								null);
						System.out.println("responseOME:" + responseOME);
						return responseOME;
					}
					pagebuf.append("and rowid >= (").append(this.pageIndex)
							.append("-1) *").append(this.pageSize).append(
									" + 1 ");
					pagebuf.append("and rowid <= ").append(this.pageIndex)
							.append("*").append(this.pageSize).append(
									" with ur");
					SSRS sResult = tExeSQL.execSQL(pagebuf.toString());
					OMFactory fac = OMAbstractFactory.getOMFactory();
					OMElement INFOLIST = fac.createOMElement("INFOLIST", null);
					if (sResult.MaxRow > 0) {
						for (int i = 1; i <= sResult.MaxRow; i++) {
							this.tPrintNO = sResult.GetText(i, 2);
							this.tInputDate = sResult.GetText(i, 3);
							this.tState = sResult.GetText(i, 4);
							OMElement subItem = buildResponseItem();
							INFOLIST.addChild(subItem);
						}
						responseOME = buildResponseOME("00", "", "", INFOLIST);
						return responseOME;
					} else {
						if (prtNo != null && !prtNo.equals("")
								&& !"null".equals(prtNo)) {
							String inputSql = "select DocCode,makedate,'等待录入'a from Es_Doc_Main where DocCode = '"
									+ prtNo + "' and scanoperator='PADSYS'";
							StringBuffer pagebuf1 = new StringBuffer();
							pagebuf1
									.append("select * from (select rownumber() over() as rowid, aa.* from (");
							pagebuf1.append(inputSql);
							pagebuf1.append(") as aa ");
							pagebuf1.append(") as tmp where 1 = 1 ");

							SSRS ssResult = tExeSQL
									.execSQL(pagebuf1.toString());

							if (ssResult.MaxRow > 0) {
								for (int i = 1; i <= ssResult.MaxRow; i++) {
									this.tPrintNO = ssResult.GetText(i, 2);
									this.tInputDate = ssResult.GetText(i, 3);
									this.tState = ssResult.GetText(i, 4);
									OMElement subItem = buildResponseItem();
									INFOLIST.addChild(subItem);
								}
								responseOME = buildResponseOME("00", "", "",
										INFOLIST);
								return responseOME;
							} else {
								responseOME = buildResponseOME("01", "23",
										"没有查询到任何信息！", null);
								System.out
										.println("responseOME:" + responseOME);
								return responseOME;
							}
						}else{
							responseOME = buildResponseOME("01", "23",
									"没有查询到任何信息！", null);
							System.out
									.println("responseOME:" + responseOME);
							return responseOME;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseOME = buildResponseOME("01", "4", "发生异常", null);
			return responseOME;
		}
		return responseOME;

	}

	/**
	 * 创建返回结点
	 * 
	 * @param state
	 * @param errCode
	 * @param errInfo
	 * @return OMElement
	 */
	private OMElement buildResponseItem() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tool = new LoginVerifyTool();

		OMElement ITEM = fac.createOMElement("ITEM", null);
		tool.addOm(ITEM, "PRTNO", tPrintNO);
		tool.addOm(ITEM, "DATE", tInputDate);
		tool.addOm(ITEM, "CONTSTATE", tState);
		return ITEM;
	}

	// 构建返回xml

	private OMElement buildResponseOME(String state, String errCode,
			String errInfo, OMElement infoList) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("CONTQUERY", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "STATE", state);

		if (state.equals("00")) {
			tool.addOm(DATASET, "TOTALROWNUM", this.rMaxrow);
			OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
			OUTPUTDATA.addChild(infoList);
			DATASET.addChild(OUTPUTDATA);
		}
		tool.addOm(DATASET, "ERRCODE", errCode);
		tool.addOm(DATASET, "ERRINFO", errInfo);

		return DATASET;
	}

	public static void main(String[] args) {

	}

}
