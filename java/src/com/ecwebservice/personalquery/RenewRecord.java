package com.ecwebservice.personalquery;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-12 下午04:14:04
 * @Description:
 * @version
 */

public class RenewRecord implements CardHYX {
	private String[] baseInfoString;

	// baseInfo[0] = batchno;
	// baseInfo[1] = sendDate;
	// baseInfo[2] = sendTime;
	// baseInfo[3] = messageType;
	// 封装结果集
	private Set resultSet = new HashSet();

	private String contNo;

	private String renewRecordCount;

	public OMElement queryData(OMElement Oms) {
		QueryTool qTool = new QueryTool();
		// 获取baseInfo
		baseInfoString = qTool.parseBaseInfo(Oms);
		Map queryStringMap = qTool.parseQueryString(Oms);// 包含查询字段的名称及值

		System.out.println("进入个险--保单续期记录处理类！");
		OMElement responseOME = null ;
		contNo = qTool.getContNoInQueryString(Oms);
		ExeSQL tExeSQL = new ExeSQL();
		String renewRecordCountSql = "select count(*) from lcpol a where contno = '"
				+ contNo + "'";
		renewRecordCount = tExeSQL.getOneValue(renewRecordCountSql);
		// 分页
		int pageIndex = Integer.parseInt(baseInfoString[4]);
		int pageSize = Integer.parseInt(baseInfoString[5]);
		int startRow = (pageIndex - 1) * pageSize + 1;
		int endRow = pageIndex * pageSize;
		String renewRecordSql = "SELECT * FROM (select (select riskname from lmriskapp where riskcode = a.riskcode) 险种名称 ,"
				+ "prem 每期缴费金额,'' 缴费次数,"
				+ "codename('payintv',char(a.payintv)) 缴费形式,paytodate 缴费对应日 ,codename('paymode',a.paymode) 缴费方式,"
				+ "rownumber() over(ORDER BY prem ASC) AS rn from lcpol a where contno = '"
				+ contNo
				+ "') AS a1 WHERE a1.rn BETWEEN "
				+ startRow
				+ " AND "
				+ endRow;

		SSRS renewRecordSSRS = tExeSQL.execSQL(renewRecordSql);
		boolean queryFlag = false;
		if (renewRecordSSRS.MaxRow>0) {
			queryFlag = true;
			int renewRecordPointer;
			for (renewRecordPointer = 1; renewRecordPointer <= renewRecordSSRS.MaxRow; renewRecordPointer++) {
				Map schemaMap = new HashMap();
				schemaMap.put("RISKNAME", renewRecordSSRS.GetText(
						renewRecordPointer, 1));
				schemaMap.put("AMOUNT", renewRecordSSRS.GetText(
						renewRecordPointer, 2));
				schemaMap.put("INDEX", renewRecordSSRS.GetText(
						renewRecordPointer, 3));// 缴费次数
				schemaMap.put("PAYTYPE", renewRecordSSRS.GetText(
						renewRecordPointer, 4));// 缴费形式
				schemaMap.put("PAYDATE", renewRecordSSRS.GetText(
						renewRecordPointer, 5));// 缴费对应日
				schemaMap.put("PAYWAY", renewRecordSSRS.GetText(
						renewRecordPointer, 6));// 缴费方式
				resultSet.add(schemaMap);
			}
		}
		if(queryFlag){
			 responseOME = buildResponseOME(resultSet, "00", "0", "成功");
		}else{
			responseOME = buildResponseOME(resultSet, "00", "*", "没有符合查询条件的结果");
		}
		
		System.out.println("返回报文："+responseOME.toString());
		return responseOME;
	}

	private OMElement buildResponseOME(Set resultSet, String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("RENEWRECORD", null);

		LoginVerifyTool tool = new LoginVerifyTool();
		QueryTool qTool = new QueryTool();
		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString,
				renewRecordCount, state, errCode, errInfo);

		DATASET.addChild(BASEINFO);// 构建基础信息

		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
		OMElement RLIST = fac.createOMElement("RLIST", null);
		// 遍历HashSet
		Iterator resultIterator = resultSet.iterator();
		while (resultIterator.hasNext()) {
			Map resultSchemaMap = (HashMap) resultIterator.next();
			OMElement ITEM = fac.createOMElement("ITEM", null);

			tool.addOm(ITEM, "RISKNAME", (String) resultSchemaMap
					.get("RISKNAME"));
			tool.addOm(ITEM, "AMOUNT", (String) resultSchemaMap.get("AMOUNT"));
			tool.addOm(ITEM, "INDEX", (String) resultSchemaMap.get("INDEX"));
			tool
					.addOm(ITEM, "PAYTYPE", (String) resultSchemaMap
							.get("PAYTYPE"));
			tool
					.addOm(ITEM, "PAYDATE", (String) resultSchemaMap
							.get("PAYDATE"));
			tool.addOm(ITEM, "PAYWAY", (String) resultSchemaMap.get("PAYWAY"));

			RLIST.addChild(ITEM);
		}

		OUTPUTDATA.addChild(RLIST);

		DATASET.addChild(OUTPUTDATA);

		return DATASET;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
