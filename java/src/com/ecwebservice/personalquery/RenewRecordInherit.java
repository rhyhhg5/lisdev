package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.ListInterface;


public class RenewRecordInherit extends ListInterface {

	public void prepareCondition() {
		QueryTool qTool = new QueryTool();
		Map queryStringMap = qTool.parseQueryString(this.getOms());
		
		String rgtSql = "select (select riskname from lmriskapp where riskcode = a.riskcode) 险种名称 ,sumactupaymoney 每期缴费金额,paycount 缴费次数," +
				"(select codename('payintv',char(b.payintv)) from lcpol b where polno = a.polno) 缴费形式,lastpaytodate 缴费对应日 ," +
				"(select codename('paymode',b.paymode)  from lcpol b where polno = a.polno) 付费方式 "
				+",a.curpaytodate 缴至日期 "
				+ "from ljapayperson a where 1=1 and paytype <> 'YEL' ";
		StringBuffer querySqlBuffer = new StringBuffer();
		querySqlBuffer.append(rgtSql);
		// 查询条件
		String contNo = (String) queryStringMap.get("CONTNO");//保单号
		if (contNo != null && !"".equals(contNo)){
			querySqlBuffer.append("and  contno = '"+contNo+"'");
		}
		//date 20101126 by gzh 现根据时间排序，再根据险种名称排序。
		querySqlBuffer.append(" order by lastpaytodate desc,险种名称");

		this.setRootTag("RENEWRECORD");
		List returnChildTag = new ArrayList();
		returnChildTag.add("RISKNAME");
		returnChildTag.add("AMOUNT");
		returnChildTag.add("INDEX");
		returnChildTag.add("PAYTYPE");
		returnChildTag.add("PAYDATE");
		returnChildTag.add("PAYWAY");
		returnChildTag.add("PAYTODATE");
		
		this.setChildTagName(returnChildTag);
		
		System.out.println("个单续期记录查询SQL："+querySqlBuffer.toString());
		setQuerySql(querySqlBuffer.toString());
		this.setOrderCol("paycount");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
