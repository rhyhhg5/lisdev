package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-12 下午05:11:22
 * @Description:
 * @version
 */

public class ContDetailInherit extends StandCardHYX {

	private String mContNo = "";

	public ContDetailInherit() {
		// 返回报文根节点
		super("CONTDETAIL");
	}

	protected void getQueryFactor(Map queryFactor) {

		// 获取条件值 CONTNO
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();
			if ("CONTNO".equalsIgnoreCase(key)) {
				mContNo = (String) queryFactor.get(key);
				System.out.println("个人保单详细信息查询,contno=" + mContNo);
			}
		}

	}

	public Map prepareData() {
		QueryTool qTool = new QueryTool();
		Map OUTPUTDATA = new HashMap();
		// 查询业务数据并封装
		
		//险种信息
		String sql1 = " select a.dutycode,(select dutyname from lmduty where dutycode=a.dutycode),'' 代码简称,a.amnt,b.enddate , a.prem,b.payintv ,"
				+ "b.payyears ,b.years ,b.PayEndYear ,b.PayEndYearFlag,b.insuyear ,b.insuyearFlag,b.mult  " +
						"from lcduty a ,lcpol b where a.polno = b.polno and b.contno = '"
				+ mContNo + "' with ur";
				
					
		SSRS riskSSRS = mExeSQL.execSQL(sql1);
		Map riskInfos = new HashMap();
		Map info = null;
		List item = new ArrayList();
		for (int i = 1; i <= riskSSRS.MaxRow; i++) {
			info = new HashMap();
			info.put("RISKCODE", riskSSRS.GetText(i, 1));
			info.put("RISKNAME", riskSSRS.GetText(i, 2));
			// info.put("RISKSHORTCODE", riskSSRS.GetText(i, 3));
//			System.out.println("保额："+riskSSRS.GetText(i, 4));
			if(riskSSRS.GetText(i, 1).equals("121001")){
				System.out.println(riskSSRS.GetText(i, 14));
				if(riskSSRS.GetText(i, 14).equals("1"))info.put("AMNT","一档");
				else if(riskSSRS.GetText(i, 14).equals("2"))info.put("AMNT","二档");
				else if(riskSSRS.GetText(i, 14).equals("3"))info.put("AMNT","三档");
				else info.put("AMNT", riskSSRS.GetText(i, 4));
			}else{
				info.put("AMNT", riskSSRS.GetText(i, 4));
			}
			LoginVerifyTool commonTool = new LoginVerifyTool();
			//拼接保险期间
//			String insuYearFlagCode = riskSSRS.GetText(i, 13);
//			String insuYearFlag = commonTool.switchInsuYearFlag(insuYearFlagCode);
			
//			info.put("TERM", riskSSRS.GetText(i, 12)+insuYearFlag);// 保险期间
//			info.put("ENDDATE", riskSSRS.GetText(i, 5));// 终止日期
			info.put("PREM", riskSSRS.GetText(i, 6));
			
			//转换缴费间隔
			String payIntvName = qTool.decodePayIntv(riskSSRS.GetText(i, 7));
			
			info.put("PAYSEP", payIntvName);// 缴费间隔
			//缴费年期（终交年龄年期） 拼接
			String payEndYearFlag = riskSSRS.GetText(i, 11);
			
			String payEndYearFlagStr = commonTool.switchInsuYearFlag(payEndYearFlag);
			
			info.put("PAYTERM", riskSSRS.GetText(i, 10)+payEndYearFlagStr);// 缴费年期
			info.put("PAYDATE", "");// 缴费对应日
			item.add(info);
		}
		riskInfos.put("ITEM", item);
		OUTPUTDATA.put("RISKINFO", riskInfos);
		// 投保人信息
		String appntInfoSql = "select a.appntname,codename('sex',a.appntsex),a.appntbirthday,codename('idtype',a.idtype),"
				+ "a.idno,codename('marriage',a.marriage),codename('nativeplace',a.nativeplace),a.occupationcode ,"
				+ "b.mobile ,b.companyphone  办公电话,b.homephone  住宅电话,b.postaladdress ,b.homezipcode ,b.email "
				//modify by zhangyige at 2014-08-19 微信项目查询增加 联系电话
				+ ",b.phone  联系电话  "
				+ "from Lcappnt a,lcaddress b where a.contno='"
				+ mContNo
				+ "' and b.customerno=a.appntno and b.addressno=a.addressno fetch first 1 rows only";
		SSRS appntInfoSSRS = mExeSQL.execSQL(appntInfoSql);

		Map appntInfosMap = new HashMap();
		Map appntInfoMap = null;
		List appntInfoItem = new ArrayList();
		for (int i = 1; i <= appntInfoSSRS.MaxRow; i++) {
			appntInfoMap = new HashMap();
			appntInfoMap.put("APPNTNAME", appntInfoSSRS.GetText(i, 1));
			appntInfoMap.put("APPNTSEX", appntInfoSSRS.GetText(i, 2));
			appntInfoMap.put("APPNTIDTYPE", appntInfoSSRS.GetText(i, 4));
			appntInfoMap.put("APPNTIDNO", appntInfoSSRS.GetText(i, 5));
			appntInfoMap.put("APPNTBIRTHDAY", appntInfoSSRS.GetText(i, 3));
			appntInfoMap.put("APPNTMARRYSTATE", appntInfoSSRS.GetText(i, 6));
			appntInfoMap.put("APPNTNATIVEPLACE", appntInfoSSRS.GetText(i, 7));
			
			appntInfoMap.put("APPNTOCCUPATION",qTool.decodeOccupationcode(appntInfoSSRS.GetText(i, 8)) );
			appntInfoMap.put("APPNTMOBILE", appntInfoSSRS.GetText(i, 9));
			appntInfoMap.put("APPNTCOMPANYPHONE", appntInfoSSRS.GetText(i, 10));
			appntInfoMap.put("APPNTHOMEPHONE", appntInfoSSRS.GetText(i, 11));
			appntInfoMap.put("APPNTADDRESS", appntInfoSSRS.GetText(i, 12));
			appntInfoMap.put("APPNTZIPCODE", appntInfoSSRS.GetText(i, 13));
			appntInfoMap.put("APPNTEMAIL", appntInfoSSRS.GetText(i, 14));
			
			appntInfoMap.put("APPNTPHONE", appntInfoSSRS.GetText(i, 15));//modify by zhangyige at 2014-08-19 微信项目查询增加 联系电话

			// insuredInfoMap.put("INSUNO", insuredInfoSSRS.GetText(i, 1));

			appntInfoItem.add(appntInfoMap);
		}
		if(appntInfoMap == null){  //团单
			String sql = "select a.appntname "
					+ "from lccont a where a.contno='" + mContNo + "'";
			SSRS rs = mExeSQL.execSQL(sql);
			if(rs.getMaxRow() > 0){
				appntInfoMap = new HashMap();
				appntInfoMap.put("APPNTNAME", rs.GetText(1, 1));
				appntInfoItem.add(appntInfoMap);
			}
		}
		
		
		appntInfosMap.put("ITEM", appntInfoItem);
		OUTPUTDATA.put("APPNTINFO", appntInfosMap);
		
		
		//*******************************************
		
//		 被保人险种信息 add by fuxin 
		String riskString = "select insuredname "
							+",(select idno from lcinsured where a.contno= contno and a.insuredno = insuredno) "
							+",(select riskname from lmriskapp where a.riskcode = riskcode) "
							+",insuyear ,enddate,payintv,payyears,"
							+ " (case when a.riskcode='170501' then (select insuaccbala from lcinsureacc where contno='"+mContNo+"' fetch first 1 rows only) else amnt end) amnt "
							//modify by zhangyige at 2014-08-19  微信项目查询增加 保费、生效日期、缴至日期、险种状态
							+",prem,CValiDate,paytodate,codename('stateflag',stateflag)"
							+" from lcpol a where contno ='"+mContNo+"'"
							+" group by a.contno,insuredname,a.insuredno,a.riskcode,insuyear,enddate,payintv,payyears,amnt,prem,CValiDate,paytodate,STATEFLAG,a.riskseqno "
							+" order by a.riskseqno"; 
		SSRS riskSSRS1 = new ExeSQL().execSQL(riskString);
		
		Map riskInfos1 = new HashMap();
		Map info1 = null;
		List item1 = new ArrayList();

			for (int i = 1 ; i<=riskSSRS1.getMaxRow(); i++ )
			{
				info1 = new HashMap();
				info1.put("IRINSUREDNAME", riskSSRS1.GetText(i, 1));
				info1.put("IRINSUREDIDNO", riskSSRS1.GetText(i, 2));
				info1.put("IRRISKNAME", riskSSRS1.GetText(i, 3));
				info1.put("IRTERM", riskSSRS1.GetText(i, 4));
				info1.put("IRENDDATE", riskSSRS1.GetText(i, 5));
				info1.put("IRPAYSEP", riskSSRS1.GetText(i, 6));
				info1.put("IRPAYTERM", riskSSRS1.GetText(i, 7));
				info1.put("IRAMNT", riskSSRS1.GetText(i, 8));
				//modify by zhangyige at 2014-08-19  微信项目查询增加 保费、生效日期、缴至日期、险种状态
				info1.put("IRPREM", riskSSRS1.GetText(i, 9));
				info1.put("IRCVALIDATE", riskSSRS1.GetText(i, 10));
				info1.put("IRPAYTODATE", riskSSRS1.GetText(i, 11));
				info1.put("IRRISKSTATE", riskSSRS1.GetText(i, 12));
				
				item1.add(info1);
			}
			riskInfos1.put("ITEM", item1);
			OUTPUTDATA.put("INSUREDRISKINFO", riskInfos1);
		
		//*******************************************

		// 被保人信息
		String insuredInfoSql = "select a.insuredno 被保人编号,(select codename from ldcode where codetype = 'relation' and code = a.relationtoappnt ) 与投保人关系,"
				+ "(case  relationtomaininsured when '00' then '主被保人' else '连带' end) 当前被保人连带与否 ,"
				+ " (select codename from ldcode where codetype = 'relation' and code = a.relationtomaininsured) 与主被保人关系,"
				+ "'' 主被保人编号,a.sequenceno 被保人顺序,a.name 被保人姓名,codename('sex',a.sex) 被保人性别,"
				+ "codename('idtype',a.idtype),a.idno,a.birthday,codename('marriage',a.marriage),codename('nativeplace',a.nativeplace) 国籍,"
				+ "a.OccupationCode 职业,a.addressno from Lcinsured a  where a.contno  = '"
				+ mContNo + " '";
		SSRS insuredInfoSSRS = mExeSQL.execSQL(insuredInfoSql);

		Map insuredInfosMap = new HashMap();
		Map insuredInfoMap = null;
		List insuredInfoItem = new ArrayList();
		for (int i = 1; i <= insuredInfoSSRS.MaxRow; i++) {
			insuredInfoMap = new HashMap();
			insuredInfoMap.put("INSUNO", insuredInfoSSRS.GetText(i, 1));

			insuredInfoMap.put("TOAPPNTRELA", insuredInfoSSRS.GetText(i, 2));
			insuredInfoMap.put("RELATIONCODE", insuredInfoSSRS.GetText(i, 3));
			insuredInfoMap.put("RELATIONTOINSURED", insuredInfoSSRS.GetText(i,
					4));
			insuredInfoMap.put("REINSUNO", insuredInfoSSRS.GetText(i, 5));
			insuredInfoMap.put("INSUREDORDER", insuredInfoSSRS.GetText(i, 6));
			insuredInfoMap.put("INSUREDNAME", insuredInfoSSRS.GetText(i, 7));
			insuredInfoMap.put("INSUREDSEX", insuredInfoSSRS.GetText(i, 8));
			insuredInfoMap.put("INSUREDIDTYPE", insuredInfoSSRS.GetText(i, 9));
			insuredInfoMap.put("INSUREDIDNO", insuredInfoSSRS.GetText(i, 10));
			insuredInfoMap.put("INSUREDBIRTHDAY", insuredInfoSSRS
					.GetText(i, 11));
			insuredInfoMap.put("INSUREDMARRYSTATE", insuredInfoSSRS.GetText(i,
					12));
			insuredInfoMap.put("INSUREDNATIVEPLACE", insuredInfoSSRS.GetText(i,
					13));
			//decode 职业编码
			String occupation =  qTool.decodeOccupationcode(insuredInfoSSRS.GetText(i,
					14));
			
			insuredInfoMap.put("INSUREDOCCUPATION", occupation);

			String insuNo = insuredInfoSSRS.GetText(i, 1);
			String addressno = insuredInfoSSRS.GetText(i, 15);
			//2018年1月5日14:57:17 jialei
			//修改1.增加团险分单处理机制，团单分单被保人地址表为空，查询不到数据，会导致程序处理异常。
			//修改2.修改查询逻辑，只查询当前保单的地址信息，确保保单和用户信息一致。
			String addressInfoSql = "select "
					+ "mobile 移动电话,"
					+ "companyphone 办公电话,"
					+ "homephone 住宅电话,"
					+ "postaladdress 联系地址, "
					+ "zipcode 邮政编码 ,"
					+ "email 邮箱 ,"
					+ "phone 联系电话 "
					+ "from  lcaddress lca  "
					+ "where lca.addressno = '"+addressno+"' and "
					+ "lca.customerno = '"+insuNo+"'";
			SSRS addressInfoSSRS = mExeSQL.execSQL(addressInfoSql);
			if(addressInfoSSRS.MaxRow==0){
				insuredInfoMap.put("INSUREDMOBILE", "");
				insuredInfoMap.put("INSUREDCOMPANYPHONE", "");
				insuredInfoMap.put("INSUREDHOMEPHONE", "");
				insuredInfoMap.put("INSUREDADDRESS", "");
				insuredInfoMap.put("INSUREDZIPCODE", "");
				insuredInfoMap.put("INSUREDEMAIL", "");
				insuredInfoMap.put("INSUREPHONE", "");
			}else{
				insuredInfoMap.put("INSUREDMOBILE", addressInfoSSRS.GetText(1, 1));
				insuredInfoMap.put("INSUREDCOMPANYPHONE", addressInfoSSRS.GetText(1, 2));
				insuredInfoMap.put("INSUREDHOMEPHONE", addressInfoSSRS.GetText(1, 3));
				insuredInfoMap.put("INSUREDADDRESS", addressInfoSSRS.GetText(1, 4));
				insuredInfoMap.put("INSUREDZIPCODE", addressInfoSSRS.GetText(1, 5));
				insuredInfoMap.put("INSUREDEMAIL", addressInfoSSRS.GetText(1, 6));
				insuredInfoMap.put("INSUREPHONE", addressInfoSSRS.GetText(1, 7));//modify by zhangyige at 2014-08-19 微信项目查询增加 联系电话
			}
			// insuredInfoMap.put("INSUNO", insuredInfoSSRS.GetText(i, 1));
			insuredInfoItem.add(insuredInfoMap);
		}
		insuredInfosMap.put("ITEM", insuredInfoItem);
		OUTPUTDATA.put("INSUREDINFO", insuredInfosMap);

		// 受益人信息
		// modify by fuxin 2010-10-14 查询受益人信息后需要分组 不然会有重复记录。
		String bnfInfoSql = " select a.name,codename('idtype',a.idtype),a.idno,a.bnfgrade  收益顺位,a.bnflot*100 收益比例 ,a.InsuredNo 被保人客户号,"
				+ "c.riskname 险种名称 ,(select codename from ldcode where codetype = 'relation' and code = a.RelationToInsured ) 所属被保人关系"
				+ " from lcbnf a,lcpol b,Lmriskapp c where a.contno  = '"
				+ mContNo
				+ " ' and a.contno = b.contno and b.riskcode = c.riskcode group by a.name,a.idtype,a.idno,a.bnfgrade,a.bnflot,a.InsuredNo,c.riskname,a.RelationToInsured  ";
		SSRS bnfInfoSSRS = mExeSQL.execSQL(bnfInfoSql);

		Map bnfInfosMap = new HashMap();
		Map bnfInfoMap = null;
		List bnfInfoItem = new ArrayList();
		for (int i = 1; i <= bnfInfoSSRS.MaxRow; i++) {
			bnfInfoMap = new HashMap();
			bnfInfoMap.put("TOINSUNO", bnfInfoSSRS.GetText(i, 6));

			bnfInfoMap.put("RNAME", bnfInfoSSRS.GetText(i, 7));// 险种名称
			bnfInfoMap.put("TOINSURELA", bnfInfoSSRS.GetText(i, 8));// 所属被保人关系
			bnfInfoMap.put("BNFGRADE", bnfInfoSSRS.GetText(i, 4));
			bnfInfoMap.put("BNFRATE", bnfInfoSSRS.GetText(i, 5));
			bnfInfoMap.put("BNFNAME", bnfInfoSSRS.GetText(i, 1));
			bnfInfoMap.put("BNFIDTYPE", bnfInfoSSRS.GetText(i, 2));
			bnfInfoMap.put("BNFIDNO", bnfInfoSSRS.GetText(i, 3));

			bnfInfoItem.add(bnfInfoMap);
		}
		bnfInfosMap.put("ITEM", bnfInfoItem);
		OUTPUTDATA.put("BNFINFO", bnfInfosMap);
		
		//****************************************
		//查询新契约回访结果 BY LI CAIYAN 2012-11-1
		
		String returnVisitSQL = "select (case returnvisitflag when '1' then '健康件' when '2' then '问题件'  when '3' then '拒访件' when '4' then '书面回访件'  when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable  where policyno = '"+mContNo+"' ";	
		SSRS returnVisitSSRS = mExeSQL.execSQL(returnVisitSQL);
		
		if (returnVisitSSRS.MaxRow > 0) {
			OUTPUTDATA.put("RETURNVISITINFO", returnVisitSSRS.GetText(1, 1));

		}
		
      //**********************************************
		String sql2 = "select a.contno,a.ManageCom, d.name 销售机构 ,b.name 服务人员姓名,b.phone 服务人员电话,Cvalidate,signdate,"
				+ "CustomGetPolDate,Getpoldate 回单录入日期 ," +
						"codename('stateflag', stateflag)  保单状态,d.branchaddress, " +
						"c.name 销售机构名称 ,c.address 销售机构地址 "+    //BY GZH 增加销售机构本级名称和地址
						"from Lccont a,laagent b ,ldcom c ,labranchgroup d "
				+ " where contno='"
				+ mContNo
				+ "' and a.agentcode = b.agentcode and a.Managecom=c.comcode and a.agentgroup=d.agentgroup ";
//		 缴费方式
		String payModeSql = "select bankaccno from ljspay where  otherno='"
				+ mContNo + "'";
		String payModeCode = mExeSQL.getOneValue(payModeSql);
		String payMode = qTool.decodePayMode(payModeCode);

		SSRS contSSRS = mExeSQL.execSQL(sql2);
		if (contSSRS.MaxRow > 0) {
			String[] errInfoArr = { "00", "0", "成功" };
			this.setErrInfoArr(errInfoArr);

			OUTPUTDATA.put("CONTNO", contSSRS.GetText(1, 1));
			OUTPUTDATA.put("MANAGECOM", contSSRS.GetText(1, 2));
//			OUTPUTDATA.put("SALECOMNAME", contSSRS.GetText(1, 3));
//			OUTPUTDATA.put("SALECOMADDRESS", contSSRS.GetText(1, 11));
			OUTPUTDATA.put("SALECOMNAME", contSSRS.GetText(1, 12));//BY GZH 增加销售机构本级名称
			OUTPUTDATA.put("SALECOMADDRESS", contSSRS.GetText(1, 13));//BY GZH 增加销售机构地址
			OUTPUTDATA.put("AGENTNAME", contSSRS.GetText(1, 4));
			OUTPUTDATA.put("AGENTTELEPHONE", contSSRS.GetText(1, 5));
			OUTPUTDATA.put("CVALIDATE", contSSRS.GetText(1, 6));
			OUTPUTDATA.put("SIGNDATE", contSSRS.GetText(1, 7));
			OUTPUTDATA.put("CUSTOMGETPOLDATE", contSSRS.GetText(1, 8));
			OUTPUTDATA.put("GETPOLDATE", contSSRS.GetText(1, 9));
			OUTPUTDATA.put("CONTSTATE", contSSRS.GetText(1, 10));

			OUTPUTDATA.put("PAYLOCATION", payMode);
			OUTPUTDATA.put("ISAUTOPAY", "");

		}
		


		return OUTPUTDATA;
	}

	/**
	 * 单元测试
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement queryData = fac.createOMElement("queryData ", null);

		OMElement DATASET = fac.createOMElement("CONTLIST", null);
		OMElement BASEINFO = fac.createOMElement("BASEINFO", null);
		// 查询条件
		OMElement INPUTDATA = fac.createOMElement("INPUTDATA", null);

		DATASET.addChild(BASEINFO);
		DATASET.addChild(INPUTDATA);
		queryData.addChild(DATASET);

		LoginVerifyTool tool = new LoginVerifyTool();

		tool.addOm(BASEINFO, "BATCHNO", "11");
		tool.addOm(BASEINFO, "SENDDATE", "11");
		tool.addOm(BASEINFO, "SENDTIME", "11");
		tool.addOm(BASEINFO, "MESSAGETYPE", "04");

		tool.addOm(INPUTDATA, "CONTNO", "000996109000001");

		ContDetailInherit t = new ContDetailInherit();
		System.out.println(t.queryData(queryData)); 

	}
}
