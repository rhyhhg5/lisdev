package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * @author yuanzw
 * 创建时间：2010-6-12 下午05:11:22
 * @Description:
 * @version 
 */

public class InsuredContDetail extends StandCardHYX {
	
	private String mContNo = "";
	
	public InsuredContDetail() {
		//返回报文根节点
		super("INSUREDCONTDETAIL");
	}

	protected void getQueryFactor(Map queryFactor) {
		
		//获取条件值  CONTNO
		Iterator factor = queryFactor.keySet().iterator();
		
		while(factor.hasNext())
		{
			String key = factor.next().toString();
			if("CONTNO".equalsIgnoreCase(key))
			{
				mContNo = (String) queryFactor.get(key);
				System.out.println("团单分单详细信息查询,contno="+mContNo);
			}
		}
		
	}
	
	public Map prepareData() {
		
		Map OUTPUTDATA = new HashMap();
		//查询业务数据并封装
		String sql1 = " select a.dutycode,(select dutyname from lmduty where dutycode=a.dutycode),'' 代码简称,a.amnt from lcduty a ,lcpol b where a.polno = b.polno and b.contno = '"+mContNo+"'"
			+" union all"
			+" select a.dutycode,(select dutyname from lmduty where dutycode=a.dutycode),'' 代码简称,a.amnt from lbduty a ,lbpol b where a.polno = b.polno and b.contno = '"+mContNo+"'";
		SSRS riskSSRS = mExeSQL.execSQL(sql1);
		
//------金额明细-------start Added by wdryn 2015-0805
		String strTemp = "";
		String sqljin = "select contno,money,makedate from lcinsureacctrace where moneytype='GA' and contno = '"+mContNo+"' ";
		SSRS tSSRS = new ExeSQL().execSQL(sqljin);
		for(int i = 1; i <= tSSRS.MaxRow; i++){
			for(int j =1;j<=tSSRS.MaxCol;j++){
				String memTemp =  tSSRS.GetText(i, j);
			    	strTemp += memTemp + "/";
			}
		}
		if(strTemp.length()>0){
			strTemp = strTemp.substring(0,strTemp.length()-1);
		}
//--------------------end

		Map riskInfos = new HashMap();
		Map info = null;
		List item = new ArrayList();
		for (int i=1;i<=riskSSRS.MaxRow;i++)
		{
			info = new HashMap();
			info.put("RISKCODE", riskSSRS.GetText(i, 1));
			info.put("RISKNAME", riskSSRS.GetText(i, 2));
//			info.put("RISKSHORTCODE", riskSSRS.GetText(i, 3));
			info.put("AMNT", riskSSRS.GetText(i, 4));
			info.put("DETAILMONEY", strTemp);
			item.add(info);
		}
		riskInfos.put("ITEM", item);
		OUTPUTDATA.put("RISKINFO", riskInfos);
		
		//新加被保人信息
		String insurSQL="select lci.insuredno,"
					       +"lci.relationtoappnt,"
					       +"case lci.relationtomaininsured when '00' then '00' else '01' end,"
					       +"lci.relationtomaininsured,"
					       +"'',"
					       +"'',"
					       +"lci.name,"
					       +"codename('sex',lci.sex),"
					       +"codename('idtype',lci.idtype),"
					       +"lci.idno,"
					       +"lci.birthday,"
					       +"lci.Marriage,"
					       +"lci.NativePlace,"
					       +"lci.WorkType,"
					       +"lca.Mobile,"
					       +"lca.CompanyPhone,"
					       +"lca.HomePhone,"
					       +"lca.PostalAddress,"
					       +"lca.ZipCode,"
					       +"lca.EMail "
						  +"from lcinsured lci "
						 +"left join lcaddress lca on lci.insuredno = lca.customerno "
                         +"and lci.addressno = lca.addressno where 1=1 and lci.contno='"+mContNo+"'"
                +" union all"+
			                " select lci.insuredno,"
						       +"lci.relationtoappnt,"
						       +"case lci.relationtomaininsured when '00' then '00' else '01' end,"
						       +"lci.relationtomaininsured,"
						       +"'',"
						       +"'',"
						       +"lci.name,"
						       +"codename('sex',lci.sex),"
						       +"codename('idtype',lci.idtype),"
						       +"lci.idno,"
						       +"lci.birthday,"
						       +"lci.Marriage,"
						       +"lci.NativePlace,"
						       +"lci.WorkType,"
						       +"lca.Mobile,"
						       +"lca.CompanyPhone,"
						       +"lca.HomePhone,"
						       +"lca.PostalAddress,"
						       +"lca.ZipCode,"
						       +"lca.EMail "
							  +"from lbinsured lci "
							 +"left join lcaddress lca on lci.insuredno = lca.customerno "
			              +"and lci.addressno = lca.addressno where 1=1 and lci.contno='"+mContNo+"'";
		SSRS insurSSRS = mExeSQL.execSQL(insurSQL);//??
		Map insuredInfos = new HashMap();
		Map item_info = null;
		List item2 = new ArrayList();
		for (int i=1;i<=insurSSRS.MaxRow;i++)
		{
			item_info = new HashMap();
			item_info.put("INSUNO", insurSSRS.GetText(i, 1));
			item_info.put("TOAPPNTRELA", insurSSRS.GetText(i, 2));
			item_info.put("RELATIONCODE", insurSSRS.GetText(i, 3));
			item_info.put("RELATIONTOINSURED", insurSSRS.GetText(i, 4));
			item_info.put("REINSUNO", insurSSRS.GetText(i, 5));
			item_info.put("INSUREDORDER", insurSSRS.GetText(i, 6));
			item_info.put("INSUREDNAME", insurSSRS.GetText(i, 7));
			item_info.put("INSUREDSEX", insurSSRS.GetText(i, 8));
			item_info.put("INSUREDIDTYPE", insurSSRS.GetText(i, 9));
			item_info.put("INSUREDIDNO", insurSSRS.GetText(i, 10));
			item_info.put("INSUREDBIRTHDAY", insurSSRS.GetText(i, 11));
			
			item_info.put("INSUREDMARRYSTATE", insurSSRS.GetText(i, 12));
			item_info.put("INSUREDNATIVEPLACE", insurSSRS.GetText(i, 13));
			item_info.put("INSUREDOCCUPATION", insurSSRS.GetText(i, 14));
			item_info.put("INSUREDMOBILE", insurSSRS.GetText(i, 15));
			item_info.put("INSUREDCOMPANYPHONE", insurSSRS.GetText(i, 16));
			item_info.put("INSUREDHOMEPHONE", insurSSRS.GetText(i, 17));
			item_info.put("INSUREDADDRESS", insurSSRS.GetText(i, 18));
			item_info.put("INSUREDZIPCODE", insurSSRS.GetText(i, 19));
			item_info.put("INSUREDEMAIL", insurSSRS.GetText(i, 20));

			item2.add(item_info);
		}
		insuredInfos.put("ITEM", item2);
		OUTPUTDATA.put("INSUREDINFO", insuredInfos);
		
		
		String sql2 = "select c.contno,b.grpname,a.linkman1,a.linkman2,a.mobile1,a.phone1,a.e_mail1,a.fax1,a.grpaddress,a.grpzipcode ,b.cvalidate,b.RelaPeoples,codename('stateflag', c.stateflag),c.CInValiDate";
		sql2 = sql2+" from lcgrpaddress a,lcgrpcont b,lccont c,lcgrpappnt lcgrp"
		 +" where a.customerno = b.appntno and b.grpcontno = c.grpcontno and lcgrp.customerno=c.appntno and lcgrp.grpcontno=c.grpcontno "
		 +" and a.addressno=lcgrp.addressno and c.contno = '"+mContNo+"' "
	+"union all"
		 +" select c.contno,b.grpname,a.linkman1,a.linkman2,a.mobile1,a.phone1,a.e_mail1,a.fax1,a.grpaddress,a.grpzipcode ,b.cvalidate,b.RelaPeoples,codename('stateflag', c.stateflag),c.CInValiDate"
	 	 +" from lcgrpaddress a,lcgrpcont b,lbcont c,lcgrpappnt lcgrp"
		 +" where a.customerno = b.appntno and b.grpcontno = c.grpcontno and lcgrp.customerno=c.appntno and lcgrp.grpcontno=c.grpcontno "
		 +" and a.addressno=lcgrp.addressno and c.contno = '"+mContNo+"' ";
		
		SSRS contSSRS = mExeSQL.execSQL(sql2);
		if(contSSRS.MaxRow>0){
			String[] errInfoArr = {"00","0","成功"};
			this.setErrInfoArr(errInfoArr);
			
			OUTPUTDATA.put("CONTNO", contSSRS.GetText(1, 1));
			OUTPUTDATA.put("GRPCUSTNAME", contSSRS.GetText(1, 2));
			OUTPUTDATA.put("LINKMAN1", contSSRS.GetText(1, 3));
			OUTPUTDATA.put("LINKMAN2", contSSRS.GetText(1, 4));
			OUTPUTDATA.put("PHONE1", contSSRS.GetText(1, 5));
			OUTPUTDATA.put("PHONE2", contSSRS.GetText(1, 6));
			OUTPUTDATA.put("EMAIL1", contSSRS.GetText(1, 7));
			OUTPUTDATA.put("EMAIL2", contSSRS.GetText(1, 8));
			OUTPUTDATA.put("ADDRESS", contSSRS.GetText(1, 9));
			OUTPUTDATA.put("ZIPCODE", contSSRS.GetText(1, 10));				
			OUTPUTDATA.put("CVALIDATE", contSSRS.GetText(1, 11));
			OUTPUTDATA.put("ENDDATE", contSSRS.GetText(1, 14));
			OUTPUTDATA.put("RELAMATEPEOPLES", contSSRS.GetText(1, 12));	
			OUTPUTDATA.put("CONTSTATE", contSSRS.GetText(1, 13));
		}
		
		
		

		// TODO Auto-generated method stub
		
		return OUTPUTDATA;		
	}	

	/**
	 * 单元测试
	 * @param args
	 */
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement queryData  = fac.createOMElement("queryData ", null);
		
		OMElement DATASET = fac.createOMElement("CONTLIST", null);
		OMElement BASEINFO = fac.createOMElement("BASEINFO", null);		
		//查询条件
		OMElement INPUTDATA = fac.createOMElement("INPUTDATA", null);

		DATASET.addChild(BASEINFO);
		DATASET.addChild(INPUTDATA);
		queryData.addChild(DATASET);
		
		LoginVerifyTool tool = new LoginVerifyTool();
				
		tool.addOm(BASEINFO, "BATCHNO", "11");
		tool.addOm(BASEINFO, "SENDDATE", "11");
		tool.addOm(BASEINFO, "SENDTIME", "11");
		tool.addOm(BASEINFO, "MESSAGETYPE", "11");
		
		tool.addOm(INPUTDATA, "CONTNO", "00");
		

		InsuredContDetail t = new InsuredContDetail();
		System.out.println(t.queryData(queryData));

	}
}
