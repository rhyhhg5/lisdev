/**
 * 个人分红信息查询处理类
 * 2012-11-1 BYlicaiyan
 */
package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.topinterface.ListInterface;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;


public class QueryBonusShareInfo extends StandCardHYX {

private String mContNo = "";
	
	public QueryBonusShareInfo() {
		//返回报文根节点
		super("BONUSSHARE");
	}

	protected void getQueryFactor(Map queryFactor) {
		
		//获取条件值  CONTNO
		Iterator factor = queryFactor.keySet().iterator();
		
		while(factor.hasNext())
		{
			String key = factor.next().toString();
			if("CONTNO".equalsIgnoreCase(key))
			{
				mContNo = (String) queryFactor.get(key);
				System.out.println("分红信息查询,contno="+mContNo);
			}
		}
		
	}
	//由保单号查询险种号
	public SSRS getRiskCodeByContNo(){
		String getPolNoSql = "select polno from lcpol where contno='"+mContNo+"'";
		SSRS polnoSSRS = mExeSQL.execSQL(getPolNoSql);
		return polnoSSRS;
	}
	
	public Map prepareData() {
		SSRS polnoSSRS=getRiskCodeByContNo();
		Map OUTPUTDATA = new HashMap();
		for (int i=1;i<=polnoSSRS.MaxRow;i++){
			String polNoStr =  polnoSSRS.GetText(i, 1);
					
				String sql1 = "select polno,fiscalyear,codename('bonusgetmode', bonusflag),sgetdate,bonusmoney from lobonuspol where '1351750663000'='1351750663000' and  polno= '"+polNoStr+"' order by fiscalyear  ";
				SSRS bonusShareSSRS = mExeSQL.execSQL(sql1);
				
				Map bonusInfos = new HashMap();
				Map info = null;
				List item = new ArrayList();
				this.setRecordCount(String.valueOf(bonusShareSSRS.MaxRow));//返回最大行数
				if(bonusShareSSRS.MaxRow > 0){							   //返回状态信息
					String[] errInfoArr = { "00", "0", "成功" };//
					this.setErrInfoArr(errInfoArr);
				}
				for (int j=1;j<=bonusShareSSRS.MaxRow;j++)
				{
					
					info = new HashMap();
					info.put("POLNO", bonusShareSSRS.GetText(j, 1));  //polno 险种号
					info.put("FISCALYEAR", bonusShareSSRS.GetText(j, 2));//红利公布年度
					info.put("BONUSGETMODE", bonusShareSSRS.GetText(j, 3));//红利分配方式
					info.put("SGETDATE", bonusShareSSRS.GetText(j, 4));//红利应分配时间
					info.put("BONUSMONEY", bonusShareSSRS.GetText(j, 5));//分配金额
					item.add(info);
				}
				bonusInfos.put("ITEM", item);
				OUTPUTDATA.put("BONUSINFOS", bonusInfos);
				
				
			}
		return OUTPUTDATA;	
	}	

}
