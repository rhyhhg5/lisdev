package com.ecwebservice.personalquery;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-12 下午04:58:25
 * @Description:
 * @version
 */

public class InsuredContList implements CardHYX {
	private String[] baseInfoString;

	private Set resultSet = new HashSet();

	// baseInfo[0] = batchno;
	// baseInfo[1] = sendDate;
	// baseInfo[2] = sendTime;
	// baseInfo[3] = messageType;

	public OMElement queryData(OMElement Oms) {
		QueryTool qTool = new QueryTool();
		// 获取baseInfo
		baseInfoString = qTool.parseBaseInfo(Oms);
		Map queryStringMap = qTool.parseQueryString(Oms);// 包含查询字段的名称及值
		System.out.println("进入个险--团单分单列表信息处理类！");
		Set queryKeySet = queryStringMap.keySet();
		Iterator queryKeyIterator = queryKeySet.iterator();
		while (queryKeyIterator.hasNext()) {
			Object queryKeyObject = queryKeyIterator.next();
			String queryStringItemValue = queryStringMap.get(queryKeyObject)
					.toString();
			// 获取字段名称
			String queryKeyString = queryKeyObject.toString();

			System.out.println("查询条件为 查询字段名称： " + queryKeyObject.toString()
					+ "   查询字段值为： " + queryStringItemValue.toString());
			if (queryKeyObject.toString().equalsIgnoreCase("CONTNO")) {
				// 拆分合同号
				String[] contNo = queryStringItemValue.split(",");
				for (int i = 0; i < contNo.length; i++) {

					System.out.println("拆分后的contNo： " + contNo[i]);
					StringBuffer querySql = new StringBuffer();
					String frontPartSql = "select c.cvalidate 团单分单生效日期,"
							+ "codename('stateflag', c.stateflag) 分单状态 from  lcgrpcont a,lccont c "
							+ "where  a.grpcontno = c.grpcontno  and ";// 团单下分单标记conttype
																								// =
																								// '2'
					String polSql = "select (select riskname from lmriskapp where riskcode = b.riskcode)险种名称 ,b.amnt 保额 " +
							"from  lcpol b,lccont c where b.contno = c.contno and   ";
					// 险种名称

					querySql.append(frontPartSql);

					String wherePartSql = "";
					// 对每个保单号进行处理
					wherePartSql = " c.contno='" + contNo[i] + "' and ";
					querySql.append(wherePartSql);

					// 测试addWherePart方法
					// String testSql =
					// qTool.addWherePartCutRedundanceString(querySql,
					// wherePartSql);
					// System.out.println("测试addWherePart方法 Sql: " + testSql);

					polSql = polSql.concat(wherePartSql);
					polSql = polSql.substring(0, polSql.length() - 4);

					// 得到查询sql
					String stringQuerySql = querySql.substring(0, querySql
							.length() - 4);
					System.out.println("打印查询sql: " + stringQuerySql);
					System.out.println("打印查询Pol Sql: " + polSql);
					// 执行sql 得到结果集
					ExeSQL tExeSQL = new ExeSQL();
					SSRS contSSRS = tExeSQL.execSQL(stringQuerySql);
					SSRS polSSRS = tExeSQL.execSQL(polSql);

					// 对险种进行循环 2个险种进行测试

					for (int j = 1; j <= polSSRS.MaxRow; j++) {
						Map schemaMap = new HashMap();
						// System.out.println("生成Map " );
						schemaMap.put("CONTNO", contNo[i]);
						if (contSSRS.MaxRow > 0) {
							schemaMap.put("CVALIDATE", contSSRS.GetText(1, 1));
							schemaMap.put("CONTSTATE", contSSRS.GetText(1, 2));
						}

						// 查询险种名称 首先获取riskcode
//						String riskcode = polSSRS.GetText(j, 3);
//						String riskNameSql = "select a.riskname from Lmriskapp a where a.riskcode='"
//								+ riskcode + "'";
//						String riskName = tExeSQL.getOneValue(riskNameSql);
						schemaMap.put("RISKNAME", polSSRS.GetText(j, 1));
						schemaMap.put("AMNT", polSSRS.GetText(j, 2));
						// schemaMap.put("PREM",polSSRS.GetText(j, 2));

						resultSet.add(schemaMap);

					}

				}

			}

		}

		OMElement responseOME = buildResponseOME(resultSet, null, null, null);
		return responseOME;
	}

	// 构建返回报文
	private OMElement buildResponseOME(Set resultSet, String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("INSUREDCONTLIST", null);

		LoginVerifyTool tool = new LoginVerifyTool();
		QueryTool qTool = new QueryTool();
		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString, state,
				errCode, errInfo);

		DATASET.addChild(BASEINFO);// 构建基础信息

		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
		OMElement CLIST = fac.createOMElement("CLIST", null);
		// 遍历HashSet
		Iterator resultIterator = resultSet.iterator();
		while (resultIterator.hasNext()) {
			Map resultSchemaMap = (HashMap) resultIterator.next();
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "CONTNO", (String) resultSchemaMap.get("CONTNO"));
			tool.addOm(ITEM, "CVALIDATE", (String) resultSchemaMap
					.get("CVALIDATE"));
			tool.addOm(ITEM, "CONTSTATE", (String) resultSchemaMap
					.get("CONTSTATE"));

			// OMElement RISKINFO = fac.createOMElement("RISKINFO", null);
			tool.addOm(ITEM, "RISKNAME", (String) resultSchemaMap
					.get("RISKNAME"));
//			tool.addOm(ITEM, "PREM", (String) resultSchemaMap.get("PREM"));
			tool.addOm(ITEM, "AMNT", (String) resultSchemaMap.get("AMNT"));

			// ITEM.addChild(RISKINFO);

			CLIST.addChild(ITEM);

		}

		OUTPUTDATA.addChild(CLIST);
		DATASET.addChild(OUTPUTDATA);

		return DATASET;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
