package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.ListInterface;
import com.sinosoft.lis.pubfun.PubFun;


public class ClaimList extends ListInterface {

	public void prepareCondition() {
		QueryTool qTool = new QueryTool();
		Map queryStringMap = qTool.parseQueryString(this.getOms());
		
		String rgtSql = "select distinct c.caseno,c.customername,c.idno,c.rgtdate,c.rgtstate "
				+" ,c.EndCaseDate " //结案日期 added by zhangyige 2014-08-21 for weixin
				+" ,sum(d.realpay)"//赔付金额
				+ "from llcase c,llclaimdetail d "
				+ "where 1=1   ";
		StringBuffer querySqlBuffer = new StringBuffer();
		querySqlBuffer.append(rgtSql);
		// 查询条件
		String contNo = (String) queryStringMap.get("CONTNO");//保单号
		if (contNo != null && !"".equals(contNo)){
			querySqlBuffer.append("and d.contno='"+contNo+"' and c.caseno=d.caseno");
		}
		
		String clmNo = (String) queryStringMap.get("CLMNO");// 赔案号
		if (clmNo != null && !"".equals(clmNo)) {
			querySqlBuffer.append(" and c.caseno='" + clmNo
					+ "'  ");
		}
		//案件号和保单号同时传空则返回结果为空
		if((clmNo==null||"".equals(clmNo))&&(contNo == null ||"".equals(contNo))){
			querySqlBuffer.append(" and 1=2 ");
		}
		
        // 被保险人姓名
		String tCustomerName = (String) queryStringMap.get("INSUREDNAME");
		
		if (tCustomerName !=null && !"".equals(tCustomerName)) {
			querySqlBuffer.append(" and c.customername='"+tCustomerName+"' ");
		}
		
		// 立案起始日期
		String rgtStartDate = (String) queryStringMap.get("ACCSTARTDATE");
		// 立案结束日期 

		String rgtEndDate = (String) queryStringMap.get("ACCENDDATE");
		if(rgtStartDate == null||"".equals(rgtStartDate)){
//			System.out.println("无起始日期.");
			rgtStartDate = "1900-01-01";
		}
		if(rgtEndDate == null||"".equals(rgtEndDate)){
//			System.out.println("无结束日期.");
			rgtEndDate = PubFun.getCurrentDate();
		}
		
//		if((rgtStartDate != null && !"".equals(rgtStartDate))&&(rgtEndDate != null && !"".equals(rgtEndDate))){
//			
//		}
		querySqlBuffer.append(" and date(c.rgtdate) between '"+rgtStartDate+"' and '"+rgtEndDate+"'");
		querySqlBuffer.append(" group by c.caseno,c.customername,c.idno,c.rgtdate,c.rgtstate ,c.EndCaseDate");
		
		// System.out.println("赔案号为："+clmNo);

		this.setRootTag("CLAIMLIST");
		List returnChildTag = new ArrayList();
		returnChildTag.add("CLMNO");
		returnChildTag.add("INSUREDNAME");
		returnChildTag.add("INSUREDIDNO");
		returnChildTag.add("CLMDATE");
		returnChildTag.add("CLMSTATE");
		returnChildTag.add("CLMDONEDATE");//结案日期 added by zhangyige 2014-08-21 for weixin
		returnChildTag.add("COMPENSATIONAMOUNT");//赔付金额
		this.setChildTagName(returnChildTag);
		// sql
		// String totalRecordsSql =
		// String rgtSql = "select
		// accdate,codename('llrgtstate',c.rgtstate),c.caseno "
		// + "from llsubreport a,llcaserela b,llcase c " + "where 1=1 ";

		setQuerySql(querySqlBuffer.toString());
		this.setOrderCol("rgtdate");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	}

}
