package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.ListInterface;


public class AccountHistoryInherit extends ListInterface {

	public void prepareCondition() {
		QueryTool qTool = new QueryTool();
		Map queryStringMap = qTool.parseQueryString(this.getOms());
		
		String rgtSql = "select (select money from lcinsureacctrace  where othertype='6' and otherno=a.sequenceno and moneytype='LX')  月度收益," +
				"(select money from lcinsureacctrace  where othertype='6' and otherno=a.sequenceno and moneytype='MF')  保单管理费 ," +
				"(select money from lcinsureacctrace  where othertype='6' and otherno=a.sequenceno and moneytype='RP')   风险保费扣费,rundate    结算日期," +
				"(select rate from db2inst1.lminsuaccrate where InsuAccNo=a.InsuAccNo and baladate=a.duebaladate) 结算利率,insuaccbalabefore    起初账户价值 ," +
				"insuaccbalaafter    期末账户价值 from lcinsureaccbalance a where 1=1  ";
		StringBuffer querySqlBuffer = new StringBuffer();
		querySqlBuffer.append(rgtSql);
		// 查询条件
		String contNo = (String) queryStringMap.get("CONTNO");//保单号
		if (contNo != null && !"".equals(contNo)){
			querySqlBuffer.append("and  contno = '"+contNo+"'");
		}
		
		

		this.setRootTag("ACCOUNTHISTORY");
		List returnChildTag = new ArrayList();
		returnChildTag.add("MONTHPROFITS");
		returnChildTag.add("CONTMAINTENANCE");
		returnChildTag.add("RISKPREMREBATE");
		returnChildTag.add("BALANCEDATE");
		returnChildTag.add("BALANCERATE");
		returnChildTag.add("INCEPTCONTVALUE");
		returnChildTag.add("CURRENTCONTVALUE");
		
		this.setChildTagName(returnChildTag);
		

		setQuerySql(querySqlBuffer.toString());
		this.setOrderCol("rundate");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
