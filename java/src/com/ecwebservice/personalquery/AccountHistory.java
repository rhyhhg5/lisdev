package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-12 下午05:11:22
 * @Description:
 * @version
 */

public class AccountHistory extends StandCardHYX {

	private String mContNo = "";

	public AccountHistory() {
		// 返回报文根节点
		super("ACCOUNTHISTORY");
	}

	protected void getQueryFactor(Map queryFactor) {

		// 获取条件值 CONTNO
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();
			if ("CONTNO".equalsIgnoreCase(key)) {
				mContNo = (String) queryFactor.get(key);
				System.out.println("细信息查询,contno=" + mContNo);
			}
		}

	}

	public Map prepareData() {
		QueryTool qTool = new QueryTool();
		Map OUTPUTDATA = new HashMap();
		// 查询业务数据并封装

		

//		String accountHistorySql = "select rundate    结算日期,insuaccbalabefore    起初账户价值 ,insuaccbalaafter    期末账户价值 , sequenceno,"
//				+ "a.InsuAccNo ,a.duebaladate from lcinsureaccbalance a where contno='"
//				+ mContNo + "'";
		String accountHistorySql = " select sum(case tmp.MoneyType when 'LX' then tmp.Money else 0 end) 月度收益, " +
				"sum(case tmp.MoneyType when 'MF' then tmp.Money else 0 end) 保单管理费, " +
				"sum(case tmp.MoneyType when 'RP' then tmp.Money else 0 end) 风险保费扣费, tmp.Rate 结算利率, tmp.InsuAccBalaBefore 起初账户价值, " +
				"tmp.InsuAccBalaAfter  期末账户价值,tmp.rundate 结算日期  " +
				"from ( select lciat.MoneyType, lciat.Money, lmiar.Rate, lciab.InsuAccBalaBefore, lciab.InsuAccBalaAfter ,lciab.rundate " +
				"from LCInsureAccBalance lciab " +
				"inner join LMInsuAccRate lmiar on lciab.InsuAccNo = lmiar.InsuAccNo and lmiar.BalaDate = lciab.DueBalaDate " +
				"inner join LCInsureAccTrace lciat on lciat.OtherNo = lciab.SequenceNo and lciat.OtherType = '6' where 1 = 1 " +
				"and lciab.ContNo = '"+mContNo+"' and lciat.MoneyType in ('LX', 'MF', 'RP') ) as tmp " +
				"group by tmp.Rate, tmp.InsuAccBalaBefore, tmp.InsuAccBalaAfter,tmp.rundate ";
		
		// 记录总数
		String recordsCountSql = "select count(1) from ( "+accountHistorySql+" ) num";
				;
		ExeSQL tExeSQL = new ExeSQL();
		String recordCount = tExeSQL.getOneValue(recordsCountSql);
		this.setRecordCount(recordCount);
		
		String pageindex = this.baseInfoString[4];
		String pageSize = this.baseInfoString[5];
		String pageSql = qTool.buildPageSql(accountHistorySql,
				Integer.parseInt(pageindex), Integer.parseInt(pageSize));
		SSRS riskSSRS = mExeSQL.execSQL(pageSql);

		// Map riskInfos = new HashMap();
		Map info = null;
		List item = new ArrayList();
		if (riskSSRS.MaxRow > 0) {
			String[] errInfoArr = { "00", "0", "成功" };
			this.setErrInfoArr(errInfoArr);

			for (int i = 1; i <= riskSSRS.MaxRow; i++) {
				String sequenceno = riskSSRS.GetText(i, 4);
				String InsuAccNo = riskSSRS.GetText(i, 5);
				String duebaladate = riskSSRS.GetText(i, 6);
				
				// 月收益
//				String monthProfitsSql = "select money from lcinsureacctrace  where othertype='6' and otherno='"+sequenceno+"' and moneytype='LX'";
//				SSRS tSSRS = tExeSQL.execSQL("select money,moneytype from lcinsureacctrace  " +
//						"where othertype='6' and otherno='"+sequenceno+"' and moneytype in ('LX','RP','MF')");
//				
//				String monthProfits = null ;//月度收益
//				String manageFee = null;//管理费
//				String riskPremRebate = null;//风险保费扣费
//				for(int moneyPointer=1;moneyPointer<=tSSRS.MaxRow;moneyPointer++){
//					String moneyType = tSSRS.GetText(moneyPointer, 2);
//					if("LX".equalsIgnoreCase(moneyType)){
//						//月度收益
//						monthProfits = tSSRS.GetText(moneyPointer, 1);
//						continue;
//						
//					}
//					if("MF".equalsIgnoreCase(moneyType)){
//						//管理费
//						manageFee = tSSRS.GetText(moneyPointer, 1);
//						continue;
//					}
//					if("RP".equalsIgnoreCase(moneyType)){
//						//风险保费扣费
//						riskPremRebate = tSSRS.GetText(moneyPointer, 1);
//						continue;
//					}
//				}
//				tSSRS.MaxRow = 0;
//				tSSRS.GetText(1, 2);
//				String monthProfits =  tExeSQL.getOneValue(monthProfitsSql);
				
				//管理费
//				String manageFeeSql = "select money from lcinsureacctrace  where othertype='6' and otherno='"+sequenceno+"' and moneytype='MF'";
				//风险保费扣费
//				String riskPremRebateSql = "select money from lcinsureacctrace  where othertype='6' and otherno='"+sequenceno+"' and moneytype='RP'";
				//结算利率
				String balanceRateSql = "select rate from lminsuaccrate where InsuAccNo='"+InsuAccNo+"' and baladate='"+duebaladate+"'";

				info = new HashMap();
				info.put("MONTHPROFITS",riskSSRS.GetText(i, 1));// 月度收益
				info.put("CONTMAINTENANCE",  riskSSRS.GetText(i, 2));// 保单管理费
				info.put("RISKPREMREBATE", riskSSRS.GetText(i, 3));// 风险保费扣费
				info.put("BALANCEDATE", riskSSRS.GetText(i, 7));// 结算日期
				info.put("BALANCERATE", riskSSRS.GetText(i, 4));// 结算利率

				info.put("INCEPTCONTVALUE", riskSSRS.GetText(i, 5));// 起初账户价值
				info.put("CURRENTCONTVALUE", riskSSRS.GetText(i, 6));// 期末账户价值
				item.add(info);
			}
		}

		OUTPUTDATA.put("ITEM", item);

		return OUTPUTDATA;
	}
	/**
	 * 单元测试
	 * 
	 * @param args
	 */

}
