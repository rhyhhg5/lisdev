package com.ecwebservice.personalquery;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;

/**
 * Title:
 * @author yuanzw
 * 创建时间：2010-6-12 下午03:34:48
 * @Description:
 * @version 
 */

public class CustomerRegister implements CardHYX {
	private String[] baseInfoString;

	// baseInfo[0] = batchno;
	// baseInfo[1] = sendDate;
	// baseInfo[2] = sendTime;
	// baseInfo[3] = messageType;

	public OMElement queryData(OMElement Oms) {
		QueryTool qTool = new QueryTool();
		// 获取baseInfo
		baseInfoString = qTool.parseBaseInfo(Oms);

		Map queryStringMap = qTool.parseQueryString(Oms);// 包含查询字段的名称及值
		System.out.println("开始打印查询条件： ");
		Set queryKeySet = queryStringMap.keySet();
		Iterator queryKeyIterator = queryKeySet.iterator();
		while (queryKeyIterator.hasNext()) {
			Object queryKeyObject = queryKeyIterator.next();
			String queryStringItemValue = queryStringMap.get(queryKeyObject)
					.toString();
			System.out.println("查询条件为 查询字段名称： " + queryKeyObject.toString()
					+ "   查询字段值为： " + queryStringItemValue.toString());

		}
		System.out.println("进入个险--用户注册处理类！");
		OMElement responseOME = buildResponseOME(null,null,null); 
		System.out.println("个险用户注册返回报文： "+responseOME.toString());
		return responseOME;
	}
	
	
	private OMElement buildResponseOME(String state, String errCode,
			String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("CUSTOMERREGISTER", null);
		
		LoginVerifyTool tool = new LoginVerifyTool();
		QueryTool qTool = new QueryTool();
		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString, state, errCode, errInfo);
		
		
		DATASET.addChild(BASEINFO);//构建基础信息
		
		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
		
		tool.addOm(OUTPUTDATA, "CONTNO", ""); 
		
		DATASET.addChild(OUTPUTDATA);
		

		return DATASET;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
