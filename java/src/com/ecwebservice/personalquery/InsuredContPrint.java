
package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

public class InsuredContPrint extends StandCardHYX{
	
	private String mContno = "";
	
	public InsuredContPrint(){
		
		//返回报文根节点
		super("INSUREDCONTPRINT");
	}
	
	protected void getQueryFactor(Map queryFactor){
		
		//获取查询条件值
		Iterator factor = queryFactor.keySet().iterator();
		while(factor.hasNext()){
			String key = factor.next().toString();
			if("CONTNO".equalsIgnoreCase(key)){
				mContno = (String)queryFactor.get(key);
				System.out.println("团单分单个人保险凭证详细信息查询,contNo="+mContno);
			}
		}
	}
	
	public Map prepareData(){
		
		Map OUTPUTDATA = new HashMap();
		
		String sqla = "select d.riskcode 险种代码, " +
					  " ( select riskname from lmriskapp where riskcode = d.riskcode) 险种名称,d.amnt 保额, "  +
					  " lc.dutycode 责任代码,lc.amnt 医疗费用限额,lc.mult 档次, " + 
					  " (select calfactorvalue from LCContPlanDutyParam where grpcontno=d.grpcontno and dutycode=lc.dutycode and contplancode = d.contplancode and calfactor='GetLimit') 免赔额, " +
					  " (select calfactorvalue from LCContPlanDutyParam where grpcontno=d.grpcontno and dutycode=lc.dutycode and contplancode = d.contplancode and calfactor='GetRate' ) 给付比例, " +
					  " lc.prem 保险费, (select dutyname from lmduty where dutycode = lc.dutycode) 责任名称" +
					  " from lcpol d,lcduty lc " +
					  " where lc.polno = d.polno and lc.contno = d.contno and d.contno= '" + mContno + "'";
		SSRS riskSSRS = mExeSQL.execSQL(sqla);
		
		Map riskInfos = new HashMap();
		Map info = null;
		List item = new ArrayList();
		for(int i = 1; i <= riskSSRS.MaxRow; i ++){
			info = new HashMap();
			info.put("RISKCODE", riskSSRS.GetText(i, 1));
			info.put("RISKNAME", riskSSRS.GetText(i, 2));
			info.put("RISKAMNT", riskSSRS.GetText(i, 3));
			info.put("DUTYCODE", riskSSRS.GetText(i, 4));
			info.put("LIMITAMNT", riskSSRS.GetText(i, 5));
			info.put("MULT", riskSSRS.GetText(i, 6));
			info.put("GETLIMIT", riskSSRS.GetText(i, 7));
			info.put("GETRATE", riskSSRS.GetText(i, 8));
			info.put("PREM", riskSSRS.GetText(i, 9));
			info.put("DUTYNAME", riskSSRS.GetText(i, 10));
			item.add(info);
		}
		riskInfos.put("ITEM", item);
		OUTPUTDATA.put("RISKINFO", riskInfos);
		
		String sqlb = "select name,codename('idtype',idtype),idno from lcbnf where contno='" + mContno + "'";
		SSRS bnfSSRS = mExeSQL.execSQL(sqlb);
		Map bnfInfos = new HashMap();
		Map bnfInfo = null;
		List bnfItem = new ArrayList();
		for(int i = 1; i <= bnfSSRS.MaxRow; i ++){
			bnfInfo = new HashMap();
			bnfInfo.put("BNFNAME", bnfSSRS.GetText(i, 1));
			bnfInfo.put("BNFIDTYPE", bnfSSRS.GetText(i, 2));
			bnfInfo.put("BNFIDNO", bnfSSRS.GetText(i, 3));
			bnfItem.add(bnfInfo);
		}
		bnfInfos.put("ITEM", bnfItem);
		OUTPUTDATA.put("BNFINFO", bnfInfos);
		
		String sqlc = "select name,insuredno,codename('idtype',idtype),idno from lcinsured where contno='" + mContno + "'";
		SSRS insuSSRS = mExeSQL.execSQL(sqlc);
		Map insuInfos = new HashMap();
		Map insuInfo = null;
		List insuList = new ArrayList();
		for(int i = 1; i <= insuSSRS.MaxRow; i ++){
			insuInfo = new HashMap();
			insuInfo.put("INSUNAME", insuSSRS.GetText(i, 1));
			insuInfo.put("INSUNO", insuSSRS.GetText(i, 2));
			insuInfo.put("INSUIDTYPE", insuSSRS.GetText(i, 3));
			insuInfo.put("INSUIDNO", insuSSRS.GetText(i, 4));
			insuList.add(insuInfo);
		}
		insuInfos.put("ITEM", insuList);
		OUTPUTDATA.put("INSULIST", insuInfos);
		
		String sqld = "select a.grpcontno,a.cvalidate 团单生效日期,a.grpname 投保人姓名,dc.name,dc.address,dc.zipcode, " +
					" a.agentcom,(select name from lacom where agentcom = a.agentcom ) 代理机构名称, " +
					" a.agentcode,(select name from laagent where agentcode = a.agentcode) 代理人名称, " +
					" c.contno 团单分单号,c.cvalidate 团单分单生效日期,codename('paymode',c.paymode) 缴费方式,c.signdate 签单日期,c.cinvalidate 失效日期, " +
					" (select min(confmakedate) from ljtempfee where 1=1 and otherno=c.grpcontno and tempfeetype='1') 收费确认日期 " +
					" from  lcgrpcont a,lccont c,ldcom dc " +
					" where  a.grpcontno = c.grpcontno and left(a.managecom,4) = dc.comcode and c.contno='" + mContno + "'";
		SSRS contSSRS = mExeSQL.execSQL(sqld);
		if(contSSRS.MaxRow>0){
			String[] errInfoArr = {"00","0","成功"};
			this.setErrInfoArr(errInfoArr);
			OUTPUTDATA.put("GRPCONTNO", contSSRS.GetText(1, 1));
			OUTPUTDATA.put("GRPCVALIDATE", contSSRS.GetText(1, 2));
			OUTPUTDATA.put("APPNAME", contSSRS.GetText(1, 3));
			OUTPUTDATA.put("MANAGECOMNAME", contSSRS.GetText(1, 4));
			OUTPUTDATA.put("MANAGECOMADDR", contSSRS.GetText(1, 5));
			OUTPUTDATA.put("ZIPCODE", contSSRS.GetText(1, 6));
			OUTPUTDATA.put("AGENTCOM", contSSRS.GetText(1, 7));
			OUTPUTDATA.put("AGENTCOMNAME", contSSRS.GetText(1, 8));
			OUTPUTDATA.put("AGENTCODE", contSSRS.GetText(1, 9));
			OUTPUTDATA.put("AGENTCODENAME", contSSRS.GetText(1, 10));
			OUTPUTDATA.put("CONTNO", contSSRS.GetText(1, 11));
			OUTPUTDATA.put("CVALIDATE", contSSRS.GetText(1, 12));
			OUTPUTDATA.put("PAYMODE", contSSRS.GetText(1, 13));
			OUTPUTDATA.put("SIGNDATE", contSSRS.GetText(1, 14));
			OUTPUTDATA.put("ENDDATE", contSSRS.GetText(1, 15));
			OUTPUTDATA.put("FIRSTPAYDATE", contSSRS.GetText(1, 16));
		}
		
		return OUTPUTDATA;
	}
}