package com.ecwebservice.personalquery;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.ExeSQL;


/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-12 下午05:11:22
 * @Description:
 * @version
 */

public class AccountValue extends StandCardHYX {

	private String mContNo = "";

	public AccountValue() {
		// 返回报文根节点
		super("ACCOUNTVALUE");
	}

	protected void getQueryFactor(Map queryFactor) {

		// 获取条件值 CONTNO
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();
			if ("CONTNO".equalsIgnoreCase(key)) {
				mContNo = (String) queryFactor.get(key);
				System.out.println("个人保单账户价值信息查询,contno=" + mContNo);
			}
		}

	}

	public Map prepareData() {

		Map OUTPUTDATA = new HashMap();
		// 查询业务数据并封装
		ExeSQL tExeSQL = new ExeSQL();
		//账户名称
//		String accountNameSql = "select riskname from lmrisk where riskcode in (select riskcode from lcpol where contno='"
//				+ mContNo + "' and polno=mainpolno and riskcode='4') ";
		String accountNameSql = "select insuaccname from lmrisktoacc where insuaccno in  (select insuaccno from lcinsureacc where contno='"+mContNo+"')";
		String accountName = tExeSQL.getOneValue(accountNameSql);
		
		//管理费用
		String manageFeeSql = "select fee from lcinsureaccfeetrace where contno='"+mContNo+"' and moneytype='GL'";
		String manageFee = tExeSQL.getOneValue(manageFeeSql);
		
		//最新保单价值
		String diggContValueSql = "select sum(insuaccbala) from lcinsureacc where contno='"+mContNo+"' ";
		String diggContValue = tExeSQL.getOneValue(diggContValueSql);
		
		//起初保单价值
		String inceptionContValueSql = "select money from lcinsureacctrace where contno='"+mContNo+"'  and moneytype='BF' " +
				"order by makedate  fetch first 1 row only";
		String inceptionContValue = tExeSQL.getOneValue(inceptionContValueSql);

		

		OUTPUTDATA.put("ACCOUNTNAME", accountName);
		OUTPUTDATA.put("CREATEPAY", manageFee);
		OUTPUTDATA.put("CONTNEWVALUE", diggContValue);
		OUTPUTDATA.put("CONTSTARTVALUE", inceptionContValue);
		
		String[] errInfoArr = { "00", "0", "成功" };
		this.setErrInfoArr(errInfoArr);

		

		return OUTPUTDATA;
	}

	/**
	 * 单元测试
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement queryData = fac.createOMElement("queryData ", null);

		OMElement DATASET = fac.createOMElement("CONTLIST", null);
		OMElement BASEINFO = fac.createOMElement("BASEINFO", null);
		// 查询条件
		OMElement INPUTDATA = fac.createOMElement("INPUTDATA", null);

		DATASET.addChild(BASEINFO);
		DATASET.addChild(INPUTDATA);
		queryData.addChild(DATASET);

		LoginVerifyTool tool = new LoginVerifyTool();

		tool.addOm(BASEINFO, "BATCHNO", "11");
		tool.addOm(BASEINFO, "SENDDATE", "11");
		tool.addOm(BASEINFO, "SENDTIME", "11");
		tool.addOm(BASEINFO, "MESSAGETYPE", "11");

		tool.addOm(INPUTDATA, "CONTNO", "00");

		AccountValue t = new AccountValue();
		System.out.println(t.queryData(queryData));

	}
}
