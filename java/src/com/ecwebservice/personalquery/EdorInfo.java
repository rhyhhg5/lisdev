package com.ecwebservice.personalquery;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class EdorInfo implements CardHYX {
	private String[] baseInfoString;

	// baseInfo[0] = batchno;
	// baseInfo[1] = sendDate;
	// baseInfo[2] = sendTime;
	// baseInfo[3] = messageType;
	// 封装结果集
	private Set resultSet = new HashSet();

	private String contNo;

	private String edorCount;

	public OMElement queryData(OMElement Oms) {
		System.out.println("进入个险保全！");
		OMElement responseOME = null;

		QueryTool qTool = new QueryTool();
		baseInfoString = qTool.parseBaseInfo(Oms);
		contNo = qTool.getContNoInQueryString(Oms);
		System.out.println("保单号为：" + contNo);
		ExeSQL tExeSQL = new ExeSQL();
		int pageIndex = Integer.parseInt(baseInfoString[4]);
		int pageSize = Integer.parseInt(baseInfoString[5]);
		int startRow = (pageIndex - 1) * pageSize + 1;
		int endRow = pageIndex * pageSize;
		String edorCountSql = "select count(*) "
				+ "from lpedoritem a ,lgwork b where a.edoracceptno = b.workno and a.contno = '"+contNo+"'";
		edorCount = tExeSQL.getOneValue(edorCountSql);
		String edorSql = "SELECT * FROM (select a.edoracceptno ,b.applyname ,a.edorappdate ,a.edorvalidate ,"
				+ "codename('applytypeno',b.ApplyTypeNo) ,codename('appedorstate',a.EdorState)  批改状态,a.EdorType 批改类型, rownumber() over(ORDER BY a.edoracceptno ASC) AS rn "
				+ "from lpedoritem a ,lgwork b where a.edoracceptno = b.workno and a.contno = '"
				+ contNo
				+ "' ) AS a1 "
				+ "WHERE a1.rn BETWEEN "
				+ startRow
				+ " AND " + endRow;
		SSRS edorSSRS = tExeSQL.execSQL(edorSql);
		boolean queryFlag = false;
		if (edorSSRS.MaxRow > 0) {
			queryFlag = true;
			for (int j = 1; j <= edorSSRS.MaxRow; j++) {
				Map schemaMap = new HashMap();
				schemaMap.put("PRESERVENO", edorSSRS.GetText(j, 1));// 保全批单号
				schemaMap.put("APPLYNAME", edorSSRS.GetText(j, 2));

				schemaMap.put("APPLYDATE", edorSSRS.GetText(j, 3));
				schemaMap.put("CVALIDATE", edorSSRS.GetText(j, 4));
				schemaMap.put("APPLYTYPE", edorSSRS.GetText(j, 5));
				
				schemaMap.put("EDORSTATE", edorSSRS.GetText(j, 6));
				
//				获取批改类型名称 作为CAUSE
				String edorTypeName = qTool.decodeEdorType(edorSSRS.GetText(j, 7));
				schemaMap.put("CAUSE", edorTypeName);

				resultSet.add(schemaMap);
			}
		}
		if (queryFlag) {

			responseOME = buildResponseOME(resultSet, "00", "0", "成功");
		}else{
			responseOME = buildResponseOME(resultSet, "00", "*", "查询失败");
		}

		System.out.println("返回报文："+responseOME.toString());
		return responseOME;
	}

	private OMElement buildResponseOME(Set resultSet, String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("EDORINFO", null);

		LoginVerifyTool tool = new LoginVerifyTool();
		QueryTool qTool = new QueryTool();
		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString,
				edorCount, state, errCode, errInfo);

		DATASET.addChild(BASEINFO);// 构建基础信息

		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
		OMElement EDORLIST = fac.createOMElement("EDORLIST", null);

		// 遍历HashSet
		Iterator resultIterator = resultSet.iterator();
		while (resultIterator.hasNext()) {
			Map resultSchemaMap = (HashMap) resultIterator.next();
			OMElement ITEM = fac.createOMElement("ITEM", null);
			tool.addOm(ITEM, "PRESERVENO", (String) resultSchemaMap
					.get("PRESERVENO"));
			tool.addOm(ITEM, "APPLYNAME", (String) resultSchemaMap
					.get("APPLYNAME"));
			tool.addOm(ITEM, "APPLYDATE", (String) resultSchemaMap
					.get("APPLYDATE"));
			tool.addOm(ITEM, "CVALIDATE", (String) resultSchemaMap
					.get("CVALIDATE"));

//			tool.addOm(ITEM, "POLICYNO", "");

			tool.addOm(ITEM, "CAUSE", (String) resultSchemaMap
					.get("CAUSE"));
			tool.addOm(ITEM, "APPLYTYPE", (String) resultSchemaMap
					.get("APPLYTYPE"));
			tool.addOm(ITEM, "EDORSTATE", (String) resultSchemaMap
					.get("EDORSTATE"));

			// OMElement RISKINFO = fac.createOMElement("RISKINFO", null);
			// tool.addOm(RISKINFO, "RISKNAME", "");
			// tool.addOm(RISKINFO, "PREM", "");
			// tool.addOm(RISKINFO, "AMNT", "");
			//
			// ITEM.addChild(RISKINFO);

			EDORLIST.addChild(ITEM);
		}

		OUTPUTDATA.addChild(EDORLIST);
		DATASET.addChild(OUTPUTDATA);

		return DATASET;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
