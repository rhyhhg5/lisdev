package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-12 下午05:25:29
 * @Description:
 * @version
 */

public class InsuredEdorInfo extends StandCardHYX {
	private String mContNo = "";

	public InsuredEdorInfo() {
		// 返回报文根节点
		super("InsuredEdorInfo");
	}

	protected void getQueryFactor(Map queryFactor) {

		// 获取条件值 CONTNO
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();
			if ("CONTNO".equalsIgnoreCase(key)) {
				mContNo = (String) queryFactor.get(key);
				System.out.println("团单分单保全信息查询,contno=" + mContNo);
			}
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Map prepareData() {
		QueryTool qTool = new QueryTool();

		Map OUTPUTDATA = new HashMap();
		Map EDORLIST = new HashMap();
		List item = new ArrayList();
		Map info = null;
		String sql = "select a.edoracceptno 保全批单号,b.applyname 申请人姓名,b.acceptdate  申请时间,"
				+ "a.edorvalidate 生效时间,'没有' 保单代码,'没有' 申请人证件类型,'没有' 申请人证件号码,'没有' 保全变更原因,"
				+ "codename('applytypeno', b.ApplyTypeNo) 申请人类型,"
				+ " codename('appedorstate',a.EdorState) 保全状态"
				//modify by zhangyige at 2014-08-19  微信项目查询增加 保全项目
				+ ",(select edorname from lmedoritem where appobj='I' and edorcode= a.EdorType)"
				+ "from lpedoritem a, lgwork b where a.edoracceptno = b.workno  and a.contno = '"
				+ mContNo + "'";

		SSRS contSSRS = mExeSQL.execSQL(sql);
		//传入记录条数
		this.setRecordCount(String.valueOf(contSSRS.MaxRow));
		if(contSSRS.MaxRow>0){
			String[] errInfoArr = {"00","0","成功"};
			this.setErrInfoArr(errInfoArr);
		}
		for (int i = 1; i <= contSSRS.MaxRow; i++) {
			info = new HashMap();

			info.put("PRESERVENO", contSSRS.GetText(i, 1));
			info.put("APPLYNAME", contSSRS.GetText(i, 2));
			info.put("APPLYDATE", contSSRS.GetText(i, 3));
			info.put("CVALIDATE", contSSRS.GetText(i, 4));
//			info.put("POLICYNO", mContNo);
			// info.put("APPLYIDTYPE", contSSRS.GetText(i, 6));
			// info.put("APPLYIDNO", contSSRS.GetText(i, 7));
			//获取批改类型名称 作为CAUSE
			String edorTypeName = qTool.decodeEdorType(contSSRS.GetText(i, 11));
			
			info.put("CAUSE", edorTypeName);
			info.put("APPLYTYPE", contSSRS.GetText(i, 9));
			info.put("EDORSTATE", contSSRS.GetText(i, 10));
			info.put("EDORTYPE", contSSRS.GetText(i, 11));

			item.add(info);
		}
		EDORLIST.put("ITEM", item);
		OUTPUTDATA.put("EDORLIST", EDORLIST);
		// 查询业务数据并封装
		return OUTPUTDATA;
	}

}
