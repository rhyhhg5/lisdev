package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class PerQueryListForCS extends StandCardHYX{

	private String mContNo = "";
	private String name="";
	private String idno = "";
	private String customNo = "";
	private String pageIndex = null;
	private String pageSize=null;

	public PerQueryListForCS() {
		// 返回报文根节点
		super("PERQUERYLISTFORCS");
	}

	protected void getQueryFactor(Map queryFactor) {

		// 获取条件值 CONTNO
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();
			if ("CONTNO".equalsIgnoreCase(key)) {
				mContNo = (String) queryFactor.get(key);
			}
			
			if ("NAME".equalsIgnoreCase(key)) {
				name = (String) queryFactor.get(key);
			}
			
			if ("IDNO".equalsIgnoreCase(key)) {
				idno = (String) queryFactor.get(key);
			}
			
			if ("CUSTOMNO".equalsIgnoreCase(key)) {
				customNo = (String) queryFactor.get(key);
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Map prepareData() {
		QueryTool qTool = new QueryTool();

		Map OUTPUTDATA = new HashMap();
		Map EDORLIST = new HashMap();
		List item = new ArrayList();
		Map info = null;
		

		pageIndex =baseInfoString[4];
		pageSize=baseInfoString[5];
		
		
		StringBuffer sql = new StringBuffer();
		StringBuffer numSQL = new StringBuffer();
		//111
		sql.append("select distinct cs.contno,cs.conttype,cs.cvalidate,cs.flag,cs.appname,cs.id " +
				"from (select lc.contno, " +
				"codename('conttype',lc.conttype) conttype, " +
				"lc.cvalidate ," +
				"codename('stateflag',lc.stateflag) flag," +
				"lca.appntname appname,"+
				"lca.idno id " +
				"from lccont lc " +
				"inner join lcinsured lci on lc.contno=lci.contno " +
				"left join lcappnt lca on lca.contno = lc.contno " +
				"where 1=1 ");
		numSQL.append("select distinct count(1) " +
				"from (select lc.contno, " +
				"codename('conttype',lc.conttype) conttype, " +
				"lc.cvalidate ,codename('stateflag',lc.stateflag) flag " +
				"from lccont lc " +
				"inner join lcinsured lci on lc.contno=lci.contno " +
				"left join lcappnt lca on lca.contno = lc.contno " +
				"where 1=1 ");
				
			if (mContNo != null && !"".equals(mContNo)){
				sql.append("and  lci.contno = '"+mContNo+"' ");
				numSQL.append("and  lci.contno = '"+mContNo+"' ");
			}		
			if (name != null && !"".equals(name)){
				sql.append(" and lci.name= '"+name+"' ");
				numSQL.append(" and lci.name= '"+name+"' ");
			}
			if (idno != null && !"".equals(idno)){
				sql.append(" and lci.idno= '"+idno+"' ");
				numSQL.append(" and lci.idno= '"+idno+"' ");
			}
			if (customNo != null && !"".equals(customNo)){
				sql.append(" and lci.InsuredNo = '"+customNo+"' ");
				numSQL.append(" and lci.InsuredNo = '"+customNo+"' ");
			}
			
		sql.append(" union select lc.contno, " +
					"codename('conttype',lc.conttype) conttype, " +
					"lc.cvalidate ," +
					"codename('stateflag',lc.stateflag) flag," +
					"lca.appntname appname,"+
					"lca.idno id " +
					"from lccont lc " +
					"inner join lcinsured lci on lc.contno=lci.contno " +
					"left join lcappnt lca on lca.contno = lc.contno " +
					"where 1=1 ");
		numSQL.append(" union select lc.contno, " +
					"codename('conttype',lc.conttype) conttype, " +
					"lc.cvalidate ,codename('stateflag',lc.stateflag) flag " +
					"from lccont lc " +
					"inner join lcinsured lci on lc.contno=lci.contno " +
					"left join lcappnt lca on lca.contno = lc.contno " +
					"where 1=1 ");
					
				if (mContNo != null && !"".equals(mContNo)){
					sql.append("and  lci.contno = '"+mContNo+"' ");
					numSQL.append("and  lci.contno = '"+mContNo+"' ");
				}		
				if (name != null && !"".equals(name)){
					sql.append(" and lci.name= '"+name+"' ");
					numSQL.append(" and lci.name= '"+name+"' ");
				}
				if (idno != null && !"".equals(idno)){
					sql.append(" and lci.idno= '"+idno+"' ");
					numSQL.append(" and lci.idno= '"+idno+"' ");
				}
				if (customNo != null && !"".equals(customNo)){
					sql.append(" and lca.AppntNo  = '"+customNo+"' ");
					numSQL.append(" and lca.AppntNo  = '"+customNo+"' ");
				}
		
		//22
		sql.append(" union select lc.contno, " +
				"codename('conttype',lc.conttype) conttype, " +
				"lc.cvalidate ," +
				"codename('stateflag',lc.stateflag) flag, " +
				"lca.appntname appname,"+
				"lca.idno id " +
				"from lccont lc " +
				"inner join lcappnt lca on lca.contno= lc.contno " +
				"left join lcinsured lci on lc.contno=lci.contno " +
				"where 1=1 ");
		numSQL.append(" union select lc.contno, " +
				"codename('conttype',lc.conttype) conttype, " +
				"lc.cvalidate ," +
				"codename('stateflag',lc.stateflag) flag " +
				"from lccont lc " +
				"inner join lcappnt lca on lca.contno= lc.contno " +
				"left join lcinsured lci on lc.contno=lci.contno " +
				"where 1=1 ");
			if (mContNo != null && !"".equals(mContNo)){
				sql.append("and  lca.contno = '"+mContNo+"' ");
				numSQL.append("and  lca.contno = '"+mContNo+"' ");
			}		
			if (name != null && !"".equals(name)){
				sql.append(" and lca.appntname= '"+name+"' ");
				numSQL.append(" and lca.appntname= '"+name+"' ");
			}
			if (idno != null && !"".equals(idno)){
				sql.append(" and lca.idno= '"+idno+"' ");
				numSQL.append(" and lca.idno= '"+idno+"' ");
			}
			if (customNo != null && !"".equals(customNo)){
				sql.append(" and lci.InsuredNo = '"+customNo+"' ");
				numSQL.append(" and lci.InsuredNo = '"+customNo+"' ");
			}
			
			sql.append(" union select lc.contno, " +
					"codename('conttype',lc.conttype) conttype, " +
					"lc.cvalidate ," +
					"codename('stateflag',lc.stateflag) flag, " +
					"lca.appntname appname,"+
					"lca.idno id " +
					"from lccont lc inner join lcappnt lca on lca.contno= lc.contno where 1=1 ");
			numSQL.append(" union select lc.contno, " +
					"codename('conttype',lc.conttype) conttype, " +
					"lc.cvalidate ," +
					"codename('stateflag',lc.stateflag) flag " +
					"from lccont lc inner join lcappnt lca on lca.contno= lc.contno where 1=1 ");
				if (mContNo != null && !"".equals(mContNo)){
					sql.append("and  lca.contno = '"+mContNo+"' ");
					numSQL.append("and  lca.contno = '"+mContNo+"' ");
				}		
				if (name != null && !"".equals(name)){
					sql.append(" and lca.appntname= '"+name+"' ");
					numSQL.append(" and lca.appntname= '"+name+"' ");
				}
				if (idno != null && !"".equals(idno)){
					sql.append(" and lca.idno= '"+idno+"' ");
					numSQL.append(" and lca.idno= '"+idno+"' ");
				}
				if (customNo != null && !"".equals(customNo)){
					sql.append(" and lca.AppntNo  = '"+customNo+"' ");
					numSQL.append(" and lca.AppntNo  = '"+customNo+"' ");
				}
			//33
		sql.append(" union select lc.contno, " +
				"codename('conttype',lc.conttype) conttype, " +
				"lc.cvalidate ," +
				"codename('stateflag',lc.stateflag) flag," +
				"lca.appntname appname,"+
				" lca.idno id " +
				"from lccont lc " +
				"inner join lcbnf lcb on lc.contno=lcb.contno " +
				"left join lcappnt lca on lca.contno = lc.contno " +
				"left join lcinsured lci on lc.contno=lci.contno " +
				"where 1=1 ");
		numSQL.append(" union select lc.contno, " +
				"codename('conttype',lc.conttype) conttype, " +
				"lc.cvalidate ," +
				"codename('stateflag',lc.stateflag) flag " +
				"from lccont lc " +
				"inner join lcbnf lcb on lc.contno=lcb.contno " +
				"left join lcappnt lca on lca.contno = lc.contno " +
				"left join lcinsured lci on lc.contno=lci.contno " +
				"where 1=1 ");
			if (mContNo != null && !"".equals(mContNo)){
				sql.append(" and  lcb.contno = '"+mContNo+"' ");
				numSQL.append(" and  lcb.contno = '"+mContNo+"' ");
			}		
			if (name != null && !"".equals(name)){
				sql.append(" and lcb.name= '"+name+"' ");
				numSQL.append(" and lcb.name= '"+name+"' ");
			}
			if (idno != null && !"".equals(idno)){
				sql.append(" and lcb.idno= '"+idno+"' ");
				numSQL.append(" and lcb.idno= '"+idno+"' ");
			}
			if (customNo != null && !"".equals(customNo)){
				sql.append(" and lci.InsuredNo = '"+customNo+"' ");
				numSQL.append(" and lci.InsuredNo = '"+customNo+"' ");
			}
			
			sql.append(" union select lc.contno, " +
					"codename('conttype',lc.conttype) conttype, " +
					"lc.cvalidate ," +
					"codename('stateflag',lc.stateflag) flag," +
					"lca.appntname appname,"+
					" lca.idno id " +
					"from lccont lc " +
					"inner join lcbnf lcb on lc.contno=lcb.contno " +
					"left join lcappnt lca on lca.contno = lc.contno " +
					"where 1=1 ");
			numSQL.append(" union select lc.contno, " +
					"codename('conttype',lc.conttype) conttype, " +
					"lc.cvalidate ," +
					"codename('stateflag',lc.stateflag) flag " +
					"from lccont lc " +
					"inner join lcbnf lcb on lc.contno=lcb.contno " +
					"left join lcappnt lca on lca.contno = lc.contno " +
					"where 1=1 ");
				if (mContNo != null && !"".equals(mContNo)){
					sql.append(" and  lcb.contno = '"+mContNo+"' ");
					numSQL.append(" and  lcb.contno = '"+mContNo+"' ");
				}		
				if (name != null && !"".equals(name)){
					sql.append(" and lcb.name= '"+name+"' ");
					numSQL.append(" and lcb.name= '"+name+"' ");
				}
				if (idno != null && !"".equals(idno)){
					sql.append(" and lcb.idno= '"+idno+"' ");
					numSQL.append(" and lcb.idno= '"+idno+"' ");
				}
				if (customNo != null && !"".equals(customNo)){
					sql.append(" and lca.AppntNo  = '"+customNo+"' ");
					numSQL.append(" and lca.AppntNo  = '"+customNo+"' ");
				}
		sql.append(" ) cs order by cs.conttype");
		numSQL.append(" ) cs ");
		String num = new ExeSQL().getOneValue(numSQL.toString());
		String pageSQL=qTool.buildPageSql(sql.toString(),Integer.valueOf(pageIndex).intValue(),Integer.valueOf(pageSize).intValue());
		SSRS contSSRS = mExeSQL.execSQL(pageSQL);
		//传入记录条数
		this.setRecordCount(String.valueOf(num));
		if(contSSRS.MaxRow>0){
			String[] errInfoArr = {"00","0","成功"};
			this.setErrInfoArr(errInfoArr);
		}
		for (int i = 1; i <= contSSRS.MaxRow; i++) {
			info = new HashMap();

			info.put("CONTNO", contSSRS.GetText(i, 1));
			info.put("CONTTYPE", contSSRS.GetText(i, 2));
			info.put("CVALIDATE", contSSRS.GetText(i, 3));	
			info.put("CONTSTATE", contSSRS.GetText(i, 4));
			info.put("APPNAME", contSSRS.GetText(i, 5));
			info.put("APPIDNO", contSSRS.GetText(i, 6));
			item.add(info);
		}
		EDORLIST.put("ITEM", item);
		OUTPUTDATA.put("CLIST", EDORLIST);
		// 查询业务数据并封装
		return OUTPUTDATA;
	}
}
