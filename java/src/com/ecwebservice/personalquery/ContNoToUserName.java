package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-12 下午03:48:37
 * @Description:使用5要素+保单号来进行校验
 * @version
 */

public class ContNoToUserName implements CardHYX {
	private String[] baseInfoString;
	private String[] inputInfoString;
	private String rootTag;
	String errors = "";

	public OMElement queryData(OMElement Oms) {
		System.out.println("进入个险--保单号手动绑定到用户名处理类！");
		OMElement responseOME ;
		QueryTool qTool = new QueryTool();
		rootTag = qTool.getRootTag(Oms);
		// 获取baseInfo
		baseInfoString = qTool.parseBaseInfo(Oms);
		Map contNoAndTypeMap = new HashMap();
		if (rootTag.startsWith("GRP")) {
			// 处理团险绑定
			System.out.println("处理团险绑定！");
			contNoAndTypeMap = qTool.grpBindToContNo(Oms);

		} else {
			// 个险绑定
			System.out.println("处理个险绑定！");
			if("AUTOCONTNOTOUSERNAME".equals(rootTag)){//个单自动保定，报文中无contno，证件类型必须为身份证
				errors = this.perAUTOCheck(Oms);
				if(!errors.equals("")){
					responseOME = buildResponseOME(contNoAndTypeMap, "01", "*",errors);
				}else{
					contNoAndTypeMap = qTool.bindToAUTOContNoOtherWay(Oms);
				}
			}else{
				errors = this.perCheck(Oms);
				if(!errors.equals("")){
					responseOME = buildResponseOME(contNoAndTypeMap, "01", "*",errors);
				}else{
					contNoAndTypeMap = qTool.bindToContNoOtherWay(Oms);
				}
			}
			System.out.println(errors+"=========errors");
		}
		// 获取校验的where part sql
		// String wherePartSql = qTool.buildWherePartSqlByOme(Oms);
		// System.out.println("获取校验的where part sql！"+wherePartSql);
		if(contNoAndTypeMap.size()>0){
			 responseOME = buildResponseOME(contNoAndTypeMap, "00", "0",
				"成功");
		}else{
			 responseOME = buildResponseOME(contNoAndTypeMap, "01", "*",
					 errors);
		}
		
		System.out.println("返回报文："+responseOME.toString());
		return responseOME;
	}

	private OMElement buildResponseOME(Map contNoAndTypeMap, String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement(rootTag, null);

		LoginVerifyTool tool = new LoginVerifyTool();
		QueryTool qTool = new QueryTool();

		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
		String contNo = (String)contNoAndTypeMap.get("contNo");
		String contType = (String)contNoAndTypeMap.get("contType");
		String flag = (String)contNoAndTypeMap.get("flag");
//		Set contNoKeySet = contNoAndTypeMap.keySet();
//		Iterator contNoKeyIterator = contNoKeySet.iterator();
//		StringBuffer contNoStringBuff = new StringBuffer();
//		while (contNoKeyIterator.hasNext()) {
//			String contNoKeyIteratorObject = (String) contNoKeyIterator.next();// 保单号
			// 如果存在Map中存在保单号 则绑定成功
			// if(contNoKeyIteratorObject!=null&&!"".equals(contNoKeyIteratorObject)){
			// contNoStringBuff.append(contNoKeyIteratorObject+",");
			// state = "0";
			// errCode = "00";
			// errInfo = "成功";
			//				
			// }

//			String contType = (String) contNoAndTypeMap
//					.get(contNoKeyIteratorObject);
			OMElement ITEM = fac.createOMElement("ITEM", null);
			
			String contNoLabel;
			if (rootTag.startsWith("GRP")){
				contNoLabel = "GRPCONTNO";
			}else{
				contNoLabel = "CONTNO";
			}
			
			tool.addOm(ITEM, contNoLabel, contNo);
			tool.addOm(ITEM, "CONTTYPE", contType);
			tool.addOm(ITEM, "FLAG", flag);
				
			OUTPUTDATA.addChild(ITEM);
//		}

		DATASET.addChild(OUTPUTDATA);

		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString, state,
				errCode, errInfo);

		DATASET.addChild(BASEINFO);// 构建基础信息

		return DATASET;
	}
// date 20101126 by gzh
	private String perCheck(OMElement Oms){
		String ErrInfo = "";
		QueryTool qTool1 = new QueryTool();
		inputInfoString = qTool1.parseIputInfo(Oms);
		//检查团单号是否存在
		String grpcontnosql = "select prtno from lcgrpcont where grpcontno = '"+inputInfoString[0]+"' with ur";
		System.out.println("团单是否存在此保单："+grpcontnosql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS GrpContnoSSRS = tExeSQL.execSQL(grpcontnosql);
		
		//校验保单是否存在
		String contnosql = "select * from lccont where contno = '"+inputInfoString[0]+"' with ur";
		System.out.println("个单是否存在此保单："+contnosql);
		SSRS ContnoSSRS = tExeSQL.execSQL(contnosql);
		if(GrpContnoSSRS.MaxRow>0){
			System.out.println("该保单是团单单号");
			//根据团单号查询出所有的分单号信息
			String InsuredSql = "select name,sex,idtype,idno,birthday from lcinsured where grpcontno = '"+inputInfoString[0]+"' and " +
					"name ='"+ inputInfoString[1] +"' and sex='"+ inputInfoString[2]+"' and idtype='"+ inputInfoString[3] +"' and " +
					" idno='"+ inputInfoString[4] +"' and birthday= '"+ inputInfoString[5] +"' union select name,sex,idtype,idno," +
					"birthday from lbinsured where grpcontno = '"+inputInfoString[0]+"' and name ='"+ inputInfoString[1] +"' " +
					"and sex='"+ inputInfoString[2]+"' and idtype='"+ inputInfoString[3] +"' and " +" idno='"+ inputInfoString[4] +"' " +
							"and birthday= '"+ inputInfoString[5] +"'";
			System.out.println("获取被保人信息："+InsuredSql);
			//获取受益人信息
			String LcbnfSql = "select name,sex,idtype,idno,birthday from lcbnf a where exists (select 1 from lcinsured where " +
					"contno = a.contno and insuredno = a.insuredno and grpcontno = '"+ inputInfoString[0] +"')  and name ='"+ inputInfoString[1] +"' " +
							"and sex='"+ inputInfoString[2]+"' and idtype='"+ inputInfoString[3] +"' and  idno='"+ inputInfoString[4] +"' " +
							"and birthday= '"+ inputInfoString[5] +"' union select name,sex,idtype,idno,birthday from lbbnf b" +
							" where exists (select 1 from lbinsured where contno = b.contno and insuredno = b.insuredno " +
							"and grpcontno = '"+ inputInfoString[0] +"') and name ='"+ inputInfoString[1] +"' and sex='"+ inputInfoString[2]+"' " +
							"and idtype='"+ inputInfoString[3] +"' and " +" idno='"+ inputInfoString[4] +"' and" +
									" birthday= '"+ inputInfoString[5] +"'";
			System.out.println("获取受益人信息："+LcbnfSql);
			SSRS InsuredSSRS = tExeSQL.execSQL(InsuredSql);
			SSRS LcbnfSSRS = tExeSQL.execSQL(LcbnfSql);
			//当传来的contNo是团单号
			if(InsuredSSRS.MaxRow<=0||!(LcbnfSSRS.MaxRow<=0)){
				
				ErrInfo="保单"+inputInfoString[0]+"对应被保人的信息不正确！";	
			}
		}else if(ContnoSSRS.MaxRow>0){ //不是团单
			System.out.println("该单号是个单单号");
//			获取被保人信息
			String AppntSql = "select appntname,appntsex,idtype,idno,appntbirthday from lcappnt where contno = '"+inputInfoString[0]+"' union "+
			                  "select appntname,appntsex,idtype,idno,appntbirthday from lbappnt where contno = '"+inputInfoString[0]+"'";
			System.out.println("获取被保人信息："+AppntSql);
//			获取投保人信息
			String InsuredSql = "select name,sex,idtype,idno,birthday from lcinsured where contno = '"+inputInfoString[0]+"' union "+
			                 "select name,sex,idtype,idno,birthday from lbinsured where contno = '"+inputInfoString[0]+"' ";
			System.out.println("获取投保人信息："+InsuredSql);
//			获取受益人信息
			String LcbnfSql = "select name,sex,idtype,idno,birthday from lcbnf where contno = '"+inputInfoString[0]+"' union "+
			                  "select name,sex,idtype,idno,birthday from lbbnf where contno = '"+inputInfoString[0]+"'";
			System.out.println("获取受益人信息："+LcbnfSql);
			SSRS AppntSSRS = tExeSQL.execSQL(AppntSql);
			SSRS InsuredSSRS = tExeSQL.execSQL(InsuredSql);
			SSRS LcbnfSSRS = tExeSQL.execSQL(LcbnfSql);
			ArrayList nameList = new ArrayList();
			ArrayList sexList = new ArrayList();
			ArrayList idtypeList = new ArrayList();
			ArrayList idnoList = new ArrayList();
			ArrayList birthdayList = new ArrayList();
			for(int AppntCout=1;AppntCout<=AppntSSRS.MaxRow;AppntCout++){
				nameList.add(AppntSSRS.GetText(AppntCout, 1));
				sexList.add(AppntSSRS.GetText(AppntCout, 2));
				idtypeList.add(AppntSSRS.GetText(AppntCout, 3));
				idnoList.add(AppntSSRS.GetText(AppntCout, 4));
				birthdayList.add(AppntSSRS.GetText(AppntCout, 5));
			}
			for(int InsuredCont=1;InsuredCont<=InsuredSSRS.MaxRow;InsuredCont++){
				nameList.add(InsuredSSRS.GetText(InsuredCont, 1));
				sexList.add(InsuredSSRS.GetText(InsuredCont, 2));
				idtypeList.add(InsuredSSRS.GetText(InsuredCont, 3));
				idnoList.add(InsuredSSRS.GetText(InsuredCont, 4));
				birthdayList.add(InsuredSSRS.GetText(InsuredCont, 5));
			}
			for(int LcbnfCont=1;LcbnfCont<=LcbnfSSRS.MaxRow;LcbnfCont++){
				nameList.add(LcbnfSSRS.GetText(LcbnfCont, 1));
				sexList.add(LcbnfSSRS.GetText(LcbnfCont, 2));
				idtypeList.add(LcbnfSSRS.GetText(LcbnfCont, 3));
				idnoList.add(LcbnfSSRS.GetText(LcbnfCont, 4));
				birthdayList.add(LcbnfSSRS.GetText(LcbnfCont, 5));
			}
			if(nameList.size()<=0){//此保单号有对应的投保人、被保人、受益人信息
				ErrInfo="保单"+inputInfoString[0]+"无对应的投保人、被保人、受益人信息，请核查！";
				return ErrInfo;
			}
			if(!nameList.contains(inputInfoString[1])){
				ErrInfo="保单"+inputInfoString[0]+"对应的姓名信息错误，请核查！";
				return ErrInfo;
			}
//			if(!idnoList.contains(inputInfoString[4])){
//				ErrInfo="保单"+inputInfoString[0]+"对应的证件号码信息错误，请核查！";
//				return ErrInfo;
//			}
			for(int i=0;i<nameList.size();i++){
				if(inputInfoString[1].equals(nameList.get(i))){
					if(!inputInfoString[2].equals(sexList.get(i))){
						ErrInfo="姓名为"+inputInfoString[1]+"对应的性别信息错误，请核查！";
						//return ErrInfo;
						break;
					}
					if("8".equals(inputInfoString[3])){
						inputInfoString[3] = "4";
					}
					if(!inputInfoString[3].equals(idtypeList.get(i))){
						ErrInfo="姓名为"+inputInfoString[1]+"对应的证件类型信息错误，请核查！";
						//return ErrInfo;
						break;
					}
					if(!inputInfoString[4].equals(idnoList.get(i))){
						ErrInfo="姓名为"+inputInfoString[1]+"对应的证件号码信息错误，请核查！";
						//return ErrInfo;
						break;
					}
					if(!inputInfoString[5].equals(birthdayList.get(i))){
						ErrInfo="姓名为"+inputInfoString[1]+"对应的出生日期信息错误，请核查！";
						//return ErrInfo;
						break;
					}else{
						ErrInfo="";
						return ErrInfo;
					}
				}
			}
		}else{
			ErrInfo="保单"+inputInfoString[0]+"不存在，请核查！";
		}
		return ErrInfo;
	}
	
//	 date 20110329 by gzh
	private String perAUTOCheck(OMElement Oms){
		String ErrInfo = "";
		QueryTool qTool1 = new QueryTool();
		inputInfoString = qTool1.parseIputInfo(Oms);
		//自动绑定校验证件类型是否为身份证
		if(!"0".equals(inputInfoString[3])){
			ErrInfo="个单自动绑定证件类型必须为身份证！";
		}
		return ErrInfo;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
