/**
 * 个人分红信息查询处理类
 * 2012-11-1 BYlicaiyan
 */
package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.topinterface.ListInterface;
import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;


public class QueryRateTraceInfo extends StandCardHYX {

private String mContNo = "";
	
	public QueryRateTraceInfo() {
		//返回报文根节点
		super("BONUSSHARE");
	}

	protected void getQueryFactor(Map queryFactor) {
		
		//获取条件值  CONTNO
		Iterator factor = queryFactor.keySet().iterator();
		
		while(factor.hasNext())
		{
			String key = factor.next().toString();
			if("CONTNO".equalsIgnoreCase(key))
			{
				mContNo = (String) queryFactor.get(key);
				System.out.println("分红信息查询,contno="+mContNo);
			}
		}
		
	}
	
	public Map prepareData() {
		
		//由contno获取polno
		String getPolNoSql = "select polno from lcpol where contno='"+mContNo+"'";
		SSRS polnoSSRS = mExeSQL.execSQL(getPolNoSql);
		
		Map OUTPUTDATA = new HashMap();
		for (int i=1;i<=polnoSSRS.MaxRow;i++){
			
			//由polNO查询分红信息并封装
			String polNoStr =  polnoSSRS.GetText(i, 1);
			
			///由polNo查询累积生息帐户历史信息
			String rateTraceSql = "select polno, paydate paydate,codename('bonusmoneytype', moneytype),otherno,money from lcinsureacctrace where '1351825603000'='1351825603000' and  polno= '"+polNoStr+"'  union all select polno,paydate paydate,codename('bonusmoneytype', moneytype),otherno,money from lbinsureacctrace where polno= '"+polNoStr+"'  order by paydate desc  with ur";
			SSRS rateTraceSSRS = mExeSQL.execSQL(rateTraceSql);
			Map rateTraceInfos = new HashMap();
			Map rateTrace = null;
			List item = new ArrayList();
			this.setRecordCount(String.valueOf(rateTraceSSRS.MaxRow));//返回最大行数
			if(polnoSSRS.MaxRow > 0){								  //置状态信息
				String[] errInfoArr = { "00", "0", "成功" };//
				this.setErrInfoArr(errInfoArr);
			}
			for(int k=1;k<=rateTraceSSRS.MaxRow;k++){
				rateTrace = new HashMap();
				rateTrace.put("POLNO", rateTraceSSRS.GetText(k, 1));//险种号
				rateTrace.put("PAYDATE", rateTraceSSRS.GetText(k, 2));//发生时间
				rateTrace.put("MONEYTYPE", rateTraceSSRS.GetText(k, 3));//业务类型
				rateTrace.put("OTHERNO", rateTraceSSRS.GetText(k, 4));//业务号
				rateTrace.put("MONEY", rateTraceSSRS.GetText(k, 5));//变动金额
				item.add(rateTrace);
			}
			rateTraceInfos.put("ITEM", item);
			OUTPUTDATA.put("RATETRACEINFOS", rateTraceInfos);
			
		}
		return OUTPUTDATA;	
	}	

}
