/**
 * GrpContDetail.java
 * com.ecwebservice.grpquery
 *
 * Function： TODO 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2010-6-25 		caosg
 *
 * Copyright (c) 2010, TNT All Rights Reserved.
 */

package com.ecwebservice.personalquery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import com.ecwebservice.topinterface.StandCardHYX;
import com.sinosoft.utility.SSRS;

/**
 * ClassName:ContDetail Function: TODO ADD FUNCTION Reason: TODO ADD REASON
 * 
 * @author
 * @version
 * @since Ver 1.1
 * @Date 2010-6-25 下午03:50:37
 */
public class ClaimDetail extends StandCardHYX {

	private String mContNo = "";

	private String mClmNo = "";

	public ClaimDetail() {

		super("CLAIMDETAIL");

	}

	protected void getQueryFactor(Map queryFactor) {
		// 获取条件值
		Iterator factor = queryFactor.keySet().iterator();

		while (factor.hasNext()) {
			String key = factor.next().toString();
			if ("CONTNO".equalsIgnoreCase(key)) {
				mContNo = (String) queryFactor.get(key);
				System.out.println("个险客户--理赔详细信息查询,ContNo=" + mContNo);
			}
			if ("CLMNO".equalsIgnoreCase(key)) {
				mClmNo = (String) queryFactor.get(key);
				System.out.println("个险客户--理赔详细信息查询,CLMNO=" + mClmNo);
			}
		}

	}

	public Map prepareData() {
		// 查询业务数据并封装
		Map OUTPUTDATA = new HashMap();

		String clmSql = "select  a.caseno 赔案号,d.accdesc 事故描述,"
				+ "e.rgtantname 申请人姓名,e.rgtantphone 申请人电话,e.rgtantmobile 申请人手机,a.rgtdate 立案日期,"
				+ "codename('llaskmode',e.rgttype) 受理方式,a.rgtstate 案件状态,"
				+ "a.customername 出险人姓名,codename('sex',a.customersex) 出险人性别,a.customerage 出险人年龄,"
				+ "codename('llcuststatus',a.custstate) 出险结果, d.accdate 事故日期 ,codename('relation',e.Relation) 申请人与出险人关系 "
				+ " ,a.mngcom 案件受理机构,c.polmngcom 保单机构 "
				+ "from llcase a,llcaserela b,llclaimdetail c,llsubreport d,llregister e "
				+ "where a.caseno=b.caseno and a.caseno=c.caseno and a.rgtno=e.rgtno "
				+ "and b.caserelano=c.caserelano and b.subrptno=d.subrptno and c.contno='"
				+ mContNo + "'  and c.caseno='" + mClmNo + "' "
				+ "fetch  first 1 rows only with ur ";
		SSRS clmSSRS = mExeSQL.execSQL(clmSql);
		// 赔款合计
		String realPaySql = "select sum(c.realpay)  from llclaimdetail c where  c.caseno='"
				+ mClmNo + "'";
		String realPay = mExeSQL.getOneValue(realPaySql);
		// 医院名称
		String hospitalNameSql = "select hospitalname from llfeemain where caseno='"
				+ mClmNo + "'";
		String hospitalName = mExeSQL.getOneValue(hospitalNameSql);

		if (realPay != null && !"".equals(realPay)) {
			OUTPUTDATA.put("PAYSUM", realPay);
		}

		if (clmSSRS.MaxRow > 0) {
			String[] errInfoArr = { "00", "0", "成功" };
			this.setErrInfoArr(errInfoArr);
			// 赔案基本信息
			OUTPUTDATA.put("CLMNO", clmSSRS.GetText(1, 1));

			OUTPUTDATA.put("CLMDATE", clmSSRS.GetText(1, 6));
			// OUTPUTDATA.put("ACCIDENTDATE", clmSSRS.GetText(1, 13));
			OUTPUTDATA.put("CLMSTATE", clmSSRS.GetText(1, 8));

			OUTPUTDATA.put("APPLYDATE", clmSSRS.GetText(1, 6));// 申请日期

			OUTPUTDATA.put("APPLYNAME", clmSSRS.GetText(1, 3));
			OUTPUTDATA.put("APPLYPHONE", clmSSRS.GetText(1, 4));
			OUTPUTDATA.put("RELATIONTOINSURED", clmSSRS.GetText(1, 14));
			OUTPUTDATA.put("APPLYWAY", clmSSRS.GetText(1, 7));// 报案方式
			// OUTPUTDATA.put("APPLYTYPE", "");

			OUTPUTDATA.put("INSUREDNAME", clmSSRS.GetText(1, 9));
			OUTPUTDATA.put("INSUREDSEX", clmSSRS.GetText(1, 10));
			OUTPUTDATA.put("INSUREDAGE", clmSSRS.GetText(1, 11));
			OUTPUTDATA.put("CLMMANAGECOM", clmSSRS.GetText(1, 15));  //案件所属的管理机构(新增字段)
			OUTPUTDATA.put("CONTMANAGECOM" , clmSSRS.GetText(1, 16)) ;//保单所属的管理机构(新增字段)
			

			// OUTPUTDATA.put("ACCIDENTCAUSE", "");
			// OUTPUTDATA.put("ACCIDENTDESC", clmSSRS.GetText(1, 2));
			// OUTPUTDATA.put("ACCIDENTRESULT1", clmSSRS.GetText(1, 12));
			// OUTPUTDATA.put("ACCIDENTRESULT2", "");
			// OUTPUTDATA.put("OTHERINFO", "");

			// 出险人信息
			// Map customerInfoMap = new HashMap();
			// customerInfoMap.put("INSUREDNAME", clmSSRS.GetText(1, 9));
			// customerInfoMap.put("INSUREDSEX", clmSSRS.GetText(1, 10));
			// customerInfoMap.put("INSUREDAGE", clmSSRS.GetText(1, 11));
			// customerInfoMap.put("HOSPITAL", hospitalName);
			// customerInfoMap.put("TREATSTATE", "");
			//
			// OUTPUTDATA.put("INSUREDINFO", customerInfoMap);

		}

		// 投保人信息
		// Map applyInfoMap = new HashMap();
		// applyInfoMap.put("APPLYNAME", "");
		// applyInfoMap.put("APPLYPHONE", "");
		// applyInfoMap.put("RELATIONTOINSURED", clmSSRS.GetText(1, 14));
		// applyInfoMap.put("APPLYDATE", "");
		// applyInfoMap.put("APPLYWAY", "");
		// applyInfoMap.put("APPLYTYPE", "");
		//
		// OUTPUTDATA.put("APPLYINFO", applyInfoMap);
		
		//赔款信息，新增
		String indeSQL="select a.caseno,"
		       +"(select givetype from llclaim where caseno = a.caseno ) 赔付结论,"
		       +"sum(a.realpay) 实赔金额,"
		       +"(select drawer "
		          +"from ljagetclaim lm, ljaget lj"
		         +" where lm.actugetno = lj.actugetno"
		          +" and not exists (select 1 from llcase c,llhospcase b where c.caseno=b.caseno and b.casetype='04' and c.mngcom like '8695%' and c.caseno=a.caseno)"
		          + " and not exists (select 1 from llcase c where c.caseno=a.caseno and c.rgtno like 'P%' and c.mngcom like '8695%')"
		           +" and (lm.otherno = a.caseno or lm.otherno = a.rgtno) and lm.othernotype='5' fetch"
		         +" first 1 rows only) 领取人姓名,"
		       +"(select DrawerID "
		          +"from ljagetclaim lm, ljaget lj"
		         +" where lm.actugetno = lj.actugetno"
		         +" and not exists (select 1 from llcase c,llhospcase b where c.caseno=b.caseno and b.casetype='04' and c.mngcom like '8695%' and c.caseno=a.caseno)"
		         + " and not exists (select 1 from llcase c where c.caseno=a.caseno and c.rgtno like 'P%' and c.mngcom like '8695%')"
		           +" and (lm.otherno = a.caseno or lm.otherno = a.rgtno) and lm.othernotype='5' fetch"
		         +" first 1 rows only) 领取人身份证号,"
		       +"(select codename"
		          +" from ldcode"
		         +" where codetype = 'paymode'"
		           +" and code = (select paymode"
		                         +" from ljagetclaim lm, ljaget lj"
		                        +" where lm.actugetno = lj.actugetno"
		                          +" and (lm.otherno = a.caseno or lm.otherno = a.rgtno)"
		                        +" fetch first 1 rows only)) 领取人方式,"
		       +" (select BankAccNo"
		          +" from ljagetclaim lm, ljaget lj"
		         +" where lm.actugetno = lj.actugetno"
		           +" and (lm.otherno = a.caseno or lm.otherno = a.rgtno) fetch"
		         +" first 1 rows only) 领取人账号,"
//		       +"(select makedate"
//		          +" from ljagetclaim"
//		         +" where otherno = a.caseno fetch first 1 rows only) 划账时间"
//		       by gzh 20110420 理赔划账时间为理赔给财务钱的时间，而显示时，应是财务给客户钱的时间。
		       +"(select ConfDate from LJAGet where 1=1 and (otherno = a.caseno or otherno = a.rgtno) fetch first 1 rows only) 划账时间"
		         +" ,(select reason from LLClaimDecline where caseno = a.caseno), "
		       +" (select bankname from ldbank where bankcode in (select bankcode"
		          +" from ljagetclaim lm, ljaget lj"
		         +" where lm.actugetno = lj.actugetno"
		           +" and (lm.otherno = a.caseno or lm.otherno = a.rgtno) fetch"
		         +" first 1 rows only)) 领取人银行名称 "//by gzh 20110411 增加领取银行名称
		  +" from llclaimdetail a"
		 +" where a.contno = '"+this.mContNo+"'"
		   +" and a.caseno = '"+this.mClmNo+"'"
		 +" group by  a.caseno, a.rgtno";
		SSRS indeSSRS = mExeSQL.execSQL(indeSQL);
		if(indeSSRS!=null && indeSSRS.MaxRow == 1){
			Map indeInfo = null;		
			indeInfo = new HashMap();
			indeInfo.put("INDEMRESULT", indeSSRS.GetText(1, 2));
			indeInfo.put("INDEMBASIS", indeSSRS.GetText(1, 9));
			indeInfo.put("INDEMAMOUNT", indeSSRS.GetText(1, 3));
			indeInfo.put("RECENAME", indeSSRS.GetText(1, 4));
			indeInfo.put("RECEIDNO", indeSSRS.GetText(1, 5));
			indeInfo.put("RECETYPE", indeSSRS.GetText(1, 6));
			indeInfo.put("RECEACCOUNT", indeSSRS.GetText(1, 7));
			indeInfo.put("TRANDATE", indeSSRS.GetText(1, 8));
			indeInfo.put("RECEBANK", indeSSRS.GetText(1, 10));//by gzh 20110411 增加领取银行名称
			OUTPUTDATA.put("INDEMNITY", indeInfo);
		}
		

		// 事件信息
		String subRptSql = " select c.SubRptNo 事件号 ,c.AccDate 发生日期 ,c.AccPlace 事故地点,c.InHospitalDate 入院日期, c.OutHospitalDate 出院日期, "
				+ "(case  c.AccidentType when '1' then '疾病' when '2' then '意外' else '未知' end ) 事件类型 ,c.AccDesc 事故描述 from llcase a, llcaserela  b ,llsubreport c "
				+ "where a.caseno='"
				+ mClmNo
				+ "' and a.caseno=b.caseno and b.SubRptNo = c.SubRptNo ";
		SSRS subRptSSRS = mExeSQL.execSQL(subRptSql);

		Map subRptInfosMap = new HashMap();
		Map subRptInfo = null;
		List subRptItem = new ArrayList();
		for (int i = 1; i <= subRptSSRS.MaxRow; i++) {
			subRptInfo = new HashMap();
			subRptInfo.put("ACCNO", subRptSSRS.GetText(i, 1));

			subRptInfo.put("ACCDATE", subRptSSRS.GetText(i, 2));
			subRptInfo.put("ACCLOC", subRptSSRS.GetText(i, 3));
			subRptInfo.put("INHOSPITALDATE", subRptSSRS.GetText(i, 4));
			subRptInfo.put("OUTHOSPITALDATE", subRptSSRS.GetText(i, 5));
			subRptInfo.put("ACCTYPE", subRptSSRS.GetText(i, 6));
			subRptInfo.put("ACCINFO", subRptSSRS.GetText(i, 7));

			subRptItem.add(subRptInfo);
		}
		subRptInfosMap.put("ITEM", subRptItem);
		OUTPUTDATA.put("ACCLIST", subRptInfosMap);
		
		// 账单信息
		String billSql = "  select ReceiptNo 账单收据号,HospitalName 医院名称,FeeDate 账单日期,sumfee 账单金额  from LLFeeMain where caseno='"
				+ mClmNo + "'";
		SSRS billSSRS = mExeSQL.execSQL(billSql);

		Map billInfosMap = new HashMap();
		Map billInfo = null;
		List billItem = new ArrayList();
		for (int i = 1; i <= billSSRS.MaxRow; i++) {
			billInfo = new HashMap();
			billInfo.put("BILLNO", billSSRS.GetText(i, 1));

			billInfo.put("HOSPITALNAME", billSSRS.GetText(i, 2));
			billInfo.put("BILLDATE", billSSRS.GetText(i, 3));
			billInfo.put("BILLAMOUNT", billSSRS.GetText(i, 1));

			billInfo.put("BILLAMOUNT", billSSRS.GetText(i, 4));

			billItem.add(billInfo);
		}
		billInfosMap.put("ITEM", billItem);
		OUTPUTDATA.put("BILLLIST", billInfosMap);

		//date 20101129 by gzh
//		 给付责任信息 GETDUTYLIST
		String getdutylistSql = "select lmr.riskname,llc.riskcode,lmd.getdutyname,llc.realpay "+
		       " from llclaimdetail llc,lmdutyget lmd,lmrisk lmr "+
		       " where caseno='"+mClmNo + 
		       "' and llc.getdutycode = lmd.getdutycode"+
		       " and llc.riskcode = lmr.riskcode ";
		System.out.println("个单给付责任信息SQL:"+getdutylistSql);
		SSRS dutylistSSRS = mExeSQL.execSQL(getdutylistSql);

		Map dutyInfosMap = new HashMap();
		Map dutyInfo = null;
		List dutyItem = new ArrayList();
		for (int i = 1; i <= dutylistSSRS.MaxRow; i++) {
			dutyInfo = new HashMap();
			dutyInfo.put("RISKNAME", dutylistSSRS.GetText(i, 1));
			dutyInfo.put("RISKCODE", dutylistSSRS.GetText(i, 2));
			dutyInfo.put("GETDUTYNAME", dutylistSSRS.GetText(i, 3));
			dutyInfo.put("REALPAY", dutylistSSRS.GetText(i, 4));

			dutyItem.add(dutyInfo);
		}
		dutyInfosMap.put("ITEM", dutyItem);
		OUTPUTDATA.put("GETDUTYLIST", dutyInfosMap);
		
		return OUTPUTDATA;

	}

	/**
	 * main:(这里用一句话描述这个方法的作用)
	 * 
	 * @param
	 * @param args
	 *            设定文件
	 * @return void DOM对象
	 * @throws
	 * @since CodingExample Ver 1.1
	 */

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		StringBuffer sql1 = new StringBuffer(
				" select b.grpcontno ,b.grpname,(select codename from ldcode where codetype = 'businesstype' and code = b.businesstype),");
		sql1
				.append(" (select codename from ldcode where codetype = 'grpnature' and code = b.GrpNature),'' 注册资本,'' 资产总额,Fax,");
		sql1
				.append(" a.linkman1,a.linkman2,a.mobile1,a.mobile2,a.e_mail1,a.e_mail2,a.grpaddress,a.grpzipcode ,b.cvalidate,'' 投保人数,b.RelaPeoples,(case b.stateflag when '1' then '有效' else '失效' end)");
		sql1
				.append(" from lcgrpaddress a,lcgrpcont b where a.customerno = b.appntno and b.grpcontno='  '");
		System.out.println(sql1.toString());
	}
}
