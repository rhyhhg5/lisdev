package com.ecwebservice.personalquery;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-10 下午05:21:23
 * @Description:
 * @version
 */

public class ContDetail implements CardHYX {

	/**
	 * @param args
	 */
	private String[] baseInfoString;

	// baseInfo[0] = batchno;
	// baseInfo[1] = sendDate;
	// baseInfo[2] = sendTime;
	// baseInfo[3] = messageType;
	private String contNo;

	// 封装结果集
	private Set resultSet = new HashSet();

	public static void main(String[] args) {

	}

	public OMElement queryData(OMElement Oms) {
		System.out.println("进入个险保单详细信息查询处理类！");
		OMElement responseOME ;
		//包含处理状态 及错误信息
		String[] errInfoArr = new String[3];
		
		QueryTool qTool = new QueryTool();
		LCAddressDB tLCAddressDB = new LCAddressDB();
		// 获取baseInfo
		baseInfoString = qTool.parseBaseInfo(Oms);

		
		contNo = qTool.getContNoInQueryString(Oms);
		String contSql = " select Lccont.Managecom, Cvalidate,signdate,CustomGetPolDate,Getpoldate,case stateflag when '1' then '有效' else '失效' end  from Lccont where contno='"
				+ contNo + "'";
		// 服务人员agent）
		String agentSql = "select name,phone from laagent where agentcode in (select  agentcode  from Lccont where contno='"
				+ contNo + "')";
		// 缴费方式
		String payModeSql = "select bankaccno from ljspay where  otherno='"
				+ contNo + "'";
		//险种信息
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(contNo);
		LCPolSet tLCPolSet = tLCPolDB.query();
		// 投保人信息 LCAppntSchema
		LCAppntDB tLCAppntDB = new LCAppntDB();
		tLCAppntDB.setContNo(contNo);
		LCAppntSet tLCAppntSet = tLCAppntDB.query();
		// resultSet.add(tLCAppntSet);
		//投保人地址等信息
		LCAddressSet tLCAddressSet = tLCAddressDB
		.executeQuery("select * from lcaddress where customerno in(select insuredno from lcinsured where contno='"
				+ contNo + "')");
		// 被保人信息
		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		tLCInsuredDB.setContNo(contNo);
		LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
		// 受益人信息
		LCBnfDB tLCBnfDB = new LCBnfDB();
		tLCBnfDB.setContNo(contNo);
		LCBnfSet tLCBnfSet = tLCBnfDB.query();

		ExeSQL tExeSQL = new ExeSQL();
		SSRS contSSRS = tExeSQL.execSQL(contSql);
		SSRS agentSSRS = tExeSQL.execSQL(agentSql);
//		SSRS payModeSSRS = tExeSQL.execSQL(payModeSql);
		// 被保人地址等信息
		
		LCAddressSet tAppntLCAddressSet = tLCAddressDB
				.executeQuery("select * from lcaddress where customerno in(select appntno from lcappnt where contno='"+contNo+"') ");
		Map schemaMap = new HashMap();
		if (contSSRS.MaxRow > 0) {
			schemaMap.put("MANAGEORGAN", contSSRS.GetText(1, 1));

			schemaMap.put("CVALIDATE", contSSRS.GetText(1, 2));
			schemaMap.put("SIGNDATE", contSSRS.GetText(1, 3));
			schemaMap.put("CUSTOMGETPOLDATE", contSSRS.GetText(1, 4));
			schemaMap.put("GETPOLDATE", contSSRS.GetText(1, 5));
			schemaMap.put("CONTSTATE", contSSRS.GetText(1, 6));

		}
		if (agentSSRS.MaxRow > 0) {
			schemaMap.put("AGENTNAME", agentSSRS.GetText(1, 1));
			schemaMap.put("AGENTTELEPHONE", agentSSRS.GetText(1, 2));
		}
	
			String payModeCode = tExeSQL.getOneValue(payModeSql);
			String payMode = qTool.decodePayMode(payModeCode);
			schemaMap.put("PAYLOCATION", payMode);
		
		resultSet.add(schemaMap);
		if(resultSet.size()>0&&tLCPolSet.size()>0){
			errInfoArr[0] = "00";
			errInfoArr[1] = "0";
			errInfoArr[2] = "成功";
		}else{
			errInfoArr[0] = "01";
			errInfoArr[1] = "*";
			errInfoArr[2] = "没有符合查询条件的数据";
		}
		responseOME = buildResponseOME(resultSet, tLCPolSet,tLCAppntSet,tAppntLCAddressSet,
				tLCInsuredSet, tLCAddressSet, tLCBnfSet, errInfoArr[0], errInfoArr[1], errInfoArr[2]);
		return responseOME;
	}

	private OMElement buildResponseOME(Set resultSet,LCPolSet tLCPolSet , LCAppntSet tLCAppntSet,LCAddressSet tAppntLCAddressSet,
			LCInsuredSet tLCInsuredSet, LCAddressSet tLCAddressSet,
			LCBnfSet tLCBnfSet, String state, String errCode, String errInfo) {
		ExeSQL tempExeSQL = new ExeSQL();
		QueryTool tQueryTool = new QueryTool();
		Map resultMap = tQueryTool.getResultOnlyMap(resultSet);
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("CONTDETAIL", null);

		LoginVerifyTool tool = new LoginVerifyTool();
		QueryTool qTool = new QueryTool();
		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString, state,
				errCode, errInfo);

		DATASET.addChild(BASEINFO);// 构建基础信息

		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);

		// 保单节点
		OMElement CONTINFO = fac.createOMElement("CONTINFO", null);
		// 投保人信息节点
		OMElement APPNTINFO = fac.createOMElement("APPNTINFO", null);
		// 被保人信息节点
		OMElement INSUREDINFO = fac.createOMElement("INSUREDINFO", null);
		// 受益人信息节点
		OMElement BNFINFO = fac.createOMElement("BNFINFO", null);

		// 开始构建保单信息
		tool.addOm(CONTINFO, "CONTNO", contNo);
//		tool.addOm(CONTINFO, "CUSTOMERNO", "");
		tool.addOm(CONTINFO, "MANAGEORGAN", (String) resultMap
				.get("MANAGEORGAN"));
		tool.addOm(CONTINFO, "AGENTNAME", (String) resultMap.get("AGENTNAME"));
		tool.addOm(CONTINFO, "AGENTTELEPHONE", (String) resultMap
				.get("AGENTTELEPHONE"));
		tool.addOm(CONTINFO, "CVALIDATE", (String) resultMap.get("CVALIDATE"));
		tool.addOm(CONTINFO, "SIGNDATE", (String) resultMap.get("SIGNDATE"));
//		tool.addOm(CONTINFO, "TERM", "");
		tool.addOm(CONTINFO, "CUSTOMGETPOLDATE", (String) resultMap
				.get("CUSTOMGETPOLDATE"));
		tool
				.addOm(CONTINFO, "GETPOLDATE", (String) resultMap
						.get("GETPOLDATE"));
		tool.addOm(CONTINFO, "CONTSTATE", (String) resultMap.get("CONTSTATE"));
		tool.addOm(CONTINFO, "PAYLOCATION", (String) resultMap
				.get("PAYLOCATION"));//缴费方式
		tool.addOm(CONTINFO, "ISAUTOPAY", "");// 目前核心没有
		// 构建险种信息 多条记录
		
		OMElement RISKINFO = fac.createOMElement("RISKINFO", null);
		
		if(tLCPolSet!=null){
			int lcpolPointer;
			for(lcpolPointer=1;lcpolPointer<=tLCPolSet.size();lcpolPointer++){
				LCPolSchema tLCPolSchema = tLCPolSet.get(lcpolPointer);
				OMElement RISKITEM = fac.createOMElement("ITEM", null);
				tool.addOm(RISKITEM, "RISKCODE", tLCPolSchema.getPolNo());
				String riskNameSql = "select a.riskname from Lmriskapp a where a.riskcode='"+tLCPolSchema.getRiskCode()+"'";
				String riskName = tempExeSQL.getOneValue(riskNameSql);
				tool.addOm(RISKITEM, "RISKNAME", riskName);
				tool.addOm(RISKITEM, "TERM", String.valueOf(tLCPolSchema.getYears()));
				tool.addOm(RISKITEM, "ENDDATE", tLCPolSchema.getEndDate());
				tool.addOm(RISKITEM, "PREM", String.valueOf(tLCPolSchema.getPrem()));
				tool.addOm(RISKITEM, "AMNT", String.valueOf(tLCPolSchema.getAmnt()));
				tool.addOm(RISKITEM, "PAYSEP", String.valueOf(tLCPolSchema.getPayIntv()));
				tool.addOm(RISKITEM, "PAYTERM", String.valueOf(tLCPolSchema.getPayYears()));
				tool.addOm(RISKITEM, "PAYDATE", "");
				RISKINFO.addChild(RISKITEM);
			}
			
		}
		
		
		CONTINFO.addChild(RISKINFO);

		OUTPUTDATA.addChild(CONTINFO);

		// 开始构建投保人信息
		if (tLCAppntSet != null) {
			int lcappntPointer;
			for (lcappntPointer = 1; lcappntPointer <= tLCAppntSet.size(); lcappntPointer++) {
				LCAppntSchema tLCAppntSchema = tLCAppntSet.get(lcappntPointer);
				tool.addOm(APPNTINFO, "APPNTNAME", tLCAppntSchema.getAppntName());
				tool.addOm(APPNTINFO, "APPNTSEX", tLCAppntSchema.getAppntSex());
				tool.addOm(APPNTINFO, "APPNTIDTYPE", tLCAppntSchema.getIDType());
				tool.addOm(APPNTINFO, "APPNTIDNO", tLCAppntSchema.getIDNo());
				tool.addOm(APPNTINFO, "APPNTBIRTHDAY", tLCAppntSchema.getAppntBirthday());
				tool.addOm(APPNTINFO, "APPNTMARRYSTATE", tLCAppntSchema.getMarriage());
				tool.addOm(APPNTINFO, "APPNTNATIVEPLACE", tLCAppntSchema.getNativePlace());
				tool.addOm(APPNTINFO, "APPNTOCCUPATION", tLCAppntSchema.getOccupationCode());
//				 获取投保人编号，用此编号查询该投保人电话等信息（lcaddress）
				String appntNo = tLCAppntSchema.getAppntNo();
				LCAddressSchema tAppntLCAddressSchema = new LCAddressSchema();
				if (tAppntLCAddressSet != null) {
					int appntAddressPointer;
					for (appntAddressPointer = 1; appntAddressPointer <= tAppntLCAddressSet.size(); appntAddressPointer++) {
						tAppntLCAddressSchema = tAppntLCAddressSet.get(appntAddressPointer);
						String inLCAddressAppntNo = tAppntLCAddressSchema
								.getCustomerNo();
						if (appntNo.equals(inLCAddressAppntNo)) {
							break;
						}
					}

				}
				tool.addOm(APPNTINFO, "APPNTMOBILE", tAppntLCAddressSchema.getMobile());
				tool.addOm(APPNTINFO, "APPNTCOMPANYPHONE", tAppntLCAddressSchema.getCompanyPhone());
				tool.addOm(APPNTINFO, "APPNTFAX", tAppntLCAddressSchema.getFax());
				tool.addOm(APPNTINFO, "APPNTHOMEPHONE", tAppntLCAddressSchema.getHomePhone());
				tool.addOm(APPNTINFO, "APPNTADDRESS", tAppntLCAddressSchema.getPostalAddress());
				tool.addOm(APPNTINFO, "APPNTZIPCODE", tAppntLCAddressSchema.getZipCode());
				tool.addOm(APPNTINFO, "APPNTEMAIL", tAppntLCAddressSchema.getEMail());

				OUTPUTDATA.addChild(APPNTINFO);
			}
		}

		

		// 开始构建被保人信息节点
		if (tLCInsuredSet != null) {
			int i;
			for (i = 1; i <= tLCInsuredSet.size(); i++) {
				LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(i);

				// System.out.println("被保人姓名："+tLCInsuredSchema.getName());
				OMElement ITEM = fac.createOMElement("ITEM", null);
				tool.addOm(ITEM, "INSUNO", tLCInsuredSchema
						.getInsuredNo());
				tool.addOm(ITEM, "TOAPPNTRELA", tLCInsuredSchema
						.getRelationToAppnt());
				tool.addOm(ITEM, "RELATIONCODE", tLCInsuredSchema
						.getRelationToMainInsured());
				String relationToMainInsuredSql = "select codename from ldcode, lcinsured where lcinsured.contno='"
						+ contNo
						+ "' "
						+ "and codetype = 'relation' and   code = '"
						+ tLCInsuredSchema.getRelationToMainInsured() + "'";
				String relationToMainInsured = tempExeSQL
						.getOneValue(relationToMainInsuredSql);
				tool.addOm(ITEM, "RELATIONTOINSURED",
						relationToMainInsured);// 与主被保人关系
				tool.addOm(ITEM, "REINSUNO", "");
				tool.addOm(ITEM, "INSUREDORDER", tLCInsuredSchema
						.getSequenceNo());
				tool.addOm(ITEM, "INSUREDNAME", tLCInsuredSchema
						.getName());
				tool
						.addOm(ITEM, "INSUREDSEX", tLCInsuredSchema
								.getSex());
				tool.addOm(ITEM, "INSUREDIDTYPE", tLCInsuredSchema
						.getIDType());
				tool.addOm(ITEM, "INSUREDIDNO", tLCInsuredSchema
						.getIDNo());
				tool.addOm(ITEM, "INSUREDBIRTHDAY", tLCInsuredSchema
						.getBirthday());
				tool.addOm(ITEM, "INSUREDMARRYSTATE", tLCInsuredSchema
						.getMarriage());
				tool.addOm(ITEM, "INSUREDNATIVEPLACE", tLCInsuredSchema
						.getNativePlace());
				tool.addOm(ITEM, "INSUREDOCCUPATION", tLCInsuredSchema
						.getOccupationCode());
				// 获取被保人编号，用此编号查询该被保人电话等信息（lcaddress）
				String insuredNo = tLCInsuredSchema.getInsuredNo();
				LCAddressSchema tLCAddressSchema = new LCAddressSchema();
				if (tLCAddressSet != null) {
					int j;
					for (j = 1; j <= tLCAddressSet.size(); j++) {
						tLCAddressSchema = tLCAddressSet.get(j);
						String inLCAddressInsuredNo = tLCAddressSchema
								.getCustomerNo();
						if (insuredNo.equals(inLCAddressInsuredNo)) {
							break;
						}
					}

				}
				tool.addOm(ITEM, "INSUREDMOBILE", tLCAddressSchema
						.getMobile());
				tool.addOm(ITEM, "INSUREDCOMPANYPHONE", tLCAddressSchema
						.getCompanyPhone());
				tool
						.addOm(ITEM, "INSUREDFAX", tLCAddressSchema
								.getFax());
				tool.addOm(ITEM, "INSUREDHOMEPHONE", tLCAddressSchema
						.getHomePhone());
				tool.addOm(ITEM, "INSUREDADDRESS", tLCAddressSchema
						.getPostalAddress());
				tool.addOm(ITEM, "INSUREDZIPCODE", tLCAddressSchema
						.getZipCode());
				tool.addOm(ITEM, "INSUREDEMAIL", tLCAddressSchema
						.getEMail());

				INSUREDINFO.addChild(ITEM);
			}
		}

		// 开始构建受益人信息节点
		if (tLCBnfSet != null) {
			int bnfPointer;
			for (bnfPointer = 1; bnfPointer <= tLCBnfSet.size(); bnfPointer++) {
				LCBnfSchema tLCBnfSchema = tLCBnfSet.get(bnfPointer);
				OMElement ITEM = fac.createOMElement("ITEM", null);
				tool.addOm(ITEM, "TOINSUNO", tLCBnfSchema.getInsuredNo());
				//添加险种名称
//				String riskNameSql = "select a.riskname from Lmriskapp a where a.riskcode in  (select riskcode from lcpol where contno='13000542058')";
//				String riskName = tempExeSQL.getOneValue(riskNameSql);
				//lcbnf中的polNo -- 险种名称
				tool.addOm(ITEM, "RNAME", tLCBnfSchema.getPolNo());
				tool.addOm(ITEM, "TOINSURELA", tLCBnfSchema
						.getRelationToInsured());
				tool.addOm(ITEM, "BNFTYPE", tLCBnfSchema.getBnfType());
				tool.addOm(ITEM, "BNFGRADE", tLCBnfSchema.getBnfGrade());
				tool.addOm(ITEM, "BNFRATE", String.valueOf(tLCBnfSchema
						.getBnfLot()));
				tool.addOm(ITEM, "BNFNAME", tLCBnfSchema.getName());
				tool.addOm(ITEM, "BNFSEX", tLCBnfSchema.getSex());
				tool.addOm(ITEM, "BNFIDTYPE", tLCBnfSchema.getIDType());
				tool.addOm(ITEM, "BNFIDNO", tLCBnfSchema.getIDNo());
//				tool.addOm(ITEM, "BNFBIRTHDAY", tLCBnfSchema.getBirthday());

				BNFINFO.addChild(ITEM);

			}
		}
		//添加被保人节点
		OUTPUTDATA.addChild(INSUREDINFO);
		//添加受益人节点
		OUTPUTDATA.addChild(BNFINFO);
		
		DATASET.addChild(OUTPUTDATA);

		return DATASET;
	}

}
