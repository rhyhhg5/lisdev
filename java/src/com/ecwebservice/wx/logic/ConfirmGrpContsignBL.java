package com.ecwebservice.wx.logic;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class ConfirmGrpContsignBL {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mGrpContNo=null;
	
	private String operator =null;
	
	private String manageCom = null;

	/**
	 * 确认签单
	 * @param tGrpContNo
	 * @param tGlobalInput
	 * @return
	 */
	public boolean submit(String tGrpContNo,GlobalInput tGlobalInput){
		
		if(!getInputData(tGrpContNo,tGlobalInput)){
			buildError("getInputData", "确认签单没有得到数据！");
	        return false;
		}
		
		if(!checkData()){
			//buildError("checkData", "确认签单没有得到数据！");
	        return false;
		}
		
		if(!dealData()){
			return false;
		}
		
		LCGrpContSignBL tLCGrpContSignBL = new LCGrpContSignBL();
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(tGrpContNo);
		LCGrpContSet tLCGrpContSet=tLCGrpContDB.query();
		if(tLCGrpContSet == null || tLCGrpContSet.size()<1){
			buildError("confirmGrpContSign", "没有得到签单的信息！");
	         return false;
		}
		LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);
		
		VData tVData = new VData();
		tVData.add(this.mGlobalInput);	
		tVData.add(tLCGrpContSchema);
		if(!tLCGrpContSignBL.submitData(tVData, "")){
			buildError("confirmGrpContSign", "确认签单时失败！");
	         return false;
		}
		//回置清单列表
		UPCertPaytradestatus upstatus = new UPCertPaytradestatus();
		if(!upstatus.upCertStatus(tGrpContNo)){
			System.out.println("确认签单后,回置licertify.paytradestatus失败！老团单号为"+tGrpContNo);
			buildError("upCertStatus", "确认签单后,回置licertify.paytradestatus失败！");
	        return false;
		}
		return true;
	}
	
	
	private boolean getInputData(String tGrpContNo,GlobalInput tGlobalInput){
		if(tGlobalInput !=null){
			this.mGlobalInput = tGlobalInput;
			
		}else{
			buildError("getInputData", "确认签单时,没有得到mGlobalInput信息！");
	         return false;
		}
		
		if(tGrpContNo!=null && !tGrpContNo.equals("")){
			this.mGrpContNo = tGrpContNo;
		}else{
			buildError("getInputData", "确认签单时,没有得到tGrpContNo信息！");
	         return false;
		}
		return true;
	}
	
	private boolean checkData(){
		return true;
	}
	
	private boolean dealData(){
		return true;
	}
	/**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CardNoInfoToLicertiry";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String args[]){
    	ConfirmGrpContsignBL cgs = new ConfirmGrpContsignBL();
    	GlobalInput mGlobalInput = new GlobalInput();
    	mGlobalInput.Operator="SYS_ECWX";
    	mGlobalInput.ManageCom="86000000";
    	cgs.submit("1400006332", mGlobalInput);
    }
}
