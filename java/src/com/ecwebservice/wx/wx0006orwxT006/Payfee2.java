package com.ecwebservice.wx.wx0006orwxT006;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.mail.Mailsender2;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.finfee.TempFeeUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.taskservice.BqFinishTask;
import com.sinosoft.lis.taskservice.LLClaimFinishTask;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class Payfee2 {
	private ExeSQL mexeSQL = new ExeSQL();
	String contno;
	String info;
	CErrors tError = new CErrors();
	SSRS tSsrs = new SSRS();
	ExeSQL tExeSQL = new ExeSQL();
	boolean flag;
    GlobalInput tGI = new GlobalInput(); 
    public Payfee2(){}
    public OMElement queryData(OMElement oms){
    	OMElement responseOME = null;
    	//解析报文
    	flag = getInfo(oms);
    	if(!flag){
    		System.out.println(info);
    		responseOME = buildResponse(oms, info);
    		return responseOME;
    	}
    	//付费
    	flag = payfeeSave(contno);
    	if(!flag){
    		System.out.println(info);
    		responseOME = buildResponse(oms, info);
    		return responseOME;
    	}
    	//发送邮件
    	flag = sendMail(contno);
    	if(!flag){
    		System.out.println("在线续保，个险续期续保保费对账单发送失败，失败的保单号是："+contno);
    	}
    	responseOME = buildResponse(oms, "SUCCESS");
    	return responseOME;
    }
    public boolean getInfo(OMElement oms){
		System.out.println("接收的报文为："+oms.toString());
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement om = (OMElement)iterator.next();
				if(om.getLocalName().equals("PAYFEE")){
					Iterator child = om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("CONTNO")){
							contno = child_Element.getText();
							
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			info ="解析报文信息失败";
			return false;
		}
		return true;
	}
    public boolean sendMail(String contno){
    	try {
			String subject = "个险续期续保保费对账单";
			SSRS tSsrs = new SSRS();
			SSRS tSsrs2 = new SSRS();
			String sql = "select (select riskname from lmrisk where riskcode = a.riskcode ),insuredname,amnt,prem from lcpol a where contno='"+contno+"' with ur";
			String sql2 = "select appntName,appntsex,appntno,addressno from lcappnt where contno='"+contno+"' with ur";
			tSsrs = mexeSQL.execSQL(sql);
			tSsrs2= mexeSQL.execSQL(sql2);
			String sql3 = "select email from lcaddress where customerno='"+tSsrs2.GetText(1, 3)+"' and addressno='"+tSsrs2.GetText(1, 4)+"' with ur";
			String sql4 = "select  sumprem from lccont where contno='"+contno+"' with ur";
			String receiver = mexeSQL.getOneValue(sql3);
			String sumprem = mexeSQL.getOneValue(sql4);
			if(receiver==null||receiver.equals("")){
				return true;
			}
			String contents =  Mailsender2.getContent3(tSsrs2.GetText(1, 1), tSsrs2.GetText(1, 2), sumprem, contno, PubFun.getCurrentDate(),tSsrs);
			Mailsender2.sendEmail(receiver, subject, contents);
		} catch (Exception e) {
			// TODO: handle exception
			
			return false;
		}
		return true;
    }
    public boolean payfeeSave(String contno){
    	CErrors tError = new CErrors();
    	  String FlagStr = "";
    	  String Content = "";
    	  String ShowFinance = "N"; //显示缴纳金额信息的标记（如果财务收费完全正确，则显示金额的详细信息）
    	  double CashValue = 0;
    	  double ChequeValue = 0;
    	  GlobalInput tGI = new GlobalInput(); //repair:
//    	  tGI = (GlobalInput) session.getValue("GI"); //参见loginSubmit.jsp
    	  String sql = "select * from lccont where contno='"+contno+"' with ur";
      	  System.out.println("ExeSql:"+sql);
      	  LCContDB db = new LCContDB();
      	  LCContSchema pLCContSchema = db.executeQuery(sql).get(1);
      	  tGI.Operator = pLCContSchema.getOperator();
      	  tGI.ComCode = pLCContSchema.getManageCom();
      	  tGI.ManageCom = pLCContSchema.getManageCom();
      	  
      	LCPolDB mLCPolDB = new LCPolDB();
		mLCPolDB.setContNo(contno);
		LCPolSet mLCPolSet = mLCPolDB.query();
    	  if (tGI == null) {
    	    System.out.println("页面失效,请重新登陆");
    	    FlagStr = "Fail";
    	    Content = "页面失效,请重新登陆";
    	  }
    	  else { //页面有效
    	    String Operator = tGI.Operator; //保存登陆管理员账号
    	    String ManageCom = tGI.ComCode; //保存登陆区站,ManageCom=内存中登陆区站代码
    	    String tTempFeeType = "2";//续期续保
    	    //---------- modify by zhangyige 2013-11-22 ------------------------
    	    String sql3 = "select riskcode,sum(SumDuePayMoney) as prem from ljspayperson where contno='"+contno+"' group by riskcode";
    	    SSRS ljspayList = tExeSQL.execSQL(sql3);
    	    double paySum = 0.0;
    	    for(int i=0;i<ljspayList.MaxRow;i++){
    	    	String riskPrem = ljspayList.GetText(i+1, 2);
    	    	paySum = paySum + Double.parseDouble(riskPrem);
    	    }
    	    String tempfeeSql = "select Getnoticeno from Ljspay where otherno='"+contno+"' with ur";
		    String getNoticeno = tExeSQL.getOneValue(tempfeeSql);
		    String currentDate = PubFun.getCurrentDate();
		    
    		LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    		
        	SSRS BankInfos = getBankAccCode();
        	String ZBankManageCom = BankInfos.GetText(1, 1);//总公司机构
        	String ZBankAccNo = BankInfos.GetText(1, 2);
        	for(int i=0;i<ljspayList.MaxRow;i++){
        		String riskcode = ljspayList.GetText(i+1, 1); 
        		String riskPrem = ljspayList.GetText(i+1, 2);
	    		LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema(); 
	    		tLJTempFeeSchema.setTempFeeNo(getNoticeno);
	    		tLJTempFeeSchema.setOtherNo(contno); //投保单号
	    		tLJTempFeeSchema.setTempFeeType(tTempFeeType);//2-续期续保
	    		tLJTempFeeSchema.setOtherNoType("4");	//4--印刷号
	    		tLJTempFeeSchema.setRiskCode(riskcode);//默认000000
	    		tLJTempFeeSchema.setPayIntv(mLCPolSet.get(1).getPayIntv());
	    		tLJTempFeeSchema.setAPPntName(mLCPolSet.get(1).getAppntName());
	    		tLJTempFeeSchema.setPayMoney(riskPrem);
	    		tLJTempFeeSchema.setManageCom(ZBankManageCom);	//收费机构
	    		tLJTempFeeSchema.setPolicyCom(pLCContSchema.getManageCom());	//管理机构
	    		tLJTempFeeSchema.setPayDate(PubFun.getCurrentDate());
	    		tLJTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate());
	    		tLJTempFeeSchema.setSaleChnl(pLCContSchema.getSaleChnl());
	    		tLJTempFeeSchema.setAgentCom(pLCContSchema.getAgentCom());
	    		tLJTempFeeSchema.setAgentGroup(pLCContSchema.getAgentGroup());
	    		tLJTempFeeSchema.setAgentCode(pLCContSchema.getAgentCode());
	    		tLJTempFeeSchema.setConfFlag("0");	//是否核销 0：没有财务核销 1：已有财务核销
	    		tLJTempFeeSchema.setOperator(Operator);
	    		tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
	    		tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
	    		tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
	    		tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
	    		tLJTempFeeSet.add(tLJTempFeeSchema);
        	}
    		LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    		
	    	LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
    		mLJTempFeeClassSchema.setTempFeeNo(getNoticeno);
    		mLJTempFeeClassSchema.setPayMode("12");
    		mLJTempFeeClassSchema.setPayMoney(paySum);
    		mLJTempFeeClassSchema.setAppntName(pLCContSchema.getAppntName());
    		mLJTempFeeClassSchema.setPayDate(PubFun.getCurrentDate());
    		mLJTempFeeClassSchema.setEnterAccDate(PubFun.getCurrentDate());
    		mLJTempFeeClassSchema.setConfMakeDate(currentDate);
    		mLJTempFeeClassSchema.setConfFlag("0"); //是否核销 0：没有财务核销 1：已有财务核销
    		mLJTempFeeClassSchema.setManageCom(ZBankManageCom);
    		mLJTempFeeClassSchema.setPolicyCom(tGI.ManageCom);
    		mLJTempFeeClassSchema.setInsBankAccNo(ZBankAccNo);
        	String aBankCode = getBankCode(ZBankAccNo);//获取银行账号对应的bankcode
        	mLJTempFeeClassSchema.setBankCode(aBankCode);
        	mLJTempFeeClassSchema.setBankCode(pLCContSchema.getBankCode());
    		mLJTempFeeClassSchema.setBankAccNo(pLCContSchema.getBankAccNo());
    		mLJTempFeeClassSchema.setAccName(pLCContSchema.getAccName());
    		mLJTempFeeClassSchema.setOperator(Operator);
    		mLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
    		mLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
    		mLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
    		mLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
    		tLJTempFeeClassSet.add(mLJTempFeeClassSchema);
    		
    		
    		//----------------- modify end -----------------------------------
    	    // 准备传输数据 VData
    	    VData tVData = new VData();
    	    tVData.addElement(tLJTempFeeSet);
    	    tVData.addElement(tLJTempFeeClassSet);
    	    tVData.addElement(tGI);
    	    // 数据传输
    	    TempFeeUI tTempFeeUI = new TempFeeUI();
    	    tTempFeeUI.submitData(tVData, "INSERT");
    	    //如果在Catch中发现异常，则不从错误类中提取错误信息
    	    try {
    	      tError = tTempFeeUI.mErrors;
    	      String[] errTempFeeNo = tTempFeeUI.getResult(); //
    	      if (!tError.needDealError()) {
    	        Content = " 操作成功! ";
    	        FlagStr = "Succ";
    	        if (errTempFeeNo != null) {
    	          for (int n = 0; n < errTempFeeNo.length; n++) {
    	            if (errTempFeeNo[n] == null) {
    	              if (n == 0) { //如果没有纪录直接退出
    	                ShowFinance = "Y"; //显示缴纳金额信息的标记为真
    	                break;
    	              }
    	              Content = Content + " )  原因可能是：1-该纪录您已经录入；2-暂交费号对应的险种编码选择有误";
    	              Content = Content + ". 请您确认或查询后重新操作这些纪录!";
    	              break; //退出
    	            }
    	            if (n == 0) {
    	              Content = "操作完成,但是以下暂交费纪录已经存在,不能保存 ( ";
    	            }
    	            Content = Content + (n + 1) + ": " + errTempFeeNo[n] + " | ";
    	            if (n == errTempFeeNo.length - 1) { //如果是正常结束
    	              Content = Content + " ) 请您确认或查询后重新操作这些纪录! ";
    	            }
    	          } //end for
    	        } //end if
    	        else {
    	          ShowFinance = "Y"; //显示缴纳金额信息的标记为真
    	        }
    	      }
    	      else {
    	        Content = " 失败，原因:" + tError.getFirstError();
    	        FlagStr = "Fail";
    	        info = "核心承保失败";
    	        System.out.println(Content);
    	        return false;
    	      }
    	    }
    	    catch (Exception ex) {
    	      Content = " 失败，原因:" + ex.toString();
    	      FlagStr = "Fail";
    	      info = "核心承保失败";
    	      System.out.println(Content);
    	      return false;
    	    }
    	  }
    	return true;
    } 
    /**
     * 根据网销结算时总公司的机构编码和银行账户。
     * @param szFunc
     * @param szErrMsg
     */
	
    public SSRS getBankAccCode()
    {
    	String sql = "select code,codename from ldcode where codetype = 'InsBankAccNo' and othersign = 'WX'";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0)
    	{
        	return null;
    	}
        return tSSRS;
    }
    /**
     * 根据机构编码获取银行账户。
     * @param szFunc
     * @param szErrMsg
     */
    public String getBankCode(String aBankAccNo)
    {
    	String sql = "select bankcode from ldfinbank where bankaccno = '"+aBankAccNo+"' ";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS ==null || tSSRS.getMaxRow()<=0)
    	{
        	return null;
    	}
        return tSSRS.GetText(1, 1);
    }
    public OMElement buildResponse(OMElement oms,String info){
		addOm(oms, "MESSAGE", info);
		return oms;
	}
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
    public static void main(String[] args) {
    	new Payfee2().sendMail("013936587000073");
//    	new Payfee2().sendMail("013970182000018");
	}
}
