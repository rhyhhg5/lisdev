package com.ecwebservice.querytools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.impl.llom.OMTextImpl;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
public class QueryTool {

	/**
	 * @param args
	 */

	// 解析发送报文中的baseInfo
	public String[] parseBaseInfo(OMElement Oms) {
		String[] baseInfoArray = new String[8];
		String batchno = null;// 批次号
		String sendDate = null;// 报文发送日期
		String sendTime = null;// 报文发送时间
		String messageType = null;// 报文类型
		String pageIndex = null;// 当前页码
		String pageSize = null;// 每页的记录数
		int startRow = 0;// 起始行
		int endRow = 0;// 结束行
		Iterator iter1 = Oms.getChildren();
		// 至根节点
		OMElement Om = (OMElement) iter1.next();

		Iterator baseInfoIterator = Om.getChildren();
		while (baseInfoIterator.hasNext()) {
			// 至baseInfo
			OMElement baseInfoOme = (OMElement) baseInfoIterator.next();
			if (baseInfoOme.getLocalName().equalsIgnoreCase("BASEINFO")) {
				Iterator itemIterator = baseInfoOme.getChildren();

				while (itemIterator.hasNext()) {

					// 至item
					OMElement itemOme = (OMElement) itemIterator.next();

					// System.out.println("In tool
					// 节点名称："+itemOme.getLocalName());
					if (itemOme.getLocalName().equalsIgnoreCase("BATCHNO")) {
						batchno = itemOme.getText();
						System.out.println("批次号：" + batchno);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("SENDDATE")) {
						sendDate = itemOme.getText();
						System.out.println("发送日期：" + sendDate);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("SENDTIME")) {
						sendTime = itemOme.getText();
						System.out.println("发送时间：" + sendTime);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("MESSAGETYPE")) {
						messageType = itemOme.getText();
						System.out.println("报文类型：" + messageType);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("PAGEINDEX")) {
						pageIndex = itemOme.getText();
						System.out.println("当前页码：" + pageIndex);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("PAGESIZE")) {
						pageSize = itemOme.getText();
						System.out.println("每页记录数：" + pageSize);
					}
				}

				// 将baseInfo 写入 String 数组
				baseInfoArray[0] = batchno;
				baseInfoArray[1] = sendDate;
				baseInfoArray[2] = sendTime;
				baseInfoArray[3] = messageType;
				baseInfoArray[4] = pageIndex;
				baseInfoArray[5] = pageSize;
				if ((pageIndex != null && !"".equals(pageIndex))
						&& (pageSize != null && !"".equals(pageSize))) {
					startRow = (Integer.parseInt(pageIndex) - 1)
							* Integer.parseInt(pageSize) + 1;
					endRow = Integer.parseInt(pageIndex)
							* Integer.parseInt(pageSize);
				}
				baseInfoArray[6] = String.valueOf(startRow);
				baseInfoArray[7] = String.valueOf(endRow);
			}
		}
		return baseInfoArray;
	}

	/**
	 * {方法的功能：解析发送报文中的查询条件,并将所有查询条件封装至map，在map中每个查询条件对应一个key-value对，其中key
	 * 为发送报文中查询条件的标签，value为值}
	 * 
	 * @param {引入参数名：OMElement
	 *            Oms} {引入参数说明：完整的发送报文}
	 * @return {返回参数名：Map} {返回参数说明：所有查询条件封装至map}
	 * @exception {异常相关信息：}
	 */
	public Map parseQueryString(OMElement Oms) {
		Map queryStringMap = new HashMap();
		// 获取Input Data
		Iterator iter1 = Oms.getChildren();
		// 至根节点
		OMElement Om = (OMElement) iter1.next();

		Iterator fatherIterator = Om.getChildren();
		while (fatherIterator.hasNext()) {
			// 至Input Data
			OMElement fatherOme = (OMElement) fatherIterator.next();
			if (fatherOme.getLocalName().equalsIgnoreCase("INPUTDATA")) {
				// System.out.println("获取查询条件INPUTDATA");
				Iterator itemIterator = fatherOme.getChildren();
				while (itemIterator.hasNext()) {
					// 至item
					OMElement itemOme = (OMElement) itemIterator.next();

					// 增加对查询条件为多层嵌套的处理
					// Iterator isMulIterator = itemOme.getChildren();

					String queryStringItemKey = itemOme.getLocalName();// 查询字段名称

					if ("CLIST".equals(queryStringItemKey)) {
						System.out.println("查询条件为多层嵌套 ");
						Iterator contListIterator = itemOme.getChildren();

						int contNoMark = 1;
						while (contListIterator.hasNext()) {
							OMElement innerItemOme = (OMElement) contListIterator
									.next();
							Iterator innerQueryStrIterator = innerItemOme
									.getChildren();

							OMElement innerQueryContNoOme = null;
							while (innerQueryStrIterator.hasNext()) {

								innerQueryContNoOme = (OMElement) innerQueryStrIterator
										.next();

							}
							queryStringMap.put(innerQueryContNoOme
									.getLocalName()
									+ (contNoMark++), innerQueryContNoOme
									.getText());
							System.out.println("多层查询条件： "
									+ innerQueryContNoOme.getText());
						}

					} else {
						System.out.println("查询条件为单层");
						String queryStringItemValue = itemOme.getText();// 查询字段值
						queryStringMap.put(queryStringItemKey,
								queryStringItemValue);
					}

					// System.out.println("查询条件: "+queryStringItemValue);

				}
			}
		}
		return queryStringMap;
	}

	// 构建返回报文中的baseInfo
	public OMElement buildReturnBaseInfo(String[] baseInfoString, String state,
			String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement baseInfoOme = fac.createOMElement("BASEINFO", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		if (!"".equals(baseInfoString) && baseInfoString != null) {
			tool.addOm(baseInfoOme, "BATCHNO", baseInfoString[0]);
			tool.addOm(baseInfoOme, "SENDDATE", baseInfoString[1]);
			tool.addOm(baseInfoOme, "SENDTIME", baseInfoString[2]);
			tool.addOm(baseInfoOme, "MESSAGETYPE", baseInfoString[3]);
		}

		tool.addOm(baseInfoOme, "STATE", state);
		tool.addOm(baseInfoOme, "ERRCODE", errCode);
		tool.addOm(baseInfoOme, "ERRINFO", errInfo);

		return baseInfoOme;

	}

	// 构建返回报文中的baseInfo(分页 包含记录总数)
	public OMElement buildReturnBaseInfo(String[] baseInfoString,
			String recordCount, String state, String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement baseInfoOme = fac.createOMElement("BASEINFO", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		if (!"".equals(baseInfoString) && baseInfoString != null) {
			tool.addOm(baseInfoOme, "BATCHNO", baseInfoString[0]);
			tool.addOm(baseInfoOme, "SENDDATE", baseInfoString[1]);
			tool.addOm(baseInfoOme, "SENDTIME", baseInfoString[2]);
			tool.addOm(baseInfoOme, "MESSAGETYPE", baseInfoString[3]);
		}

		tool.addOm(baseInfoOme, "TOTALROWNUM", recordCount);// 记录总数
		tool.addOm(baseInfoOme, "STATE", state);
		tool.addOm(baseInfoOme, "ERRCODE", errCode);
		tool.addOm(baseInfoOme, "ERRINFO", errInfo);

		return baseInfoOme;

	}

	// 拼接where part sql
	public String getWherePartSql(Map queryStringMap) {
		StringBuffer querySql = new StringBuffer();
		Set queryKeySet = queryStringMap.keySet();
		Iterator queryKeyIterator = queryKeySet.iterator();
		while (queryKeyIterator.hasNext()) {
			Object queryKeyObject = queryKeyIterator.next();
			String queryStringItemValue = queryStringMap.get(queryKeyObject)
					.toString();
			// 获取字段名称
			String queryKeyString = queryKeyObject.toString();

			String wherePartSql = "";
			// 对每个保单号进行处理
			wherePartSql = queryKeyString + "='" + queryStringItemValue
					+ "' and ";
			querySql.append(wherePartSql);
		}

		String stringQuerySql = querySql.substring(0, querySql.length() - 4);

		return stringQuerySql;
	}

	// 将一个sql 添加where 子句 最后去掉String 的后4个字符（即and ）
	public String addWherePartCutRedundanceString(Object frontSql,
			String wherePartSql) {
		String sql = frontSql.toString();
		sql = sql.concat(wherePartSql);
		sql = sql.substring(0, sql.length() - 3);

		// if (true) {
		// System.out.println("1"+frontSql.getClass().getName());
		// System.out.println("2"+frontSql.toString());
		//
		// }
		return sql;

	}

	// 获取发送报文中的查询条件（contNo）
	public String getContNoInQueryString(OMElement Oms) {
		String contNo = null;
		Map queryStringMap = parseQueryString(Oms);// 包含查询字段的名称及值

		System.out.println("开始打印查询条件： ");
		Set queryKeySet = queryStringMap.keySet();
		Iterator queryKeyIterator = queryKeySet.iterator();
		if (queryKeyIterator.hasNext()) {
			Object queryKeyObject = queryKeyIterator.next();
			contNo = (String) queryStringMap.get(queryKeyObject);
			System.out.println("查询的保单号为： " + contNo);
		}
		return contNo;
	}

	// 获取封装后结果集中的唯一Map
	public Map getResultOnlyMap(Set resultSet) {
		Map resultMap = null;
		// 遍历HashSet
		Iterator resultIterator = resultSet.iterator();
		if (resultIterator.hasNext()) {
			resultMap = (Map) resultIterator.next();
		}
		return resultMap;
	}

	// 付费方式
	public String decodePayMode(String payModeCode) {
		if (!"".equals(payModeCode) && payModeCode != null) {
			return "银行转账";
		} else {
			return "现金";
		}

	}

	public String buildWherePartSqlByOme(OMElement Oms) {
		Map queryMap = parseQueryString(Oms);
		String wherePartSql = getWherePartSql(queryMap);
		return wherePartSql;

	}

	// 替换查询sql 中的字段
	public String replaceQueryField(String oldSql, Map replaceMap) {
		String newSql = oldSql;
		Set replaceSet = replaceMap.keySet();
		Iterator replaceIterator = replaceSet.iterator();
		while (replaceIterator.hasNext()) {
			Object oldFieldObj = replaceIterator.next();
			String oldField = (String) oldFieldObj;
			String newField = (String) replaceMap.get(oldFieldObj);
			if (!"".equals(newSql) && newSql != null) {
				newSql = newSql.replaceAll(oldField, newField);
			}

		}
		return newSql;

	}
	
	public Map putSSRSToMap(SSRS resultSSRS, Map map, boolean grpFlag) {
		ExeSQL tExeSQL = new ExeSQL();
		if (resultSSRS.MaxRow > 0) {
			int pointer;
			for (pointer = 1; pointer <= resultSSRS.MaxRow; pointer++) {
				String contNo = resultSSRS.GetText(pointer, 1);
				System.out.println("保单号列表：" + contNo);
				// 判断是否为团体保单号 grpFlag=true 为团体保单号
				String contTypeSql;
				if (grpFlag) {
					contTypeSql = "select conttype from lccont where grpcontno='"
							+ contNo + "'";
				} else {
					contTypeSql = "select conttype from lccont where contno='"
							+ contNo + "'";
				}

				String contType = tExeSQL.getOneValue(contTypeSql);
				map.put(contNo, contType);
			}
		}
		return map;
	}
	public Map putSSRSToMap2(SSRS resultSSRS, Map map, boolean grpFlag) {
		ExeSQL tExeSQL = new ExeSQL();
		if (resultSSRS.MaxRow > 0) {
			int pointer;
			for (pointer = 1; pointer <= resultSSRS.MaxRow; pointer++) {
				String contNo = resultSSRS.GetText(pointer, 1);
				System.out.println("保单号列表：" + contNo);
				// 判断是否为团体保单号 grpFlag=true 为团体保单号
				String contTypeSql;
				String contNoSql = "";
				String flag = "";   //标记分单团单，0表示分单，1表示团单。
				if (grpFlag) {
					contTypeSql = "select conttype from lccont where contno='"
							+ contNo + "'";
					flag = "1";
				} else {
					contNoSql = "select grpcontno from lccont where contno='"+ contNo +"'"; //当传入的contNo是分单号时，获取其团单号
					System.out.println("获取团单号SQL：："+contNoSql);
					contTypeSql = "select conttype from lccont where contno='"
							+ contNo + "'";
					contNo = tExeSQL.getOneValue(contNoSql);   //将团单号放入map中
					flag = "0";
				}
				
				String contType = tExeSQL.getOneValue(contTypeSql);
				map.put("contNo", contNo);
				map.put("contType", contType);
				map.put("flag", flag);
			}
		}
		return map;
	}

	// map 中封装新 旧字段之间的映射关系
	public Map getReplaceFieldMap(ArrayList oldFieldList, ArrayList newFieldList) {
		Map replaceFieldMap = new HashMap();
		Iterator oldFieldIterator = oldFieldList.iterator();
		Iterator newFieldIterator = newFieldList.iterator();
		while (oldFieldIterator.hasNext()) {
			String oldField = (String) oldFieldIterator.next();
			String newField = (String) newFieldIterator.next();
			replaceFieldMap.put(oldField, newField);
		}

		return replaceFieldMap;
	}

	// 个险绑定至保单号--暂时废弃
	public Map bindToContNo(OMElement Oms) {
		// 校验证件类型
		QueryTool qTool = new QueryTool();
		ExeSQL tExeSQL = new ExeSQL();

		// 获取校验的where part sql
		String wherePartSql = qTool.buildWherePartSqlByOme(Oms);
		System.out.println("获取校验insured的where part sql！" + wherePartSql);
		Map replaceMap = new HashMap();// 封装要替换的sql字段
		ArrayList oldFieldList = new ArrayList();
		oldFieldList.add("NAME");
		oldFieldList.add("SEX");
		oldFieldList.add("BIRTHDAY");

		ArrayList newFieldList = new ArrayList();
		newFieldList.add("AppntName");
		newFieldList.add("AppntSex");
		newFieldList.add("AppntBirthday");

		// replaceMap.put("NAME", "AppntName");
		// replaceMap.put("SEX", "AppntSex");
		// replaceMap.put("BIRTHDAY", "AppntBirthday");
		replaceMap = qTool.getReplaceFieldMap(oldFieldList, newFieldList);

		String appntWherePartSql = qTool.replaceQueryField(wherePartSql,
				replaceMap);
		// appntWherePartSql = wherePartSql.replaceAll("SEX", "AppntSex");
		// appntWherePartSql = wherePartSql.replaceAll("BIRTHDAY",
		// "AppntBirthday");
		System.out.println("获取校验appnt的where part sql！" + appntWherePartSql);

		// 在lcinsured lbinsured 中进行校验
		String lcinsuredSql = "select contno from lcinsured where 1=1 and "
				+ wherePartSql;
		String lbinsuredSql = "select contno from lbinsured where 1=1 and "
				+ wherePartSql;
		String querySql = lcinsuredSql + " union  " + lbinsuredSql;
		System.out.println("打印 insured sql:" + querySql);
		SSRS contNoSSRS = tExeSQL.execSQL(querySql);
		Map schemaMap = new HashMap();

		qTool.putSSRSToMap(contNoSSRS, schemaMap, false);

		// 在 lcappnt lbappnt 中进行校验
		String lcappntSql = "select contno from lcappnt where 1=1 and "
				+ appntWherePartSql;
		String lbappntSql = "select contno from lbappnt where 1=1 and "
				+ appntWherePartSql;
		String appntSql = lcappntSql + " union " + lbappntSql;
		System.out.println("打印Appnt sql:" + appntSql);
		SSRS appntSSRS = tExeSQL.execSQL(appntSql);

		qTool.putSSRSToMap(appntSSRS, schemaMap, false);
		return schemaMap;
	}

	/**
	 * {方法的功能：个险绑定至保单号 根据报文中的条件动态构建校验sql}
	 * 
	 * @param {引入参数名：OMElement
	 *            Oms} {引入参数说明：完整的发送报文}
	 * @return {返回参数名：Map} {返回参数说明：Map中封装结果集 每个key-value对应一个保单号-保单类型}
	 * @exception {异常相关信息：}
	 */
	public Map bindToContNoOtherWay(OMElement Oms) {
		// 校验证件类型
		Map schemaMap = new HashMap();
		QueryTool qTool = new QueryTool();
		ExeSQL tExeSQL = new ExeSQL();
		boolean flag=false;
		StringBuffer lcinsuredSql = new StringBuffer();
		lcinsuredSql.append("select contno from lcinsured where 1=1  ");
		StringBuffer lbinsuredSql = new StringBuffer();
		lbinsuredSql.append("select contno from lbinsured where 1=1  ");
		StringBuffer lcappntSql = new StringBuffer();
		lcappntSql.append("select contno from lcappnt where 1=1  ");
		StringBuffer lbappntSql = new StringBuffer();
		lbappntSql.append("select contno from lbappnt where 1=1  ");
		StringBuffer lcbnfSql = new StringBuffer();
		lcbnfSql.append("select contno from lcbnf a where 1=1 ");
		StringBuffer lbbnfSql = new StringBuffer();
		lbbnfSql.append("select contno from lbbnf b where 1=1 ");
		Map queryMap = qTool.parseQueryString(Oms);
		String name = (String)queryMap.get("NAME");
//		Iterator queryMapIterator = queryMap.keySet().iterator();
//		while (queryMapIterator.hasNext()) {
//			String queryString = (String) queryMapIterator.next();
			if (name!=null || !name.equals("")) {
				lcinsuredSql.append("and name='" + name + "' ");
				lbinsuredSql.append("and name='" + name + "' ");
				lcappntSql.append("and AppntName='" + name + "' ");
				lbappntSql.append("and AppntName='" + name + "' ");
				lcbnfSql.append("and name='" + name + "' ");
				lbbnfSql.append("and name='" + name + "' ");
//				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
				String sex = (String)queryMap.get("SEX");
			if (sex!=null || !sex.equals("")) {
				lcinsuredSql.append("and sex='" + sex + "' ");
				lbinsuredSql.append("and sex='" + sex + "' ");
				lcappntSql.append("and AppntSex='" + sex + "' ");
				lbappntSql.append("and AppntSex='" + sex + "' ");
				lcbnfSql.append("and sex='" + sex + "' ");
				lbbnfSql.append("and sex='" + sex + "' ");
//				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			String birthday = (String)queryMap.get("BIRTHDAY");
			if (birthday!=null || !birthday.equals("")) {
				lcinsuredSql.append("and BIRTHDAY='" + birthday + "' ");
				lbinsuredSql.append("and BIRTHDAY='"+ birthday + "' ");
				lcappntSql.append("and AppntBirthday='"+ birthday + "' ");
				lbappntSql.append("and AppntBirthday='"+ birthday + "' ");
				lcbnfSql.append("and BIRTHDAY='" + birthday + "' ");
				lbbnfSql.append("and BIRTHDAY='" + birthday + "' ");
				
//				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			String idno = (String)queryMap.get("IDNO");
			if (idno!=null || !idno.equals("")) {
				lcinsuredSql.append("and IDNO='" + idno + "' ");
				lbinsuredSql.append("and IDNO='" + idno + "' ");
				lcappntSql.append("and IDNO='" + idno + "' ");
				lbappntSql.append("and IDNO='" + idno + "' ");
				lcbnfSql.append("and IDNO='" + idno + "' ");
				lbbnfSql.append("and IDNO='"  + idno + "' ");
//				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			String idType = (String)queryMap.get("IDTYPE");
			if (idType!=null || !idType.equals("")) {
				lcinsuredSql.append("and IDTYPE='" + idType + "' ");
				lbinsuredSql.append("and IDTYPE='" + idType + "' ");
				lcappntSql.append("and IDTYPE='" + idType + "' ");
				lbappntSql.append("and IDTYPE='" + idType + "' ");
				lcbnfSql.append("and IDTYPE='" + idType + "' ");
				lbbnfSql.append("and IDTYPE='" + idType + "' ");
//				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			String contNo = (String)queryMap.get("CONTNO"); 
			if (contNo!=null || !contNo.equals("")) {
				String grpcontnosql = "select prtno from lcgrpcont where grpcontno = '"+contNo+"'";
				SSRS GrpContnoSSRS = tExeSQL.execSQL(grpcontnosql);
				//ContNo是团单号
				if(GrpContnoSSRS.getMaxRow()>0){
					lcinsuredSql.append("and GRPCONTNO='" + contNo + "' ");
					lbinsuredSql.append("and GRPCONTNO='" + contNo + "' ");
					lcappntSql.append("and GRPCONTNO='" + contNo + "' ");
					lbappntSql.append("and GRPCONTNO='" + contNo + "' ");
					lcbnfSql.append("and exists(select 1 from lcinsured where " +
					"contno = a.contno and insuredno = a.insuredno and grpcontno = '"+ contNo +"') ");
					lbbnfSql.append("and exists(select 1 from lbinsured where " +
					"contno = b.contno and insuredno = b.insuredno and grpcontno = '"+ contNo +"') ");
					flag=true;
//					continue;
				}else{  //contNo是分单号
					lcinsuredSql.append("and CONTNO='" + contNo + "' ");
					lbinsuredSql.append("and CONTNO='" + contNo + "' ");
					lcappntSql.append("and CONTNO='" + contNo + "' ");
					lbappntSql.append("and CONTNO='" + contNo + "' ");
					lcbnfSql.append("and CONTNO='" + contNo + "' ");
					lbbnfSql.append("and CONTNO='" + contNo + "' ");
//					continue;
					// System.out.println("test sql:"+lcinsuredSql);
				}
			}
			
//		}

		String insuredSql = lcinsuredSql.toString() + " union "
				+ lbinsuredSql.toString();
		String appntSql = lcappntSql.toString() + " union "
				+ lbappntSql.toString();
		String bnfSql = lcbnfSql.toString() + " union "
				+ lbbnfSql.toString();

		SSRS insuredSSRS = tExeSQL.execSQL(insuredSql);
		qTool.putSSRSToMap2(insuredSSRS, schemaMap, flag);

		SSRS appntSSRS = tExeSQL.execSQL(appntSql);
		qTool.putSSRSToMap2(appntSSRS, schemaMap, flag);
		
		SSRS bnfSSRS = tExeSQL.execSQL(bnfSql);
		qTool.putSSRSToMap2(bnfSSRS, schemaMap, flag);

		return schemaMap;
	}
	
	/**
	 * {方法的功能：个险绑定至保单号 根据报文中的条件动态构建校验sql}
	 * 自动绑定 过滤掉未承保及退保状态
	 * @param {引入参数名：OMElement
	 *            Oms} {引入参数说明：完整的发送报文}
	 * @return {返回参数名：Map} {返回参数说明：Map中封装结果集 每个key-value对应一个保单号-保单类型}
	 * @exception {异常相关信息：}
	 */
	public Map bindToAUTOContNoOtherWay(OMElement Oms) {
		// 校验证件类型
		Map schemaMap = new HashMap();
		QueryTool qTool = new QueryTool();
		ExeSQL tExeSQL = new ExeSQL();
		StringBuffer lcinsuredSql = new StringBuffer();
		lcinsuredSql.append("select a.contno from lcinsured a,lccont b where 1=1  ");
		StringBuffer lcappntSql = new StringBuffer();
		lcappntSql.append("select a.contno from lcappnt a,lccont b where 1=1  ");
		StringBuffer lcbnfSql = new StringBuffer();
		lcbnfSql.append("select a.contno from lcbnf a,lccont b where 1=1 ");
		Map queryMap = qTool.parseQueryString(Oms);
		Iterator queryMapIterator = queryMap.keySet().iterator();
		while (queryMapIterator.hasNext()) {
			String queryString = (String) queryMapIterator.next();
			if ("NAME".equalsIgnoreCase(queryString)) {
				lcinsuredSql.append("and a.name='" + queryMap.get(queryString)
						+ "' ");
				lcappntSql.append("and a.AppntName='" + queryMap.get(queryString)
						+ "' ");
				lcbnfSql.append("and a.name='" + queryMap.get(queryString)
						+ "' ");
				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			if ("SEX".equalsIgnoreCase(queryString)) {
				lcinsuredSql.append("and a.sex='" + queryMap.get(queryString)
						+ "' ");
				lcappntSql.append("and a.AppntSex='" + queryMap.get(queryString)
						+ "' ");
				lcbnfSql.append("and a.sex='" + queryMap.get(queryString)
						+ "' ");
				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			if ("BIRTHDAY".equalsIgnoreCase(queryString)) {
				lcinsuredSql.append("and a.BIRTHDAY='"
						+ queryMap.get(queryString) + "' ");
				lcappntSql.append("and a.AppntBirthday='"
						+ queryMap.get(queryString) + "' ");
				lcbnfSql.append("and a.BIRTHDAY='" 
						+ queryMap.get(queryString) + "' ");
				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			if ("IDNO".equalsIgnoreCase(queryString)) {
				lcinsuredSql.append("and a.IDNO='" + queryMap.get(queryString)
						+ "' ");
				lcappntSql.append("and a.IDNO='" + queryMap.get(queryString)
						+ "' ");
				lcbnfSql.append("and a.IDNO='" 
						+ queryMap.get(queryString) + "' ");
				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			if ("IDTYPE".equalsIgnoreCase(queryString)) {
				lcinsuredSql.append("and a.IDTYPE='" + queryMap.get(queryString)
						+ "' ");
				lcappntSql.append("and a.IDTYPE='" + queryMap.get(queryString)
						+ "' ");
				lcbnfSql.append("and a.IDTYPE='" + queryMap.get(queryString)
						+ "' ");
				continue;
				// System.out.println("test sql:"+lcinsuredSql);
			}
			
		}
		lcinsuredSql.append("and a.contno=b.contno and b.appflag = '1' ");
		lcappntSql.append("and a.contno=b.contno and b.appflag = '1' ");
		lcbnfSql.append("and a.contno=b.contno and b.appflag = '1' ");

		String insuredSql = lcinsuredSql.toString() ;
		String appntSql = lcappntSql.toString() ;
		String bnfSql = lcbnfSql.toString() ;

		SSRS insuredSSRS = tExeSQL.execSQL(insuredSql);
		qTool.putSSRSToMap(insuredSSRS, schemaMap, false);

		SSRS appntSSRS = tExeSQL.execSQL(appntSql);
		qTool.putSSRSToMap(appntSSRS, schemaMap, false);
		
		SSRS bnfSSRS = tExeSQL.execSQL(bnfSql);
		qTool.putSSRSToMap(bnfSSRS, schemaMap, false);

		return schemaMap;
	}

	// 团险绑定
	public Map grpBindToContNo(OMElement Oms) {
		Map schemaMap = new HashMap();
		QueryTool qTool = new QueryTool();
		ExeSQL tExeSQL = new ExeSQL();
		Map queryMap = qTool.parseQueryString(Oms);
		String contNo = (String) queryMap.get("GRPCONTNO");
		String grpCode = (String) queryMap.get("GRPCODE");
		String grpName = (String) queryMap.get("GRPNAME");

		//
		String verifySql = "select grpcontno from lcgrpappnt a where " +
				" a.name='"+grpName+"' and EXISTS (select 1 from lcgrpcont b where  b.grpcontno=a.grpcontno)";
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(verifySql);
		if (contNo != null && !"".equals(contNo)) {
			queryBuffer.append(" and grpcontno='" + contNo + "'  ");
		}
		if(grpCode != null && !"".equals(grpCode)){
			queryBuffer.append(" and organcomcode='" + grpCode + "'  ");
		}
		SSRS appntSSRS = tExeSQL.execSQL(queryBuffer.toString());
		qTool.putSSRSToMap(appntSSRS, schemaMap, true);

		return schemaMap;
	}

	// 获取根节点名称
	public String getRootTag(OMElement Oms) {
		Iterator iter1 = Oms.getChildren();
		// 至根节点
		OMElement Om = (OMElement) iter1.next();
		return Om.getLocalName();

	}

	// 构建分页sql

	/**
	 * {方法的功能：构建DB2分页sql}
	 * 
	 * @param {引入参数名：String
	 *            subSql 欲分页的sql,String ordrerCol 排序字段，int startRow 起始行，int
	 *            pageSize 每页记录数} {引入参数说明：}
	 * @return {返回参数名：String} {返回参数说明：形成完整的分页sql}
	 * @exception {异常相关信息：}
	 */
	/** 
	* @desc 
	* @author Yuanzw
	* 创建时间：Jul 16, 2010 6:30:05 PM
	* @param       
	* @return      
	* @exception  
	*/
	public String buildPageSql(String subSql, String ordrerCol, int startRow,
			int pageSize) {
		// subSql = subSql.toLowerCase();
		StringBuffer SqlBuffer = new StringBuffer();

		StringBuffer subSqlBuffer = new StringBuffer();
		subSqlBuffer.append(subSql);
		int offset = subSqlBuffer.lastIndexOf("from");
		if (offset > 0) {
			subSqlBuffer.insert(offset, ",rank() over(ORDER BY " + ordrerCol
					+ " ASC) AS rn ");
		}
		// System.out.println("打印截取位置:" + offset);

		System.out.println("构建分页sql中1--打印拼接后的子串:" + subSqlBuffer.toString());
		String outSideSql = "SELECT * FROM () AS a1 WHERE a1.rn>= " + startRow
				+ " fetch first " + pageSize + " rows only  ";
		SqlBuffer.append(outSideSql);
		int outOffset = outSideSql.indexOf(")");
		SqlBuffer.insert(outOffset, subSqlBuffer.toString());
		System.out.println("构建分页sql中2--打印拼接后的sql:" + SqlBuffer.toString());

		return SqlBuffer.toString();

	}
	
	
	
	
	/** 
	* @desc 重写
	* @author Yuanzw
	* 创建时间：Jul 16, 2010 6:31:25 PM
	* @param       int fromIndex：第几个"from"
	* @return      
	* @exception  
	*/
	public String buildPageSql(String subSql, int pageIndex,int pageSize)
	{
		// subSql = subSql.toLowerCase();
//		StringBuffer SqlBuffer = new StringBuffer();
//
//		StringBuffer subSqlBuffer = new StringBuffer();
//		subSqlBuffer.append(subSql);
//		int offset = subSqlBuffer.indexOf("from",fromIndex);
//		if (offset > 0) {
//			subSqlBuffer.insert(offset, ",rank() over(ORDER BY " + ordrerCol
//					+ " ASC) AS rn ");
//		}
//		// System.out.println("打印截取位置:" + offset);
//
//		System.out.println("构建分页sql中1--打印拼接后的子串:" + subSqlBuffer.toString());
//		String outSideSql = "SELECT * FROM () AS a1 WHERE a1.rn>= " + startRow
//				+ " fetch first " + pageSize + " rows only  ";
//		SqlBuffer.append(outSideSql);
//		int outOffset = outSideSql.indexOf(")");
//		SqlBuffer.insert(outOffset, subSqlBuffer.toString());
//		System.out.println("构建分页sql中2--打印拼接后的sql:" + SqlBuffer.toString());
		
		StringBuffer pagebuf = new StringBuffer();
		pagebuf.append("select * from (select aa.*,rownumber() over() as rowid from (");
		pagebuf.append(subSql);
		pagebuf.append(") as aa ");
		pagebuf.append(") as bb where 1 = 1 ");
		
		pagebuf.append("and rowid >= (").append(pageIndex).append("-1) *").append(pageSize).append(" + 1 ");
		pagebuf.append("and rowid <= ").append(pageIndex).append("*").append(pageSize).append(" ");

		return pagebuf.toString();

	}

	// decode 缴费间隔
	public String decodePayIntv(String payIntvCode) {
		String payIntvSql = "select codename from ldcode where codetype = 'payintv' and code = '"
				+ payIntvCode + "'";
		ExeSQL tExeSQL = new ExeSQL();
		String payIntv = tExeSQL.getOneValue(payIntvSql);
		return payIntv;
	}

	// decode 职业编码
	public String decodeOccupationcode(String occupationcode) {
		ExeSQL tExeSQL = new ExeSQL();
		String occupationcodeSql = "select a.occupationname from ldoccupation  a where occupationcode='"
				+ occupationcode + "'";
		String occupation = tExeSQL.getOneValue(occupationcodeSql);
		return occupation;

	}

	// 查询保全中的批改类型名称
	public String decodeEdorType(String edorTypeCode) {
		ExeSQL tExeSQL = new ExeSQL();
		String edorTypeSql = "select edorname from lmedoritem where edorcode='"
				+ edorTypeCode + "'";
		String edorTypeName = tExeSQL.getOneValue(edorTypeSql);
		return edorTypeName;

	}

	// date 20101126 by gzh
//	 解析发送报文中的baseInfo
	public String[] parseIputInfo(OMElement Oms) {
		String[] inputInfoArray = new String[6];
		String CONTNO = null;//保单号
		String NAME = null;//用户姓名
		String SEX = null;//用户性别
		String IDTYPE = null;//用户证件类型
		String IDNO = null;//用户证件号码
		String BIRTHDAY = null;//保单用户出生日期
		Iterator iter1 = Oms.getChildren();
		// 至根节点
		OMElement Om = (OMElement) iter1.next();

		Iterator inputInfoIterator = Om.getChildren();
		while (inputInfoIterator.hasNext()) {
			// 至baseInfo
			OMElement baseInfoOme = (OMElement) inputInfoIterator.next();
			if (baseInfoOme.getLocalName().equalsIgnoreCase("INPUTDATA")) {
				Iterator itemIterator = baseInfoOme.getChildren();
				while (itemIterator.hasNext()) {
					// 至item
					OMElement itemOme = (OMElement) itemIterator.next();
					if (itemOme.getLocalName().equalsIgnoreCase("CONTNO")) {
						CONTNO = itemOme.getText();
						System.out.println("保单号：" + CONTNO);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("NAME")) {
						NAME = itemOme.getText();
						System.out.println("姓名：" + NAME);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("SEX")) {
						SEX = itemOme.getText();
						System.out.println("性别：" + SEX);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("IDTYPE")) {
						IDTYPE = itemOme.getText();
						System.out.println("证件类型：" + IDTYPE);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("IDNO")) {
						IDNO = itemOme.getText();
						System.out.println("证件号码：" + IDNO);
					}
					if (itemOme.getLocalName().equalsIgnoreCase("BIRTHDAY")) {
						BIRTHDAY = itemOme.getText();
						System.out.println("保单用户出生日期：" + BIRTHDAY);
					}
				}

				// 将baseInfo 写入 String 数组
				inputInfoArray[0] = CONTNO;
				inputInfoArray[1] = NAME;
				inputInfoArray[2] = SEX;
				inputInfoArray[3] = IDTYPE;
				inputInfoArray[4] = IDNO;
				inputInfoArray[5] = BIRTHDAY;
			}
		}
		return inputInfoArray;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
