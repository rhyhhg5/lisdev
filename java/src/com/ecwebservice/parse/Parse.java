package com.ecwebservice.parse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMDocument;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMException;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Parse {
	// Document可以看作是XML在内存中的一个镜像,那么一旦获取这个Document 就意味着可以通过对
	// 内存的操作来实现对XML的操作,首先第一步获取XML相关的Document
	private Document doc = null;

	public void init(String xmlFile) throws Exception {
		// 很明显该类是一个单例,先获取产生DocumentBuilder工厂
		// 的工厂,在通过这个工厂产生一个DocumentBuilder,
		// DocumentBuilder就是用来产生Document的
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		// 这个Document就是一个XML文件在内存中的镜像
		doc = db.parse(new File(xmlFile));
	}

	// 该方法负责把XML文件的内容显示出来
	public void viewXML(String xmlFile) throws Exception {
		this.init(xmlFile);
		// 在xml文件里,只有一个根元素,先把根元素拿出来看看
		Element element = doc.getDocumentElement();
		System.out.println("根元素为:" + element.getTagName());
		
		//遍历
		
		NodeList rootNodeList = doc.getFirstChild().getChildNodes();
		for(int rootNodeListPointer=0;rootNodeListPointer<rootNodeList.getLength(); rootNodeListPointer++){
			Node childNode = rootNodeList.item(rootNodeListPointer);
			if (childNode instanceof Element) {
				System.out.println("根元素的子节点为:" + childNode.getNodeName());
				String rootZChildName = childNode.getNodeName();
				
				NodeList nodeList = doc.getElementsByTagName(rootZChildName);
				System.out.println("dbstore节点链的长度:" + nodeList.getLength());

				Node fatherNode = nodeList.item(0);
				System.out.println("父节点为:" + fatherNode.getNodeName());
				parseNode(fatherNode);
			}
			
		}

		


	}

	public void parseNode(Node fatherNode){

		// 把父节点的属性拿出来
		NamedNodeMap attributes = fatherNode.getAttributes();

		for (int i = 0; i < attributes.getLength(); i++) {
			Node attribute = attributes.item(i);
			System.out.println("dbstore的属性名为:" + attribute.getNodeName()
					+ " 相对应的属性值为:" + attribute.getNodeValue());
		}

		NodeList childNodes = fatherNode.getChildNodes();
		System.out.println(childNodes.getLength());
		for (int j = 0; j < childNodes.getLength(); j++) {
			Node childNode = childNodes.item(j);
			// 如果这个节点属于Element ,再进行取值
			
			if (childNode instanceof Element&&childNode.getFirstChild()!=null) {
				// System.out.println("子节点名为:"+childNode.getNodeName()+"相对应的值为"+childNode.getFirstChild().getNodeValue());
				if(childNode.getChildNodes().getLength()==1){
					String elementValue = null ;
					if( childNode.getFirstChild()!=null){
						elementValue = childNode.getFirstChild().getNodeValue();
					}
					
					System.out.println("子节点名为:" + childNode.getNodeName()
							+ "  相对应的值为" + elementValue+"child 节点长度 "+childNode.getChildNodes().getLength());
				}else{
					parseNode(childNode);
				}
				
				
			}
			
			
		}
	}
	
	public OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
	//Axiom读XML
	public void axiomReadXML(String xmlFile){
		 // 首先构建parser，
	    XMLStreamReader parser = null;
		try {
			parser = XMLInputFactory.newInstance().createXMLStreamReader(
			        new FileInputStream(xmlFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // 还需要builder对象，
	    StAXOMBuilder builder = new StAXOMBuilder(parser);
	    // get the root element 
	    // OMElement documentElement = builder.getDocumentElement();
	    OMDocument doc = builder.getDocument();

//	    OMElement cre = doc.getOMDocumentElement().getFirstChildWithName(new QName("fool"));
	     System.out.println(" 节点信息："+doc.getOMDocumentElement().toString());
	    Iterator  creIterator = doc.getOMDocumentElement().getChildrenWithName(new QName("PolInfo"));
	    OMElement cre = null;
	    while(creIterator.hasNext()){
	    	cre = (OMElement) creIterator.next();
	    	System.out.println(cre);
	    }

	    // OMElement有一系列的get方法来获得内容。

//	    try {
//			cre.serialize(System.out);
//		} catch (XMLStreamException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} // cache on
//	    try {
//			cre.serializeAndConsume(System.out);
//		} catch (XMLStreamException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} // cache off

	    // will NOT build the OMTree in the memory. 
	    // So you are at your own risk of losing information.
	    try {
			String creStr = cre.toStringWithConsume();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	    // call toString, will build the OMTree in the memory. 
	  


	}
	
	
	public void bulidAXIOMFromXML(){
		try {
//			   File file = new File("java/src/com/ecwebservice/parse/line-item.xml");
			   
			   File file = new File("line-item.xml");
			   FileInputStream fis = new FileInputStream(file);
			   XMLInputFactory xif = XMLInputFactory.newInstance();
			   XMLStreamReader reader = xif.createXMLStreamReader(fis);
			   StAXOMBuilder builder = new StAXOMBuilder(reader);
			   OMElement lineItem = builder.getDocumentElement();
			   XMLOutputFactory xof = XMLOutputFactory.newInstance();
			   XMLStreamWriter writer = xof.createXMLStreamWriter(System.out);

			   lineItem.serialize(writer);
			   writer.flush();
			   OMAttribute quantity = lineItem.getAttribute(
			         new QName("http://openuri.org/easypo", "quantity"));
		   System.out.println("quantity= " + quantity.getAttributeValue());
			   OMElement price = lineItem.getFirstChildWithName(
			         new QName("http://openuri.org/easypo", "price"));
			   System.out.println("price= " + price.getText());
			   OMElement description = lineItem.getFirstChildWithName(
			         new QName("http://openuri.org/easypo", "description"));
			   System.out.println("description= " + description.getText());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void main(String[] args) throws Exception {
		Parse parse = new Parse();

		// 我的XML文件
//		parse.viewXML("java/src/com/ecwebservice/parse/netct.xml");
//		parse.axiomReadXML("java/src/com/ecwebservice/parse/5.xml");
		parse.bulidAXIOMFromXML();
	}
}
