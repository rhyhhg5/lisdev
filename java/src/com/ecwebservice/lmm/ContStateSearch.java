package com.ecwebservice.lmm;

import java.util.Iterator;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.ecwebservice.querytools.QueryTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * 驴妈妈保单查询
 * @author Administrator
 *
 */
public class ContStateSearch {
	private String info;
	private String appntName;
	private String contNo;
	private String idType;
	private String idNo;
	private String startDate;
	private String endDate;
	private String cardStatus;
	private String cvalistartDate;
	private String cvaliendDate;
	private String cardHeading;
	private String pageIndex;
	private String pageSize;
	private String totalcount;
	SSRS tSsrs = new SSRS();
	
	public OMElement queryData(OMElement oms){
		boolean flag;
		OMElement responseOME = null;
		//解析报文
		flag = paraseXML(oms);
		if(!flag){
			System.out.println(info);
			responseOME = buildErrorOME(info);
			return responseOME;
		}
		//获取所需信息
		flag = getInfoFromDB();
		if(!flag){
			System.out.println(info);
			responseOME = buildErrorOME(info);
			return responseOME;
		}
		responseOME = buildResponseOME(tSsrs);
		return responseOME;
	}
	private boolean paraseXML(OMElement oms){
		System.out.println("接收的报文为："+oms);
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement Om = (OMElement)iterator.next();
				if(Om.getLocalName().equals("LMMCONTSTATESEARCH")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("APPNTNAME")){
							appntName = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CONTNO")){
							contNo = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("IDTYPE")){
							idType = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("IDNO")){
							idNo = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("STARTDATE")){
							startDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("ENDDATE")){
							endDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CVALISTARTDATE")){
							cvalistartDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CVALIENDDATE")){
							cvaliendDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CARDSTATUS")){
							cardStatus = child_Element.getText().trim();
							
						}
						if(child_Element.getLocalName().equals("CARDHEADING")){
							cardHeading = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("PAGEINDEX")){
							pageIndex = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("PAGESIZE")){
							pageSize = child_Element.getText().trim();
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			info = "获取报文中的四种保单号字符串失败";
			return false;
		}
		return true;
	}
	//查询所需信息
	private boolean getInfoFromDB(){
		QueryTool qTool = new QueryTool();
		try {
//			1.根据appntName，idtype,idno查wfappntlist
			String infoSQL = "select li.cardno,app.name,app.idno,app.birthday,li.prem,cont.senddate,li.cvalidate,(select distinct(from) from wfcontlist cont where cont.cardno = li.cardno),cont.messagetype,li.cardstatus,cont.certifycode,cont.riskcode ,cont.bak3 from "+ 
							"WFAPPNTLIST app "+
							"inner join LICARDACTIVEINFOLIST li on app.cardno = li.cardno "+
							"inner join WFCONTLIST cont on app.cardno = cont.cardno "+
							"where 1=1 and cont.branchcode = 'LMM' ";

			String countSQL = "select count(1) from "+ 
			"WFAPPNTLIST app "+
			"inner join LICARDACTIVEINFOLIST li on app.cardno = li.cardno "+
			"inner join WFCONTLIST cont on app.cardno = cont.cardno "+
			"where 1=1 and cont.branchcode = 'LMM' ";
			if(cardHeading!=null&&!("").equals(cardHeading)){
				String[] cardHeadings = cardHeading.split("@");
				int length = cardHeadings.length;
				infoSQL = infoSQL + "and (";
				countSQL = countSQL + "and (";
				for(int i=0;i<length;i++){
					if(i == length -1){
						infoSQL = infoSQL + "cont.cardno like '"+cardHeadings[i]+"%' ) ";
						countSQL = countSQL + "cont.cardno like '"+cardHeadings[i]+"%' ) ";
						break;
					}
					infoSQL = infoSQL + "cont.cardno like '"+cardHeadings[i]+"%' or ";
					countSQL = countSQL + "cont.cardno like '"+cardHeadings[i]+"%' or ";
				}
				
			}
			if(appntName!=null&&!("").equals(appntName)){
				infoSQL = infoSQL + "and app.Name = '"+appntName+"' ";
				countSQL = countSQL + "and app.Name = '"+appntName+"' ";
			}
			if(contNo!=null&&!("").equals(contNo)){
				infoSQL = infoSQL +"and li.cardno = '"+contNo+"' ";
				countSQL = countSQL +"and li.cardno = '"+contNo+"' ";
			}
			if(idType!=null&&!("").equals(idType)){
				infoSQL = infoSQL + "and app.idtype = '"+idType+"' ";
				countSQL = countSQL + "and app.idtype = '"+idType+"' ";
			}
			if(idNo!=null&&!("").equals(idNo)){
				infoSQL = infoSQL + "and app.idNo = '"+idNo+"' ";
				countSQL = countSQL + "and app.idNo = '"+idNo+"' ";
			}
			if(cardStatus!=null&&!("").equals(cardStatus)){
				infoSQL = infoSQL + "and li.cardStatus = '"+cardStatus+"' ";
				countSQL = countSQL + "and li.cardStatus = '"+cardStatus+"' ";
			}
			if(startDate!=null&&!("").equals(startDate)&&endDate!=null&&!("").equals(endDate)){
				infoSQL = infoSQL +"and cont.senddate >= '"+startDate+"' and cont.senddate <= '"+endDate+"' ";
//				infoSQL = infoSQL + "and li.activedate >= '"+startDate+"' and li.activedate<='"+endDate+"' ";
//				countSQL = countSQL + "and li.activedate >= '"+startDate+"' and li.activedate<='"+endDate+"' ";
				countSQL = countSQL + "and cont.senddate >= '"+startDate+"' and cont.senddate <= '"+endDate+"' ";
			}
			if(cvalistartDate!=null&&!("").equals(cvalistartDate)&&cvaliendDate!=null&&!("").equals(cvaliendDate)){
				infoSQL = infoSQL + "and li.cvalidate >= '"+cvalistartDate+"' and li.cvalidate<='"+cvaliendDate+"' ";
				countSQL = countSQL + "and li.cvalidate >= '"+cvalistartDate+"' and li.cvalidate<='"+cvaliendDate+"' ";
			}
			System.out.println("infoSQL:  "+infoSQL);
			System.out.println("countSQL: "+countSQL);
			ExeSQL exeSQL = new ExeSQL();
			String pageSQL = qTool.buildPageSql(infoSQL,Integer.valueOf(pageIndex).intValue(),Integer.valueOf(pageSize).intValue());
			System.out.println("pageSQL: "+pageSQL);
			tSsrs = exeSQL.execSQL(pageSQL);
			totalcount = exeSQL.getOneValue(countSQL);
			
			if(tSsrs == null || tSsrs.getMaxRow() == 0 || totalcount == null || totalcount.equals("0")){
				info = "没有符合条件的数据";
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			info = "查询驴妈妈相关信息失败";
			return false;
		}
		return true;
	}
	//返回正确报文
	private OMElement buildResponseOME(SSRS tSsrs){
		int length = tSsrs.getMaxRow();
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement contStateSearch = factory.createOMElement("LMMCONTSTATESEARCH",null);
		for(int i=1;i<=length;i++){
			
			OMElement data = factory.createOMElement("DATA",null);
			addOm(data, "CONTNO", tSsrs.GetText(i, 1));
			addOm(data, "APPNTNAME", tSsrs.GetText(i, 2));
			addOm(data, "IDNO", tSsrs.GetText(i, 3));
			addOm(data, "BIRTHDAY", tSsrs.GetText(i, 4));
			addOm(data, "PREM", tSsrs.GetText(i, 5));
			addOm(data, "APPLICANTDATE", tSsrs.GetText(i, 6));
			addOm(data, "CVALIDATE", tSsrs.GetText(i, 7));
			addOm(data, "CERTIFYCODE", tSsrs.GetText(i, 11));//单证编码
			addOm(data, "RISKCODE", tSsrs.GetText(i, 12));//套餐编码
			addOm(data, "CINVALIDATE", tSsrs.GetText(i, 13));//套餐编码
			String status = tSsrs.GetText(i, 10);
			if(status.equals("02"))status = "03";
			addOm(data, "CARDSTATUS", status);
	
			contStateSearch.addChild(data);
		}
		addOm(contStateSearch, "TOTALROWNUM", totalcount);
		root.addChild(contStateSearch);
		return root;
	}
	//返回问题报文
	private OMElement buildErrorOME(String info){
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement contStateSearchError = factory.createOMElement("LMMCONTSTATESEARCHERROR",null);
		addOm(contStateSearchError, "ERRORINFO", info);
		root.addChild(contStateSearchError);
		return root;
	}
	private static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}
	
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
}
