package com.ecwebservice.client;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.soap.SOAP11Constants;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;

/**
 * Title:
 * 
 * @author yuanzw 创建时间：2010-6-10 下午03:10:37
 * @Description:
 * @version
 */

public class TestECQueryClient {
	// 设定webservice调用路径及服务类
	private static EndpointReference targetEPR = new EndpointReference(
			"http://127.0.0.1:8080/piccQuery/services/piccserver");

	public static boolean dd(OMElement Om) {
		try {
			// System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data befor send out : " + Om);
			OMElement ome = sender.sendReceive(Om);
			System.out.println("after call the server : " + ome);
			// String b = ome.getText();
			// if (b.equals("true"))
			// return true;
			// else
			// return false;
			return true;
			// String s = ome.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	// 生成Options类
	private static Options buildOptions() throws AxisFault {
		Options options = new Options();
		options.setSoapVersionURI(SOAP11Constants.SOAP_ENVELOPE_NAMESPACE_URI);
		options.setTo(targetEPR);
		options.setProperty(Constants.Configuration.ENABLE_MTOM,
				Constants.VALUE_TRUE);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(1000 * 120);
		// options.setManageSession(true);
		return options;
	}

	public static OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}

	public static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}

	// 个险 保单列表
	public OMElement testContList() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement CONTLIST = factory.createOMElement("CONTLIST", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		CONTLIST.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);
		
		OMElement INNERCONTLIST = factory.createOMElement("CLIST", null);
		
		
		String[] contNoArr = {"13000515795","13000009296"};
		
		for(int i=0;i<contNoArr.length;i++){
			OMElement ITEM = factory.createOMElement("ITEM", null);
			addOm(ITEM, "CONTNO", contNoArr[i]);
			INNERCONTLIST.addChild(ITEM);
		}
		
		
		
//		addOm(INPUTDATA, "CONTNO", "13000542058,13000542060");
		INPUTDATA.addChild(INNERCONTLIST);
		CONTLIST.addChild(INPUTDATA);

		root.addChild(CONTLIST);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 保单详细信息
	public OMElement testContDetail() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement CONTLIST = factory.createOMElement("CONTDETAIL", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		CONTLIST.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		addOm(INPUTDATA, "CONTNO", "000000011000010");
		CONTLIST.addChild(INPUTDATA);

		root.addChild(CONTLIST);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 理赔列表
	public OMElement testClaimList() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement CLAIMLIST = factory.createOMElement("CLAIMLIST", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		addOm(BASEINFO, "PAGEINDEX", "1");
		addOm(BASEINFO, "PAGESIZE", "15");
		CLAIMLIST.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

//		addOm(INPUTDATA, "CLMNO", "C1100050707000009");
		addOm(INPUTDATA, "CONTNO", "2300007300");
		addOm(INPUTDATA, "ACCSTARTDATE", "2005-7-1");
		addOm(INPUTDATA, "ACCENDDATE", "2005-7-31");
		CLAIMLIST.addChild(INPUTDATA);

		root.addChild(CLAIMLIST);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 理赔详细信息
	public OMElement testClaimDetail() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement CLAIMLIST = factory.createOMElement("CLAIMDETAIL", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		
		CLAIMLIST.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		addOm(INPUTDATA, "CLMNO", "C1100050922000006");
//		addOm(INPUTDATA, "CONTNO", "2300007300");
		
		CLAIMLIST.addChild(INPUTDATA);

		root.addChild(CLAIMLIST);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 保全信息
	public OMElement testEdorInfo() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement EDORINFO = factory.createOMElement("EDORINFO", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		addOm(BASEINFO, "PAGEINDEX", "1");
		addOm(BASEINFO, "PAGESIZE", "10");
		EDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

	
		addOm(INPUTDATA, "CONTNO", "00000929501");
		
		EDORINFO.addChild(INPUTDATA);

		root.addChild(EDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	// 个险 保单号号手动绑定到用户名
	public OMElement testContNoToUserName() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement CONTNOTOUSERNAME = factory.createOMElement("CONTNOTOUSERNAME", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		CONTNOTOUSERNAME.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		addOm(INPUTDATA, "CONTNO", "0000000030");
		addOm(INPUTDATA, "NAME", "99sdfjsdg");
		addOm(INPUTDATA, "SEX", "1");
//		addOm(INPUTDATA, "IDTYPE", "1");
//		addOm(INPUTDATA, "IDNO", "122222199308088822");
		addOm(INPUTDATA, "BIRTHDAY", "1988-8-8");
		
		CONTNOTOUSERNAME.addChild(INPUTDATA);

		root.addChild(CONTNOTOUSERNAME);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 注册
	public OMElement testCustemerRegister() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement CONTNOTOUSERNAME = factory.createOMElement("CUSTOMERREGISTER", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		CONTNOTOUSERNAME.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		addOm(INPUTDATA, "CONTNO", "2300000001");
		addOm(INPUTDATA, "NAME", "测试一");
		addOm(INPUTDATA, "SEX", "1");
		addOm(INPUTDATA, "IDTYPE", "0");
		addOm(INPUTDATA, "IDNO", "125446612312235011");
		addOm(INPUTDATA, "BIRTHDAY", "1990-01-02");
		
		CONTNOTOUSERNAME.addChild(INPUTDATA);

		root.addChild(CONTNOTOUSERNAME);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	
	
	// 个险 保单续期记录
	public OMElement testRenewRecord() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement RENEWRECORD = factory.createOMElement("RENEWRECORD", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "0152");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		addOm(BASEINFO, "PAGEINDEX", "1");
		addOm(BASEINFO, "PAGESIZE", "12");
		
		RENEWRECORD.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "CONTNO", "000920491000001");
		
		
		RENEWRECORD.addChild(INPUTDATA);

		root.addChild(RENEWRECORD);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
//	 个险 保单账户价值
	public OMElement testAccountValue() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ACCOUNTVALUE = factory.createOMElement("ACCOUNTVALUE", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1024");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		addOm(BASEINFO, "PAGEINDEX", "1");
		addOm(BASEINFO, "PAGESIZE", "5");
		
		ACCOUNTVALUE.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "CONTNO", "000919379000001");
		
		
		ACCOUNTVALUE.addChild(INPUTDATA);

		root.addChild(ACCOUNTVALUE);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//保单账户历史信息
	public OMElement testAccountHistoryInfo() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ACCOUNTHISTORY = factory.createOMElement("ACCOUNTHISTORY", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		addOm(BASEINFO, "PAGEINDEX", "2");
		addOm(BASEINFO, "PAGESIZE", "5");
		
		ACCOUNTHISTORY.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "CONTNO", "000919379000001");
		
		
		ACCOUNTHISTORY.addChild(INPUTDATA);

		root.addChild(ACCOUNTHISTORY);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 保单号号自动绑定到用户名
	public OMElement testAutoConnToUserName() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement AUTOCONTNOTOUSERNAME = factory.createOMElement("AUTOCONTNOTOUSERNAME", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		AUTOCONTNOTOUSERNAME.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "NAME", "99sdfjsdg");
		addOm(INPUTDATA, "SEX", "1");
		addOm(INPUTDATA, "IDTYPE", "1");
		addOm(INPUTDATA, "IDNO", "122222199308088822");
		addOm(INPUTDATA, "BIRTHDAY", "1988-8-8");
		
		AUTOCONTNOTOUSERNAME.addChild(INPUTDATA);

		root.addChild(AUTOCONTNOTOUSERNAME);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 保单账户历史信息
	public OMElement testInsuredContList() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDCONTLIST = factory.createOMElement("INSUREDCONTLIST", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		INSUREDCONTLIST.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		OMElement INNERCONTLIST = factory.createOMElement("CLIST", null);
		
		
		String[] contNoArr = {"2300000017","2300000018"};
		
		for(int i=0;i<contNoArr.length;i++){
			OMElement ITEM = factory.createOMElement("ITEM", null);
			addOm(ITEM, "CONTNO", contNoArr[i]);
			INNERCONTLIST.addChild(ITEM);
		}
		
		
		
//		addOm(INPUTDATA, "CONTNO", "13000542058,13000542060");
		INPUTDATA.addChild(INNERCONTLIST);
		
		
		
		INSUREDCONTLIST.addChild(INPUTDATA);

		root.addChild(INSUREDCONTLIST);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 团单分单详细信息
	public OMElement testInsuredContDetail() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDCONTDETAIL = factory.createOMElement("INSUREDCONTDETAIL", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1980");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		
		INSUREDCONTDETAIL.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "CONTNO", "2300000017");
		
		
		INSUREDCONTDETAIL.addChild(INPUTDATA);

		root.addChild(INSUREDCONTDETAIL);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 个险 团单分单保全信息
	public OMElement testInsuredEdorInfo() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("INSUREDEDORINFO", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		addOm(BASEINFO, "PAGEINDEX", "9");
		addOm(BASEINFO, "PAGESIZE", "10");
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "CONTNO", "2300011105");
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	// 团险 保单详细信息
	public OMElement testgrpContDetail() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPCONTDETAIL", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "GRPCONTNO", "0000010001");
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团险理赔列表
	public OMElement testgrpClaimList() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement CLAIMLIST = factory.createOMElement("GRPCLAIMLIST", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1111100008691");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
		
		addOm(BASEINFO, "PAGEINDEX", "2");
		addOm(BASEINFO, "PAGESIZE", "5");
		CLAIMLIST.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

//		addOm(INPUTDATA, "CLMNO", "C0000070926000001");
		addOm(INPUTDATA, "GRPCONTNO", "0000000801 ");
//		addOm(INPUTDATA, "ACCSTARTDATE", "2010-04-27");
		addOm(INPUTDATA, "ACCENDDATE", "2010-04-29");
		CLAIMLIST.addChild(INPUTDATA);

		root.addChild(CLAIMLIST);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团险理赔详细信息
	public OMElement testgrpClaimDetail() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPCLAIMDETAIL", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "GRPCONTNO", "0000000801");
		addOm(INPUTDATA, "CLMNO", "C1100050922000006");
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团险手动绑定
	public OMElement testgrpContNoToUserName() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPCONTNOTOUSERNAME", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "GRPCONTNO", "00003012000002");
		addOm(INPUTDATA, "GRPCODE", "1234567890");
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团险 自动绑定
	public OMElement testgrpAutoContNoToUserName() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPAUTOTOUSERNAME", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		
		addOm(INPUTDATA, "GRPCODE", "1234567890");
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团单注册
	public OMElement testgrpEdor (){
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPEDORINFO", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		addOm(BASEINFO, "PAGEINDEX", "1");
		addOm(BASEINFO, "PAGESIZE", "4");
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "GRPCONTNO", "0000165301");
//		addOm(INPUTDATA, "GRPCODE", "1234567890");
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团险 保单列表
	public OMElement testgrpContList() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPCONTLIST", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);
		OMElement CONTLIST = factory.createOMElement("CLIST", null);
		
		
		String[] grpContNoArr = {"0000010001","0000007502"};
		
		for(int i=0;i<grpContNoArr.length;i++){
			OMElement ITEM = factory.createOMElement("ITEM", null);
			addOm(ITEM, "GRPCONTNO", grpContNoArr[i]);
			CONTLIST.addChild(ITEM);
		}
		
		
		
		
		INPUTDATA.addChild(CONTLIST);
		
		
//		addOm(INPUTDATA, "GRPCUSTOMERNO", "1400005897");
		
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团险保全
	//团单注册
	public OMElement testgrpCustomerRegister() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPCUSTOMERREGISTER", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "CONTNO", "1400005897");
		addOm(INPUTDATA, "GRPCODE", "1234567890");
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团险 账户余额（价值）
	public OMElement testgrpAccountBalance() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPACCOUNTBALANCE", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "GRPCONTNO", "00002470000001");
		
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	//团险 账户历史信息
	public OMElement testgrpAccountHistory() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement INSUREDEDORINFO = factory.createOMElement("GRPACCOUNTHISTORY", null);

		OMElement BASEINFO = factory.createOMElement("BASEINFO", null);

		addOm(BASEINFO, "BATCHNO", "1986");
		addOm(BASEINFO, "SENDDATE", "2010-2-29");
		addOm(BASEINFO, "SENDTIME", "19:51:08");
		addOm(BASEINFO, "MESSAGETYPE", "99");
	
		
		INSUREDEDORINFO.addChild(BASEINFO);

		OMElement INPUTDATA = factory.createOMElement("INPUTDATA", null);

		
		addOm(INPUTDATA, "GRPCONTNO", "00002822000004");
		
		
		
		INSUREDEDORINFO.addChild(INPUTDATA);

		root.addChild(INSUREDEDORINFO);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}

	public static void main(String args[]) {
		TestECQueryClient client = new TestECQueryClient();
		OMElement root2;
		// 电子商务查询系统
//		root2 = client.testContList();// 个险保单列表
//		root2 = client.testContDetail();// 个险保单详细信息
//		root2 = client.testClaimList();// 个险理赔列表
//		root2 = client.testClaimDetail();// 个险理赔详细信息
//		root2 = client.testEdorInfo();// 个险保全信息
		
				//add  at 2010-6-12
//		root2 = client.testCustemerRegister();// 个险 注册 
//		root2 = client.testContNoToUserName();// 个险 手动绑定
//		root2 = client.testAutoConnToUserName();// 个险 自动绑定
//		root2 = client.testRenewRecord();// 个险 保单续期记录
//		root2 = client.testAccountValue();// 个险 保单账户价值
		root2 = client.testAccountHistoryInfo();// 个险 保单账户历史信息
//		root2 = client.testInsuredContList();// 个险 团单分单列表
//		root2 = client.testInsuredContDetail();// 个险 团单分单详细信息
//		root2 = client.testInsuredEdorInfo();// 个险 团单分单保全信息
		// System.out.println("Root2 :"+root2.toString());
		
		//************************************* 团险 ************************************
//		root2 = client.testgrpContDetail();//团险保单详细信息
//		root2 = client.testgrpClaimList();//团险理赔列表
//		root2 = client.testgrpClaimDetail();//团险理赔详细信息
//		root2 = client.testgrpContNoToUserName();//团险 手动绑定
//		root2 = client.testgrpAutoContNoToUserName();//团险 自动绑定
//		root2 = client.testgrpCustomerRegister();//团险 注册
//		root2 = client.testgrpContList();//团险 保单列表
//		root2 = client.testgrpEdor();//团险 保全信息
//		root2 = client.testgrpAccountBalance();//团险 账户余额（价值）
//		root2 = client.testgrpAccountHistory();//团险 账户历史信息

		boolean root1 = client.dd(root2);
	}

}
