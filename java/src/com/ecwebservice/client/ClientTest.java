package com.ecwebservice.client;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.soap.SOAP11Constants;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;

import com.ecwebservice.subinterface.CardActiveInfo;
import com.ecwebservice.subinterface.CardLoginVerify;
import com.ecwebservice.subinterface.HYXCont;
import com.ecwebservice.subinterface.HYXEndor;
import com.ecwebservice.subinterface.HYXQuery;
import com.ecwebservice.subinterface.HYXTb;
import com.ecwebservice.subinterface.UserInfoTransferServer;
import com.ecwebservice.subinterface.ZCardQuery;
import com.ecwebservice.utils.OMElementUtil;



public class ClientTest { 
	// 设定webservice调用路径及服务类
	private static EndpointReference targetEPR = new EndpointReference(
	"http://127.0.0.1:8080/piccQuery/services/piccserver");
	
//	private static EndpointReference targetEPR = new EndpointReference(
//			"http://10.252.3.8:900/services/piccserver");
	
//		private static EndpointReference targetEPR = new EndpointReference(
//		"http://10.252.4.19/ui/services/piccserver");
//	private static EndpointReference targetEPR = new EndpointReference(
//	"http://192.168.71.197/picc_face/services/piccserver");

	public static boolean dd(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryData: " + Om);
			OMElement ome = sender.sendReceive(Om);
			System.out
					.println("Convert the data to element in method queryData: "
							+ ome);
//			String b = ome.getText();
//			if (b.equals("true"))
//				return true;
//			else
//				return false;
			return true;
			// String s = ome.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	// 生成Options类
	private static Options buildOptions() throws AxisFault {
		Options options = new Options();
		options.setSoapVersionURI(SOAP11Constants.SOAP_ENVELOPE_NAMESPACE_URI);
		options.setTo(targetEPR);
		options.setProperty(Constants.Configuration.ENABLE_MTOM,
				Constants.VALUE_TRUE);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
		options.setTimeOutInMilliSeconds(1000 * 120);
		//options.setManageSession(true); 
		return options;
	}

	public static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}

	public static OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}

	public static boolean queryData(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryData: " + Om);

			UserInfoTransferServer server = new UserInfoTransferServer();
			server.queryData(Om);
			// OMElement ome = sender.sendReceive(Om);
			// System.out.println("Convert the data to element in method
			// queryData: "
			// + ome);
			// String b = ome.getText();
			// if(b.equals("true"))
			// return true;
			// else
			// return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean queryCardData(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryCardData: " + Om);

			CardLoginVerify server = new CardLoginVerify();
			server.queryData(Om);
			// OMElement ome = sender.sendReceive(Om);
			// System.out.println("Convert the data to element in method
			// queryData: "
			// + ome);
			// String b = ome.getText();
			// if(b.equals("true"))
			// return true;
			// else
			// return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean queryMCardData(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryMCardData: " + Om);

			ZCardQuery server = new ZCardQuery();
			server.queryData(Om);
			// OMElement ome = sender.sendReceive(Om);
			// System.out.println("Convert the data to element in method
			// queryData: "
			// + ome);
			// String b = ome.getText();
			// if(b.equals("true"))
			// return true;
			// else
			// return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean queryCContData(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryMCardData: " + Om);

			HYXTb server = new HYXTb();
			server.queryData(Om);
			// OMElement ome = sender.sendReceive(Om);
			// System.out.println("Convert the data to element in method
			// queryData: "
			// + ome);
			// String b = ome.getText();
			// if(b.equals("true"))
			// return true;
			// else
			// return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// 远程出单 当日保单查询
	public static boolean queryHContData(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryHContData: " + Om);

			HYXQuery server = new HYXQuery();
			server.queryData(Om);
			// OMElement ome = sender.sendReceive(Om);
			// System.out.println("Convert the data to element in method
			// queryData: "
			// + ome);
			// String b = ome.getText();
			// if(b.equals("true"))
			// return true;
			// else
			// return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean queryCardActiveData(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryCardData: " + Om);

			CardActiveInfo server = new CardActiveInfo();
			server.queryData(Om);
			// OMElement ome = sender.sendReceive(Om);
			// System.out.println("Convert the data to element in method
			// queryData: "
			// + ome);
			// String b = ome.getText();
			// if(b.equals("true"))
			// return true;
			// else
			// return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// 卡单修改
//	public static boolean queryCardEndoreData(OMElement Om) {
//		try {
//			System.out.println(Om);
//			Options options = buildOptions();
//			ConfigurationContext configContext = ConfigurationContextFactory
//					.createConfigurationContextFromFileSystem(null, null);
//			ServiceClient sender = new ServiceClient(configContext, null);
//			sender.setOptions(options);
//			System.out.println("The data in method queryCardEndoreData: " + Om);
//
//			ZCardEndor server = new ZCardEndor();
//			server.queryData(Om);
//			// OMElement ome = sender.sendReceive(Om);
//			// System.out.println("Convert the data to element in method
//			// queryData: "
//			// + ome);
//			// String b = ome.getText();
//			// if(b.equals("true"))
//			// return true;
//			// else
//			// return false;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return false;
//	}

	// 远程出单
	public static boolean queryRemoteContData(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryCardEndoreData: " + Om);

			HYXCont server = new HYXCont();
			server.queryData(Om);
			// OMElement ome = sender.sendReceive(Om);
			// System.out.println("Convert the data to element in method
			// queryData: "
			// + ome);
			// String b = ome.getText();
			// if(b.equals("true"))
			// return true;
			// else
			// return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// 远程出单 当日保单修改
	public static boolean RemoteContEndoreData(OMElement Om) {
		try {
			System.out.println(Om);
			Options options = buildOptions();
			ConfigurationContext configContext = ConfigurationContextFactory
					.createConfigurationContextFromFileSystem(null, null);
			ServiceClient sender = new ServiceClient(configContext, null);
			sender.setOptions(options);
			System.out.println("The data in method queryCardEndoreData: " + Om);

			HYXEndor server = new HYXEndor();
			server.queryData(Om);
			// OMElement ome = sender.sendReceive(Om);
			// System.out.println("Convert the data to element in method
			// queryData: "
			// + ome);
			// String b = ome.getText();
			// if(b.equals("true"))
			// return true;
			// else
			// return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}



	public  OMElement testQueryData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement HYXLOGIN = factory.createOMElement("HYXLOGIN", null);

		addOm(HYXLOGIN, "BATCHNO", "00008694");
		addOm(HYXLOGIN, "SENDDATE", "2010-1-29");
		addOm(HYXLOGIN, "SENDTIME", "9:51:08");
		addOm(HYXLOGIN, "MESSAGETYPE", "99");
		// addOm(HYXLOGIN, "SENDOPERATOR", "bjdz");
		// addOm(HYXLOGIN, "PASSWORD", "722B8F522C4AE0D3");
		addOm(HYXLOGIN, "SENDOPERATOR", "xj");
		addOm(HYXLOGIN, "PASSWORD", "xj");
		
		
		root.addChild(HYXLOGIN);
		return root;
		// boolean rtv = TestClient.queryData(root);
	}
	
	public  OMElement testRemoteCardNoVerify() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement HYXVALIDATEPRINTID = factory.createOMElement("HYXVALIDATEPRINTID", null);

		addOm(HYXVALIDATEPRINTID, "BATCHNO", "00008704");
		addOm(HYXVALIDATEPRINTID, "SENDDATE", "2010-1-29");
		addOm(HYXVALIDATEPRINTID, "SENDTIME", "9:51:08");
		addOm(HYXVALIDATEPRINTID, "MESSAGETYPE", "99");
		// addOm(HYXLOGIN, "SENDOPERATOR", "bjdz");
		// addOm(HYXLOGIN, "PASSWORD", "722B8F522C4AE0D3");
		addOm(HYXVALIDATEPRINTID, "CERTIFYCODE", "WR0162");
		addOm(HYXVALIDATEPRINTID, "COMCODE", "PC1100000004");
		addOm(HYXVALIDATEPRINTID, "PRINTID", "Xa000001631");

		root.addChild(HYXVALIDATEPRINTID);
		return root;
		// boolean rtv = TestClient.queryData(root);
	}

	public  OMElement testQueryCardData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ZCARDLOGIN = factory.createOMElement("ZCARDLOGIN", null);

		addOm(ZCARDLOGIN, "BATCHNO", "111110000904");
		addOm(ZCARDLOGIN, "SENDDATE", "2010-2-29");
		addOm(ZCARDLOGIN, "SENDTIME", "19:51:08");
		addOm(ZCARDLOGIN, "MESSAGETYPE", "99");
		addOm(ZCARDLOGIN, "CARDNO", "PO000000027");
		// addOm(ZCARDLOGIN, "CARDNO", "77");
		// addOm(ZCARDLOGIN, "PASSWORD", "526906");
		addOm(ZCARDLOGIN, "PASSWORD", "496041");

		root.addChild(ZCARDLOGIN);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}

	// 卡单查询
	public  OMElement testCardQueryData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ZCARDQUERY = factory.createOMElement("ZCARDQUERY", null);

		addOm(ZCARDQUERY, "BATCHNO", "869273");
		addOm(ZCARDQUERY, "SENDDATE", "2010-03-12");
		addOm(ZCARDQUERY, "SENDTIME", "09:51:08");
		addOm(ZCARDQUERY, "MESSAGETYPE", "04");
		addOm(ZCARDQUERY, "CARDNO", "12");
		addOm(ZCARDQUERY, "PASSWORD", "3");

		root.addChild(ZCARDQUERY);
		return root;
		// boolean rtv = TestClient.queryMCardData(root);
	}

	public  OMElement testCancelContData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement HYXTB = factory.createOMElement("HYXTB", null);

		addOm(HYXTB, "BATCHNO", "G15");
		addOm(HYXTB, "SENDDATE", "2010-08-09");
		addOm(HYXTB, "SENDTIME", "18:51:08");
		addOm(HYXTB, "MESSAGETYPE", "02");
		addOm(HYXTB, "CARDNO", "BN000000052");
		addOm(HYXTB, "EFFECTDATE", "2009-05-27 19:34:34");
		addOm(HYXTB, "REVOCATIONDATE", "2009-05-27");

		root.addChild(HYXTB);
		return root;
		// boolean rtv = TestClient.queryCContData(root);
	}

	// 远程出单 当日保单查询费
	public  OMElement testRemoteContQueryData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement HYXQUERY = factory.createOMElement("HYXQUERY", null);

		addOm(HYXQUERY, "BATCHNO", "40471");
		addOm(HYXQUERY, "SENDDATE", "2010-08-29");
		addOm(HYXQUERY, "SENDTIME", "07:51:08");
		addOm(HYXQUERY, "MESSAGETYPE", "04");
		addOm(HYXQUERY, "CARDNO", "12"); 

		root.addChild(HYXQUERY);
		return root;
		// boolean rtv = TestClient.queryHContData(root);
	}

	// 卡单激活
	public  OMElement testQueryCardActiveData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ZCARDENABLE = factory.createOMElement("ZCARDENABLE", null);

		addOm(ZCARDENABLE, "BATCHNO", "1970120");
		addOm(ZCARDENABLE, "SENDDATE", "2010-8-29");
		addOm(ZCARDENABLE, "SENDTIME", "9:51:08");
		addOm(ZCARDENABLE, "MESSAGETYPE", "01");
		addOm(ZCARDENABLE, "CARDDEALTYPE", "01");
		
		//测试
//		addOm(ZCARDENABLE, "CARDNO", "W00000024083");

		// 保险卡信息

		OMElement CONTLIST = factory.createOMElement("CONTLIST", null);
		OMElement ITEM = factory.createOMElement("ITEM", null);
		addOm(ITEM, "CARDNO", "W00000024083");
		addOm(ITEM, "PASSWORD", "441852");
		addOm(ITEM, "CERTIFYCODE", "类型二");
		addOm(ITEM, "CERTIFYNAME", "产品一");
		addOm(ITEM, "CARDAREA", "北京");
		addOm(ITEM, "EFFECTDATE", "");
		addOm(ITEM, "ACTIVATIONDATE", "2010-04-28");
		addOm(ITEM, "RISKCODE", "WR0007");
		addOm(ITEM, "RLEVEL", "2");

		// 投保人信息
		OMElement APPNTLIST = factory.createOMElement("APPNTLIST", null);
		OMElement APPITEM = factory.createOMElement("ITEM", null);
		addOm(APPITEM, "NAME", "刘20100527");
		addOm(APPITEM, "SEX", "女");
		addOm(APPITEM, "BIRTHDAY", "2010-07-29");
		addOm(APPITEM, "IDTYPE", "8");
		addOm(APPITEM, "IDNO", "3");
		addOm(APPITEM, "OCCUPATIONTYPE", "4");
		addOm(APPITEM, "OCCUPATIONCODE", "5");
		addOm(APPITEM, "POSTALADDRESS", "北京市海淀区");
		addOm(APPITEM, "ZIPCODE", "110022");
		addOm(APPITEM, "PHONT", "8");
		addOm(APPITEM, "MOBILE", "13000001111");
		addOm(APPITEM, "EMAIL", "1@126.com");
		addOm(APPITEM, "GRPNAME", "搜易");
		addOm(APPITEM, "COMPANYPHONE", "123");
		addOm(APPITEM, "COMPADDR", "上海市区");
		addOm(APPITEM, "COMPZIPCODE", "110034");

		// // 被保人信息
		OMElement INSULIST = factory.createOMElement("INSULIST", null);
		OMElement INSITEM = factory.createOMElement("ITEM", null);
		addOm(INSITEM, "INSUNO", "Q4567");
		addOm(INSITEM, "TOAPPNTRELA", "11");
		addOm(INSITEM, "RELATIONCODE", "00");
		addOm(INSITEM, "NAME", "0527查尔");
		addOm(INSITEM, "SEX", "m");
		addOm(INSITEM, "BIRTHDAY", "1986-01-02");
		addOm(INSITEM, "IDTYPE", "8");
		addOm(INSITEM, "IDNO", "771362222");
		addOm(INSITEM, "OCCUPATIONTYPE", "74362222");
		addOm(INSITEM, "OCCUPATIONCODE", "02");
		addOm(INSITEM, "POSTALADDRESS", "华尔街");
		addOm(INSITEM, "ZIPCODE", "110022");
		addOm(INSITEM, "PHONT", "13962222");
		addOm(INSITEM, "MOBILE", "01462222");
		addOm(INSITEM, "EMAIL", "2@123.com");
		addOm(INSITEM, "GRPNAME", "林");
		addOm(INSITEM, "COMPANYPHONE", "65362222");
		addOm(INSITEM, "COMPADDR", "上海市");
		addOm(INSITEM, "COMPZIPCODE", "119222");

		 OMElement INS2ITEM = factory.createOMElement("ITEM", null);
		 addOm(INS2ITEM, "INSUNO", "555012");
		 addOm(INS2ITEM, "TOAPPNTRELA", "11");
		 addOm(INS2ITEM, "NAME", "林可");
		 addOm(INS2ITEM, "SEX", "g");
		 addOm(INS2ITEM, "BIRTHDAY", "1986-01-03");
		 addOm(INS2ITEM, "IDTYPE", "8");
		 addOm(INS2ITEM, "IDNO", "771362222");
		 addOm(INS2ITEM, "OCCUPATIONTYPE", "74362222");
		 addOm(INS2ITEM, "OCCUPATIONCODE", "02");
		 addOm(INS2ITEM, "POSTALADDRESS", "中关村1号");
		 addOm(INS2ITEM, "ZIPCODE", "000022");
		 addOm(INS2ITEM, "PHONT", "13962222");
		 addOm(INS2ITEM, "MOBILE", "01462222");
		 addOm(INS2ITEM, "EMAIL", "34@147.com");
		 addOm(INS2ITEM, "GRPNAME", "竹林");
		 addOm(INS2ITEM, "COMPANYPHONE", "65362222");
		 addOm(INS2ITEM, "COMPADDR", "中关村1号");
		 addOm(INS2ITEM, "COMPZIPCODE", "119222");
		//
		// // 受益人信息
		OMElement BNFLIST = factory.createOMElement("BNFLIST", null);
		OMElement BNFITEM = factory.createOMElement("ITEM", null);
		addOm(BNFITEM, "TOINSUNO", "6611");
		addOm(BNFITEM, "TOINSURELA", "11");
		addOm(BNFITEM, "BNFTYPE", "22");
		addOm(BNFITEM, "BNFGRADE", "2");
		addOm(BNFITEM, "BNFRATE", "10");
		addOm(BNFITEM, "NAME", "马克");
		addOm(BNFITEM, "SEX", "g");
		addOm(BNFITEM, "BIRTHDAY", "1987-02-01");
		addOm(BNFITEM, "IDTYPE", "8");
		addOm(BNFITEM, "IDNO", "2361");

		 OMElement BNF2ITEM = factory.createOMElement("ITEM", null);
		 addOm(BNF2ITEM, "TOINSUNO", "7711");
		 addOm(BNF2ITEM, "TOINSURELA", "01");
		 addOm(BNF2ITEM, "BNFTYPE", "12");
		 addOm(BNF2ITEM, "BNFGRADE", "5");
		 addOm(BNF2ITEM, "BNFRATE", "15");
		 addOm(BNF2ITEM, "NAME", "02 999911");
		 addOm(BNF2ITEM, "SEX", "g");
		 addOm(BNF2ITEM, "BIRTHDAY", "78609");
		 addOm(BNF2ITEM, "IDTYPE", "8");
		 addOm(BNF2ITEM, "IDNO", "mation362222");

		CONTLIST.addChild(ITEM);
		ZCARDENABLE.addChild(CONTLIST);
		// 投保人
		APPNTLIST.addChild(APPITEM);

		ZCARDENABLE.addChild(APPNTLIST);

		// 被保人
		INSULIST.addChild(INSITEM);
		 INSULIST.addChild(INS2ITEM);
		ZCARDENABLE.addChild(INSULIST);
		// 受益人
		BNFLIST.addChild(BNFITEM);
		BNFLIST.addChild(BNF2ITEM);
		ZCARDENABLE.addChild(BNFLIST);

		root.addChild(ZCARDENABLE);

		// boolean rtv = TestClient.queryCardActiveData(root);
		return root;
	}

	// 测试卡单修改
	public  OMElement testQueryCardEndorData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ZCARDENDOR = factory.createOMElement("ZCARDENDOR", null);

		addOm(ZCARDENDOR, "BATCHNO", "K93");
		addOm(ZCARDENDOR, "SENDDATE", "2014-8-29");
		addOm(ZCARDENDOR, "SENDTIME", "14:51:08");
		addOm(ZCARDENDOR, "MESSAGETYPE", "03");
		addOm(ZCARDENDOR, "CARDDEALTYPE", "2");

		// 保险卡信息

		OMElement CONTLIST = factory.createOMElement("CONTLIST", null);
		OMElement ITEM = factory.createOMElement("ITEM", null);
		addOm(ITEM, "CARDNO", "YX000027910");
		addOm(ITEM, "PASSWORD", "4462222");

		// 投保人信息
		OMElement APPNTLIST = factory.createOMElement("APPNTLIST", null);
		OMElement APPITEM = factory.createOMElement("ITEM", null);
		addOm(APPITEM, "NAME", "杰克");
		addOm(APPITEM, "SEX", "w");
		addOm(APPITEM, "BIRTHDAY", "1914-8-29");
		addOm(APPITEM, "IDTYPE", "7");
		addOm(APPITEM, "IDNO", "65242222");
		addOm(APPITEM, "OCCUPATIONTYPE", "职业40");
		addOm(APPITEM, "OCCUPATIONCODE", "92222");
		addOm(APPITEM, "POSTALADDRESS", "海淀区中街");
		addOm(APPITEM, "ZIPCODE", "119022");
		addOm(APPITEM, "PHONT", "12562222");
		addOm(APPITEM, "MOBILE", "41982222");
		addOm(APPITEM, "EMAIL", "23@123.com");
		addOm(APPITEM, "GRPNAME", "科研");
		addOm(APPITEM, "COMPANYPHONE", "69855222");
		addOm(APPITEM, "COMPADDR", "一街");
		addOm(APPITEM, "COMPZIPCODE", "98562");

		// 被保人信息
		OMElement INSULIST = factory.createOMElement("INSULIST", null);
		OMElement INSITEM = factory.createOMElement("ITEM", null);
		addOm(INSITEM, "INSUNO", "7070");
		addOm(INSITEM, "TOAPPNTRELA", "10");

		addOm(INSITEM, "RELATIONCODE", "00");
		addOm(INSITEM, "NAME", "海一");
		addOm(INSITEM, "SEX", "g");
		addOm(INSITEM, "BIRTHDAY", "1944-8-29");
		addOm(INSITEM, "IDTYPE", "4");
		addOm(INSITEM, "IDNO", "36562222");
		addOm(INSITEM, "OCCUPATIONTYPE", "职业34");
		addOm(INSITEM, "OCCUPATIONCODE", "102422");
		addOm(INSITEM, "POSTALADDRESS", "龙街");
		addOm(INSITEM, "ZIPCODE", "21222");
		addOm(INSITEM, "PHONT", "3658");
		addOm(INSITEM, "MOBILE", "56985");
		addOm(INSITEM, "EMAIL", "23@01.com");
		addOm(INSITEM, "GRPNAME", "灵异");
		addOm(INSITEM, "COMPANYPHONE", "12362222");
		addOm(INSITEM, "COMPADDR", "海口");
		addOm(INSITEM, "COMPZIPCODE", "02772");

		// OMElement INS2ITEM = factory.createOMElement("ITEM", null);
		// addOm(INS2ITEM, "INSUNO", "77");
		// addOm(INS2ITEM, "TOAPPNTRELA", "10");
		// addOm(INS2ITEM, "NAME", "ENDORInsiapp999911");
		// addOm(INS2ITEM, "SEX", "g");
		// addOm(INS2ITEM, "BIRTHDAY", "236510");
		// addOm(INS2ITEM, "IDTYPE", "4");
		// addOm(INS2ITEM, "IDNO", "36562222");
		// addOm(INS2ITEM, "OCCUPATIONTYPE", "98622");
		// addOm(INS2ITEM, "OCCUPATIONCODE", "102422");
		// addOm(INS2ITEM, "POSTALADDRESS", "ENDORInspost362222");
		// addOm(INS2ITEM, "ZIPCODE", "21222");
		// addOm(INS2ITEM, "PHONT", "3658");
		// addOm(INS2ITEM, "MOBILE", "56985");
		// addOm(INS2ITEM, "EMAIL", "ENDORInsemail362222");
		// addOm(INS2ITEM, "GRPNAME", "ENDORInsgrpName362222");
		// addOm(INS2ITEM, "COMPANYPHONE", "ENDORInscom362222");
		// addOm(INS2ITEM, "COMPADDR", "ENDORInscomAddr362222");
		// addOm(INS2ITEM, "COMPZIPCODE", "02772");

		// 受益人信息
		OMElement BNFLIST = factory.createOMElement("BNFLIST", null);
		OMElement BNFITEM = factory.createOMElement("ITEM", null);
		addOm(BNFITEM, "TOINSUNO", "01");
		addOm(BNFITEM, "TOINSURELA", "6");
		addOm(BNFITEM, "BNFTYPE", "45");
		addOm(BNFITEM, "BNFGRADE", "67");
		addOm(BNFITEM, "BNFRATE", "102");
		addOm(BNFITEM, "NAME", "另一");
		addOm(BNFITEM, "SEX", "w");
		addOm(BNFITEM, "BIRTHDAY", "1986-09-19");
		addOm(BNFITEM, "IDTYPE", "7");
		addOm(BNFITEM, "IDNO", "362222");

		// OMElement BNF2ITEM = factory.createOMElement("ITEM", null);
		// addOm(BNF2ITEM, "TOINSUNO", "00");
		// addOm(BNF2ITEM, "TOINSURELA", "6");
		// addOm(BNF2ITEM, "BNFTYPE", "45");
		// addOm(BNF2ITEM, "BNFGRADE", "67");
		// addOm(BNF2ITEM, "BNFRATE", "102");
		// addOm(BNF2ITEM, "NAME", "ENDORbapp999911");
		// addOm(BNF2ITEM, "SEX", "w");
		// addOm(BNF2ITEM, "BIRTHDAY", "63548");
		// addOm(BNF2ITEM, "IDTYPE", "7");
		// addOm(BNF2ITEM, "IDNO", "ENDORbidinfo362222");

		CONTLIST.addChild(ITEM);
		ZCARDENDOR.addChild(CONTLIST);
		// 投保人
		APPNTLIST.addChild(APPITEM);
		ZCARDENDOR.addChild(APPNTLIST);

		// 被保人
		INSULIST.addChild(INSITEM);
		// INSULIST.addChild(INS2ITEM);
		ZCARDENDOR.addChild(INSULIST);
		// 受益人
		BNFLIST.addChild(BNFITEM);
		// BNFLIST.addChild(BNF2ITEM);
		ZCARDENDOR.addChild(BNFLIST);

		root.addChild(ZCARDENDOR);

		// boolean rtv = TestClient.queryCardEndoreData(root);
		return root;
	}

	// 远程出单
	public  OMElement testRemoteContData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ZCARDENDOR = factory.createOMElement("HYXCONT", null);

		addOm(ZCARDENDOR, "BATCHNO", "33694726");
		addOm(ZCARDENDOR, "SENDDATE", "2014-8-29");
		addOm(ZCARDENDOR, "SENDTIME", "20:51:08");
		addOm(ZCARDENDOR, "BRANCHCODE", "1100000003");
		addOm(ZCARDENDOR, "SENDOPERATOR", "1100000003");
		addOm(ZCARDENDOR, "MESSAGETYPE", "01");

		// 保险卡信息

		OMElement CONTLIST = factory.createOMElement("CONTLIST", null);
		OMElement ITEM = factory.createOMElement("ITEM", null);
		addOm(ITEM, "CARDNO", "PO000000028");
		addOm(ITEM, "PASSWORD", "496041");

		addOm(ITEM, "CERTIFYCODE", "WR0007");
		addOm(ITEM, "CERTIFYNAME", "产品34");
		addOm(ITEM, "ACTIVEDATE", "2002-01-03");
		addOm(ITEM, "MANAGECOM", "");
		addOm(ITEM, "SALECHNL", "7");
		addOm(ITEM, "AGENTCOM", "0701");
		addOm(ITEM, "AGENTCODE", "567");
		addOm(ITEM, "AGENTNAME", "业务员一");
		addOm(ITEM, "PAYMODE", "7");
		addOm(ITEM, "PAYINTV", "12");
		addOm(ITEM, "PAYYEAR", "30");
		addOm(ITEM, "PAYYEARFLAG", "1");
		addOm(ITEM, "CVALIDATE", "2002-07-01");
		
		addOm(ITEM, "CVALIDATETIME", "15:01:02");
		addOm(ITEM, "INSUYEAR", "4");
		addOm(ITEM, "INSUYEARFLAG", "2");
		addOm(ITEM, "PREM", "4567");
		addOm(ITEM, "AMNT", "987");
		addOm(ITEM, "MULT", "5");
		addOm(ITEM, "COPYS", "20");
		addOm(ITEM, "REMARK", "特别约定");

		// 投保人信息
		OMElement APPNTLIST = factory.createOMElement("APPNTLIST", null);
		OMElement APPITEM = factory.createOMElement("ITEM", null);
		addOm(APPITEM, "NAME", "投保人一");
		addOm(APPITEM, "SEX", "r");
		addOm(APPITEM, "BIRTHDAY", "1986-01-03");
		addOm(APPITEM, "IDTYPE", "8");
		addOm(APPITEM, "IDNO", "0898777");
		addOm(APPITEM, "OCCUPATIONTYPE", "职业2");
		addOm(APPITEM, "OCCUPATIONCODE", "335455");
		addOm(APPITEM, "POSTALADDRESS", "北京地址");
		addOm(APPITEM, "ZIPCODE", "3555");
		addOm(APPITEM, "PHONT", "444556667");
		addOm(APPITEM, "MOBILE", "554434");
		addOm(APPITEM, "EMAIL", "2@123.com");
		addOm(APPITEM, "GRPNAME", "单位34");
		addOm(APPITEM, "COMPANYPHONE", "44567778");
		addOm(APPITEM, "COMPADDR", "公司地址3");
		addOm(APPITEM, "COMPZIPCODE", "86665");

		// 被保人信息
		OMElement INSULIST = factory.createOMElement("INSULIST", null);
		OMElement INSITEM = factory.createOMElement("ITEM", null);
		addOm(INSITEM, "INSUNO", "40");
		addOm(INSITEM, "TOAPPNTRELA", "5");
		addOm(INSITEM, "RELATIONCODE", "00");
		addOm(INSITEM, "NAME", "被保人一");
		addOm(INSITEM, "SEX", "0");
		addOm(INSITEM, "BIRTHDAY", "1987-04-02");
		addOm(INSITEM, "IDTYPE", "8");
		addOm(INSITEM, "IDNO", "55");
		addOm(INSITEM, "OCCUPATIONTYPE", "职业33");
		addOm(INSITEM, "OCCUPATIONCODE", "45");
		addOm(INSITEM, "POSTALADDRESS", "被保人邮寄地址");
		addOm(INSITEM, "ZIPCODE", "445");
		addOm(INSITEM, "PHONT", "7565");
		addOm(INSITEM, "MOBILE", "434");
		addOm(INSITEM, "EMAIL", "67@45.com");
		addOm(INSITEM, "GRPNAME", "公司e");
		addOm(INSITEM, "COMPANYPHONE", "45656");
		addOm(INSITEM, "COMPADDR", "公司地址6");
		addOm(INSITEM, "COMPZIPCODE", "11323");

		 OMElement INS2ITEM = factory.createOMElement("ITEM", null);
		 addOm(INS2ITEM, "INSUNO", "44");
		 addOm(INS2ITEM, "TOAPPNTRELA", "5");
		 addOm(INS2ITEM, "NAME", "被保人二");
		 addOm(INS2ITEM, "SEX", "y");
		 addOm(INS2ITEM, "BIRTHDAY", "66788");
		 addOm(INS2ITEM, "IDTYPE", "8");
		 addOm(INS2ITEM, "IDNO", "55");
		 addOm(INS2ITEM, "OCCUPATIONTYPE", "33");
		 addOm(INS2ITEM, "OCCUPATIONCODE", "45566");
		 addOm(INS2ITEM, "POSTALADDRESS", "66");
		 addOm(INS2ITEM, "ZIPCODE", "445");
		 addOm(INS2ITEM, "PHONT", "7565");
		 addOm(INS2ITEM, "MOBILE", "434");
		 addOm(INS2ITEM, "EMAIL", "676788");
		 addOm(INS2ITEM, "GRPNAME", "907876");
		 addOm(INS2ITEM, "COMPANYPHONE", "45656");
		 addOm(INS2ITEM, "COMPADDR", "33422");
		 addOm(INS2ITEM, "COMPZIPCODE", "11323");

		// 受益人信息
		OMElement BNFLIST = factory.createOMElement("BNFLIST", null);
		OMElement BNFITEM = factory.createOMElement("ITEM", null);
		addOm(BNFITEM, "TOINSUNO", "50");
		addOm(BNFITEM, "TOINSURELA", "5");
		addOm(BNFITEM, "BNFTYPE", "2");
		addOm(BNFITEM, "BNFGRADE", "6");
		addOm(BNFITEM, "BNFRATE", "7");
		addOm(BNFITEM, "NAME", "受益人34");
		addOm(BNFITEM, "SEX", "p");
		addOm(BNFITEM, "BIRTHDAY", "1986-01-02");
		addOm(BNFITEM, "IDTYPE", "8");
		addOm(BNFITEM, "IDNO", "346667");

		 OMElement BNF2ITEM = factory.createOMElement("ITEM", null);
		 addOm(BNF2ITEM, "TOINSUNO", "55");
		 addOm(BNF2ITEM, "TOINSURELA", "5");
		 addOm(BNF2ITEM, "BNFTYPE", "2");
		 addOm(BNF2ITEM, "BNFGRADE", "6");
		 addOm(BNF2ITEM, "BNFRATE", "7");
		 addOm(BNF2ITEM, "NAME", "第二受益人");
		 addOm(BNF2ITEM, "SEX", "p");
		 addOm(BNF2ITEM, "BIRTHDAY", "890");
		 addOm(BNF2ITEM, "IDTYPE", "1");
		 addOm(BNF2ITEM, "IDNO", "346667");

		CONTLIST.addChild(ITEM);
		ZCARDENDOR.addChild(CONTLIST);
		// 投保人
		APPNTLIST.addChild(APPITEM);
		ZCARDENDOR.addChild(APPNTLIST);

		// 被保人
		INSULIST.addChild(INSITEM);
		 INSULIST.addChild(INS2ITEM);
		ZCARDENDOR.addChild(INSULIST);
		// 受益人
		BNFLIST.addChild(BNFITEM);
		BNFLIST.addChild(BNF2ITEM);
		ZCARDENDOR.addChild(BNFLIST);

		root.addChild(ZCARDENDOR);
		return root;
		// boolean rtv = TestClient.queryRemoteContData(root);
	}

	// 测试远程出单 当日保单修改
	public  OMElement testRemoteContEndorData() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ZCARDENDOR = factory.createOMElement("HYXENDOR", null);

		addOm(ZCARDENDOR, "BATCHNO", "rt2553");
		addOm(ZCARDENDOR, "SENDDATE", "2004-8-29");
		addOm(ZCARDENDOR, "SENDTIME", "14:51:08");
		addOm(ZCARDENDOR, "MESSAGETYPE", "03");
		addOm(ZCARDENDOR, "CARDNO", "YX000027910");

		// 投保人信息
		OMElement APPNTLIST = factory.createOMElement("APPNTLIST", null);
		OMElement APPITEM = factory.createOMElement("ITEM", null);
		addOm(APPITEM, "NAME", "投保人一");
		addOm(APPITEM, "SEX", "m");
		addOm(APPITEM, "BIRTHDAY", "1986-01-02");
		addOm(APPITEM, "IDTYPE", "8");
		addOm(APPITEM, "IDNO", "698522222");
		addOm(APPITEM, "OCCUPATIONTYPE", "职业4");
		addOm(APPITEM, "OCCUPATIONCODE", "8362222");
		addOm(APPITEM, "POSTALADDRESS", "邮寄地址92");
		addOm(APPITEM, "ZIPCODE", "9622");
//		addOm(APPITEM, "PHONT", "4472222");
//		addOm(APPITEM, "MOBILE", "9687752222");
//		addOm(APPITEM, "EMAIL", "342@goo.com");
//		addOm(APPITEM, "GRPNAME", "欧冠");
//		addOm(APPITEM, "COMPANYPHONE", "74582222");
//		addOm(APPITEM, "COMPADDR", "公司地址er");
//		addOm(APPITEM, "COMPZIPCODE", "69582");

		// 被保人信息
		OMElement INSULIST = factory.createOMElement("INSULIST", null);
		OMElement INSITEM = factory.createOMElement("ITEM", null);
//		addOm(INSITEM, "INSUNO", "s72023");
//		addOm(INSITEM, "TOAPPNTRELA", "8");
//		addOm(INSITEM, "RELATIONCODE", "00");
		addOm(INSITEM, "NAME", "被保人f0");
//		addOm(INSITEM, "SEX", "n");
//		addOm(INSITEM, "BIRTHDAY", "1986-01-02");
//		addOm(INSITEM, "IDTYPE", "8");
//		addOm(INSITEM, "IDNO", "154222");
//		addOm(INSITEM, "OCCUPATIONTYPE", "职业702");
//		addOm(INSITEM, "OCCUPATIONCODE", "542222");
//		addOm(INSITEM, "POSTALADDRESS", "邮寄地址36");
//		addOm(INSITEM, "ZIPCODE", "72222");
//		addOm(INSITEM, "PHONT", "857222");
//		addOm(INSITEM, "MOBILE", "8572222");
//		addOm(INSITEM, "EMAIL", "456@goo.com");
//		addOm(INSITEM, "GRPNAME", "被保人公司名称");
//		addOm(INSITEM, "COMPANYPHONE", "5662222");
//		addOm(INSITEM, "COMPADDR", "公司地址86");
//		addOm(INSITEM, "COMPZIPCODE", "8122");

//		 OMElement INS2ITEM = factory.createOMElement("ITEM", null);
//		 addOm(INS2ITEM, "INSUNO", "22");
//		 addOm(INS2ITEM, "TOAPPNTRELA", "8");
//		 addOm(INS2ITEM, "NAME", "被保人222");
		// addOm(INS2ITEM, "SEX", "n");
		// addOm(INS2ITEM, "BIRTHDAY", "42222");
		// addOm(INS2ITEM, "IDTYPE", "7");
		// addOm(INS2ITEM, "IDNO", "154222");
		// addOm(INS2ITEM, "OCCUPATIONTYPE", "7062222");
		// addOm(INS2ITEM, "OCCUPATIONCODE", "542222");
		// addOm(INS2ITEM, "POSTALADDRESS", "Remote ENDORpost362222");
		// addOm(INS2ITEM, "ZIPCODE", "72222");
		// addOm(INS2ITEM, "PHONT", "857222");
		// addOm(INS2ITEM, "MOBILE", "8572222");
		// addOm(INS2ITEM, "EMAIL", "Remote ENDORemail362222");
		// addOm(INS2ITEM, "GRPNAME", "Remote ENDORgrpName362222");
		// addOm(INS2ITEM, "COMPANYPHONE", "5662222");
		// addOm(INS2ITEM, "COMPADDR", "Remote ENDORcomAddr362222");
		// addOm(INS2ITEM, "COMPZIPCODE", "8122");

		// 受益人信息
		OMElement BNFLIST = factory.createOMElement("BNFLIST", null);
		OMElement BNFITEM = factory.createOMElement("ITEM", null);
		addOm(BNFITEM, "TOINSUNO", "30");
		addOm(BNFITEM, "TOINSURELA", "5");
		addOm(BNFITEM, "BNFTYPE", "3");
		addOm(BNFITEM, "BNFGRADE", "8");
		addOm(BNFITEM, "BNFRATE", "2");
		addOm(BNFITEM, "NAME", "受益人11");
		addOm(BNFITEM, "SEX", "s");
		addOm(BNFITEM, "BIRTHDAY", "1902-01-02");
		addOm(BNFITEM, "IDTYPE", "8");
		addOm(BNFITEM, "IDNO", "742222");

		// OMElement BNF2ITEM = factory.createOMElement("ITEM", null);
		// addOm(BNF2ITEM, "TOINSUNO", "33");
		// addOm(BNF2ITEM, "TOINSURELA", "5");
		// addOm(BNF2ITEM, "BNFTYPE", "3");
		// addOm(BNF2ITEM, "BNFGRADE", "8");
		// addOm(BNF2ITEM, "BNFRATE", "2");
		// addOm(BNF2ITEM, "NAME", "Remote ENDORname999911");
		// addOm(BNF2ITEM, "SEX", "s");
		// addOm(BNF2ITEM, "BIRTHDAY", "41222");
		// addOm(BNF2ITEM, "IDTYPE", "8");
		// addOm(BNF2ITEM, "IDNO", "742222");

		// 投保人
		APPNTLIST.addChild(APPITEM);
		ZCARDENDOR.addChild(APPNTLIST);

		// 被保人
		INSULIST.addChild(INSITEM);
//		INSULIST.addChild(INS2ITEM);
		ZCARDENDOR.addChild(INSULIST);
		// 受益人
		BNFLIST.addChild(BNFITEM);
		// BNFLIST.addChild(BNF2ITEM);
		ZCARDENDOR.addChild(BNFLIST);

		root.addChild(ZCARDENDOR);
		return root;
		// boolean rtv = TestClient.RemoteContEndoreData(root);
	}
	
	
	//远程出单代理人校验及信息查询
	public  OMElement testRemoteAgentInfo() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement HYXLOGIN = factory.createOMElement("HYXQUERYAGENT", null);

		addOm(HYXLOGIN, "BATCHNO", "00008695");
		addOm(HYXLOGIN, "SENDDATE", "2010-1-29");
		addOm(HYXLOGIN, "SENDTIME", "9:51:08");
		addOm(HYXLOGIN, "MESSAGETYPE", "99");
		// addOm(HYXLOGIN, "SENDOPERATOR", "bjdz");
		// addOm(HYXLOGIN, "PASSWORD", "722B8F522C4AE0D3");
		addOm(HYXLOGIN, "AGENTCODE", "c1101000007");
		addOm(HYXLOGIN, "COMCODE", "86110000");
		
		
		root.addChild(HYXLOGIN);
		return root;
		// boolean rtv = TestClient.queryData(root);
	}
	
	public  OMElement testOme() {
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ZCARDLOGIN = factory.createOMElement("ZCARDLOGIN", null);

		addOm(ZCARDLOGIN, "BATCHNO", "111110000904");
		
		//开始测试Ome
		OMElement sendDateOme = factory.createOMElement("SENDDATE", null, ZCARDLOGIN);
		sendDateOme.setText("2010-9-29");
		
//		addOm(ZCARDLOGIN, "SENDDATE", "2010-2-29");
//		addOm(ZCARDLOGIN, "SENDTIME", "19:51:08");
//		addOm(ZCARDLOGIN, "MESSAGETYPE", "99");
//		addOm(ZCARDLOGIN, "CARDNO", "PO000000027");
		// addOm(ZCARDLOGIN, "CARDNO", "77");
		// addOm(ZCARDLOGIN, "PASSWORD", "526906");
		OMElementUtil tOMElementUtil = new OMElementUtil();
		
		tOMElementUtil.addOm(ZCARDLOGIN, "PASSWORD", "496041");

		root.addChild(ZCARDLOGIN);
		return root;
		// boolean rtv = TestClient.queryCardData(root);
	}
	
	public static void main(String agrs[]) {

		try {
			// OMElement root = buildQueryData();
			// OMFactory factory = OMAbstractFactory.getOMFactory();
			// OMElement Feeback = factory.createOMElement("HYXLOGIN", null);
			//			
			// OMElement FeeInfo = factory.createOMElement("FeeInfo", null);
			// addOm(FeeInfo, "OtherNo","200120079900000218");
			// addOm(FeeInfo, "OtherNoType","4");
			// addOm(FeeInfo, "PayMoney","525");
			// addOm(FeeInfo, "FeeType","0");
			// addOm(FeeInfo, "EnterAccDate","2009-04-09");
			// addOm(FeeInfo, "SENDOPERATOR","119");
			// addOm(FeeInfo, "PASSWORD","EE4B420EC6E63870");
			//			
			// Feeback.addChild(FeeInfo);
			// root.addChild(Feeback);

			ClientTest client = new ClientTest();
			OMElement root2;
//			root2 = client.testRemoteContData();// 测试远程出单
//			root2 = client.testQueryCardActiveData();// 测试卡单激活
//			root2 = client.testCancelContData();// 测试远程出单当日撤单
			//root2 = client.testRemoteContEndorData();// 测试远程出单当日保单修改

//			root2 = client.testRemoteContQueryData();// 远程出单 当日保单查询
//			root2 = client.testCardQueryData();// 测试卡单查询
//			root2 = client.testQueryCardData();// 测试卡单登陆
//			root2 = client.testQueryData();// 测试远程出单代理机构登陆

			//root2 = client.testQueryCardEndorData();// 测试卡单修改
//			root2 = client.testRemoteCardNoVerify();//远程出单单证号即时校验
//			root2 = client.testRemoteAgentInfo();//远程出单代理人校验并查询
			
			//测试从文件中读取
			OMElementUtil tempOMElementUtil = new OMElementUtil();
			root2 = tempOMElementUtil.bulidAXIOMFromXML("java/src/com/ecwebservice/client/logon.xml");
			
//			root2 = client.testOme();
			

			boolean root1 = ClientTest.dd(root2);

		} catch (Exception e) {
			System.out.println("Create File Error!");
			e.printStackTrace();
		}
		System.out.println("Create Ok");
	}
}
