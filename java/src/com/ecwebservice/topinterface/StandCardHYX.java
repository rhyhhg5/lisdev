/**
 * StandCardHYX.java
 * com.ecwebservice.personalquery
 *
 * Function： TODO 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2010-6-24 		caosg
 *
 * Copyright (c) 2010, TNT All Rights Reserved.
 */

package com.ecwebservice.topinterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;

/**
 * 
 * @author 曹淑国
 * @version
 * @since Ver 1.1
 * @Date 2010-6-24 下午10:42:50
 */
public abstract class StandCardHYX implements CardHYX {

	protected String[] baseInfoString;

	private String[] errInfoArr = {"00","*","未传值"};

	// 返回报文根节点
	private String root;
	//记录条数
	protected String recordCount;

	protected LoginVerifyTool mTool = new LoginVerifyTool();

	protected ExeSQL mExeSQL = new ExeSQL();

	public StandCardHYX(String root) {
		this.root = root;
	}

	public OMElement queryData(OMElement Oms) {
		OMElement responseOME = null;
		Map resultMap = new HashMap();
		try {
			QueryTool qTool = new QueryTool();
			//获取根节点标签
			root = qTool.getRootTag(Oms);
			
			// 获取baseInfo
			baseInfoString = qTool.parseBaseInfo(Oms);

			// 获取查询条件
			getQueryFactor(qTool.parseQueryString(Oms));

			resultMap = prepareData();

			responseOME = buildResponseOME(resultMap);
		} catch (Exception e) {
			e.printStackTrace();
			String[] InfoArr = { "01", "*", "系统查询数据发生异常,请稍候再试！" };
			this.setErrInfoArr(InfoArr);
			responseOME = this.buildResponseOME(resultMap);
			return responseOME;

		}

		System.out.println("返回报文："+responseOME.toString());
		return responseOME;
	}

	/**
	 * prepareData:(查询业务数据并封装:ITEM 用ArrayList; 其余用HashMap )
	 * 
	 * @param
	 * @param queryFactor
	 * @param
	 * @return 设定文件
	 * @return Set DOM对象
	 * @throws
	 */
	public abstract Map prepareData();

	/**
	 * 获取查询条件
	 * 
	 * @param
	 * @param queryFactor
	 *            设定文件
	 * @return void DOM对象
	 * @throws
	 * @since CodingExample Ver 1.1
	 */
	protected abstract void getQueryFactor(Map queryFactor);

	/**
	 * 封装返回报文
	 * 
	 * @param
	 * @param resultMap
	 * @param
	 * @return 设定文件
	 * @return OMElement DOM对象
	 * @throws
	 * @since CodingExample Ver 1.1
	 */
	private OMElement buildResponseOME(Map resultMap) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement(root, null);

		QueryTool qTool = new QueryTool();
		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString,this.getRecordCount(),
				errInfoArr[0], errInfoArr[1], errInfoArr[2]);
		DATASET.addChild(BASEINFO);// 构建基础信息

		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);

		// 遍历HashSet outputdateMap
		Iterator result = resultMap.keySet().iterator();
		while (result.hasNext()) {
			String key = (String) result.next(); // CLIST
			Object object = resultMap.get(key);

			prepare(key, object, fac, OUTPUTDATA);
		}
		DATASET.addChild(OUTPUTDATA);

		return DATASET;
	}

	/**
	 * 解析封装的业务查询数据
	 * 
	 * @param
	 * @param key
	 * @param
	 * @param object
	 * @param
	 * @param fac
	 * @param
	 * @param oem
	 * @param
	 * @return 设定文件
	 * @return OMElement DOM对象
	 * @throws
	 * @since CodingExample Ver 1.1
	 */
	private OMElement prepare(String key, Object object, OMFactory fac,
			OMElement oem) {
		if (object instanceof HashMap) {
			OMElement Info1 = fac.createOMElement(key, null);

			System.out.println("Info-key:" + key);

			Map resultMap = (HashMap) object;
			Iterator result = resultMap.keySet().iterator();
			while (result.hasNext()) {
				String key1 = (String) result.next();
				Object object1 = resultMap.get(key1);

				prepare(key1, object1, fac, Info1);
			}
			oem.addChild(Info1);
		} else if (object instanceof ArrayList) {
			List item = (ArrayList) object;
			Iterator resultSet = item.iterator();
			while (resultSet.hasNext()) {
				prepare(key, resultSet.next(), fac, oem);
			}
		} else {
			mTool.addOm(oem, key, (String) object);
		}
		return oem;
	}

	public LoginVerifyTool getTool() {
		return mTool;
	}

	public String[] getErrInfoArr() {
		return errInfoArr;
	}

	public void setErrInfoArr(String[] errInfoArr) {
		this.errInfoArr = errInfoArr;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public String getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}
}
