package com.ecwebservice.topinterface;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.querytools.QueryTool;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public abstract class ListInterface implements CardHYX {
	private OMElement Oms;
	private List childTagName;
	private String[] baseInfoString;

	// baseInfo[0] = batchno;
	// baseInfo[1] = sendDate;
	// baseInfo[2] = sendTime;
	// baseInfo[3] = messageType;
	private List riskList;
	private String riskSql;
	private String querySql;
	private String countSql;
	private String rootTag;
	private String orderCol;//排序列名
	private String totalRecords;//记录总数
	private OMElement responseOME ;
	List resultSet = null;
	List resultRiskSet = null;
	List returnChildTag = null;
	public abstract void prepareCondition() ;

	public OMElement queryData(OMElement Oms) {
		try{
			
			this.setOms(Oms);
			prepareCondition();
//			Set resultSet = new HashSet();
			resultSet = new ArrayList();
			resultRiskSet = new ArrayList();//存放险种赔付信息
			returnChildTag = getChildTagName();
			
			QueryTool qTool = new QueryTool();

			ExeSQL tExeSQL = new ExeSQL();

			// 获取baseInfo
			baseInfoString = qTool.parseBaseInfo(Oms);
			int pageIndex = Integer.parseInt(baseInfoString[4]);
			int pageSize = Integer.parseInt(baseInfoString[5]);
			int startRow = (pageIndex - 1) * pageSize + 1;
			int endRow = pageIndex * pageSize;
			
			String rgtSql =this.getQuerySql();
			System.out.println("rgtsql=="+rgtSql);
			//记录总数
			if(rootTag.equals("GRPCLAIMLIST")){
				String countSql = this.getCountSql();
				totalRecords = String.valueOf(tExeSQL.getOneValue(countSql));
				
				
				//获取险种的赔付信息-(2013-12-9)
				String riskpaySql = this.getRiskSql();
				SSRS riskSSRS = tExeSQL.execSQL(riskpaySql);
				for(int k=1;k<=riskSSRS.MaxRow;k++){
					Map riskMap = new HashMap();
					int keynum = riskList.size();
					for(int m=1; m<=keynum; m++){
						riskMap.put(riskList.get(m - 1), riskSSRS.GetText(k, m));
					}
					resultRiskSet.add(riskMap);
				}
				
			}else{
				totalRecords = String.valueOf(tExeSQL.execSQL(rgtSql).MaxRow);
			}
			 
			 
			 //页码校验 
			 if( startRow>endRow||startRow>Integer.parseInt(totalRecords)){
				 responseOME = buildResponseOME(resultSet, returnChildTag,
							"00", "*", "请检查分页信息！");
				 return responseOME;
				}
			// 获取分页sql
			String pageRgtSql = qTool.buildPageSql(rgtSql, pageIndex,
					pageSize);

			SSRS rgtSSRS = tExeSQL.execSQL(pageRgtSql);
			int rgtCount = rgtSSRS.MaxRow;
			for (int i = 1; i <= rgtCount; i++) {
				Map schemaMap = new HashMap();
				for (int tagPointer = 0; tagPointer < returnChildTag.size(); tagPointer++) {
					schemaMap.put(returnChildTag.get(tagPointer), rgtSSRS.GetText(
							i, tagPointer + 1));
				}

				resultSet.add(schemaMap);

			}
			if(rgtCount>0){
					responseOME = buildResponseOME(resultSet, returnChildTag,
							"00", "0", "成功");
				 
			}else{
				 responseOME = buildResponseOME(resultSet, returnChildTag,
							"01", "*", "没有符合查询条件的数据");
			}
		}catch(Exception e){
			responseOME = buildResponseOME(resultSet, returnChildTag,
						"01", "*", "系统内部异常");
			e.printStackTrace();
		}
		
		System.out.println("返回报文:"+responseOME.toString());
		 
		return responseOME;
	}
	
	
	private OMElement buildResponseOME(List resultSet, List returnChildTag,
			String state, String errCode, String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement(this.getRootTag(), null);

		LoginVerifyTool tool = new LoginVerifyTool();
		QueryTool qTool = new QueryTool();
		OMElement BASEINFO = qTool.buildReturnBaseInfo(baseInfoString, totalRecords,
				state, errCode, errInfo);

		DATASET.addChild(BASEINFO);// 构建基础信息

		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
		if(resultSet!=null){
//			 遍历HashSet
			Iterator resultIterator = resultSet.iterator();
			while (resultIterator.hasNext()) {
				Map resultSchemaMap = (HashMap) resultIterator.next();
				OMElement ITEM = fac.createOMElement("ITEM", null);
				for(int tagCount=0;tagCount<returnChildTag.size();tagCount++){
					String childTagName = (String)returnChildTag.get(tagCount);
					tool.addOm(ITEM, childTagName, (String)resultSchemaMap.get(childTagName));
				}


				OUTPUTDATA.addChild(ITEM);
				
			}
		}
		if(rootTag.equals("GRPCLAIMLIST")){//增加险种的赔付信息2013-12-9
			OMElement OUTPUTDATA2 = fac.createOMElement("OUTPUTDATA2", null);
			
			if(resultRiskSet != null){
				Iterator riskIte = resultRiskSet.iterator();
				while(riskIte.hasNext()){
					Map riskmap = (HashMap)riskIte.next();
					OMElement item = fac.createOMElement("ITEM", null);
					for(int j=0;j<riskList.size();j++){
						String tagName = (String)riskList.get(j);
						tool.addOm(item, tagName, (String)riskmap.get(tagName));
					}
					OUTPUTDATA2.addChild(item);
				}
			}
			DATASET.addChild(OUTPUTDATA2);
		}

		DATASET.addChild(OUTPUTDATA);

		return DATASET;
	}


	public List getChildTagName() {
		return childTagName;
	}

	public void setChildTagName(List childTagName) {
		this.childTagName = childTagName;
	}


	public String getQuerySql() {
		return querySql;
	}


	public void setQuerySql(String querySql) {
		this.querySql = querySql;
	}

	public String getRootTag() {
		return rootTag;
	}

	public void setRootTag(String rootTag) {
		this.rootTag = rootTag;
	}

	public String getOrderCol() {
		return orderCol;
	}

	public void setOrderCol(String orderCol) {
		this.orderCol = orderCol;
	}

	public OMElement getOms() {
		return Oms;
	}

	public void setOms(OMElement oms) {
		Oms = oms;
	}

	public String getCountSql() {
		return countSql;
	}

	public void setCountSql(String countSql) {
		this.countSql = countSql;
	}

	public String getRiskSql() {
		return riskSql;
	}

	public void setRiskSql(String riskSql) {
		this.riskSql = riskSql;
	}

	public List getRiskList() {
		return riskList;
	}

	public void setRiskList(List riskList) {
		this.riskList = riskList;
	}

	public static void main(String[] args) {
		
	}
}
