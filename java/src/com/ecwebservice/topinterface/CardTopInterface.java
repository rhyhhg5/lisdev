/**
 * StandCardHYX.java
 * com.ecwebservice.personalquery
 *
 * Function： TODO 
 *
 *   ver     date      		author
 * ──────────────────────────────────
 *   		 2010-6-24 		caosg
 *
 * Copyright (c) 2010, TNT All Rights Reserved.
 */

package com.ecwebservice.topinterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.subinterface.CardHYX;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;

/**
 * 
 * @author Yuanzw
 * @version
 * @since Ver 1.1
 * @Date 
 */
/**
 * @author Administrator
 *
 */
public abstract class CardTopInterface implements CardHYX {

	protected String[] baseInfoString;

	private String[] errInfoArr = {"","","","00","*","未传值"};

	// 返回报文根节点
	private String root;
	

	protected LoginVerifyTool mTool = new LoginVerifyTool();

	protected ExeSQL mExeSQL = new ExeSQL();

	public CardTopInterface(String root) {
		this.root = root;
	}

	public OMElement queryData(OMElement Oms) {
		System.out.println("进入CardTopInterface！");
		OMElement responseOME = null;
		Map resultMap = new HashMap();
		Map conditionMap = new HashMap();
		
		
		try {
//			 获取查询条件Map
			conditionMap = mTool.getConditionMap(Oms);
			
//			getQueryFactor(conditionMap);
			
			//校验
			boolean checkFlag = checkData(conditionMap);
			String[] InfoArr = new String[6];
			InfoArr[0] = (String) conditionMap.get("BATCHNO");
			InfoArr[1] = (String) conditionMap.get("SENDDATE");
			InfoArr[2] = (String) conditionMap.get("SENDTIME");
			if(!checkFlag){
				InfoArr[3] = "01";
				InfoArr[4] = "1";
				InfoArr[5] = "校验失败";
				this.setErrInfoArr(InfoArr);
				
				responseOME = this.buildResponseOME(resultMap);
				return responseOME;
				
			}else{
				InfoArr[3] = "00";
				InfoArr[4] = "0";
				InfoArr[5] = "成功";
				this.setErrInfoArr(InfoArr);
			}

			resultMap = prepareData();

			responseOME = buildResponseOME(resultMap);
		} catch (Exception e) {
			e.printStackTrace();
			String[] InfoArr = { "","","","01", "*", "系统发生异常,请稍候再试！" };
			this.setErrInfoArr(InfoArr);
			responseOME = this.buildResponseOME(resultMap);
			return responseOME;

		}

		System.out.println("返回报文："+responseOME.toString());
		return responseOME;
	}

	/**
	 * prepareData:(查询业务数据并封装:ITEM 用ArrayList; 其余用HashMap )
	 * 
	 * @param
	 * @param queryFactor
	 * @param
	 * @return 设定文件
	 * @return Set DOM对象
	 * @throws
	 */
	public abstract Map prepareData();

	/**
	 * 获取查询条件
	 * 
	 * @param
	 * @param queryFactor
	 *            设定文件
	 * @return void DOM对象
	 * @throws
	 * @since CodingExample Ver 1.1
	 */
	protected abstract void getQueryFactor(Map queryFactor);
	
	//数据校验
	protected abstract boolean  checkData(Map conditionMap);

	/**
	 * 封装返回报文
	 * 
	 * @param
	 * @param resultMap
	 * @param
	 * @return 设定文件
	 * @return OMElement DOM对象
	 * @throws
	 * @since CodingExample Ver 1.1
	 */
	private OMElement buildResponseOME(Map resultMap) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement DATASET = fac.createOMElement(root, null);

		//构建基础信息 
		mTool.addOm(DATASET, "BATCHNO", errInfoArr[0]);
		mTool.addOm(DATASET, "SENDDATE", errInfoArr[1]);
		mTool.addOm(DATASET, "SENDTIME", errInfoArr[2]);
		mTool.addOm(DATASET, "STATE", errInfoArr[3]);
		mTool.addOm(DATASET, "ERRCODE", errInfoArr[4]);
		mTool.addOm(DATASET, "ERRINFO", errInfoArr[5]);
		

//		OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);

		// 遍历HashSet outputdateMap
		Iterator result = resultMap.keySet().iterator();
		while (result.hasNext()) {
			String key = (String) result.next(); // CLIST
			Object object = resultMap.get(key);

			prepare(key, object, fac, DATASET);
		}
//		DATASET.addChild(OUTPUTDATA);

		return DATASET;
	}

	/**
	 * 解析封装的业务查询数据
	 * 
	 * @param
	 * @param key
	 * @param
	 * @param object
	 * @param
	 * @param fac
	 * @param
	 * @param oem
	 * @param
	 * @return 设定文件
	 * @return OMElement DOM对象
	 * @throws
	 * @since CodingExample Ver 1.1
	 */
	private OMElement prepare(String key, Object object, OMFactory fac,
			OMElement oem) {
		if (object instanceof HashMap) {
			OMElement Info1 = fac.createOMElement(key, null);

			System.out.println("Info-key:" + key);

			Map resultMap = (HashMap) object;
			Iterator result = resultMap.keySet().iterator();
			while (result.hasNext()) {
				String key1 = (String) result.next();
				Object object1 = resultMap.get(key1);

				prepare(key1, object1, fac, Info1);
			}
			oem.addChild(Info1);
		} else if (object instanceof ArrayList) {
			List item = (ArrayList) object;
			Iterator resultSet = item.iterator();
			while (resultSet.hasNext()) {
				prepare(key, resultSet.next(), fac, oem);
			}
		} else {
			mTool.addOm(oem, key, (String) object);
		}
		return oem;
	}

	public LoginVerifyTool getTool() {
		return mTool;
	}

	public String[] getErrInfoArr() {
		return errInfoArr;
	}

	public void setErrInfoArr(String[] errInfoArr) {
		this.errInfoArr = errInfoArr;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}


}
