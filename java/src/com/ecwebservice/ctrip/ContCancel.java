package com.ecwebservice.ctrip;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.poi.hssf.record.formula.functions.Loginv;

import com.ecwebservice.ctrip.form.ApplyForm;
import com.ecwebservice.ctrip.form.CancelForm;
import com.ecwebservice.ctrip.form.PraseXmlUtil;
import com.ecwebservice.subinterface.ExternalActive;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.db.WFAppntListDB;
import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.db.WFTransLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFTransLogSchema;
import com.sinosoft.lis.vdb.WFTransLogDBSet;
import com.sinosoft.lis.vschema.WFContListSet;
import com.sinosoft.lis.vschema.WFTransLogSet;
import com.sinosoft.midplat.channel.util.IOTrans;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 功能：携程网（20元、30元航空旅游险）出保接口
 * @author daihongchang 2013-5-28
 *
 */
public class ContCancel {
	/*
	 * 错误处理类
	 */
	public CErrors mErrors = new CErrors();
	
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private CancelForm form = null;
	
	private LoginVerifyTool mTool = new LoginVerifyTool();
	
	private WFTransLogSet mTransLogSet = new WFTransLogSet();
	private WFTransLogSchema mTransLogSchema;
	private String repeatTransDate="";
	
	private String repeatTransTime="";
	MMap tMMap = new MMap();
	
	SSRS tSsrs = new SSRS();
	//结果状态
	String ResultStatus;
	//结果信息
	String ResultMsg;
	//批次号集合
	private String[] Batchnos = null;
	
	//保单号集合
	String[] InsuranceNo = null;
	
	//存储错误结果
	private String errinfo = "";
	
	//存储报文返回内容
	private String result = "";
	
	WFTransLogSchema mtransLogSchema = new WFTransLogSchema();
	public String Surrender(String request){
		System.out.println("携程退保发送报文"+request);
		boolean flag = false;
		try {
			//解析报文
			flag = parseDatas(request);
			System.out.println(flag);
			if(!flag){
				result = buildResponseOME("80", errinfo, null,"false");
				System.out.println("携程返回报文内容为"+result);
				return result;
			}
			//校验重复报文，如果flag若为true，则是新报文，继续处理；反之，不是新报文，返回上次报文后程序结束
			flag = checkNew();//flag若为true，新报文继续处理。
			if(!flag){
				result = buildResponseOME(ResultStatus, ResultMsg, InsuranceNo,"true");
				System.out.println("携程返回报文内容："+result);
				return result;
			}
//			校验基本信息
			flag = checkInfo();
			if(!flag){
				result = buildResponseOME("81", errinfo, null,"false");
				writeLog(form.getTransID(), "02",
						changDate(form.getTransDate()), changTime(form.getTransTime()), "00", "100", "81", errinfo);
				System.out.println("携程返回报文内容："+result);
				return result;
			}
//			 退保处理
			flag = deal();
			if (!flag) {
				if(ResultStatus==null||ResultStatus.equals("")){
					ResultStatus = "80";
				}
				result = buildResponseOME(ResultStatus, errinfo, null,"false");
				writeLog(form.getTransID(), "02",
						changDate(form.getTransDate()), changTime(form.getTransTime()), "00", "100", ResultStatus, errinfo);
				System.out.println("携程出保返回报文：" + result);
				return result;
			}
//			 保存数据
			flag = save();
			if (!flag) {
				writeLog(form.getTransID(), "02",
						changDate(form.getTransDate()), changTime(form.getTransTime()), "00", "100", "80", errinfo);
				result = buildResponseOME("80", errinfo, null,"false");
				System.out.println("携程出保返回报文：" + result);
				return result;
			}
			result = buildResponseOME("00", "成功", InsuranceNo,"false");
			System.out.println("携程出保返回报文：" + result);
			writeLog(form.getTransID(), "02",
					changDate(form.getTransDate()), changTime(form.getTransTime()), "00", "100", "00", "成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return result;
	}
	//解析报文内容方法 
	private boolean parseDatas(String request){
		try {
			form = (CancelForm)PraseXmlUtil.praseXml("//CancelRequest",request, new CancelForm());
			System.out.println("--------------->"+form.getTransSignature()); //签名此时应为空
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			errinfo = "退保异常，解析退保报文失败";
			return false;
		}
		return true;
	}
	/*
	 * 校验重复报文
	 */
	private boolean checkNew(){
//		首先，检查该条报文是不是重复发送的报文。
		String TransID = form.getTransID();
		String ApprovalTransID = form.getApprovalTransID();
		//modify by zhangyige 
		//1、80 为交易异常（如并发导致的卡号重复，数据提交异常），可以在不改变交易号的情况下重试完成交易
		//2、网络超时导致的 我司已承保，返回报文未发出的情况，携程将收到空报文，可以理解为80，可进行重试得到上次处理结果 00 - 成功
		//3、逻辑：不等于80的交易（如81、00）为老报文，直接返回处理结果；等于80的或者未交易过的为新报文，继续处理。
		String checkSQL = "select * from WFTransLog where batchno='"+TransID+"' and error <>'80' with ur";
		System.out.println("SQL:"+checkSQL);
		WFTransLogSet transLogSet = new WFTransLogDB().executeQuery(checkSQL);
		
		if(transLogSet.size()==0){
			System.out.println("该报文是新报文请求");
			return true;
		}else{
			System.out.println("该报文已经被处理,返回上次处理结果.");
			WFTransLogSchema transLogSchema = transLogSet.get(1);
			repeatTransDate = rechangeDate(transLogSchema.getSendDate());
			repeatTransTime = rechangeTime(transLogSchema.getSendTime());
			ResultStatus = transLogSchema.getErrCode();
			ResultMsg = transLogSchema.getErrInfo();
			if(form.getOriginalInsurances()!=null&&form.getOriginalInsuranceNo().length>0&&!form.getOriginalInsuranceNo()[0].equals("")){
				InsuranceNo = form.getOriginalInsuranceNo();
				return false;
			}else{
				String sql = "select cardno from WFContlist where bak4='"+TransID+"' and messageType='02' with ur";
				
				tSsrs = new ExeSQL().execSQL(sql);
				int length = tSsrs.getMaxRow();
				InsuranceNo = new String[length];
				for(int i=0;i<length;i++){
					InsuranceNo[i] = tSsrs.GetText(i+1, 1);
				}
			}
			return false;
		}
	}
	/*
	 * 校验基本信息
	 */
	private boolean checkInfo(){
		
		if(form.getTransID()==null||form.getTransID().equals("")){
			errinfo = "退保失败，退保请求中交易流水号为空";
			return false;
		}
		if(form.getApprovalTransID()==null||form.getApprovalTransID().equals("")){
			errinfo = "退保失败，退保请求中出保交易流水号为空";
			return false;
		}
		if(form.getTransDate()==null||form.getTransDate().equals("")){
			errinfo = "退保失败，退保请求中退保日期为空";
			return false;
		}
		if(form.getTransTime()==null||form.getTransTime().equals("")){
			errinfo = "退保失败，退保请求中退保时间为空";
			return false;
		}
//		if(form.getTransDate().trim().length()!=8){
//			errinfo = "退保失败，退保请求中退保时间格式不匹配";
//			return false;
//		}
		//卡是否生效（生效日期与退保日期比较）
//		String sql = "select CvaliDate from WFContList where bak4 = '"+form.getApprovalTransID()+"' with ur";
//		String Cvalidate = new ExeSQL().getOneValue(sql);
//		System.out.println("Cvalidate"+Cvalidate+"Transdate"+form.getTransDate());
//		if(Cvalidate.compareTo(changDate(form.getTransDate().trim()))<=0){
//			errinfo = "退保失败，卡已生效，不可撤单";
//			return false;
//		}
		//卡是否结算
		String sql2 = "select cardno from WFContlist where bak4='"+form.getApprovalTransID()+"' and messageType='01' with ur";
		tSsrs = new ExeSQL().execSQL(sql2);
		int length = tSsrs.getMaxRow();
		InsuranceNo = new String[length];
		for(int i=0;i<length;i++){
			InsuranceNo[i] = tSsrs.GetText(i+1, 1);
			String certifySql = "select * from licertify where cardno = '"+InsuranceNo[i]+"'";
			SSRS certifySSRS= new ExeSQL().execSQL(certifySql);
			if(certifySSRS != null && certifySSRS.MaxRow>0){
				errinfo = "退保失败，该卡已进入结算状态，不可以撤单！";
				return false;
			}
		}
		return true;
	}
	/*
	 * 退保处理
	 */
	private boolean deal(){
		//退保记录插入到保单表中和//写入外部激活信息表
		boolean b = true;
		VData tVData = new VData();
		WFContListSet contListSet = new WFContListSet();
		if(form.getOriginalInsuranceNo()==null||form.getOriginalInsuranceNo().length<=0||form.getOriginalInsuranceNo()[0].trim().equals("")){
			String sql ="select * from WFContlist where bak4 = '"+form.getApprovalTransID()+"' with ur";
			WFContListSet mContListSet = new WFContListDB().executeQuery(sql);
			WFContListSchema mContListSchema ;
			int length = mContListSet.size();
			Batchnos = new String[length];
			errinfo = "";
			for(int j=0;j<length;j++){
				String esql = "select CardStatus from LICardActiveInfoList where CardNo='"+mContListSet.get(j+1).getCardNo()+"' with ur";
				String CardStatus = new ExeSQL().getOneValue(esql);
				if(CardStatus.equals("03")){
					ResultStatus = "81";
					errinfo = errinfo+mContListSet.get(j+1).getCardNo()+" ";
					b = false;
				}
			}
			if(!b){
				errinfo = "退保失败，卡号为："+errinfo+"的卡已经退保";
				return false;
			}
			for(int i = 0;i<length;i++){
				mContListSchema = new WFContListSchema();
				Batchnos[i] = makeBatchNO();
				mContListSchema = mContListSet.get(i+1);
				mContListSchema.setBatchNo(Batchnos[i]);
				mContListSchema.setMessageType("02");
				mContListSchema.setSendDate(changDate(form.getTransDate()));
				mContListSchema.setSendTime(changTime(form.getTransTime()));
				//System.out.println("退保时间："+changDate(form.getTransDate())+"------退保日期："+changTime(form.getTransTime()));
				mContListSchema.setBak4(form.getTransID());
				contListSet.add(mContListSchema);
				
				//写入外部激活卡
				ExternalActive tExternalActive = new ExternalActive();
				
				boolean externalActiveFlag = tExternalActive.cancelCont(mContListSchema.getCardNo());
				if(!externalActiveFlag){
					errinfo = "退保异常，保单号为"+mContListSchema.getCardNo()+"的保单，写入外部激活卡失败";
					return false;
				}
			}
		
			tMMap.put(contListSet, "INSERT");
			return true;
		}
		InsuranceNo = form.getOriginalInsuranceNo();
		
		WFContListSchema mContListSchema;
		int length = InsuranceNo.length;
		Batchnos = new String[length];
		for(int j=0;j<length;j++){
			InsuranceNo[j] = InsuranceNo[j].trim();
			String esql = "select CardStatus from LICardActiveInfoList where CardNo='"+InsuranceNo[j]+"' with ur";
			String CardStatus = new ExeSQL().getOneValue(esql);
			if(CardStatus.equals("03")){
				ResultStatus = "81";
				errinfo = "退保失败，卡号为："+InsuranceNo[j]+"的卡已经退保";
				return false;
			}
		}
		for(int i=0;i<length;i++){
			String sql ="select * from WFContlist where bak4 = '"+form.getApprovalTransID().trim()+"' and cardno='" +
					InsuranceNo[i].trim()+"' with ur";
			System.out.println("SQL:"+sql);
			mContListSchema = new WFContListSchema();
			try {
				
				mContListSchema = new WFContListDB().executeQuery(sql).get(1);
			} catch (Exception e) {
				// TODO: handle exception
				ResultStatus = "81";
				errinfo="上次交易号或卡号数据有误";
				return false;
			}
			if(mContListSchema == null){
				ResultStatus = "81";
				errinfo="上次交易号或卡号数据有误";
				return false;
			}
			Batchnos[i] = makeBatchNO();
			mContListSchema.setBatchNo(Batchnos[i]);
			mContListSchema.setMessageType("02");
			mContListSchema.setBak4(form.getTransID());
			mContListSchema.setSendDate(changDate(form.getTransDate()));
			mContListSchema.setSendTime(changTime(form.getTransTime()));
			contListSet.add(mContListSchema);
			
			//写入外部激活卡
			
			ExternalActive tExternalActive = new ExternalActive();
			boolean externalActiveFlag = tExternalActive.cancelCont(InsuranceNo[i]);
			if(!externalActiveFlag){
				errinfo = "退保异常，卡号为"+mContListSchema.getCardNo()+"的卡，写入外部激活卡失败";
				return false;
			}
		}
		tMMap.put(contListSet, "INSERT");
		return true;
		
	}
	/*
	 * 数据保存
	 */
//	 提交卡激活信息
	private boolean save() {
		try {
			VData v = new VData();
			v.add(tMMap);
			PubSubmit p = new PubSubmit();
			if (!p.submitData(v, "")) {
				errinfo = "退保异常，数据提交失败!";
				return false;
			}
		} catch (Exception e) {
			System.out.println("退保异常，数据提交异常");
			e.printStackTrace();
			return false;
		}

		return true;
	}


	//添加原报文发送内容
	private void addOldData(OMElement CancelResponse, OMElement TransData) {
		// 添加主节点
		addOm(TransData,"TransID",form.getTransID());//交易流水号
		addOm(TransData,"ApprovalTransID",form.getApprovalTransID());//出保交易流水号
		addOm(TransData,"MerchantNo",form.getMerchantNo());//商户号
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement OriginalInsurances = fac.createOMElement("OriginalInsurances",null);
		if(form.getOriginalInsurances()==null||form.getOriginalInsuranceNo()[0] == null || form.getOriginalInsuranceNo()[0].equals("")){
			if(InsuranceNo!=null){
				for (int i = 0; i < InsuranceNo.length; i++) {
					System.out.println("IN------------"+InsuranceNo[i]);
					addOm(OriginalInsurances, "OriginalInsuranceNo", InsuranceNo[i]);
				}
			}else{
				
				addOm(OriginalInsurances, "OriginalInsuranceNo", "");
			}
		}else{
			
			for(int i=0;i<form.getOriginalInsuranceNo().length;i++){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
				addOm(OriginalInsurances, "OriginalInsuranceNo", form.getOriginalInsuranceNo()[i]);
			}
		}
		
		addOm(TransData, "Extend1", form.getExtend1());//扩展字段
		
		TransData.addChild(OriginalInsurances);
		
		//添加签名节点
		addOm(CancelResponse,"TransSignature",form.getTransSignature());
	}
	private String buildResponseOME(String ResultStatus,String ResultMsg,String[] InsuranceNo,String repeatFlag){
		String message = "";
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement CancelResponse = fac.createOMElement("CancelResponse",null);
		
		OMElement TransData = fac.createOMElement("TransData", null);
		
		//原样返回报文
		addOldData(CancelResponse, TransData);
		if(repeatFlag.equals("true")){
			addOm(TransData, "TransDate", repeatTransDate); // 出保日期
			addOm(TransData, "TransTime", repeatTransTime); // 出保时间
		}else{
			addOm(TransData, "TransDate", form.getTransDate()); // 出保日期
			addOm(TransData, "TransTime", form.getTransTime()); // 出保时间
		}
		addOm(TransData, "ResultStatus", ResultStatus);
		addOm(TransData, "ResultMsg", ResultMsg);
//		OMElement Insurances = fac.createOMElement("Insurances", null);
//		if(InsuranceNo!=null){
//			for (int i = 0; i < InsuranceNo.length; i++) {
//				addOm(Insurances, "InsuranceNo", InsuranceNo[i]);
//			}
//		}
//		TransData.addChild(Insurances);
		CancelResponse.addChild(TransData);
		message = "<?xml version=\"1.0\" encoding=\"GBK\"?>"+CancelResponse.toString();
		return message;
		
	}
	private static OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
	/**
	 * 生成批次号
	 */
	private static String  makeBatchNO(){
		//Calendar nowTime=Calendar.getInstance();
		//System.out.println(nowTime);
		//return nowTime.getTimeInMillis()+"";
		Calendar CD = Calendar.getInstance();
		int YY = CD.get(Calendar.YEAR);
		int MM = CD.get(Calendar.MONTH)+1;
		int DD = CD.get(Calendar.DATE);
		int HH = CD.get(Calendar.HOUR_OF_DAY);
		int NN = CD.get(Calendar.MINUTE);
		int SS = CD.get(Calendar.SECOND);
		int MI = CD.get(Calendar.MILLISECOND);
		return ""+YY+MM+DD+HH+NN+SS+MI+genRandomNum(4);
	}
	/**
     * 生成随即密码
     * @param pwd_len 生成的密码的总长度
     * @return  密码的字符串
     */
    private static String genRandomNum(int pwd_len) {
        //35是因为数组是从0开始的，26个字母+10个数字
//        final int maxNum = 100;
        int i; //生成的随机数
        int count = 0; //生成的密码的长度
        char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        StringBuffer pwd = new StringBuffer("");
//        Random r = new Random();
        while (count < pwd_len) 
        {
            //生成随机数，取绝对值，防止生成负数，
//            i = Math.abs(r.nextInt(maxNum)); //生成的数最大为36-1
        	i = (int)(Math.random()*10);
            if (i >= 0 && i < str.length) 
            {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }
	
	//根据出保交易流水号查出所有保单号
//	//处理后的记录保存到数据库中（使用LoginVerifyTool中的方法）
    private void writeLog(String batchNo,
			String messageType, String sendDate, String sendTime,
			String batchFlag, String opertator, String errCode, String errInfo) {
		MMap map = new MMap();
		VData tVData = new VData();
		System.out.println("senddate---------------"+sendDate);
		System.out.println("sendtime---------------"+sendTime);
		// 错误处理类
		CErrors mErrors = new CErrors();
		WFTransLogSchema trransLogSchema = new WFTransLogSchema();
		trransLogSchema.setBatchNo(batchNo);
		trransLogSchema.setMessageType(messageType);
		trransLogSchema.setSendDate(sendDate);
		trransLogSchema.setSendTime(sendTime);
		trransLogSchema.setBatchFlag(batchFlag);
		trransLogSchema.setopertator(opertator);
		trransLogSchema.setErrCode(errCode);
		trransLogSchema.setErrInfo(errInfo);

		map.put(trransLogSchema, "INSERT");

		// 入库
		PubSubmit tPubSubmit = new PubSubmit();
		tVData.add(map);
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			// @@错误处理
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LoginVerifyTool";
			tError.functionName = "writeLog";
			tError.errorMessage = "数据提交失败!";
			mErrors.addOneError(tError);

		}
	}
    /**
     * 日期格式转换
     * @param args
     */
    public String changDate(String s){
    	StringBuffer sf = new StringBuffer();
		sf.append(s.substring(0, 4));
		sf.append("-");
		sf.append(s.substring(4, 6));
		sf.append("-");
		sf.append(s.substring(6, 8));
    	return sf.toString();
    }
    /**
     * 时间格式转换
     * @param args
     */
    public String changTime(String s){
    	StringBuffer sf = new StringBuffer();
		sf.append(s.substring(0, 2));
		sf.append(":");
		sf.append(s.substring(2, 4));
		sf.append(":");
		sf.append(s.substring(4, 6));
    	return sf.toString();
    }
    /**
     * 日期格式再次转换
     * @param s
     * @return
     */
    public String rechangeDate(String s){
    	StringBuffer sf = new StringBuffer();
    	String[] ss = s.split("-");
    	for(int i=0;i<ss.length;i++){
    		sf.append(ss[i]);
    	}
    	return sf.toString();
    }
    /**
     * 时间格式再次转换
     * @param args
     */
    public String rechangeTime(String s){
    	StringBuffer sf = new StringBuffer();
    	String[] ss = s.split(":");
    	for(int i=0;i<ss.length;i++){
    		sf.append(ss[i]);
    	}
    	return sf.toString();
    }
	public static void main(String[] args) {
		ContCancel cc = new ContCancel();
		try {
			String mInFilePath = "D:\\退保请求.xml";
			
			InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = IOTrans.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "gbk");
			cc.Surrender(mInXmlStr);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
}
