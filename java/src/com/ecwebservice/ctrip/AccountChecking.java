package com.ecwebservice.ctrip;


import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.ctrip.form.AccountForm;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.vschema.WFContListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

/**
 * 功能：对账接口
 * @author daihongchang 2013-5-30
 *
 */
public class AccountChecking {
	/**错误处理类*/
	public CErrors mErrors = new CErrors();
	
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private boolean flag;
	
	private String erroinfo;
	
	//存储报文返回内容
	private String result;
	
	private AccountForm form;

	public String Check(String request){
		System.out.println("收到的请求内容"+request);//请求应该是个日期字符串
		
		try {
			flag = deal(request);
			if(!flag){
				result = buildResponseOME("01", erroinfo);
				System.out.println("携程返回报文："+result);
				return result;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println("携程返回报文"+result);
		return result;
	}
	private boolean deal(String request){
		//判断是日对账还是月对账
		request = request.trim();
		if(request.length()==10){    
			//(2013-05-10)长度为10，进入日账单
			flag = dealAccountByDay(request);
			if(!flag){
				erroinfo = "提取日对账数据时出错";
				return false;
			}
			return true;
		}else{
			//进入月对账
			flag = dealAccountByMonth(request);
			if(!flag){
				erroinfo = "提取月对账数据时出错";
				return false;
			}
			return true;
		}
	}
	private boolean dealAccountByDay(String requeset){
		try {
			String daySQL = "select * from WFcontlist where senddate='" +requeset+
			"' and sendoperator='ctrip' with ur";
			System.out.println("daySQL:"+daySQL);
			WFContListSet tContListSet = new WFContListDB().executeQuery(daySQL);
			result = buildResponseOME("00", "成功", tContListSet);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}
	private boolean dealAccountByMonth(String request){
		try {
			String monthSQL = "select * from WFContlist where substr(senddate,1,7)='" +request+
			"' and sendoperator='ctrip' with ur";
			System.out.println("monthSQL:"+monthSQL);
			WFContListSet tContListSet = new WFContListDB().executeQuery(monthSQL);
			result = buildResponseOME("00", "成功", tContListSet);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private String buildResponseOME(String ResultStatus,String ResultMsg,WFContListSet tContListSet){
		WFContListSchema tContListSchema;
		LICardActiveInfoListSchema tCardActiveInfoListSchema;
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement CheckingApply = fac.createOMElement("CheckingApply",null);
		
		OMElement TransDatas = fac.createOMElement("TransDatas",null);
		
		OMElement TransData ;
		
		for(int i=1;i<=tContListSet.size();i++){
			TransData = fac.createOMElement("TransData",null);
			form = new AccountForm();
			tContListSchema = new WFContListSchema();
			tContListSchema = tContListSet.get(i);
			String sql = "select * from LICardActiveInfoList where cardno='"+tContListSchema.getCardNo()+"' with ur";
			System.out.println("sql:"+sql);
			tCardActiveInfoListSchema = new LICardActiveInfoListDB().executeQuery(sql).get(1);
			form.setPolicyNo(tCardActiveInfoListSchema.getCardNo());
			form.setProductCode(tCardActiveInfoListSchema.getCardType());
			form.setInsuranceNum("1");
			String messageType ="";
			if(tContListSchema.getMessageType().equals("02")){
				messageType = "N";
			}
			if(tContListSchema.getMessageType().equals("01")){
				messageType = "P";
			}
			form.setInsuranceType(messageType);
			form.setPremium(tCardActiveInfoListSchema.getPrem());
			form.setEffecTime(tContListSchema.getCvaliDate()+" "+tContListSchema.getCvaliDateTime());
			form.setExpiryTime(tCardActiveInfoListSchema.getInActiveDate()+" "+tContListSchema.getCvaliDateTime());
			form.setTransTime(tContListSchema.getSendDate()+" "+tContListSchema.getSendTime());
			form.setCardNo(tCardActiveInfoListSchema.getIdNo());
			form.setName(tCardActiveInfoListSchema.getName());
			String sql2 = "select (days(inactivedate) - days(cvalidate)) from LICardActiveInfoList where cardno='"+tContListSchema.getCardNo()+"' with ur";

			form.setPolicyDays(new ExeSQL().getOneValue(sql2));
	
			
			//添加节点（PolicyNo,ProductCode,InsuranceType）
			addOm(TransData, "PolicyNo", form.getPolicyNo());
			addOm(TransData,"ProductCode",form.getProductCode());
			addOm(TransData,"InsuranceNum",form.getInsuranceNum());
			addOm(TransData,"InsuranceType",form.getInsuranceType());
			addOm(TransData,"Premium", form.getPremium());
			addOm(TransData, "EffecTime", form.getEffecTime());
			addOm(TransData, "ExpiryTime", form.getExpiryTime());
			addOm(TransData,"TransTime", form.getTransTime());
			addOm(TransData, "CardNo", form.getCardNo());
			addOm(TransData, "Name", form.getName());
			addOm(TransData, "PolicyDays", form.getPolicyDays());
			TransDatas.addChild(TransData);
		}
		
		addOm(TransDatas, "ResultStatus", ResultStatus);
		
		addOm(TransDatas, "ResultMsg", ResultMsg);
		CheckingApply.addChild(TransDatas);
		
		return CheckingApply.toString();
	}
	private String buildResponseOME(String ResultStatus,String ResultMsg){
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement CheckingApply = fac.createOMElement("CheckingApply",null);
		
		OMElement TransDatas = fac.createOMElement("TransDatas",null);
		
		OMElement TransData = fac.createOMElement("TransData",null);
		
		addOm(TransDatas, "ResultStatus", ResultStatus);
		
		addOm(TransDatas, "ResultMsg", ResultMsg);
		
		TransDatas.addChild(TransData);
		
		CheckingApply.addChild(TransDatas);
		
		return CheckingApply.toString();
	}
	private static OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
	
	public static void main(String[] args) {
		AccountChecking cc = new AccountChecking();
		try {
			
			cc.Check("2013-05-17");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}
}
