package com.ecwebservice.ctrip;

import java.util.Iterator;

import oracle.jdbc.dbaccess.DBAccess;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.sinosoft.utility.ExeSQL;

public class GetApprovalTransID {
	/** 卡号 */
	private String cardno;
	
	
	public OMElement queryData(OMElement oms){
		OMElement responseOMElement = null;
		boolean flag;
		cardno = parseXML(oms);
		if(cardno == null || cardno.equals("")){
			return null;
		}
		String approvalTransID = queryApprovalTransIDbyCardno(cardno);
		responseOMElement = buildResponseOME(approvalTransID);
		return responseOMElement;
}
	
	/**
	 * 解析报文获取卡号
	 */
	public String parseXML(OMElement oms){
		System.out.println("接收的报文为："+oms.toString());
		try {
			Iterator it = oms.getChildren();
			while(it.hasNext()){
				OMElement om = (OMElement)it.next();
				
				if(om.getLocalName().equals("GETAPPROVALTRANSID")){
					Iterator child = om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("CARDNO")){
							cardno = child_Element.getText();
							return cardno;
						}
					}
				}
			}
			return "";
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}
	}
	private String queryApprovalTransIDbyCardno(String cardno){
		String cardnoSQL = "select bak4 "+
							  "from wfcontlist cont "+
							  "where cardno = '"+cardno+"' "+
							    "and messagetype = (select (case "+
							                                "when li.cardstatus = '01' then "+
							                                 "'01' "+
							                                "when li.cardstatus = '03' then "+
							                                 "'02' "+
							                              "end) "+
							                         "from licardactiveinfolist li "+
							                        "where li.cardno = cont.cardno) with ur";
		ExeSQL exeSQL = new ExeSQL();
		String approvalTransID = exeSQL.getOneValue(cardnoSQL);
		return approvalTransID;
	}
	/**
	 * 返回报文
	 */ 
	private OMElement buildResponseOME(String approvalTransID){
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement getApprovalTransID = factory.createOMElement("GETAPPROVALTRANSID",null);
		addOm(getApprovalTransID, "APPROVALTRANSID", approvalTransID);
		root.addChild(getApprovalTransID);
		return root;
	}
	private static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}
	
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
}
