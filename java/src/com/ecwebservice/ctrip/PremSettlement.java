package com.ecwebservice.ctrip;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class PremSettlement {

	/** 回复报文 */
	private OMElement responseOME = null;

	private OMElement mOME = null;

	/** 批次号 */
	private String batchno = "";

	/** 报文发送日期 */
	private String sendDate = "";

	/** 报文发送时间 */
	private String sendTime = "";

	/** 报文类型 */
	private String messageType = "";

	/** 投保日期起期 */
	private String startDate = null;

	/** 投保日期止期 */
	private String endDate = null;

	/** 生效日期起期 */
	private String cvalistartDate = null;

	/** 生效日期止期 */
	private String cvaliendDate = null;
	
	/** 交易日期起期 */
	private String bstartDate = null;

	/** 交易日期止期 */
	private String bendDate = null;
	
	/** 卡号题头*/
	private String cardHeading = null;
//	/** 套餐编码 */
//	private String wrapcode = null;

	/** 返回信息 */
	private String RISKNAME = null;

	private String POLICYCONT = null;

	private String CANCELCONT = null;

	private String POLICYPREM = null;

	private String CANCELPREM = null;

	private String SUMPOLICYPREM = null;
	
	private String SUMCANCELPREM = null;
	
	private String SUMPOLICYCOUNT = null;
	
	private String SUMCANCELCOUNT = null;

	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if (!load()) {
			responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
					null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if (!check()) {
			responseOME = buildCardActiveResponseOME("01", "02", "校验信息失败", null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if (!deal()) {
			responseOME = buildCardActiveResponseOME("01", "03", "处理信息失败", null);
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		return responseOME;
	}

	/**
	 * 接收报文
	 * 
	 * @return
	 */
	private boolean load() {
		try {
			Iterator iter1 = this.mOME.getChildren();
			while (iter1.hasNext()) {
				OMElement Om = (OMElement) iter1.next();
				if (Om.getLocalName().equals("PREMSETTLEMENT")) {
					Iterator leaf_element = Om.getChildren();
					while (leaf_element.hasNext()) {
						OMElement leaf = (OMElement) leaf_element.next();
						if (leaf.getLocalName().equals("BATCHNO")) {
							this.batchno = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("SENDDATE")) {
							this.sendDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("SENDTIME")) {
							this.sendTime = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("MESSAGETYPE")) {
							this.messageType = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("STARTDATE")) {
							this.startDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("ENDDATE")) {
							this.endDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("BSTARTDATE")) {
							this.bstartDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("BENDDATE")) {
							this.bendDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("CARDHEADING")) {
							this.cardHeading = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("CVALISTARTDATE")) {
							this.cvalistartDate = leaf.getText();
							continue;
						}
						if (leaf.getLocalName().equals("CVALIENDDATE")) {
							this.cvaliendDate = leaf.getText();
							continue;
						}
//						if (leaf.getLocalName().equals("WRAPCODE")) {
//							this.wrapcode = leaf.getText();
//							continue;
//						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 校验报文信息
	 * 
	 * @return
	 */
	private boolean check() {
//		if ((this.startDate == null || this.startDate.equals(""))
//				|| (this.endDate == null || this.endDate.equals(""))) {
//			responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
//					null);
//			System.out.println("responseOME:" + responseOME);
//			return false;
//		}
//		日期两边都必填
		if(startDate!=null&&!("").equals(startDate)){
			if(endDate == null || endDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(endDate!=null && !("").equals(endDate)){
			if(startDate == null || startDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(bstartDate!=null&&!("").equals(bstartDate)){
			if(bendDate == null || bendDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(bendDate!=null && !("").equals(bendDate)){
			if(bstartDate == null || bstartDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(cvalistartDate!=null&&!("").equals(cvalistartDate)){
			if(cvaliendDate == null || cvaliendDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(cvaliendDate!=null && !("").equals(cvaliendDate)){
			if(cvalistartDate == null || cvalistartDate.equals("")){
				responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
						null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if((bstartDate == null||bstartDate.equals(""))&&(bendDate == null||bendDate.equals(""))&&
						(startDate == null||startDate.equals(""))&&(endDate == null||endDate.equals(""))&&
						(cvalistartDate == null||cvalistartDate.equals(""))&&(cvaliendDate == null||cvaliendDate.equals(""))){
			responseOME = buildCardActiveResponseOME("01", "01", "接收查询信息失败",
					null);
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		
		return true;
	}

	/**
	 * 报文业务处理
	 * 
	 * @return
	 */
	private boolean deal() {
		try {

			String policySQL = "select (select wrapname from ldwrap where riskwrapcode = li.cardtype),"
					+ "count(1),sum(double(li.prem)) "
					+ "from licardactiveinfolist li "
					+ "inner join WFCONTLIST cont on li.cardno = cont.cardno "
					+ "where 1=1 "
					+ "and li.operator='ctrip' " + "and li.cardstatus ='01' and cont.messagetype = '01' ";
			StringBuffer buf1 = new StringBuffer(policySQL);
//			if (this.wrapcode != null && !this.wrapcode.equals("")) {
//				buf1.append("and li.cardtype = '").append(this.wrapcode)
//						.append("' ");
//			}
			if(cardHeading!=null&&!("").equals(cardHeading)){
				String[] cardHeadings = cardHeading.split("@");
				int length = cardHeadings.length;
				buf1.append("and (");
				for(int i=0;i<length;i++){
					if(i == length -1){
						buf1.append("cont.cardno like '").append(cardHeadings[i]).append("%') ");
						break;
					}
					buf1.append("cont.cardno like '").append(cardHeadings[i]).append("%' or ");
				}
				
			}
			if (this.startDate != null && !this.startDate.equals("")) {
				buf1.append("and cont.senddate >='").append(startDate).append("' ");
			}
			if (this.endDate != null && !this.endDate.equals("")) {
				buf1.append("and cont.senddate <='").append(endDate).append("' ");
			}
			if (this.bstartDate != null && !this.bstartDate.equals("")) {
				buf1.append("and li.cvalidate + 30 day >= '").append(this.bstartDate)
						.append("' ");
			}
			if (this.bendDate != null && !this.bendDate.equals("")) {
				buf1.append("and li.cvalidate + 30 day <= '").append(this.bendDate)
						.append("' ");
			}
			
			if (this.cvalistartDate != null && !this.cvalistartDate.equals("")) {
				buf1.append("and li.cvalidate >= '").append(this.cvalistartDate)
						.append("' ");
			}
			if (this.cvaliendDate != null && !this.cvaliendDate.equals("")) {
				buf1.append("and li.cvalidate <= '").append(this.cvaliendDate)
						.append("' ");
			}

			buf1.append(" group by li.cardtype with ur");
			
			SSRS tSSRS1 = null;
			tSSRS1 = new ExeSQL().execSQL(buf1.toString());
			if (tSSRS1 == null || tSSRS1.MaxRow < 1) {
				responseOME = buildCardActiveResponseOME("01", "01",
						"没有得到查询结果", null);
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			
			String cancelSQL = "select (select wrapname from ldwrap where riskwrapcode = li.cardtype),"
				+ "count(1),sum(double(li.prem)) "
				+ "from licardactiveinfolist li "
				+ "inner join WFCONTLIST cont on li.cardno = cont.cardno "
				+ "where 1=1 "
				+ "and li.operator='ctrip' " + "and li.cardstatus ='03' and cont.messageType = '02' ";
			StringBuffer buf2 = new StringBuffer(cancelSQL);
//			if (this.wrapcode != null && !this.wrapcode.equals("")) {
//				buf2.append("and li.cardtype = '").append(this.wrapcode)
//				.append("' ");
//			}
			if(cardHeading!=null&&!("").equals(cardHeading)){
				String[] cardHeadings = cardHeading.split("@");
				int length = cardHeadings.length;
				buf2.append("and (");
				for(int i=0;i<length;i++){
					if(i == length -1){
						buf2.append("cont.cardno like '").append(cardHeadings[i]).append("%') ");
						break;
					}
					buf2.append("cont.cardno like '").append(cardHeadings[i]).append("%' or ");
				}
				
			}
			if (this.startDate != null && !this.startDate.equals("")) {
				buf2.append("and cont.senddate >='").append(startDate).append("' ");
			}
			if (this.endDate != null && !this.endDate.equals("")) {
				buf2.append("and cont.senddate <='").append(endDate).append("' ");
			}
			if (this.bstartDate != null && !this.bstartDate.equals("")) {
				buf2.append("and li.cvalidate + 30 day >= '").append(this.bstartDate)
						.append("' ");
			}
			if (this.bendDate != null && !this.bendDate.equals("")) {
				buf2.append("and li.cvalidate + 30 day <= '").append(this.bendDate)
						.append("' ");
			}
			if (this.cvalistartDate != null && !this.cvalistartDate.equals("")) {
				buf2.append("and li.cvalidate >= '").append(this.cvalistartDate)
						.append("' ");
			}
			if (this.cvaliendDate != null && !this.cvaliendDate.equals("")) {
				buf2.append("and li.cvalidate <= '").append(this.cvaliendDate)
						.append("' ");
			}
			buf2.append(" group by li.cardtype with ur");
			
			SSRS tSSRS2 = null;
			tSSRS2 = new ExeSQL().execSQL(buf2.toString());
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMElement CONTLIST = fac.createOMElement("CONTLIST", null);
			int policyCount = 0,cancelCount = 0;
			double policyPrem = 0.0,cancelPrem = 0.0;
			if (tSSRS2 == null || tSSRS2.MaxRow < 1) {
//				responseOME = buildCardActiveResponseOME("01", "01",
//						"没有得到查询结果", null);
//				System.out.println("responseOME:" + responseOME);
//				return false;
				for (int i = 1; i <= tSSRS1.MaxRow; i++) {
					this.RISKNAME = tSSRS1.GetText(i, 1);
					this.POLICYCONT = tSSRS1.GetText(i, 2);
					this.POLICYPREM = tSSRS1.GetText(i, 3);
					this.CANCELCONT = "0";
					this.CANCELPREM = "0";
					
					policyPrem = policyPrem + Double.parseDouble(POLICYPREM);
					cancelPrem = cancelPrem + Double.parseDouble(CANCELPREM);
					policyCount = policyCount + Integer.parseInt(this.POLICYCONT);
					cancelCount = cancelCount + Integer.parseInt(this.CANCELCONT);
					
					OMElement subItem = buildCardActiveResponseItem();
					CONTLIST.addChild(subItem);
				}
			}else{
				for (int i = 1; i <= tSSRS1.MaxRow; i++) {
				this.RISKNAME = tSSRS1.GetText(i, 1);
				this.POLICYCONT = tSSRS1.GetText(i, 2);
				this.POLICYPREM = tSSRS1.GetText(i, 3);
				if(tSSRS2.GetText(i, 2)==null||"".equals(tSSRS2.GetText(i, 2))){
					this.CANCELCONT ="0";
				}else{
					this.CANCELCONT = tSSRS2.GetText(i, 2);
				}
				if(tSSRS2.GetText(i, 3)==null||"".equals(tSSRS2.GetText(i, 3))){
					this.CANCELPREM ="0";
				}else{
					this.CANCELPREM = tSSRS2.GetText(i, 3);
				}
				policyPrem = policyPrem + Double.parseDouble(POLICYPREM);
				cancelPrem = cancelPrem + Double.parseDouble(CANCELPREM);
				policyCount = policyCount + Integer.parseInt(this.POLICYCONT);
				cancelCount = cancelCount + Integer.parseInt(this.CANCELCONT);
				
				OMElement subItem = buildCardActiveResponseItem();
				CONTLIST.addChild(subItem);
			 }
			}
			this.SUMPOLICYPREM = String.valueOf(policyPrem);
			this.SUMCANCELPREM = String.valueOf(cancelPrem);
			this.SUMPOLICYCOUNT = String.valueOf(policyCount);
			this.SUMCANCELCOUNT = String.valueOf(cancelCount);
			this.responseOME = buildCardActiveResponseOME("00", "", "",
					CONTLIST);
		} catch (Exception e) {
			System.out.println("业务处理异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * 创建返回报文
	 * 
	 * @param state
	 * @param errCode
	 * @param errInfo
	 * @return OMElement
	 */
	private OMElement buildCardActiveResponseOME(String state, String errCode,
			String errInfo, OMElement contList) {
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement DATASET = fac.createOMElement("PREMSETTLEMENT", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "MESSAGETYPE", this.messageType);

		if (state.equals("01")) {
			tool.addOm(DATASET, "STATE", state);
			tool.addOm(DATASET, "ERRCODE", errCode);
			tool.addOm(DATASET, "ERRINFO", errInfo);
			tool.addOm(DATASET, "OUTPUTDATA", null);
			tool.addOm(DATASET, "STARTDATE", "");
			tool.addOm(DATASET, "ENDDATE", "");
			tool.addOm(DATASET, "SUMPOLICYPREM", "");
			tool.addOm(DATASET, "SUMCANCELPREM", "");
			tool.addOm(DATASET, "SUMPOLICYCOUNT", "");
			tool.addOm(DATASET, "SUMCANCELCOUNT", "");
		}

		if (state.equals("00")) {
			tool.addOm(DATASET, "STATE", state);
			tool.addOm(DATASET, "ERRCODE", null);
			tool.addOm(DATASET, "ERRINFO", null);
			tool.addOm(DATASET, "STARTDATE", this.startDate);
			tool.addOm(DATASET, "ENDDATE", this.endDate);
			tool.addOm(DATASET, "CVALISTARTDATE", this.cvalistartDate);
			tool.addOm(DATASET, "CVALIENDDATE", this.cvaliendDate);
			tool.addOm(DATASET, "SUMPOLICYPREM", this.SUMPOLICYPREM);
			tool.addOm(DATASET, "SUMCANCELPREM", this.SUMCANCELPREM);
			tool.addOm(DATASET, "SUMPOLICYCOUNT", this.SUMPOLICYCOUNT);
			tool.addOm(DATASET, "SUMCANCELCOUNT", this.SUMCANCELCOUNT);
			OMElement OUTPUTDATA = fac.createOMElement("OUTPUTDATA", null);
			OUTPUTDATA.addChild(contList);
			DATASET.addChild(OUTPUTDATA);
		}
		return DATASET;
	}

	/**
	 * 创建返回结点
	 * 
	 * @param state
	 * @param errCode
	 * @param errInfo
	 * @return OMElement
	 */
	private OMElement buildCardActiveResponseItem() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tool = new LoginVerifyTool();

		OMElement ITEM = fac.createOMElement("ITEM", null);
		tool.addOm(ITEM, "RISKNAME", this.RISKNAME);
		tool.addOm(ITEM, "POLICYCONT", this.POLICYCONT);
		tool.addOm(ITEM, "CANCELCONT", this.CANCELCONT);
		tool.addOm(ITEM, "POLICYPREM", this.POLICYPREM);
		tool.addOm(ITEM, "CANCELPREM", this.CANCELPREM);
		return ITEM;
	}
}
