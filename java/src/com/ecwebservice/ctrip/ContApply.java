package com.ecwebservice.ctrip;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.xml.ctrl.blogic.CtripPrintBL;
import com.ecwebservice.ctrip.form.ApplyForm;
import com.ecwebservice.ctrip.form.Des3;
import com.ecwebservice.ctrip.form.PraseXmlUtil;
import com.ecwebservice.subinterface.ExternalActive;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.db.WFContListDB;
import com.sinosoft.lis.db.WFTransLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFBnfListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.schema.WFTransLogSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.WFAppntListSet;
import com.sinosoft.lis.vschema.WFBnfListSet;
import com.sinosoft.lis.vschema.WFContListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.lis.vschema.WFTransLogSet;
import com.sinosoft.midplat.channel.util.IOTrans;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 功能：携程网（20元、30元航空旅游险）出保接口
 * 
 * @author zhangyige 2013-05-17
 * 
 */
public class ContApply {
	
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = new GlobalInput();

	private MMap tMMap = new MMap();

	private LoginVerifyTool mTool = new LoginVerifyTool();

	private ApplyForm form = null;
	
	//结果状态
	String ResultStatus;
	//结果信息
	String ResultMsg;
	
	//发送信息时传递的Map集合
	private HashMap mAndeMap = new HashMap();

	// 存储返回报文内容
	private String result = "";

	// 存储错误结果
	private String errinfo = "";

	// 保单号集合
	private String[] InsuranceNo = null;
	// 批次号集合
	private String[] Batchnos = null;

	// 投保人性别
	private String appsex = "";
	
	//投保人出生日期
	private String appBirthday = "";
	
	// 被保人性别
	private String insusex = "";
	
	private String repeatTransDate="";
	
	private String repeatTransTime="";
	
	public ContApply(){
		synchronized (ContApply.class) {
			try {
				String sqlurl = "select codename from LDCODE  where codetype='ctripIntv' and code='intv'";//获取并发间隔
		    	String intv = new ExeSQL().getOneValue(sqlurl);
		    	long l = Long.parseLong(intv);
		    	//System.out.println("停止时间(ms)"+l);
				Thread.sleep(l);//初始为500
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public String Insure(String request) {
		System.out.println("携程出保发送报文开始时间："+System.currentTimeMillis()+"内容：" + request);
		boolean flag = false;
		try {
			// 解析出保报文
			flag = parseDatas(request);
			System.out.println("flag:"+flag);
			if (!flag) {
				result = buildResponseOME("80", errinfo, null,"false");
				System.out.println("携程出保返回报文：" + result);
				return result;
			}
//			校验重复报文，如果flag若为true，则是新报文，继续处理；反之，不是新报文，返回上次报文后程序结束
			flag = checkNew();//flag若为true，新报文继续处理。
			if(!flag){
				result = buildResponseOME(ResultStatus, ResultMsg, InsuranceNo,"true");
				System.out.println("携程返回报文内容："+result);
				return result;
			}
			// 校验基本信息
			flag = checkInfo();
			if (!flag) {
				result = buildResponseOME("81", errinfo, null,"false");
				writeLog(form.getTransID(), "01",
						changDate(form.getTransDate()), changTime(form.getTransTime()), "00", "100", "81", errinfo);
				System.out.println("携程出保返回报文：" + result);
				return result;
			}
			// 承保处理
			flag = deal();
			if (!flag) {
				result = buildResponseOME("80", errinfo, null,"false");
				writeLog(form.getTransID(), "01",
						changDate(form.getTransDate()), changTime(form.getTransTime()), "00", "100", "80", errinfo);
				System.out.println("携程出保返回报文：" + result);
				return result;
			}
		
//			if (!flag) {
//				result = buildResponseOME("80", errinfo, null,"false");
//				writeLog(form.getTransID(), "01",
//						PubFun.getCurrentDate(), PubFun.getCurrentTime(), "00", "100", "80", errinfo);
//				System.out.println("携程出保返回报文：" + result);
//				return result;
//			}
			// 保存数据
			System.out.println("保存数据开始"+System.currentTimeMillis());
			flag = save();
			System.out.println("保存数据结束"+System.currentTimeMillis());
			if (!flag) {
				result = buildResponseOME("80", errinfo, null,"false");
				writeLog(form.getTransID(), "01",
						changDate(form.getTransDate()), changTime(form.getTransTime()), "00", "100", "80", errinfo);
				System.out.println("携程出保返回报文：" + result);
				return result;
			}
//			 发送短信和邮件
			flag = noticeCustomer();
			makePDF(InsuranceNo);
			result = buildResponseOME("00", "成功", InsuranceNo,"false");
			writeLog(form.getTransID(), "01",
					changDate(form.getTransDate()), changTime(form.getTransTime()), "00", "100", "00", "成功");
			System.out.println("携程出保返回报文：" + result);

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return result;
	}
	//生成电子保单
	private void makePDF(String[] cardnos){
		int len = cardnos.length;
		for(int i=0;i<len;i++){
			String cardno = cardnos[i];
			CtripPrintBL ecp = new CtripPrintBL();
			MsgCollection tMsgCollection = new MsgCollection();
			MsgHead tMsgHead = new MsgHead();
			tMsgHead.setBatchNo(makeBatchNO());
			tMsgHead.setBranchCode("001");
			tMsgHead.setMsgType("INDIGO");
			tMsgHead.setSendDate(PubFun.getCurrentDate());
			tMsgHead.setSendTime(PubFun.getCurrentTime());
			tMsgHead.setSendOperator("EC_WX");
			tMsgCollection.setMsgHead(tMsgHead);
			CertPrintTable tCertPrintTable = new CertPrintTable();
			tCertPrintTable.setCardNo(cardno);
			tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);
			ecp.deal(tMsgCollection);
		}
	}

	// 解析报文内容
	private boolean parseDatas(String request) {

		try {
			form = (ApplyForm) PraseXmlUtil.praseXml("//ApplyRequest", request,
					new ApplyForm());
			System.out.println("----------------->"+form.getTransSignature());//暂时没有签名
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errinfo = "投保异常,解析出保报文失败！";
			return false;
		}
		return true;
	}
	/**
	 * 检验重复报文
	 */
	private boolean checkNew(){
		//首先，检查该报文是不是重复发送的报文
		//modify by zhangyige 
		//1、80 为交易异常（如并发导致的卡号重复，数据提交异常），可以在不改变交易号的情况下重试完成交易
		//2、网络超时导致的 我司已承保，返回报文未发出的情况，携程将收到空报文，可以理解为80，可进行重试得到上次处理结果 00 - 成功
		//3、逻辑：不等于80的交易（如81、00）为老报文，直接返回处理结果；等于80的或者未交易过的为新报文，继续处理。
		String TransID = form.getTransID().trim();
		String checkSQL = "select * from WFTransLog where batchno='"+TransID+"' and errcode <>'80' with ur";
		
		System.out.println("SQL:"+checkSQL);
		WFTransLogSet transLogSet = new WFTransLogDB().executeQuery(checkSQL);
		if(transLogSet.size()==0){
			System.out.println("该报文是新报文请求");
			return true;
		}else{
			
			String insuresSQL = "select * from WFContlist where bak4='"+TransID+"' with ur";
			WFContListSet contlistSet = new WFContListDB().executeQuery(insuresSQL);
			int length = contlistSet.size();
			InsuranceNo = new String[length];
			for(int i=0;i<length;i++){
				InsuranceNo[i] = contlistSet.get(i+1).getCardNo();
			}
			
			System.out.println("该报文已经被处理,返回上次处理结果.");
			WFTransLogSchema transLogSchema = transLogSet.get(1);
			repeatTransDate = rechangeDate(transLogSet.get(1).getSendDate());
			repeatTransTime = rechangeTime(transLogSet.get(1).getSendTime());
			ResultStatus = transLogSchema.getErrCode();
			ResultMsg = transLogSchema.getErrInfo();
			
			return false;
		}
	}
	/**
	 * 校验基本信息
	 * 
	 * @return
	 */
	private boolean checkInfo() {
		
		if (form.getTransID() == null || form.getTransID().equals("")) {
			errinfo = "投保失败，没有得到批次号!";
			return false;
		}
		boolean validBatchNo = mTool.isValidBatchNo(form.getTransID());
		if (!validBatchNo) {
			errinfo = "投保失败，批次号不唯一!";
			return false;
		}
		if (form.getProductCode().equals("") || form.getProductCode() == null) {
			errinfo = "投保失败，没有得到套餐编码!";
			return false;
		}
		if (form.getTransDate().equals("") || form.getTransDate() == null) {
			errinfo = "投保失败，没有得到激活时间!";
			return false;
		}
		if (form.getEffectDate().equals("") || form.getEffectDate() == null) {
			errinfo = "投保失败，没有得到生效时间!";
			return false;
		}
		
		// 投保人信息判断
		if (form.getProposerName().equals("") || form.getProposerName() == null) {
			errinfo = "投保失败，没有得到投保人姓名!";
			return false;
		}
		if (form.getProposerBirthDate().equals("")
				|| form.getProposerBirthDate() == null) {
			errinfo = "投保失败，没有得到投保人出生日期!";
			return false;
		}
		if (form.getProposerCardType().equals("")
				|| form.getProposerCardType() == null) {
			errinfo = "投保失败，没有得到投保人ID类型!";
			return false;
		}
		if (form.getProposerCardNo().equals("")
				|| form.getProposerCardNo() == null) {
			errinfo = "投保失败，没有得到投保人ID号码!";
			return false;
		}
//		if (form.getProposerMobile().equals("")
//				|| form.getProposerMobile() == null) {
//			errinfo = "投保失败，没有得到投保人手机号码!";
//			return false;
//		}
		// 被保人信息判断
		if (form.getOwnerName().equals("") || form.getOwnerName() == null) {
			errinfo = "投保失败，没有得到被保人姓名!";
			return false;
		}
		if (form.getBirthDate().equals("") || form.getBirthDate() == null) {
			errinfo = "投保失败，没有得到被保人出生日期!";
			return false;
		}
		if (form.getCardType().equals("") || form.getCardType() == null) {
			errinfo = "投保失败，没有得到被保人ID类型!";
			return false;
		}
		if (form.getCardNo().equals("") || form.getCardNo() == null) {
			errinfo = "投保失败，没有得到被保人ID号码!";
			return false;
		}
		// 如果证件类型为身份证，处理投保人与被保人性别
		if (form.getProposerCardType().equals("01")) {
			appsex = PubFun.getSexFromId(form.getProposerCardNo());
			if(appsex==null||appsex.equals("")){
				errinfo = "投保失败，身份证号码有误！";
				return false;
			}
		}else{
			appsex = "2";
		}
		if (form.getCardType().equals("01")) {
			insusex = PubFun.getSexFromId(form.getCardNo());
		}else{
			insusex = "2";
		}
		// 携程订单号不能为空
		if (form.getOrderID().equals("") || form.getOrderID() == null) {
			errinfo = "投保失败，没有得到携程订单号!";
			return false;
		}
		// 投保份数不能为空
		if (form.getInsuranceNum().equals("") || form.getInsuranceNum() == null) {
			errinfo = "投保失败，没有得到投保份数!";
			return false;
		}
		// 投保份数必须大于0
		if (Integer.parseInt(form.getInsuranceNum()) <= 0) {
			errinfo = "投保失败，投保份数必须大于0!";
			return false;
		}
		if(form.getBirthDate().trim().length()!=10){
			errinfo = "投保失败，被保人出生日期格式不正确";
			return false;
		}
		if(form.getTransDate().trim().length()!=8){
			errinfo = "投保失败，出保日期";
			return false;
		}
		// 获取可用卡号（确定获取的是携程网的卡号）
		String copys = form.getInsuranceNum();
//		String searchCardnoSQL = "select lzcn.cardno,lzcn.cardpassword from LZCardNumber lzcn "
//				+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode "
//				+ "and lzc.StartNo = lzcn.CardSerNo "
//				+ "and lzc.EndNo = lzcn.CardSerNo "
//				+ "where  lzc.State in ('10', '11','2','13') "
//				+ "and not exists (select 1 from licardactiveinfolist where cardno = lzcn.cardno)"
//				+ "and lzc.certifycode in (select certifycode from lmcardrisk where riskcode='"+form.getProductCode()+"' and risktype='W') "
//				+ "fetch first "+ copys + " rows only ";
		String searchCardnoSQL = "select lzcs.CardNo,lzcs.CardPassword from LZCardSynchro lzcs "
				+ "where lzcs.state in ('10','11') "
				+ "and not exists (select 1 from licardactiveinfolist where cardno = lzcs.cardno) "
				+ "and RiskWrapCode = '"+form.getProductCode()+"' "
				+ "fetch first "+ copys + " rows only ";
		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(searchCardnoSQL);
		if (tSSRS != null) {
			int length = tSSRS.getMaxRow();
			if(length==0){
				errinfo = "投保失败，无可用保单号";
				return false;
			}
			InsuranceNo = new String[length];
			Batchnos = new String[length];
			
	        PubSubmit tempPubSubmit=new PubSubmit();
	        VData tempVData = new VData();
			for (int i = 0; i < length; i++) {
				InsuranceNo[i] = tSSRS.GetText(i + 1, 1);
				System.out.println("------------------------------------>生成卡号"+InsuranceNo[i]);
				String tUpdateStateSQL = "update LZCardSynchro set state = '14' where cardno = '"+InsuranceNo[i]+"' ";
				MMap tempMap = new MMap();
				tempMap.put(tUpdateStateSQL,"UPDATE");
		        tempVData.add(tempMap);
		        tempPubSubmit.submitData(tempVData,"");
				Batchnos[i] = makeBatchNO();
			}
		}
		// 投保份数超限。该被保人已经购买了N份保险，还可以再购买M份保险。
		String[] tCopys = copysLimit();
		if (tCopys != null) {
			String N = tCopys[0];
			String M = tCopys[1];
			errinfo = "投保失败，投保份数超限。该被保人已经购买了" + N + "份保险，还可以再购买" + M + "份保险。";
			return false;
		}

		return true;
	}

	// 投保份数超限。该被保人已经购买了N份保险，还可以再购买M份保险。
	private String[] copysLimit() {
		String[] tCopys = new String[2];
		// 被保人份数校验开始
		int copys = Integer.parseInt(form.getInsuranceNum());
		String cardNo = InsuranceNo[0];
		ExeSQL tExeSQL = new ExeSQL();
		// 准备校验SQL,校验该激活卡是否是lmcardrisk中的risktype = 'W'的类型
		String checkSql = " select lmcr.riskcode, re.CertifyCode from lmcardrisk lmcr,"
				+ "(select lzcn.CardNo, lzc.CertifyCode, lzc.State from LZCardNumber lzcn "
				+ " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and "
				+ "lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
				+ "where 1 = 1 and lzcn.CardNo = '"
				+ cardNo
				+ "') re where lmcr.risktype = 'W' and lmcr.certifycode = re.certifycode ";
		SSRS checkSSRS = tExeSQL.execSQL(checkSql);

		String insureSql = null;
		String cardNoStr = cardNo.toString().substring(0, 2);
		if (checkSSRS.MaxRow != 0
				&& (form.getEffectDate() != null || form.getEffectDate() != "")) {
			insureSql = "select li.cardno,li.Cvalidate,li.InActiveDate,wf.bak5,wf.bak6,wf.bak7,wf.bak8 from LICardActiveInfoList li inner join Wfcontlist wf " 
					+ " on li.cardno = wf.cardno where 1=1 "
//					+ " and li.CardNo like '"+ cardNoStr+ "%' "
					+ " and li.cardtype = '"+ checkSSRS.GetText(1, 1)+ "' "
					+ " and li.name='"
					+ form.getOwnerName()
					+ "' and li.sex='"
					+ insusex
					+ "'and li.birthday='"
					+ form.getBirthDate()
					+ "' and li.idtype='"
					+ codeChange(form.getCardType())   //转变卡类型编号
					+ "'and li.idno='"
					+ form.getCardNo()
					+ "' and li.InActiveDate>'"
					+ PubFun.getCurrentDate()
//					+ "' and cvalidate<='"
//					+ PubFun.getCurrentDate()
					+ "' "
					+ "and (li.CardStatus = '01' or li.CardStatus is null)";

			SSRS insureSSRS = tExeSQL.execSQL(insureSql);
			int now = 0;
			int alreadyNo = 0;
			FDate fdate = new FDate();
			LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
			System.out.println("insureSSRS.MaxRow:--lcb:" + insureSSRS.MaxRow);
			for (int i = 1; i <= insureSSRS.MaxRow; i++) {
				Date oldCvalidate = fdate.getDate(insureSSRS.GetText(i, 2)); // 结果中的生效日期，用FDate处理
				Date oldInactivedate = fdate.getDate(insureSSRS.GetText(i, 3)); // 结果中失效日期，用FDate处理
				Date newCvalidate = fdate.getDate(changDate(form.getEffectDate().trim())); // 当前激活卡的生效日期，用FDate处理
				
				String oldD = insureSSRS.GetText(i, 4)+insureSSRS.GetText(i, 5);
				String oldA = insureSSRS.GetText(i, 6)+insureSSRS.GetText(i, 7);
				System.out.println("oldD"+oldD+" oldA"+oldA+" newD"+form.getDepartureDate()+form.getDepartureTime()+" newA"+form.getArrivalDate()+form.getArrivalTime());
				String[] inactivedate = tLoginVerifyTool.getMyProductInfo(
						cardNo, changDate(form.getEffectDate().trim()), null);
				Date newInactivedate = fdate.getDate(inactivedate[2]); // 当前激活卡的失效日期，用FDate处理
				if ((oldCvalidate.compareTo(newCvalidate) <= 0 && oldInactivedate
						.compareTo(newCvalidate) > 0)
						|| (oldCvalidate.compareTo(newInactivedate) < 0 && oldInactivedate
								.compareTo(newInactivedate) >= 0)) {
					// compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
					String newD = form.getDepartureDate().trim()+form.getDepartureTime().trim();
					String newA = form.getArrivalDate().trim()+form.getArrivalTime().trim();
					if ((oldD.compareTo(newD) <= 0 && oldA
							.compareTo(newA) > 0)
							|| (oldD.compareTo(newA) < 0 && oldA
									.compareTo(newA) >= 0)) {
						alreadyNo++;
					}
				}
				System.out.println("alreadyNo:--lcb:hasdone:" + alreadyNo);
			}
			now = alreadyNo + copys;
			String maxNoSql = null;
			// 取最大份数
			maxNoSql = " select maxcopys from ldwrap where riskwrapcode='"
					+ form.getProductCode() + "' ";
			String temp2 = tExeSQL.execSQL(maxNoSql).GetText(1, 1);
			int maxNo = 0;
			if (temp2.matches("^\\d+$")) {
				maxNo = Integer.parseInt(temp2);
				if (maxNo < now) {
					// @@错误处理被保人投保份数超过验证
					// this.mErrors.copyAllErrors(tPubSubmit.mErrors);
					tCopys[0] = String.valueOf(alreadyNo);
					tCopys[1] = String.valueOf(maxNo - alreadyNo);
					return tCopys;
				}

			}

		} else {
			insureSql = "select count(1) + 1 from LICardActiveInfoList li where 1=1 "
//					+ " and CardNo like '"+ cardNoStr + "%' "
					+ " and li.cardtype = '"+ form.getProductCode()+ "' "
					+ " and name='"
					+ form.getOwnerName()
					+ "' and sex='"
					+ insusex
					+ "' and birthday='"
					+ form.getBirthDate()
					+ "' and idtype='"
					+ codeChange(form.getCardType())  //转变卡类型编码
					+ "' and idno='"
					+ form.getCardNo() + "' ";

			String temp1 = tExeSQL.execSQL(insureSql).GetText(1, 1);
			int now = 0;
			int alreadyNo = 0;
			if (temp1.matches("^\\d+$")) {
				alreadyNo = Integer.parseInt(temp1);
			}
			now = alreadyNo + copys;
			String maxNoSql = null;
			maxNoSql = " select maxcopys from ldwrap where riskwrapcode='"
					+ form.getProductCode() + "' ";
			String temp2 = tExeSQL.execSQL(maxNoSql).GetText(1, 1);
			int maxNo = 0;
			if (temp2.matches("^\\d+$")) {
				maxNo = Integer.parseInt(temp2);
				if (maxNo < alreadyNo) {
					tCopys[0] = String.valueOf(alreadyNo);
					tCopys[1] = String.valueOf(maxNo - alreadyNo);
					return tCopys;
				}

			}
			
		}
		return null;
	}

	/**
	 * 处理卡激活信息
	 * 
	 * @return
	 */
	private boolean deal() {
		try {
			String insuYear = null;
			String insuYearFlag = null;
			String prem = null;
			String Amnt = null;
			String certifyCode = null;
			String certifyName = null;
		
			String insuYearSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
					+ form.getProductCode()
					+ "' and calfactor='InsuYear'   with ur";
			String insuYearFlagSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
					+ form.getProductCode()
					+ "' and calfactor='InsuYearFlag'   with ur";
			String premSql = "select sum(double(calfactorvalue)) from ldriskdutywrap where riskwrapcode= '" 
				    + form.getProductCode()
				    +"' and calfactor='Prem' with ur";
			String AmntSql = "select sum(double(calfactorvalue)) from ldriskdutywrap where riskwrapcode= '" 
				    + form.getProductCode()
					+ "' and calfactor='Amnt' with ur";
			String certifyCodeSql = "select certifycode from lmcardrisk where riskcode='"+form.getProductCode()+"' and risktype='W' fetch first 1 rows only with ur";
			String certifyNameSql = "select wrapname from ldwrap where riskwrapcode='"+form.getProductCode()+"' with ur";
			prem = new ExeSQL().getOneValue(premSql);
			Amnt = new ExeSQL().getOneValue(AmntSql);
		    certifyCode = new ExeSQL().getOneValue(certifyCodeSql);
		    certifyName = new ExeSQL().getOneValue(certifyNameSql);
			insuYear = new ExeSQL().getOneValue(insuYearSql);
			insuYearFlag = new ExeSQL().getOneValue(insuYearFlagSql);
			int length = InsuranceNo.length;
			for (int i = 0; i < length; i++) {
				WFContListSet tContListList = new WFContListSet();
				WFAppntListSet tAppntList = new WFAppntListSet();
				WFInsuListSet tInsuList = new WFInsuListSet();
				WFBnfListSet tBnfList = new WFBnfListSet();
				LICardActiveInfoListSet exCardActiveInfoListSet  = new LICardActiveInfoListSet();
				String cardNo = InsuranceNo[i];
				// 保险卡信息
				WFContListSchema contListSchema = new WFContListSchema();
				// 投保人信息
				WFAppntListSchema appntListSchema = new WFAppntListSchema();
				// 被保人信息
				WFInsuListSchema insuListSchema = new WFInsuListSchema();
				// 受益人信息
				WFBnfListSchema bnfListSchema ;

				// 保单表
				contListSchema.setBatchNo(Batchnos[i]);
				contListSchema.setSendDate(changDate(form.getTransDate().trim()));
				contListSchema.setSendTime(changTime(form.getTransTime().trim()));
				contListSchema.setBranchCode("ctrip");
				contListSchema.setSendOperator("ctrip");
				contListSchema.setMessageType("01");// 正常出单（报文类型）
				contListSchema.setCardDealType("01");//处理方式
				contListSchema.setCardNo(cardNo);
				
				contListSchema.setPassword("");
				contListSchema.setCertifyCode(certifyCode);//  保险卡类型
				contListSchema.setCertifyName(certifyName);//  保险卡名称
				contListSchema.setRiskCode(form.getProductCode());//     套餐编码 即报文中的产品编号
				contListSchema.setActiveDate(changDate(form.getTransDate().trim()));//激活日期 ，即是出保日期
				contListSchema.setManageCom("");
				contListSchema.setSaleChnl("");
				contListSchema.setAgentCom("");//处理机构
				contListSchema.setAgentCode("");//业务员代码
				contListSchema.setAgentName("");
				contListSchema.setPayMode("");//缴费方式
				contListSchema.setPayIntv("");
				contListSchema.setPayYear("");//缴费年期
				contListSchema.setPayYearFlag("");
				contListSchema.setCvaliDate(changDate(form.getEffectDate().trim()));
				contListSchema.setCvaliDateTime(changTime(form.getEffectTime().trim()));
				contListSchema.setPrem(prem);
				contListSchema.setAmnt(Amnt);
				contListSchema.setAppntNo("1");//投保人号码
				contListSchema.setInsuYear(Integer.parseInt(insuYear));
				contListSchema.setInsuYearFlag(insuYearFlag);
				contListSchema.setCopys("1");
				contListSchema.setTicketNo(form.getOrderID());// 乘意险票号，即订单号
				contListSchema.setTeamNo(form.getFlightNo());
				contListSchema.setSeatNo("");//座位号
				contListSchema.setFrom(form.getDepartureCity());
				contListSchema.setTo(form.getArrivalCity());
				/*保存下面四个字段是因为携程舱对舱的需要*/
				contListSchema.setBak5(form.getDepartureDate().trim());
				contListSchema.setBak6(form.getDepartureTime().trim());
				contListSchema.setBak7(form.getArrivalDate().trim());
				contListSchema.setBak8(form.getArrivalTime().trim());
				
				contListSchema.setRemark("");//  说明备注
				contListSchema.setCardArea("");//   承保区域
				contListSchema.setMult(0);
				
				contListSchema.setBak4(form.getTransID());//保存携程发来的批次号
				tContListList.add(contListSchema);
				tMMap.put(tContListList, SysConst.INSERT);
				
				// 投保人表
				appntListSchema.setCardNo(cardNo);
				appntListSchema.setBatchNo(Batchnos[i]);
				appntListSchema.setAppntNo("1");
				
				appntListSchema.setName(form.getProposerName());
				appntListSchema.setSex(appsex);//性别（非空）
				appntListSchema.setBirthday(changDate(form.getProposerBirthDate()));//出生日期（非空）
				appntListSchema.setIDType(codeChange(form.getProposerCardType()));//转变卡类型编码
				appntListSchema.setIDNo(form.getProposerCardNo());

				appntListSchema
						.setOccupationType("");//投保人职业类型
				appntListSchema
						.setOccupationCode("");//职业代码
				appntListSchema.setPostalAddress("");//联系地址
				appntListSchema.setZipCode("");
				appntListSchema.setPhont("");//联系电话
				appntListSchema.setMobile(form.getProposerMobile());
				appntListSchema.setEmail(form.getProposerEmail());
				appntListSchema.setGrpName("");//单位名称
				appntListSchema.setCompanyPhone("");//单位电话
				appntListSchema.setCompAddr("");//单位地址
				appntListSchema.setCompZipCode("");//单位邮编
				
				tAppntList.add(appntListSchema);
				
				tMMap.put(tAppntList, SysConst.INSERT);
				
				// 被保人表
				insuListSchema.setCardNo(cardNo);
				insuListSchema.setBatchNo(Batchnos[i]);
				
				insuListSchema.setInsuNo("1");// 编号     //暂定为1 ，？？？？？
				insuListSchema.setToAppntRela(form.getProposerRelation());// 被保人和投保人关系 与 投保人和被保人关系的区别
				insuListSchema
						.setRelationCode("00");// 被保人是否连带
				insuListSchema
						.setRelationToInsured("");//  与主要被保人关系
				insuListSchema.setReInsuNo("");//  主被保人编号
				insuListSchema.setName(form.getOwnerName());
				insuListSchema.setSex(insusex);
				insuListSchema.setBirthday(form.getBirthDate());
				insuListSchema.setIDType(codeChange(form.getCardType()));  //转变卡类型编码
				insuListSchema.setIDNo(form.getCardNo());
				insuListSchema
						.setOccupationCode("");//被保人的职业类别
				insuListSchema
						.setOccupationType("");
				insuListSchema
						.setPostalAddress("");
				insuListSchema.setZipCode("");
				insuListSchema.setPhont("");
				insuListSchema.setMobile(form.getMobile());
				insuListSchema.setEmail(form.getEmail());
				insuListSchema.setGrpName("");
				insuListSchema
						.setCompanyPhone("");
				insuListSchema.setCompAddr("");
				insuListSchema.setCompZipCode("");
				
				tInsuList.add(insuListSchema);
				tMMap.put(tInsuList, SysConst.INSERT);
				
				//在此处应该做个循环
				for(int j=0;j<form.getSN().length;j++){
					bnfListSchema = new WFBnfListSchema();
					// 受益人表(主键BatchNo,CardNo,ToInsuNo,BnfType,BnfGrade)
					bnfListSchema.setCardNo(cardNo.trim());
					bnfListSchema.setBatchNo(form.getTransID());
					bnfListSchema.setBnfGrade("1");//收益顺位
					bnfListSchema.setBnfType("1");//收益类别
					bnfListSchema.setToInsuNo("1"); //所属被保人(在多个被保人的情况下才能出现,暂时定为1)
					bnfListSchema.setToInsuRela(form.getBeneficiaryRelation()[j].trim());//所属被保人关系
					if(form.getBeneficiaryRate()[j].trim()!=null&&!form.getBeneficiaryRate()[j].trim().equals("")){
						bnfListSchema.setBnfGradeNo(form.getSN()[j].trim());
						bnfListSchema.setBnfRate(form.getBeneficiaryRate()[j]);
						bnfListSchema.setName(form.getBeneficiaryName()[j].trim());
						bnfListSchema.setSex("3");//受益人性别（非空）  报文中未提及性别 ，先默认为3
						bnfListSchema.setBirthday(changDate(form.getBeneficiaryBirthDate()[j].trim()));
						bnfListSchema.setIDType(codeChange(form.getBeneficiaryCardType()[j]).trim());   //卡类型编码转换
						bnfListSchema.setIDNo(form.getBeneficiaryCardNo()[j].trim());
						tBnfList.add(bnfListSchema);
					}else{
						System.out.println("报文中受益人部分为空");
					}
					
				}
				if(tBnfList.size()>0){
					
					tMMap.put(tBnfList, SysConst.INSERT);
				}
				
				// 卡单外部激活信息
				ExternalActive tExternalActive = new ExternalActive();
				exCardActiveInfoListSet = tExternalActive
						.writeLICardActiveInfoList(contListSchema, tInsuList);
				if(exCardActiveInfoListSet == null ){
					errinfo = tExternalActive.mErrors.getErrContent();
					return false;
				}
				// state In Lzcard 需要修改为14.
				String[] updateArr = tExternalActive.getUpdateLzcardSql(cardNo);
				String stateInLzcard = updateArr[1];
				System.out.println("卡号："+InsuranceNo[i]+"----stateInLzcard--------"+stateInLzcard);
				// 判断单证号是否有效
				if ("2".equals(stateInLzcard) || "10".equals(stateInLzcard)
						|| "11".equals(stateInLzcard)
						|| "13".equals(stateInLzcard)) {
					System.out.println("单证号有效.");
				} else {
					errinfo = "投保异常，单证号无效!";
					return false;
				}
				if ("11".equals(stateInLzcard) || "10".equals(stateInLzcard)) {
					System.out.println("state In Lzcard 需要修改为14.");
					String modifyStateSql = updateArr[0];
					tMMap.put(modifyStateSql, SysConst.UPDATE);
				}
				tMMap.put(exCardActiveInfoListSet, SysConst.INSERT);
				
				//发送短信和邮件所需信息
				mAndeMap.put("cardNo"+i, cardNo);
				if(i==length-1){
					mAndeMap.put("endindex", ""+i);
				}
			}
			mAndeMap.put("certifyName", certifyName);
			mAndeMap.put("mobile", form.getProposerMobile());
			mAndeMap.put("Cvalidate", changDate(form.getEffectDate().trim()));
			mAndeMap.put("orderID", form.getOrderID());
			mAndeMap.put("proposerEmail",form.getProposerEmail());
			mAndeMap.put("ProposerNeedSMS", form.getProposerNeedSMS());
			mAndeMap.put("ProposerName", form.getProposerName());
		} catch (Exception e) {
			System.out.println("处理卡激活信息异常");
			e.printStackTrace();
			errinfo = "投报异常，处理卡激活信息异常";
			return false;
		}

		return true;
	}

	// 提交卡激活信息
	private boolean save() {
		try {
			int length = InsuranceNo.length;
			for (int i = 0; i < length; i++) {
				String cardNo = InsuranceNo[i];
				String cheCardNoSQL = "select 1 from WFContList where cardno='"
						+ cardNo + "'";
				String cheCardNoRes = new ExeSQL().getOneValue(cheCardNoSQL);
				if (cheCardNoRes.equals("1")) {
					errinfo = "投保异常，cardNo不唯一";
					return false;
				}
//				lockCont(cardNo);
			}

			VData v = new VData();
			v.add(tMMap);
			PubSubmit p = new PubSubmit();
			if (!p.submitData(v, "")) {
				errinfo = "投保异常，数据提交失败!";
				return false;
			}
		} catch (Exception e) {
			System.out.println("投保异常，数据提交异常");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * 通知客户 短信和邮件
	 */
	private boolean noticeCustomer() {
		//此方法中只考虑为投保人发送短信和邮件
		boolean messageFlag = true;
		boolean emailFlag = true;
		errinfo = "";
		if(mAndeMap.get("ProposerNeedSMS").equals("T")){
			System.out.println(((String)mAndeMap.get("mobile")).length()+"----------mobile");
			//短息通知
			if(mAndeMap.get("mobile")!=null && !("").equals(mAndeMap.get("mobile"))){
				if(new EmailAndMessageBL().sendMsg(mAndeMap)){
					System.out.println("手机号为"+mAndeMap.get("mobile")+"的短息发送成功");
				}else{
					messageFlag = false;
					//errinfo = errinfo+"投保异常，短息发送失败  ";
					System.out.println("手机号为"+mAndeMap.get("mobile")+"的短息发送失败");
				}
			}
			
			//邮件通知
			if(mAndeMap.get("proposerEmail")!=null && !("").equals(mAndeMap.get("proposerEmail"))){
				if(EmailAndMessageBL.sendEmail(mAndeMap)){
//					System.out.println("邮箱号为"+mAndeMap.get("proposerEmail")+"的邮件发送成功");
				}else{
					//errinfo = errinfo+"投保异常，邮件发送失败";
					System.out.println("邮箱号为"+mAndeMap.get("proposerEmail")+"的邮件发送失败");
					emailFlag = false;
				}
				if(messageFlag&emailFlag){
					return true;
				}else{
					return false;
				}
			}
			
		}
		return true;
	}

	/**
	 * 锁定激活卡号。
	 * 
	 * @param cLCContSchema
	 * @return
	 */
	private MMap lockCont(String cardno) {
		MMap tMMap = null;

		// 签单锁定标志为：“SC”
		String tLockNoType = "WX";// 网销锁定
		// -----------------------

		// 锁定有效时间
		String tAIS = "300";
		// -----------------------

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LockNoKey", cardno);
		tTransferData.setNameAndValue("LockNoType", tLockNoType);
		tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

		VData tVData = new VData();
		tVData.add(mGlobalInput);
		tVData.add(tTransferData);

		LockTableActionBL tLockTableActionBL = new LockTableActionBL();
		tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
		if (tMMap == null) {
			mErrors.copyAllErrors(tLockTableActionBL.mErrors);
			return null;
		}

		return tMMap;
	}

	// 添加原发送报文内容
	private void addOldData(OMElement ApplyResponse, OMElement TransData) {
		// 添加主节点
		addOm(TransData, "TransID", form.getTransID()); // 出保交易流水号
		addOm(TransData, "MerchantNo", form.getMerchantNo()); // 商户号
		
		addOm(TransData, "OrderID", form.getOrderID()); // 携程订单号
		addOm(TransData, "ProductCode", form.getProductCode()); // 产品编号
		addOm(TransData, "EffectDate", form.getEffectDate()); // 生效日期
		addOm(TransData, "EffectTime", form.getEffectTime()); // 生效时间
		addOm(TransData, "ExpiryDate", form.getExpiryDate()); // 失效日期
		addOm(TransData, "ExpiryTime", form.getExpiryTime()); // 失效时间
		addOm(TransData, "OwnerName", form.getOwnerName()); // 被保人姓名
		addOm(TransData, "CardType", form.getCardType()); // 被保人证件类型
		addOm(TransData, "CardNo", form.getCardNo()); // 被保人证件号码
		addOm(TransData, "BirthDate", form.getBirthDate()); // 被保人出生日期
		addOm(TransData, "Mobile", form.getMobile()); // 被保人手机号
		addOm(TransData, "NeedSMS", form.getNeedSMS()); // 被保人是否短信
		addOm(TransData, "Email", form.getEmail()); // 被保人邮件
		addOm(TransData, "InsuranceNum", form.getInsuranceNum()); // 购买份数
		addOm(TransData, "FlightNo", form.getFlightNo()); // 航班号
		addOm(TransData, "DepartureCity", form.getDepartureCity()); // 出发城市
		addOm(TransData, "DepartureDate", form.getDepartureDate()); // 出发日期
		addOm(TransData, "DepartureTime", form.getDepartureTime()); // 出发时间
		addOm(TransData, "ArrivalCity", form.getArrivalCity()); // 到达城市
		addOm(TransData, "ArrivalDate", form.getArrivalDate()); // 到达日期
		addOm(TransData, "ArrivalTime", form.getArrivalTime()); // 到达时间
		addOm(TransData, "ProposerName", form.getProposerName()); // 投保人姓名
		addOm(TransData, "ProposerCardType", form.getProposerCardType()); // 投保人证件类型
		addOm(TransData, "ProposerCardNo", form.getProposerCardNo()); // 投保人证件号码
		addOm(TransData, "ProposerBirthDate", changDate(form.getProposerBirthDate().trim())); // 投保人出生日期
		addOm(TransData, "ProposerMobile", form.getProposerMobile()); // 投保人手机号
		addOm(TransData, "ProposerNeedSMS", form.getProposerNeedSMS()); // 投保人是否短信
		addOm(TransData, "ProposerEmail", form.getProposerEmail()); // 投保人邮件
		addOm(TransData, "ProposerRelation", form.getProposerRelation()); // 投保人与被保人关系
		addOm(TransData, "Extend1", form.getExtend1()); // 扩展信息
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement Beneficiarys = fac.createOMElement("Beneficiarys", null);
		OMElement Beneficiary = fac.createOMElement("Beneficiary", null);

		for (int i = 0; i < form.getSN().length; i++) {
			addOm(Beneficiary, "SN", form.getSN()[i].trim()); // 受益人顺序号
			addOm(Beneficiary, "BeneficiaryName", form.getBeneficiaryName()[i].trim()); // 受益人姓名
			addOm(Beneficiary, "BeneficiaryCardType", form
					.getBeneficiaryCardType()[i].trim()); // 受益人证件类型
			addOm(Beneficiary, "BeneficiaryCardNo",
					form.getBeneficiaryCardNo()[i].trim()); // 受益人证件号码
			addOm(Beneficiary, "BeneficiaryBirthDate", form
					.getBeneficiaryBirthDate()[i].trim()); // 受益人出生日期
			addOm(Beneficiary, "BeneficiaryMobile",
					form.getBeneficiaryMobile()[i].trim()); // 受益人手机号
			addOm(Beneficiary, "BeneficiaryNeedSMS", form
					.getBeneficiaryNeedSMS()[i].trim()); // 受益人是否短信
			addOm(Beneficiary, "BeneficiaryEmail",
					form.getBeneficiaryEmail()[i].trim()); // 受益人邮件
			addOm(Beneficiary, "BeneficiaryRelation", form
					.getBeneficiaryRelation()[i].trim()); // 受益人与被保人关系
			addOm(Beneficiary, "BeneficiaryRate", form.getBeneficiaryRate()[i]); // 受益比例
		}
		Beneficiarys.addChild(Beneficiary);
		TransData.addChild(Beneficiarys);
		// 添加签名节点
		addOm(ApplyResponse, "TransSignature", form.getTransSignature());

	}

	private String buildResponseOME(String ResultStatus, String ResultMsg,
			String[] InsuranceNo,String repeatFlag) {
		String message = "";
		OMFactory fac = OMAbstractFactory.getOMFactory();

		OMElement ApplyResponse = fac.createOMElement("ApplyResponse", null);

		OMElement TransData = fac.createOMElement("TransData", null);
		
	
		// 原样返回发送报文
		addOldData(ApplyResponse, TransData);
		// OMNamespace omNs = null;
		// ApplyResponse.addAttribute("type", "REQUEST", omNs);
		// ApplyResponse.addAttribute("version", "1.0", omNs);
		if(repeatFlag.equals("true")){
			addOm(TransData, "TransDate", repeatTransDate); // 出保日期
			addOm(TransData, "TransTime", repeatTransTime); // 出保时间
		}else{
			addOm(TransData, "TransDate", form.getTransDate()); // 出保日期
			addOm(TransData, "TransTime", form.getTransTime()); // 出保时间
		}
		addOm(TransData, "ResultStatus", ResultStatus);
		addOm(TransData, "ResultMsg", ResultMsg);
		if(InsuranceNo!=null){
			if(InsuranceNo.length!=0){
				OMElement Insurances = fac.createOMElement("Insurances", null);
				for (int i = 0; i < InsuranceNo.length; i++) {
					addOm(Insurances, "InsuranceNo", InsuranceNo[i]);
					String contno = Des3.encode("piccandthirdpart",InsuranceNo[i]);
					String hrlString = "http://59.108.65.220:8088/ec/newSales/history/thirdCheckPDF.jsp?contno="+contno;
					addOm(Insurances, "EPolicyInfo", hrlString);
				}
				TransData.addChild(Insurances);
			}
		}
		ApplyResponse.addChild(TransData);
		message = "<?xml version=\"1.0\" encoding=\"GBK\"?>"+ApplyResponse.toString();
		return message;
	}

	private static OMElement addOm(OMElement Om, String Name, String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName);
		return Om;
	}
	private String codeChange(String code){
		if(code.equals("01")){
			code = "0";
		}else if(code.equals("02")){
			code = "1";
		}else if(code.equals("04")){
			code = "2";
		}else if(code.equals("25")){
			code = "5";
		}else{
			code = "4";
		}
		return code;
	}
//	//处理后的记录保存到数据库中（使用LoginVerifyTool中的方法）
    private void writeLog(String batchNo,
			String messageType, String sendDate, String sendTime,
			String batchFlag, String opertator, String errCode, String errInfo) {
		MMap map = new MMap();
		VData tVData = new VData();
		// 错误处理类
		CErrors mErrors = new CErrors();
		WFTransLogSchema trransLogSchema = new WFTransLogSchema();
		trransLogSchema.setBatchNo(batchNo);
		trransLogSchema.setMessageType(messageType);
		trransLogSchema.setSendDate(sendDate);
		trransLogSchema.setSendTime(sendTime);
		trransLogSchema.setBatchFlag(batchFlag);
		trransLogSchema.setopertator(opertator);
		trransLogSchema.setErrCode(errCode);
		trransLogSchema.setErrInfo(errInfo);

		map.put(trransLogSchema, "INSERT");

		// 入库
		PubSubmit tPubSubmit = new PubSubmit();
		tVData.add(map);
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			// @@错误处理
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LoginVerifyTool";
			tError.functionName = "writeLog";
			tError.errorMessage = "数据提交失败!";
			mErrors.addOneError(tError);

		}
	}
	/**
	 * 生成批次号
	 */
	private static String  makeBatchNO(){
		//Calendar nowTime=Calendar.getInstance();
		//System.out.println(nowTime);
		//return nowTime.getTimeInMillis()+"";
		Calendar CD = Calendar.getInstance();
		int YY = CD.get(Calendar.YEAR);
		int MM = CD.get(Calendar.MONTH)+1;
		int DD = CD.get(Calendar.DATE);
		int HH = CD.get(Calendar.HOUR_OF_DAY);
		int NN = CD.get(Calendar.MINUTE);
		int SS = CD.get(Calendar.SECOND);
		int MI = CD.get(Calendar.MILLISECOND);
		return ""+YY+MM+DD+HH+NN+SS+MI+genRandomNum(4);
	}
	/**
     * 生成随即密码
     * @param pwd_len 生成的密码的总长度
     * @return  密码的字符串
     */
    private static String genRandomNum(int pwd_len) {
        //35是因为数组是从0开始的，26个字母+10个数字
//        final int maxNum = 100;
        int i; //生成的随机数
        int count = 0; //生成的密码的长度
        char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        StringBuffer pwd = new StringBuffer("");
//        Random r = new Random();
        while (count < pwd_len) 
        {
            //生成随机数，取绝对值，防止生成负数，
//            i = Math.abs(r.nextInt(maxNum)); //生成的数最大为36-1
        	i = (int)(Math.random()*10);
            if (i >= 0 && i < str.length) 
            {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }
    /**
     * 日期格式转换
     * @param args
     */
    public String changDate(String s){
    	StringBuffer sf = new StringBuffer();
		sf.append(s.substring(0, 4));
		sf.append("-");
		sf.append(s.substring(4, 6));
		sf.append("-");
		sf.append(s.substring(6, 8));
    	return sf.toString();
    }
    /**
     * 时间格式转换
     * @param args
     */
    public String changTime(String s){
    	StringBuffer sf = new StringBuffer();
		sf.append(s.substring(0, 2));
		sf.append(":");
		sf.append(s.substring(2, 4));
		sf.append(":");
		sf.append(s.substring(4, 6));
    	return sf.toString();
    }
    /**
     * 日期格式再次转换
     * @param s
     * @return
     */
    public String rechangeDate(String s){
    	StringBuffer sf = new StringBuffer();
    	String[] ss = s.split("-");
    	for(int i=0;i<ss.length;i++){
    		sf.append(ss[i]);
    	}
    	return sf.toString();
    }
    /**
     * 时间格式再次转换
     * @param args
     */
    public String rechangeTime(String s){
    	StringBuffer sf = new StringBuffer();
    	String[] ss = s.split(":");
    	for(int i=0;i<ss.length;i++){
    		sf.append(ss[i]);
    	}
    	return sf.toString();
    }
	public static void main(String[] args) {
		ContApply cc = new ContApply();
		try {
			String mInFilePath = "D:\\101.xml";

			InputStream mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = IOTrans.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "gbk");
			cc.Insure(mInXmlStr);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}
}
