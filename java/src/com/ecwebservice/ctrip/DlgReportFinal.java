package com.ecwebservice.ctrip;

import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Alignment;
import jxl.write.Border;
import jxl.write.BorderLineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.VerticalAlignment;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class DlgReportFinal {
	private String info;
	private String fileName;
	private String fPayDate;
	private String tPayDate;
	private String appntName;
	private String contNo;
	private String idType;
	private String idNo;
	private String startDate;
	private String endDate;
	private String cvalistartDate;
	private String cvaliendDate;
	private String cardStatus;
	private String cardHeading;
	private String bstartDate;
	private String bendDate;
	SSRS tSsrs = new SSRS();
	public OMElement queryData(OMElement oms){
		boolean flag;
		OMElement responseOME = null;
		//		解析报文
		flag = paraseXML(oms);
		if(!flag){
			System.out.println(info);
			
			return null;
		}
//		获取所需信息
		flag = getInfoFromDB();
		if(!flag){
			System.out.println(info);
			return null;
		}
		//下载清单
		downloadReport(fileName, tSsrs);
		responseOME = buildResponseOME();
		return responseOME;
	}
	private boolean paraseXML(OMElement oms){
		System.out.println("接收的报文为："+oms);
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement Om = (OMElement)iterator.next();
				if(Om.getLocalName().equals("DLGREPORT")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("APPNTNAME")){
							appntName = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("FILENAME")){
							fileName = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("FPAYDATE")){
							fPayDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("TPAYDATE")){
							tPayDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CONTNO")){
							contNo = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("IDTYPE")){
							idType = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("IDNO")){
							idNo = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("STARTDATE")){
							startDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("ENDDATE")){
							endDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CVALISTARTDATE")){
							cvalistartDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CVALIENDDATE")){
							cvaliendDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("BSTARTDATE")){
							bstartDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("BENDDATE")){
							bendDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CARDHEADING")){
							cardHeading = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CARDSTATUS")){
							cardStatus = child_Element.getText().trim();
							
						}
						
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			info = "获取报文中的四种保单号字符串失败";
			return false;
		}
		return true;
	}
//	查询所需信息
	private boolean getInfoFromDB(){
		if(!fPayDate.equals("")&&fPayDate!=null){
			startDate = fPayDate;
		}
		if(!tPayDate.equals("")&&tPayDate!=null){
			endDate = tPayDate;
		}
		try {
			
//			1.根据appntName，idtype,idno查wfappntlist
			String infoSQL = "select li.cardno,app.name,app.idno,app.birthday,li.prem,li.activedate,li.cvalidate,(select distinct(from) from wfcontlist cont where cont.cardno = li.cardno),cont.messagetype from "+ 
							"WFAPPNTLIST app "+
							"inner join LICARDACTIVEINFOLIST li on app.cardno = li.cardno "+
							"inner join WFCONTLIST cont on app.cardno = cont.cardno "+
							"where 1=1 and cont.branchcode='DLG' ";
			
//			String countSQL = "select count(1) from "+ 
//			"WFCONTLIST cont inner join WFAPPNTLIST app on cont.cardno = app.cardno "+
//			"inner join LICARDACTIVEINFOLIST li on cont.cardno = li.cardno "+
//			"where 1=1 ";
			if(cardHeading!=null&&!("").equals(cardHeading)){
				String[] cardHeadings = cardHeading.split("@");
				int length = cardHeadings.length;
				infoSQL = infoSQL + "and (";
				for(int i=0;i<length;i++){
					if(i == length -1){
						infoSQL = infoSQL + "cont.cardno like '"+cardHeadings[i]+"%' ) ";
						break;
					}
					infoSQL = infoSQL + "cont.cardno like '"+cardHeadings[i]+"%' or ";
				}
				
			}
			if(appntName!=null&&!("").equals(appntName)){
				infoSQL = infoSQL + "and app.Name = '"+appntName+"' ";
//				countSQL = countSQL + "and app.appntName = '"+appntName+"' ";
			}
			if(contNo!=null&&!("").equals(contNo)){
				infoSQL = infoSQL +"and li.cardno = '"+contNo+"' ";
//				countSQL = countSQL +"and cont.cardno = '"+contNo+"' ";
			}
			if(idType!=null&&!("").equals(idType)){
				infoSQL = infoSQL + "and app.idtype = '"+idType+"' ";
//				countSQL = countSQL + "and app.idtype = '"+idType+"' ";
			}
			if(idNo!=null&&!("").equals(idNo)){
				infoSQL = infoSQL + "and app.idNo = '"+idNo+"' ";
//				countSQL = countSQL + "and app.idNo = '"+idNo+"' ";
			}
			if(cardStatus!=null&&!("").equals(cardStatus)){
				infoSQL = infoSQL + "and li.cardStatus = '"+cardStatus+"' ";
//				countSQL = countSQL + "and li.cardStatus = '"+cardStatus+"' ";
			}
			if(startDate!=null&&!("").equals(startDate)&&endDate!=null&&!("").equals(endDate)){
				infoSQL = infoSQL +"and cont.senddate >= '"+startDate+"' and cont.senddate <= '"+endDate+"'";
			}
			if(cvalistartDate!=null&&!("").equals(cvalistartDate)&&cvaliendDate!=null&&!("").equals(cvaliendDate)){
				infoSQL = infoSQL + "and li.cvalidate >= '"+cvalistartDate+"' and li.cvalidate<='"+cvaliendDate+"' ";
//				countSQL = countSQL + "and cont.sendDate >= '"+startDate+"' and cont.sendDate<="+endDate+"' ";
			}
			if(bstartDate!=null&&!("").equals(bstartDate)&&bendDate!=null&&!("").equals(bendDate)){
				infoSQL = infoSQL +"and li.cvalidate + 30 day >= '"+bstartDate+"' and li.cvalidate + 30 day <= '"+bendDate+"'";
			}
			System.out.println("infoSQL:  "+infoSQL);
//			System.out.println("countSQL: "+countSQL);
			ExeSQL exeSQL = new ExeSQL();
			tSsrs = exeSQL.execSQL(infoSQL);
//			totalcount = exeSQL.getOneValue(countSQL);
			
			if(tSsrs == null || tSsrs.getMaxRow() == 0 ){
				info = "没有符合条件的数据";
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			info = "查询携程相关信息失败";
			return false;
		}
		return true;
	}
	public void downloadReport(String filename,SSRS tSsrs){
		try {
			FileOutputStream os =new FileOutputStream(getProjectLocalPath()+"/temp/padapp/"+filename +".xls");
//			WritableWorkbook wwb = Workbook.createWorkbook(new FileOutputStream(getProjectLocalPath()+"/temp/padapp/"+filename +".xls"));
			WritableWorkbook wwb = Workbook.createWorkbook(os);
			WritableFont headColor = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK); 
			WritableFont headFont = new WritableFont(headColor);
			WritableCellFormat headFormat = new WritableCellFormat(headFont);
			headFormat.setBorder(Border.ALL, BorderLineStyle.THIN); // 线条
			headFormat.setVerticalAlignment(VerticalAlignment.CENTRE); // 垂直对齐
			headFormat.setAlignment(Alignment.LEFT);// 水平对齐
			headFormat.setWrap(true); // 是否换行
			
			WritableFont  normalFont = new WritableFont(WritableFont.createFont("宋体"), 10);
			WritableCellFormat normalFormat = new WritableCellFormat(normalFont);
			normalFormat.setBorder(Border.ALL, BorderLineStyle.THIN); //线条
			normalFormat.setVerticalAlignment(VerticalAlignment.CENTRE); //垂直对齐
			normalFormat.setAlignment(Alignment.CENTRE);// 水平对齐
			normalFormat.setWrap(true); // 是否换行
			WritableSheet ws = wwb.createSheet("携程业务对账报表", 0);//创建Excel工作表
			
			//ws.mergeCells(0, 0, 16, 0);
			ws.setColumnView(0, 20);
			ws.addCell(new Label(0, 0, "保单号", normalFormat));
			ws.setColumnView(1, 25);
			ws.addCell(new Label(1, 0, "投保人姓名", normalFormat));
			ws.setColumnView(2, 20);
			ws.addCell(new Label(2, 0, "投保人证件号码", normalFormat));
			ws.setColumnView(3, 15);
			ws.addCell(new Label(3, 0, "投保人出生日期", normalFormat));
			ws.addCell(new Label(4, 0, "保费", normalFormat));
			ws.addCell(new Label(5, 0, "交易日期", normalFormat));
			ws.addCell(new Label(6, 0, "生效日期", normalFormat));
			ws.setColumnView(7, 15);
			ws.addCell(new Label(7, 0, "始发城市", normalFormat));
			ws.addCell(new Label(8, 0, "保单状态", normalFormat));
			
			
			String[][] data = getData(tSsrs);
			if(data != null){
				for(int i = 0; i < data.length; i++){
					for(int j = 0; j < data[i].length; j++){
						ws.addCell(new Label(j, 1+i, data[i][j], normalFormat));
					}
				}
			}
			wwb.write();
			wwb.close();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取对账报表数据
	 * @return
	 */
	
	public String[][] getData(SSRS tSsrs){
		try {
			int length = tSsrs.getMaxRow();
			String[][] values = new String[length][9];
//			NumberFormat format = new DecimalFormat("####0.00");
			for(int i = 0; i < length; i++){
				values[i][0] = tSsrs.GetText(i+1, 1);//订单号
				values[i][1] = tSsrs.GetText(i+1, 2);
				values[i][2] = tSsrs.GetText(i+1, 3);
				values[i][3] = tSsrs.GetText(i+1, 4);
				values[i][4] = tSsrs.GetText(i+1, 5);
				values[i][5] = tSsrs.GetText(i+1, 6);
				values[i][6] = tSsrs.GetText(i+1, 7);
				values[i][7] = tSsrs.GetText(i+1, 8);
				values[i][8] = "";
				if(tSsrs.GetText(i+1, 9).equals("01")){
					values[i][8] = "P";
				}
				if(tSsrs.GetText(i+1, 9).equals("02")){
					values[i][8] = "T";
				}	
				
			}
			return values;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 获取项目所在路径
	 * @return				项目路径
	 * @throws Exception	未找到路径
	 */
	private String getProjectLocalPath() {
		String path = DlgReportFinal.class.getResource("").getFile();
		try {
			path = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		path = path.substring(0,path.lastIndexOf("/WEB-INF"));
		String temp = path.substring(0, 5);
		if("file:".equalsIgnoreCase(temp)){
			path = path.substring(5);
		}
		return path;
	}
	
//	返回问题报文
	private OMElement buildResponseOME(){
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement ctripReport = factory.createOMElement("CTRIPREPORT",null);
		addOm(ctripReport, "SUCCESS", "success");
		root.addChild(ctripReport);
		return root;
	}
	private static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}
	
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
	public static void main(String[] args) {
		DlgReportFinal report = new DlgReportFinal();
		String path = report.getProjectLocalPath();
		System.out.println("path:"+path);
		
	}
}
