package com.ecwebservice.ctrip.form;

public class ApplyForm {
	private String TransID;   //出保交易流水号
	private String MerchantNo;   //商户号
	private String TransDate;   //出保日期
	private String TransTime;   //出保时间
	private String OrderID;   //携程订单号
	private String ProductCode;   //产品编号
	private String EffectDate;   //生效日期		
	private String EffectTime;   //生效时间
	private String ExpiryDate;   //失效日期
	private String ExpiryTime;   //失效时间
	private String OwnerName;   //被保人姓名			
	private String CardType;   //被保人证件类型	
	private String CardNo;   //被保人证件号码
	private String BirthDate;   //被保人出生日期	
	private String Mobile;   //被保人手机号
	private String NeedSMS;   //被保人是否短信
	private String Email;   //被保人邮件
	private String InsuranceNum;   //购买份数
	private String FlightNo;   //航班号
	private String DepartureCity;   //出发城市
	private String DepartureDate;   //出发日期
	private String DepartureTime;   //出发时间
	private String ArrivalCity;   //到达城市
	private String ArrivalDate;   //到达日期
	private String ArrivalTime;   //到达时间
	private String ProposerName;   //投保人姓名
	private String ProposerCardType;   //投保人证件类型
	private String ProposerCardNo;   //投保人证件号码
	private String ProposerBirthDate;   //投保人出生日期
	private String ProposerMobile;   //投保人手机号
	private String ProposerNeedSMS;   //投保人是否短信
	private String ProposerEmail;   //投保人邮件
	private String ProposerRelation;   //投保人与被保人关系
	private String Extend1;   //扩展信息
	private String[] SN;   //受益人顺序号
	private String[] BeneficiaryName;   //受益人姓名
	private String[] BeneficiaryCardType;   //受益人证件类型
	private String[] BeneficiaryCardNo;   //受益人证件号码
	private String[] BeneficiaryBirthDate;   //受益人出生日期
	private String[] BeneficiaryMobile;   //受益人手机号				
	private String[] BeneficiaryNeedSMS;   //受益人是否短信	
	private String[] BeneficiaryEmail;   //受益人邮件
	private String[] BeneficiaryRelation;   //受益人与被保人关系
	private String[] BeneficiaryRate;   //受益比例
	private String TransSignature;			//交易签名
	public String getArrivalCity() {
		return ArrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		ArrivalCity = arrivalCity;
	}
	public String getArrivalDate() {
		return ArrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		ArrivalDate = arrivalDate;
	}
	public String getArrivalTime() {
		return ArrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		ArrivalTime = arrivalTime;
	}
	public String getBirthDate() {
		return BirthDate;
	}
	public void setBirthDate(String birthDate) {
		BirthDate = birthDate;
	}
	public String getCardNo() {
		return CardNo;
	}
	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}
	public String getCardType() {
		return CardType;
	}
	public void setCardType(String cardType) {
		CardType = cardType;
	}
	public String getDepartureCity() {
		return DepartureCity;
	}
	public void setDepartureCity(String departureCity) {
		DepartureCity = departureCity;
	}
	public String getDepartureDate() {
		return DepartureDate;
	}
	public void setDepartureDate(String departureDate) {
		DepartureDate = departureDate;
	}
	public String getDepartureTime() {
		return DepartureTime;
	}
	public void setDepartureTime(String departureTime) {
		DepartureTime = departureTime;
	}
	public String getEffectDate() {
		return EffectDate;
	}
	public void setEffectDate(String effectDate) {
		EffectDate = effectDate;
	}
	public String getEffectTime() {
		return EffectTime;
	}
	public void setEffectTime(String effectTime) {
		EffectTime = effectTime;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getExpiryDate() {
		return ExpiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		ExpiryDate = expiryDate;
	}
	public String getExpiryTime() {
		return ExpiryTime;
	}
	public void setExpiryTime(String expiryTime) {
		ExpiryTime = expiryTime;
	}
	public String getExtend1() {
		return Extend1;
	}
	public void setExtend1(String extend1) {
		Extend1 = extend1;
	}
	public String getFlightNo() {
		return FlightNo;
	}
	public void setFlightNo(String flightNo) {
		FlightNo = flightNo;
	}
	public String getInsuranceNum() {
		return InsuranceNum;
	}
	public void setInsuranceNum(String insuranceNum) {
		InsuranceNum = insuranceNum;
	}
	public String getMerchantNo() {
		return MerchantNo;
	}
	public void setMerchantNo(String merchantNo) {
		MerchantNo = merchantNo;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getNeedSMS() {
		return NeedSMS;
	}
	public void setNeedSMS(String needSMS) {
		NeedSMS = needSMS;
	}
	public String getOrderID() {
		return OrderID;
	}
	public void setOrderID(String orderID) {
		OrderID = orderID;
	}
	public String getOwnerName() {
		return OwnerName;
	}
	public void setOwnerName(String ownerName) {
		OwnerName = ownerName;
	}
	public String getProductCode() {
		return ProductCode;
	}
	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}
	public String getProposerBirthDate() {
		return ProposerBirthDate;
	}
	public void setProposerBirthDate(String proposerBirthDate) {
		ProposerBirthDate = proposerBirthDate;
	}
	public String getProposerCardNo() {
		return ProposerCardNo;
	}
	public void setProposerCardNo(String proposerCardNo) {
		ProposerCardNo = proposerCardNo;
	}
	public String getProposerCardType() {
		return ProposerCardType;
	}
	public void setProposerCardType(String proposerCardType) {
		ProposerCardType = proposerCardType;
	}
	public String getProposerEmail() {
		return ProposerEmail;
	}
	public void setProposerEmail(String proposerEmail) {
		ProposerEmail = proposerEmail;
	}
	public String getProposerMobile() {
		return ProposerMobile;
	}
	public void setProposerMobile(String proposerMobile) {
		ProposerMobile = proposerMobile;
	}
	public String getProposerName() {
		return ProposerName;
	}
	public void setProposerName(String proposerName) {
		ProposerName = proposerName;
	}
	public String getProposerNeedSMS() {
		return ProposerNeedSMS;
	}
	public void setProposerNeedSMS(String proposerNeedSMS) {
		ProposerNeedSMS = proposerNeedSMS;
	}
	public String getProposerRelation() {
		return ProposerRelation;
	}
	public void setProposerRelation(String proposerRelation) {
		ProposerRelation = proposerRelation;
	}
	public String getTransDate() {
		return TransDate;
	}
	public void setTransDate(String transDate) {
		TransDate = transDate;
	}
	public String getTransID() {
		return TransID;
	}
	public void setTransID(String transID) {
		TransID = transID;
	}
	public String getTransTime() {
		return TransTime;
	}
	public void setTransTime(String transTime) {
		TransTime = transTime;
	}
	public String[] getBeneficiaryBirthDate() {
		return BeneficiaryBirthDate;
	}
	public void setBeneficiaryBirthDate(String[] beneficiaryBirthDate) {
		BeneficiaryBirthDate = beneficiaryBirthDate;
	}
	public String[] getBeneficiaryCardNo() {
		return BeneficiaryCardNo;
	}
	public void setBeneficiaryCardNo(String[] beneficiaryCardNo) {
		BeneficiaryCardNo = beneficiaryCardNo;
	}
	public String[] getBeneficiaryCardType() {
		return BeneficiaryCardType;
	}
	public void setBeneficiaryCardType(String[] beneficiaryCardType) {
		BeneficiaryCardType = beneficiaryCardType;
	}
	public String[] getBeneficiaryEmail() {
		return BeneficiaryEmail;
	}
	public void setBeneficiaryEmail(String[] beneficiaryEmail) {
		BeneficiaryEmail = beneficiaryEmail;
	}
	public String[] getBeneficiaryMobile() {
		return BeneficiaryMobile;
	}
	public void setBeneficiaryMobile(String[] beneficiaryMobile) {
		BeneficiaryMobile = beneficiaryMobile;
	}
	public String[] getBeneficiaryName() {
		return BeneficiaryName;
	}
	public void setBeneficiaryName(String[] beneficiaryName) {
		BeneficiaryName = beneficiaryName;
	}
	public String[] getBeneficiaryNeedSMS() {
		return BeneficiaryNeedSMS;
	}
	public void setBeneficiaryNeedSMS(String[] beneficiaryNeedSMS) {
		BeneficiaryNeedSMS = beneficiaryNeedSMS;
	}
	public String[] getBeneficiaryRate() {
		return BeneficiaryRate;
	}
	public void setBeneficiaryRate(String[] beneficiaryRate) {
		BeneficiaryRate = beneficiaryRate;
	}
	public String[] getBeneficiaryRelation() {
		return BeneficiaryRelation;
	}
	public void setBeneficiaryRelation(String[] beneficiaryRelation) {
		BeneficiaryRelation = beneficiaryRelation;
	}
	public String[] getSN() {
		return SN;
	}
	public void setSN(String[] sn) {
		SN = sn;
	}
	public String getTransSignature() {
		return TransSignature;
	}
	public void setTransSignature(String transSignature) {
		TransSignature = transSignature;
	}
}
