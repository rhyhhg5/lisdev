package com.ecwebservice.ctrip.form;
/*
 * 退保接口（客户定义）
 */
public class CancelForm {
	private String TransID;//交易流水号
	private String ApprovalTransID;//出保交易流水号
	private String MerchantNo;//商户号
	private String OriginalInsurances;
	private String[] OriginalInsuranceNo;//原保单号
	private String TransDate;//退保日期
	private String TransTime;//退保时间
	private String Extend1;//扩展字段
	private String TransSignature;//交易签名
	
	
	public String getOriginalInsurances() {
		return OriginalInsurances;
	}
	public void setOriginalInsurances(String originalInsurances) {
		OriginalInsurances = originalInsurances;
	}
	public String getApprovalTransID() {
		return ApprovalTransID;
	}
	public void setApprovalTransID(String approvalTransID) {
		ApprovalTransID = approvalTransID;
	}
	public String getExtend1() {
		return Extend1;
	}
	public void setExtend1(String extend1) {
		Extend1 = extend1;
	}
	public String getMerchantNo() {
		return MerchantNo;
	}
	public void setMerchantNo(String merchantNo) {
		MerchantNo = merchantNo;
	}
	public String[] getOriginalInsuranceNo() {
		return OriginalInsuranceNo;
	}
	public void setOriginalInsuranceNo(String[] originalInsuranceNo) {
		OriginalInsuranceNo = originalInsuranceNo;
	}
	public String getTransDate() {
		return TransDate;
	}
	public void setTransDate(String transDate) {
		TransDate = transDate;
	}
	public String getTransID() {
		return TransID;
	}
	public void setTransID(String transID) {
		TransID = transID;
	}
	public String getTransSignature() {
		return TransSignature;
	}
	public void setTransSignature(String transSignature) {
		TransSignature = transSignature;
	}
	public String getTransTime() {
		return TransTime;
	}
	public void setTransTime(String transTime) {
		TransTime = transTime;
	}
	
}
