package com.ecwebservice.ctrip.form;

public class AccountForm {
	private String PolicyNo; //保单号
	private String ProductCode;  //保险类别，产品编码
	private String InsuranceNum;  //该保单包含份数（其实都是1份）
	private String InsuranceType;  //出保，退保标识符
	private String Premium;        //保费(8位整数，两位小数)
	private String EffecTime;		//生效时间（yyyy-MM-dd HH:mm:ss）
	private String ExpiryTime;		//失效时间（yyyy-MM-dd HH:mm:ss）
	private String TransTime;       //交易时间
	private String CardNo;          //被保人证件号码
	private String Name;            //被保人姓名
	private String PolicyDays;      //被保天数
	public String getCardNo() {
		return CardNo;
	}
	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}
	public String getEffecTime() {
		return EffecTime;
	}
	public void setEffecTime(String effecTime) {
		EffecTime = effecTime;
	}
	public String getExpiryTime() {
		return ExpiryTime;
	}
	public void setExpiryTime(String expiryTime) {
		ExpiryTime = expiryTime;
	}
	public String getInsuranceNum() {
		return InsuranceNum;
	}
	public void setInsuranceNum(String insuranceNum) {
		InsuranceNum = insuranceNum;
	}
	public String getInsuranceType() {
		return InsuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		InsuranceType = insuranceType;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getPolicyDays() {
		return PolicyDays;
	}
	public void setPolicyDays(String policyDays) {
		PolicyDays = policyDays;
	}
	public String getPolicyNo() {
		return PolicyNo;
	}
	public void setPolicyNo(String policyNo) {
		PolicyNo = policyNo;
	}
	public String getPremium() {
		return Premium;
	}
	public void setPremium(String premium) {
		Premium = premium;
	}
	public String getProductCode() {
		return ProductCode;
	}
	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}
	public String getTransTime() {
		return TransTime;
	}
	public void setTransTime(String transTime) {
		TransTime = transTime;
	}
	
}
