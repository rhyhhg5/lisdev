package com.ecwebservice.ctrip;

import java.util.Iterator;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.ecwebservice.querytools.QueryTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * 携程保单查询
 * @author Administrator
 *
 */
public class ContStateSearch {
	private String plat;
	private String info;
	private String appntName;
	private String contNo;
	private String idType;
	private String idNo;
	private String startDate;
	private String endDate;
	private String cardStatus;
	private String cvalistartDate;
	private String cvaliendDate;
	private String cardHeading;
	private String pageIndex;
	private String pageSize;
	private String totalcount;
	SSRS tSsrs = new SSRS();
	
	public OMElement queryData(OMElement oms){
		boolean flag;
		OMElement responseOME = null;
		//解析报文
		flag = paraseXML(oms);
		if(!flag){
			System.out.println(info);
			responseOME = buildErrorOME(info);
			return responseOME;
		}
		//获取所需信息
		flag = getInfoFromDB();
		if(!flag){
			System.out.println(info);
			responseOME = buildErrorOME(info);
			return responseOME;
		}
		responseOME = buildResponseOME(tSsrs);
		return responseOME;
	}
	private boolean paraseXML(OMElement oms){
		System.out.println("接收的报文为："+oms);
		try {
			Iterator iterator = oms.getChildren();
			while(iterator.hasNext()){
				OMElement Om = (OMElement)iterator.next();
				if(Om.getLocalName().equals("CONTSTATESEARCH")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_Element = (OMElement)child.next();
						if(child_Element.getLocalName().equals("PLAT")){
							plat = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("APPNTNAME")){
							appntName = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CONTNO")){
							contNo = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("IDTYPE")){
							idType = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("IDNO")){
							idNo = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("STARTDATE")){
							startDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("ENDDATE")){
							endDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CVALISTARTDATE")){
							cvalistartDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CVALIENDDATE")){
							cvaliendDate = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("CARDSTATUS")){
							cardStatus = child_Element.getText().trim();
							System.out.println(cardStatus+"cardStatus");
						}
						if(child_Element.getLocalName().equals("CARDHEADING")){
							cardHeading = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("PAGEINDEX")){
							pageIndex = child_Element.getText().trim();
						}
						if(child_Element.getLocalName().equals("PAGESIZE")){
							pageSize = child_Element.getText().trim();
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			info = "获取报文中的四种保单号字符串失败";
			return false;
		}
		return true;
	}
	//查询所需信息
	private boolean getInfoFromDB(){
		QueryTool qTool = new QueryTool();
		String infoSQL = "";
		String countSQL = "";
		try {
			if(!"ctrip".equals(plat)){
//				1.根据appntName，idtype,idno查wfappntlist
				infoSQL = "select distinct li.cardno,cont.certifycode,ld.riskwrapcode,ld.wrapname,app.name,app.idno,app.birthday,cont.senddate,li.cvalidate,li.inactivedate,li.prem,li.CARDSTATUS,(select distinct(from) from wfcontlist cont where cont.cardno = li.cardno) from "+ 
								"WFAPPNTLIST app "+
								"inner join LICARDACTIVEINFOLIST li on app.cardno = li.cardno "+
								"inner join WFCONTLIST cont on app.cardno = cont.cardno "+
								"inner join lmcardrisk lr on cont.certifycode=lr.certifycode"+
								" inner join ldwrap ld on ld.riskwrapcode=lr.riskcode "+
								"where 1=1 ";
				
				countSQL = "select count(1) from "+ 
				"WFAPPNTLIST app "+
				"inner join LICARDACTIVEINFOLIST li on app.cardno = li.cardno "+
				"inner join WFCONTLIST cont on app.cardno = cont.cardno "+
				"inner join lmcardrisk lr on cont.certifycode=lr.certifycode"+
				" inner join ldwrap ld on ld.riskwrapcode=lr.riskcode "+
				"where 1=1 ";
				System.out.println("cardHeading:"+cardHeading);
				if(cardHeading!=null&&!("").equals(cardHeading)){
					String[] cardHeadings = cardHeading.split("@");
					int length = cardHeadings.length;
					infoSQL = infoSQL + "and (";
					countSQL = countSQL + "and (";
					for(int i=0;i<length;i++){
						if(i == length -1){
							infoSQL = infoSQL + "cont.cardno like '"+cardHeadings[i]+"%' ) ";
							countSQL = countSQL + "cont.cardno like '"+cardHeadings[i]+"%' ) ";
							break;
						}
						infoSQL = infoSQL + "cont.cardno like '"+cardHeadings[i]+"%' or ";
						countSQL = countSQL + "cont.cardno like '"+cardHeadings[i]+"%' or ";
					}
					
				}
				if(plat!=null&&!("").equals(plat)){
					infoSQL = infoSQL + "and cont.branchCode = '"+plat+"' ";
					countSQL = countSQL + "and cont.branchCode = '"+plat+"' ";
				}
				if(appntName!=null&&!("").equals(appntName)){
					infoSQL = infoSQL + "and app.Name = '"+appntName+"' ";
					countSQL = countSQL + "and app.Name = '"+appntName+"' ";
				}
				if(contNo!=null&&!("").equals(contNo)){
					infoSQL = infoSQL +"and li.cardno = '"+contNo+"' ";
					countSQL = countSQL +"and li.cardno = '"+contNo+"' ";
				}
				if(idType!=null&&!("").equals(idType)){
					infoSQL = infoSQL + "and app.idtype = '"+idType+"' ";
					countSQL = countSQL + "and app.idtype = '"+idType+"' ";
				}
				if(idNo!=null&&!("").equals(idNo)){
					infoSQL = infoSQL + "and app.idNo = '"+idNo+"' ";
					countSQL = countSQL + "and app.idNo = '"+idNo+"' ";
				}
				if(cardStatus!=null&&!("").equals(cardStatus)){
					infoSQL = infoSQL + "and li.cardStatus = '"+cardStatus+"' ";
					countSQL = countSQL + "and li.cardStatus = '"+cardStatus+"' ";
				}
				if(startDate!=null&&!("").equals(startDate)&&endDate!=null&&!("").equals(endDate)){
					infoSQL = infoSQL +"and cont.senddate >= '"+startDate+"' and cont.senddate <= '"+endDate+"' ";
//					infoSQL = infoSQL + "and li.activedate >= '"+startDate+"' and li.activedate<='"+endDate+"' ";
//					countSQL = countSQL + "and li.activedate >= '"+startDate+"' and li.activedate<='"+endDate+"' ";
					countSQL = countSQL + "and cont.senddate >= '"+startDate+"' and cont.senddate <= '"+endDate+"' ";
				}
				if(cvalistartDate!=null&&!("").equals(cvalistartDate)&&cvaliendDate!=null&&!("").equals(cvaliendDate)){
					infoSQL = infoSQL + "and li.cvalidate >= '"+cvalistartDate+"' and li.cvalidate<='"+cvaliendDate+"' ";
					countSQL = countSQL + "and li.cvalidate >= '"+cvalistartDate+"' and li.cvalidate<='"+cvaliendDate+"' ";
				}
			}else{
//				1.根据appntName，idtype,idno查wfappntlist
				infoSQL = "select distinct li.cardno,cont.certifycode,ld.riskwrapcode,ld.wrapname,app.name,app.idno,app.birthday,cont.senddate,li.cvalidate,li.inactivedate,li.prem,li.CARDSTATUS,(select distinct(from) from wfcontlist cont where cont.cardno = li.cardno) from "+ 
								"WFAPPNTLIST app "+
								"inner join LICARDACTIVEINFOLIST li on app.cardno = li.cardno "+
								"inner join  WFCONTLIST cont on app.cardno = cont.cardno "+
								"inner join lmcardrisk lr on cont.certifycode=lr.certifycode"+
								" inner join ldwrap ld on ld.riskwrapcode=lr.riskcode "+
								"where 1=1 and cont.messagetype='01' ";
				
				countSQL = "select count(1) from "+ 
				"WFAPPNTLIST app "+
				"inner join LICARDACTIVEINFOLIST li on app.cardno = li.cardno "+
				"inner join WFCONTLIST cont on app.cardno = cont.cardno "+
				"inner join lmcardrisk lr on cont.certifycode=lr.certifycode"+
				" inner join ldwrap ld on ld.riskwrapcode=lr.riskcode "+
				"where 1=1 and cont.messagetype='01' ";
				System.out.println("cardHeading:"+cardHeading);
				if(cardHeading!=null&&!("").equals(cardHeading)){
					String[] cardHeadings = cardHeading.split("@");
					int length = cardHeadings.length;
					infoSQL = infoSQL + "and (";
					countSQL = countSQL + "and (";
					for(int i=0;i<length;i++){
						if(i == length -1){
							infoSQL = infoSQL + "cont.cardno like '"+cardHeadings[i]+"%' ) ";
							countSQL = countSQL + "cont.cardno like '"+cardHeadings[i]+"%' ) ";
							break;
						}
						infoSQL = infoSQL + "cont.cardno like '"+cardHeadings[i]+"%' or ";
						countSQL = countSQL + "cont.cardno like '"+cardHeadings[i]+"%' or ";
					}
					
				}
				if(plat!=null&&!("").equals(plat)){
					infoSQL = infoSQL + "and cont.branchCode = '"+plat+"' ";
					countSQL = countSQL + "and cont.branchCode = '"+plat+"' ";
				}
				if(appntName!=null&&!("").equals(appntName)){
					infoSQL = infoSQL + "and app.Name = '"+appntName+"' ";
					countSQL = countSQL + "and app.Name = '"+appntName+"' ";
				}
				if(contNo!=null&&!("").equals(contNo)){
					infoSQL = infoSQL +"and li.cardno = '"+contNo+"' ";
					countSQL = countSQL +"and li.cardno = '"+contNo+"' ";
				}
				if(idType!=null&&!("").equals(idType)){
					infoSQL = infoSQL + "and app.idtype = '"+idType+"' ";
					countSQL = countSQL + "and app.idtype = '"+idType+"' ";
				}
				if(idNo!=null&&!("").equals(idNo)){
					infoSQL = infoSQL + "and app.idNo = '"+idNo+"' ";
					countSQL = countSQL + "and app.idNo = '"+idNo+"' ";
				}
				if(cardStatus!=null&&!("").equals(cardStatus)){
					infoSQL = infoSQL + "and li.cardStatus = '"+cardStatus+"' ";
					countSQL = countSQL + "and li.cardStatus = '"+cardStatus+"' ";
				}
				if(startDate!=null&&!("").equals(startDate)&&endDate!=null&&!("").equals(endDate)){
					infoSQL = infoSQL +"and cont.senddate >= '"+startDate+"' and cont.senddate <= '"+endDate+"' ";
//					infoSQL = infoSQL + "and li.activedate >= '"+startDate+"' and li.activedate<='"+endDate+"' ";
//					countSQL = countSQL + "and li.activedate >= '"+startDate+"' and li.activedate<='"+endDate+"' ";
					countSQL = countSQL + "and cont.senddate >= '"+startDate+"' and cont.senddate <= '"+endDate+"' ";
				}
				if(cvalistartDate!=null&&!("").equals(cvalistartDate)&&cvaliendDate!=null&&!("").equals(cvaliendDate)){
					infoSQL = infoSQL + "and li.cvalidate >= '"+cvalistartDate+"' and li.cvalidate<='"+cvaliendDate+"' ";
					countSQL = countSQL + "and li.cvalidate >= '"+cvalistartDate+"' and li.cvalidate<='"+cvaliendDate+"' ";
				}
			}

			System.out.println("infoSQL:  "+infoSQL);
			System.out.println("countSQL: "+countSQL);
			ExeSQL exeSQL = new ExeSQL();
			String pageSQL = qTool.buildPageSql(infoSQL,Integer.valueOf(pageIndex).intValue(),Integer.valueOf(pageSize).intValue());
			System.out.println("pageSQL: "+pageSQL);
			tSsrs = exeSQL.execSQL(pageSQL);
			totalcount = exeSQL.getOneValue(countSQL);
			
			if(tSsrs == null || tSsrs.getMaxRow() == 0 || totalcount == null || totalcount.equals("0")){
				info = "没有符合条件的数据";
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			info = "查询携程相关信息失败";
			return false;
		}
		return true;
	}
	//返回正确报文
	private OMElement buildResponseOME(SSRS tSsrs){
		int length = tSsrs.getMaxRow();
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement contStateSearch = factory.createOMElement("CONTSTATESEARCH",null);
		for(int i=1;i<=length;i++){
			OMElement data = factory.createOMElement("DATA",null);
			addOm(data, "CARDNO", tSsrs.GetText(i, 1));
			addOm(data, "CERTIFYCODE", tSsrs.GetText(i, 2));
			addOm(data, "RISKCODE", tSsrs.GetText(i, 3));
			addOm(data, "RISKNAME", tSsrs.GetText(i, 4));
			addOm(data, "APPNAME", tSsrs.GetText(i, 5));
			addOm(data, "IDNO", tSsrs.GetText(i, 6));
			addOm(data, "BIRTHDAY", tSsrs.GetText(i, 7));
			addOm(data, "SENDDATE", tSsrs.GetText(i, 8));
			addOm(data, "CVALISTARTDATE", tSsrs.GetText(i, 9));
			addOm(data, "CVALIENDDATE", tSsrs.GetText(i, 10));
			addOm(data, "PREM", tSsrs.GetText(i, 11));
			addOm(data, "CARDSTATUS", tSsrs.GetText(i, 12));
			addOm(data, "FROM", tSsrs.GetText(i, 13));
	
			contStateSearch.addChild(data);
		}
		addOm(contStateSearch, "TOTALROWNUM", totalcount);
		root.addChild(contStateSearch);
		return root;
	}
	//返回问题报文
	private OMElement buildErrorOME(String info){
		OMElement root = buildQueryData();
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement contStateSearchError = factory.createOMElement("CONTSTATESEARCHERROR",null);
		addOm(contStateSearchError, "ERRORINFO", info);
		root.addChild(contStateSearchError);
		return root;
	}
	private static OMElement buildQueryData() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace("http://example.org/filedata",
				"fd");
		OMElement data = fac.createOMElement("queryData", omNs);
		return data;
	}
	
	private static OMElement addOm(OMElement Om,String Name,String Value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement tName = factory.createOMElement(Name, null);
		tName.setText(Value);
		Om.addChild(tName); 
		return Om;
	}
}
