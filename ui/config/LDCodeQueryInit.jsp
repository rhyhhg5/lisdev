<%
//程序名称：LDCodeQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-26 13:18:17
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CodeType').value = "";
    fm.all('Code').value = "";
    fm.all('CodeName').value = "";
    fm.all('CodeAlias').value = "";
    fm.all('ComCode').value = "";
    fm.all('OtherSign').value = "";
  }
  catch(ex) {
    alert("在LDCodeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDCodeGrid();  
  }
  catch(re) {
    alert("LDCodeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDCodeGrid;
function initLDCodeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="50px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="编码类型";         		//列名
    iArray[1][1]="50px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="编码";         		//列名
    iArray[2][1]="50px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="编码名称";         		//列名
    iArray[3][1]="50px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="编码别名";         		//列名
    iArray[4][1]="50px";         		//列名
    iArray[4][3]=0;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="机构代码";         		//列名
    iArray[5][1]="50px";         		//列名
    iArray[5][3]=0;         		//列名
    
    iArray[6]=new Array();
    iArray[6][0]="其它标志";         		//列名
    iArray[6][1]="50px";         		//列名
    iArray[6][3]=0;         		//列名
   
    LDCodeGrid = new MulLineEnter( "fm" , "LDCodeGrid" ); 
    //这些属性必须在loadMulLine前

    LDCodeGrid.mulLineCount = 0;   
    LDCodeGrid.displayTitle = 1;
    LDCodeGrid.hiddenPlus = 1;
    LDCodeGrid.hiddenSubtraction = 1;
    LDCodeGrid.canSel = 1;
    LDCodeGrid.canChk = 0;
    LDCodeGrid.selBoxEventFuncName = "showOne";

    LDCodeGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDCodeGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
