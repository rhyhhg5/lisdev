<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-26 10:05:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDBankRateInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDBankRateInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype()" >
  <form action="./LDBankRateSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDBankRate1);">
    		</td>
    		 <td class= titleImg>
        		 银行利率表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDBankRate1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      开始日期
    </TD>
    <TD  class= input>
     
      <Input class="coolDatePicker" elementtype=nacessary  verify="开始日期|DATE&NOTNULL" dateFormat="short" name=StartDate >
    </TD>
    <TD  class= title>
      结束日期
    </TD>
    <TD  class= input>

      <Input class="coolDatePicker" elementtype=nacessary  verify="结束日期|DATE&NOTNULL" dateFormat="short" name=EndDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      利率类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RateType elementtype=nacessary verify="利率类型|notnull&len<2">
    </TD>
    <TD  class= title>
      利率间隔
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RateIntv elementtype=nacessary verify="利率间隔|notnull&len<2" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      利率
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Rate >
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
