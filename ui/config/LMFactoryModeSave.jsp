<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LMFactoryModeSave.jsp
//程序功能：
//创建日期：2004-12-22 14:24:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.config.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LMFactoryModeSchema tLMFactoryModeSchema   = new LMFactoryModeSchema();
  LMParamModeSet tLMParamModeSet   = new LMParamModeSet();
  LMFactoryModeUI tLMFactoryModeUI   = new LMFactoryModeUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    tLMFactoryModeSchema.setRiskCode(request.getParameter("RiskCode"));
    tLMFactoryModeSchema.setFactoryType(request.getParameter("FactoryType"));
    tLMFactoryModeSchema.setFactoryCode(request.getParameter("FactoryCode"));
    tLMFactoryModeSchema.setFactoryName(request.getParameter("FactoryName"));
    tLMFactoryModeSchema.setFactorySubCode(request.getParameter("FactorySubCode"));
    tLMFactoryModeSchema.setFactorySubName(request.getParameter("FactorySubName"));
    tLMFactoryModeSchema.setCalSql(request.getParameter("CalSql"));
    tLMFactoryModeSchema.setParams(request.getParameter("Params"));
    tLMFactoryModeSchema.setCalRemark(request.getParameter("CalRemark"));
    int ParamsNum=0;
    int lineCount = 0; 
    String no[] = request.getParameterValues("ParamGridNo");
    String tChk[] = request.getParameterValues("InpParamGridChk"); 
    String paraParamsName[] = request.getParameterValues("ParamGrid1");  
    String paraSequenceID[] = request.getParameterValues("ParamGrid2");  
    String paraParamsDesc[] = request.getParameterValues("ParamGrid3");  
    if(tChk!=null)
    {  
      lineCount = tChk.length; //行数  
      System.out.println("---hhhhhh---"+lineCount);   
      for(int i=0;i<lineCount;i++)
      { 
       if(tChk[i].equals("1"))
       { 
        ParamsNum++;
        LMParamModeSchema tLMParamModeSchema   = new LMParamModeSchema();
        tLMParamModeSchema.setRiskCode(request.getParameter("RiskCode"));
        tLMParamModeSchema.setFactoryType(request.getParameter("FactoryType"));
        tLMParamModeSchema.setFactoryCode(request.getParameter("FactoryCode"));
        tLMParamModeSchema.setFactorySubCode(request.getParameter("FactorySubCode"));
        tLMParamModeSchema.setFactorySubName(request.getParameter("FactorySubName"));
        tLMParamModeSchema.setParamsName(paraParamsName[i]);  
        tLMParamModeSchema.setSequenceID(paraSequenceID[i]);
        tLMParamModeSchema.setParamsDesc(paraParamsDesc[i]);        
        tLMParamModeSet.add(tLMParamModeSchema);
       }
      }       
    }
    tLMFactoryModeSchema.setParamsNum(ParamsNum);
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLMFactoryModeSchema);
	tVData.add(tLMParamModeSet);
  	tVData.add(tG);
    tLMFactoryModeUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLMFactoryModeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
