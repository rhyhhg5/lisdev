<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：E_RenewalConfInit.jsp
//程序功能：
//创建日期：2017-12-08 
//创建人  ：lzj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initLAAgentGrid(flag){
	var iArray = new Array();
	try {
		
	  	iArray[0]=new Array();
	  	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  	iArray[0][1]="30px";            		//列宽
	  	iArray[0][2]=10;            			//列最大值
	  	iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

	  	iArray[1]=new Array();
	  	iArray[1][0]="业务员/中介机构代码";
	  	iArray[1][1]="60px";
	  	iArray[1][2]=100;
	  	iArray[1][3]=0;
	  	iArray[1][21]="code";

	  	iArray[2]=new Array();
	  	iArray[2][0]="姓名/名称";
	  	iArray[2][1]="100px";
	  	iArray[2][2]=100;
	  	iArray[2][3]=0;
	  	iArray[2][21]="codeName";

	  	iArray[3]=new Array();
	  	iArray[3][0]="管理机构";
	  	iArray[3][1]="60px";
	  	iArray[3][2]=100;
	  	iArray[3][3]=0;

	  	iArray[4]=new Array();
	  	iArray[4][0]="状态";
	  	iArray[4][1]="60px";
	  	iArray[4][2]=100;
	  	iArray[4][3]=0;

	  	iArray[5]=new Array();
	  	iArray[5][0]="联系电话";
	  	iArray[5][1]="60px";
	  	iArray[5][2]=100;
	  	iArray[5][3]=0;

	  	iArray[6]=new Array();
	  	iArray[6][0]="业务员/中介机构代码（2）";
	  	iArray[6][1]="60px";
	  	iArray[6][2]=100;
		iArray[6][3]=3;
	  	iArray[6][21]="realCode";

	  	iArray[7]=new Array();
	  	iArray[7][0]="续期配置状态";
	  	iArray[7][1]="60px";
	  	iArray[7][2]=100;
	  	iArray[7][3]=0;
	  	iArray[7][21]="state";
	  	LAAgentGrid = new MulLineEnter( "fm" , "LAAgentGrid" ); 
		//这些属性必须在loadMulLine前
		LAAgentGrid.mulLineCount = 0;   
		LAAgentGrid.displayTitle = 1;
		LAAgentGrid.canSel=1;
		LAAgentGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		LAAgentGrid.hiddenSubtraction=1;
		LAAgentGrid.loadMulLine(iArray); 
			
	} catch (e) {
	}
}
function initLDCodeGrid(flag){
	var iArray = new Array();
	try {
		
	  	iArray[0]=new Array();
	  	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  	iArray[0][1]="30px";            		//列宽
	  	iArray[0][2]=10;            			//列最大值
	  	iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

	  	iArray[1]=new Array();
	  	iArray[1][0]="业务员/中介机构代码";
	  	iArray[1][1]="60px";
	  	iArray[1][2]=100;
	  	iArray[1][3]=0;
	  	iArray[1][21]="code";

	  	iArray[2]=new Array();
	  	iArray[2][0]="姓名/名称";
	  	iArray[2][1]="100px";
	  	iArray[2][2]=100;
	  	iArray[2][3]=0;
	  	iArray[2][21]="codeName";

	  	iArray[3]=new Array();
	  	iArray[3][0]="管理机构";
	  	iArray[3][1]="60px";
	  	iArray[3][2]=100;
	  	iArray[3][3]=0;

	  	iArray[4]=new Array();
	  	iArray[4][0]="状态";
	  	iArray[4][1]="60px";
	  	iArray[4][2]=100;
	  	iArray[4][3]=0;

	  	iArray[5]=new Array();
	  	iArray[5][0]="联系电话";
	  	iArray[5][1]="60px";
	  	iArray[5][2]=100;
	  	iArray[5][3]=0;

	  	iArray[6]=new Array();
	  	iArray[6][0]="业务员/中介机构代码（2）";
	  	iArray[6][1]="60px";
	  	iArray[6][2]=100;
	  	iArray[6][3]=3;
	  	iArray[6][21]="realCode";

	  	iArray[7]=new Array();
	  	iArray[7][0]="续期配置状态";
	  	iArray[7][1]="60px";
	  	iArray[7][2]=100;
	  	iArray[7][3]=0;
	  	LDCodeGrid = new MulLineEnter( "fm" , "LDCodeGrid" ); 
		//这些属性必须在loadMulLine前
		LDCodeGrid.mulLineCount = 0;   
		LDCodeGrid.displayTitle = 1;
		LDCodeGrid.canSel=1;
		LDCodeGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		LDCodeGrid.hiddenSubtraction=1;
		LDCodeGrid.loadMulLine(iArray); 
			
	} catch (e) {
	}
}
function initInpBox()
{ 
  try
  {                                   
  }
  catch(ex)
  {
    alert("在E_RenewalConfInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                       
function initForm()
{
  try{
	  initElementtype();
	  initInpBox();
	  initLAAgentGrid();
	  initLDCodeGrid();
	  showAllCodeName();
  }
  catch(ex)
  {
    alert("E_RenewalConfInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
