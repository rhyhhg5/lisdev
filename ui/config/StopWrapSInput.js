
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function beforeSubmit() {
	if(fm.WrapCode.value == null || fm.WrapCode.value == "" || fm.WrapCode.value == "null"){
		alert("套餐编码不能为空！");
		return false;
	}
	if(fm.SaleChnl.value == null || fm.SaleChnl.value == "" || fm.SaleChnl.value == "null"){
		alert("销售渠道不能为空！");
		return false;
	}
	var tWSQL = "select 1 from LDWrap where riskwrapcode = '"+fm.WrapCode.value+"'";
	var strWResult = easyExecSql(tWSQL);
	if(!strWResult){
		alert("获取套餐编码["+fm.WrapCode.value+"]信息失败，请核查套餐编码是否正确！");
		return false;
	}
	var tStopSQL = "select 1 from LDCode1 where 1=1 and codetype = 'stopwrap' and code = '"+fm.WrapCode.value+"' and code1 = '"+fm.SaleChnl.value+"' ";
	var strSResult = easyExecSql(tStopSQL);
	if(strSResult){
		alert("套餐编码为["+fm.WrapCode.value+"]的套餐已在销售渠道["+fm.SaleChnl.value+"]停售，无法再次停售！");
		return false;
	}
	tStopSQL = "select 1 from LDCode1 where 1=1 and codetype = 'stopwrapb' and code = '"+fm.WrapCode.value+"' and code1 = '"+fm.SaleChnl.value+"' ";
	strSResult = easyExecSql(tStopSQL);
	if(strSResult){
		alert("套餐编码为["+fm.WrapCode.value+"]的套餐已在销售渠道["+fm.SaleChnl.value+"]配置，请查询后进行[已配置套餐停售]操作！");
		return false;
	}
	return true;
}
function submitForm() {
	if (!beforeSubmit()) {
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "Add";
	fm.submit();
}
function stopWrap(){
	if(!beforeUpAndDel()){
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "Stop";
	fm.submit();
}
function qstopWrap(){
	if(!beforeUpAndDel()){
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "QStop";
	fm.submit();
}
function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}
function beforeUpAndDel(){
	//判定是否有选择打印数据
    var count = 0;
    var tStopCont = 0;
    var tQStopCont = 0;
	for(var i = 0; i < WrapGrid.mulLineCount; i++ )
	{
		if( WrapGrid.getChkNo(i) == true )
		{
			count ++;
			var tRowDatas = WrapGrid.getRowData(i);
		    var tStopFlag = tRowDatas[5];
		    if(tStopFlag == '0'){
		    	tStopCont++;
		    }else{
		    	tQStopCont++;
		    }
		}
	}
	if(count == 0){
		alert("请选择需要处理的套餐！");
		return false;
	}
	if(tStopCont>0 && tQStopCont>0){
		alert("处理的套餐中，存在已停售和未停售两种状态，不能同时处理！");
		return false;
	}
	return true;
}
function queryWrap(){
	initWrapGrid();
	var tTJ = "";
	if(fm.StateFlag.value == '1'){
		tTJ = " and lc1.codetype = 'stopwrap' ";
	}
	if(fm.StateFlag.value == '2'){
		tTJ = " and lc1.codetype = 'stopwrapb' ";
	}
	var strSql = "select code, codename,code1,"
			   +" (select distinct codename from ldcode where codetype in ('salechnl','lcsalechnl') and code = lc1.code1), "
	           +" case when lc1.codetype = 'stopwrap' then '已停售' else '未停售' end ,"
	           +" case when lc1.codetype = 'stopwrap' then '0' else '1' end "
	           + "from LDCode1 lc1 where 1=1 and lc1.codetype in ('stopwrap','stopwrapb') "
	           + tTJ
		       + getWherePart('code', 'WrapCode')
		       + getWherePart('code1', 'SaleChnl')
		       + " order by lc1.code,lc1.code1";
	var strResult = easyExecSql(strSql);
	if(!strResult){
		alert("未查询到已配置的该套餐信息！");
		return ;
	}	       				   
	turnPage1.queryModal(strSql, WrapGrid);
}