<%
//程序名称：LDSysVarQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-26 08:49:24
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('SysVar').value = "";
    fm.all('SysVarType').value = "";
    fm.all('SysVarValue').value = "";
  }
  catch(ex) {
    alert("在LDSysVarQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDSysVarGrid();  
  }
  catch(re) {
    alert("LDSysVarQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDSysVarGrid;
function initLDSysVarGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="系统变量名";         		//列名
    iArray[1][1]="50px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="系统变量类型";         		//列名
    iArray[2][1]="50px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="系统变量值";         		//列名
    iArray[3][1]="50px";         		//列名
    iArray[3][3]=0;         		//列名
  
    LDSysVarGrid = new MulLineEnter( "fm" , "LDSysVarGrid" ); 
    //这些属性必须在loadMulLine前

    LDSysVarGrid.mulLineCount = 0;   
    LDSysVarGrid.displayTitle = 1;
    LDSysVarGrid.hiddenPlus = 1;
    LDSysVarGrid.hiddenSubtraction = 1;
    LDSysVarGrid.canSel = 1;
    LDSysVarGrid.canChk = 0;
    LDSysVarGrid.selBoxEventFuncName = "showOne";

    LDSysVarGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDSysVarGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
