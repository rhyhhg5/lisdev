var turnPage = new turnPageClass();
function setConfig(flag){
	var selNo = LMEdorItemGrid.getSelNo();
	if(selNo==0){
		alert("请选择一条保全项目！");
		return;
	}
	var isNeedScanning = LMEdorItemGrid.getRowColDataByName(selNo-1,"isNeedScanning");
	if(isNeedScanning=="需要"){
		if(flag == 1){
			alert("该保全项目已添加过保全结案扫\n描件配置,无需重复配置!");
			return;
		}		
		fm.IsNeedScanning.value = flag;
	}
	if(isNeedScanning=="不需要"||isNeedScanning==null||isNeedScanning==""){
		if(flag == 0){
			alert("该保全项目未添加保全结案扫\n描件配置,无法进行删除操作!");
			return;
		}		
		fm.IsNeedScanning.value = flag;
	}
	var appObjType = LMEdorItemGrid.getRowColDataByName(selNo-1,"appObjType");
	var appObj = "";
	if(appObjType=="团单"){
		appObj = "G";
	}
	if(appObjType=="个单"){
		appObj = "I";
	}
	fm.appObj.value = appObj;	
	var edorCode = LMEdorItemGrid.getRowColDataByName(selNo-1,"edorCode");
	var edorName = LMEdorItemGrid.getRowColDataByName(selNo-1,"edorName");
	fm.edorType.value = edorCode;	
	fm.EdorName.value = edorName;	
	
	fm.fmtransact.value="DELETE&INSERT||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,result ){
	showInfo.close();
	if (FlagStr == "Fail" ){             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
		easyQuery(result);
	}
}
function easyQuery(result){
	var arr = result.split(",")
	var edorcode = arr[0];
	var appobj = arr[1];
	chgsql = "select EdorCode,EdorName,(case AppObj when 'I' then '个单' when 'G' then '团单' end)," +
			"(case IsNeedScanning when '1' then '需要' else '不需要' end)" +
			" from LMEdorItem where edorcode = '"+edorcode+"' and appobj = '"+appobj+"'";
	turnPage.queryModal(chgsql, LMEdorItemGrid);
}
function queryLMEdorItem(){
	if(verifyInput2()==false)return;
	var contType = fm.contType.value.trim();
	var edorType = fm.edorType.value.trim();
	var state = fm.state.value.trim();
	var edorcodeStr = "";
	if(edorType){
		edorcodeStr = " and edorcode = '"+edorType+"'";
	}
	var appObjStr = "";
	if(contType=="1"){
		appObjStr = "appObj = 'I'";
	}else if (contType=="2"){
		appObjStr = "appObj = 'G'";
	}else{
		appObjStr = "appObj in ('G','I')";
	}
	var IsNeedScanningstr = "";
	if(state=="1"){
		IsNeedScanningstr = " and IsNeedScanning = '1'";
	}else if(state=="2"){
		IsNeedScanningstr = " and (IsNeedScanning <> '1' or IsNeedScanning is null)";
	}
	var sql = "select EdorCode," +
			"EdorName," +
			"(case AppObj when 'I' then '个单' when 'G' then '团单' end)," +
			"(case IsNeedScanning when '1' then '需要' else '不需要' end)" +
			" from LMEdorItem where "+appObjStr+ edorcodeStr + IsNeedScanningstr+"order by EdorCode";
	turnPage.queryModal(sql,LMEdorItemGrid);
}
/**
 * 反显
 * @return
 */
//function invert(){
//	var num = LMEdorItemGrid.getSelNo()-1;
//	var appObjType = LMEdorItemGrid.getRowColDataByName(num,"appObjType");
//	if(appObjType=="团单"){
//		fm.contType.value = "2";
//	}
//	if(appObjType=="个单"){
//		fm.contType.value = "1";
//	}
//	fm.ContType.value = appObjType;
//	fm.edorType.value = LMEdorItemGrid.getRowColDataByName(num,"edorCode");
//	fm.EdorName.value = LMEdorItemGrid.getRowColDataByName(num,"edorName");
//	var isNeedScanning = LMEdorItemGrid.getRowColDataByName(num,"isNeedScanning");
//	if(isNeedScanning=="需要"){
//		fm.state.value = "1";
//		fm.stateName.value = "已配置";
//	}
//	if(isNeedScanning=="不需要"){
//		fm.state.value = "2";
//		fm.stateName.value = "未配置";
//	}
//}
function resetQuery(){
	fm.contType.value = '0';
	fm.ContType.value = '全部';
	fm.edorType.value = '';
	fm.EdorName.value = '';
	fm.state.value = '';
	fm.stateName.value = '';
	initForm();
}