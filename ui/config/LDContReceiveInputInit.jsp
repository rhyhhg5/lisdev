<%
//程序名称：LDContReceiveInput.jsp
//程序功能：
//创建日期：2006-08-25 10:42:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = "";
    //fm.all('ReceiveOperator').value = "";
    //fm.all('Phone').value = "";
    //fm.all('Mobile').value = "";
    fm.all('Address').value = "";
    fm.all('ZipCode').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LDContReceiveInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LDContReceiveInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initLDContReceiveGrid();  
  }
  catch(re)
  {
    alert("LDContReceiveInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDContReceiveGrid;
function initLDContReceiveGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="接收人";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][3]=1;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="手机";         		//列名
    iArray[2][1]="30px";         		//列名
    iArray[2][3]=1;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="办公电话";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=1;         		//列名
    LDContReceiveGrid = new MulLineEnter( "fm" , "LDContReceiveGrid" ); 
    //这些属性必须在loadMulLine前

    LDContReceiveGrid.mulLineCount = 1;   
    LDContReceiveGrid.displayTitle = 1;
    LDContReceiveGrid.hiddenPlus = 0;
    LDContReceiveGrid.hiddenSubtraction = 0;
    LDContReceiveGrid.canSel = 0;
    LDContReceiveGrid.canChk = 0;
    LDContReceiveGrid.selBoxEventFuncName = "showOne";

    LDContReceiveGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDContReceiveGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
