//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrResult;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput2() == false ) return false;
  var i = 0;
  arrResult = easyExecSql("select * from lduwuser where usercode = '" + fm.UserCode.value + "' and uwtype='"+ fm.UWType.value + "'", 1, 0);
  if(arrResult!=null)
  {
    alert("此核保师信息已经保存过，需要修改，请点击修改按钮！");
    return ;
  }
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
   window.focus(); 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDUWUser.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
    arrResult = easyExecSql("select * from lduwuser where usercode = '" + fm.UserCode.value + "' and uwtype='"+ fm.UWType.value + "'", 1, 0);
  if(arrResult==null)
  {
    alert("还没有保存此核保师信息，请点击[保存]按钮先进行保存！");
    return ;
  }
  if (confirm("您确实想修改该记录吗?"))
  {
  	if( verifyInput2() == false ) return false;
     var i = 0;
     var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     
     //showSubmitFrame(mDebug);
     fm.fmtransact.value = "UPDATE||MAIN";
     fm.submit(); //提交
  }
  else
  {
     //mOperate="";
     alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDUWUserQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	var uwTypeCondition = "";
	if(arrQueryResult[0][1] == "")
	{
	   uwTypeCondition = "  and (UWType is null or UWType = '') "
	}
	else
	{
	  uwTypeCondition = "  and UWType = '" + arrQueryResult[0][1] + "' ";
	}
	
	if( arrQueryResult != null )
	{
		var strSQL="select UserCode,(select username from lduser where usercode = LDUWUser.usercode),"
							+ "UWType,(case uwtype when '01' then '个险' when '02' then '团险' end),"
							+ " UWPopedom,(select codename from ldcode where codetype = 'uwpopedom' and code =LDUWUser.UWPopedom),"
							+ " UpUserCode,(select username from lduser where usercode=LDUWUser.upusercode),"
							+ " PopUWFlag,(case PopUWFlag when '1' then '是首席核保师' when '0' then '不是首席核保师' end),"
							+ " AgentUserCode,(select username from lduser where usercode=LDUWUser.AgentUserCode),"
							+ " (select UWRate from LDSpotUWRate where UserCode=LDUWUser.UserCode and uwtype=LDUWUser.uwtype) "
							+ "from LDUWUser "
							+ "where UserCode = '" + arrQueryResult[0][0] + "' "
							+ uwTypeCondition;
	    arrResult = easyExecSql(strSQL);
        fm.all('UserCode').value= arrResult[0][0];
        fm.all('UserCodeName').value= arrResult[0][1];
        fm.all('UWType').value= arrResult[0][2];
        fm.all('UWTypeName').value= arrResult[0][3];
        fm.all('UWPopedom').value= arrResult[0][4];
        fm.all('UWPopedomName').value= arrResult[0][5];
        fm.all('UpUserCode').value= arrResult[0][6];
        fm.all('UpUserCodeName').value= arrResult[0][7];
        fm.all('PopUWFlag').value= arrResult[0][8];
        fm.all('PopUWFlagName').value= arrResult[0][9];
        fm.all('AgentUserCode').value= arrResult[0][10];
        fm.all('AgentUserCodeName').value= arrResult[0][11];
        fm.all('UWRate').value= arrResult[0][12];
                     
      }
}    

//查询下级核保师
function queryJunior()
{
  if(fm.UserCode.value == "")
  {
    alert("请录入核保师编码");
    return false;
  }
  
	var sql = "select a.UserCode, a.UserName, "
	          + "   case b.UWType when '01' then '个险' when '02' then '团险' end, "
	          + "   CodeName('uwpopedom', b.UWPopedom), "
	          + "   case b.PopUWFlag when '1' then '是首席核保师' when '0' then '不是首席核保师' end "
	          + "from LDUser a, LDUWUser b "
	          + "where a.UserCode = b.UserCode "
	          + "   and b.UpUserCode = '" + fm.UserCode.value + "' ";
					
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, JuniorGrid);
	
	if(JuniorGrid.mulLineCount == 0)
	{
	  alert("没有查询到下级核保师");
	  return false;
	}
	
	return true;
}