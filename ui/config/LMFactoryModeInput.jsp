<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-12-22 14:24:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="LMFactoryModeInput.js"></SCRIPT>
  <%@include file="LMFactoryModeInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LMFactoryModeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMFactoryMode1);">
    		</td>
    		 <td class= titleImg>
        		 要素基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLMFactoryMode1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      险种编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskCode elementtype=nacessary verify="险种编码|notnull">
    </TD>
    <TD  class= title>
      要素类型
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=FactoryType  ondblclick="getFactoryTypecodedata();return showCodeListEx('getFactoryType',[this,FactoryTypeName],[0,1],'', '', '', true);" onkeyup="return showCodeListKeyEx('getFactoryType',[this,FactoryTypeName],[0,1],'', '', '', true);"  verify="要素类型|notnull"><input  class="codename" name="FactoryTypeName" elementtype=nacessary > 	
    </TD>
    <TD  class= title>
      计算编码
    </TD>
    <TD  class= input>
      <Input class= 'code8' name=FactoryCode elementtype=nacessary ondblclick="getFactoryCodecodedata();return showCodeListEx('getFactoryCode',[this,FactoryName],[0,1],'', '', '', true);" onkeyup="return showCodeListKeyEx('getFactoryCode',[this,FactoryName],[0,1],'', '', '', true);" verify="计算编码|notnull">
    </TD>    
  </TR>
  <TR  class= common>
    <TD  class= title>
      计算名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FactoryName readonly >
    </TD>
    <TD  class= title>
      计算子编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FactorySubCode elementtype=nacessary verify="计算子编码|notnull">
    </TD>
    <TD  class= title>
      计算子名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FactorySubName elementtype=nacessary verify="计算子名称|notnull">
    </TD>    
  </TR>
  <TR  class= common>
    <TD  class= title>
      计算SQL
    </TD>
    <TD  class= input colspan=6 >
      <textarea name="CalSql" cols="100" rows="3" class="common" >
      </textarea>    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      参数展现形式
    </TD>
    <TD  class= input colspan=6>
      <Input class= 'common3' name=Params >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      算法备注
    </TD>
    <TD  class= input colspan=6>
      <textarea name="CalRemark" cols="100" rows="3" class="common" >
      </textarea>      
    </TD>
  </TR>  
</table>
    </Div>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMParamMode);">
    		</td>
    		 <td class= titleImg>
        		 要素参数信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLMParamMode" style= "display: ''">
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanParamGrid" >
	 </span> 
	</td>
       </tr>
    </Table>
    </Div>
    <hr/>
    <INPUT VALUE="要素类型定制" class=cssButton TYPE=button onclick="LMFactoryType();">      
    <INPUT VALUE="计算编码定制"    class=cssButton TYPE=button onclick="LMFactoryCodeToType();">
    <Input type=hidden class= 'common' name=ParamsNum >
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
