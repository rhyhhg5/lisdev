<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDOperatorInsideSave.jsp
//程序功能：
//创建日期：2006-08-23 14:52:31
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.config.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDOperatorInsideSchema tLDOperatorInsideSchema   = new LDOperatorInsideSchema();
  OLDOperatorInsideUI tOLDOperatorInsideUI   = new OLDOperatorInsideUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	String mUserCode = "";
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLDOperatorInsideSchema.setUserCode(request.getParameter("UserCode"));
    tLDOperatorInsideSchema.setUserType("02");
    tLDOperatorInsideSchema.setUserName(request.getParameter("UserName"));
    tLDOperatorInsideSchema.setSex(request.getParameter("Sex"));
    tLDOperatorInsideSchema.setBirthday(request.getParameter("Birthday"));
    tLDOperatorInsideSchema.setPhone(request.getParameter("Phone"));
    tLDOperatorInsideSchema.setMobile(request.getParameter("Mobile"));
    tLDOperatorInsideSchema.setManageCom(request.getParameter("ManageCom"));
    tLDOperatorInsideSchema.setAgentCom(request.getParameter("AgentCom"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLDOperatorInsideSchema);
  	tVData.add(tG);
    tOLDOperatorInsideUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
  	ex.printStackTrace();
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDOperatorInsideUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  if(FlagStr.equals("Success")){
    VData mResult = tOLDOperatorInsideUI.getResult();
    LDOperatorInsideSchema mLDOperatorInsideSchema = (LDOperatorInsideSchema) mResult.getObjectByObjectName("LDOperatorInsideSchema",0);
  	mUserCode = mLDOperatorInsideSchema.getUserCode();
  }
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mUserCode%>");
</script>
</html>
