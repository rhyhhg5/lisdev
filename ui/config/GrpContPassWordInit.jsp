<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：GrpContPassWordInit.jsp
//程序功能：
//创建日期：2003-06-16
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在GrpContPassWordInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在GrpContPassWordInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
		initGrpContInfoGrid();
  }
  catch(re)
  {
    alert("GrpContPassWordInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 个人客户信息列表的初始化
function initGrpContInfoGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="团体保单号";         		//列名
		iArray[1][1]="70px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="印刷号";          		//列名
		iArray[2][1]="120px";            		//列宽
		iArray[2][2]=85;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="投保单位名称";        		  //列名
		iArray[3][1]="70px";            		//列宽
		iArray[3][2]=85;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="密码";          		//列名
		iArray[4][1]="40px";            		//列宽
		iArray[4][2]=16;            		 	  //列最大值
		iArray[4][3]=1;              			  //是否允许输入,1表示允许，0表示不允许
		iArray[4][21]="PassWord";

		GrpContInfoGrid = new MulLineEnter("fm" , "GrpContInfoGrid");
		// 这些属性必须在loadMulLine前
		GrpContInfoGrid.mulLineCount = 1;
		GrpContInfoGrid.displayTitle = 1;
		GrpContInfoGrid.locked = 1;
		GrpContInfoGrid.canSel = 1;
		GrpContInfoGrid.hiddenPlus = 1;
    GrpContInfoGrid.hiddenSubtraction = 1;
		GrpContInfoGrid.loadMulLine(iArray);		
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>