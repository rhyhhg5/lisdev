//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if(beforeSubmit())
	{
	fm.all('fmtransact').value ="INSERT";	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //mAction = "INSERT";

  fm.submit(); //提交
 }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDUWAddFee.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
   if(!verifyInput2()) 
    return false;
/*  if(fm.all('ICDCode').value!='')
    {
      alert("查询后，不能保存");
      return false;
      }
      */
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDUWAddFeeQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	fm.all('ICDCode').value=arrQueryResult[0][0];
	fm.all('DiseasDegree').value=arrQueryResult[0][2];
	fm.all('Sex').value=arrQueryResult[0][3];
	fm.all('Age').value=arrQueryResult[0][4];
	fm.all('RiskKind').value=arrQueryResult[0][5];
	fm.all('OPSFlag').value=arrQueryResult[0][6];
	fm.all('OPSNo').value=arrQueryResult[0][7];
/*	*/
 easyQueryClick();
}
	
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL = "";
	var tICDCode = fm.all( 'ICDCode' ).value;
	var tDiseasDegree=fm.all('DiseasDegree').value;
	var tSex = fm.all( 'Sex' ).value;
	var tAge = fm.all( 'Age' ).value;
	var tRiskKind = fm.all( 'RiskKind' ).value;
	var tOPSFlag = fm.all( 'OPSFlag' ).value;
	var tOPSNo = fm.all( 'OPSNo' ).value;
	strSQL = "select ICDCode,ICDName,DiseasDegree,Sex,Age,RiskKind,OPSFlag,OPSNo,DiseasBTypeCode,DiseasType,DiseasSTypeCode,DiseasSType,RiskGrade,OPSName,Relapse from LDUWAddFee "
	+"where 1=1 "
	+getWherePart( 'ICDCode' )
	+getWherePart( 'DiseasDegree' )
	+getWherePart( 'Sex' )
	+getWherePart( 'Age' )
	+getWherePart( 'RiskKind' )
	+getWherePart( 'OPSFlag' )
	+getWherePart( 'OPSNo' );	
  // alert(strSQL);
 // var arrResult=new Array();
        arrResult=easyExecSql( strSQL,1,0 );
        fm.all('ICDCode').value= arrResult[0][0];
        fm.all('ICDCode').readOnly=true;
        fm.all('ICDName').value= arrResult[0][1];
        fm.all('DiseasDegree').value= arrResult[0][2];
        fm.all('DiseasDegree').readOnly=true;
        fm.all('Sex').value= arrResult[0][3];
        fm.all('Sex').readOnly=true;
        fm.all('Age').value= arrResult[0][4];
        fm.all('Age').readOnly=true;
        fm.all('RiskKind').value= arrResult[0][5];
        fm.all('RiskKind').readOnly=true;
        fm.all('OPSFlag').value= arrResult[0][6];
        fm.all('OPSFlag').readOnly=true;
        fm.all('OPSNo').value= arrResult[0][7];
         fm.all('OPSNo').readOnly=true;
        fm.all('DiseasBTypeCode').value= arrResult[0][8];
        fm.all('DiseasType').value= arrResult[0][9];
        fm.all('DiseasSTypeCode').value= arrResult[0][10];
        fm.all('DiseasSType').value= arrResult[0][11];
        fm.all('RiskGrade').value= arrResult[0][12];
        fm.all('OPSName').value= arrResult[0][13];
        fm.all('Relapse').value= arrResult[0][14];
        
	}          
function getHospitCode()
{
    var strsql = "";
    var tCodeData = "";
    strsql = "select ICDCode,ICDName from LDDisease ";
    //alert("strsql :" + strsql);
    fm.all("ICDCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
} 

        
