<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDUWUserSave.jsp
//程序功能：
//创建日期：2005-01-24 18:15:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.config.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LDUWUserSchema tLDUWUserSchema   = new LDUWUserSchema();
  LCUWReponsComSet tLCUWReponsComSet = new LCUWReponsComSet();
  OLDUWUserUI tOLDUWUserUI   = new OLDUWUserUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tImpartNum[] = request.getParameterValues("LCUWReponsComGridNo");            //
  String tManageCom[] = request.getParameterValues("LCUWReponsComGrid1");      // 负责机构
  String tValidStartDate[] = request.getParameterValues("LCUWReponsComGrid2"); //有效开始日期 
  String tValidEndDate[] = request.getParameterValues("LCUWReponsComGrid3");   //有效结束日期

  
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
    tLDUWUserSchema.setUserCode(request.getParameter("UserCode"));
    tLDUWUserSchema.setUWType(request.getParameter("UWType"));
    tLDUWUserSchema.setUWPopedom(request.getParameter("UWPopedom"));
    tLDUWUserSchema.setUpUserCode(request.getParameter("UpUserCode"));
    tLDUWUserSchema.setPopUWFlag(request.getParameter("PopUWFlag"));
    tLDUWUserSchema.setAgentUserCode(request.getParameter("AgentUserCode"));
    
    
    //核保抽检比率    UserCode UserName UWGrade UWType UWRate
   LDSpotUWRateSet tLDSpotUWRateSet = new LDSpotUWRateSet();  
    
		LDSpotUWRateSchema tLDSpotUWRateSchema = new LDSpotUWRateSchema();
		tLDSpotUWRateSchema.setUserCode(request.getParameter("UserCode"));
		tLDSpotUWRateSchema.setUserName(request.getParameter("UserName"));
		tLDSpotUWRateSchema.setUWGrade(request.getParameter("UWPopedom"));				
		tLDSpotUWRateSchema.setUWRate(request.getParameter("UWRate"));
		tLDSpotUWRateSchema.setUWType(request.getParameter("UWType")) ;
		tLDSpotUWRateSet.add(tLDSpotUWRateSchema);
		
    //核保负责机构
	int ImpartCount = 0;
	if (tImpartNum != null) ImpartCount = tImpartNum.length;
	System.out.println("UserCode"+request.getParameter("UserCode"));

    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLDUWUserSchema);
	tVData.add(tLDSpotUWRateSet);
  	tVData.add(tG);
  	  System.out.println("操作是:"+transact);
    tOLDUWUserUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDUWUserUI.mErrors;
    if (!tError.needDealError())
    {                          
    	//Content = " 保存成功! ";
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>