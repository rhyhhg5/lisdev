
<%@page import="com.sinosoft.lis.config.AddScanConfUI"%><%
//程序名称：AddScanConfSave.jsp
//程序功能：
//创建日期：2017-11-21
//创建人  ：lzj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
	//接收信息，并作校验处理。
	//输入参数
	LMEdorItemSchema lmEdorItemSchema = new LMEdorItemSchema();
	AddScanConfUI addScanConfUI = new AddScanConfUI();
	CErrors tError = null;
	//后面要执行的动作：添加，修改
	
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	String Result= "";
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
		
	lmEdorItemSchema.setAppObj(request.getParameter("appObj"));
	lmEdorItemSchema.setEdorCode(request.getParameter("edorType"));
	String IsNeedScanning = request.getParameter("IsNeedScanning");
	try
  	{
		 // 准备传输数据 VData	  
	  	 VData tVData = new VData();  
	  	
		 tVData.addElement(tG);
		 tVData.addElement(lmEdorItemSchema);
		 tVData.addElement(IsNeedScanning);
		 if(addScanConfUI.submitData(tVData,transact)) 
			 transact="QUERY||MAIN";	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}			
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr==""){
		tError = addScanConfUI.mErrors;
	  	if (!tError.needDealError()){                          
			Content = " 保存成功";
	  		FlagStr = "Success";
	  		if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL")){
			  	if (addScanConfUI.getResult()!=null&&addScanConfUI.getResult().size()>0){
			  		LMEdorItemSchema item = (LMEdorItemSchema)addScanConfUI.getResult().get(0);
			  		Result = item.getEdorCode()+","+item.getAppObj();
			  		if (Result==null){
			  			FlagStr = "Fail";
			  			Content = "提交失败!!";
			  		}
			  	}
	  		}
	  }else{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	  }
	}
  	//添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

