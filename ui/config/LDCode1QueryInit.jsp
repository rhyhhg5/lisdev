<%
//程序名称：LDCode1QueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-26 11:24:08
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CodeType').value = "";
    fm.all('Code').value = "";
    fm.all('Code1').value = "";
    fm.all('CodeName').value = "";
    fm.all('CodeAlias').value = "";
    fm.all('ComCode').value = "";
    fm.all('OtherSign').value = "";
  }
  catch(ex) {
    alert("在LDCode1QueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDCode1Grid();  
  }
  catch(re) {
    alert("LDCode1QueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDCode1Grid;
function initLDCode1Grid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="50px";         		//列名
    iArray[0][3]=0;         		//列名
    
     iArray[1]=new Array();
    iArray[1][0]="编码类型";         		//列名
    iArray[1][1]="50px";         		//列名
    iArray[1][3]=0;         		//列名
    
     iArray[2]=new Array();
    iArray[2][0]="编码";         		//列名
    iArray[2][1]="50px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="子编码";         		//列名
    iArray[3][1]="50px";         		//列名
    iArray[3][3]=0;         		//列名
    
     iArray[4]=new Array();
    iArray[4][0]="编码名称";         		//列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=0;         		//列名
    
     iArray[5]=new Array();
    iArray[5][0]="编码别名";         		//列名
    iArray[5][1]="50px";         		//列名
    iArray[5][3]=0;         		//列名
    
     iArray[6]=new Array();
    iArray[6][0]="机构代码";         		//列名
    iArray[6][1]="50px";         		//列名
    iArray[6][3]=0;         		//列名
    
       iArray[7]=new Array();
    iArray[7][0]="其它标志";         		//列名
    iArray[7][1]="50px";         		//列名
    iArray[7][3]=0;         		//列名
 
    LDCode1Grid = new MulLineEnter( "fm" , "LDCode1Grid" ); 
    //这些属性必须在loadMulLine前

    LDCode1Grid.mulLineCount = 10;   
    LDCode1Grid.displayTitle = 1;
    LDCode1Grid.hiddenPlus = 1;
    LDCode1Grid.hiddenSubtraction = 1;
    LDCode1Grid.canSel = 1;
    LDCode1Grid.canChk = 0;
    LDCode1Grid.selBoxEventFuncName = "showOne";

    LDCode1Grid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDCode1Grid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
