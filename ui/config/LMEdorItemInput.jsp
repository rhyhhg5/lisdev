<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-26 09:25:23
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LMEdorItemInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LMEdorItemInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LMEdorItemSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMEdorItem1);">
    		</td>
    		 <td class= titleImg>
        		 保全项目定义表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLMEdorItem1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      保全项目编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EdorCode elementtype=nacessary verify="保全项目编码|notnull&len<=6">
    </TD>
    <TD  class= title>
      保全项目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EdorName elementtype=nacessary verify="保全项目名称|notnull&len<=60">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      申请对象类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppObj elementtype=nacessary verify="申请对象类型|notnull&len<=1">
    </TD>
    <TD  class= title>
      页面展示层次标记
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DisplayFlag verify="页面战士层次标记|len<=1">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否需要重算
    </TD>
    <TD  class= input>
      <Input class= 'code' name=CalFlag elementtype=nacessary verify="是否需要重算|notnull&len<=1" CodeData= "0|^Y|是^N|否" ondblClick="showCodeListEx('CalFlag',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('CalFlag',[this,''],[0,1]);">
    </TD>
    <TD  class= title>
      界面中是否显示项目明细
    </TD>
    <TD  class= input>
      <Input class= 'common' name=NeedDetail verify="界面时候显示项目明细|len<=1">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否需要打印保全清单
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpNeedList verify="是否需要打印保权清单|len<=1">
    </TD>
    <TD  class= title>
      保全权限
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EdorPopedom verify="保权权限|len<=2">
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
