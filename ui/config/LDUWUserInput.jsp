<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-24 18:15:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDUWUserInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LDUWUserInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDUWUserSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDUWUser1);">
    		</td>
    		 <td class= titleImg>
        		 核保师信息表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDUWUser1" style= "display: ''">
    <table  class= common align='center' >
      <TR  class= common>
        <TD  class= title>
          核保师编码
        </TD>
        <TD  class= input>
          <Input class= 'codeno' name=UserCode  verify="核保师编码|notnull&len<=10" ondblclick="return showCodeList('uwusercode',[this,UserCodeName],[0,1]);" onkeyup="return showCodeListKey('uwusercode', [this,UserCodeName],[0,1]);"><input  class="codename" name="UserCodeName" elementtype=nacessary > 	
        </TD>
        <TD  class= title>
          核保师类型
        </TD>
        <TD  class= input>
          <Input class= 'codeno' name=UWType verify="核保师类型|len<=2" CodeData= "0|^01|个险^02|团险" ondblClick="showCodeListEx('UWType',[this,UWTypeName],[0,1]);" onkeyup="showCodeListKeyEx('UWType',[this,UWTypeName],[0,1]);"><input  class="codename" name="UWTypeName" elementtype=nacessary > 	
        </TD>
        <TD  class= title>
          核保级别
        </TD>
        <TD  class= input>
          <Input class= 'codeno' name=UWPopedom  verify="核保级别|notnull&len<=2" ondblclick=" showCodeList('UWPopedom',[this,UWPopedomName],[0,1]);"  onkeyup="return showCodeListKey('UWPopedom',[this,UWPopedomName],[0,1]);"><input  class="codename" name="UWPopedomName" elementtype=nacessary > 	
        </TD>
      </TR>  
      <TR  class= common>
      	<TD  class= title>
          抽检比率
        </TD>
        <TD  class= input>
          <Input class= 'common' name=UWRate verify="抽检比率|NUM&len<=3"  elementtype=nacessary>
        </TD>
        
        <TD  class= title>
          上级核保师
        </TD>
        <TD  class= input>
          <Input class= 'codeno'  name=UpUserCode verify="上级核保师|notnull&len<=10"  ondblclick="return showCodeList('uwusercode',[this,UpUserCodeName],[0,1]);" onkeyup="return showCodeListKey('uwusercode', [this,UpUserCodeName],[0,1]);"><input  class="codename" name="UpUserCodeName" elementtype=nacessary > 	
        </TD>  	
        <TD  class= title>
          首席核保标志
        </TD>
        <TD  class= input>
          <Input class= 'codeno' name=PopUWFlag verify="首席核保标志|len<=1" CodeData= "0|^1|是首席核保师^0|不是首席核保师" ondblClick="showCodeListEx('PopUWFlag',[this,PopUWFlagName],[0,1]);" onkeyup="showCodeListKeyEx('PopUWFlag',[this,PopUWFlagName],[0,1]);"><input  class="codename" name="PopUWFlagName" elementtype=nacessary > 	
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          核保委托人
        </TD>
        <TD  class= input>
          <Input class= 'codeno' name=AgentUserCode verify="核保委托人|len<=10" ondblclick="return showCodeList('uwusercode',[this,AgentUserCodeName],[0,1]);" onkeyup="return showCodeListKey('uwusercode', [this,AgentUserCodeName],[0,1]);"><input  class="codename" name="AgentUserCodeName"> 	
        </TD>
      </TR>
    </table>
    </Div>	   

    <!--下级核保师-->
  	<INPUT class=cssButton name="queryJuniorButton" VALUE="查询下级"  TYPE=button onclick="queryJunior();">

    <table>
      <tr> 
        <td class= common> 
        	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divJuniorGrid);"> 
        </td><br>
        <td class= titleImg>下级核保师</td>
        <td class= common> 
          &nbsp;&nbsp;
        </td>
      </tr>
    </table>
    <Div  id= "divJuniorGrid" style= "display: ''" align="center">  
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
    				<span id="spanJuniorGrid" >
    				</span> 
    	  		</td>
    		</tr>
    	</table>
    	<Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
    	</Div>	
    	
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>