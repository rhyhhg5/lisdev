
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;

function queryWrapGrid() {
	var strSql = "select wrapcode,wrapcodename,makedate "
        + "from  ldwrapxq where state='1' "
	    + " with ur";
//	var strResult = easyExecSql(strSql);
	turnPage1.pageLineNum = 10;
	turnPage1.queryModal(strSql,WrapGrid);
	
}


function submitForm() {
	if(!check()){
		return false;
	}
	fm.tWrapCode.value = fm.WrapCode2.value;
	fm.tWrapCodeName.value = fm.WrapName2.value;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "Add";
	fm.submit();
}

function deleteClick(){
	if(!beforeUpAndDel()){
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "Delete";
	fm.submit();
}

function beforeUpAndDel(){
	//判定是否有选择
	var checkFlag = 0;
	var WrapCode;
	var WrapCodeName;
	for (i = 0; i < WrapGrid.mulLineCount; i++)
	{
		if (WrapGrid.getSelNo(i)) 
		{
		checkFlag = WrapGrid.getSelNo();
		}
	}

	if (checkFlag == 0)
	{
		alert("请选择一个套餐！");
		return false;
	}
	fm.tWrapCode.value = WrapGrid.getRowColData(checkFlag-1, 1);
	fm.tWrapCodeName.value = WrapGrid.getRowColData(checkFlag-1, 2);
	return true;
}
function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}

function queryWrap(){
	var strSql = "select riskwrapcode,wrapname "
	           + "from  LDWrap where riskwrapcode = '"+fm.RiskWrapCode.value+"'"
		       + " with ur";
	var strResult = easyExecSql(strSql);
	if(!strResult){
		alert("未查询到该套餐信息！");
		return false;
	}
	 fm.WrapCode2.value = strResult[0][0];
     fm.WrapName2.value = strResult[0][1];
	
}
function check(){
	var strSql = "select wrapcode "
        + "from  LDWrapXq where wrapcode = '"+fm.WrapCode2.value+"' and state='1'"
	    + " with ur";
	var strResult = easyExecSql(strSql);
	if(fm.WrapCode2.value==''||fm.WrapCode2.value==null){
		alert("请先查询！");
		return false;
	}
	if(strResult){
		alert("该套餐信息已配置！");
		return false;
	}
	return true;
}