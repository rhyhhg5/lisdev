
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function beforeSubmit() {
	if(fm.RiskCode.value == null || fm.RiskCode.value == "" || fm.RiskCode.value == "null"){
		alert("险种编码不能为空！");
		return false;
	}
	var tWSQL = "select 1 from lmriskapp where riskcode = '"+fm.RiskCode.value+"'";
	var strWResult = easyExecSql(tWSQL);
	if(!strWResult){
		alert("获取险种编码["+fm.RiskCode.value+"]信息失败，请核查险种编码是否正确！");
		return false;
	}
	return true;
}


//触发新建停售stopRisk

function updateRisk(state){
	fm.subriskflag.value = state;
	if(!beforeUpAndDel()){
		return false;
	}
	
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
		doRefresh();
	}
}
//在数据提交前，如果我们要新增停售的话需要将日期输入
//如果取消停售的话，将enddate设置为空
function beforeUpAndDel(){
	//判定是否有选择
    var count = 0;
    var tStopCont = 0;
    var tQStopCont = 0;
    var tErrors = "";
	for(var i = 0; i < RiskGrid.mulLineCount; i++ )
	{
		if( RiskGrid.getChkNo(i) == true )
		{
			count ++;
			var tRowDatas = RiskGrid.getRowData(i);
			var tRiskCode = tRowDatas[0];
		    var tstateflag = tRowDatas[2];
		    var tFlag = fm.subriskflag.value;
		    if(tFlag == 'M'){
		    	if(tstateflag=='主险'){
		    		tStopCont++;
		    		tErrors = tErrors + "险种："+tRiskCode+"的状态为【主险】，不可进行变更为主险处理！\n";
		    	}
			    	
		    }else if(tFlag == 'S'){
		    	if(tstateflag == '附加险'){
		    		tQStopCont++;
			    	tErrors = tErrors + "险种："+tRiskCode+"的状态为【附件险】，不可进行变更为附加险处理！\n";
		    	}
		    }
		}
	}
	if(count == 0){
		alert("请选择需要处理的险种！");
		return false;
	}
	if(tStopCont>0 && tQStopCont>0){
		alert("处理的险种中，存在主险和附加险两种状态，不能同时处理！");
		return false;
	}
	if(tErrors != ""){
		alert("进行处理时：\n"+tErrors);
		return false;
	}
	return true;
}


//点击查询 将符合要求的险种显示出来
function queryRisk(){
	initRiskGrid();
	var tTJ = "";
	if(fm.StateFlag.value == 'M'){
		tTJ = " and subriskflag='M' ";
	}
	if(fm.StateFlag.value == 'S'){
		tTJ = " and subriskflag='S' ";
	}
	var strSql = "select lm.RiskCode, lm.RiskName, "
	           +" case when subriskflag='M' then '主险' else '附加险'  end "
	           + "from LMRiskApp lm,ldcode ld where 1=1  and ld.codetype='riskcodems' and ld.code=lm.riskcode "
	           + tTJ
		       + getWherePart('lm.RiskCode', 'RiskCode')
		       + " order by RiskCode ";
	var strResult = easyExecSql(strSql);
	if(!strResult){
		alert("未查询到已配置的该险种信息！");
		return false;
	}	       				   
	turnPage1.queryModal(strSql, RiskGrid);
}

//清除缓存
function doRefresh(){
  	
		if (!confirm("确定清空缓存信息吗？")) {return false;}
		
		
	var url=[
	
			"http://10.136.1.1:6201/app/RefreshCachedRiskInfoAll.jsp",
			"http://10.136.1.1:9908/app/RefreshCachedRiskInfoAll.jsp",
			"http://10.136.1.2:9003/app/RefreshCachedRiskInfoAll.jsp",
			"http://10.136.1.2:6204/app/RefreshCachedRiskInfoAll.jsp",
			"http://10.136.1.3:6205/app/RefreshCachedRiskInfoAll.jsp",
			"http://10.136.1.3:9907/app/RefreshCachedRiskInfoAll.jsp",
			"http://10.136.1.4:6207/app/RefreshCachedRiskInfoAll.jsp",
			"http://10.136.1.4:9909/app/RefreshCachedRiskInfoAll.jsp",
			"http://10.136.1.6:6209/app/RefreshCachedRiskInfoAll.jsp"

			];
	
	
	
		for(var i=0;i<url.length;i++){
				url[i]+="?dowhat=refreshCached&doclose=true";
			}
	
			
		for(var i=0;i<url.length;i++){
				open(url[i]);			
			}
		
		alert(url.length+" 个缓存："+"清除成功！");
		
	}