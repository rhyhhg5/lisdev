<%
//程序名称：LDOccupationQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-25 18:26:51
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('OccupationCode').value = "";
    fm.all('OccupationName').value = "";
    fm.all('OccupationType').value = "";
    fm.all('WorkType').value = "";
    fm.all('WorkName').value = "";
    fm.all('MedRate').value = "";
    fm.all('SuddRisk').value = "";
    fm.all('DiseaRisk').value = "";
    fm.all('HosipRisk').value = "";
  }
  catch(ex) {
    alert("在LDOccupationQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDOccupationGrid();  
  }
  catch(re) {
    alert("LDOccupationQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDOccupationGrid;
function initLDOccupationGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="工种代码";   
	  iArray[1][1]="80px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=3;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="行业代码";   
	  iArray[2][1]="80px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=1;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="行业名称";   
	  iArray[3][1]="120px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=1;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="职业名称";   
	  iArray[4][1]="120px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=1;
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="职业代码";   
	  iArray[5][1]="80px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=1;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="职业类别";   
	  iArray[6][1]="50px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=1;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="具体说明";   
	  iArray[7][1]="300px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=1;
	
	    
    LDOccupationGrid = new MulLineEnter( "fm" , "LDOccupationGrid" ); 
    //这些属性必须在loadMulLine前

    LDOccupationGrid.mulLineCount = 3;   
    LDOccupationGrid.displayTitle = 1;
    LDOccupationGrid.hiddenPlus = 1;
    LDOccupationGrid.hiddenSubtraction = 1;
    LDOccupationGrid.canSel = 1;
    LDOccupationGrid.canChk = 0;
    //LDOccupationGrid.selBoxEventFuncName = "showOne";

    LDOccupationGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDOccupationGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
