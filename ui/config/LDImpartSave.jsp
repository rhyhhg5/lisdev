<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDImpartSave.jsp
//程序功能：
//创建日期：2005-01-25 12:57:38
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.config.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDImpartSchema tLDImpartSchema   = new LDImpartSchema();
  OLDImpartUI tOLDImpartUI   = new OLDImpartUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLDImpartSchema.setImpartVer(request.getParameter("ImpartVer"));
    tLDImpartSchema.setImpartCode(request.getParameter("ImpartCode"));
    tLDImpartSchema.setImpartContent(request.getParameter("ImpartContent"));
    tLDImpartSchema.setImpartParamModle(request.getParameter("ImpartParamModle"));
    tLDImpartSchema.setUWClaimFlg(request.getParameter("UWClaimFlg"));
    tLDImpartSchema.setPrtFlag(request.getParameter("PrtFlag"));
    tLDImpartSchema.setRemark(request.getParameter("Remark"));
    //tLDImpartSchema.setMakeDate(request.getParameter("MakeDate"));
    //tLDImpartSchema.setMakeTime(request.getParameter("MakeTime"));
   // tLDImpartSchema.setModifyDate(request.getParameter("ModifyDate"));
   // tLDImpartSchema.setModifyTime(request.getParameter("ModifyTime"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLDImpartSchema);
  	tVData.add(tG);
  	System.out.println("Easdasdasdas"+transact);
    tOLDImpartUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDImpartUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
