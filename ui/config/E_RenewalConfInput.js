var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
turnPage2.pageDivName = "divPage2";
function setConfig(flag){
	if(flag == 1){
		var selNo = LAAgentGrid.getSelNo();
		if(selNo==0){
			alert("请选择一条业务员/中介机构信息！");
			return;
		}
		var state = LAAgentGrid.getRowColDataByName(selNo-1,"state");
		if(state == "已配置"){
			alert("该业务员/中介机构已配置，不能重复进行配置！");
			return;
		}

		var realCode = LAAgentGrid.getRowColDataByName(selNo-1,"realCode");
		var	code = LAAgentGrid.getRowColDataByName(selNo-1,"code");
		var codeName = LAAgentGrid.getRowColDataByName(selNo-1,"codeName");
		fm.codeName.value = codeName;
		fm.code.value = realCode;
		if(fm.agentCode.value != null && fm.agentCode.value != ""){
			fm.otherSign.value = code;			
		}else{
			fm.otherSign.value = "";
		}
		fm.fmtransact.value="INSERT||MAIN";		
	}
	if(flag == 2){
		var selNo = LDCodeGrid.getSelNo();
		if(selNo==0){
			alert("请选择一条业务员/中介机构信息！");
			return;
		}
		var realCode = LDCodeGrid.getRowColDataByName(selNo-1,"realCode");
		var codeName = LDCodeGrid.getRowColDataByName(selNo-1,"codeName");
		var code = LDCodeGrid.getRowColDataByName(selNo-1,"code");
		fm.code.value = realCode;
		fm.codeName.value = codeName;
		fm.otherSign.value = code;
		fm.fmtransact.value="DELETE||MAIN";	
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,result ){
	showInfo.close();
	if (FlagStr == "Fail" ){             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
		easyQuery(result);
		initLAAgentGrid();
	}
}
function easyQuery(result){
	var code = result;
	var chgsql = "select nvl(otherSign,code),codename, "
				+ " (select ManageCom from laagent where GroupAgentCode = a.otherSign "
				+ " union "
				+ " select ManageCom from lacom where AgentCom = a.code "
				+ " ),"
				+ " (select AgentState from laagent where GroupAgentCode = a.otherSign" 
				+ " union "
				+ " select endflag from lacom where AgentCom = a.code "
				+ " ),"
				+ " (select Phone from laagent where GroupAgentCode = a.otherSign "
				+ " union "
				+ " select Phone from lacom where AgentCom = a.code "
				+ " ),"
				+ " code,"
				+ " '已配置'"
				+ " from ldcode a "
				+ " where codetype = 'DSXQ' and code = '"+code+"'"
				+ " with ur ";
	turnPage2.queryModal(chgsql, LDCodeGrid);
}
function queryLDCode(){
	var sql = "select nvl(otherSign,code),codename, "
			+ " (select ManageCom from laagent where AgentCode = a.code "
			+ " union "
			+ " select ManageCom from lacom where AgentCom = a.code "
			+ " ),"
			+ " (select AgentState from laagent where AgentCode = a.code" 
			+ " union "
			+ " select endflag from lacom where AgentCom = a.code "
			+ " ),"
			+ " (select Phone from laagent where AgentCode = a.code "
			+ " union "
			+ " select Phone from lacom where AgentCom = a.code "
			+ " ),"
			+ " code,"
			+ " '已配置'"
			+ " from ldcode a "
			+ " where codetype = 'DSXQ' "
			+ " with ur ";
	turnPage2.queryModal(sql,LDCodeGrid);
}
function switchSalChnl(saleChnl){
	var result = new Array();
	if(saleChnl == "01"){
		result[0] = "1";
        result[1] = "01";
	}
	if(saleChnl == "02"){
		result[0] = "2";
		result[1] = "01";
	}
	if(saleChnl == "03"){
		result[0] = "2";
		result[1] = "02";
	}
	if(saleChnl == "04" || saleChnl == "13"){
		result[0] = "3";
		result[1] = "01";
	}
	if(saleChnl == "06"){
		result[0] = "1";
		result[1] = "03";
	}
	if(saleChnl == "07"){
		result[0] = "2";
		result[1] = "01";
	}
	if(saleChnl == "10"){
		result[0] = "1";
		result[1] = "02";
	}
	if(saleChnl == "14"){
		result[0] = "5";
		result[1] = "01";
	}
	if(saleChnl == "15"){
		result[0] = "5";
		result[1] = "01";
	}
	if(saleChnl == "16"){
		result[0] = "6";
		result[1] = "01";
	}
	if(saleChnl == "17"){
		result[0] = "7";
		result[1] = "01";
	}
	if(saleChnl == "18"){
		result[0] = "6";
		result[1] = "01";
	}
	if(saleChnl == "19"){
		result[0] = "1";
		result[1] = "05";
	}
	if(saleChnl == "20"){
		result[0] = "6";
		result[1] = "02";
	}
	if(saleChnl == "21"){
		result[0] = "8";
		result[1] = "01";
	}
	if(saleChnl == "22"){
		result[0] = "8";
		result[1] = "02";
	}
	return result;
}
function queryLAAgent(){
	if(!verifyInput2())
		return;
	var saleChnl = fm.saleChnl.value;
	var agentCode = fm.agentCode.value;
	var agentCom = fm.agentCom.value;
	if((agentCode == null || agentCode == "") && (agentCom == null || agentCom == "")){
		alert("业务员代码和中介机构代码不能同时为空！");
		return;
	}
	var result = switchSalChnl(saleChnl);
	var branchType = result[0];
	var branchType2 = result[1];
	var sql;
	if(agentCode != null && agentCode != ""){
		fm.comCode.value = "1";
		sql = " select GroupAgentCode,Name,ManageCom,AgentState,Phone,agentcode,"
			+ " (case 1 when (select 1 from ldcode where codetype = 'DSXQ' and code = a.AgentCode) then '已配置' else '未配置' end)"
			+ " from laagent a "
			+ " where"
			+ " a.GroupAgentCode = '"+agentCode+"' "
			+ " and branchType = '"+branchType+"'"
			+ " and branchType2 = '"+branchType2+"' " 
			+ " and agentstate <= '02'"
			+ " with ur";
	}else{
		fm.comCode.value = "2";
		sql = " select AgentCom,Name,ManageCom,endflag,Phone,AgentCom,"
			+ " (case 1 when (select 1 from ldcode where codetype = 'DSXQ' and code = a.AgentCom) then '已配置' else '未配置' end) "
			+ " from lacom a "
			+ " where"
			+ " a.AgentCom = '"+agentCom+"'"
			+ " and branchType = '"+branchType+"'"
			+ " and branchType2 = '"+branchType2+"' "
			+ " with ur ";
	}
	turnPage.queryModal(sql,LAAgentGrid);
	if(LAAgentGrid.mulLineCount == 0){
		if(fm.comCode.value == "1"){
			alert("当前渠道下未查询到该业务员信息，请核对查询条件是否正确！");
		}
		if(fm.comCode.value == "2"){
			alert("当前渠道下未查询到该中介机构信息，请核对查询条件是否正确！");
		}
	}
}
function monitor(flag){
	var saleChnl = fm.saleChnl.value;
	var result = switchSalChnl(saleChnl);
	var branchType = result[0];
	var branchType2 = result[1];
	if(flag == 1){
		if(branchType2=="02" || saleChnl == "04" || saleChnl == "15"){
			alert("当前销售渠道为中介渠道，只能录入中介机构代码！");
			fm.agentCode.value = "";
		}
	}
	if(flag == 2){
		if(!(branchType2=="02" || saleChnl == "04" || saleChnl == "15") && saleChnl != "" && saleChnl != null){
			alert("当前销售渠道不为中介渠道，不能录入中介机构代码！");
			fm.agentCom.value = "";
		}
	}
}
function checkInput(){
	var saleChnl = fm.saleChnl.value;
	var agentCode = fm.agentCode.value;
	var agentCom = fm.agentCom.value;
	var result = switchSalChnl(saleChnl);
	var branchType = result[0];
	var branchType2 = result[1];
	if(saleChnl != null && saleChnl != "" && (!(agentCode == "" || agentCode == null) || !(agentCom == "" || agentCom == null))){
		if(!(agentCode == "" || agentCode == null) && (branchType2=="02" || saleChnl == "04" || saleChnl == "15")){
			alert("当前销售渠道为中介渠道，只能录入中介机构代码！");
			fm.agentCode.value = "";
		}
		if(!(agentCom == "" || agentCom == null) &&(!(branchType2=="02" || saleChnl == "04" || saleChnl == "15"))){
			alert("当前销售渠道不为中介渠道，不能录入中介机构代码！");
			fm.agentCom.value = "";
		}
	}
}