<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：RevisitDetInput.jsp
 //程序功能：回访任务处理
 //创建日期：2009-7-29
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
 
   
					
%>
<script>
  var operator = "<%=tGI.Operator%>";   //记录操作员
 
</script>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
 <SCRIPT src="RevisitDet.js"></SCRIPT>
 <%@include file="RevisitDetInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form  method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >回访基本资料</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">回访号</td>
    <td class="input">
    	<input class="readonly" name="AnswerID" readonly >
    </td>
    <td class="title">回访类型</td>
    <td class="input">
    	<input class="readonly" name="AnswerType" readonly >
    	<!--input class="code8" name="AnswerType" readonly ondblclick="return showCodeList('answertype',[this],[0,1]);" onkeyup="return showCodeListKey('answertype',[this],[0,1]);" -->
    </td>
    <td class="title">应访时间</td>
    <td class="input">
    	<input class="readonly" name="ShouldVisitDate" readonly >
    </td>  
  </tr>
<tr class="common">
    <td CLASS="title">管理机构</td>
			<td CLASS="input" COLSPAN="1">
			<Input class="readonly" name="ManageCom" readonly> 
			</td>

    <td class="title"></td>
        <td class="title">
    </td>
    <td class="title"></td>
        <td class="title">
    </td>
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >客户资料</td>
  </tr>
</table>
<table>
  <tr>
    <td class="titleImg" >投保人信息</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">客户号</td>
    <td class="input">
    	<input class="readonly" name="CustomNo" readonly >
    </td>
    <td class="title">姓名</td>
    <td class="input">
    	<input class="readonly" name="CSNM" readonly >
    </td>
    <td class="title">性别</td>
    <td class="input">
    	<input class="readonly" name="Sex" readonly >
    </td>  
  </tr>
<tr class="common">
    <td class="title">年龄</td>
    <td class="input">
    	<input class="readonly" name="Age" readonly >
    </td>
    <td class="title">证件类型</td>
    <td class="input">
    	<input class="readonly" name="IdType" readonly >
    </td>
    <td class="title">证件号码</td>
    <td class="input">
    	<input class="readonly" name="IdNo" readonly >
    </td>  
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >被保人信息</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">客户号</td>
    <td class="input">
    	<input class="readonly" name="InsuredNo" readonly >
    </td>
    <td class="title">姓名</td>
    <td class="input">
    	<input class="readonly" name="InsuredName" readonly >
    </td>
    <td class="title">性别</td>
    <td class="input">
    	<input class="readonly" name="InsuredSex" readonly >
    </td>  
  </tr>
<tr class="common">
    <td class="title">年龄</td>
    <td class="input">
    	<input class="readonly" name="InsuredAge" readonly >
    </td>
    <td class="title">证件类型</td>
    <td class="input">
    	<input class="readonly" name="InsuredIdType" readonly >
    </td>      
    <td class="title">证件号码</td>
    <td class="input">
    	<input class="readonly" name="InsuredIdNo" readonly >
    </td>  
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >联系电话清单</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">固定电话</td>
    <td class="input">
    	<input class="readonly" name="Phone" readonly >
    </td>      
    <td class="title">移动电话</td>
    <td class="input">
    	<input class=readonly name="TelPhone" readonly  >
    </td>      
    <TD class= title>
            回访电话类型
    </TD>
    <TD class= input>
        <input class="codeno" name="RevisitPhoneType" ondblclick="return showCodeList('RevisitPhoneType',[this,RevisitPhoneTypeName],[0,1]);" onkeyup="return showCodeListKey('RevisitPhoneType',[this,RevisitPhoneTypeName],[0,1]);"><input class=codename name=RevisitPhoneTypeName readonly=true>
    </TD>
    <tr>
    <TD  class= title>
            回访电话
    </TD>
    <TD  class= input>
            <Input class= common name="RevisitPhone" id="RevisitPhone" maxlength=15>
    </TD>
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >保单资料</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">保单合同号</td>
    <td class="input">
    	<input class="readonly" name="ContNo" readonly >
    </td>      
    <td class="title">保单申请日期</td>
    <td class="input">
    	<input class="readonly" name="PolApplyDate" readonly >
    </td>      
    <td class="title">保单生效日期</td>
    <td class="input">
    	<input class="readonly" name="SignDate" readonly >
    </td>      
    <td class="title">保单打印日期</td>
    <td class="input">
    	<input class="readonly" name="PolPringDate" readonly >
    </td>  
  </tr>
<tr class="common">
    <td class="title">保单签收时间</td>
    <td class="input">
    	<input class="readonly" name="CustomGetPolDate" readonly >
    </td>      
    <td class="title">投保单类型</td>
    <td class="input">
    	<input class="readonly" name="SaleChnl" readonly >
    </td>      
    <td class="title">代理人编码</td>
    <td class="input">
    	<input class="readonly" name="AgentCode" readonly >
    </td>      
    <td class="title">代理人姓名</td>
    <td class="input">
    	<input class="readonly" name="AgentName" readonly >
    </td>  
  </tr>
<tr class="common">
 <td class="title">代理机构</td>
    <td class="input">
    	<input class="readonly" name="AgentManageCom" readonly >
    </td>
    <td class="title">联系地址</td>
    <td class="input">
    	<input class="readonly" name="PostalAddress" readonly >
    </td>      
    <td class="title">邮编</td>
    <td class="input">
    	<input class="readonly" name="ZipCode" readonly >
    </td>      
    <td class="title">银行网点名称</td>
    <td class="input">
    	<input class="readonly" name="BankCode" readonly >
    </td>  
  </tr>
<tr class="common">
    <td class="title">转账开户行</td>
    <td class="input">
    	<input class="readonly" name="BankName" readonly >
    </td>      
    <td class="title">银行转账账号</td>
    <td class="input">
    	<input class="readonly" name="BankAccNo" readonly >
    </td>  
     
      <td class="title"></td>
    <td class="input">
    </td> 
     <td class="title"> </td>
    <td class="input">
    <input value="既往投保信息" type=button  onclick="showApp()" class="cssButton" name="ShowAppButton">
    </td> 
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >险种清单（包括主险与附加险）</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanRiskGrid" >
     </span> 
      </td>
   </tr>
</table>

<table>
  <tr>
    <td class="titleImg" >回访历程</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanAnswerGrid" >
     </span> 
      </td>
   </tr>
</table>

<table>
  <tr>
    <td class="titleImg" >回访信息</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">回访状态</td>
    <td class="input">
    	<input class="codeno" name="ResvisitState"  ondblclick="return showCodeList('AnswerStatus2',[this,ResvisitStateName],[0,1]);" onkeyup="return showCodeListKey('AnswerStatus2',[this,ResvisitStateName],[0,1]);"  verify="回访状态|notnull"><input class="codename" name="ResvisitStateName" elementtype=nacessary>
    	</td>      
    	 
           <td class="title"><!--回访日期--></td>
    	 <td class="input">
    	<!-- <input class="coolDatePicker" dateFormat="short" name="AnswerDate"  > -->
    	</td>        
    	<td class="title"><!-- 回访时间(HH:MM) --></td>
    	 <td class="input">
    	<!--  <input class="common" name="AnswerTime"  >-->
    	</td>      
    	<td class="title"></td>    
    	<td class="title"></td>
  </tr>
</TABLE>
<TABLE  class="common" id="div01" style="display: 'none'">
  <tr class="common">
  <td class="title"> 预约日期</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="BookingDate" >
    </td>
    <td class="title">预约时间(HH:MM)</td>  
    <td class="input">
    	<input class="common" name="BookingTime" >
    </td>  
  	<td class="title"><!--  回访人员--></td>
    	 <td class="input">
    	<!-- <input class="common" name="AnswerPerson"  > -->
    </td>    
    <td class="title"><!--  预约时间(HH:MM)--></td>
    	 <td class="input">
    	<!-- <input class="common" name="BookingTime"  > -->
    </td>        
    <td class="title"></td>    
    <td class="title"></td>
  </tr>
</TABLE>
<Div  id= "div1" style="display:'none'">
<table>
  <tr>
    <td class="titleImg" >问题类型</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanIssueGrid" >
     </span> 
      </td>
   </tr>
</table>
</Div>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">关闭原因</td>
    <td class="input">
    	<input class="code" name="CloseReason" ondblclick="return showCodeList('closereason',[this],[0,1]);" onkeyup="return showCodeListKey('closereason',[this],[0,1]);" >
    </td>      
    <td class="title"></td>    
    <td class="title"></td>    
    <td class="title"></td>
  </tr>
</TABLE>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">特急案件标识</td>
    <td class="input">
    	<input class="code" name="UrgencySign" ondblclick="return showCodeList('urgencysign',[this],[0]);" onkeyup="return showCodeListKey('urgencysign',[this],[0]);" >
    </td>      
    <td class="title"></td>    
    <td class="title"></td>    
    <td class="title"></td>
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >回访备注</td>
  </tr>
</table>
<textarea class="common" name="Remark" cols="100%" rows="2" value="1000字" ></textarea><BR>
<table>
  <tr>
    <td class="titleImg" >同日抽取关联保单</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanAnsweridGrid" >
     </span> 
      </td>
   </tr>
</table>
<br>
<input value="处理完成" type=button  onclick="submitForm()" class="cssButton" name="update">&nbsp;&nbsp;&nbsp;
<input value="返回" type=button  onclick="GoToReturn()" class="cssButton" >
<input type="hidden" name="Operator">
<br><br>


</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
