<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ConsultationAccSave.jsp
//程序功能：咨诉受理
//创建日期：2009-8-6
//创建人  ：keke
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.revisit.*"%>
<%
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String transact = "";

 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 if (tG == null) {
	 	System.out.println("页面失效,请重新登陆");
	 	FlagStr = "Fail";
	 	Content = "页面失效,请重新登陆";
 }
 else{
		VData tVData = new VData();
		ConsultationAccUI tConsultationAccUI = new ConsultationAccUI();
		LIComplaintsInfoSchema tLIComplaintsInfoSchema= new LIComplaintsInfoSchema();
		tLIComplaintsInfoSchema.setTelephoneNo(request.getParameter("TelephoneNo"));
		tLIComplaintsInfoSchema.setNeedDealFlag(request.getParameter("NeedDealFlag"));
		tLIComplaintsInfoSchema.setCustomerName(request.getParameter("CustomerName"));
		tLIComplaintsInfoSchema.setIssueType(request.getParameter("IssueType"));
		tLIComplaintsInfoSchema.setComplaintType(request.getParameter("ComplaintType"));
		tLIComplaintsInfoSchema.setTransmitCom(request.getParameter("TransmitCom"));
		tLIComplaintsInfoSchema.setOtherNo(request.getParameter("OtherNo"));
		tLIComplaintsInfoSchema.setUndertakeTime(request.getParameter("UndertakeTime"));
		tLIComplaintsInfoSchema.setUndertakeDate(request.getParameter("UndertakeDate"));
		tLIComplaintsInfoSchema.setCustomerNo(request.getParameter("CustomerNo"));
		tLIComplaintsInfoSchema.setCustomerAddress(request.getParameter("CustomerAddress"));
		tLIComplaintsInfoSchema.setTelephoneCom(request.getParameter("TelephoneCom"));
		tLIComplaintsInfoSchema.setBookingDate(request.getParameter("BookingDate"));
		tLIComplaintsInfoSchema.setBookingTime(request.getParameter("BookingTime"));
		tLIComplaintsInfoSchema.setComplaintContent(request.getParameter("ComplaintContent"));
		tLIComplaintsInfoSchema.setAcceptOperator(tG.Operator);
		tLIComplaintsInfoSchema.setAcceptManageCom(tG.ManageCom);
		tLIComplaintsInfoSchema.setOperator(tG.Operator);
		tLIComplaintsInfoSchema.setUrgencySign(request.getParameter("UrgencySign"));
    try{
			tVData.add(tG);
			tVData.add(tLIComplaintsInfoSchema);
			tConsultationAccUI.submitData(tVData,"insert");
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
	  //如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr==""){
			tError = tConsultationAccUI.mErrors;
			if (!tError.needDealError()){                          
				 Content = " 保存成功! ";
				 FlagStr = "Success";
			}
			else{
				 Content = " 保存失败，原因是:" + tError.getFirstError();
				 FlagStr = "Fail";
			}
		}
 }
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
   parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

