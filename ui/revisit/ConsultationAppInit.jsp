<%
  //程序名称：ConsultationAppInit.jsp
  //程序功能：咨诉任务申请
  //创建日期：2009-8-6
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initForm(){
	try{
		initComplaintGrid();
		initComplainttwoGrid();
		queryclick2();
	}
	catch(re){
		alert("ConsultationAppInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initComplaintGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="咨诉号";
		iArray[1][1]="45px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="问题件类型";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="客户姓名";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="保单合同号";
		iArray[4][1]="75px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="咨诉状态";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="转交时间";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="预约时间";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="承办时间";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=0;
		
	    iArray[9]=new Array();
		iArray[9][0]="特急件标识";
		iArray[9][1]="60px";
		iArray[9][2]=100;
		iArray[9][3]=0;

		ComplaintGrid = new MulLineEnter( "fm" , "ComplaintGrid" ); 

		ComplaintGrid.mulLineCount=1;
		ComplaintGrid.displayTitle=1;
		ComplaintGrid.canSel=1;
		ComplaintGrid.canChk=0;
		ComplaintGrid.hiddenPlus=1;
		ComplaintGrid.hiddenSubtraction=1;

		ComplaintGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
function initComplainttwoGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="咨诉号";
		iArray[1][1]="45px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="问题件类型";
		iArray[2][1]="45px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="客户姓名";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="保单合同号";
		iArray[4][1]="75px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="咨诉状态";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="转交时间";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="预约时间";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="承办时间";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=0;
		
	    iArray[9]=new Array();
		iArray[9][0]="特急件标识";
		iArray[9][1]="60px";
		iArray[9][2]=100;
		iArray[9][3]=0;


		ComplainttwoGrid = new MulLineEnter( "fm" , "ComplainttwoGrid" ); 

		ComplainttwoGrid.mulLineCount=1;
		ComplainttwoGrid.displayTitle=1;
		ComplainttwoGrid.canSel=1;
		ComplainttwoGrid.canChk=0;
		ComplainttwoGrid.hiddenPlus=1;
		ComplainttwoGrid.hiddenSubtraction=1;
		ComplainttwoGrid. selBoxEventFuncName ="getComplaintDetail";

		ComplainttwoGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
</script>
