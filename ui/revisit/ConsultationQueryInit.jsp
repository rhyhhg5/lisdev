<%
  //程序名称：ConsultationQueryInit.jsp
  //程序功能：咨诉记录查询
  //创建日期：2009-8-6
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initForm(){
	try{
		initConsultationQueryGrid();
	}
	catch(re){
		alert("ConsultationQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initConsultationQueryGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="咨诉号";
		iArray[1][1]="50px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="来电时间";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="来电号码";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="客户姓名";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="咨诉类型";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="保单合同号";
		iArray[6][1]="75px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="来电机构";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="问题件类型";
		iArray[8][1]="75px";
		iArray[8][2]=100;
		iArray[8][3]=0;

		iArray[9]=new Array();
		iArray[9][0]="问题件状态";
		iArray[9][1]="75px";
		iArray[9][2]=100;
		iArray[9][3]=0;

		iArray[10]=new Array();
		iArray[10][0]="坐席人员";
		iArray[10][1]="40px";
		iArray[10][2]=100;
		iArray[10][3]=0;

		iArray[11]=new Array();
		iArray[11][0]="问题件处理人员";
		iArray[11][1]="50px";
		iArray[11][2]=100;
		iArray[11][3]=0;


		ConsultationQueryGrid = new MulLineEnter( "fm" , "ConsultationQueryGrid" ); 

		ConsultationQueryGrid.displayTitle=1;
		ConsultationQueryGrid.locked = 1;
		ConsultationQueryGrid.canSel=1;
		ConsultationQueryGrid.canChk=0;
		ConsultationQueryGrid.hiddenPlus=1;
		ConsultationQueryGrid.hiddenSubtraction=1;
		ConsultationQueryGrid.mulLineCount = 0;
    ConsultationQueryGrid.chkBoxEventFuncName = "";
    ConsultationQueryGrid.selBoxEventFuncName = "getComplaintDetail";

		ConsultationQueryGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
