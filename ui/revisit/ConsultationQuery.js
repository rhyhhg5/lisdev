//该文件中包含客户端需要处理的函数和事件

//程序名称：ConsultationQuery.js
//程序功能：咨诉记录查询
//创建日期：2009-8-6
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();
var turnPageConsultationQueryGrid = new turnPageClass();


function queryclick(){
	if (!verifyInput2())return false;
  var QuerySQL;
	QuerySQL = "select a.ComplaintID,"
				          +" a.MakeDate,"
				          +" a.TelephoneNo,"
				          +" a.CustomerName,"
				          +" (select codename from ldcode where codetype='complainttype' and trim(code)=a.ComplaintType),"
				          +" a.otherno,"
				          +" (select name from ldcom where comcode=a.TelephoneCom),"
				          +" (select codename from ldcode where codetype='complaintissuetype' and code=a.IssueType),"
				          +" (select codename from ldcode where codetype='issuestate' and code=a.IssueState),"
				          +" (select username from lduser where usercode=a.AcceptOperator),"
				          +" (select username from lduser where usercode=a.DealOperator)"
				   +" from LIComplaintsInfo a"
				   +" where 1 = 1 and a.TransmitCom like '"+comcode+"%' "
                + getWherePart("a.TelephoneNo", "TelephoneNo")
                + getWherePart("a.MakeDate", "MakeDate",">=")
                + getWherePart("a.MakeDate", "MakeDate1","<=")
                + getWherePart("a.MakeDate", "MakeDate2","=")
                + getWherePart("a.CustomerName", "CustomerName")
                + getWherePart("a.OtherNo", "OtherNo")
                + " order by a.MakeDate desc ";
  try{
     turnPageConsultationQueryGrid.pageDivName = "divTurnPageConsultationQueryGrid";
     turnPageConsultationQueryGrid.queryModal(QuerySQL, ConsultationQueryGrid,0,1);
  }
  catch (ex){
     alert("警告：查询咨诉信息出现异常！ ");
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}



function getComplaintDetail()
{
	var i = 0;
  var checkFlag = 0;
  var cComplaintID;
  for (i=0; i<ConsultationQueryGrid.mulLineCount; i++) {
    if (ConsultationQueryGrid.getSelNo(i)) { 
       checkFlag = ConsultationQueryGrid.getSelNo();
       break;
    }
  }
  if (checkFlag) { 
    cComplaintID = ConsultationQueryGrid.getRowColData(checkFlag - 1, 1); 
    if(cComplaintID==""){
    	alert("没有选中需要申请的咨诉!");
    	return false;
    }
    var sFeatures = "";
    var  viewFlag= 1 ;
	  window.open("./ConsultationDealInputToMain.jsp?ComplaintID="+cComplaintID+"&viewFlag=2","",sFeatures);
	}
	else{
		alert("没有选中要处理的咨诉!")
	}
	return true;
}

function queryclick2()
{
}

