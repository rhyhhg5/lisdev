<%
  //程序名称：RedealDetInit.jsp
  //程序功能：转办任务处理
  //创建日期：2009-8-4
  //创建人  ：liwb
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initForm(){
	try{
	 
		//基本信息
		initInputBox();
		//险种信息
		initPolGrid();
		QueryPolGrid();
		//回访历程
		initRevisitCourseGrid();
		QueryRevisitCourseGrid();
		//问题件
		initAnswerGrid();
		QueryAnswerGrid();
		//关联保单
		initAnsweridGrid();
		QueryAnsweridGrid();
		//界面显示
		initDiv();
		
		viewFlag();//查看状态　部分按纽置灰
	}
	catch(re){
		alert("RedealDetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initPolGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="险种编码";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="险种名称";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="保额";
		iArray[3][1]="30px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="保费";
		iArray[4][1]="30px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="缴费期间";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="保险期间";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;


		PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 

		PolGrid.mulLineCount=1;
		PolGrid.displayTitle=1;
		PolGrid.canSel=0;
		PolGrid.canChk=0;
		PolGrid.hiddenPlus=1;
		PolGrid.hiddenSubtraction=1;

		PolGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initRevisitCourseGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="回访人编号";
		iArray[1][1]="75px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="回访人姓名";
		iArray[2][1]="75px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="回访时间";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="回访结果";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="原因";
		iArray[5][1]="30px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="备注";
		iArray[6][1]="30px";
		iArray[6][2]=100;
		iArray[6][3]=0;


		RevisitCourseGrid = new MulLineEnter( "fm" , "RevisitCourseGrid" ); 

		RevisitCourseGrid.mulLineCount=1;
		RevisitCourseGrid.displayTitle=1;
		RevisitCourseGrid.canSel=0;
		RevisitCourseGrid.canChk=0;
		RevisitCourseGrid.hiddenPlus=1;
		RevisitCourseGrid.hiddenSubtraction=1;

		RevisitCourseGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
function initAnswerGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="问题类型代码";
		iArray[1][1]="30px";
		iArray[1][2]=100;
		iArray[1][3]=3;
		
		
	    iArray[2]=new Array();
		iArray[2][0]="问题类型";
		iArray[2][1]="30px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="处理状态";
		iArray[3][1]="20px";
		iArray[3][2]=100;
		iArray[3][3]=2;
		iArray[3][4] = "issuestutas";             			//是否允许输入,1表示允许，0表示不允许
        iArray[3][5]="3|4";     //引用代码对应第几列，'|'为分割符
        iArray[3][6]="0|1";    //上面的列中放置引用代码中第几位值

		iArray[4]=new Array();
		iArray[4][0]="处理状态";
		iArray[4][1]="30px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="问题说明";
		iArray[5][1]="100px";
		iArray[5][2]=100;
		iArray[5][3]=1;

		AnswerGrid = new MulLineEnter( "fm" , "AnswerGrid" ); 

		AnswerGrid.mulLineCount=0;
		AnswerGrid.displayTitle=1;
		AnswerGrid.canSel=0;
		AnswerGrid.canChk=0;
		AnswerGrid.hiddenPlus=1;
		AnswerGrid.hiddenSubtraction=1;

		AnswerGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}

function initAnsweridGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="回访号";
		iArray[1][1]="45px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="保单合同号";
		iArray[2][1]="75px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="投保日期";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="承保日期";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="折扣后保费";
		iArray[5][1]="75px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="第一主险";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="第一主险保额";
		iArray[7][1]="90px";
		iArray[7][2]=100;
		iArray[7][3]=0;


		AnsweridGrid = new MulLineEnter( "fm" , "AnsweridGrid" ); 

		AnsweridGrid.mulLineCount=1;
		AnsweridGrid.displayTitle=1;
		AnsweridGrid.canSel=0;
		AnsweridGrid.canChk=0;
		AnsweridGrid.hiddenPlus=1;
		AnsweridGrid.hiddenSubtraction=1;

		AnsweridGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}



</script>
