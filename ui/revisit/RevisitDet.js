//该文件中包含客户端需要处理的函数和事件

//程序名称：RevisitDet.js
//程序功能：回访任务处理
//创建日期：2009-7-29
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
turnPage3.pageLineNum = 17;

//提交前的校验、计算  
function submitForm()
{
  
	if( verifyInput2() == false ) return false;
	
	if(fm.RevisitPhoneType.value !='')
	{
	   if(fm.RevisitPhone.value == '')
	   {  
	      alert("你选择了回访电话类型，回访电话不能为空！");
	      return false;
	   }
	}
	
  var answerstatus=fm.all("ResvisitState").value;
  var bookingdate =fm.all("BookingDate").value;  
     
  var bookingtime =fm.all("BookingTime").value;
  
  var Remark=fm.all("Remark").value;
 
  if(answerstatus=="4"){
	  if (bookingdate==""){
	  	alert("预约日期没有录入");
	  	return false;
	  }
	  if (bookingtime==""){
	  alert("预约时间没有录入");
	  	return false;
	  }
  }
  if(bookingtime==""){
   }
  else{
	  if(checkV(bookingtime)==false){
 		  fm.BookingTime.focus(); 
 	  return false;
	 }
 }
  if(answerstatus=="3"){
  	if(Remark==""){
	  	alert("回访备注没有录入");
	  	fm.Remark.focus(); 
	  	return false;
	  }
  }
  
  var num=0;
  if(answerstatus=="2"){
  	for(var i=0;i<IssueGrid.mulLineCount;i++){
  		if(IssueGrid.getChkNo(i)){
   			num++;
   		}
   	}
   	if(num<=0){
		 	alert("没有选择问题件");
		 	return;
		}
	} 
   
   if(answerstatus == '98')
   {  
     var closeReason = fm.CloseReason.value;
     if(closeReason == null || ''==closeReason)
     {
        alert("关闭原因没有录入");
        return false;
     }
   }
	 
  //var answerdate =fm.all("AnswerDate").value;
  //var answertime =fm.all("AnswerTime").value;
 // if(answertime==""){
 // }
 // else {
//	  if(checkV(answertime)==false){ 
//		  fm.AnswerTime.focus(); 
//		  return false;
//		}
 // }
 
  
   var Sql_cont ="select * from LIAnswerInfo  a where a.AppntNo = '"+fm.all("CustomNo").value+"' and a.OtherNo <> '"+fm.all("ContNo").value+"' and  a.Operator ='"+operator+"'  " ;
    
    var arrResult=easyExecSql(Sql_cont,1,0);
    if(null!=arrResult)
    {
       alert(" 该客户在您的回访池中还有其他未处理保单，请处理！");
    }
  var closereason=fm.all("CloseReason").value;
  var UrgencySign=fm.all("UrgencySign").value;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./RevisitDetSave.jsp";
   fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    initLianswerInfo();
    initAnswerGridBox();
    initEdom();  
    top.close();
    top.opener.initForm();
  }
}


function GoToReturn(){
	try{
		top.opener.initRevisitNum();
		top.opener.queryclick();	
		top.opener.queryclick2();		
	}
	catch(ex){
		alert( "没有发现父窗口的afterQuery接口。" + ex );
	}
	top.close();
}


//既往投保信息
function showApp()
{
  
   if(null==tAppntNo||tAppntNo=="")
   {
   	  alert("投保人客户号码为空，请检查！");
   	  return false ;
   }
   
   window.open("./RevisitOldAppMain.jsp?ContNo="+tContNo+"&CustomerNo="+tAppntNo);

}



function checkV(temp_str){
	if(temp_str.length != 5){
	  alert("输入格式有误。");
	  return false;
	}
	var hour_s = temp_str.substr(0,2);
	var split1_s = temp_str.substr(2,1);
	var min_s = temp_str.substr(3,2);
	if(!(split1_s==":")){
	  alert("输入格式有误。");
	  return false;
	}
	if(!(hour_s>=0&&hour_s<24&&min_s>=0&&min_s<60)){
	  alert("输入时间有误。");
	  return false;
	}
}

function afterCodeSelect(a, b) 
{
	if(a === 'AnswerStatus2')
	{
	   	if(fm.all("ResvisitState").value == '4')
		{
		   div01.style.display="";
		}else
		{ 
		   div01.style.display="none";
		}
		if(fm.all("ResvisitState").value == '2')
		{
		   div1.style.display="";
		}
		else 
		{
           div1.style.display="none";
		}
	}
	if(a == "RevisitPhoneType") 
	{   
	
	    var RevisitPhoneType = fm.RevisitPhoneType.value;
		if(RevisitPhoneType=="0")
		{
			if(fm.TelPhone.value == "")
			{
				alert("您选择了同手机号码，但是手机号码为空，请重新选择电话回访类型！");
				fm.RevisitPhoneType.value = '';
				fm.RevisitPhoneTypeName.value = '';
				fm.RevisitPhone.value = '';
			}
			else
			{
				fm.RevisitPhone.value = fm.TelPhone.value;
				document.getElementById("RevisitPhone").readOnly = true;

			}
		}
		else if(RevisitPhoneType=="1")
		{
			if(fm.Phone.value == "" || fm.Phone.value == "")
			{
				alert("您选择了同固定电话，但是固定电话号码为空，请重新选择电话回访类型！");
				fm.RevisitPhoneType.value = '';
				fm.RevisitPhoneTypeName.value = '';
				fm.RevisitPhone.value = '';
			}
			else
			{
				fm.RevisitPhone.value = fm.Phone.value;	
				document.getElementById("RevisitPhone").readOnly = true;			
			}
		}
		else
		{
			fm.RevisitPhone.value = "";	 
			document.getElementById("RevisitPhone").readOnly = false;
		}
	}
}
