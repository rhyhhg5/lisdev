<%
  //程序名称：RevisitDetInit.jsp
  //程序功能：回访任务处理
  //创建日期：2009-7-29
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%
   String tAppntNo = request.getParameter("AppntNo");
   System.out.println("AppntNo="+tAppntNo);
   String tContNo = request.getParameter("ContNo");
   String tAnswerID = request.getParameter("AnswerID");
   String tManageCom = request.getParameter("ManageCom");
   ExeSQL tExeSQL = new ExeSQL();
   tManageCom = tExeSQL.getOneValue("select name from ldcom where comcode='"+tManageCom+"'");
   String tAnswerType = request.getParameter("AnswerType");
   String tOperator = request.getParameter("Operator");
   String tShouldVisitDate=request.getParameter("ShouldVisitDate");
   String tAnswerStatus=request.getParameter("AnswerStatus");
%>


<script language="JavaScript">
var tAppntNo = '<%=tAppntNo%>' ;
var tContNo = '<%=tContNo%>' ;
var tAnswerID = '<%=tAnswerID%>' ;
var tManageCom = '<%=tManageCom%>' ;
var tAnswerType = '<%=tAnswerType%>' ;
var tOperator = '<%=tOperator%>' ;
var tShouldVisitDate = '<%=tShouldVisitDate%>' ;
var tAnswerStatus = '<%=tAnswerStatus%>' ;

function initForm(){
	try{
	    div01.style.display="none";
		initLianswerInfo();
		initInpBox();
		//initEdom();
		initRiskGridBox();
		initAnswerGridBox();
		initIssueGridBox();
		initAnsweridGridBox();
		showCodeName();
	}
	catch(re){
		alert("RevisitDetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initLianswerInfo(){
	var strSQL4 = "select a.AnswerStatus,"
								       +" a.AnswerDate,"
								       +" a.AnswerTime,"
								       +" a.AnswerPerson,"
								       +" a.BookingDate,"
								       +" a.BookingTime,"
								       +" a.CloseReason,"
								       +" a.UrgencySign,"
								       +" a.Remark,"
								       +" a.UrgencySign"
							 +" from lianswerinfo a"
							 +" where a.answerid = '"+tAnswerID+"'"
							    +" and a.otherno = '"+tContNo+"'"
							    +" and appntno = '"+tAppntNo+"'";
	var strQueryResult4  = easyQueryVer3(strSQL4, 1, 0, 1);
			//判断是否查询成功
	if (!strQueryResult4) {
			return false;
	}
	else{							
		var arrSelected = decodeEasyQueryResult(strQueryResult4);
		fm.all("ResvisitState").value = arrSelected[0][0];
		if(fm.all("ResvisitState").value == '4')
		{
		   div01.style.display="";
		}
		//fm.all("AnswerDate").value = arrSelected[0][1];
		//fm.all("AnswerTime").value = arrSelected[0][2];
		//fm.all("AnswerPerson").value = arrSelected[0][3];
		fm.all("BookingDate").value = arrSelected[0][4];
	    fm.all("BookingTime").value = arrSelected[0][5];
		fm.all("CloseReason").value = arrSelected[0][6];
		if(arrSelected[0][7]==""){
			fm.all("UrgencySign").value="N";
		}else{
			fm.all("UrgencySign").value = arrSelected[0][7];
		}
		fm.all("Remark").value = arrSelected[0][8];
  }
}
function initInpBox(){
		fm.all("AnswerID").value = tAnswerID;			
	  fm.all("AnswerType").value = tAnswerType;	
	  fm.all("ShouldVisitDate").value = tShouldVisitDate;	
	  fm.all("ManageCom").value = tManageCom;	
	  fm.all("ContNo").value = tContNo;	
	  fm.all("CustomNo").value = tAppntNo;	
	  fm.all("Operator").value = tOperator;	
	  
	  
	  var strSQL="select a.appntname,"
							      +" (select codename from ldcode where codetype='sex' and code=a.appntsex),"
							      //+" trunc(months_between(sysdate, a.appntbirthday) / 12, 0),"
							      +" a.appntbirthday ,"
							      +" (select codename from ldcode where codetype='idtype' and code=a.idtype),"
							      +" a.idno,"
							      +" ( select b.PostalAddress   from lcaddress b where a.appntno=b.customerno and  a.addressno = b.addressno   )  ,"
							      +" ( select  b.ZipCode  from lcaddress b where a.appntno=b.customerno and  a.addressno = b.addressno   ) ,"
							      +"( select  b.phone   from lcaddress b where a.appntno=b.customerno and  a.addressno = b.addressno   )  ,"
							      +"( select b.Mobile from lcaddress b where a.appntno=b.customerno and  a.addressno = b.addressno   ),  "
							      +" a.AnswerPhoneType,(select codename from ldcode where code=a.AnswerPhoneType and codetype='revisitphonetype'),AnswerPhone "
						  +" from lcappnt a  "
							+" where 1=1"
								+" and a.appntno = '"+tAppntNo+"'"
								+" and a.contno = '"+tContNo+"'"
								;
		var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
			//判断是否查询成功
		if (!strQueryResult) {
			return false;
		}
		else{							
			var arrSelected = decodeEasyQueryResult(strQueryResult);
			fm.all("CSNM").value = arrSelected[0][0];
			fm.all("Sex").value = arrSelected[0][1];
			fm.all("Age").value = arrSelected[0][2];
			fm.all("IdType").value = arrSelected[0][3];
		  fm.all("IdNo").value = arrSelected[0][4];
			fm.all("PostalAddress").value = arrSelected[0][5];
			fm.all("ZipCode").value = arrSelected[0][6];
			fm.all("Phone").value = arrSelected[0][7];
			fm.all("TelPhone").value = arrSelected[0][8];
		    fm.all("RevisitPhoneType").value = arrSelected[0][9];
			fm.all("RevisitPhoneTypeName").value = arrSelected[0][10];
		    fm.all("RevisitPhone").value = arrSelected[0][11];
    }
    var strSQL2="select a.insuredno,"
							       +" a.name,"
					               +" (select codename from ldcode where codetype='sex' and code=a.sex),"
							       //+" trunc(months_between(sysdate, a.birthday) / 12, 0),"
							       +" a.birthday ,"
					               +" (select codename from ldcode where codetype='idtype' and code=a.idtype),"
							       +" a.idno"
							+" from lcinsured a"
							+" where a.contno = '"+tContNo+"'"
							   +" and a.appntno = '"+tAppntNo+"'"
							   +" and a.relationtomaininsured = '00'";
		var strQueryResult2  = easyQueryVer3(strSQL2, 1, 0, 1);
			//判断是否查询成功
	  if (!strQueryResult2) {
			return false;
		}
		else{							
			var arrSelected = decodeEasyQueryResult(strQueryResult2);
			fm.all("InsuredNo").value = arrSelected[0][0];
			fm.all("InsuredName").value = arrSelected[0][1];
			fm.all("InsuredSex").value = arrSelected[0][2];
			fm.all("InsuredAge").value = arrSelected[0][3];
			fm.all("InsuredIdType").value = arrSelected[0][4];
			fm.all("InsuredIdNo").value = arrSelected[0][5];
    }
		var strSQL3=" select a.PolApplyDate,"
							        +" a.signdate,"
							        +" (select makedate from lcpolprint where mainpolno = a.contno fetch first 1 rows only ),"
							        +" a.CustomGetPolDate,"
							        +" (select codename from ldcode where codetype='salechnl' and code=a.SaleChnl fetch first 1 rows only),"
							        +" a.AgentCode,"
							        +" (select name from laagent where agentcode = a.agentcode fetch first 1 rows only),"
							        +" a.bankcode,"
							        +" (select bankname from ldbank where bankcode = a.bankcode and fetch first 1 rows only),"
							        +" a.BankAccNo ,"
							        +"(select c.name from laagent b,ldcom c where agentcode = a.agentcode and b.managecom=c.comcode fetch first 1 rows only)"
							 +" from v_bc_lccont a"
							 +" where a.contno = '"+tContNo+"'"
							    +" and a.appntno = '"+tAppntNo+"'";
		var strQueryResult3  = easyQueryVer3(strSQL3, 1, 0, 1);
			//判断是否查询成功
	  if (!strQueryResult3) {
			return false;
		}
		else{							
			var arrSelected = decodeEasyQueryResult(strQueryResult3);
			fm.all("PolApplyDate").value = arrSelected[0][0];
			fm.all("SignDate").value = arrSelected[0][1];
			fm.all("PolPringDate").value = arrSelected[0][2];
			fm.all("CustomGetPolDate").value = arrSelected[0][3];
			fm.all("SaleChnl").value = arrSelected[0][4];
			fm.all("AgentCode").value = arrSelected[0][5];
			fm.all("AgentName").value = arrSelected[0][6];
			fm.all("BankCode").value = arrSelected[0][7];
			fm.all("BankName").value = arrSelected[0][8];
			fm.all("BankAccNo").value = arrSelected[0][9];
			fm.all("AgentManageCom").value =  arrSelected[0][10];
    }
    			  
}

function initEdom(){
	var edomsql="select 1 "
             +" from lianswerinfo a"
						+" where a.AnswerStatus='1' and a.answerid = '"+tAnswerID+"'"
							  +" and a.otherno = '"+tContNo+"'"
							  +" and appntno = '"+tAppntNo+"'";
  var edom=easyExecSql(edomsql);
	if (edom=='1'||edom=="1"){
		fm.all("update").disabled = false;
	}
	else {
		fm.all("update").disabled = true;
	}			
}
function initRiskGridBox(){
	initRiskGrid();
	var strSQL = "select a.riskcode,"
						        +" (select riskname from lmrisk where riskcode = a.riskcode),"
						        +" a.prem,"
						        +" a.amnt,"
						        +" (case"
						          +" when a.payendyearflag = 'M' then"
						             +"  a.payendyear / 12"
						          +" when a.payendyearflag = 'D' then"
						             +"  a.payendyear / 365"
						          +" else"
						             +"  a.payendyear"
						          +" end),"
						        +" (case"
						          +" when a.InsuYearFlag = 'M' then"
						             +"  a.InsuYear / 12"
						          +" when a.InsuYearFlag = 'D' then"
						             +"  a.InsuYear / 365"
						          +" else"
						             +"  a.InsuYear"
						          +" end)"
						      +" from lcpol a"
						      +" where a.contno = '"+tContNo+"'";
	turnPage.queryModal(strSQL, RiskGrid,1,1);	
}
function initRiskGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="险种编码";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="险种名称";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="保额";
		iArray[3][1]="30px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="保费";
		iArray[4][1]="30px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="缴费期间";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="保险期间";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;


		RiskGrid = new MulLineEnter( "fm" , "RiskGrid" ); 

		RiskGrid.mulLineCount=1;
		RiskGrid.displayTitle=1;
		RiskGrid.canSel=0;
		RiskGrid.canChk=0;
		RiskGrid.hiddenPlus=1;
		RiskGrid.hiddenSubtraction=1;

		RiskGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initAnswerGridBox(){
	 initAnswerGrid();
	 var strSQL = " select a.AnswerPerson,(select username from lduser where usercode=a.AnswerPerson),"
							        +" a.AnswerDate,"
							        +" (select codename from ldcode where code = a.AnswerStatus and codetype='answerstatus'),"
							        +" (select codename from ldcode where code = a.CloseReason and codetype='CloseReason'),"
							        +" a.Remark"
							 +" from LIAnswerInfoTrace a "
							 +" where a.tracetype='02' and a.AnswerID = '"+tAnswerID+"' order by SerialNo";			    
							    
   turnPage2.queryModal(strSQL, AnswerGrid,1,1);	
}
function initAnswerGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="回访人编号";
		iArray[1][1]="75px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="回访人姓名";
		iArray[2][1]="75px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="回访时间";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="回访结果";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="原因";
		iArray[5][1]="30px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="备注";
		iArray[6][1]="30px";
		iArray[6][2]=100;
		iArray[6][3]=0;


		AnswerGrid = new MulLineEnter( "fm" , "AnswerGrid" ); 

		AnswerGrid.mulLineCount=1;
		AnswerGrid.displayTitle=1;
		AnswerGrid.canSel=0;
		AnswerGrid.canChk=0;
		AnswerGrid.hiddenPlus=1;
		AnswerGrid.hiddenSubtraction=1;

		AnswerGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
function initIssueGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="问题件类型";
		iArray[1][1]="75px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="问题件类型";
		iArray[2][1]="100px";
		iArray[2][2]=100;
		iArray[2][3]=3;
		
		iArray[3]=new Array();
		iArray[3][0]="问题件类型";
		iArray[3][1]="100px";
		iArray[3][2]=100;
		iArray[3][3]=3;

		IssueGrid = new MulLineEnter( "fm" , "IssueGrid" ); 

		IssueGrid.mulLineCount=10;
		IssueGrid.displayTitle=1;
		IssueGrid.canSel=0;
		IssueGrid.canChk=1;
		IssueGrid.hiddenPlus=1;
		IssueGrid.hiddenSubtraction=1;
		IssueGrid.loadMulLine(iArray);
		

	}
	catch(ex){
		alert(ex);
	}
}
function initIssueGridBox(){
   initIssueGrid();
   var strSQL = "select a.codename,"
							       +" a.codetype,"
							       +" a.code"
							+" from ldcode a"
							+" where codetype = 'issuetype'";
							
   turnPage3.queryModal(strSQL, IssueGrid,1,1);	
   
  
    
}
function initAnsweridGridBox(){
	  initAnsweridGrid();
    var strSQL = " select a.answerid,"
						           +" b.contno,"
						           +" b.PolApplyDate,"
						           +" b.signdate,"
						           +" b.Prem,"
						           +" (select d.riskname"
						                  +" from lcpol c,lmrisk d"
						                  +" where c.polno = c.mainpolno and c.riskcode=d.riskcode"
						                  +"  and c.contno = b.contno  fetch first 1 rows only ),"
						           +"(select c.amnt"
						                  +" from lcpol c"
						                  +" where c.polno = c.mainpolno"
						                  +"  and c.contno = b.contno  fetch first 1 rows only)"
						    +" from lianswerinfo a, v_bc_lccont b"
						    +" where a.otherno = b.contno"
						      +"  and a.appntno = '"+tAppntNo+"'";
		turnPage4.queryModal(strSQL, AnsweridGrid,1,1);	
}
function initAnsweridGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="0px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="回访号";
		iArray[1][1]="45px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="保单合同号";
		iArray[2][1]="75px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="投保日期";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="承保日期";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="折扣后保费";
		iArray[5][1]="75px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="第一主险";
		iArray[6][1]="80px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="第一主险保额";
		iArray[7][1]="90px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		AnsweridGrid = new MulLineEnter( "fm" , "AnsweridGrid" ); 

		AnsweridGrid.mulLineCount=1;
		AnsweridGrid.displayTitle=1;
		AnsweridGrid.canSel=0;
		AnsweridGrid.canChk=0;
		AnsweridGrid.hiddenPlus=1;
		AnsweridGrid.hiddenSubtraction=1;

		AnsweridGrid.loadMulLine(iArray);
	}
	catch(ex){
		alert(ex);
	}
}
</script>
