    <!-- 调用 JSP Init 初始化页面 : 开始 -->


 <script language="JavaScript">


function initForm(){
  try{   
    initRevisitGrid();
  }
  catch (ex){
    alert("在 RevisitQueryInit.jsp --> initForm 函数中发生异常: 初始化界面错误！");
  }
}

function initRevisitGrid(){
   var iArray = new Array();                           //总数组, 返回给 MultiLine 表格
   try{
      iArray[0] = new Array();
      iArray[0][0] = "序号";                          //列名(顺序号, 无意义)
      iArray[0][1] = "30px";                          //列宽
      iArray[0][2] = 30;                              //列最大值
      iArray[0][3] = 0;
                                               //是否允许输入: 0表示不允许; 1表示允许  
      iArray[1] = new Array();
      iArray[1][0] = "回访ID";
      iArray[1][1] = "100px";
      iArray[1][2] = 100;
      iArray[1][3] = 0;
      iArray[1][21] = 3;
                
      iArray[2] = new Array();
      iArray[2][0] = "回访类型";
      iArray[2][1] = "70px";
      iArray[2][2] = 100;
      iArray[2][3] = 0;
      iArray[2][21] = 3;

      iArray[3] = new Array();
      iArray[3][0] = "客户ID";
      iArray[3][1] = "80px";
      iArray[3][2] = 100;
      iArray[3][3] = 0;
      iArray[3][21] = 2;              
                
      iArray[4] = new Array();
      iArray[4][0] = "客户姓名";
      iArray[4][1] = "60px";
      iArray[4][2] = 100;
      iArray[4][3] = 0;
      iArray[4][21] = 3;
                
      iArray[5] = new Array();
      iArray[5][0] = "保单ID";
      iArray[5][1] = "100px";
      iArray[5][2] = 100;
      iArray[5][3] = 0;
      iArray[5][21] = 3;
                
      iArray[6] = new Array();
      iArray[6][0] = "应访日期";
      iArray[6][1] = "70px";
      iArray[6][2] = 100;
      iArray[6][3] = 0;
      iArray[6][21] = 3;
                
      iArray[7] = new Array();
      iArray[7][0] = "预约时间";
      iArray[7][1] = "100px";
      iArray[7][2] = 100;
      iArray[7][3] = 0;
      iArray[7][21] = 3;
                
      iArray[8] = new Array();
      iArray[8][0] = "回访状态";
      iArray[8][1] = "50px";
      iArray[8][2] = 100;
      iArray[8][3] = 0;
                
      iArray[9] = new Array();
      iArray[9][0] = "回访时间";
      iArray[9][1] = "70px";
      iArray[9][2] = 100;
      iArray[9][3] = 0;
             
      iArray[10] = new Array();
      iArray[10][0] = "回访人员";
      iArray[10][1] = "50px";
      iArray[10][2] = 100;
      iArray[10][3] = 0;               
                
      iArray[11] = new Array();
      iArray[11][0] = "回访方式";
      iArray[11][1] = "50px";
      iArray[11][2] = 100;
      iArray[11][3] = 0;
      
      iArray[12] = new Array();
      iArray[12][0] = "回访状态";
      iArray[12][1] = "0px";
      iArray[12][2] = 100;
      iArray[12][3] = 3;
      
      iArray[13]=new Array();
	  iArray[13][0]="回访时段";
	  iArray[13][1]="70px";
	  iArray[13][2]=100;
	  iArray[13][3]=0;
                
   }
   catch (ex){
      alert("在 RevisitQueryInit.jsp --> initRevisitGrid 函数中发生异常: 初始化数组错误！");
   }
   try{
      RevisitGrid = new MulLineEnter("fm", "RevisitGrid");
      RevisitGrid.mulLineCount = 0;
      RevisitGrid.displayTitle = 1;
      RevisitGrid.locked = 1;
      RevisitGrid.hiddenPlus = 1;
      RevisitGrid.hiddenSubtraction = 1;
      RevisitGrid.canChk = 0;
      RevisitGrid.canSel = 1;
      RevisitGrid.chkBoxEventFuncName = "";
      RevisitGrid.selBoxEventFuncName = "intoRedealDet";
     //上面属性必须在 MulLineEnter loadMulLine 之前
      RevisitGrid.loadMulLine(iArray);
   }
   catch (ex){
      alert("在 RevisitQueryInit.jsp --> initRevisitGrid 函数中发生异常: 初始化界面错误！");
   }
}
</script>


