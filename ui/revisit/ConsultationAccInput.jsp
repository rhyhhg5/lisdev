<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：ConsultationAccInput.jsp
 //程序功能：咨诉受理
 //创建日期：2009-8-6
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<% GlobalInput tGI = new GlobalInput();
	 tGI = (GlobalInput) session.getValue("GI");
%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="ConsultationAcc.js"></SCRIPT>
  <%@include file="ConsultationAccInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form  method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >咨诉受理</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">来电号码</td>
    <td class="input">
    	<input class="common" name="TelephoneNo" elementtype="nacessary" verify="来电号码|notnull" >
    </td>      
    <td class="title">是否后续处理</td>
    <td class="input">
    	<input class="codeno" name="NeedDealFlag" verify="是否后续处理|notnull" ondblclick="return showCodeList('needdealflag',[this,NeedDealFlagName],[0,1]);" onkeyup="return showCodeListKey('needdealflag',[this,NeedDealFlagName],[0,1]);" ><input class="codename" name="NeedDealFlagName" elementtype="nacessary" readonly>
    </td>  
  </tr>
<tr class="common">
    <td class="title">客户姓名</td>
    <td class="input">
    	<input class="common" name="CustomerName" >
    </td>      
    <td class="title">问题件类型</td>
    <td class="input">
    	<input class="codeno" name="IssueType" ondblclick="return showCodeList('complaintissuetype',[this,IssueTypeName],[0,1]);" onkeyup="return showCodeListKey('complaintissuetype',[this,IssueTypeName],[0,1]);" ><input class="codename" name="IssueTypeName" readonly>
    </td>  
  </tr>
<tr class="common">
    <td class="title">咨诉类型</td>
    <td class="input">
    	<input class="codeno" name="ComplaintType" verify="咨诉类型|notnull" ondblclick="return showCodeList('complainttype',[this,ComplaintTypeName],[0,1]);" onkeyup="return showCodeListKey('complainttype',[this,ComplaintTypeName],[0,1]);" ><input class="codename" name="ComplaintTypeName" elementtype="nacessary"  readonly >
    </td>      
    <td class="title">转交机构</td>
    <td class="input">
    	<!--<input class="codeno" name="TransmitCom"  ondblclick="return showCodeList('Consultation_station',[this,TransmitComName],[0,1]);" onkeyup="return showCodeListKey('Consultation_station',[this,TransmitComName],[0,1]);" ><input class="codename" name="TransmitComName" readonly>-->
    	<Input class="codeno" name=TransmitCom ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);"><input name=ManageComName class=codename readonly=true>
    </td>    
    <td class="title">特急案件标识</td>
    <td class="input">
    	<input class="code" name="UrgencySign" ondblclick="return showCodeList('urgencysign',[this],[0]);" onkeyup="return showCodeListKey('urgencysign',[this],[0]);" >
    </td>      
  </tr>
<tr class="common">
    <td class="title">保单合同号</td>
    <td class="input">
    	<input class="common" name="OtherNo" >
    </td>      
    <td class="title">承办日期</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="UndertakeDate" >
    </td>  
    <td class="title">承办时间(HH:MM)</td>
    <td class="input">
    	<input class="common" name="UndertakeTime" >
    </td>  
  </tr>
<tr class="common">
    <td class="title">客户类型</td>
    <td class="input">
    	<input class="codeno" name="CustomerNo" verify="客户类型|notnull" ondblclick="return showCodeList('customertype2',[this,CustomerNoName],[0,1]);" onkeyup="return showCodeListKey('customertype2',[this,CustomerNoName],[0,1]);" nextcasing=><input class="codename" name="CustomerNoName" elementtype="nacessary" readonly >
    </td>      
    <td class="title">客户联系方式</td>
    <td class="input">
    	<input class="common" name="CustomerAddress" >
    </td>  
  </tr>
<tr class="common">
    <td class="title">来电机构</td>
    <td class="input">
    	<input class="common" name="TelephoneCom" elementtype="nacessary" verify="来电机构|notnull" >
    </td>      
    <td class="title">预约日期</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="BookingDate" >
    </td>
    <td class="title">预约时间(HH:MM)</td>  
    <td class="input">
    	<input class="common" name="BookingTime" >
    </td>  
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >咨诉内容</td>
  </tr>
</table>
<textarea class="common" name="ComplaintContent" cols="100%" rows="2" value="1000字" ></textarea><BR>
<input value="完  成" type=button  onclick="submitForm()" class="cssButton" name="insert" >
<br><br>


</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
