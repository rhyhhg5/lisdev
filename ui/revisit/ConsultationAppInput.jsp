<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：ConsultationAppInput.jsp
 //程序功能：咨诉任务申请
 //创建日期：2009-8-6
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	%>
<script>
  var operator = "<%=tGI.Operator%>";   //记录操作员
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
</script>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
 <SCRIPT src="ConsultationApp.js"></SCRIPT>
 <%@include file="ConsultationAppInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./ConsultationAppSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >咨诉任务查询</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">机构</td>
    <td class="input">
    	<Input class="codeno" name=TransmitCom ondblclick="return showCodeList('station',[this,TransmitComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,TransmitComName],[0,1]);"><input name=TransmitComName class=codename readonly=true>
    </td>      
    <td class="title">问题件类型</td>
    <td class="input">
    	<input class="codeno" name="IssueType" ondblclick="return showCodeList('complaintissuetype',[this,IssueTypeName],[0,1]);" onkeyup="return showCodeListKey('complaintissuetype',[this,IssueTypeName],[0,1]);" ><input class="codename" name="IssueTypeName" readonly>
    </td>      
    <td class="title">客户姓名</td>
    <td class="input">
    	<input class="common" name="CustomerName" >
    </td>  
  </tr>
<tr class="common">
    <td class="title">保单合同号</td>
    <td class="input">
    	<input class="common" name="OtherNo" >
    </td>      
    <td class="title">转交时间</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="TransmitDate" >
    </td>      
    <td class="title">预约时间</td>
    <td class="input">
    	<input class="coolDatePicker" dateFormat="short" name="BookingDate" >
    </td>  
  </tr>
</TABLE>
<input value="查  询" type=button  onclick="queryclick()" class="cssButton" type="button" >
<br><br>
<table>
  <tr>
    <td class="titleImg" >咨诉任务公共池</td>
  </tr>
</table>
				<table  class= common>
				   <tr  class= common>
				      <td text-align: left colSpan=1>
				     <span id="spanComplaintGrid" >
				     </span> 
				      </td>
				   </tr>
				</table>
						<div id="divTurnPageComplaintGrid" style="display:'none'" align="center" >
                <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" />
                <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" />
                <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" />
                <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" />
            </div>

<input value="申  请" type=button  onclick="queryupdate()" class="cssButton" type="button" >
<br><br>
<table>
  <tr>
    <td class="titleImg" >咨诉任务个人池</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanComplainttwoGrid" >
     </span> 
      </td>
   </tr>
</table>



</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
