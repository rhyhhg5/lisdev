//<!-- JavaScript Document BGN -->


/*============================================================================*/

var showInfo;                                           //全局变量, 弹出提示窗口, 必须有
var turnPage = new turnPageClass();                     //全局变量, 查询结果翻页, 必须有
var turnPageRevisitGrid = new turnPageClass();    //全局变量, 可选上报报文查询结果翻页
 

/**
 * 查询回访信息
 */
function queryRevisitGrid(){
	var managecom=fm.all("ManageCom").value;
  var QuerySQL;
	QuerySQL = "select a.answerid,"
				          +" (select codename from ldcode where codetype = 'answertype' and trim(code) = a.answertype),"
				          +" a.appntno,"
				          +" a.appntname,"
				          +" a.otherno,"
				          +" a.shouldvisitdate,"
		                  +"a.Bookingdate,"
				          +" (select codename from ldcode where codetype='answerstatus' and code=a.answerstatus),"
				          +" a.answerdate,"
				          +" a.answerperson,"
				          +" (select codename from ldcode where codetype='takemode' and code=a.takemode),"
				          +" a.answerstatus, "
				          +" (select codename from ldcode where codetype='revisitime' and code=(select AnswerRemark from LCAppnt where contno=a.OtherNo and AppntNo=a.AppntNo)) as revisitime "
				   +" from LIAnswerInfo a"
				   +" where 1 = 1 and a.managecom like '"+comcode+"%' "
                 + getWherePart("a.SaleChnl", "SaleChnlDetail")
                 + getWherePart("a.AgentCode", "AgentCode")
                 + getWherePart("a.RiskCode", "RiskCode")
                 + getWherePart("a.AnswerStatus", "ResvisitState")
                 + getWherePart("a.AppntName", "CustomName")
                 + getWherePart("a.OtherNo", "ContNo")
                 + getWherePart("a.AnswerPerson", "AnswerPerson")
                 + getWherePart("a.ShouldVisitDate", "ShouldVisitStartDate", ">=")
                 + getWherePart("a.ShouldVisitDate", "ShouldVisitEndDate", "<=")
                 + getWherePart("a.AnswerDate", "AnswerDate")
                 + getWherePart("a.AnswerType", "AnswerType")
                 + " and a.ManageCom like'"+managecom+"%' "
           + " order by a.ShouldVisitDate,a.AppntNo,a.managecom,a.BookingDate,a.Bookingtime,revisitime ";
  try{
     turnPageRevisitGrid.pageDivName = "divTurnPageRevisitGrid";
     turnPageRevisitGrid.queryModal(QuerySQL, RevisitGrid,0,1);     
  }
  catch (ex){
     alert("警告：查询回访信息出现异常！ ");
  }
}

 function resetForm(){
  fm.all("ManageCom").value = "";
  fm.all("SaleChnlDetail").value = "";
  fm.all("AgentCode").value = "";
  fm.all("RiskCode").value = "";  
  fm.all("ResvisitState").value = "";
  fm.all("CustomName").value = "";  
  fm.all("ContNo").value = "";
  fm.all("AnswerPerson").value = ""; 
  fm.all("ShouldVisitStartDate").value = "";
  fm.all("ShouldVisitEndDate").value = "";  
  fm.all("AnswerDate").value = "";
  fm.all("AnswerType").value = "";
  fm.all("ManageComName").value = ""; 
  fm.all("SaleChnlDetailName").value = "";
  fm.all("ResvisitStateName").value = "";  
  fm.all("RiskCodeName").value = "";
  fm.all("AnswerTypeName").value = "";          
	initForm();
	return true;		
}

//进入转办处理页面
function intoRedealDet(){
  
	var i = 0;
  var checkFlag = 0;
  for (i=0; i<RevisitGrid.mulLineCount; i++){
    if (RevisitGrid.getSelNo(i)) { 
      checkFlag = RevisitGrid.getSelNo();
      break;
    }
  }

	var tAnswerID = RevisitGrid.getRowColData(checkFlag - 1, 1); 
	var tAppntNo = RevisitGrid.getRowColData(checkFlag - 1, 3); 
	var tOtherNo = RevisitGrid.getRowColData(checkFlag - 1, 5); 
	 
	var tMissionID ; //= RevisitGrid.getRowColData(checkFlag - 1, 10);
	var viewFlag =1;
	var tAnswerStatus = RevisitGrid.getRowColData(checkFlag - 1, 12);
	window.open("./RedealDetInputToMainQuery.jsp?AnswerID="+ tAnswerID +"&AppntNo=" + tAppntNo 
		+ "&OtherNo=" + tOtherNo + "&AnswerStatus=" + tAnswerStatus + "&viewFlag=" + viewFlag);
		 
}

