//该文件中包含客户端需要处理的函数和事件

//程序名称：ConsultationDeal.js
//程序功能：咨诉任务处理
//创建日期：2009-8-6
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initForm();
  }
  top.opener.queryclick2();
  top.close();
}

function submitForm(){
	if( verifyInput2() == false ) return false;
  var tBookingDate=fm.all("BookingDate1").value;
	var tBookingTime=fm.all("BookingTime").value;
	if(tBookingDate==""){
   	if(!(tBookingTime=="")){
	   	 alert("预约日期和预约时间必须同时录入");
	   	 return false;	
   	}
  }
  else{
   	if(tBookingTime==""){
	   	 alert("预约日期和预约时间必须同时录入");
	   	 return false;	
   	}
  }
  if(tBookingTime==""){
  }
  else{
	  if(checkV(tBookingTime)==false){
		  fm.BookingTime.focus(); 
		  return false;
		}
  }
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./ConsultationDealSave.jsp";
	fm.submit();
}

function checkV(temp_str){
	if(temp_str.length != 5){
	  alert("输入格式有误。");
	  return false;
	}
	var hour_s = temp_str.substr(0,2);
	var split1_s = temp_str.substr(2,1);
	var min_s = temp_str.substr(3,2);
	if(!(split1_s==":")){
	  alert("输入格式有误。");
	  return false;
	}
	if(!(hour_s>=0&&hour_s<24&&min_s>=0&&min_s<60)){
	  alert("输入时间有误。");
	  return false;
	}
}
function GoToReturn(){
	try{
		top.opener.queryclick2();
	}
	catch(ex){
		alert( "没有发现父窗口的afterQuery接口。" + ex );
	}
	top.opener.queryclick2();
	top.close();
}


function viewFlag()//若是查看状态，所有按纽置灰
{ 
   if(null!=tviewFlag&&tviewFlag ==2)
        fm.update.disabled = true;
 
}