//程序名称：UWApp.js
//程序功能：既往投保信息查询
//创建日期：2002-06-19 11:10:36
//创建人  ： WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var tPOLNO = "";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var cflag = "1" //操作位置　1.以往投保信息查询

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

                   

 
                     

           

    

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

 

// 既往险种保单信息查询
function easyQueryClick()
{
    
	var strSQL = "";
	 
	 
	var insureno = fm.all('InsureNoHide').value;
	var appntno = fm.all('AppntNoHide').value;
	
  var tSel = ContGrid.getSelNo();
  if( tSel == 0 || tSel == null )
  	alert( "请先选择一条记录，再点击明细查询按钮。" );
  else
  {
			var cContno=ContGrid.getRowColData(tSel-1,1);
			var cPrtNo=ContGrid.getRowColData(tSel-1,2);
  }
	
  strSQL = " select p.riskcode , (select  a.riskname  from lmriskapp a where  a.riskcode = p.riskcode) ,p.insuredno , p.amnt ,p.prem , p.payyears ,p.years from lcpol p where p.contno= '"+cContno+"' 	 " ; 	 
	 
	turnPage1.queryModal(strSQL, PolGrid);
	 
     return true;	
     
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		alert("result:"+arrResult);
	} // end of if
}

/*
function ChoClick(parm1,parm2)
{
	if(fm.all(parm1).all('InpPolGridSel').value == '1' )
	{
	//当前行第1列的值设为：选中
   		fm.ProposalNoHide.value = fm.all(parm1).all('PolGrid2').value;
  	}
}
*/
/*********************************************************************
 *  初始化合同信息
 *  参数  ：  无
 *  返回值：  无 modify by zhuwen
 *********************************************************************
 */	
function initContInfo(){
	var Sql_cont = "select ProPosalContNo,PrtNo from v_bc_lccont where contno='"+fm.all('ContNoHide').value+"'";
	var arrResult=easyExecSql(Sql_cont,1,0);
	if(arrResult!=null){
		fm.all('ProPosalContNo').value = arrResult[0][0];	
		fm.all('PrtNo').value = arrResult[0][1];	
	}
}

/*********************************************************************
 *  查询个人合同信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */	
function queryCont(tApproveflag)
{
	// 书写SQL语句
	//modify by zhuwen 添加一个方法查询对应的ProposalContNo
	initContInfo();
	//alert("test");
	var strSQL = "";
	var i, j, m, n;
	var contno = fm.all('ContNoHide').value;
	var insureno = fm.all('InsureNoHide').value;
	var appntno = fm.all('AppntNoHide').value;
	//modify by zhuwen
	var initProPosalContNo = fm.all('ProPosalContNo').value;
	//end of modify
 
		strSQL = " ( select c.contno , c.prtno , c.appntno , c.polapplydate , c.cvalidate , " +
		            " d.AnswerDate ," +
                    " (select codename from ldcode where codetype='answerstatus' and code=d.AnswerStatus)"+
		            "  from v_bc_lccont c,LIAnswerInfo d  where c.contno=d.OtherNo and c.appntno='"+appntno+"' )  union " +
		            " ( select c.contno , c.prtno , c.appntno , c.polapplydate , c.cvalidate , " +
		            " d.AnswerDate," +
                    " (select codename from ldcode where codetype='answerstatus' and code=d.AnswerStatus)"+
		            "  from v_bc_lccont c,LIAnswerInfo d  where c.contno=d.OtherNo and exists (select p.contno from lcpol p where p.contno = c.contno and  p.insuredno ='"+appntno+"' )  )  ";
	turnPage2.queryModal(strSQL, ContGrid,0,1);
  return true;	
}

 

 
 

 