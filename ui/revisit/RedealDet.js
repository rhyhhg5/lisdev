//该文件中包含客户端需要处理的函数和事件

//程序名称：RedealDet.js
//程序功能：转办任务处理
//创建日期：2009-8-4
//创建人  ：liwb
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
 


//完  成按钮
function submitForm(){
	if( verifyInput2() == false ) return false;
	if(fm.RevisitPhoneType.value !='')
	{
	   if(fm.RevisitPhone.value == '')
	   {  
	      alert("你选择了回访电话类型，回访电话不能为空！");
	      return false;
	   }
	}
	if(!beforeSubmit()) {
		return false;
	}
	
	var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

//返  回按钮
function back(){
	top.close();
}


//提交前的校验、计算  
function beforeSubmit(){
  //问题件
  if(tAnswerStatus == "2")
  {	
	     var sql ="select * from LIIssuePol p where p.AnswerID ='"+tAnswerID+"' and p.IssueType in ('15' ,'16')  " ;
	     var arrResult=easyExecSql(sql,1,0);
	   
	     if(null==arrResult)//问题件类型不包含“ 方言件、 信函/上门回访”时
	     {
	       if(fm.all("ResvisitState").value != "92"&&fm.all("ResvisitState").value != "94")
	       {
	         alert("问题件类型不包含 [方言件]、[信函/上门回访]　时,回访状态只能选择　[92-问题件处理失败]、[94-问题件处理成功]　！")
	         fm.ResvisitState.focus();
	         return false;
	       }
	     }else 
	     {
	       if(fm.all("ResvisitState").value != "93"&&fm.all("ResvisitState").value != "95"&&fm.all("ResvisitState").value != "98"&&fm.all("ResvisitState").value != "99")
	       {
	         alert("问题件类型包含 [方言件]、[信函/上门回访]　时 , 回访状态只能选择　[93-联系方式追踪失败]、[95-多次联系不到追踪失败]、[98-关闭]、[99-成功]　;　！")
	         fm.ResvisitState.focus();
	         return false;
	       }
	     
	     }
      
 }
	//联系方式有误
 else if(tAnswerStatus == "3")
 {
        if(fm.all("ResvisitState").value != "0"&&fm.all("ResvisitState").value != "4"&&fm.all("ResvisitState").value != "93"&&fm.all("ResvisitState").value != "98")
	       {
	         alert("联系方式有误转办,回访状态只能选择　[0-待回访]、[4-预约]、[93-联系方式追踪失败]、[98-关闭]　！")
	         fm.ResvisitState.focus();
	         return false;
	       }
 
    
		if(fm.Remark.value == "" || fm.Remark.value == null){
			alert("请录入回访备注！");
			return false;
		}
		//if(fm.TakeMode.value == "" || fm.TakeMode.value == null){
		//	alert("请录入处理方式！");
		//	return false;
		//}
  }
  else if(tAnswerStatus == "5")//多次联系不到转办
  {	
  		 if(fm.all("ResvisitState").value != "0"&&fm.all("ResvisitState").value != "4"&&fm.all("ResvisitState").value != "95"&&fm.all("ResvisitState").value != "98")
	       {
	         alert("联系方式有误转办,回访状态只能选择　[0-待回访]、[4-预约]、[95-多次联系不到追踪失败]、[98-关闭]　！")
	         fm.ResvisitState.focus();
	         return false;
	       }
	
  }
	if(fm.AnswerExplain.value == "" || fm.AnswerExplain.value == null){
		alert("请录入处理情况说明！");
		return false;
	}	
	
	if(fm.all("ResvisitState").value == "98")
	{
	   if(null==fm.all("CloseReason").value||fm.all("CloseReason").value=="")
	   {
	   		alert("请录入关闭原因！");
	   		fm.CloseReason.focus();
	   		return false ;
	   }
		
	}
	
   if(fm.all("ResvisitState").value == "99")
	{
	   if(null==fm.all("TakeMode").value||fm.all("TakeMode").value=="")
	   {
	   		alert("请录入处理方式！");
	   		fm.TakeMode.focus();
	   		return false ;
	   }
		
	}
	
	if(fm.all("ResvisitState").value == "4")
	{
	   if(null==fm.all("BookingDate").value||fm.all("BookingDate").value=="")
	   {
	   		alert("请录入预约日期！");
	   		fm.BookingDate.focus()
	   		return false ;
	   }
	   if(null==fm.all("BookingTime").value||fm.all("BookingTime").value=="")
	   {
	   		alert("请录入预约时间！");
	   		fm.BookingTime.focus()
	   		return false ;
	   }
	  if(checkV(fm.all("BookingTime").value)==false){
 		  fm.BookingTime.focus(); 
 	  return false;
	 }
		
	}
	return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
  
   var Sql_cont ="select * from  lwmission a where 1=1  and a.processid = '0000000010' and a.activityid = '0000003002' and  a.MissionProp4 ='"+tAppntNo+"'  and a.defaultoperator = '"+operator+"'  ";
    
    var arrResult=easyExecSql(Sql_cont,1,0);
    if(null!=arrResult)
    {
       alert(" 该客户在您的转办池中还有其他未处理保单，请处理！");
    }
  
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    top.opener.easyQueryClick();
    top.close();
  }
}


function afterCodeSelect(cCodeName, Field) 
{   var  state;
	
	if(cCodeName == "ResvisitState") 
	{    
	    fm.all("div2").style.display = "none";
		fm.all("div3").style.display = 'none';
		fm.all("div4").style.display = 'none';
		fm.all("div5").style.display = 'none';
	    state=fm.all('ResvisitState').value; 
			if(null!=state&&state=="99")
			{
				fm.all("div3").style.display = '';
			}
			if(null!=state&&state=="98")
			{
				fm.all("div4").style.display = '';
			}
			
			if(null!=state&&state=="4")
			{
				fm.all("div5").style.display = '';
			}
			if(tAnswerStatus=="2")
			{
			  div2.style.display="";
			}
			
	}
	if(cCodeName == "RevisitPhoneType") 
	{   
	
	    var RevisitPhoneType = fm.RevisitPhoneType.value;
		if(RevisitPhoneType=="0")
		{
			if(fm.TelPhone.value == "")
			{
				alert("您选择了同手机号码，但是手机号码为空，请重新选择电话回访类型！");
				fm.RevisitPhoneType.value = '';
				fm.RevisitPhoneTypeName.value = '';
				fm.RevisitPhone.value = '';
			}
			else
			{
				fm.RevisitPhone.value = fm.TelPhone.value;
				document.getElementById("RevisitPhone").readOnly = true;

			}
		}
		else if(RevisitPhoneType=="1")
		{
			if(fm.Phone.value == "" || fm.Phone.value == "")
			{
				alert("您选择了同固定电话，但是固定电话号码为空，请重新选择电话回访类型！");
				fm.RevisitPhoneType.value = '';
				fm.RevisitPhoneTypeName.value = '';
				fm.RevisitPhone.value = '';
			}
			else
			{
				fm.RevisitPhone.value = fm.Phone.value;	
				document.getElementById("RevisitPhone").readOnly = true;			
			}
		}
		else
		{
			fm.RevisitPhone.value = "";	 
			document.getElementById("RevisitPhone").readOnly = false;
		}
	}
}

//界面显示
function initDiv(){
    
	if(tAnswerStatus == "2"){
		div2.style.display="";
	}
	if(tAnswerStatus=="99")
	{
		fm.all("div3").style.display = "";
	}
	if(tAnswerStatus=="98")
	{
	    fm.all("div4").style.display = "";
	}
	if(tAnswerStatus=="4")
	{
		fm.all("div5").style.display = "";
	}
}


function initInputBox(){
	var tSql = "";
	var arrResult = "";
	//保单信息
	tSql = "select a.ContNo, "
	            +" a.PolApplyDate,"
	            +" a.SignDate,"
	            +" '', "
	            +" a.CustomGetPolDate,"
	            +" (select codename from ldcode where codetype='salechnl' and code=a.SaleChnl),"
	            +" a.AgentCode, "
			        +  "(select Name from laagent where agentcode = a.AgentCode), "
			        +  "(select Name from lacom where agentcom = a.AgentCom), "
			        +  "(select BankName from ldbank where bankcode = a.BankCode and comcode = a.ManageCom), "
			        +  "a.BankAccNo ,"
			        +  "a.PayMode ,"
			        +"(select c.name from LAAgent b,ldcom c where  b.AgentCode = a.AgentCode and b.managecom=c.comcode) "			        
			 + "from v_bc_lccont a "
			 + "where a.contno = '" + tOtherNo + "'";	
	arrResult = easyExecSql(tSql);
	if (arrResult != null){
		fm.ContNo.value = arrResult[0][0];
		fm.PolApplyDate.value = arrResult[0][1];
		fm.SignDate.value = arrResult[0][2];
		fm.PrintDate.value= arrResult[0][3];
		fm.CustomGetPolDate.value = arrResult[0][4];
		fm.SaleChnl.value = arrResult[0][5];
		fm.AgentCode.value= arrResult[0][6];
		fm.AgentName.value = arrResult[0][7];
		fm.AgentCom.value= arrResult[0][8];
		if(arrResult[0][11] == "4"){
			fm.BankCode.value = arrResult[0][9];
			fm.BankAccNo.value = arrResult[0][10];
		}
		else{
			fm.BankCode.value = "";
			fm.BankAccNo.value = "";
		}
		fm.AppManageCom.value= arrResult[0][12];
	}
	//保单打印时间
	tSql = "select a.MakeDate "
			 + "from lcpolprint a "
			 + "where a.mainpolno = '" + tOtherNo + "'";	
	arrResult = easyExecSql(tSql);
	if (arrResult != null){
		fm.PrintDate.value = arrResult[0][0];
	}
	//查询回访基本资料
  tSql = "select a.AnswerID, "
               + "(case a.AnswerType when '1' then '新契约回访' else '' end), " 
               + "a.ShouldVisitDate,"
               + "(select name from ldcom where comcode=a.ManageCom),a.Remark,a.AnswerStatus, "
               +" (select codename from ldcode where code=a.AnswerStatus and codetype='answerstatus'),"
               +" (select codename from ldcode where code=a.UrgencySign and codetype='yesno'),AnswerExplain,BookingDate,BookingTime,TakeMode, "
               +" (select codename from ldcode where code=a.TakeMode and codetype='takemode'),CloseReason,"
               +" (select codename from ldcode where code=a.CloseReason and codetype='closereason') "
			 + " from lianswerinfo a "
			 + "where a.answerid = '" + tAnswerID + "'"
			 + "   and a.otherno = '" + tOtherNo + "'"
			 + "   and a.appntno = '" + tAppntNo + "'";
	arrResult = easyExecSql(tSql);
	if (arrResult != null){
		fm.AnswerID.value = arrResult[0][0];
		fm.AnswerType.value = arrResult[0][1];
		fm.ShouldVisitDate.value = arrResult[0][2];
		fm.ManageCom.value = arrResult[0][3];
		fm.Remark.value = arrResult[0][4];
		fm.ResvisitState.value = arrResult[0][5];
		fm.ResvisitStateName.value = arrResult[0][6];
		fm.UrgencySign.value = arrResult[0][7];
	    fm.AnswerExplain.value = arrResult[0][8];
	   	fm.BookingDate.value = arrResult[0][9];
	    fm.BookingTime.value = arrResult[0][10];
	    fm.TakeMode.value = arrResult[0][11];
	    fm.TakeModeName.value = arrResult[0][12];
	   	fm.CloseReason.value = arrResult[0][13];
	   	fm.CloseReasonName.value = arrResult[0][14];
	}
	//投保人信息
  tSql = "select a.AppntNo,"
              +" a.AppntName, "
  		        + "(select CodeName from ldcode where codetype = 'sex' and code = a.AppntSex), " 
  		        + "(select CodeName from ldcode where codetype = 'idtype' and code = a.IDType), "
  		        + "a.IDNo,"
  		        + "a.AppntBirthday, "
  		        + "( select  b.PostalAddress  from lcaddress b where a.appntno=b.customerno and  a.addressno = b.addressno   ), "
  		        + "( select  b.ZipCode  from lcaddress b where a.appntno=b.customerno and  a.addressno = b.addressno   ), "
                +"( select  b.phone   from lcaddress b where a.appntno=b.customerno and  a.addressno = b.addressno   )  ,"
	            +"( select b.Mobile from lcaddress b where a.appntno=b.customerno and  a.addressno = b.addressno   ),  "
			    +" a.AnswerPhoneType,(select codename from ldcode where code=a.AnswerPhoneType and codetype='revisitphonetype'),AnswerPhone "
			 + "from lcappnt a "
			 + "where a.contno = '" + tOtherNo + "'"
		 ;	
	arrResult = easyExecSql(tSql);
	if (arrResult != null){
		fm.AppntNo.value = arrResult[0][0];
		fm.AppntName.value = arrResult[0][1];
		fm.AppntSex.value = arrResult[0][2];
		//fm.AppntAge.value=calAgeNew(arrResult[0][5],fm.PolApplyDate.value);
	fm.AppntAge.value=arrResult[0][5];	//modify by fuxin
		fm.AppntIDType.value = arrResult[0][3];
		fm.AppntIDNo.value = arrResult[0][4];
	
		fm.Address.value = arrResult[0][6];
		fm.ZipCode.value = arrResult[0][7];
		fm.Phone.value = arrResult[0][8];
	    fm.TelPhone.value = arrResult[0][9];
	    fm.RevisitPhoneType.value = arrResult[0][10];
	    fm.RevisitPhoneTypeName.value = arrResult[0][11];
	    fm.RevisitPhone.value = arrResult[0][12];
	}
	//被保人信息	
  tSql = "select a.InsuredNo, "
              + "a.Name, "
  		        + "(select CodeName from ldcode where codetype = 'sex' and code = a.Sex), "
  		        + "(select CodeName from ldcode where codetype = 'idtype' and code = a.IDType), "
  		        + "a.IDNo, "
  		        + "a.Birthday "
			 + "from lcinsured a "
			 + "where a.contno = '" + tOtherNo + "'";	
	arrResult = easyExecSql(tSql);
	if (arrResult != null){
		fm.InsuredNo.value = arrResult[0][0];
		fm.InsuredName.value = arrResult[0][1];
		fm.InsuredSex.value = arrResult[0][2];
		//fm.InsuredAge.value=calAgeNew(arrResult[0][5],fm.PolApplyDate.value);
		fm.InsuredAge.value= arrResult[0][5] ; // modify by fuxin
		fm.InsuredIDType.value = arrResult[0][3];
		fm.InsuredIDNo.value = arrResult[0][4];
	}
	
	//为隐藏变量赋值
	fm.MissionID.value = tMissionID;
	fm.AnswerStatus.value = tAnswerStatus;
}	
//险种清单
function QueryPolGrid(){
  var tSql = "";
  tSql = "select a.RiskCode, "
			        + "(select RiskName from lmrisk where riskcode = a.riskcode), "
			        + "a.Prem,"
			        +" a.Amnt, "
			        + "(case "
			            + "when a.PayendYearFlag = 'M' then a.PayEndYear / 12 "
			            + "when a.PayendYearFlag = 'D' then a.PayEndYear / 365 "
			            + "else a.PayEndYear "
			            + "end), "
			        + "(case "
			            + "when a.InsuYearFlag = 'M' then a.InsuYear / 12 "
			            + "when a.InsuYearFlag = 'D' then a.InsuYear / 365 "
			            + "else a.InsuYear "
			            + "end) "
			 + "from lcpol a "
			 + "where a.contno = '" + tOtherNo + "'";
  turnPage.queryModal(tSql, PolGrid);
}	
//回访历程
function QueryRevisitCourseGrid(){
  var tSql = "";
  var tSql = " select a.AnswerPerson,(select username from lduser where usercode=a.AnswerPerson),"
							        +" a.AnswerDate ,"
							        +" (select codename from ldcode where code = a.AnswerStatus and codetype='answerstatus'),"
							        +" (select codename from ldcode where code = a.CloseReason and codetype='CloseReason'),"
							        +" a.Remark"
							 +" from LIAnswerInfoTrace a "
							 +" where a.tracetype='02' and a.AnswerID = '"+tAnswerID+"' order by SerialNo";		
  turnPage1.queryModal(tSql, RevisitCourseGrid);
}

//问题信息
function QueryAnswerGrid(){
  var tSql = "";
  tSql = "select a.IssueType, (select b.codename  from ldcode b  where b.codetype = 'issuetype' and b.code = a.IssueType),"
  		        + "(case when a.IssueStutas is null then '0' else a.IssueStutas end), "
  		        + "(case a.IssueStutas when '1' then '处理完成' else '未处理' end), "
  		        + "a.IssueExplain "
       + "from LIIssuePol a "
       + "where a.answerid = '" + tAnswerID + "' "
       + "order by a.IssueType";
       
       
   turnPage2.pageLineNum=17;       
  turnPage2.queryModal(tSql, AnswerGrid);
  
 
}
//同日转办关联保单
function QueryAnsweridGrid(){
  var tSql = "";
  tSql = "select a.AnswerID, "
              + "b.ContNo, "
              + "b.PolApplyDate, "
              + "b.SignDate, "
              + "b.Prem, "
			        + "(select d.Riskname from lcpol c,lmrisk d where c.riskcode=d.riskcode and c.polno = c.mainpolno and c.contno = b.contno), "
		          + "(select c.Amnt from lcpol c where c.polno = c.mainpolno and c.contno = b.contno) "
			 + "from lianswerinfo a, v_bc_lccont b "
			 + "where a.otherno = b.contno"
			 + "   and a.appntno = '" + tAppntNo + "'";
  turnPage3.queryModal(tSql, AnsweridGrid);
}


//既往投保信息
function showApp()
{
  
   if(null==tAppntNo||tAppntNo=="")
   {
   	  alert("投保人客户号码为空，请检查！");
   	  return false ;
   }
   window.open("./RevisitOldAppMain.jsp?ContNo="+tOtherNo+"&CustomerNo="+tAppntNo);

}


 


function viewFlag()//若是查看状态，所有按纽置灰
{ 
   if(null!=tviewFlag&&tviewFlag ==1)
        fm.submitButton.disabled = true;
 
}

//提取出相的回访状态
function selectstate()
{    
    var sql2 ="select AnswerStatus from LIAnswerInfo where AnswerID='"+fm.AnswerID.value+"'";
    var state =  easyExecSql(sql2);
    var sql = "";
    if(state == '2')
    {
      var sql ="select * from LIIssuePol p where p.AnswerID ='"+tAnswerID+"' and p.IssueType in ('15' ,'16')  " ;
	  var arrResult=easyExecSql(sql,1,0);
	  if(null==arrResult)//问题件类型不包含“ 方言件、 信函/上门回访”时
	  {
	      sql = "select code,codename from ldcode where codetype='answerstatus' and code in ('2','92','94')";
	  }else
	  { 
	      sql = "select code,codename from ldcode where codetype='answerstatus' and code in ('2','93','95','98','99')";
	  }
    } 
    else if(state == '3')
    {
      sql = "select code,codename from ldcode where codetype='answerstatus' and code in ('0','3','4','93','98')";
    }
    else if(state == '5')
    {
      sql = "select code,codename from ldcode where codetype='answerstatus' and code in ('0','4','5','95','98')";
    }
	var strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
	 fm.ResvisitState.CodeData=strQueryResult;
	 fm.ResvisitState.disabled=false;
}

function checkV(temp_str){
	if(temp_str.length != 5){
	  alert("预约时间输入格式有误。");
	  return false;
	}
	var hour_s = temp_str.substr(0,2);
	var split1_s = temp_str.substr(2,1);
	var min_s = temp_str.substr(3,2);
	if(!(split1_s==":")){
	  alert("预约时间输入格式有误。");
	  return false;
	}
	if(!(hour_s>=0&&hour_s<24&&min_s>=0&&min_s<60)){
	  alert("输入预约时间有误。");
	  return false;
	}
}