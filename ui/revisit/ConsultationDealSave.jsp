<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ConsultationDealSave.jsp
//程序功能：咨诉任务处理
//创建日期：2009-8-6
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.revisit.*"%>
<%
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String transact = "";

 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 System.out.println("123");  
 if (tG == null) {
	 	System.out.println("页面失效,请重新登陆");
	 	FlagStr = "Fail";
	 	Content = "页面失效,请重新登陆";
 }
 else{
    VData tVData = new VData();
		ConsultationDealUI tConsultationDealUI = new ConsultationDealUI();
		LIComplaintsInfoSchema tLIComplaintsInfoSchema= new LIComplaintsInfoSchema();
		tLIComplaintsInfoSchema.setComplaintID(request.getParameter("ComplaintID"));
		tLIComplaintsInfoSchema.setComplaintContent(request.getParameter("ComplaintContent"));
		tLIComplaintsInfoSchema.setDealContent(request.getParameter("DealContent"));
		tLIComplaintsInfoSchema.setBookingDate(request.getParameter("BookingDate1"));
		tLIComplaintsInfoSchema.setBookingTime(request.getParameter("BookingTime"));
		tLIComplaintsInfoSchema.setIssueState(request.getParameter("IssueState"));
		try{
			tVData.add(tG);
			tVData.add(tLIComplaintsInfoSchema);
			tConsultationDealUI.submitData(tVData,"insert");
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (FlagStr==""){
		  tError = tConsultationDealUI.mErrors;
			if (!tError.needDealError()){                          
					Content = " 保存成功! ";
					FlagStr = "Success";
			}
			else{
					Content = " 保存失败，原因是:" + tError.getFirstError();
				  FlagStr = "Fail";
			}
		}
 }
%>                      
<%=Content%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

