<%
  //程序名称：ConsultationDealInit.jsp
  //程序功能：咨诉任务处理
  //创建日期：2009-8-6
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  String tComplaintID = request.getParameter("ComplaintID");
  System.out.println("ComplaintID="+tComplaintID);
  String viewFlag = request.getParameter("viewFlag");

%>
<script language="JavaScript">
	var tComplaintID = '<%=tComplaintID%>' ;
    var tviewFlag = '<%=viewFlag%>';
function initForm(){  
	initBox();
	showCodeName();
	viewFlag() ;
}


function initBox(){
	fm.all("ComplaintID").value = tComplaintID;
	var strSQL = "select  a.telephoneno,"
							       +" (select codename from ldcode where codetype='needdealflag' and trim(code)=a.Needdealflag),"
							       +" a.CustomerName,"
							       +" (select codename from ldcode where codetype='complaintissuetype' and trim(code)=a.IssueType),"
							       +" (select codename from ldcode where codetype='complainttype' and trim(code)=a.ComplaintType),"
							       +" (select name from ldcom where comcode=a.TransmitCom),"
							       +" a.OtherNo,"
							       +" a.UndertakeDate ,"
							       +" (select codename from ldcode where codetype='customertype2' and trim(code)=a.CustomerNo),"
							       +" a.CustomerAddress,"
							       +" (select name from ldcom where comcode=a.TelephoneCom),"
							       +" a.BookingDate ,"
							       +" a.ComplaintContent,"
							       +" a.DealContent,"
							       +" a.BookingDate,"
							       +" a.BookingTime,"
							       +" a.IssueState,(select codename from ldcode where code=a.UrgencySign and codetype='yesno') "
						+" from LIComplaintsInfo a"
						+" where a.complaintid = '"+tComplaintID+"'";
	var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if (!strQueryResult) {
		return false;
	}
	else{							
		var arrSelected = decodeEasyQueryResult(strQueryResult);
		fm.all("TelephoneNo").value = arrSelected[0][0];
		fm.all("NeedDealFlag").value = arrSelected[0][1];
		fm.all("CustomerName").value = arrSelected[0][2];
		fm.all("IssueType").value = arrSelected[0][3];
		fm.all("ComplaintType").value = arrSelected[0][4];
		fm.all("TransmitCom").value = arrSelected[0][5];
		fm.all("OtherNo").value = arrSelected[0][6];
		fm.all("UndertakeDate").value = arrSelected[0][7];
		fm.all("CustomerNo").value = arrSelected[0][8];
		fm.all("CustomerAddress").value = arrSelected[0][9];
		fm.all("TelephoneCom").value = arrSelected[0][10];
		fm.all("BookingDate").value = arrSelected[0][11];
		fm.all("ComplaintContent").value = arrSelected[0][12];
		fm.all("DealContent").value = arrSelected[0][13];
		fm.all("BookingDate1").value = arrSelected[0][14];
		fm.all("BookingTime").value = arrSelected[0][15];
		fm.all("IssueState").value = arrSelected[0][16];
		fm.all("UrgencySign").value = arrSelected[0][17];
	}
  var edomsql="select 1 "
            +" from LIComplaintsInfo a"
						+" where a.IssueState='01' and a.complaintid = '"+tComplaintID+"'";
  var edom=easyExecSql(edomsql);
	if (edom=='1'||edom=="1"){
		fm.all("update").disabled = false;
	}
	else {
		fm.all("update").disabled = true;
	}	
}
</script>
