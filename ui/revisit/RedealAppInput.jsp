<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：RedealAppInput.jsp
 //程序功能：转办件任务申请
 //创建日期：2009-7-31
 //创建人  ：liwb
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
  var operator = "<%=tGI.Operator%>";   //记录操作员
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
</script>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
 <SCRIPT src="RedealApp.js"></SCRIPT>
 <%@include file="RedealAppInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./RedealAppSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >转办件任务查询</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">管理机构</td><td class="input"><input class="codeno" name="ManageCom" ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);" nextcasing=><input class="codename" name="ManageComName" ></td>      
    <td class="title">投保单类型</td><td class="input"><input class="codeno" name="SaleChnl" ondblclick="return showCodeList('salechnl1',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKey('salechnl1',[this,SaleChnlName],[0,1]);" nextcasing=><input class="codename" name="SaleChnlName" ></td>      
    <td class="title">业务员代码</td><td class="input"><input class="codeno" name="AgentCode" ondblclick="return showCodeList('agentcode',[this,AgentCodeName],[0,1]);" onkeyup="return showCodeListKey('agentcode',[this,AgentCodeName],[0,1]);" nextcasing=><input class="codename" name="AgentCodeName" ></td>      
    <td class="title">客户姓名</td><td class="input"><input class="common" name="AppntName" ></td>  
  </tr>
<tr class="common">
    <td class="title">应回访日期</td><td class="input"><input class="coolDatePicker" dateFormat="short" name="ShouldVisitDate" ></td>      
    <td class="title">保险合同号</td><td class="input"><input class="common" name="ContNo" ></td>      
    <td class="title">回访类型</td><td class="input"><input class="codeno" name="AnswerType" ondblclick="return showCodeList('answertype',[this,AnswerTypeName],[0,1]);" onkeyup="return showCodeListKey('answertype',[this,AnswerTypeName],[0,1]);" nextcasing=><input class="codename" name="AnswerTypeName" ></td>      
    <td class="title">转办类型</td><td class="input"><input class="codeno" name="DealType" ondblclick="return showCodeList('dealtype',[this,DealTypeName],[0,1]);" onkeyup="return showCodeListKey('dealtype',[this,DealTypeName],[0,1]);" nextcasing=><input class="codename" name="DealTypeName" ></td>  
  </tr>
</TABLE>
<input value="任务查询" type=button  onclick="AllTaskQuery()" class="cssButton" type="button" >
<br><br>
<table>
  <tr>
    <td class="titleImg" >转办件任务公共池</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanAllTaskGrid" >
     </span> 
      </td>
   </tr>
</table>

<TABLE  class="common" >
  <tr class="common">
    <td class="title">转办件总量</td><td class="input"><input class="readonly" name="AllDealNum" readonly ></td>      
    <td class="title">待处理量</td><td class="input"><input class="readonly" name="WaitDealNum" readonly ></td>      
    <td class="title"></td><td class="input"><input class="readonly" name="" readonly ></td>      
    <td class="title"></td><td class="input"><input class="readonly" name="" readonly ></td>  
  </tr>
</TABLE>
<input value="任务申请" type=button  onclick="TaskApply()" class="cssButton" type="button" >
<br><br>
<table>
  <tr>
    <td class="titleImg" >转办件任务个人池</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanTaskGrid" >
     </span> 
      </td>
   </tr>
</table>



</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
