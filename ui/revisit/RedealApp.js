//该文件中包含客户端需要处理的函数和事件

//程序名称：RedealApp.js
//程序功能：转办件任务申请
//创建日期：2009-7-31
//创建人  ：liwb：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();


//任务查询按钮
function AllTaskQuery()
{
	// 书写SQL语句
	var strSQL = "select  b.AnswerID,"
	                   +" case b.AnswerType "
	                       +"when '1' then '新契约回访'" 
	                       + "else '' end,"
	                 + "	b.AppntNo,"
	                 + "	b.AppntName,"
	                 + "	b.OtherNo, "
		                 +" (select codename from ldcode where code=b.AnswerStatus and codetype='answerstatus2'),"
								   + "'待处理',"
						       + "a.MakeDate,"
						       + "b.UrgencySign,"
						       + "a.MissionID,"
						        +"(select codename from ldcode where codetype='revisitime' and code=(select AnswerRemark from LCAppnt where contno=b.OtherNo and AppntNo=b.AppntNo)) "
						 + " from lwmission a,lianswerinfo b "
						 + "where a.missionprop1 = b.answerid "
						 + "   and a.processid = '0000000010' "
						 + "   and a.activityid = '0000003002' "
						 +" and b.managecom like '"+comcode+"%'"
						 + "   and a.defaultoperator is null "
								 + getWherePart('b.ManageCom','ManageCom','like')
								 + getWherePart('b.SaleChnl','SaleChnl')
								 + getWherePart('b.AgentCode','AgentCode')
								 + getWherePart('b.AppntName','AppntName')
								 + getWherePart('b.ShouldVisitDate','ShouldVisitDate')
								 + getWherePart('b.OtherNo','ContNo')
								 + getWherePart('b.AnswerType','AnswerType')
								 + getWherePart('b.AnswerStatus','DealType')
						 + "group by b.AppntNo,b.AppntName,b.OtherNo,AnswerID,b.AnswerType,b.AnswerStatus,a.MakeDate,b.UrgencySign,a.MissionID "
   					 + "order by b.UrgencySign desc,a.MakeDate";;
	
	turnPage.queryModal(strSQL,AllTaskGrid,0,1);
	return true;
}

//任务申请按钮
function TaskApply(){
	var i = 0;
  var checkFlag = 0;
  var tMissionID;
  for (i=0; i<AllTaskGrid.mulLineCount; i++){
    if (AllTaskGrid.getSelNo(i)) { 
      checkFlag = AllTaskGrid.getSelNo();
      break;
    }
  } 
  if (checkFlag) { 
    tMissionID = AllTaskGrid.getRowColData(checkFlag - 1, 10); 
    if(tMissionID==""){
    	alert("没有选中需要申请的任务!");
    	return false;
    }
    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.action="./RedealAppSave.jsp?MissionID="+tMissionID;
	  fm.submit();
	}
	else
	{
		alert("没有选中需要申请的任务!")
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    easyQueryClick();
  }
}

//初始化界面
function initInputBox(){
	var strSql = "select count(*) "
							+ "from lwmission a,lianswerinfo b "
							+ "where processid = '0000000010' and b.AnswerStatus in ('2','3','5') "
							+ "   and activityid = '0000003002' and a.missionprop1 = b.answerid and b.managecom like '"+comcode+"%'"
	var arrResult=easyExecSql(strSql,1,0);
	if(arrResult != null){
		fm.AllDealNum.value = arrResult[0][0];
	}
	var strSql1 = "select count(*) "
							+ "from lwmission a,lianswerinfo b "
							+ "where processid = '0000000010' "
							+ "   and activityid = '0000003002' "
							+ "   and defaultoperator is null and a.missionprop1 = b.answerid and b.managecom like '"+comcode+"%'";
	var arrResult1=easyExecSql(strSql1,1,0);
	if(arrResult1 != null){
		fm.WaitDealNum.value = arrResult1[0][0];
	}
}

//初始化查询
function easyQueryClick(){
	// 初始化查询
	initInputBox();
	initAllTaskGrid();
	initTaskGrid();
	AllTaskQuery();
	TaskQuery();
}

//初始化个人池
function TaskQuery(){
	// 书写SQL语句
	var strSQL = "select  b.AnswerID,"
	                   +" case b.AnswerType when '1' then '新契约回访' else '' end,"
		                 +" b.AppntNo,"
		                 +" b.AppntName,"
		                 +" b.OtherNo, "
		                 +" (select codename from ldcode where code=b.AnswerStatus and codetype='answerstatus2'),"
						         +" '处理中',"
						         +" a.MakeDate,"
						         +" b.UrgencySign,"
						         +" a.MissionID,"
						         +" b.AnswerStatus, "
						  +"(select codename from ldcode where codetype='revisitime' and code=(select AnswerRemark from LCAppnt where contno=b.OtherNo and AppntNo=b.AppntNo)) "
						 + "from lwmission a,lianswerinfo b "
						 + "where a.missionprop1 = b.answerid "
						 +" and b.managecom like '"+comcode+"%'"
						 + "   and a.processid = '0000000010' "
						 + "   and a.activityid = '0000003002' "
						 + "   and defaultoperator = '" + operator + "' and b.AnswerStatus in ('2','3','5') "
						 + "group by b.AppntNo,b.AppntName,b.OtherNo,AnswerID,b.AnswerType,b.AnswerStatus,a.MakeDate,b.UrgencySign,a.MissionID "
   					 + "order by b.UrgencySign desc,a.MakeDate";
	
	turnPage.queryModal(strSQL,TaskGrid,0,1);
	return true;
}

//进入转办处理页面
function intoRedealDet(){
	var i = 0;
  var checkFlag = 0;
  for (i=0; i<TaskGrid.mulLineCount; i++){
    if (TaskGrid.getSelNo(i)) { 
      checkFlag = TaskGrid.getSelNo();
      break;
    }
  }
 
	var tAnswerID = TaskGrid.getRowColData(checkFlag - 1, 1); 
	var tAppntNo = TaskGrid.getRowColData(checkFlag - 1, 3); 
	var tOtherNo = TaskGrid.getRowColData(checkFlag - 1, 5); 
	var tMissionID = TaskGrid.getRowColData(checkFlag - 1, 10);
	var tAnswerStatus = TaskGrid.getRowColData(checkFlag - 1, 11);
	
	var Sql_cont ="select 'Y' from LIAnswerInfo  a where a.AppntNo = '"+tAppntNo+"' and a.OtherNo <> '"+tOtherNo+"'  and ShouldVisitDate >= Add_Months (date '"+currentDate+"', -3 ) "+
    " and exists (select p1.riskcode  from lcpol p1 , lcpol p2 where  p1.contno = a.otherno  and  p2.contno ='"+tOtherNo+"'  and  p1.mainpolno = p1.polno and p2.mainpolno = p2.polno and p1.riskcode = p2.riskcode ) " ;
    var arrResult=easyExecSql(Sql_cont,1,0);
    if(null!=arrResult)
    {
       alert(" 该客户有相同险种保单转办件任务处理记录 ,处于回访状态，请关注！");
    }
	
	window.open("./RedealDetInputToMain.jsp?AnswerID="+ tAnswerID +"&AppntNo=" + tAppntNo 
		+ "&OtherNo=" + tOtherNo + "&AnswerStatus=" + tAnswerStatus + "&MissionID=" + tMissionID);
}