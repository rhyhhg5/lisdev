<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：RevisitDetSave.jsp
//程序功能：回访任务处理
//创建日期：2009-7-29
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.revisit.*"%>
<%

 //输出参数
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String transact = "";

 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 VData tVData = new VData();
 RevisitDetUI tRevisitDetUI = new RevisitDetUI();
 LIAnswerInfoSchema mLIAnswerInfoSchema=new LIAnswerInfoSchema();
 LCAppntSchema mLCAppntSchema = new LCAppntSchema();
 LIAnswerInfoDB mLIAnswerInfoDB=new LIAnswerInfoDB();
 String theCurrentDate = PubFun.getCurrentDate();
 String theCurrentTime = PubFun.getCurrentTime();
 
 if (tG == null) {
	 	System.out.println("页面失效,请重新登陆");
	 	FlagStr = "Fail";
	 	Content = "页面失效,请重新登陆";
 }
 else {  
	 mLCAppntSchema.setContNo(request.getParameter("ContNo"));
	 mLCAppntSchema.setAppntNo(request.getParameter("CustomNo"));
	 mLCAppntSchema.setAnswerPhoneType(request.getParameter("RevisitPhoneType"));
	 mLCAppntSchema.setAnswerPhone(request.getParameter("RevisitPhone"));
	 
	  String AnswerStatus=request.getParameter("ResvisitState");
		String BookingDate=request.getParameter("BookingDate");
		String BookingTime=request.getParameter("BookingTime");
		String AnswerDate=request.getParameter("AnswerDate");
		String AnswerTime=request.getParameter("AnswerTime"); 
		String CloseReason=request.getParameter("CloseReason");
		String UrgencySign=request.getParameter("UrgencySign");
		String Remark=request.getParameter("Remark");
		String AnswerPerson=request.getParameter("AnswerPerson");
		String tChk[] = request.getParameterValues("InpIssueGridChk"); 
		String tTYPE2[] = request.getParameterValues("IssueGrid3");
		String[] tTYPE=null;
		System.out.println("Success123");
		int mulcount = tTYPE2.length;
	  tTYPE=new String[mulcount];
		int num=0; 
		for(int index=0;index<tChk.length;index++){
		   if(tChk[index].equals("1")){
		      System.out.println("第"+index+"行被选中");
			    tTYPE[num]= tTYPE2[index];
			    System.out.println("tTYPE[num]="+tTYPE[num]);
			    num++;       
		   }
		   if(tChk[index].equals("0")) {     
		      System.out.println("第"+index+"行未被选中");
		   }
		}
		mLIAnswerInfoDB.setAnswerID(request.getParameter("AnswerID"));
	  mLIAnswerInfoDB.getInfo();
		mLIAnswerInfoSchema=mLIAnswerInfoDB.getSchema();
		mLIAnswerInfoSchema.setAnswerID(request.getParameter("AnswerID"));
		mLIAnswerInfoSchema.setOtherNo(request.getParameter("ContNo"));
		mLIAnswerInfoSchema.setAppntNo(request.getParameter("CustomNo"));       
		mLIAnswerInfoSchema.setAnswerStatus(AnswerStatus);
		if("4".equals(AnswerStatus))
		{
		 mLIAnswerInfoSchema.setBookingDate(BookingDate);
		 mLIAnswerInfoSchema.setBookingTime(BookingTime);
		}
		mLIAnswerInfoSchema.setAnswerDate(AnswerDate);
		mLIAnswerInfoSchema.setAnswerTime(AnswerTime);
		mLIAnswerInfoSchema.setCloseReason(CloseReason);
		mLIAnswerInfoSchema.setUrgencySign(UrgencySign);
		mLIAnswerInfoSchema.setAnswerPerson(AnswerPerson);
		mLIAnswerInfoSchema.setRemark(Remark);
		mLIAnswerInfoSchema.setModifyDate(theCurrentDate);
		mLIAnswerInfoSchema.setModifyTime(theCurrentTime);
		if(AnswerStatus.equals("2")){
		   LIIssuePolSet tLIIssuePolSet= new LIIssuePolSet();
		   for(int i=0;i<num;i++){
		    	LIIssuePolSchema tLIIssuePolSchema= new LIIssuePolSchema();
		    	tLIIssuePolSchema.setAnswerID(request.getParameter("AnswerID"));
		    	tLIIssuePolSchema.setIssueType(tTYPE[i]);
		    	System.out.println("tTYPE[i]="+tTYPE[i]);
		    	tLIIssuePolSchema.setOtherNo(request.getParameter("ContNo"));
		    	tLIIssuePolSchema.setOtherNoType("00");
		    	tLIIssuePolSchema.setAppntNo(request.getParameter("CustomNo"));
		    	tLIIssuePolSchema.setOperator(request.getParameter("Operator"));
		    	tLIIssuePolSchema.setMakeDate(theCurrentDate);
		    	tLIIssuePolSchema.setMakeTime(theCurrentTime);
		    	tLIIssuePolSchema.setModifyDate(theCurrentDate);
		    	tLIIssuePolSchema.setModifyTime(theCurrentTime);
		    	tLIIssuePolSet.add(tLIIssuePolSchema);		    		
		    }
		    tVData.add(tLIIssuePolSet);
		}
		mLIAnswerInfoSchema.setCloseReason(CloseReason);
		tVData.add(tG);
	  tVData.add(mLIAnswerInfoSchema);
	  tVData.add(AnswerStatus);
	  tVData.add(mLCAppntSchema);
    try{
			tRevisitDetUI.submitData(tVData,"");
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (FlagStr==""){
			tError = tRevisitDetUI.mErrors;
			if (!tError.needDealError()){                          
				 Content = " 保存成功! ";
				 FlagStr = "Success";
			}
		  else{
				 Content = " 保存失败，原因是:" + tError.getFirstError();
				 FlagStr = "Fail";
			}
		}
}
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
   parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

