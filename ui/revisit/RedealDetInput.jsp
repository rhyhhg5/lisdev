<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：RedealDetInput.jsp
 //程序功能：转办任务处理
 //创建日期：2009-8-4
 //创建人  ：liwb
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	String viewFlag ="";
	viewFlag = request.getParameter("viewFlag") ;
	System.out.println(viewFlag);
%>
<script>
  var operator = "<%=tGI.Operator%>";   //记录操作员
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
  var	tAnswerID = "<%=request.getParameter("AnswerID")%>";
  var	tAppntNo = "<%=request.getParameter("AppntNo")%>";
  var	tOtherNo = "<%=request.getParameter("OtherNo")%>";
  var	tAnswerStatus = "<%=request.getParameter("AnswerStatus")%>";
  var	tMissionID = "<%=request.getParameter("MissionID")%>";
  var   tviewFlag="<%=viewFlag%>";
</script>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
 <SCRIPT src="RedealDet.js"></SCRIPT>
 <%@include file="RedealDetInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./RedealDetSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >回访基本资料</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">回访号</td><td class="input"><input class="readonly" name="AnswerID" readonly ></td>      
    <td class="title">回访类型</td><td class="input"><input class="readonly" name="AnswerType" readonly ></td>      
    <td class="title">应访时间</td><td class="input"><input class="readonly" name="ShouldVisitDate" readonly ></td>  
  </tr>
<tr class="common">
    <td class="title">机构名称</td><td class="input"><input class="readonly" name="ManageCom" readonly ></td>      
    <td class="title"></td>    
    <td class="title"></td>    
    <td class="title"></td>    
    <td class="title"></td>
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >客户资料</td>
  </tr>
</table>
<table>
  <tr>
    <td class="titleImg" >投保人信息</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">客户号</td><td class="input"><input class="readonly" name="AppntNo" readonly ></td>      
    <td class="title">姓名</td><td class="input"><input class="readonly" name="AppntName" readonly ></td>      
    <td class="title">性别</td><td class="input"><input class="readonly" name="AppntSex" readonly ></td>  
  </tr>
<tr class="common">
    <td class="title">年龄</td><td class="input"><input class="readonly" name="AppntAge" readonly ></td>      
    <td class="title">证件类型</td><td class="input"><input class="readonly" name="AppntIDType" readonly ></td>      
    <td class="title">证件号码</td><td class="input"><input class="readonly" name="AppntIDNo" readonly ></td>  
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >被保人信息</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">客户号</td><td class="input"><input class="readonly" name="InsuredNo" readonly ></td>      
    <td class="title">姓名</td><td class="input"><input class="readonly" name="InsuredName" readonly ></td>      
    <td class="title">性别</td><td class="input"><input class="readonly" name="InsuredSex" readonly ></td>  
  </tr>
<tr class="common">
    <td class="title">年龄</td><td class="input"><input class="readonly" name="InsuredAge" readonly ></td>      
    <td class="title">证件类型</td><td class="input"><input class="readonly" name="InsuredIDType" readonly ></td>      
    <td class="title">证件号码</td><td class="input"><input class="readonly" name="InsuredIDNo" readonly ></td>  
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >联系电话清单</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">固定电话</td>
    <td class="input">
    	<input class="readonly" name="Phone" readonly >
    </td>      
    <td class="title">移动电话</td>
    <td class="input">
    	<input class=readonly name="TelPhone" readonly  >
    </td>      
    <TD class= title>
            回访电话类型
    </TD>
    <TD class= input>
        <input class="codeno" name="RevisitPhoneType" ondblclick="return showCodeList('RevisitPhoneType',[this,RevisitPhoneTypeName],[0,1]);" onkeyup="return showCodeListKey('RevisitPhoneType',[this,RevisitPhoneTypeName],[0,1]);"><input class=codename name=RevisitPhoneTypeName readonly=true>
    </TD>
    <tr>
    <TD  class= title>
            回访电话
    </TD>
    <TD  class= input>
            <Input class= common name="RevisitPhone" id="RevisitPhone" maxlength=15>
    </TD>
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >保单资料</td>
  </tr>
</table>
<TABLE  class="common" >
  <tr class="common">
    <td class="title">保单合同号</td><td class="input"><input class="readonly" name="ContNo" readonly ></td>      
    <td class="title">保单申请日期</td><td class="input"><input class="readonly" name="PolApplyDate" readonly ></td>      
    <td class="title">保单生效日期</td><td class="input"><input class="readonly" name="SignDate" readonly ></td>      
    <td class="title">保单打印时间</td><td class="input"><input class="readonly" name="PrintDate" readonly ></td>  
  </tr>
<tr class="common">
    <td class="title">保单签收时间</td><td class="input"><input class="readonly" name="CustomGetPolDate" readonly ></td>      
    <td class="title">投保单类型</td><td class="input"><input class="readonly" name="SaleChnl" readonly ></td>      
    <td class="title">代理人编码</td><td class="input"><input class="readonly" name="AgentCode" readonly ></td>      
    <td class="title">代理人姓名</td><td class="input"><input class="readonly" name="AgentName" readonly ></td>  
  </tr>
<tr class="common">
   <td class="title">代理机构</td><td class="input"><input class="readonly" name="AppManageCom" readonly ></td>
    <td class="title">联系地址</td><td class="input"><input class="readonly" name="Address" readonly ></td>      
    <td class="title">邮编</td><td class="input"><input class="readonly" name="ZipCode" readonly ></td>      
    <td class="title">银行网点名称</td><td class="input"><input class="readonly" name="AgentCom" readonly ></td>      
    
  </tr>
<tr class="common">
    <td class="title">银行名称</td><td class="input"><input class="readonly" name="BankCode" readonly ></td>      
    <td class="title">银行转账账号</td><td class="input"><input class="readonly" name="BankAccNo" readonly ></td>      
    <td class="title"></td>    
    <td class="title"></td>    
    <td class="title"></td>    
    <td  class="input" ><input value="既往投保信息" type=button  onclick="showApp()" class="cssButton" name="ShowAppButton"></td>
  </tr>
</TABLE>
<table>
  <tr>
    <td class="titleImg" >险种清单（包括主险与附加险）</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanPolGrid" >
     </span> 
      </td>
   </tr>
</table>

<table>
  <tr>
    <td class="titleImg" >回访历程</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanRevisitCourseGrid" >
     </span> 
      </td>
   </tr>
</table>
<Div  id= "div1" style="display:''">
<table>
  <tr>
    <td class="titleImg" >回访备注</td>
  </tr>
</table>
<textarea class="common" name="Remark" cols="100%" rows="2" value="1000字" readonly=true></textarea><BR>
</div>
<Div  id= "div2" style="display:'none'">
<table>
  <tr>
    <td class="titleImg" >问题信息</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanAnswerGrid" >
     </span> 
      </td>
   </tr>
</table>
</div>
<br>


<table class="common"  >
<tr class="common">
  <td CLASS="title">回访状态</td>
  <td CLASS="input" COLSPAN="1">
　<Input class=codeno name=ResvisitState ondblclick="return showCodeListEx('ResvisitState',[this,ResvisitStateName],[0,1],null,null,null,'1');"   verify="回访状态|notnull" onkeyup="return showCodeListKeyEx('ResvisitState',[this,ResvisitStateName],[0,1],null,null,null,'1');" onfocus="selectstate();"><input class=codename  elementtype="nacessary" verify="回访状态|notnull"  name=ResvisitStateName readonly=true  >
  </td>	
  <td  ></td>
  <td  ></td>
  <td  ></td>   
</tr>
</table>

<Div  id= "div3" style="display:'none'">
<table class="common"  >
<tr class="common">
  <td CLASS="title">处理方式</td>
  <td CLASS="input" COLSPAN="1">
　<input class="codeno" name="TakeMode" ondblclick="return showCodeList('takemode',[this,TakeModeName],[0,1]);" onkeyup="return showCodeListKey('takemode',[this,TakeModeName],[0,1]);" nextcasing=><input class="codename"  name="TakeModeName"  readonly=true >
  </td>	
  <td  ></td>
  <td  ></td>
  <td  ></td>   
</tr>
</table>
</div>


<Div  id= "div4" style="display:'none'">
<table class="common"  >
<tr class="common">
  <td CLASS="title">关闭原因</td>
  <td CLASS="input" COLSPAN="1">
　<Input class=codeno name=CloseReason   CodeData="0|^01|拒访^02|其它^03|撤单" ondblclick="return showCodeListEx('CloseReason',[this,CloseReasonName],[0,1],null,null,null,1);" verify="关闭原因|code:CloseReason" onkeyup="return showCodeListKeyEx('CloseReason',[this,CloseReasonName],[0,1]);"><input class=codename  name=CloseReasonName   readonly=true  >
  </td>	
  <td  ></td>
  <td ></td>
</tr>
</table>
</Div>
<Div  id= "div5" style="display:'none'">
<table class="common"  >
<tr class="common">
  <td CLASS="title">预约日期</td>
  <td CLASS="input" COLSPAN="1">
　<input class="coolDatePicker" dateFormat="short" name="BookingDate">
  </td>	
    <td class="title">预约时间(HH:MM)</td>  
    <td class="input">
    	<input class="common" name="BookingTime">
    </td>  
</tr>
</table>
</Div>
<tr class="common">
  <td class="title">特急案件标识</td>
   <td class="input" COLSPAN="1">
   <input class="input" name="UrgencySign" readonly=true>
  </td>  
</tr>
<table>
  <tr>
    <td class="titleImg" >处理情况说明</td>
  </tr>
</table>
<textarea class="common" name="AnswerExplain" cols="100%" rows="2" value="1000字" ></textarea><BR>
<table>
  <tr>
    <td class="titleImg" >同日转办关联保单</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanAnsweridGrid" >
     </span> 
      </td>
   </tr>
</table>
<br>



<br>
<input value="完  成" type=button  onclick="submitForm()" class="cssButton" type="button"  name ="submitButton"  >
<input value="返  回" type=button  onclick="back()" class="cssButton" type="button"  name ="backButton">
<br><br>

<INPUT  type= "hidden" class= "Common" name= "MissionID">
<INPUT  type= "hidden" class= "Common" name= "AnswerStatus">

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
