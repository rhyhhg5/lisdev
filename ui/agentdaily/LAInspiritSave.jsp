<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAPlanAgentSave.jsp
//程序功能：
//创建日期：2005-12-14 14:16
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAInspiritSchema tLAInspiritSchema = new LAInspiritSchema();
  LAInspiritUI tLAInspiritUI = new LAInspiritUI();

  //输出参数
  CErrors tError = null;
  String tOperate = request.getParameter("Operate");
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	System.out.println("Save页面处理开始...");
	
  // 收集页面数据
  tLAInspiritSchema.setSeqNo(request.getParameter("SeqNo"));
	tLAInspiritSchema.setStartDate(request.getParameter("StartDate"));
  tLAInspiritSchema.setEndDate(request.getParameter("EndDate"));
  tLAInspiritSchema.setStatDate(request.getParameter("StatDate"));
  tLAInspiritSchema.setOrganizer(request.getParameter("Organizer"));
  tLAInspiritSchema.setPloyName(request.getParameter("PloyName"));
  tLAInspiritSchema.setAgentCode(request.getParameter("AgentCode"));
  tLAInspiritSchema.setAgentName(request.getParameter("AgentCodeName"));
  tLAInspiritSchema.setInspiritName(request.getParameter("InspiritName"));
  tLAInspiritSchema.setInspiritContent(request.getParameter("InspiritContent"));
	
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.add(tLAInspiritSchema);
  System.out.println("add over");
  
  try
  {
    tLAInspiritUI.submitData(tVData,tOperate)	;
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAInspiritUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

