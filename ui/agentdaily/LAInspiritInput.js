/**************************************
 * 设置人员列表
 * 参数：  cObj,dobj  两个相关页面上的控件
 * 返回：  无
 **************************************/
function EdorType(cObj,dobj)
{
	var tBranchType  = document.fm.BranchType.value;
	var tBranchType2 = document.fm.BranchType2.value;
	var tManageCom   = document.fm.ManageCom.value;

	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# and agentcode in (select a.agentcode from latree a where a.agentgrade<>#A00#)";

	showCodeList('Agentcode',[cObj,dobj], [0,1], null, mEdorType, "1");
}

/**************************************
 * 设置人员列表
 * 参数：  cObj,dobj  两个相关页面上的控件
 * 返回：  无
 **************************************/
function KeyUp(cObj,dobj)
{
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
	var tManageCom   = document.fm.ManageCom.value;

	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# and agentcode in (select a.agentcode from latree a where a.agentgrade<>#A00#)";

	showCodeList('Agentcode',[cObj,dobj], [0,1], null, mEdorType, "1");
}

/**************************************
 * 提交画面
 * 参数：  无
 * 返回：  无
 **************************************/
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}

/**************************************
 * 点击保存按钮时触发的事件
 * 参数：  无
 * 返回：  无
 **************************************/
function saveClick()
{
	// 进行必录项验证
	if(!verifyInput())
	{
		return false;
	}
	
	if("" != document.fm.SeqNo.value)
	{
		alert("查询出来的数据只能进行修改！");
		return false;
	}
	
	// 进行画面验证
	if(!beforeSubmit())
	{
		return false;
	}
	
	document.fm.Operate.value = "INSERT||MAIN";

	// 验证通过  画面提交
	submitForm();
	
	return true;
}

/**************************************
 * 点击"修改"按钮时的触发事件
 * 参数：  无
 * 返回：  无
 **************************************/
function updateClick()
{
	// 进行必录项验证
	if(!verifyInput())
	{
		return false;
	}
	// 判断是否是已有数据
	if("" == document.fm.SeqNo.value)
	{
		alert("请查询返回一条数据再作修改操作！");
		return false;
	}
	// 进行画面验证
	if(!beforeSubmit())
	{
		return false;
	}
	
	document.fm.Operate.value = "UPDATE||MAIN";

	// 验证通过  画面提交
	submitForm();
	
	return true;
}

/**************************************
 * 点击"查询"按钮时的触发事件
 * 参数：  无
 * 返回：  无
 **************************************/
function queryClick()
{
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  showInfo=window.open("./LAInspiritQueryInputHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

/**************************************
 * 起脚画面前进行的画面验证
 * 参数：  无
 * 返回：  无
 **************************************/
function beforeSubmit()
{
	var tInputValue
	// 验证输入的时间是否合法
	tInputValue = trim(document.fm.StartDate.value);
	if(tInputValue != "" && tInputValue != null)
	{
		if(isDate(tInputValue) == false)
		{
			alert("[活动起期]请录入一个格式为‘YYYY-MM-DD’的有效日期！");
			return false;
		}
	}
	tInputValue = trim(document.fm.EndDate.value);
	if(tInputValue != "" && tInputValue != null)
	{
		if(isDate(tInputValue) == false)
		{
			alert("[活动止期]请录入一个格式为‘YYYY-MM-DD’的有效日期！");
			return false;
		}
	}
	// 验证活动起止期是否有效
	if(trim(document.fm.StartDate.value) > trim(document.fm.EndDate.value))
	{
		alert("[活动止期]不能小于[活动起期]！");
		return false;
	}
	tInputValue = trim(document.fm.StatDate.value);
	if(tInputValue != "" && tInputValue != null)
	{
		if(isDate(tInputValue) == false)
		{
			alert("[统计时间]请录入一个格式为‘YYYY-MM-DD’的有效日期！");
			return false;
		}
	}
	// 验证输入文字是否超长
	tInputValue = trim(document.fm.Organizer.value);
	if(tInputValue != "" && tInputValue != null)
	{
		if(strLen(tInputValue,300,"[组织单位]最多可录入100个文字或300个英文字母！") == false)
		  return false;
	}
	tInputValue = trim(document.fm.PloyName.value);
	if(tInputValue != "" && tInputValue != null)
	{
		if(strLen(tInputValue,300,"[活动名称]最多可录入100个文字或300个英文字母！") == false)
		  return false;
	}
	tInputValue = trim(document.fm.InspiritName.value);

	if(tInputValue != "" && tInputValue != null)
	{
		if(strLen(tInputValue,120,"[获奖称号]最多可录入40个文字或120个英文字母！") == false)
		  return false;
	}
	tInputValue = trim(document.fm.InspiritContent.value);
	if(tInputValue != "" && tInputValue != null)
	{
		if(strLen(tInputValue,900,"[奖励内容]最多可录入300个文字或900个英文字母！") == false)
		  return false;
	}
	
	return true;
}

/**************************************
 * 提交后操作,服务器数据返回后执行的操作
 * 参数：  无
 * 返回：  无
 **************************************/
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    initForm();
  }
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个一维数组，数组下标从[0]开始
 *  参数  ：  查询返回的一维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	// 清空画面
	initInpBox();
	
	if( arrQueryResult != null )
	{
		document.fm.StartDate.value = arrQueryResult[0];
		document.fm.EndDate.value = arrQueryResult[1];
		document.fm.StatDate.value = arrQueryResult[2];
		document.fm.Organizer.value = arrQueryResult[4];
		document.fm.AgentCode.value = arrQueryResult[5];
		document.fm.AgentCodeName.value = arrQueryResult[6];
		document.fm.PloyName.value = arrQueryResult[3];
		document.fm.InspiritName.value = arrQueryResult[7];
		document.fm.InspiritContent.value = arrQueryResult[8];
		document.fm.SeqNo.value = arrQueryResult[9];
	}
}

/***************************************
 * 验证字符的字节长度是否超标
 * 参数：pmStr      准备验证的字符串
 *       pmStrLen   长度标准
 *       pmMissage  报错信息
 * 返回：返回字符串字节长度
 ***************************************/
function strLen(pmStr,pmStrLen,pmMissage)
{
	var i;
  var len;
  len = 0;
  for (i=0;i<pmStr.length;i++)
  {
    if (pmStr.charCodeAt(i)>255) len+=3; else len++;
  }

  if(len > pmStrLen)
  {
  	alert(pmMissage);
  	return false;
  }
  
  return true;
}