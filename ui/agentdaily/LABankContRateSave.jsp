<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAArchieveSave.jsp
//程序功能：
//创建日期：2005-03-20 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAConRateSchema tLAConRateSchema   = new LAConRateSchema();
  LAConRateSet tLAConRateSet = new LAConRateSet();
  LAContRateUI tLAContRateUI   = new LAContRateUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tOperate ="INSERT||MAIN";
  String mTest ="";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tManageCom=request.getParameter("ManageCom");
  String tBranchType=request.getParameter("BranchType");
  String tBranchType2=request.getParameter("tBranchType2");
  
//取得Muline信息
  String tChk[] = request.getParameterValues("InpArchieveGridChk");
  int lineCount = 0;
  String tAgentCom[] = request.getParameterValues("ArchieveGrid1");
  String tGrpContNo[] = request.getParameterValues("ArchieveGrid3");
  String tRiskCode[] = request.getParameterValues("ArchieveGrid4");
  String tRate[] = request.getParameterValues("ArchieveGrid6");
  String tGrpPolNo[]=request.getParameterValues("ArchieveGrid10");
  String tSalechnl[]=request.getParameterValues("ArchieveGrid11");
  String tSalechnldetail[]=request.getParameterValues("ArchieveGrid12");
  lineCount = tRiskCode.length; //行数
 // System.out.println("test String:"+tArchType[1]);
  System.out.println("length= "+String.valueOf(lineCount));
  int number=0;
  for(int index=0;index<tChk.length;index++)
  {
     if(tChk[index].equals("1"))           
     number++;
  } 
  if(number==0)
  {
     Content = " 失败，原因:没有选择要保存的信息！";
     FlagStr = "Fail";		
  } 
  else 
  {
        for(int i=1;i<=lineCount;i++)
        {
           System.out.println("transact22222||||"+tChk[i-1]); 
           if(tChk[i-1].trim().equals("1"))
           {
          
              tLAConRateSchema = new LAConRateSchema();
              tLAConRateSchema.setAgentCom(tAgentCom[i-1]);
              tLAConRateSchema.setGrpPolNo(tGrpPolNo[i-1]);             
              tLAConRateSchema.setRiskCode(tRiskCode[i-1]);  
              tLAConRateSchema.setGrpContNo(tGrpContNo[i-1]); 
              tLAConRateSchema.setBranchType(tBranchType);
              tLAConRateSchema.setBranchType2(tBranchType2);
              tLAConRateSchema.setRate(tRate[i-1]); 
              tLAConRateSchema.setManageCom(tManageCom);
              tLAConRateSchema.setChargeType("11");               
              tLAConRateSet.add(tLAConRateSchema);    
              System.out.println("transact||||||||||||||||"+tLAConRateSchema.getRate()); 
           }   
        }
        
         System.out.println("transact"+transact); 
       
        System.out.println("end 档案信息...");
        
       // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tG);
        tVData.addElement(tLAConRateSet);
        try
        {
           tLAContRateUI.submitData(tVData,transact);
        }
        catch(Exception ex)
        {
           Content = "保存失败，原因是:" + ex.toString();
           FlagStr = "Fail";
         }
   }      
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLAContRateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
