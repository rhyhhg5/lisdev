<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");

%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAAddSubPerInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAAddSubPerInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<%
  //加扣款类别
  
  String tDoneFlag1 = "";
  String tDoneFlag2 = "";
  String tTitleAgent = "";
  if ("1".equals(BranchType))
  {
    tDoneFlag1 = "0|^1|附加佣金加款|^2|同业衔接加款|^3|员工制加款|^5|其他加款|^7|财补加款";
    tDoneFlag2 = "0|^1|附加佣金扣款|^2|同业衔接扣款|^3|员工制扣款|^5|其他扣款";
    tTitleAgent = "营销员";
  } 
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAddSubPerSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAddSub);">
    
    <td class=titleImg>
      <%=tTitleAgent%>加扣款信息
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divAddSub" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		      <%=tTitleAgent%>代码 
		    </td>
        <td  class= input> 
		      <input class= common name=AgentCode MaxLength=10 OnChange="return checkvalid();" verify="业务员编码|NOTNULL" elementtype=nacessary> 
		    </td>
		    <td  class= title>
		      <%=tTitleAgent%>姓名
		    </td>
        <td  class= input>
		  <input name=Name class='readonly' readonly >
		</td>
      </tr>
      <tr  class= common> 
      <td  class= title> 
		  销售单位 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=AgentGroup > 
		</td>
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ManageCom > 
		</td>
      </tr>
      <tr  class= common> 
	<td class=title>
	加款类型
	</td>
	<td class=input><input name=DoneFlag class=codeno name="DoneFlag" verify="加扣款类型|code:DoneFlagList"
         CodeData=<%=tDoneFlag1%> MaxLength=1
         ondblClick="showCodeListEx('DoneFlagList',[this,DoneFlagName],[0,1]);"
         onkeyup="showCodeListKeyEx('DoneFlagList',[this,DoneFlagName],[0,1]);"
         ><Input class=codename name=DoneFlagName readOnly  > 
        </td>	
        <td  class= title> 
		  加款原因
		</td>
        <td  class= input> 
		  <input name=PunishRsn class= common   > 
		</td>
      </tr>

      <tr  class= common> 
        <td  class= title> 
		  加款金额(元)
		</td>
        <td  class= input> 
		  <input name=Money class= common verify="金额|num" > 
		</td>
      </tr>
      <tr  class= common> 
	<td class=title>
	扣款类型
	</td>
	<td class=input><input name=DoneFlag1 class=codeno   verify="加扣款类型|code:DoneFlag1List"
         CodeData=<%=tDoneFlag2%> MaxLength=1
         ondblClick="showCodeListEx('DoneFlag1List',[this,DoneFlagName1],[0,1]);"
         onkeyup="showCodeListKeyEx('DoneFlag1List',[this,DoneFlagName1],[0,1]);"
         ><Input class=codename name=DoneFlagName1 readOnly > 
        </td>	
        <td  class= title> 
		  扣款原因
		</td>
        <td  class= input> 
		  <input name=PunishRsn1 class= common   > 
		</td>
      </tr>

      <tr  class= common> 
        <td  class= title> 
		  扣款金额(元)
		</td>
        <td  class= input> 
		  <input name=Money1 class= common verify="金额|num" > 
		</td>

      </tr>      
      <tr  class= common>
        <TD  class= title width="25%"> 
		  执行日期
		</td>
        <TD  class= input  width="25%"  >
             <Input class= "coolDatePicker" dateFormat="short" name=DoneDate MaxLength=10 verify="执行日期|NOTNULL&Date"  elementtype=nacessary> 
	</td>
        <td  class= title> 
		  备注
		</td>
        <td  class= input> 
	     <input name=Noti class= common > 
	</td>	

		</tr>
      <tr class=common>
        <td  class= title>
		   操作员代码
		</td>
        <td  class= input>
		  <input name=Operator class="readonly" readonly >
		</td>
        <td  class= title> 
		  最近操作日
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ModifyDate > 
		</td>
	</tr>
    </table>
  </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=Idx value=''>
    <input type=hidden name=Flag value=''>
    <input type=hidden name=Flag1 value=''>
    <input type=hidden name=AwardTitle value=''>
    <input type=hidden name=BranchType value=''>   
    <input type=hidden name=BranchType2 value=''>   
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
