//               该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}

//提交，保存按钮对应操作
function DoInsert()
{
	if(!verifyInput())
   {
	return false;
   }	
  //校验是否该月佣金已算过
  var tWageNo = fm.wageno.value;
  var strSQ =" select state from lawagehistory  " 
            +" where wageno= '"+tWageNo+"' and BranchType='1' and BranchType2='01' "
            +" and managecom='"+fm.ManageCom.value+"'";

  //判断是否查询成功
var strQueryResult  = easyQueryVer3(strSQ, 1, 1, 1);
if(!strQueryResult)
{   alert("佣金试算还未进行,不可进行薪资重算操作!");
    return false;
    }
var arr = decodeEasyQueryResult(strQueryResult);
var tState=arr[0][0] ;
if (tState=="00") {  
    alert("佣金正在试算中！");
    return false;
}
else if(tState=="12"){  
    alert("佣金正在试算或重算中！");
    return false;
}
else if(tState=="13"){  
    alert("佣金计算已经确认！");
    return false;
}	
else if(tState=="14"){  
    alert("佣金已经审核发放！");
    return false;
}			
else if(tState=="11"){
	var strSql =" select AgentCode from LAWageTemp " 
             +" where IndexCalNo = '"+tWageNo+"'"
             +" and BranchType='1' and BranchType2='01' "
             +" and managecom='"+fm.ManageCom.value+"'";  
	//查询SQL，返回结果字符串
  var strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  //判断是否查询成功
  if (!strQueryResult) {  
    alert("该月佣金没有试算过，无法进行重算操作！");
    return false;
  }	
}	
else 
	{  
    alert("佣金试算还未进行!");
    return false;
  }	
	
	//fm.fmAction.value = "INSERT";
	//if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initSetGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select indexcalno,agentcode,agentgrade from lawage "
	 +"where branchtype='2' and branchtype2='01' "
	 +"and indexcalno between '"+fm.all('wageno').value+"' "
	 //+"and '"+fm.all('wagenoend').value+"'"
	+ getWherePart( 'ManageCom','ManageCom','like'	)
	//+ getWherePart( 'indexcalno','wageno');

    
	strSQL += " order by agentcode";
					//查询SQL，返回结果字符串
					turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

					//判断是否查询成功
					if (!turnPage.strQueryResult) {
						alert("没有符合条件的数据，请重新录入查询条件！");
						return false;
					}

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;

					//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
					//设置初始化过的MULTILINE对象
					turnPage.pageDisplayGrid = SetGrid;
					//保存SQL语句
					turnPage.strQuerySql = strSQL;

					//设置查询起始位置
					turnPage.pageIndex = 0;

					//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
					tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

					//调用MULTILINE对象显示查询结果
					displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
				}


				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
	//		initForm();
		}
		}

	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
	function DoReset()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}








