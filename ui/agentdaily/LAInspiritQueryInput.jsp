<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAInspiritQueryInput.jsp
//程序功能：
//创建日期：2006-1-19 10:13
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAInspiritQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAInspiritQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>业务激励查询</title>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();" >
  <!--form action="./LAAnnuityQuerySubmit.jsp" method=post name=fm target="fraSubmit"-->
  <form method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
  <table>
  	<tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAnnuity);">
      </td>
      <td class= titleImg>
        业务激励查询条件
      </td>
    </tr>
  </table>
  <Div  id= "divLAAnnuity" style= "display: ''">
  <table  class= common>
      <TR  class= common>
        <TD class= title> 
          活动名称 
        </TD>
        <TD  class= input> 
          <Input class=common  name=PloyName >
        </TD>
        <TD class= title>
          活动起期
        </TD>
        <TD  class= input>
          <Input name=StartDateF class="coolDatePicker" dateFormat="short" >
        </TD>
        <TD class= title>
          ～ 
        </TD>
        <TD class= input>
          <Input name=StartDateT class="coolDatePicker" dateFormat="short" >
        </TD>
      </TR>
      <TR  class= common>
        <TD class= title> 
          组织单位
        </TD>
        <TD  class= input> 
          <Input class=common  name=Organizer >
        </TD>
        <TD class= title>
          活动止期
        </TD>
        <TD  class= input>
          <Input name=EndDateF class="coolDatePicker" dateFormat="short" >
        </TD>
        <TD class= title>
          ～ 
        </TD>
        <TD class= input>
          <Input name=EndDateT class="coolDatePicker" dateFormat="short" >
        </TD>
      </TR>
      <TR  class= common>   
        <TD class= title>
       获奖人代码
    </TD>
    <TD class= input>
      <Input name=AgentCode class="code" verify="获奖人代码|NOTNULL&code:Agentcode" id="AgentCode"
             ondblclick="EdorType(this,AgentName);" onkeyup="KeyUp(this,AgentName);"
             elementtype=nacessary >
    </TD>
    <TD class= title>
      获奖人姓名
    </TD>
    <TD class= input> 
      <Input class= 'readonly' readonly name=AgentName>
    </TD>
        <TD class= title>
          获奖称号
        </TD>
        <TD class= input>
          <Input class=common  name=InspiritName >
        </TD>
      </TR>
  </table>
  <input type=hidden name=BranchType value=''>
  <input type=hidden name=BranchType2 value=''>
  <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();"> 
  <INPUT VALUE="返  回" class="cssbutton" TYPE=button onclick="returnParent();"> 	
  </Div>      
          				
  <table>
  	<tr>
      <td class=common>
	      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAnnuityGrid);">
  		</td>
   		<td class= titleImg>
   			 查询结果
   		</td>
   	</tr>
  </table>
	<Div  id= "divInspiritGrid" style= "display: ''">
  	<table  class= common>
   		<tr  class= common>
     		<td text-align: left colSpan=1>
		  		<span id="spanInspiritGrid" >
					</span> 
			 	</td>
			</tr>
  	</table>
    <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 						
	</div>
	<input type=hidden id="ManageCom" name="ManageCom">
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
