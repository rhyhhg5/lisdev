<%
//程序名称：ALAAscriptionQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-21 15:48:13
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ManageCom').value = "";
    fm.all('PreAgentCode').value = "";
    fm.all('AgentCode').value = "";
    fm.all('AscripState').value = "";
    fm.all('ContNo').value = "";
    
  }
  catch(ex) {
    alert("在ALAAscriptionQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLACrossGrid();  
  }
  catch(re) {
    alert("ALAAscriptionQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LACrossGrid;
function initLACrossGrid() {                               
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="归属序列号";         		//列名
    iArray[1][1]="0px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="保单号码";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="原代理人";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="原代理人姓名";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;  
    
    iArray[5]=new Array();
    iArray[5][0]="现代理人";         		//列名
    iArray[5][1]="100px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();
    iArray[6][0]="现代理人姓名";         		//列名
    iArray[6][1]="100px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;  
    
    iArray[7]=new Array();
    iArray[7][0]="代理人展业机构";         		//列名
    iArray[7][1]="0px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="归属次数";         		//列名
    iArray[8][1]="100px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="归属类别";         		//列名
    iArray[9][1]="100px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="归属类别名称";         		//列名
    iArray[10][1]="100px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;  
    
    iArray[11]=new Array();
    iArray[11][0]="是否已成历史纪录";         		//列名
    iArray[11][1]="100px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="归属日期";         		//列名
    iArray[12][1]="100px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="归属状态";         		//列名
    iArray[13][1]="100px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许
                                                                                                                                                                                                                                                                                                                                             
    iArray[14]=new Array();
    iArray[14][0]="归属状态";         		//列名
    iArray[14][1]="0px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=3;  

    iArray[15]=new Array();
    iArray[15][0]="操作员";         		//列名
    iArray[15][1]="0px";         		//宽度
    iArray[15][3]=100;         		//最大长度
    iArray[15][4]=3;     
    
    iArray[16]=new Array();
    iArray[16][0]="备注";         		//列名
    iArray[16][1]="0px";         		//宽度
    iArray[16][3]=100;         		//最大长度
    iArray[16][4]=3;     
                                                                                                                                                                                                                                                                                                                                              
    iArray[17]=new Array();
    iArray[17][0]="最近操作日";         		//列名
    iArray[17][1]="0px";         		//宽度
    iArray[17][3]=100;         		//最大长度
    iArray[17][4]=3;
    
    iArray[18]=new Array();
    iArray[18][0]="最近操作日";         		//列名
    iArray[18][1]="0px";         		//宽度
    iArray[18][3]=100;         		//最大长度
    iArray[18][4]=3;
    LACrossGrid = new MulLineEnter( "fm" , "LACrossGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACrossGrid.mulLineCount = 0;   
    LACrossGrid.displayTitle = 1;
    LACrossGrid.hiddenPlus = 1;
    LACrossGrid.hiddenSubtraction = 1;
    LACrossGrid.canSel = 1;
    LACrossGrid.canChk = 0;
    LACrossGrid.selBoxEventFuncName = "showOne";
  
    LACrossGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACrossGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
</script>
