<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAPresenceInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAPresenceSchema tLAPresenceSchema   = new LAPresenceSchema();

  ALAPresenceUI tLAPresence   = new ALAPresenceUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLAPresenceSchema.setAgentCode(request.getParameter("AgentCode"));
    
    //存储真实的AgentGroup
    tLAPresenceSchema.setBranchAttr(request.getParameter("AgentGroup"));
    tLAPresenceSchema.setAgentGroup(request.getParameter("HiddenAgentGroup"));
    
    tLAPresenceSchema.setBranchType(request.getParameter("BranchType"));
    tLAPresenceSchema.setBranchType2(request.getParameter("BranchType2"));
    tLAPresenceSchema.setManageCom(request.getParameter("ManageCom"));
    tLAPresenceSchema.setIdx(request.getParameter("Idx"));
    tLAPresenceSchema.setTimes(request.getParameter("Times"));
    tLAPresenceSchema.setSumMoney(request.getParameter("SumMoney"));
    tLAPresenceSchema.setAClass(request.getParameter("AClass"));
    tLAPresenceSchema.setDoneDate(request.getParameter("DoneDate"));
    tLAPresenceSchema.setNoti(request.getParameter("Noti"));
    tLAPresenceSchema.setOperator(request.getParameter("Operator"));

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLAPresenceSchema);
	tVData.add(tG);
  try
  {
    tLAPresence.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAPresence.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

