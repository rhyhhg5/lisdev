<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAADFundQueryInput.jsp
//程序功能：
//创建日期：2003-06-16 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAADFundQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAADFundQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>加扣款查询</title>
</head>
<body  onload="initForm();" >
  <form action="./LAADFundQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAADFund1);">
    
    <td class=titleImg>
      加扣款信息查询条件
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLAADFund1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  员工编码 
		</td>
        <td  class= input> 
		  <!--input class= common name=AgentCode --> 
		  <Input class="codeno" name=AgentCode verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName], [0,1],null,acodeSql,'1',null,500);" onkeyup="return showCodeListKey('AgentCode', [this,AgentCodeName], [0,1],null,acodeSql,'1',null,500);"><input class=codename name=AgentCodeName readonly=true >
		</td>
        <td  class= title> 
		  销售机构 
		</td>
        <td  class= input> 
		  <input class=common name=AgentGroup > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  管理机构 
		</td>

		  <!--input class=common name=ManageCom --> 
		  <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   

        <td  class= title> 
		  记录顺序号 
		</td>
        <td  class= input> 
		  <input class=common name=Idx > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  加扣款称号
		</td>
        <td  class= input> 
        	  <Input class="codeno" name=AwardTitle ondblclick="return showCodeList('adfund',[this,AwardTitleName], [0,1],null,dcodeSql,'1',null,500);" onkeyup="return showCodeListKey('adfund',[this,AwardTitleName], [0,1],null,dcodeSql,'1',null,500);"><input class=codename name=AwardTitleName readonly=true >
		  <!--input name=AwardTitle class= common  --> 
		</td>
        <td  class= title> 
		  加扣款原因
		</td>
        <td  class= input> 
		  <input name=PunishRsn class= common  > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  加扣款发放单位类别
		</td>
        <td  class= input> 
		  <input name=AClass class='codeno' ondblclick="return showCodeList('rpaclass',[this,AClassName],[0,1]);" onkeyup="return showCodeListKey('rpaclass',[this,AClassName],[0,1]);" ><input class=codename name=AClassName readonly=true >  
		</td>
        <td  class= title> 
		  发放单位
		</td>
        <td  class= input> 
		  <input class= common name=SendGrp > 
		</td>
      </tr>
      <tr  class= common> 
          <TD  class= title>
            展业类型
          </TD>
          <TD  class= input>
            <Input class="codeno" name=BranchType  ondblclick="showCodeList('branchtype',[this,BranchTypeName],[0,1],null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],[0,1],null,mcodeSql,'1',null);" ><input class=codename name=BranchTypeName readonly=true >  
          </TD>
        <td  class= title> 
		  金额
		</td>
        <td  class= input> 
		  <input name=Money class= common > 
		</td>

      </tr>
      <tr  class= common>
        <td  class= title> 
		  执行日期
		</td>
        <td  class= input> 
		  <!--input name=DoneDate class= common --> 
		  <input name=DoneDate class='coolDatePicker' dateFormat='short'>
		</td>
        <td  class= title>
		   操作员代码
		</td>
        <td  class= input>
		  <input name=Operator class=common >
		</td>
      </tr>
      
    </table>
          <!--input type=hidden name=BranchType value=''-->
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRewardPunishGrid);">
    		</td>
    		<td class= titleImg>
    			 加扣款信息结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divRewardPunishGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="SpanRewardPunishGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>


<script>
  var acodeSql = "1 and (BranchType=#2# or BranchType=#3#) and (AgentState = #01# or AgentState = #02#)";
  //var dcodeSql = "#1#";
  var dcodeSql = "1 and code<>#01#";	
  var mcodeSql = "1 and (code = #2# or code = #3#)";
</script>
