<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAddBSubSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数

  LARewardPunishSet tLARewardPunishSet   = new LARewardPunishSet();
  LAActiveAddReduceBL  tLAActiveAddReduceBL            = new LAActiveAddReduceBL();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");
  String mWageNo = request.getParameter("WageNo");
  String mManageCom  = request.getParameter("ManageCom");
  String mFlag  = request.getParameter("Flag");
  String mFlag1  = request.getParameter("Flag1");
  //如果加款有录入
  if ("2".equals(mFlag))
  {
    LARewardPunishSchema tLARewardPunishSchema   = new LARewardPunishSchema();
    tLARewardPunishSchema.setAgentCode(request.getParameter("AgentCode"));
    //###
    tLARewardPunishSchema.setIdx(request.getParameter("Idx"));
    tLARewardPunishSchema.setBranchAttr(request.getParameter("AgentGroup"));
    tLARewardPunishSchema.setAgentGroup(request.getParameter("HiddenAgentGroup"));
    tLARewardPunishSchema.setBranchType(request.getParameter("BranchType"));
    tLARewardPunishSchema.setBranchType2(request.getParameter("BranchType2"));
    tLARewardPunishSchema.setManageCom(request.getParameter("ManageCom"));
   
    tLARewardPunishSchema.setAClass("0");
    //-----------------------------------------------加 款记录
    tLARewardPunishSchema.setMoney(request.getParameter("Money"));
    tLARewardPunishSchema.setDoneFlag(request.getParameter("DoneFlag"));
    tLARewardPunishSchema.setAwardTitle(request.getParameter("PunishRsn"));//加款原因
    //------------------------------
    tLARewardPunishSchema.setWageNo(request.getParameter("WageNo"));
    tLARewardPunishSchema.setSendGrp(request.getParameter("SendGrp"));
    tLARewardPunishSchema.setNoti(request.getParameter("Noti"));
    tLARewardPunishSchema.setOperator(tG.Operator);
    tLARewardPunishSet.add(tLARewardPunishSchema);
  }
  //如果扣款有录入
  if ("2".equals(mFlag1))
  {
    LARewardPunishSchema tLARewardPunishSchema   = new LARewardPunishSchema();
    tLARewardPunishSchema.setAgentCode(request.getParameter("AgentCode"));
    //###
    tLARewardPunishSchema.setIdx(request.getParameter("Idx"));
    tLARewardPunishSchema.setBranchAttr(request.getParameter("AgentGroup"));
    tLARewardPunishSchema.setAgentGroup(request.getParameter("HiddenAgentGroup"));
    tLARewardPunishSchema.setBranchType(request.getParameter("BranchType"));
    tLARewardPunishSchema.setBranchType2(request.getParameter("BranchType2"));
    tLARewardPunishSchema.setManageCom(request.getParameter("ManageCom"));
   
    tLARewardPunishSchema.setAClass("1");
    //-----------------------------------------------扣款记录
    tLARewardPunishSchema.setMoney(request.getParameter("Money1"));
    //为了测试 扩字段tLARewardPunishSchema.setDoneFlag(request.getParameter("DoneFlag1"));
    //tLARewardPunishSchema.setDoneFlag("3");
    tLARewardPunishSchema.setDoneFlag(request.getParameter("DoneFlag1"));
    tLARewardPunishSchema.setPunishRsn(request.getParameter("PunishRsn1"));//扣 款原因
    //-----------------------------------------------   
    tLARewardPunishSchema.setWageNo(request.getParameter("WageNo"));    
    tLARewardPunishSchema.setSendGrp(request.getParameter("SendGrp"));
    tLARewardPunishSchema.setNoti(request.getParameter("Noti"));
    tLARewardPunishSchema.setOperator(tG.Operator);
    tLARewardPunishSet.add(tLARewardPunishSchema);
  }    


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.add(mWageNo);
	tVData.add(mManageCom);
	tVData.addElement(tLARewardPunishSet);
	
  try
  {
    tLAActiveAddReduceBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAActiveAddReduceBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
