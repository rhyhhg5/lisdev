<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAccountsSave.jsp
//程序功能：
//创建日期：2005-03-20 18:03:35
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAccountsSchema tLAAccountsSchema   = new LAAccountsSchema();
  OLAAccountsUI tOLAAccountsUI   = new OLAAccountsUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  request.setCharacterEncoding("GBK");
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   transact = request.getParameter("fmtransact");
    tLAAccountsSchema.setAgentCode(request.getParameter("AgentCode"));
    tLAAccountsSchema.setAccount(request.getParameter("Account"));
    tLAAccountsSchema.setState(request.getParameter("State"));
    tLAAccountsSchema.setBank(request.getParameter("Bank"));
    tLAAccountsSchema.setOpenDate(request.getParameter("OpenDate"));
    tLAAccountsSchema.setDestoryDate(request.getParameter("DestoryDate"));
    tLAAccountsSchema.setAccountName(request.getParameter("AgentName"));
    tLAAccountsSchema.setOperator(request.getParameter("Operator"));

	TransferData tTransferData= new TransferData();
	tTransferData.setNameAndValue("AgentCodeOld",request.getParameter("AgentCodeOld"));
	tTransferData.setNameAndValue("AccountOld",request.getParameter("AccountOld"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	System.out.println(transact);
	tVData.add(tLAAccountsSchema);
  	tVData.add(tG);
	tVData.add(tTransferData);
    tOLAAccountsUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLAAccountsUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
