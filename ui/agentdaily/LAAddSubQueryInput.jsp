<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LARewardPunishQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAAddSubQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAddSubQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>奖惩查询</title>
</head>  
<body  onload="initForm();" >
  <form action="./LAAddSubQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLARewardPunish1);">
    
    <td class=titleImg>
      奖惩信息查询条件
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLARewardPunish1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  营销员代码
		</td>
        <td  class= input> 
		  <input class= common name=AgentCode > 
		</td>
        <td  class= title> 
		  销售单位
		</td>
        <td  class= input> 
		  <input class=common name=AgentGroup > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class=common name=ManageCom > 
		</td>
        <td  class= title> 
		  纪录顺序号 
		</td>
        <td  class= input> 
		  <input class=common name=Idx > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  奖励称号
		</td>
        <td  class= input> 
		  <input name=AwardTitle class= common  > 
		</td>
        <td  class= title> 
		  奖惩原因
		</td>
        <td  class= input> 
		  <input name=PunishRsn class= common  > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  奖惩发放单位类别
		</td>
        <td  class= input> 
		  <input name=AClass class='codeno' 
		   ondblclick="return showCodeList('rpaclass',[this,AClassName],[0,1]);" 
		   onkeyup="return showCodeListKey('rpaclass',[this,AClassName],[0,1]);" 
		  ><Input name=AClassName class="codename"> 
		</td>
        <td  class= title> 
		  发放单位
		</td>
        <td  class= input> 
		  <input class= common name=SendGrp > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  金额
		</td>
        <td  class= input> 
		  <input name=Money class= common > 
		</td>
        <td  class= title> 
		  批注
		</td>
        <td  class= input> 
		  <input name=Noti class= common > 
		</td>
      </tr>
      <tr  class= common>
        <td  class= title> 
		  执行日期
		</td>
        <td  class= input> 
		  <input name=DoneDate class= common > 
		</td>
        <td  class= title>
		   操作员代码
		</td>
        <td  class= input>
		  <input name=Operator class=common >
		</td>
      </tr>
      <tr class=common>
        <td class=title>加扣款类型</td>
        <td class=input><input name=DoneFlag class=codeno name="DoneFlag"
         CodeData="0|^1|附加佣金加扣款|^2|同业衔接加扣款"
         ondblClick="showCodeListEx('DoneFlagList',[this],[0,1]);"
         onkeyup="showCodeListKeyEx('DoneFlagList',[this],[0,1]);"
        ><Input name=DoneFlagName class="codename">
        </td>
        <td class=input colspan=2></td>
      </tr>          
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRewardPunishGrid);">
    		</td>
    		<td class= titleImg>
    			 奖惩信息结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divRewardPunishGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="SpanRewardPunishGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
