<%
//程序名称：DataIntoLACommisionInit.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value ='';
    fm.all('AgentCom').value='';
    fm.all('AgentComName').value ='';
    fm.all('StartDate').value='';
    fm.all('EndDate').value ='';
    fm.all('AppntName').value ='';
    fm.all('BranchType').value = mBranchType;
   	//fm.all('BranchType2').value = mBranchType2;
  }
  catch(ex)
  {
    alert("PremiumGatherInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
   // alert(121);
    initLACommGrid(); 
   
  }
  catch(re)
  {
    alert("PremiumGatherInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LACommGrid;
function initLACommGrid() {  
//alert(12);                            
  var iArray = new Array();
//  alert(12);
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构编码";         		//列名
    iArray[1][1]="80px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="中介机构编码";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    iArray[4]=new Array();
    iArray[4][0]="中介机构名称";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="投保单位";         		//列名
    iArray[5][1]="80px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许    
    
    iArray[6]=new Array();
    iArray[6][0]="保费收入";         		//列名
    iArray[6][1]="60px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="应付手续费";         		//列名
    iArray[7][1]="60px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
           
    iArray[8]=new Array();
    iArray[8][0]="实付手续费";         		//列名
    iArray[8][1]="80px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    LACommGrid = new MulLineEnter( "fm" , "LACommGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACommGrid.mulLineCount = 0;   
    LACommGrid.displayTitle = 1;
    LACommGrid.hiddenPlus = 1;
    LACommGrid.hiddenSubtraction = 1;
    LACommGrid.canSel = 0;
    LACommGrid.canChk = 0;
  //  LACommisionGrid.selBoxEventFuncName = "showOne";
  
    LACommGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACommisionGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


</script>
