//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;

//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    } 
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	if (mOperate=="")
	{
		addClick();
	}
	 //if (mOperate == "QUERY||MAIN"){
   	//initForm();
   	//alert("请重新输入信息！");
   	//return false;
  //}
  //if (!beforeSubmit())
  //alert(mOperate);
   //if (mOperate == "QUERY||MAIN"){
   	//initForm();
   	//alert("请重新输入信息！");
   	//return false;
  //}
   if (!beforeSubmit())
   return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  mOperate=""
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    //parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    resetForm();
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("在LATrainInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if(fm.all('AgentCode').value==null || trim(fm.all('AgentCode').value)=="")
  {
    alert("请输入代理人编码！");
    return false;
  }
  
  // 检验输入长度
  // 培训名称
  if(!strLen(document.fm.TrainName.value,120,"[培训名称]请录入40个汉字或120个数字或字母！"))
    return false;
  // 培训主办单位
  if(!strLen(document.fm.TrainUnit.value,20,"[培训主办单位]请录入6个汉字或20个数字或字母！"))
    return false;
  // 讲师
  if(!strLen(document.fm.Charger.value,20,"[讲师]请录入6个汉字或20个数字或字母！"))
    return false;
  // 成绩
  if(!strLen(document.fm.Result.value,50,"[成绩]请录入16个汉字或50个数字或字母！"))
    return false;
  // 批注
  if(!strLen(document.fm.Noti.value,100,"[批注]请录入33个汉字或100个数字或字母！"))
    return false;
  
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if ((fm.all("AgentCode").value==null)||(trim(fm.all("AgentCode").value)==''))
    alert('请先查询出要修改的纪录！');
  else if ((fm.all("Idx").value==null)||(trim(fm.all("Idx").value)==''))
    alert('请先查询出要修改的纪录！');
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LATrainQuery.html?BranchType="+fm.all('BranchType').value+"");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert('请先查询出要删除的纪录！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要删除的纪录！');
  else
  {
    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";  
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//验证代理人编码的合理性
function checkValid()
{ 
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  if(fm.GroupAgentCode.value !=null && fm.GroupAgentCode.value != ""){
  	var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+fm.GroupAgentCode.value+"' ";
  	var strQueryResult = easyExecSql(tYAgentCodeSQL);
  	if(strQueryResult == null){
  		alert("获取业务员信息失败！");
  		return false;
  	}
  	fm.AgentCode.value = strQueryResult[0][0];
  }
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select * from LAAgent where '1'='1' "
	     + getWherePart('AgentCode')
	     +" and ((AgentState <'06' ) or (AgentState is null))"
	     +" and branchtype='"+tBranchType+"'";
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此代理人！");
    fm.all('AgentCode').value  = "";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  //<rem>######//
  //fm.all('AgentGroup').value = tArr[0][1];
  //</rem>######//
  fm.all('Name').value = tArr[0][5];
  fm.all('ManageCom').value = tArr[0][2];
  //存储原AgentGroup值，以备保存时使用  
  //<addcode>############################################################//
  old_AgentGroup=tArr[0][74];
  
  fm.all('HiddenAgentGroup').value=tArr[0][1];
  
  strSQL_AgentGroup = "select trim(BranchAttr) from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"'"
     var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  //var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  //以备显示时使用
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
    //alert(new_AgentGroup);
  //</addcode>############################################################//
}


//用来显示返回的选项
function afterQuery(arrQueryResult)
{	

  var arrResult = new Array();
  //alert(arrQueryResult);
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
   
    fm.all('GroupAgentCode').value = arrResult[0][0];
    var tYAgentCodeSQL = "select agentcode,agentgroup from laagent where groupagentcode = '"+arrResult[0][0]+"' ";
    var strQueryResult = easyExecSql(tYAgentCodeSQL);
    if(strQueryResult == null){
    	alert("获取业务员工号失败！");
    	return false;
    }
    fm.all('AgentCode').value = strQueryResult[0][0];
    fm.all('Name').value = arrResult[0][1];
    fm.all('AgentGroup').value = arrResult[0][2]
    fm.all('ManageCom').value = arrResult[0][3];
   
    fm.all('AClass').value = arrResult[0][4];
   fm.all('TrainCode').value=arrResult[0][5];
   fm.all('TrainName').value=arrResult[0][6];
    
    fm.all('TrainStart').value = arrResult[0][7];
    fm.all('TrainEnd').value = arrResult[0][8];
    fm.all('TrainUnit').value = arrResult[0][9];
    fm.all('Charger').value = arrResult[0][10];
    
    fm.all('Result').value = arrResult[0][11];
    //fm.all('ResultLevel').value = arrResult[0][12];
    fm.all('TrainPassFlag').value = arrResult[0][13];
     fm.all('Noti').value = arrResult[0][14];
    fm.all('DoneDate').value = arrResult[0][15];
     fm.all('Idx').value = arrResult[0][16];
      fm.all('Operator').value = arrResult[0][17];
    
    //<addcode>############################################################//
    fm.all('HiddenAgentGroup').value=strQueryResult[0][1];
    //</addcode>############################################################//
                                                                                                                                                                                                                                      	
  }
    
}

/***************************************
 * 验证字符的字节长度是否超标
 * 参数：pmStr      准备验证的字符串
 *       pmStrLen   长度标准
 *       pmMissage  报错信息
 * 返回：返回字符串字节长度
 ***************************************/
function strLen(pmStr,pmStrLen,pmMissage)
{
	var i;
  var len;
  len = 0;
  for (i=0;i<pmStr.length;i++)
  {
    if (pmStr.charCodeAt(i)>255) len+=3; else len++;
  }
  //alert("[" + pmStr + "] 长度为： " + len + " 规定长度为："+pmStrLen);
  if(len > pmStrLen)
  {
  	alert(pmMissage);
  	return false;
  }
  
  return true;
}