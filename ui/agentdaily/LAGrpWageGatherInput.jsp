<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
//程序名称：LAWageGatherInput.jsp
//程序功能：当前没有主管的销售单位查询
//创建日期：2005-06-03 9:45
//创建人  ： LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="LAGrpWageGatherInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAGrpWageGatherInit.jsp"%>
  <title> 数据汇总 </title>
</head>
<body onload="initForm();">
  <form method=post name=fm target="fraSubmit" action="./LAGrpWageGatherSave.jsp">
    <!-- 集体信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			  <td class= titleImg align= center>请输入查询条件：</td>
		  </tr>
	  </table>
    <table  class= common align=center>
      <TR  class= common>
        <TD class = title>
           管理机构
        </TD>
        <TD  class= input>
          <Input class="codeno" name=ManageCom 
           ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'4','to_char(length(trim(comcode)))',1);"
           onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,'4','to_char(length(trim(comcode)))',1);"
           verify="管理机构|notnull&code:comcode" ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
        </TD>
         <TD  class= title>
          回退年月
        </TD>
        <TD  class= input>
          <Input name='YearMounth' class='common' elementtype=nacessary verify="回退年月|notnull&len=6"><font color="red">(yyyymm)
        </TD>
    </table>
    <input type=hidden name=BranchType value=''> 
    <input type=hidden name=BranchType2 value=''>      
    <INPUT VALUE="回  退" class=cssButton  TYPE=button onclick="ApplyInput();">
  </form>
	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>