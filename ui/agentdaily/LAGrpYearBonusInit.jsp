<%
//程序名称：LAGroupZTRateCommSetInit.jsp
//程序功能：职团险种提奖比例录入
//创建时间：2008-05-15
//创建人  ：Elsa
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom=" 1 and char(length(trim(comcode))) = (#8#) ";
 var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and riskprop = #I#)";
 
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('BranchAttr').value='';
    fm.all('AgentCode').value='';
    fm.all('AgentName').value='';
    fm.all('WageNo').value='';    
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>'; 
    fm2.all('BranchType').value = '<%=BranchType%>';
    fm2.all('BranchType2').value = '<%=BranchType2%>'; 
    fm2.diskimporttype.value='LAAttend';       
    fm.all('GroupAgentCode').value='';      
  }
  catch(ex)
  {
    alert("在LAGrpYearBonusInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[1][4]="ComCode";
    iArray[1][9]="管理机构|NotNull&NUM&len=8";      
    iArray[1][15]="1";
    iArray[1][16]=tmanagecom;
    
    
    iArray[2]=new Array();
    iArray[2][0]="销售机构编码"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[2][4]="branchattr2";
    iArray[2][5]="2|3|4";
    iArray[2][6]="0|1|2"
    iArray[2][9]="代理机构";
    iArray[2][15]="managecom";
    iArray[2][17]= "1"; 
    
    
    iArray[3]=new Array();
    iArray[3][0]="销售机构名称"; //列名
    iArray[3][1]="120px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    
    iArray[4]=new Array();
    iArray[4][0]="内部编码"; //列名
    iArray[4][1]="0px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[5]=new Array();
    iArray[5][0]="业务员代码"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[5][4]="agentcode";
    iArray[5][5]="5|6";
    iArray[5][6]="0|1"
    iArray[5][9]="业务员代码";
    iArray[5][15]="agentgroup";
    iArray[5][17]= "4"; 
    
       
    iArray[6]=new Array();
    iArray[6][0]="业务员姓名"; //列名
    iArray[6][1]="50px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许
       
    iArray[7]=new Array();
    iArray[7][0]="薪资年月"; //列名
    iArray[7][1]="50px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许         
   
    iArray[8]=new Array();
    iArray[8][0]="效益奖"; //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许    
 
    iArray[9]=new Array();
    iArray[9][0]="序号"; //列名
    iArray[9][1]="00px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="原薪资月"; //列名
    iArray[10][1]="00px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="原管理机构"; //列名
    iArray[11][1]="00px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="原销售机构编码"; //列名
    iArray[12][1]="00px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[13]=new Array();
    iArray[13][0]="原业务员编码"; //列名
    iArray[13][1]="00px";        //列宽
    iArray[13][2]=100;            //列最大值
    iArray[13][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[14]=new Array();
    iArray[14][0]="原集团业务员编码"; //列名
    iArray[14][1]="00px";        //列宽
    iArray[14][2]=100;            //列最大值
    iArray[14][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
    SetGrid.displayTitle = 1;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAGrpYearBonusInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}

function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	iArray[1][0]="导入文件批次";          		//列名
	iArray[1][1]="120px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    initImportResultGrid();
    
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>