<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";


  TempWageBL tTempWageBL = new TempWageBL();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
  //    String tAction = request.getParameter("fmAction");
      LAWageSet tLAWageSet = new LAWageSet();
      String tChk[] = request.getParameterValues("InpSetGridChk");    
      String tIndexcalno[] = request.getParameterValues("SetGrid1");
      String tAgentCode[] = request.getParameterValues("SetGrid2");
      String tAgentGrade[] = request.getParameterValues("SetGrid3");
	 //获取最大的ID号      
      //创建数据集
      for(int i=0;i<tChk.length;i++)
      {
      	//创建一个新的Schema
      	  LAWageSchema tSch = new LAWageSchema();      		
          tSch.setAgentCode(tAgentCode[i]);
          tSch.setAgentGrade(tAgentGrade[i]);
          tSch.setIndexCalNo(tIndexcalno[i]);
         
          tLAWageSet.add(tSch);     
          System.out.println("tLAWageSet get"+tLAWageSet.size());
      }
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";      
      tVData.add(tGI);    
      tVData.add(request.getParameter("ManageCom"));
      tVData.add(request.getParameter("wageno"));
      tVData.add(request.getParameter("wagenoend"));
      tVData.add(tLAWageSet);
      //没有更新或删除或插入的数据  	
       tTempWageBL.submitData(tVData,"");
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tTempWageBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
