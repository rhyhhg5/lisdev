<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html> 
<%
//程序名称：LANoLeadGrpQueryInput.jsp
//程序功能：当前没有主管的销售单位查询
//创建日期：2005-5-24 14:54
//创建人  ： LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="LANoLeadGrpQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LANoLeadGrpQueryInit.jsp"%>
  <title> 无主管销售单位 </title>
</head>
<body onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 集体信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
     <TR  class= common>
        <TD  class= title>
          管理机构
        </TD>
        <TD  class= input>
          <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
        </TD>
        <TD  class= title>
          销售机构
        </TD>
        <TD  class= input>
          <Input class="common" name=BranchAttr>
        </TD>
      </TR>
    </table>
    <INPUT VALUE="查  询" class=cssButton  TYPE=button onclick="easyQueryClick();">
    
    <!-- 没有主管的销售单位（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSpace);">
    		</td>
    		<td class= titleImg>
    			 单位列表
    		</td>
    	</tr>
    </table>
	<Div  id= "divSpace" style= "display: ''" align = center>
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanNoLeadGrpGrid" >
					</span> 
				</td>
			</tr>
		</table>
		
    <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();">
    <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();">
    <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();">
    <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>