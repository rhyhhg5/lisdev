<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LARewardPunishQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAPresenceAddSubQuery.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAPresenceAddSubQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>差勤扣款查询</title>
</head>  
<body  onload="initForm();initElementtype();" >
  <form action="./LAAddSubQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLARewardPunish1);">
    
    <td class=titleImg>
      差勤扣款查询条件
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLARewardPunish1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  营销员代码 
		</td>
        <td  class= input> 
		  <input class= common name=GroupAgentCode OnChange="return checkvalid();"> 
		</td>
        <td  class= title> 
		  销售单位
		</td>
        <td  class= input> 
		  <input class=common name=AgentGroup > 
		</td>
      </tr>
      <tr  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD> 
   
      </tr>
       
      <tr  class= common> 
        <td  class= title> 
		  金额
		</td>
        <td  class= input> 
		  <input name=Money class= common > 
		</td>
        <TD  class= title width="25%"> 
		  执行日期
		</td>
        <TD  class= input  width="25%"  >
        <Input class= "coolDatePicker" dateFormat="short" name=DoneDate verify="执行日期|Date"  > 
	</td>
        <tr  class= common> 
        <td  class= title>
		   操作员代码
		</td>
        <td  class= input>
		  <input name=Operator class=common >
		</td>
      </tr>
              
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=AgentCode value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRewardPunishGrid);">
    		</td>
    		<td class= titleImg>
    			 差勤扣款结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divRewardPunishGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="SpanRewardPunishGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  <input type=hidden name=Idx value=''>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
