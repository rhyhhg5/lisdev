<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LAGroupZTRateCommSetSave.jsp
//程序功能：
//创建日期：2008-05-15
//创建人  ：   Elsa
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  LAGrpYearBonusUI tLAGrpYearBonusUI = new LAGrpYearBonusUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      System.out.println("前台传过来的操作符："+tAction);
      LAAttendSet tSetU = new LAAttendSet();//用于更新
      LAAttendSet tSetI = new LAAttendSet();//用于插入
      LAAttendSet tSetD = new LAAttendSet();//用于删除
           
      String tChk[] = request.getParameterValues("InpSetGridChk"); 
      String tManageCom[] = request.getParameterValues("SetGrid1");
      String tBranchAttr[] = request.getParameterValues("SetGrid2");
      String tAgentGroup[] = request.getParameterValues("SetGrid4");
      String tAgentCode[] = request.getParameterValues("SetGrid13");
      String tWageNo[] = request.getParameterValues("SetGrid7");
      String tYearBonus[] = request.getParameterValues("SetGrid8");
	  String tSerialno[]=request.getParameterValues("SetGrid9");
	  String tGroupAgentCode[] = request.getParameterValues("SetGrid5");
      //创建数据集
      for(int i=0;i<tChk.length;i++)
      {
      	if(tChk[i].equals("1"))
      	{
      		//创建一个新的Schema
      	  LAAttendSchema tSch = new LAAttendSchema();
      	  tSch.setManagecom(tManageCom[i].trim());
      	  tSch.setAgentGroup(tAgentGroup[i].trim());
          tSch.setBranchType("2");
          tSch.setBranchType2("01");
          if(tAgentCode[i].trim() != null && !"".equals(tAgentCode[i].trim())){
          	tSch.setAgentCode(tAgentCode[i].trim());
          }else{
          	String tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+tGroupAgentCode[i]+"' ";
          	String tYAgentCode = new ExeSQL().getOneValue(tYAgentCodeSQL);
          	tSch.setAgentCode(tYAgentCode);
          }
          
          tSch.setSummoney(Double.parseDouble(tYearBonus[i].trim()));
          tSch.setWageNo(tWageNo[i].trim());
          System.out.println("前台传进来wageno值为："+tSch.getWageNo());
          
      		if((tSerialno[i] == null)||(tSerialno[i].equals("")))
      		{
      			//需要插入记录
      			tSetI.add(tSch);
       		}
      		else
      		{
            if(tAction.trim().equals("UPDATE"))      			      	
      			{
      			tSch.setIdx(tSerialno[i]);      			
      			tSetU.add(tSch);
      			}
            System.out.println("前台传进来tAction值为："+tAction);
            if(tAction.trim().equals("DELETE"))      			      	
  			{
  			  tSch.setIdx(tSerialno[i]);      			
  			  tSetD.add(tSch);
  			}
      		 
       		}
      	}      	
      }
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      
      tVData.add(tGI);
      //没有更新或删除或插入的数据
      if ((tSetU.size() == 0)&&(tSetI.size() == 0)&&(tSetD.size() == 0))
      {
        	FlagStr = "Fail";
        	Content = FlagStr + "未选中要处理的数据！";      	
      }
    else if (tSetI.size() != 0)
    	{
        	//只有插入数据
        	tVData.add(tSetI);
        	System.out.println("Start LAGroupRateCommSetUI Submit...INSERT");
        	tLAGrpYearBonusUI.submitData(tVData,"INSERT");        	    	
    	}
    else if (tSetU.size() != 0)
    	{
        	//只有删除数据
        	tVData.add(tSetU);
        	System.out.println("Start LAGroupRateCommSetUI Submit...UPDATE");
        	tLAGrpYearBonusUI.submitData(tVData,"UPDATE");        	    	
    	}
    else if (tSetD.size() != 0)
	{
    	//只有删除数据
    	tVData.add(tSetD);
    	System.out.println("Start LAGroupRateCommSetUI Submit...DELETE");
    	tLAGrpYearBonusUI.submitData(tVData,"DELETE");        	    	
	}

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAGrpYearBonusUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
