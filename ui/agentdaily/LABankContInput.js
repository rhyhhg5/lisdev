//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,ProtocalNo )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    fm.all('ProtocolNo').value=ProtocalNo;
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LACont.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
	//添加操作
	if( verifyInput() == false ) return false;

	var tAgentCom=fm.all('AgentCom').value;
	
	if (tAgentCom==null || tAgentCom=='')
	{
		alert('请录入签署银邮机构');
		return false;
	}
	
	if (trim(fm.all('StartDate').value) >= trim(fm.all('EndDate').value))
	{
		alert('请输入正确的协议时间范围');
		return false;
	}
	
	if (trim(fm.all('SignDate').value) > trim(fm.all('StartDate').value))
	{
		alert('请输入正确的签约时间');
		return false;
	}
	var sql="select ACType,OtherSign from LACom a,lDCode where AgentCom='"+tAgentCom+"' and ACType=Code and codetype = 'actype'"
	+ getWherePart("BranchType", "BranchType")
	+ getWherePart("BranchType2", "BranchType2");
	var Result = easyQueryVer3(sql, 1, 0, 1);
	if (!Result)
	{
		alert("签署银邮机构录入不正确!");
		return false;
	}
	var Result_Arr=decodeEasyQueryResult(Result);
	
	// 验证是否填写险种及险种比例
	var rowNum=ContGrid.mulLineCount;
	var i,j;
	for(i=0;i<rowNum;i++)
	{
		//alert(ContGrid.getRowColData(i,1) + "   "+ContGrid.getRowColData(i,3));
		if((ContGrid.getRowColData(i,1)=="" || null == ContGrid.getRowColData(i,1)) ||
		   (ContGrid.getRowColData(i,3)=="" || null == ContGrid.getRowColData(i,3)))
		{
			alert("请选择协议险种，并填写框架协议手续费比例，不要有空行！");
			return false;
		}
		// 对手续费比例 作数字验证
		if(!isNumeric(ContGrid.getRowColData(i,3)))
		{
			alert("险种["+ContGrid.getRowColData(i,1)+"]的手续费比例录入非法！请录入数字！");
			return false;
		}
		// 如果录入最高保额 进行数字验证
		if(ContGrid.getRowColData(i,5) != "" && null != ContGrid.getRowColData(i,5))
		{
			if(!isNumeric(ContGrid.getRowColData(i,5)))
			{
				alert("险种["+ContGrid.getRowColData(i,1)+"]的最高保额录入非法！请录入数字！");
				return false;
			}
		}
		// 判断是否有重复的险种
		for(j=0;j<rowNum;j++)
		{
			if(j == i)
			{
				continue;
			}
			if(ContGrid.getRowColData(i,1) == ContGrid.getRowColData(j,1))
			{
				alert("险种["+ContGrid.getRowColData(i,1)+"]出现多个！请不要重复录入险种");
				return false;
			}
		}
	}
	
	
////////if (Result_Arr[0][1]!=fm.all('ACType').value)
////////{
////////	alert("录入的中介机构不是代理公司!");
////////	return false;
////////}
        return true;
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
  if(!chkADD()){
    return false;	
  }
  if (!beforeSubmit())
  {
  	return false;
  }
   var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	 if (!beforeSubmit())
  {
  	return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}                    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//Click事件，当点击“查询”图片时触发该函数
function QueryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all("BranchType").value;
  showInfo=window.open("./LABankContQueryhtml.jsp?BranchType="+tBranchType+"");
}   
function easyQueryClick() {
	initContGrid();
	//此处书写SQL语句			     			    
	var strSql = "select ProtocolNo,SignDate,ManageCom,AgentCom,RepresentA,RepresentB,StartDate,EndDate from LACont where 1=1 "
    + getWherePart('ProtocolNo', 'ProtocolNo')
    + getWherePart('SignDate', 'SignDate')
    + getWherePart('ManageCom', 'ManageCom')
    + getWherePart('AgentCom', 'AgentCom');
    + getWherePart("RepresentA", "RepresentA")
    + getWherePart("RepresentB", "RepresentB")
    + getWherePart("StartDate", "StartDate")
    + getWherePart("EndDate", "EndDate")
     
  ;
	//turnPage.queryModal(strSql, ContGrid);
		turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);
 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
   //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.ContGrid = ContGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.ContGrid);
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = ArchieveGrid.getRowData(tRow-1);	
	return arrSelected;
}             
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		    if(arrResult[0][0]!="")
        	    
        fm.all('ProtocolNo').value= arrResult[0][0];
        if(arrResult[0][1]!="")
        fm.all('SignDate').value= arrResult[0][1];
        if(arrResult[0][2]!="")
        fm.all('ManageCom').value= arrResult[0][2];
        if(arrResult[0][3]!="")
        
        fm.all('AgentCom').value=arrResult[0][3];
        
        if(arrResult[0][4]!="")
        fm.all('RepresentA').value= arrResult[0][4];
        if(arrResult[0][5]!="")
        fm.all('RepresentB').value= arrResult[0][5];
        if(arrResult[0][6]!="")
        fm.all('StartDate').value= arrResult[0][6];
        
        if(arrResult[0][7]!="")
        fm.all('EndDate').value= arrResult[0][7];
        if(arrResult[0][8]!="")
        {        
        }        
        if(arrResult[0][9]!="" || arrResult[0][9]!=null)
        {      
        	fm.all('BranchType2').value= arrResult[0][9]; 
        	
        	fm.all('BranchType2Name').value= "银邮代理";   
        }
        
       initContGrid();     
    
	//此处书写SQL语句riskcode=LACont.riskcode and 		
	var strSql = "select riskcode,(select RiskName from LMRisk where a.riskcode=riskcode),(select value(rate,0) from laratecharge where OtherNo=LACont.ProtocolNo and PayIntv=0 and StartYear=0 and EndYear=999  and Riskcode=a.riskcode and caltype ='510'),AuthorizeArea,MaxAmnt from LACont,LAAuthorize a where 1=1 and ProtocolNo='"+arrResult[0][0]+"' and a.AuthorObj=LACont.ProtocolNo and a.AuthorType='0' ";     	
try
  {		    
	   
		 turnPage.strQueryResult = easyQueryVer3(strSql, 1, 0, 1);
	}catch(Ex)
	{
		alert(Ex);
	}
		 
 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert("查询失败！");
    return false;
    }
    
   //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.ContGrid = ContGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.ContGrid);
       return true
	}
}             
function chkADD(){
	
	var tProtocolNo = fm.all('ProtocolNo').value;	
	
  var sql="select ProtocolNo from LACont where ProtocolNo='"+tProtocolNo+"'";	
	var Result = easyQueryVer3(sql, 1, 0, 1);
	if (Result)
	{
		alert("该协议号已存在,请执行修改操作!");
		return false;
	}
	return true;
}
function getAgentCom(cObj,cName)
{
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
    {
		//showCodeList('agentcom',[cObj,cName],[0,1],null,'1 and BranchType=#3# and BranchType2=#01# ',1,1);
		alert("在选择签署银邮机构之前请先录入签署管理机构！");
		return false;
   	}
	else if (fm.all('ManageCom').value != null || trim(fm.all('ManageCom').value) != '') 
	{
		var strsql =" 1 and BranchType=#3# and BranchType2=#01#  and managecom like #" + fm.all('ManageCom').value + "%# " ;
		showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,1,1);
	}
}							