<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：DiskImportAgentSave.jsp
//程序功能：代理人相关信息磁盘导入上传
//创建日期：2006-09-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
//MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	
//System.out.println("............diskimportagentsave:diskimporttype:"+multipartRequest.getParameter("diskimporttype"));

%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.agentdaily.*"%>
<%
	String flag = "";
	String content = "";
	String path = "";      //文件上传路径
	String fileName = "";  //上传文件名
	String diskimporttype="";
	int count=0;
	//得到全局变量
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	
    //得到excel文件的保存路径
    String sql = "select SysvarValue "
                + "from ldsysvar "
                + "where sysvar='SaleXmlPath'";
    ExeSQL tExeSQL = new ExeSQL();
    String subPath = tExeSQL.getOneValue(sql);
 	path = application.getRealPath(subPath);
	if(!path.endsWith("/")){
      path += "/";
	}
	 
	//System.out.println("path : " + path);
	
	DiskFileUpload fu = new DiskFileUpload();
	// 设置允许用户上传文件大小,单位:字节
	fu.setSizeMax(10000000);
	// maximum size that will be stored in memory?
	// 设置最多只允许在内存中存储的数据,单位:字节
	fu.setSizeThreshold(4096);//4096
	// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
	fu.setRepositoryPath(path);
	//开始读取上传信息
	List fileItems=null;
	try
	{
	   fileItems = fu.parseRequest(request);
	}
	catch (Exception e){
		e.printStackTrace();
	}

	String ImportPath = "";

	// 依次处理每个上传的文件
	Iterator iter = fileItems.iterator();
	while (iter.hasNext())
	{
	  FileItem item = (FileItem) iter.next();
	  //if (item.getFieldName().compareTo("ImportPath")==0)
	  //{
	    //ImportPath = item.getString();
	    //System.out.println("上传路径:"+ImportPath);
	  //}
	  if(item.getFieldName().compareTo("diskimporttype")==0){
	  	diskimporttype=item.getString();
	  	//System.out.println("........save:diskimporttype"+diskimporttype);
	  	System.out.println("diskimporttype : " + diskimporttype);
	  }
	  //忽略其他不是文件域的所有表单信息
	  if (!item.isFormField())
	  {
	    String name = item.getName();
	    
	    System.out.println("name : " + name);
	    long size = item.getSize();
	
	    if((name==null||name.equals("")) && size==0)
	      continue;
	    ImportPath= path ;
	//    ImportPath= path + ImportPath;
		ImportPath = ImportPath.replace('\\','/');
	    fileName = name.replace('\\','/');
	    fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
	    System.out.println("fileName(ImportPath + fileName):"+ImportPath + fileName);
	    
	    //保存上传的文件到指定的目录
	    try
	    {
	    //System.out.println("..............savejsphere:once");
	      item.write(new File(ImportPath + fileName));
	      count = 1;
	     // System.out.println("count:"+count);
	    }
	    catch(Exception e){
	      System.out.println("upload file error ...");
	    }
	  }
	}
	
	TransferData t = new TransferData();
	t.setNameAndValue("diskimporttype", diskimporttype);
	//System.out.println(".................get  diskimporttype here!"+diskimporttype);	
	t.setNameAndValue("path", path);
	System.out.println(".................get  path here!"+path);	
	t.setNameAndValue("fileName", fileName);
	//System.out.println(".................get  filename here!"+fileName);	
	VData v = new VData();
	v.add(t);
	v.add(tGI);	

	//从磁盘导入代理人相关信息清单
	DiskImportAgentInfoUI tDiskImportAgentInfoUI = new DiskImportAgentInfoUI();
	if (!tDiskImportAgentInfoUI.submitData(v, "INSERT"))
	{
        flag = "Fail";
        content = tDiskImportAgentInfoUI.mErrors.getErrContent();
        System.out.println(tDiskImportAgentInfoUI.mErrors.getFirstError());
	}
	else
	{
        flag = "Succ";
        if(tDiskImportAgentInfoUI.getImportPersons() != 0)
        {
            content = "成功导入" + tDiskImportAgentInfoUI.getImportPersons() + "条记录";
        }
        
        //执行成功，但是有未导入信息
        if(tDiskImportAgentInfoUI.mErrors.needDealError())
        {
            content += tDiskImportAgentInfoUI.mErrors.getErrContent();
        }
	}
	System.out.println(content);
	content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>

