<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAZTContFYCRateSave.jsp
//程序功能：
//创建日期：2008-07-01 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  String tWageNo = request.getParameter("WageNo");
  String tContNo = request.getParameter("ContNo");
  String tBranchType2 = request.getParameter("BranchType2");
  System.out.println("能打印出来么"+tWageNo);
  LAContFYCRateSchema tLAContFYCRateSchema   = new LAContFYCRateSchema();
  LAContFYCRateSet tLAContFYCRateSet = new LAContFYCRateSet();
  LAZTContFYCRateUI tLAZTContFYCRateUI   = new LAZTContFYCRateUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tOperate ="INSERT||MAIN";
  String mTest ="";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  //取得Muline信息
  int lineCount = 0;
  String tChk[] = request.getParameterValues("InpArchieveGridChk");
  String tRiskCode[] = request.getParameterValues("ArchieveGrid1");
  String tCommisionSN[] = request.getParameterValues("ArchieveGrid9");
  String tRate[] = request.getParameterValues("ArchieveGrid6"); 
  String tMoney[] = request.getParameterValues("ArchieveGrid8"); 
  for(int i=0;i<tChk.length;i++)
  {
	if(tChk[i].equals("1"))
    {
     tLAContFYCRateSchema = new LAContFYCRateSchema();
     tLAContFYCRateSchema.setRiskCode(tRiskCode[i].trim());  
     tLAContFYCRateSchema.setGrpContNo(tCommisionSN[i].trim()); // 这里开始调整.为了编码主键冲突 grpcontno 存储的是主键
     tLAContFYCRateSchema.setRate(tRate[i].trim()); 
     tLAContFYCRateSchema.setFlag("Y");
     tLAContFYCRateSchema.setMoney(tMoney[i]);
     tLAContFYCRateSet.add(tLAContFYCRateSchema);    
     System.out.println("传入数据量大小为"+tLAContFYCRateSet.size());
   }
  }

 
  // 准备传输数据 VData
  VData tVData = new VData();
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("WageNo",tWageNo);
  tTransferData.setNameAndValue("ContNo",tContNo);
  tTransferData.setNameAndValue("LAContFYCRateSet",tLAContFYCRateSet);
  tTransferData.setNameAndValue("BranchType2",tBranchType2);
  tVData.add(tG);
  tVData.add(tTransferData);
  
  try
  {
	  tLAZTContFYCRateUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLAZTContFYCRateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
