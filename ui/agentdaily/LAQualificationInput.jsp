<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-03-21 15:48:13
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String agentcode=request.getParameter("AgentCode");
    if (agentcode==null || agentcode.equals(""))
    {
       agentcode="";
    }
  String BranchType=request.getParameter("BranchType");
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAQualificationInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAQualificationInputInit.jsp"%>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAQualificationSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton2.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 资格证基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLAQualification1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      <%=tTitleAgent%>代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GroupAgentCode verify="营销员代码|NotNull " elementtype=nacessary onchange="return checkvalid();">
    </TD>
    <TD  class= title>
      <%=tTitleAgent%>名称
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=AgentName readonly>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      资格证书号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=QualifNo verify="资格证书号|NotNull " elementtype=nacessary  onblur='checkInput(this.value);'>
    </TD>
    <TD  class= title>
      批准单位
    </TD>
    <TD  class= input>
       <Input class= 'common' name=GrantUnit verify="批准单位|NotNull " elementtype=nacessary >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      发放日期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=GrantDate verify="发放日期|NOTNULL&Date"  elementtype=nacessary> 
    </TD>
    <TD  class= title>
      有效起期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=ValidStart verify="有效起期|NOTNULL&Date"  elementtype=nacessary> 
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      有效止期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=ValidEnd verify="有效止期|NOTNULL&Date"  elementtype=nacessary> 
    </TD>
    <TD  class= title>
      资格证书状态
    </TD>
    <TD class=input><input name=QualifState class="codeno" name="QualifState" verify="资格证书状态|NOTNULL " 
         CodeData="0|^0|有效|^1|失效"
         ondblClick="showCodeListEx('QualifStateList',[this,QualifStateName],[0,1]);"
         onkeyup="showCodeListKeyEx('QualifStateList',[this,QualifStateName],[0,1]);"
         ><Input class=codename name=QualifStateName readOnly elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common> 
    <TD  class= title>
      失效日期
    </TD>
    <TD  class= input>
        <Input class= "coolDatePicker" dateFormat="short" name=InvalidDate  verify="失效日期|Date"> 
    </TD>
  	<TD  class= title>
      失效原因
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InvalidRsn >
    </TD>
  </TR>
  <TR  class= common> 
    <TD  class= title>
      通过考试日期
    </TD>
    <TD  class= input>
        <Input class= "coolDatePicker" dateFormat="short" name=PasExamDate  verify="通过考试日期|Date"> 
    </TD>  
   	<TD  class= title>
      考试年度
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ExamYear >
    </TD>
  </TR>
  <TR  class= common> 
    <TD  class= title>
      考试次数
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ExamTimes >
    </TD>
    <TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=Operator>
    </TD>
  </TR>
  <TR  class= common> 
    <TD  class= title>
      最近操作日
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=ModifyDate>
    </TD>
  </TR>
  
 <TR class=common>
	<TD class=title>
		
	</TD>
	<TD class=input>
		<Input class="readonly" type=hidden name=diskimporttype>
</TR>

</table>

<br><hr>
<table class="common" align=center>
<tr align=right>
				<td class=button >
				&nbsp;&nbsp;
			</td>
	<td class=button width="10%">		
  <Input type=button class=cssButton name="goDiskImport" value="磁盘导入" id="goDiskImport" class=cssButton onclick="diskImport()">
</td>
</tr>
</table>

    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden name=QualifState1 value=''>
    <input type=hidden name=QualifNoQuery value=''>
    <input type=hidden name=AgentCodeQuery value=''>
    <input type=hidden name=AgentCode value=''>
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
