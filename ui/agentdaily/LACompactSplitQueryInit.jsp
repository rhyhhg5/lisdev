  <%
//程序名称：LACompactSplitInit.jsp
//程序功能：
//创建日期：2005-10-19 14:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{
  try
  {
    document.fm.GrpContNo.value = "";
    document.fm.GrpPolNo.value = "";
		document.fm.ManageCom.value = "";
		document.fm.BranchAttr.value = "";
		document.fm.AppntNo.value = "";
		document.fm.AppntName.value = "";
		document.fm.CurPayToDateF.value = "";
		document.fm.SignDateF.value = "";
		document.fm.CurPayToDateT.value = "";
		document.fm.SignDateT.value = "";
    fm.all('BranchType').value = '<%=BranchType%>'; 
    fm.all('BranchType2').value = '<%=BranchType2%>'; 
  }
  catch(ex)
  {
    alert("在LACompactSplitInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initPrincipalGrid();
  }
  catch(re)
  {
    alert("在LACompactSplitInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//保单负责人列表初始化
function initPrincipalGrid()
{
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="团体合同号";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="团体保单号";         		//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="管理机构";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="销售机构";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="投保客户号";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="投保客户名";         		//列名
      iArray[6][1]="120px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="缴费日期";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="签单日期";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      ContGrid = new MulLineEnter( "fm" , "ContGrid" );
      //这些属性必须在loadMulLine前
      ContGrid.displayTitle = 1;
      ContGrid.mulLineCount = 0;
      ContGrid.hiddenPlus=1;
      ContGrid.canSel=1;
      ContGrid.hiddenSubtraction=1;
      ContGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //ContGrid.setRowColData(1,1,"asdf");
    }
    catch(ex)
    {
      alert(ex);
    }
}
</script>