 <%
//程序名称：LACompactSplitInit.jsp
//程序功能：
//创建日期：2005-10-19 14:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{
  try
  {
    document.fm.GrpContNo.value = "";
    document.fm.GrpPolNo.value = "";
		document.fm.ManageCom.value = "";
		document.fm.BranchAttr.value = "";
		document.fm.AppntNo.value = "";
		document.fm.AppntName.value = "";
		document.fm.TransMoney.value = "";
		document.fm.RiskNum.value = "";
		document.fm.CurPayToDate.value = "";
		document.fm.SignDate.value = "";
		document.fm.Operator.value = "";
    fm.all('BranchType').value = '<%=BranchType%>'; 
    fm.all('BranchType2').value = '<%=BranchType2%>'; 
  }
  catch(ex)
  {
    alert("在LACompactSplitInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initPrincipalGrid();
  }
  catch(re)
  {
    alert("在LACompactSplitInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//保单负责人列表初始化
function initPrincipalGrid()
{
    var iArray = new Array();
    var i11Array = getAgentGradeStr();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="业务员代码";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][10]='AgentCode';
      iArray[1][11]=i11Array;
      iArray[1][12]="1|2|3|4";
      iArray[1][13]="0|1|2|3";
      iArray[1][19]= 1; //1是需要强制刷新
      
      iArray[2]=new Array();
      iArray[2][0]="业务员姓名";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="职级代码";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="职级名称";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="保费比例(%)";         		//列名
      iArray[5][1]="40px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="是否分配客户数";         		//列名
      iArray[6][1]="40px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][4]="YesNo";
      iArray[6][5]="6";
      iArray[6][6]="0";

      PrincipalGrid = new MulLineEnter( "fm" , "PrincipalGrid" );
      //这些属性必须在loadMulLine前
      PrincipalGrid.displayTitle = 1;
      PrincipalGrid.mulLineCount = 1;
      PrincipalGrid.hiddenPlus=0;
      PrincipalGrid.hiddenSubtraction=0;
      PrincipalGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //PrincipalGrid.setRowColData(1,1,"asdf");
    }
    catch(ex)
    {
      alert(ex);
    }
}
</script>