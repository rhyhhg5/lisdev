<%
//程序名称：LAAccountsInput.jsp
//程序功能：功能描述
//创建日期：2005-03-20 18:03:36
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
 
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="LAAccountsQueryInput.js"></SCRIPT> 
  <%@include file="LAAccountsQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  
  
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();initElementtype();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLAAccountsGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      营销员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GroupAgentCode onchange="return getAgentName();">
    </TD>
    <TD class= title>
          管理机构
        </TD>
        <TD  class= input>
            <Input class="codeno" name=ManageCom  verify="管理机构|NotNull" 
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
             ><Input name=ManageComName class="codename" elementtype=nacessary> 
        </TD> 
  </TR>
  <TR  class= common>
    <TD  class= title>
      帐号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Account >
    </TD>
    <TD  class= title>
      帐号名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccountName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      状态
    </TD>
    <TD  class= input>
      <Input class=codeno name=State CodeData="0|^0|有效|^1|无效"
         ondblClick="showCodeListEx('StateList',[this,StateName],[0,1]);"
         onkeyup="showCodeListKeyEx('StateList',[this,StateName],[0,1]);"
         ><Input name=StateName class="codename">
    </TD>
    <TD  class= title>
      开户行名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Bank >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      开户时间
    </TD>
    <TD  class= input>
      <Input name=OpenDate class="coolDatePicker" dateFormat="short" >
    </TD>
    <TD  class= title>
      销户日期
    </TD>
    <TD  class= input>
      <Input name=DestoryDate class="coolDatePicker" dateFormat="short">
    </TD>
  </TR>
  <TR  class= common>
      <TD  class= title>
     销售团队
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AgentGroup >
    </TD>
      <TD  class= title>开户行行号
    </TD>
    <TD  class= input>
      <Input name =BankCode class='common' >
    </TD>
  </TR>
  
 
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查 询" TYPE=button class=cssButton onclick="easyQueryClick();">
        <INPUT VALUE="下 载" TYPE=button class=cssbutton onclick="ListExecl();">
        <INPUT VALUE="返 回" TYPE=button class=cssButton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAccounts1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLAAccounts1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLAAccountsGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
 <Div id= "divPage2" align=center style= "display: '' ">
  
  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
     <INPUT VALUE="" TYPE=hidden name=serialNo> 
     <input type=hidden id="fmtransact" name="fmtransact">
     <input type=hidden class=Common name=querySql > 
     <input type=hidden class=Common name=querySqlTitle >
     <input type=hidden class=Common name=Title >
     <input type=hidden  class=Common name=AgentCode>
     <input type=hidden  class=Common name=BranchType value='1'>
     <input type=hidden  class=Common name=BranchType2 value='01'>
     
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
