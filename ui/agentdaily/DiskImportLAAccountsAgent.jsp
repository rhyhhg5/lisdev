<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DiskImportZT.jsp
//程序功能：磁盘导入
//创建日期：2005-10-08
//创建人 ：Yang Yalin
//更新记录： 更新人  更新日期   更新原因/内容
%>
<html>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="DiskImportLAAccountsAgent.js"></SCRIPT>
<%@include file="./DiskImportLAAccountsAgentInit.jsp"%>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>磁盘导入</title>
</head>
<body onload="initForm();">
	<form name="fm" action="DiskImportLAAccountsAgentSave.jsp" method=post target="fraSubmit" ENCTYPE="multipart/form-data">
		<input type="hidden" name="diskimporttype">
		<!--input type="hidden" name="grpContNo--">
		<!--input type="hidden" name="edorType"-->
		<br><br>
		<table class=common>
			<TR>
				<TD width='15%' style="font:9pt">文件名</TD>
				<TD width='35%'>
					<Input type="file" width="100%" name="FileName">
					<INPUT VALUE="导  入" class="cssButton" TYPE=button onclick="importFile();">
				</TD>
				<td width='30%'>
				<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()">
				</td>
			</TR>
		</table>
		<table class=common>
			<TR align="center">
				<TD id="Info" width='100%' style="font:10pt"></TD>
			</TR>
		</table>
		
	</form>
</body>
</html>