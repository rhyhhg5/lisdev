<%
//程序名称：LAAscriptionInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     String gToday = PubFun.getCurrentDate(); //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                  
  fm.all('ViolatNo').value ='';
  fm.all('ViolatDate').value ='';
  fm.all('ViolatEndDate').value ='';
  
   fm.all('ContNo').value ='';
   
  fm.all('PrtNO').value ='';
   
  fm.all('AgentCode').value ='';
  fm.all('AgentName').value ='';
  fm.all('AppntName').value ='';
  fm.all('InsuredName1').value ='';
  fm.all('InsuredName2').value ='';
  fm.all('InsuredName3').value ='';
  fm.all('State').value ='0';   //案件处理状态 默认为为处理0,当录入结案日期时为1 
  fm.all('ManageCom').value ='';
  fm.all('AgentGroup').value ='';
  fm.all('ModifyDate').value ='';
  fm.all('AppntNo').value ='';
  fm.all('InsuredNo1').value ='';
  fm.all('InsuredNo2').value ='';
  fm.all('InsuredNo3').value ='';
   
  fm.all('DirectLoss').value ='';
  fm.all('ReplevyLoss').value ='';
  fm.all('DealMind').value ='';
  fm.all('Operator').value ='';
  fm.all('BranchType').value = '<%=BranchType%>';
  fm.all('BranchType2').value = '<%=BranchType2%>';     
     

  
  }
  catch(ex)
  {
    alert("在LAAscriptionInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误1111!");
  }      
}

var LGAppealGrid;
function initLGAppealGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="投诉对象编号";          		        //列名
    iArray[1][1]="70px";      	      		//列宽
    iArray[1][2]=20;            			//列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
   
    
    iArray[2]=new Array();
    iArray[2][0]="投诉对象名";          		        //列名
    iArray[2][1]="70px";      	      		//列宽
    iArray[2][2]=20;            			//列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    
    iArray[3]=new Array();
    iArray[3][0]="投诉对象电话";          		        //列名
    iArray[3][1]="80px";      	      		//列宽
    iArray[3][2]=20;            			//列最大值
    iArray[3][3]=0;             //是否允许输入,    
    
   
    iArray[4]=new Array();
    iArray[4][0]="投诉对象所属部门";          		        //列名
    iArray[4][1]="100px";      	      		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
    
    iArray[5]=new Array();
    iArray[5][0]="投诉方式";  
    iArray[5][1]="80px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[5][2]=100;   
    iArray[5][3]=0;  
    
    iArray[6]=new Array();
    iArray[6][0]="投诉内容";  
    iArray[6][1]="300px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[6][2]=100;   
    iArray[6][3]=0;  
     
     iArray[7]=new Array();
    iArray[7][0]="投诉人姓名";  
    iArray[7][1]="100px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[7][2]=100;   
    iArray[7][3]=0; 
     
      iArray[8]=new Array();
    iArray[8][0]="投诉人电话";  
    iArray[8][1]="70px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[8][2]=100;   
    iArray[8][3]=0; 
    
     iArray[9]=new Array();
    iArray[9][0]="投诉人地址";  
    iArray[9][1]="70px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[9][2]=100;   
    iArray[9][3]=0;  
    
    iArray[10]=new Array();
    iArray[10][0]="投诉处理状态";  
    iArray[10][1]="70px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[10][2]=100;   
    iArray[10][3]=0; 

iArray[11]=new Array();
    iArray[11][0]="投诉单编号";  
    iArray[11][1]="0px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[11][2]=100;   
    iArray[11][3]=0; 

iArray[12]=new Array();
    iArray[12][0]="投诉处理编号";  
    iArray[12][1]="0px";  // ?你可以将列宽设为0，该列就隐藏了
    iArray[12][2]=100;   
    iArray[12][3]=0; 


    LGAppealGrid = new MulLineEnter( "fm" , "LGAppealGrid" ); 
    //这些属性必须在loadMulLine前
    LGAppealGrid.mulLineCount = 0;   
    LGAppealGrid.displayTitle = 1;
    LGAppealGrid.hiddenPlus = 1;
    LGAppealGrid.hiddenSubtraction = 1;
    LGAppealGrid.canSel = 1;
   LGAppealGrid.canChk = 0;
 //  ArchieveGrid.selBoxEventFuncName = "showOne";

    LGAppealGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ArchieveGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("ALACaseInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}   
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
 //   initLGAppealGrid();
  }
  catch(re)
  {
    alert("在LAHolsInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
</script>          