
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：RiskQuarterAssessRateSave.jsp
//程序功能：
//创建日期：2009-02-19
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentdaily.*"%>
<%
  // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tGI = new GlobalInput(); 
  tGI=(GlobalInput)session.getValue("GI"); 
    
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  {
  	transact = request.getParameter("fmtransact");
//    String tManageCom = request.getParameter("ManageCom");
    String tBranchType= request.getParameter("BranchType");
    String tBranchType2= request.getParameter("BranchType2");
    String tChk[] = request.getParameterValues("InpRiskQuarterAssessRateGridChk");
    String tIDX[] = request.getParameterValues("RiskQuarterAssessRateGrid1");          // 主键
//    String tAgentGrade[] = request.getParameterValues("RiskQuarterAssessRateGrid2");   // 职级
    String tRiskCode[] = request.getParameterValues("RiskQuarterAssessRateGrid6");     // 险种号
    String tInsureYear[] = request.getParameterValues("RiskQuarterAssessRateGrid8");   // 保费类型
    String tManageCom[] = request.getParameterValues("RiskQuarterAssessRateGrid4");     //管理机构
    
    String tPayYearsStart[] = request.getParameterValues("RiskQuarterAssessRateGrid12"); //最低缴费年期 
    String tPayYearsEnd[] = request.getParameterValues("RiskQuarterAssessRateGrid13");   //缴费年期小于
    // add new
    String tPayintv[] = request.getParameterValues("RiskQuarterAssessRateGrid9");// 缴费方式
    String tYears[] = request.getParameterValues("RiskQuarterAssessRateGrid10"); //保险期间
    String tCuryear[] = request.getParameterValues("RiskQuarterAssessRateGrid15"); //保单年度
    String tRate[] = request.getParameterValues("RiskQuarterAssessRateGrid14");         //间接绩效提奖比例（小数）
    LADiscountSet tLADiscountSet=new LADiscountSet();
    for(int i=0;i<tChk.length;i++)
    {
    	if(tChk[i].equals("1"))
    	{
    		LADiscountSchema tLADiscountSchema=new LADiscountSchema();
    		if(StrTool.cTrim(tIDX[i]).equals(""))
    		{
    			tLADiscountSchema.setIdx(-1);
    		}
    		else
    		{
    			tLADiscountSchema.setIdx(Integer.parseInt(tIDX[i]));
    		}
//    		tLADiscountSchema.setAgentGrade(tAgentGrade[i]);
    		tLADiscountSchema.setManageCom(tManageCom[i]);
    		tLADiscountSchema.setBranchType(tBranchType);
    		tLADiscountSchema.setBranchType2(tBranchType2);
    		tLADiscountSchema.setRiskCode(tRiskCode[i]);
    		tLADiscountSchema.setInsureYear(tInsureYear[i]);//保费类型
    		tLADiscountSchema.setCode1(tPayYearsStart[i]);//最低缴费年期
    		tLADiscountSchema.setCode2(tPayYearsEnd[i]);//缴费年期小于
    		// add new 
    		tLADiscountSchema.setPayIntv(tPayintv[i]); // payintv  缴费频次
    		tLADiscountSchema.setCode4(tYears[i]);    // 保险期间
    		tLADiscountSchema.setCode5(tCuryear[i]);  // 保单年度 默认为1 只提首期
//    		System.out.println("职级"+tAgentGrade[i]+" 险种"+tRiskCode[i]+" 标准"+tRate[i]);
    		tLADiscountSchema.setRate(tRate[i]);
    		tLADiscountSchema.setDiscountType("22");//员工制银代直销标识
    		tLADiscountSet.add(tLADiscountSchema);
    	}
    	
    }
    if(tLADiscountSet.size()==0)
    {
    	FlagStr = "Fail";
    	Content="没有得到要处理的数据！";
    }
    else
    {
   		System.out.println("init finish .............................");
    	try
  		{
	  		// 准备传输数据 VData
	    	VData tVData = new VData();
	    	tVData.addElement(tLADiscountSet);
	    	tVData.addElement(tGI);
	    	// 数据传输
	    	LARiskQuarterAssessRateBL tRiskQuarterAssessRateBL = new LARiskQuarterAssessRateBL();
	    	tRiskQuarterAssessRateBL.submitData(tVData, transact);
	    	tError = tRiskQuarterAssessRateBL.mErrors;
  		}
	  	catch(Exception ex)
	  	{
	  		System.out.println(ex.toString());
	    	Content = "操作失败，原因是:" + ex.toString();
	    	FlagStr = "Fail";
	  	}
	  	if (FlagStr=="")
  		{   
	    	if (!tError.needDealError())
	    	{                          
	    		Content = " 操作成功! ";
	    		FlagStr = "Success";
	   	 	}
	    	else                                                                           
	    	{
	    		Content = " 操作失败，原因是:" + tError.getFirstError();
	    		FlagStr = "Fail";
	    	}
  		}
    }
  }
%>
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
