var turnPage = new turnPageClass();
/***************************************************
 * 查询允许分配的业务员（当前保单本部门内的人员）
 ***************************************************/
function getAgentGradeStr()
{
  var tReturn = "";
  var tBranchAttr = document.fm.BranchAttr.value;
  var tManageCom  = document.fm.ManageCom.value;
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  var sSql = "";

  sSql  = "SELECT";
  sSql += "   a.agentcode,";
  sSql += "   c.name,";
  sSql += "   a.agentgrade,";
  sSql += "   d.gradename";
  sSql += " FROM";
  sSql += "   LATree a,";
  sSql += "   LABranchGroup b,";
  sSql += "   LAAgent c,";
  sSql += "   LAAgentGrade d";
  sSql += " WHERE";
  sSql += "   b.branchtype='"+tBranchType+"' AND";
  sSql += "   b.branchtype2='"+tBranchType2+"' AND";
  sSql += "   a.agentcode=c.agentcode   AND";
  sSql += "   a.agentgroup = b.agentgroup AND";
  sSql += "   b.ManageCom like '"+tManageCom+"%'  AND";         
  sSql += "   a.agentgrade=d.gradecode";

  var tqueryRe = easyQueryVer3(sSql,1,0,1);
  if (tqueryRe)
    tReturn = tqueryRe;
  else
  	tReturn = null;

  return tReturn;
}

/***************************************
 * 输入团体合同号触发的事件
 *     参数：无
 *     返回：boolean
 ***************************************/
function queryCompact()
{
	var tGrpCont = trim(document.fm.GrpContNo.value);
	var arrGrpCont;
	var tAgentCode = "";
	var tManagecom="";
	var  state="";
	if (tGrpCont == "")
	  return false;
	
	arrGrpCont = queryGrpCont(tGrpCont);
	
	if (arrGrpCont == null)
	{
		alert("团体保单号码不存在！");
		//初始化画面
		initForm();
		document.fm.GrpContNo.value = tGrpCont;
	  return false;
	}
	
	
	
	//设置画面各文本框的值
	document.fm.GrpPolNo.value = arrGrpCont[0][1];
	document.fm.ManageCom.value = arrGrpCont[0][2];
	document.fm.BranchAttr.value = arrGrpCont[0][3];
	document.fm.AppntNo.value = arrGrpCont[0][4];
	document.fm.AppntName.value = arrGrpCont[0][5];
	document.fm.TransMoney.value = arrGrpCont[0][6];
	document.fm.RiskNum.value = arrGrpCont[0][7];
	document.fm.CurPayToDate.value = arrGrpCont[0][8];
	document.fm.SignDate.value = arrGrpCont[0][9];
	document.fm.Operator.value = arrGrpCont[0][10];
	tAgentCode = arrGrpCont[0][11];
	tManagecom = arrGrpCont[0][2];
	
//	state = queryWageState(tGrpCont,tManagecom);
//
//	if (state != null)
//	{
//		alert("业务员已经算过薪资，不能进行保单分配！");
//		//初始化画面
//		initForm();
//		//document.fm.GrpContNo.value = tGrpCont;
//	  return false;
//	}
	//初始化列表
	initPrincipalGrid();
	//设置负责人信息
	queryAgent(tGrpCont,tAgentCode);
	
	return true;
}

/***************************************
 * 通过团体合同号进行查询负责业务员信息
 *     参数：pmGrpContNo  团体合同号
 *           pmAgentCode  保单原始负责人编码
 *     返回：查询结果
 ***************************************/
function queryAgent(pmGrpContNo,pmAgentCode)
{
	var tSQL = "";
	
	// 查询当前保单是否被分配了多个业务员
	tSQL = "SELECT * FROM LAGrpCommisionDetail WHERE GrpContNo='"+pmGrpContNo+"'";
	// 取得查询结果
	var arrRS = getArrValueBySQL(tSQL);
	// 判断是否有结果
	//if(arrRS==null)
	//{
	//	return false;
	//}

	// 如果查询数据的条数大于'0'说明保单被分配过，存在多个业务员
	if(null != arrRS)
	{// 存在多个业务员
	  tSQL  = "select a.AgentCode,";
	  tSQL += "       b.Name,";
	  tSQL += "       c.AgentGrade,";
	  tSQL += "       d.GradeNAme,";
	  tSQL += "       a.BusiRate,";
	  tSQL += "       a.AppntRate";
	  tSQL += "  from LAGrpCommisionDetail a,LAAgent b,LATree c,LAAgentGrade d";
	  tSQL += " where a.AgentCode = b.AgentCode";
	  tSQL += "   and a.AgentCode = c.AgentCode";
	  tSQL += "   and c.AgentGrade = d.GradeCode";
	  tSQL += "   and a.GrpContNo = '"+pmGrpContNo+"'";
	  
	  // 显示查询结果
	  queryAgentGride(tSQL);
	}
	else
	{// 保单未被分配过
	  tSQL  = "select a.AgentCode,";
	  tSQL += "       a.Name,";
	  tSQL += "       b.AgentGrade,";
	  tSQL += "       c.GradeName";
	  tSQL += "  from LAAgent a,LATree b,LAAgentGrade c";
	  tSQL += " where a.AgentCode = b.AgentCode";
	  tSQL += "   and b.AgentGrade = c.GradeCode";
	  tSQL += "   and a.BranchType='"+document.fm.BranchType.value+"'";
	  tSQL += "   and a.BranchType2='"+document.fm.BranchType2.value+"'";
	  tSQL += "   and a.AgentCode = '"+pmAgentCode+"'";

		// 显示查询结果
	  queryAgentGride(tSQL);
  }
}


/***************************************
 * 通过团体合同号进行查询负责业务员的薪资计算状态
 *     参数：pmGrpContNo  团体合同号
 *           pmAgentCode  保单原始负责人编码
 *     返回：查询结果
 ***************************************/
function queryWageState(pmGrpContNo,pmManagecom)
{
	var tSQL = "";
	
	// 查询当前保单的提数信息
	tSQL = "SELECT min(wageno) FROM LACommision WHERE GrpContNo='"+pmGrpContNo+"'";
	// 取得查询结果
	var arrRS = getArrValueBySQL(tSQL);
	// 判断是否有结果
	//if(arrRS==null)
	//{
	//	return false;
	//}

	// 如果查询结果存在表示已经提过数据，不存在则表示还没有提数(没算过薪资)
	if(null != arrRS)
	{
	  tSQL  = "select  distinct indexcalno";
	  tSQL += "   from lawage  where  managecom='"+pmManagecom+"'";
	  tSQL += "   and  indexcalno='"+arrRS+"' and branchtype='2' and branchtype2='01' ";
	  return getArrValueBySQL(tSQL);
	}
	else
	{
		return null;
  }
}

/***************************************
 * 通过团体合同号进行查询其详细信息
 *     参数：pmGrpContNo  团体合同号
 *     返回：查询结果
 ***************************************/
function queryGrpCont(pmGrpContNo)
{
	var tSQL = "";
	
  tSQL  = "SELECT";
  tSQL += "   grpcontno,";
  tSQL += "   '',";
  tSQL += "   managecom,";
  tSQL += "   (select BranchAttr from labranchgroup where  labranchgroup.agentgroup=lcgrpcont.agentgroup),";
  tSQL += "   AppntNo,";
  tSQL += "   Grpname,";
  tSQL += "   prem,";
  tSQL += "   (select COUNT(RiskCode) from lcgrppol where lcgrppol.grpcontno=lcgrpcont.grpcontno) ,";
  tSQL += "   (select max(enteraccdate) from ljtempfee where ljtempfee.otherno=lcgrpcont.grpcontno),";
  tSQL += "   signdate,";
  tSQL += "   Operator,";
  tSQL += "   AgentCode";
  tSQL += " FROM";
  tSQL += "   lcgrpcont";
  tSQL += " WHERE";
  tSQL += "   grpcontno<>'00000000000000000000' AND";
  tSQL += "   grpcontno='"+pmGrpContNo+"'";
	
	return getArrValueBySQL(tSQL);
}

/***************************************
 * 根据参数查询结果
 *     参数：pmSQL  查询用的SQL文
 *     返回：查询结果
 ***************************************/
function getArrValueBySQL(pmSQL)
{
	//alert(pmSQL);
	var strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
	
	if (!strQueryResult)
		return null;
		
	var arr = decodeEasyQueryResult(strQueryResult);
	
	return arr;
}

/***************************************
 * 点击查询按钮的触发事件
 ***************************************/
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LACompactSplitQueryHtml.jsp?BranchType="+fm.all('BranchType').value+"&BranchType2="+fm.all('BranchType2').value);
}

/***************************************
 * 响应查询页面返回按钮的事件
 *     参数：pmGrpContNo
 *     返回：无
 ***************************************/
function afterQuery(pmGrpContNo)
{
	//表示出返回的团体保单号
	document.fm.GrpContNo.value = pmGrpContNo;
	
	//通过传回来的团体保单号查询相关信息
	queryCompact(pmGrpContNo);
}

/***************************************
 * 通过SQL文查询结果显示结果
 *     参数：pmSQL  查询语句
 *     返回：无
 ***************************************/
function queryAgentGride(pmSQL)
{
  //执行查询并返回结果
	turnPage.queryModal(pmSQL, PrincipalGrid);
}

/**************************************
 * 提交画面
 **************************************/
function submitForm()
{
  if(!verifyInput())
  {
	 	return false;
	}
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}

/**************************************
 * 提交后操作,服务器数据返回后执行的操作
 **************************************/
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //document.fm.listAgentCode.value="";
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    initForm();
  }
}