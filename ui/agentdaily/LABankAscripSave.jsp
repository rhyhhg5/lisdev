<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  String transact = "";
  String tOperate ="INSERT||MAIN";
  transact = request.getParameter("fmtransact");
	String tManageCom = request.getParameter("ManageCom");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	String tContNo[] = request.getParameterValues("AscriptionGrid1");
	String tAgentOld[] = request.getParameterValues("AscriptionGrid9");
	String tAgentComOld[] = request.getParameterValues("AscriptionGrid11");
	String tAgentNew[] = request.getParameterValues("AscriptionGrid13");
	String tAgentComNew[] = request.getParameterValues("AscriptionGrid15");
	String tconttype="";  //保单类型：1--个单  2--团单
	int tDateCount = 0;
  String tChk[] = request.getParameterValues("InpAscriptionGridChk");
  //输入参数
  LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();
  LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
  LABankAscripUI mLABankAscripUI = new LABankAscripUI();

  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	// 准备数据

  System.out.println("开始准备后台处理");

  // 准备保单归属信息
	tDateCount = tContNo.length;
	System.out.println("本次处理数:["+tDateCount+"]");
	for(int index=0;index<tChk.length;index++)
	{
     if(tChk[index].equals("1"))           
        {    
          System.out.println("该行被选中----start");
	      	mLAAscriptionSchema = new LAAscriptionSchema();
	      	tconttype=new ExeSQL().getOneValue("select conttype from lccont where grpcontno='00000000000000000000' and contno='"+tContNo[index]+"' union select conttype from lccont where grpcontno<>'00000000000000000000' and grpcontno='"+tContNo[index]+"'");
	      	System.out.println("保单类型:["+tconttype+"]");
	      	if(tconttype.equals("1"))
	      	{
	      	 mLAAscriptionSchema.setContNo(tContNo[index]);
	      	 System.out.println("保单:["+tContNo[index]+"]");
	      	}
	      	else if(tconttype.equals("2"))
	      	{
	      	 mLAAscriptionSchema.setGrpContNo(tContNo[index]);
	      	 System.out.println("保单:["+tContNo[index]+"]");
	      	}
	      	mLAAscriptionSchema.setAgentOld(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+tAgentOld[index]+"'"));
	      	mLAAscriptionSchema.setAgentComOld(tAgentComOld[index]);
	      	mLAAscriptionSchema.setAgentNew(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+tAgentNew[index]+"'"));
	      	mLAAscriptionSchema.setAgentComNew(tAgentComNew[index]);
	      	mLAAscriptionSchema.setBranchType(tBranchType);
	      	mLAAscriptionSchema.setBranchType2(tBranchType2);
	      	mLAAscriptionSet.add(mLAAscriptionSchema);
	      	System.out.println("该行被选中----end");
	      	}
        else{  
            System.out.println("该行未被选中");
	          }
			}


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLAAscriptionSet);
  tVData.add(tconttype);
  System.out.println("add over");
  try
  {
    mLABankAscripUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLABankAscripUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

