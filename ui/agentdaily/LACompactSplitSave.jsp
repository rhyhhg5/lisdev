<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABatchAuthorizeSave.jsp
//程序功能：
//创建日期：2005-11-1 16:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAGrpCommisionDetailSet tLAGrpCommisionDetailSet = new LAGrpCommisionDetailSet();
	LACompactSplitUI tLACompactSplitUI = new LACompactSplitUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	System.out.println("Save页面处理开始...");
  // 获取画面上取得的数据
  String tGrpContNo = request.getParameter("GrpContNo").trim();
  String tManageCom = request.getParameter("ManageCom").trim();
  System.out.println(tGrpContNo + " / " + tManageCom);
  
  // 分配人员信息
  String arrCount[] = request.getParameterValues("PrincipalGridNo");
  String tAgentCode[] = request.getParameterValues("PrincipalGrid1");
  String tBusiRate[] = request.getParameterValues("PrincipalGrid5");
  String tAppAllotMark[] = request.getParameterValues("PrincipalGrid6");
  // 循环处理分配人员信息
  for(int j=0;j<arrCount.length;j++)
  {
  	System.out.println(tBusiRate[j]+" / "+tAppAllotMark[j]);
    LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new LAGrpCommisionDetailSchema();
    tLAGrpCommisionDetailSchema.setAgentCode(tAgentCode[j]);
    tLAGrpCommisionDetailSchema.setMngCom(tManageCom);
    tLAGrpCommisionDetailSchema.setBusiRate(tBusiRate[j]);
    tLAGrpCommisionDetailSchema.setAppAllotMark(tAppAllotMark[j]);
    tLAGrpCommisionDetailSet.add(tLAGrpCommisionDetailSchema);
  }
	
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tGrpContNo);
	tVData.add(tLAGrpCommisionDetailSet);
	tVData.add(tG);
  System.out.println("add over");
  
  try
  {
    tLACompactSplitUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLACompactSplitUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

