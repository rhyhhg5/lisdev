//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var old_AgentGroup="";
var new_AgentGroup="";

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	
  if (!beforeSubmit()){
	  if(mOperate=="UPDATE"){
	    fm.all('DoneFlag').disabled=true;
	    fm.all('DoneFlag1').disabled=true; 
	  }
	  mOperate="";
    return false; 
  }
  if (mOperate=="")
	{
		if((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
		{
			
		addClick();
		}
		else
			{
		        fm.all('DoneFlag').disabled=true;
		        fm.all('DoneFlag1').disabled=true; 
				alert("此条数据只能进行“修改”或“删除”，不能再次“保存”");
				return false;
				}
    
	}
	   if(!checkADState()){
		   if(mOperate=="UPDATE"){
			    fm.all('DoneFlag').disabled=true;
			    fm.all('DoneFlag1').disabled=true; 
			  }
			  mOperate="";
		   return false;
	   }
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  
   
    fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  mOperate="";
  fm.all('PunishRsn').readOnly=false;
  fm.all('Money').readOnly=false;
  fm.all('DoneFlag').disabled=false;  
  fm.all('PunishRsn1').readOnly=false;
  fm.all('Money1').readOnly=false;
  fm.all('DoneFlag1').disabled=false;    
  showInfo.close();
  fm.reset();
  initForm();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("在LARewardPunish.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if(!verifyInput()) 
  return false;
 
  var tDoneFlag=document.fm.DoneFlag.value;   
  var tPunishRsn=document.fm.PunishRsn.value;
  var tMoney=document.fm.Money.value;  
  
  var tDoneFlag1=document.fm.DoneFlag1.value; 
  var tPunishRsn1=document.fm.PunishRsn1.value;
  var tMoney1=document.fm.Money1.value;
  
  
  
  
  var tFlag=0;
  var tFlag1=0;
  
  if ((tDoneFlag1!=null && tDoneFlag1!='') && (tMoney1!=null && tMoney1!='') )
  {	
  	tFlag1=2;	
  	fm.all("Flag1").value='2';
  	
  }
  else  if ((tDoneFlag1==null || tDoneFlag1=='') && (tMoney1==null || tMoney1=='') )
  {
  	tFlag1=0;
  }
  else
  {
	tFlag1=1;
   }	
		
  if (tFlag1==1)
  {
  	
    alert("扣款信息必须全部有录入(类型和金额)!");
  	return false ; 	
    }
  
  if ((tDoneFlag!=null && tDoneFlag!='') && (tMoney!=null && tMoney!='') )
  {	
  	tFlag=2;	
  	fm.all("Flag").value='2';
  }  
  else  if ((tDoneFlag==null || tDoneFlag=='') && (tMoney==null || tMoney=='') )
  {
  	tFlag=0; 
  }  
  else
  {
  	tFlag=1;
  }
  if (tFlag==1)
  {
  	
       alert("加款信息必须全部有录入(类型和金额)!");
  	return false ; 	
   }  
  
  if (tFlag==0  && tFlag1==0)
  {
  	alert("加款信息和扣款信息必须有一项录入!");
  	return false ; 
  }
  

  // 判断输入内容长度
  // 验证奖励称号
  // 奖惩原因
  if(!(strLen(document.fm.PunishRsn1.value,50,"[奖惩原因]请输入50个汉字或150个字母或数字！")))
    return false;
  if(!(strLen(document.fm.PunishRsn.value,50,"[奖惩原因]请输入50个汉字或150个字母或数字！")))
    return false;  
  return true	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
	 mOperate="INSERT";
  //下面增加相应的代码
	 
}           

function checkADState() {
	
	//加扣款同向校验
	  var addSQL = "";
	  var trainerCode= fm.all('TrainerCode').value;
	  var wageno = fm.all('WageNo').value;
	  var tDoneFlag=document.fm.DoneFlag.value; 
	  var tDoneFlag1=document.fm.DoneFlag1.value; 
	  if(tDoneFlag!=null&&tDoneFlag!=""&&tDoneFlag1!=null&&tDoneFlag1!=""){
	      if(tDoneFlag.substring(1,2)==tDoneFlag1.substring(1,2)){
	          alert("不能同时录入类型相同的加款和扣款");
	         return false; 
	      }
	  }
	  var idxString=" and 1=1 ";
		  if(fm.all('Idx').value!=null&&fm.all('Idx').value!=""){
			  idxString=idxString+" and a.idx <>'"+fm.all('Idx').value+"'";
		  }
	   
	  if(tDoneFlag!=null&&tDoneFlag!=""){
	      addSQL="select 1 from   LATrainerIndex a where a.wagecode='GX0003' and  a.trainerCode = '"+trainerCode
	           +"' and a.wageno='"+wageno+"' and substr(ADState,2,2)='"+tDoneFlag.substring(1,2)+"'"+idxString;
	      var strQueryResult = easyQueryVer3(addSQL, 1, 1, 1);
	      if (strQueryResult) {
	    	    alert('该组训人员薪资月'+wageno+'已录入此类型的加款或扣款请检查');
	    	    return false;
	    	  }
	  }
	  
	  if(tDoneFlag1!=null&&tDoneFlag1!=""){
	      addSQL="select * from   latrainerIndex a where a.wagecode='GX0003' and  a.trainerCode = '"+trainerCode
	           +"' and a.wageno='"+wageno+"' and substr(ADState,2,2)='"+tDoneFlag1.substring(1,2)+"'"+idxString;
	      
	      var strQueryResult = easyQueryVer3(addSQL, 1, 1, 1);
	      if (strQueryResult) {
	    	    alert('该组训人员薪资月'+wageno+'已录入此类型的加款或扣款请检查');
	    	    return false;
	    	  }
	  }
	  return true;
	  
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码  
  if ((fm.all("TrainerCode").value==null)||(fm.all("TrainerCode").value==''))
    alert('请重新输入代码！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要修改的纪录！');
  else if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE";
    fm.all('DoneFlag').disabled=false;
    fm.all('DoneFlag1').disabled=false;

    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LATrainerAddSubPerQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
  
}   
function queryClicksure()
{
  //下面增加相应的代码
  mOperate="QUERY";
  fm.submit();
  
}              

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{

  //下面增加相应的删除代码  
  if ((fm.all("TrainerCode").value==null)||(fm.all("TrainerCode").value==''))
    alert('请先查询出要删除的纪录！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要删除的纪录！');
  else if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function checkvalid()
{
  var strSQL = "";
  if (getWherePart('TrainerCode')!='')
  {
     strSQL = "select TrainerCode,trainerName,cmanagecom from latrainer where 1=1 "
       +" and cmanagecom like '"+fm.all('LogManagecom').value+"%'"
	     + getWherePart('TrainerCode','TrainerCode')
	     + getWherePart('BranchType')
	     + getWherePart('BranchType2');
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('TrainerCode').value = '';
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("此机构下无此代理人或此代理人已经离职！");
    fm.all('TrainerCode').value="";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return;
  }
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  fm.all('TrainerCode').value = tArr[0][0];
  fm.all('Name').value = tArr[0][1];
  fm.all('ManageCom').value  = tArr[0][2];

}

function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();

  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    

    fm.all('TrainerCode').value = arrResult[0][0];
    fm.all('TrainerCode').readOnly = true;
    fm.all('Name').value = arrResult[0][1];                                              
    fm.all('ManageCom').value = arrResult[0][2];
    fm.all('Operator').value = arrResult[0][3];
    fm.all('WageNo').value = arrResult[0][4];                                           
    fm.all('ModifyDate').value = arrResult[0][5];
    fm.all('Idx').value = arrResult[0][11];
    var aclass=arrResult[0][6];

    if (aclass=="0")
    {
       fm.all('PunishRsn').readOnly=false;
       fm.all('Money').readOnly=false;
       fm.all('DoneFlag').value = arrResult[0][7];
       fm.all('DoneFlagName').value = arrResult[0][8];
       fm.all('PunishRsn').value=arrResult[0][9]   
       fm.all('Money').value = arrResult[0][10];
       fm.all('PunishRsn1').value ='';
       fm.all('Money1').value ='';
       fm.all('DoneFlag1').value ='';
       fm.all('DoneFlag1Name').value ='';
       fm.all('PunishRsn1').readOnly=true;
       fm.all('Money1').readOnly=true;
       fm.all('DoneFlag').disabled=true;
       fm.all('DoneFlag1').disabled=true;

    }
    else if (aclass=="1")
    {
       fm.all('PunishRsn1').readOnly=false;
       fm.all('Money1').readOnly=false;
       fm.all('DoneFlag1').value = arrResult[0][7];
       fm.all('DoneFlag1Name').value = arrResult[0][8];
       fm.all('PunishRsn1').value=arrResult[0][9] ;
       fm.all('Money1').value = arrResult[0][10];  
       fm.all('PunishRsn').value='';
       fm.all('Money').value='';
       fm.all('DoneFlag').value='';
       fm.all('DoneFlagName').value='';
       fm.all('PunishRsn').readOnly=true;
       fm.all('Money').readOnly=true;
       fm.all('DoneFlag1').disabled=true;
       fm.all('DoneFlag').disabled=true;

    }
                                                                                                                                                                                                                                                 	
  }
     
}

/***************************************
 * 验证字符的字节长度是否超标
 * 参数：pmStr      准备验证的字符串
 *       pmStrLen   长度标准
 *       pmMissage  报错信息
 * 返回：返回字符串字节长度
 ***************************************/
function strLen(pmStr,pmStrLen,pmMissage)
{
	var i;
  var len;
  len = 0;
  for (i=0;i<pmStr.length;i++)
  {
    if (pmStr.charCodeAt(i)>255) len+=3; else len++;
  }
  if(len > pmStrLen)
  {
  	alert(pmMissage);
  	return false;
  }
  
  return true;
}
