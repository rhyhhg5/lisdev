<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAWageConfirmInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAWageConfirm.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAWageConfirmGrpInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
  <title>薪酬审核发放 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAWageConfirmSubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAWage1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAWage1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            年月代码
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo >
          </TD>
          <TD  class= title>
            业务人员代码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            销售单位
          </TD>
          <TD  class= input>
            <Input class= common name=AgentGroup >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='code' name=ManageCom verify = "管理机构|notnull&code:ComCode" ondblclick="return showCodeList('ComCode',[this]);" onkeyup="return showCodeListKey('ComCode',[this]);">
          </TD>
        </TR>
        <!--TR  class= common>
          <TD  class= title>
            发放状态
          </TD>
          <TD  class= input>
            <Input class= common name=State >
          </TD>
          <TD  class= title>
            审核人
          </TD>
          <TD  class= input>
            <Input class=common name=Operator2 >
          </TD>
        </TR-->
      </table>
      <input type=hidden name=BranchType value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 				
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
    <Div  id= "divAGroupGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanWageGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 		
      <p>
        <INPUT VALUE="薪酬审核" class=common TYPE=button onclick="submitForm();"> 
      </p>			
    </div>     
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
