<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  String transact = "";
  String tOperate ="INSERT||MAIN";
  transact = request.getParameter("fmtransact");
	String tManageCom = request.getParameter("ManageCom");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	String tNewAgentCode = request.getParameter("NewAgentCode");
	String querySQL = request.getParameter("querySQL");
  //输入参数
  LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
  LABankAscripAllUI mLABankAscripAllUI = new LABankAscripAllUI();

  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String OperatAscript = "ALLAscript";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	// 准备数据
  System.out.println("开始准备后台处理");
  // 准备保单归属信息
  mLAAscriptionSchema = new LAAscriptionSchema();
  mLAAscriptionSchema.setManageCom(tManageCom);
  mLAAscriptionSchema.setAgentNew(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+tNewAgentCode+"'"));
  mLAAscriptionSchema.setBranchType(tBranchType);
  mLAAscriptionSchema.setBranchType2(tBranchType2);
  
  System.out.println("封装数据");
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.add(OperatAscript);
  tVData.add(querySQL);
  tVData.add(mLAAscriptionSchema);
  System.out.println("数据封装完毕");
  try
  {
    mLABankAscripAllUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLABankAscripAllUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

