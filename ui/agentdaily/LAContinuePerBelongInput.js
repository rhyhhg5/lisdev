 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var mOperate="";
var turnPage = new turnPageClass();
var outWorkDate="";
var outWorkDate1="";
var ascriptiondate="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if(!beforeSubmit())
  {
   return false;
  }
 
  fm.all("hideOperate").value=mOperate;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //fm.reset();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{  
} 

//取消按钮对应操作
function cancelForm()
{
}
 
 	
function  checkname()
{
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var tPreName=fm.all('PreName').value;
  if(tPreName==null || tPreName==''){
  	alert('请输入原销售人员代码');
  	fm.all('AgentNew').value='';
  	return false;  	
  }
  var  sql="select 1 from laagent where 1=1 "
  		+ getWherePart('groupagentcode','AgentOld')
  		+" and managecom =(select managecom from laagent where 1=1 "
  		+ getWherePart('groupagentcode','AgentNew')
  		+")";
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("原代理人与新代理人不在同一个管理机构！操作失败。");
    fm.all('NameNew').value='';
    fm.all('AgentNew').value = '';
    return false;
  } 	
  /*var  sql = "select a.BranchManager,a.AgentGroup from LABranchGroup a where 1=1 "
	   + getWherePart('a.BranchAttr','BranchAttrNew') + " and EndFlag!='Y'"
	    + " and branchtype='"+tBranchType+"'"
	    + " and branchtype2='"+tBranchType2+"'";
  */
  sql = "select a.name from laagent a where 1=1 "
	   + getWherePart('a.groupagentcode','AgentNew') + " and Agentstate<'03'"
	   + " and branchtype='"+tBranchType+"'"
	   + " and branchtype2='"+tBranchType2+"'";
	    
  strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("没有此代理人！");
    fm.all('NameNew').value='';
    fm.all('AgentNew').value = '';
    return;
  }  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
　//<rem>######//　
  fm.all('NameNew').value  = tArr[0][0];
  //根据原业务员和新业务员两个维度进行判断
  //查询已经做归属但是未确认的记录，可以进行修改
  var tSQL =  "select count(*) from LAAscription where agentold=getAgentCode('"+fm.all('AgentOld').value+"') and agentnew=getAgentCode('"+fm.all('AgentNew').value+"') and ValidFlag='N' and AscripState='2' and maketype <> '01' ";
  var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
  if(strQueryResult)
  {
      var tArr = new Array();
      tArr = decodeEasyQueryResult(strQueryResult);
      var count=tArr[0][0];
     //  alert(count);
      var i = parseInt(count);
    //   	   alert(i);
      if(i>1)
      {
        fm.all('AgentNew1').value = fm.all('AgentNew').value;             	 
      }
      if(i==1)
      {
      	  //进行单张保单归属修改
         var tttSQL = "select a.ContNo,a.GrpContNo from LAAscription a where agentold=getAgentCode('"+fm.all('AgentOld').value+"') and agentnew=getAgentCode('"+fm.all('AgentNew').value+"') and ValidFlag='N' and AscripState='2' and maketype <> '01' ";	
             
         var strQueryResult = easyQueryVer3(tttSQL, 1, 1, 1);
         if(!strQueryResult)
         {
           alert("归属出错！");
           return false;	
         }
         else
         {
	      	var ttArr = new Array();
	        tttArr = decodeEasyQueryResult(strQueryResult);
	        fm.all('ContNo').value = tttArr[0][0];   
	        fm.all('ContNo1').value = tttArr[0][0];
	         fm.all('GrpContNo').value = tttArr[0][1];   
	        fm.all('GrpContNo1').value = tttArr[0][1];
	        fm.all('AgentNew1').value = fm.all('AgentNew').value;             
         }    
       }
    } 
}
	
function	checkCont()
{
  var sql="select contno,managecom,getUniteCode(agentcode),(select name from laagent where agentcode=lccont.agentcode) "
           +" ,(select agentstate from laagent where agentcode=lccont.agentcode) "
           +" from lccont where 1=1  and grpcontno='00000000000000000000'and salechnl ='01' "
	       + getWherePart('ContNo','ContNo')
           + getWherePart('groupAgentCode','AgentOld');  
//	       + getWherePart('AgentCode','AgentOld') ;
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("代理人没有此个单！");
    fm.all('ContNo').value='';
    return false;
   }
   else
   {
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('ManageCom').value=tArr[0][1];
    fm.all('AgentOld').value=tArr[0][2];
    fm.all('PreName').value=tArr[0][3];
    var state = tArr[0][4];
    fm.all('AgentCodeOld').value=tArr[0][5];
    if(state>="06"){
    	alert("此保单的代理人不为在职人员，无法使用此功能进行保单分配！")
    	fm.all('ContNo').value ='';
    }
    
   }
  //根据保单进行判断
  var tSQL = "select getUniteCode(agentnew) from LAAscription where agentold=getAgentCode('"+fm.all('AgentOld').value+"') and contno='"+fm.all('ContNo').value+"' and ValidFlag='N' and AscripState='2' and maketype <> '01' ";
  var strQueryResult1 = easyQueryVer3(tSQL, 1, 1, 1);
  if (strQueryResult1)
  {
    tArr = decodeEasyQueryResult(strQueryResult1);
    fm.all('AgentNew1').value=tArr[0][0];
  //  alert(fm.all('AgentNew1').value);
  }
  return true;
}


function	checkGrpCont()
{
  var sql="select grpcontno,managecom ,getUniteCode(agentcode),(select name from laagent where agentcode=lcgrpcont.agentcode) "
           +" ,(select agentstate from laagent where agentcode=lcgrpcont.agentcode) "
  +" from lcgrpcont where 1=1 and salechnl ='01' "
	       + getWherePart('GrpContNo','GrpContNo');
	       + getWherePart('groupAgentCode','AgentOld') ;
  
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("代理人没有此团单");
    fm.all('GrpContNo').value='';
    return false;
  }
  else
   {
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('ManageCom').value=tArr[0][1];
    fm.all('AgentOld').value=tArr[0][2];
    fm.all('PreName').value=tArr[0][3];
    var state = tArr[0][4];
    if(state >= "06")
    {
    	alert("此保单的代理人不为在职人员，无法使用此功能进行保单分配！")
    }
   }
  
  //根据保单进行判断
  var tSQL = "select  getUniteCode(agentnew) from LAAscription where agentold=getAgentCode('"+fm.all('AgentOld').value+"') and grpcontno='"+fm.all('GrpContNo').value+"' and ValidFlag='N' and AscripState='2' and maketype <> '01' ";
  var strQueryResult1 = easyQueryVer3(tSQL, 1, 1, 1);
 // alert(tSQL);
  if (strQueryResult1)
  {
    tArr = decodeEasyQueryResult(strQueryResult1);
    fm.all('AgentNew1').value=tArr[0][0];
  }
  
  return true;
}
 	
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if ((fm.all('AgentOld').value == '')||(fm.all('AgentOld').value == null))
  {
  	alert("请员输入代理人编码！");
  	fm.all('AgentOld').focus();
  	return false;
  }
  
  if ((fm.all('AgentNew').value == '')||(fm.all('AgentNew').value == null))
  {
  	alert("请输入新代理人编码！");
  	fm.all('AgentNew').focus();
  	return false;
  }
  if ((fm.all('ContNo').value == '')||(fm.all('ContNo').value == null) && (fm.all('GrpContNo').value == '')||(fm.all('GrpContNo').value == null))
  {
  	alert("请输入个单或团单号！");
  	fm.all('ContNo').focus();
  	return false;
  } 
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
//	alert(fm.all('AgentNew1').value);
//	alert(fm.all('AgentNew').value);
	//下面增加相应的代码
  if (fm.all('AgentNew1').value!=null&&fm.all('AgentNew1').value!="")
	{
    //为了实现确认前能执行对同一个多次分配保单，并且能通过前台查值进行修改，所以要进行数据库操作
     if(fm.all('ContNo').value!=null&&fm.all('ContNo').value!='')
     {
         var SQL = "select 'Y' from laascription where agentold=getAgentCode('"+fm.all('AgentOld').value+"') and agentnew=getAgentCode('"+fm.all('AgentNew').value+"') and contno='"+fm.all('ContNo').value+"'"
         + "  and ascripstate='2' and validflag='N' and maketype <> '01' ";
         var strQueryResult = easyQueryVer3(SQL, 1, 1, 1);
         if(strQueryResult)
         {
             alert('请执行修改操作！');           
             return false;  
         }        
     }
     else 
     {
	     if(fm.all('AgentNew1').value!=fm.all('AgentNew').value)
	     {        
	        alert("请执行修改操作！");
	        return false;
	     }
     }
        //为了实现确认前能执行对同一个多次分配保单，并且能通过前台查值进行修改，所以要进行数据库操作
     if(fm.all('GrpContNo').value!=null&&fm.all('GrpContNo').value!='')
     {
         var SQL = "select 'Y' from laascription where agentold=getAgentCode('"+fm.all('AgentOld').value+"') and agentnew=getAgentCode('"+fm.all('AgentNew').value+"') and grpcontno='"+fm.all('GrpContNo').value+"'"
         + "  and ascripstate='2' and validflag='N' and maketype <> '01'";
         var strQueryResult = easyQueryVer3(SQL, 1, 1, 1);
         
         if(strQueryResult)
         {
             alert('请执行修改操作！');           
             return false;  
         }        
      }
	}
  if(fm.all('AgentOld').value ==fm.all('AgentNew').value)
  {
  	  	alert("原代理人不能与新代理人相同!!");
  	  	return false;
  }
  //添加注释 modify by Elsa 20101122
  //if(!checkCostcenter()){
  // return false;
  //}
  if(!checkContNoGrp()){
   return false;
  }
    
 
 // if((fm.all('ContNo').value==''||fm.all('ContNo').value==null)&&ContGrid.mulLineCount>0){
  //	alert("原代理人含有未签单或未回执回销的保单，不能做保单分配!");
  //	return false;
  //}
  mOperate="INSERT||MAIN";
  submitForm();
}           


function checkCostcenter()
{
  var strSQL = "select AgentGroup,managecom,costcenter  from LABranchGroup "
             +"where agentgroup=(select agentgroup from laagent where agentcode=getAgentCode('"+fm.all('AgentOld').value +"')) "   
          ;
  var arr = easyExecSql(strSQL);
  var tManageCom=arr[0][1] ;
  var tCostCenter=arr[0][2] ;
  strSQL = "select AgentGroup,managecom ,costcenter from LABranchGroup "
	     +"where agentgroup=(select agentgroup from laagent where agentcode=getAgentCode('"+fm.all('AgentNew').value +"'))  and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null) "   
          ;
  arr = easyExecSql(strSQL);
  var tupManageCom=arr[0][1] ;
  var tupCostCenter=arr[0][2] ;
  if(tupManageCom != tManageCom )
  {
	alert("不能在不同的管理机构间进行分配！");	
    return false;
  }
  //if(tupCostCenter != tCostCenter )
  //{
//	alert("不能在不同的成本中心调动进行分配！");	
  //  return false;
 // }
  return true;
}


function checkContNoGrp()
{
  if(fm.all('ContNo').value!=null&&fm.all('ContNo').value!=""
   &&fm.all('GrpContNo').value!=null&&fm.all('GrpContNo').value!="")
  {
    alert("个单号和团单号只能存在一个，不能同时存在！");
    return false;
  }
  return true;
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
 // alert(fm.all('AgentNew').value);
 // alert(fm.all('AgentNew1').value);
 if(fm.all('AgentNew1').value==null||fm.all('AgentNew1').value=="")
 {
   alert("请执行保存操作！");
   return false;
 }
else if(fm.all('AgentNew').value==fm.all('AgentNew1').value)
 {
   //添加注释 modify by Elsa 20101122
   //if(!checkCostcenter()){
   //   return false;
   //}
   if(!checkContNoGrp()){
     return false;
   }
   if((fm.all('ContNo').value!=null&&fm.all('ContNo').value!=""))
   {
	  var SQL = "select 'Y' from LAAscription where agentold=getAgentCode('"+fm.all('AgentOld').value+"') "
	  +"and agentnew=getAgentCode('"+fm.all('AgentNew').value+"') "
	  +"and "
	  + "ValidFlag='N' and AscripState='2' and contno='"+fm.all('ContNo').value+"' and maketype <> '01' ";
	  var strQueryResult = easyQueryVer3(SQL, 1, 1, 1);
	//  alert(strQueryResult);
	  if(strQueryResult)
	  {
          alert("请执行删除操作！");
          return false;
      }  
      else
	  {
    	//alert("111")
		if((fm.all('ContNo').value==''||fm.all('ContNo').value==null)&&ContGrid.mulLineCount>0){
  			alert("原代理人含有未签单或未回执回销的保单，不能做保单分配!");
  			return false;
  		}
	  //alert("111")
		if (confirm("您确实想修改该记录吗?"))
		{
		    mOperate="UPDATE||MAIN";
		    submitForm();
		}
		else
		{
		    mOperate="";
		    alert("您取消了修改操作！");
		}
	   } 
	 }   	 
	 else//一个人多张保单分配的情况
	 {
	      alert("请执行删除操作！");
	      return false;
	 }
	    
	 if(fm.all('GrpContNo').value!=null&&fm.all('GrpContNo').value!="")
	 {
	  var SQL = "select 'Y' from LAAscription where agentold=getAgentCode('"+fm.all('AgentOld').value+"') and agentnew=getAgentCode('"+fm.all('AgentNew').value+"') and "
	  + "ValidFlag='N' and AscripState='2'  and grpcontno='"+fm.all('GrpContNo').value+"' and maketype <> '01'";
	  var strQueryResult = easyQueryVer3(SQL, 1, 1, 1);
	//  alert(strQueryResult);
	  if(strQueryResult)
	  {
          alert("请执行删除操作！");
          return false;
      }  
      else
	  {
		if((fm.all('GrpContNo').value==''||fm.all('GrpContNo').value==null)&&ContGrid.mulLineCount>0){
  			alert("原代理人含有未签单或未回执回销的保单，不能做保单分配!");
  			return false;
  		}
		if (confirm("您确实想修改该记录吗?"))
		{
		    mOperate="UPDATE||MAIN";
		    submitForm();
		}
		else
		{
		    mOperate="";
		    alert("您取消了修改操作！");
		}
	   } 
	 }   	 
	 else//一个人多张保单分配的情况
	 {
	    alert("请执行删除操作！");
	    return false;
	 }   	    
    }
   else
   {
     if (confirm("您确实想修改该记录吗?"))
	 {
		    mOperate="UPDATE||MAIN";
		    submitForm();
	 }
	 else
	 {
		    mOperate="";
		    alert("您取消了修改操作！");
	 }
  }
}           


//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  //判断新代理人与保单号是否完全相同，相同则执行删除
 
  if(fm.all('AgentNew').value!=fm.all('AgentNew1').value)
  {
   	 alert("请执行修改操作！");
  }
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function queryAgentCont(pmAgentCode)
{
	var tSQL = "";
	tSQL =" select grpcontno,contno,prtno,appntname,prem,signdate,"
	    +" case when customgetpoldate is null then customgetpoldate else getpoldate end "
	    +" from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"') and grpcontno='00000000000000000000' "
	    +" and   (signdate is null or getpoldate is null or customgetpoldate is null ) "
	    +" and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) "
	    +" and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
	tSQL +=" union select grpcontno,'团单对应多个个单号',prtno,grpname,prem,signdate,"
	     +" case when customgetpoldate is null then customgetpoldate else getpoldate end   "
	     +" from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"') "
	     +" and   (signdate is null or getpoldate is null or customgetpoldate is null ) "
	     +" and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2'  and uwflag<>'1') or uwflag is null) "
	     +" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = lcgrpcont.grpcontno)" ;
	
	//执行查询并返回结果
	turnPage.queryModal(tSQL, ContGrid);
}
function doDownload(){
   var pmAgentCode  = fm.all('AgentOld').value;
   if ( pmAgentCode == null || pmAgentCode == "" )
   {
	alert("请输入原销售人员代码！");
	return;
   }
// 书写SQL语句
	var tSQL = "";
	
	tSQL =" select grpcontno,contno,prtno,appntname,prem,signdate,"
	     +" case when customgetpoldate is null then customgetpoldate else getpoldate end "
	     +" from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"') and grpcontno='00000000000000000000'"
	     +" and   (signdate is null or getpoldate is null or customgetpoldate is null ) "
	     +" and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) "
	     +" and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
	tSQL +=" union select grpcontno,'团单对应多个个单号',prtno,grpname,prem,signdate,"
	     +" case when customgetpoldate is null then customgetpoldate else getpoldate end  "
	     +" from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"') "
	     +" and   (signdate is null or getpoldate is null or customgetpoldate is null )"
	     +" and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2'  and uwflag<>'1') or uwflag is null)"
	     +" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = lcgrpcont.grpcontno)" ;

    fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "ALAAscriptionReport.jsp";
    fm.submit();
    fm.action = oldAction;
}


