var turnPage = new turnPageClass();

/************************************************
 * 功能描述：点击查询按钮触发的事件
 * 传入参数：无
 * 返 回 值：无
 ************************************************/
function queryClick()
{
	var tSQL = "";
	
	tSQL  = "select a.Proposalgrpcontno,";
	tSQL += "       a.ManageCom,";
	tSQL += "       b.BranchAttr,";
	tSQL += "       b.Name,";
	tSQL += "       getUniteCode(a.AgentCode),";
	tSQL += "       c.Name,";
	//tSQL += "       a.AppntNo,";
	tSQL += "       d.GrpName,";
	tSQL += "       a.Peoples2,";
	tSQL += "       a.HandlerDate,";
	tSQL += "   case";
	tSQL += "       when a.AppFlag = '1' then '已承保'";
	tSQL += "       else '未承保'";
	tSQL += "   end as AppFlag,";
	tSQL += "       a.PrtNo,";
	tSQL += "       a.GrpContNo";
	tSQL += "  from LCGrpCont a,";
	tSQL += "       LABranchGroup b,";
	tSQL += "       LAAgent c,";
	tSQL += "       LDGrp d";
	tSQL += " where a.salechnl = '03'";
	tSQL += "   and a.AgentGroup = b.AgentGroup";
	tSQL += "   and a.AgentCode = c.AgentCode";
	tSQL += "   and a.AppntNo = d.CustomerNo ";
	tSQL += "   and a.ManageCom like '"+document.fm.ManageComCode.value+"%25'";
	tSQL += getWherePart('a.Proposalgrpcontno','GrpContNo','like');
	tSQL += getWherePart('a.ManageCom','ManageCom');
	tSQL += getWherePart('b.BranchAttr','BranchAttr','like');
	tSQL += getWherePart('b.Name','BranchName','like');
	tSQL += getWherePart('c.groupAgentCode','AgentCode','like');
	tSQL += getWherePart('c.Name','AgentName','like');
	tSQL += getWherePart('a.AppntNo','CustomerNo','like');
	tSQL += getWherePart('d.GrpName','CustomerName','like');
	tSQL += getWherePart('b.BranchType','BranchType');
	tSQL += getWherePart('b.BranchType2','BranchType2');
	// 判断是否录入限制期间
	var tLogginDateF = document.fm.LogginDateF.value;
	var tLogginDateT = document.fm.LogginDateT.value;
	if(tLogginDateF!="" && tLogginDateF != null)
	{
		tSQL += "  and a.HandlerDate >= '"+tLogginDateF+"'";
	}
	if (tLogginDateT!="" && tLogginDateT != null)
	{
		tSQL += "  and a.HandlerDate <= '"+tLogginDateT+"'";
	}
	//alert(tSQL);
	//执行查询并返回结果
	turnPage.queryModal(tSQL, ContGrid);
}

/************************************************
 * 功能描述：点击查看详细按钮触发的事件
 * 传入参数：无
 * 返 回 值：无
 ************************************************/
function partClick()
{
	var checkFlag = -1;
	var i = 0;
	var tGrpContNo = "";
	var tGrpPolNo = "";
	var tGrpPrtNo = "";
	
	for (i=0; i<ContGrid.mulLineCount; i++)
	{
    if (ContGrid.getSelNo(i))
    { 
      checkFlag = ContGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag == -1)
  {
  	alert("请选择一条记录！");
  	return;
  }
  
  tGrpContNo = ContGrid.getRowColData(checkFlag-1,1);      // 获得选中的保单号
  tGrpPrtNo = ContGrid.getRowColData(checkFlag-1,11);      // 获得选中的印刷号
  
  easyScanWin = window.open("./ALAParticularQuery.jsp?LoadFlag=16&contNo="+tGrpContNo+"&prtNo="+tGrpPrtNo, "", "status=no,resizable=yes,scrollbars=yes ");
}