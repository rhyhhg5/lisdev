//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet; 
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  initPolGrid();
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAHolsQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
/*
function returnParent()
{
 
  var tRow=HolsGrid.getSelNo();
   if(tRow>0)
   {
     //得到被选中的记录字符串
     tRow--;
     //alert(tRow);
     var str=fm.all("LAHolsInfo"+tRow).value;
     var arrRecord = str.split("|");  //拆分字符串，形成返回的数组
     for(var i=0; i<15;i++)
     {
       if(arrRecord[i]=='null')
         arrRecord[i]='';
     }
     top.opener.fm.all('AgentCode').value = arrRecord[0];
     top.opener.fm.all('AgentGroup').value = arrRecord[1];
     top.opener.fm.all('ManageCom').value = arrRecord[2];
     top.opener.fm.all('Idx').value = arrRecord[3];
     top.opener.fm.all('AClass').value = arrRecord[4];
     top.opener.fm.all('VacDays').value = arrRecord[5];
     top.opener.fm.all('LeaveDate').value = arrRecord[6];
     top.opener.fm.all('AbsDays').value = arrRecord[7];
     top.opener.fm.all('EndDate').value = arrRecord[8];
     top.opener.fm.all('ApproveCode').value = arrRecord[9];
     top.opener.fm.all('FillFlag').value = arrRecord[10];
     top.opener.fm.all('ConfIdenFlag').value = arrRecord[11];
     top.opener.fm.all('AddVacFlag').value = arrRecord[12];
     top.opener.fm.all('Noti').value = arrRecord[13];
     top.opener.fm.all('Operator').value = arrRecord[14];
     top.close();
   }
   else
   {
     alert("请先选择记录！");	
   }
}*/

//修改人：解青青   时间：2014-11-14

function checkValid()
{
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  if (getWherePart('GroupAgentCode')!='')
  {
     strSQL = "select a.agentcode from LAHols a,LAAgent b where 1=1 and a.agentcode = b.agentcode "
	   + getWherePart('GroupAgentCode','GroupAgentCode')
	    + " and a.branchtype='"+tBranchType+"'"
	    + " and a.branchtype2='"+tBranchType2+"'";
	    
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('GroupAgentCode').value = '';
    return false;
  }
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('GroupAgentCode').value='';
    return;
  }
  //查询成功则拆分字符串，返回二维数组
   
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);　
  fm.all('AgentCode').value = tArr[0][0];
}


function returnParent()
{
	
  var arrReturn = new Array();
	var tSel = HolsGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				
				top.opener.afterQuery( arrReturn );
				
				
			}
			
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			
			top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = HolsGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");	
	arrSelected = new Array();
	var strSQL = "";
	strSQL = "select "
	         + "getunitecode(a.AgentCode),"
	         +"a.BranchAttr,"
	         +"a.ManageCom,"
	         +"a.Idx,"
	         +"a.IndexCalNo,"	         
	         +"a.Aclass,"
	         +"a.VACDays,"
	         +"a.LeaveDate,"
	         +"a.ShouldEndDate,"
	         +"a.ABSDays,"
	         +"a.FillFlag,"
	         +"a.ConfidenFlag,"
	         +"a.AddVACFlag,"
	         +"a.SumMoney,"
	         +"a.APPRoveCode,"
	         +"a.Noti,"	
	         +"a.Operator,"
	         +"a.AgentGroup,"
	         +"a.EndDate,"
	         +"c.Name,"
	         +"a.LeaveState,a.ModifyDate"
	         +",c.agentcode "
	         +" from LAHols a,LAAgent c where 1=1 "
	         +" and c.groupagentcode = '"+HolsGrid.getRowColData(tRow-1,1)+"' and a.idx="+HolsGrid.getRowColData(tRow-1,2)+""
	         +" and a.AgentCode=c.AgentCode"
	         +" order by a.agentcode";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功

  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	 
	return arrSelected;
}


// 查询按钮
function easyQueryClick()
{
	/*
	//查询修改
	var old_AgentGroup=fm.all('AgentGroup').value;
	var new_AgentGroup="";
	if (old_AgentGroup!='')
	{
      strSQL_AgentGroup = "select AgentGroup from labranchgroup where 1=1 "
                         +"and BranchAttr='"+old_AgentGroup+"' and (state<>'1' or state is null)"
      var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
      if (!strQueryResult_AgentGroup)
      {
      	alert("查询失败");
      	return false;
      }
      if (strQueryResult_AgentGroup)
      {
        var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
        var tArr_AgentGroup = new Array();
        tArr_AgentGroup = chooseArray(arrDataSet_AgentGroup,[0,1,2]);
        //以备显示时使用
        //fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
        new_AgentGroup=tArr_AgentGroup[0][0];
      }
    }
    var str_new_AgentGroup="";
    if (new_AgentGroup!='')
      str_new_AgentGroup=" and a.AgentGroup='"+new_AgentGroup+"' ";
    */
    
		// 初始化表格
	initHolsGrid();
	var strAgent = "";
//	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
//		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
//	}
	var strSQL = "";
	var tReturn = getManageComLimitlike("a.ManageCom");
	strSQL = "select getUniteCode(a.AgentCode),a.Idx,a.BranchAttr,a.ManageCom,a.Aclass,a.LeaveDate,a.VACDays,a.ShouldEndDate,a.EndDate"
	         +" from LAHols a where 1=1 "
	         + tReturn
	         + getWherePart('a.AgentCode','AgentCode')
//	         +strAgent
	         //+ str_new_AgentGroup
	         + getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Idx','Idx','','1')
	         + getWherePart('a.AClass','AClass')
	         + getWherePart('a.VacDays','VacDays','','1')
	         + getWherePart('a.LeaveDate','LeaveDate')
	         + getWherePart('a.ShouldEndDate','ShouldEndDate')
	         + getWherePart('a.ApproveCode','ApproveCode')
	         + getWherePart('a.FillFlag','FillFlag')
	         + getWherePart('a.ConfIdenFlag','ConfIdenFlag')
	         + getWherePart('a.AddVacFlag','AddVacFlag')
	         + getWherePart('a.Noti','Noti')
	         + getWherePart('a.Operator','Operator')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	         + getWherePart('a.BranchAttr','AgentGroup');		 
	         //if (fm.all('SumMoney').value!=null && fm.all('SumMoney').value!='')
	         //{
	         //  strSQL+=" and a.SumMoney="+fm.all('SumMoney').value;
	         //}
	         //alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }    
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = HolsGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES); 
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}
function ListExecl()
{  
//定义查询的数据
var strSQL = "";
strSQL = "select getUniteCode(a.AgentCode),a.Idx,a.BranchAttr,a.ManageCom,a.Aclass,a.LeaveDate,a.VACDays,a.ShouldEndDate,a.EndDate"
	         +" from LAHols a where 1=1 "
	         + getWherePart('a.AgentCode','AgentCode')
	         + getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Idx','Idx','','1')
	         + getWherePart('a.AClass','AClass')
	         + getWherePart('a.VacDays','VacDays','','1')
	         + getWherePart('a.LeaveDate','LeaveDate')
	         + getWherePart('a.ShouldEndDate','ShouldEndDate')
	         + getWherePart('a.ApproveCode','ApproveCode')
	         + getWherePart('a.FillFlag','FillFlag')
	         + getWherePart('a.ConfIdenFlag','ConfIdenFlag')
	         + getWherePart('a.AddVacFlag','AddVacFlag')
	         + getWherePart('a.Noti','Noti')
	         + getWherePart('a.Operator','Operator')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	         + getWherePart('a.BranchAttr','AgentGroup');
             
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '营销员代码','记录顺序号','销售机构','管理机构','类别','请假日期','请假天数','应销假日期','销假日期'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '请(销)假管理 查询下载' from dual where 1=1  ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}