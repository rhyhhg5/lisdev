var turnPage = new turnPageClass();

/***************************************************
 * 查询允许分配的业务员（当前保单本部门内的人员）
 ***************************************************/
function getAgentGradeStr()
{
  var tReturn = "";
  var tBranchAttr = document.fm.BranchAttr.value;
  var tManageCom  = document.fm.ManageCom.value;
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  
  var tBranchattr = fm.all('BranchAttr').value;  // 加个机构编码 
  var sSql = "";

  sSql  = "SELECT";
  sSql += "   a.agentcode,";
  sSql += "   c.name,";
  sSql += "   a.agentgrade,";
  sSql += "   d.gradename";
  sSql += " FROM";
  sSql += "   LATree a,";
  sSql += "   LABranchGroup b,";
  sSql += "   LAAgent c,";
  sSql += "   LAAgentGrade d";
  sSql += " WHERE";
  sSql += "   b.branchtype='"+tBranchType+"' AND";
  sSql += "   b.branchtype2='"+tBranchType2+"' AND";
  sSql += "   a.agentcode=c.agentcode   AND";
  sSql += "   a.agentgroup = b.agentgroup AND";
  sSql += "   b.ManageCom like '"+tManageCom+"%'  AND";  
  sSql += "   b.branchattr like '"+tBranchattr+"%'  AND";  
  sSql += "   a.agentgrade=d.gradecode  and ";
  sSql += "   c.agentState<='02' ";

  var tqueryRe = easyQueryVer3(sSql,1,0,1);
  if (tqueryRe)
    tReturn = tqueryRe;
  else
  	tReturn = null;
  return tReturn;
  
}

/***************************************
 * 输入团体合同号触发的事件
 *     参数：无
 *     返回：boolean
 ***************************************/
function queryCompact()
{
	var tGrpCont = trim(document.fm.GrpContNo.value);
	var arrGrpCont;
	var tAgentCode = "";
	
	if (tGrpCont == "")
	  return false;
	
	
	arrGrpCont = queryGrpCont(tGrpCont);
	//alert("$$$$");
	if (fm.all('conttype').value =='2'&&arrGrpCont == null)
	{
		alert("团体保单号码不存在！");
		//初始化画面
		initForm();
		document.fm.GrpContNo.value = tGrpCont;
	  return false;
	}
	else if(fm.all('conttype').value=='1'&&arrGrpCont == null){
	   alert("个单号码不存在！");
	 //初始化画面
		initForm();
		document.fm.GrpContNo.value = tGrpCont;
	  return false;
	}
	//alert("&&&&&&")
	//设置画面各文本框的值
	document.fm.GrpPolNo.value = arrGrpCont[0][1];
	document.fm.ManageCom.value = arrGrpCont[0][2];
	document.fm.BranchAttr.value = arrGrpCont[0][3];
	document.fm.AppntNo.value = arrGrpCont[0][4];
	document.fm.AppntName.value = arrGrpCont[0][5];
	document.fm.TransMoney.value = arrGrpCont[0][6];
	document.fm.RiskNum.value = arrGrpCont[0][7];
	document.fm.CurPayToDate.value = arrGrpCont[0][8];
	document.fm.SignDate.value = arrGrpCont[0][9];
	document.fm.Operator.value = arrGrpCont[0][10];
	tAgentCode = arrGrpCont[0][11];
	fm.all('OldAgentCode').value=tAgentCode;
	fm.all('AgentGroup').value=arrGrpCont[0][12];
	fm.all('OldAgentCode').value=tAgentCode;
	//初始化列表
	initPrincipalGrid();
	
	//设置负责人信息
	queryAgent(tGrpCont,tAgentCode);
	
	return true;
}

/***************************************
 * 通过团体合同号进行查询负责业务员信息
 *     参数：pmGrpContNo  团体合同号
 *           pmAgentCode  保单原始负责人编码
 *     返回：查询结果
 ***************************************/
function queryAgent(pmGrpContNo,pmAgentCode)
{  //alert("!!!!:"+pmAgentCode);
   //alert("rrrrrr:"+fm.all('BranchType').value);
   //alert("tttttt:"+fm.all('BranchType2').value);
	var tSQL = "";
	
   // 直接从保单层获取人员信息 update 2013-5-2
	    tSQL  = "select a.AgentCode,";
	  tSQL += "       a.Name,";
	  tSQL += "       b.AgentGrade,";
	  tSQL += "       c.GradeName";
	  tSQL += "  from LAAgent a,LATree b,LAAgentGrade c";
	  tSQL += " where a.AgentCode = b.AgentCode";
	  tSQL += "   and b.AgentGrade = c.GradeCode";
	  tSQL += "   and a.BranchType='"+fm.all('BranchType').value+"'";
	  tSQL += "   and a.BranchType2='"+fm.all('BranchType2').value+"'";
	  tSQL += "   and a.AgentCode = '"+pmAgentCode+"'";
      //alert("yyyyy:"+tSQL);
		// 显示查询结果
	  queryAgentGride(tSQL);
}

/***************************************
 * 通过团体合同号进行查询其详细信息
 *     参数：pmGrpContNo  团体合同号
 *     返回：查询结果
 ***************************************/
function queryGrpCont(pmGrpContNo)
{ //alert("%%%%");
 var tSQL1 = "";
 var sql="select conttype from lccont where grpcontno='00000000000000000000' and  contno='"+pmGrpContNo+"'";
     sql+="union all select conttype from lccont where grpcontno<>'00000000000000000000' and grpcontno='"+pmGrpContNo+"'";
 var strQueryResult1 = easyQueryVer3(sql, 1, 1, 1);
 //alert("####");
 //查询成功则拆分字符串，返回二维数组
 var tArr = new Array();
 tArr = decodeEasyQueryResult(strQueryResult1);
 if( tArr != null )
 {
   	fm.all('conttype').value= tArr[0][0];
   	//alert("@@@@:"+tArr[0][0]);
   if(fm.all('conttype').value=="2")
   {
  	tSQL1  = "SELECT";
  	tSQL1 += "   grpcontno,";
  	tSQL1 += "   '',";
  	tSQL1 += "   managecom,";
  	tSQL1 += "   (select BranchAttr from labranchgroup where  labranchgroup.agentgroup=lcgrpcont.agentgroup),";
  	tSQL1 += "   AppntNo,";
  	tSQL1 += "   Grpname,";
  	tSQL1 += "   prem,";
  	tSQL1 += "   (select COUNT(RiskCode) from lcgrppol where lcgrppol.grpcontno=lcgrpcont.grpcontno) ,";
  	tSQL1 += "   (select max(enteraccdate) from ljtempfee where ljtempfee.otherno=lcgrpcont.grpcontno),";
  	tSQL1 += "   signdate,";
  	tSQL1 += "   Operator,";
  	tSQL1 += "   AgentCode,";
  	tSQL1 += "   AgentGroup";
  	tSQL1 += " FROM";
  	tSQL1 += "   lcgrpcont";
  	tSQL1 += " WHERE";
  	tSQL1 += "   grpcontno<>'00000000000000000000' AND";
  	tSQL1 += "   grpcontno='"+pmGrpContNo+"'";              
   }
   else if(fm.all('conttype').value=="1"){
   tSQL1="select contno,'',managecom,(select branchattr from labranchgroup where labranchgroup.agentgroup=lccont.agentgroup),";
   tSQL1 +="AppntNo,AppntName,prem,(select COUNT(RiskCode) from lcpol where lcpol.contno=lccont.contno),";
   tSQL1 +="(select max(enteraccdate) from ljtempfee where ljtempfee.otherno=lccont.contno),signdate,Operator,AgentCode,AgentGroup from lccont where ";
   tSQL1 +="grpcontno='00000000000000000000' and contno='"+pmGrpContNo+"'";                    
   }
  
  }
    //alert("*****");
    //alert("^^^:"+tSQL1);
	return getArrValueBySQL(tSQL1);
}

/***************************************
 * 根据参数查询结果
 *     参数：pmSQL  查询用的SQL文
 *     返回：查询结果
 ***************************************/
function getArrValueBySQL(pmSQL)
{
	//alert(pmSQL);
	var strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
	
	if (!strQueryResult)
		return null;
		
	var arr = decodeEasyQueryResult(strQueryResult);
	
	return arr;
}

/***************************************
 * 点击查询按钮的触发事件
 ***************************************/
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LACompactSplitQueryHtml.jsp?BranchType="+fm.all('BranchType').value+"&BranchType2="+fm.all('BranchType2').value);
}

/***************************************
 * 响应查询页面返回按钮的事件
 *     参数：pmGrpContNo
 *     返回：无
 ***************************************/
function afterQuery(pmGrpContNo)
{
	//表示出返回的团体保单号
	document.fm.GrpContNo.value = pmGrpContNo;
	
	//通过传回来的团体保单号查询相关信息
	queryCompact(pmGrpContNo);
}

/***************************************
 * 通过SQL文查询结果显示结果
 *     参数：pmSQL  查询语句
 *     返回：无
 ***************************************/
function queryAgentGride(pmSQL)
{
  //执行查询并返回结果
	turnPage.queryModal(pmSQL, PrincipalGrid);
}

/**************************************
 * 提交画面
 **************************************/
function submitForm()
{
  if(!verifyInput())
  {
	 	return false;
	}
	if(!PrincipalGrid.checkValue())
  {
	 	return false;
	}
	if(!chkMulLine())
	{
	 	return false;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}

/**************************************
 * 提交后操作,服务器数据返回后执行的操作
 **************************************/
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //document.fm.listAgentCode.value="";
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    initForm();
  }
}
function afterCodeSelect( cCodeName, Field )
{
	var tvalue = Field.value;
	//alert(tvalue);
  if (tvalue == null || tvalue == '')
     return false;
	try	
	{
		if (cCodeName=="AgentCode")
		{
			var tsql="select agentcode from laagent where agentcode='"+tvalue+"' and  agentstate>='03' ";
			
			var strQueryResult = easyQueryVer3(tsql, 1, 1, 1);
		  if (strQueryResult)
		  {
		   	  	alert("人员"+tvalue+"已经离职，不能归属！");
		  	  	return false;
		   }
		    
		}
	}
	catch(ex)
	{
		alert("选择业务员出错");
	}
}

							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
		{
								//alert("enter chkmulline");
								var i;
								var selFlag = true;
								var iCount = 0;
								var rowNum = PrincipalGrid.mulLineCount;
								var tagentgroup= fm.all('AgentGroup').value;
								for(i=0;i<rowNum;i++)
								{
									var tagent=PrincipalGrid.getRowColData(i,1);
									var tsql="select agentcode from laagent where agentcode='"+tagent+"' and  agentstate<='02'  and agentgroup='"+tagentgroup+"'";
			
			            var strQueryResult = easyQueryVer3(tsql, 1, 0, 1);
		              if (!strQueryResult)
		               {
		   	  	        alert("人员"+tagent+"不在保单所属团队，无法进行保单归属操作！");
		  	  	        return false;
		               }
								}	
										return true;
     }