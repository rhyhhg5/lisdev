<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AbsenceQueryInput.jsp
//程序功能：
//创建日期：2003-7-7
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./AbsenceQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./AbsenceQueryInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>员工考勤信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./AbsenceQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAbsence1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divAbsence1" style= "display: ''">
    <table  class= common>
        <TR  class= common>
          <TD  class= title>
            员工编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this], [0,1],null,acodeSql,'1',null,500);" onkeyup="return showCodeListKey('AgentCode', [this], [0,1],null,acodeSql,'1',null,500);">
          </TD>
          <TD  class= title>
            销售机构
          </TD>
          <TD  class= input>
            <Input class=common name=AgentGroup >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);" >
          </TD>
          <TD  class= title>
            纪录顺序号
          </TD>
          <TD  class= input>
            <Input class=common name=Idx >
          </TD>
        </TR>
        <!--TR  class= common>
          <TD  class= title>
            类别
          </TD>
          <TD  class= input>
            <Input class='code' name=AClass maxlength=1 ondblclick="return showCodeList('presenceaclass',[this]);" onkeyup="return showCodeListKey('presenceaclass',[this]);">
          </TD>
          <TD  class= title>
            考勤执行次数
          </TD>
          <TD  class= input>
            <Input class= common name=Times >
          </TD>
        </TR-->
        <TR  class= common>
          <TD  class= title>
            缺勤时间（单位：小时）
          </TD>
          <TD  class= input>
            <Input class=common name=SumMoney >
          </TD>
          <TD  class= title>
            批注
          </TD>
          <TD  class= input>
            <Input class= common name=Noti >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            执行日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=DoneDate dateFormat='short' >
          </TD>
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class=common name=Operator >
          </TD>
        </TR>
      </table>
          <input type=hidden name=BranchType value=''>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPresenceGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPresenceGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPresenceGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

<script>
  var acodeSql = "#1# and (BranchType=#2# or BranchType=#3#) and (AgentState = #01# or AgentState = #02#)";
</script>
