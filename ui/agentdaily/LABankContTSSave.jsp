<%@include file="../common/jsp/UsrCheck.jsp"%> 
<% 
  //程序名称：LABankCOntSave.jsp
  //程序功能：
  //创建日期：2005-03-20 18:05:58 
  //创建人  ：CrtHtml程序创建
  //更新记录：  更新人    更新日期     更新原因/内容 
%> 
    <!--用户校验类--> 
   <%@page contentType="text/html;charset=GBK" %> 
   <%@page import="com.sinosoft.utility.*"%>
   <%@page import="com.sinosoft.lis.schema.*"%> 
   <%@page import="com.sinosoft.lis.vschema.*"%> 
   <%@page import="com.sinosoft.lis.pubfun.*"%> 
   <%@page import="com.sinosoft.lis.agentdaily.*"%> 
<% 
  //接收信息，并作校验处理
  LAContSchema tLAContSchema   = new LAContSchema(); 
  LAContSet  tLAContSet = new LAContSet(); 
  LARateChargeSchema tLARateChargeShema=new LARateChargeSchema();
  LARateChargeSet tLARateChargeSet=new LARateChargeSet();
  LAAuthorizeSchema tLAAuthorizeSchema=new LAAuthorizeSchema();
  LAAuthorizeSet tLAAuthorizeSet=new LAAuthorizeSet();
  String mProtocalNo="";
  LAContUI tLAContUI   = new LAContUI();
  //输出参数 
  CErrors tError = null;
  String tRela  = "";             
  String FlagStr = "";
  String Content = "";
  String transact = "";  
  String tOperate="INSERT||MAIN"; 
  String mTest =""; 
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录       
  transact = request.getParameter("fmtransact");
  String LAContType=request.getParameter("LAContType");
  System.out.println("LAContType:"+LAContType);
	String tProtocolNo = request.getParameter("ProtocolNo");
	if(!transact.equals("INSERT||MAIN"))
	{
		tLAContSchema.setProtocolNo(tProtocolNo);
	}

  tLAContSchema.setSignDate(request.getParameter("SignDate"));
  tLAContSchema.setManageCom(request.getParameter("ManageCom"));
  tLAContSchema.setAgentCom(request.getParameter("AgentCom"));

  tLAContSchema.setRepresentA(request.getParameter("RepresentA"));
  tLAContSchema.setRepresentB(request.getParameter("RepresentB"));
  tLAContSchema.setStartDate(request.getParameter("StartDate"));
  tLAContSchema.setEndDate(request.getParameter("EndDate"));

  //取得Muline信息
  int lineCount = 0;
  String tRiskCode[] = request.getParameterValues("ContGrid1");
  String tRate[] = request.getParameterValues("ContGrid3");//Rate
  String tAuthorizeArea[] = request.getParameterValues("ContGrid4");
  String tMaxAmnt[]=request.getParameterValues("ContGrid5");
  String tCalType[]=request.getParameterValues("ContGrid6");//0代表不区分出单方式1代表柜台银保通出单2代表自助和网银出单
  if(tRiskCode == null)
  {
  	lineCount = 0;
  }
  else
  {
  	lineCount = tRiskCode.length; //行数
  }
  System.out.println("length= "+String.valueOf(lineCount));

  try
  {
    tLAAuthorizeSchema=new LAAuthorizeSchema();
    for(int i=0;i<lineCount;i++)
    {
      
     tLAAuthorizeSchema=new LAAuthorizeSchema();
     System.out.println("tLAAuthorizeSet Begin " + tRiskCode[i]);
     tLAAuthorizeSchema.setRiskCode(tRiskCode[i]); 
     tLAAuthorizeSchema.setAuthorType(tCalType[i]);
     tLAAuthorizeSchema.setBranchType(request.getParameter("BranchType"));
     tLAAuthorizeSchema.setBranchType2(request.getParameter("BranchType2"));
     tLAAuthorizeSchema.setAuthorizeArea(tAuthorizeArea[i]);
     tLAAuthorizeSchema.setMaxAmnt(tMaxAmnt[i]);
     tLAAuthorizeSet.add(tLAAuthorizeSchema);    
     LARateChargeSchema aLARateChargeSchema=new LARateChargeSchema();	  
     System.out.println("tLAAuthorizeSet OK "+tRiskCode[i]);
     aLARateChargeSchema.setRiskCode(tRiskCode[i]); 
     aLARateChargeSchema.setPayIntv("0");
     aLARateChargeSchema.setStartYear(0);
     aLARateChargeSchema.setEndYear(999);
     aLARateChargeSchema.setRate(tRate[i]);
     aLARateChargeSchema.setCalType("51"+tCalType[i]);   
 //    aLARateChargeSchema.setF03(tF3[i].trim());
     tLARateChargeSet.add(aLARateChargeSchema);
    }
  }catch(Exception exex)
  {
  exex.printStackTrace();
  }
  System.out.println(tLARateChargeSet.size());
  System.out.println(tLAAuthorizeSet.size());

  System.out.println("transact"+transact); 
 
  System.out.println("end 档案信息...");
  //    mTest=tLAContSet.get(1).getAgentCode();
   //   System.out.println("mTest+++"+mTest);
 // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.addElement(tLAContSchema);
  tVData.addElement(tLARateChargeSet);
  tVData.addElement(tLAAuthorizeSet);
  tVData.addElement(LAContType);
  try
  {
    tLAContUI.submitData(tVData,transact);
    
  }
  catch(Exception ex)
  {
 	ex.printStackTrace();
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLAContUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	mProtocalNo=(String)(tLAContUI.getResult().get(0));
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProtocalNo%>");
</script>
</html>
