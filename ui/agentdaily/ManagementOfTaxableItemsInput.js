//该文件中包含客户端需要处理的函数和事件
//程序名称：ManagementOfTaxableItemsSave.jsp
//程序功能：
//创建日期：2016-03-30
//创建人  ：  zhangxinjing
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var queryFlag = false;
var strSQL ="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
	if(showInfo!=null)
		{
		  try
		  {
		    showInfo.focus();  
		  }
		  catch(ex)
		  {
		    showInfo=null;
		  }
	}
}
//提交，保存按钮对应操作
function DoInsert()
{
	if(fm.all('hideOperate').value=='Query') 
	{	
		alert("查询的结果只能进行修改和删除操作！");
		return false;
	}
	
	if(verifyInput() == false ) return false; 
	if(!beforeSubmit()) return false;
	if (confirm("您确实想保存该记录吗?"))
	{
		fm.all('hideOperate').value='INSERT';
		if(fm.all('CBPTaxes').value==''||fm.all('CBPTaxes').value==null){
			fm.all('CBPTaxes').value=0;
		}
		if(fm.all('PCBPTaxes').value==''||fm.all('PCBPTaxes').value==null){
			fm.all('PCBPTaxes').value=0;
		}
		if(fm.all('ITBPTaxes').value==''||fm.all('ITBPTaxes').value==null){
			fm.all('ITBPTaxes').value=0;
		}
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
		fm.all('GroupAgentCode').value='';
	}
	else
	{
		alert("您取消了保存操作！");
	}
}
function DoUpdate()
{	
	if(fm.all('hideOperate').value!='Query') 
	{	
		alert("请先点击查询！");
		return false;
	}
	if(verifyInput() == false ) return false; 
	if (confirm("您确实想修改该记录吗?"))
	{		
		fm.all('hideOperate').value='UPDATE'; 		
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交		
		fm.all('GroupAgentCode').value='';		
	}
	else
	{
		alert("您取消了修改操作！");
	}
}
function DoDel()
{
	if(fm.all('hideOperate').value!='Query') 
	{	
		alert("请先点击查询！");
		return false;
	}	
	if(verifyInput() == false ) return false; 
	if(!beforeSubmit()) return false;
	if (confirm("您确实想删除该记录吗?"))
	{
		fm.all('hideOperate').value='DELETE';
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
		fm.all('GroupAgentCode').value='';
	}
	else
	{
		alert("您取消了删除操作！");
	}
}

function clearImport(){
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
}
// 查询按钮
function easyQueryClick()
{
	fm.all('hideOperate').value='';
	fm.all('GroupAgentCode').value='';
	fm.all('AgentName').value='';
	fm.all('Branch').value='';
	fm.all('Managecom').value='';
	fm.all('CBPTaxes').value='';
	fm.all('PCBPTaxes').value='';
	fm.all('ITBPTaxes').value=''; 
	fm.all('WageNo').value='';
	fm.all('Operator').value=''; 
	fm.all('ModifyDate').value=''; 
	fm.all('AgentCode').value='';
	fm.all('BranchType').value=''; 
	fm.all('BranchType2').value='';
    showInfo=window.open("./ManagementOfTaxableItemsQueryHtml.jsp");
}
				
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

//提交前的校验、计算
function beforeSubmit()
{
	return true;
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	 document.all.GroupAgentCode.readOnly=false;
	 document.all.WageNo.readOnly=false;
	 initInpBox();
	 showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}
function importList2()
{
	fmImport.btnImport2.disabled = true;   
    var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fmImport.action = "./MaImportActiveSave.jsp";
    fmImport.submit();
}
function afterImportCertifyList(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("导入批次失败，请尝试重新进行申请。");
       
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");       
    }
    fmImport.btnImport2.disabled = false; 
}

function moduleDownload(){
	var tSQL="";
	fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "./ManagementOfTaxableItemsDownload.jsp";
    fm.submit();
    fm.action = oldAction;
}
function queryImportResult(){
	var tbatchno=fmImport.FileImportNo.value;
	var tqueryType=fmImport.queryType.value;
	strSQL = "select batchno,contid,errorinfo"
	+ " from lcgrpimportlog "
	+ " where 1=1 and contno='Active'  and grpcontno='00000000000000000000' " ;
	if(tbatchno!=""&&tbatchno!=null)
	{
		strSQL+=" and batchno='"+tbatchno+"' ";
	}
	if(tqueryType==1){
		strSQL+=" and errorstate='1'";
	}else if(tqueryType==2){
		strSQL+=" and errorstate='0'";
	}
	strSQL+=" order by contid";
  //执行查询并返回结果
	turnPage2.queryModal(strSQL, ImportResultGrid);
}

function changeAgent()
{
	var tAgentCode=fm.all('GroupAgentCode').value;
	var strSQL = "select a.name,(select branchattr from labranchgroup where agentgroup=a.agentgroup),a.managecom,a.agentcode,a.BranchType,a.BranchType2"
				+ " from laagent a where a.groupagentcode='"+fm.all('GroupAgentCode').value+"' ";
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   	if (!strQueryResult)
   	{
   		alert("系统中没有代码为"+tAgentCode+"营销员!");
   		fm.all('AgentName').value='';
   		fm.all('Branch').value='';
   		fm.all('Managecom').value='';
   		fm.all('AgentCode').value='';
   		fm.all('BranchType').value='';
   		fm.all('BranchType2').value=''; 	
   		return false;
   	}
	var arrResult = decodeEasyQueryResult(strQueryResult);
   	fm.all('AgentName').value=arrResult[0][0];
   	fm.all('Branch').value=arrResult[0][1];
   	fm.all('Managecom').value=arrResult[0][2]; 
   	fm.all('AgentCode').value=arrResult[0][3];
 	fm.all('BranchType').value=arrResult[0][4]; 
   	fm.all('BranchType2').value=arrResult[0][5];
}

function afterCodeSelect( cCodeName, Field )
{
	
}

//查询页面，查询返回操作
function afterQuery(arrQueryResult)
{
	var arr = new Array();
	if( arrQueryResult != null )
	{
		arr = arrQueryResult;
		fm.all('GroupAgentCode').value=arr[0][0];
		fm.all('AgentName').value=arr[0][1];
		fm.all('Branch').value=arr[0][2];
		fm.all('Managecom').value=arr[0][3];
		fm.all('CBPTaxes').value=arr[0][4];
		fm.all('PCBPTaxes').value=arr[0][5];
		fm.all('ITBPTaxes').value=arr[0][6];
		fm.all('WageNo').value=arr[0][7];
		fm.all('Operator').value=arr[0][8];
		fm.all('ModifyDate').value=arr[0][9];
		fm.all('AgentCode').value=arr[0][10];
		fm.all('BranchType').value=arr[0][11];
		fm.all('BranchType2').value=arr[0][12];
		fm.WageNo.readOnly=true;
		fm.GroupAgentCode.readOnly=true;
		//fm.button1.disabled=true;
		fm.all('hideOperate').value='Query';		
	}
}


