/** 
 * 程序名称：ALAAscriptionQuery.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-21 15:48:13
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮

function easyQueryClick() {
	//此处书写SQL语句			     
        var tManageCom=getManageComLimit();
	var strSql = "select a.AscripNo, a.ContNo, a.AgentOld,d.name,a.AgentNew,e.name,(select b.AgentGroup from LATree b where b.AgentCode=a.AgentOld) , a.AscriptionCount, a.AClass,(select codename from ldcode where codetype='ascripttype' and code=a.aclass),a.ValidFlag,a.AscriptionDate,case when a.ascripstate='1' then '失败'  when a.ascripstate='2' then '未确认' else '确认' end  ,a.ascripstate,a.operator,a.Noti,a.ModifyDate,a.managecom"
	           + " from LAAscription a,laagent d,laagent e where 1=1 and a.AgentOld=d.agentcode and a.agentnew=e.agentcode  and d.managecom like '"+tManageCom+"%25'  and  e.managecom like '"+tManageCom+"%25' and a.ascripstate<='2' and validflag='N'"  
	           + getWherePart('a.AgentOld', 'PreAgentCode')
          	 + getWherePart('a.AgentOld', 'PreAgentCode')
          	 + getWherePart('d.name', 'PreAgentCodeName')
          	 + getWherePart('e.name', 'AgentCodeName')
          	 + getWherePart('a.AgentNew', 'AgentCode')
          	 + getWherePart('a.AscripState', 'AscripState')
          	 + getWherePart('d.ManageCom', 'ManageCom')
          	 + getWherePart('e.ManageCom', 'ManageCom')
          	 + getWherePart('a.ContNo', 'ContNo');
         if ((fm.all("AgentCode").value==null || fm.all("AgentCode").value=="") && (fm.all("AgentCodeName").value==null || fm.all("AgentCodeName").value=="" ))
         { 
           strSql= " select a.AscripNo, a.ContNo, a.AgentOld,d.name,a.AgentNew,(select name from laagent e where e.agentcode=a.agentnew),(select b.AgentGroup from LATree b where b.AgentCode=a.AgentOld) , a.AscriptionCount, a.AClass,(select codename from ldcode where codetype='ascripttype' and code=a.aclass),a.ValidFlag,a.AscriptionDate,case when a.ascripstate='1' then '失败'  when a.ascripstate='2' then '未确认' else '确认' end  ,a.ascripstate,a.operator,a.Noti,a.ModifyDate,a.managecom"
	         + " from LAAscription a,laagent d  where 1=1 and a.AgentOld=d.agentcode and d.managecom like '"+tManageCom+"%25' and a.ascripstate<='2' and validflag='N'"    
	         + getWherePart('a.AgentOld', 'PreAgentCode')
          	 + getWherePart('a.AgentOld', 'PreAgentCode')
          	 + getWherePart('d.name', 'PreAgentCodeName')
          	 + getWherePart('d.ManageCom', 'ManageCom')
          	
          	 + getWherePart('a.AscripState', 'AscripState')
          	 + getWherePart('a.ContNo', 'ContNo') 	 
          	 ;
          }
  //alert(strSql);
 //alert(fm.all("PreAgentCode").value);
	turnPage.queryModal(strSql, LACrossGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  //alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LACrossGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
			//	alert(3);
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
//	alert(1);
	var arrSelected = null;
	tRow = LACrossGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LACrossGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
//alert(2);
	return arrSelected;
}
function showDiv(cDiv,cShow)           
{                                      
  if (cShow=="true")                   
  {                                    
    cDiv.style.display="";             
  }                                    
  else                                 
  {                                    
    cDiv.style.display="none";         
  }                                    
}   
//显示frmSubmit框架，用来调试                                                   
function showSubmitFrame(cDebug)             
{                                            
  if(cDebug=="1")                            
  {                                          
			parent.fraMain.rows = "0,0,50,82,*";   
  }                                          
 	else {                                     
  		parent.fraMain.rows = "0,0,0,82,*";    
 	}                                          
}                                            