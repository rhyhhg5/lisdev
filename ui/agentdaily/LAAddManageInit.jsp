<%
//程序名称：LAWageConfirmInit.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String tBranchtype = "";
    tBranchtype = request.getParameter("BranchType"); 
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  //$initfield$
  }
  catch(ex)
  {
    alert("在LAAddManageInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  fm.BranchType.value = '<%=BranchType%>';
  fm.BranchType2.value = '<%=BranchType2%>';
  fm.LogManagecom.value = '<%=ManageCom%>';

  }
  catch(ex)
  {
    alert("在LAAddManageInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	  initWageGrid();
	  check();
  }
  catch(re)
  {
    alert("LAAddManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化WageGrid
 ************************************************************
 */
function initWageGrid()
  {                               
    
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="调整年月";         //列名
        iArray[1][1]="60px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="营销员代码";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="营销员名称";         //列名
        iArray[3][1]="70px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;   
 
        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="80px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="销售单位代码";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;   

        iArray[6]=new Array();
        iArray[6][0]="调整类型";         //列名
        iArray[6][1]="60px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[7]=new Array();
        iArray[7][0]="调整项目";         //列名
        iArray[7][1]="80px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="调整原因";         //列名
        iArray[8][1]="80px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="调整金额";         //列名
        iArray[9][1]="60px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
      
        
        iArray[10]=new Array();
        iArray[10][0]="流转状态";         //列名
        iArray[10][1]="80px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[11]=new Array();
        iArray[11][0]="次数";         //列名
        iArray[11][1]="0px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
  
        WageGrid = new MulLineEnter( "fm" , "WageGrid" ); 

        //这些属性必须在loadMulLine前
        WageGrid.mulLineCount = 0;   
        WageGrid.displayTitle = 1;
        WageGrid.locked=1;
        WageGrid.canSel=0;
        WageGrid.canChk=1;
       	WageGrid.hiddenPlus = 1;
      	WageGrid.hiddenSubtraction = 1;
        WageGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化WageGrid时出错："+ ex);
      }
    }


</script>