<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AbsenceInput.jsp
//程序功能：
//创建日期：2003-7-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAPresenceSchema tLAPresenceSchema   = new LAPresenceSchema();

  AbsenceUI tAbsence   = new AbsenceUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");


    tLAPresenceSchema.setAgentCode(request.getParameter("AgentCode"));
    
    //存储真实的AgentGroup
    //tLAPresenceSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLAPresenceSchema.setAgentGroup(request.getParameter("HiddenAgentGroup"));
    tLAPresenceSchema.setManageCom(request.getParameter("ManageCom"));
    tLAPresenceSchema.setIdx(request.getParameter("Idx"));
    
    tLAPresenceSchema.setBranchType(request.getParameter("BranchType"));
    
    
    //tLAPresenceSchema.setTimes(request.getParameter("Times"));
    tLAPresenceSchema.setSumMoney(request.getParameter("SumMoney"));
    tLAPresenceSchema.setAClass(request.getParameter("AClass"));
    tLAPresenceSchema.setDoneDate(request.getParameter("DoneDate"));
    tLAPresenceSchema.setNoti(request.getParameter("Noti"));
    tLAPresenceSchema.setDoneFlag("0");
    //tLAPresenceSchema.setOperator(tG.Operator);

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLAPresenceSchema);
	tVData.add(tG);
  try
  {
    tAbsence.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tAbsence.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

