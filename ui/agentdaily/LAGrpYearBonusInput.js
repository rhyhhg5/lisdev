//               该文件中包含客户端需要处理的函数和事件
//程序名称：LAGrpYearBonusInput.js
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
//xqq 2014-11-28
function checkAgentCode()
{
	if(fm.GroupAgentCode.value == null || fm.GroupAgentCode.value == ""){
		fm.AgentCode.value = "";
	}else{
		//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
	   	var sql=" select a.agentcode  from laattend a, laagent b, labranchgroup c where a.agentcode = b.agentcode and a.agentgroup = c.agentgroup" 
	             +getWherePart("b.groupagentcode","GroupAgentCode")
	//   		"groupagentcode='"+fm.GroupAgentCode.value+"' "
	            + getWherePart("a.ManageCom","ManageCom",'like')	;
	    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
	    if(!strQueryResult)
	    {
	      alert("系统中不存在该代理人！");   
	      return false;
	    }
	    
	    var arrDataSet = decodeEasyQueryResult(strQueryResult);
	    var tArr = new Array();
	    tArr = decodeEasyQueryResult(strQueryResult);
	    fm.all('AgentCode').value  =tArr[0][0];
		}
}


//提交，修改按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}
//提交，删除按钮对应操作
function DoDelete()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}
//提交，新增按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initSetGrid();
	if(fm.all('ManageCom').value==null || fm.all('ManageCom').value==''){
		alert("请先选择管理机构！");
		return false;
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.managecom,c.branchattr,c.name,c.agentgroup,getUniteCode(a.agentcode),b.name,a.wageno,a.summoney,a.idx,a.wageno,a.managecom,c.branchattr,a.agentcode,b.groupagentcode "
	+ " from laattend a,laagent b,labranchgroup c "
	+ " where a.agentcode=b.agentcode and a.agentgroup=c.agentgroup and a.managecom like '"
	+ fm.all('ManageCom').value+"%' "
	+ getWherePart( 'a.agentcode','AgentCode')
	+ getWherePart( 'c.branchattr','BranchAttr')
	+ getWherePart( 'b.name','AgentName')
	+ getWherePart( 'a.wageno','WageNo')
	+ " order by  a.managecom,a.agentgroup ";
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
			alert("没有符合条件的数据，请重新录入查询条件！");
	  	return false;
	}

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;

	//查询成功则拆分字符串，返回二维数?				
	//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = SetGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;

	//设置查询起始位置
	turnPage.pageIndex = 0;

	//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);
	//return true;
 }


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	  showInfo.close();
	  if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	  else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			initForm();
		}
		}

//提交前的校验、计算
function beforeSubmit()
{
   if(!chkMulLine()) return false;
   return true;
}
//判断是否选择了要增加、修改或删除的行
function chkMulLine()
	{
		var i;
		var selFlag = true;
		var iCount = 0;
		var rowNum = SetGrid.mulLineCount;
		var tOperator =fm.fmAction.value;
		for(i=0;i<rowNum;i++)
		{
			if(SetGrid.getChkNo(i))
			{
				iCount++;
				if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
				{
					alert("管理机构不能为空");
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
				if((SetGrid.getRowColData(i,2) == null)||(SetGrid.getRowColData(i,2) == ""))
				{
					alert("销售机构编码不能为空");
					SetGrid.setFocus(i,2,SetGrid);
					selFlag = false;
					break;
				}
			if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5) == ""))
				{
					alert("业务员代码不能为空");
					SetGrid.setFocus(i,5,SetGrid);
					selFlag = false;
					break;
				}
				if((SetGrid.getRowColData(i,7) == null)||(SetGrid.getRowColData(i,7) == ""))
				{
					alert("薪资年月不能为空");
					SetGrid.setFocus(i,7,SetGrid);
					selFlag = false;
					break;
				}
				
				if((SetGrid.getRowColData(i,8) == null)||(SetGrid.getRowColData(i,8) == ""))
				{
					alert("年效益奖不能为空");
					SetGrid.setFocus(i,8,SetGrid);
					selFlag = false;
					break;
				}
				if(tOperator=="INSERT")
				{
				  if((SetGrid.getRowColData(i,9) != null)&&(SetGrid.getRowColData(i,9)!= ""))
				  {
				  	var temp = i+1;
				  	alert("第"+temp+"行的数据为查询出来的数据，烦请通过修改按钮进行业务操作！");
				  	selFlag = false;
					break; 
				  }
				}
				if(tOperator=="UPDATE")
				{
				  if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9)== ""))
				  {
				  	var temp = i+1;
				  	alert("第"+temp+"行的数据为新录入的数据，烦请通过新增按钮进行业务操作！");
				  	selFlag = false;
					break; 
				  }
				  // 校验不能修改薪资月
				  if((SetGrid.getRowColData(i,7) != SetGrid.getRowColData(i,10)))
				  {
				    alert("年效益奖修改操作不支持修改薪资年月项!");
					SetGrid.setFocus(i,7,SetGrid);
					selFlag = false;
					break;
				  }
				  // 校验不能修改管理机构
				  if((SetGrid.getRowColData(i,1) != SetGrid.getRowColData(i,11)))
				  {
				    alert("年效益奖修改操作不支持修改管理机构项!");
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				  }
				  // 校验不能修改销售机构编码
				  if((SetGrid.getRowColData(i,2) != SetGrid.getRowColData(i,12)))
				  {
				    alert("年效益奖修改操作不支持修改销售机构编码项!");
					SetGrid.setFocus(i,2,SetGrid);
					selFlag = false;
					break;
				  }
				  // 校验不能修改业务员
				  if((SetGrid.getRowColData(i,5) != SetGrid.getRowColData(i,14)))
				  {
				    alert("年效益奖修改操作不支持修改业务员编码项!");
					SetGrid.setFocus(i,5,SetGrid);
					selFlag = false;
					break;
				  }
				}
				}else
					{//不是选中的行
						if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9)==""))
						{
							alert("有未保存的新增纪录！请先保存记录。");
							SetGrid.checkBoxAllNot();
							SetGrid.setFocus(i,1,SetGrid);
							selFlag = false;
							break;
						}
					}
				}
				if(!selFlag) return selFlag;
				if(iCount == 0)
				{
					alert("请选择要保存或删除的记录!");
					return false
				}
				return true;
			}



	function showDiv(cDiv,cShow)
	{
		if (cShow=="true")
		{
			cDiv.style.display="";
		}
		else
			{
				cDiv.style.display="none";
			}
	}
	//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
   if(cDebug=="1")
     {
	   parent.fraMain.rows = "0,0,0,0,*";
     }else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}

function clearImport(){
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
}

function diskImport()
{ 
   if(fm2.FileName.value==null||fm2.FileName.value==""){
   	 alert("请选择要导入的文件！");
   	 return false;
   }
   var showStr="正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   fm2.submit();
}
function moduleDownload(){
	var tSQL="";
    var oldAction = fm.action;
    fm.action = "GrpYearBonusModule.jsp";
    fm.submit();
    fm.action = oldAction;
}
function queryImportResult(){
	var tbatchno=fm2.FileImportNo.value;
	var tqueryType=fm2.queryType.value;
	if(tbatchno==null||tbatchno==""){
		alert("请输入查询文件批次号！");
		return false;
	}
	if(tqueryType==null||tqueryType==""){
		alert("请选择查询类型！");
		return false;
	}
	strSQL = "select batchno,contid,errorinfo"
	+ " from lcgrpimportlog"
	+ " where batchno='"+tbatchno+"' ";
	if(tqueryType==1){
		strSQL+=" and errorstate='1'";
	}else if(tqueryType==2){
		strSQL+=" and errorstate='0'";
	}
	strSQL+=" order by contid";
  //执行查询并返回结果
	turnPage2.queryModal(strSQL, ImportResultGrid);
	}									

						





