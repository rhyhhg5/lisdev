//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;

//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在Absence.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	 
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
  {
  	alert("请输入员工编码！");
  	fm.all('AgentCode').focus();
  	return false;
  }
  
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
  {
    alert('请先查询出要修改的纪录！');
  }
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要修改的纪录！');
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
  	//alert(fm.all('hiddenAgentGroup').value);
  	//alert(fm.all('HiddenAgentGroup').value);
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./AbsenceQuery.jsp");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert('请先查询出要删除的纪录！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要删除的纪录！');
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function checkValid()
{
  //alert("111");
  var strSQL = "";
  var tReturn = parseManageComLimitlike();
  if (getWherePart('AgentCode')!='')
  {
     //alert("222");
     
     strSQL = "select * from LAAgent where 1=1 "
	     + getWherePart('AgentCode')+" and ((AgentState  not like '03') or (AgentState is null))"
	     + tReturn;
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);

  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value = "";
    fm.all('BranchType').value = "";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此员工！");
    fm.all('AgentCode').value="";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    fm.all('BranchType').value = "";
    //fm.all('AgentCode').focus();
    return false;
  }
  //alert('aa');
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  fm.all('ManageCom').value  = tArr[0][2];
  //<addcode>############################################################//
  fm.all('HiddenAgentGroup').value = tArr[0][1];
  //</addcode>############################################################//
  fm.all('Name').value = tArr[0][5];
  fm.all('BranchType').value = tArr[0][72];
  
  //存储原AgentGroup值，以备保存时使用  
  //<addcode>############################################################//
  old_AgentGroup=tArr[0][1];
  strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"' and (state<>'1' or state is null)"+tReturn;
  var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  //以备显示时使用
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
  //</addcode>############################################################//
}
/*
function agentConfirm()
{
  fm.all('AgentGroup').value='';
  fm.all('ManageCom').value='';	
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
  {
     alert("请输入员工编码！");
     fm.all('AgentCode').focus();
     return false;
  }
  fm.action = "AgentCodeQuery.jsp";	
  fm.submit();
  fm.action = "AbsenceSave.jsp";	
  //alert('change');
}*/

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('AgentCode').value = arrResult[0][0];  
		fm.all('AgentGroup').value = arrResult[0][18];
		fm.all('Name').value = arrResult[0][19];                                               
		fm.all('ManageCom').value = arrResult[0][2];                                              
		fm.all('Idx').value = arrResult[0][3];                         
                //fm.all('Times').value = arrResult[0][5];   
		//fm.all('AClass').value = arrResult[0][6];                                             
		fm.all('DoneDate').value = arrResult[0][7];                                           
		fm.all('SumMoney').value = arrResult[0][8];                                           
		fm.all('Noti').value = arrResult[0][9];                                           
		fm.all('Operator').value = arrResult[0][11]; 
		
	//<addcode>############################################################//
	    fm.all('HiddenAgentGroup').value = arrResult[0][1];
	    //alert(arrResult[0][12]);
	   
	//<addcode>############################################################//	                                                                                                                                                                                                                                                	
 	}
 	
}
//无用
function handler(key_event)
{
	//status = String.fromCharCode(key_event.keyCode);
	if (document.all)
	{
		if (key_event.keyCode==13)
		{
		    parent.fraInterface.fm.action = "AgentCodeQuery.jsp";	
		    if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
                    {
  	              alert("请输入员工代码！");
  	              return false;
                }
                
		    fm.submit();
		    parent.fraInterface.fm.action = "AbsenceSave.jsp";	
		    return true;
		}
	}
}


/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	//alert(cCodeName);
	try	{
		if( cCodeName == "AgentCode" )	
		{
			checkValid();//loadFlag在页面出始化的时候声明
		}
	}
	catch( ex ) {
	}
}