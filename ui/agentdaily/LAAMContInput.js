//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var tManageCom ="";
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.HManageCom.value=tManageCom;
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,ProtocalNo )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    fm.all('ProtocolNo').value=ProtocalNo;
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LACont.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
	//添加操作
	if( verifyInput() == false ) return false;

	var tAgentCom=fm.all('AgentCom').value;
	//alert(tAgentCom);
	if (tAgentCom==null || tAgentCom=='')
	{
		alert('请录入签署中介机构');
		return false;
	}
	
	
	var sql="select ACType,OtherSign from LACom a,lDCode where AgentCom='"+tAgentCom+"' and ACType=Code and codetype = 'actype'";
	var Result = easyQueryVer3(sql, 1, 0, 1);
	if (!Result)
	{
		alert("签署中介机构录入不正确!");
		return false;
	}
	
	var Result_Arr=decodeEasyQueryResult(Result);
	
	
	var sql = "select branchtype,branchtype2 from LACom where 1=1"
          + getWherePart('AgentCom','AgentCom');
    var strResult = easyQueryVer3(sql, 1, 1, 1);
    if(!strResult){
        alert("签署中介机构不正确!");
		return false;
    }
    else 
    {
  	    var arrDataSet = decodeEasyQueryResult(strResult);
  	    fm.all('BranchType').value = arrDataSet[0][0];
  	    fm.all('BranchType2').value = arrDataSet[0][1];
  	}
   
         return true;
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
  
  if (!beforeSubmit())
  {
  	return false;
  }
   var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	 if (!beforeSubmit())
  {
  	return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}                    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//Click事件，当点击“查询”图片时触发该函数
function QueryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LAAMContQuery.html");
}   
function easyQueryClick() {
	initContGrid();
	//此处书写SQL语句			     			    
	var strSql = "select ProtocolNo,SignDate,ManageCom,AgentCom,RepresentA,RepresentB,StartDate,EndDate from LACont where 1=1 "
    + getWherePart('ProtocolNo', 'ProtocolNo')
    + getWherePart('SignDate', 'SignDate')
    + getWherePart('ManageCom', 'ManageCom')
    + getWherePart('AgentCom', 'AgentCom');
    + getWherePart("RepresentA", "RepresentA")
    + getWherePart("RepresentB", "RepresentB")
    + getWherePart("StartDate", "StartDate")
    + getWherePart("EndDate", "EndDate")
     
  ;
	//turnPage.queryModal(strSql, ContGrid);
		turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);
 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
   //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = ArchieveGrid.getRowData(tRow-1);	
	return arrSelected;
}             
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		    if(arrResult[0][0]!="")
        	    
        fm.all('ProtocolNo').value= arrResult[0][0];
        if(arrResult[0][1]!="")
        fm.all('SignDate').value= arrResult[0][1];
        if(arrResult[0][2]!="")
        fm.all('ManageCom').value= arrResult[0][2];
        tManageCom = fm.all('ManageCom').value;
        fm.all('ManageCom').disabled = true;
        fm.all('HManageCom').value =tManageCom;
        var sql="select name from LdCom  where Comcode='"+arrResult[0][2]+"'";
		var Result = easyQueryVer3(sql, 1, 1, 1);
		fm.all('ManageComName').value= Result.substring(4,Result.length);
        if(arrResult[0][3]!="")
        fm.all('AgentCom').value=arrResult[0][3];
        sql="select name from LaCom  where agentcom='"+arrResult[0][3]+"'";
		Result = easyQueryVer3(sql, 1, 1, 1);
		fm.all('AgentComName').value= Result.substring(4,Result.length);
        if(arrResult[0][4]!="")
        fm.all('RepresentA').value= arrResult[0][4];
        if(arrResult[0][5]!="")
        fm.all('RepresentB').value= arrResult[0][5];
        if(arrResult[0][6]!="")
        fm.all('StartDate').value= arrResult[0][6];
        
        if(arrResult[0][7]!="")
        fm.all('EndDate').value= arrResult[0][7];
        if(arrResult[0][8]!="")
        {
        	
     //   fm.all('LAContType').value= arrResult[0][8];
         }
  
       return true;
	}
}             
function getAgentCom(cObj,cName)
{
if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
    alert("请先输入管理机构！");
}
else if (fm.all('ManageCom').value != null || trim(fm.all('ManageCom').value) != '') 
{
var strsql =" 1 and BranchType=#1# and BranchType2 in (#02#,#04#) and managecom like #" + fm.all('ManageCom').value + "%# " ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,1,1);
}

}


