 <%
//程序名称：LAAuthorizeInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">                                

function initForm()
{
  try
  {
    //initInpBox();
    //initSelBox(); 
    // fm.all('BranchType2').value = <%=BranchType2%>; 
    fm.all('BranchType').value = <%=BranchType%>;  
    
  //  alert( fm.all('BranchType2').value);
    
    initAuthorizeGrid();    
  }
  catch(re)
  {
    alert("LAAuthorizeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 险种授权的初始化
function initAuthorizeGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";          		//列名
      iArray[1][1]="50px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";
      iArray[1][9]="险种编码|code:RiskCode";              	        //校验输入是否正确


      iArray[2]=new Array();
      iArray[2][0]="险种名称";         			//列名
      iArray[2][1]="160px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0; 
               	        //校验输入是否正确
          			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="取消销售资格起期(YYYY-MM-DD)";      	   		//列名
      iArray[3][1]="120px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=1; 
      iArray[3][4]="Date";     //引用代码:
      iArray[3][9]="取消销售资格起期|Date"; 
             			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[4]=new Array();
      iArray[4][0]="取消销售资格止期(YYYY-MM-DD)";      	   		//列名
      iArray[4][1]="120px";            			//列宽
      iArray[4][2]=10;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
     iArray[3][4]="Date";     //引用代码:
      iArray[4][9]="取消销售资格止期|Date"; 
      AuthorizeGrid = new MulLineEnter( "fm" , "AuthorizeGrid" ); 
      //这些属性必须在loadMulLine前
      AuthorizeGrid.mulLineCount = 1;   
      AuthorizeGrid.displayTitle = 1;
      AuthorizeGrid.hiddenPlus = 0;
      AuthorizeGrid.hiddenSubtraction = 0;
      AuthorizeGrid.locked=0;      
      AuthorizeGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>
