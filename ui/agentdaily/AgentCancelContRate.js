//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
	if(!checkValue()) return false;
  fm.target="f1print";
 	fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AgentCancelContRate.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
  
}

function afterCodeSelect(codeName,Field)
{
	
}

function beforeSubmit()
{
   return true;
}

function checkImportNumber(control){
	var reg=/^(\d*)$/;
	if(!reg.test(control)){
			return false;
	}
	return true;
}

function checkValue(){
	if(!checkImportNumber(fm.all("WageNo").value)){
			alert('所属年月请输入数字');
			return false;
	}
	var  sql="select enddate from lastatsegment where stattype='2' and yearmonth="+fm.all("WageNo").value;
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
	var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
	if( tArr == null ){
		alert('所属年月请按季度输入,例如:200803、200806、200809');
		return false;
	}
	return true 
}
