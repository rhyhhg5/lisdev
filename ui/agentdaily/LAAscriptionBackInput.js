 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
 
	if(!verifyInput())
 {
 	return false;
 }	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	fm.reset;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
    if (fm.action == './LAAscriptionBackSave.jsp')     
      saveClick=true;
  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  
} 

//取消按钮对应操作
function cancelForm()
{

}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  
	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证字段的值
function checkagentcode()
{
 var tReturn = parseManageComLimitlike();
 strSQL = "select * from LAAscription where  Ascripstate='3'  "
          +tReturn
	  + getWherePart('ManageCom')
	  + getWherePart('Acsriptiondate','AcsriptionDate')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');   
  if(fm.all('ContNo').value!=null){
  	  strSQL+=getWherePart('ContNo');
  }
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  
  //判断是否查询成功
  if (!strQueryResult) 
  {
    alert("没有此代理人！");
    fm.all('ManageCom').value="";
    fm.all('ManageComName').value="";
    fm.all('AscriptionDate').value ="";
    fm.all('ContNo').value="";
    return ;
  } 
  
  var ttArr = new Array();
  ttArr = decodeEasyQueryResult(strQueryResult);
   
}
function agentConfirm()
{
	
	if(!verifyInput())
 {
 	return false;
 }		

  var strSQL = "";
  var tAscriptionDate = fm.all('AcsriptionDate').value;
  var tManageCom=fm.all('ManageCom').value;
  var tContNo=fm.all('ContNo').value;
  initAscriptionGrid();   
  var tReturn = parseManageComLimitlike();
  strSQL="select distinct a.AscripNo,c.managecom,a.ContNo,a.AgentOld,c.name,a.AgentNew,(select name from laagent dd where dd.agentcode=a.agentnew)    from laascription a,laagent c where c.agentcode=a.agentold and  a.AscripState='3' and a.validflag='N' "
      //+ tReturn
	  + getWherePart('c.ManageCom','ManageCom')
	  + getWherePart('a.Ascriptiondate','AcsriptionDate')
	  ;	

	if (tContNo!=null){
		strSQL += getWherePart('a.contno','ContNo');
	}    

  turnPage.queryModal(strSQL, AscriptionGrid); 
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败");
    return ;
    }
  

}   
function submitSave()
{  
   var lineCount = AscriptionGrid.mulLineCount;
   var str='';
   var tManageCom = fm.all('ManageCom').value;
   var tAscriptionDate=fm.all('AcsriptionDate').value;
   var tContNo=fm.all('ContNo').value;
      //fm.action="./LAAscriptionBackSave.jsp";
      submitForm();
     return ;
}
function submitSaveb()
{
	fm.all("Flag").value="SELECT";
        tSel=false;
	var lineCount = AscriptionGrid.mulLineCount;
	for( i=0;i<lineCount;i++)
	{
	 tSel = AscriptionGrid.getChkNo(i);
	 if(tSel==true)
	 i=lineCount;
        }
	if( tSel == false || tSel == null )
		alert( "请先选择一条记录!" );
	else
	{
				
	submitSave();
	}
}
function submitSaveall()
{
	fm.all("Flag").value="ALL";

	submitSave();
}

function clearMulLine()
{  
   AscriptionGrid.clearData("AscriptionGrid");
   saveClick=false;
}