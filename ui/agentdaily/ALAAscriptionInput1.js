 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var mOperate="";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if(!beforeSubmit())
   {
 	return false;
 	}
 	fm.all("hideOperate").value='INSERT||MAIN';
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
   initForm();
} 
 
 function checkPrename()
 {
  
 	var sql="select (select b.branchattr from labranchgroup b where b.agentgroup=a.agentgroup),a.name,a.agentgroup,a.managecom from laagent a where 1=1 "
 	 + getWherePart("a.AgentCode","AgentNew") 
 	;
     var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
     if (!strQueryResult)
  {
    alert("没有此代理人！");
    fm.all('AgentOld').value='';	
    fm.all('BranchAttr').value = '';
    fm.all('Prename').value  = '';
    fm.all('AgentGroup').value  = '';
    fm.all('ManageCom').value = '';
    return;
  }
   //查询成功则拆分字符串，返回二维数组
   
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  fm.all('BranchAttr').value  = tArr[0][0];
  fm.all('PreName').value = tArr[0][1];
  fm.all('AgentGroup').value = tArr[0][2];
  fm.all('ManageCom').value=tArr[0][3];
  showOneCodeNametoAfter('comcode','ManageCom','ManageComName');
 
  //存储原AgentGroup值，以备保存时使用  
  //<addcode>############################################################//
 
 	}
 	
function 	checkname()
{
	
	var  sql = "select a.name,a.managecom,(select name from ldcom where comcode=a.managecom),(select name from labranchgroup where agentgroup=a.agentgroup) from laagent a where 1=1 "
	    + getWherePart('a.agentcode','AgentNew') + " and Agentstate<'03'"
	    ;
	    
	var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
     if (!strQueryResult)
  {
    alert("没有此代理人！");
    fm.all('NameNew').value='';
    fm.all('AgentNew').value = '';
    return false;
  }  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);　
  fm.all('NameNew').value  = tArr[0][0];
  fm.all('ManageCom').value = tArr[0][1];
  fm.all('ManageComName').value = tArr[0][2];
  fm.all('BranchAttr').value = tArr[0][3];
  
 }
	
function	checkCount()
{
	var sql="select contno,agentcode from lccont where 1=1 "
	       + getWherePart('ContNo','ContNo')
	      + getWherePart('AgentCode','AgentOld') ;
   var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("此保单不存在！");
    fm.all('ContNo').value='';
    return false;
   }
   //查询保单所属的业务员
   else
   {
     var ttArr = new Array();
     ttArr = decodeEasyQueryResult(strQueryResult);
     var AgentCode = ttArr[0][1];
     var tsql = "select a.name,a.managecom,(select name from ldcom where comcode=a.managecom),"
      +" (select name from labranchgroup where agentgroup=a.branchcode),agentgroup,"
      + " (select branchattr from labranchgroup where agentgroup=a.agentgroup) from laagent a where agentcode='"+AgentCode+"'";
     var strQueryResult = easyQueryVer3(tsql, 1, 1, 1);
     if(!strQueryResult)
     {
      alert("保单所属的代理人不存在！");
      fm.all('ContNo').value='';
      return false;
     }
     else
     {
       var tttArr1 = new Array();
       tttArr1 = decodeEasyQueryResult(strQueryResult);
       fm.all('AgentOld').value = AgentCode;
       fm.all('PreName').value = tttArr1[0][0];
       fm.all('ManageComOld').value = tttArr1[0][1];
       fm.all('ManageComNameOld').value = tttArr1[0][2];
       fm.all('BranchAttrOld').value = tttArr1[0][3];
       fm.all('AgentGroup').value = tttArr1[0][4];
       fm.all('BranchAttrOld1').value = tttArr1[0][5];
     }
   }
   
}
 	
//提交前的校验、计算  
function beforeSubmit()
{
  if ((fm.all('AgentNew').value == '')||(fm.all('AgentNew').value == null))
  {
  	alert("请输入新代理人编码！");
  	fm.all('AgentNew').focus();
  	return false;
  } 
  //新代理人不能与原代理人相同
  if((fm.all('AgentNew').value==fm.all('AgentOld').value))
  {
    alert("原代理人与新代理人相同！");
    return false;
    fm.all('AgentNew').vlaue = '';
  }
  //不能跨机构进行调整
  if((fm.all('ManageComOld').value!=fm.all('ManageCom').value))
  {
   alert("不能跨机构进行业务员的变更！");
   return false;
   fm.all('AgentNew').vlaue = '';
  }
  //提示
  if(!confirm("确定将保单'"+fm.all('ContNo').value+"'转到业务员'"+fm.all('AgentNew').value+"'名下?"))
  {
    alert("你取消了操作!");
    resetForm();
    return false;
  }
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码 
   if(fm.all('AgentOld').value ==fm.all('AgentNew').value)
  {
  	  	alert("原代理人不能与新代理人相同!!");
  	  	return false;
  }
  mOperate="INSERT||MAIN";
  submitForm();
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




