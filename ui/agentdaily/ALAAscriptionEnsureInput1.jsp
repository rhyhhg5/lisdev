<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAAscriptionInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String MakeType=request.getParameter("MakeType");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAManualASInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAManualASInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAManualASSave.jsp" method=post name=fm target="fraSubmit">  
     <table>
    <tr class=common with = 100%>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
    <td class=titleImg>
      原销售人员信息
    </td>
    </td> 
    </tr>
    </table>
  <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common>
       <TR  class= common> 
          <TD  class= title>
            原销售人员代码
          </TD>          
          <TD  class= input>
            <Input class=common name=AgentCode onchange="return checkagentcode();">
          </TD>  
          <td  class= title>
		   销售人员姓名
		</td>
        <td  class= input>
		  <input name=Name class= "readOnly" readOnly onchange="return checkagentname();" >
		</td> 
		 </TR>  
		<TR  class= common>      
  <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|notnull&code:comcode&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
          ><Input class="codename" name=ManageComName   readOnly elementtype=nacessary> 
          </TD> 
       <td class=title>个单号 </td>
       <td class=input>
       <input class=common name=ContNo></td>
       </TR>
       	<TR  class= common>      
  
       <td class=title>团单号 </td>
       <td class=input>
       <input class=common name=GrpContNo></td>
       </TR>
       <td>
      <input type=button class=cssbutton value='查  询' onclick="return agentConfirm()">
       <!--<Input type=button class= common value="确认1" OnClick="return agentConfirm1111();">确定按钮 -->  
     </td>
    </table>  
     <p> <font color="#ff0000">注：只变更归属日期之后的保单业绩和提佣。涉及到跨成本中心调整的数据反冲，归属月为操作日所在月份，请尽量当月处理归属数据。 </font></p> 
     <p> <font color="#ff0000">注：全部确认针对一个人下的所有保单进行确认，而不针对某一个保单进行确认  </font></p> 
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription2);">
    <td class=titleImg>
      保单归属信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divLAAscription2" style= "display: ''">
    <table  class= common>
            <tr  class= common>
                    
        <td text-align: left colSpan=1> 
		  <span id="spanAscriptionGrid" >
		  </span> 
        </td>
  			</tr>
    	</table>
    	<Div id="divPage" style= "display: ''">
      <INPUT class=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">	
      </Div>
      <table>
      <!--TD  class= title>
         现销售人员编码
      </TD>          
      <TD  class= input>
          <Input class=common name=NewAgentCode >
      </TD-->        
    </table>    
        <input type=button class=cssbutton name=saveb value='选择确认' onclick="submitSaveb();"> 
        <input type=button class=cssbutton name=saveall value='全部确认' onclick="submitSaveall();">          
 
    </div>   
   </div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=''>
     <input type=hidden name=BranchType2 value=''>
     <input type=hidden name=Flag value=''>
     <input type=hidden name=MakeType value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
