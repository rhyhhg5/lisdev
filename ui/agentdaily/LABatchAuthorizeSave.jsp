<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABatchAuthorizeSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
  LAAgentSet tLAAgentSet = new LAAgentSet();
  LABatchAuthorizeUI tLABatchAuthorizeUI = new LABatchAuthorizeUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	System.out.println("Save页面处理开始...");
  // 获取画面上取得的数据
  String strAgent = request.getParameter("listAgentCode");
  System.out.println(strAgent);
  String arrAgent[] = strAgent.split(",");
  System.out.println("业务员个数："+arrAgent.length);
  // 循环处理准备数据
  for(int i=0;i<arrAgent.length;i++)
  {// 增加被授权人员代码
    LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    tLAAgentSchema.setAgentCode(arrAgent[i]);
    tLAAgentSet.add(tLAAgentSchema);
  }
  
  // 授权险种信息
  String arrCount[] = request.getParameterValues("AuthorizeGridNo");
  String tRiskCode[] = request.getParameterValues("AuthorizeGrid1");
  String tRiskTypeName[] = request.getParameterValues("AuthorizeGrid2");
  String tAuthorStartDate[] = request.getParameterValues("AuthorizeGrid3");
  String tAuthorEndDate[] = request.getParameterValues("AuthorizeGrid4");
  
  // 循环处理授权险种
  for(int j=0;j<arrCount.length;j++)
  {
    LAAuthorizeSchema tLAAuthorizeSchema = new LAAuthorizeSchema();
    tLAAuthorizeSchema.setRiskCode(tRiskCode[j]);
    tLAAuthorizeSchema.setAuthorStartDate(tAuthorStartDate[j]);
    tLAAuthorizeSchema.setAuthorEndDate(tAuthorEndDate[j]);
    tLAAuthorizeSet.add(tLAAuthorizeSchema);
  }
	
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(request.getParameter("AgentCode").trim());
	tVData.add(request.getParameter("AgentName").trim());
	tVData.add(request.getParameter("ManageCom").trim());
	tVData.add(request.getParameter("BranchAttr").trim());
	tVData.add(request.getParameter("AgentGrade").trim());
	tVData.add(request.getParameter("EnterYears").trim());
	tVData.add(request.getParameter("checkType").trim());
	tVData.add(request.getParameter("BranchType").trim());
	tVData.add(request.getParameter("BranchType2").trim());
	tVData.add("");//取消时 需要riskcode,后台要的到(String) pmInputData.getObject(9)
	tVData.add(tLAAgentSet);
	tVData.add(tLAAuthorizeSet);
	tVData.add(tG);
  System.out.println("add over");
  
  try
  {
    tLABatchAuthorizeUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABatchAuthorizeUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

