<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAddSubSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LARewardPunishSchema tLARewardPunishSchema   = new LARewardPunishSchema();
  LARewardPunishGrpUI tLARewardPunishGrpUI   = new LARewardPunishGrpUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
    String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();	

  tG=(GlobalInput)session.getValue("GI");
  String mDoneDate = request.getParameter("DoneDate");
  String mManageCom  = request.getParameter("ManageCom");

  tLARewardPunishSchema.setAgentCode(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("AgentCode")+"'"));
  tLARewardPunishSchema.setIdx(request.getParameter("Idx"));
  tLARewardPunishSchema.setBranchAttr(request.getParameter("AgentGroup"));
  tLARewardPunishSchema.setAgentGroup(request.getParameter("HiddenAgentGroup"));
  tLARewardPunishSchema.setBranchType(request.getParameter("BranchType"));
  tLARewardPunishSchema.setBranchType2(request.getParameter("BranchType2"));
  tLARewardPunishSchema.setManageCom(request.getParameter("ManageCom"));    
  tLARewardPunishSchema.setWageNo(request.getParameter("WageNo"));
  tLARewardPunishSchema.setLateCount(request.getParameter("LateCount"));    
  tLARewardPunishSchema.setSickCount(request.getParameter("SickCount"));
  tLARewardPunishSchema.setAffairCount(request.getParameter("AffairCount"));
  tLARewardPunishSchema.setAbsentCount(request.getParameter("AbsentCount"));
  tLARewardPunishSchema.setDoneFlag("3");    
  tLARewardPunishSchema.setNoti(request.getParameter("Noti"));
  tLARewardPunishSchema.setOperator(tG.Operator);    
    
  System.out.println(request.getParameter("AgentCode"));
  System.out.println(request.getParameter("AgentGroup"));
  System.out.println(request.getParameter("HiddenAgentGroup"));
  System.out.println(request.getParameter("BranchType"));
  System.out.println(request.getParameter("ManageCom"));
  System.out.println(request.getParameter("BranchType2"));
  System.out.println(request.getParameter("WageNo"));
  System.out.println(request.getParameter("Idx")); 
  System.out.println(tG.Operator);
  System.out.println(request.getParameter("LateCount")); 


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.add(mDoneDate);
	tVData.add(mManageCom);
	tVData.addElement(tLARewardPunishSchema);
	
 try
 {
   tLARewardPunishGrpUI.submitData(tVData,tOperate);
 }
 catch(Exception ex)
 {
   Content = "保存失败，原因是:" + ex.toString();
   FlagStr = "Fail";
 }
 
 if (!FlagStr.equals("Fail"))
 {
   tError = tLARewardPunishGrpUI.mErrors;
   if (!tError.needDealError())
   {                          
   	Content = " 保存成功! ";
   	FlagStr = "Succ";
   }
   else                                                                           
   {
   	Content = " 保存失败，原因是:" + tError.getFirstError();
   	FlagStr = "Fail";
   }
 }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
