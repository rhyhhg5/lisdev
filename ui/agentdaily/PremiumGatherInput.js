var showInfo;
var mDebug ="0"

var turnPage = new turnPageClass();
var len =manageCom.length;
/************************************************
 * 功能描述：点击查询按钮触发的事件
 * 传入参数：无
 * 返 回 值：无
 ************************************************/
function queryClick(){
	
	if(!verifyInput2()){
		return false;
	}
	
	var tManageCom = fm.all('ManageCom').value;
	var tAgentCom = fm.all('AgentCom').value;
	var tStartDate = fm.all('StartDate').value;
	var tEndDate = fm.all('EndDate').value;
	var tAppntName = fm.all('AppntName').value;
	
	if(tAgentCom!=null && tAgentCom !=""){
	var strSQL = "select a.managecom ,(select name from ldcom where comcode =a.managecom),a.agentcom ,(select name from lacom where agentcom =a.agentcom),"+
	"c.p11,sum(a.transmoney),sum(b.sumgetmoney),sum((case when b.confdate is not null then b.SumGetMoney else 0 end )) cc from lacharge a ,ljaget b,lacommision c where a.receiptno =b.otherno and a.commisionsn =c.commisionsn and a.branchtype='2' and a.branchtype2='02' "+
    "and a.tmakedate >= '"+tStartDate+"' and a.tmakedate <= '"+tEndDate+"' and c.p11 like'%%"+tAppntName+"%%' and a.agentcom ='"+tAgentCom+"' and a.managecom like '"+tManageCom+"%'  "+
    " group by a.manageCom,a.agentCom,c.p11";
	}else if(tManageCom!=null && tManageCom !=""){
		var strSQL = "select a.managecom ,(select name from ldcom where comcode =a.managecom),'' ,'',"+
		"c.p11,sum(a.transmoney),sum(b.sumgetmoney),sum((case when b.confdate is not null then b.SumGetMoney else 0 end )) cc from lacharge a ,ljaget b,lacommision c where a.receiptno =b.otherno and a.commisionsn = c.commisionsn and a.branchtype='2' and a.branchtype2='02' "+
	    "and a.tmakedate >= '"+tStartDate+"' and a.tmakedate <= '"+tEndDate+"' and c.p11 like'%%"+tAppntName+"%%' and a.managecom like '"+tManageCom+"%'  "+
	    "group by a.managecom,c.p11";
	}else{
		var strSQL ="select a.managecom ,(select name from ldcom where comcode =a.managecom),'' ,'',"+
		"c.p11,sum(a.transmoney),sum(b.sumgetmoney),sum((case when b.confdate is not null then b.SumGetMoney else 0 end )) cc from lacharge a ,ljaget b,lacommision c  where a.receiptno =b.otherno and a.commisionsn =c.commisionsn  and a.branchtype='2' and a.branchtype2='02' "+
		" and a.tmakedate >='"+tStartDate+"' and a.tmakedate <='"+tEndDate+"' and c.p11 like'%%"+tAppntName+"%%' and a.managecom like '86%' "+
		"group by a.managecom ,c.p11";
	}
   
	//执行查询并返回结果
turnPage.queryModal(strSQL, LACommGrid);
}


function DoReset(){
	try
	{   
		initForm();
	}
	catch(re)
	{
		alert("初始化页面错误，重置出错");
	}
}

//双击得到agentcom
function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#2# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}