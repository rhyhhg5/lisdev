<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAHolsEndInput.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAHolsInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();" >
  <form action="./LAHolsSave.jsp" method=post name=fm target="fraSubmit">
    <%--@include file="../common/jsp/OperateButton.jsp"--%>
    <%--@include file="../common/jsp/InputButton.jsp"--%>
   <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAHolsEnd1);">
     <td class=titleImg>
      <%=tTitleAgent%>销假信息
     </td>
    </td>
    </table>
    <INPUT class = cssbutton VALUE="查  询" TYPE=button onclick="queryClick();"> 	
    <INPUT class = cssbutton VALUE="销假确认" TYPE=button onclick="updateClick();"> 
  <Div  id= "divLAHolsEnd1" style= "display: ''"> 
    
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  <%=tTitleAgent%>代码
		</td>
        <td  class= input> 
		  <input class=common  name=GroupAgentCode onchange="return checkValid();"> 
		</td> 
          <td  class= title>
		   <%=tTitleAgent%>姓名
		</td>
        <td  class= input>
		  <input name=Name class='readonly' readonly >
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  销售单位 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=AgentGroup > 
		</td>
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ManageCom > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  请假类型
		</td>
        <td  class= input> 
		  <input name=AClass class="readonly" readonly maxlength=2 ondblclick="return showCodeList('holsaclass',[this]);" onkeyup="return showCodeListKey('holsaclass',[this]);" > 
		</td>
        <td  class= title> 
		  假期天数
		</td>
        <td  class= input> 
		  <input name=VacDays class="readonly" readonly > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  请假日期 
		</td>
        <td  class= input> 
		  <input name=LeaveDate class="readonly" readonly > 
		</td>
        <td  class= common> 
		  销假日期
		</td>
        <td  class= input> 
		  <input name=EndDate class='coolDatePicker'  dateFormat='short' > 
		</td>
      </tr>
      <tr  class= common>
        <td  class= title>
		  请假单填写标志
		</td>
        <td  class= input>
		  <input name=FillFlag class="readonly" readonly maxlength=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
		</td>
        <td  class= title>
		  诊断证明书标志
		</td>
        <td  class= input>
		  <input name=ConfIdenFlag class="readonly" readonly maxlength=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title>
		   产假延长标志
		</td>
        <td  class= input>
		  <input name=AddVacFlag class="readonly" readonly maxlength=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
		</td>
        <td  class= title> 
		 		应销假日期
		</td>
        <td  class= input> 
		  <input name=ShouldEndDate class="readonly" readonly  > 
		</td>
      </tr>
      <tr  class= common>
        <!--td  class= title>
		  金额
		</td>
        <td  class= input>
		  <input name=SumMoney class="readonly" readonly >
		</td-->
        <td  class= title>
		  备注
		</td>
        <td  class= input>
		  <input name=Noti class="readonly" readonly id="Noti" >
		</td>
    <!--td  class= title> 
		  纪录顺序号 
		</td>
    <td class= input> 
		  <input class="readonly" readonly name=Idx > 
		</td-->
      </tr>
      <tr  class= common>
		  <td  class= title> 
		核准人
		</td>
        <td  class= input> 
		  <input name=ApproveCode class="readonly" readonly > 
		</td>
    <td  class= title>
		  累计请假天数
		</td>
    <td  class= input>
		  <input name=SumDays class="readonly" readonly >
		</td>
  </tr>
  <TR class=common>
    <td  class= title>
		  操作员代码
		</td>
    <td  class= input>
		  <input name=Operator class="readonly" readonly >
		</td>
    <td  class= title>
		  最近操作日
		</td>
    <td  class= input>
		  <input name=ModifyDate class="readonly" readonly >
		</td>
  </TR>
  </table>
  </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=LeaveState value='1'>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=Idx value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=AgentCode value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
