<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AbsenceInput.jsp
//程序功能：
//创建日期：2003-7-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bdyz.*"%>
  <%@page import="com.sinosoft.lis.taskservice.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  DealBdyzDataOfAllIDNo tDealBdyzDataOfAllIDNo = new DealBdyzDataOfAllIDNo();
	BDYZExtractPersonTask tBDYZExtractPersonTask = new BDYZExtractPersonTask();
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("operator");
  String tYesterDay = request.getParameter("YesterDay");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

   
  try
  {
	  if("all".equals(tOperate))
	  {
		  tDealBdyzDataOfAllIDNo.update(tYesterDay);
	  }
	  else if("task".equals(tOperate))
	  {
		  tBDYZExtractPersonTask.mYesterDay = tYesterDay;
		  tBDYZExtractPersonTask.run();
	  }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

