var turnPage = new turnPageClass();

/**************************************
 * 在MulLine某条数据被选中时触发
 * 参数：  无
 * 返回：  无
 **************************************/
function getOnePlan()
{
	var tGrideRow = PlanGrid.getSelNo();
	var tPlanObject = PlanGrid.getRowColData(tGrideRow-1,1);
	var tPlanPeriodUnit = PlanGrid.getRowColData(tGrideRow-1,4);
	var tPlanStartYM = PlanGrid.getRowColData(tGrideRow-1,5);
	//alert("2/"+tPlanObject+"/"+tPlanPeriodUnit+"/"+tPlanStartYM);
	var tTimeType = "";
	if("1" == tPlanPeriodUnit)
	{
		tTimeType = "月计划";
	}
	if("3" == tPlanPeriodUnit)
	{
		tTimeType = "季计划";
	}
	if("12" == tPlanPeriodUnit)
	{
		tTimeType = "年计划";
	}
	
	var tSQL = "select PlanValue,PlanValue2,PlanMark,PlanItemType from LAPlanUnit where PlanType = '0'";
	tSQL += " and PlanObject = '"+tPlanObject+"' and PlanPeriodUnit = '"+tPlanPeriodUnit + "'";
	tSQL += " and PlanStartYM = '"+tPlanStartYM+"' and PlanItemType in ('01','02','03','04','05','06')";
	tSQL += " Order By PlanItemType";
	
	var arrValue = getArrValueBySQL(tSQL);
	
	if(null == arrValue || arrValue.length != 6)
	{
		alert("检索分公司["+tPlanObject+"]的["+tPlanStartYM+"]年月的 "+tTimeType+" 信息失败！");
		return false;
	}
	
	document.fm.ComCode.value = tPlanObject;
	document.fm.ComCodeName.value = PlanGrid.getRowColData(tGrideRow-1,2);
	document.fm.ManageCom.value = PlanGrid.getRowColData(tGrideRow-1,3);
	document.fm.PlanPeriodUnit.value = PlanGrid.getRowColData(tGrideRow-1,4);
	document.fm.DateStartYM.value = tPlanStartYM;
	
	// 设置计划内容
  document.fm.Money.value = arrValue[0][0];
  document.fm.TransMoney.value = arrValue[1][0];
  document.fm.AgentCont.value = arrValue[2][0];
  document.fm.AppCont.value = arrValue[3][0];
  document.fm.ActRate.value = arrValue[4][0];
  document.fm.YearKeepRate.value = arrValue[5][0];
  document.fm.Other.value = arrValue[0][1];
  document.fm.Mark.value = arrValue[0][2];
  
  // 查询出来的数据不允许更改计划时间
  document.fm.PlanPeriodUnit.disabled = true;
  document.fm.DateStartYM.disabled = true;
}

/**************************************
 * 点击"查询已有的计划"按钮时的触发事件
 * 参数：  无
 * 返回：  无
 **************************************/
function onQueryClick()
{
	var tSQL = "";
	
	tSQL  = "select a.PlanObject,b.Name,a.ManageCom,a.PlanPeriodUnit,a.PlanStartYM,'',";
	tSQL += "  (select Sum(aa.PlanValue) from LAPlanUnit aa where aa.PlanType = a.PlanType and aa.PlanObject = a.PlanObject and aa.PlanPeriodUnit = a.PlanPeriodUnit and aa.PlanStartYM = a.PlanStartYM and aa.PlanItemType = '01'),";
	tSQL += "  (select Sum(aa.PlanValue) from LAPlanUnit aa where aa.PlanType = a.PlanType and aa.PlanObject = a.PlanObject and aa.PlanPeriodUnit = a.PlanPeriodUnit and aa.PlanStartYM = a.PlanStartYM and aa.PlanItemType = '02'),";
	tSQL += "  (select Sum(aa.PlanValue) from LAPlanUnit aa where aa.PlanType = a.PlanType and aa.PlanObject = a.PlanObject and aa.PlanPeriodUnit = a.PlanPeriodUnit and aa.PlanStartYM = a.PlanStartYM and aa.PlanItemType = '03'),";
	tSQL += "  (select Sum(aa.PlanValue) from LAPlanUnit aa where aa.PlanType = a.PlanType and aa.PlanObject = a.PlanObject and aa.PlanPeriodUnit = a.PlanPeriodUnit and aa.PlanStartYM = a.PlanStartYM and aa.PlanItemType = '04'),";
	tSQL += "  (select Sum(aa.PlanValue) from LAPlanUnit aa where aa.PlanType = a.PlanType and aa.PlanObject = a.PlanObject and aa.PlanPeriodUnit = a.PlanPeriodUnit and aa.PlanStartYM = a.PlanStartYM and aa.PlanItemType = '05'),";
	tSQL += "  (select Sum(aa.PlanValue) from LAPlanUnit aa where aa.PlanType = a.PlanType and aa.PlanObject = a.PlanObject and aa.PlanPeriodUnit = a.PlanPeriodUnit and aa.PlanStartYM = a.PlanStartYM and aa.PlanItemType = '06')";
	tSQL += "  ,a.Operator,a.ModifyDate";
	tSQL += "  from LAPlanUnit a,LDCom b";
	tSQL += " where a.PlanType = '0'";
	tSQL += "  and a.PlanObject = b.ComCode";
	tSQL += getWherePart('a.PlanObject','ComCode','like');
	tSQL += getWherePart('a.PlanPeriodUnit','PlanPeriodUnit');
	tSQL += getWherePart('a.ManageCom','ManageCom');
	tSQL += " group by a.PlanObject,a.PlanType,b.Name,a.ManageCom,a.PlanPeriodUnit,a.PlanStartYM,a.Operator,a.ModifyDate";
	tSQL += " order by a.PlanStartYM DESC,PlanObject";
	
	turnPage.queryModal(tSQL, PlanGrid);
	
}

/**************************************
 * 点击"保存"按钮时的触发事件
 * 参数：  无
 * 返回：  无
 **************************************/
function saveClick()
{
	// 进行必录项验证
	if(!verifyInput())
	{
		return false;
	}
	// 进行画面验证
	if(!beforeSubmit())
	{
		return false;
	}
	
	document.fm.Operate.value = "INSERT||MAIN";
	
	// 验证通过  画面提交
	submitForm();
	
	return true;
}

/**************************************
 * 点击"修改"按钮时的触发事件
 * 参数：  无
 * 返回：  无
 **************************************/
function updateClick()
{
	// 进行必录项验证
	if(!verifyInput())
	{
		return false;
	}
	// 进行画面验证
	if(!beforeSubmit())
	{
		return false;
	}
	
	document.fm.Operate.value = "UPDATE||MAIN";
	
	// 验证通过  画面提交
	submitForm();
	
	return true;
}

/**************************************
 * 起脚画面前进行的画面验证
 * 参数：  无
 * 返回：  无
 **************************************/
function beforeSubmit()
{
	// 验证计划年月是否是合法年月
	var tYM = trim(document.fm.DateStartYM.value);
	var tYear = tYM.substring(0,4);
	var tMonth = tYM.substring(4,6);
	var tCheckDate = tYear + "-" + tMonth + "-01";
	//alert(tCheckDate);
	if(!isDate(tCheckDate))
	{
		alert("请在[计划起期年月]里输入一个合法年月！");
	}
	// 对计划起期年月有效性验证  
	var tDate = new Date();
	//alert(tDate.getYear());
	var tPlanPeriodUnit = trim(document.fm.PlanPeriodUnit.value);
	if("1" == tPlanPeriodUnit)
	{// 月计划
	}
	if("3" == tPlanPeriodUnit)
	{// 季计划
		if("01" != tMonth && "04" != tMonth && "07" != tMonth && "10" != tMonth)
		{
			alert("季计划时[计划起期年月]月份部分要录入'01','04','07','10'中的一个！如'200607'");
			return false;
		}
	}
	if("12" == tPlanPeriodUnit)
	{// 年计划
		if("01" != tMonth)
		{
			alert("年计划时[计划起期年月]月份部分要录入是'01'！如'200601'");
			return false;
		}
	}
	// 验证备注长度是否太长
	var tMarkValue = "";
	tMarkValue = document.fm.Mark.value;
	if(!strLen(tMarkValue,300,"[备注]内容太长！请录入100个汉字或300个字母或数字！"))
	  return false;
	
	return true;
}

/**************************************
 * "重置"事件
 * 参数：  无
 * 返回：  无
 **************************************/
function resetClick()
{
	// 清空画面
	initForm();
	
  document.fm.PlanPeriodUnit.disabled = false;
  document.fm.DateStartYM.disabled = false;
  
	return true;
}

/**************************************
 * 提交画面
 * 参数：  无
 * 返回：  无
 **************************************/
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}

/**************************************
 * 提交后操作,服务器数据返回后执行的操作
 * 参数：  无
 * 返回：  无
 **************************************/
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    initForm();
  }
}

/***************************************
 * 根据参数查询结果
 * 参数：pmSQL  查询用的SQL文
 * 返回：查询结果
 ***************************************/
function getArrValueBySQL(pmSQL)
{
	//alert(pmSQL);
	var strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
	
	if (!strQueryResult)
		return null;
		
	var arr = decodeEasyQueryResult(strQueryResult);
	
	return arr;
}

/***************************************
 * 验证字符的字节长度是否超标
 * 参数：pmStr      准备验证的字符串
 *       pmStrLen   长度标准
 *       pmMissage  报错信息
 * 返回：返回字符串字节长度
 ***************************************/
function strLen(pmStr,pmStrLen,pmMissage)
{
	var i;
  var len;
  len = 0;
  for (i=0;i<pmStr.length;i++)
  {
    if (pmStr.charCodeAt(i)>255) len+=3; else len++;
  }
  //alert("[" + pmStr + "] 长度为： " + len + " 规定长度为："+pmStrLen);
  if(len > pmStrLen)
  {
  	alert(pmMissage);
  	return false;
  }
  
  return true;
}