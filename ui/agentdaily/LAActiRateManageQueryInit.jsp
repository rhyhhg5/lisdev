<%
//程序名称：LAActiRateManageQueryInit.jsp
//程序功能：
//创建日期：2008-02-03 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    
    fm.all('Score').value = '';
   // fm.all('AscClass').value = '';
    
    fm.all('DoneDate').value = '';
    
    
    fm.all('BranchType').value = top.opener.fm.all('BranchType').value;
  }
  catch(ex)
  {
    alert("在LAActiRateManageQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                                

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();   
    initActiRateManageGrid();
  }
  catch(re)
  {
    alert("LAActiRateManageQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化ActiRateManageGrid
 ************************************************************
 */
function initActiRateManageGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="业务员代码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="业务员姓名";         //列名
        iArray[2][1]="140px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        
        iArray[3]=new Array();
        iArray[3][0]="销售单位";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="50px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
       /* iArray[5]=new Array();
        iArray[5][0]="分数类型";         //列名
        iArray[5][1]="150px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许*/
        
        iArray[5]=new Array();
        iArray[5][0]="分数";         //列名
        iArray[5][1]="150px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="执行日期";         //列名
        iArray[6][1]="70px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        
        iArray[7]=new Array();
        iArray[7][0]="记录顺序号";         //列名
        iArray[7][1]="0px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
       /* iArray[9]=new Array();
        iArray[9][0]="最后修改时间";         //列名
        iArray[9][1]="0px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许*/
        
        
        
  
        ActiRateManageGrid = new MulLineEnter( "fm" , "ActiRateManageGrid" ); 

        //这些属性必须在loadMulLine前
        ActiRateManageGrid.mulLineCount = 0;   
        ActiRateManageGrid.displayTitle = 1;
        ActiRateManageGrid.hiddenPlus = 1;
        ActiRateManageGrid.hiddenSubtraction = 1;
        ActiRateManageGrid.locked=1;
        ActiRateManageGrid.canSel=1;
        ActiRateManageGrid.canChk=0;
        ActiRateManageGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化ActiRateManageGrid时出错："+ ex);
      }
    }


</script>