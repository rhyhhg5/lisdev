<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAPresenceInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAPresenceGrpInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAPresenceInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "销售人员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();" >
  <form action="./LAPresenceSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPresence1);">
    <td class=titleImg>
      <%=tTitleAgent%>差勤信息
    </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAPresence1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            <%=tTitleAgent%>代码
          </TD>
          <TD  class= input>
            <Input class= common name=GroupAgentCode onchange="return checkValid();">
          </TD>
          <TD  class= title>
            <%=tTitleAgent%>姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Name>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            销售机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AgentGroup>
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ManageCom>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            纪录顺序号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Idx >
          </TD>
          <TD  class= title>
            类别
          </TD>
          <TD  class= input>
            <Input class='codeno' name=AClass verify="类别|notnull&code:presenceaclass" maxlength=1 
             ondblclick="return showCodeList('presenceaclass',[this,AClassName],[0,1]);" 
             onkeyup="return showCodeListKey('presenceaclass',[this,AClassName],[0,1]);"
             ><Input class=codename name=AClassName readOnly elementtype=nacessary>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            考勤执行次数
          </TD>
          <TD  class= input>
            <Input class= common name=Times verify="类别|INT">
          </TD>
          <!--TD  class= title>
            金额
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=SumMoney >
          </TD-->
          <TD  class= title>
            批注
          </TD>
          <TD  class= input>
            <Input class= common name=Noti >
          </TD>
        </TR>
        <TR  class= common><TD  class= title>
            执行日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=DoneDate  verify="类别|Date" dateFormat='short' >
          </TD>
         
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Operator >
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=ModifyDate value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=AgentCode value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
