<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAAscriptionBackInput.jsp
//程序功能：
//创建日期：2008-04-17 16:25:40
//创建人  ：苗祥征
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String MakeType= request.getParameter("MakeType");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<head >
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAAscriptionBackInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAAscriptionBackInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAscriptionBackSave.jsp" method=post name=fm target="fraSubmit">  
     <table>
    <tr class=common with = 100%>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
    <td class=titleImg>
      原销售人员信息
    </td>
    </td> 
    </tr>
    </table>
  <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common>
         
		<TR  class= common>      
  <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|notnull&code:comcode&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
          ><Input class="codename" name=ManageComName   readOnly elementtype=nacessary> 
          </TD> 
       	  <td class=title>归属日期 </td>
       <td class=input>
       <input class= "coolDatePicker" dateFormat="short" name=AcsriptionDate verify="归属日期|notnull&DATE"  elementtype=nacessary></td>
       </TR>
       <TR  class= common> 
          <td class=title>保单号 </td>
       <td class=input>
       <input class=common name=ContNo></td>
		 </TR>	
        <td>
      <input type=button class=cssbutton value='查  询' onclick="return agentConfirm()">
       <!--<Input type=button class= common value="确认1" OnClick="return agentConfirm1111();">确定按钮 -->  
      </TD>       
      
    </table>  
    
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription2);">
    <td class=titleImg>
      保单归属信息
    </td><td>(<font color="red" size="2">选中"序号"前复选框，则当前页所有纪录都将会被选中。</font>)</td>
    </td>
    </tr>
    </table>
  <Div  id= "divLAAscription2" style= "display: ''">
    <table  class= common>
            <tr  class= common>
                    
        <td text-align: left colSpan=1> 
		  <span id="spanAscriptionGrid" >
		  </span> 
        </td>
  			</tr>
    	</table>
    	<Div id="divPage" style= "display: ''">
      <INPUT class=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">	
      </Div>
      <table>
      <!--TD  class= title>
         现销售人员编码
      </TD>          
      <TD  class= input>
          <Input class=common name=NewAgentCode >
      </TD-->        
    </table>    
        <input type=button class=cssbutton name=saveb value='选择回退' onclick="submitSaveb();"> 
    </div>   
   </div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=''>
     <input type=hidden name=BranchType2 value=''>
     <input type=hidden name=Flag value=''>
     
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
