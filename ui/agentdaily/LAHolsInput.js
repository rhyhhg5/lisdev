//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;

//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
  if (mOperate=="")
  {
  	addClick();
  }
  //alert(mOperate);
   //if (mOperate == "QUERY||MAIN"){
   	//initForm();
   	//alert("请重新输入信息！");
   	//return false;
  //}
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  //if (!afterSubmit())
  //  return false;
   //alert("222");
  fm.submit(); //提交
 //  alert("111222");
  
  //  alert("111");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
 //fm.all.value = '';
 //var  tLeaveState=fm.all('LeaveState').value
var tAgentCode=fm.all('AgentCode').value;
 var tAgentGroup=fm.all('HiddenAgentGroup').value;
var ttime=fm.all('VacDays').value;
 
  document.all.AgentCode.readOnly=false;
  //document.all.AgentCode.readOnly="true";
 fm.reset();
 initInpBox();
//alert(document.all.AgentCode.value);
 
//alert("123");
  mOperate="";
  showInfo.close();
  //fm.all.value == '';
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
     if(ttime>10)
     {
      branchtrustee(tAgentCode,tAgentGroup);//如果请假超过十天,进入团队托管页面
     }
  }
 
}

function branchtrustee(tAgentCode,tAgentGroup)
{
 var strsql="select * from labranchgroup where agentgroup='"+tAgentGroup+"' and branchmanager='"+tAgentCode+"'  ";
 	
 var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);//如果是主管,进入团队托管
 if(strQueryResult)
 {	
  //var SQL="select * from laBranchTrustee";没有进行团队托管者进行
  alert("营销员代码为"+tAgentGroup+"的主管请假超过十天，请进入团队托管");
}	
	
}	

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LAHols.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	 
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
  {
  	alert("请输入代理人编码！");
  	fm.all('AgentCode').focus();
  	return false;
  }
  if ((fm.all('AgentGroup').value == '')||(fm.all('AgentGroup').value == null))
  {
  	alert("所输代理人编码无效！");
  	fm.all('AgentCode').focus();
  	return false;
  }
  
  if(!verifyInput())
       return false;
       
  if ((fm.all('VacDays').value != '')&&(fm.all('VacDays').value != null))   
  {
    if(fm.all('VacDays').value<=0)
    {
      alert("请假天数应该为有效整数!");
  	  fm.all('VacDays').focus(); 
  	  return false;	
  	}
  }   
//if ((fm.all('LeaveDate').value == '')||(fm.all('LeaveDate').value == null))
//{
//	alert("请输入请假日期！");
//	fm.all('LeaveDate').focus();
//	return false;
//}
	
	if((fm.all('LeaveDate').value != '')&&(fm.all('LeaveDate').value != null) &&
	   (fm.all('ShouldEndDate').value != '')&&(fm.all('ShouldEndDate').value != null))
	{
		if(fm.all('LeaveDate').value > fm.all('ShouldEndDate').value)
		{
			alert("销假日期不能早于请假日期！");
			return false;
		}
	}
	
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert('请先查询出要修改的纪录！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要修改的纪录！');
  else if ((fm.all("LeaveDate").value==null)||(fm.all("LeaveDate").value==''))
    alert('请确定请假日期！');
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAHolsQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert('请确定代理人！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请确定要删除的记录！');
  else if ((fm.all("LeaveDate").value==null)||(fm.all("LeaveDate").value==''))
  {
    alert('请确定请假日期！');
    fm.all("LeaveDate").focus();  
  }
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function checkValid()
{
  var strSQL = "";
  var strAgent = "";
	if(fm.GroupAgentCode.value!=null&&fm.GroupAgentCode.value!=''){
		strAgent = " and GroupAgentCode=getGroupAgentCode('"+fm.GroupAgentCode.value+"') ";
	}
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  if (getWherePart('GroupAgentCode')!='')
  {
     strSQL = "select * from LAAgent where 1=1 "
	   + /*strAgent*/getWherePart('GroupAgentCode','GroupAgentCode') + " and ((AgentState <'06') or (AgentState is null))"
	    + " and branchtype='"+tBranchType+"'"
	    + " and branchtype2='"+tBranchType2+"'";
	    
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('GroupAgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value  = '';
    fm.all('Name').value  = '';
    return false;
  }
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('GroupAgentCode').value='';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value  = '';
    fm.all('Name').value  = '';
    return;
  }
  //查询成功则拆分字符串，返回二维数组
   
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
　//<rem>######//　
  fm.all('Name').value = tArr[0][5];
  fm.all('AgentCode').value = tArr[0][0];
  //</rem>######//
  fm.all('ManageCom').value  = tArr[0][2];
    
  //存储原AgentGroup值，以备保存时使用  
  //<addcode>############################################################//
  old_AgentGroup=tArr[0][1];
  
  fm.all('HiddenAgentGroup').value = tArr[0][1];
  
  strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"' and (state<>'1' or state is null)"
  var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  if (strQueryResult_AgentGroup)
  {
  var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  
  var tArr_AgentGroup = arrDataSet_AgentGroup;

  //以备显示时使用
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
}
  //</addcode>############################################################//
}
/*
function agentConfirm()
{
  fm.all('AgentGroup').value='';
  fm.all('ManageCom').value='';	
  parent.fraInterface.fm.action = "AgentCodeQuery.jsp";	
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
  {
     alert("请输入代理人编码！");
     fm.all('AgentCode').focus();
     return false;
  }
  fm.submit();
  parent.fraInterface.fm.action = "LAHolsSave.jsp";	
  //alert('change');
}*/

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
                fm.all('GroupAgentCode').value = arrResult[0][0];
                var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+arrResult[0][0]+"' ";
			    var strQueryResult = easyExecSql(tYAgentCodeSQL);
			    if(strQueryResult == null){
			    	alert("获取业务员工号失败！");
			    	return false;
			    }
			    fm.all('AgentCode').value = strQueryResult[0][0];
                //fm.all('AgentCode').readonly=true;
                //document.getElementById("myid").readonly=true;
               document.all.AgentCode.readOnly="true";
                fm.all('AgentGroup').value = arrResult[0][1];
                fm.all('ManageCom').value = arrResult[0][2];
                fm.all('Idx').value = arrResult[0][3];
                fm.all('ModifyDate').value = arrResult[0][21];
                fm.all('AClass').value = arrResult[0][5];
                 if(fm.all('AClass').value !=null &&fm.all('AClass').value !="")
                {
                 var strSql_name = "select  codename from ldcode where codetype='holsaclass' "
                              + getWherePart("code", "AClass");
                var strQueryResult = easyQueryVer3(strSql_name, 1, 1, 1);  
                var tArr = new Array();
                tArr = decodeEasyQueryResult(strQueryResult);                            
                fm.all('AClassName').value = tArr[0][0];
               }
                
                fm.all('VacDays').value = arrResult[0][6];
                fm.all('LeaveDate').value = arrResult[0][7];
                fm.all('Name').value = arrResult[0][19];
                fm.all('ShouldEndDate').value = arrResult[0][8];
                fm.all('FillFlag').value = arrResult[0][10];
                if(fm.all('FillFlag').value !=null &&fm.all('FillFlag').value !="")
                {
                strSql_name = "select  codename from ldcode where codetype='yesno' "
                              + getWherePart("code", "FillFlag");
                strQueryResult = easyQueryVer3(strSql_name, 1, 1, 1);  
                tArr = new Array();
                tArr = decodeEasyQueryResult(strQueryResult);
                fm.all('FillFlagName').value = tArr[0][0];
               }
                
                fm.all('ConfIdenFlag').value = arrResult[0][11];
                 if(fm.all('ConfIdenFlag').value !=null &&fm.all('ConfIdenFlag').value !="")
                {
                 var strSql_name = "select  codename from ldcode where codetype='yesno' "
                              + getWherePart("code", "ConfIdenFlag");
                var strQueryResult = easyQueryVer3(strSql_name, 1, 1, 1);  
                var tArr = new Array();
                tArr = decodeEasyQueryResult(strQueryResult);                            
                fm.all('ConfIdenFlagName').value = tArr[0][0];
               }
                fm.all('AddVacFlag').value = arrResult[0][12];
                  if(fm.all('AddVacFlag').value !=null &&fm.all('AddVacFlag').value !="")
                {
                 var strSql_name = "select  codename from ldcode where codetype='yesno' "
                              + getWherePart("code", "AddVacFlag");
                var strQueryResult = easyQueryVer3(strSql_name, 1, 1, 1);  
                var tArr = new Array();
                tArr = decodeEasyQueryResult(strQueryResult);                            
                fm.all('AddVacFlagName').value = tArr[0][0];
               }
                fm.all('ApproveCode').value = arrResult[0][14];
                fm.all('Noti').value = arrResult[0][15];
                fm.all('Operator').value = arrResult[0][16];   
                //<addcode>############################################################//
                fm.all('HiddenAgentGroup').value = arrResult[0][17];
                
                var tLeaveState=""
                tLeaveState= arrResult[0][20];
                //alert(tLeaveState);
                if(tLeaveState=="0") fm.all('LeaveState').value='N';
               else fm.all('LeaveState').value='Y';
      //             if(tLeaveState !=null &&tLeaveState !="")
      //          {
      //           var strSql_name = "select  codename from ldcode where codetype='yesno' and code='"+tLeaveState+"' "
    //   + getWherePart("code", "LeaveState");
   //             var strQueryResult = easyQueryVer3(strSql_name, 1, 1, 1);  
    //            var tArr = new Array();
    //            tArr = decodeEasyQueryResult(strQueryResult);                            
    //            fm.all('LeaveState').value = tArr[0][0];
     //          }
                 
                
                //<addcode>############################################################//                                                                                                                                                                                                                                            	
 	 //return true;
 	}     

 
}

//无用
function handler(key_event)
{
	//onkeyPress="return handler(event);"
	//status = String.fromCharCode(key_event.keyCode);
	if (document.all)
	{
		if (key_event.keyCode==13)
		{
		    parent.fraInterface.fm.action = "AgentCodeQuery.jsp";	
		    if ((fm.all('GroupAgentCode').value == '')||(fm.all('GroupAgentCode').value == null))
                    {
  	              alert("请输入代理人编码！");
  	              return false;
                    }
		    fm.submit();
		    parent.fraInterface.fm.action = "LAHolsSave.jsp";	
		    return true;
		}
	}
}
function BranchClick()
{
var BranchType=fm.all('BranchType').value;	
var BranchType2=fm.all('BranchType2').value;
var AgentGroup=fm.all('HiddenAgentGroup').value;
	if(!AgentGroup)
	{
		alert('销售机构代码不能为空!');
		return false;
	}
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("../agentbranch/BranchTrusteeInput.jsp?BranchType=" + BranchType +"&BranchType2=" + BranchType2+"&AgentGroup="+AgentGroup);	
	
	
}	
	