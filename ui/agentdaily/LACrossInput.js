//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	
	if ( mOperate=="")
	{
		addClick();
	
	}
	
	if (!beforeSubmit())
    return false;
   
	  //alert(mOperate);
    //if (mOperate == "QUERY||MAIN"){
   	//initForm();
   	//alert("请重新输入信息！");
   	//return false;
  //}
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  fm.reset();
	 mOperate="";
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("在LARewardPunish.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
 
 if (!verifyInput()) return false;
  
 if (fm.all('StartAgentCode').value == '')
  {
  alert("请查询出一条数据！");
   fm.all('StartAgentCode').focus();
  return false;	  
  }
  if (fm.all('NowAgentCode').value == '')
  {
  alert("请查询出一条数据！");
   fm.all('NowAgentCode').focus();
  return false;	  
  }
  
   if (fm.all('StartPolNo').value == '')
  {
  alert("请查询出一条数据！");
   fm.all('StartPolNo').focus();
  return false;	  
  }
  if (fm.all('NowPolNo').value == '')
  {
  alert("请查询出一条数据！");
   fm.all('NowPolNo').focus();
  return false;	  
  }
   if (fm.all('RiskCode').value == '')
  {
  alert("请查询出一条数据！");
   fm.all('RiskCode').focus();
  return false;	  
  }
   if (fm.all('IdCardNo').value == '')
  {
  alert("请查询出一条数据！");
   fm.all('IdCardNo').focus();
  return false;	  
  }
          
  return true;	
}           


//显示frmSubmit框架，用来调试


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	
  //下面增加相应的代码  
   if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LACrossQueryInput.html");
  
}   
function queryClicksure()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  fm.submit();
  
}              

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码  
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function checkvalid()
{
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  //alert ("1");
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select agentcode,managecom,name,branchcode from LAAgent where 1=1 "
	     + getWherePart('AgentCode')+" and ((AgentState  not like '03') or (AgentState is null))"
	     + " and branchtype='"+tBranchType+"'";
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
 // alert("2");
  else
  {
    fm.all('AgentCode').value ="";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return false;
  }
  //alert("3");
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此代理人！");
    fm.all('AgentCode').value="";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return;
  }
  //alert("4");
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  //<rem>######//
  fm.all('Name').value = tArr[0][2];
  //</rem>######//
 // alert("5");
  fm.all('ManageCom').value  = tArr[0][1];
  
  //存储原AgentGroup值，以备保存时使用  
  //<addcode>############################################################//
  old_AgentGroup=tArr[0][3];
  
  fm.all('HiddenAgentGroup').value=tArr[0][3];
 // alert("6");
  var strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"' and (state<>'1' or state is null)"
     var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  //var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  //以备显示时使用
  //alert("7");
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
  //</addcode>############################################################//
  //alert("8");
}

function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
  //alert(arrQueryResult);
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.all('NowPolNo').value = arrResult[0][0]; 
    fm.all('NowP11').value = arrResult[0][1];  
    fm.all('NowAgentCode').value = arrResult[0][2];                                              
    fm.all('NowAgentName').value = arrResult[0][3];                                              
    fm.all('NowTransMoney').value = arrResult[0][4];
    fm.all('IdCardNo').value=arrResult[0][5];   
    fm.all('P13').value = arrResult[0][6];                                             
    fm.all('StartPolNo').value = arrResult[0][7]; 
    //fm.all('MarkRsn1').value = arrResult[0][7];                                           
    fm.all('StartTransMoney').value = arrResult[0][8];
    fm.all('StartP11').value = arrResult[0][9];
    fm.all('StartAgentCode').value = arrResult[0][10];
    
    fm.all('StartAgentName').value = arrResult[0][11];
    fm.all('PayYears').value = arrResult[0][12];
    fm.all('RiskCode').value = arrResult[0][13];
    
    
     
    //<addcode>############################################################//
  //  fm.all('HiddenAgentGroup').value = arrResult[0][11];
    //</addcode>############################################################//
                                                                                                                                                                                                                                                 	
  }
 // return true;
     
}