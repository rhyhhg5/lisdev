<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：AdjustCommData.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="AdjustCommData.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AdjustCommDataInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./AdjustCommDataSave.jsp" method=post name=fm target="fraSubmit">  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent);">
    <td class=titleImg>
      代理人业绩归属
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent" style= "display: ''">      
      <table class=common>
       <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='code' name=ManageCom verify="管理机构|code:comcode&NOTNULL" 
                                ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" 
                                onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
          </TD>
          <TD  class= title>
            代理人代码
          </TD>
          <TD  class= input>
            <Input class=common name=AgentCode verify="代理人代码|notnull">
          </TD>
        </TR>
       <TR  class= common>
          <TD  class= title>
            原机构代码
          </TD>
          <TD  class= input>
            <Input class=common name=OriginBranchAttr verify="原机构代码|notnull">
          </TD>
          <TD  class= title>
            归属后机构代码
          </TD>
          <TD  class= input>
            <Input class=common name=NewBranchAttr verify="归属后机构代码|notnull">
          </TD>
        </TR>
       <TR  class= common>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=StartDate dateformat = 'short' 
                                          verify="起始日期|notnull&Date">
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=Enddate dateformat = 'short' 
                                          verify="终止日期|Date">
          </TD>
        </TR>      	
    </table> 
    
    
    <BR> 
        <Input type=button class= common value="保存" onclick="AdjustAgentclick();">
    <input type=hidden name=BranchType>
    <!--input type=hidden name=OriginBranchCode>
    <input type=hidden name=NewBranchCode-->
   </div>
   
   <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustComm);">
    <td class=titleImg>
      代理人业绩打折调整
    </td>
    </td>
    </tr>
    </table>
    <Div  id= "divAdjustComm" style= "display: ''">      
      <table class=common>
       <TR  class= common>
         
          <TD  class= title>
            代理人代码
          </TD>
          <TD  class= input>
            <Input class=common name=AgentCode1 >
          </TD>
          
          <TD  class= title>
            调整起始年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo >例如:200401
          </TD>
          
        </TR>
       
          	
    </table> 
    
    
    <BR> 
        <Input type=button class=cssbutton value="调  整" onclick="AdjustCommclick();">
    <input type=hidden name=BranchType>
    <input type=hidden name=Operation>
    <!--input type=hidden name=OriginBranchCode>
    <input type=hidden name=NewBranchCode-->
   </div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
</body>
</html>
