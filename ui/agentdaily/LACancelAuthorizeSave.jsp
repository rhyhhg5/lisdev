<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABatchAuthorizeSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
  LAAgentSet tLAAgentSet = new LAAgentSet();
  LABatchAuthorizeUI tLABatchAuthorizeUI = new LABatchAuthorizeUI();
  String  tcheckType=request.getParameter("checkType").trim();
  //输出参数
  CErrors tError = null;
  String tOperate="DELETE||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	System.out.println("Save页面处理开始..."+tcheckType);  
  // 授权险种信息
 if ("FALSE".equals(tcheckType)) 
 { 
  String tChk[] = request.getParameterValues("InpAgentGridChk");
  
  String arrCount[] = request.getParameterValues("AgentGridNo");
   
  String tRiskCode[] = request.getParameterValues("AgentGrid8");
  
  String tAuthorObj[] = request.getParameterValues("AgentGrid1");
  System.out.println("||||||||||||||||||||"+arrCount.length);
  // 循环处理授权险种
  for(int j=0;j<arrCount.length;j++)
  {
  //如果被选中
    if(tChk[j].equals("1"))
    {
    LAAuthorizeSchema tLAAuthorizeSchema = new LAAuthorizeSchema();
    tLAAuthorizeSchema.setRiskCode(tRiskCode[j]);
    tLAAuthorizeSchema.setAuthorObj(tAuthorObj[j] );
    tLAAuthorizeSchema.setAuthorType("0");
    tLAAuthorizeSet.add(tLAAuthorizeSchema);
    }
  }
 }
  
//if (tLAAuthorizeSet.size()<=0)
//{
// Content = "请选择一条或多条授权信息!" ;
//  FlagStr = "Fail";
//  
//}
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(request.getParameter("AgentCode").trim());
	tVData.add(request.getParameter("AgentName").trim());
	tVData.add(request.getParameter("ManageCom").trim());
	tVData.add(request.getParameter("BranchAttr").trim());
	tVData.add(request.getParameter("AgentGrade").trim());
	tVData.add(request.getParameter("EnterYears").trim());
	tVData.add(request.getParameter("checkType").trim());
	tVData.add(request.getParameter("BranchType").trim());
	tVData.add(request.getParameter("BranchType2").trim());
	tVData.add(request.getParameter("RiskCode").trim());
	tVData.add(tLAAuthorizeSet);
	tVData.add(tG);
  System.out.println("add over");
  
  try
  {
    tLABatchAuthorizeUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABatchAuthorizeUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

