//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet; 
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  initPolGrid();
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LATrainQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
  var arrReturn = new Array();
	var tSel = TrainGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
                               // alert(arrReturn);
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}

//修改人：解青青 时间：20141106
function checkValid()
{ 
  var strSQL = "";
  if (getWherePart('GroupAgentCode')!=''){
     strSQL = "select getunitecode(agentcode) from LAAgent where 1=1 "
	     + getWherePart('groupagentcode','GroupAgentCode');
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  } else
  {
    fm.all('GroupAgentCode').value = '';
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此营销员！");
    fm.all('GroupAgentCode').value  = "";
    return false;
  }
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentCode').value = tArr[0][0];
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = TrainGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected;
	arrSelected = new Array();
	//alert(tRow);
	var GroupAgentCode=TrainGrid.getRowColData(tRow-1,1);
//	var AgentCode=fm.all('AgentCode').value;
	var idx=TrainGrid.getRowColData(tRow-1,2);
  //alert(idx);
	var strSQL = "select getUniteCode(a.agentcode),c.name,a.branchattr,a.managecom,a.aclass,a.traincode,a.trainname,a.trainstart,a.trainend,a.trainunit,a.charger,a.result,a.resultlevel,a.trainpassflag,a.noti,a.donedate,a.idx,a.operator,a.ModifyDate from LATrain a,LAAgent c where c.GroupAgentCode = '"+trim(GroupAgentCode)+"' and a.Idx = "+trim(idx)+""
	            //+" and a.AgentGroup = b.AgentGroup "
	            +" and a.AgentCode = c.AgentCode "
	            +getWherePart("c.BranchType","ttttBranchType")
	            +getWherePart("c.BranchType2","ttttBranchType2");
	           
	
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	
	arrSelected = decodeEasyQueryResult(strQueryResult);
	
	return arrSelected;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initTrainGrid();

	// 书写SQL语句
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSQL = "";
	var tReturn = getManageComLimitlike("a.ManageCom");
	strSQL = "select getUniteCode(a.AgentCode),a.Idx,a.branchattr,a.ManageCom,"
	         +"a.TrainName,a.Aclass,a.TrainStart,a.TrainEnd "
	         +"from LATrain a,LAAgent b where a.agentcode=b.agentcode "
	         + tReturn	        
	        // + getWherePart('a.AgentCode','AgentCode')
	         +strAgent
	         + getWherePart('a.ManageCom','ManageCom')
	         + getWherePart('a.Idx','Idx','','1')
	         + getWherePart('a.AClass','AClass')
	         + getWherePart('a.TrainUnit','TrainUnit')
	         + getWherePart('a.TrainName','TrainName')
	         + getWherePart('a.Charger','Charger')
	         + getWherePart('a.Result','Result')
	         + getWherePart('a.TrainPassFlag','TrainPassFlag')
	         + getWherePart('a.TrainStart','TrainStart')
	         + getWherePart('a.TrainEnd','TrainEnd')
	         + getWherePart('a.Operator','Operator')
	         + getWherePart('a.DoneDate','DoneDate')
	         + getWherePart('a.DoneFlag','DoneFlag')
	         + getWherePart('b.BranchType','ttttBranchType')
	         + getWherePart('a.Noti','Noti')
	         + getWherePart('a.BranchAttr','AgentGroup');		         
	         +"order by a.agentcode";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,3,1,2,6,4,11,12]);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = TrainGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES); 
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function ListExecl()
{
  
//定义查询的数据
var strSQL = "";
strSQL = "select getUniteCode(a.AgentCode),a.Idx,a.branchattr,a.ManageCom,"
	         +"a.TrainName,a.Aclass,a.TrainStart,a.TrainEnd "
	         +"from LATrain a,LAAgent b where a.agentcode=b.agentcode "       
	         + getWherePart('a.AgentCode','AgentCode')
	         + getWherePart('a.ManageCom','ManageCom')
	         + getWherePart('a.Idx','Idx','','1')
	         + getWherePart('a.AClass','AClass')
	         + getWherePart('a.TrainUnit','TrainUnit')
	         + getWherePart('a.TrainName','TrainName')
	         + getWherePart('a.Charger','Charger')
	         + getWherePart('a.Result','Result')
	         + getWherePart('a.TrainPassFlag','TrainPassFlag')
	         + getWherePart('a.TrainStart','TrainStart')
	         + getWherePart('a.TrainEnd','TrainEnd')
	         + getWherePart('a.Operator','Operator')
	         + getWherePart('a.DoneDate','DoneDate')
	         + getWherePart('a.DoneFlag','DoneFlag')
	         + getWherePart('b.BranchType','ttttBranchType')
	         + getWherePart('a.Noti','Noti')
	         + getWherePart('a.BranchAttr','AgentGroup');		         
	         +"order by a.agentcode";
     
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '代理人代码','记录顺序号','销售机构','管理机构','培训名称','培训类型','培训起始时间','培训中止时间'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '培训管理 查询下载' from dual where 1=1  ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}
