var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
/**************************************
 * 点击查询按钮执行的操作
 **************************************/
function agentQueryClick()
{
	
	if( verifyInput() == false ) return false;
	var tSQL = "";

	var tEnterYears = trim(document.fm.EnterYears.value);

	if(tEnterYears=="")
	{
	  tEnterYears = "1000";
	}else
	{
		// 验证输入的工龄是否是整数
		if (!isInteger(tEnterYears))
		{
			alert("[入司工龄限制]请输入一个正整数！");
			return false;
		}
	}
	  
	tSQL  = "SELECT";
	tSQL += "     a.AgentCode,";
	tSQL += "     a.Name,";
	tSQL += "     a.ManageCom,";
	tSQL += "     b.BranchAttr,";
	tSQL += "     b.Name,";
	tSQL += "     a.EmployDate,c.agentgrade";
	tSQL += " FROM";
	tSQL += "     LAAgent a,";
	tSQL += "     LABranchGroup b,";
	tSQL += "     LATree c";
	tSQL += " WHERE";
	tSQL += "     a.AgentGroup = b.AgentGroup";
	tSQL += " AND a.AgentCode = c.AgentCode";
	tSQL += getWherePart('a.AgentCode','AgentCode','like');
	tSQL += getWherePart('a.Name','AgentName','like');
	tSQL += getWherePart('a.ManageCom','ManageCom','like');
	tSQL += getWherePart('b.BranchAttr','BranchAttr','like');
	if(document.fm.AgentGrade.value!=null  && document.fm.AgentGrade.value!='')
	{
	tSQL += " AND c.agentgrade <'"+document.fm.AgentGrade.value+"'    "; 
        }
 
	tSQL += " AND a.EmployDate + "+tEnterYears+" year > current date";
	tSQL += " AND a.BranchType='" +document.fm.BranchType.value + "'";
	tSQL += " AND a.BranchType2='" +document.fm.BranchType2.value + "'";
	tSQL += " AND a.AgentState in ('01','02')";
	tSQL += " ORDER BY a.ManageCom,b.BranchAttr,a.AgentCode";
	
	turnPage1.queryModal(tSQL, AgentGrid);
	//alert(tSQL);
}

/**************************************
 * 点击删除按钮执行的操作
 **************************************/
function agentDeleteClick()
{
	var se = document.getElementById("AgentList");
	var tSeCount = se.options.length;
	var i
	
	if (tSeCount < 1)
	{
		alert("当前列表中没有内容！");
		return;
	}
	// 检索被选择的项目，去掉他们
	for(i=0;i<tSeCount;i++)
	{
		if(se.options[i].selected)
		{
			se.options.remove(i);
			i--;
			tSeCount--;
		}
	}
}

/**************************************
 * 点击查询按钮执行的操作
 **************************************/
function agentAddClick()
{
	var tGridCount = AgentGrid.mulLineCount;   // 查询出来的人员列表的行数
	var i

  // 循环处理
  for(i=0;i<tGridCount;i++)
  {
  	// 判断是否被选择
  	if(AgentGrid.getChkNo(i))
  	{
  		var tAgentCode = AgentGrid.getRowColData(i,1);
  		var tAgentName = AgentGrid.getRowColData(i,2);
  		//加入到左边列表里
  		addSelectOption(tAgentCode+" - "+tAgentName,tAgentCode);
  	}
  }
}

/**************************************
 * 在select控件中增加一个Option内容
 * 参数：  pmText  ：Option的text属性
 *         pmValue ：Option的value属性
 * 返回：  无
 **************************************/
function addSelectOption(pmText,pmValue)
{
	var se = document.getElementById("AgentList");
	var op = new Option;
	var i
	
	// 判断当前数据是否已经加进去了
	var count = se.options.length;
	for(i=0;i<count;i++)
	{
		if(pmValue == se.options[i].value)
		return;
	}
	
	op.value = pmValue;
	op.text = pmText;
	se.add(op);
}

/**************************************
 * 在选择列表中选择一个人的时候查询他的当前授权情况
 * (有问题，暂时不能实现)
 **************************************/
function clickAgentSelected()
{
	var se = document.getElementById("AgentList");
	var tSeCount = se.options.length;
	var i
	var tSelectedCount = 0;
	var tSQL = "";
	var tAgentCode = "";
	
	for(i=0;i<tSeCount;i++)
	{
		if(se.options[i].selected)
		{
			tAgentCode = se.options[i].value;
			tSelectedCount++;
		}
	}
	// 如果被选择的项目条数大于1  清空险种列表
	if (tSelectedCount>1)
	{
		clearAuthorizeMulLine();
		return;
	}
	// 被选择的项目就是一条时
	tSQL = "select distinct riskcode,(select lmrisk.riskname from lmrisk where lmrisk.riskcode=LAAuthorize.riskcode),authorstartdate,authorenddate "
       + " from LAAuthorize where 1=1 and LAAuthorize.AuthorObj = '"+tAgentCode+"'";
       
  turnPage.queryModal(tSQL, AuthorizeGrid);
}

/**************************************
 * 清空险种列表
 **************************************/
function clearAuthorizeMulLine()
{
   AuthorizeGrid.clearData("AuthorizeGrid");
}

/**************************************
 * 把被选择的人员，加入到一个隐藏框里
 **************************************/
function setAgentList()
{
	var se = document.getElementById("AgentList");
	var tSeCount = se.options.length;
	var i
	var tAgentCode = "";
	var tAgentCodeList = document.fm.listAgentCode.value;
	
	for(i=0;i<tSeCount;i++)
	{
		tAgentCode = se.options[i].value;
		if(tAgentCodeList.indexOf(tAgentCode) == -1)
		{
		  tAgentCodeList += tAgentCode + ",";
		}
	}
	
	//document.fm.listAgentCode.value = tAgentCodeList.substring(0,tAgentCodeList.length-1);
	document.fm.listAgentCode.value = tAgentCodeList
}

/**************************************
 * 执行按钮触发的事件
 **************************************/
function agentSubmitClick()
{
	if( verifyInput() == false ) return false;
	// 如果用户选择了“指定条件的全体人员”的选择框  设置标记
	if (document.fm.checkbox1.checked)
	{
	  document.fm.checkType.value = "TRUE";
	  // 如果设置了工龄条件，检验是否是数字
	  if(!isInteger(document.fm.EnterYears.value) && "" != trim(document.fm.EnterYears.value))
	  {
	  	alert("[入司工龄限制]项必须录入一个整数！");
	  	return false;
	  }
	}
	else
	{
		document.fm.checkType.value = "FALSE";
		// 设置授权人员列表
		setAgentList();
	}
	
	// 如果用户没有选择“指定条件的全体人员”的选择框  判断用户是否选择了目标业务员
	if(document.fm.checkType.value == "FALSE")
	{
		if(document.fm.AgentList.length == 0)
		{
			alert("请您先选择需要授权的人员！");
			return false;
		}
	}
	
	// 判断是否存在等待授权的险种
	if(!checkAuthorizeGrid())
	{
		return false;
	}
	
	document.fm.hideOperate.value = "INSERT||MAIN";
	
	// 验证通过提交画面
	submitForm();
}

/**************************************
 * 验证险种列表是否有内容
 * 参数：  无
 * 返回：  Boolean
 **************************************/
function checkAuthorizeGrid()
{
	var tGridCount = AuthorizeGrid.mulLineCount;   // 查询出来的险种列表的行数
	var tRturn = false;
	var i
	var tAlertText = "请录入险种信息！";

	// 循环检查是否有等待授权的险种
	for(i=0;i<tGridCount;i++)
	{
		var tRiskCode = '';
		tRiskCode = AuthorizeGrid.getRowColData(i,1);
		if(tRiskCode==null ||tRiskCode=='')
		{
			alert("险种编码不能为空！");
			return false;
		}
		
		for(var j=i+1;j<tGridCount;j++)
   	{
   	   if (tRiskCode==AuthorizeGrid.getRowColData(j,1))
   	   {
   	      alert('一种险种不能授权两次以上！');
   	      return false;	
   	   }
   	}
   	
   	var tRC1='';      	
    tRC1=AuthorizeGrid.getRowColData(i,3);
    if ((tRC1==null)||(tRC1==''))
    {
    	alert('取消销售资格起期不能为空！');
      return false;
    }
    var tRC2=''; 
   	tRC2=AuthorizeGrid.getRowColData(i,4);
	  if ((tRC2==null)||(tRC2==''))
   	{
   		alert('取消销售资格止期不能为空！');
   		return false;
   	}
   	if(tRC2<tRC1)
   	{
   	  alert('取消销售资格止期应该大于取消销售资格起期！');
   		return false;
   	}
	}
	
	return true;
}

/**************************************
 * 提交画面
 **************************************/
function submitForm()
{
  if(!verifyInput())
  {
	 	return false;
	}
	if(AuthorizeGrid.mulLineCount==0)
	{
	  alert('授权险种信息不能为空！');
   		return false;	
	}
	if(!AuthorizeGrid.checkValue())
	{
		return false;
	}
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}

/**************************************
 * 提交后操作,服务器数据返回后执行的操作
 **************************************/
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    document.fm.listAgentCode.value="";
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    initForm();
  }
}