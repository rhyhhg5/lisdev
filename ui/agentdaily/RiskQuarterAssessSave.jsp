<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：RiskQuarterAssessRateSave.jsp
//程序功能：
//创建日期：2009-02-19
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
<%
  // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tGI = new GlobalInput(); 
  tGI=(GlobalInput)session.getValue("GI"); 
    
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  {
  	transact = request.getParameter("fmtransact");
    String tManageCom = request.getParameter("ManageCom");
    String tBranchType= request.getParameter("BranchType");
    String tBranchType2= request.getParameter("BranchType2");
    String tChk[] = request.getParameterValues("InpRiskQuarterAssessGridChk");
    String tIDX[] = request.getParameterValues("RiskQuarterAssessGrid1");
    String tAgentGrade[] = request.getParameterValues("RiskQuarterAssessGrid2");
    String tRiskCode[] = request.getParameterValues("RiskQuarterAssessGrid5");
    String tStandPrem[] = request.getParameterValues("RiskQuarterAssessGrid6");
    LADiscountSet tLADiscountSet=new LADiscountSet();
    for(int i=0;i<tChk.length;i++)
    {
    	if(tChk[i].equals("1"))
    	{
    		LADiscountSchema tLADiscountSchema=new LADiscountSchema();
    		if(StrTool.cTrim(tIDX[i]).equals("")||StrTool.cTrim(tIDX[i]).equals("null"))
    		{
    			tLADiscountSchema.setIdx(-1);
    		}
    		else
    		{
    			tLADiscountSchema.setIdx(Integer.parseInt(tIDX[i]));
    		}
    		tLADiscountSchema.setAgentGrade(tAgentGrade[i]);
    		tLADiscountSchema.setManageCom(tManageCom);
    		tLADiscountSchema.setBranchType(tBranchType);
    		tLADiscountSchema.setBranchType2(tBranchType2);
    		tLADiscountSchema.setRiskCode(tRiskCode[i]);

    		tLADiscountSchema.setStandPrem(tStandPrem[i]);
    		tLADiscountSet.add(tLADiscountSchema);
    	}
    	
    }
    if(tLADiscountSet.size()==0)
    {
    	FlagStr = "Fail";
    	Content="没有得到要处理的数据！";
    }
    else
    {
   		System.out.println("init finish .............................");
    	try
  		{
	  		// 准备传输数据 VData
	    	VData tVData = new VData();
	    	tVData.addElement(tLADiscountSet);
	    	tVData.addElement(tGI);
	    	// 数据传输
	    	RiskQuarterAssessBL tRiskQuarterAssessBL = new RiskQuarterAssessBL();
	    	tRiskQuarterAssessBL.submitData(tVData, transact);
	    	tError = tRiskQuarterAssessBL.mErrors;
  		}
	  	catch(Exception ex)
	  	{
	  		System.out.println(ex.toString());
	    	Content = "操作失败，原因是:" + ex.toString();
	    	FlagStr = "Fail";
	  	}
	  	if (FlagStr=="")
  		{   
	    	if (!tError.needDealError())
	    	{                          
	    		Content = " 操作成功! ";
	    		FlagStr = "Success";
	   	 	}
	    	else                                                                           
	    	{
	    		Content = " 操作失败，原因是:" + tError.getFirstError();
	    		FlagStr = "Fail";
	    	}
  		}
    }
  }
%>
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>