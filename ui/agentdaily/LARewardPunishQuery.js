//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  initPolGrid();
//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LARewardPunishQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//xqq 2014-12-2 
function checkvalid()
{
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
  
  if (getWherePart('GroupAgentCode')!='')
  {
     strSQL = "select distinct a.AgentCode from LARewardPunish a,LAAgent b where a.agentcode = b.agentcode"
	    + getWherePart('b.GroupAgentCode','AgentCode')
	  + getWherePart('a.BranchType','BranchType')
	  + getWherePart('a.ManageCom','ManageCom')
	   + getWherePart('a.BranchType2','BranchType2');
	  
	     
    var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
     //alert(strQueryResult);
  }
  else
  {
    fm.all('GroupAgentCode').value = '';
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此代理人！");
    fm.all('GroupAgentCode').value="";
    return;
  }
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  
  fm.all('AgentCode').value = tArr[0][0];
//  alert(fm.all('AgentCode').value);
}


function getQueryResult()
{
  var arrSelected = null;
  tRow = RewardPunishGrid.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet == null )
    return arrSelected;
  arrSelected = new Array();
  //alert("112||"+tRow);
  var GroupAgentCode=RewardPunishGrid.getRowColData(tRow-1,1);
  var idx=RewardPunishGrid.getRowColData(tRow-1,4)
  var strSQL ="select getUniteCode(a.AgentCode),a.agentgroup,a.managecom,a.idx,a.money,a.aclass,a.sendgrp,a.awardtitle,a.punishrsn,a.donedate,a.noti,a.doneflag,a.operator,a.makedate,a.maketime,a.modifydate,a.modifytime,a.branchtype,a.branchattr,a.violationtype,a.penaltytype,a.branchtype2,a.latecount,a.sickcount,a.affaircount,a.wageno,a.check1,a.check2,a.check3,a.agentcom,c.Name from LARewardPunish a,LAAgent c where c.GroupAgentCode = '"+trim(GroupAgentCode)+"' and a.Idx = "+trim(idx)+" "
//	      "select a.*,c.Name from LARewardPunish a,LAAgent c where c.GroupAgentCode = '"+trim(GroupAgentCode)+"' and a.Idx = "+trim(idx)+" "
              +" and a.AgentCode = c.AgentCode";
	          + getWherePart('a.BranchType','BranchType')
	          + getWherePart('a.BranchType2','BranchType2')
//  //alert(strSQL);
  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
// // alert(strQueryResult);
 arrSelected = decodeEasyQueryResult(strQueryResult);
 return arrSelected;
}


function returnParent()
{
  var arrReturn = new Array();
  var tSel = RewardPunishGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
    try
    {	
      //alert(tSel);
      arrReturn = getQueryResult();
      top.opener.afterQuery( arrReturn );
    }
    catch(ex)
    {
      alert( "没有发现父窗口的afterQuery接口。" + ex );
    }
    top.close();
		
  }
}




function easyQueryClick()
{
	/*
	//查询修改
	var old_AgentGroup=fm.all('AgentGroup').value;
	var new_AgentGroup="";
	if (old_AgentGroup!='')
	{
            var strSQL_AgentGroup = "select AgentGroup from labranchgroup where 1=1 "
                                   +"and BranchAttr='"+old_AgentGroup+"' and (state<>'1' or state is null)"
            var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
            if (!strQueryResult_AgentGroup)
            {
            	alert("查询失败");
          	return false;
            }
            var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
            new_AgentGroup=arrDataSet_AgentGroup[0][0];
        }
         var str_new_AgentGroup="";
        if (new_AgentGroup!='')
            str_new_AgentGroup=" and a.AgentGroup='"+new_AgentGroup+"' ";
            */
	
  // 初始化表格
  initRewardPunishGrid();
  if(!verifyInput())
  {
  return false;
  }
	var tReturn = getManageComLimitlike("a.ManageCom");
  // 书写SQL语句
  var strSQL = "";
  
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  
	strSQL = "select getUniteCode(a.AgentCode),a.BranchAttr,a.ManageCom,a.Idx,a.AwardTitle,a.PunishRsn,a.Money,a.DoneDate"	                  
	         +" from LARewardPunish a where a.DoneFlag='0' "
	         +tReturn
//	   + getWherePart('a.AgentCode','AgentCode')
	         +strAgent
	   + getWherePart('a.ManageCom','ManageCom')
	   + getWherePart('a.Idx','Idx','','1')
	   + getWherePart('a.AwardTitle','AwardTitle')
	   + getWherePart('a.PunishRsn','PunishRsn')
	   + getWherePart('a.AClass','AClass')
	   + getWherePart('a.SendGrp','SendGrp')
	   + getWherePart('a.Money','Money','','1')
	   + getWherePart('a.Noti','Noti')
	   + getWherePart('a.DoneDate','DoneDate')
	   + getWherePart('a.Operator','Operator')
	   + getWherePart('a.BranchType','BranchType')
	   + getWherePart('a.BranchType2','BranchType2')
	   + getWherePart('a.BranchAttr','AgentGroup');	   	   
//	   + str_new_AgentGroup;
     +" order by a.agentcode";
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,7,8,9]);
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = RewardPunishGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES); 

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}
function ListExecl()
{
   if(!verifyInput())
  {
  return false;
  }
//定义查询的数据
var strSQL = "";
var strAgent = "";
if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
	strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
}

strSQL =  "select getUniteCode(a.AgentCode),(select name from laagent where agentcode =a.agentcode ),a.BranchAttr,(select name from labranchgroup where agentgroup = a.agentgroup),a.ManageCom,a.AwardTitle,a.PunishRsn,a.Money,a.DoneDate"	                  
	         +" from LARewardPunish a where a.DoneFlag='0' and managecom like '86%' "
//	   + getWherePart('a.AgentCode','AgentCode')
	         +strAgent
	   + getWherePart('a.ManageCom','ManageCom')
	   + getWherePart('a.AwardTitle','AwardTitle')
	   + getWherePart('a.PunishRsn','PunishRsn')
	   + getWherePart('a.AClass','AClass')
	   + getWherePart('a.SendGrp','SendGrp')
	   + getWherePart('a.Money','Money','','1')
	   + getWherePart('a.Noti','Noti')
	   + getWherePart('a.DoneDate','DoneDate')
	   + getWherePart('a.Operator','Operator')
	   + getWherePart('a.BranchType','BranchType')
	   + getWherePart('a.BranchType2','BranchType2')
	   + getWherePart('a.BranchAttr','AgentGroup');	   	   
//	   + str_new_AgentGroup;
     +" order by a.agentcode";
     
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '代理人代码','代理人姓名','团队编码','团队名称','管理机构','奖励称号','惩罚原因','金额','执行日期'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '奖惩信息 查询下载' from dual where 1=1  ";  

//添加一个合计功能


  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}
