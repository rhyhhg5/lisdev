/** 
 * 程序名称：LAAccountsInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-20 18:03:36
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作

function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

function getAgentName()
{
	var tSql = "";
  
  tSql  = "SELECT";
  tSql += "    AgentCode";
  tSql += "  FROM";
  tSql += "    LAAgent";
  tSql += " WHERE 1=1";
  tSql += getWherePart('BranchType','BranchType')
  tSql += getWherePart('BranchType2','BranchType2')
  tSql += getWherePart('groupagentcode','GroupAgentCode');

  var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
  
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  document.fm.AgentCode.value = tArr[0][0];
}

// 查询按钮
function easyQueryClick() {
	if(!verifyInput())
  	{
  	return false;
    }
	initLAAccountsGrid();	    
	var strSql = "select getUniteCode(a.AgentCode),a.Account,a.AccountName, CASE a.State WHEN '0' THEN '有效' ELSE '无效' END,a.OpenDate,a.DestoryDate,a.bank,a.Operator,a.ModifyDate,b.ManageCom from LAAccounts a,laagent b where 1=1 and a.AgentCode=b.AgentCode "
    + getWherePart("b.groupagentcode", "GroupAgentCode")
    + getWherePart("a.Account", "Account")
    + getWherePart("a.State", "State")
    + getWherePart("a.OpenDate", "OpenDate")
    + getWherePart("a.DestoryDate", "DestoryDate")
    + getWherePart("a.AccountName", "AccountName")
    + getWherePart("b.AgentGroup", "AgentGroup")
    + getWherePart("b.BranchType", "BranchType")
    + getWherePart("b.BranchType2", "BranchType2")
    + getWherePart("b.BankCode","Bank")
    + " and b.ManageCom like '"+fm.all('ManageCom').value+"%'"
    +"order by a.agentcode";
  //alert(strSql);
 //turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1); 		
	turnPage.queryModal(strSql, LAAccountsGrid);
	 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有满足条件的信息！");
    return false;
    }
////查询成功则拆分字符串，返回二维数组
 //arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
//  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
//  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
 // turnPage.arrDataCacheSet = arrDataSet;
//  //turnPage.arrDataCacheSet = chooseArray(turnPage.arrDataCacheSet,[0,3,1,2,4,5]);
//  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
 //turnPage.pageDisplayGrid = LAAccountsGrid;    
//          
//  //保存SQL语句
 // turnPage.strQuerySql     = strSql; 
//  
//  //设置查询起始位置
 //turnPage.pageIndex       = 0;  
//  
//  //在查询结果数组中取出符合页面显示大小设置的数组
//  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 // var tArr = new Array();
 // tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
//  //调用MULTILINE对象显示查询结果
//  
//  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
// displayMultiline(tArr, turnPage.pageDisplayGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LAAccountsGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LAAccountsGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LAAccountsGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}



