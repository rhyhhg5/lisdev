var turnPage = new turnPageClass();

/**************************************
 * 点击"查询"按钮时触发的事件
 * 参数：  无
 * 返回：  无
 **************************************/
function easyQueryClick()
{
	var tSQL = "";
	var tInputValue = "";
	
	tSQL  = "select StartDate,EndDate,StatDate,PloyName,Organizer,AgentCode,AgentName,InspiritName,InspiritContent,SeqNo";
	tSQL += "  from LAInspirit";
	tSQL += " where 1=1";
	tSQL += getWherePart('PloyName','PloyName','like');
	tSQL += getWherePart('Organizer','Organizer','like');
	tSQL += getWherePart('AgentCode','AgentCode','like');
	tSQL += getWherePart('AgentName','AgentName','like');
	tSQL += getWherePart('InspiritName','InspiritName','like');
	tInputValue = trim(document.fm.StartDateF.value);
	if(tInputValue != "")
	{
		if(!isDate(tInputValue))
		{
			alert("[活动起期]内请输入格式为:'YYYY-MM-DD'的有效日期!");
			return;
		}
		tSQL += " and StartDate >= '" + tInputValue + "'";
	}
	tInputValue = trim(document.fm.StartDateT.value);
	if(tInputValue != "")
	{
		if(!isDate(tInputValue))
		{
			alert("[活动起期]内请输入格式为:'YYYY-MM-DD'的有效日期!");
			return;
		}
		tSQL += " and StartDate <= '" + tInputValue + "'";
	}
	tInputValue = trim(document.fm.EndDateF.value);
	if(tInputValue != "")
	{
		if(!isDate(tInputValue))
		{
			alert("[活动止期]内请输入格式为:'YYYY-MM-DD'的有效日期!");
			return;
		}
		tSQL += " and EndDate >= '" + tInputValue + "'";
	}
	tInputValue = trim(document.fm.EndDateT.value);
	if(tInputValue != "")
	{
		if(!isDate(tInputValue))
		{
			alert("[活动止期]内请输入格式为:'YYYY-MM-DD'的有效日期!");
			return;
		}
		tSQL += " and EndDate <= '" + tInputValue + "'";
	}
	
	turnPage.queryModal(tSQL, InspiritGrid);
}

/**************************************
 * 点击"返回"按钮时触发的事件
 * 参数：  无
 * 返回：  无
 **************************************/
function returnParent()
{
  var arrReturn = new Array();
	var tSel = InspiritGrid.getSelNo();

	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}
	else
	{

			try
			{
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
	}
}

/**************************************
 * 获取选中的行的全部数据
 * 参数：  无
 * 返回：  一个一维数组
 **************************************/
function getQueryResult()
{
	var tRow = InspiritGrid.getSelNo();
	var tArray = InspiritGrid.getRowData(tRow - 1);
	
	return tArray;
}

function EdorType(cObj,dobj)
{
	var tBranchType  = document.fm.BranchType.value;
	var tBranchType2 = document.fm.BranchType2.value;
	var tManageCom   = document.fm.ManageCom.value;

	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# and agentcode in (select a.agentcode from latree a where a.agentgrade<>#A00#)";

	showCodeList('Agentcode',[cObj,dobj], [0,1], null, mEdorType, "1");
}

/**************************************
 * 设置人员列表
 * 参数：  cObj,dobj  两个相关页面上的控件
 * 返回：  无
 **************************************/
function KeyUp(cObj,dobj)
{
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
	var tManageCom   = document.fm.ManageCom.value;

	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# and agentcode in (select a.agentcode from latree a where a.agentgrade<>#A00#)";

	showCodeList('Agentcode',[cObj,dobj], [0,1], null, mEdorType, "1");
}