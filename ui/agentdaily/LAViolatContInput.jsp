<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ALACaseInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<head >
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LAViolatContInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAViolatContInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAViolatContSave.jsp" method=post name=fm target="fraSubmit">  
      <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
     <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
    <td class=titleImg>
     违规保单信息维护    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common>
       <TR  class= common> 
          <TD  class= title>
            立案编号
          </TD>          
          <TD  class= input>
            <Input class=readonly readonly name=ViolatNo >
          </TD>  
          <td  class= title>
	    立案日期
	  </td>
          <td  class= input>
	    <input name=ViolatDate class="coolDatePicker" dateFormat="short" verify="立案日期|NotNull&DATE " elementtype=nacessary>
          </TD>
       </TR>  
       <TR  class= common>  
          <TD  class= title>
            投保单号
          </TD>          
          <TD  class= input>
            <Input class=common  name=PrtNo verify="投保单号|notnull" elementtype=nacessary onchange="return getcontno();" >
          </TD> 
	  <td  class= title>
	    保险单号
	  </td>
          <td  class= input>
            <input class=common name=ContNo verify="保险单号|notnull" elementtype=nacessary  onchange="return getprtno();">
	  </td>  
       </TR>  
       <TR  class= common>        	  
	
     	  <td  class= title>
	    营销员代码
	  </td>
          <td  class= input>
	    <input name=AgentCode class=readonly readonly  >
	  </td> 
	  <TD  class= title>
            营销员姓名
          </TD>          
          <TD  class= input>
            <Input class=readonly readonly  name=AgentName >
          </TD>  
       </TR> 	
       <TR  class= common>   

          <td  class= title> 
	    投保人姓名
	  </td>
          <td  class= input> 
            <input name=AppntName class=readonly readonly >
	  </td>  
          <td  class= title>
            被保险人姓名
	  </td>
          <td  class= input>
	    <input name=InsuredName1 class=readonly readonly >
	  </td>	  	
       </TR>  
       <TR  class= common> 

	  <td  class= title>
            被保险人姓名
	  </td>
	
          <td  class= input>
	    <input name=InsuredName2 class=readonly readonly  >
	  </td>
	  <td  class= title>
            被保险人姓名
	  </td>
          <td  class= input>
	    <input name=InsuredName3 class=readonly readonly  >
	  </td>	  
	</TR>   
	<TR  class= common> 

	  <td  class= title>
	    直接损失
	  </td>
          <td  class= input>
	    <input name=DirectLoss class=common >
	  </td>
          <TD  class= title>
            追回损失
          </TD>          
          <TD  class= input>
            <Input class=common name=ReplevyLoss  >
          </TD>	  
       </TR> 
       <TR  class= common> 	  
          <TD  class= title>
           结案日期
          </TD>          
          <TD  class= input>
            <Input  name=ViolatEndDate class="coolDatePicker" dateFormat="short" verify="立案日期|DATE " >
          </TD>     
       </TR>
    </table>
    <table class=common> 
       <TR> 
          <TD  class= title>
            保单处理意见
          </TD>       
       </TR>    
       <tr  class= common> 
          <td  class= common>
          <textarea name=DealMind cols="110" rows="3" class="common" >
          </textarea>
          </td>
	</tr>
    </table>
    <table class=common>		
	<TR  class= common>    
          <TD  class= title>
           操作员
          </TD>          
          <TD  class= input>
            <Input class=readonly readonly  name=Operator  >
          </TD>  
          <TD  class= title>
           操作日期
          </TD>          
          <TD  class= input>
            <Input class=readonly readonly  name= ModifyDate  >
          </TD>  
	</TR>	
    </table>    
   </div>
     <input type=hidden name=AppealNo value=''>
    
   <input type=hidden name=ManageCom value=''>
   <input type=hidden name=AgentGroup value=''>
   <input type=hidden name=State value=''>
   <input type=hidden name=AppntNo value=''>
   <input type=hidden name=InsuredNo1 value=''>
   <input type=hidden name=InsuredNo2 value=''>
   <input type=hidden name=InsuredNo3 value=''>
   <input type=hidden name=hideOperate value=''>
   <input type=hidden name=BranchType value=<%=BranchType%>>
   <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
     
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
