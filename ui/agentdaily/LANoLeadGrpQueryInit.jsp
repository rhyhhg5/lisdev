<%
//程序名称：LANoLeadGrpQueryInit.jsp
//程序功能：
//创建日期：2005-5-24 14:56
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<SCRIPT Language="JavaScript">
  //画面初始化
  function initForm()
  {
    //初始化MulLine
    initNoLeadGrpGrid();
  }
  
  //MulLine 初始化
  function initNoLeadGrpGrid()
  {
    var iArray = new Array();
  
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单位外部编码";		    	//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="单位名称";	    	//列名
      iArray[2][1]="250px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="成立时间";       		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="管理机构";       		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      NoLeadGrpGrid = new MulLineEnter( "fm" , "NoLeadGrpGrid" ); 
      //这些属性必须在loadMulLine前
      NoLeadGrpGrid.mulLineCount = 10;
      NoLeadGrpGrid.displayTitle = 1;
      NoLeadGrpGrid.locked = 1;
      NoLeadGrpGrid.canSel = 0;                 // 设置隐藏单选钮
      NoLeadGrpGrid.hiddenPlus = 1;
      NoLeadGrpGrid.hiddenSubtraction = 1;
      NoLeadGrpGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //NoLeadGrpGrid.setRowColData(1,1,"asdf");
    }
      catch(ex)
    {
	    alert(ex);
    }
  }
</SCRIPT>