<%
//程序名称：RiskQuarterAssessRateInit.jsp
//程序功能：
//创建日期：2009-02-19
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script language="JavaScript">
  var StrSql=" 1 and branchtype=#3# and branchtype2=#01# and GRADECODE <>#F00# AND GRADECODE>=#G41# ";
  var rSql=" 1  and code<>#1201# and code in (#730101#,#332401#) ";
</SCRIPT>
<script language="JavaScript">

function initForm()
{
  try
  {
    initRiskQuarterAssessRateGrid();
  }
  catch(re)
  {
    alert("RiskQuarterAssessInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var RiskQuarterAssessRateGrid;
function initRiskQuarterAssessRateGrid() {                            
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    
    iArray[1]=new Array();
    iArray[1][0]="主键";         		//列名
    iArray[1][1]="0px";         		//宽度
   // iArray[1][3]=3;         		//宽度

    iArray[2]=new Array();                                                 
    iArray[2][0]="职级类别";         		//列名                             
    iArray[2][1]="0px";            		//列宽                             
    iArray[2][2]=200;            			//列最大值                           
    //iArray[2][3]=2;                                                        
    //iArray[2][4]="gradelevel";                                           
    //iArray[2][5]="2|3";              	                                     
    //iArray[2][6]="0|1";                                                    
    //iArray[2][9]="职级类别|code:gradelevel";   
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
    iArray[2][14]='G5';                            
           
    
    iArray[3]=new Array();
    iArray[3][0]="职级类别名称";         		//列名
    iArray[3][1]="0px";         		//宽度
    iArray[3][2]=200;
    iArray[3][3]=0;         		//最大长度
    iArray[3][14]='G5';            		//最大长度
     
    iArray[4]=new Array();
    iArray[4][0]="险种编码";         		//列名
    iArray[4][1]="60px";         		//宽度
    iArray[4][2]=20;
    iArray[4][3]=2;         		//最大长度
    iArray[4][4]="bankfinish";              	        //是否引用代码:null||""为不引用
    iArray[4][5]="4|5"; ;
    iArray[4][6]="0|1";
    iArray[4][9]="险种编码|NotNull"; 
    iArray[4][15]= "1";
    iArray[4][16]=rSql;                                                     
    
    iArray[5]=new Array();
    iArray[5][0]="险种名称";         		//列名
    iArray[5][1]="140px";         		//宽度
    iArray[5][2]=200;
    iArray[5][3]=0;         		//最大长度
    
    
    iArray[6]=new Array();
    iArray[6][0]="保费类型"; //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[6][9]="保费类型|NotNull";
    iArray[6][10]="transtate";
    iArray[6][11]="0|^00|基本保费|^03|追加保费";
    
    iArray[7]=new Array();
    iArray[7][0]="渠道类型"; //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[7][9]="渠道类型|NotNull";
    iArray[7][10]="banktype";
    iArray[7][11]="0|^04|银代业务|^13|银代直销";
    
    iArray[8]=new Array();
    iArray[8][0]="缴费方式"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[8][9]="缴费方式|NotNull";
    iArray[8][10]="PayintvCode";
    iArray[8][11]="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴";
    
    
    iArray[9]=new Array();
    iArray[9][0]="保险期间"; //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=2;              //是否允许输入,1表示允许,0表示不允许 
    iArray[9][4]="payyear"; 
     
    
    
    iArray[10]=new Array();
    iArray[10][0]="最低缴费年期";         		//列名
    iArray[10][1]="80px";         		//宽度
    iArray[10][2]=100;
    iArray[10][3]=1;         		//最大长度

    
    iArray[11]=new Array();
    iArray[11][0]="缴费年期小于";         		//列名
    iArray[11][1]="80px";         		//宽度
    iArray[11][2]=100;
    iArray[11][3]=1;         		//最大长度

    
    iArray[12]=new Array();
    iArray[12][0]="间接绩效提奖比例（小数）";         		//列名
    iArray[12][1]="140px";         		//宽度
    iArray[12][2]=200;
    iArray[12][3]=1;         		//最大长度
    
    RiskQuarterAssessRateGrid = new MulLineEnter( "fm" , "RiskQuarterAssessRateGrid" ); 
    //这些属性必须在loadMulLine前
    RiskQuarterAssessRateGrid.canChk = 1;  
    RiskQuarterAssessRateGrid.displayTitle = 1;
    RiskQuarterAssessRateGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("在RiskQuarterAssessRateInit.jsp-->initRiskQuarterAssessRateGrid函数中发生异常:初始化界面错误!");
  }
}

</script>
