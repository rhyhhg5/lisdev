//程序名称：LADataGatherInput.js
//程序功能：查询没有主管的单位的函数
//创建日期：2005-5-30 9:30
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

/*********************************************************************
 *  查询提数情况
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQuery()
{
	// 初始化表格
	initNoLeadGrpGrid();
var tDayNum=fm.all("DayNum").value;
	// 书写SQL语句
  var strSQL = "SELECT";
  strSQL    += "    WageNo,";
  strSQL    += "    ManageCom,";
  strSQL    += "    BranchType,";
  strSQL    += "    BranchType2,";
  strSQL    += "    CASE";
  strSQL    += "      WHEN State = '00' THEN '初始状态'";
  strSQL    += "      WHEN State = '10' THEN '拆分结束'";
  strSQL    += "      WHEN State = '11' THEN '计算结束'";
  strSQL    += "      WHEN State = '12' THEN '重计算结束'";
  strSQL    += "      WHEN State = '13' THEN '佣金前重新计算'";
  strSQL    += "      WHEN State = '14' THEN '佣金计算完毕'";
  strSQL    += "      ELSE State";
  strSQL    += "    END AS AA,";
  strSQL    += "    StartDate,";
  strSQL    += "    EndDate,";
  strSQL    += "    cast(ModifyDate as CHAR(11)) || ModifyTime as DateTime,";
  strSQL    += "    Operator";
  strSQL    += "  FROM";
  strSQL    += "    LAWageLog";
  strSQL    += " WHERE";
  strSQL    += " 1=1";
  strSQL    += getWherePart('ManageCom','ManageCom','like');
  strSQL    += getWherePart('BranchType','BranchType','like');
  strSQL    += getWherePart('BranchType2','BranchType2','like');
//if (tDayNum!=null  && tDayNum!="")
//{
//     strSQL    +=  " and current day-"+tDayNum+" day ";
//}
  strSQL    += " ORDER BY";
  strSQL    += "    WageNo DESC";
//alert(strSQL);
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有提数信息！");
    return "";
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，DataGatherGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = DataGatherGrid;

  //保存SQL语句
  turnPage.strQuerySql = strSQL;

  //设置查询起始位置
  turnPage.pageIndex = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

/*********************************************************************
 *  点击查询按钮响应事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	 //if(verifyInput2() == false) return false;
	easyQuery();
}

/*********************************************************************
 *  描述  ：  执行回退操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{
	//判断画面输入内容是否合法
  if(verifyInput2() == false) return false;
  
  //判断输入的是数字
  var dayNum = document.fm.DayNum.value;
  if(isNumeric(dayNum) == false)
  {
    alert("'回退天数'必须填写数字！");
    return false;
  }

	if(document.fm.BranchType.value!="1" || document.fm.BranchType2.value!="01")
	{
		alert("展业类型和渠道代表的不是个人代理，请确认！");
		return false;
	}

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
  fm.submit();
}

/*********************************************************************
 *  回退操作结束后的处理
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	 	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		// 刷新查询结果
		//easyQueryClick();		
	}
}