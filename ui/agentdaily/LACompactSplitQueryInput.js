var turnPage = new turnPageClass();
/***************************************
 * 根据参数查询结果
 *     参数：pmSQL  查询用的SQL文
 *     返回：查询结果
 ***************************************/
function getArrValueBySQL(pmSQL)
{
	//alert(pmSQL);
	//执行SQL文查询结果
	var strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
	//判断查询是否成功
	if (!strQueryResult)
		return null;
	//把查询结果赋给一个数组变量
	var arr = decodeEasyQueryResult(strQueryResult);
	
	return arr;
}

/***************************************
 * 画面上查询按钮点击事件
 *     参数：无
 *     返回：boolean
 ***************************************/
function easyQueryClick()
{
	var tSQL = "";
	var tSelect = "";
	
	if (trim(document.fm.CurPayToDateF.value)!="")
	{
		tSelect += " AND(select max(enteraccdate) from ljtempfee where otherno=a.grpcontno) >= '"+trim(document.fm.CurPayToDateF.value)+"' ";
	}
	if (trim(document.fm.CurPayToDateT.value)!="")
	{
		tSelect += " AND (select max(enteraccdate) from ljtempfee where otherno=a.grpcontno) <= '"+trim(document.fm.CurPayToDateT.value)+"' ";
	}
	if (trim(document.fm.SignDateF.value)!="")
	{
		tSelect += " AND signdate >= '"+trim(document.fm.SignDateF.value)+"' ";
	}
	if (trim(document.fm.SignDateT.value)!="")
	{
		tSelect += " AND signdate <= '"+trim(document.fm.SignDateT.value)+"' ";
	}
	if (trim(document.fm.BranchAttr.value)!="")
	{
		tSelect += " AND (select BranchAttr from labranchgroup where  a.agentgroup=agentgroup) like '"+trim(document.fm.BranchAttr.value)+"%' ";
	}	
  tSQL  = "SELECT";
  tSQL += "  a.grpcontno A,";
  tSQL += "  '' B,";
  tSQL += "  a.ManageCom C,";
  tSQL += " (select BranchAttr from labranchgroup where  a.agentgroup=agentgroup) D,";
  tSQL += "  a.appntNo E,";
  tSQL += "  a.Grpname F,";
  tSQL += "  (select max(enteraccdate) from ljtempfee where otherno=a.grpcontno) G,";
  tSQL += "  a.signdate H";
  tSQL += " FROM";
  tSQL += "  lcgrpcont a";
  tSQL += " WHERE";
  tSQL += "  a.grpcontno<>'00000000000000000000' ";
  tSQL += getWherePart('a.GrpContNo','GrpContNo','like');
  tSQL += getWherePart('a.GrpPolNo','GrpPolNo','like');
  tSQL += getWherePart('a.ManageCom','ManageCom','like');
  tSQL += getWherePart('a.AppntNo','AppntNo','like');
  tSQL += getWherePart('a.Grpname','AppntName','like');
  tSQL += tSelect;
   tSQL += " order by grpcontno";
  
  turnPage.queryModal(tSQL, ContGrid);
}

/***************************************
 * 画面上返回按钮点击事件
 *     参数：无
 *     返回：boolean
 ***************************************/
function returnParent()
{
	var tSel = ContGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{	
				//alert(ContGrid.getRowColData(tSel-1,1));
				top.opener.afterQuery( ContGrid.getRowColData(tSel-1,1) );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
	}
}