<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAQualityAssessQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./LAQualityAssessQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LAQualityAssessQueryInit.jsp"%>

<title>品质查询</title>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();" > 

<form action="./LAQualityAssessQuerySubmit.jsp" method=post name=fm target="fraSubmit"> 
  
  <table> 
    <tr class=common> 
      <td class=common> <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLARewardPunish1);"> </td> 
      <td class=titleImg> 品质信息查询条件 </td> 
    </tr> 
  </table> 
  <Div  id= "divLAQualityAssess1" style= "display: ''"> 
  <table  class= common> 
    <tr  class= common> 
      <td  class= title> <%=tTitleAgent%>代码 </td> 
      <td  class= input> <input class= common name=GroupAgentCode verify="<%=tTitleAgent%>代码|notnull" onchange="return checkvalid();"> </td> 
      <td  class= title> <%=tTitleAgent%>姓名 </td> 
      <td  class= input> <input name=Name class=common> </td> 
    </tr> 
    <tr  class= common> 
      <td  class= title> 销售单位 </td> 
      <td  class= input> <input class=common name=AgentGroup > </td> 
      <td  class= title> 管理机构 </td> 
      <td  class= input> <input class=common name=ManageCom > </td> 
    </tr> 
    <tr  class= common> 
      <td  class= title> 分数类型 </td> 
      <td  class= input> <input class= codeno name=MarkType 
      	  ondblclick="return showCodeList('MarkType',[this,MarkTypeName],[0,1],null,null,null,1);" 
      	  onkeyup="return showCodeList('MarkType',[this,MarkTypeName],[0,1],null,null,null,1);" 
      ><Input name=MarkTypeName class="codename">
      </td> 
    </tr> 
    <tr  class= common> 
      <td  class= title> 加扣分原因 </td> 
      <td  class= input> <Input class=code name=MarkRsn ondblclick="return showCodeList('MarkRsn',[this,MarkRsn1],[0,1],null,fm.all('MarkType').value,'ItemType',null,700);" onkeyup="return showCodeList('MarkRsn',[this,MarkRsn1],[0,1],null,fm.all('MarkType').value,'ItemType',null,700);" > </td> 
    </tr> 
  </table> 
  <table class=common> 
    <tr  class= common> 
      <td  class= title8> 详细情况 </td> 
    </tr> 
    <tr  class= common> 
      <td  class= common> <textarea name="MarkRsn1" cols="110" rows="3" class="common" >
        </textarea> </td> 
    </tr> 
  </table> 
  <table class=common> 
    <tr  class= common> 
      <td  class= title> 分数 </td> 
      <td  class= input> <input name=Mark class= common  > </td> 
      <td  class= title> 执行日期 </td> 
      <td  class= input> <Input class= 'coolDatePicker' name=DoneDate verify="执行日期|notnull&DATE" format='short'> </td> 
    </tr> 
    <tr  class= common> 
      <td  class= title> 操作员代码 </td> 
      <td  class= input> <input name=Operator class="readonly" readonly > </td> 
      <td  class= title> 记录顺序号 </td> 
      <td  class= input> <input class="readonly" readonly name=Idx > </td> 
    </tr> 
  </table> 
  <input type=hidden name=BranchType value=''> 
  <input type=hidden name=BranchType2 value=''>
  <input type=hidden class=Common name=querySql > 
  <input type=hidden class=Common name=querySqlTitle > 
  <input type=hidden class=Common name=Title >
  <input type=hidden class=Common name=AgentCode>
   
  <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 
  <INPUT VALUE="下  载" class=cssbutton TYPE=button onclick="ListExecl();">
  <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> 
  
  <table> 
    <tr> 
      <td class=common> <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQualityAssessGrid);"> </td> 
      <td class= titleImg> 品质管理信息结果 </td> 
    </tr> 
  </table> 
  <Div  id= "divQualityAssessGrid" style= "display: ''"> 
    <table  class= common> 
      <tr  class= common> 
        <td text-align: left colSpan=1> <span id="SpanQualityAssessGrid" > </span> </td> 
      </tr> 
    </table> 
    <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 
    <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
  </div> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</form> 

</body>
</html>
