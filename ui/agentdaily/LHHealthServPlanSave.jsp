<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHHealthServPlanSave.jsp
//程序功能：
//创建日期：2006-03-20 11:21:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
 // LHHealthServPlanSchema tLHHealthServPlanSchema   = new LHHealthServPlanSchema();
  LHHealthServPlanUI tLHHealthServPlanUI   = new LHHealthServPlanUI();
  
   LHHealthServPlanSet tLHHealthServPlanSet = new  LHHealthServPlanSet();		//合同责任信息
  
    String tServItemCode[] = request.getParameterValues("LHHealthServPlanGrid1");					//MulLine的列存储数组
		String tServItemName[] = request.getParameterValues("LHHealthServPlanGrid2");					//MulLine的列存储数组
		
		String tServItemSN[] = request.getParameterValues("LHHealthServPlanGrid3");         //MulLine的列存储数组

//		String tContraItemNo[] = request.getParameterValues("LHHealthServPlanGrid4");
		
		
								
  
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
     
    int LHHealthServPlanCount = 0;
		if(tServItemCode != null)
		{	
			LHHealthServPlanCount = tServItemCode.length;
		}	
		System.out.println(" LHHealthServPlanCount is : "+LHHealthServPlanCount);
		
		for(int i = 0; i < LHHealthServPlanCount; i++)
		{
			
				LHHealthServPlanSchema tLHHealthServPlanSchema = new LHHealthServPlanSchema();
			
				  tLHHealthServPlanSchema.setServPlanCode(request.getParameter("ServPlanCode"));
				  tLHHealthServPlanSchema.setServPlanName(request.getParameter("ServPlanName"));
			    tLHHealthServPlanSchema.setServPlanLevel(request.getParameter("ServPlanLevelCode"));
			    tLHHealthServPlanSchema.setComID(request.getParameter("ComIDCode"));
			    tLHHealthServPlanSchema.setServPlayType(request.getParameter("ServPlayType"));
			    System.out.println("********************"+tLHHealthServPlanSchema.getServPlayType());


				  tLHHealthServPlanSchema.setServItemCode(tServItemCode[i]);	
				  tLHHealthServPlanSchema.setServItemName(tServItemName[i]);	
				  tLHHealthServPlanSchema.setServItemSN(tServItemSN[i]);	
			//	  tLHHealthServPlanSchema.setContraItemNo(tContraItemNo[i]);	
				 
                              
				tLHHealthServPlanSchema.setOperator(request.getParameter("Operator"));
				tLHHealthServPlanSchema.setMakeDate(request.getParameter("MakeDate"));
				tLHHealthServPlanSchema.setMakeTime(request.getParameter("MakeTime"));
				tLHHealthServPlanSchema.setModifyDate(request.getParameter("ModifyDate"));
				tLHHealthServPlanSchema.setModifyTime(request.getParameter("ModifyTime"));
                              
			                        
				tLHHealthServPlanSet.add(tLHHealthServPlanSchema);
			                        
		}
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  //	tVData.add(tLHHealthServPlanSchema);
  	tVData.add(tLHHealthServPlanSet);
  	tVData.add(tG);
    
    tLHHealthServPlanUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHHealthServPlanUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>