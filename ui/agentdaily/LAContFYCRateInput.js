//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("LAContFYCRateInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

/**********************************************
 * 注: 保存前 校验相关信息
 * //提交前的校验、计算 
 **********************************************/ 
 
function beforeSubmit()
{
   if(!chkMulLine()){ return false;}
   return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
/*****************************************
 * 注: 保存按钮对应操作
 *  Click事件，当点击增加图片时触发该函数
 *****************************************/
function addClick()
{
  if (verifyInput() == false)
  return false;	
  if(!beforeSubmit())
  {
    return false;
  }
	
  //下面增加相应的代码
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}     
      
/*****************************************
 * 注: 保存按钮对应操作
 *  Click事件，当点击“修改”图片时触发该函数
 *****************************************/
function updateClick()
{
  if (verifyInput() == false)
  return false;	
 if(!beforeSubmit())
  {
    return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
     var i = 0;
     var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
     fm.fmtransact.value = "UPDATE||MAIN";
     fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}                    
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/***************************************************
* 注:查询按钮对应操作
*    返回结果集 显示页面   *** 这个地方还需要修改下。。
****************************************************/

function easyQueryClick()
 {
  if (verifyInput() == false)
  return false;	
  initArchieveGrid();
   var tBranchtype2 =fm.all('BranchType2').value;
  //alert("*****:"+fm.all('managecom').value)
	//此处书写SQL语句		    			    
  var strSql = "select b.riskcode,(select riskname  from  lmriskapp d  where d.riskcode=b.riskcode ),b.grpcontno, "
	    +" b.managecom, "
	    +"DECIMAL(b.fycrate,12,6),"
	    +"case when (select value(a.rate,0)  from LAContFYCRate a  where  a.grpcontno=b.commisionsn and a.riskcode=b.riskcode) is null then 0 else " 
	    + "(select value(a.rate,0)  from LAContFYCRate a  where  a.grpcontno=b.commisionsn and a.riskcode=b.riskcode) end"
	    +",b.wageno,b.transmoney,b.commisionsn "
	    +" from  lacommision  b "
	    +" where 1=1 and b.branchtype='2' and b.branchtype2='"+tBranchtype2+"' and b.grpcontno<>'00000000000000000000' and commdire <= '1'"
	   +getWherePart('b.managecom', 'managecom','like')
    + getWherePart('b.GrpContNo', 'GrpContNo')
    + getWherePart('b.WageNo', 'WageNo')
    +" order by riskcode " ;
    
  var arrResult = new Array();
  arrResult = easyExecSql(strSql);
  //alert("wwww"+strSql);
//alert("ZZZZ"+arrResult);
  if (!arrResult) 
    {
    alert("此单没有收费或者不存在此保单！");
    return false;
    }
    displayMultiline(arrResult,ArchieveGrid);
   //查询成功则拆分字符串，返回二维数组
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}
// 校验MulLine

function chkMulLine()
{
	var i;
	var iCount = 0;
	var rowNum = ArchieveGrid.mulLineCount;
	for(i=0;i<rowNum;i++){
		if(ArchieveGrid.getChkNo(i)){
		var trate=ArchieveGrid.getRowColData(i,6); //非标
		if(trate==null||trate=="")
		{
		   alert("被选中的第"+(i+1)+"条记录非标比例为空,请重新录入！");
		   return false;
		}
			iCount++;
		} 
	}
	if(iCount == 0){
		alert("请选择要录入非标的记录!");
		return false
	}
	return true;
}