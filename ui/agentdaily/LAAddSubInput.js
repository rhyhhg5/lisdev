//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	if(!verifyInput()) 
  {
  	 return false;	  
  }
  
	if (mOperate=="")
	{
		addClick();
	}	 
	
  if (!beforeSubmit())
    return false; 
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
   
   fm.submit(); //提交

 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  mOperate="";
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  initForm();
    
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("在LARewardPunish.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if(!verifyInput()) 
  return false;
  if (fm.all('AgentCode').value == '')
  {
    alert("请输入代理人编码！");
    fm.all('AgentCode').focus();
    return false;	  
  }
  
   if (trim(fm.all('WageNo').value).length < 6)
  {
    alert("请输入正确的薪资年月！");
    fm.all('WageNo').focus();
    return false;	  
  }
 
  if (fm.all('LateCount').value!=null && fm.all('LateCount').value!='' && !isInteger(fm.all('LateCount').value))
  {
  	alert("请输入正确的迟到/早退次数");
  	return false;
  }	
  if (fm.all('SickCount').value!=null && fm.all('SickCount').value!='' && !isInteger(fm.all('SickCount').value))
  {
  	alert("请输入正确的病假次数");
  	return false;
  }	
  if (fm.all('AffairCount').value!=null && fm.all('AffairCount').value!='' && !isInteger(fm.all('AffairCount').value))
  {
  	alert("请输入正确的事假次数");
  	return false;
  }	
  if (fm.all('AbsentCount').value!=null && fm.all('AbsentCount').value!='' && !isInteger(fm.all('AbsentCount').value))
  {
  	alert("请输入正确的旷工次数");
  	return false;
  }	
  
  
  if(mOperate =='INSERT||MAIN'){
  	 var strSql  = "select count(*) from LARewardPunish where AgentCode=getagentcode('"
		  + fm.all("AgentCode").value + "')" 
		  + " and wageno='" + fm.all("WageNo").value + "'"
		  + " and BranchType ='2' and BranchType2='01'" ;
	  var sResult = easyExecSql(strSql);	 
	  
	  if(sResult && sResult[0][0] >0 ){
	  	 alert("该业务员相应年月考勤扣款信息已录入，请进行修改操作！");
	  	 return false;
	  }  
  }
  
  // 备注
  if(!(strLen(document.fm.Noti.value,100,"[备注]请输入33个汉字或100个字母或数字！")))
    return false;
  

  return true	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN"; 
 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert('请先查询出要修改的纪录！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要修改的纪录！');
  else if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";    
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAAddSubGrpQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
  
}   
function queryClicksure()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  fm.submit();
  
}              

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert('请先查询出要删除的纪录！');
  else if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要删除的纪录！');
  else if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function checkvalid()
{
  var strSQL = "";
  var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select getunitecode(agentcode),agentgroup,managecom,name from LAAgent where 1=1 "
	     //+ getWherePart('AgentCode')
         +strAgent
	     + getWherePart('BranchType')
	     + getWherePart('BranchType2');
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此代理人！");
    fm.all('AgentCode').value="";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return;
  }
  //查询成功则拆分字符串，返回二维数组
  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  //<rem>######//
  fm.all('Name').value = tArr[0][3];
  //</rem>######//
  
  fm.all('ManageCom').value  = tArr[0][2];
  
  //存储原AgentGroup值，以备保存时使用  
  //<addcode>############################################################//
  old_AgentGroup=tArr[0][1];
  
  fm.all('HiddenAgentGroup').value=tArr[0][1];
 
  strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"'"
           
     var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
       
  if (!strQueryResult_AgentGroup)
  {
  	alert("没有查询到业务员"+fm.all('AgentCode').value+"对应的销售单位信息!");
  	return false;
  }
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
 
  
}

function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();

  if( arrQueryResult != null )
  {
  	
    arrResult = arrQueryResult;
    fm.all('AgentCode').value = arrResult[0][0];  
    fm.all('Name').value = arrResult[0][1];                                              
    fm.all('AgentGroup').value = arrResult[0][2];                                              
    fm.all('ManageCom').value = arrResult[0][3];                         
    fm.all('LateCount').value = arrResult[0][4];
    fm.all('SickCount').value=arrResult[0][5]                                       
    fm.all('AffairCount').value = arrResult[0][6];
    fm.all('AbsentCount').value = arrResult[0][7];                                           
    fm.all('wageno').value = arrResult[0][8];
    fm.all('Noti').value = arrResult[0][9];    
    fm.all('Operator').value = arrResult[0][10]; 
    fm.all('Idx').value = arrResult[0][12];  
   
    //<addcode>############################################################//
    fm.all('HiddenAgentGroup').value = arrResult[0][11];
    //</addcode>############################################################//
    
    fm.all('AgentCode').readOnly=true;  
    fm.all('wageno').readOnly=true;                                                                                                                                                                                                                                           	
  }
     
}

/***************************************
 * 验证字符的字节长度是否超标
 * 参数：pmStr      准备验证的字符串
 *       pmStrLen   长度标准
 *       pmMissage  报错信息
 * 返回：返回字符串字节长度
 ***************************************/
function strLen(pmStr,pmStrLen,pmMissage)
{
	var i;
  var len;
  len = 0;
  for (i=0;i<pmStr.length;i++)
  {
    if (pmStr.charCodeAt(i)>255) len+=3; else len++;
  }
  //alert("[" + pmStr + "] 长度为： " + len + " 规定长度为："+pmStrLen);
  if(len > pmStrLen)
  {
  	alert(pmMissage);
  	return false;
  }
  
  return true;
}