<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String ManageCom=tG.ManageCom;
  
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAAddBSubPerInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAAddBSubPerInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<%
  //加扣款类别

  String tTitleAgent = "";
  if ("1".equals(BranchType))
  {
   
    tTitleAgent = "营销员";
  } 
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAddBSubPerSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAddSub);">
    
    <td class=titleImg>
      <%=tTitleAgent%>加款信息
    </td> 
    </td>
    </tr>
    </table>
    <Div  id= "divAddSub" style= "display: ''">  
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		      <%=tTitleAgent%>代码 
		    </td>
        <td  class= input> 
		      <input class= common name=GroupAgentCode MaxLength=10 OnChange="return checkvalid();" verify="业务员编码|NOTNULL" elementtype=nacessary> 
		    </td>
		    <td  class= title>
		      <%=tTitleAgent%>姓名
		    </td>
        <td  class= input>
		      <input name=Name class='readonly' readonly >
		    </td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		      销售单位 
		    </td>
        <td  class= input> 
		      <input class="readonly" readonly name=AgentGroup > 
		    </td>
        <td  class= title> 
		      管理机构 
		    </td>
        <td  class= input> 
		      <input class="readonly" readonly name=ManageCom > 
		    </td>
      </tr>
      <tr  class= common>
          <td  class= title> 
		      业务职级 
		    </td>
        <td  class= input> 
		      <input class="readonly" readonly name=AgentGrade > 
		    </td> 
        <td class=title>
	        加款类型
	      </td>
	      <td class=input>
		   <input name=DoneFlag class=codeno  verify="加款类型"
               ondblclick="return getAddMoney(DoneFlag,DoneFlagName);"
               onkeyup="return getAddMoney(DoneFlag,DoneFlagName);"
        	><Input class=codename name=DoneFlagName readOnly  > 
        </td>	
      </tr>  
      <tr>
         <td  class= title> 
		      加款原因
		     </td>
         <td  class= input> 
		     <input name=PunishRsn class= common   > 
		     </td>
         <td  class= title> 
		      加款金额(元)
		     </td>
         <td  class= input> 
		       <input name=Money class= common verify="金额|num&value>0" > 
		     </td>
       </tr>
    </table>
    </Div>
      
    <table>
    <tr>
      <td class=common>
		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
      </td>
    	<td class= titleImg>
    	  <%=tTitleAgent%>扣款信息
     	</td>
    </tr>
    </table>
    <Div  id= "divAGroupGrid" style= "display: ''">
    <table  class= common>
    <tr  class= common> 
  	  <td class=title>
	      扣款类型
	    </td>
	    <td class=input>
	    	<input name=DoneFlag1 class=codeno   verify="扣款类型"
               ondblclick="return getMinMoney(DoneFlag1,DoneFlag1Name);"
               onkeyup="return getMinMoney(DoneFlag1,DoneFlag1Name);"
         ><Input class=codename name=DoneFlag1Name readOnly > 
       </td>	
       <td  class= title> 
		    扣款原因
		   </td>
       <td  class= input> 
		     <input name=PunishRsn1 class= common   > 
	  </td>
	 </tr>
      <tr  class= common> 
       <td  class= title> 
		    扣款金额(元)
		   </td>
       <td  class= input> 
		     <input name=Money1 class= common verify="金额|num&value>0" > 
		   </td>
      </tr>  
      <tr  class= common>
        <TD  class= title width="25%"> 
		     调整年月
		    </td>
         <td class= input>
         <Input class= 'common'   name=WageNo verify="调整年月|NOTNULL&len=6" elementtype=nacessary>
         <font color="red">(yyyymm)
         </td>
        <td  class= title> 
		     备注
		    </td>
        <td  class= input> 
	       <input name=Noti class= common > 
	      </td>	
		 </tr>
     <tr class=common>
       <td  class= title>
		   操作员代码
		   </td>
       <td  class= input>
		    <input name=Operator class="readonly" readonly >
		   </td>
       <td  class= title> 
		   最近操作日
		   </td>
       <td  class= input> 
		    <input class="readonly" readonly name=ModifyDate > 
		   </td>
	   </tr>    
   </table>
    <p> <font color='red'>注：加款项目和扣款项目至少录入一项！加款金额和扣款金额都为正数！类型和金额是必填项！</font></p>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=Idx value=''>
    <input type=hidden name=Flag value=''>
    <input type=hidden name=Flag1 value=''>
    <input type=hidden name=AwardTitle value=''>
    <input type=hidden name=BranchType value=''>   
    <input type=hidden name=BranchType2 value=''>  
    <input type=hidden name=SendGrp value=''> 
    <input type=hidden name=LogManagecom value='<%=ManageCom%>'>
    <input type=hidden name=AgentCode value=''>
    <Input type=hidden name=AgentType value=''>
    <!--input type=hidden name=SendGrp1 value=''-->  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
