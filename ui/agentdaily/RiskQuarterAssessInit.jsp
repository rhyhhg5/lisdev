<%
//程序名称：RiskQuarterAssessInit.jsp
//程序功能：
//创建日期：2009-02-18
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script language="JavaScript">
  var StrSql=" 1 and branchtype=#3# and branchtype2=#01# and GRADECODE <>#F00# ";
  var rSql=" 1 and code in (#331201#,#331601#) ";
</SCRIPT>
<script language="JavaScript">

function initForm()
{
  try
  {
    initRiskQuarterAssessGrid();
  }
  catch(re)
  {
    alert("RiskQuarterAssessInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var RiskQuarterAssessGrid;
function initRiskQuarterAssessGrid() {                            
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    
    iArray[1]=new Array();
    iArray[1][0]="主键";         		//列名
    iArray[1][1]="0px";         		//宽度
   // iArray[1][3]=3;         		//宽度
    
    iArray[2]=new Array();
    iArray[2][0]="职级";         		//列名
    iArray[2][1]="50px";         		//宽度
    iArray[2][2]=200;
    iArray[2][3]=2;         		//最大长度
    iArray[2][4]="assessgrade2";         		//是否允许录入，0--不能，1--允许
    iArray[2][5]="2|3"; ;
    iArray[2][6]="0|1";
    iArray[2][9]="职级代码|code:assessgrade2"; 
    iArray[2][15]= "1";                                                    
    iArray[2][16]= StrSql;  
    
    iArray[3]=new Array();
    iArray[3][0]="职级名称";         		//列名
    iArray[3][1]="120px";         		//宽度
    iArray[3][2]=200;
    iArray[3][3]=0;         		//最大长度
     
    iArray[4]=new Array();
    iArray[4][0]="险种编码";         		//列名
    iArray[4][1]="0px";         		//宽度
    iArray[4][2]=20;
    iArray[4][3]=2;         		//最大长度
    iArray[4][4]="bankfinish";              	        //是否引用代码:null||""为不引用
    iArray[4][5]="4|5"; ;
    iArray[4][6]="0|1";
    iArray[4][9]="险种编码|NotNull"; 
    iArray[4][14]= "RRR"; 
    iArray[4][15]= "1"; 
    iArray[4][16]= rSql;                                                     
    
    iArray[5]=new Array();
    iArray[5][0]="险种名称";         		//列名
    iArray[5][1]="0px";         		//宽度
    iArray[5][2]=200;
    iArray[5][3]=0;         		//最大长度
    iArray[5][14]= "RRR"; 
    
    iArray[6]=new Array();
    iArray[6][0]="季度考核标准（单位：元）";         		//列名
    iArray[6][1]="120px";         		//宽度
    iArray[6][2]=200;
    iArray[6][3]=1;         		//最大长度
    
    RiskQuarterAssessGrid = new MulLineEnter( "fm" , "RiskQuarterAssessGrid" ); 
    //这些属性必须在loadMulLine前
    RiskQuarterAssessGrid.canChk = 1;
    //QuarterAssessStandardGrid.mulLineCount = 0;   
    RiskQuarterAssessGrid.displayTitle = 1;
    //QuarterAssessStandardGrid.hiddenPlus = 0;
    //QuarterAssessStandardGrid.hiddenSubtraction = 0;
    //QuarterAssessStandardGrid.canSel = 0;
    RiskQuarterAssessGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("在RiskQuarterAssessInit.jsp-->initRiskQuarterAssessGrid函数中发生异常:初始化界面错误!");
  }
}

</script>
