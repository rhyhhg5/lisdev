<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAContWriteSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAWageSchema mLAWageSchema = new LAWageSchema();
  LAWageSet  mLAWageSet = new LAWageSet();
  LAAgentSchema  mLAAgentSchema=new  LAAgentSchema();
  LAWageConfGatherUI mLAWageConfGatherUI = new LAWageConfGatherUI();
  //输出参数
  CErrors tError = null;
  CErrors tError2 = null;
  String tOperate="UPDATE||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String flag="0";            //验证往后台传入SET还是SHEMA
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin LAContAccessory schema...");
  //取得佣金信息
  mLAWageSchema.setIndexCalNo(request.getParameter("IndexCalNo"));
  mLAWageSchema.setAgentCode(request.getParameter("AgentCode"));
  mLAWageSchema.setAgentGroup(request.getParameter("AgentGroup"));
  mLAWageSchema.setManageCom(request.getParameter("ManageCom"));
  mLAWageSchema.setBranchType(request.getParameter("BranchType"));
  mLAWageSchema.setBranchType2(request.getParameter("BranchType2"));
  mLAAgentSchema.setName(request.getParameter("Name"));
  //mLAWageSchema.setState("0");
  
  int lineCount = 0;
  String tSel[] = request.getParameterValues("InpWageGridChk");
  String tIndexcalno[] = request.getParameterValues("WageGrid1");
  String tAgentcode[] = request.getParameterValues("WageGrid2");
  String tManagecom[] = request.getParameterValues("WageGrid4"); 
  //String tSummoney[] = request.getParameterValues("WageGrid6");
 // String tState[] = request.getParameterValues("WageGrid5");
  System.out.println("begin LAContAccessory schema...1"+tSel);
  if (tSel!=null)
  {//没有点击查询时 tSel==null
    lineCount = tSel.length; //行数
    System.out.println("begin LAContAccessory schema2...");
    for(int i=0;i<lineCount;i++)
    {
      if(tSel[i].equals("1"))
      {
     	mLAWageSchema=new LAWageSchema();
    	mLAWageSchema.setIndexCalNo(tIndexcalno[i]);
    	System.out.println("统一工号："+tAgentcode[i]);//根据现有逻辑tAgentcode[i]为集团统一工号，需转换为内部工号，报纸与原逻辑一致，但根据该功能，此处用不上，为严谨，一起修改。。by gzh
    	String tSQL = "select agentcode from laagent where groupagentcode = '"+tAgentcode[i]+"' ";
    	String tYAgentCode = new ExeSQL().getOneValue(tSQL);
    	System.out.println("原AgentCode工号："+tYAgentCode);
    	mLAWageSchema.setAgentCode(tYAgentCode);
     	mLAWageSchema.setManageCom(tManagecom[i]);
//     	mLAWageSchema.setSumMoney(tSummoney[i]);
        mLAWageSchema.setBranchType(request.getParameter("BranchType"));
        mLAWageSchema.setBranchType2(request.getParameter("BranchType2"));    	
//     	mLAWageSchema.setState(tState[i]);
     	System.out.println("save indexcalno"+mLAWageSchema.getIndexCalNo());
     	mLAWageSet.add(mLAWageSchema);
     	flag="1";
      }
    }
  }
 

  VData tVData = new VData();
  FlagStr="";
        tVData.add(tG);
        tVData.addElement(mLAWageSchema);
        tVData.addElement(mLAAgentSchema);
        tVData.addElement(mLAWageSet);
        tVData.addElement(flag);
  
  try
  {
    System.out.println("begin to submit UI");
    mLAWageConfGatherUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAWageConfGatherUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>