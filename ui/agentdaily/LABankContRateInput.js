//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //initArchieveGrid();
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("LAContFYCRateInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  if(!verifyInput())
 {
 	return false;
}
 
    var tSel = ArchieveGrid.mulLineCount;		
  if( tSel == 0 || tSel == null )
  {
    alert("请先查询出保单信息!");
    return false;
  }
 
  // 循环处理
  for(var i=0;i<tSel;i++)
  {
  	// 判断是否被选择
    if(ArchieveGrid.getChkNo(i))
    {
       var tRate = ArchieveGrid.getRowColData(i,6);
        
       if (tRate == null || tRate =='')
       {
       	 alert("手续费比例不能为空！");
       	  return false ; 
       	}
       if (!isNumeric(tRate) )
       {
       	  alert("手续费比例应该为数字！");
       	  return false ;  
       }	
       if(tRate<=0 || tRate>=1)
       {
          alert("手续费比例应该大于0小于1！");
       	  return false ;       	
       } 
      
       if(tRate.length>8)
       {
          alert("手续费比例长度不能大于8！");
       	  return false ;       	
       }   
    }
  } 
  return true ;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  if(!beforeSubmit())
  {
    return false;
  }
	
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
   var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	  if(!beforeSubmit())
  {
    return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}                    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if(!beforeSubmit())
  {
    return false;
  }  
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//交验是否有此人员代码
function getname()
{
 var strsql="select name from laagent where  1=1 "
            + getWherePart('AgentCode','AgentCode')
            + getWherePart('BranchType','BranchType') ;
	  //团险中介和直销都可以，所以没有 branchtype2
 var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
 //alert(strsql);
     if (!strQueryResult)
  {
    alert("没有此代理人！");

    fm.all('AgentCode').value  = '';
    fm.all('AgentCodeName').value  = '';
    return;
  }
   //查询成功则拆分字符串，返回二维数组
   
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);	    
  fm.all('AgentCodeName').value=tArr[0][0];
}

//交验是否有此人名
function getcode()
{
 var strsql="select agentcode from laagent where  1=1 "
            + getWherePart('Name','AgentCodeName')
            + getWherePart('BranchType','BranchType')	;
	  //团险中介和直销都可以，所以没有 branchtype2
 var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
 //alert(strsql);
     if (!strQueryResult)
  {
    alert("没有此代理人！");

    fm.all('AgentCode').value  = '';
    fm.all('AgentCodeName').value  = '';
    return;
  }
   //查询成功则拆分字符串，返回二维数组
 
}



function easyQueryClick() {    
  if (verifyInput() == false)
  return false;	
  initArchieveGrid();
   var tReturn = getManageComLimitlike("a.ManageCom");
	//此处书写SQL语句	
	         		     			    
  var strSql = "select distinct a.agentcom,d.name,a.grpcontno,a.riskcode,"
              + "a.sumactupaymoney,(select  rate from laconrate b where b.grppolno=a.grppolno),"
              + "c.name,a.appntno,a.makedate,a.grppolno ,b.salechnl,b.salechnldetail"
              + " from  ljapaygrp a,lcgrppol b ,laagent c,lacom  d where 1=1   and  a.grppolno=b.grppolno "
              + " and a.agentcode=c.agentcode and a.agentcom=d.agentcom and b.salechnl='03' "
              + " and a.agentcom is not null      "
	      +tReturn   
    + getWherePart('a.ManageCom', 'ManageCom')	
    + getWherePart('a.GrpContNo', 'GrpContNo')	 
    + getWherePart('a.AgentCode', 'AgentCode')	
    + getWherePart('c.Name', 'AgentCodeName')
    + getWherePart('a.AgentCom', 'AgentCom')
    + getWherePart('d.Name', 'AgentComName')	    
    +" order by a.agentcom,a.grpcontno  with ur " ;
    turnPage.strQueryResult = easyQueryVer3(strSql, 1, 1, 1);
  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      alert("没有满足条件的信息!");
  
     return false;
    }     
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult); 
    turnPage.pageDisplayGrid = ArchieveGrid;              
    //保存SQL语句
    turnPage.strQuerySql     = strSql;   
    //设置查询起始位置
    turnPage.pageIndex       = 0;    
    //在查询结果数组中取出符合页面显示大小设置的数组
    var tArr = new Array();
    tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
     
     for(var i=0;i<tArr.length;i++)
    {
    var tRate=tArr[i][5];
    if (tRate==null || tRate=='null')
    {
    	tArr[i][5]='';
     }
    
    }
    //调用MULTILINE对象显示查询结果  
    displayMultiline(tArr, turnPage.pageDisplayGrid);    
    
 
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ArchieveGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = ArchieveGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}             
