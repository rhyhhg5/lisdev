<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAPlanQueryInput.jsp
//程序功能：
//创建日期：2003-07-08 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAMettingAssessQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAMettingAssessQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
	<%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>会议信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAPlanQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPlan1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAPlan1" style= "display: ''">
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  会议代码 
		</td>
        <td  class= input> 
		  <input class= common name=SeriesNo > 
		</td>
        <td  class= title> 
		  会议目的
		</td>
        <td  class= input> 
		  <input class=common name=MettingAim > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  主办机构 
		</td>
        <td  class= input> 
		  <input class=common name=AgentGroup > 
		</td>
        <td  class= title> 
		  会议形式 
		</td>
        <td  class= input> 
		  <input class=common name=MettingForm > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  重要性 
		</td>
        <td  class= input> 
		  <input class=common name=Import > 
		</td>
        <td  class= title> 
		  会议地点 
		</td>
        <td  class= input> 
		  <input class=common name=Address > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  费用
		</td>
        <td  class= input> 
		  <input class= common name=PayMoney > 
		</td>
        <td  class= title> 
		  会议时间
		</td>
        <td  class= input> 
		  <input name=DateTime class='coolDatePicker' dateFormat='short' > 
		</td>
      </tr>
      </table>
          <input type=hidden name=BranchType value=''>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();"  class="cssButton"> 
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();"  class="cssButton"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPlanGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPlanGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPlanGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"  class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"  class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"  class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"  class="cssButton"> 				
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
