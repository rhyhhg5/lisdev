<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
//程序名称：LADataGatherInput.jsp
//程序功能：当前没有主管的销售单位查询
//创建日期：2005-5-27 11:26
//创建人  ： LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="LADataGatherInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LADataGatherInit.jsp"%>
  <title> 数据汇总 </title>
</head>
<body onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./LADataGatherSave.jsp">
    <!-- 集体信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			  <td class= titleImg align= center>请输入查询条件：</td>
		  </tr>
	  </table>
    <table  class= common align=center>
      <TR  class= common>
        <!--TD  class= title>
          管理机构
        </TD>
        <TD  class= input>
          <Input class="code" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
        </TD-->
        <TD class = title>
           管理机构
        </TD>
        <TD  class= input>
          <Input class="codeno" name=ManageCom 
           ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);"
           onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);"
           verify="管理机构|notnull&code:comcode" ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
        </TD>
        <TD  class= title>
          回退天数
        </TD>
        <TD  class= input>
          <Input name=DayNum class='common' elementtype=nacessary verify="回退天数|notnull">
        </TD>
        <!--TD  class= title>
          展业类型
        </TD>
        <TD  class= input>
          <Input class='code' name=BranchType elementtype=nacessary verify="展业类型|code:BranchType&notnull" ondblclick="return showCodeList('BranchType',[this]);">
        </TD-->

      </TR>
      <!--TR  class= common>
        <!--TD  class= title>
          渠道类型
        </TD>
        <TD  class= input>
          <Input class='code' name=BranchType2 elementtype=nacessary verify="渠道类型|code&notnull" ondblclick="return showCodeList('BranchType2',[this],null,null,fm.all('BranchType').value,'codealias',1);">
        </TD-->
        <!--TD class = title>
           展业类型
        </TD>
        <TD  class= input>
          <Input class='codeno' name='BranchType'
           ondblclick="return showCodeList('BranchType',[this,BranchTypeName],[0,1]);" verify="展业类型|code:BranchType" 
           ><Input class=codename name=BranchTypeName readOnly elementtype=nacessary>
        </TD>        
        <TD class = title>
           渠道类型
        </TD>
        <TD  class= input>
          <Input class='codeno' name='BranchType2' 
           ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,fm.all('BranchType').value,'codealias',1);" verify="渠道类型|code" 
           ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
        </TD>

      </TR-->
    </table>
    <INPUT VALUE="查  询" class=cssButton  TYPE=button onclick="easyQueryClick();">
    <INPUT VALUE="回  退" class=cssButton  TYPE=button onclick="ApplyInput();">
    
    <!-- 数据汇总（列表） -->
    <table>
    	<tr>
      	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSpace);">
    		</td>
    		<td class= titleImg>
    			 单位列表
    		</td>
    	</tr>
    </table>
    <Div  id= "divSpace" style= "display: ''" align = center>
      <table  class= common>
        <tr  class= common>
    	    <td text-align: left colSpan=1 >
					  <span id="spanDataGatherGrid" ></span> 
			    </td>
			  </tr>
		  </table>
		
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();">
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();">
    </Div>
        <input type=hidden name=BranchType value=''> 
    <input type=hidden name=BranchType2 value=''>
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>