/** 
 * 程序名称：LAAddressInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-20 18:07:04
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

 function changeGroup()
{
	var strSQL = "";
 var tBranchAttr=fm.all('BranchAttr').value;
 alert(12);
   if (getWherePart('BranchAttr')!='')
  {	
     strSQL = "select AgentGroup,branchmanager,branchmanagername from LAbranchgroup where 1=1  "
     + getWherePart('BranchType','BranchType')
       + getWherePart('BranchType2','BranchType2')
	     + getWherePart('BranchAttr','BranchAttr');
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此组别！");
    fm.all('BranchAttr').value="";
    fm.all('AgentGroup').value = "";
  //  fm.all('BranchManager').value = "";
  //   fm.all('BranchManagername').value = "";
    
    return;
  }
  }
 
else
	{
		fm.all('BranchAttr').value="";
    fm.all('AgentGroup').value = "";
 //    fm.all('BranchManager').value = "";
 //    fm.all('BranchManagername').value = "";
     return;
  }
  //alert("4");
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);

  fm.all('AgentGroup').value  =tArr[0][0];
  // fm.all('BranchManager').value  =tArr[0][1];
 //  fm.all('BranchManagername').value  =tArr[0][2];
  
//  alert(fm.all('AgentGroup').value);
 
	}


// 查询按钮
function easyQueryClick() {
	
		// 初始化表格
	  initALACaseGrid()
  //alert(document.fm.AgentPutCase.value);
 	var strSql = "select AgentPutCase,CaseDate,CaseHappenDate,State,AgentEndCase,DirectLoss,ReplevyLoss,CaseNo,AppealNo from laCaseinfo where 1=1" 	 
   +  getWherePart("a.CaseNo", "CaseNo")
    + getWherePart("CaseDate", "CaseDate")
    + getWherePart("CaseHappenDate", "CaseHappenDate")
    + getWherePart("AgentPutCase","AgentPutCase")    
    + getWherePart("State", "StateCode")
    + getWherePart("AgentEndCase","AgentEndCase")
    + getWherePart("DirectLoss","DirectLoss",'','1')
      + getWherePart("ReplevyLoss", "ReplevyLoss",'','1')
    + getWherePart("AgentEndTel","AgentEndTel")
//      + getWherePart("DeductCent", "DeductCent")
    + getWherePart("EndCaseUnit","EndCaseManUnit")
    
    //+ getWherePart("AppealNo","AppealNo")
    + getWherePart("BranchType","BranchType")
    + getWherePart("BranchType2","BranchType2")
       ;
   //    alert(strSql);
    //   alert(1);	 
	turnPage.queryModal(strSql, ALACaseGrid); 
//	alert(strSql);
}
function showOne(parm1, parm2) {	

}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = ALACaseGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				 
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ALACaseGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	//'"+HolsGrid.getRowColData(tRow-1,1)+"'
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	arrSelected = new Array();
	
	
	var strSQL="select AgentPutCase,CaseDate,CaseHappenDate,State,AgentEndCase,DirectLoss,ReplevyLoss,"
	            +"DeDuctCent,CaseNo,AppealNo,EndCaseUnit,AgentEndTel,DealMind from laCaseinfo where 1=1 "
	            +" and CaseNo='"+ALACaseGrid.getRowColData(tRow-1,8)+"' "
	            + getWherePart("BranchType","BranchType")
                    + getWherePart("BranchType2","BranchType2");
        //            alert(strSQL);
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
        arrSelected = decodeEasyQueryResult(strQueryResult);
   
        return arrSelected;
	
}
