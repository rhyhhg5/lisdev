<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAPlanAgentSave.jsp
//程序功能：
//创建日期：2005-12-14 14:16
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAPlanUnitSchema tLAPlanUnitSchema = new LAPlanUnitSchema();
  LAPlanUnitSchema tLAPlanUnitSchema1 = new LAPlanUnitSchema();
  LAPlanUnitSet tLAPlanUnitSet = new LAPlanUnitSet();
  LAPlanUnitUI tLAPlanUnitUI = new LAPlanUnitUI();

  //输出参数
  CErrors tError = null;
  String tOperate = request.getParameter("Operate");
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	System.out.println("Save页面处理开始...");
	
  // 收集页面数据
  // 基础信息
  tLAPlanUnitSchema.setPlanType("0");
  tLAPlanUnitSchema.setPlanObject(request.getParameter("ComCode"));
  tLAPlanUnitSchema.setPlanPeriodUnit(request.getParameter("PlanPeriodUnit"));
  tLAPlanUnitSchema.setPlanStartYM(request.getParameter("DateStartYM"));
  tLAPlanUnitSchema.setManageCom(request.getParameter("ManageCom"));
  tLAPlanUnitSchema.setBranchType(request.getParameter("BranchType"));
  tLAPlanUnitSchema.setBranchType2(request.getParameter("BranchType2"));
  
  // 计划项目及  计划数据
  tLAPlanUnitSchema1 = new LAPlanUnitSchema();
  tLAPlanUnitSchema1.setPlanItemType("01");       // 保费计划
  tLAPlanUnitSchema1.setPlanValue(request.getParameter("Money"));
  tLAPlanUnitSchema1.setPlanValue2(request.getParameter("Other"));
  tLAPlanUnitSchema1.setPlanMark(request.getParameter("Mark"));
  tLAPlanUnitSet.add(tLAPlanUnitSchema1);
  
  tLAPlanUnitSchema1 = new LAPlanUnitSchema();
  tLAPlanUnitSchema1.setPlanItemType("02");       // 标准保费
  if(request.getParameter("TransMoney")==null||request.getParameter("TransMoney").equals(""))
  tLAPlanUnitSchema1.setPlanValue("0");
  else
  tLAPlanUnitSchema1.setPlanValue(request.getParameter("TransMoney"));
  tLAPlanUnitSchema1.setPlanValue2("");
  tLAPlanUnitSchema1.setPlanMark("");
  tLAPlanUnitSet.add(tLAPlanUnitSchema1);
  
  tLAPlanUnitSchema1 = new LAPlanUnitSchema();
  tLAPlanUnitSchema1.setPlanItemType("03");       // 人力
  if(request.getParameter("AgentCont")==null||request.getParameter("AgentCont").equals(""))
  tLAPlanUnitSchema1.setPlanValue("0");
  else
  tLAPlanUnitSchema1.setPlanValue(request.getParameter("AgentCont"));
  tLAPlanUnitSchema1.setPlanValue2("");
  tLAPlanUnitSchema1.setPlanMark("");
  tLAPlanUnitSet.add(tLAPlanUnitSchema1);
  
  tLAPlanUnitSchema1 = new LAPlanUnitSchema();
  tLAPlanUnitSchema1.setPlanItemType("04");       // 计划客户数
  if(request.getParameter("AppCont")==null||request.getParameter("AppCont").equals(""))
  tLAPlanUnitSchema1.setPlanValue("0");
  else
  tLAPlanUnitSchema1.setPlanValue(request.getParameter("AppCont"));
  tLAPlanUnitSchema1.setPlanValue2("");
  tLAPlanUnitSchema1.setPlanMark("");
  tLAPlanUnitSet.add(tLAPlanUnitSchema1);
  
  tLAPlanUnitSchema1 = new LAPlanUnitSchema();
  tLAPlanUnitSchema1.setPlanItemType("05");       // 活动率( ％)
  if(request.getParameter("ActRate")==null||request.getParameter("ActRate").equals(""))
  tLAPlanUnitSchema1.setPlanValue("0");
  else
  tLAPlanUnitSchema1.setPlanValue(request.getParameter("ActRate"));
  tLAPlanUnitSchema1.setPlanValue2("");
  tLAPlanUnitSchema1.setPlanMark("");
  tLAPlanUnitSet.add(tLAPlanUnitSchema1);
  
  tLAPlanUnitSchema1 = new LAPlanUnitSchema();
  tLAPlanUnitSchema1.setPlanItemType("06");       // 年继续率( ％)
  if(request.getParameter("YearKeepRate")==null||request.getParameter("YearKeepRate").equals(""))
  tLAPlanUnitSchema1.setPlanValue("0");
  else
  tLAPlanUnitSchema1.setPlanValue(request.getParameter("YearKeepRate"));
  tLAPlanUnitSchema1.setPlanValue2("");
  tLAPlanUnitSchema1.setPlanMark("");
  tLAPlanUnitSet.add(tLAPlanUnitSchema1);
	
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.add(tLAPlanUnitSchema);
	tVData.add(tLAPlanUnitSet);
  System.out.println("add over");
  
  try
  {
    tLAPlanUnitUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAPlanUnitUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

