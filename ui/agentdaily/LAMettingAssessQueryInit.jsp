<%
//程序名称：LAMettingAssessQueryInit.jsp
//程序功能：
//创建日期：2003-07-08 14:14:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('MettingAim').value = '';    
    fm.all('SeriesNo').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('PayMoney').value = '';
    fm.all('MettingForm').value = '';
    fm.all('Import').value = '';
    fm.all('DateTime').value = '';
    fm.all('Address').value = '';
  
   
   
  }
  catch(ex)
  {
    alert("在LAPlanQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAPlanQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initPlanGrid();
  }
  catch(re)
  {
    alert("LAPlanQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化PlanGrid
 ************************************************************
 */
function initPlanGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="会议代码";         //列名
        iArray[1][1]="45px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="会议目的";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="主办机构";         //列名
        iArray[3][1]="60px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="会议形式";         //列名
        iArray[4][1]="50px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="费用";         //列名
        iArray[5][1]="30px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="重要性";         //列名
        iArray[6][1]="30px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[7]=new Array();
        iArray[7][0]="会议时间";         //列名
        iArray[7][1]="40px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="会议地点";         //列名
        iArray[8][1]="100px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
  
        PlanGrid = new MulLineEnter( "fm" , "PlanGrid" ); 

        //这些属性必须在loadMulLine前
        PlanGrid.mulLineCount = 0;   
        PlanGrid.displayTitle = 1;
        PlanGrid.locked=1;
        PlanGrid.canSel=1;
        PlanGrid.canChk=0;
        PlanGrid.hiddenPlus = 1;
      	PlanGrid.hiddenSubtraction = 1;
        PlanGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化PlanGrid时出错："+ ex);
      }
    }


</script>