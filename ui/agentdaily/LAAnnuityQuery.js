/** 
 * 程序名称：LAAnnuityQuery.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-7-8 14:17
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function easyQueryClick()
{
  // 初始化表格
  initAnnuityGrid();
  
  //得到查询条件
  var tEmployDateF = document.fm.EmployDateF.value;
  var tEmployDateT = document.fm.EmployDateT.value;
  var tOutWorkDateF = document.fm.OutWorkDateF.value;
  var tOutWorkDateT = document.fm.OutWorkDateT.value;
  var tDrawMarker = document.fm.DrawMarker.value;
  var tWherePrama = "";
  
  //设置日期区间条件
  if(tEmployDateF.length != 0)
  {
  	tWherePrama += " and '"+tEmployDateF+"' <= b.EmployDate";
  }
  if(tEmployDateT.length != 0)
  {
  	tWherePrama += " and b.EmployDate <= '"+tEmployDateT+"'";
  }
  if(tOutWorkDateF.length != 0)
  {
  	tWherePrama += " and '"+tOutWorkDateF+"' <= b.OutWorkDate";
  }
  if(tOutWorkDateT.length != 0)
  {
  	tWherePrama += " and b.OutWorkDate<= '"+tOutWorkDateT+"'";
  }
  
  //查询语句
  var tSql  = "";
  var tSql1 = "";
  var tSql2 = "";
  var tSql3 = "";
  /*
	tSql1  = " select b.AgentCode,";
	tSql1 += "        b.Name,";
	tSql1 += "        c.BranchAttr,";
	tSql1 += "        b.ManageCom,";
  tSql1 += "        b.EmployDate,";
	tSql1 += "        b.OutWorkDate,";
	tSql1 += "        Max(d.IndexCalNo),";
	tSql1 += "        (select bb.K15";
	tSql1 += "           from LAWage bb";
	tSql1 += "          where bb.IndexCalNo = Max(d.IndexCalNo)";
	tSql1 += "            and bb.AgentCode = b.AgentCode";
	tSql1 += "        ) as maxannuity,";
	tSql1 += "        (select decimal(aa.DrawRate / 100,12,2)";
	tSql1 += "           from LAWageRadix2 aa";
	tSql1 += "          where aa.WageCode = 'WP0025'";
	tSql1 += "            and aa.AgentGrade = e.AgentGrade";
	tSql1 += "        ) as DrawRate,";
	tSql1 += "        decimal(sum(d.K15),12,2) as sumannuity,";
	tSql1 += "        e.AgentGrade,";
	tSql1 += "        (CASE ";
	tSql1 += "           WHEN (select count(*) from LAAnnuity where agentcode=b.AgentCode) = 0 THEN '未领取'";
	tSql1 += "           ELSE '已领取'";
	tSql1 += "         END";
	tSql1 += "        ) as aaa,";
	tSql1 += "        f.SeriesNo";
	tSql1 += "   from LAWage a,";
	tSql1 += "        LAAgent b,";
	tSql1 += "        LABranchGroup c,";
	tSql1 += "        LAWage d,";
	tSql1 += "        LATree e,";
	tSql1 += "        LAAnnuity f";
	tSql1 += " where a.AgentCode = b.AgentCode";
	tSql1 += "    and b.AgentGroup = c.AgentGroup";
	tSql1 += "    and b.AgentCode = d.AgentCode";
	tSql1 += "    and b.AgentCode = e.AgentCode";
	tSql1 += "    and a.AgentCode = f.AgentCode";
	//tSql1 += "    and b.AgentCode = '1101000003'";
	tSql1 += getWherePart("b.AgentCode", "AgentCode","Like");
	//tSql1 += "    and b.ManageCom = '86110000'";
	tSql1 += getWherePart("b.ManageCom", "ManageCom","Like");
	//tSql1 += "    and c.BranchAttr = '86110000010101'";
	tSql1 += getWherePart("c.BranchAttr", "BranchAttr","Like");
	//tSql1 += "    and e.AgentGrade = 'B04'";
	tSql1 += getWherePart("e.AgentGrade", "AgentGrade","Like");
	//tSql1 += "    and '2005-01-01' <= b.EmployDate";
	//tSql1 += "    and b.EmployDate <= '2005-12-31'";
	//tSql1 += "    and '2005-01-01' <= b.OutWorkDate";
	//tSql1 += "    and b.OutWorkDate<= '2005-12-31'";
	tSql1 += tWherePrama;
	tSql1 += "  group by";
	tSql1 += "       b.AgentCode,";
	tSql1 += "       b.Name,";
	tSql1 += "       c.BranchAttr,";
	tSql1 += "       b.ManageCom,";
	tSql1 += "       b.EmployDate,";
	tSql1 += "       e.AgentGrade,";
	tSql1 += "       f.SeriesNo";
	
	tSql2 += " select b.AgentCode,";
	tSql2 += "        b.Name,";
	tSql2 += "        c.BranchAttr,";
	tSql2 += "        b.ManageCom,";
	tSql2 += "        b.EmployDate,";
	tSql2 += "        b.OutWorkDate,";
	tSql2 += "        Max(d.IndexCalNo),";
	tSql2 += "        (select bb.K15";
	tSql2 += "           from LAWage bb";
	tSql2 += "          where bb.IndexCalNo = Max(d.IndexCalNo)";
	tSql2 += "            and bb.AgentCode = b.AgentCode";
	tSql2 += "        ) as maxannuity,";
	tSql2 += "        (select decimal(aa.DrawRate / 100,12,2)";
	tSql2 += "           from LAWageRadix2 aa";
	tSql2 += "          where aa.WageCode = 'WP0025'";
	tSql2 += "            and aa.AgentGrade = e.AgentGrade";
	tSql2 += "        ) as DrawRate,";
	tSql2 += "        decimal(sum(d.K15),12,2) as sumannuity,";
	tSql2 += "        e.AgentGrade,";
	tSql2 += "        (CASE ";
	tSql2 += "           WHEN (select count(*) from LAAnnuity where agentcode=b.AgentCode) = 0 THEN '未领取'";
	tSql2 += "           ELSE '已领取'";
	tSql2 += "         END";
	tSql2 += "        ) as aaa,";
	tSql2 += "        ''";
	tSql2 += "   from LAWage a,";
	tSql2 += "        LAAgent b,";
	tSql2 += "        LABranchGroup c,";
	tSql2 += "        LAWage d,";
	tSql2 += "        LATree e";
	tSql2 += " where a.AgentCode = b.AgentCode";
	tSql2 += "       and b.AgentGroup = c.AgentGroup";
	tSql2 += "       and b.AgentCode = d.AgentCode";
	tSql2 += "       and b.AgentCode = e.AgentCode";
	//tSql2 += "       and b.AgentCode = '1101000004'";
	tSql2 += getWherePart("b.AgentCode", "AgentCode","Like");
	//tSql2 += "       and b.ManageCom = '86110000'";
	tSql2 += getWherePart("b.ManageCom", "ManageCom","Like");
	//tSql2 += "       and c.BranchAttr = '86110000010101'";
	tSql2 += getWherePart("c.BranchAttr", "BranchAttr","Like");
	//tSql2 += "       and e.AgentGrade = 'B04'";
	tSql2 += getWherePart("e.AgentGrade", "AgentGrade","Like");
	//tSql2 += "       and '2005-01-01' <= b.EmployDate";
	//tSql2 += "       and b.EmployDate <= '2005-12-31'";
	//tSql2 += "       and '2005-01-01' <= b.OutWorkDate";
	//tSql2 += "       and b.OutWorkDate<= '2005-12-31'";
	tSql2 += tWherePrama;
	tSql2 += "       and a.AgentCode not in (select AgentCode from LAAnnuity)";
	tSql2 += "  group by";
	tSql2 += "       b.AgentCode,";
	tSql2 += "       b.Name,";
	tSql2 += "       c.BranchAttr,";
	tSql2 += "       b.ManageCom,";
	tSql2 += "       b.EmployDate,";
	tSql2 += "       e.AgentGrade";
	
	tSql3  = " select * ";
	tSql3 += "  from ( ";
	tSql3 += tSql1 +"  union all " + tSql2 + " )TempTable ";
	tSql3 += "  order by ";
	tSql3 += "    TempTable.AgentCode ";
	
	if (tDrawMarker == "0")
	{
		tSql = tSql2;
	}else if (tDrawMarker == "1")
	{
		tSql = tSql1;
	}
	else 
	{
		tSql = tSql3;
	}
	*/
	//document.fm.testtxt.value = tSql3;
	
	tSql  = "select a.AgentCode,b.Name,c.BranchAttr,c.Name,a.ManageCom,b.OutWorkDate,a.PayDate,";
	tSql += "       a.DealAnnuity,a.DrawAnnuity,case";
	tSql += "          when a.DrawMarker = '1' then '已领取'";
	tSql += "          when a.DrawMarker = '0' then '未领取'";
	tSql += "          else '无记录'";
	tSql += "       end,a.DrawMarker,a.ScaleAnnuity,a.SeriesNo,a.SumAnnutity";
	tSql += "  from LAAnnuity a,";
	tSql += "       LAAgent b,";
	tSql += "       LABranchGroup c";
	tSql += " where a.AgentCode=b.AgentCode";
	tSql += "   and b.AgentGroup=c.AgentGroup";
	tSql += getWherePart("a.AgentCode", "AgentCode");
	tSql += getWherePart("a.ManageCom", "ManageCom");
	tSql += getWherePart("c.BranchAttr", "BranchAttr");
	tSql += getWherePart("a.DrawMarker", "DrawMarker");
	tSql += tWherePrama;

  turnPage.queryModal(tSql, AnnuityGrid);
}

//返回操作
function returnParent()
{
  var arrReturn = new Array();
	var tSel = AnnuityGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{	
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

//取得选中的数据
function getQueryResult()
{
	var arrSelected = null;
	tRow = AnnuityGrid.getSelNo();

	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = AnnuityGrid.getRowData(tRow-1);
	
	return arrSelected;
}