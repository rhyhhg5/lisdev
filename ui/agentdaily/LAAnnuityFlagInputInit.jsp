<%
//程序名称：LAAddressQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-20 18:07:04
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
   fm.all('ManageCom').value = '';
   fm.all('ManageComName').value = "";
   fm.all('Flag').value = "";
   fm.all('FlagName').value = "";
   fm.all('AgentCode').value = "";
   fm.all('AgentName').value = "";
   fm.all('BranchAttr').value = "";
   fm.all('BranchName').value = "";
   
   fm.all('BranchType').value = '<%=BranchType%>';
   fm.all('BranchType2').value = '<%=BranchType2%>';
  }
  catch(ex) {
    alert("在LAAddressQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
//    initLAAddressGrid();  
  }
  catch(re) {
    alert("LAAddressQueryInit11.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LAAddressGrid;
function initLAAddressGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构";         		//列名
    iArray[1][1]="100px";         		//宽度
    iArray[1][2]=100;         		//最大长度
    iArray[1][3]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="销售员代码";         		//列名
    iArray[2][1]="150px";         		//宽度
    iArray[2][2]=100;         		//最大长度
    iArray[2][3]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="销售员姓名";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][2]=100;         		//最大长度
    iArray[3][3]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="展业机构代码";         		//列名
    iArray[4][1]="80px";         		//宽度
    iArray[4][2]=200;         		//最大长度
    iArray[4][3]="0";         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array(); 
    iArray[5][0]="展业机构名称";   //列名
    iArray[5][1]="80px"        //宽度
    iArray[5][2]=200;        //最大长度
    iArray[5][3]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();  
    iArray[6][0]="养老金领取标志";         		//列名 
    iArray[6][1]="50px"        //宽度 
    iArray[6][2]=200;        //最大长度 
    iArray[6][3]=2;  //是否允许录入，0--不能，1--允许
    iArray[6][4]='yesno'; 
    
  
                                                                                                                  
    LAAddressGrid = new MulLineEnter( "fm" , "LAAddressGrid" ); 
    //这些属性必须在loadMulLine前
 
    LAAddressGrid.mulLineCount = 1;   
    LAAddressGrid.displayTitle = 1;
    LAAddressGrid.hiddenPlus = 1;
    LAAddressGrid.hiddenSubtraction = 1;
    LAAddressGrid.canSel = 1;
    LAAddressGrid.canChk = 0;
    LAAddressGrid.selBoxEventFuncName = "showOne";
 
    LAAddressGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAAddressGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
