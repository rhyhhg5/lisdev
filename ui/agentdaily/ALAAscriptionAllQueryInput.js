//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	 
   if (verifyInput() == false)
    return false;
   
   	 
    if( !beforeSubmit())
   {
   	return false;
   	
   	}	

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function beforeSubmit()
{
 
	return true;
	
}


function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function easyQueryClick() 
{
　if (verifyInput() == false)
    return false;
  //此处书写SQL语句			     
  var tManageCom=getManageComLimit();
   var tsql1="";
   var tsql2="";
   if(fm.all('ContNo').value!=""&&fm.all('ContNo').value!=null&&fm.all('GrpContNo').value!=""&&fm.all('GrpContNo').value!=null)
 	{
 	}
 	else {
 	if(fm.all('ContNo').value!=""&&fm.all('ContNo').value!=null)
 	{
 	tsql2=" and 1=2 ";
 	}
 	if(fm.all('GrpContNo').value!=""&&fm.all('GrpContNo').value!=null)
 	{
 	tsql1=" and 1=2 ";
 	}
 	}
  
  var strSql = "";
	
    strSql = "select distinct '',a.contno,b.appntname,"
    + " c.name,getUniteCode(a.agentold),a.branchattr,"
    +" (select name from laagent where agentcode=a.agentnew),getUniteCode(a.agentnew),"
    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select agentgroup from laagent where agentcode=a.agentnew))h, "
    +" a.makedate,case a.ascripstate when '1' then '自动归属失败' when '2' then '未确认' when '3' then '已确认' else '未知类型' end,"
    +"case a.maketype when '01' then '手工分配' when '02' then '孤儿单自动归属' when '09' then '保单业务员变更' else '保单分配' end, "
    +"case when e.postaladdress is null then e.homeaddress else e.postaladdress end,"
    +"case when e.phone is not null then e.phone when e.mobile is not null then e.mobile"
    +" when e.companyphone is not null then e.companyphone else e.homephone end "
    + " from laascription a ,lccont b,lcappnt d,lcaddress e,laagent c "
    +"where b.contno=a.contno  and a.agentold=c.agentcode and b.contno=d.contno "
    +"and b.appntno=d.appntno and d.appntno=e.customerno and d.addressno=e.addressno "
    +"and a.validflag='N'  and a.branchtype='1' and a.branchtype2='01' and  a.grpcontno is null "
      + getWherePart('a.ManageCom','ManageCom','like')
//	  + getWherePart('a.AgentOld','AgentCode')
//	  + getWherePart('a.AgentNew','AgentNew')
	  + getWherePart('c.Name','AgentName')
	  + getWherePart('a.ContNo','ContNo')
	  + getWherePart('a.BranchAttr','BranchAttr')
	  + getWherePart('a.makedate','MakeDate')
	   + tsql1;
//  modify lyc 2014-11-27 统一工号
    if(fm.all("AgentCode").value!=""){
    	strSql +=" and a.AgentOld = getAgentCode('"+fm.AgentCode.value+"')";
    }
    if(fm.all("AgentNew").value!=""){
    	strSql +=" and a.AgentNew = getAgentCode('"+fm.AgentNew.value+"')";
    }
	  if(fm.all('AgentNewName').value!=null && fm.all('AgentNewName').value!=''){
	  	strSql=strSql+" and a.agentnew in (select agentcode from laagent where branchtype='1' and branchtype2='01' and name='"
	  	+fm.all('AgentNewName').value+"')";
	  }
	  if(fm.all('StartDate').value!=null && fm.all('StartDate').value!=''){
	  	strSql=strSql+" and a.ascriptiondate>='"
	  	+fm.all('StartDate').value+"'";
	  }
	  if(fm.all('EndDate').value!=null && fm.all('EndDate').value!=''){
	  	strSql=strSql+" and a.ascriptiondate<='"
	  	+fm.all('EndDate').value+"'";
	  }
	  strSql=strSql+" and ascripno in (select ascripno from laascription where "
	  +"contno=a.contno and validflag='N' order by makedate desc,maketime desc,modifydate desc,modifytime desc fetch first 1 rows only) "
	  +" union "
	  + "select distinct a.grpcontno ,'',b.grpname,"
    + " c.name,getUniteCode(a.agentold),a.branchattr,"
    +" (select name from laagent where agentcode=a.agentnew),getUniteCode(a.agentnew),"
    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select agentgroup from laagent where agentcode=a.agentnew))h, "
    +" a.makedate,case a.ascripstate when '1' then '自动归属失败' when '2' then '未确认' when '3' then '已确认' else '未知类型' end,"
    +"case a.maketype when '01' then '手工分配' when '02' then '孤儿单自动归属' when '09' then '保单业务员变更' else '保单分配' end, "
    +"e.grpaddress ,"
    +"e.phone1 "
    + " from laascription a ,lcgrpcont b,lcgrpappnt d,lcgrpaddress e,laagent c "
    +"where b.grpcontno=a.grpcontno  and a.agentold=c.agentcode and b.grpcontno=d.grpcontno "
    +"and b.appntno=d.customerno and d.customerno=e.customerno and d.addressno=e.addressno "
    +"and a.validflag='N'  and a.branchtype='1' and a.branchtype2='01' and  a.contno is null "
      + getWherePart('a.ManageCom','ManageCom','like')
//	  + getWherePart('a.AgentOld','AgentCode')
//	  + getWherePart('a.AgentNew','AgentNew')
	  + getWherePart('c.Name','AgentName')
	  +  getWherePart('a.GrpContNo','GrpContNo')
	  + getWherePart('a.BranchAttr','BranchAttr')
	  + getWherePart('a.makedate','MakeDate')
	   + tsql2;
	//  modify lyc 2014-11-27 统一工号
	    if(fm.all("AgentCode").value!=""){
	    	strSql +=" and a.AgentOld = getAgentCode('"+fm.AgentCode.value+"')";
	    }
	    if(fm.all("AgentNew").value!=""){
	    	strSql +=" and a.AgentNew = getAgentCode('"+fm.AgentNew.value+"')";
	    }
	  if(fm.all('AgentNewName').value!=null && fm.all('AgentNewName').value!=''){
	  	strSql=strSql+" and a.agentnew in (select agentcode from laagent where branchtype='1' and branchtype2='01' and name='"
	  	+fm.all('AgentNewName').value+"')";
	  }
	  if(fm.all('StartDate').value!=null && fm.all('StartDate').value!=''){
	  	strSql=strSql+" and a.ascriptiondate>='"
	  	+fm.all('StartDate').value+"'";
	  }
	  if(fm.all('EndDate').value!=null && fm.all('EndDate').value!=''){
	  	strSql=strSql+" and a.ascriptiondate<='"
	  	+fm.all('EndDate').value+"'";
	  }
	  strSql=strSql+" and ascripno in (select ascripno from laascription where "
	  +"grpcontno=a.grpcontno and validflag='N' order by makedate desc,maketime desc,modifydate desc,modifytime desc fetch first 1 rows only) ";	  
	//  alert(strSql);
   turnPage.queryModal(strSql, LACrossGrid);
  if (!turnPage.strQueryResult) 
  {
    alert("没有查询到相应的保单分配记录！没有进行保单分配。");
    return false;
  }
}

function afterCodeSelect( cCodeName, Field )
{
　	  
  if(cCodeName=="comcode"){
  	 fm.all("BranchAttr").value="";
  	 fm.all("BranchName").value="";
  	if(fm.all("ManageCom").value=='86' )
  	{
  		fm.all("BranchAttr").disabled=true;
  	}
       else
       	{
       		var tManageCom=fm.all("ManageCom").value;
       		fm.all("BranchAttr").disabled=false;
       		msql1=" 1 and   branchtype=#2#  and branchtype2=#01#  and managecom like #"+tManageCom+"%# and endflag=#N#";
       	}
}
 if(cCodeName=="branchattr")
 {
   if(fm.all("ManageCom").value==null || fm.all("ManageCom").value=='')
    {
      alert("请先录入管理机构");
      fm.all("BranchAttr").value='';
       fm.all("BranchName").value='';
       return false;
  
     }
}
}        
function validContNo(){
	if(fm.all('ContNo').value==null || fm.all('ContNo').value==''){
		return true;
	}else{
		var tSQL="select 1 from lccont where contno ='"+fm.all('ContNo').value
		+"' union select 1 from lbcont where contno ='"+fm.all('ContNo').value+"'";
		var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(!strQueryResult){
			alert('无效的个单号！');
			fm.all('ContNo').value='';
			return false;
		}else{
			return true;
		}
	}
}  
function validGrpContNo(){
	if(fm.all('GrpContNo').value==null || fm.all('GrpContNo').value==''){
		return true;
	}else{
		var tSQL="select 1 from lcgrpcont where grpcontno ='"+fm.all('GrpContNo').value
		+"' union select 1 from lbgrpcont where grpcontno ='"+fm.all('GrpContNo').value+"'";
		var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(!strQueryResult){
			alert('无效的团单号！');
			fm.all('GrpContNo').value='';
			return false;
		}else{
			return true;
		}
	}
} 
function validAgentCode(){
	if(fm.all('AgentCode').value==null || fm.all('AgentCode').value==''){
		return true;
	}else{
		var tSQL="select 1 from laagent where agentcode =getAgentCode('"+fm.all('AgentCode').value+"')";
		var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(!strQueryResult){
			alert('无效的代理人编码！');
			fm.all('AgentCode').value='';
			return false;
		}else{
			return true;
		}
	}
}   
function validAgentName(){
	if(fm.all('AgentName').value==null || fm.all('AgentName').value==''){
		return true;
	}else{
		var tSQL="select distinct 1 from laagent where name ='"+fm.all('AgentName').value+"'";
		var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(!strQueryResult){
			alert('无效的代理人姓名！');
			fm.all('AgentName').value='';
			return false;
		}else{
			return true;
		}
	}
}   
function validAgentCode2(){
	if(fm.all('AgentNew').value==null || fm.all('AgentNew').value==''){
		return true;
	}else{
		var tSQL="select 1 from laagent where agentcode =getAgentCode('"+fm.all('AgentNew').value+"')";
		var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(!strQueryResult){
			alert('无效的代理人编码！');
			fm.all('AgentNew').value='';
			return false;
		}else{
			return true;
		}
	}
}   
function validAgentName2(){
	if(fm.all('AgentNewName').value==null || fm.all('AgentNewName').value==''){
		return true;
	}else{
		var tSQL="select distinct 1 from laagent where name ='"+fm.all('AgentNewName').value+"'";
		var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(!strQueryResult){
			alert('无效的代理人姓名！');
			fm.all('AgentNewName').value='';
			return false;
		}else{
			return true;
		}
	}
}
function validBranchattr(){
	if(fm.all('BranchAttr').value==null || fm.all('BranchAttr').value==''){
		return true;
	}else{
		var tSQL="select distinct 1 from labranchgroup where branchattr ='"+fm.all('BranchAttr').value
		+"' and branchtype='1' and branchtype2='01'";
		var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(!strQueryResult){
			alert('无效的原销售单位代码！');
			fm.all('BranchAttr').value='';
			return false;
		}else{
			return true;
		}
	}
}           
function doDownLoad(){
	if (verifyInput() == false)
    return false;
  //此处书写SQL语句			     
  var tsql1="";
   var tsql2="";
   if(fm.all('ContNo').value!=""&&fm.all('ContNo').value!=null&&fm.all('GrpContNo').value!=""&&fm.all('GrpContNo').value!=null)
 	{
 	}
 	else {
 	if(fm.all('ContNo').value!=""&&fm.all('ContNo').value!=null)
 	{
 	tsql2=" and 1=2 ";
 	}
 	if(fm.all('GrpContNo').value!=""&&fm.all('GrpContNo').value!=null)
 	{
 	tsql1=" and 1=2 ";
 	}
 	}
  
  var strSql = "";
	
    strSql = "select distinct '',a.contno,b.appntname,"
    + " c.name,getUniteCode(a.agentold),a.branchattr,"
    +" (select name from laagent where agentcode=a.agentnew),getUniteCode(a.agentnew),"
    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select agentgroup from laagent where agentcode=a.agentnew))h, "
    +" a.makedate,case a.ascripstate when '1' then '自动归属失败' when '2' then '未确认' when '3' then '已确认' else '未知类型' end,"
    +"case a.maketype when '01' then '手工分配' when '02' then '孤儿单自动归属' when '09' then '保单业务员变更' else '保单分配' end, "
    +"case when e.postaladdress is null then e.homeaddress else e.postaladdress end,"
    +"case when e.phone is not null then e.phone when e.mobile is not null then e.mobile"
    +" when e.companyphone is not null then e.companyphone else e.homephone end "
    + " from laascription a ,lccont b,lcappnt d,lcaddress e,laagent c "
    +"where b.contno=a.contno  and a.agentold=c.agentcode and b.contno=d.contno "
    +"and b.appntno=d.appntno and d.appntno=e.customerno and d.addressno=e.addressno "
    +"and a.validflag='N'  and a.branchtype='1' and a.branchtype2='01' and  a.grpcontno is null "
     // + getWherePart('a.ManageCom','ManageCom','like')
     +" and a.managecom like '"+fm.all('ManageCom').value +"%'"
//	  + getWherePart('a.AgentOld','AgentCode')
//	  + getWherePart('a.AgentNew','AgentNew')
	  + getWherePart('c.Name','AgentName')
	  + getWherePart('a.ContNo','ContNo')
	  + getWherePart('a.BranchAttr','BranchAttr')
	  + getWherePart('a.makedate','MakeDate')
	   + tsql1;
	//  modify lyc 2014-11-27 统一工号
    if(fm.all("AgentCode").value!=""){
    	strSql +=" and a.AgentOld = getAgentCode('"+fm.AgentCode.value+"')";
    }
    if(fm.all("AgentNew").value!=""){
    	strSql +=" and a.AgentNew = getAgentCode('"+fm.AgentNew.value+"')";
    }
	  if(fm.all('AgentNewName').value!=null && fm.all('AgentNewName').value!=''){
	  	strSql=strSql+" and a.agentnew in (select agentcode from laagent where branchtype='1' and branchtype2='01' and name='"
	  	+fm.all('AgentNewName').value+"')";
	  }
	  if(fm.all('StartDate').value!=null && fm.all('StartDate').value!=''){
	  	strSql=strSql+" and a.ascriptiondate>='"
	  	+fm.all('StartDate').value+"'";
	  }
	  if(fm.all('EndDate').value!=null && fm.all('EndDate').value!=''){
	  	strSql=strSql+" and a.ascriptiondate<='"
	  	+fm.all('EndDate').value+"'";
	  }
	  strSql=strSql+" and ascripno in (select ascripno from laascription where "
	  +"contno=a.contno and validflag='N' order by makedate desc,maketime desc,modifydate desc,modifytime desc fetch first 1 rows only) "
	  +" union "
	  + "select distinct a.grpcontno ,'',b.grpname,"
    + " c.name,getUniteCode(a.agentold),a.branchattr,"
    +" (select name from laagent where agentcode=a.agentnew),getUniteCode(a.agentnew),"
    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select agentgroup from laagent where agentcode=a.agentnew))h, "
    +" a.makedate,case a.ascripstate when '1' then '自动归属失败' when '2' then '未确认' when '3' then '已确认' else '未知类型' end,"
    +"case a.maketype when '01' then '手工分配' when '02' then '孤儿单自动归属' when '09' then '保单业务员变更' else '保单分配' end, "
    +"e.grpaddress ,"
    +"e.phone1 "
    + " from laascription a ,lcgrpcont b,lcgrpappnt d,lcgrpaddress e,laagent c "
    +"where b.grpcontno=a.grpcontno  and a.agentold=c.agentcode and b.grpcontno=d.grpcontno "
    +"and b.appntno=d.customerno and d.customerno=e.customerno and d.addressno=e.addressno "
    +"and a.validflag='N'  and a.branchtype='1' and a.branchtype2='01' and  a.contno is null "
      //+ getWherePart('a.ManageCom','ManageCom','like')
      +" and a.managecom like '"+fm.all('ManageCom').value +"%'  "
//	  + getWherePart('a.AgentOld','AgentCode')
//	  + getWherePart('a.AgentNew','AgentNew')
	  + getWherePart('c.Name','AgentName')
	  +  getWherePart('a.GrpContNo','GrpContNo')
	  + getWherePart('a.BranchAttr','BranchAttr')
	  + getWherePart('a.makedate','MakeDate')
	   + tsql2;
		//  modify lyc 2014-11-27 统一工号
	    if(fm.all("AgentCode").value!=""){
	    	strSql +=" and a.AgentOld = getAgentCode('"+fm.AgentCode.value+"')";
	    }
	    if(fm.all("AgentNew").value!=""){
	    	strSql +=" and a.AgentNew = getAgentCode('"+fm.AgentNew.value+"')";
	    }
	  if(fm.all('AgentNewName').value!=null && fm.all('AgentNewName').value!=''){
	  	strSql=strSql+" and a.agentnew in (select agentcode from laagent where branchtype='1' and branchtype2='01' and name='"
	  	+fm.all('AgentNewName').value+"')";
	  }
	  if(fm.all('StartDate').value!=null && fm.all('StartDate').value!=''){
	  	strSql=strSql+" and a.ascriptiondate>='"
	  	+fm.all('StartDate').value+"'";
	  }
	  if(fm.all('EndDate').value!=null && fm.all('EndDate').value!=''){
	  	strSql=strSql+" and a.ascriptiondate<='"
	  	+fm.all('EndDate').value+"'";
	  }
	  strSql=strSql+" and ascripno in (select ascripno from laascription where "
	  +"grpcontno=a.grpcontno and validflag='N' order by makedate desc,maketime desc,modifydate desc,modifytime desc fetch first 1 rows only) ";	  
  fm.all('querySQL').value=strSql;
  fm.action = "./ALAAscriptionAllReport.jsp";
  fm.submit();
}
                                                                                                      