<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAPlanManageInput.jsp
//程序功能：进行个险 年、季、月 计划的录入
//创建日期：2005-12-13 15:19
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<script> 
   var BranchType=<%=BranchType%>;
   var BranchType2=<%=BranchType2%>;
   var msql='1 and code in (#1#,#3#,#12#) ';
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
 <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
 <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAPlanManageInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAPlanManageInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAPlanManageSave.jsp" method=post name=fm target="fraSubmit">
    <table class="common" align=center>
		  <tr align=right>
			  <td class=button>
				  &nbsp;&nbsp;
			  </td>
			  <td class=button width="10%">
				  <INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return saveClick();">
			  </td>
			  <td class=button width="10%">
				  <INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">
				</td>
			  <td class=button width="10%">
				  <INPUT class=cssButton VALUE="重  置"  TYPE=button onclick="return resetClick();">
				</td>
		  </tr>
	  </table>
    <table>
    	<tr>
    		<td>
    	     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
        <td class= titleImg >
          分公司信息
       	</td>
    	</tr>
    </table>
    <Div id= "divCode1" style= "display: ''"> 
    <table class= common align='center'>
      <TR class= common>
        <TD class= title>
          分公司
        </TD>          
        <TD class= input>
          <Input class="codeno" name=ComCode verify="分公司|NotNull&len=4 " maxsize = 4
            ondblclick="return showCodeList('comcode',[this,ComCodeName],[0,1],null,4,'char(length(trim(comcode)))',1);" 
            onkeyup="return showCodeListKey('comcode',[this,ComCodeName],[0,1],null,4,'char(length(trim(comcode)))',1);"
          ><Input class=codename name=ComCodeName readOnly elementtype=nacessary >
        </TD>
        <TD class= title>
          管理机构
        </TD>          
        <TD class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|len=2" readonly
           ><Input class=codename name=ManageComName readOnly elementtype=nacessary >
        </TD>
      </TR>
      <TR class= common>
        <TD class= title>
          计划时段类别
        </TD>
        <TD class= input>
          <Input class=codeno name=PlanPeriodUnit verify="计划时段类别|NotNull&len<=2 " maxsize = 2
           ondblclick="return showCodeList('PlanPeriodUnit',[this,PlanPeriodUnitName],[0,1],null,msql,1);"
           onkeyup="return showCodeListKey('PlanPeriodUnit',[this,PlanPeriodUnitName],[0,1],null,msql,1);"
           ><Input class=codename readonly name=PlanPeriodUnitName elementtype=nacessary >
        </TD>
      </TR>
      <Tr class= common>
        <TD class= title>
          <INPUT VALUE="查询已有的计划" TYPE=button onclick="onQueryClick()" class="cssButton">
        </TD>
      </TR>
    </table>
    </Div>
		<table>
    	<tr>
    		<td>
    	     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode2);">
    		</td>
        <td class= titleImg >
          计划信息
       	</td>
    	</tr>
    </table>
    <Div id= "divCode2" style= "display: ''">
    <table class= common align='center'>
      <Tr class= common>
        <TD class= title>
          计划保费
        </TD>
        <TD class= input>
          <Input class=common name=Money elementtype=nacessary verify="计划保费|NotNull&NUM">
        </TD>
        <TD class= title>
          标准保费
        </TD>
        <TD class= input>
          <Input class=common name=TransMoney verify="标准保费|NUM">
        </TD>
      </Tr>
      <Tr class= common>
        <TD class= title>
          人力
        </TD>
        <TD class= input>
          <Input class=common name=AgentCont  verify="人力|NUM">
        </TD>
        <TD class= title>
          计划客户数
        </TD>
        <TD class= input>
          <Input class=common name=AppCont verify="计划客户数|NUM">
        </TD>
      </Tr>
      <Tr class= common>
        <TD class= title>
          活动率( ％)
        </TD>
        <TD class= input>
          <Input class=common name=ActRate  verify="活动率|NUM">
        </TD>
        <TD class= title>
          年继续率( ％)
        </TD>
        <TD class= input>
          <Input class=common name=YearKeepRate verify="年继续率|NUM">
        </TD>
      </Tr>
      <Tr class= common>
        <TD class= title>
          计划起期年月
        </TD>
        <TD class= input>
          <Input class=common name=DateStartYM verify="计划起期年月|NotNull&len=6&INT"
           elementtype=nacessary verify="计划起期年月|NOTNULL ">
        </TD>
        <TD class= title>
          其他
        </TD>
        <TD class= input>
          <Input class=common name=Other verify="其他|NUM">
        </TD>
      </Tr>
      <Tr class= common>
        <TD class= title>
          备注
        </TD>
        <TD class= input colSpan=3 size=300 >
          <Input class=common3 name=Mark >
        </TD>
      </Tr>
    </table>
    </Div>
    <table>
    	<tr>
    		<td>
    	     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode3);">
    		</td>
        <td class= titleImg >
          计划信息
       	</td>
    	</tr>
    </table>
    <Div id= "divCode3" style= "display: ''">
    <table class= common align='center'>
      <Tr class= common>
        <td text-align: left colSpan=1>
  			  <span id="spanPlanGrid" >
  				</span>
  			</td>
    	</tr>
    </table>
    <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
    <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
    <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
    <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
    </Div>

    <input type=hidden name=BranchType value=''> 
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=Operate value=''>
   
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
