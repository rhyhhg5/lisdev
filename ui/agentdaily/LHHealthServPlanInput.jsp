<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHHealthServPlanInput.jsp
//程序功能：个人服务计划内容定义(页面显示)
//创建日期：2006-03-20 10:09:30
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHHealthServPlanInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHHealthServPlanInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHHealthServPlanSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHHealthServPlanGrid1);">
    		</td>
    		<td class= titleImg>
        		 个人服务计划内容定义
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHHealthServPlanGird1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      服务计划代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ServPlanCode verify="服务计划代码|NOTNULL&len<=20">
    </TD>
    <TD  class= title>
      服务计划名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ServPlanName verify="个人服务计划名称|NOTNULL&len<=300">
    </TD>
    <TD class= title>
    	服务计划档次
    </TD>
    <TD  class= input>
    	<Input  type = hidden  name= ServPlanLevelCode>
    	<Input class= 'code' name=ServPlanLevelName verify="个人服务计划档次|len<=10" ondblclick="showCodeList('hmservplanlevel',[this,ServPlanLevelCode],[0,1],null,fm.ServPlanLevelName.value,'ServPlanLevelName','',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservplanlevel',[this,ServPlanLevelCode],[0,1],null,fm.ServPlanLevelName.value,'ServPlanLevelName');"   onkeydown="return showCodeListKey('hmservplanlevel',[this,ServPlanLevelCode],[0,1],null,fm.ServPlanLevelName.value,'ServPlanLevelName'); codeClear(ServPlanLevelName,ServPlanLevelCode);">
   </TD>   
  </TR>
  <TR  class= common>
  	 <TD  class= title>
      机构属性标识
    </TD>
    <TD  class= input>
      <Input  type = hidden  name=ComIDCode>
      <Input class= 'code' name=ComIDName verify="机构属性标识|len<=10" ondblclick="showCodeList('hmhospitalmng',[this,ComIDCode],[0,1],null,fm.ComIDName.value,'ComIDName','1',260);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmhospitalmng',[this,ComIDCode],[0,1],null,fm.ComIDName.value,'ComIDName');" onkeydown="return showCodeListKey('hmhospitalmng',[this,ComIDCode],[0,1],null,fm.ComIDName.value,'ComIDName'); codeClear(ComIDName,ComIDCode);">	
    </TD>
   </TR>
</table>
    </Div>

     
      <table>
		  	<tr>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHHealthServPlanGrid);">
		  		</td>
		  		<td class=titleImg>
		  			 信息
		  		</td>
	
		  	</tr>
		  </table>
		  <!-- 信息（列表） -->
		<Div id="divLHHealthServPlanGrid1" style="display:''">
	     <div id="divDutyExolai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textDutyExolai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backDutyExolai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backDutyExolaiButton()">
			 </div>
			 <div id="divFeeExplai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textFeeExplai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backFeeExplai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backFeeExplaiButton()">
			 </div>
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHHealthServPlanGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
		</div>

		</table>
    </Div>
		<div id="divLHHealthServPlanGrid1" style="display: 'none'">
			  <table class=common>
			  <TR  class= common>
			    <TD  class= title>
			      执行者
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=linkman >
			    </TD>
			    <TD  class= title>
			      责  任
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Phone >
			    </TD>
			    <TD >
			    </TD>
			  </TR>
			  </table>
		</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
    <input type = hidden name = ServPlayType>
    <input type = hidden name = ContraItemNo>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
