<%
//程序名称：LAPlanManageInit.jsp
//程序功能：
//创建日期：2005-12-12 15:04
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">
function initInpBox1()
{ 
  try
  {
    document.fm.ComCode.value = "";
    document.fm.ComCodeName.value = "";
    document.fm.ManageCom.value = "86";
    document.fm.ManageComName.value = "总公司";
    document.fm.PlanPeriodUnit.value = "";
    document.fm.PlanPeriodUnitName.value = "";
  }
  catch(ex)
  {
    alert("在LAAscriptionInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误1111!");
  }
}
function initInpBox()
{ 
  try
  {
    document.fm.Money.value = "";
    document.fm.TransMoney.value = "";
    document.fm.AgentCont.value = "";
    document.fm.AppCont.value = "";
    document.fm.ActRate.value = "";
    document.fm.YearKeepRate.value = "";
    document.fm.Other.value = "";
    document.fm.Mark.value = "";
    
    document.fm.DateStartYM.value = "";
    document.fm.Operate.value = "";
    
    document.fm.BranchType.value = "<%=BranchType%>";
    document.fm.BranchType2.value = "<%=BranchType2%>";
  }
  catch(ex)
  {
    alert("在LAAscriptionInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误1111!");
  }
}

var PlanGrid;
function initLAPlanGrid() {
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名
    
    iArray[1]=new Array();
    iArray[1][0]="分公司代码";         //列名
    iArray[1][1]="65px";         //列名
    iArray[1][2]=100;         //列名
    iArray[1][3]=0;         //列名
    
    iArray[2]=new Array();
    iArray[2][0]="分公司名称";         //列名
    iArray[2][1]="90px";         //列名
    iArray[2][2]=100;         //列名
    iArray[2][3]=0;         //列名
    
    iArray[3]=new Array();
    iArray[3][0]="管理机构";         //列名
    iArray[3][1]="0px";         //列名
    iArray[3][2]=100;         //列名
    iArray[3][3]=3;         //列名
    
    iArray[4]=new Array();
    iArray[4][0]="计划时段";         //列名
    iArray[4][1]="40px";         //列名
    iArray[4][2]=100;         //列名
    iArray[4][3]=0;         //列名
    
    iArray[5]=new Array();
    iArray[5][0]="计划起期年月";         //列名
    iArray[5][1]="60px";         //列名
    iArray[5][2]=100;         //列名
    iArray[5][3]=0;         //列名
    
    iArray[6]=new Array();
    iArray[6][0]="计划止期";         //列名
    iArray[6][1]="0px";         //列名
    iArray[6][2]=100;         //列名
    iArray[6][3]=3;         //列名
    
    iArray[7]=new Array();
    iArray[7][0]="保费";         //列名
    iArray[7][1]="55px";         //列名
    iArray[7][2]=100;         //列名
    iArray[7][3]=0;         //列名
    
    iArray[8]=new Array();
    iArray[8][0]="标保";         //列名
    iArray[8][1]="55px";         //列名
    iArray[8][2]=100;         //列名
    iArray[8][3]=0;         //列名
    
    iArray[9]=new Array();
    iArray[9][0]="人力";         //列名
    iArray[9][1]="35px";         //列名
    iArray[9][2]=100;         //列名
    iArray[9][3]=0;         //列名
    
    iArray[10]=new Array();
    iArray[10][0]="客户数";         //列名
    iArray[10][1]="35px";         //列名
    iArray[10][2]=100;         //列名
    iArray[10][3]=0;         //列名
    
    iArray[11]=new Array();
    iArray[11][0]="活动率";         //列名
    iArray[11][1]="35px";         //列名
    iArray[11][2]=100;         //列名
    iArray[11][3]=0;         //列名
    
    iArray[12]=new Array();
    iArray[12][0]="继续率";         //列名
    iArray[12][1]="35px";         //列名
    iArray[12][2]=100;         //列名
    iArray[12][3]=0;         //列名
    
    iArray[13]=new Array();
    iArray[13][0]="操作员";         //列名
    iArray[13][1]="50px";         //列名
    iArray[13][2]=100;         //列名
    iArray[13][3]=0;         //列名
    
    iArray[14]=new Array();
    iArray[14][0]="最近操作日";         //列名
    iArray[14][1]="60px";         //列名
    iArray[14][2]=100;         //列名
    iArray[14][3]=0;         //列名
    
    PlanGrid = new MulLineEnter( "fm" , "PlanGrid" );
    PlanGrid.selBoxEventFuncName = "getOnePlan";
    //这些属性必须在loadMulLine前
    PlanGrid.mulLineCount = 0;
    PlanGrid.displayTitle = 1;
    PlanGrid.canSel = 1;
    PlanGrid.hiddenPlus = 1;
    PlanGrid.hiddenSubtraction = 1;
    PlanGrid.loadMulLine(iArray);
  }
  catch(ex) {
    alert(ex);
  }
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("ALACaseInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
 
function initForm()
{
  try
  {
    initInpBox1();
    initInpBox();
    initSelBox();
    initLAPlanGrid();
  }
  catch(re)
  {
    alert("在LAHolsInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>