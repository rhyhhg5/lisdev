<%
//程序名称：AbsenceQueryInit.js
//程序功能：
//创建日期：2003-7-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    fm.all('Idx').value = '';
    //fm.all('Times').value = '';
    fm.all('SumMoney').value = '';
    //fm.all('AClass').value = '';
    fm.all('DoneDate').value = '';
    fm.all('Noti').value = '';
    fm.all('Operator').value = '';  
    fm.all('BranchType').value = top.opener.fm.all('BranchType').value;
    //alert(fm.all('BranchType').value);
  }
  catch(ex)
  {
    alert("在AbsenceQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AbsenceQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	  initPresenceGrid();
  }
  catch(re)
  {
    alert("AbsenceQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化PresenceGrid
 ************************************************************
 */
function initPresenceGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="员工代码";         //列名
        iArray[1][1]="150px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="纪录顺序号";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="销售机构";         //列名
        iArray[3][1]="120px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="120px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="缺勤时间";         //列名
        iArray[5][1]="120px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="执行日期";         //列名
        iArray[6][1]="120px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

  
        PresenceGrid = new MulLineEnter( "fm" , "PresenceGrid" ); 

        //这些属性必须在loadMulLine前
        PresenceGrid.mulLineCount = 0;   
        PresenceGrid.displayTitle = 1;
        PresenceGrid.locked=1;
        PresenceGrid.canSel=1;
        PresenceGrid.canChk=0;
        PresenceGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化PresenceGrid时出错："+ ex);
      }
    }


</script>