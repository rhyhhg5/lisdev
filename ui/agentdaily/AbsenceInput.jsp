<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：AbsenceInput.jsp
//程序功能：
//创建日期：2003-7-7
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="AbsenceInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="AbsenceInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./AbsenceSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAbsence1);">
    <td class=titleImg>
      员工差勤信息
    </td>
    </td>
    </tr>
    </table>
    <Div  id= "divAbsence1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            员工编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode readonly verify="代理人编码|code:AgentCode" 
            ondblclick="return showCodeList('AgentCode',[this], [0,1],null,acodeSql,'1',null,500);" 
            onkeyup="return showCodeListKey('AgentCode', [this], [0,1],null,acodeSql,'1',null,500);">            
          </TD>
          <TD  class= title>
            员工姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Name>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            销售机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AgentGroup verify="销售机构|notnull&code:AgentGroup">
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ManageCom verify="管理机构|notnull&code:ManageCom">
          </TD>
        </TR>
        <!--TR  class= common>
          <TD  class= title>
            纪录顺序号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Idx >
          </TD>
          <TD  class= title>
            类别
          </TD>
          <TD  class= input>
            <Input class='code' name=AClass maxlength=1 ondblclick="return showCodeList('presenceaclass',[this]);" onkeyup="return showCodeListKey('presenceaclass',[this]);">
          </TD>
        </TR-->
        <TR  class= common>
          <TD  class= title>
            缺勤时间（单位：小时）
          </TD>
          <TD  class= input>
            <Input class= common name=SumMoney >
          </TD>
          <TD  class= title>
            批注
          </TD>
          <TD  class= input>
            <Input class= common name=Noti >
          </TD>
        </TR>
        <TR  class= common>
        <TD  class= title>
            执行日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=DoneDate dateFormat='short' >
          </TD>
          <TD  class= title>
            纪录顺序号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Idx >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Operator >
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=AClass >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var acodeSql = "#1# and (BranchType=#2# or BranchType=#3#) and (AgentState = #01# or AgentState = #02#)";
</script>
