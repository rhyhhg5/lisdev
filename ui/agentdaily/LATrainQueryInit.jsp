<%
//程序名称：LATrainQueryInit.js
//程序功能：
//创建日期：2003-01-20 14:14:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    fm.all('Idx').value = '';
    fm.all('AClass').value = '';
    fm.all('TrainUnit').value = '';
    fm.all('TrainName').value = '';
    fm.all('Charger').value = '';
    //fm.all('ResultLevel').value = '';
    fm.all('Result').value = '';
    fm.all('TrainPassFlag').value = '';
    fm.all('TrainStart').value = '';
    fm.all('TrainEnd').value = '';
    fm.all('DoneDate').value = '';
    fm.all('DoneFlag').value = '';
    fm.all('Noti').value = '';
    fm.all('Operator').value = '';
    fm.all('ttttBranchType').value = '<%=tBranchType%>';
    
   
  }
  catch(ex)
  {
    alert("在LATrainQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LATrainQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	  initTrainGrid();
  }
  catch(re)
  {
    alert("LATrainQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化TrainGrid
 ************************************************************
 */
function initTrainGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="代理人代码";         //列名
        iArray[1][1]="100px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="纪录顺序号";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="销售机构";         //列名
        iArray[3][1]="150px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="120px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="培训名称";         //列名
        iArray[5][1]="120px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="培训类别";         //列名
        iArray[6][1]="100px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[7]=new Array();
        iArray[7][0]="培训起始时间";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="培训中止时间";         //列名
        iArray[8][1]="100px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
  
        TrainGrid = new MulLineEnter( "fm" , "TrainGrid" ); 

        //这些属性必须在loadMulLine前
        TrainGrid.mulLineCount = 0;   
        TrainGrid.displayTitle = 1;
        TrainGrid.locked=1;
        TrainGrid.canSel=1;
        TrainGrid.hiddenPlus = 1;
        TrainGrid.hiddenSubtraction = 1;
        TrainGrid.canChk=0; 
        TrainGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化TrainGrid时出错："+ ex);
      }
    }


</script>