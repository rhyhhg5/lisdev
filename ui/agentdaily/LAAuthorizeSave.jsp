<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAuthorizeSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAuthorizeSchema tLAAuthorizeSchema;
  LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
  ALAAuthorizeUI tALAAuthorizeUI  = new ALAAuthorizeUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin agent schema...");
    //取得授权信息
  int lineCount = 0;
  String arrCount[] = request.getParameterValues("AuthorizeGridNo");
  String tRiskCode[] = request.getParameterValues("AuthorizeGrid1");
  String tRiskTypeName[] = request.getParameterValues("AuthorizeGrid2");
  String tAuthorStartDate[] = request.getParameterValues("AuthorizeGrid3");
  String tAuthorEndDate[] = request.getParameterValues("AuthorizeGrid4");
  String tAgentCode = request.getParameter("AgentCode");
  String tBranchType = request.getParameter("BranchType");
  String tBranchType2 = request.getParameter("BranchType2");
  
  lineCount = arrCount.length; //行数
  System.out.println("Count:"+Integer.toString(lineCount));
  for(int i=0;i<lineCount;i++)
  {
    tLAAuthorizeSchema = new LAAuthorizeSchema();
    tLAAuthorizeSchema.setAuthorObj(tAgentCode); 
    tLAAuthorizeSchema.setAuthorType("0");   
    tLAAuthorizeSchema.setBranchType(tBranchType);    
    tLAAuthorizeSchema.setBranchType2(tBranchType2);
    tLAAuthorizeSchema.setRiskCode(tRiskCode[i]);
   //tLAAuthorizeSchema.setRiskType(tRiskCode[i]); 
   // tLAAuthorizeSchema.setRiskTypeName(tRiskTypeName[i]);   
    tLAAuthorizeSchema.setAuthorStartDate(tAuthorStartDate[i]);
    tLAAuthorizeSchema.setAuthorEndDate(tAuthorEndDate[i]);
    tLAAuthorizeSet.add(tLAAuthorizeSchema);
  }
  System.out.println("end 授权信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  LAAuthorizeSchema mLAAuthorizeSchema = new LAAuthorizeSchema();
        tVData.addElement(tLAAuthorizeSet);
        tVData.add(tG);
        tVData.add(mLAAuthorizeSchema); //无用
  System.out.println("add over");
  try
  {
    tALAAuthorizeUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tALAAuthorizeUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

