<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAManageAllowRateSave.jsp
//程序功能：
//创建日期：20090219
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
<%
  CErrors  tError = null;
  String tRela  = "";
  String Content = "";
  String FlagStr = "Fail";
  String transact = "";
  String tOperate ="INSERT||MAIN";
  String mTest ="";
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");

  LABaseWageSchema tLABaseWageSchema   = new LABaseWageSchema();
  LABaseWageSet tLABaseWageSet = new LABaseWageSet();
  LABaseWageSet tSetU = new LABaseWageSet();	//用于修改
  LABaseWageSet tSetI = new LABaseWageSet();	//用于保存 新增
  LAManageAllowRateUI tLAManageAllowRateUI= new LAManageAllowRateUI();
  try
  {

  //接收信息，并作校验处理。
  //输入参数
  //执行动作：insert 添加纪录，update 修改纪录
  transact = request.getParameter("fmtransact");
  //取得Muline信息
  int lineCount = 0;
  String tChk[] = request.getParameterValues("InpManageAllowRateGridChk");
  String tAgentGrade[] = request.getParameterValues("ManageAllowRateGrid1");
  String tYearRate[] = request.getParameterValues("ManageAllowRateGrid3");
  String tIdx[] = request.getParameterValues("ManageAllowRateGrid4");
  String tManageCom = request.getParameter("ManageCom");
  lineCount = tAgentGrade.length; //行数

 
  //获取最大的ID号
  ExeSQL tExe = new ExeSQL();
  String tSql = "select int(max(idx)) from labasewage order by 1 desc ";
  String strIdx = "";
  int tMaxIdx = 0;

  strIdx = tExe.getOneValue(tSql);
  if (strIdx == null || strIdx.trim().equals(""))
  {
      tMaxIdx = 1;
  }
  else
  {
      tMaxIdx = Integer.parseInt(strIdx);
      System.out.println(tMaxIdx);
  }
  //创建数据集
  for(int i=0;i<tChk.length;i++)
  {
  	if(tChk[i].equals("1"))
  	{
      System.out.println("savejsp"+i);
  		//创建一个新的Schema
      LABaseWageSchema tSch = new LABaseWageSchema();
      tSch.setAgentGrade(tAgentGrade[i]);
      tSch.setBranchType("3");
      tSch.setBranchType2("01");
      tSch.setYearRate(Double.parseDouble(tYearRate[i].trim()));  
      tSch.setManageCom(tManageCom);
      if((tIdx[i] == null)||(tIdx[i].equals(""))||(tIdx[i].equals("0")))
      {
  		tMaxIdx++;
        tSch.setIdx(String.valueOf(tMaxIdx));
  		tSetI.add(tSch);
      }
      else if(transact.trim().equals("UPDATE"))
      {
        tSch.setIdx(tIdx[i]);
        tSetU.add(tSch);
      }
  	}
  }
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  Content="";
  tVData.add(tGI);
  //没有更新或删除或插入的数据
  if ((tSetU.size() == 0)&&(tSetI.size() == 0))
  {
    FlagStr = "Fail";
    Content = FlagStr + "未选中要处理的数据！";
  }
  else if (tSetI.size() != 0)
  {
    tVData.add(tSetI);
    System.out.println("Start LAManageAllowRateUI Submit...INSERT");
    tLAManageAllowRateUI.submitData(tVData,"INSERT");
  }
  else if (tSetU.size() != 0)
  {
        	//只有修改数据
    tVData.add(tSetU);
    System.out.println("Start LAManageAllowRateUI Submit...UPDATE");
    tLAManageAllowRateUI.submitData(tVData,"UPDATE");
  }
  }
  catch (Exception ex)
  {
    ex.printStackTrace();
    FlagStr = "Fail";
    Content = FlagStr + "操作失败，原因是：" + ex.toString();
  }
  if(!FlagStr.equals("Fail"))
  {
    System.out.println("nihao");
    tError = tLAManageAllowRateUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功! ";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
  <script language="javascript" type="">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
