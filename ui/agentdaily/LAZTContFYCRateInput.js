//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //initArchieveGrid();
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("LAContFYCRateInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
   if(!chkMulLine()){ return false;}
    return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  if (verifyInput() == false)
  return false;	
 
  if(!beforeSubmit())
  {
    return false;
  }
	
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
   var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if (verifyInput() == false)
  return false;	
if(!beforeSubmit())
  {
    return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}                    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if(!beforeSubmit())
  {
    return false;
  }  
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



function easyQueryClick() {
  if (verifyInput() == false)
  return false;	
  
  var tBranchtype2 = fm.BranchType2.value;
  //校验保单是否属于银行代理的保单
  var SQL = "select contno from lccont where contno='"+fm.ContNo.value
  +"' and exists  (select '1' from laagent where branchtype ='2' and branchtype2='"+tBranchtype2+"' and agentcode=lccont.agentcode) "
  +" and grpcontno='00000000000000000000' "
  +" union select contno from lbcont where contno='"+fm.ContNo.value
  +"' and exists  (select '1' from laagent where branchtype ='2' and branchtype2='"+tBranchtype2+"' and agentcode=lbcont.agentcode)"
  +" and grpcontno='00000000000000000000' ";
  var strQueryResult = easyQueryVer3(SQL, 1, 1, 1);
 
  if(!strQueryResult)
  {
   alert("该保单不是职团业务!");
   return false;
  }
  initArchieveGrid();
   var tReturn = getManageComLimitlike("ManageCom");
	//此处书写SQL语句			     			    
 var strSql = "select distinct riskcode,"+
                "(select riskname from lmriskapp where riskcode = lacommision.riskcode),"+
                "contno,managecom,fycrate,"+
                "case when (select a.rate from LAContFYCRate a where a.grpcontno = lacommision.commisionsn and a.riskcode = lacommision.riskcode) is null then '' else (select  to_char(a.rate)  from LAContFYCRate a where a.grpcontno = lacommision.commisionsn and a.riskcode = lacommision.riskcode) end,"+
                "wageno,transmoney,commisionsn"+
  " from lacommision"+
  " where 1=1 and branchtype='2' and branchtype2='"+tBranchtype2+"' and commdire<='1'"
  +tReturn
    + getWherePart('ContNo', 'ContNo')
    + getWherePart('WageNo', 'WageNo') ;
    
  var arrResult = new Array();
  arrResult = easyExecSql(strSql);
  
  if (!arrResult) 
    {
    alert("此保单当月没有相应收费信息,请核实相关保单信息！");
    return false;
    }
    displayMultiline(arrResult,ArchieveGrid);
   //查询成功则拆分字符串，返回二维数组
 
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ArchieveGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = ArchieveGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}             
function chkMulLine()
{
	var i;
	var iCount = 0;
	var rowNum = ArchieveGrid.mulLineCount;
	for(i=0;i<rowNum;i++){
		if(ArchieveGrid.getChkNo(i)){
		var trate=ArchieveGrid.getRowColData(i,6); //非标
		if(trate==null||trate=="")
		{
		   alert("被选中的第"+(i+1)+"条记录非标比例为空,请重新录入！");
		   return false;
		}
			iCount++;
		} 
	}
	if(iCount == 0){
		alert("请选择要录入非标的记录!");
		return false
	}
	return true;
}