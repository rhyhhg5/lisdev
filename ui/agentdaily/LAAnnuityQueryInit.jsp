<%
//程序名称：LAAnnuityInit.jsp
//程序功能：
//创建日期：2005-6-30 10:10
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
  //添加页面控件的初始化。
%>                            
<script language="JavaScript">
var tBranchType = "<%=BranchType%>";
var tAgentTitleGrid = "营销员";
if("2"==tBranchType)
{
  tAgentTitleGrid = "业务员";
}

//初始化画面上的文本框
function initInpBox()
{ 
  try
  {
    document.fm.AgentCode.value = "";
    document.fm.ManageCom.value = "";
    document.fm.BranchAttr.value = "";
    document.fm.EmployDateF.value = "";
    document.fm.EmployDateT.value = "";
    //document.fm.AgentGrade.value = "";
    document.fm.OutWorkDateF.value = "";
    document.fm.OutWorkDateT.value = "";
    document.fm.DrawMarker.value = "";
  }
  catch(ex)
  {
    alert("在LAAccountsInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

//初始化页面
function initForm()
{
  try
  {
    initInpBox();
    initAnnuityGrid();
  }
  catch(re)
  {
    alert("LAAccountsInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//初始化列表
function initAnnuityGrid()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]=tAgentTitleGrid+"代码";         //列名
    iArray[1][1]="80px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]=tAgentTitleGrid+"姓名";         //列名
    iArray[2][1]="60px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="销售单位";         //列名
    iArray[3][1]="100px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="销售单位名称";         //列名
    iArray[4][1]="120px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="管理机构";         //列名
    iArray[5][1]="60px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="离司时间";         //列名
    iArray[6][1]="80px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="实付时间";         //列名
    iArray[7][1]="80px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="应付金额";         //列名
    iArray[8][1]="80px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="实付金额";         //列名
    iArray[9][1]="60px";         //宽度
    iArray[9][2]=100;         //最大长度
    iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="领取状态";         //列名
    iArray[10][1]="50px";         //宽度
    iArray[10][2]=100;         //最大长度
    iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="领取ID";         //列名
    iArray[11][1]="0px";         //宽度
    iArray[11][2]=100;         //最大长度
    iArray[11][3]=3;         //是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="养老金比例";         //列名
    iArray[12][1]="0px";         //宽度
    iArray[12][2]=100;         //最大长度
    iArray[12][3]=3;         //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="养老金序列号";         //列名
    iArray[13][1]="0px";         //宽度
    iArray[13][2]=100;         //最大长度
    iArray[13][3]=3;         //是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();
    iArray[14][0]="养老金合计";         //列名
    iArray[14][1]="0px";         //宽度
    iArray[14][2]=100;         //最大长度
    iArray[14][3]=3;         //是否允许录入，0--不能，1--允许
    
    AnnuityGrid = new MulLineEnter( "fm" , "AnnuityGrid" ); 

    //这些属性必须在loadMulLine前
    AnnuityGrid.mulLineCount = 0;   
    AnnuityGrid.displayTitle = 1;
    AnnuityGrid.hiddenPlus = 1;
    AnnuityGrid.hiddenSubtraction = 1;
    AnnuityGrid.locked=1;
    AnnuityGrid.canSel=1;
    AnnuityGrid.canChk=0;
    AnnuityGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("初始化AnnuityGrid时出错："+ ex);
  }
}
</script>
