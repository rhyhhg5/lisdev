<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：RiskQuarterAssessInput.jsp
//程序功能：
//创建日期：2009-02-18
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="RiskQuarterAssess.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="RiskQuarterAssessInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./RiskQuarterAssessSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    	  <td class= titleImg>
        	管理机构信息
          </td>
    	</tr>
    </table>
    
    <table  class= common>
      <TR class = common>
        <TD class = title>
           管理机构
        </TD>
        <TD  class= input>
          <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);" verify="管理机构|notnull&code:comcode"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
        </TD>            
      </TR>          
    </table>       
    <input type =button class=cssButton value="查 询" onclick="easyQueryClick();">
    <input type =button class=cssButton value="保 存" onclick="save();">
    <input type =button class=cssButton value="修 改" onclick="update();"> 
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden name=BranchType value=<%=BranchType%>> 
    <input type=hidden name=BranchType2 value=<%=BranchType2%>>       
    
    <table>
  		<tr>
      	<td class=common>
		    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSpanOperatorIndexMarkGrid);">
  			</td>
  			<td class=titleImg>
  			 险种季度考核标准录入
  			</td>
  		</tr>
  	</table>
		<Div id= "divSpanRiskQuarterAssessGrid" align=center style= "display: '' ">
      <table class=common>
    		<tr class=common>
	  			<td text-align:left colSpan=1>
  					<span id="spanRiskQuarterAssessGrid">
  					</span> 
		    	</td>
				</tr>
			</table>
		</div>
    <!--Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
    </div-->
     
   
     
     
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>