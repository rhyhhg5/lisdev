<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script language="JavaScript">
function initForm()
{
  try
  {
	initProjectGrid();
    initElementtype();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function initProjectGrid() 
{                               
  try 
  {
	  var iArray = new Array();


	    iArray[0]=new Array();
	    iArray[0][0]="序号";         	//列名
	    iArray[0][1]="30px";         	//列名    
	    iArray[0][3]=0;         		//列名
	            		
	    iArray[1]=new Array();
	    iArray[1][0]="机构";         	 //列名
	    iArray[1][1]="50px";            //列宽
	    iArray[1][2]=200;            			//列最大值
	    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	    
	    iArray[2]=new Array();
	    iArray[2][0]="产品名称";         	
	    iArray[2][1]="120px";            	
	    iArray[2][2]=200;            		 
	    iArray[2][3]=0;              		 
	    
	    iArray[3]=new Array();
	    iArray[3][0]="产品代号";         	  //列名
	    iArray[3][1]="60px";            	//列宽
	    iArray[3][2]=200;            			//列最大值
	    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[4]=new Array();
	    iArray[4][0]="个团标志";      //列名
	    iArray[4][1]="60px";            	//列宽
	    iArray[4][2]=200;            			//列最大值
	    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	     
	    iArray[5]=new Array();
	    iArray[5][0]="保单号";      //列名
	    iArray[5][1]="100px";            	//列宽
	    iArray[5][2]=200;            			//列最大值
	    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	     
	    iArray[6]=new Array();
	    iArray[6][0]="客户名称";      //列名
	    iArray[6][1]="120px";            	//列宽
	    iArray[6][2]=200;            			//列最大值
	    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	     
	    iArray[7]=new Array();
	    iArray[7][0]="客户号";      //列名
	    iArray[7][1]="80px";            	//列宽
	    iArray[7][2]=200;            			//列最大值
	    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[8]=new Array();
	    iArray[8][0]="签单日期";      //列名
	    iArray[8][1]="80px";            	//列宽
	    iArray[8][2]=200;            			//列最大值
	    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[9]=new Array();
	    iArray[9][0]="生效日期";      //列名
	    iArray[9][1]="80px";            	//列宽
	    iArray[9][2]=200;            			//列最大值
	    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[10]=new Array();
	    iArray[10][0]="终止日期";      //列名
	    iArray[10][1]="80px";            	//列宽
	    iArray[10][2]=200;            			//列最大值
	    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[11]=new Array();
	    iArray[11][0]="缴费方式";      //列名
	    iArray[11][1]="60px";            	//列宽
	    iArray[11][2]=200;            			//列最大值
	    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[12]=new Array();
	    iArray[12][0]="保费收入";      //列名
	    iArray[12][1]="60px";            	//列宽
	    iArray[12][2]=200;            			//列最大值
	    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[13]=new Array();
	    iArray[13][0]="年龄";      //列名
	    iArray[13][1]="60px";            	//列宽
	    iArray[13][2]=200;            			//列最大值
	    iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[14]=new Array();
	    iArray[14][0]="性别";      //列名
	    iArray[14][1]="60px";            	//列宽
	    iArray[14][2]=200;            			//列最大值
	    iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[15]=new Array();
	    iArray[15][0]="被保险人数";      //列名
	    iArray[15][1]="60px";            	//列宽
	    iArray[15][2]=200;            			//列最大值
	    iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[16]=new Array();
	    iArray[16][0]="公共保额";      //列名
	    iArray[16][1]="60px";            	//列宽
	    iArray[16][2]=200;            			//列最大值
	    iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[17]=new Array();
	    iArray[17][0]="销售渠道";      //列名
	    iArray[17][1]="60px";            	//列宽
	    iArray[17][2]=200;            			//列最大值
	    iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[18]=new Array();
	    iArray[18][0]="共收保费";      //列名
	    iArray[18][1]="60px";            	//列宽
	    iArray[18][2]=200;            			//列最大值
	    iArray[18][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[19]=new Array();
	    iArray[19][0]="当期应收保费";      //列名
	    iArray[19][1]="80px";            	//列宽
	    iArray[19][2]=200;            			//列最大值
	    iArray[19][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[20]=new Array();
	    iArray[20][0]="续保";      //列名
	    iArray[20][1]="60px";            	//列宽
	    iArray[20][2]=200;            			//列最大值
	    iArray[20][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[21]=new Array();
	    iArray[21][0]="保单状态";      //列名
	    iArray[21][1]="60px";            	//列宽
	    iArray[21][2]=200;            			//列最大值
	    iArray[21][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[22]=new Array();
	    iArray[22][0]="退保终止日期";      //列名
	    iArray[22][1]="80px";            	//列宽
	    iArray[22][2]=200;            			//列最大值
	    iArray[22][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[23]=new Array();
	    iArray[23][0]="市场类型";      //列名
	    iArray[23][1]="60px";            	//列宽
	    iArray[23][2]=200;            			//列最大值
	    iArray[23][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[24]=new Array();
	    iArray[24][0]="成本中心";      //列名
	    iArray[24][1]="80px";            	//列宽
	    iArray[24][2]=200;            			//列最大值
	    iArray[24][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[25]=new Array();
	    iArray[25][0]="档次";      //列名
	    iArray[25][1]="60px";            	//列宽
	    iArray[25][2]=200;            			//列最大值
	    iArray[25][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[26]=new Array();
	    iArray[26][0]="死伤理疗给付";      //列名
	    iArray[26][1]="80px";            	//列宽
	    iArray[26][2]=200;            			//列最大值
	    iArray[26][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[27]=new Array();
	    iArray[27][0]="退保金";      //列名
	    iArray[27][1]="60px";            	//列宽
	    iArray[27][2]=200;            			//列最大值
	    iArray[27][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[28]=new Array();
	    iArray[28][0]="退保日期";      //列名
	    iArray[28][1]="60px";            	//列宽
	    iArray[28][2]=200;            			//列最大值
	    iArray[28][3]=0;              			//是否允许输入,1表示允许，0表示不允许  

	    
	    ProjectGrid = new MulLineEnter("fm", "ProjectGrid");
	    ProjectGrid.mulLineCount = 0;
	    ProjectGrid.displayTitle = 1;
	    ProjectGrid.hiddenPlus = 1;
	    ProjectGrid.locked = 1;
	    ProjectGrid.hiddenSubtraction = 1;
	    ProjectGrid.canChk = 1; //复选框 
	    ProjectGrid.selBoxEventFuncName = "getQueryResult";
	    ProjectGrid.loadMulLine(iArray);
    
  }
  catch(ex) 
  {
    alert(ex);
  }
}
                                      

</script>
