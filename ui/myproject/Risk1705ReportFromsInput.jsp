<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="Risk1705ReportFromsInit.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
//程序名称：Risk1705ReportFromsInput.jsp
//程序功能
//创建时间：2018-05-03
//查询信息
%>

<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="Risk1705ReportFromsInput.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>1705险种提数</title>
</head>
<body onload="initForm();">
	<form action="" method=post name=fm target="fraSubmit">
		<table>
			<TR>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,queryInput);"></td>
				<td class=titleImg>查询条件</td>
			</TR>
		</table>
		<table class=common id="queryInput" style="display: ''">
			<tr>
			
			<td class=title8>统计截止日期</td>
				<td class=input><input class="coolDatePicker"  elementtype=nacessary
					dateFormat="short" name=sdate onkeydown="QueryOnKeyDown()"></td>
			
			</tr>
		</table>
		<table align='center'>
					<input type="hidden" class="common" name="querySql" >
					<td class=button width="10%" align=left>
						<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
						<INPUT CLASS=cssButton VALUE="  下  载  " TYPE=button onclick="downLoad()">
					</td>
				</table>

		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divPersonGrid);">
				</td>
				<td class=titleImg>个人信息</td>
			</tr>
		</table>
		<div id="divPersonGrid" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1><span id="spanProjectGrid"></span>
					</td>
				</tr>
			</table>
			<Div id="divPage1" align=center style="display: ''">
				<INPUT CLASS=cssButton VALUE="首页" TYPE=button
					onclick="turnPage.firstPage();"> <INPUT CLASS=cssButton
					VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
				<INPUT CLASS=cssButton VALUE="下一页" TYPE=button
					onclick="turnPage.nextPage();"> <INPUT CLASS=cssButton
					VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
			</div>
		</div>

		<Input id=querySql type=hidden value=''>
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
