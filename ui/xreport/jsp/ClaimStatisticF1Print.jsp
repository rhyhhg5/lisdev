<%
//程序名称:理赔报表打印
//程序功能：
//创建日期：2003-9-1
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%  
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
    String Code = request.getParameter("Code");
    System.out.println("报表类型代码:"+Code);
    //时间
    String mDay[]=new String[2];
    mDay[0]=request.getParameter("StartTime");
    mDay[1]=request.getParameter("EndTime");
    
    String strOperation ="PRINTPAY";
    VData tVData = new VData();
    VData mResult = new VData();
    tVData.addElement(Code);
    tVData.addElement(mDay);
    tVData.addElement(tG);
    
    CError cError = new CError( );
    CErrors mErrors = new CErrors();
    XmlExport txmlExport = new XmlExport();
    RiskClaimstatisticUI tRiskClaimstatisticUI = new RiskClaimstatisticUI();
    if(!tRiskClaimstatisticUI.submitData(tVData,strOperation)){
      mErrors.copyAllErrors(tRiskClaimstatisticUI.mErrors);
      cError.moduleName = "ClaimstatisticF1Print";
      cError.functionName = "submitData";
      cError.errorMessage = "RiskClaimstatisticUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tRiskClaimstatisticUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null){
       return;
    }else{
       session.putValue("PrintStream", txmlExport.getInputStream());
       System.out.println("put session value");
       response.sendRedirect("../../f1print/GetF1Print.jsp");
    }
   
%> 
<html>
  <script language="javascript">
    top.opener.focus();
    top.close();
   </script>
</html>
    
    

 
