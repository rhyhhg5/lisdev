
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-12-17 16:10:00
//创建人  lixy
//更新记录：  更新人     更新日期     更新原因/内容
//          AppleWood  2003/05/15  将下拉列表框的代码显示为文字.

%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@ include file="init.jsp"%>
<%
//	Code code=new Code();
//        code.getReportMainMap();
//	Vector vecReport=code.getReportMain();
//	Vector vecBranchId=new Vector();
//	Vector vecReportId=new Vector();
//	Vector vecReportEdition=new Vector();
//	int length;
//	code.getReportMainMap();
//	length=vecReport.size();
//	for(int i=0;i<length;i++)
//	{
//		ReportMain report=(ReportMain)vecReport.elementAt(i);
//		if(!vecBranchId.contains(report.getBranchId()))
//			vecBranchId.addElement(report.getBranchId());
//		if(!vecReportId.contains(report.getReportId()))
//		    vecReportId.addElement(report.getReportId());
//		if(!vecReportEdition.contains(report.getReportEdition()))
//			vecReportEdition.addElement(report.getReportEdition());
//	}

//AppleWood's implement...
ReportMain reportMain=new ReportMain();
Vector vRM=reportMain.query();

Vector vecBranchId=new Vector();
Vector vecReportName=new Vector();
Vector vecReportId=new Vector();
Vector vecReportEdition=new Vector();
for(int i=0;i<vRM.size();i++)
{
  ReportMain report=(ReportMain)vRM.elementAt(i);
  if(!vecBranchId.contains(report.getBranchId()))
    vecBranchId.addElement(report.getBranchId());
  if(!vecReportId.contains(report.getReportId()))
  {
    vecReportId.addElement(report.getReportId());
    vecReportName.addElement(report.getReportName());  //<---name at here
  }
  if(!vecReportEdition.contains(report.getReportEdition()))
    vecReportEdition.addElement(report.getReportEdition());
}



%>
<head >
  <LINK href="./Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
    function loadBranch()
    {
     var branch=document.all.branch;
     var report=document.all.code;
     var edition=document.all.edition;
     <%
     for(int i=0;i<vecBranchId.size();i++)
     {
     %>
       var branchoption=document.createElement("option");
       branchoption.innerHTML="<%=(String)vecBranchId.elementAt(i)%>";
       branchoption.value="<%=(String)vecBranchId.elementAt(i)%>";
       branch.appendChild(branchoption);
     <%}%>
     <%
     for(int i=0;i<vecReportId.size();i++)
     {
     %>
       var reportoption=document.createElement("option");
       reportoption.innerHTML="<%=(String)vecReportName.elementAt(i)%>";
       reportoption.value="<%=(String)vecReportId.elementAt(i)%>";
       report.appendChild(reportoption);
     <%}%>
     <%
     for(int i=0;i<vecReportEdition.size();i++)
     {
     %>
       var editionoption=document.createElement("option");
       editionoption.innerHTML="<%=(String)vecReportEdition.elementAt(i)%>";
       editionoption.value="<%=(String)vecReportEdition.elementAt(i)%>";
       edition.appendChild(editionoption);
     <%}%>
    }

    function submitForm()
    {
    	if(document.all.code.value=='month4')
    	{
    		fm.action="./min1calculate.jsp";
    	}
    	else if(document.all.code.value=='month3')
    	{
    		fm.action="./min2calculate.jsp";
    	}
    	fm.submit();
    }

  </script>
</head>
  <body onload="loadBranch()">
    <form action="./calculate.jsp" method=post name=fm>

      <table align="right" class= "common">
       <tr>
      <td height="100"></td>
      </tr>
      	<tr class="common">
      	<td width="30%"></td>
        	<td height="30"  width="15%">
        		<font size="3">计算报表</font>
        	</td>
        	<td>
        	</td>
        </tr>
        <TR class= "common"><td width="30%"></td>
          <TD class= "title" width="10%">
            单位代码
          </TD>
          <TD class= "input">
            <select class= "code" name="branch">
            </select>
          </TD>
          <td width="10%"></td>
          </tr>
			<tr class= "common"><td width="30%"></td>
          <TD class= "title" width="10%">
            报表代码
          </TD>
          <TD class= "input">
            <select class= "code" name="code" />
          </TD>
          <td width="10%"></td>
          </tr>
          <tr class= "common"><td width="30%"></td>
          <TD class= "title" width="10%">
            报表版别
          </TD>
          <TD class= "input">
            <select class= "code" name="edition" />
          </TD>
          <td width="10%"></td>
          </tr>


        <TR class= "common"><td width="30%"></td>
        	<TD class="title" width="10%">
        		开始时间
        	</TD>
        	<TD class="input" style="width:20">
        		<input class="code" name="time" value="<%=Str.getDate()%>">
        	</TD><td width="10%"></td>


          <TR class= "common"><td width="30%"></td>
                <TD class="title" width="10%">
                        结束时间
                </TD>
                <TD class="input" style="width:20">
                        <input class="code" name="timeEnd" value="">
        	</TD><td width="10%"></td>

        </tr>
        <TR class= "common">
        <td width="30%"></td>
        <td></td>
        	<TD class="button" width="10%" align="left">
        		<img src='../../common/images/butCalculate.gif' alt="计算"  align="right" width="65" height="23" class="button"
				onclick="submitForm()"
				onmouseover=""
				onmouseout=""></img>
        	</TD><td width="35%"></td>
        </tr>
      </table>
  </form>
</body>
</html>
