<%@ page contentType="text/html; charset=GBK" %>
<%@ page import="com.sinosoft.lis.pubfun.*" %>
<%@ page import="com.sinosoft.xreport.bl.*" %>
<%@ page import="com.sinosoft.xreport.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="org.apache.log4j.*" %>
<%@ include file="init.jsp"%>
<%--@todo: 计算单位取登录单位,操作员取登录操作员--%>
<%!
  boolean isEmpty(String in)
  {
    return null==in||"".equals(in);
  }

  static final String DATASOURCE="DataSource";
  static final String ENVIRONMENT="Environment";
  static final String TABLERELATIONS="TableRelations";


  org.apache.log4j.Logger log=XTLogger.getLogger(this.getClass());

%>
<%

  log.debug(SysConfig.TRUEHOST+":"+SysConfig.FILEPATH);

//  if(true)
//  	return;

  try
{

  Class.forName("oracle.jdbc.driver.OracleDriver");
 

  long time=Calendar.getInstance().getTime().getTime();

  String defineBranch=request.getParameter("branch");
  System.out.println("branch :" + defineBranch);
  String defineCode=request.getParameter("code");
  System.out.println("code :" + defineCode);
  String defineEdition=request.getParameter("edition");
  System.out.println("edition :" + defineEdition);  
  String calculateDate=request.getParameter("time");  //计算时间
  //calculateDate = "2003-12-01";
  System.out.println("calculateDate : " + calculateDate);
  String calculateDateEnd=request.getParameter("timeEnd"); //03/05/15 第二时间
  System.out.println("calculateDateEnd : " + calculateDateEnd);

  if(isEmpty(defineBranch)||isEmpty(defineCode)||isEmpty(defineEdition))
  {
    out.println("err|参数不全.\n定义单位,代码,版别为必需的参数");
    return;
  }

  //数据层对象
  com.sinosoft.xreport.dl.DataSource dataSource=(com.sinosoft.xreport.dl.DataSource) session.getAttribute(DATASOURCE);
  if(dataSource==null)
  {
    dataSource=new com.sinosoft.xreport.dl.BufferDataSourceImpl();
    session.setAttribute(DATASOURCE,dataSource);
  }
  
  System.out.println("1");
  
  //环境对象
  Environment environment=(Environment) session.getAttribute(ENVIRONMENT);
  if(environment==null)
  {
    environment=new EnvironmentImpl();
    session.setAttribute(ENVIRONMENT,environment);
  }

  System.out.println("2");
  
  //table关联对象,因为耗时长,变动少,所以作缓冲
  TableRelations tableRelations=(TableRelations) session.getAttribute(TABLERELATIONS);
  if(tableRelations==null)
  {
    tableRelations=new TableRelations(dataSource);
    session.setAttribute(TABLERELATIONS,tableRelations);
  }

  System.out.println("3");
  
  //@todo:登录单位,操作员,权限, add here
  GlobalInput gi=(GlobalInput)session.getAttribute("GI");

  if(null==gi)
  {
    out.println("err|超时.需要重新登录");
    return;
  }

  String operatorID=gi.Operator;  //操作员ID
  String calculateBranch=gi.ManageCom; //登陆单位ID

//  String operatorID="001";  //操作员ID
//  String calculateBranch="86"; //登陆单位ID


  environment.setEnv(Environment.OPERATORID,operatorID);
  environment.setEnv(Environment.CALCBRANCH,calculateBranch);

  //计算时间.
  if(!isEmpty(calculateDate))
    environment.setEnv(Environment.CALCDATE,calculateDate);
  //没有设定,就是当前时间,见EnvironmentImpl

  if(!isEmpty(calculateDateEnd))  //第二个时间也来了,有看头
  {
    environment.setEnv(Environment.CALCDATEEND,calculateDateEnd);
  }


  //报表对象
  Report r=new Report(defineBranch,defineCode,defineEdition);
  r.setRunEnv(environment);

  System.out.println("4");
  
  //设定db对象╭∩╮（︶︿︶）╭∩╮
  r.setDbDataSource(dataSource);
  r.setTableRelations(tableRelations);

  r.init();
  r.calculate(); //it's high cost!
  ReportWriter rw=new ReportWriter();
  ReportWriter rwExcel=new ExcelWriter(); //写到excel,当然可以写到xml,html...
  rw.setEnvironment(environment);
  rwExcel.setEnvironment(environment);
  r.write(rw); //写到xml
  
  System.out.println("5");
  
  try {
    r.write(rwExcel);
    System.out.println("6");
  }catch (Exception ex)
  {
    log.error("写入Excel文件时出错,请检查格式文件是否存在.exmsg:"+ex.getMessage());
    System.out.println("7");
  }

  CalculateLog.save();

  System.out.println("8");
  time=Calendar.getInstance().getTime().getTime()-time;
  System.out.println("ok|计算成功.");
  out.println("ok|计算成功.");
  System.out.println("9");

  log.info("......calcuate.."+r.getReportData().getDataInfo().getDefineFilePath()+"..total time.."+time);
  
  System.out.println("10"); 
  }catch(Exception ex)
  {
    
    out.println("err|计算失败.查看日志文件,获取更多信息.errmsg:");
    ex.printStackTrace(new PrintWriter((Writer)out));
    log.error(ex.getMessage(),ex.fillInStackTrace());
  }

%>