<%@ page contentType="text/html; charset=GBK" %>
<%@ page import="com.sinosoft.lis.*" %>
<%@ page import="com.sinosoft.lis.pubfun.*" %>
<%@ page import="com.sinosoft.xreport.bl.*" %>
<%@ page import="com.sinosoft.xreport.util.*" %>
<%@ page import="com.sinosoft.utility.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.net.*" %>
<%@ page import="org.apache.log4j.*" %>

<%!




  static final String DATASOURCE="DataSource";
  static final String ENVIRONMENT="Environment";
  static final String TABLERELATIONS="TableRelations";
  boolean isEmpty(String in)
  {
    return null==in||"".equals(in);
  }
  String defineBranch="86";
        String defineCode="";
        String CodeName="";
        String defineEdition="20030401";
        String time="";
        String timeend="";

        String CurrentMonth = "";
        String firstDayDate="";
        String lastDayDate="";


%>
<%

    SysConfig.FILEPATH="E:\\lis\\xreport_data\\";
    SysConfig.FUNCTIONFILE="E:\\lis\\xreport_data\\conf\\function.bsh";
    SysConfig.TRUEHOST="http://10.0.16.12:8080/ui/xreport/jsp/";

    XTLogger xl=new XTLogger();
    org.apache.log4j.Logger log=XTLogger.getLogger(xl.getClass());

    log.debug(SysConfig.TRUEHOST+":"+SysConfig.FILEPATH);

    try{

      ReportCalculate rc=new ReportCalculate();
      Class.forName("oracle.jdbc.driver.OracleDriver");

      com.sinosoft.xreport.dl.DataSource dataSource=new com.sinosoft.xreport.dl.BufferDataSourceImpl();
      Environment environment=new EnvironmentImpl();
      TableRelations tableRelations=new TableRelations(dataSource);

      ReportMain reportMain=new ReportMain();
      Vector vRM=reportMain.query();
      Vector vecBranchId=new Vector();
      Vector vecReportName=new Vector();
      Vector vecReportId=new Vector();
      Vector vecReportEdition=new Vector();

      for(int i=0;i<vRM.size();i++)
      {
        



        ReportMain report=(ReportMain)vRM.elementAt(i);
        defineCode=report.getReportId();
        CodeName = report.getReportName();



        //日报

        if((defineCode.equals("day1"))||
           (defineCode.equals("day2"))||
           (defineCode.equals("agent"))||
           (defineCode.equals("dayForPerson"))||
           (defineCode.equals("dayForPersonItem")))
        {

          String CurrentDate = PubFun.getCurrentDate();
          time= CurrentDate;

          String calculateBranch="86";
          environment.setEnv(Environment.CALCBRANCH,calculateBranch);


          if(!isEmpty(time))
            environment.setEnv(Environment.CALCDATE,time);

          if(!isEmpty(timeend)){
            environment.setEnv(Environment.CALCDATEEND,timeend);

          }
          Report r=new Report(defineBranch,defineCode,defineEdition);
          r.setRunEnv(environment);
          r.setDbDataSource(dataSource);
          r.setTableRelations(tableRelations);
          r.init();
          r.calculate();
          ReportWriter rw=new ReportWriter();
          ReportWriter rwExcel=new ExcelWriter();
          rw.setEnvironment(environment);
          rwExcel.setEnvironment(environment);
          r.write(rw);
          try {
            r.write(rwExcel);
          }
          catch (Exception ex){
            log.error("写入Excel文件时出错.请检查格式文件是否存在.exmsg:"+ex.getMessage());
          }
          CalculateLog.save();

        }



        //月报

        if((defineCode.equals("month1"))
           ||(defineCode.equals("month2"))
           ||(defineCode.equals("month301"))
           ||(defineCode.equals("month302"))
           ||(defineCode.equals("month303")))
       {
          System.out.println("gx");
          String CurrentDate = PubFun.getCurrentDate();
          FDate fDate = new FDate();
          Date CurDate = fDate.getDate(CurrentDate);
          GregorianCalendar mCalendar = new GregorianCalendar();
          mCalendar.setTime(CurDate);
          int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
          int Years = mCalendar.get(Calendar.YEAR);

          if (Months < 10){
              firstDayDate =Years + "-0" + Months + "-01";


          }else{
              firstDayDate =Years + "-" + Months + "-01";

          }
          time=firstDayDate;
          System.out.println(firstDayDate);
          String calculateBranch="86";
          environment.setEnv(Environment.CALCBRANCH,calculateBranch);

        //计算时间.
          if(!isEmpty(time))
            environment.setEnv(Environment.CALCDATE,time);

          if(!isEmpty(timeend)){
            environment.setEnv(Environment.CALCDATEEND,timeend);

          }

          Report r=new Report(defineBranch,defineCode,defineEdition);
          r.setRunEnv(environment);
          r.setDbDataSource(dataSource);
          r.setTableRelations(tableRelations);
          r.init();
          r.calculate();
          ReportWriter rw=new ReportWriter();
          ReportWriter rwExcel=new ExcelWriter();
          rw.setEnvironment(environment);
          rwExcel.setEnvironment(environment);
          r.write(rw);
          try {
            r.write(rwExcel);
          }
          catch (Exception ex){
            log.error("写入Excel文件时出错.请检查格式文件是否存在.exmsg:"+ex.getMessage());
          }
          CalculateLog.save();
        //log.println(r.getReportData().getDataInfo().getDefineFilePath());

        }

        if(defineCode.equals("month4"))

        {
          System.out.println("gx");
          String CurrentDate = PubFun.getCurrentDate();
          FDate fDate = new FDate();
          Date CurDate = fDate.getDate(CurrentDate);
          GregorianCalendar mCalendar = new GregorianCalendar();
          mCalendar.setTime(CurDate);
          int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
          int Years = mCalendar.get(Calendar.YEAR);

          if (Months < 10){
              firstDayDate =Years + "-0" + Months + "-01";


          }else{
              firstDayDate =Years + "-" + Months + "-01";

          }
          time=firstDayDate;
          System.out.println(firstDayDate);
          String calculateBranch="86";
          environment.setEnv(Environment.CALCBRANCH,calculateBranch);

        //计算时间.
          if(!isEmpty(time))
            environment.setEnv(Environment.CALCDATE,time);

          if(!isEmpty(timeend)){
            environment.setEnv(Environment.CALCDATEEND,timeend);

          }

          Report r=new Report(defineBranch,defineCode,defineEdition +"_gens");
          r.setRunEnv(environment);
          r.setDbDataSource(dataSource);
          r.setTableRelations(tableRelations);
          r.init();
          r.calculate();
          ReportWriter rw=new ReportWriter();
          ReportWriter rwExcel=new ExcelWriter();
          rw.setEnvironment(environment);
          rwExcel.setEnvironment(environment);
          r.write(rw);
          try {
            r.write(rwExcel);
          }
          catch (Exception ex){
            log.error("写入Excel文件时出错.请检查格式文件是否存在.exmsg:"+ex.getMessage());
          }
          CalculateLog.save();
          //log.println(r.getReportData().getDataInfo().getDefineFilePath());

        }


        //季报
       if (defineCode.equals("rzquarter"))

       {
         String CurrentDate = PubFun.getCurrentDate();
         FDate fDate = new FDate();
         Date CurDate = fDate.getDate(CurrentDate);
         GregorianCalendar mCalendar = new GregorianCalendar();
         mCalendar.setTime(CurDate);
         int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
         int Years = mCalendar.get(Calendar.YEAR);
         int Quarter = (Months - 1) / 3 + 1;  //计算当前季度
         int endmonth = Quarter * 3;
         int firstmonth = endmonth - 2;
         String firstday ="";
         String lastday = "";
         firstday=Years + "-" + firstmonth + "-01";
         if ((Quarter == 1 )|| (Quarter == 4)){
           lastday =Years + "-" + firstmonth + "-31";
         }else{
           lastday =Years + "-" + firstmonth + "-30";
         }
         time=firstday;
         timeend=lastday;
         System.out.println(time);
         System.out.println(timeend);

         String calculateBranch="86";
         environment.setEnv(Environment.CALCBRANCH,calculateBranch);

         //计算时间.
         if(!isEmpty(time)){
           environment.setEnv(Environment.CALCDATE,time);
           System.out.println("gx");
         }
         if(!isEmpty(timeend)){
           environment.setEnv(Environment.CALCDATEEND,timeend);
           System.out.println("gx");
         }
         Report r=new Report(defineBranch,defineCode,defineEdition);
         r.setRunEnv(environment);
         r.setDbDataSource(dataSource);
         r.setTableRelations(tableRelations);
         r.init();
         r.calculate();
                 System.out.println(time);
         System.out.println(timeend);
         ReportWriter rw=new ReportWriter();
         ReportWriter rwExcel=new ExcelWriter();
         rw.setEnvironment(environment);
         rwExcel.setEnvironment(environment);
         r.write(rw);
         try {
           r.write(rwExcel);
         }
         catch (Exception ex){
           log.error("写入Excel文件时出错.请检查格式文件是否存在.exmsg:"+ex.getMessage());
         }
         CalculateLog.save();


       }

      }


    }catch(Exception ex){
      System.out.println("err|计算失败.查看日志文件.获取更多信息.errmsg:");
    }


    


%>












<form action="./c.jsp" method=post name=fm >

   <Div id= divCmdButton>
     <INPUT VALUE="test" TYPE=submit >  
     
  </Div>  
</form>