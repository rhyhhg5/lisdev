<%@ page import="weblogic.management.MBeanHome,weblogic.management.Helper,weblogic.management.RemoteMBeanServer,weblogic.management.runtime.*,javax.management.*" %>

<html>
<head>
<title>JMX Test</title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
</head>
<body bgcolor="#ffffff" text="#000000">



<%


    // The client obtains the MBeanHome of the administration server using the
    // Helper class.
    MBeanHome mbh = Helper.getMBeanHome("system", "weblogic","t3:10.0.5.6:7001","myserver");


    String poolName = "ConnectionPoolName";

    JDBCConnectionPoolRuntimeMBean runtimeMBean = (JDBCConnectionPoolRuntimeMBean)mbh.getRuntimeMBean(poolName,
                "JDBCConnectionPoolRuntime");
                
    int conn_num = runtimeMBean.getActiveConnectionsCurrentCount();
    out.println("current connections:"+conn_num);
    
    conn_num = runtimeMBean.getConnectionLeakProfileCount(); 
    out.println("current Leak connections:"+conn_num);
    
   
    runtimeMBean.reset();
    conn_num = runtimeMBean.getActiveConnectionsCurrentCount();
    out.println("current connections:"+conn_num);


%>

</body>
</html>