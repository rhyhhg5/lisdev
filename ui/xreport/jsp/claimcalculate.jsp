<%
//程序名称:理赔报表打印(初始值）
//程序功能:各模块的清单类型的报表打印  
//创建日期：2003-8-27
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
//            guoxiang    2003-10-15   承保统计的报表也可公用该文件，
//                                     该文件名claimcalculate不仅仅
//                                     指理赔报表还包括其它打印报表 
//其中：claim 指理赔  uw 指承保
// * <p>claim3：险种赔付状况</p>
// * <p>claim12：核赔人工作量统计</p>
// * <p>claim13：理赔月结案清单（分审核人）</p>
// * <p>claim14：理赔月未结案清单（分审核人）</p>
// * <p>claim2：人身险赔付率</p>
// * <p>claim8：临时保单出险状况</p>
// * <p>claim9：短险保单出险状况</p>
// * <p>claim5：七日结案率</p>
// * <p>claim6：三日理赔调查完成率</p>
// * <p>claim10：人均案件状况（分审核人）</p>
// * <p>claim11：机构理赔进度月报（分审核人）</p>
// * <p>uw1：保险体检件统计报表</p>
// * <p>uw2: 撤单件统计报表</p>
// * <p>uw3: 拒保延期件统计报表</p>
// * <p>uw4: 职业分布统计报表</p>
// * <p>uw5: 高保额件分布状况统计报表</p>
// * <p>uw6: 高保额件明细清单</p>
// * <p>uw7: 承保工作效率统计表</p>
// * <p>uw8: 新单统计表</p>
// * <p>uw9: 问题件统计表</p>
// * <p>uw10: 核保师工作量统计表</p>
%>

<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.vdb.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
    String Code = request.getParameter("Code");
    String CodeName ="";
    System.out.println("url的参数为:"+Code);
    String CurrentDate = PubFun.getCurrentDate();
    String mDay[]=PubFun.calFLDate(CurrentDate);
    LDMenuDB tLDMenuDB = new LDMenuDB();
    String name="../xreport/jsp/claimcalculate.jsp?Code="+Code;
    tLDMenuDB.setRunScript(name);
    LDMenuSet mLDMenuSet=tLDMenuDB.query();
    for (int i=1;i<=mLDMenuSet.size();i++){
      LDMenuSchema tLDMenuSchema = mLDMenuSet.get(i);
       CodeName=tLDMenuSchema.getNodeName();
    }  
      
%>

<html> 
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../../common/javascript/Common.js"></SC RIPT>
  <SCRIPT src="../../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/EasyQuery.js"></SCRIPT>
  <LINK href="Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
   function submitForm(){
      var code="<%=Code%>";
      if (fm.StationCode.value.length ==0){
	       alert("统计区域没选择！！！！");
	       return false;
      }
      if ((fm.StartTime.value.length ==0)||(fm.EndTime.value.length ==0)){
          alert("时间没有选择！！！！");
          return false;
      }
      if(code=="claim1"||code=="claim2"||code=="claim3"||code=="claim4"
         ||code=="claim5"||code=="claim6"||code=="claim7"
         ||code=="claim8"||code=="claim9"||code=="claim12"
         ||code=="claim13"||code=="claim14"){
	  
	     fm.target = "f1jprint";
	     fm.action="./ClaimReportF1Print.jsp";
      }
      if(code=="claim10"||code=="claim11"||code=="claim15"){
	     fm.target = "f1jprint";
	     fm.action="./ClaimStatisticF1Print.jsp";
      }
      if(code=="uw1"||code=="uw2"||code=="uw3"||code=="uw4"||code=="uw5"||code=="uw6"||code=="uw7"||code=="uw8"||code=="uw9"||code=="uw10"){
          fm.target = "f1jprint";
          fm.action="./UWF1Print.jsp";
      }
      fm.submit();
   }  
   function changeMode(objCheck){

	if(objCheck.checked == true) 
	   fm.RiskCode.disabled = true;
    else 
	   fm.RiskCode.disabled = false;
   }
  
</script>
</head>
<body>
<form method=post name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>
            报表名称
          </TD>  
          <TD class= title>
             <input readonly class=common name=CodeName value=<%=CodeName%>>
          </TD>
          <TD class= input style="display: none">
            <Input class=common name=Code value = <%=Code%> >
          </TD>
      </TR>
<%     
if(Code.equals("claim10")||Code.equals("claim11")||Code.equals("claim15")){  
%>      
     <TR class = common disabled>    
          <TD class=title>
            统计区域
          </TD>          
          <TD class=input>    
              <input type="text" class="common" name=StationCode value="86" disabled>
          </TD>
      </TR>
      <TR class =common>    
          <TD class= title>
            开始时间
          </TD>          
          <TD class=input> 
            <Input class="coolDatePicker" dateFormat="short" verify="起始时间|NOTNULL" name=StartTime value="<%=mDay[0]%>"> 
          </TD>
          <TD class =title> 
            结束时间
          </TD>
          <TD class=input>  
            <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL" name=EndTime value="<%= mDay[1]%>" >
          </TD>
      </TR>
<%
}else{
     if(Code.equals("uw1")||Code.equals("uw2")||Code.equals("uw3")||Code.equals("uw4")||
        Code.equals("uw5")||Code.equals("uw6")||Code.equals("uw8")||Code.equals("uw9")||Code.equals("uw10")){
%>    
     <TR class = common>    
          <TD class=title>
            统计区域
          </TD>          
          <TD class=input>    
              <input type="text" class="code" name=StationCode onDblClick="showCodeList('station',[this],null,null,codeSql,'1',null,250);"  onKeyUp="return showCodeListKey('station',[this],null,null,codeSql,'1',null,250);">
          </TD>       
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskCode ondblclick="showCodeList('RiskInd',[this]);" onkeyup="return showCodeListKey('RiskInd',[this]);">
          </TD>
      </TR>
      <TR class =common>    
          <TD class= title>
            开始时间
          </TD>          
          <TD class=input> 
            <Input class="coolDatePicker" dateFormat="short" verify="起始时间|NOTNULL" name=StartTime> 
          </TD>
          <TD class =title> 
            结束时间
          </TD>
          <TD class=input>  
            <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL" name=EndTime>
          </TD>
      </TR>
     <TR class =common>    
          <TD class= title>
            统计全部主险
          </TD>  
          <TD class= title>
            <Input type="checkbox" name=RiskFlag onclick="changeMode(this)">是
          </TD>  
     </TR>


<%
    }else{
         if(Code.equals("uw7")){    
            GlobalInput tG = new GlobalInput();
            tG = (GlobalInput)session.getValue("GI"); 
%>
      <TR class = common>    
          <TD class=title>
            统计区域
          </TD>          
          <TD class=common>    
              <input type="text" readonly class="common" name=StationCode value="<%=tG.ManageCom%>" >
          </TD>       
          <Input class="code" name=RiskCode style="display: none" value="000000">
      </TR>
      
      
      
<%
         }else{    
%>     
     <TR class = common>    
          <TD class=title>
            统计区域
          </TD>          
          <TD class=input>    
              <input type="text" class="code" name=StationCode onDblClick="showCodeList('station',[this],null,null,codeSql,'1',null,250);"  onKeyUp="return showCodeListKey('station',[this],null,null,codeSql,'1',null,250);">
          </TD>       
      </TR>
<%     
      }
%>      
     <TR class =common>    
          <TD class= title>
            开始时间
          </TD>          
          <TD class=input> 
            <Input class="coolDatePicker" dateFormat="short" verify="起始时间|NOTNULL" name=StartTime> 
          </TD>
          <TD class =title> 
           结束时间
          </TD>
          <TD class=input>  
            <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL" name=EndTime>
          </TD>
      </TR>
<%        
   }
}
%>    
    </Table>  
    <Div id= divCmdButton style="display: ''">
       <INPUT class=common  VALUE="打印报表" TYPE=button onclick="submitForm()">     
    </Div>
 </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"> </span> 
</body>
</html>
<script>
var codeSql = "1  and code like #"+<%=Branch%>+"%#";
</script>