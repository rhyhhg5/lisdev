<%
//程序名称:扫描报表打印(初始值）
//程序功能:扫描报表的清单类型的报表打印  
//创建日期：2003-11-11
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
//其中
// * <p>prt：扫描打印表格</p>
%>
<%@include file="../../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.vdb.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    String strCurTime = PubFun.getCurrentDate();     
    String Code = request.getParameter("Code");
    String CodeName ="";
    System.out.println("参数为:"+Code);
    String CurrentDate = PubFun.getCurrentDate();
    String mDay[]=PubFun.calFLDate(CurrentDate);
    LDMenuDB tLDMenuDB = new LDMenuDB();
    String name="../xreport/jsp/ScanCalculate.jsp?Code="+Code;
    tLDMenuDB.setRunScript(name);
    LDMenuSet mLDMenuSet=tLDMenuDB.query();
    for (int i=1;i<=mLDMenuSet.size();i++){
      LDMenuSchema tLDMenuSchema = mLDMenuSet.get(i);
       CodeName=tLDMenuSchema.getNodeName();
    }  
      
%>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../../common/javascript/Common.js"></SC RIPT>
  <SCRIPT src="../../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/EasyQuery.js"></SCRIPT>
  <LINK href="Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
   function submitForm(){
      var code="<%=Code%>";
      if (fm.RptObj.value.length ==0){
          alert("险类没有选择！！！！");
          return false;
      }
      if ((fm.StartTime.value.length ==0)||(fm.EndTime.value.length ==0)){
          alert("时间没有选择！！！！");
          return false;
      }
      if(code=="prt"){
          
          fm.target = "f1jprint";
          fm.action="./ScanF1Print.jsp";
          
      }
      fm.submit();
   }  
  </script>
</head>
<body>
<form method=post name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>
            报表名称
          </TD>  
          <TD class= title>
             <input readonly class=common name=CodeName value=<%=CodeName%>>
          </TD>
          <TD class= input style="display: none">
            <Input class=common name=Code value = <%=Code%> >
          </TD>
      </TR>
      <TR class =common>    
          <TD class= title>
            扫描开始时间
          </TD>          
          <TD class=input> 
            <Input class="coolDatePicker" dateFormat="short" verify="起始时间|NOTNULL" name=StartTime value = '<%= strCurTime %>'> 
          </TD>
          <TD class =title> 
            扫描结束时间
          </TD>
          <TD class=input>  
            <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL" name=EndTime value = '<%= strCurTime %>'>
          </TD>
      </TR>   
      <TR class=common> 
         <TD class=title>
            管理机构
          </TD>          
          <TD class=input>    
              <input  class="codeno" name=StationCode onDblClick="showCodeList('station',[this,StationCodeName],[0,1]);"  onKeyUp="return showCodeListKey('station',[this,StationCodeName],[0,1]);"><input class=codename name =StationCodeName>
          </TD>    
         <TD class= title>
            操作员
         </TD>
         <TD class= input>
            <Input class=common name=ScanOperor>
         </TD>
     </TR>
     <TR class=common>    
         <TD  class= title>
            险种类型
         </TD>
         <TD  class= input>
            <Input class="codeno" name=RptObj verify="险种类型|NOTNULL" CodeData="0|^0|个单^1|团单^2|银代^3|简易保单" ondblClick="showCodeListEx('RptObjReport',[this,RptObjName],[0,1,2]);" onkeyup="showCodeListKeyEx('RptObjReport',[this,RptObjName],[0,1,2]);"><input class=codename name =RptObjName>
         </TD>
      </TR>
      
      
    </Table>  
    <Div id= divCmdButton style="display: ''">
       <INPUT class=common  VALUE="打印报表" TYPE=button onclick="submitForm()">     
    </Div>
 </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"> </span> 
</body>
</html>