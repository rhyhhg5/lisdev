<%
//程序名称:扫描报表打印
//程序功能：
//创建日期：2003-11-11
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    System.out.println("开始执行打印操作");
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");

    //报表种类
    String Code = request.getParameter("Code");
    System.out.println("报表类型代码:"+Code);
    //险种：这里是指区分个险，团险，银代
    String mRiskCode="";
    mRiskCode=request.getParameter("RptObj");
    //时间
    String mDay[]=new String[2];
    mDay[0]=request.getParameter("StartTime");
    mDay[1]=request.getParameter("EndTime");
    //机构
    String gi="";
    gi=request.getParameter("StationCode");
    //操作员：扫描操作员
    String ScanOperor=request.getParameter("ScanOperor");
    //操作
    String strOperation ="PRINTPAY";
    VData tVData = new VData();
    VData mResult = new VData();
    tVData.addElement(mRiskCode);
    tVData.addElement(mDay);
    tVData.addElement(gi);
    tVData.addElement(ScanOperor);
    tVData.addElement(Code);
    tVData.addElement(tG);
    CError cError = new CError( );
    CErrors mErrors = new CErrors();
    XmlExport txmlExport = new XmlExport();
    
    ScanCheckUI tScanCheckUI = new ScanCheckUI();
    if(!tScanCheckUI.submitData(tVData,strOperation)){
      mErrors.copyAllErrors(tScanCheckUI.mErrors);
      cError.moduleName = "ScanF1Print";
      cError.functionName = "submitData";
      cError.errorMessage = "tScanCheckUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tScanCheckUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null){
       System.out.println("null没有正确的打印数据！！！");
    }else{
       session.putValue("PrintStream", txmlExport.getInputStream());
       System.out.println("put session value");
       response.sendRedirect("../../f1print/GetF1Print.jsp");
    }

%> 
<html>
  <script language="javascript">
    top.opener.focus();
    top.close();
   </script>
</html>

