<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//Name:ReportLCPolInit.jsp
//Function:初始化报案中的保单信息的查询界面
//Author:LiuYansong
%>
<script language="JavaScript">
var turnPage = new turnPageClass();
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在ReportLCPolInputInit.jsp-->InitInpBox函数中发生异常：初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在ReportLCPolInputInit.jsp-->InitSelBox函数中发生异常：初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initReportLCPolGrid();
    showInsuredLCPolInfo();
  }
  catch(re)
  {
    alert("ReportLCPolInputInit.jsp-->InitForm函数中发生异常111：初始化界面错误!");
  }
}
//立案分案明细
function initReportLCPolGrid()
{
  var iArray = new Array();

  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=40;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="主险保单号码";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="保单号码";
    iArray[2][1]="150px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="保单性质";
    iArray[3][1]="60px";
    iArray[3][2]=100;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="投保人";
    iArray[4][1]="80px";
    iArray[4][2]=100;
    iArray[4][3]=0;

    iArray[5]=new Array();
    iArray[5][0]="被保人";
    iArray[5][1]="100px";
    iArray[5][2]=100;
    iArray[5][3]=0;

    iArray[6]=new Array();
    iArray[6][0]="出险人性质";
    iArray[6][1]="200px";
    iArray[6][2]=100;
    iArray[6][3]=0;

    iArray[7]=new Array();
    iArray[7][0]="出单机构";
    iArray[7][1]="150px";
    iArray[7][2]=100;
    iArray[7][3]=0;

    iArray[8]=new Array();
    iArray[8][0]="类别";
    iArray[8][1]="60px";
    iArray[8][2]=100;
    iArray[8][3]=0;

    iArray[9]=new Array();
    iArray[9][0]="险种名称";
    iArray[9][1]="200px";
    iArray[9][2]=100;
    iArray[9][3]=0;

    iArray[10]=new Array();
    iArray[10][0]="保险金额";
    iArray[10][1]="100px";
    iArray[10][2]=100;
    iArray[10][3]=0;

    iArray[11]=new Array();
    iArray[11][0]="生效日期";
    iArray[11][1]="80px";
    iArray[11][2]=100;
    iArray[11][3]=0;

    iArray[12]=new Array();
    iArray[12][0]="险种状态";
    iArray[12][1]="60px";
    iArray[12][2]=100;
    iArray[12][3]=0;

    iArray[13]=new Array();
    iArray[13][0]="导致状态的原因";
    iArray[13][1]="200px";
    iArray[13][2]=100;
    iArray[13][3]=0;

    ReportLCPolGrid = new MulLineEnter( "fm" , "ReportLCPolGrid" );
    ReportLCPolGrid.displayTitle = 1;
    ReportLCPolGrid.mulLineCount = 1;
    ReportLCPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>