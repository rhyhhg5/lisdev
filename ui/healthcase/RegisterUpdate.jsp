<%
//程序名称：RegisterUpdate.jsp
//程序功能：为"立案"模块的基本功能－修改－准备数据
//创建日期：2002-11-08
//创建人  ：刘岩松
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>


<%

  LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();

  LLCaseSchema tLLCaseSchema=new LLCaseSchema();
  LLCaseSet tLLCaseSet=new LLCaseSet();

  LLRgtAffixSchema tLLRgtAffixSchema=new LLRgtAffixSchema();
  LLRgtAffixSet tLLRgtAffixSet=new LLRgtAffixSet();

  RegisterUpdateUI tRegisterUpdateUI   = new RegisterUpdateUI();
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  VData tVData = new VData();

  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";
  transact = request.getParameter("fmtransact");
  System.out.println("-------transact:"+transact);

    String strRgtNo=request.getParameter("RgtNo");
    String Path = application.getRealPath("config//Conversion.config");
		tLLRegisterSchema.setAccidentReason(StrTool.Conversion(request.getParameter("AccidentReason"),Path));
		tLLRegisterSchema.setAccidentCourse(StrTool.Conversion(request.getParameter("AccidentCourse"),Path));
		tLLRegisterSchema.setRemark(StrTool.Conversion(request.getParameter("Remark"),Path));
    tLLRegisterSchema.setAccidentSite(StrTool.Conversion(request.getParameter("AccidentSite"),Path));
    tLLRegisterSchema.setRgtReason(StrTool.Conversion(request.getParameter("RgtReason"),Path));

    tLLRegisterSchema.setRptNo(request.getParameter("RptNo"));
    tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
    tLLRegisterSchema.setCaseGetMode(request.getParameter("CaseGetMode"));
    tLLRegisterSchema.setRgtantName(request.getParameter("RgtantName"));
    tLLRegisterSchema.setRgtantSex(request.getParameter("Sex"));
    tLLRegisterSchema.setRelation(request.getParameter("Relation"));
    tLLRegisterSchema.setRgtantAddress(request.getParameter("RgtantAddress"));
    tLLRegisterSchema.setRgtantPhone(request.getParameter("RgtantPhone"));
    tLLRegisterSchema.setRgtDate(request.getParameter("RgtDate"));
    tLLRegisterSchema.setAccidentDate(request.getParameter("AccidentDate"));
    tLLRegisterSchema.setAgentCode(request.getParameter("AgentCode"));
    tLLRegisterSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLLRegisterSchema.setMngCom(request.getParameter("MngCom"));
    tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
    tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLRegisterSchema.setIDNo(request.getParameter("IDNo"));
    tLLRegisterSchema.setIDType(request.getParameter("IDType"));
    tLLRegisterSchema.setApplyType(request.getParameter("ApplyType"));
    tLLRegisterSchema.setHandler1(request.getParameter("Handler1"));
    tLLRegisterSchema.setHandler1Phone(request.getParameter("Handler1Phone"));
    tLLRegisterSchema.setAccName(request.getParameter("AccName"));

	  String[] strNumberCase=request.getParameterValues("CaseGridNo");
    String[] strCaseNo=request.getParameterValues("CaseGrid1");
    String[] strCustomerNo=request.getParameterValues("CaseGrid2");
    String[] strCustomerName=request.getParameterValues("CaseGrid3");
    String[] strCustomerSex=request.getParameterValues("CaseGrid4");
    String[] strIDType=request.getParameterValues("CaseGrid5");
    String[] strIDNo=request.getParameterValues("CaseGrid6");
    String[] strCustomerAge=request.getParameterValues("CaseGrid7");
    String[] strAccidentType=request.getParameterValues("CaseGrid8");


    int intLength=0;

	  if(strNumberCase!=null)
	  intLength=strNumberCase.length;
    for(int i=0;i<intLength;i++)
    {
    		tLLCaseSchema=new LLCaseSchema();

    		tLLCaseSchema.setCustomerNo(strCustomerNo[i]);
    		tLLCaseSchema.setCustomerName(strCustomerName[i]);
    		tLLCaseSchema.setCustomerSex(strCustomerSex[i]);
    		tLLCaseSchema.setIDType(strIDType[i]);
    		tLLCaseSchema.setIDNo(strIDNo[i]);
    		tLLCaseSchema.setCustomerAge(strCustomerAge[i]);
    		tLLCaseSchema.setCaseNo(strCaseNo[i]);
        tLLCaseSchema.setAccidentType(strAccidentType[i]);
        tLLCaseSet.add(tLLCaseSchema);
    		System.out.println("CaseNo="+strCaseNo[i]);
    }

    String[] strNumberAffix=request.getParameterValues("AffixGridNo");
    String[] strAffixCode=request.getParameterValues("AffixGrid1");
    String[] strAffixNo=request.getParameterValues("AffixGrid3");

    intLength=0;
	  if(strNumberAffix!=null)
    intLength=strNumberAffix.length;
    for(int i=0;i<intLength;i++)
    {
		tLLRgtAffixSchema=new LLRgtAffixSchema();
		tLLRgtAffixSchema.setAffixCode(strAffixCode[i]);
		tLLRgtAffixSchema.setAffixNo(strAffixNo[i]);
		tLLRgtAffixSet.add(tLLRgtAffixSchema);

    }

		System.out.println("开始9");
	  try
	  {
		   // 准备传输数据 VData

		   //保存报案信息
		  tVData.addElement(tLLRegisterSchema);
   		tVData.addElement(tLLCaseSet);
   		tVData.addElement(tLLRgtAffixSet);
      tVData.addElement(tG);
		 	System.out.println("案件号码是："+tLLRegisterSchema.getRgtNo());
      tRegisterUpdateUI.submitData(tVData,transact);


    } catch(Exception ex) {
	    Content = transact+"失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	  }
	  //如果在Catch中发现异常，则不从错误类中提取错误信息
	  if (FlagStr=="")
	  {
	    tError = tRegisterUpdateUI.mErrors;
	    if (!tError.needDealError())
	    {
	      Content = " 修改成功";
	    	FlagStr = "Succ";
	    	tVData.clear();
	    	tVData = tRegisterUpdateUI.getResult();
	    	LLRegisterSchema yyLLRegisterSchema = new LLRegisterSchema();
	    	yyLLRegisterSchema.setSchema((LLRegisterSchema)tVData.getObjectByObjectName("LLRegisterSchema",0));
	   		%>
	    	<script language="javascript">
	    		parent.fraInterface.fm.all("MngCom").value="<%=yyLLRegisterSchema.getMngCom()%>";
				</script>
			<%
	    }
	    else
	    {
	    	Content = "修改失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  System.out.println("结束");
	  }
	  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>