<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ReportQueryOut2.jsp
//程序功能：
//创建人  ：刘岩松
//创建日期：
//更新记录：  
//更新人
//更新日期
//更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>

<%
  //输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  System.out.println("---hahahahah---");
  
  LCPolSchema tLCPolSchema   = new LCPolSchema();
  LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
  
  String szRgtObjNo = request.getParameter("RgtObjNo");
  
  if( szRgtObjNo == null ) {
    out.println("szRgtObjNo == null");
    return;
  }
    
    if (request.getParameter("RgtObj").equals("0")) 
        tLCPolSchema.setContNo(szRgtObjNo);
    else if(request.getParameter("RgtObj").equals("1")) 
        tLCPolSchema.setGrpPolNo(szRgtObjNo);
    else if (request.getParameter("RgtObj").equals("2")) 
        tLCPolSchema.setPolNo(szRgtObjNo);
    else 
        tLCPolSchema.setInsuredNo(szRgtObjNo);
    //关键是把值传过去。
    tLLRegisterSchema.setRgtObj(request.getParameter("RgtObj"));
    tLLRegisterSchema.setRgtObjNo(request.getParameter("RgtObjNo"));
 
    
    // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLLRegisterSchema);
	tVData.addElement(tLCPolSchema);
	

  RegisterQueryRgtUI tRegisterQueryRgtUI   = new RegisterQueryRgtUI();

	if (!tRegisterQueryRgtUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tRegisterQueryRgtUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tRegisterQueryRgtUI.getResult();
		
		// 显示
		LCPolSet mLCPolSet = new LCPolSet(); 
		mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolBLSet",0));
		int n = mLCPolSet.size();
		System.out.println("get report "+n);
	%>
	   	<script language="javascript">
			parent.fraInterface.CaseGrid.clearData();
		</script>
	
	<%
		for (int i = 1; i <= n; i++)
		{
		  	LCPolSchema mLCPolSchema = mLCPolSet.get(i);
		   	%>
		   	<script language="javascript">
		   	  parent.fraInterface.CaseGrid.addOne("CaseGrid")
		   		parent.fraInterface.CaseGrid.setRowColData(<%=i-1%>,2,"<%=mLCPolSchema.getInsuredNo()%>");
		   		parent.fraInterface.CaseGrid.setRowColData(<%=i-1%>,3,"<%=mLCPolSchema.getInsuredName()%>");
		   	</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tRegisterQueryRgtUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

