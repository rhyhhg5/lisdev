<%
//Name    ：QueryDiseaseInit.jsp
//Function：代码查询程序
//Author  ：LiuYansong
//Date    ：2003-10-14
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
//单击时查询
function RegisterDetailClick(cObj)
{
  var ex,ey;
  ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  divDetailInfo.style.left=ex;
  divDetailInfo.style.top =ey;
  divDetailInfo.style.display ='';
}

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在QueryDiseaseInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在QueryDiseaseInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initQueryCodeGrid();
  }
  catch(re)
  {
    alert("QueryDiseaseInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 报案信息列表的初始化
function initQueryCodeGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="代码";    	//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="名称";         			//列名
    iArray[2][1]="300px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="拼音简称";         			//列名
    iArray[3][1]="100px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    QueryCodeGrid = new MulLineEnter( "fm" , "QueryCodeGrid" );
    QueryCodeGrid.mulLineCount = 10;
    QueryCodeGrid.displayTitle = 1;
    QueryCodeGrid.canSel=1;
    QueryCodeGrid.locked = 1;
    QueryCodeGrid.loadMulLine(iArray);
    QueryCodeGrid.detailInfo="单击显示详细信息";
    QueryCodeGrid.detailClick=RegisterDetailClick;
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>