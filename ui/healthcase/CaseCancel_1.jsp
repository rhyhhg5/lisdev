<%
/*****************************************************************************
 * Name     :CaseCancel.jsp
 * Function :立案撤销的前台传递程序；
 * Date     ：2003-7-24
 * Author   :LiuYansong
 */
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
    <%@page import="com.sinosoft.utility.*"%>
    <%@page import="com.sinosoft.lis.schema.*"%>
    <%@page import="com.sinosoft.lis.vschema.*"%>
    <%@page import="com.sinosoft.lis.db.*"%>
    <%@page import="com.sinosoft.lis.vdb.*"%>
    <%@page import="com.sinosoft.lis.sys.*"%>
    <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.llhealthcase.*"%>
    <%@page import="com.sinosoft.lis.vbl.*"%>
    <%@page contentType="text/html;charset=GBK" %>
<%
  RegisterUI tRegisterUI = new RegisterUI();
  CErrors tError = null;

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String RgtNo = request.getParameter("RgtNo");
  VData tVData = new VData();
  try
  {
    tVData.addElement(RgtNo);
    tRegisterUI.submitData(tVData,"CANCEL");
  }
  catch(Exception ex)
  {
    Content = "案件撤销失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
   if (FlagStr=="")
   {
     tError = tRegisterUI.mErrors;
     if (!tError.needDealError())
     {
       Content = " 案件撤销成功！！！";
       FlagStr = "Succ";
     }
   }
   else
   {
     Content = " 案件撤销失败，原因是:" + tError.getFirstError();
     FlagStr = "Fail";
   }
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>