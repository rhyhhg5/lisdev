<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="QueryDisease.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="QueryDiseaseInit.jsp"%>
  <title>代码查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./QueryDiseaseOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 报案信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLLReport1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            *号码类型
          </TD>
          <TD  class= input>
            <Input class= "code" name=CodeType verify="号码类型|NOTNULL" CodeData="0|^0|疾病代码^1|事故代码" ondblClick="showCodeListEx('CodeListQuery',[this],[0,1]);" onkeyup="showCodeListKeyEx('CodeListQuery',[this],[0,1]);">
          </TD>
          <TD  class= title>
            拼音简称
          </TD>
          <TD  class= input>
            <Input class= common name=SpellCode>
          </TD>
        </TR>

        <TR>
          <TD  class= title>
            代码
          </TD>
          <TD  class= input>
            <Input class= common name=QueryCode>
          </TD>
          <TD class= title>
            名称
          </TD>
          <TD>
            <Input class= common name= QueryName>
          </TD>
        </TR>
        <tr>
        <td>
          <INPUT class=common VALUE="查    询" TYPE=button onclick="submitForm();return false;">
        </td>
        <td>
          <INPUT class=common VALUE="返    回" TYPE=button onclick="returnParent();">
          </td>
          </tr>
</table>
    </Div>

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 详细信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanQueryCodeGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button class=common onclick="getFirstPage();">
      <INPUT VALUE="上一页" TYPE=button class=common onclick="getPreviousPage();">
      <INPUT VALUE="下一页" TYPE=button class=common onclick="getNextPage();">
      <INPUT VALUE="尾  页" TYPE=button class=common onclick="getLastPage();">
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>