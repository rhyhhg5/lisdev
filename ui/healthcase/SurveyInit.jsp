<%
//程序名称：LLSurveyInput.jsp
//程序功能：
//创建日期：2002-07-21 21:03:23
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>                            
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
//单击时查询
function surveyDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divSurveyDetail.style.left=ex;
  	divSurveyDetail.style.top =ey;
    divSurveyDetail.style.display ='';
}


function initInpBox()
{ 
  
  try
  {                                   
    fm.all('CaseNo').value = '';
    fm.all('SubReportNo').value = '';
    fm.all('CustomerNo').value = '';
    fm.all('CustomerName').value = '';
    fm.all('SerialNo').value = '';
    fm.all('Surveyor').value = '';
    fm.all('SurveySite').value = '';
    fm.all('SurveyStartDate').value = '';
    fm.all('SurveyEndDate').value = '';
    fm.all('Content').value = '';
    fm.all('Result').value = '';
    
  }
  catch(ex)
  {
    alert("在LLSurveyInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LLSurveyInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("LLSurveyInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  
}

</script>