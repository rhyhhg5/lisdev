<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput GI = new GlobalInput();
  GI = (GlobalInput)session.getValue("GI");
%>
<script>
  var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
  var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="SurveyQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="SurveyQueryInit.jsp"%>
  <title>调查报告查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./SurveyQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 报案信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLLReport1" style= "display: ''">
      <table  class= common>
        <TR  class= common>

				<TD  class= title>
            立案号
          </TD>
          <TD  class= input>
            <Input class= common name=RgtNo >
          </TD>
            <Input class= common name=ClmCaseNo type=hidden>
            <Input type=hidden class=code name=SurveyFlag verify="调查报告类型" CodeData="0|^0|已提起未接收^1|已接收未调查^2|已调查（调查完成）" ondblClick="showCodeListEx('SuerveyFlagChoose',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('RgtObjRegister',[this],[0,1,2]);">
            <Input type=hidden name=Type >
          <TD  class= title>
            序号/调查次数
          </TD>
          <TD  class= input>
            <Input class= common name=SerialNo >
          </TD>
        </TR>
      </table>
    </Div>
          <INPUT VALUE="查询" class=common TYPE=button onclick="submitForm();return false;"> 
          <INPUT VALUE="返回" class=common TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 调查报告信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanSurveyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button class=common onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button class=common onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button class=common onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" TYPE=button class=common onclick="getLastPage();"> 				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.SurveyFlag.value = '2';
}
catch(ex)
{
  alert(ex);
}

</script>
</html>
