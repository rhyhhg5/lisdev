<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：RgtSurveyQueryOut.jsp
//程序功能：
//创建日期：2002-11-22
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  System.out.println("开始执行RgtSurveyQueryOut.jsp页面");
  // 保单信息部分
  LLSurveySchema tLLSurveySchema = new LLSurveySchema();

  tLLSurveySchema.setClmCaseNo(request.getParameter("ClmCaseNo"));
  tLLSurveySchema.setType(request.getParameter("Type"));
  tLLSurveySchema.setRgtNo(request.getParameter("RgtNo"));
  tLLSurveySchema.setSurveyorName(request.getParameter("SurveyorName"));

     // 准备传输数据 VData
  VData tVData = new VData();
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
	tVData.addElement(tLLSurveySchema);
  tVData.addElement(tG);

  // 数据传输
  RgtSurveyQueryUI tRgtSurveyQueryUI = new RgtSurveyQueryUI();
	if (!tRgtSurveyQueryUI.submitData(tVData,"QUERY"))
	{
      Content = " 查询失败，原因是: " + tRgtSurveyQueryUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tRgtSurveyQueryUI.getResult();

		// 显示
		LLSurveySet mLLSurveySet = new LLSurveySet();
		mLLSurveySet.set((LLSurveySet)tVData.getObjectByObjectName("LLSurveySet",0));
		int n = mLLSurveySet.size();
		System.out.println("get report "+n);
		String Strtest = "0|" + n + "^" + mLLSurveySet.encode();
		System.out.println("Strtest==="+Strtest);
		String Path = application.getRealPath("config//Conversion.config");
		Strtest = StrTool.Conversion(Strtest,Path);
		System.out.println("Path==="+Path);
		System.out.println("Strtest1==="+Strtest);
		   	%>
		   	<script language="javascript">
		   	try {
		   	  parent.fraInterface.displayQueryResult("<%=Strtest%>");
		   	} catch(ex) {}
			</script>

			<%

	} // end of if

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tRgtSurveyQueryUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>