<%@page contentType="text/html;charset=GBK" %>

<%
//Name：ReportQueryOut.jsp
//Function：
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  // 保单信息部分
  LLReportSchema tLLReportSchema   = new LLReportSchema();
  String People=request.getParameter("People");
  tLLReportSchema.setRptNo(request.getParameter("RptNo"));
  tLLReportSchema.setRptObj(request.getParameter("RptObj"));
  tLLReportSchema.setRptObjNo(request.getParameter("RptObjNo"));
  tLLReportSchema.setRptDate(request.getParameter("RptDate"));
	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

	
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.addElement(People);
  tVData.addElement(tG);
	tVData.addElement(tLLReportSchema);
  // 数据传输
  ReportUI tReportUI   = new ReportUI();
	if (!tReportUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tReportUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tReportUI.getResult();
    LLReportSet yLLReportSet = new LLReportSet();
    yLLReportSet.set((LLReportSet)tVData.getObjectByObjectName("LLReportSet",0));
    int n = yLLReportSet.size();
    System.out.println("get report "+n);
		String Strtest ="0|" + n + "^"+yLLReportSet.encode();
		System.out.println("Strtest==="+Strtest);
      %>
      <script language="javascript">
        try {
        parent.fraInterface.displayQueryResult('<%=Strtest%>');
		   	} catch(ex) {}
			</script>
			<%

	} // end of if

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tReportUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>