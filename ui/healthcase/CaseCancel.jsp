<%
//Name：CaseCancel.jsp
//Function：为"立案"模块的基本功能-撤销
//Date：2003-7-25
//Author  ：刘岩松
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>
<%
  RegisterUI tRegisterUI   = new RegisterUI();
  VData tVData = new VData();
  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String RgtNo = request.getParameter("RgtNo");
  String RgtReason = request.getParameter("RgtReason");
  try
  {
    tVData.addElement(RgtNo);
    tVData.addElement(RgtReason);
    if(tRegisterUI.submitData(tVData,"CANCEL"))
    {
      Content = "案件撤销成功";
      FlagStr = "Succ";
    }
    else
    {
      FlagStr = "Fail";
    }
  }
  catch(Exception ex)
  {
    Content = "案件撤销失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("返回的标志是"+FlagStr);
  if (FlagStr=="Fail")
  {
    tError = tRegisterUI.mErrors;
    if (tError.needDealError())
    {
      Content = " 撤销失败,原因是"+tError.getFirstError();
      FlagStr = "Fail";
      tVData.clear();
    }
  }
  else
  {
    Content = "撤销成功！";
    FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>