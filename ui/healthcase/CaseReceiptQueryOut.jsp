<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：CaseReceiptQueryOut.jsp
//程序功能：在"立案"模块中的收据明细中将按照"立案号"查询出的数据显示在"收据明细"界面上的控件上
//创建人  ：刘岩松
//创建日期：2002-11-15
//更新记录：
//更新人:刘岩松
//更新日期:2002-09-25
//         2002-11-01
//更新原因/内容:
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>

<%
    //输出参数
    String Content = "";
    CErrors tError = null;
    String FlagStr = "Fail";
    
    LLCaseReceiptSchema tLLCaseReceiptSchema = new LLCaseReceiptSchema();
    tLLCaseReceiptSchema.setCaseNo(request.getParameter("CaseNo"));
 
    
    
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(tLLCaseReceiptSchema);
    
    CaseReceiptQueryUI tCaseReceiptQueryUI   = new CaseReceiptQueryUI();
    if (!tCaseReceiptQueryUI.submitData(tVData,"QUERY||MAIN"))
    {
    		Content = " 查询失败，原因是: " + tCaseReceiptQueryUI.mErrors.getError(0).errorMessage;
      		FlagStr = "Fail";
    }
    else
    {
		tVData.clear();
		tVData = tCaseReceiptQueryUI.getResult();
		
		LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet();
		mLLCaseReceiptSet.set((LLCaseReceiptSet)tVData.getObjectByObjectName("LLCaseReceiptBLSet",0));
		LLCaseReceiptSchema mLLCaseReceiptSchema = mLLCaseReceiptSet.get(1);
		
     }
    tVData.clear();
    tVData.addElement(tLLCaseReceiptSchema);
    
     %>
<html>
<script language="javascript">
	</script>
</html>