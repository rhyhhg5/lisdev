<%
//Name：RegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">

function initInpBox( )
{
  try
  {
  }
  catch(ex)
  {
    alert("在HeaRegisterInit的初始化时出错!");
  }
}

function initSelBox()
{
  try
  {

  }

  catch(ex)
  {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常yyyyyyy:初始化界面错误!");
  }
}

function initForm()
{
  try
  {

    initInpBox();
    initCustomerGrid();
  }
  catch(re)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//立案附件信息
function initCustomerGrid()
{
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许



      iArray[1]=new Array();
      iArray[1][0]="事故者号码";         			//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="事故者名称";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="保单号码";         		//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[4]=new Array();
      iArray[4][0]="责任开始日期";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[5]=new Array();
      iArray[5][0]="责任终止日期";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用


      iArray[6]=new Array();
      iArray[6][0]="证件类型";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[7]=new Array();
      iArray[7][0]="证件号码";         		//列名
      iArray[7][1]="150px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[8]=new Array();
      iArray[8][0]="性别";         		//列名
      iArray[8][1]="30px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[9]=new Array();
      iArray[9][0]="年龄";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[10]=new Array();
      iArray[10][0]="其他";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用


      CustomerGrid = new MulLineEnter( "fm" , "CustomerGrid" );
      //这些属性必须在loadMulLine前
      CustomerGrid.mulLineCount = 0;
      CustomerGrid.displayTitle = 1;
      CustomerGrid.canSel=1;
      CustomerGrid.loadMulLine(iArray);

  }
      catch(ex)
      {
        alert(ex);
      }

}
//立案分案信息
function initCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="分案号";    	//列名
    iArray[1][1]="0px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="事故者客户号";         			//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="事故者名称";         			//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="性别";         		//列名
    iArray[4][1]="40px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=2;
    iArray[4][4]="Sex";
    //是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[5]= new Array();
    iArray[5][0]="证件类型";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=2;
    iArray[5][4]="IDType";

    iArray[6]= new Array();
    iArray[6][0]="证件号码";
    iArray[6][1]="150px";
    iArray[6][2]=100;
    iArray[6][3]=1;

    iArray[7]= new Array();
    iArray[7][0]="年龄";
    iArray[7][1]="40px";
    iArray[7][2]=100;
    iArray[7][3]=1;

    iArray[8]=new Array();
    iArray[8][0]="事故类型";
    iArray[8][1]="50px";
    iArray[8][2]=100;
    iArray[8][3]=0;

    iArray[9]=new Array();
    iArray[9][0]="分报案号码";
    iArray[9][1]="0px";
    iArray[9][2]=100;
    iArray[9][3]=0;

    CaseGrid = new MulLineEnter( "fm" , "CaseGrid" );
    //这些属性必须在loadMulLine前
    CaseGrid.mulLineCount = 0;
    CaseGrid.displayTitle = 1;
    CaseGrid.canChk =1;
    CaseGrid.hiddenPlus=1;
    CaseGrid.locked = 1;
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>