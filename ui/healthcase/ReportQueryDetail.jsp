<%@page contentType="text/html;charset=GBK" %>

<%
//Name：ReportQueryDetail.jsp
//Function：将选中的信息进行返回
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    String CaseState = "";
    String a_Name="";
    String tRptNo = request.getParameter("RptNo");
    VData tVData = new VData();
    tVData.addElement(tRptNo);
    ReportUI tReportUI   = new ReportUI();
    if (!tReportUI.submitData(tVData,"RETURN"))
    {
    		Content = " 查询失败，原因是: " + tReportUI.mErrors.getError(0).errorMessage;
      		FlagStr = "Fail";
    }
    else
    {
      tVData.clear();
      tVData = tReportUI.getResult();
      LLReportSchema mLLReportSchema = new LLReportSchema();
      LLSubReportSet mLLSubReportSet = new LLSubReportSet();
      CaseState=(String)tVData.get(0);
      a_Name=(String)tVData.get(1);
      mLLReportSchema.setSchema((LLReportSchema)tVData.getObjectByObjectName("LLReportSchema",0));
      mLLSubReportSet.set((LLSubReportSet)tVData.getObjectByObjectName("LLSubReportSet",0));
      String Path = application.getRealPath("config//Conversion.config");
      mLLReportSchema.setAccidentReason(StrTool.Conversion(mLLReportSchema.getAccidentReason(),Path));
      mLLReportSchema.setAccidentCourse(StrTool.Conversion(mLLReportSchema.getAccidentCourse(),Path));
      mLLReportSchema.setRemark(StrTool.Conversion(mLLReportSchema.getRemark(),Path));
     %>
     <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
     <script language="javascript">
       var tAccidentReason = Conversion("<%=mLLReportSchema.getAccidentReason()%>");
       var tAccidentCourse = Conversion("<%=mLLReportSchema.getAccidentCourse()%>");
       var tRemark = Conversion("<%=mLLReportSchema.getRemark()%>");
       top.opener.fm.all("RptNo").value = "<%=mLLReportSchema.getRptNo()%>";
       top.opener.fm.all("RptObj").value = "<%=mLLReportSchema.getRptObj()%>";
       top.opener.fm.all("RptObjNo").value = "<%=mLLReportSchema.getRptObjNo()%>";
       top.opener.fm.all("RptorName").value = "<%=mLLReportSchema.getRptorName()%>";
       top.opener.fm.all("RptorAddress").value = "<%=mLLReportSchema.getRptorAddress()%>";
       top.opener.fm.all("RptorPhone").value = "<%=mLLReportSchema.getRptorPhone()%>";
       top.opener.fm.all("Operator").value = "<%=a_Name%>";
       top.opener.fm.all("Relation").value = "<%=mLLReportSchema.getRelation()%>";
       top.opener.fm.all("RptDate").value = "<%=mLLReportSchema.getRptDate()%>";
       top.opener.fm.all("RptMode").value = "<%=mLLReportSchema.getRptMode()%>";
       top.opener.fm.all("AccidentSite").value = "<%=mLLReportSchema.getAccidentSite()%>";
       top.opener.fm.all("AccidentDate").value = "<%=mLLReportSchema.getAccidentDate()%>";
       top.opener.fm.all("MngCom").value = "<%=mLLReportSchema.getMngCom()%>";
       top.opener.fm.all("AccidentReason").value = tAccidentReason;
       top.opener.fm.all("AccidentCourse").value = tAccidentCourse;
       top.opener.fm.all("Remark").value = tRemark;
       top.opener.fm.all("PeopleType").value = "<%=mLLReportSchema.getCustomerType()%>";
       top.opener.fm.all("CaseState").value = "<%=CaseState%>";
       top.opener.emptyUndefined();
       top.opener.SubReportGrid.clearData();
     </script>
    <%
		int m = mLLSubReportSet.size();
		for(int i=1;i<=m;i++)
		{
			LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
      mLLSubReportSchema=mLLSubReportSet.get(i);
			%>
			<script language="javascript">
			top.opener.SubReportGrid.addOne("SubReportGrid")
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,1,"<%=mLLSubReportSchema.getCustomerNo()%>");
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,2,"<%=mLLSubReportSchema.getCustomerName()%>");
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,3,"<%=mLLSubReportSchema.getHospitalCode()%>");
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,4,"<%=mLLSubReportSchema.getHospitalName()%>");
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,5,"<%=mLLSubReportSchema.getInHospitalDate()%>");
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,6,"<%=mLLSubReportSchema.getOutHospitalDate()%>");
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,7,"<%=mLLSubReportSchema.getRemark()%>");
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,8,"<%=mLLSubReportSchema.getSubRptNo()%>");
			top.opener.SubReportGrid.setRowColData(<%=i-1%>,9,"<%=mLLSubReportSchema.getAccidentType()%>");
		  top.opener.SubReportGrid.checkBoxAll();
		  top.opener.emptyUndefined();
      </script>
    		<%
		} //end of for
	} //end of if

    if (FlagStr == "Fail")
    {
      tError = tReportUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 查询成功! ";
        FlagStr = "Succ";
      }
      else
      {
        Content = " 查询失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }
  System.out.println("------end------");
  System.out.println(FlagStr);
  System.out.println(Content);
  out.println("<script language=javascript>");
  out.println("top.close();");
  out.println("</script>");
%>