<%
//程序名称：RegisterInfQry.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//更新记录：  更新人    更新日期     更新原因/内容

%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
    LLRegisterSchema tLLRegisterSchema   = new LLRegisterSchema();
   tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
   
    
  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLLRegisterSchema);

  // 数据传输
  RegisterUI tRegisterUI   = new RegisterUI();
	if (!tRegisterUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tRegisterUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tRegisterUI.getResult();
		
		// 显示
		LLRegisterSet mLLRegisterSet = new LLRegisterSet(); 
		LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
		mLLRegisterSet.set((LLRegisterSet)tVData.getObjectByObjectName("LLRegisterBLSet",0));
		mLLCasePolicySet.set((LLCasePolicySet)tVData.getObjectByObjectName("LLCasePolicySet",0));
		
		tLLRegisterSchema=mLLRegisterSet.get(0);
		
		
%>
		<script language="javascript">
		   	  parent.fraInterface.fm.Rgt.value="<%=tLLRegisterSchema.getRgtNo()%>";
		</script>

			<%
		int n = mLLCasePolicySet.size();
		
		for (int i = 1; i <= n; i++)
		{
		  	LLCasePolicySchema mLLCasePolicySchema = mLLCasePolicySet.get(i);
		   	
		   	
		   	
		   	
		   	
		   	
		   	
		   	
		   	
		   	
		   	
		   	
		   	%>
		   	<script language="javascript">
		   	  	parent.fraInterface.spanClaimDetailGrid.addOne("spanClaimDetailGrid")
		   		parent.fraInterface.fm.spanClaimDetailGrid1[<%=i-1%>].value="<%=mLLCasePolicySchema.getKindCode()%>";
		   		parent.fraInterface.fm.spanClaimDetailGrid2[<%=i-1%>].value="<%=mLLCasePolicySchema.getAppntNo()%>";
		   		parent.fraInterface.fm.spanClaimDetailGrid3[<%=i-1%>].value="<%=mLLCasePolicySchema.getValidDate()%>";
		   		
			</script>
			<%
		}
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tRegisterUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

