<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llhealthcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  String RgtNo = request.getParameter("RgtNo");
  String OperationType =request.getParameter("OperationType");//若没有手术信息，则该项"N"；手术赔付类型：0按费用；1按档次
  String mzh_flag = "0";//门诊录入信息的标志
  String zhy_flag = "0";//住院录入信息的标志
  String jy_flag = "0";//救援信息录入的标志
  //对分案信息的操作
  LLCaseSchema mLLCaseSchema = new LLCaseSchema();
  mLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
  mLLCaseSchema.setInHospitalDate(request.getParameter("InHospitalDate"));
  mLLCaseSchema.setOutHospitalDate(request.getParameter("OutHospitalDate"));
  mLLCaseSchema.setInHospitalDays(request.getParameter("InHospitalDays"));

  LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet();//分案收据信息存放门诊费用信息
  LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();//分案诊疗信息存放住院信息及手术信息
  CaseCureUI tCaseCureUI   = new CaseCureUI();

  CErrors tError = null;
  String transact = "INSERT";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";

  //门诊信息描述－放在分案收据明细信息中
  String[] strNumber=        request.getParameterValues("ICaseCureGridNo");
  if(strNumber==null)
  {
    mLLCaseReceiptSet.clear();
  }
  else
  {
    String[] strHospitalName = request.getParameterValues("ICaseCureGrid1");
    String[] strFeeItemType  = request.getParameterValues("ICaseCureGrid2");
    String[] strFeeItemCode  = request.getParameterValues("ICaseCureGrid3");
    String[] strFeeItemName  = request.getParameterValues("ICaseCureGrid4");
    String[] strApplyFee     = request.getParameterValues("ICaseCureGrid5");
    String[] strFee     = request.getParameterValues("ICaseCureGrid6");

    int intLength=0;
    if(strNumber!=null)
      intLength=strNumber.length;
    for(int i=0;i<intLength;i++)
    {
      mzh_flag="1";//若有门诊信息则将mzh_flag置1
      LLCaseReceiptSchema tLLCaseReceiptSchema=new LLCaseReceiptSchema();
      tLLCaseReceiptSchema.setSerialNo(String.valueOf(i));
      tLLCaseReceiptSchema.setCaseNo(request.getParameter("CaseNo"));
      tLLCaseReceiptSchema.setCustomerNo(request.getParameter("CustomerNo"));
      tLLCaseReceiptSchema.setCustomerName(request.getParameter("CustomerName"));
      tLLCaseReceiptSchema.setFeeItemType(strFeeItemType[i]);
      tLLCaseReceiptSchema.setHospitalName(strHospitalName[i]);
      tLLCaseReceiptSchema.setFeeItemCode(strFeeItemCode[i]);
      tLLCaseReceiptSchema.setFeeItemName(strFeeItemName[i]);
      tLLCaseReceiptSchema.setFee(strFee[i]);
      tLLCaseReceiptSchema.setApplyFee(strApplyFee[i]);
      mLLCaseReceiptSet.add(tLLCaseReceiptSchema);
    }
  }
  //住院费用
  String[] strNumber1          = request.getParameterValues("HospitalGridNo");
  if(strNumber1==null)
  {
    mLLCaseCureSet.clear();
  }
  else
  {
    String[] strHospitalName1    = request.getParameterValues("HospitalGrid1");
    //添加费用项目类型和手术给付比例//住院费用
    String[] strAccidentType1    = request.getParameterValues("HospitalGrid2");
    String[] strFeeItemCode1     = request.getParameterValues("HospitalGrid3");
    String[] strFeeItemName1     = request.getParameterValues("HospitalGrid4");
    String[] strApplyFee1        = request.getParameterValues("HospitalGrid5");
    String[] strFee1             = request.getParameterValues("HospitalGrid6");

    int HospitalLengh=0;
    if(strNumber1!=null)
      HospitalLengh= strNumber1.length;
    System.out.println("￥￥￥￥2003-10-24医院发生的费用的记录是"+HospitalLengh);
    for(int j=0;j<HospitalLengh;j++)
    {
      zhy_flag = "1";//若有住院信息，则将zhy_flag置1
      LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
      tLLCaseCureSchema.setSerialNo("HO"+String.valueOf(j));
      tLLCaseCureSchema.setCaseNo(request.getParameter("CaseNo"));
      tLLCaseCureSchema.setCustomerNo(request.getParameter("CustomerNo"));
      tLLCaseCureSchema.setCustomerName(request.getParameter("CustomerName"));
      tLLCaseCureSchema.setHospitalName(strHospitalName1[j]);
      tLLCaseCureSchema.setAccidentType(strAccidentType1[j]);
      tLLCaseCureSchema.setFeeItemName(strFeeItemName1[j]);
      tLLCaseCureSchema.setFeeItemCode(strFeeItemCode1[j]);
      tLLCaseCureSchema.setFee(strFee1[j]);
      tLLCaseCureSchema.setApplyFee(strApplyFee1[j]);
      mLLCaseCureSet.add(tLLCaseCureSchema);
    }
  }

  //对救援费用的存储，存在LLCaseCure表中。代理机构代码存放在疾病名称中
  String[] strNumber4          = request.getParameterValues("SuccorGridNo");
  if(!(strNumber4==null))
  {
    String[] strHospitalName4    = request.getParameterValues("SuccorGrid1");
    String[] strDiseaseName4     = request.getParameterValues("SuccorGrid2");
    //添加费用项目类型和手术给付比例//住院费用
    String[] strAccidentType4    = request.getParameterValues("SuccorGrid3");
    String[] strFeeItemCode4     = request.getParameterValues("SuccorGrid4");
    String[] strFeeItemName4     = request.getParameterValues("SuccorGrid5");
    String[] strApplyFee4        = request.getParameterValues("SuccorGrid6");
    String[] strFee4             = request.getParameterValues("SuccorGrid7");

    int SuccorLengh=0;
    if(strNumber4!=null)
      SuccorLengh= strNumber4.length;
    System.out.println("￥￥￥￥2003-12-16救援费用的记录有"+SuccorLengh);
    for(int j=0;j<SuccorLengh;j++)
    {
      jy_flag = "1";//若有救援信息，则将zhy_flag置1
      LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
      tLLCaseCureSchema.setSerialNo("JY"+String.valueOf(j));
      System.out.println("救援信息是"+tLLCaseCureSchema.getSerialNo());
      tLLCaseCureSchema.setCaseNo(request.getParameter("CaseNo"));
      tLLCaseCureSchema.setCustomerNo(request.getParameter("CustomerNo"));
      tLLCaseCureSchema.setCustomerName(request.getParameter("CustomerName"));
      tLLCaseCureSchema.setHospitalName(strHospitalName4[j]);
      tLLCaseCureSchema.setDiseaseName(strDiseaseName4[j]);
      tLLCaseCureSchema.setAccidentType(strAccidentType4[j]);
      tLLCaseCureSchema.setFeeItemName(strFeeItemName4[j]);
      tLLCaseCureSchema.setFeeItemCode(strFeeItemCode4[j]);
      tLLCaseCureSchema.setFee(strFee4[j]);
      tLLCaseCureSchema.setApplyFee(strApplyFee4[j]);
      mLLCaseCureSet.add(tLLCaseCureSchema);
    }
  }

  //对手术信息的存储
  if(!(OperationType==null||OperationType.equals("")))
  {
    if(OperationType.equals("1"))
    {
      System.out.println("开始执行按比例进行赔付的信息");
      String[] strNumber3          = request.getParameterValues("DegreeOperationGridNo");
      if(strNumber3!=null)
      {
        String[] strFeeItemCode3     = request.getParameterValues("DegreeOperationGrid2");
        String[] strFeeItemName3     = request.getParameterValues("DegreeOperationGrid1");
        String[] strFee3             = request.getParameterValues("DegreeOperationGrid3");
        String[] strApplyFee3        = request.getParameterValues("DegreeOperationGrid4");
        int DegreeOperationLengh=0;
        if(strNumber3!=null)
          DegreeOperationLengh = strNumber3.length;
        for(int h=0;h<DegreeOperationLengh;h++)
        {
          LLCaseCureSchema bLLCaseCureSchema = new LLCaseCureSchema();
          bLLCaseCureSchema.setCaseNo(request.getParameter("CaseNo"));
          bLLCaseCureSchema.setSerialNo("DF"+String.valueOf(h));
          bLLCaseCureSchema.setCustomerName(request.getParameter("CustomerName"));
          bLLCaseCureSchema.setCustomerNo(request.getParameter("CustomerNo"));
          bLLCaseCureSchema.setFeeItemName(strFeeItemName3[h]);
          bLLCaseCureSchema.setFeeItemCode(strFeeItemCode3[h]);
          bLLCaseCureSchema.setFee(strFee3[h]);
          bLLCaseCureSchema.setApplyFee(strApplyFee3[h]);
          mLLCaseCureSet.add(bLLCaseCureSchema);
        }
      }
    }

    if(OperationType.equals("0"))
    {
      System.out.println("开始执行按费用进行手术赔付的程序");
      String[] strNumber2          = request.getParameterValues("FeeOperationGridNo");
      if(strNumber2!=null)
      {
        String[] strFeeItemCode2     = request.getParameterValues("FeeOperationGrid2");
        String[] strFeeItemName2     = request.getParameterValues("FeeOperationGrid1");
        String[] strFee2             = request.getParameterValues("FeeOperationGrid3");
        String[] strApplyFee2        = request.getParameterValues("FeeOperationGrid4");
        int FeeOperationLengh=0;
        if(strNumber2!=null)
          FeeOperationLengh = strNumber2.length;
        for(int k=0;k<FeeOperationLengh;k++)
        {
          LLCaseCureSchema aLLCaseCureSchema = new LLCaseCureSchema();
          aLLCaseCureSchema.setCaseNo(request.getParameter("CaseNo"));
          aLLCaseCureSchema.setSerialNo("FO"+String.valueOf(k));
          aLLCaseCureSchema.setCustomerNo(request.getParameter("CustomerNo"));
          aLLCaseCureSchema.setCustomerName(request.getParameter("CustomerName"));
          aLLCaseCureSchema.setFeeItemCode(strFeeItemCode2[k]);
          aLLCaseCureSchema.setFeeItemName(strFeeItemName2[k]);
          aLLCaseCureSchema.setFee(strFee2[k]);
          aLLCaseCureSchema.setApplyFee(strApplyFee2[k]);
          mLLCaseCureSet.add(aLLCaseCureSchema);
        }
      }
    }
  }

  //增加对救援费用的存储

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  try
  {
    VData tVData = new VData();
    tVData.addElement(RgtNo);//立案号码
    tVData.addElement(OperationType);//手术给付类型
    tVData.addElement(mzh_flag);//门诊信息标志
    tVData.addElement(zhy_flag);//住院信息标志
    tVData.addElement(jy_flag);//救援信息的标志
    tVData.addElement(mLLCaseReceiptSet);//门诊信息
    tVData.addElement(mLLCaseCureSet);//住院及手术信息
    tVData.addElement(mLLCaseSchema);
    tVData.addElement(tG);
    tCaseCureUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCaseCureUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>